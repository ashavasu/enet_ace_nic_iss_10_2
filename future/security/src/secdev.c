/*****************************************************************************/
/* Copyright (C) 2010 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2001-2010                                          */
/*                                                                           */
/* $Id: secdev.c,v 1.10 2011/09/08 07:01:34 siva Exp $                       */
/*                                                                           */
/*  FILE NAME             : secdev.c                                         */
/*  PRINCIPAL AUTHOR      : Aricent Inc.                                     */
/*  SUBSYSTEM NAME        : Kern Module                                      */
/*  MODULE NAME           : KERN                                             */
/*  LANGUAGE              : C                                                */
/*  TARGET ENVIRONMENT    : Any                                              */
/*  DATE OF FIRST RELEASE : 16 Dec 2010                                      */
/*  AUTHOR                : Aricent Inc.                                     */
/*  DESCRIPTION           : This file contains CHR interface related routines*/
/*****************************************************************************/
#ifndef _SECDEV_C_
#define _SECDEV_C_

#include "secinc.h"
#include "seckinc.h"
#include "sectrc.h"

static struct file_operations fops = {
  owner:THIS_MODULE,
  read:SecDevRead,
  write:SecDevWrite,
  ioctl:SecDevIoctl,
  open:SecDevOpen,
  release:SecDevRelease
};

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : SecDevOpen                                       */
/*                                                                           */
/*    Description         : Increments the refrence count of this chr device.*/
/*                                                                           */
/*    Input(s)            : pInode  - Pointer to the inode                   */
/*                        : pFile   - Pointer to the chr device              */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : 0 for success -1 for failure                      */
/*                                                                           */
/*****************************************************************************/
INT4
SecDevOpen (tInode * pInode, tFile * pFile)
{

    UNUSED_PARAM (pInode);
    pFile->private_data = NULL;
#ifdef LINUX_KERNEL_2_4_20VER
    MOD_INC_USE_COUNT;
#endif
    return 0;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : SecDevRelease                                    */
/*                                                                           */
/*    Description         : Decrements the refrence count of this chr device.*/
/*                                                                           */
/*    Input(s)            : pInode  - Pointer to the inode                   */
/*                        : pFile   - Pointer to the chr device              */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : 0 for success -1 for failure                      */
/*                                                                           */
/*****************************************************************************/

INT4
SecDevRelease (tInode * pInode, tFile * pFile)
{
    UINT4               u4ModData = 0;

    UNUSED_PARAM (pInode);

    u4ModData = *((UINT4 *) (pFile->private_data));

    if (NPSIM_MODULE == u4ModData)
    {
        OsixQueDel (gu4NpSimReadModIdx);
        OsixQueDel (gu4NpSimWriteModIdx);
        gu4NpSimReadModIdx = 0;
        gu4NpSimWriteModIdx = 0;
    }
    else if (SECUSR_MODULE == u4ModData)
    {
        OsixQueDel (gu4SecReadModIdx);
        OsixQueDel (gu4SecWriteModIdx);
        gu4SecReadModIdx = 0;
        gu4SecWriteModIdx = 0;
    }
    else if (SNORT_MODULE == u4ModData)
    {
        OsixQueDel (gu4SnortReadModIdx);
        OsixQueDel (gu4SnortWriteModIdx);
        gu4SnortReadModIdx = 0;
        gu4SnortWriteModIdx = 0;
    }
    pFile->private_data = NULL;
#ifdef LINUX_KERNEL_2_4_20VER
    MOD_DEC_USE_COUNT;
#endif
    return 0;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : SecDevIoctl                                      */
/*                                                                           */
/*    Description         : I/O ctrl function for this chr device.           */
/*                                                                           */
/*    Input(s)            : pInode  - Pointer to the inode                   */
/*                        : pFile   - Pointer to the chr device              */
/*                        : u4IoctlNum - Ioctl Number                        */
/*                        : ulIoctlParam - Arguments for the IOCTL           */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : 0 for success -1 for failure                      */
/*                                                                           */
/*****************************************************************************/
INT4
SecDevIoctl (tInode * pInode, tFile * pFile, UINT4 u4IoctlNum,
             FS_ULONG ulIoctlParam)
{
    INT4                i4RetVal = SECMOD_FAILURE;

    UNUSED_PARAM (pInode);
    switch (u4IoctlNum)
    {
#ifdef FIREWALL_WANTED
        case FWL_NMH_IOCTL:
            i4RetVal = FwlNmhIoctl (ulIoctlParam);
            break;
#endif

#ifdef NAT_WANTED
        case NAT_NMH_IOCTL:
            i4RetVal = NatNmhIoctl (ulIoctlParam);
            break;
#endif
#ifdef VPN_WANTED
        case VPN_NMH_IOCTL:
            i4RetVal = VpnNmhIoctl (ulIoctlParam);
            break;
#endif

#ifdef IPSECv4_WANTED
#ifdef IKE_WANTED
        case IKE_IPSEC_IOCTL:
            i4RetVal = Secv4Ioctl (ulIoctlParam);
            break;
#endif
#endif

        case SEC_MOD_INIT_IOCTL:
            i4RetVal = SecDevRegisterModule ((INT1) ulIoctlParam, pFile);
            break;

        case SEC_CFA_IF_CREATE:
        case SEC_CFA_IF_DELETE:
        case SEC_CFA_IF_UPDATE:
        case SEC_IP6_ADDR_ADD:
        case SEC_IP6_ADDR_DEL:
            i4RetVal = SecDevUpdateIfInfo (u4IoctlNum, ulIoctlParam);
            break;
#ifdef VPN_WANTED
        case FIPS_IPSEC_IOCTL:
            i4RetVal = Secv4FipsIoctl ();
            break;
#endif

        case SEC_CFA_ADD_PPP_SESSION_INFO:
            i4RetVal = SecDevPPPoEAddNewSession (ulIoctlParam);
            break;
        case SEC_CFA_DEL_PPP_SESSION_INFO:
            i4RetVal = SecDevPPPoEDelSession (ulIoctlParam);
            break;

#ifdef IPSECv6_WANTED
        case IPSECV6_IOCTL:
            i4RetVal = Secv6Ioctl (ulIoctlParam);
            break;
#endif
        case SEC_IVR_UPDT_IOCTL:
            i4RetVal = SecDevUpdateSecIvrIndex (ulIoctlParam);
            break;
        case SEC_BRIDGING_STAT_UPDT_IOCTL:
            i4RetVal = SecDevUpdateSecBridgingStatus (ulIoctlParam);
            break;
        case SEC_SET_VLAN_LIST:
            i4RetVal = SecDevSetVlanList (ulIoctlParam);
            break;
        case SEC_RESET_VLAN_LIST:
            i4RetVal = SecDevReSetVlanList (ulIoctlParam);
            break;
        default:
            break;
    }
    return i4RetVal;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : SecDevRead                                       */
/*                                                                           */
/*    Description         : Read function for this chr device.               */
/*                                                                           */
/*    Input(s)            : pFile   - Pointer to the chr device              */
/*                        : buf_length - size of the buffer                  */
/*                        : pos - postion to start the reading.              */
/*                                                                           */
/*    Output(s)           : pc1Buf - Buffer to store.                        */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : length of read data.                              */
/*                                                                           */
/*****************************************************************************/
ssize_t
SecDevRead (tFile * pFile, CHR1 * pc1Buf, size_t buf_length, loff_t * pos)
{
    tSkb               *pSkb = NULL;
    tOsixQId            QueId = 0;
    tOsixKernUserInfo   OsixKerUseInfo;
    UINT4               u4ModData = 0;

    if (buf_length == 0)
    {
        SEC_TRC (SEC_CONTROL_PLANE_TRC, "SecDevRead:"
                 "Invalid buffer length !!!\r\n");
        return 0;
    }

    if (pFile->private_data == NULL)
    {
        /*
         * SEC_MOD_INIT_IOCTL is not issued for this FD.
         */
        SEC_TRC (SEC_CONTROL_PLANE_TRC, "SecDevRead:"
                 "Invalid private data from the file !!!\r\n");
        return -EFAULT;
    }
    u4ModData = *((UINT4 *) (pFile->private_data));

    if (u4ModData == OSIX_FALSE)
    {
        SEC_TRC (SEC_CONTROL_PLANE_TRC, "SecDevRead:"
                 "Invalid module data !!!\r\n");
        return -EFAULT;
    }

    if (u4ModData == SECUSR_MODULE)
    {
        QueId = gu4SecReadModIdx;
    }
    else if (u4ModData == NPSIM_MODULE)
    {
        QueId = gu4NpSimReadModIdx;
    }
    else if (u4ModData == SNORT_MODULE)
    {
        QueId = gu4SnortReadModIdx;
    }
    else
    {
        SEC_TRC (SEC_CONTROL_PLANE_TRC, "SecDevRead:"
                 "Invalid Queue Id !!!\r\n");
        return -EFAULT;
    }

    if (OsixQueRecv (QueId, (UINT1 *) &pSkb, 0, 0) == OSIX_FAILURE)
    {
        SEC_TRC (SEC_CONTROL_PLANE_TRC, "SecDevRead:"
                 "Failed to receive message !!!\r\n");
        return -EFAULT;
    }

    if (NULL == pSkb)
    {
        SEC_TRC (SEC_CONTROL_PLANE_TRC, "SecDevRead:"
                 "NULL skb is received !!!\r\n");
        return -EFAULT;
    }

    if (pSkb->len <= *pos)
    {
        SEC_TRC (SEC_CONTROL_PLANE_TRC, "SecDevRead:" "Invalid length !!!\r\n");
        CRU_BUF_KernMemFreeSkb (pSkb);
        return -EFAULT;
    }

    /* check if requested len is greater than available len */
    if (buf_length > (pSkb->len - *pos))
    {
        buf_length = pSkb->len - *pos;

    }
    else
    {
        SEC_TRC (SEC_CONTROL_PLANE_TRC, "SecDevRead:" "Invalid length !!!\r\n");
        CRU_BUF_KernMemFreeSkb (pSkb);
        return -EFAULT;
    }

    /* copy the data to the user */
    OsixKerUseInfo.pDest = pc1Buf;
    OsixKerUseInfo.pSrc = pSkb->data + *pos;

    if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                     buf_length) == OSIX_FAILURE)
    {
        SEC_TRC (SEC_CONTROL_PLANE_TRC, "SecDevRead:"
                 "Failed to send message !!!\r\n");
        CRU_BUF_KernMemFreeSkb (pSkb);
        return -EFAULT;
    }

    CRU_BUF_KernMemFreeSkb (pSkb);
    return buf_length;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : SecDevWrite                                      */
/*                                                                           */
/*    Description         : Write function for this chr device.              */
/*                                                                           */
/*    Input(s)            : pFile   - Pointer to the chr device              */
/*                        : pBuf - Buffer to write.                          */
/*                        : length - size of the buffer                      */
/*                        : ppos - postion to start the writing.             */
/*                                                                           */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : length of written data.                           */
/*                                                                           */
/*****************************************************************************/
ssize_t
SecDevWrite (tFile * pFile, CONST CHR1 * pBuf, size_t length, loff_t * ppos)
{
    tSkb               *pSkb = NULL;
    tOsixQId            QueId;
    UINT4               u4ModData = 0;
    tOsixKernUserInfo   OsixKerUseInfo;
    tCRU_BUF_CHAIN_HEADER *pCruBuf = NULL;
    tSecQueMsg         *pSecQueMsg = NULL;
    UINT1               u1MsgType;
    UINT4               u4IfIndex = 0;
    UINT1               u1Direction = 0;
    INT1                i1RetVal;

    UNUSED_PARAM (ppos);

    if (length == 0)
    {
        SEC_TRC (SEC_CONTROL_PLANE_TRC, "SecDevWrite:"
                 "Invalid Length !!!\r\n");
        return 0;
    }

    if (pFile->private_data == NULL)
    {
        /*
         * SEC_MOD_INIT_IOCTL is not issued for this FD.
         */
        SEC_TRC (SEC_CONTROL_PLANE_TRC, "SecDevWrite:"
                 "Invalid private data in the file !!!\r\n");
        return -EFAULT;
    }

    u4ModData = *((UINT4 *) (pFile->private_data));

    if (u4ModData == OSIX_FALSE)
    {
        SEC_TRC (SEC_CONTROL_PLANE_TRC, "SecDevWrite:"
                 "Invalid Module Data !!!\r\n");
        return -EFAULT;
    }

    if (u4ModData == SECUSR_MODULE)
    {
        QueId = gu4SecWriteModIdx;
    }
    else if (u4ModData == NPSIM_MODULE)
    {
        QueId = gu4NpSimWriteModIdx;
    }
    else if (u4ModData == SNORT_MODULE)
    {
        QueId = gu4SnortWriteModIdx;
    }
    else
    {
        SEC_TRC (SEC_CONTROL_PLANE_TRC, "SecDevWrite:"
                 "Invalid Queue Id !!!\r\n");
        return -EFAULT;
    }
    pCruBuf = CRU_BUF_Allocate_MsgBufChain (length, 0);

    if (NULL == pCruBuf)
    {
        SEC_TRC (SEC_CONTROL_PLANE_TRC, "SecDevWrite:"
                 "CRU buffer Allocation Failed !!!\r\n");
        return -EFAULT;
    }
    if (NULL == pCruBuf->pSkb)
    {
        SEC_TRC (SEC_CONTROL_PLANE_TRC, "SecDevWrite:"
                 "Skb allocation is Failed !!!\r\n");
        return -EFAULT;
    }
    pSkb = pCruBuf->pSkb;

    pSkb->dev = NULL;

    OsixKerUseInfo.pDest = (VOID *) pSkb;
    OsixKerUseInfo.pSrc = (VOID *) pBuf;

    if (OsixQueRecv (gu4CopySkbModIdx, (UINT1 *) &OsixKerUseInfo, length, 0) ==
        OSIX_FAILURE)
    {
        SEC_TRC (SEC_CONTROL_PLANE_TRC, "SecDevWrite:"
                 "Copy to Skb is Failed !!!\r\n");
        CRU_BUF_Release_MsgBufChain (pCruBuf, FALSE);
        return -EFAULT;
    }

    pSecQueMsg = (tSecQueMsg *) pSkb->data;

    u1MsgType = pSecQueMsg->u1MsgType;
    u4IfIndex = pSecQueMsg->ModuleParam.PktInfo.i4InIfIdx;
    u1Direction = pSecQueMsg->ModuleParam.PktInfo.u1Direction;

    SEC_SET_MSGTYPE (pCruBuf, u1MsgType);
    SEC_SET_IFINDEX (pCruBuf, u4IfIndex);
    SEC_SET_DIRECTION (pCruBuf, u1Direction);

    if (CRU_BUF_MoveToReadOffset (pCruBuf, sizeof (tSecQueMsg), NULL, NULL)
        == CRU_FAILURE)
    {
        SEC_TRC (SEC_CONTROL_PLANE_TRC, "SecDevWrite:"
                 "Moving the buffer offset is failed !!!\r\n");
        CRU_BUF_Release_MsgBufChain (pCruBuf, FALSE);
        return -EFAULT;
    }

    i1RetVal =
        OsixQueSend (gKernCfaQueId, (UINT1 *) &pCruBuf, OSIX_DEF_MSG_LEN);

    if (i1RetVal != OSIX_SUCCESS)
    {
        CRU_BUF_Release_MsgBufChain (pCruBuf, FALSE);
        return -EFAULT;
    }

    /* trigger the packet arrival event for CFA */
    OsixEvtSend (gu4KernCfaTaskId, CFA_PACKET_ARRIVAL_EVENT);

    return length;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : SecDevUpdateIfInfo                               */
/*                                                                           */
/*    Description         : Utility function to update interface Information.*/
/*                                                                           */
/*    Input(s)            : pIfInfo- Pointer to the tSecIfInfo structure.    */
/*                        : u4Command - Received command                     */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : SECMOD_FAILURE or SECMOD_SUCCESS                  */
/*                                                                           */
/*****************************************************************************/

INT4
SecDevUpdateIfInfo (UINT4 u4Command, FS_ULONG ulPtr)
{
    INT4                i4Index = 0;
    tSecIfInfo          IfInfo;
    tOsixKernUserInfo   OsixKerUseInfo;

    OsixKerUseInfo.pDest = &IfInfo;
    OsixKerUseInfo.pSrc = (VOID *) ulPtr;

    if (OsixQueRecv
        (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo, sizeof (tSecIfInfo),
         0) == OSIX_FAILURE)
    {
        SEC_TRC (SEC_FAILURE_TRC, "SecDevUpdateIfInfo:"
                 "Failed to receive message !!!\r\n");
        return -EFAULT;
    }

    if (SecUtilUpdateIfInfo (u4Command, &IfInfo) == OSIX_FAILURE)
    {
        SEC_TRC (SEC_FAILURE_TRC, "SecDevUpdateIfInfo:"
                 "Update interface info failed !!!\r\n");
        return (-EFAULT);
    }

    return IOCTL_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : SecDevPPPoEAddNewSession                         */
/*                                                                           */
/*    Description         : Utility function to add PPP session Information. */
/*                                                                           */
/*    Input(s)            : ulPtr-Pointer to the tSecPppSessionInfo structure.*/
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : SECMOD_FAILURE or SECMOD_SUCCESS                  */
/*                                                                           */
/*****************************************************************************/

INT4
SecDevPPPoEAddNewSession (FS_ULONG ulPtr)
{
    tSecPppSessionInfo  PppSessionInfo;
    tOsixKernUserInfo   OsixKerUseInfo;

    OsixKerUseInfo.pDest = &PppSessionInfo;
    OsixKerUseInfo.pSrc = (VOID *) ulPtr;

    if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                     sizeof (tSecPppSessionInfo), 0) == OSIX_FAILURE)
    {
        SEC_TRC (SEC_FAILURE_TRC, "SecDevPPPoEAddNewSession:"
                 "Failed to receive message !!!\r\n");
        return (-EFAULT);
    }
    if (SecUtilPPPoEAddNewSession (&PppSessionInfo) == OSIX_FAILURE)
    {
        SEC_TRC (SEC_FAILURE_TRC, "SecDevPPPoEAddNewSession:"
                 "Adding PPP session info failed !!!\r\n");
        return (-EFAULT);
    }
    return IOCTL_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : SecDevPPPoEDelSession                            */
/*                                                                           */
/*    Description         : Utility function to delete PPP session Info.     */
/*                                                                           */
/*    Input(s)            : ulPtr-Pointer to the tSecPppSessionInfo structure.*/
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : SECMOD_FAILURE or SECMOD_SUCCESS                  */
/*                                                                           */
/*****************************************************************************/

INT4
SecDevPPPoEDelSession (FS_ULONG ulPtr)
{
    tSecPppSessionInfo  PppSessionInfo;
    tOsixKernUserInfo   OsixKerUseInfo;

    OsixKerUseInfo.pDest = &PppSessionInfo;
    OsixKerUseInfo.pSrc = (VOID *) ulPtr;

    if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                     sizeof (tSecPppSessionInfo), 0) == OSIX_FAILURE)
    {
        SEC_TRC (SEC_FAILURE_TRC, "SecDevPPPoEDelSession:"
                 "Failed to receive message !!!\r\n");
        return (-EFAULT);
    }
    if (SecUtilPPPoEDelSession (&PppSessionInfo) == OSIX_FAILURE)
    {
        SEC_TRC (SEC_FAILURE_TRC, "SecDevPPPoEDelSession:"
                 "Deleting PPP session info failed !!!\r\n");
        return (-EFAULT);
    }
    return IOCTL_SUCCESS;
}

/*****************************************************************************/
/* Function Name        : SecDevRegisterModule                               */
/*                                                                           */
/* Description          : This function checks whether the given MAC address */
/*                        contains                                           */
/*                       all zeroes                                          */
/*                                                                           */
/* Input(s)             : pu1MacAddr - Mac address                           */
/*                                                                           */
/* Output(s)            : None                                               */
/*                                                                           */
/* Global Variables                                                          */
/* Referred             : None                                               */
/*                                                                           */
/* Global Variables                                                          */
/* Modified             : None                                               */
/*                                                                           */
/* Return Value(s)      : OSIX_TRUE - If the MAC is all zeroes               */
/*                       OSIX_FALSE - otherwise                              */
/*****************************************************************************/

INT4
SecDevRegisterModule (INT1 i1Module, tFile * pFile)
{

    if (i1Module == SECUSR_MODULE)
    {
        gu4SecReadModIdx = KERN_USER_RQ;
        if (OsixQueCrt ("SCR", 0, 0, &gu4SecReadModIdx) == OSIX_FAILURE)
        {
            return (-EFAULT);
        }
        gu4SecWriteModIdx = KERN_USER_WQ;
        if (OsixQueCrt ("SCW", 0, 0, &gu4SecWriteModIdx) == OSIX_FAILURE)
        {
            return (-EFAULT);
        }
        gu4SecUsrModule = SECUSR_MODULE;
        pFile->private_data = (VOID *) &gu4SecUsrModule;
    }
    else if (i1Module == NPSIM_MODULE)
    {
        gu4NpSimReadModIdx = KERN_USER_RQ;
        if (OsixQueCrt ("NSR", 0, 0, &gu4NpSimReadModIdx) == OSIX_FAILURE)
        {
            return (-EFAULT);
        }
        gu4NpSimWriteModIdx = KERN_USER_WQ;
        if (OsixQueCrt ("NSW", 0, 0, &gu4NpSimWriteModIdx) == OSIX_FAILURE)
        {
            return (-EFAULT);
        }
        gu4NpSimModule = NPSIM_MODULE;
        pFile->private_data = (VOID *) &gu4NpSimModule;
    }
    else if (i1Module == SNORT_MODULE)
    {
        gu4SnortReadModIdx = KERN_USER_RQ;
        if (OsixQueCrt ("SNR", 0, 0, &gu4SnortReadModIdx) == OSIX_FAILURE)
        {
            return (-EFAULT);
        }
        gu4SnortWriteModIdx = KERN_USER_WQ;
        if (OsixQueCrt ("SNW", 0, 0, &gu4SnortWriteModIdx) == OSIX_FAILURE)
        {
            return (-EFAULT);
        }
        gu4SnortModule = SNORT_MODULE;
        pFile->private_data = (VOID *) &gu4SnortModule;
    }
    else
    {
        SEC_TRC (SEC_FAILURE_TRC, "SecDevRegisterModule:"
                 "Invalid Module Id !!!\r\n");
        return (-EFAULT);
    }
    return IOCTL_SUCCESS;
}

/*****************************************************************************/
/* Function Name        : SecDevRegisterChrDevice                            */
/*                                                                           */
/* Description          : This function is used to de-register the devices   */
/*                        in the kernel                                      */
/*                                                                           */
/* Input(s)             : pu1MacAddr - Mac address                           */
/*                                                                           */
/* Output(s)            : None                                               */
/*                                                                           */
/* Global Variables                                                          */
/* Referred             : None                                               */
/*                                                                           */
/* Global Variables                                                          */
/* Modified             : None                                               */
/*                                                                           */
/* Return Value(s)      : OSIX_TRUE - If the MAC is all zeroes               */
/*                       OSIX_FALSE - otherwise                              */
/*****************************************************************************/

INT4
SecDevRegisterChrDevice (VOID)
{
    UINT1               au1FileName[SYS_COMMAND_MAX_LEN];
    MEMSET (au1FileName, 0, SYS_COMMAND_MAX_LEN);
    SNPRINTF (au1FileName, STRLEN ("/dev/ksec"), "/dev/ksec");
#ifdef LINUX_KERNEL_2_4_20VER
    if (register_chrdev (SEC_MAJOR_NUMBER, SEC_DEVICE_FILE_NAME, pfops) < 0)
#else

    if ((register_chrdev (SEC_MAJOR_NUMBER, au1FileName,
                          (const struct file_operations *) &fops)) < 0)
#endif
    {
        SEC_TRC (SEC_FAILURE_TRC, "SecDevRegisterChrDevice:"
                 "Failed to create queue for dev_put !!!\r\n");
        return OSIX_FAILURE;
    }

    gu4CopyModIdx = KERN_USER_CTRL_Q;
    if (OsixQueCrt ("CPY", 0, 0, &gu4CopyModIdx) == OSIX_FAILURE)
    {
        SEC_TRC (SEC_FAILURE_TRC, "SecDevRegisterChrDevice:"
                 "Failed to create queue for copy from user"
                 "and copy to user !!!\r\n");
        return OSIX_FAILURE;
    }

    gu4CopySkbModIdx = KERN_USER_DATA_Q;
    if (OsixQueCrt ("CPS", 0, 0, &gu4CopySkbModIdx) == OSIX_FAILURE)
    {
        SEC_TRC (SEC_FAILURE_TRC, "SecDevRegisterChrDevice:"
                 "Failed to create queue for copy skb  !!!\r\n");
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : SecDevUpdateSecIvrIndex                          */
/*                                                                           */
/*    Description         : Util function to set the IVR interface index that*/
/*                          aids in applying security over bridged packets.  */
/*                                                                           */
/*    Input(s)            : ulIoctlParam - IVR CFA Index.                    */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*                                                                           */
/*****************************************************************************/
INT4
SecDevUpdateSecIvrIndex (FS_ULONG ulIoctlPtr)
{
    tOsixKernUserInfo   OsixKerUseInfo;

    OsixKerUseInfo.pDest = &gi4SecIvrIfIndex;
    OsixKerUseInfo.pSrc = (VOID *) ulIoctlPtr;

    if (OsixQueRecv
        (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo, sizeof (INT4),
         0) == OSIX_FAILURE)
    {
        SEC_TRC (SEC_FAILURE_TRC, "SecDevUpdateSecIvrIndex:"
                 "Failed to receive message !!!\r\n");
        return -EFAULT;
    }

    return IOCTL_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : SecApiUpdSecBridgingStatus                       */
/*                                                                           */
/*    Description         : API function to set the global variables that    */
/*                          controls whether security should be applied to   */
/*                          bridged packets or not.                          */
/*                                                                           */
/*    Input(s)            : ulIoctlParam - Brigding status var.              */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*                                                                           */
/*****************************************************************************/
INT4
SecDevUpdateSecBridgingStatus (FS_ULONG ulIoctlParam)
{
    tOsixKernUserInfo   OsixKerUseInfo;

    OsixKerUseInfo.pDest = &gu4SecBridgingStatus;
    OsixKerUseInfo.pSrc = (VOID *) ulIoctlParam;

    if (OsixQueRecv
        (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo, sizeof (UINT4),
         0) == OSIX_FAILURE)
    {
        SEC_TRC (SEC_FAILURE_TRC, "SecDevUpdateSecBridgingStatus:"
                 "Failed to receive message !!!\r\n");
        return -EFAULT;
    }

    return IOCTL_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : SecDevSetVlanList                                */
/*                                                                           */
/*    Description         : Util function to set the L2 VLAN list, which are */
/*                          eligible for applying security over the packets  */
/*                          received on the VLANs.                           */
/*                                                                           */
/*    Input(s)            : ulIoctlParam - List of security VLANs            */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*                                                                           */
/*****************************************************************************/
INT4
SecDevSetVlanList (FS_ULONG ulIoctlParam)
{
    tOsixKernUserInfo   OsixKerUseInfo;
    tSecVlanList        SecVlanList;

    OsixKerUseInfo.pDest = SecVlanList;
    OsixKerUseInfo.pSrc = (VOID *) ulIoctlParam;

    if (OsixQueRecv
        (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo, sizeof (tSecVlanList),
         0) == OSIX_FAILURE)
    {
        SEC_TRC (SEC_FAILURE_TRC, "SecDevUpdateSecBridgingStatus:"
                 "Failed to receive message !!!\r\n");
        return -EFAULT;
    }

    SEC_ADD_VLAN_LIST (gSecvlanList, SecVlanList);

    return IOCTL_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : SecDevReSetVlanList                              */
/*                                                                           */
/*    Description         : API function to remove few L2 VLANs from the L2  */
/*                          VLANs configured in the security VLAN list.      */
/*                                                                           */
/*    Input(s)            : ulIoctlParam - List of security VLANs            */
/*                          to be removed from the security VLAN list in the */
/*                          kernel address space.                            */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*                                                                           */
/*****************************************************************************/
INT4
SecDevReSetVlanList (FS_ULONG ulIoctlParam)
{
    tOsixKernUserInfo   OsixKerUseInfo;
    tSecVlanList        SecVlanList;

    OsixKerUseInfo.pDest = SecVlanList;
    OsixKerUseInfo.pSrc = (VOID *) ulIoctlParam;

    if (OsixQueRecv
        (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo, sizeof (tSecVlanList),
         0) == OSIX_FAILURE)
    {
        SEC_TRC (SEC_FAILURE_TRC, "SecDevUpdateSecBridgingStatus:"
                 "Failed to receive message !!!\r\n");
        return -EFAULT;
    }

    SEC_REMOVE_VLAN_LIST (gSecvlanList, SecVlanList);

    return IOCTL_SUCCESS;
}
#endif /* _SECDEV_C_ */
