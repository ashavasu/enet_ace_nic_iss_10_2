# include  "lr.h"
# include  "fssnmp.h"
# include  "fsseclw.h"
# include  "fssecwr.h"
# include  "fssecdb.h"

VOID
RegisterFSSEC ()
{
    SNMPRegisterMib (&fssecOID, &fssecEntry, SNMP_MSR_TGR_FALSE);
    SNMPAddSysorEntry (&fssecOID, (const UINT1 *) "fssec");
}

VOID
UnRegisterFSSEC ()
{
    SNMPUnRegisterMib (&fssecOID, &fssecEntry);
    SNMPDelSysorEntry (&fssecOID, (const UINT1 *) "fssec");
}

INT4
FsSecVersionInfoGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsSecVersionInfo (pMultiData->pOctetStrValue));
}

INT4
FsSecDebugOptionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsSecDebugOption (&(pMultiData->i4_SLongValue)));
}

INT4
FsSecReloadIdsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsSecReloadIds (&(pMultiData->i4_SLongValue)));
}

INT4
FsSecDebugOptionSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsSecDebugOption (pMultiData->i4_SLongValue));
}

INT4
FsSecReloadIdsSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsSecReloadIds (pMultiData->i4_SLongValue));
}

INT4
FsSecDebugOptionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsSecDebugOption (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsSecReloadIdsTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                    tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsSecReloadIds (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsSecDebugOptionDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsSecDebugOption (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsSecReloadIdsDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                   tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsSecReloadIds (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}
