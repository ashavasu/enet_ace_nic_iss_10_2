/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsseclw.c,v 1.1 2011/05/04 11:22:55 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "secinc.h"
# include  "secextn.h"

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsSecVersionInfo
 Input       :  The Indices

                The Object 
                retValFsSecVersionInfo
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSecVersionInfo (tSNMP_OCTET_STRING_TYPE * pRetValFsSecVersionInfo)
{
    tIdsGetInfo         IdsGetInfo;

    MEMSET (&IdsGetInfo, 0, sizeof (tIdsGetInfo));
    pRetValFsSecVersionInfo->i4_Length = 0;

    if (SecIdsGetInfo (ISS_IDS_VERSION_REQ, &IdsGetInfo) != IDS_FAILURE)
    {
        if (&IdsGetInfo != NULL)
        {
            pRetValFsSecVersionInfo->i4_Length = STRLEN (IdsGetInfo.au1Version);
            MEMCPY (pRetValFsSecVersionInfo->pu1_OctetList,
                    IdsGetInfo.au1Version, pRetValFsSecVersionInfo->i4_Length);
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsSecDebugOption
 Input       :  The Indices

                The Object 
                retValFsSecDebugOption
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSecDebugOption (INT4 *pi4RetValFsSecDebugOption)
{
    *pi4RetValFsSecDebugOption = (INT4) gIDSGlobalInfo.u4IdsTrcFlag;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsSecReloadIds
 Input       :  The Indices

                The Object 
                retValFsSecReloadIds
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSecReloadIds (INT4 *pi4RetValFsSecReloadIds)
{
    *pi4RetValFsSecReloadIds = 0;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsSecDebugOption
 Input       :  The Indices

                The Object 
                setValFsSecDebugOption
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsSecDebugOption (INT4 i4SetValFsSecDebugOption)
{
    gIDSGlobalInfo.u4IdsTrcFlag = (UINT4) i4SetValFsSecDebugOption;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsSecReloadIds
 Input       :  The Indices

                The Object 
                setValFsSecReloadIds
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsSecReloadIds (INT4 i4SetValFsSecReloadIds)
{
    UNUSED_PARAM (i4SetValFsSecReloadIds);

    if (SecIdsSetInfo (ISS_IDS_RESET_IDS, 0) != IDS_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsSecDebugOption
 Input       :  The Indices

                The Object 
                testValFsSecDebugOption
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsSecDebugOption (UINT4 *pu4ErrorCode, INT4 i4TestValFsSecDebugOption)
{
    if (i4TestValFsSecDebugOption < 0)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsSecReloadIds
 Input       :  The Indices

                The Object 
                testValFsSecReloadIds
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsSecReloadIds (UINT4 *pu4ErrorCode, INT4 i4TestValFsSecReloadIds)
{
    if (i4TestValFsSecReloadIds != IDS_RELOAD)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsSecDebugOption
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsSecDebugOption (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsSecReloadIds
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsSecReloadIds (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
