/*****************************************************************************/
/* Copyright (C) 2010 Aricent Inc . All Rights Reserved                      */
/* License Aricent Inc., 2001-2010                                           */
/*                                                                           */
/* $Id: secids.c,v 1.6 2011/07/15 10:49:14 siva Exp $                        */
/*                                                                           */
/*  FILE NAME             : secids.c                                         */
/*  PRINCIPAL AUTHOR      : Aricent Inc.                                     */
/*  SUBSYSTEM NAME        : Signature Based IDS                              */
/*  MODULE NAME           : IDS Module                                       */
/*  LANGUAGE              : C                                                */
/*  TARGET ENVIRONMENT    : Any                                              */
/*  DATE OF FIRST RELEASE : 31 Mar 2011                                      */
/*  AUTHOR                : Aricent Inc.                                     */
/*  DESCRIPTION           : This file contains IDS Task related routines     */
/*****************************************************************************/
#include "secinc.h"
#include "secextn.h"
#include "selutil.h"
#include "sizereg.h"
#include "secidssz.h"
#include "msr.h"

/*****************************************************************************
 *    Function Name       : SecIdsTaskMain
 *
 *    Description         : This is the main function called whenever the
 *                          ids task is created. It initializes the 
 *                          global variables associated with the ids  
 *                          thread in security module. 
 *
 *    Input(s)            : None
 *
 *    Output(s)           : NONE.
 *
 *    Returns             : OSIX_FAILURE/OSIX_SUCCESS
 *****************************************************************************/
PUBLIC UINT4
SecIdsTaskMain (VOID)
{
    UINT4               u4Events = 0;

    if (OSIX_FAILURE == SecIdsMainTaskInit ())
    {
        IDS_TRC (IDS_FAILURE_TRC, "SecIdsTaskMain:"
                 "IDS Task Initialization FAILED !!!\r\n");

        SecIdsMainTaskDeInit ();
        lrInitComplete (OSIX_FAILURE);
        return OSIX_FAILURE;
    }

    if (OsixGetTaskId (SELF, IDS_TASK_NAME, &(IDS_TASK_ID)) == OSIX_FAILURE)
    {
        IDS_TRC (IDS_RESOURCE_TRC | IDS_FAILURE_TRC,
                 "SecIdsTaskMain: Obtaining Task Id FAILED !!!\r\n");

        SecIdsMainTaskDeInit ();
        lrInitComplete (OSIX_FAILURE);
        return OSIX_FAILURE;
    }

    lrInitComplete (OSIX_SUCCESS);

    while (1)
    {
        if (OsixEvtRecv (IDS_TASK_ID, IDS_ALL_EVENTS, OSIX_WAIT, &u4Events)
            == OSIX_SUCCESS)
        {
            if (u4Events & IDS_PKT_ENQ_EVENT)
            {
                SecIdsHandleEnqPktEvt ();
            }
        }
    }                            /* End of While */

    return OSIX_SUCCESS;
}

/*****************************************************************************
 *    Function Name       : SecIdsMainTaskInit
 *
 *    Description         : This function is Used to Initialise IDS-IPC Module.
 *
 *    Input(s)            : None
 *
 *    Output(s)           : None.
 *
 *    Returns             : OSIX_SUCCESS/OSIX_FAILURE
 *****************************************************************************/
INT4
SecIdsMainTaskInit (VOID)
{
    MEMSET (&gIDSGlobalInfo, 0, sizeof (tIDSGlobalInfo));

    /* IDS Task creation of Queue */
    if (OsixQueCrt ((UINT1 *) IDS_TASK_QUE_NAME, OSIX_MAX_Q_MSG_LEN,
                    IDS_MAX_Q_DEPTH, &IDS_QUE_ID) == OSIX_FAILURE)
    {
        IDS_TRC (IDS_RESOURCE_TRC | IDS_FAILURE_TRC,
                 "SecIdsMainTaskInit: IDS Task Q Creation FAILED !!!\r\n");
        return OSIX_FAILURE;
    }

    /* Semphore creation for IDS Task */
    if (OsixCreateSem (IDS_SEMAPHORE, IDS_BINARY_SEM, IDS_SEM_FLAGS,
                       &gIdsSemId) == OSIX_FAILURE)
    {
        IDS_TRC (IDS_RESOURCE_TRC | IDS_FAILURE_TRC,
                 "SecIdsMainTaskInit: IDS semaphote Creation FAILED !!!\r\n");
        return OSIX_FAILURE;
    }

    /* Initialize Mempools */
    if (OSIX_FAILURE == SecidsSizingMemCreateMemPools ())
    {
        IDS_TRC (IDS_RESOURCE_TRC | IDS_FAILURE_TRC, "IdsMainTaskInit:"
                 " IDS Memory Initialization FAILED !!!\r\n");
        return OSIX_FAILURE;
    }

    /*Assigning Packet Buffer mempool ID */
    IDS_PKT_BUF_POOL_ID = SECIDSMemPoolIds[MAX_IDS_PKT_BUF_SIZING_ID];
    IDS_ATTACk_PKT_POOL_ID = SECIDSMemPoolIds[MAX_IDS_ATTACK_PKT_BUF_SIZING_ID];

    /* Set default trace flag */
    IDS_TRC_FLAG = IDS_TRC_NONE;

    /* InterProcess Communication Initialization */
    if (IDS_FAILURE == SecIdsIPCInit ())
    {
        IDS_TRC (IDS_RESOURCE_TRC | IDS_FAILURE_TRC, "IdsMainTaskInit:"
                 "IPC:Socket Initialization FAILED !!!\r\n");
        SecidsSizingMemDeleteMemPools ();
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *    Function Name       : SecIdsMainTaskDeInit
 *
 *    Description         : This function is Used to DeInitialise IDS-IPC Module.
 *
 *    Input(s)            : None
 *
 *    Output(s)           : None.
 *
 *    Returns             : OSIX_SUCCESS/OSIX_FAILURE
 *****************************************************************************/
INT4
SecIdsMainTaskDeInit (VOID)
{
    /* Delete the IDS Queue */
    if (IDS_QUE_ID != 0)
    {
        OsixQueDel (IDS_QUE_ID);
        IDS_QUE_ID = 0;
    }

    /* Delete the IDS Semaphore */
    if (gIdsSemId != 0)
    {
        OsixSemDel (gIdsSemId);
    }

    /* Delete Memory Pools */
    SecidsSizingMemDeleteMemPools ();

    /* Reset the global variables of IDS-IPC thread */
    IDS_PKT_BUF_POOL_ID = 0;
    IDS_TRC_FLAG = IDS_TRC_NONE;

    /* DeInit of InterProcess Communication */
    if (IDS_FAILURE == SecIdsIPCDeInit ())
    {
        IDS_TRC (IDS_RESOURCE_TRC | IDS_FAILURE_TRC, "SecIdsMainTaskDeInit:"
                 "Socket DeInitialization FAILED !!!\r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SecIdsEnqueueDataFrame                               */
/*                                                                           */
/* Description        : This function is invoked from secutity/ids module to */
/*                      send the data frame to the SEC-IDS task.             */
/*                                                                           */
/* Input(s)           : pBuf - data Frame                                    */
/*                      u1MesgType -Message Type, can be                     */
/*                      DATA_FROM_SEC_TO_IDS -posting data from secutity to  */
/*                                            IDS thread                     */
/*                      DATA_FROM_IDS_TO_CFA -posting data from IDS to cfa   */
/*                                            task                           */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : IDS_SUCCESS - On success.                            */
/*                      IDS_FAILURE - On failure.                            */
/*****************************************************************************/
INT4
SecIdsEnqueueDataFrame (tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 u1MesgType,
                        UINT4 u4IfIndex)
{
    if (NULL == pBuf)
    {
        IDS_TRC (IDS_FAILURE_TRC, "SecIdsEnqueueDataFrame:"
                 "Packet Buffer pointer is NULL !!!\n");
        return IDS_FAILURE;
    }

    if ((u1MesgType != DATA_FROM_SEC_TO_IDS)
        && (u1MesgType != DATA_FROM_IDS_TO_CFA))
    {
        IDS_TRC (IDS_FAILURE_TRC, "SecIdsEnqueueDataFrame:"
                 "Invalid MessageType for IDS DataFrame !!!\r\n");
        return IDS_FAILURE;
    }

    SEC_SET_COMMAND (pBuf, u1MesgType);
    SEC_SET_IFINDEX (pBuf, u4IfIndex);

    if (OsixQueSend (IDS_QUE_ID, (UINT1 *) &pBuf, OSIX_DEF_MSG_LEN)
        != OSIX_SUCCESS)
    {
        /* Release the allocated memory for the queue if the message
         * posting to the queue fails */
        IDS_TRC (IDS_FAILURE_TRC, "SecIdsEnqueueDataFrame:"
                 "Osix send to Queue Failed !!! \r\n");
        return IDS_FAILURE;
    }

    OsixEvtSend (IDS_TASK_ID, IDS_PKT_ENQ_EVENT);

    return IDS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SecIdsEnqueueCtrlMsg                                 */
/*                                                                           */
/* Description        : This function is invoked for sending log information */
/*                      to Ids and IDS will send the Blacklisted IP info to  */
/*                      ISS by invoking this function                        */
/*                                                                           */
/* Input(s)           : pBuf - data Frame                                    */
/*                      u1MesgType -Message Type, can be                     */
/*                      CTRLMSG_FRM_ISS_TO_IDS -posting dos pkt from cfa     */
/*                                              to IDS task                  */
/*                      CTRLMSG_FRM_IDS_TO_ISS -posting dos pkt from IDS     */
/*                                              to cfa task                  */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : IDS_SUCCESS - On success.                            */
/*                      IDS_FAILURE - On failure.                            */
/*****************************************************************************/
INT4
SecIdsEnqueueCtrlMsg (tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 u1MesgType)
{
    IDS_TRC (IDS_INIT_SHUT_TRC, "Entered SecIdsEnqueueCtrlMsg\r\n");

    if (NULL == pBuf)
    {
        IDS_TRC (IDS_FAILURE_TRC, "SecIdsEnqueueCtrlMsg:"
                 "Packet Buffer pointer is NULL !!!\n");
        return IDS_FAILURE;
    }

    if ((u1MesgType != CTRLMSG_FRM_ISS_TO_IDS)
        && (u1MesgType != CTRLMSG_FRM_IDS_TO_ISS))
    {
        IDS_TRC (IDS_FAILURE_TRC, "SecIdsEnqueueCtrlMsg:"
                 "Invalid MessageType for IDS Ctrl Message !!!\r\n");
        return IDS_FAILURE;
    }

    SEC_SET_COMMAND (pBuf, u1MesgType);

    if (OsixQueSend (IDS_QUE_ID, (UINT1 *) &pBuf, OSIX_DEF_MSG_LEN)
        != OSIX_SUCCESS)
    {
        /* Release the allocated memory for the queue if the posting
         * message to the queue fails */
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        IDS_TRC (IDS_FAILURE_TRC, "SecIdsEnqueueCtrlMsg:"
                 "Osix send to Queue Failed !!! \r\n");
        return IDS_FAILURE;
    }

    OsixEvtSend (IDS_TASK_ID, IDS_PKT_ENQ_EVENT);

    return IDS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SecIdsHandleEnqPktEvt                                */
/*                                                                           */
/* Description        : This function is invoked whenever the task receives  */
/*                      a packet Enque Event. This event is posted by        */
/*                      the security module for performing signature based   */
/*                      detection and posted by the IDS module for sending   */
/*                      back the valid frames to cfa task. And also for      */
/*                      sending the attack packets from ISS to Ids for       */
/*                      logging, and also for handling the trap message      */
/*                      indications from Ids.                                */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
SecIdsHandleEnqPktEvt (VOID)
{
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    UINT4               u4IfIndex = 0;
    UINT1               u1MsgType = 0;
    INT4                i4RetVal = 0;

    /* Event received, dequeue messages */
    while (OsixQueRecv (IDS_QUE_ID, (UINT1 *) &pBuf, OSIX_DEF_MSG_LEN,
                        OSIX_NO_WAIT) == OSIX_SUCCESS)
    {
        u1MsgType = SEC_GET_COMMAND (pBuf);
        u4IfIndex = SEC_GET_IFINDEX (pBuf);

        if (u1MsgType == DATA_FROM_SEC_TO_IDS)
        {
            SecIdsIpcSendDataPkt (pBuf, u4IfIndex);
        }
        else if (u1MsgType == DATA_FROM_IDS_TO_CFA)
        {
            SecUtilSendDataPktToCfa (pBuf, u4IfIndex);
        }
        else if (u1MsgType == CTRLMSG_FRM_ISS_TO_IDS)
        {
            SecIdsIpcSendCtrlMsg (pBuf);
        }
        else if (u1MsgType == CTRLMSG_FRM_IDS_TO_ISS)
        {
            /* MessageType for future use */
        }
        else if (u1MsgType == IDS_CTRL_MSG_TO_SEC)
        {
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            pBuf = NULL;
            i4RetVal = SecIdsIpcRcvCtrlMsg (pBuf);
            if (IDS_NO_DATA_READ != i4RetVal)
            {
                SelAddFd (ISS_IDS_CTRLFD, SecIdsIpcWriteCallBackFn);
            }
        }
        else if (u1MsgType == IDS_DATA_PKT_TO_SEC)
        {
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            pBuf = NULL;
            i4RetVal = SecIdsIpcRcvDataPkt (pBuf);
            if (IDS_NO_DATA_READ != i4RetVal)
            {
                SelAddFd (ISS_IDS_DATAFD, SecIdsIpcWriteCallBackFn);
            }
        }
        else
        {
            IDS_TRC (IDS_FAILURE_TRC, "SecIdsHandleEnqPktEvt:"
                     "Invalid Message type is received. !!!\r\n");
        }
    }
    return;
}

/*****************************************************************************/
/* Function Name      : SecIdsGetInfo                                        */
/*                                                                           */
/* Description        : This function do a IPC communication with Ids for    */
/*                      getting any of the following request.                */
/*                      a. Ids Version Request                               */
/*                      b. Global Statistics Information from Ids            */
/*                      c. Interface specific stats from Ids                 */
/*                                                                           */
/* Input(s)           :                                                      */
/*                      u4MsgType -Message Type, can be                      */
/*                      ISS_IDS_VERSION_REQ - for getting the current Ids    */
/*                                              version                      */
/*                      ISS_IDS_GLB_STATS_REQ - Allowed/Dropped counters     */
/*                      ISS_IDS_INTF_STATS_REQ - Allowed/Dropped counters    */
/*                                                 per interface             */
/*                                                                           */
/* Output(s)          : pIdsGetInfo - Version/stats data corresponding to    */
/*                                    message type                           */
/*                                                                           */
/* Return Value(s)    : IDS_SUCCESS - On success.                            */
/*                      IDS_FAILURE - On failure.                            */
/*****************************************************************************/
INT4
SecIdsGetInfo (UINT4 u4MsgType, tIdsGetInfo * pIdsGetInfo)
{
    UINT4               u4IfIndex = 0;

    IDS_TRC (IDS_INIT_SHUT_TRC, "Entered SecIdsGetInfo\r\n");

    if (!IS_IDS_MGMT_GET_MSG_VALID (u4MsgType))
    {
        IDS_TRC (IDS_FAILURE_TRC, "SecIdsGetInfo:"
                 "Invalid MessageType for IDS Get Information !!!\r\n");
        return IDS_FAILURE;
    }

    if (NULL == pIdsGetInfo)
    {
        IDS_TRC (IDS_FAILURE_TRC, "SecIdsGetInfo:"
                 "pIdsGetInfo is NULL inn IDS Get Information !!!\r\n");
        return IDS_FAILURE;
    }

    if (u4MsgType & ISS_IDS_INTF_STATS_REQ)
    {
        u4IfIndex = pIdsGetInfo->u4IfIndex;
    }

    IDS_LOCK ();

    if (IDS_SUCCESS != SecIdsIpcSendMgmtMsg (u4MsgType, u4IfIndex))
    {
        IDS_UNLOCK ();
        IDS_TRC (IDS_FAILURE_TRC, "SecIdsGetInfo:"
                 "Unable to send control message to Ids !!!\r\n");
        return IDS_FAILURE;
    }

    if (IDS_SUCCESS != SecIdsIpcRcvMgmtMsg (pIdsGetInfo))
    {
        IDS_UNLOCK ();
        IDS_TRC (IDS_FAILURE_TRC, "SecIdsGetInfo:"
                 "Unable to recv response message from Ids !!!\r\n");
        return IDS_FAILURE;
    }
    IDS_UNLOCK ();

    return IDS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SecIdsSetInfo                                        */
/*                                                                           */
/* Description        : This function do a IPC communication with Ids for    */
/*                      setting any of the following request.                */
/*                      a. set threshold for allowed packets                 */
/*                      b. set threshold for Ids log file                    */
/*                      c. set max size for Ids log file                     */
/*                      d. set log status as Enable/Disable                  */
/*                      e. Interface delete notification                     */
/*                      f. Clear global stats                                */
/*                      g. Clear interface specific stats                    */
/*                                                                           */
/* Input(s)           :                                                      */
/*                      u4MsgType -Message Type, can be                      */
/*                      ISS_IDS_SET_PKT_DROP_THRESH                          */
/*                      ISS_IDS_SET_LOG_THRESH                               */
/*                      ISS_IDS_SET_LOG_MAXSIZE                              */
/*                      ISS_IDS_SET_LOG_STATUS                               */
/*                      ISS_IDS_SET_DEL_INTF                                 */
/*                      ISS_IDS_CLEAR_GLB_STATS                              */
/*                      ISS_IDS_CLEAR_INTF_STATS                             */
/*                      ISS_IDS_RESET_IDS                                    */
/*                      u4Value - Incoming set value                         */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : IDS_SUCCESS - On success.                            */
/*                      IDS_FAILURE - On failure.                            */
/*****************************************************************************/
INT4
SecIdsSetInfo (UINT4 u4MsgType, UINT4 u4Value)
{
    tIdsGetInfo         IdsIpcGetInfo;

    IDS_TRC (IDS_INIT_SHUT_TRC, "Entered SecIdsSetInfo\r\n");

    MEMSET (&IdsIpcGetInfo, 0, sizeof (tIdsGetInfo));

    if (!IS_IDS_MGMT_SET_MSG_VALID (u4MsgType))
    {
        IDS_TRC (IDS_FAILURE_TRC, "SecIdsSetInfo:"
                 "Invalid MessageType for IDS Set Information !!!\r\n");
        return IDS_FAILURE;
    }

    IDS_LOCK ();

    if (IDS_SUCCESS != SecIdsIpcSendMgmtMsg (u4MsgType, u4Value))
    {
        IDS_UNLOCK ();
        IDS_TRC (IDS_CONTROL_PLANE_TRC, "SecIdsSetInfo:"
                 "Unable to send the configuration req to Ids !!!\r\n");
        return IDS_FAILURE;
    }

    if (IDS_SUCCESS != SecIdsIpcRcvMgmtMsg (&IdsIpcGetInfo))
    {
        IDS_UNLOCK ();
        IDS_TRC (IDS_CONTROL_PLANE_TRC, "SecIdsSetInfo:"
                 "Unable to get the conf response from Ids !!!\r\n");
        return IDS_FAILURE;
    }

    IDS_UNLOCK ();

    return IDS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SecIdsLock                                           */
/*                                                                           */
/* Description        : This function is used to take the IDS lock, to avoid */
/*                      simultaneous access to mangement socket communication*/
/*                      between Ids and IDS IPC thread.                      */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : IDS_SUCCESS - On success.                            */
/*                      IDS_FAILURE - On failure.                            */
/*****************************************************************************/
INT4
SecIdsLock (VOID)
{
    if (OsixSemTake (gIdsSemId) != OSIX_SUCCESS)
    {
        return IDS_FAILURE;
    }

    return IDS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SecIdsUnLock                                         */
/*                                                                           */
/* Description        : This function is used to give the IDS lock, to avoid */
/*                      simultaneous access to mangement socket communication*/
/*                      between Ids and IDS IPC thread.                      */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : IDS_SUCCESS - On success.                            */
/*                      IDS_FAILURE - On failure.                            */
/*****************************************************************************/
INT4
SecIdsUnLock (VOID)
{
    OsixSemGive (gIdsSemId);
    return IDS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SecIdsCheckForIdsFiles                               */
/*                                                                           */
/* Description        : This function is used to check whether the incoming  */
/*                      file is part of IDS files or not, if IDS files, this */
/*                      function will initiate the Ids reload.               */
/*                                                                           */
/* Input(s)           : pu1FileName - Incoming File Name                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
SecIdsCheckForIdsFiles (UINT1 *pu1FileName)
{
    FILE               *pFilePtr = NULL;
    UINT1               au1Value[IDS_PID_LEN];
    CHR1                ac1Cmd[IDS_CMD_LEN];
    UINT4               u4Pid = 0;

    IDS_TRC (IDS_INIT_SHUT_TRC, "Entered SecIdsCheckForIdsFiles\r\n");

    MEMSET (au1Value, 0, IDS_PID_LEN);
    MEMSET (ac1Cmd, 0, IDS_CMD_LEN);

    /* Check whether the incoming file is part of ids file or not */
    if ((STRSTR (pu1FileName, IDS_CONF_FILE)) ||
        (STRSTR (pu1FileName, IDS_RULES_DIR)))
    {
        pFilePtr = fopen (IDS_PID_FILE, "r");
        if (pFilePtr == NULL)
        {
            IDS_TRC (IDS_FAILURE_TRC, "SecIdsCheckForIdsFiles"
                     "Pid File is not available\r\n");
            return;
        }
        /* Read the file data */
        fgets ((char *) au1Value, IDS_PID_LEN, pFilePtr);

        if (0 != STRLEN (au1Value))
        {
            u4Pid = ATOI (au1Value);
        }

        if (0 != u4Pid)
        {
            SNPRINTF (ac1Cmd, sizeof (ac1Cmd), "kill -s 1 %d", u4Pid);
            system (ac1Cmd);
        }

        /* Close the File pointer */
        fclose (pFilePtr);
    }
}
