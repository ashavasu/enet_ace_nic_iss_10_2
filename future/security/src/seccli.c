/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: seccli.c,v 1.1 2011/05/04 11:22:59 siva Exp $
 *
 * Description: File containing CLI routines for firewall module
 * *******************************************************************/

#ifndef __SECCLI_C__
#define __SECCLI_C__
/* SOURCE FILE HEADER :
 *
 *  ---------------------------------------------------------------------------
 * |  FILE NAME             : seccli.c                                         |
 * |                                                                           |
 * |  PRINCIPAL AUTHOR      : Future Sofware Ltd                               |
 * |                                                                           |
 * |  SUBSYSTEM NAME        : CLI                                              |
 * |                                                                           |
 * |  MODULE NAME           : SECURITY                                         |
 * |                                                                           |
 * |  LANGUAGE              : C                                                |
 * |                                                                           |
 * |  TARGET ENVIRONMENT    :                                                  |
 * |                                                                           |
 * |  DATE OF FIRST RELEASE :                                                  |
 * |                                                                           |
 * |  DESCRIPTION           : Action routines for CLI SECURITY MODULE commands        |
 * |                                                                           |
 *  ---------------------------------------------------------------------------
 *
 */

#include "secinc.h"
#include "secids.h"
#include "secidscli.h"

/*****************************************************************************/
/*     FUNCTION NAME    : cli_process_sec_cmd                                */
/*                                                                           */
/*     DESCRIPTION      : This function takes in variable no. of arguments   */
/*                        and process the commands for the security module   */
/*                        defined in seccmd.def                              */
/*                                                                           */
/*     INPUT            : CliHandle -  CLIHandler                            */
/*                        u4Command -  Command Identifier                    */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/
INT4
cli_process_sec_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT4              *args[CLI_MAX_ARGS];
    INT1                argno = 0;
    INT4                i4RetVal = CLI_SUCCESS;

    va_start (ap, u4Command);

    /* Walk through the rest of the arguements and store in args array. 
     * Store fourteen arguements at the max. This is because fwl commands do not
     * take more than fourteen inputs from the command line. Another reason to
     * store is in some cases first input may be optional but user may give 
     * second input. In that case first arg will be null and second arg only 
     * has value */
    while (1)
    {
        args[argno++] = va_arg (ap, UINT4 *);

        if (argno == SEC_CLI_MAX_ARGS)
            break;
    }

    va_end (ap);

    switch (u4Command)
    {
        case CLI_SEC_SHOW_VERSION:
            i4RetVal = SecCliShowIdsVersion (CliHandle);
            break;

        case CLI_SEC_SET_DEBUG_LEVEL:
            i4RetVal = SecCliSetDebugLevel (CliHandle, CLI_PTR_TO_I4 (args[1]),
                                            CLI_ENABLE);
            break;

        case CLI_SEC_RESET_DEBUG_LEVEL:
            i4RetVal = SecCliSetDebugLevel (CliHandle, CLI_PTR_TO_I4 (args[1]),
                                            CLI_DISABLE);
            break;

        case CLI_SEC_RELOAD_IDS:
            i4RetVal = SecCliReloadIds (CliHandle, CLI_ENABLE);
            break;

        default:
            CliPrintf (CliHandle, "ERROR: Unknown command !\r\n");
            i4RetVal = CLI_FAILURE;
    }

    return i4RetVal;
}

/*****************************************************************************/
/*     FUNCTION NAME    : SecCliShowIdsVersion                               */
/*                                                                           */
/*     DESCRIPTION      : This function displays the current version of      */
/*                        IDS(Intrusion Detection System)                    */
/*                                                                           */
/*     INPUT            : CliHandle -  CLIHandler                            */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
SecCliShowIdsVersion (tCliHandle CliHandle)
{
    tSNMP_OCTET_STRING_TYPE IdsVersion;
    UINT1               au1IdsVersion[IDS_VERSION_LEN];

    MEMSET (au1IdsVersion, 0, IDS_VERSION_LEN);
    IdsVersion.pu1_OctetList = &au1IdsVersion[0];

    nmhGetFsSecVersionInfo (&IdsVersion);

    if (IdsVersion.i4_Length != 0)
    {
        CliPrintf (CliHandle, "\r\n%-33s : %s\r\n", "IDS Version",
                   IdsVersion.pu1_OctetList);
    }
    else
    {
        CliPrintf (CliHandle, "\r\n%-33s\r\n", " %% IDS is disabled");
    }

    CliPrintf (CliHandle, "\r\n");

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : SecCliSetDebugLevel                                */
/*                                                                           */
/*     DESCRIPTION      : This function sets the debug Level for security    */
/*                        module.                                            */
/*                                                                           */
/*     INPUT            : CliHandle -  CLIHandler                            */
/*                        i4CliDebugVal - Debug value.                       */
/*                        u1TraceFlag - Enable/Disable trace                 */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
SecCliSetDebugLevel (tCliHandle CliHandle, INT4 i4CliDebugVal,
                     UINT1 u1TraceFlag)
{
    INT4                i4DebugVal = 0;
    UINT4               u4ErrorCode = 0;

    /* Get existing Trace Option */
    nmhGetFsSecDebugOption (&i4DebugVal);

    if (u1TraceFlag == CLI_ENABLE)
    {
        i4DebugVal |= i4CliDebugVal;
    }
    else
    {
        i4DebugVal &= (~(i4CliDebugVal));
    }
    if (nmhTestv2FsSecDebugOption (&u4ErrorCode, i4DebugVal) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to set trace messages\r\n");
        return CLI_FAILURE;
    }
    if (nmhSetFsSecDebugOption (i4DebugVal) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to set trace messages\r\n");
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : SecCliReloadIds                                    */
/*                                                                           */
/*     DESCRIPTION      : This function informs to Snort for reload with     */
/*                        new set of rules/configurations                    */
/*                                                                           */
/*     INPUT            : CliHandle -  CLIHandler                            */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
SecCliReloadIds (tCliHandle CliHandle, INT4 i4Status)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsSecReloadIds (&u4ErrorCode, i4Status) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to Reload Ids \r\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsSecReloadIds (i4Status) == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\n%-33s", "Successfully informed"
                   " to Snort for reload");
    }
    else
    {
        CliPrintf (CliHandle, "\r\n%-33s\r\n", " %% IDS is disabled");
    }

    CliPrintf (CliHandle, "\r\n");

    return CLI_SUCCESS;
}
#endif /* __SECCLI_C__ */
