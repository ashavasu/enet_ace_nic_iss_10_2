/*****************************************************************************/
/* Copyright (C) 2011 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2001-2010                                          */
/*                                                                           */
/* $Id: secmain.c,v 1.11 2011/07/19 11:24:06 siva Exp $                       */
/*                                                                           */
/*  FILE NAME             : secmain.c                                        */
/*  PRINCIPAL AUTHOR      : Aricent Inc.                                     */
/*  SUBSYSTEM NAME        : Security Module                                  */
/*  MODULE NAME           : Security                                         */
/*  LANGUAGE              : C                                                */
/*  TARGET ENVIRONMENT    : Any                                              */
/*  DATE OF FIRST RELEASE : 31 Jan 2011                                      */
/*  AUTHOR                : Aricent Inc.                                     */
/*  DESCRIPTION           : This file contains Security Main Task related routines*/
/*****************************************************************************/
#ifndef _SECMAIN_C_
#define _SECMAIN_C_

#include "secinc.h"
#include "secglob.h"
#include "secextn.h"
#ifdef SECURITY_KERNEL_WANTED
#include "seckinc.h"
#endif
#include "sectrc.h"

/*****************************************************************************
 *
 *    Function Name       : SecMainTask
 *
 *    Description         : This is the main function called whenever the
 *                          security task is created. It initializes the 
 *                          global variables associated with the security  
 *                          module. 
 *
 *    Input(s)            : None
 *
 *    Output(s)           : NONE.
 *
 * Global Variables Referred : None.                                         
 *                                                                           
 * Global Variables Modified : None.                                         
 *                                                                           
 * Use of Recursion          : None.                                         
 *
 *    Output(s)           : NONE.
 *
 *    Returns             : OSIX_FAILURE/OSIX_SUCCESS
 *****************************************************************************/
PUBLIC UINT4
SecMainTask (VOID)
{
    lrInitComplete (OSIX_SUCCESS);

    if (SEC_KERN_USER == gi4SysOperMode)
    {
        SecInit ();
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name       : SecMainInit
 *
 *    Description         : This function is Used to Initialise Security
 *                               Module. 
 *                          If the gi4SysOperMode is SEC_KERN initializes
 *                          security module in the kernel.
 *
 *                          If the gi4SysOperMode is SEC_KERN_USER creates the 
 *                          device arsec with major number 200, loads the 
 *                          ISSSec.ko in the kernel, and calls the ioctl 
 *                          to create the sec user module queue in the kernel by
 *                          calling SecInitDevices.
 *                         
 *                          If the gi4SysOperMode is SEC_KERN_USER and SEC_USER 
 *                          security module will register with CFA to receive the 
 *                          interface creation/deletion/status updation from CFA, 
 *                          and initializes the gaSecWanIfInfo/gaSecLanIfInfo
 *
 *
 *    Input(s)            : None
 *
 *    Output(s)           : NONE.
 *
 *    Global Variables Referred : gi4SysOperMode.                                         
 *                                                                           
 *    Global Variables Modified : None.                                         
 *                                                                           
 *    Use of Recursion          : None.                                         
 *
 *    Output(s)           : NONE.
 *
 *    Returns             : OSIX_SUCCESS/OSIX_FAILURE
 *****************************************************************************/
INT4
SecMainInit (VOID)
{

    INT4                i4Index = 0;
    UINT1               SemName[4];

    STRCPY (SemName, "SEC");

    /* Initialize the security module in kernel when the gi4SysOperMode 
     * is SEC_KERN */
    if (SEC_KERN == gi4SysOperMode)
    {
        if (OSIX_FAILURE == SeckInit ())
        {
            SEC_TRC (SEC_FAILURE_TRC, "SecMainInit:"
                     "Failed to initialize security in kernel !!!\r\n");
            SecMainDeInit ();
            lrInitComplete (OSIX_FAILURE);
            return OSIX_FAILURE;
        }
    }
    /* Initialize the user space part of the security module when the 
     * gi4SysOperMode is SEC_KERN_USER */
    if (SEC_KERN_USER == gi4SysOperMode)
    {
        if (OSIX_FAILURE == SecInitDevices ())
        {
            SEC_TRC (SEC_FAILURE_TRC, "SecMainInit:"
                     "Failed to initialize security in user !!!\r\n");
            SecMainDeInit ();
            lrInitComplete (OSIX_FAILURE);
            return OSIX_FAILURE;
        }
    }

    /* Initialize Security interface info  to UNALLOCATED */

    for (i4Index = 0; i4Index < SYS_MAX_WAN_INTERFACES; i4Index++)
    {
        gaSecWanIfInfo[i4Index].i4IfIndex = SEC_UNALLOCATED;
    }

    for (i4Index = 0; i4Index < SYS_MAX_LAN_INTERFACES; i4Index++)
    {
        gaSecLanIfInfo[i4Index].i4IfIndex = SEC_UNALLOCATED;
    }

    /* Initialize Mempools */
    if (OSIX_FAILURE == SecSizingMemCreateMemPools ())
    {
        SEC_TRC (SEC_RESOURCE_TRC | SEC_FAILURE_TRC, "SeckInit:"
                 " Security Memory Initialization FAILED !!!\r\n");
        return OSIX_FAILURE;
    }
    /* gSecPppSessionInfoList maintains the PPP session information. 
     */
    TMO_SLL_Init (&gSecPppSessionInfoList);

    /* Register with CFA to receive the interface create/delete/status 
     * change from CFA.  */

    if (SEC_KERN != gi4SysOperMode)
    {
        if (SecInitRegWithCfa () == OSIX_FAILURE)
        {
            SEC_TRC (SEC_FAILURE_TRC, "SecMainInit:"
                     "Failed to register with CFA !!!\r\n");
            SecMainDeInit ();
            lrInitComplete (OSIX_FAILURE);
            return OSIX_FAILURE;
        }
    }

    /* Register with Netip6 to receive ipv6 interface change notification
     * from Netip6 Module. */
    if (SEC_KERN != gi4SysOperMode)
    {
        if (SecInitRegWithIpv6 () == OSIX_FAILURE)
        {
            SEC_TRC (SEC_FAILURE_TRC, "SecMainInit:"
                     "Failed to register with IPv6 Module !!!\r\n");
            SecMainDeInit ();
            lrInitComplete (OSIX_FAILURE);
            return OSIX_FAILURE;
        }
    }

#ifdef SNMP_2_WANTED
    /* Register the MIB with SNMP */
    RegisterFSSEC ();
#endif
    lrInitComplete (OSIX_SUCCESS);

    return OSIX_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name       : SecMainDeInit
 *
 *    Description         : This function is Used to DeInitialise Security
 *                                   Module. 
 *
 *    Input(s)            : None.
 *
 *    Output(s)           : NONE.
 *
 *    Global Variables Referred : gi4SysOperMode.                                         
 *                                                                           
 *    Global Variables Modified : None.                                         
 *                                                                           
 *    Use of Recursion     : None.                                         
 *
 *    Output(s)            : NONE.
 *
 *    Returns              : OSIX_SUCCESS/OSIX_FAILURE
 *****************************************************************************/

INT4
SecMainDeInit (VOID)
{
    tSecPppSessionInfo *pSecPppSessionNode = NULL;

    INT4                i4Index = 0;

    if (SEC_KERN == gi4SysOperMode)
    {
        SeckDeInit ();
    }

    /* De Initialize the user space part of the security module when the 
     * gi4SysOperMode is SEC_KERN_USER */
    if (SEC_KERN_USER == gi4SysOperMode)
    {
        SecDeInitDevices ();
    }

    /* DeInitialize Security interface info  to UNALLOCATED */

    for (i4Index = 0; i4Index < SYS_MAX_WAN_INTERFACES; i4Index++)
    {
        gaSecWanIfInfo[i4Index].i4IfIndex = SEC_UNALLOCATED;
    }

    /* DeInitialize Security interface info  to UNALLOCATED */

    for (i4Index = 0; i4Index < SYS_MAX_LAN_INTERFACES; i4Index++)
    {
        gaSecLanIfInfo[i4Index].i4IfIndex = SEC_UNALLOCATED;
    }

    /* delete the Session Info List */
    while (TMO_SLL_Count (&gSecPppSessionInfoList) != 0)
    {
        pSecPppSessionNode =
            (tSecPppSessionInfo *) TMO_SLL_Get (&gSecPppSessionInfoList);
        MemReleaseMemBlock (PPPOE_DEF_SESSION_POOL_ID,
                            (UINT1 *) pSecPppSessionNode);
    }
    /* Delete Memory Pools */
    SecSizingMemDeleteMemPools ();

    /* Register with CFA to receive the interface create/delete/status change from CFA.  */
    if (SEC_KERN != gi4SysOperMode)
    {
        SecInitDeRegWithCfa ();
    }

    /* DeRegister with netip6 to stop receiving the interface change notification from IP.  */
    if (SEC_KERN != gi4SysOperMode)
    {
        SecInitDeRegWithIpv6 ();
    }
    return OSIX_SUCCESS;

}
#endif /* _SECMAIN_C_ */
