#define _SECSZ_C
#include "secinc.h"
extern INT4
 
 
 
 
IssSzRegisterModuleSizingParams (CHR1 * pu1ModName,
                                 tFsModSizingParams * pModSizingParams);
INT4
SecSizingMemCreateMemPools ()
{
    INT4                i4RetVal;
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < SEC_MAX_SIZING_ID; i4SizingId++)
    {
        i4RetVal = MemCreateMemPool (FsSECSizingParams[i4SizingId].u4StructSize,
                                     FsSECSizingParams[i4SizingId].
                                     u4PreAllocatedUnits,
                                     MEM_DEFAULT_MEMORY_TYPE,
                                     &(SECMemPoolIds[i4SizingId]));
        if (i4RetVal == (INT4) MEM_FAILURE)
        {
            SecSizingMemDeleteMemPools ();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

INT4
SecSzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, FsSECSizingParams);
    return OSIX_SUCCESS;
}

VOID
SecSizingMemDeleteMemPools ()
{
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < SEC_MAX_SIZING_ID; i4SizingId++)
    {
        if (SECMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (SECMemPoolIds[i4SizingId]);
            SECMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
