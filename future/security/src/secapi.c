/*****************************************************************************/
/* Copyright (C) 2011 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2001-2011                                          */
/*                                                                           */
/* $Id: secapi.c,v 1.14 2011/09/08 07:01:34 siva Exp $                        */
/*                                                                           */
/*  FILE NAME             : secapi.c                                         */
/*  PRINCIPAL AUTHOR      : Aricent Inc.                                     */
/*  MODULE NAME           : Security Module                                  */
/*  LANGUAGE              : C                                                */
/*  TARGET ENVIRONMENT    : Any                                              */
/*  DATE OF FIRST RELEASE : 16 Dec 2010                                      */
/*  AUTHOR                : Aricent Inc.                                     */
/*  DESCRIPTION           : This file contains APIs for security             */
/*                          module                                           */
/*****************************************************************************/
#ifndef _SECAPI_C_
#define _SECAPI_C_

#include "secinc.h"
#include "secextn.h"
#include "sectrc.h"

/*****************************************************************************
 *
 *    Function Name       : SecApiIntfCreateCallBack
 *
 *    Description         : This is the callback API provided by the Sec 
 *                           module to the Interface manager in the system 
 *                           to notify about interface creation that has   
 *                           occured in the system. In ISS, this will be   
 *                           invoked by the CFA module.                    
 *
 *    Input(s)            : pCfaRegInfo - Pointer to Cfa Registration Info.
 *
 *    Output(s)           : NONE.
 *                                                                           
 * Global Variables Referred : None.                                         
 *                                                                           
 * Global Variables Modified : None.                                         
 *                                                                           
 * Use of Recursion          : None.                                         
 *                                                                           
 * Returns                   : OSIX_SUCCESS/OSIX_FAILURE                       
 *                                                                           
 *****************************************************************************/
PUBLIC INT4
SecApiIntfCreateCallBack (tCfaRegInfo * pCfaRegInfo)
{
    tSecIfInfo          SecIfInfo;
    tCfaIfInfo          IfInfo;
    tIpConfigInfo       IpInfo;
    UINT1               u1NwType = 0;
    tSecPppSessionInfo  SecPppSessionInfo;

    MEMSET (&IfInfo, 0, sizeof (tCfaIfInfo));
    MEMSET (&SecIfInfo, 0, sizeof (tSecIfInfo));
    MEMSET (&IpInfo, 0, sizeof (tIpConfigInfo));
    MEMSET (&SecPppSessionInfo, 0, sizeof (tSecPppSessionInfo));

    if (SEC_KERN != gi4SysOperMode)
    {
        /* Get the interface info */
        if (CFA_FAILURE == CfaGetIfInfo (pCfaRegInfo->u4IfIndex, &IfInfo))
        {
            SEC_TRC (SEC_FAILURE_TRC, "SecApiIntfCreateCallBack:"
                     "Failed to get the interface info from CFA !!!\r\n");
            return OSIX_FAILURE;
        }

        SecIfInfo.u1NwType = IfInfo.u1NwType;

        /* Get the IP interface info */
        if (CFA_FAILURE == CfaIpIfGetIfInfo (pCfaRegInfo->u4IfIndex, &(IpInfo)))
        {
            CfaGetIfNwType (pCfaRegInfo->u4IfIndex, &u1NwType);
            if (CFA_NETWORK_TYPE_WAN != u1NwType)
            {
                return OSIX_FAILURE;
            }
        }

        if (pCfaRegInfo->CfaIntfInfo.u2VlanId != 0)
        {
            SecUpdateVlanWanInfo (pCfaRegInfo->CfaIntfInfo.u2VlanId,
                                  &SecIfInfo);
            SecIfInfo.u2VlanId = pCfaRegInfo->CfaIntfInfo.u2VlanId;
        }
        else
        {
            if (IfInfo.u1IfType == CFA_PPP)
            {
                SecPppSessionInfo.i4PPPIfIndex = pCfaRegInfo->u4IfIndex;
                SecUtilPPPoEGetSessionInfo (&SecPppSessionInfo);
                SecIfInfo.i4PhyIndex = SecPppSessionInfo.i4IfIndex;
                SecIfInfo.u2VlanId =
                    SecPortFsNpGetDummyVlanId (SecPppSessionInfo.i4IfIndex);
            }
            else
            {
                SecIfInfo.u2VlanId =
                    SecPortFsNpGetDummyVlanId (pCfaRegInfo->u4IfIndex);
            }
        }

        SecIfInfo.i4IfIndex = (INT4) pCfaRegInfo->u4IfIndex;
        SecIfInfo.u1WanType = IfInfo.u1WanType;
        SecIfInfo.u4Mtu = IfInfo.u4IfMtu;
        SecIfInfo.u4IpAddr = IpInfo.u4Addr;
        SecIfInfo.u4NetMask = IpInfo.u4NetMask;
        SecIfInfo.u4BcastAddr = IpInfo.u4BcastAddr;
        SecIfInfo.u1OperStatus = IfInfo.u1IfOperStatus;
        SecIfInfo.u1IfType = IfInfo.u1IfType;

        MEMCPY (SecIfInfo.au1InfName, IfInfo.au1IfName,
                CFA_MAX_PORT_NAME_LENGTH);

        MEMCPY (SecIfInfo.MacAddress, IfInfo.au1MacAddr, CFA_ENET_ADDR_LEN);

        /* Update the gaSecLanIfInfo/gaSecWanIfInfo */
        if (OSIX_FAILURE ==
            SecApiUpdateIfInfo (SEC_MOD_CFA_IF_CREATE, &SecIfInfo))
        {
            SEC_TRC (SEC_FAILURE_TRC, "SecApiIntfCreateCallBack:"
                     "Failed to update the interface info in user space !!!\r\n");
            return OSIX_FAILURE;
        }
        if (SEC_KERN_USER == gi4SysOperMode)
        {
            if (SecInitInvokeIoctl (SEC_CFA_IF_CREATE, &SecIfInfo) ==
                OSIX_FAILURE)
            {
                SEC_TRC (SEC_FAILURE_TRC, "SecApiIntfCreateCallBack:"
                         "ioctl Failed to send the interface info to kernel !!!\r\n");
                return OSIX_FAILURE;
            }
        }

        SecNotifyHigherLayerIntfStatus (&SecIfInfo, &IpInfo,
                                        pCfaRegInfo->u4IfIndex, CFA_IF_CREATE);
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name       : SecApiIntfDelCallBack
 * 
 *    Description         : This is the callback API provided by the Sec 
 *                           module to the Interface manager in the system 
 *                           to notify about interface deletion that has   
 *                           occured in the system. In ISS, this will be   
 *                           invoked by the CFA module.                    
 *
 *    Input(s)            : pCfaRegInfo - Pointer to Cfa Registration Info.
 *
 *    Output(s)           : NONE.
 *
 * Global Variables Referred : None.                                         
 *                                                                           
 * Global Variables Modified : None.                                         
 *                                                                           
 * Use of Recursion          : None.                                         
 *                                                                           
 *    Returns             : OSIX_SUCCESS/OSIX_FAILURE
 *****************************************************************************/
PUBLIC INT4
SecApiIntfDelCallBack (tCfaRegInfo * pCfaRegInfo)
{
    tSecIfInfo          SecIfInfo;
    tCfaIfInfo          IfInfo;

    MEMSET (&SecIfInfo, 0, sizeof (tSecIfInfo));
    MEMSET (&IfInfo, 0, sizeof (tCfaIfInfo));

    if (SEC_KERN != gi4SysOperMode)
    {
        SecIfInfo.i4IfIndex = (INT4) pCfaRegInfo->u4IfIndex;

        if (CFA_FAILURE == CfaGetIfInfo (pCfaRegInfo->u4IfIndex, &IfInfo))
        {
            SEC_TRC (SEC_FAILURE_TRC, "SecApiIntfUpdtCallBack:"
                     "Failed to get the interface info from CFA !!!\r\n");
            return OSIX_FAILURE;
        }

        SecNotifyHigherLayerIntfStatus (&SecIfInfo, NULL,
                                        pCfaRegInfo->u4IfIndex, CFA_IF_DELETE);
        /* Delete the interface */
        if (OSIX_FAILURE ==
            SecApiUpdateIfInfo (SEC_MOD_CFA_IF_DELETE, &SecIfInfo))
        {
            SEC_TRC (SEC_FAILURE_TRC, "SecApiIntfDelCallBack:"
                     "Failed to update the interface info in user space !!!\r\n");
            return OSIX_FAILURE;
        }
        if (SEC_KERN_USER == gi4SysOperMode)
        {
            if (SecInitInvokeIoctl (SEC_CFA_IF_DELETE, &SecIfInfo) ==
                OSIX_FAILURE)
            {
                SEC_TRC (SEC_FAILURE_TRC, "SecApiIntfDelCallBack:"
                         "Failed to send the interface info to kernel space !!!\r\n");
                return OSIX_FAILURE;
            }
        }

    }

    return OSIX_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name       : SecApiIntfUpdtCallBack
 *
 *    Description         : This is the callback API provided by the Sec 
 *                           module to the Interface manager in the system 
 *                           to notify about interface deletion that has   
 *                           occured in the system. In ISS, this will be   
 *                           invoked by the CFA module.                    
 *
 *    Input(s)            : pCfaRegInfo - Pointer to Cfa Registration Info.
 *
 *    Output(s)           : NONE.
 *
 * Global Variables Referred : None.                                         
 *                                                                           
 * Global Variables Modified : None.                                         
 *                                                                           
 * Use of Recursion          : None.                                         
 *
 *    Returns             : OSIX_SUCCESS/OSIX_FAILURE
 *****************************************************************************/
PUBLIC INT4
SecApiIntfUpdtCallBack (tCfaRegInfo * pCfaRegInfo)
{
    tSecIfInfo          SecIfInfo;
    tCfaIfInfo          IfInfo;
    tIpConfigInfo       IpInfo;
    UINT1               u1NwType = 0;
    UINT1               u1Action = CFA_IF_UPDATE;
    tSecPppSessionInfo  SecPppSessionInfo;

    MEMSET (&SecIfInfo, 0, sizeof (tSecIfInfo));
    MEMSET (&IfInfo, 0, sizeof (tCfaIfInfo));
    MEMSET (&IpInfo, 0, sizeof (tIpConfigInfo));
    MEMSET (&SecPppSessionInfo, 0, sizeof (tSecPppSessionInfo));

    if (SEC_KERN != gi4SysOperMode)
    {
        /* Interface creation notification received from CFA through Update
         * Callback(e.g. PPP interface creation will be notified only through
         * this update callback handler from CFA)
         * SecNotifyHigherLayerIntfStatus should be called with the 
         * action set to CFA_IF_CREATE. 
         */
        if (OSIX_FAILURE == SecUtilGetIfInfo (pCfaRegInfo->u4IfIndex, &IfInfo))
        {
            u1Action = CFA_IF_CREATE;
        }
        else
        {
            u1Action = CFA_IF_UPDATE;
        }
        /* Get the interface info */
        if (CFA_FAILURE == CfaGetIfInfo (pCfaRegInfo->u4IfIndex, &IfInfo))
        {
            SEC_TRC (SEC_FAILURE_TRC, "SecApiIntfUpdtCallBack:"
                     "Failed to get the interface info from CFA !!!\r\n");
            return OSIX_FAILURE;
        }

        if (OSIX_SUCCESS ==
            SecUtilGetIfNwType (pCfaRegInfo->u4IfIndex, &u1NwType))
        {
            /* If the interface network type has changed from LAN to WAN or vice-versa
             * delete the interface info from SEC database and create afresh.
             * This is to avoid dual presence of the interface information in LAN and
             * WAN database. 
             */
            if (u1NwType != IfInfo.u1NwType)
            {
                SecApiIntfDelCallBack (pCfaRegInfo);
                SecNotifyHigherLayerIntfStatus (&SecIfInfo, &IpInfo,
                                                pCfaRegInfo->u4IfIndex,
                                                CFA_IF_DELETE);
                u1Action = CFA_IF_CREATE;
            }
        }

        SecIfInfo.u1NwType = IfInfo.u1NwType;
        SecIfInfo.u1WanType = IfInfo.u1WanType;

        /* Get the IP interface info */
        if (CFA_FAILURE == CfaIpIfGetIfInfo (pCfaRegInfo->u4IfIndex, &(IpInfo)))
        {
            return OSIX_FAILURE;
        }

        /* For L3 VLANs, check if any of the member ports are of WAN type
         * If so, mark the VLAN as WAN type. 
         */
        if (pCfaRegInfo->CfaIntfInfo.u2VlanId != 0)
        {
            SecUpdateVlanWanInfo (pCfaRegInfo->CfaIntfInfo.u2VlanId,
                                  &SecIfInfo);
            SecIfInfo.u2VlanId = pCfaRegInfo->CfaIntfInfo.u2VlanId;
        }
        else
        {
            if (IfInfo.u1IfType == CFA_PPP)
            {
                SecPppSessionInfo.i4PPPIfIndex = pCfaRegInfo->u4IfIndex;
                SecUtilPPPoEGetSessionInfo (&SecPppSessionInfo);
                SecIfInfo.i4PhyIndex = SecPppSessionInfo.i4IfIndex;
                SecIfInfo.u2VlanId =
                    SecPortFsNpGetDummyVlanId (SecPppSessionInfo.i4IfIndex);
            }
            else
            {
                SecIfInfo.u2VlanId =
                    SecPortFsNpGetDummyVlanId (pCfaRegInfo->u4IfIndex);
            }
        }

        SecIfInfo.i4IfIndex = (INT4) pCfaRegInfo->u4IfIndex;
        SecIfInfo.u4Mtu = IfInfo.u4IfMtu;
        SecIfInfo.u4IpAddr = IpInfo.u4Addr;
        SecIfInfo.u4NetMask = IpInfo.u4NetMask;
        SecIfInfo.u4BcastAddr = IpInfo.u4BcastAddr;
        SecIfInfo.u1OperStatus = IfInfo.u1IfOperStatus;
        SecIfInfo.u1IfType = IfInfo.u1IfType;

        MEMCPY (SecIfInfo.au1InfName, IfInfo.au1IfName,
                CFA_MAX_PORT_NAME_LENGTH);
        MEMCPY (SecIfInfo.MacAddress, IfInfo.au1MacAddr, CFA_ENET_ADDR_LEN);

        if (OSIX_FAILURE ==
            SecApiUpdateIfInfo (SEC_MOD_CFA_IF_UPDATE, &SecIfInfo))
        {
            SEC_TRC (SEC_FAILURE_TRC, "SecApiIntfUpdtCallBack:"
                     "Failed to update the interface info !!!\r\n");
            return OSIX_FAILURE;
        }

        if (SEC_KERN_USER == gi4SysOperMode)
        {
            if (SecInitInvokeIoctl (SEC_CFA_IF_UPDATE, &SecIfInfo) ==
                OSIX_FAILURE)
            {
                SEC_TRC (SEC_FAILURE_TRC, "SecApiIntfUpdtCallBack:"
                         "ioctl Failed to send the interface info to kernel space !!!\r\n");
                return OSIX_FAILURE;
            }
        }

        SecNotifyHigherLayerIntfStatus (&SecIfInfo, &IpInfo,
                                        pCfaRegInfo->u4IfIndex, u1Action);
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : SecApiUpdateIfInfo                              */
/*                                                                           */
/*    Description         : Utility function to update interface Information.*/
/*                                                                           */
/*    Input(s)            : pIfInfo- Pointer to the tSecIfInfo structure.    */
/*                        : u4Command - Received command r device            */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : OSIX_FAILURE or OSIX_SUCCESS                      */
/*                                                                           */
/*****************************************************************************/

INT4
SecApiUpdateIfInfo (UINT4 u4Command, tSecIfInfo * pSecIfInfo)
{
    INT4                i4Index = 0;
    INT4                i4FreeIndex = SEC_UNALLOCATED;
    INT1                i1MatchFound = OSIX_FALSE;

    switch (u4Command)
    {
        case SEC_MOD_CFA_IF_CREATE:
            if (pSecIfInfo->u1NwType == CFA_NETWORK_TYPE_WAN)
            {
                for (i4Index = 0; i4Index < SYS_MAX_WAN_INTERFACES; i4Index++)
                {
                    if (gaSecWanIfInfo[i4Index].i4IfIndex == SEC_UNALLOCATED)
                    {
                        break;
                    }
                }
                if (i4Index == SYS_MAX_WAN_INTERFACES)
                {
                    SEC_TRC (SEC_FAILURE_TRC, "SecApiUpdateIfInfo:"
                             "Reached the maximum interfaces !!!\r\n");
                    return OSIX_FAILURE;
                }
                MEMCPY (&gaSecWanIfInfo[i4Index], pSecIfInfo,
                        sizeof (tSecIfInfo));
                break;
            }
            else
            {
                for (i4Index = 0; i4Index < SYS_MAX_LAN_INTERFACES; i4Index++)
                {
                    if (gaSecLanIfInfo[i4Index].i4IfIndex == SEC_UNALLOCATED)
                    {
                        break;
                    }
                }
                if (i4Index == SYS_MAX_LAN_INTERFACES)
                {
                    SEC_TRC (SEC_FAILURE_TRC, "SecApiUpdateIfInfo:"
                             "Reached the maximum interfaces !!!\r\n");
                    return OSIX_FAILURE;
                }
                MEMCPY (&gaSecLanIfInfo[i4Index], pSecIfInfo,
                        sizeof (tSecIfInfo));
                break;
            }
            break;

        case SEC_MOD_CFA_IF_DELETE:
            for (i4Index = 0; i4Index < SYS_MAX_WAN_INTERFACES; i4Index++)
            {
                if (gaSecWanIfInfo[i4Index].i4IfIndex == pSecIfInfo->i4IfIndex)
                {
                    MEMSET (&gaSecWanIfInfo[i4Index], 0, sizeof (tSecIfInfo));
                    gaSecWanIfInfo[i4Index].i4IfIndex = SEC_UNALLOCATED;
                    u4Command = SEC_CFA_IF_DELETE;
                    break;
                }
            }
            for (i4Index = 0; i4Index < SYS_MAX_LAN_INTERFACES; i4Index++)
            {
                if (gaSecLanIfInfo[i4Index].i4IfIndex == pSecIfInfo->i4IfIndex)
                {
                    MEMSET (&gaSecLanIfInfo[i4Index], 0, sizeof (tSecIfInfo));
                    gaSecLanIfInfo[i4Index].i4IfIndex = SEC_UNALLOCATED;
                    u4Command = SEC_CFA_IF_DELETE;
                    break;
                }
            }
            break;

        case SEC_MOD_CFA_IF_UPDATE:
            if (pSecIfInfo->u1NwType == CFA_NETWORK_TYPE_WAN)
            {
                for (i4Index = 0; i4Index < SYS_MAX_WAN_INTERFACES; i4Index++)
                {
                    if (gaSecWanIfInfo[i4Index].i4IfIndex ==
                        pSecIfInfo->i4IfIndex)
                    {
                        MEMCPY (&gaSecWanIfInfo[i4Index], pSecIfInfo,
                                sizeof (tSecIfInfo));
                        i1MatchFound = OSIX_TRUE;
                        break;
                    }
                    else if (SEC_UNALLOCATED ==
                             gaSecWanIfInfo[i4Index].i4IfIndex)
                    {
                        if (SEC_UNALLOCATED == i4FreeIndex)
                        {
                            i4FreeIndex = i4Index;
                        }
                    }
                }
                if (OSIX_TRUE == i1MatchFound)
                {
                    MEMCPY (&gaSecWanIfInfo[i4Index], pSecIfInfo,
                            sizeof (tSecIfInfo));
                    break;
                }
                else
                {
                    if (SEC_UNALLOCATED != i4FreeIndex)
                    {
                        MEMCPY (&gaSecWanIfInfo[i4FreeIndex], pSecIfInfo,
                                sizeof (tSecIfInfo));
                        break;
                    }
                }
            }
            else
            {
                for (i4Index = 0; i4Index < SYS_MAX_LAN_INTERFACES; i4Index++)
                {
                    if (gaSecLanIfInfo[i4Index].i4IfIndex ==
                        pSecIfInfo->i4IfIndex)
                    {
                        MEMCPY (&gaSecLanIfInfo[i4Index], pSecIfInfo,
                                sizeof (tSecIfInfo));
                        i1MatchFound = OSIX_TRUE;
                        break;
                    }
                    else if (SEC_UNALLOCATED ==
                             gaSecLanIfInfo[i4Index].i4IfIndex)
                    {
                        if (SEC_UNALLOCATED == i4FreeIndex)
                        {
                            i4FreeIndex = i4Index;
                        }
                    }
                }
                if (OSIX_TRUE == i1MatchFound)
                {
                    MEMCPY (&gaSecLanIfInfo[i4Index], pSecIfInfo,
                            sizeof (tSecIfInfo));
                    break;
                }
                else
                {
                    if (SEC_UNALLOCATED != i4FreeIndex)
                    {
                        MEMCPY (&gaSecLanIfInfo[i4FreeIndex], pSecIfInfo,
                                sizeof (tSecIfInfo));
                        break;
                    }
                }
            }
            break;
        case SEC_MOD_CFA_IF_UPDATE_VLANID:
            for (i4Index = 0; i4Index < SYS_MAX_WAN_INTERFACES; i4Index++)
            {
                if (gaSecWanIfInfo[i4Index].i4IfIndex == pSecIfInfo->i4IfIndex)
                {
                    gaSecWanIfInfo[i4Index].u2VlanId = pSecIfInfo->u2VlanId;
                    break;
                }
            }
            if (pSecIfInfo->u1IfType == CFA_L3IPVLAN)
            {
                if (gaSecLanIfInfo[i4Index].i4IfIndex == pSecIfInfo->i4IfIndex)
                {
                    gaSecLanIfInfo[i4Index].u2VlanId = pSecIfInfo->u2VlanId;
                    break;
                }
            }
            break;

        case SEC_MOD_IP6_ADDR_ADD:
            if (pSecIfInfo->u1NwType == CFA_NETWORK_TYPE_WAN)
            {
                for (i4Index = 0; i4Index < SYS_MAX_WAN_INTERFACES; i4Index++)
                {
                    if (gaSecWanIfInfo[i4Index].i4IfIndex ==
                        pSecIfInfo->i4IfIndex)
                    {
                        MEMCPY (&gaSecWanIfInfo[i4Index].Ip6Addr,
                                &pSecIfInfo->Ip6Addr, sizeof (tIp6Addr));
                        gaSecWanIfInfo[i4Index].u4Ip6PrefixLen =
                            pSecIfInfo->u4Ip6PrefixLen;
                        break;
                    }
                }
            }
            else
            {
                for (i4Index = 0; i4Index < SYS_MAX_LAN_INTERFACES; i4Index++)
                {
                    if (gaSecLanIfInfo[i4Index].i4IfIndex ==
                        pSecIfInfo->i4IfIndex)
                    {
                        MEMCPY (&gaSecLanIfInfo[i4Index].Ip6Addr,
                                &pSecIfInfo->Ip6Addr, sizeof (tIp6Addr));
                        gaSecLanIfInfo[i4Index].u4Ip6PrefixLen =
                            pSecIfInfo->u4Ip6PrefixLen;
                        break;
                    }
                }
            }
            break;
        case SEC_MOD_IP6_ADDR_DELETE:
            if (pSecIfInfo->u1NwType == CFA_NETWORK_TYPE_WAN)
            {
                for (i4Index = 0; i4Index < SYS_MAX_WAN_INTERFACES; i4Index++)
                {
                    if (gaSecWanIfInfo[i4Index].i4IfIndex ==
                        pSecIfInfo->i4IfIndex)
                    {
                        MEMSET (&gaSecWanIfInfo[i4Index].Ip6Addr, 0,
                                sizeof (tIp6Addr));
                        gaSecWanIfInfo[i4Index].u4Ip6PrefixLen = 0;
                        break;
                    }
                }
            }
            else
            {
                for (i4Index = 0; i4Index < SYS_MAX_LAN_INTERFACES; i4Index++)
                {
                    if (gaSecLanIfInfo[i4Index].i4IfIndex ==
                        pSecIfInfo->i4IfIndex)
                    {
                        MEMSET (&gaSecLanIfInfo[i4Index].Ip6Addr, 0,
                                sizeof (tIp6Addr));
                        gaSecLanIfInfo[i4Index].u4Ip6PrefixLen = 0;
                        break;
                    }
                }
            }
            break;

        default:
            SEC_TRC (SEC_FAILURE_TRC, "SecApiUpdateIfInfo:"
                     "Not a Valid Operation !!!\r\n");
            return OSIX_FAILURE;

    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : SecUpdateVlanWanInfo                             */
/*                                                                           */
/*    Description         : Utility function to check if any of the member   */
/*                          ports of the VLAN is of WAN type.                */
/*                                                                           */
/*    Input(s)            :                                                  */
/*                        : VlanId    - L2 VLAN Identifier.                  */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None.                                             */
/*                                                                           */
/*****************************************************************************/
VOID
SecUpdateVlanWanInfo (tVlanId VlanId, tSecIfInfo * pSecIfInfo)
{
    tPortList           EgressPortList;
    tPortList           UntagPortList;
    UINT4               u4PhyIfIndex = 0;
    UINT1               u1NwType = 0;
    UINT1               u1WanType = 0;
    BOOL1               bResult = OSIX_FALSE;

    VlanGetVlanMemberPorts (VlanId, EgressPortList, UntagPortList);

    for (u4PhyIfIndex = 0; u4PhyIfIndex <= BRG_NUM_PHY_PLUS_LOG_PORTS;
         u4PhyIfIndex++)
    {
        OSIX_BITLIST_IS_BIT_SET (EgressPortList, u4PhyIfIndex,
                                 BRG_PORT_LIST_SIZE, bResult);

        if (OSIX_TRUE == bResult)
        {
            CfaGetIfNwType (u4PhyIfIndex, &u1NwType);
            if (u1NwType == CFA_NETWORK_TYPE_WAN)
            {
                CfaGetIfWanType (u4PhyIfIndex, &u1WanType);
                pSecIfInfo->u1NwType = u1NwType;
                pSecIfInfo->u1WanType = u1WanType;
                pSecIfInfo->i4PhyIndex = u4PhyIfIndex;
                return;
            }
        }
    }                            /* End of for */
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : SecApiPPPoEAddNewSession                        */
/*                                                                           */
/*    Description         : API function to add PPP session Information.     */
/*                                                                           */
/*    Input(s)            : u4IfIndex - PPP Index.                           */
/*                        : u2SessionId - PPP session Id.                    */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : OSIX_FAILURE or OSIX_SUCCESS                      */
/*                                                                           */
/*****************************************************************************/

INT4
SecApiPPPoEAddNewSession (UINT4 u4PPPIfIndex, UINT4 u4IfIndex,
                          UINT2 u2SessionId)
{
    tSecPppSessionInfo  SecPppSessionInfo;

    MEMSET (&SecPppSessionInfo, 0, sizeof (tSecPppSessionInfo));

    SecPppSessionInfo.i4PPPIfIndex = u4PPPIfIndex;
    SecPppSessionInfo.i4IfIndex = u4IfIndex;
    SecPppSessionInfo.u2SessionId = u2SessionId;

    if (OSIX_FAILURE == SecUtilPPPoEAddNewSession (&SecPppSessionInfo))
    {
        SEC_TRC (SEC_FAILURE_TRC, "SecApiPPPoEAddNewSession:"
                 "Failed to add PPP session info in user space !!!\r\n");
        return OSIX_FAILURE;
    }

    if (SEC_KERN_USER == gi4SysOperMode)
    {
        if (OSIX_FAILURE ==
            SecInitInvokeIoctl (SEC_CFA_ADD_PPP_SESSION_INFO,
                                &SecPppSessionInfo))
        {
            SEC_TRC (SEC_FAILURE_TRC, "SecApiPPPoEAddNewSession:"
                     "ioctl Failed to send the PPP session info to kernel space !!!\r\n");
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : SecApiPPPoEDelSession                            */
/*                                                                           */
/*    Description         : API function to delete PPP session Information.  */
/*                                                                           */
/*    Input(s)            : u4IfIndex - PPP Index.                           */
/*                        : u2SessionId - PPP session Id.                    */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : OSIX_FAILURE or OSIX_SUCCESS                      */
/*                                                                           */
/*****************************************************************************/

INT4
SecApiPPPoEDelSession (UINT4 u4PPPIfIndex, UINT4 u4IfIndex, UINT2 u2SessionId)
{
    tSecPppSessionInfo  SecPppSessionInfo;

    MEMSET (&SecPppSessionInfo, 0, sizeof (tSecPppSessionInfo));

    SecPppSessionInfo.i4PPPIfIndex = u4PPPIfIndex;
    SecPppSessionInfo.i4IfIndex = u4IfIndex;
    SecPppSessionInfo.u2SessionId = u2SessionId;

    if (OSIX_FAILURE == SecUtilPPPoEDelSession (&SecPppSessionInfo))
    {
        SEC_TRC (SEC_FAILURE_TRC, "SecApiPPPoEDelSession:"
                 "Failed to delete PPP session info in user space !!!\r\n");
        return OSIX_FAILURE;
    }

    if (SEC_KERN_USER == gi4SysOperMode)
    {
        if (OSIX_FAILURE ==
            SecInitInvokeIoctl (SEC_CFA_DEL_PPP_SESSION_INFO,
                                &SecPppSessionInfo))
        {
            SEC_TRC (SEC_FAILURE_TRC, "SecApiPPPoEDelSession:"
                     "ioctl Failed to send the PPP session info to kernel space !!!\r\n");
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : SecApiNotifyIpv6IfStatusChange
 *
 *    DESCRIPTION      : This Callback function is registered with ipv6 
 *                       module(using NetIpv6RegisterHigherLayerProtocol() API)
 *                       to get the notification about the If status change.
 *                       Whenever status of any ip interface changed this
 *                       callback routine is invoked. 
 *                       After getting this notification, security module
 *                       will update its local database. 
 *
 *    INPUT            : tNetIpv6HliParams - If Config Record Structure
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : NONE 
 *
 ****************************************************************************/
PUBLIC VOID
SecApiNotifyIpv6IfStatusChange (tNetIpv6HliParams * pNetIpv6HlParams)
{
    tSecIfInfo          SecIfInfo;
    UINT4               u4Mask = 0;
    UINT4               u4IfIndex = 0;
    UINT4               u4Command = 0;
    UINT4               u4IoctlCmd = 0;
    UINT1               u1NwType = 0;

    MEMSET (&SecIfInfo, 0, sizeof (tSecIfInfo));

    u4Mask = pNetIpv6HlParams->unIpv6HlCmdType.AddrChange.u4Mask;

    if ((IS_ADDR_UNSPECIFIED
         (pNetIpv6HlParams->unIpv6HlCmdType.AddrChange.Ipv6AddrInfo.Ip6Addr)) ||
        (IS_ADDR_LLOCAL
         (pNetIpv6HlParams->unIpv6HlCmdType.AddrChange.Ipv6AddrInfo.Ip6Addr)) ||
        (IS_ADDR_MULTI
         (pNetIpv6HlParams->unIpv6HlCmdType.AddrChange.Ipv6AddrInfo.Ip6Addr)))
    {
        return;
    }

    if (pNetIpv6HlParams->u4Command != NETIPV6_ADDRESS_CHANGE)
    {
        return;
    }

    if ((u4Mask != NETIPV6_ADDRESS_ADD) && (u4Mask != NETIPV6_ADDRESS_DELETE))
    {
        SEC_TRC (ALL_FAILURE_TRC, "SecApiNotifyIpv6IfStatusChange:"
                 "Process only the IP address Addition and Deletion"
                 " Event. Ignoring all other events\r\n");
        /* Process only the IP address Addition And Deletion Notification */
        return;
    }

    if (u4Mask == NETIPV6_ADDRESS_ADD)
    {
        u4Command = SEC_MOD_IP6_ADDR_ADD;
        u4IoctlCmd = SEC_IP6_ADDR_ADD;
    }
    else if (u4Mask == NETIPV6_ADDRESS_DELETE)
    {
        u4Command = SEC_MOD_IP6_ADDR_DELETE;
        u4IoctlCmd = SEC_IP6_ADDR_DEL;
    }

    u4IfIndex = pNetIpv6HlParams->unIpv6HlCmdType.AddrChange.u4Index;

    if (OSIX_FAILURE == SecUtilGetIfNwType (u4IfIndex, &u1NwType))
    {
        SEC_TRC (SEC_CONTROL_PLANE_TRC, "SecApiNotifyIpv6IfStatusChange:"
                 "Invalid Interface Index1 !!!\r\n");
        return;
    }

    SecIfInfo.i4IfIndex = u4IfIndex;
    SecIfInfo.u1NwType = u1NwType;

    MEMCPY (&SecIfInfo.Ip6Addr,
            &pNetIpv6HlParams->unIpv6HlCmdType.AddrChange.Ipv6AddrInfo.Ip6Addr,
            sizeof (tIp6Addr));
    SecIfInfo.u4Ip6PrefixLen =
        pNetIpv6HlParams->unIpv6HlCmdType.AddrChange.Ipv6AddrInfo.
        u4PrefixLength;

    /* Update the gaSecLanIfInfo/gaSecWanIfInfo */
    if (OSIX_FAILURE == SecApiUpdateIfInfo (u4Command, &SecIfInfo))
    {
        SEC_TRC (SEC_FAILURE_TRC, "SecApiNotifyIpv6IfStatusChange:"
                 "Failed to update the Ipv6 addr change in user space !!!\r\n");
        return;
    }

    if (SEC_KERN_USER == gi4SysOperMode)
    {
        if (SecInitInvokeIoctl (u4IoctlCmd, &SecIfInfo) == OSIX_FAILURE)
        {
            SEC_TRC (SEC_FAILURE_TRC, "SecApiNotifyIpv6IfStatusChange:"
                     "ioctl Failed to send the IPv6 addr change to kernel !!!\r\n");
            return;
        }
    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : SecApiSetSecIvrIfIndex                           */
/*                                                                           */
/*    Description         : API function to set the IVR interface index that */
/*                          aids in applying security over bridged packets.  */
/*                                                                           */
/*    Input(s)            : i4SecIvrIfIndex - IVR CFA Index.                 */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*                                                                           */
/*****************************************************************************/
VOID
SecApiSetSecIvrIfIndex (INT4 i4SecIvrIfIndex)
{
    gi4SecIvrIfIndex = i4SecIvrIfIndex;

    if (gi4SysOperMode == SEC_KERN_USER)
    {
        if (SecInitInvokeIoctl (SEC_IVR_UPDT_IOCTL, &i4SecIvrIfIndex) ==
            OSIX_FAILURE)
        {
            SEC_TRC (SEC_FAILURE_TRC, "SecApiSetSecIvrIfIndex:"
                     "ioctl Failed to send the Security IVR index to kernel !!!\r\n");
            return;
        }
    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : SecApiUpdSecBridgingStatus                       */
/*                                                                           */
/*    Description         : API function to set the global variables that    */
/*                          controls whether security should be applied to   */
/*                          bridged packets or not.                          */
/*                                                                           */
/*    Input(s)            : u4SecurityBridgingStatus - Brigding status var.  */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*                                                                           */
/*****************************************************************************/
VOID
SecApiUpdSecBridgingStatus (UINT4 u4SecurityBridgingStatus)
{
    gu4SecBridgingStatus = u4SecurityBridgingStatus;

    if (gi4SysOperMode == SEC_KERN_USER)
    {
        if (SecInitInvokeIoctl (SEC_BRIDGING_STAT_UPDT_IOCTL,
                                &u4SecurityBridgingStatus) == OSIX_FAILURE)
        {
            SEC_TRC (SEC_FAILURE_TRC, "SecApiUpdSecBridgingStatus:"
                     "ioctl Failed to send the security bridging status  to kernel !!!\r\n");
            return;
        }
    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : SecApiSetSecVlanList                             */
/*                                                                           */
/*    Description         : API function to set the L2 VLAN list, which are  */
/*                          eligible for applying security over the packets  */
/*                          received on the VLANs.                           */
/*                                                                           */
/*    Input(s)            : tSecVlanList SecVlanList - List of security VLANs*/
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*                                                                           */
/*****************************************************************************/
VOID
SecApiSetSecVlanList (tSecVlanList SecVlanList)
{
    SEC_ADD_VLAN_LIST (gSecvlanList, SecVlanList);

    if (gi4SysOperMode == SEC_KERN_USER)
    {
        if (SecInitInvokeIoctl (SEC_SET_VLAN_LIST, SecVlanList) == OSIX_FAILURE)
        {
            SEC_TRC (SEC_FAILURE_TRC, "SecApiSetSecVlanList:"
                     "ioctl Failed to send the set of security VLANs to kernel !!!\r\n");
            return;
        }
    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : SecApiResetSecVlanList                           */
/*                                                                           */
/*    Description         : API function to remove few L2 VLANs from the L2  */
/*                          VLANs configured in the security VLAN list.      */
/*                                                                           */
/*    Input(s)            : tSecVlanList SecVlanList - List of security VLANs*/
/*                          to be removed from the security VLAN list in the */
/*                          kernel address space.                            */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*                                                                           */
/*****************************************************************************/
VOID
SecApiResetSecVlanList (tSecVlanList SecVlanList)
{

    SEC_REMOVE_VLAN_LIST (gSecvlanList, SecVlanList);

    if (gi4SysOperMode == SEC_KERN_USER)
    {
        if (SecInitInvokeIoctl (SEC_RESET_VLAN_LIST,
                                SecVlanList) == OSIX_FAILURE)
        {
            SEC_TRC (SEC_FAILURE_TRC, "SecApiReSetSecVlanList:"
                     "ioctl Failed to send the set of security VLANs to kernel !!!\r\n");
            return;
        }
    }
    return;
}

#endif /* _SECAPI_C_ */
