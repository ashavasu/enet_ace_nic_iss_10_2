/*****************************************************************************/
/* Copyright (C) 2011 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2001-2011                                          */
/*                                                                           */
/* $Id: secinit.c,v 1.16 2011/08/24 06:34:14 siva Exp $                       */
/*                                                                           */
/*  FILE NAME             : secinit.c                                        */
/*  PRINCIPAL AUTHOR      : Aricent Inc.                                     */
/*  SUBSYSTEM NAME        : Packet Handler                                   */
/*  MODULE NAME           : Security Module                                  */
/*  LANGUAGE              : C                                                */
/*  TARGET ENVIRONMENT    : Any                                              */
/*  DATE OF FIRST RELEASE : 16 Dec 2010                                      */
/*  AUTHOR                : Aricent Inc.                                     */
/*  DESCRIPTION           : This file contains initialization functions      */
/*  in the user space                                                        */
/*****************************************************************************/

#ifndef _SECUINIT_C_
#define _SECUINIT_C_
#ifndef SECURITY_KERNEL_WANTED
#include "secinc.h"
#include "secextn.h"
#else
#include "seckinc.h"
#endif
#include "sectrc.h"

tSecIfInfo          gaSecWanIfInfo[SYS_MAX_WAN_INTERFACES];
tSecIfInfo          gaSecLanIfInfo[SYS_MAX_LAN_INTERFACES];
UINT1               gau1RxMsgBuf[SEC_MAX_MSG_BUF_SIZE];
UINT1               gau1TxMsgBuf[SEC_MAX_MSG_BUF_SIZE];
UINT4               gu4SecTrcFlag;

INT4                gi4SecDevFd;    /* Interface between ISS and Security Kernel
                                     * Module 
                                     */
INT4                gi4SysOperMode;
/*****************************************************************************
 *
 *    Function Name       : SecInit
 *
 *    Description         : This function is Used to Initialise Security
 *                          Module.  This function blocks on reading data from 
 *                          the security character device.
 *
 *
 *    Input(s)            : None
 *
 *    Output(s)           : NONE.
 *
 *    Global Variables Referred : gi4SecDevFd, gau1RxMsgBuf.                                         
 *                                                                           
 *    Global Variables Modified : None.                                         
 *                                                                           
 *    Use of Recursion          : None.                                         
 *
 *    Output(s)           : None.
 *
 *    Returns             : None
 *****************************************************************************/
VOID
SecInit (VOID)
{
    INT4                i4RetVal = 0;
    if (gi4SysOperMode == SEC_KERN_USER)
    {
        while (1)
        {

            i4RetVal = FileRead (gi4SecDevFd, (CHR1 *) gau1RxMsgBuf,
                                 SEC_MAX_MSG_BUF_SIZE);
            if (i4RetVal > 0)
            {
                SecInitHandleMsgFromKernel (gau1RxMsgBuf, i4RetVal);
            }
        }
    }
    return;
}

/*****************************************************************************
 *
 *    Function Name       : SecInitDevices 
 *
 *    Description         : This function is Used to Initialise Security
 *                          Module in user space. 
 *
 *                          It creates the device arsec  with major number 
 *                          200, loads the ISSSec.ko in the kernel, and 
 *                          calls the ioctl  to create the sec user module
 *                          queue in the kernel.
 *                         
 *                          It invokes the ioctl to initialize the queue in
 *                          the character device to interact with the user 
 *                          space.
 *
 *
 *    Input(s)            : None
 *
 *    Output(s)           : NONE.
 *
 *    Global Variables Referred : None.                                         
 *                                                                           
 *    Global Variables Modified : None.                                         
 *                                                                           
 *    Use of Recursion          : None.                                         
 *
 *    Output(s)           : NONE.
 *
 *    Returns             : OSIX_SUCCESS/OSIX_FAILURE
 *****************************************************************************/

INT4
SecInitDevices (VOID)
{

    /* Queue should be created for User <-> Kernel communication. 
     */

    /* Sleep may be needed so that the kernel module is loaded and
     * the following ioctl will be executed */

    if (SecInitInvokeIoctl (SEC_MOD_INIT_IOCTL, NULL) == OSIX_FAILURE)
    {
        SEC_TRC (SEC_FAILURE_TRC, "SecInitDevices:"
                 "Failed to invoke ioctl to initialize the"
                 "security queue in the kernel !!!\r\n");
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;

}

/*****************************************************************************
 *
 *    Function Name    : SecDeInitDevices 
 *
 *    Description      : This function is Used to unload the ISSSec.ko 
 *                       from the running kernel 
 *
 *
 *    Input(s)         : None
 *
 *    Output(s)        : NONE.
 *
 *    Global Variables Referred : gi4SysOperMode.                                         
 *                                                                           
 *    Global Variables Modified : None.                                         
 *                                                                           
 *    Use of Recursion          : None.                                         
 *
 *    Output(s)           : NONE.
 *
 *    Returns             : OSIX_SUCCESS/OSIX_FAILURE
 *****************************************************************************/
VOID
SecDeInitDevices (VOID)
{
    return;
}

/*****************************************************************************
 *
 *    Function Name       : SecInitHandleMsgFromKernel
 *
 *    Description         : This function handles the packets and messages  
 *                                posted from kernel. 
 *                          (i) SEC_PKT_TO_CFA_FROM_KERNEL - packet posted from 
 *                              kernel space.
 *                          (ii) SEC_LOGMSG_TO_FWL_FROM_KERNEL - Firewall Log message
 *                               posted from kernel space. 
 *                          (iii) SEC_TRAPMSG_TO_FWL_FROM_KERNEL - Firewall Trap message
 *                                posted from kernel space.
 *
 *    Input(s)            : pu1Buf - Pointer to Message.
 *                             u4BufLen - Buffer length.
 *
 *
 * Global Variables Referred : None.                                         
 *                                                                           
 * Global Variables Modified : None.                                         
 *                                                                           
 * Use of Recursion          : None.                                         
 *
 *    Output(s)           : NONE.
 *
 *    Returns             : None
 *****************************************************************************/
VOID
SecInitHandleMsgFromKernel (UINT1 *pu1Buf, UINT4 u4BufLen)
{
    tSecQueMsg         *pSecQueMsg = NULL;

    pSecQueMsg = (tSecQueMsg *) (VOID *) pu1Buf;

    switch (pSecQueMsg->u1MsgType)
    {
            /* Packet from kernel to user space which should be handled by CFA */
        case SEC_PKT_TO_CFA_FROM_KERNEL:
            SecInitPostPktToCfa (pu1Buf, u4BufLen);
            break;
            /* Log Message from kernel to user space which should be handled by Firewall */
        case SEC_LOGMSG_TO_FWL_FROM_KERNEL:
            SecInitHandleFwlLogMessage (pu1Buf, u4BufLen);
            break;
            /* Trap Message from kernel to user space which should be handled by Firewall */
        case SEC_TRAPMSG_TO_FWL_FROM_KERNEL:
            SecInitHandleFwlTrapMsssage (pu1Buf, u4BufLen);
            break;
        case SEC_MSG_TO_IKE_FROM_KERNEL:
            SecInitHandleIkeMsssage (pu1Buf, u4BufLen);
            break;
        case SEC_ERRMSG_TO_FWL_FROM_KERNEL:
            SecInitHandleFwlErrorMsssage (pu1Buf, u4BufLen);
            break;
        case SEC_MSG_TO_SECV4_FROM_KERNEL:
            SecInitHandleSecv4ErrorMsssage (pu1Buf, u4BufLen);
            break;
        default:
            break;
    }
    return;
}

/*****************************************************************************
 *
 *    Function Name       : SecInitPostPktToCfa
 *
 *    Description         : This function is a Used to Post Packet to Cfa
 *                          (i) Enqueues the packet to CFA_PACKET_QUEUE_MUX.
 *                          (ii) Send CFA_GDD_INTERRUPT_EVENT to CFA.
 *
 *    Input(s)            : pu1PktBuf - Pointer to Packet Buffer
 *                          u4PktSize - Packet Length
 *
 *    Output(s)           : None.
 *
 * Global Variables Referred : None.                                         
 *                                                                           
 * Global Variables Modified : None.                                         
 *                                                                           
 * Use of Recursion          : None.                                         
 *
 *    Returns             : None.
 *****************************************************************************/
VOID
SecInitPostPktToCfa (UINT1 *pu1PktBuf, UINT4 u4BufLen)
{
    tCRU_BUF_CHAIN_HEADER *pCruBuf = NULL;
    tSecModuleData     *pSecModuleData = NULL;
    tCfaIfInfo          IfInfo;
    UINT4               u4IfIndex;

    UINT4               u4PktSize = 0;

    pSecModuleData =
        (tSecModuleData *) (VOID *) (pu1PktBuf + sizeof (tSecQueMsg));

    u4PktSize = u4BufLen - (sizeof (tSecModuleData) + sizeof (tSecQueMsg));

    /* allocate CRU buffer */
    pCruBuf =
        (tCRU_BUF_CHAIN_HEADER *) CRU_BUF_Allocate_MsgBufChain (u4PktSize, 0);

    if (pCruBuf == NULL)
    {
        SEC_TRC (SEC_FAILURE_TRC, "SecInitPostPktToCfa:"
                 "Failed to allocate CRU buffer !!!\r\n");
        return;
    }

    /* copy the received packet */
    if (CRU_FAILURE == CRU_BUF_Copy_OverBufChain (pCruBuf,
                                                  ((UINT1 *) (pu1PktBuf +
                                                              sizeof
                                                              (tSecModuleData) +
                                                              sizeof
                                                              (tSecQueMsg))), 0,
                                                  u4PktSize))
    {
        SEC_TRC (SEC_FAILURE_TRC, "SecInitPostPktToCfa:"
                 "Failed to failed to copy the security module data !!!\r\n");
        return;
    }

    /* Copy the module Data */
    MEMCPY (SEC_GET_MODULE_DATA_PTR (pCruBuf), pSecModuleData,
            sizeof (tSecModuleData));
    SEC_SET_COMMAND (pCruBuf, SEC_TO_CFA);
    /* Handover the packet to CFA */
    if (SecUtilGetIfInfo (SEC_GET_IFINDEX (pCruBuf), &IfInfo) == OSIX_SUCCESS)
    {
        if (CFA_L3IPVLAN == IfInfo.u1IfType)
        {
            if (OSIX_SUCCESS ==
                SecUtilGetPhyIndexFromL3VlanIndex (SEC_GET_IFINDEX (pCruBuf),
                                                   &u4IfIndex))
            {
                SEC_SET_IFINDEX (pCruBuf, u4IfIndex);
            }
        }
    }
    CfaHandlePktFromSecKern (pCruBuf);

    return;
}

/*****************************************************************************
 *
 *    Function Name       :  SecInitPostPacketToKernel
 *
 *    Description         : This function does the following. 
 *                          1. If the interface index is invalid, return 
 *                             SECMOD_FAILURE.
 *                          2. If the packet has to be written on a non-WAN 
 *                             interface,return SECMOD_CONTINUE.  
 *                          3. If the packet has to be written on a valid WAN
 *                             interface, 
 *                             (i) Security Message header is 
 *                                 appropriately filled and prepended to 
 *                                 the packet.
 *                             (ii) The packet is written on Security 
 *                                  character device. 
 *                          4. If write fails, return SECMOD_FAILURE.
 *
 *    Input(s)            : pu1PktBuf - Pointer to packet buffer.
 *                          u4IfIdx - Interface Index On which Packet has to 
 *                          be transmitted.
 *
 *    Output(s)           : NONE.
 *
 *    Returns             : SECMOD_CONTINUE - if u4IfIdx is a non-WAN interface
 *                          SECMOD_FAILURE  - if interface is invalid or write 
 *                                            fails. 
 *                          SECMOD_SUCCESS - If write on WAN interface succeeds
 *****************************************************************************/
INT4
SecInitPostPacketToKernel (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIdx,
                           UINT1 u1Direction)
{
    tSecQueMsg         *pSecQueMsg = NULL;
    UINT4               u4RetVal = 0;
    UINT4               u4PktSize = 0;
    UINT4               u4BufSize = 0;

    u4PktSize = CRU_BUF_Get_ChainValidByteCount (pBuf);

    pSecQueMsg = (tSecQueMsg *) (VOID *) gau1TxMsgBuf;
    pSecQueMsg->u1MsgType = SEC_MSG_TYPE_ETH_FRAME;
    pSecQueMsg->ModuleParam.PktInfo.i4InIfIdx = (INT4) u4IfIdx;
    pSecQueMsg->ModuleParam.PktInfo.u2PktLen = (UINT2) u4PktSize;
    pSecQueMsg->ModuleParam.PktInfo.u1Direction = u1Direction;

    /* Prepend the Security header to the packet. */
    if (CRU_FAILURE == CRU_BUF_Copy_FromBufChain (pBuf, (gau1TxMsgBuf +
                                                         sizeof (tSecQueMsg)),
                                                  0, u4PktSize))
    {
        SEC_TRC (SEC_FAILURE_TRC, "SecInitPostPacketToKernel:"
                 "Failed to failed to copy the header from the data !!!\r\n");
        return OSIX_FAILURE;
    }

    u4BufSize = sizeof (tSecQueMsg) + u4PktSize;

    /* Write on security character device (gi4SecDevFd). */
    u4RetVal = FileWrite (gi4SecDevFd, (const CHR1 *) gau1TxMsgBuf, u4BufSize);
    if (u4RetVal == u4BufSize)
    {
        /* Return success if the number of bytes written matches the 
         * number of bytes in the buffer.
         */
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return OSIX_SUCCESS;
    }

    SEC_TRC (SEC_FAILURE_TRC, "SecInitPostPacketToKernel:"
             "Failed to write data on the kernel FD !!!\r\n");
    return OSIX_FAILURE;
}

/*****************************************************************************
 *
 *    Function Name       : SecInitHandleSecv4ErrorMsssage
 *
 *    Description         : This function is Used to hand over the buffer 
 *                          and log information to the firewall.
 *
 *
 *    Input(s)            : pu1Buf - Pointer to the packet buffer.
 *                              u4BufLen - Length of the packet. 
 *
 *    Output(s)           : NONE.
 *
 *    Global Variables Referred : None.                                         
 *                                                                           
 *    Global Variables Modified : None.                                         
 *                                                                           
 *    Use of Recursion          : None.                                         
 *
 *    Output(s)           : NONE.
 *
 *    Returns             : OSIX_SUCCESS/OSIX_FAILURE
 *****************************************************************************/

VOID
SecInitHandleSecv4ErrorMsssage (UINT1 *pu1Buf, UINT4 u4BufLen)
{
    UINT4               u4PktLen = 0;
    UINT4               u4SecHdrLen = 0;
    tSecQueMsg         *pSecQueMsg = NULL;
    tSecv4ErrMsg       *pSecv4ErrMsg = NULL;
    tCRU_BUF_CHAIN_HEADER *pCruBuf = NULL;
    t_ICMP              Icmp;

    MEMSET (&Icmp, 0, sizeof (t_ICMP));
    u4SecHdrLen = sizeof (tSecModuleData) + sizeof (tSecQueMsg);
    u4PktLen = u4BufLen - u4SecHdrLen;

    /* allocate CRU buffer */
    pCruBuf =
        (tCRU_BUF_CHAIN_HEADER *) CRU_BUF_Allocate_MsgBufChain (u4PktLen, 0);
    if (pCruBuf == NULL)
    {
        SEC_TRC (SEC_FAILURE_TRC, "SecInitHandleSecv4ErrorMsssage:"
                 "Failed to allocate CRU buffer !!!\r\n");
        return;
    }

    /* copy the received packet */
    if (pCruBuf != NULL)
    {
        if (CRU_FAILURE == CRU_BUF_Copy_OverBufChain (pCruBuf,
                                                      ((UINT1 *) (pu1Buf +
                                                                  sizeof
                                                                  (tSecModuleData)
                                                                  +
                                                                  sizeof
                                                                  (tSecQueMsg))),
                                                      0, u4PktLen))
        {
            CRU_BUF_Release_MsgBufChain (pCruBuf, FALSE);
            return;
        }

    }

    /* Get the firewall error information from the packet */
    pSecQueMsg = (tSecQueMsg *) (VOID *) pu1Buf;
    pSecv4ErrMsg = &(pSecQueMsg->ModuleParam.Secv4ErrMsg);
    Icmp.u4Mtu = pSecv4ErrMsg->u4Mtu;
    Icmp.i1Type = pSecv4ErrMsg->i1Type;
    Icmp.i1Code = pSecv4ErrMsg->i1Code;
    Icmp.args.u4Unused = 0;

    SecPortSecv4SendIcmpErrMsg (pCruBuf, &Icmp);

    /* Release the buffer once the logging is done */
    CRU_BUF_Release_MsgBufChain (pCruBuf, FALSE);
    return;
}

/*****************************************************************************
 *
 *    Function Name       : SecInitHandleFwlErrorMsssage
 *
 *    Description         : This function is Used to hand over the buffer 
 *                          and log information to the firewall.
 *
 *
 *    Input(s)            : pu1Buf - Pointer to the packet buffer.
 *                              u4BufLen - Length of the packet. 
 *
 *    Output(s)           : NONE.
 *
 *    Global Variables Referred : None.                                         
 *                                                                           
 *    Global Variables Modified : None.                                         
 *                                                                           
 *    Use of Recursion          : None.                                         
 *
 *    Output(s)           : NONE.
 *
 *    Returns             : OSIX_SUCCESS/OSIX_FAILURE
 *****************************************************************************/

VOID
SecInitHandleFwlErrorMsssage (UINT1 *pu1Buf, UINT4 u4BufLen)
{
    UINT4               u4PktLen = 0;
    UINT4               u4IfIndex = 0;
    tSecQueMsg         *pSecQueMsg = NULL;
    tSecModuleData     *pSecModuleData = NULL;
    tFwlErrMsg         *pFwlErrMsg = NULL;
    tCRU_BUF_CHAIN_HEADER *pCruBuf = NULL;

    pSecModuleData = (tSecModuleData *) (VOID *) (pu1Buf + sizeof (tSecQueMsg));

    u4PktLen = sizeof (tSecModuleData) + sizeof (tSecQueMsg);

    /* allocate CRU buffer */
    pCruBuf =
        (tCRU_BUF_CHAIN_HEADER *)
        CRU_BUF_Allocate_MsgBufChain ((u4BufLen - u4PktLen), 0);
    if (pCruBuf == NULL)
    {
        SEC_TRC (SEC_FAILURE_TRC, "SecInitHandleFwlErrorMsssage:"
                 "Failed to allocate CRU buffer !!!\r\n");
        return;
    }

    /* copy the received packet */
    if (pCruBuf != NULL)
    {
        if (CRU_FAILURE == CRU_BUF_Copy_OverBufChain (pCruBuf,
                                                      ((UINT1 *) (pu1Buf +
                                                                  sizeof
                                                                  (tSecModuleData)
                                                                  +
                                                                  sizeof
                                                                  (tSecQueMsg))),
                                                      0, (u4BufLen - u4PktLen)))
        {
            CRU_BUF_Release_MsgBufChain (pCruBuf, FALSE);
            return;
        }

        /* Copy the module Data */
        MEMCPY (SEC_GET_MODULE_DATA_PTR (pCruBuf), pSecModuleData,
                sizeof (tSecModuleData));
    }

    /* Get the firewall error information from the packet */

    pSecQueMsg = (tSecQueMsg *) (VOID *) pu1Buf;
    pFwlErrMsg = &(pSecQueMsg->ModuleParam.FwlErrMsg);
    u4IfIndex = pFwlErrMsg->u4IfIndex;

    SecPortFwlErrorMessageGenerate (pCruBuf, u4IfIndex);

    /* Release the buffer once the logging is done */
    CRU_BUF_Release_MsgBufChain (pCruBuf, FALSE);
    return;
}

/*****************************************************************************
 *
 *    Function Name       : SecInitHandleFwlLogMessage
 *
 *    Description         : This function is Used to hand over the buffer 
 *                          and log information to the firewall.
 *
 *
 *    Input(s)            : pu1Buf - Pointer to the packet buffer.
 *                              u4BufLen - Length of the packet. 
 *
 *    Output(s)           : NONE.
 *
 *    Global Variables Referred : None.                                         
 *                                                                           
 *    Global Variables Modified : None.                                         
 *                                                                           
 *    Use of Recursion          : None.                                         
 *
 *    Output(s)           : NONE.
 *
 *    Returns             : OSIX_SUCCESS/OSIX_FAILURE
 *****************************************************************************/

VOID
SecInitHandleFwlLogMessage (UINT1 *pu1Buf, UINT4 u4BufLen)
{
    tSecQueMsg         *pSecQueMsg = NULL;
    tFwlLogMsg         *pFwlLogMsg = NULL;
    tSecModuleData     *pSecModuleData = NULL;
    tCRU_BUF_CHAIN_HEADER *pCruBuf = NULL;
    UINT4               u4IfaceNum;
    UINT4               u4AttackType;
    UINT4               u4LogLevel;
    UINT4               u4Severity;
    UINT4               u4SecHdrLen = 0;
    UINT4               u4PktLen = 0;
    UINT1               au1Msg[SEC_MAX_LOG_BUF_SIZE];

    pSecModuleData = (tSecModuleData *) (VOID *) (pu1Buf + sizeof (tSecQueMsg));
    pSecQueMsg = (tSecQueMsg *) (VOID *) pu1Buf;

    u4SecHdrLen = sizeof (tSecModuleData) + sizeof (tSecQueMsg);
    u4PktLen = u4BufLen - u4SecHdrLen;

    /* Get the firewall log information from the packet */

    pFwlLogMsg = &(pSecQueMsg->ModuleParam.FwlLogMsg);
    u4AttackType = pFwlLogMsg->u4AttackType;
    u4IfaceNum = pFwlLogMsg->u4IfaceNum;
    u4LogLevel = pFwlLogMsg->u4LogLevel;
    u4Severity = pFwlLogMsg->u4Severity;
    MEMCPY (au1Msg, pFwlLogMsg->au1Msg, SEC_MAX_LOG_BUF_SIZE);

    /* allocate CRU buffer */
    pCruBuf = (tCRU_BUF_CHAIN_HEADER *)
        CRU_BUF_Allocate_MsgBufChain (u4PktLen, 0);

    /*pCruBuf can be NULL (if u4BufLen and u4SecHdrLen values are
       same) when log messages are coming from nmh routines. */
    /* copy the received packet */
    if (pCruBuf != NULL)
    {
        if (CRU_FAILURE == CRU_BUF_Copy_OverBufChain (pCruBuf,
                                                      ((UINT1 *) (pu1Buf +
                                                                  sizeof
                                                                  (tSecModuleData)
                                                                  +
                                                                  sizeof
                                                                  (tSecQueMsg))),
                                                      0, u4PktLen))
        {
            CRU_BUF_Release_MsgBufChain (pCruBuf, FALSE);
            return;
        }

        /* Copy the module Data */
        MEMCPY (SEC_GET_MODULE_DATA_PTR (pCruBuf), pSecModuleData,
                sizeof (tSecModuleData));
    }

    /* Handover the packet and log information to the firewall. */
    SecPortFwlLogMessage (u4AttackType, u4IfaceNum, pCruBuf, u4LogLevel,
                          u4Severity, au1Msg);

    /* Release the buffer once the logging is done */
    CRU_BUF_Release_MsgBufChain (pCruBuf, FALSE);
}

/*****************************************************************************
 *
 *    Function Name       : SecInitHandleFwlTrapMsssage
 *
 *    Description         : This function is Used to post the trap information 
 *                                to the firewall.
 *
 *
 *    Input(s)            : pu1Buf - Pointer to the packet buffer.
 *                              u4BufLen - Length of the packet. 
 *
 *    Output(s)           : NONE.
 *
 *    Global Variables Referred : None.                                         
 *                                                                           
 *    Global Variables Modified : None.                                         
 *                                                                           
 *    Use of Recursion          : None.                                         
 *
 *    Output(s)           : NONE.
 *
 *    Returns             : OSIX_SUCCESS/OSIX_FAILURE
 *****************************************************************************/
VOID
SecInitHandleFwlTrapMsssage (UINT1 *pu1Buf, UINT4 u4BufLen)
{
    tSecQueMsg         *pSecQueMsg = NULL;
    tFwlTrapMsg        *pFwlTrapMsg = NULL;

    UNUSED_PARAM (u4BufLen);
    pSecQueMsg = (tSecQueMsg *) (VOID *) pu1Buf;
    pFwlTrapMsg = &(pSecQueMsg->ModuleParam.FwlTrapMsg);

    switch (pFwlTrapMsg->u4TrapType)
    {
        case FWL_THRESHOLD_EXCEED_TRAP:
            SecPortFwlSndThresholdExceedTrp (pFwlTrapMsg->u4IfaceNum,
                                             pFwlTrapMsg->u4PktCount);
            break;
        case FWL_GEN_MEM_FAILURE_TRAP:
            SecPortFwlGenerateMemFailureTrap (pFwlTrapMsg->u1NodeName);
            break;
        case FWL_GEN_ATTACK_SUM_TRAP:
            SecPortFwlGenerateAttackSumTrap ();
            break;
        case FWL_IDS_ATTACK_PKT_TRAP:
            SecPortIdsSendAttackPktTrap (pFwlTrapMsg->au1BlackListIpAddr);
            break;
        default:
            break;
    }
    return;
}

/*****************************************************************************
 *
 *    Function Name       : SecInitHandleIkeMsssage 
 *
 *    Description         : This function is Used to post the IKE message 
 *                          from IPSEC in kernel to IKE in user space.
 *
 *
 *    Input(s)            : pu1Buf - Pointer to the packet buffer.
 *                              u4BufLen - Length of the packet. 
 *
 *    Output(s)           : NONE.
 *
 *    Global Variables Referred : None.                                         
 *                                                                           
 *    Global Variables Modified : None.                                         
 *                                                                           
 *    Use of Recursion          : None.                                         
 *
 *    Output(s)           : NONE.
 *
 *    Returns             : OSIX_SUCCESS/OSIX_FAILURE
 *****************************************************************************/
VOID
SecInitHandleIkeMsssage (UINT1 *pu1Buf, UINT4 u4BufLen)
{
    tSecQueMsg         *pSecQueMsg = NULL;
    tIkeQMsg           *pIkeMsg = NULL;

    UNUSED_PARAM (u4BufLen);

    pSecQueMsg = (tSecQueMsg *) (VOID *) pu1Buf;
    pIkeMsg = &(pSecQueMsg->ModuleParam.IkeQMsg);
#ifdef IKE_WANTED
    IkeProcessKernelIpsecRequest ((VOID *) pIkeMsg);
#endif
    return;
}

/*****************************************************************************
 *
 *    Function Name       : SecInitRegWithCfa
 *
 *    Description         : This function is Used to Register with Cfa for
 *                          Interface Callback Notifications.
 *
 *    Input(s)            : None
 *
 *    Output(s)           : NONE.
 *
 * Global Variables Referred : None.                                         
 *                                                                           
 * Global Variables Modified : None.                                         
 *                                                                           
 * Use of Recursion          : None.                                         
 *
 *    Output(s)           : NONE.
 *
 *    Returns             : OSIX_SUCCESS/OSIX_FAILURE
 *****************************************************************************/
INT4
SecInitRegWithCfa (VOID)
{
    tCfaRegParams       CfaRegParams;

    MEMSET (&CfaRegParams, 0, sizeof (tCfaRegParams));

    CfaRegParams.u2RegMask = CFA_IF_DELETE | CFA_IF_CREATE |
        CFA_IF_UPDATE | CFA_IF_OPER_ST_CHG;
    CfaRegParams.pIfCreate = SecApiIntfCreateCallBack;
    CfaRegParams.pIfDelete = SecApiIntfDelCallBack;
    CfaRegParams.pIfUpdate = SecApiIntfUpdtCallBack;
    CfaRegParams.pIfOperStChg = SecApiIntfUpdtCallBack;
    CfaRegParams.pIfRcvPkt = NULL;

    if (CfaRegisterHL (&CfaRegParams) != CFA_SUCCESS)
    {
        SEC_TRC (SEC_FAILURE_TRC, "SecInitRegWithCfa:"
                 "Failed to Register with CFA !!!\r\n");
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name       : SecInitDeRegWithCfa
 *
 *    Description         : This function is Used to DeRegister with Cfa for
 *                          Interface Callback Notifications.
 *
 *    Input(s)            : None
 *
 *    Output(s)           : NONE.
 *
 * Global Variables Referred : None.                                         
 *                                                                           
 * Global Variables Modified : None.                                         
 *                                                                           
 * Use of Recursion          : None.                                         
 *
 *    Output(s)           : NONE.
 *
 *    Returns             : OSIX_SUCCESS/OSIX_FAILURE
 *****************************************************************************/
INT4
SecInitDeRegWithCfa (VOID)
{

    CfaDeregisterHL (CFA_ENET_IPV4);
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : SecInitInvokeIoctl                               */
/*                                                                           */
/*    Description         : Utility function to to invoke the ioctl          */
/*                          to send the information to the kernel            */
/*                                                                           */
/*    Input(s)            : u4Command-Command to identify the request        */
/*                          pSecInfo - Information to be passed to kernel    */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : OSIX_FAILURE or OSIX_SUCCESS                      */
/*                                                                           */
/*****************************************************************************/

INT4
SecInitInvokeIoctl (UINT4 u4Command, VOID *pSecInfo)
{
    if (u4Command == SEC_MOD_INIT_IOCTL)
    {
        if (ioctl (gi4SecDevFd, u4Command, SEC_USR_MODULE) < 0)
        {
            SEC_TRC (SEC_FAILURE_TRC, "SecInitInvokeIoctl:"
                     "ioctl Failed to initialize the security"
                     " queue in the kernel !!!\r\n");
            return OSIX_FAILURE;
        }
    }
    else
    {
        if (ioctl (gi4SecDevFd, u4Command, pSecInfo) < 0)
        {
            SEC_TRC (SEC_FAILURE_TRC, "SecInitInvokeIoctl:"
                     "ioctl Failed to send a command to security"
                     " module in kernel !!!\r\n");
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name       : SecInitRegWithIpv6
 *
 *    Description         : This function is Used to Register with netip for
 *                          getting IPv6 Interface Callback Notifications.
 *
 *    Input(s)            : None
 *
 *    Output(s)           : NONE.
 *
 * Global Variables Referred : None.                                         
 *                                                                           
 * Global Variables Modified : None.                                         
 *                                                                           
 * Use of Recursion          : None.                                         
 *
 *    Output(s)           : NONE.
 *
 *    Returns             : OSIX_SUCCESS/OSIX_FAILURE
 *****************************************************************************/
INT4
SecInitRegWithIpv6 (VOID)
{
#ifdef IP6_WANTED
    if (NetIpv6RegisterHigherLayerProtocol (SECURITY_ID,
                                            NETIPV6_ADDRESS_CHANGE,
                                            SecApiNotifyIpv6IfStatusChange)
        == NETIPV6_FAILURE)
    {
        SEC_TRC (SEC_FAILURE_TRC, "SecInitRegWithIp:"
                 "Failed to register with IPv6 Module !!!\r\n");
        return OSIX_FAILURE;
    }
#endif

    return OSIX_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name       : SecInitDeRegWithIpv6
 *
 *    Description         : This function is Used to DeRegister with IP for
 *                          Interface Callback Notifications from netip6
 *
 *    Input(s)            : None
 *
 *    Output(s)           : NONE.
 *
 * Global Variables Referred : None.                                         
 *                                                                           
 * Global Variables Modified : None.                                         
 *                                                                           
 * Use of Recursion          : None.                                         
 *
 *    Output(s)           : NONE.
 *
 *    Returns             : OSIX_SUCCESS/OSIX_FAILURE
 *****************************************************************************/
INT4
SecInitDeRegWithIpv6 (VOID)
{
#ifdef IP6_WANTED
    /* De-register with IPv6 */
    if (NetIpv6DeRegisterHigherLayerProtocol (SECURITY_ID) == NETIPV6_FAILURE)
    {
        SEC_TRC (ALL_FAILURE_TRC, "SecInitDeRegWithIp:"
                 "Failed to de-register with IPv6 module.\r\n");
        return OSIX_FAILURE;
    }
#endif /* for IP6_WANTED */

    return OSIX_SUCCESS;
}
#endif
