
 /** $Id: stdip6lw.h,v 1.4 2011/10/25 09:57:15 siva Exp $*/
/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIpv6Forwarding ARG_LIST((INT4 *));

INT1
nmhGetIpv6DefaultHopLimit ARG_LIST((INT4 *));

INT1
nmhGetIpv6Interfaces ARG_LIST((UINT4 *));

INT1
nmhGetIpv6IfTableLastChange ARG_LIST((UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIpv6Forwarding ARG_LIST((INT4 ));

INT1
nmhSetIpv6DefaultHopLimit ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Ipv6Forwarding ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2Ipv6DefaultHopLimit ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Ipv6Forwarding ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Ipv6DefaultHopLimit ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Ipv6IfTable. */
INT1
nmhValidateIndexInstanceIpv6IfTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for Ipv6IfTable  */

INT1
nmhGetFirstIndexIpv6IfTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIpv6IfTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIpv6IfDescr ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIpv6IfLowerLayer ARG_LIST((INT4 ,tSNMP_OID_TYPE * ));

INT1
nmhGetIpv6IfEffectiveMtu ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIpv6IfReasmMaxSize ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIpv6IfIdentifier ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIpv6IfIdentifierLength ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIpv6IfPhysicalAddress ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIpv6IfAdminStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIpv6IfOperStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIpv6IfLastChange ARG_LIST((INT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIpv6IfDescr ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetIpv6IfIdentifier ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetIpv6IfIdentifierLength ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIpv6IfAdminStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Ipv6IfDescr ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Ipv6IfIdentifier ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Ipv6IfIdentifierLength ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Ipv6IfAdminStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Ipv6IfTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Ipv6IfStatsTable. */
INT1
nmhValidateIndexInstanceIpv6IfStatsTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for Ipv6IfStatsTable  */

INT1
nmhGetFirstIndexIpv6IfStatsTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIpv6IfStatsTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIpv6IfStatsInReceives ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIpv6IfStatsInHdrErrors ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIpv6IfStatsInTooBigErrors ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIpv6IfStatsInNoRoutes ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIpv6IfStatsInAddrErrors ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIpv6IfStatsInUnknownProtos ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIpv6IfStatsInTruncatedPkts ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIpv6IfStatsInDiscards ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIpv6IfStatsInDelivers ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIpv6IfStatsOutForwDatagrams ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIpv6IfStatsOutRequests ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIpv6IfStatsOutDiscards ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIpv6IfStatsOutFragOKs ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIpv6IfStatsOutFragFails ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIpv6IfStatsOutFragCreates ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIpv6IfStatsReasmReqds ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIpv6IfStatsReasmOKs ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIpv6IfStatsReasmFails ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIpv6IfStatsInMcastPkts ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIpv6IfStatsOutMcastPkts ARG_LIST((INT4 ,UINT4 *));

/* Proto Validate Index Instance for Ipv6AddrPrefixTable. */
INT1
nmhValidateIndexInstanceIpv6AddrPrefixTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ));

/* Proto Type for Low Level GET FIRST fn for Ipv6AddrPrefixTable  */

INT1
nmhGetFirstIndexIpv6AddrPrefixTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE *  , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIpv6AddrPrefixTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIpv6AddrPrefixOnLinkFlag ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

INT1
nmhGetIpv6AddrPrefixAutonomousFlag ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

INT1
nmhGetIpv6AddrPrefixAdvPreferredLifetime ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,UINT4 *));

INT1
nmhGetIpv6AddrPrefixAdvValidLifetime ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,UINT4 *));

/* Proto Validate Index Instance for Ipv6AddrTable. */
INT1
nmhValidateIndexInstanceIpv6AddrTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for Ipv6AddrTable  */

INT1
nmhGetFirstIndexIpv6AddrTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIpv6AddrTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIpv6AddrPfxLength ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetIpv6AddrType ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetIpv6AddrAnycastFlag ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetIpv6AddrStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIpv6RouteNumber ARG_LIST((UINT4 *));

INT1
nmhGetIpv6DiscardedRoutes ARG_LIST((UINT4 *));

/* Proto Validate Index Instance for Ipv6RouteTable. */
INT1
nmhValidateIndexInstanceIpv6RouteTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Ipv6RouteTable  */

INT1
nmhGetFirstIndexIpv6RouteTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *  , INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIpv6RouteTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIpv6RouteIfIndex ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , UINT4 ,INT4 *));

INT1
nmhGetIpv6RouteNextHop ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIpv6RouteType ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , UINT4 ,INT4 *));

INT1
nmhGetIpv6RouteProtocol ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , UINT4 ,INT4 *));

INT1
nmhGetIpv6RoutePolicy ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , UINT4 ,INT4 *));

INT1
nmhGetIpv6RouteAge ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , UINT4 ,UINT4 *));

INT1
nmhGetIpv6RouteNextHopRDI ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , UINT4 ,UINT4 *));

INT1
nmhGetIpv6RouteMetric ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , UINT4 ,UINT4 *));

INT1
nmhGetIpv6RouteWeight ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , UINT4 ,UINT4 *));

INT1
nmhGetIpv6RouteInfo ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , UINT4 ,tSNMP_OID_TYPE * ));

INT1
nmhGetIpv6RouteValid ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIpv6RouteValid ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Ipv6RouteValid ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , INT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Ipv6RouteTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Ipv6NetToMediaTable. */
INT1
nmhValidateIndexInstanceIpv6NetToMediaTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for Ipv6NetToMediaTable  */

INT1
nmhGetFirstIndexIpv6NetToMediaTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIpv6NetToMediaTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIpv6NetToMediaPhysAddress ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIpv6NetToMediaType ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetIpv6IfNetToMediaState ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetIpv6IfNetToMediaLastUpdated ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetIpv6NetToMediaValid ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIpv6NetToMediaValid ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Ipv6NetToMediaValid ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Ipv6NetToMediaTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
