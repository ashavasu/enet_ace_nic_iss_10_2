/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmpipv6db.h,v 1.16 2015/06/19 10:29:36 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSMPIPV6DB_H
#define _FSMPIPV6DB_H

UINT1 FsMIIpv6ContextTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMIIpv6IfTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 FsMIIpv6IfStatsTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 FsMIIpv6AddrTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_FIXED_LENGTH_OCTET_STRING ,16 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMIIpv6AddrProfileTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsMIIpv6IcmpStatsTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMIIpv6PmtuTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_FIXED_LENGTH_OCTET_STRING ,16};
UINT1 FsMIIpv6PrefTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER};
UINT1 FsMIIpv6NDProxyListTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_FIXED_LENGTH_OCTET_STRING ,16};
UINT1 FsMIIpv6PingTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMIIpv6AddrSelPolicyTableINDEX [] = {SNMP_FIXED_LENGTH_OCTET_STRING ,16 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER};
UINT1 FsMIIpv6IfScopeZoneMapTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 FsMIIpv6ScopeZoneTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 FsMIIpv6RARouteInfoTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_FIXED_LENGTH_OCTET_STRING ,16 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMIIpv6IfRaRDNSSTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};

UINT4 fsmpipv6 [10] ={1,3,6,1,4,1,29601,2,35};
tSNMP_OID_TYPE fsmpipv6OID = {10, fsmpipv6};


UINT4 FsMIIpv6NdCacheMaxRetries [ ] ={1,3,6,1,4,1,29601,2,35,1,1,1,1};
UINT4 FsMIIpv6PmtuConfigStatus [ ] ={1,3,6,1,4,1,29601,2,35,1,1,1,2};
UINT4 FsMIIpv6PmtuTimeOutInterval [ ] ={1,3,6,1,4,1,29601,2,35,1,1,1,3};
UINT4 FsMIIpv6JumboEnable [ ] ={1,3,6,1,4,1,29601,2,35,1,1,1,4};
UINT4 FsMIIpv6NumOfSendJumbo [ ] ={1,3,6,1,4,1,29601,2,35,1,1,1,5};
UINT4 FsMIIpv6NumOfRecvJumbo [ ] ={1,3,6,1,4,1,29601,2,35,1,1,1,6};
UINT4 FsMIIpv6ErrJumbo [ ] ={1,3,6,1,4,1,29601,2,35,1,1,1,7};
UINT4 FsMIIpv6ContextDebug [ ] ={1,3,6,1,4,1,29601,2,35,1,1,1,8};
UINT4 FsMIIpv6RFC5095Compatibility [ ] ={1,3,6,1,4,1,29601,2,35,1,1,1,9};
UINT4 FsMIIpv6RFC5942Compatibility [ ] ={1,3,6,1,4,1,29601,2,35,1,1,1,10};
UINT4 FsMIIpv6SENDSecLevel [ ] ={1,3,6,1,4,1,29601,2,35,1,1,1,11};
UINT4 FsMIIpv6SENDNbrSecLevel [ ] ={1,3,6,1,4,1,29601,2,35,1,1,1,12};
UINT4 FsMIIpv6SENDAuthType [ ] ={1,3,6,1,4,1,29601,2,35,1,1,1,13};
UINT4 FsMIIpv6SENDMinBits [ ] ={1,3,6,1,4,1,29601,2,35,1,1,1,14};
UINT4 FsMIIpv6SENDSecDAD [ ] ={1,3,6,1,4,1,29601,2,35,1,1,1,15};
UINT4 FsMIIpv6SENDPrefixChk [ ] ={1,3,6,1,4,1,29601,2,35,1,1,1,16};
UINT4 FsMIIpv6IfType [ ] ={1,3,6,1,4,1,29601,2,35,1,2,1,1};
UINT4 FsMIIpv6IfPortNum [ ] ={1,3,6,1,4,1,29601,2,35,1,2,1,2};
UINT4 FsMIIpv6IfCircuitNum [ ] ={1,3,6,1,4,1,29601,2,35,1,2,1,3};
UINT4 FsMIIpv6IfToken [ ] ={1,3,6,1,4,1,29601,2,35,1,2,1,4};
UINT4 FsMIIpv6IfOperStatus [ ] ={1,3,6,1,4,1,29601,2,35,1,2,1,5};
UINT4 FsMIIpv6IfRouterAdvStatus [ ] ={1,3,6,1,4,1,29601,2,35,1,2,1,6};
UINT4 FsMIIpv6IfRouterAdvFlags [ ] ={1,3,6,1,4,1,29601,2,35,1,2,1,7};
UINT4 FsMIIpv6IfHopLimit [ ] ={1,3,6,1,4,1,29601,2,35,1,2,1,8};
UINT4 FsMIIpv6IfDefRouterTime [ ] ={1,3,6,1,4,1,29601,2,35,1,2,1,9};
UINT4 FsMIIpv6IfReachableTime [ ] ={1,3,6,1,4,1,29601,2,35,1,2,1,10};
UINT4 FsMIIpv6IfRetransmitTime [ ] ={1,3,6,1,4,1,29601,2,35,1,2,1,11};
UINT4 FsMIIpv6IfDelayProbeTime [ ] ={1,3,6,1,4,1,29601,2,35,1,2,1,12};
UINT4 FsMIIpv6IfPrefixAdvStatus [ ] ={1,3,6,1,4,1,29601,2,35,1,2,1,13};
UINT4 FsMIIpv6IfMinRouterAdvTime [ ] ={1,3,6,1,4,1,29601,2,35,1,2,1,14};
UINT4 FsMIIpv6IfMaxRouterAdvTime [ ] ={1,3,6,1,4,1,29601,2,35,1,2,1,15};
UINT4 FsMIIpv6IfDADRetries [ ] ={1,3,6,1,4,1,29601,2,35,1,2,1,16};
UINT4 FsMIIpv6IfForwarding [ ] ={1,3,6,1,4,1,29601,2,35,1,2,1,17};
UINT4 FsMIIpv6IfRoutingStatus [ ] ={1,3,6,1,4,1,29601,2,35,1,2,1,18};
UINT4 FsMIIpv6IfIcmpErrInterval [ ] ={1,3,6,1,4,1,29601,2,35,1,2,1,19};
UINT4 FsMIIpv6IfIcmpTokenBucketSize [ ] ={1,3,6,1,4,1,29601,2,35,1,2,1,20};
UINT4 FsMIIpv6IfDestUnreachableMsg [ ] ={1,3,6,1,4,1,29601,2,35,1,2,1,21};
UINT4 FsMIIpv6IfUnnumAssocIPIf [ ] ={1,3,6,1,4,1,29601,2,35,1,2,1,22};
UINT4 FsMIIpv6IfRedirectMsg [ ] ={1,3,6,1,4,1,29601,2,35,1,2,1,23};
UINT4 FsMIIpv6IfAdvSrcLLAdr [ ] ={1,3,6,1,4,1,29601,2,35,1,2,1,24};
UINT4 FsMIIpv6IfAdvIntOpt [ ] ={1,3,6,1,4,1,29601,2,35,1,2,1,25};
UINT4 FsMIIpv6IfNDProxyAdminStatus [ ] ={1,3,6,1,4,1,29601,2,35,1,2,1,26};
UINT4 FsMIIpv6IfNDProxyMode [ ] ={1,3,6,1,4,1,29601,2,35,1,2,1,27};
UINT4 FsMIIpv6IfNDProxyOperStatus [ ] ={1,3,6,1,4,1,29601,2,35,1,2,1,28};
UINT4 FsMIIpv6IfNDProxyUpStream [ ] ={1,3,6,1,4,1,29601,2,35,1,2,1,29};
UINT4 FsMIIpv6IfSENDSecStatus [ ] ={1,3,6,1,4,1,29601,2,35,1,2,1,30};
UINT4 FsMIIpv6IfSENDDeltaTimestamp [ ] ={1,3,6,1,4,1,29601,2,35,1,2,1,31};
UINT4 FsMIIpv6IfSENDFuzzTimestamp [ ] ={1,3,6,1,4,1,29601,2,35,1,2,1,32};
UINT4 FsMIIpv6IfSENDDriftTimestamp [ ] ={1,3,6,1,4,1,29601,2,35,1,2,1,33};
UINT4 FsMIIpv6IfDefRoutePreference [ ] ={1,3,6,1,4,1,29601,2,35,1,2,1,34};
UINT4 FsMIIpv6IfStatsTooBigErrors [ ] ={1,3,6,1,4,1,29601,2,35,1,3,1,1};
UINT4 FsMIIpv6IfStatsInRouterSols [ ] ={1,3,6,1,4,1,29601,2,35,1,3,1,2};
UINT4 FsMIIpv6IfStatsInRouterAdvs [ ] ={1,3,6,1,4,1,29601,2,35,1,3,1,3};
UINT4 FsMIIpv6IfStatsInNeighSols [ ] ={1,3,6,1,4,1,29601,2,35,1,3,1,4};
UINT4 FsMIIpv6IfStatsInNeighAdvs [ ] ={1,3,6,1,4,1,29601,2,35,1,3,1,5};
UINT4 FsMIIpv6IfStatsInRedirects [ ] ={1,3,6,1,4,1,29601,2,35,1,3,1,6};
UINT4 FsMIIpv6IfStatsOutRouterSols [ ] ={1,3,6,1,4,1,29601,2,35,1,3,1,7};
UINT4 FsMIIpv6IfStatsOutRouterAdvs [ ] ={1,3,6,1,4,1,29601,2,35,1,3,1,8};
UINT4 FsMIIpv6IfStatsOutNeighSols [ ] ={1,3,6,1,4,1,29601,2,35,1,3,1,9};
UINT4 FsMIIpv6IfStatsOutNeighAdvs [ ] ={1,3,6,1,4,1,29601,2,35,1,3,1,10};
UINT4 FsMIIpv6IfStatsOutRedirects [ ] ={1,3,6,1,4,1,29601,2,35,1,3,1,11};
UINT4 FsMIIpv6IfStatsLastRouterAdvTime [ ] ={1,3,6,1,4,1,29601,2,35,1,3,1,12};
UINT4 FsMIIpv6IfStatsNextRouterAdvTime [ ] ={1,3,6,1,4,1,29601,2,35,1,3,1,13};
UINT4 FsMIIpv6IfStatsIcmp6ErrRateLmtd [ ] ={1,3,6,1,4,1,29601,2,35,1,3,1,14};
UINT4 FsMIIpv6IfStatsSENDDroppedPkts [ ] ={1,3,6,1,4,1,29601,2,35,1,3,1,15};
UINT4 FsMIIpv6IfStatsSENDInvalidPkts [ ] ={1,3,6,1,4,1,29601,2,35,1,3,1,16};
UINT4 FsMIIpv6AddrAddress [ ] ={1,3,6,1,4,1,29601,2,35,1,4,1,1};
UINT4 FsMIIpv6AddrPrefixLen [ ] ={1,3,6,1,4,1,29601,2,35,1,4,1,2};
UINT4 FsMIIpv6AddrAdminStatus [ ] ={1,3,6,1,4,1,29601,2,35,1,4,1,3};
UINT4 FsMIIpv6AddrType [ ] ={1,3,6,1,4,1,29601,2,35,1,4,1,4};
UINT4 FsMIIpv6AddrProfIndex [ ] ={1,3,6,1,4,1,29601,2,35,1,4,1,5};
UINT4 FsMIIpv6AddrOperStatus [ ] ={1,3,6,1,4,1,29601,2,35,1,4,1,6};
UINT4 FsMIIpv6AddrContextId [ ] ={1,3,6,1,4,1,29601,2,35,1,4,1,7};
UINT4 FsMIIpv6AddrScope [ ] ={1,3,6,1,4,1,29601,2,35,1,4,1,8};
UINT4 FsMIIpv6AddrSENDCgaStatus [ ] ={1,3,6,1,4,1,29601,2,35,1,4,1,9};
UINT4 FsMIIpv6AddrSENDCgaModifier [ ] ={1,3,6,1,4,1,29601,2,35,1,4,1,10};
UINT4 FsMIIpv6AddrSENDCollisionCount [ ] ={1,3,6,1,4,1,29601,2,35,1,4,1,11};
UINT4 FsMIIpv6AddrProfileIndex [ ] ={1,3,6,1,4,1,29601,2,35,1,5,1,1};
UINT4 FsMIIpv6AddrProfileStatus [ ] ={1,3,6,1,4,1,29601,2,35,1,5,1,2};
UINT4 FsMIIpv6AddrProfilePrefixAdvStatus [ ] ={1,3,6,1,4,1,29601,2,35,1,5,1,3};
UINT4 FsMIIpv6AddrProfileOnLinkAdvStatus [ ] ={1,3,6,1,4,1,29601,2,35,1,5,1,4};
UINT4 FsMIIpv6AddrProfileAutoConfAdvStatus [ ] ={1,3,6,1,4,1,29601,2,35,1,5,1,5};
UINT4 FsMIIpv6AddrProfilePreferredTime [ ] ={1,3,6,1,4,1,29601,2,35,1,5,1,6};
UINT4 FsMIIpv6AddrProfileValidTime [ ] ={1,3,6,1,4,1,29601,2,35,1,5,1,7};
UINT4 FsMIIpv6AddrProfileValidLifeTimeFlag [ ] ={1,3,6,1,4,1,29601,2,35,1,5,1,8};
UINT4 FsMIIpv6AddrProfilePreferredLifeTimeFlag [ ] ={1,3,6,1,4,1,29601,2,35,1,5,1,9};
UINT4 FsMIIpv6IcmpInMsgs [ ] ={1,3,6,1,4,1,29601,2,35,1,6,1,1};
UINT4 FsMIIpv6IcmpInErrors [ ] ={1,3,6,1,4,1,29601,2,35,1,6,1,2};
UINT4 FsMIIpv6IcmpInDestUnreachs [ ] ={1,3,6,1,4,1,29601,2,35,1,6,1,3};
UINT4 FsMIIpv6IcmpInTimeExcds [ ] ={1,3,6,1,4,1,29601,2,35,1,6,1,4};
UINT4 FsMIIpv6IcmpInParmProbs [ ] ={1,3,6,1,4,1,29601,2,35,1,6,1,5};
UINT4 FsMIIpv6IcmpInPktTooBigs [ ] ={1,3,6,1,4,1,29601,2,35,1,6,1,6};
UINT4 FsMIIpv6IcmpInEchos [ ] ={1,3,6,1,4,1,29601,2,35,1,6,1,7};
UINT4 FsMIIpv6IcmpInEchoReps [ ] ={1,3,6,1,4,1,29601,2,35,1,6,1,8};
UINT4 FsMIIpv6IcmpInRouterSolicits [ ] ={1,3,6,1,4,1,29601,2,35,1,6,1,9};
UINT4 FsMIIpv6IcmpInRouterAdvertisements [ ] ={1,3,6,1,4,1,29601,2,35,1,6,1,10};
UINT4 FsMIIpv6IcmpInNeighborSolicits [ ] ={1,3,6,1,4,1,29601,2,35,1,6,1,11};
UINT4 FsMIIpv6IcmpInNeighborAdvertisements [ ] ={1,3,6,1,4,1,29601,2,35,1,6,1,12};
UINT4 FsMIIpv6IcmpInRedirects [ ] ={1,3,6,1,4,1,29601,2,35,1,6,1,13};
UINT4 FsMIIpv6IcmpInAdminProhib [ ] ={1,3,6,1,4,1,29601,2,35,1,6,1,14};
UINT4 FsMIIpv6IcmpOutMsgs [ ] ={1,3,6,1,4,1,29601,2,35,1,6,1,15};
UINT4 FsMIIpv6IcmpOutErrors [ ] ={1,3,6,1,4,1,29601,2,35,1,6,1,16};
UINT4 FsMIIpv6IcmpOutDestUnreachs [ ] ={1,3,6,1,4,1,29601,2,35,1,6,1,17};
UINT4 FsMIIpv6IcmpOutTimeExcds [ ] ={1,3,6,1,4,1,29601,2,35,1,6,1,18};
UINT4 FsMIIpv6IcmpOutParmProbs [ ] ={1,3,6,1,4,1,29601,2,35,1,6,1,19};
UINT4 FsMIIpv6IcmpOutPktTooBigs [ ] ={1,3,6,1,4,1,29601,2,35,1,6,1,20};
UINT4 FsMIIpv6IcmpOutEchos [ ] ={1,3,6,1,4,1,29601,2,35,1,6,1,21};
UINT4 FsMIIpv6IcmpOutEchoReps [ ] ={1,3,6,1,4,1,29601,2,35,1,6,1,22};
UINT4 FsMIIpv6IcmpOutRouterSolicits [ ] ={1,3,6,1,4,1,29601,2,35,1,6,1,23};
UINT4 FsMIIpv6IcmpOutRouterAdvertisements [ ] ={1,3,6,1,4,1,29601,2,35,1,6,1,24};
UINT4 FsMIIpv6IcmpOutNeighborSolicits [ ] ={1,3,6,1,4,1,29601,2,35,1,6,1,25};
UINT4 FsMIIpv6IcmpOutNeighborAdvertisements [ ] ={1,3,6,1,4,1,29601,2,35,1,6,1,26};
UINT4 FsMIIpv6IcmpOutRedirects [ ] ={1,3,6,1,4,1,29601,2,35,1,6,1,27};
UINT4 FsMIIpv6IcmpOutAdminProhib [ ] ={1,3,6,1,4,1,29601,2,35,1,6,1,28};
UINT4 FsMIIpv6IcmpInBadCode [ ] ={1,3,6,1,4,1,29601,2,35,1,6,1,29};
UINT4 FsMIIpv6IcmpInNARouterFlagSet [ ] ={1,3,6,1,4,1,29601,2,35,1,6,1,30};
UINT4 FsMIIpv6IcmpInNASolicitedFlagSet [ ] ={1,3,6,1,4,1,29601,2,35,1,6,1,31};
UINT4 FsMIIpv6IcmpInNAOverrideFlagSet [ ] ={1,3,6,1,4,1,29601,2,35,1,6,1,32};
UINT4 FsMIIpv6IcmpOutNARouterFlagSet [ ] ={1,3,6,1,4,1,29601,2,35,1,6,1,33};
UINT4 FsMIIpv6IcmpOutNASolicitedFlagSet [ ] ={1,3,6,1,4,1,29601,2,35,1,6,1,34};
UINT4 FsMIIpv6IcmpOutNAOverrideFlagSet [ ] ={1,3,6,1,4,1,29601,2,35,1,6,1,35};
UINT4 FsMIIpv6PmtuDest [ ] ={1,3,6,1,4,1,29601,2,35,1,7,1,1};
UINT4 FsMIIpv6Pmtu [ ] ={1,3,6,1,4,1,29601,2,35,1,7,1,2};
UINT4 FsMIIpv6PmtuTimeStamp [ ] ={1,3,6,1,4,1,29601,2,35,1,7,1,3};
UINT4 FsMIIpv6PmtuAdminStatus [ ] ={1,3,6,1,4,1,29601,2,35,1,7,1,4};
UINT4 FsMIIpv6Protocol [ ] ={1,3,6,1,4,1,29601,2,35,2,1,1,1};
UINT4 FsMIIpv6Preference [ ] ={1,3,6,1,4,1,29601,2,35,2,1,1,2};
UINT4 FsMIIpv6NDProxyAddr [ ] ={1,3,6,1,4,1,29601,2,35,1,9,1,1};
UINT4 FsMIIpv6NDProxyAdminStatus [ ] ={1,3,6,1,4,1,29601,2,35,1,9,1,2};
UINT4 FsMIIpv6PingIndex [ ] ={1,3,6,1,4,1,29601,2,35,1,10,1,1};
UINT4 FsMIIpv6PingDest [ ] ={1,3,6,1,4,1,29601,2,35,1,10,1,2};
UINT4 FsMIIpv6PingIfIndex [ ] ={1,3,6,1,4,1,29601,2,35,1,10,1,3};
UINT4 FsMIIpv6PingContextId [ ] ={1,3,6,1,4,1,29601,2,35,1,10,1,4};
UINT4 FsMIIpv6PingAdminStatus [ ] ={1,3,6,1,4,1,29601,2,35,1,10,1,5};
UINT4 FsMIIpv6PingInterval [ ] ={1,3,6,1,4,1,29601,2,35,1,10,1,6};
UINT4 FsMIIpv6PingRcvTimeout [ ] ={1,3,6,1,4,1,29601,2,35,1,10,1,7};
UINT4 FsMIIpv6PingTries [ ] ={1,3,6,1,4,1,29601,2,35,1,10,1,8};
UINT4 FsMIIpv6PingSize [ ] ={1,3,6,1,4,1,29601,2,35,1,10,1,9};
UINT4 FsMIIpv6PingSentCount [ ] ={1,3,6,1,4,1,29601,2,35,1,10,1,10};
UINT4 FsMIIpv6PingAverageTime [ ] ={1,3,6,1,4,1,29601,2,35,1,10,1,11};
UINT4 FsMIIpv6PingMaxTime [ ] ={1,3,6,1,4,1,29601,2,35,1,10,1,12};
UINT4 FsMIIpv6PingMinTime [ ] ={1,3,6,1,4,1,29601,2,35,1,10,1,13};
UINT4 FsMIIpv6PingOperStatus [ ] ={1,3,6,1,4,1,29601,2,35,1,10,1,14};
UINT4 FsMIIpv6PingSuccesses [ ] ={1,3,6,1,4,1,29601,2,35,1,10,1,15};
UINT4 FsMIIpv6PingPercentageLoss [ ] ={1,3,6,1,4,1,29601,2,35,1,10,1,16};
UINT4 FsMIIpv6PingData [ ] ={1,3,6,1,4,1,29601,2,35,1,10,1,17};
UINT4 FsMIIpv6PingSrcAddr [ ] ={1,3,6,1,4,1,29601,2,35,1,10,1,18};
UINT4 FsMIIpv6PingZoneId [ ] ={1,3,6,1,4,1,29601,2,35,1,10,1,19};
UINT4 FsMIIpv6PingDestAddrType [ ] ={1,3,6,1,4,1,29601,2,35,1,10,1,20};
UINT4 FsMIIpv6PingHostName [ ] ={1,3,6,1,4,1,29601,2,35,1,10,1,21};
UINT4 FsMIIpv6GlobalDebug [ ] ={1,3,6,1,4,1,29601,2,35,1,11};
UINT4 FsMIIpv6AddrSelPolicyPrefix [ ] ={1,3,6,1,4,1,29601,2,35,1,12,1,1};
UINT4 FsMIIpv6AddrSelPolicyPrefixLen [ ] ={1,3,6,1,4,1,29601,2,35,1,12,1,2};
UINT4 FsMIIpv6AddrSelPolicyIfIndex [ ] ={1,3,6,1,4,1,29601,2,35,1,12,1,3};
UINT4 FsMIIpv6AddrSelPolicyScope [ ] ={1,3,6,1,4,1,29601,2,35,1,12,1,4};
UINT4 FsMIIpv6AddrSelPolicyPrecedence [ ] ={1,3,6,1,4,1,29601,2,35,1,12,1,5};
UINT4 FsMIIpv6AddrSelPolicyLabel [ ] ={1,3,6,1,4,1,29601,2,35,1,12,1,6};
UINT4 FsMIIpv6AddrSelPolicyAddrType [ ] ={1,3,6,1,4,1,29601,2,35,1,12,1,7};
UINT4 FsMIIpv6AddrSelPolicyIsPublicAddr [ ] ={1,3,6,1,4,1,29601,2,35,1,12,1,8};
UINT4 FsMIIpv6AddrSelPolicyIsSelfAddr [ ] ={1,3,6,1,4,1,29601,2,35,1,12,1,9};
UINT4 FsMIIpv6AddrSelPolicyReachabilityStatus [ ] ={1,3,6,1,4,1,29601,2,35,1,12,1,10};
UINT4 FsMIIpv6AddrSelPolicyConfigStatus [ ] ={1,3,6,1,4,1,29601,2,35,1,12,1,11};
UINT4 FsMIIpv6AddrSelPolicyRowStatus [ ] ={1,3,6,1,4,1,29601,2,35,1,12,1,12};
UINT4 FsMIIpv6ScopeZoneIndexIfIndex [ ] ={1,3,6,1,4,1,29601,2,35,1,13,1,1};
UINT4 FsMIIpv6ScopeZoneIndexInterfaceLocal [ ] ={1,3,6,1,4,1,29601,2,35,1,13,1,2};
UINT4 FsMIIpv6ScopeZoneIndexLinkLocal [ ] ={1,3,6,1,4,1,29601,2,35,1,13,1,3};
UINT4 FsMIIpv6ScopeZoneIndex3 [ ] ={1,3,6,1,4,1,29601,2,35,1,13,1,4};
UINT4 FsMIIpv6ScopeZoneIndexAdminLocal [ ] ={1,3,6,1,4,1,29601,2,35,1,13,1,5};
UINT4 FsMIIpv6ScopeZoneIndexSiteLocal [ ] ={1,3,6,1,4,1,29601,2,35,1,13,1,6};
UINT4 FsMIIpv6ScopeZoneIndex6 [ ] ={1,3,6,1,4,1,29601,2,35,1,13,1,7};
UINT4 FsMIIpv6ScopeZoneIndex7 [ ] ={1,3,6,1,4,1,29601,2,35,1,13,1,8};
UINT4 FsMIIpv6ScopeZoneIndexOrganizationLocal [ ] ={1,3,6,1,4,1,29601,2,35,1,13,1,9};
UINT4 FsMIIpv6ScopeZoneIndex9 [ ] ={1,3,6,1,4,1,29601,2,35,1,13,1,10};
UINT4 FsMIIpv6ScopeZoneIndexA [ ] ={1,3,6,1,4,1,29601,2,35,1,13,1,11};
UINT4 FsMIIpv6ScopeZoneIndexB [ ] ={1,3,6,1,4,1,29601,2,35,1,13,1,12};
UINT4 FsMIIpv6ScopeZoneIndexC [ ] ={1,3,6,1,4,1,29601,2,35,1,13,1,13};
UINT4 FsMIIpv6ScopeZoneIndexD [ ] ={1,3,6,1,4,1,29601,2,35,1,13,1,14};
UINT4 FsMIIpv6ScopeZoneIndexE [ ] ={1,3,6,1,4,1,29601,2,35,1,13,1,15};
UINT4 FsMIIpv6IfScopeZoneCreationStatus [ ] ={1,3,6,1,4,1,29601,2,35,1,13,1,16};
UINT4 FsMIIpv6IfScopeZoneRowStatus [ ] ={1,3,6,1,4,1,29601,2,35,1,13,1,17};
UINT4 FsMIIpv6ScopeZoneContextId [ ] ={1,3,6,1,4,1,29601,2,35,1,14,1,1};
UINT4 FsMIIpv6ScopeZoneName [ ] ={1,3,6,1,4,1,29601,2,35,1,14,1,2};
UINT4 FsMIIpv6ScopeZoneIndex [ ] ={1,3,6,1,4,1,29601,2,35,1,14,1,3};
UINT4 FsMIIpv6ScopeZoneCreationStatus [ ] ={1,3,6,1,4,1,29601,2,35,1,14,1,4};
UINT4 FsMIIpv6ScopeZoneInterfaceList [ ] ={1,3,6,1,4,1,29601,2,35,1,14,1,5};
UINT4 FsMIIpv6IsDefaultScopeZone [ ] ={1,3,6,1,4,1,29601,2,35,1,14,1,6};
UINT4 FsMIIpv6RARouteIfIndex [ ] ={1,3,6,1,4,1,29601,2,35,1,15,1,1};
UINT4 FsMIIpv6RARoutePrefix [ ] ={1,3,6,1,4,1,29601,2,35,1,15,1,2};
UINT4 FsMIIpv6RARoutePrefixLen [ ] ={1,3,6,1,4,1,29601,2,35,1,15,1,3};
UINT4 FsMIIpv6RARoutePreference [ ] ={1,3,6,1,4,1,29601,2,35,1,15,1,4};
UINT4 FsMIIpv6RARouteLifetime [ ] ={1,3,6,1,4,1,29601,2,35,1,15,1,5};
UINT4 FsMIIpv6RARouteRowStatus [ ] ={1,3,6,1,4,1,29601,2,35,1,15,1,6};
UINT4 FsMIIpv6TestRedEntryTime [ ] ={1,3,6,1,4,1,29601,2,35,3,1};
UINT4 FsMIIpv6TestRedExitTime [ ] ={1,3,6,1,4,1,29601,2,35,3,2};
UINT4 FsMIIpv6IfRaRDNSSIndex [ ] ={1,3,6,1,4,1,29601,2,35,4,1,1,1};
UINT4 FsMIIpv6IfRadvRDNSSOpen [ ] ={1,3,6,1,4,1,29601,2,35,4,1,1,2};
UINT4 FsMIIpv6IfRaRDNSSPreference [ ] ={1,3,6,1,4,1,29601,2,35,4,1,1,3};
UINT4 FsMIIpv6IfRaRDNSSLifetime [ ] ={1,3,6,1,4,1,29601,2,35,4,1,1,4};
UINT4 FsMIIpv6IfRaRDNSSAddrOne [ ] ={1,3,6,1,4,1,29601,2,35,4,1,1,5};
UINT4 FsMIIpv6IfRaRDNSSAddrTwo [ ] ={1,3,6,1,4,1,29601,2,35,4,1,1,6};
UINT4 FsMIIpv6IfRaRDNSSAddrThree [ ] ={1,3,6,1,4,1,29601,2,35,4,1,1,7};
UINT4 FsMIIpv6IfRaRDNSSRowStatus [ ] ={1,3,6,1,4,1,29601,2,35,4,1,1,8};
UINT4 FsMIIpv6IfDomainNameOne [ ] ={1,3,6,1,4,1,29601,2,35,4,1,1,9};
UINT4 FsMIIpv6IfDomainNameTwo [ ] ={1,3,6,1,4,1,29601,2,35,4,1,1,10};
UINT4 FsMIIpv6IfDomainNameThree [ ] ={1,3,6,1,4,1,29601,2,35,4,1,1,11};
UINT4 FsMIIpv6IfDnsLifeTime [ ] ={1,3,6,1,4,1,29601,2,35,4,1,1,12};
UINT4 FsMIIpv6IfRaRDNSSAddrOneLife [ ] ={1,3,6,1,4,1,29601,2,35,4,1,1,13};
UINT4 FsMIIpv6IfRaRDNSSAddrTwoLife [ ] ={1,3,6,1,4,1,29601,2,35,4,1,1,14};
UINT4 FsMIIpv6IfRaRDNSSAddrThreeLife [ ] ={1,3,6,1,4,1,29601,2,35,4,1,1,15};
UINT4 FsMIIpv6IfDomainNameOneLife [ ] ={1,3,6,1,4,1,29601,2,35,4,1,1,16};
UINT4 FsMIIpv6IfDomainNameTwoLife [ ] ={1,3,6,1,4,1,29601,2,35,4,1,1,17};
UINT4 FsMIIpv6IfDomainNameThreeLife [ ] ={1,3,6,1,4,1,29601,2,35,4,1,1,18};


tMbDbEntry fsmpip6MibEntry[]= {

{{13,FsMIIpv6NdCacheMaxRetries}, GetNextIndexFsMIIpv6ContextTable, FsMIIpv6NdCacheMaxRetriesGet, FsMIIpv6NdCacheMaxRetriesSet, FsMIIpv6NdCacheMaxRetriesTest, FsMIIpv6ContextTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIIpv6ContextTableINDEX, 1, 0, 0, "3"},

{{13,FsMIIpv6PmtuConfigStatus}, GetNextIndexFsMIIpv6ContextTable, FsMIIpv6PmtuConfigStatusGet, FsMIIpv6PmtuConfigStatusSet, FsMIIpv6PmtuConfigStatusTest, FsMIIpv6ContextTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIIpv6ContextTableINDEX, 1, 0, 0, "1"},

{{13,FsMIIpv6PmtuTimeOutInterval}, GetNextIndexFsMIIpv6ContextTable, FsMIIpv6PmtuTimeOutIntervalGet, FsMIIpv6PmtuTimeOutIntervalSet, FsMIIpv6PmtuTimeOutIntervalTest, FsMIIpv6ContextTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIIpv6ContextTableINDEX, 1, 0, 0, "60"},

{{13,FsMIIpv6JumboEnable}, GetNextIndexFsMIIpv6ContextTable, FsMIIpv6JumboEnableGet, FsMIIpv6JumboEnableSet, FsMIIpv6JumboEnableTest, FsMIIpv6ContextTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIIpv6ContextTableINDEX, 1, 0, 0, "1"},

{{13,FsMIIpv6NumOfSendJumbo}, GetNextIndexFsMIIpv6ContextTable, FsMIIpv6NumOfSendJumboGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIIpv6ContextTableINDEX, 1, 0, 0, NULL},

{{13,FsMIIpv6NumOfRecvJumbo}, GetNextIndexFsMIIpv6ContextTable, FsMIIpv6NumOfRecvJumboGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIIpv6ContextTableINDEX, 1, 0, 0, NULL},

{{13,FsMIIpv6ErrJumbo}, GetNextIndexFsMIIpv6ContextTable, FsMIIpv6ErrJumboGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIIpv6ContextTableINDEX, 1, 0, 0, NULL},

{{13,FsMIIpv6ContextDebug}, GetNextIndexFsMIIpv6ContextTable, FsMIIpv6ContextDebugGet, FsMIIpv6ContextDebugSet, FsMIIpv6ContextDebugTest, FsMIIpv6ContextTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIIpv6ContextTableINDEX, 1, 0, 0, NULL},

{{13,FsMIIpv6RFC5095Compatibility}, GetNextIndexFsMIIpv6ContextTable, FsMIIpv6RFC5095CompatibilityGet, FsMIIpv6RFC5095CompatibilitySet, FsMIIpv6RFC5095CompatibilityTest, FsMIIpv6ContextTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIIpv6ContextTableINDEX, 1, 0, 0, NULL},

{{13,FsMIIpv6RFC5942Compatibility}, GetNextIndexFsMIIpv6ContextTable, FsMIIpv6RFC5942CompatibilityGet, FsMIIpv6RFC5942CompatibilitySet, FsMIIpv6RFC5942CompatibilityTest, FsMIIpv6ContextTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIIpv6ContextTableINDEX, 1, 0, 0, "2"},

{{13,FsMIIpv6SENDSecLevel}, GetNextIndexFsMIIpv6ContextTable, FsMIIpv6SENDSecLevelGet, FsMIIpv6SENDSecLevelSet, FsMIIpv6SENDSecLevelTest, FsMIIpv6ContextTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIIpv6ContextTableINDEX, 1, 0, 0, NULL},

{{13,FsMIIpv6SENDNbrSecLevel}, GetNextIndexFsMIIpv6ContextTable, FsMIIpv6SENDNbrSecLevelGet, FsMIIpv6SENDNbrSecLevelSet, FsMIIpv6SENDNbrSecLevelTest, FsMIIpv6ContextTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIIpv6ContextTableINDEX, 1, 0, 0, NULL},

{{13,FsMIIpv6SENDAuthType}, GetNextIndexFsMIIpv6ContextTable, FsMIIpv6SENDAuthTypeGet, FsMIIpv6SENDAuthTypeSet, FsMIIpv6SENDAuthTypeTest, FsMIIpv6ContextTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIIpv6ContextTableINDEX, 1, 0, 0, NULL},

{{13,FsMIIpv6SENDMinBits}, GetNextIndexFsMIIpv6ContextTable, FsMIIpv6SENDMinBitsGet, FsMIIpv6SENDMinBitsSet, FsMIIpv6SENDMinBitsTest, FsMIIpv6ContextTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIIpv6ContextTableINDEX, 1, 0, 0, "1024"},

{{13,FsMIIpv6SENDSecDAD}, GetNextIndexFsMIIpv6ContextTable, FsMIIpv6SENDSecDADGet, FsMIIpv6SENDSecDADSet, FsMIIpv6SENDSecDADTest, FsMIIpv6ContextTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIIpv6ContextTableINDEX, 1, 0, 0, "2"},

{{13,FsMIIpv6SENDPrefixChk}, GetNextIndexFsMIIpv6ContextTable, FsMIIpv6SENDPrefixChkGet, FsMIIpv6SENDPrefixChkSet, FsMIIpv6SENDPrefixChkTest, FsMIIpv6ContextTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIIpv6ContextTableINDEX, 1, 0, 0, "2"},

{{13,FsMIIpv6IfType}, GetNextIndexFsMIIpv6IfTable, FsMIIpv6IfTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIIpv6IfTableINDEX, 1, 0, 0, NULL},

{{13,FsMIIpv6IfPortNum}, GetNextIndexFsMIIpv6IfTable, FsMIIpv6IfPortNumGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIIpv6IfTableINDEX, 1, 0, 0, NULL},

{{13,FsMIIpv6IfCircuitNum}, GetNextIndexFsMIIpv6IfTable, FsMIIpv6IfCircuitNumGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIIpv6IfTableINDEX, 1, 0, 0, NULL},

{{13,FsMIIpv6IfToken}, GetNextIndexFsMIIpv6IfTable, FsMIIpv6IfTokenGet, FsMIIpv6IfTokenSet, FsMIIpv6IfTokenTest, FsMIIpv6IfTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIIpv6IfTableINDEX, 1, 0, 0, NULL},

{{13,FsMIIpv6IfOperStatus}, GetNextIndexFsMIIpv6IfTable, FsMIIpv6IfOperStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIIpv6IfTableINDEX, 1, 0, 0, NULL},

{{13,FsMIIpv6IfRouterAdvStatus}, GetNextIndexFsMIIpv6IfTable, FsMIIpv6IfRouterAdvStatusGet, FsMIIpv6IfRouterAdvStatusSet, FsMIIpv6IfRouterAdvStatusTest, FsMIIpv6IfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIIpv6IfTableINDEX, 1, 0, 0, "2"},

{{13,FsMIIpv6IfRouterAdvFlags}, GetNextIndexFsMIIpv6IfTable, FsMIIpv6IfRouterAdvFlagsGet, FsMIIpv6IfRouterAdvFlagsSet, FsMIIpv6IfRouterAdvFlagsTest, FsMIIpv6IfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIIpv6IfTableINDEX, 1, 0, 0, "6"},

{{13,FsMIIpv6IfHopLimit}, GetNextIndexFsMIIpv6IfTable, FsMIIpv6IfHopLimitGet, FsMIIpv6IfHopLimitSet, FsMIIpv6IfHopLimitTest, FsMIIpv6IfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIIpv6IfTableINDEX, 1, 0, 0, "64"},

{{13,FsMIIpv6IfDefRouterTime}, GetNextIndexFsMIIpv6IfTable, FsMIIpv6IfDefRouterTimeGet, FsMIIpv6IfDefRouterTimeSet, FsMIIpv6IfDefRouterTimeTest, FsMIIpv6IfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIIpv6IfTableINDEX, 1, 0, 0, "0"},

{{13,FsMIIpv6IfReachableTime}, GetNextIndexFsMIIpv6IfTable, FsMIIpv6IfReachableTimeGet, FsMIIpv6IfReachableTimeSet, FsMIIpv6IfReachableTimeTest, FsMIIpv6IfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIIpv6IfTableINDEX, 1, 0, 0, "30"},

{{13,FsMIIpv6IfRetransmitTime}, GetNextIndexFsMIIpv6IfTable, FsMIIpv6IfRetransmitTimeGet, FsMIIpv6IfRetransmitTimeSet, FsMIIpv6IfRetransmitTimeTest, FsMIIpv6IfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIIpv6IfTableINDEX, 1, 0, 0, "1"},

{{13,FsMIIpv6IfDelayProbeTime}, GetNextIndexFsMIIpv6IfTable, FsMIIpv6IfDelayProbeTimeGet, FsMIIpv6IfDelayProbeTimeSet, FsMIIpv6IfDelayProbeTimeTest, FsMIIpv6IfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIIpv6IfTableINDEX, 1, 0, 0, "5"},

{{13,FsMIIpv6IfPrefixAdvStatus}, GetNextIndexFsMIIpv6IfTable, FsMIIpv6IfPrefixAdvStatusGet, FsMIIpv6IfPrefixAdvStatusSet, FsMIIpv6IfPrefixAdvStatusTest, FsMIIpv6IfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIIpv6IfTableINDEX, 1, 0, 0, "1"},

{{13,FsMIIpv6IfMinRouterAdvTime}, GetNextIndexFsMIIpv6IfTable, FsMIIpv6IfMinRouterAdvTimeGet, FsMIIpv6IfMinRouterAdvTimeSet, FsMIIpv6IfMinRouterAdvTimeTest, FsMIIpv6IfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIIpv6IfTableINDEX, 1, 0, 0, "198"},

{{13,FsMIIpv6IfMaxRouterAdvTime}, GetNextIndexFsMIIpv6IfTable, FsMIIpv6IfMaxRouterAdvTimeGet, FsMIIpv6IfMaxRouterAdvTimeSet, FsMIIpv6IfMaxRouterAdvTimeTest, FsMIIpv6IfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIIpv6IfTableINDEX, 1, 0, 0, "600"},

{{13,FsMIIpv6IfDADRetries}, GetNextIndexFsMIIpv6IfTable, FsMIIpv6IfDADRetriesGet, FsMIIpv6IfDADRetriesSet, FsMIIpv6IfDADRetriesTest, FsMIIpv6IfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIIpv6IfTableINDEX, 1, 0, 0, "1"},

{{13,FsMIIpv6IfForwarding}, GetNextIndexFsMIIpv6IfTable, FsMIIpv6IfForwardingGet, FsMIIpv6IfForwardingSet, FsMIIpv6IfForwardingTest, FsMIIpv6IfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIIpv6IfTableINDEX, 1, 0, 0, "1"},

{{13,FsMIIpv6IfRoutingStatus}, GetNextIndexFsMIIpv6IfTable, FsMIIpv6IfRoutingStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIIpv6IfTableINDEX, 1, 0, 0, NULL},

{{13,FsMIIpv6IfIcmpErrInterval}, GetNextIndexFsMIIpv6IfTable, FsMIIpv6IfIcmpErrIntervalGet, FsMIIpv6IfIcmpErrIntervalSet, FsMIIpv6IfIcmpErrIntervalTest, FsMIIpv6IfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIIpv6IfTableINDEX, 1, 0, 0, "100"},

{{13,FsMIIpv6IfIcmpTokenBucketSize}, GetNextIndexFsMIIpv6IfTable, FsMIIpv6IfIcmpTokenBucketSizeGet, FsMIIpv6IfIcmpTokenBucketSizeSet, FsMIIpv6IfIcmpTokenBucketSizeTest, FsMIIpv6IfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIIpv6IfTableINDEX, 1, 0, 0, "10"},

{{13,FsMIIpv6IfDestUnreachableMsg}, GetNextIndexFsMIIpv6IfTable, FsMIIpv6IfDestUnreachableMsgGet, FsMIIpv6IfDestUnreachableMsgSet, FsMIIpv6IfDestUnreachableMsgTest, FsMIIpv6IfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIIpv6IfTableINDEX, 1, 0, 0, "1"},

{{13,FsMIIpv6IfUnnumAssocIPIf}, GetNextIndexFsMIIpv6IfTable, FsMIIpv6IfUnnumAssocIPIfGet, FsMIIpv6IfUnnumAssocIPIfSet, FsMIIpv6IfUnnumAssocIPIfTest, FsMIIpv6IfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIIpv6IfTableINDEX, 1, 0, 0, NULL},

{{13,FsMIIpv6IfRedirectMsg}, GetNextIndexFsMIIpv6IfTable, FsMIIpv6IfRedirectMsgGet, FsMIIpv6IfRedirectMsgSet, FsMIIpv6IfRedirectMsgTest, FsMIIpv6IfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIIpv6IfTableINDEX, 1, 0, 0, "2"},

{{13,FsMIIpv6IfAdvSrcLLAdr}, GetNextIndexFsMIIpv6IfTable, FsMIIpv6IfAdvSrcLLAdrGet, FsMIIpv6IfAdvSrcLLAdrSet, FsMIIpv6IfAdvSrcLLAdrTest, FsMIIpv6IfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIIpv6IfTableINDEX, 1, 0, 0, "2"},

{{13,FsMIIpv6IfAdvIntOpt}, GetNextIndexFsMIIpv6IfTable, FsMIIpv6IfAdvIntOptGet, FsMIIpv6IfAdvIntOptSet, FsMIIpv6IfAdvIntOptTest, FsMIIpv6IfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIIpv6IfTableINDEX, 1, 0, 0, "2"},

{{13,FsMIIpv6IfNDProxyAdminStatus}, GetNextIndexFsMIIpv6IfTable, FsMIIpv6IfNDProxyAdminStatusGet, FsMIIpv6IfNDProxyAdminStatusSet, FsMIIpv6IfNDProxyAdminStatusTest, FsMIIpv6IfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIIpv6IfTableINDEX, 1, 0, 0, "2"},

{{13,FsMIIpv6IfNDProxyMode}, GetNextIndexFsMIIpv6IfTable, FsMIIpv6IfNDProxyModeGet, FsMIIpv6IfNDProxyModeSet, FsMIIpv6IfNDProxyModeTest, FsMIIpv6IfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIIpv6IfTableINDEX, 1, 0, 0, "1"},

{{13,FsMIIpv6IfNDProxyOperStatus}, GetNextIndexFsMIIpv6IfTable, FsMIIpv6IfNDProxyOperStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIIpv6IfTableINDEX, 1, 0, 0, "2"},

{{13,FsMIIpv6IfNDProxyUpStream}, GetNextIndexFsMIIpv6IfTable, FsMIIpv6IfNDProxyUpStreamGet, FsMIIpv6IfNDProxyUpStreamSet, FsMIIpv6IfNDProxyUpStreamTest, FsMIIpv6IfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIIpv6IfTableINDEX, 1, 0, 0, "2"},

{{13,FsMIIpv6IfSENDSecStatus}, GetNextIndexFsMIIpv6IfTable, FsMIIpv6IfSENDSecStatusGet, FsMIIpv6IfSENDSecStatusSet, FsMIIpv6IfSENDSecStatusTest, FsMIIpv6IfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIIpv6IfTableINDEX, 1, 0, 0, "2"},

{{13,FsMIIpv6IfSENDDeltaTimestamp}, GetNextIndexFsMIIpv6IfTable, FsMIIpv6IfSENDDeltaTimestampGet, FsMIIpv6IfSENDDeltaTimestampSet, FsMIIpv6IfSENDDeltaTimestampTest, FsMIIpv6IfTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIIpv6IfTableINDEX, 1, 0, 0, "300"},

{{13,FsMIIpv6IfSENDFuzzTimestamp}, GetNextIndexFsMIIpv6IfTable, FsMIIpv6IfSENDFuzzTimestampGet, FsMIIpv6IfSENDFuzzTimestampSet, FsMIIpv6IfSENDFuzzTimestampTest, FsMIIpv6IfTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIIpv6IfTableINDEX, 1, 0, 0, "1"},

{{13,FsMIIpv6IfSENDDriftTimestamp}, GetNextIndexFsMIIpv6IfTable, FsMIIpv6IfSENDDriftTimestampGet, FsMIIpv6IfSENDDriftTimestampSet, FsMIIpv6IfSENDDriftTimestampTest, FsMIIpv6IfTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIIpv6IfTableINDEX, 1, 0, 0, "1"},

{{13,FsMIIpv6IfDefRoutePreference}, GetNextIndexFsMIIpv6IfTable, FsMIIpv6IfDefRoutePreferenceGet, FsMIIpv6IfDefRoutePreferenceSet, FsMIIpv6IfDefRoutePreferenceTest, FsMIIpv6IfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIIpv6IfTableINDEX, 1, 0, 0, "1"},

{{13,FsMIIpv6IfStatsTooBigErrors}, GetNextIndexFsMIIpv6IfStatsTable, FsMIIpv6IfStatsTooBigErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIIpv6IfStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsMIIpv6IfStatsInRouterSols}, GetNextIndexFsMIIpv6IfStatsTable, FsMIIpv6IfStatsInRouterSolsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIIpv6IfStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsMIIpv6IfStatsInRouterAdvs}, GetNextIndexFsMIIpv6IfStatsTable, FsMIIpv6IfStatsInRouterAdvsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIIpv6IfStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsMIIpv6IfStatsInNeighSols}, GetNextIndexFsMIIpv6IfStatsTable, FsMIIpv6IfStatsInNeighSolsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIIpv6IfStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsMIIpv6IfStatsInNeighAdvs}, GetNextIndexFsMIIpv6IfStatsTable, FsMIIpv6IfStatsInNeighAdvsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIIpv6IfStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsMIIpv6IfStatsInRedirects}, GetNextIndexFsMIIpv6IfStatsTable, FsMIIpv6IfStatsInRedirectsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIIpv6IfStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsMIIpv6IfStatsOutRouterSols}, GetNextIndexFsMIIpv6IfStatsTable, FsMIIpv6IfStatsOutRouterSolsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIIpv6IfStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsMIIpv6IfStatsOutRouterAdvs}, GetNextIndexFsMIIpv6IfStatsTable, FsMIIpv6IfStatsOutRouterAdvsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIIpv6IfStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsMIIpv6IfStatsOutNeighSols}, GetNextIndexFsMIIpv6IfStatsTable, FsMIIpv6IfStatsOutNeighSolsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIIpv6IfStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsMIIpv6IfStatsOutNeighAdvs}, GetNextIndexFsMIIpv6IfStatsTable, FsMIIpv6IfStatsOutNeighAdvsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIIpv6IfStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsMIIpv6IfStatsOutRedirects}, GetNextIndexFsMIIpv6IfStatsTable, FsMIIpv6IfStatsOutRedirectsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIIpv6IfStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsMIIpv6IfStatsLastRouterAdvTime}, GetNextIndexFsMIIpv6IfStatsTable, FsMIIpv6IfStatsLastRouterAdvTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsMIIpv6IfStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsMIIpv6IfStatsNextRouterAdvTime}, GetNextIndexFsMIIpv6IfStatsTable, FsMIIpv6IfStatsNextRouterAdvTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsMIIpv6IfStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsMIIpv6IfStatsIcmp6ErrRateLmtd}, GetNextIndexFsMIIpv6IfStatsTable, FsMIIpv6IfStatsIcmp6ErrRateLmtdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIIpv6IfStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsMIIpv6IfStatsSENDDroppedPkts}, GetNextIndexFsMIIpv6IfStatsTable, FsMIIpv6IfStatsSENDDroppedPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIIpv6IfStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsMIIpv6IfStatsSENDInvalidPkts}, GetNextIndexFsMIIpv6IfStatsTable, FsMIIpv6IfStatsSENDInvalidPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIIpv6IfStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsMIIpv6AddrAddress}, GetNextIndexFsMIIpv6AddrTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsMIIpv6AddrTableINDEX, 3, 0, 0, NULL},

{{13,FsMIIpv6AddrPrefixLen}, GetNextIndexFsMIIpv6AddrTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIIpv6AddrTableINDEX, 3, 0, 0, NULL},

{{13,FsMIIpv6AddrAdminStatus}, GetNextIndexFsMIIpv6AddrTable, FsMIIpv6AddrAdminStatusGet, FsMIIpv6AddrAdminStatusSet, FsMIIpv6AddrAdminStatusTest, FsMIIpv6AddrTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIIpv6AddrTableINDEX, 3, 0, 1, NULL},

{{13,FsMIIpv6AddrType}, GetNextIndexFsMIIpv6AddrTable, FsMIIpv6AddrTypeGet, FsMIIpv6AddrTypeSet, FsMIIpv6AddrTypeTest, FsMIIpv6AddrTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIIpv6AddrTableINDEX, 3, 0, 0, NULL},

{{13,FsMIIpv6AddrProfIndex}, GetNextIndexFsMIIpv6AddrTable, FsMIIpv6AddrProfIndexGet, FsMIIpv6AddrProfIndexSet, FsMIIpv6AddrProfIndexTest, FsMIIpv6AddrTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIIpv6AddrTableINDEX, 3, 0, 0, "0"},

{{13,FsMIIpv6AddrOperStatus}, GetNextIndexFsMIIpv6AddrTable, FsMIIpv6AddrOperStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIIpv6AddrTableINDEX, 3, 0, 0, NULL},

{{13,FsMIIpv6AddrContextId}, GetNextIndexFsMIIpv6AddrTable, FsMIIpv6AddrContextIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIIpv6AddrTableINDEX, 3, 0, 0, NULL},

{{13,FsMIIpv6AddrScope}, GetNextIndexFsMIIpv6AddrTable, FsMIIpv6AddrScopeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIIpv6AddrTableINDEX, 3, 0, 0, NULL},

{{13,FsMIIpv6AddrSENDCgaStatus}, GetNextIndexFsMIIpv6AddrTable, FsMIIpv6AddrSENDCgaStatusGet, FsMIIpv6AddrSENDCgaStatusSet, FsMIIpv6AddrSENDCgaStatusTest, FsMIIpv6AddrTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIIpv6AddrTableINDEX, 3, 0, 0, "2"},

{{13,FsMIIpv6AddrSENDCgaModifier}, GetNextIndexFsMIIpv6AddrTable, FsMIIpv6AddrSENDCgaModifierGet, FsMIIpv6AddrSENDCgaModifierSet, FsMIIpv6AddrSENDCgaModifierTest, FsMIIpv6AddrTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIIpv6AddrTableINDEX, 3, 0, 0, NULL},

{{13,FsMIIpv6AddrSENDCollisionCount}, GetNextIndexFsMIIpv6AddrTable, FsMIIpv6AddrSENDCollisionCountGet, FsMIIpv6AddrSENDCollisionCountSet, FsMIIpv6AddrSENDCollisionCountTest, FsMIIpv6AddrTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIIpv6AddrTableINDEX, 3, 0, 0, NULL},

{{13,FsMIIpv6AddrProfileIndex}, GetNextIndexFsMIIpv6AddrProfileTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsMIIpv6AddrProfileTableINDEX, 1, 0, 0, NULL},

{{13,FsMIIpv6AddrProfileStatus}, GetNextIndexFsMIIpv6AddrProfileTable, FsMIIpv6AddrProfileStatusGet, FsMIIpv6AddrProfileStatusSet, FsMIIpv6AddrProfileStatusTest, FsMIIpv6AddrProfileTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIIpv6AddrProfileTableINDEX, 1, 0, 0, NULL},

{{13,FsMIIpv6AddrProfilePrefixAdvStatus}, GetNextIndexFsMIIpv6AddrProfileTable, FsMIIpv6AddrProfilePrefixAdvStatusGet, FsMIIpv6AddrProfilePrefixAdvStatusSet, FsMIIpv6AddrProfilePrefixAdvStatusTest, FsMIIpv6AddrProfileTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIIpv6AddrProfileTableINDEX, 1, 0, 0, "1"},

{{13,FsMIIpv6AddrProfileOnLinkAdvStatus}, GetNextIndexFsMIIpv6AddrProfileTable, FsMIIpv6AddrProfileOnLinkAdvStatusGet, FsMIIpv6AddrProfileOnLinkAdvStatusSet, FsMIIpv6AddrProfileOnLinkAdvStatusTest, FsMIIpv6AddrProfileTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIIpv6AddrProfileTableINDEX, 1, 0, 0, "1"},

{{13,FsMIIpv6AddrProfileAutoConfAdvStatus}, GetNextIndexFsMIIpv6AddrProfileTable, FsMIIpv6AddrProfileAutoConfAdvStatusGet, FsMIIpv6AddrProfileAutoConfAdvStatusSet, FsMIIpv6AddrProfileAutoConfAdvStatusTest, FsMIIpv6AddrProfileTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIIpv6AddrProfileTableINDEX, 1, 0, 0, "1"},

{{13,FsMIIpv6AddrProfilePreferredTime}, GetNextIndexFsMIIpv6AddrProfileTable, FsMIIpv6AddrProfilePreferredTimeGet, FsMIIpv6AddrProfilePreferredTimeSet, FsMIIpv6AddrProfilePreferredTimeTest, FsMIIpv6AddrProfileTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIIpv6AddrProfileTableINDEX, 1, 0, 0, "604800"},

{{13,FsMIIpv6AddrProfileValidTime}, GetNextIndexFsMIIpv6AddrProfileTable, FsMIIpv6AddrProfileValidTimeGet, FsMIIpv6AddrProfileValidTimeSet, FsMIIpv6AddrProfileValidTimeTest, FsMIIpv6AddrProfileTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIIpv6AddrProfileTableINDEX, 1, 0, 0, "2147483647"},

{{13,FsMIIpv6AddrProfileValidLifeTimeFlag}, GetNextIndexFsMIIpv6AddrProfileTable, FsMIIpv6AddrProfileValidLifeTimeFlagGet, FsMIIpv6AddrProfileValidLifeTimeFlagSet, FsMIIpv6AddrProfileValidLifeTimeFlagTest, FsMIIpv6AddrProfileTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIIpv6AddrProfileTableINDEX, 1, 0, 0, "1"},

{{13,FsMIIpv6AddrProfilePreferredLifeTimeFlag}, GetNextIndexFsMIIpv6AddrProfileTable, FsMIIpv6AddrProfilePreferredLifeTimeFlagGet, FsMIIpv6AddrProfilePreferredLifeTimeFlagSet, FsMIIpv6AddrProfilePreferredLifeTimeFlagTest, FsMIIpv6AddrProfileTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIIpv6AddrProfileTableINDEX, 1, 0, 0, "1"},

{{13,FsMIIpv6IcmpInMsgs}, GetNextIndexFsMIIpv6IcmpStatsTable, FsMIIpv6IcmpInMsgsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIIpv6IcmpStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsMIIpv6IcmpInErrors}, GetNextIndexFsMIIpv6IcmpStatsTable, FsMIIpv6IcmpInErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIIpv6IcmpStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsMIIpv6IcmpInDestUnreachs}, GetNextIndexFsMIIpv6IcmpStatsTable, FsMIIpv6IcmpInDestUnreachsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIIpv6IcmpStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsMIIpv6IcmpInTimeExcds}, GetNextIndexFsMIIpv6IcmpStatsTable, FsMIIpv6IcmpInTimeExcdsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIIpv6IcmpStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsMIIpv6IcmpInParmProbs}, GetNextIndexFsMIIpv6IcmpStatsTable, FsMIIpv6IcmpInParmProbsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIIpv6IcmpStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsMIIpv6IcmpInPktTooBigs}, GetNextIndexFsMIIpv6IcmpStatsTable, FsMIIpv6IcmpInPktTooBigsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIIpv6IcmpStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsMIIpv6IcmpInEchos}, GetNextIndexFsMIIpv6IcmpStatsTable, FsMIIpv6IcmpInEchosGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIIpv6IcmpStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsMIIpv6IcmpInEchoReps}, GetNextIndexFsMIIpv6IcmpStatsTable, FsMIIpv6IcmpInEchoRepsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIIpv6IcmpStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsMIIpv6IcmpInRouterSolicits}, GetNextIndexFsMIIpv6IcmpStatsTable, FsMIIpv6IcmpInRouterSolicitsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIIpv6IcmpStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsMIIpv6IcmpInRouterAdvertisements}, GetNextIndexFsMIIpv6IcmpStatsTable, FsMIIpv6IcmpInRouterAdvertisementsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIIpv6IcmpStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsMIIpv6IcmpInNeighborSolicits}, GetNextIndexFsMIIpv6IcmpStatsTable, FsMIIpv6IcmpInNeighborSolicitsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIIpv6IcmpStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsMIIpv6IcmpInNeighborAdvertisements}, GetNextIndexFsMIIpv6IcmpStatsTable, FsMIIpv6IcmpInNeighborAdvertisementsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIIpv6IcmpStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsMIIpv6IcmpInRedirects}, GetNextIndexFsMIIpv6IcmpStatsTable, FsMIIpv6IcmpInRedirectsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIIpv6IcmpStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsMIIpv6IcmpInAdminProhib}, GetNextIndexFsMIIpv6IcmpStatsTable, FsMIIpv6IcmpInAdminProhibGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIIpv6IcmpStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsMIIpv6IcmpOutMsgs}, GetNextIndexFsMIIpv6IcmpStatsTable, FsMIIpv6IcmpOutMsgsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIIpv6IcmpStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsMIIpv6IcmpOutErrors}, GetNextIndexFsMIIpv6IcmpStatsTable, FsMIIpv6IcmpOutErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIIpv6IcmpStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsMIIpv6IcmpOutDestUnreachs}, GetNextIndexFsMIIpv6IcmpStatsTable, FsMIIpv6IcmpOutDestUnreachsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIIpv6IcmpStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsMIIpv6IcmpOutTimeExcds}, GetNextIndexFsMIIpv6IcmpStatsTable, FsMIIpv6IcmpOutTimeExcdsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIIpv6IcmpStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsMIIpv6IcmpOutParmProbs}, GetNextIndexFsMIIpv6IcmpStatsTable, FsMIIpv6IcmpOutParmProbsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIIpv6IcmpStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsMIIpv6IcmpOutPktTooBigs}, GetNextIndexFsMIIpv6IcmpStatsTable, FsMIIpv6IcmpOutPktTooBigsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIIpv6IcmpStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsMIIpv6IcmpOutEchos}, GetNextIndexFsMIIpv6IcmpStatsTable, FsMIIpv6IcmpOutEchosGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIIpv6IcmpStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsMIIpv6IcmpOutEchoReps}, GetNextIndexFsMIIpv6IcmpStatsTable, FsMIIpv6IcmpOutEchoRepsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIIpv6IcmpStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsMIIpv6IcmpOutRouterSolicits}, GetNextIndexFsMIIpv6IcmpStatsTable, FsMIIpv6IcmpOutRouterSolicitsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIIpv6IcmpStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsMIIpv6IcmpOutRouterAdvertisements}, GetNextIndexFsMIIpv6IcmpStatsTable, FsMIIpv6IcmpOutRouterAdvertisementsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIIpv6IcmpStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsMIIpv6IcmpOutNeighborSolicits}, GetNextIndexFsMIIpv6IcmpStatsTable, FsMIIpv6IcmpOutNeighborSolicitsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIIpv6IcmpStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsMIIpv6IcmpOutNeighborAdvertisements}, GetNextIndexFsMIIpv6IcmpStatsTable, FsMIIpv6IcmpOutNeighborAdvertisementsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIIpv6IcmpStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsMIIpv6IcmpOutRedirects}, GetNextIndexFsMIIpv6IcmpStatsTable, FsMIIpv6IcmpOutRedirectsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIIpv6IcmpStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsMIIpv6IcmpOutAdminProhib}, GetNextIndexFsMIIpv6IcmpStatsTable, FsMIIpv6IcmpOutAdminProhibGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIIpv6IcmpStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsMIIpv6IcmpInBadCode}, GetNextIndexFsMIIpv6IcmpStatsTable, FsMIIpv6IcmpInBadCodeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIIpv6IcmpStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsMIIpv6IcmpInNARouterFlagSet}, GetNextIndexFsMIIpv6IcmpStatsTable, FsMIIpv6IcmpInNARouterFlagSetGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIIpv6IcmpStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsMIIpv6IcmpInNASolicitedFlagSet}, GetNextIndexFsMIIpv6IcmpStatsTable, FsMIIpv6IcmpInNASolicitedFlagSetGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIIpv6IcmpStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsMIIpv6IcmpInNAOverrideFlagSet}, GetNextIndexFsMIIpv6IcmpStatsTable, FsMIIpv6IcmpInNAOverrideFlagSetGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIIpv6IcmpStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsMIIpv6IcmpOutNARouterFlagSet}, GetNextIndexFsMIIpv6IcmpStatsTable, FsMIIpv6IcmpOutNARouterFlagSetGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIIpv6IcmpStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsMIIpv6IcmpOutNASolicitedFlagSet}, GetNextIndexFsMIIpv6IcmpStatsTable, FsMIIpv6IcmpOutNASolicitedFlagSetGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIIpv6IcmpStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsMIIpv6IcmpOutNAOverrideFlagSet}, GetNextIndexFsMIIpv6IcmpStatsTable, FsMIIpv6IcmpOutNAOverrideFlagSetGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIIpv6IcmpStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsMIIpv6PmtuDest}, GetNextIndexFsMIIpv6PmtuTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsMIIpv6PmtuTableINDEX, 2, 0, 0, NULL},

{{13,FsMIIpv6Pmtu}, GetNextIndexFsMIIpv6PmtuTable, FsMIIpv6PmtuGet, FsMIIpv6PmtuSet, FsMIIpv6PmtuTest, FsMIIpv6PmtuTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIIpv6PmtuTableINDEX, 2, 0, 0, NULL},

{{13,FsMIIpv6PmtuTimeStamp}, GetNextIndexFsMIIpv6PmtuTable, FsMIIpv6PmtuTimeStampGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIIpv6PmtuTableINDEX, 2, 0, 0, NULL},

{{13,FsMIIpv6PmtuAdminStatus}, GetNextIndexFsMIIpv6PmtuTable, FsMIIpv6PmtuAdminStatusGet, FsMIIpv6PmtuAdminStatusSet, FsMIIpv6PmtuAdminStatusTest, FsMIIpv6PmtuTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIIpv6PmtuTableINDEX, 2, 0, 0, NULL},

{{13,FsMIIpv6NDProxyAddr}, GetNextIndexFsMIIpv6NDProxyListTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsMIIpv6NDProxyListTableINDEX, 2, 0, 0, NULL},

{{13,FsMIIpv6NDProxyAdminStatus}, GetNextIndexFsMIIpv6NDProxyListTable, FsMIIpv6NDProxyAdminStatusGet, FsMIIpv6NDProxyAdminStatusSet, FsMIIpv6NDProxyAdminStatusTest, FsMIIpv6NDProxyListTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIIpv6NDProxyListTableINDEX, 2, 0, 0, NULL},

{{13,FsMIIpv6PingIndex}, GetNextIndexFsMIIpv6PingTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIIpv6PingTableINDEX, 1, 0, 0, NULL},

{{13,FsMIIpv6PingDest}, GetNextIndexFsMIIpv6PingTable, FsMIIpv6PingDestGet, FsMIIpv6PingDestSet, FsMIIpv6PingDestTest, FsMIIpv6PingTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIIpv6PingTableINDEX, 1, 0, 0, NULL},

{{13,FsMIIpv6PingIfIndex}, GetNextIndexFsMIIpv6PingTable, FsMIIpv6PingIfIndexGet, FsMIIpv6PingIfIndexSet, FsMIIpv6PingIfIndexTest, FsMIIpv6PingTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIIpv6PingTableINDEX, 1, 0, 0, NULL},

{{13,FsMIIpv6PingContextId}, GetNextIndexFsMIIpv6PingTable, FsMIIpv6PingContextIdGet, FsMIIpv6PingContextIdSet, FsMIIpv6PingContextIdTest, FsMIIpv6PingTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIIpv6PingTableINDEX, 1, 0, 0, NULL},

{{13,FsMIIpv6PingAdminStatus}, GetNextIndexFsMIIpv6PingTable, FsMIIpv6PingAdminStatusGet, FsMIIpv6PingAdminStatusSet, FsMIIpv6PingAdminStatusTest, FsMIIpv6PingTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIIpv6PingTableINDEX, 1, 0, 0, NULL},

{{13,FsMIIpv6PingInterval}, GetNextIndexFsMIIpv6PingTable, FsMIIpv6PingIntervalGet, FsMIIpv6PingIntervalSet, FsMIIpv6PingIntervalTest, FsMIIpv6PingTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIIpv6PingTableINDEX, 1, 0, 0, "1"},

{{13,FsMIIpv6PingRcvTimeout}, GetNextIndexFsMIIpv6PingTable, FsMIIpv6PingRcvTimeoutGet, FsMIIpv6PingRcvTimeoutSet, FsMIIpv6PingRcvTimeoutTest, FsMIIpv6PingTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIIpv6PingTableINDEX, 1, 0, 0, "5"},

{{13,FsMIIpv6PingTries}, GetNextIndexFsMIIpv6PingTable, FsMIIpv6PingTriesGet, FsMIIpv6PingTriesSet, FsMIIpv6PingTriesTest, FsMIIpv6PingTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIIpv6PingTableINDEX, 1, 0, 0, "5"},

{{13,FsMIIpv6PingSize}, GetNextIndexFsMIIpv6PingTable, FsMIIpv6PingSizeGet, FsMIIpv6PingSizeSet, FsMIIpv6PingSizeTest, FsMIIpv6PingTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIIpv6PingTableINDEX, 1, 0, 0, "100"},

{{13,FsMIIpv6PingSentCount}, GetNextIndexFsMIIpv6PingTable, FsMIIpv6PingSentCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIIpv6PingTableINDEX, 1, 0, 0, NULL},

{{13,FsMIIpv6PingAverageTime}, GetNextIndexFsMIIpv6PingTable, FsMIIpv6PingAverageTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIIpv6PingTableINDEX, 1, 0, 0, NULL},

{{13,FsMIIpv6PingMaxTime}, GetNextIndexFsMIIpv6PingTable, FsMIIpv6PingMaxTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIIpv6PingTableINDEX, 1, 0, 0, NULL},

{{13,FsMIIpv6PingMinTime}, GetNextIndexFsMIIpv6PingTable, FsMIIpv6PingMinTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIIpv6PingTableINDEX, 1, 0, 0, NULL},

{{13,FsMIIpv6PingOperStatus}, GetNextIndexFsMIIpv6PingTable, FsMIIpv6PingOperStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIIpv6PingTableINDEX, 1, 0, 0, NULL},

{{13,FsMIIpv6PingSuccesses}, GetNextIndexFsMIIpv6PingTable, FsMIIpv6PingSuccessesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIIpv6PingTableINDEX, 1, 0, 0, NULL},

{{13,FsMIIpv6PingPercentageLoss}, GetNextIndexFsMIIpv6PingTable, FsMIIpv6PingPercentageLossGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIIpv6PingTableINDEX, 1, 0, 0, NULL},

{{13,FsMIIpv6PingData}, GetNextIndexFsMIIpv6PingTable, FsMIIpv6PingDataGet, FsMIIpv6PingDataSet, FsMIIpv6PingDataTest, FsMIIpv6PingTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIIpv6PingTableINDEX, 1, 0, 0, NULL},

{{13,FsMIIpv6PingSrcAddr}, GetNextIndexFsMIIpv6PingTable, FsMIIpv6PingSrcAddrGet, FsMIIpv6PingSrcAddrSet, FsMIIpv6PingSrcAddrTest, FsMIIpv6PingTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIIpv6PingTableINDEX, 1, 0, 0, NULL},

{{13,FsMIIpv6PingZoneId}, GetNextIndexFsMIIpv6PingTable, FsMIIpv6PingZoneIdGet, FsMIIpv6PingZoneIdSet, FsMIIpv6PingZoneIdTest, FsMIIpv6PingTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIIpv6PingTableINDEX, 1, 0, 0, NULL},

{{13,FsMIIpv6PingDestAddrType}, GetNextIndexFsMIIpv6PingTable, FsMIIpv6PingDestAddrTypeGet, FsMIIpv6PingDestAddrTypeSet, FsMIIpv6PingDestAddrTypeTest, FsMIIpv6PingTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIIpv6PingTableINDEX, 1, 0, 0, "0"},

{{13,FsMIIpv6PingHostName}, GetNextIndexFsMIIpv6PingTable, FsMIIpv6PingHostNameGet, FsMIIpv6PingHostNameSet, FsMIIpv6PingHostNameTest, FsMIIpv6PingTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIIpv6PingTableINDEX, 1, 0, 0, NULL},

{{11,FsMIIpv6GlobalDebug}, NULL, FsMIIpv6GlobalDebugGet, FsMIIpv6GlobalDebugSet, FsMIIpv6GlobalDebugTest, FsMIIpv6GlobalDebugDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{13,FsMIIpv6AddrSelPolicyPrefix}, GetNextIndexFsMIIpv6AddrSelPolicyTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsMIIpv6AddrSelPolicyTableINDEX, 3, 0, 0, NULL},

{{13,FsMIIpv6AddrSelPolicyPrefixLen}, GetNextIndexFsMIIpv6AddrSelPolicyTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIIpv6AddrSelPolicyTableINDEX, 3, 0, 0, NULL},

{{13,FsMIIpv6AddrSelPolicyIfIndex}, GetNextIndexFsMIIpv6AddrSelPolicyTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIIpv6AddrSelPolicyTableINDEX, 3, 0, 0, NULL},

{{13,FsMIIpv6AddrSelPolicyScope}, GetNextIndexFsMIIpv6AddrSelPolicyTable, FsMIIpv6AddrSelPolicyScopeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIIpv6AddrSelPolicyTableINDEX, 3, 0, 0, NULL},

{{13,FsMIIpv6AddrSelPolicyPrecedence}, GetNextIndexFsMIIpv6AddrSelPolicyTable, FsMIIpv6AddrSelPolicyPrecedenceGet, FsMIIpv6AddrSelPolicyPrecedenceSet, FsMIIpv6AddrSelPolicyPrecedenceTest, FsMIIpv6AddrSelPolicyTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIIpv6AddrSelPolicyTableINDEX, 3, 0, 0, "30"},

{{13,FsMIIpv6AddrSelPolicyLabel}, GetNextIndexFsMIIpv6AddrSelPolicyTable, FsMIIpv6AddrSelPolicyLabelGet, FsMIIpv6AddrSelPolicyLabelSet, FsMIIpv6AddrSelPolicyLabelTest, FsMIIpv6AddrSelPolicyTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIIpv6AddrSelPolicyTableINDEX, 3, 0, 0, "2"},

{{13,FsMIIpv6AddrSelPolicyAddrType}, GetNextIndexFsMIIpv6AddrSelPolicyTable, FsMIIpv6AddrSelPolicyAddrTypeGet, FsMIIpv6AddrSelPolicyAddrTypeSet, FsMIIpv6AddrSelPolicyAddrTypeTest, FsMIIpv6AddrSelPolicyTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIIpv6AddrSelPolicyTableINDEX, 3, 0, 0, NULL},

{{13,FsMIIpv6AddrSelPolicyIsPublicAddr}, GetNextIndexFsMIIpv6AddrSelPolicyTable, FsMIIpv6AddrSelPolicyIsPublicAddrGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIIpv6AddrSelPolicyTableINDEX, 3, 0, 0, NULL},

{{13,FsMIIpv6AddrSelPolicyIsSelfAddr}, GetNextIndexFsMIIpv6AddrSelPolicyTable, FsMIIpv6AddrSelPolicyIsSelfAddrGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIIpv6AddrSelPolicyTableINDEX, 3, 0, 0, NULL},

{{13,FsMIIpv6AddrSelPolicyReachabilityStatus}, GetNextIndexFsMIIpv6AddrSelPolicyTable, FsMIIpv6AddrSelPolicyReachabilityStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIIpv6AddrSelPolicyTableINDEX, 3, 0, 0, NULL},

{{13,FsMIIpv6AddrSelPolicyConfigStatus}, GetNextIndexFsMIIpv6AddrSelPolicyTable, FsMIIpv6AddrSelPolicyConfigStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIIpv6AddrSelPolicyTableINDEX, 3, 0, 0, NULL},

{{13,FsMIIpv6AddrSelPolicyRowStatus}, GetNextIndexFsMIIpv6AddrSelPolicyTable, FsMIIpv6AddrSelPolicyRowStatusGet, FsMIIpv6AddrSelPolicyRowStatusSet, FsMIIpv6AddrSelPolicyRowStatusTest, FsMIIpv6AddrSelPolicyTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIIpv6AddrSelPolicyTableINDEX, 3, 0, 1, NULL},

{{13,FsMIIpv6ScopeZoneIndexIfIndex}, GetNextIndexFsMIIpv6IfScopeZoneMapTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIIpv6IfScopeZoneMapTableINDEX, 1, 0, 0, "Invalid"},

{{13,FsMIIpv6ScopeZoneIndexInterfaceLocal}, GetNextIndexFsMIIpv6IfScopeZoneMapTable, FsMIIpv6ScopeZoneIndexInterfaceLocalGet, FsMIIpv6ScopeZoneIndexInterfaceLocalSet, FsMIIpv6ScopeZoneIndexInterfaceLocalTest, FsMIIpv6IfScopeZoneMapTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIIpv6IfScopeZoneMapTableINDEX, 1, 0, 0, "Invalid"},

{{13,FsMIIpv6ScopeZoneIndexLinkLocal}, GetNextIndexFsMIIpv6IfScopeZoneMapTable, FsMIIpv6ScopeZoneIndexLinkLocalGet, FsMIIpv6ScopeZoneIndexLinkLocalSet, FsMIIpv6ScopeZoneIndexLinkLocalTest, FsMIIpv6IfScopeZoneMapTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIIpv6IfScopeZoneMapTableINDEX, 1, 0, 0, "Invalid"},

{{13,FsMIIpv6ScopeZoneIndex3}, GetNextIndexFsMIIpv6IfScopeZoneMapTable, FsMIIpv6ScopeZoneIndex3Get, FsMIIpv6ScopeZoneIndex3Set, FsMIIpv6ScopeZoneIndex3Test, FsMIIpv6IfScopeZoneMapTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIIpv6IfScopeZoneMapTableINDEX, 1, 0, 0, "Invalid"},

{{13,FsMIIpv6ScopeZoneIndexAdminLocal}, GetNextIndexFsMIIpv6IfScopeZoneMapTable, FsMIIpv6ScopeZoneIndexAdminLocalGet, FsMIIpv6ScopeZoneIndexAdminLocalSet, FsMIIpv6ScopeZoneIndexAdminLocalTest, FsMIIpv6IfScopeZoneMapTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIIpv6IfScopeZoneMapTableINDEX, 1, 0, 0, "Invalid"},

{{13,FsMIIpv6ScopeZoneIndexSiteLocal}, GetNextIndexFsMIIpv6IfScopeZoneMapTable, FsMIIpv6ScopeZoneIndexSiteLocalGet, FsMIIpv6ScopeZoneIndexSiteLocalSet, FsMIIpv6ScopeZoneIndexSiteLocalTest, FsMIIpv6IfScopeZoneMapTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIIpv6IfScopeZoneMapTableINDEX, 1, 0, 0, "Invalid"},

{{13,FsMIIpv6ScopeZoneIndex6}, GetNextIndexFsMIIpv6IfScopeZoneMapTable, FsMIIpv6ScopeZoneIndex6Get, FsMIIpv6ScopeZoneIndex6Set, FsMIIpv6ScopeZoneIndex6Test, FsMIIpv6IfScopeZoneMapTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIIpv6IfScopeZoneMapTableINDEX, 1, 0, 0, "Invalid"},

{{13,FsMIIpv6ScopeZoneIndex7}, GetNextIndexFsMIIpv6IfScopeZoneMapTable, FsMIIpv6ScopeZoneIndex7Get, FsMIIpv6ScopeZoneIndex7Set, FsMIIpv6ScopeZoneIndex7Test, FsMIIpv6IfScopeZoneMapTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIIpv6IfScopeZoneMapTableINDEX, 1, 0, 0, "Invalid"},

{{13,FsMIIpv6ScopeZoneIndexOrganizationLocal}, GetNextIndexFsMIIpv6IfScopeZoneMapTable, FsMIIpv6ScopeZoneIndexOrganizationLocalGet, FsMIIpv6ScopeZoneIndexOrganizationLocalSet, FsMIIpv6ScopeZoneIndexOrganizationLocalTest, FsMIIpv6IfScopeZoneMapTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIIpv6IfScopeZoneMapTableINDEX, 1, 0, 0, "Invalid"},

{{13,FsMIIpv6ScopeZoneIndex9}, GetNextIndexFsMIIpv6IfScopeZoneMapTable, FsMIIpv6ScopeZoneIndex9Get, FsMIIpv6ScopeZoneIndex9Set, FsMIIpv6ScopeZoneIndex9Test, FsMIIpv6IfScopeZoneMapTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIIpv6IfScopeZoneMapTableINDEX, 1, 0, 0, "Invalid"},

{{13,FsMIIpv6ScopeZoneIndexA}, GetNextIndexFsMIIpv6IfScopeZoneMapTable, FsMIIpv6ScopeZoneIndexAGet, FsMIIpv6ScopeZoneIndexASet, FsMIIpv6ScopeZoneIndexATest, FsMIIpv6IfScopeZoneMapTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIIpv6IfScopeZoneMapTableINDEX, 1, 0, 0, "Invalid"},

{{13,FsMIIpv6ScopeZoneIndexB}, GetNextIndexFsMIIpv6IfScopeZoneMapTable, FsMIIpv6ScopeZoneIndexBGet, FsMIIpv6ScopeZoneIndexBSet, FsMIIpv6ScopeZoneIndexBTest, FsMIIpv6IfScopeZoneMapTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIIpv6IfScopeZoneMapTableINDEX, 1, 0, 0, "Invalid"},

{{13,FsMIIpv6ScopeZoneIndexC}, GetNextIndexFsMIIpv6IfScopeZoneMapTable, FsMIIpv6ScopeZoneIndexCGet, FsMIIpv6ScopeZoneIndexCSet, FsMIIpv6ScopeZoneIndexCTest, FsMIIpv6IfScopeZoneMapTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIIpv6IfScopeZoneMapTableINDEX, 1, 0, 0, "Invalid"},

{{13,FsMIIpv6ScopeZoneIndexD}, GetNextIndexFsMIIpv6IfScopeZoneMapTable, FsMIIpv6ScopeZoneIndexDGet, FsMIIpv6ScopeZoneIndexDSet, FsMIIpv6ScopeZoneIndexDTest, FsMIIpv6IfScopeZoneMapTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIIpv6IfScopeZoneMapTableINDEX, 1, 0, 0, "Invalid"},

{{13,FsMIIpv6ScopeZoneIndexE}, GetNextIndexFsMIIpv6IfScopeZoneMapTable, FsMIIpv6ScopeZoneIndexEGet, FsMIIpv6ScopeZoneIndexESet, FsMIIpv6ScopeZoneIndexETest, FsMIIpv6IfScopeZoneMapTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIIpv6IfScopeZoneMapTableINDEX, 1, 0, 0, "Invalid"},

{{13,FsMIIpv6IfScopeZoneCreationStatus}, GetNextIndexFsMIIpv6IfScopeZoneMapTable, FsMIIpv6IfScopeZoneCreationStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIIpv6IfScopeZoneMapTableINDEX, 1, 0, 0, "1"},

{{13,FsMIIpv6IfScopeZoneRowStatus}, GetNextIndexFsMIIpv6IfScopeZoneMapTable, FsMIIpv6IfScopeZoneRowStatusGet, FsMIIpv6IfScopeZoneRowStatusSet, FsMIIpv6IfScopeZoneRowStatusTest, FsMIIpv6IfScopeZoneMapTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIIpv6IfScopeZoneMapTableINDEX, 1, 0, 1, NULL},

{{13,FsMIIpv6ScopeZoneContextId}, GetNextIndexFsMIIpv6ScopeZoneTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIIpv6ScopeZoneTableINDEX, 2, 0, 0, NULL},

{{13,FsMIIpv6ScopeZoneName}, GetNextIndexFsMIIpv6ScopeZoneTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsMIIpv6ScopeZoneTableINDEX, 2, 0, 0, NULL},

{{13,FsMIIpv6ScopeZoneIndex}, GetNextIndexFsMIIpv6ScopeZoneTable, FsMIIpv6ScopeZoneIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMIIpv6ScopeZoneTableINDEX, 2, 0, 0, NULL},

{{13,FsMIIpv6ScopeZoneCreationStatus}, GetNextIndexFsMIIpv6ScopeZoneTable, FsMIIpv6ScopeZoneCreationStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIIpv6ScopeZoneTableINDEX, 2, 0, 0, NULL},

{{13,FsMIIpv6ScopeZoneInterfaceList}, GetNextIndexFsMIIpv6ScopeZoneTable, FsMIIpv6ScopeZoneInterfaceListGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMIIpv6ScopeZoneTableINDEX, 2, 0, 0, NULL},

{{13,FsMIIpv6IsDefaultScopeZone}, GetNextIndexFsMIIpv6ScopeZoneTable, FsMIIpv6IsDefaultScopeZoneGet, FsMIIpv6IsDefaultScopeZoneSet, FsMIIpv6IsDefaultScopeZoneTest, FsMIIpv6ScopeZoneTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIIpv6ScopeZoneTableINDEX, 2, 0, 0, "2"},

{{13,FsMIIpv6RARouteIfIndex}, GetNextIndexFsMIIpv6RARouteInfoTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIIpv6RARouteInfoTableINDEX, 3, 0, 0, NULL},

{{13,FsMIIpv6RARoutePrefix}, GetNextIndexFsMIIpv6RARouteInfoTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsMIIpv6RARouteInfoTableINDEX, 3, 0, 0, NULL},

{{13,FsMIIpv6RARoutePrefixLen}, GetNextIndexFsMIIpv6RARouteInfoTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIIpv6RARouteInfoTableINDEX, 3, 0, 0, NULL},

{{13,FsMIIpv6RARoutePreference}, GetNextIndexFsMIIpv6RARouteInfoTable, FsMIIpv6RARoutePreferenceGet, FsMIIpv6RARoutePreferenceSet, FsMIIpv6RARoutePreferenceTest, FsMIIpv6RARouteInfoTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIIpv6RARouteInfoTableINDEX, 3, 0, 0, "1"},

{{13,FsMIIpv6RARouteLifetime}, GetNextIndexFsMIIpv6RARouteInfoTable, FsMIIpv6RARouteLifetimeGet, FsMIIpv6RARouteLifetimeSet, FsMIIpv6RARouteLifetimeTest, FsMIIpv6RARouteInfoTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIIpv6RARouteInfoTableINDEX, 3, 0, 0, "2147483647"},

{{13,FsMIIpv6RARouteRowStatus}, GetNextIndexFsMIIpv6RARouteInfoTable, FsMIIpv6RARouteRowStatusGet, FsMIIpv6RARouteRowStatusSet, FsMIIpv6RARouteRowStatusTest, FsMIIpv6RARouteInfoTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIIpv6RARouteInfoTableINDEX, 3, 0, 1, NULL},

{{11,FsMIIpv6TestRedEntryTime}, NULL, FsMIIpv6TestRedEntryTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsMIIpv6TestRedExitTime}, NULL, FsMIIpv6TestRedExitTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},
{{13,FsMIIpv6IfRaRDNSSIndex}, GetNextIndexFsMIIpv6IfRaRDNSSTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIIpv6IfRaRDNSSTableINDEX, 1, 0, 0, NULL},

{{13,FsMIIpv6IfRadvRDNSSOpen}, GetNextIndexFsMIIpv6IfRaRDNSSTable, FsMIIpv6IfRadvRDNSSOpenGet, FsMIIpv6IfRadvRDNSSOpenSet, FsMIIpv6IfRadvRDNSSOpenTest, FsMIIpv6IfRaRDNSSTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIIpv6IfRaRDNSSTableINDEX, 1, 0, 0, "2"},

{{13,FsMIIpv6IfRaRDNSSPreference}, GetNextIndexFsMIIpv6IfRaRDNSSTable, FsMIIpv6IfRaRDNSSPreferenceGet, FsMIIpv6IfRaRDNSSPreferenceSet, FsMIIpv6IfRaRDNSSPreferenceTest, FsMIIpv6IfRaRDNSSTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIIpv6IfRaRDNSSTableINDEX, 1, 0, 0, "8"},

{{13,FsMIIpv6IfRaRDNSSLifetime}, GetNextIndexFsMIIpv6IfRaRDNSSTable, FsMIIpv6IfRaRDNSSLifetimeGet, FsMIIpv6IfRaRDNSSLifetimeSet, FsMIIpv6IfRaRDNSSLifetimeTest, FsMIIpv6IfRaRDNSSTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIIpv6IfRaRDNSSTableINDEX, 1, 0, 0, NULL},

{{13,FsMIIpv6IfRaRDNSSAddrOne}, GetNextIndexFsMIIpv6IfRaRDNSSTable, FsMIIpv6IfRaRDNSSAddrOneGet, FsMIIpv6IfRaRDNSSAddrOneSet, FsMIIpv6IfRaRDNSSAddrOneTest, FsMIIpv6IfRaRDNSSTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIIpv6IfRaRDNSSTableINDEX, 1, 0, 0, NULL},

{{13,FsMIIpv6IfRaRDNSSAddrTwo}, GetNextIndexFsMIIpv6IfRaRDNSSTable, FsMIIpv6IfRaRDNSSAddrTwoGet, FsMIIpv6IfRaRDNSSAddrTwoSet, FsMIIpv6IfRaRDNSSAddrTwoTest, FsMIIpv6IfRaRDNSSTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIIpv6IfRaRDNSSTableINDEX, 1, 0, 0, NULL},

{{13,FsMIIpv6IfRaRDNSSAddrThree}, GetNextIndexFsMIIpv6IfRaRDNSSTable, FsMIIpv6IfRaRDNSSAddrThreeGet, FsMIIpv6IfRaRDNSSAddrThreeSet, FsMIIpv6IfRaRDNSSAddrThreeTest, FsMIIpv6IfRaRDNSSTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIIpv6IfRaRDNSSTableINDEX, 1, 0, 0, NULL},

{{13,FsMIIpv6IfRaRDNSSRowStatus}, GetNextIndexFsMIIpv6IfRaRDNSSTable, FsMIIpv6IfRaRDNSSRowStatusGet, FsMIIpv6IfRaRDNSSRowStatusSet, FsMIIpv6IfRaRDNSSRowStatusTest, FsMIIpv6IfRaRDNSSTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIIpv6IfRaRDNSSTableINDEX, 1, 0, 1, NULL},
{{13,FsMIIpv6IfDomainNameOne}, GetNextIndexFsMIIpv6IfRaRDNSSTable, FsMIIpv6IfDomainNameOneGet, FsMIIpv6IfDomainNameOneSet, FsMIIpv6IfDomainNameOneTest, FsMIIpv6IfRaRDNSSTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIIpv6IfRaRDNSSTableINDEX, 1, 0, 0, NULL},

{{13,FsMIIpv6IfDomainNameTwo}, GetNextIndexFsMIIpv6IfRaRDNSSTable, FsMIIpv6IfDomainNameTwoGet, FsMIIpv6IfDomainNameTwoSet, FsMIIpv6IfDomainNameTwoTest, FsMIIpv6IfRaRDNSSTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIIpv6IfRaRDNSSTableINDEX, 1, 0, 0, NULL},

{{13,FsMIIpv6IfDomainNameThree}, GetNextIndexFsMIIpv6IfRaRDNSSTable, FsMIIpv6IfDomainNameThreeGet, FsMIIpv6IfDomainNameThreeSet, FsMIIpv6IfDomainNameThreeTest, FsMIIpv6IfRaRDNSSTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIIpv6IfRaRDNSSTableINDEX, 1, 0, 0, NULL},

{{13,FsMIIpv6IfDnsLifeTime}, GetNextIndexFsMIIpv6IfRaRDNSSTable, FsMIIpv6IfDnsLifeTimeGet, FsMIIpv6IfDnsLifeTimeSet, FsMIIpv6IfDnsLifeTimeTest, FsMIIpv6IfRaRDNSSTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIIpv6IfRaRDNSSTableINDEX, 1, 0, 0, NULL},

{{13,FsMIIpv6IfRaRDNSSAddrOneLife}, GetNextIndexFsMIIpv6IfRaRDNSSTable, FsMIIpv6IfRaRDNSSAddrOneLifeGet, FsMIIpv6IfRaRDNSSAddrOneLifeSet, FsMIIpv6IfRaRDNSSAddrOneLifeTest, FsMIIpv6IfRaRDNSSTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIIpv6IfRaRDNSSTableINDEX, 1, 0, 0, NULL},

{{13,FsMIIpv6IfRaRDNSSAddrTwoLife}, GetNextIndexFsMIIpv6IfRaRDNSSTable, FsMIIpv6IfRaRDNSSAddrTwoLifeGet, FsMIIpv6IfRaRDNSSAddrTwoLifeSet, FsMIIpv6IfRaRDNSSAddrTwoLifeTest, FsMIIpv6IfRaRDNSSTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIIpv6IfRaRDNSSTableINDEX, 1, 0, 0, NULL},

{{13,FsMIIpv6IfRaRDNSSAddrThreeLife}, GetNextIndexFsMIIpv6IfRaRDNSSTable, FsMIIpv6IfRaRDNSSAddrThreeLifeGet, FsMIIpv6IfRaRDNSSAddrThreeLifeSet, FsMIIpv6IfRaRDNSSAddrThreeLifeTest, FsMIIpv6IfRaRDNSSTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIIpv6IfRaRDNSSTableINDEX, 1, 0, 0, NULL},

{{13,FsMIIpv6IfDomainNameOneLife}, GetNextIndexFsMIIpv6IfRaRDNSSTable, FsMIIpv6IfDomainNameOneLifeGet, FsMIIpv6IfDomainNameOneLifeSet, FsMIIpv6IfDomainNameOneLifeTest, FsMIIpv6IfRaRDNSSTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIIpv6IfRaRDNSSTableINDEX, 1, 0, 0, NULL},

{{13,FsMIIpv6IfDomainNameTwoLife}, GetNextIndexFsMIIpv6IfRaRDNSSTable, FsMIIpv6IfDomainNameTwoLifeGet, FsMIIpv6IfDomainNameTwoLifeSet, FsMIIpv6IfDomainNameTwoLifeTest, FsMIIpv6IfRaRDNSSTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIIpv6IfRaRDNSSTableINDEX, 1, 0, 0, NULL},

{{13,FsMIIpv6IfDomainNameThreeLife}, GetNextIndexFsMIIpv6IfRaRDNSSTable, FsMIIpv6IfDomainNameThreeLifeGet, FsMIIpv6IfDomainNameThreeLifeSet, FsMIIpv6IfDomainNameThreeLifeTest, FsMIIpv6IfRaRDNSSTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIIpv6IfRaRDNSSTableINDEX, 1, 0, 0, NULL},

};
tMibData fsmpip6Entry = { sizeof(fsmpip6MibEntry) / sizeof(fsmpip6MibEntry[0]),  fsmpip6MibEntry };

tMbDbEntry fsmprt6MibEntry[]= {
{{13,FsMIIpv6Protocol}, GetNextIndexFsMIIpv6PrefTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIIpv6PrefTableINDEX, 2, 0, 0, NULL},

{{13,FsMIIpv6Preference}, GetNextIndexFsMIIpv6PrefTable, FsMIIpv6PreferenceGet, FsMIIpv6PreferenceSet, FsMIIpv6PreferenceTest, FsMIIpv6PrefTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIIpv6PrefTableINDEX, 2, 0, 0, NULL},
};
tMibData fsmprt6Entry = { sizeof(fsmprt6MibEntry) / sizeof(fsmprt6MibEntry[0]), fsmprt6MibEntry };

/* fsmpipv6Entry is manually added to group the ip6 and ip6route mib objects */
tMibData *fsmpipv6Entry[] = {
    NULL,
    &fsmpip6Entry,
    &fsmprt6Entry,
};
#endif /* _FSMPIPV6DB_H */

