/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsip6low.h,v 1.22 2015/11/13 12:05:07 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsipv6NdCacheMaxRetries ARG_LIST((INT4 *));

INT1
nmhGetFsipv6PmtuConfigStatus ARG_LIST((INT4 *));

INT1
nmhGetFsipv6PmtuTimeOutInterval ARG_LIST((UINT4 *));

INT1
nmhGetFsipv6JumboEnable ARG_LIST((INT4 *));

INT1
nmhGetFsipv6NumOfSendJumbo ARG_LIST((INT4 *));

INT1
nmhGetFsipv6NumOfRecvJumbo ARG_LIST((INT4 *));

INT1
nmhGetFsipv6ErrJumbo ARG_LIST((INT4 *));

INT1
nmhGetFsipv6GlobalDebug ARG_LIST((UINT4 *));

INT1
nmhGetFsipv6MaxRouteEntries ARG_LIST((UINT4 *));

INT1
nmhGetFsipv6MaxLogicalIfaces ARG_LIST((UINT4 *));

INT1
nmhGetFsipv6MaxTunnelIfaces ARG_LIST((UINT4 *));

INT1
nmhGetFsipv6MaxAddresses ARG_LIST((UINT4 *));

INT1
nmhGetFsipv6MaxFragReasmEntries ARG_LIST((UINT4 *));

INT1
nmhGetFsipv6Nd6MaxCacheEntries ARG_LIST((UINT4 *));

INT1
nmhGetFsipv6PmtuMaxDest ARG_LIST((UINT4 *));

INT1
nmhGetFsipv6RFC5095Compatibility ARG_LIST((INT4 *));

INT1
nmhGetFsipv6RFC5942Compatibility ARG_LIST((INT4 *));


/* Low Level SET Routine for All Objects.  */

INT1
nmhGetFsipv6SENDSecLevel ARG_LIST((INT4 *));

INT1
nmhGetFsipv6SENDNbrSecLevel ARG_LIST((INT4 *));

INT1
nmhGetFsipv6SENDAuthType ARG_LIST((INT4 *));

INT1
nmhGetFsipv6SENDMinBits ARG_LIST((INT4 *));

INT1
nmhGetFsipv6SENDSecDAD ARG_LIST((INT4 *));

INT1
nmhGetFsipv6SENDPrefixChk ARG_LIST((INT4 *));

INT1
nmhGetFsipv6ECMPPRTTimer ARG_LIST((INT4 *));

INT1
nmhGetFsipv6NdCacheTimeout ARG_LIST((INT4 *));


/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsipv6NdCacheMaxRetries ARG_LIST((INT4 ));

INT1
nmhSetFsipv6PmtuConfigStatus ARG_LIST((INT4 ));

INT1
nmhSetFsipv6PmtuTimeOutInterval ARG_LIST((UINT4 ));

INT1
nmhSetFsipv6JumboEnable ARG_LIST((INT4 ));

INT1
nmhSetFsipv6GlobalDebug ARG_LIST((UINT4 ));

INT1
nmhSetFsipv6MaxRouteEntries ARG_LIST((UINT4 ));

INT1
nmhSetFsipv6MaxLogicalIfaces ARG_LIST((UINT4 ));

INT1
nmhSetFsipv6MaxTunnelIfaces ARG_LIST((UINT4 ));

INT1
nmhSetFsipv6MaxAddresses ARG_LIST((UINT4 ));

INT1
nmhSetFsipv6MaxFragReasmEntries ARG_LIST((UINT4 ));

INT1
nmhSetFsipv6Nd6MaxCacheEntries ARG_LIST((UINT4 ));

INT1
nmhSetFsipv6PmtuMaxDest ARG_LIST((UINT4 ));

INT1
nmhSetFsipv6RFC5095Compatibility ARG_LIST((INT4 ));

INT1
nmhSetFsipv6RFC5942Compatibility ARG_LIST((INT4 ));

INT1
nmhSetFsipv6SENDSecLevel ARG_LIST((INT4 ));

INT1
nmhSetFsipv6SENDNbrSecLevel ARG_LIST((INT4 ));

INT1
nmhSetFsipv6SENDAuthType ARG_LIST((INT4 ));

INT1
nmhSetFsipv6SENDMinBits ARG_LIST((INT4 ));

INT1
nmhSetFsipv6SENDSecDAD ARG_LIST((INT4 ));

INT1
nmhSetFsipv6SENDPrefixChk ARG_LIST((INT4 ));

INT1
nmhSetFsipv6ECMPPRTTimer ARG_LIST((INT4 ));

INT1
nmhSetFsipv6NdCacheTimeout ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Fsipv6NdCacheMaxRetries ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2Fsipv6PmtuConfigStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2Fsipv6PmtuTimeOutInterval ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2Fsipv6JumboEnable ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2Fsipv6GlobalDebug ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2Fsipv6MaxRouteEntries ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2Fsipv6MaxLogicalIfaces ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2Fsipv6MaxTunnelIfaces ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2Fsipv6MaxAddresses ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2Fsipv6MaxFragReasmEntries ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2Fsipv6Nd6MaxCacheEntries ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2Fsipv6PmtuMaxDest ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2Fsipv6RFC5095Compatibility ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2Fsipv6RFC5942Compatibility ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2Fsipv6SENDSecLevel ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2Fsipv6SENDNbrSecLevel ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2Fsipv6SENDAuthType ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2Fsipv6SENDMinBits ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2Fsipv6SENDSecDAD ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2Fsipv6SENDPrefixChk ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2Fsipv6ECMPPRTTimer ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2Fsipv6NdCacheTimeout ARG_LIST((UINT4 *  ,INT4 ));


/* Low Level DEP Routines for.  */

INT1
nmhDepv2Fsipv6NdCacheMaxRetries ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Fsipv6PmtuConfigStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Fsipv6PmtuTimeOutInterval ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Fsipv6JumboEnable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Fsipv6GlobalDebug ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Fsipv6MaxRouteEntries ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Fsipv6MaxLogicalIfaces ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Fsipv6MaxTunnelIfaces ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Fsipv6MaxAddresses ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Fsipv6MaxFragReasmEntries ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Fsipv6Nd6MaxCacheEntries ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Fsipv6PmtuMaxDest ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Fsipv6RFC5095Compatibility ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Fsipv6RFC5942Compatibility ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Fsipv6SENDSecLevel ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Fsipv6SENDNbrSecLevel ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Fsipv6SENDAuthType ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Fsipv6SENDMinBits ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Fsipv6SENDSecDAD ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Fsipv6SENDPrefixChk ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Fsipv6ECMPPRTTimer ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Fsipv6NdCacheTimeout ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));


/* Proto Validate Index Instance for Fsipv6IfTable. */
INT1
nmhValidateIndexInstanceFsipv6IfTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for Fsipv6IfTable  */

INT1
nmhGetFirstIndexFsipv6IfTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsipv6IfTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsipv6IfType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsipv6IfPortNum ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsipv6IfCircuitNum ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsipv6IfToken ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsipv6IfAdminStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsipv6IfOperStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsipv6IfRouterAdvStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsipv6IfRouterAdvFlags ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsipv6IfHopLimit ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsipv6IfDefRouterTime ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsipv6IfReachableTime ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsipv6IfRetransmitTime ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsipv6IfDelayProbeTime ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsipv6IfPrefixAdvStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsipv6IfMinRouterAdvTime ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsipv6IfMaxRouterAdvTime ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsipv6IfDADRetries ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsipv6IfForwarding ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsipv6IfRoutingStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsipv6IfIcmpErrInterval ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsipv6IfIcmpTokenBucketSize ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsipv6IfDestUnreachableMsg ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsipv6IfUnnumAssocIPIf ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsipv6IfRedirectMsg ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsipv6IfAdvSrcLLAdr ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsipv6IfAdvIntOpt ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsipv6IfNDProxyAdminStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsipv6IfNDProxyMode ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsipv6IfNDProxyOperStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsipv6IfNDProxyUpStream ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsipv6IfSENDSecStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsipv6IfSENDDeltaTimestamp ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsipv6IfSENDFuzzTimestamp ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsipv6IfSENDDriftTimestamp ARG_LIST((INT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhGetFsipv6IfDefRoutePreference ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsipv6IfToken ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsipv6IfAdminStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsipv6IfRouterAdvStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsipv6IfRouterAdvFlags ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsipv6IfHopLimit ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsipv6IfDefRouterTime ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsipv6IfReachableTime ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsipv6IfRetransmitTime ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsipv6IfDelayProbeTime ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsipv6IfPrefixAdvStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsipv6IfMinRouterAdvTime ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsipv6IfMaxRouterAdvTime ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsipv6IfDADRetries ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsipv6IfForwarding ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsipv6IfIcmpErrInterval ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsipv6IfIcmpTokenBucketSize ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsipv6IfDestUnreachableMsg ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsipv6IfUnnumAssocIPIf ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsipv6IfRedirectMsg ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsipv6IfAdvSrcLLAdr ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsipv6IfAdvIntOpt ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsipv6IfNDProxyAdminStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsipv6IfNDProxyMode ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsipv6IfNDProxyUpStream ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsipv6IfSENDSecStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsipv6IfSENDDeltaTimestamp ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsipv6IfSENDFuzzTimestamp ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsipv6IfSENDDriftTimestamp ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsipv6IfDefRoutePreference ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Fsipv6IfToken ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Fsipv6IfAdminStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Fsipv6IfRouterAdvStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Fsipv6IfRouterAdvFlags ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Fsipv6IfHopLimit ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Fsipv6IfDefRouterTime ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Fsipv6IfReachableTime ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Fsipv6IfRetransmitTime ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Fsipv6IfDelayProbeTime ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Fsipv6IfPrefixAdvStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Fsipv6IfMinRouterAdvTime ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Fsipv6IfMaxRouterAdvTime ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Fsipv6IfDADRetries ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Fsipv6IfForwarding ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Fsipv6IfIcmpErrInterval ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Fsipv6IfIcmpTokenBucketSize ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Fsipv6IfDestUnreachableMsg ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Fsipv6IfUnnumAssocIPIf ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Fsipv6IfRedirectMsg ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Fsipv6IfAdvSrcLLAdr ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Fsipv6IfAdvIntOpt ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Fsipv6IfNDProxyAdminStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Fsipv6IfNDProxyMode ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Fsipv6IfNDProxyUpStream ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Fsipv6IfSENDSecStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Fsipv6IfSENDDeltaTimestamp ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Fsipv6IfSENDFuzzTimestamp ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Fsipv6IfSENDDriftTimestamp ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Fsipv6IfDefRoutePreference ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Fsipv6IfTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Fsipv6IfStatsTable. */
INT1
nmhValidateIndexInstanceFsipv6IfStatsTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for Fsipv6IfStatsTable  */

INT1
nmhGetFirstIndexFsipv6IfStatsTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsipv6IfStatsTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsipv6IfStatsInReceives ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsipv6IfStatsInHdrErrors ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsipv6IfStatsTooBigErrors ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsipv6IfStatsInAddrErrors ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsipv6IfStatsForwDatagrams ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsipv6IfStatsInUnknownProtos ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsipv6IfStatsInDiscards ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsipv6IfStatsInDelivers ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsipv6IfStatsOutRequests ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsipv6IfStatsOutDiscards ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsipv6IfStatsOutNoRoutes ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsipv6IfStatsReasmReqds ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsipv6IfStatsReasmOKs ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsipv6IfStatsReasmFails ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsipv6IfStatsFragOKs ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsipv6IfStatsFragFails ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsipv6IfStatsFragCreates ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsipv6IfStatsInMcastPkts ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsipv6IfStatsOutMcastPkts ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsipv6IfStatsInTruncatedPkts ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsipv6IfStatsInRouterSols ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsipv6IfStatsInRouterAdvs ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsipv6IfStatsInNeighSols ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsipv6IfStatsInNeighAdvs ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsipv6IfStatsInRedirects ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsipv6IfStatsOutRouterSols ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsipv6IfStatsOutRouterAdvs ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsipv6IfStatsOutNeighSols ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsipv6IfStatsOutNeighAdvs ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsipv6IfStatsOutRedirects ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsipv6IfStatsLastRouterAdvTime ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsipv6IfStatsNextRouterAdvTime ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsipv6IfStatsIcmp6ErrRateLmtd ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsipv6IfStatsSENDDroppedPkts ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsipv6IfStatsSENDInvalidPkts ARG_LIST((INT4 ,UINT4 *));

/* Proto Validate Index Instance for Fsipv6PrefixTable. */
INT1
nmhValidateIndexInstanceFsipv6PrefixTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ));

/* Proto Type for Low Level GET FIRST fn for Fsipv6PrefixTable  */

INT1
nmhGetFirstIndexFsipv6PrefixTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE *  , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsipv6PrefixTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsipv6PrefixProfileIndex ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

INT1
nmhGetFsipv6SupportEmbeddedRp ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

INT1
nmhGetFsipv6PrefixAdminStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsipv6PrefixProfileIndex ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

INT1
nmhSetFsipv6SupportEmbeddedRp ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

INT1
nmhSetFsipv6PrefixAdminStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Fsipv6PrefixProfileIndex ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

INT1
nmhTestv2Fsipv6SupportEmbeddedRp ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

INT1
nmhTestv2Fsipv6PrefixAdminStatus ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Fsipv6PrefixTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Fsipv6AddrTable. */
INT1
nmhValidateIndexInstanceFsipv6AddrTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ));

/* Proto Type for Low Level GET FIRST fn for Fsipv6AddrTable  */

INT1
nmhGetFirstIndexFsipv6AddrTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE *  , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsipv6AddrTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsipv6AddrAdminStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

INT1
nmhGetFsipv6AddrType ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

INT1
nmhGetFsipv6AddrProfIndex ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

INT1
nmhGetFsipv6AddrOperStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

INT1
nmhGetFsipv6AddrScope ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

INT1
nmhGetFsipv6AddrSENDCgaStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

INT1
nmhGetFsipv6AddrSENDCgaModifier ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsipv6AddrSENDCollisionCount ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsipv6AddrAdminStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

INT1
nmhSetFsipv6AddrType ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

INT1
nmhSetFsipv6AddrProfIndex ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

INT1
nmhSetFsipv6AddrSENDCgaStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

INT1
nmhSetFsipv6AddrSENDCgaModifier ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsipv6AddrSENDCollisionCount ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Fsipv6AddrAdminStatus ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

INT1
nmhTestv2Fsipv6AddrType ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

INT1
nmhTestv2Fsipv6AddrProfIndex ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

INT1
nmhTestv2Fsipv6AddrSENDCgaStatus ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

INT1
nmhTestv2Fsipv6AddrSENDCgaModifier ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Fsipv6AddrSENDCollisionCount ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Fsipv6AddrTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Fsipv6AddrProfileTable. */
INT1
nmhValidateIndexInstanceFsipv6AddrProfileTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Fsipv6AddrProfileTable  */

INT1
nmhGetFirstIndexFsipv6AddrProfileTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsipv6AddrProfileTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsipv6AddrProfileStatus ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsipv6AddrProfilePrefixAdvStatus ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsipv6AddrProfileOnLinkAdvStatus ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsipv6AddrProfileAutoConfAdvStatus ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsipv6AddrProfilePreferredTime ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsipv6AddrProfileValidTime ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsipv6AddrProfileValidLifeTimeFlag ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsipv6AddrProfilePreferredLifeTimeFlag ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsipv6AddrProfileStatus ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsipv6AddrProfilePrefixAdvStatus ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsipv6AddrProfileOnLinkAdvStatus ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsipv6AddrProfileAutoConfAdvStatus ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsipv6AddrProfilePreferredTime ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsipv6AddrProfileValidTime ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsipv6AddrProfileValidLifeTimeFlag ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsipv6AddrProfilePreferredLifeTimeFlag ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Fsipv6AddrProfileStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Fsipv6AddrProfilePrefixAdvStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Fsipv6AddrProfileOnLinkAdvStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Fsipv6AddrProfileAutoConfAdvStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Fsipv6AddrProfilePreferredTime ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Fsipv6AddrProfileValidTime ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Fsipv6AddrProfileValidLifeTimeFlag ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Fsipv6AddrProfilePreferredLifeTimeFlag ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Fsipv6AddrProfileTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Fsipv6PmtuTable. */
INT1
nmhValidateIndexInstanceFsipv6PmtuTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for Fsipv6PmtuTable  */

INT1
nmhGetFirstIndexFsipv6PmtuTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsipv6PmtuTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsipv6Pmtu ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsipv6PmtuTimeStamp ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsipv6PmtuAdminStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsipv6Pmtu ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsipv6PmtuAdminStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Fsipv6Pmtu ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2Fsipv6PmtuAdminStatus ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Fsipv6PmtuTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Fsipv6NdLanCacheTable. */
INT1
nmhValidateIndexInstanceFsipv6NdLanCacheTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for Fsipv6NdLanCacheTable  */

INT1
nmhGetFirstIndexFsipv6NdLanCacheTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsipv6NdLanCacheTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsipv6NdLanCachePhysAddr ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsipv6NdLanCacheStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsipv6NdLanCacheState ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsipv6NdLanCacheUseTime ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetFsipv6NdLanCacheIsSecure ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsipv6NdLanCachePhysAddr ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsipv6NdLanCacheStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Fsipv6NdLanCachePhysAddr ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Fsipv6NdLanCacheStatus ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Fsipv6NdLanCacheTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Fsipv6NdWanCacheTable. */
INT1
nmhValidateIndexInstanceFsipv6NdWanCacheTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for Fsipv6NdWanCacheTable  */

INT1
nmhGetFirstIndexFsipv6NdWanCacheTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsipv6NdWanCacheTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsipv6NdWanCacheStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsipv6NdWanCacheState ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsipv6NdWanCacheUseTime ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsipv6NdWanCacheStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Fsipv6NdWanCacheStatus ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Fsipv6NdWanCacheTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Fsipv6PingTable. */
INT1
nmhValidateIndexInstanceFsipv6PingTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for Fsipv6PingTable  */

INT1
nmhGetFirstIndexFsipv6PingTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsipv6PingTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsipv6PingDest ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsipv6PingIfIndex ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsipv6PingAdminStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsipv6PingInterval ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsipv6PingRcvTimeout ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsipv6PingTries ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsipv6PingSize ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsipv6PingSentCount ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsipv6PingAverageTime ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsipv6PingMaxTime ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsipv6PingMinTime ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsipv6PingOperStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsipv6PingSuccesses ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsipv6PingPercentageLoss ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsipv6PingData ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsipv6PingSrcAddr ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsipv6PingZoneId ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsipv6PingDestAddrType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsipv6PingHostName ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsipv6PingDest ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsipv6PingIfIndex ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsipv6PingAdminStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsipv6PingInterval ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsipv6PingRcvTimeout ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsipv6PingTries ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsipv6PingSize ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsipv6PingData ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsipv6PingSrcAddr ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsipv6PingZoneId ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsipv6PingDestAddrType ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsipv6PingHostName ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Fsipv6PingDest ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Fsipv6PingIfIndex ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Fsipv6PingAdminStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Fsipv6PingInterval ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Fsipv6PingRcvTimeout ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Fsipv6PingTries ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Fsipv6PingSize ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Fsipv6PingData ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Fsipv6PingSrcAddr ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Fsipv6PingZoneId ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Fsipv6PingDestAddrType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Fsipv6PingHostName ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Fsipv6PingTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Fsipv6NDProxyListTable. */
INT1
nmhValidateIndexInstanceFsipv6NDProxyListTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for Fsipv6NDProxyListTable  */

INT1
nmhGetFirstIndexFsipv6NDProxyListTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsipv6NDProxyListTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsipv6NdProxyAdminStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsipv6NdProxyAdminStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Fsipv6NdProxyAdminStatus ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Fsipv6NDProxyListTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Fsipv6AddrSelPolicyTable. */
INT1
nmhValidateIndexInstanceFsipv6AddrSelPolicyTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for Fsipv6AddrSelPolicyTable  */

INT1
nmhGetFirstIndexFsipv6AddrSelPolicyTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *  , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsipv6AddrSelPolicyTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsipv6AddrSelPolicyScope ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsipv6AddrSelPolicyPrecedence ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsipv6AddrSelPolicyLabel ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsipv6AddrSelPolicyAddrType ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsipv6AddrSelPolicyIsPublicAddr ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsipv6AddrSelPolicyIsSelfAddr ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsipv6AddrSelPolicyReachabilityStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsipv6AddrSelPolicyConfigStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsipv6AddrSelPolicyRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsipv6AddrSelPolicyPrecedence ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  ,INT4 ));

INT1
nmhSetFsipv6AddrSelPolicyLabel ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  ,INT4 ));

INT1
nmhSetFsipv6AddrSelPolicyAddrType ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  ,INT4 ));

INT1
nmhSetFsipv6AddrSelPolicyRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Fsipv6AddrSelPolicyPrecedence ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2Fsipv6AddrSelPolicyLabel ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2Fsipv6AddrSelPolicyAddrType ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2Fsipv6AddrSelPolicyRowStatus ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Fsipv6AddrSelPolicyTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));


/* Proto Validate Index Instance for Fsipv6IfScopeZoneMapTable. */
INT1
nmhValidateIndexInstanceFsipv6IfScopeZoneMapTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for Fsipv6IfScopeZoneMapTable  */

INT1
nmhGetFirstIndexFsipv6IfScopeZoneMapTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsipv6IfScopeZoneMapTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsipv6ScopeZoneIndexInterfaceLocal ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsipv6ScopeZoneIndexLinkLocal ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsipv6ScopeZoneIndex3 ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsipv6ScopeZoneIndexAdminLocal ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsipv6ScopeZoneIndexSiteLocal ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsipv6ScopeZoneIndex6 ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsipv6ScopeZoneIndex7 ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsipv6ScopeZoneIndexOrganizationLocal ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsipv6ScopeZoneIndex9 ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsipv6ScopeZoneIndexA ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsipv6ScopeZoneIndexB ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsipv6ScopeZoneIndexC ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsipv6ScopeZoneIndexD ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsipv6ScopeZoneIndexE ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsipv6IfScopeZoneCreationStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsipv6IfScopeZoneRowStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsipv6ScopeZoneIndexInterfaceLocal ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsipv6ScopeZoneIndexLinkLocal ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsipv6ScopeZoneIndex3 ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsipv6ScopeZoneIndexAdminLocal ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsipv6ScopeZoneIndexSiteLocal ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsipv6ScopeZoneIndex6 ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsipv6ScopeZoneIndex7 ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsipv6ScopeZoneIndexOrganizationLocal ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsipv6ScopeZoneIndex9 ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsipv6ScopeZoneIndexA ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsipv6ScopeZoneIndexB ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsipv6ScopeZoneIndexC ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsipv6ScopeZoneIndexD ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsipv6ScopeZoneIndexE ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsipv6IfScopeZoneRowStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Fsipv6ScopeZoneIndexInterfaceLocal ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Fsipv6ScopeZoneIndexLinkLocal ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Fsipv6ScopeZoneIndex3 ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Fsipv6ScopeZoneIndexAdminLocal ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Fsipv6ScopeZoneIndexSiteLocal ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Fsipv6ScopeZoneIndex6 ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Fsipv6ScopeZoneIndex7 ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Fsipv6ScopeZoneIndexOrganizationLocal ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Fsipv6ScopeZoneIndex9 ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Fsipv6ScopeZoneIndexA ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Fsipv6ScopeZoneIndexB ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Fsipv6ScopeZoneIndexC ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Fsipv6ScopeZoneIndexD ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Fsipv6ScopeZoneIndexE ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Fsipv6IfScopeZoneRowStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Fsipv6IfScopeZoneMapTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Fsipv6ScopeZoneTable. */
INT1
nmhValidateIndexInstanceFsipv6ScopeZoneTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for Fsipv6ScopeZoneTable  */

INT1
nmhGetFirstIndexFsipv6ScopeZoneTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsipv6ScopeZoneTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsipv6ScopeZoneIndex ARG_LIST((tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetFsipv6ScopeZoneCreationStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsipv6ScopeZoneInterfaceList ARG_LIST((tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsipv6IsDefaultScopeZone ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsipv6IsDefaultScopeZone ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Fsipv6ScopeZoneCreationStatus ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2Fsipv6IsDefaultScopeZone ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Fsipv6ScopeZoneTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Fsipv6SENDSentPktStatsTable. */
INT1
nmhValidateIndexInstanceFsipv6SENDSentPktStatsTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for Fsipv6SENDSentPktStatsTable  */

INT1
nmhGetFirstIndexFsipv6SENDSentPktStatsTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsipv6SENDSentPktStatsTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsipv6SENDSentCgaOptPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsipv6SENDSentCertOptPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsipv6SENDSentMtuOptPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsipv6SENDSentNonceOptPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsipv6SENDSentPrefixOptPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsipv6SENDSentRedirHdrPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsipv6SENDSentRsaOptPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsipv6SENDSentSrcLinkAddrPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsipv6SENDSentTgtLinkAddrPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsipv6SENDSentTaOptPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsipv6SENDSentTimeStampOptPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

/* Proto Validate Index Instance for Fsipv6SENDRcvPktStatsTable. */
INT1
nmhValidateIndexInstanceFsipv6SENDRcvPktStatsTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for Fsipv6SENDRcvPktStatsTable  */

INT1
nmhGetFirstIndexFsipv6SENDRcvPktStatsTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsipv6SENDRcvPktStatsTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsipv6SENDRcvCgaOptPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsipv6SENDRcvCertOptPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsipv6SENDRcvMtuOptPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsipv6SENDRcvNonceOptPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsipv6SENDRcvPrefixOptPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsipv6SENDRcvRedirHdrPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsipv6SENDRcvRsaOptPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsipv6SENDRcvSrcLinkAddrPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsipv6SENDRcvTgtLinkAddrPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsipv6SENDRcvTaOptPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsipv6SENDRcvTimeStampOptPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

/* Proto Validate Index Instance for Fsipv6SENDDrpPktStatsTable. */
INT1
nmhValidateIndexInstanceFsipv6SENDDrpPktStatsTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for Fsipv6SENDDrpPktStatsTable  */

INT1
nmhGetFirstIndexFsipv6SENDDrpPktStatsTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsipv6SENDDrpPktStatsTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsipv6SENDDrpCgaOptPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsipv6SENDDrpCertOptPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsipv6SENDDrpMtuOptPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsipv6SENDDrpNonceOptPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsipv6SENDDrpPrefixOptPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsipv6SENDDrpRedirHdrPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsipv6SENDDrpRsaOptPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsipv6SENDDrpSrcLinkAddrPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsipv6SENDDrpTgtLinkAddrPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsipv6SENDDrpTaOptPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsipv6SENDDrpTimeStampOptPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

/* Proto Validate Index Instance for Fsipv6RARouteInfoTable. */
INT1
nmhValidateIndexInstanceFsipv6RARouteInfoTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ));

/* Proto Type for Low Level GET FIRST fn for Fsipv6RARouteInfoTable  */

INT1
nmhGetFirstIndexFsipv6RARouteInfoTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE *  , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsipv6RARouteInfoTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsipv6RARoutePreference ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

INT1
nmhGetFsipv6RARouteLifetime ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,UINT4 *));

INT1
nmhGetFsipv6RARouteRowStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsipv6RARoutePreference ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

INT1
nmhSetFsipv6RARouteLifetime ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,UINT4 ));

INT1
nmhSetFsipv6RARouteRowStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Fsipv6RARoutePreference ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

INT1
nmhTestv2Fsipv6RARouteLifetime ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,UINT4 ));

INT1
nmhTestv2Fsipv6RARouteRowStatus ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Fsipv6RARouteInfoTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsipv6IcmpInMsgs ARG_LIST((UINT4 *));

INT1
nmhGetFsipv6IcmpInErrors ARG_LIST((UINT4 *));

INT1
nmhGetFsipv6IcmpInDestUnreachs ARG_LIST((UINT4 *));

INT1
nmhGetFsipv6IcmpInTimeExcds ARG_LIST((UINT4 *));

INT1
nmhGetFsipv6IcmpInParmProbs ARG_LIST((UINT4 *));

INT1
nmhGetFsipv6IcmpInPktTooBigs ARG_LIST((UINT4 *));

INT1
nmhGetFsipv6IcmpInEchos ARG_LIST((UINT4 *));

INT1
nmhGetFsipv6IcmpInEchoReps ARG_LIST((UINT4 *));

INT1
nmhGetFsipv6IcmpInRouterSolicits ARG_LIST((UINT4 *));

INT1
nmhGetFsipv6IcmpInRouterAdvertisements ARG_LIST((UINT4 *));

INT1
nmhGetFsipv6IcmpInNeighborSolicits ARG_LIST((UINT4 *));

INT1
nmhGetFsipv6IcmpInNeighborAdvertisements ARG_LIST((UINT4 *));

INT1
nmhGetFsipv6IcmpInRedirects ARG_LIST((UINT4 *));

INT1
nmhGetFsipv6IcmpInAdminProhib ARG_LIST((UINT4 *));

INT1
nmhGetFsipv6IcmpOutMsgs ARG_LIST((UINT4 *));

INT1
nmhGetFsipv6IcmpOutErrors ARG_LIST((UINT4 *));

INT1
nmhGetFsipv6IcmpOutDestUnreachs ARG_LIST((UINT4 *));

INT1
nmhGetFsipv6IcmpOutTimeExcds ARG_LIST((UINT4 *));

INT1
nmhGetFsipv6IcmpOutParmProbs ARG_LIST((UINT4 *));

INT1
nmhGetFsipv6IcmpOutPktTooBigs ARG_LIST((UINT4 *));

INT1
nmhGetFsipv6IcmpOutEchos ARG_LIST((UINT4 *));

INT1
nmhGetFsipv6IcmpOutEchoReps ARG_LIST((UINT4 *));

INT1
nmhGetFsipv6IcmpOutRouterSolicits ARG_LIST((UINT4 *));

INT1
nmhGetFsipv6IcmpOutRouterAdvertisements ARG_LIST((UINT4 *));

INT1
nmhGetFsipv6IcmpOutNeighborSolicits ARG_LIST((UINT4 *));

INT1
nmhGetFsipv6IcmpOutNeighborAdvertisements ARG_LIST((UINT4 *));

INT1
nmhGetFsipv6IcmpOutRedirects ARG_LIST((UINT4 *));

INT1
nmhGetFsipv6IcmpOutAdminProhib ARG_LIST((UINT4 *));

INT1
nmhGetFsipv6IcmpInBadCode ARG_LIST((UINT4 *));

INT1
nmhGetFsipv6IcmpInNARouterFlagSet ARG_LIST((UINT4 *));

INT1
nmhGetFsipv6IcmpInNASolicitedFlagSet ARG_LIST((UINT4 *));

INT1
nmhGetFsipv6IcmpInNAOverrideFlagSet ARG_LIST((UINT4 *));

INT1
nmhGetFsipv6IcmpOutNARouterFlagSet ARG_LIST((UINT4 *));

INT1
nmhGetFsipv6IcmpOutNASolicitedFlagSet ARG_LIST((UINT4 *));

INT1
nmhGetFsipv6IcmpOutNAOverrideFlagSet ARG_LIST((UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsipv6UdpInDatagrams ARG_LIST((UINT4 *));

INT1
nmhGetFsipv6UdpNoPorts ARG_LIST((UINT4 *));

INT1
nmhGetFsipv6UdpInErrors ARG_LIST((UINT4 *));

INT1
nmhGetFsipv6UdpOutDatagrams ARG_LIST((UINT4 *));

/* Proto Validate Index Instance for Fsipv6RouteTable. */
INT1
nmhValidateIndexInstanceFsipv6RouteTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for Fsipv6RouteTable  */

INT1
nmhGetFirstIndexFsipv6RouteTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *  , INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsipv6RouteTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsipv6RouteIfIndex ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsipv6RouteMetric ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetFsipv6RouteType ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsipv6RouteTag ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetFsipv6RouteAge ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsipv6RouteAdminStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsipv6RouteAddrType ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsipv6RouteHwStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsipv6RoutePreference ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsipv6RouteIfIndex ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsipv6RouteMetric ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

INT1
nmhSetFsipv6RouteType ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsipv6RouteTag ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

INT1
nmhSetFsipv6RouteAdminStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsipv6RouteAddrType ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsipv6RoutePreference ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));


/* Low Level TEST Routines for.  */

INT1
nmhTestv2Fsipv6RouteIfIndex ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2Fsipv6RouteMetric ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

INT1
nmhTestv2Fsipv6RouteType ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2Fsipv6RouteTag ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

INT1
nmhTestv2Fsipv6RouteAdminStatus ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2Fsipv6RouteAddrType ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2Fsipv6RoutePreference ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Fsipv6RouteTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Fsipv6PrefTable. */
INT1
nmhValidateIndexInstanceFsipv6PrefTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for Fsipv6PrefTable  */

INT1
nmhGetFirstIndexFsipv6PrefTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsipv6PrefTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsipv6Preference ARG_LIST((INT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsipv6Preference ARG_LIST((INT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Fsipv6Preference ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Fsipv6PrefTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
/* Proto type for Low Level GET Routine All Objects.  */
INT1
nmhGetFsIpv6TestRedEntryTime ARG_LIST((INT4 *));
INT1
nmhGetFsIpv6TestRedExitTime ARG_LIST((INT4 *));

/* Proto Validate Index Instance for Fsipv6IfRaRDNSSTable. */
INT1
nmhValidateIndexInstanceFsipv6IfRaRDNSSTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for Fsipv6IfRaRDNSSTable  */

INT1
nmhGetFirstIndexFsipv6IfRaRDNSSTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsipv6IfRaRDNSSTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsipv6IfRadvRDNSSOpen ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsipv6IfRaRDNSSPreference ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsipv6IfRaRDNSSLifetime ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsipv6IfRaRDNSSAddrOne ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsipv6IfRaRDNSSAddrOneLife ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsipv6IfRaRDNSSAddrTwo ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsipv6IfRaRDNSSAddrTwoLife ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsipv6IfRaRDNSSAddrThree ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsipv6IfRaRDNSSAddrThreeLife ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsipv6IfRaRDNSSRowStatus ARG_LIST((INT4 ,INT4 *));


INT1
nmhGetFsipv6IfDomainNameOne ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsipv6IfDomainNameTwo ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsipv6IfDomainNameThree ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsipv6IfDnsLifeTime ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsipv6IfDomainNameOneLife ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsipv6IfDomainNameTwoLife ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsipv6IfDomainNameThreeLife ARG_LIST((INT4 ,INT4 *));


/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsipv6IfRadvRDNSSOpen ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsipv6IfRaRDNSSPreference ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsipv6IfRaRDNSSLifetime ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsipv6IfRaRDNSSAddrOne ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsipv6IfRaRDNSSAddrOneLife ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsipv6IfRaRDNSSAddrTwo ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsipv6IfRaRDNSSAddrThree ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsipv6IfRaRDNSSAddrTwoLife ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsipv6IfRaRDNSSAddrThreeLife ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsipv6IfRaRDNSSRowStatus ARG_LIST((INT4  ,INT4 ));


INT1
nmhSetFsipv6IfDomainNameOne ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsipv6IfDomainNameTwo ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsipv6IfDomainNameThree ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsipv6IfDnsLifeTime ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsipv6IfDomainNameOneLife ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsipv6IfDomainNameTwoLife ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsipv6IfDomainNameThreeLife ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Fsipv6IfRadvRDNSSOpen ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Fsipv6IfRaRDNSSPreference ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Fsipv6IfRaRDNSSLifetime ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Fsipv6IfRaRDNSSAddrOne ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Fsipv6IfRaRDNSSAddrOneLife ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Fsipv6IfRaRDNSSAddrTwo ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Fsipv6IfRaRDNSSAddrTwoLife ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Fsipv6IfRaRDNSSAddrThree ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Fsipv6IfRaRDNSSAddrThreeLife ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Fsipv6IfRaRDNSSRowStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));


INT1
nmhTestv2Fsipv6IfDomainNameOne ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Fsipv6IfDomainNameTwo ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Fsipv6IfDomainNameThree ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Fsipv6IfDnsLifeTime ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Fsipv6IfDomainNameOneLife ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Fsipv6IfDomainNameTwoLife ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Fsipv6IfDomainNameThreeLife ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Fsipv6IfRaRDNSSTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
