/*******************************************************************
 ** Copyright (C) 2006 Aricent Inc . All Rights Reserved
 **
 ** $Id: ip6ipvx.h,v 1.5 2014/11/28 02:11:47 siva Exp $
 **
 ** Description: This file contains IPv6 information
 **
 ********************************************************************/

#ifndef __IP6_IPVX_H__
#define __IP6_IPVX_H__

/* OSPF route types */

#define IP6SYS_INC_IN_RECEVES(Ip6SysStats) \
if ((Ip6SysStats.u4InRcvs + 1) < Ip6SysStats.u4InRcvs) \
{ \
    (Ip6SysStats.u4HCInReceives++); \
} \
(Ip6SysStats.u4InRcvs++)

#define IP6IF_INC_IN_RECEVES(pIP6If) \
if (pIP6If != NULL) \
{ \
    if ((pIP6If->stats.u4InRcvs + 1) < pIP6If->stats.u4InRcvs) \
    { \
        (pIP6If->stats.u4HCInReceives++); \
    } \
    (pIP6If->stats.u4InRcvs++); \
    IPvxUpdateIfStatsTblLstChange (); \
}

#define IP6SYS_INC_IN_OCTETS(Ip6SysStats, u4Oct) \
if ((Ip6SysStats.u4InOctets + u4Oct) < Ip6SysStats.u4InOctets) \
{ \
    (Ip6SysStats.u4HCInOctets++); \
} \
(Ip6SysStats.u4InOctets += u4Oct)

#define IP6IF_INC_IN_OCTETS(pIP6If, u4Oct) \
if (pIP6If != NULL) \
{ \
    if ((pIP6If->stats.u4InOctets + u4Oct) < pIP6If->stats.u4InOctets) \
    { \
        (pIP6If->stats.u4HCInOctets++); \
    } \
    (pIP6If->stats.u4InOctets += u4Oct); \
    IPvxUpdateIfStatsTblLstChange (); \
} 

#define IP6SYS_INC_IN_HDR_ERR(Ip6SysStats)  (Ip6SysStats.u4InHdrerrs++)

#define IP6IF_INC_IN_HDR_ERR(pIP6If) \
if (pIP6If != NULL) \
{ \
    (pIP6If->stats.u4InHdrerrs++); \
    IPvxUpdateIfStatsTblLstChange (); \
}

#define IP6SYS_INC_IN_NO_ROUTE(Ip6SysStats)  (Ip6SysStats.u4InNorts++)
#define IP6IF_INC_IN_NO_ROUTE(pIP6If) \
    if (pIP6If != NULL) \
{ \
    (pIP6If->stats.u4InNorts++); \
    IPvxUpdateIfStatsTblLstChange (); \
}

#define IP6SYS_INC_IN_ADDR_ERR(Ip6SysStats)  (Ip6SysStats.u4InAddrerrs++)

#define IP6IF_INC_IN_ADDR_ERR(pIP6If) \
if (pIP6If != NULL) \
{ \
    (pIP6If->stats.u4InAddrerrs++); \
    IPvxUpdateIfStatsTblLstChange (); \
}

#define IP6SYS_INC_IN_UNKNOWN_PROTOS(Ip6SysStats)  (Ip6SysStats.u4InUnkprots++)

#define IP6IF_INC_IN_UNKNOWN_PROTOS(pIP6If) \
if (pIP6If != NULL) \
{ \
    (pIP6If->stats.u4InUnkprots++); \
    IPvxUpdateIfStatsTblLstChange (); \
}

#define IP6SYS_INC_IN_TRUNCATED(Ip6SysStats)  (Ip6SysStats.u4InTruncs++)

#define IP6IF_INC_IN_TRUNCATED(pIP6If) \
if (pIP6If != NULL) \
{ \
    (pIP6If->stats.u4InTruncs++); \
    IPvxUpdateIfStatsTblLstChange (); \
}

#define IP6SYS_INC_IN_FORW_DGRAMS(Ip6SysStats) \
if ((Ip6SysStats.u4ForwDgrams + 1) < Ip6SysStats.u4ForwDgrams) \
{ \
    (Ip6SysStats.u4HCInForwDgrams++); \
} \
(Ip6SysStats.u4ForwDgrams++)

#define IP6IF_INC_IN_FORW_DGRAMS(pIP6If) \
if (pIP6If != NULL) \
{ \
    if ((pIP6If->stats.u4ForwDgrams + 1) < pIP6If->stats.u4ForwDgrams) \
    { \
        (pIP6If->stats.u4HCInForwDgrams++); \
    } \
    (pIP6If->stats.u4ForwDgrams++); \
    IPvxUpdateIfStatsTblLstChange (); \
}

#define IP6SYS_INC_REASM_REQDS(Ip6SysStats)  (Ip6SysStats.u4Reasmreqs++)

#define IP6IF_INC_REASM_REQDS(pIP6If) \
if (pIP6If != NULL) \
{ \
    (pIP6If->stats.u4Reasmreqs++); \
    IPvxUpdateIfStatsTblLstChange (); \
}

#define IP6SYS_INC_REASM_OKS(Ip6SysStats)  (Ip6SysStats.u4Reasmoks++)

#define IP6IF_INC_REASM_OKS(pIP6If) \
if (pIP6If != NULL) \
{ \
    (pIP6If->stats.u4Reasmoks++); \
    IPvxUpdateIfStatsTblLstChange (); \
}

#define IP6SYS_INC_REASM_FAILS(Ip6SysStats)  (Ip6SysStats.u4Reasmfails++)

#define IP6IF_INC_REASM_FAILS(pIP6If) \
if (pIP6If != NULL) \
{ \
    (pIP6If->stats.u4Reasmfails++); \
    IPvxUpdateIfStatsTblLstChange (); \
}

#define IP6SYS_INC_IN_DISCARDS(Ip6SysStats)  (Ip6SysStats.u4InDiscards++)

#define IP6IF_INC_IN_DISCARDS(pIP6If) \
if (pIP6If != NULL) \
{ \
    (pIP6If->stats.u4InDiscards++); \
    IPvxUpdateIfStatsTblLstChange (); \
}

#define IP6SYS_INC_IN_DELIVERS(Ip6SysStats) \
if ((Ip6SysStats.u4InDelivers + 1) < Ip6SysStats.u4InDelivers) \
{ \
    (Ip6SysStats.u4HCInDelivers++); \
} \
(Ip6SysStats.u4InDelivers++)

#define IP6IF_INC_IN_DELIVERS(pIP6If) \
if (pIP6If != NULL) \
{ \
    if ((pIP6If->stats.u4InDelivers+ 1) < pIP6If->stats.u4InDelivers) \
    { \
        (pIP6If->stats.u4HCInDelivers++); \
    } \
    (pIP6If->stats.u4InDelivers++); \
    IPvxUpdateIfStatsTblLstChange (); \
} 

#define IP6SYS_INC_OUT_REQS(Ip6SysStats) \
if ((Ip6SysStats.u4OutReqs + 1) < Ip6SysStats.u4OutReqs) \
{ \
    (Ip6SysStats.u4HCOutRequests++); \
} \
(Ip6SysStats.u4OutReqs++)

#define IP6IF_INC_OUT_REQS(pIP6If) \
if (pIP6If != NULL) \
{ \
    if ((pIP6If->stats.u4OutReqs + 1) < pIP6If->stats.u4OutReqs) \
    { \
        (pIP6If->stats.u4HCOutRequests++); \
    } \
    (pIP6If->stats.u4OutReqs++); \
    IPvxUpdateIfStatsTblLstChange (); \
}

#define IP6SYS_INC_OUT_NO_ROUTE(Ip6SysStats)  (Ip6SysStats.u4OutNorts++)

#define IP6IF_INC_OUT_NO_ROUTE(pIP6If) \
if (pIP6If != NULL) \
{ \
    (pIP6If->stats.u4OutNorts++); \
    IPvxUpdateIfStatsTblLstChange (); \
}

#define IP6SYS_INC_OUT_FWD_DGRAMS(Ip6SysStats) \
if ((Ip6SysStats.u4OutForwds + 1) < Ip6SysStats.u4OutForwds) \
{ \
    (Ip6SysStats.u4HCOutForwds++); \
} \
(Ip6SysStats.u4OutForwds++)

#define IP6IF_INC_OUT_FWD_DGRAMS(pIP6If) \
if (pIP6If != NULL) \
{ \
    if ((pIP6If->stats.u4OutForwds+ 1) < pIP6If->stats.u4OutForwds) \
    { \
        (pIP6If->stats.u4HCOutForwds++); \
    } \
    (pIP6If->stats.u4OutForwds++); \
    IPvxUpdateIfStatsTblLstChange (); \
}

#define IP6SYS_INC_OUT_DISCARDS(Ip6SysStats)  (Ip6SysStats.u4OutDiscards++)

#define IP6IF_INC_OUT_DISCARDS(pIP6If) \
if (pIP6If != NULL) \
{ \
    (pIP6If->stats.u4OutDiscards++); \
    IPvxUpdateIfStatsTblLstChange (); \
}

#define IP6SYS_INC_OUT_FRAG_REQS(Ip6SysStats)  (Ip6SysStats.u4OutFragReqds++)

#define IP6IF_INC_OUT_FRAG_REQS(pIP6If) \
if (pIP6If != NULL) \
{ \
    (pIP6If->stats.u4OutFragReqds++); \
    IPvxUpdateIfStatsTblLstChange (); \
}

#define IP6SYS_INC_OUT_FRAG_OKS(Ip6SysStats)  (Ip6SysStats.u4Fragoks++)

#define IP6IF_INC_OUT_FRAG_OKS(pIP6If) \
if (pIP6If != NULL) \
{ \
    (pIP6If->stats.u4Fragoks++); \
    IPvxUpdateIfStatsTblLstChange (); \
} 

#define IP6SYS_INC_OUT_FRAG_FAILS(Ip6SysStats)  (Ip6SysStats.u4Fragfails++)

#define IP6IF_INC_OUT_FRAG_FAILS(pIP6If) \
if (pIP6If != NULL) \
{ \
    (pIP6If->stats.u4Fragfails++); \
    IPvxUpdateIfStatsTblLstChange (); \
}

#define IP6SYS_INC_OUT_FRAG_CREATES(Ip6SysStats)  (Ip6SysStats.u4Fragcreates++)

#define IP6IF_INC_OUT_FRAG_CREATES(pIP6If) \
if (pIP6If != NULL) \
{ \
    (pIP6If->stats.u4Fragcreates++); \
    IPvxUpdateIfStatsTblLstChange (); \
}

#define IP6SYS_INC_OUT_TRANSMITS(Ip6SysStats) \
if ((Ip6SysStats.u4OutTrans + 1) < Ip6SysStats.u4OutTrans) \
{ \
    (Ip6SysStats.u4HCOutTrans++); \
} \
(Ip6SysStats.u4OutTrans++)

#define IP6IF_INC_OUT_TRANSMITS(pIP6If) \
if (pIP6If != NULL) \
{ \
    if ((pIP6If->stats.u4OutTrans+ 1) < pIP6If->stats.u4OutTrans) \
    { \
        (pIP6If->stats.u4HCOutTrans++); \
    } \
    (pIP6If->stats.u4OutTrans++); \
    IPvxUpdateIfStatsTblLstChange (); \
} 

#define IP6SYS_INC_OUT_OCTETS(Ip6SysStats, u4Oct) \
if ((Ip6SysStats.u4OutOctets + u4Oct) < Ip6SysStats.u4OutOctets) \
{ \
    (Ip6SysStats.u4HCOutOctets++); \
} \
(Ip6SysStats.u4OutOctets += u4Oct)

#define IP6IF_INC_OUT_OCTETS(pIP6If,u4Oct) \
if (pIP6If != NULL) \
{ \
    if ((pIP6If->stats.u4OutOctets + u4Oct) < pIP6If->stats.u4OutOctets) \
    { \
        (pIP6If->stats.u4HCOutOctets++); \
    } \
    (pIP6If->stats.u4OutOctets += u4Oct); \
    IPvxUpdateIfStatsTblLstChange (); \
} 

#define IP6SYS_INC_IN_MCAST_PKT(Ip6SysStats) \
if ((Ip6SysStats.u4InMcasts + 1) < Ip6SysStats.u4InMcasts) \
{ \
    (Ip6SysStats.u4InMcasts++); \
} \
(Ip6SysStats.u4InMcasts++)

#define IP6IF_INC_IN_MCAST_PKT(pIP6If) \
if (pIP6If != NULL) \
{ \
    if ((pIP6If->stats.u4InMcasts+ 1) < pIP6If->stats.u4InMcasts) \
    { \
        (pIP6If->stats.u4InMcasts++); \
    } \
    (pIP6If->stats.u4InMcasts++); \
    IPvxUpdateIfStatsTblLstChange (); \
} 

#define IP6SYS_INC_IN_MCAST_OCTETS(Ip6SysStats, u4Oct) \
if ((Ip6SysStats.u4InMcastOctets + u4Oct) < Ip6SysStats.u4InMcastOctets) \
{ \
    (Ip6SysStats.u4HCInMcastOctets++); \
} \
(Ip6SysStats.u4InMcastOctets += u4Oct)

#define IP6IF_INC_IN_MCAST_OCTETS(pIP6If, u4Oct) \
if (pIP6If != NULL) \
{ \
    if ((pIP6If->stats.u4InMcastOctets + u4Oct) < pIP6If->stats.u4InMcastOctets) \
    { \
        (pIP6If->stats.u4HCInMcastOctets++); \
    } \
    (pIP6If->stats.u4InMcastOctets += u4Oct); \
    IPvxUpdateIfStatsTblLstChange (); \
} 

#define IP6SYS_INC_OUT_MCAST_PKT(Ip6SysStats) \
if ((Ip6SysStats.u4OutMcasts + 1) < Ip6SysStats.u4OutMcasts) \
{ \
    (Ip6SysStats.u4HCOutMcastPkts++); \
} \
(Ip6SysStats.u4OutMcasts++)

#define IP6IF_INC_OUT_MCAST_PKT(pIP6If) \
if (pIP6If != NULL) \
{ \
    if ((pIP6If->stats.u4OutMcasts + 1) < pIP6If->stats.u4OutMcasts) \
    { \
        (pIP6If->stats.u4HCOutMcastPkts++); \
    } \
    (pIP6If->stats.u4OutMcasts++); \
    IPvxUpdateIfStatsTblLstChange (); \
}

#define IP6SYS_INC_OUT_MCAST_OCTETS(Ip6SysStats, u4Oct) \
if ((Ip6SysStats.u4OutMcastOctets + u4Oct) < Ip6SysStats.u4OutMcastOctets) \
{ \
    (Ip6SysStats.u4HCOutMcastOctets++); \
} \
(Ip6SysStats.u4OutMcastOctets += u4Oct)

#define IP6IF_INC_OUT_MCAST_OCTETS(pIP6If, u4Oct) \
if (pIP6If != NULL) \
{ \
    if ((pIP6If->stats.u4OutMcastOctets + u4Oct) < \
        pIP6If->stats.u4OutMcastOctets) \
    { \
        (pIP6If->stats.u4HCOutMcastOctets++); \
    } \
    (pIP6If->stats.u4OutMcastOctets += u4Oct); \
    IPvxUpdateIfStatsTblLstChange (); \
} 

#endif /* __IP6_IPVX_H__ */

/****************************   End of the File ***************************/

