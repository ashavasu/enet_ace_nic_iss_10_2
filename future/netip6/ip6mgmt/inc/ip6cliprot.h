/*******************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ip6cliprot.h,v 1.28 2015/11/13 12:05:08 siva Exp $
 *
 * Description: Cli related functions are handled here.
 *
 *******************************************************************/

VOID Ip6SetRoutingStatus PROTO ((tCliHandle CliHandle,
                                  INT1 i1Status));

VOID Ip6CliSetIfaceRoutingStatus PROTO ((tCliHandle CliHandle, INT4 i4IfIndex,
                                  INT1 i1Status));

INT1 Ip6SetInterfaceAdminStatus PROTO ((tCliHandle CliHandle,
                                        UINT4 u4IfIndex, INT4 i4Status));

VOID Ip6SetAddr PROTO ((tCliHandle CliHandle, UINT4 u4IfIndex, 
                        tIp6Addr Ip6Addr, INT4 i4PrefixLen,
                        INT4 i4AddrType));

VOID Ip6PingInCxt PROTO ((tCliHandle CliHandle, UINT4 u4IfIndex, UINT4 u4VcId,
                          tIp6Addr Ip6Addr, UINT1 *pu1PingData, INT4 i4PingCount,
                          INT4 i4PingSize, tIp6Addr PingSrcAddr,
                          INT4 i4PingTimeout, INT4 i4AddrType,UINT1 *pPingZoneId,
                          UINT1 *au1HostName));


VOID Ip6IfShowInCxt PROTO ((tCliHandle CliHandle, UINT4 u4IfIndex, UINT4 u4VcId));
/*
VOID Ip6EnableTcpEchoSrv PROTO ((tIp6CliConfigParams *pu1Ip6Input,
                                 UINT1 **ppu1OutMsg));

VOID Ip6EnableUdpEchoSrv PROTO ((tIp6CliConfigParams *pu1Ip6Input,
                                 UINT1 **ppu1OutMsg));
*/

VOID Ip6RouteShowInCxt PROTO ((tCliHandle CliHandle, UINT4 u4VcId, INT4 i4Protocol,
                   UINT1 u1IpPresent, tIp6Addr RouteDest, INT4 i4PrefixLen));
VOID Ip6FrtRouteShowInCxt (tCliHandle CliHandle);

/* VOID Ip6AddrShow PROTO ((tIp6CliConfigParams *pu1Ip6Input,
                         UINT1 **ppu1OutMsg));*/

VOID
Ip6RouteAddInCxt  PROTO ((tCliHandle CliHandle, UINT4 u4RouteIndex,
                          UINT4 u4ContextId, tIp6Addr RouteDest,
                    tIp6Addr NextHop, INT4 i4PrefixLen, INT4 i4RoutePreference,INT4 i4AddrType));

/* Static Route AD specific protoypes */
VOID Ip6SetDefAdminDistance PROTO ((UINT4, INT4 ));
VOID Ip6ShowDefAdminDistance PROTO ((tCliHandle , UINT4));

VOID
Ip6RouteDel PROTO ((tCliHandle CliHandle, tIp6Addr RouteDest, INT4 i4PrefixLen,
      tIp6Addr NextHop));

INT4
Ip6Nd6ShowInCxt PROTO((tCliHandle CliHandle, UINT4 u4VcId));

INT4
Ip6Nd6ShowSummaryInCxt PROTO((tCliHandle CliHandle, UINT4 u4VcId));

VOID
Ip6EnableRouteAdvStatus PROTO ((tCliHandle CliHandle, UINT4 u4Index,
                               INT4 i4RouteAdvStatus));

INT4
Ip6SetNDProxyAdminStatus PROTO ((tCliHandle CliHandle, INT4 i4Index,
         INT4 i4ProxyAdminStatus));

INT4
Ip6SetNDProxyMode PROTO ((tCliHandle CliHandle, INT4 i4Index,
       INT4 i4ProxyMode));

INT4
Ip6SetNDProxyUpstream PROTO ((tCliHandle CliHandle, INT4 i4Index,
        INT4 i4ProxyUpstream));

INT4
Ip6SetSeNDSecLevel PROTO ((tCliHandle CliHandle, UINT4 u4SecValue));

INT4 
Ip6SetSeNDNbrSecLevel PROTO ((tCliHandle CliHandle, UINT4 u4SecValue));

INT4
Ip6SetSeNDPrefixChkRelax PROTO ((tCliHandle CliHandle, INT4 i4PrefixCheck));

INT4
Ip6SetSeNDMinKeyLength PROTO ((tCliHandle CliHandle, UINT4 u4KeyLength));

INT4
Ip6SetSeNDTimeStamp PROTO ((tCliHandle CliHandle, INT4 i4Index,
                     UINT4 u4TimeStampValue, INT4 i4TimeStampType));
INT4
Ip6SetSeNDStatus PROTO ((tCliHandle CliHandle, INT4 i4Index, INT4 i4SecureMode));

INT4
Ip6SetSeNDAcceptUnsecureAdv PROTO ((tCliHandle CliHandle, INT4 i4AcceptStatus));

INT4
Ip6SetSeNDAuthType PROTO ((tCliHandle CliHandle, INT4 i4AuthType));

VOID
Ip6EnableRouteAdvFlags PROTO ((tCliHandle CliHandle, UINT4 u4Index, INT4 i4RouteAdvFlags));

VOID
Ip6HopLimit PROTO ((tCliHandle CliHandle, UINT4 u4Index, INT4 i4HopLimit));

VOID
Ip6DefRouterLifeTime PROTO ((tCliHandle CliHandle, UINT4 u4Index, UINT4 u4DefRouterTime));

/*VOID
Ip6PrefixAdvStatus PROTO((tIp6CliConfigParams *pu1Ip6Input,UINT1 **ppu1OutMsg));*/

VOID
Ip6DADRetries PROTO ((tCliHandle CliHandle, UINT4 u4Index, INT4 i4DADRetries));


VOID
Ip6EnableRouteAdvRDNSS PROTO ((tCliHandle CliHandle, UINT4 u4Index,tIp6Addr Ip6Addr,
                                UINT4 u4LifetimeOne,INT4  i4RouteAdvRDNSS));

VOID
Ip6DisableRouteAdvRDNSS PROTO ((tCliHandle CliHandle, UINT4 u4Index,tIp6Addr Ip6Addr,
                               tIp6Addr,tIp6Addr,INT4  i4RouteAdvRDNSS));

VOID 
Ip6ValidateRDNSS PROTO ((tIp6Addr Ip6Addr, INT4 i4flag,INT4 u4Index));

VOID
Ip6EnableRouteAdvRDNSSStatus  PROTO ((tCliHandle CliHandle, UINT4 u4Index,
                               INT4  i4RouteAdvRDNSSStatus));

VOID 
Ip6RaRDNSSPreference PROTO ((tCliHandle CliHandle, UINT4 u4Index, INT4 i4RDNSSPreference));

VOID
Ip6RaAdvtInterval PROTO ((tCliHandle CliHandle, UINT4 u4Index, INT4 i4AdvInterval ));

VOID
Ip6RaAdvLinkLocal PROTO ((tCliHandle CliHandle, UINT4 u4Index, INT4 i4AdvtLinkLocal ));

VOID
Ip6RaRDNSSLifetime PROTO ((tCliHandle CliHandle, UINT4 u4Index, INT4 i4RDNSSLifetime ));

VOID
Ip6RaDnsDomNameLifeTime PROTO ((tCliHandle CliHandle, UINT4 u4Index, tSNMP_OCTET_STRING_TYPE * pDomNamOneOct,
                                UINT4 i4TimeVal, INT4  i4DomLifeTime));
VOID
Ip6RaNoDnsDomNameLifeTime PROTO ((tCliHandle CliHandle, UINT4 u4Index, tSNMP_OCTET_STRING_TYPE * pDomNamOneOct,
                                 tSNMP_OCTET_STRING_TYPE * pDomNameTwoOct, tSNMP_OCTET_STRING_TYPE * pDomNameThreeOct,
                                 INT4 i4DomLifeTime));

VOID
Ip6RaDnsLifeTime PROTO ((tCliHandle CliHandle, UINT4 u4Index, INT4 i4TimeVal));

VOID
Ip6ValidateDNS PROTO ((tSNMP_OCTET_STRING_TYPE * pDomNameOneOct, INT4 i4flag,INT4 u4Index));

/*
VOID  
Ip6IfStatsShow  PROTO((tIp6CliConfigParams *pu1Ip6Input, UINT1 **ppu1OutMsg)); 
*/

INT4
Ip6TrafficShowInCxt PROTO ((tCliHandle CliHandle, UINT1 u1HCFlag, UINT4 u4VcId));

INT4
Ip6IfTrafficShowInCxt PROTO 
((tCliHandle CliHandle, UINT4 u4IfIndex, UINT1 u1HCFlag, UINT4 u4VcId));

INT1
Ip6DefaultHopLimit PROTO ((tCliHandle CliHandle, INT4 i4HopLimit));

VOID
Ip6ReachableTime  PROTO ((tCliHandle CliHandle, UINT4 u4Index, UINT4 u4ReachableTime));
VOID
Ip6RetransmitTime PROTO ((tCliHandle CliHandle, UINT4 u4Index, 
                          UINT4 u4ReTransTime));
/*
VOID
Ip6RetransTime PROTO((tIp6CliConfigParams *pu1Ip6Input, UINT1 **ppu1OutMsg));*/

VOID
Ip6RouteAdvInterval PROTO ((tCliHandle CliHandle, UINT4 u4Index, 
                            UINT4 u4MinRAInterval, UINT4 u4MaxRAInterval));

VOID
Ip6CliAddrDelete  PROTO ((tCliHandle CliHandle, UINT4 u4Ip6IfIndex, tIp6Addr Ip6Addr,
                          UINT4 i4PrefixLen, INT4 i4AddrType));

VOID
Ip6TraceInCxt PROTO ((tCliHandle CliHandle, UINT4 u4DbgMap, UINT4 u4ContextId));

/*VOID Ip6CliAddDefaultRoute PROTO ((tIp6CliConfigParams * pu1Ip6Input,
                                   UINT1 **ppRespMsg));

VOID Ip6CliDelDefaultRoute PROTO ((tIp6CliConfigParams * pu1Ip6Input,
                                   UINT1 **ppRespMsg));

VOID Ip6DefaultRouteShow PROTO ((tIp6CliConfigParams * pu1Ip6Input,
                                 UINT1 **ppRespMsg));

VOID Ip6CliProtoPreference PROTO ((tIp6CliConfigParams * pu1Ip6Input,
                                   UINT1 **ppRespMsg));
*/

INT4
Ip6SetPrefixList PROTO ((tCliHandle CliHandle, UINT4 u4ProfileIndex,
           UINT4 u4ValidTime, UINT1 u1ValidTimeFlag,
           UINT4 u4PrefTime, UINT1 u1PrefTimeFlag,
    UINT1 u1AdvtStatus, UINT1 u1OnLinkStatus,
                  UINT1 u1AutoConfigStatus));

VOID
Ip6SetPrefix PROTO ((tCliHandle CliHandle, UINT4 u4IfIndex, tIp6Addr Ip6Prefix,
                     INT4 i4PrefixLen, UINT4 u4ValidTime, UINT1 u1ValidTimeFlag,
       UINT4 u4PreferredTime, UINT1 u1PrefTimeFlag,
               UINT1 u1NoAdveriseFlag, UINT1 u1OnLinkFlag, 
       UINT1 u1NoAutoConfigFlag, UINT1 u1SupportEmbeddedRp,INT4 i4DefaultFlag));

VOID
Ip6DelPrefix PROTO ((tCliHandle CliHandle, UINT4 u4IfIndex, tIp6Addr Ip6Prefix,
                    INT4 i4PrefixLen));

INT4
Ip6DelPrefixList PROTO ((tCliHandle CliHandle, UINT4 u4ProfileIndex));

VOID
Ip6ShowPrefixInCxt PROTO ((tCliHandle CliHandle, UINT4 u4Index, UINT4 u4VcId));

VOID
Ip6SetPolicyPrefix PROTO ((tCliHandle CliHandle, UINT4 u4IfIndex, tIp6Addr Ip6PolicyPrefix,
                              UINT4 u4PrefixLen,UINT4  u4Label,UINT4  u4Precedence,INT4 i4AddrType));

VOID
Ip6DelPolicyPrefix PROTO ((tCliHandle CliHandle, UINT4 u4IfIndex, tIp6Addr Ip6PolicyPrefix,
                              UINT4 u4PrefixLen,UINT4  u4Label,UINT4  u4Precedence,INT4 i4AddrType));


VOID
Ip6Nd6CacheAdd PROTO ((tCliHandle CliHandle, UINT4 u4IfIndex, tIp6Addr Ip6Addr,
                       UINT1 *au1PhyMacAddr, UINT4 u4ContextId));

VOID
Ip6Nd6CacheDel PROTO ((tCliHandle CliHandle, UINT4 u4IfIndex,
                       tIp6Addr Ip6Addr, UINT4 u4ContextId));

VOID Ip6TraceRoute PROTO  ((tCliHandle CliHandle, tIp6Addr Ip6Addr));
/*
VOID Ip6AllowUnSecuredPing PROTO ((tIp6CliConfigParams *pu1Ip6Input,
                                   UINT1 **ppRespMsg));
VOID Ip6SetUniTunlSrc  PROTO ((tIp6CliConfigParams *pu1Ip6Input,
                               UINT1 **ppRespMsg));

VOID Ip6ResetUniTunlSrc  PROTO ((tIp6CliConfigParams *pu1Ip6Input,
                               UINT1 **ppRespMsg));*/

VOID Ip6ClearNeighbors PROTO ((tCliHandle CliHandle, UINT4 u4VcId, tIp6Addr * pIp6Addr));
VOID Ip6ClearTraffic PROTO ((tCliHandle CliHandle, UINT4 u4VcId));
VOID Ip6ClearRoute PROTO ((tCliHandle CliHandle, UINT4 u4VcId));

VOID Ip6RouteSummaryShowInCxt PROTO ((tCliHandle CliHandle, UINT4 u4VcId));
VOID
Ipv6ShowRunningConfigScalarsInCxt (tCliHandle CliHandle, UINT4 u4ContextId);
VOID
Ipv6ShowRunningConfigTablesInCxt (tCliHandle CliHandle, UINT4 u4ContextId);
INT4
Ipv6ShowRunningConfigInCxt (tCliHandle CliHandle, UINT4 u4Module, UINT4 u4ContextId);
VOID IssIp6ShowUtilTrace PROTO ((tCliHandle,UINT4));
VOID IssIp6ShowDebugging PROTO ((tCliHandle));


VOID Fsipv6IfTableInfo (tCliHandle CliHandle,INT4 i4IfIndex );
VOID Fsipv6PrefixProfileTableInfo (tCliHandle CliHandle,INT4 i4IfIndex ,
                                   tSNMP_OCTET_STRING_TYPE * pPrefixAddrs,
                                   INT4 i4PrefixAddrLen);
VOID Fsipv6AddrTableInfo (tCliHandle CliHandle,INT4 i4IfIndex ,
                          tSNMP_OCTET_STRING_TYPE * pPrefixAddrs,
                          INT4 i4PrefixAddrLen);
VOID 
Fsipv6InterfaceScopeZoneTableInfo PROTO ((tCliHandle CliHandle, INT4 i4IfIndex));

VOID
Fsipv6PolicyPrefixTableInfo PROTO ((tCliHandle CliHandle,
                     tSNMP_OCTET_STRING_TYPE * pPrefixAddrs,
                     INT4 i4PrefixAddrLen,  INT4 i4IfIndex));

INT4 Ipv6ShowRunningConfigInterfaceInCxt (tCliHandle CliHandle, UINT4 u4ContextId);

INT4
Ip6IfTrafficShowForAllVlanInCxt PROTO ((tCliHandle CliHandle,
                       UINT4 u4IfIndex, UINT1 u1HCFlag,
                       UINT4 u4VcId, UINT1 *pu1IfName));
VOID
Ip6ShowPrefixForAllVlanInCxt PROTO ((tCliHandle CliHandle,
              UINT4 u4IfIndex, UINT4 u4VcId, UINT1 *pu1IfName));
VOID
Ip6IfShowForAllVlanInCxt PROTO ((tCliHandle CliHandle,
             UINT4 u4IfIndex, UINT4 u4VcId, UINT1 *pu1IfName));

VOID 
Ip6IfShowForIndex PROTO ((tCliHandle CliHandle, UINT4 u4IfIndex));

INT4
Ip6SetPmtuDiscovery PROTO ((tCliHandle CliHandle, INT4 i4PmtuStatus,
                             UINT4 u4VcId));

INT4
Ip6SetPmtuValue PROTO ((tCliHandle CliHandle, tIp6Addr Ip6Addr,
                       INT4 i4PmtuValue, UINT4 u4VcId, INT4 i4Admin));
INT4
Ip6ShowPmtu PROTO ((tCliHandle CliHandle, UINT4 u4VcId));
INT4
Ip6SetRFC5095Compatibilty PROTO (( tCliHandle CliHandle, 
                                 INT4 i4Value, UINT4 u4VcId));

INT4
Ip6SetRFC5942Compatibilty PROTO (( tCliHandle CliHandle,
                                 INT4 i4Value));


/* Prototypes for Scoped Address Changes RFC4007 -start */
VOID
Ip6SetScopeZone PROTO ((tCliHandle CliHandle,UINT4  i4IfaceIndex,UINT1  u1Scope,
                        UINT1* au1ScopeZone));
VOID
Ip6CliDeleteScopeZone PROTO ((tCliHandle CliHandle,UINT4 u4Index,UINT1 u1Scope,
                              UINT1 *au1ScopeZone));
VOID
Ip6CliConfigDefaultScopeZone PROTO ((tCliHandle CliHandle,UINT1* au1ScopeZone));
VOID
Icmp6ErrMsgRateLimit PROTO ((tCliHandle CliHandle, UINT4 i4IfaceIndex,
                     INT4 IcmpErrInterval, INT4 IcmpTokenBucketSize));

VOID
Ip6IfDisabeDestUnreachMsg PROTO ((tCliHandle CliHandle, UINT4 i4IfaceIndex,
                                  INT4 i4DestUnreachable));

VOID
Ip6UnnumIf PROTO ((tCliHandle CliHandle, INT4 u4IfaceIndex, INT4 i4UnnumAssocIPIf));

VOID
Ip6IfIcmpRedirectMsg PROTO ((tCliHandle CliHandle, UINT4 i4IfaceIndex,
                                  INT4 i4RedirectMsg));
VOID
Ip6NdCacheTimeout  PROTO ((tCliHandle CliHandle, INT4 i4NdCacheTimeout ));

VOID
Ip6CliGetDefaultScopeZone PROTO ((tCliHandle CliHandle,UINT4 u4ContextId));

VOID
Ip6CliGetDefaultScopeZoneForAllCxt PROTO((tCliHandle CliHandle, UINT4 u4VcId));

VOID
Ip6CliConfigEcmpTimeInterval (tCliHandle CliHandle, INT4 i4EcmpPRTInterval);

INT4
Ip6ShowAddrSelPolicyTable PROTO ((tCliHandle CliHandle));

VOID
Ip6ShowZoneInCxt PROTO ((tCliHandle CliHandle, UINT4 u4IfIndex, UINT4 u4VcId));

VOID
Ip6ShowIfZoneInfoInCxt PROTO ((tCliHandle CliHandle, UINT4 u4IfIndex, UINT4 u4L3ContextId));

VOID
Ip6ShowZoneInfoForAllVlanInCxt PROTO ((tCliHandle CliHandle,UINT4 u4IfIndex, UINT4 u4VcId,
                                        UINT1  *au1IfName));
VOID
IP6ShowZoneInterfacesInCxt PROTO ((tCliHandle CliHandle,UINT4  u4VcId,UINT1 *au1ScopeZone));
/* Prototypes for Scoped Address Changes RFC4007 - End */

INT4
IP6ShowInterfacesInZone PROTO((tCliHandle CliHandle,
                        tSNMP_OCTET_STRING_TYPE * pOctetStr,
                        UINT4 u4MaxPorts,UINT4 *pu4PagingStatus,
                        UINT1 u1MaxPortsPerLine));

VOID
Ip6IfShowNdSecureForAllVlanInCxt PROTO ((tCliHandle CliHandle,
                                  UINT4 u4IfIndex, UINT4 u4VcId,
                                  UINT1 *pu1IfName, INT4 i4NdShowType));

VOID
Ip6IfShowNdSecureInCxt PROTO ((tCliHandle CliHandle, UINT4 u4IfIndex, UINT4 u4VcId, INT4 i4NdShowType));

VOID
Ip6IfShowNd6SecureTimeStampForIndex PROTO ((tCliHandle CliHandle,  UINT4 u4NdCxtId, UINT4 u4IfIndex));

VOID
Ip6IfShowNd6SecureNonceForIndex PROTO ((tCliHandle CliHandle, UINT4 u4NdCxtId, UINT4 u4IfIndex));

VOID
Ip6IfShowNd6SecureCgaForIndex PROTO ((tCliHandle CliHandle, UINT4 u4IfIndex));

VOID
Ip6Nd6SecureCgaAddrTableInfo PROTO ((tCliHandle CliHandle, INT4 i4IfIndex,
                     tSNMP_OCTET_STRING_TYPE * pPrefixAddrs,
                     INT4 i4PrefixAddrLen));
VOID
Ip6IfShowNd6SecureStatistics PROTO ((tCliHandle CliHandle, UINT4 u4IfIndex));

VOID
Ip6Nd6ShowSentPktsStatsInfo PROTO ((tCliHandle CliHandle, INT4 i4Index, INT4 i4PktType));

VOID
Ip6Nd6ShowRcvPktsStatsInfo PROTO ((tCliHandle CliHandle, INT4 i4Index, INT4 i4PktType));

VOID
Ip6Nd6ShowDrpPktsStatsInfo PROTO ((tCliHandle CliHandle, INT4 i4Index, INT4 i4PktType));

VOID
Ip6SetRARouteInfo PROTO ((tCliHandle CliHandle, INT4 i4RAIfIndex,
                   tIp6Addr Ip6RARoutePrefix, INT4 i4RARoutPrefixLen,
                   INT4 i4RARoutePreference, UINT4 u4RARouteLifetime));

VOID
Ip6DelRARouteInfo PROTO ((tCliHandle CliHandle, INT4 i4RAIfIndex,
                   tIp6Addr Ip6RARoutePrefix, INT4 i4RARoutPrefixLen));

VOID
IpSetDefRoutePref PROTO ((tCliHandle CliHandle, INT4 i4IfIndex,
                   INT4 i4DefRoutePreference));



/* Ipvx Prototypes */
VOID CliSetIp6RALinkMTU (tCliHandle CliHandle, INT4 i4Index, UINT4 u4LinkMTU);
INT1 nmhValidateIndexInstanceIpv6RouterAdvertTable ARG_LIST((INT4 ));
INT1 nmhGetFirstIndexIpv6RouterAdvertTable ARG_LIST((INT4 *));
INT1 nmhGetNextIndexIpv6RouterAdvertTable ARG_LIST((INT4 , INT4 *));
INT1 nmhGetIpv6RouterAdvertSendAdverts ARG_LIST((INT4 ,INT4 *));
INT1 nmhGetIpv6RouterAdvertMaxInterval ARG_LIST((INT4 ,UINT4 *));
INT1 nmhGetIpv6RouterAdvertMinInterval ARG_LIST((INT4 ,UINT4 *));
INT1 nmhGetIpv6RouterAdvertManagedFlag ARG_LIST((INT4 ,INT4 *));
INT1 nmhGetIpv6RouterAdvertOtherConfigFlag ARG_LIST((INT4 ,INT4 *));
INT1 nmhGetIpv6RouterAdvertLinkMTU ARG_LIST((INT4 ,UINT4 *));
INT1 nmhGetIpv6RouterAdvertReachableTime ARG_LIST((INT4 ,UINT4 *));
INT1 nmhGetIpv6RouterAdvertRetransmitTime ARG_LIST((INT4 ,UINT4 *));
INT1 nmhGetIpv6RouterAdvertCurHopLimit ARG_LIST((INT4 ,UINT4 *));
INT1 nmhGetIpv6RouterAdvertDefaultLifetime ARG_LIST((INT4 ,UINT4 *));
INT1 nmhGetIpv6RouterAdvertRowStatus ARG_LIST((INT4 ,INT4 *));
INT1 nmhSetIpv6RouterAdvertSendAdverts ARG_LIST((INT4  ,INT4 ));
INT1 nmhSetIpv6RouterAdvertMaxInterval ARG_LIST((INT4  ,UINT4 ));
INT1 nmhSetIpv6RouterAdvertMinInterval ARG_LIST((INT4  ,UINT4 ));
INT1 nmhSetIpv6RouterAdvertManagedFlag ARG_LIST((INT4  ,INT4 ));
INT1 nmhSetIpv6RouterAdvertOtherConfigFlag ARG_LIST((INT4  ,INT4 ));
INT1 nmhSetIpv6RouterAdvertLinkMTU ARG_LIST((INT4  ,UINT4 ));
INT1 nmhSetIpv6RouterAdvertReachableTime ARG_LIST((INT4  ,UINT4 ));
INT1 nmhSetIpv6RouterAdvertRetransmitTime ARG_LIST((INT4  ,UINT4 ));
INT1 nmhSetIpv6RouterAdvertCurHopLimit ARG_LIST((INT4  ,UINT4 ));
INT1 nmhSetIpv6RouterAdvertDefaultLifetime ARG_LIST((INT4  ,UINT4 ));
INT1 nmhSetIpv6RouterAdvertRowStatus ARG_LIST((INT4  ,INT4 ));
INT1 nmhTestv2Ipv6RouterAdvertMaxInterval ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));
INT1 nmhTestv2Ipv6RouterAdvertMinInterval ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));
INT1 nmhTestv2Ipv6RouterAdvertManagedFlag ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
INT1 nmhTestv2Ipv6RouterAdvertOtherConfigFlag ARG_LIST((UINT4 *, INT4 ,INT4 ));
INT1 nmhTestv2Ipv6RouterAdvertLinkMTU ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));
INT1 nmhTestv2Ipv6RouterAdvertReachableTime ARG_LIST((UINT4 * ,INT4 ,UINT4 ));
INT1 nmhTestv2Ipv6RouterAdvertRetransmitTime ARG_LIST((UINT4 * ,INT4 ,UINT4 ));
INT1 nmhTestv2Ipv6RouterAdvertCurHopLimit ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));
INT1 nmhTestv2Ipv6RouterAdvertDefaultLifetime ARG_LIST((UINT4 * ,INT4 ,UINT4));
INT1 nmhTestv2Ipv6RouterAdvertRowStatus ARG_LIST((UINT4 * ,INT4  ,INT4 ));

INT1 Ip6TestScopeZoneEntry PROTO ((UINT4 *pu4ErrorCode, UINT4 u4Index,
                       UINT1 u1Scope, UINT1 *au1ScopeZone, INT4 i4len,UINT1 u1ZoneCreationStatus));
INT4 Ip6ScopeZoneCreate PROTO ((UINT4 u4Index,UINT1 u1Scope,UINT1 *au1ScopeZone, 
            UINT1 u1CreationStatus));

extern INT1
nmhGetFsMIStdInetCidrRouteNumber ARG_LIST((INT4 ,UINT4 *));
extern INT1
nmhGetFirstIndexFsMIStdIpNetToPhysicalTable ARG_LIST((INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE * ));
extern INT1
nmhGetFsMIStdIpNetToPhysicalContextId ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));
extern INT1
nmhGetFsMIStdIpNetToPhysicalPhysAddress ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));
extern INT1
nmhGetFsMIStdIpNetToPhysicalType ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));
extern INT1
nmhGetNextIndexFsMIStdIpNetToPhysicalTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));
extern INT1
nmhGetFsMIStdIpIfStatsHCInReceives ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));
extern INT1
nmhGetFsMIStdIpIfStatsHCInOctets ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

extern INT1
nmhGetFsMIStdIpIfStatsInHdrErrors ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetFsMIStdIpIfStatsInNoRoutes ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetFsMIStdIpIfStatsInAddrErrors ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetFsMIStdIpIfStatsInUnknownProtos ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetFsMIStdIpIfStatsInTruncatedPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetFsMIStdIpIfStatsInForwDatagrams ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetFsMIStdIpIfStatsHCInForwDatagrams ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

extern INT1
nmhGetFsMIStdIpIfStatsReasmReqds ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetFsMIStdIpIfStatsReasmOKs ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetFsMIStdIpIfStatsReasmFails ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetFsMIStdIpIfStatsInDiscards ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetFsMIStdIpIfStatsInDelivers ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetFsMIStdIpIfStatsHCInDelivers ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

extern INT1
nmhGetFsMIStdIpIfStatsOutRequests ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetFsMIStdIpIfStatsHCOutRequests ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

extern INT1
nmhGetFsMIStdIpIfStatsOutForwDatagrams ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetFsMIStdIpIfStatsHCOutForwDatagrams ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

extern INT1
nmhGetFsMIStdIpIfStatsOutDiscards ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetFsMIStdIpIfStatsOutFragReqds ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetFsMIStdIpIfStatsOutFragOKs ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetFsMIStdIpIfStatsOutFragFails ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetFsMIStdIpIfStatsOutFragCreates ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetFsMIStdIpIfStatsOutTransmits ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetFsMIStdIpIfStatsHCOutTransmits ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

extern INT1
nmhGetFsMIStdIpIfStatsOutOctets ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetFsMIStdIpIfStatsHCOutOctets ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

extern INT1
nmhGetFsMIStdIpIfStatsInMcastPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetFsMIStdIpIfStatsHCInMcastPkts ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

extern INT1
nmhGetFsMIStdIpIfStatsInMcastOctets ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetFsMIStdIpIfStatsHCInMcastOctets ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

extern INT1
nmhGetFsMIStdIpIfStatsOutMcastPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetFsMIStdIpIfStatsHCOutMcastPkts ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

extern INT1
nmhGetFsMIStdIpIfStatsOutMcastOctets ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetFsMIStdIpIfStatsHCOutMcastOctets ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

extern INT1
nmhGetFsMIStdIpIfStatsInBcastPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetFsMIStdIpIfStatsHCInBcastPkts ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

extern INT1
nmhGetFsMIStdIpIfStatsOutBcastPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetFsMIStdIpIfStatsHCOutBcastPkts ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

extern INT1
nmhGetFsMIStdIpIfStatsDiscontinuityTime ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetFsMIStdIpIfStatsRefreshRate ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetFsMIStdIpIfStatsContextId ARG_LIST((INT4  , INT4 ,INT4 *));

extern INT1
nmhGetFsMIStdIpSystemStatsInReceives ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetFsMIStdIpSystemStatsHCInReceives ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

extern INT1
nmhGetFsMIStdIpSystemStatsInOctets ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetFsMIStdIpSystemStatsHCInOctets ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

extern INT1
nmhGetFsMIStdIpSystemStatsInHdrErrors ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetFsMIStdIpSystemStatsInNoRoutes ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetFsMIStdIpSystemStatsInAddrErrors ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetFsMIStdIpSystemStatsInUnknownProtos ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetFsMIStdIpSystemStatsInTruncatedPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetFsMIStdIpSystemStatsInForwDatagrams ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetFsMIStdIpSystemStatsHCInForwDatagrams ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

extern INT1
nmhGetFsMIStdIpSystemStatsReasmReqds ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetFsMIStdIpSystemStatsReasmOKs ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetFsMIStdIpSystemStatsReasmFails ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetFsMIStdIpSystemStatsInDiscards ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetFsMIStdIpSystemStatsInDelivers ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetFsMIStdIpSystemStatsHCInDelivers ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

extern INT1
nmhGetFsMIStdIpSystemStatsOutRequests ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetFsMIStdIpSystemStatsHCOutRequests ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

extern INT1
nmhGetFsMIStdIpSystemStatsOutNoRoutes ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetFsMIStdIpSystemStatsOutForwDatagrams ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetFsMIStdIpSystemStatsHCOutForwDatagrams ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

extern INT1
nmhGetFsMIStdIpSystemStatsOutDiscards ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetFsMIStdIpSystemStatsOutFragReqds ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetFsMIStdIpSystemStatsOutFragOKs ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetFsMIStdIpSystemStatsOutFragFails ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetFsMIStdIpSystemStatsOutFragCreates ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetFsMIStdIpSystemStatsOutTransmits ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetFsMIStdIpSystemStatsHCOutTransmits ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

extern INT1
nmhGetFsMIStdIpSystemStatsOutOctets ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetFsMIStdIpSystemStatsHCOutOctets ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

extern INT1
nmhGetFsMIStdIpSystemStatsInMcastPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetFsMIStdIpSystemStatsHCInMcastPkts ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

extern INT1
nmhGetFsMIStdIpSystemStatsInMcastOctets ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetFsMIStdIpSystemStatsHCInMcastOctets ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

extern INT1
nmhGetFsMIStdIpSystemStatsOutMcastPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetFsMIStdIpSystemStatsHCOutMcastPkts ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

extern INT1
nmhGetFsMIStdIpSystemStatsOutMcastOctets ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetFsMIStdIpSystemStatsHCOutMcastOctets ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

extern INT1
nmhGetFsMIStdIpSystemStatsInBcastPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetFsMIStdIpSystemStatsHCInBcastPkts ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

extern INT1
nmhGetFsMIStdIpSystemStatsOutBcastPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetFsMIStdIpSystemStatsHCOutBcastPkts ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

extern INT1
nmhGetFsMIStdIpSystemStatsDiscontinuityTime ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetFsMIStdIpSystemStatsRefreshRate ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetFsMIStdIpReasmTimeout ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetFsMIStdIpv6IpForwarding ARG_LIST((INT4 ,INT4 *));


extern INT1
nmhGetFsMIStdInetCidrRouteMetric1 ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

extern INT1
nmhGetFsMIStdInetCidrRouteMetric5 ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

extern INT1
nmhGetFsMIStdIcmpMsgStatsInPkts ARG_LIST((INT4  , INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetFsMIStdIcmpMsgStatsOutPkts ARG_LIST((INT4  , INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetFsMiUdpIpvxInDatagrams ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetFsMiUdpIpvxNoPorts ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetFsMiUdpIpvxInErrors ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetFsMiUdpIpvxOutDatagrams ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetFsMiUdpIpvxHCInDatagrams ARG_LIST((INT4 ,tSNMP_COUNTER64_TYPE *));

extern INT1
nmhGetFsMiUdpIpvxHCOutDatagrams ARG_LIST((INT4 ,tSNMP_COUNTER64_TYPE *));

extern INT1
nmhGetFsMIStdIpv6IpDefaultHopLimit ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetFirstIndexFsMIStdInetCidrRouteTable ARG_LIST ((INT4 *, INT4 *,
                                           tSNMP_OCTET_STRING_TYPE *,
                                           UINT4 *, tSNMP_OID_TYPE *,
                                           INT4 *, tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhGetNextIndexFsMIStdInetCidrRouteTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , UINT4 , UINT4 * , tSNMP_OID_TYPE *, tSNMP_OID_TYPE *  , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

extern INT1
nmhGetFsMIStdInetCidrRouteStatus ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

extern INT1
nmhGetFsMIStdInetCidrRouteProto ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

extern INT1
nmhGetFsMIStdInetCidrRouteIfIndex ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

extern INT1
nmhGetFsMIStdIcmpStatsOutMsgs ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetFsMIStdIcmpStatsOutErrors ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetFsMIStdIpv6IfContextId ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhSetFsMIStdInetCidrRouteStatus ARG_LIST ((INT4, INT4, 
                                            tSNMP_OCTET_STRING_TYPE *,
                                            UINT4, tSNMP_OID_TYPE *,
                                            INT4, tSNMP_OCTET_STRING_TYPE *,
                                            INT4));


extern UINT1               gau1ZeroPingHostName[DNS_MAX_QUERY_LEN];
extern tIp6Addr            gZeroIp6Addr;

/* RFC 4191 Default router preference Implementation Prototypes */
VOID
Ip6ShowRARoutesForAllVlanInCxt PROTO ((tCliHandle CliHandle,
                              UINT4 u4IfIndex, UINT4 u4VcId, UINT1 *pu1IfName));

VOID
Ip6ShowRARoutesInCxt PROTO ((tCliHandle CliHandle, UINT4 u4IfIndex, UINT4 u4VcId));

VOID
Ip6IfShowNd6RARoutes PROTO ((tCliHandle CliHandle, UINT4 u4IfIndex));

VOID
Ip6Nd6ShowRARouteInfo PROTO ((tCliHandle CliHandle, INT4 i4Index,
                      tSNMP_OCTET_STRING_TYPE *pIp6Prefix,
                      INT4 i4PrefixLen));

VOID
FsipvRARouteTableInfo PROTO ((tCliHandle CliHandle, INT4 i4Index,
                       tSNMP_OCTET_STRING_TYPE * pIp6Prefix,
                       INT4 i4PrefixLen));
