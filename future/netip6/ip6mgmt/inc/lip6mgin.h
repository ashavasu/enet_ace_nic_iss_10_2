/**************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: lip6mgin.h,v 1.4 2013/01/07 12:24:00 siva Exp $
 *
 * Description: Interface management routines for Linux IPv6
 ***************************************************************************/
#ifndef _LIP6MGINC_H
#define _LIP6MGINC_H

/* IPv6 Include Files */
#include "lr.h"
#include "cfa.h"
#include "ipv6.h"
#include "ip.h"
#include "ip6util.h"
#include "ripv6.h"
#include "trieinc.h"               /* Trie2 Related info */
#include "rtm6.h"                  /* RTM6 Info */

/* For prototypes */
#include "lip6port.h"
#include "lip6prot.h"
/* For statistics */
#include "lip6stat.h"

#include "stdip6lw.h"
#include "fsip6low.h"
#include "fsmpipv6lw.h"

#ifdef MLD_WANTED
#include "mld.h"
#endif

#ifdef TUNNEL_WANTED

#ifdef SLI_WANTED
# include "tcp.h"
# include "sli.h"
#endif

#endif

#ifdef MIP6_WANTED
#include "mipv6.h"
#endif


#include "ipvx.h"
#include "ip6ipvx.h"

#include "vcm.h"

#include "ip6snmp.h"
#include "png6tdfs.h"
#include "png6def.h"
#include "ping6.h"
#endif /* _LIP6MGINC_H */
/* END OF FILE */
