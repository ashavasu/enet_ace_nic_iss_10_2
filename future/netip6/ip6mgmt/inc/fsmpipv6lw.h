/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmpipv6lw.h,v 1.17 2015/02/12 11:55:56 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto Validate Index Instance for FsMIIpv6ContextTable. */
INT1
nmhValidateIndexInstanceFsMIIpv6ContextTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIIpv6ContextTable  */

INT1
nmhGetFirstIndexFsMIIpv6ContextTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIIpv6ContextTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIIpv6NdCacheMaxRetries ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIIpv6PmtuConfigStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIIpv6PmtuTimeOutInterval ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIIpv6JumboEnable ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIIpv6NumOfSendJumbo ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIIpv6NumOfRecvJumbo ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIIpv6ErrJumbo ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIIpv6ContextDebug ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIIpv6RFC5095Compatibility ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIIpv6RFC5942Compatibility ARG_LIST((INT4 ,INT4 *));


/* Low Level SET Routine for All Objects.  */

INT1
nmhGetFsMIIpv6SENDSecLevel ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIIpv6SENDNbrSecLevel ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIIpv6SENDAuthType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIIpv6SENDMinBits ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIIpv6SENDSecDAD ARG_LIST((INT4 ,INT4 *));

INT1
nmhSetFsMIIpv6NdCacheMaxRetries ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIIpv6PmtuConfigStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIIpv6PmtuTimeOutInterval ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsMIIpv6JumboEnable ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIIpv6ContextDebug ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsMIIpv6RFC5095Compatibility ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIIpv6RFC5942Compatibility ARG_LIST((INT4 ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIIpv6NdCacheMaxRetries ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIIpv6PmtuConfigStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIIpv6PmtuTimeOutInterval ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsMIIpv6JumboEnable ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIIpv6ContextDebug ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsMIIpv6RFC5095Compatibility ARG_LIST((UINT4 *  ,INT4  ,INT4 ));


INT1
nmhTestv2FsMIIpv6RFC5942Compatibility ARG_LIST((UINT4 *  ,INT4 ,INT4  ));

INT1
nmhSetFsMIIpv6SENDSecLevel ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIIpv6SENDNbrSecLevel ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIIpv6SENDAuthType ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIIpv6SENDMinBits ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIIpv6SENDSecDAD ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIIpv6SENDPrefixChk ARG_LIST((INT4  ,INT4 ));

INT1
nmhGetFsMIIpv6SENDPrefixChk ARG_LIST((INT4 , INT4 *));


/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIIpv6SENDSecLevel ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIIpv6SENDNbrSecLevel ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIIpv6SENDAuthType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIIpv6SENDMinBits ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIIpv6SENDSecDAD ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIIpv6SENDPrefixChk ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIIpv6ContextTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMIIpv6SENDSecLevel ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMIIpv6SENDNbrSecLevel ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMIIpv6SENDAuthType ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMIIpv6SENDMinBits ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMIIpv6SENDSecDAD ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMIIpv6SENDPrefixChk ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIIpv6IfTable. */
INT1
nmhValidateIndexInstanceFsMIIpv6IfTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIIpv6IfTable  */

INT1
nmhGetFirstIndexFsMIIpv6IfTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIIpv6IfTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIIpv6IfType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIIpv6IfPortNum ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIIpv6IfCircuitNum ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIIpv6IfToken ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIIpv6IfOperStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIIpv6IfRouterAdvStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIIpv6IfRouterAdvFlags ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIIpv6IfHopLimit ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIIpv6IfDefRouterTime ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIIpv6IfReachableTime ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIIpv6IfRetransmitTime ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIIpv6IfDelayProbeTime ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIIpv6IfPrefixAdvStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIIpv6IfMinRouterAdvTime ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIIpv6IfMaxRouterAdvTime ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIIpv6IfDADRetries ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIIpv6IfForwarding ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsMIIpv6IfIcmpErrInterval ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsMIIpv6IfIcmpTokenBucketSize ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsMIIpv6IfDestUnreachableMsg ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsMIIpv6IfRoutingStatus ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsMIIpv6IfUnnumAssocIPIf ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsMIIpv6IfRedirectMsg ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsMIIpv6IfAdvSrcLLAdr ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsMIIpv6IfAdvIntOpt ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsMIIpv6IfNDProxyAdminStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIIpv6IfNDProxyMode ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIIpv6IfNDProxyOperStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIIpv6IfNDProxyUpStream ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIIpv6IfSENDSecStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIIpv6IfSENDDeltaTimestamp ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIIpv6IfSENDFuzzTimestamp ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIIpv6IfSENDDriftTimestamp ARG_LIST((INT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhGetFsMIIpv6IfDefRoutePreference ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIIpv6IfToken ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIIpv6IfRouterAdvStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIIpv6IfRouterAdvFlags ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIIpv6IfHopLimit ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIIpv6IfDefRouterTime ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIIpv6IfReachableTime ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIIpv6IfRetransmitTime ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIIpv6IfDelayProbeTime ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIIpv6IfPrefixAdvStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIIpv6IfMinRouterAdvTime ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIIpv6IfMaxRouterAdvTime ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIIpv6IfDADRetries ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIIpv6IfForwarding ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsMIIpv6IfIcmpErrInterval ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsMIIpv6IfIcmpTokenBucketSize ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsMIIpv6IfDestUnreachableMsg ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsMIIpv6IfUnnumAssocIPIf ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsMIIpv6IfRedirectMsg ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsMIIpv6IfAdvSrcLLAdr ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsMIIpv6IfAdvIntOpt ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsMIIpv6IfNDProxyAdminStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIIpv6IfNDProxyMode ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIIpv6IfNDProxyUpStream ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIIpv6IfSENDSecStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIIpv6IfSENDDeltaTimestamp ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsMIIpv6IfSENDFuzzTimestamp ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsMIIpv6IfSENDDriftTimestamp ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsMIIpv6IfDefRoutePreference ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIIpv6IfToken ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIIpv6IfRouterAdvStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIIpv6IfRouterAdvFlags ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIIpv6IfHopLimit ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIIpv6IfDefRouterTime ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIIpv6IfReachableTime ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIIpv6IfRetransmitTime ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIIpv6IfDelayProbeTime ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIIpv6IfPrefixAdvStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIIpv6IfMinRouterAdvTime ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIIpv6IfMaxRouterAdvTime ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIIpv6IfDADRetries ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIIpv6IfForwarding ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsMIIpv6IfIcmpErrInterval ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsMIIpv6IfIcmpTokenBucketSize ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsMIIpv6IfDestUnreachableMsg ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsMIIpv6IfUnnumAssocIPIf ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsMIIpv6IfRedirectMsg ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsMIIpv6IfAdvSrcLLAdr ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsMIIpv6IfAdvIntOpt ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsMIIpv6IfNDProxyAdminStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIIpv6IfNDProxyMode ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIIpv6IfNDProxyUpStream ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIIpv6IfSENDSecStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIIpv6IfSENDDeltaTimestamp ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsMIIpv6IfSENDFuzzTimestamp ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsMIIpv6IfSENDDriftTimestamp ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsMIIpv6IfDefRoutePreference ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIIpv6IfTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIIpv6IfStatsTable. */
INT1
nmhValidateIndexInstanceFsMIIpv6IfStatsTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIIpv6IfStatsTable  */

extern INT1
nmhGetFirstIndexFsMIIpv6IfStatsTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

extern INT1
nmhGetNextIndexFsMIIpv6IfStatsTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIIpv6IfStatsTooBigErrors ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIIpv6IfStatsInRouterSols ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIIpv6IfStatsInRouterAdvs ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIIpv6IfStatsInNeighSols ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIIpv6IfStatsInNeighAdvs ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIIpv6IfStatsInRedirects ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIIpv6IfStatsOutRouterSols ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIIpv6IfStatsOutRouterAdvs ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIIpv6IfStatsOutNeighSols ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIIpv6IfStatsOutNeighAdvs ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIIpv6IfStatsOutRedirects ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIIpv6IfStatsLastRouterAdvTime ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIIpv6IfStatsNextRouterAdvTime ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIIpv6IfStatsIcmp6ErrRateLmtd ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIIpv6IfStatsSENDDroppedPkts ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIIpv6IfStatsSENDInvalidPkts ARG_LIST((INT4 ,UINT4 *));

/* Proto Validate Index Instance for FsMIIpv6AddrTable. */
INT1
nmhValidateIndexInstanceFsMIIpv6AddrTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIIpv6AddrTable  */

INT1
nmhGetFirstIndexFsMIIpv6AddrTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE *  , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIIpv6AddrTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIIpv6AddrAdminStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

INT1
nmhGetFsMIIpv6AddrType ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

INT1
nmhGetFsMIIpv6AddrProfIndex ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

INT1
nmhGetFsMIIpv6AddrOperStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

INT1
nmhGetFsMIIpv6AddrContextId ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

INT1
nmhGetFsMIIpv6AddrScope ARG_LIST((UINT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

INT1
nmhGetFsMIIpv6AddrSENDCgaStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

INT1
nmhGetFsMIIpv6AddrSENDCgaModifier ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIIpv6AddrSENDCollisionCount ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIIpv6AddrAdminStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

INT1
nmhSetFsMIIpv6AddrType ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

INT1
nmhSetFsMIIpv6AddrProfIndex ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

INT1
nmhSetFsMIIpv6AddrSENDCgaStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

INT1
nmhSetFsMIIpv6AddrSENDCgaModifier ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIIpv6AddrSENDCollisionCount ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIIpv6AddrAdminStatus ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

INT1
nmhTestv2FsMIIpv6AddrType ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

INT1
nmhTestv2FsMIIpv6AddrProfIndex ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

INT1
nmhTestv2FsMIIpv6AddrSENDCgaStatus ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

INT1
nmhTestv2FsMIIpv6AddrSENDCgaModifier ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIIpv6AddrSENDCollisionCount ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIIpv6AddrTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIIpv6AddrProfileTable. */
INT1
nmhValidateIndexInstanceFsMIIpv6AddrProfileTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIIpv6AddrProfileTable  */

INT1
nmhGetFirstIndexFsMIIpv6AddrProfileTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIIpv6AddrProfileTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIIpv6AddrProfileStatus ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsMIIpv6AddrProfilePrefixAdvStatus ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsMIIpv6AddrProfileOnLinkAdvStatus ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsMIIpv6AddrProfileAutoConfAdvStatus ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsMIIpv6AddrProfilePreferredTime ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsMIIpv6AddrProfileValidTime ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsMIIpv6AddrProfileValidLifeTimeFlag ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsMIIpv6AddrProfilePreferredLifeTimeFlag ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIIpv6AddrProfileStatus ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsMIIpv6AddrProfilePrefixAdvStatus ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsMIIpv6AddrProfileOnLinkAdvStatus ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsMIIpv6AddrProfileAutoConfAdvStatus ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsMIIpv6AddrProfilePreferredTime ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsMIIpv6AddrProfileValidTime ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsMIIpv6AddrProfileValidLifeTimeFlag ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsMIIpv6AddrProfilePreferredLifeTimeFlag ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIIpv6AddrProfileStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsMIIpv6AddrProfilePrefixAdvStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsMIIpv6AddrProfileOnLinkAdvStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsMIIpv6AddrProfileAutoConfAdvStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsMIIpv6AddrProfilePreferredTime ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIIpv6AddrProfileValidTime ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIIpv6AddrProfileValidLifeTimeFlag ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsMIIpv6AddrProfilePreferredLifeTimeFlag ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIIpv6AddrProfileTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIIpv6IcmpStatsTable. */
INT1
nmhValidateIndexInstanceFsMIIpv6IcmpStatsTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIIpv6IcmpStatsTable  */

extern INT1
nmhGetFirstIndexFsMIIpv6IcmpStatsTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

extern INT1
nmhGetNextIndexFsMIIpv6IcmpStatsTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIIpv6IcmpInMsgs ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIIpv6IcmpInErrors ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIIpv6IcmpInDestUnreachs ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIIpv6IcmpInTimeExcds ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIIpv6IcmpInParmProbs ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIIpv6IcmpInPktTooBigs ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIIpv6IcmpInEchos ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIIpv6IcmpInEchoReps ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIIpv6IcmpInRouterSolicits ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIIpv6IcmpInRouterAdvertisements ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIIpv6IcmpInNeighborSolicits ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIIpv6IcmpInNeighborAdvertisements ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIIpv6IcmpInRedirects ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIIpv6IcmpInAdminProhib ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIIpv6IcmpOutMsgs ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIIpv6IcmpOutErrors ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIIpv6IcmpOutDestUnreachs ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIIpv6IcmpOutTimeExcds ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIIpv6IcmpOutParmProbs ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIIpv6IcmpOutPktTooBigs ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIIpv6IcmpOutEchos ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIIpv6IcmpOutEchoReps ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIIpv6IcmpOutRouterSolicits ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIIpv6IcmpOutRouterAdvertisements ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIIpv6IcmpOutNeighborSolicits ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIIpv6IcmpOutNeighborAdvertisements ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIIpv6IcmpOutRedirects ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIIpv6IcmpOutAdminProhib ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIIpv6IcmpInBadCode ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIIpv6IcmpInNARouterFlagSet ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIIpv6IcmpInNASolicitedFlagSet ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIIpv6IcmpInNAOverrideFlagSet ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIIpv6IcmpOutNARouterFlagSet ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIIpv6IcmpOutNASolicitedFlagSet ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIIpv6IcmpOutNAOverrideFlagSet ARG_LIST((INT4 ,UINT4 *));

/* Proto Validate Index Instance for FsMIIpv6PmtuTable. */
INT1
nmhValidateIndexInstanceFsMIIpv6PmtuTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FsMIIpv6PmtuTable  */

INT1
nmhGetFirstIndexFsMIIpv6PmtuTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIIpv6PmtuTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIIpv6Pmtu ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIIpv6PmtuTimeStamp ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIIpv6PmtuAdminStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIIpv6Pmtu ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsMIIpv6PmtuAdminStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIIpv6Pmtu ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsMIIpv6PmtuAdminStatus ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIIpv6PmtuTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIIpv6RouteTable. */
INT1
nmhValidateIndexInstanceFsMIIpv6RouteTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FsMIIpv6RouteTable  */

INT1
nmhGetFirstIndexFsMIIpv6RouteTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE *  , INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIIpv6RouteTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIIpv6RouteIfIndex ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIIpv6RouteMetric ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetFsMIIpv6RouteType ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIIpv6RouteTag ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetFsMIIpv6RouteAge ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIIpv6RouteAdminStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIIpv6RouteIfIndex ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsMIIpv6RouteMetric ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

INT1
nmhSetFsMIIpv6RouteType ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsMIIpv6RouteTag ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

INT1
nmhSetFsMIIpv6RouteAdminStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIIpv6RouteIfIndex ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsMIIpv6RouteMetric ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

INT1
nmhTestv2FsMIIpv6RouteType ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsMIIpv6RouteTag ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

INT1
nmhTestv2FsMIIpv6RouteAdminStatus ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIIpv6RouteTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIIpv6PrefTable. */
INT1
nmhValidateIndexInstanceFsMIIpv6PrefTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIIpv6PrefTable  */

INT1
nmhGetFirstIndexFsMIIpv6PrefTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIIpv6PrefTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIIpv6Preference ARG_LIST((INT4  , INT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIIpv6Preference ARG_LIST((INT4  , INT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIIpv6Preference ARG_LIST((UINT4 *  ,INT4  , INT4  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIIpv6PrefTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIIpv6NDProxyListTable. */
INT1
nmhValidateIndexInstanceFsMIIpv6NDProxyListTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FsMIIpv6NDProxyListTable  */

INT1
nmhGetFirstIndexFsMIIpv6NDProxyListTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIIpv6NDProxyListTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIIpv6NDProxyAdminStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIIpv6NDProxyAdminStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIIpv6NDProxyAdminStatus ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIIpv6NDProxyListTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIIpv6PingTable. */
INT1
nmhValidateIndexInstanceFsMIIpv6PingTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIIpv6PingTable  */

INT1
nmhGetFirstIndexFsMIIpv6PingTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIIpv6PingTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIIpv6PingDest ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIIpv6PingIfIndex ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIIpv6PingContextId ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIIpv6PingAdminStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIIpv6PingInterval ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIIpv6PingRcvTimeout ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIIpv6PingTries ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIIpv6PingSize ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIIpv6PingSentCount ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIIpv6PingAverageTime ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIIpv6PingMaxTime ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIIpv6PingMinTime ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIIpv6PingOperStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIIpv6PingSuccesses ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIIpv6PingPercentageLoss ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIIpv6PingData ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIIpv6PingSrcAddr ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIIpv6PingZoneId ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIIpv6PingDestAddrType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIIpv6PingHostName ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIIpv6PingDest ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIIpv6PingIfIndex ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIIpv6PingContextId ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIIpv6PingAdminStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIIpv6PingInterval ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIIpv6PingRcvTimeout ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIIpv6PingTries ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIIpv6PingSize ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIIpv6PingData ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIIpv6PingSrcAddr ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIIpv6PingZoneId ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIIpv6PingDestAddrType ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIIpv6PingHostName ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIIpv6PingDest ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIIpv6PingIfIndex ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIIpv6PingContextId ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIIpv6PingAdminStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIIpv6PingInterval ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIIpv6PingRcvTimeout ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIIpv6PingTries ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIIpv6PingSize ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIIpv6PingData ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIIpv6PingSrcAddr ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIIpv6PingZoneId ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIIpv6PingDestAddrType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIIpv6PingHostName ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIIpv6PingTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIIpv6GlobalDebug ARG_LIST((UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIIpv6GlobalDebug ARG_LIST((UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIIpv6GlobalDebug ARG_LIST((UINT4 *  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIIpv6GlobalDebug ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIIpv6AddrSelPolicyTable. */
INT1
nmhValidateIndexInstanceFsMIIpv6AddrSelPolicyTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIIpv6AddrSelPolicyTable  */

INT1
nmhGetFirstIndexFsMIIpv6AddrSelPolicyTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *  , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIIpv6AddrSelPolicyTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIIpv6AddrSelPolicyScope ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIIpv6AddrSelPolicyPrecedence ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIIpv6AddrSelPolicyLabel ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIIpv6AddrSelPolicyAddrType ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIIpv6AddrSelPolicyIsPublicAddr ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIIpv6AddrSelPolicyIsSelfAddr ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIIpv6AddrSelPolicyReachabilityStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIIpv6AddrSelPolicyConfigStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIIpv6AddrSelPolicyRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIIpv6AddrSelPolicyPrecedence ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  ,INT4 ));

INT1
nmhSetFsMIIpv6AddrSelPolicyLabel ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  ,INT4 ));

INT1
nmhSetFsMIIpv6AddrSelPolicyAddrType ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  ,INT4 ));

INT1
nmhSetFsMIIpv6AddrSelPolicyRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIIpv6AddrSelPolicyPrecedence ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsMIIpv6AddrSelPolicyLabel ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsMIIpv6AddrSelPolicyAddrType ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsMIIpv6AddrSelPolicyRowStatus ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIIpv6AddrSelPolicyTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIIpv6IfScopeZoneMapTable. */
INT1
nmhValidateIndexInstanceFsMIIpv6IfScopeZoneMapTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIIpv6IfScopeZoneMapTable  */

INT1
nmhGetFirstIndexFsMIIpv6IfScopeZoneMapTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIIpv6IfScopeZoneMapTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIIpv6ScopeZoneIndexInterfaceLocal ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIIpv6ScopeZoneIndexLinkLocal ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIIpv6ScopeZoneIndex3 ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIIpv6ScopeZoneIndexAdminLocal ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIIpv6ScopeZoneIndexSiteLocal ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIIpv6ScopeZoneIndex6 ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIIpv6ScopeZoneIndex7 ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIIpv6ScopeZoneIndexOrganizationLocal ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIIpv6ScopeZoneIndex9 ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIIpv6ScopeZoneIndexA ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIIpv6ScopeZoneIndexB ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIIpv6ScopeZoneIndexC ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIIpv6ScopeZoneIndexD ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIIpv6ScopeZoneIndexE ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIIpv6IfScopeZoneCreationStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIIpv6IfScopeZoneRowStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIIpv6ScopeZoneIndexInterfaceLocal ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIIpv6ScopeZoneIndexLinkLocal ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIIpv6ScopeZoneIndex3 ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIIpv6ScopeZoneIndexAdminLocal ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIIpv6ScopeZoneIndexSiteLocal ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIIpv6ScopeZoneIndex6 ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIIpv6ScopeZoneIndex7 ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIIpv6ScopeZoneIndexOrganizationLocal ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIIpv6ScopeZoneIndex9 ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIIpv6ScopeZoneIndexA ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIIpv6ScopeZoneIndexB ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIIpv6ScopeZoneIndexC ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIIpv6ScopeZoneIndexD ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIIpv6ScopeZoneIndexE ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIIpv6IfScopeZoneRowStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIIpv6ScopeZoneIndexInterfaceLocal ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIIpv6ScopeZoneIndexLinkLocal ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIIpv6ScopeZoneIndex3 ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIIpv6ScopeZoneIndexAdminLocal ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIIpv6ScopeZoneIndexSiteLocal ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIIpv6ScopeZoneIndex6 ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIIpv6ScopeZoneIndex7 ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIIpv6ScopeZoneIndexOrganizationLocal ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIIpv6ScopeZoneIndex9 ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIIpv6ScopeZoneIndexA ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIIpv6ScopeZoneIndexB ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIIpv6ScopeZoneIndexC ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIIpv6ScopeZoneIndexD ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIIpv6ScopeZoneIndexE ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIIpv6IfScopeZoneRowStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIIpv6IfScopeZoneMapTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIIpv6ScopeZoneTable. */
INT1
nmhValidateIndexInstanceFsMIIpv6ScopeZoneTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FsMIIpv6ScopeZoneTable  */

INT1
nmhGetFirstIndexFsMIIpv6ScopeZoneTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIIpv6ScopeZoneTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIIpv6ScopeZoneIndex ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetFsMIIpv6ScopeZoneCreationStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIIpv6ScopeZoneInterfaceList ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIIpv6IsDefaultScopeZone ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIIpv6IsDefaultScopeZone ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIIpv6IsDefaultScopeZone ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIIpv6ScopeZoneTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIIpv6RARouteInfoTable. */
INT1
nmhValidateIndexInstanceFsMIIpv6RARouteInfoTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIIpv6RARouteInfoTable  */

INT1
nmhGetFirstIndexFsMIIpv6RARouteInfoTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE *  , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIIpv6RARouteInfoTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIIpv6RARoutePreference ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

INT1
nmhGetFsMIIpv6RARouteLifetime ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,UINT4 *));

INT1
nmhGetFsMIIpv6RARouteRowStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIIpv6RARoutePreference ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

INT1
nmhSetFsMIIpv6RARouteLifetime ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,UINT4 ));

INT1
nmhSetFsMIIpv6RARouteRowStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIIpv6RARoutePreference ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

INT1
nmhTestv2FsMIIpv6RARouteLifetime ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,UINT4 ));

INT1
nmhTestv2FsMIIpv6RARouteRowStatus ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIIpv6RARouteInfoTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIIpv6TestRedEntryTime ARG_LIST((INT4 *));

INT1
nmhGetFsMIIpv6TestRedExitTime ARG_LIST((INT4 *));

/* Proto Validate Index Instance for FsMIIpv6IfRaRDNSSTable. */
INT1
nmhValidateIndexInstanceFsMIIpv6IfRaRDNSSTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIIpv6IfRaRDNSSTable  */

INT1
nmhGetFirstIndexFsMIIpv6IfRaRDNSSTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIIpv6IfRaRDNSSTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIIpv6IfRadvRDNSSOpen ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIIpv6IfRaRDNSSPreference ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIIpv6IfRaRDNSSLifetime ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIIpv6IfRaRDNSSAddrOne ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIIpv6IfRaRDNSSAddrTwo ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIIpv6IfRaRDNSSAddrThree ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIIpv6IfRaRDNSSRowStatus ARG_LIST((INT4 ,INT4 *));


INT1
nmhGetFsMIIpv6IfDomainNameOne ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIIpv6IfDomainNameTwo ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIIpv6IfDomainNameThree ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIIpv6IfDnsLifeTime ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIIpv6IfRaRDNSSAddrOneLife ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIIpv6IfRaRDNSSAddrTwoLife ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIIpv6IfRaRDNSSAddrThreeLife ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIIpv6IfDomainNameOneLife ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIIpv6IfDomainNameTwoLife ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIIpv6IfDomainNameThreeLife ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIIpv6IfRadvRDNSSOpen ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIIpv6IfRaRDNSSPreference ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIIpv6IfRaRDNSSLifetime ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsMIIpv6IfRaRDNSSAddrOne ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIIpv6IfRaRDNSSAddrTwo ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIIpv6IfRaRDNSSAddrThree ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIIpv6IfRaRDNSSRowStatus ARG_LIST((INT4  ,INT4 ));


INT1
nmhSetFsMIIpv6IfDomainNameOne ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIIpv6IfDomainNameTwo ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIIpv6IfDomainNameThree ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIIpv6IfDnsLifeTime ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIIpv6IfRaRDNSSAddrOneLife ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsMIIpv6IfRaRDNSSAddrTwoLife ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsMIIpv6IfRaRDNSSAddrThreeLife ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsMIIpv6IfDomainNameOneLife ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIIpv6IfDomainNameTwoLife ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIIpv6IfDomainNameThreeLife ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIIpv6IfRadvRDNSSOpen ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIIpv6IfRaRDNSSPreference ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIIpv6IfRaRDNSSLifetime ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsMIIpv6IfRaRDNSSAddrOne ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIIpv6IfRaRDNSSAddrTwo ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIIpv6IfRaRDNSSAddrThree ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIIpv6IfRaRDNSSRowStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));


INT1
nmhTestv2FsMIIpv6IfDomainNameOne ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIIpv6IfDomainNameTwo ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIIpv6IfDomainNameThree ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIIpv6IfDnsLifeTime ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIIpv6IfRaRDNSSAddrOneLife ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsMIIpv6IfRaRDNSSAddrTwoLife ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsMIIpv6IfRaRDNSSAddrThreeLife ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsMIIpv6IfDomainNameOneLife ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIIpv6IfDomainNameTwoLife ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIIpv6IfDomainNameThreeLife ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIIpv6IfRaRDNSSTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
