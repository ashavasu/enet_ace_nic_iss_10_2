
 /** $Id: stdip6wr.h,v 1.5 2015/01/14 12:33:41 siva Exp $*/

#ifndef _STDIP6WRAP_H 
#define _STDIP6WRAP_H
#include "ipv6.h"
#include "fssocket.h"
#ifdef LNXIP6_WANTED
#include "cfa.h"
#include "ip.h"
#include "lip6mac.h"
#include "lip6tdfs.h"
#endif
VOID RegisterSTDIP6(VOID);
INT4 Ipv6ForwardingTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ipv6ForwardingSet (tSnmpIndex *, tRetVal *);
INT4 Ipv6ForwardingGet (tSnmpIndex *, tRetVal *);
INT4 Ipv6DefaultHopLimitTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ipv6ForwardingDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 Ipv6DefaultHopLimitDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 Ipv6DefaultHopLimitSet (tSnmpIndex *, tRetVal *);
INT4 Ipv6DefaultHopLimitGet (tSnmpIndex *, tRetVal *);
INT4 Ipv6InterfacesGet (tSnmpIndex *, tRetVal *);
INT4 Ipv6IfTableLastChangeGet (tSnmpIndex *, tRetVal *);
INT4 Ipv6IfIndexGet (tSnmpIndex *, tRetVal *);
INT4 Ipv6IfDescrTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ipv6IfDescrSet (tSnmpIndex *, tRetVal *);
INT4 Ipv6IfDescrGet (tSnmpIndex *, tRetVal *);
INT4 Ipv6IfLowerLayerGet (tSnmpIndex *, tRetVal *);
INT4 Ipv6IfEffectiveMtuGet (tSnmpIndex *, tRetVal *);
INT4 Ipv6IfReasmMaxSizeGet (tSnmpIndex *, tRetVal *);
INT4 Ipv6IfIdentifierTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ipv6IfIdentifierSet (tSnmpIndex *, tRetVal *);
INT4 Ipv6IfIdentifierGet (tSnmpIndex *, tRetVal *);
INT4 Ipv6IfIdentifierLengthTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ipv6IfIdentifierLengthSet (tSnmpIndex *, tRetVal *);
INT4 Ipv6IfIdentifierLengthGet (tSnmpIndex *, tRetVal *);
INT4 Ipv6IfPhysicalAddressGet (tSnmpIndex *, tRetVal *);
INT4 Ipv6IfAdminStatusTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ipv6IfTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 Ipv6IfAdminStatusSet (tSnmpIndex *, tRetVal *);
INT4 Ipv6IfAdminStatusGet (tSnmpIndex *, tRetVal *);
INT4 Ipv6IfOperStatusGet (tSnmpIndex *, tRetVal *);
INT4 Ipv6IfLastChangeGet (tSnmpIndex *, tRetVal *);
INT4 Ipv6IfStatsInReceivesGet (tSnmpIndex *, tRetVal *);
INT4 Ipv6IfStatsInHdrErrorsGet (tSnmpIndex *, tRetVal *);
INT4 Ipv6IfStatsInTooBigErrorsGet (tSnmpIndex *, tRetVal *);
INT4 Ipv6IfStatsInNoRoutesGet (tSnmpIndex *, tRetVal *);
INT4 Ipv6IfStatsInAddrErrorsGet (tSnmpIndex *, tRetVal *);
INT4 Ipv6IfStatsInUnknownProtosGet (tSnmpIndex *, tRetVal *);
INT4 Ipv6IfStatsInTruncatedPktsGet (tSnmpIndex *, tRetVal *);
INT4 Ipv6IfStatsInDiscardsGet (tSnmpIndex *, tRetVal *);
INT4 Ipv6IfStatsInDeliversGet (tSnmpIndex *, tRetVal *);
INT4 Ipv6IfStatsOutForwDatagramsGet (tSnmpIndex *, tRetVal *);
INT4 Ipv6IfStatsOutRequestsGet (tSnmpIndex *, tRetVal *);
INT4 Ipv6IfStatsOutDiscardsGet (tSnmpIndex *, tRetVal *);
INT4 Ipv6IfStatsOutFragOKsGet (tSnmpIndex *, tRetVal *);
INT4 Ipv6IfStatsOutFragFailsGet (tSnmpIndex *, tRetVal *);
INT4 Ipv6IfStatsOutFragCreatesGet (tSnmpIndex *, tRetVal *);
INT4 Ipv6IfStatsReasmReqdsGet (tSnmpIndex *, tRetVal *);
INT4 Ipv6IfStatsReasmOKsGet (tSnmpIndex *, tRetVal *);
INT4 Ipv6IfStatsReasmFailsGet (tSnmpIndex *, tRetVal *);
INT4 Ipv6IfStatsInMcastPktsGet (tSnmpIndex *, tRetVal *);
INT4 Ipv6IfStatsOutMcastPktsGet (tSnmpIndex *, tRetVal *);
INT4 Ipv6AddrPrefixGet (tSnmpIndex *, tRetVal *);
INT4 Ipv6AddrPrefixLengthGet (tSnmpIndex *, tRetVal *);
INT4 Ipv6AddrPrefixOnLinkFlagGet (tSnmpIndex *, tRetVal *);
INT4 Ipv6AddrPrefixAutonomousFlagGet (tSnmpIndex *, tRetVal *);
INT4 Ipv6AddrPrefixAdvPreferredLifetimeGet (tSnmpIndex *, tRetVal *);
INT4 Ipv6AddrPrefixAdvValidLifetimeGet (tSnmpIndex *, tRetVal *);
INT4 Ipv6AddrAddressGet (tSnmpIndex *, tRetVal *);
INT4 Ipv6AddrPfxLengthGet (tSnmpIndex *, tRetVal *);
INT4 Ipv6AddrTypeGet (tSnmpIndex *, tRetVal *);
INT4 Ipv6AddrAnycastFlagGet (tSnmpIndex *, tRetVal *);
INT4 Ipv6AddrStatusGet (tSnmpIndex *, tRetVal *);
INT4 Ipv6RouteNumberGet (tSnmpIndex *, tRetVal *);
INT4 Ipv6DiscardedRoutesGet (tSnmpIndex *, tRetVal *);
INT4 Ipv6RouteDestGet (tSnmpIndex *, tRetVal *);
INT4 Ipv6RoutePfxLengthGet (tSnmpIndex *, tRetVal *);
INT4 Ipv6RouteIndexGet (tSnmpIndex *, tRetVal *);
INT4 Ipv6RouteIfIndexGet (tSnmpIndex *, tRetVal *);
INT4 Ipv6RouteNextHopGet (tSnmpIndex *, tRetVal *);
INT4 Ipv6RouteTypeGet (tSnmpIndex *, tRetVal *);
INT4 Ipv6RouteProtocolGet (tSnmpIndex *, tRetVal *);
INT4 Ipv6RoutePolicyGet (tSnmpIndex *, tRetVal *);
INT4 Ipv6RouteAgeGet (tSnmpIndex *, tRetVal *);
INT4 Ipv6RouteNextHopRDIGet (tSnmpIndex *, tRetVal *);
INT4 Ipv6RouteMetricGet (tSnmpIndex *, tRetVal *);
INT4 Ipv6RouteWeightGet (tSnmpIndex *, tRetVal *);
INT4 Ipv6RouteInfoGet (tSnmpIndex *, tRetVal *);
INT4 Ipv6RouteValidTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ipv6RouteTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 Ipv6RouteValidSet (tSnmpIndex *, tRetVal *);
INT4 Ipv6RouteValidGet (tSnmpIndex *, tRetVal *);
INT4 Ipv6NetToMediaNetAddressGet (tSnmpIndex *, tRetVal *);
INT4 Ipv6NetToMediaPhysAddressGet (tSnmpIndex *, tRetVal *);
INT4 Ipv6NetToMediaTypeGet (tSnmpIndex *, tRetVal *);
INT4 Ipv6IfNetToMediaStateGet (tSnmpIndex *, tRetVal *);
INT4 Ipv6IfNetToMediaLastUpdatedGet (tSnmpIndex *, tRetVal *);
INT4 Ipv6NetToMediaValidTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ipv6NetToMediaTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 Ipv6NetToMediaValidSet (tSnmpIndex *, tRetVal *);
INT4 Ipv6NetToMediaValidGet (tSnmpIndex *, tRetVal *);

 /*  GetNext Function Prototypes */

INT4 GetNextIndexIpv6NetToMediaTable( tSnmpIndex *, tSnmpIndex *);

INT4 GetNextIndexIpv6RouteTable( tSnmpIndex *, tSnmpIndex *);

INT4 GetNextIndexIpv6AddrTable( tSnmpIndex *, tSnmpIndex *);

INT4 GetNextIndexIpv6AddrPrefixTable( tSnmpIndex *, tSnmpIndex *);

INT4 GetNextIndexIpv6IfStatsTable( tSnmpIndex *, tSnmpIndex *);

INT4 GetNextIndexIpv6IfTable( tSnmpIndex *, tSnmpIndex *);

INT4 SetIpv6IfAddress PROTO ((INT4 ,tIp6Addr ,INT4 ));

tIp6AddrSelPolicy   *Ip6PolicyPrefixCreate PROTO ((UINT4 u4IfIndex,
        tIp6Addr * pPolicyPrefix,
               UINT1 u1PrefixLen,
                                                  UINT1 u1RowStatus));

tIp6AddrSelPolicy  *Ip6GetPolicyPrefix  PROTO ((UINT4 u4IfIndex,
                                                tIp6Addr * pPolicyPrefix,
                                                UINT1 u1PrefixLen));

tIp6AddrSelPolicy  *Ip6GetFirstPolicyPrefix PROTO ((UINT4 u4IfIndex));

tIp6AddrSelPolicy  *Ip6GetNextPolicyPrefix PROTO ((UINT4 u4IfIndex,
                                                   tIp6Addr * pIp6Addr,
                                                   UINT1 u1PrefixLen));

INT1 Ip6PolicyPrefixDelete  PROTO ((UINT4 u4IfIndex,
                                   tIp6Addr * pPolicyPrefix,
                                   UINT1 u1PrefixLen));

INT1 Ip6IsAddrPublic PROTO ((tIp6Addr Addr1));


VOID
IncMsrForIp6PolicyPrefixTable  PROTO ((INT4 i4IfIndex,tSNMP_OCTET_STRING_TYPE
                                     * pIpv6Address, INT4 i4PrefixLen, CHR1 cDatatype,
                                       VOID *pSetVal, UINT4 *pu4ObjectId,
                                       UINT4 u4OIdLen, UINT1 IsRowStatus));

INT1 Ip6addrProfEntryExists PROTO ((UINT4 u4AddrprofIndex));

INT4 Ip6PolicyPrefixInfoCmp PROTO ((tRBElem * pRBElem1, tRBElem * pRBElem2));
#define ONE               1

#define  IP6_NAME         "IP6"

/*#define  IP6_OFFSET(x,y)        FSAP_OFFSETOF(x,y)*/

#define  IP6_ADDR_PREFIX_ADV           1
#define  IP6_ADDR_ONLINK_ADV           2
#define  IP6_ADDR_AUTO_ADV             4

# define IP6_MAX_DEFAULT_POLICY_ENTRIES 5   /* Default entries in the policy
                                               table*/
#define IP6_SELF_ADDR             1   /*Address configured on the given interface*/
#define IP6_NOT_SELF_ADDR         2   /*Address not configured on the given interface*/
#define IP6_DEFAULT_POLICY_ENTRY  3   /*Default address entry */

#define IP6_POLICY_ENTRY_REACHABLE   1   /*Reachability status of the entry */
#define IP6_POLICY_ENTRY_UNREACHABLE 2

#define IP6_POLICY_STATUS_AUTO          1  /*Default entry in the table */
#define IP6_POLICY_STATUS_MANUAL        2  /*Configured entry */

#define IP6_ADDR_VALID_TIME_FLAG       8
#define IP6_ADDR_PREF_TIME_FLAG       16
#define IP6_ADDR_PROF_REF_COUNT(i)   gIp6GblInfo.apIp6AddrProfile[i]->u2RefCnt
#define IP6_ADDR_PROF_ADMIN(i)       gIp6GblInfo.apIp6AddrProfile[i]->u1AdminStatus
#define IP6_ADDR_PROF_CONF(i)        gIp6GblInfo.apIp6AddrProfile[i]->u1RaPrefCnf
#define IP6_ADDR_PREF_TIME(i)        gIp6GblInfo.apIp6AddrProfile[i]->u4PrefLife
#define IP6_ADDR_VALID_TIME(i)       gIp6GblInfo.apIp6AddrProfile[i]->u4ValidLife



#define IP6_ADDR_ON_LINK(i)     \
        ((gIp6GblInfo.apIp6AddrProfile[i]->u1RaPrefCnf) & IP6_ADDR_ONLINK_ADV)

#define IP6_ADDR_AUTO_CONFIG(i) \
        ((gIp6GblInfo.apIp6AddrProfile[i]->u1RaPrefCnf) & IP6_ADDR_AUTO_ADV)

#define IP6_ADDR_SEND_PREFIX(i) \
        ((gIp6GblInfo.apIp6AddrProfile[i]->u1RaPrefCnf) & IP6_ADDR_PREFIX_ADV)

#define IP6_ADDR_PREF_TIME_FIXED(i) \
        ((gIp6GblInfo.apIp6AddrProfile[i]->u1RaPrefCnf) & \
         IP6_ADDR_PREF_TIME_FLAG)

#define IP6_ADDR_VALID_TIME_FIXED(i)     \
        ((gIp6GblInfo.apIp6AddrProfile[i]->u1RaPrefCnf) & \
         IP6_ADDR_VALID_TIME_FLAG)


#define IP6_ADDR_DEF_RA_PREF_CNF (IP6_ADDR_PREFIX_ADV | IP6_ADDR_ONLINK_ADV |\
                                 IP6_ADDR_AUTO_ADV | IP6_ADDR_VALID_TIME_FLAG |\
                                 IP6_ADDR_PREF_TIME_FLAG )


#define  ADDR6_LLOCAL_PREFIX_1         0xFE
#define  ADDR6_LLOCAL_PREFIX_2         0x80

#define IP6_LLADDR_PTR_FROM_SLL(sll)  \
        (tIp6LlocalInfo *) (VOID *)\
        ((UINT1 *)(sll) - IP6_OFFSET(tIp6LlocalInfo, nextLif))

#define IP6_ADDR_PTR_FROM_SLL(sll)  \
        (tIp6AddrInfo *) (VOID *)\
        ((UINT1 *)(sll) - IP6_OFFSET(tIp6AddrInfo, nextAif))

#define  IP6_RB_GREATER               1
#define  IP6_RB_LESSER               -1
#define  IP6_RB_EQUAL                 0

#define  IP6_L2VLAN_INTERFACE_TYPE    CFA_L2VLAN

#ifdef LNXIP6_WANTED
typedef struct _IP6_LINK_LOCAL_INFORMATION
{
    /* configurable parameters */

    tIp6Addr  ip6Addr;        /* IPv6 address */

    /* Runtime parameters (status, statistics ...) */

    tLip6If       *pIf6;       /* Pointer to interface */
    tTMO_SLL_NODE  nextLif;   /* Pointer to next link-local
                               * address on this interface
                               *                                *                                */
    UINT2       u2DadSent;    /* Number of DAD NS sent */
    UINT1       u1Status;     /* Status of the address */
    UINT1       u1ConfigMethod; /* Manual or STATELESS Auto Configuration */
    UINT1       u1AdminStatus;  /* Administrative Status */
    UINT1       u1Pad[3];     /* Padding */
    tIp6Timer   dadTimer;     /* Timer Node for DAD */

}
tIp6LlocalInfo;
#else
typedef struct _IP6_LINK_LOCAL_INFORMATION
{
    /* configurable parameters */

    tIp6Addr  ip6Addr;        /* IPv6 address */

    /* Runtime parameters (status, statistics ...) */

    tIp6If       *pIf6;       /* Pointer to interface */
    tTMO_SLL_NODE  nextLif;   /* Pointer to next link-local
                               * address on this interface
                               */
    UINT2       u2DadSent;    /* Number of DAD NS sent */
    UINT1       u1Status;     /* Status of the address */
    UINT1       u1ConfigMethod; /* Manual or STATELESS Auto Configuration */
    UINT1       u1AdminStatus;  /* Administrative Status */
    UINT1       u1Pad[2];     /* Padding */
    BOOL1       b1SeNDCgaStatus; /* SeND Cga Status */
    tIp6Timer   dadTimer;     /* Timer Node for DAD */
    tCgaOptions cgaParams;      /* CGA Parameter Data Structure */

}
tIp6LlocalInfo;
#endif

#ifndef LNXIP6_WANTED
INT4    Ip6IsMyAddr             PROTO ((tIp6Addr *pIp6Addr, tIp6If *pIf6));
#else
INT4    Ip6IsMyAddr             PROTO((tIp6Addr *pIp6Addr,tLip6If * pIf6));
#endif

#endif
