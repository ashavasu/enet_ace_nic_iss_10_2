/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdip6db.h,v 1.3 2010/08/02 06:56:48 prabuc Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _STDIP6DB_H
#define _STDIP6DB_H

UINT1 Ipv6IfTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 Ipv6IfStatsTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 Ipv6AddrPrefixTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER};
UINT1 Ipv6AddrTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 Ipv6RouteTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Ipv6NetToMediaTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_OCTET_PRIM};

UINT4 stdip6 [] ={1,3,6,1,2,1,55};
tSNMP_OID_TYPE stdip6OID = {7, stdip6};


UINT4 Ipv6Forwarding [ ] ={1,3,6,1,2,1,55,1,1};
UINT4 Ipv6DefaultHopLimit [ ] ={1,3,6,1,2,1,55,1,2};
UINT4 Ipv6Interfaces [ ] ={1,3,6,1,2,1,55,1,3};
UINT4 Ipv6IfTableLastChange [ ] ={1,3,6,1,2,1,55,1,4};
UINT4 Ipv6IfIndex [ ] ={1,3,6,1,2,1,55,1,5,1,1};
UINT4 Ipv6IfDescr [ ] ={1,3,6,1,2,1,55,1,5,1,2};
UINT4 Ipv6IfLowerLayer [ ] ={1,3,6,1,2,1,55,1,5,1,3};
UINT4 Ipv6IfEffectiveMtu [ ] ={1,3,6,1,2,1,55,1,5,1,4};
UINT4 Ipv6IfReasmMaxSize [ ] ={1,3,6,1,2,1,55,1,5,1,5};
UINT4 Ipv6IfIdentifier [ ] ={1,3,6,1,2,1,55,1,5,1,6};
UINT4 Ipv6IfIdentifierLength [ ] ={1,3,6,1,2,1,55,1,5,1,7};
UINT4 Ipv6IfPhysicalAddress [ ] ={1,3,6,1,2,1,55,1,5,1,8};
UINT4 Ipv6IfAdminStatus [ ] ={1,3,6,1,2,1,55,1,5,1,9};
UINT4 Ipv6IfOperStatus [ ] ={1,3,6,1,2,1,55,1,5,1,10};
UINT4 Ipv6IfLastChange [ ] ={1,3,6,1,2,1,55,1,5,1,11};
UINT4 Ipv6IfStatsInReceives [ ] ={1,3,6,1,2,1,55,1,6,1,1};
UINT4 Ipv6IfStatsInHdrErrors [ ] ={1,3,6,1,2,1,55,1,6,1,2};
UINT4 Ipv6IfStatsInTooBigErrors [ ] ={1,3,6,1,2,1,55,1,6,1,3};
UINT4 Ipv6IfStatsInNoRoutes [ ] ={1,3,6,1,2,1,55,1,6,1,4};
UINT4 Ipv6IfStatsInAddrErrors [ ] ={1,3,6,1,2,1,55,1,6,1,5};
UINT4 Ipv6IfStatsInUnknownProtos [ ] ={1,3,6,1,2,1,55,1,6,1,6};
UINT4 Ipv6IfStatsInTruncatedPkts [ ] ={1,3,6,1,2,1,55,1,6,1,7};
UINT4 Ipv6IfStatsInDiscards [ ] ={1,3,6,1,2,1,55,1,6,1,8};
UINT4 Ipv6IfStatsInDelivers [ ] ={1,3,6,1,2,1,55,1,6,1,9};
UINT4 Ipv6IfStatsOutForwDatagrams [ ] ={1,3,6,1,2,1,55,1,6,1,10};
UINT4 Ipv6IfStatsOutRequests [ ] ={1,3,6,1,2,1,55,1,6,1,11};
UINT4 Ipv6IfStatsOutDiscards [ ] ={1,3,6,1,2,1,55,1,6,1,12};
UINT4 Ipv6IfStatsOutFragOKs [ ] ={1,3,6,1,2,1,55,1,6,1,13};
UINT4 Ipv6IfStatsOutFragFails [ ] ={1,3,6,1,2,1,55,1,6,1,14};
UINT4 Ipv6IfStatsOutFragCreates [ ] ={1,3,6,1,2,1,55,1,6,1,15};
UINT4 Ipv6IfStatsReasmReqds [ ] ={1,3,6,1,2,1,55,1,6,1,16};
UINT4 Ipv6IfStatsReasmOKs [ ] ={1,3,6,1,2,1,55,1,6,1,17};
UINT4 Ipv6IfStatsReasmFails [ ] ={1,3,6,1,2,1,55,1,6,1,18};
UINT4 Ipv6IfStatsInMcastPkts [ ] ={1,3,6,1,2,1,55,1,6,1,19};
UINT4 Ipv6IfStatsOutMcastPkts [ ] ={1,3,6,1,2,1,55,1,6,1,20};
UINT4 Ipv6AddrPrefix [ ] ={1,3,6,1,2,1,55,1,7,1,1};
UINT4 Ipv6AddrPrefixLength [ ] ={1,3,6,1,2,1,55,1,7,1,2};
UINT4 Ipv6AddrPrefixOnLinkFlag [ ] ={1,3,6,1,2,1,55,1,7,1,3};
UINT4 Ipv6AddrPrefixAutonomousFlag [ ] ={1,3,6,1,2,1,55,1,7,1,4};
UINT4 Ipv6AddrPrefixAdvPreferredLifetime [ ] ={1,3,6,1,2,1,55,1,7,1,5};
UINT4 Ipv6AddrPrefixAdvValidLifetime [ ] ={1,3,6,1,2,1,55,1,7,1,6};
UINT4 Ipv6AddrAddress [ ] ={1,3,6,1,2,1,55,1,8,1,1};
UINT4 Ipv6AddrPfxLength [ ] ={1,3,6,1,2,1,55,1,8,1,2};
UINT4 Ipv6AddrType [ ] ={1,3,6,1,2,1,55,1,8,1,3};
UINT4 Ipv6AddrAnycastFlag [ ] ={1,3,6,1,2,1,55,1,8,1,4};
UINT4 Ipv6AddrStatus [ ] ={1,3,6,1,2,1,55,1,8,1,5};
UINT4 Ipv6RouteNumber [ ] ={1,3,6,1,2,1,55,1,9};
UINT4 Ipv6DiscardedRoutes [ ] ={1,3,6,1,2,1,55,1,10};
UINT4 Ipv6RouteDest [ ] ={1,3,6,1,2,1,55,1,11,1,1};
UINT4 Ipv6RoutePfxLength [ ] ={1,3,6,1,2,1,55,1,11,1,2};
UINT4 Ipv6RouteIndex [ ] ={1,3,6,1,2,1,55,1,11,1,3};
UINT4 Ipv6RouteIfIndex [ ] ={1,3,6,1,2,1,55,1,11,1,4};
UINT4 Ipv6RouteNextHop [ ] ={1,3,6,1,2,1,55,1,11,1,5};
UINT4 Ipv6RouteType [ ] ={1,3,6,1,2,1,55,1,11,1,6};
UINT4 Ipv6RouteProtocol [ ] ={1,3,6,1,2,1,55,1,11,1,7};
UINT4 Ipv6RoutePolicy [ ] ={1,3,6,1,2,1,55,1,11,1,8};
UINT4 Ipv6RouteAge [ ] ={1,3,6,1,2,1,55,1,11,1,9};
UINT4 Ipv6RouteNextHopRDI [ ] ={1,3,6,1,2,1,55,1,11,1,10};
UINT4 Ipv6RouteMetric [ ] ={1,3,6,1,2,1,55,1,11,1,11};
UINT4 Ipv6RouteWeight [ ] ={1,3,6,1,2,1,55,1,11,1,12};
UINT4 Ipv6RouteInfo [ ] ={1,3,6,1,2,1,55,1,11,1,13};
UINT4 Ipv6RouteValid [ ] ={1,3,6,1,2,1,55,1,11,1,14};
UINT4 Ipv6NetToMediaNetAddress [ ] ={1,3,6,1,2,1,55,1,12,1,1};
UINT4 Ipv6NetToMediaPhysAddress [ ] ={1,3,6,1,2,1,55,1,12,1,2};
UINT4 Ipv6NetToMediaType [ ] ={1,3,6,1,2,1,55,1,12,1,3};
UINT4 Ipv6IfNetToMediaState [ ] ={1,3,6,1,2,1,55,1,12,1,4};
UINT4 Ipv6IfNetToMediaLastUpdated [ ] ={1,3,6,1,2,1,55,1,12,1,5};
UINT4 Ipv6NetToMediaValid [ ] ={1,3,6,1,2,1,55,1,12,1,6};


tMbDbEntry stdip6MibEntry[]= {

{{9,Ipv6Forwarding}, NULL, Ipv6ForwardingGet, Ipv6ForwardingSet, Ipv6ForwardingTest, Ipv6ForwardingDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{9,Ipv6DefaultHopLimit}, NULL, Ipv6DefaultHopLimitGet, Ipv6DefaultHopLimitSet, Ipv6DefaultHopLimitTest, Ipv6DefaultHopLimitDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "64"},

{{9,Ipv6Interfaces}, NULL, Ipv6InterfacesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{9,Ipv6IfTableLastChange}, NULL, Ipv6IfTableLastChangeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,Ipv6IfIndex}, GetNextIndexIpv6IfTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Ipv6IfTableINDEX, 1, 0, 0, NULL},

{{11,Ipv6IfDescr}, GetNextIndexIpv6IfTable, Ipv6IfDescrGet, Ipv6IfDescrSet, Ipv6IfDescrTest, Ipv6IfTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Ipv6IfTableINDEX, 1, 0, 0, NULL},

{{11,Ipv6IfLowerLayer}, GetNextIndexIpv6IfTable, Ipv6IfLowerLayerGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OBJECT_ID, SNMP_READONLY, Ipv6IfTableINDEX, 1, 0, 0, NULL},

{{11,Ipv6IfEffectiveMtu}, GetNextIndexIpv6IfTable, Ipv6IfEffectiveMtuGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Ipv6IfTableINDEX, 1, 0, 0, NULL},

{{11,Ipv6IfReasmMaxSize}, GetNextIndexIpv6IfTable, Ipv6IfReasmMaxSizeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Ipv6IfTableINDEX, 1, 0, 0, NULL},

{{11,Ipv6IfIdentifier}, GetNextIndexIpv6IfTable, Ipv6IfIdentifierGet, Ipv6IfIdentifierSet, Ipv6IfIdentifierTest, Ipv6IfTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Ipv6IfTableINDEX, 1, 0, 0, NULL},

{{11,Ipv6IfIdentifierLength}, GetNextIndexIpv6IfTable, Ipv6IfIdentifierLengthGet, Ipv6IfIdentifierLengthSet, Ipv6IfIdentifierLengthTest, Ipv6IfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ipv6IfTableINDEX, 1, 0, 0, NULL},

{{11,Ipv6IfPhysicalAddress}, GetNextIndexIpv6IfTable, Ipv6IfPhysicalAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Ipv6IfTableINDEX, 1, 0, 0, NULL},

{{11,Ipv6IfAdminStatus}, GetNextIndexIpv6IfTable, Ipv6IfAdminStatusGet, Ipv6IfAdminStatusSet, Ipv6IfAdminStatusTest, Ipv6IfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ipv6IfTableINDEX, 1, 0, 0, NULL},

{{11,Ipv6IfOperStatus}, GetNextIndexIpv6IfTable, Ipv6IfOperStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Ipv6IfTableINDEX, 1, 0, 0, NULL},

{{11,Ipv6IfLastChange}, GetNextIndexIpv6IfTable, Ipv6IfLastChangeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, Ipv6IfTableINDEX, 1, 0, 0, NULL},

{{11,Ipv6IfStatsInReceives}, GetNextIndexIpv6IfStatsTable, Ipv6IfStatsInReceivesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Ipv6IfStatsTableINDEX, 1, 0, 0, NULL},

{{11,Ipv6IfStatsInHdrErrors}, GetNextIndexIpv6IfStatsTable, Ipv6IfStatsInHdrErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Ipv6IfStatsTableINDEX, 1, 0, 0, NULL},

{{11,Ipv6IfStatsInTooBigErrors}, GetNextIndexIpv6IfStatsTable, Ipv6IfStatsInTooBigErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Ipv6IfStatsTableINDEX, 1, 0, 0, NULL},

{{11,Ipv6IfStatsInNoRoutes}, GetNextIndexIpv6IfStatsTable, Ipv6IfStatsInNoRoutesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Ipv6IfStatsTableINDEX, 1, 0, 0, NULL},

{{11,Ipv6IfStatsInAddrErrors}, GetNextIndexIpv6IfStatsTable, Ipv6IfStatsInAddrErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Ipv6IfStatsTableINDEX, 1, 0, 0, NULL},

{{11,Ipv6IfStatsInUnknownProtos}, GetNextIndexIpv6IfStatsTable, Ipv6IfStatsInUnknownProtosGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Ipv6IfStatsTableINDEX, 1, 0, 0, NULL},

{{11,Ipv6IfStatsInTruncatedPkts}, GetNextIndexIpv6IfStatsTable, Ipv6IfStatsInTruncatedPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Ipv6IfStatsTableINDEX, 1, 0, 0, NULL},

{{11,Ipv6IfStatsInDiscards}, GetNextIndexIpv6IfStatsTable, Ipv6IfStatsInDiscardsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Ipv6IfStatsTableINDEX, 1, 0, 0, NULL},

{{11,Ipv6IfStatsInDelivers}, GetNextIndexIpv6IfStatsTable, Ipv6IfStatsInDeliversGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Ipv6IfStatsTableINDEX, 1, 0, 0, NULL},

{{11,Ipv6IfStatsOutForwDatagrams}, GetNextIndexIpv6IfStatsTable, Ipv6IfStatsOutForwDatagramsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Ipv6IfStatsTableINDEX, 1, 0, 0, NULL},

{{11,Ipv6IfStatsOutRequests}, GetNextIndexIpv6IfStatsTable, Ipv6IfStatsOutRequestsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Ipv6IfStatsTableINDEX, 1, 0, 0, NULL},

{{11,Ipv6IfStatsOutDiscards}, GetNextIndexIpv6IfStatsTable, Ipv6IfStatsOutDiscardsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Ipv6IfStatsTableINDEX, 1, 0, 0, NULL},

{{11,Ipv6IfStatsOutFragOKs}, GetNextIndexIpv6IfStatsTable, Ipv6IfStatsOutFragOKsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Ipv6IfStatsTableINDEX, 1, 0, 0, NULL},

{{11,Ipv6IfStatsOutFragFails}, GetNextIndexIpv6IfStatsTable, Ipv6IfStatsOutFragFailsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Ipv6IfStatsTableINDEX, 1, 0, 0, NULL},

{{11,Ipv6IfStatsOutFragCreates}, GetNextIndexIpv6IfStatsTable, Ipv6IfStatsOutFragCreatesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Ipv6IfStatsTableINDEX, 1, 0, 0, NULL},

{{11,Ipv6IfStatsReasmReqds}, GetNextIndexIpv6IfStatsTable, Ipv6IfStatsReasmReqdsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Ipv6IfStatsTableINDEX, 1, 0, 0, NULL},

{{11,Ipv6IfStatsReasmOKs}, GetNextIndexIpv6IfStatsTable, Ipv6IfStatsReasmOKsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Ipv6IfStatsTableINDEX, 1, 0, 0, NULL},

{{11,Ipv6IfStatsReasmFails}, GetNextIndexIpv6IfStatsTable, Ipv6IfStatsReasmFailsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Ipv6IfStatsTableINDEX, 1, 0, 0, NULL},

{{11,Ipv6IfStatsInMcastPkts}, GetNextIndexIpv6IfStatsTable, Ipv6IfStatsInMcastPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Ipv6IfStatsTableINDEX, 1, 0, 0, NULL},

{{11,Ipv6IfStatsOutMcastPkts}, GetNextIndexIpv6IfStatsTable, Ipv6IfStatsOutMcastPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Ipv6IfStatsTableINDEX, 1, 0, 0, NULL},

{{11,Ipv6AddrPrefix}, GetNextIndexIpv6AddrPrefixTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, Ipv6AddrPrefixTableINDEX, 3, 0, 0, NULL},

{{11,Ipv6AddrPrefixLength}, GetNextIndexIpv6AddrPrefixTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Ipv6AddrPrefixTableINDEX, 3, 0, 0, NULL},

{{11,Ipv6AddrPrefixOnLinkFlag}, GetNextIndexIpv6AddrPrefixTable, Ipv6AddrPrefixOnLinkFlagGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Ipv6AddrPrefixTableINDEX, 3, 0, 0, NULL},

{{11,Ipv6AddrPrefixAutonomousFlag}, GetNextIndexIpv6AddrPrefixTable, Ipv6AddrPrefixAutonomousFlagGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Ipv6AddrPrefixTableINDEX, 3, 0, 0, NULL},

{{11,Ipv6AddrPrefixAdvPreferredLifetime}, GetNextIndexIpv6AddrPrefixTable, Ipv6AddrPrefixAdvPreferredLifetimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Ipv6AddrPrefixTableINDEX, 3, 0, 0, NULL},

{{11,Ipv6AddrPrefixAdvValidLifetime}, GetNextIndexIpv6AddrPrefixTable, Ipv6AddrPrefixAdvValidLifetimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Ipv6AddrPrefixTableINDEX, 3, 0, 0, NULL},

{{11,Ipv6AddrAddress}, GetNextIndexIpv6AddrTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, Ipv6AddrTableINDEX, 2, 0, 0, NULL},

{{11,Ipv6AddrPfxLength}, GetNextIndexIpv6AddrTable, Ipv6AddrPfxLengthGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Ipv6AddrTableINDEX, 2, 0, 0, NULL},

{{11,Ipv6AddrType}, GetNextIndexIpv6AddrTable, Ipv6AddrTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Ipv6AddrTableINDEX, 2, 0, 0, NULL},

{{11,Ipv6AddrAnycastFlag}, GetNextIndexIpv6AddrTable, Ipv6AddrAnycastFlagGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Ipv6AddrTableINDEX, 2, 0, 0, NULL},

{{11,Ipv6AddrStatus}, GetNextIndexIpv6AddrTable, Ipv6AddrStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Ipv6AddrTableINDEX, 2, 0, 0, NULL},

{{9,Ipv6RouteNumber}, NULL, Ipv6RouteNumberGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{9,Ipv6DiscardedRoutes}, NULL, Ipv6DiscardedRoutesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,Ipv6RouteDest}, GetNextIndexIpv6RouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, Ipv6RouteTableINDEX, 3, 0, 0, NULL},

{{11,Ipv6RoutePfxLength}, GetNextIndexIpv6RouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Ipv6RouteTableINDEX, 3, 0, 0, NULL},

{{11,Ipv6RouteIndex}, GetNextIndexIpv6RouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Ipv6RouteTableINDEX, 3, 0, 0, NULL},

{{11,Ipv6RouteIfIndex}, GetNextIndexIpv6RouteTable, Ipv6RouteIfIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Ipv6RouteTableINDEX, 3, 0, 0, NULL},

{{11,Ipv6RouteNextHop}, GetNextIndexIpv6RouteTable, Ipv6RouteNextHopGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Ipv6RouteTableINDEX, 3, 0, 0, NULL},

{{11,Ipv6RouteType}, GetNextIndexIpv6RouteTable, Ipv6RouteTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Ipv6RouteTableINDEX, 3, 0, 0, NULL},

{{11,Ipv6RouteProtocol}, GetNextIndexIpv6RouteTable, Ipv6RouteProtocolGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Ipv6RouteTableINDEX, 3, 0, 0, NULL},

{{11,Ipv6RoutePolicy}, GetNextIndexIpv6RouteTable, Ipv6RoutePolicyGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Ipv6RouteTableINDEX, 3, 0, 0, NULL},

{{11,Ipv6RouteAge}, GetNextIndexIpv6RouteTable, Ipv6RouteAgeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Ipv6RouteTableINDEX, 3, 0, 0, NULL},

{{11,Ipv6RouteNextHopRDI}, GetNextIndexIpv6RouteTable, Ipv6RouteNextHopRDIGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Ipv6RouteTableINDEX, 3, 0, 0, NULL},

{{11,Ipv6RouteMetric}, GetNextIndexIpv6RouteTable, Ipv6RouteMetricGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Ipv6RouteTableINDEX, 3, 0, 0, NULL},

{{11,Ipv6RouteWeight}, GetNextIndexIpv6RouteTable, Ipv6RouteWeightGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Ipv6RouteTableINDEX, 3, 0, 0, NULL},

{{11,Ipv6RouteInfo}, GetNextIndexIpv6RouteTable, Ipv6RouteInfoGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OBJECT_ID, SNMP_READONLY, Ipv6RouteTableINDEX, 3, 0, 0, NULL},

{{11,Ipv6RouteValid}, GetNextIndexIpv6RouteTable, Ipv6RouteValidGet, Ipv6RouteValidSet, Ipv6RouteValidTest, Ipv6RouteTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ipv6RouteTableINDEX, 3, 0, 0, "1"},

{{11,Ipv6NetToMediaNetAddress}, GetNextIndexIpv6NetToMediaTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, Ipv6NetToMediaTableINDEX, 2, 0, 0, NULL},

{{11,Ipv6NetToMediaPhysAddress}, GetNextIndexIpv6NetToMediaTable, Ipv6NetToMediaPhysAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Ipv6NetToMediaTableINDEX, 2, 0, 0, NULL},

{{11,Ipv6NetToMediaType}, GetNextIndexIpv6NetToMediaTable, Ipv6NetToMediaTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Ipv6NetToMediaTableINDEX, 2, 0, 0, NULL},

{{11,Ipv6IfNetToMediaState}, GetNextIndexIpv6NetToMediaTable, Ipv6IfNetToMediaStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Ipv6NetToMediaTableINDEX, 2, 0, 0, NULL},

{{11,Ipv6IfNetToMediaLastUpdated}, GetNextIndexIpv6NetToMediaTable, Ipv6IfNetToMediaLastUpdatedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, Ipv6NetToMediaTableINDEX, 2, 0, 0, NULL},

{{11,Ipv6NetToMediaValid}, GetNextIndexIpv6NetToMediaTable, Ipv6NetToMediaValidGet, Ipv6NetToMediaValidSet, Ipv6NetToMediaValidTest, Ipv6NetToMediaTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ipv6NetToMediaTableINDEX, 2, 0, 0, "1"},
};
tMibData stdip6Entry = { 68, stdip6MibEntry };
#endif /* _STDIP6DB_H */

