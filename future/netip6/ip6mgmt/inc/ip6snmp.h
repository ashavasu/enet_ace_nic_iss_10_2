/*******************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ip6snmp.h,v 1.19 2015/06/30 06:06:17 siva Exp $
 *
 * Description: This file contains IPv6 SNMP based information
 *
 *******************************************************************/

#ifndef _IP6SNMP_H
#define _IP6SNMP_H


/*** SNMP CONSTANTS ***/

/* Administrative Status */
#define  ADMIN_UP       NETIPV6_ADMIN_UP
#define  ADMIN_DOWN     NETIPV6_ADMIN_DOWN 
#define  ADMIN_INVALID  NETIPV6_ADMIN_INVALID
#define  ADMIN_VALID    NETIPV6_ADMIN_VALID

/* Oper Status */
#define  OPER_UP        NETIPV6_OPER_UP 
#define  OPER_DOWN      NETIPV6_OPER_DOWN
#define  OPER_NOIFIDENTIFIER NETIPV6_OPER_NOIFIDENTIFIER
#define  OPER_DOWN_INPROGRESS NETIPV6_OPER_DOWN_INPROGRESS

/*
 * IP6 constants
 */

/* GLOBALS */
#define  IP6_MIN_HOP_LIMIT                 0   
#define  IP6_MAX_HOP_LIMIT                 255 

#define  IP6_DEF_ROUTING_PROTO             2   

/* Macros for performing corresponding route look-up. */
#define  IP6_ROUTE_PREFERENCE_BEST_METRIC  1   
#define  IP6_ROUTE_PREFERENCE_STATIC       2   
#define  IP6_ROUTE_PREFERENCE_LOCAL        3
#define  IP6_ROUTE_PREFERENCE_RIPNG        4
#define  IP6_ROUTE_PREFERENCE_OSPF         5
#define  IP6_ROUTE_PREFERENCE_BGP          6

#define  IP6_PROPOGATE_DYNAMIC_AND_STATIC  1   
#define  IP6_PROPOGATE_DYNAMIC_ONLY        2   
#define  IP6_DEF_PROPOGATE                 2   

#define  ND6_MIN_CACHE_SOLICIT_RETRIES     1   
#define  ND6_MAX_CACHE_SOLICIT_RETRIES     10  
#define  ND6_DEF_CACHE_SOLICIT_RETRIES     3   

/*  
 * ND Protocol constants 
 */

#define  MAX_INITIAL_RTR_ADVERT_INTERVAL  16     /* seconds */
#define  MAX_INITIAL_RTR_ADVERTISEMENTS   3      /* transmissions */

/* Changed the const value to 3 */
#define  MAX_FINAL_RTR_ADVERTISEMENTS  3         /* transmissions */

#define  MIN_DELAY_BETWEEN_RAS         3         /* seconds */
#define  MAX_RA_DELAY_TIME             ((1 * SYS_NUM_OF_TIME_UNITS_IN_A_SEC)/2)  /* seconds */

#define  MAX_RTR_SOLICITATION_DELAY    1         /* second */
#ifdef MN_WANTED
#define  RTR_SOLICITATION_INTERVAL(x)     Ip6GetRtrSolInterval(x->u4Index)
#else
#define  RTR_SOLICITATION_INTERVAL     4         /* seconds */
#endif
#define  MAX_RTR_SOLICITATIONS         3         /* transmissions */

#define  MAX_MULTICAST_SOLICIT         3         /* transmissions */
#define  MAX_UNICAST_SOLICIT           3         /* transmissions */
#define  MAX_ANYCAST_DELAY_TIME        1         /* second */
#define  MAX_NEIGHBOR_ADVERTISEMENT    3         /* transmissions */
#define  REACHABLE_TIME                30000     /* milliseconds */
#ifdef LNXIP6_WANTED
#define  RETRANS_TIMER                 0      /* milliseconds */
#else
#define  RETRANS_TIMER                 1000      /* milliseconds */
#endif

#define  DELAY_FIRST_PROBE_TIME        5         /* seconds */
#define  MIN_RANDOM_FACTOR             (1/2) 
#define  MAX_RANDOM_FACTOR             (3/2) 

/* Defaults */

#ifdef HA_WANTED
#define  BASE_MAX_RTR_ADV_INTERVAL  3  
#define  BASE_MIN_RTR_ADV_INTERVAL  1      /* 0.33 * MAX_RTR_ADV_INTERVAL */
#else
#define  BASE_MAX_RTR_ADV_INTERVAL  600  
#define  BASE_MIN_RTR_ADV_INTERVAL  198      /* 0.33 * MAX_RTR_ADV_INTERVAL */
#endif
#define  BASE_ADV_DEF_LIFETIME      1800     /* 3 * MAX_RTR_ADV_INTERVAL */
#define  BASE_ADV_LINK_MTU          0     /* 0 * BASE_ADV_LINK_MTU */
#define  BASE_ADV_RETRANS_TIME      0     /* 0 * BASE_ADV_RETRANSMIT_TIME */

/* Other constants */

#define  ND6_LAN_CACHE_ENTRY_VALID    1 
#define  ND6_LAN_CACHE_ENTRY_INVALID  2 

#define  ND6_WAN_CACHE_ENTRY_VALID    1 
#define  ND6_WAN_CACHE_ENTRY_INVALID  2 

/* ICMP6 related constants */

/*
#define  ICMP6_ERR_INETRVAL             100
#define  ICMP6_BUCKET_SIZE              10
#define  ICMP6_DEST_UNREACHABLE_ENABLE  1
#define  ICMP6_DEST_UNREACHABLE_DISABLE 0 
#define  ICMP6_RATE_LIMIT_ENABLE        1
#define  ICMP6_RATE_LIMIT_DISABLE       0
*/


/* Common values for ND Lan/Wan cache tables */

#define  IP6IF_AUTH_STATUS_ENABLED     1 
#define  IP6IF_AUTH_STATUS_DISABLED    2 

/* IPv6 LAN Interface */
#define  IP6_IF_RA_ADV              1                     
#define  IP6_IF_RS_SEND             1
#define  IP6_IF_NO_RS_SEND          0

#define  IP6_IF_ROUT_ADV_ENABLED     1                      
#define  IP6_IF_ROUT_ADV_DISABLED    2                      
#define  IP6_IF_DEF_ROUT_ADV_STATUS  1
#define  IP6_IF_ROUT_ADV_RDNSS     1
#define  IP6_IF_ROUT_ADV_NO_RDNSS     2
#define  IP6_IF_ROUT_ADV_RDNSS_ENABLED     1
#define  IP6_IF_ROUT_ADV_RDNSS_DISABLED     2                      

#define  IP6_IF_ROUT_ADV_M_BIT       1                /* M bit */
#define  IP6_IF_ROUT_ADV_O_BIT       2                /* O bit */
#define  IP6_IF_ROUT_ADV_NO_M_BIT    3                /* No M Bit */
#define  IP6_IF_ROUT_ADV_NO_O_BIT    4                /* No O Bit */
#define  IP6_IF_ROUT_ADV_BOTH_BIT    5                /* M and O Bits */
#define  IP6_IF_ROUT_ADV_NO_BIT      6                /* None */
#define  IP6_IF_DEF_ROUT_ADV_BIT     4                      

#define  IP6_IF_PREFIX_ADV          1                     
#define  IP6_IF_NO_PREFIX_ADV       2                     
#define  IP6_RA_RDNSS_OPEN          1
#define  IP6_RA_NO_RDNSS_OPEN       2
#define IP6_RA_ADV_INTERVAL         1
#define IP6_RA_NO_ADV_INTERVAL      2
#define IP6_RA_ADV_LINKLOCAL        1
#define IP6_RA_NO_ADV_LINKLOCAL     2
#define  IP6_IF_RA_RDNSS_MIN_PREF   0
#define  IP6_IF_RA_RDNSS_MAX_PREF   15
#define  IP6_IF_RA_RDNSS_DEF_PREF    8
#define  IP6_IF_MIN_HOPLMT           1                      
#define  IP6_IF_MAX_HOPLMT           255                    
#define  IP6_IF_DEF_HOPLMT           64                     

#define  IP6_IF_MAX_DEFTIME          9000                   
#define  IP6_IF_DEF_DEFTIME          3 * IP6_IF_DEF_MAX_RA_TIME

#define  IP6_IF_MIN_REACHTIME        0                /* In MSec. */   
#define  IP6_IF_MAX_REACHTIME        3600000          /* In MSec. */ 
#define  IP6_IF_DEF_REACHTIME        REACHABLE_TIME   /* In MSec. */      

#define  IP6_IF_MIN_RETTIME          1000              /* In MSec. */        
#define  IP6_IF_MAX_RETTIME          3600000           /* In MSec. */      
#define  IP6_IF_DEF_RETTIME          RETRANS_TIMER     /* In MSec. */       
#define  IP6_MIN_RETR_RANGE          1            /* Min-Range */
#define  IP6_MAX_RETR_RANGE          10            /* Max-Range */

#define  IP6_IF_MIN_PDTIME           0                      
#define  IP6_IF_MAX_PDTIME           10                     
#define  IP6_IF_DEF_PDTIME           DELAY_FIRST_PROBE_TIME 

#define  IP6_IF_PREFIX_ADV_ENABLED   1                      
#define  IP6_IF_PREFIX_ADV_DISABLED  2                      
#define  IP6_IF_DEF_PREFIX_ADV       1                      

/* MaxRA time */
#define  IP6_IF_MAX_RA_MIN_TIME  4                         
#define  IP6_IF_MAX_RA_MAX_TIME  1800                      

#define  IP6_IF_DEF_MAX_RA_TIME  BASE_MAX_RTR_ADV_INTERVAL 

#define  IP6_IF_M_BIT_ADV           2                     
#define  IP6_IF_O_BIT_ADV           4                     
#define  IP6_IF_NO_RA_ADV           8                     

/* MinRA time */
#define  IP6_IF_MIN_RA_MIN_TIME  3                         
#define  IP6_IF_DEF_MIN_RA_TIME  BASE_MIN_RTR_ADV_INTERVAL 

#define  IP6_IF_MIN_DAD_SEND     0                         
#define  IP6_IF_MAX_DAD_SEND     10                        
#define  IP6_IF_DEF_DAD_SEND     1                         

/* IPvx MIB RA Default Values and Macros */
#define IP6_RA_INVALID         0x08

#define IP6_RA_MIN_LINK_MTU      1280
#define IP6_RA_MAX_LINK_MTU      1500
#define IP6_RA_MIN_HOPLMT        0
#define IP6_RA_MAX_HOPLMT        255

#define IP6_RA_DEF_REACHTIME     0
#define IP6_RA_DEF_RETTIME       0
#define IP6_RA_DEF_MAX_RA_TIME   600
#define IP6_RA_DEF_MIN_RA_TIME   (0.33 * IP6_RA_DEF_MAX_RA_TIME)
#define IP6_RA_DEF_DEFTIME       (3 * IP6_RA_DEF_MAX_RA_TIME)
#define IP6_RA_DEF_HOPLMT        64
#define IP6_RA_DEF_LINK_MTU      0

/* IPv6 Address */

#define IP6_ADDR_TYPE_UNICAST       ADDR6_UNICAST     
#define IP6_ADDR_TYPE_ANYCAST       ADDR6_ANYCAST     
#define IP6_ADDR_TYPE_LINK_LOCAL    ADDR6_LLOCAL      

#define  IP6_ADDR_OPER_TENTATIVE    1   
#define  IP6_ADDR_OPER_COMPLETE     2   
#define  IP6_ADDR_OPER_DOWN         3   
#define  IP6_ADDR_OPER_FAILED       4   

/* HOST - related defintions */
#define  IP6_ADDR_STAT_PREFERRED      1
#define  IP6_ADDR_STAT_DEPRECATE      2
#define  IP6_ADDR_STAT_INVALID        3
#define  IP6_ADDR_STAT_INACCESSIBLE   4   
#define  IP6_ADDR_STAT_UNKNOWN        5

/* Address Profile */

#define  IP6_ADDR_PROF_VALID           1          
#define  IP6_ADDR_PROF_INVALID         2          

#define  IP6_ADDR_PROF_PREF_ADV_ON     1          
#define  IP6_ADDR_PROF_PREF_ADV_OFF    2          
#define  IP6_ADDR_PROF_PREF_ADV_DFL    1          

#define  IP6_ADDR_PROF_ONLINK_ADV_ON   1          
#define  IP6_ADDR_PROF_ONLINK_ADV_OFF  2          

#define  IP6_ADDR_PROF_AUTO_ADV_ON     1          
#define  IP6_ADDR_PROF_AUTO_ADV_OFF    2          

#define  IP6_ADDR_PROF_MAX_VALID_LIFE  0xffffffff 
#define  IP6_ADDR_PROF_DEF_VALID_LIFE  2592000    /* 30 Days */
#define  IP6_ADDR_PROF_MAX_PREF_LIFE   0xffffffff 
#define  IP6_ADDR_PROF_DEF_PREF_LIFE   604800     /* 7 Days */

#define  IP6_MAX_IF_COST             15  
#define  IP6_MIN_IF_COST             1   
#define  IP6_DFL_IF_COST             1   

#define  IP6_MAX_PROFILE_INDEX       10  
#define  IP6_MIN_PROFILE_INDEX       0   
#define  IP6_DFL_PROFILE_INDEX       1   

#define  IP6_SUPPORT_EMBD_RP_ENABLE        1  
#define  IP6_SUPPORT_EMBD_RP_DISABLE       2  
 
#define  IP6_ROUTE_MIN_PFX_LENGTH    0   
#define  IP6_ROUTE_MAX_PFX_LENGTH    128 

#define  IP6_ROUTE_MIN_METRIC        1   
#define  IP6_ROUTE_MAX_METRIC        0xFFFFFFFF  
#define  IP6_ROUTE_DFL_METRIC        1   

/* Address profile Valid Lifetime flag */
#define  IP6_FIXED_TIME        1
#define  IP6_VARIABLE_TIME     2

#define IP6_FORW_ENABLE       1
#define IP6_FORW_DISABLE      2

#define IP6_IF_FORW_ENABLE    1
#define IP6_IF_FORW_DISABLE   2
/* IPV6 Protocol Preference */
#define  IP6_MIN_PROTO_PREFERENCE        0
#define  IP6_MAX_PROTO_PREFERENCE        255

/* Values for status of the address */

#define  ADDR6_COMPLETE(pIf6)         ((UINT1)Ip6RetAddrStatComplete(pIf6))
#define  ADDR6_TENTATIVE        0x01
#define  ADDR6_FAILED           0x04
#define  ADDR6_UP               0x10
#define  ADDR6_DOWN             0x20

#define ADDR6_PREFERRED         0x08
#define ADDR6_DEPRECATED        ~(ADDR6_PREFERRED)
#define ADDR6_INACCESSIBLE      0x40
#define ADDR6_UNKNOWN           0x80

#define  IP6_ENET_ADDR_LEN           6       /* Ethernet LinkLayerAddr Len */

/* Default IP6 instance */
#define   IP6_DEFAULT_CONTEXT        VCM_DEFAULT_CONTEXT 

/* Protocol default Preference Value. moved to ipv6.h */

/* Malloc Changes */
#define MAX_IP6_ADVT_INFO_LEN      3
#define MAX_IP6_ONLINK_INFO_LEN    5
#define MAX_IP6_VALID_TIME_LEN     9
#define MAX_IP6_PREF_TIME_LEN      9

/* PING */

#define  IP6_PING_ENABLE           1    
#define  IP6_PING_DISABLE          2    
#define  IP6_PING_INVALID          3    
#define  IP6_PING_VALID            4    

#define  IP6_PING_IN_PROGRESS      1    
#define  IP6_PING_NOT_IN_PROGRESS  2    

#define  IP6_PING_MAX_INTERVAL     100  
#define  IP6_PING_DEF_INTERVAL     1    
#define  IP6_PING_MIN_INTERVAL     1    

#define  IP6_PING_MAX_TRIES        200 
#define  IP6_PING_DEF_TRIES        5    
#define  IP6_PING_MIN_TRIES        1    

#define  IP6_PING_MAX_SIZE         2080 
#define  IP6_PING_DEF_SIZE         100  
#define  IP6_PING_MIN_SIZE         0   

#define  IP6_PING_MAX_RCV_TIMEOUT  100  
#define  IP6_PING_DEF_RCV_TIMEOUT  1    
#define  IP6_PING_MIN_RCV_TIMEOUT  1    

#define  IP6_PING_DATA_SIZE        6 

#define  IP6_PMTU_ENTRY_VALID      1    
#define  IP6_PMTU_ENTRY_INVALID    2    

#define IP6_FSIP6_TBL_OID             1
#define IP6_ROUTE_TBL_OID             6

/* The configuration status for Zone start*/
#define   IP6_ZONE_TYPE_AUTO                    1
#define   IP6_ZONE_TYPE_MANUAL                  2
#define   IP6_ZONE_TYPE_OVERRIDDEN              3
/* The configuration status for Zone End*/

#define   IP6_ZONE_FALSE                        2
#define   IP6_ZONE_TRUE                         1

#define IP6_SCOPE_ZONE_PTR_FROM_SLL(sll) \
          (tIp6IfZoneMapInfo*) (VOID *)\
           ((UINT1 *)(sll) - IP6_OFFSET(tIp6IfZoneMapInfo, nextZone))

/*RATE LIMIT TMR MACROS*/
#define  ICMP6_RATE_LIMIT_ENABLE          1
#define  ICMP6_RATE_LIMIT_DISABLE         0
#define  ICMP6_DEST_UNREACHABLE_ENABLE    1
#define  ICMP6_DEST_UNREACHABLE_DISABLE   2
#define  ICMP6_RATE_LIMIT_TIME            1  /* in seconds */
#define  ICMP6_MAX_ERR_INTERVAL           65535
#define  ICMP6_MAX_BUCKET_SIZE            200
#define  ICMP6_DEF_ERR_INETRVAL           100
#define  ICMP6_DEF_BUCKET_SIZE            10
#define  ICMP6_ERR_MSG_ALLOW_TO_SEND      1
#define  ICMP6_ERR_MSG_NOT_ALLOW_TO_SEND  0
#define  ICMP6_DEFAULT_VALUE              0xffff

/* For LinuxIp compatibility this macro is added here */
UINT1 Ip6IsIfFwdEnabled PROTO ((INT4 i4IfIndex));

#endif /* _IP6SNMP_H */
