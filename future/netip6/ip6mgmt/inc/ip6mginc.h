/**************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: ip6mginc.h,v 1.7 2014/04/10 13:27:06 siva Exp $
 *
 * Description: 
 ***************************************************************************/
#ifndef _IP6MGINC_H
#define _IP6MGINC_H

#include "lr.h"
#include "cfa.h"
#include "vcm.h"
#include "trieinc.h" 

#include "ipv6.h"
#include "rtm6.h"   

#include "ip6snmp.h"

#include "ping6.h" 
#include "ip6sys.h" 
#include "ip6util.h"
#include "ip6if.h"  
#include "ip6frag.h"
#include "icmp6.h"
#include "nd6.h"
#include "ip6addr.h"
#include "ip6zone.h"
#include "ip6glob.h"
#include "ip6ext.h"
#include "ip6port.h"
#include "ip6inc.h"
#include "ipv6npwr.h"
#endif /* _IP6MGINC_H */
