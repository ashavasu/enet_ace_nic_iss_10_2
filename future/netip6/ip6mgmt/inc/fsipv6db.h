/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsipv6db.h,v 1.24 2016/03/24 10:27:40 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSIPV6DB_H
#define _FSIPV6DB_H

UINT1 Fsipv6IfTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 Fsipv6IfStatsTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 Fsipv6PrefixTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_FIXED_LENGTH_OCTET_STRING ,16 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 Fsipv6AddrTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_FIXED_LENGTH_OCTET_STRING ,16 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 Fsipv6AddrProfileTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Fsipv6PmtuTableINDEX [] = {SNMP_FIXED_LENGTH_OCTET_STRING ,16};
UINT1 Fsipv6NdLanCacheTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_FIXED_LENGTH_OCTET_STRING ,16};
UINT1 Fsipv6NdWanCacheTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_FIXED_LENGTH_OCTET_STRING ,16};
UINT1 Fsipv6PingTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 Fsipv6NDProxyListTableINDEX [] = {SNMP_FIXED_LENGTH_OCTET_STRING ,16};
UINT1 Fsipv6AddrSelPolicyTableINDEX [] = {SNMP_FIXED_LENGTH_OCTET_STRING ,16 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER};
UINT1 Fsipv6IfScopeZoneMapTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 Fsipv6ScopeZoneTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 Fsipv6SENDSentPktStatsTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER};
UINT1 Fsipv6SENDRcvPktStatsTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER};
UINT1 Fsipv6SENDDrpPktStatsTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER};
UINT1 Fsipv6RARouteInfoTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_FIXED_LENGTH_OCTET_STRING ,16 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 Fsipv6RouteTableINDEX [] = {SNMP_FIXED_LENGTH_OCTET_STRING ,16 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_FIXED_LENGTH_OCTET_STRING ,16};
UINT1 Fsipv6PrefTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 Fsipv6IfRaRDNSSTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};

/* ATTENTION : this is manually created db.h , The array size 9 is required to accomodate the usage of fsipv6[8] in wr.c*/
UINT4 fsipv6 [9] ={1,3,6,1,4,1,2076,28};
tSNMP_OID_TYPE fsipv6OID = {9, fsipv6};


UINT4 Fsipv6NdCacheMaxRetries [ ] ={1,3,6,1,4,1,2076,28,1,1,1};
UINT4 Fsipv6PmtuConfigStatus [ ] ={1,3,6,1,4,1,2076,28,1,1,2};
UINT4 Fsipv6PmtuTimeOutInterval [ ] ={1,3,6,1,4,1,2076,28,1,1,3};
UINT4 Fsipv6JumboEnable [ ] ={1,3,6,1,4,1,2076,28,1,1,4};
UINT4 Fsipv6NumOfSendJumbo [ ] ={1,3,6,1,4,1,2076,28,1,1,5};
UINT4 Fsipv6NumOfRecvJumbo [ ] ={1,3,6,1,4,1,2076,28,1,1,6};
UINT4 Fsipv6ErrJumbo [ ] ={1,3,6,1,4,1,2076,28,1,1,7};
UINT4 Fsipv6GlobalDebug [ ] ={1,3,6,1,4,1,2076,28,1,1,8};
UINT4 Fsipv6MaxRouteEntries [ ] ={1,3,6,1,4,1,2076,28,1,1,9};
UINT4 Fsipv6MaxLogicalIfaces [ ] ={1,3,6,1,4,1,2076,28,1,1,10};
UINT4 Fsipv6MaxTunnelIfaces [ ] ={1,3,6,1,4,1,2076,28,1,1,11};
UINT4 Fsipv6MaxAddresses [ ] ={1,3,6,1,4,1,2076,28,1,1,12};
UINT4 Fsipv6MaxFragReasmEntries [ ] ={1,3,6,1,4,1,2076,28,1,1,13};
UINT4 Fsipv6Nd6MaxCacheEntries [ ] ={1,3,6,1,4,1,2076,28,1,1,14};
UINT4 Fsipv6PmtuMaxDest [ ] ={1,3,6,1,4,1,2076,28,1,1,15};
UINT4 Fsipv6RFC5095Compatibility [ ] ={1,3,6,1,4,1,2076,28,1,1,16};
UINT4 Fsipv6RFC5942Compatibility [ ] ={1,3,6,1,4,1,2076,28,1,1,17};
UINT4 Fsipv6SENDSecLevel [ ] ={1,3,6,1,4,1,2076,28,1,1,18};
UINT4 Fsipv6SENDNbrSecLevel [ ] ={1,3,6,1,4,1,2076,28,1,1,19};
UINT4 Fsipv6SENDAuthType [ ] ={1,3,6,1,4,1,2076,28,1,1,20};
UINT4 Fsipv6SENDMinBits [ ] ={1,3,6,1,4,1,2076,28,1,1,21};
UINT4 Fsipv6SENDSecDAD [ ] ={1,3,6,1,4,1,2076,28,1,1,22};
UINT4 Fsipv6SENDPrefixChk [ ] ={1,3,6,1,4,1,2076,28,1,1,23};
UINT4 Fsipv6ECMPPRTTimer [ ] ={1,3,6,1,4,1,2076,28,1,1,24};
UINT4 Fsipv6NdCacheTimeout [ ] ={1,3,6,1,4,1,2076,28,1,1,25};
UINT4 Fsipv6IfIndex [ ] ={1,3,6,1,4,1,2076,28,1,2,1,1,1};
UINT4 Fsipv6IfType [ ] ={1,3,6,1,4,1,2076,28,1,2,1,1,2};
UINT4 Fsipv6IfPortNum [ ] ={1,3,6,1,4,1,2076,28,1,2,1,1,3};
UINT4 Fsipv6IfCircuitNum [ ] ={1,3,6,1,4,1,2076,28,1,2,1,1,4};
UINT4 Fsipv6IfToken [ ] ={1,3,6,1,4,1,2076,28,1,2,1,1,5};
UINT4 Fsipv6IfAdminStatus [ ] ={1,3,6,1,4,1,2076,28,1,2,1,1,6};
UINT4 Fsipv6IfOperStatus [ ] ={1,3,6,1,4,1,2076,28,1,2,1,1,7};
UINT4 Fsipv6IfRouterAdvStatus [ ] ={1,3,6,1,4,1,2076,28,1,2,1,1,8};
UINT4 Fsipv6IfRouterAdvFlags [ ] ={1,3,6,1,4,1,2076,28,1,2,1,1,9};
UINT4 Fsipv6IfHopLimit [ ] ={1,3,6,1,4,1,2076,28,1,2,1,1,10};
UINT4 Fsipv6IfDefRouterTime [ ] ={1,3,6,1,4,1,2076,28,1,2,1,1,11};
UINT4 Fsipv6IfReachableTime [ ] ={1,3,6,1,4,1,2076,28,1,2,1,1,12};
UINT4 Fsipv6IfRetransmitTime [ ] ={1,3,6,1,4,1,2076,28,1,2,1,1,13};
UINT4 Fsipv6IfDelayProbeTime [ ] ={1,3,6,1,4,1,2076,28,1,2,1,1,14};
UINT4 Fsipv6IfPrefixAdvStatus [ ] ={1,3,6,1,4,1,2076,28,1,2,1,1,15};
UINT4 Fsipv6IfMinRouterAdvTime [ ] ={1,3,6,1,4,1,2076,28,1,2,1,1,16};
UINT4 Fsipv6IfMaxRouterAdvTime [ ] ={1,3,6,1,4,1,2076,28,1,2,1,1,17};
UINT4 Fsipv6IfDADRetries [ ] ={1,3,6,1,4,1,2076,28,1,2,1,1,18};
UINT4 Fsipv6IfForwarding [ ] ={1,3,6,1,4,1,2076,28,1,2,1,1,19};
UINT4 Fsipv6IfRoutingStatus [ ] ={1,3,6,1,4,1,2076,28,1,2,1,1,20};
UINT4 Fsipv6IfIcmpErrInterval [ ] ={1,3,6,1,4,1,2076,28,1,2,1,1,21};
UINT4 Fsipv6IfIcmpTokenBucketSize [ ] ={1,3,6,1,4,1,2076,28,1,2,1,1,22};
UINT4 Fsipv6IfDestUnreachableMsg [ ] ={1,3,6,1,4,1,2076,28,1,2,1,1,23};
UINT4 Fsipv6IfUnnumAssocIPIf [ ] ={1,3,6,1,4,1,2076,28,1,2,1,1,24};
UINT4 Fsipv6IfRedirectMsg [ ] ={1,3,6,1,4,1,2076,28,1,2,1,1,25};
UINT4 Fsipv6IfAdvSrcLLAdr [ ] ={1,3,6,1,4,1,2076,28,1,2,1,1,26};
UINT4 Fsipv6IfAdvIntOpt [ ] ={1,3,6,1,4,1,2076,28,1,2,1,1,27};
UINT4 Fsipv6IfNDProxyAdminStatus [ ] ={1,3,6,1,4,1,2076,28,1,2,1,1,28};
UINT4 Fsipv6IfNDProxyMode [ ] ={1,3,6,1,4,1,2076,28,1,2,1,1,29};
UINT4 Fsipv6IfNDProxyOperStatus [ ] ={1,3,6,1,4,1,2076,28,1,2,1,1,30};
UINT4 Fsipv6IfNDProxyUpStream [ ] ={1,3,6,1,4,1,2076,28,1,2,1,1,31};
UINT4 Fsipv6IfSENDSecStatus [ ] ={1,3,6,1,4,1,2076,28,1,2,1,1,32};
UINT4 Fsipv6IfSENDDeltaTimestamp [ ] ={1,3,6,1,4,1,2076,28,1,2,1,1,33};
UINT4 Fsipv6IfSENDFuzzTimestamp [ ] ={1,3,6,1,4,1,2076,28,1,2,1,1,34};
UINT4 Fsipv6IfSENDDriftTimestamp [ ] ={1,3,6,1,4,1,2076,28,1,2,1,1,35};
UINT4 Fsipv6IfDefRoutePreference [ ] ={1,3,6,1,4,1,2076,28,1,2,1,1,36};
UINT4 Fsipv6IfStatsIndex [ ] ={1,3,6,1,4,1,2076,28,1,2,2,1,1};
UINT4 Fsipv6IfStatsInReceives [ ] ={1,3,6,1,4,1,2076,28,1,2,2,1,2};
UINT4 Fsipv6IfStatsInHdrErrors [ ] ={1,3,6,1,4,1,2076,28,1,2,2,1,3};
UINT4 Fsipv6IfStatsTooBigErrors [ ] ={1,3,6,1,4,1,2076,28,1,2,2,1,4};
UINT4 Fsipv6IfStatsInAddrErrors [ ] ={1,3,6,1,4,1,2076,28,1,2,2,1,5};
UINT4 Fsipv6IfStatsForwDatagrams [ ] ={1,3,6,1,4,1,2076,28,1,2,2,1,6};
UINT4 Fsipv6IfStatsInUnknownProtos [ ] ={1,3,6,1,4,1,2076,28,1,2,2,1,7};
UINT4 Fsipv6IfStatsInDiscards [ ] ={1,3,6,1,4,1,2076,28,1,2,2,1,8};
UINT4 Fsipv6IfStatsInDelivers [ ] ={1,3,6,1,4,1,2076,28,1,2,2,1,9};
UINT4 Fsipv6IfStatsOutRequests [ ] ={1,3,6,1,4,1,2076,28,1,2,2,1,10};
UINT4 Fsipv6IfStatsOutDiscards [ ] ={1,3,6,1,4,1,2076,28,1,2,2,1,11};
UINT4 Fsipv6IfStatsOutNoRoutes [ ] ={1,3,6,1,4,1,2076,28,1,2,2,1,12};
UINT4 Fsipv6IfStatsReasmReqds [ ] ={1,3,6,1,4,1,2076,28,1,2,2,1,13};
UINT4 Fsipv6IfStatsReasmOKs [ ] ={1,3,6,1,4,1,2076,28,1,2,2,1,14};
UINT4 Fsipv6IfStatsReasmFails [ ] ={1,3,6,1,4,1,2076,28,1,2,2,1,15};
UINT4 Fsipv6IfStatsFragOKs [ ] ={1,3,6,1,4,1,2076,28,1,2,2,1,16};
UINT4 Fsipv6IfStatsFragFails [ ] ={1,3,6,1,4,1,2076,28,1,2,2,1,17};
UINT4 Fsipv6IfStatsFragCreates [ ] ={1,3,6,1,4,1,2076,28,1,2,2,1,18};
UINT4 Fsipv6IfStatsInMcastPkts [ ] ={1,3,6,1,4,1,2076,28,1,2,2,1,19};
UINT4 Fsipv6IfStatsOutMcastPkts [ ] ={1,3,6,1,4,1,2076,28,1,2,2,1,20};
UINT4 Fsipv6IfStatsInTruncatedPkts [ ] ={1,3,6,1,4,1,2076,28,1,2,2,1,21};
UINT4 Fsipv6IfStatsInRouterSols [ ] ={1,3,6,1,4,1,2076,28,1,2,2,1,22};
UINT4 Fsipv6IfStatsInRouterAdvs [ ] ={1,3,6,1,4,1,2076,28,1,2,2,1,23};
UINT4 Fsipv6IfStatsInNeighSols [ ] ={1,3,6,1,4,1,2076,28,1,2,2,1,24};
UINT4 Fsipv6IfStatsInNeighAdvs [ ] ={1,3,6,1,4,1,2076,28,1,2,2,1,25};
UINT4 Fsipv6IfStatsInRedirects [ ] ={1,3,6,1,4,1,2076,28,1,2,2,1,26};
UINT4 Fsipv6IfStatsOutRouterSols [ ] ={1,3,6,1,4,1,2076,28,1,2,2,1,27};
UINT4 Fsipv6IfStatsOutRouterAdvs [ ] ={1,3,6,1,4,1,2076,28,1,2,2,1,28};
UINT4 Fsipv6IfStatsOutNeighSols [ ] ={1,3,6,1,4,1,2076,28,1,2,2,1,29};
UINT4 Fsipv6IfStatsOutNeighAdvs [ ] ={1,3,6,1,4,1,2076,28,1,2,2,1,30};
UINT4 Fsipv6IfStatsOutRedirects [ ] ={1,3,6,1,4,1,2076,28,1,2,2,1,31};
UINT4 Fsipv6IfStatsLastRouterAdvTime [ ] ={1,3,6,1,4,1,2076,28,1,2,2,1,32};
UINT4 Fsipv6IfStatsNextRouterAdvTime [ ] ={1,3,6,1,4,1,2076,28,1,2,2,1,33};
UINT4 Fsipv6IfStatsIcmp6ErrRateLmtd [ ] ={1,3,6,1,4,1,2076,28,1,2,2,1,34};
UINT4 Fsipv6IfStatsSENDDroppedPkts [ ] ={1,3,6,1,4,1,2076,28,1,2,2,1,35};
UINT4 Fsipv6IfStatsSENDInvalidPkts [ ] ={1,3,6,1,4,1,2076,28,1,2,2,1,36};
UINT4 Fsipv6PrefixIndex [ ] ={1,3,6,1,4,1,2076,28,1,2,3,1,1};
UINT4 Fsipv6PrefixAddress [ ] ={1,3,6,1,4,1,2076,28,1,2,3,1,2};
UINT4 Fsipv6PrefixAddrLen [ ] ={1,3,6,1,4,1,2076,28,1,2,3,1,3};
UINT4 Fsipv6PrefixProfileIndex [ ] ={1,3,6,1,4,1,2076,28,1,2,3,1,4};
UINT4 Fsipv6PrefixAdminStatus [ ] ={1,3,6,1,4,1,2076,28,1,2,3,1,5};
UINT4 Fsipv6SupportEmbeddedRp [ ] ={1,3,6,1,4,1,2076,28,1,2,3,1,6};
UINT4 Fsipv6AddrIndex [ ] ={1,3,6,1,4,1,2076,28,1,2,4,1,1};
UINT4 Fsipv6AddrAddress [ ] ={1,3,6,1,4,1,2076,28,1,2,4,1,2};
UINT4 Fsipv6AddrPrefixLen [ ] ={1,3,6,1,4,1,2076,28,1,2,4,1,3};
UINT4 Fsipv6AddrAdminStatus [ ] ={1,3,6,1,4,1,2076,28,1,2,4,1,4};
UINT4 Fsipv6AddrType [ ] ={1,3,6,1,4,1,2076,28,1,2,4,1,5};
UINT4 Fsipv6AddrProfIndex [ ] ={1,3,6,1,4,1,2076,28,1,2,4,1,6};
UINT4 Fsipv6AddrOperStatus [ ] ={1,3,6,1,4,1,2076,28,1,2,4,1,7};
UINT4 Fsipv6AddrScope [ ] ={1,3,6,1,4,1,2076,28,1,2,4,1,8};
UINT4 Fsipv6AddrSENDCgaStatus [ ] ={1,3,6,1,4,1,2076,28,1,2,4,1,9};
UINT4 Fsipv6AddrSENDCgaModifier [ ] ={1,3,6,1,4,1,2076,28,1,2,4,1,10};
UINT4 Fsipv6AddrSENDCollisionCount [ ] ={1,3,6,1,4,1,2076,28,1,2,4,1,11};
UINT4 Fsipv6AddrProfileIndex [ ] ={1,3,6,1,4,1,2076,28,1,2,5,1,1};
UINT4 Fsipv6AddrProfileStatus [ ] ={1,3,6,1,4,1,2076,28,1,2,5,1,2};
UINT4 Fsipv6AddrProfilePrefixAdvStatus [ ] ={1,3,6,1,4,1,2076,28,1,2,5,1,3};
UINT4 Fsipv6AddrProfileOnLinkAdvStatus [ ] ={1,3,6,1,4,1,2076,28,1,2,5,1,4};
UINT4 Fsipv6AddrProfileAutoConfAdvStatus [ ] ={1,3,6,1,4,1,2076,28,1,2,5,1,5};
UINT4 Fsipv6AddrProfilePreferredTime [ ] ={1,3,6,1,4,1,2076,28,1,2,5,1,6};
UINT4 Fsipv6AddrProfileValidTime [ ] ={1,3,6,1,4,1,2076,28,1,2,5,1,7};
UINT4 Fsipv6AddrProfileValidLifeTimeFlag [ ] ={1,3,6,1,4,1,2076,28,1,2,5,1,8};
UINT4 Fsipv6AddrProfilePreferredLifeTimeFlag [ ] ={1,3,6,1,4,1,2076,28,1,2,5,1,9};
UINT4 Fsipv6PmtuDest [ ] ={1,3,6,1,4,1,2076,28,1,2,6,1,1};
UINT4 Fsipv6Pmtu [ ] ={1,3,6,1,4,1,2076,28,1,2,6,1,2};
UINT4 Fsipv6PmtuTimeStamp [ ] ={1,3,6,1,4,1,2076,28,1,2,6,1,3};
UINT4 Fsipv6PmtuAdminStatus [ ] ={1,3,6,1,4,1,2076,28,1,2,6,1,4};
UINT4 Fsipv6NdLanCacheIfIndex [ ] ={1,3,6,1,4,1,2076,28,1,2,7,1,1};
UINT4 Fsipv6NdLanCacheIPv6Addr [ ] ={1,3,6,1,4,1,2076,28,1,2,7,1,2};
UINT4 Fsipv6NdLanCacheStatus [ ] ={1,3,6,1,4,1,2076,28,1,2,7,1,3};
UINT4 Fsipv6NdLanCachePhysAddr [ ] ={1,3,6,1,4,1,2076,28,1,2,7,1,4};
UINT4 Fsipv6NdLanCacheState [ ] ={1,3,6,1,4,1,2076,28,1,2,7,1,5};
UINT4 Fsipv6NdLanCacheUseTime [ ] ={1,3,6,1,4,1,2076,28,1,2,7,1,6};
UINT4 Fsipv6NdLanCacheIsSecure [ ] ={1,3,6,1,4,1,2076,28,1,2,7,1,7};
UINT4 Fsipv6NdWanCacheIfIndex [ ] ={1,3,6,1,4,1,2076,28,1,2,8,1,1};
UINT4 Fsipv6NdWanCacheIPv6Addr [ ] ={1,3,6,1,4,1,2076,28,1,2,8,1,2};
UINT4 Fsipv6NdWanCacheStatus [ ] ={1,3,6,1,4,1,2076,28,1,2,8,1,3};
UINT4 Fsipv6NdWanCacheState [ ] ={1,3,6,1,4,1,2076,28,1,2,8,1,4};
UINT4 Fsipv6NdWanCacheUseTime [ ] ={1,3,6,1,4,1,2076,28,1,2,8,1,5};
UINT4 Fsipv6PingIndex [ ] ={1,3,6,1,4,1,2076,28,1,2,12,1,1};
UINT4 Fsipv6PingDest [ ] ={1,3,6,1,4,1,2076,28,1,2,12,1,2};
UINT4 Fsipv6PingIfIndex [ ] ={1,3,6,1,4,1,2076,28,1,2,12,1,3};
UINT4 Fsipv6PingAdminStatus [ ] ={1,3,6,1,4,1,2076,28,1,2,12,1,4};
UINT4 Fsipv6PingInterval [ ] ={1,3,6,1,4,1,2076,28,1,2,12,1,5};
UINT4 Fsipv6PingRcvTimeout [ ] ={1,3,6,1,4,1,2076,28,1,2,12,1,6};
UINT4 Fsipv6PingTries [ ] ={1,3,6,1,4,1,2076,28,1,2,12,1,7};
UINT4 Fsipv6PingSize [ ] ={1,3,6,1,4,1,2076,28,1,2,12,1,8};
UINT4 Fsipv6PingSentCount [ ] ={1,3,6,1,4,1,2076,28,1,2,12,1,9};
UINT4 Fsipv6PingAverageTime [ ] ={1,3,6,1,4,1,2076,28,1,2,12,1,10};
UINT4 Fsipv6PingMaxTime [ ] ={1,3,6,1,4,1,2076,28,1,2,12,1,11};
UINT4 Fsipv6PingMinTime [ ] ={1,3,6,1,4,1,2076,28,1,2,12,1,12};
UINT4 Fsipv6PingOperStatus [ ] ={1,3,6,1,4,1,2076,28,1,2,12,1,13};
UINT4 Fsipv6PingSuccesses [ ] ={1,3,6,1,4,1,2076,28,1,2,12,1,14};
UINT4 Fsipv6PingPercentageLoss [ ] ={1,3,6,1,4,1,2076,28,1,2,12,1,15};
UINT4 Fsipv6PingData [ ] ={1,3,6,1,4,1,2076,28,1,2,12,1,16};
UINT4 Fsipv6PingSrcAddr [ ] ={1,3,6,1,4,1,2076,28,1,2,12,1,17};
UINT4 Fsipv6PingZoneId [ ] ={1,3,6,1,4,1,2076,28,1,2,12,1,18};
UINT4 Fsipv6PingDestAddrType [ ] ={1,3,6,1,4,1,2076,28,1,2,12,1,19};
UINT4 Fsipv6PingHostName [ ] ={1,3,6,1,4,1,2076,28,1,2,12,1,20};
UINT4 Fsipv6NdProxyAddr [ ] ={1,3,6,1,4,1,2076,28,1,2,13,1,1};
UINT4 Fsipv6NdProxyAdminStatus [ ] ={1,3,6,1,4,1,2076,28,1,2,13,1,2};
UINT4 Fsipv6AddrSelPolicyPrefix [ ] ={1,3,6,1,4,1,2076,28,1,2,14,1,1};
UINT4 Fsipv6AddrSelPolicyPrefixLen [ ] ={1,3,6,1,4,1,2076,28,1,2,14,1,2};
UINT4 Fsipv6AddrSelPolicyIfIndex [ ] ={1,3,6,1,4,1,2076,28,1,2,14,1,3};
UINT4 Fsipv6AddrSelPolicyScope [ ] ={1,3,6,1,4,1,2076,28,1,2,14,1,4};
UINT4 Fsipv6AddrSelPolicyPrecedence [ ] ={1,3,6,1,4,1,2076,28,1,2,14,1,5};
UINT4 Fsipv6AddrSelPolicyLabel [ ] ={1,3,6,1,4,1,2076,28,1,2,14,1,6};
UINT4 Fsipv6AddrSelPolicyAddrType [ ] ={1,3,6,1,4,1,2076,28,1,2,14,1,7};
UINT4 Fsipv6AddrSelPolicyIsPublicAddr [ ] ={1,3,6,1,4,1,2076,28,1,2,14,1,8};
UINT4 Fsipv6AddrSelPolicyIsSelfAddr [ ] ={1,3,6,1,4,1,2076,28,1,2,14,1,9};
UINT4 Fsipv6AddrSelPolicyReachabilityStatus [ ] ={1,3,6,1,4,1,2076,28,1,2,14,1,10};
UINT4 Fsipv6AddrSelPolicyConfigStatus [ ] ={1,3,6,1,4,1,2076,28,1,2,14,1,11};
UINT4 Fsipv6AddrSelPolicyRowStatus [ ] ={1,3,6,1,4,1,2076,28,1,2,14,1,12};
UINT4 Fsipv6ScopeZoneIndexIfIndex [ ] ={1,3,6,1,4,1,2076,28,1,2,15,1,1};
UINT4 Fsipv6ScopeZoneIndexInterfaceLocal [ ] ={1,3,6,1,4,1,2076,28,1,2,15,1,2};
UINT4 Fsipv6ScopeZoneIndexLinkLocal [ ] ={1,3,6,1,4,1,2076,28,1,2,15,1,3};
UINT4 Fsipv6ScopeZoneIndex3 [ ] ={1,3,6,1,4,1,2076,28,1,2,15,1,4};
UINT4 Fsipv6ScopeZoneIndexAdminLocal [ ] ={1,3,6,1,4,1,2076,28,1,2,15,1,5};
UINT4 Fsipv6ScopeZoneIndexSiteLocal [ ] ={1,3,6,1,4,1,2076,28,1,2,15,1,6};
UINT4 Fsipv6ScopeZoneIndex6 [ ] ={1,3,6,1,4,1,2076,28,1,2,15,1,7};
UINT4 Fsipv6ScopeZoneIndex7 [ ] ={1,3,6,1,4,1,2076,28,1,2,15,1,8};
UINT4 Fsipv6ScopeZoneIndexOrganizationLocal [ ] ={1,3,6,1,4,1,2076,28,1,2,15,1,9};
UINT4 Fsipv6ScopeZoneIndex9 [ ] ={1,3,6,1,4,1,2076,28,1,2,15,1,10};
UINT4 Fsipv6ScopeZoneIndexA [ ] ={1,3,6,1,4,1,2076,28,1,2,15,1,11};
UINT4 Fsipv6ScopeZoneIndexB [ ] ={1,3,6,1,4,1,2076,28,1,2,15,1,12};
UINT4 Fsipv6ScopeZoneIndexC [ ] ={1,3,6,1,4,1,2076,28,1,2,15,1,13};
UINT4 Fsipv6ScopeZoneIndexD [ ] ={1,3,6,1,4,1,2076,28,1,2,15,1,14};
UINT4 Fsipv6ScopeZoneIndexE [ ] ={1,3,6,1,4,1,2076,28,1,2,15,1,15};
UINT4 Fsipv6IfScopeZoneCreationStatus [ ] ={1,3,6,1,4,1,2076,28,1,2,15,1,16};
UINT4 Fsipv6IfScopeZoneRowStatus [ ] ={1,3,6,1,4,1,2076,28,1,2,15,1,17};
UINT4 Fsipv6ScopeZoneName [ ] ={1,3,6,1,4,1,2076,28,1,2,16,1,1};
UINT4 Fsipv6ScopeZoneIndex [ ] ={1,3,6,1,4,1,2076,28,1,2,16,1,2};
UINT4 Fsipv6ScopeZoneCreationStatus [ ] ={1,3,6,1,4,1,2076,28,1,2,16,1,3};
UINT4 Fsipv6ScopeZoneInterfaceList [ ] ={1,3,6,1,4,1,2076,28,1,2,16,1,4};
UINT4 Fsipv6IsDefaultScopeZone [ ] ={1,3,6,1,4,1,2076,28,1,2,16,1,5};
UINT4 Fsipv6SENDSentPktType [ ] ={1,3,6,1,4,1,2076,28,1,2,17,1,1};
UINT4 Fsipv6SENDSentCgaOptPkts [ ] ={1,3,6,1,4,1,2076,28,1,2,17,1,2};
UINT4 Fsipv6SENDSentCertOptPkts [ ] ={1,3,6,1,4,1,2076,28,1,2,17,1,3};
UINT4 Fsipv6SENDSentMtuOptPkts [ ] ={1,3,6,1,4,1,2076,28,1,2,17,1,4};
UINT4 Fsipv6SENDSentNonceOptPkts [ ] ={1,3,6,1,4,1,2076,28,1,2,17,1,5};
UINT4 Fsipv6SENDSentPrefixOptPkts [ ] ={1,3,6,1,4,1,2076,28,1,2,17,1,6};
UINT4 Fsipv6SENDSentRedirHdrPkts [ ] ={1,3,6,1,4,1,2076,28,1,2,17,1,7};
UINT4 Fsipv6SENDSentRsaOptPkts [ ] ={1,3,6,1,4,1,2076,28,1,2,17,1,8};
UINT4 Fsipv6SENDSentSrcLinkAddrPkts [ ] ={1,3,6,1,4,1,2076,28,1,2,17,1,9};
UINT4 Fsipv6SENDSentTgtLinkAddrPkts [ ] ={1,3,6,1,4,1,2076,28,1,2,17,1,10};
UINT4 Fsipv6SENDSentTaOptPkts [ ] ={1,3,6,1,4,1,2076,28,1,2,17,1,11};
UINT4 Fsipv6SENDSentTimeStampOptPkts [ ] ={1,3,6,1,4,1,2076,28,1,2,17,1,12};
UINT4 Fsipv6SENDRcvPktType [ ] ={1,3,6,1,4,1,2076,28,1,2,18,1,1};
UINT4 Fsipv6SENDRcvCgaOptPkts [ ] ={1,3,6,1,4,1,2076,28,1,2,18,1,2};
UINT4 Fsipv6SENDRcvCertOptPkts [ ] ={1,3,6,1,4,1,2076,28,1,2,18,1,3};
UINT4 Fsipv6SENDRcvMtuOptPkts [ ] ={1,3,6,1,4,1,2076,28,1,2,18,1,4};
UINT4 Fsipv6SENDRcvNonceOptPkts [ ] ={1,3,6,1,4,1,2076,28,1,2,18,1,5};
UINT4 Fsipv6SENDRcvPrefixOptPkts [ ] ={1,3,6,1,4,1,2076,28,1,2,18,1,6};
UINT4 Fsipv6SENDRcvRedirHdrPkts [ ] ={1,3,6,1,4,1,2076,28,1,2,18,1,7};
UINT4 Fsipv6SENDRcvRsaOptPkts [ ] ={1,3,6,1,4,1,2076,28,1,2,18,1,8};
UINT4 Fsipv6SENDRcvSrcLinkAddrPkts [ ] ={1,3,6,1,4,1,2076,28,1,2,18,1,9};
UINT4 Fsipv6SENDRcvTgtLinkAddrPkts [ ] ={1,3,6,1,4,1,2076,28,1,2,18,1,10};
UINT4 Fsipv6SENDRcvTaOptPkts [ ] ={1,3,6,1,4,1,2076,28,1,2,18,1,11};
UINT4 Fsipv6SENDRcvTimeStampOptPkts [ ] ={1,3,6,1,4,1,2076,28,1,2,18,1,12};
UINT4 Fsipv6SENDDrpPktType [ ] ={1,3,6,1,4,1,2076,28,1,2,19,1,1};
UINT4 Fsipv6SENDDrpCgaOptPkts [ ] ={1,3,6,1,4,1,2076,28,1,2,19,1,2};
UINT4 Fsipv6SENDDrpCertOptPkts [ ] ={1,3,6,1,4,1,2076,28,1,2,19,1,3};
UINT4 Fsipv6SENDDrpMtuOptPkts [ ] ={1,3,6,1,4,1,2076,28,1,2,19,1,4};
UINT4 Fsipv6SENDDrpNonceOptPkts [ ] ={1,3,6,1,4,1,2076,28,1,2,19,1,5};
UINT4 Fsipv6SENDDrpPrefixOptPkts [ ] ={1,3,6,1,4,1,2076,28,1,2,19,1,6};
UINT4 Fsipv6SENDDrpRedirHdrPkts [ ] ={1,3,6,1,4,1,2076,28,1,2,19,1,7};
UINT4 Fsipv6SENDDrpRsaOptPkts [ ] ={1,3,6,1,4,1,2076,28,1,2,19,1,8};
UINT4 Fsipv6SENDDrpSrcLinkAddrPkts [ ] ={1,3,6,1,4,1,2076,28,1,2,19,1,9};
UINT4 Fsipv6SENDDrpTgtLinkAddrPkts [ ] ={1,3,6,1,4,1,2076,28,1,2,19,1,10};
UINT4 Fsipv6SENDDrpTaOptPkts [ ] ={1,3,6,1,4,1,2076,28,1,2,19,1,11};
UINT4 Fsipv6SENDDrpTimeStampOptPkts [ ] ={1,3,6,1,4,1,2076,28,1,2,19,1,12};
UINT4 Fsipv6RARouteIfIndex [ ] ={1,3,6,1,4,1,2076,28,1,2,20,1,1};
UINT4 Fsipv6RARoutePrefix [ ] ={1,3,6,1,4,1,2076,28,1,2,20,1,2};
UINT4 Fsipv6RARoutePrefixLen [ ] ={1,3,6,1,4,1,2076,28,1,2,20,1,3};
UINT4 Fsipv6RARoutePreference [ ] ={1,3,6,1,4,1,2076,28,1,2,20,1,4};
UINT4 Fsipv6RARouteLifetime [ ] ={1,3,6,1,4,1,2076,28,1,2,20,1,5};
UINT4 Fsipv6RARouteRowStatus [ ] ={1,3,6,1,4,1,2076,28,1,2,20,1,6};
UINT4 Fsipv6IcmpInMsgs [ ] ={1,3,6,1,4,1,2076,28,2,1};
UINT4 Fsipv6IcmpInErrors [ ] ={1,3,6,1,4,1,2076,28,2,2};
UINT4 Fsipv6IcmpInDestUnreachs [ ] ={1,3,6,1,4,1,2076,28,2,3};
UINT4 Fsipv6IcmpInTimeExcds [ ] ={1,3,6,1,4,1,2076,28,2,4};
UINT4 Fsipv6IcmpInParmProbs [ ] ={1,3,6,1,4,1,2076,28,2,5};
UINT4 Fsipv6IcmpInPktTooBigs [ ] ={1,3,6,1,4,1,2076,28,2,6};
UINT4 Fsipv6IcmpInEchos [ ] ={1,3,6,1,4,1,2076,28,2,7};
UINT4 Fsipv6IcmpInEchoReps [ ] ={1,3,6,1,4,1,2076,28,2,8};
UINT4 Fsipv6IcmpInRouterSolicits [ ] ={1,3,6,1,4,1,2076,28,2,9};
UINT4 Fsipv6IcmpInRouterAdvertisements [ ] ={1,3,6,1,4,1,2076,28,2,10};
UINT4 Fsipv6IcmpInNeighborSolicits [ ] ={1,3,6,1,4,1,2076,28,2,11};
UINT4 Fsipv6IcmpInNeighborAdvertisements [ ] ={1,3,6,1,4,1,2076,28,2,12};
UINT4 Fsipv6IcmpInRedirects [ ] ={1,3,6,1,4,1,2076,28,2,13};
UINT4 Fsipv6IcmpInAdminProhib [ ] ={1,3,6,1,4,1,2076,28,2,14};
UINT4 Fsipv6IcmpOutMsgs [ ] ={1,3,6,1,4,1,2076,28,2,15};
UINT4 Fsipv6IcmpOutErrors [ ] ={1,3,6,1,4,1,2076,28,2,16};
UINT4 Fsipv6IcmpOutDestUnreachs [ ] ={1,3,6,1,4,1,2076,28,2,17};
UINT4 Fsipv6IcmpOutTimeExcds [ ] ={1,3,6,1,4,1,2076,28,2,18};
UINT4 Fsipv6IcmpOutParmProbs [ ] ={1,3,6,1,4,1,2076,28,2,19};
UINT4 Fsipv6IcmpOutPktTooBigs [ ] ={1,3,6,1,4,1,2076,28,2,20};
UINT4 Fsipv6IcmpOutEchos [ ] ={1,3,6,1,4,1,2076,28,2,21};
UINT4 Fsipv6IcmpOutEchoReps [ ] ={1,3,6,1,4,1,2076,28,2,22};
UINT4 Fsipv6IcmpOutRouterSolicits [ ] ={1,3,6,1,4,1,2076,28,2,23};
UINT4 Fsipv6IcmpOutRouterAdvertisements [ ] ={1,3,6,1,4,1,2076,28,2,24};
UINT4 Fsipv6IcmpOutNeighborSolicits [ ] ={1,3,6,1,4,1,2076,28,2,25};
UINT4 Fsipv6IcmpOutNeighborAdvertisements [ ] ={1,3,6,1,4,1,2076,28,2,26};
UINT4 Fsipv6IcmpOutRedirects [ ] ={1,3,6,1,4,1,2076,28,2,27};
UINT4 Fsipv6IcmpOutAdminProhib [ ] ={1,3,6,1,4,1,2076,28,2,28};
UINT4 Fsipv6IcmpInBadCode [ ] ={1,3,6,1,4,1,2076,28,2,29};
UINT4 Fsipv6IcmpInNARouterFlagSet [ ] ={1,3,6,1,4,1,2076,28,2,30};
UINT4 Fsipv6IcmpInNASolicitedFlagSet [ ] ={1,3,6,1,4,1,2076,28,2,31};
UINT4 Fsipv6IcmpInNAOverrideFlagSet [ ] ={1,3,6,1,4,1,2076,28,2,32};
UINT4 Fsipv6IcmpOutNARouterFlagSet [ ] ={1,3,6,1,4,1,2076,28,2,33};
UINT4 Fsipv6IcmpOutNASolicitedFlagSet [ ] ={1,3,6,1,4,1,2076,28,2,34};
UINT4 Fsipv6IcmpOutNAOverrideFlagSet [ ] ={1,3,6,1,4,1,2076,28,2,35};
UINT4 Fsipv6UdpInDatagrams [ ] ={1,3,6,1,4,1,2076,28,3,1,1};
UINT4 Fsipv6UdpNoPorts [ ] ={1,3,6,1,4,1,2076,28,3,1,2};
UINT4 Fsipv6UdpInErrors [ ] ={1,3,6,1,4,1,2076,28,3,1,3};
UINT4 Fsipv6UdpOutDatagrams [ ] ={1,3,6,1,4,1,2076,28,3,1,4};
UINT4 Fsipv6RouteDest [ ] ={1,3,6,1,4,1,2076,28,6,1,1,1};
UINT4 Fsipv6RoutePfxLength [ ] ={1,3,6,1,4,1,2076,28,6,1,1,2};
UINT4 Fsipv6RouteProtocol [ ] ={1,3,6,1,4,1,2076,28,6,1,1,3};
UINT4 Fsipv6RouteNextHop [ ] ={1,3,6,1,4,1,2076,28,6,1,1,4};
UINT4 Fsipv6RouteIfIndex [ ] ={1,3,6,1,4,1,2076,28,6,1,1,5};
UINT4 Fsipv6RouteMetric [ ] ={1,3,6,1,4,1,2076,28,6,1,1,6};
UINT4 Fsipv6RouteType [ ] ={1,3,6,1,4,1,2076,28,6,1,1,7};
UINT4 Fsipv6RouteTag [ ] ={1,3,6,1,4,1,2076,28,6,1,1,8};
UINT4 Fsipv6RouteAge [ ] ={1,3,6,1,4,1,2076,28,6,1,1,9};
UINT4 Fsipv6RouteAdminStatus [ ] ={1,3,6,1,4,1,2076,28,6,1,1,10};
UINT4 Fsipv6RouteAddrType [ ] ={1,3,6,1,4,1,2076,28,6,1,1,11};
UINT4 Fsipv6RouteHwStatus [ ] ={1,3,6,1,4,1,2076,28,6,1,1,12};
UINT4 Fsipv6RoutePreference [ ] ={1,3,6,1,4,1,2076,28,6,1,1,13};
UINT4 Fsipv6Protocol [ ] ={1,3,6,1,4,1,2076,28,6,2,1,1};
UINT4 Fsipv6Preference [ ] ={1,3,6,1,4,1,2076,28,6,2,1,2};
UINT4 FsIpv6TestRedEntryTime [ ] ={1,3,6,1,4,1,2076,28,1,3,1};
UINT4 FsIpv6TestRedExitTime [ ] ={1,3,6,1,4,1,2076,28,1,3,2};
UINT4 Fsipv6IfRaRDNSSIndex [ ] ={1,3,6,1,4,1,2076,28,1,4,1,1,1};
UINT4 Fsipv6IfRadvRDNSSOpen [ ] ={1,3,6,1,4,1,2076,28,1,4,1,1,2};
UINT4 Fsipv6IfRaRDNSSPreference [ ] ={1,3,6,1,4,1,2076,28,1,4,1,1,3};
UINT4 Fsipv6IfRaRDNSSLifetime [ ] ={1,3,6,1,4,1,2076,28,1,4,1,1,4};
UINT4 Fsipv6IfRaRDNSSAddrOne [ ] ={1,3,6,1,4,1,2076,28,1,4,1,1,5};
UINT4 Fsipv6IfRaRDNSSAddrTwo [ ] ={1,3,6,1,4,1,2076,28,1,4,1,1,6};
UINT4 Fsipv6IfRaRDNSSAddrThree [ ] ={1,3,6,1,4,1,2076,28,1,4,1,1,7};
UINT4 Fsipv6IfRaRDNSSRowStatus [ ] ={1,3,6,1,4,1,2076,28,1,4,1,1,8};
UINT4 Fsipv6IfDomainNameOne [ ] ={1,3,6,1,4,1,2076,28,1,4,1,1,9};
UINT4 Fsipv6IfDomainNameTwo [ ] ={1,3,6,1,4,1,2076,28,1,4,1,1,10};
UINT4 Fsipv6IfDomainNameThree [ ] ={1,3,6,1,4,1,2076,28,1,4,1,1,11};
UINT4 Fsipv6IfDnsLifeTime [ ] ={1,3,6,1,4,1,2076,28,1,4,1,1,12};
UINT4 Fsipv6IfRaRDNSSAddrOneLife [ ] ={1,3,6,1,4,1,2076,28,1,4,1,1,13};
UINT4 Fsipv6IfRaRDNSSAddrTwoLife [ ] ={1,3,6,1,4,1,2076,28,1,4,1,1,14};
UINT4 Fsipv6IfRaRDNSSAddrThreeLife [ ] ={1,3,6,1,4,1,2076,28,1,4,1,1,15};
UINT4 Fsipv6IfDomainNameOneLife [ ] ={1,3,6,1,4,1,2076,28,1,4,1,1,16};
UINT4 Fsipv6IfDomainNameTwoLife [ ] ={1,3,6,1,4,1,2076,28,1,4,1,1,17};
UINT4 Fsipv6IfDomainNameThreeLife [ ] ={1,3,6,1,4,1,2076,28,1,4,1,1,18};




tMbDbEntry fsipv6MibEntry[]= {

{{11,Fsipv6NdCacheMaxRetries}, NULL, Fsipv6NdCacheMaxRetriesGet, Fsipv6NdCacheMaxRetriesSet, Fsipv6NdCacheMaxRetriesTest, Fsipv6NdCacheMaxRetriesDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "3"},

{{11,Fsipv6PmtuConfigStatus}, NULL, Fsipv6PmtuConfigStatusGet, Fsipv6PmtuConfigStatusSet, Fsipv6PmtuConfigStatusTest, Fsipv6PmtuConfigStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{11,Fsipv6PmtuTimeOutInterval}, NULL, Fsipv6PmtuTimeOutIntervalGet, Fsipv6PmtuTimeOutIntervalSet, Fsipv6PmtuTimeOutIntervalTest, Fsipv6PmtuTimeOutIntervalDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "60"},

{{11,Fsipv6JumboEnable}, NULL, Fsipv6JumboEnableGet, Fsipv6JumboEnableSet, Fsipv6JumboEnableTest, Fsipv6JumboEnableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{11,Fsipv6NumOfSendJumbo}, NULL, Fsipv6NumOfSendJumboGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,Fsipv6NumOfRecvJumbo}, NULL, Fsipv6NumOfRecvJumboGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,Fsipv6ErrJumbo}, NULL, Fsipv6ErrJumboGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,Fsipv6GlobalDebug}, NULL, Fsipv6GlobalDebugGet, Fsipv6GlobalDebugSet, Fsipv6GlobalDebugTest, Fsipv6GlobalDebugDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,Fsipv6MaxRouteEntries}, NULL, Fsipv6MaxRouteEntriesGet, Fsipv6MaxRouteEntriesSet, Fsipv6MaxRouteEntriesTest, Fsipv6MaxRouteEntriesDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,Fsipv6MaxLogicalIfaces}, NULL, Fsipv6MaxLogicalIfacesGet, Fsipv6MaxLogicalIfacesSet, Fsipv6MaxLogicalIfacesTest, Fsipv6MaxLogicalIfacesDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,Fsipv6MaxTunnelIfaces}, NULL, Fsipv6MaxTunnelIfacesGet, Fsipv6MaxTunnelIfacesSet, Fsipv6MaxTunnelIfacesTest, Fsipv6MaxTunnelIfacesDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,Fsipv6MaxAddresses}, NULL, Fsipv6MaxAddressesGet, Fsipv6MaxAddressesSet, Fsipv6MaxAddressesTest, Fsipv6MaxAddressesDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,Fsipv6MaxFragReasmEntries}, NULL, Fsipv6MaxFragReasmEntriesGet, Fsipv6MaxFragReasmEntriesSet, Fsipv6MaxFragReasmEntriesTest, Fsipv6MaxFragReasmEntriesDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,Fsipv6Nd6MaxCacheEntries}, NULL, Fsipv6Nd6MaxCacheEntriesGet, Fsipv6Nd6MaxCacheEntriesSet, Fsipv6Nd6MaxCacheEntriesTest, Fsipv6Nd6MaxCacheEntriesDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,Fsipv6PmtuMaxDest}, NULL, Fsipv6PmtuMaxDestGet, Fsipv6PmtuMaxDestSet, Fsipv6PmtuMaxDestTest, Fsipv6PmtuMaxDestDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,Fsipv6RFC5095Compatibility}, NULL, Fsipv6RFC5095CompatibilityGet, Fsipv6RFC5095CompatibilitySet, Fsipv6RFC5095CompatibilityTest, Fsipv6RFC5095CompatibilityDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{11,Fsipv6RFC5942Compatibility}, NULL, Fsipv6RFC5942CompatibilityGet, Fsipv6RFC5942CompatibilitySet, Fsipv6RFC5942CompatibilityTest, Fsipv6RFC5942CompatibilityDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},   

{{11,Fsipv6SENDSecLevel}, NULL, Fsipv6SENDSecLevelGet, Fsipv6SENDSecLevelSet, Fsipv6SENDSecLevelTest, Fsipv6SENDSecLevelDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,Fsipv6SENDNbrSecLevel}, NULL, Fsipv6SENDNbrSecLevelGet, Fsipv6SENDNbrSecLevelSet, Fsipv6SENDNbrSecLevelTest, Fsipv6SENDNbrSecLevelDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,Fsipv6SENDAuthType}, NULL, Fsipv6SENDAuthTypeGet, Fsipv6SENDAuthTypeSet, Fsipv6SENDAuthTypeTest, Fsipv6SENDAuthTypeDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,Fsipv6SENDMinBits}, NULL, Fsipv6SENDMinBitsGet, Fsipv6SENDMinBitsSet, Fsipv6SENDMinBitsTest, Fsipv6SENDMinBitsDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1024"},

{{11,Fsipv6SENDSecDAD}, NULL, Fsipv6SENDSecDADGet, Fsipv6SENDSecDADSet, Fsipv6SENDSecDADTest, Fsipv6SENDSecDADDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{11,Fsipv6SENDPrefixChk}, NULL, Fsipv6SENDPrefixChkGet, Fsipv6SENDPrefixChkSet, Fsipv6SENDPrefixChkTest, Fsipv6SENDPrefixChkDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{11,Fsipv6ECMPPRTTimer}, NULL, Fsipv6ECMPPRTTimerGet, Fsipv6ECMPPRTTimerSet, Fsipv6ECMPPRTTimerTest, Fsipv6ECMPPRTTimerDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "60"},

{{11,Fsipv6NdCacheTimeout}, NULL, Fsipv6NdCacheTimeoutGet, Fsipv6NdCacheTimeoutSet, Fsipv6NdCacheTimeoutTest, Fsipv6NdCacheTimeoutDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "30"},

{{13,Fsipv6IfIndex}, GetNextIndexFsipv6IfTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Fsipv6IfTableINDEX, 1, 0, 0, NULL},

{{13,Fsipv6IfType}, GetNextIndexFsipv6IfTable, Fsipv6IfTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Fsipv6IfTableINDEX, 1, 0, 0, NULL},

{{13,Fsipv6IfPortNum}, GetNextIndexFsipv6IfTable, Fsipv6IfPortNumGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Fsipv6IfTableINDEX, 1, 0, 0, NULL},

{{13,Fsipv6IfCircuitNum}, GetNextIndexFsipv6IfTable, Fsipv6IfCircuitNumGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Fsipv6IfTableINDEX, 1, 0, 0, NULL},

{{13,Fsipv6IfToken}, GetNextIndexFsipv6IfTable, Fsipv6IfTokenGet, Fsipv6IfTokenSet, Fsipv6IfTokenTest, Fsipv6IfTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Fsipv6IfTableINDEX, 1, 0, 0, NULL},

{{13,Fsipv6IfAdminStatus}, GetNextIndexFsipv6IfTable, Fsipv6IfAdminStatusGet, Fsipv6IfAdminStatusSet, Fsipv6IfAdminStatusTest, Fsipv6IfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsipv6IfTableINDEX, 1, 0, 0, NULL},

{{13,Fsipv6IfOperStatus}, GetNextIndexFsipv6IfTable, Fsipv6IfOperStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Fsipv6IfTableINDEX, 1, 0, 0, NULL},

{{13,Fsipv6IfRouterAdvStatus}, GetNextIndexFsipv6IfTable, Fsipv6IfRouterAdvStatusGet, Fsipv6IfRouterAdvStatusSet, Fsipv6IfRouterAdvStatusTest, Fsipv6IfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsipv6IfTableINDEX, 1, 0, 0, "2"},

{{13,Fsipv6IfRouterAdvFlags}, GetNextIndexFsipv6IfTable, Fsipv6IfRouterAdvFlagsGet, Fsipv6IfRouterAdvFlagsSet, Fsipv6IfRouterAdvFlagsTest, Fsipv6IfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsipv6IfTableINDEX, 1, 0, 0, "6"},

{{13,Fsipv6IfHopLimit}, GetNextIndexFsipv6IfTable, Fsipv6IfHopLimitGet, Fsipv6IfHopLimitSet, Fsipv6IfHopLimitTest, Fsipv6IfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Fsipv6IfTableINDEX, 1, 0, 0, "64"},

{{13,Fsipv6IfDefRouterTime}, GetNextIndexFsipv6IfTable, Fsipv6IfDefRouterTimeGet, Fsipv6IfDefRouterTimeSet, Fsipv6IfDefRouterTimeTest, Fsipv6IfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Fsipv6IfTableINDEX, 1, 0, 0, "0"},

{{13,Fsipv6IfReachableTime}, GetNextIndexFsipv6IfTable, Fsipv6IfReachableTimeGet, Fsipv6IfReachableTimeSet, Fsipv6IfReachableTimeTest, Fsipv6IfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Fsipv6IfTableINDEX, 1, 0, 0, "30"},

{{13,Fsipv6IfRetransmitTime}, GetNextIndexFsipv6IfTable, Fsipv6IfRetransmitTimeGet, Fsipv6IfRetransmitTimeSet, Fsipv6IfRetransmitTimeTest, Fsipv6IfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Fsipv6IfTableINDEX, 1, 0, 0, "1"},

{{13,Fsipv6IfDelayProbeTime}, GetNextIndexFsipv6IfTable, Fsipv6IfDelayProbeTimeGet, Fsipv6IfDelayProbeTimeSet, Fsipv6IfDelayProbeTimeTest, Fsipv6IfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Fsipv6IfTableINDEX, 1, 0, 0, "5"},

{{13,Fsipv6IfPrefixAdvStatus}, GetNextIndexFsipv6IfTable, Fsipv6IfPrefixAdvStatusGet, Fsipv6IfPrefixAdvStatusSet, Fsipv6IfPrefixAdvStatusTest, Fsipv6IfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsipv6IfTableINDEX, 1, 0, 0, "1"},

{{13,Fsipv6IfMinRouterAdvTime}, GetNextIndexFsipv6IfTable, Fsipv6IfMinRouterAdvTimeGet, Fsipv6IfMinRouterAdvTimeSet, Fsipv6IfMinRouterAdvTimeTest, Fsipv6IfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Fsipv6IfTableINDEX, 1, 0, 0, "198"},

{{13,Fsipv6IfMaxRouterAdvTime}, GetNextIndexFsipv6IfTable, Fsipv6IfMaxRouterAdvTimeGet, Fsipv6IfMaxRouterAdvTimeSet, Fsipv6IfMaxRouterAdvTimeTest, Fsipv6IfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Fsipv6IfTableINDEX, 1, 0, 0, "600"},

{{13,Fsipv6IfDADRetries}, GetNextIndexFsipv6IfTable, Fsipv6IfDADRetriesGet, Fsipv6IfDADRetriesSet, Fsipv6IfDADRetriesTest, Fsipv6IfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Fsipv6IfTableINDEX, 1, 0, 0, "1"},

{{13,Fsipv6IfForwarding}, GetNextIndexFsipv6IfTable, Fsipv6IfForwardingGet, Fsipv6IfForwardingSet, Fsipv6IfForwardingTest, Fsipv6IfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsipv6IfTableINDEX, 1, 0, 0, "1"},

{{13,Fsipv6IfRoutingStatus}, GetNextIndexFsipv6IfTable, Fsipv6IfRoutingStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Fsipv6IfTableINDEX, 1, 0, 0, NULL},

{{13,Fsipv6IfIcmpErrInterval}, GetNextIndexFsipv6IfTable, Fsipv6IfIcmpErrIntervalGet, Fsipv6IfIcmpErrIntervalSet, Fsipv6IfIcmpErrIntervalTest, Fsipv6IfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Fsipv6IfTableINDEX, 1, 0, 0, "100"},

{{13,Fsipv6IfIcmpTokenBucketSize}, GetNextIndexFsipv6IfTable, Fsipv6IfIcmpTokenBucketSizeGet, Fsipv6IfIcmpTokenBucketSizeSet, Fsipv6IfIcmpTokenBucketSizeTest, Fsipv6IfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Fsipv6IfTableINDEX, 1, 0, 0, "10"},

{{13,Fsipv6IfDestUnreachableMsg}, GetNextIndexFsipv6IfTable, Fsipv6IfDestUnreachableMsgGet, Fsipv6IfDestUnreachableMsgSet, Fsipv6IfDestUnreachableMsgTest, Fsipv6IfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsipv6IfTableINDEX, 1, 0, 0, "1"},

{{13,Fsipv6IfUnnumAssocIPIf}, GetNextIndexFsipv6IfTable, Fsipv6IfUnnumAssocIPIfGet, Fsipv6IfUnnumAssocIPIfSet, Fsipv6IfUnnumAssocIPIfTest, Fsipv6IfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsipv6IfTableINDEX, 1, 0, 0, NULL},

{{13,Fsipv6IfRedirectMsg}, GetNextIndexFsipv6IfTable, Fsipv6IfRedirectMsgGet, Fsipv6IfRedirectMsgSet, Fsipv6IfRedirectMsgTest, Fsipv6IfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsipv6IfTableINDEX, 1, 0, 0, "2"},

{{13,Fsipv6IfAdvSrcLLAdr}, GetNextIndexFsipv6IfTable, Fsipv6IfAdvSrcLLAdrGet, Fsipv6IfAdvSrcLLAdrSet, Fsipv6IfAdvSrcLLAdrTest, Fsipv6IfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsipv6IfTableINDEX, 1, 0, 0, "2"},

{{13,Fsipv6IfAdvIntOpt}, GetNextIndexFsipv6IfTable, Fsipv6IfAdvIntOptGet, Fsipv6IfAdvIntOptSet, Fsipv6IfAdvIntOptTest, Fsipv6IfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsipv6IfTableINDEX, 1, 0, 0, "2"},

{{13,Fsipv6IfNDProxyAdminStatus}, GetNextIndexFsipv6IfTable, Fsipv6IfNDProxyAdminStatusGet, Fsipv6IfNDProxyAdminStatusSet, Fsipv6IfNDProxyAdminStatusTest, Fsipv6IfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsipv6IfTableINDEX, 1, 0, 0, "2"},

{{13,Fsipv6IfNDProxyMode}, GetNextIndexFsipv6IfTable, Fsipv6IfNDProxyModeGet, Fsipv6IfNDProxyModeSet, Fsipv6IfNDProxyModeTest, Fsipv6IfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsipv6IfTableINDEX, 1, 0, 0, "1"},

{{13,Fsipv6IfNDProxyOperStatus}, GetNextIndexFsipv6IfTable, Fsipv6IfNDProxyOperStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Fsipv6IfTableINDEX, 1, 0, 0, "2"},

{{13,Fsipv6IfNDProxyUpStream}, GetNextIndexFsipv6IfTable, Fsipv6IfNDProxyUpStreamGet, Fsipv6IfNDProxyUpStreamSet, Fsipv6IfNDProxyUpStreamTest, Fsipv6IfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsipv6IfTableINDEX, 1, 0, 0, "2"},

{{13,Fsipv6IfSENDSecStatus}, GetNextIndexFsipv6IfTable, Fsipv6IfSENDSecStatusGet, Fsipv6IfSENDSecStatusSet, Fsipv6IfSENDSecStatusTest, Fsipv6IfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsipv6IfTableINDEX, 1, 0, 0, "2"},

{{13,Fsipv6IfSENDDeltaTimestamp}, GetNextIndexFsipv6IfTable, Fsipv6IfSENDDeltaTimestampGet, Fsipv6IfSENDDeltaTimestampSet, Fsipv6IfSENDDeltaTimestampTest, Fsipv6IfTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Fsipv6IfTableINDEX, 1, 0, 0, "300"},

{{13,Fsipv6IfSENDFuzzTimestamp}, GetNextIndexFsipv6IfTable, Fsipv6IfSENDFuzzTimestampGet, Fsipv6IfSENDFuzzTimestampSet, Fsipv6IfSENDFuzzTimestampTest, Fsipv6IfTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Fsipv6IfTableINDEX, 1, 0, 0, "1"},

{{13,Fsipv6IfSENDDriftTimestamp}, GetNextIndexFsipv6IfTable, Fsipv6IfSENDDriftTimestampGet, Fsipv6IfSENDDriftTimestampSet, Fsipv6IfSENDDriftTimestampTest, Fsipv6IfTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Fsipv6IfTableINDEX, 1, 0, 0, "1"},

{{13,Fsipv6IfDefRoutePreference}, GetNextIndexFsipv6IfTable, Fsipv6IfDefRoutePreferenceGet, Fsipv6IfDefRoutePreferenceSet, Fsipv6IfDefRoutePreferenceTest, Fsipv6IfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsipv6IfTableINDEX, 1, 0, 0, "2"},

{{13,Fsipv6IfStatsIndex}, GetNextIndexFsipv6IfStatsTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Fsipv6IfStatsTableINDEX, 1, 0, 0, NULL},

{{13,Fsipv6IfStatsInReceives}, GetNextIndexFsipv6IfStatsTable, Fsipv6IfStatsInReceivesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsipv6IfStatsTableINDEX, 1, 0, 0, NULL},

{{13,Fsipv6IfStatsInHdrErrors}, GetNextIndexFsipv6IfStatsTable, Fsipv6IfStatsInHdrErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsipv6IfStatsTableINDEX, 1, 0, 0, NULL},

{{13,Fsipv6IfStatsTooBigErrors}, GetNextIndexFsipv6IfStatsTable, Fsipv6IfStatsTooBigErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsipv6IfStatsTableINDEX, 1, 0, 0, NULL},

{{13,Fsipv6IfStatsInAddrErrors}, GetNextIndexFsipv6IfStatsTable, Fsipv6IfStatsInAddrErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsipv6IfStatsTableINDEX, 1, 0, 0, NULL},

{{13,Fsipv6IfStatsForwDatagrams}, GetNextIndexFsipv6IfStatsTable, Fsipv6IfStatsForwDatagramsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsipv6IfStatsTableINDEX, 1, 0, 0, NULL},

{{13,Fsipv6IfStatsInUnknownProtos}, GetNextIndexFsipv6IfStatsTable, Fsipv6IfStatsInUnknownProtosGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsipv6IfStatsTableINDEX, 1, 0, 0, NULL},

{{13,Fsipv6IfStatsInDiscards}, GetNextIndexFsipv6IfStatsTable, Fsipv6IfStatsInDiscardsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsipv6IfStatsTableINDEX, 1, 0, 0, NULL},

{{13,Fsipv6IfStatsInDelivers}, GetNextIndexFsipv6IfStatsTable, Fsipv6IfStatsInDeliversGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsipv6IfStatsTableINDEX, 1, 0, 0, NULL},

{{13,Fsipv6IfStatsOutRequests}, GetNextIndexFsipv6IfStatsTable, Fsipv6IfStatsOutRequestsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsipv6IfStatsTableINDEX, 1, 0, 0, NULL},

{{13,Fsipv6IfStatsOutDiscards}, GetNextIndexFsipv6IfStatsTable, Fsipv6IfStatsOutDiscardsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsipv6IfStatsTableINDEX, 1, 0, 0, NULL},

{{13,Fsipv6IfStatsOutNoRoutes}, GetNextIndexFsipv6IfStatsTable, Fsipv6IfStatsOutNoRoutesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsipv6IfStatsTableINDEX, 1, 0, 0, NULL},

{{13,Fsipv6IfStatsReasmReqds}, GetNextIndexFsipv6IfStatsTable, Fsipv6IfStatsReasmReqdsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsipv6IfStatsTableINDEX, 1, 0, 0, NULL},

{{13,Fsipv6IfStatsReasmOKs}, GetNextIndexFsipv6IfStatsTable, Fsipv6IfStatsReasmOKsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsipv6IfStatsTableINDEX, 1, 0, 0, NULL},

{{13,Fsipv6IfStatsReasmFails}, GetNextIndexFsipv6IfStatsTable, Fsipv6IfStatsReasmFailsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsipv6IfStatsTableINDEX, 1, 0, 0, NULL},

{{13,Fsipv6IfStatsFragOKs}, GetNextIndexFsipv6IfStatsTable, Fsipv6IfStatsFragOKsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsipv6IfStatsTableINDEX, 1, 0, 0, NULL},

{{13,Fsipv6IfStatsFragFails}, GetNextIndexFsipv6IfStatsTable, Fsipv6IfStatsFragFailsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsipv6IfStatsTableINDEX, 1, 0, 0, NULL},

{{13,Fsipv6IfStatsFragCreates}, GetNextIndexFsipv6IfStatsTable, Fsipv6IfStatsFragCreatesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsipv6IfStatsTableINDEX, 1, 0, 0, NULL},

{{13,Fsipv6IfStatsInMcastPkts}, GetNextIndexFsipv6IfStatsTable, Fsipv6IfStatsInMcastPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsipv6IfStatsTableINDEX, 1, 0, 0, NULL},

{{13,Fsipv6IfStatsOutMcastPkts}, GetNextIndexFsipv6IfStatsTable, Fsipv6IfStatsOutMcastPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsipv6IfStatsTableINDEX, 1, 0, 0, NULL},

{{13,Fsipv6IfStatsInTruncatedPkts}, GetNextIndexFsipv6IfStatsTable, Fsipv6IfStatsInTruncatedPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsipv6IfStatsTableINDEX, 1, 0, 0, NULL},

{{13,Fsipv6IfStatsInRouterSols}, GetNextIndexFsipv6IfStatsTable, Fsipv6IfStatsInRouterSolsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsipv6IfStatsTableINDEX, 1, 0, 0, NULL},

{{13,Fsipv6IfStatsInRouterAdvs}, GetNextIndexFsipv6IfStatsTable, Fsipv6IfStatsInRouterAdvsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsipv6IfStatsTableINDEX, 1, 0, 0, NULL},

{{13,Fsipv6IfStatsInNeighSols}, GetNextIndexFsipv6IfStatsTable, Fsipv6IfStatsInNeighSolsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsipv6IfStatsTableINDEX, 1, 0, 0, NULL},

{{13,Fsipv6IfStatsInNeighAdvs}, GetNextIndexFsipv6IfStatsTable, Fsipv6IfStatsInNeighAdvsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsipv6IfStatsTableINDEX, 1, 0, 0, NULL},

{{13,Fsipv6IfStatsInRedirects}, GetNextIndexFsipv6IfStatsTable, Fsipv6IfStatsInRedirectsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsipv6IfStatsTableINDEX, 1, 0, 0, NULL},

{{13,Fsipv6IfStatsOutRouterSols}, GetNextIndexFsipv6IfStatsTable, Fsipv6IfStatsOutRouterSolsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsipv6IfStatsTableINDEX, 1, 0, 0, NULL},

{{13,Fsipv6IfStatsOutRouterAdvs}, GetNextIndexFsipv6IfStatsTable, Fsipv6IfStatsOutRouterAdvsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsipv6IfStatsTableINDEX, 1, 0, 0, NULL},

{{13,Fsipv6IfStatsOutNeighSols}, GetNextIndexFsipv6IfStatsTable, Fsipv6IfStatsOutNeighSolsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsipv6IfStatsTableINDEX, 1, 0, 0, NULL},

{{13,Fsipv6IfStatsOutNeighAdvs}, GetNextIndexFsipv6IfStatsTable, Fsipv6IfStatsOutNeighAdvsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsipv6IfStatsTableINDEX, 1, 0, 0, NULL},

{{13,Fsipv6IfStatsOutRedirects}, GetNextIndexFsipv6IfStatsTable, Fsipv6IfStatsOutRedirectsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsipv6IfStatsTableINDEX, 1, 0, 0, NULL},

{{13,Fsipv6IfStatsLastRouterAdvTime}, GetNextIndexFsipv6IfStatsTable, Fsipv6IfStatsLastRouterAdvTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, Fsipv6IfStatsTableINDEX, 1, 0, 0, NULL},

{{13,Fsipv6IfStatsNextRouterAdvTime}, GetNextIndexFsipv6IfStatsTable, Fsipv6IfStatsNextRouterAdvTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, Fsipv6IfStatsTableINDEX, 1, 0, 0, NULL},

{{13,Fsipv6IfStatsIcmp6ErrRateLmtd}, GetNextIndexFsipv6IfStatsTable, Fsipv6IfStatsIcmp6ErrRateLmtdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsipv6IfStatsTableINDEX, 1, 0, 0, NULL},

{{13,Fsipv6IfStatsSENDDroppedPkts}, GetNextIndexFsipv6IfStatsTable, Fsipv6IfStatsSENDDroppedPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsipv6IfStatsTableINDEX, 1, 0, 0, NULL},

{{13,Fsipv6IfStatsSENDInvalidPkts}, GetNextIndexFsipv6IfStatsTable, Fsipv6IfStatsSENDInvalidPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsipv6IfStatsTableINDEX, 1, 0, 0, NULL},

{{13,Fsipv6PrefixIndex}, GetNextIndexFsipv6PrefixTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Fsipv6PrefixTableINDEX, 3, 0, 0, NULL},

{{13,Fsipv6PrefixAddress}, GetNextIndexFsipv6PrefixTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, Fsipv6PrefixTableINDEX, 3, 0, 0, NULL},

{{13,Fsipv6PrefixAddrLen}, GetNextIndexFsipv6PrefixTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Fsipv6PrefixTableINDEX, 3, 0, 0, NULL},

{{13,Fsipv6PrefixProfileIndex}, GetNextIndexFsipv6PrefixTable, Fsipv6PrefixProfileIndexGet, Fsipv6PrefixProfileIndexSet, Fsipv6PrefixProfileIndexTest, Fsipv6PrefixTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Fsipv6PrefixTableINDEX, 3, 0, 0, "0"},

{{13,Fsipv6PrefixAdminStatus}, GetNextIndexFsipv6PrefixTable, Fsipv6PrefixAdminStatusGet, Fsipv6PrefixAdminStatusSet, Fsipv6PrefixAdminStatusTest, Fsipv6PrefixTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsipv6PrefixTableINDEX, 3, 0, 1, NULL},

{{13,Fsipv6SupportEmbeddedRp}, GetNextIndexFsipv6PrefixTable, Fsipv6SupportEmbeddedRpGet, Fsipv6SupportEmbeddedRpSet, Fsipv6SupportEmbeddedRpTest, Fsipv6PrefixTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsipv6PrefixTableINDEX, 3, 0, 0, "2"},

{{13,Fsipv6AddrIndex}, GetNextIndexFsipv6AddrTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Fsipv6AddrTableINDEX, 3, 0, 0, NULL},

{{13,Fsipv6AddrAddress}, GetNextIndexFsipv6AddrTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, Fsipv6AddrTableINDEX, 3, 0, 0, NULL},

{{13,Fsipv6AddrPrefixLen}, GetNextIndexFsipv6AddrTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Fsipv6AddrTableINDEX, 3, 0, 0, NULL},

{{13,Fsipv6AddrAdminStatus}, GetNextIndexFsipv6AddrTable, Fsipv6AddrAdminStatusGet, Fsipv6AddrAdminStatusSet, Fsipv6AddrAdminStatusTest, Fsipv6AddrTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsipv6AddrTableINDEX, 3, 0, 1, NULL},

{{13,Fsipv6AddrType}, GetNextIndexFsipv6AddrTable, Fsipv6AddrTypeGet, Fsipv6AddrTypeSet, Fsipv6AddrTypeTest, Fsipv6AddrTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsipv6AddrTableINDEX, 3, 0, 0, NULL},

{{13,Fsipv6AddrProfIndex}, GetNextIndexFsipv6AddrTable, Fsipv6AddrProfIndexGet, Fsipv6AddrProfIndexSet, Fsipv6AddrProfIndexTest, Fsipv6AddrTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Fsipv6AddrTableINDEX, 3, 0, 0, "0"},

{{13,Fsipv6AddrOperStatus}, GetNextIndexFsipv6AddrTable, Fsipv6AddrOperStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Fsipv6AddrTableINDEX, 3, 0, 0, NULL},

{{13,Fsipv6AddrScope}, GetNextIndexFsipv6AddrTable, Fsipv6AddrScopeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Fsipv6AddrTableINDEX, 3, 0, 0, NULL},

{{13,Fsipv6AddrSENDCgaStatus}, GetNextIndexFsipv6AddrTable, Fsipv6AddrSENDCgaStatusGet, Fsipv6AddrSENDCgaStatusSet, Fsipv6AddrSENDCgaStatusTest, Fsipv6AddrTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsipv6AddrTableINDEX, 3, 0, 0, "2"},

{{13,Fsipv6AddrSENDCgaModifier}, GetNextIndexFsipv6AddrTable, Fsipv6AddrSENDCgaModifierGet, Fsipv6AddrSENDCgaModifierSet, Fsipv6AddrSENDCgaModifierTest, Fsipv6AddrTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Fsipv6AddrTableINDEX, 3, 0, 0, NULL},

{{13,Fsipv6AddrSENDCollisionCount}, GetNextIndexFsipv6AddrTable, Fsipv6AddrSENDCollisionCountGet, Fsipv6AddrSENDCollisionCountSet, Fsipv6AddrSENDCollisionCountTest, Fsipv6AddrTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Fsipv6AddrTableINDEX, 3, 0, 0, NULL},

{{13,Fsipv6AddrProfileIndex}, GetNextIndexFsipv6AddrProfileTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Fsipv6AddrProfileTableINDEX, 1, 0, 0, NULL},

{{13,Fsipv6AddrProfileStatus}, GetNextIndexFsipv6AddrProfileTable, Fsipv6AddrProfileStatusGet, Fsipv6AddrProfileStatusSet, Fsipv6AddrProfileStatusTest, Fsipv6AddrProfileTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsipv6AddrProfileTableINDEX, 1, 0, 0, NULL},

{{13,Fsipv6AddrProfilePrefixAdvStatus}, GetNextIndexFsipv6AddrProfileTable, Fsipv6AddrProfilePrefixAdvStatusGet, Fsipv6AddrProfilePrefixAdvStatusSet, Fsipv6AddrProfilePrefixAdvStatusTest, Fsipv6AddrProfileTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsipv6AddrProfileTableINDEX, 1, 0, 0, "1"},

{{13,Fsipv6AddrProfileOnLinkAdvStatus}, GetNextIndexFsipv6AddrProfileTable, Fsipv6AddrProfileOnLinkAdvStatusGet, Fsipv6AddrProfileOnLinkAdvStatusSet, Fsipv6AddrProfileOnLinkAdvStatusTest, Fsipv6AddrProfileTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsipv6AddrProfileTableINDEX, 1, 0, 0, "1"},

{{13,Fsipv6AddrProfileAutoConfAdvStatus}, GetNextIndexFsipv6AddrProfileTable, Fsipv6AddrProfileAutoConfAdvStatusGet, Fsipv6AddrProfileAutoConfAdvStatusSet, Fsipv6AddrProfileAutoConfAdvStatusTest, Fsipv6AddrProfileTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsipv6AddrProfileTableINDEX, 1, 0, 0, "1"},

{{13,Fsipv6AddrProfilePreferredTime}, GetNextIndexFsipv6AddrProfileTable, Fsipv6AddrProfilePreferredTimeGet, Fsipv6AddrProfilePreferredTimeSet, Fsipv6AddrProfilePreferredTimeTest, Fsipv6AddrProfileTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Fsipv6AddrProfileTableINDEX, 1, 0, 0, "604800"},

{{13,Fsipv6AddrProfileValidTime}, GetNextIndexFsipv6AddrProfileTable, Fsipv6AddrProfileValidTimeGet, Fsipv6AddrProfileValidTimeSet, Fsipv6AddrProfileValidTimeTest, Fsipv6AddrProfileTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Fsipv6AddrProfileTableINDEX, 1, 0, 0, "2147483647"},

{{13,Fsipv6AddrProfileValidLifeTimeFlag}, GetNextIndexFsipv6AddrProfileTable, Fsipv6AddrProfileValidLifeTimeFlagGet, Fsipv6AddrProfileValidLifeTimeFlagSet, Fsipv6AddrProfileValidLifeTimeFlagTest, Fsipv6AddrProfileTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsipv6AddrProfileTableINDEX, 1, 0, 0, "1"},

{{13,Fsipv6AddrProfilePreferredLifeTimeFlag}, GetNextIndexFsipv6AddrProfileTable, Fsipv6AddrProfilePreferredLifeTimeFlagGet, Fsipv6AddrProfilePreferredLifeTimeFlagSet, Fsipv6AddrProfilePreferredLifeTimeFlagTest, Fsipv6AddrProfileTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsipv6AddrProfileTableINDEX, 1, 0, 0, "1"},

{{13,Fsipv6PmtuDest}, GetNextIndexFsipv6PmtuTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, Fsipv6PmtuTableINDEX, 1, 0, 0, NULL},

{{13,Fsipv6Pmtu}, GetNextIndexFsipv6PmtuTable, Fsipv6PmtuGet, Fsipv6PmtuSet, Fsipv6PmtuTest, Fsipv6PmtuTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Fsipv6PmtuTableINDEX, 1, 0, 0, NULL},

{{13,Fsipv6PmtuTimeStamp}, GetNextIndexFsipv6PmtuTable, Fsipv6PmtuTimeStampGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Fsipv6PmtuTableINDEX, 1, 0, 0, NULL},

{{13,Fsipv6PmtuAdminStatus}, GetNextIndexFsipv6PmtuTable, Fsipv6PmtuAdminStatusGet, Fsipv6PmtuAdminStatusSet, Fsipv6PmtuAdminStatusTest, Fsipv6PmtuTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsipv6PmtuTableINDEX, 1, 0, 0, NULL},

{{13,Fsipv6NdLanCacheIfIndex}, GetNextIndexFsipv6NdLanCacheTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Fsipv6NdLanCacheTableINDEX, 2, 0, 0, NULL},

{{13,Fsipv6NdLanCacheIPv6Addr}, GetNextIndexFsipv6NdLanCacheTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, Fsipv6NdLanCacheTableINDEX, 2, 0, 0, NULL},

{{13,Fsipv6NdLanCacheStatus}, GetNextIndexFsipv6NdLanCacheTable, Fsipv6NdLanCacheStatusGet, Fsipv6NdLanCacheStatusSet, Fsipv6NdLanCacheStatusTest, Fsipv6NdLanCacheTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsipv6NdLanCacheTableINDEX, 2, 0, 0, NULL},

{{13,Fsipv6NdLanCachePhysAddr}, GetNextIndexFsipv6NdLanCacheTable, Fsipv6NdLanCachePhysAddrGet, Fsipv6NdLanCachePhysAddrSet, Fsipv6NdLanCachePhysAddrTest, Fsipv6NdLanCacheTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Fsipv6NdLanCacheTableINDEX, 2, 0, 0, NULL},

{{13,Fsipv6NdLanCacheState}, GetNextIndexFsipv6NdLanCacheTable, Fsipv6NdLanCacheStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Fsipv6NdLanCacheTableINDEX, 2, 0, 0, NULL},

{{13,Fsipv6NdLanCacheUseTime}, GetNextIndexFsipv6NdLanCacheTable, Fsipv6NdLanCacheUseTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, Fsipv6NdLanCacheTableINDEX, 2, 0, 0, NULL},

{{13,Fsipv6NdLanCacheIsSecure}, GetNextIndexFsipv6NdLanCacheTable, Fsipv6NdLanCacheIsSecureGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Fsipv6NdLanCacheTableINDEX, 2, 0, 0, NULL},

{{13,Fsipv6NdWanCacheIfIndex}, GetNextIndexFsipv6NdWanCacheTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Fsipv6NdWanCacheTableINDEX, 2, 0, 0, NULL},

{{13,Fsipv6NdWanCacheIPv6Addr}, GetNextIndexFsipv6NdWanCacheTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, Fsipv6NdWanCacheTableINDEX, 2, 0, 0, NULL},

{{13,Fsipv6NdWanCacheStatus}, GetNextIndexFsipv6NdWanCacheTable, Fsipv6NdWanCacheStatusGet, Fsipv6NdWanCacheStatusSet, Fsipv6NdWanCacheStatusTest, Fsipv6NdWanCacheTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsipv6NdWanCacheTableINDEX, 2, 0, 0, "1"},

{{13,Fsipv6NdWanCacheState}, GetNextIndexFsipv6NdWanCacheTable, Fsipv6NdWanCacheStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Fsipv6NdWanCacheTableINDEX, 2, 0, 0, NULL},

{{13,Fsipv6NdWanCacheUseTime}, GetNextIndexFsipv6NdWanCacheTable, Fsipv6NdWanCacheUseTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, Fsipv6NdWanCacheTableINDEX, 2, 0, 0, NULL},

{{13,Fsipv6PingIndex}, GetNextIndexFsipv6PingTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Fsipv6PingTableINDEX, 1, 0, 0, NULL},

{{13,Fsipv6PingDest}, GetNextIndexFsipv6PingTable, Fsipv6PingDestGet, Fsipv6PingDestSet, Fsipv6PingDestTest, Fsipv6PingTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Fsipv6PingTableINDEX, 1, 0, 0, NULL},

{{13,Fsipv6PingIfIndex}, GetNextIndexFsipv6PingTable, Fsipv6PingIfIndexGet, Fsipv6PingIfIndexSet, Fsipv6PingIfIndexTest, Fsipv6PingTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsipv6PingTableINDEX, 1, 0, 0, NULL},

{{13,Fsipv6PingAdminStatus}, GetNextIndexFsipv6PingTable, Fsipv6PingAdminStatusGet, Fsipv6PingAdminStatusSet, Fsipv6PingAdminStatusTest, Fsipv6PingTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsipv6PingTableINDEX, 1, 0, 0, NULL},

{{13,Fsipv6PingInterval}, GetNextIndexFsipv6PingTable, Fsipv6PingIntervalGet, Fsipv6PingIntervalSet, Fsipv6PingIntervalTest, Fsipv6PingTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Fsipv6PingTableINDEX, 1, 0, 0, "1"},

{{13,Fsipv6PingRcvTimeout}, GetNextIndexFsipv6PingTable, Fsipv6PingRcvTimeoutGet, Fsipv6PingRcvTimeoutSet, Fsipv6PingRcvTimeoutTest, Fsipv6PingTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Fsipv6PingTableINDEX, 1, 0, 0, "5"},

{{13,Fsipv6PingTries}, GetNextIndexFsipv6PingTable, Fsipv6PingTriesGet, Fsipv6PingTriesSet, Fsipv6PingTriesTest, Fsipv6PingTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Fsipv6PingTableINDEX, 1, 0, 0, "5"},

{{13,Fsipv6PingSize}, GetNextIndexFsipv6PingTable, Fsipv6PingSizeGet, Fsipv6PingSizeSet, Fsipv6PingSizeTest, Fsipv6PingTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Fsipv6PingTableINDEX, 1, 0, 0, "100"},

{{13,Fsipv6PingSentCount}, GetNextIndexFsipv6PingTable, Fsipv6PingSentCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Fsipv6PingTableINDEX, 1, 0, 0, NULL},

{{13,Fsipv6PingAverageTime}, GetNextIndexFsipv6PingTable, Fsipv6PingAverageTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Fsipv6PingTableINDEX, 1, 0, 0, NULL},

{{13,Fsipv6PingMaxTime}, GetNextIndexFsipv6PingTable, Fsipv6PingMaxTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Fsipv6PingTableINDEX, 1, 0, 0, NULL},

{{13,Fsipv6PingMinTime}, GetNextIndexFsipv6PingTable, Fsipv6PingMinTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Fsipv6PingTableINDEX, 1, 0, 0, NULL},

{{13,Fsipv6PingOperStatus}, GetNextIndexFsipv6PingTable, Fsipv6PingOperStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Fsipv6PingTableINDEX, 1, 0, 0, NULL},

{{13,Fsipv6PingSuccesses}, GetNextIndexFsipv6PingTable, Fsipv6PingSuccessesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsipv6PingTableINDEX, 1, 0, 0, NULL},

{{13,Fsipv6PingPercentageLoss}, GetNextIndexFsipv6PingTable, Fsipv6PingPercentageLossGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Fsipv6PingTableINDEX, 1, 0, 0, NULL},

{{13,Fsipv6PingData}, GetNextIndexFsipv6PingTable, Fsipv6PingDataGet, Fsipv6PingDataSet, Fsipv6PingDataTest, Fsipv6PingTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Fsipv6PingTableINDEX, 1, 0, 0, NULL},

{{13,Fsipv6PingSrcAddr}, GetNextIndexFsipv6PingTable, Fsipv6PingSrcAddrGet, Fsipv6PingSrcAddrSet, Fsipv6PingSrcAddrTest, Fsipv6PingTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Fsipv6PingTableINDEX, 1, 0, 0, NULL},

{{13,Fsipv6PingZoneId}, GetNextIndexFsipv6PingTable, Fsipv6PingZoneIdGet, Fsipv6PingZoneIdSet, Fsipv6PingZoneIdTest, Fsipv6PingTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Fsipv6PingTableINDEX, 1, 0, 0, NULL},

{{13,Fsipv6PingDestAddrType}, GetNextIndexFsipv6PingTable, Fsipv6PingDestAddrTypeGet, Fsipv6PingDestAddrTypeSet, Fsipv6PingDestAddrTypeTest, Fsipv6PingTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsipv6PingTableINDEX, 1, 0, 0, "0"},

{{13,Fsipv6PingHostName}, GetNextIndexFsipv6PingTable, Fsipv6PingHostNameGet, Fsipv6PingHostNameSet, Fsipv6PingHostNameTest, Fsipv6PingTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Fsipv6PingTableINDEX, 1, 0, 0, NULL},

{{13,Fsipv6NdProxyAddr}, GetNextIndexFsipv6NDProxyListTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, Fsipv6NDProxyListTableINDEX, 1, 0, 0, NULL},

{{13,Fsipv6NdProxyAdminStatus}, GetNextIndexFsipv6NDProxyListTable, Fsipv6NdProxyAdminStatusGet, Fsipv6NdProxyAdminStatusSet, Fsipv6NdProxyAdminStatusTest, Fsipv6NDProxyListTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsipv6NDProxyListTableINDEX, 1, 0, 0, NULL},

{{13,Fsipv6AddrSelPolicyPrefix}, GetNextIndexFsipv6AddrSelPolicyTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, Fsipv6AddrSelPolicyTableINDEX, 3, 0, 0, NULL},

{{13,Fsipv6AddrSelPolicyPrefixLen}, GetNextIndexFsipv6AddrSelPolicyTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Fsipv6AddrSelPolicyTableINDEX, 3, 0, 0, NULL},

{{13,Fsipv6AddrSelPolicyIfIndex}, GetNextIndexFsipv6AddrSelPolicyTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Fsipv6AddrSelPolicyTableINDEX, 3, 0, 0, NULL},

{{13,Fsipv6AddrSelPolicyScope}, GetNextIndexFsipv6AddrSelPolicyTable, Fsipv6AddrSelPolicyScopeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Fsipv6AddrSelPolicyTableINDEX, 3, 0, 0, NULL},

{{13,Fsipv6AddrSelPolicyPrecedence}, GetNextIndexFsipv6AddrSelPolicyTable, Fsipv6AddrSelPolicyPrecedenceGet, Fsipv6AddrSelPolicyPrecedenceSet, Fsipv6AddrSelPolicyPrecedenceTest, Fsipv6AddrSelPolicyTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Fsipv6AddrSelPolicyTableINDEX, 3, 0, 0, "30"},

{{13,Fsipv6AddrSelPolicyLabel}, GetNextIndexFsipv6AddrSelPolicyTable, Fsipv6AddrSelPolicyLabelGet, Fsipv6AddrSelPolicyLabelSet, Fsipv6AddrSelPolicyLabelTest, Fsipv6AddrSelPolicyTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Fsipv6AddrSelPolicyTableINDEX, 3, 0, 0, "2"},

{{13,Fsipv6AddrSelPolicyAddrType}, GetNextIndexFsipv6AddrSelPolicyTable, Fsipv6AddrSelPolicyAddrTypeGet, Fsipv6AddrSelPolicyAddrTypeSet, Fsipv6AddrSelPolicyAddrTypeTest, Fsipv6AddrSelPolicyTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsipv6AddrSelPolicyTableINDEX, 3, 0, 0, NULL},

{{13,Fsipv6AddrSelPolicyIsPublicAddr}, GetNextIndexFsipv6AddrSelPolicyTable, Fsipv6AddrSelPolicyIsPublicAddrGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Fsipv6AddrSelPolicyTableINDEX, 3, 0, 0, NULL},

{{13,Fsipv6AddrSelPolicyIsSelfAddr}, GetNextIndexFsipv6AddrSelPolicyTable, Fsipv6AddrSelPolicyIsSelfAddrGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Fsipv6AddrSelPolicyTableINDEX, 3, 0, 0, NULL},

{{13,Fsipv6AddrSelPolicyReachabilityStatus}, GetNextIndexFsipv6AddrSelPolicyTable, Fsipv6AddrSelPolicyReachabilityStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Fsipv6AddrSelPolicyTableINDEX, 3, 0, 0, NULL},

{{13,Fsipv6AddrSelPolicyConfigStatus}, GetNextIndexFsipv6AddrSelPolicyTable, Fsipv6AddrSelPolicyConfigStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Fsipv6AddrSelPolicyTableINDEX, 3, 0, 0, NULL},

{{13,Fsipv6AddrSelPolicyRowStatus}, GetNextIndexFsipv6AddrSelPolicyTable, Fsipv6AddrSelPolicyRowStatusGet, Fsipv6AddrSelPolicyRowStatusSet, Fsipv6AddrSelPolicyRowStatusTest, Fsipv6AddrSelPolicyTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsipv6AddrSelPolicyTableINDEX, 3, 0, 1, NULL},

{{13,Fsipv6ScopeZoneIndexIfIndex}, GetNextIndexFsipv6IfScopeZoneMapTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Fsipv6IfScopeZoneMapTableINDEX, 1, 0, 0, "Invalid"},

{{13,Fsipv6ScopeZoneIndexInterfaceLocal}, GetNextIndexFsipv6IfScopeZoneMapTable, Fsipv6ScopeZoneIndexInterfaceLocalGet, Fsipv6ScopeZoneIndexInterfaceLocalSet, Fsipv6ScopeZoneIndexInterfaceLocalTest, Fsipv6IfScopeZoneMapTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Fsipv6IfScopeZoneMapTableINDEX, 1, 0, 0, "Invalid"},

{{13,Fsipv6ScopeZoneIndexLinkLocal}, GetNextIndexFsipv6IfScopeZoneMapTable, Fsipv6ScopeZoneIndexLinkLocalGet, Fsipv6ScopeZoneIndexLinkLocalSet, Fsipv6ScopeZoneIndexLinkLocalTest, Fsipv6IfScopeZoneMapTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Fsipv6IfScopeZoneMapTableINDEX, 1, 0, 0, "Invalid"},

{{13,Fsipv6ScopeZoneIndex3}, GetNextIndexFsipv6IfScopeZoneMapTable, Fsipv6ScopeZoneIndex3Get, Fsipv6ScopeZoneIndex3Set, Fsipv6ScopeZoneIndex3Test, Fsipv6IfScopeZoneMapTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Fsipv6IfScopeZoneMapTableINDEX, 1, 0, 0, "Invalid"},

{{13,Fsipv6ScopeZoneIndexAdminLocal}, GetNextIndexFsipv6IfScopeZoneMapTable, Fsipv6ScopeZoneIndexAdminLocalGet, Fsipv6ScopeZoneIndexAdminLocalSet, Fsipv6ScopeZoneIndexAdminLocalTest, Fsipv6IfScopeZoneMapTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Fsipv6IfScopeZoneMapTableINDEX, 1, 0, 0, "Invalid"},

{{13,Fsipv6ScopeZoneIndexSiteLocal}, GetNextIndexFsipv6IfScopeZoneMapTable, Fsipv6ScopeZoneIndexSiteLocalGet, Fsipv6ScopeZoneIndexSiteLocalSet, Fsipv6ScopeZoneIndexSiteLocalTest, Fsipv6IfScopeZoneMapTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Fsipv6IfScopeZoneMapTableINDEX, 1, 0, 0, "Invalid"},

{{13,Fsipv6ScopeZoneIndex6}, GetNextIndexFsipv6IfScopeZoneMapTable, Fsipv6ScopeZoneIndex6Get, Fsipv6ScopeZoneIndex6Set, Fsipv6ScopeZoneIndex6Test, Fsipv6IfScopeZoneMapTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Fsipv6IfScopeZoneMapTableINDEX, 1, 0, 0, "Invalid"},

{{13,Fsipv6ScopeZoneIndex7}, GetNextIndexFsipv6IfScopeZoneMapTable, Fsipv6ScopeZoneIndex7Get, Fsipv6ScopeZoneIndex7Set, Fsipv6ScopeZoneIndex7Test, Fsipv6IfScopeZoneMapTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Fsipv6IfScopeZoneMapTableINDEX, 1, 0, 0, "Invalid"},

{{13,Fsipv6ScopeZoneIndexOrganizationLocal}, GetNextIndexFsipv6IfScopeZoneMapTable, Fsipv6ScopeZoneIndexOrganizationLocalGet, Fsipv6ScopeZoneIndexOrganizationLocalSet, Fsipv6ScopeZoneIndexOrganizationLocalTest, Fsipv6IfScopeZoneMapTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Fsipv6IfScopeZoneMapTableINDEX, 1, 0, 0, "Invalid"},

{{13,Fsipv6ScopeZoneIndex9}, GetNextIndexFsipv6IfScopeZoneMapTable, Fsipv6ScopeZoneIndex9Get, Fsipv6ScopeZoneIndex9Set, Fsipv6ScopeZoneIndex9Test, Fsipv6IfScopeZoneMapTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Fsipv6IfScopeZoneMapTableINDEX, 1, 0, 0, "Invalid"},

{{13,Fsipv6ScopeZoneIndexA}, GetNextIndexFsipv6IfScopeZoneMapTable, Fsipv6ScopeZoneIndexAGet, Fsipv6ScopeZoneIndexASet, Fsipv6ScopeZoneIndexATest, Fsipv6IfScopeZoneMapTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Fsipv6IfScopeZoneMapTableINDEX, 1, 0, 0, "Invalid"},

{{13,Fsipv6ScopeZoneIndexB}, GetNextIndexFsipv6IfScopeZoneMapTable, Fsipv6ScopeZoneIndexBGet, Fsipv6ScopeZoneIndexBSet, Fsipv6ScopeZoneIndexBTest, Fsipv6IfScopeZoneMapTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Fsipv6IfScopeZoneMapTableINDEX, 1, 0, 0, "Invalid"},

{{13,Fsipv6ScopeZoneIndexC}, GetNextIndexFsipv6IfScopeZoneMapTable, Fsipv6ScopeZoneIndexCGet, Fsipv6ScopeZoneIndexCSet, Fsipv6ScopeZoneIndexCTest, Fsipv6IfScopeZoneMapTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Fsipv6IfScopeZoneMapTableINDEX, 1, 0, 0, "Invalid"},

{{13,Fsipv6ScopeZoneIndexD}, GetNextIndexFsipv6IfScopeZoneMapTable, Fsipv6ScopeZoneIndexDGet, Fsipv6ScopeZoneIndexDSet, Fsipv6ScopeZoneIndexDTest, Fsipv6IfScopeZoneMapTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Fsipv6IfScopeZoneMapTableINDEX, 1, 0, 0, "Invalid"},

{{13,Fsipv6ScopeZoneIndexE}, GetNextIndexFsipv6IfScopeZoneMapTable, Fsipv6ScopeZoneIndexEGet, Fsipv6ScopeZoneIndexESet, Fsipv6ScopeZoneIndexETest, Fsipv6IfScopeZoneMapTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Fsipv6IfScopeZoneMapTableINDEX, 1, 0, 0, "Invalid"},

{{13,Fsipv6IfScopeZoneCreationStatus}, GetNextIndexFsipv6IfScopeZoneMapTable, Fsipv6IfScopeZoneCreationStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Fsipv6IfScopeZoneMapTableINDEX, 1, 0, 0, "0"},

{{13,Fsipv6IfScopeZoneRowStatus}, GetNextIndexFsipv6IfScopeZoneMapTable, Fsipv6IfScopeZoneRowStatusGet, Fsipv6IfScopeZoneRowStatusSet, Fsipv6IfScopeZoneRowStatusTest, Fsipv6IfScopeZoneMapTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsipv6IfScopeZoneMapTableINDEX, 1, 0, 1, NULL},

{{13,Fsipv6ScopeZoneName}, GetNextIndexFsipv6ScopeZoneTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, Fsipv6ScopeZoneTableINDEX, 1, 0, 0, NULL},

{{13,Fsipv6ScopeZoneIndex}, GetNextIndexFsipv6ScopeZoneTable, Fsipv6ScopeZoneIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Fsipv6ScopeZoneTableINDEX, 1, 0, 0, NULL},

{{13,Fsipv6ScopeZoneCreationStatus}, GetNextIndexFsipv6ScopeZoneTable, Fsipv6ScopeZoneCreationStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Fsipv6ScopeZoneTableINDEX, 1, 0, 0, NULL},

{{13,Fsipv6ScopeZoneInterfaceList}, GetNextIndexFsipv6ScopeZoneTable, Fsipv6ScopeZoneInterfaceListGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Fsipv6ScopeZoneTableINDEX, 1, 0, 0, NULL},

{{13,Fsipv6IsDefaultScopeZone}, GetNextIndexFsipv6ScopeZoneTable, Fsipv6IsDefaultScopeZoneGet, Fsipv6IsDefaultScopeZoneSet, Fsipv6IsDefaultScopeZoneTest, Fsipv6ScopeZoneTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsipv6ScopeZoneTableINDEX, 1, 0, 0, "2"},

{{13,Fsipv6SENDSentPktType}, GetNextIndexFsipv6SENDSentPktStatsTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Fsipv6SENDSentPktStatsTableINDEX, 2, 0, 0, NULL},

{{13,Fsipv6SENDSentCgaOptPkts}, GetNextIndexFsipv6SENDSentPktStatsTable, Fsipv6SENDSentCgaOptPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsipv6SENDSentPktStatsTableINDEX, 2, 0, 0, NULL},

{{13,Fsipv6SENDSentCertOptPkts}, GetNextIndexFsipv6SENDSentPktStatsTable, Fsipv6SENDSentCertOptPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsipv6SENDSentPktStatsTableINDEX, 2, 0, 0, NULL},

{{13,Fsipv6SENDSentMtuOptPkts}, GetNextIndexFsipv6SENDSentPktStatsTable, Fsipv6SENDSentMtuOptPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsipv6SENDSentPktStatsTableINDEX, 2, 0, 0, NULL},

{{13,Fsipv6SENDSentNonceOptPkts}, GetNextIndexFsipv6SENDSentPktStatsTable, Fsipv6SENDSentNonceOptPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsipv6SENDSentPktStatsTableINDEX, 2, 0, 0, NULL},

{{13,Fsipv6SENDSentPrefixOptPkts}, GetNextIndexFsipv6SENDSentPktStatsTable, Fsipv6SENDSentPrefixOptPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsipv6SENDSentPktStatsTableINDEX, 2, 0, 0, NULL},

{{13,Fsipv6SENDSentRedirHdrPkts}, GetNextIndexFsipv6SENDSentPktStatsTable, Fsipv6SENDSentRedirHdrPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsipv6SENDSentPktStatsTableINDEX, 2, 0, 0, NULL},

{{13,Fsipv6SENDSentRsaOptPkts}, GetNextIndexFsipv6SENDSentPktStatsTable, Fsipv6SENDSentRsaOptPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsipv6SENDSentPktStatsTableINDEX, 2, 0, 0, NULL},

{{13,Fsipv6SENDSentSrcLinkAddrPkts}, GetNextIndexFsipv6SENDSentPktStatsTable, Fsipv6SENDSentSrcLinkAddrPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsipv6SENDSentPktStatsTableINDEX, 2, 0, 0, NULL},

{{13,Fsipv6SENDSentTgtLinkAddrPkts}, GetNextIndexFsipv6SENDSentPktStatsTable, Fsipv6SENDSentTgtLinkAddrPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsipv6SENDSentPktStatsTableINDEX, 2, 0, 0, NULL},

{{13,Fsipv6SENDSentTaOptPkts}, GetNextIndexFsipv6SENDSentPktStatsTable, Fsipv6SENDSentTaOptPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsipv6SENDSentPktStatsTableINDEX, 2, 0, 0, NULL},

{{13,Fsipv6SENDSentTimeStampOptPkts}, GetNextIndexFsipv6SENDSentPktStatsTable, Fsipv6SENDSentTimeStampOptPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsipv6SENDSentPktStatsTableINDEX, 2, 0, 0, NULL},

{{13,Fsipv6SENDRcvPktType}, GetNextIndexFsipv6SENDRcvPktStatsTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Fsipv6SENDRcvPktStatsTableINDEX, 2, 0, 0, NULL},

{{13,Fsipv6SENDRcvCgaOptPkts}, GetNextIndexFsipv6SENDRcvPktStatsTable, Fsipv6SENDRcvCgaOptPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsipv6SENDRcvPktStatsTableINDEX, 2, 0, 0, NULL},

{{13,Fsipv6SENDRcvCertOptPkts}, GetNextIndexFsipv6SENDRcvPktStatsTable, Fsipv6SENDRcvCertOptPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsipv6SENDRcvPktStatsTableINDEX, 2, 0, 0, NULL},

{{13,Fsipv6SENDRcvMtuOptPkts}, GetNextIndexFsipv6SENDRcvPktStatsTable, Fsipv6SENDRcvMtuOptPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsipv6SENDRcvPktStatsTableINDEX, 2, 0, 0, NULL},

{{13,Fsipv6SENDRcvNonceOptPkts}, GetNextIndexFsipv6SENDRcvPktStatsTable, Fsipv6SENDRcvNonceOptPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsipv6SENDRcvPktStatsTableINDEX, 2, 0, 0, NULL},

{{13,Fsipv6SENDRcvPrefixOptPkts}, GetNextIndexFsipv6SENDRcvPktStatsTable, Fsipv6SENDRcvPrefixOptPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsipv6SENDRcvPktStatsTableINDEX, 2, 0, 0, NULL},

{{13,Fsipv6SENDRcvRedirHdrPkts}, GetNextIndexFsipv6SENDRcvPktStatsTable, Fsipv6SENDRcvRedirHdrPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsipv6SENDRcvPktStatsTableINDEX, 2, 0, 0, NULL},

{{13,Fsipv6SENDRcvRsaOptPkts}, GetNextIndexFsipv6SENDRcvPktStatsTable, Fsipv6SENDRcvRsaOptPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsipv6SENDRcvPktStatsTableINDEX, 2, 0, 0, NULL},

{{13,Fsipv6SENDRcvSrcLinkAddrPkts}, GetNextIndexFsipv6SENDRcvPktStatsTable, Fsipv6SENDRcvSrcLinkAddrPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsipv6SENDRcvPktStatsTableINDEX, 2, 0, 0, NULL},

{{13,Fsipv6SENDRcvTgtLinkAddrPkts}, GetNextIndexFsipv6SENDRcvPktStatsTable, Fsipv6SENDRcvTgtLinkAddrPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsipv6SENDRcvPktStatsTableINDEX, 2, 0, 0, NULL},

{{13,Fsipv6SENDRcvTaOptPkts}, GetNextIndexFsipv6SENDRcvPktStatsTable, Fsipv6SENDRcvTaOptPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsipv6SENDRcvPktStatsTableINDEX, 2, 0, 0, NULL},

{{13,Fsipv6SENDRcvTimeStampOptPkts}, GetNextIndexFsipv6SENDRcvPktStatsTable, Fsipv6SENDRcvTimeStampOptPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsipv6SENDRcvPktStatsTableINDEX, 2, 0, 0, NULL},

{{13,Fsipv6SENDDrpPktType}, GetNextIndexFsipv6SENDDrpPktStatsTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Fsipv6SENDDrpPktStatsTableINDEX, 2, 0, 0, NULL},

{{13,Fsipv6SENDDrpCgaOptPkts}, GetNextIndexFsipv6SENDDrpPktStatsTable, Fsipv6SENDDrpCgaOptPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsipv6SENDDrpPktStatsTableINDEX, 2, 0, 0, NULL},

{{13,Fsipv6SENDDrpCertOptPkts}, GetNextIndexFsipv6SENDDrpPktStatsTable, Fsipv6SENDDrpCertOptPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsipv6SENDDrpPktStatsTableINDEX, 2, 0, 0, NULL},

{{13,Fsipv6SENDDrpMtuOptPkts}, GetNextIndexFsipv6SENDDrpPktStatsTable, Fsipv6SENDDrpMtuOptPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsipv6SENDDrpPktStatsTableINDEX, 2, 0, 0, NULL},

{{13,Fsipv6SENDDrpNonceOptPkts}, GetNextIndexFsipv6SENDDrpPktStatsTable, Fsipv6SENDDrpNonceOptPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsipv6SENDDrpPktStatsTableINDEX, 2, 0, 0, NULL},

{{13,Fsipv6SENDDrpPrefixOptPkts}, GetNextIndexFsipv6SENDDrpPktStatsTable, Fsipv6SENDDrpPrefixOptPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsipv6SENDDrpPktStatsTableINDEX, 2, 0, 0, NULL},

{{13,Fsipv6SENDDrpRedirHdrPkts}, GetNextIndexFsipv6SENDDrpPktStatsTable, Fsipv6SENDDrpRedirHdrPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsipv6SENDDrpPktStatsTableINDEX, 2, 0, 0, NULL},

{{13,Fsipv6SENDDrpRsaOptPkts}, GetNextIndexFsipv6SENDDrpPktStatsTable, Fsipv6SENDDrpRsaOptPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsipv6SENDDrpPktStatsTableINDEX, 2, 0, 0, NULL},

{{13,Fsipv6SENDDrpSrcLinkAddrPkts}, GetNextIndexFsipv6SENDDrpPktStatsTable, Fsipv6SENDDrpSrcLinkAddrPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsipv6SENDDrpPktStatsTableINDEX, 2, 0, 0, NULL},

{{13,Fsipv6SENDDrpTgtLinkAddrPkts}, GetNextIndexFsipv6SENDDrpPktStatsTable, Fsipv6SENDDrpTgtLinkAddrPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsipv6SENDDrpPktStatsTableINDEX, 2, 0, 0, NULL},

{{13,Fsipv6SENDDrpTaOptPkts}, GetNextIndexFsipv6SENDDrpPktStatsTable, Fsipv6SENDDrpTaOptPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsipv6SENDDrpPktStatsTableINDEX, 2, 0, 0, NULL},

{{13,Fsipv6SENDDrpTimeStampOptPkts}, GetNextIndexFsipv6SENDDrpPktStatsTable, Fsipv6SENDDrpTimeStampOptPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsipv6SENDDrpPktStatsTableINDEX, 2, 0, 0, NULL},

{{13,Fsipv6RARouteIfIndex}, GetNextIndexFsipv6RARouteInfoTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Fsipv6RARouteInfoTableINDEX, 3, 0, 0, NULL},

{{13,Fsipv6RARoutePrefix}, GetNextIndexFsipv6RARouteInfoTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, Fsipv6RARouteInfoTableINDEX, 3, 0, 0, NULL},

{{13,Fsipv6RARoutePrefixLen}, GetNextIndexFsipv6RARouteInfoTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Fsipv6RARouteInfoTableINDEX, 3, 0, 0, NULL},

{{13,Fsipv6RARoutePreference}, GetNextIndexFsipv6RARouteInfoTable, Fsipv6RARoutePreferenceGet, Fsipv6RARoutePreferenceSet, Fsipv6RARoutePreferenceTest, Fsipv6RARouteInfoTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsipv6RARouteInfoTableINDEX, 3, 0, 0, "2"},

{{13,Fsipv6RARouteLifetime}, GetNextIndexFsipv6RARouteInfoTable, Fsipv6RARouteLifetimeGet, Fsipv6RARouteLifetimeSet, Fsipv6RARouteLifetimeTest, Fsipv6RARouteInfoTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Fsipv6RARouteInfoTableINDEX, 3, 0, 0, "2147483647"},

{{13,Fsipv6RARouteRowStatus}, GetNextIndexFsipv6RARouteInfoTable, Fsipv6RARouteRowStatusGet, Fsipv6RARouteRowStatusSet, Fsipv6RARouteRowStatusTest, Fsipv6RARouteInfoTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsipv6RARouteInfoTableINDEX, 3, 0, 1, NULL},

{{11,FsIpv6TestRedEntryTime}, NULL, FsIpv6TestRedEntryTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsIpv6TestRedExitTime}, NULL, FsIpv6TestRedExitTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,Fsipv6IcmpInMsgs}, NULL, Fsipv6IcmpInMsgsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,Fsipv6IcmpInErrors}, NULL, Fsipv6IcmpInErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,Fsipv6IcmpInDestUnreachs}, NULL, Fsipv6IcmpInDestUnreachsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,Fsipv6IcmpInTimeExcds}, NULL, Fsipv6IcmpInTimeExcdsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,Fsipv6IcmpInParmProbs}, NULL, Fsipv6IcmpInParmProbsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,Fsipv6IcmpInPktTooBigs}, NULL, Fsipv6IcmpInPktTooBigsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,Fsipv6IcmpInEchos}, NULL, Fsipv6IcmpInEchosGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,Fsipv6IcmpInEchoReps}, NULL, Fsipv6IcmpInEchoRepsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,Fsipv6IcmpInRouterSolicits}, NULL, Fsipv6IcmpInRouterSolicitsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,Fsipv6IcmpInRouterAdvertisements}, NULL, Fsipv6IcmpInRouterAdvertisementsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,Fsipv6IcmpInNeighborSolicits}, NULL, Fsipv6IcmpInNeighborSolicitsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,Fsipv6IcmpInNeighborAdvertisements}, NULL, Fsipv6IcmpInNeighborAdvertisementsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,Fsipv6IcmpInRedirects}, NULL, Fsipv6IcmpInRedirectsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,Fsipv6IcmpInAdminProhib}, NULL, Fsipv6IcmpInAdminProhibGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,Fsipv6IcmpOutMsgs}, NULL, Fsipv6IcmpOutMsgsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,Fsipv6IcmpOutErrors}, NULL, Fsipv6IcmpOutErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,Fsipv6IcmpOutDestUnreachs}, NULL, Fsipv6IcmpOutDestUnreachsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,Fsipv6IcmpOutTimeExcds}, NULL, Fsipv6IcmpOutTimeExcdsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,Fsipv6IcmpOutParmProbs}, NULL, Fsipv6IcmpOutParmProbsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,Fsipv6IcmpOutPktTooBigs}, NULL, Fsipv6IcmpOutPktTooBigsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,Fsipv6IcmpOutEchos}, NULL, Fsipv6IcmpOutEchosGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,Fsipv6IcmpOutEchoReps}, NULL, Fsipv6IcmpOutEchoRepsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,Fsipv6IcmpOutRouterSolicits}, NULL, Fsipv6IcmpOutRouterSolicitsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,Fsipv6IcmpOutRouterAdvertisements}, NULL, Fsipv6IcmpOutRouterAdvertisementsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,Fsipv6IcmpOutNeighborSolicits}, NULL, Fsipv6IcmpOutNeighborSolicitsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,Fsipv6IcmpOutNeighborAdvertisements}, NULL, Fsipv6IcmpOutNeighborAdvertisementsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,Fsipv6IcmpOutRedirects}, NULL, Fsipv6IcmpOutRedirectsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,Fsipv6IcmpOutAdminProhib}, NULL, Fsipv6IcmpOutAdminProhibGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,Fsipv6IcmpInBadCode}, NULL, Fsipv6IcmpInBadCodeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,Fsipv6IcmpInNARouterFlagSet}, NULL, Fsipv6IcmpInNARouterFlagSetGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,Fsipv6IcmpInNASolicitedFlagSet}, NULL, Fsipv6IcmpInNASolicitedFlagSetGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,Fsipv6IcmpInNAOverrideFlagSet}, NULL, Fsipv6IcmpInNAOverrideFlagSetGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,Fsipv6IcmpOutNARouterFlagSet}, NULL, Fsipv6IcmpOutNARouterFlagSetGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,Fsipv6IcmpOutNASolicitedFlagSet}, NULL, Fsipv6IcmpOutNASolicitedFlagSetGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,Fsipv6IcmpOutNAOverrideFlagSet}, NULL, Fsipv6IcmpOutNAOverrideFlagSetGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,Fsipv6UdpInDatagrams}, NULL, Fsipv6UdpInDatagramsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,Fsipv6UdpNoPorts}, NULL, Fsipv6UdpNoPortsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,Fsipv6UdpInErrors}, NULL, Fsipv6UdpInErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,Fsipv6UdpOutDatagrams}, NULL, Fsipv6UdpOutDatagramsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

};
tMibData fsipv6Entry = { sizeof(fsipv6MibEntry) / sizeof(fsipv6MibEntry[0]), fsipv6MibEntry };

tMbDbEntry fsipv6rtMibEntry[]= {
{{12,Fsipv6RouteDest}, GetNextIndexFsipv6RouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, Fsipv6RouteTableINDEX, 4, 0, 0, NULL},

{{12,Fsipv6RoutePfxLength}, GetNextIndexFsipv6RouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Fsipv6RouteTableINDEX, 4, 0, 0, NULL},

{{12,Fsipv6RouteProtocol}, GetNextIndexFsipv6RouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Fsipv6RouteTableINDEX, 4, 0, 0, NULL},

{{12,Fsipv6RouteNextHop}, GetNextIndexFsipv6RouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, Fsipv6RouteTableINDEX, 4, 0, 0, NULL},

{{12,Fsipv6RouteIfIndex}, GetNextIndexFsipv6RouteTable, Fsipv6RouteIfIndexGet, Fsipv6RouteIfIndexSet, Fsipv6RouteIfIndexTest, Fsipv6RouteTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsipv6RouteTableINDEX, 4, 0, 0, NULL},

{{12,Fsipv6RouteMetric}, GetNextIndexFsipv6RouteTable, Fsipv6RouteMetricGet, Fsipv6RouteMetricSet, Fsipv6RouteMetricTest, Fsipv6RouteTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Fsipv6RouteTableINDEX, 4, 0, 0, "1"},

{{12,Fsipv6RouteType}, GetNextIndexFsipv6RouteTable, Fsipv6RouteTypeGet, Fsipv6RouteTypeSet, Fsipv6RouteTypeTest, Fsipv6RouteTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsipv6RouteTableINDEX, 4, 0, 0, NULL},

{{12,Fsipv6RouteTag}, GetNextIndexFsipv6RouteTable, Fsipv6RouteTagGet, Fsipv6RouteTagSet, Fsipv6RouteTagTest, Fsipv6RouteTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Fsipv6RouteTableINDEX, 4, 0, 0, NULL},

{{12,Fsipv6RouteAge}, GetNextIndexFsipv6RouteTable, Fsipv6RouteAgeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Fsipv6RouteTableINDEX, 4, 0, 0, NULL},

{{12,Fsipv6RouteAdminStatus}, GetNextIndexFsipv6RouteTable, Fsipv6RouteAdminStatusGet, Fsipv6RouteAdminStatusSet, Fsipv6RouteAdminStatusTest, Fsipv6RouteTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsipv6RouteTableINDEX, 4, 0, 1, NULL},

{{12,Fsipv6RouteAddrType}, GetNextIndexFsipv6RouteTable, Fsipv6RouteAddrTypeGet, Fsipv6RouteAddrTypeSet, Fsipv6RouteAddrTypeTest, Fsipv6RouteTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsipv6RouteTableINDEX, 4, 0, 0, "1"},

{{12,Fsipv6RouteHwStatus}, GetNextIndexFsipv6RouteTable, Fsipv6RouteHwStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Fsipv6RouteTableINDEX, 4, 0, 0, NULL},

{{12,Fsipv6RoutePreference}, GetNextIndexFsipv6RouteTable, Fsipv6RoutePreferenceGet, Fsipv6RoutePreferenceSet, Fsipv6RoutePreferenceTest, Fsipv6RouteTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Fsipv6RouteTableINDEX, 4, 0, 0, NULL},

{{12,Fsipv6Protocol}, GetNextIndexFsipv6PrefTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Fsipv6PrefTableINDEX, 1, 0, 0, NULL},

{{12,Fsipv6Preference}, GetNextIndexFsipv6PrefTable, Fsipv6PreferenceGet, Fsipv6PreferenceSet, Fsipv6PreferenceTest, Fsipv6PrefTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Fsipv6PrefTableINDEX, 1, 0, 0, NULL},
};
tMibData fsipv6rtEntry = { sizeof (fsipv6rtMibEntry) / sizeof (fsipv6rtMibEntry[0]), fsipv6rtMibEntry};

tMbDbEntry Fsipv6IfRaRDNSSTableMibEntry[]= {

{{13,Fsipv6IfRaRDNSSIndex}, GetNextIndexFsipv6IfRaRDNSSTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Fsipv6IfRaRDNSSTableINDEX, 1, 0, 0, NULL},

{{13,Fsipv6IfRadvRDNSSOpen}, GetNextIndexFsipv6IfRaRDNSSTable, Fsipv6IfRadvRDNSSOpenGet, Fsipv6IfRadvRDNSSOpenSet, Fsipv6IfRadvRDNSSOpenTest, Fsipv6IfRaRDNSSTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsipv6IfRaRDNSSTableINDEX, 1, 0, 0, "2"},

{{13,Fsipv6IfRaRDNSSPreference}, GetNextIndexFsipv6IfRaRDNSSTable, Fsipv6IfRaRDNSSPreferenceGet, Fsipv6IfRaRDNSSPreferenceSet, Fsipv6IfRaRDNSSPreferenceTest, Fsipv6IfRaRDNSSTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Fsipv6IfRaRDNSSTableINDEX, 1, 0, 0, "8"},

{{13,Fsipv6IfRaRDNSSLifetime}, GetNextIndexFsipv6IfRaRDNSSTable, Fsipv6IfRaRDNSSLifetimeGet, Fsipv6IfRaRDNSSLifetimeSet, Fsipv6IfRaRDNSSLifetimeTest, Fsipv6IfRaRDNSSTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Fsipv6IfRaRDNSSTableINDEX, 1, 0, 0, NULL},

{{13,Fsipv6IfRaRDNSSAddrOne}, GetNextIndexFsipv6IfRaRDNSSTable, Fsipv6IfRaRDNSSAddrOneGet, Fsipv6IfRaRDNSSAddrOneSet, Fsipv6IfRaRDNSSAddrOneTest, Fsipv6IfRaRDNSSTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Fsipv6IfRaRDNSSTableINDEX, 1, 0, 0, NULL},

{{13,Fsipv6IfRaRDNSSAddrTwo}, GetNextIndexFsipv6IfRaRDNSSTable, Fsipv6IfRaRDNSSAddrTwoGet, Fsipv6IfRaRDNSSAddrTwoSet, Fsipv6IfRaRDNSSAddrTwoTest, Fsipv6IfRaRDNSSTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Fsipv6IfRaRDNSSTableINDEX, 1, 0, 0, NULL},

{{13,Fsipv6IfRaRDNSSAddrThree}, GetNextIndexFsipv6IfRaRDNSSTable, Fsipv6IfRaRDNSSAddrThreeGet, Fsipv6IfRaRDNSSAddrThreeSet, Fsipv6IfRaRDNSSAddrThreeTest, Fsipv6IfRaRDNSSTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Fsipv6IfRaRDNSSTableINDEX, 1, 0, 0, NULL},

{{13,Fsipv6IfRaRDNSSRowStatus}, GetNextIndexFsipv6IfRaRDNSSTable, Fsipv6IfRaRDNSSRowStatusGet, Fsipv6IfRaRDNSSRowStatusSet, Fsipv6IfRaRDNSSRowStatusTest, Fsipv6IfRaRDNSSTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsipv6IfRaRDNSSTableINDEX, 1, 0, 1, NULL},

{{13,Fsipv6IfDomainNameOne}, GetNextIndexFsipv6IfRaRDNSSTable, Fsipv6IfDomainNameOneGet, Fsipv6IfDomainNameOneSet, Fsipv6IfDomainNameOneTest, Fsipv6IfRaRDNSSTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Fsipv6IfRaRDNSSTableINDEX, 1, 0, 0, NULL},

{{13,Fsipv6IfDomainNameTwo}, GetNextIndexFsipv6IfRaRDNSSTable, Fsipv6IfDomainNameTwoGet, Fsipv6IfDomainNameTwoSet, Fsipv6IfDomainNameTwoTest, Fsipv6IfRaRDNSSTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Fsipv6IfRaRDNSSTableINDEX, 1, 0, 0, NULL},

{{13,Fsipv6IfDomainNameThree}, GetNextIndexFsipv6IfRaRDNSSTable, Fsipv6IfDomainNameThreeGet, Fsipv6IfDomainNameThreeSet, Fsipv6IfDomainNameThreeTest, Fsipv6IfRaRDNSSTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Fsipv6IfRaRDNSSTableINDEX, 1, 0, 0, NULL},

{{13,Fsipv6IfDnsLifeTime}, GetNextIndexFsipv6IfRaRDNSSTable, Fsipv6IfDnsLifeTimeGet, Fsipv6IfDnsLifeTimeSet, Fsipv6IfDnsLifeTimeTest, Fsipv6IfRaRDNSSTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Fsipv6IfRaRDNSSTableINDEX, 1, 0, 0, NULL},

{{13,Fsipv6IfRaRDNSSAddrOneLife}, GetNextIndexFsipv6IfRaRDNSSTable, Fsipv6IfRaRDNSSAddrOneLifeGet, Fsipv6IfRaRDNSSAddrOneLifeSet, Fsipv6IfRaRDNSSAddrOneLifeTest, Fsipv6IfRaRDNSSTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Fsipv6IfRaRDNSSTableINDEX, 1, 0, 0, NULL},

{{13,Fsipv6IfRaRDNSSAddrTwoLife}, GetNextIndexFsipv6IfRaRDNSSTable, Fsipv6IfRaRDNSSAddrTwoLifeGet, Fsipv6IfRaRDNSSAddrTwoLifeSet, Fsipv6IfRaRDNSSAddrTwoLifeTest, Fsipv6IfRaRDNSSTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Fsipv6IfRaRDNSSTableINDEX, 1, 0, 0, NULL},

{{13,Fsipv6IfRaRDNSSAddrThreeLife}, GetNextIndexFsipv6IfRaRDNSSTable, Fsipv6IfRaRDNSSAddrThreeLifeGet, Fsipv6IfRaRDNSSAddrThreeLifeSet, Fsipv6IfRaRDNSSAddrThreeLifeTest, Fsipv6IfRaRDNSSTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Fsipv6IfRaRDNSSTableINDEX, 1, 0, 0, NULL},

{{13,Fsipv6IfDomainNameOneLife}, GetNextIndexFsipv6IfRaRDNSSTable, Fsipv6IfDomainNameOneLifeGet, Fsipv6IfDomainNameOneLifeSet, Fsipv6IfDomainNameOneLifeTest, Fsipv6IfRaRDNSSTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Fsipv6IfRaRDNSSTableINDEX, 1, 0, 0, NULL},

{{13,Fsipv6IfDomainNameTwoLife}, GetNextIndexFsipv6IfRaRDNSSTable, Fsipv6IfDomainNameTwoLifeGet, Fsipv6IfDomainNameTwoLifeSet, Fsipv6IfDomainNameTwoLifeTest, Fsipv6IfRaRDNSSTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Fsipv6IfRaRDNSSTableINDEX, 1, 0, 0, NULL},

{{13,Fsipv6IfDomainNameThreeLife}, GetNextIndexFsipv6IfRaRDNSSTable, Fsipv6IfDomainNameThreeLifeGet, Fsipv6IfDomainNameThreeLifeSet, Fsipv6IfDomainNameThreeLifeTest, Fsipv6IfRaRDNSSTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Fsipv6IfRaRDNSSTableINDEX, 1, 0, 0, NULL},

};
tMibData Fsipv6IfRaRDNSSTableEntry = { 18, Fsipv6IfRaRDNSSTableMibEntry };


/* fsipv6Entry is manually added to group the ip6 and ip6route mib objects */
tMibData *fsip6Entry[] = {
    NULL,
    &fsipv6Entry,
    &fsipv6rtEntry,
    &Fsipv6IfRaRDNSSTableEntry,
};

#endif /* _FSIPV6DB_H */

