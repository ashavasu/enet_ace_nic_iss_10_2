/*****************************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: ip6mcnpapi.c,v 1.3 2015/03/18 13:39:08 siva Exp $
 *
 * Description: This file contains function for invoking NP calls of IP Multicast module.
 ******************************************************************************/

#ifndef _IP6MCNPAPI_C_
#define _IP6MCNPAPI_C_

#include "nputil.h"

#ifdef PIMV6_WANTED
/***************************************************************************
 *                                                                          
 *    Function Name       : Ip6mcFsNpIpv6McInit                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv6McInit
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv6McInit
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
Ip6mcFsNpIpv6McInit ()
{
    tFsHwNp             FsHwNp;
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_ENTRY, MCAST_NP_DBG_NPAPI, 
    		  "Entering Ip6mcFsNpIpv6McInit\n ");
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP6MC_MOD,    /* Module ID */
                         FS_NP_IPV6_MC_INIT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
    	MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_FAILURE, MCAST_NP_DBG_NPAPI,
	                      "Np Ipv6 Init Failed \n ");
        return (FNP_FAILURE);
    }
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_EXIT, MCAST_NP_DBG_NPAPI, 
    		  "Exiting Ip6mcFsNpIpv6McInit\n ");
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : Ip6mcFsNpIpv6McDeInit                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv6McDeInit
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv6McDeInit
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
Ip6mcFsNpIpv6McDeInit ()
{
    tFsHwNp             FsHwNp;
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_ENTRY, MCAST_NP_DBG_NPAPI, 
    		  "Entering Ip6mcFsNpIpv6McDeInit\n ");
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP6MC_MOD,    /* Module ID */
                         FS_NP_IPV6_MC_DE_INIT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
    	MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_FAILURE, MCAST_NP_DBG_NPAPI,
	              "Np Ipv6 De Init Failed \n ");
        return (FNP_FAILURE);
    }
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_EXIT, MCAST_NP_DBG_NPAPI, 
    		  "Exiting Ip6mcFsNpIpv6McDeInit\n ");
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : Ip6mcFsNpIpv6McAddRouteEntry                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv6McAddRouteEntry
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv6McAddRouteEntry
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
Ip6mcFsNpIpv6McAddRouteEntry (UINT4 u4VrId, UINT1 *pGrpAddr, UINT4 u4GrpPrefix,
                              UINT1 *pSrcIpAddr, UINT4 u4SrcIpPrefix,
                              UINT1 u1CallerId, tMc6RtEntry rtEntry,
                              UINT2 u2NoOfDownStreamIf,
                              tMc6DownStreamIf * pDownStreamIf)
{
    tFsHwNp             FsHwNp;
    tIp6mcNpModInfo    *pIp6mcNpModInfo = NULL;
    tIp6mcNpWrFsNpIpv6McAddRouteEntry *pEntry = NULL;
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_ENTRY, MCAST_NP_DBG_NPAPI, 
    		  "Entering Ip6mcFsNpIpv6McAddRouteEntry\n ");
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP6MC_MOD,    /* Module ID */
                         FS_NP_IPV6_MC_ADD_ROUTE_ENTRY,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIp6mcNpModInfo = &(FsHwNp.Ip6mcNpModInfo);
    pEntry = &pIp6mcNpModInfo->Ip6mcNpFsNpIpv6McAddRouteEntry;

    pEntry->u4VrId = u4VrId;
    pEntry->pGrpAddr = pGrpAddr;
    pEntry->u4GrpPrefix = u4GrpPrefix;
    pEntry->pSrcIpAddr = pSrcIpAddr;
    pEntry->u4SrcIpPrefix = u4SrcIpPrefix;
    pEntry->u1CallerId = u1CallerId;
    pEntry->rtEntry = rtEntry;
    pEntry->u2NoOfDownStreamIf = u2NoOfDownStreamIf;
    pEntry->pDownStreamIf = pDownStreamIf;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        MCAST_NP_DBG3 (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_FAILURE, MCAST_NP_DBG_NPAPI,
                      "Np Ipv6 Route Entry Addition Failed" "The VrId is %u\n" 
		      "The CallerId is %u\n"
		      "The No. Of Down Stream If is %u\n",
                      (pEntry->u4VrId), (pEntry->u1CallerId), (pEntry->u2NoOfDownStreamIf));
        return (FNP_FAILURE);
    }
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_EXIT, MCAST_NP_DBG_NPAPI, 
    		  "Exiting Ip6mcFsNpIpv6McAddRouteEntry\n ");
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : Ip6mcFsNpIpv6McDelRouteEntry                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv6McDelRouteEntry
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv6McDelRouteEntry
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
Ip6mcFsNpIpv6McDelRouteEntry (UINT4 u4VrId, UINT1 *pGrpAddr, UINT4 u4GrpPrefix,
                              UINT1 *pSrcIpAddr, UINT4 u4SrcIpPrefix,
                              tMc6RtEntry rtEntry, UINT2 u2NoOfDownStreamIf,
                              tMc6DownStreamIf * pDownStreamIf)
{
    tFsHwNp             FsHwNp;
    tIp6mcNpModInfo    *pIp6mcNpModInfo = NULL;
    tIp6mcNpWrFsNpIpv6McDelRouteEntry *pEntry = NULL;
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_ENTRY, MCAST_NP_DBG_NPAPI, 
    		  "Entering Ip6mcFsNpIpv6McDelRouteEntry\n ");
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP6MC_MOD,    /* Module ID */
                         FS_NP_IPV6_MC_DEL_ROUTE_ENTRY,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIp6mcNpModInfo = &(FsHwNp.Ip6mcNpModInfo);
    pEntry = &pIp6mcNpModInfo->Ip6mcNpFsNpIpv6McDelRouteEntry;

    pEntry->u4VrId = u4VrId;
    pEntry->pGrpAddr = pGrpAddr;
    pEntry->u4GrpPrefix = u4GrpPrefix;
    pEntry->pSrcIpAddr = pSrcIpAddr;
    pEntry->u4SrcIpPrefix = u4SrcIpPrefix;
    pEntry->rtEntry = rtEntry;
    pEntry->u2NoOfDownStreamIf = u2NoOfDownStreamIf;
    pEntry->pDownStreamIf = pDownStreamIf;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
	MCAST_NP_DBG2 (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_FAILURE, MCAST_NP_DBG_NPAPI,
                      "Np Ipv6 Route Entry Deletion Failed" 
		      "The VrId is %u\n" "The No. Of Down Stream If is %u\n",
                      (pEntry->u4VrId), (pEntry->u2NoOfDownStreamIf));
        return (FNP_FAILURE);
    }
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_EXIT, MCAST_NP_DBG_NPAPI, 
    		  "Exiting Ip6mcFsNpIpv6McDelRouteEntry\n ");
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : Ip6mcFsNpIpv6McClearAllRoutes                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv6McClearAllRoutes
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv6McClearAllRoutes
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
Ip6mcFsNpIpv6McClearAllRoutes (UINT4 u4VrId)
{
    tFsHwNp             FsHwNp;
    tIp6mcNpModInfo    *pIp6mcNpModInfo = NULL;
    tIp6mcNpWrFsNpIpv6McClearAllRoutes *pEntry = NULL;
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_ENTRY, MCAST_NP_DBG_NPAPI, 
    		  "Entering Ip6mcFsNpIpv6McClearAllRoutes\n ");
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP6MC_MOD,    /* Module ID */
                         FS_NP_IPV6_MC_CLEAR_ALL_ROUTES,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIp6mcNpModInfo = &(FsHwNp.Ip6mcNpModInfo);
    pEntry = &pIp6mcNpModInfo->Ip6mcNpFsNpIpv6McClearAllRoutes;

    pEntry->u4VrId = u4VrId;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
    	MCAST_NP_DBG1 (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_FAILURE, MCAST_NP_DBG_NPAPI,
	              "Np Ipv6 ClearAllRoutes Failed \n " "The VrId is %u\n",
		      (pEntry->u4VrId));
        return (FNP_FAILURE);
    }
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_EXIT, MCAST_NP_DBG_NPAPI, 	
    		  "Exiting Ip6mcFsNpIpv6McClearAllRoutes\n ");
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : Ip6mcFsNpIpv6McUpdateOifVlanEntry                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv6McUpdateOifVlanEntry
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv6McUpdateOifVlanEntry
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
Ip6mcFsNpIpv6McUpdateOifVlanEntry (UINT4 u4VrId, UINT1 *pGrpAddr,
                                   UINT4 u4GrpPrefix, UINT1 *pSrcIpAddr,
                                   UINT4 u4SrcIpPrefix, tMc6RtEntry rtEntry,
                                   tMc6DownStreamIf downStreamIf)
{
    tFsHwNp             FsHwNp;
    tIp6mcNpModInfo    *pIp6mcNpModInfo = NULL;
    tIp6mcNpWrFsNpIpv6McUpdateOifVlanEntry *pEntry = NULL;
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_ENTRY, MCAST_NP_DBG_NPAPI, 
    		  "Entering Ip6mcFsNpIpv6McUpdateOifVlanEntry\n ");
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP6MC_MOD,    /* Module ID */
                         FS_NP_IPV6_MC_UPDATE_OIF_VLAN_ENTRY,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIp6mcNpModInfo = &(FsHwNp.Ip6mcNpModInfo);
    pEntry = &pIp6mcNpModInfo->Ip6mcNpFsNpIpv6McUpdateOifVlanEntry;

    pEntry->u4VrId = u4VrId;
    pEntry->pGrpAddr = pGrpAddr;
    pEntry->u4GrpPrefix = u4GrpPrefix;
    pEntry->pSrcIpAddr = pSrcIpAddr;
    pEntry->u4SrcIpPrefix = u4SrcIpPrefix;
    pEntry->rtEntry = rtEntry;
    pEntry->downStreamIf = downStreamIf;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        MCAST_NP_DBG1 (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_FAILURE, MCAST_NP_DBG_NPAPI,
                      "Updation of OifVlanEntry Failed" 
		      "The VrId is %u\n",
                      (pEntry->u4VrId));
        return (FNP_FAILURE);
    }
   MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_EXIT, MCAST_NP_DBG_NPAPI, 
   		 "Exiting Ip6mcFsNpIpv6McUpdateOifVlanEntry\n "); 
   return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : Ip6mcFsNpIpv6McUpdateIifVlanEntry                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv6McUpdateIifVlanEntry
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv6McUpdateIifVlanEntry
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
Ip6mcFsNpIpv6McUpdateIifVlanEntry (UINT4 u4VrId, UINT1 *pGrpAddr,
                                   UINT4 u4GrpPrefix, UINT1 *pSrcIpAddr,
                                   UINT4 u4SrcIpPrefix, tMc6RtEntry rtEntry,
                                   tMc6DownStreamIf downStreamIf)
{
    tFsHwNp             FsHwNp;
    tIp6mcNpModInfo    *pIp6mcNpModInfo = NULL;
    tIp6mcNpWrFsNpIpv6McUpdateIifVlanEntry *pEntry = NULL;
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_ENTRY, MCAST_NP_DBG_NPAPI,
    		  "Entering Ip6mcFsNpIpv6McUpdateIifVlanEntry\n ");
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP6MC_MOD,    /* Module ID */
                         FS_NP_IPV6_MC_UPDATE_IIF_VLAN_ENTRY,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIp6mcNpModInfo = &(FsHwNp.Ip6mcNpModInfo);
    pEntry = &pIp6mcNpModInfo->Ip6mcNpFsNpIpv6McUpdateIifVlanEntry;

    pEntry->u4VrId = u4VrId;
    pEntry->pGrpAddr = pGrpAddr;
    pEntry->u4GrpPrefix = u4GrpPrefix;
    pEntry->pSrcIpAddr = pSrcIpAddr;
    pEntry->u4SrcIpPrefix = u4SrcIpPrefix;
    pEntry->rtEntry = rtEntry;
    pEntry->downStreamIf = downStreamIf;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
	MCAST_NP_DBG1 (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_FAILURE, MCAST_NP_DBG_NPAPI,
                      "Updation of IifVlanEntry Failed" 
		      "The VrId is %u\n",
                      (pEntry->u4VrId));
        return (FNP_FAILURE);
    }
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_EXIT, MCAST_NP_DBG_NPAPI, 
    		  "Exiting Ip6mcFsNpIpv6McUpdateIifVlanEntry\n ");
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : Ip6mcFsNpIpv6McGetHitStatus                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv6McGetHitStatus
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv6McGetHitStatus
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
Ip6mcFsNpIpv6McGetHitStatus (UINT1 *pSrcIpAddr, UINT1 *pGrpAddr, UINT4 u4Iif,
                             UINT2 u2VlanId, UINT4 *pu4HitStatus)
{
    tFsHwNp             FsHwNp;
    tIp6mcNpModInfo    *pIp6mcNpModInfo = NULL;
    tIp6mcNpWrFsNpIpv6McGetHitStatus *pEntry = NULL;
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_ENTRY, MCAST_NP_DBG_NPAPI, 
    		  "Entering Ip6mcFsNpIpv6McGetHitStatus\n ");
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP6MC_MOD,    /* Module ID */
                         FS_NP_IPV6_MC_GET_HIT_STATUS,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIp6mcNpModInfo = &(FsHwNp.Ip6mcNpModInfo);
    pEntry = &pIp6mcNpModInfo->Ip6mcNpFsNpIpv6McGetHitStatus;

    pEntry->pSrcIpAddr = pSrcIpAddr;
    pEntry->pGrpAddr = pGrpAddr;
    pEntry->u4Iif = u4Iif;
    pEntry->u2VlanId = u2VlanId;
    pEntry->pu4HitStatus = pu4HitStatus;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
	MCAST_NP_DBG2 (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_FAILURE, MCAST_NP_DBG_NPAPI,
                      "Np Mbsm Hit Status Failed" 
		      "The Iif is %u\n" "The VlanId is %u\n",
		      (pEntry->u4Iif), (pEntry->u2VlanId));
        return (FNP_FAILURE);
    }
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_EXIT, MCAST_NP_DBG_NPAPI, 
    		  "Exiting Ip6mcFsNpIpv6McGetHitStatus\n ");
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : Ip6mcFsNpIpv6McAddCpuPort                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv6McAddCpuPort
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv6McAddCpuPort
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
Ip6mcFsNpIpv6McAddCpuPort (UINT1 u1GenRtrId, UINT1 *pGrpAddr, UINT4 u4GrpPrefix,
                           UINT1 *pSrcIpAddr, UINT4 u4SrcIpPrefix,
                           tMc6RtEntry rtEntry, UINT2 u2NoOfDownStreamIf,
                           tMc6DownStreamIf * pDownStreamIf)
{
    tFsHwNp             FsHwNp;
    tIp6mcNpModInfo    *pIp6mcNpModInfo = NULL;
    tIp6mcNpWrFsNpIpv6McAddCpuPort *pEntry = NULL;
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_ENTRY, MCAST_NP_DBG_NPAPI, 
    		  "Entering Ip6mcFsNpIpv6McAddCpuPort\n ");
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP6MC_MOD,    /* Module ID */
                         FS_NP_IPV6_MC_ADD_CPU_PORT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIp6mcNpModInfo = &(FsHwNp.Ip6mcNpModInfo);
    pEntry = &pIp6mcNpModInfo->Ip6mcNpFsNpIpv6McAddCpuPort;

    pEntry->u1GenRtrId = u1GenRtrId;
    pEntry->pGrpAddr = pGrpAddr;
    pEntry->u4GrpPrefix = u4GrpPrefix;
    pEntry->pSrcIpAddr = pSrcIpAddr;
    pEntry->u4SrcIpPrefix = u4SrcIpPrefix;
    pEntry->rtEntry = rtEntry;
    pEntry->u2NoOfDownStreamIf = u2NoOfDownStreamIf;
    pEntry->pDownStreamIf = pDownStreamIf;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
	MCAST_NP_DBG2 (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_FAILURE, MCAST_NP_DBG_NPAPI,
                      "Adding CpuPort Failed" 
		      "The GenRtrId is %u\n" "The No. Of Down Stream If is %u\n",
                      (pEntry->u1GenRtrId), (pEntry->u2NoOfDownStreamIf));
        return (FNP_FAILURE);
    }
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_EXIT, MCAST_NP_DBG_NPAPI, 
    		  "Exiting Ip6mcFsNpIpv6McAddCpuPort\n ");
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : Ip6mcFsNpIpv6McDelCpuPort                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv6McDelCpuPort
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv6McDelCpuPort
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
Ip6mcFsNpIpv6McDelCpuPort (UINT1 u1GenRtrId, UINT1 *pGrpAddr, UINT4 u4GrpPrefix,
                           UINT1 *pSrcIpAddr, UINT4 u4SrcIpPrefix,
                           tMc6RtEntry rtEntry)
{
    tFsHwNp             FsHwNp;
    tIp6mcNpModInfo    *pIp6mcNpModInfo = NULL;
    tIp6mcNpWrFsNpIpv6McDelCpuPort *pEntry = NULL;
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_ENTRY, MCAST_NP_DBG_NPAPI, 
    		  "Entering Ip6mcFsNpIpv6McDelCpuPort\n ");
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP6MC_MOD,    /* Module ID */
                         FS_NP_IPV6_MC_DEL_CPU_PORT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIp6mcNpModInfo = &(FsHwNp.Ip6mcNpModInfo);
    pEntry = &pIp6mcNpModInfo->Ip6mcNpFsNpIpv6McDelCpuPort;

    pEntry->u1GenRtrId = u1GenRtrId;
    pEntry->pGrpAddr = pGrpAddr;
    pEntry->u4GrpPrefix = u4GrpPrefix;
    pEntry->pSrcIpAddr = pSrcIpAddr;
    pEntry->u4SrcIpPrefix = u4SrcIpPrefix;
    pEntry->rtEntry = rtEntry;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {	
	MCAST_NP_DBG1 (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_FAILURE, MCAST_NP_DBG_NPAPI,
                      "Deletion of CpuPort Failed" 
		      "The GenRtrId is %u\n",
                      (pEntry->u1GenRtrId));
        return (FNP_FAILURE);
    }
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_EXIT, MCAST_NP_DBG_NPAPI, 
    		  "Exiting Ip6mcFsNpIpv6McDelCpuPort\n ");
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : Ip6mcFsNpIpv6GetMRouteStats                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv6GetMRouteStats
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv6GetMRouteStats
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
Ip6mcFsNpIpv6GetMRouteStats (UINT4 u4VrId, UINT1 *pu1GrpAddr, UINT1 *pu1SrcAddr,
                             INT4 i4StatType, UINT4 *pu4RetValue)
{
    tFsHwNp             FsHwNp;
    tIp6mcNpModInfo    *pIp6mcNpModInfo = NULL;
    tIp6mcNpWrFsNpIpv6GetMRouteStats *pEntry = NULL;
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_ENTRY, MCAST_NP_DBG_NPAPI, 
    		  "Entering Ip6mcFsNpIpv6GetMRouteStats\n ");
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP6MC_MOD,    /* Module ID */
                         FS_NP_IPV6_GET_M_ROUTE_STATS,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIp6mcNpModInfo = &(FsHwNp.Ip6mcNpModInfo);
    pEntry = &pIp6mcNpModInfo->Ip6mcNpFsNpIpv6GetMRouteStats;

    pEntry->u4VrId = u4VrId;
    pEntry->pu1GrpAddr = pu1GrpAddr;
    pEntry->pu1SrcAddr = pu1SrcAddr;
    pEntry->i4StatType = i4StatType;
    pEntry->pu4RetValue = pu4RetValue;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
	MCAST_NP_DBG2 (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_FAILURE, MCAST_NP_DBG_NPAPI,
                      "Np Ipv6 Route Stats Failed" 
		      "The VrId is %u\n" "The StatType is %d\n",
                      (pEntry->u4VrId), (pEntry->i4StatType));
        return (FNP_FAILURE);
    }
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_EXIT, MCAST_NP_DBG_NPAPI, 
    		  "Exiting Ip6mcFsNpIpv6GetMRouteStats\n ");
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : Ip6mcFsNpIpv6GetMRouteHCStats                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv6GetMRouteHCStats
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv6GetMRouteHCStats
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
Ip6mcFsNpIpv6GetMRouteHCStats (UINT4 u4VrId, UINT1 *pu1GrpAddr,
                               UINT1 *pu1SrcAddr, INT4 i4StatType,
                               tSNMP_COUNTER64_TYPE * pu8RetValue)
{
    tFsHwNp             FsHwNp;
    tIp6mcNpModInfo    *pIp6mcNpModInfo = NULL;
    tIp6mcNpWrFsNpIpv6GetMRouteHCStats *pEntry = NULL;
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_ENTRY, MCAST_NP_DBG_NPAPI, 
    		  "Entering Ip6mcFsNpIpv6GetMRouteHCStats\n ");
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP6MC_MOD,    /* Module ID */
                         FS_NP_IPV6_GET_M_ROUTE_H_C_STATS,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIp6mcNpModInfo = &(FsHwNp.Ip6mcNpModInfo);
    pEntry = &pIp6mcNpModInfo->Ip6mcNpFsNpIpv6GetMRouteHCStats;

    pEntry->u4VrId = u4VrId;
    pEntry->pu1GrpAddr = pu1GrpAddr;
    pEntry->pu1SrcAddr = pu1SrcAddr;
    pEntry->i4StatType = i4StatType;
    pEntry->pu8RetValue = pu8RetValue;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
	MCAST_NP_DBG2 (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_FAILURE, MCAST_NP_DBG_NPAPI,
                      "Np Ipv6 RouteHCStats Failed" 
		      "The VrId is %u\n" "The StatType is %d\n",
                      (pEntry->u4VrId), (pEntry->i4StatType));
        return (FNP_FAILURE);
    }
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_EXIT, MCAST_NP_DBG_NPAPI, 
    		  "Exiting Ip6mcFsNpIpv6GetMRouteHCStats\n ");
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : Ip6mcFsNpIpv6GetMNextHopStats                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv6GetMNextHopStats
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv6GetMNextHopStats
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
Ip6mcFsNpIpv6GetMNextHopStats (UINT4 u4VrId, UINT1 *pu1GrpAddr,
                               UINT1 *pu1SrcAddr, INT4 i4OutIfIndex,
                               INT4 i4StatType, UINT4 *pu4RetValue)
{
    tFsHwNp             FsHwNp;
    tIp6mcNpModInfo    *pIp6mcNpModInfo = NULL;
    tIp6mcNpWrFsNpIpv6GetMNextHopStats *pEntry = NULL;
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_ENTRY, MCAST_NP_DBG_NPAPI, 
    		  "Entering Ip6mcFsNpIpv6GetMNextHopStats\n ");
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP6MC_MOD,    /* Module ID */
                         FS_NP_IPV6_GET_M_NEXT_HOP_STATS,    /* Function/OpCode */
                         i4OutIfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIp6mcNpModInfo = &(FsHwNp.Ip6mcNpModInfo);
    pEntry = &pIp6mcNpModInfo->Ip6mcNpFsNpIpv6GetMNextHopStats;

    pEntry->u4VrId = u4VrId;
    pEntry->pu1GrpAddr = pu1GrpAddr;
    pEntry->pu1SrcAddr = pu1SrcAddr;
    pEntry->i4OutIfIndex = i4OutIfIndex;
    pEntry->i4StatType = i4StatType;
    pEntry->pu4RetValue = pu4RetValue;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
	MCAST_NP_DBG3 (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_FAILURE, MCAST_NP_DBG_NPAPI,
                      "Np Ipv6 MNextHopStats Failed" 
		      "The VrId is %u\n" "The OutIfIndex is %d\n" "The StatType is %d\n",
    	              (pEntry->u4VrId), (pEntry->i4OutIfIndex), 
		      (pEntry->i4StatType));
        return (FNP_FAILURE);
    }
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_EXIT, MCAST_NP_DBG_NPAPI, 
    		  "Exiting Ip6mcFsNpIpv6GetMNextHopStats\n ");
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : Ip6mcFsNpIpv6GetMIfaceStats                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv6GetMIfaceStats
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv6GetMIfaceStats
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
Ip6mcFsNpIpv6GetMIfaceStats (UINT4 u4VrId, INT4 i4IfIndex, INT4 i4StatType,
                             UINT4 *pu4RetValue)
{
    tFsHwNp             FsHwNp;
    tIp6mcNpModInfo    *pIp6mcNpModInfo = NULL;
    tIp6mcNpWrFsNpIpv6GetMIfaceStats *pEntry = NULL;
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_ENTRY, MCAST_NP_DBG_NPAPI, 
    		  "Entering Ip6mcFsNpIpv6GetMIfaceStats\n ");
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP6MC_MOD,    /* Module ID */
                         FS_NP_IPV6_GET_M_IFACE_STATS,    /* Function/OpCode */
                         i4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIp6mcNpModInfo = &(FsHwNp.Ip6mcNpModInfo);
    pEntry = &pIp6mcNpModInfo->Ip6mcNpFsNpIpv6GetMIfaceStats;

    pEntry->u4VrId = u4VrId;
    pEntry->i4IfIndex = i4IfIndex;
    pEntry->i4StatType = i4StatType;
    pEntry->pu4RetValue = pu4RetValue;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
    	MCAST_NP_DBG3 (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_FAILURE, MCAST_NP_DBG_NPAPI,
	              "Np Ipv6 Getting IfaceStats Failed \n " "The VrId is %u\n"
		      "The IfIndex is %d\n" "The StatType is %d\n",
		      (pEntry->u4VrId), (pEntry->i4IfIndex), (pEntry->i4StatType));
        return (FNP_FAILURE);
    }
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_EXIT, MCAST_NP_DBG_NPAPI, 
    		  "Exiting Ip6mcFsNpIpv6GetMIfaceStats\n ");
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : Ip6mcFsNpIpv6GetMIfaceHCStats                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv6GetMIfaceHCStats
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv6GetMIfaceHCStats
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
Ip6mcFsNpIpv6GetMIfaceHCStats (UINT4 u4VrId, INT4 i4IfIndex, INT4 i4StatType,
                               tSNMP_COUNTER64_TYPE * pu8RetValue)
{
    tFsHwNp             FsHwNp;
    tIp6mcNpModInfo    *pIp6mcNpModInfo = NULL;
    tIp6mcNpWrFsNpIpv6GetMIfaceHCStats *pEntry = NULL;
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_ENTRY, MCAST_NP_DBG_NPAPI, 
    		  "Entering Ip6mcFsNpIpv6GetMIfaceHCStats\n ");
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP6MC_MOD,    /* Module ID */
                         FS_NP_IPV6_GET_M_IFACE_H_C_STATS,    /* Function/OpCode */
                         i4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIp6mcNpModInfo = &(FsHwNp.Ip6mcNpModInfo);
    pEntry = &pIp6mcNpModInfo->Ip6mcNpFsNpIpv6GetMIfaceHCStats;

    pEntry->u4VrId = u4VrId;
    pEntry->i4IfIndex = i4IfIndex;
    pEntry->i4StatType = i4StatType;
    pEntry->pu8RetValue = pu8RetValue;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
    	MCAST_NP_DBG3 (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_FAILURE, MCAST_NP_DBG_NPAPI,
	              "Np Ipv6 Getting Iface HCStats Failed \n " "The VrId is %u\n"
		      "The IfIndex is %d\n" "The StatType is %d\n",
		      (pEntry->u4VrId), (pEntry->i4IfIndex), (pEntry->i4StatType));
        return (FNP_FAILURE);
    }
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_EXIT, MCAST_NP_DBG_NPAPI, 
    		  "Exiting Ip6mcFsNpIpv6GetMIfaceHCStats\n ");
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : Ip6mcFsNpIpv6SetMIfaceTtlTreshold                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv6SetMIfaceTtlTreshold
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv6SetMIfaceTtlTreshold
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
Ip6mcFsNpIpv6SetMIfaceTtlTreshold (UINT4 u4VrId, INT4 i4IfIndex,
                                   INT4 i4TtlTreshold)
{
    tFsHwNp             FsHwNp;
    tIp6mcNpModInfo    *pIp6mcNpModInfo = NULL;
    tIp6mcNpWrFsNpIpv6SetMIfaceTtlTreshold *pEntry = NULL;
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_ENTRY, MCAST_NP_DBG_NPAPI, 
    		  "Entering Ip6mcFsNpIpv6SetMIfaceTtlTreshold\n ");
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP6MC_MOD,    /* Module ID */
                         FS_NP_IPV6_SET_M_IFACE_TTL_TRESHOLD,    /* Function/OpCode */
                         i4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIp6mcNpModInfo = &(FsHwNp.Ip6mcNpModInfo);
    pEntry = &pIp6mcNpModInfo->Ip6mcNpFsNpIpv6SetMIfaceTtlTreshold;

    pEntry->u4VrId = u4VrId;
    pEntry->i4IfIndex = i4IfIndex;
    pEntry->i4TtlTreshold = i4TtlTreshold;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
    	MCAST_NP_DBG3 (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_FAILURE, MCAST_NP_DBG_NPAPI,
	              "Np Ipv6 Getting Iface TtlTreshold Failed \n " "The VrId is %u\n"
		      "The IfIndex is %d\n" "The TtlTreshold is %d\n",
		      (pEntry->u4VrId), (pEntry->i4IfIndex), (pEntry->i4TtlTreshold));
        return (FNP_FAILURE);
    }
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_EXIT, MCAST_NP_DBG_NPAPI, 
    		  "Exiting Ip6mcFsNpIpv6SetMIfaceTtlTreshold\n ");
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : Ip6mcFsNpIpv6SetMIfaceRateLimit                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv6SetMIfaceRateLimit
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv6SetMIfaceRateLimit
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
Ip6mcFsNpIpv6SetMIfaceRateLimit (UINT4 u4VrId, INT4 i4IfIndex, INT4 i4RateLimit)
{
    tFsHwNp             FsHwNp;
    tIp6mcNpModInfo    *pIp6mcNpModInfo = NULL;
    tIp6mcNpWrFsNpIpv6SetMIfaceRateLimit *pEntry = NULL;
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_ENTRY, MCAST_NP_DBG_NPAPI, 
    		  "Entering Ip6mcFsNpIpv6SetMIfaceRateLimit\n ");
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP6MC_MOD,    /* Module ID */
                         FS_NP_IPV6_SET_M_IFACE_RATE_LIMIT,    /* Function/OpCode */
                         i4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIp6mcNpModInfo = &(FsHwNp.Ip6mcNpModInfo);
    pEntry = &pIp6mcNpModInfo->Ip6mcNpFsNpIpv6SetMIfaceRateLimit;

    pEntry->u4VrId = u4VrId;
    pEntry->i4IfIndex = i4IfIndex;
    pEntry->i4RateLimit = i4RateLimit;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
    	MCAST_NP_DBG3 (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_FAILURE, MCAST_NP_DBG_NPAPI,
	              "Np Ipv6 Getting Iface Rate Limit  Failed \n " "The VrId is %u\n"
		      "The IfIndex is %d\n" "The RateLimit is %d\n",
		      (pEntry->u4VrId), (pEntry->i4IfIndex), (pEntry->i4RateLimit));
        return (FNP_FAILURE);
    }
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_EXIT, MCAST_NP_DBG_NPAPI, 
    		  "Exiting Ip6mcFsNpIpv6SetMIfaceRateLimit\n ");
    return (FNP_SUCCESS);
}


/***************************************************************************
 *                                                                          
 *    Function Name       : Ip6mcFsPimv6NpInitHw                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsPimv6NpInitHw
 *                                                                          
 *    Input(s)            : Arguments of FsPimv6NpInitHw
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
Ip6mcFsPimv6NpInitHw ()
{
    tFsHwNp             FsHwNp;
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_ENTRY, MCAST_NP_DBG_NPAPI, 
    		  "Entering Ip6mcFsPimv6NpInitHw\n ");
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP6MC_MOD,    /* Module ID */
                         FS_PIMV6_NP_INIT_HW,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
    	MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_FAILURE, MCAST_NP_DBG_NPAPI,
	              "Np Ipv6 InitHw Failed \n ");
        return (FNP_FAILURE);
    }
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_EXIT, MCAST_NP_DBG_NPAPI, 
    		  "Exiting Ip6mcFsPimv6NpInitHw\n ");
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : Ip6mcFsPimv6NpDeInitHw                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsPimv6NpDeInitHw
 *                                                                          
 *    Input(s)            : Arguments of FsPimv6NpDeInitHw
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
Ip6mcFsPimv6NpDeInitHw ()
{
    tFsHwNp             FsHwNp;
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_ENTRY, MCAST_NP_DBG_NPAPI, 
    		  "Entering Ip6mcFsPimv6NpDeInitHw\n ");
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP6MC_MOD,    /* Module ID */
                         FS_PIMV6_NP_DE_INIT_HW,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
    	MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_FAILURE, MCAST_NP_DBG_NPAPI,
	              "Np Ipv6 DeInitHw Failed \n ");
        return (FNP_FAILURE);
    }
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_EXIT, MCAST_NP_DBG_NPAPI, "Exiting Ip6mcFsPimv6NpDeInitHw\n ");
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : Ip6mcFsNpIpv6McClearHitBit                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv6McClearHitBit
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv6McClearHitBit
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
Ip6mcFsNpIpv6McClearHitBit (UINT2 u2VlanId, UINT1 *pSrcIpAddr, UINT1 *pGrpAddr)
{
    tFsHwNp             FsHwNp;
    tIp6mcNpModInfo    *pIp6mcNpModInfo = NULL;
    tIp6mcNpWrFsNpIpv6McClearHitBit *pEntry = NULL;
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_ENTRY, MCAST_NP_DBG_NPAPI, 
    		  "Entering Ip6mcFsNpIpv6McClearHitBit\n ");
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP6MC_MOD,    /* Module ID */
                         FS_NP_IPV6_MC_CLEAR_HIT_BIT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIp6mcNpModInfo = &(FsHwNp.Ip6mcNpModInfo);
    pEntry = &pIp6mcNpModInfo->Ip6mcNpFsNpIpv6McClearHitBit;

    pEntry->u2VlanId = u2VlanId;
    pEntry->pSrcIpAddr = pSrcIpAddr;
    pEntry->pGrpAddr = pGrpAddr;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
	MCAST_NP_DBG1 (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_FAILURE, MCAST_NP_DBG_NPAPI,
                      "Np Ipv6 ClearHitBit Failed" 
		      "The VrId is %u\n",
                      (pEntry->u2VlanId));
        return (FNP_FAILURE);
    }
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_EXIT, MCAST_NP_DBG_NPAPI, 
    		  "Exiting Ip6mcFsNpIpv6McClearHitBit\n ");
    return (FNP_SUCCESS);
}
#ifdef MBSM_WANTED

/***************************************************************************
 *                                                                          
 *    Function Name       : Ip6mcFsNpIpv6MbsmMcInit                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv6MbsmMcInit
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv6MbsmMcInit
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
Ip6mcFsNpIpv6MbsmMcInit (tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tIp6mcNpModInfo     *pIp6mcNpModInfo = NULL;
    tIp6mcNpWrFsNpIpv6MbsmMcInit *pEntry = NULL;
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_ENTRY, MCAST_NP_DBG_NPAPI, 
    		  "Entering Ip6mcFsNpIpv6MbsmMcInit \n ");
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP6MC_MOD,    /* Module ID */
                         FS_NP_IPV6_MBSM_MC_INIT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIp6mcNpModInfo = &(FsHwNp.Ip6mcNpModInfo);
    pEntry = &pIp6mcNpModInfo->Ip6mcNpFsNpIpv6MbsmMcInit;

    pEntry->pSlotInfo = pSlotInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
    	MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_FAILURE, MCAST_NP_DBG_NPAPI,
	              "Np Ipv6 Mbsm McInit Failed \n ");
        return (FNP_FAILURE);
    }
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_EXIT, MCAST_NP_DBG_NPAPI, 
    	          "Exiting Ip6mcFsNpIpv6MbsmMcInit\n ");
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : Ip6mcFsNpIpv6MbsmMcAddRouteEntry                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv6MbsmMcAddRouteEntry
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv6MbsmMcAddRouteEntry
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
Ip6mcFsNpIpv6MbsmMcAddRouteEntry (UINT4 u4VrId, UINT1 *pGrpAddr, UINT4 u4GrpPrefix,
                              UINT1 *pSrcIpAddr, UINT4 u4SrcIpPrefix,
                              UINT1 u1CallerId, tMc6RtEntry rtEntry,
                              UINT2 u2NoOfDownStreamIf,
                              tMc6DownStreamIf * pDownStreamIf,
                                 tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tIp6mcNpModInfo     *pIp6mcNpModInfo = NULL;
    tIp6mcNpWrFsNpIpv6MbsmMcAddRouteEntry *pEntry = NULL;
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_ENTRY, MCAST_NP_DBG_NPAPI, 
    		  "Entering Ip6mcFsNpIpv6MbsmMcAddRouteEntry\n ");
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP6MC_MOD,    /* Module ID */
                         FS_NP_IPV6_MBSM_MC_ADD_ROUTE_ENTRY,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIp6mcNpModInfo = &(FsHwNp.Ip6mcNpModInfo);
    pEntry = &pIp6mcNpModInfo->Ip6mcNpFsNpIpv6MbsmMcAddRouteEntry;

    pEntry->u4VrId = u4VrId;
    pEntry->pGrpAddr = pGrpAddr;
    pEntry->u4GrpPrefix = u4GrpPrefix;
    pEntry->pSrcIpAddr = pSrcIpAddr;
    pEntry->u4SrcIpPrefix = u4SrcIpPrefix;
    pEntry->u1CallerId = u1CallerId;
    pEntry->rtEntry = rtEntry;
    pEntry->u2NoOfDownStreamIf = u2NoOfDownStreamIf;
    pEntry->pDownStreamIf = pDownStreamIf;
    pEntry->pSlotInfo = pSlotInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
	MCAST_NP_DBG3 (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_FAILURE, MCAST_NP_DBG_NPAPI,
                      "Np Ipv6 Mbsm Addition of RouteEntry Failed" 
		      "The No. of DownstreamIf is %u\n" "The VrId is %u\n" "The CallerId is %u\n",
                      (pEntry->u2NoOfDownStreamIf), (pEntry->u4VrId), (pEntry->u1CallerId));
        return (FNP_FAILURE);
    }
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_EXIT, MCAST_NP_DBG_NPAPI, 
    		  "Exiting Ip6mcFsNpIpv6MbsmMcAddRouteEntry\n ");
    return (FNP_SUCCESS);
}


/***************************************************************************
 *                                                                          
 *    Function Name       : Ip6mcFsPimMbsmNpInitHw                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsPimMbsmNpInitHw
 *                                                                          
 *    Input(s)            : Arguments of FsPimMbsmNpInitHw
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
Ip6mcFsPimv6MbsmNpInitHw (tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tIp6mcNpModInfo     *pIp6mcNpModInfo = NULL;
    tIp6mcNpWrFsPimv6MbsmNpInitHw *pEntry = NULL;
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_ENTRY, MCAST_NP_DBG_NPAPI, 
    		  "Entering Ip6mcFsPimv6MbsmNpInitHw\n ");
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP6MC_MOD,    /* Module ID */
                         FS_PIMV6_MBSM_NP_INIT_HW,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIp6mcNpModInfo = &(FsHwNp.Ip6mcNpModInfo);
    pEntry = &pIp6mcNpModInfo->Ip6mcNpFsPimv6MbsmNpInitHw;

    pEntry->pSlotInfo = pSlotInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
    	MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_FAILURE, MCAST_NP_DBG_NPAPI,
	              "Np Ipv6 Mbsm McInit Failed \n ");
        return (FNP_FAILURE);
    }
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_EXIT, MCAST_NP_DBG_NPAPI, 
    		  "Exiting Ip6mcFsPimv6MbsmNpInitHw\n ");
    return (FNP_SUCCESS);
}
#endif 
#endif /* PIMV6_WANTED */
#endif
