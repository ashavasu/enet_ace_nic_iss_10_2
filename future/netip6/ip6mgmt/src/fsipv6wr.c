/*******************************************************************
 **   Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 *    $Id: fsipv6wr.c,v 1.21 2015/11/13 12:05:09 siva Exp $
 *
 * ****************************************************************/

# include  "lr.h"
# include  "fssnmp.h"
# include  "fsipv6wr.h"
# include  "fsipv6db.h"
#ifndef LNXIP6_WANTED            /* FSIP */
#include "ip6inc.h"
#else
#include "lip6mgin.h"
#endif

VOID
RegisterFSIPV6 ()
{
    fsipv6OID.pu4_OidList[8] = IP6_FSIP6_TBL_OID;
    SNMPRegisterMibWithContextIdAndLock (&fsipv6OID, fsip6Entry[IP6_ONE],
                                         Ip6Lock, Ip6UnLock,
                                         Ip6SelectContext, Ip6ReleaseContext,
                                         SNMP_MSR_TGR_FALSE);
    SNMPAddSysorEntry (&fsipv6OID, (const UINT1 *) "fsipv6");
    fsipv6OID.pu4_OidList[8] = IP6_ROUTE_TBL_OID;
    SNMPRegisterMibWithContextIdAndLock (&fsipv6OID, fsip6Entry[IP6_TWO],
                                         Rtm6Lock, Rtm6UnLock,
                                         UtilRtm6SetContext,
                                         UtilRtm6ResetContext,
                                         SNMP_MSR_TGR_FALSE);
    SNMPAddSysorEntry (&fsipv6OID, (const UINT1 *) "fsipv6");
}

VOID
UnRegisterFSIPV6 ()
{
    fsipv6OID.pu4_OidList[8] = IP6_ONE;
    SNMPUnRegisterMib (&fsipv6OID, fsip6Entry[IP6_ONE]);
    SNMPDelSysorEntry (&fsipv6OID, (const UINT1 *) "fsipv6");
    fsipv6OID.pu4_OidList[8] = IP6_TWO;
    SNMPUnRegisterMib (&fsipv6OID, fsip6Entry[IP6_TWO]);
    SNMPDelSysorEntry (&fsipv6OID, (const UINT1 *) "fsipv6");
}

INT4
Fsipv6NdCacheMaxRetriesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsipv6NdCacheMaxRetries (&(pMultiData->i4_SLongValue)));
}

INT4
Fsipv6PmtuConfigStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsipv6PmtuConfigStatus (&(pMultiData->i4_SLongValue)));
}

INT4
Fsipv6PmtuTimeOutIntervalGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsipv6PmtuTimeOutInterval (&(pMultiData->u4_ULongValue)));
}

INT4
Fsipv6JumboEnableGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsipv6JumboEnable (&(pMultiData->i4_SLongValue)));
}

INT4
Fsipv6NumOfSendJumboGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsipv6NumOfSendJumbo (&(pMultiData->i4_SLongValue)));
}

INT4
Fsipv6NumOfRecvJumboGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsipv6NumOfRecvJumbo (&(pMultiData->i4_SLongValue)));
}

INT4
Fsipv6ErrJumboGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsipv6ErrJumbo (&(pMultiData->i4_SLongValue)));
}

INT4
Fsipv6GlobalDebugGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsipv6GlobalDebug (&(pMultiData->u4_ULongValue)));
}

INT4
Fsipv6MaxRouteEntriesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsipv6MaxRouteEntries (&(pMultiData->u4_ULongValue)));
}

INT4
Fsipv6MaxLogicalIfacesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsipv6MaxLogicalIfaces (&(pMultiData->u4_ULongValue)));
}

INT4
Fsipv6MaxTunnelIfacesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsipv6MaxTunnelIfaces (&(pMultiData->u4_ULongValue)));
}

INT4
Fsipv6MaxAddressesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsipv6MaxAddresses (&(pMultiData->u4_ULongValue)));
}

INT4
Fsipv6MaxFragReasmEntriesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsipv6MaxFragReasmEntries (&(pMultiData->u4_ULongValue)));
}

INT4
Fsipv6Nd6MaxCacheEntriesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsipv6Nd6MaxCacheEntries (&(pMultiData->u4_ULongValue)));
}

INT4
Fsipv6PmtuMaxDestGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsipv6PmtuMaxDest (&(pMultiData->u4_ULongValue)));
}

INT4
Fsipv6RFC5095CompatibilityGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsipv6RFC5095Compatibility (&(pMultiData->i4_SLongValue)));
}

INT4
Fsipv6RFC5942CompatibilityGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsipv6RFC5942Compatibility (&(pMultiData->i4_SLongValue)));
}

INT4
Fsipv6NdCacheMaxRetriesSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsipv6NdCacheMaxRetries (pMultiData->i4_SLongValue));
}

INT4
Fsipv6PmtuConfigStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsipv6PmtuConfigStatus (pMultiData->i4_SLongValue));
}

INT4
Fsipv6PmtuTimeOutIntervalSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsipv6PmtuTimeOutInterval (pMultiData->u4_ULongValue));
}

INT4
Fsipv6JumboEnableSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsipv6JumboEnable (pMultiData->i4_SLongValue));
}

INT4
Fsipv6GlobalDebugSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsipv6GlobalDebug (pMultiData->u4_ULongValue));
}

INT4
Fsipv6MaxRouteEntriesSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsipv6MaxRouteEntries (pMultiData->u4_ULongValue));
}

INT4
Fsipv6MaxLogicalIfacesSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsipv6MaxLogicalIfaces (pMultiData->u4_ULongValue));
}

INT4
Fsipv6MaxTunnelIfacesSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsipv6MaxTunnelIfaces (pMultiData->u4_ULongValue));
}

INT4
Fsipv6MaxAddressesSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsipv6MaxAddresses (pMultiData->u4_ULongValue));
}

INT4
Fsipv6MaxFragReasmEntriesSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsipv6MaxFragReasmEntries (pMultiData->u4_ULongValue));
}

INT4
Fsipv6Nd6MaxCacheEntriesSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsipv6Nd6MaxCacheEntries (pMultiData->u4_ULongValue));
}

INT4
Fsipv6PmtuMaxDestSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsipv6PmtuMaxDest (pMultiData->u4_ULongValue));
}

INT4
Fsipv6RFC5095CompatibilitySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsipv6RFC5095Compatibility (pMultiData->i4_SLongValue));
}

INT4
Fsipv6RFC5942CompatibilitySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsipv6RFC5942Compatibility (pMultiData->i4_SLongValue));
}

INT4
Fsipv6NdCacheMaxRetriesTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2Fsipv6NdCacheMaxRetries
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
Fsipv6PmtuConfigStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2Fsipv6PmtuConfigStatus
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
Fsipv6PmtuTimeOutIntervalTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2Fsipv6PmtuTimeOutInterval
            (pu4Error, pMultiData->u4_ULongValue));
}

INT4
Fsipv6JumboEnableTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2Fsipv6JumboEnable (pu4Error, pMultiData->i4_SLongValue));
}

INT4
Fsipv6GlobalDebugTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2Fsipv6GlobalDebug (pu4Error, pMultiData->u4_ULongValue));
}

INT4
Fsipv6MaxRouteEntriesTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2Fsipv6MaxRouteEntries
            (pu4Error, pMultiData->u4_ULongValue));
}

INT4
Fsipv6MaxLogicalIfacesTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2Fsipv6MaxLogicalIfaces
            (pu4Error, pMultiData->u4_ULongValue));
}

INT4
Fsipv6MaxTunnelIfacesTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2Fsipv6MaxTunnelIfaces
            (pu4Error, pMultiData->u4_ULongValue));
}

INT4
Fsipv6MaxAddressesTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2Fsipv6MaxAddresses (pu4Error, pMultiData->u4_ULongValue));
}

INT4
Fsipv6MaxFragReasmEntriesTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2Fsipv6MaxFragReasmEntries
            (pu4Error, pMultiData->u4_ULongValue));
}

INT4
Fsipv6Nd6MaxCacheEntriesTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2Fsipv6Nd6MaxCacheEntries
            (pu4Error, pMultiData->u4_ULongValue));
}

INT4
Fsipv6PmtuMaxDestTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2Fsipv6PmtuMaxDest (pu4Error, pMultiData->u4_ULongValue));
}

INT4
Fsipv6RFC5095CompatibilityTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2Fsipv6RFC5095Compatibility
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
Fsipv6RFC5942CompatibilityTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2Fsipv6RFC5942Compatibility
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
Fsipv6NdCacheMaxRetriesDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsipv6NdCacheMaxRetries
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
Fsipv6PmtuConfigStatusDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsipv6PmtuConfigStatus
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
Fsipv6PmtuTimeOutIntervalDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsipv6PmtuTimeOutInterval
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
Fsipv6JumboEnableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsipv6JumboEnable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
Fsipv6GlobalDebugDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsipv6GlobalDebug
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
Fsipv6MaxRouteEntriesDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsipv6MaxRouteEntries
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
Fsipv6MaxLogicalIfacesDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsipv6MaxLogicalIfaces
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
Fsipv6MaxTunnelIfacesDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsipv6MaxTunnelIfaces
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
Fsipv6MaxAddressesDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsipv6MaxAddresses
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
Fsipv6MaxFragReasmEntriesDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsipv6MaxFragReasmEntries
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
Fsipv6Nd6MaxCacheEntriesDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsipv6Nd6MaxCacheEntries
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
Fsipv6PmtuMaxDestDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsipv6PmtuMaxDest
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
Fsipv6RFC5095CompatibilityDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsipv6RFC5095Compatibility
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
Fsipv6RFC5942CompatibilityDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsipv6RFC5942Compatibility
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4 
Fsipv6SENDSecLevelDep(UINT4 *pu4Error,tSnmpIndexList *pSnmpIndexList, 
                      tSNMP_VAR_BIND *pSnmpvarbinds)
{
    return(nmhDepv2Fsipv6SENDSecLevel
           (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4 
Fsipv6SENDNbrSecLevelDep(UINT4 *pu4Error,tSnmpIndexList *pSnmpIndexList, 
                         tSNMP_VAR_BIND *pSnmpvarbinds)
{
    return(nmhDepv2Fsipv6SENDNbrSecLevel
           (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4 
Fsipv6SENDAuthTypeDep(UINT4 *pu4Error,tSnmpIndexList *pSnmpIndexList, 
                      tSNMP_VAR_BIND *pSnmpvarbinds)
{
    return(nmhDepv2Fsipv6SENDAuthType
           (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4 
Fsipv6SENDMinBitsDep(UINT4 *pu4Error,tSnmpIndexList *pSnmpIndexList, 
                     tSNMP_VAR_BIND *pSnmpvarbinds)
{
    return(nmhDepv2Fsipv6SENDMinBits
           (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4 
Fsipv6SENDSecDADDep(UINT4 *pu4Error,tSnmpIndexList *pSnmpIndexList, 
                    tSNMP_VAR_BIND *pSnmpvarbinds)
{
    return(nmhDepv2Fsipv6SENDSecDAD
           (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4 
Fsipv6SENDPrefixChkDep(UINT4 *pu4Error,tSnmpIndexList *pSnmpIndexList, 
                       tSNMP_VAR_BIND *pSnmpvarbinds)
{
    return(nmhDepv2Fsipv6SENDPrefixChk
           (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4 
Fsipv6ECMPPRTTimerDep(UINT4 *pu4Error,tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
        return(nmhDepv2Fsipv6ECMPPRTTimer(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4 Fsipv6NdCacheTimeoutDep (UINT4 *pu4Error, tSnmpIndexList *pSnmpIndexList,
                              tSNMP_VAR_BIND *pSnmpvarbinds)
{
   return(nmhDepv2Fsipv6NdCacheTimeout(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsipv6IfTable (tSnmpIndex * pFirstMultiIndex,
                           tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsipv6IfTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsipv6IfTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Fsipv6IfTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6IfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6IfType (pMultiIndex->pIndex[0].i4_SLongValue,
                                &(pMultiData->i4_SLongValue)));

}

INT4
Fsipv6IfPortNumGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6IfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6IfPortNum (pMultiIndex->pIndex[0].i4_SLongValue,
                                   &(pMultiData->i4_SLongValue)));

}

INT4
Fsipv6IfCircuitNumGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6IfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6IfCircuitNum (pMultiIndex->pIndex[0].i4_SLongValue,
                                      &(pMultiData->i4_SLongValue)));

}

INT4
Fsipv6IfTokenGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6IfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6IfToken (pMultiIndex->pIndex[0].i4_SLongValue,
                                 pMultiData->pOctetStrValue));

}

INT4
Fsipv6IfAdminStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6IfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6IfAdminStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                       &(pMultiData->i4_SLongValue)));

}

INT4
Fsipv6IfOperStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6IfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6IfOperStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                      &(pMultiData->i4_SLongValue)));

}

INT4
Fsipv6IfRouterAdvStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6IfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6IfRouterAdvStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                           &(pMultiData->i4_SLongValue)));

}

INT4
Fsipv6IfRouterAdvFlagsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6IfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6IfRouterAdvFlags (pMultiIndex->pIndex[0].i4_SLongValue,
                                          &(pMultiData->i4_SLongValue)));

}

INT4
Fsipv6IfHopLimitGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6IfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6IfHopLimit (pMultiIndex->pIndex[0].i4_SLongValue,
                                    &(pMultiData->i4_SLongValue)));

}

INT4
Fsipv6IfDefRouterTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6IfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6IfDefRouterTime (pMultiIndex->pIndex[0].i4_SLongValue,
                                         &(pMultiData->i4_SLongValue)));

}

INT4
Fsipv6IfReachableTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6IfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6IfReachableTime (pMultiIndex->pIndex[0].i4_SLongValue,
                                         &(pMultiData->i4_SLongValue)));

}

INT4
Fsipv6IfRetransmitTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6IfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6IfRetransmitTime (pMultiIndex->pIndex[0].i4_SLongValue,
                                          &(pMultiData->i4_SLongValue)));

}

INT4
Fsipv6IfDelayProbeTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6IfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6IfDelayProbeTime (pMultiIndex->pIndex[0].i4_SLongValue,
                                          &(pMultiData->i4_SLongValue)));

}

INT4
Fsipv6IfPrefixAdvStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6IfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6IfPrefixAdvStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                           &(pMultiData->i4_SLongValue)));

}

INT4
Fsipv6IfMinRouterAdvTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6IfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6IfMinRouterAdvTime
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsipv6IfMaxRouterAdvTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6IfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6IfMaxRouterAdvTime
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsipv6IfDADRetriesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6IfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6IfDADRetries (pMultiIndex->pIndex[0].i4_SLongValue,
                                      &(pMultiData->i4_SLongValue)));

}

INT4
Fsipv6IfForwardingGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6IfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6IfForwarding (pMultiIndex->pIndex[0].i4_SLongValue,
                                      &(pMultiData->i4_SLongValue)));

}

INT4
Fsipv6IfRoutingStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6IfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6IfRoutingStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                         &(pMultiData->i4_SLongValue)));

}

INT4
Fsipv6IfIcmpErrIntervalGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6IfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6IfIcmpErrInterval (pMultiIndex->pIndex[0].i4_SLongValue,
                                           &(pMultiData->i4_SLongValue)));

}

INT4
Fsipv6IfIcmpTokenBucketSizeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6IfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6IfIcmpTokenBucketSize
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsipv6IfDestUnreachableMsgGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6IfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6IfDestUnreachableMsg
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsipv6IfUnnumAssocIPIfGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6IfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6IfUnnumAssocIPIf
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsipv6IfRedirectMsgGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6IfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6IfRedirectMsg (pMultiIndex->pIndex[0].i4_SLongValue,
                                       &(pMultiData->i4_SLongValue)));

}

INT4
Fsipv6IfAdvSrcLLAdrGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6IfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6IfAdvSrcLLAdr (pMultiIndex->pIndex[0].i4_SLongValue,
                                       &(pMultiData->i4_SLongValue)));

}

INT4
Fsipv6IfAdvIntOptGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6IfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6IfAdvIntOpt (pMultiIndex->pIndex[0].i4_SLongValue,
                                     &(pMultiData->i4_SLongValue)));

}

INT4
Fsipv6IfNDProxyAdminStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6IfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6IfNDProxyAdminStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsipv6IfNDProxyModeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6IfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6IfNDProxyMode (pMultiIndex->pIndex[0].i4_SLongValue,
                                       &(pMultiData->i4_SLongValue)));

}

INT4
Fsipv6IfNDProxyOperStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6IfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6IfNDProxyOperStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsipv6IfNDProxyUpStreamGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6IfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6IfNDProxyUpStream (pMultiIndex->pIndex[0].i4_SLongValue,
                                           &(pMultiData->i4_SLongValue)));

}

INT4
Fsipv6IfTokenSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsipv6IfToken (pMultiIndex->pIndex[0].i4_SLongValue,
                                 pMultiData->pOctetStrValue));

}

INT4
Fsipv6IfAdminStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsipv6IfAdminStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiData->i4_SLongValue));

}

INT4
Fsipv6IfRouterAdvStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsipv6IfRouterAdvStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->i4_SLongValue));

}

INT4
Fsipv6IfRouterAdvFlagsSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsipv6IfRouterAdvFlags (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
Fsipv6IfHopLimitSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsipv6IfHopLimit (pMultiIndex->pIndex[0].i4_SLongValue,
                                    pMultiData->i4_SLongValue));

}

INT4
Fsipv6IfDefRouterTimeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsipv6IfDefRouterTime (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->i4_SLongValue));

}

INT4
Fsipv6IfReachableTimeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsipv6IfReachableTime (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->i4_SLongValue));

}

INT4
Fsipv6IfRetransmitTimeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsipv6IfRetransmitTime (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
Fsipv6IfDelayProbeTimeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsipv6IfDelayProbeTime (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
Fsipv6IfPrefixAdvStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsipv6IfPrefixAdvStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->i4_SLongValue));

}

INT4
Fsipv6IfMinRouterAdvTimeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsipv6IfMinRouterAdvTime
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
Fsipv6IfMaxRouterAdvTimeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsipv6IfMaxRouterAdvTime
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
Fsipv6IfDADRetriesSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsipv6IfDADRetries (pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiData->i4_SLongValue));

}

INT4
Fsipv6IfForwardingSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsipv6IfForwarding (pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiData->i4_SLongValue));

}

INT4
Fsipv6IfIcmpErrIntervalSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsipv6IfIcmpErrInterval (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->i4_SLongValue));

}

INT4
Fsipv6IfIcmpTokenBucketSizeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsipv6IfIcmpTokenBucketSize
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
Fsipv6IfDestUnreachableMsgSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsipv6IfDestUnreachableMsg
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
Fsipv6IfUnnumAssocIPIfSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsipv6IfUnnumAssocIPIf
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
Fsipv6IfRedirectMsgSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsipv6IfRedirectMsg (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiData->i4_SLongValue));

}

INT4
Fsipv6IfAdvSrcLLAdrSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsipv6IfAdvSrcLLAdr (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiData->i4_SLongValue));

}

INT4
Fsipv6IfAdvIntOptSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsipv6IfAdvIntOpt (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiData->i4_SLongValue));

}

INT4
Fsipv6IfNDProxyAdminStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsipv6IfNDProxyAdminStatus
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
Fsipv6IfNDProxyModeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsipv6IfNDProxyMode (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiData->i4_SLongValue));

}

INT4
Fsipv6IfNDProxyUpStreamSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsipv6IfNDProxyUpStream (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->i4_SLongValue));

}

INT4
Fsipv6IfTokenTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                   tRetVal * pMultiData)
{
    return (nmhTestv2Fsipv6IfToken (pu4Error,
                                    pMultiIndex->pIndex[0].i4_SLongValue,
                                    pMultiData->pOctetStrValue));

}

INT4
Fsipv6IfAdminStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2Fsipv6IfAdminStatus (pu4Error,
                                          pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
Fsipv6IfRouterAdvStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2Fsipv6IfRouterAdvStatus (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              i4_SLongValue,
                                              pMultiData->i4_SLongValue));

}

INT4
Fsipv6IfRouterAdvFlagsTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2Fsipv6IfRouterAdvFlags (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             i4_SLongValue,
                                             pMultiData->i4_SLongValue));

}

INT4
Fsipv6IfHopLimitTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    return (nmhTestv2Fsipv6IfHopLimit (pu4Error,
                                       pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiData->i4_SLongValue));

}

INT4
Fsipv6IfDefRouterTimeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2Fsipv6IfDefRouterTime (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            pMultiData->i4_SLongValue));

}

INT4
Fsipv6IfReachableTimeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2Fsipv6IfReachableTime (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            pMultiData->i4_SLongValue));

}

INT4
Fsipv6IfRetransmitTimeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2Fsipv6IfRetransmitTime (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             i4_SLongValue,
                                             pMultiData->i4_SLongValue));

}

INT4
Fsipv6IfDelayProbeTimeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2Fsipv6IfDelayProbeTime (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             i4_SLongValue,
                                             pMultiData->i4_SLongValue));

}

INT4
Fsipv6IfPrefixAdvStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2Fsipv6IfPrefixAdvStatus (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              i4_SLongValue,
                                              pMultiData->i4_SLongValue));

}

INT4
Fsipv6IfMinRouterAdvTimeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2Fsipv6IfMinRouterAdvTime (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               i4_SLongValue,
                                               pMultiData->i4_SLongValue));

}

INT4
Fsipv6IfMaxRouterAdvTimeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2Fsipv6IfMaxRouterAdvTime (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               i4_SLongValue,
                                               pMultiData->i4_SLongValue));

}

INT4
Fsipv6IfDADRetriesTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    return (nmhTestv2Fsipv6IfDADRetries (pu4Error,
                                         pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->i4_SLongValue));

}

INT4
Fsipv6IfForwardingTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    return (nmhTestv2Fsipv6IfForwarding (pu4Error,
                                         pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->i4_SLongValue));

}

INT4
Fsipv6IfIcmpErrIntervalTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2Fsipv6IfIcmpErrInterval (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              i4_SLongValue,
                                              pMultiData->i4_SLongValue));

}

INT4
Fsipv6IfIcmpTokenBucketSizeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    return (nmhTestv2Fsipv6IfIcmpTokenBucketSize (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  i4_SLongValue,
                                                  pMultiData->i4_SLongValue));

}

INT4
Fsipv6IfDestUnreachableMsgTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    return (nmhTestv2Fsipv6IfDestUnreachableMsg (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 i4_SLongValue,
                                                 pMultiData->i4_SLongValue));

}

INT4
Fsipv6IfUnnumAssocIPIfTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2Fsipv6IfUnnumAssocIPIf (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             i4_SLongValue,
                                             pMultiData->i4_SLongValue));

}

INT4
Fsipv6IfRedirectMsgTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2Fsipv6IfRedirectMsg (pu4Error,
                                          pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
Fsipv6IfAdvSrcLLAdrTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2Fsipv6IfAdvSrcLLAdr (pu4Error,
                                          pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
Fsipv6IfAdvIntOptTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    return (nmhTestv2Fsipv6IfAdvIntOpt (pu4Error,
                                        pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiData->i4_SLongValue));

}

INT4
Fsipv6IfNDProxyAdminStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    return (nmhTestv2Fsipv6IfNDProxyAdminStatus (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 i4_SLongValue,
                                                 pMultiData->i4_SLongValue));

}

INT4
Fsipv6IfNDProxyModeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2Fsipv6IfNDProxyMode (pu4Error,
                                          pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
Fsipv6IfNDProxyUpStreamTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2Fsipv6IfNDProxyUpStream (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              i4_SLongValue,
                                              pMultiData->i4_SLongValue));

}

INT4
Fsipv6IfTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                  tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsipv6IfTable (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsipv6IfStatsTable (tSnmpIndex * pFirstMultiIndex,
                                tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsipv6IfStatsTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsipv6IfStatsTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Fsipv6IfStatsInReceivesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6IfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6IfStatsInReceives (pMultiIndex->pIndex[0].i4_SLongValue,
                                           &(pMultiData->u4_ULongValue)));

}

INT4
Fsipv6IfStatsInHdrErrorsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6IfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6IfStatsInHdrErrors
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Fsipv6IfStatsTooBigErrorsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6IfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6IfStatsTooBigErrors
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Fsipv6IfStatsInAddrErrorsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6IfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6IfStatsInAddrErrors
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Fsipv6IfStatsForwDatagramsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6IfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6IfStatsForwDatagrams
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Fsipv6IfStatsInUnknownProtosGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6IfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6IfStatsInUnknownProtos
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Fsipv6IfStatsInDiscardsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6IfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6IfStatsInDiscards (pMultiIndex->pIndex[0].i4_SLongValue,
                                           &(pMultiData->u4_ULongValue)));

}

INT4
Fsipv6IfStatsInDeliversGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6IfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6IfStatsInDelivers (pMultiIndex->pIndex[0].i4_SLongValue,
                                           &(pMultiData->u4_ULongValue)));

}

INT4
Fsipv6IfStatsOutRequestsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6IfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6IfStatsOutRequests
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Fsipv6IfStatsOutDiscardsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6IfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6IfStatsOutDiscards
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Fsipv6IfStatsOutNoRoutesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6IfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6IfStatsOutNoRoutes
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Fsipv6IfStatsReasmReqdsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6IfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6IfStatsReasmReqds (pMultiIndex->pIndex[0].i4_SLongValue,
                                           &(pMultiData->u4_ULongValue)));

}

INT4
Fsipv6IfStatsReasmOKsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6IfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6IfStatsReasmOKs (pMultiIndex->pIndex[0].i4_SLongValue,
                                         &(pMultiData->u4_ULongValue)));

}

INT4
Fsipv6IfStatsReasmFailsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6IfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6IfStatsReasmFails (pMultiIndex->pIndex[0].i4_SLongValue,
                                           &(pMultiData->u4_ULongValue)));

}

INT4
Fsipv6IfStatsFragOKsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6IfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6IfStatsFragOKs (pMultiIndex->pIndex[0].i4_SLongValue,
                                        &(pMultiData->u4_ULongValue)));

}

INT4
Fsipv6IfStatsFragFailsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6IfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6IfStatsFragFails (pMultiIndex->pIndex[0].i4_SLongValue,
                                          &(pMultiData->u4_ULongValue)));

}

INT4
Fsipv6IfStatsFragCreatesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6IfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6IfStatsFragCreates
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Fsipv6IfStatsInMcastPktsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6IfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6IfStatsInMcastPkts
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Fsipv6IfStatsOutMcastPktsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6IfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6IfStatsOutMcastPkts
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Fsipv6IfStatsInTruncatedPktsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6IfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6IfStatsInTruncatedPkts
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Fsipv6IfStatsInRouterSolsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6IfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6IfStatsInRouterSols
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Fsipv6IfStatsInRouterAdvsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6IfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6IfStatsInRouterAdvs
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Fsipv6IfStatsInNeighSolsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6IfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6IfStatsInNeighSols
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Fsipv6IfStatsInNeighAdvsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6IfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6IfStatsInNeighAdvs
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Fsipv6IfStatsInRedirectsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6IfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6IfStatsInRedirects
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Fsipv6IfStatsOutRouterSolsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6IfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6IfStatsOutRouterSols
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Fsipv6IfStatsOutRouterAdvsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6IfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6IfStatsOutRouterAdvs
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Fsipv6IfStatsOutNeighSolsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6IfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6IfStatsOutNeighSols
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Fsipv6IfStatsOutNeighAdvsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6IfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6IfStatsOutNeighAdvs
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Fsipv6IfStatsOutRedirectsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6IfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6IfStatsOutRedirects
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Fsipv6IfStatsLastRouterAdvTimeGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6IfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6IfStatsLastRouterAdvTime
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Fsipv6IfStatsNextRouterAdvTimeGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6IfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6IfStatsNextRouterAdvTime
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Fsipv6IfStatsIcmp6ErrRateLmtdGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6IfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6IfStatsIcmp6ErrRateLmtd
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));
}

INT4
GetNextIndexFsipv6PrefixTable (tSnmpIndex * pFirstMultiIndex,
                               tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsipv6PrefixTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pNextMultiIndex->pIndex[1].pOctetStrValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsipv6PrefixTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].pOctetStrValue,
             pNextMultiIndex->pIndex[1].pOctetStrValue,
             pFirstMultiIndex->pIndex[2].i4_SLongValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Fsipv6PrefixProfileIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6PrefixTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6PrefixProfileIndex
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsipv6PrefixAdminStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6PrefixTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6PrefixAdminStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiIndex->pIndex[1].
                                           pOctetStrValue,
                                           pMultiIndex->pIndex[2].i4_SLongValue,
                                           &(pMultiData->i4_SLongValue)));

}

INT4
Fsipv6SupportEmbeddedRpGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6PrefixTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6SupportEmbeddedRp (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiIndex->pIndex[1].
                                           pOctetStrValue,
                                           pMultiIndex->pIndex[2].i4_SLongValue,
                                           &(pMultiData->i4_SLongValue)));

}

INT4
Fsipv6PrefixProfileIndexSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsipv6PrefixProfileIndex
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiIndex->pIndex[2].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
Fsipv6SupportEmbeddedRpSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsipv6SupportEmbeddedRp (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiIndex->pIndex[1].
                                           pOctetStrValue,
                                           pMultiIndex->pIndex[2].i4_SLongValue,
                                           pMultiData->i4_SLongValue));

}

INT4
Fsipv6PrefixAdminStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsipv6PrefixAdminStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiIndex->pIndex[1].
                                           pOctetStrValue,
                                           pMultiIndex->pIndex[2].i4_SLongValue,
                                           pMultiData->i4_SLongValue));

}

INT4
Fsipv6PrefixProfileIndexTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2Fsipv6PrefixProfileIndex (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               i4_SLongValue,
                                               pMultiIndex->pIndex[1].
                                               pOctetStrValue,
                                               pMultiIndex->pIndex[2].
                                               i4_SLongValue,
                                               pMultiData->i4_SLongValue));

}

INT4
Fsipv6SupportEmbeddedRpTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2Fsipv6SupportEmbeddedRp (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              i4_SLongValue,
                                              pMultiIndex->pIndex[1].
                                              pOctetStrValue,
                                              pMultiIndex->pIndex[2].
                                              i4_SLongValue,
                                              pMultiData->i4_SLongValue));

}

INT4
Fsipv6PrefixAdminStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2Fsipv6PrefixAdminStatus (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              i4_SLongValue,
                                              pMultiIndex->pIndex[1].
                                              pOctetStrValue,
                                              pMultiIndex->pIndex[2].
                                              i4_SLongValue,
                                              pMultiData->i4_SLongValue));

}

INT4
Fsipv6PrefixTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsipv6PrefixTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsipv6AddrTable (tSnmpIndex * pFirstMultiIndex,
                             tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsipv6AddrTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pNextMultiIndex->pIndex[1].pOctetStrValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsipv6AddrTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].pOctetStrValue,
             pNextMultiIndex->pIndex[1].pOctetStrValue,
             pFirstMultiIndex->pIndex[2].i4_SLongValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Fsipv6AddrAdminStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6AddrTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6AddrAdminStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiIndex->pIndex[1].pOctetStrValue,
                                         pMultiIndex->pIndex[2].i4_SLongValue,
                                         &(pMultiData->i4_SLongValue)));

}

INT4
Fsipv6AddrTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6AddrTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6AddrType (pMultiIndex->pIndex[0].i4_SLongValue,
                                  pMultiIndex->pIndex[1].pOctetStrValue,
                                  pMultiIndex->pIndex[2].i4_SLongValue,
                                  &(pMultiData->i4_SLongValue)));

}

INT4
Fsipv6AddrProfIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6AddrTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6AddrProfIndex (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].pOctetStrValue,
                                       pMultiIndex->pIndex[2].i4_SLongValue,
                                       &(pMultiData->i4_SLongValue)));

}

INT4
Fsipv6AddrOperStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6AddrTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6AddrOperStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiIndex->pIndex[1].pOctetStrValue,
                                        pMultiIndex->pIndex[2].i4_SLongValue,
                                        &(pMultiData->i4_SLongValue)));

}

INT4
Fsipv6AddrScopeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6AddrTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6AddrScope (pMultiIndex->pIndex[0].i4_SLongValue,
                                   pMultiIndex->pIndex[1].pOctetStrValue,
                                   pMultiIndex->pIndex[2].i4_SLongValue,
                                   &(pMultiData->i4_SLongValue)));
}

INT4
Fsipv6AddrAdminStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsipv6AddrAdminStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiIndex->pIndex[1].pOctetStrValue,
                                         pMultiIndex->pIndex[2].i4_SLongValue,
                                         pMultiData->i4_SLongValue));

}

INT4
Fsipv6AddrTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsipv6AddrType (pMultiIndex->pIndex[0].i4_SLongValue,
                                  pMultiIndex->pIndex[1].pOctetStrValue,
                                  pMultiIndex->pIndex[2].i4_SLongValue,
                                  pMultiData->i4_SLongValue));

}

INT4
Fsipv6AddrProfIndexSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsipv6AddrProfIndex (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].pOctetStrValue,
                                       pMultiIndex->pIndex[2].i4_SLongValue,
                                       pMultiData->i4_SLongValue));

}

INT4 Fsipv6AddrSENDCgaStatusSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsipv6AddrSENDCgaStatus(
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiIndex->pIndex[1].pOctetStrValue,
        pMultiIndex->pIndex[2].i4_SLongValue,
        pMultiData->i4_SLongValue));

}

INT4 
Fsipv6AddrSENDCgaModifierSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsipv6AddrSENDCgaModifier(
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiIndex->pIndex[1].pOctetStrValue,
        pMultiIndex->pIndex[2].i4_SLongValue,
        pMultiData->pOctetStrValue));

}

INT4 
Fsipv6AddrSENDCollisionCountSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsipv6AddrSENDCollisionCount(
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiIndex->pIndex[1].pOctetStrValue,
        pMultiIndex->pIndex[2].i4_SLongValue,
        pMultiData->i4_SLongValue));

}

INT4
Fsipv6AddrAdminStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2Fsipv6AddrAdminStatus (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            pMultiIndex->pIndex[1].
                                            pOctetStrValue,
                                            pMultiIndex->pIndex[2].
                                            i4_SLongValue,
                                            pMultiData->i4_SLongValue));

}

INT4
Fsipv6AddrTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                    tRetVal * pMultiData)
{
    return (nmhTestv2Fsipv6AddrType (pu4Error,
                                     pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiIndex->pIndex[1].pOctetStrValue,
                                     pMultiIndex->pIndex[2].i4_SLongValue,
                                     pMultiData->i4_SLongValue));

}

INT4
Fsipv6AddrProfIndexTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2Fsipv6AddrProfIndex (pu4Error,
                                          pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiIndex->pIndex[1].pOctetStrValue,
                                          pMultiIndex->pIndex[2].i4_SLongValue,
                                          pMultiData->i4_SLongValue));

}

INT4 Fsipv6AddrSENDCgaStatusTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2Fsipv6AddrSENDCgaStatus(pu4Error,
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiIndex->pIndex[1].pOctetStrValue,
        pMultiIndex->pIndex[2].i4_SLongValue,
        pMultiData->i4_SLongValue));

}


INT4 
Fsipv6AddrSENDCgaModifierTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2Fsipv6AddrSENDCgaModifier(pu4Error,
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiIndex->pIndex[1].pOctetStrValue,
        pMultiIndex->pIndex[2].i4_SLongValue,
        pMultiData->pOctetStrValue));

}

INT4 
Fsipv6AddrSENDCollisionCountTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2Fsipv6AddrSENDCollisionCount(pu4Error,
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiIndex->pIndex[1].pOctetStrValue,
        pMultiIndex->pIndex[2].i4_SLongValue,
        pMultiData->i4_SLongValue));

}

INT4
Fsipv6AddrTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                    tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsipv6AddrTable (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsipv6AddrProfileTable (tSnmpIndex * pFirstMultiIndex,
                                    tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsipv6AddrProfileTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsipv6AddrProfileTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Fsipv6AddrProfileStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6AddrProfileTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6AddrProfileStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                           &(pMultiData->i4_SLongValue)));

}

INT4
Fsipv6AddrProfilePrefixAdvStatusGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6AddrProfileTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6AddrProfilePrefixAdvStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsipv6AddrProfileOnLinkAdvStatusGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6AddrProfileTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6AddrProfileOnLinkAdvStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsipv6AddrProfileAutoConfAdvStatusGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6AddrProfileTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6AddrProfileAutoConfAdvStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsipv6AddrProfilePreferredTimeGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6AddrProfileTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6AddrProfilePreferredTime
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Fsipv6AddrProfileValidTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6AddrProfileTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6AddrProfileValidTime
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Fsipv6AddrProfileValidLifeTimeFlagGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6AddrProfileTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6AddrProfileValidLifeTimeFlag
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsipv6AddrProfilePreferredLifeTimeFlagGet (tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6AddrProfileTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6AddrProfilePreferredLifeTimeFlag
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsipv6AddrProfileStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsipv6AddrProfileStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiData->i4_SLongValue));

}

INT4
Fsipv6AddrProfilePrefixAdvStatusSet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhSetFsipv6AddrProfilePrefixAdvStatus
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
Fsipv6AddrProfileOnLinkAdvStatusSet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhSetFsipv6AddrProfileOnLinkAdvStatus
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
Fsipv6AddrProfileAutoConfAdvStatusSet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhSetFsipv6AddrProfileAutoConfAdvStatus
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
Fsipv6AddrProfilePreferredTimeSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhSetFsipv6AddrProfilePreferredTime
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
Fsipv6AddrProfileValidTimeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsipv6AddrProfileValidTime
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
Fsipv6AddrProfileValidLifeTimeFlagSet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhSetFsipv6AddrProfileValidLifeTimeFlag
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
Fsipv6AddrProfilePreferredLifeTimeFlagSet (tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    return (nmhSetFsipv6AddrProfilePreferredLifeTimeFlag
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
Fsipv6AddrProfileStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2Fsipv6AddrProfileStatus (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              u4_ULongValue,
                                              pMultiData->i4_SLongValue));

}

INT4
Fsipv6AddrProfilePrefixAdvStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhTestv2Fsipv6AddrProfilePrefixAdvStatus (pu4Error,
                                                       pMultiIndex->pIndex[0].
                                                       u4_ULongValue,
                                                       pMultiData->
                                                       i4_SLongValue));

}

INT4
Fsipv6AddrProfileOnLinkAdvStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhTestv2Fsipv6AddrProfileOnLinkAdvStatus (pu4Error,
                                                       pMultiIndex->pIndex[0].
                                                       u4_ULongValue,
                                                       pMultiData->
                                                       i4_SLongValue));

}

INT4
Fsipv6AddrProfileAutoConfAdvStatusTest (UINT4 *pu4Error,
                                        tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    return (nmhTestv2Fsipv6AddrProfileAutoConfAdvStatus (pu4Error,
                                                         pMultiIndex->pIndex[0].
                                                         u4_ULongValue,
                                                         pMultiData->
                                                         i4_SLongValue));

}

INT4
Fsipv6AddrProfilePreferredTimeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhTestv2Fsipv6AddrProfilePreferredTime (pu4Error,
                                                     pMultiIndex->pIndex[0].
                                                     u4_ULongValue,
                                                     pMultiData->
                                                     u4_ULongValue));

}

INT4
Fsipv6AddrProfileValidTimeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    return (nmhTestv2Fsipv6AddrProfileValidTime (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 u4_ULongValue,
                                                 pMultiData->u4_ULongValue));

}

INT4
Fsipv6AddrProfileValidLifeTimeFlagTest (UINT4 *pu4Error,
                                        tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    return (nmhTestv2Fsipv6AddrProfileValidLifeTimeFlag (pu4Error,
                                                         pMultiIndex->pIndex[0].
                                                         u4_ULongValue,
                                                         pMultiData->
                                                         i4_SLongValue));

}

INT4
Fsipv6AddrProfilePreferredLifeTimeFlagTest (UINT4 *pu4Error,
                                            tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    return (nmhTestv2Fsipv6AddrProfilePreferredLifeTimeFlag (pu4Error,
                                                             pMultiIndex->
                                                             pIndex[0].
                                                             u4_ULongValue,
                                                             pMultiData->
                                                             i4_SLongValue));

}

INT4
Fsipv6AddrProfileTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsipv6AddrProfileTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsipv6PmtuTable (tSnmpIndex * pFirstMultiIndex,
                             tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsipv6PmtuTable
            (pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsipv6PmtuTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Fsipv6PmtuGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6PmtuTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6Pmtu (pMultiIndex->pIndex[0].pOctetStrValue,
                              &(pMultiData->i4_SLongValue)));

}

INT4
Fsipv6PmtuTimeStampGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6PmtuTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6PmtuTimeStamp (pMultiIndex->pIndex[0].pOctetStrValue,
                                       &(pMultiData->i4_SLongValue)));

}

INT4
Fsipv6PmtuAdminStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6PmtuTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6PmtuAdminStatus (pMultiIndex->pIndex[0].pOctetStrValue,
                                         &(pMultiData->i4_SLongValue)));

}

INT4
Fsipv6PmtuSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsipv6Pmtu (pMultiIndex->pIndex[0].pOctetStrValue,
                              pMultiData->i4_SLongValue));

}

INT4
Fsipv6PmtuAdminStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsipv6PmtuAdminStatus (pMultiIndex->pIndex[0].pOctetStrValue,
                                         pMultiData->i4_SLongValue));

}

INT4
Fsipv6PmtuTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2Fsipv6Pmtu (pu4Error,
                                 pMultiIndex->pIndex[0].pOctetStrValue,
                                 pMultiData->i4_SLongValue));

}

INT4
Fsipv6PmtuAdminStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2Fsipv6PmtuAdminStatus (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            pOctetStrValue,
                                            pMultiData->i4_SLongValue));

}

INT4
Fsipv6PmtuTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                    tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsipv6PmtuTable (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsipv6NdLanCacheTable (tSnmpIndex * pFirstMultiIndex,
                                   tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsipv6NdLanCacheTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pNextMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsipv6NdLanCacheTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].pOctetStrValue,
             pNextMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Fsipv6NdLanCachePhysAddrGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6NdLanCacheTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6NdLanCachePhysAddr
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
Fsipv6NdLanCacheStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6NdLanCacheTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6NdLanCacheStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiIndex->pIndex[1].pOctetStrValue,
                                          &(pMultiData->i4_SLongValue)));

}

INT4
Fsipv6NdLanCacheStateGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6NdLanCacheTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6NdLanCacheState (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiIndex->pIndex[1].pOctetStrValue,
                                         &(pMultiData->i4_SLongValue)));

}

INT4
Fsipv6NdLanCacheUseTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6NdLanCacheTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6NdLanCacheUseTime (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiIndex->pIndex[1].
                                           pOctetStrValue,
                                           &(pMultiData->u4_ULongValue)));

}

INT4
Fsipv6NdLanCachePhysAddrSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsipv6NdLanCachePhysAddr
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
Fsipv6NdLanCacheStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsipv6NdLanCacheStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiIndex->pIndex[1].pOctetStrValue,
                                          pMultiData->i4_SLongValue));

}

INT4
Fsipv6NdLanCachePhysAddrTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2Fsipv6NdLanCachePhysAddr (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               i4_SLongValue,
                                               pMultiIndex->pIndex[1].
                                               pOctetStrValue,
                                               pMultiData->pOctetStrValue));

}

INT4
Fsipv6NdLanCacheStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2Fsipv6NdLanCacheStatus (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             i4_SLongValue,
                                             pMultiIndex->pIndex[1].
                                             pOctetStrValue,
                                             pMultiData->i4_SLongValue));

}

INT4
Fsipv6NdLanCacheTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsipv6NdLanCacheTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsipv6NdWanCacheTable (tSnmpIndex * pFirstMultiIndex,
                                   tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsipv6NdWanCacheTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pNextMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsipv6NdWanCacheTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].pOctetStrValue,
             pNextMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Fsipv6NdWanCacheStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6NdWanCacheTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6NdWanCacheStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiIndex->pIndex[1].pOctetStrValue,
                                          &(pMultiData->i4_SLongValue)));

}

INT4
Fsipv6NdWanCacheStateGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6NdWanCacheTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6NdWanCacheState (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiIndex->pIndex[1].pOctetStrValue,
                                         &(pMultiData->i4_SLongValue)));

}

INT4
Fsipv6NdWanCacheUseTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6NdWanCacheTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6NdWanCacheUseTime (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiIndex->pIndex[1].
                                           pOctetStrValue,
                                           &(pMultiData->u4_ULongValue)));

}

INT4
Fsipv6NdWanCacheStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsipv6NdWanCacheStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiIndex->pIndex[1].pOctetStrValue,
                                          pMultiData->i4_SLongValue));

}

INT4
Fsipv6NdWanCacheStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2Fsipv6NdWanCacheStatus (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             i4_SLongValue,
                                             pMultiIndex->pIndex[1].
                                             pOctetStrValue,
                                             pMultiData->i4_SLongValue));

}

INT4
Fsipv6NdWanCacheTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsipv6NdWanCacheTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsipv6PingTable (tSnmpIndex * pFirstMultiIndex,
                             tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsipv6PingTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsipv6PingTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Fsipv6PingDestGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6PingTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6PingDest (pMultiIndex->pIndex[0].i4_SLongValue,
                                  pMultiData->pOctetStrValue));

}

INT4
Fsipv6PingIfIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6PingTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6PingIfIndex (pMultiIndex->pIndex[0].i4_SLongValue,
                                     &(pMultiData->i4_SLongValue)));

}

INT4
Fsipv6PingAdminStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6PingTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6PingAdminStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                         &(pMultiData->i4_SLongValue)));

}

INT4
Fsipv6PingIntervalGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6PingTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6PingInterval (pMultiIndex->pIndex[0].i4_SLongValue,
                                      &(pMultiData->i4_SLongValue)));

}

INT4
Fsipv6PingRcvTimeoutGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6PingTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6PingRcvTimeout (pMultiIndex->pIndex[0].i4_SLongValue,
                                        &(pMultiData->i4_SLongValue)));

}

INT4
Fsipv6PingTriesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6PingTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6PingTries (pMultiIndex->pIndex[0].i4_SLongValue,
                                   &(pMultiData->i4_SLongValue)));

}

INT4
Fsipv6PingSizeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6PingTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6PingSize (pMultiIndex->pIndex[0].i4_SLongValue,
                                  &(pMultiData->i4_SLongValue)));

}

INT4
Fsipv6PingSentCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6PingTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6PingSentCount (pMultiIndex->pIndex[0].i4_SLongValue,
                                       &(pMultiData->i4_SLongValue)));

}

INT4
Fsipv6PingAverageTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6PingTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6PingAverageTime (pMultiIndex->pIndex[0].i4_SLongValue,
                                         &(pMultiData->i4_SLongValue)));

}

INT4
Fsipv6PingMaxTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6PingTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6PingMaxTime (pMultiIndex->pIndex[0].i4_SLongValue,
                                     &(pMultiData->i4_SLongValue)));

}

INT4
Fsipv6PingMinTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6PingTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6PingMinTime (pMultiIndex->pIndex[0].i4_SLongValue,
                                     &(pMultiData->i4_SLongValue)));

}

INT4
Fsipv6PingOperStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6PingTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6PingOperStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                        &(pMultiData->i4_SLongValue)));

}

INT4
Fsipv6PingSuccessesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6PingTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6PingSuccesses (pMultiIndex->pIndex[0].i4_SLongValue,
                                       &(pMultiData->u4_ULongValue)));

}

INT4
Fsipv6PingPercentageLossGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6PingTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6PingPercentageLoss
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsipv6PingDataGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6PingTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6PingData (pMultiIndex->pIndex[0].i4_SLongValue,
                                  pMultiData->pOctetStrValue));

}

INT4
Fsipv6PingSrcAddrGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6PingTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6PingSrcAddr (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiData->pOctetStrValue));

}

INT4
Fsipv6PingZoneIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6PingTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6PingZoneId (pMultiIndex->pIndex[0].i4_SLongValue,
                                    pMultiData->pOctetStrValue));
}

INT4
Fsipv6PingDestAddrTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6PingTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6PingDestAddrType (pMultiIndex->pIndex[0].i4_SLongValue,
                                          &(pMultiData->i4_SLongValue)));

}

INT4 Fsipv6PingHostNameGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6PingTable(
        pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return(nmhGetFsipv6PingHostName(
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiData->pOctetStrValue));

}


INT4
Fsipv6PingDestSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsipv6PingDest (pMultiIndex->pIndex[0].i4_SLongValue,
                                  pMultiData->pOctetStrValue));

}

INT4
Fsipv6PingIfIndexSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsipv6PingIfIndex (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiData->i4_SLongValue));

}

INT4
Fsipv6PingAdminStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsipv6PingAdminStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->i4_SLongValue));

}

INT4
Fsipv6PingIntervalSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsipv6PingInterval (pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiData->i4_SLongValue));

}

INT4
Fsipv6PingRcvTimeoutSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsipv6PingRcvTimeout (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiData->i4_SLongValue));

}

INT4
Fsipv6PingTriesSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsipv6PingTries (pMultiIndex->pIndex[0].i4_SLongValue,
                                   pMultiData->i4_SLongValue));

}

INT4
Fsipv6PingSizeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsipv6PingSize (pMultiIndex->pIndex[0].i4_SLongValue,
                                  pMultiData->i4_SLongValue));

}

INT4
Fsipv6PingDataSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsipv6PingData (pMultiIndex->pIndex[0].i4_SLongValue,
                                  pMultiData->pOctetStrValue));

}

INT4
Fsipv6PingSrcAddrSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsipv6PingSrcAddr (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiData->pOctetStrValue));

}

INT4
Fsipv6PingZoneIdSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsipv6PingZoneId (pMultiIndex->pIndex[0].i4_SLongValue,
                                    pMultiData->pOctetStrValue));

}

INT4
Fsipv6PingDestAddrTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsipv6PingDestAddrType (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiData->i4_SLongValue));

}

INT4 Fsipv6PingHostNameSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsipv6PingHostName(
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiData->pOctetStrValue));

}


INT4
Fsipv6PingDestTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                    tRetVal * pMultiData)
{
    return (nmhTestv2Fsipv6PingDest (pu4Error,
                                     pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiData->pOctetStrValue));

}

INT4
Fsipv6PingIfIndexTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    return (nmhTestv2Fsipv6PingIfIndex (pu4Error,
                                        pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiData->i4_SLongValue));

}

INT4
Fsipv6PingAdminStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2Fsipv6PingAdminStatus (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            pMultiData->i4_SLongValue));

}

INT4
Fsipv6PingIntervalTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    return (nmhTestv2Fsipv6PingInterval (pu4Error,
                                         pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->i4_SLongValue));

}

INT4
Fsipv6PingRcvTimeoutTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2Fsipv6PingRcvTimeout (pu4Error,
                                           pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->i4_SLongValue));

}

INT4
Fsipv6PingTriesTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    return (nmhTestv2Fsipv6PingTries (pu4Error,
                                      pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiData->i4_SLongValue));

}

INT4
Fsipv6PingSizeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                    tRetVal * pMultiData)
{
    return (nmhTestv2Fsipv6PingSize (pu4Error,
                                     pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiData->i4_SLongValue));

}

INT4
Fsipv6PingDataTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                    tRetVal * pMultiData)
{
    return (nmhTestv2Fsipv6PingData (pu4Error,
                                     pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiData->pOctetStrValue));

}

INT4
Fsipv6PingSrcAddrTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    return (nmhTestv2Fsipv6PingSrcAddr (pu4Error,
                                        pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiData->pOctetStrValue));

}

INT4
Fsipv6PingZoneIdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    return (nmhTestv2Fsipv6PingZoneId (pu4Error,
                                       pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiData->pOctetStrValue));

}

INT4
Fsipv6PingDestAddrTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2Fsipv6PingDestAddrType (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             i4_SLongValue,
                                             pMultiData->i4_SLongValue));

}

INT4 Fsipv6PingHostNameTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2Fsipv6PingHostName(pu4Error,
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiData->pOctetStrValue));

}

INT4
Fsipv6PingTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                    tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsipv6PingTable (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsipv6NDProxyListTable (tSnmpIndex * pFirstMultiIndex,
                                    tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsipv6NDProxyListTable
            (pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsipv6NDProxyListTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Fsipv6NdProxyAdminStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6NDProxyListTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6NdProxyAdminStatus
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsipv6NdProxyAdminStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsipv6NdProxyAdminStatus
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
Fsipv6NdProxyAdminStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2Fsipv6NdProxyAdminStatus (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               pOctetStrValue,
                                               pMultiData->i4_SLongValue));

}

INT4
Fsipv6NDProxyListTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsipv6NDProxyListTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsipv6AddrSelPolicyTable (tSnmpIndex * pFirstMultiIndex,
                                      tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsipv6AddrSelPolicyTable
            (pNextMultiIndex->pIndex[0].pOctetStrValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             &(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)

        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsipv6AddrSelPolicyTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[0].pOctetStrValue,
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pFirstMultiIndex->pIndex[2].i4_SLongValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)

        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Fsipv6AddrSelPolicyScopeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6AddrSelPolicyTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6AddrSelPolicyScope
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsipv6AddrSelPolicyPrecedenceGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6AddrSelPolicyTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6AddrSelPolicyPrecedence
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));
}

INT4
Fsipv6AddrSelPolicyLabelGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6AddrSelPolicyTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6AddrSelPolicyLabel
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsipv6AddrSelPolicyAddrTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6AddrSelPolicyTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6AddrSelPolicyAddrType
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsipv6AddrSelPolicyIsPublicAddrGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6AddrSelPolicyTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6AddrSelPolicyIsPublicAddr
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsipv6AddrSelPolicyIsSelfAddrGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6AddrSelPolicyTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6AddrSelPolicyIsSelfAddr
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsipv6AddrSelPolicyReachabilityStatusGet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6AddrSelPolicyTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6AddrSelPolicyReachabilityStatus
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsipv6AddrSelPolicyConfigStatusGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6AddrSelPolicyTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6AddrSelPolicyConfigStatus
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsipv6AddrSelPolicyRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6AddrSelPolicyTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6AddrSelPolicyRowStatus
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsipv6AddrSelPolicyPrecedenceSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhSetFsipv6AddrSelPolicyPrecedence
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
Fsipv6AddrSelPolicyLabelSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsipv6AddrSelPolicyLabel
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
Fsipv6AddrSelPolicyAddrTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsipv6AddrSelPolicyAddrType
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
Fsipv6AddrSelPolicyRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsipv6AddrSelPolicyRowStatus
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
Fsipv6AddrSelPolicyPrecedenceTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhTestv2Fsipv6AddrSelPolicyPrecedence (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    pOctetStrValue,
                                                    pMultiIndex->pIndex[1].
                                                    i4_SLongValue,
                                                    pMultiIndex->pIndex[2].
                                                    i4_SLongValue,
                                                    pMultiData->i4_SLongValue));

}

INT4
Fsipv6AddrSelPolicyLabelTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2Fsipv6AddrSelPolicyLabel (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               pOctetStrValue,
                                               pMultiIndex->pIndex[1].
                                               i4_SLongValue,
                                               pMultiIndex->pIndex[2].
                                               i4_SLongValue,
                                               pMultiData->i4_SLongValue));

}

INT4
Fsipv6AddrSelPolicyAddrTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    return (nmhTestv2Fsipv6AddrSelPolicyAddrType (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  pOctetStrValue,
                                                  pMultiIndex->pIndex[1].
                                                  i4_SLongValue,
                                                  pMultiIndex->pIndex[2].
                                                  i4_SLongValue,
                                                  pMultiData->i4_SLongValue));

}

INT4
Fsipv6AddrSelPolicyRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhTestv2Fsipv6AddrSelPolicyRowStatus (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   pOctetStrValue,
                                                   pMultiIndex->pIndex[1].
                                                   i4_SLongValue,
                                                   pMultiIndex->pIndex[2].
                                                   i4_SLongValue,
                                                   pMultiData->i4_SLongValue));

}

INT4
Fsipv6AddrSelPolicyTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsipv6AddrSelPolicyTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsipv6IfScopeZoneMapTable (tSnmpIndex * pFirstMultiIndex,
                                       tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsipv6IfScopeZoneMapTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsipv6IfScopeZoneMapTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Fsipv6ScopeZoneIndexInterfaceLocalGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6IfScopeZoneMapTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6ScopeZoneIndexInterfaceLocal
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
Fsipv6ScopeZoneIndexLinkLocalGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6IfScopeZoneMapTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6ScopeZoneIndexLinkLocal
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
Fsipv6ScopeZoneIndex3Get (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6IfScopeZoneMapTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6ScopeZoneIndex3 (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->pOctetStrValue));

}

INT4
Fsipv6ScopeZoneIndexAdminLocalGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6IfScopeZoneMapTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6ScopeZoneIndexAdminLocal
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
Fsipv6ScopeZoneIndexSiteLocalGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6IfScopeZoneMapTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6ScopeZoneIndexSiteLocal
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
Fsipv6ScopeZoneIndex6Get (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6IfScopeZoneMapTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6ScopeZoneIndex6 (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->pOctetStrValue));

}

INT4
Fsipv6ScopeZoneIndex7Get (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6IfScopeZoneMapTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6ScopeZoneIndex7 (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->pOctetStrValue));

}

INT4
Fsipv6ScopeZoneIndexOrganizationLocalGet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6IfScopeZoneMapTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6ScopeZoneIndexOrganizationLocal
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
Fsipv6ScopeZoneIndex9Get (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6IfScopeZoneMapTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6ScopeZoneIndex9 (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->pOctetStrValue));

}

INT4
Fsipv6ScopeZoneIndexAGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6IfScopeZoneMapTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6ScopeZoneIndexA (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->pOctetStrValue));

}

INT4
Fsipv6ScopeZoneIndexBGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6IfScopeZoneMapTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6ScopeZoneIndexB (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->pOctetStrValue));

}

INT4
Fsipv6ScopeZoneIndexCGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6IfScopeZoneMapTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6ScopeZoneIndexC (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->pOctetStrValue));

}

INT4
Fsipv6ScopeZoneIndexDGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6IfScopeZoneMapTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6ScopeZoneIndexD (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->pOctetStrValue));

}

INT4
Fsipv6ScopeZoneIndexEGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6IfScopeZoneMapTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6ScopeZoneIndexE (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->pOctetStrValue));

}

INT4
Fsipv6IfScopeZoneCreationStatusGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6IfScopeZoneMapTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6IfScopeZoneCreationStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsipv6IfScopeZoneRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6IfScopeZoneMapTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6IfScopeZoneRowStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsipv6ScopeZoneIndexInterfaceLocalSet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhSetFsipv6ScopeZoneIndexInterfaceLocal
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
Fsipv6ScopeZoneIndexLinkLocalSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhSetFsipv6ScopeZoneIndexLinkLocal
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
Fsipv6ScopeZoneIndex3Set (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsipv6ScopeZoneIndex3 (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->pOctetStrValue));

}

INT4
Fsipv6ScopeZoneIndexAdminLocalSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhSetFsipv6ScopeZoneIndexAdminLocal
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
Fsipv6ScopeZoneIndexSiteLocalSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhSetFsipv6ScopeZoneIndexSiteLocal
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
Fsipv6ScopeZoneIndex6Set (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsipv6ScopeZoneIndex6 (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->pOctetStrValue));

}

INT4
Fsipv6ScopeZoneIndex7Set (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsipv6ScopeZoneIndex7 (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->pOctetStrValue));

}

INT4
Fsipv6ScopeZoneIndexOrganizationLocalSet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    return (nmhSetFsipv6ScopeZoneIndexOrganizationLocal
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
Fsipv6ScopeZoneIndex9Set (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsipv6ScopeZoneIndex9 (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->pOctetStrValue));

}

INT4
Fsipv6ScopeZoneIndexASet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsipv6ScopeZoneIndexA (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->pOctetStrValue));

}

INT4
Fsipv6ScopeZoneIndexBSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsipv6ScopeZoneIndexB (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->pOctetStrValue));

}

INT4
Fsipv6ScopeZoneIndexCSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsipv6ScopeZoneIndexC (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->pOctetStrValue));

}

INT4
Fsipv6ScopeZoneIndexDSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsipv6ScopeZoneIndexD (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->pOctetStrValue));

}

INT4
Fsipv6ScopeZoneIndexESet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsipv6ScopeZoneIndexE (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->pOctetStrValue));

}

INT4
Fsipv6IfScopeZoneRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsipv6IfScopeZoneRowStatus
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
Fsipv6ScopeZoneIndexInterfaceLocalTest (UINT4 *pu4Error,
                                        tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    return (nmhTestv2Fsipv6ScopeZoneIndexInterfaceLocal (pu4Error,
                                                         pMultiIndex->pIndex[0].
                                                         i4_SLongValue,
                                                         pMultiData->
                                                         pOctetStrValue));

}

INT4
Fsipv6ScopeZoneIndexLinkLocalTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhTestv2Fsipv6ScopeZoneIndexLinkLocal (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    i4_SLongValue,
                                                    pMultiData->
                                                    pOctetStrValue));

}

INT4
Fsipv6ScopeZoneIndex3Test (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2Fsipv6ScopeZoneIndex3 (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            pMultiData->pOctetStrValue));

}

INT4
Fsipv6ScopeZoneIndexAdminLocalTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhTestv2Fsipv6ScopeZoneIndexAdminLocal (pu4Error,
                                                     pMultiIndex->pIndex[0].
                                                     i4_SLongValue,
                                                     pMultiData->
                                                     pOctetStrValue));

}

INT4
Fsipv6ScopeZoneIndexSiteLocalTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhTestv2Fsipv6ScopeZoneIndexSiteLocal (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    i4_SLongValue,
                                                    pMultiData->
                                                    pOctetStrValue));

}

INT4
Fsipv6ScopeZoneIndex6Test (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2Fsipv6ScopeZoneIndex6 (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            pMultiData->pOctetStrValue));

}

INT4
Fsipv6ScopeZoneIndex7Test (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2Fsipv6ScopeZoneIndex7 (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            pMultiData->pOctetStrValue));

}

INT4
Fsipv6ScopeZoneIndexOrganizationLocalTest (UINT4 *pu4Error,
                                           tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    return (nmhTestv2Fsipv6ScopeZoneIndexOrganizationLocal (pu4Error,
                                                            pMultiIndex->
                                                            pIndex[0].
                                                            i4_SLongValue,
                                                            pMultiData->
                                                            pOctetStrValue));

}

INT4
Fsipv6ScopeZoneIndex9Test (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2Fsipv6ScopeZoneIndex9 (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            pMultiData->pOctetStrValue));

}

INT4
Fsipv6ScopeZoneIndexATest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2Fsipv6ScopeZoneIndexA (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            pMultiData->pOctetStrValue));

}

INT4
Fsipv6ScopeZoneIndexBTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2Fsipv6ScopeZoneIndexB (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            pMultiData->pOctetStrValue));

}

INT4
Fsipv6ScopeZoneIndexCTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2Fsipv6ScopeZoneIndexC (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            pMultiData->pOctetStrValue));

}

INT4
Fsipv6ScopeZoneIndexDTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2Fsipv6ScopeZoneIndexD (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            pMultiData->pOctetStrValue));

}

INT4
Fsipv6ScopeZoneIndexETest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2Fsipv6ScopeZoneIndexE (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            pMultiData->pOctetStrValue));

}

INT4
Fsipv6IfScopeZoneRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    return (nmhTestv2Fsipv6IfScopeZoneRowStatus (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 i4_SLongValue,
                                                 pMultiData->i4_SLongValue));

}

INT4
Fsipv6IfScopeZoneMapTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsipv6IfScopeZoneMapTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsipv6ScopeZoneTable (tSnmpIndex * pFirstMultiIndex,
                                  tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsipv6ScopeZoneTable
            (pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsipv6ScopeZoneTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Fsipv6ScopeZoneIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6ScopeZoneTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6ScopeZoneIndex (pMultiIndex->pIndex[0].pOctetStrValue,
                                        &(pMultiData->u4_ULongValue)));

}

INT4
Fsipv6ScopeZoneCreationStatusGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6ScopeZoneTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6ScopeZoneCreationStatus
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsipv6ScopeZoneInterfaceListGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6ScopeZoneTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6ScopeZoneInterfaceList
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
Fsipv6IsDefaultScopeZoneGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6ScopeZoneTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6IsDefaultScopeZone
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsipv6IsDefaultScopeZoneSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsipv6IsDefaultScopeZone
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
Fsipv6IsDefaultScopeZoneTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2Fsipv6IsDefaultScopeZone (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               pOctetStrValue,
                                               pMultiData->i4_SLongValue));

}

INT4
Fsipv6ScopeZoneTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsipv6ScopeZoneTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
Fsipv6IcmpInMsgsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsipv6IcmpInMsgs (&(pMultiData->u4_ULongValue)));
}

INT4
Fsipv6IcmpInErrorsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsipv6IcmpInErrors (&(pMultiData->u4_ULongValue)));
}

INT4
Fsipv6IcmpInDestUnreachsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsipv6IcmpInDestUnreachs (&(pMultiData->u4_ULongValue)));
}

INT4
Fsipv6IcmpInTimeExcdsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsipv6IcmpInTimeExcds (&(pMultiData->u4_ULongValue)));
}

INT4
Fsipv6IcmpInParmProbsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsipv6IcmpInParmProbs (&(pMultiData->u4_ULongValue)));
}

INT4
Fsipv6IcmpInPktTooBigsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsipv6IcmpInPktTooBigs (&(pMultiData->u4_ULongValue)));
}

INT4
Fsipv6IcmpInEchosGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsipv6IcmpInEchos (&(pMultiData->u4_ULongValue)));
}

INT4
Fsipv6IcmpInEchoRepsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsipv6IcmpInEchoReps (&(pMultiData->u4_ULongValue)));
}

INT4
Fsipv6IcmpInRouterSolicitsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsipv6IcmpInRouterSolicits (&(pMultiData->u4_ULongValue)));
}

INT4
Fsipv6IcmpInRouterAdvertisementsGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsipv6IcmpInRouterAdvertisements
            (&(pMultiData->u4_ULongValue)));
}

INT4
Fsipv6IcmpInNeighborSolicitsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsipv6IcmpInNeighborSolicits (&(pMultiData->u4_ULongValue)));
}

INT4
Fsipv6IcmpInNeighborAdvertisementsGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsipv6IcmpInNeighborAdvertisements
            (&(pMultiData->u4_ULongValue)));
}

INT4
Fsipv6IcmpInRedirectsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsipv6IcmpInRedirects (&(pMultiData->u4_ULongValue)));
}

INT4
Fsipv6IcmpInAdminProhibGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsipv6IcmpInAdminProhib (&(pMultiData->u4_ULongValue)));
}

INT4
Fsipv6IcmpOutMsgsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsipv6IcmpOutMsgs (&(pMultiData->u4_ULongValue)));
}

INT4
Fsipv6IcmpOutErrorsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsipv6IcmpOutErrors (&(pMultiData->u4_ULongValue)));
}

INT4
Fsipv6IcmpOutDestUnreachsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsipv6IcmpOutDestUnreachs (&(pMultiData->u4_ULongValue)));
}

INT4
Fsipv6IcmpOutTimeExcdsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsipv6IcmpOutTimeExcds (&(pMultiData->u4_ULongValue)));
}

INT4
Fsipv6IcmpOutParmProbsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsipv6IcmpOutParmProbs (&(pMultiData->u4_ULongValue)));
}

INT4
Fsipv6IcmpOutPktTooBigsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsipv6IcmpOutPktTooBigs (&(pMultiData->u4_ULongValue)));
}

INT4
Fsipv6IcmpOutEchosGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsipv6IcmpOutEchos (&(pMultiData->u4_ULongValue)));
}

INT4
Fsipv6IcmpOutEchoRepsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsipv6IcmpOutEchoReps (&(pMultiData->u4_ULongValue)));
}

INT4
Fsipv6IcmpOutRouterSolicitsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsipv6IcmpOutRouterSolicits (&(pMultiData->u4_ULongValue)));
}

INT4
Fsipv6IcmpOutRouterAdvertisementsGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsipv6IcmpOutRouterAdvertisements
            (&(pMultiData->u4_ULongValue)));
}

INT4
Fsipv6IcmpOutNeighborSolicitsGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsipv6IcmpOutNeighborSolicits (&(pMultiData->u4_ULongValue)));
}

INT4
Fsipv6IcmpOutNeighborAdvertisementsGet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsipv6IcmpOutNeighborAdvertisements
            (&(pMultiData->u4_ULongValue)));
}

INT4
Fsipv6IcmpOutRedirectsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsipv6IcmpOutRedirects (&(pMultiData->u4_ULongValue)));
}

INT4
Fsipv6IcmpOutAdminProhibGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsipv6IcmpOutAdminProhib (&(pMultiData->u4_ULongValue)));
}

INT4
Fsipv6IcmpInBadCodeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsipv6IcmpInBadCode (&(pMultiData->u4_ULongValue)));
}

INT4
Fsipv6IcmpInNARouterFlagSetGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsipv6IcmpInNARouterFlagSet (&(pMultiData->u4_ULongValue)));
}

INT4
Fsipv6IcmpInNASolicitedFlagSetGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsipv6IcmpInNASolicitedFlagSet
            (&(pMultiData->u4_ULongValue)));
}

INT4
Fsipv6IcmpInNAOverrideFlagSetGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsipv6IcmpInNAOverrideFlagSet (&(pMultiData->u4_ULongValue)));
}

INT4
Fsipv6IcmpOutNARouterFlagSetGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsipv6IcmpOutNARouterFlagSet (&(pMultiData->u4_ULongValue)));
}

INT4
Fsipv6IcmpOutNASolicitedFlagSetGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsipv6IcmpOutNASolicitedFlagSet
            (&(pMultiData->u4_ULongValue)));
}

INT4
Fsipv6IcmpOutNAOverrideFlagSetGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsipv6IcmpOutNAOverrideFlagSet
            (&(pMultiData->u4_ULongValue)));
}

INT4
Fsipv6UdpInDatagramsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsipv6UdpInDatagrams (&(pMultiData->u4_ULongValue)));
}

INT4
Fsipv6UdpNoPortsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsipv6UdpNoPorts (&(pMultiData->u4_ULongValue)));
}

INT4
Fsipv6UdpInErrorsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsipv6UdpInErrors (&(pMultiData->u4_ULongValue)));
}

INT4
Fsipv6UdpOutDatagramsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsipv6UdpOutDatagrams (&(pMultiData->u4_ULongValue)));
}

INT4
GetNextIndexFsipv6RouteTable (tSnmpIndex * pFirstMultiIndex,
                              tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsipv6RouteTable
            (pNextMultiIndex->pIndex[0].pOctetStrValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             &(pNextMultiIndex->pIndex[2].i4_SLongValue),
             pNextMultiIndex->pIndex[3].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsipv6RouteTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[0].pOctetStrValue,
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pFirstMultiIndex->pIndex[2].i4_SLongValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue),
             pFirstMultiIndex->pIndex[3].pOctetStrValue,
             pNextMultiIndex->pIndex[3].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Fsipv6RouteIfIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6RouteTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6RouteIfIndex (pMultiIndex->pIndex[0].pOctetStrValue,
                                      pMultiIndex->pIndex[1].i4_SLongValue,
                                      pMultiIndex->pIndex[2].i4_SLongValue,
                                      pMultiIndex->pIndex[3].pOctetStrValue,
                                      &(pMultiData->i4_SLongValue)));

}

INT4
Fsipv6RouteMetricGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6RouteTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6RouteMetric (pMultiIndex->pIndex[0].pOctetStrValue,
                                     pMultiIndex->pIndex[1].i4_SLongValue,
                                     pMultiIndex->pIndex[2].i4_SLongValue,
                                     pMultiIndex->pIndex[3].pOctetStrValue,
                                     &(pMultiData->u4_ULongValue)));

}

INT4
Fsipv6RouteTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6RouteTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6RouteType (pMultiIndex->pIndex[0].pOctetStrValue,
                                   pMultiIndex->pIndex[1].i4_SLongValue,
                                   pMultiIndex->pIndex[2].i4_SLongValue,
                                   pMultiIndex->pIndex[3].pOctetStrValue,
                                   &(pMultiData->i4_SLongValue)));

}

INT4
Fsipv6RouteTagGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6RouteTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6RouteTag (pMultiIndex->pIndex[0].pOctetStrValue,
                                  pMultiIndex->pIndex[1].i4_SLongValue,
                                  pMultiIndex->pIndex[2].i4_SLongValue,
                                  pMultiIndex->pIndex[3].pOctetStrValue,
                                  &(pMultiData->u4_ULongValue)));

}

INT4
Fsipv6RouteAgeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6RouteTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6RouteAge (pMultiIndex->pIndex[0].pOctetStrValue,
                                  pMultiIndex->pIndex[1].i4_SLongValue,
                                  pMultiIndex->pIndex[2].i4_SLongValue,
                                  pMultiIndex->pIndex[3].pOctetStrValue,
                                  &(pMultiData->i4_SLongValue)));

}

INT4
Fsipv6RouteAdminStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6RouteTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6RouteAdminStatus (pMultiIndex->pIndex[0].pOctetStrValue,
                                          pMultiIndex->pIndex[1].i4_SLongValue,
                                          pMultiIndex->pIndex[2].i4_SLongValue,
                                          pMultiIndex->pIndex[3].pOctetStrValue,
                                          &(pMultiData->i4_SLongValue)));

}

INT4
Fsipv6RouteAddrTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6RouteTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6RouteAddrType (pMultiIndex->pIndex[0].pOctetStrValue,
                                       pMultiIndex->pIndex[1].i4_SLongValue,
                                       pMultiIndex->pIndex[2].i4_SLongValue,
                                       pMultiIndex->pIndex[3].pOctetStrValue,
                                       &(pMultiData->i4_SLongValue)));

}

INT4
Fsipv6RouteHwStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6RouteTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6RouteHwStatus (pMultiIndex->pIndex[0].pOctetStrValue,
                                       pMultiIndex->pIndex[1].i4_SLongValue,
                                       pMultiIndex->pIndex[2].i4_SLongValue,
                                       pMultiIndex->pIndex[3].pOctetStrValue,
                                       &(pMultiData->i4_SLongValue)));

}

INT4 Fsipv6RoutePreferenceGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6RouteTable(
        pMultiIndex->pIndex[0].pOctetStrValue,
        pMultiIndex->pIndex[1].i4_SLongValue,
        pMultiIndex->pIndex[2].i4_SLongValue,
        pMultiIndex->pIndex[3].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return(nmhGetFsipv6RoutePreference(
        pMultiIndex->pIndex[0].pOctetStrValue,
        pMultiIndex->pIndex[1].i4_SLongValue,
        pMultiIndex->pIndex[2].i4_SLongValue,
        pMultiIndex->pIndex[3].pOctetStrValue,
        &(pMultiData->i4_SLongValue)));

}

INT4
Fsipv6RouteIfIndexSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsipv6RouteIfIndex (pMultiIndex->pIndex[0].pOctetStrValue,
                                      pMultiIndex->pIndex[1].i4_SLongValue,
                                      pMultiIndex->pIndex[2].i4_SLongValue,
                                      pMultiIndex->pIndex[3].pOctetStrValue,
                                      pMultiData->i4_SLongValue));

}

INT4
Fsipv6RouteMetricSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsipv6RouteMetric (pMultiIndex->pIndex[0].pOctetStrValue,
                                     pMultiIndex->pIndex[1].i4_SLongValue,
                                     pMultiIndex->pIndex[2].i4_SLongValue,
                                     pMultiIndex->pIndex[3].pOctetStrValue,
                                     pMultiData->u4_ULongValue));

}

INT4
Fsipv6RouteTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsipv6RouteType (pMultiIndex->pIndex[0].pOctetStrValue,
                                   pMultiIndex->pIndex[1].i4_SLongValue,
                                   pMultiIndex->pIndex[2].i4_SLongValue,
                                   pMultiIndex->pIndex[3].pOctetStrValue,
                                   pMultiData->i4_SLongValue));

}

INT4
Fsipv6RouteTagSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsipv6RouteTag (pMultiIndex->pIndex[0].pOctetStrValue,
                                  pMultiIndex->pIndex[1].i4_SLongValue,
                                  pMultiIndex->pIndex[2].i4_SLongValue,
                                  pMultiIndex->pIndex[3].pOctetStrValue,
                                  pMultiData->u4_ULongValue));

}

INT4
Fsipv6RouteAdminStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsipv6RouteAdminStatus (pMultiIndex->pIndex[0].pOctetStrValue,
                                          pMultiIndex->pIndex[1].i4_SLongValue,
                                          pMultiIndex->pIndex[2].i4_SLongValue,
                                          pMultiIndex->pIndex[3].pOctetStrValue,
                                          pMultiData->i4_SLongValue));

}

INT4
Fsipv6RouteAddrTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsipv6RouteAddrType (pMultiIndex->pIndex[0].pOctetStrValue,
                                       pMultiIndex->pIndex[1].i4_SLongValue,
                                       pMultiIndex->pIndex[2].i4_SLongValue,
                                       pMultiIndex->pIndex[3].pOctetStrValue,
                                       pMultiData->i4_SLongValue));

}

INT4 Fsipv6RoutePreferenceSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsipv6RoutePreference(
        pMultiIndex->pIndex[0].pOctetStrValue,
        pMultiIndex->pIndex[1].i4_SLongValue,
        pMultiIndex->pIndex[2].i4_SLongValue,
        pMultiIndex->pIndex[3].pOctetStrValue,
        pMultiData->i4_SLongValue));

}

INT4
Fsipv6RouteIfIndexTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    return (nmhTestv2Fsipv6RouteIfIndex (pu4Error,
                                         pMultiIndex->pIndex[0].pOctetStrValue,
                                         pMultiIndex->pIndex[1].i4_SLongValue,
                                         pMultiIndex->pIndex[2].i4_SLongValue,
                                         pMultiIndex->pIndex[3].pOctetStrValue,
                                         pMultiData->i4_SLongValue));

}

INT4
Fsipv6RouteMetricTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    return (nmhTestv2Fsipv6RouteMetric (pu4Error,
                                        pMultiIndex->pIndex[0].pOctetStrValue,
                                        pMultiIndex->pIndex[1].i4_SLongValue,
                                        pMultiIndex->pIndex[2].i4_SLongValue,
                                        pMultiIndex->pIndex[3].pOctetStrValue,
                                        pMultiData->u4_ULongValue));

}

INT4
Fsipv6RouteTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    return (nmhTestv2Fsipv6RouteType (pu4Error,
                                      pMultiIndex->pIndex[0].pOctetStrValue,
                                      pMultiIndex->pIndex[1].i4_SLongValue,
                                      pMultiIndex->pIndex[2].i4_SLongValue,
                                      pMultiIndex->pIndex[3].pOctetStrValue,
                                      pMultiData->i4_SLongValue));

}

INT4
Fsipv6RouteTagTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                    tRetVal * pMultiData)
{
    return (nmhTestv2Fsipv6RouteTag (pu4Error,
                                     pMultiIndex->pIndex[0].pOctetStrValue,
                                     pMultiIndex->pIndex[1].i4_SLongValue,
                                     pMultiIndex->pIndex[2].i4_SLongValue,
                                     pMultiIndex->pIndex[3].pOctetStrValue,
                                     pMultiData->u4_ULongValue));

}

INT4
Fsipv6RouteAdminStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2Fsipv6RouteAdminStatus (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             pOctetStrValue,
                                             pMultiIndex->pIndex[1].
                                             i4_SLongValue,
                                             pMultiIndex->pIndex[2].
                                             i4_SLongValue,
                                             pMultiIndex->pIndex[3].
                                             pOctetStrValue,
                                             pMultiData->i4_SLongValue));

}

INT4
Fsipv6RouteAddrTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2Fsipv6RouteAddrType (pu4Error,
                                          pMultiIndex->pIndex[0].pOctetStrValue,
                                          pMultiIndex->pIndex[1].i4_SLongValue,
                                          pMultiIndex->pIndex[2].i4_SLongValue,
                                          pMultiIndex->pIndex[3].pOctetStrValue,
                                          pMultiData->i4_SLongValue));

}

INT4 Fsipv6RoutePreferenceTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2Fsipv6RoutePreference(pu4Error,
        pMultiIndex->pIndex[0].pOctetStrValue,
        pMultiIndex->pIndex[1].i4_SLongValue,
        pMultiIndex->pIndex[2].i4_SLongValue,
        pMultiIndex->pIndex[3].pOctetStrValue,
        pMultiData->i4_SLongValue));

}

INT4
Fsipv6RouteTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsipv6RouteTable (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsipv6PrefTable (tSnmpIndex * pFirstMultiIndex,
                             tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsipv6PrefTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsipv6PrefTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Fsipv6PreferenceGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6PrefTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6Preference (pMultiIndex->pIndex[0].i4_SLongValue,
                                    &(pMultiData->u4_ULongValue)));

}

INT4
Fsipv6PreferenceSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsipv6Preference (pMultiIndex->pIndex[0].i4_SLongValue,
                                    pMultiData->u4_ULongValue));

}

INT4
Fsipv6PreferenceTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    return (nmhTestv2Fsipv6Preference (pu4Error,
                                       pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiData->u4_ULongValue));

}

INT4
Fsipv6PrefTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                    tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsipv6PrefTable (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsIpv6TestRedEntryTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsIpv6TestRedEntryTime (&(pMultiData->i4_SLongValue)));
}

INT4
FsIpv6TestRedExitTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsIpv6TestRedExitTime (&(pMultiData->i4_SLongValue)));
}

INT4
GetNextIndexFsipv6IfRaRDNSSTable (tSnmpIndex * pFirstMultiIndex,
                                  tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsipv6IfRaRDNSSTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsipv6IfRaRDNSSTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Fsipv6IfRadvRDNSSOpenGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6IfRaRDNSSTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6IfRadvRDNSSOpen (pMultiIndex->pIndex[0].i4_SLongValue,
                                         &(pMultiData->i4_SLongValue)));

}

INT4
Fsipv6IfRaRDNSSPreferenceGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6IfRaRDNSSTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6IfRaRDNSSPreference
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsipv6IfRaRDNSSLifetimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6IfRaRDNSSTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6IfRaRDNSSLifetime (pMultiIndex->pIndex[0].i4_SLongValue,
                                           &(pMultiData->u4_ULongValue)));

}

INT4
Fsipv6IfRaRDNSSAddrOneGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6IfRaRDNSSTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6IfRaRDNSSAddrOne (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiData->pOctetStrValue));

}

INT4
Fsipv6IfRaRDNSSAddrTwoGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6IfRaRDNSSTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6IfRaRDNSSAddrTwo (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiData->pOctetStrValue));

}

INT4
Fsipv6IfRaRDNSSAddrThreeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6IfRaRDNSSTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6IfRaRDNSSAddrThree
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
Fsipv6IfRaRDNSSRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6IfRaRDNSSTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6IfRaRDNSSRowStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}


INT4
Fsipv6IfDomainNameOneGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6IfRaRDNSSTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6IfDomainNameOne (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->pOctetStrValue));

}

INT4
Fsipv6IfDomainNameTwoGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6IfRaRDNSSTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6IfDomainNameTwo (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->pOctetStrValue));

}

INT4
Fsipv6IfDomainNameThreeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6IfRaRDNSSTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6IfDomainNameThree (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->pOctetStrValue));

}

INT4
Fsipv6IfDnsLifeTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6IfRaRDNSSTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6IfDnsLifeTime (pMultiIndex->pIndex[0].i4_SLongValue,
                                       &(pMultiData->i4_SLongValue)));

}
INT4 
Fsipv6IfRaRDNSSAddrOneLifeGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
        if (nmhValidateIndexInstanceFsipv6IfRaRDNSSTable(
                pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
        {
                return SNMP_FAILURE;
        }
        return(nmhGetFsipv6IfRaRDNSSAddrOneLife(
                pMultiIndex->pIndex[0].i4_SLongValue,
                &(pMultiData->u4_ULongValue)));

}
INT4 
Fsipv6IfRaRDNSSAddrTwoLifeGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
        if (nmhValidateIndexInstanceFsipv6IfRaRDNSSTable(
                pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
        {
                return SNMP_FAILURE;
        }
        return(nmhGetFsipv6IfRaRDNSSAddrTwoLife(
                pMultiIndex->pIndex[0].i4_SLongValue,
                &(pMultiData->u4_ULongValue)));

}
INT4 
Fsipv6IfRaRDNSSAddrThreeLifeGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
        if (nmhValidateIndexInstanceFsipv6IfRaRDNSSTable(
                pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
        {
                return SNMP_FAILURE;
        }
        return(nmhGetFsipv6IfRaRDNSSAddrThreeLife(
                pMultiIndex->pIndex[0].i4_SLongValue,
                &(pMultiData->u4_ULongValue)));

}
INT4 Fsipv6IfDomainNameOneLifeGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
        if (nmhValidateIndexInstanceFsipv6IfRaRDNSSTable(
                pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
        {
                return SNMP_FAILURE;
        }
        return(nmhGetFsipv6IfDomainNameOneLife(
                pMultiIndex->pIndex[0].i4_SLongValue,
                &(pMultiData->i4_SLongValue)));

}
INT4 Fsipv6IfDomainNameTwoLifeGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
        if (nmhValidateIndexInstanceFsipv6IfRaRDNSSTable(
                pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
        {
                return SNMP_FAILURE;
        }
        return(nmhGetFsipv6IfDomainNameTwoLife(
                pMultiIndex->pIndex[0].i4_SLongValue,
                &(pMultiData->i4_SLongValue)));

}
INT4 Fsipv6IfDomainNameThreeLifeGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
        if (nmhValidateIndexInstanceFsipv6IfRaRDNSSTable(
                pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
        {
                return SNMP_FAILURE;
        }
        return(nmhGetFsipv6IfDomainNameThreeLife(
                pMultiIndex->pIndex[0].i4_SLongValue,
                &(pMultiData->i4_SLongValue)));

}

INT4
Fsipv6IfRadvRDNSSOpenSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsipv6IfRadvRDNSSOpen (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->i4_SLongValue));

}

INT4
Fsipv6IfRaRDNSSPreferenceSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsipv6IfRaRDNSSPreference
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
Fsipv6IfRaRDNSSLifetimeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsipv6IfRaRDNSSLifetime (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->u4_ULongValue));

}

INT4
Fsipv6IfRaRDNSSAddrOneSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsipv6IfRaRDNSSAddrOne (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiData->pOctetStrValue));

}

INT4
Fsipv6IfRaRDNSSAddrTwoSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsipv6IfRaRDNSSAddrTwo (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiData->pOctetStrValue));

}

INT4
Fsipv6IfRaRDNSSAddrThreeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsipv6IfRaRDNSSAddrThree
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
Fsipv6IfRaRDNSSRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsipv6IfRaRDNSSRowStatus
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
Fsipv6IfDomainNameOneSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsipv6IfDomainNameOne (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->pOctetStrValue));

}

INT4
Fsipv6IfDomainNameTwoSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsipv6IfDomainNameTwo (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->pOctetStrValue));

}

INT4
Fsipv6IfDomainNameThreeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsipv6IfDomainNameThree (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->pOctetStrValue));

}

INT4
Fsipv6IfDnsLifeTimeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsipv6IfDnsLifeTime (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiData->i4_SLongValue));

}
INT4 
Fsipv6IfRaRDNSSAddrOneLifeSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
        return (nmhSetFsipv6IfRaRDNSSAddrOneLife(
                pMultiIndex->pIndex[0].i4_SLongValue,
                pMultiData->u4_ULongValue));

}

INT4 
Fsipv6IfRaRDNSSAddrTwoLifeSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
        return (nmhSetFsipv6IfRaRDNSSAddrTwoLife(
                pMultiIndex->pIndex[0].i4_SLongValue,
                pMultiData->u4_ULongValue));

}

INT4 
Fsipv6IfRaRDNSSAddrThreeLifeSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
        return (nmhSetFsipv6IfRaRDNSSAddrThreeLife(
                pMultiIndex->pIndex[0].i4_SLongValue,
                pMultiData->u4_ULongValue));

}
INT4 Fsipv6IfDomainNameOneLifeSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
        return (nmhSetFsipv6IfDomainNameOneLife(
                pMultiIndex->pIndex[0].i4_SLongValue,
                pMultiData->i4_SLongValue));

}

INT4 Fsipv6IfDomainNameTwoLifeSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
        return (nmhSetFsipv6IfDomainNameTwoLife(
                pMultiIndex->pIndex[0].i4_SLongValue,
                pMultiData->i4_SLongValue));

}

INT4 Fsipv6IfDomainNameThreeLifeSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
        return (nmhSetFsipv6IfDomainNameThreeLife(
                pMultiIndex->pIndex[0].i4_SLongValue,
                pMultiData->i4_SLongValue));

}


INT4
Fsipv6IfRadvRDNSSOpenTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2Fsipv6IfRadvRDNSSOpen (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            pMultiData->i4_SLongValue));

}

INT4
Fsipv6IfRaRDNSSPreferenceTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2Fsipv6IfRaRDNSSPreference (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                i4_SLongValue,
                                                pMultiData->i4_SLongValue));

}

INT4
Fsipv6IfRaRDNSSLifetimeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2Fsipv6IfRaRDNSSLifetime (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              i4_SLongValue,
                                              pMultiData->u4_ULongValue));

}

INT4
Fsipv6IfRaRDNSSAddrOneTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2Fsipv6IfRaRDNSSAddrOne (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             i4_SLongValue,
                                             pMultiData->pOctetStrValue));

}

INT4
Fsipv6IfRaRDNSSAddrTwoTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2Fsipv6IfRaRDNSSAddrTwo (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             i4_SLongValue,
                                             pMultiData->pOctetStrValue));

}

INT4
Fsipv6IfRaRDNSSAddrThreeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2Fsipv6IfRaRDNSSAddrThree (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               i4_SLongValue,
                                               pMultiData->pOctetStrValue));

}

INT4
Fsipv6IfRaRDNSSRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2Fsipv6IfRaRDNSSRowStatus (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               i4_SLongValue,
                                               pMultiData->i4_SLongValue));

}

INT4
Fsipv6IfDomainNameOneTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2Fsipv6IfDomainNameOne (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            pMultiData->pOctetStrValue));

}

INT4
Fsipv6IfDomainNameTwoTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2Fsipv6IfDomainNameTwo (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            pMultiData->pOctetStrValue));

}

INT4
Fsipv6IfDomainNameThreeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2Fsipv6IfDomainNameThree (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              i4_SLongValue,
                                              pMultiData->pOctetStrValue));

}

INT4
Fsipv6IfDnsLifeTimeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2Fsipv6IfDnsLifeTime (pu4Error,
                                          pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiData->u4_ULongValue));

}
INT4 
Fsipv6IfRaRDNSSAddrOneLifeTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
        return (nmhTestv2Fsipv6IfRaRDNSSAddrOneLife(pu4Error,
                pMultiIndex->pIndex[0].i4_SLongValue,
                pMultiData->u4_ULongValue));

}

INT4 
Fsipv6IfRaRDNSSAddrTwoLifeTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
        return (nmhTestv2Fsipv6IfRaRDNSSAddrTwoLife(pu4Error,
                pMultiIndex->pIndex[0].i4_SLongValue,
                pMultiData->u4_ULongValue));

}

INT4 
Fsipv6IfRaRDNSSAddrThreeLifeTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
        return (nmhTestv2Fsipv6IfRaRDNSSAddrThreeLife(pu4Error,
                pMultiIndex->pIndex[0].i4_SLongValue,
                pMultiData->u4_ULongValue));

}

INT4 Fsipv6IfDomainNameOneLifeTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
        return (nmhTestv2Fsipv6IfDomainNameOneLife(pu4Error,
                pMultiIndex->pIndex[0].i4_SLongValue,
                (UINT4) pMultiData->i4_SLongValue));

}

INT4 Fsipv6IfDomainNameTwoLifeTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
        return (nmhTestv2Fsipv6IfDomainNameTwoLife(pu4Error,
                pMultiIndex->pIndex[0].i4_SLongValue,
                (UINT4) pMultiData->i4_SLongValue));

}

INT4 Fsipv6IfDomainNameThreeLifeTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
        return (nmhTestv2Fsipv6IfDomainNameThreeLife(pu4Error,
                pMultiIndex->pIndex[0].i4_SLongValue,
                (UINT4) pMultiData->i4_SLongValue));

}
INT4
Fsipv6IfRaRDNSSTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsipv6IfRaRDNSSTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4 Fsipv6SENDSecLevelGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    return(nmhGetFsipv6SENDSecLevel(&(pMultiData->i4_SLongValue)));
}
INT4 Fsipv6SENDNbrSecLevelGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    return(nmhGetFsipv6SENDNbrSecLevel(&(pMultiData->i4_SLongValue)));
}
INT4 Fsipv6SENDAuthTypeGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    return(nmhGetFsipv6SENDAuthType(&(pMultiData->i4_SLongValue)));
}
INT4 Fsipv6SENDMinBitsGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    return(nmhGetFsipv6SENDMinBits(&(pMultiData->i4_SLongValue)));
}
INT4 Fsipv6SENDSecDADGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    return(nmhGetFsipv6SENDSecDAD(&(pMultiData->i4_SLongValue)));
}
INT4 Fsipv6SENDPrefixChkGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    return(nmhGetFsipv6SENDPrefixChk(&(pMultiData->i4_SLongValue)));

}
INT4 Fsipv6ECMPPRTTimerGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
        UNUSED_PARAM(pMultiIndex);
        return(nmhGetFsipv6ECMPPRTTimer(&(pMultiData->i4_SLongValue)));
}
INT4 Fsipv6NdCacheTimeoutGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
   UNUSED_PARAM(pMultiIndex);
   return(nmhGetFsipv6NdCacheTimeout(&(pMultiData->i4_SLongValue)));
}
INT4 Fsipv6SENDSecLevelSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    return(nmhSetFsipv6SENDSecLevel(pMultiData->i4_SLongValue));
}


INT4 Fsipv6SENDNbrSecLevelSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    return(nmhSetFsipv6SENDNbrSecLevel(pMultiData->i4_SLongValue));
}


INT4 Fsipv6SENDAuthTypeSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    return(nmhSetFsipv6SENDAuthType(pMultiData->i4_SLongValue));
}


INT4 Fsipv6SENDMinBitsSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    return(nmhSetFsipv6SENDMinBits(pMultiData->i4_SLongValue));
}


INT4 Fsipv6SENDSecDADSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    return(nmhSetFsipv6SENDSecDAD(pMultiData->i4_SLongValue));
}


INT4 Fsipv6SENDPrefixChkSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    return(nmhSetFsipv6SENDPrefixChk(pMultiData->i4_SLongValue));
}

INT4 Fsipv6ECMPPRTTimerSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
        UNUSED_PARAM(pMultiIndex);
        return(nmhSetFsipv6ECMPPRTTimer(pMultiData->i4_SLongValue));
}

INT4 Fsipv6NdCacheTimeoutSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
   UNUSED_PARAM(pMultiIndex);
   return(nmhSetFsipv6NdCacheTimeout(pMultiData->i4_SLongValue));
}

INT4 Fsipv6SENDSecLevelTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    return(nmhTestv2Fsipv6SENDSecLevel(pu4Error, pMultiData->i4_SLongValue));
}


INT4 Fsipv6SENDNbrSecLevelTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    return(nmhTestv2Fsipv6SENDNbrSecLevel(pu4Error, pMultiData->i4_SLongValue));
}


INT4 Fsipv6SENDAuthTypeTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    return(nmhTestv2Fsipv6SENDAuthType(pu4Error, pMultiData->i4_SLongValue));
}


INT4 Fsipv6SENDMinBitsTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    return(nmhTestv2Fsipv6SENDMinBits(pu4Error, pMultiData->i4_SLongValue));
}


INT4 Fsipv6SENDSecDADTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    return(nmhTestv2Fsipv6SENDSecDAD(pu4Error, pMultiData->i4_SLongValue));
}


INT4 Fsipv6SENDPrefixChkTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    return(nmhTestv2Fsipv6SENDPrefixChk(pu4Error, pMultiData->i4_SLongValue));
}

INT4 Fsipv6ECMPPRTTimerTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
        UNUSED_PARAM(pMultiIndex);
        return(nmhTestv2Fsipv6ECMPPRTTimer(pu4Error, pMultiData->i4_SLongValue));
}

INT4 Fsipv6NdCacheTimeoutTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
   UNUSED_PARAM(pMultiIndex);
   return(nmhTestv2Fsipv6NdCacheTimeout(pu4Error, pMultiData->i4_SLongValue));
}

INT4 Fsipv6IfSENDSecStatusGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6IfTable(
        pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return(nmhGetFsipv6IfSENDSecStatus(
        pMultiIndex->pIndex[0].i4_SLongValue,
        &(pMultiData->i4_SLongValue)));

}

INT4 Fsipv6IfSENDDeltaTimestampGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6IfTable(
        pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return(nmhGetFsipv6IfSENDDeltaTimestamp(
        pMultiIndex->pIndex[0].i4_SLongValue,
        &(pMultiData->u4_ULongValue)));

}
INT4 Fsipv6IfSENDFuzzTimestampGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6IfTable(
        pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return(nmhGetFsipv6IfSENDFuzzTimestamp(
        pMultiIndex->pIndex[0].i4_SLongValue,
        &(pMultiData->u4_ULongValue)));

}
INT4 Fsipv6IfSENDDriftTimestampGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6IfTable(
        pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return(nmhGetFsipv6IfSENDDriftTimestamp(
        pMultiIndex->pIndex[0].i4_SLongValue,
        &(pMultiData->u4_ULongValue)));

}
INT4 Fsipv6IfDefRoutePreferenceGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
        if (nmhValidateIndexInstanceFsipv6IfTable(
                pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
        {
                return SNMP_FAILURE;
        }
        return(nmhGetFsipv6IfDefRoutePreference(
                pMultiIndex->pIndex[0].i4_SLongValue,
                &(pMultiData->i4_SLongValue)));

}

INT4 Fsipv6IfSENDSecStatusSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsipv6IfSENDSecStatus(
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiData->i4_SLongValue));

}

INT4 Fsipv6IfSENDDeltaTimestampSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsipv6IfSENDDeltaTimestamp(
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiData->u4_ULongValue));

}

INT4 Fsipv6IfSENDFuzzTimestampSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsipv6IfSENDFuzzTimestamp(
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiData->u4_ULongValue));

}

INT4 Fsipv6IfSENDDriftTimestampSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsipv6IfSENDDriftTimestamp(
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiData->u4_ULongValue));

}
INT4 Fsipv6IfDefRoutePreferenceSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
        return (nmhSetFsipv6IfDefRoutePreference(
                pMultiIndex->pIndex[0].i4_SLongValue,
                pMultiData->i4_SLongValue));

}

INT4 Fsipv6IfSENDSecStatusTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2Fsipv6IfSENDSecStatus(pu4Error,
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiData->i4_SLongValue));

}

INT4 Fsipv6IfSENDDeltaTimestampTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2Fsipv6IfSENDDeltaTimestamp(pu4Error,
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiData->u4_ULongValue));

}

INT4 Fsipv6IfSENDFuzzTimestampTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2Fsipv6IfSENDFuzzTimestamp(pu4Error,
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiData->u4_ULongValue));

}

INT4 Fsipv6IfSENDDriftTimestampTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2Fsipv6IfSENDDriftTimestamp(pu4Error,
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiData->u4_ULongValue));

}

INT4 Fsipv6IfDefRoutePreferenceTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
        return (nmhTestv2Fsipv6IfDefRoutePreference(pu4Error,
                pMultiIndex->pIndex[0].i4_SLongValue,
                pMultiData->i4_SLongValue));

}

INT4 Fsipv6IfStatsSENDDroppedPktsGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6IfStatsTable(
        pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return(nmhGetFsipv6IfStatsSENDDroppedPkts(
        pMultiIndex->pIndex[0].i4_SLongValue,
        &(pMultiData->u4_ULongValue)));

}
INT4 Fsipv6IfStatsSENDInvalidPktsGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6IfStatsTable(
        pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return(nmhGetFsipv6IfStatsSENDInvalidPkts(
        pMultiIndex->pIndex[0].i4_SLongValue,
        &(pMultiData->u4_ULongValue)));

}

INT4 Fsipv6AddrSENDCgaStatusGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6AddrTable(
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiIndex->pIndex[1].pOctetStrValue,
        pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return(nmhGetFsipv6AddrSENDCgaStatus(
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiIndex->pIndex[1].pOctetStrValue,
        pMultiIndex->pIndex[2].i4_SLongValue,
        &(pMultiData->i4_SLongValue)));

}
INT4 Fsipv6AddrSENDCgaModifierGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6AddrTable(
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiIndex->pIndex[1].pOctetStrValue,
        pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return(nmhGetFsipv6AddrSENDCgaModifier(
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiIndex->pIndex[1].pOctetStrValue,
        pMultiIndex->pIndex[2].i4_SLongValue,
        pMultiData->pOctetStrValue));

}
INT4 Fsipv6AddrSENDCollisionCountGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6AddrTable(
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiIndex->pIndex[1].pOctetStrValue,
        pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return(nmhGetFsipv6AddrSENDCollisionCount(
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiIndex->pIndex[1].pOctetStrValue,
        pMultiIndex->pIndex[2].i4_SLongValue,
        &(pMultiData->i4_SLongValue)));

}
INT4 Fsipv6NdLanCacheIsSecureGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6NdLanCacheTable(
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return(nmhGetFsipv6NdLanCacheIsSecure(
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiIndex->pIndex[1].pOctetStrValue,
        &(pMultiData->i4_SLongValue)));

}

INT4 GetNextIndexFsipv6SENDSentPktStatsTable(tSnmpIndex *pFirstMultiIndex,tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL) 
    {
        if (nmhGetFirstIndexFsipv6SENDSentPktStatsTable(
            &(pNextMultiIndex->pIndex[0].i4_SLongValue),
            &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsipv6SENDSentPktStatsTable(
            pFirstMultiIndex->pIndex[0].i4_SLongValue,
            &(pNextMultiIndex->pIndex[0].i4_SLongValue),
            pFirstMultiIndex->pIndex[1].i4_SLongValue,
            &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    
    return SNMP_SUCCESS;
}
INT4 Fsipv6SENDSentCgaOptPktsGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6SENDSentPktStatsTable(
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return(nmhGetFsipv6SENDSentCgaOptPkts(
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiIndex->pIndex[1].i4_SLongValue,
        &(pMultiData->u4_ULongValue)));

}
INT4 Fsipv6SENDSentCertOptPktsGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6SENDSentPktStatsTable(
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return(nmhGetFsipv6SENDSentCertOptPkts(
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiIndex->pIndex[1].i4_SLongValue,
        &(pMultiData->u4_ULongValue)));

}
INT4 Fsipv6SENDSentMtuOptPktsGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6SENDSentPktStatsTable(
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return(nmhGetFsipv6SENDSentMtuOptPkts(
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiIndex->pIndex[1].i4_SLongValue,
        &(pMultiData->u4_ULongValue)));

}
INT4 Fsipv6SENDSentNonceOptPktsGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6SENDSentPktStatsTable(
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return(nmhGetFsipv6SENDSentNonceOptPkts(
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiIndex->pIndex[1].i4_SLongValue,
        &(pMultiData->u4_ULongValue)));

}
INT4 Fsipv6SENDSentPrefixOptPktsGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6SENDSentPktStatsTable(
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return(nmhGetFsipv6SENDSentPrefixOptPkts(
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiIndex->pIndex[1].i4_SLongValue,
        &(pMultiData->u4_ULongValue)));

}
INT4 Fsipv6SENDSentRedirHdrPktsGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6SENDSentPktStatsTable(
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return(nmhGetFsipv6SENDSentRedirHdrPkts(
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiIndex->pIndex[1].i4_SLongValue,
        &(pMultiData->u4_ULongValue)));

}
INT4 Fsipv6SENDSentRsaOptPktsGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6SENDSentPktStatsTable(
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return(nmhGetFsipv6SENDSentRsaOptPkts(
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiIndex->pIndex[1].i4_SLongValue,
        &(pMultiData->u4_ULongValue)));

}
INT4 Fsipv6SENDSentSrcLinkAddrPktsGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6SENDSentPktStatsTable(
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return(nmhGetFsipv6SENDSentSrcLinkAddrPkts(
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiIndex->pIndex[1].i4_SLongValue,
        &(pMultiData->u4_ULongValue)));

}
INT4 Fsipv6SENDSentTgtLinkAddrPktsGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6SENDSentPktStatsTable(
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return(nmhGetFsipv6SENDSentTgtLinkAddrPkts(
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiIndex->pIndex[1].i4_SLongValue,
        &(pMultiData->u4_ULongValue)));

}
INT4 Fsipv6SENDSentTaOptPktsGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6SENDSentPktStatsTable(
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return(nmhGetFsipv6SENDSentTaOptPkts(
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiIndex->pIndex[1].i4_SLongValue,
        &(pMultiData->u4_ULongValue)));

}
INT4 Fsipv6SENDSentTimeStampOptPktsGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6SENDSentPktStatsTable(
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return(nmhGetFsipv6SENDSentTimeStampOptPkts(
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiIndex->pIndex[1].i4_SLongValue,
        &(pMultiData->u4_ULongValue)));

}

INT4 GetNextIndexFsipv6SENDRcvPktStatsTable(tSnmpIndex *pFirstMultiIndex,tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL) 
    {
        if (nmhGetFirstIndexFsipv6SENDRcvPktStatsTable(
            &(pNextMultiIndex->pIndex[0].i4_SLongValue),
            &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsipv6SENDRcvPktStatsTable(
            pFirstMultiIndex->pIndex[0].i4_SLongValue,
            &(pNextMultiIndex->pIndex[0].i4_SLongValue),
            pFirstMultiIndex->pIndex[1].i4_SLongValue,
            &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    
    return SNMP_SUCCESS;
}
INT4 Fsipv6SENDRcvCgaOptPktsGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6SENDRcvPktStatsTable(
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return(nmhGetFsipv6SENDRcvCgaOptPkts(
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiIndex->pIndex[1].i4_SLongValue,
        &(pMultiData->u4_ULongValue)));

}
INT4 Fsipv6SENDRcvCertOptPktsGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6SENDRcvPktStatsTable(
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return(nmhGetFsipv6SENDRcvCertOptPkts(
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiIndex->pIndex[1].i4_SLongValue,
        &(pMultiData->u4_ULongValue)));

}
INT4 Fsipv6SENDRcvMtuOptPktsGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6SENDRcvPktStatsTable(
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return(nmhGetFsipv6SENDRcvMtuOptPkts(
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiIndex->pIndex[1].i4_SLongValue,
        &(pMultiData->u4_ULongValue)));

}
INT4 Fsipv6SENDRcvNonceOptPktsGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6SENDRcvPktStatsTable(
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return(nmhGetFsipv6SENDRcvNonceOptPkts(
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiIndex->pIndex[1].i4_SLongValue,
        &(pMultiData->u4_ULongValue)));

}
INT4 Fsipv6SENDRcvPrefixOptPktsGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6SENDRcvPktStatsTable(
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return(nmhGetFsipv6SENDRcvPrefixOptPkts(
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiIndex->pIndex[1].i4_SLongValue,
        &(pMultiData->u4_ULongValue)));

}
INT4 Fsipv6SENDRcvRedirHdrPktsGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6SENDRcvPktStatsTable(
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return(nmhGetFsipv6SENDRcvRedirHdrPkts(
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiIndex->pIndex[1].i4_SLongValue,
        &(pMultiData->u4_ULongValue)));

}
INT4 Fsipv6SENDRcvRsaOptPktsGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6SENDRcvPktStatsTable(
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return(nmhGetFsipv6SENDRcvRsaOptPkts(
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiIndex->pIndex[1].i4_SLongValue,
        &(pMultiData->u4_ULongValue)));

}
INT4 Fsipv6SENDRcvSrcLinkAddrPktsGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6SENDRcvPktStatsTable(
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return(nmhGetFsipv6SENDRcvSrcLinkAddrPkts(
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiIndex->pIndex[1].i4_SLongValue,
        &(pMultiData->u4_ULongValue)));

}
INT4 Fsipv6SENDRcvTgtLinkAddrPktsGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6SENDRcvPktStatsTable(
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return(nmhGetFsipv6SENDRcvTgtLinkAddrPkts(
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiIndex->pIndex[1].i4_SLongValue,
        &(pMultiData->u4_ULongValue)));

}
INT4 Fsipv6SENDRcvTaOptPktsGet(tSnmpIndex * pMultiIndex, 
                               tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6SENDRcvPktStatsTable(
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return(nmhGetFsipv6SENDRcvTaOptPkts(
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiIndex->pIndex[1].i4_SLongValue,
        &(pMultiData->u4_ULongValue)));

}
INT4 Fsipv6SENDRcvTimeStampOptPktsGet(tSnmpIndex * pMultiIndex, 
                                      tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6SENDRcvPktStatsTable(
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return(nmhGetFsipv6SENDRcvTimeStampOptPkts(
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiIndex->pIndex[1].i4_SLongValue,
        &(pMultiData->u4_ULongValue)));

}

INT4 GetNextIndexFsipv6SENDDrpPktStatsTable(tSnmpIndex *pFirstMultiIndex,
                                            tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL) 
    {
        if (nmhGetFirstIndexFsipv6SENDDrpPktStatsTable(
            &(pNextMultiIndex->pIndex[0].i4_SLongValue),
            &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsipv6SENDDrpPktStatsTable(
            pFirstMultiIndex->pIndex[0].i4_SLongValue,
            &(pNextMultiIndex->pIndex[0].i4_SLongValue),
            pFirstMultiIndex->pIndex[1].i4_SLongValue,
            &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    
    return SNMP_SUCCESS;
}
INT4 Fsipv6SENDDrpCgaOptPktsGet(tSnmpIndex * pMultiIndex, 
                                tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6SENDDrpPktStatsTable(
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return(nmhGetFsipv6SENDDrpCgaOptPkts(
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiIndex->pIndex[1].i4_SLongValue,
        &(pMultiData->u4_ULongValue)));

}
INT4 Fsipv6SENDDrpCertOptPktsGet(tSnmpIndex * pMultiIndex, 
                                 tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6SENDDrpPktStatsTable(
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return(nmhGetFsipv6SENDDrpCertOptPkts(
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiIndex->pIndex[1].i4_SLongValue,
        &(pMultiData->u4_ULongValue)));

}
INT4 Fsipv6SENDDrpMtuOptPktsGet(tSnmpIndex * pMultiIndex, 
                                tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6SENDDrpPktStatsTable(
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return(nmhGetFsipv6SENDDrpMtuOptPkts(
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiIndex->pIndex[1].i4_SLongValue,
        &(pMultiData->u4_ULongValue)));

}
INT4 Fsipv6SENDDrpNonceOptPktsGet(tSnmpIndex * pMultiIndex, 
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6SENDDrpPktStatsTable(
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return(nmhGetFsipv6SENDDrpNonceOptPkts(
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiIndex->pIndex[1].i4_SLongValue,
        &(pMultiData->u4_ULongValue)));

}
INT4 Fsipv6SENDDrpPrefixOptPktsGet(tSnmpIndex * pMultiIndex, 
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6SENDDrpPktStatsTable(
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return(nmhGetFsipv6SENDDrpPrefixOptPkts(
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiIndex->pIndex[1].i4_SLongValue,
        &(pMultiData->u4_ULongValue)));

}
INT4 Fsipv6SENDDrpRedirHdrPktsGet(tSnmpIndex * pMultiIndex, 
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6SENDDrpPktStatsTable(
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return(nmhGetFsipv6SENDDrpRedirHdrPkts(
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiIndex->pIndex[1].i4_SLongValue,
        &(pMultiData->u4_ULongValue)));

}
INT4 Fsipv6SENDDrpRsaOptPktsGet(tSnmpIndex * pMultiIndex, 
                                tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6SENDDrpPktStatsTable(
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return(nmhGetFsipv6SENDDrpRsaOptPkts(
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiIndex->pIndex[1].i4_SLongValue,
        &(pMultiData->u4_ULongValue)));

}
INT4 Fsipv6SENDDrpSrcLinkAddrPktsGet(tSnmpIndex * pMultiIndex, 
                                     tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6SENDDrpPktStatsTable(
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return(nmhGetFsipv6SENDDrpSrcLinkAddrPkts(
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiIndex->pIndex[1].i4_SLongValue,
        &(pMultiData->u4_ULongValue)));

}
INT4 Fsipv6SENDDrpTgtLinkAddrPktsGet(tSnmpIndex * pMultiIndex, 
                                     tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6SENDDrpPktStatsTable(
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return(nmhGetFsipv6SENDDrpTgtLinkAddrPkts(
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiIndex->pIndex[1].i4_SLongValue,
        &(pMultiData->u4_ULongValue)));

}
INT4 Fsipv6SENDDrpTaOptPktsGet(tSnmpIndex * pMultiIndex, 
                               tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6SENDDrpPktStatsTable(
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return(nmhGetFsipv6SENDDrpTaOptPkts(
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiIndex->pIndex[1].i4_SLongValue,
        &(pMultiData->u4_ULongValue)));

}
INT4 Fsipv6SENDDrpTimeStampOptPktsGet(tSnmpIndex * pMultiIndex, 
                                      tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsipv6SENDDrpPktStatsTable(
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return(nmhGetFsipv6SENDDrpTimeStampOptPkts(
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiIndex->pIndex[1].i4_SLongValue,
        &(pMultiData->u4_ULongValue)));

}

INT4 GetNextIndexFsipv6RARouteInfoTable(tSnmpIndex *pFirstMultiIndex,tSnmpIndex * pNextMultiIndex)
{
	if (pFirstMultiIndex == NULL) 
	{
		if (nmhGetFirstIndexFsipv6RARouteInfoTable(
			&(pNextMultiIndex->pIndex[0].i4_SLongValue),
			pNextMultiIndex->pIndex[1].pOctetStrValue,
			&(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
		{
 			return SNMP_FAILURE;
		}
	}
	else
	{
		if (nmhGetNextIndexFsipv6RARouteInfoTable(
			pFirstMultiIndex->pIndex[0].i4_SLongValue,
			&(pNextMultiIndex->pIndex[0].i4_SLongValue),
			pFirstMultiIndex->pIndex[1].pOctetStrValue,
			pNextMultiIndex->pIndex[1].pOctetStrValue,
			pFirstMultiIndex->pIndex[2].i4_SLongValue,
			&(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
		{
			return SNMP_FAILURE;
		}
	}
	
	return SNMP_SUCCESS;
}
INT4 Fsipv6RARoutePreferenceGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsipv6RARouteInfoTable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].pOctetStrValue,
		pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsipv6RARoutePreference(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].pOctetStrValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 Fsipv6RARouteLifetimeGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsipv6RARouteInfoTable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].pOctetStrValue,
		pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsipv6RARouteLifetime(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].pOctetStrValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 Fsipv6RARouteRowStatusGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsipv6RARouteInfoTable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].pOctetStrValue,
		pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsipv6RARouteRowStatus(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].pOctetStrValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 Fsipv6RARoutePreferenceSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsipv6RARoutePreference(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].pOctetStrValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 Fsipv6RARouteLifetimeSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsipv6RARouteLifetime(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].pOctetStrValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		pMultiData->u4_ULongValue));

}

INT4 Fsipv6RARouteRowStatusSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsipv6RARouteRowStatus(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].pOctetStrValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 Fsipv6RARoutePreferenceTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2Fsipv6RARoutePreference(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].pOctetStrValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 Fsipv6RARouteLifetimeTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2Fsipv6RARouteLifetime(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].pOctetStrValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		pMultiData->u4_ULongValue));

}

INT4 Fsipv6RARouteRowStatusTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2Fsipv6RARouteRowStatus(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].pOctetStrValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 Fsipv6RARouteInfoTableDep(UINT4 *pu4Error, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2Fsipv6RARouteInfoTable(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

