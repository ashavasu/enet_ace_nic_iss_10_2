/*****************************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: ip6mcnpwr.c,v 1.1 2014/12/18 12:21:47 siva Exp $
 *
 * Description: This file contains the NP wrappers for Multicast module.
 *****************************************************************************/

#ifndef __IP6MC_NPWR_C__
#define __IP6MC_NPWR_C__

#include "nputil.h"

/***************************************************************************
 *                                                                          
 *    Function Name       : Ip6mcNpWrHwProgram                                         
 *                                                                          
 *    Description         : This function takes care of calling appropriate 
 *                          Np call using the tIp6mcNpModInfo
 *                                                                          
 *    Input(s)            : FsHwNpParam of type tfsHwNp                     
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

PUBLIC UINT1
Ip6mcNpWrHwProgram (tFsHwNp * pFsHwNp)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tIp6mcNpModInfo    *pIp6mcNpModInfo = NULL;

    if (NULL == pFsHwNp)
    {
        return FNP_FAILURE;
    }

    u4Opcode = pFsHwNp->u4Opcode;
    pIp6mcNpModInfo = &(pFsHwNp->Ip6mcNpModInfo);

    if (NULL == pIp6mcNpModInfo)
    {
        return FNP_FAILURE;
    }

    switch (u4Opcode)
    {
#ifdef PIMV6_WANTED
        case FS_NP_IPV6_MC_INIT:
        {
            u1RetVal = FsNpIpv6McInit ();
            break;
        }
        case FS_NP_IPV6_MC_DE_INIT:
        {
            u1RetVal = FsNpIpv6McDeInit ();
            break;
        }
        case FS_NP_IPV6_MC_ADD_ROUTE_ENTRY:
        {
            tIp6mcNpWrFsNpIpv6McAddRouteEntry *pEntry = NULL;
            pEntry = &pIp6mcNpModInfo->Ip6mcNpFsNpIpv6McAddRouteEntry;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsNpIpv6McAddRouteEntry (pEntry->u4VrId, pEntry->pGrpAddr,
                                         pEntry->u4GrpPrefix,
                                         pEntry->pSrcIpAddr,
                                         pEntry->u4SrcIpPrefix,
                                         pEntry->u1CallerId, pEntry->rtEntry,
                                         pEntry->u2NoOfDownStreamIf,
                                         pEntry->pDownStreamIf);
            break;
        }
        case FS_NP_IPV6_MC_DEL_ROUTE_ENTRY:
        {
            tIp6mcNpWrFsNpIpv6McDelRouteEntry *pEntry = NULL;
            pEntry = &pIp6mcNpModInfo->Ip6mcNpFsNpIpv6McDelRouteEntry;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsNpIpv6McDelRouteEntry (pEntry->u4VrId, pEntry->pGrpAddr,
                                         pEntry->u4GrpPrefix,
                                         pEntry->pSrcIpAddr,
                                         pEntry->u4SrcIpPrefix, pEntry->rtEntry,
                                         pEntry->u2NoOfDownStreamIf,
                                         pEntry->pDownStreamIf);
            break;
        }
        case FS_NP_IPV6_MC_CLEAR_ALL_ROUTES:
        {
            tIp6mcNpWrFsNpIpv6McClearAllRoutes *pEntry = NULL;
            pEntry = &pIp6mcNpModInfo->Ip6mcNpFsNpIpv6McClearAllRoutes;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            FsNpIpv6McClearAllRoutes (pEntry->u4VrId);
            break;
        }
        case FS_NP_IPV6_MC_UPDATE_OIF_VLAN_ENTRY:
        {
            tIp6mcNpWrFsNpIpv6McUpdateOifVlanEntry *pEntry = NULL;
            pEntry = &pIp6mcNpModInfo->Ip6mcNpFsNpIpv6McUpdateOifVlanEntry;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            FsNpIpv6McUpdateOifVlanEntry (pEntry->u4VrId, pEntry->pGrpAddr,
                                          pEntry->u4GrpPrefix,
                                          pEntry->pSrcIpAddr,
                                          pEntry->u4SrcIpPrefix,
                                          pEntry->rtEntry,
                                          pEntry->downStreamIf);
            break;
        }
        case FS_NP_IPV6_MC_UPDATE_IIF_VLAN_ENTRY:
        {
            tIp6mcNpWrFsNpIpv6McUpdateIifVlanEntry *pEntry = NULL;
            pEntry = &pIp6mcNpModInfo->Ip6mcNpFsNpIpv6McUpdateIifVlanEntry;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            FsNpIpv6McUpdateIifVlanEntry (pEntry->u4VrId, pEntry->pGrpAddr,
                                          pEntry->u4GrpPrefix,
                                          pEntry->pSrcIpAddr,
                                          pEntry->u4SrcIpPrefix,
                                          pEntry->rtEntry,
                                          pEntry->downStreamIf);
            break;
        }
        case FS_NP_IPV6_MC_GET_HIT_STATUS:
        {
            tIp6mcNpWrFsNpIpv6McGetHitStatus *pEntry = NULL;
            pEntry = &pIp6mcNpModInfo->Ip6mcNpFsNpIpv6McGetHitStatus;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            FsNpIpv6McGetHitStatus (pEntry->pSrcIpAddr, pEntry->pGrpAddr,
                                    pEntry->u4Iif, pEntry->u2VlanId,
                                    pEntry->pu4HitStatus);
            break;
        }
        case FS_NP_IPV6_MC_ADD_CPU_PORT:
        {
            tIp6mcNpWrFsNpIpv6McAddCpuPort *pEntry = NULL;
            pEntry = &pIp6mcNpModInfo->Ip6mcNpFsNpIpv6McAddCpuPort;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsNpIpv6McAddCpuPort (pEntry->u1GenRtrId, pEntry->pGrpAddr,
                                      pEntry->u4GrpPrefix, pEntry->pSrcIpAddr,
                                      pEntry->u4SrcIpPrefix, pEntry->rtEntry,
                                      pEntry->u2NoOfDownStreamIf,
                                      pEntry->pDownStreamIf);
            break;
        }
        case FS_NP_IPV6_MC_DEL_CPU_PORT:
        {
            tIp6mcNpWrFsNpIpv6McDelCpuPort *pEntry = NULL;
            pEntry = &pIp6mcNpModInfo->Ip6mcNpFsNpIpv6McDelCpuPort;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsNpIpv6McDelCpuPort (pEntry->u1GenRtrId, pEntry->pGrpAddr,
                                      pEntry->u4GrpPrefix, pEntry->pSrcIpAddr,
                                      pEntry->u4SrcIpPrefix, pEntry->rtEntry);
            break;
        }
        case FS_NP_IPV6_GET_M_ROUTE_STATS:
        {
            tIp6mcNpWrFsNpIpv6GetMRouteStats *pEntry = NULL;
            pEntry = &pIp6mcNpModInfo->Ip6mcNpFsNpIpv6GetMRouteStats;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsNpIpv6GetMRouteStats (pEntry->u4VrId, pEntry->pu1GrpAddr,
                                        pEntry->pu1SrcAddr, pEntry->i4StatType,
                                        pEntry->pu4RetValue);
            break;
        }
        case FS_NP_IPV6_GET_M_ROUTE_H_C_STATS:
        {
            tIp6mcNpWrFsNpIpv6GetMRouteHCStats *pEntry = NULL;
            pEntry = &pIp6mcNpModInfo->Ip6mcNpFsNpIpv6GetMRouteHCStats;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsNpIpv6GetMRouteHCStats (pEntry->u4VrId, pEntry->pu1GrpAddr,
                                          pEntry->pu1SrcAddr,
                                          pEntry->i4StatType,
                                          pEntry->pu8RetValue);
            break;
        }
        case FS_NP_IPV6_GET_M_NEXT_HOP_STATS:
        {
            tIp6mcNpWrFsNpIpv6GetMNextHopStats *pEntry = NULL;
            pEntry = &pIp6mcNpModInfo->Ip6mcNpFsNpIpv6GetMNextHopStats;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsNpIpv6GetMNextHopStats (pEntry->u4VrId, pEntry->pu1GrpAddr,
                                          pEntry->pu1SrcAddr,
                                          pEntry->i4OutIfIndex,
                                          pEntry->i4StatType,
                                          pEntry->pu4RetValue);
            break;
        }
        case FS_NP_IPV6_GET_M_IFACE_STATS:
        {
            tIp6mcNpWrFsNpIpv6GetMIfaceStats *pEntry = NULL;
            pEntry = &pIp6mcNpModInfo->Ip6mcNpFsNpIpv6GetMIfaceStats;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsNpIpv6GetMIfaceStats (pEntry->u4VrId, pEntry->i4IfIndex,
                                        pEntry->i4StatType,
                                        pEntry->pu4RetValue);
            break;
        }
        case FS_NP_IPV6_GET_M_IFACE_H_C_STATS:
        {
            tIp6mcNpWrFsNpIpv6GetMIfaceHCStats *pEntry = NULL;
            pEntry = &pIp6mcNpModInfo->Ip6mcNpFsNpIpv6GetMIfaceHCStats;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsNpIpv6GetMIfaceHCStats (pEntry->u4VrId, pEntry->i4IfIndex,
                                          pEntry->i4StatType,
                                          pEntry->pu8RetValue);
            break;
        }
        case FS_NP_IPV6_SET_M_IFACE_TTL_TRESHOLD:
        {
            tIp6mcNpWrFsNpIpv6SetMIfaceTtlTreshold *pEntry = NULL;
            pEntry = &pIp6mcNpModInfo->Ip6mcNpFsNpIpv6SetMIfaceTtlTreshold;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsNpIpv6SetMIfaceTtlTreshold (pEntry->u4VrId, pEntry->i4IfIndex,
                                              pEntry->i4TtlTreshold);
            break;
        }
        case FS_NP_IPV6_SET_M_IFACE_RATE_LIMIT:
        {
            tIp6mcNpWrFsNpIpv6SetMIfaceRateLimit *pEntry = NULL;
            pEntry = &pIp6mcNpModInfo->Ip6mcNpFsNpIpv6SetMIfaceRateLimit;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsNpIpv6SetMIfaceRateLimit (pEntry->u4VrId, pEntry->i4IfIndex,
                                            pEntry->i4RateLimit);
            break;
        }
        case FS_PIMV6_NP_INIT_HW:
        {
            u1RetVal = FsPimv6NpInitHw ();
            break;
        }
        case FS_PIMV6_NP_DE_INIT_HW:
        {
            u1RetVal = FsPimv6NpDeInitHw ();
            break;
        }
        case FS_NP_IPV6_MC_CLEAR_HIT_BIT:
        {
            tIp6mcNpWrFsNpIpv6McClearHitBit *pEntry = NULL;
            pEntry = &pIp6mcNpModInfo->Ip6mcNpFsNpIpv6McClearHitBit;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsNpIpv6McClearHitBit (pEntry->u2VlanId, pEntry->pSrcIpAddr,
                                       pEntry->pGrpAddr);
            break;
        }
#ifdef MBSM_WANTED
        case FS_NP_IPV6_MBSM_MC_INIT:
        {
            tIp6mcNpWrFsNpIpv6MbsmMcInit *pEntry = NULL;
            pEntry = &pIp6mcNpModInfo->Ip6mcNpFsNpIpv6MbsmMcInit;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsNpIpv6MbsmMcInit (pEntry->pSlotInfo);
            break;
        }
        case FS_NP_IPV6_MBSM_MC_ADD_ROUTE_ENTRY:
        {
            tIp6mcNpWrFsNpIpv6MbsmMcAddRouteEntry *pEntry = NULL;
            pEntry = &pIp6mcNpModInfo->Ip6mcNpFsNpIpv6MbsmMcAddRouteEntry;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsNpIpv6MbsmMcAddRouteEntry (pEntry->u4VrId, pEntry->pGrpAddr,
                                         pEntry->u4GrpPrefix,
                                         pEntry->pSrcIpAddr,
                                         pEntry->u4SrcIpPrefix,
                                         pEntry->u1CallerId, pEntry->rtEntry,
                                         pEntry->u2NoOfDownStreamIf,
                                         pEntry->pDownStreamIf,
                                         pEntry->pSlotInfo);
            break;
        }
        case FS_PIMV6_MBSM_NP_INIT_HW:
        {
            tIp6mcNpWrFsPimv6MbsmNpInitHw *pEntry = NULL;
            pEntry = &pIp6mcNpModInfo->Ip6mcNpFsPimv6MbsmNpInitHw;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsPimv6MbsmNpInitHw (pEntry->pSlotInfo);
            break;
        }
#endif 
#endif /* PIMV6_WANTED */
        default:
            u1RetVal = FNP_FAILURE;
            break;

    }                            /* switch */

    return (u1RetVal);
}
#endif
