/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsipv6lw.c,v 1.76 2017/12/19 13:41:55 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
#include "ip6inc.h"
#include "ip6cli.h"
#include "rmgr.h"
#include "fsmsipcli.h"
#include "fsmpipv6cli.h"
#include "fsmpipvxcli.h"
#include "fsssl.h"

extern INT4         i4Ip6unicastId;
extern UINT4        Fsipv6PrefixProfileIndex[13];
extern UINT4        Fsipv6SupportEmbeddedRp[13];
extern UINT4        Fsipv6PrefixAdminStatus[13];
extern UINT4        FsMIIpv6IfNDProxyAdminStatus[13];
extern UINT4        FsMIIpv6IfNDProxyMode[13];
extern UINT4        FsMIIpv6IfNDProxyUpStream[13];
extern UINT4        Fsipv6ECMPPRTTimer[13];
extern UINT4        FsMIStdInetCidrRoutePreference[12];

extern INT1 nmhGetFsRtm6StaticRouteDistance ARG_LIST ((INT4 *));
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsipv6NdCacheMaxRetries
 Input       :  The Indices

                The Object 
                retValFsipv6NdCacheMaxRetries
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6NdCacheMaxRetries (INT4 *pi4RetValFsipv6NdCacheMaxRetries)
{
    if (gIp6GblInfo.pIp6CurrCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsipv6NdCacheMaxRetries =
        gIp6GblInfo.pIp6CurrCxt->i4Nd6CacheMaxRetries;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6PmtuConfigStatus
 Input       :  The Indices

                The Object 
                retValFsipv6PmtuConfigStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6PmtuConfigStatus (INT4 *pi4RetValFsipv6PmtuConfigStatus)
{
    if (gIp6GblInfo.pIp6CurrCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsipv6PmtuConfigStatus = gIp6GblInfo.pIp6CurrCxt->u1PmtuEnable;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6PmtuTimeOutInterval
 Input       :  The Indices

                The Object 
                retValFsipv6PmtuTimeOutInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6PmtuTimeOutInterval (UINT4 *pu4RetValFsipv6PmtuTimeOutInterval)
{
    if (gIp6GblInfo.pIp6CurrCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsipv6PmtuTimeOutInterval = gIp6GblInfo.pIp6CurrCxt->u4CfgTimeOut;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6JumboEnable
 Input       :  The Indices

                The Object 
                retValFsipv6JumboEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6JumboEnable (INT4 *pi4RetValFsipv6JumboEnable)
{
    if (gIp6GblInfo.pIp6CurrCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsipv6JumboEnable = gIp6GblInfo.pIp6CurrCxt->u1JmbCfgStatus;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6NumOfSendJumbo
 Input       :  The Indices

                The Object 
                retValFsipv6NumOfSendJumbo
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6NumOfSendJumbo (INT4 *pi4RetValFsipv6NumOfSendJumbo)
{
    if (gIp6GblInfo.pIp6CurrCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsipv6NumOfSendJumbo = gIp6GblInfo.pIp6CurrCxt->u2JmbSendPkts;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6NumOfRecvJumbo
 Input       :  The Indices

                The Object 
                retValFsipv6NumOfRecvJumbo
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6NumOfRecvJumbo (INT4 *pi4RetValFsipv6NumOfRecvJumbo)
{
    if (gIp6GblInfo.pIp6CurrCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsipv6NumOfRecvJumbo = gIp6GblInfo.pIp6CurrCxt->u2JmbRecdPkts;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6ErrJumbo
 Input       :  The Indices

                The Object 
                retValFsipv6ErrJumbo
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6ErrJumbo (INT4 *pi4RetValFsipv6ErrJumbo)
{
    if (gIp6GblInfo.pIp6CurrCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsipv6ErrJumbo = gIp6GblInfo.pIp6CurrCxt->u2JmbErrPkts;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6GlobalDebug
 Input       :  The Indices

                The Object 
                retValFsipv6GlobalDebug
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6GlobalDebug (UINT4 *pu4RetValFsipv6GlobalDebug)
{
    *pu4RetValFsipv6GlobalDebug = gIp6GblInfo.u4GlbIp6Dbg;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6MaxRouteEntries
 Input       :  The Indices

                The Object 
                retValFsipv6MaxRouteEntries
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6MaxRouteEntries (UINT4 *pu4RetValFsipv6MaxRouteEntries)
{
    *pu4RetValFsipv6MaxRouteEntries = (UINT4) MAX_RTM6_ROUTE_TABLE_ENTRIES;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6MaxLogicalIfaces
 Input       :  The Indices

                The Object 
                retValFsipv6MaxLogicalIfaces
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6MaxLogicalIfaces (UINT4 *pu4RetValFsipv6MaxLogicalIfaces)
{
    *pu4RetValFsipv6MaxLogicalIfaces = (UINT4) IP6_MAX_LOGICAL_IFACES;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6MaxTunnelIfaces
 Input       :  The Indices

                The Object 
                retValFsipv6MaxTunnelIfaces
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6MaxTunnelIfaces (UINT4 *pu4RetValFsipv6MaxTunnelIfaces)
{
#ifdef TUNNEL_WANTED
    *pu4RetValFsipv6MaxTunnelIfaces = MAX_IP6_TUNNEL_INTERFACES;
    return SNMP_SUCCESS;
#else
    *pu4RetValFsipv6MaxTunnelIfaces = 0;
    return SNMP_SUCCESS;
#endif
}

/****************************************************************************
 Function    :  nmhGetFsipv6MaxAddresses
 Input       :  The Indices

                The Object 
                retValFsipv6MaxAddresses
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6MaxAddresses (UINT4 *pu4RetValFsipv6MaxAddresses)
{
    *pu4RetValFsipv6MaxAddresses = MAX_IP6_UNICAST_ADDRESS;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6MaxFragReasmEntries
 Input       :  The Indices

                The Object 
                retValFsipv6MaxFragReasmEntries
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6MaxFragReasmEntries (UINT4 *pu4RetValFsipv6MaxFragReasmEntries)
{
    *pu4RetValFsipv6MaxFragReasmEntries = MAX_IP6_REASM_ENTRIES;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6Nd6MaxCacheEntries
 Input       :  The Indices

                The Object 
                retValFsipv6Nd6MaxCacheEntries
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6Nd6MaxCacheEntries (UINT4 *pu4RetValFsipv6Nd6MaxCacheEntries)
{
    *pu4RetValFsipv6Nd6MaxCacheEntries = MAX_IP6_ND6_CACHE_ENTRIES;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6PmtuMaxDest
 Input       :  The Indices

                The Object 
                retValFsipv6PmtuMaxDest
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6PmtuMaxDest (UINT4 *pu4RetValFsipv6PmtuMaxDest)
{
    *pu4RetValFsipv6PmtuMaxDest = MAX_IP6_PMTU_ENTRIES;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6RFC5095Compatibility
 Input       :  The Indices

                The Object 
                retValFsipv6RFC5095Compatibility
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6RFC5095Compatibility (INT4 *pi4RetValFsipv6RFC5095Compatibility)
{
    *pi4RetValFsipv6RFC5095Compatibility =
        (INT4) gIp6GblInfo.u1RFC5095Compatibility;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6RFC5942Compatibility
 Input       :  The Indices

                The Object
                retValFsipv6RFC5942Compatibility
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6RFC5942Compatibility (INT4 *pi4RetValFsipv6RFC5942Compatibility)
{
    *pi4RetValFsipv6RFC5942Compatibility =
        (INT4) gIp6GblInfo.u1RFC5942Compatibility;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6SEND
 Input       :  The Indices

                The Object 
                retValFsipv6SENDSecLevel
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SENDSecLevel (INT4 *pi4RetValFsipv6SENDSecLevel)
{
    *pi4RetValFsipv6SENDSecLevel = gNd6GblInfo.u1Nd6SeNDSec;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6SENDNbrSecLevel
 Input       :  The Indices

                The Object 
                retValFsipv6SENDNbrSecLevel
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SENDNbrSecLevel (INT4 *pi4RetValFsipv6SENDNbrSecLevel)
{
    *pi4RetValFsipv6SENDNbrSecLevel = gNd6GblInfo.u1Nd6SeNDNbrSecLevel;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6SENDAuthType
 Input       :  The Indices

                The Object 
                retValFsipv6SENDAuthType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SENDAuthType (INT4 *pi4RetValFsipv6SENDAuthType)
{
    *pi4RetValFsipv6SENDAuthType = gNd6GblInfo.u1Nd6SeNDAuthType;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6SENDMinBits
 Input       :  The Indices

                The Object 
                retValFsipv6SENDMinBits
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SENDMinBits (INT4 *pi4RetValFsipv6SENDMinBits)
{
    *pi4RetValFsipv6SENDMinBits = (INT4) gNd6GblInfo.u4Nd6SeNDMinKeyLength;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6SENDSecDAD
 Input       :  The Indices

                The Object 
                retValFsipv6SENDSecDAD
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SENDSecDAD (INT4 *pi4RetValFsipv6SENDSecDAD)
{
    *pi4RetValFsipv6SENDSecDAD = (INT4) gNd6GblInfo.u4Nd6SeNDAcceptUnsecure;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6SENDPrefixChk
 Input       :  The Indices

                The Object 
                retValFsipv6SENDPrefixChk
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SENDPrefixChk (INT4 *pi4RetValFsipv6SENDPrefixChk)
{
    *pi4RetValFsipv6SENDPrefixChk = gNd6GblInfo.u4Nd6SeNDPrefixCheck;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6ECMPPRTTimer
 Input       :  The Indices

                The Object 
                retValFsipv6ECMPPRTTimer
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6ECMPPRTTimer (INT4 *pi4RetValFsipv6ECMPPRTTimer)
{
    *pi4RetValFsipv6ECMPPRTTimer = gIp6GblInfo.i4ECMPPRTTInterval;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6NdCacheTimeout
 Input       :  The Indices

                The Object
                retValFsipv6NdCacheTimeout
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6NdCacheTimeout (INT4 *pi4RetValFsipv6NdCacheTimeout)
{
    *pi4RetValFsipv6NdCacheTimeout = gIp6GblInfo.i4NdCacheTimeout;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsipv6NdCacheMaxRetries
 Input       :  The Indices

                The Object 
                setValFsipv6NdCacheMaxRetries
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6NdCacheMaxRetries (INT4 i4SetValFsipv6NdCacheMaxRetries)
{
    if (gIp6GblInfo.pIp6CurrCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    gIp6GblInfo.pIp6CurrCxt->i4Nd6CacheMaxRetries =
        i4SetValFsipv6NdCacheMaxRetries;

    IncMsrForIpv6ContextTable (gIp6GblInfo.pIp6CurrCxt->u4ContextId,
                               'i', &i4SetValFsipv6NdCacheMaxRetries,
                               FsMIIpv6NdCacheMaxRetries,
                               sizeof (FsMIIpv6NdCacheMaxRetries) /
                               sizeof (UINT4));
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsipv6PmtuConfigStatus
 Input       :  The Indices

                The Object 
                setValFsipv6PmtuConfigStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6PmtuConfigStatus (INT4 i4SetValFsipv6PmtuConfigStatus)
{
    if (gIp6GblInfo.pIp6CurrCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    gIp6GblInfo.pIp6CurrCxt->u1PmtuEnable =
        (UINT1) i4SetValFsipv6PmtuConfigStatus;
    IncMsrForIpv6ContextTable (gIp6GblInfo.pIp6CurrCxt->u4ContextId,
                               'i', &i4SetValFsipv6PmtuConfigStatus,
                               FsMIIpv6PmtuConfigStatus,
                               sizeof (FsMIIpv6PmtuConfigStatus) /
                               sizeof (UINT4));

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6PmtuTimeOutInterval
 Input       :  The Indices

                The Object 
                setValFsipv6PmtuTimeOutInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6PmtuTimeOutInterval (UINT4 u4SetValFsipv6PmtuTimeOutInterval)
{
    if (gIp6GblInfo.pIp6CurrCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    gIp6GblInfo.pIp6CurrCxt->u4CfgTimeOut = u4SetValFsipv6PmtuTimeOutInterval;
    IncMsrForIpv6ContextTable (gIp6GblInfo.pIp6CurrCxt->u4ContextId,
                               'u', &u4SetValFsipv6PmtuTimeOutInterval,
                               FsMIIpv6PmtuTimeOutInterval,
                               sizeof (FsMIIpv6PmtuTimeOutInterval) /
                               sizeof (UINT4));

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6JumboEnable
 Input       :  The Indices

                The Object 
                setValFsipv6JumboEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6JumboEnable (INT4 i4SetValFsipv6JumboEnable)
{
    if (gIp6GblInfo.pIp6CurrCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    gIp6GblInfo.pIp6CurrCxt->u1JmbCfgStatus = (UINT1) i4SetValFsipv6JumboEnable;
    IncMsrForIpv6ContextTable (gIp6GblInfo.pIp6CurrCxt->u4ContextId,
                               'i', &i4SetValFsipv6JumboEnable,
                               FsMIIpv6JumboEnable,
                               sizeof (FsMIIpv6JumboEnable) / sizeof (UINT4));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6GlobalDebug
 Input       :  The Indices

                The Object 
                setValFsipv6GlobalDebug
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6GlobalDebug (UINT4 u4SetValFsipv6GlobalDebug)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = IPVX_ZERO;
    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    gIp6GblInfo.u4GlbIp6Dbg = u4SetValFsipv6GlobalDebug;
    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = FsMIIpv6GlobalDebug;
    SnmpNotifyInfo.u4OidLen = sizeof (FsMIIpv6GlobalDebug) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = Ip6Lock;
    SnmpNotifyInfo.pUnLockPointer = Ip6UnLock;
    SnmpNotifyInfo.u4Indices = IP6_ZERO;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u", u4SetValFsipv6GlobalDebug));

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6MaxRouteEntries
 Input       :  The Indices

                The Object 
                setValFsipv6MaxRouteEntries
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6MaxRouteEntries (UINT4 u4SetValFsipv6MaxRouteEntries)
{
    /* DEPRECATED OBJECT */
    tIp6SystemSize      Ip6SystemSizeInfo;

    /* Get the previous value */
    GetIp6SizingParams (&Ip6SystemSizeInfo);

    /* Modify the No. of Route Entries */
    Ip6SystemSizeInfo.u4Ip6MaxRoutes = u4SetValFsipv6MaxRouteEntries;

    /* Set the values */
    SetIp6SizingParams (&Ip6SystemSizeInfo);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6MaxLogicalIfaces
 Input       :  The Indices

                The Object 
                setValFsipv6MaxLogicalIfaces
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6MaxLogicalIfaces (UINT4 u4SetValFsipv6MaxLogicalIfaces)
{
    /* DEPRECATED OBJECT */
    tIp6SystemSize      Ip6SystemSizeInfo;

    /* Get the previous value */
    GetIp6SizingParams (&Ip6SystemSizeInfo);

    /* Modify the No. of Route Entries */
    Ip6SystemSizeInfo.i4Ip6MaxLogicalIfaces = u4SetValFsipv6MaxLogicalIfaces;

    /* Set the values */
    SetIp6SizingParams (&Ip6SystemSizeInfo);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6MaxTunnelIfaces
 Input       :  The Indices

                The Object 
                setValFsipv6MaxTunnelIfaces
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6MaxTunnelIfaces (UINT4 u4SetValFsipv6MaxTunnelIfaces)
{
    /* DEPRECATED OBJECT */
#ifdef TUNNEL_WANTED
    tIp6SystemSize      Ip6SystemSizeInfo;

    /* Get the previous value */
    GetIp6SizingParams (&Ip6SystemSizeInfo);

    /* Modify the No. of Route Entries */
    Ip6SystemSizeInfo.u4Ip6MaxTunnelIfaces = u4SetValFsipv6MaxTunnelIfaces;

    /* Set the values */
    SetIp6SizingParams (&Ip6SystemSizeInfo);

    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (u4SetValFsipv6MaxTunnelIfaces);
    return SNMP_FAILURE;
#endif
}

/****************************************************************************
 Function    :  nmhSetFsipv6MaxAddresses
 Input       :  The Indices

                The Object 
                setValFsipv6MaxAddresses
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6MaxAddresses (UINT4 u4SetValFsipv6MaxAddresses)
{
    /* DEPRECATED OBJECT */
    tIp6SystemSize      Ip6SystemSizeInfo;

    /* Get the previous value */
    GetIp6SizingParams (&Ip6SystemSizeInfo);

    /* Modify the No. of Route Entries */
    Ip6SystemSizeInfo.u4Ip6MaxAddr = u4SetValFsipv6MaxAddresses;

    /* Set the values */
    SetIp6SizingParams (&Ip6SystemSizeInfo);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6MaxFragReasmEntries
 Input       :  The Indices

                The Object 
                setValFsipv6MaxFragReasmEntries
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6MaxFragReasmEntries (UINT4 u4SetValFsipv6MaxFragReasmEntries)
{
    /* DEPRECATED OBJECT */
    tIp6SystemSize      Ip6SystemSizeInfo;

    /* Get the previous value */
    GetIp6SizingParams (&Ip6SystemSizeInfo);

    /* Modify the No. of Route Entries */
    Ip6SystemSizeInfo.u4Ip6MaxFragReasmList = u4SetValFsipv6MaxFragReasmEntries;

    /* Set the values */
    SetIp6SizingParams (&Ip6SystemSizeInfo);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6Nd6MaxCacheEntries
 Input       :  The Indices

                The Object 
                setValFsipv6Nd6MaxCacheEntries
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6Nd6MaxCacheEntries (UINT4 u4SetValFsipv6Nd6MaxCacheEntries)
{
    /* DEPRECATED OBJECT */
    tIp6SystemSize      Ip6SystemSizeInfo;

    /* Get the previous value */
    GetIp6SizingParams (&Ip6SystemSizeInfo);

    /* Modify the No. of Route Entries */
    Ip6SystemSizeInfo.u4Ip6MaxCacheEntries = u4SetValFsipv6Nd6MaxCacheEntries;

    /* Set the values */
    SetIp6SizingParams (&Ip6SystemSizeInfo);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6PmtuMaxDest
 Input       :  The Indices

                The Object 
                setValFsipv6PmtuMaxDest
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6PmtuMaxDest (UINT4 u4SetValFsipv6PmtuMaxDest)
{
    /* DEPRECATED OBJECT */
    tIp6SystemSize      Ip6SystemSizeInfo;

    /* Get the previous value */
    GetIp6SizingParams (&Ip6SystemSizeInfo);

    /* Modify the No. of Route Entries */
    Ip6SystemSizeInfo.u4Ip6MaxPmtuEntries = u4SetValFsipv6PmtuMaxDest;

    /* Set the values */
    SetIp6SizingParams (&Ip6SystemSizeInfo);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6RFC5095Compatibility
 Input       :  The Indices

                The Object 
                setValFsipv6RFC5095Compatibility
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6RFC5095Compatibility (INT4 i4SetValFsipv6RFC5095Compatibility)
{
    gIp6GblInfo.u1RFC5095Compatibility =
        (UINT1) i4SetValFsipv6RFC5095Compatibility;

    IncMsrForIpv6ContextTable (gIp6GblInfo.pIp6CurrCxt->u4ContextId, 'i',
                               &i4SetValFsipv6RFC5095Compatibility,
                               FsMIIpv6RFC5095Compatibility,
                               sizeof (FsMIIpv6RFC5095Compatibility) /
                               sizeof (UINT4));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6RFC5942Compatibility
 Input       :  The Indices

                The Object
                setValFsipv6RFC5942Compatibility
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6RFC5942Compatibility (INT4 i4SetValFsipv6RFC5942Compatibility)
{
    UNUSED_PARAM (i4SetValFsipv6RFC5942Compatibility);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6SENDSecLevel
 Input       :  The Indices

                The Object 
                setValFsipv6SENDSecLevel
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6SENDSecLevel (INT4 i4SetValFsipv6SENDSecLevel)
{
    gNd6GblInfo.u1Nd6SeNDSec = (UINT1) i4SetValFsipv6SENDSecLevel;
    IncMsrForIpv6ContextTable (gIp6GblInfo.pIp6CurrCxt->u4ContextId,
                               'i', &i4SetValFsipv6SENDSecLevel,
                               FsMIIpv6SENDSecLevel,
                               sizeof (FsMIIpv6SENDSecLevel) / sizeof (UINT4));
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsipv6SENDNbrSecLevel
 Input       :  The Indices

                The Object 
                setValFsipv6SENDNbrSecLevel
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6SENDNbrSecLevel (INT4 i4SetValFsipv6SENDNbrSecLevel)
{
    gNd6GblInfo.u1Nd6SeNDNbrSecLevel = (UINT1) i4SetValFsipv6SENDNbrSecLevel;
    IncMsrForIpv6ContextTable (gIp6GblInfo.pIp6CurrCxt->u4ContextId,
                               'i', &i4SetValFsipv6SENDNbrSecLevel,
                               FsMIIpv6SENDNbrSecLevel,
                               sizeof (FsMIIpv6SENDNbrSecLevel) /
                               sizeof (UINT4));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6SENDAuthType
 Input       :  The Indices

                The Object 
                setValFsipv6SENDAuthType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6SENDAuthType (INT4 i4SetValFsipv6SENDAuthType)
{
    gNd6GblInfo.u1Nd6SeNDAuthType = (UINT1) i4SetValFsipv6SENDAuthType;
    IncMsrForIpv6ContextTable (gIp6GblInfo.pIp6CurrCxt->u4ContextId,
                               'i', &i4SetValFsipv6SENDAuthType,
                               FsMIIpv6SENDAuthType,
                               sizeof (FsMIIpv6SENDAuthType) / sizeof (UINT4));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6SENDMinBits
 Input       :  The Indices

                The Object 
                setValFsipv6SENDMinBits
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6SENDMinBits (INT4 i4SetValFsipv6SENDMinBits)
{
    gNd6GblInfo.u4Nd6SeNDMinKeyLength = (UINT4) i4SetValFsipv6SENDMinBits;
    IncMsrForIpv6ContextTable (gIp6GblInfo.pIp6CurrCxt->u4ContextId,
                               'i', &i4SetValFsipv6SENDMinBits,
                               FsMIIpv6SENDMinBits,
                               sizeof (FsMIIpv6SENDMinBits) / sizeof (UINT4));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6SENDSecDAD
 Input       :  The Indices

                The Object 
                setValFsipv6SENDSecDAD
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6SENDSecDAD (INT4 i4SetValFsipv6SENDSecDAD)
{
    gNd6GblInfo.u4Nd6SeNDAcceptUnsecure = (UINT4) i4SetValFsipv6SENDSecDAD;
    IncMsrForIpv6ContextTable (gIp6GblInfo.pIp6CurrCxt->u4ContextId,
                               'i', &i4SetValFsipv6SENDSecDAD,
                               FsMIIpv6SENDSecDAD,
                               sizeof (FsMIIpv6SENDSecDAD) / sizeof (UINT4));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6SENDPrefixChk
 Input       :  The Indices

                The Object 
                setValFsipv6SENDPrefixChk
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6SENDPrefixChk (INT4 i4SetValFsipv6SENDPrefixChk)
{
    gNd6GblInfo.u4Nd6SeNDPrefixCheck = (UINT4) i4SetValFsipv6SENDPrefixChk;
    IncMsrForIpv6ContextTable (gIp6GblInfo.pIp6CurrCxt->u4ContextId,
                               'i', &i4SetValFsipv6SENDPrefixChk,
                               FsMIIpv6SENDPrefixChk,
                               sizeof (FsMIIpv6SENDPrefixChk) / sizeof (UINT4));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6ECMPPRTTimer
 Input       :  The Indices

                The Object 
                setValFsipv6ECMPPRTTimer
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6ECMPPRTTimer (INT4 i4SetValFsipv6ECMPPRTTimer)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = IPVX_ZERO;
    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    gIp6GblInfo.i4ECMPPRTTInterval = i4SetValFsipv6ECMPPRTTimer;
    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = Fsipv6ECMPPRTTimer;
    SnmpNotifyInfo.u4OidLen = sizeof (Fsipv6ECMPPRTTimer) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = Ip6Lock;
    SnmpNotifyInfo.pUnLockPointer = Ip6UnLock;
    SnmpNotifyInfo.u4Indices = IP6_TWO;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%d", i4SetValFsipv6ECMPPRTTimer));
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsipv6NdCacheTimeout
 Input       :  The Indices

                The Object
                setValFsipv6NdCacheTimeout
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6NdCacheTimeout (INT4 i4SetValFsipv6NdCacheTimeout)
{
    gIp6GblInfo.i4NdCacheTimeout = i4SetValFsipv6NdCacheTimeout;
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Fsipv6NdCacheMaxRetries
 Input       :  The Indices

                The Object 
                testValFsipv6NdCacheMaxRetries
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6NdCacheMaxRetries (UINT4 *pu4ErrorCode,
                                  INT4 i4TestValFsipv6NdCacheMaxRetries)
{
    if (gIp6GblInfo.pIp6CurrCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    if (i4TestValFsipv6NdCacheMaxRetries < ND6_MIN_CACHE_SOLICIT_RETRIES ||
        i4TestValFsipv6NdCacheMaxRetries > ND6_MAX_CACHE_SOLICIT_RETRIES)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6PmtuConfigStatus
 Input       :  The Indices

                The Object 
                testValFsipv6PmtuConfigStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6PmtuConfigStatus (UINT4 *pu4ErrorCode,
                                 INT4 i4TestValFsipv6PmtuConfigStatus)
{
    if (gIp6GblInfo.pIp6CurrCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if ((i4TestValFsipv6PmtuConfigStatus == IP6_PMTU_ENABLE) ||
        (i4TestValFsipv6PmtuConfigStatus == IP6_PMTU_DISABLE))
    {
        return SNMP_SUCCESS;
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6PmtuTimeOutInterval
 Input       :  The Indices

                The Object 
                testValFsipv6PmtuTimeOutInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6PmtuTimeOutInterval (UINT4 *pu4ErrorCode,
                                    UINT4 u4TestValFsipv6PmtuTimeOutInterval)
{
    if (gIp6GblInfo.pIp6CurrCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (u4TestValFsipv6PmtuTimeOutInterval > IP6_MAX_VALUE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6JumboEnable
 Input       :  The Indices

                The Object 
                testValFsipv6JumboEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6JumboEnable (UINT4 *pu4ErrorCode,
                            INT4 i4TestValFsipv6JumboEnable)
{
    if (gIp6GblInfo.pIp6CurrCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if ((i4TestValFsipv6JumboEnable != IP6_JUMBO_ENABLE)
        && (i4TestValFsipv6JumboEnable != IP6_JUMBO_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6GlobalDebug
 Input       :  The Indices

                The Object 
                testValFsipv6GlobalDebug
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6GlobalDebug (UINT4 *pu4ErrorCode,
                            UINT4 u4TestValFsipv6GlobalDebug)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4TestValFsipv6GlobalDebug);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6MaxRouteEntries
 Input       :  The Indices

                The Object 
                testValFsipv6MaxRouteEntries
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6MaxRouteEntries (UINT4 *pu4ErrorCode,
                                UINT4 u4TestValFsipv6MaxRouteEntries)
{
    if (u4TestValFsipv6MaxRouteEntries < IP6_MIN_ROUTE_ENTRIES)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6MaxLogicalIfaces
 Input       :  The Indices

                The Object 
                testValFsipv6MaxLogicalIfaces
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6MaxLogicalIfaces (UINT4 *pu4ErrorCode,
                                 UINT4 u4TestValFsipv6MaxLogicalIfaces)
{
    if (u4TestValFsipv6MaxLogicalIfaces <= 0)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6MaxTunnelIfaces
 Input       :  The Indices

                The Object 
                testValFsipv6MaxTunnelIfaces
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6MaxTunnelIfaces (UINT4 *pu4ErrorCode,
                                UINT4 u4TestValFsipv6MaxTunnelIfaces)
{
#ifdef TUNNEL_WANTED
    if (u4TestValFsipv6MaxTunnelIfaces <= 0)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4TestValFsipv6MaxTunnelIfaces);
    return SNMP_FAILURE;
#endif
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6MaxAddresses
 Input       :  The Indices

                The Object 
                testValFsipv6MaxAddresses
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6MaxAddresses (UINT4 *pu4ErrorCode,
                             UINT4 u4TestValFsipv6MaxAddresses)
{
    if (u4TestValFsipv6MaxAddresses <= 0)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6MaxFragReasmEntries
 Input       :  The Indices

                The Object 
                testValFsipv6MaxFragReasmEntries
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6MaxFragReasmEntries (UINT4 *pu4ErrorCode,
                                    UINT4 u4TestValFsipv6MaxFragReasmEntries)
{
    if (u4TestValFsipv6MaxFragReasmEntries <= 0)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6Nd6MaxCacheEntries
 Input       :  The Indices

                The Object 
                testValFsipv6Nd6MaxCacheEntries
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6Nd6MaxCacheEntries (UINT4 *pu4ErrorCode,
                                   UINT4 u4TestValFsipv6Nd6MaxCacheEntries)
{
    if (u4TestValFsipv6Nd6MaxCacheEntries <= 0)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6PmtuMaxDest
 Input       :  The Indices

                The Object 
                testValFsipv6PmtuMaxDest
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6PmtuMaxDest (UINT4 *pu4ErrorCode,
                            UINT4 u4TestValFsipv6PmtuMaxDest)
{
    if (u4TestValFsipv6PmtuMaxDest <= 0)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6RFC5095Compatibility
 Input       :  The Indices

                The Object 
                testValFsipv6RFC5095Compatibility
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6RFC5095Compatibility (UINT4 *pu4ErrorCode,
                                     INT4 i4TestValFsipv6RFC5095Compatibility)
{
    if ((i4TestValFsipv6RFC5095Compatibility != IP6_RFC5095_COMPATIBLE) &&
        (i4TestValFsipv6RFC5095Compatibility != IP6_RFC5095_NOT_COMPATIBLE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6RFC5942Compatibility
 Input       :  The Indices

                The Object
                testValFsipv6RFC5942Compatibility
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6RFC5942Compatibility (UINT4 *pu4ErrorCode,
                                     INT4 i4TestValFsipv6RFC5942Compatibility)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4TestValFsipv6RFC5942Compatibility);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6SENDSecLevel
 Input       :  The Indices

                The Object 
                testValFsipv6SENDSecLevel
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6SENDSecLevel (UINT4 *pu4ErrorCode,
                             INT4 i4TestValFsipv6SENDSecLevel)
{
    if ((i4TestValFsipv6SENDSecLevel < 0) || (i4TestValFsipv6SENDSecLevel > 1))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6SENDNbrSecLevel
 Input       :  The Indices

                The Object 
                testValFsipv6SENDNbrSecLevel
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6SENDNbrSecLevel (UINT4 *pu4ErrorCode,
                                INT4 i4TestValFsipv6SENDNbrSecLevel)
{
    if ((i4TestValFsipv6SENDNbrSecLevel < 0) ||
        (i4TestValFsipv6SENDNbrSecLevel > 7))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6SENDAuthType
 Input       :  The Indices

                The Object 
                testValFsipv6SENDAuthType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6SENDAuthType (UINT4 *pu4ErrorCode,
                             INT4 i4TestValFsipv6SENDAuthType)
{
    if ((i4TestValFsipv6SENDAuthType < 0) || (i4TestValFsipv6SENDAuthType > 3))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (i4TestValFsipv6SENDAuthType != 0)
    {
        CLI_SET_ERR (CLI_IP6_SEND_AUTH_TYPE_NOT_ALLOWED);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6SENDMinBits
 Input       :  The Indices

                The Object 
                testValFsipv6SENDMinBits
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6SENDMinBits (UINT4 *pu4ErrorCode,
                            INT4 i4TestValFsipv6SENDMinBits)
{
    if ((i4TestValFsipv6SENDMinBits != 512) &&
        (i4TestValFsipv6SENDMinBits != 1024))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6SENDSecDAD
 Input       :  The Indices

                The Object 
                testValFsipv6SENDSecDAD
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6SENDSecDAD (UINT4 *pu4ErrorCode, INT4 i4TestValFsipv6SENDSecDAD)
{
    if ((i4TestValFsipv6SENDSecDAD != 1) && (i4TestValFsipv6SENDSecDAD != 2))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6SENDPrefixChk
 Input       :  The Indices

                The Object 
                testValFsipv6SENDPrefixChk
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6SENDPrefixChk (UINT4 *pu4ErrorCode,
                              INT4 i4TestValFsipv6SENDPrefixChk)
{
    if ((i4TestValFsipv6SENDPrefixChk != 1) &&
        (i4TestValFsipv6SENDPrefixChk != 2))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6ECMPPRTTimer
 Input       :  The Indices

                The Object 
                testValFsipv6ECMPPRTTimer
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6ECMPPRTTimer (UINT4 *pu4ErrorCode,
                             INT4 i4TestValFsipv6ECMPPRTTimer)
{
    if ((i4TestValFsipv6ECMPPRTTimer < IP6_ECMP_MIN_PRT_INTERVAL) ||
        (i4TestValFsipv6ECMPPRTTimer > IP6_ECMP_MAX_PRT_INTERVAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6NdCacheTimeout
 Input       :  The Indices

                The Object
                testValFsipv6NdCacheTimeout
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6NdCacheTimeout (UINT4 *pu4ErrorCode,
                               INT4 i4TestValFsipv6NdCacheTimeout)
{
    if (i4TestValFsipv6NdCacheTimeout == IP6_ZERO)
    {
        return SNMP_FAILURE;
    }

    if ((i4TestValFsipv6NdCacheTimeout < ND_MIN_CACHE_TIMEOUT)
        || (i4TestValFsipv6NdCacheTimeout > ND_MAX_CACHE_TIMEOUT))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_TOUT_ERR);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Fsipv6NdCacheMaxRetries
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsipv6NdCacheMaxRetries (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Fsipv6PmtuConfigStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsipv6PmtuConfigStatus (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Fsipv6PmtuTimeOutInterval
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsipv6PmtuTimeOutInterval (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Fsipv6JumboEnable
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsipv6JumboEnable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Fsipv6GlobalDebug
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsipv6GlobalDebug (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Fsipv6MaxRouteEntries
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsipv6MaxRouteEntries (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Fsipv6MaxLogicalIfaces
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsipv6MaxLogicalIfaces (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Fsipv6MaxTunnelIfaces
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsipv6MaxTunnelIfaces (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Fsipv6MaxAddresses
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsipv6MaxAddresses (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Fsipv6MaxFragReasmEntries
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsipv6MaxFragReasmEntries (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Fsipv6Nd6MaxCacheEntries
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsipv6Nd6MaxCacheEntries (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Fsipv6PmtuMaxDest
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsipv6PmtuMaxDest (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Fsipv6RFC5095Compatibility
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsipv6RFC5095Compatibility (UINT4 *pu4ErrorCode,
                                    tSnmpIndexList * pSnmpIndexList,
                                    tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Fsipv6RFC5942Compatibility
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsipv6RFC5942Compatibility (UINT4 *pu4ErrorCode,
                                    tSnmpIndexList * pSnmpIndexList,
                                    tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Fsipv6SENDSecLevel
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsipv6SENDSecLevel (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Fsipv6SENDNbrSecLevel
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsipv6SENDNbrSecLevel (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Fsipv6SENDAuthType
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsipv6SENDAuthType (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Fsipv6SENDMinBits
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsipv6SENDMinBits (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Fsipv6SENDSecDAD
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsipv6SENDSecDAD (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Fsipv6SENDPrefixChk
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsipv6SENDPrefixChk (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Fsipv6ECMPPRTTimer
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsipv6ECMPPRTTimer (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Fsipv6NdCacheTimeout
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsipv6NdCacheTimeout (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Fsipv6IfTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsipv6IfTable
 Input       :  The Indices
                Fsipv6IfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsipv6IfTable (INT4 i4Fsipv6IfIndex)
{
    if (Ip6ifEntryExists ((UINT4) i4Fsipv6IfIndex) == IP6_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsipv6IfTable
 Input       :  The Indices
                Fsipv6IfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsipv6IfTable (INT4 *pi4Fsipv6IfIndex)
{
    *pi4Fsipv6IfIndex = 1;

    if (Ip6ifEntryExists ((UINT4) *pi4Fsipv6IfIndex) == IP6_SUCCESS)
    {
        return (SNMP_SUCCESS);
    }

    if (Ip6ifGetNextIndex ((UINT4 *) pi4Fsipv6IfIndex) == IP6_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsipv6IfTable
 Input       :  The Indices
                Fsipv6IfIndex
                nextFsipv6IfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsipv6IfTable (INT4 i4Fsipv6IfIndex, INT4 *pi4NextFsipv6IfIndex)
{
    INT4                i4Index = (i4Fsipv6IfIndex) + 1;

    if ((i4Fsipv6IfIndex < 0) || (i4Index < 0))
    {
        return (nmhGetFirstIndexFsipv6IfTable (pi4NextFsipv6IfIndex));
    }

    IP6_IF_SCAN (i4Index, (INT4) IP6_MAX_LOGICAL_IF_INDEX)
    {
        if (Ip6ifEntryExists ((UINT4) i4Index) == IP6_SUCCESS)
        {
            *pi4NextFsipv6IfIndex = i4Index;
            return (SNMP_SUCCESS);
        }
    }

    return (SNMP_FAILURE);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsipv6IfType
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                retValFsipv6IfType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfType (INT4 i4Fsipv6IfIndex, INT4 *pi4RetValFsipv6IfType)
{
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsipv6IfType = pIf6->u1IfType;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfPortNum
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                retValFsipv6IfPortNum
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfPortNum (INT4 i4Fsipv6IfIndex, INT4 *pi4RetValFsipv6IfPortNum)
{
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsipv6IfPortNum = (INT4) pIf6->u4Index;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfCircuitNum
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                retValFsipv6IfCircuitNum
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfCircuitNum (INT4 i4Fsipv6IfIndex,
                          INT4 *pi4RetValFsipv6IfCircuitNum)
{
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsipv6IfCircuitNum = pIf6->u2CktIndex;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfToken
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                retValFsipv6IfToken
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfToken (INT4 i4Fsipv6IfIndex,
                     tSNMP_OCTET_STRING_TYPE * pRetValFsipv6IfToken)
{
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);
    UINT1               tokLen;
    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    tokLen = MEM_MAX_BYTES (pIf6->u1TokLen, IP6_EUI_ADDRESS_LEN);
    if (tokLen != 0)
    {
        MEMCPY (pRetValFsipv6IfToken->pu1_OctetList, pIf6->ifaceTok, tokLen);
    }
    pRetValFsipv6IfToken->i4_Length = tokLen;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfAdminStatus
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                retValFsipv6IfAdminStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfAdminStatus (INT4 i4Fsipv6IfIndex,
                           INT4 *pi4RetValFsipv6IfAdminStatus)
{
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsipv6IfAdminStatus = (INT4) pIf6->u1AdminStatus;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfOperStatus
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                retValFsipv6IfOperStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfOperStatus (INT4 i4Fsipv6IfIndex,
                          INT4 *pi4RetValFsipv6IfOperStatus)
{
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsipv6IfOperStatus = (INT4) pIf6->u1OperStatus;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfRouterAdvStatus
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                retValFsipv6IfRouterAdvStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfRouterAdvStatus (INT4 i4Fsipv6IfIndex,
                               INT4 *pi4RetValFsipv6IfRouterAdvStatus)
{
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pIf6->u1RaCnf & IP6_IF_RA_ADV)
    {
        *pi4RetValFsipv6IfRouterAdvStatus = IP6_IF_ROUT_ADV_ENABLED;
    }
    else
    {
        *pi4RetValFsipv6IfRouterAdvStatus = IP6_IF_ROUT_ADV_DISABLED;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsipv6IfRouterAdvFlags
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                retValFsipv6IfRouterAdvFlags
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfRouterAdvFlags (INT4 i4Fsipv6IfIndex,
                              INT4 *pi4RetValFsipv6IfRouterAdvFlags)
{
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);
    UINT1               u1Mbit;
    UINT1               u1_obit;

    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    u1Mbit = (UINT1) (pIf6->u1RaCnf & IP6_IF_M_BIT_ADV);
    u1_obit = (UINT1) (pIf6->u1RaCnf & IP6_IF_O_BIT_ADV);

    if (u1Mbit && u1_obit)
    {
        *pi4RetValFsipv6IfRouterAdvFlags = IP6_IF_ROUT_ADV_BOTH_BIT;
    }
    else if (u1Mbit)
    {
        *pi4RetValFsipv6IfRouterAdvFlags = IP6_IF_ROUT_ADV_M_BIT;
    }
    else if (u1_obit)
    {
        *pi4RetValFsipv6IfRouterAdvFlags = IP6_IF_ROUT_ADV_O_BIT;
    }
    else
    {
        *pi4RetValFsipv6IfRouterAdvFlags = IP6_IF_ROUT_ADV_NO_BIT;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsipv6IfHopLimit
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                retValFsipv6IfHopLimit
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfHopLimit (INT4 i4Fsipv6IfIndex, INT4 *pi4RetValFsipv6IfHopLimit)
{
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsipv6IfHopLimit = (INT4) pIf6->u1Hoplmt;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfDefRouterTime
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                retValFsipv6IfDefRouterTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfDefRouterTime (INT4 i4Fsipv6IfIndex,
                             INT4 *pi4RetValFsipv6IfDefRouterTime)
{
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsipv6IfDefRouterTime = (INT4) pIf6->u2Deftime;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsipv6IfReachableTime
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                retValFsipv6IfReachableTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfReachableTime (INT4 i4Fsipv6IfIndex,
                             INT4 *pi4RetValFsipv6IfReachableTime)
{
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsipv6IfReachableTime = ((INT4) (pIf6->u4Reachtime / 1000));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfRetransmitTime
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                retValFsipv6IfRetransmitTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfRetransmitTime (INT4 i4Fsipv6IfIndex,
                              INT4 *pi4RetValFsipv6IfRetransmitTime)
{
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsipv6IfRetransmitTime = (INT4) (pIf6->u4Rettime / 1000);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfDelayProbeTime
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                retValFsipv6IfDelayProbeTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfDelayProbeTime (INT4 i4Fsipv6IfIndex,
                              INT4 *pi4RetValFsipv6IfDelayProbeTime)
{
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsipv6IfDelayProbeTime = (INT4) pIf6->u4Pdelaytime;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfPrefixAdvStatus
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                retValFsipv6IfPrefixAdvStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfPrefixAdvStatus (INT4 i4Fsipv6IfIndex,
                               INT4 *pi4RetValFsipv6IfPrefixAdvStatus)
{
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pIf6->u1PrefAdv == IP6_IF_PREFIX_ADV)
    {
        *pi4RetValFsipv6IfPrefixAdvStatus = IP6_IF_PREFIX_ADV_ENABLED;
    }
    else
    {
        *pi4RetValFsipv6IfPrefixAdvStatus = IP6_IF_PREFIX_ADV_DISABLED;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfMinRouterAdvTime
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                retValFsipv6IfMinRouterAdvTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfMinRouterAdvTime (INT4 i4Fsipv6IfIndex,
                                INT4 *pi4RetValFsipv6IfMinRouterAdvTime)
{
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsipv6IfMinRouterAdvTime = (INT4) pIf6->u4MinRaTime;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfMaxRouterAdvTime
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                retValFsipv6IfMaxRouterAdvTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfMaxRouterAdvTime (INT4 i4Fsipv6IfIndex,
                                INT4 *pi4RetValFsipv6IfMaxRouterAdvTime)
{
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsipv6IfMaxRouterAdvTime = (INT4) pIf6->u4MaxRaTime;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfDADRetries
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                retValFsipv6IfDADRetries
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfDADRetries (INT4 i4Fsipv6IfIndex,
                          INT4 *pi4RetValFsipv6IfDADRetries)
{
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsipv6IfDADRetries = (INT4) pIf6->u2DadSend;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfForwarding
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                retValFsipv6IfForwarding
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfForwarding (INT4 i4Fsipv6IfIndex,
                          INT4 *pi4RetValFsipv6IfForwarding)
{
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsipv6IfForwarding = (INT4) pIf6->u1Ipv6IfFwdStatusConfigured;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfRoutingStatus
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                retValFsipv6IfRoutingStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfRoutingStatus (INT4 i4Fsipv6IfIndex,
                             INT4 *pi4RetValFsipv6IfRoutingStatus)
{
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsipv6IfRoutingStatus = (INT4) pIf6->u1Ipv6IfFwdOperStatus;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfIcmpErrInterval
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                retValFsipv6IfIcmpErrInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfIcmpErrInterval (INT4 i4Fsipv6IfIndex,
                               INT4 *pu4RetValFsipv6IfIcmpErrInterval)
{
    tIp6If             *pIf6 = NULL;
    pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsipv6IfIcmpErrInterval = pIf6->Icmp6ErrRLInfo.i4Icmp6ErrInterval;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfIcmpTokenBucketSize
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                retValFsipv6IfIcmpTokenBucketSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfIcmpTokenBucketSize (INT4 i4Fsipv6IfIndex,
                                   INT4 *pi4RetValFsipv6IfIcmpTokenBucketSize)
{
    tIp6If             *pIf6 = NULL;
    pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsipv6IfIcmpTokenBucketSize =
        pIf6->Icmp6ErrRLInfo.i4Icmp6BucketSize;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfDestUnreachableMsg
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                retValFsipv6IfDestUnreachableMsg
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfDestUnreachableMsg (INT4 i4Fsipv6IfIndex,
                                  INT4 *pi4RetValFsipv6IfDestUnreachableMsg)
{
    tIp6If             *pIf6 = NULL;
    pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsipv6IfDestUnreachableMsg =
        pIf6->Icmp6ErrRLInfo.i4Icmp6DstUnReachable;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfUnnumAssocIPIf
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                retValFsipv6IfUnnumAssocIPIf
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfUnnumAssocIPIf (INT4 i4Fsipv6IfIndex,
                              INT4 *pi4RetValFsipv6IfUnnumAssocIPIf)
{
    tIp6If             *pIf6 = NULL;

    pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);
    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsipv6IfUnnumAssocIPIf = (INT4) pIf6->u4UnnumAssocIPv6If;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfRedirectMsg
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                retValFsipv6IfRedirectMsg
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfRedirectMsg (INT4 i4Fsipv6IfIndex,
                           INT4 *pi4RetValFsipv6IfRedirectMsg)
{

    tIp6If             *pIf6 = NULL;
    pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsipv6IfRedirectMsg = pIf6->Icmp6ErrRLInfo.i4Icmp6RedirectMsg;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsipv6IfAdvSrcLLAdr
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                retValFsipv6IfAdvSrcLLAdr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfAdvSrcLLAdr (INT4 i4Fsipv6IfIndex,
                           INT4 *pi4RetValFsipv6IfAdvSrcLLAdr)
{
    tIp6If             *pIf6 = NULL;

    pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsipv6IfAdvSrcLLAdr = IP6_RA_NO_ADV_LINKLOCAL;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfAdvIntOpt
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                retValFsipv6IfAdvIntOpt
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfAdvIntOpt (INT4 i4Fsipv6IfIndex, INT4 *pi4RetValFsipv6IfAdvIntOpt)
{
    tIp6If             *pIf6 = NULL;

    pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsipv6IfAdvIntOpt = IP6_RA_NO_ADV_INTERVAL;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfNDProxyAdminStatus
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                retValFsipv6IfNDProxyAdminStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfNDProxyAdminStatus (INT4 i4Fsipv6IfIndex,
                                  INT4 *pi4RetValFsipv6IfNDProxyAdminStatus)
{
    tIp6If             *pIf6 = NULL;

    pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);
    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsipv6IfNDProxyAdminStatus = (INT4) pIf6->u1NDProxyAdminStatus;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfNDProxyMode
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                retValFsipv6IfNDProxyMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfNDProxyMode (INT4 i4Fsipv6IfIndex,
                           INT4 *pi4RetValFsipv6IfNDProxyMode)
{
    tIp6If             *pIf6 = NULL;

    pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsipv6IfNDProxyMode = (INT4) pIf6->u1NDProxyMode;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfNDProxyOperStatus
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                retValFsipv6IfNDProxyOperStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfNDProxyOperStatus (INT4 i4Fsipv6IfIndex,
                                 INT4 *pi4RetValFsipv6IfNDProxyOperStatus)
{
    tIp6If             *pIf6 = NULL;

    pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsipv6IfNDProxyOperStatus = (INT4) pIf6->u1NDProxyOperStatus;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfNDProxyUpStream
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                retValFsipv6IfNDProxyUpStream
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfNDProxyUpStream (INT4 i4Fsipv6IfIndex,
                               INT4 *pi4RetValFsipv6IfNDProxyUpStream)
{
    tIp6If             *pIf6 = NULL;

    pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsipv6IfNDProxyUpStream = (INT4) pIf6->b1NDProxyUpStream;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfSENDSecStatus
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                retValFsipv6IfSENDSecStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfSENDSecStatus (INT4 i4Fsipv6IfIndex,
                             INT4 *pi4RetValFsipv6IfSENDSecStatus)
{
    tIp6If             *pIf6 = NULL;

    pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsipv6IfSENDSecStatus = pIf6->u1SeNDStatus;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfSENDDeltaTimestamp
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                retValFsipv6IfSENDDeltaTimestamp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfSENDDeltaTimestamp (INT4 i4Fsipv6IfIndex,
                                  UINT4 *pu4RetValFsipv6IfSENDDeltaTimestamp)
{
    tIp6If             *pIf6 = NULL;

    pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsipv6IfSENDDeltaTimestamp = pIf6->u2SeNDDeltaTime;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfSENDFuzzTimestamp
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                retValFsipv6IfSENDFuzzTimestamp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfSENDFuzzTimestamp (INT4 i4Fsipv6IfIndex,
                                 UINT4 *pu4RetValFsipv6IfSENDFuzzTimestamp)
{
    tIp6If             *pIf6 = NULL;

    pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsipv6IfSENDFuzzTimestamp = pIf6->u2SeNDFuzzTime;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfSENDDriftTimestamp
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                retValFsipv6IfSENDDriftTimestamp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfSENDDriftTimestamp (INT4 i4Fsipv6IfIndex,
                                  UINT4 *pu4RetValFsipv6IfSENDDriftTimestamp)
{
    tIp6If             *pIf6 = NULL;

    pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsipv6IfSENDDriftTimestamp = pIf6->u1SeNDDriftTime;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfDefRoutePreference
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                retValFsipv6IfDefRoutePreference
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfDefRoutePreference (INT4 i4Fsipv6IfIndex,
                                  INT4 *pi4RetValFsipv6IfDefRoutePreference)
{
    tIp6If             *pIf6 = NULL;

    pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6 == NULL)
    {
        CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
        return SNMP_FAILURE;
    }

    *pi4RetValFsipv6IfDefRoutePreference = (INT4) pIf6->u2Preference;
    return (SNMP_SUCCESS);
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsipv6IfToken
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                setValFsipv6IfToken
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6IfToken (INT4 i4Fsipv6IfIndex,
                     tSNMP_OCTET_STRING_TYPE * pSetValFsipv6IfToken)
{
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6 == NULL)
    {
#ifdef RM_WANTED
        /* In case of standby node, we can ignore the deletion of ipv6 token
         * syncup failure, because its already deleted in standby */
        if ((pSetValFsipv6IfToken->i4_Length == 0)
            && (RmGetNodeState () == RM_STANDBY))
        {
            return SNMP_SUCCESS;
        }
#endif
        return SNMP_FAILURE;
    }
    if ((pIf6->u1IfType == IP6_FR_INTERFACE_TYPE) ||
        (pIf6->u1IfType == IP6_L3VLAN_INTERFACE_TYPE) ||
        (pIf6->u1IfType == IP6_LAGG_INTERFACE_TYPE) ||
        (pIf6->u1IfType == IP6_PSEUDO_WIRE_INTERFACE_TYPE) ||
        (pIf6->u1IfType == IP6_ENET_INTERFACE_TYPE) ||
        (pIf6->u1IfType == IP6_L3SUB_INTF_TYPE))
    {
        pIf6->u1TokLen = (UINT1) pSetValFsipv6IfToken->i4_Length;
        MEMCPY (pIf6->ifaceTok, pSetValFsipv6IfToken->pu1_OctetList,
                pSetValFsipv6IfToken->i4_Length);
        IncMsrForIpv6IfTable (i4Fsipv6IfIndex, 's', pSetValFsipv6IfToken,
                              FsMIIpv6IfToken,
                              sizeof (FsMIIpv6IfToken) / sizeof (UINT4));
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhSetFsipv6IfAdminStatus
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                setValFsipv6IfAdminStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6IfAdminStatus (INT4 i4Fsipv6IfIndex,
                           INT4 i4SetValFsipv6IfAdminStatus)
{
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pIf6->u1AdminStatus == i4SetValFsipv6IfAdminStatus)
    {
        /* No change in the Status. */
        pIf6->u1AdminConfigFlag = TRUE;
        return SNMP_SUCCESS;
    }

    if (i4SetValFsipv6IfAdminStatus == ADMIN_UP)
    {
        /* Enable IPv6 Over this interface. */
        if (Ip6EnableIf ((UINT4) i4Fsipv6IfIndex) == IP6_FAILURE)
        {
            return SNMP_FAILURE;
        }
        pIf6->u1AdminConfigFlag = TRUE;
    }
    else
    {
        if (((pIf6->u1OperStatus != OPER_DOWN) &&
             (pIf6->u4CurOperStatus != OPER_DOWN_INPROGRESS)) ||
            (pIf6->u1AdminStatus == ADMIN_UP))
        {
            /* IPv6 needs to disabled over this interface. */
            Ip6DisableIf ((UINT4) i4Fsipv6IfIndex);
        }
    }

    IncMsrForIPVXIfTable (i4Fsipv6IfIndex, i4SetValFsipv6IfAdminStatus,
                          FsMIStdIpv6InterfaceEnableStatus,
                          (sizeof (FsMIStdIpv6InterfaceEnableStatus) /
                           sizeof (UINT4)), FALSE);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6IfRouterAdvStatus
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                setValFsipv6IfRouterAdvStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6IfRouterAdvStatus (INT4 i4Fsipv6IfIndex,
                               INT4 i4SetValFsipv6IfRouterAdvStatus)
{

    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6 == NULL)
    {
#ifdef RM_WANTED
        /* In case of standby node, we can ignore the disable status
         * syncup failure, because its already disabled in standby */
        if ((i4SetValFsipv6IfRouterAdvStatus == IP6_IF_ROUT_ADV_DISABLED)
            && (RmGetNodeState () == RM_STANDBY))
        {
            return SNMP_SUCCESS;
        }
#endif
        return SNMP_FAILURE;
    }

    if (Ip6SetFsipv6IfRouterAdvStatus (i4Fsipv6IfIndex,
                                       i4SetValFsipv6IfRouterAdvStatus)
        == IP6_FAILURE)
    {
        return SNMP_FAILURE;
    }

    IncMsrForIpv6IfTable (i4Fsipv6IfIndex, 'i',
                          &i4SetValFsipv6IfRouterAdvStatus,
                          FsMIIpv6IfRouterAdvStatus,
                          sizeof (FsMIIpv6IfRouterAdvStatus) / sizeof (UINT4));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6IfRouterAdvFlags
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                setValFsipv6IfRouterAdvFlags
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6IfRouterAdvFlags (INT4 i4Fsipv6IfIndex,
                              INT4 i4SetValFsipv6IfRouterAdvFlags)
{

    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6 == NULL)
    {
#ifdef RM_WANTED
        /* In case of standby node, we can ignore the adv Flag status
         * syncup failure, because its already set as no bit in standby */
        if ((i4SetValFsipv6IfRouterAdvFlags == IP6_IF_ROUT_ADV_NO_BIT)
            && (RmGetNodeState () == RM_STANDBY))
        {
            return SNMP_SUCCESS;
        }
#endif
        return SNMP_FAILURE;
    }

    if (Ip6SetFsipv6IfRouterAdvFlags (i4Fsipv6IfIndex,
                                      i4SetValFsipv6IfRouterAdvFlags)
        == IP6_FAILURE)
    {
        return SNMP_FAILURE;
    }

    IncMsrForIpv6IfTable (i4Fsipv6IfIndex, 'i',
                          &i4SetValFsipv6IfRouterAdvFlags,
                          FsMIIpv6IfRouterAdvFlags,
                          sizeof (FsMIIpv6IfRouterAdvFlags) / sizeof (UINT4));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6IfHopLimit
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                setValFsipv6IfHopLimit
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6IfHopLimit (INT4 i4Fsipv6IfIndex, INT4 i4SetValFsipv6IfHopLimit)
{
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6 == NULL)
    {
#ifdef RM_WANTED
        /* In case of standby node, we can ignore the Hop Limit
         * syncup failure,because its already set as default hop Limit in standby */
        if ((i4SetValFsipv6IfHopLimit == IP6_IF_DEF_HOPLMT)
            && (RmGetNodeState () == RM_STANDBY))
        {
            return SNMP_SUCCESS;
        }
#endif
        return SNMP_FAILURE;
    }

    pIf6->u1Hoplmt = (UINT1) i4SetValFsipv6IfHopLimit;

    IncMsrForIpv6IfTable (i4Fsipv6IfIndex, 'i', &i4SetValFsipv6IfHopLimit,
                          FsMIIpv6IfHopLimit,
                          sizeof (FsMIIpv6IfHopLimit) / sizeof (UINT4));
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsipv6IfDefRouterTime
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                setValFsipv6IfDefRouterTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6IfDefRouterTime (INT4 i4Fsipv6IfIndex,
                             INT4 i4SetValFsipv6IfDefRouterTime)
{
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6 == NULL)
    {
#ifdef RM_WANTED
        /* In case of standby node, we can ignore the router time
         * syncup failure, because its already set with default router time in standby */
        if ((i4SetValFsipv6IfDefRouterTime == IP6_IF_DEF_DEFTIME)
            && (RmGetNodeState () == RM_STANDBY))
        {
            return SNMP_SUCCESS;
        }
#endif
        return SNMP_FAILURE;
    }

    pIf6->u2Deftime = (UINT2) i4SetValFsipv6IfDefRouterTime;

    IncMsrForIpv6IfTable (i4Fsipv6IfIndex, 'i', &i4SetValFsipv6IfDefRouterTime,
                          FsMIIpv6IfDefRouterTime,
                          sizeof (FsMIIpv6IfDefRouterTime) / sizeof (UINT4));
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsipv6IfReachableTime
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                setValFsipv6IfReachableTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6IfReachableTime (INT4 i4Fsipv6IfIndex,
                             INT4 i4SetValFsipv6IfReachableTime)
{
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6 == NULL)
    {
#ifdef RM_WANTED
        /* In case of standby node, we can ignore the reachable time
         * syncup failure, because its already set with default reachable time in standby */
        if ((i4SetValFsipv6IfReachableTime == (IP6_IF_DEF_REACHTIME / 1000))
            && (RmGetNodeState () == RM_STANDBY))
        {
            return SNMP_SUCCESS;
        }
#endif
        return SNMP_FAILURE;
    }

    pIf6->u4Reachtime = (UINT4) (i4SetValFsipv6IfReachableTime * 1000);

    OsixGetSysTime (&gIp6GblInfo.u4Ip6IfTableLastChange);

    IncMsrForIpv6IfTable (i4Fsipv6IfIndex, 'i',
                          &i4SetValFsipv6IfReachableTime,
                          FsMIIpv6IfReachableTime,
                          sizeof (FsMIIpv6IfReachableTime) / sizeof (UINT4));
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsipv6IfRetransmitTime
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                setValFsipv6IfRetransmitTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6IfRetransmitTime (INT4 i4Fsipv6IfIndex,
                              INT4 i4SetValFsipv6IfRetransmitTime)
{
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6 == NULL)
    {
#ifdef RM_WANTED
        /* In case of standby node, we can ignore the retransmit time
         * syncup failure, because its already set with default retransmit time in standby */
        if ((i4SetValFsipv6IfRetransmitTime == (IP6_IF_DEF_RETTIME / 1000))
            && (RmGetNodeState () == RM_STANDBY))
        {
            return SNMP_SUCCESS;
        }
#endif
        return SNMP_FAILURE;
    }

    pIf6->u4Rettime = (UINT4) i4SetValFsipv6IfRetransmitTime *1000;

    OsixGetSysTime (&gIp6GblInfo.u4Ip6IfTableLastChange);

    IncMsrForIpv6IfTable (i4Fsipv6IfIndex, 'i',
                          &i4SetValFsipv6IfRetransmitTime,
                          FsMIIpv6IfRetransmitTime,
                          sizeof (FsMIIpv6IfRetransmitTime) / sizeof (UINT4));
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsipv6IfDelayProbeTime
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                setValFsipv6IfDelayProbeTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6IfDelayProbeTime (INT4 i4Fsipv6IfIndex,
                              INT4 i4SetValFsipv6IfDelayProbeTime)
{
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6 == NULL)
    {
#ifdef RM_WANTED
        /* In case of standby node, we can ignore the Probe time syncup failure,
         * because its already set with default Probe time in standby */
        if ((i4SetValFsipv6IfDelayProbeTime == IP6_IF_DEF_PDTIME)
            && (RmGetNodeState () == RM_STANDBY))
        {
            return SNMP_SUCCESS;
        }
#endif
        return SNMP_FAILURE;
    }

    pIf6->u4Pdelaytime = (UINT4) i4SetValFsipv6IfDelayProbeTime;
    IncMsrForIpv6IfTable (i4Fsipv6IfIndex, 'i',
                          &i4SetValFsipv6IfDelayProbeTime,
                          FsMIIpv6IfDelayProbeTime,
                          sizeof (FsMIIpv6IfDelayProbeTime) / sizeof (UINT4));
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsipv6IfPrefixAdvStatus
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                setValFsipv6IfPrefixAdvStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6IfPrefixAdvStatus (INT4 i4Fsipv6IfIndex,
                               INT4 i4SetValFsipv6IfPrefixAdvStatus)
{
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6 == NULL)
    {
#ifdef RM_WANTED
        /* In case of standby node, we can ignore the prefix advance status
         * syncup failure, because its already set with prefix advance status in standby */
        if ((i4SetValFsipv6IfPrefixAdvStatus == IP6_IF_PREFIX_ADV_ENABLED)
            && (RmGetNodeState () == RM_STANDBY))
        {
            return SNMP_SUCCESS;
        }
#endif
        return SNMP_FAILURE;
    }

    if (i4SetValFsipv6IfPrefixAdvStatus == IP6_IF_PREFIX_ADV_ENABLED)
    {
        pIf6->u1PrefAdv = IP6_IF_PREFIX_ADV;
    }
    else if (i4SetValFsipv6IfPrefixAdvStatus == IP6_IF_PREFIX_ADV_DISABLED)
    {
        pIf6->u1PrefAdv = IP6_IF_NO_PREFIX_ADV;
    }

    IncMsrForIpv6IfTable (i4Fsipv6IfIndex, 'i',
                          &i4SetValFsipv6IfPrefixAdvStatus,
                          FsMIIpv6IfPrefixAdvStatus,
                          sizeof (FsMIIpv6IfPrefixAdvStatus) / sizeof (UINT4));
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsipv6IfMinRouterAdvTime
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                setValFsipv6IfMinRouterAdvTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6IfMinRouterAdvTime (INT4 i4Fsipv6IfIndex,
                                INT4 i4SetValFsipv6IfMinRouterAdvTime)
{
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6 == NULL)
    {
#ifdef RM_WANTED
        /* In case of standby node, we can ignore the Min Router Adv time
         * syncup failure, because its already set with default Min Router Adv time in standby */
        if ((i4SetValFsipv6IfMinRouterAdvTime == IP6_IF_DEF_MIN_RA_TIME)
            && (RmGetNodeState () == RM_STANDBY))
        {
            return SNMP_SUCCESS;
        }
#endif
        return SNMP_FAILURE;
    }

    pIf6->u4MinRaTime = (UINT4) i4SetValFsipv6IfMinRouterAdvTime;
    IncMsrForIpv6IfTable (i4Fsipv6IfIndex, 'i',
                          &i4SetValFsipv6IfMinRouterAdvTime,
                          FsMIIpv6IfMinRouterAdvTime,
                          sizeof (FsMIIpv6IfMinRouterAdvTime) / sizeof (UINT4));
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsipv6IfMaxRouterAdvTime
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                setValFsipv6IfMaxRouterAdvTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6IfMaxRouterAdvTime (INT4 i4Fsipv6IfIndex,
                                INT4 i4SetValFsipv6IfMaxRouterAdvTime)
{
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6 == NULL)
    {
#ifdef RM_WANTED
        /* In case of standby node, we can ignore the Max Router Adv time
         * syncup failure, because its already set with default Max Router Adv time in standby */
        if ((i4SetValFsipv6IfMaxRouterAdvTime == IP6_IF_DEF_MAX_RA_TIME)
            && (RmGetNodeState () == RM_STANDBY))
        {
            return SNMP_SUCCESS;
        }
#endif
        return SNMP_FAILURE;
    }

    pIf6->u4MaxRaTime = (UINT4) i4SetValFsipv6IfMaxRouterAdvTime;
    IncMsrForIpv6IfTable (i4Fsipv6IfIndex, 'i',
                          &i4SetValFsipv6IfMaxRouterAdvTime,
                          FsMIIpv6IfMaxRouterAdvTime,
                          sizeof (FsMIIpv6IfMaxRouterAdvTime) / sizeof (UINT4));
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetFsipv6IfDADRetries
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                setValFsipv6IfDADRetries
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6IfDADRetries (INT4 i4Fsipv6IfIndex, INT4 i4SetValFsipv6IfDADRetries)
{
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6 == NULL)
    {
#ifdef RM_WANTED
        /* In case of standby node, we can ignore the DAD retries syncup failure,
         * because its already set with default DAD send in standby */
        if ((i4SetValFsipv6IfDADRetries == IP6_IF_DEF_DAD_SEND)
            && (RmGetNodeState () == RM_STANDBY))
        {
            return SNMP_SUCCESS;
        }
#endif
        return SNMP_FAILURE;
    }

    pIf6->u2DadSend = (UINT2) i4SetValFsipv6IfDADRetries;

    IncMsrForIpv6IfTable (i4Fsipv6IfIndex, 'i', &i4SetValFsipv6IfDADRetries,
                          FsMIIpv6IfDADRetries,
                          sizeof (FsMIIpv6IfDADRetries) / sizeof (UINT4));

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsipv6IfForwarding
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                setValFsipv6IfForwarding
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6IfForwarding (INT4 i4Fsipv6IfIndex, INT4 i4SetValFsipv6IfForwarding)
{
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);
    tIp6Addr            tmpAddr;

    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (&tmpAddr, 0, IP6_ADDR_SIZE);

    if (i4SetValFsipv6IfForwarding == IP6_IF_FORW_ENABLE)
    {

        if ((pIf6->u1AdminStatus == ADMIN_UP) &&
            (pIf6->pIp6Cxt->u4ForwFlag == IP6_FORW_ENABLE))
        {
            /* Ensure that the ALL ROUTER Multicast address is added. */
            SET_ALL_ROUTERS_MULTI (tmpAddr);
            Ip6AddrCreateMcast (i4Fsipv6IfIndex, &tmpAddr);

            pIf6->u1Ipv6IfFwdOperStatus = IP6_IF_FORW_ENABLE;
            /* If RA is enabled, then ensure that RA message are sent. */
            if ((Ip6CheckRAStatus ((INT4) (pIf6->u4Index))) == TRUE)
            {
                Nd6ActOnRaCnf (pIf6, IP6_IF_RA_ADV);
            }
        }
    }

    else
    {

        if (pIf6->u1AdminStatus == ADMIN_UP)
        {
            /* Ensure that the ALL ROUTER Multicast address is deleted. */
            SET_ALL_ROUTERS_MULTI (tmpAddr);
            Ip6AddrDeleteMcast (i4Fsipv6IfIndex, &tmpAddr);
        }

        /* If RA is enabled, then ensure that Final RA message are sent
         * out. */
        if ((Ip6CheckRAStatus ((INT4) (pIf6->u4Index))) == TRUE)
        {
            Nd6ActOnRaCnf (pIf6, IP6_IF_NO_RA_ADV);
        }
        pIf6->u1Ipv6IfFwdOperStatus = IP6_IF_FORW_DISABLE;
        /*  Send the router solicitation and schedule
         * the sending of next router solicitation  */
        if ((pIf6->u1OperStatus == OPER_UP) &&
            ((pIf6->u1IfType == IP6_ENET_INTERFACE_TYPE) ||
#ifdef TUNNEL_WANTED
             (pIf6->u1IfType == IP6_TUNNEL_INTERFACE_TYPE) ||
#endif /* TUNNEL_WANTED */
#ifdef WGS_WANTED
             (pIf6->u1IfType == IP6_L2VLAN_INTERFACE_TYPE) ||
#endif
             (pIf6->u1IfType == IP6_PSEUDO_WIRE_INTERFACE_TYPE) ||
             (pIf6->u1IfType == IP6_L3VLAN_INTERFACE_TYPE) ||
             (pIf6->u1IfType == IP6_LAGG_INTERFACE_TYPE) ||
             (pIf6->u1IfType == IP6_L3SUB_INTF_TYPE)))

        {
            Nd6SendRouterSol (pIf6, NULL);
            Nd6SchedNextRoutSol (pIf6);
        }
    }

    pIf6->u1Ipv6IfFwdStatusConfigured = (UINT1) i4SetValFsipv6IfForwarding;

    IncMsrForIpv6IfTable (i4Fsipv6IfIndex, 'i', &i4SetValFsipv6IfForwarding,
                          FsMIIpv6IfForwarding,
                          sizeof (FsMIIpv6IfForwarding) / sizeof (UINT4));
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsipv6IfIcmpErrInterval
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                setValFsipv6IfIcmpErrInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6IfIcmpErrInterval (INT4 i4Fsipv6IfIndex,
                               INT4 u4SetValFsipv6IfIcmpErrInterval)
{
    tIp6If             *pIf6 = NULL;
    pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    pIf6->Icmp6ErrRLInfo.i4Icmp6ErrInterval = u4SetValFsipv6IfIcmpErrInterval;

    if (u4SetValFsipv6IfIcmpErrInterval)
    {
        pIf6->Icmp6ErrRLInfo.i4Icmp6ErrRLFlag = ICMP6_RATE_LIMIT_ENABLE;
    }
    else
    {
        pIf6->Icmp6ErrRLInfo.i4Icmp6ErrRLFlag = ICMP6_RATE_LIMIT_DISABLE;
    }

    IncMsrForIpv6IfTable (i4Fsipv6IfIndex, 'i',
                          &u4SetValFsipv6IfIcmpErrInterval,
                          FsMIIpv6IfIcmpErrInterval,
                          sizeof (FsMIIpv6IfIcmpErrInterval) / sizeof (UINT4));

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsipv6IfIcmpTokenBucketSize
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                setValFsipv6IfIcmpTokenBucketSize
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6IfIcmpTokenBucketSize (INT4 i4Fsipv6IfIndex,
                                   INT4 i4SetValFsipv6IfIcmpTokenBucketSize)
{
    tIp6If             *pIf6 = NULL;
    pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    pIf6->Icmp6ErrRLInfo.i4Icmp6BucketSize =
        i4SetValFsipv6IfIcmpTokenBucketSize;

    IncMsrForIpv6IfTable (i4Fsipv6IfIndex, 'i',
                          &i4SetValFsipv6IfIcmpTokenBucketSize,
                          FsMIIpv6IfIcmpTokenBucketSize,
                          sizeof (FsMIIpv6IfIcmpTokenBucketSize) /
                          sizeof (UINT4));

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsipv6IfDestUnreachableMsg
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                setValFsipv6IfDestUnreachableMsg
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6IfDestUnreachableMsg (INT4 i4Fsipv6IfIndex,
                                  INT4 i4SetValFsipv6IfDestUnreachableMsg)
{
    tIp6If             *pIf6 = NULL;
    pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    pIf6->Icmp6ErrRLInfo.i4Icmp6DstUnReachable =
        i4SetValFsipv6IfDestUnreachableMsg;

    IncMsrForIpv6IfTable (i4Fsipv6IfIndex, 'i',
                          &i4SetValFsipv6IfDestUnreachableMsg,
                          FsMIIpv6IfDestUnreachableMsg,
                          sizeof (FsMIIpv6IfDestUnreachableMsg) /
                          sizeof (UINT4));

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsipv6IfUnnumAssocIPIf
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                setValFsipv6IfUnnumAssocIPIf
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6IfUnnumAssocIPIf (INT4 i4Fsipv6IfIndex,
                              INT4 i4SetValFsipv6IfUnnumAssocIPIf)
{
    tIp6If             *pIf6 = NULL;

    /*RFC 4007 scope to be created */

    UINT1               u1Scope = ADDR6_SCOPE_GLOBAL;
    tIp6IfZoneMapInfo  *pIp6IfZoneMapInfo = NULL;
    INT4                i4ZoneId = 0;
    UINT1               u1AdminStatus = ADMIN_DOWN;

    pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);
    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pIf6->u4UnnumAssocIPv6If == (UINT4) i4SetValFsipv6IfUnnumAssocIPIf)
    {
        return SNMP_SUCCESS;
    }

    u1AdminStatus = pIf6->u1AdminStatus;

    /* RFC Create global scope-zone for this address if the global zone
       doesnot exist already create it and map this interface to that zone */

    if (i4SetValFsipv6IfUnnumAssocIPIf == 0)
    {
        if (u1AdminStatus != ADMIN_DOWN)
        {
            Ip6DisableIf ((UINT4) i4Fsipv6IfIndex);
        }

        pIf6->u4UnnumAssocIPv6If = (UINT4) i4SetValFsipv6IfUnnumAssocIPIf;

        if (u1AdminStatus != ADMIN_DOWN)
        {
            Ip6EnableIf ((UINT4) i4Fsipv6IfIndex);
        }

        pIp6IfZoneMapInfo = Ip6ZoneGetIfScopeZoneEntry
            ((UINT4) i4Fsipv6IfIndex, u1Scope);

        if (NULL != pIp6IfZoneMapInfo)
        {
            i4ZoneId = pIp6IfZoneMapInfo->i4ZoneId;
            Ip6ScopeZoneDelete ((UINT4) i4Fsipv6IfIndex, u1Scope, i4ZoneId);
        }
    }
    else
    {
        /* Making the interface down before changing unnumbered interface 
         * association, to indicate Higher layer, the change in unnumbered 
         * association */

        if (u1AdminStatus != ADMIN_DOWN)
        {
            Ip6DisableIf ((UINT4) i4Fsipv6IfIndex);
        }

        pIf6->u4UnnumAssocIPv6If = (UINT4) i4SetValFsipv6IfUnnumAssocIPIf;

        if (u1AdminStatus != ADMIN_DOWN)
        {
            Ip6EnableIf ((UINT4) i4Fsipv6IfIndex);
        }

        Ip6ZoneCreateAutoScopeZone ((UINT4) i4Fsipv6IfIndex, u1Scope);
    }

    IncMsrForIpv6IfTable (i4Fsipv6IfIndex, 'i',
                          &i4SetValFsipv6IfUnnumAssocIPIf,
                          FsMIIpv6IfUnnumAssocIPIf,
                          sizeof (FsMIIpv6IfUnnumAssocIPIf) / sizeof (UINT4));

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsipv6IfRedirectMsg
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                setValFsipv6IfRedirectMsg
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6IfRedirectMsg (INT4 i4Fsipv6IfIndex,
                           INT4 i4SetValFsipv6IfRedirectMsg)
{
    tIp6If             *pIf6 = NULL;
    pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    pIf6->Icmp6ErrRLInfo.i4Icmp6RedirectMsg = i4SetValFsipv6IfRedirectMsg;

    IncMsrForIpv6IfTable (i4Fsipv6IfIndex, 'i',
                          &i4SetValFsipv6IfRedirectMsg,
                          FsMIIpv6IfRedirectMsg,
                          sizeof (FsMIIpv6IfRedirectMsg) / sizeof (UINT4));

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsipv6IfAdvSrcLLAdr
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                setValFsipv6IfAdvSrcLLAdr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6IfAdvSrcLLAdr (INT4 i4Fsipv6IfIndex,
                           INT4 i4SetValFsipv6IfAdvSrcLLAdr)
{
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (i4SetValFsipv6IfAdvSrcLLAdr);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6IfAdvIntOpt
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                setValFsipv6IfAdvIntOpt
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6IfAdvIntOpt (INT4 i4Fsipv6IfIndex, INT4 i4SetValFsipv6IfAdvIntOpt)
{
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (i4SetValFsipv6IfAdvIntOpt);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6IfNDProxyAdminStatus
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                setValFsipv6IfNDProxyAdminStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6IfNDProxyAdminStatus (INT4 i4Fsipv6IfIndex,
                                  INT4 i4SetValFsipv6IfNDProxyAdminStatus)
{
    tIp6If             *pIf6 = NULL;

    pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);
    if (pIf6 == NULL)
    {
        CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
        return SNMP_FAILURE;
    }

    if (i4SetValFsipv6IfNDProxyAdminStatus == (INT4) pIf6->u1NDProxyAdminStatus)
    {
        return (SNMP_SUCCESS);
    }

    pIf6->u1NDProxyAdminStatus = (UINT1) i4SetValFsipv6IfNDProxyAdminStatus;

    if (i4SetValFsipv6IfNDProxyAdminStatus == ND6_IF_PROXY_ADMIN_UP)
    {
        Nd6ProxyUtlSetOperStatus (pIf6, ND6_PROXY_OPER_UP);
    }
    else
    {
        Nd6ProxyUtlSetOperStatus (pIf6, ND6_PROXY_OPER_DOWN);
    }

    if (i4SetValFsipv6IfNDProxyAdminStatus == ND6_IF_PROXY_ADMIN_DOWN)
    {
        /* stop the timer when disabling the proxy */
        Ip6TmrStop (pIf6->pIp6Cxt->u4ContextId,
                    ND6_PROXY_LOOP_TIMER_ID, gIp6GblInfo.Ip6TimerListId,
                    &(pIf6->proxyLoopTimer.appTimer));
    }

    IncMsrForIpv6IfTable (i4Fsipv6IfIndex, 'i',
                          &i4SetValFsipv6IfNDProxyAdminStatus,
                          FsMIIpv6IfNDProxyAdminStatus,
                          sizeof (FsMIIpv6IfNDProxyAdminStatus) /
                          sizeof (UINT4));

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsipv6IfNDProxyMode
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                setValFsipv6IfNDProxyMode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6IfNDProxyMode (INT4 i4Fsipv6IfIndex,
                           INT4 i4SetValFsipv6IfNDProxyMode)
{
    tIp6If             *pIf6 = NULL;

    pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6 == NULL)
    {
        CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
        return SNMP_FAILURE;
    }

    pIf6->u1NDProxyMode = (UINT1) i4SetValFsipv6IfNDProxyMode;

    IncMsrForIpv6IfTable (i4Fsipv6IfIndex, 'i', &i4SetValFsipv6IfNDProxyMode,
                          FsMIIpv6IfNDProxyMode,
                          sizeof (FsMIIpv6IfNDProxyMode) / sizeof (UINT4));
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsipv6IfNDProxyUpStream
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                setValFsipv6IfNDProxyUpStream
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6IfNDProxyUpStream (INT4 i4Fsipv6IfIndex,
                               INT4 i4SetValFsipv6IfNDProxyUpStream)
{
    tIp6If             *pIf6 = NULL;

    pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6 == NULL)
    {
        CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
        return SNMP_FAILURE;
    }

    pIf6->b1NDProxyUpStream = (BOOL1) i4SetValFsipv6IfNDProxyUpStream;
    IncMsrForIpv6IfTable (i4Fsipv6IfIndex, 'i',
                          &i4SetValFsipv6IfNDProxyUpStream,
                          FsMIIpv6IfNDProxyUpStream,
                          sizeof (FsMIIpv6IfNDProxyUpStream) / sizeof (UINT4));

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsipv6IfSENDSecStatus
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                setValFsipv6IfSENDSecStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6IfSENDSecStatus (INT4 i4Fsipv6IfIndex,
                             INT4 i4SetValFsipv6IfSENDSecStatus)
{
#ifdef SSL_WANTED
    tSNMP_OCTET_STRING_TYPE OctetCgaModifier;
    tSNMP_OCTET_STRING_TYPE OctetIp6Addr;
    tIp6If             *pIf6 = NULL;
    tIp6LlocalInfo     *pLlocalInfo = NULL;
    tTMO_SLL_NODE      *pLlAddrSll = NULL;
    UINT1              *pu1Buffer = NULL;
    UINT1              *pu1TempBuffer = NULL;
    UINT1               au1Array[ND6_SEND_CGA_MOD_LEN];

    MEMSET (au1Array, IP6_ZERO, ND6_SEND_CGA_MOD_LEN);
    MEMSET (&OctetCgaModifier, IP6_ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));

    OctetCgaModifier.pu1_OctetList = au1Array;
    OctetCgaModifier.i4_Length = ND6_SEND_CGA_MOD_LEN;

    OctetIp6Addr.i4_Length = IP6_ADDR_SIZE;

    pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6 == NULL)
    {
        CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
        return SNMP_FAILURE;
    }

    if (IP6_FAILURE == Nd6SecureCheck (i4Fsipv6IfIndex))
    {
        CLI_SET_ERR (CLI_IP6_SEND_REMOVE_CONF_ADDR);
        return SNMP_FAILURE;
    }

    pu1Buffer =
        Ip6GetMem (pIf6->pIp6Cxt->u4ContextId,
                   gIp6GblInfo.i4Ip6Nd6SecurePoolId);
    if (NULL == pu1Buffer)
    {
        return SNMP_FAILURE;
    }

    pu1TempBuffer = pu1Buffer;
    MEMSET (pu1Buffer, 0, ND6_SEND_MAX_BUF_SIZE);

    TMO_SLL_Scan (&gIp6GblInfo.apIp6If[i4Fsipv6IfIndex]->lla6Ilist,
                  pLlAddrSll, tTMO_SLL_NODE *)
    {
        pLlocalInfo = IP6_LLADDR_PTR_FROM_SLL (pLlAddrSll);
        if (pLlocalInfo->u1ConfigMethod == IP6_ADDR_AUTO_SL)
        {
            pLlocalInfo->b1SeNDCgaStatus = IP6_ONE;
            if (SSL_FAILURE == SslArGetDerRsaPubKey (&pu1Buffer,
                                                     &(pLlocalInfo->cgaParams.
                                                       u2PubKeyLen)))
            {
                Ip6RelMem (pIf6->pIp6Cxt->u4ContextId,
                           (UINT2) gIp6GblInfo.i4Ip6Nd6SecurePoolId, pu1Buffer);
                return SNMP_FAILURE;
            }
            /* 
             * pu1Buffer is modified to point to last in SslArGetDerRsaPubKey
             * so assign key from pu1TempBuffer
             */
            pLlocalInfo->cgaParams.pu1DerPubKey = pu1TempBuffer;
            MEMCPY (pLlocalInfo->cgaParams.au1Prefix,
                    &pLlocalInfo->ip6Addr, ND6_SEND_CGA_PREFIX_LEN);

            /*
             * CGA generation in standby results in different CGA address
             * due to different random number, so it will be synced dynamically
             * Similarly, CGA generation is not required in MSR
             */
            if ((ND6_GET_NODE_STATUS () == RM_ACTIVE) &&
                (MsrIsMibRestoreInProgress () != OSIX_SUCCESS))
            {
                if (IP6_FAILURE == Nd6SeNDCgaGenerate (pIf6,
                                                       &(pLlocalInfo->ip6Addr),
                                                       &(pLlocalInfo->
                                                         cgaParams)))
                {
                    Ip6RelMem (pIf6->pIp6Cxt->u4ContextId,
                               (UINT2) gIp6GblInfo.i4Ip6Nd6SecurePoolId,
                               pu1Buffer);
                    return SNMP_FAILURE;
                }

                /*
                 * explicit sync of modifier is required to replace
                 * the old cga address to the newly generated
                 * --> Modifier value is zero for matching
                 */
                OctetIp6Addr.pu1_OctetList = pLlocalInfo->ip6Addr.u1_addr;
                IncMsrForIpv6AddrTable (i4Fsipv6IfIndex, &OctetIp6Addr,
                                        0xFF, 's',
                                        &OctetCgaModifier,
                                        FsMIIpv6AddrSENDCgaModifier,
                                        sizeof (FsMIIpv6AddrSENDCgaModifier) /
                                        sizeof (UINT4), FALSE);

                OctetCgaModifier.pu1_OctetList =
                    pLlocalInfo->cgaParams.au1Modifier;

                /*
                 * actual sync of modifier
                 */
                IncMsrForIpv6AddrTable (i4Fsipv6IfIndex, &OctetIp6Addr,
                                        0xFF, 's',
                                        &OctetCgaModifier,
                                        FsMIIpv6AddrSENDCgaModifier,
                                        sizeof (FsMIIpv6AddrSENDCgaModifier) /
                                        sizeof (UINT4), FALSE);

            }
            break;
        }
    }
    pIf6->u1SeNDStatus = (UINT1) i4SetValFsipv6IfSENDSecStatus;
    Ip6RelMem (pIf6->pIp6Cxt->u4ContextId,
               (UINT2) gIp6GblInfo.i4Ip6Nd6SecurePoolId, pu1Buffer);
    IncMsrForIpv6IfTable (i4Fsipv6IfIndex, 'i', &i4SetValFsipv6IfSENDSecStatus,
                          FsMIIpv6IfSENDSecStatus,
                          sizeof (FsMIIpv6IfSENDSecStatus) / sizeof (UINT4));
    return (SNMP_SUCCESS);
#else
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (i4SetValFsipv6IfSENDSecStatus);
    return (SNMP_FAILURE);
#endif
}

/****************************************************************************
 Function    :  nmhSetFsipv6IfSENDDeltaTimestamp
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                setValFsipv6IfSENDDeltaTimestamp
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6IfSENDDeltaTimestamp (INT4 i4Fsipv6IfIndex,
                                  UINT4 u4SetValFsipv6IfSENDDeltaTimestamp)
{
    tIp6If             *pIf6 = NULL;

    pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6 == NULL)
    {
        CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
        return SNMP_FAILURE;
    }

    pIf6->u2SeNDDeltaTime = (UINT2) u4SetValFsipv6IfSENDDeltaTimestamp;
    IncMsrForIpv6IfTable (i4Fsipv6IfIndex, 'u',
                          &u4SetValFsipv6IfSENDDeltaTimestamp,
                          FsMIIpv6IfSENDDeltaTimestamp,
                          sizeof (FsMIIpv6IfSENDDeltaTimestamp) /
                          sizeof (UINT4));
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsipv6IfSENDFuzzTimestamp
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                setValFsipv6IfSENDFuzzTimestamp
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6IfSENDFuzzTimestamp (INT4 i4Fsipv6IfIndex,
                                 UINT4 u4SetValFsipv6IfSENDFuzzTimestamp)
{
    tIp6If             *pIf6 = NULL;

    pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6 == NULL)
    {
        CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
        return SNMP_FAILURE;
    }

    pIf6->u2SeNDFuzzTime = (UINT2) u4SetValFsipv6IfSENDFuzzTimestamp;
    IncMsrForIpv6IfTable (i4Fsipv6IfIndex, 'u',
                          &u4SetValFsipv6IfSENDFuzzTimestamp,
                          FsMIIpv6IfSENDFuzzTimestamp,
                          sizeof (FsMIIpv6IfSENDFuzzTimestamp) /
                          sizeof (UINT4));
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsipv6IfSENDDriftTimestamp
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                setValFsipv6IfSENDDriftTimestamp
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6IfSENDDriftTimestamp (INT4 i4Fsipv6IfIndex,
                                  UINT4 u4SetValFsipv6IfSENDDriftTimestamp)
{
    tIp6If             *pIf6 = NULL;

    pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6 == NULL)
    {
        CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
        return SNMP_FAILURE;
    }

    pIf6->u1SeNDDriftTime = (UINT1) u4SetValFsipv6IfSENDDriftTimestamp;
    IncMsrForIpv6IfTable (i4Fsipv6IfIndex, 'u',
                          &u4SetValFsipv6IfSENDDriftTimestamp,
                          FsMIIpv6IfSENDDriftTimestamp,
                          sizeof (FsMIIpv6IfSENDDriftTimestamp) /
                          sizeof (UINT4));
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsipv6IfDefRoutePreference
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                setValFsipv6IfDefRoutePreference
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6IfDefRoutePreference (INT4 i4Fsipv6IfIndex,
                                  INT4 i4SetValFsipv6IfDefRoutePreference)
{
    tIp6If             *pIf6 = NULL;

    pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6 == NULL)
    {
        CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
        return SNMP_FAILURE;
    }

    pIf6->u2Preference = (UINT2) i4SetValFsipv6IfDefRoutePreference;

    IncMsrForIpv6IfTable (i4Fsipv6IfIndex, 'i',
                          &i4SetValFsipv6IfDefRoutePreference,
                          FsMIIpv6IfDefRoutePreference,
                          sizeof (FsMIIpv6IfDefRoutePreference) /
                          sizeof (UINT4));

    return (SNMP_SUCCESS);
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Fsipv6IfToken
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                testValFsipv6IfToken
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6IfToken (UINT4 *pu4ErrorCode, INT4 i4Fsipv6IfIndex,
                        tSNMP_OCTET_STRING_TYPE * pTestValFsipv6IfToken)
{
    tIp6If             *pIf6 = NULL;

    if (((pTestValFsipv6IfToken->i4_Length) > IP6_EUI_ADDRESS_LEN) ||
        pTestValFsipv6IfToken->i4_Length == 0)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return (SNMP_FAILURE);
    }

    if (Ip6ifEntryExists ((UINT4) i4Fsipv6IfIndex) == IP6_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pIf6->u1IfType != IP6_FR_INTERFACE_TYPE)
    {
        /* This object can be set only for the Frame Relay interface. */
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6IfAdminStatus
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                testValFsipv6IfAdminStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6IfAdminStatus (UINT4 *pu4ErrorCode,
                              INT4 i4Fsipv6IfIndex,
                              INT4 i4TestValFsipv6IfAdminStatus)
{
    tIp6If             *pIf6 = NULL;

    if (Ip6ifEntryExists ((UINT4) i4Fsipv6IfIndex) == IP6_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
        return SNMP_FAILURE;
    }

    pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);
    if (pIf6 == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
        return SNMP_FAILURE;
    }

    if ((i4TestValFsipv6IfAdminStatus != ADMIN_UP) &&
        (i4TestValFsipv6IfAdminStatus != ADMIN_DOWN))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_INVALID_ADMIN_STATUS);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6IfRouterAdvStatus
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                testValFsipv6IfRouterAdvStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6IfRouterAdvStatus (UINT4 *pu4ErrorCode, INT4 i4Fsipv6IfIndex,
                                  INT4 i4TestValFsipv6IfRouterAdvStatus)
{
    tIp6If             *pIf6 = NULL;

    if (Ip6ifEntryExists ((UINT4) i4Fsipv6IfIndex) == IP6_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
        return SNMP_FAILURE;
    }

    pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);
    if (pIf6 == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
        return SNMP_FAILURE;
    }

    if (Ip6Testv2Fsipv6IfRouterAdvStatus (pu4ErrorCode, i4Fsipv6IfIndex,
                                          i4TestValFsipv6IfRouterAdvStatus)
        == IP6_SUCCESS)
    {
        return SNMP_SUCCESS;
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    CLI_SET_ERR (CLI_IP6_INVALID_RT_ADV_STATUS);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6IfRouterAdvFlags
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                testValFsipv6IfRouterAdvFlags
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6IfRouterAdvFlags (UINT4 *pu4ErrorCode, INT4 i4Fsipv6IfIndex,
                                 INT4 i4TestValFsipv6IfRouterAdvFlags)
{
    tIp6If             *pIf6 = NULL;

    if (Ip6ifEntryExists ((UINT4) i4Fsipv6IfIndex) == IP6_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
        return SNMP_FAILURE;
    }

    pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);
    if (pIf6 == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
        return SNMP_FAILURE;
    }

    if (Ip6Testv2Fsipv6IfRouterAdvFlags (pu4ErrorCode, i4Fsipv6IfIndex,
                                         i4TestValFsipv6IfRouterAdvFlags)
        == IP6_SUCCESS)
    {
        return SNMP_SUCCESS;
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    CLI_SET_ERR (CLI_IP6_INVALID_RT_ADV_FLAGS);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6IfHopLimit
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                testValFsipv6IfHopLimit
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6IfHopLimit (UINT4 *pu4ErrorCode, INT4 i4Fsipv6IfIndex,
                           INT4 i4TestValFsipv6IfHopLimit)
{
    tIp6If             *pIf6 = NULL;

    if (Ip6ifEntryExists ((UINT4) i4Fsipv6IfIndex) == IP6_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
        return SNMP_FAILURE;
    }

    pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);
    if (pIf6 == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
        return SNMP_FAILURE;
    }

    if (i4TestValFsipv6IfHopLimit < IP6_IF_MIN_HOPLMT ||
        i4TestValFsipv6IfHopLimit > IP6_IF_MAX_HOPLMT)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_INVALID_HOPLIMIT);
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6IfDefRouterTime
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                testValFsipv6IfDefRouterTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6IfDefRouterTime (UINT4 *pu4ErrorCode, INT4 i4Fsipv6IfIndex,
                                INT4 i4TestValFsipv6IfDefRouterTime)
{

    if (Ip6ifEntryExists ((UINT4) i4Fsipv6IfIndex) == IP6_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
        return SNMP_FAILURE;
    }

    if (Ip6Testv2Fsipv6IfDefRouterTime (pu4ErrorCode, i4Fsipv6IfIndex,
                                        i4TestValFsipv6IfDefRouterTime) ==
        IP6_SUCCESS)
    {
        return SNMP_SUCCESS;
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    CLI_SET_ERR (CLI_IP6_INVALID_RA_LIFETIME);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6IfReachableTime
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                testValFsipv6IfReachableTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6IfReachableTime (UINT4 *pu4ErrorCode, INT4 i4Fsipv6IfIndex,
                                INT4 i4TestValFsipv6IfReachableTime)
{
    tIp6If             *pIf6 = NULL;
    INT4                i4ReachTime = 0;

    if (Ip6ifEntryExists ((UINT4) i4Fsipv6IfIndex) == IP6_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
        return SNMP_FAILURE;
    }

    pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);
    if (pIf6 == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
        return SNMP_FAILURE;
    }

    i4ReachTime = i4TestValFsipv6IfReachableTime * 1000;
    if (i4ReachTime < IP6_IF_MIN_REACHTIME ||
        i4ReachTime > IP6_IF_MAX_REACHTIME)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_INVALID_RA_REACHTIME);
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6IfRetransmitTime
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                testValFsipv6IfRetransmitTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6IfRetransmitTime (UINT4 *pu4ErrorCode, INT4 i4Fsipv6IfIndex,
                                 INT4 i4TestValFsipv6IfRetransmitTime)
{
    tIp6If             *pIf6 = NULL;
    INT4                i4RetTime = 0;

    if (Ip6ifEntryExists ((UINT4) i4Fsipv6IfIndex) == IP6_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);
    if (pIf6 == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
        return SNMP_FAILURE;
    }

    i4RetTime = i4TestValFsipv6IfRetransmitTime * 1000;
    if (i4RetTime < IP6_IF_MIN_RETTIME || i4RetTime > IP6_IF_MAX_RETTIME)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_INVALID_RA_RETTIME);
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6IfDelayProbeTime
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                testValFsipv6IfDelayProbeTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6IfDelayProbeTime (UINT4 *pu4ErrorCode, INT4 i4Fsipv6IfIndex,
                                 INT4 i4TestValFsipv6IfDelayProbeTime)
{
    tIp6If             *pIf6 = NULL;

    if (Ip6ifEntryExists ((UINT4) i4Fsipv6IfIndex) == IP6_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);
    if (pIf6 == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (i4TestValFsipv6IfDelayProbeTime < IP6_IF_MIN_PDTIME ||
        i4TestValFsipv6IfDelayProbeTime > IP6_IF_MAX_PDTIME)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6IfPrefixAdvStatus
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                testValFsipv6IfPrefixAdvStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6IfPrefixAdvStatus (UINT4 *pu4ErrorCode, INT4 i4Fsipv6IfIndex,
                                  INT4 i4TestValFsipv6IfPrefixAdvStatus)
{

    if (Ip6ifEntryExists ((UINT4) i4Fsipv6IfIndex) == IP6_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (Ip6Testv2Fsipv6IfPrefixAdvStatus (pu4ErrorCode, i4Fsipv6IfIndex,
                                          i4TestValFsipv6IfPrefixAdvStatus)
        == IP6_SUCCESS)
    {
        return SNMP_SUCCESS;
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6IfMinRouterAdvTime
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                testValFsipv6IfMinRouterAdvTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6IfMinRouterAdvTime (UINT4 *pu4ErrorCode, INT4 i4Fsipv6IfIndex,
                                   INT4 i4TestValFsipv6IfMinRouterAdvTime)
{

    if (Ip6ifEntryExists ((UINT4) i4Fsipv6IfIndex) == IP6_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (Ip6Testv2Fsipv6IfMinRouterAdvTime (pu4ErrorCode, i4Fsipv6IfIndex,
                                           i4TestValFsipv6IfMinRouterAdvTime)
        == IP6_SUCCESS)
    {
        return SNMP_SUCCESS;
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6IfMaxRouterAdvTime
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                testValFsipv6IfMaxRouterAdvTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6IfMaxRouterAdvTime (UINT4 *pu4ErrorCode, INT4 i4Fsipv6IfIndex,
                                   INT4 i4TestValFsipv6IfMaxRouterAdvTime)
{

    if (Ip6ifEntryExists ((UINT4) i4Fsipv6IfIndex) == IP6_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
        return SNMP_FAILURE;
    }

    if (Ip6Testv2Fsipv6IfMaxRouterAdvTime (pu4ErrorCode, i4Fsipv6IfIndex,
                                           i4TestValFsipv6IfMaxRouterAdvTime)
        == IP6_SUCCESS)
    {
        return SNMP_SUCCESS;
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    CLI_SET_ERR (CLI_IP6_INVALID_RA_INTERVAL);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6IfDADRetries
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                testValFsipv6IfDADRetries
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6IfDADRetries (UINT4 *pu4ErrorCode, INT4 i4Fsipv6IfIndex,
                             INT4 i4TestValFsipv6IfDADRetries)
{
    tIp6If             *pIf6 = NULL;

    if (Ip6ifEntryExists ((UINT4) i4Fsipv6IfIndex) == IP6_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
        return SNMP_FAILURE;
    }

    pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);
    if (pIf6 == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
        return SNMP_FAILURE;
    }

    if (i4TestValFsipv6IfDADRetries >= IP6_IF_MIN_DAD_SEND &&
        i4TestValFsipv6IfDADRetries <= IP6_IF_MAX_DAD_SEND)
    {
        return (SNMP_SUCCESS);
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    CLI_SET_ERR (CLI_IP6_INVALID_DAD_RETRIES);
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6IfForwarding
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                testValFsipv6IfForwarding
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6IfForwarding (UINT4 *pu4ErrorCode, INT4 i4Fsipv6IfIndex,
                             INT4 i4TestValFsipv6IfForwarding)
{
    tIp6If             *pIf6 = NULL;

    if (Ip6ifEntryExists ((UINT4) i4Fsipv6IfIndex) == IP6_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
        return SNMP_FAILURE;
    }

    pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);
    if (pIf6 == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
        return SNMP_FAILURE;
    }

    if (i4TestValFsipv6IfForwarding == IP6_IF_FORW_ENABLE ||
        i4TestValFsipv6IfForwarding == IP6_IF_FORW_DISABLE)
    {
        return (SNMP_SUCCESS);
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6IfIcmpErrInterval
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                testValFsipv6IfIcmpErrInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6IfIcmpErrInterval (UINT4 *pu4ErrorCode, INT4 i4Fsipv6IfIndex,
                                  INT4 u4TestValFsipv6IfIcmpErrInterval)
{

    if (Ip6ifEntryExists ((UINT4) i4Fsipv6IfIndex) == IP6_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((u4TestValFsipv6IfIcmpErrInterval >= ICMP6_RATE_LIMIT_DISABLE) &&
        (u4TestValFsipv6IfIcmpErrInterval <= ICMP6_MAX_ERR_INTERVAL))
    {
        return SNMP_SUCCESS;
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6IfIcmpTokenBucketSize
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                testValFsipv6IfIcmpTokenBucketSize
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6IfIcmpTokenBucketSize (UINT4 *pu4ErrorCode, INT4 i4Fsipv6IfIndex,
                                      INT4 i4TestValFsipv6IfIcmpTokenBucketSize)
{

    if (Ip6ifEntryExists ((UINT4) i4Fsipv6IfIndex) == IP6_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsipv6IfIcmpTokenBucketSize >= ICMP6_RATE_LIMIT_DISABLE) &&
        (i4TestValFsipv6IfIcmpTokenBucketSize <= ICMP6_MAX_BUCKET_SIZE))
    {
        return SNMP_SUCCESS;
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6IfDestUnreachableMsg
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                testValFsipv6IfDestUnreachableMsg
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6IfDestUnreachableMsg (UINT4 *pu4ErrorCode, INT4 i4Fsipv6IfIndex,
                                     INT4 i4TestValFsipv6IfDestUnreachableMsg)
{

    if (Ip6ifEntryExists ((UINT4) i4Fsipv6IfIndex) == IP6_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsipv6IfDestUnreachableMsg < ICMP6_DEST_UNREACHABLE_ENABLE) ||
        (i4TestValFsipv6IfDestUnreachableMsg > ICMP6_DEST_UNREACHABLE_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6IfUnnumAssocIPIf
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                testValFsipv6IfUnnumAssocIPIf
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6IfUnnumAssocIPIf (UINT4 *pu4ErrorCode,
                                 INT4 i4Fsipv6IfIndex,
                                 INT4 i4TestValFsipv6IfUnnumAssocIPIf)
{
    tIp6If             *pIf6 = NULL;
    tIp6If             *pAssocIf6 = NULL;

    pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);
    if (pIf6 == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
        return SNMP_FAILURE;
    }

    if (pIf6->u1IfType == IP6_LOOPBACK_INTERFACE_TYPE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_LOOPBACK_UNNUM_NOT_ALLOWED);
        return SNMP_FAILURE;
    }

    if (TMO_SLL_Count (&pIf6->addr6Ilist) > 0)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        CLI_SET_ERR (CLI_IP6_ADDR_PRESENT_FOR_UNNUM);
        return SNMP_FAILURE;
    }

    /* To Unassociate unnumbered interface the value of
     * i4TestValFsipv6IfUnnumAssocIPIf should be 0 */
    if (i4TestValFsipv6IfUnnumAssocIPIf == 0)
    {
        return SNMP_SUCCESS;
    }

    pAssocIf6 = Ip6ifGetEntry ((UINT4) i4TestValFsipv6IfUnnumAssocIPIf);
    if (pAssocIf6 == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_INVALID_UNNUM_ASSOC_IF);
        return SNMP_FAILURE;
    }

    if ((pAssocIf6->u1IfType != IP6_L3VLAN_INTERFACE_TYPE) &&
        (pAssocIf6->u1IfType != IP6_LAGG_INTERFACE_TYPE) &&
        (pAssocIf6->u1IfType != IP6_ENET_INTERFACE_TYPE) &&
        (pAssocIf6->u1IfType != IP6_LOOPBACK_INTERFACE_TYPE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_INVALID_UNNUM_INDEX_TYPE);
        return SNMP_FAILURE;
    }

    if (TMO_SLL_Count (&pAssocIf6->addr6Ilist) == 0)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_NO_UNNUM_INDEX_ADDR);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6IfRedirectMsg
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                testValFsipv6IfRedirectMsg
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6IfRedirectMsg (UINT4 *pu4ErrorCode, INT4 i4Fsipv6IfIndex,
                              INT4 i4TestValFsipv6IfRedirectMsg)
{

    if (Ip6ifEntryExists ((UINT4) i4Fsipv6IfIndex) == IP6_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsipv6IfRedirectMsg == ICMP6_REDIRECT_ENABLE) ||
        (i4TestValFsipv6IfRedirectMsg == ICMP6_REDIRECT_DISABLE))
    {
        return SNMP_SUCCESS;
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6IfAdvSrcLLAdr
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                testValFsipv6IfAdvSrcLLAdr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6IfAdvSrcLLAdr (UINT4 *pu4ErrorCode, INT4 i4Fsipv6IfIndex,
                              INT4 i4TestValFsipv6IfAdvSrcLLAdr)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (i4TestValFsipv6IfAdvSrcLLAdr);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6IfAdvIntOpt
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                testValFsipv6IfAdvIntOpt
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6IfAdvIntOpt (UINT4 *pu4ErrorCode, INT4 i4Fsipv6IfIndex,
                            INT4 i4TestValFsipv6IfAdvIntOpt)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (i4TestValFsipv6IfAdvIntOpt);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6IfNDProxyAdminStatus
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                testValFsipv6IfNDProxyAdminStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6IfNDProxyAdminStatus (UINT4 *pu4ErrorCode,
                                     INT4 i4Fsipv6IfIndex,
                                     INT4 i4TestValFsipv6IfNDProxyAdminStatus)
{
    tIp6If             *pIf6 = NULL;

    pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6 == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
        return SNMP_FAILURE;
    }

    if (pIf6->u1AdminStatus == ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_NO_ACCESS;
        CLI_SET_ERR (CLI_IP6_INVALID_ADMIN_STATUS);
        return SNMP_FAILURE;
    }

    if ((pIf6->u1SeNDStatus == ND6_SECURE_ENABLE) ||
        (pIf6->u1SeNDStatus == ND6_SECURE_MIXED))
    {
        *pu4ErrorCode = SNMP_ERR_NO_ACCESS;
        CLI_SET_ERR (CLI_IP6_PROXY_SEND_DISABLE);
        return SNMP_FAILURE;
    }

    if ((i4TestValFsipv6IfNDProxyAdminStatus != ND6_IF_PROXY_ADMIN_UP) &&
        (i4TestValFsipv6IfNDProxyAdminStatus != ND6_IF_PROXY_ADMIN_DOWN))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_INVALID_NDPROXY_STATUS);
        return SNMP_FAILURE;
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6IfNDProxyMode
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                testValFsipv6IfNDProxyMode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6IfNDProxyMode (UINT4 *pu4ErrorCode,
                              INT4 i4Fsipv6IfIndex,
                              INT4 i4TestValFsipv6IfNDProxyMode)
{
    tIp6If             *pIf6 = NULL;

    pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6 == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
        return SNMP_FAILURE;
    }

    if (pIf6->u1AdminStatus == ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_NO_ACCESS;
        CLI_SET_ERR (CLI_IP6_INVALID_ADMIN_STATUS);
        return SNMP_FAILURE;
    }

    if ((i4TestValFsipv6IfNDProxyMode != ND6_PROXY_MODE_GLOBAL) &&
        (i4TestValFsipv6IfNDProxyMode != ND6_PROXY_MODE_LOCAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_INVALID_NDPROXY_MODE);
        return SNMP_FAILURE;
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6IfNDProxyUpStream
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                testValFsipv6IfNDProxyUpStream
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6IfNDProxyUpStream (UINT4 *pu4ErrorCode,
                                  INT4 i4Fsipv6IfIndex,
                                  INT4 i4TestValFsipv6IfNDProxyUpStream)
{
    tIp6If             *pIf6 = NULL;

    pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6 == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
        return SNMP_FAILURE;
    }

    if (pIf6->u1AdminStatus == ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_NO_ACCESS;
        CLI_SET_ERR (CLI_IP6_INVALID_ADMIN_STATUS);
        return SNMP_FAILURE;
    }

    if ((i4TestValFsipv6IfNDProxyUpStream != ND6_PROXY_IF_UPSTREAM) &&
        (i4TestValFsipv6IfNDProxyUpStream != ND6_PROXY_IF_DOWNSTREAM))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_INVALID_NDPROXY_UPSTREAM);
        return SNMP_FAILURE;
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6IfSENDSecStatus
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                testValFsipv6IfSENDSecStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6IfSENDSecStatus (UINT4 *pu4ErrorCode, INT4 i4Fsipv6IfIndex,
                                INT4 i4TestValFsipv6IfSENDSecStatus)
{
    tIp6If             *pIf6 = NULL;

    pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6 == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
        return SNMP_FAILURE;
    }

    if (pIf6->u1AdminStatus == ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_NO_ACCESS;
        CLI_SET_ERR (CLI_IP6_INVALID_ADMIN_STATUS);
        return SNMP_FAILURE;
    }

    if (pIf6->u1NDProxyAdminStatus == ND6_IF_PROXY_ADMIN_UP)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_SEND_PROXY_DISABLE);
        return SNMP_FAILURE;
    }
#ifdef SSL_WANTED
    if (SslArCheckRsaKey () == SSL_FAILURE)
    {
        CLI_SET_ERR (CLI_IP6_SEND_RSA_ERROR);
        return SNMP_FAILURE;
    }
#else
    return SNMP_FAILURE;
#endif

    if ((i4TestValFsipv6IfSENDSecStatus != ND6_SECURE_ENABLE) &&
        (i4TestValFsipv6IfSENDSecStatus != ND6_SECURE_MIXED) &&
        (i4TestValFsipv6IfSENDSecStatus != ND6_SECURE_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_INVALID_SEND_MODE);
        return SNMP_FAILURE;
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6IfSENDDeltaTimestamp
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                testValFsipv6IfSENDDeltaTimestamp
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6IfSENDDeltaTimestamp (UINT4 *pu4ErrorCode, INT4 i4Fsipv6IfIndex,
                                     UINT4 u4TestValFsipv6IfSENDDeltaTimestamp)
{
    tIp6If             *pIf6 = NULL;

    pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6 == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
        return SNMP_FAILURE;
    }

    if (pIf6->u1AdminStatus == ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_NO_ACCESS;
        CLI_SET_ERR (CLI_IP6_INVALID_ADMIN_STATUS);
        return SNMP_FAILURE;
    }

    if (u4TestValFsipv6IfSENDDeltaTimestamp > 1000)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_SEND_INVALID_DELTA_TIME);
        return SNMP_FAILURE;
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6IfSENDFuzzTimestamp
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                testValFsipv6IfSENDFuzzTimestamp
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6IfSENDFuzzTimestamp (UINT4 *pu4ErrorCode, INT4 i4Fsipv6IfIndex,
                                    UINT4 u4TestValFsipv6IfSENDFuzzTimestamp)
{
    tIp6If             *pIf6 = NULL;

    pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6 == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
        return SNMP_FAILURE;
    }

    if (pIf6->u1AdminStatus == ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_NO_ACCESS;
        CLI_SET_ERR (CLI_IP6_INVALID_ADMIN_STATUS);
        return SNMP_FAILURE;
    }

    if (u4TestValFsipv6IfSENDFuzzTimestamp > 10000)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_SEND_INVALID_FUZZ_TIME);
        return SNMP_FAILURE;
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6IfSENDDriftTimestamp
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                testValFsipv6IfSENDDriftTimestamp
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6IfSENDDriftTimestamp (UINT4 *pu4ErrorCode, INT4 i4Fsipv6IfIndex,
                                     UINT4 u4TestValFsipv6IfSENDDriftTimestamp)
{
    tIp6If             *pIf6 = NULL;

    pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6 == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
        return SNMP_FAILURE;
    }

    if (pIf6->u1AdminStatus == ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_NO_ACCESS;
        CLI_SET_ERR (CLI_IP6_INVALID_ADMIN_STATUS);
        return SNMP_FAILURE;
    }

    if (u4TestValFsipv6IfSENDDriftTimestamp > 100)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_SEND_INVALID_DRIFT_TIME);
        return SNMP_FAILURE;
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6IfDefRoutePreference
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                testValFsipv6IfDefRoutePreference
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6IfDefRoutePreference (UINT4 *pu4ErrorCode,
                                     INT4 i4Fsipv6IfIndex,
                                     INT4 i4TestValFsipv6IfDefRoutePreference)
{
    tIp6If             *pIf6 = NULL;

    pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6 == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
        return SNMP_FAILURE;
    }

    if (pIf6->u1AdminStatus == ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_NO_ACCESS;
        CLI_SET_ERR (CLI_IP6_INVALID_ADMIN_STATUS);
        return SNMP_FAILURE;
    }

    if (i4TestValFsipv6IfDefRoutePreference != IP6_RA_ROUTE_PREF_LOW &&
        i4TestValFsipv6IfDefRoutePreference != IP6_RA_ROUTE_PREF_MED &&
        i4TestValFsipv6IfDefRoutePreference != IP6_RA_ROUTE_PREF_HIGH)
    {
        CLI_SET_ERR (CLI_IP6_INVALID_DEF_ROUT_PREF);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Fsipv6IfTable
 Input       :  The Indices
                Fsipv6IfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsipv6IfTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Fsipv6IfStatsTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsipv6IfStatsTable
 Input       :  The Indices
                Fsipv6IfStatsIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsipv6IfStatsTable (INT4 i4Fsipv6IfStatsIndex)
{
    if (Ip6ifEntryExists ((UINT4) i4Fsipv6IfStatsIndex) == IP6_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if (Ip6ifGetEntry ((UINT2) i4Fsipv6IfStatsIndex) == NULL)
    {
        return (SNMP_FAILURE);
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsipv6IfStatsTable
 Input       :  The Indices
                Fsipv6IfStatsIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsipv6IfStatsTable (INT4 *pi4Fsipv6IfStatsIndex)
{
    *pi4Fsipv6IfStatsIndex = 0;

    if (Ip6ifEntryExists (PTR_TO_U4 (pi4Fsipv6IfStatsIndex)) == IP6_SUCCESS)
    {
        return (SNMP_SUCCESS);
    }

    if (Ip6ifGetNextIndex ((UINT4 *) pi4Fsipv6IfStatsIndex) == IP6_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsipv6IfStatsTable
 Input       :  The Indices
                Fsipv6IfStatsIndex
                nextFsipv6IfStatsIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsipv6IfStatsTable (INT4 i4Fsipv6IfStatsIndex,
                                   INT4 *pi4NextFsipv6IfStatsIndex)
{
    INT4                i4Index = (i4Fsipv6IfStatsIndex) + 1;

    if ((i4Fsipv6IfStatsIndex < 0) || (i4Index < 0))
    {
        return (nmhGetFirstIndexFsipv6IfStatsTable (pi4NextFsipv6IfStatsIndex));
    }

    IP6_IF_SCAN (i4Index, (INT4) IP6_MAX_LOGICAL_IF_INDEX)
    {
        if (Ip6ifEntryExists ((UINT4) i4Index) == IP6_SUCCESS)
        {
            *pi4NextFsipv6IfStatsIndex = i4Index;
            return (SNMP_SUCCESS);
        }
    }

    return (SNMP_FAILURE);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsipv6IfStatsInReceives
 Input       :  The Indices
                Fsipv6IfStatsIndex

                The Object 
                retValFsipv6IfStatsInReceives
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfStatsInReceives (INT4 i4Fsipv6IfStatsIndex,
                               UINT4 *pu4RetValFsipv6IfStatsInReceives)
{
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfStatsIndex);

    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

#ifdef NPAPI_WANTED
    if (Ipv6FsNpIpv6GetStats
        (pIf6->pIp6Cxt->u4ContextId, (UINT4) i4Fsipv6IfStatsIndex,
         NP_STAT_IP6_IN_RECIEVES,
         pu4RetValFsipv6IfStatsInReceives) == FNP_FAILURE)
    {
        return SNMP_FAILURE;
    }
#else
    *pu4RetValFsipv6IfStatsInReceives = pIf6->stats.u4InRcvs;
#endif

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfStatsInHdrErrors
 Input       :  The Indices
                Fsipv6IfStatsIndex

                The Object 
                retValFsipv6IfStatsInHdrErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfStatsInHdrErrors (INT4 i4Fsipv6IfStatsIndex,
                                UINT4 *pu4RetValFsipv6IfStatsInHdrErrors)
{
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfStatsIndex);

    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsipv6IfStatsInHdrErrors = pIf6->stats.u4InHdrerrs;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfStatsTooBigErrors
 Input       :  The Indices
                Fsipv6IfStatsIndex

                The Object 
                retValFsipv6IfStatsTooBigErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfStatsTooBigErrors (INT4 i4Fsipv6IfStatsIndex,
                                 UINT4 *pu4RetValFsipv6IfStatsTooBigErrors)
{
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfStatsIndex);

    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

#ifdef NPAPI_WANTED
    if (Ipv6FsNpIpv6GetStats
        (pIf6->pIp6Cxt->u4ContextId, (UINT4) i4Fsipv6IfStatsIndex,
         NP_STAT_IP6_TOO_BIG_ERROR,
         pu4RetValFsipv6IfStatsTooBigErrors) == FNP_FAILURE)
    {
        return SNMP_FAILURE;
    }
#else
    *pu4RetValFsipv6IfStatsTooBigErrors = pIf6->stats.u4TooBigerrs;
#endif

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfStatsInAddrErrors
 Input       :  The Indices
                Fsipv6IfStatsIndex

                The Object 
                retValFsipv6IfStatsInAddrErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfStatsInAddrErrors (INT4 i4Fsipv6IfStatsIndex,
                                 UINT4 *pu4RetValFsipv6IfStatsInAddrErrors)
{
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfStatsIndex);

    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsipv6IfStatsInAddrErrors = pIf6->stats.u4InAddrerrs;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfStatsForwDatagrams
 Input       :  The Indices
                Fsipv6IfStatsIndex

                The Object 
                retValFsipv6IfStatsForwDatagrams
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfStatsForwDatagrams (INT4 i4Fsipv6IfStatsIndex,
                                  UINT4 *pu4RetValFsipv6IfStatsForwDatagrams)
{
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfStatsIndex);

    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

#ifdef NPAPI_WANTED
    if (Ipv6FsNpIpv6GetStats
        (pIf6->pIp6Cxt->u4ContextId, (UINT4) i4Fsipv6IfStatsIndex,
         NP_STAT_IP6_FORW_DATAGRAMS,
         pu4RetValFsipv6IfStatsForwDatagrams) == FNP_FAILURE)
    {
        return SNMP_FAILURE;
    }
#else
    *pu4RetValFsipv6IfStatsForwDatagrams = pIf6->stats.u4ForwDgrams;
#endif

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfStatsInUnknownProtos
 Input       :  The Indices
                Fsipv6IfStatsIndex

                The Object 
                retValFsipv6IfStatsInUnknownProtos
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfStatsInUnknownProtos (INT4 i4Fsipv6IfStatsIndex,
                                    UINT4
                                    *pu4RetValFsipv6IfStatsInUnknownProtos)
{
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfStatsIndex);

    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsipv6IfStatsInUnknownProtos = pIf6->stats.u4InUnkprots;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfStatsInDiscards
 Input       :  The Indices
                Fsipv6IfStatsIndex

                The Object 
                retValFsipv6IfStatsInDiscards
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfStatsInDiscards (INT4 i4Fsipv6IfStatsIndex,
                               UINT4 *pu4RetValFsipv6IfStatsInDiscards)
{
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfStatsIndex);

    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

#ifdef NPAPI_WANTED
    if (Ipv6FsNpIpv6GetStats
        (pIf6->pIp6Cxt->u4ContextId, (UINT4) i4Fsipv6IfStatsIndex,
         NP_STAT_IP6_IN_DISCARDS,
         pu4RetValFsipv6IfStatsInDiscards) == FNP_FAILURE)
    {
        return SNMP_FAILURE;
    }
#else
    *pu4RetValFsipv6IfStatsInDiscards = pIf6->stats.u4InDiscards;
#endif

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfStatsInDelivers
 Input       :  The Indices
                Fsipv6IfStatsIndex

                The Object 
                retValFsipv6IfStatsInDelivers
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfStatsInDelivers (INT4 i4Fsipv6IfStatsIndex,
                               UINT4 *pu4RetValFsipv6IfStatsInDelivers)
{
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfStatsIndex);

    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsipv6IfStatsInDelivers = pIf6->stats.u4InDelivers;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfStatsOutRequests
 Input       :  The Indices
                Fsipv6IfStatsIndex

                The Object 
                retValFsipv6IfStatsOutRequests
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfStatsOutRequests (INT4 i4Fsipv6IfStatsIndex,
                                UINT4 *pu4RetValFsipv6IfStatsOutRequests)
{
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfStatsIndex);

    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsipv6IfStatsOutRequests = pIf6->stats.u4OutReqs;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfStatsOutDiscards
 Input       :  The Indices
                Fsipv6IfStatsIndex

                The Object 
                retValFsipv6IfStatsOutDiscards
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfStatsOutDiscards (INT4 i4Fsipv6IfStatsIndex,
                                UINT4 *pu4RetValFsipv6IfStatsOutDiscards)
{
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfStatsIndex);

    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsipv6IfStatsOutDiscards = pIf6->stats.u4OutDiscards;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfStatsOutNoRoutes
 Input       :  The Indices
                Fsipv6IfStatsIndex

                The Object 
                retValFsipv6IfStatsOutNoRoutes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfStatsOutNoRoutes (INT4 i4Fsipv6IfStatsIndex,
                                UINT4 *pu4RetValFsipv6IfStatsOutNoRoutes)
{
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfStatsIndex);

    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsipv6IfStatsOutNoRoutes = pIf6->stats.u4OutNorts;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfStatsReasmReqds
 Input       :  The Indices
                Fsipv6IfStatsIndex

                The Object 
                retValFsipv6IfStatsReasmReqds
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfStatsReasmReqds (INT4 i4Fsipv6IfStatsIndex,
                               UINT4 *pu4RetValFsipv6IfStatsReasmReqds)
{
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfStatsIndex);

    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsipv6IfStatsReasmReqds = pIf6->stats.u4Reasmreqs;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfStatsReasmOKs
 Input       :  The Indices
                Fsipv6IfStatsIndex

                The Object 
                retValFsipv6IfStatsReasmOKs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfStatsReasmOKs (INT4 i4Fsipv6IfStatsIndex,
                             UINT4 *pu4RetValFsipv6IfStatsReasmOKs)
{
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfStatsIndex);

    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsipv6IfStatsReasmOKs = pIf6->stats.u4Reasmoks;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfStatsReasmFails
 Input       :  The Indices
                Fsipv6IfStatsIndex

                The Object 
                retValFsipv6IfStatsReasmFails
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfStatsReasmFails (INT4 i4Fsipv6IfStatsIndex,
                               UINT4 *pu4RetValFsipv6IfStatsReasmFails)
{
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfStatsIndex);

    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsipv6IfStatsReasmFails = pIf6->stats.u4Reasmfails;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfStatsFragOKs
 Input       :  The Indices
                Fsipv6IfStatsIndex

                The Object 
                retValFsipv6IfStatsFragOKs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfStatsFragOKs (INT4 i4Fsipv6IfStatsIndex,
                            UINT4 *pu4RetValFsipv6IfStatsFragOKs)
{
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfStatsIndex);

    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsipv6IfStatsFragOKs = pIf6->stats.u4Fragoks;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfStatsFragFails
 Input       :  The Indices
                Fsipv6IfStatsIndex

                The Object 
                retValFsipv6IfStatsFragFails
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfStatsFragFails (INT4 i4Fsipv6IfStatsIndex,
                              UINT4 *pu4RetValFsipv6IfStatsFragFails)
{
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfStatsIndex);

    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsipv6IfStatsFragFails = pIf6->stats.u4Fragfails;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfStatsFragCreates
 Input       :  The Indices
                Fsipv6IfStatsIndex

                The Object 
                retValFsipv6IfStatsFragCreates
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfStatsFragCreates (INT4 i4Fsipv6IfStatsIndex,
                                UINT4 *pu4RetValFsipv6IfStatsFragCreates)
{
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfStatsIndex);

    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsipv6IfStatsFragCreates = pIf6->stats.u4Fragcreates;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfStatsInMcastPkts
 Input       :  The Indices
                Fsipv6IfStatsIndex

                The Object 
                retValFsipv6IfStatsInMcastPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfStatsInMcastPkts (INT4 i4Fsipv6IfStatsIndex,
                                UINT4 *pu4RetValFsipv6IfStatsInMcastPkts)
{
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfStatsIndex);

    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

#ifdef NPAPI_WANTED
    if (Ipv6FsNpIpv6GetStats
        (pIf6->pIp6Cxt->u4ContextId, (UINT4) i4Fsipv6IfStatsIndex,
         NP_STAT_IP6_IN_MCAST_RECIEVES,
         pu4RetValFsipv6IfStatsInMcastPkts) == FNP_FAILURE)
    {
        return SNMP_FAILURE;
    }
#else
    *pu4RetValFsipv6IfStatsInMcastPkts = pIf6->stats.u4InMcasts;
#endif

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfStatsOutMcastPkts
 Input       :  The Indices
                Fsipv6IfStatsIndex

                The Object 
                retValFsipv6IfStatsOutMcastPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfStatsOutMcastPkts (INT4 i4Fsipv6IfStatsIndex,
                                 UINT4 *pu4RetValFsipv6IfStatsOutMcastPkts)
{
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfStatsIndex);

    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

#ifdef NPAPI_WANTED
    if (Ipv6FsNpIpv6GetStats
        (pIf6->pIp6Cxt->u4ContextId, (UINT4) i4Fsipv6IfStatsIndex,
         NP_STAT_IP6_OUT_MCAST_RECIEVES,
         pu4RetValFsipv6IfStatsOutMcastPkts) == FNP_FAILURE)
    {
        return SNMP_FAILURE;
    }
#else
    *pu4RetValFsipv6IfStatsOutMcastPkts = pIf6->stats.u4OutMcasts;
#endif

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfStatsInTruncatedPkts
 Input       :  The Indices
                Fsipv6IfStatsIndex

                The Object 
                retValFsipv6IfStatsInTruncatedPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfStatsInTruncatedPkts (INT4 i4Fsipv6IfStatsIndex,
                                    UINT4
                                    *pu4RetValFsipv6IfStatsInTruncatedPkts)
{
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfStatsIndex);

    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsipv6IfStatsInTruncatedPkts = pIf6->stats.u4InTruncs;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfStatsInRouterSols
 Input       :  The Indices
                Fsipv6IfStatsIndex

                The Object 
                retValFsipv6IfStatsInRouterSols
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfStatsInRouterSols (INT4 i4Fsipv6IfStatsIndex,
                                 UINT4 *pu4RetValFsipv6IfStatsInRouterSols)
{
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfStatsIndex);

    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsipv6IfStatsInRouterSols = pIf6->stats.u4InRoutsols;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfStatsInRouterAdvs
 Input       :  The Indices
                Fsipv6IfStatsIndex

                The Object 
                retValFsipv6IfStatsInRouterAdvs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfStatsInRouterAdvs (INT4 i4Fsipv6IfStatsIndex,
                                 UINT4 *pu4RetValFsipv6IfStatsInRouterAdvs)
{
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfStatsIndex);

    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsipv6IfStatsInRouterAdvs = pIf6->stats.u4InRoutadvs;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfStatsInNeighSols
 Input       :  The Indices
                Fsipv6IfStatsIndex

                The Object 
                retValFsipv6IfStatsInNeighSols
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfStatsInNeighSols (INT4 i4Fsipv6IfStatsIndex,
                                UINT4 *pu4RetValFsipv6IfStatsInNeighSols)
{
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfStatsIndex);

    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsipv6IfStatsInNeighSols = pIf6->stats.u4InNsols;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfStatsInNeighAdvs
 Input       :  The Indices
                Fsipv6IfStatsIndex

                The Object 
                retValFsipv6IfStatsInNeighAdvs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfStatsInNeighAdvs (INT4 i4Fsipv6IfStatsIndex,
                                UINT4 *pu4RetValFsipv6IfStatsInNeighAdvs)
{
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfStatsIndex);

    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsipv6IfStatsInNeighAdvs = pIf6->stats.u4InNadvs;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfStatsInRedirects
 Input       :  The Indices
                Fsipv6IfStatsIndex

                The Object 
                retValFsipv6IfStatsInRedirects
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfStatsInRedirects (INT4 i4Fsipv6IfStatsIndex,
                                UINT4 *pu4RetValFsipv6IfStatsInRedirects)
{
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfStatsIndex);

    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsipv6IfStatsInRedirects = pIf6->stats.u4InRoutRedirs;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfStatsOutRouterSols
 Input       :  The Indices
                Fsipv6IfStatsIndex

                The Object 
                retValFsipv6IfStatsOutRouterSols
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfStatsOutRouterSols (INT4 i4Fsipv6IfStatsIndex,
                                  UINT4 *pu4RetValFsipv6IfStatsOutRouterSols)
{
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfStatsIndex);

    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsipv6IfStatsOutRouterSols = pIf6->stats.u4OutRoutSols;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfStatsOutRouterAdvs
 Input       :  The Indices
                Fsipv6IfStatsIndex

                The Object 
                retValFsipv6IfStatsOutRouterAdvs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfStatsOutRouterAdvs (INT4 i4Fsipv6IfStatsIndex,
                                  UINT4 *pu4RetValFsipv6IfStatsOutRouterAdvs)
{
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfStatsIndex);

    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsipv6IfStatsOutRouterAdvs = pIf6->stats.u4OutRoutadvs;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfStatsOutNeighSols
 Input       :  The Indices
                Fsipv6IfStatsIndex

                The Object 
                retValFsipv6IfStatsOutNeighSols
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfStatsOutNeighSols (INT4 i4Fsipv6IfStatsIndex,
                                 UINT4 *pu4RetValFsipv6IfStatsOutNeighSols)
{
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfStatsIndex);

    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsipv6IfStatsOutNeighSols = pIf6->stats.u4OutNsols;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfStatsOutNeighAdvs
 Input       :  The Indices
                Fsipv6IfStatsIndex

                The Object 
                retValFsipv6IfStatsOutNeighAdvs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfStatsOutNeighAdvs (INT4 i4Fsipv6IfStatsIndex,
                                 UINT4 *pu4RetValFsipv6IfStatsOutNeighAdvs)
{
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfStatsIndex);

    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsipv6IfStatsOutNeighAdvs = pIf6->stats.u4OutNadvs;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfStatsOutRedirects
 Input       :  The Indices
                Fsipv6IfStatsIndex

                The Object 
                retValFsipv6IfStatsOutRedirects
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfStatsOutRedirects (INT4 i4Fsipv6IfStatsIndex,
                                 UINT4 *pu4RetValFsipv6IfStatsOutRedirects)
{
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfStatsIndex);

    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsipv6IfStatsOutRedirects = pIf6->stats.u4OutRedirs;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfStatsLastRouterAdvTime
 Input       :  The Indices
                Fsipv6IfStatsIndex

                The Object 
                retValFsipv6IfStatsLastRouterAdvTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfStatsLastRouterAdvTime (INT4 i4Fsipv6IfStatsIndex,
                                      UINT4
                                      *pu4RetValFsipv6IfStatsLastRouterAdvTime)
{
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfStatsIndex);

    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsipv6IfStatsLastRouterAdvTime = pIf6->stats.u4RaSentTime;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfStatsNextRouterAdvTime
 Input       :  The Indices
                Fsipv6IfStatsIndex

                The Object 
                retValFsipv6IfStatsNextRouterAdvTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfStatsNextRouterAdvTime (INT4 i4Fsipv6IfStatsIndex,
                                      UINT4
                                      *pu4RetValFsipv6IfStatsNextRouterAdvTime)
{
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfStatsIndex);

    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsipv6IfStatsNextRouterAdvTime = pIf6->stats.u4RaSchedTime;
    return SNMP_SUCCESS;
}

/****************************************************************************
 *  Function    :  nmhGetFsipv6IfStatsIcmp6ErrRateLmtd
 *  Input       :  The Indices
 *                 Fsipv6IfStatsIndex
 *                 The Object
 *                 retValFsipv6IfStatsIcmp6ErrRateLmtd
 *  Output      :  The Get Low Lev Routine Take the Indices &
 *                 store the Value requested in the Return val.
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
INT1
nmhGetFsipv6IfStatsIcmp6ErrRateLmtd (INT4 i4Fsipv6IfStatsIndex,
                                     UINT4
                                     *pu4RetValFsipv6IfStatsIcmp6ErrRateLmtd)
{
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfStatsIndex);

    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsipv6IfStatsIcmp6ErrRateLmtd = pIf6->stats.u4Icmp6RLErrMsgCnt;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsipv6IfStatsSENDDroppedPkts
 Input       :  The Indices
                Fsipv6IfStatsIndex

                The Object 
                retValFsipv6IfStatsSENDDroppedPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfStatsSENDDroppedPkts (INT4 i4Fsipv6IfStatsIndex,
                                    UINT4
                                    *pu4RetValFsipv6IfStatsSENDDroppedPkts)
{
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfStatsIndex);
    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsipv6IfStatsSENDDroppedPkts = pIf6->stats.u4NdSecureDroppedPkts;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfStatsSENDInvalidPkts
 Input       :  The Indices
                Fsipv6IfStatsIndex

                The Object 
                retValFsipv6IfStatsSENDInvalidPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfStatsSENDInvalidPkts (INT4 i4Fsipv6IfStatsIndex,
                                    UINT4
                                    *pu4RetValFsipv6IfStatsSENDInvalidPkts)
{
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfStatsIndex);
    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsipv6IfStatsSENDInvalidPkts = pIf6->stats.u4NdSecureInvalidPkts;

    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Fsipv6PrefixTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsipv6PrefixTable
 Input       :  The Indices
                Fsipv6PrefixIndex
                Fsipv6PrefixAddress
                Fsipv6PrefixAddrLen
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */
INT1
nmhValidateIndexInstanceFsipv6PrefixTable (INT4 i4Fsipv6PrefixIndex,
                                           tSNMP_OCTET_STRING_TYPE *
                                           pFsipv6PrefixAddress,
                                           INT4 i4Fsipv6PrefixAddrLen)
{
    INT1                i1AddrType;

    /* Interface Index Validation */
    if (Ip6ifEntryExists ((UINT4) i4Fsipv6PrefixIndex) == IP6_FAILURE)
    {
        return SNMP_FAILURE;
    }

    /* Prefix Validation */
    i1AddrType = Ip6AddrType ((tIp6Addr *) (VOID *)
                              pFsipv6PrefixAddress->pu1_OctetList);
    if (i1AddrType != ADDR6_UNICAST)
    {
        return SNMP_FAILURE;
    }

    /* Prefix Length Validation. */
    if ((i4Fsipv6PrefixAddrLen <= 0) ||
        (i4Fsipv6PrefixAddrLen > IP6_ADDR_MAX_PREFIX))
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsipv6PrefixTable
 Input       :  The Indices
                Fsipv6PrefixIndex
                Fsipv6PrefixAddress
                Fsipv6PrefixAddrLen
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */
INT1
nmhGetFirstIndexFsipv6PrefixTable (INT4 *pi4Fsipv6PrefixIndex,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsipv6PrefixAddress,
                                   INT4 *pi4Fsipv6PrefixAddrLen)
{
    tIp6PrefixNode     *pPrefix = NULL;
    UINT4               u4Index = 1;

    IP6_IF_SCAN (u4Index, IP6_MAX_LOGICAL_IF_INDEX)
    {
        pPrefix = Ip6GetFirstPrefix (u4Index);
        if (pPrefix != NULL)
        {
            *pi4Fsipv6PrefixIndex = (INT4) u4Index;
            MEMCPY (pFsipv6PrefixAddress->pu1_OctetList, &pPrefix->Ip6Prefix,
                    IP6_ADDR_SIZE);
            pFsipv6PrefixAddress->i4_Length = IP6_ADDR_SIZE;
            *pi4Fsipv6PrefixAddrLen = pPrefix->u1PrefixLen;
            return SNMP_SUCCESS;
        }
        continue;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsipv6PrefixTable
 Input       :  The Indices
                Fsipv6PrefixIndex
                nextFsipv6PrefixIndex
                Fsipv6PrefixAddress
                nextFsipv6PrefixAddress
                Fsipv6PrefixAddrLen
                nextFsipv6PrefixAddrLen
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsipv6PrefixTable (INT4 i4Fsipv6PrefixIndex,
                                  INT4 *pi4NextFsipv6PrefixIndex,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pFsipv6PrefixAddress,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pNextFsipv6PrefixAddress,
                                  INT4 i4Fsipv6PrefixAddrLen,
                                  INT4 *pi4NextFsipv6PrefixAddrLen)
{
    tIp6PrefixNode     *pNextPrefix = NULL;
    UINT4               u4Index;

    if (i4Fsipv6PrefixIndex < 0)
    {
        return (SNMP_FAILURE);
    }

    pNextPrefix = Ip6GetNextPrefix ((UINT4) i4Fsipv6PrefixIndex,
                                    (tIp6Addr *) (VOID *) pFsipv6PrefixAddress->
                                    pu1_OctetList,
                                    (UINT1) i4Fsipv6PrefixAddrLen);
    if (pNextPrefix != NULL)
    {
        *pi4NextFsipv6PrefixIndex = i4Fsipv6PrefixIndex;
        MEMCPY (pNextFsipv6PrefixAddress->pu1_OctetList,
                &pNextPrefix->Ip6Prefix, IP6_ADDR_SIZE);
        pNextFsipv6PrefixAddress->i4_Length = IP6_ADDR_SIZE;
        *pi4NextFsipv6PrefixAddrLen = pNextPrefix->u1PrefixLen;
        return SNMP_SUCCESS;
    }

    u4Index = i4Fsipv6PrefixIndex + 1;
    IP6_IF_SCAN (u4Index, IP6_MAX_LOGICAL_IF_INDEX)
    {
        pNextPrefix = Ip6GetFirstPrefix (u4Index);
        if (pNextPrefix != NULL)
        {
            *pi4NextFsipv6PrefixIndex = (INT4) u4Index;
            MEMCPY (pNextFsipv6PrefixAddress->pu1_OctetList,
                    &pNextPrefix->Ip6Prefix, IP6_ADDR_SIZE);
            pNextFsipv6PrefixAddress->i4_Length = IP6_ADDR_SIZE;
            *pi4NextFsipv6PrefixAddrLen = pNextPrefix->u1PrefixLen;
            return SNMP_SUCCESS;
        }
        continue;
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsipv6PrefixProfileIndex
 Input       :  The Indices
                Fsipv6PrefixIndex
                Fsipv6PrefixAddress
                Fsipv6PrefixAddrLen

                The Object 
                retValFsipv6PrefixProfileIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6PrefixProfileIndex (INT4 i4Fsipv6PrefixIndex,
                                tSNMP_OCTET_STRING_TYPE * pFsipv6PrefixAddress,
                                INT4 i4Fsipv6PrefixAddrLen,
                                INT4 *pi4RetValFsipv6PrefixProfileIndex)
{
    if (Ip6GetFsipv6PrefixProfileIndex (i4Fsipv6PrefixIndex,
                                        pFsipv6PrefixAddress,
                                        i4Fsipv6PrefixAddrLen,
                                        pi4RetValFsipv6PrefixProfileIndex) ==
        IP6_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6SupportEmbeddedRp
 Input       :  The Indices
                Fsipv6PrefixIndex
                Fsipv6PrefixAddress
                Fsipv6PrefixAddrLen

                The Object 
                retValFsipv6SupportEmbeddedRp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SupportEmbeddedRp (INT4 i4Fsipv6PrefixIndex,
                               tSNMP_OCTET_STRING_TYPE * pFsipv6PrefixAddress,
                               INT4 i4Fsipv6PrefixAddrLen,
                               INT4 *pi4RetValFsipv6SupportEmbeddedRp)
{
    if (Ip6GetFsipv6SupportEmbeddedRp (i4Fsipv6PrefixIndex,
                                       pFsipv6PrefixAddress,
                                       i4Fsipv6PrefixAddrLen,
                                       pi4RetValFsipv6SupportEmbeddedRp) ==
        IP6_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6PrefixAdminStatus
 Input       :  The Indices
                Fsipv6PrefixIndex
                Fsipv6PrefixAddress
                Fsipv6PrefixAddrLen

                The Object 
                retValFsipv6PrefixAdminStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6PrefixAdminStatus (INT4 i4Fsipv6PrefixIndex,
                               tSNMP_OCTET_STRING_TYPE * pFsipv6PrefixAddress,
                               INT4 i4Fsipv6PrefixAddrLen,
                               INT4 *pi4RetValFsipv6PrefixAdminStatus)
{
    if (Ip6GetFsipv6PrefixAdminStatus (i4Fsipv6PrefixIndex,
                                       pFsipv6PrefixAddress,
                                       i4Fsipv6PrefixAddrLen,
                                       pi4RetValFsipv6PrefixAdminStatus) ==
        IP6_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsipv6PrefixProfileIndex
 Input       :  The Indices
                Fsipv6PrefixIndex
                Fsipv6PrefixAddress
                Fsipv6PrefixAddrLen

                The Object 
                setValFsipv6PrefixProfileIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6PrefixProfileIndex (INT4 i4Fsipv6PrefixIndex,
                                tSNMP_OCTET_STRING_TYPE * pFsipv6PrefixAddress,
                                INT4 i4Fsipv6PrefixAddrLen,
                                INT4 i4SetValFsipv6PrefixProfileIndex)
{

    tIp6If             *pIf6 = NULL;
    pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6PrefixIndex);

    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    if (Ip6SetFsipv6PrefixProfileIndex (i4Fsipv6PrefixIndex,
                                        pFsipv6PrefixAddress,
                                        i4Fsipv6PrefixAddrLen,
                                        i4SetValFsipv6PrefixProfileIndex)
        == IP6_FAILURE)
    {
        return SNMP_FAILURE;
    }

    IncMsrForIp6PrefixTable (i4Fsipv6PrefixIndex, i4Fsipv6PrefixAddrLen,
                             pFsipv6PrefixAddress,
                             i4SetValFsipv6PrefixProfileIndex,
                             Fsipv6PrefixProfileIndex,
                             (sizeof (Fsipv6PrefixProfileIndex) /
                              sizeof (UINT4)), FALSE);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6SupportEmbeddedRp
 Input       :  The Indices
                Fsipv6PrefixIndex
                Fsipv6PrefixAddress
                Fsipv6PrefixAddrLen

                The Object 
                setValFsipv6SupportEmbeddedRp
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6SupportEmbeddedRp (INT4 i4Fsipv6PrefixIndex,
                               tSNMP_OCTET_STRING_TYPE * pFsipv6PrefixAddress,
                               INT4 i4Fsipv6PrefixAddrLen,
                               INT4 i4SetValFsipv6SupportEmbeddedRp)
{
    tIp6If             *pIf6 = NULL;
    pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6PrefixIndex);

    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    if (Ip6SetFsipv6SupportEmbeddedRp (i4Fsipv6PrefixIndex,
                                       pFsipv6PrefixAddress,
                                       i4Fsipv6PrefixAddrLen,
                                       i4SetValFsipv6SupportEmbeddedRp)
        == IP6_FAILURE)
    {
        return SNMP_FAILURE;
    }

    IncMsrForIp6PrefixTable (i4Fsipv6PrefixIndex, i4Fsipv6PrefixAddrLen,
                             pFsipv6PrefixAddress,
                             i4SetValFsipv6SupportEmbeddedRp,
                             Fsipv6SupportEmbeddedRp,
                             (sizeof (Fsipv6SupportEmbeddedRp) /
                              sizeof (UINT4)), FALSE);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6PrefixAdminStatus
 Input       :  The Indices
                Fsipv6PrefixIndex
                Fsipv6PrefixAddress
                Fsipv6PrefixAddrLen

                The Object 
                setValFsipv6PrefixAdminStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6PrefixAdminStatus (INT4 i4Fsipv6PrefixIndex,
                               tSNMP_OCTET_STRING_TYPE * pFsipv6PrefixAddress,
                               INT4 i4Fsipv6PrefixAddrLen,
                               INT4 i4SetValFsipv6PrefixAdminStatus)
{

    tIp6If             *pIf6 = NULL;
    pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6PrefixIndex);

    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    if (Ip6SetFsipv6PrefixAdminStatus (i4Fsipv6PrefixIndex,
                                       pFsipv6PrefixAddress,
                                       i4Fsipv6PrefixAddrLen,
                                       i4SetValFsipv6PrefixAdminStatus) ==
        IP6_FAILURE)
    {
        return SNMP_FAILURE;
    }

    IncMsrForIp6PrefixTable (i4Fsipv6PrefixIndex, i4Fsipv6PrefixAddrLen,
                             pFsipv6PrefixAddress,
                             i4SetValFsipv6PrefixAdminStatus,
                             Fsipv6PrefixAdminStatus,
                             (sizeof (Fsipv6PrefixAdminStatus) /
                              sizeof (UINT4)), TRUE);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Fsipv6PrefixProfileIndex
 Input       :  The Indices
                Fsipv6PrefixIndex
                Fsipv6PrefixAddress
                Fsipv6PrefixAddrLen

                The Object 
                testValFsipv6PrefixProfileIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6PrefixProfileIndex (UINT4 *pu4ErrorCode,
                                   INT4 i4Fsipv6PrefixIndex,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsipv6PrefixAddress,
                                   INT4 i4Fsipv6PrefixAddrLen,
                                   INT4 i4TestValFsipv6PrefixProfileIndex)
{
    if (Ip6Testv2Fsipv6PrefixProfileIndex (pu4ErrorCode,
                                           i4Fsipv6PrefixIndex,
                                           pFsipv6PrefixAddress,
                                           i4Fsipv6PrefixAddrLen,
                                           i4TestValFsipv6PrefixProfileIndex)
        == IP6_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6SupportEmbeddedRp
 Input       :  The Indices
                Fsipv6PrefixIndex
                Fsipv6PrefixAddress
                Fsipv6PrefixAddrLen

                The Object 
                testValFsipv6SupportEmbeddedRp
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6SupportEmbeddedRp (UINT4 *pu4ErrorCode,
                                  INT4 i4Fsipv6PrefixIndex,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pFsipv6PrefixAddress,
                                  INT4 i4Fsipv6PrefixAddrLen,
                                  INT4 i4TestValFsipv6SupportEmbeddedRp)
{
    if (Ip6Testv2Fsipv6SupportEmbeddedRp (pu4ErrorCode,
                                          i4Fsipv6PrefixIndex,
                                          pFsipv6PrefixAddress,
                                          i4Fsipv6PrefixAddrLen,
                                          i4TestValFsipv6SupportEmbeddedRp)
        == IP6_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6PrefixAdminStatus
 Input       :  The Indices
                Fsipv6PrefixIndex
                Fsipv6PrefixAddress
                Fsipv6PrefixAddrLen

                The Object 
                testValFsipv6PrefixAdminStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6PrefixAdminStatus (UINT4 *pu4ErrorCode,
                                  INT4 i4Fsipv6PrefixIndex,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pFsipv6PrefixAddress,
                                  INT4 i4Fsipv6PrefixAddrLen,
                                  INT4 i4TestValFsipv6PrefixAdminStatus)
{
    if (Ip6Testv2Fsipv6PrefixAdminStatus (pu4ErrorCode,
                                          i4Fsipv6PrefixIndex,
                                          pFsipv6PrefixAddress,
                                          i4Fsipv6PrefixAddrLen,
                                          i4TestValFsipv6PrefixAdminStatus) ==
        IP6_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Fsipv6PrefixTable
 Input       :  The Indices
                Fsipv6PrefixIndex
                Fsipv6PrefixAddress
                Fsipv6PrefixAddrLen
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsipv6PrefixTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Fsipv6AddrTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsipv6AddrTable
 Input       :  The Indices
                Fsipv6AddrIndex
                Fsipv6AddrAddress
                Fsipv6AddrPrefixLen
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsipv6AddrTable (INT4 i4Fsipv6AddrIndex,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pFsipv6AddrAddress,
                                         INT4 i4Fsipv6AddrPrefixLen)
{
    tIp6Addr            ip6addr;

    if (Ip6ifEntryExists ((UINT4) i4Fsipv6AddrIndex) == IP6_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (pFsipv6AddrAddress == NULL)
    {
        return SNMP_FAILURE;
    }

    Ip6AddrCopy (&ip6addr, (tIp6Addr *) (VOID *)
                 pFsipv6AddrAddress->pu1_OctetList);
    if (IS_ADDR_MULTI (ip6addr) || IS_ADDR_UNSPECIFIED (ip6addr))
    {
        return SNMP_FAILURE;
    }

    if ((i4Fsipv6AddrPrefixLen < IP6_ADDR_MIN_PREFIX) ||
        (i4Fsipv6AddrPrefixLen > IP6_ADDR_MAX_PREFIX))
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsipv6AddrTable
 Input       :  The Indices
                Fsipv6AddrIndex
                Fsipv6AddrAddress
                Fsipv6AddrPrefixLen
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsipv6AddrTable (INT4 *pi4Fsipv6AddrIndex,
                                 tSNMP_OCTET_STRING_TYPE * pFsipv6AddrAddress,
                                 INT4 *pi4Fsipv6AddrPrefixLen)
{
    UINT1               u1Prefix = 0;

    *pi4Fsipv6AddrIndex = 0;
    if (Ip6AddrTblNextAddr ((UINT4 *) pi4Fsipv6AddrIndex,
                            (tIp6Addr *) (VOID *) pFsipv6AddrAddress->
                            pu1_OctetList, &u1Prefix,
                            IP6_ADDR_NEXT_SRCH_ALIST) == IP6_FAILURE)
    {
        return SNMP_FAILURE;
    }

    pFsipv6AddrAddress->i4_Length = sizeof (tIp6Addr);
    *pi4Fsipv6AddrPrefixLen = u1Prefix;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsipv6AddrTable
 Input       :  The Indices
                Fsipv6AddrIndex
                nextFsipv6AddrIndex
                Fsipv6AddrAddress
                nextFsipv6AddrAddress
                Fsipv6AddrPrefixLen
                nextFsipv6AddrPrefixLen
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsipv6AddrTable (INT4 i4Fsipv6AddrIndex,
                                INT4 *pi4NextFsipv6AddrIndex,
                                tSNMP_OCTET_STRING_TYPE * pFsipv6AddrAddress,
                                tSNMP_OCTET_STRING_TYPE *
                                pNextFsipv6AddrAddress,
                                INT4 i4Fsipv6AddrPrefixLen,
                                INT4 *pi4NextFsipv6AddrPrefixLen)
{
    tIp6AddrInfo       *pAddrInfo = NULL;
    tIp6LlocalInfo     *pLlocalInfo = NULL;
    tTMO_SLL_NODE      *pCur, *pNext;
    tIp6If             *pIf6;
    INT4                i4AddrCmp = -1;
    UINT1               u1Prefix = 0;
    if (i4Fsipv6AddrIndex < 0)
    {
        return (SNMP_FAILURE);
    }

    /* If the index is 0, return the address in next available index */
    if (i4Fsipv6AddrIndex == 0)
    {
        i4Fsipv6AddrIndex++;
    }

    if (Ip6ifEntryExists ((UINT4) i4Fsipv6AddrIndex) != IP6_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    pIf6 = gIp6GblInfo.apIp6If[i4Fsipv6AddrIndex];

    if (Ip6AddrType ((tIp6Addr *) (VOID *) pFsipv6AddrAddress->pu1_OctetList) !=
        ADDR6_LLOCAL)
    {
        pNext = TMO_SLL_First (&pIf6->addr6Ilist);

        while ((pCur = pNext) != NULL)
        {
            pNext = TMO_SLL_Next (&pIf6->addr6Ilist, pCur);

            pAddrInfo = IP6_ADDR_PTR_FROM_SLL (pCur);

            /* If the given addr is equal to the addr in list
             * return the next addr in the list */
            if ((MEMCMP
                 (&pAddrInfo->ip6Addr, pFsipv6AddrAddress->pu1_OctetList,
                  sizeof (tIp6Addr)) == 0)
                && (i4Fsipv6AddrPrefixLen == pAddrInfo->u1PrefLen))

            {
                if (pNext)
                {
                    pAddrInfo = IP6_ADDR_PTR_FROM_SLL (pNext);

                    *pi4NextFsipv6AddrIndex = i4Fsipv6AddrIndex;

                    Ip6AddrCopy
                        ((tIp6Addr *) (VOID *) pNextFsipv6AddrAddress->
                         pu1_OctetList, &pAddrInfo->ip6Addr);
                    pNextFsipv6AddrAddress->i4_Length = sizeof (tIp6Addr);

                    *pi4NextFsipv6AddrPrefixLen = pAddrInfo->u1PrefLen;
                    return (SNMP_SUCCESS);
                }
                else
                {
                    if (Ip6AddrTblNextAddr
                        ((UINT4 *) &i4Fsipv6AddrIndex,
                         (tIp6Addr *) (VOID *) pFsipv6AddrAddress->
                         pu1_OctetList, &u1Prefix,
                         IP6_ADDR_NEXT_SRCH_LLIST) == IP6_FAILURE)
                    {
                        return SNMP_FAILURE;
                    }

                    i4Fsipv6AddrPrefixLen = u1Prefix;
                    *pi4NextFsipv6AddrIndex = i4Fsipv6AddrIndex;
                    MEMCPY (pNextFsipv6AddrAddress->pu1_OctetList,
                            pFsipv6AddrAddress->pu1_OctetList,
                            sizeof (tIp6Addr));

                    pNextFsipv6AddrAddress->i4_Length = sizeof (tIp6Addr);
                    *pi4NextFsipv6AddrPrefixLen = i4Fsipv6AddrPrefixLen;

                    return SNMP_SUCCESS;
                }
            }
            else
            {
                /* If the given addr is less than current addr in list
                 * return the current addr */

                i4AddrCmp =
                    MEMCMP (&pAddrInfo->ip6Addr,
                            pFsipv6AddrAddress->pu1_OctetList,
                            sizeof (tIp6Addr));
                if ((i4AddrCmp > 0)
                    || ((i4AddrCmp == 0)
                        && (i4Fsipv6AddrPrefixLen < pAddrInfo->u1PrefLen)))
                {
                    pAddrInfo = IP6_ADDR_PTR_FROM_SLL (pCur);

                    *pi4NextFsipv6AddrIndex = i4Fsipv6AddrIndex;

                    Ip6AddrCopy
                        ((tIp6Addr *) (VOID *) pNextFsipv6AddrAddress->
                         pu1_OctetList, &pAddrInfo->ip6Addr);
                    pNextFsipv6AddrAddress->i4_Length = sizeof (tIp6Addr);

                    *pi4NextFsipv6AddrPrefixLen = pAddrInfo->u1PrefLen;
                    return (SNMP_SUCCESS);
                }
            }
        }
    }

    pNext = pCur = TMO_SLL_First (&pIf6->lla6Ilist);

    while ((pCur = pNext) != NULL)
    {
        pNext = TMO_SLL_Next (&pIf6->lla6Ilist, pCur);

        pLlocalInfo = IP6_LLADDR_PTR_FROM_SLL (pCur);

        /* If the given addr is equal to the addr in list
         * return the next addr in the list */
        if (MEMCMP
            (&pLlocalInfo->ip6Addr, pFsipv6AddrAddress->pu1_OctetList,
             sizeof (tIp6Addr)) == 0)
        {
            if (pNext)
            {
                pLlocalInfo = IP6_LLADDR_PTR_FROM_SLL (pNext);

                *pi4NextFsipv6AddrIndex = i4Fsipv6AddrIndex;

                Ip6AddrCopy ((tIp6Addr *) (VOID *)
                             pNextFsipv6AddrAddress->pu1_OctetList,
                             &pLlocalInfo->ip6Addr);

                pNextFsipv6AddrAddress->i4_Length = sizeof (tIp6Addr);

                *pi4NextFsipv6AddrPrefixLen = IP6_ADDR_SIZE_IN_BITS;

                return (SNMP_SUCCESS);
            }
            else
            {

                i4Fsipv6AddrIndex++;
                if (Ip6AddrTblNextAddr ((UINT4 *) &i4Fsipv6AddrIndex,
                                        (tIp6Addr *) (VOID *)
                                        pFsipv6AddrAddress->pu1_OctetList,
                                        &u1Prefix,
                                        IP6_ADDR_NEXT_SRCH_ALIST) ==
                    IP6_FAILURE)
                {
                    return SNMP_FAILURE;
                }

                *pi4NextFsipv6AddrIndex = i4Fsipv6AddrIndex;
                i4Fsipv6AddrPrefixLen = u1Prefix;
                MEMCPY (pNextFsipv6AddrAddress->pu1_OctetList,
                        pFsipv6AddrAddress->pu1_OctetList, sizeof (tIp6Addr));
                pNextFsipv6AddrAddress->i4_Length = sizeof (tIp6Addr);
                *pi4NextFsipv6AddrPrefixLen = i4Fsipv6AddrPrefixLen;

                return SNMP_SUCCESS;
            }
        }
        else
        {
            /* If the given addr is less than the current addr in list
             * return the current addr */
            if (MEMCMP
                (&pLlocalInfo->ip6Addr, pFsipv6AddrAddress->pu1_OctetList,
                 sizeof (tIp6Addr)) > 0)
            {
                pLlocalInfo = IP6_LLADDR_PTR_FROM_SLL (pCur);

                *pi4NextFsipv6AddrIndex = i4Fsipv6AddrIndex;

                Ip6AddrCopy ((tIp6Addr *) (VOID *)
                             pNextFsipv6AddrAddress->pu1_OctetList,
                             &pLlocalInfo->ip6Addr);

                pNextFsipv6AddrAddress->i4_Length = sizeof (tIp6Addr);

                *pi4NextFsipv6AddrPrefixLen = IP6_ADDR_SIZE_IN_BITS;

                return (SNMP_SUCCESS);
            }
        }
    }

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsipv6AddrAdminStatus
 Input       :  The Indices
                Fsipv6AddrIndex
                Fsipv6AddrAddress
                Fsipv6AddrPrefixLen

                The Object 
                retValFsipv6AddrAdminStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6AddrAdminStatus (INT4 i4Fsipv6AddrIndex,
                             tSNMP_OCTET_STRING_TYPE * pFsipv6AddrAddress,
                             INT4 i4Fsipv6AddrPrefixLen,
                             INT4 *pi4RetValFsipv6AddrAdminStatus)
{
    tIp6AddrInfo       *pAddrInfo = NULL;
    tIp6LlocalInfo     *pLlocalInfo = NULL;
    tIp6Addr            Fsipv6Address;

    Ip6AddrCopy (&Fsipv6Address,
                 (tIp6Addr *) (VOID *) pFsipv6AddrAddress->pu1_OctetList);

    if (Ip6AddrType (&Fsipv6Address) == ADDR6_LLOCAL)
    {
        pLlocalInfo =
            Ip6AddrTblGetLlocalEntry ((UINT4) i4Fsipv6AddrIndex,
                                      (tIp6Addr *) & Fsipv6Address);
        if (pLlocalInfo == NULL)
        {
            /* Required Link Local Address is not present. */
            return SNMP_FAILURE;
        }

        *pi4RetValFsipv6AddrAdminStatus = pLlocalInfo->u1AdminStatus;
        return SNMP_SUCCESS;
    }

    pAddrInfo =
        Ip6AddrTblGetEntry ((UINT4) i4Fsipv6AddrIndex,
                            (tIp6Addr *) & Fsipv6Address,
                            (UINT1) i4Fsipv6AddrPrefixLen);

    if (pAddrInfo == NULL)
    {
        /* Required Address is not present. */
        return SNMP_FAILURE;
    }

    *pi4RetValFsipv6AddrAdminStatus = (UINT4) pAddrInfo->u1AdminStatus;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6AddrType
 Input       :  The Indices
                Fsipv6AddrIndex
                Fsipv6AddrAddress
                Fsipv6AddrPrefixLen

                The Object 
                retValFsipv6AddrType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6AddrType (INT4 i4Fsipv6AddrIndex,
                      tSNMP_OCTET_STRING_TYPE * pFsipv6AddrAddress,
                      INT4 i4Fsipv6AddrPrefixLen, INT4 *pi4RetValFsipv6AddrType)
{
    tIp6AddrInfo       *pAddrInfo = NULL;
    tIp6LlocalInfo     *pLlAddrInfo = NULL;
    tIp6Addr            Fsipv6Address;

    Ip6AddrCopy (&Fsipv6Address,
                 (tIp6Addr *) (VOID *) pFsipv6AddrAddress->pu1_OctetList);
    if (Ip6AddrType (&Fsipv6Address) == ADDR6_LLOCAL)
    {
        pLlAddrInfo = Ip6AddrTblGetLlocalEntry ((UINT4) i4Fsipv6AddrIndex,
                                                &Fsipv6Address);
        if (pLlAddrInfo == NULL)
        {
            return SNMP_FAILURE;
        }
        *pi4RetValFsipv6AddrType = ADDR6_LLOCAL;
        return SNMP_SUCCESS;
    }

    pAddrInfo =
        Ip6AddrTblGetEntry ((UINT2) i4Fsipv6AddrIndex,
                            (tIp6Addr *) & Fsipv6Address,
                            (UINT1) i4Fsipv6AddrPrefixLen);
    if (pAddrInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pAddrInfo->u1AddrType == ADDR6_ANYCAST)
    {
        *pi4RetValFsipv6AddrType = ADDR6_ANYCAST;
        return SNMP_SUCCESS;
    }

    *pi4RetValFsipv6AddrType = ADDR6_UNICAST;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6AddrProfIndex
 Input       :  The Indices
                Fsipv6AddrIndex
                Fsipv6AddrAddress
                Fsipv6AddrPrefixLen

                The Object 
                retValFsipv6AddrProfIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6AddrProfIndex (INT4 i4Fsipv6AddrIndex,
                           tSNMP_OCTET_STRING_TYPE * pFsipv6AddrAddress,
                           INT4 i4Fsipv6AddrPrefixLen,
                           INT4 *pi4RetValFsipv6AddrProfIndex)
{
    tIp6AddrInfo       *pAddrInfo = NULL;
    tIp6Addr            Fsipv6Address;

    Ip6AddrCopy (&Fsipv6Address,
                 (tIp6Addr *) (VOID *) pFsipv6AddrAddress->pu1_OctetList);
    if (Ip6AddrType (&Fsipv6Address) == ADDR6_LLOCAL)
    {
        *pi4RetValFsipv6AddrProfIndex = 1;
        return SNMP_SUCCESS;
    }
    pAddrInfo =
        Ip6AddrTblGetEntry ((UINT2) i4Fsipv6AddrIndex,
                            (tIp6Addr *) & Fsipv6Address,
                            (UINT1) i4Fsipv6AddrPrefixLen);
    if (pAddrInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsipv6AddrProfIndex = (INT4) pAddrInfo->u2Addr6Profile;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6AddrOperStatus
 Input       :  The Indices
                Fsipv6AddrIndex
                Fsipv6AddrAddress
                Fsipv6AddrPrefixLen

                The Object 
                retValFsipv6AddrOperStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6AddrOperStatus (INT4 i4Fsipv6AddrIndex,
                            tSNMP_OCTET_STRING_TYPE * pFsipv6AddrAddress,
                            INT4 i4Fsipv6AddrPrefixLen,
                            INT4 *pi4RetValFsipv6AddrOperStatus)
{
    if (Ip6GetFsipv6AddrOperStatus (i4Fsipv6AddrIndex, pFsipv6AddrAddress,
                                    i4Fsipv6AddrPrefixLen,
                                    pi4RetValFsipv6AddrOperStatus) ==
        SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhGetFsipv6AddrScope
 Input       :  The Indices
                Fsipv6AddrIndex
                Fsipv6AddrAddress
                Fsipv6AddrPrefixLen

                The Object 
                retValFsipv6AddrScope
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6AddrScope (INT4 i4Fsipv6AddrIndex,
                       tSNMP_OCTET_STRING_TYPE * pFsipv6AddrAddress,
                       INT4 i4Fsipv6AddrPrefixLen,
                       INT4 *pi4RetValFsipv6AddrScope)
{
    tIp6AddrInfo       *pAddrInfo = NULL;
    tIp6LlocalInfo     *pLLAddrInfo = NULL;
    tIp6Addr            Fsipv6Address;

    Ip6AddrCopy (&Fsipv6Address,
                 (tIp6Addr *) (VOID *) pFsipv6AddrAddress->pu1_OctetList);

    if (Ip6AddrType (&Fsipv6Address) == ADDR6_LLOCAL)
    {
        pLLAddrInfo =
            Ip6AddrTblGetLlocalEntry ((UINT4) i4Fsipv6AddrIndex,
                                      (tIp6Addr *) & Fsipv6Address);
        if (pLLAddrInfo == NULL)
        {
            return SNMP_FAILURE;
        }
        *pi4RetValFsipv6AddrScope = ADDR6_SCOPE_LLOCAL;
        return SNMP_SUCCESS;
    }
    pAddrInfo = Ip6AddrTblGetEntry ((UINT2) i4Fsipv6AddrIndex,
                                    (tIp6Addr *) & Fsipv6Address,
                                    (UINT1) i4Fsipv6AddrPrefixLen);
    if (pAddrInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsipv6AddrScope = (INT4) pAddrInfo->u1AddrScope;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6AddrSENDCgaStatus
 Input       :  The Indices
                Fsipv6AddrIndex
                Fsipv6AddrAddress
                Fsipv6AddrPrefixLen

                The Object 
                retValFsipv6AddrSENDCgaStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6AddrSENDCgaStatus (INT4 i4Fsipv6AddrIndex,
                               tSNMP_OCTET_STRING_TYPE * pFsipv6AddrAddress,
                               INT4 i4Fsipv6AddrPrefixLen,
                               INT4 *pi4RetValFsipv6AddrSENDCgaStatus)
{
    tIp6AddrInfo       *pAddrInfo = NULL;
    tIp6LlocalInfo     *pLlocalInfo = NULL;
    tIp6Addr            Fsipv6Address;

    Ip6AddrCopy (&Fsipv6Address,
                 (tIp6Addr *) (VOID *) pFsipv6AddrAddress->pu1_OctetList);

    if (Ip6AddrType (&Fsipv6Address) == ADDR6_LLOCAL)
    {
        pLlocalInfo =
            Ip6AddrTblGetLlocalEntry ((UINT4) i4Fsipv6AddrIndex,
                                      (tIp6Addr *) & Fsipv6Address);
        if (pLlocalInfo == NULL)
        {
            /* Required Link Local Address is not present. */
            return SNMP_FAILURE;
        }
        *pi4RetValFsipv6AddrSENDCgaStatus =
            (UINT1) pLlocalInfo->b1SeNDCgaStatus;
        return SNMP_SUCCESS;

    }
    pAddrInfo =
        Ip6AddrTblGetEntry ((UINT4) i4Fsipv6AddrIndex,
                            (tIp6Addr *) & Fsipv6Address,
                            (UINT1) i4Fsipv6AddrPrefixLen);

    if (pAddrInfo == NULL)
    {
        /* Required Address is not present. */
        return SNMP_FAILURE;
    }

    *pi4RetValFsipv6AddrSENDCgaStatus = (UINT1) pAddrInfo->b1SeNDCgaStatus;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6AddrSENDCgaModifier
 Input       :  The Indices
                Fsipv6AddrIndex
                Fsipv6AddrAddress
                Fsipv6AddrPrefixLen

                The Object
                retValFsipv6AddrSENDCgaModifier
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6AddrSENDCgaModifier (INT4 i4Fsipv6AddrIndex,
                                 tSNMP_OCTET_STRING_TYPE * pFsipv6AddrAddress,
                                 INT4 i4Fsipv6AddrPrefixLen,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pRetValFsipv6AddrSENDCgaModifier)
{
    tIp6AddrInfo       *pAddrInfo = NULL;
    tIp6LlocalInfo     *pLlocalInfo = NULL;
    tIp6Addr            Fsipv6Address;

    Ip6AddrCopy (&Fsipv6Address,
                 (tIp6Addr *) (VOID *) pFsipv6AddrAddress->pu1_OctetList);

    if (Ip6AddrType (&Fsipv6Address) == ADDR6_LLOCAL)
    {
        pLlocalInfo =
            Ip6AddrTblGetLlocalEntry ((UINT4) i4Fsipv6AddrIndex,
                                      (tIp6Addr *) & Fsipv6Address);
        if (pLlocalInfo == NULL)
        {
            /* Required Link Local Address is not present. */
            return SNMP_FAILURE;
        }
        pRetValFsipv6AddrSENDCgaModifier->i4_Length = 16;
        MEMCPY (pRetValFsipv6AddrSENDCgaModifier->pu1_OctetList,
                pLlocalInfo->cgaParams.au1Modifier, 16);
        return SNMP_SUCCESS;

    }
    pAddrInfo =
        Ip6AddrTblGetEntry ((UINT4) i4Fsipv6AddrIndex,
                            (tIp6Addr *) & Fsipv6Address,
                            (UINT1) i4Fsipv6AddrPrefixLen);

    if (pAddrInfo == NULL)
    {
        /* Required Address is not present. */
        return SNMP_FAILURE;
    }

    pRetValFsipv6AddrSENDCgaModifier->i4_Length = 16;
    MEMCPY (pRetValFsipv6AddrSENDCgaModifier->pu1_OctetList,
            pAddrInfo->cgaParams.au1Modifier, 16);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6AddrSENDCollisionCount
 Input       :  The Indices
                Fsipv6AddrIndex
                Fsipv6AddrAddress
                Fsipv6AddrPrefixLen

                The Object
                retValFsipv6AddrSENDCollisionCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6AddrSENDCollisionCount (INT4 i4Fsipv6AddrIndex,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsipv6AddrAddress,
                                    INT4 i4Fsipv6AddrPrefixLen,
                                    INT4 *pi4RetValFsipv6AddrSENDCollisionCount)
{
    tIp6AddrInfo       *pAddrInfo = NULL;
    tIp6LlocalInfo     *pLlocalInfo = NULL;
    tIp6Addr            Fsipv6Address;

    Ip6AddrCopy (&Fsipv6Address,
                 (tIp6Addr *) (VOID *) pFsipv6AddrAddress->pu1_OctetList);

    if (Ip6AddrType (&Fsipv6Address) == ADDR6_LLOCAL)
    {
        pLlocalInfo =
            Ip6AddrTblGetLlocalEntry ((UINT4) i4Fsipv6AddrIndex,
                                      (tIp6Addr *) & Fsipv6Address);
        if (pLlocalInfo == NULL)
        {
            /* Required Link Local Address is not present. */
            return SNMP_FAILURE;
        }
        *pi4RetValFsipv6AddrSENDCollisionCount =
            (UINT1) pLlocalInfo->cgaParams.u1Collisions;
        return SNMP_SUCCESS;

    }
    pAddrInfo =
        Ip6AddrTblGetEntry ((UINT4) i4Fsipv6AddrIndex,
                            (tIp6Addr *) & Fsipv6Address,
                            (UINT1) i4Fsipv6AddrPrefixLen);
    if (pAddrInfo == NULL)
    {
        /* Required Address is not present. */
        return SNMP_FAILURE;
    }

    *pi4RetValFsipv6AddrSENDCollisionCount =
        (UINT1) pAddrInfo->cgaParams.u1Collisions;
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsipv6AddrAdminStatus
 Input       :  The Indices
                Fsipv6AddrIndex
                Fsipv6AddrAddress
                Fsipv6AddrPrefixLen

                The Object 
                setValFsipv6AddrAdminStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6AddrAdminStatus (INT4 i4Fsipv6AddrIndex,
                             tSNMP_OCTET_STRING_TYPE * pFsipv6AddrAddress,
                             INT4 i4Fsipv6AddrPrefixLen,
                             INT4 i4SetValFsipv6AddrAdminStatus)
{
    tIp6LlocalInfo     *pLLAddrInfo = NULL;
    tIp6AddrInfo       *pAddrInfo = NULL;
    tIp6Addr            Fsipv6Address;
    INT4                i4RetValFsipv6IfSENDSecStatus = 0;
    Ip6AddrCopy (&Fsipv6Address,
                 (tIp6Addr *) (VOID *) pFsipv6AddrAddress->pu1_OctetList);

    if (Ip6ifEntryExists ((UINT4) i4Fsipv6AddrIndex) != IP6_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    switch (i4SetValFsipv6AddrAdminStatus)
    {
        case IP6FWD_CREATE_AND_GO:
            /* Create the new UNICAST Address and make the operation Status
             * Active */

            if (Ip6AddrType (&Fsipv6Address) == ADDR6_LLOCAL)
            {
                pLLAddrInfo =
                    Ip6AddrTblGetLlocalEntry ((UINT4) i4Fsipv6AddrIndex,
                                              (tIp6Addr *) & Fsipv6Address);
                if (pLLAddrInfo == NULL)
                {
                    if ((pLLAddrInfo = Ip6LlAddrCreate
                         ((UINT4) i4Fsipv6AddrIndex,
                          &Fsipv6Address, ADMIN_DOWN, IP6_ADDR_STATIC)) == NULL)
                    {
                        return SNMP_FAILURE;
                    }

                    /* Set it as ACTIVE. */
                    if (Ip6AddrUp
                        ((UINT4) i4Fsipv6AddrIndex,
                         (tIp6Addr *) & Fsipv6Address,
                         (UINT1) i4Fsipv6AddrPrefixLen, NULL) == IP6_FAILURE)
                    {
                        Ip6LlAddrDelete ((UINT4) i4Fsipv6AddrIndex,
                                         &Fsipv6Address);
                        KW_FALSEPOSITIVE_FIX (pLLAddrInfo);
                        return SNMP_FAILURE;
                    }

                    pLLAddrInfo->u1AdminStatus = IP6FWD_ACTIVE;
                }
                else
                {
                    /* Statically configured Link-Local Address matches
                     * the Link-Local Address created using Stateless
                     * Autoconfiguration. Just change the configuration
                     * method for this entry. No need for DAD or any other
                     * operation since all these will be performed while
                     * the address is created first. */
                    pLLAddrInfo->u1ConfigMethod = IP6_ADDR_STATIC;
                    pLLAddrInfo->u1AdminStatus = IP6FWD_ACTIVE;
                }
            }
            else
            {
                if ((pAddrInfo = Ip6AddrCreate ((UINT4) i4Fsipv6AddrIndex,
                                                &Fsipv6Address,
                                                (UINT1) i4Fsipv6AddrPrefixLen,
                                                ADMIN_DOWN,
                                                IP6_ADDR_TYPE_UNICAST, 0,
                                                IP6_ADDR_STATIC)) == NULL)
                {
                    return SNMP_FAILURE;
                }

                /* Set it as ACTIVE. */
                if (Ip6AddrUp
                    ((UINT4) i4Fsipv6AddrIndex, (tIp6Addr *) & Fsipv6Address,
                     (UINT1) i4Fsipv6AddrPrefixLen, pAddrInfo) == IP6_FAILURE)
                {
                    return SNMP_FAILURE;
                }

                pAddrInfo->u1AdminStatus = IP6FWD_ACTIVE;
                pAddrInfo->u1PrefixRowStatus = IP6FWD_ACTIVE;
                pAddrInfo->u1AddrRowStatus = IP6FWD_ACTIVE;
                pAddrInfo->u1IpStorageType = IPVX_STORAGE_TYPE_VOLATILE;
                pAddrInfo->u1IpStatus = IPVX_ADDR_STATUS_PREFERRED;
                pAddrInfo->u1Origin = IPVX_ADDR_PREFIX_ORIGIN_MANUAL;
                pAddrInfo->u4IpCreated = OsixGetSysUpTime ();

                /* It will be set  IP6FWD_ACTIVE in If Up Event */
                if (gIp6GblInfo.apIp6If[i4Fsipv6AddrIndex]->u1AdminStatus !=
                    ADMIN_UP)
                {
                    pAddrInfo->u1AdminStatus = IP6FWD_NOT_READY;
                }
            }
            break;

        case IP6FWD_CREATE_AND_WAIT:
            /* Create the new UNICAST Address and make the operation Status
             * Down */

            if (Ip6AddrType (&Fsipv6Address) == ADDR6_LLOCAL)
            {
                pLLAddrInfo =
                    Ip6AddrTblGetLlocalEntry ((UINT4) i4Fsipv6AddrIndex,
                                              (tIp6Addr *) & Fsipv6Address);
                if (pLLAddrInfo == NULL)
                {
                    if ((pLLAddrInfo = Ip6LlAddrCreate
                         ((UINT4) i4Fsipv6AddrIndex,
                          &Fsipv6Address, ADMIN_DOWN, IP6_ADDR_STATIC)) == NULL)
                    {
                        return SNMP_FAILURE;
                    }
                    pLLAddrInfo->u1AdminStatus = IP6FWD_NOT_READY;
                }
                else
                {
                    /* Statically configured Link-Local Address matches
                     * the Link-Local Address created using Stateless
                     * Autoconfiguration. Just change the configuration
                     * method for this entry. No need for DAD or any other
                     * operation since all these will be performed while
                     * the address is created first. */
                    pLLAddrInfo->u1ConfigMethod = IP6_ADDR_STATIC;
                }
            }
            else
            {
                if ((pAddrInfo = Ip6AddrCreate ((UINT4) i4Fsipv6AddrIndex,
                                                &Fsipv6Address,
                                                (UINT1) i4Fsipv6AddrPrefixLen,
                                                ADMIN_DOWN,
                                                IP6_ADDR_TYPE_UNICAST, 0,
                                                IP6_ADDR_STATIC)) == NULL)
                {
                    return SNMP_FAILURE;
                }
                pAddrInfo->u1AdminStatus = IP6FWD_NOT_READY;
                pAddrInfo->u1AddrRowStatus = IP6FWD_NOT_READY;
                pAddrInfo->u1PrefixRowStatus = IP6FWD_NOT_READY;
                pAddrInfo->u1IpStorageType = IPVX_STORAGE_TYPE_VOLATILE;
                pAddrInfo->u1IpStatus = IPVX_ADDR_STATUS_PREFERRED;
                pAddrInfo->u1Origin = IPVX_ADDR_PREFIX_ORIGIN_MANUAL;
                pAddrInfo->u4IpCreated = OsixGetSysUpTime ();
            }
            break;

        case IP6FWD_ACTIVE:
            if (Ip6AddrType (&Fsipv6Address) == ADDR6_LLOCAL)
            {
                pLLAddrInfo =
                    Ip6AddrTblGetLlocalEntry ((UINT4) i4Fsipv6AddrIndex,
                                              (tIp6Addr *) & Fsipv6Address);

                if (pLLAddrInfo == NULL)
                {
                    return SNMP_FAILURE;
                }
                if (pLLAddrInfo->u1AdminStatus == IP6FWD_ACTIVE)
                {
                    /* Address Already Enabled. */
                    return SNMP_SUCCESS;
                }

                /* Set this Address Status as operationally UP. */
                if (Ip6AddrUp
                    ((UINT4) i4Fsipv6AddrIndex, (tIp6Addr *) & Fsipv6Address,
                     (UINT1) i4Fsipv6AddrPrefixLen, NULL) == IP6_FAILURE)
                {
                    return SNMP_FAILURE;
                }

                pLLAddrInfo->u1AdminStatus = IP6FWD_ACTIVE;
            }
            else
            {
                pAddrInfo = Ip6AddrTblGetEntry ((UINT4) i4Fsipv6AddrIndex,
                                                (tIp6Addr *) & Fsipv6Address,
                                                (UINT1) i4Fsipv6AddrPrefixLen);

                if (pAddrInfo == NULL)
                {
                    return SNMP_FAILURE;
                }
                if (pAddrInfo->u1AdminStatus == IP6FWD_ACTIVE)
                {
                    /* Address Already Enabled. */
                    return SNMP_SUCCESS;
                }

                /* Set this Address Status as operationally UP. */
                if (Ip6AddrUp
                    ((UINT4) i4Fsipv6AddrIndex, (tIp6Addr *) & Fsipv6Address,
                     (UINT1) i4Fsipv6AddrPrefixLen, pAddrInfo) == IP6_FAILURE)
                {
                    return SNMP_FAILURE;
                }

                pAddrInfo->u1AdminStatus = IP6FWD_ACTIVE;
                pAddrInfo->u1AddrRowStatus = IP6FWD_ACTIVE;
                pAddrInfo->u1PrefixRowStatus = IP6FWD_ACTIVE;
            }
            break;

        case IP6FWD_DESTROY:
            if (Ip6AddrType (&Fsipv6Address) == ADDR6_LLOCAL)
            {
                nmhGetFsipv6IfSENDSecStatus (i4Fsipv6AddrIndex,
                                             &i4RetValFsipv6IfSENDSecStatus);
                if (i4RetValFsipv6IfSENDSecStatus == 1
                    || i4RetValFsipv6IfSENDSecStatus == 3)
                {
                    CLI_SET_ERR (CLI_IP6_SEND_LL_ADDR_DEL_NOT_ALLOWED);
                    return SNMP_FAILURE;
                }
                else
                {
                    if (Ip6LlAddrDelete
                        ((UINT4) i4Fsipv6AddrIndex,
                         &Fsipv6Address) == IP6_FAILURE)
                    {
                        return SNMP_FAILURE;
                    }
                }
            }
            else
            {
                pAddrInfo = Ip6AddrTblGetEntry ((UINT4) i4Fsipv6AddrIndex,
                                                (tIp6Addr *) & Fsipv6Address,
                                                (UINT1) i4Fsipv6AddrPrefixLen);
                /* Delete this Address */
#ifdef HA_WANTED
                HAPrcsAddrUpdate (pAddrInfo->pIf6, (UINT1 *) pAddrInfo,
                                  ADMIN_INVALID, 0);
#endif
                UNUSED_PARAM (pAddrInfo);
                if (Ip6AddrDelete ((UINT4) i4Fsipv6AddrIndex, &Fsipv6Address,
                                   (UINT1) i4Fsipv6AddrPrefixLen, 1) ==
                    IP6_FAILURE)
                {
                    return SNMP_FAILURE;
                }
            }
            break;

        case IP6FWD_NOT_IN_SERVICE:
        default:
            return SNMP_FAILURE;
    }
    IncMsrForIpv6AddrTable (i4Fsipv6AddrIndex, pFsipv6AddrAddress,
                            i4Fsipv6AddrPrefixLen, 'i',
                            &i4SetValFsipv6AddrAdminStatus,
                            FsMIIpv6AddrAdminStatus,
                            sizeof (FsMIIpv6AddrAdminStatus) / sizeof (UINT4),
                            TRUE);
    KW_FALSEPOSITIVE_FIX (pLLAddrInfo);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6AddrType
 Input       :  The Indices
                Fsipv6AddrIndex
                Fsipv6AddrAddress
                Fsipv6AddrPrefixLen

                The Object 
                setValFsipv6AddrType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6AddrType (INT4 i4Fsipv6AddrIndex,
                      tSNMP_OCTET_STRING_TYPE * pFsipv6AddrAddress,
                      INT4 i4Fsipv6AddrPrefixLen, INT4 i4SetValFsipv6AddrType)
{
    tIp6LlocalInfo     *pLLAddrInfo = NULL;
    tIp6AddrInfo       *pAddrInfo = NULL;
    tIp6Addr            Fsipv6Address;
    tIp6If             *pIf6 = NULL;

    pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6AddrIndex);

    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    Ip6AddrCopy (&Fsipv6Address,
                 (tIp6Addr *) (VOID *) pFsipv6AddrAddress->pu1_OctetList);

    if (i4SetValFsipv6AddrType == IP6_ADDR_TYPE_LINK_LOCAL)
    {
        pLLAddrInfo =
            Ip6AddrTblGetLlocalEntry ((UINT4) i4Fsipv6AddrIndex,
                                      &Fsipv6Address);

        if (pLLAddrInfo == NULL)
        {
            return SNMP_FAILURE;
        }

        IncMsrForIpv6AddrTable (i4Fsipv6AddrIndex, pFsipv6AddrAddress,
                                i4Fsipv6AddrPrefixLen, 'i',
                                &i4SetValFsipv6AddrType, FsMIIpv6AddrType,
                                sizeof (FsMIIpv6AddrType) / sizeof (UINT4),
                                FALSE);

        /* Link-local Address is present. */
        return SNMP_SUCCESS;
    }

    pAddrInfo =
        Ip6AddrTblGetEntry ((UINT4) i4Fsipv6AddrIndex, &Fsipv6Address,
                            (UINT1) i4Fsipv6AddrPrefixLen);

    if (pAddrInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (i4SetValFsipv6AddrType == IP6_ADDR_TYPE_UNICAST)
    {
        pAddrInfo->u1AddrType = ADDR6_UNICAST;
    }
    else if (i4SetValFsipv6AddrType == IP6_ADDR_TYPE_ANYCAST)
    {
        pAddrInfo->u1AddrType = ADDR6_ANYCAST;
    }

    IncMsrForIpv6AddrTable (i4Fsipv6AddrIndex, pFsipv6AddrAddress,
                            i4Fsipv6AddrPrefixLen, 'i', &i4SetValFsipv6AddrType,
                            FsMIIpv6AddrType,
                            sizeof (FsMIIpv6AddrType) / sizeof (UINT4), FALSE);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6AddrProfIndex
 Input       :  The Indices
                Fsipv6AddrIndex
                Fsipv6AddrAddress
                Fsipv6AddrPrefixLen

                The Object 
                setValFsipv6AddrProfIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6AddrProfIndex (INT4 i4Fsipv6AddrIndex,
                           tSNMP_OCTET_STRING_TYPE * pFsipv6AddrAddress,
                           INT4 i4Fsipv6AddrPrefixLen,
                           INT4 i4SetValFsipv6AddrProfIndex)
{
    tIp6AddrInfo       *pAddrInfo = NULL;
    tIp6Addr            Fsipv6Address;
    tIp6If             *pIf6 = NULL;

    pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6AddrIndex);

    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    Ip6AddrCopy (&Fsipv6Address,
                 (tIp6Addr *) (VOID *) pFsipv6AddrAddress->pu1_OctetList);

    pAddrInfo =
        Ip6AddrTblGetEntry ((UINT4) i4Fsipv6AddrIndex,
                            (tIp6Addr *) & Fsipv6Address,
                            (UINT1) i4Fsipv6AddrPrefixLen);
    if (pAddrInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pAddrInfo->u2Addr6Profile != (UINT2) i4SetValFsipv6AddrProfIndex)
    {
        /* Decrease the Ref Count for Current Associated Profile Entry */
        if (pAddrInfo->u2Addr6Profile < gIp6GblInfo.u4MaxAddrProfileLimit)
        {
            IP6_ADDR_PROF_REF_COUNT (pAddrInfo->u2Addr6Profile)--;
        }

        /* Assign the New Profile Index */
        pAddrInfo->u2Addr6Profile = (UINT2) i4SetValFsipv6AddrProfIndex;

        /* Increase the Ref Count for Current Associated Profile Entry */
        IP6_ADDR_PROF_REF_COUNT (i4SetValFsipv6AddrProfIndex)++;
    }
    IncMsrForIpv6AddrTable (i4Fsipv6AddrIndex, pFsipv6AddrAddress,
                            i4Fsipv6AddrPrefixLen, 'i',
                            &i4SetValFsipv6AddrProfIndex, FsMIIpv6AddrProfIndex,
                            sizeof (FsMIIpv6AddrProfIndex) / sizeof (UINT4),
                            FALSE);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6AddrSENDCgaModifier
 Input       :  The Indices
                Fsipv6AddrIndex
                Fsipv6AddrAddress
                Fsipv6AddrPrefixLen

                The Object
                setValFsipv6AddrSENDCgaModifier
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6AddrSENDCgaModifier (INT4 i4Fsipv6AddrIndex,
                                 tSNMP_OCTET_STRING_TYPE * pFsipv6AddrAddress,
                                 INT4 i4Fsipv6AddrPrefixLen,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pSetValFsipv6AddrSENDCgaModifier)
{
    tIp6AddrInfo       *pAddrInfo = NULL;
    tIp6LlocalInfo     *pLlocalInfo = NULL;
    tIp6Addr            Fsipv6Address;
    tIp6If             *pIf6 = NULL;
    UINT1               u1Type = IP6_ZERO;

    pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6AddrIndex);
    if (pIf6 == NULL)
    {
        CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
        return SNMP_FAILURE;
    }

    Ip6AddrCopy (&Fsipv6Address,
                 (tIp6Addr *) (VOID *) pFsipv6AddrAddress->pu1_OctetList);

    if (Ip6AddrType (&Fsipv6Address) == ADDR6_LLOCAL)
    {
        u1Type = ADDR6_LLOCAL;
        pLlocalInfo =
            Ip6AddrTblGetLlocalEntry ((UINT4) i4Fsipv6AddrIndex,
                                      &Fsipv6Address);
        if (pLlocalInfo == NULL)
        {
            if ((ND6_GET_NODE_STATUS () == RM_ACTIVE))
            {
                return SNMP_FAILURE;
            }

            /*
             * this might be the case where the cga address is regenerated
             * due to collision in DAD, so we need to update to new address
             * by fetching the entry based on modifier
             */
            pLlocalInfo = (tIp6LlocalInfo *) (VOID *)
                Ip6UtlCgaAddrInfoGet ((UINT4) i4Fsipv6AddrIndex, u1Type,
                                      pSetValFsipv6AddrSENDCgaModifier->
                                      pu1_OctetList);

            if (pLlocalInfo == NULL)
            {
                return SNMP_FAILURE;
            }
            Ip6AddrCopy (&pLlocalInfo->ip6Addr, &Fsipv6Address);
            return SNMP_SUCCESS;
        }
        MEMCPY (&(pLlocalInfo->cgaParams.au1Modifier),
                (UINT1 *) (VOID *) pSetValFsipv6AddrSENDCgaModifier->
                pu1_OctetList, ND6_SEND_CGA_MOD_LEN);

        return SNMP_SUCCESS;
    }
    pAddrInfo =
        Ip6AddrTblGetEntry ((UINT4) i4Fsipv6AddrIndex,
                            (tIp6Addr *) & Fsipv6Address,
                            (UINT1) i4Fsipv6AddrPrefixLen);

    if (pAddrInfo == NULL)
    {
        if ((ND6_GET_NODE_STATUS () == RM_ACTIVE))
        {
            return SNMP_FAILURE;
        }

        /*
         * this might be the case where the cga address is regenerated
         * due to collision in DAD, so we need to update to new address
         * by fetching the entry based on modifier
         */
        pAddrInfo = (tIp6AddrInfo *) (VOID *)
            Ip6UtlCgaAddrInfoGet ((UINT4) i4Fsipv6AddrIndex, u1Type,
                                  pSetValFsipv6AddrSENDCgaModifier->
                                  pu1_OctetList);

        if (pAddrInfo == NULL)
        {
            return SNMP_FAILURE;
        }
        Ip6AddrCopy (&pAddrInfo->ip6Addr, &Fsipv6Address);
        return SNMP_SUCCESS;
    }
    MEMCPY (&(pAddrInfo->cgaParams.au1Modifier),
            (UINT1 *) (VOID *) pSetValFsipv6AddrSENDCgaModifier->pu1_OctetList,
            ND6_SEND_CGA_MOD_LEN);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6AddrSENDCgaStatus
 Input       :  The Indices
                Fsipv6AddrIndex
                Fsipv6AddrAddress
                Fsipv6AddrPrefixLen

                The Object 
                setValFsipv6AddrSENDCgaStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6AddrSENDCgaStatus (INT4 i4Fsipv6AddrIndex,
                               tSNMP_OCTET_STRING_TYPE * pFsipv6AddrAddress,
                               INT4 i4Fsipv6AddrPrefixLen,
                               INT4 i4SetValFsipv6AddrSENDCgaStatus)
{
#ifdef SSL_WANTED
    tSNMP_OCTET_STRING_TYPE CgaModifier;
    tTMO_SLL_NODE      *pAddrSll = NULL;
    tIp6AddrInfo       *pAddrInfo = NULL;
    tIp6LlocalInfo     *pLlocalInfo = NULL;
    tIp6Addr            Fsipv6Address;
    tIp6If             *pIf6 = NULL;
    UINT1              *pu1Buffer = NULL;
    UINT1              *pu1TempBuffer = NULL;
    UINT1               u1Hit = IP6_ZERO;

    MEMSET (&CgaModifier, IP6_ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));

    pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6AddrIndex);
    if (pIf6 == NULL)
    {
        /* Interface Does not Exist */
        CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
        return SNMP_FAILURE;
    }

    pu1Buffer =
        Ip6GetMem (pIf6->pIp6Cxt->u4ContextId,
                   gIp6GblInfo.i4Ip6Nd6SecurePoolId);
    if (NULL == pu1Buffer)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pu1Buffer, 0, ND6_SEND_MAX_BUF_SIZE);

    pu1TempBuffer = pu1Buffer;
    Ip6AddrCopy (&Fsipv6Address,
                 (tIp6Addr *) (VOID *) pFsipv6AddrAddress->pu1_OctetList);

    if (Ip6AddrType (&Fsipv6Address) == ADDR6_LLOCAL)
    {
        pLlocalInfo =
            Ip6AddrTblGetLlocalEntry ((UINT4) i4Fsipv6AddrIndex,
                                      (tIp6Addr *) & Fsipv6Address);
        if (pLlocalInfo == NULL)
        {
            if ((ND6_GET_NODE_STATUS () == RM_STANDBY) &&
                (pIf6->u1SeNDStatus != ND6_SECURE_DISABLE))
            {
                /* If the interface is secure, check for the last address */
                TMO_SLL_Scan (&gIp6GblInfo.apIp6If[i4Fsipv6AddrIndex]->
                              lla6Ilist, pAddrSll, tTMO_SLL_NODE *)
                {
                    pLlocalInfo = IP6_LLADDR_PTR_FROM_SLL (pAddrSll);

                    /*
                     * If the address in the info table is cga enabled
                     * but in "create and wait state" it is due to the 
                     * cga generation skip, replace the old address with
                     * cga address in order to facilitate the success of 
                     * next set routines
                     */
                    if ((pLlocalInfo->u1AdminStatus == IP6FWD_NOT_READY) &&
                        (i4SetValFsipv6AddrSENDCgaStatus == IP6_TRUE))
                    {
                        Ip6AddrCopy (&pLlocalInfo->ip6Addr, &Fsipv6Address);
                        u1Hit = IP6_ONE;
                        break;
                    }
                    else
                    {
                        continue;
                    }
                }

                if (u1Hit != IP6_ONE)
                {
                    Ip6RelMem (pIf6->pIp6Cxt->u4ContextId,
                               (UINT2) gIp6GblInfo.i4Ip6Nd6SecurePoolId,
                               pu1Buffer);
                    return SNMP_FAILURE;
                }
            }
            else
            {
                Ip6RelMem (pIf6->pIp6Cxt->u4ContextId,
                           (UINT2) gIp6GblInfo.i4Ip6Nd6SecurePoolId, pu1Buffer);
                /* Required Link Local Address is not present. */
                return SNMP_FAILURE;
            }
        }

        pLlocalInfo->b1SeNDCgaStatus = (BOOL1) i4SetValFsipv6AddrSENDCgaStatus;

        if (SSL_FAILURE == SslArGetDerRsaPubKey (&pu1Buffer,
                                                 &(pLlocalInfo->cgaParams.
                                                   u2PubKeyLen)))
        {
            Ip6RelMem (pIf6->pIp6Cxt->u4ContextId,
                       (UINT2) gIp6GblInfo.i4Ip6Nd6SecurePoolId, pu1Buffer);
            return SNMP_FAILURE;
        }

        /* 
         * pu1Buffer is modified to point to last in SslArGetDerRsaPubKey
         * so assign key from pu1TempBuffer
         */
        pLlocalInfo->cgaParams.pu1DerPubKey = pu1TempBuffer;
        MEMCPY (pLlocalInfo->cgaParams.au1Prefix,
                &Fsipv6Address, ND6_SEND_CGA_PREFIX_LEN);

        /*
         * CGA generation in standby results in different CGA address
         * due to different random number, so it will be synced dynamically
         * Similarly, CGA generation is not required in MSR
         */
        if ((ND6_GET_NODE_STATUS () == RM_ACTIVE) &&
            (MsrIsMibRestoreInProgress () != OSIX_SUCCESS))
        {
            if (IP6_FAILURE ==
                Nd6SeNDCgaGenerate (pIf6, &(pLlocalInfo->ip6Addr),
                                    &(pLlocalInfo->cgaParams)))
            {
                Ip6RelMem (pIf6->pIp6Cxt->u4ContextId,
                           (UINT2) gIp6GblInfo.i4Ip6Nd6SecurePoolId, pu1Buffer);
                return SNMP_FAILURE;
            }
            CgaModifier.pu1_OctetList = pLlocalInfo->cgaParams.au1Modifier;
            CgaModifier.i4_Length = ND6_SEND_CGA_MOD_LEN;
        }
        /*
         * Copy the generated address to original address to be 
         * used by other functions
         */
        Ip6AddrCopy ((tIp6Addr *) (VOID *) pFsipv6AddrAddress->pu1_OctetList,
                     &(pLlocalInfo->ip6Addr));

    }
    else
    {
        /* non-link local address */
        pAddrInfo = Ip6AddrTblGetEntry ((UINT4) i4Fsipv6AddrIndex,
                                        (tIp6Addr *) & Fsipv6Address,
                                        (UINT1) i4Fsipv6AddrPrefixLen);

        if (pAddrInfo == NULL)
        {
            if ((ND6_GET_NODE_STATUS () == RM_STANDBY) &&
                (pIf6->u1SeNDStatus != ND6_SECURE_DISABLE))
            {
                /* If the interface is secure, check for the last address */
                TMO_SLL_Scan (&gIp6GblInfo.apIp6If[i4Fsipv6AddrIndex]->
                              addr6Ilist, pAddrSll, tTMO_SLL_NODE *)
                {
                    pAddrInfo = IP6_ADDR_PTR_FROM_SLL (pAddrSll);
                    /*
                     * If the address in the info table is cga enabled
                     * but in "create and wait state" it is due to the 
                     * cga generation skip, replace the old address with
                     * cga address in order to facilitate the success of 
                     * next set routines
                     */
                    if ((pAddrInfo->u1AdminStatus == IP6FWD_NOT_READY) &&
                        (i4SetValFsipv6AddrSENDCgaStatus == IP6_TRUE))
                    {
                        Ip6AddrCopy (&pAddrInfo->ip6Addr, &Fsipv6Address);
                        u1Hit = IP6_ONE;
                        break;
                    }
                    else
                    {
                        continue;
                    }
                }

                if (u1Hit != IP6_ONE)
                {
                    Ip6RelMem (pIf6->pIp6Cxt->u4ContextId,
                               (UINT2) gIp6GblInfo.i4Ip6Nd6SecurePoolId,
                               pu1Buffer);
                    return SNMP_FAILURE;
                }
            }
            else
            {
                /* Required Address is not present. */
                Ip6RelMem (pIf6->pIp6Cxt->u4ContextId,
                           (UINT2) gIp6GblInfo.i4Ip6Nd6SecurePoolId, pu1Buffer);
                return SNMP_FAILURE;
            }
        }

        pAddrInfo->b1SeNDCgaStatus = (BOOL1) i4SetValFsipv6AddrSENDCgaStatus;

        if (SSL_FAILURE == SslArGetDerRsaPubKey (&pu1Buffer,
                                                 &(pAddrInfo->cgaParams.
                                                   u2PubKeyLen)))
        {
            Ip6RelMem (pIf6->pIp6Cxt->u4ContextId,
                       (UINT2) gIp6GblInfo.i4Ip6Nd6SecurePoolId, pu1Buffer);
            return SNMP_FAILURE;
        }

        /* 
         * pu1Buffer is modified to point to last in SslArGetDerRsaPubKey
         * so assign key from pu1TempBuffer
         */
        pAddrInfo->cgaParams.pu1DerPubKey = pu1TempBuffer;
        MEMCPY (pAddrInfo->cgaParams.au1Prefix,
                &Fsipv6Address, ND6_SEND_CGA_PREFIX_LEN);

        /*
         * CGA generation in standby results in different CGA address
         * due to different random number, so it will be synced dynamically
         * Similarly, CGA generation is not required in MSR
         */
        if ((ND6_GET_NODE_STATUS () == RM_ACTIVE) &&
            (MsrIsMibRestoreInProgress () != OSIX_SUCCESS))
        {
            if (Nd6SeNDCgaGenerate (pIf6, &(pAddrInfo->ip6Addr),
                                    &(pAddrInfo->cgaParams)) == IP6_FAILURE)
            {
                Ip6RelMem (pIf6->pIp6Cxt->u4ContextId,
                           (UINT2) gIp6GblInfo.i4Ip6Nd6SecurePoolId, pu1Buffer);
                return SNMP_FAILURE;
            }
            CgaModifier.pu1_OctetList = pAddrInfo->cgaParams.au1Modifier;
            CgaModifier.i4_Length = ND6_SEND_CGA_MOD_LEN;
        }
        /*
         * Copy the generated address to original address to be 
         * used by other functions
         */
        Ip6AddrCopy ((tIp6Addr *) (VOID *) pFsipv6AddrAddress->pu1_OctetList,
                     &(pAddrInfo->ip6Addr));
    }

    IncMsrForIpv6AddrTable (i4Fsipv6AddrIndex, pFsipv6AddrAddress,
                            i4Fsipv6AddrPrefixLen, 'i',
                            &i4SetValFsipv6AddrSENDCgaStatus,
                            FsMIIpv6AddrSENDCgaStatus,
                            sizeof (FsMIIpv6AddrSENDCgaStatus) / sizeof (UINT4),
                            FALSE);

    /* sync the modifier as well */
    IncMsrForIpv6AddrTable (i4Fsipv6AddrIndex, pFsipv6AddrAddress,
                            i4Fsipv6AddrPrefixLen, 's',
                            &CgaModifier,
                            FsMIIpv6AddrSENDCgaModifier,
                            sizeof (FsMIIpv6AddrSENDCgaModifier) /
                            sizeof (UINT4), FALSE);

    Ip6RelMem (pIf6->pIp6Cxt->u4ContextId,
               (UINT2) gIp6GblInfo.i4Ip6Nd6SecurePoolId, pu1Buffer);
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (i4Fsipv6AddrIndex);
    UNUSED_PARAM (pFsipv6AddrAddress);
    UNUSED_PARAM (i4Fsipv6AddrPrefixLen);
    UNUSED_PARAM (i4SetValFsipv6AddrSENDCgaStatus);

    return SNMP_FAILURE;
#endif /* SSL_WANTED */
}

/****************************************************************************
 Function    :  nmhSetFsipv6AddrSENDCollisionCount
 Input       :  The Indices
                Fsipv6AddrIndex
                Fsipv6AddrAddress
                Fsipv6AddrPrefixLen

                The Object
                setValFsipv6AddrSENDCollisionCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6AddrSENDCollisionCount (INT4 i4Fsipv6AddrIndex,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsipv6AddrAddress,
                                    INT4 i4Fsipv6AddrPrefixLen,
                                    INT4 i4SetValFsipv6AddrSENDCollisionCount)
{
    tIp6AddrInfo       *pAddrInfo = NULL;
    tIp6LlocalInfo     *pLlocalInfo = NULL;
    tIp6Addr            Fsipv6Address;
    tIp6If             *pIf6 = NULL;

    pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6AddrIndex);
    if (pIf6 == NULL)
    {
        CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
        return SNMP_FAILURE;
    }

    Ip6AddrCopy (&Fsipv6Address,
                 (tIp6Addr *) (VOID *) pFsipv6AddrAddress->pu1_OctetList);

    if (Ip6AddrType (&Fsipv6Address) == ADDR6_LLOCAL)
    {
        pLlocalInfo =
            Ip6AddrTblGetLlocalEntry ((UINT4) i4Fsipv6AddrIndex,
                                      (tIp6Addr *) & Fsipv6Address);
        if (pLlocalInfo == NULL)
        {
            return SNMP_FAILURE;
        }
        pLlocalInfo->cgaParams.u1Collisions =
            (UINT1) i4SetValFsipv6AddrSENDCollisionCount;
    }
    pAddrInfo =
        Ip6AddrTblGetEntry ((UINT4) i4Fsipv6AddrIndex,
                            (tIp6Addr *) & Fsipv6Address,
                            (UINT1) i4Fsipv6AddrPrefixLen);

    if (pAddrInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    pAddrInfo->cgaParams.u1Collisions =
        (UINT1) i4SetValFsipv6AddrSENDCollisionCount;
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Fsipv6AddrAdminStatus
 Input       :  The Indices
                Fsipv6AddrIndex
                Fsipv6AddrAddress
                Fsipv6AddrPrefixLen

                The Object 
                testValFsipv6AddrAdminStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6AddrAdminStatus (UINT4 *pu4ErrorCode, INT4 i4Fsipv6AddrIndex,
                                tSNMP_OCTET_STRING_TYPE * pFsipv6AddrAddress,
                                INT4 i4Fsipv6AddrPrefixLen,
                                INT4 i4TestValFsipv6AddrAdminStatus)
{
    tIp6AddrInfo       *pAddrInfo = NULL;
    tIp6AddrInfo       *pAdrInfo = NULL;
    tIp6LlocalInfo     *pLlocalInfo = NULL;
    tIp6If             *pIf6 = NULL;
    tIp6Addr            ip6addr;
    tIp6Addr           *pIp6addr = NULL;
    UINT4               u4ContextId = 0;
#ifdef VRRP_WANTED
    tIPvXAddr           CurIpAddr;
    tIPvXAddr           PrevIpAddr;
#endif

#ifdef VRRP_WANTED
    IPVX_ADDR_CLEAR (&CurIpAddr);
    IPVX_ADDR_CLEAR (&PrevIpAddr);
#endif

    pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6AddrIndex);
    if (pIf6 == NULL)
    {
        /* Interface Does not Exist */
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
        return SNMP_FAILURE;
    }

    Ip6AddrCopy (&ip6addr,
                 (tIp6Addr *) (VOID *) pFsipv6AddrAddress->pu1_OctetList);

    /* Validate the Address. */
    if (IS_ADDR_MULTI (ip6addr) || IS_ADDR_UNSPECIFIED (ip6addr) ||
        IS_ADDR_LOOPBACK (ip6addr) || IS_ADDR_V4_COMPAT (ip6addr))
    {
        /* Invalid Address. */
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_ADDRESS);
        return SNMP_FAILURE;
    }

    /* Validate the Address Prefix Length */
    if ((i4Fsipv6AddrPrefixLen < IP6_ADDR_MIN_PREFIX) ||
        (i4Fsipv6AddrPrefixLen > IP6_ADDR_MAX_PREFIX))
    {
        /* Invalid Address Prefix Length */
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_PREFIXLEN);
        return SNMP_FAILURE;
    }

    if (CfaIsL3SubIfIndex ((UINT4) i4Fsipv6AddrIndex) == CFA_TRUE)
    {
        /* Ipv6 address configuration to l3Subinterface is allowed only if the
         * encapsulation type is set , So check the encapsualtion */
        if (Ip6GetDot1qVlanEncapStatus ((UINT4) i4Fsipv6AddrIndex) == IP6_FALSE)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_IP6_L3SUB_IF_DOT1Q_VLAN_DISABLED);
            return SNMP_FAILURE;
        }
    }

    /* Validate the Row Status Value. */
    if ((i4TestValFsipv6AddrAdminStatus == IP6FWD_NOT_READY) ||
        (i4TestValFsipv6AddrAdminStatus == IP6FWD_NOT_IN_SERVICE) ||
        (i4TestValFsipv6AddrAdminStatus < IP6FWD_ACTIVE) ||
        (i4TestValFsipv6AddrAdminStatus > IP6FWD_DESTROY))
    {
        /* Invalid Row Status Value. */
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (IS_ADDR_LLOCAL (ip6addr))
    {
        if (i4Fsipv6AddrPrefixLen != IP6_ADDR_MAX_PREFIX)
        {
            /* Invalid Prefix Length. For Link-Local Address Length is 128 */
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            CLI_SET_ERR (CLI_IP6_INVALID_PREFIXLEN);
            return SNMP_FAILURE;
        }

        pLlocalInfo =
            Ip6AddrTblGetLlocalEntry ((UINT4) i4Fsipv6AddrIndex,
                                      (tIp6Addr *) (VOID *)
                                      pFsipv6AddrAddress->pu1_OctetList);
    }
    else
    {
        if (pIf6->u4UnnumAssocIPv6If != 0)
        {
            /* UNICAST Address cannot be configured if interface is unnumbered */
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            CLI_SET_ERR (CLI_IP6_UNNUM_PRESENT_FOR_ADDR);
            return SNMP_FAILURE;
        }
        if (((i4TestValFsipv6AddrAdminStatus == IP6FWD_DESTROY)
             && (Ip6UtlGetUnnumIfEntry ((UINT4) i4Fsipv6AddrIndex))) ==
            OSIX_FAILURE)
        {
            CLI_SET_ERR (CLI_IP6_DEL_UNNUMBERED_ADDR);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;

        }
        if ((i4TestValFsipv6AddrAdminStatus == IP6FWD_CREATE_AND_GO) ||
            (i4TestValFsipv6AddrAdminStatus == IP6FWD_CREATE_AND_WAIT))
        {
            /* UNICAST Address - We should not configure more than one
             * address for a node, which share the same prefix. */
            Ip6GetCxtId ((UINT4) i4Fsipv6AddrIndex, &u4ContextId);
            pAdrInfo = Ip6AddrScanAllIfInCxt (u4ContextId, &ip6addr,
                                              (UINT1) i4Fsipv6AddrPrefixLen);
            if (pIf6->u1IfStatusOnLink == IPV6_IF_ACTIVE)
            {
                if (pAdrInfo != NULL)
                {
                    if ((pIf6->u4OnLinkActiveIfId !=
                         pAdrInfo->pIf6->u4OnLinkActiveIfId)
                        || (pAdrInfo->pIf6->u1IfStatusOnLink !=
                            IPV6_IF_STANDBY))
                    {
                        /* Already a UNICAST address is present for the same prefix */
                        CLI_SET_ERR (CLI_IP6_ADDR_ALREADY_EXISTS);
                        return SNMP_FAILURE;
                    }
                }
            }
            else if (pIf6->u1IfStatusOnLink == IPV6_IF_STANDBY)
            {
                if (pAdrInfo != NULL)
                {
                    if ((pIf6->u4OnLinkActiveIfId !=
                         pAdrInfo->pIf6->u4OnLinkActiveIfId)
                        || (pAdrInfo->pIf6->u1IfStatusOnLink != IPV6_IF_ACTIVE))
                    {
                        /* Already a UNICAST address is present for the same prefix */
                        CLI_SET_ERR (CLI_IP6_ADDR_ALREADY_EXISTS);
                        return SNMP_FAILURE;
                    }
                }
            }

#ifdef TUNNEL_WANTED
            /* If the address is for Auto tunnel interface check the 
             * address compatiblity with the interface's ipv4 address */
            if (Ip6CheckAutoTunlAddr (&ip6addr, i4Fsipv6AddrIndex) ==
                IP6_FAILURE)
            {
                CLI_SET_ERR (CLI_IP6_INCOMPATIBLE_ADDR);
                return SNMP_FAILURE;
            }
#endif
        }
        pAddrInfo =
            Ip6AddrTblGetEntry ((UINT2) i4Fsipv6AddrIndex,
                                (tIp6Addr *) & ip6addr,
                                (UINT1) i4Fsipv6AddrPrefixLen);
    }

    if ((pIf6->u1IfType == IP6_LOOPBACK_INTERFACE_TYPE) &&
        (i4Fsipv6AddrPrefixLen != IP6_ADDR_MAX_PREFIX))
    {
        /* Invalid Prefix Length. For Loopback Address Length is 128 */
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        CLI_SET_ERR (CLI_IP6_INVALID_PREFIXLEN);
        return SNMP_FAILURE;
    }

    if ((pAddrInfo == NULL) && (pLlocalInfo == NULL))
    {
        /* Address does not exist. */
        if ((i4TestValFsipv6AddrAdminStatus == IP6FWD_ACTIVE) ||
            (i4TestValFsipv6AddrAdminStatus == IP6FWD_DESTROY))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            CLI_SET_ERR (CLI_IP6_ADDR_NOT_FOUND);
            return SNMP_FAILURE;
        }
    }
    else
    {
        /* Address exist. */
        if ((i4TestValFsipv6AddrAdminStatus == IP6FWD_CREATE_AND_GO) ||
            (i4TestValFsipv6AddrAdminStatus == IP6FWD_CREATE_AND_WAIT))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            CLI_SET_ERR (CLI_IP6_ADDR_ALREADY_EXISTS);
            return SNMP_FAILURE;
        }
    }
#ifdef VRRP_WANTED
    pIp6addr = &ip6addr;
    IPVX_ADDR_INIT_FROMV6 (CurIpAddr, pIp6addr);

    if ((i4TestValFsipv6AddrAdminStatus == IP6FWD_CREATE_AND_GO) ||
        (i4TestValFsipv6AddrAdminStatus == IP6FWD_CREATE_AND_WAIT))
    {
        if (VrrpApiValDecPriority (i4Fsipv6AddrIndex,
                                   (UINT1) VRRP_VAL_PRIO_OPER_IP_ADD,
                                   PrevIpAddr, CurIpAddr) == VRRP_NOT_OK)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    else if (i4TestValFsipv6AddrAdminStatus == IP6FWD_DESTROY)
    {
        if (VrrpApiValIfIpChange (i4Fsipv6AddrIndex, CurIpAddr, 0) == VRRP_OK)
        {
            CLI_SET_ERR (CLI_IP6_IP_NO_CHG_VRRP_INUSE);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }

        if (VrrpApiValDecPriority (i4Fsipv6AddrIndex,
                                   (UINT1) VRRP_VAL_PRIO_OPER_IP_DEL,
                                   PrevIpAddr, CurIpAddr) == VRRP_NOT_OK)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
#else
    UNUSED_PARAM (pIp6addr);
#endif

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6AddrType
 Input       :  The Indices
                Fsipv6AddrIndex
                Fsipv6AddrAddress
                Fsipv6AddrPrefixLen

                The Object 
                testValFsipv6AddrType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6AddrType (UINT4 *pu4ErrorCode, INT4 i4Fsipv6AddrIndex,
                         tSNMP_OCTET_STRING_TYPE * pFsipv6AddrAddress,
                         INT4 i4Fsipv6AddrPrefixLen,
                         INT4 i4TestValFsipv6AddrType)
{
    tIp6AddrInfo       *pAddrInfo;
    tIp6LlocalInfo     *pLlocalInfo = NULL;
    tIp6Addr            ip6addr;
    tIp6If             *pIf6 = NULL;
    *pu4ErrorCode = SNMP_ERR_NO_CREATION;

    if (Ip6ifEntryExists ((UINT4) i4Fsipv6AddrIndex) == IP6_FAILURE)
    {
        CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
        return SNMP_FAILURE;
    }
    pIf6 = gIp6GblInfo.apIp6If[i4Fsipv6AddrIndex];
    if (pIf6 == NULL)
    {
        CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
        return SNMP_FAILURE;
    }

    if (pFsipv6AddrAddress == NULL)
    {
        CLI_SET_ERR (CLI_IP6_INVALID_ADDRESS);
        return SNMP_FAILURE;
    }
    Ip6AddrCopy (&ip6addr,
                 (tIp6Addr *) (VOID *) pFsipv6AddrAddress->pu1_OctetList);
    if (IS_ADDR_MULTI (ip6addr) || IS_ADDR_UNSPECIFIED (ip6addr))
    {
        CLI_SET_ERR (CLI_IP6_INVALID_ADDRESS);
        return SNMP_FAILURE;
    }

    if ((i4Fsipv6AddrPrefixLen < IP6_ADDR_MIN_PREFIX) ||
        (i4Fsipv6AddrPrefixLen > IP6_ADDR_MAX_PREFIX))
    {
        CLI_SET_ERR (CLI_IP6_INVALID_PREFIXLEN);
        return SNMP_FAILURE;
    }
    if ((i4TestValFsipv6AddrType != IP6_ADDR_TYPE_UNICAST) &&
        (i4TestValFsipv6AddrType != IP6_ADDR_TYPE_ANYCAST) &&
        (i4TestValFsipv6AddrType != IP6_ADDR_TYPE_LINK_LOCAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_INVALID_ADDR_TYPE);
        return SNMP_FAILURE;
    }

    /* ANYCAST address can be set only for router and not for Host */
    if ((i4TestValFsipv6AddrType == IP6_ADDR_TYPE_ANYCAST) &&
        (pIf6->u1Ipv6IfFwdOperStatus == IP6_IF_FORW_DISABLE))
    {
        /* Routing Not Enabled. So Set FAILS. */
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_IP6_INVALID_FORWARDING_STATUS);
        return SNMP_FAILURE;
    }

    if (i4TestValFsipv6AddrType == IP6_ADDR_TYPE_LINK_LOCAL)
    {
        if (IS_ADDR_LLOCAL (ip6addr) == 0)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_IP6_INVALID_ADDRESS);
            return SNMP_FAILURE;
        }

        pLlocalInfo =
            Ip6AddrTblGetLlocalEntry ((UINT4) i4Fsipv6AddrIndex,
                                      (tIp6Addr *) (VOID *)
                                      pFsipv6AddrAddress->pu1_OctetList);
        if (pLlocalInfo == NULL)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_IP6_ADDR_NOT_FOUND);
            return SNMP_FAILURE;
        }
    }
    else
    {
        pAddrInfo = Ip6AddrTblGetEntry ((UINT4) i4Fsipv6AddrIndex,
                                        (tIp6Addr *) (VOID *)
                                        pFsipv6AddrAddress->pu1_OctetList,
                                        (UINT1) i4Fsipv6AddrPrefixLen);

        if (pAddrInfo != NULL)
        {
            if (pAddrInfo->u1Status == ADDR6_UP)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_IP6_ADDR_ALREADY_EXISTS);
                return SNMP_FAILURE;
            }
        }
        else
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_IP6_ADDR_NOT_FOUND);
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6AddrProfIndex
 Input       :  The Indices
                Fsipv6AddrIndex
                Fsipv6AddrAddress
                Fsipv6AddrPrefixLen

                The Object 
                testValFsipv6AddrProfIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6AddrProfIndex (UINT4 *pu4ErrorCode, INT4 i4Fsipv6AddrIndex,
                              tSNMP_OCTET_STRING_TYPE * pFsipv6AddrAddress,
                              INT4 i4Fsipv6AddrPrefixLen,
                              INT4 i4TestValFsipv6AddrProfIndex)
{
    tIp6Addr            ip6addr;

    if (i4TestValFsipv6AddrProfIndex >= (INT4) gIp6GblInfo.u4MaxAddrProfileLimit
        || i4TestValFsipv6AddrProfIndex < IP6_MIN_ADDR_PROFILES)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    *pu4ErrorCode = SNMP_ERR_NO_CREATION;

    if (Ip6ifEntryExists ((UINT4) i4Fsipv6AddrIndex) == IP6_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (pFsipv6AddrAddress == NULL)
    {
        return SNMP_FAILURE;
    }

    Ip6AddrCopy (&ip6addr,
                 (tIp6Addr *) (VOID *) pFsipv6AddrAddress->pu1_OctetList);

    if (IS_ADDR_MULTI (ip6addr) || IS_ADDR_UNSPECIFIED (ip6addr) ||
        IS_ADDR_LLOCAL (ip6addr))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4Fsipv6AddrPrefixLen < IP6_ADDR_MIN_PREFIX) ||
        (i4Fsipv6AddrPrefixLen > IP6_ADDR_MAX_PREFIX))
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6AddrSENDCgaModifier
 Input       :  The Indices
                Fsipv6AddrIndex
                Fsipv6AddrAddress
                Fsipv6AddrPrefixLen

                The Object
                testValFsipv6AddrSENDCgaModifier
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6AddrSENDCgaModifier (UINT4 *pu4ErrorCode, INT4 i4Fsipv6AddrIndex,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsipv6AddrAddress,
                                    INT4 i4Fsipv6AddrPrefixLen,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pTestValFsipv6AddrSENDCgaModifier)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4Fsipv6AddrIndex);
    UNUSED_PARAM (pFsipv6AddrAddress);
    UNUSED_PARAM (i4Fsipv6AddrPrefixLen);
    UNUSED_PARAM (pTestValFsipv6AddrSENDCgaModifier);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6AddrSENDCgaStatus
 Input       :  The Indices
                Fsipv6AddrIndex
                Fsipv6AddrAddress
                Fsipv6AddrPrefixLen

                The Object 
                testValFsipv6AddrSENDCgaStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6AddrSENDCgaStatus (UINT4 *pu4ErrorCode, INT4 i4Fsipv6AddrIndex,
                                  tSNMP_OCTET_STRING_TYPE * pFsipv6AddrAddress,
                                  INT4 i4Fsipv6AddrPrefixLen,
                                  INT4 i4TestValFsipv6AddrSENDCgaStatus)
{
    INT4                i4RetValFsipv6IfSENDSecStatus = 0;

    *pu4ErrorCode = SNMP_ERR_NO_CREATION;

    nmhGetFsipv6IfSENDSecStatus (i4Fsipv6AddrIndex,
                                 &i4RetValFsipv6IfSENDSecStatus);
    if (i4RetValFsipv6IfSENDSecStatus == 2)
    {
        CLI_SET_ERR (CLI_IP6_SEND_NOT_ENABLE);
        return SNMP_FAILURE;
    }

    if (Ip6ifEntryExists ((UINT4) i4Fsipv6AddrIndex) == IP6_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (pFsipv6AddrAddress == NULL)
    {
        return SNMP_FAILURE;
    }

    if ((i4Fsipv6AddrPrefixLen < IP6_ADDR_MIN_PREFIX) ||
        (i4Fsipv6AddrPrefixLen > IP6_ADDR_MAX_PREFIX))
    {
        return SNMP_FAILURE;
    }
#ifdef SSL_WANTED
    if (SslArCheckRsaKey () == SSL_FAILURE)
    {
        CLI_SET_ERR (CLI_IP6_SEND_RSA_ERROR);
        return SNMP_FAILURE;
    }
#else
    return SNMP_FAILURE;
#endif
    if (gNd6GblInfo.u1Nd6SeNDSec > 1)
    {
        CLI_SET_ERR (CLI_IP6_SEND_RSA_ERROR);
        return SNMP_FAILURE;
    }

    if (i4TestValFsipv6AddrSENDCgaStatus != 1)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6AddrSENDCollisionCount
 Input       :  The Indices
                Fsipv6AddrIndex
                Fsipv6AddrAddress
                Fsipv6AddrPrefixLen

                The Object
                testValFsipv6AddrSENDCollisionCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6AddrSENDCollisionCount (UINT4 *pu4ErrorCode,
                                       INT4 i4Fsipv6AddrIndex,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFsipv6AddrAddress,
                                       INT4 i4Fsipv6AddrPrefixLen,
                                       INT4
                                       i4TestValFsipv6AddrSENDCollisionCount)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4Fsipv6AddrIndex);
    UNUSED_PARAM (pFsipv6AddrAddress);
    UNUSED_PARAM (i4Fsipv6AddrPrefixLen);
    UNUSED_PARAM (i4TestValFsipv6AddrSENDCollisionCount);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Fsipv6AddrTable
 Input       :  The Indices
                Fsipv6AddrIndex
                Fsipv6AddrAddress
                Fsipv6AddrPrefixLen
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsipv6AddrTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Fsipv6AddrProfileTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsipv6AddrProfileTable
 Input       :  The Indices
                Fsipv6AddrProfileIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsipv6AddrProfileTable (UINT4 u4Fsipv6AddrProfileIndex)
{
    if (u4Fsipv6AddrProfileIndex < gIp6GblInfo.u4MaxAddrProfileLimit)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsipv6AddrProfileTable
 Input       :  The Indices
                Fsipv6AddrProfileIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsipv6AddrProfileTable (UINT4 *pu4Fsipv6AddrProfileIndex)
{
    *pu4Fsipv6AddrProfileIndex = 0;
    if (Ip6addrProfEntryExists (*pu4Fsipv6AddrProfileIndex) == IP6_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsipv6AddrProfileTable
 Input       :  The Indices
                Fsipv6AddrProfileIndex
                nextFsipv6AddrProfileIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsipv6AddrProfileTable (UINT4 u4Fsipv6AddrProfileIndex,
                                       UINT4 *pu4NextFsipv6AddrProfileIndex)
{
    UINT4               u4Index;

    /* if index is max value of UNIT4 ie 4294967295 , return failure */
    /*  if ((u4Fsipv6AddrProfileIndex + 1) == 0) */
    if (u4Fsipv6AddrProfileIndex == IP6_MAX_VALUE)
    {
        return (SNMP_FAILURE);
    }

    for (u4Index = u4Fsipv6AddrProfileIndex + 1;
         u4Index < gIp6GblInfo.u4MaxAddrProfileLimit; u4Index++)
    {
        if (Ip6addrProfEntryExists (u4Index) == IP6_SUCCESS)
        {
            *pu4NextFsipv6AddrProfileIndex = u4Index;
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsipv6AddrProfileStatus
 Input       :  The Indices
                Fsipv6AddrProfileIndex

                The Object 
                retValFsipv6AddrProfileStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6AddrProfileStatus (UINT4 u4Fsipv6AddrProfileIndex,
                               INT4 *pi4RetValFsipv6AddrProfileStatus)
{
    if (Ip6addrProfEntryExists (u4Fsipv6AddrProfileIndex) == IP6_SUCCESS)
    {
        *pi4RetValFsipv6AddrProfileStatus =
            (INT4) IP6_ADDR_PROF_ADMIN (u4Fsipv6AddrProfileIndex);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsipv6AddrProfilePrefixAdvStatus
 Input       :  The Indices
                Fsipv6AddrProfileIndex

                The Object 
                retValFsipv6AddrProfilePrefixAdvStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6AddrProfilePrefixAdvStatus (UINT4 u4Fsipv6AddrProfileIndex,
                                        INT4
                                        *pi4RetValFsipv6AddrProfilePrefixAdvStatus)
{
    if (Ip6addrProfEntryExists (u4Fsipv6AddrProfileIndex) == IP6_SUCCESS)
    {

        if (IP6_ADDR_SEND_PREFIX (u4Fsipv6AddrProfileIndex))
        {
            *pi4RetValFsipv6AddrProfilePrefixAdvStatus =
                (INT4) IP6_ADDR_PROF_PREF_ADV_ON;
            return SNMP_SUCCESS;
        }
        else
            *pi4RetValFsipv6AddrProfilePrefixAdvStatus =
                (INT4) IP6_ADDR_PROF_PREF_ADV_OFF;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsipv6AddrProfileOnLinkAdvStatus
 Input       :  The Indices
                Fsipv6AddrProfileIndex

                The Object 
                retValFsipv6AddrProfileOnLinkAdvStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6AddrProfileOnLinkAdvStatus (UINT4 u4Fsipv6AddrProfileIndex,
                                        INT4
                                        *pi4RetValFsipv6AddrProfileOnLinkAdvStatus)
{
    if (Ip6addrProfEntryExists (u4Fsipv6AddrProfileIndex) == IP6_SUCCESS)
    {
        if (IP6_ADDR_ON_LINK (u4Fsipv6AddrProfileIndex))
        {
            *pi4RetValFsipv6AddrProfileOnLinkAdvStatus =
                (INT4) IP6_ADDR_PROF_ONLINK_ADV_ON;
            return SNMP_SUCCESS;
        }
        *pi4RetValFsipv6AddrProfileOnLinkAdvStatus =
            (INT4) IP6_ADDR_PROF_ONLINK_ADV_OFF;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsipv6AddrProfileAutoConfAdvStatus
 Input       :  The Indices
                Fsipv6AddrProfileIndex

                The Object 
                retValFsipv6AddrProfileAutoConfAdvStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6AddrProfileAutoConfAdvStatus (UINT4 u4Fsipv6AddrProfileIndex,
                                          INT4
                                          *pi4RetValFsipv6AddrProfileAutoConfAdvStatus)
{
    if (Ip6addrProfEntryExists (u4Fsipv6AddrProfileIndex) == IP6_SUCCESS)
    {
        if (IP6_ADDR_AUTO_CONFIG (u4Fsipv6AddrProfileIndex))
        {
            *pi4RetValFsipv6AddrProfileAutoConfAdvStatus =
                (INT4) IP6_ADDR_PROF_AUTO_ADV_ON;
            return SNMP_SUCCESS;
        }
        *pi4RetValFsipv6AddrProfileAutoConfAdvStatus =
            (INT4) IP6_ADDR_PROF_AUTO_ADV_OFF;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsipv6AddrProfilePreferredTime
 Input       :  The Indices
                Fsipv6AddrProfileIndex

                The Object 
                retValFsipv6AddrProfilePreferredTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6AddrProfilePreferredTime (UINT4 u4Fsipv6AddrProfileIndex,
                                      UINT4
                                      *pu4RetValFsipv6AddrProfilePreferredTime)
{
    if (Ip6addrProfEntryExists (u4Fsipv6AddrProfileIndex) == IP6_SUCCESS)
    {
        *pu4RetValFsipv6AddrProfilePreferredTime =
            IP6_ADDR_PREF_TIME (u4Fsipv6AddrProfileIndex);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsipv6AddrProfileValidTime
 Input       :  The Indices
                Fsipv6AddrProfileIndex

                The Object 
                retValFsipv6AddrProfileValidTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6AddrProfileValidTime (UINT4 u4Fsipv6AddrProfileIndex,
                                  UINT4 *pu4RetValFsipv6AddrProfileValidTime)
{
    if (Ip6addrProfEntryExists (u4Fsipv6AddrProfileIndex) == IP6_SUCCESS)
    {
        *pu4RetValFsipv6AddrProfileValidTime =
            IP6_ADDR_VALID_TIME (u4Fsipv6AddrProfileIndex);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsipv6AddrProfileValidLifeTimeFlag
 Input       :  The Indices
                Fsipv6AddrProfileIndex

                The Object 
                retValFsipv6AddrProfileValidLifeTimeFlag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6AddrProfileValidLifeTimeFlag (UINT4 u4Fsipv6AddrProfileIndex,
                                          INT4
                                          *pi4RetValFsipv6AddrProfileValidLifeTimeFlag)
{
    if (Ip6addrProfEntryExists (u4Fsipv6AddrProfileIndex) == IP6_SUCCESS)
    {
        if (IP6_ADDR_VALID_TIME_FIXED (u4Fsipv6AddrProfileIndex))
            *pi4RetValFsipv6AddrProfileValidLifeTimeFlag = IP6_FIXED_TIME;
        else
            *pi4RetValFsipv6AddrProfileValidLifeTimeFlag = IP6_VARIABLE_TIME;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsipv6AddrProfilePreferredLifeTimeFlag
 Input       :  The Indices
                Fsipv6AddrProfileIndex

                The Object 
                retValFsipv6AddrProfilePreferredLifeTimeFlag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6AddrProfilePreferredLifeTimeFlag (UINT4 u4Fsipv6AddrProfileIndex,
                                              INT4
                                              *pi4RetValFsipv6AddrProfilePreferredLifeTimeFlag)
{
    if (Ip6addrProfEntryExists (u4Fsipv6AddrProfileIndex) == IP6_SUCCESS)
    {
        if (IP6_ADDR_PREF_TIME_FIXED (u4Fsipv6AddrProfileIndex))
            *pi4RetValFsipv6AddrProfilePreferredLifeTimeFlag = IP6_FIXED_TIME;
        else
            *pi4RetValFsipv6AddrProfilePreferredLifeTimeFlag =
                IP6_VARIABLE_TIME;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsipv6AddrProfileStatus
 Input       :  The Indices
                Fsipv6AddrProfileIndex

                The Object 
                setValFsipv6AddrProfileStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6AddrProfileStatus (UINT4 u4Fsipv6AddrProfileIndex,
                               INT4 i4SetValFsipv6AddrProfileStatus)
{
    if (Ip6addrProfEntryExists (u4Fsipv6AddrProfileIndex) == IP6_SUCCESS)
    {
        /* first entry cannot be deleted */
        if ((i4SetValFsipv6AddrProfileStatus == IP6_ADDR_PROF_INVALID)
            && (u4Fsipv6AddrProfileIndex == 0))
            return SNMP_FAILURE;
        IP6_ADDR_PROF_ADMIN (u4Fsipv6AddrProfileIndex) =
            (UINT1) i4SetValFsipv6AddrProfileStatus;
        IncMsrForIpv6AddrProfileTable (u4Fsipv6AddrProfileIndex,
                                       'i', &i4SetValFsipv6AddrProfileStatus,
                                       FsMIIpv6AddrProfileStatus,
                                       sizeof (FsMIIpv6AddrProfileStatus) /
                                       sizeof (UINT4));
        return SNMP_SUCCESS;
    }
    else
    {                            /* create a profile entry */
        if (i4SetValFsipv6AddrProfileStatus == IP6_ADDR_PROF_VALID)
        {
            IP6_ADDR_PROF_ADMIN (u4Fsipv6AddrProfileIndex) =
                (UINT1) i4SetValFsipv6AddrProfileStatus;
            IncMsrForIpv6AddrProfileTable (u4Fsipv6AddrProfileIndex,
                                           'i',
                                           &i4SetValFsipv6AddrProfileStatus,
                                           FsMIIpv6AddrProfileStatus,
                                           sizeof (FsMIIpv6AddrProfileStatus) /
                                           sizeof (UINT4));
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsipv6AddrProfilePrefixAdvStatus
 Input       :  The Indices
                Fsipv6AddrProfileIndex

                The Object 
                setValFsipv6AddrProfilePrefixAdvStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6AddrProfilePrefixAdvStatus (UINT4 u4Fsipv6AddrProfileIndex,
                                        INT4
                                        i4SetValFsipv6AddrProfilePrefixAdvStatus)
{
    if (Ip6addrProfEntryExists (u4Fsipv6AddrProfileIndex) == IP6_SUCCESS)
    {
        if (i4SetValFsipv6AddrProfilePrefixAdvStatus ==
            IP6_ADDR_PROF_PREF_ADV_ON)
        {
            IP6_ADDR_PROF_CONF (u4Fsipv6AddrProfileIndex) |=
                IP6_ADDR_PREFIX_ADV;
        }

        if (i4SetValFsipv6AddrProfilePrefixAdvStatus ==
            IP6_ADDR_PROF_PREF_ADV_OFF)
        {
            IP6_ADDR_PROF_CONF (u4Fsipv6AddrProfileIndex) &=
                ~IP6_ADDR_PREFIX_ADV;
        }
        IncMsrForIpv6AddrProfileTable (u4Fsipv6AddrProfileIndex,
                                       'i',
                                       &i4SetValFsipv6AddrProfilePrefixAdvStatus,
                                       FsMIIpv6AddrProfilePrefixAdvStatus,
                                       sizeof
                                       (FsMIIpv6AddrProfilePrefixAdvStatus) /
                                       sizeof (UINT4));

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsipv6AddrProfileOnLinkAdvStatus
 Input       :  The Indices
                Fsipv6AddrProfileIndex

                The Object 
                setValFsipv6AddrProfileOnLinkAdvStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6AddrProfileOnLinkAdvStatus (UINT4 u4Fsipv6AddrProfileIndex,
                                        INT4
                                        i4SetValFsipv6AddrProfileOnLinkAdvStatus)
{
    if (Ip6addrProfEntryExists (u4Fsipv6AddrProfileIndex) == IP6_SUCCESS)
    {
        if (i4SetValFsipv6AddrProfileOnLinkAdvStatus ==
            IP6_ADDR_PROF_ONLINK_ADV_ON)
        {
            IP6_ADDR_PROF_CONF (u4Fsipv6AddrProfileIndex) |=
                IP6_ADDR_ONLINK_ADV;
        }

        if (i4SetValFsipv6AddrProfileOnLinkAdvStatus ==
            IP6_ADDR_PROF_ONLINK_ADV_OFF)
        {
            IP6_ADDR_PROF_CONF (u4Fsipv6AddrProfileIndex) &=
                ~IP6_ADDR_ONLINK_ADV;
        }
        IncMsrForIpv6AddrProfileTable (u4Fsipv6AddrProfileIndex,
                                       'i',
                                       &i4SetValFsipv6AddrProfileOnLinkAdvStatus,
                                       FsMIIpv6AddrProfileOnLinkAdvStatus,
                                       sizeof
                                       (FsMIIpv6AddrProfileOnLinkAdvStatus) /
                                       sizeof (UINT4));

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsipv6AddrProfileAutoConfAdvStatus
 Input       :  The Indices
                Fsipv6AddrProfileIndex

                The Object 
                setValFsipv6AddrProfileAutoConfAdvStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6AddrProfileAutoConfAdvStatus (UINT4 u4Fsipv6AddrProfileIndex,
                                          INT4
                                          i4SetValFsipv6AddrProfileAutoConfAdvStatus)
{
    if (Ip6addrProfEntryExists (u4Fsipv6AddrProfileIndex) == IP6_SUCCESS)
    {
        if (i4SetValFsipv6AddrProfileAutoConfAdvStatus ==
            IP6_ADDR_PROF_AUTO_ADV_ON)
        {
            IP6_ADDR_PROF_CONF (u4Fsipv6AddrProfileIndex) |= IP6_ADDR_AUTO_ADV;
        }

        if (i4SetValFsipv6AddrProfileAutoConfAdvStatus ==
            IP6_ADDR_PROF_AUTO_ADV_OFF)
        {
            IP6_ADDR_PROF_CONF (u4Fsipv6AddrProfileIndex) &= ~IP6_ADDR_AUTO_ADV;
        }
        IncMsrForIpv6AddrProfileTable (u4Fsipv6AddrProfileIndex,
                                       'i',
                                       &i4SetValFsipv6AddrProfileAutoConfAdvStatus,
                                       FsMIIpv6AddrProfileAutoConfAdvStatus,
                                       sizeof
                                       (FsMIIpv6AddrProfileAutoConfAdvStatus) /
                                       sizeof (UINT4));
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsipv6AddrProfilePreferredTime
 Input       :  The Indices
                Fsipv6AddrProfileIndex

                The Object 
                setValFsipv6AddrProfilePreferredTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6AddrProfilePreferredTime (UINT4 u4Fsipv6AddrProfileIndex,
                                      UINT4
                                      u4SetValFsipv6AddrProfilePreferredTime)
{
    if (Ip6addrProfEntryExists (u4Fsipv6AddrProfileIndex) == IP6_SUCCESS)
    {
        IP6_ADDR_PREF_TIME (u4Fsipv6AddrProfileIndex) =
            u4SetValFsipv6AddrProfilePreferredTime;
        IncMsrForIpv6AddrProfileTable (u4Fsipv6AddrProfileIndex,
                                       'u',
                                       &u4SetValFsipv6AddrProfilePreferredTime,
                                       FsMIIpv6AddrProfilePreferredTime,
                                       sizeof (FsMIIpv6AddrProfilePreferredTime)
                                       / sizeof (UINT4));
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsipv6AddrProfileValidTime
 Input       :  The Indices
                Fsipv6AddrProfileIndex

                The Object 
                setValFsipv6AddrProfileValidTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6AddrProfileValidTime (UINT4 u4Fsipv6AddrProfileIndex,
                                  UINT4 u4SetValFsipv6AddrProfileValidTime)
{
    if (Ip6addrProfEntryExists (u4Fsipv6AddrProfileIndex) == IP6_SUCCESS)
    {
        IP6_ADDR_VALID_TIME (u4Fsipv6AddrProfileIndex) =
            u4SetValFsipv6AddrProfileValidTime;
        IncMsrForIpv6AddrProfileTable (u4Fsipv6AddrProfileIndex,
                                       'u', &u4SetValFsipv6AddrProfileValidTime,
                                       FsMIIpv6AddrProfileValidTime,
                                       sizeof (FsMIIpv6AddrProfileValidTime) /
                                       sizeof (UINT4));
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsipv6AddrProfileValidLifeTimeFlag
 Input       :  The Indices
                Fsipv6AddrProfileIndex

                The Object 
                setValFsipv6AddrProfileValidLifeTimeFlag
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6AddrProfileValidLifeTimeFlag (UINT4 u4Fsipv6AddrProfileIndex,
                                          INT4
                                          i4SetValFsipv6AddrProfileValidLifeTimeFlag)
{
    if (Ip6addrProfEntryExists (u4Fsipv6AddrProfileIndex) == IP6_SUCCESS)
    {
        if (i4SetValFsipv6AddrProfileValidLifeTimeFlag == IP6_FIXED_TIME)
        {
            IP6_ADDR_PROF_CONF (u4Fsipv6AddrProfileIndex) |=
                IP6_ADDR_VALID_TIME_FLAG;
        }
        else
        {
            IP6_ADDR_PROF_CONF (u4Fsipv6AddrProfileIndex) &=
                ~IP6_ADDR_VALID_TIME_FLAG;
        }
        IncMsrForIpv6AddrProfileTable (u4Fsipv6AddrProfileIndex,
                                       'i',
                                       &i4SetValFsipv6AddrProfileValidLifeTimeFlag,
                                       FsMIIpv6AddrProfileValidLifeTimeFlag,
                                       sizeof
                                       (FsMIIpv6AddrProfileValidLifeTimeFlag) /
                                       sizeof (UINT4));
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsipv6AddrProfilePreferredLifeTimeFlag
 Input       :  The Indices
                Fsipv6AddrProfileIndex

                The Object 
                setValFsipv6AddrProfilePreferredLifeTimeFlag
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6AddrProfilePreferredLifeTimeFlag (UINT4 u4Fsipv6AddrProfileIndex,
                                              INT4
                                              i4SetValFsipv6AddrProfilePreferredLifeTimeFlag)
{
    if (Ip6addrProfEntryExists (u4Fsipv6AddrProfileIndex) == IP6_SUCCESS)
    {
        if (i4SetValFsipv6AddrProfilePreferredLifeTimeFlag == IP6_FIXED_TIME)
        {
            IP6_ADDR_PROF_CONF (u4Fsipv6AddrProfileIndex) |=
                IP6_ADDR_PREF_TIME_FLAG;
        }
        else
        {
            IP6_ADDR_PROF_CONF (u4Fsipv6AddrProfileIndex) &=
                ~IP6_ADDR_PREF_TIME_FLAG;
        }
        IncMsrForIpv6AddrProfileTable (u4Fsipv6AddrProfileIndex,
                                       'i',
                                       &i4SetValFsipv6AddrProfilePreferredLifeTimeFlag,
                                       FsMIIpv6AddrProfilePreferredLifeTimeFlag,
                                       sizeof
                                       (FsMIIpv6AddrProfilePreferredLifeTimeFlag)
                                       / sizeof (UINT4));

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Fsipv6AddrProfileStatus
 Input       :  The Indices
                Fsipv6AddrProfileIndex

                The Object 
                testValFsipv6AddrProfileStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6AddrProfileStatus (UINT4 *pu4ErrorCode,
                                  UINT4 u4Fsipv6AddrProfileIndex,
                                  INT4 i4TestValFsipv6AddrProfileStatus)
{
    if (u4Fsipv6AddrProfileIndex >= gIp6GblInfo.u4MaxAddrProfileLimit)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_PROF_INDEX);
        return SNMP_FAILURE;
    }

    if ((i4TestValFsipv6AddrProfileStatus != IP6_ADDR_PROF_INVALID)
        && (i4TestValFsipv6AddrProfileStatus != IP6_ADDR_PROF_VALID))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_INVALID_PROF_STATUS);
        return SNMP_FAILURE;
    }

    /* In case of invalidating an Profile entry, ensure that the profile
     * index is not used by any address or prefix. */
    if ((i4TestValFsipv6AddrProfileStatus == IP6_ADDR_PROF_INVALID) &&
        (IP6_ADDR_PROF_REF_COUNT (u4Fsipv6AddrProfileIndex) > 0))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_IP6_PROFILE_IN_USE);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6AddrProfilePrefixAdvStatus
 Input       :  The Indices
                Fsipv6AddrProfileIndex

                The Object 
                testValFsipv6AddrProfilePrefixAdvStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6AddrProfilePrefixAdvStatus (UINT4 *pu4ErrorCode,
                                           UINT4 u4Fsipv6AddrProfileIndex,
                                           INT4
                                           i4TestValFsipv6AddrProfilePrefixAdvStatus)
{
    if (u4Fsipv6AddrProfileIndex > gIp6GblInfo.u4MaxAddrProfileLimit)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_PROF_INDEX);
        return SNMP_FAILURE;
    }

    if ((i4TestValFsipv6AddrProfilePrefixAdvStatus != IP6_ADDR_PROF_PREF_ADV_ON)
        && (i4TestValFsipv6AddrProfilePrefixAdvStatus !=
            IP6_ADDR_PROF_PREF_ADV_OFF))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_INVALID_PREFIX_ADV_STATUS);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6AddrProfileOnLinkAdvStatus
 Input       :  The Indices
                Fsipv6AddrProfileIndex

                The Object 
                testValFsipv6AddrProfileOnLinkAdvStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6AddrProfileOnLinkAdvStatus (UINT4 *pu4ErrorCode,
                                           UINT4 u4Fsipv6AddrProfileIndex,
                                           INT4
                                           i4TestValFsipv6AddrProfileOnLinkAdvStatus)
{
    if (u4Fsipv6AddrProfileIndex > gIp6GblInfo.u4MaxAddrProfileLimit)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_PROF_INDEX);
        return SNMP_FAILURE;
    }

    if ((i4TestValFsipv6AddrProfileOnLinkAdvStatus !=
         IP6_ADDR_PROF_ONLINK_ADV_ON)
        && (i4TestValFsipv6AddrProfileOnLinkAdvStatus !=
            IP6_ADDR_PROF_ONLINK_ADV_OFF))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_PROF_ONLINK_ADV_STATUS);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6AddrProfileAutoConfAdvStatus
 Input       :  The Indices
                Fsipv6AddrProfileIndex

                The Object 
                testValFsipv6AddrProfileAutoConfAdvStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6AddrProfileAutoConfAdvStatus (UINT4 *pu4ErrorCode,
                                             UINT4 u4Fsipv6AddrProfileIndex,
                                             INT4
                                             i4TestValFsipv6AddrProfileAutoConfAdvStatus)
{
    if (u4Fsipv6AddrProfileIndex > gIp6GblInfo.u4MaxAddrProfileLimit)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_PROF_INDEX);
        return SNMP_FAILURE;
    }

    if ((i4TestValFsipv6AddrProfileAutoConfAdvStatus !=
         IP6_ADDR_PROF_AUTO_ADV_ON)
        && (i4TestValFsipv6AddrProfileAutoConfAdvStatus !=
            IP6_ADDR_PROF_AUTO_ADV_OFF))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_AUTOCONF_ADV_STATUS);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6AddrProfilePreferredTime
 Input       :  The Indices
                Fsipv6AddrProfileIndex

                The Object 
                testValFsipv6AddrProfilePreferredTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6AddrProfilePreferredTime (UINT4 *pu4ErrorCode,
                                         UINT4 u4Fsipv6AddrProfileIndex,
                                         UINT4
                                         u4TestValFsipv6AddrProfilePreferredTime)
{
    UNUSED_PARAM (u4TestValFsipv6AddrProfilePreferredTime);
    if (u4Fsipv6AddrProfileIndex > gIp6GblInfo.u4MaxAddrProfileLimit)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_PROF_INDEX);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6AddrProfileValidTime
 Input       :  The Indices
                Fsipv6AddrProfileIndex

                The Object 
                testValFsipv6AddrProfileValidTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6AddrProfileValidTime (UINT4 *pu4ErrorCode,
                                     UINT4 u4Fsipv6AddrProfileIndex,
                                     UINT4 u4TestValFsipv6AddrProfileValidTime)
{
    if (u4Fsipv6AddrProfileIndex > gIp6GblInfo.u4MaxAddrProfileLimit)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_PROF_INDEX);
        return SNMP_FAILURE;
    }
    UNUSED_PARAM (u4TestValFsipv6AddrProfileValidTime);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6AddrProfileValidLifeTimeFlag
 Input       :  The Indices
                Fsipv6AddrProfileIndex

                The Object 
                testValFsipv6AddrProfileValidLifeTimeFlag
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6AddrProfileValidLifeTimeFlag (UINT4 *pu4ErrorCode,
                                             UINT4 u4Fsipv6AddrProfileIndex,
                                             INT4
                                             i4TestValFsipv6AddrProfileValidLifeTimeFlag)
{
    if (u4Fsipv6AddrProfileIndex >= gIp6GblInfo.u4MaxAddrProfileLimit)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_PROF_INDEX);
        return SNMP_FAILURE;
    }
    if (i4TestValFsipv6AddrProfileValidLifeTimeFlag != IP6_FIXED_TIME &&
        i4TestValFsipv6AddrProfileValidLifeTimeFlag != IP6_VARIABLE_TIME)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_INV_VAL_LIFETIME_FLAG);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6AddrProfilePreferredLifeTimeFlag
 Input       :  The Indices
                Fsipv6AddrProfileIndex

                The Object 
                testValFsipv6AddrProfilePreferredLifeTimeFlag
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6AddrProfilePreferredLifeTimeFlag (UINT4 *pu4ErrorCode,
                                                 UINT4 u4Fsipv6AddrProfileIndex,
                                                 INT4
                                                 i4TestValFsipv6AddrProfilePreferredLifeTimeFlag)
{
    if (u4Fsipv6AddrProfileIndex >= gIp6GblInfo.u4MaxAddrProfileLimit)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_PROF_INDEX);
        return SNMP_FAILURE;
    }

    if (i4TestValFsipv6AddrProfilePreferredLifeTimeFlag != IP6_FIXED_TIME &&
        i4TestValFsipv6AddrProfilePreferredLifeTimeFlag != IP6_VARIABLE_TIME)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_INV_PREF_LIFETIME_FLAG);
        return SNMP_FAILURE;
    }

    /* PreferredLifeTimeFlag can be set to FIXED only if
     * ValidLifeTimeFlag is FIXED */
    if (i4TestValFsipv6AddrProfilePreferredLifeTimeFlag == IP6_FIXED_TIME)
    {
        if (!(IP6_ADDR_VALID_TIME_FIXED (u4Fsipv6AddrProfileIndex)))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_IP6_VALID_NOT_FIXED);
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Fsipv6AddrProfileTable
 Input       :  The Indices
                Fsipv6AddrProfileIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsipv6AddrProfileTable (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Fsipv6PmtuTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsipv6PmtuTable
 Input       :  The Indices
                Fsipv6PmtuDest
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1                nmhValidateIndexInstanceFsipv6PmtuTable
    (tSNMP_OCTET_STRING_TYPE * pFsipv6PmtuDest)
{
    INT4                i4AddrType;

    i4AddrType = Ip6AddrType ((tIp6Addr *) (VOID *)
                              pFsipv6PmtuDest->pu1_OctetList);

    if (i4AddrType == ADDR6_MULTI || i4AddrType == ADDR6_UNSPECIFIED)
    {
        return (SNMP_FAILURE);
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsipv6PmtuTable
 Input       :  The Indices
                Fsipv6PmtuDest
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsipv6PmtuTable (tSNMP_OCTET_STRING_TYPE * pFsipv6PmtuDest)
{
    tPmtuEntry         *pPmtuEntry = NULL;
    tPmtuEntry          PmtuEntry;

    if (gIp6GblInfo.pIp6CurrCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    MEMSET (&PmtuEntry, IP6_ZERO, sizeof (tPmtuEntry));
    PmtuEntry.pIp6Cxt = gIp6GblInfo.pIp6CurrCxt;

    pPmtuEntry = (tPmtuEntry *) RBTreeGetNext (gIp6GblInfo.PmtuRBTable,
                                               (tRBElem *) & PmtuEntry, NULL);

    if ((pPmtuEntry != NULL) &&
        (pPmtuEntry->pIp6Cxt == gIp6GblInfo.pIp6CurrCxt))
    {
        Ip6AddrCopy ((tIp6Addr *) (VOID *) pFsipv6PmtuDest->pu1_OctetList,
                     &pPmtuEntry->Ip6Addr);
        pFsipv6PmtuDest->i4_Length = sizeof (tIp6Addr);
        return SNMP_SUCCESS;

    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsipv6PmtuTable
 Input       :  The Indices
                Fsipv6PmtuDest
                nextFsipv6PmtuDest
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsipv6PmtuTable (tSNMP_OCTET_STRING_TYPE * pFsipv6PmtuDest,
                                tSNMP_OCTET_STRING_TYPE * pNextFsipv6PmtuDest)
{
    tPmtuEntry          PmtuEntry;
    tPmtuEntry         *pNextEntry;

    if (gIp6GblInfo.pIp6CurrCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    MEMSET (&PmtuEntry, IP6_ZERO, sizeof (tPmtuEntry));

    Ip6AddrCopy (&PmtuEntry.Ip6Addr,
                 (tIp6Addr *) (VOID *) pFsipv6PmtuDest->pu1_OctetList);
    PmtuEntry.pIp6Cxt = gIp6GblInfo.pIp6CurrCxt;

    pNextEntry =
        (tPmtuEntry *) RBTreeGetNext (gIp6GblInfo.PmtuRBTable,
                                      (tRBElem *) & PmtuEntry, NULL);
    if ((pNextEntry != NULL) &&
        (pNextEntry->pIp6Cxt == gIp6GblInfo.pIp6CurrCxt))
    {
        Ip6AddrCopy ((tIp6Addr *) (VOID *) pNextFsipv6PmtuDest->pu1_OctetList,
                     &pNextEntry->Ip6Addr);
        pNextFsipv6PmtuDest->i4_Length = sizeof (tIp6Addr);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsipv6Pmtu
 Input       :  The Indices
                Fsipv6PmtuDest

                The Object 
                retValFsipv6Pmtu
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6Pmtu (tSNMP_OCTET_STRING_TYPE * pFsipv6PmtuDest,
                  INT4 *pi4RetValFsipv6Pmtu)
{
    tPmtuEntry         *pPmtuEntry;

    if (gIp6GblInfo.pIp6CurrCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if (ip6PmtuLookupEntryInCxt
        (gIp6GblInfo.pIp6CurrCxt->u4ContextId,
         (tIp6Addr *) (VOID *) pFsipv6PmtuDest->pu1_OctetList,
         &pPmtuEntry) == IP6_SUCCESS)
    {
        *pi4RetValFsipv6Pmtu = pPmtuEntry->u4Pmtu;
        return SNMP_SUCCESS;
    }
    UNUSED_PARAM (pFsipv6PmtuDest);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsipv6PmtuTimeStamp
 Input       :  The Indices
                Fsipv6PmtuDest

                The Object 
                retValFsipv6PmtuTimeStamp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6PmtuTimeStamp (tSNMP_OCTET_STRING_TYPE * pFsipv6PmtuDest,
                           INT4 *pi4RetValFsipv6PmtuTimeStamp)
{
    tPmtuEntry         *pPmtuEntry;

    if (gIp6GblInfo.pIp6CurrCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if (ip6PmtuLookupEntryInCxt
        (gIp6GblInfo.pIp6CurrCxt->u4ContextId,
         (tIp6Addr *) (VOID *) pFsipv6PmtuDest->pu1_OctetList,
         &pPmtuEntry) == IP6_SUCCESS)
    {
        *pi4RetValFsipv6PmtuTimeStamp = pPmtuEntry->PmtuTimeStamp;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsipv6PmtuAdminStatus
 Input       :  The Indices
                Fsipv6PmtuDest

                The Object 
                retValFsipv6PmtuAdminStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6PmtuAdminStatus (tSNMP_OCTET_STRING_TYPE * pFsipv6PmtuDest,
                             INT4 *pi4RetValFsipv6PmtuAdminStatus)
{
    tPmtuEntry         *pPmtuEntry = NULL;

    if (gIp6GblInfo.pIp6CurrCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if (ip6PmtuLookupEntryInCxt
        (gIp6GblInfo.pIp6CurrCxt->u4ContextId,
         (tIp6Addr *) (VOID *) pFsipv6PmtuDest->pu1_OctetList,
         &pPmtuEntry) == IP6_SUCCESS)
    {
        *pi4RetValFsipv6PmtuAdminStatus = IP6_PMTU_ENTRY_VALID;
    }
    else
    {
        *pi4RetValFsipv6PmtuAdminStatus = IP6_PMTU_ENTRY_INVALID;
    }
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsipv6Pmtu
 Input       :  The Indices
                Fsipv6PmtuDest

                The Object 
                setValFsipv6Pmtu
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6Pmtu (tSNMP_OCTET_STRING_TYPE * pFsipv6PmtuDest,
                  INT4 i4SetValFsipv6Pmtu)
{
    tPmtuEntry         *pPmtuEntry;

    if (gIp6GblInfo.pIp6CurrCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if (ip6PmtuLookupEntryInCxt
        (gIp6GblInfo.pIp6CurrCxt->u4ContextId,
         (tIp6Addr *) (VOID *) pFsipv6PmtuDest->pu1_OctetList,
         &pPmtuEntry) == IP6_SUCCESS)
    {
        pPmtuEntry->u4Pmtu = i4SetValFsipv6Pmtu;
        IncMsrForIpv6PmtuTable (gIp6GblInfo.pIp6CurrCxt->u4ContextId,
                                pFsipv6PmtuDest, i4SetValFsipv6Pmtu,
                                FsMIIpv6Pmtu,
                                sizeof (FsMIIpv6Pmtu) / sizeof (UINT4));
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsipv6PmtuAdminStatus
 Input       :  The Indices
                Fsipv6PmtuDest

                The Object 
                setValFsipv6PmtuAdminStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6PmtuAdminStatus (tSNMP_OCTET_STRING_TYPE * pFsipv6PmtuDest,
                             INT4 i4SetValFsipv6PmtuAdminStatus)
{
    tPmtuEntry         *pPmtuEntry = NULL;

    if (gIp6GblInfo.pIp6CurrCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    ip6PmtuLookupEntryInCxt (gIp6GblInfo.pIp6CurrCxt->u4ContextId,
                             (tIp6Addr *) (VOID *) pFsipv6PmtuDest->
                             pu1_OctetList, &pPmtuEntry);

    /* AdminStatus valid */
    if (i4SetValFsipv6PmtuAdminStatus == IP6_PMTU_ENTRY_VALID)
    {
        if (pPmtuEntry == NULL)
        {                        /* Entry not present */
            pPmtuEntry =
                (tPmtuEntry *) (VOID *) Ip6GetMem (gIp6GblInfo.pIp6CurrCxt->
                                                   u4ContextId,
                                                   (UINT2) gIp6GblInfo.
                                                   MemPoolId);

            if (pPmtuEntry != NULL)
            {
                Ip6AddrCopy ((tIp6Addr *) & pPmtuEntry->Ip6Addr,
                             (tIp6Addr *) (VOID *) pFsipv6PmtuDest->
                             pu1_OctetList);

                /* For static pmtu entries timestamp should be
                 * infinity (0xFFFFFFFF) */
                pPmtuEntry->PmtuTimeStamp = IP6_MAX_SYS_TIME;

                pPmtuEntry->pIp6Cxt = gIp6GblInfo.pIp6CurrCxt;
                if (RBTreeAdd (gIp6GblInfo.PmtuRBTable,
                               pPmtuEntry) == RB_FAILURE)
                {
                    return SNMP_FAILURE;
                }
                IncMsrForIpv6PmtuTable (gIp6GblInfo.pIp6CurrCxt->u4ContextId,
                                        pFsipv6PmtuDest,
                                        i4SetValFsipv6PmtuAdminStatus,
                                        FsMIIpv6PmtuAdminStatus,
                                        sizeof (FsMIIpv6PmtuAdminStatus)
                                        / sizeof (UINT4));
                return SNMP_SUCCESS;
            }
        }
        else
        {
            return SNMP_SUCCESS;
        }
    }
    else
    {
        if (pPmtuEntry != NULL)
        {                        /* Entry present */
            RBTreeRem (gIp6GblInfo.PmtuRBTable, pPmtuEntry);
            ip6PmtuReleaseEntry ((tPmtuEntry *) pPmtuEntry);
            IncMsrForIpv6PmtuTable (gIp6GblInfo.pIp6CurrCxt->u4ContextId,
                                    pFsipv6PmtuDest,
                                    i4SetValFsipv6PmtuAdminStatus,
                                    FsMIIpv6PmtuAdminStatus,
                                    sizeof (FsMIIpv6PmtuAdminStatus)
                                    / sizeof (UINT4));
            return SNMP_SUCCESS;
        }
        else
        {
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Fsipv6Pmtu
 Input       :  The Indices
                Fsipv6PmtuDest

                The Object 
                testValFsipv6Pmtu
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6Pmtu (UINT4 *pu4ErrorCode,
                     tSNMP_OCTET_STRING_TYPE * pFsipv6PmtuDest,
                     INT4 i4TestValFsipv6Pmtu)
{
    tIp6Addr            TempFsipv6Address;

    if (pFsipv6PmtuDest == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsipv6Pmtu < IP6_MIN_MTU)
        || (i4TestValFsipv6Pmtu > MAX_PAYLOAD_SIZE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    MEMCPY (&TempFsipv6Address, pFsipv6PmtuDest->pu1_OctetList,
            sizeof (tIp6Addr));

    if (IS_ADDR_MULTI (TempFsipv6Address)
        || IS_ADDR_UNSPECIFIED (TempFsipv6Address))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6PmtuAdminStatus
 Input       :  The Indices
                Fsipv6PmtuDest

                The Object 
                testValFsipv6PmtuAdminStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6PmtuAdminStatus (UINT4 *pu4ErrorCode,
                                tSNMP_OCTET_STRING_TYPE * pFsipv6PmtuDest,
                                INT4 i4TestValFsipv6PmtuAdminStatus)
{
    tIp6Addr            TempFsipv6Address;

    *pu4ErrorCode = SNMP_ERR_NO_CREATION;

    if (pFsipv6PmtuDest == NULL)
    {
        return SNMP_FAILURE;
    }

    MEMCPY (&TempFsipv6Address, pFsipv6PmtuDest->pu1_OctetList,
            sizeof (tIp6Addr));

    if (IS_ADDR_MULTI (TempFsipv6Address)
        || IS_ADDR_UNSPECIFIED (TempFsipv6Address))
    {
        return SNMP_FAILURE;
    }

    if ((i4TestValFsipv6PmtuAdminStatus == IP6_PMTU_ENTRY_VALID) ||
        (i4TestValFsipv6PmtuAdminStatus == IP6_PMTU_ENTRY_INVALID))
    {
        return SNMP_SUCCESS;
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Fsipv6PmtuTable
 Input       :  The Indices
                Fsipv6PmtuDest
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsipv6PmtuTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Fsipv6NdLanCacheTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsipv6NdLanCacheTable
 Input       :  The Indices
                Fsipv6NdLanCacheIfIndex
                Fsipv6NdLanCacheIPv6Addr
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsipv6NdLanCacheTable (INT4 i4Fsipv6NdLanCacheIfIndex,
                                               tSNMP_OCTET_STRING_TYPE *
                                               pFsipv6NdLanCacheIPv6Addr)
{
    INT4                i4AddrType;
    tIp6If             *pIf6 = NULL;

    if (Ip6ifEntryExists ((UINT4) i4Fsipv6NdLanCacheIfIndex) == IP6_FAILURE)
    {
        return (SNMP_FAILURE);
    }

    if (pFsipv6NdLanCacheIPv6Addr == NULL)
    {
        return (SNMP_FAILURE);
    }

    /* Address can be only link-local/unicast/anycast */

    i4AddrType =
        Ip6AddrType ((tIp6Addr *) (VOID *) pFsipv6NdLanCacheIPv6Addr->
                     pu1_OctetList);

    if (i4AddrType == ADDR6_MULTI || i4AddrType == ADDR6_UNSPECIFIED)
    {
        return (SNMP_FAILURE);
    }

    pIf6 = gIp6GblInfo.apIp6If[i4Fsipv6NdLanCacheIfIndex];

    if (Nd6IsCacheForAddr (pIf6,
                           (tIp6Addr *) (VOID *)
                           pFsipv6NdLanCacheIPv6Addr->pu1_OctetList) == NULL)
    {
        return SNMP_FAILURE;
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsipv6NdLanCacheTable
 Input       :  The Indices
                Fsipv6NdLanCacheIfIndex
                Fsipv6NdLanCacheIPv6Addr
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsipv6NdLanCacheTable (INT4 *pi4Fsipv6NdLanCacheIfIndex,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFsipv6NdLanCacheIPv6Addr)
{
    tIp6Addr            TempAddr1, TempAddr2;

    MEMSET (&TempAddr1, 0, IP6_ADDR_SIZE);
    MEMSET (&TempAddr2, 0, IP6_ADDR_SIZE);

    if (Ip6GetNextNdLanCacheTableEntry (0, pi4Fsipv6NdLanCacheIfIndex,
                                        TempAddr1, &TempAddr2) == SNMP_SUCCESS)
    {
        Ip6AddrCopy ((tIp6Addr *) (VOID *)
                     pFsipv6NdLanCacheIPv6Addr->pu1_OctetList, &TempAddr2);
        pFsipv6NdLanCacheIPv6Addr->i4_Length = IP6_ADDR_SIZE;
        return SNMP_SUCCESS;
    }
    else
        return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsipv6NdLanCacheTable
 Input       :  The Indices
                Fsipv6NdLanCacheIfIndex
                nextFsipv6NdLanCacheIfIndex
                Fsipv6NdLanCacheIPv6Addr
                nextFsipv6NdLanCacheIPv6Addr
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsipv6NdLanCacheTable (INT4 i4Fsipv6NdLanCacheIfIndex,
                                      INT4 *pi4NextFsipv6NdLanCacheIfIndex,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pFsipv6NdLanCacheIPv6Addr,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pNextFsipv6NdLanCacheIPv6Addr)
{
    tIp6Addr            TempAddr;
    tIp6Addr            TempNextAddr;

    if (i4Fsipv6NdLanCacheIfIndex > IP6_MAX_LOGICAL_IF_INDEX)
    {
        return (SNMP_FAILURE);
    }

    MEMSET (&TempAddr, 0, IP6_ADDR_SIZE);
    MEMSET (&TempNextAddr, 0, IP6_ADDR_SIZE);

    Ip6AddrCopy (&TempAddr,
                 (tIp6Addr *) (VOID *) pFsipv6NdLanCacheIPv6Addr->
                 pu1_OctetList);

    if (Ip6GetNextNdLanCacheTableEntry (i4Fsipv6NdLanCacheIfIndex,
                                        pi4NextFsipv6NdLanCacheIfIndex,
                                        TempAddr,
                                        &TempNextAddr) == SNMP_SUCCESS)
    {
        Ip6AddrCopy ((tIp6Addr *) (VOID *)
                     pNextFsipv6NdLanCacheIPv6Addr->pu1_OctetList,
                     &TempNextAddr);

        pNextFsipv6NdLanCacheIPv6Addr->i4_Length = IP6_ADDR_SIZE;
        return SNMP_SUCCESS;
    }

    MEMSET (pNextFsipv6NdLanCacheIPv6Addr->pu1_OctetList, IP6_ZERO,
            IP6_ADDR_SIZE);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsipv6NdLanCachePhysAddr
 Input       :  The Indices
                Fsipv6NdLanCacheIfIndex
                Fsipv6NdLanCacheIPv6Addr

                The Object 
                retValFsipv6NdLanCachePhysAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6NdLanCachePhysAddr (INT4 i4Fsipv6NdLanCacheIfIndex,
                                tSNMP_OCTET_STRING_TYPE *
                                pFsipv6NdLanCacheIPv6Addr,
                                tSNMP_OCTET_STRING_TYPE *
                                pRetValFsipv6NdLanCachePhysAddr)
{
    tIp6If             *pIf6;
    tNd6CacheEntry     *pNd6cEntry;
    tNd6CacheLanInfo   *pNd6cLinfo;
    UINT1               u1LlaLen;

    pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6NdLanCacheIfIndex);
    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }
    pNd6cEntry = Nd6IsCacheForAddr (pIf6,
                                    (tIp6Addr *) (VOID *)
                                    pFsipv6NdLanCacheIPv6Addr->pu1_OctetList);
    if (pNd6cEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pNd6cLinfo = (tNd6CacheLanInfo *) (VOID *) pNd6cEntry->pNd6cInfo;

    if (pNd6cLinfo == NULL)
    {
        return SNMP_FAILURE;
    }
    u1LlaLen = MEM_MAX_BYTES (pNd6cLinfo->u1LlaLen, IP6_MAX_LLA_LEN);
    MEMCPY (pRetValFsipv6NdLanCachePhysAddr->pu1_OctetList, pNd6cLinfo->lladdr,
            u1LlaLen);

    pRetValFsipv6NdLanCachePhysAddr->i4_Length = u1LlaLen;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsipv6NdLanCacheStatus
 Input       :  The Indices
                Fsipv6NdLanCacheIfIndex
                Fsipv6NdLanCacheIPv6Addr

                The Object 
                retValFsipv6NdLanCacheStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6NdLanCacheStatus (INT4 i4Fsipv6NdLanCacheIfIndex,
                              tSNMP_OCTET_STRING_TYPE *
                              pFsipv6NdLanCacheIPv6Addr,
                              INT4 *pi4RetValFsipv6NdLanCacheStatus)
{
    tIp6If             *pIf6;
    tNd6CacheEntry     *pNd6cEntry;
    pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6NdLanCacheIfIndex);
    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }
    pNd6cEntry = Nd6IsCacheForAddr (pIf6,
                                    (tIp6Addr *) (VOID *)
                                    pFsipv6NdLanCacheIPv6Addr->pu1_OctetList);
    if (pNd6cEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsipv6NdLanCacheStatus = ND6_LAN_CACHE_ENTRY_VALID;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6NdLanCacheState
 Input       :  The Indices
                Fsipv6NdLanCacheIfIndex
                Fsipv6NdLanCacheIPv6Addr

                The Object 
                retValFsipv6NdLanCacheState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6NdLanCacheState (INT4 i4Fsipv6NdLanCacheIfIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pFsipv6NdLanCacheIPv6Addr,
                             INT4 *pi4RetValFsipv6NdLanCacheState)
{
    tIp6If             *pIf6;
    tNd6CacheEntry     *pNd6cEntry;

    pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6NdLanCacheIfIndex);
    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }
    pNd6cEntry = Nd6IsCacheForAddr (pIf6,
                                    (tIp6Addr *) (VOID *)
                                    pFsipv6NdLanCacheIPv6Addr->pu1_OctetList);
    if (pNd6cEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsipv6NdLanCacheState = (INT4) pNd6cEntry->u1ReachState;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6NdLanCacheUseTime
 Input       :  The Indices
                Fsipv6NdLanCacheIfIndex
                Fsipv6NdLanCacheIPv6Addr

                The Object 
                retValFsipv6NdLanCacheUseTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6NdLanCacheUseTime (INT4 i4Fsipv6NdLanCacheIfIndex,
                               tSNMP_OCTET_STRING_TYPE *
                               pFsipv6NdLanCacheIPv6Addr,
                               UINT4 *pu4RetValFsipv6NdLanCacheUseTime)
{
    tIp6If             *pIf6;
    tNd6CacheEntry     *pNd6cEntry;

    pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6NdLanCacheIfIndex);
    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }
    pNd6cEntry = Nd6IsCacheForAddr (pIf6,
                                    (tIp6Addr *) (VOID *)
                                    pFsipv6NdLanCacheIPv6Addr->pu1_OctetList);
    if (pNd6cEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsipv6NdLanCacheUseTime = (INT4) pNd6cEntry->u4LastUseTime;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6NdLanCacheIsSecure
 Input       :  The Indices
                Fsipv6NdLanCacheIfIndex
                Fsipv6NdLanCacheIPv6Addr

                The Object 
                retValFsipv6NdLanCacheIsSecure
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6NdLanCacheIsSecure (INT4 i4Fsipv6NdLanCacheIfIndex,
                                tSNMP_OCTET_STRING_TYPE *
                                pFsipv6NdLanCacheIPv6Addr,
                                INT4 *pi4RetValFsipv6NdLanCacheIsSecure)
{
    tIp6If             *pIf6;
    tNd6CacheEntry     *pNd6cEntry;

    pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6NdLanCacheIfIndex);
    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }
    pNd6cEntry = Nd6IsCacheForAddr (pIf6,
                                    (tIp6Addr *) (VOID *)
                                    pFsipv6NdLanCacheIPv6Addr->pu1_OctetList);
    if (pNd6cEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsipv6NdLanCacheIsSecure = (INT4) pNd6cEntry->u1SecureFlag;
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsipv6NdLanCachePhysAddr
 Input       :  The Indices
                Fsipv6NdLanCacheIfIndex
                Fsipv6NdLanCacheIPv6Addr

                The Object 
                setValFsipv6NdLanCachePhysAddr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6NdLanCachePhysAddr (INT4 i4Fsipv6NdLanCacheIfIndex,
                                tSNMP_OCTET_STRING_TYPE *
                                pFsipv6NdLanCacheIPv6Addr,
                                tSNMP_OCTET_STRING_TYPE *
                                pSetValFsipv6NdLanCachePhysAddr)
{
    tIp6If             *pIf6;
    tNd6CacheEntry     *pNd6cEntry;
    tNd6CacheLanInfo   *pNd6cLinfo;
    INT4                i4RSValue = 0;
    UINT1               u1LlaLen = IP6_MAX_LLA_LEN;

    pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6NdLanCacheIfIndex);
    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }
    pNd6cEntry = Nd6IsCacheForAddr (pIf6,
                                    (tIp6Addr *) (VOID *)
                                    pFsipv6NdLanCacheIPv6Addr->pu1_OctetList);
    if (pNd6cEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pNd6cLinfo = (tNd6CacheLanInfo *) (VOID *) pNd6cEntry->pNd6cInfo;

    /* pLladdr not declared and initialized */

    if (pNd6cLinfo == NULL)
    {
        return SNMP_FAILURE;
    }
    if (pSetValFsipv6NdLanCachePhysAddr == NULL)
    {
        MEMSET (pNd6cLinfo->lladdr, 0, u1LlaLen);
        pNd6cLinfo->u1LlaLen = u1LlaLen;    /* MUST be ZERO */
        return SNMP_FAILURE;
    }

    MEMCPY (pNd6cLinfo->lladdr, pSetValFsipv6NdLanCachePhysAddr->pu1_OctetList,
            u1LlaLen);

    pNd6cLinfo->u1LlaLen = u1LlaLen;

    /* 
     * The Reachability State MUST be set to the value as STATIC 
     * upon any of the configurable object of a dynamic entry being
     * modified from SNMP
     */
    Nd6SetReachState (pNd6cEntry, ND6C_STATIC);
    /* update the RowStatus */
    pNd6cEntry->u1RowStatus = ACTIVE;

    /* In case of STATIC ND entry, the ND timer should not be running. */
    Nd6CancelCacheTimer (pNd6cEntry);

    IncMsrForIpv6NetToPhyTable (i4Fsipv6NdLanCacheIfIndex,
                                pFsipv6NdLanCacheIPv6Addr,
                                's', pSetValFsipv6NdLanCachePhysAddr,
                                FsMIStdIpNetToPhysicalPhysAddress,
                                (sizeof (FsMIStdIpNetToPhysicalPhysAddress) /
                                 sizeof (UINT4)), FALSE);

    /* For the Ipv6LanCacheTable, there is no separate rowstatus object.
     * When the physical address is configured for NetToPhysicalTable,
     * the rowstatus for the entry is set to STATIC_NOT_IN_SERVICE.
     * To make the entry rowstatus to active a separate notification
     * is sent for the object FsMIStdIpNetToPhysicalRowStatus */

    i4RSValue = ACTIVE;
    IncMsrForIpv6NetToPhyTable (i4Fsipv6NdLanCacheIfIndex,
                                pFsipv6NdLanCacheIPv6Addr,
                                'i', &i4RSValue,
                                FsMIStdIpNetToPhysicalRowStatus,
                                (sizeof (FsMIStdIpNetToPhysicalRowStatus) /
                                 sizeof (UINT4)), TRUE);

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsipv6NdLanCacheStatus
 Input       :  The Indices
                Fsipv6NdLanCacheIfIndex
                Fsipv6NdLanCacheIPv6Addr

                The Object 
                setValFsipv6NdLanCacheStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6NdLanCacheStatus (INT4 i4Fsipv6NdLanCacheIfIndex,
                              tSNMP_OCTET_STRING_TYPE *
                              pFsipv6NdLanCacheIPv6Addr,
                              INT4 i4SetValFsipv6NdLanCacheStatus)
{
    tIp6If             *pIf6;
    tNd6CacheEntry     *pNd6cEntry;

    pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6NdLanCacheIfIndex);
    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }
    pNd6cEntry =
        Nd6IsCacheForAddr (pIf6,
                           (tIp6Addr *) (VOID *) pFsipv6NdLanCacheIPv6Addr->
                           pu1_OctetList);
    switch (i4SetValFsipv6NdLanCacheStatus)
    {
        case ND6_LAN_CACHE_ENTRY_VALID:
            if (pNd6cEntry == NULL)
            {
                pNd6cEntry =
                    Nd6CreateCache (pIf6,
                                    (tIp6Addr *) (VOID *)
                                    pFsipv6NdLanCacheIPv6Addr->pu1_OctetList,
                                    NULL, IP6_ZERO, ND6C_STATIC_NOT_IN_SERVICE,
                                    IP6_ZERO);
                if (pNd6cEntry == NULL)
                {
                    return (SNMP_FAILURE);
                }
            }
            break;

        case ND6_LAN_CACHE_ENTRY_INVALID:
            if (pNd6cEntry != NULL)
            {
                if (Nd6PurgeCache (pNd6cEntry) != IP6_SUCCESS)
                    return (SNMP_FAILURE);
            }
            break;

        default:
            return (SNMP_FAILURE);
    }

    IncMsrForIpv6NetToPhyTable (i4Fsipv6NdLanCacheIfIndex,
                                pFsipv6NdLanCacheIPv6Addr,
                                'i', &i4SetValFsipv6NdLanCacheStatus,
                                FsMIStdIpNetToPhysicalType,
                                (sizeof (FsMIStdIpNetToPhysicalType) /
                                 sizeof (UINT4)), FALSE);

    return (SNMP_SUCCESS);
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Fsipv6NdLanCachePhysAddr
 Input       :  The Indices
                Fsipv6NdLanCacheIfIndex
                Fsipv6NdLanCacheIPv6Addr

                The Object 
                testValFsipv6NdLanCachePhysAddr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6NdLanCachePhysAddr (UINT4 *pu4ErrorCode,
                                   INT4 i4Fsipv6NdLanCacheIfIndex,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsipv6NdLanCacheIPv6Addr,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pTestValFsipv6NdLanCachePhysAddr)
{
    tIp6Addr            TempFsipv6Address;
    UINT1               au1MacAllZero[] = { IP6_ZERO, IP6_ZERO, IP6_ZERO,
        IP6_ZERO, IP6_ZERO, IP6_ZERO
    };

    *pu4ErrorCode = SNMP_ERR_NO_CREATION;

    if (Ip6ifEntryExists ((UINT4) i4Fsipv6NdLanCacheIfIndex) == IP6_FAILURE)
    {
        CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
        return SNMP_FAILURE;
    }

    if (pFsipv6NdLanCacheIPv6Addr == NULL)
    {
        CLI_SET_ERR (CLI_IP6_INVALID_ADDRESS);
        return SNMP_FAILURE;
    }

    MEMCPY (&TempFsipv6Address, pFsipv6NdLanCacheIPv6Addr->pu1_OctetList,
            sizeof (tIp6Addr));

    if (IS_ADDR_MULTI (TempFsipv6Address)
        || IS_ADDR_UNSPECIFIED (TempFsipv6Address))
    {
        CLI_SET_ERR (CLI_IP6_INVALID_ADDR_TYPE);
        return SNMP_FAILURE;
    }

    if (pTestValFsipv6NdLanCachePhysAddr->i4_Length != CFA_ENET_ADDR_LEN)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        CLI_SET_ERR (CLI_IP6_INVALID_PHYS_ADDR);
        return SNMP_FAILURE;
    }

    /* Check the the given Mac is all Zero. */
    if ((MEMCMP (&(au1MacAllZero),
                 pTestValFsipv6NdLanCachePhysAddr->pu1_OctetList,
                 pTestValFsipv6NdLanCachePhysAddr->i4_Length)) == IP6_ZERO)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_INVALID_PHYS_ADDR);
        return (SNMP_FAILURE);
    }

    /* Check if the given mac is a multicast mac */
    if (FS_UTIL_IS_MCAST_MAC (pTestValFsipv6NdLanCachePhysAddr->pu1_OctetList)
        == OSIX_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_INVALID_PHYS_ADDR);
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6NdLanCacheStatus
 Input       :  The Indices
                Fsipv6NdLanCacheIfIndex
                Fsipv6NdLanCacheIPv6Addr

                The Object 
                testValFsipv6NdLanCacheStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6NdLanCacheStatus (UINT4 *pu4ErrorCode,
                                 INT4 i4Fsipv6NdLanCacheIfIndex,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pFsipv6NdLanCacheIPv6Addr,
                                 INT4 i4TestValFsipv6NdLanCacheStatus)
{
    tIp6Addr            TempFsipv6Address;
    tIp6If             *pIf6 = NULL;
    UINT4               u4Index;

    *pu4ErrorCode = SNMP_ERR_NO_CREATION;

    if (Ip6ifEntryExists ((UINT4) i4Fsipv6NdLanCacheIfIndex) == IP6_FAILURE)
    {
        CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
        return SNMP_FAILURE;
    }

    if (pFsipv6NdLanCacheIPv6Addr == NULL)
    {
        CLI_SET_ERR (CLI_IP6_INVALID_ADDRESS);
        return SNMP_FAILURE;
    }

    MEMCPY (&TempFsipv6Address, pFsipv6NdLanCacheIPv6Addr->pu1_OctetList,
            sizeof (tIp6Addr));

    if (IS_ADDR_MULTI (TempFsipv6Address) ||
        IS_ADDR_UNSPECIFIED (TempFsipv6Address))
    {
        CLI_SET_ERR (CLI_IP6_INVALID_ADDR_TYPE);
        return SNMP_FAILURE;
    }

    pIf6 = gIp6GblInfo.apIp6If[i4Fsipv6NdLanCacheIfIndex];
    /* Verify that the address does not belong to any of the local interface */
    if (Ip6IsOurAddrInCxt (pIf6->pIp6Cxt->u4ContextId,
                           &TempFsipv6Address, &u4Index) == IP6_SUCCESS)
    {
        /* Given Address belongs to this interface. We shall not
         * add an entry in Neighbor cache table for any address
         * belonging to local interface. */
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_ND_OUR_ADDR);
        return SNMP_FAILURE;
    }

    if (i4TestValFsipv6NdLanCacheStatus != ND6_LAN_CACHE_ENTRY_VALID &&
        i4TestValFsipv6NdLanCacheStatus != ND6_LAN_CACHE_ENTRY_INVALID)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_ND_LAN_CACHE_STATUS);
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Fsipv6NdLanCacheTable
 Input       :  The Indices
                Fsipv6NdLanCacheIfIndex
                Fsipv6NdLanCacheIPv6Addr
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsipv6NdLanCacheTable (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Fsipv6NdWanCacheTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsipv6NdWanCacheTable
 Input       :  The Indices
                Fsipv6NdWanCacheIfIndex
                Fsipv6NdWanCacheIPv6Addr
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsipv6NdWanCacheTable (INT4 i4Fsipv6NdWanCacheIfIndex,
                                               tSNMP_OCTET_STRING_TYPE *
                                               pFsipv6NdWanCacheIPv6Addr)
{
    tIp6Addr            TempFsipv6Address;
    tIp6If             *pIf6 = NULL;

    if (Ip6ifEntryExists ((UINT4) i4Fsipv6NdWanCacheIfIndex) == IP6_FAILURE)
    {
        return (SNMP_FAILURE);
    }

    pIf6 = gIp6GblInfo.apIp6If[i4Fsipv6NdWanCacheIfIndex];
    if ((pIf6->u1IfType != IP6_X25_INTERFACE_TYPE) &&
        (pIf6->u1IfType != IP6_FR_INTERFACE_TYPE))
    {
        return SNMP_FAILURE;
    }

    if (pFsipv6NdWanCacheIPv6Addr == NULL)
    {
        return (SNMP_FAILURE);
    }

    MEMCPY (&TempFsipv6Address, pFsipv6NdWanCacheIPv6Addr->pu1_OctetList,
            sizeof (tIp6Addr));

    if (IS_ADDR_MULTI (TempFsipv6Address)
        || IS_ADDR_UNSPECIFIED (TempFsipv6Address))
    {
        return SNMP_FAILURE;
    }

    if (Nd6IsCacheForAddr (pIf6, &TempFsipv6Address) == NULL)
    {
        return SNMP_FAILURE;
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsipv6NdWanCacheTable
 Input       :  The Indices
                Fsipv6NdWanCacheIfIndex
                Fsipv6NdWanCacheIPv6Addr
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsipv6NdWanCacheTable (INT4 *pi4Fsipv6NdWanCacheIfIndex,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFsipv6NdWanCacheIPv6Addr)
{
    tIp6If             *pIf6;
    tNd6CacheEntry     *pNd6cEntry = NULL;
    tNd6CacheEntry     *pNd6cNextEntry = NULL;

    pNd6cEntry = RBTreeGetFirst (gNd6GblInfo.Nd6CacheTable);

    while (pNd6cEntry != NULL)
    {
        pNd6cNextEntry = RBTreeGetNext (gNd6GblInfo.Nd6CacheTable,
                                        pNd6cEntry, NULL);

        pIf6 = pNd6cEntry->pIf6;
        if (pIf6->u1IfType == IP6_X25_INTERFACE_TYPE ||
            pIf6->u1IfType == IP6_FR_INTERFACE_TYPE)
        {
            *pi4Fsipv6NdWanCacheIfIndex = pIf6->u4Index;
            Ip6AddrCopy ((tIp6Addr *) pFsipv6NdWanCacheIPv6Addr,
                         &pNd6cEntry->addr6);
            return (SNMP_SUCCESS);
        }
        pNd6cEntry = pNd6cNextEntry;
    }

    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsipv6NdWanCacheTable
 Input       :  The Indices
                Fsipv6NdWanCacheIfIndex
                nextFsipv6NdWanCacheIfIndex
                Fsipv6NdWanCacheIPv6Addr
                nextFsipv6NdWanCacheIPv6Addr
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsipv6NdWanCacheTable (INT4 i4Fsipv6NdWanCacheIfIndex,
                                      INT4 *pi4NextFsipv6NdWanCacheIfIndex,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pFsipv6NdWanCacheIPv6Addr,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pNextFsipv6NdWanCacheIPv6Addr)
{
    UINT4               u4TempIndex = 0;
    tIp6If             *pIf6 = NULL;
    tNd6CacheEntry      Nd6cEntry;
    tNd6CacheEntry     *pNd6cEntry = NULL;

    MEMSET (&Nd6cEntry, 0, sizeof (tNd6CacheEntry));

    if (i4Fsipv6NdWanCacheIfIndex > IP6_MAX_LOGICAL_IF_INDEX)
    {
        return (SNMP_FAILURE);
    }

    pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6NdWanCacheIfIndex);
    if (pIf6 == NULL)
    {
        u4TempIndex = i4Fsipv6NdWanCacheIfIndex + 1;

        IP6_IF_SCAN (u4TempIndex, IP6_MAX_LOGICAL_IF_INDEX)
        {
            if (gIp6GblInfo.apIp6If[u4TempIndex] != NULL)
            {
                pIf6 = gIp6GblInfo.apIp6If[u4TempIndex];
                break;
            }
        }
        if (u4TempIndex > IP6_MAX_LOGICAL_IF_INDEX)
        {
            return SNMP_FAILURE;
        }
        MEMSET (&Nd6cEntry.addr6, 0, sizeof (tIp6Addr));
    }
    else
    {
        Ip6AddrCopy (&Nd6cEntry.addr6, (tIp6Addr *) pFsipv6NdWanCacheIPv6Addr);
    }

    Nd6cEntry.pIf6 = pIf6;

    pNd6cEntry = RBTreeGetNext (gNd6GblInfo.Nd6CacheTable,
                                (tRBElem *) & Nd6cEntry, NULL);

    while (pNd6cEntry != NULL)
    {
        if ((pNd6cEntry->pIf6->u1IfType == IP6_X25_INTERFACE_TYPE) ||
            (pNd6cEntry->pIf6->u1IfType == IP6_FR_INTERFACE_TYPE))
        {
            *pi4NextFsipv6NdWanCacheIfIndex = pNd6cEntry->pIf6->u4Index;
            Ip6AddrCopy ((tIp6Addr *) pNextFsipv6NdWanCacheIPv6Addr,
                         &pNd6cEntry->addr6);
            return (SNMP_SUCCESS);
        }

        pNd6cEntry = RBTreeGetNext (gNd6GblInfo.Nd6CacheTable,
                                    pNd6cEntry, NULL);
    }
    return (SNMP_FAILURE);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsipv6NdWanCacheStatus
 Input       :  The Indices
                Fsipv6NdWanCacheIfIndex
                Fsipv6NdWanCacheIPv6Addr

                The Object 
                retValFsipv6NdWanCacheStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6NdWanCacheStatus (INT4 i4Fsipv6NdWanCacheIfIndex,
                              tSNMP_OCTET_STRING_TYPE *
                              pFsipv6NdWanCacheIPv6Addr,
                              INT4 *pi4RetValFsipv6NdWanCacheStatus)
{
    tIp6If             *pIf6;

    pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6NdWanCacheIfIndex);
    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }
    if (Nd6IsCacheForAddr (pIf6,
                           (tIp6Addr *) (VOID *) pFsipv6NdWanCacheIPv6Addr->
                           pu1_OctetList) != NULL)
    {
        *pi4RetValFsipv6NdWanCacheStatus = ND6_WAN_CACHE_ENTRY_VALID;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsipv6NdWanCacheState
 Input       :  The Indices
                Fsipv6NdWanCacheIfIndex
                Fsipv6NdWanCacheIPv6Addr

                The Object 
                retValFsipv6NdWanCacheState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6NdWanCacheState (INT4 i4Fsipv6NdWanCacheIfIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pFsipv6NdWanCacheIPv6Addr,
                             INT4 *pi4RetValFsipv6NdWanCacheState)
{
    tIp6If             *pIf6;
    tNd6CacheEntry     *pNd6cEntry;

    pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6NdWanCacheIfIndex);
    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }
    pNd6cEntry =
        Nd6IsCacheForAddr (pIf6,
                           (tIp6Addr *) (VOID *) pFsipv6NdWanCacheIPv6Addr->
                           pu1_OctetList);

    if (pNd6cEntry == NULL)
        return SNMP_FAILURE;
    *pi4RetValFsipv6NdWanCacheState = (INT4) pNd6cEntry->u1ReachState;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6NdWanCacheUseTime
 Input       :  The Indices
                Fsipv6NdWanCacheIfIndex
                Fsipv6NdWanCacheIPv6Addr

                The Object 
                retValFsipv6NdWanCacheUseTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6NdWanCacheUseTime (INT4 i4Fsipv6NdWanCacheIfIndex,
                               tSNMP_OCTET_STRING_TYPE *
                               pFsipv6NdWanCacheIPv6Addr,
                               UINT4 *pu4RetValFsipv6NdWanCacheUseTime)
{
    tIp6If             *pIf6;
    tNd6CacheEntry     *pNd6cEntry;

    pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6NdWanCacheIfIndex);
    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }
    pNd6cEntry =
        Nd6IsCacheForAddr (pIf6,
                           (tIp6Addr *) (VOID *) pFsipv6NdWanCacheIPv6Addr->
                           pu1_OctetList);

    if (pNd6cEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsipv6NdWanCacheUseTime = (INT4) pNd6cEntry->u4LastUseTime;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsipv6NdWanCacheStatus
 Input       :  The Indices
                Fsipv6NdWanCacheIfIndex
                Fsipv6NdWanCacheIPv6Addr

                The Object 
                setValFsipv6NdWanCacheStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6NdWanCacheStatus (INT4 i4Fsipv6NdWanCacheIfIndex,
                              tSNMP_OCTET_STRING_TYPE *
                              pFsipv6NdWanCacheIPv6Addr,
                              INT4 i4SetValFsipv6NdWanCacheStatus)
{
    tIp6If             *pIf6;
    tNd6CacheEntry     *pNd6cEntry;

    pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6NdWanCacheIfIndex);
    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }
    pNd6cEntry =
        Nd6IsCacheForAddr (pIf6,
                           (tIp6Addr *) (VOID *) pFsipv6NdWanCacheIPv6Addr->
                           pu1_OctetList);

    /* Delete the entry if the status is Invalid */
    if (i4SetValFsipv6NdWanCacheStatus == ND6_WAN_CACHE_ENTRY_INVALID)
    {
        if (Nd6PurgeCache (pNd6cEntry) != IP6_SUCCESS)
        {
            return (SNMP_FAILURE);
        }
    }

    return (SNMP_SUCCESS);
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Fsipv6NdWanCacheStatus
 Input       :  The Indices
                Fsipv6NdWanCacheIfIndex
                Fsipv6NdWanCacheIPv6Addr

                The Object 
                testValFsipv6NdWanCacheStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6NdWanCacheStatus (UINT4 *pu4ErrorCode,
                                 INT4 i4Fsipv6NdWanCacheIfIndex,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pFsipv6NdWanCacheIPv6Addr,
                                 INT4 i4TestValFsipv6NdWanCacheStatus)
{
    tIp6Addr            TempFsipv6Address;
    tIp6If             *pIf6 = NULL;

    *pu4ErrorCode = SNMP_ERR_NO_CREATION;

    if (Ip6ifEntryExists ((UINT4) i4Fsipv6NdWanCacheIfIndex) == IP6_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (pFsipv6NdWanCacheIPv6Addr == NULL)
    {
        return SNMP_FAILURE;
    }

    MEMCPY (&TempFsipv6Address, pFsipv6NdWanCacheIPv6Addr->pu1_OctetList,
            sizeof (tIp6Addr));

    if (IS_ADDR_MULTI (TempFsipv6Address)
        || IS_ADDR_UNSPECIFIED (TempFsipv6Address))
    {
        return SNMP_FAILURE;
    }

    if (i4TestValFsipv6NdWanCacheStatus != ND6_WAN_CACHE_ENTRY_VALID &&
        i4TestValFsipv6NdWanCacheStatus != ND6_WAN_CACHE_ENTRY_INVALID)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6NdWanCacheIfIndex);
    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    if ((pIf6->u1IfType != IP6_X25_INTERFACE_TYPE) &&
        (pIf6->u1IfType != IP6_FR_INTERFACE_TYPE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Fsipv6NdWanCacheTable
 Input       :  The Indices
                Fsipv6NdWanCacheIfIndex
                Fsipv6NdWanCacheIPv6Addr
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsipv6NdWanCacheTable (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Fsipv6PingTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsipv6PingTable
 Input       :  The Indices
                Fsipv6PingIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsipv6PingTable (INT4 i4Fsipv6PingIndex)
{
    if (i4Fsipv6PingIndex < (INT4) gu4MaxPing6Dst)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsipv6PingTable
 Input       :  The Indices
                Fsipv6PingIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsipv6PingTable (INT4 *pi4Fsipv6PingIndex)
{
    *pi4Fsipv6PingIndex = 0;

    if (pPing[*pi4Fsipv6PingIndex]->u1Admin != IP6_PING_INVALID)
    {
        return SNMP_SUCCESS;
    }

    return (nmhGetNextIndexFsipv6PingTable
            (*pi4Fsipv6PingIndex, pi4Fsipv6PingIndex));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsipv6PingTable
 Input       :  The Indices
                Fsipv6PingIndex
                nextFsipv6PingIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsipv6PingTable (INT4 i4Fsipv6PingIndex,
                                INT4 *pi4NextFsipv6PingIndex)
{
    INT4                i4I = i4Fsipv6PingIndex + 1;

    if ((i4Fsipv6PingIndex < 0) || (i4I < 0))
    {
        return SNMP_FAILURE;
    }

    for (; i4I < (INT4) gu4MaxPing6Dst; i4I++)
    {
        if (pPing[i4I]->u1Admin != IP6_PING_INVALID)
        {
            *pi4NextFsipv6PingIndex = i4I;
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsipv6PingDest
 Input       :  The Indices
                Fsipv6PingIndex

                The Object 
                retValFsipv6PingDest
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6PingDest (INT4 i4Fsipv6PingIndex,
                      tSNMP_OCTET_STRING_TYPE * pRetValFsipv6PingDest)
{
    if (pPing[i4Fsipv6PingIndex]->u1Admin != IP6_PING_INVALID)
    {
        Ip6AddrCopy ((tIp6Addr *) (VOID *) pRetValFsipv6PingDest->pu1_OctetList,
                     &pPing[i4Fsipv6PingIndex]->ping6Dst);
        pRetValFsipv6PingDest->i4_Length = 16;
        return (SNMP_SUCCESS);
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsipv6PingIfIndex
 Input       :  The Indices
                Fsipv6PingIndex

                The Object 
                retValFsipv6PingIfIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6PingIfIndex (INT4 i4Fsipv6PingIndex,
                         INT4 *pi4RetValFsipv6PingIfIndex)
{
    if (pPing[i4Fsipv6PingIndex]->u1Admin != IP6_PING_INVALID)
    {
        *pi4RetValFsipv6PingIfIndex =
            (INT4) pPing[i4Fsipv6PingIndex]->u2IfIndex;
        return (SNMP_SUCCESS);
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsipv6PingAdminStatus
 Input       :  The Indices
                Fsipv6PingIndex

                The Object 
                retValFsipv6PingAdminStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6PingAdminStatus (INT4 i4Fsipv6PingIndex,
                             INT4 *pi4RetValFsipv6PingAdminStatus)
{
    if (pPing[i4Fsipv6PingIndex]->u1Admin != IP6_PING_INVALID)
    {
        *pi4RetValFsipv6PingAdminStatus = pPing[i4Fsipv6PingIndex]->u1Admin;
        return (SNMP_SUCCESS);
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsipv6PingInterval
 Input       :  The Indices
                Fsipv6PingIndex

                The Object 
                retValFsipv6PingInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6PingInterval (INT4 i4Fsipv6PingIndex,
                          INT4 *pi4RetValFsipv6PingInterval)
{
    if (pPing[i4Fsipv6PingIndex]->u1Admin != IP6_PING_INVALID)
    {
        *pi4RetValFsipv6PingInterval =
            (INT4) pPing[i4Fsipv6PingIndex]->u2Ping6Interval;
        return (SNMP_SUCCESS);
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsipv6PingRcvTimeout
 Input       :  The Indices
                Fsipv6PingIndex

                The Object 
                retValFsipv6PingRcvTimeout
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6PingRcvTimeout (INT4 i4Fsipv6PingIndex,
                            INT4 *pi4RetValFsipv6PingRcvTimeout)
{
    if (pPing[i4Fsipv6PingIndex]->u1Admin != IP6_PING_INVALID)
    {
        *pi4RetValFsipv6PingRcvTimeout =
            (INT4) pPing[i4Fsipv6PingIndex]->u2Ping6RcvTimeout;
        return (SNMP_SUCCESS);
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsipv6PingTries
 Input       :  The Indices
                Fsipv6PingIndex

                The Object 
                retValFsipv6PingTries
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6PingTries (INT4 i4Fsipv6PingIndex, INT4 *pi4RetValFsipv6PingTries)
{
    if (pPing[i4Fsipv6PingIndex]->u1Admin != IP6_PING_INVALID)
    {
        *pi4RetValFsipv6PingTries =
            (INT4) pPing[i4Fsipv6PingIndex]->u2Ping6MaxTries;
        return (SNMP_SUCCESS);
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsipv6PingSize
 Input       :  The Indices
                Fsipv6PingIndex

                The Object 
                retValFsipv6PingSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6PingSize (INT4 i4Fsipv6PingIndex, INT4 *pi4RetValFsipv6PingSize)
{
    if (pPing[i4Fsipv6PingIndex]->u1Admin != IP6_PING_INVALID)
    {
        *pi4RetValFsipv6PingSize = (INT4) pPing[i4Fsipv6PingIndex]->u2Ping6Size;
        return (SNMP_SUCCESS);
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsipv6PingSentCount
 Input       :  The Indices
                Fsipv6PingIndex

                The Object 
                retValFsipv6PingSentCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6PingSentCount (INT4 i4Fsipv6PingIndex,
                           INT4 *pi4RetValFsipv6PingSentCount)
{
    if (pPing[i4Fsipv6PingIndex]->u1Admin != IP6_PING_INVALID)
    {
        *pi4RetValFsipv6PingSentCount =
            (INT4) pPing[i4Fsipv6PingIndex]->u2SentCount;
        return (SNMP_SUCCESS);
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsipv6PingAverageTime
 Input       :  The Indices
                Fsipv6PingIndex

                The Object 
                retValFsipv6PingAverageTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6PingAverageTime (INT4 i4Fsipv6PingIndex,
                             INT4 *pi4RetValFsipv6PingAverageTime)
{
    if (pPing[i4Fsipv6PingIndex]->u1Admin != IP6_PING_INVALID)
    {
        *pi4RetValFsipv6PingAverageTime =
            (INT4) pPing[i4Fsipv6PingIndex]->u2AverageTime;
        return (SNMP_SUCCESS);
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsipv6PingMaxTime
 Input       :  The Indices
                Fsipv6PingIndex

                The Object 
                retValFsipv6PingMaxTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6PingMaxTime (INT4 i4Fsipv6PingIndex,
                         INT4 *pi4RetValFsipv6PingMaxTime)
{
    if (pPing[i4Fsipv6PingIndex]->u1Admin != IP6_PING_INVALID)
    {
        *pi4RetValFsipv6PingMaxTime =
            (INT4) pPing[i4Fsipv6PingIndex]->u2MaxTime;
        return (SNMP_SUCCESS);
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsipv6PingMinTime
 Input       :  The Indices
                Fsipv6PingIndex

                The Object 
                retValFsipv6PingMinTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6PingMinTime (INT4 i4Fsipv6PingIndex,
                         INT4 *pi4RetValFsipv6PingMinTime)
{
    if (pPing[i4Fsipv6PingIndex]->u1Admin != IP6_PING_INVALID)
    {
        *pi4RetValFsipv6PingMinTime =
            (INT4) pPing[i4Fsipv6PingIndex]->u2MinTime;
        return (SNMP_SUCCESS);
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsipv6PingOperStatus
 Input       :  The Indices
                Fsipv6PingIndex

                The Object 
                retValFsipv6PingOperStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6PingOperStatus (INT4 i4Fsipv6PingIndex,
                            INT4 *pi4RetValFsipv6PingOperStatus)
{
    if (pPing[i4Fsipv6PingIndex]->u1Admin != IP6_PING_INVALID)
    {
        *pi4RetValFsipv6PingOperStatus =
            (INT4) pPing[i4Fsipv6PingIndex]->u1Oper;
        return (SNMP_SUCCESS);
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsipv6PingSuccesses
 Input       :  The Indices
                Fsipv6PingIndex

                The Object 
                retValFsipv6PingSuccesses
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6PingSuccesses (INT4 i4Fsipv6PingIndex,
                           UINT4 *pu4RetValFsipv6PingSuccesses)
{
    if (pPing[i4Fsipv6PingIndex]->u1Admin != IP6_PING_INVALID)
    {
        *pu4RetValFsipv6PingSuccesses =
            (INT4) pPing[i4Fsipv6PingIndex]->u2Successes;
        return (SNMP_SUCCESS);
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsipv6PingPercentageLoss
 Input       :  The Indices
                Fsipv6PingIndex

                The Object 
                retValFsipv6PingPercentageLoss
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6PingPercentageLoss (INT4 i4Fsipv6PingIndex,
                                INT4 *pi4RetValFsipv6PingPercentageLoss)
{
    if (pPing[i4Fsipv6PingIndex]->u1Admin != IP6_PING_INVALID)
    {
        if (pPing[i4Fsipv6PingIndex]->u2SentCount != 0)
        {
            *pi4RetValFsipv6PingPercentageLoss =
                ((INT4)
                 ((pPing
                   [i4Fsipv6PingIndex]->u2SentCount -
                   pPing[i4Fsipv6PingIndex]->u2Successes) * 100 /
                  pPing[i4Fsipv6PingIndex]->u2SentCount));
            return SNMP_SUCCESS;
        }
        *pi4RetValFsipv6PingPercentageLoss = 0;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsipv6PingData
 Input       :  The Indices
                Fsipv6PingIndex

                The Object 
                retValFsipv6PingData
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6PingData (INT4 i4Fsipv6PingIndex,
                      tSNMP_OCTET_STRING_TYPE * pRetValFsipv6PingData)
{
    if (pPing[i4Fsipv6PingIndex]->u1Admin != IP6_PING_INVALID)
    {
        MEMCPY (pRetValFsipv6PingData->pu1_OctetList,
                &pPing[i4Fsipv6PingIndex]->PingData, 6);
        pRetValFsipv6PingData->i4_Length = 6;
        return (SNMP_SUCCESS);
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsipv6PingSrcAddr
 Input       :  The Indices
                Fsipv6PingIndex

                The Object 
                retValFsipv6PingSrcAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6PingSrcAddr (INT4 i4Fsipv6PingIndex,
                         tSNMP_OCTET_STRING_TYPE * pRetValFsipv6PingSrcAddr)
{
    if (pPing[i4Fsipv6PingIndex]->u1Admin != IP6_PING_INVALID)
    {
        Ip6AddrCopy ((tIp6Addr *) (VOID *) pRetValFsipv6PingSrcAddr->
                     pu1_OctetList, &pPing[i4Fsipv6PingIndex]->PingSrcAddr);
        pRetValFsipv6PingSrcAddr->i4_Length = 16;
        return (SNMP_SUCCESS);
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsipv6PingZoneId
 Input       :  The Indices
                Fsipv6PingIndex

                The Object 
                retValFsipv6PingZoneId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6PingZoneId (INT4 i4Fsipv6PingIndex,
                        tSNMP_OCTET_STRING_TYPE * pRetValFsipv6PingZoneId)
{
    if (pPing[i4Fsipv6PingIndex]->u1Admin != IP6_PING_INVALID)
    {
        STRCPY (pRetValFsipv6PingZoneId->pu1_OctetList,
                pPing[i4Fsipv6PingIndex]->au1ZoneId);
        pRetValFsipv6PingZoneId->i4_Length =
            STRLEN (pPing[i4Fsipv6PingIndex]->au1ZoneId);
        return SNMP_SUCCESS;

    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsipv6PingDestAddrType
 Input       :  The Indices
                Fsipv6PingIndex

                The Object 
                retValFsipv6PingDestAddrType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6PingDestAddrType (INT4 i4Fsipv6PingIndex,
                              INT4 *pi4RetValFsipv6PingDestAddrType)
{
    if (pPing[i4Fsipv6PingIndex]->u1Admin != IP6_PING_INVALID)
    {
        *pi4RetValFsipv6PingDestAddrType = pPing[i4Fsipv6PingIndex]->u1AddrType;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6PingHostName
 Input       :  The Indices
                Fsipv6PingIndex

                The Object
                retValFsipv6PingHostName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6PingHostName (INT4 i4Fsipv6PingIndex,
                          tSNMP_OCTET_STRING_TYPE * pRetValFsipv6PingHostName)
{
    if (pPing[i4Fsipv6PingIndex]->u1Admin != (UINT1) IP6_PING_INVALID)
    {
        STRNCPY (pRetValFsipv6PingHostName->pu1_OctetList,
                 pPing[i4Fsipv6PingIndex]->au1HostName,
                 STRLEN (pPing[i4Fsipv6PingIndex]->au1HostName));

        pRetValFsipv6PingHostName->i4_Length =
            (INT4) STRLEN (pPing[i4Fsipv6PingIndex]->au1HostName);

        pRetValFsipv6PingHostName->pu1_OctetList
            [STRLEN (pPing[i4Fsipv6PingIndex]->au1HostName)] = '\0';
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsipv6PingDest
 Input       :  The Indices
                Fsipv6PingIndex

                The Object 
                setValFsipv6PingDest
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6PingDest (INT4 i4Fsipv6PingIndex,
                      tSNMP_OCTET_STRING_TYPE * pSetValFsipv6PingDest)
{
    if (pPing[i4Fsipv6PingIndex]->u1Admin != IP6_PING_INVALID)
    {
        Ip6AddrCopy (&pPing[i4Fsipv6PingIndex]->ping6Dst,
                     (tIp6Addr *) (VOID *) pSetValFsipv6PingDest->
                     pu1_OctetList);
        return (SNMP_SUCCESS);
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsipv6PingIfIndex
 Input       :  The Indices
                Fsipv6PingIndex

                The Object 
                setValFsipv6PingIfIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6PingIfIndex (INT4 i4Fsipv6PingIndex, INT4 i4SetValFsipv6PingIfIndex)
{
    if (pPing[i4Fsipv6PingIndex]->u1Admin != IP6_PING_INVALID)
    {
        pPing[i4Fsipv6PingIndex]->u2IfIndex = (UINT2) i4SetValFsipv6PingIfIndex;
        return (SNMP_SUCCESS);
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsipv6PingAdminStatus
 Input       :  The Indices
                Fsipv6PingIndex

                The Object 
                setValFsipv6PingAdminStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6PingAdminStatus (INT4 i4Fsipv6PingIndex,
                             INT4 i4SetValFsipv6PingAdminStatus)
{
    if (pPing[i4Fsipv6PingIndex]->u1Admin == IP6_PING_INVALID)
    {
        /* entry does not exist */
        if (i4SetValFsipv6PingAdminStatus == IP6_PING_VALID)
        {
            /* create a ping entry */
            pPing[i4Fsipv6PingIndex]->u1Admin = IP6_PING_VALID;
            /* initializing the ping entry */
            Ping6CreateEntry ((UINT2) i4Fsipv6PingIndex, ICMP6_DEFAULT_VALUE,
                              NULL, IP6_PING_DISABLE);
            return SNMP_SUCCESS;
        }
        else
        {
            /* silent ignore */
            return SNMP_SUCCESS;
        }
    }
    /* entry is present */
    if (i4SetValFsipv6PingAdminStatus != IP6_PING_VALID)
    {
        if (Ping6AdminSet ((UINT2) i4Fsipv6PingIndex,
                           i4SetValFsipv6PingAdminStatus) == IP6_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6PingInterval
 Input       :  The Indices
                Fsipv6PingIndex

                The Object 
                setValFsipv6PingInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6PingInterval (INT4 i4Fsipv6PingIndex,
                          INT4 i4SetValFsipv6PingInterval)
{
    if (pPing[i4Fsipv6PingIndex]->u1Admin != IP6_PING_INVALID)
    {
        pPing[i4Fsipv6PingIndex]->u2Ping6Interval =
            (UINT2) i4SetValFsipv6PingInterval;
        return (SNMP_SUCCESS);
    }
    else
        return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsipv6PingRcvTimeout
 Input       :  The Indices
                Fsipv6PingIndex

                The Object 
                setValFsipv6PingRcvTimeout
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6PingRcvTimeout (INT4 i4Fsipv6PingIndex,
                            INT4 i4SetValFsipv6PingRcvTimeout)
{
    if (pPing[i4Fsipv6PingIndex]->u1Admin != IP6_PING_INVALID)
    {
        pPing[i4Fsipv6PingIndex]->u2Ping6RcvTimeout =
            (UINT2) i4SetValFsipv6PingRcvTimeout;
        return (SNMP_SUCCESS);
    }
    else
        return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsipv6PingTries
 Input       :  The Indices
                Fsipv6PingIndex

                The Object 
                setValFsipv6PingTries
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6PingTries (INT4 i4Fsipv6PingIndex, INT4 i4SetValFsipv6PingTries)
{
    if (pPing[i4Fsipv6PingIndex]->u1Admin != IP6_PING_INVALID)
    {
        pPing[i4Fsipv6PingIndex]->u2Ping6MaxTries =
            (UINT2) i4SetValFsipv6PingTries;
        return (SNMP_SUCCESS);
    }
    else
        return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsipv6PingSize
 Input       :  The Indices
                Fsipv6PingIndex

                The Object 
                setValFsipv6PingSize
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6PingSize (INT4 i4Fsipv6PingIndex, INT4 i4SetValFsipv6PingSize)
{
    if (pPing[i4Fsipv6PingIndex]->u1Admin != IP6_PING_INVALID)
    {
        pPing[i4Fsipv6PingIndex]->u2Ping6Size = (UINT2) i4SetValFsipv6PingSize;
        return (SNMP_SUCCESS);
    }
    else
        return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsipv6PingData
 Input       :  The Indices
                Fsipv6PingIndex

                The Object 
                setValFsipv6PingData
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6PingData (INT4 i4Fsipv6PingIndex,
                      tSNMP_OCTET_STRING_TYPE * pSetValFsipv6PingData)
{
    if (pPing[i4Fsipv6PingIndex]->u1Admin != IP6_PING_INVALID)
    {
        MEMCPY (&pPing[i4Fsipv6PingIndex]->PingData,
                pSetValFsipv6PingData->pu1_OctetList, 6);
        return (SNMP_SUCCESS);
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsipv6PingSrcAddr
 Input       :  The Indices
                Fsipv6PingIndex

                The Object 
                setValFsipv6PingSrcAddr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6PingSrcAddr (INT4 i4Fsipv6PingIndex,
                         tSNMP_OCTET_STRING_TYPE * pSetValFsipv6PingSrcAddr)
{
    if (pPing[i4Fsipv6PingIndex]->u1Admin != IP6_PING_INVALID)
    {
        Ip6AddrCopy (&pPing[i4Fsipv6PingIndex]->PingSrcAddr,
                     (tIp6Addr *) (VOID *) pSetValFsipv6PingSrcAddr->
                     pu1_OctetList);
        return (SNMP_SUCCESS);
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsipv6PingZoneId
 Input       :  The Indices
                Fsipv6PingIndex

                The Object 
                setValFsipv6PingZoneId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhSetFsipv6PingZoneId (INT4 i4Fsipv6PingIndex,
                        tSNMP_OCTET_STRING_TYPE * pSetValFsipv6PingZoneId)
{
    UINT2               u2Len = 0;

    if (pPing[i4Fsipv6PingIndex]->u1Admin != IP6_PING_INVALID)
    {
        u2Len =
            (UINT2) (STRLEN (pSetValFsipv6PingZoneId->pu1_OctetList) <
                     (CFA_CLI_MAX_IF_NAME_LEN -
                      1) ? STRLEN (pSetValFsipv6PingZoneId->
                                   pu1_OctetList) : CFA_CLI_MAX_IF_NAME_LEN -
                     1);
        STRNCPY (&pPing[i4Fsipv6PingIndex]->au1ZoneId,
                 pSetValFsipv6PingZoneId->pu1_OctetList, u2Len);
        pPing[i4Fsipv6PingIndex]->au1ZoneId[u2Len] = '\0';
        return (SNMP_SUCCESS);
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsipv6PingDestAddrType
 Input       :  The Indices
                Fsipv6PingIndex

                The Object 
                setValFsipv6PingDestAddrType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6PingDestAddrType (INT4 i4Fsipv6PingIndex,
                              INT4 i4SetValFsipv6PingDestAddrType)
{
    if (pPing[i4Fsipv6PingIndex]->u1Admin != IP6_PING_INVALID)
    {
        pPing[i4Fsipv6PingIndex]->u1AddrType = i4SetValFsipv6PingDestAddrType;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsipv6PingHostName
 Input       :  The Indices
                Fsipv6PingIndex
                                                                                                                       The Object
                setValFsipv6PingHostName
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6PingHostName (INT4 i4Fsipv6PingIndex,
                          tSNMP_OCTET_STRING_TYPE * pSetValFsipv6PingHostName)
{
    if (pPing[i4Fsipv6PingIndex]->u1Admin != (UINT1) IP6_PING_INVALID)
    {
        STRNCPY (pPing[i4Fsipv6PingIndex]->au1HostName,
                 pSetValFsipv6PingHostName->pu1_OctetList,
                 pSetValFsipv6PingHostName->i4_Length);

        pPing[i4Fsipv6PingIndex]->au1HostName
            [pSetValFsipv6PingHostName->i4_Length] = '\0';

        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Fsipv6PingDest
 Input       :  The Indices
                Fsipv6PingIndex

                The Object 
                testValFsipv6PingDest
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6PingDest (UINT4 *pu4ErrorCode, INT4 i4Fsipv6PingIndex,
                         tSNMP_OCTET_STRING_TYPE * pTestValFsipv6PingDest)
{
    UINT1               u1AddrType;

    if (i4Fsipv6PingIndex >= (INT4) gu4MaxPing6Dst || i4Fsipv6PingIndex < 0)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_PING_INDEX);
        return SNMP_FAILURE;
    }

    if (pTestValFsipv6PingDest == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_INVALID_PING_DEST);
        return SNMP_FAILURE;
    }

    if (pTestValFsipv6PingDest->i4_Length != IP6_ADDR_SIZE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        CLI_SET_ERR (CLI_IP6_INVALID_PING_DEST);
        return SNMP_FAILURE;
    }

    u1AddrType =
        Ip6AddrType ((tIp6Addr *) (VOID *) pTestValFsipv6PingDest->
                     pu1_OctetList);
    if (u1AddrType == ADDR6_UNSPECIFIED)
    {
        CLI_SET_ERR (CLI_IP6_INVALID_ADDR_TYPE);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6PingIfIndex
 Input       :  The Indices
                Fsipv6PingIndex

                The Object 
                testValFsipv6PingIfIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6PingIfIndex (UINT4 *pu4ErrorCode, INT4 i4Fsipv6PingIndex,
                            INT4 i4TestValFsipv6PingIfIndex)
{

    UINT1               au1IfAlias[CFA_CLI_MAX_IF_NAME_LEN];
    UINT1               au1TempZoneId[CFA_CLI_MAX_IF_NAME_LEN];

    MEMSET (au1TempZoneId, 0, sizeof (au1TempZoneId));
    MEMSET (au1IfAlias, 0, sizeof (au1IfAlias));

    if ((i4Fsipv6PingIndex >= (INT4) gu4MaxPing6Dst) || (i4Fsipv6PingIndex < 0))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_PING_INDEX);
        return SNMP_FAILURE;
    }

    if (Ip6ifEntryExists ((UINT4) i4TestValFsipv6PingIfIndex) != IP6_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
        return SNMP_FAILURE;
    }

    CfaGetInterfaceNameFromIndex ((UINT4) i4TestValFsipv6PingIfIndex,
                                  au1IfAlias);

    if (((MEMCMP (pPing[i4Fsipv6PingIndex]->au1ZoneId, au1TempZoneId,
                  sizeof (pPing[i4Fsipv6PingIndex]->au1ZoneId))) != 0) &&
        (STRCMP (pPing[i4Fsipv6PingIndex]->au1ZoneId, au1IfAlias) != 0))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_MISMATCH_OF_INTERFACE);
        return SNMP_FAILURE;

    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6PingAdminStatus
 Input       :  The Indices
                Fsipv6PingIndex

                The Object 
                testValFsipv6PingAdminStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6PingAdminStatus (UINT4 *pu4ErrorCode, INT4 i4Fsipv6PingIndex,
                                INT4 i4TestValFsipv6PingAdminStatus)
{
    UNUSED_PARAM (i4Fsipv6PingIndex);
    if ((i4Fsipv6PingIndex >= (INT4) gu4MaxPing6Dst) || (i4Fsipv6PingIndex < 0))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_PING_INDEX);
        return SNMP_FAILURE;
    }
    if ((i4TestValFsipv6PingAdminStatus == IP6_PING_INVALID) ||
        (i4TestValFsipv6PingAdminStatus == IP6_PING_VALID) ||
        (i4TestValFsipv6PingAdminStatus == IP6_PING_DISABLE) ||
        (i4TestValFsipv6PingAdminStatus == IP6_PING_ENABLE))
    {
        return SNMP_SUCCESS;
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    CLI_SET_ERR (CLI_IP6_INVALID_PING_ADMIN_STATUS);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6PingInterval
 Input       :  The Indices
                Fsipv6PingIndex

                The Object 
                testValFsipv6PingInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6PingInterval (UINT4 *pu4ErrorCode, INT4 i4Fsipv6PingIndex,
                             INT4 i4TestValFsipv6PingInterval)
{
    if ((i4Fsipv6PingIndex >= (INT4) gu4MaxPing6Dst) || (i4Fsipv6PingIndex < 0))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (pPing[i4Fsipv6PingIndex]->u1Admin != IP6_PING_INVALID)
    {
        if (pPing[i4Fsipv6PingIndex]->u1Oper == IP6_PING_IN_PROGRESS)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
    }
    if ((i4TestValFsipv6PingInterval >= IP6_PING_MIN_INTERVAL) &&
        (i4TestValFsipv6PingInterval <= IP6_PING_MAX_INTERVAL))
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6PingRcvTimeout
 Input       :  The Indices
                Fsipv6PingIndex

                The Object 
                testValFsipv6PingRcvTimeout
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6PingRcvTimeout (UINT4 *pu4ErrorCode, INT4 i4Fsipv6PingIndex,
                               INT4 i4TestValFsipv6PingRcvTimeout)
{
    if ((i4Fsipv6PingIndex >= (INT4) gu4MaxPing6Dst) || (i4Fsipv6PingIndex < 0))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_PING_INDEX);
        return SNMP_FAILURE;
    }
    if (pPing[i4Fsipv6PingIndex]->u1Admin != IP6_PING_INVALID)
    {
        if (pPing[i4Fsipv6PingIndex]->u1Oper == IP6_PING_IN_PROGRESS)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_IP6_PING_IN_PROGRESS);
            return SNMP_FAILURE;
        }
    }

    if ((i4TestValFsipv6PingRcvTimeout >= IP6_PING_MIN_RCV_TIMEOUT &&
         i4TestValFsipv6PingRcvTimeout <= IP6_PING_MAX_RCV_TIMEOUT) ||
        (i4TestValFsipv6PingRcvTimeout == 0))
    {
        return SNMP_SUCCESS;
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    CLI_SET_ERR (CLI_IP6_INVALID_PING_TIMEOUT);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6PingTries
 Input       :  The Indices
                Fsipv6PingIndex

                The Object 
                testValFsipv6PingTries
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6PingTries (UINT4 *pu4ErrorCode, INT4 i4Fsipv6PingIndex,
                          INT4 i4TestValFsipv6PingTries)
{
    if ((i4Fsipv6PingIndex >= (INT4) gu4MaxPing6Dst) || (i4Fsipv6PingIndex < 0))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_PING_INDEX);
        return SNMP_FAILURE;
    }
    if (pPing[i4Fsipv6PingIndex]->u1Admin != IP6_PING_INVALID)
    {
        if (pPing[i4Fsipv6PingIndex]->u1Oper == IP6_PING_IN_PROGRESS)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_IP6_PING_IN_PROGRESS);
            return SNMP_FAILURE;
        }
    }

    if (i4TestValFsipv6PingTries >= IP6_PING_MIN_TRIES
        && i4TestValFsipv6PingTries <= IP6_PING_MAX_TRIES)
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    CLI_SET_ERR (CLI_IP6_INVALID_PING_TRIES);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6PingSize
 Input       :  The Indices
                Fsipv6PingIndex

                The Object 
                testValFsipv6PingSize
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6PingSize (UINT4 *pu4ErrorCode, INT4 i4Fsipv6PingIndex,
                         INT4 i4TestValFsipv6PingSize)
{
    if ((i4Fsipv6PingIndex >= (INT4) gu4MaxPing6Dst) || (i4Fsipv6PingIndex < 0))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_PING_INDEX);
        return SNMP_FAILURE;
    }
    if (pPing[i4Fsipv6PingIndex]->u1Admin != IP6_PING_INVALID)
    {
        if (pPing[i4Fsipv6PingIndex]->u1Oper == IP6_PING_IN_PROGRESS)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_IP6_PING_IN_PROGRESS);
            return SNMP_FAILURE;
        }
    }

    if ((i4TestValFsipv6PingSize >= IP6_PING_MIN_SIZE)
        && (i4TestValFsipv6PingSize <= IP6_PING_MAX_SIZE))
    {
        return SNMP_SUCCESS;
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    CLI_SET_ERR (CLI_IP6_INVALID_PING_SIZE);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6PingData
 Input       :  The Indices
                Fsipv6PingIndex

                The Object 
                testValFsipv6PingData
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6PingData (UINT4 *pu4ErrorCode, INT4 i4Fsipv6PingIndex,
                         tSNMP_OCTET_STRING_TYPE * pTestValFsipv6PingData)
{
    INT4                i4Data = 0;
    if ((i4Fsipv6PingIndex >= (INT4) gu4MaxPing6Dst) || (i4Fsipv6PingIndex < 0))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_PING_INDEX);
        return SNMP_FAILURE;
    }
    if (pPing[i4Fsipv6PingIndex]->u1Admin != IP6_PING_INVALID)
    {
        if (pPing[i4Fsipv6PingIndex]->u1Oper == IP6_PING_IN_PROGRESS)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_IP6_PING_IN_PROGRESS);
            return SNMP_FAILURE;
        }
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

    if (pTestValFsipv6PingData == NULL)
    {
        CLI_SET_ERR (CLI_IP6_INVALID_PING_DATA);
        return SNMP_FAILURE;
    }

    if (pTestValFsipv6PingData->i4_Length != IP6_PING_DATA_SIZE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        CLI_SET_ERR (CLI_IP6_INVALID_PING_DATA);
        return SNMP_FAILURE;
    }

    i4Data = CliHexStrToDecimal (pTestValFsipv6PingData->pu1_OctetList);
    if ((i4Data < 0) || (i4Data > 65535))
    {
        CLI_SET_ERR (CLI_IP6_INVALID_PING_DATA);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6PingSrcAddr
 Input       :  The Indices
                Fsipv6PingIndex

                The Object 
                testValFsipv6PingSrcAddr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6PingSrcAddr (UINT4 *pu4ErrorCode, INT4 i4Fsipv6PingIndex,
                            tSNMP_OCTET_STRING_TYPE * pTestValFsipv6PingSrcAddr)
{
    UINT1               u1AddrType;

    if ((i4Fsipv6PingIndex >= (INT4) gu4MaxPing6Dst) || (i4Fsipv6PingIndex < 0))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_PING_INDEX);
        return SNMP_FAILURE;
    }
    if (pPing[i4Fsipv6PingIndex]->u1Admin != IP6_PING_INVALID)
    {
        if (pPing[i4Fsipv6PingIndex]->u1Oper == IP6_PING_IN_PROGRESS)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_IP6_PING_IN_PROGRESS);
            return SNMP_FAILURE;
        }
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

    if (pTestValFsipv6PingSrcAddr == NULL)
    {
        CLI_SET_ERR (CLI_IP6_INVALID_PING_SOURCE);
        return SNMP_FAILURE;
    }

    if (pTestValFsipv6PingSrcAddr->i4_Length != IP6_ADDR_SIZE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        CLI_SET_ERR (CLI_IP6_INVALID_PING_SOURCE);
        return SNMP_FAILURE;
    }

    u1AddrType =
        Ip6AddrType ((tIp6Addr *) (VOID *) pTestValFsipv6PingSrcAddr->
                     pu1_OctetList);
    if (u1AddrType == ADDR6_UNSPECIFIED)
    {
        CLI_SET_ERR (CLI_IP6_INVALID_ADDR_TYPE);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6PingZoneId
 Input       :  The Indices
                Fsipv6PingIndex

                The Object 
                testValFsipv6PingZoneId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6PingZoneId (UINT4 *pu4ErrorCode,
                           INT4 i4Fsipv6PingIndex,
                           tSNMP_OCTET_STRING_TYPE * pTestValFsipv6PingZoneId)
{

    UINT4               u4ContextId = 0;
    UINT4               u4Index = 0;

    if (gIp6GblInfo.pIp6CurrCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    u4ContextId = gIp6GblInfo.pIp6CurrCxt->u4ContextId;

    if (OSIX_FAILURE == CfaGetInterfaceIndexFromNameInCxt (u4ContextId,
                                                           pTestValFsipv6PingZoneId->
                                                           pu1_OctetList,
                                                           &u4Index))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if ((i4Fsipv6PingIndex >= (INT4) gu4MaxPing6Dst) || (i4Fsipv6PingIndex < 0))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_PING_INDEX);
        return SNMP_FAILURE;
    }

    if ((pPing[i4Fsipv6PingIndex]->u2IfIndex != ICMP6_DEFAULT_VALUE) &&
        ((UINT2) u4Index != pPing[i4Fsipv6PingIndex]->u2IfIndex))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_MISMATCH_OF_INTERFACE);
        return (SNMP_FAILURE);
    }
    pPing[i4Fsipv6PingIndex]->u2IfIndex = (UINT2) u4Index;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6PingDestAddrType
 Input       :  The Indices
                Fsipv6PingIndex

                The Object 
                testValFsipv6PingDestAddrType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6PingDestAddrType (UINT4 *pu4ErrorCode, INT4 i4Fsipv6PingIndex,
                                 INT4 i4TestValFsipv6PingDestAddrType)
{
    if ((i4Fsipv6PingIndex >= (INT4) gu4MaxPing6Dst) || (i4Fsipv6PingIndex < 0))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_PING_INDEX);
        return SNMP_FAILURE;
    }
    if ((i4TestValFsipv6PingDestAddrType == ADDR6_ANYCAST)
        || (i4TestValFsipv6PingDestAddrType == ZERO))
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6PingHostName
 Input       :  The Indices
                Fsipv6PingIndex
                                                                                                                       The Object
                testValFsipv6PingHostName
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)                                                 SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6PingHostName (UINT4 *pu4ErrorCode,
                             INT4 i4Fsipv6PingIndex,
                             tSNMP_OCTET_STRING_TYPE
                             * pTestValFsipv6PingHostName)
{
    if (pPing[i4Fsipv6PingIndex]->u1Admin == (UINT1) IP6_PING_INVALID)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_PING_INDEX);
        return SNMP_FAILURE;
    }

    if (pTestValFsipv6PingHostName->i4_Length > (INT4) DNS_MAX_QUERY_LEN)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        CLI_SET_ERR (CLI_IP6_INVALID_PING_DEST);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Fsipv6PingTable
 Input       :  The Indices
                Fsipv6PingIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsipv6PingTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Fsipv6NDProxyListTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsipv6NDProxyListTable
 Input       :  The Indices
                Fsipv6NdProxyAddr
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 
     
     
     
     
     
     
     
    nmhValidateIndexInstanceFsipv6NDProxyListTable
    (tSNMP_OCTET_STRING_TYPE * pFsipv6NdProxyAddr)
{
    tIp6Addr            Ip6Addr;

    MEMSET (&Ip6Addr, IP6_ZERO, sizeof (tIp6Addr));
    if (gIp6GblInfo.pIp6CurrCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    Ip6AddrCopy (&Ip6Addr, (tIp6Addr *) (VOID *) pFsipv6NdProxyAddr->
                 pu1_OctetList);
    if ((IS_ADDR_MULTI (Ip6Addr)) || (IS_ADDR_UNSPECIFIED (Ip6Addr)))
    {
        return SNMP_FAILURE;
    }

    if (Nd6ProxyCheckAddrInCxt (gIp6GblInfo.pIp6CurrCxt, Ip6Addr) == NULL)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsipv6NDProxyListTable
 Input       :  The Indices
                Fsipv6NdProxyAddr
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1 
     
     
     
     
     
     
     
    nmhGetFirstIndexFsipv6NDProxyListTable
    (tSNMP_OCTET_STRING_TYPE * pFsipv6NdProxyAddr)
{
    tNd6ProxyListNode  *pProxyListNode;
    tTMO_SLL_NODE      *pFirst;

    if (gIp6GblInfo.pIp6CurrCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if (TMO_SLL_Count (&(gIp6GblInfo.pIp6CurrCxt->Nd6ProxyList)) > 0)
    {
        /*Get the First Node in the List */
        pFirst = TMO_SLL_First (&(gIp6GblInfo.pIp6CurrCxt->Nd6ProxyList));
        pProxyListNode = ND6_PROXY_LST_PTR_FROM_SLL (pFirst);

        Ip6AddrCopy ((tIp6Addr *) (VOID *) pFsipv6NdProxyAddr->pu1_OctetList,
                     &pProxyListNode->Nd6ProxyAddr);
        pFsipv6NdProxyAddr->i4_Length = sizeof (tIp6Addr);

        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsipv6NDProxyListTable
 Input       :  The Indices
                Fsipv6NdProxyAddr
                nextFsipv6NdProxyAddr
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsipv6NDProxyListTable (tSNMP_OCTET_STRING_TYPE *
                                       pFsipv6NdProxyAddr,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pNextFsipv6NdProxyAddr)
{
    tNd6ProxyListNode  *pProxyListNode;
    tIp6Addr            Ip6Addr;

    MEMSET (&Ip6Addr, IP6_ZERO, sizeof (tIp6Addr));
    if (gIp6GblInfo.pIp6CurrCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if (TMO_SLL_Count (&(gIp6GblInfo.pIp6CurrCxt->Nd6ProxyList)) > 0)
    {
        Ip6AddrCopy (&Ip6Addr, (tIp6Addr *) (VOID *)
                     pFsipv6NdProxyAddr->pu1_OctetList);

        /* Get the Next Nd6 Proxy List */
        if ((pProxyListNode = Nd6NextProxyAddrInCxt (gIp6GblInfo.pIp6CurrCxt,
                                                     Ip6Addr)) != NULL)
        {
            Ip6AddrCopy ((tIp6Addr *) (VOID *)
                         pNextFsipv6NdProxyAddr->pu1_OctetList,
                         &pProxyListNode->Nd6ProxyAddr);
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsipv6NdProxyAdminStatus
 Input       :  The Indices
                Fsipv6NdProxyAddr

                The Object 
                retValFsipv6NdProxyAdminStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6NdProxyAdminStatus (tSNMP_OCTET_STRING_TYPE * pFsipv6NdProxyAddr,
                                INT4 *pi4RetValFsipv6NdProxyAdminStatus)
{
    tIp6Addr            Ip6Addr;

    MEMSET (&Ip6Addr, IP6_ZERO, sizeof (tIp6Addr));
    if (gIp6GblInfo.pIp6CurrCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    Ip6AddrCopy (&Ip6Addr, (tIp6Addr *) (VOID *)
                 pFsipv6NdProxyAddr->pu1_OctetList);

    if (Nd6ProxyCheckAddrInCxt (gIp6GblInfo.pIp6CurrCxt, Ip6Addr) != NULL)
    {
        *pi4RetValFsipv6NdProxyAdminStatus = ND6_ADD_TO_PROXY_LIST;
        return SNMP_SUCCESS;
    }
    else
        return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsipv6NdProxyAdminStatus
 Input       :  The Indices
                Fsipv6NdProxyAddr

                The Object 
                setValFsipv6NdProxyAdminStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6NdProxyAdminStatus (tSNMP_OCTET_STRING_TYPE * pFsipv6NdProxyAddr,
                                INT4 i4SetValFsipv6NdProxyAdminStatus)
{
    tIp6Addr            Ip6Addr;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = IPVX_ZERO;
    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    MEMSET (&Ip6Addr, IP6_ZERO, sizeof (tIp6Addr));
    if (gIp6GblInfo.pIp6CurrCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    Ip6AddrCopy (&Ip6Addr, (tIp6Addr *) (VOID *)
                 pFsipv6NdProxyAddr->pu1_OctetList);

    if (Nd6ProxyUpdateInCxt (gIp6GblInfo.pIp6CurrCxt, Ip6Addr,
                             i4SetValFsipv6NdProxyAdminStatus) == IP6_SUCCESS)
    {
        RM_GET_SEQ_NUM (&u4SeqNum);
        SnmpNotifyInfo.pu4ObjectId = FsMIIpv6NDProxyAdminStatus;
        SnmpNotifyInfo.u4OidLen =
            sizeof (FsMIIpv6NDProxyAdminStatus) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = u4SeqNum;
        SnmpNotifyInfo.u1RowStatus = FALSE;
        SnmpNotifyInfo.pLockPointer = Ip6Lock;
        SnmpNotifyInfo.pUnLockPointer = Ip6UnLock;
        SnmpNotifyInfo.u4Indices = IPVX_TWO;
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %s %i",
                          gIp6GblInfo.pIp6CurrCxt->u4ContextId,
                          pFsipv6NdProxyAddr,
                          i4SetValFsipv6NdProxyAdminStatus));

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Fsipv6NdProxyAdminStatus
 Input       :  The Indices
                Fsipv6NdProxyAddr

                The Object 
                testValFsipv6NdProxyAdminStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6NdProxyAdminStatus (UINT4 *pu4ErrorCode,
                                   tSNMP_OCTET_STRING_TYPE * pFsipv6NdProxyAddr,
                                   INT4 i4TestValFsipv6NdProxyAdminStatus)
{
    UNUSED_PARAM (pFsipv6NdProxyAddr);

    if (gIp6GblInfo.pIp6CurrCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if ((i4TestValFsipv6NdProxyAdminStatus == ND6_ADD_TO_PROXY_LIST)
        || (i4TestValFsipv6NdProxyAdminStatus == ND6_DEL_FROM_PROXY_LIST))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Fsipv6NDProxyListTable
 Input       :  The Indices
                Fsipv6NdProxyAddr
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsipv6NDProxyListTable (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Fsipv6AddrSelPolicyTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsipv6AddrSelPolicyTable
 Input       :  The Indices
                Fsipv6AddrSelPolicyPrefix
                Fsipv6AddrSelPolicyPrefixLen
                Fsipv6AddrSelPolicyIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1                nmhValidateIndexInstanceFsipv6AddrSelPolicyTable
    (tSNMP_OCTET_STRING_TYPE * pFsipv6AddrSelPolicyPrefix,
     INT4 i4Fsipv6AddrSelPolicyPrefixLen, INT4 i4Fsipv6AddrSelPolicyIfIndex)
{
    INT1                i1AddrType;

    /*Interface index validation */
    if (i4Fsipv6AddrSelPolicyIfIndex != 0)
    {
        if (Ip6ifEntryExists ((UINT4) i4Fsipv6AddrSelPolicyIfIndex) ==
            IP6_FAILURE)
        {
            return SNMP_FAILURE;
        }

        /* Prefix Validation */
        i1AddrType = Ip6AddrType ((tIp6Addr *) (VOID *)
                                  pFsipv6AddrSelPolicyPrefix->pu1_OctetList);
        if ((i1AddrType != ADDR6_UNICAST) && (i1AddrType != ADDR6_ANYCAST) &&
            (i1AddrType != ADDR6_MULTI) && (i1AddrType != ADDR6_LOOPBACK))
        {
            return SNMP_FAILURE;
        }

        /* Prefix Length Validation. */
        if ((i4Fsipv6AddrSelPolicyPrefixLen <= 0) ||
            (i4Fsipv6AddrSelPolicyPrefixLen > IP6_ADDR_MAX_PREFIX))
        {
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsipv6AddrSelPolicyTable
 Input       :  The Indices
                Fsipv6AddrSelPolicyPrefix
                Fsipv6AddrSelPolicyPrefixLen
                Fsipv6AddrSelPolicyIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1                nmhGetFirstIndexFsipv6AddrSelPolicyTable
    (tSNMP_OCTET_STRING_TYPE * pFsipv6AddrSelPolicyPrefix,
     INT4 *pi4Fsipv6AddrSelPolicyPrefixLen, INT4 *pi4Fsipv6AddrSelPolicyIfIndex)
{
    tIp6AddrSelPolicy  *pPolicyPrefix = NULL;
    UINT4               u4Index = 0;

    pPolicyPrefix = Ip6GetFirstPolicyPrefix (u4Index);
    if (pPolicyPrefix != NULL)
    {
        *pi4Fsipv6AddrSelPolicyIfIndex = (INT4) u4Index;
        MEMCPY (pFsipv6AddrSelPolicyPrefix->pu1_OctetList,
                &pPolicyPrefix->Ip6PolicyPrefix, IP6_ADDR_SIZE);
        pFsipv6AddrSelPolicyPrefix->i4_Length = IP6_ADDR_SIZE;
        *pi4Fsipv6AddrSelPolicyPrefixLen = pPolicyPrefix->u1PrefixLen;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsipv6AddrSelPolicyTable
 Input       :  The Indices
                Fsipv6AddrSelPolicyPrefix
                nextFsipv6AddrSelPolicyPrefix
                Fsipv6AddrSelPolicyPrefixLen
                nextFsipv6AddrSelPolicyPrefixLen
                Fsipv6AddrSelPolicyIfIndex
                nextFsipv6AddrSelPolicyIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1                nmhGetNextIndexFsipv6AddrSelPolicyTable
    (tSNMP_OCTET_STRING_TYPE * pFsipv6AddrSelPolicyPrefix,
     tSNMP_OCTET_STRING_TYPE * pNextFsipv6AddrSelPolicyPrefix,
     INT4 i4Fsipv6AddrSelPolicyPrefixLen,
     INT4 *pi4NextFsipv6AddrSelPolicyPrefixLen,
     INT4 i4Fsipv6AddrSelPolicyIfIndex, INT4 *pi4NextFsipv6AddrSelPolicyIfIndex)
{
    tIp6AddrSelPolicy  *pNextPrefix = NULL;

    if (i4Fsipv6AddrSelPolicyIfIndex < 0)
    {
        return (SNMP_FAILURE);
    }
    pNextPrefix = Ip6GetNextPolicyPrefix ((UINT4) i4Fsipv6AddrSelPolicyIfIndex,
                                          (tIp6Addr *) (VOID *)
                                          pFsipv6AddrSelPolicyPrefix->
                                          pu1_OctetList,
                                          (UINT1)
                                          i4Fsipv6AddrSelPolicyPrefixLen);

    if (pNextPrefix != NULL)
    {
        *pi4NextFsipv6AddrSelPolicyIfIndex = pNextPrefix->u4IfIndex;
        MEMCPY (pNextFsipv6AddrSelPolicyPrefix->pu1_OctetList,
                &pNextPrefix->Ip6PolicyPrefix, IP6_ADDR_SIZE);
        pNextFsipv6AddrSelPolicyPrefix->i4_Length = IP6_ADDR_SIZE;
        *pi4NextFsipv6AddrSelPolicyPrefixLen = pNextPrefix->u1PrefixLen;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsipv6AddrSelPolicyScope
 Input       :  The Indices
                Fsipv6AddrSelPolicyPrefix
                Fsipv6AddrSelPolicyPrefixLen
                Fsipv6AddrSelPolicyIfIndex

                The Object 
                retValFsipv6AddrSelPolicyScope
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFsipv6AddrSelPolicyScope
    (tSNMP_OCTET_STRING_TYPE * pFsipv6AddrSelPolicyPrefix,
     INT4 i4Fsipv6AddrSelPolicyPrefixLen,
     INT4 i4Fsipv6AddrSelPolicyIfIndex, INT4 *pi4RetValFsipv6AddrSelPolicyScope)
{
    tIp6AddrSelPolicy  *pPolicyPrefix = NULL;

    pPolicyPrefix = Ip6GetPolicyPrefix ((UINT4) i4Fsipv6AddrSelPolicyIfIndex,
                                        (tIp6Addr *) (VOID *)
                                        pFsipv6AddrSelPolicyPrefix->
                                        pu1_OctetList,
                                        (UINT1) i4Fsipv6AddrSelPolicyPrefixLen);

    if (pPolicyPrefix != NULL)
    {
        *pi4RetValFsipv6AddrSelPolicyScope = pPolicyPrefix->u1Scope;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsipv6AddrSelPolicyPrecedence
 Input       :  The Indices
                Fsipv6AddrSelPolicyPrefix
                Fsipv6AddrSelPolicyPrefixLen
                Fsipv6AddrSelPolicyIfIndex

                The Object 
                retValFsipv6AddrSelPolicyPrecedence
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFsipv6AddrSelPolicyPrecedence
    (tSNMP_OCTET_STRING_TYPE * pFsipv6AddrSelPolicyPrefix,
     INT4 i4Fsipv6AddrSelPolicyPrefixLen,
     INT4 i4Fsipv6AddrSelPolicyIfIndex,
     INT4 *pi4RetValFsipv6AddrSelPolicyPrecedence)
{

    tIp6AddrSelPolicy  *pPolicyPrefix = NULL;

    pPolicyPrefix = Ip6GetPolicyPrefix ((UINT4) i4Fsipv6AddrSelPolicyIfIndex,
                                        (tIp6Addr *) (VOID *)
                                        pFsipv6AddrSelPolicyPrefix->
                                        pu1_OctetList,
                                        (UINT1) i4Fsipv6AddrSelPolicyPrefixLen);
    if (pPolicyPrefix != NULL)
    {
        *pi4RetValFsipv6AddrSelPolicyPrecedence = pPolicyPrefix->u1Precedence;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsipv6AddrSelPolicyLabel
 Input       :  The Indices
                Fsipv6AddrSelPolicyPrefix
                Fsipv6AddrSelPolicyPrefixLen
                Fsipv6AddrSelPolicyIfIndex

                The Object 
                retValFsipv6AddrSelPolicyLabel
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6AddrSelPolicyLabel (tSNMP_OCTET_STRING_TYPE *
                                pFsipv6AddrSelPolicyPrefix,
                                INT4 i4Fsipv6AddrSelPolicyPrefixLen,
                                INT4 i4Fsipv6AddrSelPolicyIfIndex,
                                INT4 *pi4RetValFsipv6AddrSelPolicyLabel)
{
    tIp6AddrSelPolicy  *pPolicyPrefix = NULL;

    pPolicyPrefix = Ip6GetPolicyPrefix ((UINT4) i4Fsipv6AddrSelPolicyIfIndex,
                                        (tIp6Addr *) (VOID *)
                                        pFsipv6AddrSelPolicyPrefix->
                                        pu1_OctetList,
                                        (UINT1) i4Fsipv6AddrSelPolicyPrefixLen);
    if (pPolicyPrefix != NULL)
    {
        *pi4RetValFsipv6AddrSelPolicyLabel = pPolicyPrefix->u1Label;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsipv6AddrSelPolicyAddrType
 Input       :  The Indices
                Fsipv6AddrSelPolicyPrefix
                Fsipv6AddrSelPolicyPrefixLen
                Fsipv6AddrSelPolicyIfIndex

                The Object 
                retValFsipv6AddrSelPolicyAddrType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6AddrSelPolicyAddrType (tSNMP_OCTET_STRING_TYPE
                                   * pFsipv6AddrSelPolicyPrefix,
                                   INT4 i4Fsipv6AddrSelPolicyPrefixLen,
                                   INT4 i4Fsipv6AddrSelPolicyIfIndex,
                                   INT4 *pi4RetValFsipv6AddrSelPolicyAddrType)
{

    if (Ip6GetFsipv6AddrSelPolicyAddrType (i4Fsipv6AddrSelPolicyIfIndex,
                                           pFsipv6AddrSelPolicyPrefix,
                                           i4Fsipv6AddrSelPolicyPrefixLen,
                                           pi4RetValFsipv6AddrSelPolicyAddrType)
        == IP6_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6AddrSelPolicyIsPublicAddr
 Input       :  The Indices
                Fsipv6AddrSelPolicyPrefix
                Fsipv6AddrSelPolicyPrefixLen
                Fsipv6AddrSelPolicyIfIndex

                The Object 
                retValFsipv6AddrSelPolicyIsPublicAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6AddrSelPolicyIsPublicAddr (tSNMP_OCTET_STRING_TYPE
                                       * pFsipv6AddrSelPolicyPrefix,
                                       INT4 i4Fsipv6AddrSelPolicyPrefixLen,
                                       INT4 i4Fsipv6AddrSelPolicyIfIndex,
                                       INT4
                                       *pi4RetValFsipv6AddrSelPolicyIsPublicAddr)
{

    tIp6AddrSelPolicy  *pPolicyPrefix = NULL;
    pPolicyPrefix = Ip6GetPolicyPrefix ((UINT4) i4Fsipv6AddrSelPolicyIfIndex,
                                        (tIp6Addr *) (VOID *)
                                        pFsipv6AddrSelPolicyPrefix->
                                        pu1_OctetList,
                                        (UINT1) i4Fsipv6AddrSelPolicyPrefixLen);
    if (pPolicyPrefix != NULL)
    {
        *pi4RetValFsipv6AddrSelPolicyIsPublicAddr =
            pPolicyPrefix->u1IsPublicAddr;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsipv6AddrSelPolicyIsSelfAddr
 Input       :  The Indices
                Fsipv6AddrSelPolicyPrefix
                Fsipv6AddrSelPolicyPrefixLen
                Fsipv6AddrSelPolicyIfIndex

                The Object 
                retValFsipv6AddrSelPolicyIsSelfAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6AddrSelPolicyIsSelfAddr (tSNMP_OCTET_STRING_TYPE
                                     * pFsipv6AddrSelPolicyPrefix,
                                     INT4 i4Fsipv6AddrSelPolicyPrefixLen,
                                     INT4 i4Fsipv6AddrSelPolicyIfIndex,
                                     INT4
                                     *pi4RetValFsipv6AddrSelPolicyIsSelfAddr)
{

    tIp6AddrSelPolicy  *pPolicyPrefix = NULL;
    pPolicyPrefix = Ip6GetPolicyPrefix ((UINT4) i4Fsipv6AddrSelPolicyIfIndex,
                                        (tIp6Addr *) (VOID *)
                                        pFsipv6AddrSelPolicyPrefix->
                                        pu1_OctetList,
                                        (UINT1) i4Fsipv6AddrSelPolicyPrefixLen);
    if (pPolicyPrefix != NULL)
    {
        *pi4RetValFsipv6AddrSelPolicyIsSelfAddr = pPolicyPrefix->u1IsSelfAddr;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsipv6AddrSelPolicyReachabilityStatus
 Input       :  The Indices
                Fsipv6AddrSelPolicyPrefix
                Fsipv6AddrSelPolicyPrefixLen
                Fsipv6AddrSelPolicyIfIndex

                The Object 
                retValFsipv6AddrSelPolicyReachabilityStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6AddrSelPolicyReachabilityStatus (tSNMP_OCTET_STRING_TYPE
                                             * pFsipv6AddrSelPolicyPrefix,
                                             INT4
                                             i4Fsipv6AddrSelPolicyPrefixLen,
                                             INT4 i4Fsipv6AddrSelPolicyIfIndex,
                                             INT4
                                             *pi4RetValFsipv6AddrSelPolicyReachabilityStatus)
{

    tIp6AddrSelPolicy  *pPolicyPrefix = NULL;
    pPolicyPrefix = Ip6GetPolicyPrefix ((UINT4) i4Fsipv6AddrSelPolicyIfIndex,
                                        (tIp6Addr *) (VOID *)
                                        pFsipv6AddrSelPolicyPrefix->
                                        pu1_OctetList,
                                        (UINT1) i4Fsipv6AddrSelPolicyPrefixLen);
    if (pPolicyPrefix != NULL)
    {
        *pi4RetValFsipv6AddrSelPolicyReachabilityStatus =
            pPolicyPrefix->u1ReachabilityStatus;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsipv6AddrSelPolicyConfigStatus
 Input       :  The Indices
                Fsipv6AddrSelPolicyPrefix
                Fsipv6AddrSelPolicyPrefixLen
                Fsipv6AddrSelPolicyIfIndex

                The Object 
                retValFsipv6AddrSelPolicyConfigStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6AddrSelPolicyConfigStatus (tSNMP_OCTET_STRING_TYPE
                                       * pFsipv6AddrSelPolicyPrefix,
                                       INT4 i4Fsipv6AddrSelPolicyPrefixLen,
                                       INT4 i4Fsipv6AddrSelPolicyIfIndex,
                                       INT4
                                       *pi4RetValFsipv6AddrSelPolicyConfigStatus)
{

    tIp6AddrSelPolicy  *pPolicyPrefix = NULL;
    pPolicyPrefix = Ip6GetPolicyPrefix ((UINT4) i4Fsipv6AddrSelPolicyIfIndex,
                                        (tIp6Addr *) (VOID *)
                                        pFsipv6AddrSelPolicyPrefix->
                                        pu1_OctetList,
                                        (UINT1) i4Fsipv6AddrSelPolicyPrefixLen);
    if (pPolicyPrefix != NULL)
    {
        *pi4RetValFsipv6AddrSelPolicyConfigStatus =
            pPolicyPrefix->u1CreateStatus;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsipv6AddrSelPolicyRowStatus
 Input       :  The Indices
                Fsipv6AddrSelPolicyPrefix
                Fsipv6AddrSelPolicyPrefixLen
                Fsipv6AddrSelPolicyIfIndex

                The Object 
                retValFsipv6AddrSelPolicyRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6AddrSelPolicyRowStatus (tSNMP_OCTET_STRING_TYPE
                                    * pFsipv6AddrSelPolicyPrefix,
                                    INT4 i4Fsipv6AddrSelPolicyPrefixLen,
                                    INT4 i4Fsipv6AddrSelPolicyIfIndex,
                                    INT4 *pi4RetValFsipv6AddrSelPolicyRowStatus)
{

    tIp6AddrSelPolicy  *pPolicyPrefix = NULL;
    pPolicyPrefix = Ip6GetPolicyPrefix ((UINT4) i4Fsipv6AddrSelPolicyIfIndex,
                                        (tIp6Addr *) (VOID *)
                                        pFsipv6AddrSelPolicyPrefix->
                                        pu1_OctetList,
                                        (UINT1) i4Fsipv6AddrSelPolicyPrefixLen);
    if (pPolicyPrefix != NULL)
    {
        *pi4RetValFsipv6AddrSelPolicyRowStatus = pPolicyPrefix->u1RowStatus;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsipv6AddrSelPolicyPrecedence
 Input       :  The Indices
                Fsipv6AddrSelPolicyPrefix
                Fsipv6AddrSelPolicyPrefixLen
                Fsipv6AddrSelPolicyIfIndex

                The Object 
                setValFsipv6AddrSelPolicyPrecedence
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6AddrSelPolicyPrecedence (tSNMP_OCTET_STRING_TYPE
                                     * pFsipv6AddrSelPolicyPrefix,
                                     INT4 i4Fsipv6AddrSelPolicyPrefixLen,
                                     INT4 i4Fsipv6AddrSelPolicyIfIndex,
                                     INT4 i4SetValFsipv6AddrSelPolicyPrecedence)
{
    tIp6AddrSelPolicy  *pPolicyPrefix = NULL;
    tIp6If             *pIf6 =
        Ip6ifGetEntry ((UINT4) i4Fsipv6AddrSelPolicyIfIndex);

    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    pPolicyPrefix = Ip6GetPolicyPrefix ((UINT4) i4Fsipv6AddrSelPolicyIfIndex,
                                        (tIp6Addr *) (VOID *)
                                        pFsipv6AddrSelPolicyPrefix->
                                        pu1_OctetList,
                                        (UINT1) i4Fsipv6AddrSelPolicyPrefixLen);
    if (pPolicyPrefix != NULL)
    {
        if (pPolicyPrefix->u1Precedence !=
            (UINT4) i4SetValFsipv6AddrSelPolicyPrecedence)
        {
            pPolicyPrefix->u1Precedence =
                (UINT4) i4SetValFsipv6AddrSelPolicyPrecedence;

        }
        IncMsrForIp6PolicyPrefixTable (i4Fsipv6AddrSelPolicyIfIndex,
                                       pFsipv6AddrSelPolicyPrefix,
                                       i4Fsipv6AddrSelPolicyPrefixLen, 'i',
                                       &i4SetValFsipv6AddrSelPolicyPrecedence,
                                       FsMIIpv6AddrSelPolicyPrecedence,
                                       sizeof (FsMIIpv6AddrSelPolicyPrecedence)
                                       / sizeof (UINT4), FALSE);

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsipv6AddrSelPolicyLabel
 Input       :  The Indices
                Fsipv6AddrSelPolicyPrefix
                Fsipv6AddrSelPolicyPrefixLen
                Fsipv6AddrSelPolicyIfIndex

                The Object 
                setValFsipv6AddrSelPolicyLabel
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6AddrSelPolicyLabel (tSNMP_OCTET_STRING_TYPE
                                * pFsipv6AddrSelPolicyPrefix,
                                INT4 i4Fsipv6AddrSelPolicyPrefixLen,
                                INT4 i4Fsipv6AddrSelPolicyIfIndex,
                                INT4 i4SetValFsipv6AddrSelPolicyLabel)
{

    tIp6AddrSelPolicy  *pPolicyPrefix = NULL;
    tIp6If             *pIf6 =
        Ip6ifGetEntry ((UINT4) i4Fsipv6AddrSelPolicyIfIndex);

    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    pPolicyPrefix = Ip6GetPolicyPrefix ((UINT4) i4Fsipv6AddrSelPolicyIfIndex,
                                        (tIp6Addr *) (VOID *)
                                        pFsipv6AddrSelPolicyPrefix->
                                        pu1_OctetList,
                                        (UINT1) i4Fsipv6AddrSelPolicyPrefixLen);
    if (pPolicyPrefix != NULL)
    {
        if (pPolicyPrefix->u1Label != (UINT1) i4SetValFsipv6AddrSelPolicyLabel)
        {
            pPolicyPrefix->u1Label = (UINT1) i4SetValFsipv6AddrSelPolicyLabel;

        }
        IncMsrForIp6PolicyPrefixTable (i4Fsipv6AddrSelPolicyIfIndex,
                                       pFsipv6AddrSelPolicyPrefix,
                                       i4Fsipv6AddrSelPolicyPrefixLen, 'i',
                                       &i4SetValFsipv6AddrSelPolicyLabel,
                                       FsMIIpv6AddrSelPolicyLabel,
                                       sizeof (FsMIIpv6AddrSelPolicyLabel) /
                                       sizeof (UINT4), FALSE);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/***************************************************************************
 Function    :  nmhSetFsipv6AddrSelPolicyAddrType
 Input       :  The Indices
                Fsipv6AddrSelPolicyPrefix
                Fsipv6AddrSelPolicyPrefixLen
                Fsipv6AddrSelPolicyIfIndex

                The Object
                setValFsipv6AddrSelPolicyAddrType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6AddrSelPolicyAddrType (tSNMP_OCTET_STRING_TYPE
                                   * pFsipv6AddrSelPolicyPrefix,
                                   INT4 i4Fsipv6AddrSelPolicyPrefixLen,
                                   INT4 i4Fsipv6AddrSelPolicyIfIndex,
                                   INT4 i4SetValFsipv6AddrSelPolicyAddrType)
{
    tIp6AddrSelPolicy  *pPolicyPrefix = NULL;
    tIp6If             *pIf6 =
        Ip6ifGetEntry ((UINT4) i4Fsipv6AddrSelPolicyIfIndex);

    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    pPolicyPrefix = Ip6GetPolicyPrefix ((UINT4) i4Fsipv6AddrSelPolicyIfIndex,
                                        (tIp6Addr *) (VOID *)
                                        pFsipv6AddrSelPolicyPrefix->
                                        pu1_OctetList,
                                        (UINT1) i4Fsipv6AddrSelPolicyPrefixLen);
    if (pPolicyPrefix != NULL)
    {
        if (pPolicyPrefix->u1AddrType !=
            (UINT4) i4SetValFsipv6AddrSelPolicyAddrType)
        {
            pPolicyPrefix->u1AddrType =
                (UINT1) i4SetValFsipv6AddrSelPolicyAddrType;

        }
        IncMsrForIp6PolicyPrefixTable (i4Fsipv6AddrSelPolicyIfIndex,
                                       pFsipv6AddrSelPolicyPrefix,
                                       i4Fsipv6AddrSelPolicyPrefixLen, 'i',
                                       &i4SetValFsipv6AddrSelPolicyAddrType,
                                       FsMIIpv6AddrSelPolicyAddrType,
                                       sizeof (FsMIIpv6AddrSelPolicyAddrType) /
                                       sizeof (UINT4), FALSE);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsipv6AddrSelPolicyRowStatus
 Input       :  The Indices
                Fsipv6AddrSelPolicyPrefix
                Fsipv6AddrSelPolicyPrefixLen
                Fsipv6AddrSelPolicyIfIndex

                The Object 
                setValFsipv6AddrSelPolicyRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6AddrSelPolicyRowStatus (tSNMP_OCTET_STRING_TYPE
                                    * pFsipv6AddrSelPolicyPrefix,
                                    INT4 i4Fsipv6AddrSelPolicyPrefixLen,
                                    INT4 i4Fsipv6AddrSelPolicyIfIndex,
                                    INT4 i4SetValFsipv6AddrSelPolicyRowStatus)
{
    tIp6AddrSelPolicy  *pPolicyPrefix = NULL;
    tIp6If             *pIf6 =
        Ip6ifGetEntry ((UINT4) i4Fsipv6AddrSelPolicyIfIndex);

    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    switch (i4SetValFsipv6AddrSelPolicyRowStatus)
    {
        case IP6FWD_CREATE_AND_WAIT:
            pPolicyPrefix = Ip6PolicyPrefixCreate ((UINT4)
                                                   i4Fsipv6AddrSelPolicyIfIndex,
                                                   (tIp6Addr *) (VOID *)
                                                   pFsipv6AddrSelPolicyPrefix->
                                                   pu1_OctetList,
                                                   (UINT1)
                                                   i4Fsipv6AddrSelPolicyPrefixLen,
                                                   IP6FWD_NOT_READY);

            if (pPolicyPrefix == NULL)
            {
                return SNMP_FAILURE;
            }
            break;

        case IP6FWD_ACTIVE:
            pPolicyPrefix = Ip6GetPolicyPrefix ((UINT4)
                                                i4Fsipv6AddrSelPolicyIfIndex,
                                                (tIp6Addr *) (VOID *)
                                                pFsipv6AddrSelPolicyPrefix->
                                                pu1_OctetList,
                                                (UINT1)
                                                i4Fsipv6AddrSelPolicyPrefixLen);
            if (pPolicyPrefix == NULL)
            {
                return SNMP_FAILURE;
            }

            if (pPolicyPrefix->u1RowStatus == IP6FWD_ACTIVE)
            {
                /* Already Active */
                break;
            }

            pPolicyPrefix->u1RowStatus = IP6FWD_ACTIVE;

            break;

        case IP6FWD_DESTROY:
            pPolicyPrefix = Ip6GetPolicyPrefix ((UINT4)
                                                i4Fsipv6AddrSelPolicyIfIndex,
                                                (tIp6Addr *) (VOID *)
                                                pFsipv6AddrSelPolicyPrefix->
                                                pu1_OctetList,
                                                (UINT1)
                                                i4Fsipv6AddrSelPolicyPrefixLen);

            if (pPolicyPrefix == NULL)
            {
                return SNMP_FAILURE;
            }

            /* Delete the Prefix from interface */
            Ip6PolicyPrefixDelete ((UINT4) i4Fsipv6AddrSelPolicyIfIndex,
                                   (tIp6Addr *) (VOID *)
                                   pFsipv6AddrSelPolicyPrefix->pu1_OctetList,
                                   (UINT1) i4Fsipv6AddrSelPolicyPrefixLen);
            break;

        case IP6FWD_NOT_IN_SERVICE:
            pPolicyPrefix = Ip6GetPolicyPrefix ((UINT4)
                                                i4Fsipv6AddrSelPolicyIfIndex,
                                                (tIp6Addr *) (VOID *)
                                                pFsipv6AddrSelPolicyPrefix->
                                                pu1_OctetList,
                                                (UINT1)
                                                i4Fsipv6AddrSelPolicyPrefixLen);
            if (pPolicyPrefix == NULL)
            {
                return SNMP_FAILURE;
            }
            pPolicyPrefix->u1RowStatus = IP6FWD_NOT_IN_SERVICE;
            break;

        default:
            return SNMP_FAILURE;
    }
    IncMsrForIp6PolicyPrefixTable (i4Fsipv6AddrSelPolicyIfIndex,
                                   pFsipv6AddrSelPolicyPrefix,
                                   i4Fsipv6AddrSelPolicyPrefixLen, 'i',
                                   &i4SetValFsipv6AddrSelPolicyRowStatus,
                                   FsMIIpv6AddrSelPolicyRowStatus,
                                   sizeof (FsMIIpv6AddrSelPolicyRowStatus) /
                                   sizeof (UINT4), TRUE);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Fsipv6AddrSelPolicyPrecedence
 Input       :  The Indices
                Fsipv6AddrSelPolicyPrefix
                Fsipv6AddrSelPolicyPrefixLen
                Fsipv6AddrSelPolicyIfIndex

                The Object 
                testValFsipv6AddrSelPolicyPrecedence
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6AddrSelPolicyPrecedence (UINT4 *pu4ErrorCode,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pFsipv6AddrSelPolicyPrefix,
                                        INT4 i4Fsipv6AddrSelPolicyPrefixLen,
                                        INT4 i4Fsipv6AddrSelPolicyIfIndex,
                                        INT4
                                        i4TestValFsipv6AddrSelPolicyPrecedence)
{

    INT1                i1AddrType;

    /* Prefix Validation */
    i1AddrType = Ip6AddrType ((tIp6Addr *) (VOID *)
                              pFsipv6AddrSelPolicyPrefix->pu1_OctetList);
    if (i1AddrType != ADDR6_UNICAST)

    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_ADDR_TYPE);
        return SNMP_FAILURE;
    }

    /* Prefix Len Validation */
    if ((i4Fsipv6AddrSelPolicyPrefixLen < IP6_ADDR_MIN_PREFIX) ||
        (i4Fsipv6AddrSelPolicyPrefixLen > IP6_ADDR_MAX_PREFIX))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_PREFIXLEN);
        return SNMP_FAILURE;
    }

    if ((i4TestValFsipv6AddrSelPolicyPrecedence < IP6_MIN_PRECEDENCE) ||
        (i4TestValFsipv6AddrSelPolicyPrecedence > IP6_MAX_PRECEDENCE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_INVALID_PRECEDENCE);
        return SNMP_FAILURE;
    }
    UNUSED_PARAM (i4Fsipv6AddrSelPolicyIfIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6AddrSelPolicyLabel
 Input       :  The Indices
                Fsipv6AddrSelPolicyPrefix
                Fsipv6AddrSelPolicyPrefixLen
                Fsipv6AddrSelPolicyIfIndex

                The Object 
                testValFsipv6AddrSelPolicyLabel
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6AddrSelPolicyLabel (UINT4 *pu4ErrorCode,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsipv6AddrSelPolicyPrefix,
                                   INT4 i4Fsipv6AddrSelPolicyPrefixLen,
                                   INT4 i4Fsipv6AddrSelPolicyIfIndex,
                                   INT4 i4TestValFsipv6AddrSelPolicyLabel)
{
    INT1                i1AddrType;

    /* Prefix Validation */
    i1AddrType = Ip6AddrType ((tIp6Addr *) (VOID *) pFsipv6AddrSelPolicyPrefix->
                              pu1_OctetList);
    if (i1AddrType != ADDR6_UNICAST)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_ADDR_TYPE);
        return SNMP_FAILURE;
    }

    /* Prefix Len Validation */
    if ((i4Fsipv6AddrSelPolicyPrefixLen < IP6_ADDR_MIN_PREFIX) ||
        (i4Fsipv6AddrSelPolicyPrefixLen > IP6_ADDR_MAX_PREFIX))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_PREFIXLEN);
        return SNMP_FAILURE;
    }

    if ((i4TestValFsipv6AddrSelPolicyLabel < IP6_MIN_LABEL) ||
        (i4TestValFsipv6AddrSelPolicyLabel > IP6_MAX_LABEL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_INVALID_LABEL);
        return SNMP_FAILURE;
    }
    UNUSED_PARAM (i4Fsipv6AddrSelPolicyIfIndex);
    return SNMP_SUCCESS;

}

/*************************************************************************
Function    :  nmhTestv2Fsipv6AddrSelPolicyAddrType
 Input       :  The Indices
                Fsipv6AddrSelPolicyPrefix
                Fsipv6AddrSelPolicyPrefixLen
                Fsipv6AddrSelPolicyIfIndex

                The Object
                testValFsipv6AddrSelPolicyAddrType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6AddrSelPolicyAddrType (UINT4 *pu4ErrorCode,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pFsipv6AddrSelPolicyPrefix,
                                      INT4 i4Fsipv6AddrSelPolicyPrefixLen,
                                      INT4 i4Fsipv6AddrSelPolicyIfIndex,
                                      INT4 i4TestValFsipv6AddrSelPolicyAddrType)
{

    INT1                i1AddrType;

    /* Prefix Validation */
    i1AddrType = Ip6AddrType ((tIp6Addr *) (VOID *) pFsipv6AddrSelPolicyPrefix->
                              pu1_OctetList);
    if (i1AddrType != ADDR6_UNICAST)

    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_ADDR_TYPE);
        return SNMP_FAILURE;
    }

    /* Prefix Len Validation */
    if ((i4Fsipv6AddrSelPolicyPrefixLen < IP6_ADDR_MIN_PREFIX) ||
        (i4Fsipv6AddrSelPolicyPrefixLen > IP6_ADDR_MAX_PREFIX))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_PREFIXLEN);
        return SNMP_FAILURE;
    }
    if ((i4TestValFsipv6AddrSelPolicyAddrType < ADDR6_UNICAST) ||
        (i4TestValFsipv6AddrSelPolicyAddrType > ADDR6_ANYCAST))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_INVALID_LABEL);
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (i4Fsipv6AddrSelPolicyIfIndex);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6AddrSelPolicyRowStatus
 Input       :  The Indices
                Fsipv6AddrSelPolicyPrefix
                Fsipv6AddrSelPolicyPrefixLen
                Fsipv6AddrSelPolicyIfIndex

                The Object 
                testValFsipv6AddrSelPolicyRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6AddrSelPolicyRowStatus (UINT4 *pu4ErrorCode,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFsipv6AddrSelPolicyPrefix,
                                       INT4 i4Fsipv6AddrSelPolicyPrefixLen,
                                       INT4 i4Fsipv6AddrSelPolicyIfIndex,
                                       INT4
                                       i4TestValFsipv6AddrSelPolicyRowStatus)
{
    INT1                i1AddrType;

    /* Prefix Validation */
    i1AddrType = Ip6AddrType ((tIp6Addr *) (VOID *)
                              pFsipv6AddrSelPolicyPrefix->pu1_OctetList);
    if (i1AddrType != ADDR6_UNICAST)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_ADDR_TYPE);
        return SNMP_FAILURE;
    }

    /* Prefix Len Validation */
    if ((i4Fsipv6AddrSelPolicyPrefixLen < 0) ||
        (i4Fsipv6AddrSelPolicyPrefixLen > IP6_ADDR_MAX_PREFIX))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_PREFIXLEN);
        return SNMP_FAILURE;
    }
    if ((i4TestValFsipv6AddrSelPolicyRowStatus != IP6FWD_CREATE_AND_WAIT) &&
        (i4TestValFsipv6AddrSelPolicyRowStatus != IP6FWD_ACTIVE) &&
        (i4TestValFsipv6AddrSelPolicyRowStatus != IP6FWD_DESTROY) &&
        (i4TestValFsipv6AddrSelPolicyRowStatus != IP6FWD_NOT_IN_SERVICE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_INVALID_PREF_ADMIN_STATUS);
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (i4Fsipv6AddrSelPolicyIfIndex);
    return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Fsipv6AddrSelPolicyTable
 Input       :  The Indices
                Fsipv6AddrSelPolicyPrefix
                Fsipv6AddrSelPolicyPrefixLen
                Fsipv6AddrSelPolicyIfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsipv6AddrSelPolicyTable (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Fsipv6IfScopeZoneMapTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsipv6IfScopeZoneMapTable
 Input       :  The Indices
                Fsipv6ScopeZoneIndexIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsipv6IfScopeZoneMapTable (INT4
                                                   i4Fsipv6ScopeZoneIndexIfIndex)
{
    if (Ip6ifEntryExists ((UINT4) i4Fsipv6ScopeZoneIndexIfIndex) == IP6_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsipv6IfScopeZoneMapTable
 Input       :  The Indices
                Fsipv6ScopeZoneIndexIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsipv6IfScopeZoneMapTable (INT4 *pi4Fsipv6ScopeZoneIndexIfIndex)
{
    *pi4Fsipv6ScopeZoneIndexIfIndex = 0;

    if (Ip6ifGetNextIndex ((UINT4 *) pi4Fsipv6ScopeZoneIndexIfIndex) ==
        IP6_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsipv6IfScopeZoneMapTable
 Input       :  The Indices
                Fsipv6ScopeZoneIndexIfIndex
                nextFsipv6ScopeZoneIndexIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsipv6IfScopeZoneMapTable (INT4 i4Fsipv6ScopeZoneIndexIfIndex,
                                          INT4
                                          *pi4NextFsipv6ScopeZoneIndexIfIndex)
{
    INT4                i4Index = (i4Fsipv6ScopeZoneIndexIfIndex) + 1;

    if ((i4Fsipv6ScopeZoneIndexIfIndex < 0) ||
        (i4Fsipv6ScopeZoneIndexIfIndex >= IP6_MAX_LOGICAL_IF_INDEX))
    {
        return SNMP_FAILURE;
    }

    IP6_IF_SCAN (i4Index, (INT4) IP6_MAX_LOGICAL_IF_INDEX)
    {
        if (Ip6ifEntryExists ((UINT4) i4Index) == IP6_SUCCESS)
        {
            *pi4NextFsipv6ScopeZoneIndexIfIndex = i4Index;
            return (SNMP_SUCCESS);
        }
    }
    return (SNMP_FAILURE);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsipv6ScopeZoneIndexInterfaceLocal
 Input       :  The Indices
                Fsipv6ScopeZoneIndexIfIndex

                The Object 
                retValFsipv6ScopeZoneIndexInterfaceLocal
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6ScopeZoneIndexInterfaceLocal (INT4 i4Fsipv6ScopeZoneIndexIfIndex,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pRetValFsipv6ScopeZoneIndexInterfaceLocal)
{
    tIp6IfZoneMapInfo  *pIp6IfZoneMapInfo = NULL;
    UINT1               u1Scope = ADDR6_SCOPE_INTLOCAL;

    pIp6IfZoneMapInfo =
        Ip6ZoneGetIfScopeZoneEntry ((UINT4) i4Fsipv6ScopeZoneIndexIfIndex,
                                    u1Scope);
    if (NULL == pIp6IfZoneMapInfo)
    {
        pRetValFsipv6ScopeZoneIndexInterfaceLocal->i4_Length =
            STRLEN (IP6_DEFAULT_SCOPE_ZONE_NAME);
        STRCPY (pRetValFsipv6ScopeZoneIndexInterfaceLocal->pu1_OctetList,
                IP6_DEFAULT_SCOPE_ZONE_NAME);
    }
    else
    {
        pRetValFsipv6ScopeZoneIndexInterfaceLocal->i4_Length =
            STRLEN (pIp6IfZoneMapInfo->au1ZoneName);
        MEMCPY (pRetValFsipv6ScopeZoneIndexInterfaceLocal->pu1_OctetList,
                pIp6IfZoneMapInfo->au1ZoneName,
                pRetValFsipv6ScopeZoneIndexInterfaceLocal->i4_Length);
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsipv6ScopeZoneIndexLinkLocal
 Input       :  The Indices
                Fsipv6ScopeZoneIndexIfIndex

                The Object 
                retValFsipv6ScopeZoneIndexLinkLocal
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6ScopeZoneIndexLinkLocal (INT4 i4Fsipv6ScopeZoneIndexIfIndex,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pRetValFsipv6ScopeZoneIndexLinkLocal)
{
    tIp6IfZoneMapInfo  *pIp6IfZoneMapInfo = NULL;
    UINT1               u1Scope = ADDR6_SCOPE_LLOCAL;

    pIp6IfZoneMapInfo =
        Ip6ZoneGetIfScopeZoneEntry ((UINT4) i4Fsipv6ScopeZoneIndexIfIndex,
                                    u1Scope);

    if (NULL == pIp6IfZoneMapInfo)
    {
        pRetValFsipv6ScopeZoneIndexLinkLocal->i4_Length =
            STRLEN (IP6_DEFAULT_SCOPE_ZONE_NAME);
        STRCPY (pRetValFsipv6ScopeZoneIndexLinkLocal->pu1_OctetList,
                IP6_DEFAULT_SCOPE_ZONE_NAME);
    }
    else
    {
        pRetValFsipv6ScopeZoneIndexLinkLocal->i4_Length =
            STRLEN (pIp6IfZoneMapInfo->au1ZoneName);
        MEMCPY (pRetValFsipv6ScopeZoneIndexLinkLocal->pu1_OctetList,
                pIp6IfZoneMapInfo->au1ZoneName,
                pRetValFsipv6ScopeZoneIndexLinkLocal->i4_Length);
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6ScopeZoneIndex3
 Input       :  The Indices
                Fsipv6ScopeZoneIndexIfIndex

                The Object 
                retValFsipv6ScopeZoneIndex3
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6ScopeZoneIndex3 (INT4 i4Fsipv6ScopeZoneIndexIfIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pRetValFsipv6ScopeZoneIndex3)
{
    tIp6IfZoneMapInfo  *pIp6IfZoneMapInfo = NULL;
    UINT1               u1Scope = ADDR6_SCOPE_SUBNETLOCAL;

    pIp6IfZoneMapInfo =
        Ip6ZoneGetIfScopeZoneEntry ((UINT4) i4Fsipv6ScopeZoneIndexIfIndex,
                                    u1Scope);
    if (NULL == pIp6IfZoneMapInfo)
    {
        pRetValFsipv6ScopeZoneIndex3->i4_Length =
            STRLEN (IP6_DEFAULT_SCOPE_ZONE_NAME);
        STRCPY (pRetValFsipv6ScopeZoneIndex3->pu1_OctetList,
                IP6_DEFAULT_SCOPE_ZONE_NAME);
    }
    else
    {
        pRetValFsipv6ScopeZoneIndex3->i4_Length =
            STRLEN (pIp6IfZoneMapInfo->au1ZoneName);
        MEMCPY (pRetValFsipv6ScopeZoneIndex3->pu1_OctetList,
                pIp6IfZoneMapInfo->au1ZoneName,
                pRetValFsipv6ScopeZoneIndex3->i4_Length);
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsipv6ScopeZoneIndexAdminLocal
 Input       :  The Indices
                Fsipv6ScopeZoneIndexIfIndex

                The Object 
                retValFsipv6ScopeZoneIndexAdminLocal
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6ScopeZoneIndexAdminLocal (INT4 i4Fsipv6ScopeZoneIndexIfIndex,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pRetValFsipv6ScopeZoneIndexAdminLocal)
{
    tIp6IfZoneMapInfo  *pIp6IfZoneMapInfo = NULL;
    UINT1               u1Scope = ADDR6_SCOPE_ADMINLOCAL;

    pIp6IfZoneMapInfo =
        Ip6ZoneGetIfScopeZoneEntry ((UINT4) i4Fsipv6ScopeZoneIndexIfIndex,
                                    u1Scope);
    if (NULL == pIp6IfZoneMapInfo)
    {
        pRetValFsipv6ScopeZoneIndexAdminLocal->i4_Length =
            STRLEN (IP6_DEFAULT_SCOPE_ZONE_NAME);
        STRCPY (pRetValFsipv6ScopeZoneIndexAdminLocal->pu1_OctetList,
                IP6_DEFAULT_SCOPE_ZONE_NAME);
    }
    else
    {
        pRetValFsipv6ScopeZoneIndexAdminLocal->i4_Length =
            STRLEN (pIp6IfZoneMapInfo->au1ZoneName);
        MEMCPY (pRetValFsipv6ScopeZoneIndexAdminLocal->pu1_OctetList,
                pIp6IfZoneMapInfo->au1ZoneName,
                pRetValFsipv6ScopeZoneIndexAdminLocal->i4_Length);
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsipv6ScopeZoneIndexSiteLocal
 Input       :  The Indices
                Fsipv6ScopeZoneIndexIfIndex

                The Object 
                retValFsipv6ScopeZoneIndexSiteLocal
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6ScopeZoneIndexSiteLocal (INT4 i4Fsipv6ScopeZoneIndexIfIndex,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pRetValFsipv6ScopeZoneIndexSiteLocal)
{
    tIp6IfZoneMapInfo  *pIp6IfZoneMapInfo = NULL;
    UINT1               u1Scope = ADDR6_SCOPE_SITELOCAL;

    pIp6IfZoneMapInfo =
        Ip6ZoneGetIfScopeZoneEntry (i4Fsipv6ScopeZoneIndexIfIndex, u1Scope);
    if (NULL == pIp6IfZoneMapInfo)
    {
        pRetValFsipv6ScopeZoneIndexSiteLocal->i4_Length =
            STRLEN (IP6_DEFAULT_SCOPE_ZONE_NAME);
        STRCPY (pRetValFsipv6ScopeZoneIndexSiteLocal->pu1_OctetList,
                IP6_DEFAULT_SCOPE_ZONE_NAME);
    }
    else
    {
        pRetValFsipv6ScopeZoneIndexSiteLocal->i4_Length =
            STRLEN (pIp6IfZoneMapInfo->au1ZoneName);
        MEMCPY (pRetValFsipv6ScopeZoneIndexSiteLocal->pu1_OctetList,
                pIp6IfZoneMapInfo->au1ZoneName,
                pRetValFsipv6ScopeZoneIndexSiteLocal->i4_Length);
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6ScopeZoneIndex6
 Input       :  The Indices
                Fsipv6ScopeZoneIndexIfIndex

                The Object 
                retValFsipv6ScopeZoneIndex6
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6ScopeZoneIndex6 (INT4 i4Fsipv6ScopeZoneIndexIfIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pRetValFsipv6ScopeZoneIndex6)
{
    tIp6IfZoneMapInfo  *pIp6IfZoneMapInfo = NULL;
    UINT1               u1Scope = ADDR6_SCOPE_UNASSIGN6;

    pIp6IfZoneMapInfo =
        Ip6ZoneGetIfScopeZoneEntry (i4Fsipv6ScopeZoneIndexIfIndex, u1Scope);
    if (NULL == pIp6IfZoneMapInfo)
    {
        pRetValFsipv6ScopeZoneIndex6->i4_Length =
            STRLEN (IP6_DEFAULT_SCOPE_ZONE_NAME);
        STRCPY (pRetValFsipv6ScopeZoneIndex6->pu1_OctetList,
                IP6_DEFAULT_SCOPE_ZONE_NAME);
    }
    else
    {
        pRetValFsipv6ScopeZoneIndex6->i4_Length =
            STRLEN (pIp6IfZoneMapInfo->au1ZoneName);
        MEMCPY (pRetValFsipv6ScopeZoneIndex6->pu1_OctetList,
                pIp6IfZoneMapInfo->au1ZoneName,
                pRetValFsipv6ScopeZoneIndex6->i4_Length);
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6ScopeZoneIndex7
 Input       :  The Indices
                Fsipv6ScopeZoneIndexIfIndex

                The Object 
                retValFsipv6ScopeZoneIndex7
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6ScopeZoneIndex7 (INT4 i4Fsipv6ScopeZoneIndexIfIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pRetValFsipv6ScopeZoneIndex7)
{
    tIp6IfZoneMapInfo  *pIp6IfZoneMapInfo = NULL;
    UINT1               u1Scope = ADDR6_SCOPE_UNASSIGN7;

    pIp6IfZoneMapInfo =
        Ip6ZoneGetIfScopeZoneEntry (i4Fsipv6ScopeZoneIndexIfIndex, u1Scope);
    if (NULL == pIp6IfZoneMapInfo)
    {
        pRetValFsipv6ScopeZoneIndex7->i4_Length =
            STRLEN (IP6_DEFAULT_SCOPE_ZONE_NAME);
        STRCPY (pRetValFsipv6ScopeZoneIndex7->pu1_OctetList,
                IP6_DEFAULT_SCOPE_ZONE_NAME);
    }
    else
    {
        pRetValFsipv6ScopeZoneIndex7->i4_Length =
            STRLEN (pIp6IfZoneMapInfo->au1ZoneName);
        MEMCPY (pRetValFsipv6ScopeZoneIndex7->pu1_OctetList,
                pIp6IfZoneMapInfo->au1ZoneName,
                pRetValFsipv6ScopeZoneIndex7->i4_Length);
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6ScopeZoneIndexOrganizationLocal
 Input       :  The Indices
                Fsipv6ScopeZoneIndexIfIndex

                The Object 
                retValFsipv6ScopeZoneIndexOrganizationLocal
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6ScopeZoneIndexOrganizationLocal (INT4 i4Fsipv6ScopeZoneIndexIfIndex,
                                             tSNMP_OCTET_STRING_TYPE *
                                             pRetValFsipv6ScopeZoneIndexOrganizationLocal)
{
    tIp6IfZoneMapInfo  *pIp6IfZoneMapInfo = NULL;
    UINT1               u1Scope = ADDR6_SCOPE_ORGLOCAL;

    pIp6IfZoneMapInfo =
        Ip6ZoneGetIfScopeZoneEntry (i4Fsipv6ScopeZoneIndexIfIndex, u1Scope);
    if (NULL == pIp6IfZoneMapInfo)
    {
        pRetValFsipv6ScopeZoneIndexOrganizationLocal->i4_Length =
            STRLEN (IP6_DEFAULT_SCOPE_ZONE_NAME);
        STRCPY (pRetValFsipv6ScopeZoneIndexOrganizationLocal->pu1_OctetList,
                IP6_DEFAULT_SCOPE_ZONE_NAME);
    }
    else
    {
        pRetValFsipv6ScopeZoneIndexOrganizationLocal->i4_Length =
            STRLEN (pIp6IfZoneMapInfo->au1ZoneName);
        MEMCPY (pRetValFsipv6ScopeZoneIndexOrganizationLocal->pu1_OctetList,
                pIp6IfZoneMapInfo->au1ZoneName,
                pRetValFsipv6ScopeZoneIndexOrganizationLocal->i4_Length);
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6ScopeZoneIndex9
 Input       :  The Indices
                Fsipv6ScopeZoneIndexIfIndex

                The Object 
                retValFsipv6ScopeZoneIndex9
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6ScopeZoneIndex9 (INT4 i4Fsipv6ScopeZoneIndexIfIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pRetValFsipv6ScopeZoneIndex9)
{
    tIp6IfZoneMapInfo  *pIp6IfZoneMapInfo = NULL;
    UINT1               u1Scope = ADDR6_SCOPE_UNASSIGN9;

    pIp6IfZoneMapInfo =
        Ip6ZoneGetIfScopeZoneEntry (i4Fsipv6ScopeZoneIndexIfIndex, u1Scope);
    if (NULL == pIp6IfZoneMapInfo)
    {
        pRetValFsipv6ScopeZoneIndex9->i4_Length =
            STRLEN (IP6_DEFAULT_SCOPE_ZONE_NAME);
        STRCPY (pRetValFsipv6ScopeZoneIndex9->pu1_OctetList,
                IP6_DEFAULT_SCOPE_ZONE_NAME);
    }
    else
    {
        pRetValFsipv6ScopeZoneIndex9->i4_Length =
            STRLEN (pIp6IfZoneMapInfo->au1ZoneName);
        MEMCPY (pRetValFsipv6ScopeZoneIndex9->pu1_OctetList,
                pIp6IfZoneMapInfo->au1ZoneName,
                pRetValFsipv6ScopeZoneIndex9->i4_Length);
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6ScopeZoneIndexA
 Input       :  The Indices
                Fsipv6ScopeZoneIndexIfIndex

                The Object 
                retValFsipv6ScopeZoneIndexA
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6ScopeZoneIndexA (INT4 i4Fsipv6ScopeZoneIndexIfIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pRetValFsipv6ScopeZoneIndexA)
{
    tIp6IfZoneMapInfo  *pIp6IfZoneMapInfo = NULL;
    UINT1               u1Scope = ADDR6_SCOPE_UNASSIGNA;

    pIp6IfZoneMapInfo =
        Ip6ZoneGetIfScopeZoneEntry (i4Fsipv6ScopeZoneIndexIfIndex, u1Scope);
    if (NULL == pIp6IfZoneMapInfo)
    {
        pRetValFsipv6ScopeZoneIndexA->i4_Length =
            STRLEN (IP6_DEFAULT_SCOPE_ZONE_NAME);
        STRCPY (pRetValFsipv6ScopeZoneIndexA->pu1_OctetList,
                IP6_DEFAULT_SCOPE_ZONE_NAME);
    }
    else
    {
        pRetValFsipv6ScopeZoneIndexA->i4_Length =
            STRLEN (pIp6IfZoneMapInfo->au1ZoneName);
        MEMCPY (pRetValFsipv6ScopeZoneIndexA->pu1_OctetList,
                pIp6IfZoneMapInfo->au1ZoneName,
                pRetValFsipv6ScopeZoneIndexA->i4_Length);
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6ScopeZoneIndexB
 Input       :  The Indices
                Fsipv6ScopeZoneIndexIfIndex

                The Object 
                retValFsipv6ScopeZoneIndexB
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6ScopeZoneIndexB (INT4 i4Fsipv6ScopeZoneIndexIfIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pRetValFsipv6ScopeZoneIndexB)
{
    tIp6IfZoneMapInfo  *pIp6IfZoneMapInfo = NULL;
    UINT1               u1Scope = ADDR6_SCOPE_UNASSIGNB;

    pIp6IfZoneMapInfo =
        Ip6ZoneGetIfScopeZoneEntry (i4Fsipv6ScopeZoneIndexIfIndex, u1Scope);
    if (NULL == pIp6IfZoneMapInfo)
    {
        pRetValFsipv6ScopeZoneIndexB->i4_Length =
            STRLEN (IP6_DEFAULT_SCOPE_ZONE_NAME);
        STRCPY (pRetValFsipv6ScopeZoneIndexB->pu1_OctetList,
                IP6_DEFAULT_SCOPE_ZONE_NAME);
    }
    else
    {
        pRetValFsipv6ScopeZoneIndexB->i4_Length =
            STRLEN (pIp6IfZoneMapInfo->au1ZoneName);
        MEMCPY (pRetValFsipv6ScopeZoneIndexB->pu1_OctetList,
                pIp6IfZoneMapInfo->au1ZoneName,
                pRetValFsipv6ScopeZoneIndexB->i4_Length);
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6ScopeZoneIndexC
 Input       :  The Indices
                Fsipv6ScopeZoneIndexIfIndex

                The Object 
                retValFsipv6ScopeZoneIndexC
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetFsipv6ScopeZoneIndexC (INT4 i4Fsipv6ScopeZoneIndexIfIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pRetValFsipv6ScopeZoneIndexC)
{
    tIp6IfZoneMapInfo  *pIp6IfZoneMapInfo = NULL;
    UINT1               u1Scope = ADDR6_SCOPE_UNASSIGNC;

    pIp6IfZoneMapInfo =
        Ip6ZoneGetIfScopeZoneEntry (i4Fsipv6ScopeZoneIndexIfIndex, u1Scope);
    if (NULL == pIp6IfZoneMapInfo)
    {
        pRetValFsipv6ScopeZoneIndexC->i4_Length =
            STRLEN (IP6_DEFAULT_SCOPE_ZONE_NAME);
        STRCPY (pRetValFsipv6ScopeZoneIndexC->pu1_OctetList,
                IP6_DEFAULT_SCOPE_ZONE_NAME);
    }
    else
    {
        pRetValFsipv6ScopeZoneIndexC->i4_Length =
            STRLEN (pIp6IfZoneMapInfo->au1ZoneName);
        MEMCPY (pRetValFsipv6ScopeZoneIndexC->pu1_OctetList,
                pIp6IfZoneMapInfo->au1ZoneName,
                pRetValFsipv6ScopeZoneIndexC->i4_Length);
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6ScopeZoneIndexD
 Input       :  The Indices
                Fsipv6ScopeZoneIndexIfIndex

                The Object 
                retValFsipv6ScopeZoneIndexD
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6ScopeZoneIndexD (INT4 i4Fsipv6ScopeZoneIndexIfIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pRetValFsipv6ScopeZoneIndexD)
{
    tIp6IfZoneMapInfo  *pIp6IfZoneMapInfo = NULL;
    UINT1               u1Scope = ADDR6_SCOPE_UNASSIGND;

    pIp6IfZoneMapInfo =
        Ip6ZoneGetIfScopeZoneEntry (i4Fsipv6ScopeZoneIndexIfIndex, u1Scope);
    if (NULL == pIp6IfZoneMapInfo)
    {
        pRetValFsipv6ScopeZoneIndexD->i4_Length =
            STRLEN (IP6_DEFAULT_SCOPE_ZONE_NAME);
        STRCPY (pRetValFsipv6ScopeZoneIndexD->pu1_OctetList,
                IP6_DEFAULT_SCOPE_ZONE_NAME);
    }
    else
    {
        pRetValFsipv6ScopeZoneIndexD->i4_Length =
            STRLEN (pIp6IfZoneMapInfo->au1ZoneName);
        MEMCPY (pRetValFsipv6ScopeZoneIndexD->pu1_OctetList,
                pIp6IfZoneMapInfo->au1ZoneName,
                pRetValFsipv6ScopeZoneIndexD->i4_Length);
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6ScopeZoneIndexE
 Input       :  The Indices
                Fsipv6ScopeZoneIndexIfIndex

                The Object 
                retValFsipv6ScopeZoneIndexE
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6ScopeZoneIndexE (INT4 i4Fsipv6ScopeZoneIndexIfIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pRetValFsipv6ScopeZoneIndexE)
{
    tIp6IfZoneMapInfo  *pIp6IfZoneMapInfo = NULL;
    UINT1               u1Scope = ADDR6_SCOPE_GLOBAL;

    pIp6IfZoneMapInfo =
        Ip6ZoneGetIfScopeZoneEntry (i4Fsipv6ScopeZoneIndexIfIndex, u1Scope);
    if (NULL == pIp6IfZoneMapInfo)
    {
        pRetValFsipv6ScopeZoneIndexE->i4_Length =
            STRLEN (IP6_DEFAULT_SCOPE_ZONE_NAME);
        STRCPY (pRetValFsipv6ScopeZoneIndexE->pu1_OctetList,
                IP6_DEFAULT_SCOPE_ZONE_NAME);
    }
    else
    {
        pRetValFsipv6ScopeZoneIndexE->i4_Length =
            STRLEN (pIp6IfZoneMapInfo->au1ZoneName);
        MEMCPY (pRetValFsipv6ScopeZoneIndexE->pu1_OctetList,
                pIp6IfZoneMapInfo->au1ZoneName,
                pRetValFsipv6ScopeZoneIndexE->i4_Length);
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsipv6IfScopeZoneCreationStatus
 Input       :  The Indices
                Fsipv6ScopeZoneIndexIfIndex

                The Object 
                retValFsipv6IfScopeZoneCreationStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfScopeZoneCreationStatus (INT4 i4Fsipv6ScopeZoneIndexIfIndex,
                                       INT4
                                       *pi4RetValFsipv6IfScopeZoneCreationStatus)
{
    tIp6If             *pIf6 =
        Ip6ifGetEntry ((UINT4) i4Fsipv6ScopeZoneIndexIfIndex);

    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsipv6IfScopeZoneCreationStatus = pIf6->u1ZoneCreationStatus;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfScopeZoneRowStatus
 Input       :  The Indices
                Fsipv6ScopeZoneIndexIfIndex

                The Object 
                retValFsipv6IfScopeZoneRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
***************************************************************************/
INT1
nmhGetFsipv6IfScopeZoneRowStatus (INT4 i4Fsipv6ScopeZoneIndexIfIndex,
                                  INT4 *pi4RetValFsipv6IfScopeZoneRowStatus)
{
    tIp6If             *pIf6 =
        Ip6ifGetEntry ((UINT4) i4Fsipv6ScopeZoneIndexIfIndex);

    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsipv6IfScopeZoneRowStatus = (INT4) pIf6->u1ZoneRowStatus;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsipv6ScopeZoneIndexInterfaceLocal
 Input       :  The Indices
                Fsipv6ScopeZoneIndexIfIndex

                The Object 
                setValFsipv6ScopeZoneIndexInterfaceLocal
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6ScopeZoneIndexInterfaceLocal (INT4 i4Fsipv6ScopeZoneIndexIfIndex,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pSetValFsipv6ScopeZoneIndexInterfaceLocal)
{
    UINT1               u1Scope = ADDR6_SCOPE_INTLOCAL;
    UINT1               au1ZoneName[IP6_SCOPE_ZONE_NAME_LEN];
    UINT1               u1ZoneCreationStatus = IP6_ZONE_TYPE_MANUAL;
    INT4                i4ZoneId = 0;
    INT4                i4RetVal = SNMP_SUCCESS;
    tIp6If             *pIf6 =
        Ip6ifGetEntry ((UINT4) i4Fsipv6ScopeZoneIndexIfIndex);

    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    MEMSET (au1ZoneName, 0, IP6_SCOPE_ZONE_NAME_LEN);

    Ip6ScopeZoneCopy (au1ZoneName,
                      pSetValFsipv6ScopeZoneIndexInterfaceLocal->pu1_OctetList);

    if (IP6_FAILURE ==
        Ip6ZoneGetZoneIdFromZoneName (au1ZoneName, &u1Scope, &i4ZoneId))
    {
        return SNMP_FAILURE;
    }

    i4RetVal = Ip6ZoneCreateScopeZoneInfo
        ((UINT4) i4Fsipv6ScopeZoneIndexIfIndex,
         u1Scope, au1ZoneName, u1ZoneCreationStatus, i4ZoneId);

    if (IP6_FAILURE == i4RetVal)
    {
        return SNMP_FAILURE;
    }

    IncMsrForIpv6IfZoneMapTable (i4Fsipv6ScopeZoneIndexIfIndex, 's',
                                 pSetValFsipv6ScopeZoneIndexInterfaceLocal,
                                 FsMIIpv6ScopeZoneIndexInterfaceLocal,
                                 sizeof (FsMIIpv6ScopeZoneIndexInterfaceLocal) /
                                 sizeof (UINT4), FALSE);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6ScopeZoneIndexLinkLocal
 Input       :  The Indices
                Fsipv6ScopeZoneIndexIfIndex

                The Object 
                setValFsipv6ScopeZoneIndexLinkLocal
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6ScopeZoneIndexLinkLocal (INT4 i4Fsipv6ScopeZoneIndexIfIndex,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pSetValFsipv6ScopeZoneIndexLinkLocal)
{
    tIp6IfZoneMapInfo  *pIp6IfZoneMapInfo = NULL;
    tIp6IfZoneMapInfo  *pIp6ActiveIfZoneMapInfo = NULL;
    UINT1               u1Scope = ADDR6_SCOPE_LLOCAL;
    UINT1               au1ZoneName[IP6_SCOPE_ZONE_NAME_LEN];
    UINT1               u1ZoneCreationStatus = IP6_ZONE_TYPE_MANUAL;
    tIp6If             *pIf6 =
        Ip6ifGetEntry ((UINT4) i4Fsipv6ScopeZoneIndexIfIndex);
    INT4                i4ZoneId = 0;

    MEMSET (au1ZoneName, 0, IP6_SCOPE_ZONE_NAME_LEN);

    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }
    Ip6ScopeZoneCopy (au1ZoneName,
                      pSetValFsipv6ScopeZoneIndexLinkLocal->pu1_OctetList);

    pIp6IfZoneMapInfo =
        Ip6ZoneGetIfScopeZoneEntry (i4Fsipv6ScopeZoneIndexIfIndex, u1Scope);
    if ((NULL != pIp6IfZoneMapInfo) &&
        (pIp6IfZoneMapInfo->u1ConfigStatus == IP6_ZONE_TYPE_AUTO) &&
        (pIf6->u1IfStatusOnLink == IPV6_IF_STANDBY))
    {
        u1ZoneCreationStatus = IP6_ZONE_TYPE_OVERRIDDEN;

        pIp6ActiveIfZoneMapInfo =
            Ip6ZoneGetIfScopeZoneEntry (pIf6->u4OnLinkActiveIfId, u1Scope);
        if (NULL == pIp6ActiveIfZoneMapInfo)
            return SNMP_FAILURE;

        if (IP6_FAILURE ==
            Ip6ZoneReplaceZoneOnStandbyIf (i4Fsipv6ScopeZoneIndexIfIndex,
                                           u1Scope, u1ZoneCreationStatus,
                                           pIp6ActiveIfZoneMapInfo->i4ZoneId))
        {
            return SNMP_FAILURE;
        }

    }
    else
    {
        if (IP6_FAILURE ==
            Ip6ZoneGetZoneIdFromZoneName (au1ZoneName, &u1Scope, &i4ZoneId))
        {
            return SNMP_FAILURE;
        }

        if (IP6_FAILURE ==
            Ip6ZoneCreateScopeZoneInfo ((UINT4) i4Fsipv6ScopeZoneIndexIfIndex,
                                        u1Scope, au1ZoneName,
                                        u1ZoneCreationStatus, i4ZoneId))
        {
            return SNMP_FAILURE;
        }

    }

    IncMsrForIpv6IfZoneMapTable (i4Fsipv6ScopeZoneIndexIfIndex, 's',
                                 pSetValFsipv6ScopeZoneIndexLinkLocal,
                                 FsMIIpv6ScopeZoneIndexLinkLocal,
                                 sizeof (FsMIIpv6ScopeZoneIndexLinkLocal) /
                                 sizeof (UINT4), FALSE);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6ScopeZoneIndex3
 Input       :  The Indices
                Fsipv6ScopeZoneIndexIfIndex

                The Object 
                setValFsipv6ScopeZoneIndex3
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6ScopeZoneIndex3 (INT4 i4Fsipv6ScopeZoneIndexIfIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pSetValFsipv6ScopeZoneIndex3)
{
    tIp6IfZoneMapInfo  *pIp6IfZoneMapInfo = NULL;
    tIp6If             *pIf6 = NULL;
    UINT1               u1Scope = ADDR6_SCOPE_SUBNETLOCAL;
    UINT1               au1ZoneName[IP6_SCOPE_ZONE_NAME_LEN];
    UINT1               u1ZoneCreationStatus = IP6_ZONE_TYPE_MANUAL;
    INT4                i4ZoneId = 0;
    INT4                i4RetVal;

    pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6ScopeZoneIndexIfIndex);
    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (au1ZoneName, 0, IP6_SCOPE_ZONE_NAME_LEN);

    Ip6ScopeZoneCopy (au1ZoneName, pSetValFsipv6ScopeZoneIndex3->pu1_OctetList);

    pIp6IfZoneMapInfo =
        Ip6ZoneGetIfScopeZoneEntry (i4Fsipv6ScopeZoneIndexIfIndex, u1Scope);

    if (pIp6IfZoneMapInfo != NULL)
        return SNMP_SUCCESS;

    if (IP6_FAILURE ==
        Ip6ZoneGetZoneIdFromZoneName (au1ZoneName, &u1Scope, &i4ZoneId))
    {
        return SNMP_FAILURE;
    }

    i4RetVal = Ip6ZoneCreateScopeZoneInfo
        ((UINT4) i4Fsipv6ScopeZoneIndexIfIndex,
         u1Scope, au1ZoneName, u1ZoneCreationStatus, i4ZoneId);

    if (IP6_FAILURE == i4RetVal)
    {
        return SNMP_FAILURE;
    }

    IncMsrForIpv6IfZoneMapTable (i4Fsipv6ScopeZoneIndexIfIndex, 's',
                                 pSetValFsipv6ScopeZoneIndex3,
                                 FsMIIpv6ScopeZoneIndex3,
                                 sizeof (FsMIIpv6ScopeZoneIndex3) /
                                 sizeof (UINT4), FALSE);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6ScopeZoneIndexAdminLocal
 Input       :  The Indices
                Fsipv6ScopeZoneIndexIfIndex

                The Object 
                setValFsipv6ScopeZoneIndexAdminLocal
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6ScopeZoneIndexAdminLocal (INT4 i4Fsipv6ScopeZoneIndexIfIndex,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pSetValFsipv6ScopeZoneIndexAdminLocal)
{
    tIp6IfZoneMapInfo  *pIp6IfZoneMapInfo = NULL;
    tIp6If             *pIf6 = NULL;
    UINT1               u1Scope = ADDR6_SCOPE_ADMINLOCAL;
    UINT1               au1ZoneName[IP6_SCOPE_ZONE_NAME_LEN];
    UINT1               u1ZoneCreationStatus = IP6_ZONE_TYPE_MANUAL;
    INT4                i4RetVal;
    INT4                i4ZoneId = 0;

    MEMSET (au1ZoneName, 0, IP6_SCOPE_ZONE_NAME_LEN);

    pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6ScopeZoneIndexIfIndex);
    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    Ip6ScopeZoneCopy (au1ZoneName,
                      pSetValFsipv6ScopeZoneIndexAdminLocal->pu1_OctetList);

    pIp6IfZoneMapInfo =
        Ip6ZoneGetIfScopeZoneEntry (i4Fsipv6ScopeZoneIndexIfIndex, u1Scope);

    if (pIp6IfZoneMapInfo != NULL)
        return SNMP_SUCCESS;

    if (IP6_FAILURE ==
        Ip6ZoneGetZoneIdFromZoneName (au1ZoneName, &u1Scope, &i4ZoneId))
    {
        return SNMP_FAILURE;
    }

    i4RetVal = Ip6ZoneCreateScopeZoneInfo
        ((UINT4) i4Fsipv6ScopeZoneIndexIfIndex,
         u1Scope, au1ZoneName, u1ZoneCreationStatus, i4ZoneId);

    if (IP6_FAILURE == i4RetVal)
    {
        return SNMP_FAILURE;
    }

    IncMsrForIpv6IfZoneMapTable (i4Fsipv6ScopeZoneIndexIfIndex, 's',
                                 pSetValFsipv6ScopeZoneIndexAdminLocal,
                                 FsMIIpv6ScopeZoneIndexAdminLocal,
                                 sizeof (FsMIIpv6ScopeZoneIndexAdminLocal) /
                                 sizeof (UINT4), FALSE);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6ScopeZoneIndexSiteLocal
 Input       :  The Indices
                Fsipv6ScopeZoneIndexIfIndex

                The Object 
                setValFsipv6ScopeZoneIndexSiteLocal
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6ScopeZoneIndexSiteLocal (INT4 i4Fsipv6ScopeZoneIndexIfIndex,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pSetValFsipv6ScopeZoneIndexSiteLocal)
{
    tIp6IfZoneMapInfo  *pIp6IfZoneMapInfo = NULL;
    tIp6If             *pIf6 = NULL;
    UINT1               u1Scope = ADDR6_SCOPE_SITELOCAL;
    UINT1               au1ZoneName[IP6_SCOPE_ZONE_NAME_LEN];
    UINT1               u1ZoneCreationStatus = IP6_ZONE_TYPE_MANUAL;
    INT4                i4RetVal;
    INT4                i4ZoneId = 0;

    MEMSET (au1ZoneName, 0, IP6_SCOPE_ZONE_NAME_LEN);

    pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6ScopeZoneIndexIfIndex);
    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    Ip6ScopeZoneCopy (au1ZoneName,
                      pSetValFsipv6ScopeZoneIndexSiteLocal->pu1_OctetList);

    pIp6IfZoneMapInfo =
        Ip6ZoneGetIfScopeZoneEntry (i4Fsipv6ScopeZoneIndexIfIndex, u1Scope);

    if (pIp6IfZoneMapInfo != NULL)
        return SNMP_SUCCESS;

    if (IP6_FAILURE ==
        Ip6ZoneGetZoneIdFromZoneName (au1ZoneName, &u1Scope, &i4ZoneId))
    {
        return SNMP_FAILURE;
    }

    i4RetVal = Ip6ZoneCreateScopeZoneInfo
        ((UINT4) i4Fsipv6ScopeZoneIndexIfIndex,
         u1Scope, au1ZoneName, u1ZoneCreationStatus, i4ZoneId);

    if (IP6_FAILURE == i4RetVal)
    {
        return SNMP_FAILURE;
    }

    IncMsrForIpv6IfZoneMapTable (i4Fsipv6ScopeZoneIndexIfIndex, 's',
                                 pSetValFsipv6ScopeZoneIndexSiteLocal,
                                 FsMIIpv6ScopeZoneIndexSiteLocal,
                                 sizeof (FsMIIpv6ScopeZoneIndexSiteLocal) /
                                 sizeof (UINT4), FALSE);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6ScopeZoneIndex6
 Input       :  The Indices
                Fsipv6ScopeZoneIndexIfIndex

                The Object 
                setValFsipv6ScopeZoneIndex6
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6ScopeZoneIndex6 (INT4 i4Fsipv6ScopeZoneIndexIfIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pSetValFsipv6ScopeZoneIndex6)
{
    tIp6IfZoneMapInfo  *pIp6IfZoneMapInfo = NULL;
    tIp6If             *pIf6 = NULL;
    UINT1               u1Scope = ADDR6_SCOPE_UNASSIGN6;
    UINT1               au1ZoneName[IP6_SCOPE_ZONE_NAME_LEN];
    UINT1               u1ZoneCreationStatus = IP6_ZONE_TYPE_MANUAL;
    INT4                i4RetVal;
    INT4                i4ZoneId = 0;

    MEMSET (au1ZoneName, 0, IP6_SCOPE_ZONE_NAME_LEN);

    pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6ScopeZoneIndexIfIndex);
    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    Ip6ScopeZoneCopy (au1ZoneName, pSetValFsipv6ScopeZoneIndex6->pu1_OctetList);

    pIp6IfZoneMapInfo =
        Ip6ZoneGetIfScopeZoneEntry (i4Fsipv6ScopeZoneIndexIfIndex, u1Scope);

    if (pIp6IfZoneMapInfo != NULL)
        return SNMP_SUCCESS;

    if (IP6_FAILURE ==
        Ip6ZoneGetZoneIdFromZoneName (au1ZoneName, &u1Scope, &i4ZoneId))
    {
        return SNMP_FAILURE;
    }

    i4RetVal = Ip6ZoneCreateScopeZoneInfo
        ((UINT4) i4Fsipv6ScopeZoneIndexIfIndex,
         u1Scope, au1ZoneName, u1ZoneCreationStatus, i4ZoneId);

    if (IP6_FAILURE == i4RetVal)
    {
        return SNMP_FAILURE;
    }

    IncMsrForIpv6IfZoneMapTable (i4Fsipv6ScopeZoneIndexIfIndex, 's',
                                 pSetValFsipv6ScopeZoneIndex6,
                                 FsMIIpv6ScopeZoneIndex6,
                                 sizeof (FsMIIpv6ScopeZoneIndex6) /
                                 sizeof (UINT4), FALSE);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6ScopeZoneIndex7
 Input       :  The Indices
                Fsipv6ScopeZoneIndexIfIndex

                The Object 
                setValFsipv6ScopeZoneIndex7
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6ScopeZoneIndex7 (INT4 i4Fsipv6ScopeZoneIndexIfIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pSetValFsipv6ScopeZoneIndex7)
{
    tIp6IfZoneMapInfo  *pIp6IfZoneMapInfo = NULL;
    tIp6If             *pIf6 = NULL;
    UINT1               u1Scope = ADDR6_SCOPE_UNASSIGN7;
    UINT1               au1ZoneName[IP6_SCOPE_ZONE_NAME_LEN];
    UINT1               u1ZoneCreationStatus = IP6_ZONE_TYPE_MANUAL;
    INT4                i4RetVal;
    INT4                i4ZoneId = 0;

    MEMSET (au1ZoneName, 0, IP6_SCOPE_ZONE_NAME_LEN);

    pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6ScopeZoneIndexIfIndex);
    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    Ip6ScopeZoneCopy (au1ZoneName, pSetValFsipv6ScopeZoneIndex7->pu1_OctetList);

    pIp6IfZoneMapInfo =
        Ip6ZoneGetIfScopeZoneEntry (i4Fsipv6ScopeZoneIndexIfIndex, u1Scope);

    if (pIp6IfZoneMapInfo != NULL)
        return SNMP_SUCCESS;

    if (IP6_FAILURE ==
        Ip6ZoneGetZoneIdFromZoneName (au1ZoneName, &u1Scope, &i4ZoneId))
    {
        return SNMP_FAILURE;
    }

    i4RetVal = Ip6ZoneCreateScopeZoneInfo
        ((UINT4) i4Fsipv6ScopeZoneIndexIfIndex,
         u1Scope, au1ZoneName, u1ZoneCreationStatus, i4ZoneId);

    if (IP6_FAILURE == i4RetVal)
    {
        return SNMP_FAILURE;
    }

    IncMsrForIpv6IfZoneMapTable (i4Fsipv6ScopeZoneIndexIfIndex, 's',
                                 pSetValFsipv6ScopeZoneIndex7,
                                 FsMIIpv6ScopeZoneIndex7,
                                 sizeof (FsMIIpv6ScopeZoneIndex7) /
                                 sizeof (UINT4), FALSE);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6ScopeZoneIndexOrganizationLocal
 Input       :  The Indices
                Fsipv6ScopeZoneIndexIfIndex

                The Object 
                setValFsipv6ScopeZoneIndexOrganizationLocal
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6ScopeZoneIndexOrganizationLocal (INT4 i4Fsipv6ScopeZoneIndexIfIndex,
                                             tSNMP_OCTET_STRING_TYPE *
                                             pSetValFsipv6ScopeZoneIndexOrganizationLocal)
{
    tIp6IfZoneMapInfo  *pIp6IfZoneMapInfo = NULL;
    tIp6If             *pIf6 = NULL;
    UINT1               u1Scope = ADDR6_SCOPE_ORGLOCAL;
    UINT1               au1ZoneName[IP6_SCOPE_ZONE_NAME_LEN];
    UINT1               u1ZoneCreationStatus = IP6_ZONE_TYPE_MANUAL;
    INT4                i4RetVal;
    INT4                i4ZoneId = 0;

    pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6ScopeZoneIndexIfIndex);
    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (au1ZoneName, 0, IP6_SCOPE_ZONE_NAME_LEN);

    Ip6ScopeZoneCopy (au1ZoneName,
                      pSetValFsipv6ScopeZoneIndexOrganizationLocal->
                      pu1_OctetList);

    pIp6IfZoneMapInfo =
        Ip6ZoneGetIfScopeZoneEntry (i4Fsipv6ScopeZoneIndexIfIndex, u1Scope);

    if (pIp6IfZoneMapInfo != NULL)
        return SNMP_SUCCESS;

    if (IP6_FAILURE ==
        Ip6ZoneGetZoneIdFromZoneName (au1ZoneName, &u1Scope, &i4ZoneId))
    {
        return SNMP_FAILURE;
    }

    i4RetVal = Ip6ZoneCreateScopeZoneInfo
        ((UINT4) i4Fsipv6ScopeZoneIndexIfIndex,
         u1Scope, au1ZoneName, u1ZoneCreationStatus, i4ZoneId);

    if (IP6_FAILURE == i4RetVal)
    {
        return SNMP_FAILURE;
    }

    IncMsrForIpv6IfZoneMapTable (i4Fsipv6ScopeZoneIndexIfIndex, 's',
                                 pSetValFsipv6ScopeZoneIndexOrganizationLocal,
                                 FsMIIpv6ScopeZoneIndexOrganizationLocal,
                                 sizeof
                                 (FsMIIpv6ScopeZoneIndexOrganizationLocal) /
                                 sizeof (UINT4), FALSE);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6ScopeZoneIndex9
 Input       :  The Indices
                Fsipv6ScopeZoneIndexIfIndex

                The Object 
                setValFsipv6ScopeZoneIndex9
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6ScopeZoneIndex9 (INT4 i4Fsipv6ScopeZoneIndexIfIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pSetValFsipv6ScopeZoneIndex9)
{
    tIp6IfZoneMapInfo  *pIp6IfZoneMapInfo = NULL;
    tIp6If             *pIf6 = NULL;
    UINT1               u1Scope = ADDR6_SCOPE_UNASSIGN9;
    UINT1               au1ZoneName[IP6_SCOPE_ZONE_NAME_LEN];
    UINT1               u1ZoneCreationStatus = IP6_ZONE_TYPE_MANUAL;
    INT4                i4RetVal;
    INT4                i4ZoneId = 0;

    MEMSET (au1ZoneName, 0, IP6_SCOPE_ZONE_NAME_LEN);

    pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6ScopeZoneIndexIfIndex);
    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    Ip6ScopeZoneCopy (au1ZoneName, pSetValFsipv6ScopeZoneIndex9->pu1_OctetList);

    pIp6IfZoneMapInfo =
        Ip6ZoneGetIfScopeZoneEntry (i4Fsipv6ScopeZoneIndexIfIndex, u1Scope);

    if (pIp6IfZoneMapInfo != NULL)
        return SNMP_SUCCESS;

    if (IP6_FAILURE ==
        Ip6ZoneGetZoneIdFromZoneName (au1ZoneName, &u1Scope, &i4ZoneId))
    {
        return SNMP_FAILURE;
    }

    i4RetVal = Ip6ZoneCreateScopeZoneInfo
        ((UINT4) i4Fsipv6ScopeZoneIndexIfIndex,
         u1Scope, au1ZoneName, u1ZoneCreationStatus, i4ZoneId);

    if (IP6_FAILURE == i4RetVal)
    {
        return SNMP_FAILURE;
    }

    IncMsrForIpv6IfZoneMapTable (i4Fsipv6ScopeZoneIndexIfIndex, 's',
                                 pSetValFsipv6ScopeZoneIndex9,
                                 FsMIIpv6ScopeZoneIndex9,
                                 sizeof (FsMIIpv6ScopeZoneIndex9) /
                                 sizeof (UINT4), FALSE);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6ScopeZoneIndexA
 Input       :  The Indices
                Fsipv6ScopeZoneIndexIfIndex

                The Object 
                setValFsipv6ScopeZoneIndexA
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6ScopeZoneIndexA (INT4 i4Fsipv6ScopeZoneIndexIfIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pSetValFsipv6ScopeZoneIndexA)
{
    tIp6IfZoneMapInfo  *pIp6IfZoneMapInfo = NULL;
    tIp6If             *pIf6 = NULL;
    UINT1               u1Scope = ADDR6_SCOPE_UNASSIGNA;
    UINT1               au1ZoneName[IP6_SCOPE_ZONE_NAME_LEN];
    UINT1               u1ZoneCreationStatus = IP6_ZONE_TYPE_MANUAL;
    INT4                i4RetVal;
    INT4                i4ZoneId = 0;

    MEMSET (au1ZoneName, 0, IP6_SCOPE_ZONE_NAME_LEN);

    pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6ScopeZoneIndexIfIndex);
    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    Ip6ScopeZoneCopy (au1ZoneName, pSetValFsipv6ScopeZoneIndexA->pu1_OctetList);

    pIp6IfZoneMapInfo =
        Ip6ZoneGetIfScopeZoneEntry (i4Fsipv6ScopeZoneIndexIfIndex, u1Scope);

    if (pIp6IfZoneMapInfo != NULL)
        return SNMP_SUCCESS;

    if (IP6_FAILURE ==
        Ip6ZoneGetZoneIdFromZoneName (au1ZoneName, &u1Scope, &i4ZoneId))
    {
        return SNMP_FAILURE;
    }

    i4RetVal = Ip6ZoneCreateScopeZoneInfo
        ((UINT4) i4Fsipv6ScopeZoneIndexIfIndex,
         u1Scope, au1ZoneName, u1ZoneCreationStatus, i4ZoneId);

    if (IP6_FAILURE == i4RetVal)
    {
        return SNMP_FAILURE;
    }

    IncMsrForIpv6IfZoneMapTable (i4Fsipv6ScopeZoneIndexIfIndex, 's',
                                 pSetValFsipv6ScopeZoneIndexA,
                                 FsMIIpv6ScopeZoneIndexA,
                                 sizeof (FsMIIpv6ScopeZoneIndexA) /
                                 sizeof (UINT4), FALSE);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6ScopeZoneIndexB
 Input       :  The Indices
                Fsipv6ScopeZoneIndexIfIndex

                The Object 
                setValFsipv6ScopeZoneIndexB
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6ScopeZoneIndexB (INT4 i4Fsipv6ScopeZoneIndexIfIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pSetValFsipv6ScopeZoneIndexB)
{
    tIp6IfZoneMapInfo  *pIp6IfZoneMapInfo = NULL;
    tIp6If             *pIf6 = NULL;
    UINT1               u1Scope = ADDR6_SCOPE_UNASSIGNB;
    UINT1               au1ZoneName[IP6_SCOPE_ZONE_NAME_LEN];
    UINT1               u1ZoneCreationStatus = IP6_ZONE_TYPE_MANUAL;
    INT4                i4RetVal;
    INT4                i4ZoneId = 0;

    MEMSET (au1ZoneName, 0, IP6_SCOPE_ZONE_NAME_LEN);

    pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6ScopeZoneIndexIfIndex);
    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    Ip6ScopeZoneCopy (au1ZoneName, pSetValFsipv6ScopeZoneIndexB->pu1_OctetList);

    pIp6IfZoneMapInfo =
        Ip6ZoneGetIfScopeZoneEntry (i4Fsipv6ScopeZoneIndexIfIndex, u1Scope);

    if (pIp6IfZoneMapInfo != NULL)
        return SNMP_SUCCESS;

    if (IP6_FAILURE ==
        Ip6ZoneGetZoneIdFromZoneName (au1ZoneName, &u1Scope, &i4ZoneId))
    {
        return SNMP_FAILURE;
    }

    i4RetVal = Ip6ZoneCreateScopeZoneInfo
        ((UINT4) i4Fsipv6ScopeZoneIndexIfIndex,
         u1Scope, au1ZoneName, u1ZoneCreationStatus, i4ZoneId);

    if (IP6_FAILURE == i4RetVal)
    {
        return SNMP_FAILURE;
    }

    IncMsrForIpv6IfZoneMapTable (i4Fsipv6ScopeZoneIndexIfIndex, 's',
                                 pSetValFsipv6ScopeZoneIndexB,
                                 FsMIIpv6ScopeZoneIndexB,
                                 sizeof (FsMIIpv6ScopeZoneIndexB) /
                                 sizeof (UINT4), FALSE);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6ScopeZoneIndexC
 Input       :  The Indices
                Fsipv6ScopeZoneIndexIfIndex

                The Object 
                setValFsipv6ScopeZoneIndexC
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6ScopeZoneIndexC (INT4 i4Fsipv6ScopeZoneIndexIfIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pSetValFsipv6ScopeZoneIndexC)
{
    tIp6IfZoneMapInfo  *pIp6IfZoneMapInfo = NULL;
    tIp6If             *pIf6 = NULL;
    UINT1               u1Scope = ADDR6_SCOPE_UNASSIGNC;
    UINT1               au1ZoneName[IP6_SCOPE_ZONE_NAME_LEN];
    UINT1               u1ZoneCreationStatus = IP6_ZONE_TYPE_MANUAL;
    INT4                i4RetVal;
    INT4                i4ZoneId = 0;

    MEMSET (au1ZoneName, 0, IP6_SCOPE_ZONE_NAME_LEN);

    pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6ScopeZoneIndexIfIndex);
    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    Ip6ScopeZoneCopy (au1ZoneName, pSetValFsipv6ScopeZoneIndexC->pu1_OctetList);

    pIp6IfZoneMapInfo =
        Ip6ZoneGetIfScopeZoneEntry (i4Fsipv6ScopeZoneIndexIfIndex, u1Scope);

    if (pIp6IfZoneMapInfo != NULL)
        return SNMP_SUCCESS;

    if (IP6_FAILURE ==
        Ip6ZoneGetZoneIdFromZoneName (au1ZoneName, &u1Scope, &i4ZoneId))
    {
        return SNMP_FAILURE;
    }

    i4RetVal = Ip6ZoneCreateScopeZoneInfo
        ((UINT4) i4Fsipv6ScopeZoneIndexIfIndex,
         u1Scope, au1ZoneName, u1ZoneCreationStatus, i4ZoneId);

    if (IP6_FAILURE == i4RetVal)
    {
        return SNMP_FAILURE;
    }

    IncMsrForIpv6IfZoneMapTable (i4Fsipv6ScopeZoneIndexIfIndex, 's',
                                 pSetValFsipv6ScopeZoneIndexC,
                                 FsMIIpv6ScopeZoneIndexC,
                                 sizeof (FsMIIpv6ScopeZoneIndexC) /
                                 sizeof (UINT4), FALSE);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6ScopeZoneIndexD
 Input       :  The Indices
                Fsipv6ScopeZoneIndexIfIndex

                The Object 
                setValFsipv6ScopeZoneIndexD
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6ScopeZoneIndexD (INT4 i4Fsipv6ScopeZoneIndexIfIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pSetValFsipv6ScopeZoneIndexD)
{
    tIp6IfZoneMapInfo  *pIp6IfZoneMapInfo = NULL;
    tIp6If             *pIf6 = NULL;
    UINT1               u1Scope = ADDR6_SCOPE_UNASSIGND;
    UINT1               au1ZoneName[IP6_SCOPE_ZONE_NAME_LEN];
    UINT1               u1ZoneCreationStatus = IP6_ZONE_TYPE_MANUAL;
    INT4                i4ZoneId = 0;
    INT4                i4RetVal;

    MEMSET (au1ZoneName, 0, IP6_SCOPE_ZONE_NAME_LEN);
    pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6ScopeZoneIndexIfIndex);
    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    Ip6ScopeZoneCopy (au1ZoneName, pSetValFsipv6ScopeZoneIndexD->pu1_OctetList);

    pIp6IfZoneMapInfo =
        Ip6ZoneGetIfScopeZoneEntry (i4Fsipv6ScopeZoneIndexIfIndex, u1Scope);

    if (pIp6IfZoneMapInfo != NULL)
        return SNMP_SUCCESS;

    if (IP6_FAILURE ==
        Ip6ZoneGetZoneIdFromZoneName (au1ZoneName, &u1Scope, &i4ZoneId))
    {
        return SNMP_FAILURE;
    }

    i4RetVal = Ip6ZoneCreateScopeZoneInfo
        ((UINT4) i4Fsipv6ScopeZoneIndexIfIndex,
         u1Scope, au1ZoneName, u1ZoneCreationStatus, i4ZoneId);

    if (IP6_FAILURE == i4RetVal)
    {
        return SNMP_FAILURE;
    }

    IncMsrForIpv6IfZoneMapTable (i4Fsipv6ScopeZoneIndexIfIndex, 's',
                                 pSetValFsipv6ScopeZoneIndexD,
                                 FsMIIpv6ScopeZoneIndexD,
                                 sizeof (FsMIIpv6ScopeZoneIndexD) /
                                 sizeof (UINT4), FALSE);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6ScopeZoneIndexE
 Input       :  The Indices
                Fsipv6ScopeZoneIndexIfIndex

                The Object 
                setValFsipv6ScopeZoneIndexE
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6ScopeZoneIndexE (INT4 i4Fsipv6ScopeZoneIndexIfIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pSetValFsipv6ScopeZoneIndexE)
{
    tIp6IfZoneMapInfo  *pIp6IfZoneMapInfo = NULL;
    tIp6If             *pIf6 = NULL;
    UINT1               u1Scope = ADDR6_SCOPE_GLOBAL;
    UINT1               au1ZoneName[IP6_SCOPE_ZONE_NAME_LEN];
    UINT1               u1ZoneCreationStatus = IP6_ZONE_TYPE_MANUAL;
    INT4                i4ZoneId = 0;
    INT4                i4RetVal;

    MEMSET (au1ZoneName, 0, IP6_SCOPE_ZONE_NAME_LEN);

    pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6ScopeZoneIndexIfIndex);
    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    Ip6ScopeZoneCopy (au1ZoneName, pSetValFsipv6ScopeZoneIndexE->pu1_OctetList);

    pIp6IfZoneMapInfo =
        Ip6ZoneGetIfScopeZoneEntry (i4Fsipv6ScopeZoneIndexIfIndex, u1Scope);

    if (pIp6IfZoneMapInfo != NULL)
        return SNMP_SUCCESS;

    if (IP6_FAILURE ==
        Ip6ZoneGetZoneIdFromZoneName (au1ZoneName, &u1Scope, &i4ZoneId))
    {
        return SNMP_FAILURE;
    }

    i4RetVal = Ip6ZoneCreateScopeZoneInfo
        ((UINT4) i4Fsipv6ScopeZoneIndexIfIndex,
         u1Scope, au1ZoneName, u1ZoneCreationStatus, i4ZoneId);

    if (IP6_FAILURE == i4RetVal)
    {
        return SNMP_FAILURE;
    }

    IncMsrForIpv6IfZoneMapTable (i4Fsipv6ScopeZoneIndexIfIndex, 's',
                                 pSetValFsipv6ScopeZoneIndexE,
                                 FsMIIpv6ScopeZoneIndexE,
                                 sizeof (FsMIIpv6ScopeZoneIndexE) /
                                 sizeof (UINT4), FALSE);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6IfScopeZoneRowStatus
 Input       :  The Indices
                Fsipv6ScopeZoneIndexIfIndex

                The Object 
                setValFsipv6IfScopeZoneRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6IfScopeZoneRowStatus (INT4 i4Fsipv6ScopeZoneIndexIfIndex,
                                  INT4 i4SetValFsipv6IfScopeZoneRowStatus)
{
    tIp6If             *pIf6 = NULL;
    tIp6IfZoneMapInfo  *pIp6ZoneMapInfo = NULL;
    UINT1               u1Scope = 0;

    pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6ScopeZoneIndexIfIndex);

    if (pIf6 == NULL)
    {
        CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
        return SNMP_FAILURE;
    }

    switch (i4SetValFsipv6IfScopeZoneRowStatus)
    {
        case IP6_ZONE_ROW_STATUS_NOT_IN_SERVICE:
        case IP6_ZONE_ROW_STATUS_CREATE_AND_WAIT:
            pIf6->u1ZoneRowStatus = IP6_ZONE_ROW_STATUS_NOT_IN_SERVICE;
            break;

        case IP6_ZONE_ROW_STATUS_CREATE_AND_GO:
        case IP6_ZONE_ROW_STATUS_ACTIVE:
            pIf6->u1ZoneRowStatus = IP6_ZONE_ROW_STATUS_ACTIVE;
            break;

        case IP6_ZONE_ROW_STATUS_DESTROY:
            for (u1Scope = ADDR6_SCOPE_INTLOCAL; u1Scope <= ADDR6_SCOPE_GLOBAL;
                 u1Scope++)
            {
                pIp6ZoneMapInfo =
                    Ip6ZoneGetIfScopeZoneEntry (i4Fsipv6ScopeZoneIndexIfIndex,
                                                u1Scope);
                if ((pIp6ZoneMapInfo != NULL)
                    && (pIp6ZoneMapInfo->u1ConfigStatus != IP6_ZONE_TYPE_AUTO))
                {
                    Ip6ScopeZoneDelete ((UINT4) i4Fsipv6ScopeZoneIndexIfIndex,
                                        u1Scope, pIp6ZoneMapInfo->i4ZoneId);
                }

            }

            break;
    }
    IncMsrForIpv6IfZoneMapTable (i4Fsipv6ScopeZoneIndexIfIndex, 'i',
                                 &i4SetValFsipv6IfScopeZoneRowStatus,
                                 FsMIIpv6IfScopeZoneRowStatus,
                                 sizeof (FsMIIpv6IfScopeZoneRowStatus) /
                                 sizeof (UINT4), TRUE);

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Fsipv6ScopeZoneIndexInterfaceLocal
 Input       :  The Indices
                Fsipv6ScopeZoneIndexIfIndex

                The Object 
                testValFsipv6ScopeZoneIndexInterfaceLocal
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhTestv2Fsipv6ScopeZoneIndexInterfaceLocal
    (UINT4 *pu4ErrorCode,
     INT4 i4Fsipv6ScopeZoneIndexIfIndex,
     tSNMP_OCTET_STRING_TYPE * pTestValFsipv6ScopeZoneIndexInterfaceLocal)
{
    UINT1               u1Scope = ADDR6_SCOPE_INTLOCAL;
    UINT1               au1ZoneName[IP6_SCOPE_ZONE_NAME_LEN];
    UINT1               u1ZoneCreationStatus = IP6_ZONE_TYPE_MANUAL;

    MEMSET (au1ZoneName, 0, IP6_SCOPE_ZONE_NAME_LEN);
    if (pTestValFsipv6ScopeZoneIndexInterfaceLocal->i4_Length >=
        IP6_SCOPE_ZONE_NAME_LEN)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    Ip6ScopeZoneCopy (au1ZoneName,
                      pTestValFsipv6ScopeZoneIndexInterfaceLocal->
                      pu1_OctetList);

    if (SNMP_SUCCESS == Ip6ZoneTestScopeZoneEntry (pu4ErrorCode,
                                                   (UINT4)
                                                   i4Fsipv6ScopeZoneIndexIfIndex,
                                                   u1Scope, au1ZoneName,
                                                   u1ZoneCreationStatus))
    {
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6ScopeZoneIndexLinkLocal
 Input       :  The Indices
                Fsipv6ScopeZoneIndexIfIndex

                The Object 
                testValFsipv6ScopeZoneIndexLinkLocal
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6ScopeZoneIndexLinkLocal (UINT4 *pu4ErrorCode,
                                        INT4 i4Fsipv6ScopeZoneIndexIfIndex,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pTestValFsipv6ScopeZoneIndexLinkLocal)
{
    UINT1               u1Scope = ADDR6_SCOPE_LLOCAL;
    UINT1               au1ZoneName[IP6_SCOPE_ZONE_NAME_LEN];
    UINT1               u1ZoneCreationStatus = IP6_ZONE_TYPE_MANUAL;

    MEMSET (au1ZoneName, 0, IP6_SCOPE_ZONE_NAME_LEN);
    if (pTestValFsipv6ScopeZoneIndexLinkLocal->i4_Length >=
        IP6_SCOPE_ZONE_NAME_LEN)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    Ip6ScopeZoneCopy (au1ZoneName,
                      pTestValFsipv6ScopeZoneIndexLinkLocal->pu1_OctetList);

    if (SNMP_SUCCESS == Ip6ZoneTestScopeZoneEntry (pu4ErrorCode,
                                                   (UINT4)
                                                   i4Fsipv6ScopeZoneIndexIfIndex,
                                                   u1Scope, au1ZoneName,
                                                   u1ZoneCreationStatus))
    {
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6ScopeZoneIndex3
 Input       :  The Indices
                Fsipv6ScopeZoneIndexIfIndex

                The Object 
                testValFsipv6ScopeZoneIndex3
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6ScopeZoneIndex3 (UINT4 *pu4ErrorCode,
                                INT4 i4Fsipv6ScopeZoneIndexIfIndex,
                                tSNMP_OCTET_STRING_TYPE *
                                pTestValFsipv6ScopeZoneIndex3)
{
    UINT1               u1Scope = ADDR6_SCOPE_SUBNETLOCAL;
    UINT1               au1ZoneName[IP6_SCOPE_ZONE_NAME_LEN];
    UINT1               u1ZoneCreationStatus = IP6_ZONE_TYPE_MANUAL;

    MEMSET (au1ZoneName, 0, IP6_SCOPE_ZONE_NAME_LEN);
    if (pTestValFsipv6ScopeZoneIndex3->i4_Length >= IP6_SCOPE_ZONE_NAME_LEN)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    Ip6ScopeZoneCopy (au1ZoneName,
                      pTestValFsipv6ScopeZoneIndex3->pu1_OctetList);

    if (SNMP_SUCCESS == Ip6ZoneTestScopeZoneEntry (pu4ErrorCode,
                                                   (UINT4)
                                                   i4Fsipv6ScopeZoneIndexIfIndex,
                                                   u1Scope, au1ZoneName,
                                                   u1ZoneCreationStatus))
    {
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6ScopeZoneIndexAdminLocal
 Input       :  The Indices
                Fsipv6ScopeZoneIndexIfIndex

                The Object 
                testValFsipv6ScopeZoneIndexAdminLocal
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6ScopeZoneIndexAdminLocal (UINT4 *pu4ErrorCode,
                                         INT4 i4Fsipv6ScopeZoneIndexIfIndex,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pTestValFsipv6ScopeZoneIndexAdminLocal)
{
    UINT1               u1Scope = ADDR6_SCOPE_ADMINLOCAL;
    UINT1               au1ZoneName[IP6_SCOPE_ZONE_NAME_LEN];
    UINT1               u1ZoneCreationStatus = IP6_ZONE_TYPE_MANUAL;

    MEMSET (au1ZoneName, 0, IP6_SCOPE_ZONE_NAME_LEN);
    if (pTestValFsipv6ScopeZoneIndexAdminLocal->i4_Length >=
        IP6_SCOPE_ZONE_NAME_LEN)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    Ip6ScopeZoneCopy (au1ZoneName,
                      pTestValFsipv6ScopeZoneIndexAdminLocal->pu1_OctetList);

    if (SNMP_SUCCESS == Ip6ZoneTestScopeZoneEntry (pu4ErrorCode,
                                                   (UINT4)
                                                   i4Fsipv6ScopeZoneIndexIfIndex,
                                                   u1Scope, au1ZoneName,
                                                   u1ZoneCreationStatus))
    {
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6ScopeZoneIndexSiteLocal
 Input       :  The Indices
                Fsipv6ScopeZoneIndexIfIndex

                The Object 
                testValFsipv6ScopeZoneIndexSiteLocal
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6ScopeZoneIndexSiteLocal (UINT4 *pu4ErrorCode,
                                        INT4 i4Fsipv6ScopeZoneIndexIfIndex,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pTestValFsipv6ScopeZoneIndexSiteLocal)
{
    UINT1               u1Scope = ADDR6_SCOPE_SITELOCAL;
    UINT1               au1ZoneName[IP6_SCOPE_ZONE_NAME_LEN];
    UINT1               u1ZoneCreationStatus = IP6_ZONE_TYPE_MANUAL;

    MEMSET (au1ZoneName, 0, IP6_SCOPE_ZONE_NAME_LEN);
    if (pTestValFsipv6ScopeZoneIndexSiteLocal->i4_Length >=
        IP6_SCOPE_ZONE_NAME_LEN)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    Ip6ScopeZoneCopy (au1ZoneName,
                      pTestValFsipv6ScopeZoneIndexSiteLocal->pu1_OctetList);

    if (SNMP_SUCCESS == Ip6ZoneTestScopeZoneEntry (pu4ErrorCode,
                                                   (UINT4)
                                                   i4Fsipv6ScopeZoneIndexIfIndex,
                                                   u1Scope, au1ZoneName,
                                                   u1ZoneCreationStatus))
    {
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6ScopeZoneIndex6
 Input       :  The Indices
                Fsipv6ScopeZoneIndexIfIndex

                The Object 
                testValFsipv6ScopeZoneIndex6
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6ScopeZoneIndex6 (UINT4 *pu4ErrorCode,
                                INT4 i4Fsipv6ScopeZoneIndexIfIndex,
                                tSNMP_OCTET_STRING_TYPE *
                                pTestValFsipv6ScopeZoneIndex6)
{
    UINT1               u1Scope = ADDR6_SCOPE_UNASSIGN6;
    UINT1               au1ZoneName[IP6_SCOPE_ZONE_NAME_LEN];
    UINT1               u1ZoneCreationStatus = IP6_ZONE_TYPE_MANUAL;

    MEMSET (au1ZoneName, 0, IP6_SCOPE_ZONE_NAME_LEN);
    if (pTestValFsipv6ScopeZoneIndex6->i4_Length >= IP6_SCOPE_ZONE_NAME_LEN)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    Ip6ScopeZoneCopy (au1ZoneName,
                      pTestValFsipv6ScopeZoneIndex6->pu1_OctetList);

    if (SNMP_SUCCESS == Ip6ZoneTestScopeZoneEntry (pu4ErrorCode,
                                                   (UINT4)
                                                   i4Fsipv6ScopeZoneIndexIfIndex,
                                                   u1Scope, au1ZoneName,
                                                   u1ZoneCreationStatus))
    {
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6ScopeZoneIndex7
 Input       :  The Indices
                Fsipv6ScopeZoneIndexIfIndex

                The Object 
                testValFsipv6ScopeZoneIndex7
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6ScopeZoneIndex7 (UINT4 *pu4ErrorCode,
                                INT4 i4Fsipv6ScopeZoneIndexIfIndex,
                                tSNMP_OCTET_STRING_TYPE *
                                pTestValFsipv6ScopeZoneIndex7)
{
    UINT1               u1Scope = ADDR6_SCOPE_UNASSIGN7;
    UINT1               au1ZoneName[IP6_SCOPE_ZONE_NAME_LEN];
    UINT1               u1ZoneCreationStatus = IP6_ZONE_TYPE_MANUAL;

    MEMSET (au1ZoneName, 0, IP6_SCOPE_ZONE_NAME_LEN);
    if (pTestValFsipv6ScopeZoneIndex7->i4_Length >= IP6_SCOPE_ZONE_NAME_LEN)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }
    Ip6ScopeZoneCopy (au1ZoneName,
                      pTestValFsipv6ScopeZoneIndex7->pu1_OctetList);

    if (SNMP_SUCCESS == Ip6ZoneTestScopeZoneEntry (pu4ErrorCode,
                                                   (UINT4)
                                                   i4Fsipv6ScopeZoneIndexIfIndex,
                                                   u1Scope, au1ZoneName,
                                                   u1ZoneCreationStatus))
    {
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6ScopeZoneIndexOrganizationLocal
 Input       :  The Indices
                Fsipv6ScopeZoneIndexIfIndex

                The Object 
                testValFsipv6ScopeZoneIndexOrganizationLocal
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6ScopeZoneIndexOrganizationLocal (UINT4 *pu4ErrorCode,
                                                INT4
                                                i4Fsipv6ScopeZoneIndexIfIndex,
                                                tSNMP_OCTET_STRING_TYPE *
                                                pTestValFsipv6ScopeZoneIndexOrganizationLocal)
{
    UINT1               u1Scope = ADDR6_SCOPE_ORGLOCAL;
    UINT1               au1ZoneName[IP6_SCOPE_ZONE_NAME_LEN];
    UINT1               u1ZoneCreationStatus = IP6_ZONE_TYPE_MANUAL;

    MEMSET (au1ZoneName, 0, IP6_SCOPE_ZONE_NAME_LEN);
    if (pTestValFsipv6ScopeZoneIndexOrganizationLocal->i4_Length >=
        IP6_SCOPE_ZONE_NAME_LEN)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    Ip6ScopeZoneCopy (au1ZoneName,
                      pTestValFsipv6ScopeZoneIndexOrganizationLocal->
                      pu1_OctetList);

    if (SNMP_SUCCESS == Ip6ZoneTestScopeZoneEntry (pu4ErrorCode,
                                                   (UINT4)
                                                   i4Fsipv6ScopeZoneIndexIfIndex,
                                                   u1Scope, au1ZoneName,
                                                   u1ZoneCreationStatus))
    {
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6ScopeZoneIndex9
 Input       :  The Indices
                Fsipv6ScopeZoneIndexIfIndex

                The Object 
                testValFsipv6ScopeZoneIndex9
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6ScopeZoneIndex9 (UINT4 *pu4ErrorCode,
                                INT4 i4Fsipv6ScopeZoneIndexIfIndex,
                                tSNMP_OCTET_STRING_TYPE *
                                pTestValFsipv6ScopeZoneIndex9)
{
    UINT1               u1Scope = ADDR6_SCOPE_UNASSIGN9;
    UINT1               au1ZoneName[IP6_SCOPE_ZONE_NAME_LEN];
    UINT1               u1ZoneCreationStatus = IP6_ZONE_TYPE_MANUAL;

    MEMSET (au1ZoneName, 0, IP6_SCOPE_ZONE_NAME_LEN);
    if (pTestValFsipv6ScopeZoneIndex9->i4_Length >= IP6_SCOPE_ZONE_NAME_LEN)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    Ip6ScopeZoneCopy (au1ZoneName,
                      pTestValFsipv6ScopeZoneIndex9->pu1_OctetList);

    if (SNMP_SUCCESS == Ip6ZoneTestScopeZoneEntry (pu4ErrorCode,
                                                   (UINT4)
                                                   i4Fsipv6ScopeZoneIndexIfIndex,
                                                   u1Scope, au1ZoneName,
                                                   u1ZoneCreationStatus))
    {
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6ScopeZoneIndexA
 Input       :  The Indices
                Fsipv6ScopeZoneIndexIfIndex

                The Object 
                testValFsipv6ScopeZoneIndexA
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6ScopeZoneIndexA (UINT4 *pu4ErrorCode,
                                INT4 i4Fsipv6ScopeZoneIndexIfIndex,
                                tSNMP_OCTET_STRING_TYPE *
                                pTestValFsipv6ScopeZoneIndexA)
{
    UINT1               u1Scope = ADDR6_SCOPE_UNASSIGNA;
    UINT1               au1ZoneName[IP6_SCOPE_ZONE_NAME_LEN];
    UINT1               u1ZoneCreationStatus = IP6_ZONE_TYPE_MANUAL;

    MEMSET (au1ZoneName, 0, IP6_SCOPE_ZONE_NAME_LEN);
    if (pTestValFsipv6ScopeZoneIndexA->i4_Length >= IP6_SCOPE_ZONE_NAME_LEN)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    Ip6ScopeZoneCopy (au1ZoneName,
                      pTestValFsipv6ScopeZoneIndexA->pu1_OctetList);

    if (SNMP_SUCCESS == Ip6ZoneTestScopeZoneEntry (pu4ErrorCode,
                                                   (UINT4)
                                                   i4Fsipv6ScopeZoneIndexIfIndex,
                                                   u1Scope, au1ZoneName,
                                                   u1ZoneCreationStatus))
    {
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6ScopeZoneIndexB
 Input       :  The Indices
                Fsipv6ScopeZoneIndexIfIndex

                The Object 
                testValFsipv6ScopeZoneIndexB
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6ScopeZoneIndexB (UINT4 *pu4ErrorCode,
                                INT4 i4Fsipv6ScopeZoneIndexIfIndex,
                                tSNMP_OCTET_STRING_TYPE *
                                pTestValFsipv6ScopeZoneIndexB)
{
    UINT1               u1Scope = ADDR6_SCOPE_UNASSIGNB;
    UINT1               au1ZoneName[IP6_SCOPE_ZONE_NAME_LEN];
    UINT1               u1ZoneCreationStatus = IP6_ZONE_TYPE_MANUAL;

    MEMSET (au1ZoneName, 0, IP6_SCOPE_ZONE_NAME_LEN);
    if (pTestValFsipv6ScopeZoneIndexB->i4_Length >= IP6_SCOPE_ZONE_NAME_LEN)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    Ip6ScopeZoneCopy (au1ZoneName,
                      pTestValFsipv6ScopeZoneIndexB->pu1_OctetList);

    if (SNMP_SUCCESS == Ip6ZoneTestScopeZoneEntry (pu4ErrorCode,
                                                   (UINT4)
                                                   i4Fsipv6ScopeZoneIndexIfIndex,
                                                   u1Scope, au1ZoneName,
                                                   u1ZoneCreationStatus))
    {
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6ScopeZoneIndexC
 Input       :  The Indices
                Fsipv6ScopeZoneIndexIfIndex

                The Object 
                testValFsipv6ScopeZoneIndexC
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6ScopeZoneIndexC (UINT4 *pu4ErrorCode,
                                INT4 i4Fsipv6ScopeZoneIndexIfIndex,
                                tSNMP_OCTET_STRING_TYPE *
                                pTestValFsipv6ScopeZoneIndexC)
{
    UINT1               u1Scope = ADDR6_SCOPE_UNASSIGNC;
    UINT1               au1ZoneName[IP6_SCOPE_ZONE_NAME_LEN];
    UINT1               u1ZoneCreationStatus = IP6_ZONE_TYPE_MANUAL;

    MEMSET (au1ZoneName, 0, IP6_SCOPE_ZONE_NAME_LEN);
    if (pTestValFsipv6ScopeZoneIndexC->i4_Length >= IP6_SCOPE_ZONE_NAME_LEN)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }
    Ip6ScopeZoneCopy (au1ZoneName,
                      pTestValFsipv6ScopeZoneIndexC->pu1_OctetList);

    if (SNMP_SUCCESS == Ip6ZoneTestScopeZoneEntry (pu4ErrorCode,
                                                   (UINT4)
                                                   i4Fsipv6ScopeZoneIndexIfIndex,
                                                   u1Scope, au1ZoneName,
                                                   u1ZoneCreationStatus))
    {
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6ScopeZoneIndexD
 Input       :  The Indices
                Fsipv6ScopeZoneIndexIfIndex

                The Object 
                testValFsipv6ScopeZoneIndexD
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6ScopeZoneIndexD (UINT4 *pu4ErrorCode,
                                INT4 i4Fsipv6ScopeZoneIndexIfIndex,
                                tSNMP_OCTET_STRING_TYPE *
                                pTestValFsipv6ScopeZoneIndexD)
{
    UINT1               u1Scope = ADDR6_SCOPE_UNASSIGND;
    UINT1               au1ZoneName[IP6_SCOPE_ZONE_NAME_LEN];
    UINT1               u1ZoneCreationStatus = IP6_ZONE_TYPE_MANUAL;

    MEMSET (au1ZoneName, 0, IP6_SCOPE_ZONE_NAME_LEN);
    if (pTestValFsipv6ScopeZoneIndexD->i4_Length >= IP6_SCOPE_ZONE_NAME_LEN)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }
    Ip6ScopeZoneCopy (au1ZoneName,
                      pTestValFsipv6ScopeZoneIndexD->pu1_OctetList);

    if (SNMP_SUCCESS == Ip6ZoneTestScopeZoneEntry (pu4ErrorCode,
                                                   (UINT4)
                                                   i4Fsipv6ScopeZoneIndexIfIndex,
                                                   u1Scope, au1ZoneName,
                                                   u1ZoneCreationStatus))
    {
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6ScopeZoneIndexE
 Input       :  The Indices
                Fsipv6ScopeZoneIndexIfIndex

                The Object 
                testValFsipv6ScopeZoneIndexE
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6ScopeZoneIndexE (UINT4 *pu4ErrorCode,
                                INT4 i4Fsipv6ScopeZoneIndexIfIndex,
                                tSNMP_OCTET_STRING_TYPE *
                                pTestValFsipv6ScopeZoneIndexE)
{
    UINT1               u1Scope = ADDR6_SCOPE_GLOBAL;
    UINT1               au1ZoneName[IP6_SCOPE_ZONE_NAME_LEN];
    UINT1               u1ZoneCreationStatus = IP6_ZONE_TYPE_MANUAL;

    MEMSET (au1ZoneName, 0, IP6_SCOPE_ZONE_NAME_LEN);
    if (pTestValFsipv6ScopeZoneIndexE->i4_Length >= IP6_SCOPE_ZONE_NAME_LEN)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    Ip6ScopeZoneCopy (au1ZoneName,
                      pTestValFsipv6ScopeZoneIndexE->pu1_OctetList);

    if (SNMP_SUCCESS == Ip6ZoneTestScopeZoneEntry (pu4ErrorCode,
                                                   (UINT4)
                                                   i4Fsipv6ScopeZoneIndexIfIndex,
                                                   u1Scope, au1ZoneName,
                                                   u1ZoneCreationStatus))
    {
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6IfScopeZoneRowStatus
 Input       :  The Indices
                Fsipv6ScopeZoneIndexIfIndex

                The Object 
                testValFsipv6IfScopeZoneRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6IfScopeZoneRowStatus (UINT4 *pu4ErrorCode,
                                     INT4 i4Fsipv6ScopeZoneIndexIfIndex,
                                     INT4 i4TestValFsipv6IfScopeZoneRowStatus)
{
    tIp6If             *pIf6 = NULL;
    pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6ScopeZoneIndexIfIndex);

    if (pIf6 == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
        return SNMP_FAILURE;
    }

    if ((i4TestValFsipv6IfScopeZoneRowStatus < IP6_ZONE_ROW_STATUS_ACTIVE) ||
        (i4TestValFsipv6IfScopeZoneRowStatus > IP6_ZONE_ROW_STATUS_DESTROY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_INVALID_ROW_STATUS_FOR_ZONE);
        return SNMP_FAILURE;

    }
    if (pIf6->u1ZoneRowStatus == IP6_ZONE_ROW_STATUS_ACTIVE)
    {
        if ((i4TestValFsipv6IfScopeZoneRowStatus ==
             IP6_ZONE_ROW_STATUS_CREATE_AND_GO)
            || (i4TestValFsipv6IfScopeZoneRowStatus ==
                IP6_ZONE_ROW_STATUS_CREATE_AND_WAIT))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_IP6_INVALID_ROW_STATUS_FOR_ZONE);
            return SNMP_FAILURE;
        }
    }
    if ((i4TestValFsipv6IfScopeZoneRowStatus ==
         IP6_ZONE_ROW_STATUS_NOT_IN_SERVICE)
        && (pIf6->u1ZoneRowStatus != IP6_ZONE_ROW_STATUS_ACTIVE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_IP6_INVALID_ROW_STATUS_FOR_ZONE);
        return SNMP_FAILURE;
    }
    if ((i4TestValFsipv6IfScopeZoneRowStatus == IP6_ZONE_ROW_STATUS_DESTROY) &&
        (pIf6->u1ZoneRowStatus != IP6_ZONE_ROW_STATUS_ACTIVE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_IP6_INVALID_ROW_STATUS_FOR_ZONE);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Fsipv6IfScopeZoneMapTable
 Input       :  The Indices
                Fsipv6ScopeZoneIndexIfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsipv6IfScopeZoneMapTable (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Fsipv6ScopeZoneTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsipv6ScopeZoneTable
 Input       :  The Indices
                Fsipv6ScopeZoneName
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 
     
     
     
     
     
     
     
    nmhValidateIndexInstanceFsipv6ScopeZoneTable
    (tSNMP_OCTET_STRING_TYPE * pFsipv6ScopeZoneName)
{
    tIp6ScopeZoneInfo  *pScopeZoneInfo = NULL;
    tIp6ScopeZoneInfo   Ip6ScopeZoneInfo;
    INT4                i4ZoneId = 0;
    UINT4               u4ContextId = 0;
    UINT4               u4ErrorCode = 0;
    UINT1               au1ZoneName[IP6_SCOPE_ZONE_NAME_LEN];
    UINT1               u1Scope = 0;
    UINT2               u2Len = 0;

    MEMSET (au1ZoneName, 0, IP6_SCOPE_ZONE_NAME_LEN);
    u2Len =
        (UINT2) (STRLEN (pFsipv6ScopeZoneName->pu1_OctetList) <
                 (IP6_SCOPE_ZONE_NAME_LEN -
                  1) ? STRLEN (pFsipv6ScopeZoneName->
                               pu1_OctetList) : IP6_SCOPE_ZONE_NAME_LEN - 1);
    STRNCPY (au1ZoneName, pFsipv6ScopeZoneName->pu1_OctetList, u2Len);
    au1ZoneName[u2Len] = '\0';

    if (gIp6GblInfo.pIp6CurrCxt == NULL)
        return SNMP_FAILURE;
    u4ContextId = gIp6GblInfo.pIp6CurrCxt->u4ContextId;

    if (IP6_FAILURE == Ip6ZoneTestZoneNameandId (&u4ErrorCode, au1ZoneName))
    {
        return SNMP_FAILURE;
    }

    Ip6ZoneGetZoneIdFromZoneName (au1ZoneName, &u1Scope, &i4ZoneId);

    Ip6ScopeZoneInfo.u1Scope = u1Scope;
    Ip6ScopeZoneInfo.i4ZoneId = i4ZoneId;
    Ip6ScopeZoneInfo.u4ContextId = u4ContextId;
    pScopeZoneInfo = RBTreeGet (gIp6GblInfo.ScopeZoneTree,
                                (tRBElem *) & Ip6ScopeZoneInfo);
    if (pScopeZoneInfo == NULL)
    {
        CLI_SET_ERR (CLI_IP6_ZONE_DOES_NOT_EXIST);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsipv6ScopeZoneTable
 Input       :  The Indices
                Fsipv6ScopeZoneName
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsipv6ScopeZoneTable (tSNMP_OCTET_STRING_TYPE *
                                      pFsipv6ScopeZoneName)
{
    tIp6ScopeZoneInfo  *pScopeZoneInfo = NULL;
    tIp6ScopeZoneInfo   Ip6ScopeZoneInfo;

    if (gIp6GblInfo.pIp6CurrCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    MEMSET (&Ip6ScopeZoneInfo, 0, sizeof (tIp6ScopeZoneInfo));

    Ip6ScopeZoneInfo.u4ContextId = gIp6GblInfo.pIp6CurrCxt->u4ContextId;

    pScopeZoneInfo =
        (tIp6ScopeZoneInfo *) RBTreeGetNext (gIp6GblInfo.ScopeZoneTree,
                                             (tRBElem *) & Ip6ScopeZoneInfo,
                                             NULL);

    if ((pScopeZoneInfo != NULL) &&
        (pScopeZoneInfo->u4ContextId == gIp6GblInfo.pIp6CurrCxt->u4ContextId))
    {
        Ip6ScopeZoneCopy (pFsipv6ScopeZoneName->pu1_OctetList,
                          pScopeZoneInfo->au1ZoneName);
        pFsipv6ScopeZoneName->i4_Length = STRLEN (pScopeZoneInfo->au1ZoneName);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsipv6ScopeZoneTable
 Input       :  The Indices
                Fsipv6ScopeZoneName
                nextFsipv6ScopeZoneName
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsipv6ScopeZoneTable (tSNMP_OCTET_STRING_TYPE *
                                     pFsipv6ScopeZoneName,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pNextFsipv6ScopeZoneName)
{
    tIp6ScopeZoneInfo  *pNextScopeZoneInfo = NULL;
    tIp6ScopeZoneInfo   Ip6ScopeZoneInfo;
    INT4                i4ZoneId = 0;
    UINT1               u1Scope = 0;
    UINT1               au1ZoneName[IP6_SCOPE_ZONE_NAME_LEN];
    UINT2               u2Len = 0;

    if (gIp6GblInfo.pIp6CurrCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    MEMSET (au1ZoneName, 0, IP6_SCOPE_ZONE_NAME_LEN);
    MEMSET (&Ip6ScopeZoneInfo, 0, sizeof (tIp6ScopeZoneInfo));
    u2Len =
        (UINT2) (STRLEN (pFsipv6ScopeZoneName->pu1_OctetList) <
                 (IP6_SCOPE_ZONE_NAME_LEN -
                  1) ? STRLEN (pFsipv6ScopeZoneName->
                               pu1_OctetList) : IP6_SCOPE_ZONE_NAME_LEN - 1);
    STRNCPY (au1ZoneName, pFsipv6ScopeZoneName->pu1_OctetList, u2Len);
    au1ZoneName[u2Len] = '\0';
    Ip6ZoneGetZoneIdFromZoneName (au1ZoneName, &u1Scope, &i4ZoneId);
    Ip6ScopeZoneInfo.u4ContextId = gIp6GblInfo.pIp6CurrCxt->u4ContextId;
    Ip6ScopeZoneInfo.i4ZoneId = i4ZoneId;
    Ip6ScopeZoneInfo.u1Scope = u1Scope;

    pNextScopeZoneInfo =
        (tIp6ScopeZoneInfo *) RBTreeGetNext (gIp6GblInfo.ScopeZoneTree,
                                             (tRBElem *) & Ip6ScopeZoneInfo,
                                             NULL);

    if ((NULL != pNextScopeZoneInfo) &&
        (pNextScopeZoneInfo->u4ContextId ==
         gIp6GblInfo.pIp6CurrCxt->u4ContextId))
    {
        pNextFsipv6ScopeZoneName->i4_Length =
            STRLEN (pNextScopeZoneInfo->au1ZoneName);
        STRCPY (pNextFsipv6ScopeZoneName->pu1_OctetList,
                pNextScopeZoneInfo->au1ZoneName);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsipv6ScopeZoneIndex
 Input       :  The Indices
                Fsipv6ScopeZoneName

                The Object 
                retValFsipv6ScopeZoneIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6ScopeZoneIndex (tSNMP_OCTET_STRING_TYPE * pFsipv6ScopeZoneName,
                            UINT4 *pu4RetValFsipv6ScopeZoneIndex)
{
    tIp6ScopeZoneInfo  *pScopeZoneInfo = NULL;
    tIp6ScopeZoneInfo   Ip6ScopeZoneInfo;
    UINT2               u2Len = 0;

    MEMSET (&Ip6ScopeZoneInfo, IP6_ZERO, sizeof (tIp6ScopeZoneInfo));
    u2Len =
        (UINT2) (STRLEN (pFsipv6ScopeZoneName->pu1_OctetList) <
                 (IP6_SCOPE_ZONE_NAME_LEN -
                  1) ? STRLEN (pFsipv6ScopeZoneName->
                               pu1_OctetList) : IP6_SCOPE_ZONE_NAME_LEN - 1);
    STRNCPY (Ip6ScopeZoneInfo.au1ZoneName, pFsipv6ScopeZoneName->pu1_OctetList,
             u2Len);
    Ip6ScopeZoneInfo.au1ZoneName[u2Len] = '\0';
    if (gIp6GblInfo.pIp6CurrCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    Ip6ScopeZoneInfo.u4ContextId = gIp6GblInfo.pIp6CurrCxt->u4ContextId;

    pScopeZoneInfo =
        Ip6ZoneGetRBTreeScopeZoneEntry (Ip6ScopeZoneInfo.au1ZoneName,
                                        Ip6ScopeZoneInfo.u4ContextId);

    if (NULL != pScopeZoneInfo)
    {
        *pu4RetValFsipv6ScopeZoneIndex = pScopeZoneInfo->i4ZoneIndex;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsipv6ScopeZoneCreationStatus
 Input       :  The Indices
                Fsipv6ScopeZoneName

                The Object 
                retValFsipv6ScopeZoneCreationStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6ScopeZoneCreationStatus (tSNMP_OCTET_STRING_TYPE *
                                     pFsipv6ScopeZoneName,
                                     INT4
                                     *pi4RetValFsipv6ScopeZoneCreationStatus)
{
    tIp6ScopeZoneInfo  *pScopeZoneInfo = NULL;
    tIp6ScopeZoneInfo   Ip6ScopeZoneInfo;
    UINT2               u2Len = 0;

    MEMSET (&Ip6ScopeZoneInfo, IP6_ZERO, sizeof (tIp6ScopeZoneInfo));
    u2Len =
        (UINT2) (STRLEN (pFsipv6ScopeZoneName->pu1_OctetList) <
                 (IP6_SCOPE_ZONE_NAME_LEN -
                  1) ? STRLEN (pFsipv6ScopeZoneName->
                               pu1_OctetList) : IP6_SCOPE_ZONE_NAME_LEN - 1);
    STRNCPY (Ip6ScopeZoneInfo.au1ZoneName, pFsipv6ScopeZoneName->pu1_OctetList,
             u2Len);
    Ip6ScopeZoneInfo.au1ZoneName[u2Len] = '\0';
    if (gIp6GblInfo.pIp6CurrCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    Ip6ScopeZoneInfo.u4ContextId = gIp6GblInfo.pIp6CurrCxt->u4ContextId;

    pScopeZoneInfo =
        Ip6ZoneGetRBTreeScopeZoneEntry (Ip6ScopeZoneInfo.au1ZoneName,
                                        Ip6ScopeZoneInfo.u4ContextId);

    if (NULL != pScopeZoneInfo)
    {
        *pi4RetValFsipv6ScopeZoneCreationStatus =
            pScopeZoneInfo->u1CreateStatus;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsipv6ScopeZoneInterfaceList
 Input       :  The Indices
                Fsipv6ScopeZoneName

                The Object 
                retValFsipv6ScopeZoneInterfaceList
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6ScopeZoneInterfaceList (tSNMP_OCTET_STRING_TYPE *
                                    pFsipv6ScopeZoneName,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pRetValFsipv6ScopeZoneInterfaceList)
{
    tIp6ScopeZoneInfo  *pScopeZoneInfo = NULL;
    tIp6ScopeZoneInfo   Ip6ScopeZoneInfo;
    UINT2               u2Len = 0;

    MEMSET (&Ip6ScopeZoneInfo, IP6_ZERO, sizeof (tIp6ScopeZoneInfo));
    u2Len =
        (UINT2) (STRLEN (pFsipv6ScopeZoneName->pu1_OctetList) <
                 (IP6_SCOPE_ZONE_NAME_LEN -
                  1) ? STRLEN (pFsipv6ScopeZoneName->
                               pu1_OctetList) : IP6_SCOPE_ZONE_NAME_LEN - 1);
    STRNCPY (Ip6ScopeZoneInfo.au1ZoneName, pFsipv6ScopeZoneName->pu1_OctetList,
             u2Len);
    Ip6ScopeZoneInfo.au1ZoneName[u2Len] = '\0';

    if (gIp6GblInfo.pIp6CurrCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    Ip6ScopeZoneInfo.u4ContextId = gIp6GblInfo.pIp6CurrCxt->u4ContextId;

    pScopeZoneInfo =
        Ip6ZoneGetRBTreeScopeZoneEntry (Ip6ScopeZoneInfo.au1ZoneName,
                                        Ip6ScopeZoneInfo.u4ContextId);

    if (NULL != pScopeZoneInfo)
    {
        /*MEMSET (pRetValFsipv6ScopeZoneInterfaceList->pu1_OctetList, 0, VLAN_PORT_LIST_SIZE); */
        MEMSET (pRetValFsipv6ScopeZoneInterfaceList->pu1_OctetList, 0,
                IP6_MAX_ZONE_INT_LIST_SIZE);
        IP6_ADD_ZONE_INT_LIST (pRetValFsipv6ScopeZoneInterfaceList->
                               pu1_OctetList, pScopeZoneInfo->InterfaceList);
        pRetValFsipv6ScopeZoneInterfaceList->i4_Length =
            IP6_MAX_ZONE_INT_LIST_SIZE;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IsDefaultScopeZone
 Input       :  The Indices
                Fsipv6ScopeZoneName

                The Object 
                retValFsipv6IsDefaultScopeZone
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IsDefaultScopeZone (tSNMP_OCTET_STRING_TYPE * pFsipv6ScopeZoneName,
                                INT4 *pi4RetValFsipv6IsDefaultScopeZone)
{
    tIp6ScopeZoneInfo  *pScopeZoneInfo = NULL;
    tIp6ScopeZoneInfo   Ip6ScopeZoneInfo;
    UINT2               u2Len = 0;

    MEMSET (&Ip6ScopeZoneInfo, IP6_ZERO, sizeof (tIp6ScopeZoneInfo));
    u2Len =
        (UINT2) (STRLEN (pFsipv6ScopeZoneName->pu1_OctetList) <
                 (IP6_SCOPE_ZONE_NAME_LEN -
                  1) ? STRLEN (pFsipv6ScopeZoneName->
                               pu1_OctetList) : IP6_SCOPE_ZONE_NAME_LEN - 1);
    STRNCPY (Ip6ScopeZoneInfo.au1ZoneName, pFsipv6ScopeZoneName->pu1_OctetList,
             u2Len);
    Ip6ScopeZoneInfo.au1ZoneName[u2Len] = '\0';

    if (gIp6GblInfo.pIp6CurrCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    pScopeZoneInfo =
        Ip6ZoneGetRBTreeScopeZoneEntry (Ip6ScopeZoneInfo.au1ZoneName,
                                        Ip6ScopeZoneInfo.u4ContextId);

    if (NULL != pScopeZoneInfo)
    {
        *pi4RetValFsipv6IsDefaultScopeZone = pScopeZoneInfo->u1IsDefaultZone;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetFsipv6IsDefaultScopeZone
 Input       :  The Indices
                Fsipv6ScopeZoneName

                The Object 
                setValFsipv6IsDefaultScopeZone
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6IsDefaultScopeZone (tSNMP_OCTET_STRING_TYPE * pFsipv6ScopeZoneName,
                                INT4 i4SetValFsipv6IsDefaultScopeZone)
{
    tIp6ScopeZoneInfo  *pScopeZoneInfo = NULL;
    tIp6ScopeZoneInfo   Ip6ScopeZoneInfo;
    UINT1               u1Scope = ADDR6_SCOPE_INVALID;
    INT4                i4ZoneId = IP6_ZONE_INVALID;
    UINT2               u2Len = 0;

    MEMSET (&Ip6ScopeZoneInfo, IP6_ZERO, sizeof (tIp6ScopeZoneInfo));
    u2Len =
        (UINT2) (STRLEN (pFsipv6ScopeZoneName->pu1_OctetList) <
                 (IP6_SCOPE_ZONE_NAME_LEN -
                  1) ? STRLEN (pFsipv6ScopeZoneName->
                               pu1_OctetList) : IP6_SCOPE_ZONE_NAME_LEN - 1);
    STRNCPY (Ip6ScopeZoneInfo.au1ZoneName, pFsipv6ScopeZoneName->pu1_OctetList,
             u2Len);
    Ip6ScopeZoneInfo.au1ZoneName[u2Len] = '\0';

    if (gIp6GblInfo.pIp6CurrCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    Ip6ScopeZoneInfo.u4ContextId = gIp6GblInfo.pIp6CurrCxt->u4ContextId;

    pScopeZoneInfo =
        Ip6ZoneGetRBTreeScopeZoneEntry (Ip6ScopeZoneInfo.au1ZoneName,
                                        Ip6ScopeZoneInfo.u4ContextId);

    if (pScopeZoneInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    Ip6ZoneGetZoneIdFromZoneName (Ip6ScopeZoneInfo.au1ZoneName,
                                  &u1Scope, &i4ZoneId);

    if (i4SetValFsipv6IsDefaultScopeZone == IP6_ZONE_TRUE)
    {
        Ip6ZoneResetOtherDefaultZoneEntry (u1Scope, i4ZoneId,
                                           gIp6GblInfo.pIp6CurrCxt->
                                           u4ContextId);
    }
    pScopeZoneInfo->u1IsDefaultZone = (UINT1) i4SetValFsipv6IsDefaultScopeZone;

    IncMsrForIpv6ScopeZoneTable (gIp6GblInfo.pIp6CurrCxt->u4ContextId,
                                 pFsipv6ScopeZoneName,
                                 i4SetValFsipv6IsDefaultScopeZone,
                                 FsMIIpv6IsDefaultScopeZone,
                                 sizeof (FsMIIpv6IsDefaultScopeZone) /
                                 sizeof (UINT4), FALSE);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6IsDefaultScopeZone
 Input       :  The Indices
                Fsipv6ScopeZoneName

                The Object 
                testValFsipv6IsDefaultScopeZone
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6IsDefaultScopeZone (UINT4 *pu4ErrorCode,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsipv6ScopeZoneName,
                                   INT4 i4TestValFsipv6IsDefaultScopeZone)
{
    tIp6ScopeZoneInfo  *pScopeZoneInfo = NULL;
    tIp6ScopeZoneInfo   Ip6ScopeZoneInfo;
    INT4                i4ZoneId = 0;
    UINT1               au1ZoneName[IP6_SCOPE_ZONE_NAME_LEN];
    UINT4               u4ContextId = 0;
    UINT1               u1Scope = 0;
    UINT2               u2Len = 0;

    if (gIp6GblInfo.pIp6CurrCxt == NULL)
        return SNMP_FAILURE;

    u4ContextId = gIp6GblInfo.pIp6CurrCxt->u4ContextId;

    MEMSET (au1ZoneName, 0, IP6_SCOPE_ZONE_NAME_LEN);
    MEMSET (&Ip6ScopeZoneInfo, 0, sizeof (tIp6ScopeZoneInfo));
    u2Len =
        (UINT2) (STRLEN (pFsipv6ScopeZoneName->pu1_OctetList) <
                 (IP6_SCOPE_ZONE_NAME_LEN -
                  1) ? STRLEN (pFsipv6ScopeZoneName->
                               pu1_OctetList) : IP6_SCOPE_ZONE_NAME_LEN - 1);
    STRNCPY (au1ZoneName, pFsipv6ScopeZoneName->pu1_OctetList, u2Len);
    au1ZoneName[u2Len] = '\0';
    if (IP6_FAILURE == Ip6ZoneTestZoneNameandId (pu4ErrorCode, au1ZoneName))
    {
        CLI_SET_ERR (CLI_IP6_INVALID_ZONE_NAME);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    Ip6ZoneGetZoneIdFromZoneName (au1ZoneName, &u1Scope, &i4ZoneId);

    Ip6ScopeZoneInfo.u1Scope = u1Scope;
    Ip6ScopeZoneInfo.i4ZoneId = i4ZoneId;
    Ip6ScopeZoneInfo.u4ContextId = u4ContextId;
    pScopeZoneInfo = RBTreeGet (gIp6GblInfo.ScopeZoneTree,
                                (tRBElem *) & Ip6ScopeZoneInfo);

    if (pScopeZoneInfo == NULL)
    {
        CLI_SET_ERR (CLI_IP6_ZONE_DOES_NOT_EXIST);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if ((i4TestValFsipv6IsDefaultScopeZone != IP6_ZONE_TRUE) &&
        (i4TestValFsipv6IsDefaultScopeZone != IP6_ZONE_FALSE))
    {
        CLI_SET_ERR (CLI_IP6_INVALID_DEF_ZONE_VAL);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Fsipv6ScopeZoneTable
 Input       :  The Indices
                Fsipv6ScopeZoneName
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsipv6ScopeZoneTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Fsipv6SENDSentPktStatsTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsipv6SENDSentPktStatsTable
 Input       :  The Indices
                Fsipv6IfIndex
                Fsipv6SENDSentPktType
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsipv6SENDSentPktStatsTable (INT4 i4Fsipv6IfIndex,
                                                     INT4
                                                     i4Fsipv6SENDSentPktType)
{
    if (Ip6ifEntryExists ((UINT4) i4Fsipv6IfIndex) == IP6_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if (Ip6ifGetEntry ((UINT2) i4Fsipv6IfIndex) == NULL)
    {
        return (SNMP_FAILURE);
    }

    if (i4Fsipv6SENDSentPktType < 1 || i4Fsipv6SENDSentPktType > 7)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsipv6SENDSentPktStatsTable
 Input       :  The Indices
                Fsipv6IfIndex
                Fsipv6SENDSentPktType
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsipv6SENDSentPktStatsTable (INT4 *pi4Fsipv6IfIndex,
                                             INT4 *pi4Fsipv6SENDSentPktType)
{
    *pi4Fsipv6IfIndex = 1;
    *pi4Fsipv6SENDSentPktType = 1;

    if (Ip6ifEntryExists ((UINT4) *pi4Fsipv6IfIndex) == IP6_SUCCESS)
    {
        return (SNMP_SUCCESS);
    }

    if (Ip6ifGetNextIndex ((UINT4 *) pi4Fsipv6IfIndex) == IP6_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsipv6SENDSentPktStatsTable
 Input       :  The Indices
                Fsipv6IfIndex
                nextFsipv6IfIndex
                Fsipv6SENDSentPktType
                nextFsipv6SENDSentPktType
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsipv6SENDSentPktStatsTable (INT4 i4Fsipv6IfIndex,
                                            INT4 *pi4NextFsipv6IfIndex,
                                            INT4 i4Fsipv6SENDSentPktType,
                                            INT4 *pi4NextFsipv6SENDSentPktType)
{
    *pi4NextFsipv6IfIndex = i4Fsipv6IfIndex;
    if (Ip6ifEntryExists ((UINT4) i4Fsipv6IfIndex) == IP6_SUCCESS)
    {

        if (i4Fsipv6SENDSentPktType >= 1 && i4Fsipv6SENDSentPktType <= 6)
        {
            *pi4NextFsipv6SENDSentPktType = i4Fsipv6SENDSentPktType + 1;
            *pi4NextFsipv6IfIndex = i4Fsipv6IfIndex;
            return (SNMP_SUCCESS);
        }
    }
    if (Ip6ifGetNextIndex ((UINT4 *) pi4NextFsipv6IfIndex) != IP6_FAILURE)
    {
        *pi4NextFsipv6SENDSentPktType = 1;
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsipv6SENDSentCgaOptPkts
 Input       :  The Indices
                Fsipv6IfIndex
                Fsipv6SENDSentPktType

                The Object 
                retValFsipv6SENDSentCgaOptPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SENDSentCgaOptPkts (INT4 i4Fsipv6IfIndex,
                                INT4 i4Fsipv6SENDSentPktType,
                                UINT4 *pu4RetValFsipv6SENDSentCgaOptPkts)
{
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);
    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    if (i4Fsipv6SENDSentPktType > 7 || i4Fsipv6SENDSentPktType < 1)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsipv6SENDSentCgaOptPkts =
        pIf6->pIfIcmp6Stats->Nd6SecOutStats[i4Fsipv6SENDSentPktType].
        u4CgaOptPkts;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6SENDSentCertOptPkts
 Input       :  The Indices
                Fsipv6IfIndex
                Fsipv6SENDSentPktType

                The Object 
                retValFsipv6SENDSentCertOptPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SENDSentCertOptPkts (INT4 i4Fsipv6IfIndex,
                                 INT4 i4Fsipv6SENDSentPktType,
                                 UINT4 *pu4RetValFsipv6SENDSentCertOptPkts)
{
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);
    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    if (i4Fsipv6SENDSentPktType > 7 || i4Fsipv6SENDSentPktType < 1)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsipv6SENDSentCertOptPkts =
        pIf6->pIfIcmp6Stats->Nd6SecOutStats[i4Fsipv6SENDSentPktType].
        u4CertOptPkts;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6SENDSentMtuOptPkts
 Input       :  The Indices
                Fsipv6IfIndex
                Fsipv6SENDSentPktType

                The Object 
                retValFsipv6SENDSentMtuOptPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SENDSentMtuOptPkts (INT4 i4Fsipv6IfIndex,
                                INT4 i4Fsipv6SENDSentPktType,
                                UINT4 *pu4RetValFsipv6SENDSentMtuOptPkts)
{
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);
    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    if (i4Fsipv6SENDSentPktType > 7 || i4Fsipv6SENDSentPktType < 1)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsipv6SENDSentMtuOptPkts =
        pIf6->pIfIcmp6Stats->Nd6SecOutStats[i4Fsipv6SENDSentPktType].
        u4MtuOptPkts;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6SENDSentNonceOptPkts
 Input       :  The Indices
                Fsipv6IfIndex
                Fsipv6SENDSentPktType

                The Object 
                retValFsipv6SENDSentNonceOptPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SENDSentNonceOptPkts (INT4 i4Fsipv6IfIndex,
                                  INT4 i4Fsipv6SENDSentPktType,
                                  UINT4 *pu4RetValFsipv6SENDSentNonceOptPkts)
{
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);
    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    if (i4Fsipv6SENDSentPktType > 7 || i4Fsipv6SENDSentPktType < 1)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsipv6SENDSentNonceOptPkts =
        pIf6->pIfIcmp6Stats->Nd6SecOutStats[i4Fsipv6SENDSentPktType].
        u4NonceOptPkts;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6SENDSentPrefixOptPkts
 Input       :  The Indices
                Fsipv6IfIndex
                Fsipv6SENDSentPktType

                The Object 
                retValFsipv6SENDSentPrefixOptPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SENDSentPrefixOptPkts (INT4 i4Fsipv6IfIndex,
                                   INT4 i4Fsipv6SENDSentPktType,
                                   UINT4 *pu4RetValFsipv6SENDSentPrefixOptPkts)
{
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);
    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    if (i4Fsipv6SENDSentPktType > 7 || i4Fsipv6SENDSentPktType < 1)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsipv6SENDSentPrefixOptPkts =
        pIf6->pIfIcmp6Stats->Nd6SecOutStats[i4Fsipv6SENDSentPktType].
        u4PrefixOptPkts;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6SENDSentRedirHdrPkts
 Input       :  The Indices
                Fsipv6IfIndex
                Fsipv6SENDSentPktType

                The Object 
                retValFsipv6SENDSentRedirHdrPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SENDSentRedirHdrPkts (INT4 i4Fsipv6IfIndex,
                                  INT4 i4Fsipv6SENDSentPktType,
                                  UINT4 *pu4RetValFsipv6SENDSentRedirHdrPkts)
{
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);
    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    if (i4Fsipv6SENDSentPktType > 7 || i4Fsipv6SENDSentPktType < 1)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsipv6SENDSentRedirHdrPkts =
        pIf6->pIfIcmp6Stats->Nd6SecOutStats[i4Fsipv6SENDSentPktType].
        u4RedirHrPkts;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6SENDSentRsaOptPkts
 Input       :  The Indices
                Fsipv6IfIndex
                Fsipv6SENDSentPktType

                The Object 
                retValFsipv6SENDSentRsaOptPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SENDSentRsaOptPkts (INT4 i4Fsipv6IfIndex,
                                INT4 i4Fsipv6SENDSentPktType,
                                UINT4 *pu4RetValFsipv6SENDSentRsaOptPkts)
{
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);
    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    if (i4Fsipv6SENDSentPktType > 7 || i4Fsipv6SENDSentPktType < 1)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsipv6SENDSentRsaOptPkts =
        pIf6->pIfIcmp6Stats->Nd6SecOutStats[i4Fsipv6SENDSentPktType].
        u4RsaOptPkts;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6SENDSentSrcLinkAddrPkts
 Input       :  The Indices
                Fsipv6IfIndex
                Fsipv6SENDSentPktType

                The Object 
                retValFsipv6SENDSentSrcLinkAddrPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SENDSentSrcLinkAddrPkts (INT4 i4Fsipv6IfIndex,
                                     INT4 i4Fsipv6SENDSentPktType,
                                     UINT4
                                     *pu4RetValFsipv6SENDSentSrcLinkAddrPkts)
{
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);
    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    if (i4Fsipv6SENDSentPktType > 7 || i4Fsipv6SENDSentPktType < 1)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsipv6SENDSentSrcLinkAddrPkts =
        pIf6->pIfIcmp6Stats->Nd6SecOutStats[i4Fsipv6SENDSentPktType].
        u4SrcLinkAddrPkts;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6SENDSentTgtLinkAddrPkts
 Input       :  The Indices
                Fsipv6IfIndex
                Fsipv6SENDSentPktType

                The Object 
                retValFsipv6SENDSentTgtLinkAddrPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SENDSentTgtLinkAddrPkts (INT4 i4Fsipv6IfIndex,
                                     INT4 i4Fsipv6SENDSentPktType,
                                     UINT4
                                     *pu4RetValFsipv6SENDSentTgtLinkAddrPkts)
{
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);
    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    if (i4Fsipv6SENDSentPktType > 7 || i4Fsipv6SENDSentPktType < 1)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsipv6SENDSentTgtLinkAddrPkts =
        pIf6->pIfIcmp6Stats->Nd6SecOutStats[i4Fsipv6SENDSentPktType].
        u4TgtLinkAddrPkts;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6SENDSentTaOptPkts
 Input       :  The Indices
                Fsipv6IfIndex
                Fsipv6SENDSentPktType

                The Object 
                retValFsipv6SENDSentTaOptPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SENDSentTaOptPkts (INT4 i4Fsipv6IfIndex,
                               INT4 i4Fsipv6SENDSentPktType,
                               UINT4 *pu4RetValFsipv6SENDSentTaOptPkts)
{
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);
    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    if (i4Fsipv6SENDSentPktType > 7 || i4Fsipv6SENDSentPktType < 1)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsipv6SENDSentTaOptPkts =
        pIf6->pIfIcmp6Stats->Nd6SecOutStats[i4Fsipv6SENDSentPktType].
        u4TaOptPkts;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6SENDSentTimeStampOptPkts
 Input       :  The Indices
                Fsipv6IfIndex
                Fsipv6SENDSentPktType

                The Object 
                retValFsipv6SENDSentTimeStampOptPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SENDSentTimeStampOptPkts (INT4 i4Fsipv6IfIndex,
                                      INT4 i4Fsipv6SENDSentPktType,
                                      UINT4
                                      *pu4RetValFsipv6SENDSentTimeStampOptPkts)
{
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);
    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    if (i4Fsipv6SENDSentPktType > 7 || i4Fsipv6SENDSentPktType < 1)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsipv6SENDSentTimeStampOptPkts =
        pIf6->pIfIcmp6Stats->Nd6SecOutStats[i4Fsipv6SENDSentPktType].
        u4TimeStampOptPkts;

    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Fsipv6SENDRcvPktStatsTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsipv6SENDRcvPktStatsTable
 Input       :  The Indices
                Fsipv6IfIndex
                Fsipv6SENDRcvPktType
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsipv6SENDRcvPktStatsTable (INT4 i4Fsipv6IfIndex,
                                                    INT4 i4Fsipv6SENDRcvPktType)
{
    if (Ip6ifEntryExists ((UINT4) i4Fsipv6IfIndex) == IP6_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if (Ip6ifGetEntry ((UINT2) i4Fsipv6IfIndex) == NULL)
    {
        return (SNMP_FAILURE);
    }

    if (i4Fsipv6SENDRcvPktType < 1 || i4Fsipv6SENDRcvPktType > 7)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsipv6SENDRcvPktStatsTable
 Input       :  The Indices
                Fsipv6IfIndex
                Fsipv6SENDRcvPktType
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsipv6SENDRcvPktStatsTable (INT4 *pi4Fsipv6IfIndex,
                                            INT4 *pi4Fsipv6SENDRcvPktType)
{
    *pi4Fsipv6IfIndex = 1;
    *pi4Fsipv6SENDRcvPktType = 1;

    if (Ip6ifEntryExists ((UINT4) *pi4Fsipv6IfIndex) == IP6_SUCCESS)
    {
        return (SNMP_SUCCESS);
    }

    if (Ip6ifGetNextIndex ((UINT4 *) pi4Fsipv6IfIndex) == IP6_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsipv6SENDRcvPktStatsTable
 Input       :  The Indices
                Fsipv6IfIndex
                nextFsipv6IfIndex
                Fsipv6SENDRcvPktType
                nextFsipv6SENDRcvPktType
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsipv6SENDRcvPktStatsTable (INT4 i4Fsipv6IfIndex,
                                           INT4 *pi4NextFsipv6IfIndex,
                                           INT4 i4Fsipv6SENDRcvPktType,
                                           INT4 *pi4NextFsipv6SENDRcvPktType)
{
    *pi4NextFsipv6IfIndex = i4Fsipv6IfIndex;
    if (Ip6ifEntryExists ((UINT4) i4Fsipv6IfIndex) == IP6_SUCCESS)
    {
        if (i4Fsipv6SENDRcvPktType >= 1 && i4Fsipv6SENDRcvPktType <= 6)
        {
            *pi4NextFsipv6SENDRcvPktType = i4Fsipv6SENDRcvPktType + 1;
            *pi4NextFsipv6IfIndex = i4Fsipv6IfIndex;
            return (SNMP_SUCCESS);
        }
    }
    if (Ip6ifGetNextIndex ((UINT4 *) pi4NextFsipv6IfIndex) != IP6_FAILURE)
    {
        *pi4NextFsipv6SENDRcvPktType = 1;
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsipv6SENDRcvCgaOptPkts
 Input       :  The Indices
                Fsipv6IfIndex
                Fsipv6SENDRcvPktType

                The Object 
                retValFsipv6SENDRcvCgaOptPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SENDRcvCgaOptPkts (INT4 i4Fsipv6IfIndex,
                               INT4 i4Fsipv6SENDRcvPktType,
                               UINT4 *pu4RetValFsipv6SENDRcvCgaOptPkts)
{
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);
    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    if (i4Fsipv6SENDRcvPktType > 7 || i4Fsipv6SENDRcvPktType < 1)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsipv6SENDRcvCgaOptPkts =
        pIf6->pIfIcmp6Stats->Nd6SecInStats[i4Fsipv6SENDRcvPktType].u4CgaOptPkts;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6SENDRcvCertOptPkts
 Input       :  The Indices
                Fsipv6IfIndex
                Fsipv6SENDRcvPktType

                The Object 
                retValFsipv6SENDRcvCertOptPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SENDRcvCertOptPkts (INT4 i4Fsipv6IfIndex,
                                INT4 i4Fsipv6SENDRcvPktType,
                                UINT4 *pu4RetValFsipv6SENDRcvCertOptPkts)
{
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);
    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    if (i4Fsipv6SENDRcvPktType > 7 || i4Fsipv6SENDRcvPktType < 1)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsipv6SENDRcvCertOptPkts =
        pIf6->pIfIcmp6Stats->Nd6SecInStats[i4Fsipv6SENDRcvPktType].
        u4CertOptPkts;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsipv6SENDRcvMtuOptPkts
 Input       :  The Indices
                Fsipv6IfIndex
                Fsipv6SENDRcvPktType

                The Object 
                retValFsipv6SENDRcvMtuOptPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SENDRcvMtuOptPkts (INT4 i4Fsipv6IfIndex,
                               INT4 i4Fsipv6SENDRcvPktType,
                               UINT4 *pu4RetValFsipv6SENDRcvMtuOptPkts)
{
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);
    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    if (i4Fsipv6SENDRcvPktType > 7 || i4Fsipv6SENDRcvPktType < 1)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsipv6SENDRcvMtuOptPkts =
        pIf6->pIfIcmp6Stats->Nd6SecInStats[i4Fsipv6SENDRcvPktType].u4MtuOptPkts;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6SENDRcvNonceOptPkts
 Input       :  The Indices
                Fsipv6IfIndex
                Fsipv6SENDRcvPktType

                The Object 
                retValFsipv6SENDRcvNonceOptPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SENDRcvNonceOptPkts (INT4 i4Fsipv6IfIndex,
                                 INT4 i4Fsipv6SENDRcvPktType,
                                 UINT4 *pu4RetValFsipv6SENDRcvNonceOptPkts)
{
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);
    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    if (i4Fsipv6SENDRcvPktType > 7 || i4Fsipv6SENDRcvPktType < 1)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsipv6SENDRcvNonceOptPkts =
        pIf6->pIfIcmp6Stats->Nd6SecInStats[i4Fsipv6SENDRcvPktType].
        u4NonceOptPkts;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6SENDRcvPrefixOptPkts
 Input       :  The Indices
                Fsipv6IfIndex
                Fsipv6SENDRcvPktType

                The Object 
                retValFsipv6SENDRcvPrefixOptPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SENDRcvPrefixOptPkts (INT4 i4Fsipv6IfIndex,
                                  INT4 i4Fsipv6SENDRcvPktType,
                                  UINT4 *pu4RetValFsipv6SENDRcvPrefixOptPkts)
{
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);
    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    if (i4Fsipv6SENDRcvPktType > 7 || i4Fsipv6SENDRcvPktType < 1)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsipv6SENDRcvPrefixOptPkts =
        pIf6->pIfIcmp6Stats->Nd6SecInStats[i4Fsipv6SENDRcvPktType].
        u4PrefixOptPkts;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6SENDRcvRedirHdrPkts
 Input       :  The Indices
                Fsipv6IfIndex
                Fsipv6SENDRcvPktType

                The Object 
                retValFsipv6SENDRcvRedirHdrPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SENDRcvRedirHdrPkts (INT4 i4Fsipv6IfIndex,
                                 INT4 i4Fsipv6SENDRcvPktType,
                                 UINT4 *pu4RetValFsipv6SENDRcvRedirHdrPkts)
{
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);
    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    if (i4Fsipv6SENDRcvPktType > 7 || i4Fsipv6SENDRcvPktType < 1)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsipv6SENDRcvRedirHdrPkts =
        pIf6->pIfIcmp6Stats->Nd6SecInStats[i4Fsipv6SENDRcvPktType].
        u4RedirHrPkts;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6SENDRcvRsaOptPkts
 Input       :  The Indices
                Fsipv6IfIndex
                Fsipv6SENDRcvPktType

                The Object 
                retValFsipv6SENDRcvRsaOptPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SENDRcvRsaOptPkts (INT4 i4Fsipv6IfIndex,
                               INT4 i4Fsipv6SENDRcvPktType,
                               UINT4 *pu4RetValFsipv6SENDRcvRsaOptPkts)
{
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);
    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    if (i4Fsipv6SENDRcvPktType > 7 || i4Fsipv6SENDRcvPktType < 1)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsipv6SENDRcvRsaOptPkts =
        pIf6->pIfIcmp6Stats->Nd6SecInStats[i4Fsipv6SENDRcvPktType].u4RsaOptPkts;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6SENDRcvSrcLinkAddrPkts
 Input       :  The Indices
                Fsipv6IfIndex
                Fsipv6SENDRcvPktType

                The Object 
                retValFsipv6SENDRcvSrcLinkAddrPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SENDRcvSrcLinkAddrPkts (INT4 i4Fsipv6IfIndex,
                                    INT4 i4Fsipv6SENDRcvPktType,
                                    UINT4
                                    *pu4RetValFsipv6SENDRcvSrcLinkAddrPkts)
{
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);
    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    if (i4Fsipv6SENDRcvPktType > 7 || i4Fsipv6SENDRcvPktType < 1)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsipv6SENDRcvSrcLinkAddrPkts =
        pIf6->pIfIcmp6Stats->Nd6SecInStats[i4Fsipv6SENDRcvPktType].
        u4SrcLinkAddrPkts;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6SENDRcvTgtLinkAddrPkts
 Input       :  The Indices
                Fsipv6IfIndex
                Fsipv6SENDRcvPktType

                The Object 
                retValFsipv6SENDRcvTgtLinkAddrPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SENDRcvTgtLinkAddrPkts (INT4 i4Fsipv6IfIndex,
                                    INT4 i4Fsipv6SENDRcvPktType,
                                    UINT4
                                    *pu4RetValFsipv6SENDRcvTgtLinkAddrPkts)
{
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);
    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    if (i4Fsipv6SENDRcvPktType > 7 || i4Fsipv6SENDRcvPktType < 1)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsipv6SENDRcvTgtLinkAddrPkts =
        pIf6->pIfIcmp6Stats->Nd6SecInStats[i4Fsipv6SENDRcvPktType].
        u4TgtLinkAddrPkts;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6SENDRcvTaOptPkts
 Input       :  The Indices
                Fsipv6IfIndex
                Fsipv6SENDRcvPktType

                The Object 
                retValFsipv6SENDRcvTaOptPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SENDRcvTaOptPkts (INT4 i4Fsipv6IfIndex,
                              INT4 i4Fsipv6SENDRcvPktType,
                              UINT4 *pu4RetValFsipv6SENDRcvTaOptPkts)
{
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);
    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    if (i4Fsipv6SENDRcvPktType > 7 || i4Fsipv6SENDRcvPktType < 1)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsipv6SENDRcvTaOptPkts =
        pIf6->pIfIcmp6Stats->Nd6SecInStats[i4Fsipv6SENDRcvPktType].u4TaOptPkts;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6SENDRcvTimeStampOptPkts
 Input       :  The Indices
                Fsipv6IfIndex
                Fsipv6SENDRcvPktType

                The Object 
                retValFsipv6SENDRcvTimeStampOptPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SENDRcvTimeStampOptPkts (INT4 i4Fsipv6IfIndex,
                                     INT4 i4Fsipv6SENDRcvPktType,
                                     UINT4
                                     *pu4RetValFsipv6SENDRcvTimeStampOptPkts)
{
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);
    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    if (i4Fsipv6SENDRcvPktType > 7 || i4Fsipv6SENDRcvPktType < 1)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsipv6SENDRcvTimeStampOptPkts =
        pIf6->pIfIcmp6Stats->Nd6SecInStats[i4Fsipv6SENDRcvPktType].
        u4TimeStampOptPkts;

    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Fsipv6SENDDrpPktStatsTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsipv6SENDDrpPktStatsTable
 Input       :  The Indices
                Fsipv6IfIndex
                Fsipv6SENDDrpPktType
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsipv6SENDDrpPktStatsTable (INT4 i4Fsipv6IfIndex,
                                                    INT4 i4Fsipv6SENDDrpPktType)
{
    if (Ip6ifEntryExists ((UINT4) i4Fsipv6IfIndex) == IP6_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if (Ip6ifGetEntry ((UINT2) i4Fsipv6IfIndex) == NULL)
    {
        return (SNMP_FAILURE);
    }

    if (i4Fsipv6SENDDrpPktType < 1 || i4Fsipv6SENDDrpPktType > 7)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsipv6SENDDrpPktStatsTable
 Input       :  The Indices
                Fsipv6IfIndex
                Fsipv6SENDDrpPktType
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsipv6SENDDrpPktStatsTable (INT4 *pi4Fsipv6IfIndex,
                                            INT4 *pi4Fsipv6SENDDrpPktType)
{
    *pi4Fsipv6IfIndex = 1;
    *pi4Fsipv6SENDDrpPktType = 1;

    if (Ip6ifEntryExists ((UINT4) *pi4Fsipv6IfIndex) == IP6_SUCCESS)
    {
        return (SNMP_SUCCESS);
    }

    if (Ip6ifGetNextIndex ((UINT4 *) pi4Fsipv6IfIndex) == IP6_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsipv6SENDDrpPktStatsTable
 Input       :  The Indices
                Fsipv6IfIndex
                nextFsipv6IfIndex
                Fsipv6SENDDrpPktType
                nextFsipv6SENDDrpPktType
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsipv6SENDDrpPktStatsTable (INT4 i4Fsipv6IfIndex,
                                           INT4 *pi4NextFsipv6IfIndex,
                                           INT4 i4Fsipv6SENDDrpPktType,
                                           INT4 *pi4NextFsipv6SENDDrpPktType)
{

    *pi4NextFsipv6IfIndex = i4Fsipv6IfIndex;
    if (Ip6ifEntryExists ((UINT4) i4Fsipv6IfIndex) == IP6_SUCCESS)
    {

        if (i4Fsipv6SENDDrpPktType >= 1 && i4Fsipv6SENDDrpPktType <= 6)
        {
            *pi4NextFsipv6SENDDrpPktType = i4Fsipv6SENDDrpPktType + 1;
            *pi4NextFsipv6IfIndex = i4Fsipv6IfIndex;
            return (SNMP_SUCCESS);
        }
    }
    if (Ip6ifGetNextIndex ((UINT4 *) pi4NextFsipv6IfIndex) != IP6_FAILURE)
    {
        *pi4NextFsipv6SENDDrpPktType = 1;
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsipv6SENDDrpCgaOptPkts
 Input       :  The Indices
                Fsipv6IfIndex
                Fsipv6SENDDrpPktType

                The Object 
                retValFsipv6SENDDrpCgaOptPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SENDDrpCgaOptPkts (INT4 i4Fsipv6IfIndex,
                               INT4 i4Fsipv6SENDDrpPktType,
                               UINT4 *pu4RetValFsipv6SENDDrpCgaOptPkts)
{

    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);
    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    if (i4Fsipv6SENDDrpPktType > 7 || i4Fsipv6SENDDrpPktType < 1)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsipv6SENDDrpCgaOptPkts =
        pIf6->pIfIcmp6Stats->Nd6SecDropStats[i4Fsipv6SENDDrpPktType].
        u4CgaOptPkts;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6SENDDrpCertOptPkts
 Input       :  The Indices
                Fsipv6IfIndex
                Fsipv6SENDDrpPktType

                The Object 
                retValFsipv6SENDDrpCertOptPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SENDDrpCertOptPkts (INT4 i4Fsipv6IfIndex,
                                INT4 i4Fsipv6SENDDrpPktType,
                                UINT4 *pu4RetValFsipv6SENDDrpCertOptPkts)
{
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);
    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    if (i4Fsipv6SENDDrpPktType > 7 || i4Fsipv6SENDDrpPktType < 1)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsipv6SENDDrpCertOptPkts =
        pIf6->pIfIcmp6Stats->Nd6SecDropStats[i4Fsipv6SENDDrpPktType].
        u4CertOptPkts;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6SENDDrpMtuOptPkts
 Input       :  The Indices
                Fsipv6IfIndex
                Fsipv6SENDDrpPktType

                The Object 
                retValFsipv6SENDDrpMtuOptPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SENDDrpMtuOptPkts (INT4 i4Fsipv6IfIndex,
                               INT4 i4Fsipv6SENDDrpPktType,
                               UINT4 *pu4RetValFsipv6SENDDrpMtuOptPkts)
{
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);
    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    if (i4Fsipv6SENDDrpPktType > 7 || i4Fsipv6SENDDrpPktType < 1)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsipv6SENDDrpMtuOptPkts =
        pIf6->pIfIcmp6Stats->Nd6SecDropStats[i4Fsipv6SENDDrpPktType].
        u4MtuOptPkts;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6SENDDrpNonceOptPkts
 Input       :  The Indices
                Fsipv6IfIndex
                Fsipv6SENDDrpPktType

                The Object 
                retValFsipv6SENDDrpNonceOptPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SENDDrpNonceOptPkts (INT4 i4Fsipv6IfIndex,
                                 INT4 i4Fsipv6SENDDrpPktType,
                                 UINT4 *pu4RetValFsipv6SENDDrpNonceOptPkts)
{
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);
    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    if (i4Fsipv6SENDDrpPktType > 7 || i4Fsipv6SENDDrpPktType < 1)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsipv6SENDDrpNonceOptPkts =
        pIf6->pIfIcmp6Stats->Nd6SecDropStats[i4Fsipv6SENDDrpPktType].
        u4NonceOptPkts;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6SENDDrpPrefixOptPkts
 Input       :  The Indices
                Fsipv6IfIndex
                Fsipv6SENDDrpPktType

                The Object 
                retValFsipv6SENDDrpPrefixOptPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SENDDrpPrefixOptPkts (INT4 i4Fsipv6IfIndex,
                                  INT4 i4Fsipv6SENDDrpPktType,
                                  UINT4 *pu4RetValFsipv6SENDDrpPrefixOptPkts)
{
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);
    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    if (i4Fsipv6SENDDrpPktType > 7 || i4Fsipv6SENDDrpPktType < 1)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsipv6SENDDrpPrefixOptPkts =
        pIf6->pIfIcmp6Stats->Nd6SecDropStats[i4Fsipv6SENDDrpPktType].
        u4PrefixOptPkts;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6SENDDrpRedirHdrPkts
 Input       :  The Indices
                Fsipv6IfIndex
                Fsipv6SENDDrpPktType

                The Object 
                retValFsipv6SENDDrpRedirHdrPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SENDDrpRedirHdrPkts (INT4 i4Fsipv6IfIndex,
                                 INT4 i4Fsipv6SENDDrpPktType,
                                 UINT4 *pu4RetValFsipv6SENDDrpRedirHdrPkts)
{
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);
    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    if (i4Fsipv6SENDDrpPktType > 7 || i4Fsipv6SENDDrpPktType < 1)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsipv6SENDDrpRedirHdrPkts =
        pIf6->pIfIcmp6Stats->Nd6SecDropStats[i4Fsipv6SENDDrpPktType].
        u4RedirHrPkts;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6SENDDrpRsaOptPkts
 Input       :  The Indices
                Fsipv6IfIndex
                Fsipv6SENDDrpPktType

                The Object 
                retValFsipv6SENDDrpRsaOptPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SENDDrpRsaOptPkts (INT4 i4Fsipv6IfIndex,
                               INT4 i4Fsipv6SENDDrpPktType,
                               UINT4 *pu4RetValFsipv6SENDDrpRsaOptPkts)
{
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);
    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    if (i4Fsipv6SENDDrpPktType > 7 || i4Fsipv6SENDDrpPktType < 1)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsipv6SENDDrpRsaOptPkts =
        pIf6->pIfIcmp6Stats->Nd6SecDropStats[i4Fsipv6SENDDrpPktType].
        u4RsaOptPkts;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6SENDDrpSrcLinkAddrPkts
 Input       :  The Indices
                Fsipv6IfIndex
                Fsipv6SENDDrpPktType

                The Object 
                retValFsipv6SENDDrpSrcLinkAddrPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SENDDrpSrcLinkAddrPkts (INT4 i4Fsipv6IfIndex,
                                    INT4 i4Fsipv6SENDDrpPktType,
                                    UINT4
                                    *pu4RetValFsipv6SENDDrpSrcLinkAddrPkts)
{
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);
    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    if (i4Fsipv6SENDDrpPktType > 7 || i4Fsipv6SENDDrpPktType < 1)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsipv6SENDDrpSrcLinkAddrPkts =
        pIf6->pIfIcmp6Stats->Nd6SecDropStats[i4Fsipv6SENDDrpPktType].
        u4SrcLinkAddrPkts;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6SENDDrpTgtLinkAddrPkts
 Input       :  The Indices
                Fsipv6IfIndex
                Fsipv6SENDDrpPktType

                The Object 
                retValFsipv6SENDDrpTgtLinkAddrPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SENDDrpTgtLinkAddrPkts (INT4 i4Fsipv6IfIndex,
                                    INT4 i4Fsipv6SENDDrpPktType,
                                    UINT4
                                    *pu4RetValFsipv6SENDDrpTgtLinkAddrPkts)
{
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);
    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    if (i4Fsipv6SENDDrpPktType > 7 || i4Fsipv6SENDDrpPktType < 1)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsipv6SENDDrpTgtLinkAddrPkts =
        pIf6->pIfIcmp6Stats->Nd6SecDropStats[i4Fsipv6SENDDrpPktType].
        u4TgtLinkAddrPkts;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6SENDDrpTaOptPkts
 Input       :  The Indices
                Fsipv6IfIndex
                Fsipv6SENDDrpPktType

                The Object 
                retValFsipv6SENDDrpTaOptPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SENDDrpTaOptPkts (INT4 i4Fsipv6IfIndex,
                              INT4 i4Fsipv6SENDDrpPktType,
                              UINT4 *pu4RetValFsipv6SENDDrpTaOptPkts)
{
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);
    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    if (i4Fsipv6SENDDrpPktType > 7 || i4Fsipv6SENDDrpPktType < 1)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsipv6SENDDrpTaOptPkts =
        pIf6->pIfIcmp6Stats->Nd6SecDropStats[i4Fsipv6SENDDrpPktType].
        u4TaOptPkts;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6SENDDrpTimeStampOptPkts
 Input       :  The Indices
                Fsipv6IfIndex
                Fsipv6SENDDrpPktType

                The Object 
                retValFsipv6SENDDrpTimeStampOptPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SENDDrpTimeStampOptPkts (INT4 i4Fsipv6IfIndex,
                                     INT4 i4Fsipv6SENDDrpPktType,
                                     UINT4
                                     *pu4RetValFsipv6SENDDrpTimeStampOptPkts)
{
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6IfIndex);
    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    if (i4Fsipv6SENDDrpPktType > 7 || i4Fsipv6SENDDrpPktType < 1)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsipv6SENDDrpTimeStampOptPkts =
        pIf6->pIfIcmp6Stats->Nd6SecDropStats[i4Fsipv6SENDDrpPktType].
        u4TimeStampOptPkts;

    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Fsipv6RARouteInfoTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsipv6RARouteInfoTable
 Input       :  The Indices
                Fsipv6RARouteIfIndex
                Fsipv6RARoutePrefix
                Fsipv6RARoutePrefixLen
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsipv6RARouteInfoTable (INT4 i4Fsipv6RARouteIfIndex,
                                                tSNMP_OCTET_STRING_TYPE
                                                * pFsipv6RARoutePrefix,
                                                INT4 i4Fsipv6RARoutePrefixLen)
{
    tIp6Addr            ip6addr;

    if (Ip6ifEntryExists ((UINT4) i4Fsipv6RARouteIfIndex) == IP6_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (pFsipv6RARoutePrefix == NULL)
    {
        return SNMP_FAILURE;
    }

    Ip6AddrCopy (&ip6addr, (tIp6Addr *) (VOID *)
                 pFsipv6RARoutePrefix->pu1_OctetList);
    if (IS_ADDR_MULTI (ip6addr) ||
        IS_ADDR_LOOPBACK (ip6addr) ||
        IS_ADDR_V4_COMPAT (ip6addr) || IS_ADDR_LLOCAL (ip6addr))
    {
        return SNMP_FAILURE;
    }

    if ((i4Fsipv6RARoutePrefixLen < IP6_ADDR_MIN_PREFIX) ||
        (i4Fsipv6RARoutePrefixLen > IP6_ADDR_MAX_PREFIX))
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsipv6RARouteInfoTable
 Input       :  The Indices
                Fsipv6RARouteIfIndex
                Fsipv6RARoutePrefix
                Fsipv6RARoutePrefixLen
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsipv6RARouteInfoTable (INT4 *pi4Fsipv6RARouteIfIndex,
                                        tSNMP_OCTET_STRING_TYPE
                                        * pFsipv6RARoutePrefix,
                                        INT4 *pi4Fsipv6RARoutePrefixLen)
{
    tIp6RARouteInfoNode *pRARouteInfo = NULL;
    UINT4               u4IfIndex = 0;

    IP6_IF_SCAN (u4IfIndex, (UINT4) IP6_MAX_LOGICAL_IF_INDEX)
    {
        if (Ip6ifEntryExists (u4IfIndex) == IP6_SUCCESS)
        {
            *pi4Fsipv6RARouteIfIndex = (INT4) u4IfIndex;
            pRARouteInfo = Ip6GetFirstRARouteInfo (u4IfIndex);
            if (pRARouteInfo != NULL)
            {
                MEMCPY (pFsipv6RARoutePrefix->pu1_OctetList,
                        &pRARouteInfo->Ip6RARoutePrefix, IP6_ADDR_SIZE);
                pFsipv6RARoutePrefix->i4_Length = IP6_ADDR_SIZE;
                *pi4Fsipv6RARoutePrefixLen = pRARouteInfo->i4RARoutePrefixLen;
                return SNMP_SUCCESS;
            }
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsipv6RARouteInfoTable
 Input       :  The Indices
                Fsipv6RARouteIfIndex
                nextFsipv6RARouteIfIndex
                Fsipv6RARoutePrefix
                nextFsipv6RARoutePrefix
                Fsipv6RARoutePrefixLen
                nextFsipv6RARoutePrefixLen
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1 
     
     
     
     
     
     
     
    nmhGetNextIndexFsipv6RARouteInfoTable
    (INT4 i4Fsipv6RARouteIfIndex,
     INT4 *pi4NextFsipv6RARouteIfIndex,
     tSNMP_OCTET_STRING_TYPE * pFsipv6RARoutePrefix,
     tSNMP_OCTET_STRING_TYPE * pNextFsipv6RARoutePrefix,
     INT4 i4Fsipv6RARoutePrefixLen, INT4 *pi4NextFsipv6RARoutePrefixLen)
{
    tIp6RARouteInfoNode *pRANextRouteInfo = NULL;
    tIp6RARouteInfoNode *pRARouteInfo = NULL;
    UINT4               u4Index = 0;
    if (i4Fsipv6RARouteIfIndex < 0)
    {
        return (SNMP_FAILURE);
    }
    pRANextRouteInfo = Ip6GetNextRARouteInfo ((UINT4) i4Fsipv6RARouteIfIndex,
                                              (tIp6Addr *) (VOID *)
                                              pFsipv6RARoutePrefix->
                                              pu1_OctetList,
                                              i4Fsipv6RARoutePrefixLen);

    if (pRANextRouteInfo != NULL)
    {
        *pi4NextFsipv6RARouteIfIndex = pRANextRouteInfo->i4RARouteIfIndex;
        MEMCPY (pNextFsipv6RARoutePrefix->pu1_OctetList,
                &pRANextRouteInfo->Ip6RARoutePrefix, IP6_ADDR_SIZE);
        pNextFsipv6RARoutePrefix->i4_Length = IP6_ADDR_SIZE;
        *pi4NextFsipv6RARoutePrefixLen = pRANextRouteInfo->i4RARoutePrefixLen;
        return SNMP_SUCCESS;
    }
    else
    {
        u4Index = (UINT4) i4Fsipv6RARouteIfIndex;
        while (Ip6ifGetNextIndex (&u4Index) != IP6_FAILURE)
        {
            pRARouteInfo = Ip6GetFirstRARouteInfo (u4Index);
            if (pRARouteInfo != NULL)
            {
                *pi4NextFsipv6RARouteIfIndex = (INT4) u4Index;
                MEMCPY (pNextFsipv6RARoutePrefix->pu1_OctetList,
                        &pRARouteInfo->Ip6RARoutePrefix, IP6_ADDR_SIZE);
                pNextFsipv6RARoutePrefix->i4_Length = IP6_ADDR_SIZE;
                *pi4NextFsipv6RARoutePrefixLen =
                    pRARouteInfo->i4RARoutePrefixLen;
                return SNMP_SUCCESS;
            }
        }
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetFsipv6RARoutePreference
 Input       :  The Indices
                Fsipv6RARouteIfIndex
                Fsipv6RARoutePrefix
                Fsipv6RARoutePrefixLen

                The Object 
                retValFsipv6RARoutePreference
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6RARoutePreference (INT4 i4Fsipv6RARouteIfIndex,
                               tSNMP_OCTET_STRING_TYPE * pFsipv6RARoutePrefix,
                               INT4 i4Fsipv6RARoutePrefixLen,
                               INT4 *pi4RetValFsipv6RARoutePreference)
{
    tIp6Addr            Ip6Addr;
    tIp6RARouteInfoNode *pIp6RARouteInfoNode = NULL;
    MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));

    Ip6AddrCopy (&Ip6Addr,
                 (tIp6Addr *) (VOID *) pFsipv6RARoutePrefix->pu1_OctetList);

    if (Ip6ifEntryExists ((UINT4) i4Fsipv6RARouteIfIndex) != IP6_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if ((pIp6RARouteInfoNode = Ip6GetRARoutInfo ((UINT4) i4Fsipv6RARouteIfIndex,
                                                 &Ip6Addr,
                                                 i4Fsipv6RARoutePrefixLen))
        == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsipv6RARoutePreference = pIp6RARouteInfoNode->i4RARoutePref;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6RARouteLifetime
 Input       :  The Indices
                Fsipv6RARouteIfIndex
                Fsipv6RARoutePrefix
                Fsipv6RARoutePrefixLen

                The Object 
                retValFsipv6RARouteLifetime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6RARouteLifetime (INT4 i4Fsipv6RARouteIfIndex,
                             tSNMP_OCTET_STRING_TYPE * pFsipv6RARoutePrefix,
                             INT4 i4Fsipv6RARoutePrefixLen,
                             UINT4 *pu4RetValFsipv6RARouteLifetime)
{
    tIp6Addr            Ip6Addr;
    tIp6RARouteInfoNode *pIp6RARouteInfoNode = NULL;
    MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));

    Ip6AddrCopy (&Ip6Addr,
                 (tIp6Addr *) (VOID *) pFsipv6RARoutePrefix->pu1_OctetList);

    if (Ip6ifEntryExists ((UINT4) i4Fsipv6RARouteIfIndex) != IP6_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if ((pIp6RARouteInfoNode = Ip6GetRARoutInfo (i4Fsipv6RARouteIfIndex,
                                                 &Ip6Addr,
                                                 i4Fsipv6RARoutePrefixLen))
        == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsipv6RARouteLifetime = pIp6RARouteInfoNode->u4RARouteLifetime;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6RARouteRowStatus
 Input       :  The Indices
                Fsipv6RARouteIfIndex
                Fsipv6RARoutePrefix
                Fsipv6RARoutePrefixLen

                The Object 
                retValFsipv6RARouteRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6RARouteRowStatus (INT4 i4Fsipv6RARouteIfIndex,
                              tSNMP_OCTET_STRING_TYPE * pFsipv6RARoutePrefix,
                              INT4 i4Fsipv6RARoutePrefixLen,
                              INT4 *pi4RetValFsipv6RARouteRowStatus)
{
    tIp6Addr            Ip6Addr;
    tIp6RARouteInfoNode *pIp6RARouteInfoNode = NULL;
    MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));

    Ip6AddrCopy (&Ip6Addr,
                 (tIp6Addr *) (VOID *) pFsipv6RARoutePrefix->pu1_OctetList);

    if (Ip6ifEntryExists ((UINT4) i4Fsipv6RARouteIfIndex) != IP6_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if ((pIp6RARouteInfoNode = Ip6GetRARoutInfo (i4Fsipv6RARouteIfIndex,
                                                 &Ip6Addr,
                                                 i4Fsipv6RARoutePrefixLen))
        == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsipv6RARouteRowStatus = pIp6RARouteInfoNode->i4RARouteRowStatus;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsipv6RARoutePreference
 Input       :  The Indices
                Fsipv6RARouteIfIndex
                Fsipv6RARoutePrefix
                Fsipv6RARoutePrefixLen

                The Object 
                setValFsipv6RARoutePreference
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6RARoutePreference (INT4 i4Fsipv6RARouteIfIndex,
                               tSNMP_OCTET_STRING_TYPE * pFsipv6RARoutePrefix,
                               INT4 i4Fsipv6RARoutePrefixLen,
                               INT4 i4SetValFsipv6RARoutePreference)
{
    tIp6Addr            Ip6Addr;
    tIp6RARouteInfoNode *pIp6RARouteInfoNode = NULL;
    MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));

    Ip6AddrCopy (&Ip6Addr,
                 (tIp6Addr *) (VOID *) pFsipv6RARoutePrefix->pu1_OctetList);

    if (Ip6ifEntryExists ((UINT4) i4Fsipv6RARouteIfIndex) != IP6_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if ((pIp6RARouteInfoNode = Ip6GetRARoutInfo (i4Fsipv6RARouteIfIndex,
                                                 &Ip6Addr,
                                                 i4Fsipv6RARoutePrefixLen))
        == NULL)
    {
        return SNMP_FAILURE;
    }
    pIp6RARouteInfoNode->i4RARoutePref = i4SetValFsipv6RARoutePreference;
    IncMsrForIpv6RARouteTable (i4Fsipv6RARouteIfIndex, pFsipv6RARoutePrefix,
                               i4Fsipv6RARoutePrefixLen, 'i',
                               &i4SetValFsipv6RARoutePreference,
                               FsMIIpv6RARoutePreference,
                               sizeof (FsMIIpv6RARoutePreference) /
                               sizeof (UINT4), FALSE);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6RARouteLifetime
 Input       :  The Indices
                Fsipv6RARouteIfIndex
                Fsipv6RARoutePrefix
                Fsipv6RARoutePrefixLen

                The Object 
                setValFsipv6RARouteLifetime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6RARouteLifetime (INT4 i4Fsipv6RARouteIfIndex,
                             tSNMP_OCTET_STRING_TYPE * pFsipv6RARoutePrefix,
                             INT4 i4Fsipv6RARoutePrefixLen,
                             UINT4 u4SetValFsipv6RARouteLifetime)
{
    tIp6Addr            Ip6Addr;
    tIp6RARouteInfoNode *pIp6RARouteInfoNode = NULL;
    MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));

    Ip6AddrCopy (&Ip6Addr,
                 (tIp6Addr *) (VOID *) pFsipv6RARoutePrefix->pu1_OctetList);

    if (Ip6ifEntryExists ((UINT4) i4Fsipv6RARouteIfIndex) != IP6_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if ((pIp6RARouteInfoNode = Ip6GetRARoutInfo (i4Fsipv6RARouteIfIndex,
                                                 &Ip6Addr,
                                                 i4Fsipv6RARoutePrefixLen))
        == NULL)
    {
        return SNMP_FAILURE;
    }
    pIp6RARouteInfoNode->u4RARouteLifetime = u4SetValFsipv6RARouteLifetime;
    IncMsrForIpv6RARouteTable (i4Fsipv6RARouteIfIndex, pFsipv6RARoutePrefix,
                               i4Fsipv6RARoutePrefixLen, 'u',
                               &u4SetValFsipv6RARouteLifetime,
                               FsMIIpv6RARouteLifetime,
                               sizeof (FsMIIpv6RARouteLifetime) /
                               sizeof (UINT4), FALSE);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6RARouteRowStatus
 Input       :  The Indices
                Fsipv6RARouteIfIndex
                Fsipv6RARoutePrefix
                Fsipv6RARoutePrefixLen

                The Object 
                setValFsipv6RARouteRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6RARouteRowStatus (INT4 i4Fsipv6RARouteIfIndex,
                              tSNMP_OCTET_STRING_TYPE * pFsipv6RARoutePrefix,
                              INT4 i4Fsipv6RARoutePrefixLen,
                              INT4 i4SetValFsipv6RARouteRowStatus)
{
    tIp6Addr            Ip6Addr;
    tIp6RARouteInfoNode *pIp6RARouteInfoNode = NULL;
    MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));

    Ip6AddrCopy (&Ip6Addr,
                 (tIp6Addr *) (VOID *) pFsipv6RARoutePrefix->pu1_OctetList);

    if (Ip6ifEntryExists ((UINT4) i4Fsipv6RARouteIfIndex) != IP6_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    switch (i4SetValFsipv6RARouteRowStatus)
    {
        case IP6_RA_ROUTE_ROW_STATUS_CREATE_AND_WAIT:
            pIp6RARouteInfoNode =
                Ip6RARouteInfoCreate ((UINT4) i4Fsipv6RARouteIfIndex, &Ip6Addr,
                                      i4Fsipv6RARoutePrefixLen,
                                      IP6_RA_ROUTE_ROW_STATUS_NOT_READY);
            if (pIp6RARouteInfoNode == NULL)
            {
                return SNMP_FAILURE;
            }
            break;

        case IP6_RA_ROUTE_ROW_STATUS_CREATE_AND_GO:
            pIp6RARouteInfoNode =
                Ip6RARouteInfoCreate ((UINT4) i4Fsipv6RARouteIfIndex, &Ip6Addr,
                                      i4Fsipv6RARoutePrefixLen,
                                      IP6_RA_ROUTE_ROW_STATUS_ACTIVE);
            if (pIp6RARouteInfoNode == NULL)
            {
                return SNMP_FAILURE;
            }
            break;

        case IP6_RA_ROUTE_ROW_STATUS_DESTROY:
            if (IP6_FAILURE ==
                IP6DelRARouteInfo ((UINT4) i4Fsipv6RARouteIfIndex, &Ip6Addr,
                                   i4Fsipv6RARoutePrefixLen))
            {
                return SNMP_FAILURE;
            }
            break;

        case IP6_RA_ROUTE_ROW_STATUS_ACTIVE:
            pIp6RARouteInfoNode =
                Ip6GetRARoutInfo ((UINT4) i4Fsipv6RARouteIfIndex, &Ip6Addr,
                                  i4Fsipv6RARoutePrefixLen);

            if (pIp6RARouteInfoNode == NULL)
            {
                return SNMP_FAILURE;
            }
            pIp6RARouteInfoNode->i4RARouteRowStatus =
                IP6_RA_ROUTE_ROW_STATUS_ACTIVE;
            break;

        case IP6_RA_ROUTE_ROW_STATUS_NOT_IN_SERVICE:
            pIp6RARouteInfoNode =
                Ip6GetRARoutInfo ((UINT4) i4Fsipv6RARouteIfIndex, &Ip6Addr,
                                  i4Fsipv6RARoutePrefixLen);

            if (pIp6RARouteInfoNode == NULL)
            {
                return SNMP_FAILURE;
            }
            pIp6RARouteInfoNode->i4RARouteRowStatus =
                IP6_RA_ROUTE_ROW_STATUS_NOT_IN_SERVICE;
            break;

        default:
            return SNMP_FAILURE;
    }
    IncMsrForIpv6RARouteTable (i4Fsipv6RARouteIfIndex, pFsipv6RARoutePrefix,
                               i4Fsipv6RARoutePrefixLen, 'i',
                               &i4SetValFsipv6RARouteRowStatus,
                               FsMIIpv6RARouteRowStatus,
                               sizeof (FsMIIpv6RARouteRowStatus) /
                               sizeof (UINT4), TRUE);

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */
/****************************************************************************
 Function    :  nmhTestv2Fsipv6RARoutePreference
 Input       :  The Indices
                Fsipv6RARouteIfIndex
                Fsipv6RARoutePrefix
                Fsipv6RARoutePrefixLen

                The Object 
                testValFsipv6RARoutePreference
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6RARoutePreference (UINT4 *pu4ErrorCode,
                                  INT4 i4Fsipv6RARouteIfIndex,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pFsipv6RARoutePrefix,
                                  INT4 i4Fsipv6RARoutePrefixLen,
                                  INT4 i4TestValFsipv6RARoutePreference)
{
    tIp6If             *pIf6 = NULL;
    tIp6Addr            ip6addr;

    MEMSET (&ip6addr, 0, sizeof (tIp6Addr));

    *pu4ErrorCode = SNMP_ERR_NO_CREATION;

    if (Ip6ifEntryExists ((UINT4) i4Fsipv6RARouteIfIndex) == IP6_FAILURE)
    {
        CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
        return SNMP_FAILURE;
    }

    pIf6 = gIp6GblInfo.apIp6If[i4Fsipv6RARouteIfIndex];
    if (pIf6 == NULL)
    {
        CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
        return SNMP_FAILURE;
    }

    if (pFsipv6RARoutePrefix == NULL)
    {
        CLI_SET_ERR (CLI_IP6_INVALID_ADDRESS);
        return SNMP_FAILURE;
    }

    Ip6AddrCopy (&ip6addr,
                 (tIp6Addr *) (VOID *) pFsipv6RARoutePrefix->pu1_OctetList);

    if (i4TestValFsipv6RARoutePreference != IP6_RA_ROUTE_PREF_LOW &&
        i4TestValFsipv6RARoutePreference != IP6_RA_ROUTE_PREF_MED &&
        i4TestValFsipv6RARoutePreference != IP6_RA_ROUTE_PREF_HIGH)
    {
        CLI_SET_ERR (CLI_IP6_INVALID_RA_ROUTE_PREFERENCE);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    UNUSED_PARAM (i4Fsipv6RARoutePrefixLen);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6RARouteLifetime
 Input       :  The Indices
                Fsipv6RARouteIfIndex
                Fsipv6RARoutePrefix
                Fsipv6RARoutePrefixLen

                The Object 
                testValFsipv6RARouteLifetime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6RARouteLifetime (UINT4 *pu4ErrorCode,
                                INT4 i4Fsipv6RARouteIfIndex,
                                tSNMP_OCTET_STRING_TYPE * pFsipv6RARoutePrefix,
                                INT4 i4Fsipv6RARoutePrefixLen,
                                UINT4 u4TestValFsipv6RARouteLifetime)
{
    tIp6If             *pIf6 = NULL;
    tIp6Addr            ip6addr;

    MEMSET (&ip6addr, 0, sizeof (tIp6Addr));
    *pu4ErrorCode = SNMP_ERR_NO_CREATION;

    if (Ip6ifEntryExists ((UINT4) i4Fsipv6RARouteIfIndex) == IP6_FAILURE)
    {
        CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
        return SNMP_FAILURE;
    }

    pIf6 = gIp6GblInfo.apIp6If[i4Fsipv6RARouteIfIndex];
    if (pIf6 == NULL)
    {
        CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
        return SNMP_FAILURE;
    }

    if (pFsipv6RARoutePrefix == NULL)
    {
        CLI_SET_ERR (CLI_IP6_INVALID_ADDRESS);
        return SNMP_FAILURE;
    }
    Ip6AddrCopy (&ip6addr,
                 (tIp6Addr *) (VOID *) pFsipv6RARoutePrefix->pu1_OctetList);

    if (u4TestValFsipv6RARouteLifetime > IP6_RA_ROUTE_MAX_LIFETIME)
    {
        CLI_SET_ERR (CLI_IP6_INVALID_RA_ROUTE_LIFETIME);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    UNUSED_PARAM (i4Fsipv6RARoutePrefixLen);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6RARouteRowStatus
 Input       :  The Indices
                Fsipv6RARouteIfIndex
                Fsipv6RARoutePrefix
                Fsipv6RARoutePrefixLen

                The Object 
                testValFsipv6RARouteRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6RARouteRowStatus (UINT4 *pu4ErrorCode,
                                 INT4 i4Fsipv6RARouteIfIndex,
                                 tSNMP_OCTET_STRING_TYPE * pFsipv6RARoutePrefix,
                                 INT4 i4Fsipv6RARoutePrefixLen,
                                 INT4 i4TestValFsipv6RARouteRowStatus)
{
    tIp6If             *pIf6 = NULL;
    tIp6Addr            ip6addr;
    tIp6Addr            Ip6Addr;
    tIp6RARouteInfoNode *pIp6RARouteInfo = NULL;
    tIp6PrefixNode     *pPrefix = NULL;
    UINT4               u4RoutInfoCount = 0;

    MEMSET (&ip6addr, 0, sizeof (tIp6Addr));
    MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));

    if (pFsipv6RARoutePrefix == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_ADDRESS);
        return SNMP_FAILURE;
    }

    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
    Ip6AddrCopy (&ip6addr,
                 (tIp6Addr *) (VOID *) pFsipv6RARoutePrefix->pu1_OctetList);

    Ip6CopyAddrBits (&Ip6Addr,
                     (tIp6Addr *) (VOID *) pFsipv6RARoutePrefix->pu1_OctetList,
                     i4Fsipv6RARoutePrefixLen);

    pIf6 = Ip6ifGetEntry ((UINT4) i4Fsipv6RARouteIfIndex);
    if (pIf6 == NULL)
    {
        /* Interface Does not Exist */
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
        return SNMP_FAILURE;
    }

    if (IS_ADDR_MULTI (ip6addr) ||
        IS_ADDR_LOOPBACK (ip6addr) ||
        IS_ADDR_V4_COMPAT (ip6addr) || IS_ADDR_LLOCAL (ip6addr))
    {
        /* Invalid Address. */
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_ADDRESS);
        return SNMP_FAILURE;
    }

    /* For default route Prefix Length should be zero */
    if (IS_ADDR_UNSPECIFIED (ip6addr))
    {
        if (i4Fsipv6RARoutePrefixLen != 0)
        {
            CLI_SET_ERR (CLI_IP6_DEF_ROUTE_ERR);
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (i4Fsipv6RARoutePrefixLen == 0)
        {
            CLI_SET_ERR (CLI_IP6_PREFIX_LEN_ERR);
            return SNMP_FAILURE;
        }
    }

    if (Ip6AddrCompare (ip6addr, Ip6Addr) != IP6_ZERO)
    {
        CLI_SET_ERR (CLI_IP6_NOT_A_VALID_PREFIX);
        return SNMP_FAILURE;
    }

    /* Validate the Address Prefix Length */
    if ((i4Fsipv6RARoutePrefixLen < IP6_ADDR_MIN_PREFIX) ||
        (i4Fsipv6RARoutePrefixLen > IP6_ADDR_MAX_PREFIX))
    {
        /* Invalid Address Prefix Length */
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_PREFIXLEN);
        return SNMP_FAILURE;
    }

    if ((i4TestValFsipv6RARouteRowStatus == IP6_RA_ROUTE_ROW_STATUS_NOT_READY)
        || (i4TestValFsipv6RARouteRowStatus ==
            IP6_RA_ROUTE_ROW_STATUS_NOT_IN_SERVICE)
        || (i4TestValFsipv6RARouteRowStatus ==
            IP6_RA_ROUTE_ROW_STATUS_CREATE_AND_GO)
        || (i4TestValFsipv6RARouteRowStatus < IP6_RA_ROUTE_ROW_STATUS_ACTIVE)
        || (i4TestValFsipv6RARouteRowStatus > IP6_RA_ROUTE_ROW_STATUS_DESTROY))
    {
        /* Invalid Row Status Value. */
        CLI_SET_ERR (CLI_IP6_INVALID_RA_ROWSTATUS);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pIp6RARouteInfo =
        Ip6GetRARoutInfo ((UINT4) i4Fsipv6RARouteIfIndex, &ip6addr,
                          i4Fsipv6RARoutePrefixLen);

    pPrefix = Ip6GetRAPrefix ((UINT4) i4Fsipv6RARouteIfIndex,
                              (tIp6Addr *) (VOID *)
                              pFsipv6RARoutePrefix->pu1_OctetList,
                              (UINT1) i4Fsipv6RARoutePrefixLen);

    if ((pIp6RARouteInfo != NULL) &&
        (i4TestValFsipv6RARouteRowStatus ==
         IP6_RA_ROUTE_ROW_STATUS_CREATE_AND_WAIT))
    {
        CLI_SET_ERR (CLI_IP6_RA_ROUTE_ALREADY_CONFIGD);
        return SNMP_FAILURE;
    }

    if ((pIp6RARouteInfo == NULL) &&
        (i4TestValFsipv6RARouteRowStatus == IP6_RA_ROUTE_ROW_STATUS_DESTROY))
    {
        CLI_SET_ERR (CLI_IP6_RA_INFO_NOT_PRESENT);
        return SNMP_FAILURE;
    }

    if (pPrefix != NULL)
    {
        CLI_SET_ERR (CLI_IP6_PREFIX_ALREADY_CONFIG_FOR_RAINFO);
        return SNMP_FAILURE;
    }

    if (RB_SUCCESS !=
        (RBTreeCount (pIf6->Ip6RARouteInfoTree, &u4RoutInfoCount)))
    {
        return SNMP_FAILURE;
    }

    if ((u4RoutInfoCount >= IP6_RA_MAX_RIO_PER_INT) &&
        (i4TestValFsipv6RARouteRowStatus != IP6_RA_ROUTE_ROW_STATUS_DESTROY) &&
        (i4TestValFsipv6RARouteRowStatus != IP6_RA_ROUTE_ROW_STATUS_ACTIVE))
    {
        CLI_SET_ERR (CLI_IP6_MAX_RAROUTE_INFO_REACHD);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Fsipv6RARouteInfoTable
 Input       :  The Indices
                Fsipv6RARouteIfIndex
                Fsipv6RARoutePrefix
                Fsipv6RARoutePrefixLen
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsipv6RARouteInfoTable (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsipv6IcmpInMsgs
 Input       :  The Indices

                The Object 
                retValFsipv6IcmpInMsgs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IcmpInMsgs (UINT4 *pu4RetValFsipv6IcmpInMsgs)
{
    if (gIp6GblInfo.pIp6CurrCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsipv6IcmpInMsgs = gIp6GblInfo.pIp6CurrCxt->Icmp6Stats.u4InMsgs;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IcmpInErrors
 Input       :  The Indices

                The Object 
                retValFsipv6IcmpInErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IcmpInErrors (UINT4 *pu4RetValFsipv6IcmpInErrors)
{
    if (gIp6GblInfo.pIp6CurrCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsipv6IcmpInErrors = gIp6GblInfo.pIp6CurrCxt->Icmp6Stats.u4InErrs;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IcmpInDestUnreachs
 Input       :  The Indices

                The Object 
                retValFsipv6IcmpInDestUnreachs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IcmpInDestUnreachs (UINT4 *pu4RetValFsipv6IcmpInDestUnreachs)
{
    if (gIp6GblInfo.pIp6CurrCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsipv6IcmpInDestUnreachs =
        gIp6GblInfo.pIp6CurrCxt->Icmp6Stats.u4InDstUnreach;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IcmpInTimeExcds
 Input       :  The Indices

                The Object 
                retValFsipv6IcmpInTimeExcds
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IcmpInTimeExcds (UINT4 *pu4RetValFsipv6IcmpInTimeExcds)
{
    if (gIp6GblInfo.pIp6CurrCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsipv6IcmpInTimeExcds =
        gIp6GblInfo.pIp6CurrCxt->Icmp6Stats.u4InTmexceeded;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IcmpInParmProbs
 Input       :  The Indices

                The Object 
                retValFsipv6IcmpInParmProbs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IcmpInParmProbs (UINT4 *pu4RetValFsipv6IcmpInParmProbs)
{
    if (gIp6GblInfo.pIp6CurrCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsipv6IcmpInParmProbs =
        gIp6GblInfo.pIp6CurrCxt->Icmp6Stats.u4InParamprob;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IcmpInPktTooBigs
 Input       :  The Indices

                The Object 
                retValFsipv6IcmpInPktTooBigs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IcmpInPktTooBigs (UINT4 *pu4RetValFsipv6IcmpInPktTooBigs)
{
    if (gIp6GblInfo.pIp6CurrCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsipv6IcmpInPktTooBigs =
        gIp6GblInfo.pIp6CurrCxt->Icmp6Stats.u4InToobig;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IcmpInEchos
 Input       :  The Indices

                The Object 
                retValFsipv6IcmpInEchos
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IcmpInEchos (UINT4 *pu4RetValFsipv6IcmpInEchos)
{
    if (gIp6GblInfo.pIp6CurrCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsipv6IcmpInEchos =
        gIp6GblInfo.pIp6CurrCxt->Icmp6Stats.u4InEchoReq;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IcmpInEchoReps
 Input       :  The Indices

                The Object 
                retValFsipv6IcmpInEchoReps
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IcmpInEchoReps (UINT4 *pu4RetValFsipv6IcmpInEchoReps)
{
    if (gIp6GblInfo.pIp6CurrCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsipv6IcmpInEchoReps =
        gIp6GblInfo.pIp6CurrCxt->Icmp6Stats.u4InEchoResp;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IcmpInRouterSolicits
 Input       :  The Indices

                The Object 
                retValFsipv6IcmpInRouterSolicits
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IcmpInRouterSolicits (UINT4 *pu4RetValFsipv6IcmpInRouterSolicits)
{
    if (gIp6GblInfo.pIp6CurrCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsipv6IcmpInRouterSolicits =
        gIp6GblInfo.pIp6CurrCxt->Icmp6Stats.u4InRouterSol;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IcmpInRouterAdvertisements
 Input       :  The Indices

                The Object 
                retValFsipv6IcmpInRouterAdvertisements
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IcmpInRouterAdvertisements (UINT4
                                        *pu4RetValFsipv6IcmpInRouterAdvertisements)
{
    if (gIp6GblInfo.pIp6CurrCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsipv6IcmpInRouterAdvertisements =
        gIp6GblInfo.pIp6CurrCxt->Icmp6Stats.u4InRouterAdv;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IcmpInNeighborSolicits
 Input       :  The Indices

                The Object 
                retValFsipv6IcmpInNeighborSolicits
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetFsipv6IcmpInNeighborSolicits
    (UINT4 *pu4RetValFsipv6IcmpInNeighborSolicits)
{
    if (gIp6GblInfo.pIp6CurrCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsipv6IcmpInNeighborSolicits =
        gIp6GblInfo.pIp6CurrCxt->Icmp6Stats.u4InNeighSol;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IcmpInNeighborAdvertisements
 Input       :  The Indices

                The Object 
                retValFsipv6IcmpInNeighborAdvertisements
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetFsipv6IcmpInNeighborAdvertisements
    (UINT4 *pu4RetValFsipv6IcmpInNeighborAdvertisements)
{
    if (gIp6GblInfo.pIp6CurrCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsipv6IcmpInNeighborAdvertisements =
        gIp6GblInfo.pIp6CurrCxt->Icmp6Stats.u4InNeighAdv;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IcmpInRedirects
 Input       :  The Indices

                The Object 
                retValFsipv6IcmpInRedirects
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IcmpInRedirects (UINT4 *pu4RetValFsipv6IcmpInRedirects)
{
    if (gIp6GblInfo.pIp6CurrCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsipv6IcmpInRedirects =
        gIp6GblInfo.pIp6CurrCxt->Icmp6Stats.u4InRedir;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IcmpInAdminProhib
 Input       :  The Indices

                The Object 
                retValFsipv6IcmpInAdminProhib
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IcmpInAdminProhib (UINT4 *pu4RetValFsipv6IcmpInAdminProhib)
{
    if (gIp6GblInfo.pIp6CurrCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsipv6IcmpInAdminProhib =
        gIp6GblInfo.pIp6CurrCxt->Icmp6Stats.u4InDstUnreach;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IcmpOutMsgs
 Input       :  The Indices

                The Object 
                retValFsipv6IcmpOutMsgs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IcmpOutMsgs (UINT4 *pu4RetValFsipv6IcmpOutMsgs)
{
    if (gIp6GblInfo.pIp6CurrCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsipv6IcmpOutMsgs = gIp6GblInfo.pIp6CurrCxt->Icmp6Stats.u4OutMsgs;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IcmpOutErrors
 Input       :  The Indices

                The Object 
                retValFsipv6IcmpOutErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IcmpOutErrors (UINT4 *pu4RetValFsipv6IcmpOutErrors)
{
    if (gIp6GblInfo.pIp6CurrCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsipv6IcmpOutErrors =
        gIp6GblInfo.pIp6CurrCxt->Icmp6Stats.u4OutErrs;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IcmpOutDestUnreachs
 Input       :  The Indices

                The Object 
                retValFsipv6IcmpOutDestUnreachs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IcmpOutDestUnreachs (UINT4 *pu4RetValFsipv6IcmpOutDestUnreachs)
{
    if (gIp6GblInfo.pIp6CurrCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsipv6IcmpOutDestUnreachs =
        gIp6GblInfo.pIp6CurrCxt->Icmp6Stats.u4OutDstUnreach;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IcmpOutTimeExcds
 Input       :  The Indices

                The Object 
                retValFsipv6IcmpOutTimeExcds
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IcmpOutTimeExcds (UINT4 *pu4RetValFsipv6IcmpOutTimeExcds)
{
    if (gIp6GblInfo.pIp6CurrCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsipv6IcmpOutTimeExcds =
        gIp6GblInfo.pIp6CurrCxt->Icmp6Stats.u4OutTmexceeded;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IcmpOutParmProbs
 Input       :  The Indices

                The Object 
                retValFsipv6IcmpOutParmProbs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IcmpOutParmProbs (UINT4 *pu4RetValFsipv6IcmpOutParmProbs)
{
    if (gIp6GblInfo.pIp6CurrCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsipv6IcmpOutParmProbs =
        gIp6GblInfo.pIp6CurrCxt->Icmp6Stats.u4OutParamprob;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IcmpOutPktTooBigs
 Input       :  The Indices

                The Object 
                retValFsipv6IcmpOutPktTooBigs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IcmpOutPktTooBigs (UINT4 *pu4RetValFsipv6IcmpOutPktTooBigs)
{
    if (gIp6GblInfo.pIp6CurrCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsipv6IcmpOutPktTooBigs =
        gIp6GblInfo.pIp6CurrCxt->Icmp6Stats.u4OutToobig;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IcmpOutEchos
 Input       :  The Indices

                The Object 
                retValFsipv6IcmpOutEchos
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IcmpOutEchos (UINT4 *pu4RetValFsipv6IcmpOutEchos)
{
    if (gIp6GblInfo.pIp6CurrCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsipv6IcmpOutEchos =
        gIp6GblInfo.pIp6CurrCxt->Icmp6Stats.u4OutEchoReq;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IcmpOutEchoReps
 Input       :  The Indices

                The Object 
                retValFsipv6IcmpOutEchoReps
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IcmpOutEchoReps (UINT4 *pu4RetValFsipv6IcmpOutEchoReps)
{
    if (gIp6GblInfo.pIp6CurrCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsipv6IcmpOutEchoReps =
        gIp6GblInfo.pIp6CurrCxt->Icmp6Stats.u4OutEchoResp;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IcmpOutRouterSolicits
 Input       :  The Indices

                The Object 
                retValFsipv6IcmpOutRouterSolicits
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IcmpOutRouterSolicits (UINT4 *pu4RetValFsipv6IcmpOutRouterSolicits)
{
    if (gIp6GblInfo.pIp6CurrCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsipv6IcmpOutRouterSolicits =
        gIp6GblInfo.pIp6CurrCxt->Icmp6Stats.u4OutRouterSol;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IcmpOutRouterAdvertisements
 Input       :  The Indices

                The Object 
                retValFsipv6IcmpOutRouterAdvertisements
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetFsipv6IcmpOutRouterAdvertisements
    (UINT4 *pu4RetValFsipv6IcmpOutRouterAdvertisements)
{
    if (gIp6GblInfo.pIp6CurrCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsipv6IcmpOutRouterAdvertisements =
        gIp6GblInfo.pIp6CurrCxt->Icmp6Stats.u4OutRouterAdv;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IcmpOutNeighborSolicits
 Input       :  The Indices

                The Object 
                retValFsipv6IcmpOutNeighborSolicits
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetFsipv6IcmpOutNeighborSolicits
    (UINT4 *pu4RetValFsipv6IcmpOutNeighborSolicits)
{
    if (gIp6GblInfo.pIp6CurrCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsipv6IcmpOutNeighborSolicits =
        gIp6GblInfo.pIp6CurrCxt->Icmp6Stats.u4OutNeighSol;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IcmpOutNeighborAdvertisements
 Input       :  The Indices

                The Object 
                retValFsipv6IcmpOutNeighborAdvertisements
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetFsipv6IcmpOutNeighborAdvertisements
    (UINT4 *pu4RetValFsipv6IcmpOutNeighborAdvertisements)
{
    if (gIp6GblInfo.pIp6CurrCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsipv6IcmpOutNeighborAdvertisements =
        gIp6GblInfo.pIp6CurrCxt->Icmp6Stats.u4OutNeighAdv;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IcmpOutRedirects
 Input       :  The Indices

                The Object 
                retValFsipv6IcmpOutRedirects
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IcmpOutRedirects (UINT4 *pu4RetValFsipv6IcmpOutRedirects)
{
    if (gIp6GblInfo.pIp6CurrCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsipv6IcmpOutRedirects =
        gIp6GblInfo.pIp6CurrCxt->Icmp6Stats.u4OutRedir;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IcmpOutAdminProhib
 Input       :  The Indices

                The Object 
                retValFsipv6IcmpOutAdminProhib
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IcmpOutAdminProhib (UINT4 *pu4RetValFsipv6IcmpOutAdminProhib)
{
    if (gIp6GblInfo.pIp6CurrCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsipv6IcmpOutAdminProhib =
        gIp6GblInfo.pIp6CurrCxt->Icmp6Stats.u4OutDstUnreach;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IcmpInBadCode
 Input       :  The Indices

                The Object 
                retValFsipv6IcmpInBadCode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IcmpInBadCode (UINT4 *pu4RetValFsipv6IcmpInBadCode)
{
    if (gIp6GblInfo.pIp6CurrCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsipv6IcmpInBadCode =
        gIp6GblInfo.pIp6CurrCxt->Icmp6Stats.u4InBadcode;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IcmpInNARouterFlagSet
 Input       :  The Indices

                The Object 
                retValFsipv6IcmpInNARouterFlagSet
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IcmpInNARouterFlagSet (UINT4 *pu4RetValFsipv6IcmpInNARouterFlagSet)
{
    if (gIp6GblInfo.pIp6CurrCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsipv6IcmpInNARouterFlagSet =
        gIp6GblInfo.pIp6CurrCxt->Icmp6Stats.u4InNARouterFlag;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IcmpInNASolicitedFlagSet
 Input       :  The Indices

                The Object 
                retValFsipv6IcmpInNASolicitedFlagSet
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IcmpInNASolicitedFlagSet (UINT4
                                      *pu4RetValFsipv6IcmpInNASolicitedFlagSet)
{
    if (gIp6GblInfo.pIp6CurrCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsipv6IcmpInNASolicitedFlagSet =
        gIp6GblInfo.pIp6CurrCxt->Icmp6Stats.u4InNASolicitedFlag;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IcmpInNAOverrideFlagSet
 Input       :  The Indices

                The Object 
                retValFsipv6IcmpInNAOverrideFlagSet
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IcmpInNAOverrideFlagSet (UINT4
                                     *pu4RetValFsipv6IcmpInNAOverrideFlagSet)
{
    if (gIp6GblInfo.pIp6CurrCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsipv6IcmpInNAOverrideFlagSet =
        gIp6GblInfo.pIp6CurrCxt->Icmp6Stats.u4InNAOverrideFlag;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IcmpOutNARouterFlagSet
 Input       :  The Indices

                The Object 
                retValFsipv6IcmpOutNARouterFlagSet
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IcmpOutNARouterFlagSet (UINT4
                                    *pu4RetValFsipv6IcmpOutNARouterFlagSet)
{
    if (gIp6GblInfo.pIp6CurrCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsipv6IcmpOutNARouterFlagSet =
        gIp6GblInfo.pIp6CurrCxt->Icmp6Stats.u4OutNARouterFlag;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IcmpOutNASolicitedFlagSet
 Input       :  The Indices

                The Object 
                retValFsipv6IcmpOutNASolicitedFlagSet
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IcmpOutNASolicitedFlagSet (UINT4
                                       *pu4RetValFsipv6IcmpOutNASolicitedFlagSet)
{
    if (gIp6GblInfo.pIp6CurrCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsipv6IcmpOutNASolicitedFlagSet =
        gIp6GblInfo.pIp6CurrCxt->Icmp6Stats.u4OutNASolicitedFlag;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IcmpOutNAOverrideFlagSet
 Input       :  The Indices

                The Object 
                retValFsipv6IcmpOutNAOverrideFlagSet
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IcmpOutNAOverrideFlagSet (UINT4
                                      *pu4RetValFsipv6IcmpOutNAOverrideFlagSet)
{
    if (gIp6GblInfo.pIp6CurrCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsipv6IcmpOutNAOverrideFlagSet =
        gIp6GblInfo.pIp6CurrCxt->Icmp6Stats.u4OutNAOverrideFlag;
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsipv6UdpInDatagrams
 Input       :  The Indices

                The Object 
                retValFsipv6UdpInDatagrams
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6UdpInDatagrams (UINT4 *pu4RetValFsipv6UdpInDatagrams)
{
    *pu4RetValFsipv6UdpInDatagrams = gIp6GblInfo.Udp6Stats.u4InDgrams;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6UdpNoPorts
 Input       :  The Indices

                The Object 
                retValFsipv6UdpNoPorts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6UdpNoPorts (UINT4 *pu4RetValFsipv6UdpNoPorts)
{
    *pu4RetValFsipv6UdpNoPorts = gIp6GblInfo.Udp6Stats.u4NumPorts;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6UdpInErrors
 Input       :  The Indices

                The Object 
                retValFsipv6UdpInErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6UdpInErrors (UINT4 *pu4RetValFsipv6UdpInErrors)
{
    *pu4RetValFsipv6UdpInErrors = gIp6GblInfo.Udp6Stats.u4InErrs;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6UdpOutDatagrams
 Input       :  The Indices

                The Object 
                retValFsipv6UdpOutDatagrams
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6UdpOutDatagrams (UINT4 *pu4RetValFsipv6UdpOutDatagrams)
{
    *pu4RetValFsipv6UdpOutDatagrams = gIp6GblInfo.Udp6Stats.u4OutDgrams;
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Fsipv6RouteTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsipv6RouteTable
 Input       :  The Indices
                Fsipv6RouteDest
                Fsipv6RoutePfxLength
                Fsipv6RouteProtocol
                Fsipv6RouteNextHop
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsipv6RouteTable (tSNMP_OCTET_STRING_TYPE *
                                          pFsipv6RouteDest,
                                          INT4 i4Fsipv6RoutePfxLength,
                                          INT4 i4Fsipv6RouteProtocol,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pFsipv6RouteNextHop)
{
    tIp6Addr            ip6Addr;

    MEMSET (&ip6Addr, IP6_ZERO, sizeof (tIp6Addr));
    UNUSED_PARAM (pFsipv6RouteNextHop);

    /* validate protocol id. */
    if ((i4Fsipv6RouteProtocol < IP6_OTHER_PROTOID) ||
        (i4Fsipv6RouteProtocol > IP6_IGRP_PROTOID))
    {
        /* Invalid Protocol Id */
        return SNMP_FAILURE;
    }

    /* Validate prefix and prefixlen. */
    Ip6AddrCopy (&ip6Addr,
                 (tIp6Addr *) (VOID *) pFsipv6RouteDest->pu1_OctetList);

    if ((IS_ADDR_MULTI (ip6Addr)) || (IS_ADDR_LLOCAL (ip6Addr)) ||
        (i4Fsipv6RoutePfxLength < IP6_ADDR_MIN_PREFIX) ||
        (i4Fsipv6RoutePfxLength > IP6_ADDR_MAX_PREFIX))
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsipv6RouteTable
 Input       :  The Indices
                Fsipv6RouteDest
                Fsipv6RoutePfxLength
                Fsipv6RouteProtocol
                Fsipv6RouteNextHop
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsipv6RouteTable (tSNMP_OCTET_STRING_TYPE * pFsipv6RouteDest,
                                  INT4 *pi4Fsipv6RoutePfxLength,
                                  INT4 *pi4Fsipv6RouteProtocol,
                                  tSNMP_OCTET_STRING_TYPE * pFsipv6RouteNextHop)
{
    tNetIpv6RtInfo      NetIpv6RtInfo;
    UINT4               u4ContextId;
    INT4                i4RetVal = RTM6_FAILURE;
    INT4                i4RetVal1 = RTM6_FAILURE;

    /* Get the first route from the TRIE. */
    MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));
    u4ContextId = UtilRtm6GetCurrCxtId ();
    i4RetVal = Rtm6UtilGetFirstFwdTableRtEntry (u4ContextId, &NetIpv6RtInfo);
    i4RetVal1 = Rtm6UtilGetFirstInActiveTableRtEntry (&NetIpv6RtInfo);
    if ((i4RetVal1 == RTM6_FAILURE) && (i4RetVal == RTM6_FAILURE))
    {
        /* No IPV6 Route present. */
        return SNMP_FAILURE;
    }
    if (i4RetVal == RTM6_FAILURE)
    {
        Rtm6UtilGetFirstInActiveTableRtEntry (&NetIpv6RtInfo);
    }
    else if (i4RetVal1 == RTM6_FAILURE)
    {
        Rtm6UtilGetFirstFwdTableRtEntry (u4ContextId, &NetIpv6RtInfo);
    }
    else if ((i4RetVal1 == RTM6_SUCCESS) && (i4RetVal == RTM6_SUCCESS))
    {
        Rtm6UtilGetFirstFwdTableRtEntry (u4ContextId, &NetIpv6RtInfo);
    }

    *pi4Fsipv6RoutePfxLength = NetIpv6RtInfo.u1Prefixlen;
    *pi4Fsipv6RouteProtocol = NetIpv6RtInfo.i1Proto;
    Ip6AddrCopy ((tIp6Addr *) (VOID *) pFsipv6RouteDest->pu1_OctetList,
                 &NetIpv6RtInfo.Ip6Dst);
    pFsipv6RouteDest->i4_Length = IP6_ADDR_SIZE;
    Ip6AddrCopy ((tIp6Addr *) (VOID *) pFsipv6RouteNextHop->pu1_OctetList,
                 &NetIpv6RtInfo.NextHop);
    pFsipv6RouteNextHop->i4_Length = IP6_ADDR_SIZE;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsipv6RouteTable
 Input       :  The Indices
                Fsipv6RouteDest
                nextFsipv6RouteDest
                Fsipv6RoutePfxLength
                nextFsipv6RoutePfxLength
                Fsipv6RouteProtocol
                nextFsipv6RouteProtocol
                Fsipv6RouteNextHop
                nextFsipv6RouteNextHop
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsipv6RouteTable (tSNMP_OCTET_STRING_TYPE * pFsipv6RouteDest,
                                 tSNMP_OCTET_STRING_TYPE * pNextFsipv6RouteDest,
                                 INT4 i4Fsipv6RoutePfxLength,
                                 INT4 *pi4NextFsipv6RoutePfxLength,
                                 INT4 i4Fsipv6RouteProtocol,
                                 INT4 *pi4NextFsipv6RouteProtocol,
                                 tSNMP_OCTET_STRING_TYPE * pFsipv6RouteNextHop,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pNextFsipv6RouteNextHop)
{
    tNetIpv6RtInfo      NetIpv6RtInfo;
    UINT4               u4ContextId;

    MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));
    u4ContextId = UtilRtm6GetCurrCxtId ();
    /* Get the first route from the TRIE. */
    if (Rtm6ApiTrieGetNextRtWithLeastNHInCxt
        (u4ContextId, (tIp6Addr *) (VOID *) pFsipv6RouteDest->pu1_OctetList,
         i4Fsipv6RoutePfxLength, i4Fsipv6RouteProtocol,
         (tIp6Addr *) (VOID *) pFsipv6RouteNextHop->pu1_OctetList,
         &NetIpv6RtInfo) == RTM6_FAILURE)
    {
        /* No IPV6 Route present. */
        return SNMP_FAILURE;
    }
    /* Found Next route */
    *pi4NextFsipv6RoutePfxLength = NetIpv6RtInfo.u1Prefixlen;
    *pi4NextFsipv6RouteProtocol = NetIpv6RtInfo.i1Proto;
    Ip6AddrCopy ((tIp6Addr *) (VOID *) pNextFsipv6RouteDest->pu1_OctetList,
                 &NetIpv6RtInfo.Ip6Dst);
    pNextFsipv6RouteDest->i4_Length = IP6_ADDR_SIZE;
    Ip6AddrCopy ((tIp6Addr *) (VOID *) pNextFsipv6RouteNextHop->pu1_OctetList,
                 &NetIpv6RtInfo.NextHop);
    pNextFsipv6RouteNextHop->i4_Length = IP6_ADDR_SIZE;
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsipv6RouteIfIndex
 Input       :  The Indices
                Fsipv6RouteDest
                Fsipv6RoutePfxLength
                Fsipv6RouteProtocol
                Fsipv6RouteNextHop

                The Object 
                retValFsipv6RouteIfIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6RouteIfIndex (tSNMP_OCTET_STRING_TYPE * pFsipv6RouteDest,
                          INT4 i4Fsipv6RoutePfxLength,
                          INT4 i4Fsipv6RouteProtocol,
                          tSNMP_OCTET_STRING_TYPE * pFsipv6RouteNextHop,
                          INT4 *pi4RetValFsipv6RouteIfIndex)
{
    tIp6Addr            ip6Addr;
    tIp6Addr            NextHop;
    tNetIpv6RtInfo      NetIpv6RtInfo;
    UINT4               u4ContextId;

    MEMSET (&ip6Addr, IP6_ZERO, sizeof (tIp6Addr));
    MEMSET (&NextHop, IP6_ZERO, sizeof (tIp6Addr));
    Ip6AddrCopy (&ip6Addr, (tIp6Addr *) (VOID *)
                 pFsipv6RouteDest->pu1_OctetList);
    Ip6AddrCopy (&NextHop, (tIp6Addr *) (VOID *)
                 pFsipv6RouteNextHop->pu1_OctetList);

    MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));
    u4ContextId = (UINT4) UtilRtm6GetCurrCxtId ();
    if (Rtm6ApiFindRtEntryInCxt (u4ContextId, &ip6Addr,
                                 (UINT1) i4Fsipv6RoutePfxLength,
                                 (INT1) i4Fsipv6RouteProtocol, &NextHop,
                                 &NetIpv6RtInfo) == RTM6_SUCCESS)
    {
        *pi4RetValFsipv6RouteIfIndex = NetIpv6RtInfo.u4Index;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsipv6RouteMetric
 Input       :  The Indices
                Fsipv6RouteDest
                Fsipv6RoutePfxLength
                Fsipv6RouteProtocol
                Fsipv6RouteNextHop

                The Object 
                retValFsipv6RouteMetric
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6RouteMetric (tSNMP_OCTET_STRING_TYPE * pFsipv6RouteDest,
                         INT4 i4Fsipv6RoutePfxLength,
                         INT4 i4Fsipv6RouteProtocol,
                         tSNMP_OCTET_STRING_TYPE * pFsipv6RouteNextHop,
                         UINT4 *pu4RetValFsipv6RouteMetric)
{
    tIp6Addr            ip6Addr;
    tIp6Addr            NextHop;
    tNetIpv6RtInfo      NetIpv6RtInfo;
    UINT4               u4ContextId;

    MEMSET (&ip6Addr, IP6_ZERO, sizeof (tIp6Addr));
    MEMSET (&NextHop, IP6_ZERO, sizeof (tIp6Addr));
    Ip6AddrCopy (&ip6Addr, (tIp6Addr *) (VOID *)
                 pFsipv6RouteDest->pu1_OctetList);
    Ip6AddrCopy (&NextHop, (tIp6Addr *) (VOID *)
                 pFsipv6RouteNextHop->pu1_OctetList);

    MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));
    u4ContextId = (UINT4) UtilRtm6GetCurrCxtId ();
    if (Rtm6ApiFindRtEntryInCxt (u4ContextId, &ip6Addr,
                                 (UINT1) i4Fsipv6RoutePfxLength,
                                 (INT1) i4Fsipv6RouteProtocol, &NextHop,
                                 &NetIpv6RtInfo) == RTM6_SUCCESS)
    {
        *pu4RetValFsipv6RouteMetric = NetIpv6RtInfo.u4Metric;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsipv6RouteType
 Input       :  The Indices
                Fsipv6RouteDest
                Fsipv6RoutePfxLength
                Fsipv6RouteProtocol
                Fsipv6RouteNextHop

                The Object 
                retValFsipv6RouteType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6RouteType (tSNMP_OCTET_STRING_TYPE * pFsipv6RouteDest,
                       INT4 i4Fsipv6RoutePfxLength,
                       INT4 i4Fsipv6RouteProtocol,
                       tSNMP_OCTET_STRING_TYPE * pFsipv6RouteNextHop,
                       INT4 *pi4RetValFsipv6RouteType)
{
    tIp6Addr            ip6Addr;
    tIp6Addr            NextHop;
    tNetIpv6RtInfo      NetIpv6RtInfo;
    UINT4               u4ContextId;

    MEMSET (&ip6Addr, IP6_ZERO, sizeof (tIp6Addr));
    MEMSET (&NextHop, IP6_ZERO, sizeof (tIp6Addr));
    Ip6AddrCopy (&ip6Addr,
                 (tIp6Addr *) (VOID *) pFsipv6RouteDest->pu1_OctetList);
    Ip6AddrCopy (&NextHop,
                 (tIp6Addr *) (VOID *) pFsipv6RouteNextHop->pu1_OctetList);

    MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));
    u4ContextId = (UINT4) UtilRtm6GetCurrCxtId ();
    if (Rtm6ApiFindRtEntryInCxt (u4ContextId, &ip6Addr,
                                 (UINT1) i4Fsipv6RoutePfxLength,
                                 (INT1) i4Fsipv6RouteProtocol, &NextHop,
                                 &NetIpv6RtInfo) == RTM6_SUCCESS)
    {
        *pi4RetValFsipv6RouteType = (INT4) NetIpv6RtInfo.i1Type;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsipv6RouteTag
 Input       :  The Indices
                Fsipv6RouteDest
                Fsipv6RoutePfxLength
                Fsipv6RouteProtocol
                Fsipv6RouteNextHop

                The Object 
                retValFsipv6RouteTag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6RouteTag (tSNMP_OCTET_STRING_TYPE * pFsipv6RouteDest,
                      INT4 i4Fsipv6RoutePfxLength,
                      INT4 i4Fsipv6RouteProtocol,
                      tSNMP_OCTET_STRING_TYPE * pFsipv6RouteNextHop,
                      UINT4 *pu4RetValFsipv6RouteTag)
{
    tIp6Addr            ip6Addr;
    tIp6Addr            NextHop;
    tNetIpv6RtInfo      NetIpv6RtInfo;
    UINT4               u4ContextId;

    MEMSET (&ip6Addr, IP6_ZERO, sizeof (tIp6Addr));
    MEMSET (&NextHop, IP6_ZERO, sizeof (tIp6Addr));
    Ip6AddrCopy (&ip6Addr,
                 (tIp6Addr *) (VOID *) pFsipv6RouteDest->pu1_OctetList);
    Ip6AddrCopy (&NextHop,
                 (tIp6Addr *) (VOID *) pFsipv6RouteNextHop->pu1_OctetList);

    MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));
    u4ContextId = (UINT4) UtilRtm6GetCurrCxtId ();
    if (Rtm6ApiFindRtEntryInCxt (u4ContextId,
                                 &ip6Addr, (UINT1) i4Fsipv6RoutePfxLength,
                                 (INT1) i4Fsipv6RouteProtocol, &NextHop,
                                 &NetIpv6RtInfo) == RTM6_SUCCESS)
    {
        *pu4RetValFsipv6RouteTag = NetIpv6RtInfo.u4RouteTag;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsipv6RouteAge
 Input       :  The Indices
                Fsipv6RouteDest
                Fsipv6RoutePfxLength
                Fsipv6RouteProtocol
                Fsipv6RouteNextHop

                The Object 
                retValFsipv6RouteAge
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6RouteAge (tSNMP_OCTET_STRING_TYPE * pFsipv6RouteDest,
                      INT4 i4Fsipv6RoutePfxLength, INT4 i4Fsipv6RouteProtocol,
                      tSNMP_OCTET_STRING_TYPE * pFsipv6RouteNextHop,
                      INT4 *pi4RetValFsipv6RouteAge)
{
    tIp6Addr            ip6Addr;
    tIp6Addr            NextHop;
    UINT4               u4SysTime;
    tNetIpv6RtInfo      NetIpv6RtInfo;
    UINT4               u4ContextId;

    MEMSET (&ip6Addr, IP6_ZERO, sizeof (tIp6Addr));
    MEMSET (&NextHop, IP6_ZERO, sizeof (tIp6Addr));
    MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));
    u4ContextId = (UINT4) UtilRtm6GetCurrCxtId ();
    Ip6AddrCopy (&ip6Addr,
                 (tIp6Addr *) (VOID *) pFsipv6RouteDest->pu1_OctetList);
    Ip6AddrCopy (&NextHop,
                 (tIp6Addr *) (VOID *) pFsipv6RouteNextHop->pu1_OctetList);

    if (Rtm6ApiFindRtEntryInCxt (u4ContextId,
                                 &ip6Addr, (UINT1) i4Fsipv6RoutePfxLength,
                                 (INT1) i4Fsipv6RouteProtocol, &NextHop,
                                 &NetIpv6RtInfo) == RTM6_SUCCESS)
    {
        if (NetIpv6RtInfo.i1Proto == IP6_NETMGMT_PROTOID ||
            NetIpv6RtInfo.i1Proto == IP6_LOCAL_PROTOID)
        {
            *pi4RetValFsipv6RouteAge = 0;
            return SNMP_SUCCESS;
        }

        u4SysTime = OsixGetSysUpTime ();
        *pi4RetValFsipv6RouteAge = (u4SysTime - NetIpv6RtInfo.u4ChangeTime);
        return SNMP_SUCCESS;

    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsipv6RouteAdminStatus
 Input       :  The Indices
                Fsipv6RouteDest
                Fsipv6RoutePfxLength
                Fsipv6RouteProtocol
                Fsipv6RouteNextHop

                The Object 
                retValFsipv6RouteAdminStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6RouteAdminStatus (tSNMP_OCTET_STRING_TYPE * pFsipv6RouteDest,
                              INT4 i4Fsipv6RoutePfxLength,
                              INT4 i4Fsipv6RouteProtocol,
                              tSNMP_OCTET_STRING_TYPE * pFsipv6RouteNextHop,
                              INT4 *pi4RetValFsipv6RouteAdminStatus)
{
    tIp6Addr            ip6Addr;
    tIp6Addr            NextHop;
    tNetIpv6RtInfo      NetIpv6RtInfo;
    UINT4               u4ContextId;

    MEMSET (&ip6Addr, IP6_ZERO, sizeof (tIp6Addr));
    MEMSET (&NextHop, IP6_ZERO, sizeof (tIp6Addr));
    MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));
    u4ContextId = (UINT4) UtilRtm6GetCurrCxtId ();
    Ip6AddrCopy (&ip6Addr,
                 (tIp6Addr *) (VOID *) pFsipv6RouteDest->pu1_OctetList);
    Ip6AddrCopy (&NextHop,
                 (tIp6Addr *) (VOID *) pFsipv6RouteNextHop->pu1_OctetList);

    if (Rtm6ApiFindRtEntryInCxt (u4ContextId,
                                 &ip6Addr, (UINT1) i4Fsipv6RoutePfxLength,
                                 (INT1) i4Fsipv6RouteProtocol, &NextHop,
                                 &NetIpv6RtInfo) == RTM6_SUCCESS)
    {
        *pi4RetValFsipv6RouteAdminStatus = NetIpv6RtInfo.u4RowStatus;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 *  Function    :  nmhGetFsipv6RouteAddrType
 *  Input       :  The Indices
 *                 Fsipv6RouteDest
 *                 Fsipv6RoutePfxLength
 *                 Fsipv6RouteProtocol
 *                 Fsipv6RouteNextHop
 *
 *                 The Object 
 *                 retValFsipv6RouteAddrType
 * Output      :  The Get Low Lev Routine Take the Indices &
 * store the Value requested in the Return val.
 * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ****************************************************************************/
INT1
nmhGetFsipv6RouteAddrType (tSNMP_OCTET_STRING_TYPE * pFsipv6RouteDest,
                           INT4 i4Fsipv6RoutePfxLength,
                           INT4 i4Fsipv6RouteProtocol,
                           tSNMP_OCTET_STRING_TYPE * pFsipv6RouteNextHop,
                           INT4 *pi4RetValFsipv6RouteAddrType)
{
    tIp6Addr            ip6Addr;
    tIp6Addr            NextHop;
    tNetIpv6RtInfo      NetIpv6RtInfo;
    UINT4               u4ContextId;
    MEMSET (&ip6Addr, IP6_ZERO, sizeof (tIp6Addr));
    MEMSET (&NextHop, IP6_ZERO, sizeof (tIp6Addr));
    Ip6AddrCopy (&ip6Addr, (tIp6Addr *) (VOID *)
                 pFsipv6RouteDest->pu1_OctetList);
    Ip6AddrCopy (&NextHop, (tIp6Addr *) (VOID *)
                 pFsipv6RouteNextHop->pu1_OctetList);
    u4ContextId = (UINT4) UtilRtm6GetCurrCxtId ();
    if (Rtm6ApiFindRtEntryInCxt (u4ContextId, &ip6Addr,
                                 (UINT1) i4Fsipv6RoutePfxLength,
                                 (INT1) i4Fsipv6RouteProtocol, &NextHop,
                                 &NetIpv6RtInfo) == RTM6_SUCCESS)
    {
        *pi4RetValFsipv6RouteAddrType = NetIpv6RtInfo.u1AddrType;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 *  Function    :  nmhGetFsipv6RouteHwStatus
 *  Input       :  The Indices
 *                 Fsipv6RouteDest
 *                 Fsipv6RoutePfxLength
 *                 Fsipv6RouteProtocol
 *                 Fsipv6RouteNextHop
 *
 *                 The Object 
 *                 retValFsipv6RouteHwStatus
 * Output      :  The Get Low Lev Routine Take the Indices &
 * store the Value requested in the Return val.
 * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ****************************************************************************/
INT1
nmhGetFsipv6RouteHwStatus (tSNMP_OCTET_STRING_TYPE * pFsipv6RouteDest,
                           INT4 i4Fsipv6RoutePfxLength,
                           INT4 i4Fsipv6RouteProtocol,
                           tSNMP_OCTET_STRING_TYPE * pFsipv6RouteNextHop,
                           INT4 *pi4RetValFsipv6RouteHwStatus)
{
    tIp6Addr            ip6Addr;
    tIp6Addr            NextHop;
    tNetIpv6RtInfo      NetIpv6RtInfo;
    UINT4               u4ContextId;
    MEMSET (&ip6Addr, IP6_ZERO, sizeof (tIp6Addr));
    MEMSET (&NextHop, IP6_ZERO, sizeof (tIp6Addr));
    Ip6AddrCopy (&ip6Addr, (tIp6Addr *) (VOID *)
                 pFsipv6RouteDest->pu1_OctetList);
    Ip6AddrCopy (&NextHop, (tIp6Addr *) (VOID *)
                 pFsipv6RouteNextHop->pu1_OctetList);
    u4ContextId = (UINT4) UtilRtm6GetCurrCxtId ();
    if (Rtm6ApiFindRtEntryInCxt (u4ContextId, &ip6Addr,
                                 (UINT1) i4Fsipv6RoutePfxLength,
                                 (INT1) i4Fsipv6RouteProtocol, &NextHop,
                                 &NetIpv6RtInfo) == RTM6_SUCCESS)
    {
        *pi4RetValFsipv6RouteHwStatus = (INT4) NetIpv6RtInfo.u1HwStatus;
        return SNMP_SUCCESS;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6RoutePreference
 Input       :  The Indices
                Fsipv6RouteDest
                Fsipv6RoutePfxLength
                Fsipv6RouteProtocol
                Fsipv6RouteNextHop

                The Object
                retValFsipv6RoutePreference
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6RoutePreference (tSNMP_OCTET_STRING_TYPE * pFsipv6RouteDest,
                             INT4 i4Fsipv6RoutePfxLength,
                             INT4 i4Fsipv6RouteProtocol,
                             tSNMP_OCTET_STRING_TYPE * pFsipv6RouteNextHop,
                             INT4 *pi4RetValFsipv6RoutePreference)
{
    tIp6Addr            ip6Addr;
    tIp6Addr            NextHop;
    tNetIpv6RtInfo      NetIpv6RtInfo;
    UINT4               u4ContextId = IP6_ZERO;

    MEMSET (&ip6Addr, IP6_ZERO, sizeof (tIp6Addr));
    MEMSET (&NextHop, IP6_ZERO, sizeof (tIp6Addr));
    Ip6AddrCopy (&ip6Addr, (tIp6Addr *) (VOID *)
                 pFsipv6RouteDest->pu1_OctetList);
    Ip6AddrCopy (&NextHop, (tIp6Addr *) (VOID *)
                 pFsipv6RouteNextHop->pu1_OctetList);

    MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));
    u4ContextId = (UINT4) UtilRtm6GetCurrCxtId ();
    if (Rtm6ApiFindRtEntryInCxt (u4ContextId, &ip6Addr,
                                 (UINT1) i4Fsipv6RoutePfxLength,
                                 (INT1) i4Fsipv6RouteProtocol, &NextHop,
                                 &NetIpv6RtInfo) == RTM6_SUCCESS)
    {
        *pi4RetValFsipv6RoutePreference = (INT4) NetIpv6RtInfo.u1Preference;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsipv6RouteIfIndex
 Input       :  The Indices
                Fsipv6RouteDest
                Fsipv6RoutePfxLength
                Fsipv6RouteProtocol
                Fsipv6RouteNextHop

                The Object 
                setValFsipv6RouteIfIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6RouteIfIndex (tSNMP_OCTET_STRING_TYPE * pFsipv6RouteDest,
                          INT4 i4Fsipv6RoutePfxLength,
                          INT4 i4Fsipv6RouteProtocol,
                          tSNMP_OCTET_STRING_TYPE * pFsipv6RouteNextHop,
                          INT4 i4SetValFsipv6RouteIfIndex)
{
    tIp6Addr            ip6Addr;
    tIp6Addr            NextHop;
    UINT4               u4ContextId;
    tNd6CacheEntry     *pNd6c = NULL;
    tNetIpv6RtInfo      NetIp6RtInfo;
    tIp6If             *pTempRouteIndex = NULL;

    MEMSET (&ip6Addr, IP6_ZERO, sizeof (tIp6Addr));
    MEMSET (&NextHop, IP6_ZERO, sizeof (tIp6Addr));
    u4ContextId = UtilRtm6GetCurrCxtId ();
    Ip6AddrCopy (&ip6Addr,
                 (tIp6Addr *) (VOID *) pFsipv6RouteDest->pu1_OctetList);
    Ip6AddrCopy (&NextHop,
                 (tIp6Addr *) (VOID *) pFsipv6RouteNextHop->pu1_OctetList);

    if (!IS_ADDR_UNSPECIFIED (NextHop) && !IS_ADDR_LLOCAL (NextHop))
    {
        RTM6_TASK_UNLOCK ();
        IP6_TASK_LOCK ();
        pTempRouteIndex =
            Ip6GetRouteInCxt (u4ContextId, &NextHop, &pNd6c, &NetIp6RtInfo);
        IP6_TASK_UNLOCK ();
        RTM6_TASK_LOCK ();
        if (pTempRouteIndex != NULL)
        {
            if (NetIp6RtInfo.u4Index != (UINT4) i4SetValFsipv6RouteIfIndex)
            {
                CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE_FOR_NEXTHOP);
                IP6_TASK_UNLOCK ();
                return SNMP_FAILURE;
            }
        }
    }
    if (Rtm6ApiSetRtParamsInCxt (u4ContextId,
                                 &ip6Addr, (UINT1) i4Fsipv6RoutePfxLength,
                                 (INT1) i4Fsipv6RouteProtocol, &NextHop,
                                 RTM6_ROUTE_INDEX, i4SetValFsipv6RouteIfIndex)
        == RTM6_SUCCESS)
    {
        IncMsrForIpv6RouteTable (u4ContextId, pFsipv6RouteDest,
                                 i4Fsipv6RoutePfxLength,
                                 pFsipv6RouteNextHop, 'i',
                                 &i4SetValFsipv6RouteIfIndex,
                                 FsMIStdInetCidrRouteIfIndex,
                                 sizeof (FsMIStdInetCidrRouteIfIndex) /
                                 sizeof (UINT4), FALSE);

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsipv6RouteMetric
 Input       :  The Indices
                Fsipv6RouteDest
                Fsipv6RoutePfxLength
                Fsipv6RouteProtocol
                Fsipv6RouteNextHop

                The Object 
                setValFsipv6RouteMetric
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6RouteMetric (tSNMP_OCTET_STRING_TYPE * pFsipv6RouteDest,
                         INT4 i4Fsipv6RoutePfxLength,
                         INT4 i4Fsipv6RouteProtocol,
                         tSNMP_OCTET_STRING_TYPE * pFsipv6RouteNextHop,
                         UINT4 u4SetValFsipv6RouteMetric)
{
    tIp6Addr            ip6Addr;
    tIp6Addr            NextHop;
    tNetIpv6RtInfo      NetIpv6RtInfo;
    UINT4               u4ContextId;

    MEMSET (&ip6Addr, IP6_ZERO, sizeof (tIp6Addr));
    MEMSET (&NextHop, IP6_ZERO, sizeof (tIp6Addr));
    MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));
    u4ContextId = UtilRtm6GetCurrCxtId ();
    Ip6AddrCopy (&ip6Addr,
                 (tIp6Addr *) (VOID *) pFsipv6RouteDest->pu1_OctetList);
    Ip6AddrCopy (&NextHop,
                 (tIp6Addr *) (VOID *) pFsipv6RouteNextHop->pu1_OctetList);

    if (Rtm6ApiFindRtEntryInCxt (u4ContextId, &ip6Addr,
                                 (UINT1) i4Fsipv6RoutePfxLength,
                                 (INT1) i4Fsipv6RouteProtocol, &NextHop,
                                 &NetIpv6RtInfo) == RTM6_SUCCESS)
    {
        /* For Metric dont just update the metric Value. Because, the route
         * will be getting re-ordered as per the new metric Value. */
        Ip6AddRouteInCxt (u4ContextId, &NetIpv6RtInfo.Ip6Dst,
                          NetIpv6RtInfo.u1Prefixlen,
                          &NetIpv6RtInfo.NextHop, u4SetValFsipv6RouteMetric,
                          (INT4) NetIpv6RtInfo.u1Preference,
                          NetIpv6RtInfo.i1Type, NetIpv6RtInfo.i1Proto,
                          NetIpv6RtInfo.u4Index, NetIpv6RtInfo.u1AddrType,
                          NetIpv6RtInfo.u4RouteTag, NetIpv6RtInfo.u4RowStatus);
        IncMsrForIpv6RouteTable (u4ContextId, pFsipv6RouteDest,
                                 i4Fsipv6RoutePfxLength,
                                 pFsipv6RouteNextHop,
                                 'u', &u4SetValFsipv6RouteMetric,
                                 FsMIStdInetCidrRouteMetric1,
                                 sizeof (FsMIStdInetCidrRouteMetric1) /
                                 sizeof (UINT4), FALSE);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsipv6RouteType
 Input       :  The Indices
                Fsipv6RouteDest
                Fsipv6RoutePfxLength
                Fsipv6RouteProtocol
                Fsipv6RouteNextHop

                The Object 
                setValFsipv6RouteType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6RouteType (tSNMP_OCTET_STRING_TYPE * pFsipv6RouteDest,
                       INT4 i4Fsipv6RoutePfxLength,
                       INT4 i4Fsipv6RouteProtocol,
                       tSNMP_OCTET_STRING_TYPE * pFsipv6RouteNextHop,
                       INT4 i4SetValFsipv6RouteType)
{
    tIp6Addr            dest;
    tIp6Addr            NextHop;
    UINT4               u4ContextId;

    MEMSET (&dest, IP6_ZERO, sizeof (tIp6Addr));
    MEMSET (&NextHop, IP6_ZERO, sizeof (tIp6Addr));
    Ip6AddrCopy (&NextHop,
                 (tIp6Addr *) (VOID *) pFsipv6RouteNextHop->pu1_OctetList);
    Ip6AddrCopy (&dest, (tIp6Addr *) (VOID *) pFsipv6RouteDest->pu1_OctetList);

    u4ContextId = UtilRtm6GetCurrCxtId ();
    if (Rtm6ApiSetRtParamsInCxt (u4ContextId,
                                 &dest, (UINT1) i4Fsipv6RoutePfxLength,
                                 (INT1) i4Fsipv6RouteProtocol, &NextHop,
                                 RTM6_ROUTE_TYPE, i4SetValFsipv6RouteType)
        == RTM6_SUCCESS)
    {
        IncMsrForIpv6RouteTable (u4ContextId, pFsipv6RouteDest,
                                 i4Fsipv6RoutePfxLength,
                                 pFsipv6RouteNextHop,
                                 'i', &i4SetValFsipv6RouteType,
                                 FsMIStdInetCidrRouteType,
                                 sizeof (FsMIStdInetCidrRouteType) /
                                 sizeof (UINT4), FALSE);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsipv6RouteTag
 Input       :  The Indices
                Fsipv6RouteDest
                Fsipv6RoutePfxLength
                Fsipv6RouteProtocol
                Fsipv6RouteNextHop

                The Object 
                setValFsipv6RouteTag
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6RouteTag (tSNMP_OCTET_STRING_TYPE * pFsipv6RouteDest,
                      INT4 i4Fsipv6RoutePfxLength,
                      INT4 i4Fsipv6RouteProtocol,
                      tSNMP_OCTET_STRING_TYPE * pFsipv6RouteNextHop,
                      UINT4 u4SetValFsipv6RouteTag)
{

    tIp6Addr            ip6Addr;
    tIp6Addr            NextHop;
    UINT4               u4ContextId;

    MEMSET (&ip6Addr, IP6_ZERO, sizeof (tIp6Addr));
    MEMSET (&NextHop, IP6_ZERO, sizeof (tIp6Addr));
    Ip6AddrCopy (&ip6Addr,
                 (tIp6Addr *) (VOID *) pFsipv6RouteDest->pu1_OctetList);
    Ip6AddrCopy (&NextHop,
                 (tIp6Addr *) (VOID *) pFsipv6RouteNextHop->pu1_OctetList);

    u4ContextId = UtilRtm6GetCurrCxtId ();
    if (Rtm6ApiSetRtParamsInCxt (u4ContextId,
                                 &ip6Addr, (UINT1) i4Fsipv6RoutePfxLength,
                                 (INT1) i4Fsipv6RouteProtocol, &NextHop,
                                 RTM6_ROUTE_TAG, u4SetValFsipv6RouteTag)
        == RTM6_SUCCESS)
    {
        IncMsrForIpv6RouteTable (u4ContextId, pFsipv6RouteDest,
                                 i4Fsipv6RoutePfxLength,
                                 pFsipv6RouteNextHop,
                                 'u', &u4SetValFsipv6RouteTag,
                                 FsMIStdInetCidrRouteNextHopAS,
                                 sizeof (FsMIStdInetCidrRouteNextHopAS) /
                                 sizeof (UINT4), FALSE);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsipv6RouteAdminStatus
 Input       :  The Indices
                Fsipv6RouteDest
                Fsipv6RoutePfxLength
                Fsipv6RouteProtocol
                Fsipv6RouteNextHop

                The Object 
                setValFsipv6RouteAdminStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6RouteAdminStatus (tSNMP_OCTET_STRING_TYPE * pFsipv6RouteDest,
                              INT4 i4Fsipv6RoutePfxLength,
                              INT4 i4Fsipv6RouteProtocol,
                              tSNMP_OCTET_STRING_TYPE * pFsipv6RouteNextHop,
                              INT4 i4SetValFsipv6RouteAdminStatus)
{
    tNetIpv6RtInfo      NetIpv6RtInfo;
    tIp6Addr            ip6Addr;
    tIp6Addr            NextHop;
    tIp6If             *pDstIf6 = NULL;
    INT1                i1Type;
    UINT4               u4ContextId;
    INT4                i4RoutePreference = IP6_ZERO;

    MEMSET (&ip6Addr, IP6_ZERO, sizeof (tIp6Addr));
    MEMSET (&NextHop, IP6_ZERO, sizeof (tIp6Addr));
    MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));
    u4ContextId = UtilRtm6GetCurrCxtId ();
    Ip6AddrCopy (&ip6Addr,
                 (tIp6Addr *) (VOID *) pFsipv6RouteDest->pu1_OctetList);
    Ip6AddrCopy (&NextHop,
                 (tIp6Addr *) (VOID *) pFsipv6RouteNextHop->pu1_OctetList);

    switch (i4SetValFsipv6RouteAdminStatus)
    {
        case IP6FWD_CREATE_AND_WAIT:
            /* Create the Route with the Default Values. */
            if (IS_ADDR_UNSPECIFIED (NextHop))
            {
                /* Direct Route */
                i1Type = IP6_ROUTE_TYPE_DIRECT;
            }
            else
            {
                /* Possibly indirect Route. */
                i1Type = IP6_ROUTE_TYPE_INDIRECT;
            }

            /* If best route is already aviable for the destination (connected
             * route), and that route's interface oper status/admin status
             * in in down state, then remove the best route and add the new
             * route in the CIDR table.
             * For example, 3000::1 interface is created in the system and
             * ipv6 is not enabled in the interface. Then static route for
             * the same network 3000:: is added with an another valid next
             * hop (another ipv6 interface), the connected route 3000::1
             * should be removed and the static route (3000::) should be added
             * in the CIDR table */

            if (Rtm6ApiGetBestRouteEntryInCxt (u4ContextId, &ip6Addr,
                                               (UINT1) i4Fsipv6RoutePfxLength,
                                               0,
                                               &NetIpv6RtInfo) == RTM6_SUCCESS)
            {
                pDstIf6 = gIp6GblInfo.apIp6If[NetIpv6RtInfo.u4Index];

                if ((NetIpv6RtInfo.i1Proto == IP6_LOCAL_PROTOID) &&
                    (pDstIf6 != NULL) &&
                    (Ip6SendIfCheck (pDstIf6) != IP6_SUCCESS))
                {
                    if (Ip6DelRouteInCxt (u4ContextId, &ip6Addr,
                                          (UINT1) i4Fsipv6RoutePfxLength,
                                          (INT1) NetIpv6RtInfo.i1Proto,
                                          &(NetIpv6RtInfo.NextHop)) ==
                        IP6_FAILURE)
                    {
                        return SNMP_FAILURE;
                    }
                }
            }

            /* Get the default Adminstrative distance for given context */
            if ((nmhGetFsRtm6StaticRouteDistance (&i4RoutePreference)) !=
                SNMP_SUCCESS)
            {
                return SNMP_FAILURE;
            }

            if (Ip6AddRouteInCxt (u4ContextId, &ip6Addr,
                                  (UINT1) i4Fsipv6RoutePfxLength, &NextHop,
                                  1 /* Default Metric */ , i4RoutePreference,
                                  i1Type, (INT1) i4Fsipv6RouteProtocol, 0,    /* Index should be set before ACTIVE is set */
                                  0, 0,    /* Route Tag Also Defaults to 0 */
                                  IP6FWD_NOT_READY) == IP6_FAILURE)
            {
                return SNMP_FAILURE;
            }
            break;

        case IP6FWD_ACTIVE:
            if (Rtm6ApiFindRtEntryInCxt (u4ContextId, &ip6Addr,
                                         (UINT1) i4Fsipv6RoutePfxLength,
                                         (INT1) i4Fsipv6RouteProtocol, &NextHop,
                                         &NetIpv6RtInfo) == RTM6_FAILURE)
            {
                return SNMP_FAILURE;
            }

            /* If Index Should be set before Admin Status is made ACTIVE */

            if (IP6_TASK_LOCK () == SNMP_FAILURE)
            {
                return SNMP_FAILURE;
            }

            if (NetIpv6RtInfo.u4Index == 0)
            {
                IP6_TASK_UNLOCK ();
                CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE_FOR_NEXTHOP);
                return SNMP_FAILURE;
            }
            if (Ip6ifEntryExists (NetIpv6RtInfo.u4Index) == IP6_FAILURE)
            {
                IP6_TASK_UNLOCK ();
                return SNMP_FAILURE;
            }
            IP6_TASK_UNLOCK ();

            if (NetIpv6RtInfo.u4RowStatus == IP6FWD_ACTIVE)
            {
                IncMsrForIpv6RouteTable (u4ContextId, pFsipv6RouteDest,
                                         i4Fsipv6RoutePfxLength,
                                         pFsipv6RouteNextHop, 'u',
                                         &i4SetValFsipv6RouteAdminStatus,
                                         FsMIStdInetCidrRouteStatus,
                                         sizeof (FsMIStdInetCidrRouteStatus) /
                                         sizeof (UINT4), TRUE);
                return SNMP_SUCCESS;
            }

            /* Valid Index Exists. Make the entry as ACTIVE. */
            if (Ip6AddRouteInCxt (u4ContextId, &ip6Addr,
                                  (UINT1) i4Fsipv6RoutePfxLength, &NextHop,
                                  NetIpv6RtInfo.u4Metric,
                                  (INT4) NetIpv6RtInfo.u1Preference,
                                  NetIpv6RtInfo.i1Type,
                                  (INT1) i4Fsipv6RouteProtocol,
                                  NetIpv6RtInfo.u4Index,
                                  (UINT1) NetIpv6RtInfo.u1AddrType,
                                  NetIpv6RtInfo.u4RouteTag,
                                  IP6FWD_ACTIVE) == IP6_FAILURE)
            {
                return SNMP_FAILURE;
            }
            break;

        case IP6FWD_NOT_IN_SERVICE:
            if (Rtm6ApiFindRtEntryInCxt (u4ContextId, &ip6Addr,
                                         (UINT1) i4Fsipv6RoutePfxLength,
                                         (INT1) i4Fsipv6RouteProtocol,
                                         &NextHop,
                                         &NetIpv6RtInfo) == RTM6_FAILURE)
            {
                return SNMP_FAILURE;
            }

            if (NetIpv6RtInfo.u4RowStatus == IP6FWD_NOT_IN_SERVICE)
            {
                IncMsrForIpv6RouteTable (u4ContextId, pFsipv6RouteDest,
                                         i4Fsipv6RoutePfxLength,
                                         pFsipv6RouteNextHop, 'u',
                                         &i4SetValFsipv6RouteAdminStatus,
                                         FsMIStdInetCidrRouteStatus,
                                         sizeof (FsMIStdInetCidrRouteStatus) /
                                         sizeof (UINT4), TRUE);

                return SNMP_SUCCESS;
            }

            if (Ip6AddRouteInCxt (u4ContextId, &ip6Addr,
                                  (UINT1) i4Fsipv6RoutePfxLength, &NextHop,
                                  NetIpv6RtInfo.u4Metric,
                                  (INT4) NetIpv6RtInfo.u1Preference,
                                  NetIpv6RtInfo.i1Type,
                                  (INT1) i4Fsipv6RouteProtocol,
                                  NetIpv6RtInfo.u4Index,
                                  NetIpv6RtInfo.u1AddrType,
                                  NetIpv6RtInfo.u4RouteTag,
                                  IP6FWD_NOT_IN_SERVICE) == IP6_FAILURE)
            {
                return SNMP_FAILURE;
            }
            break;

        case IP6FWD_DESTROY:
            if (Ip6DelRouteInCxt (u4ContextId, &ip6Addr,
                                  (UINT1) i4Fsipv6RoutePfxLength,
                                  (INT1) i4Fsipv6RouteProtocol, &NextHop) ==
                IP6_FAILURE)
            {
                return SNMP_FAILURE;
            }
            break;

        default:
            return SNMP_FAILURE;
    }
    IncMsrForIpv6RouteTable (u4ContextId, pFsipv6RouteDest,
                             i4Fsipv6RoutePfxLength, pFsipv6RouteNextHop,
                             'u', &i4SetValFsipv6RouteAdminStatus,
                             FsMIStdInetCidrRouteStatus,
                             sizeof (FsMIStdInetCidrRouteStatus) /
                             sizeof (UINT4), TRUE);

    return SNMP_SUCCESS;
}

/***********************************************************************
Function    :  nmhSetFsipv6RouteAddrType
 Input       :  The Indices
                Fsipv6RouteDest
                Fsipv6RoutePfxLength
                Fsipv6RouteProtocol
                Fsipv6RouteNextHop

                The Object 
                setValFsipv6RouteAddrType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6RouteAddrType (tSNMP_OCTET_STRING_TYPE * pFsipv6RouteDest,
                           INT4 i4Fsipv6RoutePfxLength,
                           INT4 i4Fsipv6RouteProtocol,
                           tSNMP_OCTET_STRING_TYPE * pFsipv6RouteNextHop,
                           INT4 i4SetValFsipv6RouteAddrType)
{
    tIp6Addr            ip6Addr;
    tIp6Addr            NextHop;
    UINT4               u4ContextId;

    MEMSET (&ip6Addr, IP6_ZERO, sizeof (tIp6Addr));
    MEMSET (&NextHop, IP6_ZERO, sizeof (tIp6Addr));
    u4ContextId = UtilRtm6GetCurrCxtId ();
    Ip6AddrCopy (&ip6Addr,
                 (tIp6Addr *) (VOID *) pFsipv6RouteDest->pu1_OctetList);
    Ip6AddrCopy (&NextHop,
                 (tIp6Addr *) (VOID *) pFsipv6RouteNextHop->pu1_OctetList);

    if (Rtm6ApiSetRtParamsInCxt (u4ContextId,
                                 &ip6Addr, (UINT1) i4Fsipv6RoutePfxLength,
                                 (INT1) i4Fsipv6RouteProtocol, &NextHop,
                                 RTM6_ROUTE_ADDR_TYPE,
                                 (UINT1) i4SetValFsipv6RouteAddrType) ==
        RTM6_SUCCESS)
    {
        IncMsrForIpv6RouteTable (u4ContextId, pFsipv6RouteDest,
                                 i4Fsipv6RoutePfxLength,
                                 pFsipv6RouteNextHop, 'i',
                                 &i4SetValFsipv6RouteAddrType,
                                 FsMIStdInetCidrRouteAddrType,
                                 sizeof (FsMIStdInetCidrRouteIfIndex) /
                                 sizeof (UINT4), FALSE);

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetFsipv6RoutePreference
 Input       :  The Indices
                Fsipv6RouteDest
                Fsipv6RoutePfxLength
                Fsipv6RouteProtocol
                Fsipv6RouteNextHop

                The Object
                setValFsipv6RoutePreference
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6RoutePreference (tSNMP_OCTET_STRING_TYPE * pFsipv6RouteDest,
                             INT4 i4Fsipv6RoutePfxLength,
                             INT4 i4Fsipv6RouteProtocol,
                             tSNMP_OCTET_STRING_TYPE * pFsipv6RouteNextHop,
                             INT4 i4SetValFsipv6RoutePreference)
{

    tIp6Addr            ip6Addr;
    tIp6Addr            NextHop;
    tNetIpv6RtInfo      NetIpv6RtInfo;
    UINT4               u4ContextId;

    MEMSET (&ip6Addr, IP6_ZERO, sizeof (tIp6Addr));
    MEMSET (&NextHop, IP6_ZERO, sizeof (tIp6Addr));
    MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));
    u4ContextId = (UINT4) UtilRtm6GetCurrCxtId ();
    Ip6AddrCopy (&ip6Addr,
                 (tIp6Addr *) (VOID *) pFsipv6RouteDest->pu1_OctetList);
    Ip6AddrCopy (&NextHop,
                 (tIp6Addr *) (VOID *) pFsipv6RouteNextHop->pu1_OctetList);

    if (Rtm6ApiFindRtEntryInCxt (u4ContextId, &ip6Addr,
                                 (UINT1) i4Fsipv6RoutePfxLength,
                                 (INT1) i4Fsipv6RouteProtocol, &NextHop,
                                 &NetIpv6RtInfo) == RTM6_SUCCESS)
    {
        /* For RoutePreference dont just update the AD Value. Because, the route
         * will be getting re-ordered as per the new AD Value. */
        Ip6AddRouteInCxt (u4ContextId, &NetIpv6RtInfo.Ip6Dst,
                          NetIpv6RtInfo.u1Prefixlen,
                          &NetIpv6RtInfo.NextHop, IP6_ONE,
                          (INT4) i4SetValFsipv6RoutePreference,
                          NetIpv6RtInfo.i1Type, NetIpv6RtInfo.i1Proto,
                          NetIpv6RtInfo.u4Index, NetIpv6RtInfo.u1AddrType,
                          NetIpv6RtInfo.u4RouteTag, NetIpv6RtInfo.u4RowStatus);
        IncMsrForIpv6RouteTable (u4ContextId, pFsipv6RouteDest,
                                 i4Fsipv6RoutePfxLength,
                                 pFsipv6RouteNextHop,
                                 'i', &i4SetValFsipv6RoutePreference,
                                 FsMIStdInetCidrRoutePreference,
                                 sizeof (FsMIStdInetCidrRoutePreference) /
                                 sizeof (UINT4), FALSE);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Fsipv6RouteIfIndex
 Input       :  The Indices
                Fsipv6RouteDest
                Fsipv6RoutePfxLength
                Fsipv6RouteProtocol
                Fsipv6RouteNextHop

                The Object 
                testValFsipv6RouteIfIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6RouteIfIndex (UINT4 *pu4ErrorCode,
                             tSNMP_OCTET_STRING_TYPE * pFsipv6RouteDest,
                             INT4 i4Fsipv6RoutePfxLength,
                             INT4 i4Fsipv6RouteProtocol,
                             tSNMP_OCTET_STRING_TYPE * pFsipv6RouteNextHop,
                             INT4 i4TestValFsipv6RouteIfIndex)
{
    tIp6Addr            ip6addr;
    UINT4               u4IfContextId = IP6_ZERO;
    UINT4               u4ContextId = IP6_ZERO;
    tIp6Addr            NextHop;
    tNd6CacheEntry     *pNd6c = NULL;
    tNetIpv6RtInfo      NetIp6RtInfo;

    MEMSET (&ip6addr, IP6_ZERO, sizeof (tIp6Addr));
    MEMSET (&NetIp6RtInfo, 0, sizeof (tNetIpv6RtInfo));

    Ip6AddrCopy (&NextHop,
                 (tIp6Addr *) (VOID *) pFsipv6RouteNextHop->pu1_OctetList);

    u4ContextId = UtilRtm6GetCurrCxtId ();
    VcmGetContextIdFromCfaIfIndex (i4TestValFsipv6RouteIfIndex, &u4IfContextId);
    if (u4ContextId != u4IfContextId)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (IP6_TASK_LOCK () == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (!IS_ADDR_UNSPECIFIED (NextHop) && !IS_ADDR_LLOCAL (NextHop))
    {
        RTM6_TASK_UNLOCK ();
        Ip6GetRouteInCxt (u4ContextId, &NextHop, &pNd6c, &NetIp6RtInfo);
        RTM6_TASK_LOCK ();

        if (NetIp6RtInfo.u4Index != (UINT4) i4TestValFsipv6RouteIfIndex)
        {
            CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE_FOR_NEXTHOP);
            IP6_TASK_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    if (Ip6ifEntryExists ((UINT4) i4TestValFsipv6RouteIfIndex) == IP6_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        IP6_TASK_UNLOCK ();
        return SNMP_FAILURE;
    }
    IP6_TASK_UNLOCK ();

    *pu4ErrorCode = SNMP_ERR_NO_CREATION;

    if (pFsipv6RouteDest == NULL)
    {
        return SNMP_FAILURE;
    }

    Ip6AddrCopy (&ip6addr,
                 (tIp6Addr *) (VOID *) pFsipv6RouteDest->pu1_OctetList);

    if ((IS_ADDR_MULTI (ip6addr)) || (IS_ADDR_LLOCAL (ip6addr)))
    {
        return SNMP_FAILURE;
    }

    if ((i4Fsipv6RoutePfxLength < IP6_ADDR_MIN_PREFIX) ||
        (i4Fsipv6RoutePfxLength > IP6_ADDR_MAX_PREFIX))
    {
        return SNMP_FAILURE;
    }

    if (i4Fsipv6RouteProtocol != IP6_NETMGMT_PROTOID)
    {
        /* Invalid Protocol Id */
        return SNMP_FAILURE;
    }

    Ip6AddrCopy (&ip6addr,
                 (tIp6Addr *) (VOID *) pFsipv6RouteNextHop->pu1_OctetList);

    if (IS_ADDR_MULTI (ip6addr) || ((IS_ADDR_LLOCAL (ip6addr)) &&
                                    (i4TestValFsipv6RouteIfIndex == 0)))
    {
        CLI_SET_ERR (CLI_IP6_INVALID_NEXTHOP);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6RouteMetric
 Input       :  The Indices
                Fsipv6RouteDest
                Fsipv6RoutePfxLength
                Fsipv6RouteProtocol
                Fsipv6RouteNextHop

                The Object 
                testValFsipv6RouteMetric
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6RouteMetric (UINT4 *pu4ErrorCode,
                            tSNMP_OCTET_STRING_TYPE * pFsipv6RouteDest,
                            INT4 i4Fsipv6RoutePfxLength,
                            INT4 i4Fsipv6RouteProtocol,
                            tSNMP_OCTET_STRING_TYPE * pFsipv6RouteNextHop,
                            UINT4 u4TestValFsipv6RouteMetric)
{
    tIp6Addr            ip6addr;

    MEMSET (&ip6addr, IP6_ZERO, sizeof (tIp6Addr));

    if (u4TestValFsipv6RouteMetric < IP6_ROUTE_MIN_METRIC)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_INVALID_ROUTE_METRIC);
        return SNMP_FAILURE;
    }

    *pu4ErrorCode = SNMP_ERR_NO_CREATION;

    if (pFsipv6RouteDest == NULL)
    {
        CLI_SET_ERR (CLI_IP6_INVALID_ADDRESS);
        return SNMP_FAILURE;
    }

    Ip6AddrCopy (&ip6addr,
                 (tIp6Addr *) (VOID *) pFsipv6RouteDest->pu1_OctetList);

    if ((IS_ADDR_MULTI (ip6addr)) || (IS_ADDR_LLOCAL (ip6addr)))
    {
        CLI_SET_ERR (CLI_IP6_INVALID_ADDR_TYPE);
        return SNMP_FAILURE;
    }

    if ((i4Fsipv6RoutePfxLength < IP6_ADDR_MIN_PREFIX) ||
        (i4Fsipv6RoutePfxLength > IP6_ADDR_MAX_PREFIX))
    {
        CLI_SET_ERR (CLI_IP6_INVALID_PREFIXLEN);
        return SNMP_FAILURE;
    }

    if ((i4Fsipv6RouteProtocol != IP6_NETMGMT_PROTOID) &&
        (i4Fsipv6RouteProtocol != IP6_LOCAL_PROTOID))
    {
        /* Invalid Protocol Id */
        CLI_SET_ERR (CLI_IP6_INVALID_ROUTE_PROTO);
        return SNMP_FAILURE;
    }

    Ip6AddrCopy (&ip6addr,
                 (tIp6Addr *) (VOID *) pFsipv6RouteNextHop->pu1_OctetList);

    if (IS_ADDR_MULTI (ip6addr))
    {
        CLI_SET_ERR (CLI_IP6_INVALID_NEXTHOP);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6RouteType
 Input       :  The Indices
                Fsipv6RouteDest
                Fsipv6RoutePfxLength
                Fsipv6RouteProtocol
                Fsipv6RouteNextHop

                The Object 
                testValFsipv6RouteType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6RouteType (UINT4 *pu4ErrorCode,
                          tSNMP_OCTET_STRING_TYPE * pFsipv6RouteDest,
                          INT4 i4Fsipv6RoutePfxLength,
                          INT4 i4Fsipv6RouteProtocol,
                          tSNMP_OCTET_STRING_TYPE * pFsipv6RouteNextHop,
                          INT4 i4TestValFsipv6RouteType)
{
    tIp6Addr            ip6addr;

    MEMSET (&ip6addr, IP6_ZERO, sizeof (tIp6Addr));

    if ((i4TestValFsipv6RouteType != IP6_ROUTE_TYPE_DIRECT) &&
        (i4TestValFsipv6RouteType != IP6_ROUTE_TYPE_OTHER) &&
        (i4TestValFsipv6RouteType != IP6_ROUTE_TYPE_INDIRECT))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    *pu4ErrorCode = SNMP_ERR_NO_CREATION;

    if (pFsipv6RouteDest == NULL)
    {
        return SNMP_FAILURE;
    }

    Ip6AddrCopy (&ip6addr,
                 (tIp6Addr *) (VOID *) pFsipv6RouteDest->pu1_OctetList);

    if ((IS_ADDR_MULTI (ip6addr)) || (IS_ADDR_LLOCAL (ip6addr)))
    {
        return SNMP_FAILURE;
    }

    if ((i4Fsipv6RoutePfxLength < IP6_ADDR_MIN_PREFIX) ||
        (i4Fsipv6RoutePfxLength > IP6_ADDR_MAX_PREFIX))
    {
        return SNMP_FAILURE;
    }

    if (i4Fsipv6RouteProtocol != IP6_NETMGMT_PROTOID)
    {
        /* Invalid Protocol Id */
        return SNMP_FAILURE;
    }

    Ip6AddrCopy (&ip6addr,
                 (tIp6Addr *) (VOID *) pFsipv6RouteNextHop->pu1_OctetList);

    if (IS_ADDR_MULTI (ip6addr))
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6RouteTag
 Input       :  The Indices
                Fsipv6RouteDest
                Fsipv6RoutePfxLength
                Fsipv6RouteProtocol
                Fsipv6RouteNextHop

                The Object 
                testValFsipv6RouteTag
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6RouteTag (UINT4 *pu4ErrorCode,
                         tSNMP_OCTET_STRING_TYPE * pFsipv6RouteDest,
                         INT4 i4Fsipv6RoutePfxLength,
                         INT4 i4Fsipv6RouteProtocol,
                         tSNMP_OCTET_STRING_TYPE * pFsipv6RouteNextHop,
                         UINT4 u4TestValFsipv6RouteTag)
{
    tIp6Addr            ip6addr;

    MEMSET (&ip6addr, IP6_ZERO, sizeof (tIp6Addr));

    if (((u4TestValFsipv6RouteTag & 0xFFFF0000) != 0) &&
        ((u4TestValFsipv6RouteTag & 0x0000FFFF) == 0))
    {
        /* Invalid Route Tag */
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    *pu4ErrorCode = SNMP_ERR_NO_CREATION;

    if (pFsipv6RouteDest == NULL)
    {
        return SNMP_FAILURE;
    }

    Ip6AddrCopy (&ip6addr,
                 (tIp6Addr *) (VOID *) pFsipv6RouteDest->pu1_OctetList);

    if ((IS_ADDR_MULTI (ip6addr)) || (IS_ADDR_LLOCAL (ip6addr)))
    {
        return SNMP_FAILURE;
    }

    if ((i4Fsipv6RoutePfxLength < IP6_ADDR_MIN_PREFIX) ||
        (i4Fsipv6RoutePfxLength > IP6_ADDR_MAX_PREFIX))
    {
        return SNMP_FAILURE;
    }

    if (i4Fsipv6RouteProtocol != IP6_NETMGMT_PROTOID)
    {
        /* Invalid Protocol Id */
        return SNMP_FAILURE;
    }

    Ip6AddrCopy (&ip6addr,
                 (tIp6Addr *) (VOID *) pFsipv6RouteNextHop->pu1_OctetList);

    if (IS_ADDR_MULTI (ip6addr))
    {
        return SNMP_FAILURE;
    }

    *pu4ErrorCode = 0;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6RouteAdminStatus
 Input       :  The Indices
                Fsipv6RouteDest
                Fsipv6RoutePfxLength
                Fsipv6RouteProtocol
                Fsipv6RouteNextHop

                The Object 
                testValFsipv6RouteAdminStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6RouteAdminStatus (UINT4 *pu4ErrorCode,
                                 tSNMP_OCTET_STRING_TYPE * pFsipv6RouteDest,
                                 INT4 i4Fsipv6RoutePfxLength,
                                 INT4 i4Fsipv6RouteProtocol,
                                 tSNMP_OCTET_STRING_TYPE * pFsipv6RouteNextHop,
                                 INT4 i4TestValFsipv6RouteAdminStatus)
{
    tNetIpv6RtInfo      NetIpv6RtInfo;
    tIp6Addr            ip6Addr;
    tIp6Addr            NextHop;
    INT4                i4RetVal = RTM6_FAILURE;
    UINT4               u4ContextId;
    UINT4               u4FreeRouteCount = IP6_ZERO;

    MEMSET (&ip6Addr, IP6_ZERO, sizeof (tIp6Addr));
    MEMSET (&NextHop, IP6_ZERO, sizeof (tIp6Addr));
    MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));

    if (i4TestValFsipv6RouteAdminStatus < IP6FWD_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if ((i4TestValFsipv6RouteAdminStatus == IP6FWD_CREATE_AND_GO) ||
        (i4TestValFsipv6RouteAdminStatus == IP6FWD_NOT_READY) ||
        (i4TestValFsipv6RouteAdminStatus == IP6FWD_ROW_DOES_NOT_EXIST))
    {
        /* Both of these values are not supported. */
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    u4ContextId = UtilRtm6GetCurrCxtId ();
    *pu4ErrorCode = SNMP_ERR_NO_CREATION;

    Ip6AddrCopy (&ip6Addr,
                 (tIp6Addr *) (VOID *) pFsipv6RouteDest->pu1_OctetList);
    Ip6AddrCopy (&NextHop,
                 (tIp6Addr *) (VOID *) pFsipv6RouteNextHop->pu1_OctetList);

    if ((IS_ADDR_MULTI (ip6Addr)) || (IS_ADDR_LLOCAL (ip6Addr)))
    {
        CLI_SET_ERR (CLI_IP6_INVALID_ADDR_TYPE);
        return SNMP_FAILURE;
    }

    if ((i4Fsipv6RoutePfxLength < IP6_ADDR_MIN_PREFIX) ||
        (i4Fsipv6RoutePfxLength > IP6_ADDR_MAX_PREFIX))
    {
        CLI_SET_ERR (CLI_IP6_INVALID_PREFIXLEN);
        return SNMP_FAILURE;
    }

    if (i4Fsipv6RouteProtocol != IP6_NETMGMT_PROTOID)
    {
        /* Invalid Protocol Id */
        CLI_SET_ERR (CLI_IP6_INVALID_ROUTE_PROTO);
        return SNMP_FAILURE;
    }

    /* Validate Next Hop */
    if (IS_ADDR_MULTI (NextHop))
    {
        return SNMP_FAILURE;
    }

    i4RetVal = Rtm6ApiFindRtEntryInCxt (u4ContextId, &ip6Addr,
                                        (UINT1) i4Fsipv6RoutePfxLength,
                                        (INT1) i4Fsipv6RouteProtocol, &NextHop,
                                        &NetIpv6RtInfo);

    if (i4TestValFsipv6RouteAdminStatus == IP6FWD_CREATE_AND_WAIT)
    {
        if (i4RetVal == RTM6_SUCCESS)
        {
            /* Route Already Present */
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_IP6_ROUTE_ALREADY_PRESENT);
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (i4RetVal == RTM6_FAILURE)
        {
            /* Route Does Not Exist */
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_IP6_ROUTE_NOT_FOUND);
            return SNMP_FAILURE;
        }
        if (NetIpv6RtInfo.u4Index == 0)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE_FOR_NEXTHOP);
            return SNMP_FAILURE;
        }
        if ((i4TestValFsipv6RouteAdminStatus == IP6FWD_DESTROY) &&
            (NetIpv6RtInfo.i1MetricType5 == IPVX_ONE) &&
            (i4RetVal == RTM6_SUCCESS))
        {
            /* Deletion of PD route learnt from DHCP6 server 
             * should not be allowed*/
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_IP6_DHCP_PD_ROUTE);
            return SNMP_FAILURE;
        }
    }

    /* Check whether free space is available to add new route entry */
    Rtm6ApiFreeRouteCount (&u4FreeRouteCount);
    if (u4FreeRouteCount == IP6_ZERO)
    {
        *pu4ErrorCode = SNMP_ERR_RESOURCE_UNAVAILABLE;
        CLI_SET_ERR (CLI_IP6_MAX_ROUTE);
        return SNMP_FAILURE;
    }

    *pu4ErrorCode = 0;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6RouteAddrType
 Input       :  The Indices
                Fsipv6RouteDest
                Fsipv6RoutePfxLength
                Fsipv6RouteProtocol
                Fsipv6RouteNextHop

                The Object 
                testValFsipv6RouteAddrType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6RouteAddrType (UINT4 *pu4ErrorCode,
                              tSNMP_OCTET_STRING_TYPE * pFsipv6RouteDest,
                              INT4 i4Fsipv6RoutePfxLength,
                              INT4 i4Fsipv6RouteProtocol,
                              tSNMP_OCTET_STRING_TYPE * pFsipv6RouteNextHop,
                              INT4 i4TestValFsipv6RouteAddrType)
{

    tIp6Addr            ip6addr;

    MEMSET (&ip6addr, IP6_ZERO, sizeof (tIp6Addr));

    if (i4TestValFsipv6RouteAddrType != IP6_CLI_ADDR_ANYCAST &&
        i4TestValFsipv6RouteAddrType != IP6_CLI_ADDR_UNICAST)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_INVALID_ADDR_TYPE);
        return SNMP_FAILURE;
    }
    *pu4ErrorCode = SNMP_ERR_NO_CREATION;

    if (pFsipv6RouteDest == NULL)
    {
        CLI_SET_ERR (CLI_IP6_INVALID_ADDRESS);
        return SNMP_FAILURE;
    }

    if ((i4Fsipv6RoutePfxLength < IP6_ADDR_MIN_PREFIX) ||
        (i4Fsipv6RoutePfxLength > IP6_ADDR_MAX_PREFIX))
    {
        CLI_SET_ERR (CLI_IP6_INVALID_PREFIXLEN);
        return SNMP_FAILURE;
    }

    if ((i4Fsipv6RouteProtocol != IP6_NETMGMT_PROTOID) &&
        (i4Fsipv6RouteProtocol != IP6_LOCAL_PROTOID))
    {
        /* Invalid Protocol Id */
        CLI_SET_ERR (CLI_IP6_INVALID_ROUTE_PROTO);
        return SNMP_FAILURE;
    }

    Ip6AddrCopy (&ip6addr,
                 (tIp6Addr *) (VOID *) pFsipv6RouteNextHop->pu1_OctetList);

    if (IS_ADDR_MULTI (ip6addr))
    {
        CLI_SET_ERR (CLI_IP6_INVALID_NEXTHOP);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6RoutePreference
 Input       :  The Indices
                Fsipv6RouteDest
                Fsipv6RoutePfxLength
                Fsipv6RouteProtocol
                Fsipv6RouteNextHop

                The Object
                testValFsipv6RoutePreference
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6RoutePreference (UINT4 *pu4ErrorCode,
                                tSNMP_OCTET_STRING_TYPE * pFsipv6RouteDest,
                                INT4 i4Fsipv6RoutePfxLength,
                                INT4 i4Fsipv6RouteProtocol,
                                tSNMP_OCTET_STRING_TYPE * pFsipv6RouteNextHop,
                                INT4 i4TestValFsipv6RoutePreference)
{
    tIp6Addr            ip6addr;

    MEMSET (&ip6addr, IP6_ZERO, sizeof (tIp6Addr));

    if ((i4TestValFsipv6RoutePreference >= RTM6_IP6_ROUTE_MAX_DISTANCE) ||
        (i4TestValFsipv6RoutePreference < RTM6_IP6_ROUTE_MIN_DISTANCE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    *pu4ErrorCode = SNMP_ERR_NO_CREATION;

    if (pFsipv6RouteDest == NULL)
    {
        CLI_SET_ERR (CLI_IP6_INVALID_ADDRESS);
        return SNMP_FAILURE;
    }

    Ip6AddrCopy (&ip6addr,
                 (tIp6Addr *) (VOID *) pFsipv6RouteDest->pu1_OctetList);

    if ((IS_ADDR_MULTI (ip6addr)) || (IS_ADDR_LLOCAL (ip6addr)))
    {
        CLI_SET_ERR (CLI_IP6_INVALID_ADDR_TYPE);
        return SNMP_FAILURE;
    }

    if ((i4Fsipv6RoutePfxLength < IP6_ADDR_MIN_PREFIX) ||
        (i4Fsipv6RoutePfxLength > IP6_ADDR_MAX_PREFIX))
    {
        CLI_SET_ERR (CLI_IP6_INVALID_PREFIXLEN);
        return SNMP_FAILURE;
    }

    if ((i4Fsipv6RouteProtocol != IP6_NETMGMT_PROTOID) &&
        (i4Fsipv6RouteProtocol != IP6_LOCAL_PROTOID))
    {
        /* Invalid Protocol Id */
        CLI_SET_ERR (CLI_IP6_INVALID_ROUTE_PROTO);
        return SNMP_FAILURE;
    }

    Ip6AddrCopy (&ip6addr,
                 (tIp6Addr *) (VOID *) pFsipv6RouteNextHop->pu1_OctetList);

    if (IS_ADDR_MULTI (ip6addr))
    {
        CLI_SET_ERR (CLI_IP6_INVALID_NEXTHOP);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Fsipv6RouteTable
 Input       :  The Indices
                Fsipv6RouteDest
                Fsipv6RoutePfxLength
                Fsipv6RouteProtocol
                Fsipv6RouteNextHop
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsipv6RouteTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Fsipv6PrefTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsipv6PrefTable
 Input       :  The Indices
                Fsipv6Protocol
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsipv6PrefTable (INT4 i4Fsipv6Protocol)
{
    if ((i4Fsipv6Protocol < IP6_OTHER_PROTOID) ||
        (i4Fsipv6Protocol > IP6_IGRP_PROTOID))
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsipv6PrefTable
 Input       :  The Indices
                Fsipv6Protocol
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsipv6PrefTable (INT4 *pi4Fsipv6Protocol)
{
    *pi4Fsipv6Protocol = IP6_OTHER_PROTOID;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsipv6PrefTable
 Input       :  The Indices
                Fsipv6Protocol
                nextFsipv6Protocol
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsipv6PrefTable (INT4 i4Fsipv6Protocol,
                                INT4 *pi4NextFsipv6Protocol)
{
    if (i4Fsipv6Protocol < IP6_MAX_ROUTING_PROTOCOLS)
    {
        *pi4NextFsipv6Protocol = i4Fsipv6Protocol + 1;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsipv6Preference
 Input       :  The Indices
                Fsipv6Protocol

                The Object 
                retValFsipv6Preference
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6Preference (INT4 i4Fsipv6Protocol, UINT4 *pu4RetValFsipv6Preference)
{
    UINT4               u4ContextId;
    INT4                i4RetVal;

    u4ContextId = UtilRtm6GetCurrCxtId ();
    i4RetVal = Rtm6ApiGetProtocolPrefInCxt (u4ContextId, i4Fsipv6Protocol,
                                            pu4RetValFsipv6Preference);
    if (i4RetVal == RTM6_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsipv6Preference
 Input       :  The Indices
                Fsipv6Protocol

                The Object 
                setValFsipv6Preference
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6Preference (INT4 i4Fsipv6Protocol, UINT4 u4SetValFsipv6Preference)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = IPVX_ZERO;
    UINT4               u4ContextId;
    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    u4ContextId = UtilRtm6GetCurrCxtId ();
    if (Rtm6ApiSetProtocolPrefInCxt (u4ContextId, i4Fsipv6Protocol,
                                     u4SetValFsipv6Preference) == RTM6_FAILURE)
    {
        return SNMP_FAILURE;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = FsMIIpv6Preference;
    SnmpNotifyInfo.u4OidLen = sizeof (FsMIIpv6Preference) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = Rtm6Lock;
    SnmpNotifyInfo.pUnLockPointer = Rtm6UnLock;
    SnmpNotifyInfo.u4Indices = IP6_TWO;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %u", (INT4) u4ContextId,
                      i4Fsipv6Protocol, u4SetValFsipv6Preference));

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Fsipv6Preference
 Input       :  The Indices
                Fsipv6Protocol

                The Object 
                testValFsipv6Preference
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6Preference (UINT4 *pu4ErrorCode, INT4 i4Fsipv6Protocol,
                           UINT4 u4TestValFsipv6Preference)
{
    if ((i4Fsipv6Protocol < IP6_OTHER_PROTOID) ||
        (i4Fsipv6Protocol > IP6_IGRP_PROTOID))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (u4TestValFsipv6Preference > IP6_MAX_PROTO_PREFERENCE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Fsipv6PrefTable
 Input       :  The Indices
                Fsipv6Protocol
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsipv6PrefTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsIpv6TestRedEntryTime
 Input       :  The Indices

                The Object 
                retValFsIpv6TestRedEntryTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIpv6TestRedEntryTime (INT4 *pi4RetValFsIpv6TestRedEntryTime)
{
    *pi4RetValFsIpv6TestRedEntryTime = gNd6RedGlobalInfo.i4Nd6RedEntryTime;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsIpv6TestRedExitTime
 Input       :  The Indices

                The Object 
                retValFsIpv6TestRedExitTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIpv6TestRedExitTime (INT4 *pi4RetValFsIpv6TestRedExitTime)
{
    *pi4RetValFsIpv6TestRedExitTime = gNd6RedGlobalInfo.i4Nd6RedExitTime;
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Fsipv6IfRaRDNSSTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsipv6IfRaRDNSSTable
 Input       :  The Indices
                Fsipv6IfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsipv6IfRaRDNSSTable (INT4 i4Fsipv6IfIndex)
{
    UNUSED_PARAM (i4Fsipv6IfIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsipv6IfRaRDNSSTable
 Input       :  The Indices
                Fsipv6IfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsipv6IfRaRDNSSTable (INT4 *pi4Fsipv6IfIndex)
{
    UNUSED_PARAM (pi4Fsipv6IfIndex);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsipv6IfRaRDNSSTable
 Input       :  The Indices
                Fsipv6IfIndex
                nextFsipv6IfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsipv6IfRaRDNSSTable (INT4 i4Fsipv6IfIndex,
                                     INT4 *pi4NextFsipv6IfIndex)
{
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (pi4NextFsipv6IfIndex);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsipv6IfRadvRDNSSOpen
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                retValFsipv6IfRadvRDNSSOpen
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfRadvRDNSSOpen (INT4 i4Fsipv6IfIndex,
                             INT4 *pi4RetValFsipv6IfRadvRDNSSOpen)
{
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (pi4RetValFsipv6IfRadvRDNSSOpen);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfRaRDNSSPreference
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                retValFsipv6IfRaRDNSSPreference
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfRaRDNSSPreference (INT4 i4Fsipv6IfIndex,
                                 INT4 *pi4RetValFsipv6IfRaRDNSSPreference)
{
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (pi4RetValFsipv6IfRaRDNSSPreference);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfRaRDNSSLifetime
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                retValFsipv6IfRaRDNSSLifetime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfRaRDNSSLifetime (INT4 i4Fsipv6IfIndex,
                               UINT4 *pu4RetValFsipv6IfRaRDNSSLifetime)
{
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (pu4RetValFsipv6IfRaRDNSSLifetime);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfRaRDNSSAddrOne
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                retValFsipv6IfRaRDNSSAddrOne
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfRaRDNSSAddrOne (INT4 i4Fsipv6IfIndex,
                              tSNMP_OCTET_STRING_TYPE *
                              pRetValFsipv6IfRaRDNSSAddrOne)
{
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (pRetValFsipv6IfRaRDNSSAddrOne);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsipv6IfRaRDNSSAddrTwo
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                retValFsipv6IfRaRDNSSAddrTwo
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfRaRDNSSAddrTwo (INT4 i4Fsipv6IfIndex,
                              tSNMP_OCTET_STRING_TYPE *
                              pRetValFsipv6IfRaRDNSSAddrTwo)
{
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (pRetValFsipv6IfRaRDNSSAddrTwo);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsipv6IfRaRDNSSAddrThree
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                retValFsipv6IfRaRDNSSAddrThree
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfRaRDNSSAddrThree (INT4 i4Fsipv6IfIndex,
                                tSNMP_OCTET_STRING_TYPE *
                                pRetValFsipv6IfRaRDNSSAddrThree)
{
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (pRetValFsipv6IfRaRDNSSAddrThree);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsipv6IfRaRDNSSRowStatus
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                retValFsipv6IfRaRDNSSRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfRaRDNSSRowStatus (INT4 i4Fsipv6IfIndex,
                                INT4 *pi4RetValFsipv6IfRaRDNSSRowStatus)
{
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (pi4RetValFsipv6IfRaRDNSSRowStatus);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfDomainNameOne
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                retValFsipv6IfDomainNameOne
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfDomainNameOne (INT4 i4Fsipv6IfIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pRetValFsipv6IfDomainNameOne)
{
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (pRetValFsipv6IfDomainNameOne);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfDomainNameTwo
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                retValFsipv6IfDomainNameTwo
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfDomainNameTwo (INT4 i4Fsipv6IfIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pRetValFsipv6IfDomainNameTwo)
{
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (pRetValFsipv6IfDomainNameTwo);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfDomainNameThree
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                retValFsipv6IfDomainNameThree
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfDomainNameThree (INT4 i4Fsipv6IfIndex,
                               tSNMP_OCTET_STRING_TYPE *
                               pRetValFsipv6IfDomainNameThree)
{
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (pRetValFsipv6IfDomainNameThree);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfDnsLifeTime
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                retValFsipv6IfDnsLifeTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfDnsLifeTime (INT4 i4Fsipv6IfIndex,
                           INT4 *pi4RetValFsipv6IfDnsLifeTime)
{
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (pi4RetValFsipv6IfDnsLifeTime);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfRaRDNSSAddrOneLife
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                retValFsipv6IfRaRDNSSAddrOneLife
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfRaRDNSSAddrOneLife (INT4 i4Fsipv6IfIndex,
                                  UINT4 *pu4RetValFsipv6IfRaRDNSSAddrOneLife)
{
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (pu4RetValFsipv6IfRaRDNSSAddrOneLife);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfRaRDNSSAddrTwoLife
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                retValFsipv6IfRaRDNSSAddrTwoLife
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfRaRDNSSAddrTwoLife (INT4 i4Fsipv6IfIndex,
                                  UINT4 *pu4RetValFsipv6IfRaRDNSSAddrTwoLife)
{

    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (pu4RetValFsipv6IfRaRDNSSAddrTwoLife);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfRaRDNSSAddrThreeLife
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                retValFsipv6IfRaRDNSSAddrThreeLife
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfRaRDNSSAddrThreeLife (INT4 i4Fsipv6IfIndex,
                                    UINT4
                                    *pu4RetValFsipv6IfRaRDNSSAddrThreeLife)
{
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (pu4RetValFsipv6IfRaRDNSSAddrThreeLife);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfDomainNameOneLife
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                retValFsipv6IfDnsLifeTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfDomainNameOneLife (INT4 i4Fsipv6IfIndex,
                                 INT4 *pi4RetValFsipv6IfDnsLifeTime)
{
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (pi4RetValFsipv6IfDnsLifeTime);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfDomainNameTwo
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                retValFsipv6IfDnsLifeTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfDomainNameTwoLife (INT4 i4Fsipv6IfIndex,
                                 INT4 *pi4RetValFsipv6IfDnsLifeTime)
{
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (pi4RetValFsipv6IfDnsLifeTime);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfDomainNameThree
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                retValFsipv6IfDnsLifeTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfDomainNameThreeLife (INT4 i4Fsipv6IfIndex,
                                   INT4 *pi4RetValFsipv6IfDnsLifeTime)
{
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (pi4RetValFsipv6IfDnsLifeTime);
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsipv6IfRadvRDNSSOpen
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                setValFsipv6IfRadvRDNSSOpen
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6IfRadvRDNSSOpen (INT4 i4Fsipv6IfIndex,
                             INT4 i4SetValFsipv6IfRadvRDNSSOpen)
{
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (i4SetValFsipv6IfRadvRDNSSOpen);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6IfRaRDNSSPreference
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                setValFsipv6IfRaRDNSSPreference
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6IfRaRDNSSPreference (INT4 i4Fsipv6IfIndex,
                                 INT4 i4SetValFsipv6IfRaRDNSSPreference)
{
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (i4SetValFsipv6IfRaRDNSSPreference);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6IfRaRDNSSLifetime
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                setValFsipv6IfRaRDNSSLifetime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6IfRaRDNSSLifetime (INT4 i4Fsipv6IfIndex,
                               UINT4 u4SetValFsipv6IfRaRDNSSLifetime)
{
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (u4SetValFsipv6IfRaRDNSSLifetime);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6IfRaRDNSSAddrOne
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                setValFsipv6IfRaRDNSSAddrOne
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6IfRaRDNSSAddrOne (INT4 i4Fsipv6IfIndex,
                              tSNMP_OCTET_STRING_TYPE *
                              pSetValFsipv6IfRaRDNSSAddrOne)
{
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (pSetValFsipv6IfRaRDNSSAddrOne);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6IfRaRDNSSAddrTwo
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                setValFsipv6IfRaRDNSSAddrTwo
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6IfRaRDNSSAddrTwo (INT4 i4Fsipv6IfIndex,
                              tSNMP_OCTET_STRING_TYPE *
                              pSetValFsipv6IfRaRDNSSAddrTwo)
{
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (pSetValFsipv6IfRaRDNSSAddrTwo);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6IfRaRDNSSAddrThree
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                setValFsipv6IfRaRDNSSAddrThree
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6IfRaRDNSSAddrThree (INT4 i4Fsipv6IfIndex,
                                tSNMP_OCTET_STRING_TYPE *
                                pSetValFsipv6IfRaRDNSSAddrThree)
{
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (pSetValFsipv6IfRaRDNSSAddrThree);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6IfRaRDNSSRowStatus
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                setValFsipv6IfRaRDNSSRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6IfRaRDNSSRowStatus (INT4 i4Fsipv6IfIndex,
                                INT4 i4SetValFsipv6IfRaRDNSSRowStatus)
{
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (i4SetValFsipv6IfRaRDNSSRowStatus);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6IfDomainNameOne
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                setValFsipv6IfDomainNameOne
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6IfDomainNameOne (INT4 i4Fsipv6IfIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pSetValFsipv6IfDomainNameOne)
{
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (pSetValFsipv6IfDomainNameOne);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsipv6IfDomainNameTwo
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                setValFsipv6IfDomainNameTwo
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6IfDomainNameTwo (INT4 i4Fsipv6IfIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pSetValFsipv6IfDomainNameTwo)
{
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (pSetValFsipv6IfDomainNameTwo);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsipv6IfDomainNameThree
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                setValFsipv6IfDomainNameThree
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6IfDomainNameThree (INT4 i4Fsipv6IfIndex,
                               tSNMP_OCTET_STRING_TYPE *
                               pSetValFsipv6IfDomainNameThree)
{
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (pSetValFsipv6IfDomainNameThree);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsipv6IfDnsLifeTime
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                setValFsipv6IfDnsLifeTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6IfDnsLifeTime (INT4 i4Fsipv6IfIndex,
                           INT4 i4SetValFsipv6IfDnsLifeTime)
{
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (i4SetValFsipv6IfDnsLifeTime);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetFsipv6IfRaRDNSSAddrOneLife
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                setValFsipv6IfRaRDNSSAddrOneLife
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6IfRaRDNSSAddrOneLife (INT4 i4Fsipv6IfIndex,
                                  UINT4 u4SetValFsipv6IfRaRDNSSAddrOneLife)
{
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (u4SetValFsipv6IfRaRDNSSAddrOneLife);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsipv6IfRaRDNSSAddrTwoLife
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                setValFsipv6IfRaRDNSSAddrTwoLife
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6IfRaRDNSSAddrTwoLife (INT4 i4Fsipv6IfIndex,
                                  UINT4 u4SetValFsipv6IfRaRDNSSAddrTwoLife)
{
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (u4SetValFsipv6IfRaRDNSSAddrTwoLife);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsipv6IfRaRDNSSAddrThreeLife
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                setValFsipv6IfRaRDNSSAddrThreeLife
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6IfRaRDNSSAddrThreeLife (INT4 i4Fsipv6IfIndex,
                                    UINT4 u4SetValFsipv6IfRaRDNSSAddrThreeLife)
{
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (u4SetValFsipv6IfRaRDNSSAddrThreeLife);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsipv6IfDomainNameOneLife
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                setValFsipv6IfDnsLifeTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6IfDomainNameOneLife (INT4 i4Fsipv6IfIndex,
                                 INT4 i4SetValFsipv6IfDnsLifeTime)
{
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (i4SetValFsipv6IfDnsLifeTime);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsipv6IfDomainNameTwoLife
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                setValFsipv6IfDnsLifeTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6IfDomainNameTwoLife (INT4 i4Fsipv6IfIndex,
                                 INT4 i4SetValFsipv6IfDnsLifeTime)
{
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (i4SetValFsipv6IfDnsLifeTime);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsipv6IfDomainNameThreeLife
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                setValFsipv6IfDnsLifeTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6IfDomainNameThreeLife (INT4 i4Fsipv6IfIndex,
                                   INT4 i4SetValFsipv6IfDnsLifeTime)
{
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (i4SetValFsipv6IfDnsLifeTime);
    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Fsipv6IfRadvRDNSSOpen
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                testValFsipv6IfRadvRDNSSOpen
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6IfRadvRDNSSOpen (UINT4 *pu4ErrorCode, INT4 i4Fsipv6IfIndex,
                                INT4 i4TestValFsipv6IfRadvRDNSSOpen)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (i4TestValFsipv6IfRadvRDNSSOpen);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6IfRaRDNSSPreference
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                testValFsipv6IfRaRDNSSPreference
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6IfRaRDNSSPreference (UINT4 *pu4ErrorCode, INT4 i4Fsipv6IfIndex,
                                    INT4 i4TestValFsipv6IfRaRDNSSPreference)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (i4TestValFsipv6IfRaRDNSSPreference);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6IfRaRDNSSLifetime
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                testValFsipv6IfRaRDNSSLifetime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6IfRaRDNSSLifetime (UINT4 *pu4ErrorCode, INT4 i4Fsipv6IfIndex,
                                  UINT4 u4TestValFsipv6IfRaRDNSSLifetime)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (u4TestValFsipv6IfRaRDNSSLifetime);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6IfRaRDNSSAddrOne
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                testValFsipv6IfRaRDNSSAddrOne
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6IfRaRDNSSAddrOne (UINT4 *pu4ErrorCode, INT4 i4Fsipv6IfIndex,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pTestValFsipv6IfRaRDNSSAddrOne)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (pTestValFsipv6IfRaRDNSSAddrOne);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6IfRaRDNSSAddrTwo
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                testValFsipv6IfRaRDNSSAddrTwo
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6IfRaRDNSSAddrTwo (UINT4 *pu4ErrorCode, INT4 i4Fsipv6IfIndex,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pTestValFsipv6IfRaRDNSSAddrTwo)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (pTestValFsipv6IfRaRDNSSAddrTwo);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6IfRaRDNSSAddrThree
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                testValFsipv6IfRaRDNSSAddrThree
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6IfRaRDNSSAddrThree (UINT4 *pu4ErrorCode, INT4 i4Fsipv6IfIndex,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pTestValFsipv6IfRaRDNSSAddrThree)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (pTestValFsipv6IfRaRDNSSAddrThree);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6IfRaRDNSSRowStatus
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                testValFsipv6IfRaRDNSSRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6IfRaRDNSSRowStatus (UINT4 *pu4ErrorCode, INT4 i4Fsipv6IfIndex,
                                   INT4 i4TestValFsipv6IfRaRDNSSRowStatus)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (i4TestValFsipv6IfRaRDNSSRowStatus);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6IfDomainNameOne
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                testValFsipv6IfDomainNameOne
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6IfDomainNameOne (UINT4 *pu4ErrorCode, INT4 i4Fsipv6IfIndex,
                                tSNMP_OCTET_STRING_TYPE *
                                pTestValFsipv6IfDomainNameOne)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (pTestValFsipv6IfDomainNameOne);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6IfDomainNameTwo
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                testValFsipv6IfDomainNameTwo
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6IfDomainNameTwo (UINT4 *pu4ErrorCode, INT4 i4Fsipv6IfIndex,
                                tSNMP_OCTET_STRING_TYPE *
                                pTestValFsipv6IfDomainNameTwo)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (pTestValFsipv6IfDomainNameTwo);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6IfDomainNameThree
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                testValFsipv6IfDomainNameThree
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6IfDomainNameThree (UINT4 *pu4ErrorCode, INT4 i4Fsipv6IfIndex,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pTestValFsipv6IfDomainNameThree)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (pTestValFsipv6IfDomainNameThree);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6IfDnsLifeTime
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                testValFsipv6IfDnsLifeTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6IfDnsLifeTime (UINT4 *pu4ErrorCode, INT4 i4Fsipv6IfIndex,
                              UINT4 i4TestValFsipv6IfDnsLifeTime)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (i4TestValFsipv6IfDnsLifeTime);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6IfRaRDNSSAddrOneLife
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                testValFsipv6IfRaRDNSSAddrOneLife
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6IfRaRDNSSAddrOneLife (UINT4 *pu4ErrorCode, INT4 i4Fsipv6IfIndex,
                                     UINT4 u4TestValFsipv6IfRaRDNSSAddrOneLife)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (u4TestValFsipv6IfRaRDNSSAddrOneLife);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6IfRaRDNSSAddrTwoLife
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                testValFsipv6IfRaRDNSSAddrTwoLife
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6IfRaRDNSSAddrTwoLife (UINT4 *pu4ErrorCode, INT4 i4Fsipv6IfIndex,
                                     UINT4 u4TestValFsipv6IfRaRDNSSAddrTwoLife)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (u4TestValFsipv6IfRaRDNSSAddrTwoLife);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6IfRaRDNSSAddrThreeLife
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                testValFsipv6IfRaRDNSSAddrThreeLife
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6IfRaRDNSSAddrThreeLife (UINT4 *pu4ErrorCode,
                                       INT4 i4Fsipv6IfIndex,
                                       UINT4
                                       u4TestValFsipv6IfRaRDNSSAddrThreeLife)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (u4TestValFsipv6IfRaRDNSSAddrThreeLife);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6IfDomainNameOneLife
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                testValFsipv6IfRaDNSAddrOneLife
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6IfDomainNameOneLife (UINT4 *pu4ErrorCode, INT4 i4Fsipv6IfIndex,
                                    UINT4 u4TestValFsipv6IfRaDNSAddrOneLife)
{

    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (u4TestValFsipv6IfRaDNSAddrOneLife);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6IfDomainNameTwoLife
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                testValFsipv6IfRaDNSAddrTwoLife
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6IfDomainNameTwoLife (UINT4 *pu4ErrorCode, INT4 i4Fsipv6IfIndex,
                                    UINT4 u4TestValFsipv6IfRaDNSAddrTwoLife)
{

    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (u4TestValFsipv6IfRaDNSAddrTwoLife);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6IfDomainNameThreeLife
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                testValFsipv6IfRaDNSAddrThreeLife
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6IfDomainNameThreeLife (UINT4 *pu4ErrorCode, INT4 i4Fsipv6IfIndex,
                                      UINT4 u4TestValFsipv6IfRaDNSAddrThreeLife)
{

    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (u4TestValFsipv6IfRaDNSAddrThreeLife);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Fsipv6IfRaRDNSSTable
 Input       :  The Indices
                Fsipv6IfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsipv6IfRaRDNSSTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
