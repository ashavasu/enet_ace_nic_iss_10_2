/********************************************************************
 * Copyright (C) Aricent Technology Holdings Limited, 2008 - 2009
 *
 * $Id: ipv6npwr.c,v 1.5 2015/01/28 12:37:02 siva Exp $
 * Description:
 *       Contains wrapper functions for IPV6 NP calls.
 *******************************************************************/

#ifndef __IPV6_NP_WR_C
#define __IPV6_NP_WR_C

#include "nputil.h"

/***************************************************************************
 *                                                                          
 *    Function Name       : Ipv6NpWrHwProgram                                         
 *                                                                          
 *    Description         : This function takes care of calling appropriate 
 *                          Np call using the tIpv6NpModInfo
 *                                                                          
 *    Input(s)            : FsHwNpParam of type tfsHwNp                     
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

PUBLIC UINT1
Ipv6NpWrHwProgram (tFsHwNp * pFsHwNp)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tIpv6NpModInfo     *pIpv6NpModInfo = NULL;

    if (NULL == pFsHwNp)
    {
        return FNP_FAILURE;
    }

    u4Opcode = pFsHwNp->u4Opcode;
    pIpv6NpModInfo = &(pFsHwNp->Ipv6NpModInfo);

    if (NULL == pIpv6NpModInfo)
    {
        return FNP_FAILURE;
    }

    switch (u4Opcode)
    {
        case FS_NP_IPV6_INIT:
        {
            u1RetVal = FsNpIpv6Init ();
            break;
        }
        case FS_NP_IPV6_DEINIT:
        {
            u1RetVal = FsNpIpv6Deinit ();
            break;
        }
        case FS_NP_IPV6_NEIGH_CACHE_ADD:
        {
            tIpv6NpWrFsNpIpv6NeighCacheAdd *pEntry = NULL;
            pEntry = &pIpv6NpModInfo->Ipv6NpFsNpIpv6NeighCacheAdd;
            u1RetVal =
                FsNpIpv6NeighCacheAdd (pEntry->u4VrId, pEntry->pu1Ip6Addr,
                                       pEntry->u4IfIndex, pEntry->pu1HwAddr,
                                       pEntry->u1HwAddrLen,
                                       pEntry->u1ReachStatus, pEntry->u2VlanId);
            break;
        }
        case FS_NP_IPV6_NEIGH_CACHE_DEL:
        {
            tIpv6NpWrFsNpIpv6NeighCacheDel *pEntry = NULL;
            pEntry = &pIpv6NpModInfo->Ipv6NpFsNpIpv6NeighCacheDel;
            u1RetVal =
                FsNpIpv6NeighCacheDel (pEntry->u4VrId, pEntry->pu1Ip6Addr,
                                       pEntry->u4IfIndex);
            break;
        }
        case FS_NP_IPV6_UC_ROUTE_ADD:
        {
            tIpv6NpWrFsNpIpv6UcRouteAdd *pEntry = NULL;
            pEntry = &pIpv6NpModInfo->Ipv6NpFsNpIpv6UcRouteAdd;
            u1RetVal =
                FsNpIpv6UcRouteAdd (pEntry->u4VrId, pEntry->pu1Ip6Prefix,
                                    pEntry->u1PrefixLen, pEntry->pu1NextHop,
                                    pEntry->u4NHType, pEntry->pIntInfo);
            break;
        }
        case FS_NP_IPV6_UC_ROUTE_DELETE:
        {
            tIpv6NpWrFsNpIpv6UcRouteDelete *pEntry = NULL;
            pEntry = &pIpv6NpModInfo->Ipv6NpFsNpIpv6UcRouteDelete;
            u1RetVal =
                FsNpIpv6UcRouteDelete (pEntry->u4VrId, pEntry->pu1Ip6Prefix,
                                       pEntry->u1PrefixLen, pEntry->pu1NextHop,
                                       pEntry->u4IfIndex, pEntry->pRouteInfo);
            break;
        }
        case FS_NP_IPV6_RT_PRESENT_IN_FAST_PATH:
        {
            tIpv6NpWrFsNpIpv6RtPresentInFastPath *pEntry = NULL;
            pEntry = &pIpv6NpModInfo->Ipv6NpFsNpIpv6RtPresentInFastPath;
            u1RetVal =
                FsNpIpv6RtPresentInFastPath (pEntry->u4VrId,
                                             pEntry->pu1Ip6Prefix,
                                             pEntry->u1PrefixLen,
                                             pEntry->pu1NextHop,
                                             pEntry->u4IfIndex);
            break;
        }
        case FS_NP_IPV6_UC_ROUTING:
        {
            tIpv6NpWrFsNpIpv6UcRouting *pEntry = NULL;
            pEntry = &pIpv6NpModInfo->Ipv6NpFsNpIpv6UcRouting;
            u1RetVal =
                FsNpIpv6UcRouting (pEntry->u4VrId, pEntry->u4RoutingStatus);
            break;
        }
        case FS_NP_IPV6_GET_STATS:
        {
            tIpv6NpWrFsNpIpv6GetStats *pEntry = NULL;
            pEntry = &pIpv6NpModInfo->Ipv6NpFsNpIpv6GetStats;
            u1RetVal =
                FsNpIpv6GetStats (pEntry->u4VrId, pEntry->u4IfIndex,
                                  pEntry->u4StatsFlag, pEntry->pu4StatsValue);
            break;
        }
        case FS_NP_IPV6_INTF_STATUS:
        {
            tIpv6NpWrFsNpIpv6IntfStatus *pEntry = NULL;
            pEntry = &pIpv6NpModInfo->Ipv6NpFsNpIpv6IntfStatus;
            u1RetVal =
                FsNpIpv6IntfStatus (pEntry->u4VrId, pEntry->u4IfStatus,
                                    pEntry->pIntfInfo);
            break;
        }
        case FS_NP_IPV6_ADDR_CREATE:
        {
            tIpv6NpWrFsNpIpv6AddrCreate *pEntry = NULL;
            pEntry = &pIpv6NpModInfo->Ipv6NpFsNpIpv6AddrCreate;
            u1RetVal =
                FsNpIpv6AddrCreate (pEntry->u4VrId, pEntry->u4IfIndex,
                                    pEntry->u4AddrType, pEntry->pu1Ip6Addr,
                                    pEntry->u1PrefixLen);
            break;
        }
        case FS_NP_IPV6_ADDR_DELETE:
        {
            tIpv6NpWrFsNpIpv6AddrDelete *pEntry = NULL;
            pEntry = &pIpv6NpModInfo->Ipv6NpFsNpIpv6AddrDelete;
            u1RetVal =
                FsNpIpv6AddrDelete (pEntry->u4VrId, pEntry->u4IfIndex,
                                    pEntry->u4AddrType, pEntry->pu1Ip6Addr,
                                    pEntry->u1PrefixLen);
            break;
        }
        case FS_NP_IPV6_TUNL_PARAM_SET:
        {
            tIpv6NpWrFsNpIpv6TunlParamSet *pEntry = NULL;
            pEntry = &pIpv6NpModInfo->Ipv6NpFsNpIpv6TunlParamSet;
            u1RetVal =
                FsNpIpv6TunlParamSet (pEntry->u4VrId, pEntry->u4IfIndex,
                                      pEntry->u4TunlType, pEntry->u4SrcAddr,
                                      pEntry->u4DstAddr);
            break;
        }
        case FS_NP_IPV6_ADD_MCAST_M_A_C:
        {
            tIpv6NpWrFsNpIpv6AddMcastMAC *pEntry = NULL;
            pEntry = &pIpv6NpModInfo->Ipv6NpFsNpIpv6AddMcastMAC;
            u1RetVal =
                FsNpIpv6AddMcastMAC (pEntry->u4VrId, pEntry->u4IfIndex,
                                     pEntry->pu1MacAddr, pEntry->u1MacAddrLen);
            break;
        }
        case FS_NP_IPV6_DEL_MCAST_M_A_C:
        {
            tIpv6NpWrFsNpIpv6DelMcastMAC *pEntry = NULL;
            pEntry = &pIpv6NpModInfo->Ipv6NpFsNpIpv6DelMcastMAC;
            u1RetVal =
                FsNpIpv6DelMcastMAC (pEntry->u4VrId, pEntry->u4IfIndex,
                                     pEntry->pu1MacAddr, pEntry->u1MacAddrLen);
            break;
        }
        case FS_NP_IPV6_HANDLE_FDB_MAC_ENTRY_CHANGE:
        {
            tIpv6NpWrFsNpIpv6HandleFdbMacEntryChange *pEntry = NULL;
            pEntry = &pIpv6NpModInfo->Ipv6NpFsNpIpv6HandleFdbMacEntryChange;
            u1RetVal =
                FsNpIpv6HandleFdbMacEntryChange (pEntry->pu1L3Nexthop,
                                                 pEntry->u4IfIndex,
                                                 pEntry->pu1MacAddr,
                                                 pEntry->u4IsRemoved);
            break;
	}
	case FS_NP_IPV6_ENABLE:
	{
		tIpv6NpWrFsNpIpv6Enable *pEntry = NULL;
		pEntry = &pIpv6NpModInfo->Ipv6NpFsNpIpv6Enable;
		u1RetVal =
			FsNpIpv6Enable (pEntry->u4IfIndex, pEntry->u2VlanId);
		break;
	}
	case FS_NP_IPV6_DISABLE:
	{
		tIpv6NpWrFsNpIpv6Disable *pEntry = NULL;
		pEntry = &pIpv6NpModInfo->Ipv6NpFsNpIpv6Disable;
		u1RetVal =
			FsNpIpv6Disable (pEntry->u4IfIndex, pEntry->u2VlanId);
		break;
	}

#ifdef OSPF3_WANTED
        case FS_NP_OSPF3_INIT:
        {
            u1RetVal = FsNpOspf3Init ();
            break;
        }
        case FS_NP_OSPF3_DE_INIT:
        {
            FsNpOspf3DeInit ();
            break;
        }
#endif /* OSPF3_WANTED */
#ifdef RIP6_WANTED
        case FS_NP_RIP6_INIT:
        {
            u1RetVal = FsNpRip6Init ();
            break;
        }
#endif /* RIP6_WANTED */
        case FS_NP_IPV6_NEIGH_CACHE_GET:
        {
            tIpv6NpWrFsNpIpv6NeighCacheGet *pEntry = NULL;
            pEntry = &pIpv6NpModInfo->Ipv6NpFsNpIpv6NeighCacheGet;
            u1RetVal =
                FsNpIpv6NeighCacheGet (pEntry->Nd6NpInParam,
                                       pEntry->pNd6NpOutParam);
            break;
        }
        case FS_NP_IPV6_NEIGH_CACHE_GET_NEXT:
        {
            tIpv6NpWrFsNpIpv6NeighCacheGetNext *pEntry = NULL;
            pEntry = &pIpv6NpModInfo->Ipv6NpFsNpIpv6NeighCacheGetNext;
            u1RetVal =
                FsNpIpv6NeighCacheGetNext (pEntry->Nd6NpInParam,
                                           pEntry->pNd6NpOutParam);
            break;
        }
        case FS_NP_IPV6_UC_GET_ROUTE:
        {
            tIpv6NpWrFsNpIpv6UcGetRoute *pEntry = NULL;
            pEntry = &pIpv6NpModInfo->Ipv6NpFsNpIpv6UcGetRoute;
            u1RetVal =
                FsNpIpv6UcGetRoute (pEntry->Rtm6NpInParam,
                                    pEntry->pRtm6NpOutParam);
            break;
        }
#ifdef MBSM_WANTED
        case FS_NP_MBSM_IPV6_INIT:
        {
            tIpv6NpWrFsNpMbsmIpv6Init *pEntry = NULL;
            pEntry = &pIpv6NpModInfo->Ipv6NpFsNpMbsmIpv6Init;
            u1RetVal = FsNpMbsmIpv6Init (pEntry->pSlotInfo);
            break;
        }
#ifdef RIP6_WANTED
        case FS_NP_MBSM_RIP6_INIT:
        {
            tIpv6NpWrFsNpMbsmRip6Init *pEntry = NULL;
            pEntry = &pIpv6NpModInfo->Ipv6NpFsNpMbsmRip6Init;
            u1RetVal = FsNpMbsmRip6Init (pEntry->pSlotInfo);
            break;
        }
#endif
#ifdef OSPF3_WANTED
        case FS_NP_MBSM_OSPF3_INIT:
        {
            tIpv6NpWrFsNpMbsmOspf3Init *pEntry = NULL;
            pEntry = &pIpv6NpModInfo->Ipv6NpFsNpMbsmOspf3Init;
            u1RetVal = FsNpMbsmOspf3Init (pEntry->pSlotInfo);
            break;
        }
#endif /* OSPF3_WANTED */
        case FS_NP_MBSM_IPV6_NEIGH_CACHE_ADD:
        {
            tIpv6NpWrFsNpMbsmIpv6NeighCacheAdd *pEntry = NULL;
            pEntry = &pIpv6NpModInfo->Ipv6NpFsNpMbsmIpv6NeighCacheAdd;
            u1RetVal =
                FsNpMbsmIpv6NeighCacheAdd (pEntry->u4VrId, pEntry->pu1Ip6Addr,
                                           pEntry->u4IfIndex, pEntry->pu1HwAddr,
                                           pEntry->u1HwAddrLen,
                                           pEntry->u1ReachStatus,
                                           pEntry->u2VlanId, pEntry->pSlotInfo);
            break;
        }
        case FS_NP_MBSM_IPV6_NEIGH_CACHE_DEL:
        {
            tIpv6NpWrFsNpMbsmIpv6NeighCacheDel *pEntry = NULL;
            pEntry = &pIpv6NpModInfo->Ipv6NpFsNpMbsmIpv6NeighCacheDel;
            u1RetVal =
                FsNpMbsmIpv6NeighCacheDel (pEntry->u4VrId, pEntry->pu1Ip6Addr,
                                           pEntry->u4IfIndex,
                                           pEntry->pSlotInfo);
            break;
        }
        case FS_NP_MBSM_IPV6_UC_ROUTE_ADD:
        {
            tIpv6NpWrFsNpMbsmIpv6UcRouteAdd *pEntry = NULL;
            pEntry = &pIpv6NpModInfo->Ipv6NpFsNpMbsmIpv6UcRouteAdd;
            u1RetVal =
                FsNpMbsmIpv6UcRouteAdd (pEntry->u4VrId, pEntry->pu1Ip6Prefix,
                                        pEntry->u1PrefixLen, pEntry->pu1NextHop,
                                        pEntry->u4NHType, pEntry->pIntInfo,
                                        pEntry->pSlotInfo);
            break;
        }
        case FS_NP_MBSM_IPV6_RT_PRESENT_IN_FAST_PATH:
        {
            tIpv6NpWrFsNpMbsmIpv6RtPresentInFastPath *pEntry = NULL;
            pEntry = &pIpv6NpModInfo->Ipv6NpFsNpMbsmIpv6RtPresentInFastPath;
            u1RetVal =
                FsNpMbsmIpv6RtPresentInFastPath (pEntry->u4VrId,
                                                 pEntry->pu1Ip6Prefix,
                                                 pEntry->u1PrefixLen,
                                                 pEntry->pu1NextHop,
                                                 pEntry->u4Index,
                                                 pEntry->pSlotInfo);
            break;
        }
        case FS_NP_MBSM_IPV6_INTF_CREATE:
        {
            tIpv6NpWrFsNpMbsmIpv6IntfCreate *pEntry = NULL;
            pEntry = &pIpv6NpModInfo->Ipv6NpFsNpMbsmIpv6IntfCreate;
            u1RetVal =
                FsNpMbsmIpv6IntfCreate (pEntry->u4VrId, pEntry->u4IfIndex,
                                        pEntry->pSlotInfo);
            break;
        }
        case FS_NP_MBSM_IPV6_INTF_STATUS:
        {
            tIpv6NpWrFsNpMbsmIpv6IntfStatus *pEntry = NULL;
            pEntry = &pIpv6NpModInfo->Ipv6NpFsNpMbsmIpv6IntfStatus;
            u1RetVal =
                FsNpMbsmIpv6IntfStatus (pEntry->u4VrId, pEntry->u4IfStatus,
                                        pEntry->pIntInfo, pEntry->pSlotInfo);
            break;
        }
#endif /* MBSM_WANTED */
        case FS_NP_IPV6_CHECK_HIT_ON_NDC_ENTRY:
        {
            tIpv6NpWrFsNpIpv6CheckHitOnNDCacheEntry *pEntry = NULL;
            pEntry = &(pIpv6NpModInfo->Ipv6NpFsNpIpv6CheckHitOnNDCacheEntry);
            pEntry->u1HitBitStatus =
                FsNpIpv6CheckHitOnNDCacheEntry (pEntry->u4VrId,
                                                pEntry->pu1Ip6Addr,
                                                pEntry->u4IfIndex);
            u1RetVal = FNP_SUCCESS;
            break;
        }
        default:
            u1RetVal = FNP_FAILURE;
            break;

    }                            /* switch */

    return (u1RetVal);
}

#endif /* __IPV6_NP_WR_C */
