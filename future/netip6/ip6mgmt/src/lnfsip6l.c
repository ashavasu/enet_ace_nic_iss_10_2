/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: lnfsip6l.c,v 1.47 2017/12/26 13:34:22 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
#include "lip6mgin.h"
#include "ip6cli.h"
#include "fsmsipcli.h"
#include "fsmpipv6cli.h"
#include "png6prot.h"
#include "png6glob.h"
#ifdef NPAPI_WANTED
#include "nputil.h"
#endif
extern UINT4        Fsipv6PrefixAdminStatus[13];
extern UINT4        FsMIStdInetCidrRoutePreference[12];

UINT4               gu4Ip6PrevRouteInvalid = OSIX_FALSE;
INT4                gi4Ip6ValidRouteRetVal = NETIPV6_SUCCESS;
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsipv6NdCacheMaxRetries
 Input       :  The Indices

                The Object 
                retValFsipv6NdCacheMaxRetries
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6NdCacheMaxRetries (INT4 *pi4RetValFsipv6NdCacheMaxRetries)
{
    /* Not supported */
    *pi4RetValFsipv6NdCacheMaxRetries = 0;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6PmtuConfigStatus
 Input       :  The Indices

                The Object 
                retValFsipv6PmtuConfigStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6PmtuConfigStatus (INT4 *pi4RetValFsipv6PmtuConfigStatus)
{
    /* Not supported */
    *pi4RetValFsipv6PmtuConfigStatus = 0;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6PmtuTimeOutInterval
 Input       :  The Indices

                The Object 
                retValFsipv6PmtuTimeOutInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6PmtuTimeOutInterval (UINT4 *pu4RetValFsipv6PmtuTimeOutInterval)
{
    /* Not supported */
    *pu4RetValFsipv6PmtuTimeOutInterval = 0;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6JumboEnable
 Input       :  The Indices

                The Object 
                retValFsipv6JumboEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6JumboEnable (INT4 *pi4RetValFsipv6JumboEnable)
{
    /* Not supported */
    *pi4RetValFsipv6JumboEnable = 0;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6NumOfSendJumbo
 Input       :  The Indices

                The Object 
                retValFsipv6NumOfSendJumbo
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6NumOfSendJumbo (INT4 *pi4RetValFsipv6NumOfSendJumbo)
{
    /* Not supported */
    *pi4RetValFsipv6NumOfSendJumbo = 0;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6NumOfRecvJumbo
 Input       :  The Indices

                The Object 
                retValFsipv6NumOfRecvJumbo
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6NumOfRecvJumbo (INT4 *pi4RetValFsipv6NumOfRecvJumbo)
{
    /* Not supported */
    *pi4RetValFsipv6NumOfRecvJumbo = 0;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6ErrJumbo
 Input       :  The Indices

                The Object 
                retValFsipv6ErrJumbo
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6ErrJumbo (INT4 *pi4RetValFsipv6ErrJumbo)
{
    /* Not supported */
    *pi4RetValFsipv6ErrJumbo = 0;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6GlobalDebug
 Input       :  The Indices

                The Object 
                retValFsipv6GlobalDebug
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6GlobalDebug (UINT4 *pu4RetValFsipv6GlobalDebug)
{
    *pu4RetValFsipv6GlobalDebug = gIp6GblInfo.u4DebugFlag;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6MaxRouteEntries
 Input       :  The Indices

                The Object 
                retValFsipv6MaxRouteEntries
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6MaxRouteEntries (UINT4 *pu4RetValFsipv6MaxRouteEntries)
{
    *pu4RetValFsipv6MaxRouteEntries = (UINT4) MAX_RTM6_ROUTE_TABLE_ENTRIES;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6MaxLogicalIfaces
 Input       :  The Indices

                The Object 
                retValFsipv6MaxLogicalIfaces
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6MaxLogicalIfaces (UINT4 *pu4RetValFsipv6MaxLogicalIfaces)
{
    *pu4RetValFsipv6MaxLogicalIfaces = (UINT4) IP6_MAX_LOGICAL_IFACES;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6MaxTunnelIfaces
 Input       :  The Indices

                The Object 
                retValFsipv6MaxTunnelIfaces
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6MaxTunnelIfaces (UINT4 *pu4RetValFsipv6MaxTunnelIfaces)
{
#ifdef TUNNEL_WANTED
    *pu4RetValFsipv6MaxTunnelIfaces = MAX_IP6_TUNNEL_INTERFACES;
    return SNMP_SUCCESS;
#else
    *pu4RetValFsipv6MaxTunnelIfaces = 0;
    return SNMP_SUCCESS;
#endif
}

/****************************************************************************
 Function    :  nmhGetFsipv6MaxAddresses
 Input       :  The Indices

                The Object 
                retValFsipv6MaxAddresses
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6MaxAddresses (UINT4 *pu4RetValFsipv6MaxAddresses)
{
    *pu4RetValFsipv6MaxAddresses = MAX_IP6_UNICAST_ADDRESS;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6MaxFragReasmEntries
 Input       :  The Indices

                The Object 
                retValFsipv6MaxFragReasmEntries
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6MaxFragReasmEntries (UINT4 *pu4RetValFsipv6MaxFragReasmEntries)
{
    /* Not Supported */
    *pu4RetValFsipv6MaxFragReasmEntries = 0;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6Nd6MaxCacheEntries
 Input       :  The Indices

                The Object 
                retValFsipv6Nd6MaxCacheEntries
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6Nd6MaxCacheEntries (UINT4 *pu4RetValFsipv6Nd6MaxCacheEntries)
{
    /* Not Supported */
    *pu4RetValFsipv6Nd6MaxCacheEntries = 0;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6PmtuMaxDest
 Input       :  The Indices

                The Object 
                retValFsipv6PmtuMaxDest
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6PmtuMaxDest (UINT4 *pu4RetValFsipv6PmtuMaxDest)
{
    /* Not Supported */
    *pu4RetValFsipv6PmtuMaxDest = 0;
    return SNMP_SUCCESS;
}

/****************************************************************************
 *  Function    :  nmhGetFsipv6ECMPPRTTimer
 *  Input       :  The Indices
 * 
 *                 The Object
 *                 retValFsipv6ECMPPRTTimer
 *  Output      :  The Get Low Lev Routine Take the Indices &
 *                 store the Value requested in the Return val.
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ****************************************************************************/
INT1
nmhGetFsipv6ECMPPRTTimer (INT4 *pi4RetValFsipv6ECMPPRTTimer)
{
    *pi4RetValFsipv6ECMPPRTTimer = gIp6GblInfo.i4ECMPPRTTInterval;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6NdCacheTimeout
 Input       :  The Indices

                The Object
                retValFsipv6NdCacheTimeout
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6NdCacheTimeout (INT4 *pi4RetValFsipv6NdCacheTimeout)
{
    *pi4RetValFsipv6NdCacheTimeout = gIp6GblInfo.i4NdCacheTimeout;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsipv6NdCacheMaxRetries
 Input       :  The Indices

                The Object 
                setValFsipv6NdCacheMaxRetries
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6NdCacheMaxRetries (INT4 i4SetValFsipv6NdCacheMaxRetries)
{
    /* Not supported */
    UNUSED_PARAM (i4SetValFsipv6NdCacheMaxRetries);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6PmtuConfigStatus
 Input       :  The Indices

                The Object 
                setValFsipv6PmtuConfigStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6PmtuConfigStatus (INT4 i4SetValFsipv6PmtuConfigStatus)
{
    /* Not supported */
    UNUSED_PARAM (i4SetValFsipv6PmtuConfigStatus);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6PmtuTimeOutInterval
 Input       :  The Indices

                The Object 
                setValFsipv6PmtuTimeOutInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6PmtuTimeOutInterval (UINT4 u4SetValFsipv6PmtuTimeOutInterval)
{
    /* Not supported */
    UNUSED_PARAM (u4SetValFsipv6PmtuTimeOutInterval);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6JumboEnable
 Input       :  The Indices

                The Object 
                setValFsipv6JumboEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6JumboEnable (INT4 i4SetValFsipv6JumboEnable)
{
    /* Not supported */
    UNUSED_PARAM (i4SetValFsipv6JumboEnable);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6GlobalDebug
 Input       :  The Indices

                The Object 
                setValFsipv6GlobalDebug
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6GlobalDebug (UINT4 u4SetValFsipv6GlobalDebug)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = IPVX_ZERO;

    gIp6GblInfo.u4DebugFlag = u4SetValFsipv6GlobalDebug;
    RM_GET_SEQ_NUM (&u4SeqNum);

    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    SnmpNotifyInfo.pu4ObjectId = FsMIIpv6GlobalDebug;
    SnmpNotifyInfo.u4OidLen = sizeof (FsMIIpv6GlobalDebug) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = Ip6Lock;
    SnmpNotifyInfo.pUnLockPointer = Ip6UnLock;
    SnmpNotifyInfo.u4Indices = 0;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u", u4SetValFsipv6GlobalDebug));

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6MaxRouteEntries
 Input       :  The Indices

                The Object 
                setValFsipv6MaxRouteEntries
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6MaxRouteEntries (UINT4 u4SetValFsipv6MaxRouteEntries)
{
    /* DEPRECATED OBJECT */
    tIp6SystemSize      Ip6SystemSizeInfo;

    MEMSET (&Ip6SystemSizeInfo, 0, sizeof (tIp6SystemSize));
    /* Get the previous value */
    GetIp6SizingParams (&Ip6SystemSizeInfo);

    /* Modify the No. of Route Entries */
    Ip6SystemSizeInfo.u4Ip6MaxRoutes = u4SetValFsipv6MaxRouteEntries;

    /* Set the values */
    SetIp6SizingParams (&Ip6SystemSizeInfo);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6MaxLogicalIfaces
 Input       :  The Indices

                The Object 
                setValFsipv6MaxLogicalIfaces
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6MaxLogicalIfaces (UINT4 u4SetValFsipv6MaxLogicalIfaces)
{
    /* DEPRECATED OBJECT */
    tIp6SystemSize      Ip6SystemSizeInfo;

    MEMSET (&Ip6SystemSizeInfo, 0, sizeof (tIp6SystemSize));
    /* Get the previous value */
    GetIp6SizingParams (&Ip6SystemSizeInfo);

    /* Modify the No. of Route Entries */
    Ip6SystemSizeInfo.i4Ip6MaxLogicalIfaces =
        (INT4) u4SetValFsipv6MaxLogicalIfaces;

    /* Set the values */
    SetIp6SizingParams (&Ip6SystemSizeInfo);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6MaxTunnelIfaces
 Input       :  The Indices

                The Object 
                setValFsipv6MaxTunnelIfaces
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6MaxTunnelIfaces (UINT4 u4SetValFsipv6MaxTunnelIfaces)
{
    /* DEPRECATED OBJECT */
#ifdef TUNNEL_WANTED
    tIp6SystemSize      Ip6SystemSizeInfo;

    MEMSET (&Ip6SystemSizeInfo, 0, sizeof (tIp6SystemSize));
    /* Get the previous value */
    GetIp6SizingParams (&Ip6SystemSizeInfo);

    /* Modify the No. of Route Entries */
    Ip6SystemSizeInfo.u4Ip6MaxTunnelIfaces = u4SetValFsipv6MaxTunnelIfaces;

    /* Set the values */
    SetIp6SizingParams (&Ip6SystemSizeInfo);

    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (u4SetValFsipv6MaxTunnelIfaces);
    return SNMP_FAILURE;
#endif
}

/****************************************************************************
 Function    :  nmhSetFsipv6MaxAddresses
 Input       :  The Indices

                The Object 
                setValFsipv6MaxAddresses
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6MaxAddresses (UINT4 u4SetValFsipv6MaxAddresses)
{
    /* DEPRECATED OBJECT */
    tIp6SystemSize      Ip6SystemSizeInfo;

    MEMSET (&Ip6SystemSizeInfo, 0, sizeof (tIp6SystemSize));
    /* Get the previous value */
    GetIp6SizingParams (&Ip6SystemSizeInfo);

    /* Modify the No. of Route Entries */
    Ip6SystemSizeInfo.u4Ip6MaxAddr = u4SetValFsipv6MaxAddresses;

    /* Set the values */
    SetIp6SizingParams (&Ip6SystemSizeInfo);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6MaxFragReasmEntries
 Input       :  The Indices

                The Object 
                setValFsipv6MaxFragReasmEntries
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6MaxFragReasmEntries (UINT4 u4SetValFsipv6MaxFragReasmEntries)
{
    /* DEPRECATED OBJECT */
    tIp6SystemSize      Ip6SystemSizeInfo;

    MEMSET (&Ip6SystemSizeInfo, 0, sizeof (tIp6SystemSize));
    /* Get the previous value */
    GetIp6SizingParams (&Ip6SystemSizeInfo);

    /* Modify the No. of Route Entries */
    Ip6SystemSizeInfo.u4Ip6MaxFragReasmList = u4SetValFsipv6MaxFragReasmEntries;

    /* Set the values */
    SetIp6SizingParams (&Ip6SystemSizeInfo);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6Nd6MaxCacheEntries
 Input       :  The Indices

                The Object 
                setValFsipv6Nd6MaxCacheEntries
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6Nd6MaxCacheEntries (UINT4 u4SetValFsipv6Nd6MaxCacheEntries)
{
    /* DEPRECATED OBJECT */
    tIp6SystemSize      Ip6SystemSizeInfo;

    MEMSET (&Ip6SystemSizeInfo, 0, sizeof (tIp6SystemSize));
    /* Get the previous value */
    GetIp6SizingParams (&Ip6SystemSizeInfo);

    /* Modify the No. of Route Entries */
    Ip6SystemSizeInfo.u4Ip6MaxCacheEntries = u4SetValFsipv6Nd6MaxCacheEntries;

    /* Set the values */
    SetIp6SizingParams (&Ip6SystemSizeInfo);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6PmtuMaxDest
 Input       :  The Indices

                The Object 
                setValFsipv6PmtuMaxDest
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6PmtuMaxDest (UINT4 u4SetValFsipv6PmtuMaxDest)
{
    /* DEPRECATED OBJECT */
    tIp6SystemSize      Ip6SystemSizeInfo;

    MEMSET (&Ip6SystemSizeInfo, 0, sizeof (tIp6SystemSize));
    /* Get the previous value */
    GetIp6SizingParams (&Ip6SystemSizeInfo);

    /* Modify the No. of Route Entries */
    Ip6SystemSizeInfo.u4Ip6MaxPmtuEntries = u4SetValFsipv6PmtuMaxDest;

    /* Set the values */
    SetIp6SizingParams (&Ip6SystemSizeInfo);

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Fsipv6NdCacheMaxRetries
 Input       :  The Indices

                The Object 
                testValFsipv6NdCacheMaxRetries
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6NdCacheMaxRetries (UINT4 *pu4ErrorCode,
                                  INT4 i4TestValFsipv6NdCacheMaxRetries)
{
    /* Not supported */
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    UNUSED_PARAM (i4TestValFsipv6NdCacheMaxRetries);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6PmtuConfigStatus
 Input       :  The Indices

                The Object 
                testValFsipv6PmtuConfigStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6PmtuConfigStatus (UINT4 *pu4ErrorCode,
                                 INT4 i4TestValFsipv6PmtuConfigStatus)
{
    /* Not supported */
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    UNUSED_PARAM (i4TestValFsipv6PmtuConfigStatus);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6PmtuTimeOutInterval
 Input       :  The Indices

                The Object 
                testValFsipv6PmtuTimeOutInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6PmtuTimeOutInterval (UINT4 *pu4ErrorCode,
                                    UINT4 u4TestValFsipv6PmtuTimeOutInterval)
{
    /* Not supported */
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    UNUSED_PARAM (u4TestValFsipv6PmtuTimeOutInterval);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6JumboEnable
 Input       :  The Indices

                The Object 
                testValFsipv6JumboEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6JumboEnable (UINT4 *pu4ErrorCode,
                            INT4 i4TestValFsipv6JumboEnable)
{
    /* Not supported */
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    UNUSED_PARAM (i4TestValFsipv6JumboEnable);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6GlobalDebug
 Input       :  The Indices

                The Object 
                testValFsipv6GlobalDebug
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6GlobalDebug (UINT4 *pu4ErrorCode,
                            UINT4 u4TestValFsipv6GlobalDebug)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4TestValFsipv6GlobalDebug);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6MaxRouteEntries
 Input       :  The Indices

                The Object 
                testValFsipv6MaxRouteEntries
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6MaxRouteEntries (UINT4 *pu4ErrorCode,
                                UINT4 u4TestValFsipv6MaxRouteEntries)
{
    if (u4TestValFsipv6MaxRouteEntries > MAX_RTM6_ROUTE_TABLE_ENTRIES)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6MaxLogicalIfaces
 Input       :  The Indices

                The Object 
                testValFsipv6MaxLogicalIfaces
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6MaxLogicalIfaces (UINT4 *pu4ErrorCode,
                                 UINT4 u4TestValFsipv6MaxLogicalIfaces)
{
    if (u4TestValFsipv6MaxLogicalIfaces == 0)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6MaxTunnelIfaces
 Input       :  The Indices

                The Object 
                testValFsipv6MaxTunnelIfaces
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6MaxTunnelIfaces (UINT4 *pu4ErrorCode,
                                UINT4 u4TestValFsipv6MaxTunnelIfaces)
{
#ifdef TUNNEL_WANTED
    if (u4TestValFsipv6MaxTunnelIfaces == 0)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
#else
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    UNUSED_PARAM (u4TestValFsipv6MaxTunnelIfaces);
    return SNMP_FAILURE;
#endif
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6MaxAddresses
 Input       :  The Indices

                The Object 
                testValFsipv6MaxAddresses
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6MaxAddresses (UINT4 *pu4ErrorCode,
                             UINT4 u4TestValFsipv6MaxAddresses)
{
    if (u4TestValFsipv6MaxAddresses == 0)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6MaxFragReasmEntries
 Input       :  The Indices

                The Object 
                testValFsipv6MaxFragReasmEntries
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6MaxFragReasmEntries (UINT4 *pu4ErrorCode,
                                    UINT4 u4TestValFsipv6MaxFragReasmEntries)
{
    if (u4TestValFsipv6MaxFragReasmEntries == 0)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6Nd6MaxCacheEntries
 Input       :  The Indices

                The Object 
                testValFsipv6Nd6MaxCacheEntries
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6Nd6MaxCacheEntries (UINT4 *pu4ErrorCode,
                                   UINT4 u4TestValFsipv6Nd6MaxCacheEntries)
{
    if (u4TestValFsipv6Nd6MaxCacheEntries == 0)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6PmtuMaxDest
 Input       :  The Indices

                The Object 
                testValFsipv6PmtuMaxDest
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6PmtuMaxDest (UINT4 *pu4ErrorCode,
                            UINT4 u4TestValFsipv6PmtuMaxDest)
{
    UNUSED_PARAM (u4TestValFsipv6PmtuMaxDest);
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    return SNMP_FAILURE;
}

 /****************************************************************************
 Function    :  nmhTestv2Fsipv6ECMPPRTTimer
 Input       :  The Indices

                The Object
                testValFsipv6ECMPPRTTimer
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6ECMPPRTTimer (UINT4 *pu4ErrorCode,
                             INT4 i4TestValFsipv6ECMPPRTTimer)
{
    if ((i4TestValFsipv6ECMPPRTTimer < IP6_ECMP_MIN_PRT_INTERVAL) ||
        (i4TestValFsipv6ECMPPRTTimer > IP6_ECMP_MAX_PRT_INTERVAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6NdCacheTimeout
 Input       :  The Indices

                The Object
                testValFsipv6NdCacheTimeout
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6NdCacheTimeout (UINT4 *pu4ErrorCode,
                               INT4 i4TestValFsipv6NdCacheTimeout)
{
    if (i4TestValFsipv6NdCacheTimeout == IP6_ZERO)
    {
        return SNMP_FAILURE;
    }

    if ((i4TestValFsipv6NdCacheTimeout < ND_MIN_CACHE_TIMEOUT)
        || (i4TestValFsipv6NdCacheTimeout > ND_MAX_CACHE_TIMEOUT))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_TOUT_ERR);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Fsipv6NdCacheMaxRetries
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsipv6NdCacheMaxRetries (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Fsipv6PmtuConfigStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsipv6PmtuConfigStatus (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Fsipv6PmtuTimeOutInterval
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsipv6PmtuTimeOutInterval (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Fsipv6JumboEnable
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsipv6JumboEnable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Fsipv6GlobalDebug
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsipv6GlobalDebug (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Fsipv6MaxRouteEntries
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsipv6MaxRouteEntries (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Fsipv6MaxLogicalIfaces
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsipv6MaxLogicalIfaces (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Fsipv6MaxTunnelIfaces
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsipv6MaxTunnelIfaces (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Fsipv6MaxAddresses
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsipv6MaxAddresses (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Fsipv6MaxFragReasmEntries
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsipv6MaxFragReasmEntries (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Fsipv6Nd6MaxCacheEntries
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsipv6Nd6MaxCacheEntries (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Fsipv6PmtuMaxDest
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsipv6PmtuMaxDest (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Fsipv6IfTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsipv6IfTable
 Input       :  The Indices
                Fsipv6IfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsipv6IfTable (INT4 i4Fsipv6IfIndex)
{
    if (Lip6UtlIfEntryExists ((UINT4) i4Fsipv6IfIndex) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsipv6IfTable
 Input       :  The Indices
                Fsipv6IfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsipv6IfTable (INT4 *pi4Fsipv6IfIndex)
{
    *pi4Fsipv6IfIndex = 0;

    if (Lip6UtlIfGetNextIndex (0, (UINT4 *) pi4Fsipv6IfIndex) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsipv6IfTable
 Input       :  The Indices
                Fsipv6IfIndex
                nextFsipv6IfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsipv6IfTable (INT4 i4Fsipv6IfIndex, INT4 *pi4NextFsipv6IfIndex)
{
    INT4                i4Index = (i4Fsipv6IfIndex) + 1;

    if (i4Fsipv6IfIndex <= 0)
    {
        return (nmhGetFirstIndexFsipv6IfTable (pi4NextFsipv6IfIndex));
    }

    for (; i4Index <= (INT4) LIP6_MAX_INTERFACES; i4Index++)
    {
        if (Lip6UtlIfEntryExists ((UINT4) i4Index) == OSIX_SUCCESS)
        {
            *pi4NextFsipv6IfIndex = i4Index;
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsipv6IfType
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                retValFsipv6IfType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfType (INT4 i4Fsipv6IfIndex, INT4 *pi4RetValFsipv6IfType)
{
    tLip6If            *pIf6Entry = NULL;

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsipv6IfType = pIf6Entry->u1IfType;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfPortNum
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                retValFsipv6IfPortNum
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfPortNum (INT4 i4Fsipv6IfIndex, INT4 *pi4RetValFsipv6IfPortNum)
{
    tLip6If            *pIf6Entry = NULL;

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsipv6IfPortNum = (INT4) pIf6Entry->u4IpPort;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfCircuitNum
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                retValFsipv6IfCircuitNum
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfCircuitNum (INT4 i4Fsipv6IfIndex,
                          INT4 *pi4RetValFsipv6IfCircuitNum)
{
    tLip6If            *pIf6Entry = NULL;

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsipv6IfCircuitNum = pIf6Entry->u2CktIndex;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfToken
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                retValFsipv6IfToken
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfToken (INT4 i4Fsipv6IfIndex,
                     tSNMP_OCTET_STRING_TYPE * pRetValFsipv6IfToken)
{
    tLip6If            *pIf6Entry = NULL;
    UINT1               u1TokLen;

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    u1TokLen = MEM_MAX_BYTES (pIf6Entry->u1TokLen, IP6_EUI_ADDRESS_LEN);
    if (u1TokLen != 0)
    {
        MEMCPY (pRetValFsipv6IfToken->pu1_OctetList, pIf6Entry->ifaceTok,
                u1TokLen);
    }

    pRetValFsipv6IfToken->i4_Length = u1TokLen;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfAdminStatus
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                retValFsipv6IfAdminStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfAdminStatus (INT4 i4Fsipv6IfIndex,
                           INT4 *pi4RetValFsipv6IfAdminStatus)
{
    tLip6If            *pIf6Entry = NULL;

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsipv6IfAdminStatus = (INT4) pIf6Entry->u1AdminStatus;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfOperStatus
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                retValFsipv6IfOperStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfOperStatus (INT4 i4Fsipv6IfIndex,
                          INT4 *pi4RetValFsipv6IfOperStatus)
{
    tLip6If            *pIf6Entry = NULL;

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsipv6IfOperStatus = (INT4) pIf6Entry->u1OperStatus;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfRouterAdvStatus
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                retValFsipv6IfRouterAdvStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfRouterAdvStatus (INT4 i4Fsipv6IfIndex,
                               INT4 *pi4RetValFsipv6IfRouterAdvStatus)
{
    tLip6If            *pIf6Entry = NULL;

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsipv6IfRouterAdvStatus = pIf6Entry->u1RAdvStatus;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsipv6IfRouterAdvFlags
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                retValFsipv6IfRouterAdvFlags
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfRouterAdvFlags (INT4 i4Fsipv6IfIndex,
                              INT4 *pi4RetValFsipv6IfRouterAdvFlags)
{
    tLip6If            *pIf6Entry = NULL;
    UINT1               u1Mbit;
    UINT1               u1_obit;

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    u1Mbit = (UINT1) (pIf6Entry->u1RAAdvFlags & IP6_IF_M_BIT_ADV);
    u1_obit = (UINT1) (pIf6Entry->u1RAAdvFlags & IP6_IF_O_BIT_ADV);

    if (u1Mbit && u1_obit)
    {
        *pi4RetValFsipv6IfRouterAdvFlags = IP6_IF_ROUT_ADV_BOTH_BIT;
    }
    else if (u1Mbit)
    {
        *pi4RetValFsipv6IfRouterAdvFlags = IP6_IF_ROUT_ADV_M_BIT;
    }
    else if (u1_obit)
    {
        *pi4RetValFsipv6IfRouterAdvFlags = IP6_IF_ROUT_ADV_O_BIT;
    }
    else
    {
        *pi4RetValFsipv6IfRouterAdvFlags = IP6_IF_ROUT_ADV_NO_BIT;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfHopLimit
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                retValFsipv6IfHopLimit
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfHopLimit (INT4 i4Fsipv6IfIndex, INT4 *pi4RetValFsipv6IfHopLimit)
{
    tLip6If            *pIf6Entry = NULL;

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsipv6IfHopLimit = (INT4) pIf6Entry->u4RACurHopLimit;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfDefRouterTime
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                retValFsipv6IfDefRouterTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfDefRouterTime (INT4 i4Fsipv6IfIndex,
                             INT4 *pi4RetValFsipv6IfDefRouterTime)
{
    tLip6If            *pIf6Entry = NULL;

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsipv6IfDefRouterTime = (INT4) pIf6Entry->u4DefRaLifetime;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsipv6IfReachableTime
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                retValFsipv6IfReachableTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfReachableTime (INT4 i4Fsipv6IfIndex,
                             INT4 *pi4RetValFsipv6IfReachableTime)
{
    tLip6If            *pIf6Entry = NULL;

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsipv6IfReachableTime = (INT4) pIf6Entry->u4RAReachableTime;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfRetransmitTime
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                retValFsipv6IfRetransmitTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfRetransmitTime (INT4 i4Fsipv6IfIndex,
                              INT4 *pi4RetValFsipv6IfRetransmitTime)
{

    tLip6If            *pIf6Entry = NULL;

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsipv6IfRetransmitTime = (INT4) pIf6Entry->u4RARetransTimer;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfDelayProbeTime
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                retValFsipv6IfDelayProbeTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfDelayProbeTime (INT4 i4Fsipv6IfIndex,
                              INT4 *pi4RetValFsipv6IfDelayProbeTime)
{
    UNUSED_PARAM (i4Fsipv6IfIndex);
    *pi4RetValFsipv6IfDelayProbeTime = 0;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfPrefixAdvStatus
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                retValFsipv6IfPrefixAdvStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfPrefixAdvStatus (INT4 i4Fsipv6IfIndex,
                               INT4 *pi4RetValFsipv6IfPrefixAdvStatus)
{
    tLip6If            *pIf6Entry = NULL;

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsipv6IfPrefixAdvStatus = pIf6Entry->u1PrefAdv;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfMinRouterAdvTime
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                retValFsipv6IfMinRouterAdvTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfMinRouterAdvTime (INT4 i4Fsipv6IfIndex,
                                INT4 *pi4RetValFsipv6IfMinRouterAdvTime)
{
    tLip6If            *pIf6Entry = NULL;

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsipv6IfMinRouterAdvTime = (INT4) pIf6Entry->u4MinRaTime;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfMaxRouterAdvTime
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                retValFsipv6IfMaxRouterAdvTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfMaxRouterAdvTime (INT4 i4Fsipv6IfIndex,
                                INT4 *pi4RetValFsipv6IfMaxRouterAdvTime)
{
    tLip6If            *pIf6Entry = NULL;

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsipv6IfMaxRouterAdvTime = (INT4) pIf6Entry->u4MaxRaTime;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfDADRetries
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                retValFsipv6IfDADRetries
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfDADRetries (INT4 i4Fsipv6IfIndex,
                          INT4 *pi4RetValFsipv6IfDADRetries)
{
    tLip6If            *pIf6Entry = NULL;

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsipv6IfDADRetries = (INT4) pIf6Entry->u4DadAttempts;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsipv6IfToken
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                setValFsipv6IfToken
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6IfToken (INT4 i4Fsipv6IfIndex,
                     tSNMP_OCTET_STRING_TYPE * pSetValFsipv6IfToken)
{
    tLip6If            *pIf6Entry = NULL;

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pIf6Entry->u1IfType != IP6_FR_INTERFACE_TYPE)
    {
        /* This object can be set only for the Frame Relay interface. */
        return SNMP_FAILURE;
    }

    pIf6Entry->u1TokLen = (UINT1) pSetValFsipv6IfToken->i4_Length;
    MEMCPY (pIf6Entry->ifaceTok, pSetValFsipv6IfToken->pu1_OctetList,
            pSetValFsipv6IfToken->i4_Length);

    IncMsrForIpv6IfTable (i4Fsipv6IfIndex, 'i', pSetValFsipv6IfToken,
                          FsMIIpv6IfToken,
                          sizeof (FsMIIpv6IfToken) / sizeof (UINT4));
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsipv6IfAdminStatus
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                setValFsipv6IfAdminStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6IfAdminStatus (INT4 i4Fsipv6IfIndex,
                           INT4 i4SetValFsipv6IfAdminStatus)
{
    tLip6If            *pIf6Entry = NULL;
    UINT4               u4TmpDefLifetime = 0;
    INT4                i4CacheTimeout;
    UINT1               u1IfName[IP6_MAX_IF_NAME_LEN];
    CHR1                ac1Command[LNX_MAX_COMMAND_LEN];

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pIf6Entry->u1AdminStatus == i4SetValFsipv6IfAdminStatus)
    {
        /* No change in the Status. */
        return SNMP_SUCCESS;
    }
    u4TmpDefLifetime = pIf6Entry->u4DefRaLifetime;

    if (i4SetValFsipv6IfAdminStatus == NETIPV6_ADMIN_UP)
    {
        nmhSetFsipv6IfDefRouterTime (i4Fsipv6IfIndex, (INT4) u4TmpDefLifetime);
        Lip6RAConfig (pIf6Entry->u4ContextId);
        /* Enable IPv6 Over this interface. */
        if (Lip6IfEnable (pIf6Entry) != OSIX_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        if ((pIf6Entry->u1OperStatus == NETIPV6_OPER_UP))
        {
            nmhGetFsipv6NdCacheTimeout (&i4CacheTimeout);
        }
        else
        {
            i4CacheTimeout = ND_DEF_CACHE_TIMEOUT;
        }
        CfaGetInterfaceNameFromIndex (pIf6Entry->u4IfIndex, u1IfName);
        SNPRINTF (ac1Command, LNX_MAX_COMMAND_LEN,
                  "echo %d >/proc/sys/net/ipv6/neigh/%s/base_reachable_time",
                  i4CacheTimeout, u1IfName);
        system (ac1Command);
    }
    else
    {
        nmhSetFsipv6IfDefRouterTime (i4Fsipv6IfIndex, 0);
        Lip6RAConfig (pIf6Entry->u4ContextId);
        /* IPv6 needs to disabled over this interface. */
        if (Lip6IfDisable (pIf6Entry) != OSIX_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        CfaGetInterfaceNameFromIndex (pIf6Entry->u4IfIndex, u1IfName);
        SNPRINTF (ac1Command, LNX_MAX_COMMAND_LEN,
                  "echo %d >/proc/sys/net/ipv6/neigh/%s/base_reachable_time",
                  ND_DEF_CACHE_TIMEOUT, u1IfName);
        system (ac1Command);
    }
    pIf6Entry->u4DefRaLifetime = u4TmpDefLifetime;

    IncMsrForIPVXIfTable (i4Fsipv6IfIndex, i4SetValFsipv6IfAdminStatus,
                          FsMIStdIpv6InterfaceEnableStatus,
                          (sizeof (FsMIStdIpv6InterfaceEnableStatus) /
                           sizeof (UINT4)), FALSE);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6IfRouterAdvStatus
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                setValFsipv6IfRouterAdvStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6IfRouterAdvStatus (INT4 i4Fsipv6IfIndex,
                               INT4 i4SetValFsipv6IfRouterAdvStatus)
{
    INT4                i4RetVal = 0;

    tLip6If            *pIf6Entry = NULL;

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    i4RetVal = Ip6SetRtrAdvtSendAdverts (i4Fsipv6IfIndex,
                                         i4SetValFsipv6IfRouterAdvStatus);

    if (i4RetVal == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pIf6Entry->u1RAdvStatus = (UINT1) i4SetValFsipv6IfRouterAdvStatus;
    IncMsrForIpv6IfTable (i4Fsipv6IfIndex, 'i',
                          &i4SetValFsipv6IfRouterAdvStatus,
                          FsMIIpv6IfRouterAdvStatus,
                          sizeof (FsMIIpv6IfRouterAdvStatus) / sizeof (UINT4));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6IfRouterAdvFlags
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                setValFsipv6IfRouterAdvFlags
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6IfRouterAdvFlags (INT4 i4Fsipv6IfIndex,
                              INT4 i4SetValFsipv6IfRouterAdvFlags)
{
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (i4SetValFsipv6IfRouterAdvFlags);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsipv6IfHopLimit
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                setValFsipv6IfHopLimit
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6IfHopLimit (INT4 i4Fsipv6IfIndex, INT4 i4SetValFsipv6IfHopLimit)
{
    tLip6If            *pIf6Entry = NULL;
    INT4                i4RetVal = OSIX_FAILURE;

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    i4RetVal = Lip6KernSetIfHopLimit ((UINT4) i4Fsipv6IfIndex,
                                      i4SetValFsipv6IfHopLimit);

    if (i4RetVal != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    pIf6Entry->u4HopLimit = (UINT1) i4SetValFsipv6IfHopLimit;

    IncMsrForIpv6IfTable (i4Fsipv6IfIndex, 'i', &i4SetValFsipv6IfHopLimit,
                          FsMIIpv6IfHopLimit,
                          sizeof (FsMIIpv6IfHopLimit) / sizeof (UINT4));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6IfDefRouterTime
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                setValFsipv6IfDefRouterTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6IfDefRouterTime (INT4 i4Fsipv6IfIndex,
                             INT4 i4SetValFsipv6IfDefRouterTime)
{
    tLip6If            *pIf6Entry = NULL;

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    pIf6Entry->u4DefRaLifetime = (UINT4) i4SetValFsipv6IfDefRouterTime;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6IfReachableTime
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                setValFsipv6IfReachableTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6IfReachableTime (INT4 i4Fsipv6IfIndex,
                             INT4 i4SetValFsipv6IfReachableTime)
{
    tLip6If            *pIf6Entry = NULL;

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    pIf6Entry->u4RAReachableTime = (UINT4) i4SetValFsipv6IfReachableTime;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsipv6IfRetransmitTime
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                setValFsipv6IfRetransmitTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6IfRetransmitTime (INT4 i4Fsipv6IfIndex,
                              INT4 i4SetValFsipv6IfRetransmitTime)
{
    tLip6If            *pIf6Entry = NULL;

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }
    pIf6Entry->u4RARetransTimer = (UINT4) i4SetValFsipv6IfRetransmitTime;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsipv6IfDelayProbeTime
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                setValFsipv6IfDelayProbeTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6IfDelayProbeTime (INT4 i4Fsipv6IfIndex,
                              INT4 i4SetValFsipv6IfDelayProbeTime)
{
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (i4SetValFsipv6IfDelayProbeTime);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsipv6IfPrefixAdvStatus
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                setValFsipv6IfPrefixAdvStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6IfPrefixAdvStatus (INT4 i4Fsipv6IfIndex,
                               INT4 i4SetValFsipv6IfPrefixAdvStatus)
{
    tLip6If            *pIf6Entry = NULL;

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pIf6Entry->u1PrefAdv == i4SetValFsipv6IfPrefixAdvStatus)
    {
        return SNMP_SUCCESS;
    }

    pIf6Entry->u1PrefAdv = (UINT1) i4SetValFsipv6IfPrefixAdvStatus;

    Lip6RAConfig (pIf6Entry->u4ContextId);

    IncMsrForIpv6IfTable (i4Fsipv6IfIndex, 'i',
                          &i4SetValFsipv6IfPrefixAdvStatus,
                          FsMIIpv6IfPrefixAdvStatus,
                          sizeof (FsMIIpv6IfPrefixAdvStatus) / sizeof (UINT4));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6IfMinRouterAdvTime
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                setValFsipv6IfMinRouterAdvTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6IfMinRouterAdvTime (INT4 i4Fsipv6IfIndex,
                                INT4 i4SetValFsipv6IfMinRouterAdvTime)
{
    INT1                i1RetVal = 0;
    tLip6If            *pIf6Entry = NULL;

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfIndex);

    i1RetVal = Ip6SetRtrAdvtMinInterval (i4Fsipv6IfIndex,
                                         (UINT4)
                                         i4SetValFsipv6IfMinRouterAdvTime);

    if (i1RetVal == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    pIf6Entry->u4MinRaTime = (UINT4) i4SetValFsipv6IfMinRouterAdvTime;

    IncMsrForIpv6IfTable (i4Fsipv6IfIndex, 'i',
                          &i4SetValFsipv6IfMinRouterAdvTime,
                          FsMIIpv6IfMinRouterAdvTime,
                          sizeof (FsMIIpv6IfMinRouterAdvTime) / sizeof (UINT4));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6IfMaxRouterAdvTime
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                setValFsipv6IfMaxRouterAdvTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6IfMaxRouterAdvTime (INT4 i4Fsipv6IfIndex,
                                INT4 i4SetValFsipv6IfMaxRouterAdvTime)
{
    INT1                i1RetVal = 0;
    tLip6If            *pIf6Entry = NULL;

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfIndex);

    i1RetVal = Ip6SetRtrAdvtMaxInterval (i4Fsipv6IfIndex,
                                         (UINT4)
                                         i4SetValFsipv6IfMaxRouterAdvTime);

    if (i1RetVal == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    pIf6Entry->u4MaxRaTime = (UINT4) i4SetValFsipv6IfMaxRouterAdvTime;

    IncMsrForIpv6IfTable (i4Fsipv6IfIndex, 'i',
                          &i4SetValFsipv6IfMaxRouterAdvTime,
                          FsMIIpv6IfMaxRouterAdvTime,
                          sizeof (FsMIIpv6IfMaxRouterAdvTime) / sizeof (UINT4));
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsipv6IfDADRetries
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                setValFsipv6IfDADRetries
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6IfDADRetries (INT4 i4Fsipv6IfIndex, INT4 i4SetValFsipv6IfDADRetries)
{
    tLip6If            *pIf6Entry = NULL;

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (i4SetValFsipv6IfDADRetries == (INT4) pIf6Entry->u4DadAttempts)
    {
        return SNMP_SUCCESS;
    }

    if (Lip6KernSetIfDadAttempts
        ((UINT4) i4Fsipv6IfIndex, i4SetValFsipv6IfDADRetries) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    pIf6Entry->u4DadAttempts = (UINT4) i4SetValFsipv6IfDADRetries;
    return SNMP_SUCCESS;
}

 /****************************************************************************
*  Function    :  nmhSetFsipv6ECMPPRTTimer
*  Input       :  The Indices
* 
*                 The Object
*                 setValFsipv6ECMPPRTTimer
*  Output      :  The Set Low Lev Routine Take the Indices &
*                 Sets the Value accordingly.
*  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
* ****************************************************************************/
INT1
nmhSetFsipv6ECMPPRTTimer (INT4 i4SetValFsipv6ECMPPRTTimer)
{
    gIp6GblInfo.i4ECMPPRTTInterval = i4SetValFsipv6ECMPPRTTimer;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsipv6NdCacheTimeout
 Input       :  The Indices

                The Object
                setValFsipv6NdCacheTimeout
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6NdCacheTimeout (INT4 i4SetValFsipv6NdCacheTimeout)
{
    UINT4               u4L3ContextId = 0;
    UINT4               u4NextL3ContextId = 0;
    UINT4               au4Ip6Ifaces[IP6_MAX_LOGICAL_IFACES + 1];
    CHR1                ac1Command[LNX_MAX_COMMAND_LEN];
    UINT1               u1IfName[IP6_MAX_IF_NAME_LEN];
    INT4                i4RetVal = 0;
    UINT2               u2Cntr = IP6_ZERO;
    UINT1               au1ContextName[VCMALIAS_MAX_ARRAY_LEN];
    MEMSET (u1IfName, 0, sizeof (u1IfName));

    i4RetVal = VcmGetFirstActiveL3Context (&u4L3ContextId);
    while (i4RetVal == VCM_SUCCESS)
    {
        i4RetVal = VcmGetNextActiveL3Context (u4L3ContextId,
                                              &u4NextL3ContextId);
        MEMSET (au4Ip6Ifaces, IP6_ZERO,
                (IP6_MAX_LOGICAL_IFACES + 1) * sizeof (UINT4));
        if (VcmGetCxtIpIfaceList (u4L3ContextId, au4Ip6Ifaces) == VCM_FAILURE)
        {
            u4L3ContextId = u4NextL3ContextId;
            continue;
        }
        VcmGetAliasName (u4L3ContextId, au1ContextName);
        for (u2Cntr = 1; ((u2Cntr <= au4Ip6Ifaces[0]) &&
                          (u2Cntr < IP6_MAX_LOGICAL_IFACES + 1)); u2Cntr++)
        {
#ifndef LNXIP6_WANTED
            /* Check for Valid Interface . */
            if (Ip6ifEntryExists (au4Ip6Ifaces[u2Cntr]) != IP6_FAILURE)
#else
            if (Lip6UtlIfEntryExists (au4Ip6Ifaces[u2Cntr]) != OSIX_FAILURE)
#endif
            {
                CfaGetInterfaceNameFromIndex (au4Ip6Ifaces[u2Cntr], u1IfName);

                SNPRINTF (ac1Command, LNX_MAX_COMMAND_LEN,
                          "echo %d >/proc/sys/net/ipv6/neigh/%s/base_reachable_time",
                          i4SetValFsipv6NdCacheTimeout, u1IfName);
                system (ac1Command);
            }
        }
        u4L3ContextId = u4NextL3ContextId;
    }
    gIp6GblInfo.i4NdCacheTimeout = i4SetValFsipv6NdCacheTimeout;
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Fsipv6IfToken
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                testValFsipv6IfToken
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6IfToken (UINT4 *pu4ErrorCode, INT4 i4Fsipv6IfIndex,
                        tSNMP_OCTET_STRING_TYPE * pTestValFsipv6IfToken)
{
    tLip6If            *pIf6Entry = NULL;

    if (((pTestValFsipv6IfToken->i4_Length) > IP6_EUI_ADDRESS_LEN) ||
        pTestValFsipv6IfToken->i4_Length == 0)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pIf6Entry->u1IfType != IP6_FR_INTERFACE_TYPE)
    {
        /* This object can be set only for the Frame Relay interface. */
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6IfAdminStatus
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                testValFsipv6IfAdminStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6IfAdminStatus (UINT4 *pu4ErrorCode,
                              INT4 i4Fsipv6IfIndex,
                              INT4 i4TestValFsipv6IfAdminStatus)
{
    tLip6If            *pIf6Entry = NULL;

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfIndex);
    if (pIf6Entry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
        return SNMP_FAILURE;
    }

    if ((i4TestValFsipv6IfAdminStatus != NETIPV6_ADMIN_UP) &&
        (i4TestValFsipv6IfAdminStatus != NETIPV6_ADMIN_DOWN))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_INVALID_ADMIN_STATUS);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6IfRouterAdvStatus
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                testValFsipv6IfRouterAdvStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6IfRouterAdvStatus (UINT4 *pu4ErrorCode, INT4 i4Fsipv6IfIndex,
                                  INT4 i4TestValFsipv6IfRouterAdvStatus)
{
    tLip6If            *pIf6Entry = NULL;

    if (CheckIp6IsFwdEnabled (i4Fsipv6IfIndex) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfIndex);
    if (pIf6Entry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
        return SNMP_FAILURE;
    }

    if ((pIf6Entry->u1IfType != IP6_ENET_INTERFACE_TYPE) &&
#ifdef TUNNEL_WANTED
        (pIf6Entry->u1IfType != IP6_TUNNEL_INTERFACE_TYPE) &&
#endif /* TUNNEL_WANTED */
        (pIf6Entry->u1IfType != IP6_L3VLAN_INTERFACE_TYPE) &&
        (pIf6Entry->u1IfType != IP6_LAGG_INTERFACE_TYPE))
    {
        /* RA is currently not supported for these interfaces. */
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
        return SNMP_FAILURE;
    }

    if (i4TestValFsipv6IfRouterAdvStatus != IP6_IF_ROUT_ADV_ENABLED &&
        i4TestValFsipv6IfRouterAdvStatus != IP6_IF_ROUT_ADV_DISABLED)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_INVALID_RT_ADV_STATUS);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6IfRouterAdvFlags
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                testValFsipv6IfRouterAdvFlags
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6IfRouterAdvFlags (UINT4 *pu4ErrorCode, INT4 i4Fsipv6IfIndex,
                                 INT4 i4TestValFsipv6IfRouterAdvFlags)
{

    tLip6If            *pIf6Entry = NULL;

    if (CheckIp6IsFwdEnabled (i4Fsipv6IfIndex) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfIndex);
    if (pIf6Entry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
        return SNMP_FAILURE;
    }

    if (i4TestValFsipv6IfRouterAdvFlags != IP6_IF_ROUT_ADV_M_BIT &&
        i4TestValFsipv6IfRouterAdvFlags != IP6_IF_ROUT_ADV_O_BIT &&
        i4TestValFsipv6IfRouterAdvFlags != IP6_IF_ROUT_ADV_NO_M_BIT &&
        i4TestValFsipv6IfRouterAdvFlags != IP6_IF_ROUT_ADV_NO_O_BIT &&
        i4TestValFsipv6IfRouterAdvFlags != IP6_IF_ROUT_ADV_BOTH_BIT &&
        i4TestValFsipv6IfRouterAdvFlags != IP6_IF_ROUT_ADV_NO_BIT)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_INVALID_RT_ADV_FLAGS);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6IfHopLimit
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                testValFsipv6IfHopLimit
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6IfHopLimit (UINT4 *pu4ErrorCode, INT4 i4Fsipv6IfIndex,
                           INT4 i4TestValFsipv6IfHopLimit)
{
    tLip6If            *pIf6Entry = NULL;

    if (CheckIp6IsFwdEnabled (i4Fsipv6IfIndex) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfIndex);
    if (pIf6Entry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
        return SNMP_FAILURE;
    }

    if (i4TestValFsipv6IfHopLimit < IP6_IF_MIN_HOPLMT ||
        i4TestValFsipv6IfHopLimit > IP6_IF_MAX_HOPLMT)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_INVALID_HOPLIMIT);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6IfDefRouterTime
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                testValFsipv6IfDefRouterTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6IfDefRouterTime (UINT4 *pu4ErrorCode, INT4 i4Fsipv6IfIndex,
                                INT4 i4TestValFsipv6IfDefRouterTime)
{
    tLip6If            *pIf6Entry = NULL;

    if (CheckIp6IsFwdEnabled (i4Fsipv6IfIndex) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfIndex);
    if (pIf6Entry == NULL)
    {

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if ((i4TestValFsipv6IfDefRouterTime == 0) ||
        ((i4TestValFsipv6IfDefRouterTime >=
          IP6_IF_DEF_MAX_RA_TIME) &&
         (i4TestValFsipv6IfDefRouterTime <= IP6_IF_MAX_DEFTIME)))
    {

        return SNMP_SUCCESS;

    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    CLI_SET_ERR (CLI_IP6_INVALID_RA_LIFETIME);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6IfReachableTime
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                testValFsipv6IfReachableTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6IfReachableTime (UINT4 *pu4ErrorCode, INT4 i4Fsipv6IfIndex,
                                INT4 i4TestValFsipv6IfReachableTime)
{
    tLip6If            *pIf6Entry = NULL;

    if (CheckIp6IsFwdEnabled (i4Fsipv6IfIndex) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6Entry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
        return SNMP_FAILURE;
    }

    if (i4TestValFsipv6IfReachableTime < IP6_IF_MIN_REACHTIME ||
        i4TestValFsipv6IfReachableTime > IP6_IF_MAX_REACHTIME)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_INVALID_RA_REACHTIME);
        return (SNMP_FAILURE);
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6IfRetransmitTime
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                testValFsipv6IfRetransmitTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6IfRetransmitTime (UINT4 *pu4ErrorCode, INT4 i4Fsipv6IfIndex,
                                 INT4 i4TestValFsipv6IfRetransmitTime)
{

    tLip6If            *pIf6Entry = NULL;

    if (CheckIp6IsFwdEnabled (i4Fsipv6IfIndex) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6Entry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
        return SNMP_FAILURE;
    }

    if (i4TestValFsipv6IfRetransmitTime < IP6_IF_MIN_RETTIME
        || i4TestValFsipv6IfRetransmitTime > IP6_IF_MAX_RETTIME)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_INVALID_RA_RETTIME);
        return (SNMP_FAILURE);
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6IfDelayProbeTime
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                testValFsipv6IfDelayProbeTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6IfDelayProbeTime (UINT4 *pu4ErrorCode, INT4 i4Fsipv6IfIndex,
                                 INT4 i4TestValFsipv6IfDelayProbeTime)
{
    tLip6If            *pIf6Entry = NULL;

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6Entry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (i4TestValFsipv6IfDelayProbeTime < IP6_IF_MIN_PDTIME ||
        i4TestValFsipv6IfDelayProbeTime > IP6_IF_MAX_PDTIME)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6IfPrefixAdvStatus
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                testValFsipv6IfPrefixAdvStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6IfPrefixAdvStatus (UINT4 *pu4ErrorCode, INT4 i4Fsipv6IfIndex,
                                  INT4 i4TestValFsipv6IfPrefixAdvStatus)
{
    if (Lip6UtlIfEntryExists ((UINT4) i4Fsipv6IfIndex) == OSIX_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (i4TestValFsipv6IfPrefixAdvStatus != IP6_IF_PREFIX_ADV_ENABLED &&
        i4TestValFsipv6IfPrefixAdvStatus != IP6_IF_PREFIX_ADV_DISABLED)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6IfMinRouterAdvTime
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                testValFsipv6IfMinRouterAdvTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6IfMinRouterAdvTime (UINT4 *pu4ErrorCode, INT4 i4Fsipv6IfIndex,
                                   INT4 i4TestValFsipv6IfMinRouterAdvTime)
{
    tLip6If            *pIf6Entry = NULL;

    if (CheckIp6IsFwdEnabled (i4Fsipv6IfIndex) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfIndex);
    if (pIf6Entry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
        return SNMP_FAILURE;
    }

    if (i4TestValFsipv6IfMinRouterAdvTime < IP6_IF_MIN_RA_MIN_TIME)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /* MinRouterAdvTime should not be > 3/4 of MaxRouterAdvTime */
    if (i4TestValFsipv6IfMinRouterAdvTime >
        (INT4) (LIP6_THREE_FOURTH (pIf6Entry->u4MaxRaTime)))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_INVALID_MIN_RA_INTERVAL);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6IfMaxRouterAdvTime
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                testValFsipv6IfMaxRouterAdvTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6IfMaxRouterAdvTime (UINT4 *pu4ErrorCode, INT4 i4Fsipv6IfIndex,
                                   INT4 i4TestValFsipv6IfMaxRouterAdvTime)
{
    tLip6If            *pIf6Entry = NULL;

    if (CheckIp6IsFwdEnabled (i4Fsipv6IfIndex) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfIndex);
    if (pIf6Entry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
        return SNMP_FAILURE;
    }

    if (i4TestValFsipv6IfMaxRouterAdvTime < IP6_IF_MAX_RA_MIN_TIME ||
        i4TestValFsipv6IfMaxRouterAdvTime > IP6_IF_MAX_RA_MAX_TIME)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_INVALID_RA_INTERVAL);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6IfDADRetries
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                testValFsipv6IfDADRetries
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6IfDADRetries (UINT4 *pu4ErrorCode, INT4 i4Fsipv6IfIndex,
                             INT4 i4TestValFsipv6IfDADRetries)
{

    if (CheckIp6IsFwdEnabled (i4Fsipv6IfIndex) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (Lip6UtlIfEntryExists ((UINT4) i4Fsipv6IfIndex) == OSIX_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
        return SNMP_FAILURE;
    }

    if (i4TestValFsipv6IfDADRetries >= IP6_IF_MIN_DAD_SEND &&
        i4TestValFsipv6IfDADRetries <= IP6_IF_MAX_DAD_SEND)
    {
        return (SNMP_SUCCESS);
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    CLI_SET_ERR (CLI_IP6_INVALID_DAD_RETRIES);
    return (SNMP_FAILURE);
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Fsipv6IfTable
 Input       :  The Indices
                Fsipv6IfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsipv6IfTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Fsipv6IfStatsTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsipv6IfStatsTable
 Input       :  The Indices
                Fsipv6IfStatsIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsipv6IfStatsTable (INT4 i4Fsipv6IfStatsIndex)
{
    if (Lip6UtlGetIfEntry ((UINT2) i4Fsipv6IfStatsIndex) == NULL)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsipv6IfStatsTable
 Input       :  The Indices
                Fsipv6IfStatsIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsipv6IfStatsTable (INT4 *pi4Fsipv6IfStatsIndex)
{
    *pi4Fsipv6IfStatsIndex = 0;

    if (Lip6UtlIfGetNextIndex (0, (UINT4 *) pi4Fsipv6IfStatsIndex) ==
        OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsipv6IfStatsTable
 Input       :  The Indices
                Fsipv6IfStatsIndex
                nextFsipv6IfStatsIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsipv6IfStatsTable (INT4 i4Fsipv6IfStatsIndex,
                                   INT4 *pi4NextFsipv6IfStatsIndex)
{
    INT4                i4Index = (i4Fsipv6IfStatsIndex) + 1;

    if (i4Fsipv6IfStatsIndex <= 0)
    {
        return (nmhGetFirstIndexFsipv6IfStatsTable (pi4NextFsipv6IfStatsIndex));
    }

    for (; i4Index <= (INT4) IP6_MAX_LOGICAL_IFACES; i4Index++)
    {
        if (Lip6UtlIfEntryExists ((UINT4) i4Index) == OSIX_SUCCESS)
        {
            *pi4NextFsipv6IfStatsIndex = i4Index;
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsipv6IfStatsInReceives
 Input       :  The Indices
                Fsipv6IfStatsIndex

                The Object 
                retValFsipv6IfStatsInReceives
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfStatsInReceives (INT4 i4Fsipv6IfStatsIndex,
                               UINT4 *pu4RetValFsipv6IfStatsInReceives)
{
    UINT4               u4ContextId = VCM_DEFAULT_CONTEXT;
    u4ContextId = (UINT4) UtilRtm6GetCurrCxtId ();

#ifdef NPAPI_WANTED
    if (Ipv6FsNpIpv6GetStats
        (u4ContextId, (UINT4) i4Fsipv6IfStatsIndex,
         NP_STAT_IP6_IN_RECIEVES,
         pu4RetValFsipv6IfStatsInReceives) == FNP_FAILURE)
    {
        return SNMP_FAILURE;
    }
#else
    if (Lip6KernGetIfStat ((UINT4) i4Fsipv6IfStatsIndex, IP6_IN_RECEIVES,
                           pu4RetValFsipv6IfStatsInReceives,
                           u4ContextId) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
#endif

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfStatsInHdrErrors
 Input       :  The Indices
                Fsipv6IfStatsIndex

                The Object 
                retValFsipv6IfStatsInHdrErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfStatsInHdrErrors (INT4 i4Fsipv6IfStatsIndex,
                                UINT4 *pu4RetValFsipv6IfStatsInHdrErrors)
{
    INT4                i4RetVal = 0;
    UINT4               u4ContextId = IP6_ZERO;

    u4ContextId = (UINT4) UtilRtm6GetCurrCxtId ();

    i4RetVal =
        Lip6KernGetIfStat ((UINT4) i4Fsipv6IfStatsIndex, IP6_IN_HDRERRORS,
                           pu4RetValFsipv6IfStatsInHdrErrors, u4ContextId);
    if (i4RetVal == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfStatsTooBigErrors
 Input       :  The Indices
                Fsipv6IfStatsIndex

                The Object 
                retValFsipv6IfStatsTooBigErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfStatsTooBigErrors (INT4 i4Fsipv6IfStatsIndex,
                                 UINT4 *pu4RetValFsipv6IfStatsTooBigErrors)
{
    UINT4               u4ContextId = IP6_ZERO;

    u4ContextId = (UINT4) UtilRtm6GetCurrCxtId ();
#ifdef NPAPI_WANTED
    if (Ipv6FsNpIpv6GetStats
        (u4ContextId, (UINT4) i4Fsipv6IfStatsIndex,
         NP_STAT_IP6_TOO_BIG_ERROR,
         pu4RetValFsipv6IfStatsTooBigErrors) == FNP_FAILURE)
    {
        return SNMP_FAILURE;
    }
#else
    if (Lip6KernGetIfStat ((UINT4) i4Fsipv6IfStatsIndex, IP6_IN_TOOBIGERRORS,
                           pu4RetValFsipv6IfStatsTooBigErrors,
                           u4ContextId) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
#endif

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfStatsInAddrErrors
 Input       :  The Indices
                Fsipv6IfStatsIndex

                The Object 
                retValFsipv6IfStatsInAddrErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfStatsInAddrErrors (INT4 i4Fsipv6IfStatsIndex,
                                 UINT4 *pu4RetValFsipv6IfStatsInAddrErrors)
{
    INT4                i4RetVal = 0;
    UINT4               u4ContextId = IP6_ZERO;

    u4ContextId = (UINT4) UtilRtm6GetCurrCxtId ();

    i4RetVal =
        Lip6KernGetIfStat ((UINT4) i4Fsipv6IfStatsIndex, IP6_IN_ADDRERRORS,
                           pu4RetValFsipv6IfStatsInAddrErrors, u4ContextId);
    if (i4RetVal == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfStatsForwDatagrams
 Input       :  The Indices
                Fsipv6IfStatsIndex

                The Object 
                retValFsipv6IfStatsForwDatagrams
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfStatsForwDatagrams (INT4 i4Fsipv6IfStatsIndex,
                                  UINT4 *pu4RetValFsipv6IfStatsForwDatagrams)
{
    UINT4               u4ContextId = IP6_ZERO;

    u4ContextId = (UINT4) UtilRtm6GetCurrCxtId ();
#ifdef NPAPI_WANTED
    if (Ipv6FsNpIpv6GetStats
        (u4ContextId, (UINT4) i4Fsipv6IfStatsIndex,
         NP_STAT_IP6_FORW_DATAGRAMS,
         pu4RetValFsipv6IfStatsForwDatagrams) == FNP_FAILURE)
    {
        return SNMP_FAILURE;
    }
#else
    if (Lip6KernGetIfStat ((UINT4) i4Fsipv6IfStatsIndex, IP6_OUT_FORWDATAGRAMS,
                           pu4RetValFsipv6IfStatsForwDatagrams,
                           u4ContextId) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
#endif

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfStatsInUnknownProtos
 Input       :  The Indices
                Fsipv6IfStatsIndex

                The Object 
                retValFsipv6IfStatsInUnknownProtos
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfStatsInUnknownProtos (INT4 i4Fsipv6IfStatsIndex,
                                    UINT4
                                    *pu4RetValFsipv6IfStatsInUnknownProtos)
{
    INT4                i4RetVal = 0;
    UINT4               u4ContextId = IP6_ZERO;

    u4ContextId = (UINT4) UtilRtm6GetCurrCxtId ();

    i4RetVal =
        Lip6KernGetIfStat ((UINT4) i4Fsipv6IfStatsIndex, IP6_IN_UNKNOWNPROTOS,
                           pu4RetValFsipv6IfStatsInUnknownProtos, u4ContextId);
    if (i4RetVal == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfStatsInDiscards
 Input       :  The Indices
                Fsipv6IfStatsIndex

                The Object 
                retValFsipv6IfStatsInDiscards
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfStatsInDiscards (INT4 i4Fsipv6IfStatsIndex,
                               UINT4 *pu4RetValFsipv6IfStatsInDiscards)
{
    tLip6If            *pIf6Entry = NULL;
    UINT4               u4ContextId = IP6_ZERO;

    u4ContextId = (UINT4) UtilRtm6GetCurrCxtId ();

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfStatsIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

#ifdef NPAPI_WANTED
    if (Ipv6FsNpIpv6GetStats
        (u4ContextId, (UINT4) i4Fsipv6IfStatsIndex,
         NP_STAT_IP6_IN_DISCARDS,
         pu4RetValFsipv6IfStatsInDiscards) == FNP_FAILURE)
    {
        return SNMP_FAILURE;
    }
#else
    if (Lip6KernGetIfStat ((UINT4) i4Fsipv6IfStatsIndex, IP6_IN_DISCARDS,
                           pu4RetValFsipv6IfStatsInDiscards,
                           u4ContextId) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
#endif

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfStatsInDelivers
 Input       :  The Indices
                Fsipv6IfStatsIndex

                The Object 
                retValFsipv6IfStatsInDelivers
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfStatsInDelivers (INT4 i4Fsipv6IfStatsIndex,
                               UINT4 *pu4RetValFsipv6IfStatsInDelivers)
{
    INT4                i4RetVal = 0;
    UINT4               u4ContextId = IP6_ZERO;

    u4ContextId = (UINT4) UtilRtm6GetCurrCxtId ();

    i4RetVal = Lip6KernGetIfStat ((UINT4) i4Fsipv6IfStatsIndex, IP6_IN_DELIVERS,
                                  pu4RetValFsipv6IfStatsInDelivers,
                                  u4ContextId);
    if (i4RetVal == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfStatsOutRequests
 Input       :  The Indices
                Fsipv6IfStatsIndex

                The Object 
                retValFsipv6IfStatsOutRequests
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfStatsOutRequests (INT4 i4Fsipv6IfStatsIndex,
                                UINT4 *pu4RetValFsipv6IfStatsOutRequests)
{
    INT4                i4RetVal = 0;

    UINT4               u4ContextId = IP6_ZERO;

    u4ContextId = (UINT4) UtilRtm6GetCurrCxtId ();
    i4RetVal =
        Lip6KernGetIfStat ((UINT4) i4Fsipv6IfStatsIndex, IP6_OUT_REQUESTS,
                           pu4RetValFsipv6IfStatsOutRequests, u4ContextId);
    if (i4RetVal == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfStatsOutDiscards
 Input       :  The Indices
                Fsipv6IfStatsIndex

                The Object 
                retValFsipv6IfStatsOutDiscards
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfStatsOutDiscards (INT4 i4Fsipv6IfStatsIndex,
                                UINT4 *pu4RetValFsipv6IfStatsOutDiscards)
{
    INT4                i4RetVal = 0;
    UINT4               u4ContextId = IP6_ZERO;

    u4ContextId = (UINT4) UtilRtm6GetCurrCxtId ();

    i4RetVal =
        Lip6KernGetIfStat ((UINT4) i4Fsipv6IfStatsIndex, IP6_OUT_DISCARDS,
                           pu4RetValFsipv6IfStatsOutDiscards, u4ContextId);
    if (i4RetVal == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfStatsOutNoRoutes
 Input       :  The Indices
                Fsipv6IfStatsIndex

                The Object 
                retValFsipv6IfStatsOutNoRoutes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfStatsOutNoRoutes (INT4 i4Fsipv6IfStatsIndex,
                                UINT4 *pu4RetValFsipv6IfStatsOutNoRoutes)
{
    INT4                i4RetVal = 0;
    UINT4               u4ContextId = IP6_ZERO;

    u4ContextId = (UINT4) UtilRtm6GetCurrCxtId ();

    i4RetVal =
        Lip6KernGetIfStat ((UINT4) i4Fsipv6IfStatsIndex, IP6_OUT_NOROUTES,
                           pu4RetValFsipv6IfStatsOutNoRoutes, u4ContextId);
    if (i4RetVal == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfStatsReasmReqds
 Input       :  The Indices
                Fsipv6IfStatsIndex

                The Object 
                retValFsipv6IfStatsReasmReqds
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfStatsReasmReqds (INT4 i4Fsipv6IfStatsIndex,
                               UINT4 *pu4RetValFsipv6IfStatsReasmReqds)
{
    INT4                i4RetVal = 0;
    UINT4               u4ContextId = IP6_ZERO;

    u4ContextId = (UINT4) UtilRtm6GetCurrCxtId ();

    i4RetVal = Lip6KernGetIfStat ((UINT4) i4Fsipv6IfStatsIndex, IP6_REASM_REQDS,
                                  pu4RetValFsipv6IfStatsReasmReqds,
                                  u4ContextId);
    if (i4RetVal == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfStatsReasmOKs
 Input       :  The Indices
                Fsipv6IfStatsIndex

                The Object 
                retValFsipv6IfStatsReasmOKs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfStatsReasmOKs (INT4 i4Fsipv6IfStatsIndex,
                             UINT4 *pu4RetValFsipv6IfStatsReasmOKs)
{
    INT4                i4RetVal = 0;
    UINT4               u4ContextId = IP6_ZERO;

    u4ContextId = (UINT4) UtilRtm6GetCurrCxtId ();

    i4RetVal = Lip6KernGetIfStat ((UINT4) i4Fsipv6IfStatsIndex, IP6_REASM_OKS,
                                  pu4RetValFsipv6IfStatsReasmOKs, u4ContextId);
    if (i4RetVal == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfStatsReasmFails
 Input       :  The Indices
                Fsipv6IfStatsIndex

                The Object 
                retValFsipv6IfStatsReasmFails
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfStatsReasmFails (INT4 i4Fsipv6IfStatsIndex,
                               UINT4 *pu4RetValFsipv6IfStatsReasmFails)
{
    INT4                i4RetVal = 0;
    UINT4               u4ContextId = IP6_ZERO;

    u4ContextId = (UINT4) UtilRtm6GetCurrCxtId ();

    i4RetVal = Lip6KernGetIfStat ((UINT4) i4Fsipv6IfStatsIndex, IP6_REASM_FAILS,
                                  pu4RetValFsipv6IfStatsReasmFails,
                                  u4ContextId);
    if (i4RetVal == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfStatsFragOKs
 Input       :  The Indices
                Fsipv6IfStatsIndex

                The Object 
                retValFsipv6IfStatsFragOKs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfStatsFragOKs (INT4 i4Fsipv6IfStatsIndex,
                            UINT4 *pu4RetValFsipv6IfStatsFragOKs)
{
    INT4                i4RetVal = 0;
    UINT4               u4ContextId = IP6_ZERO;

    u4ContextId = (UINT4) UtilRtm6GetCurrCxtId ();

    i4RetVal = Lip6KernGetIfStat ((UINT4) i4Fsipv6IfStatsIndex, IP6_FRAG_OKS,
                                  pu4RetValFsipv6IfStatsFragOKs, u4ContextId);
    if (i4RetVal == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfStatsFragFails
 Input       :  The Indices
                Fsipv6IfStatsIndex

                The Object 
                retValFsipv6IfStatsFragFails
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfStatsFragFails (INT4 i4Fsipv6IfStatsIndex,
                              UINT4 *pu4RetValFsipv6IfStatsFragFails)
{
    INT4                i4RetVal = 0;
    UINT4               u4ContextId = IP6_ZERO;

    u4ContextId = (UINT4) UtilRtm6GetCurrCxtId ();

    i4RetVal = Lip6KernGetIfStat ((UINT4) i4Fsipv6IfStatsIndex, IP6_FRAG_FAILS,
                                  pu4RetValFsipv6IfStatsFragFails, u4ContextId);
    if (i4RetVal == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfStatsFragCreates
 Input       :  The Indices
                Fsipv6IfStatsIndex

                The Object 
                retValFsipv6IfStatsFragCreates
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfStatsFragCreates (INT4 i4Fsipv6IfStatsIndex,
                                UINT4 *pu4RetValFsipv6IfStatsFragCreates)
{
    INT4                i4RetVal = 0;
    UINT4               u4ContextId = IP6_ZERO;

    u4ContextId = (UINT4) UtilRtm6GetCurrCxtId ();

    i4RetVal =
        Lip6KernGetIfStat ((UINT4) i4Fsipv6IfStatsIndex, IP6_FRAG_CREATES,
                           pu4RetValFsipv6IfStatsFragCreates, u4ContextId);
    if (i4RetVal == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfStatsInMcastPkts
 Input       :  The Indices
                Fsipv6IfStatsIndex

                The Object 
                retValFsipv6IfStatsInMcastPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfStatsInMcastPkts (INT4 i4Fsipv6IfStatsIndex,
                                UINT4 *pu4RetValFsipv6IfStatsInMcastPkts)
{
    UINT4               u4ContextId = IP6_ZERO;

    u4ContextId = (UINT4) UtilRtm6GetCurrCxtId ();
#ifdef NPAPI_WANTED
    if (Ipv6FsNpIpv6GetStats
        (u4ContextId, (UINT4) i4Fsipv6IfStatsIndex,
         NP_STAT_IP6_IN_MCAST_RECIEVES,
         pu4RetValFsipv6IfStatsInMcastPkts) == FNP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
#else
    INT4                i4RetVal = 0;

    i4RetVal =
        Lip6KernGetIfStat ((UINT4) i4Fsipv6IfStatsIndex, IP6_IN_MCASTPKTS,
                           pu4RetValFsipv6IfStatsInMcastPkts, u4ContextId);
    if (i4RetVal == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
#endif
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfStatsOutMcastPkts
 Input       :  The Indices
                Fsipv6IfStatsIndex

                The Object 
                retValFsipv6IfStatsOutMcastPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfStatsOutMcastPkts (INT4 i4Fsipv6IfStatsIndex,
                                 UINT4 *pu4RetValFsipv6IfStatsOutMcastPkts)
{
    UINT4               u4ContextId = IP6_ZERO;

    u4ContextId = (UINT4) UtilRtm6GetCurrCxtId ();

#ifdef NPAPI_WANTED
    if (Ipv6FsNpIpv6GetStats
        (u4ContextId, (UINT4) i4Fsipv6IfStatsIndex,
         NP_STAT_IP6_OUT_MCAST_RECIEVES,
         pu4RetValFsipv6IfStatsOutMcastPkts) == FNP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
#else
    INT4                i4RetVal = 0;

    i4RetVal =
        Lip6KernGetIfStat ((UINT4) i4Fsipv6IfStatsIndex, IP6_OUT_MCASTPKTS,
                           pu4RetValFsipv6IfStatsOutMcastPkts, u4ContextId);
    if (i4RetVal == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
#endif
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfStatsInTruncatedPkts
 Input       :  The Indices
                Fsipv6IfStatsIndex

                The Object 
                retValFsipv6IfStatsInTruncatedPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfStatsInTruncatedPkts (INT4 i4Fsipv6IfStatsIndex,
                                    UINT4
                                    *pu4RetValFsipv6IfStatsInTruncatedPkts)
{
    INT4                i4RetVal = 0;
    UINT4               u4ContextId = IP6_ZERO;

    u4ContextId = (UINT4) UtilRtm6GetCurrCxtId ();

    i4RetVal =
        Lip6KernGetIfStat ((UINT4) i4Fsipv6IfStatsIndex, IP6_IN_TRUNCATEDPKTS,
                           pu4RetValFsipv6IfStatsInTruncatedPkts, u4ContextId);
    if (i4RetVal == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfStatsInRouterSols
 Input       :  The Indices
                Fsipv6IfStatsIndex

                The Object 
                retValFsipv6IfStatsInRouterSols
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfStatsInRouterSols (INT4 i4Fsipv6IfStatsIndex,
                                 UINT4 *pu4RetValFsipv6IfStatsInRouterSols)
{
    INT4                i4RetVal = 0;
    UINT4               u4ContextId = IP6_ZERO;

    u4ContextId = (UINT4) UtilRtm6GetCurrCxtId ();

    i4RetVal = Lip6KernGetIfStat ((UINT4) i4Fsipv6IfStatsIndex,
                                  ICMP6_IN_ROUTERSOLICITS,
                                  pu4RetValFsipv6IfStatsInRouterSols,
                                  u4ContextId);
    if (i4RetVal == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfStatsInRouterAdvs
 Input       :  The Indices
                Fsipv6IfStatsIndex

                The Object 
                retValFsipv6IfStatsInRouterAdvs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfStatsInRouterAdvs (INT4 i4Fsipv6IfStatsIndex,
                                 UINT4 *pu4RetValFsipv6IfStatsInRouterAdvs)
{
    INT4                i4RetVal = 0;
    UINT4               u4ContextId = IP6_ZERO;

    u4ContextId = (UINT4) UtilRtm6GetCurrCxtId ();

    i4RetVal = Lip6KernGetIfStat ((UINT4) i4Fsipv6IfStatsIndex,
                                  ICMP6_IN_ROUTERADVERTISEMENTS,
                                  pu4RetValFsipv6IfStatsInRouterAdvs,
                                  u4ContextId);
    if (i4RetVal == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfStatsInNeighSols
 Input       :  The Indices
                Fsipv6IfStatsIndex

                The Object 
                retValFsipv6IfStatsInNeighSols
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfStatsInNeighSols (INT4 i4Fsipv6IfStatsIndex,
                                UINT4 *pu4RetValFsipv6IfStatsInNeighSols)
{
    INT4                i4RetVal = 0;
    UINT4               u4ContextId = IP6_ZERO;

    u4ContextId = (UINT4) UtilRtm6GetCurrCxtId ();

    i4RetVal = Lip6KernGetIfStat ((UINT4) i4Fsipv6IfStatsIndex,
                                  ICMP6_IN_NEIGHBORSOLICITS,
                                  pu4RetValFsipv6IfStatsInNeighSols,
                                  u4ContextId);
    if (i4RetVal == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfStatsInNeighAdvs
 Input       :  The Indices
                Fsipv6IfStatsIndex

                The Object 
                retValFsipv6IfStatsInNeighAdvs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfStatsInNeighAdvs (INT4 i4Fsipv6IfStatsIndex,
                                UINT4 *pu4RetValFsipv6IfStatsInNeighAdvs)
{
    INT4                i4RetVal = 0;
    UINT4               u4ContextId = IP6_ZERO;

    u4ContextId = (UINT4) UtilRtm6GetCurrCxtId ();

    i4RetVal = Lip6KernGetIfStat ((UINT4) i4Fsipv6IfStatsIndex,
                                  ICMP6_IN_NEIGHBORADVERTISEMENTS,
                                  pu4RetValFsipv6IfStatsInNeighAdvs,
                                  u4ContextId);
    if (i4RetVal == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfStatsInRedirects
 Input       :  The Indices
                Fsipv6IfStatsIndex

                The Object 
                retValFsipv6IfStatsInRedirects
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfStatsInRedirects (INT4 i4Fsipv6IfStatsIndex,
                                UINT4 *pu4RetValFsipv6IfStatsInRedirects)
{
    INT4                i4RetVal = 0;
    UINT4               u4ContextId = IP6_ZERO;

    u4ContextId = (UINT4) UtilRtm6GetCurrCxtId ();

    i4RetVal =
        Lip6KernGetIfStat ((UINT4) i4Fsipv6IfStatsIndex, ICMP6_IN_REDIRECTS,
                           pu4RetValFsipv6IfStatsInRedirects, u4ContextId);
    if (i4RetVal == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfStatsOutRouterSols
 Input       :  The Indices
                Fsipv6IfStatsIndex

                The Object 
                retValFsipv6IfStatsOutRouterSols
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfStatsOutRouterSols (INT4 i4Fsipv6IfStatsIndex,
                                  UINT4 *pu4RetValFsipv6IfStatsOutRouterSols)
{
    INT4                i4RetVal = 0;
    UINT4               u4ContextId = IP6_ZERO;

    u4ContextId = (UINT4) UtilRtm6GetCurrCxtId ();

    i4RetVal = Lip6KernGetIfStat ((UINT4) i4Fsipv6IfStatsIndex,
                                  ICMP6_OUT_ROUTERSOLICITS,
                                  pu4RetValFsipv6IfStatsOutRouterSols,
                                  u4ContextId);
    if (i4RetVal == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfStatsOutRouterAdvs
 Input       :  The Indices
                Fsipv6IfStatsIndex

                The Object 
                retValFsipv6IfStatsOutRouterAdvs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfStatsOutRouterAdvs (INT4 i4Fsipv6IfStatsIndex,
                                  UINT4 *pu4RetValFsipv6IfStatsOutRouterAdvs)
{
    INT4                i4RetVal = 0;
    UINT4               u4ContextId = IP6_ZERO;

    u4ContextId = (UINT4) UtilRtm6GetCurrCxtId ();

    i4RetVal = Lip6KernGetIfStat ((UINT4) i4Fsipv6IfStatsIndex,
                                  ICMP6_OUT_ROUTERADVERTISEMENTS,
                                  pu4RetValFsipv6IfStatsOutRouterAdvs,
                                  u4ContextId);
    if (i4RetVal == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfStatsOutNeighSols
 Input       :  The Indices
                Fsipv6IfStatsIndex

                The Object 
                retValFsipv6IfStatsOutNeighSols
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfStatsOutNeighSols (INT4 i4Fsipv6IfStatsIndex,
                                 UINT4 *pu4RetValFsipv6IfStatsOutNeighSols)
{
    INT4                i4RetVal = 0;
    UINT4               u4ContextId = IP6_ZERO;

    u4ContextId = (UINT4) UtilRtm6GetCurrCxtId ();

    i4RetVal = Lip6KernGetIfStat ((UINT4) i4Fsipv6IfStatsIndex,
                                  ICMP6_OUT_NEIGHBORSOLICITS,
                                  pu4RetValFsipv6IfStatsOutNeighSols,
                                  u4ContextId);
    if (i4RetVal == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfStatsOutNeighAdvs
 Input       :  The Indices
                Fsipv6IfStatsIndex

                The Object 
                retValFsipv6IfStatsOutNeighAdvs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfStatsOutNeighAdvs (INT4 i4Fsipv6IfStatsIndex,
                                 UINT4 *pu4RetValFsipv6IfStatsOutNeighAdvs)
{
    INT4                i4RetVal = 0;
    UINT4               u4ContextId = IP6_ZERO;

    u4ContextId = (UINT4) UtilRtm6GetCurrCxtId ();

    i4RetVal = Lip6KernGetIfStat ((UINT4) i4Fsipv6IfStatsIndex,
                                  ICMP6_OUT_NEIGHBORADVERTISEMENTS,
                                  pu4RetValFsipv6IfStatsOutNeighAdvs,
                                  u4ContextId);
    if (i4RetVal == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfStatsOutRedirects
 Input       :  The Indices
                Fsipv6IfStatsIndex

                The Object 
                retValFsipv6IfStatsOutRedirects
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfStatsOutRedirects (INT4 i4Fsipv6IfStatsIndex,
                                 UINT4 *pu4RetValFsipv6IfStatsOutRedirects)
{
    INT4                i4RetVal = 0;
    UINT4               u4ContextId = IP6_ZERO;

    u4ContextId = (UINT4) UtilRtm6GetCurrCxtId ();

    i4RetVal = Lip6KernGetIfStat ((UINT4) i4Fsipv6IfStatsIndex,
                                  ICMP6_OUT_REDIRECTS,
                                  pu4RetValFsipv6IfStatsOutRedirects,
                                  u4ContextId);
    if (i4RetVal == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfStatsLastRouterAdvTime
 Input       :  The Indices
                Fsipv6IfStatsIndex

                The Object 
                retValFsipv6IfStatsLastRouterAdvTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfStatsLastRouterAdvTime (INT4 i4Fsipv6IfStatsIndex,
                                      UINT4
                                      *pu4RetValFsipv6IfStatsLastRouterAdvTime)
{
    UNUSED_PARAM (i4Fsipv6IfStatsIndex);
    *pu4RetValFsipv6IfStatsLastRouterAdvTime = 0;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfStatsNextRouterAdvTime
 Input       :  The Indices
                Fsipv6IfStatsIndex

                The Object 
                retValFsipv6IfStatsNextRouterAdvTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfStatsNextRouterAdvTime (INT4 i4Fsipv6IfStatsIndex,
                                      UINT4
                                      *pu4RetValFsipv6IfStatsNextRouterAdvTime)
{
    UNUSED_PARAM (i4Fsipv6IfStatsIndex);
    *pu4RetValFsipv6IfStatsNextRouterAdvTime = 0;
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Fsipv6PrefixTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsipv6PrefixTable
 Input       :  The Indices
                Fsipv6PrefixIndex
                Fsipv6PrefixAddress
                Fsipv6PrefixAddrLen
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */
INT1
nmhValidateIndexInstanceFsipv6PrefixTable (INT4 i4Fsipv6PrefixIndex,
                                           tSNMP_OCTET_STRING_TYPE *
                                           pFsipv6PrefixAddress,
                                           INT4 i4Fsipv6PrefixAddrLen)
{
    INT1                i1AddrType = 0;

    /* Interface Index Validation */
    if (Lip6UtlIfEntryExists ((UINT4) i4Fsipv6PrefixIndex) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    /* Prefix Validation */
    i1AddrType =
        Ip6AddrType ((tIp6Addr *) (VOID *) pFsipv6PrefixAddress->pu1_OctetList);
    if (i1AddrType != ADDR6_UNICAST)
    {
        return SNMP_FAILURE;
    }

    /* Prefix Length Validation. */
    if ((i4Fsipv6PrefixAddrLen <= 0) ||
        (i4Fsipv6PrefixAddrLen > IP6_ADDR_MAX_PREFIX))
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsipv6PrefixTable
 Input       :  The Indices
                Fsipv6PrefixIndex
                Fsipv6PrefixAddress
                Fsipv6PrefixAddrLen
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */
INT1
nmhGetFirstIndexFsipv6PrefixTable (INT4 *pi4Fsipv6PrefixIndex,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsipv6PrefixAddress,
                                   INT4 *pi4Fsipv6PrefixAddrLen)
{
    tLip6PrefixNode    *pPrefix = NULL;
    UINT4               u4Index = 0;

    for (u4Index = 1; u4Index <= IP6_MAX_LOGICAL_IFACES; u4Index++)
    {
        pPrefix = Lip6PrefixGetFirst (u4Index);
        if (pPrefix != NULL)
        {
            *pi4Fsipv6PrefixIndex = (INT4) u4Index;
            MEMCPY (pFsipv6PrefixAddress->pu1_OctetList, &pPrefix->Ip6Prefix,
                    IP6_ADDR_SIZE);
            pFsipv6PrefixAddress->i4_Length = IP6_ADDR_SIZE;
            *pi4Fsipv6PrefixAddrLen = pPrefix->i4PrefixLen;
            return SNMP_SUCCESS;
        }
        continue;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsipv6PrefixTable
 Input       :  The Indices
                Fsipv6PrefixIndex
                nextFsipv6PrefixIndex
                Fsipv6PrefixAddress
                nextFsipv6PrefixAddress
                Fsipv6PrefixAddrLen
                nextFsipv6PrefixAddrLen
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsipv6PrefixTable (INT4 i4Fsipv6PrefixIndex,
                                  INT4 *pi4NextFsipv6PrefixIndex,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pFsipv6PrefixAddress,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pNextFsipv6PrefixAddress,
                                  INT4 i4Fsipv6PrefixAddrLen,
                                  INT4 *pi4NextFsipv6PrefixAddrLen)
{
    tLip6PrefixNode    *pNextPrefix = NULL;
    UINT4               u4Index;

    if (i4Fsipv6PrefixIndex < 0)
    {
        return SNMP_FAILURE;
    }

    pNextPrefix = Lip6PrefixGetNext ((UINT4) i4Fsipv6PrefixIndex,
                                     (tIp6Addr *) (VOID *)
                                     pFsipv6PrefixAddress->pu1_OctetList,
                                     (UINT1) i4Fsipv6PrefixAddrLen);
    if (pNextPrefix != NULL)
    {
        *pi4NextFsipv6PrefixIndex = i4Fsipv6PrefixIndex;
        MEMCPY (pNextFsipv6PrefixAddress->pu1_OctetList,
                &pNextPrefix->Ip6Prefix, IP6_ADDR_SIZE);
        pNextFsipv6PrefixAddress->i4_Length = IP6_ADDR_SIZE;
        *pi4NextFsipv6PrefixAddrLen = pNextPrefix->i4PrefixLen;
        return SNMP_SUCCESS;
    }

    for (u4Index = (UINT4) i4Fsipv6PrefixIndex + 1;
         u4Index <= IP6_MAX_LOGICAL_IFACES; u4Index++)
    {
        pNextPrefix = Lip6PrefixGetFirst (u4Index);
        if (pNextPrefix != NULL)
        {
            *pi4NextFsipv6PrefixIndex = (INT4) u4Index;
            MEMCPY (pNextFsipv6PrefixAddress->pu1_OctetList,
                    &pNextPrefix->Ip6Prefix, IP6_ADDR_SIZE);
            pNextFsipv6PrefixAddress->i4_Length = IP6_ADDR_SIZE;
            *pi4NextFsipv6PrefixAddrLen = pNextPrefix->i4PrefixLen;
            return SNMP_SUCCESS;
        }
        continue;
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsipv6PrefixProfileIndex
 Input       :  The Indices
                Fsipv6PrefixIndex
                Fsipv6PrefixAddress
                Fsipv6PrefixAddrLen

                The Object 
                retValFsipv6PrefixProfileIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6PrefixProfileIndex (INT4 i4Fsipv6PrefixIndex,
                                tSNMP_OCTET_STRING_TYPE * pFsipv6PrefixAddress,
                                INT4 i4Fsipv6PrefixAddrLen,
                                INT4 *pi4RetValFsipv6PrefixProfileIndex)
{
    tLip6PrefixNode    *pPrefix = NULL;
    tIp6Addr            Fsipv6Address;
    MEMSET (&Fsipv6Address, 0, sizeof (tIp6Addr));

    if (Ip6IsIfFwdEnabled (i4Fsipv6PrefixIndex) == FALSE)
    {
        UNUSED_PARAM (pFsipv6PrefixAddress);
        UNUSED_PARAM (i4Fsipv6PrefixAddrLen);
        UNUSED_PARAM (pi4RetValFsipv6PrefixProfileIndex);
        return SNMP_FAILURE;
    }

    pPrefix = Lip6PrefixGetEntry ((UINT4) i4Fsipv6PrefixIndex,
                                  (tIp6Addr *) (VOID *) pFsipv6PrefixAddress->
                                  pu1_OctetList, (UINT1) i4Fsipv6PrefixAddrLen);

    if (pPrefix != NULL)
    {
        *pi4RetValFsipv6PrefixProfileIndex = pPrefix->u2ProfileIndex;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsipv6PrefixAdminStatus
 Input       :  The Indices
                Fsipv6PrefixIndex
                Fsipv6PrefixAddress
                Fsipv6PrefixAddrLen

                The Object 
                retValFsipv6PrefixAdminStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6PrefixAdminStatus (INT4 i4Fsipv6PrefixIndex,
                               tSNMP_OCTET_STRING_TYPE * pFsipv6PrefixAddress,
                               INT4 i4Fsipv6PrefixAddrLen,
                               INT4 *pi4RetValFsipv6PrefixAdminStatus)
{
    tLip6PrefixNode    *pPrefix = NULL;

    pPrefix = Lip6PrefixGetEntry ((UINT4) i4Fsipv6PrefixIndex,
                                  (tIp6Addr *) (VOID *) pFsipv6PrefixAddress->
                                  pu1_OctetList, (UINT1) i4Fsipv6PrefixAddrLen);
    if (pPrefix != NULL)
    {
        *pi4RetValFsipv6PrefixAdminStatus = pPrefix->u1AdminStatus;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsipv6PrefixProfileIndex
 Input       :  The Indices
                Fsipv6PrefixIndex
                Fsipv6PrefixAddress
                Fsipv6PrefixAddrLen

                The Object 
                setValFsipv6PrefixProfileIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6PrefixProfileIndex (INT4 i4Fsipv6PrefixIndex,
                                tSNMP_OCTET_STRING_TYPE * pFsipv6PrefixAddress,
                                INT4 i4Fsipv6PrefixAddrLen,
                                INT4 i4SetValFsipv6PrefixProfileIndex)
{
    tLip6PrefixNode    *pPrefix = NULL;
    UINT4               u4ContextId = 0;

    if (Ip6IsIfFwdEnabled (i4Fsipv6PrefixIndex) == FALSE)
    {
        UNUSED_PARAM (pFsipv6PrefixAddress);
        UNUSED_PARAM (i4Fsipv6PrefixAddrLen);
        UNUSED_PARAM (i4SetValFsipv6PrefixProfileIndex);
        return SNMP_FAILURE;
    }

    Ip6GetCxtId ((UINT4) i4Fsipv6PrefixIndex, &u4ContextId);

    pPrefix = Lip6PrefixGetEntry ((UINT4) i4Fsipv6PrefixIndex,
                                  (tIp6Addr *) (VOID *) pFsipv6PrefixAddress->
                                  pu1_OctetList, (UINT1) i4Fsipv6PrefixAddrLen);
    if (pPrefix != NULL)
    {
        pPrefix->u2ProfileIndex = (UINT2) i4SetValFsipv6PrefixProfileIndex;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsipv6PrefixAdminStatus
 Input       :  The Indices
                Fsipv6PrefixIndex
                Fsipv6PrefixAddress
                Fsipv6PrefixAddrLen

                The Object 
                setValFsipv6PrefixAdminStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6PrefixAdminStatus (INT4 i4Fsipv6PrefixIndex,
                               tSNMP_OCTET_STRING_TYPE * pFsipv6PrefixAddress,
                               INT4 i4Fsipv6PrefixAddrLen,
                               INT4 i4SetValFsipv6PrefixAdminStatus)
{
    tLip6PrefixNode    *pPrefix = NULL;
    tLip6If            *pIf6Entry = NULL;

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6PrefixIndex);

    switch (i4SetValFsipv6PrefixAdminStatus)
    {

        case IP6FWD_CREATE_AND_WAIT:
            pPrefix = Lip6PrefixCreate ((UINT4) i4Fsipv6PrefixIndex,
                                        (tIp6Addr *) (VOID *)
                                        pFsipv6PrefixAddress->pu1_OctetList,
                                        (UINT1) i4Fsipv6PrefixAddrLen,
                                        IP6FWD_NOT_READY);
            if (pPrefix == NULL)
            {
                return SNMP_FAILURE;
            }
            break;

        case IP6FWD_ACTIVE:
            pPrefix = Lip6PrefixGetEntry ((UINT4) i4Fsipv6PrefixIndex,
                                          (tIp6Addr *) (VOID *)
                                          pFsipv6PrefixAddress->pu1_OctetList,
                                          (UINT1) i4Fsipv6PrefixAddrLen);
            if (pPrefix == NULL)
            {
                return SNMP_FAILURE;
            }

            if (pPrefix->u1AdminStatus == IP6FWD_ACTIVE)
            {
                /* Already Active */
                break;
            }

            pPrefix->u1AdminStatus = IP6FWD_ACTIVE;
            /* Enable RA for the prefix */
            Lip6RAConfig (pIf6Entry->u4ContextId);
            break;

        case IP6FWD_DESTROY:
            pPrefix = Lip6PrefixGetEntry ((UINT4) i4Fsipv6PrefixIndex,
                                          (tIp6Addr *) (VOID *)
                                          pFsipv6PrefixAddress->pu1_OctetList,
                                          (UINT1) i4Fsipv6PrefixAddrLen);
            if (pPrefix == NULL)
            {
                return SNMP_FAILURE;
            }

            /* Delete the Prefix from interface */
            Lip6PrefixDelete ((UINT4) i4Fsipv6PrefixIndex,
                              (tIp6Addr *) (VOID *) pFsipv6PrefixAddress->
                              pu1_OctetList, (UINT1) i4Fsipv6PrefixAddrLen);
            /* Disable RA on the prefix */
            Lip6RAConfig (pIf6Entry->u4ContextId);
            break;

        default:
            return SNMP_FAILURE;
    }

    IncMsrForIp6PrefixTable (i4Fsipv6PrefixIndex, i4Fsipv6PrefixAddrLen,
                             pFsipv6PrefixAddress,
                             i4SetValFsipv6PrefixAdminStatus,
                             Fsipv6PrefixAdminStatus,
                             (sizeof (Fsipv6PrefixAdminStatus) /
                              sizeof (UINT4)), TRUE);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Fsipv6PrefixProfileIndex
 Input       :  The Indices
                Fsipv6PrefixIndex
                Fsipv6PrefixAddress
                Fsipv6PrefixAddrLen

                The Object 
                testValFsipv6PrefixProfileIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6PrefixProfileIndex (UINT4 *pu4ErrorCode,
                                   INT4 i4Fsipv6PrefixIndex,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsipv6PrefixAddress,
                                   INT4 i4Fsipv6PrefixAddrLen,
                                   INT4 i4TestValFsipv6PrefixProfileIndex)
{
    INT1                i1AddrType;

    if (CheckIp6IsFwdEnabled (i4Fsipv6PrefixIndex) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (Ip6IsIfFwdEnabled (i4Fsipv6PrefixIndex) == FALSE)
    {
        UNUSED_PARAM (pFsipv6PrefixAddress);
        UNUSED_PARAM (i4Fsipv6PrefixAddrLen);
        UNUSED_PARAM (i4TestValFsipv6PrefixProfileIndex);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
        return SNMP_FAILURE;
    }

    /* Prefix Validation */
    i1AddrType = Ip6AddrType ((tIp6Addr *) (VOID *) pFsipv6PrefixAddress->
                              pu1_OctetList);
    if (i1AddrType != ADDR6_UNICAST)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_ADDR_TYPE);
        return SNMP_FAILURE;
    }

    /* Prefix Len Validation */
    if ((i4Fsipv6PrefixAddrLen <= IP6_ADDR_MIN_PREFIX) ||
        (i4Fsipv6PrefixAddrLen > IP6_ADDR_MAX_PREFIX))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_PREFIXLEN);
        return SNMP_FAILURE;
    }

    if ((i4TestValFsipv6PrefixProfileIndex < IP6_MIN_PROFILE_INDEX) ||
        (i4TestValFsipv6PrefixProfileIndex >= IP6_MAX_PROFILE_INDEX))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_INVALID_PROF_INDEX);
        return SNMP_FAILURE;
    }

    /* Ensure that the Profile is already existing */
    if (Ip6addrProfEntryExists ((UINT4) i4TestValFsipv6PrefixProfileIndex) ==
        IP6_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_IP6_PROFILE_NOT_FOUND);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6PrefixAdminStatus
 Input       :  The Indices
                Fsipv6PrefixIndex
                Fsipv6PrefixAddress
                Fsipv6PrefixAddrLen

                The Object 
                testValFsipv6PrefixAdminStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6PrefixAdminStatus (UINT4 *pu4ErrorCode,
                                  INT4 i4Fsipv6PrefixIndex,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pFsipv6PrefixAddress,
                                  INT4 i4Fsipv6PrefixAddrLen,
                                  INT4 i4TestValFsipv6PrefixAdminStatus)
{
    tLip6PrefixNode    *pPrefix = NULL;
    INT1                i1AddrType = 0;

    if (CheckIp6IsFwdEnabled (i4Fsipv6PrefixIndex) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /* Index Validation */
    if (Lip6UtlIfEntryExists ((UINT4) i4Fsipv6PrefixIndex) == OSIX_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
        return SNMP_FAILURE;
    }

    /* Prefix Validation */
    i1AddrType =
        Ip6AddrType ((tIp6Addr *) (VOID *) pFsipv6PrefixAddress->pu1_OctetList);
    if (i1AddrType != ADDR6_UNICAST)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_ADDR_TYPE);
        return SNMP_FAILURE;
    }

    /* Prefix Len Validation */
    if ((i4Fsipv6PrefixAddrLen <= 0) ||
        (i4Fsipv6PrefixAddrLen > IP6_ADDR_MAX_PREFIX))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_PREFIXLEN);
        return SNMP_FAILURE;
    }

    if ((i4TestValFsipv6PrefixAdminStatus != IP6FWD_CREATE_AND_WAIT) &&
        (i4TestValFsipv6PrefixAdminStatus != IP6FWD_ACTIVE) &&
        (i4TestValFsipv6PrefixAdminStatus != IP6FWD_DESTROY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_INVALID_PREF_ADMIN_STATUS);
        return SNMP_FAILURE;
    }

    pPrefix = Lip6PrefixGetEntry ((UINT4) i4Fsipv6PrefixIndex,
                                  (tIp6Addr *) (VOID *) pFsipv6PrefixAddress->
                                  pu1_OctetList, (UINT1) i4Fsipv6PrefixAddrLen);
    if (i4TestValFsipv6PrefixAdminStatus == IP6FWD_CREATE_AND_WAIT)
    {
        if (pPrefix != NULL)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            CLI_SET_ERR (CLI_IP6_PREFIX_ALREADY_EXISTS);
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (pPrefix == NULL)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            CLI_SET_ERR (CLI_IP6_PREFIX_NOT_FOUND);
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Fsipv6PrefixTable
 Input       :  The Indices
                Fsipv6PrefixIndex
                Fsipv6PrefixAddress
                Fsipv6PrefixAddrLen
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsipv6PrefixTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Fsipv6AddrTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsipv6AddrTable
 Input       :  The Indices
                Fsipv6AddrIndex
                Fsipv6AddrAddress
                Fsipv6AddrPrefixLen
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsipv6AddrTable (INT4 i4Fsipv6AddrIndex,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pFsipv6AddrAddress,
                                         INT4 i4Fsipv6AddrPrefixLen)
{
    tIp6Addr            Ip6Addr;

    MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));

    if (Lip6UtlIfEntryExists ((UINT4) i4Fsipv6AddrIndex) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (pFsipv6AddrAddress == NULL)
    {
        return SNMP_FAILURE;
    }

    Ip6AddrCopy (&Ip6Addr,
                 (tIp6Addr *) (VOID *) pFsipv6AddrAddress->pu1_OctetList);
    if (IS_ADDR_MULTI (Ip6Addr) || IS_ADDR_UNSPECIFIED (Ip6Addr))
    {
        return SNMP_FAILURE;
    }

    if ((i4Fsipv6AddrPrefixLen < IP6_ADDR_MIN_PREFIX) ||
        (i4Fsipv6AddrPrefixLen > IP6_ADDR_MAX_PREFIX))
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsipv6AddrTable
 Input       :  The Indices
                Fsipv6AddrIndex
                Fsipv6AddrAddress
                Fsipv6AddrPrefixLen
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsipv6AddrTable (INT4 *pi4Fsipv6AddrIndex,
                                 tSNMP_OCTET_STRING_TYPE * pFsipv6AddrAddress,
                                 INT4 *pi4Fsipv6AddrPrefixLen)
{
    tIp6Addr            Ip6Addr;
    INT4                i4Prefix = 0;

    MEMSET (&Ip6Addr, 0, sizeof (Ip6Addr));
    *pi4Fsipv6AddrIndex = 0;
    if (Lip6AddrGetNext (0, &Ip6Addr,
                         (UINT4 *) pi4Fsipv6AddrIndex,
                         (tIp6Addr *) (VOID *) pFsipv6AddrAddress->
                         pu1_OctetList, &i4Prefix) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    pFsipv6AddrAddress->i4_Length = sizeof (tIp6Addr);
    *pi4Fsipv6AddrPrefixLen = i4Prefix;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsipv6AddrTable
 Input       :  The Indices
                Fsipv6AddrIndex
                nextFsipv6AddrIndex
                Fsipv6AddrAddress
                nextFsipv6AddrAddress
                Fsipv6AddrPrefixLen
                nextFsipv6AddrPrefixLen
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsipv6AddrTable (INT4 i4Fsipv6AddrIndex,
                                INT4 *pi4NextFsipv6AddrIndex,
                                tSNMP_OCTET_STRING_TYPE * pFsipv6AddrAddress,
                                tSNMP_OCTET_STRING_TYPE *
                                pNextFsipv6AddrAddress,
                                INT4 i4Fsipv6AddrPrefixLen,
                                INT4 *pi4NextFsipv6AddrPrefixLen)
{
    tIp6Addr            CurIp6Addr;
    tIp6Addr            NextIp6Addr;
    INT4                i4RetVal = OSIX_FAILURE;

    UNUSED_PARAM (i4Fsipv6AddrPrefixLen);

    MEMSET (&CurIp6Addr, 0, sizeof (tIp6Addr));
    MEMSET (&NextIp6Addr, 0, sizeof (tIp6Addr));

    MEMCPY (&CurIp6Addr, pFsipv6AddrAddress->pu1_OctetList, sizeof (tIp6Addr));

    i4RetVal = Lip6AddrGetNext ((UINT4) i4Fsipv6AddrIndex, &CurIp6Addr,
                                (UINT4 *) pi4NextFsipv6AddrIndex,
                                &NextIp6Addr, pi4NextFsipv6AddrPrefixLen);

    if (i4RetVal == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    MEMCPY (pNextFsipv6AddrAddress->pu1_OctetList, &NextIp6Addr,
            sizeof (tIp6Addr));
    pNextFsipv6AddrAddress->i4_Length = sizeof (tIp6Addr);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsipv6AddrAdminStatus
 Input       :  The Indices
                Fsipv6AddrIndex
                Fsipv6AddrAddress
                Fsipv6AddrPrefixLen

                The Object 
                retValFsipv6AddrAdminStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6AddrAdminStatus (INT4 i4Fsipv6AddrIndex,
                             tSNMP_OCTET_STRING_TYPE * pFsipv6AddrAddress,
                             INT4 i4Fsipv6AddrPrefixLen,
                             INT4 *pi4RetValFsipv6AddrAdminStatus)
{
    tLip6AddrNode      *pAddrInfo = NULL;
    tIp6Addr            Fsipv6Address;
    tIp6Addr            ZeroIp;

    MEMSET (&ZeroIp, 0, sizeof (tIp6Addr));
    MEMSET (&Fsipv6Address, 0, sizeof (tIp6Addr));

    Ip6AddrCopy (&Fsipv6Address,
                 (tIp6Addr *) (VOID *) pFsipv6AddrAddress->pu1_OctetList);

    pAddrInfo =
        Lip6AddrGetEntry ((UINT4) i4Fsipv6AddrIndex,
                          (tIp6Addr *) & Fsipv6Address,
                          (UINT1) i4Fsipv6AddrPrefixLen);

    if (pAddrInfo == NULL)
    {
        /* Required Address is not present. */
        return SNMP_FAILURE;
    }

    /* If address is present, return active */
    *pi4RetValFsipv6AddrAdminStatus = IP6FWD_ACTIVE;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6AddrType
 Input       :  The Indices
                Fsipv6AddrIndex
                Fsipv6AddrAddress
                Fsipv6AddrPrefixLen

                The Object 
                retValFsipv6AddrType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6AddrType (INT4 i4Fsipv6AddrIndex,
                      tSNMP_OCTET_STRING_TYPE * pFsipv6AddrAddress,
                      INT4 i4Fsipv6AddrPrefixLen, INT4 *pi4RetValFsipv6AddrType)
{
    tLip6AddrNode      *pAddrInfo = NULL;
    tIp6Addr            Fsipv6Address;

    MEMSET (&Fsipv6Address, 0, sizeof (tIp6Addr));
    Ip6AddrCopy (&Fsipv6Address,
                 (tIp6Addr *) (VOID *) pFsipv6AddrAddress->pu1_OctetList);

    pAddrInfo =
        Lip6AddrGetEntry ((UINT2) i4Fsipv6AddrIndex,
                          (tIp6Addr *) & Fsipv6Address,
                          (UINT1) i4Fsipv6AddrPrefixLen);
    if (pAddrInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pAddrInfo->u1AddrType == ADDR6_ANYCAST)
    {
        *pi4RetValFsipv6AddrType = ADDR6_ANYCAST;
        return SNMP_SUCCESS;
    }

    if (pAddrInfo->u1AddrType == ADDR6_LLOCAL)
    {
        *pi4RetValFsipv6AddrType = ADDR6_LLOCAL;
        return SNMP_SUCCESS;
    }

    *pi4RetValFsipv6AddrType = ADDR6_UNICAST;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6AddrProfIndex
 Input       :  The Indices
                Fsipv6AddrIndex
                Fsipv6AddrAddress
                Fsipv6AddrPrefixLen

                The Object 
                retValFsipv6AddrProfIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6AddrProfIndex (INT4 i4Fsipv6AddrIndex,
                           tSNMP_OCTET_STRING_TYPE * pFsipv6AddrAddress,
                           INT4 i4Fsipv6AddrPrefixLen,
                           INT4 *pi4RetValFsipv6AddrProfIndex)
{
    tLip6AddrNode      *pAddrInfo = NULL;
    tIp6Addr            Fsipv6Address;
    tLip6If            *pIf6Entry = NULL;
    tTMO_SLL           *pIp6AddrList = NULL;

    MEMSET (&Fsipv6Address, 0, sizeof (tIp6Addr));
    Ip6AddrCopy (&Fsipv6Address,
                 (tIp6Addr *) (VOID *) pFsipv6AddrAddress->pu1_OctetList);

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6AddrIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (IS_ADDR_LLOCAL (Fsipv6Address))
    {
        pIp6AddrList = &(pIf6Entry->Ip6LLAddrList);
    }
    else
    {
        pIp6AddrList = &(pIf6Entry->Ip6AddrList);
    }
/*scan the addresslist and check wheather the prefix is already having a profile index*/
    TMO_SLL_Scan (pIp6AddrList, pAddrInfo, tLip6AddrNode *)
    {
        if ((Ip6AddrMatch (&(pAddrInfo->Ip6Addr), &Fsipv6Address,
                           i4Fsipv6AddrPrefixLen) == TRUE) &&
            (i4Fsipv6AddrPrefixLen == pAddrInfo->i4PrefixLen))

        {
            *pi4RetValFsipv6AddrProfIndex = pAddrInfo->u2Addr6Profile;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsipv6AddrOperStatus
 Input       :  The Indices
                Fsipv6AddrIndex
                Fsipv6AddrAddress
                Fsipv6AddrPrefixLen

                The Object 
                retValFsipv6AddrOperStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6AddrOperStatus (INT4 i4Fsipv6AddrIndex,
                            tSNMP_OCTET_STRING_TYPE * pFsipv6AddrAddress,
                            INT4 i4Fsipv6AddrPrefixLen,
                            INT4 *pi4RetValFsipv6AddrOperStatus)
{
    tLip6AddrNode      *pAddrInfo = NULL;
    tIp6Addr            Ip6Addr;
    tIp6Addr            ZeroIp;

    MEMSET (&ZeroIp, 0, sizeof (tIp6Addr));
    MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));

    Ip6AddrCopy (&Ip6Addr,
                 (tIp6Addr *) (VOID *) pFsipv6AddrAddress->pu1_OctetList);

    pAddrInfo =
        Lip6AddrGetEntry ((UINT4) i4Fsipv6AddrIndex,
                          (tIp6Addr *) & Ip6Addr, i4Fsipv6AddrPrefixLen);

    if (pAddrInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsipv6AddrOperStatus = IP6_ADDR_OPER_COMPLETE;
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsipv6AddrAdminStatus
 Input       :  The Indices
                Fsipv6AddrIndex
                Fsipv6AddrAddress
                Fsipv6AddrPrefixLen

                The Object 
                setValFsipv6AddrAdminStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6AddrAdminStatus (INT4 i4Fsipv6AddrIndex,
                             tSNMP_OCTET_STRING_TYPE * pFsipv6AddrAddress,
                             INT4 i4Fsipv6AddrPrefixLen,
                             INT4 i4SetValFsipv6AddrAdminStatus)
{
    tLip6If            *pIf6Entry = NULL;
    tLip6AddrNode      *pAddrInfo = NULL;
    tIp6Addr            Fsipv6Address;
    tIp6Addr            ZeroIp;

    MEMSET (&ZeroIp, 0, sizeof (tIp6Addr));
    MEMSET (&Fsipv6Address, 0, sizeof (tIp6Addr));

    Ip6AddrCopy (&Fsipv6Address,
                 (tIp6Addr *) (VOID *) pFsipv6AddrAddress->pu1_OctetList);

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6AddrIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    switch (i4SetValFsipv6AddrAdminStatus)
    {
        case IP6FWD_CREATE_AND_WAIT:
            if (Lip6AddrUpdate ((UINT4) i4Fsipv6AddrIndex, &Fsipv6Address,
                                i4Fsipv6AddrPrefixLen, IP6FWD_CREATE_AND_WAIT)
                == OSIX_FAILURE)
            {
                return SNMP_FAILURE;
            }
            break;

        case IP6FWD_CREATE_AND_GO:

            if (Lip6AddrUpdate ((UINT4) i4Fsipv6AddrIndex, &Fsipv6Address,
                                i4Fsipv6AddrPrefixLen, IP6FWD_CREATE_AND_WAIT)
                == OSIX_FAILURE)
            {
                return SNMP_FAILURE;
            }
            /* Intentional fall through */
        case IP6FWD_ACTIVE:

            /* Create the new UNICAST Address and make the operation Status
             * Active */
            if (Lip6AddrUpdate ((UINT4) i4Fsipv6AddrIndex, &Fsipv6Address,
                                i4Fsipv6AddrPrefixLen, IP6FWD_ACTIVE)
                == OSIX_FAILURE)
            {
                return SNMP_FAILURE;
            }
            break;

        case IP6FWD_DESTROY:
            /* Intentional fall through */
        case IP6FWD_NOT_IN_SERVICE:
            pAddrInfo = Lip6AddrGetEntry ((UINT4) i4Fsipv6AddrIndex,
                                          &Fsipv6Address,
                                          i4Fsipv6AddrPrefixLen);
            if (pAddrInfo != NULL)
            {
                /* Update/Delete this Address */
                if (Lip6AddrUpdate ((UINT4) i4Fsipv6AddrIndex, &Fsipv6Address,
                                    i4Fsipv6AddrPrefixLen,
                                    i4SetValFsipv6AddrAdminStatus) ==
                    OSIX_FAILURE)
                {
                    return SNMP_FAILURE;
                }
            }
            break;

        default:
            return SNMP_FAILURE;
    }
    IncMsrForIpv6AddrTable (i4Fsipv6AddrIndex, pFsipv6AddrAddress,
                            i4Fsipv6AddrPrefixLen, 'i',
                            &i4SetValFsipv6AddrAdminStatus,
                            FsMIIpv6AddrAdminStatus,
                            sizeof (FsMIIpv6AddrAdminStatus) / sizeof (UINT4),
                            TRUE);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6AddrType
 Input       :  The Indices
                Fsipv6AddrIndex
                Fsipv6AddrAddress
                Fsipv6AddrPrefixLen

                The Object 
                setValFsipv6AddrType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6AddrType (INT4 i4Fsipv6AddrIndex,
                      tSNMP_OCTET_STRING_TYPE * pFsipv6AddrAddress,
                      INT4 i4Fsipv6AddrPrefixLen, INT4 i4SetValFsipv6AddrType)
{
    tLip6AddrNode      *pAddrInfo = NULL;
    tIp6Addr            Fsipv6Address;

    MEMSET (&Fsipv6Address, 0, sizeof (tIp6Addr));
    Ip6AddrCopy (&Fsipv6Address,
                 (tIp6Addr *) (VOID *) pFsipv6AddrAddress->pu1_OctetList);

    pAddrInfo =
        Lip6AddrGetEntry ((UINT4) i4Fsipv6AddrIndex, &Fsipv6Address,
                          (UINT1) i4Fsipv6AddrPrefixLen);

    if (pAddrInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pAddrInfo->u1AddrType = (UINT1) i4SetValFsipv6AddrType;

    IncMsrForIpv6AddrTable (i4Fsipv6AddrIndex, pFsipv6AddrAddress,
                            i4Fsipv6AddrPrefixLen, 'i', &i4SetValFsipv6AddrType,
                            FsMIIpv6AddrType,
                            sizeof (FsMIIpv6AddrType) / sizeof (UINT4), FALSE);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6AddrProfIndex
 Input       :  The Indices
                Fsipv6AddrIndex
                Fsipv6AddrAddress
                Fsipv6AddrPrefixLen

                The Object 
                setValFsipv6AddrProfIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6AddrProfIndex (INT4 i4Fsipv6AddrIndex,
                           tSNMP_OCTET_STRING_TYPE * pFsipv6AddrAddress,
                           INT4 i4Fsipv6AddrPrefixLen,
                           INT4 i4SetValFsipv6AddrProfIndex)
{
    tLip6AddrNode      *pAddrInfo = NULL;
    tIp6Addr            Fsipv6Address;
    tLip6If            *pIf6Entry = NULL;
    tTMO_SLL           *pIp6AddrList = NULL;

    MEMSET (&Fsipv6Address, 0, sizeof (tIp6Addr));
    Ip6AddrCopy (&Fsipv6Address,
                 (tIp6Addr *) (VOID *) pFsipv6AddrAddress->pu1_OctetList);

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6AddrIndex);
    if (pIf6Entry == NULL)
    {
        /* Interface Does not Exist */
        CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
        return SNMP_FAILURE;
    }

    if (IS_ADDR_LLOCAL (Fsipv6Address))
    {
        pIp6AddrList = &(pIf6Entry->Ip6LLAddrList);
    }
    else
    {
        pIp6AddrList = &(pIf6Entry->Ip6AddrList);
    }
/* scan the addresslist and sset the profile index for all the addresses with same prefix */
    TMO_SLL_Scan (pIp6AddrList, pAddrInfo, tLip6AddrNode *)
    {
        if ((Ip6AddrMatch (&(pAddrInfo->Ip6Addr), &Fsipv6Address,
                           i4Fsipv6AddrPrefixLen) == TRUE) &&
            (i4Fsipv6AddrPrefixLen == pAddrInfo->i4PrefixLen))

        {
            if (pAddrInfo->u2Addr6Profile !=
                (UINT2) i4SetValFsipv6AddrProfIndex)
            {
                /* Decrease the Ref Count for Current Associated Profile Entry */
                if (pAddrInfo->u2Addr6Profile <
                    gIp6GblInfo.u4MaxAddrProfileLimit)
                {
                    IP6_ADDR_PROF_REF_COUNT (pAddrInfo->u2Addr6Profile)--;
                }

                /* Assign the New Profile Index */
                pAddrInfo->u2Addr6Profile = (UINT2) i4SetValFsipv6AddrProfIndex;

                /* Increase the Ref Count for Current Associated Profile Entry */
                IP6_ADDR_PROF_REF_COUNT (i4SetValFsipv6AddrProfIndex)++;
            }

            /*  pAddrInfo->u1ProfileIndex = (UINT1) i4SetValFsipv6AddrProfIndex; */

            IncMsrForIpv6AddrTable (i4Fsipv6AddrIndex, pFsipv6AddrAddress,
                                    i4Fsipv6AddrPrefixLen, 'i',
                                    &i4SetValFsipv6AddrProfIndex,
                                    FsMIIpv6AddrProfIndex,
                                    sizeof (FsMIIpv6AddrProfIndex) /
                                    sizeof (UINT4), FALSE);
        }
    }
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Fsipv6AddrAdminStatus
 Input       :  The Indices
                Fsipv6AddrIndex
                Fsipv6AddrAddress
                Fsipv6AddrPrefixLen

                The Object 
                testValFsipv6AddrAdminStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6AddrAdminStatus (UINT4 *pu4ErrorCode, INT4 i4Fsipv6AddrIndex,
                                tSNMP_OCTET_STRING_TYPE * pFsipv6AddrAddress,
                                INT4 i4Fsipv6AddrPrefixLen,
                                INT4 i4TestValFsipv6AddrAdminStatus)
{
    tLip6AddrNode      *pAddrInfo = NULL;
    tIp6Addr            Ip6Addr;
    tLip6If            *pIf6Entry = NULL;

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6AddrIndex);

    if (pIf6Entry == NULL)
    {
        /* Interface Does not Exist */
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
        return SNMP_FAILURE;
    }

    Ip6AddrCopy (&Ip6Addr,
                 (tIp6Addr *) (VOID *) pFsipv6AddrAddress->pu1_OctetList);

    /* Validate the Address. */
    if (IS_ADDR_MULTI (Ip6Addr) || IS_ADDR_UNSPECIFIED (Ip6Addr) ||
        IS_ADDR_LOOPBACK (Ip6Addr) || IS_ADDR_V4_COMPAT (Ip6Addr))
    {
        /* Invalid Address. */
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_ADDRESS);
        return SNMP_FAILURE;
    }

    /* Validate the Address Prefix Length */
    if ((i4Fsipv6AddrPrefixLen < IP6_ADDR_MIN_PREFIX) ||
        (i4Fsipv6AddrPrefixLen > IP6_ADDR_MAX_PREFIX))
    {
        /* Invalid Address Prefix Length */
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_PREFIXLEN);
        return SNMP_FAILURE;
    }

    /* Validate the Row Status Value. */
    if ((i4TestValFsipv6AddrAdminStatus == IP6FWD_NOT_READY) ||
        (i4TestValFsipv6AddrAdminStatus == IP6FWD_NOT_IN_SERVICE) ||
        (i4TestValFsipv6AddrAdminStatus < IP6FWD_ACTIVE) ||
        (i4TestValFsipv6AddrAdminStatus > IP6FWD_DESTROY))
    {
        /* Invalid Row Status Value. */
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_INVALID_ADMIN_STATUS);
        return SNMP_FAILURE;
    }

    if (IS_ADDR_LLOCAL (Ip6Addr))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_ADDRESS);
        return SNMP_FAILURE;
    }
    else
    {
        if (pIf6Entry->u4UnnumAssocIPv6If != 0)
        {
            /* UNICAST Address cannot be configured if interface is unnumbered */
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            CLI_SET_ERR (CLI_IP6_UNNUM_PRESENT_FOR_ADDR);
            return SNMP_FAILURE;
        }

        if (((i4TestValFsipv6AddrAdminStatus == IP6FWD_DESTROY)
             && (Ip6UtlGetUnnumIfEntry ((UINT4) i4Fsipv6AddrIndex))) ==
            OSIX_FAILURE)
        {
            CLI_SET_ERR (CLI_IP6_DEL_UNNUMBERED_ADDR);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;

        }

    }

#ifdef TUNNEL_WANTED
    if (pIf6Entry->u1IfType == IP6_TUNNEL_INTERFACE_TYPE)
    {
        if ((i4TestValFsipv6AddrAdminStatus == IP6FWD_CREATE_AND_GO) ||
            (i4TestValFsipv6AddrAdminStatus == IP6FWD_CREATE_AND_WAIT))
        {
            /* If the address is for Auto tunnel interface check the 
             * address compatiblity with the interface's ipv4 address */
            if (Lip6PortCheckAutoTunlAddr (&Ip6Addr, (UINT4) i4Fsipv6AddrIndex)
                == OSIX_FAILURE)
            {
                CLI_SET_ERR (CLI_IP6_INCOMPATIBLE_ADDR);
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
        }
    }
#endif

    if (i4TestValFsipv6AddrAdminStatus == IP6FWD_CREATE_AND_WAIT)
    {
        /* Address exist. */
        if (Lip6AddrScanAllIf
            (&Ip6Addr, i4Fsipv6AddrPrefixLen, pIf6Entry->u4ContextId) != NULL)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            CLI_SET_ERR (CLI_IP6_ADDR_ALREADY_EXISTS);
            return SNMP_FAILURE;
        }
    }

    pAddrInfo = Lip6AddrGetEntry ((UINT4) i4Fsipv6AddrIndex,
                                  &Ip6Addr, i4Fsipv6AddrPrefixLen);

    if (pAddrInfo == NULL)
    {
        /* Address does not exist. */
        if ((i4TestValFsipv6AddrAdminStatus == IP6FWD_ACTIVE) ||
            (i4TestValFsipv6AddrAdminStatus == IP6FWD_DESTROY))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            CLI_SET_ERR (CLI_IP6_ADDR_NOT_FOUND);
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6AddrType
 Input       :  The Indices
                Fsipv6AddrIndex
                Fsipv6AddrAddress
                Fsipv6AddrPrefixLen

                The Object 
                testValFsipv6AddrType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6AddrType (UINT4 *pu4ErrorCode, INT4 i4Fsipv6AddrIndex,
                         tSNMP_OCTET_STRING_TYPE * pFsipv6AddrAddress,
                         INT4 i4Fsipv6AddrPrefixLen,
                         INT4 i4TestValFsipv6AddrType)
{
    tLip6AddrNode      *pAddrInfo = NULL;
    tIp6Addr            Ip6Addr;

    MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));
    *pu4ErrorCode = SNMP_ERR_NO_CREATION;

    if (Lip6UtlIfEntryExists ((UINT4) i4Fsipv6AddrIndex) != OSIX_SUCCESS)
    {
        CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
        return SNMP_FAILURE;
    }
    if (pFsipv6AddrAddress == NULL)
    {
        CLI_SET_ERR (CLI_IP6_INVALID_ADDRESS);
        return SNMP_FAILURE;
    }
    Ip6AddrCopy (&Ip6Addr,
                 (tIp6Addr *) (VOID *) pFsipv6AddrAddress->pu1_OctetList);
    if (IS_ADDR_MULTI (Ip6Addr) || IS_ADDR_UNSPECIFIED (Ip6Addr))
    {
        CLI_SET_ERR (CLI_IP6_INVALID_ADDRESS);
        return SNMP_FAILURE;
    }

    if ((i4Fsipv6AddrPrefixLen < IP6_ADDR_MIN_PREFIX) ||
        (i4Fsipv6AddrPrefixLen > IP6_ADDR_MAX_PREFIX))
    {
        CLI_SET_ERR (CLI_IP6_INVALID_PREFIXLEN);
        return SNMP_FAILURE;
    }
    if ((i4TestValFsipv6AddrType != ADDR6_UNICAST) &&
        (i4TestValFsipv6AddrType != ADDR6_ANYCAST) &&
        (i4TestValFsipv6AddrType != ADDR6_LLOCAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_INVALID_ADDR_TYPE);
        return SNMP_FAILURE;
    }

    if (i4TestValFsipv6AddrType == ADDR6_LLOCAL)
    {
        if (IS_ADDR_LLOCAL (Ip6Addr) == 0)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_IP6_INVALID_ADDRESS);
            return SNMP_FAILURE;
        }
    }
    else
    {
        pAddrInfo = Lip6AddrGetEntry ((UINT4) i4Fsipv6AddrIndex,
                                      (tIp6Addr *) (VOID *)
                                      pFsipv6AddrAddress->pu1_OctetList,
                                      (UINT1) i4Fsipv6AddrPrefixLen);

        if (pAddrInfo == NULL)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_IP6_ADDR_NOT_FOUND);
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6AddrProfIndex
 Input       :  The Indices
                Fsipv6AddrIndex
                Fsipv6AddrAddress
                Fsipv6AddrPrefixLen

                The Object 
                testValFsipv6AddrProfIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6AddrProfIndex (UINT4 *pu4ErrorCode, INT4 i4Fsipv6AddrIndex,
                              tSNMP_OCTET_STRING_TYPE * pFsipv6AddrAddress,
                              INT4 i4Fsipv6AddrPrefixLen,
                              INT4 i4TestValFsipv6AddrProfIndex)
{
    tIp6Addr            Ip6Addr;

    if (CheckIp6IsFwdEnabled (i4Fsipv6AddrIndex) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));

    if (Lip6UtlIfEntryExists ((UINT4) i4Fsipv6AddrIndex) != OSIX_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
        return SNMP_FAILURE;
    }
    if (pFsipv6AddrAddress == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_ADDRESS);
        return SNMP_FAILURE;
    }
    Ip6AddrCopy (&Ip6Addr,
                 (tIp6Addr *) (VOID *) pFsipv6AddrAddress->pu1_OctetList);

    if ((i4Fsipv6AddrPrefixLen < IP6_ADDR_MIN_PREFIX) ||
        (i4Fsipv6AddrPrefixLen > IP6_ADDR_MAX_PREFIX))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_INVALID_PREFIXLEN);
        return SNMP_FAILURE;
    }

    if ((i4TestValFsipv6AddrProfIndex < IP6_ADDR_MIN_PROFILES) ||
        (i4TestValFsipv6AddrProfIndex > LIP6_MAX_ADDR_PROFILE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_INVALID_PROF_INDEX);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Fsipv6AddrTable
 Input       :  The Indices
                Fsipv6AddrIndex
                Fsipv6AddrAddress
                Fsipv6AddrPrefixLen
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsipv6AddrTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Fsipv6AddrProfileTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsipv6AddrProfileTable
 Input       :  The Indices
                Fsipv6AddrProfileIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsipv6AddrProfileTable (UINT4 u4Fsipv6AddrProfileIndex)
{
    UNUSED_PARAM (u4Fsipv6AddrProfileIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsipv6AddrProfileTable
 Input       :  The Indices
                Fsipv6AddrProfileIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsipv6AddrProfileTable (UINT4 *pu4Fsipv6AddrProfileIndex)
{

    UINT4               u4Index;
    for (u4Index = 0; u4Index < LIP6_MAX_ADDR_PROFILE; u4Index++)
    {
        if (Ip6addrProfEntryExists (u4Index) == IP6_SUCCESS)
        {

            *pu4Fsipv6AddrProfileIndex = u4Index;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsipv6AddrProfileTable
 Input       :  The Indices
                Fsipv6AddrProfileIndex
                nextFsipv6AddrProfileIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsipv6AddrProfileTable (UINT4 u4Fsipv6AddrProfileIndex,
                                       UINT4 *pu4NextFsipv6AddrProfileIndex)
{

    UINT4               u4Index;

    /* if index is max value of UNIT4 ie 4294967295 , return failure */
    /*  if ((u4Fsipv6AddrProfileIndex + 1) == 0) */
    if (u4Fsipv6AddrProfileIndex == LIP6_MAX_VALUE)
    {
        return (SNMP_FAILURE);
    }

    for (u4Index = u4Fsipv6AddrProfileIndex + 1;
         u4Index < LIP6_MAX_ADDR_PROFILE; u4Index++)
    {
        if (Ip6addrProfEntryExists (u4Index) == IP6_SUCCESS)
        {
            *pu4NextFsipv6AddrProfileIndex = u4Index;
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsipv6AddrProfileStatus
 Input       :  The Indices
                Fsipv6AddrProfileIndex

                The Object 
                retValFsipv6AddrProfileStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6AddrProfileStatus (UINT4 u4Fsipv6AddrProfileIndex,
                               INT4 *pi4RetValFsipv6AddrProfileStatus)
{
    if (Ip6addrProfEntryExists (u4Fsipv6AddrProfileIndex) == IP6_SUCCESS)
    {
        *pi4RetValFsipv6AddrProfileStatus =
            (INT4) IP6_ADDR_PROF_ADMIN (u4Fsipv6AddrProfileIndex);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsipv6AddrProfilePrefixAdvStatus
 Input       :  The Indices
                Fsipv6AddrProfileIndex

                The Object 
                retValFsipv6AddrProfilePrefixAdvStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6AddrProfilePrefixAdvStatus (UINT4 u4Fsipv6AddrProfileIndex,
                                        INT4
                                        *pi4RetValFsipv6AddrProfilePrefixAdvStatus)
{
    if (Ip6addrProfEntryExists (u4Fsipv6AddrProfileIndex) == IP6_SUCCESS)
    {

        if (IP6_ADDR_SEND_PREFIX (u4Fsipv6AddrProfileIndex))
        {
            *pi4RetValFsipv6AddrProfilePrefixAdvStatus =
                (INT4) IP6_ADDR_PROF_PREF_ADV_ON;
            return SNMP_SUCCESS;
        }
        else
            *pi4RetValFsipv6AddrProfilePrefixAdvStatus =
                (INT4) IP6_ADDR_PROF_PREF_ADV_OFF;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsipv6AddrProfileOnLinkAdvStatus
 Input       :  The Indices
                Fsipv6AddrProfileIndex

                The Object 
                retValFsipv6AddrProfileOnLinkAdvStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6AddrProfileOnLinkAdvStatus (UINT4 u4Fsipv6AddrProfileIndex,
                                        INT4
                                        *pi4RetValFsipv6AddrProfileOnLinkAdvStatus)
{
    if (Ip6addrProfEntryExists (u4Fsipv6AddrProfileIndex) == IP6_SUCCESS)
    {
        if (IP6_ADDR_ON_LINK (u4Fsipv6AddrProfileIndex))
        {
            *pi4RetValFsipv6AddrProfileOnLinkAdvStatus =
                (INT4) IP6_ADDR_PROF_ONLINK_ADV_ON;
            return SNMP_SUCCESS;
        }
        *pi4RetValFsipv6AddrProfileOnLinkAdvStatus =
            (INT4) IP6_ADDR_PROF_ONLINK_ADV_OFF;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsipv6AddrProfileAutoConfAdvStatus
 Input       :  The Indices
                Fsipv6AddrProfileIndex

                The Object 
                retValFsipv6AddrProfileAutoConfAdvStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6AddrProfileAutoConfAdvStatus (UINT4 u4Fsipv6AddrProfileIndex,
                                          INT4
                                          *pi4RetValFsipv6AddrProfileAutoConfAdvStatus)
{
    if (Ip6addrProfEntryExists (u4Fsipv6AddrProfileIndex) == IP6_SUCCESS)
    {

        if (IP6_ADDR_AUTO_CONFIG (u4Fsipv6AddrProfileIndex))
        {
            *pi4RetValFsipv6AddrProfileAutoConfAdvStatus =
                (INT4) IP6_ADDR_PROF_AUTO_ADV_ON;
            return SNMP_SUCCESS;
        }
        *pi4RetValFsipv6AddrProfileAutoConfAdvStatus =
            (INT4) IP6_ADDR_PROF_AUTO_ADV_OFF;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsipv6AddrProfilePreferredTime
 Input       :  The Indices
                Fsipv6AddrProfileIndex

                The Object 
                retValFsipv6AddrProfilePreferredTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6AddrProfilePreferredTime (UINT4 u4Fsipv6AddrProfileIndex,
                                      UINT4
                                      *pu4RetValFsipv6AddrProfilePreferredTime)
{
    if (Ip6addrProfEntryExists (u4Fsipv6AddrProfileIndex) == IP6_SUCCESS)
    {
        *pu4RetValFsipv6AddrProfilePreferredTime =
            IP6_ADDR_PREF_TIME (u4Fsipv6AddrProfileIndex);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsipv6AddrProfileValidTime
 Input       :  The Indices
                Fsipv6AddrProfileIndex

                The Object 
                retValFsipv6AddrProfileValidTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6AddrProfileValidTime (UINT4 u4Fsipv6AddrProfileIndex,
                                  UINT4 *pu4RetValFsipv6AddrProfileValidTime)
{
    if (Ip6addrProfEntryExists (u4Fsipv6AddrProfileIndex) == IP6_SUCCESS)
    {
        *pu4RetValFsipv6AddrProfileValidTime =
            IP6_ADDR_VALID_TIME (u4Fsipv6AddrProfileIndex);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsipv6AddrProfileValidLifeTimeFlag
 Input       :  The Indices
                Fsipv6AddrProfileIndex

                The Object 
                retValFsipv6AddrProfileValidLifeTimeFlag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6AddrProfileValidLifeTimeFlag (UINT4 u4Fsipv6AddrProfileIndex,
                                          INT4
                                          *pi4RetValFsipv6AddrProfileValidLifeTimeFlag)
{
    if (Ip6addrProfEntryExists (u4Fsipv6AddrProfileIndex) == IP6_SUCCESS)
    {
        if (IP6_ADDR_VALID_TIME_FIXED (u4Fsipv6AddrProfileIndex))
            *pi4RetValFsipv6AddrProfileValidLifeTimeFlag = IP6_FIXED_TIME;
        else
            *pi4RetValFsipv6AddrProfileValidLifeTimeFlag = IP6_VARIABLE_TIME;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsipv6AddrProfilePreferredLifeTimeFlag
 Input       :  The Indices
                Fsipv6AddrProfileIndex

                The Object 
                retValFsipv6AddrProfilePreferredLifeTimeFlag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6AddrProfilePreferredLifeTimeFlag (UINT4 u4Fsipv6AddrProfileIndex,
                                              INT4
                                              *pi4RetValFsipv6AddrProfilePreferredLifeTimeFlag)
{
    if (Ip6addrProfEntryExists (u4Fsipv6AddrProfileIndex) == IP6_SUCCESS)
    {
        if (IP6_ADDR_PREF_TIME_FIXED (u4Fsipv6AddrProfileIndex))
            *pi4RetValFsipv6AddrProfilePreferredLifeTimeFlag = IP6_FIXED_TIME;
        else
            *pi4RetValFsipv6AddrProfilePreferredLifeTimeFlag =
                IP6_VARIABLE_TIME;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsipv6AddrProfileStatus
 Input       :  The Indices
                Fsipv6AddrProfileIndex

                The Object 
                setValFsipv6AddrProfileStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6AddrProfileStatus (UINT4 u4Fsipv6AddrProfileIndex,
                               INT4 i4SetValFsipv6AddrProfileStatus)
{
    if (Ip6addrProfEntryExists (u4Fsipv6AddrProfileIndex) == IP6_SUCCESS)
    {
        /* first entry cannot be deleted */
        if ((i4SetValFsipv6AddrProfileStatus == IP6_ADDR_PROF_INVALID)
            && (u4Fsipv6AddrProfileIndex == 0))
            return SNMP_FAILURE;
        IP6_ADDR_PROF_ADMIN (u4Fsipv6AddrProfileIndex) =
            (UINT1) i4SetValFsipv6AddrProfileStatus;
        IncMsrForIpv6AddrProfileTable (u4Fsipv6AddrProfileIndex,
                                       'i', &i4SetValFsipv6AddrProfileStatus,
                                       FsMIIpv6AddrProfileStatus,
                                       sizeof (FsMIIpv6AddrProfileStatus) /
                                       sizeof (UINT4));
        return SNMP_SUCCESS;
    }
    else
    {                            /* create a profile entry */
        if (i4SetValFsipv6AddrProfileStatus == IP6_ADDR_PROF_VALID)
        {
            IP6_ADDR_PROF_ADMIN (u4Fsipv6AddrProfileIndex) =
                (UINT1) i4SetValFsipv6AddrProfileStatus;
            IncMsrForIpv6AddrProfileTable (u4Fsipv6AddrProfileIndex,
                                           'i',
                                           &i4SetValFsipv6AddrProfileStatus,
                                           FsMIIpv6AddrProfileStatus,
                                           sizeof (FsMIIpv6AddrProfileStatus) /
                                           sizeof (UINT4));
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsipv6AddrProfilePrefixAdvStatus
 Input       :  The Indices
                Fsipv6AddrProfileIndex

                The Object 
                setValFsipv6AddrProfilePrefixAdvStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6AddrProfilePrefixAdvStatus (UINT4 u4Fsipv6AddrProfileIndex,
                                        INT4
                                        i4SetValFsipv6AddrProfilePrefixAdvStatus)
{
    if (Ip6addrProfEntryExists (u4Fsipv6AddrProfileIndex) == IP6_SUCCESS)
    {
        if (i4SetValFsipv6AddrProfilePrefixAdvStatus ==
            IP6_ADDR_PROF_PREF_ADV_ON)
        {
            IP6_ADDR_PROF_CONF (u4Fsipv6AddrProfileIndex) |=
                IP6_ADDR_PREFIX_ADV;
        }

        if (i4SetValFsipv6AddrProfilePrefixAdvStatus ==
            IP6_ADDR_PROF_PREF_ADV_OFF)
        {
            IP6_ADDR_PROF_CONF (u4Fsipv6AddrProfileIndex) &=
                ~IP6_ADDR_PREFIX_ADV;
        }
        IncMsrForIpv6AddrProfileTable (u4Fsipv6AddrProfileIndex,
                                       'i',
                                       &i4SetValFsipv6AddrProfilePrefixAdvStatus,
                                       FsMIIpv6AddrProfilePrefixAdvStatus,
                                       sizeof
                                       (FsMIIpv6AddrProfilePrefixAdvStatus) /
                                       sizeof (UINT4));

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsipv6AddrProfileOnLinkAdvStatus
 Input       :  The Indices
                Fsipv6AddrProfileIndex

                The Object 
                setValFsipv6AddrProfileOnLinkAdvStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6AddrProfileOnLinkAdvStatus (UINT4 u4Fsipv6AddrProfileIndex,
                                        INT4
                                        i4SetValFsipv6AddrProfileOnLinkAdvStatus)
{
    if (Ip6addrProfEntryExists (u4Fsipv6AddrProfileIndex) == IP6_SUCCESS)
    {
        if (i4SetValFsipv6AddrProfileOnLinkAdvStatus ==
            IP6_ADDR_PROF_ONLINK_ADV_ON)
        {
            IP6_ADDR_PROF_CONF (u4Fsipv6AddrProfileIndex) |=
                IP6_ADDR_ONLINK_ADV;
        }

        if (i4SetValFsipv6AddrProfileOnLinkAdvStatus ==
            IP6_ADDR_PROF_ONLINK_ADV_OFF)
        {
            IP6_ADDR_PROF_CONF (u4Fsipv6AddrProfileIndex) &=
                ~IP6_ADDR_ONLINK_ADV;
        }
        IncMsrForIpv6AddrProfileTable (u4Fsipv6AddrProfileIndex,
                                       'i',
                                       &i4SetValFsipv6AddrProfileOnLinkAdvStatus,
                                       FsMIIpv6AddrProfileOnLinkAdvStatus,
                                       sizeof
                                       (FsMIIpv6AddrProfileOnLinkAdvStatus) /
                                       sizeof (UINT4));

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsipv6AddrProfileAutoConfAdvStatus
 Input       :  The Indices
                Fsipv6AddrProfileIndex

                The Object 
                setValFsipv6AddrProfileAutoConfAdvStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6AddrProfileAutoConfAdvStatus (UINT4 u4Fsipv6AddrProfileIndex,
                                          INT4
                                          i4SetValFsipv6AddrProfileAutoConfAdvStatus)
{
    if (Ip6addrProfEntryExists (u4Fsipv6AddrProfileIndex) == IP6_SUCCESS)
    {
        if (i4SetValFsipv6AddrProfileAutoConfAdvStatus ==
            IP6_ADDR_PROF_AUTO_ADV_ON)
        {
            IP6_ADDR_PROF_CONF (u4Fsipv6AddrProfileIndex) |= IP6_ADDR_AUTO_ADV;
        }

        if (i4SetValFsipv6AddrProfileAutoConfAdvStatus ==
            IP6_ADDR_PROF_AUTO_ADV_OFF)
        {
            IP6_ADDR_PROF_CONF (u4Fsipv6AddrProfileIndex) &= ~IP6_ADDR_AUTO_ADV;
        }
        IncMsrForIpv6AddrProfileTable (u4Fsipv6AddrProfileIndex,
                                       'i',
                                       &i4SetValFsipv6AddrProfileAutoConfAdvStatus,
                                       FsMIIpv6AddrProfileAutoConfAdvStatus,
                                       sizeof
                                       (FsMIIpv6AddrProfileAutoConfAdvStatus) /
                                       sizeof (UINT4));
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsipv6AddrProfilePreferredTime
 Input       :  The Indices
                Fsipv6AddrProfileIndex

                The Object 
                setValFsipv6AddrProfilePreferredTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6AddrProfilePreferredTime (UINT4 u4Fsipv6AddrProfileIndex,
                                      UINT4
                                      u4SetValFsipv6AddrProfilePreferredTime)
{
    tLip6If            *pIf6Entry;
    if (Ip6addrProfEntryExists (u4Fsipv6AddrProfileIndex) == IP6_SUCCESS)
    {
        IP6_ADDR_PREF_TIME (u4Fsipv6AddrProfileIndex) =
            u4SetValFsipv6AddrProfilePreferredTime;
        pIf6Entry = Lip6UtlGetIfEntry (u4Fsipv6AddrProfileIndex);
        if (pIf6Entry != NULL)
        {

            if ((pIf6Entry->u1RAdvStatus == IP6_IF_ROUT_ADV_ENABLED) &&
                (pIf6Entry->u1OperStatus == NETIPV6_OPER_UP))
            {
                if (Lip6RAConfig (pIf6Entry->u4ContextId) == OSIX_FAILURE)
                {
                    return SNMP_FAILURE;
                }
            }

        }

        IncMsrForIpv6AddrProfileTable (u4Fsipv6AddrProfileIndex,
                                       'u',
                                       &u4SetValFsipv6AddrProfilePreferredTime,
                                       FsMIIpv6AddrProfilePreferredTime,
                                       sizeof (FsMIIpv6AddrProfilePreferredTime)
                                       / sizeof (UINT4));
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsipv6AddrProfileValidTime
 Input       :  The Indices
                Fsipv6AddrProfileIndex

                The Object 
                setValFsipv6AddrProfileValidTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6AddrProfileValidTime (UINT4 u4Fsipv6AddrProfileIndex,
                                  UINT4 u4SetValFsipv6AddrProfileValidTime)
{
    tLip6If            *pIf6Entry;
    if (Ip6addrProfEntryExists (u4Fsipv6AddrProfileIndex) == IP6_SUCCESS)
    {
        IP6_ADDR_VALID_TIME (u4Fsipv6AddrProfileIndex) =
            u4SetValFsipv6AddrProfileValidTime;

        pIf6Entry = Lip6UtlGetIfEntry (u4Fsipv6AddrProfileIndex);
        if (pIf6Entry != NULL)
        {

            if ((pIf6Entry->u1RAdvStatus == IP6_IF_ROUT_ADV_ENABLED) &&
                (pIf6Entry->u1OperStatus == NETIPV6_OPER_UP))
            {
                if (Lip6RAConfig (pIf6Entry->u4ContextId) == OSIX_FAILURE)
                {
                    return SNMP_FAILURE;
                }
            }

        }

        IncMsrForIpv6AddrProfileTable (u4Fsipv6AddrProfileIndex,
                                       'u', &u4SetValFsipv6AddrProfileValidTime,
                                       FsMIIpv6AddrProfileValidTime,
                                       sizeof (FsMIIpv6AddrProfileValidTime) /
                                       sizeof (UINT4));

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsipv6AddrProfileValidLifeTimeFlag
 Input       :  The Indices
                Fsipv6AddrProfileIndex

                The Object 
                setValFsipv6AddrProfileValidLifeTimeFlag
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6AddrProfileValidLifeTimeFlag (UINT4 u4Fsipv6AddrProfileIndex,
                                          INT4
                                          i4SetValFsipv6AddrProfileValidLifeTimeFlag)
{
    if (Ip6addrProfEntryExists (u4Fsipv6AddrProfileIndex) == IP6_SUCCESS)
    {
        if (i4SetValFsipv6AddrProfileValidLifeTimeFlag == IP6_FIXED_TIME)
        {
            IP6_ADDR_PROF_CONF (u4Fsipv6AddrProfileIndex) |=
                IP6_ADDR_VALID_TIME_FLAG;
        }
        else
        {
            IP6_ADDR_PROF_CONF (u4Fsipv6AddrProfileIndex) &=
                ~IP6_ADDR_VALID_TIME_FLAG;
        }
        IncMsrForIpv6AddrProfileTable (u4Fsipv6AddrProfileIndex,
                                       'i',
                                       &i4SetValFsipv6AddrProfileValidLifeTimeFlag,
                                       FsMIIpv6AddrProfileValidLifeTimeFlag,
                                       sizeof
                                       (FsMIIpv6AddrProfileValidLifeTimeFlag) /
                                       sizeof (UINT4));
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetFsipv6AddrProfilePreferredLifeTimeFlag
 Input       :  The Indices
                Fsipv6AddrProfileIndex

                The Object 
                setValFsipv6AddrProfilePreferredLifeTimeFlag
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6AddrProfilePreferredLifeTimeFlag (UINT4 u4Fsipv6AddrProfileIndex,
                                              INT4
                                              i4SetValFsipv6AddrProfilePreferredLifeTimeFlag)
{
    if (Ip6addrProfEntryExists (u4Fsipv6AddrProfileIndex) == IP6_SUCCESS)
    {
        if (i4SetValFsipv6AddrProfilePreferredLifeTimeFlag == IP6_FIXED_TIME)
        {
            IP6_ADDR_PROF_CONF (u4Fsipv6AddrProfileIndex) |=
                IP6_ADDR_PREF_TIME_FLAG;
        }
        else
        {
            IP6_ADDR_PROF_CONF (u4Fsipv6AddrProfileIndex) &=
                ~IP6_ADDR_PREF_TIME_FLAG;
        }
        IncMsrForIpv6AddrProfileTable (u4Fsipv6AddrProfileIndex,
                                       'i',
                                       &i4SetValFsipv6AddrProfilePreferredLifeTimeFlag,
                                       FsMIIpv6AddrProfilePreferredLifeTimeFlag,
                                       sizeof
                                       (FsMIIpv6AddrProfilePreferredLifeTimeFlag)
                                       / sizeof (UINT4));

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Fsipv6AddrProfileStatus
 Input       :  The Indices
                Fsipv6AddrProfileIndex

                The Object 
                testValFsipv6AddrProfileStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6AddrProfileStatus (UINT4 *pu4ErrorCode,
                                  UINT4 u4Fsipv6AddrProfileIndex,
                                  INT4 i4TestValFsipv6AddrProfileStatus)
{
    if (u4Fsipv6AddrProfileIndex >= MAX_IP6_ADDR_PROFILES_LIMIT)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_PROF_INDEX);
        return SNMP_FAILURE;
    }

    if ((i4TestValFsipv6AddrProfileStatus != IP6_ADDR_PROF_INVALID)
        && (i4TestValFsipv6AddrProfileStatus != IP6_ADDR_PROF_VALID))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_INVALID_PROF_STATUS);
        return SNMP_FAILURE;
    }

    /* In case of invalidating an Profile entry, ensure that the profile
     * index is not used by any address or prefix. */
    if ((i4TestValFsipv6AddrProfileStatus == IP6_ADDR_PROF_INVALID) &&
        (IP6_ADDR_PROF_REF_COUNT (u4Fsipv6AddrProfileIndex) > 0))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_IP6_PROFILE_IN_USE);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6AddrProfilePrefixAdvStatus
 Input       :  The Indices
                Fsipv6AddrProfileIndex

                The Object 
                testValFsipv6AddrProfilePrefixAdvStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6AddrProfilePrefixAdvStatus (UINT4 *pu4ErrorCode,
                                           UINT4 u4Fsipv6AddrProfileIndex,
                                           INT4
                                           i4TestValFsipv6AddrProfilePrefixAdvStatus)
{
    if (u4Fsipv6AddrProfileIndex > MAX_IP6_ADDR_PROFILES_LIMIT)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_PROF_INDEX);
        return SNMP_FAILURE;
    }

    if ((i4TestValFsipv6AddrProfilePrefixAdvStatus != IP6_ADDR_PROF_PREF_ADV_ON)
        && (i4TestValFsipv6AddrProfilePrefixAdvStatus !=
            IP6_ADDR_PROF_PREF_ADV_OFF))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_INVALID_PREFIX_ADV_STATUS);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6AddrProfileOnLinkAdvStatus
 Input       :  The Indices
                Fsipv6AddrProfileIndex

                The Object 
                testValFsipv6AddrProfileOnLinkAdvStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6AddrProfileOnLinkAdvStatus (UINT4 *pu4ErrorCode,
                                           UINT4 u4Fsipv6AddrProfileIndex,
                                           INT4
                                           i4TestValFsipv6AddrProfileOnLinkAdvStatus)
{
    if (u4Fsipv6AddrProfileIndex >= MAX_IP6_ADDR_PROFILES_LIMIT)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_PROF_INDEX);
        return SNMP_FAILURE;
    }

    if ((i4TestValFsipv6AddrProfileOnLinkAdvStatus !=
         IP6_ADDR_PROF_ONLINK_ADV_ON)
        && (i4TestValFsipv6AddrProfileOnLinkAdvStatus !=
            IP6_ADDR_PROF_ONLINK_ADV_OFF))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_PROF_ONLINK_ADV_STATUS);
        return SNMP_FAILURE;
    }
    if ((gIp6GblInfo.u1RFC5942Compatibility != IP6_RFC5942_COMPATIBLE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_PROF_ONLINK_ADV_INVALID);
        return SNMP_FAILURE;

    }
    if ((i4TestValFsipv6AddrProfileOnLinkAdvStatus ==
         IP6_ADDR_PROF_ONLINK_ADV_ON)
        && (!IP6_ADDR_PREF_TIME (u4Fsipv6AddrProfileIndex)))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_INV_PREF_LIFETIME_FLAG);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6AddrProfileAutoConfAdvStatus
 Input       :  The Indices
                Fsipv6AddrProfileIndex

                The Object 
                testValFsipv6AddrProfileAutoConfAdvStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6AddrProfileAutoConfAdvStatus (UINT4 *pu4ErrorCode,
                                             UINT4 u4Fsipv6AddrProfileIndex,
                                             INT4
                                             i4TestValFsipv6AddrProfileAutoConfAdvStatus)
{
    if (u4Fsipv6AddrProfileIndex > MAX_IP6_ADDR_PROFILES_LIMIT)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_PROF_INDEX);
        return SNMP_FAILURE;
    }

    if ((i4TestValFsipv6AddrProfileAutoConfAdvStatus !=
         IP6_ADDR_PROF_AUTO_ADV_ON)
        && (i4TestValFsipv6AddrProfileAutoConfAdvStatus !=
            IP6_ADDR_PROF_AUTO_ADV_OFF))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_AUTOCONF_ADV_STATUS);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6AddrProfilePreferredTime
 Input       :  The Indices
                Fsipv6AddrProfileIndex

                The Object 
                testValFsipv6AddrProfilePreferredTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6AddrProfilePreferredTime (UINT4 *pu4ErrorCode,
                                         UINT4 u4Fsipv6AddrProfileIndex,
                                         UINT4
                                         u4TestValFsipv6AddrProfilePreferredTime)
{
    UNUSED_PARAM (u4TestValFsipv6AddrProfilePreferredTime);
    if (u4Fsipv6AddrProfileIndex > MAX_IP6_ADDR_PROFILES_LIMIT)
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
    {
        CLI_SET_ERR (CLI_IP6_INVALID_PROF_INDEX);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6AddrProfileValidTime
 Input       :  The Indices
                Fsipv6AddrProfileIndex

                The Object 
                testValFsipv6AddrProfileValidTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6AddrProfileValidTime (UINT4 *pu4ErrorCode,
                                     UINT4 u4Fsipv6AddrProfileIndex,
                                     UINT4 u4TestValFsipv6AddrProfileValidTime)
{
    if (u4Fsipv6AddrProfileIndex > MAX_IP6_ADDR_PROFILES_LIMIT)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_PROF_INDEX);
        return SNMP_FAILURE;
    }
    UNUSED_PARAM (u4TestValFsipv6AddrProfileValidTime);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6AddrProfileValidLifeTimeFlag
 Input       :  The Indices
                Fsipv6AddrProfileIndex

                The Object 
                testValFsipv6AddrProfileValidLifeTimeFlag
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6AddrProfileValidLifeTimeFlag (UINT4 *pu4ErrorCode,
                                             UINT4 u4Fsipv6AddrProfileIndex,
                                             INT4
                                             i4TestValFsipv6AddrProfileValidLifeTimeFlag)
{
    if (u4Fsipv6AddrProfileIndex >= MAX_IP6_ADDR_PROFILES_LIMIT)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_PROF_INDEX);
        return SNMP_FAILURE;
    }
    if (i4TestValFsipv6AddrProfileValidLifeTimeFlag != IP6_FIXED_TIME &&
        i4TestValFsipv6AddrProfileValidLifeTimeFlag != IP6_VARIABLE_TIME)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_INV_VAL_LIFETIME_FLAG);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6AddrProfilePreferredLifeTimeFlag
 Input       :  The Indices
                Fsipv6AddrProfileIndex

                The Object 
                testValFsipv6AddrProfilePreferredLifeTimeFlag
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6AddrProfilePreferredLifeTimeFlag (UINT4 *pu4ErrorCode,
                                                 UINT4 u4Fsipv6AddrProfileIndex,
                                                 INT4
                                                 i4TestValFsipv6AddrProfilePreferredLifeTimeFlag)
{
    if (u4Fsipv6AddrProfileIndex >= MAX_IP6_ADDR_PROFILES_LIMIT)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_PROF_INDEX);
        return SNMP_FAILURE;
    }

    if (i4TestValFsipv6AddrProfilePreferredLifeTimeFlag != IP6_FIXED_TIME &&
        i4TestValFsipv6AddrProfilePreferredLifeTimeFlag != IP6_VARIABLE_TIME)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_INV_PREF_LIFETIME_FLAG);
        return SNMP_FAILURE;
    }

    /* PreferredLifeTimeFlag can be set to FIXED only if
     * ValidLifeTimeFlag is FIXED */
    if (i4TestValFsipv6AddrProfilePreferredLifeTimeFlag == IP6_FIXED_TIME)
    {
        if (!(IP6_ADDR_VALID_TIME_FIXED (u4Fsipv6AddrProfileIndex)))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_IP6_VALID_NOT_FIXED);
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Fsipv6AddrProfileTable
 Input       :  The Indices
                Fsipv6AddrProfileIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsipv6AddrProfileTable (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Fsipv6PmtuTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsipv6PmtuTable
 Input       :  The Indices
                Fsipv6PmtuDest
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1                nmhValidateIndexInstanceFsipv6PmtuTable
    (tSNMP_OCTET_STRING_TYPE * pFsipv6PmtuDest)
{
    UNUSED_PARAM (pFsipv6PmtuDest);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsipv6PmtuTable
 Input       :  The Indices
                Fsipv6PmtuDest
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsipv6PmtuTable (tSNMP_OCTET_STRING_TYPE * pFsipv6PmtuDest)
{
    UNUSED_PARAM (pFsipv6PmtuDest);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsipv6PmtuTable
 Input       :  The Indices
                Fsipv6PmtuDest
                nextFsipv6PmtuDest
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsipv6PmtuTable (tSNMP_OCTET_STRING_TYPE * pFsipv6PmtuDest,
                                tSNMP_OCTET_STRING_TYPE * pNextFsipv6PmtuDest)
{
    UNUSED_PARAM (pFsipv6PmtuDest);
    UNUSED_PARAM (pNextFsipv6PmtuDest);

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsipv6Pmtu
 Input       :  The Indices
                Fsipv6PmtuDest

                The Object 
                retValFsipv6Pmtu
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6Pmtu (tSNMP_OCTET_STRING_TYPE * pFsipv6PmtuDest,
                  INT4 *pi4RetValFsipv6Pmtu)
{
    UNUSED_PARAM (pi4RetValFsipv6Pmtu);
    UNUSED_PARAM (pFsipv6PmtuDest);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsipv6PmtuTimeStamp
 Input       :  The Indices
                Fsipv6PmtuDest

                The Object 
                retValFsipv6PmtuTimeStamp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6PmtuTimeStamp (tSNMP_OCTET_STRING_TYPE * pFsipv6PmtuDest,
                           INT4 *pi4RetValFsipv6PmtuTimeStamp)
{
    UNUSED_PARAM (pi4RetValFsipv6PmtuTimeStamp);
    UNUSED_PARAM (pFsipv6PmtuDest);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsipv6PmtuAdminStatus
 Input       :  The Indices
                Fsipv6PmtuDest

                The Object 
                retValFsipv6PmtuAdminStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6PmtuAdminStatus (tSNMP_OCTET_STRING_TYPE * pFsipv6PmtuDest,
                             INT4 *pi4RetValFsipv6PmtuAdminStatus)
{
    UNUSED_PARAM (pFsipv6PmtuDest);
    UNUSED_PARAM (pi4RetValFsipv6PmtuAdminStatus);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsipv6Pmtu
 Input       :  The Indices
                Fsipv6PmtuDest

                The Object 
                setValFsipv6Pmtu
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6Pmtu (tSNMP_OCTET_STRING_TYPE * pFsipv6PmtuDest,
                  INT4 i4SetValFsipv6Pmtu)
{
    UNUSED_PARAM (pFsipv6PmtuDest);
    UNUSED_PARAM (i4SetValFsipv6Pmtu);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsipv6PmtuAdminStatus
 Input       :  The Indices
                Fsipv6PmtuDest

                The Object 
                setValFsipv6PmtuAdminStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6PmtuAdminStatus (tSNMP_OCTET_STRING_TYPE * pFsipv6PmtuDest,
                             INT4 i4SetValFsipv6PmtuAdminStatus)
{
    UNUSED_PARAM (pFsipv6PmtuDest);
    UNUSED_PARAM (i4SetValFsipv6PmtuAdminStatus);
    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Fsipv6Pmtu
 Input       :  The Indices
                Fsipv6PmtuDest

                The Object 
                testValFsipv6Pmtu
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6Pmtu (UINT4 *pu4ErrorCode,
                     tSNMP_OCTET_STRING_TYPE * pFsipv6PmtuDest,
                     INT4 i4TestValFsipv6Pmtu)
{
    UNUSED_PARAM (pFsipv6PmtuDest);
    UNUSED_PARAM (i4TestValFsipv6Pmtu);
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6PmtuAdminStatus
 Input       :  The Indices
                Fsipv6PmtuDest

                The Object 
                testValFsipv6PmtuAdminStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6PmtuAdminStatus (UINT4 *pu4ErrorCode,
                                tSNMP_OCTET_STRING_TYPE * pFsipv6PmtuDest,
                                INT4 i4TestValFsipv6PmtuAdminStatus)
{
    UNUSED_PARAM (pFsipv6PmtuDest);
    UNUSED_PARAM (i4TestValFsipv6PmtuAdminStatus);

    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Fsipv6PmtuTable
 Input       :  The Indices
                Fsipv6PmtuDest
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsipv6PmtuTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Fsipv6NdLanCacheTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsipv6NdLanCacheTable
 Input       :  The Indices
                Fsipv6NdLanCacheIfIndex
                Fsipv6NdLanCacheIPv6Addr
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsipv6NdLanCacheTable (INT4 i4Fsipv6NdLanCacheIfIndex,
                                               tSNMP_OCTET_STRING_TYPE *
                                               pFsipv6NdLanCacheIPv6Addr)
{
    INT4                i4AddrType;
    tLip6If            *pIf6 = NULL;

    if (pFsipv6NdLanCacheIPv6Addr == NULL)
    {
        return SNMP_FAILURE;
    }

    if (Lip6UtlIfEntryExists ((UINT4) i4Fsipv6NdLanCacheIfIndex)
        == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    /* Address can be only link-local/unicast/anycast */

    i4AddrType =
        Ip6AddrType ((tIp6Addr *) (VOID *) pFsipv6NdLanCacheIPv6Addr->
                     pu1_OctetList);

    if (i4AddrType == ADDR6_MULTI || i4AddrType == ADDR6_UNSPECIFIED)
    {
        return SNMP_FAILURE;
    }

    pIf6 = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6NdLanCacheIfIndex);

    if (Lip6NdGetEntry (pIf6,
                        (tIp6Addr *) (VOID *)
                        pFsipv6NdLanCacheIPv6Addr->pu1_OctetList) == NULL)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsipv6NdLanCacheTable
 Input       :  The Indices
                Fsipv6NdLanCacheIfIndex
                Fsipv6NdLanCacheIPv6Addr
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsipv6NdLanCacheTable (INT4 *pi4Fsipv6NdLanCacheIfIndex,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFsipv6NdLanCacheIPv6Addr)
{
    tIp6Addr            TempAddr1, TempAddr2;

    MEMSET (&TempAddr1, 0, IP6_ADDR_SIZE);
    MEMSET (&TempAddr2, 0, IP6_ADDR_SIZE);

    if (Lip6NdGetNextCacheEntry (0, pi4Fsipv6NdLanCacheIfIndex,
                                 TempAddr1, &TempAddr2) == OSIX_SUCCESS)
    {
        Ip6AddrCopy ((tIp6Addr *) (VOID *)
                     pFsipv6NdLanCacheIPv6Addr->pu1_OctetList, &TempAddr2);
        pFsipv6NdLanCacheIPv6Addr->i4_Length = IP6_ADDR_SIZE;
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsipv6NdLanCacheTable
 Input       :  The Indices
                Fsipv6NdLanCacheIfIndex
                nextFsipv6NdLanCacheIfIndex
                Fsipv6NdLanCacheIPv6Addr
                nextFsipv6NdLanCacheIPv6Addr
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsipv6NdLanCacheTable (INT4 i4Fsipv6NdLanCacheIfIndex,
                                      INT4 *pi4NextFsipv6NdLanCacheIfIndex,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pFsipv6NdLanCacheIPv6Addr,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pNextFsipv6NdLanCacheIPv6Addr)
{
    tIp6Addr            TempAddr;
    tIp6Addr            TempNextAddr;

    if (i4Fsipv6NdLanCacheIfIndex > LIP6_MAX_INTERFACES)
    {
        return (SNMP_FAILURE);
    }

    MEMSET (&TempAddr, 0, IP6_ADDR_SIZE);
    MEMSET (&TempNextAddr, 0, IP6_ADDR_SIZE);

    Ip6AddrCopy (&TempAddr,
                 (tIp6Addr *) (VOID *) pFsipv6NdLanCacheIPv6Addr->
                 pu1_OctetList);

    if (Lip6NdGetNextCacheEntry (i4Fsipv6NdLanCacheIfIndex,
                                 pi4NextFsipv6NdLanCacheIfIndex,
                                 TempAddr, &TempNextAddr) == OSIX_SUCCESS)
    {
        Ip6AddrCopy ((tIp6Addr *) (VOID *)
                     pNextFsipv6NdLanCacheIPv6Addr->pu1_OctetList,
                     &TempNextAddr);

        pNextFsipv6NdLanCacheIPv6Addr->i4_Length = IP6_ADDR_SIZE;
        return SNMP_SUCCESS;
    }

    MEMSET (pNextFsipv6NdLanCacheIPv6Addr->pu1_OctetList, IP6_ZERO,
            IP6_ADDR_SIZE);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsipv6NdLanCachePhysAddr
 Input       :  The Indices
                Fsipv6NdLanCacheIfIndex
                Fsipv6NdLanCacheIPv6Addr

                The Object 
                retValFsipv6NdLanCachePhysAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6NdLanCachePhysAddr (INT4 i4Fsipv6NdLanCacheIfIndex,
                                tSNMP_OCTET_STRING_TYPE *
                                pFsipv6NdLanCacheIPv6Addr,
                                tSNMP_OCTET_STRING_TYPE *
                                pRetValFsipv6NdLanCachePhysAddr)
{
    tLip6If            *pIf6 = NULL;
    tLip6NdEntry       *pNd6cEntry = NULL;

    pIf6 = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6NdLanCacheIfIndex);

    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    pNd6cEntry = Lip6NdGetEntry (pIf6,
                                 (tIp6Addr *) (VOID *)
                                 pFsipv6NdLanCacheIPv6Addr->pu1_OctetList);
    if (pNd6cEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    MEMCPY (pRetValFsipv6NdLanCachePhysAddr->pu1_OctetList, pNd6cEntry->MacAddr,
            MAC_ADDR_LEN);

    pRetValFsipv6NdLanCachePhysAddr->i4_Length = MAC_ADDR_LEN;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsipv6NdLanCacheStatus
 Input       :  The Indices
                Fsipv6NdLanCacheIfIndex
                Fsipv6NdLanCacheIPv6Addr

                The Object 
                retValFsipv6NdLanCacheStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6NdLanCacheStatus (INT4 i4Fsipv6NdLanCacheIfIndex,
                              tSNMP_OCTET_STRING_TYPE *
                              pFsipv6NdLanCacheIPv6Addr,
                              INT4 *pi4RetValFsipv6NdLanCacheStatus)
{
    tLip6If            *pIf6 = NULL;
    tLip6NdEntry       *pNd6cEntry = NULL;

    pIf6 = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6NdLanCacheIfIndex);

    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    pNd6cEntry = Lip6NdGetEntry (pIf6,
                                 (tIp6Addr *) (VOID *)
                                 pFsipv6NdLanCacheIPv6Addr->pu1_OctetList);
    if (pNd6cEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsipv6NdLanCacheStatus = ND6_LAN_CACHE_ENTRY_VALID;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6NdLanCacheState
 Input       :  The Indices
                Fsipv6NdLanCacheIfIndex
                Fsipv6NdLanCacheIPv6Addr

                The Object 
                retValFsipv6NdLanCacheState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6NdLanCacheState (INT4 i4Fsipv6NdLanCacheIfIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pFsipv6NdLanCacheIPv6Addr,
                             INT4 *pi4RetValFsipv6NdLanCacheState)
{
    tLip6If            *pIf6 = NULL;
    tLip6NdEntry       *pNd6cEntry = NULL;

    pIf6 = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6NdLanCacheIfIndex);

    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    pNd6cEntry = Lip6NdGetEntry (pIf6,
                                 (tIp6Addr *) (VOID *)
                                 pFsipv6NdLanCacheIPv6Addr->pu1_OctetList);
    if (pNd6cEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsipv6NdLanCacheState = (INT4) pNd6cEntry->u1ReachState;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6NdLanCacheUseTime
 Input       :  The Indices
                Fsipv6NdLanCacheIfIndex
                Fsipv6NdLanCacheIPv6Addr

                The Object 
                retValFsipv6NdLanCacheUseTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6NdLanCacheUseTime (INT4 i4Fsipv6NdLanCacheIfIndex,
                               tSNMP_OCTET_STRING_TYPE *
                               pFsipv6NdLanCacheIPv6Addr,
                               UINT4 *pu4RetValFsipv6NdLanCacheUseTime)
{
    tLip6If            *pIf6 = NULL;
    tLip6NdEntry       *pNd6cEntry = NULL;

    pIf6 = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6NdLanCacheIfIndex);

    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    pNd6cEntry = Lip6NdGetEntry (pIf6,
                                 (tIp6Addr *) (VOID *)
                                 pFsipv6NdLanCacheIPv6Addr->pu1_OctetList);
    if (pNd6cEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsipv6NdLanCacheUseTime = pNd6cEntry->u4LastUpdatedTime;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsipv6NdLanCacheIsSecure
 Input       :  The Indices
                Fsipv6NdLanCacheIfIndex
                Fsipv6NdLanCacheIPv6Addr

                The Object
                retValFsipv6NdLanCacheIsSecure
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6NdLanCacheIsSecure (INT4 i4Fsipv6NdLanCacheIfIndex,
                                tSNMP_OCTET_STRING_TYPE *
                                pFsipv6NdLanCacheIPv6Addr,
                                INT4 *pi4RetValFsipv6NdLanCacheIsSecure)
{
    UNUSED_PARAM (i4Fsipv6NdLanCacheIfIndex);
    UNUSED_PARAM (pFsipv6NdLanCacheIPv6Addr);
    UNUSED_PARAM (pi4RetValFsipv6NdLanCacheIsSecure);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsipv6NdLanCachePhysAddr
 Input       :  The Indices
                Fsipv6NdLanCacheIfIndex
                Fsipv6NdLanCacheIPv6Addr

                The Object 
                setValFsipv6NdLanCachePhysAddr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6NdLanCachePhysAddr (INT4 i4Fsipv6NdLanCacheIfIndex,
                                tSNMP_OCTET_STRING_TYPE *
                                pFsipv6NdLanCacheIPv6Addr,
                                tSNMP_OCTET_STRING_TYPE *
                                pSetValFsipv6NdLanCachePhysAddr)
{
    tLip6If            *pIf6Entry = NULL;
    tLip6NdEntry       *pNd6cEntry = NULL;
    tIp6Addr            TmpIp6Addr;
    INT4                i4RSValue = 0;

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6NdLanCacheIfIndex);
    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    MEMSET (&TmpIp6Addr, 0, sizeof (tIp6Addr));
    MEMCPY (&TmpIp6Addr, pFsipv6NdLanCacheIPv6Addr->pu1_OctetList,
            sizeof (tIp6Addr));

    pNd6cEntry = Lip6NdGetEntry (pIf6Entry, &TmpIp6Addr);
    if (pNd6cEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pSetValFsipv6NdLanCachePhysAddr == NULL)
    {
        return SNMP_FAILURE;
    }

    MEMSET (&pNd6cEntry->MacAddr, 0, sizeof (tMacAddr));
    MEMCPY (&pNd6cEntry->MacAddr,
            pSetValFsipv6NdLanCachePhysAddr->pu1_OctetList, sizeof (tMacAddr));

    Lip6NdSetReachState (pNd6cEntry, ND6_CACHE_ENTRY_STATIC);
    /* update the RowStatus */
    pNd6cEntry->u1RowStatus = ACTIVE;

    IncMsrForIpv6NetToPhyTable (i4Fsipv6NdLanCacheIfIndex,
                                pFsipv6NdLanCacheIPv6Addr,
                                's', pSetValFsipv6NdLanCachePhysAddr,
                                FsMIStdIpNetToPhysicalPhysAddress,
                                (sizeof (FsMIStdIpNetToPhysicalPhysAddress) /
                                 sizeof (UINT4)), FALSE);

    /* For the Ipv6LanCacheTable, there is no separate rowstatus object.
     * When the physical address is configured for NetToPhysicalTable,
     * the rowstatus for the entry is set to STATIC_NOT_IN_SERVICE.
     * To make the entry rowstatus to active a separate notification
     * is sent for the object FsMIStdIpNetToPhysicalRowStatus */

    i4RSValue = ACTIVE;
    IncMsrForIpv6NetToPhyTable (i4Fsipv6NdLanCacheIfIndex,
                                pFsipv6NdLanCacheIPv6Addr,
                                'i', &i4RSValue,
                                FsMIStdIpNetToPhysicalRowStatus,
                                (sizeof (FsMIStdIpNetToPhysicalRowStatus) /
                                 sizeof (UINT4)), TRUE);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6NdLanCacheStatus
 Input       :  The Indices
                Fsipv6NdLanCacheIfIndex
                Fsipv6NdLanCacheIPv6Addr

                The Object 
                setValFsipv6NdLanCacheStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6NdLanCacheStatus (INT4 i4Fsipv6NdLanCacheIfIndex,
                              tSNMP_OCTET_STRING_TYPE *
                              pFsipv6NdLanCacheIPv6Addr,
                              INT4 i4SetValFsipv6NdLanCacheStatus)
{
    tLip6If            *pIf6Entry = NULL;
    tLip6NdEntry       *pNd6cEntry = NULL;;

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6NdLanCacheIfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    pNd6cEntry =
        Lip6NdGetEntry (pIf6Entry,
                        (tIp6Addr *) (VOID *) pFsipv6NdLanCacheIPv6Addr->
                        pu1_OctetList);

    switch (i4SetValFsipv6NdLanCacheStatus)
    {
        case ND6_LAN_CACHE_ENTRY_VALID:
            if (pNd6cEntry == NULL)
            {
                pNd6cEntry =
                    Lip6NdCreateEntry (pIf6Entry, (tIp6Addr *) (VOID *)
                                       pFsipv6NdLanCacheIPv6Addr->
                                       pu1_OctetList);
                if (pNd6cEntry == NULL)
                {
                    return SNMP_FAILURE;
                }
            }
            break;

        case ND6_LAN_CACHE_ENTRY_INVALID:
            if (pNd6cEntry != NULL)
            {
                if (Lip6NdDeleteEntry (pNd6cEntry) != OSIX_SUCCESS)
                {
                    return SNMP_FAILURE;
                }
            }
            break;

        default:
            return SNMP_FAILURE;
    }

    IncMsrForIpv6NetToPhyTable (i4Fsipv6NdLanCacheIfIndex,
                                pFsipv6NdLanCacheIPv6Addr,
                                'i', &i4SetValFsipv6NdLanCacheStatus,
                                FsMIStdIpNetToPhysicalType,
                                (sizeof (FsMIStdIpNetToPhysicalType) /
                                 sizeof (UINT4)), FALSE);

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Fsipv6NdLanCachePhysAddr
 Input       :  The Indices
                Fsipv6NdLanCacheIfIndex
                Fsipv6NdLanCacheIPv6Addr

                The Object 
                testValFsipv6NdLanCachePhysAddr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6NdLanCachePhysAddr (UINT4 *pu4ErrorCode,
                                   INT4 i4Fsipv6NdLanCacheIfIndex,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsipv6NdLanCacheIPv6Addr,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pTestValFsipv6NdLanCachePhysAddr)
{
    tIp6Addr            TmpIp6Addr;
    tMacAddr            ZeroMac;

    MEMSET (&ZeroMac, 0, sizeof (tMacAddr));

    if (Lip6UtlIfEntryExists ((UINT4) i4Fsipv6NdLanCacheIfIndex) ==
        OSIX_FAILURE)
    {
        CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (pFsipv6NdLanCacheIPv6Addr == NULL)
    {
        CLI_SET_ERR (CLI_IP6_INVALID_ADDRESS);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    MEMCPY (&TmpIp6Addr, pFsipv6NdLanCacheIPv6Addr->pu1_OctetList,
            sizeof (tIp6Addr));

    if (IS_ADDR_MULTI (TmpIp6Addr) || IS_ADDR_UNSPECIFIED (TmpIp6Addr))
    {
        CLI_SET_ERR (CLI_IP6_INVALID_ADDR_TYPE);
        return SNMP_FAILURE;
    }

    if (pTestValFsipv6NdLanCachePhysAddr->i4_Length != CFA_ENET_ADDR_LEN)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        CLI_SET_ERR (CLI_IP6_INVALID_PHYS_ADDR);
        return SNMP_FAILURE;
    }

    /* Check the the given Mac is all Zero. */
    if ((MEMCMP (&ZeroMac,
                 pTestValFsipv6NdLanCachePhysAddr->pu1_OctetList,
                 pTestValFsipv6NdLanCachePhysAddr->i4_Length)) == IP6_ZERO)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_INVALID_PHYS_ADDR);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6NdLanCacheStatus
 Input       :  The Indices
                Fsipv6NdLanCacheIfIndex
                Fsipv6NdLanCacheIPv6Addr

                The Object 
                testValFsipv6NdLanCacheStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6NdLanCacheStatus (UINT4 *pu4ErrorCode,
                                 INT4 i4Fsipv6NdLanCacheIfIndex,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pFsipv6NdLanCacheIPv6Addr,
                                 INT4 i4TestValFsipv6NdLanCacheStatus)
{
    tIp6Addr            TmpIp6Addr;
    UINT4               u4TmpIfIndex = 0;

    *pu4ErrorCode = SNMP_ERR_NO_CREATION;

    if (Lip6UtlIfEntryExists ((UINT4) i4Fsipv6NdLanCacheIfIndex) ==
        OSIX_FAILURE)
    {
        CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
        return SNMP_FAILURE;
    }

    if (pFsipv6NdLanCacheIPv6Addr == NULL)
    {
        CLI_SET_ERR (CLI_IP6_INVALID_ADDRESS);
        return SNMP_FAILURE;
    }

    MEMSET (&TmpIp6Addr, 0, sizeof (tIp6Addr));
    MEMCPY (&TmpIp6Addr, pFsipv6NdLanCacheIPv6Addr->pu1_OctetList,
            sizeof (tIp6Addr));

    if (IS_ADDR_MULTI (TmpIp6Addr) || IS_ADDR_UNSPECIFIED (TmpIp6Addr))
    {
        CLI_SET_ERR (CLI_IP6_INVALID_ADDR_TYPE);
        return SNMP_FAILURE;
    }

    /* Verify that the address does not belong to any of the local interface */
    if (Lip6UtlGetIndexForAddr (&TmpIp6Addr, &u4TmpIfIndex, VCM_DEFAULT_CONTEXT)
        == OSIX_SUCCESS)
    {
        /* Given Address belongs to this interface. We shall not
         * add an entry in Neighbor cache table for any address
         * belonging to local interface. */
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_ND_OUR_ADDR);
        return SNMP_FAILURE;
    }

    if (i4TestValFsipv6NdLanCacheStatus != ND6_LAN_CACHE_ENTRY_VALID &&
        i4TestValFsipv6NdLanCacheStatus != ND6_LAN_CACHE_ENTRY_INVALID)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_ND_LAN_CACHE_STATUS);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Fsipv6NdLanCacheTable
 Input       :  The Indices
                Fsipv6NdLanCacheIfIndex
                Fsipv6NdLanCacheIPv6Addr
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsipv6NdLanCacheTable (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Fsipv6NdWanCacheTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsipv6NdWanCacheTable
 Input       :  The Indices
                Fsipv6NdWanCacheIfIndex
                Fsipv6NdWanCacheIPv6Addr
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsipv6NdWanCacheTable (INT4 i4Fsipv6NdWanCacheIfIndex,
                                               tSNMP_OCTET_STRING_TYPE *
                                               pFsipv6NdWanCacheIPv6Addr)
{
    UNUSED_PARAM (i4Fsipv6NdWanCacheIfIndex);
    UNUSED_PARAM (pFsipv6NdWanCacheIPv6Addr);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsipv6NdWanCacheTable
 Input       :  The Indices
                Fsipv6NdWanCacheIfIndex
                Fsipv6NdWanCacheIPv6Addr
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsipv6NdWanCacheTable (INT4 *pi4Fsipv6NdWanCacheIfIndex,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFsipv6NdWanCacheIPv6Addr)
{
    UNUSED_PARAM (pi4Fsipv6NdWanCacheIfIndex);
    UNUSED_PARAM (pFsipv6NdWanCacheIPv6Addr);

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsipv6NdWanCacheTable
 Input       :  The Indices
                Fsipv6NdWanCacheIfIndex
                nextFsipv6NdWanCacheIfIndex
                Fsipv6NdWanCacheIPv6Addr
                nextFsipv6NdWanCacheIPv6Addr
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsipv6NdWanCacheTable (INT4 i4Fsipv6NdWanCacheIfIndex,
                                      INT4 *pi4NextFsipv6NdWanCacheIfIndex,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pFsipv6NdWanCacheIPv6Addr,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pNextFsipv6NdWanCacheIPv6Addr)
{
    UNUSED_PARAM (i4Fsipv6NdWanCacheIfIndex);
    UNUSED_PARAM (pi4NextFsipv6NdWanCacheIfIndex);
    UNUSED_PARAM (pFsipv6NdWanCacheIPv6Addr);
    UNUSED_PARAM (pNextFsipv6NdWanCacheIPv6Addr);

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsipv6NdWanCacheStatus
 Input       :  The Indices
                Fsipv6NdWanCacheIfIndex
                Fsipv6NdWanCacheIPv6Addr

                The Object 
                retValFsipv6NdWanCacheStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6NdWanCacheStatus (INT4 i4Fsipv6NdWanCacheIfIndex,
                              tSNMP_OCTET_STRING_TYPE *
                              pFsipv6NdWanCacheIPv6Addr,
                              INT4 *pi4RetValFsipv6NdWanCacheStatus)
{
    UNUSED_PARAM (i4Fsipv6NdWanCacheIfIndex);
    UNUSED_PARAM (pFsipv6NdWanCacheIPv6Addr);
    UNUSED_PARAM (pi4RetValFsipv6NdWanCacheStatus);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6NdWanCacheState
 Input       :  The Indices
                Fsipv6NdWanCacheIfIndex
                Fsipv6NdWanCacheIPv6Addr

                The Object 
                retValFsipv6NdWanCacheState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6NdWanCacheState (INT4 i4Fsipv6NdWanCacheIfIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pFsipv6NdWanCacheIPv6Addr,
                             INT4 *pi4RetValFsipv6NdWanCacheState)
{
    UNUSED_PARAM (i4Fsipv6NdWanCacheIfIndex);
    UNUSED_PARAM (pFsipv6NdWanCacheIPv6Addr);
    UNUSED_PARAM (pi4RetValFsipv6NdWanCacheState);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6NdWanCacheUseTime
 Input       :  The Indices
                Fsipv6NdWanCacheIfIndex
                Fsipv6NdWanCacheIPv6Addr

                The Object 
                retValFsipv6NdWanCacheUseTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6NdWanCacheUseTime (INT4 i4Fsipv6NdWanCacheIfIndex,
                               tSNMP_OCTET_STRING_TYPE *
                               pFsipv6NdWanCacheIPv6Addr,
                               UINT4 *pu4RetValFsipv6NdWanCacheUseTime)
{
    UNUSED_PARAM (i4Fsipv6NdWanCacheIfIndex);
    UNUSED_PARAM (pFsipv6NdWanCacheIPv6Addr);
    UNUSED_PARAM (pu4RetValFsipv6NdWanCacheUseTime);

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsipv6NdWanCacheStatus
 Input       :  The Indices
                Fsipv6NdWanCacheIfIndex
                Fsipv6NdWanCacheIPv6Addr

                The Object 
                setValFsipv6NdWanCacheStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6NdWanCacheStatus (INT4 i4Fsipv6NdWanCacheIfIndex,
                              tSNMP_OCTET_STRING_TYPE *
                              pFsipv6NdWanCacheIPv6Addr,
                              INT4 i4SetValFsipv6NdWanCacheStatus)
{
    UNUSED_PARAM (i4Fsipv6NdWanCacheIfIndex);
    UNUSED_PARAM (pFsipv6NdWanCacheIPv6Addr);
    UNUSED_PARAM (i4SetValFsipv6NdWanCacheStatus);

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Fsipv6NdWanCacheStatus
 Input       :  The Indices
                Fsipv6NdWanCacheIfIndex
                Fsipv6NdWanCacheIPv6Addr

                The Object 
                testValFsipv6NdWanCacheStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6NdWanCacheStatus (UINT4 *pu4ErrorCode,
                                 INT4 i4Fsipv6NdWanCacheIfIndex,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pFsipv6NdWanCacheIPv6Addr,
                                 INT4 i4TestValFsipv6NdWanCacheStatus)
{
    UNUSED_PARAM (i4TestValFsipv6NdWanCacheStatus);
    UNUSED_PARAM (pFsipv6NdWanCacheIPv6Addr);
    UNUSED_PARAM (i4Fsipv6NdWanCacheIfIndex);

    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Fsipv6NdWanCacheTable
 Input       :  The Indices
                Fsipv6NdWanCacheIfIndex
                Fsipv6NdWanCacheIPv6Addr
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsipv6NdWanCacheTable (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Fsipv6PingTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsipv6PingTable
 Input       :  The Indices
                Fsipv6PingIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsipv6PingTable (INT4 i4Fsipv6PingIndex)
{
    if ((i4Fsipv6PingIndex >= 0) && (i4Fsipv6PingIndex < PING6_MAX_INSTANCES))
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsipv6PingTable
 Input       :  The Indices
                Fsipv6PingIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsipv6PingTable (INT4 *pi4Fsipv6PingIndex)
{
    *pi4Fsipv6PingIndex = 0;

    if (gaPing6Table[*pi4Fsipv6PingIndex].i4RowStatus != PING6_STATUS_INVALID)
    {
        return SNMP_SUCCESS;
    }

    return (nmhGetNextIndexFsipv6PingTable (0, pi4Fsipv6PingIndex));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsipv6PingTable
 Input       :  The Indices
                Fsipv6PingIndex
                nextFsipv6PingIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsipv6PingTable (INT4 i4Fsipv6PingIndex,
                                INT4 *pi4NextFsipv6PingIndex)
{
    INT4                i4Index = i4Fsipv6PingIndex + 1;

    if (i4Fsipv6PingIndex <= 0)
    {
        return SNMP_FAILURE;
    }

    for (; i4Index < PING6_MAX_INSTANCES; i4Index++)
    {
        if (gaPing6Table[i4Index].i4RowStatus != PING6_STATUS_INVALID)
        {
            *pi4NextFsipv6PingIndex = i4Index;
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsipv6PingDest
 Input       :  The Indices
                Fsipv6PingIndex

                The Object 
                retValFsipv6PingDest
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6PingDest (INT4 i4Fsipv6PingIndex,
                      tSNMP_OCTET_STRING_TYPE * pRetValFsipv6PingDest)
{
    if (gaPing6Table[i4Fsipv6PingIndex].i4RowStatus != PING6_STATUS_INVALID)
    {
        Ip6AddrCopy ((tIp6Addr *) (VOID *) pRetValFsipv6PingDest->pu1_OctetList,
                     &gaPing6Table[i4Fsipv6PingIndex].DestAddr);
        pRetValFsipv6PingDest->i4_Length = sizeof (tIp6Addr);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsipv6PingIfIndex
 Input       :  The Indices
                Fsipv6PingIndex

                The Object 
                retValFsipv6PingIfIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6PingIfIndex (INT4 i4Fsipv6PingIndex,
                         INT4 *pi4RetValFsipv6PingIfIndex)
{
    if (gaPing6Table[i4Fsipv6PingIndex].i4RowStatus != PING6_STATUS_INVALID)
    {
        *pi4RetValFsipv6PingIfIndex =
            (INT4) gaPing6Table[i4Fsipv6PingIndex].u4IfIndex;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsipv6PingAdminStatus
 Input       :  The Indices
                Fsipv6PingIndex

                The Object 
                retValFsipv6PingAdminStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6PingAdminStatus (INT4 i4Fsipv6PingIndex,
                             INT4 *pi4RetValFsipv6PingAdminStatus)
{
    *pi4RetValFsipv6PingAdminStatus =
        gaPing6Table[i4Fsipv6PingIndex].i4RowStatus;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6PingInterval
 Input       :  The Indices
                Fsipv6PingIndex

                The Object 
                retValFsipv6PingInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6PingInterval (INT4 i4Fsipv6PingIndex,
                          INT4 *pi4RetValFsipv6PingInterval)
{
    if (gaPing6Table[i4Fsipv6PingIndex].i4RowStatus != PING6_STATUS_INVALID)
    {
        *pi4RetValFsipv6PingInterval =
            (INT4) gaPing6Table[i4Fsipv6PingIndex].u4Interval;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsipv6PingRcvTimeout
 Input       :  The Indices
                Fsipv6PingIndex

                The Object 
                retValFsipv6PingRcvTimeout
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6PingRcvTimeout (INT4 i4Fsipv6PingIndex,
                            INT4 *pi4RetValFsipv6PingRcvTimeout)
{
    if (gaPing6Table[i4Fsipv6PingIndex].i4RowStatus != PING6_STATUS_INVALID)
    {
        *pi4RetValFsipv6PingRcvTimeout =
            (INT4) gaPing6Table[i4Fsipv6PingIndex].u2TimeOut;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsipv6PingTries
 Input       :  The Indices
                Fsipv6PingIndex

                The Object 
                retValFsipv6PingTries
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6PingTries (INT4 i4Fsipv6PingIndex, INT4 *pi4RetValFsipv6PingTries)
{
    if (gaPing6Table[i4Fsipv6PingIndex].i4RowStatus != PING6_STATUS_INVALID)
    {
        *pi4RetValFsipv6PingTries =
            (INT4) gaPing6Table[i4Fsipv6PingIndex].i2MaxTries;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsipv6PingSize
 Input       :  The Indices
                Fsipv6PingIndex

                The Object 
                retValFsipv6PingSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6PingSize (INT4 i4Fsipv6PingIndex, INT4 *pi4RetValFsipv6PingSize)
{
    if (gaPing6Table[i4Fsipv6PingIndex].i4RowStatus != PING6_STATUS_INVALID)
    {
        *pi4RetValFsipv6PingSize =
            (INT4) gaPing6Table[i4Fsipv6PingIndex].i2DataSize;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsipv6PingSentCount
 Input       :  The Indices
                Fsipv6PingIndex

                The Object 
                retValFsipv6PingSentCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6PingSentCount (INT4 i4Fsipv6PingIndex,
                           INT4 *pi4RetValFsipv6PingSentCount)
{
    if (gaPing6Table[i4Fsipv6PingIndex].i4RowStatus != PING6_STATUS_INVALID)
    {
        *pi4RetValFsipv6PingSentCount =
            (INT4) gaPing6Table[i4Fsipv6PingIndex].u2SentCount;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsipv6PingAverageTime
 Input       :  The Indices
                Fsipv6PingIndex

                The Object 
                retValFsipv6PingAverageTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6PingAverageTime (INT4 i4Fsipv6PingIndex,
                             INT4 *pi4RetValFsipv6PingAverageTime)
{
    if (gaPing6Table[i4Fsipv6PingIndex].i4RowStatus != PING6_STATUS_INVALID)
    {
        *pi4RetValFsipv6PingAverageTime =
            (INT4) gaPing6Table[i4Fsipv6PingIndex].u2AvgTime;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsipv6PingMaxTime
 Input       :  The Indices
                Fsipv6PingIndex

                The Object 
                retValFsipv6PingMaxTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6PingMaxTime (INT4 i4Fsipv6PingIndex,
                         INT4 *pi4RetValFsipv6PingMaxTime)
{
    if (gaPing6Table[i4Fsipv6PingIndex].i4RowStatus != PING6_STATUS_INVALID)
    {
        *pi4RetValFsipv6PingMaxTime =
            (INT4) gaPing6Table[i4Fsipv6PingIndex].u2MaxTime;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsipv6PingMinTime
 Input       :  The Indices
                Fsipv6PingIndex

                The Object 
                retValFsipv6PingMinTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6PingMinTime (INT4 i4Fsipv6PingIndex,
                         INT4 *pi4RetValFsipv6PingMinTime)
{
    if (gaPing6Table[i4Fsipv6PingIndex].i4RowStatus != PING6_STATUS_INVALID)
    {
        *pi4RetValFsipv6PingMinTime =
            (INT4) gaPing6Table[i4Fsipv6PingIndex].u2MinTime;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsipv6PingOperStatus
 Input       :  The Indices
                Fsipv6PingIndex

                The Object 
                retValFsipv6PingOperStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6PingOperStatus (INT4 i4Fsipv6PingIndex,
                            INT4 *pi4RetValFsipv6PingOperStatus)
{
    if (gaPing6Table[i4Fsipv6PingIndex].i4RowStatus != PING6_STATUS_INVALID)
    {
        *pi4RetValFsipv6PingOperStatus =
            (INT4) gaPing6Table[i4Fsipv6PingIndex].i1Oper;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsipv6PingSuccesses
 Input       :  The Indices
                Fsipv6PingIndex

                The Object 
                retValFsipv6PingSuccesses
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6PingSuccesses (INT4 i4Fsipv6PingIndex,
                           UINT4 *pu4RetValFsipv6PingSuccesses)
{
    if (gaPing6Table[i4Fsipv6PingIndex].i4RowStatus != PING6_STATUS_INVALID)
    {
        *pu4RetValFsipv6PingSuccesses =
            (INT4) gaPing6Table[i4Fsipv6PingIndex].u2Successes;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsipv6PingPercentageLoss
 Input       :  The Indices
                Fsipv6PingIndex

                The Object 
                retValFsipv6PingPercentageLoss
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6PingPercentageLoss (INT4 i4Fsipv6PingIndex,
                                INT4 *pi4RetValFsipv6PingPercentageLoss)
{
    if (gaPing6Table[i4Fsipv6PingIndex].i4RowStatus != PING6_STATUS_INVALID)
    {
        if (gaPing6Table[i4Fsipv6PingIndex].u2SentCount != 0)
        {
            *pi4RetValFsipv6PingPercentageLoss =
                ((INT4)
                 ((gaPing6Table
                   [i4Fsipv6PingIndex].u2SentCount -
                   gaPing6Table[i4Fsipv6PingIndex].u2Successes) * 100 /
                  gaPing6Table[i4Fsipv6PingIndex].u2SentCount));
            return SNMP_SUCCESS;
        }
        *pi4RetValFsipv6PingPercentageLoss = 0;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsipv6PingData
 Input       :  The Indices
                Fsipv6PingIndex

                The Object 
                retValFsipv6PingData
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6PingData (INT4 i4Fsipv6PingIndex,
                      tSNMP_OCTET_STRING_TYPE * pRetValFsipv6PingData)
{
    if (gaPing6Table[i4Fsipv6PingIndex].i4RowStatus != PING6_STATUS_INVALID)
    {
        MEMCPY (pRetValFsipv6PingData->pu1_OctetList,
                &gaPing6Table[i4Fsipv6PingIndex].PingData,
                PING6_CONFIG_DATA_SIZE);
        pRetValFsipv6PingData->i4_Length = PING6_CONFIG_DATA_SIZE;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsipv6PingSrcAddr
 Input       :  The Indices
                Fsipv6PingIndex

                The Object 
                retValFsipv6PingSrcAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6PingSrcAddr (INT4 i4Fsipv6PingIndex,
                         tSNMP_OCTET_STRING_TYPE * pRetValFsipv6PingSrcAddr)
{
    if (gaPing6Table[i4Fsipv6PingIndex].i4RowStatus != PING6_STATUS_INVALID)
    {
        Ip6AddrCopy ((tIp6Addr *) (VOID *) pRetValFsipv6PingSrcAddr->
                     pu1_OctetList, &gaPing6Table[i4Fsipv6PingIndex].SrcAddr);
        pRetValFsipv6PingSrcAddr->i4_Length = sizeof (tIp6Addr);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsipv6PingDest
 Input       :  The Indices
                Fsipv6PingIndex

                The Object 
                setValFsipv6PingDest
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6PingDest (INT4 i4Fsipv6PingIndex,
                      tSNMP_OCTET_STRING_TYPE * pSetValFsipv6PingDest)
{
    if (gaPing6Table[i4Fsipv6PingIndex].i4RowStatus != PING6_STATUS_INVALID)
    {
        Ip6AddrCopy (&gaPing6Table[i4Fsipv6PingIndex].DestAddr,
                     (tIp6Addr *) (VOID *) pSetValFsipv6PingDest->
                     pu1_OctetList);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsipv6PingIfIndex
 Input       :  The Indices
                Fsipv6PingIndex

                The Object 
                setValFsipv6PingIfIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6PingIfIndex (INT4 i4Fsipv6PingIndex, INT4 i4SetValFsipv6PingIfIndex)
{
    if (gaPing6Table[i4Fsipv6PingIndex].i4RowStatus != PING6_STATUS_INVALID)
    {
        gaPing6Table[i4Fsipv6PingIndex].u4IfIndex =
            (UINT4) i4SetValFsipv6PingIfIndex;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsipv6PingAdminStatus
 Input       :  The Indices
                Fsipv6PingIndex

                The Object 
                setValFsipv6PingAdminStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6PingAdminStatus (INT4 i4Fsipv6PingIndex,
                             INT4 i4SetValFsipv6PingAdminStatus)
{
    t_PING6            *pPing6 = NULL;

    pPing6 = &gaPing6Table[i4Fsipv6PingIndex];
    pPing6->i4RowStatus = i4SetValFsipv6PingAdminStatus;

    if (i4SetValFsipv6PingAdminStatus == IP6_PING_ENABLE)
    {
        Ip6UnLock ();
        if (Ping6Send (pPing6) == OSIX_FAILURE)
        {
            /* If no valid Ping Instance exists, Close the Socket
             * Opened for sending Ping Request and receiving Ping replies */
            if (Ping6GetValidInstanceCount () == 0)
            {
                Ping6CloseSocket ();
            }
            Ip6Lock ();
            return SNMP_FAILURE;
        }
        Ip6Lock ();
    }
    else if (i4SetValFsipv6PingAdminStatus == IP6_PING_DISABLE)
    {
        pPing6->i1Oper = PING6_OPER_NOT_INITIATED;
        pPing6->u2SentCount = 0;
        pPing6->u2AvgTime = 0;
        pPing6->u2MaxTime = 0;
        pPing6->u2MinTime = 0;
        pPing6->u2Successes = 0;
    }
    else if (i4SetValFsipv6PingAdminStatus == IP6_PING_INVALID)
    {
        Ping6InitRec ((INT2) i4Fsipv6PingIndex);
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6PingInterval
 Input       :  The Indices
                Fsipv6PingIndex

                The Object 
                setValFsipv6PingInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6PingInterval (INT4 i4Fsipv6PingIndex,
                          INT4 i4SetValFsipv6PingInterval)
{
    gaPing6Table[i4Fsipv6PingIndex].u4Interval =
        (UINT2) i4SetValFsipv6PingInterval;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6PingRcvTimeout
 Input       :  The Indices
                Fsipv6PingIndex

                The Object 
                setValFsipv6PingRcvTimeout
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6PingRcvTimeout (INT4 i4Fsipv6PingIndex,
                            INT4 i4SetValFsipv6PingRcvTimeout)
{
    gaPing6Table[i4Fsipv6PingIndex].u2TimeOut =
        (UINT2) i4SetValFsipv6PingRcvTimeout;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6PingTries
 Input       :  The Indices
                Fsipv6PingIndex

                The Object 
                setValFsipv6PingTries
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6PingTries (INT4 i4Fsipv6PingIndex, INT4 i4SetValFsipv6PingTries)
{
    gaPing6Table[i4Fsipv6PingIndex].i2MaxTries = (INT2) i4SetValFsipv6PingTries;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6PingSize
 Input       :  The Indices
                Fsipv6PingIndex

                The Object 
                setValFsipv6PingSize
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6PingSize (INT4 i4Fsipv6PingIndex, INT4 i4SetValFsipv6PingSize)
{
    gaPing6Table[i4Fsipv6PingIndex].i2DataSize = (INT2) i4SetValFsipv6PingSize;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6PingData
 Input       :  The Indices
                Fsipv6PingIndex

                The Object 
                setValFsipv6PingData
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6PingData (INT4 i4Fsipv6PingIndex,
                      tSNMP_OCTET_STRING_TYPE * pSetValFsipv6PingData)
{

    SNPRINTF ((CHR1 *) & (gaPing6Table[i4Fsipv6PingIndex].PingData),
              PING6_CONFIG_DATA_SIZE, "%s", pSetValFsipv6PingData);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6PingSrcAddr
 Input       :  The Indices
                Fsipv6PingIndex

                The Object 
                setValFsipv6PingSrcAddr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6PingSrcAddr (INT4 i4Fsipv6PingIndex,
                         tSNMP_OCTET_STRING_TYPE * pSetValFsipv6PingSrcAddr)
{

    Ip6AddrCopy (&gaPing6Table[i4Fsipv6PingIndex].SrcAddr,
                 (tIp6Addr *) pSetValFsipv6PingSrcAddr);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Fsipv6PingDest
 Input       :  The Indices
                Fsipv6PingIndex

                The Object 
                testValFsipv6PingDest
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6PingDest (UINT4 *pu4ErrorCode, INT4 i4Fsipv6PingIndex,
                         tSNMP_OCTET_STRING_TYPE * pTestValFsipv6PingDest)
{
    UINT1               u1AddrType = 0;

    if (i4Fsipv6PingIndex >= PING6_MAX_INSTANCES || i4Fsipv6PingIndex < 0)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_PING_INDEX);
        return SNMP_FAILURE;
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

    if (pTestValFsipv6PingDest == NULL)
    {
        CLI_SET_ERR (CLI_IP6_INVALID_PING_DEST);
        return SNMP_FAILURE;
    }

    if (pTestValFsipv6PingDest->i4_Length != IP6_ADDR_SIZE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        CLI_SET_ERR (CLI_IP6_INVALID_PING_DEST);
        return SNMP_FAILURE;
    }

    u1AddrType =
        (UINT1) Ip6AddrType ((tIp6Addr *) (VOID *) pTestValFsipv6PingDest->
                             pu1_OctetList);
    if (u1AddrType == ADDR6_UNSPECIFIED)
    {
        CLI_SET_ERR (CLI_IP6_INVALID_ADDR_TYPE);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6PingIfIndex
 Input       :  The Indices
                Fsipv6PingIndex

                The Object 
                testValFsipv6PingIfIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6PingIfIndex (UINT4 *pu4ErrorCode, INT4 i4Fsipv6PingIndex,
                            INT4 i4TestValFsipv6PingIfIndex)
{
    if ((i4Fsipv6PingIndex >= PING6_MAX_INSTANCES) || (i4Fsipv6PingIndex < 0))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_PING_INDEX);
        return SNMP_FAILURE;
    }

    if (Lip6UtlIfEntryExists ((UINT4) i4TestValFsipv6PingIfIndex) ==
        OSIX_SUCCESS)
    {
        return SNMP_SUCCESS;
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6PingAdminStatus
 Input       :  The Indices
                Fsipv6PingIndex

                The Object 
                testValFsipv6PingAdminStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6PingAdminStatus (UINT4 *pu4ErrorCode, INT4 i4Fsipv6PingIndex,
                                INT4 i4TestValFsipv6PingAdminStatus)
{
    if ((i4Fsipv6PingIndex >= PING6_MAX_INSTANCES) || (i4Fsipv6PingIndex < 0))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_PING_INDEX);
        return SNMP_FAILURE;
    }
    if ((i4TestValFsipv6PingAdminStatus == PING6_STATUS_INVALID) ||
        (i4TestValFsipv6PingAdminStatus == PING6_STATUS_CREATE) ||
        (i4TestValFsipv6PingAdminStatus == PING6_STATUS_ENABLE) ||
        (i4TestValFsipv6PingAdminStatus == PING6_STATUS_DISABLE))
    {
        return SNMP_SUCCESS;
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    CLI_SET_ERR (CLI_IP6_INVALID_PING_ADMIN_STATUS);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6PingInterval
 Input       :  The Indices
                Fsipv6PingIndex

                The Object 
                testValFsipv6PingInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6PingInterval (UINT4 *pu4ErrorCode, INT4 i4Fsipv6PingIndex,
                             INT4 i4TestValFsipv6PingInterval)
{
    if ((i4Fsipv6PingIndex >= PING6_MAX_INSTANCES) || (i4Fsipv6PingIndex < 0))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (gaPing6Table[i4Fsipv6PingIndex].i4RowStatus != PING6_STATUS_INVALID)
    {
        if (gaPing6Table[i4Fsipv6PingIndex].i1Oper == PING6_OPER_IN_PROGRESS)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
    }
    if ((i4TestValFsipv6PingInterval >= PING6_MIN_INTERVAL) &&
        (i4TestValFsipv6PingInterval <= PING6_MAX_INTERVAL))
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6PingRcvTimeout
 Input       :  The Indices
                Fsipv6PingIndex

                The Object 
                testValFsipv6PingRcvTimeout
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6PingRcvTimeout (UINT4 *pu4ErrorCode, INT4 i4Fsipv6PingIndex,
                               INT4 i4TestValFsipv6PingRcvTimeout)
{
    if ((i4Fsipv6PingIndex >= PING6_MAX_INSTANCES) || (i4Fsipv6PingIndex < 0))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_PING_INDEX);
        return SNMP_FAILURE;
    }
    if (gaPing6Table[i4Fsipv6PingIndex].i4RowStatus != PING6_STATUS_INVALID)
    {
        if (gaPing6Table[i4Fsipv6PingIndex].i1Oper == PING6_OPER_IN_PROGRESS)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_IP6_PING_IN_PROGRESS);
            return SNMP_FAILURE;
        }
    }

    if ((i4TestValFsipv6PingRcvTimeout >= PING6_MIN_TIMEOUT &&
         i4TestValFsipv6PingRcvTimeout <= PING6_MAX_TIMEOUT) ||
        (i4TestValFsipv6PingRcvTimeout == 0))
    {
        return SNMP_SUCCESS;
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    CLI_SET_ERR (CLI_IP6_INVALID_PING_TIMEOUT);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6PingTries
 Input       :  The Indices
                Fsipv6PingIndex

                The Object 
                testValFsipv6PingTries
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6PingTries (UINT4 *pu4ErrorCode, INT4 i4Fsipv6PingIndex,
                          INT4 i4TestValFsipv6PingTries)
{
    if ((i4Fsipv6PingIndex >= PING6_MAX_INSTANCES) || (i4Fsipv6PingIndex < 0))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_PING_INDEX);
        return SNMP_FAILURE;
    }
    if (gaPing6Table[i4Fsipv6PingIndex].i4RowStatus != PING6_STATUS_INVALID)
    {
        if (gaPing6Table[i4Fsipv6PingIndex].i1Oper == PING6_OPER_IN_PROGRESS)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_IP6_PING_IN_PROGRESS);
            return SNMP_FAILURE;
        }
    }

    if (i4TestValFsipv6PingTries >= PING6_MIN_TRIES
        && i4TestValFsipv6PingTries <= PING6_MAX_TRIES)
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    CLI_SET_ERR (CLI_IP6_INVALID_PING_TRIES);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6PingSize
 Input       :  The Indices
                Fsipv6PingIndex

                The Object 
                testValFsipv6PingSize
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6PingSize (UINT4 *pu4ErrorCode, INT4 i4Fsipv6PingIndex,
                         INT4 i4TestValFsipv6PingSize)
{
    if ((i4Fsipv6PingIndex >= PING6_MAX_INSTANCES) || (i4Fsipv6PingIndex < 0))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_PING_INDEX);
        return SNMP_FAILURE;
    }
    if (gaPing6Table[i4Fsipv6PingIndex].i4RowStatus != PING6_STATUS_INVALID)
    {
        if (gaPing6Table[i4Fsipv6PingIndex].i1Oper == PING6_OPER_IN_PROGRESS)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_IP6_PING_IN_PROGRESS);
            return SNMP_FAILURE;
        }
    }

    if ((i4TestValFsipv6PingSize >= PING6_MIN_DATA_SIZE)
        && (i4TestValFsipv6PingSize <= PING6_MAX_DATA_SIZE))
    {
        return SNMP_SUCCESS;
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    CLI_SET_ERR (CLI_IP6_INVALID_PING_SIZE);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6PingData
 Input       :  The Indices
                Fsipv6PingIndex

                The Object 
                testValFsipv6PingData
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6PingData (UINT4 *pu4ErrorCode, INT4 i4Fsipv6PingIndex,
                         tSNMP_OCTET_STRING_TYPE * pTestValFsipv6PingData)
{
    INT4                i4Data = 0;
    if ((i4Fsipv6PingIndex >= PING6_MAX_INSTANCES) || (i4Fsipv6PingIndex < 0))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_PING_INDEX);
        return SNMP_FAILURE;
    }
    if (gaPing6Table[i4Fsipv6PingIndex].i4RowStatus != PING6_STATUS_INVALID)
    {
        if (gaPing6Table[i4Fsipv6PingIndex].i1Oper == PING6_OPER_IN_PROGRESS)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_IP6_PING_IN_PROGRESS);
            return SNMP_FAILURE;
        }
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

    if (pTestValFsipv6PingData == NULL)
    {
        CLI_SET_ERR (CLI_IP6_INVALID_PING_DATA);
        return SNMP_FAILURE;
    }

    if (pTestValFsipv6PingData->i4_Length != PING6_CONFIG_DATA_SIZE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        CLI_SET_ERR (CLI_IP6_INVALID_PING_DATA);
        return SNMP_FAILURE;
    }

    i4Data = CliHexStrToDecimal (pTestValFsipv6PingData->pu1_OctetList);
    if ((i4Data < 0) || (i4Data > PING6_MAX_DATA_VALUE))
    {
        CLI_SET_ERR (CLI_IP6_INVALID_PING_DATA);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6PingSrcAddr
 Input       :  The Indices
                Fsipv6PingIndex

                The Object 
                testValFsipv6PingSrcAddr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6PingSrcAddr (UINT4 *pu4ErrorCode, INT4 i4Fsipv6PingIndex,
                            tSNMP_OCTET_STRING_TYPE * pTestValFsipv6PingSrcAddr)
{
    UINT1               u1AddrType = 0;

    if ((i4Fsipv6PingIndex >= PING6_MAX_INSTANCES) || (i4Fsipv6PingIndex < 0))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_PING_INDEX);
        return SNMP_FAILURE;
    }
    if (gaPing6Table[i4Fsipv6PingIndex].i4RowStatus != PING6_STATUS_INVALID)
    {
        if (gaPing6Table[i4Fsipv6PingIndex].i1Oper == PING6_OPER_IN_PROGRESS)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_IP6_PING_IN_PROGRESS);
            return SNMP_FAILURE;
        }
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

    if (pTestValFsipv6PingSrcAddr == NULL)
    {
        CLI_SET_ERR (CLI_IP6_INVALID_PING_SOURCE);
        return SNMP_FAILURE;
    }

    if (pTestValFsipv6PingSrcAddr->i4_Length != IP6_ADDR_SIZE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        CLI_SET_ERR (CLI_IP6_INVALID_PING_SOURCE);
        return SNMP_FAILURE;
    }

    u1AddrType =
        (UINT1) Ip6AddrType ((tIp6Addr *) (VOID *) pTestValFsipv6PingSrcAddr->
                             pu1_OctetList);
    if (u1AddrType == ADDR6_UNSPECIFIED)
    {
        CLI_SET_ERR (CLI_IP6_INVALID_ADDR_TYPE);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Fsipv6PingTable
 Input       :  The Indices
                Fsipv6PingIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsipv6PingTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Fsipv6NDProxyListTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsipv6NDProxyListTable
 Input       :  The Indices
                Fsipv6NdProxyAddr
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 
     
     
     
     
     
     
     
    nmhValidateIndexInstanceFsipv6NDProxyListTable
    (tSNMP_OCTET_STRING_TYPE * pFsipv6NdProxyAddr)
{
    UNUSED_PARAM (pFsipv6NdProxyAddr);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsipv6NDProxyListTable
 Input       :  The Indices
                Fsipv6NdProxyAddr
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1 
     
     
     
     
     
     
     
    nmhGetFirstIndexFsipv6NDProxyListTable
    (tSNMP_OCTET_STRING_TYPE * pFsipv6NdProxyAddr)
{
    UNUSED_PARAM (pFsipv6NdProxyAddr);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsipv6NDProxyListTable
 Input       :  The Indices
                Fsipv6NdProxyAddr
                nextFsipv6NdProxyAddr
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsipv6NDProxyListTable (tSNMP_OCTET_STRING_TYPE *
                                       pFsipv6NdProxyAddr,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pNextFsipv6NdProxyAddr)
{
    UNUSED_PARAM (pFsipv6NdProxyAddr);
    UNUSED_PARAM (pNextFsipv6NdProxyAddr);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsipv6NdProxyAdminStatus
 Input       :  The Indices
                Fsipv6NdProxyAddr

                The Object 
                retValFsipv6NdProxyAdminStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6NdProxyAdminStatus (tSNMP_OCTET_STRING_TYPE * pFsipv6NdProxyAddr,
                                INT4 *pi4RetValFsipv6NdProxyAdminStatus)
{
    UNUSED_PARAM (pFsipv6NdProxyAddr);
    UNUSED_PARAM (pi4RetValFsipv6NdProxyAdminStatus);
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsipv6NdProxyAdminStatus
 Input       :  The Indices
                Fsipv6NdProxyAddr

                The Object 
                setValFsipv6NdProxyAdminStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6NdProxyAdminStatus (tSNMP_OCTET_STRING_TYPE * pFsipv6NdProxyAddr,
                                INT4 i4SetValFsipv6NdProxyAdminStatus)
{
    UNUSED_PARAM (pFsipv6NdProxyAddr);
    UNUSED_PARAM (i4SetValFsipv6NdProxyAdminStatus);

    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Fsipv6NdProxyAdminStatus
 Input       :  The Indices
                Fsipv6NdProxyAddr

                The Object 
                testValFsipv6NdProxyAdminStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6NdProxyAdminStatus (UINT4 *pu4ErrorCode,
                                   tSNMP_OCTET_STRING_TYPE * pFsipv6NdProxyAddr,
                                   INT4 i4TestValFsipv6NdProxyAdminStatus)
{
    UNUSED_PARAM (pFsipv6NdProxyAddr);
    UNUSED_PARAM (i4TestValFsipv6NdProxyAdminStatus);
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Fsipv6NDProxyListTable
 Input       :  The Indices
                Fsipv6NdProxyAddr
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsipv6NDProxyListTable (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsipv6IcmpInMsgs
 Input       :  The Indices

                The Object 
                retValFsipv6IcmpInMsgs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IcmpInMsgs (UINT4 *pu4RetValFsipv6IcmpInMsgs)
{
    UNUSED_PARAM (pu4RetValFsipv6IcmpInMsgs);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IcmpInErrors
 Input       :  The Indices

                The Object 
                retValFsipv6IcmpInErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IcmpInErrors (UINT4 *pu4RetValFsipv6IcmpInErrors)
{
    UNUSED_PARAM (pu4RetValFsipv6IcmpInErrors);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IcmpInDestUnreachs
 Input       :  The Indices

                The Object 
                retValFsipv6IcmpInDestUnreachs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IcmpInDestUnreachs (UINT4 *pu4RetValFsipv6IcmpInDestUnreachs)
{
    UNUSED_PARAM (pu4RetValFsipv6IcmpInDestUnreachs);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IcmpInTimeExcds
 Input       :  The Indices

                The Object 
                retValFsipv6IcmpInTimeExcds
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IcmpInTimeExcds (UINT4 *pu4RetValFsipv6IcmpInTimeExcds)
{
    UNUSED_PARAM (pu4RetValFsipv6IcmpInTimeExcds);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IcmpInParmProbs
 Input       :  The Indices

                The Object 
                retValFsipv6IcmpInParmProbs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IcmpInParmProbs (UINT4 *pu4RetValFsipv6IcmpInParmProbs)
{
    UNUSED_PARAM (pu4RetValFsipv6IcmpInParmProbs);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IcmpInPktTooBigs
 Input       :  The Indices

                The Object 
                retValFsipv6IcmpInPktTooBigs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IcmpInPktTooBigs (UINT4 *pu4RetValFsipv6IcmpInPktTooBigs)
{
    UNUSED_PARAM (pu4RetValFsipv6IcmpInPktTooBigs);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IcmpInEchos
 Input       :  The Indices

                The Object 
                retValFsipv6IcmpInEchos
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IcmpInEchos (UINT4 *pu4RetValFsipv6IcmpInEchos)
{
    UNUSED_PARAM (pu4RetValFsipv6IcmpInEchos);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IcmpInEchoReps
 Input       :  The Indices

                The Object 
                retValFsipv6IcmpInEchoReps
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IcmpInEchoReps (UINT4 *pu4RetValFsipv6IcmpInEchoReps)
{
    UNUSED_PARAM (pu4RetValFsipv6IcmpInEchoReps);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IcmpInRouterSolicits
 Input       :  The Indices

                The Object 
                retValFsipv6IcmpInRouterSolicits
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IcmpInRouterSolicits (UINT4 *pu4RetValFsipv6IcmpInRouterSolicits)
{
    UNUSED_PARAM (pu4RetValFsipv6IcmpInRouterSolicits);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IcmpInRouterAdvertisements
 Input       :  The Indices

                The Object 
                retValFsipv6IcmpInRouterAdvertisements
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IcmpInRouterAdvertisements (UINT4
                                        *pu4RetValFsipv6IcmpInRouterAdvertisements)
{
    UNUSED_PARAM (pu4RetValFsipv6IcmpInRouterAdvertisements);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IcmpInNeighborSolicits
 Input       :  The Indices

                The Object 
                retValFsipv6IcmpInNeighborSolicits
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetFsipv6IcmpInNeighborSolicits
    (UINT4 *pu4RetValFsipv6IcmpInNeighborSolicits)
{
    UNUSED_PARAM (pu4RetValFsipv6IcmpInNeighborSolicits);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IcmpInNeighborAdvertisements
 Input       :  The Indices

                The Object 
                retValFsipv6IcmpInNeighborAdvertisements
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetFsipv6IcmpInNeighborAdvertisements
    (UINT4 *pu4RetValFsipv6IcmpInNeighborAdvertisements)
{
    UNUSED_PARAM (pu4RetValFsipv6IcmpInNeighborAdvertisements);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IcmpInRedirects
 Input       :  The Indices

                The Object 
                retValFsipv6IcmpInRedirects
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IcmpInRedirects (UINT4 *pu4RetValFsipv6IcmpInRedirects)
{
    UNUSED_PARAM (pu4RetValFsipv6IcmpInRedirects);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IcmpInAdminProhib
 Input       :  The Indices

                The Object 
                retValFsipv6IcmpInAdminProhib
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IcmpInAdminProhib (UINT4 *pu4RetValFsipv6IcmpInAdminProhib)
{
    UNUSED_PARAM (pu4RetValFsipv6IcmpInAdminProhib);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IcmpOutMsgs
 Input       :  The Indices

                The Object 
                retValFsipv6IcmpOutMsgs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IcmpOutMsgs (UINT4 *pu4RetValFsipv6IcmpOutMsgs)
{
    UNUSED_PARAM (pu4RetValFsipv6IcmpOutMsgs);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IcmpOutErrors
 Input       :  The Indices

                The Object 
                retValFsipv6IcmpOutErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IcmpOutErrors (UINT4 *pu4RetValFsipv6IcmpOutErrors)
{
    UNUSED_PARAM (pu4RetValFsipv6IcmpOutErrors);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IcmpOutDestUnreachs
 Input       :  The Indices

                The Object 
                retValFsipv6IcmpOutDestUnreachs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IcmpOutDestUnreachs (UINT4 *pu4RetValFsipv6IcmpOutDestUnreachs)
{
    UNUSED_PARAM (pu4RetValFsipv6IcmpOutDestUnreachs);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IcmpOutTimeExcds
 Input       :  The Indices

                The Object 
                retValFsipv6IcmpOutTimeExcds
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IcmpOutTimeExcds (UINT4 *pu4RetValFsipv6IcmpOutTimeExcds)
{
    UNUSED_PARAM (pu4RetValFsipv6IcmpOutTimeExcds);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IcmpOutParmProbs
 Input       :  The Indices

                The Object 
                retValFsipv6IcmpOutParmProbs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IcmpOutParmProbs (UINT4 *pu4RetValFsipv6IcmpOutParmProbs)
{
    UNUSED_PARAM (pu4RetValFsipv6IcmpOutParmProbs);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IcmpOutPktTooBigs
 Input       :  The Indices

                The Object 
                retValFsipv6IcmpOutPktTooBigs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IcmpOutPktTooBigs (UINT4 *pu4RetValFsipv6IcmpOutPktTooBigs)
{
    UNUSED_PARAM (pu4RetValFsipv6IcmpOutPktTooBigs);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IcmpOutEchos
 Input       :  The Indices

                The Object 
                retValFsipv6IcmpOutEchos
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IcmpOutEchos (UINT4 *pu4RetValFsipv6IcmpOutEchos)
{
    UNUSED_PARAM (pu4RetValFsipv6IcmpOutEchos);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IcmpOutEchoReps
 Input       :  The Indices

                The Object 
                retValFsipv6IcmpOutEchoReps
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IcmpOutEchoReps (UINT4 *pu4RetValFsipv6IcmpOutEchoReps)
{
    UNUSED_PARAM (pu4RetValFsipv6IcmpOutEchoReps);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IcmpOutRouterSolicits
 Input       :  The Indices

                The Object 
                retValFsipv6IcmpOutRouterSolicits
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IcmpOutRouterSolicits (UINT4 *pu4RetValFsipv6IcmpOutRouterSolicits)
{
    UNUSED_PARAM (pu4RetValFsipv6IcmpOutRouterSolicits);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IcmpOutRouterAdvertisements
 Input       :  The Indices

                The Object 
                retValFsipv6IcmpOutRouterAdvertisements
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetFsipv6IcmpOutRouterAdvertisements
    (UINT4 *pu4RetValFsipv6IcmpOutRouterAdvertisements)
{
    UNUSED_PARAM (pu4RetValFsipv6IcmpOutRouterAdvertisements);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IcmpOutNeighborSolicits
 Input       :  The Indices

                The Object 
                retValFsipv6IcmpOutNeighborSolicits
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetFsipv6IcmpOutNeighborSolicits
    (UINT4 *pu4RetValFsipv6IcmpOutNeighborSolicits)
{
    UNUSED_PARAM (pu4RetValFsipv6IcmpOutNeighborSolicits);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IcmpOutNeighborAdvertisements
 Input       :  The Indices

                The Object 
                retValFsipv6IcmpOutNeighborAdvertisements
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetFsipv6IcmpOutNeighborAdvertisements
    (UINT4 *pu4RetValFsipv6IcmpOutNeighborAdvertisements)
{
    UNUSED_PARAM (pu4RetValFsipv6IcmpOutNeighborAdvertisements);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IcmpOutRedirects
 Input       :  The Indices

                The Object 
                retValFsipv6IcmpOutRedirects
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IcmpOutRedirects (UINT4 *pu4RetValFsipv6IcmpOutRedirects)
{
    UNUSED_PARAM (pu4RetValFsipv6IcmpOutRedirects);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IcmpOutAdminProhib
 Input       :  The Indices

                The Object 
                retValFsipv6IcmpOutAdminProhib
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IcmpOutAdminProhib (UINT4 *pu4RetValFsipv6IcmpOutAdminProhib)
{
    UNUSED_PARAM (pu4RetValFsipv6IcmpOutAdminProhib);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IcmpInBadCode
 Input       :  The Indices

                The Object 
                retValFsipv6IcmpInBadCode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IcmpInBadCode (UINT4 *pu4RetValFsipv6IcmpInBadCode)
{
    UNUSED_PARAM (pu4RetValFsipv6IcmpInBadCode);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IcmpInNARouterFlagSet
 Input       :  The Indices

                The Object 
                retValFsipv6IcmpInNARouterFlagSet
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IcmpInNARouterFlagSet (UINT4 *pu4RetValFsipv6IcmpInNARouterFlagSet)
{
    UNUSED_PARAM (pu4RetValFsipv6IcmpInNARouterFlagSet);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IcmpInNASolicitedFlagSet
 Input       :  The Indices

                The Object 
                retValFsipv6IcmpInNASolicitedFlagSet
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IcmpInNASolicitedFlagSet (UINT4
                                      *pu4RetValFsipv6IcmpInNASolicitedFlagSet)
{
    UNUSED_PARAM (pu4RetValFsipv6IcmpInNASolicitedFlagSet);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IcmpInNAOverrideFlagSet
 Input       :  The Indices

                The Object 
                retValFsipv6IcmpInNAOverrideFlagSet
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IcmpInNAOverrideFlagSet (UINT4
                                     *pu4RetValFsipv6IcmpInNAOverrideFlagSet)
{
    UNUSED_PARAM (pu4RetValFsipv6IcmpInNAOverrideFlagSet);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IcmpOutNARouterFlagSet
 Input       :  The Indices

                The Object 
                retValFsipv6IcmpOutNARouterFlagSet
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IcmpOutNARouterFlagSet (UINT4
                                    *pu4RetValFsipv6IcmpOutNARouterFlagSet)
{
    UNUSED_PARAM (pu4RetValFsipv6IcmpOutNARouterFlagSet);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IcmpOutNASolicitedFlagSet
 Input       :  The Indices

                The Object 
                retValFsipv6IcmpOutNASolicitedFlagSet
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IcmpOutNASolicitedFlagSet (UINT4
                                       *pu4RetValFsipv6IcmpOutNASolicitedFlagSet)
{
    UNUSED_PARAM (pu4RetValFsipv6IcmpOutNASolicitedFlagSet);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IcmpOutNAOverrideFlagSet
 Input       :  The Indices

                The Object 
                retValFsipv6IcmpOutNAOverrideFlagSet
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IcmpOutNAOverrideFlagSet (UINT4
                                      *pu4RetValFsipv6IcmpOutNAOverrideFlagSet)
{
    UNUSED_PARAM (pu4RetValFsipv6IcmpOutNAOverrideFlagSet);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsipv6UdpInDatagrams
 Input       :  The Indices

                The Object 
                retValFsipv6UdpInDatagrams
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6UdpInDatagrams (UINT4 *pu4RetValFsipv6UdpInDatagrams)
{
    UNUSED_PARAM (pu4RetValFsipv6UdpInDatagrams);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6UdpNoPorts
 Input       :  The Indices

                The Object 
                retValFsipv6UdpNoPorts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6UdpNoPorts (UINT4 *pu4RetValFsipv6UdpNoPorts)
{
    UNUSED_PARAM (pu4RetValFsipv6UdpNoPorts);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6UdpInErrors
 Input       :  The Indices

                The Object 
                retValFsipv6UdpInErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6UdpInErrors (UINT4 *pu4RetValFsipv6UdpInErrors)
{
    UNUSED_PARAM (pu4RetValFsipv6UdpInErrors);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6UdpOutDatagrams
 Input       :  The Indices

                The Object 
                retValFsipv6UdpOutDatagrams
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6UdpOutDatagrams (UINT4 *pu4RetValFsipv6UdpOutDatagrams)
{
    UNUSED_PARAM (pu4RetValFsipv6UdpOutDatagrams);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Fsipv6RouteTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsipv6RouteTable
 Input       :  The Indices
                Fsipv6RouteDest
                Fsipv6RoutePfxLength
                Fsipv6RouteProtocol
                Fsipv6RouteNextHop
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsipv6RouteTable (tSNMP_OCTET_STRING_TYPE *
                                          pFsipv6RouteDest,
                                          INT4 i4Fsipv6RoutePfxLength,
                                          INT4 i4Fsipv6RouteProtocol,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pFsipv6RouteNextHop)
{
    tIp6Addr            Ip6Addr;

    MEMSET (&Ip6Addr, IP6_ZERO, sizeof (tIp6Addr));
    UNUSED_PARAM (pFsipv6RouteNextHop);

    /* validate protocol id. */
    if ((i4Fsipv6RouteProtocol < IP6_OTHER_PROTOID) ||
        (i4Fsipv6RouteProtocol > IP6_IGRP_PROTOID))
    {
        /* Invalid Protocol Id */
        return SNMP_FAILURE;
    }

    /* Validate prefix and prefixlen. */
    Ip6AddrCopy (&Ip6Addr,
                 (tIp6Addr *) (VOID *) pFsipv6RouteDest->pu1_OctetList);

    if ((IS_ADDR_MULTI (Ip6Addr)) || (IS_ADDR_LLOCAL (Ip6Addr)) ||
        (i4Fsipv6RoutePfxLength < IP6_ADDR_MIN_PREFIX) ||
        (i4Fsipv6RoutePfxLength > IP6_ADDR_MAX_PREFIX))
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsipv6RouteTable
 Input       :  The Indices
                Fsipv6RouteDest
                Fsipv6RoutePfxLength
                Fsipv6RouteProtocol
                Fsipv6RouteNextHop
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsipv6RouteTable (tSNMP_OCTET_STRING_TYPE * pFsipv6RouteDest,
                                  INT4 *pi4Fsipv6RoutePfxLength,
                                  INT4 *pi4Fsipv6RouteProtocol,
                                  tSNMP_OCTET_STRING_TYPE * pFsipv6RouteNextHop)
{
    tNetIpv6RtInfo      NetIpv6RtInfo;
    tNetIpv6RtInfo      NetIpv6InvdRtInfo;
    UINT4               u4ContextId = IP6_ZERO;
    INT4                i4RetVal = 0;
    INT4                i4RetVal1 = 0;

    /* Get the first route from the TRIE. */
    MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));
    MEMSET (&NetIpv6InvdRtInfo, 0, sizeof (tNetIpv6RtInfo));
    u4ContextId = (UINT4) UtilRtm6GetCurrCxtId ();

    i4RetVal = Rtm6UtilGetFirstFwdTableRtEntry (u4ContextId, &NetIpv6RtInfo);
    i4RetVal1 = Rtm6UtilGetFirstInActiveTableRtEntry (&NetIpv6InvdRtInfo);
    if ((i4RetVal1 == RTM6_FAILURE) && (i4RetVal == RTM6_FAILURE))
    {
        /* No IPV6 Route present. */
        return SNMP_FAILURE;
    }

    if (i4RetVal == RTM6_FAILURE)
    {
        Rtm6UtilGetFirstInActiveTableRtEntry (&NetIpv6InvdRtInfo);
        *pi4Fsipv6RoutePfxLength = NetIpv6InvdRtInfo.u1Prefixlen;
        *pi4Fsipv6RouteProtocol = NetIpv6InvdRtInfo.i1Proto;
        Ip6AddrCopy ((tIp6Addr *) (VOID *) pFsipv6RouteDest->pu1_OctetList,
                     &NetIpv6InvdRtInfo.Ip6Dst);
        pFsipv6RouteDest->i4_Length = IP6_ADDR_SIZE;
        Ip6AddrCopy ((tIp6Addr *) (VOID *) pFsipv6RouteNextHop->pu1_OctetList,
                     &NetIpv6InvdRtInfo.NextHop);
        pFsipv6RouteNextHop->i4_Length = IP6_ADDR_SIZE;
        return SNMP_SUCCESS;
    }
    else if (i4RetVal1 == RTM6_FAILURE)
    {
        Rtm6UtilGetFirstFwdTableRtEntry (u4ContextId, &NetIpv6RtInfo);
    }
    else if ((i4RetVal1 == RTM6_SUCCESS) && (i4RetVal == RTM6_SUCCESS))
    {
        if (Rtm6UtilCheckIsRouteGreater (&NetIpv6RtInfo, &NetIpv6InvdRtInfo) ==
            RTM6_SUCCESS)
        {
            *pi4Fsipv6RoutePfxLength = NetIpv6InvdRtInfo.u1Prefixlen;
            *pi4Fsipv6RouteProtocol = NetIpv6InvdRtInfo.i1Proto;
            Ip6AddrCopy ((tIp6Addr *) (VOID *) pFsipv6RouteDest->pu1_OctetList,
                         &NetIpv6InvdRtInfo.Ip6Dst);
            pFsipv6RouteDest->i4_Length = IP6_ADDR_SIZE;
            Ip6AddrCopy ((tIp6Addr *) (VOID *) pFsipv6RouteNextHop->
                         pu1_OctetList, &NetIpv6InvdRtInfo.NextHop);
            pFsipv6RouteNextHop->i4_Length = IP6_ADDR_SIZE;
            return SNMP_SUCCESS;
        }
    }
    *pi4Fsipv6RoutePfxLength = NetIpv6RtInfo.u1Prefixlen;
    *pi4Fsipv6RouteProtocol = NetIpv6RtInfo.i1Proto;
    Ip6AddrCopy ((tIp6Addr *) (VOID *) pFsipv6RouteDest->pu1_OctetList,
                 &NetIpv6RtInfo.Ip6Dst);
    pFsipv6RouteDest->i4_Length = IP6_ADDR_SIZE;
    Ip6AddrCopy ((tIp6Addr *) (VOID *) pFsipv6RouteNextHop->pu1_OctetList,
                 &NetIpv6RtInfo.NextHop);
    pFsipv6RouteNextHop->i4_Length = IP6_ADDR_SIZE;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsipv6RouteTable
 Input       :  The Indices
                Fsipv6RouteDest
                nextFsipv6RouteDest
                Fsipv6RoutePfxLength
                nextFsipv6RoutePfxLength
                Fsipv6RouteProtocol
                nextFsipv6RouteProtocol
                Fsipv6RouteNextHop
                nextFsipv6RouteNextHop
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsipv6RouteTable (tSNMP_OCTET_STRING_TYPE * pFsipv6RouteDest,
                                 tSNMP_OCTET_STRING_TYPE * pNextFsipv6RouteDest,
                                 INT4 i4Fsipv6RoutePfxLength,
                                 INT4 *pi4NextFsipv6RoutePfxLength,
                                 INT4 i4Fsipv6RouteProtocol,
                                 INT4 *pi4NextFsipv6RouteProtocol,
                                 tSNMP_OCTET_STRING_TYPE * pFsipv6RouteNextHop,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pNextFsipv6RouteNextHop)
{
    tNetIpv6RtInfo      OutIpv6RtInfo1;
    tNetIpv6RtInfo      OutIpv6RtInfo2;
    static tNetIpv6RtInfo PrevIpv6RtInfo;
    UINT4               u4ContextId = IP6_ZERO;
    INT4                i4RetVal = RTM6_FAILURE;
    INT4                i4RetVal1 = RTM6_FAILURE;
    UINT1               au1RouteDest[MAX_ADDR_LEN];

    MEMSET (au1RouteDest, IP6_UINT1_ALL_ONE, MAX_ADDR_LEN);
    MEMSET (&OutIpv6RtInfo1, 0, sizeof (tNetIpv6RtInfo));
    MEMSET (&OutIpv6RtInfo2, 0, sizeof (tNetIpv6RtInfo));
    u4ContextId = (UINT4) UtilRtm6GetCurrCxtId ();

    /* Get the first route from the TRIE. */
    if (MEMCMP ((pFsipv6RouteDest->pu1_OctetList), au1RouteDest, MAX_ADDR_LEN)
        == 0)
    {
        MEMSET (&PrevIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));
        gu4Ip6PrevRouteInvalid = OSIX_FALSE;
        gi4Ip6ValidRouteRetVal = NETIPV6_SUCCESS;

        i4RetVal = Rtm6UtilGetNextFwdTableRtEntry (u4ContextId,
                                                   (tIp6Addr *) (VOID *)
                                                   pFsipv6RouteDest->
                                                   pu1_OctetList,
                                                   i4Fsipv6RoutePfxLength,
                                                   i4Fsipv6RouteProtocol,
                                                   (tIp6Addr *) (VOID *)
                                                   pFsipv6RouteNextHop->
                                                   pu1_OctetList,
                                                   &OutIpv6RtInfo1);
        i4RetVal1 =
            Rtm6UtilGetNextInActiveTableRtEntry ((tIp6Addr *) (VOID *)
                                                 pFsipv6RouteDest->
                                                 pu1_OctetList,
                                                 i4Fsipv6RoutePfxLength,
                                                 (tIp6Addr *) (VOID *)
                                                 pFsipv6RouteNextHop->
                                                 pu1_OctetList,
                                                 &OutIpv6RtInfo2);
    }
    else
    {
        if (gu4Ip6PrevRouteInvalid == OSIX_TRUE)
        {
            if (gi4Ip6ValidRouteRetVal == NETIPV4_SUCCESS)
            {
                OutIpv6RtInfo1.u1Prefixlen = PrevIpv6RtInfo.u1Prefixlen;
                OutIpv6RtInfo1.i1Proto = PrevIpv6RtInfo.i1Proto;
                OutIpv6RtInfo1.Ip6Dst = PrevIpv6RtInfo.Ip6Dst;
                OutIpv6RtInfo1.NextHop = PrevIpv6RtInfo.NextHop;

                i4RetVal = RTM6_SUCCESS;
                i4RetVal1 =
                    Rtm6UtilGetNextInActiveTableRtEntry ((tIp6Addr *) (VOID *)
                                                         pFsipv6RouteDest->
                                                         pu1_OctetList,
                                                         i4Fsipv6RoutePfxLength,
                                                         (tIp6Addr *) (VOID *)
                                                         pFsipv6RouteNextHop->
                                                         pu1_OctetList,
                                                         &OutIpv6RtInfo2);

            }
            else
            {
                i4RetVal = RTM6_FAILURE;
                i4RetVal1 =
                    Rtm6UtilGetNextInActiveTableRtEntry ((tIp6Addr *) (VOID *)
                                                         pFsipv6RouteDest->
                                                         pu1_OctetList,
                                                         i4Fsipv6RoutePfxLength,
                                                         (tIp6Addr *) (VOID *)
                                                         pFsipv6RouteNextHop->
                                                         pu1_OctetList,
                                                         &OutIpv6RtInfo2);
            }

        }
        else
        {
            i4RetVal = Rtm6UtilGetNextFwdTableRtEntry (u4ContextId,
                                                       (tIp6Addr *) (VOID *)
                                                       pFsipv6RouteDest->
                                                       pu1_OctetList,
                                                       i4Fsipv6RoutePfxLength,
                                                       i4Fsipv6RouteProtocol,
                                                       (tIp6Addr *) (VOID *)
                                                       pFsipv6RouteNextHop->
                                                       pu1_OctetList,
                                                       &OutIpv6RtInfo1);
            i4RetVal1 =
                Rtm6UtilGetNextInActiveTableRtEntry ((tIp6Addr *) (VOID *)
                                                     pFsipv6RouteDest->
                                                     pu1_OctetList,
                                                     i4Fsipv6RoutePfxLength,
                                                     (tIp6Addr *) (VOID *)
                                                     pFsipv6RouteNextHop->
                                                     pu1_OctetList,
                                                     &OutIpv6RtInfo2);
        }
    }

    if ((i4RetVal1 == RTM6_SUCCESS) && (i4RetVal == RTM6_SUCCESS))
    {
        if (Rtm6UtilCheckIsRouteGreater (&OutIpv6RtInfo1, &OutIpv6RtInfo2) ==
            RTM6_SUCCESS)
        {
            *pi4NextFsipv6RoutePfxLength = OutIpv6RtInfo2.u1Prefixlen;
            *pi4NextFsipv6RouteProtocol = OutIpv6RtInfo2.i1Proto;
            Ip6AddrCopy ((tIp6Addr *) (VOID *) pNextFsipv6RouteDest->
                         pu1_OctetList, &OutIpv6RtInfo2.Ip6Dst);
            pNextFsipv6RouteDest->i4_Length = IP6_ADDR_SIZE;
            Ip6AddrCopy ((tIp6Addr *) (VOID *) pNextFsipv6RouteNextHop->
                         pu1_OctetList, &OutIpv6RtInfo2.NextHop);
            pNextFsipv6RouteNextHop->i4_Length = IP6_ADDR_SIZE;

            PrevIpv6RtInfo.u1Prefixlen = OutIpv6RtInfo1.u1Prefixlen;
            PrevIpv6RtInfo.i1Proto = OutIpv6RtInfo1.i1Proto;
            PrevIpv6RtInfo.Ip6Dst = OutIpv6RtInfo1.Ip6Dst;
            PrevIpv6RtInfo.NextHop = OutIpv6RtInfo1.NextHop;

            gi4Ip6ValidRouteRetVal = NETIPV6_SUCCESS;
            gu4Ip6PrevRouteInvalid = OSIX_TRUE;
            return (SNMP_SUCCESS);
        }
        else
        {
            *pi4NextFsipv6RoutePfxLength = OutIpv6RtInfo1.u1Prefixlen;
            *pi4NextFsipv6RouteProtocol = OutIpv6RtInfo1.i1Proto;
            Ip6AddrCopy ((tIp6Addr *) (VOID *) pNextFsipv6RouteDest->
                         pu1_OctetList, &OutIpv6RtInfo1.Ip6Dst);
            pNextFsipv6RouteDest->i4_Length = IP6_ADDR_SIZE;
            Ip6AddrCopy ((tIp6Addr *) (VOID *) pNextFsipv6RouteNextHop->
                         pu1_OctetList, &OutIpv6RtInfo1.NextHop);
            pNextFsipv6RouteNextHop->i4_Length = IP6_ADDR_SIZE;
            gu4Ip6PrevRouteInvalid = OSIX_FALSE;
            return (SNMP_SUCCESS);
        }

    }
    if ((i4RetVal == NETIPV6_SUCCESS) && (i4RetVal1 == NETIPV6_FAILURE))
    {
        *pi4NextFsipv6RoutePfxLength = OutIpv6RtInfo1.u1Prefixlen;
        *pi4NextFsipv6RouteProtocol = OutIpv6RtInfo1.i1Proto;
        Ip6AddrCopy ((tIp6Addr *) (VOID *) pNextFsipv6RouteDest->pu1_OctetList,
                     &OutIpv6RtInfo1.Ip6Dst);
        pNextFsipv6RouteDest->i4_Length = IP6_ADDR_SIZE;
        Ip6AddrCopy ((tIp6Addr *) (VOID *) pNextFsipv6RouteNextHop->
                     pu1_OctetList, &OutIpv6RtInfo1.NextHop);
        pNextFsipv6RouteNextHop->i4_Length = IP6_ADDR_SIZE;
        gu4Ip6PrevRouteInvalid = OSIX_FALSE;
        return (SNMP_SUCCESS);
    }
    if ((i4RetVal == NETIPV6_FAILURE) && (i4RetVal1 == NETIPV6_SUCCESS))
    {
        *pi4NextFsipv6RoutePfxLength = OutIpv6RtInfo2.u1Prefixlen;
        *pi4NextFsipv6RouteProtocol = OutIpv6RtInfo2.i1Proto;
        Ip6AddrCopy ((tIp6Addr *) (VOID *) pNextFsipv6RouteDest->pu1_OctetList,
                     &OutIpv6RtInfo2.Ip6Dst);
        pNextFsipv6RouteDest->i4_Length = IP6_ADDR_SIZE;
        Ip6AddrCopy ((tIp6Addr *) (VOID *) pNextFsipv6RouteNextHop->
                     pu1_OctetList, &OutIpv6RtInfo2.NextHop);
        pNextFsipv6RouteNextHop->i4_Length = IP6_ADDR_SIZE;
        gi4Ip6ValidRouteRetVal = NETIPV6_FAILURE;
        gu4Ip6PrevRouteInvalid = OSIX_TRUE;
        return (SNMP_SUCCESS);
    }
    MEMSET (&PrevIpv6RtInfo, 0, sizeof (PrevIpv6RtInfo));
    gi4Ip6ValidRouteRetVal = NETIPV6_SUCCESS;
    gu4Ip6PrevRouteInvalid = OSIX_FALSE;
    return (SNMP_FAILURE);

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsipv6RouteIfIndex
 Input       :  The Indices
                Fsipv6RouteDest
                Fsipv6RoutePfxLength
                Fsipv6RouteProtocol
                Fsipv6RouteNextHop

                The Object 
                retValFsipv6RouteIfIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6RouteIfIndex (tSNMP_OCTET_STRING_TYPE * pFsipv6RouteDest,
                          INT4 i4Fsipv6RoutePfxLength,
                          INT4 i4Fsipv6RouteProtocol,
                          tSNMP_OCTET_STRING_TYPE * pFsipv6RouteNextHop,
                          INT4 *pi4RetValFsipv6RouteIfIndex)
{
    tIp6Addr            Ip6Addr;
    tIp6Addr            NextHop;
    tNetIpv6RtInfo      NetIpv6RtInfo;
    UINT4               u4ContextId = IP6_ZERO;

    MEMSET (&Ip6Addr, IP6_ZERO, sizeof (tIp6Addr));
    MEMSET (&NextHop, IP6_ZERO, sizeof (tIp6Addr));
    u4ContextId = (UINT4) UtilRtm6GetCurrCxtId ();
    Ip6AddrCopy (&Ip6Addr,
                 (tIp6Addr *) (VOID *) pFsipv6RouteDest->pu1_OctetList);
    Ip6AddrCopy (&NextHop,
                 (tIp6Addr *) (VOID *) pFsipv6RouteNextHop->pu1_OctetList);

    MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));
    if (Rtm6ApiFindRtEntryInCxt (u4ContextId, &Ip6Addr,
                                 (UINT1) i4Fsipv6RoutePfxLength,
                                 (INT1) i4Fsipv6RouteProtocol, &NextHop,
                                 &NetIpv6RtInfo) == RTM6_SUCCESS)
    {
        *pi4RetValFsipv6RouteIfIndex = (INT4) NetIpv6RtInfo.u4Index;
        return SNMP_SUCCESS;
    }
    else if (Rtm6UtilGetInActiveTableRtEntry (&Ip6Addr,
                                              i4Fsipv6RoutePfxLength,
                                              &NextHop,
                                              &NetIpv6RtInfo) == RTM6_SUCCESS)
    {
        *pi4RetValFsipv6RouteIfIndex = (INT4) NetIpv6RtInfo.u4Index;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsipv6RouteMetric
 Input       :  The Indices
                Fsipv6RouteDest
                Fsipv6RoutePfxLength
                Fsipv6RouteProtocol
                Fsipv6RouteNextHop

                The Object 
                retValFsipv6RouteMetric
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6RouteMetric (tSNMP_OCTET_STRING_TYPE * pFsipv6RouteDest,
                         INT4 i4Fsipv6RoutePfxLength,
                         INT4 i4Fsipv6RouteProtocol,
                         tSNMP_OCTET_STRING_TYPE * pFsipv6RouteNextHop,
                         UINT4 *pu4RetValFsipv6RouteMetric)
{
    tIp6Addr            Ip6Addr;
    tIp6Addr            NextHop;
    tNetIpv6RtInfo      NetIpv6RtInfo;
    UINT4               u4ContextId = IP6_ZERO;

    MEMSET (&Ip6Addr, IP6_ZERO, sizeof (tIp6Addr));
    MEMSET (&NextHop, IP6_ZERO, sizeof (tIp6Addr));
    u4ContextId = (UINT4) UtilRtm6GetCurrCxtId ();
    Ip6AddrCopy (&Ip6Addr,
                 (tIp6Addr *) (VOID *) pFsipv6RouteDest->pu1_OctetList);
    Ip6AddrCopy (&NextHop,
                 (tIp6Addr *) (VOID *) pFsipv6RouteNextHop->pu1_OctetList);

    MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));
    if (Rtm6ApiFindRtEntryInCxt (u4ContextId, &Ip6Addr,
                                 (UINT1) i4Fsipv6RoutePfxLength,
                                 (INT1) i4Fsipv6RouteProtocol, &NextHop,
                                 &NetIpv6RtInfo) == RTM6_SUCCESS)
    {
        *pu4RetValFsipv6RouteMetric = NetIpv6RtInfo.u4Metric;
        return SNMP_SUCCESS;
    }
    else if (Rtm6UtilGetInActiveTableRtEntry (&Ip6Addr,
                                              i4Fsipv6RoutePfxLength,
                                              &NextHop,
                                              &NetIpv6RtInfo) == RTM6_SUCCESS)
    {
        *pu4RetValFsipv6RouteMetric = NetIpv6RtInfo.u4Metric;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsipv6RouteType
 Input       :  The Indices
                Fsipv6RouteDest
                Fsipv6RoutePfxLength
                Fsipv6RouteProtocol
                Fsipv6RouteNextHop

                The Object 
                retValFsipv6RouteType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6RouteType (tSNMP_OCTET_STRING_TYPE * pFsipv6RouteDest,
                       INT4 i4Fsipv6RoutePfxLength,
                       INT4 i4Fsipv6RouteProtocol,
                       tSNMP_OCTET_STRING_TYPE * pFsipv6RouteNextHop,
                       INT4 *pi4RetValFsipv6RouteType)
{
    tIp6Addr            Ip6Addr;
    tIp6Addr            NextHop;
    tNetIpv6RtInfo      NetIpv6RtInfo;
    UINT4               u4ContextId = IP6_ZERO;

    MEMSET (&Ip6Addr, IP6_ZERO, sizeof (tIp6Addr));
    MEMSET (&NextHop, IP6_ZERO, sizeof (tIp6Addr));
    u4ContextId = (UINT4) UtilRtm6GetCurrCxtId ();
    Ip6AddrCopy (&Ip6Addr,
                 (tIp6Addr *) (VOID *) pFsipv6RouteDest->pu1_OctetList);
    Ip6AddrCopy (&NextHop,
                 (tIp6Addr *) (VOID *) pFsipv6RouteNextHop->pu1_OctetList);

    MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));
    if (Rtm6ApiFindRtEntryInCxt (u4ContextId, &Ip6Addr,
                                 (UINT1) i4Fsipv6RoutePfxLength,
                                 (INT1) i4Fsipv6RouteProtocol, &NextHop,
                                 &NetIpv6RtInfo) == RTM6_SUCCESS)
    {
        *pi4RetValFsipv6RouteType = (INT4) NetIpv6RtInfo.i1Type;
        return SNMP_SUCCESS;
    }
    else if (Rtm6UtilGetInActiveTableRtEntry (&Ip6Addr,
                                              i4Fsipv6RoutePfxLength,
                                              &NextHop,
                                              &NetIpv6RtInfo) == RTM6_SUCCESS)
    {
        *pi4RetValFsipv6RouteType = (INT4) NetIpv6RtInfo.i1Type;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsipv6RouteTag
 Input       :  The Indices
                Fsipv6RouteDest
                Fsipv6RoutePfxLength
                Fsipv6RouteProtocol
                Fsipv6RouteNextHop

                The Object 
                retValFsipv6RouteTag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6RouteTag (tSNMP_OCTET_STRING_TYPE * pFsipv6RouteDest,
                      INT4 i4Fsipv6RoutePfxLength,
                      INT4 i4Fsipv6RouteProtocol,
                      tSNMP_OCTET_STRING_TYPE * pFsipv6RouteNextHop,
                      UINT4 *pu4RetValFsipv6RouteTag)
{
    tIp6Addr            Ip6Addr;
    tIp6Addr            NextHop;
    tNetIpv6RtInfo      NetIpv6RtInfo;
    UINT4               u4ContextId = IP6_ZERO;

    MEMSET (&Ip6Addr, IP6_ZERO, sizeof (tIp6Addr));
    MEMSET (&NextHop, IP6_ZERO, sizeof (tIp6Addr));
    u4ContextId = (UINT4) UtilRtm6GetCurrCxtId ();
    Ip6AddrCopy (&Ip6Addr,
                 (tIp6Addr *) (VOID *) pFsipv6RouteDest->pu1_OctetList);
    Ip6AddrCopy (&NextHop,
                 (tIp6Addr *) (VOID *) pFsipv6RouteNextHop->pu1_OctetList);

    MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));
    if (Rtm6ApiFindRtEntryInCxt (u4ContextId,
                                 &Ip6Addr, (UINT1) i4Fsipv6RoutePfxLength,
                                 (INT1) i4Fsipv6RouteProtocol, &NextHop,
                                 &NetIpv6RtInfo) == RTM6_SUCCESS)
    {
        *pu4RetValFsipv6RouteTag = NetIpv6RtInfo.u4RouteTag;
        return SNMP_SUCCESS;
    }
    else if (Rtm6UtilGetInActiveTableRtEntry (&Ip6Addr,
                                              i4Fsipv6RoutePfxLength,
                                              &NextHop,
                                              &NetIpv6RtInfo) == RTM6_SUCCESS)
    {
        *pu4RetValFsipv6RouteTag = NetIpv6RtInfo.u4RouteTag;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsipv6RouteAge
 Input       :  The Indices
                Fsipv6RouteDest
                Fsipv6RoutePfxLength
                Fsipv6RouteProtocol
                Fsipv6RouteNextHop

                The Object 
                retValFsipv6RouteAge
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6RouteAge (tSNMP_OCTET_STRING_TYPE * pFsipv6RouteDest,
                      INT4 i4Fsipv6RoutePfxLength, INT4 i4Fsipv6RouteProtocol,
                      tSNMP_OCTET_STRING_TYPE * pFsipv6RouteNextHop,
                      INT4 *pi4RetValFsipv6RouteAge)
{
    tIp6Addr            Ip6Addr;
    tIp6Addr            NextHop;
    UINT4               u4SysTime;
    tNetIpv6RtInfo      NetIpv6RtInfo;
    UINT4               u4ContextId = IP6_ZERO;

    MEMSET (&Ip6Addr, IP6_ZERO, sizeof (tIp6Addr));
    MEMSET (&NextHop, IP6_ZERO, sizeof (tIp6Addr));
    MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));
    u4ContextId = (UINT4) UtilRtm6GetCurrCxtId ();
    Ip6AddrCopy (&Ip6Addr,
                 (tIp6Addr *) (VOID *) pFsipv6RouteDest->pu1_OctetList);
    Ip6AddrCopy (&NextHop,
                 (tIp6Addr *) (VOID *) pFsipv6RouteNextHop->pu1_OctetList);

    if (Rtm6ApiFindRtEntryInCxt (u4ContextId,
                                 &Ip6Addr, (UINT1) i4Fsipv6RoutePfxLength,
                                 (INT1) i4Fsipv6RouteProtocol, &NextHop,
                                 &NetIpv6RtInfo) == RTM6_SUCCESS)
    {
        if (NetIpv6RtInfo.i1Proto == IP6_NETMGMT_PROTOID ||
            NetIpv6RtInfo.i1Proto == IP6_LOCAL_PROTOID)
        {
            *pi4RetValFsipv6RouteAge = 0;
            return SNMP_SUCCESS;
        }

        u4SysTime = OsixGetSysUpTime ();
        *pi4RetValFsipv6RouteAge =
            (INT4) (u4SysTime - NetIpv6RtInfo.u4ChangeTime);
        return SNMP_SUCCESS;

    }
    else if (Rtm6UtilGetInActiveTableRtEntry (&Ip6Addr,
                                              i4Fsipv6RoutePfxLength,
                                              &NextHop,
                                              &NetIpv6RtInfo) == RTM6_SUCCESS)
    {
        if (NetIpv6RtInfo.i1Proto == IP6_NETMGMT_PROTOID ||
            NetIpv6RtInfo.i1Proto == IP6_LOCAL_PROTOID)
        {
            *pi4RetValFsipv6RouteAge = 0;
            return SNMP_SUCCESS;
        }

        u4SysTime = OsixGetSysUpTime ();
        *pi4RetValFsipv6RouteAge =
            (INT4) (u4SysTime - NetIpv6RtInfo.u4ChangeTime);

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsipv6RouteAdminStatus
 Input       :  The Indices
                Fsipv6RouteDest
                Fsipv6RoutePfxLength
                Fsipv6RouteProtocol
                Fsipv6RouteNextHop

                The Object 
                retValFsipv6RouteAdminStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6RouteAdminStatus (tSNMP_OCTET_STRING_TYPE * pFsipv6RouteDest,
                              INT4 i4Fsipv6RoutePfxLength,
                              INT4 i4Fsipv6RouteProtocol,
                              tSNMP_OCTET_STRING_TYPE * pFsipv6RouteNextHop,
                              INT4 *pi4RetValFsipv6RouteAdminStatus)
{
    tIp6Addr            Ip6Addr;
    tIp6Addr            NextHop;
    tNetIpv6RtInfo      NetIpv6RtInfo;
    UINT4               u4ContextId = IP6_ZERO;

    MEMSET (&Ip6Addr, IP6_ZERO, sizeof (tIp6Addr));
    MEMSET (&NextHop, IP6_ZERO, sizeof (tIp6Addr));
    MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));
    u4ContextId = (UINT4) UtilRtm6GetCurrCxtId ();
    Ip6AddrCopy (&Ip6Addr,
                 (tIp6Addr *) (VOID *) pFsipv6RouteDest->pu1_OctetList);
    Ip6AddrCopy (&NextHop,
                 (tIp6Addr *) (VOID *) pFsipv6RouteNextHop->pu1_OctetList);

    if (Rtm6ApiFindRtEntryInCxt (u4ContextId,
                                 &Ip6Addr, (UINT1) i4Fsipv6RoutePfxLength,
                                 (INT1) i4Fsipv6RouteProtocol, &NextHop,
                                 &NetIpv6RtInfo) == RTM6_SUCCESS)
    {
        *pi4RetValFsipv6RouteAdminStatus = (INT4) NetIpv6RtInfo.u4RowStatus;
        return SNMP_SUCCESS;
    }
    else if (Rtm6UtilGetInActiveTableRtEntry (&Ip6Addr,
                                              i4Fsipv6RoutePfxLength,
                                              &NextHop,
                                              &NetIpv6RtInfo) == RTM6_SUCCESS)
    {
        *pi4RetValFsipv6RouteAdminStatus = (INT4) NetIpv6RtInfo.u4RowStatus;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsipv6RoutePreference
 Input       :  The Indices
                Fsipv6RouteDest
                Fsipv6RoutePfxLength
                Fsipv6RouteProtocol
                Fsipv6RouteNextHop

                The Object
                retValFsipv6RoutePreference
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6RoutePreference (tSNMP_OCTET_STRING_TYPE * pFsipv6RouteDest,
                             INT4 i4Fsipv6RoutePfxLength,
                             INT4 i4Fsipv6RouteProtocol,
                             tSNMP_OCTET_STRING_TYPE * pFsipv6RouteNextHop,
                             INT4 *pi4RetValFsipv6RoutePreference)
{
    tIp6Addr            Ip6Addr;
    tIp6Addr            NextHop;
    tNetIpv6RtInfo      NetIpv6RtInfo;
    UINT4               u4ContextId = IP6_ZERO;

    MEMSET (&Ip6Addr, IP6_ZERO, sizeof (tIp6Addr));
    MEMSET (&NextHop, IP6_ZERO, sizeof (tIp6Addr));
    u4ContextId = (UINT4) UtilRtm6GetCurrCxtId ();
    Ip6AddrCopy (&Ip6Addr,
                 (tIp6Addr *) (VOID *) pFsipv6RouteDest->pu1_OctetList);
    Ip6AddrCopy (&NextHop,
                 (tIp6Addr *) (VOID *) pFsipv6RouteNextHop->pu1_OctetList);

    MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));
    if (Rtm6ApiFindRtEntryInCxt (u4ContextId, &Ip6Addr,
                                 (UINT1) i4Fsipv6RoutePfxLength,
                                 (INT1) i4Fsipv6RouteProtocol, &NextHop,
                                 &NetIpv6RtInfo) == RTM6_SUCCESS)
    {
        *pi4RetValFsipv6RoutePreference = (INT4) NetIpv6RtInfo.u1Preference;
        return SNMP_SUCCESS;
    }
    else if (Rtm6UtilGetInActiveTableRtEntry (&Ip6Addr,
                                              i4Fsipv6RoutePfxLength,
                                              &NextHop,
                                              &NetIpv6RtInfo) == RTM6_SUCCESS)
    {
        *pi4RetValFsipv6RoutePreference = (INT4) NetIpv6RtInfo.u1Preference;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsipv6RouteIfIndex
 Input       :  The Indices
                Fsipv6RouteDest
                Fsipv6RoutePfxLength
                Fsipv6RouteProtocol
                Fsipv6RouteNextHop

                The Object 
                setValFsipv6RouteIfIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6RouteIfIndex (tSNMP_OCTET_STRING_TYPE * pFsipv6RouteDest,
                          INT4 i4Fsipv6RoutePfxLength,
                          INT4 i4Fsipv6RouteProtocol,
                          tSNMP_OCTET_STRING_TYPE * pFsipv6RouteNextHop,
                          INT4 i4SetValFsipv6RouteIfIndex)
{
    tIp6Addr            Ip6Addr;
    tIp6Addr            NextHop;
    UINT4               u4ContextId = VCM_DEFAULT_CONTEXT;
    tNetIpv6RtInfo      NetIp6RtInfo;
    tNetIpv6RtInfoQueryMsg RtQuery;

    MEMSET (&Ip6Addr, IP6_ZERO, sizeof (tIp6Addr));
    MEMSET (&NextHop, IP6_ZERO, sizeof (tIp6Addr));
    Ip6AddrCopy (&Ip6Addr,
                 (tIp6Addr *) (VOID *) pFsipv6RouteDest->pu1_OctetList);
    Ip6AddrCopy (&NextHop,
                 (tIp6Addr *) (VOID *) pFsipv6RouteNextHop->pu1_OctetList);

    MEMSET (&NetIp6RtInfo, 0, sizeof (tNetIpv6RtInfo));
    MEMSET (&RtQuery, 0, sizeof (tNetIpv6RtInfoQueryMsg));

    u4ContextId = (UINT4) UtilRtm6GetCurrCxtId ();
    RtQuery.u4ContextId = u4ContextId;
    Ip6AddrCopy (&NetIp6RtInfo.Ip6Dst, &NextHop);
    NetIp6RtInfo.u1Prefixlen = IP6_ADDR_MAX_PREFIX;
    RTM6_TASK_UNLOCK ();
    if (NetIpv6GetRoute (&RtQuery, &NetIp6RtInfo) != NETIPV6_FAILURE)
    {
        if (NetIp6RtInfo.u4Index != (UINT4) i4SetValFsipv6RouteIfIndex)
        {
            RTM6_TASK_LOCK ();
            CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE_FOR_NEXTHOP);
            return SNMP_FAILURE;
        }
    }

    RTM6_TASK_LOCK ();

    if (Rtm6ApiSetRtParamsInCxt (u4ContextId,
                                 &Ip6Addr, (UINT1) i4Fsipv6RoutePfxLength,
                                 (INT1) i4Fsipv6RouteProtocol, &NextHop,
                                 RTM6_ROUTE_INDEX,
                                 (UINT4) i4SetValFsipv6RouteIfIndex) ==
        RTM6_SUCCESS)
    {
        IncMsrForIpv6RouteTable (u4ContextId, pFsipv6RouteDest,
                                 i4Fsipv6RoutePfxLength,
                                 pFsipv6RouteNextHop, 'i',
                                 &i4SetValFsipv6RouteIfIndex,
                                 FsMIStdInetCidrRouteIfIndex,
                                 sizeof (FsMIStdInetCidrRouteIfIndex) /
                                 sizeof (UINT4), FALSE);

        return SNMP_SUCCESS;
    }
    else
    {
        MEMSET (&NetIp6RtInfo, 0, sizeof (tNetIpv6RtInfo));
        NetIp6RtInfo.u4Index = (UINT4) i4SetValFsipv6RouteIfIndex;
        NetIp6RtInfo.u4RowStatus = IP6FWD_NOT_IN_SERVICE;
        if (Rtm6UpdateRouteInInvdRouteList (&Ip6Addr, i4Fsipv6RoutePfxLength,
                                            &NextHop,
                                            &NetIp6RtInfo) != RTM6_FAILURE)
        {
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsipv6RouteMetric
 Input       :  The Indices
                Fsipv6RouteDest
                Fsipv6RoutePfxLength
                Fsipv6RouteProtocol
                Fsipv6RouteNextHop

                The Object 
                setValFsipv6RouteMetric
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6RouteMetric (tSNMP_OCTET_STRING_TYPE * pFsipv6RouteDest,
                         INT4 i4Fsipv6RoutePfxLength,
                         INT4 i4Fsipv6RouteProtocol,
                         tSNMP_OCTET_STRING_TYPE * pFsipv6RouteNextHop,
                         UINT4 u4SetValFsipv6RouteMetric)
{
    tIp6Addr            Ip6Addr;
    tIp6Addr            NextHop;
    tNetIpv6RtInfo      NetIpv6RtInfo;
    UINT4               u4ContextId = IP6_ZERO;

    u4ContextId = (UINT4) UtilRtm6GetCurrCxtId ();
    MEMSET (&Ip6Addr, IP6_ZERO, sizeof (tIp6Addr));
    MEMSET (&NextHop, IP6_ZERO, sizeof (tIp6Addr));
    MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));
    Ip6AddrCopy (&Ip6Addr,
                 (tIp6Addr *) (VOID *) pFsipv6RouteDest->pu1_OctetList);
    Ip6AddrCopy (&NextHop,
                 (tIp6Addr *) (VOID *) pFsipv6RouteNextHop->pu1_OctetList);

    if (Rtm6ApiFindRtEntryInCxt (u4ContextId, &Ip6Addr,
                                 (UINT1) i4Fsipv6RoutePfxLength,
                                 (INT1) i4Fsipv6RouteProtocol, &NextHop,
                                 &NetIpv6RtInfo) == RTM6_SUCCESS)
    {
        NetIpv6RtInfo.u4Metric = u4SetValFsipv6RouteMetric;
        /* For Metric dont just update the metric Value. Because, the route
         * will be getting re-ordered as per the new metric Value. */
        if (Rtm6Ipv6LeakRoute (NETIPV6_ADD_ROUTE, &NetIpv6RtInfo) ==
            RTM6_FAILURE)
        {
            return SNMP_FAILURE;
        }
        IncMsrForIpv6RouteTable (u4ContextId, pFsipv6RouteDest,
                                 i4Fsipv6RoutePfxLength,
                                 pFsipv6RouteNextHop,
                                 'u', &u4SetValFsipv6RouteMetric,
                                 FsMIStdInetCidrRouteMetric1,
                                 sizeof (FsMIStdInetCidrRouteMetric1) /
                                 sizeof (UINT4), FALSE);
        return SNMP_SUCCESS;
    }
    else
    {
        MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));
        NetIpv6RtInfo.u4Metric = u4SetValFsipv6RouteMetric;
        if (Rtm6UpdateRouteInInvdRouteList (&Ip6Addr, i4Fsipv6RoutePfxLength,
                                            &NextHop,
                                            &NetIpv6RtInfo) != RTM6_FAILURE)
        {
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsipv6RouteType
 Input       :  The Indices
                Fsipv6RouteDest
                Fsipv6RoutePfxLength
                Fsipv6RouteProtocol
                Fsipv6RouteNextHop

                The Object 
                setValFsipv6RouteType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6RouteType (tSNMP_OCTET_STRING_TYPE * pFsipv6RouteDest,
                       INT4 i4Fsipv6RoutePfxLength,
                       INT4 i4Fsipv6RouteProtocol,
                       tSNMP_OCTET_STRING_TYPE * pFsipv6RouteNextHop,
                       INT4 i4SetValFsipv6RouteType)
{
    tIp6Addr            dest;
    tIp6Addr            NextHop;
    UINT4               u4ContextId = IP6_ZERO;
    tNetIpv6RtInfo      NetIpv6RtInfo;

    u4ContextId = (UINT4) UtilRtm6GetCurrCxtId ();
    MEMSET (&dest, IP6_ZERO, sizeof (tIp6Addr));
    MEMSET (&NextHop, IP6_ZERO, sizeof (tIp6Addr));
    MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));
    Ip6AddrCopy (&NextHop,
                 (tIp6Addr *) (VOID *) pFsipv6RouteNextHop->pu1_OctetList);
    Ip6AddrCopy (&dest, (tIp6Addr *) (VOID *) pFsipv6RouteDest->pu1_OctetList);

    if (Rtm6ApiSetRtParamsInCxt (u4ContextId,
                                 &dest, (UINT1) i4Fsipv6RoutePfxLength,
                                 (INT1) i4Fsipv6RouteProtocol, &NextHop,
                                 RTM6_ROUTE_TYPE,
                                 (UINT4) i4SetValFsipv6RouteType) ==
        RTM6_SUCCESS)
    {
        IncMsrForIpv6RouteTable (u4ContextId, pFsipv6RouteDest,
                                 i4Fsipv6RoutePfxLength,
                                 pFsipv6RouteNextHop,
                                 'i', &i4SetValFsipv6RouteType,
                                 FsMIStdInetCidrRouteType,
                                 sizeof (FsMIStdInetCidrRouteType) /
                                 sizeof (UINT4), FALSE);
        return SNMP_SUCCESS;
    }
    else
    {
        MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));
        NetIpv6RtInfo.i1Type = (INT1) i4SetValFsipv6RouteType;
        if (Rtm6UpdateRouteInInvdRouteList (&dest, i4Fsipv6RoutePfxLength,
                                            &NextHop, &NetIpv6RtInfo) !=
            RTM6_FAILURE)
        {
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsipv6RouteTag
 Input       :  The Indices
                Fsipv6RouteDest
                Fsipv6RoutePfxLength
                Fsipv6RouteProtocol
                Fsipv6RouteNextHop

                The Object 
                setValFsipv6RouteTag
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6RouteTag (tSNMP_OCTET_STRING_TYPE * pFsipv6RouteDest,
                      INT4 i4Fsipv6RoutePfxLength,
                      INT4 i4Fsipv6RouteProtocol,
                      tSNMP_OCTET_STRING_TYPE * pFsipv6RouteNextHop,
                      UINT4 u4SetValFsipv6RouteTag)
{

    tIp6Addr            Ip6Addr;
    tIp6Addr            NextHop;
    tNetIpv6RtInfo      NetIp6RtInfo;
    UINT4               u4ContextId = IP6_ZERO;

    u4ContextId = (UINT4) UtilRtm6GetCurrCxtId ();
    MEMSET (&Ip6Addr, IP6_ZERO, sizeof (tIp6Addr));
    MEMSET (&NextHop, IP6_ZERO, sizeof (tIp6Addr));
    MEMSET (&NetIp6RtInfo, 0, sizeof (tNetIpv6RtInfo));
    Ip6AddrCopy (&Ip6Addr,
                 (tIp6Addr *) (VOID *) pFsipv6RouteDest->pu1_OctetList);
    Ip6AddrCopy (&NextHop,
                 (tIp6Addr *) (VOID *) pFsipv6RouteNextHop->pu1_OctetList);

    if (Rtm6ApiSetRtParamsInCxt (u4ContextId,
                                 &Ip6Addr, (UINT1) i4Fsipv6RoutePfxLength,
                                 (INT1) i4Fsipv6RouteProtocol, &NextHop,
                                 RTM6_ROUTE_TAG, u4SetValFsipv6RouteTag)
        == RTM6_SUCCESS)
    {
        IncMsrForIpv6RouteTable (u4ContextId, pFsipv6RouteDest,
                                 i4Fsipv6RoutePfxLength,
                                 pFsipv6RouteNextHop,
                                 'u', &u4SetValFsipv6RouteTag,
                                 FsMIStdInetCidrRouteNextHopAS,
                                 sizeof (FsMIStdInetCidrRouteNextHopAS) /
                                 sizeof (UINT4), FALSE);
        return SNMP_SUCCESS;
    }
    else
    {
        MEMSET (&NetIp6RtInfo, 0, sizeof (tNetIpv6RtInfo));
        NetIp6RtInfo.u4RouteTag = u4SetValFsipv6RouteTag;
        if (Rtm6UpdateRouteInInvdRouteList (&Ip6Addr, i4Fsipv6RoutePfxLength,
                                            &NextHop,
                                            &NetIp6RtInfo) != RTM6_FAILURE)
        {
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsipv6RouteAdminStatus
 Input       :  The Indices
                Fsipv6RouteDest
                Fsipv6RoutePfxLength
                Fsipv6RouteProtocol
                Fsipv6RouteNextHop

                The Object 
                setValFsipv6RouteAdminStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6RouteAdminStatus (tSNMP_OCTET_STRING_TYPE * pFsipv6RouteDest,
                              INT4 i4Fsipv6RoutePfxLength,
                              INT4 i4Fsipv6RouteProtocol,
                              tSNMP_OCTET_STRING_TYPE * pFsipv6RouteNextHop,
                              INT4 i4SetValFsipv6RouteAdminStatus)
{
    tLip6If            *pIf6Entry = NULL;
    tNetIpv6RtInfo      NetIpv6RtInfo;
    tNetIpv6RtInfo      NetIp6TempRtInfo;
    tIp6Addr            Ip6Addr;
    tIp6Addr            NextHop;
    INT1                i1Type;
    UINT4               u4Index = 0;
    tNetIpv6RtInfo      NetIp6RtInfo;
    tNetIpv6RtInfoQueryMsg RtQuery;
    UINT4               u4ContextId = IP6_ZERO;
    INT4                i4RetVal = RTM6_FAILURE;
    UINT4               u4Preference = IP6_ZERO;

    u4ContextId = (UINT4) UtilRtm6GetCurrCxtId ();

    MEMSET (&NetIp6RtInfo, IP6_ZERO, sizeof (tNetIpv6RtInfo));
    MEMSET (&RtQuery, IP6_ZERO, sizeof (tNetIpv6RtInfoQueryMsg));
    MEMSET (&Ip6Addr, IP6_ZERO, sizeof (tIp6Addr));
    MEMSET (&NextHop, IP6_ZERO, sizeof (tIp6Addr));
    MEMSET (&NetIpv6RtInfo, IP6_ZERO, sizeof (tNetIpv6RtInfo));
    Ip6AddrCopy (&Ip6Addr,
                 (tIp6Addr *) (VOID *) pFsipv6RouteDest->pu1_OctetList);
    Ip6AddrCopy (&NextHop,
                 (tIp6Addr *) (VOID *) pFsipv6RouteNextHop->pu1_OctetList);

    switch (i4SetValFsipv6RouteAdminStatus)
    {
        case IP6FWD_CREATE_AND_WAIT:
            /* Create the Route with the Default Values. */
            if (IS_ADDR_UNSPECIFIED (NextHop))
            {
                /* Direct Route */
                i1Type = IP6_ROUTE_TYPE_DIRECT;
            }
            else
            {
                /* Possibly indirect Route. */
                i1Type = IP6_ROUTE_TYPE_INDIRECT;
            }

            MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));
            Ip6CopyAddrBits (&NetIpv6RtInfo.Ip6Dst, &Ip6Addr,
                             i4Fsipv6RoutePfxLength);
            NetIpv6RtInfo.u1Prefixlen = (UINT1) i4Fsipv6RoutePfxLength;
            MEMCPY (&NetIpv6RtInfo.NextHop, &NextHop, sizeof (tIp6Addr));
            NetIpv6RtInfo.u4Index = 0;    /* Index should be set before ACTIVE is set */
            NetIpv6RtInfo.u4Metric = 1;    /* Default Metric */
            NetIpv6RtInfo.i1Proto = (INT1) i4Fsipv6RouteProtocol;
            NetIpv6RtInfo.u4RouteTag = 0;    /* Route Tag Also Defaults to 0 */
            Rtm6ApiGetProtocolPrefInCxt (u4ContextId, i4Fsipv6RouteProtocol,
                                         &u4Preference);
            NetIpv6RtInfo.u1Preference = (UINT1) u4Preference;
            NetIpv6RtInfo.i1Type = i1Type;
            NetIpv6RtInfo.u4ContextId = u4ContextId;

            if ((i4Fsipv6RoutePfxLength == 0) &&
                (IS_ADDR_UNSPECIFIED (NetIpv6RtInfo.Ip6Dst)))
            {
                NetIpv6RtInfo.i1DefRtrFlag = IP6_DEF_RTR_FLAG;
            }
            else
            {
                NetIpv6RtInfo.i1DefRtrFlag = 0;
            }
            NetIpv6RtInfo.u4RowStatus = IP6FWD_NOT_READY;

            NetIpv6RtInfo.u2ChgBit = IP6_BIT_ALL;

            RtQuery.u4ContextId = u4ContextId;
            RtQuery.u1QueryFlag = RTM6_QUERIED_FOR_NEXT_HOP;
            Ip6AddrCopy (&NetIp6RtInfo.Ip6Dst, &NextHop);
            NetIp6RtInfo.u1Prefixlen = IP6_ADDR_MAX_PREFIX;

            RTM6_TASK_UNLOCK ();
            if ((NetIpv6GetRoute (&RtQuery, &NetIp6RtInfo) == NETIPV6_FAILURE))
            {
                RTM6_TASK_LOCK ();

                Ip6CopyAddrBits (&NetIpv6RtInfo.Ip6Dst, &Ip6Addr,
                                 i4Fsipv6RoutePfxLength);
                NetIpv6RtInfo.u1Prefixlen = (UINT1) i4Fsipv6RoutePfxLength;

                if (Rtm6UtilGetInActiveTableRtEntry
                    (&Ip6Addr, i4Fsipv6RoutePfxLength, &NextHop,
                     &NetIp6TempRtInfo) != RTM6_SUCCESS)
                {

                    if (Rtm6InsertRouteInInvdRouteList (&NetIpv6RtInfo) ==
                        RTM6_FAILURE)
                    {

                        return SNMP_FAILURE;
                    }
                }
            }
            else
            {
                i4RetVal =
                    Rtm6Ipv6LeakRoute (NETIPV6_ADD_ROUTE, &NetIpv6RtInfo);
                if (i4RetVal != RTM6_SUCCESS)
                {
                    return SNMP_FAILURE;
                }
            }
            break;

        case IP6FWD_ACTIVE:
            if (Rtm6ApiFindRtEntryInCxt (u4ContextId, &Ip6Addr,
                                         (UINT1) i4Fsipv6RoutePfxLength,
                                         (INT1) i4Fsipv6RouteProtocol, &NextHop,
                                         &NetIpv6RtInfo) == RTM6_FAILURE)
            {
                return SNMP_FAILURE;
            }

            /* If Index Should be set before Admin Status is made ACTIVE */
            if (NetIpv6RtInfo.u4Index == 0)
            {
                CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE_FOR_NEXTHOP);
                return SNMP_FAILURE;
            }

            pIf6Entry = Lip6UtlGetIfEntry (NetIpv6RtInfo.u4Index);
            if (pIf6Entry == NULL)
            {
                return SNMP_FAILURE;
            }

            if (NetIpv6RtInfo.u4RowStatus == IP6FWD_ACTIVE)
            {
                return SNMP_SUCCESS;
            }

            /* Valid Index Exists. Make the entry as ACTIVE. */
            NetIpv6RtInfo.u4RowStatus = IP6FWD_ACTIVE;

            i4RetVal =
                Rtm6UtilIpv6LeakRoute (NETIPV6_ADD_ROUTE, &NetIpv6RtInfo);
            if (i4RetVal != RTM6_SUCCESS)
            {
                if (i4RetVal == RTM6_NO_RESOURCE)
                {
                    CLI_SET_ERR (CLI_IP6_MEM_UNAVAILABLE);
                }
                return SNMP_FAILURE;
            }

            break;

        case IP6FWD_NOT_IN_SERVICE:
            if (Rtm6ApiFindRtEntryInCxt (u4ContextId, &Ip6Addr,
                                         (UINT1) i4Fsipv6RoutePfxLength,
                                         (INT1) i4Fsipv6RouteProtocol,
                                         &NextHop,
                                         &NetIpv6RtInfo) == RTM6_FAILURE)
            {
                /*Checking whether the route is present in Invalid route list,
                 * if present, we will silently return and will not process further,
                 * if not present, we will return FAILURE */
                if (Rtm6CheckRtExistsInInvdRouteList
                    (&Ip6Addr, i4Fsipv6RoutePfxLength,
                     &NextHop) == RTM6_SUCCESS)
                {
                    return SNMP_SUCCESS;
                }
                return SNMP_FAILURE;
            }

            if (NetIpv6RtInfo.u4RowStatus == IP6FWD_NOT_IN_SERVICE)
            {
                IncMsrForIpv6RouteTable (u4ContextId, pFsipv6RouteDest,
                                         i4Fsipv6RoutePfxLength,
                                         pFsipv6RouteNextHop, 'u',
                                         &i4SetValFsipv6RouteAdminStatus,
                                         FsMIStdInetCidrRouteStatus,
                                         sizeof (FsMIStdInetCidrRouteStatus) /
                                         sizeof (UINT4), TRUE);

                return SNMP_SUCCESS;
            }

            NetIpv6RtInfo.u4RowStatus = IP6FWD_NOT_IN_SERVICE;

            i4RetVal =
                Rtm6UtilIpv6LeakRoute (NETIPV6_ADD_ROUTE, &NetIpv6RtInfo);
            if (i4RetVal != RTM6_SUCCESS)
            {
                if (i4RetVal == RTM6_NO_RESOURCE)
                {
                    CLI_SET_ERR (CLI_IP6_MEM_UNAVAILABLE);
                }
                return SNMP_FAILURE;
            }

            break;

        case IP6FWD_DESTROY:
            RtQuery.u4ContextId = u4ContextId;
            RtQuery.u1QueryFlag = RTM6_QUERIED_FOR_NEXT_HOP;
            Ip6AddrCopy (&NetIp6RtInfo.Ip6Dst, &NextHop);
            NetIp6RtInfo.u1Prefixlen = IP6_ADDR_MAX_PREFIX;

            RTM6_TASK_UNLOCK ();
            if (NetIpv6GetRoute (&RtQuery, &NetIp6RtInfo) == NETIPV6_FAILURE)
            {
                if (Rtm6CheckRtExistsInInvdRouteList
                    (&Ip6Addr, i4Fsipv6RoutePfxLength,
                     &NextHop) == RTM6_SUCCESS)
                {
                    Rtm6UpdateInvdRouteList (u4Index, &Ip6Addr,
                                             i4Fsipv6RoutePfxLength,
                                             NETIPV6_DELETE_ROUTE);
                    RTM6_TASK_LOCK ();
                    return SNMP_SUCCESS;
                }
            }
            RTM6_TASK_LOCK ();

            MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));
            Ip6CopyAddrBits (&NetIpv6RtInfo.Ip6Dst, &Ip6Addr,
                             i4Fsipv6RoutePfxLength);
            NetIpv6RtInfo.u1Prefixlen = (UINT1) i4Fsipv6RoutePfxLength;
            MEMCPY (&NetIpv6RtInfo.NextHop, &NextHop, sizeof (tIp6Addr));
            NetIpv6RtInfo.i1Proto = (INT1) i4Fsipv6RouteProtocol;
            NetIpv6RtInfo.u4RowStatus = IP6FWD_DESTROY;
            NetIpv6RtInfo.u2ChgBit = IP6_BIT_STATUS;
            NetIpv6RtInfo.u4ContextId = u4ContextId;

            if ((i4Fsipv6RoutePfxLength == 0)
                && (IS_ADDR_UNSPECIFIED (NetIpv6RtInfo.Ip6Dst)))
            {
                NetIpv6RtInfo.i1DefRtrFlag = IP6_DEF_RTR_FLAG;
            }
            else
            {
                NetIpv6RtInfo.i1DefRtrFlag = 0;
            }

            if (Rtm6UtilIpv6LeakRoute (NETIPV6_DELETE_ROUTE, &NetIpv6RtInfo) ==
                RTM6_FAILURE)
            {
                return SNMP_FAILURE;
            }

            break;

        default:
            return SNMP_FAILURE;
    }
    IncMsrForIpv6RouteTable (u4ContextId, pFsipv6RouteDest,
                             i4Fsipv6RoutePfxLength,
                             pFsipv6RouteNextHop,
                             'u', &i4SetValFsipv6RouteAdminStatus,
                             FsMIStdInetCidrRouteStatus,
                             sizeof (FsMIStdInetCidrRouteStatus) /
                             sizeof (UINT4), TRUE);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6RoutePreference
 Input       :  The Indices
                Fsipv6RouteDest
                Fsipv6RoutePfxLength
                Fsipv6RouteProtocol
                Fsipv6RouteNextHop

                The Object
                setValFsipv6RoutePreference
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6RoutePreference (tSNMP_OCTET_STRING_TYPE * pFsipv6RouteDest,
                             INT4 i4Fsipv6RoutePfxLength,
                             INT4 i4Fsipv6RouteProtocol,
                             tSNMP_OCTET_STRING_TYPE * pFsipv6RouteNextHop,
                             INT4 i4SetValFsipv6RoutePreference)
{
    tIp6Addr            Ip6Addr;
    tIp6Addr            NextHop;
    tNetIpv6RtInfo      NetIpv6RtInfo;
    UINT4               u4ContextId = IP6_ZERO;

    u4ContextId = (UINT4) UtilRtm6GetCurrCxtId ();
    MEMSET (&Ip6Addr, IP6_ZERO, sizeof (tIp6Addr));
    MEMSET (&NextHop, IP6_ZERO, sizeof (tIp6Addr));
    MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));
    Ip6AddrCopy (&Ip6Addr,
                 (tIp6Addr *) (VOID *) pFsipv6RouteDest->pu1_OctetList);
    Ip6AddrCopy (&NextHop,
                 (tIp6Addr *) (VOID *) pFsipv6RouteNextHop->pu1_OctetList);

    if (Rtm6ApiFindRtEntryInCxt (u4ContextId, &Ip6Addr,
                                 (UINT1) i4Fsipv6RoutePfxLength,
                                 (INT1) i4Fsipv6RouteProtocol, &NextHop,
                                 &NetIpv6RtInfo) == RTM6_SUCCESS)
    {
        NetIpv6RtInfo.u1Preference = (UINT1) i4SetValFsipv6RoutePreference;
        /* For Metric dont just update the metric Value. Because, the route
         * will be getting re-ordered as per the new metric Value. */
        if (Rtm6Ipv6LeakRoute (NETIPV6_ADD_ROUTE, &NetIpv6RtInfo) ==
            RTM6_FAILURE)
        {
            return SNMP_FAILURE;
        }
        IncMsrForIpv6RouteTable (u4ContextId, pFsipv6RouteDest,
                                 i4Fsipv6RoutePfxLength,
                                 pFsipv6RouteNextHop,
                                 'i', &i4SetValFsipv6RoutePreference,
                                 FsMIStdInetCidrRoutePreference,
                                 sizeof (FsMIStdInetCidrRoutePreference) /
                                 sizeof (UINT4), FALSE);
        return SNMP_SUCCESS;
    }
    else
    {
        MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));
        NetIpv6RtInfo.u1Preference = (UINT1) i4SetValFsipv6RoutePreference;
        if (Rtm6UpdateRouteInInvdRouteList (&Ip6Addr, i4Fsipv6RoutePfxLength,
                                            &NextHop,
                                            &NetIpv6RtInfo) != RTM6_FAILURE)
        {
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Fsipv6RouteIfIndex
 Input       :  The Indices
                Fsipv6RouteDest
                Fsipv6RoutePfxLength
                Fsipv6RouteProtocol
                Fsipv6RouteNextHop

                The Object 
                testValFsipv6RouteIfIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6RouteIfIndex (UINT4 *pu4ErrorCode,
                             tSNMP_OCTET_STRING_TYPE * pFsipv6RouteDest,
                             INT4 i4Fsipv6RoutePfxLength,
                             INT4 i4Fsipv6RouteProtocol,
                             tSNMP_OCTET_STRING_TYPE * pFsipv6RouteNextHop,
                             INT4 i4TestValFsipv6RouteIfIndex)
{
    tIp6Addr            Ip6Addr;
    UINT4               u4IfContextId = IP6_ZERO;
    tIp6Addr            NextHop;
    tNetIpv6RtInfo      NetIp6RtInfo;
    tNetIpv6RtInfoQueryMsg RtQuery;
    UINT4               u4ContextId = IP6_ZERO;
    u4ContextId = (UINT4) UtilRtm6GetCurrCxtId ();

    MEMSET (&Ip6Addr, IP6_ZERO, sizeof (tIp6Addr));
    MEMSET (&NetIp6RtInfo, 0, sizeof (tNetIpv6RtInfo));
    MEMSET (&RtQuery, 0, sizeof (tNetIpv6RtInfoQueryMsg));

    if (pFsipv6RouteDest == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    Ip6AddrCopy (&Ip6Addr,
                 (tIp6Addr *) (VOID *) pFsipv6RouteDest->pu1_OctetList);

    if ((IS_ADDR_MULTI (Ip6Addr)) || (IS_ADDR_LLOCAL (Ip6Addr)))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4Fsipv6RoutePfxLength < IP6_ADDR_MIN_PREFIX) ||
        (i4Fsipv6RoutePfxLength > IP6_ADDR_MAX_PREFIX))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (i4Fsipv6RouteProtocol != IP6_NETMGMT_PROTOID)
    {
        /* Invalid Protocol Id */
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    VcmGetContextIdFromCfaIfIndex ((UINT4) i4TestValFsipv6RouteIfIndex,
                                   &u4IfContextId);
    if (u4ContextId != u4IfContextId)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    Ip6AddrCopy (&Ip6Addr,
                 (tIp6Addr *) (VOID *) pFsipv6RouteNextHop->pu1_OctetList);

    if (IS_ADDR_MULTI (Ip6Addr) || ((IS_ADDR_LLOCAL (Ip6Addr)) &&
                                    (i4TestValFsipv6RouteIfIndex == 0)))
    {
        CLI_SET_ERR (CLI_IP6_INVALID_NEXTHOP);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    RtQuery.u4ContextId = u4ContextId;
    Ip6AddrCopy (&NetIp6RtInfo.Ip6Dst, &NextHop);
    NetIp6RtInfo.u1Prefixlen = IP6_ADDR_MAX_PREFIX;
    RTM6_TASK_UNLOCK ();
    if (NetIpv6GetRoute (&RtQuery, &NetIp6RtInfo) != NETIPV6_FAILURE)
    {
        if (NetIp6RtInfo.u4Index != (UINT4) i4TestValFsipv6RouteIfIndex)
        {
            CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE_FOR_NEXTHOP);
            return SNMP_FAILURE;
        }
    }
    if (Lip6UtlIfEntryExists ((UINT4) i4TestValFsipv6RouteIfIndex) !=
        OSIX_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6RouteMetric
 Input       :  The Indices
                Fsipv6RouteDest
                Fsipv6RoutePfxLength
                Fsipv6RouteProtocol
                Fsipv6RouteNextHop

                The Object 
                testValFsipv6RouteMetric
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6RouteMetric (UINT4 *pu4ErrorCode,
                            tSNMP_OCTET_STRING_TYPE * pFsipv6RouteDest,
                            INT4 i4Fsipv6RoutePfxLength,
                            INT4 i4Fsipv6RouteProtocol,
                            tSNMP_OCTET_STRING_TYPE * pFsipv6RouteNextHop,
                            UINT4 u4TestValFsipv6RouteMetric)
{
    tIp6Addr            Ip6Addr;

    MEMSET (&Ip6Addr, IP6_ZERO, sizeof (tIp6Addr));

    if (pFsipv6RouteDest == NULL)
    {
        CLI_SET_ERR (CLI_IP6_INVALID_ADDRESS);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    Ip6AddrCopy (&Ip6Addr,
                 (tIp6Addr *) (VOID *) pFsipv6RouteDest->pu1_OctetList);

    if ((IS_ADDR_MULTI (Ip6Addr)) || (IS_ADDR_LLOCAL (Ip6Addr)))
    {
        CLI_SET_ERR (CLI_IP6_INVALID_ADDR_TYPE);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4Fsipv6RoutePfxLength < IP6_ADDR_MIN_PREFIX) ||
        (i4Fsipv6RoutePfxLength > IP6_ADDR_MAX_PREFIX))
    {
        CLI_SET_ERR (CLI_IP6_INVALID_PREFIXLEN);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4Fsipv6RouteProtocol != IP6_NETMGMT_PROTOID) &&
        (i4Fsipv6RouteProtocol != IP6_LOCAL_PROTOID))
    {
        /* Invalid Protocol Id */
        CLI_SET_ERR (CLI_IP6_INVALID_ROUTE_PROTO);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    Ip6AddrCopy (&Ip6Addr,
                 (tIp6Addr *) (VOID *) pFsipv6RouteNextHop->pu1_OctetList);

    if (IS_ADDR_MULTI (Ip6Addr))
    {
        CLI_SET_ERR (CLI_IP6_INVALID_NEXTHOP);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((u4TestValFsipv6RouteMetric >= IP6_ROUTE_MIN_METRIC) &&
        (u4TestValFsipv6RouteMetric <= IP6_ROUTE_MAX_METRIC))
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    CLI_SET_ERR (CLI_IP6_INVALID_ROUTE_METRIC);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6RouteType
 Input       :  The Indices
                Fsipv6RouteDest
                Fsipv6RoutePfxLength
                Fsipv6RouteProtocol
                Fsipv6RouteNextHop

                The Object 
                testValFsipv6RouteType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6RouteType (UINT4 *pu4ErrorCode,
                          tSNMP_OCTET_STRING_TYPE * pFsipv6RouteDest,
                          INT4 i4Fsipv6RoutePfxLength,
                          INT4 i4Fsipv6RouteProtocol,
                          tSNMP_OCTET_STRING_TYPE * pFsipv6RouteNextHop,
                          INT4 i4TestValFsipv6RouteType)
{
    tIp6Addr            Ip6Addr;

    MEMSET (&Ip6Addr, IP6_ZERO, sizeof (tIp6Addr));
    if (pFsipv6RouteDest == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    Ip6AddrCopy (&Ip6Addr,
                 (tIp6Addr *) (VOID *) pFsipv6RouteDest->pu1_OctetList);

    if ((IS_ADDR_MULTI (Ip6Addr)) || (IS_ADDR_LLOCAL (Ip6Addr)))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4Fsipv6RoutePfxLength < IP6_ADDR_MIN_PREFIX) ||
        (i4Fsipv6RoutePfxLength > IP6_ADDR_MAX_PREFIX))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (i4Fsipv6RouteProtocol != IP6_NETMGMT_PROTOID)
    {
        /* Invalid Protocol Id */
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    Ip6AddrCopy (&Ip6Addr,
                 (tIp6Addr *) (VOID *) pFsipv6RouteNextHop->pu1_OctetList);

    if (IS_ADDR_MULTI (Ip6Addr))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsipv6RouteType == IP6_ROUTE_TYPE_DIRECT) ||
        (i4TestValFsipv6RouteType == IP6_ROUTE_TYPE_OTHER) ||
        (i4TestValFsipv6RouteType == IP6_ROUTE_TYPE_INDIRECT))
    {
        return SNMP_SUCCESS;
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6RouteTag
 Input       :  The Indices
                Fsipv6RouteDest
                Fsipv6RoutePfxLength
                Fsipv6RouteProtocol
                Fsipv6RouteNextHop

                The Object 
                testValFsipv6RouteTag
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6RouteTag (UINT4 *pu4ErrorCode,
                         tSNMP_OCTET_STRING_TYPE * pFsipv6RouteDest,
                         INT4 i4Fsipv6RoutePfxLength,
                         INT4 i4Fsipv6RouteProtocol,
                         tSNMP_OCTET_STRING_TYPE * pFsipv6RouteNextHop,
                         UINT4 u4TestValFsipv6RouteTag)
{
    tIp6Addr            Ip6Addr;

    MEMSET (&Ip6Addr, IP6_ZERO, sizeof (tIp6Addr));

    if (pFsipv6RouteDest == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    Ip6AddrCopy (&Ip6Addr,
                 (tIp6Addr *) (VOID *) pFsipv6RouteDest->pu1_OctetList);

    if ((IS_ADDR_MULTI (Ip6Addr)) || (IS_ADDR_LLOCAL (Ip6Addr)))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4Fsipv6RoutePfxLength < IP6_ADDR_MIN_PREFIX) ||
        (i4Fsipv6RoutePfxLength > IP6_ADDR_MAX_PREFIX))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (i4Fsipv6RouteProtocol != IP6_NETMGMT_PROTOID)
    {
        /* Invalid Protocol Id */
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    Ip6AddrCopy (&Ip6Addr,
                 (tIp6Addr *) (VOID *) pFsipv6RouteNextHop->pu1_OctetList);

    if (IS_ADDR_MULTI (Ip6Addr))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (((u4TestValFsipv6RouteTag & 0xFFFF0000) != 0) &&
        ((u4TestValFsipv6RouteTag & 0x0000FFFF) == 0))
    {
        /* Invalid Route Tag */
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6RouteAdminStatus
 Input       :  The Indices
                Fsipv6RouteDest
                Fsipv6RoutePfxLength
                Fsipv6RouteProtocol
                Fsipv6RouteNextHop

                The Object 
                testValFsipv6RouteAdminStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6RouteAdminStatus (UINT4 *pu4ErrorCode,
                                 tSNMP_OCTET_STRING_TYPE * pFsipv6RouteDest,
                                 INT4 i4Fsipv6RoutePfxLength,
                                 INT4 i4Fsipv6RouteProtocol,
                                 tSNMP_OCTET_STRING_TYPE * pFsipv6RouteNextHop,
                                 INT4 i4TestValFsipv6RouteAdminStatus)
{
    tNetIpv6RtInfo      NetIpv6RtInfo;
    tIp6Addr            Ip6Addr;
    tIp6Addr            NextHop;
    INT4                i4RetVal = RTM6_FAILURE;
    INT4                i4InvdRetVal = RTM6_FAILURE;
    tNetIpv6RtInfo      NetIp6RtInfo;
    tNetIpv6RtInfoQueryMsg RtQuery;
    UINT4               u4ContextId = IP6_ZERO;
    UINT4               u4FreeRouteCount = IP6_ZERO;

    MEMSET (&NetIp6RtInfo, IP6_ZERO, sizeof (tNetIpv6RtInfo));
    MEMSET (&RtQuery, IP6_ZERO, sizeof (tNetIpv6RtInfoQueryMsg));
    MEMSET (&Ip6Addr, IP6_ZERO, sizeof (tIp6Addr));
    MEMSET (&NextHop, IP6_ZERO, sizeof (tIp6Addr));
    MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));

    u4ContextId = (UINT4) UtilRtm6GetCurrCxtId ();
    Ip6AddrCopy (&Ip6Addr,
                 (tIp6Addr *) (VOID *) pFsipv6RouteDest->pu1_OctetList);
    Ip6AddrCopy (&NextHop,
                 (tIp6Addr *) (VOID *) pFsipv6RouteNextHop->pu1_OctetList);

    if ((IS_ADDR_MULTI (Ip6Addr)) || (IS_ADDR_LLOCAL (Ip6Addr)))
    {
        CLI_SET_ERR (CLI_IP6_INVALID_ADDR_TYPE);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4Fsipv6RoutePfxLength < IP6_ADDR_MIN_PREFIX) ||
        (i4Fsipv6RoutePfxLength > IP6_ADDR_MAX_PREFIX))
    {
        CLI_SET_ERR (CLI_IP6_INVALID_PREFIXLEN);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (i4Fsipv6RouteProtocol != IP6_NETMGMT_PROTOID)
    {
        /* Invalid Protocol Id */
        CLI_SET_ERR (CLI_IP6_INVALID_ROUTE_PROTO);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /* Validate Next Hop */
    if (IS_ADDR_MULTI (NextHop))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if ((i4TestValFsipv6RouteAdminStatus == IP6FWD_CREATE_AND_GO) ||
        (i4TestValFsipv6RouteAdminStatus == IP6FWD_NOT_READY) ||
        (i4TestValFsipv6RouteAdminStatus == IP6FWD_ROW_DOES_NOT_EXIST))
    {
        /* Both of these values are not supported. */
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    RtQuery.u4ContextId = VCM_DEFAULT_CONTEXT;
    RtQuery.u1QueryFlag = RTM6_QUERIED_FOR_NEXT_HOP;
    Ip6AddrCopy (&NetIp6RtInfo.Ip6Dst, &NextHop);
    NetIp6RtInfo.u1Prefixlen = IP6_ADDR_MAX_PREFIX;

    RTM6_TASK_UNLOCK ();
    if (NetIpv6GetRoute (&RtQuery, &NetIp6RtInfo) == NETIPV6_FAILURE)
    {
        i4InvdRetVal =
            Rtm6CheckRtExistsInInvdRouteList (&Ip6Addr, i4Fsipv6RoutePfxLength,
                                              &NextHop);
    }
    RTM6_TASK_LOCK ();

    i4RetVal = Rtm6ApiFindRtEntryInCxt (u4ContextId, &Ip6Addr,
                                        (UINT1) i4Fsipv6RoutePfxLength,
                                        (INT1) i4Fsipv6RouteProtocol, &NextHop,
                                        &NetIpv6RtInfo);

    if (i4TestValFsipv6RouteAdminStatus == IP6FWD_CREATE_AND_WAIT)
    {
        if ((i4RetVal == RTM6_SUCCESS) || (i4InvdRetVal == RTM6_SUCCESS))
        {
            /* Route Already Present */
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            CLI_SET_ERR (CLI_IP6_ROUTE_ALREADY_PRESENT);
            return SNMP_FAILURE;
        }

        /* Check whether free space is available to add new route entry */
        /* FreeCount is decremented by 1 since that 1 Mem block is used 
         * for metric configuration for Static routes
         * It is mandatory and in macro definition, always the value of 
         * route entries should be +1 the required number of routes */

        Rtm6ApiFreeRouteCount (&u4FreeRouteCount);
        if ((u4FreeRouteCount - 1) == IP6_ZERO)
        {
            *pu4ErrorCode = SNMP_ERR_RESOURCE_UNAVAILABLE;
            CLI_SET_ERR (CLI_IP6_MAX_ROUTE);
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (i4InvdRetVal == RTM6_SUCCESS)
        {
            /* Route present in Invalid Route List */
            return SNMP_SUCCESS;
        }
        if (i4RetVal == RTM6_FAILURE)
        {
            /* Route Does Not Exist */
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            CLI_SET_ERR (CLI_IP6_ROUTE_NOT_FOUND);
            return SNMP_FAILURE;
        }
        if (NetIpv6RtInfo.u4Index == 0)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE_FOR_NEXTHOP);
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6RoutePreference
 Input       :  The Indices
                Fsipv6RouteDest
                Fsipv6RoutePfxLength
                Fsipv6RouteProtocol
                Fsipv6RouteNextHop

                The Object
                testValFsipv6RoutePreference
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6RoutePreference (UINT4 *pu4ErrorCode,
                                tSNMP_OCTET_STRING_TYPE * pFsipv6RouteDest,
                                INT4 i4Fsipv6RoutePfxLength,
                                INT4 i4Fsipv6RouteProtocol,
                                tSNMP_OCTET_STRING_TYPE * pFsipv6RouteNextHop,
                                INT4 i4TestValFsipv6RoutePreference)
{
    tIp6Addr            Ip6Addr;

    MEMSET (&Ip6Addr, IP6_ZERO, sizeof (tIp6Addr));

    if (pFsipv6RouteDest == NULL)
    {
        CLI_SET_ERR (CLI_IP6_INVALID_ADDRESS);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    Ip6AddrCopy (&Ip6Addr,
                 (tIp6Addr *) (VOID *) pFsipv6RouteDest->pu1_OctetList);

    if ((IS_ADDR_MULTI (Ip6Addr)) || (IS_ADDR_LLOCAL (Ip6Addr)))
    {
        CLI_SET_ERR (CLI_IP6_INVALID_ADDR_TYPE);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4Fsipv6RoutePfxLength < IP6_ADDR_MIN_PREFIX) ||
        (i4Fsipv6RoutePfxLength > IP6_ADDR_MAX_PREFIX))
    {
        CLI_SET_ERR (CLI_IP6_INVALID_PREFIXLEN);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4Fsipv6RouteProtocol != IP6_NETMGMT_PROTOID) &&
        (i4Fsipv6RouteProtocol != IP6_LOCAL_PROTOID))
    {
        /* Invalid Protocol Id */
        CLI_SET_ERR (CLI_IP6_INVALID_ROUTE_PROTO);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    Ip6AddrCopy (&Ip6Addr,
                 (tIp6Addr *) (VOID *) pFsipv6RouteNextHop->pu1_OctetList);

    if (IS_ADDR_MULTI (Ip6Addr))
    {
        CLI_SET_ERR (CLI_IP6_INVALID_NEXTHOP);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsipv6RoutePreference < RTM6_IP6_ROUTE_MIN_DISTANCE) ||
        (i4TestValFsipv6RoutePreference >= RTM6_IP6_ROUTE_MAX_DISTANCE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Fsipv6RouteTable
 Input       :  The Indices
                Fsipv6RouteDest
                Fsipv6RoutePfxLength
                Fsipv6RouteProtocol
                Fsipv6RouteNextHop
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsipv6RouteTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Fsipv6PrefTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsipv6PrefTable
 Input       :  The Indices
                Fsipv6Protocol
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsipv6PrefTable (INT4 i4Fsipv6Protocol)
{
    if ((i4Fsipv6Protocol < IP6_OTHER_PROTOID) ||
        (i4Fsipv6Protocol > IP6_IGRP_PROTOID))
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsipv6PrefTable
 Input       :  The Indices
                Fsipv6Protocol
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsipv6PrefTable (INT4 *pi4Fsipv6Protocol)
{
    *pi4Fsipv6Protocol = IP6_OTHER_PROTOID;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsipv6PrefTable
 Input       :  The Indices
                Fsipv6Protocol
                nextFsipv6Protocol
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsipv6PrefTable (INT4 i4Fsipv6Protocol,
                                INT4 *pi4NextFsipv6Protocol)
{
    if (i4Fsipv6Protocol < IP6_MAX_ROUTING_PROTOCOLS)
    {
        *pi4NextFsipv6Protocol = i4Fsipv6Protocol + 1;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsipv6Preference
 Input       :  The Indices
                Fsipv6Protocol

                The Object 
                retValFsipv6Preference
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6Preference (INT4 i4Fsipv6Protocol, UINT4 *pu4RetValFsipv6Preference)
{
    INT4                i4RetVal = RTM6_FAILURE;
    UINT4               u4ContextId = IP6_ZERO;

    u4ContextId = (UINT4) UtilRtm6GetCurrCxtId ();
    i4RetVal =
        Rtm6ApiGetProtocolPrefInCxt (u4ContextId, (UINT4) i4Fsipv6Protocol,
                                     pu4RetValFsipv6Preference);
    if (i4RetVal == RTM6_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsipv6Preference
 Input       :  The Indices
                Fsipv6Protocol

                The Object 
                setValFsipv6Preference
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6Preference (INT4 i4Fsipv6Protocol, UINT4 u4SetValFsipv6Preference)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = IPVX_ZERO;
    UINT4               u4ContextId = IP6_ZERO;

    u4ContextId = (UINT4) UtilRtm6GetCurrCxtId ();
    if (Rtm6ApiSetProtocolPrefInCxt (u4ContextId, (UINT4) i4Fsipv6Protocol,
                                     u4SetValFsipv6Preference) == RTM6_FAILURE)
    {
        return SNMP_FAILURE;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = FsMIIpv6Preference;
    SnmpNotifyInfo.u4OidLen = sizeof (FsMIIpv6Preference) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = Ip6Lock;
    SnmpNotifyInfo.pUnLockPointer = Ip6UnLock;
    SnmpNotifyInfo.u4Indices = IP6_TWO;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %u", (INT4) u4ContextId,
                      i4Fsipv6Protocol, u4SetValFsipv6Preference));

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Fsipv6Preference
 Input       :  The Indices
                Fsipv6Protocol

                The Object 
                testValFsipv6Preference
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6Preference (UINT4 *pu4ErrorCode, INT4 i4Fsipv6Protocol,
                           UINT4 u4TestValFsipv6Preference)
{
    if ((i4Fsipv6Protocol < IP6_OTHER_PROTOID) ||
        (i4Fsipv6Protocol > IP6_IGRP_PROTOID))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (u4TestValFsipv6Preference > IP6_MAX_PROTO_PREFERENCE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Fsipv6PrefTable
 Input       :  The Indices
                Fsipv6Protocol
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsipv6PrefTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6SupportEmbeddedRp
 Input       :  The Indices
                Fsipv6PrefixIndex
                Fsipv6PrefixAddress
                Fsipv6PrefixAddrLen

                The Object
                retValFsipv6SupportEmbeddedRp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SupportEmbeddedRp (INT4 i4Fsipv6PrefixIndex,
                               tSNMP_OCTET_STRING_TYPE * pFsipv6PrefixAddress,
                               INT4 i4Fsipv6PrefixAddrLen,
                               INT4 *pi4RetValFsipv6SupportEmbeddedRp)
{
    tLip6PrefixNode    *pPrefix = NULL;

    pPrefix = Lip6PrefixGetEntry ((UINT4) i4Fsipv6PrefixIndex,
                                  (tIp6Addr *) (VOID *) pFsipv6PrefixAddress->
                                  pu1_OctetList, (UINT1) i4Fsipv6PrefixAddrLen);
    if (pPrefix != NULL)
    {
        *pi4RetValFsipv6SupportEmbeddedRp = pPrefix->u1EmbdRpValid;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsipv6AddrSelPolicyTable
 Input       :  The Indices
                Fsipv6AddrSelPolicyPrefix
                Fsipv6AddrSelPolicyPrefixLen
                Fsipv6AddrSelPolicyIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1                nmhGetFirstIndexFsipv6AddrSelPolicyTable
    (tSNMP_OCTET_STRING_TYPE * pFsipv6AddrSelPolicyPrefix,
     INT4 *pi4Fsipv6AddrSelPolicyPrefixLen, INT4 *pi4Fsipv6AddrSelPolicyIfIndex)
{

    tIp6AddrSelPolicy  *pPolicyPrefix = NULL;
    UINT4               u4Index = 0;

    pPolicyPrefix = Ip6GetFirstPolicyPrefix (u4Index);
    if (pPolicyPrefix != NULL)
    {
        *pi4Fsipv6AddrSelPolicyIfIndex = (INT4) u4Index;
        MEMCPY (pFsipv6AddrSelPolicyPrefix->pu1_OctetList,
                &pPolicyPrefix->Ip6PolicyPrefix, IP6_ADDR_SIZE);
        pFsipv6AddrSelPolicyPrefix->i4_Length = IP6_ADDR_SIZE;
        *pi4Fsipv6AddrSelPolicyPrefixLen = pPolicyPrefix->u1PrefixLen;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsipv6AddrSelPolicyTable
 Input       :  The Indices
                Fsipv6AddrSelPolicyPrefix
                nextFsipv6AddrSelPolicyPrefix
                Fsipv6AddrSelPolicyPrefixLen
                nextFsipv6AddrSelPolicyPrefixLen
                Fsipv6AddrSelPolicyIfIndex
                nextFsipv6AddrSelPolicyIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1                nmhGetNextIndexFsipv6AddrSelPolicyTable
    (tSNMP_OCTET_STRING_TYPE * pFsipv6AddrSelPolicyPrefix,
     tSNMP_OCTET_STRING_TYPE * pNextFsipv6AddrSelPolicyPrefix,
     INT4 i4Fsipv6AddrSelPolicyPrefixLen,
     INT4 *pi4NextFsipv6AddrSelPolicyPrefixLen,
     INT4 i4Fsipv6AddrSelPolicyIfIndex, INT4 *pi4NextFsipv6AddrSelPolicyIfIndex)
{
    tIp6AddrSelPolicy  *pNextPrefix = NULL;

    if (i4Fsipv6AddrSelPolicyIfIndex < 0)
    {
        return (SNMP_FAILURE);
    }
    pNextPrefix = Ip6GetNextPolicyPrefix ((UINT4) i4Fsipv6AddrSelPolicyIfIndex,
                                          (tIp6Addr *) (VOID *)
                                          pFsipv6AddrSelPolicyPrefix->
                                          pu1_OctetList,
                                          (UINT1)
                                          i4Fsipv6AddrSelPolicyPrefixLen);

    if (pNextPrefix != NULL)
    {
        *pi4NextFsipv6AddrSelPolicyIfIndex = (INT4) pNextPrefix->u4IfIndex;
        MEMCPY (pNextFsipv6AddrSelPolicyPrefix->pu1_OctetList,
                &pNextPrefix->Ip6PolicyPrefix, IP6_ADDR_SIZE);
        pNextFsipv6AddrSelPolicyPrefix->i4_Length = IP6_ADDR_SIZE;
        *pi4NextFsipv6AddrSelPolicyPrefixLen = pNextPrefix->u1PrefixLen;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsipv6IfScopeZoneMapTable
 Input       :  The Indices
                Fsipv6ScopeZoneIndexIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsipv6IfScopeZoneMapTable (INT4 *pi4Fsipv6ScopeZoneIndexIfIndex)
{
    UNUSED_PARAM (pi4Fsipv6ScopeZoneIndexIfIndex);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsipv6IfScopeZoneMapTable
 Input       :  The Indices
                Fsipv6ScopeZoneIndexIfIndex
                nextFsipv6ScopeZoneIndexIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsipv6IfScopeZoneMapTable (INT4 i4Fsipv6ScopeZoneIndexIfIndex,
                                          INT4
                                          *pi4NextFsipv6ScopeZoneIndexIfIndex)
{
    UNUSED_PARAM (i4Fsipv6ScopeZoneIndexIfIndex);
    UNUSED_PARAM (pi4NextFsipv6ScopeZoneIndexIfIndex);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhDepv2Fsipv6IfScopeZoneMapTable
 Input       :  The Indices
                Fsipv6ScopeZoneIndexIfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsipv6IfScopeZoneMapTable (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsipv6ScopeZoneTable
 Input       :  The Indices
                Fsipv6ScopeZoneName
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsipv6ScopeZoneTable (tSNMP_OCTET_STRING_TYPE *
                                      pFsipv6ScopeZoneName)
{
    UNUSED_PARAM (pFsipv6ScopeZoneName);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsipv6ScopeZoneTable
 Input       :  The Indices
                Fsipv6ScopeZoneName
                nextFsipv6ScopeZoneName
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsipv6ScopeZoneTable (tSNMP_OCTET_STRING_TYPE *
                                     pFsipv6ScopeZoneName,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pNextFsipv6ScopeZoneName)
{
    UNUSED_PARAM (pFsipv6ScopeZoneName);
    UNUSED_PARAM (pNextFsipv6ScopeZoneName);
    return SNMP_FAILURE;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Fsipv6ScopeZoneTable
 Input       :  The Indices
                Fsipv6ScopeZoneName
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsipv6ScopeZoneTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Fsipv6RFC5095Compatibility
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsipv6RFC5095Compatibility (UINT4 *pu4ErrorCode,
                                    tSnmpIndexList * pSnmpIndexList,
                                    tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Fsipv6RFC5942Compatibility
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsipv6RFC5942Compatibility (UINT4 *pu4ErrorCode,
                                    tSnmpIndexList * pSnmpIndexList,
                                    tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6AddrScope
 Input       :  The Indices
                Fsipv6AddrIndex
                Fsipv6AddrAddress
                Fsipv6AddrPrefixLen

                The Object
                retValFsipv6AddrScope
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6AddrScope (INT4 i4Fsipv6AddrIndex,
                       tSNMP_OCTET_STRING_TYPE * pFsipv6AddrAddress,
                       INT4 i4Fsipv6AddrPrefixLen,
                       INT4 *pi4RetValFsipv6AddrScope)
{
    UNUSED_PARAM (i4Fsipv6AddrIndex);
    UNUSED_PARAM (pFsipv6AddrAddress);
    UNUSED_PARAM (i4Fsipv6AddrPrefixLen);
    UNUSED_PARAM (pi4RetValFsipv6AddrScope);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6AddrSelPolicyAddrType
 Input       :  The Indices
                Fsipv6AddrSelPolicyPrefix
                Fsipv6AddrSelPolicyPrefixLen
                Fsipv6AddrSelPolicyIfIndex

                The Object
                retValFsipv6AddrSelPolicyAddrType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6AddrSelPolicyAddrType (tSNMP_OCTET_STRING_TYPE
                                   * pFsipv6AddrSelPolicyPrefix,
                                   INT4 i4Fsipv6AddrSelPolicyPrefixLen,
                                   INT4 i4Fsipv6AddrSelPolicyIfIndex,
                                   INT4 *pi4RetValFsipv6AddrSelPolicyAddrType)
{

    tIp6AddrSelPolicy  *pPolicyPrefix = NULL;

    pPolicyPrefix = Ip6GetPolicyPrefix ((UINT4) i4Fsipv6AddrSelPolicyIfIndex,
                                        (tIp6Addr *) (VOID *)
                                        pFsipv6AddrSelPolicyPrefix->
                                        pu1_OctetList,
                                        (UINT1) i4Fsipv6AddrSelPolicyPrefixLen);
    if (pPolicyPrefix != NULL)
    {
        *pi4RetValFsipv6AddrSelPolicyAddrType = pPolicyPrefix->u1AddrType;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsipv6AddrSelPolicyConfigStatus
 Input       :  The Indices
                Fsipv6AddrSelPolicyPrefix
                Fsipv6AddrSelPolicyPrefixLen
                Fsipv6AddrSelPolicyIfIndex

                The Object
                retValFsipv6AddrSelPolicyConfigStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6AddrSelPolicyConfigStatus (tSNMP_OCTET_STRING_TYPE
                                       * pFsipv6AddrSelPolicyPrefix,
                                       INT4 i4Fsipv6AddrSelPolicyPrefixLen,
                                       INT4 i4Fsipv6AddrSelPolicyIfIndex,
                                       INT4
                                       *pi4RetValFsipv6AddrSelPolicyConfigStatus)
{

    UNUSED_PARAM (pFsipv6AddrSelPolicyPrefix);
    UNUSED_PARAM (i4Fsipv6AddrSelPolicyIfIndex);
    UNUSED_PARAM (pi4RetValFsipv6AddrSelPolicyConfigStatus);
    UNUSED_PARAM (i4Fsipv6AddrSelPolicyPrefixLen);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6AddrSelPolicyIsPublicAddr
 Input       :  The Indices
                Fsipv6AddrSelPolicyPrefix
                Fsipv6AddrSelPolicyPrefixLen
                Fsipv6AddrSelPolicyIfIndex

                The Object
                retValFsipv6AddrSelPolicyIsPublicAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6AddrSelPolicyIsPublicAddr (tSNMP_OCTET_STRING_TYPE
                                       * pFsipv6AddrSelPolicyPrefix,
                                       INT4 i4Fsipv6AddrSelPolicyPrefixLen,
                                       INT4 i4Fsipv6AddrSelPolicyIfIndex,
                                       INT4
                                       *pi4RetValFsipv6AddrSelPolicyIsPublicAddr)
{
    UNUSED_PARAM (pFsipv6AddrSelPolicyPrefix);
    UNUSED_PARAM (i4Fsipv6AddrSelPolicyPrefixLen);
    UNUSED_PARAM (i4Fsipv6AddrSelPolicyIfIndex);
    UNUSED_PARAM (pi4RetValFsipv6AddrSelPolicyIsPublicAddr);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6AddrSelPolicyIsSelfAddr
 Input       :  The Indices
                Fsipv6AddrSelPolicyPrefix
                Fsipv6AddrSelPolicyPrefixLen
                Fsipv6AddrSelPolicyIfIndex

                The Object
                retValFsipv6AddrSelPolicyIsSelfAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6AddrSelPolicyIsSelfAddr (tSNMP_OCTET_STRING_TYPE
                                     * pFsipv6AddrSelPolicyPrefix,
                                     INT4 i4Fsipv6AddrSelPolicyPrefixLen,
                                     INT4 i4Fsipv6AddrSelPolicyIfIndex,
                                     INT4
                                     *pi4RetValFsipv6AddrSelPolicyIsSelfAddr)
{
    UNUSED_PARAM (pFsipv6AddrSelPolicyPrefix);
    UNUSED_PARAM (i4Fsipv6AddrSelPolicyPrefixLen);
    UNUSED_PARAM (i4Fsipv6AddrSelPolicyIfIndex);
    UNUSED_PARAM (pi4RetValFsipv6AddrSelPolicyIsSelfAddr);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6AddrSelPolicyLabel
 Input       :  The Indices
                Fsipv6AddrSelPolicyPrefix
                Fsipv6AddrSelPolicyPrefixLen
                Fsipv6AddrSelPolicyIfIndex

                The Object
                retValFsipv6AddrSelPolicyLabel
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6AddrSelPolicyLabel (tSNMP_OCTET_STRING_TYPE *
                                pFsipv6AddrSelPolicyPrefix,
                                INT4 i4Fsipv6AddrSelPolicyPrefixLen,
                                INT4 i4Fsipv6AddrSelPolicyIfIndex,
                                INT4 *pi4RetValFsipv6AddrSelPolicyLabel)
{

    tIp6AddrSelPolicy  *pPolicyPrefix = NULL;

    pPolicyPrefix = Ip6GetPolicyPrefix ((UINT4) i4Fsipv6AddrSelPolicyIfIndex,
                                        (tIp6Addr *) (VOID *)
                                        pFsipv6AddrSelPolicyPrefix->
                                        pu1_OctetList,
                                        (UINT1) i4Fsipv6AddrSelPolicyPrefixLen);
    if (pPolicyPrefix != NULL)
    {
        *pi4RetValFsipv6AddrSelPolicyLabel = pPolicyPrefix->u1Label;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsipv6AddrSelPolicyPrecedence
 Input       :  The Indices
                Fsipv6AddrSelPolicyPrefix
                Fsipv6AddrSelPolicyPrefixLen
                Fsipv6AddrSelPolicyIfIndex

                The Object
                retValFsipv6AddrSelPolicyPrecedence
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFsipv6AddrSelPolicyPrecedence
    (tSNMP_OCTET_STRING_TYPE * pFsipv6AddrSelPolicyPrefix,
     INT4 i4Fsipv6AddrSelPolicyPrefixLen,
     INT4 i4Fsipv6AddrSelPolicyIfIndex,
     INT4 *pi4RetValFsipv6AddrSelPolicyPrecedence)
{
    tIp6AddrSelPolicy  *pPolicyPrefix = NULL;

    pPolicyPrefix = Ip6GetPolicyPrefix ((UINT4) i4Fsipv6AddrSelPolicyIfIndex,
                                        (tIp6Addr *) (VOID *)
                                        pFsipv6AddrSelPolicyPrefix->
                                        pu1_OctetList,
                                        (UINT1) i4Fsipv6AddrSelPolicyPrefixLen);
    if (pPolicyPrefix != NULL)
    {
        *pi4RetValFsipv6AddrSelPolicyPrecedence = pPolicyPrefix->u1Precedence;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsipv6AddrSelPolicyReachabilityStatus
 Input       :  The Indices
                Fsipv6AddrSelPolicyPrefix
                Fsipv6AddrSelPolicyPrefixLen
                Fsipv6AddrSelPolicyIfIndex

                The Object
                retValFsipv6AddrSelPolicyReachabilityStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6AddrSelPolicyReachabilityStatus (tSNMP_OCTET_STRING_TYPE
                                             * pFsipv6AddrSelPolicyPrefix,
                                             INT4
                                             i4Fsipv6AddrSelPolicyPrefixLen,
                                             INT4 i4Fsipv6AddrSelPolicyIfIndex,
                                             INT4
                                             *pi4RetValFsipv6AddrSelPolicyReachabilityStatus)
{

    UNUSED_PARAM (pFsipv6AddrSelPolicyPrefix);
    UNUSED_PARAM (i4Fsipv6AddrSelPolicyPrefixLen);
    UNUSED_PARAM (i4Fsipv6AddrSelPolicyIfIndex);
    UNUSED_PARAM (pi4RetValFsipv6AddrSelPolicyReachabilityStatus);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6AddrSelPolicyRowStatus
 Input       :  The Indices
                Fsipv6AddrSelPolicyPrefix
                Fsipv6AddrSelPolicyPrefixLen
                Fsipv6AddrSelPolicyIfIndex

                The Object
                retValFsipv6AddrSelPolicyRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6AddrSelPolicyRowStatus (tSNMP_OCTET_STRING_TYPE
                                    * pFsipv6AddrSelPolicyPrefix,
                                    INT4 i4Fsipv6AddrSelPolicyPrefixLen,
                                    INT4 i4Fsipv6AddrSelPolicyIfIndex,
                                    INT4 *pi4RetValFsipv6AddrSelPolicyRowStatus)
{

    tIp6AddrSelPolicy  *pPolicyPrefix = NULL;
    pPolicyPrefix = Ip6GetPolicyPrefix ((UINT4) i4Fsipv6AddrSelPolicyIfIndex,
                                        (tIp6Addr *) (VOID *)
                                        pFsipv6AddrSelPolicyPrefix->
                                        pu1_OctetList,
                                        (UINT1) i4Fsipv6AddrSelPolicyPrefixLen);
    if (pPolicyPrefix != NULL)
    {
        *pi4RetValFsipv6AddrSelPolicyRowStatus = pPolicyPrefix->u1RowStatus;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsipv6AddrSelPolicyScope
 Input       :  The Indices
                Fsipv6AddrSelPolicyPrefix
                Fsipv6AddrSelPolicyPrefixLen
                Fsipv6AddrSelPolicyIfIndex

                The Object
                retValFsipv6AddrSelPolicyScope
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFsipv6AddrSelPolicyScope
    (tSNMP_OCTET_STRING_TYPE * pFsipv6AddrSelPolicyPrefix,
     INT4 i4Fsipv6AddrSelPolicyPrefixLen,
     INT4 i4Fsipv6AddrSelPolicyIfIndex, INT4 *pi4RetValFsipv6AddrSelPolicyScope)
{

    UNUSED_PARAM (pFsipv6AddrSelPolicyPrefix);
    UNUSED_PARAM (i4Fsipv6AddrSelPolicyPrefixLen);
    UNUSED_PARAM (i4Fsipv6AddrSelPolicyIfIndex);
    UNUSED_PARAM (pi4RetValFsipv6AddrSelPolicyScope);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfDestUnreachableMsg
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                retValFsipv6IfDestUnreachableMsg
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfDestUnreachableMsg (INT4 i4Fsipv6IfIndex,
                                  INT4 *pi4RetValFsipv6IfDestUnreachableMsg)
{
    tLip6If            *pIf6Entry = NULL;

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsipv6IfDestUnreachableMsg =
        pIf6Entry->Icmp6ErrRLInfo.i4Icmp6DstUnReachable;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfUnnumAssocIPIf
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                retValFsipv6IfUnnumAssocIPIf
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfUnnumAssocIPIf (INT4 i4Fsipv6IfIndex,
                              INT4 *pi4RetValFsipv6IfUnnumAssocIPIf)
{
    tLip6If            *pIf6Entry = NULL;

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfIndex);
    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsipv6IfUnnumAssocIPIf = (INT4) pIf6Entry->u4UnnumAssocIPv6If;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfRedirectMsg
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                retValFsipv6IfRedirectMsg
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfRedirectMsg (INT4 i4Fsipv6IfIndex,
                           INT4 *pi4RetValFsipv6IfRedirectMsg)
{

    tLip6If            *pIf6Entry = NULL;
    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsipv6IfRedirectMsg =
        pIf6Entry->Icmp6ErrRLInfo.i4Icmp6RedirectMsg;

    return SNMP_SUCCESS;

}

/****************************************************************************
* + Function    :  nmhGetFsipv6IfAdvSrcLLAdr
* + Input       :  The Indices
* +                Fsipv6IfIndex
* +
* +                The Object
* +                retValFsipv6IfAdvSrcLLAdr
* + Output      :  The Get Low Lev Routine Take the Indices &
* +                store the Value requested in the Return val.
* + Returns     :  SNMP_SUCCESS or SNMP_FAILURE
* +****************************************************************************/
INT1
nmhGetFsipv6IfAdvSrcLLAdr (INT4 i4Fsipv6IfIndex,
                           INT4 *pi4RetValFsipv6IfAdvSrcLLAdr)
{
    tLip6If            *pIf6Entry = NULL;

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    if ((pIf6Entry->u1RALinkLocalStatus == IP6_RA_ADV_LINKLOCAL) ||
        (pIf6Entry->u1RALinkLocalStatus == IP6_RA_NO_ADV_LINKLOCAL))
    {
        *pi4RetValFsipv6IfAdvSrcLLAdr = pIf6Entry->u1RALinkLocalStatus;
        return SNMP_SUCCESS;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfAdvIntOpt
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                retValFsipv6IfAdvIntOpt
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfAdvIntOpt (INT4 i4Fsipv6IfIndex, INT4 *pi4RetValFsipv6IfAdvIntOpt)
{
    tLip6If            *pIf6Entry = NULL;

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pIf6Entry->u1RAInterval == IP6_RA_ADV_INTERVAL
        || IP6_RA_NO_ADV_INTERVAL)
    {
        *pi4RetValFsipv6IfAdvIntOpt = pIf6Entry->u1RAInterval;
        return SNMP_SUCCESS;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfForwarding
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                retValFsipv6IfForwarding
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfForwarding (INT4 i4Fsipv6IfIndex,
                          INT4 *pi4RetValFsipv6IfForwarding)
{
    tLip6If            *pIf6Entry = NULL;
    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsipv6IfForwarding = (INT4) pIf6Entry->u1Ipv6IfFwdOperStatus;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfIcmpErrInterval
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                retValFsipv6IfIcmpErrInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfIcmpErrInterval (INT4 i4Fsipv6IfIndex,
                               INT4 *pu4RetValFsipv6IfIcmpErrInterval)
{

    tLip6If            *pIf6Entry = NULL;

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsipv6IfIcmpErrInterval =
        pIf6Entry->Icmp6ErrRLInfo.i4Icmp6ErrInterval;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfIcmpTokenBucketSize
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                retValFsipv6IfIcmpTokenBucketSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfIcmpTokenBucketSize (INT4 i4Fsipv6IfIndex,
                                   INT4 *pi4RetValFsipv6IfIcmpTokenBucketSize)
{

    tLip6If            *pIf6Entry = NULL;

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsipv6IfIcmpTokenBucketSize =
        pIf6Entry->Icmp6ErrRLInfo.i4Icmp6BucketSize;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfRoutingStatus
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                retValFsipv6IfRoutingStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfRoutingStatus (INT4 i4Fsipv6IfIndex,
                             INT4 *pi4RetValFsipv6IfRoutingStatus)
{
    tLip6If            *pIf6 = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }
    if (gIp6GblInfo.i4Ip6Status == LIP6_FORW_ENABLE)
    {
        *pi4RetValFsipv6IfRoutingStatus = (INT4) pIf6->u1Ipv6IfFwdOperStatus;
    }
    else
    {
        *pi4RetValFsipv6IfRoutingStatus = (INT4) LIP6_FORW_DISABLE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfScopeZoneCreationStatus
 Input       :  The Indices
                Fsipv6ScopeZoneIndexIfIndex

                The Object
                retValFsipv6IfScopeZoneCreationStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfScopeZoneCreationStatus (INT4 i4Fsipv6ScopeZoneIndexIfIndex,
                                       INT4
                                       *pi4RetValFsipv6IfScopeZoneCreationStatus)
{
    UNUSED_PARAM (i4Fsipv6ScopeZoneIndexIfIndex);
    UNUSED_PARAM (pi4RetValFsipv6IfScopeZoneCreationStatus);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6ScopeZoneIndex
 Input       :  The Indices
                Fsipv6ScopeZoneName

                The Object
                retValFsipv6ScopeZoneIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6ScopeZoneIndex (tSNMP_OCTET_STRING_TYPE * pFsipv6ScopeZoneName,
                            UINT4 *pu4RetValFsipv6ScopeZoneIndex)
{
    UNUSED_PARAM (pFsipv6ScopeZoneName);
    UNUSED_PARAM (pu4RetValFsipv6ScopeZoneIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfScopeZoneRowStatus
 Input       :  The Indices
                Fsipv6ScopeZoneIndexIfIndex

                The Object
                retValFsipv6IfScopeZoneRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
***************************************************************************/
INT1
nmhGetFsipv6IfScopeZoneRowStatus (INT4 i4Fsipv6ScopeZoneIndexIfIndex,
                                  INT4 *pi4RetValFsipv6IfScopeZoneRowStatus)
{
    UNUSED_PARAM (i4Fsipv6ScopeZoneIndexIfIndex);
    UNUSED_PARAM (pi4RetValFsipv6IfScopeZoneRowStatus);
    return SNMP_SUCCESS;
}

/****************************************************************************
 *  Function    :  nmhGetFsipv6IfStatsIcmp6ErrRateLmtd
 *  Input       :  The Indices
 *                 Fsipv6IfStatsIndex
 *                 The Object
 *                 retValFsipv6IfStatsIcmp6ErrRateLmtd
 *  Output      :  The Get Low Lev Routine Take the Indices &
 *                 store the Value requested in the Return val.
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
INT1
nmhGetFsipv6IfStatsIcmp6ErrRateLmtd (INT4 i4Fsipv6IfStatsIndex,
                                     UINT4
                                     *pu4RetValFsipv6IfStatsIcmp6ErrRateLmtd)
{
    UNUSED_PARAM (i4Fsipv6IfStatsIndex);
    UNUSED_PARAM (pu4RetValFsipv6IfStatsIcmp6ErrRateLmtd);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IsDefaultScopeZone
 Input       :  The Indices
                Fsipv6ScopeZoneName

                The Object
                retValFsipv6IsDefaultScopeZone
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IsDefaultScopeZone (tSNMP_OCTET_STRING_TYPE * pFsipv6ScopeZoneName,
                                INT4 *pi4RetValFsipv6IsDefaultScopeZone)
{
    UNUSED_PARAM (pFsipv6ScopeZoneName);
    UNUSED_PARAM (pi4RetValFsipv6IsDefaultScopeZone);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6PingZoneId
 Input       :  The Indices
                Fsipv6PingIndex

                The Object
                retValFsipv6PingZoneId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6PingZoneId (INT4 i4Fsipv6PingIndex,
                        tSNMP_OCTET_STRING_TYPE * pRetValFsipv6PingZoneId)
{
    UNUSED_PARAM (i4Fsipv6PingIndex);
    UNUSED_PARAM (pRetValFsipv6PingZoneId);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6PingDestAddrType
 Input       :  The Indices
                Fsipv6PingIndex

                The Object 
                retValFsipv6PingDestAddrType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6PingDestAddrType (INT4 i4Fsipv6PingIndex,
                              INT4 *pi4RetValFsipv6PingDestAddrType)
{
    UNUSED_PARAM (i4Fsipv6PingIndex);
    UNUSED_PARAM (pi4RetValFsipv6PingDestAddrType);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6RFC5095Compatibility
 Input       :  The Indices

                The Object
                retValFsipv6RFC5095Compatibility
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6RFC5095Compatibility (INT4 *pi4RetValFsipv6RFC5095Compatibility)
{
    *pi4RetValFsipv6RFC5095Compatibility =
        (INT4) gIp6GblInfo.u1RFC5095Compatibility;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsipv6RFC5942Compatibility
 Input       :  The Indices

                The Object
                retValFsipv6RFC5942Compatibility
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6RFC5942Compatibility (INT4 *pi4RetValFsipv6RFC5942Compatibility)
{
    *pi4RetValFsipv6RFC5942Compatibility =
        (INT4) gIp6GblInfo.u1RFC5942Compatibility;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsipv6ScopeZoneCreationStatus
 Input       :  The Indices
                Fsipv6ScopeZoneName

                The Object
                retValFsipv6ScopeZoneCreationStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6ScopeZoneCreationStatus (tSNMP_OCTET_STRING_TYPE *
                                     pFsipv6ScopeZoneName,
                                     INT4
                                     *pi4RetValFsipv6ScopeZoneCreationStatus)
{
    UNUSED_PARAM (pFsipv6ScopeZoneName);
    UNUSED_PARAM (pi4RetValFsipv6ScopeZoneCreationStatus);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6ScopeZoneIndex3
 Input       :  The Indices
                Fsipv6ScopeZoneIndexIfIndex

                The Object
                retValFsipv6ScopeZoneIndex3
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6ScopeZoneIndex3 (INT4 i4Fsipv6ScopeZoneIndexIfIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pRetValFsipv6ScopeZoneIndex3)
{
    UNUSED_PARAM (i4Fsipv6ScopeZoneIndexIfIndex);
    UNUSED_PARAM (pRetValFsipv6ScopeZoneIndex3);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6ScopeZoneIndex6
 Input       :  The Indices
                Fsipv6ScopeZoneIndexIfIndex

                The Object
                retValFsipv6ScopeZoneIndex6
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6ScopeZoneIndex6 (INT4 i4Fsipv6ScopeZoneIndexIfIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pRetValFsipv6ScopeZoneIndex6)
{
    UNUSED_PARAM (i4Fsipv6ScopeZoneIndexIfIndex);
    UNUSED_PARAM (pRetValFsipv6ScopeZoneIndex6);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6ScopeZoneIndex7
 Input       :  The Indices
                Fsipv6ScopeZoneIndexIfIndex

                The Object
                retValFsipv6ScopeZoneIndex7
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6ScopeZoneIndex7 (INT4 i4Fsipv6ScopeZoneIndexIfIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pRetValFsipv6ScopeZoneIndex7)
{
    UNUSED_PARAM (i4Fsipv6ScopeZoneIndexIfIndex);
    UNUSED_PARAM (pRetValFsipv6ScopeZoneIndex7);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6ScopeZoneIndex9
 Input       :  The Indices
                Fsipv6ScopeZoneIndexIfIndex

                The Object
                retValFsipv6ScopeZoneIndex9
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6ScopeZoneIndex9 (INT4 i4Fsipv6ScopeZoneIndexIfIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pRetValFsipv6ScopeZoneIndex9)
{
    UNUSED_PARAM (i4Fsipv6ScopeZoneIndexIfIndex);
    UNUSED_PARAM (pRetValFsipv6ScopeZoneIndex9);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6ScopeZoneIndexA
 Input       :  The Indices
                Fsipv6ScopeZoneIndexIfIndex

                The Object
                retValFsipv6ScopeZoneIndexA
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6ScopeZoneIndexA (INT4 i4Fsipv6ScopeZoneIndexIfIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pRetValFsipv6ScopeZoneIndexA)
{
    UNUSED_PARAM (i4Fsipv6ScopeZoneIndexIfIndex);
    UNUSED_PARAM (pRetValFsipv6ScopeZoneIndexA);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6ScopeZoneIndexAdminLocal
 Input       :  The Indices
                Fsipv6ScopeZoneIndexIfIndex

                The Object
                retValFsipv6ScopeZoneIndexAdminLocal
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6ScopeZoneIndexAdminLocal (INT4 i4Fsipv6ScopeZoneIndexIfIndex,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pRetValFsipv6ScopeZoneIndexAdminLocal)
{
    UNUSED_PARAM (i4Fsipv6ScopeZoneIndexIfIndex);
    UNUSED_PARAM (pRetValFsipv6ScopeZoneIndexAdminLocal);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6ScopeZoneIndexB
 Input       :  The Indices
                Fsipv6ScopeZoneIndexIfIndex

                The Object
                retValFsipv6ScopeZoneIndexB
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6ScopeZoneIndexB (INT4 i4Fsipv6ScopeZoneIndexIfIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pRetValFsipv6ScopeZoneIndexB)
{
    UNUSED_PARAM (i4Fsipv6ScopeZoneIndexIfIndex);
    UNUSED_PARAM (pRetValFsipv6ScopeZoneIndexB);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6ScopeZoneIndexC
 Input       :  The Indices
                Fsipv6ScopeZoneIndexIfIndex

                The Object
                retValFsipv6ScopeZoneIndexC
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetFsipv6ScopeZoneIndexC (INT4 i4Fsipv6ScopeZoneIndexIfIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pRetValFsipv6ScopeZoneIndexC)
{
    UNUSED_PARAM (i4Fsipv6ScopeZoneIndexIfIndex);
    UNUSED_PARAM (pRetValFsipv6ScopeZoneIndexC);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6ScopeZoneIndexD
 Input       :  The Indices
                Fsipv6ScopeZoneIndexIfIndex

                The Object
                retValFsipv6ScopeZoneIndexD
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6ScopeZoneIndexD (INT4 i4Fsipv6ScopeZoneIndexIfIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pRetValFsipv6ScopeZoneIndexD)
{
    UNUSED_PARAM (i4Fsipv6ScopeZoneIndexIfIndex);
    UNUSED_PARAM (pRetValFsipv6ScopeZoneIndexD);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6ScopeZoneIndexE
 Input       :  The Indices
                Fsipv6ScopeZoneIndexIfIndex

                The Object
                retValFsipv6ScopeZoneIndexE
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6ScopeZoneIndexE (INT4 i4Fsipv6ScopeZoneIndexIfIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pRetValFsipv6ScopeZoneIndexE)
{
    UNUSED_PARAM (i4Fsipv6ScopeZoneIndexIfIndex);
    UNUSED_PARAM (pRetValFsipv6ScopeZoneIndexE);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6ScopeZoneIndexInterfaceLocal
 Input       :  The Indices
                Fsipv6ScopeZoneIndexIfIndex

                The Object
                retValFsipv6ScopeZoneIndexInterfaceLocal
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6ScopeZoneIndexInterfaceLocal (INT4 i4Fsipv6ScopeZoneIndexIfIndex,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pRetValFsipv6ScopeZoneIndexInterfaceLocal)
{
    UNUSED_PARAM (i4Fsipv6ScopeZoneIndexIfIndex);
    UNUSED_PARAM (pRetValFsipv6ScopeZoneIndexInterfaceLocal);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6ScopeZoneIndexLinkLocal
 Input       :  The Indices
                Fsipv6ScopeZoneIndexIfIndex

                The Object
                retValFsipv6ScopeZoneIndexLinkLocal
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6ScopeZoneIndexLinkLocal (INT4 i4Fsipv6ScopeZoneIndexIfIndex,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pRetValFsipv6ScopeZoneIndexLinkLocal)
{
    UNUSED_PARAM (i4Fsipv6ScopeZoneIndexIfIndex);
    UNUSED_PARAM (pRetValFsipv6ScopeZoneIndexLinkLocal);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6ScopeZoneIndexOrganizationLocal
 Input       :  The Indices
                Fsipv6ScopeZoneIndexIfIndex

                The Object
                retValFsipv6ScopeZoneIndexOrganizationLocal
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6ScopeZoneIndexOrganizationLocal (INT4 i4Fsipv6ScopeZoneIndexIfIndex,
                                             tSNMP_OCTET_STRING_TYPE *
                                             pRetValFsipv6ScopeZoneIndexOrganizationLocal)
{
    UNUSED_PARAM (i4Fsipv6ScopeZoneIndexIfIndex);
    UNUSED_PARAM (pRetValFsipv6ScopeZoneIndexOrganizationLocal);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6ScopeZoneIndexSiteLocal
 Input       :  The Indices
                Fsipv6ScopeZoneIndexIfIndex

                The Object
                retValFsipv6ScopeZoneIndexSiteLocal
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6ScopeZoneIndexSiteLocal (INT4 i4Fsipv6ScopeZoneIndexIfIndex,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pRetValFsipv6ScopeZoneIndexSiteLocal)
{
    UNUSED_PARAM (i4Fsipv6ScopeZoneIndexIfIndex);
    UNUSED_PARAM (pRetValFsipv6ScopeZoneIndexSiteLocal);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6ScopeZoneInterfaceList
 Input       :  The Indices
                Fsipv6ScopeZoneName

                The Object
                retValFsipv6ScopeZoneInterfaceList
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6ScopeZoneInterfaceList (tSNMP_OCTET_STRING_TYPE *
                                    pFsipv6ScopeZoneName,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pRetValFsipv6ScopeZoneInterfaceList)
{
    UNUSED_PARAM (pFsipv6ScopeZoneName);
    UNUSED_PARAM (pRetValFsipv6ScopeZoneInterfaceList);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsipv6AddrSelPolicyTable
 Input       :  The Indices
                Fsipv6AddrSelPolicyPrefix
                Fsipv6AddrSelPolicyPrefixLen
                Fsipv6AddrSelPolicyIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1                nmhValidateIndexInstanceFsipv6AddrSelPolicyTable
    (tSNMP_OCTET_STRING_TYPE * pFsipv6AddrSelPolicyPrefix,
     INT4 i4Fsipv6AddrSelPolicyPrefixLen, INT4 i4Fsipv6AddrSelPolicyIfIndex)
{
    UNUSED_PARAM (pFsipv6AddrSelPolicyPrefix);
    UNUSED_PARAM (i4Fsipv6AddrSelPolicyPrefixLen);
    UNUSED_PARAM (i4Fsipv6AddrSelPolicyIfIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsipv6IfScopeZoneMapTable
 Input       :  The Indices
                Fsipv6ScopeZoneIndexIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsipv6IfScopeZoneMapTable (INT4
                                                   i4Fsipv6ScopeZoneIndexIfIndex)
{
    UNUSED_PARAM (i4Fsipv6ScopeZoneIndexIfIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsipv6ScopeZoneTable
 Input       :  The Indices
                Fsipv6ScopeZoneName
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 
     
     
     
     
     
     
     
    nmhValidateIndexInstanceFsipv6ScopeZoneTable
    (tSNMP_OCTET_STRING_TYPE * pFsipv6ScopeZoneName)
{

    UNUSED_PARAM (pFsipv6ScopeZoneName);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */
/*************************************************************************
Function    :  nmhTestv2Fsipv6AddrSelPolicyAddrType
 Input       :  The Indices
                Fsipv6AddrSelPolicyPrefix
                Fsipv6AddrSelPolicyPrefixLen
                Fsipv6AddrSelPolicyIfIndex

                The Object
                testValFsipv6AddrSelPolicyAddrType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6AddrSelPolicyAddrType (UINT4 *pu4ErrorCode,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pFsipv6AddrSelPolicyPrefix,
                                      INT4 i4Fsipv6AddrSelPolicyPrefixLen,
                                      INT4 i4Fsipv6AddrSelPolicyIfIndex,
                                      INT4 i4TestValFsipv6AddrSelPolicyAddrType)
{
    INT1                i1AddrType;

    /* Prefix Validation */
    i1AddrType = Ip6AddrType ((tIp6Addr *) (VOID *) pFsipv6AddrSelPolicyPrefix->
                              pu1_OctetList);
    if (i1AddrType != ADDR6_UNICAST)

    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_ADDR_TYPE);
        return SNMP_FAILURE;
    }

    /* Prefix Len Validation */
    if ((i4Fsipv6AddrSelPolicyPrefixLen < IP6_ADDR_MIN_PREFIX) ||
        (i4Fsipv6AddrSelPolicyPrefixLen > IP6_ADDR_MAX_PREFIX))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_PREFIXLEN);
        return SNMP_FAILURE;
    }
    if ((i4TestValFsipv6AddrSelPolicyAddrType < ADDR6_UNICAST) ||
        (i4TestValFsipv6AddrSelPolicyAddrType > ADDR6_ANYCAST))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_INVALID_LABEL);
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (i4Fsipv6AddrSelPolicyIfIndex);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6AddrSelPolicyLabel
 Input       :  The Indices
                Fsipv6AddrSelPolicyPrefix
                Fsipv6AddrSelPolicyPrefixLen
                Fsipv6AddrSelPolicyIfIndex

                The Object
                testValFsipv6AddrSelPolicyLabel
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6AddrSelPolicyLabel (UINT4 *pu4ErrorCode,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsipv6AddrSelPolicyPrefix,
                                   INT4 i4Fsipv6AddrSelPolicyPrefixLen,
                                   INT4 i4Fsipv6AddrSelPolicyIfIndex,
                                   INT4 i4TestValFsipv6AddrSelPolicyLabel)
{
    INT1                i1AddrType;

    /* Prefix Validation */
    i1AddrType = Ip6AddrType ((tIp6Addr *) (VOID *) pFsipv6AddrSelPolicyPrefix->
                              pu1_OctetList);
    if (i1AddrType != ADDR6_UNICAST)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_ADDR_TYPE);
        return SNMP_FAILURE;
    }

    /* Prefix Len Validation */
    if ((i4Fsipv6AddrSelPolicyPrefixLen < IP6_ADDR_MIN_PREFIX) ||
        (i4Fsipv6AddrSelPolicyPrefixLen > IP6_ADDR_MAX_PREFIX))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_PREFIXLEN);
        return SNMP_FAILURE;
    }

    if ((i4TestValFsipv6AddrSelPolicyLabel < IP6_LABEL_MIN) ||
        (i4TestValFsipv6AddrSelPolicyLabel > IP6_LABEL_MAX))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_INVALID_LABEL);
        return SNMP_FAILURE;
    }
    UNUSED_PARAM (i4Fsipv6AddrSelPolicyIfIndex);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6AddrSelPolicyPrecedence
 Input       :  The Indices
                Fsipv6AddrSelPolicyPrefix
                Fsipv6AddrSelPolicyPrefixLen
                Fsipv6AddrSelPolicyIfIndex

                The Object
                testValFsipv6AddrSelPolicyPrecedence
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6AddrSelPolicyPrecedence (UINT4 *pu4ErrorCode,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pFsipv6AddrSelPolicyPrefix,
                                        INT4 i4Fsipv6AddrSelPolicyPrefixLen,
                                        INT4 i4Fsipv6AddrSelPolicyIfIndex,
                                        INT4
                                        i4TestValFsipv6AddrSelPolicyPrecedence)
{
    INT1                i1AddrType;

    /* Prefix Validation */
    i1AddrType = Ip6AddrType ((tIp6Addr *) (VOID *)
                              pFsipv6AddrSelPolicyPrefix->pu1_OctetList);
    if (i1AddrType != ADDR6_UNICAST)

    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_ADDR_TYPE);
        return SNMP_FAILURE;
    }

    /* Prefix Len Validation */
    if ((i4Fsipv6AddrSelPolicyPrefixLen < IP6_ADDR_MIN_PREFIX) ||
        (i4Fsipv6AddrSelPolicyPrefixLen > IP6_ADDR_MAX_PREFIX))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_PREFIXLEN);
        return SNMP_FAILURE;
    }

    if ((i4TestValFsipv6AddrSelPolicyPrecedence < IP6_PRECEDENCE_MIN) ||
        (i4TestValFsipv6AddrSelPolicyPrecedence > IP6_PRECEDENCE_MAX))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_INVALID_PRECEDENCE);
        return SNMP_FAILURE;
    }
    UNUSED_PARAM (i4Fsipv6AddrSelPolicyIfIndex);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6AddrSelPolicyRowStatus
 Input       :  The Indices
                Fsipv6AddrSelPolicyPrefix
                Fsipv6AddrSelPolicyPrefixLen
                Fsipv6AddrSelPolicyIfIndex

                The Object
                testValFsipv6AddrSelPolicyRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6AddrSelPolicyRowStatus (UINT4 *pu4ErrorCode,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFsipv6AddrSelPolicyPrefix,
                                       INT4 i4Fsipv6AddrSelPolicyPrefixLen,
                                       INT4 i4Fsipv6AddrSelPolicyIfIndex,
                                       INT4
                                       i4TestValFsipv6AddrSelPolicyRowStatus)
{
    INT1                i1AddrType;

    /* Prefix Validation */
    i1AddrType = Ip6AddrType ((tIp6Addr *) (VOID *)
                              pFsipv6AddrSelPolicyPrefix->pu1_OctetList);
    if (i1AddrType != ADDR6_UNICAST)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_ADDR_TYPE);
        return SNMP_FAILURE;
    }

    /* Prefix Len Validation */
    if ((i4Fsipv6AddrSelPolicyPrefixLen < 0) ||
        (i4Fsipv6AddrSelPolicyPrefixLen > IP6_ADDR_MAX_PREFIX))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_PREFIXLEN);
        return SNMP_FAILURE;
    }
    if ((i4TestValFsipv6AddrSelPolicyRowStatus != IP6FWD_CREATE_AND_WAIT) &&
        (i4TestValFsipv6AddrSelPolicyRowStatus != IP6FWD_ACTIVE) &&
        (i4TestValFsipv6AddrSelPolicyRowStatus != IP6FWD_DESTROY) &&
        (i4TestValFsipv6AddrSelPolicyRowStatus != IP6FWD_NOT_IN_SERVICE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_INVALID_PREF_ADMIN_STATUS);
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (i4Fsipv6AddrSelPolicyIfIndex);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6IfDestUnreachableMsg
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                testValFsipv6IfDestUnreachableMsg
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6IfDestUnreachableMsg (UINT4 *pu4ErrorCode, INT4 i4Fsipv6IfIndex,
                                     INT4 i4TestValFsipv6IfDestUnreachableMsg)
{

    tLip6If            *pIf6Entry = NULL;
    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfIndex);
    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    if ((i4TestValFsipv6IfDestUnreachableMsg < ICMP6_DEST_UNREACHABLE_ENABLE) ||
        (i4TestValFsipv6IfDestUnreachableMsg > ICMP6_DEST_UNREACHABLE_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6IfUnnumAssocIPIf
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                testValFsipv6IfUnnumAssocIPIf
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6IfUnnumAssocIPIf (UINT4 *pu4ErrorCode, INT4 i4Fsipv6IfIndex,
                                 INT4 i4TestValFsipv6IfUnnumAssocIPIf)
{
    tLip6If            *pIf6Entry = NULL;
    tLip6If            *pAssocIf6 = NULL;

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfIndex);
    if (pIf6Entry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
        return SNMP_FAILURE;
    }

    if (TMO_SLL_Count (&pIf6Entry->Ip6AddrList) > 0)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        CLI_SET_ERR (CLI_IP6_ADDR_PRESENT_FOR_UNNUM);
        return SNMP_FAILURE;
    }

    /* To Unassociate unnumbered interface the value of
     * i4TestValFsipv6IfUnnumAssocIPIf should be 0 */
    if (i4TestValFsipv6IfUnnumAssocIPIf == 0)
    {
        return SNMP_SUCCESS;
    }

    pAssocIf6 = Lip6UtlGetIfEntry ((UINT4) i4TestValFsipv6IfUnnumAssocIPIf);
    if (pAssocIf6 == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_INVALID_UNNUM_ASSOC_IF);
        return SNMP_FAILURE;
    }

    if ((pAssocIf6->u1IfType != IP6_L3VLAN_INTERFACE_TYPE) &&
        (pAssocIf6->u1IfType != IP6_LAGG_INTERFACE_TYPE) &&
        (pAssocIf6->u1IfType != IP6_ENET_INTERFACE_TYPE) &&
        (pAssocIf6->u1IfType != IP6_LOOPBACK_INTERFACE_TYPE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_INVALID_UNNUM_INDEX_TYPE);
        return SNMP_FAILURE;
    }

    if (TMO_SLL_Count (&pAssocIf6->Ip6AddrList) == 0)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_NO_UNNUM_INDEX_ADDR);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6IfRedirectMsg
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                testValFsipv6IfRedirectMsg
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6IfRedirectMsg (UINT4 *pu4ErrorCode, INT4 i4Fsipv6IfIndex,
                              INT4 i4TestValFsipv6IfRedirectMsg)
{

    tLip6If            *pIf6Entry = NULL;
    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfIndex);
    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    if ((i4TestValFsipv6IfRedirectMsg == ICMP6_REDIRECT_ENABLE) ||
        (i4TestValFsipv6IfRedirectMsg == ICMP6_REDIRECT_DISABLE))
    {
        return SNMP_SUCCESS;
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;

}

/****************************************************************************
* + Function    :  nmhTestv2Fsipv6IfAdvSrcLLAdr
* + Input       :  The Indices
* +                Fsipv6IfIndex
* +
* +                The Object
* +                testValFsipv6IfAdvSrcLLAdr
* + Output      :  The Test Low Lev Routine Take the Indices &
* +                Test whether that Value is Valid Input for Set.
* +                Stores the value of error code in the Return val
* + Error Codes :  The following error codes are to be returned
* +                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
* +                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
* +                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
* +                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
* +                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
* + Returns     :  SNMP_SUCCESS or SNMP_FAILURE
* +****************************************************************************/
INT1
nmhTestv2Fsipv6IfAdvSrcLLAdr (UINT4 *pu4ErrorCode, INT4 i4Fsipv6IfIndex,
                              INT4 i4TestValFsipv6IfAdvSrcLLAdr)
{
    if (Lip6UtlIfEntryExists ((UINT4) i4Fsipv6IfIndex) == OSIX_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (i4TestValFsipv6IfAdvSrcLLAdr != IP6_RA_ADV_LINKLOCAL &&
        i4TestValFsipv6IfAdvSrcLLAdr != IP6_RA_NO_ADV_LINKLOCAL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6IfAdvIntOpt
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                testValFsipv6IfAdvIntOpt
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6IfAdvIntOpt (UINT4 *pu4ErrorCode, INT4 i4Fsipv6IfIndex,
                            INT4 i4TestValFsipv6IfAdvIntOpt)
{
    if (Lip6UtlIfEntryExists ((UINT4) i4Fsipv6IfIndex) == OSIX_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (i4TestValFsipv6IfAdvIntOpt != IP6_RA_ADV_INTERVAL &&
        i4TestValFsipv6IfAdvIntOpt != IP6_RA_NO_ADV_INTERVAL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6IfForwarding
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                testValFsipv6IfForwarding
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6IfForwarding (UINT4 *pu4ErrorCode, INT4 i4Fsipv6IfIndex,
                             INT4 i4TestValFsipv6IfForwarding)
{
    tLip6If            *pIf6Entry = NULL;

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfIndex);
    if (pIf6Entry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
        return SNMP_FAILURE;
    }

    if (i4TestValFsipv6IfForwarding == IP6_IF_FORW_ENABLE ||
        i4TestValFsipv6IfForwarding == IP6_IF_FORW_DISABLE)
    {
        return (SNMP_SUCCESS);
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6IfIcmpErrInterval
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                testValFsipv6IfIcmpErrInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6IfIcmpErrInterval (UINT4 *pu4ErrorCode, INT4 i4Fsipv6IfIndex,
                                  INT4 u4TestValFsipv6IfIcmpErrInterval)
{

    tLip6If            *pIf6Entry = NULL;
    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfIndex);
    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }
    if ((u4TestValFsipv6IfIcmpErrInterval >= ICMP6_RATE_LIMIT_DISABLE) &&
        (u4TestValFsipv6IfIcmpErrInterval <= ICMP6_MAX_ERR_INTERVAL))
    {
        return SNMP_SUCCESS;
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6IfIcmpTokenBucketSize
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                testValFsipv6IfIcmpTokenBucketSize
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6IfIcmpTokenBucketSize (UINT4 *pu4ErrorCode, INT4 i4Fsipv6IfIndex,
                                      INT4 i4TestValFsipv6IfIcmpTokenBucketSize)
{

    tLip6If            *pIf6Entry = NULL;
    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfIndex);
    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }
    if ((i4TestValFsipv6IfIcmpTokenBucketSize >= ICMP6_RATE_LIMIT_DISABLE) &&
        (i4TestValFsipv6IfIcmpTokenBucketSize <= ICMP6_MAX_BUCKET_SIZE))
    {
        return SNMP_SUCCESS;
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6IfScopeZoneRowStatus
 Input       :  The Indices
                Fsipv6ScopeZoneIndexIfIndex
4TestValFsipv6IfIcmpTokenBucketSize <= ICMP6_MAX_BUCKET_SIZE))
    {
        return SNMP_SUCCESS;
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;

                The Object
                testValFsipv6IfScopeZoneRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6IfScopeZoneRowStatus (UINT4 *pu4ErrorCode,
                                     INT4 i4Fsipv6ScopeZoneIndexIfIndex,
                                     INT4 i4TestValFsipv6IfScopeZoneRowStatus)
{
    tLip6If            *pIf6Entry = NULL;
    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6ScopeZoneIndexIfIndex);

    if (pIf6Entry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
        return SNMP_FAILURE;
    }

    if ((i4TestValFsipv6IfScopeZoneRowStatus < IP6_ZONE_ROW_STATUS_ACTIVE) ||
        (i4TestValFsipv6IfScopeZoneRowStatus > IP6_ZONE_ROW_STATUS_DESTROY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_INVALID_ROW_STATUS_FOR_ZONE);
        return SNMP_FAILURE;

    }
    if (pIf6Entry->u1ZoneRowStatus == IP6_ZONE_ROW_STATUS_ACTIVE)
    {
        if ((i4TestValFsipv6IfScopeZoneRowStatus ==
             IP6_ZONE_ROW_STATUS_CREATE_AND_GO)
            || (i4TestValFsipv6IfScopeZoneRowStatus ==
                IP6_ZONE_ROW_STATUS_CREATE_AND_WAIT))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_IP6_INVALID_ROW_STATUS_FOR_ZONE);
            return SNMP_FAILURE;
        }
    }
    if ((i4TestValFsipv6IfScopeZoneRowStatus ==
         IP6_ZONE_ROW_STATUS_NOT_IN_SERVICE)
        && (pIf6Entry->u1ZoneRowStatus != IP6_ZONE_ROW_STATUS_ACTIVE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_IP6_INVALID_ROW_STATUS_FOR_ZONE);
        return SNMP_FAILURE;
    }
    if ((i4TestValFsipv6IfScopeZoneRowStatus == IP6_ZONE_ROW_STATUS_DESTROY) &&
        (pIf6Entry->u1ZoneRowStatus != IP6_ZONE_ROW_STATUS_ACTIVE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_IP6_INVALID_ROW_STATUS_FOR_ZONE);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6IsDefaultScopeZone
 Input       :  The Indices
                Fsipv6ScopeZoneName

                The Object
                testValFsipv6IsDefaultScopeZone
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6IsDefaultScopeZone (UINT4 *pu4ErrorCode,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsipv6ScopeZoneName,
                                   INT4 i4TestValFsipv6IsDefaultScopeZone)
{

    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pFsipv6ScopeZoneName);
    UNUSED_PARAM (i4TestValFsipv6IsDefaultScopeZone);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6PingZoneId
 Input       :  The Indices
                Fsipv6PingIndex

                The Object
                testValFsipv6PingZoneId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6PingZoneId (UINT4 *pu4ErrorCode,
                           INT4 i4Fsipv6PingIndex,
                           tSNMP_OCTET_STRING_TYPE * pTestValFsipv6PingZoneId)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4Fsipv6PingIndex);
    UNUSED_PARAM (pTestValFsipv6PingZoneId);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6PingDestAddrType
 Input       :  The Indices
                Fsipv6PingIndex

                The Object 
                testValFsipv6PingDestAddrType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6PingDestAddrType (UINT4 *pu4ErrorCode, INT4 i4Fsipv6PingIndex,
                                 INT4 i4TestValFsipv6PingDestAddrType)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4Fsipv6PingIndex);
    UNUSED_PARAM (i4TestValFsipv6PingDestAddrType);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6RFC5095Compatibility
 Input       :  The Indices

                The Object
                testValFsipv6RFC5095Compatibility
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6RFC5095Compatibility (UINT4 *pu4ErrorCode,
                                     INT4 i4TestValFsipv6RFC5095Compatibility)
{
    if ((i4TestValFsipv6RFC5095Compatibility != IP6_RFC5095_COMPATIBLE) &&
        (i4TestValFsipv6RFC5095Compatibility != IP6_RFC5095_NOT_COMPATIBLE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6RFC5942Compatibility
 Input       :  The Indices

                The Object
                testValFsipv6RFC5942Compatibility
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6RFC5942Compatibility (UINT4 *pu4ErrorCode,
                                     INT4 i4TestValFsipv6RFC5942Compatibility)
{

    if ((i4TestValFsipv6RFC5942Compatibility != IP6_RFC5942_COMPATIBLE) &&
        (i4TestValFsipv6RFC5942Compatibility != IP6_RFC5942_NOT_COMPATIBLE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6ScopeZoneIndex3
 Input       :  The Indices
                Fsipv6ScopeZoneIndexIfIndex

                The Object
                testValFsipv6ScopeZoneIndex3
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6ScopeZoneIndex3 (UINT4 *pu4ErrorCode,
                                INT4 i4Fsipv6ScopeZoneIndexIfIndex,
                                tSNMP_OCTET_STRING_TYPE *
                                pTestValFsipv6ScopeZoneIndex3)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4Fsipv6ScopeZoneIndexIfIndex);
    UNUSED_PARAM (pTestValFsipv6ScopeZoneIndex3);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6ScopeZoneIndex6
 Input       :  The Indices
                Fsipv6ScopeZoneIndexIfIndex

                The Object
                testValFsipv6ScopeZoneIndex6
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6ScopeZoneIndex6 (UINT4 *pu4ErrorCode,
                                INT4 i4Fsipv6ScopeZoneIndexIfIndex,
                                tSNMP_OCTET_STRING_TYPE *
                                pTestValFsipv6ScopeZoneIndex6)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4Fsipv6ScopeZoneIndexIfIndex);
    UNUSED_PARAM (pTestValFsipv6ScopeZoneIndex6);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6ScopeZoneIndex7
 Input       :  The Indices
                Fsipv6ScopeZoneIndexIfIndex

                The Object
                testValFsipv6ScopeZoneIndex7
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6ScopeZoneIndex7 (UINT4 *pu4ErrorCode,
                                INT4 i4Fsipv6ScopeZoneIndexIfIndex,
                                tSNMP_OCTET_STRING_TYPE *
                                pTestValFsipv6ScopeZoneIndex7)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4Fsipv6ScopeZoneIndexIfIndex);
    UNUSED_PARAM (pTestValFsipv6ScopeZoneIndex7);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6ScopeZoneIndex9
 Input       :  The Indices
                Fsipv6ScopeZoneIndexIfIndex

                The Object
                testValFsipv6ScopeZoneIndex9
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6ScopeZoneIndex9 (UINT4 *pu4ErrorCode,
                                INT4 i4Fsipv6ScopeZoneIndexIfIndex,
                                tSNMP_OCTET_STRING_TYPE *
                                pTestValFsipv6ScopeZoneIndex9)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4Fsipv6ScopeZoneIndexIfIndex);
    UNUSED_PARAM (pTestValFsipv6ScopeZoneIndex9);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6ScopeZoneIndexA
 Input       :  The Indices
                Fsipv6ScopeZoneIndexIfIndex

                The Object
                testValFsipv6ScopeZoneIndexA
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6ScopeZoneIndexA (UINT4 *pu4ErrorCode,
                                INT4 i4Fsipv6ScopeZoneIndexIfIndex,
                                tSNMP_OCTET_STRING_TYPE *
                                pTestValFsipv6ScopeZoneIndexA)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4Fsipv6ScopeZoneIndexIfIndex);
    UNUSED_PARAM (pTestValFsipv6ScopeZoneIndexA);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6ScopeZoneIndexAdminLocal
 Input       :  The Indices
                Fsipv6ScopeZoneIndexIfIndex

                The Object
                testValFsipv6ScopeZoneIndexAdminLocal
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6ScopeZoneIndexAdminLocal (UINT4 *pu4ErrorCode,
                                         INT4 i4Fsipv6ScopeZoneIndexIfIndex,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pTestValFsipv6ScopeZoneIndexAdminLocal)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4Fsipv6ScopeZoneIndexIfIndex);
    UNUSED_PARAM (pTestValFsipv6ScopeZoneIndexAdminLocal);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6ScopeZoneIndexB
 Input       :  The Indices
                Fsipv6ScopeZoneIndexIfIndex

                The Object
                testValFsipv6ScopeZoneIndexB
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6ScopeZoneIndexB (UINT4 *pu4ErrorCode,
                                INT4 i4Fsipv6ScopeZoneIndexIfIndex,
                                tSNMP_OCTET_STRING_TYPE *
                                pTestValFsipv6ScopeZoneIndexB)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4Fsipv6ScopeZoneIndexIfIndex);
    UNUSED_PARAM (pTestValFsipv6ScopeZoneIndexB);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6ScopeZoneIndexC
 Input       :  The Indices
                Fsipv6ScopeZoneIndexIfIndex

                The Object
                testValFsipv6ScopeZoneIndexC
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6ScopeZoneIndexC (UINT4 *pu4ErrorCode,
                                INT4 i4Fsipv6ScopeZoneIndexIfIndex,
                                tSNMP_OCTET_STRING_TYPE *
                                pTestValFsipv6ScopeZoneIndexC)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4Fsipv6ScopeZoneIndexIfIndex);
    UNUSED_PARAM (pTestValFsipv6ScopeZoneIndexC);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6ScopeZoneIndexD
 Input       :  The Indices
                Fsipv6ScopeZoneIndexIfIndex

                The Object
                testValFsipv6ScopeZoneIndexD
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6ScopeZoneIndexD (UINT4 *pu4ErrorCode,
                                INT4 i4Fsipv6ScopeZoneIndexIfIndex,
                                tSNMP_OCTET_STRING_TYPE *
                                pTestValFsipv6ScopeZoneIndexD)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4Fsipv6ScopeZoneIndexIfIndex);
    UNUSED_PARAM (pTestValFsipv6ScopeZoneIndexD);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6ScopeZoneIndexE
 Input       :  The Indices
                Fsipv6ScopeZoneIndexIfIndex

                The Object
                testValFsipv6ScopeZoneIndexE
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6ScopeZoneIndexE (UINT4 *pu4ErrorCode,
                                INT4 i4Fsipv6ScopeZoneIndexIfIndex,
                                tSNMP_OCTET_STRING_TYPE *
                                pTestValFsipv6ScopeZoneIndexE)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4Fsipv6ScopeZoneIndexIfIndex);
    UNUSED_PARAM (pTestValFsipv6ScopeZoneIndexE);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6ScopeZoneIndexInterfaceLocal
 Input       :  The Indices
                Fsipv6ScopeZoneIndexIfIndex

                The Object
                testValFsipv6ScopeZoneIndexInterfaceLocal
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhTestv2Fsipv6ScopeZoneIndexInterfaceLocal
    (UINT4 *pu4ErrorCode,
     INT4 i4Fsipv6ScopeZoneIndexIfIndex,
     tSNMP_OCTET_STRING_TYPE * pTestValFsipv6ScopeZoneIndexInterfaceLocal)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4Fsipv6ScopeZoneIndexIfIndex);
    UNUSED_PARAM (pTestValFsipv6ScopeZoneIndexInterfaceLocal);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6ScopeZoneIndexLinkLocal
 Input       :  The Indices
                Fsipv6ScopeZoneIndexIfIndex

                The Object
                testValFsipv6ScopeZoneIndexLinkLocal
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6ScopeZoneIndexLinkLocal (UINT4 *pu4ErrorCode,
                                        INT4 i4Fsipv6ScopeZoneIndexIfIndex,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pTestValFsipv6ScopeZoneIndexLinkLocal)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4Fsipv6ScopeZoneIndexIfIndex);
    UNUSED_PARAM (pTestValFsipv6ScopeZoneIndexLinkLocal);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6ScopeZoneIndexOrganizationLocal
 Input       :  The Indices
                Fsipv6ScopeZoneIndexIfIndex

                The Object
                testValFsipv6ScopeZoneIndexOrganizationLocal
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6ScopeZoneIndexOrganizationLocal (UINT4 *pu4ErrorCode,
                                                INT4
                                                i4Fsipv6ScopeZoneIndexIfIndex,
                                                tSNMP_OCTET_STRING_TYPE *
                                                pTestValFsipv6ScopeZoneIndexOrganizationLocal)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4Fsipv6ScopeZoneIndexIfIndex);
    UNUSED_PARAM (pTestValFsipv6ScopeZoneIndexOrganizationLocal);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6ScopeZoneIndexSiteLocal
 Input       :  The Indices
                Fsipv6ScopeZoneIndexIfIndex

                The Object
                testValFsipv6ScopeZoneIndexSiteLocal
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6ScopeZoneIndexSiteLocal (UINT4 *pu4ErrorCode,
                                        INT4 i4Fsipv6ScopeZoneIndexIfIndex,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pTestValFsipv6ScopeZoneIndexSiteLocal)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4Fsipv6ScopeZoneIndexIfIndex);
    UNUSED_PARAM (pTestValFsipv6ScopeZoneIndexSiteLocal);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6SupportEmbeddedRp
 Input       :  The Indices
                Fsipv6PrefixIndex
                Fsipv6PrefixAddress
                Fsipv6PrefixAddrLen

                The Object
                testValFsipv6SupportEmbeddedRp
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6SupportEmbeddedRp (UINT4 *pu4ErrorCode,
                                  INT4 i4Fsipv6PrefixIndex,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pFsipv6PrefixAddress,
                                  INT4 i4Fsipv6PrefixAddrLen,
                                  INT4 i4TestValFsipv6SupportEmbeddedRp)
{
    INT1                i1AddrType;

    /* Index Validation */
    if (Ip6ifEntryExists ((UINT4) i4Fsipv6PrefixIndex) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
        return SNMP_FAILURE;
    }

    /* Prefix Validation */
    i1AddrType = Ip6AddrType ((tIp6Addr *) (VOID *) pFsipv6PrefixAddress->
                              pu1_OctetList);
    if (i1AddrType != ADDR6_UNICAST)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_ADDR_TYPE);
        return SNMP_FAILURE;
    }

    /* Prefix Len Validation */
    if ((i4Fsipv6PrefixAddrLen <= IP6_ADDR_MIN_PREFIX) ||
        (i4Fsipv6PrefixAddrLen > IP6_ADDR_MAX_PREFIX))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_PREFIXLEN);
        return SNMP_FAILURE;
    }
    if ((i4TestValFsipv6SupportEmbeddedRp < IP6_SUPPORT_EMBD_RP_ENABLE) ||
        (i4TestValFsipv6SupportEmbeddedRp > IP6_SUPPORT_EMBD_RP_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_INVALID_EMBEDDED_RP);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/***************************************************************************
 Function    :  nmhSetFsipv6AddrSelPolicyAddrType
 Input       :  The Indices
                Fsipv6AddrSelPolicyPrefix
                Fsipv6AddrSelPolicyPrefixLen
                Fsipv6AddrSelPolicyIfIndex

                The Object
                setValFsipv6AddrSelPolicyAddrType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6AddrSelPolicyAddrType (tSNMP_OCTET_STRING_TYPE
                                   * pFsipv6AddrSelPolicyPrefix,
                                   INT4 i4Fsipv6AddrSelPolicyPrefixLen,
                                   INT4 i4Fsipv6AddrSelPolicyIfIndex,
                                   INT4 i4SetValFsipv6AddrSelPolicyAddrType)
{

    tIp6AddrSelPolicy  *pPolicyPrefix = NULL;
    tLip6If            *pIf6Entry =
        Lip6UtlGetIfEntry ((UINT4) i4Fsipv6AddrSelPolicyIfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    pPolicyPrefix = Ip6GetPolicyPrefix ((UINT4) i4Fsipv6AddrSelPolicyIfIndex,
                                        (tIp6Addr *) (VOID *)
                                        pFsipv6AddrSelPolicyPrefix->
                                        pu1_OctetList,
                                        (UINT1) i4Fsipv6AddrSelPolicyPrefixLen);
    if (pPolicyPrefix != NULL)
    {
        if (pPolicyPrefix->u1AddrType !=
            (UINT4) i4SetValFsipv6AddrSelPolicyAddrType)
        {
            pPolicyPrefix->u1AddrType =
                (UINT1) i4SetValFsipv6AddrSelPolicyAddrType;

        }
        IncMsrForIp6PolicyPrefixTable (i4Fsipv6AddrSelPolicyIfIndex,
                                       pFsipv6AddrSelPolicyPrefix,
                                       i4Fsipv6AddrSelPolicyPrefixLen, 'i',
                                       &i4SetValFsipv6AddrSelPolicyAddrType,
                                       FsMIIpv6AddrSelPolicyAddrType,
                                       sizeof (FsMIIpv6AddrSelPolicyAddrType) /
                                       sizeof (UINT4), FALSE);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetFsipv6AddrSelPolicyLabel
 Input       :  The Indices
                Fsipv6AddrSelPolicyPrefix
                Fsipv6AddrSelPolicyPrefixLen
                Fsipv6AddrSelPolicyIfIndex

                The Object
                setValFsipv6AddrSelPolicyLabel
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6AddrSelPolicyLabel (tSNMP_OCTET_STRING_TYPE
                                * pFsipv6AddrSelPolicyPrefix,
                                INT4 i4Fsipv6AddrSelPolicyPrefixLen,
                                INT4 i4Fsipv6AddrSelPolicyIfIndex,
                                INT4 i4SetValFsipv6AddrSelPolicyLabel)
{
    tIp6AddrSelPolicy  *pPolicyPrefix = NULL;
    tLip6If            *pIf6Entry =
        Lip6UtlGetIfEntry ((UINT4) i4Fsipv6AddrSelPolicyIfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    pPolicyPrefix = Ip6GetPolicyPrefix ((UINT4) i4Fsipv6AddrSelPolicyIfIndex,
                                        (tIp6Addr *) (VOID *)
                                        pFsipv6AddrSelPolicyPrefix->
                                        pu1_OctetList,
                                        (UINT1) i4Fsipv6AddrSelPolicyPrefixLen);
    if (pPolicyPrefix != NULL)
    {
        if (pPolicyPrefix->u1Label != (UINT4) i4SetValFsipv6AddrSelPolicyLabel)
        {
            pPolicyPrefix->u1Label = (UINT1) i4SetValFsipv6AddrSelPolicyLabel;

        }
        IncMsrForIp6PolicyPrefixTable (i4Fsipv6AddrSelPolicyIfIndex,
                                       pFsipv6AddrSelPolicyPrefix,
                                       i4Fsipv6AddrSelPolicyPrefixLen, 'i',
                                       &i4SetValFsipv6AddrSelPolicyLabel,
                                       FsMIIpv6AddrSelPolicyLabel,
                                       sizeof (FsMIIpv6AddrSelPolicyLabel) /
                                       sizeof (UINT4), FALSE);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetFsipv6AddrSelPolicyPrecedence
 Input       :  The Indices
                Fsipv6AddrSelPolicyPrefix
                Fsipv6AddrSelPolicyPrefixLen
                Fsipv6AddrSelPolicyIfIndex

                The Object
                setValFsipv6AddrSelPolicyPrecedence
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6AddrSelPolicyPrecedence (tSNMP_OCTET_STRING_TYPE
                                     * pFsipv6AddrSelPolicyPrefix,
                                     INT4 i4Fsipv6AddrSelPolicyPrefixLen,
                                     INT4 i4Fsipv6AddrSelPolicyIfIndex,
                                     INT4 i4SetValFsipv6AddrSelPolicyPrecedence)
{

    tIp6AddrSelPolicy  *pPolicyPrefix = NULL;
    tLip6If            *pIf6Entry =
        Lip6UtlGetIfEntry ((UINT4) i4Fsipv6AddrSelPolicyIfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    pPolicyPrefix = Ip6GetPolicyPrefix ((UINT4) i4Fsipv6AddrSelPolicyIfIndex,
                                        (tIp6Addr *) (VOID *)
                                        pFsipv6AddrSelPolicyPrefix->
                                        pu1_OctetList,
                                        (UINT1) i4Fsipv6AddrSelPolicyPrefixLen);
    if (pPolicyPrefix != NULL)
    {
        if (pPolicyPrefix->u1Precedence !=
            (UINT4) i4SetValFsipv6AddrSelPolicyPrecedence)
        {
            pPolicyPrefix->u1Precedence =
                (UINT1) i4SetValFsipv6AddrSelPolicyPrecedence;

        }
        IncMsrForIp6PolicyPrefixTable (i4Fsipv6AddrSelPolicyIfIndex,
                                       pFsipv6AddrSelPolicyPrefix,
                                       i4Fsipv6AddrSelPolicyPrefixLen, 'i',
                                       &i4SetValFsipv6AddrSelPolicyPrecedence,
                                       FsMIIpv6AddrSelPolicyPrecedence,
                                       sizeof (FsMIIpv6AddrSelPolicyPrecedence)
                                       / sizeof (UINT4), FALSE);

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetFsipv6AddrSelPolicyRowStatus
 Input       :  The Indices
                Fsipv6AddrSelPolicyPrefix
                Fsipv6AddrSelPolicyPrefixLen
                Fsipv6AddrSelPolicyIfIndex

                The Object
                setValFsipv6AddrSelPolicyRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6AddrSelPolicyRowStatus (tSNMP_OCTET_STRING_TYPE
                                    * pFsipv6AddrSelPolicyPrefix,
                                    INT4 i4Fsipv6AddrSelPolicyPrefixLen,
                                    INT4 i4Fsipv6AddrSelPolicyIfIndex,
                                    INT4 i4SetValFsipv6AddrSelPolicyRowStatus)
{
    tIp6AddrSelPolicy  *pPolicyPrefix = NULL;
    tLip6If            *pIf6Entry =
        Lip6UtlGetIfEntry ((UINT4) i4Fsipv6AddrSelPolicyIfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    switch (i4SetValFsipv6AddrSelPolicyRowStatus)
    {
        case IP6FWD_CREATE_AND_WAIT:
            pPolicyPrefix = Ip6PolicyPrefixCreate ((UINT4)
                                                   i4Fsipv6AddrSelPolicyIfIndex,
                                                   (tIp6Addr *) (VOID *)
                                                   pFsipv6AddrSelPolicyPrefix->
                                                   pu1_OctetList,
                                                   (UINT1)
                                                   i4Fsipv6AddrSelPolicyPrefixLen,
                                                   IP6FWD_NOT_READY);

            if (pPolicyPrefix == NULL)
            {
                return SNMP_FAILURE;
            }
            break;

        case IP6FWD_ACTIVE:
            pPolicyPrefix = Ip6GetPolicyPrefix ((UINT4)
                                                i4Fsipv6AddrSelPolicyIfIndex,
                                                (tIp6Addr *) (VOID *)
                                                pFsipv6AddrSelPolicyPrefix->
                                                pu1_OctetList,
                                                (UINT1)
                                                i4Fsipv6AddrSelPolicyPrefixLen);
            if (pPolicyPrefix == NULL)
            {
                return SNMP_FAILURE;
            }

            if (pPolicyPrefix->u1RowStatus == IP6FWD_ACTIVE)
            {
                /* Already Active */
                break;
            }

            pPolicyPrefix->u1RowStatus = IP6FWD_ACTIVE;

            break;

        case IP6FWD_DESTROY:
            pPolicyPrefix = Ip6GetPolicyPrefix ((UINT4)
                                                i4Fsipv6AddrSelPolicyIfIndex,
                                                (tIp6Addr *) (VOID *)
                                                pFsipv6AddrSelPolicyPrefix->
                                                pu1_OctetList,
                                                (UINT1)
                                                i4Fsipv6AddrSelPolicyPrefixLen);

            if (pPolicyPrefix == NULL)
            {
                return SNMP_FAILURE;
            }

            /* Delete the Prefix from interface */
            Ip6PolicyPrefixDelete ((UINT4) i4Fsipv6AddrSelPolicyIfIndex,
                                   (tIp6Addr *) (VOID *)
                                   pFsipv6AddrSelPolicyPrefix->pu1_OctetList,
                                   (UINT1) i4Fsipv6AddrSelPolicyPrefixLen);
            break;

        case IP6FWD_NOT_IN_SERVICE:
            pPolicyPrefix = Ip6GetPolicyPrefix ((UINT4)
                                                i4Fsipv6AddrSelPolicyIfIndex,
                                                (tIp6Addr *) (VOID *)
                                                pFsipv6AddrSelPolicyPrefix->
                                                pu1_OctetList,
                                                (UINT1)
                                                i4Fsipv6AddrSelPolicyPrefixLen);
            if (pPolicyPrefix == NULL)
            {
                return SNMP_FAILURE;
            }
            pPolicyPrefix->u1RowStatus = IP6FWD_NOT_IN_SERVICE;
            break;

        default:
            return SNMP_FAILURE;
    }

    IncMsrForIp6PolicyPrefixTable (i4Fsipv6AddrSelPolicyIfIndex,
                                   pFsipv6AddrSelPolicyPrefix,
                                   i4Fsipv6AddrSelPolicyPrefixLen, 'i',
                                   &i4SetValFsipv6AddrSelPolicyRowStatus,
                                   FsMIIpv6AddrSelPolicyRowStatus,
                                   sizeof (FsMIIpv6AddrSelPolicyRowStatus) /
                                   sizeof (UINT4), TRUE);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6IfDestUnreachableMsg
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                setValFsipv6IfDestUnreachableMsg
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6IfDestUnreachableMsg (INT4 i4Fsipv6IfIndex,
                                  INT4 i4SetValFsipv6IfDestUnreachableMsg)
{
    tLip6If            *pIf6Entry = NULL;

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    pIf6Entry->Icmp6ErrRLInfo.i4Icmp6DstUnReachable =
        i4SetValFsipv6IfDestUnreachableMsg;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsipv6IfUnnumAssocIPIf
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                setValFsipv6IfUnnumAssocIPIf
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6IfUnnumAssocIPIf (INT4 i4Fsipv6IfIndex,
                              INT4 i4SetValFsipv6IfUnnumAssocIPIf)
{
    tLip6If            *pIf6Entry = NULL;
    UINT1               u1AdminStatus = NETIPV6_ADMIN_DOWN;

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfIndex);
    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pIf6Entry->u4UnnumAssocIPv6If == (UINT4) i4SetValFsipv6IfUnnumAssocIPIf)
    {
        return SNMP_SUCCESS;
    }

    /* Making the interface down before changing unnumbered interface 
     * association, to indicate Higher layer, the change in unnumbered 
     * association */

    u1AdminStatus = pIf6Entry->u1AdminStatus;

    if (u1AdminStatus != NETIPV6_ADMIN_DOWN)
    {
        Lip6IfDisable (pIf6Entry);
    }

    pIf6Entry->u4UnnumAssocIPv6If = (UINT4) i4SetValFsipv6IfUnnumAssocIPIf;

    if (u1AdminStatus != NETIPV6_ADMIN_DOWN)
    {
        Lip6IfEnable (pIf6Entry);
    }

    IncMsrForIpv6IfTable (i4Fsipv6IfIndex, 'i',
                          &i4SetValFsipv6IfUnnumAssocIPIf,
                          FsMIIpv6IfUnnumAssocIPIf,
                          sizeof (FsMIIpv6IfUnnumAssocIPIf) / sizeof (UINT4));

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsipv6IfRedirectMsg
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                setValFsipv6IfRedirectMsg
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6IfRedirectMsg (INT4 i4Fsipv6IfIndex,
                           INT4 i4SetValFsipv6IfRedirectMsg)
{
    tLip6If            *pIf6Entry = NULL;
    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    pIf6Entry->Icmp6ErrRLInfo.i4Icmp6RedirectMsg = i4SetValFsipv6IfRedirectMsg;

    IncMsrForIpv6IfTable (i4Fsipv6IfIndex, 'i',
                          &i4SetValFsipv6IfRedirectMsg,
                          FsMIIpv6IfRedirectMsg,
                          sizeof (FsMIIpv6IfRedirectMsg) / sizeof (UINT4));

    return (SNMP_SUCCESS);

}

/****************************************************************************
* + Function    :  nmhSetFsipv6IfAdvSrcLLAdr
* + Input       :  The Indices
* +                Fsipv6IfIndex
* +
* +                The Object
* +                setValFsipv6IfAdvSrcLLAdr
* + Output      :  The Set Low Lev Routine Take the Indices &
* +                Sets the Value accordingly.
* + Returns     :  SNMP_SUCCESS or SNMP_FAILURE
* +****************************************************************************/
INT1
nmhSetFsipv6IfAdvSrcLLAdr (INT4 i4Fsipv6IfIndex,
                           INT4 i4SetValFsipv6IfAdvSrcLLAdr)
{
    tLip6If            *pIf6Entry = NULL;
    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    pIf6Entry->u1RALinkLocalStatus = (UINT1) i4SetValFsipv6IfAdvSrcLLAdr;

    IncMsrForIpv6IfTable (i4Fsipv6IfIndex, 'i',
                          &i4SetValFsipv6IfAdvSrcLLAdr,
                          FsMIIpv6IfAdvSrcLLAdr,
                          sizeof (FsMIIpv6IfAdvSrcLLAdr) / sizeof (UINT4));

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsipv6IfAdvIntOpt
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                setValFsipv6IfAdvIntOpt
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6IfAdvIntOpt (INT4 i4Fsipv6IfIndex, INT4 i4SetValFsipv6IfAdvIntOpt)
{
    tLip6If            *pIf6Entry = NULL;
    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    pIf6Entry->u1RAInterval = (UINT1) i4SetValFsipv6IfAdvIntOpt;
    IncMsrForIpv6IfTable (i4Fsipv6IfIndex, 'i',
                          &i4SetValFsipv6IfAdvIntOpt,
                          FsMIIpv6IfAdvIntOpt,
                          sizeof (FsMIIpv6IfAdvIntOpt) / sizeof (UINT4));

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsipv6IfForwarding
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                setValFsipv6IfForwarding
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6IfForwarding (INT4 i4Fsipv6IfIndex, INT4 i4SetValFsipv6IfForwarding)
{
    tLip6If            *pIf6 = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }
    if (Lip6KernSetIfForwarding (pIf6->au1IfName, i4SetValFsipv6IfForwarding) !=
        OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    pIf6->u1Ipv6IfFwdOperStatus = (UINT1) i4SetValFsipv6IfForwarding;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6IfIcmpErrInterval
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                setValFsipv6IfIcmpErrInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6IfIcmpErrInterval (INT4 i4Fsipv6IfIndex,
                               INT4 u4SetValFsipv6IfIcmpErrInterval)
{

    tLip6If            *pIf6Entry = NULL;

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    pIf6Entry->Icmp6ErrRLInfo.i4Icmp6ErrInterval =
        u4SetValFsipv6IfIcmpErrInterval;

    if (u4SetValFsipv6IfIcmpErrInterval)
    {
        pIf6Entry->Icmp6ErrRLInfo.i4Icmp6ErrRLFlag = ICMP6_RATE_LIMIT_ENABLE;
    }
    else
    {
        pIf6Entry->Icmp6ErrRLInfo.i4Icmp6ErrRLFlag = ICMP6_RATE_LIMIT_DISABLE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6IfIcmpTokenBucketSize
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                setValFsipv6IfIcmpTokenBucketSize
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6IfIcmpTokenBucketSize (INT4 i4Fsipv6IfIndex,
                                   INT4 i4SetValFsipv6IfIcmpTokenBucketSize)
{

    tLip6If            *pIf6Entry;

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    pIf6Entry->Icmp6ErrRLInfo.i4Icmp6BucketSize =
        i4SetValFsipv6IfIcmpTokenBucketSize;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6IfScopeZoneRowStatus
 Input       :  The Indices
                Fsipv6ScopeZoneIndexIfIndex

                The Object
                setValFsipv6IfScopeZoneRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6IfScopeZoneRowStatus (INT4 i4Fsipv6ScopeZoneIndexIfIndex,
                                  INT4 i4SetValFsipv6IfScopeZoneRowStatus)
{
    UNUSED_PARAM (i4Fsipv6ScopeZoneIndexIfIndex);
    UNUSED_PARAM (i4SetValFsipv6IfScopeZoneRowStatus);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6IsDefaultScopeZone
 Input       :  The Indices
                Fsipv6ScopeZoneName

                The Object
                setValFsipv6IsDefaultScopeZone
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6IsDefaultScopeZone (tSNMP_OCTET_STRING_TYPE * pFsipv6ScopeZoneName,
                                INT4 i4SetValFsipv6IsDefaultScopeZone)
{
    UNUSED_PARAM (pFsipv6ScopeZoneName);
    UNUSED_PARAM (i4SetValFsipv6IsDefaultScopeZone);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6PingZoneId
 Input       :  The Indices
                Fsipv6PingIndex

                The Object
                setValFsipv6PingZoneId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhSetFsipv6PingZoneId (INT4 i4Fsipv6PingIndex,
                        tSNMP_OCTET_STRING_TYPE * pSetValFsipv6PingZoneId)
{
    UNUSED_PARAM (i4Fsipv6PingIndex);
    UNUSED_PARAM (pSetValFsipv6PingZoneId);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6PingDestAddrType
 Input       :  The Indices
                Fsipv6PingIndex

                The Object 
                setValFsipv6PingDestAddrType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6PingDestAddrType (INT4 i4Fsipv6PingIndex,
                              INT4 i4SetValFsipv6PingDestAddrType)
{
    UNUSED_PARAM (i4Fsipv6PingIndex);
    UNUSED_PARAM (i4SetValFsipv6PingDestAddrType);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6RFC5095Compatibility
 Input       :  The Indices

                The Object
                setValFsipv6RFC5095Compatibility
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6RFC5095Compatibility (INT4 i4SetValFsipv6RFC5095Compatibility)
{
    gIp6GblInfo.u1RFC5095Compatibility =
        (UINT1) i4SetValFsipv6RFC5095Compatibility;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6RFC5942Compatibility
 Input       :  The Indices

                The Object
                setValFsipv6RFC5942Compatibility
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6RFC5942Compatibility (INT4 i4SetValFsipv6RFC5942Compatibility)
{
    gIp6GblInfo.u1RFC5942Compatibility =
        (UINT1) i4SetValFsipv6RFC5942Compatibility;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6ScopeZoneIndex3
 Input       :  The Indices
                Fsipv6ScopeZoneIndexIfIndex

                The Object
                setValFsipv6ScopeZoneIndex3
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6ScopeZoneIndex3 (INT4 i4Fsipv6ScopeZoneIndexIfIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pSetValFsipv6ScopeZoneIndex3)
{

    UNUSED_PARAM (i4Fsipv6ScopeZoneIndexIfIndex);
    UNUSED_PARAM (pSetValFsipv6ScopeZoneIndex3);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6ScopeZoneIndex6
 Input       :  The Indices
                Fsipv6ScopeZoneIndexIfIndex

                The Object
                setValFsipv6ScopeZoneIndex6
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6ScopeZoneIndex6 (INT4 i4Fsipv6ScopeZoneIndexIfIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pSetValFsipv6ScopeZoneIndex6)
{
    UNUSED_PARAM (i4Fsipv6ScopeZoneIndexIfIndex);
    UNUSED_PARAM (pSetValFsipv6ScopeZoneIndex6);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6ScopeZoneIndex7
 Input       :  The Indices
                Fsipv6ScopeZoneIndexIfIndex

                The Object
                setValFsipv6ScopeZoneIndex7
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6ScopeZoneIndex7 (INT4 i4Fsipv6ScopeZoneIndexIfIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pSetValFsipv6ScopeZoneIndex7)
{
    UNUSED_PARAM (i4Fsipv6ScopeZoneIndexIfIndex);
    UNUSED_PARAM (pSetValFsipv6ScopeZoneIndex7);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsipv6ScopeZoneIndex9
 Input       :  The Indices
                Fsipv6ScopeZoneIndexIfIndex

                The Object
                setValFsipv6ScopeZoneIndex9
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6ScopeZoneIndex9 (INT4 i4Fsipv6ScopeZoneIndexIfIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pSetValFsipv6ScopeZoneIndex9)
{
    UNUSED_PARAM (i4Fsipv6ScopeZoneIndexIfIndex);
    UNUSED_PARAM (pSetValFsipv6ScopeZoneIndex9);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsipv6ScopeZoneIndexA
 Input       :  The Indices
                Fsipv6ScopeZoneIndexIfIndex

                The Object
                setValFsipv6ScopeZoneIndexA
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6ScopeZoneIndexA (INT4 i4Fsipv6ScopeZoneIndexIfIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pSetValFsipv6ScopeZoneIndexA)
{
    UNUSED_PARAM (i4Fsipv6ScopeZoneIndexIfIndex);
    UNUSED_PARAM (pSetValFsipv6ScopeZoneIndexA);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsipv6ScopeZoneIndexAdminLocal
 Input       :  The Indices
                Fsipv6ScopeZoneIndexIfIndex

                The Object
                setValFsipv6ScopeZoneIndexAdminLocal
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6ScopeZoneIndexAdminLocal (INT4 i4Fsipv6ScopeZoneIndexIfIndex,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pSetValFsipv6ScopeZoneIndexAdminLocal)
{
    UNUSED_PARAM (i4Fsipv6ScopeZoneIndexIfIndex);
    UNUSED_PARAM (pSetValFsipv6ScopeZoneIndexAdminLocal);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsipv6ScopeZoneIndexB
 Input       :  The Indices
                Fsipv6ScopeZoneIndexIfIndex

                The Object
                setValFsipv6ScopeZoneIndexB
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6ScopeZoneIndexB (INT4 i4Fsipv6ScopeZoneIndexIfIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pSetValFsipv6ScopeZoneIndexB)
{
    UNUSED_PARAM (i4Fsipv6ScopeZoneIndexIfIndex);
    UNUSED_PARAM (pSetValFsipv6ScopeZoneIndexB);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsipv6ScopeZoneIndexC
 Input       :  The Indices
                Fsipv6ScopeZoneIndexIfIndex

                The Object
                setValFsipv6ScopeZoneIndexC
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6ScopeZoneIndexC (INT4 i4Fsipv6ScopeZoneIndexIfIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pSetValFsipv6ScopeZoneIndexC)
{
    UNUSED_PARAM (i4Fsipv6ScopeZoneIndexIfIndex);
    UNUSED_PARAM (pSetValFsipv6ScopeZoneIndexC);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsipv6ScopeZoneIndexD
 Input       :  The Indices
                Fsipv6ScopeZoneIndexIfIndex

                The Object
                setValFsipv6ScopeZoneIndexD
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6ScopeZoneIndexD (INT4 i4Fsipv6ScopeZoneIndexIfIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pSetValFsipv6ScopeZoneIndexD)
{
    UNUSED_PARAM (i4Fsipv6ScopeZoneIndexIfIndex);
    UNUSED_PARAM (pSetValFsipv6ScopeZoneIndexD);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsipv6ScopeZoneIndexE
 Input       :  The Indices
                Fsipv6ScopeZoneIndexIfIndex

                The Object
                setValFsipv6ScopeZoneIndexE
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6ScopeZoneIndexE (INT4 i4Fsipv6ScopeZoneIndexIfIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pSetValFsipv6ScopeZoneIndexE)
{
    UNUSED_PARAM (i4Fsipv6ScopeZoneIndexIfIndex);
    UNUSED_PARAM (pSetValFsipv6ScopeZoneIndexE);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsipv6ScopeZoneIndexInterfaceLocal
 Input       :  The Indices
                Fsipv6ScopeZoneIndexIfIndex

                The Object
                setValFsipv6ScopeZoneIndexInterfaceLocal
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6ScopeZoneIndexInterfaceLocal (INT4 i4Fsipv6ScopeZoneIndexIfIndex,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pSetValFsipv6ScopeZoneIndexInterfaceLocal)
{
    UNUSED_PARAM (i4Fsipv6ScopeZoneIndexIfIndex);
    UNUSED_PARAM (pSetValFsipv6ScopeZoneIndexInterfaceLocal);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsipv6ScopeZoneIndexLinkLocal
 Input       :  The Indices
                Fsipv6ScopeZoneIndexIfIndex

                The Object
                setValFsipv6ScopeZoneIndexLinkLocal
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6ScopeZoneIndexLinkLocal (INT4 i4Fsipv6ScopeZoneIndexIfIndex,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pSetValFsipv6ScopeZoneIndexLinkLocal)
{
    UNUSED_PARAM (i4Fsipv6ScopeZoneIndexIfIndex);
    UNUSED_PARAM (pSetValFsipv6ScopeZoneIndexLinkLocal);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsipv6ScopeZoneIndexOrganizationLocal
 Input       :  The Indices
                Fsipv6ScopeZoneIndexIfIndex

                The Object
                setValFsipv6ScopeZoneIndexOrganizationLocal
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6ScopeZoneIndexOrganizationLocal (INT4 i4Fsipv6ScopeZoneIndexIfIndex,
                                             tSNMP_OCTET_STRING_TYPE *
                                             pSetValFsipv6ScopeZoneIndexOrganizationLocal)
{
    UNUSED_PARAM (i4Fsipv6ScopeZoneIndexIfIndex);
    UNUSED_PARAM (pSetValFsipv6ScopeZoneIndexOrganizationLocal);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsipv6ScopeZoneIndexSiteLocal
 Input       :  The Indices
                Fsipv6ScopeZoneIndexIfIndex

                The Object
                setValFsipv6ScopeZoneIndexSiteLocal
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6ScopeZoneIndexSiteLocal (INT4 i4Fsipv6ScopeZoneIndexIfIndex,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pSetValFsipv6ScopeZoneIndexSiteLocal)
{
    UNUSED_PARAM (i4Fsipv6ScopeZoneIndexIfIndex);
    UNUSED_PARAM (pSetValFsipv6ScopeZoneIndexSiteLocal);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsipv6SupportEmbeddedRp
 Input       :  The Indices
                Fsipv6PrefixIndex
                Fsipv6PrefixAddress
                Fsipv6PrefixAddrLen

                The Object
                setValFsipv6SupportEmbeddedRp
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6SupportEmbeddedRp (INT4 i4Fsipv6PrefixIndex,
                               tSNMP_OCTET_STRING_TYPE * pFsipv6PrefixAddress,
                               INT4 i4Fsipv6PrefixAddrLen,
                               INT4 i4SetValFsipv6SupportEmbeddedRp)
{
    tLip6PrefixNode    *pPrefix = NULL;
    UINT4               u4ContextId = 0;

    Ip6GetCxtId ((UINT4) i4Fsipv6PrefixIndex, &u4ContextId);

    pPrefix = Lip6PrefixGetEntry ((UINT4) i4Fsipv6PrefixIndex,
                                  (tIp6Addr *) (VOID *) pFsipv6PrefixAddress->
                                  pu1_OctetList, (UINT1) i4Fsipv6PrefixAddrLen);
    if (pPrefix != NULL)
    {
        if (pPrefix->u1EmbdRpValid != (UINT1) i4SetValFsipv6SupportEmbeddedRp)
        {
            pPrefix->u1EmbdRpValid = (UINT1) i4SetValFsipv6SupportEmbeddedRp;
        }
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhDepv2Fsipv6AddrSelPolicyTable
 Input       :  The Indices
                Fsipv6AddrSelPolicyPrefix
                Fsipv6AddrSelPolicyPrefixLen
                Fsipv6AddrSelPolicyIfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsipv6AddrSelPolicyTable (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 *  Function    :  nmhGetFsipv6RouteAddrType
 *  Input       :  The Indices
 *                 Fsipv6RouteDest
 *                 Fsipv6RoutePfxLength
 *                 Fsipv6RouteProtocol
 *                 Fsipv6RouteNextHop
 *
 *                 The Object
 *                 retValFsipv6RouteAddrType
 * Output      :  The Get Low Lev Routine Take the Indices &
 * store the Value requested in the Return val.
 * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ****************************************************************************/
INT1
nmhGetFsipv6RouteAddrType (tSNMP_OCTET_STRING_TYPE * pFsipv6RouteDest,
                           INT4 i4Fsipv6RoutePfxLength,
                           INT4 i4Fsipv6RouteProtocol,
                           tSNMP_OCTET_STRING_TYPE * pFsipv6RouteNextHop,
                           INT4 *pi4RetValFsipv6RouteAddrType)
{
    tIp6Addr            Ip6Addr;
    tIp6Addr            NextHop;
    tNetIpv6RtInfo      NetIpv6RtInfo;
    UINT4               u4ContextId = IP6_ZERO;

    MEMSET (&Ip6Addr, IP6_ZERO, sizeof (tIp6Addr));
    MEMSET (&NextHop, IP6_ZERO, sizeof (tIp6Addr));
    MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));
    u4ContextId = (UINT4) UtilRtm6GetCurrCxtId ();
    Ip6AddrCopy (&Ip6Addr,
                 (tIp6Addr *) (VOID *) pFsipv6RouteDest->pu1_OctetList);
    Ip6AddrCopy (&NextHop,
                 (tIp6Addr *) (VOID *) pFsipv6RouteNextHop->pu1_OctetList);

    if (Rtm6ApiFindRtEntryInCxt (u4ContextId,
                                 &Ip6Addr, (UINT1) i4Fsipv6RoutePfxLength,
                                 (INT1) i4Fsipv6RouteProtocol, &NextHop,
                                 &NetIpv6RtInfo) == RTM6_SUCCESS)
    {
        *pi4RetValFsipv6RouteAddrType = (INT4) NetIpv6RtInfo.u1AddrType;
        return SNMP_SUCCESS;
    }
    else if (Rtm6UtilGetInActiveTableRtEntry (&Ip6Addr,
                                              i4Fsipv6RoutePfxLength,
                                              &NextHop,
                                              &NetIpv6RtInfo) == RTM6_SUCCESS)
    {
        *pi4RetValFsipv6RouteAddrType = (INT4) NetIpv6RtInfo.u1AddrType;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6RouteAddrType
 Input       :  The Indices
                Fsipv6RouteDest
                Fsipv6RoutePfxLength
                Fsipv6RouteProtocol
                Fsipv6RouteNextHop

                The Object
                testValFsipv6RouteAddrType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6RouteAddrType (UINT4 *pu4ErrorCode,
                              tSNMP_OCTET_STRING_TYPE * pFsipv6RouteDest,
                              INT4 i4Fsipv6RoutePfxLength,
                              INT4 i4Fsipv6RouteProtocol,
                              tSNMP_OCTET_STRING_TYPE * pFsipv6RouteNextHop,
                              INT4 i4TestValFsipv6RouteAddrType)
{
    tIp6Addr            ip6addr;

    MEMSET (&ip6addr, IP6_ZERO, sizeof (tIp6Addr));

    if (i4TestValFsipv6RouteAddrType != ADDR6_ANYCAST &&
        i4TestValFsipv6RouteAddrType != ADDR6_UNICAST)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_INVALID_ADDR_TYPE);
        return SNMP_FAILURE;
    }
    *pu4ErrorCode = SNMP_ERR_NO_CREATION;

    if (pFsipv6RouteDest == NULL)
    {
        CLI_SET_ERR (CLI_IP6_INVALID_ADDRESS);
        return SNMP_FAILURE;
    }

    if ((i4Fsipv6RoutePfxLength < IP6_ADDR_MIN_PREFIX) ||
        (i4Fsipv6RoutePfxLength > IP6_ADDR_MAX_PREFIX))
    {
        CLI_SET_ERR (CLI_IP6_INVALID_PREFIXLEN);
        return SNMP_FAILURE;
    }

    if ((i4Fsipv6RouteProtocol != IP6_NETMGMT_PROTOID) &&
        (i4Fsipv6RouteProtocol != IP6_LOCAL_PROTOID))
    {
        /* Invalid Protocol Id */
        CLI_SET_ERR (CLI_IP6_INVALID_ROUTE_PROTO);
        return SNMP_FAILURE;
    }

    Ip6AddrCopy (&ip6addr,
                 (tIp6Addr *) (VOID *) pFsipv6RouteNextHop->pu1_OctetList);

    if (IS_ADDR_MULTI (ip6addr))
    {
        CLI_SET_ERR (CLI_IP6_INVALID_NEXTHOP);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/***********************************************************************
Function    :  nmhSetFsipv6RouteAddrType
 Input       :  The Indices
                Fsipv6RouteDest
                Fsipv6RoutePfxLength
                Fsipv6RouteProtocol
                Fsipv6RouteNextHop

                The Object
                setValFsipv6RouteAddrType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6RouteAddrType (tSNMP_OCTET_STRING_TYPE * pFsipv6RouteDest,
                           INT4 i4Fsipv6RoutePfxLength,
                           INT4 i4Fsipv6RouteProtocol,
                           tSNMP_OCTET_STRING_TYPE * pFsipv6RouteNextHop,
                           INT4 i4SetValFsipv6RouteAddrType)
{
    UNUSED_PARAM (pFsipv6RouteDest);
    UNUSED_PARAM (i4Fsipv6RoutePfxLength);
    UNUSED_PARAM (i4Fsipv6RouteProtocol);
    UNUSED_PARAM (pFsipv6RouteNextHop);
    UNUSED_PARAM (i4SetValFsipv6RouteAddrType);
    return SNMP_SUCCESS;
}

/****************************************************************************
 *  Function    :  nmhGetFsipv6RouteHwStatus
 *  Input       :  The Indices
 *                 Fsipv6RouteDest
 *                 Fsipv6RoutePfxLength
 *                 Fsipv6RouteProtocol
 *                 Fsipv6RouteNextHop
 *
 *                 The Object 
 *                 retValFsipv6RouteHwStatus
 * Output      :  The Get Low Lev Routine Take the Indices &
 * store the Value requested in the Return val.
 * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ****************************************************************************/
INT1
nmhGetFsipv6RouteHwStatus (tSNMP_OCTET_STRING_TYPE * pFsipv6RouteDest,
                           INT4 i4Fsipv6RoutePfxLength,
                           INT4 i4Fsipv6RouteProtocol,
                           tSNMP_OCTET_STRING_TYPE * pFsipv6RouteNextHop,
                           INT4 *pi4RetValFsipv6RouteHwStatus)
{
    UNUSED_PARAM (pFsipv6RouteDest);
    UNUSED_PARAM (i4Fsipv6RoutePfxLength);
    UNUSED_PARAM (i4Fsipv6RouteProtocol);
    UNUSED_PARAM (pFsipv6RouteNextHop);
    UNUSED_PARAM (pi4RetValFsipv6RouteHwStatus);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsIpv6TestRedEntryTime
 Input       :  The Indices

                The Object
                retValFsIpv6TestRedEntryTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIpv6TestRedEntryTime (INT4 *pi4RetValFsIpv6TestRedEntryTime)
{
    UNUSED_PARAM (pi4RetValFsIpv6TestRedEntryTime);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsIpv6TestRedExitTime
 Input       :  The Indices

                The Object
                retValFsIpv6TestRedExitTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIpv6TestRedExitTime (INT4 *pi4RetValFsIpv6TestRedExitTime)
{
    UNUSED_PARAM (pi4RetValFsIpv6TestRedExitTime);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Fsipv6IfRaRDNSSTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsipv6IfRaRDNSSTable
 Input       :  The Indices
                Fsipv6IfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsipv6IfRaRDNSSTable (INT4 i4Fsipv6IfIndex)
{
    if (Lip6UtlIfEntryExists ((UINT4) i4Fsipv6IfIndex) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsipv6IfRaRDNSSTable
 Input       :  The Indices
                Fsipv6IfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsipv6IfRaRDNSSTable (INT4 *pi4Fsipv6IfIndex)
{
    *pi4Fsipv6IfIndex = 1;

    if (Lip6UtlIfGetNextIndex (1, (UINT4 *) pi4Fsipv6IfIndex) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsipv6IfRaRDNSSTable
 Input       :  The Indices
                Fsipv6IfIndex
                nextFsipv6IfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsipv6IfRaRDNSSTable (INT4 i4Fsipv6IfIndex,
                                     INT4 *pi4NextFsipv6IfIndex)
{
    INT4                i4Index = (i4Fsipv6IfIndex) + 1;

    if (i4Fsipv6IfIndex <= 0)
    {
        return (nmhGetFirstIndexFsipv6IfTable (pi4NextFsipv6IfIndex));
    }

    for (; i4Index <= (INT4) IP6_MAX_LOGICAL_IFACES; i4Index++)
    {
        if (Lip6UtlIfEntryExists ((UINT4) i4Index) == OSIX_SUCCESS)
        {
            *pi4NextFsipv6IfIndex = i4Index;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsipv6IfRadvRDNSSOpen
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                retValFsipv6IfRadvRDNSSOpen
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfRadvRDNSSOpen (INT4 i4Fsipv6IfIndex,
                             INT4 *pi4RetValFsipv6IfRadvRDNSSOpen)
{
    tLip6If            *pIf6Entry = NULL;

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsipv6IfRadvRDNSSOpen = pIf6Entry->u1RAdvRDNSSStatus;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfRaRDNSSPreference
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                retValFsipv6IfRaRDNSSPreference
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfRaRDNSSPreference (INT4 i4Fsipv6IfIndex,
                                 INT4 *pi4RetValFsipv6IfRaRDNSSPreference)
{
    tLip6If            *pIf6Entry = NULL;

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsipv6IfRaRDNSSPreference = (INT4) pIf6Entry->u4RDNSSPreference;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfRaRDNSSLifetime
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                retValFsipv6IfRaRDNSSLifetime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfRaRDNSSLifetime (INT4 i4Fsipv6IfIndex,
                               UINT4 *pu4RetValFsipv6IfRaRDNSSLifetime)
{
    tLip6If            *pIf6Entry = NULL;
    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfIndex);
    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsipv6IfRaRDNSSLifetime = pIf6Entry->u4RDNSSLifetime;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfRaRDNSSAddrOne
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                retValFsipv6IfRaRDNSSAddrOne
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfRaRDNSSAddrOne (INT4 i4Fsipv6IfIndex,
                              tSNMP_OCTET_STRING_TYPE *
                              pRetValFsipv6IfRaRDNSSAddrOne)
{
    tLip6If            *pIf6Entry = NULL;

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    MEMCPY (pRetValFsipv6IfRaRDNSSAddrOne->pu1_OctetList,
            &(pIf6Entry->Ip6RdnssAddrOne), sizeof (tIp6Addr));
    pRetValFsipv6IfRaRDNSSAddrOne->i4_Length = IP6_ADDR_SIZE;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsipv6IfRaRDNSSAddrTwo
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                retValFsipv6IfRaRDNSSAddrTwo
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfRaRDNSSAddrTwo (INT4 i4Fsipv6IfIndex,
                              tSNMP_OCTET_STRING_TYPE *
                              pRetValFsipv6IfRaRDNSSAddrTwo)
{
    tLip6If            *pIf6Entry = NULL;

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    MEMCPY (pRetValFsipv6IfRaRDNSSAddrTwo->pu1_OctetList,
            &(pIf6Entry->Ip6RdnssAddrTwo), sizeof (tIp6Addr));
    pRetValFsipv6IfRaRDNSSAddrTwo->i4_Length = IP6_ADDR_SIZE;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsipv6IfRaRDNSSAddrThree
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                retValFsipv6IfRaRDNSSAddrThree
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfRaRDNSSAddrThree (INT4 i4Fsipv6IfIndex,
                                tSNMP_OCTET_STRING_TYPE *
                                pRetValFsipv6IfRaRDNSSAddrThree)
{
    tLip6If            *pIf6Entry = NULL;

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    MEMCPY (pRetValFsipv6IfRaRDNSSAddrThree->pu1_OctetList,
            &(pIf6Entry->Ip6RdnssAddrThree), sizeof (tIp6Addr));
    pRetValFsipv6IfRaRDNSSAddrThree->i4_Length = IP6_ADDR_SIZE;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfRaRDNSSRowStatus
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                retValFsipv6IfRaRDNSSRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfRaRDNSSRowStatus (INT4 i4Fsipv6IfIndex,
                                INT4 *pi4RetValFsipv6IfRaRDNSSRowStatus)
{
    tLip6If            *pIf6Entry = NULL;

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsipv6IfRaRDNSSRowStatus = pIf6Entry->u1RAdvRDNSS;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfDomainNameOne
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                retValFsipv6IfDomainNameOne
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfDomainNameOne (INT4 i4Fsipv6IfIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pRetValFsipv6IfDomainNameOne)
{
    tLip6If            *pIf6Entry = NULL;

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    IP6_ASCIIZ_2_OCT (pRetValFsipv6IfDomainNameOne,
                      pIf6Entry->au1DomainNameOne);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfDomainNameTwo
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                retValFsipv6IfDomainNameTwo
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfDomainNameTwo (INT4 i4Fsipv6IfIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pRetValFsipv6IfDomainNameTwo)
{
    tLip6If            *pIf6Entry = NULL;

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    IP6_ASCIIZ_2_OCT (pRetValFsipv6IfDomainNameTwo,
                      pIf6Entry->au1DomainNameTwo);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfDomainNameThree
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                retValFsipv6IfDomainNameThree
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfDomainNameThree (INT4 i4Fsipv6IfIndex,
                               tSNMP_OCTET_STRING_TYPE *
                               pRetValFsipv6IfDomainNameThree)
{
    tLip6If            *pIf6Entry = NULL;

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    IP6_ASCIIZ_2_OCT (pRetValFsipv6IfDomainNameThree,
                      pIf6Entry->au1DomainNameThree);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfDnsLifeTime
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                retValFsipv6IfDnsLifeTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfDnsLifeTime (INT4 i4Fsipv6IfIndex,
                           INT4 *pi4RetValFsipv6IfDnsLifeTime)
{
    tLip6If            *pIf6Entry = NULL;

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsipv6IfDnsLifeTime = (INT4) pIf6Entry->u4DnsLifeTime;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfRaRDNSSAddrOneLife
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                retValFsipv6IfRaRDNSSAddrOneLife
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfRaRDNSSAddrOneLife (INT4 i4Fsipv6IfIndex,
                                  UINT4 *pu4RetValFsipv6IfRaRDNSSAddrOneLife)
{
    tLip6If            *pIf6Entry = NULL;
    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfIndex);
    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsipv6IfRaRDNSSAddrOneLife = pIf6Entry->u4RDNSSLifetimeOne;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsipv6IfRaRDNSSAddrTwoLife
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                retValFsipv6IfRaRDNSSAddrTwoLife
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfRaRDNSSAddrTwoLife (INT4 i4Fsipv6IfIndex,
                                  UINT4 *pu4RetValFsipv6IfRaRDNSSAddrTwoLife)
{
    tLip6If            *pIf6Entry = NULL;
    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfIndex);
    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsipv6IfRaRDNSSAddrTwoLife = pIf6Entry->u4RDNSSLifetimeTwo;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsipv6IfRaRDNSSAddrOneLife
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                retValFsipv6IfRaRDNSSAddrOneLife
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfRaRDNSSAddrThreeLife (INT4 i4Fsipv6IfIndex,
                                    UINT4
                                    *pu4RetValFsipv6IfRaRDNSSAddrThreeLife)
{
    tLip6If            *pIf6Entry = NULL;
    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfIndex);
    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsipv6IfRaRDNSSAddrThreeLife = pIf6Entry->u4RDNSSLifetimeThree;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsipv6IfDomainNameOneLife 
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                retValFsipv6IfDnsLifeTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfDomainNameOneLife (INT4 i4Fsipv6IfIndex,
                                 INT4 *pi4RetValFsipv6IfDnsLifeTime)
{
    tLip6If            *pIf6Entry = NULL;

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsipv6IfDnsLifeTime = (INT4) pIf6Entry->u4DnsLifeTimeOne;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfDomainNameTwoLife  
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                retValFsipv6IfDnsLifeTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfDomainNameTwoLife (INT4 i4Fsipv6IfIndex,
                                 INT4 *pi4RetValFsipv6IfDnsLifeTime)
{
    tLip6If            *pIf6Entry = NULL;

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsipv6IfDnsLifeTime = (INT4) pIf6Entry->u4DnsLifeTimeTwo;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfDomainNameThreeLife 
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                retValFsipv6IfDnsLifeTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfDomainNameThreeLife (INT4 i4Fsipv6IfIndex,
                                   INT4 *pi4RetValFsipv6IfDnsLifeTime)
{
    tLip6If            *pIf6Entry = NULL;

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsipv6IfDnsLifeTime = (INT4) pIf6Entry->u4DnsLifeTimeThree;

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsipv6IfRadvRDNSSOpen
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                setValFsipv6IfRadvRDNSSOpen
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6IfRadvRDNSSOpen (INT4 i4Fsipv6IfIndex,
                             INT4 i4SetValFsipv6IfRadvRDNSSOpen)
{
    INT4                i4RetVal = 0;

    tLip6If            *pIf6Entry = NULL;

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }
    i4RetVal = Ip6SetRtrAdvertRDNSSOpen (i4Fsipv6IfIndex,
                                         (UINT4) i4SetValFsipv6IfRadvRDNSSOpen);

    if (i4RetVal == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pIf6Entry->u1RAdvRDNSSStatus = (UINT1) i4SetValFsipv6IfRadvRDNSSOpen;
    IncMsrForIpv6IfTable (i4Fsipv6IfIndex, 'i',
                          &i4SetValFsipv6IfRadvRDNSSOpen,
                          FsMIIpv6IfRadvRDNSSOpen,
                          sizeof (FsMIIpv6IfRadvRDNSSOpen) / sizeof (UINT4));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6IfRaRDNSSPreference
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                setValFsipv6IfRaRDNSSPreference
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6IfRaRDNSSPreference (INT4 i4Fsipv6IfIndex,
                                 INT4 i4SetValFsipv6IfRaRDNSSPreference)
{
    tLip6If            *pIf6Entry = NULL;
    INT4                i4RetVal = OSIX_FAILURE;

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    i4RetVal = Ip6SetRtrAdvtRDNSSPref (i4Fsipv6IfIndex,
                                       (UINT4)
                                       i4SetValFsipv6IfRaRDNSSPreference);

    if (i4RetVal == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    pIf6Entry->u4RDNSSPreference = (UINT4) i4SetValFsipv6IfRaRDNSSPreference;
    IncMsrForIpv6IfTable (i4Fsipv6IfIndex, 'i',
                          &i4SetValFsipv6IfRaRDNSSPreference,
                          FsMIIpv6IfRaRDNSSPreference,
                          sizeof (FsMIIpv6IfRaRDNSSPreference) /
                          sizeof (UINT4));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6IfRaRDNSSLifetime
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                setValFsipv6IfRaRDNSSLifetime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6IfRaRDNSSLifetime (INT4 i4Fsipv6IfIndex,
                               UINT4 u4SetValFsipv6IfRaRDNSSLifetime)
{
    tLip6If            *pIf6Entry = NULL;
    INT4                i4RetVal = OSIX_FAILURE;
    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfIndex);
    if (pIf6Entry == NULL)
    {
#ifdef RM_WANTED
        /* In case of standby node, we can ignore the Hop Limit
         *          * syncup failure,because its already set as default hop Limit in standby */
        if ((u4SetValFsipv6IfRaRDNSSLifetime == IP6_IF_RA_RDNSS_DEF_LIFETIME)
            && (RmGetNodeState () == RM_STANDBY))
        {
            return SNMP_SUCCESS;
        }
#endif
        return SNMP_FAILURE;
    }
    i4RetVal = Ip6SetRtrAdvtRDNSSLife (i4Fsipv6IfIndex,
                                       u4SetValFsipv6IfRaRDNSSLifetime);
    if (i4RetVal == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    pIf6Entry->u4RDNSSLifetime = u4SetValFsipv6IfRaRDNSSLifetime;

    IncMsrForIpv6IfTable (i4Fsipv6IfIndex, 'i',
                          &u4SetValFsipv6IfRaRDNSSLifetime,
                          FsMIIpv6IfRaRDNSSLifetime,
                          sizeof (FsMIIpv6IfRaRDNSSLifetime) / sizeof (UINT4));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6IfRaRDNSSAddrOne
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                setValFsipv6IfRaRDNSSAddrOne
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6IfRaRDNSSAddrOne (INT4 i4Fsipv6IfIndex,
                              tSNMP_OCTET_STRING_TYPE *
                              pSetValFsipv6IfRaRDNSSAddrOne)
{
    tLip6If            *pIf6Entry = NULL;

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMCPY (&(pIf6Entry->Ip6RdnssAddrOne),
            pSetValFsipv6IfRaRDNSSAddrOne->pu1_OctetList, IP6_ADDR_SIZE);
    Ip6SetRaRDNSSAddress (i4Fsipv6IfIndex, pIf6Entry->Ip6RdnssAddrOne);
    IncMsrForIpv6IfTable (i4Fsipv6IfIndex, 's',
                          pSetValFsipv6IfRaRDNSSAddrOne,
                          FsMIIpv6IfRaRDNSSAddrOne,
                          sizeof (FsMIIpv6IfRaRDNSSAddrOne) / sizeof (UINT4));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6IfRaRDNSSAddrTwo
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                setValFsipv6IfRaRDNSSAddrTwo
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6IfRaRDNSSAddrTwo (INT4 i4Fsipv6IfIndex,
                              tSNMP_OCTET_STRING_TYPE *
                              pSetValFsipv6IfRaRDNSSAddrTwo)
{
    tLip6If            *pIf6Entry = NULL;

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMCPY (&(pIf6Entry->Ip6RdnssAddrTwo),
            pSetValFsipv6IfRaRDNSSAddrTwo->pu1_OctetList, sizeof (tIp6Addr));
    Ip6SetRaRDNSSAddress (i4Fsipv6IfIndex, pIf6Entry->Ip6RdnssAddrTwo);
    IncMsrForIpv6IfTable (i4Fsipv6IfIndex, 's',
                          &pSetValFsipv6IfRaRDNSSAddrTwo,
                          FsMIIpv6IfRaRDNSSAddrTwo,
                          sizeof (FsMIIpv6IfRaRDNSSAddrTwo) / sizeof (UINT4));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6IfRaRDNSSAddrThree
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                setValFsipv6IfRaRDNSSAddrThree
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6IfRaRDNSSAddrThree (INT4 i4Fsipv6IfIndex,
                                tSNMP_OCTET_STRING_TYPE *
                                pSetValFsipv6IfRaRDNSSAddrThree)
{
    tLip6If            *pIf6Entry = NULL;

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMCPY (&(pIf6Entry->Ip6RdnssAddrThree),
            pSetValFsipv6IfRaRDNSSAddrThree->pu1_OctetList, sizeof (tIp6Addr));
    Ip6SetRaRDNSSAddress (i4Fsipv6IfIndex, pIf6Entry->Ip6RdnssAddrThree);
    IncMsrForIpv6IfTable (i4Fsipv6IfIndex, 's',
                          &pSetValFsipv6IfRaRDNSSAddrThree,
                          FsMIIpv6IfRaRDNSSAddrThree,
                          sizeof (FsMIIpv6IfRaRDNSSAddrThree) / sizeof (UINT4));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6IfRaRDNSSRowStatus
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                setValFsipv6IfRaRDNSSRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6IfRaRDNSSRowStatus (INT4 i4Fsipv6IfIndex,
                                INT4 i4SetValFsipv6IfRaRDNSSRowStatus)
{
    tLip6If            *pIf6Entry = NULL;

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }
    pIf6Entry->u1RAdvRDNSS = (UINT1) i4SetValFsipv6IfRaRDNSSRowStatus;
    Ip6SetRtrAdvtRowStatus (i4Fsipv6IfIndex, i4SetValFsipv6IfRaRDNSSRowStatus);
    IncMsrForIpv6IfTable (i4Fsipv6IfIndex, 'i',
                          &i4SetValFsipv6IfRaRDNSSRowStatus,
                          FsMIIpv6IfRaRDNSSRowStatus,
                          sizeof (FsMIIpv6IfRaRDNSSRowStatus) / sizeof (UINT4));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6IfDomainNameOne
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                setValFsipv6IfDomainNameOne
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6IfDomainNameOne (INT4 i4Fsipv6IfIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pSetValFsipv6IfDomainNameOne)
{
    tLip6If            *pIf6Entry = NULL;

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pSetValFsipv6IfDomainNameOne != NULL)
    {
        IP6_OCT_2_ASCIIZ (pIf6Entry->au1DomainNameOne,
                          pSetValFsipv6IfDomainNameOne);
    }

    IncMsrForIpv6IfTable (i4Fsipv6IfIndex, 'i',
                          &pSetValFsipv6IfDomainNameOne,
                          FsMIIpv6IfDomainNameOne,
                          sizeof (FsMIIpv6IfDomainNameOne) / sizeof (UINT4));

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6IfDomainNameTwo
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                setValFsipv6IfDomainNameTwo
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6IfDomainNameTwo (INT4 i4Fsipv6IfIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pSetValFsipv6IfDomainNameTwo)
{
    tLip6If            *pIf6Entry = NULL;

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pSetValFsipv6IfDomainNameTwo != NULL)
    {
        IP6_OCT_2_ASCIIZ (pIf6Entry->au1DomainNameTwo,
                          pSetValFsipv6IfDomainNameTwo);
    }
    IncMsrForIpv6IfTable (i4Fsipv6IfIndex, 'i',
                          &pSetValFsipv6IfDomainNameTwo,
                          FsMIIpv6IfDomainNameTwo,
                          sizeof (FsMIIpv6IfDomainNameTwo) / sizeof (UINT4));

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6IfDomainNameThree
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                setValFsipv6IfDomainNameThree
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6IfDomainNameThree (INT4 i4Fsipv6IfIndex,
                               tSNMP_OCTET_STRING_TYPE *
                               pSetValFsipv6IfDomainNameThree)
{
    tLip6If            *pIf6Entry = NULL;

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pSetValFsipv6IfDomainNameThree != NULL)
    {
        IP6_OCT_2_ASCIIZ (pIf6Entry->au1DomainNameThree,
                          pSetValFsipv6IfDomainNameThree);
    }
    IncMsrForIpv6IfTable (i4Fsipv6IfIndex, 'i',
                          &pSetValFsipv6IfDomainNameThree,
                          FsMIIpv6IfDomainNameThree,
                          sizeof (FsMIIpv6IfDomainNameThree) / sizeof (UINT4));

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6IfDnsLifeTime
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                setValFsipv6IfDnsLifeTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6IfDnsLifeTime (INT4 i4Fsipv6IfIndex,
                           INT4 i4SetValFsipv6IfDnsLifeTime)
{
    tLip6If            *pIf6Entry = NULL;
    INT4                i4RetVal = OSIX_FAILURE;
    UINT4               u4OldVal = 0;

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pIf6Entry->u4DnsLifeTime == (UINT4) i4SetValFsipv6IfDnsLifeTime)
    {
        return SNMP_SUCCESS;
    }
    pIf6Entry->u4DnsLifeTime = (UINT4) i4SetValFsipv6IfDnsLifeTime;

    u4OldVal = pIf6Entry->u4DnsLifeTime;
    pIf6Entry->u4DnsLifeTime = (UINT4) i4SetValFsipv6IfDnsLifeTime;

    if (pIf6Entry->u1OperStatus == NETIPV6_OPER_UP)
    {
        i4RetVal = Lip6RAConfig (pIf6Entry->u4ContextId);
        if (i4RetVal == OSIX_FAILURE)
        {
            pIf6Entry->u4DnsLifeTime = u4OldVal;
            return SNMP_FAILURE;
        }
    }
    IncMsrForIpv6IfTable (i4Fsipv6IfIndex, 'i',
                          &i4SetValFsipv6IfDnsLifeTime,
                          FsMIIpv6IfDnsLifeTime,
                          sizeof (FsMIIpv6IfDnsLifeTime) / sizeof (INT4));

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6IfDomainNameOneLife 
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                setValFsipv6IfDnsLifeTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6IfDomainNameOneLife (INT4 i4Fsipv6IfIndex,
                                 INT4 i4SetValFsipv6IfDnsLifeTime)
{
    tLip6If            *pIf6Entry = NULL;
    INT4                i4RetVal = OSIX_FAILURE;
    UINT4               u4OldVal = 0;

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pIf6Entry->u4DnsLifeTimeOne == (UINT4) i4SetValFsipv6IfDnsLifeTime)
    {
        return SNMP_SUCCESS;
    }
    pIf6Entry->u4DnsLifeTimeOne = (UINT4) i4SetValFsipv6IfDnsLifeTime;

    u4OldVal = pIf6Entry->u4DnsLifeTimeOne;
    pIf6Entry->u4DnsLifeTimeOne = (UINT4) i4SetValFsipv6IfDnsLifeTime;

    if (pIf6Entry->u1OperStatus == NETIPV6_OPER_UP)
    {
        i4RetVal = Lip6RAConfig (pIf6Entry->u4ContextId);
        if (i4RetVal == OSIX_FAILURE)
        {
            pIf6Entry->u4DnsLifeTimeOne = u4OldVal;
            return SNMP_FAILURE;
        }
    }
    IncMsrForIpv6IfTable (i4Fsipv6IfIndex, 'i',
                          &i4SetValFsipv6IfDnsLifeTime,
                          FsMIIpv6IfDomainNameOneLife,
                          sizeof (FsMIIpv6IfDomainNameOneLife) / sizeof (INT4));

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6IfDomainNameTwoLife
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                setValFsipv6IfDnsLifeTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6IfDomainNameTwoLife (INT4 i4Fsipv6IfIndex,
                                 INT4 i4SetValFsipv6IfDnsLifeTime)
{
    tLip6If            *pIf6Entry = NULL;
    INT4                i4RetVal = OSIX_FAILURE;
    UINT4               u4OldVal = 0;

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pIf6Entry->u4DnsLifeTimeTwo == (UINT4) i4SetValFsipv6IfDnsLifeTime)
    {
        return SNMP_SUCCESS;
    }
    pIf6Entry->u4DnsLifeTimeTwo = (UINT4) i4SetValFsipv6IfDnsLifeTime;

    u4OldVal = pIf6Entry->u4DnsLifeTimeTwo;
    pIf6Entry->u4DnsLifeTimeTwo = (UINT4) i4SetValFsipv6IfDnsLifeTime;

    if (pIf6Entry->u1OperStatus == NETIPV6_OPER_UP)
    {
        i4RetVal = Lip6RAConfig (pIf6Entry->u4ContextId);
        if (i4RetVal == OSIX_FAILURE)
        {
            pIf6Entry->u4DnsLifeTimeTwo = u4OldVal;
            return SNMP_FAILURE;
        }
    }
    IncMsrForIpv6IfTable (i4Fsipv6IfIndex, 'i',
                          &i4SetValFsipv6IfDnsLifeTime,
                          FsMIIpv6IfDomainNameTwoLife,
                          sizeof (FsMIIpv6IfDomainNameTwoLife) / sizeof (INT4));

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6IfDomainNameThreeLife
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                setValFsipv6IfDnsLifeTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6IfDomainNameThreeLife (INT4 i4Fsipv6IfIndex,
                                   INT4 i4SetValFsipv6IfDnsLifeTime)
{
    tLip6If            *pIf6Entry = NULL;
    INT4                i4RetVal = OSIX_FAILURE;
    UINT4               u4OldVal = 0;

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pIf6Entry->u4DnsLifeTimeThree == (UINT4) i4SetValFsipv6IfDnsLifeTime)
    {
        return SNMP_SUCCESS;
    }
    pIf6Entry->u4DnsLifeTimeThree = (UINT4) i4SetValFsipv6IfDnsLifeTime;

    u4OldVal = pIf6Entry->u4DnsLifeTimeThree;
    pIf6Entry->u4DnsLifeTimeThree = (UINT4) i4SetValFsipv6IfDnsLifeTime;

    if (pIf6Entry->u1OperStatus == NETIPV6_OPER_UP)
    {
        i4RetVal = Lip6RAConfig (pIf6Entry->u4ContextId);
        if (i4RetVal == OSIX_FAILURE)
        {
            pIf6Entry->u4DnsLifeTimeThree = u4OldVal;
            return SNMP_FAILURE;
        }
    }
    IncMsrForIpv6IfTable (i4Fsipv6IfIndex, 'i',
                          &i4SetValFsipv6IfDnsLifeTime,
                          FsMIIpv6IfDomainNameThreeLife,
                          sizeof (FsMIIpv6IfDomainNameThreeLife) /
                          sizeof (INT4));

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6IfRaRDNSSAddrOneLife
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                setValFsipv6IfRaRDNSSAddrOneLife
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6IfRaRDNSSAddrOneLife (INT4 i4Fsipv6IfIndex,
                                  UINT4 u4SetValFsipv6IfRaRDNSSAddrOneLife)
{
    tLip6If            *pIf6Entry = NULL;
    INT4                i4RetVal = OSIX_FAILURE;
    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfIndex);
    if (pIf6Entry == NULL)
    {
#ifdef RM_WANTED
        /* In case of standby node, we can ignore the Hop Limit
         *          * syncup failure,because its already set as default hop Limit in standby */
        if ((u4SetValFsipv6IfRaRDNSSAddrOneLife == IP6_IF_RA_RDNSS_DEF_LIFETIME)
            && (RmGetNodeState () == RM_STANDBY))
        {
            return SNMP_SUCCESS;
        }
#endif
        return SNMP_FAILURE;
    }
    i4RetVal = Ip6SetRtrAdvtRDNSSLifeOne (i4Fsipv6IfIndex,
                                          u4SetValFsipv6IfRaRDNSSAddrOneLife);
    if (i4RetVal == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    pIf6Entry->u4RDNSSLifetimeOne = u4SetValFsipv6IfRaRDNSSAddrOneLife;

    IncMsrForIpv6IfTable (i4Fsipv6IfIndex, 'i',
                          &u4SetValFsipv6IfRaRDNSSAddrOneLife,
                          FsMIIpv6IfRaRDNSSAddrOneLife,
                          sizeof (FsMIIpv6IfRaRDNSSAddrOneLife) /
                          sizeof (UINT4));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6IfRaRDNSSAddrTwoLife
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                setValFsipv6IfRaRDNSSAddrTwoLife
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6IfRaRDNSSAddrTwoLife (INT4 i4Fsipv6IfIndex,
                                  UINT4 u4SetValFsipv6IfRaRDNSSAddrTwoLife)
{
    tLip6If            *pIf6Entry = NULL;
    INT4                i4RetVal = OSIX_FAILURE;
    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfIndex);
    if (pIf6Entry == NULL)
    {
#ifdef RM_WANTED
        /* In case of standby node, we can ignore the Hop Limit
         *          * syncup failure,because its already set as default hop Limit in standby */
        if ((u4SetValFsipv6IfRaRDNSSAddrTwoLife == IP6_IF_RA_RDNSS_DEF_LIFETIME)
            && (RmGetNodeState () == RM_STANDBY))
        {
            return SNMP_SUCCESS;
        }
#endif
        return SNMP_FAILURE;
    }
    i4RetVal = Ip6SetRtrAdvtRDNSSLifeTwo (i4Fsipv6IfIndex,
                                          u4SetValFsipv6IfRaRDNSSAddrTwoLife);
    if (i4RetVal == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    pIf6Entry->u4RDNSSLifetimeTwo = u4SetValFsipv6IfRaRDNSSAddrTwoLife;

    IncMsrForIpv6IfTable (i4Fsipv6IfIndex, 'i',
                          &u4SetValFsipv6IfRaRDNSSAddrTwoLife,
                          FsMIIpv6IfRaRDNSSAddrTwoLife,
                          sizeof (FsMIIpv6IfRaRDNSSAddrTwoLife) /
                          sizeof (UINT4));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6IfRaRDNSSAddrThreeLife
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                setValFsipv6IfRaRDNSSAddrThreeLife
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6IfRaRDNSSAddrThreeLife (INT4 i4Fsipv6IfIndex,
                                    UINT4 u4SetValFsipv6IfRaRDNSSAddrThreeLife)
{
    tLip6If            *pIf6Entry = NULL;
    INT4                i4RetVal = OSIX_FAILURE;
    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfIndex);
    if (pIf6Entry == NULL)
    {
#ifdef RM_WANTED
        /* In case of standby node, we can ignore the Hop Limit
         *          * syncup failure,because its already set as default hop Limit in standby */
        if ((u4SetValFsipv6IfRaRDNSSAddrThreeLife ==
             IP6_IF_RA_RDNSS_DEF_LIFETIME) && (RmGetNodeState () == RM_STANDBY))
        {
            return SNMP_SUCCESS;
        }
#endif
        return SNMP_FAILURE;
    }
    i4RetVal = Ip6SetRtrAdvtRDNSSLifeThree (i4Fsipv6IfIndex,
                                            u4SetValFsipv6IfRaRDNSSAddrThreeLife);
    if (i4RetVal == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    pIf6Entry->u4RDNSSLifetimeThree = u4SetValFsipv6IfRaRDNSSAddrThreeLife;

    IncMsrForIpv6IfTable (i4Fsipv6IfIndex, 'i',
                          &u4SetValFsipv6IfRaRDNSSAddrThreeLife,
                          FsMIIpv6IfRaRDNSSAddrThreeLife,
                          sizeof (FsMIIpv6IfRaRDNSSAddrThreeLife) /
                          sizeof (UINT4));
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Fsipv6IfRadvRDNSSOpen
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                testValFsipv6IfRadvRDNSSOpen
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6IfRadvRDNSSOpen (UINT4 *pu4ErrorCode, INT4 i4Fsipv6IfIndex,
                                INT4 i4TestValFsipv6IfRadvRDNSSOpen)
{
    tLip6If            *pIf6Entry = NULL;

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfIndex);
    if (pIf6Entry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
        return SNMP_FAILURE;
    }
    if ((pIf6Entry->u1IfType != IP6_ENET_INTERFACE_TYPE) &&
#ifdef TUNNEL_WANTED
        (pIf6Entry->u1IfType != IP6_TUNNEL_INTERFACE_TYPE) &&
#endif /* TUNNEL_WANTED */
        (pIf6Entry->u1IfType != IP6_L3VLAN_INTERFACE_TYPE) &&
        (pIf6Entry->u1IfType != IP6_LAGG_INTERFACE_TYPE))
    {
        /* RA is currently not supported for these interfaces. */
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
        return SNMP_FAILURE;
    }

    if (i4TestValFsipv6IfRadvRDNSSOpen != IP6_IF_ROUT_ADV_RDNSS_ENABLED &&
        i4TestValFsipv6IfRadvRDNSSOpen != IP6_IF_ROUT_ADV_RDNSS_DISABLED)

    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_INVALID_RT_ADV_RDNSS_STATUS);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6IfRaRDNSSPreference
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                testValFsipv6IfRaRDNSSPreference
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6IfRaRDNSSPreference (UINT4 *pu4ErrorCode, INT4 i4Fsipv6IfIndex,
                                    INT4 i4TestValFsipv6IfRaRDNSSPreference)
{
    tLip6If            *pIf6Entry = NULL;

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfIndex);
    if (pIf6Entry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
        return SNMP_FAILURE;
    }

    if (i4TestValFsipv6IfRaRDNSSPreference < IP6_IF_RA_RDNSS_MIN_PREF ||
        i4TestValFsipv6IfRaRDNSSPreference > IP6_IF_RA_RDNSS_MAX_PREF)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_INVALID_RT_ADV_RDNSS_PREFERENCE);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6IfRaRDNSSLifetime
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                testValFsipv6IfRaRDNSSLifetime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6IfRaRDNSSLifetime (UINT4 *pu4ErrorCode, INT4 i4Fsipv6IfIndex,
                                  UINT4 u4TestValFsipv6IfRaRDNSSLifetime)
{
    tLip6If            *pIf6Entry = NULL;

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6Entry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
        return SNMP_FAILURE;
    }
    if (CheckIp6IsFwdEnabled (i4Fsipv6IfIndex) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if (u4TestValFsipv6IfRaRDNSSLifetime != IPVX_ZERO)

    {

        if ((u4TestValFsipv6IfRaRDNSSLifetime > IP6_IF_RA_RDNSS_MAX_LIFETIME)
            || (pIf6Entry->u4MaxRaTime > u4TestValFsipv6IfRaRDNSSLifetime))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_IP6_INVALID_RT_ADV_RDNSS_LIFETIME);
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6IfRaRDNSSAddrOne
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                testValFsipv6IfRaRDNSSAddrOne
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6IfRaRDNSSAddrOne (UINT4 *pu4ErrorCode, INT4 i4Fsipv6IfIndex,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pTestValFsipv6IfRaRDNSSAddrOne)
{
    tIp6Addr            Ip6Addr;
    tIp6Addr            Ip6TempAddr;
    INT1                i1AddrType = 0;

    MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));
    MEMSET (&Ip6TempAddr, 0, sizeof (tIp6Addr));
    *pu4ErrorCode = SNMP_ERR_NO_CREATION;

    if (Lip6UtlIfEntryExists ((UINT4) i4Fsipv6IfIndex) != OSIX_SUCCESS)
    {
        CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
        return SNMP_FAILURE;
    }
    if (CheckIp6IsFwdEnabled (i4Fsipv6IfIndex) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    Ip6AddrCopy (&Ip6Addr,
                 (tIp6Addr *) (VOID *) pTestValFsipv6IfRaRDNSSAddrOne->
                 pu1_OctetList);
    if (Ip6AddrCompare (Ip6TempAddr, Ip6Addr) == IP6_ZERO)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        if (IS_ADDR_MULTI (Ip6Addr) || IS_ADDR_UNSPECIFIED (Ip6Addr))
        {
            CLI_SET_ERR (CLI_IP6_INVALID_ADDRESS);
            return SNMP_FAILURE;
        }
        i1AddrType =
            Ip6AddrType ((tIp6Addr *) (VOID *) pTestValFsipv6IfRaRDNSSAddrOne->
                         pu1_OctetList);
        if ((i1AddrType != ADDR6_UNICAST) && (i1AddrType != ADDR6_ANYCAST))
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            CLI_SET_ERR (CLI_IP6_INVALID_ADDR_TYPE);
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6IfRaRDNSSAddrTwo
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                testValFsipv6IfRaRDNSSAddrTwo
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6IfRaRDNSSAddrTwo (UINT4 *pu4ErrorCode, INT4 i4Fsipv6IfIndex,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pTestValFsipv6IfRaRDNSSAddrTwo)
{
    return nmhTestv2Fsipv6IfRaRDNSSAddrOne (pu4ErrorCode, i4Fsipv6IfIndex,
                                            pTestValFsipv6IfRaRDNSSAddrTwo);
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6IfRaRDNSSAddrThree
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                testValFsipv6IfRaRDNSSAddrThree
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6IfRaRDNSSAddrThree (UINT4 *pu4ErrorCode, INT4 i4Fsipv6IfIndex,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pTestValFsipv6IfRaRDNSSAddrThree)
{
    return nmhTestv2Fsipv6IfRaRDNSSAddrOne (pu4ErrorCode, i4Fsipv6IfIndex,
                                            pTestValFsipv6IfRaRDNSSAddrThree);
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6IfRaRDNSSRowStatus
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                testValFsipv6IfRaRDNSSRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6IfRaRDNSSRowStatus (UINT4 *pu4ErrorCode, INT4 i4Fsipv6IfIndex,
                                   INT4 i4TestValFsipv6IfRaRDNSSRowStatus)
{

    tIp6Addr            Ip6Addr;
    tLip6If            *pIf6Entry = NULL;

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6Entry == NULL)
    {
        /* Interface Does not Exist */
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
        return SNMP_FAILURE;
    }

    /* Validate the Row Status Value. */
    if ((i4TestValFsipv6IfRaRDNSSRowStatus == IP6FWD_NOT_READY) ||
        (i4TestValFsipv6IfRaRDNSSRowStatus == IP6FWD_NOT_IN_SERVICE) ||
        (i4TestValFsipv6IfRaRDNSSRowStatus < IP6FWD_ACTIVE) ||
        (i4TestValFsipv6IfRaRDNSSRowStatus > IP6FWD_DESTROY))
    {
        /* Invalid Row Status Value. */
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_INVALID_ADMIN_STATUS);
        return SNMP_FAILURE;
    }

#ifdef TUNNEL_WANTED
    if (pIf6Entry->u1IfType == IP6_TUNNEL_INTERFACE_TYPE)
    {
        if ((i4TestValFsipv6IfRaRDNSSRowStatus == IP6FWD_CREATE_AND_GO) ||
            (i4TestValFsipv6IfRaRDNSSRowStatus == IP6FWD_CREATE_AND_WAIT))
        {
            /* If the address is for Auto tunnel interface check the
             * address compatiblity with the interface's ipv4 address */
            if (Lip6PortCheckAutoTunlAddr (&Ip6Addr, (UINT4) i4Fsipv6IfIndex) ==
                OSIX_FAILURE)
            {
                CLI_SET_ERR (CLI_IP6_INCOMPATIBLE_ADDR);
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
        }
    }
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6IfDomainNameOne
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                testValFsipv6IfDomainNameOne
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6IfDomainNameOne (UINT4 *pu4ErrorCode, INT4 i4Fsipv6IfIndex,
                                tSNMP_OCTET_STRING_TYPE *
                                pTestValFsipv6IfDomainNameOne)
{
    tLip6If            *pIf6Entry = NULL;

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfIndex);
    if (pIf6Entry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (pTestValFsipv6IfDomainNameOne);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6IfDomainNameTwo
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                testValFsipv6IfDomainNameTwo
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6IfDomainNameTwo (UINT4 *pu4ErrorCode, INT4 i4Fsipv6IfIndex,
                                tSNMP_OCTET_STRING_TYPE *
                                pTestValFsipv6IfDomainNameTwo)
{
    tLip6If            *pIf6Entry = NULL;

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfIndex);
    if (pIf6Entry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (pTestValFsipv6IfDomainNameTwo);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6IfDomainNameThree
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                testValFsipv6IfDomainNameThree
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6IfDomainNameThree (UINT4 *pu4ErrorCode, INT4 i4Fsipv6IfIndex,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pTestValFsipv6IfDomainNameThree)
{
    tLip6If            *pIf6Entry = NULL;

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfIndex);
    if (pIf6Entry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (pTestValFsipv6IfDomainNameThree);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6IfDnsLifeTime
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                testValFsipv6IfDnsLifeTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6IfDnsLifeTime (UINT4 *pu4ErrorCode, INT4 i4Fsipv6IfIndex,
                              UINT4 i4TestValFsipv6IfDnsLifeTime)
{
    tLip6If            *pIf6Entry = NULL;

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfIndex);
    if (pIf6Entry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
        return SNMP_FAILURE;
    }
    if (CheckIp6IsFwdEnabled (i4Fsipv6IfIndex) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if (i4TestValFsipv6IfDnsLifeTime != IPVX_ZERO)
    {

        if ((i4TestValFsipv6IfDnsLifeTime > IP6_IF_RA_RDNSS_MAX_LIFETIME)
            || (pIf6Entry->u4MaxRaTime > i4TestValFsipv6IfDnsLifeTime))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_IP6_INVALID_RT_DNS_LIFETIME);
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6IfRaRDNSSAddrOneLife
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                testValFsipv6IfRaRDNSSAddrOneLife
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6IfRaRDNSSAddrOneLife (UINT4 *pu4ErrorCode, INT4 i4Fsipv6IfIndex,
                                     UINT4 u4TestValFsipv6IfRaRDNSSAddrOneLife)
{
    return nmhTestv2Fsipv6IfRaRDNSSLifetime (pu4ErrorCode, i4Fsipv6IfIndex,
                                             u4TestValFsipv6IfRaRDNSSAddrOneLife);
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6IfRaRDNSSAddrTwoLife
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                testValFsipv6IfRaRDNSSAddrTwoLife
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6IfRaRDNSSAddrTwoLife (UINT4 *pu4ErrorCode, INT4 i4Fsipv6IfIndex,
                                     UINT4 u4TestValFsipv6IfRaRDNSSAddrTwoLife)
{
    return nmhTestv2Fsipv6IfRaRDNSSLifetime (pu4ErrorCode, i4Fsipv6IfIndex,
                                             u4TestValFsipv6IfRaRDNSSAddrTwoLife);
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6IfRaRDNSSAddrThreeLife
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                testValFsipv6IfRaRDNSSAddrThreeLife
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6IfRaRDNSSAddrThreeLife (UINT4 *pu4ErrorCode,
                                       INT4 i4Fsipv6IfIndex,
                                       UINT4
                                       u4TestValFsipv6IfRaRDNSSAddrThreeLife)
{
    return nmhTestv2Fsipv6IfRaRDNSSLifetime (pu4ErrorCode, i4Fsipv6IfIndex,
                                             u4TestValFsipv6IfRaRDNSSAddrThreeLife);
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6IfDomainNameOneLife 
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                testValFsipv6IfRaDNSAddrOneLife
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6IfDomainNameOneLife (UINT4 *pu4ErrorCode, INT4 i4Fsipv6IfIndex,
                                    UINT4 u4TestValFsipv6IfRaDNSAddrOneLife)
{
    return nmhTestv2Fsipv6IfDnsLifeTime (pu4ErrorCode, i4Fsipv6IfIndex,
                                         u4TestValFsipv6IfRaDNSAddrOneLife);
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6IfDomainNameTwoLife
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                testValFsipv6IfRaDNSAddrTwoLife
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6IfDomainNameTwoLife (UINT4 *pu4ErrorCode, INT4 i4Fsipv6IfIndex,
                                    UINT4 u4TestValFsipv6IfRaDNSAddrTwoLife)
{
    return nmhTestv2Fsipv6IfDnsLifeTime (pu4ErrorCode, i4Fsipv6IfIndex,
                                         u4TestValFsipv6IfRaDNSAddrTwoLife);
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6IfDomainNameThreeLife
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                testValFsipv6IfRaDNSAddrThreeLife
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6IfDomainNameThreeLife (UINT4 *pu4ErrorCode, INT4 i4Fsipv6IfIndex,
                                      UINT4 u4TestValFsipv6IfRaDNSAddrThreeLife)
{
    return nmhTestv2Fsipv6IfDnsLifeTime (pu4ErrorCode, i4Fsipv6IfIndex,
                                         u4TestValFsipv6IfRaDNSAddrThreeLife);
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Fsipv6IfRaRDNSSTable
 Input       :  The Indices
                Fsipv6IfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsipv6IfRaRDNSSTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/*********************************************************************
 *** *   Function Name : Ip6SetIfAddr
 *** *   Description   : Configuring Ipv6 Address, when event is received
 *                       from Linux.
 *** *   Input(s)      : u4Ip6IfIndex - Interface Index.
 *                       tIp6Addr - IPv6 Address.
 *                       i4PrefixLen - Prefix of address.
 *** *   Output(s)     : None.
 *** *   Return Values : None.
 **********************************************************************/
VOID
Ip6SetIfAddr (UINT4 u4Ip6IfIndex, tIp6Addr Ip6Addr, INT4 i4PrefixLen,
              INT4 i4AddrType)
{
    tSNMP_OCTET_STRING_TYPE Addr;
    tSNMP_OCTET_STRING_TYPE IfToken;
    UINT4               u4ErrorCode;
    INT4                i4Status;
    INT4                i4Ip6IfIndex = (INT4) u4Ip6IfIndex;
    UINT1               au1AddrOctetList[IP6_ADDR_SIZE];
    UINT1               au1IfTokenOctetList[IP6_EUI_ADDRESS_LEN];

    MEMSET (au1AddrOctetList, IP6_ZERO, IP6_ADDR_SIZE);
    MEMSET (au1IfTokenOctetList, IP6_ZERO, IP6_EUI_ADDRESS_LEN);

    Addr.pu1_OctetList = au1AddrOctetList;
    MEMCPY (Addr.pu1_OctetList, &Ip6Addr, 16);
    Addr.i4_Length = 16;

    if (IS_ADDR_MULTI (Ip6Addr) || IS_ADDR_UNSPECIFIED (Ip6Addr))
    {
        CLI_SET_ERR (CLI_IP6_INVALID_ADDRESS);
        return;
    }

    /*
     * EUI-64 address configuration is supported only for CLI. When the
     * address type is set as EUI-64 form the fetch the interface
     * identifier value and form the complete IP6 address to be configured.
     */
    if (i4AddrType == ADDR6_UNSPECIFIED)
    {                            /* EUI-64 */
        /* Length should not be greater than 64. */
        if (i4PrefixLen != IP6_EUI_ADDRESS_PREFIX_LEN)
        {
            CLI_SET_ERR (CLI_IP6_INVALID_PREFIXLEN);
            return;
        }

        /* EUI-64 can be configured only if IPv6 is already enabled over
         * that interface */
        if ((nmhGetFsipv6IfAdminStatus
             (i4Ip6IfIndex, &i4Status)
             == SNMP_FAILURE) || (i4Status != NETIPV6_ADMIN_UP))
        {
            CLI_SET_ERR (CLI_IP6_ADDR_NOT_CONFIGURED);
            return;
        }

        /* Form the Actual IP6 Address */
        IfToken.pu1_OctetList = au1IfTokenOctetList;
        IfToken.i4_Length = 0;

        if (nmhGetFsipv6IfToken (i4Ip6IfIndex, &IfToken) == SNMP_FAILURE)
        {
            return;
        }

        MEMCPY (&Addr.pu1_OctetList[IP6_EUI_ADDRESS_LEN],
                IfToken.pu1_OctetList, IP6_EUI_ADDRESS_LEN);

        /* Also update the address type as UNICAST */
        i4AddrType = ADDR6_UNICAST;
    }

    /* ADMIN STATUS */
    if (nmhTestv2Fsipv6AddrAdminStatus (&u4ErrorCode,
                                        i4Ip6IfIndex, &Addr,
                                        i4PrefixLen, CREATE_AND_WAIT)
        == SNMP_FAILURE)
    {
        return;
    }

    /* First the Admin Status Should be set to CREATE_AND_WAIT */
    if (nmhSetFsipv6AddrAdminStatus (i4Ip6IfIndex,
                                     &Addr,
                                     i4PrefixLen,
                                     CREATE_AND_WAIT) == SNMP_FAILURE)
    {
        return;
    }
    /* set to specified Address Type */
    if (nmhTestv2Fsipv6AddrType (&u4ErrorCode,
                                 i4Ip6IfIndex,
                                 &Addr,
                                 i4PrefixLen, i4AddrType) == SNMP_FAILURE)
    {
        nmhSetFsipv6AddrAdminStatus (i4Ip6IfIndex, &Addr, i4PrefixLen, DESTROY);
        return;
    }

    if (nmhSetFsipv6AddrType (i4Ip6IfIndex,
                              &Addr, i4PrefixLen, i4AddrType) == SNMP_FAILURE)
    {
        nmhSetFsipv6AddrAdminStatus (i4Ip6IfIndex, &Addr, i4PrefixLen, DESTROY);
        return;
    }

    /* ADMIN STATUS */
    if (nmhTestv2Fsipv6AddrAdminStatus (&u4ErrorCode,
                                        i4Ip6IfIndex, &Addr,
                                        i4PrefixLen, ACTIVE) == SNMP_FAILURE)
    {
        nmhSetFsipv6AddrAdminStatus (i4Ip6IfIndex, &Addr, i4PrefixLen, DESTROY);
        return;
    }

    /* First the Admin Status Should be set to 1 (up) */
    if (nmhSetFsipv6AddrAdminStatus (i4Ip6IfIndex,
                                     &Addr,
                                     i4PrefixLen, ACTIVE) == SNMP_FAILURE)

    {
        nmhSetFsipv6AddrAdminStatus (i4Ip6IfIndex, &Addr, i4PrefixLen, DESTROY);
        return;
    }
    return;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfNDProxyAdminStatus
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                retValFsipv6IfNDProxyAdminStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfNDProxyAdminStatus (INT4 i4Fsipv6IfIndex,
                                  INT4 *pi4RetValFsipv6IfNDProxyAdminStatus)
{
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (pi4RetValFsipv6IfNDProxyAdminStatus);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfNDProxyMode
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                retValFsipv6IfNDProxyMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfNDProxyMode (INT4 i4Fsipv6IfIndex,
                           INT4 *pi4RetValFsipv6IfNDProxyMode)
{
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (pi4RetValFsipv6IfNDProxyMode);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfNDProxyOperStatus
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                retValFsipv6IfNDProxyOperStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfNDProxyOperStatus (INT4 i4Fsipv6IfIndex,
                                 INT4 *pi4RetValFsipv6IfNDProxyOperStatus)
{
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (pi4RetValFsipv6IfNDProxyOperStatus);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfNDProxyUpStream
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                retValFsipv6IfNDProxyUpStream
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfNDProxyUpStream (INT4 i4Fsipv6IfIndex,
                               INT4 *pi4RetValFsipv6IfNDProxyUpStream)
{
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (pi4RetValFsipv6IfNDProxyUpStream);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6IfNDProxyAdminStatus
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                setValFsipv6IfNDProxyAdminStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6IfNDProxyAdminStatus (INT4 i4Fsipv6IfIndex,
                                  INT4 i4SetValFsipv6IfNDProxyAdminStatus)
{
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (i4SetValFsipv6IfNDProxyAdminStatus);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6IfNDProxyMode
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                setValFsipv6IfNDProxyMode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6IfNDProxyMode (INT4 i4Fsipv6IfIndex,
                           INT4 i4SetValFsipv6IfNDProxyMode)
{
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (i4SetValFsipv6IfNDProxyMode);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6IfNDProxyUpStream
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                setValFsipv6IfNDProxyUpStream
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6IfNDProxyUpStream (INT4 i4Fsipv6IfIndex,
                               INT4 i4SetValFsipv6IfNDProxyUpStream)
{
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (i4SetValFsipv6IfNDProxyUpStream);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6IfNDProxyAdminStatus
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                testValFsipv6IfNDProxyAdminStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6IfNDProxyAdminStatus (UINT4 *pu4ErrorCode, INT4 i4Fsipv6IfIndex,
                                     INT4 i4TestValFsipv6IfNDProxyAdminStatus)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (i4TestValFsipv6IfNDProxyAdminStatus);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6IfNDProxyMode
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                testValFsipv6IfNDProxyMode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6IfNDProxyMode (UINT4 *pu4ErrorCode, INT4 i4Fsipv6IfIndex,
                              INT4 i4TestValFsipv6IfNDProxyMode)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (i4TestValFsipv6IfNDProxyMode);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6IfNDProxyUpStream
 Input       :  The Indices
                Fsipv6IfIndex

                The Object 
                testValFsipv6IfNDProxyUpStream
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6IfNDProxyUpStream (UINT4 *pu4ErrorCode, INT4 i4Fsipv6IfIndex,
                                  INT4 i4TestValFsipv6IfNDProxyUpStream)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (i4TestValFsipv6IfNDProxyUpStream);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6PingHostName
 Input       :  The Indices
                Fsipv6PingIndex

                The Object
                retValFsipv6PingHostName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6PingHostName (INT4 i4Fsipv6PingIndex,
                          tSNMP_OCTET_STRING_TYPE * pRetValFsipv6PingHostName)
{
    UNUSED_PARAM (i4Fsipv6PingIndex);
    UNUSED_PARAM (pRetValFsipv6PingHostName);
    return SNMP_SUCCESS;
}

/****************************************************************************                           Function    :  nmhSetFsipv6PingHostName
 Input       :  The Indices
                Fsipv6PingIndex
                                                                                                                       The Object
                setValFsipv6PingHostName
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6PingHostName (INT4 i4Fsipv6PingIndex,
                          tSNMP_OCTET_STRING_TYPE * pSetValFsipv6PingHostName)
{
    UNUSED_PARAM (i4Fsipv6PingIndex);
    UNUSED_PARAM (pSetValFsipv6PingHostName);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6PingHostName
 Input       :  The Indices
                Fsipv6PingIndex
                                                                                                                       The Object
                testValFsipv6PingHostName
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)                                                 SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6PingHostName (UINT4 *pu4ErrorCode,
                             INT4 i4Fsipv6PingIndex,
                             tSNMP_OCTET_STRING_TYPE
                             * pTestValFsipv6PingHostName)
{
    UNUSED_PARAM (i4Fsipv6PingIndex);
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pTestValFsipv6PingHostName);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6SEND
 Input       :  The Indices

                The Object
                retValFsipv6SENDSecLevel
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SENDSecLevel (INT4 *pi4RetValFsipv6SENDSecLevel)
{
    UNUSED_PARAM (pi4RetValFsipv6SENDSecLevel);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6SENDNbrSecLevel
 Input       :  The Indices

                The Object
                retValFsipv6SENDNbrSecLevel
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SENDNbrSecLevel (INT4 *pi4RetValFsipv6SENDNbrSecLevel)
{
    UNUSED_PARAM (pi4RetValFsipv6SENDNbrSecLevel);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6SENDAuthType
 Input       :  The Indices

                The Object
                retValFsipv6SENDAuthType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SENDAuthType (INT4 *pi4RetValFsipv6SENDAuthType)
{
    UNUSED_PARAM (pi4RetValFsipv6SENDAuthType);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6SENDMinBits
 Input       :  The Indices

                The Object
                retValFsipv6SENDMinBits
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SENDMinBits (INT4 *pi4RetValFsipv6SENDMinBits)
{
    UNUSED_PARAM (pi4RetValFsipv6SENDMinBits);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6SENDSecDAD
 Input       :  The Indices

                The Object
                retValFsipv6SENDSecDAD
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SENDSecDAD (INT4 *pi4RetValFsipv6SENDSecDAD)
{
    UNUSED_PARAM (pi4RetValFsipv6SENDSecDAD);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6SENDPrefixChk
 Input       :  The Indices

                The Object
                retValFsipv6SENDPrefixChk
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SENDPrefixChk (INT4 *pi4RetValFsipv6SENDPrefixChk)
{
    UNUSED_PARAM (pi4RetValFsipv6SENDPrefixChk);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6SENDSecLevel
 Input       :  The Indices

                The Object
                setValFsipv6SENDSecLevel
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6SENDSecLevel (INT4 i4SetValFsipv6SENDSecLevel)
{
    UNUSED_PARAM (i4SetValFsipv6SENDSecLevel);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6SENDNbrSecLevel
 Input       :  The Indices

                The Object
                setValFsipv6SENDNbrSecLevel
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6SENDNbrSecLevel (INT4 i4SetValFsipv6SENDNbrSecLevel)
{
    UNUSED_PARAM (i4SetValFsipv6SENDNbrSecLevel);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6SENDAuthType
 Input       :  The Indices

                The Object
                setValFsipv6SENDAuthType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6SENDAuthType (INT4 i4SetValFsipv6SENDAuthType)
{
    UNUSED_PARAM (i4SetValFsipv6SENDAuthType);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6SENDMinBits
 Input       :  The Indices

                The Object
                setValFsipv6SENDMinBits
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6SENDMinBits (INT4 i4SetValFsipv6SENDMinBits)
{
    UNUSED_PARAM (i4SetValFsipv6SENDMinBits);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6SENDSecDAD
 Input       :  The Indices

                The Object
                setValFsipv6SENDSecDAD
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6SENDSecDAD (INT4 i4SetValFsipv6SENDSecDAD)
{
    UNUSED_PARAM (i4SetValFsipv6SENDSecDAD);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6SENDPrefixChk
 Input       :  The Indices

                The Object
                setValFsipv6SENDPrefixChk
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6SENDPrefixChk (INT4 i4SetValFsipv6SENDPrefixChk)
{
    UNUSED_PARAM (i4SetValFsipv6SENDPrefixChk);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6SENDSecLevel
 Input       :  The Indices

                The Object
                testValFsipv6SENDSecLevel
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6SENDSecLevel (UINT4 *pu4ErrorCode,
                             INT4 i4TestValFsipv6SENDSecLevel)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4TestValFsipv6SENDSecLevel);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6SENDNbrSecLevel
 Input       :  The Indices

                The Object
                testValFsipv6SENDNbrSecLevel
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6SENDNbrSecLevel (UINT4 *pu4ErrorCode,
                                INT4 i4TestValFsipv6SENDNbrSecLevel)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4TestValFsipv6SENDNbrSecLevel);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6SENDAuthType
 Input       :  The Indices

                The Object
                testValFsipv6SENDAuthType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6SENDAuthType (UINT4 *pu4ErrorCode,
                             INT4 i4TestValFsipv6SENDAuthType)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4TestValFsipv6SENDAuthType);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6SENDMinBits
 Input       :  The Indices

                The Object
                testValFsipv6SENDMinBits
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6SENDMinBits (UINT4 *pu4ErrorCode,
                            INT4 i4TestValFsipv6SENDMinBits)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4TestValFsipv6SENDMinBits);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6SENDSecDAD
 Input       :  The Indices

                The Object
                testValFsipv6SENDSecDAD
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6SENDSecDAD (UINT4 *pu4ErrorCode, INT4 i4TestValFsipv6SENDSecDAD)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4TestValFsipv6SENDSecDAD);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6SENDPrefixChk
 Input       :  The Indices

                The Object
                testValFsipv6SENDPrefixChk
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6SENDPrefixChk (UINT4 *pu4ErrorCode,
                              INT4 i4TestValFsipv6SENDPrefixChk)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4TestValFsipv6SENDPrefixChk);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Fsipv6SENDSecLevel
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsipv6SENDSecLevel (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Fsipv6SENDNbrSecLevel
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsipv6SENDNbrSecLevel (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Fsipv6SENDAuthType
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsipv6SENDAuthType (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Fsipv6SENDMinBits
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsipv6SENDMinBits (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Fsipv6SENDSecDAD
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsipv6SENDSecDAD (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Fsipv6SENDPrefixChk
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsipv6SENDPrefixChk (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfSENDSecStatus
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                retValFsipv6IfSENDSecStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfSENDSecStatus (INT4 i4Fsipv6IfIndex,
                             INT4 *pi4RetValFsipv6IfSENDSecStatus)
{
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (pi4RetValFsipv6IfSENDSecStatus);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfSENDDeltaTimestamp
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                retValFsipv6IfSENDDeltaTimestamp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfSENDDeltaTimestamp (INT4 i4Fsipv6IfIndex,
                                  UINT4 *pu4RetValFsipv6IfSENDDeltaTimestamp)
{
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (pu4RetValFsipv6IfSENDDeltaTimestamp);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfSENDFuzzTimestamp
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                retValFsipv6IfSENDFuzzTimestamp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfSENDFuzzTimestamp (INT4 i4Fsipv6IfIndex,
                                 UINT4 *pu4RetValFsipv6IfSENDFuzzTimestamp)
{
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (pu4RetValFsipv6IfSENDFuzzTimestamp);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfSENDDriftTimestamp
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                retValFsipv6IfSENDDriftTimestamp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfSENDDriftTimestamp (INT4 i4Fsipv6IfIndex,
                                  UINT4 *pu4RetValFsipv6IfSENDDriftTimestamp)
{
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (pu4RetValFsipv6IfSENDDriftTimestamp);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfDefRoutePreference
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                retValFsipv6IfDefRoutePreference
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfDefRoutePreference (INT4 i4Fsipv6IfIndex,
                                  INT4 *pi4RetValFsipv6IfDefRoutePreference)
{
    tLip6If            *pIf6 = NULL;
    pIf6 = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6 == NULL)
    {
        CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
        return SNMP_FAILURE;
    }

    *pi4RetValFsipv6IfDefRoutePreference = (INT4) pIf6->u2Preference;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6IfSENDSecStatus
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                setValFsipv6IfSENDSecStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6IfSENDSecStatus (INT4 i4Fsipv6IfIndex,
                             INT4 i4SetValFsipv6IfSENDSecStatus)
{
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (i4SetValFsipv6IfSENDSecStatus);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6IfSENDDeltaTimestamp
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                setValFsipv6IfSENDDeltaTimestamp
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6IfSENDDeltaTimestamp (INT4 i4Fsipv6IfIndex,
                                  UINT4 u4SetValFsipv6IfSENDDeltaTimestamp)
{
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (u4SetValFsipv6IfSENDDeltaTimestamp);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6IfSENDFuzzTimestamp
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                setValFsipv6IfSENDFuzzTimestamp
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6IfSENDFuzzTimestamp (INT4 i4Fsipv6IfIndex,
                                 UINT4 u4SetValFsipv6IfSENDFuzzTimestamp)
{
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (u4SetValFsipv6IfSENDFuzzTimestamp);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6IfSENDDriftTimestamp
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                setValFsipv6IfSENDDriftTimestamp
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6IfSENDDriftTimestamp (INT4 i4Fsipv6IfIndex,
                                  UINT4 u4SetValFsipv6IfSENDDriftTimestamp)
{
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (u4SetValFsipv6IfSENDDriftTimestamp);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6IfDefRoutePreference
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                setValFsipv6IfDefRoutePreference
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6IfDefRoutePreference (INT4 i4Fsipv6IfIndex,
                                  INT4 i4SetValFsipv6IfDefRoutePreference)
{
    INT4                i4RetVal = OSIX_FAILURE;

    tLip6If            *pIf6 = NULL;

    pIf6 = Lip6UtlGetIfEntry ((UINT4) i4Fsipv6IfIndex);

    if (pIf6 == NULL)
    {
        CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
        return SNMP_FAILURE;
    }

    i4RetVal = Ip6SetAdvDefaultPreference (i4Fsipv6IfIndex,
                                           i4SetValFsipv6IfDefRoutePreference);

    if (i4RetVal == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    pIf6->u2Preference = (UINT2) i4SetValFsipv6IfDefRoutePreference;

    IncMsrForIpv6IfTable (i4Fsipv6IfIndex, 'i',
                          &i4SetValFsipv6IfDefRoutePreference,
                          FsMIIpv6IfDefRoutePreference,
                          sizeof (FsMIIpv6IfDefRoutePreference) /
                          sizeof (UINT4));

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6IfSENDSecStatus
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                testValFsipv6IfSENDSecStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6IfSENDSecStatus (UINT4 *pu4ErrorCode, INT4 i4Fsipv6IfIndex,
                                INT4 i4TestValFsipv6IfSENDSecStatus)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (i4TestValFsipv6IfSENDSecStatus);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6IfSENDDeltaTimestamp
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                testValFsipv6IfSENDDeltaTimestamp
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6IfSENDDeltaTimestamp (UINT4 *pu4ErrorCode, INT4 i4Fsipv6IfIndex,
                                     UINT4 u4TestValFsipv6IfSENDDeltaTimestamp)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (u4TestValFsipv6IfSENDDeltaTimestamp);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6IfSENDFuzzTimestamp
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                testValFsipv6IfSENDFuzzTimestamp
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6IfSENDFuzzTimestamp (UINT4 *pu4ErrorCode, INT4 i4Fsipv6IfIndex,
                                    UINT4 u4TestValFsipv6IfSENDFuzzTimestamp)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (u4TestValFsipv6IfSENDFuzzTimestamp);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6IfSENDDriftTimestamp
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                testValFsipv6IfSENDDriftTimestamp
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6IfSENDDriftTimestamp (UINT4 *pu4ErrorCode, INT4 i4Fsipv6IfIndex,
                                     UINT4 u4TestValFsipv6IfSENDDriftTimestamp)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (u4TestValFsipv6IfSENDDriftTimestamp);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6IfDefRoutePreference
 Input       :  The Indices
                Fsipv6IfIndex

                The Object
                testValFsipv6IfDefRoutePreference
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6IfDefRoutePreference (UINT4 *pu4ErrorCode,
                                     INT4 i4Fsipv6IfIndex,
                                     INT4 i4TestValFsipv6IfDefRoutePreference)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (i4TestValFsipv6IfDefRoutePreference);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfStatsSENDDroppedPkts
 Input       :  The Indices
                Fsipv6IfStatsIndex

                The Object
                retValFsipv6IfStatsSENDDroppedPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfStatsSENDDroppedPkts (INT4 i4Fsipv6IfStatsIndex,
                                    UINT4
                                    *pu4RetValFsipv6IfStatsSENDDroppedPkts)
{
    UNUSED_PARAM (i4Fsipv6IfStatsIndex);
    UNUSED_PARAM (pu4RetValFsipv6IfStatsSENDDroppedPkts);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6IfStatsSENDInvalidPkts
 Input       :  The Indices
                Fsipv6IfStatsIndex

                The Object
                retValFsipv6IfStatsSENDInvalidPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6IfStatsSENDInvalidPkts (INT4 i4Fsipv6IfStatsIndex,
                                    UINT4
                                    *pu4RetValFsipv6IfStatsSENDInvalidPkts)
{
    UNUSED_PARAM (i4Fsipv6IfStatsIndex);
    UNUSED_PARAM (pu4RetValFsipv6IfStatsSENDInvalidPkts);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6AddrSENDCgaStatus
 Input       :  The Indices
                Fsipv6AddrIndex
                Fsipv6AddrAddress
                Fsipv6AddrPrefixLen

                The Object
                retValFsipv6AddrSENDCgaStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6AddrSENDCgaStatus (INT4 i4Fsipv6AddrIndex,
                               tSNMP_OCTET_STRING_TYPE * pFsipv6AddrAddress,
                               INT4 i4Fsipv6AddrPrefixLen,
                               INT4 *pi4RetValFsipv6AddrSENDCgaStatus)
{
    UNUSED_PARAM (i4Fsipv6AddrIndex);
    UNUSED_PARAM (pFsipv6AddrAddress);
    UNUSED_PARAM (i4Fsipv6AddrPrefixLen);
    UNUSED_PARAM (pi4RetValFsipv6AddrSENDCgaStatus);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6AddrSENDCgaModifier
 Input       :  The Indices
                Fsipv6AddrIndex
                Fsipv6AddrAddress
                Fsipv6AddrPrefixLen

                The Object
                retValFsipv6AddrSENDCgaModifier
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6AddrSENDCgaModifier (INT4 i4Fsipv6AddrIndex,
                                 tSNMP_OCTET_STRING_TYPE * pFsipv6AddrAddress,
                                 INT4 i4Fsipv6AddrPrefixLen,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pRetValFsipv6AddrSENDCgaModifier)
{
    UNUSED_PARAM (i4Fsipv6AddrIndex);
    UNUSED_PARAM (pFsipv6AddrAddress);
    UNUSED_PARAM (i4Fsipv6AddrPrefixLen);
    UNUSED_PARAM (pRetValFsipv6AddrSENDCgaModifier);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6AddrSENDCollisionCount
 Input       :  The Indices
                Fsipv6AddrIndex
                Fsipv6AddrAddress
                Fsipv6AddrPrefixLen

                The Object
                retValFsipv6AddrSENDCollisionCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6AddrSENDCollisionCount (INT4 i4Fsipv6AddrIndex,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsipv6AddrAddress,
                                    INT4 i4Fsipv6AddrPrefixLen,
                                    INT4 *pi4RetValFsipv6AddrSENDCollisionCount)
{
    UNUSED_PARAM (i4Fsipv6AddrIndex);
    UNUSED_PARAM (pFsipv6AddrAddress);
    UNUSED_PARAM (i4Fsipv6AddrPrefixLen);
    UNUSED_PARAM (pi4RetValFsipv6AddrSENDCollisionCount);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6AddrSENDCgaModifier
 Input       :  The Indices
                Fsipv6AddrIndex
                Fsipv6AddrAddress
                Fsipv6AddrPrefixLen

                The Object
                setValFsipv6AddrSENDCgaModifier
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6AddrSENDCgaModifier (INT4 i4Fsipv6AddrIndex,
                                 tSNMP_OCTET_STRING_TYPE * pFsipv6AddrAddress,
                                 INT4 i4Fsipv6AddrPrefixLen,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pSetValFsipv6AddrSENDCgaModifier)
{
    UNUSED_PARAM (i4Fsipv6AddrIndex);
    UNUSED_PARAM (pFsipv6AddrAddress);
    UNUSED_PARAM (i4Fsipv6AddrPrefixLen);
    UNUSED_PARAM (pSetValFsipv6AddrSENDCgaModifier);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6AddrSENDCgaStatus
 Input       :  The Indices
                Fsipv6AddrIndex
                Fsipv6AddrAddress
                Fsipv6AddrPrefixLen

                The Object
                setValFsipv6AddrSENDCgaStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6AddrSENDCgaStatus (INT4 i4Fsipv6AddrIndex,
                               tSNMP_OCTET_STRING_TYPE * pFsipv6AddrAddress,
                               INT4 i4Fsipv6AddrPrefixLen,
                               INT4 i4SetValFsipv6AddrSENDCgaStatus)
{
    UNUSED_PARAM (i4Fsipv6AddrIndex);
    UNUSED_PARAM (pFsipv6AddrAddress);
    UNUSED_PARAM (i4Fsipv6AddrPrefixLen);
    UNUSED_PARAM (i4SetValFsipv6AddrSENDCgaStatus);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6AddrSENDCollisionCount
 Input       :  The Indices
                Fsipv6AddrIndex
                Fsipv6AddrAddress
                Fsipv6AddrPrefixLen

                The Object
                setValFsipv6AddrSENDCollisionCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6AddrSENDCollisionCount (INT4 i4Fsipv6AddrIndex,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsipv6AddrAddress,
                                    INT4 i4Fsipv6AddrPrefixLen,
                                    INT4 i4SetValFsipv6AddrSENDCollisionCount)
{
    UNUSED_PARAM (i4Fsipv6AddrIndex);
    UNUSED_PARAM (pFsipv6AddrAddress);
    UNUSED_PARAM (i4Fsipv6AddrPrefixLen);
    UNUSED_PARAM (i4SetValFsipv6AddrSENDCollisionCount);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6AddrSENDCgaModifier
 Input       :  The Indices
                Fsipv6AddrIndex
                Fsipv6AddrAddress
                Fsipv6AddrPrefixLen

                The Object
                testValFsipv6AddrSENDCgaModifier
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6AddrSENDCgaModifier (UINT4 *pu4ErrorCode, INT4 i4Fsipv6AddrIndex,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsipv6AddrAddress,
                                    INT4 i4Fsipv6AddrPrefixLen,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pTestValFsipv6AddrSENDCgaModifier)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4Fsipv6AddrIndex);
    UNUSED_PARAM (pFsipv6AddrAddress);
    UNUSED_PARAM (i4Fsipv6AddrPrefixLen);
    UNUSED_PARAM (pTestValFsipv6AddrSENDCgaModifier);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6AddrSENDCgaStatus
 Input       :  The Indices
                Fsipv6AddrIndex
                Fsipv6AddrAddress
                Fsipv6AddrPrefixLen

                The Object
                testValFsipv6AddrSENDCgaStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6AddrSENDCgaStatus (UINT4 *pu4ErrorCode, INT4 i4Fsipv6AddrIndex,
                                  tSNMP_OCTET_STRING_TYPE * pFsipv6AddrAddress,
                                  INT4 i4Fsipv6AddrPrefixLen,
                                  INT4 i4TestValFsipv6AddrSENDCgaStatus)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4Fsipv6AddrIndex);
    UNUSED_PARAM (pFsipv6AddrAddress);
    UNUSED_PARAM (i4Fsipv6AddrPrefixLen);
    UNUSED_PARAM (i4TestValFsipv6AddrSENDCgaStatus);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6AddrSENDCollisionCount
 Input       :  The Indices
                Fsipv6AddrIndex
                Fsipv6AddrAddress
                Fsipv6AddrPrefixLen

                The Object
                testValFsipv6AddrSENDCollisionCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6AddrSENDCollisionCount (UINT4 *pu4ErrorCode,
                                       INT4 i4Fsipv6AddrIndex,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFsipv6AddrAddress,
                                       INT4 i4Fsipv6AddrPrefixLen,
                                       INT4
                                       i4TestValFsipv6AddrSENDCollisionCount)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4Fsipv6AddrIndex);
    UNUSED_PARAM (pFsipv6AddrAddress);
    UNUSED_PARAM (i4Fsipv6AddrPrefixLen);
    UNUSED_PARAM (i4TestValFsipv6AddrSENDCollisionCount);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsipv6SENDSentPktStatsTable
 Input       :  The Indices
                Fsipv6IfIndex
                Fsipv6SENDSentPktType
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsipv6SENDSentPktStatsTable (INT4 *pi4Fsipv6IfIndex,
                                             INT4 *pi4Fsipv6SENDSentPktType)
{
    UNUSED_PARAM (pi4Fsipv6IfIndex);
    UNUSED_PARAM (pi4Fsipv6SENDSentPktType);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsipv6SENDSentPktStatsTable
 Input       :  The Indices
                Fsipv6IfIndex
                Fsipv6SENDSentPktType
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */
INT1
nmhValidateIndexInstanceFsipv6SENDSentPktStatsTable (INT4 i4Fsipv6IfIndex,
                                                     INT4
                                                     i4Fsipv6SENDSentPktType)
{
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (i4Fsipv6SENDSentPktType);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsipv6SENDSentPktStatsTable
 Input       :  The Indices
                Fsipv6IfIndex
                nextFsipv6IfIndex
                Fsipv6SENDSentPktType
                nextFsipv6SENDSentPktType
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsipv6SENDSentPktStatsTable (INT4 i4Fsipv6IfIndex,
                                            INT4 *pi4NextFsipv6IfIndex,
                                            INT4 i4Fsipv6SENDSentPktType,
                                            INT4 *pi4NextFsipv6SENDSentPktType)
{
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (i4Fsipv6SENDSentPktType);
    UNUSED_PARAM (pi4NextFsipv6IfIndex);
    UNUSED_PARAM (pi4NextFsipv6SENDSentPktType);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6SENDSentCgaOptPkts
 Input       :  The Indices
                Fsipv6IfIndex
                Fsipv6SENDSentPktType

                The Object
                retValFsipv6SENDSentCgaOptPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SENDSentCgaOptPkts (INT4 i4Fsipv6IfIndex,
                                INT4 i4Fsipv6SENDSentPktType,
                                UINT4 *pu4RetValFsipv6SENDSentCgaOptPkts)
{
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (i4Fsipv6SENDSentPktType);
    UNUSED_PARAM (pu4RetValFsipv6SENDSentCgaOptPkts);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6SENDSentCertOptPkts
 Input       :  The Indices
                Fsipv6IfIndex
                Fsipv6SENDSentPktType

                The Object
                retValFsipv6SENDSentCertOptPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SENDSentCertOptPkts (INT4 i4Fsipv6IfIndex,
                                 INT4 i4Fsipv6SENDSentPktType,
                                 UINT4 *pu4RetValFsipv6SENDSentCertOptPkts)
{
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (i4Fsipv6SENDSentPktType);
    UNUSED_PARAM (pu4RetValFsipv6SENDSentCertOptPkts);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6SENDSentMtuOptPkts
 Input       :  The Indices
                Fsipv6IfIndex
                Fsipv6SENDSentPktType

                The Object
                retValFsipv6SENDSentMtuOptPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SENDSentMtuOptPkts (INT4 i4Fsipv6IfIndex,
                                INT4 i4Fsipv6SENDSentPktType,
                                UINT4 *pu4RetValFsipv6SENDSentMtuOptPkts)
{
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (i4Fsipv6SENDSentPktType);
    UNUSED_PARAM (pu4RetValFsipv6SENDSentMtuOptPkts);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6SENDSentNonceOptPkts
 Input       :  The Indices
                Fsipv6IfIndex
                Fsipv6SENDSentPktType

                The Object
                retValFsipv6SENDSentNonceOptPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SENDSentNonceOptPkts (INT4 i4Fsipv6IfIndex,
                                  INT4 i4Fsipv6SENDSentPktType,
                                  UINT4 *pu4RetValFsipv6SENDSentNonceOptPkts)
{
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (i4Fsipv6SENDSentPktType);
    UNUSED_PARAM (pu4RetValFsipv6SENDSentNonceOptPkts);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6SENDSentPrefixOptPkts
 Input       :  The Indices
                Fsipv6IfIndex
                Fsipv6SENDSentPktType

                The Object
                retValFsipv6SENDSentPrefixOptPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SENDSentPrefixOptPkts (INT4 i4Fsipv6IfIndex,
                                   INT4 i4Fsipv6SENDSentPktType,
                                   UINT4 *pu4RetValFsipv6SENDSentPrefixOptPkts)
{
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (i4Fsipv6SENDSentPktType);
    UNUSED_PARAM (pu4RetValFsipv6SENDSentPrefixOptPkts);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6SENDSentRedirHdrPkts
 Input       :  The Indices
                Fsipv6IfIndex
                Fsipv6SENDSentPktType

                The Object
                retValFsipv6SENDSentRedirHdrPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SENDSentRedirHdrPkts (INT4 i4Fsipv6IfIndex,
                                  INT4 i4Fsipv6SENDSentPktType,
                                  UINT4 *pu4RetValFsipv6SENDSentRedirHdrPkts)
{
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (i4Fsipv6SENDSentPktType);
    UNUSED_PARAM (pu4RetValFsipv6SENDSentRedirHdrPkts);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6SENDSentRsaOptPkts
 Input       :  The Indices
                Fsipv6IfIndex
                Fsipv6SENDSentPktType

                The Object
                retValFsipv6SENDSentRsaOptPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SENDSentRsaOptPkts (INT4 i4Fsipv6IfIndex,
                                INT4 i4Fsipv6SENDSentPktType,
                                UINT4 *pu4RetValFsipv6SENDSentRsaOptPkts)
{
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (i4Fsipv6SENDSentPktType);
    UNUSED_PARAM (pu4RetValFsipv6SENDSentRsaOptPkts);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6SENDSentSrcLinkAddrPkts
 Input       :  The Indices
                Fsipv6IfIndex
                Fsipv6SENDSentPktType

                The Object
                retValFsipv6SENDSentSrcLinkAddrPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SENDSentSrcLinkAddrPkts (INT4 i4Fsipv6IfIndex,
                                     INT4 i4Fsipv6SENDSentPktType,
                                     UINT4
                                     *pu4RetValFsipv6SENDSentSrcLinkAddrPkts)
{
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (i4Fsipv6SENDSentPktType);
    UNUSED_PARAM (pu4RetValFsipv6SENDSentSrcLinkAddrPkts);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6SENDSentTgtLinkAddrPkts
 Input       :  The Indices
                Fsipv6IfIndex
                Fsipv6SENDSentPktType

                The Object
                retValFsipv6SENDSentTgtLinkAddrPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SENDSentTgtLinkAddrPkts (INT4 i4Fsipv6IfIndex,
                                     INT4 i4Fsipv6SENDSentPktType,
                                     UINT4
                                     *pu4RetValFsipv6SENDSentTgtLinkAddrPkts)
{
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (i4Fsipv6SENDSentPktType);
    UNUSED_PARAM (pu4RetValFsipv6SENDSentTgtLinkAddrPkts);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6SENDSentTaOptPkts
 Input       :  The Indices
                Fsipv6IfIndex
                Fsipv6SENDSentPktType

                The Object
                retValFsipv6SENDSentTaOptPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SENDSentTaOptPkts (INT4 i4Fsipv6IfIndex,
                               INT4 i4Fsipv6SENDSentPktType,
                               UINT4 *pu4RetValFsipv6SENDSentTaOptPkts)
{
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (i4Fsipv6SENDSentPktType);
    UNUSED_PARAM (pu4RetValFsipv6SENDSentTaOptPkts);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6SENDSentTimeStampOptPkts
 Input       :  The Indices
                Fsipv6IfIndex
                Fsipv6SENDSentPktType

                The Object
                retValFsipv6SENDSentTimeStampOptPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SENDSentTimeStampOptPkts (INT4 i4Fsipv6IfIndex,
                                      INT4 i4Fsipv6SENDSentPktType,
                                      UINT4
                                      *pu4RetValFsipv6SENDSentTimeStampOptPkts)
{
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (i4Fsipv6SENDSentPktType);
    UNUSED_PARAM (pu4RetValFsipv6SENDSentTimeStampOptPkts);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Fsipv6SENDRcvPktStatsTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsipv6SENDRcvPktStatsTable
 Input       :  The Indices
                Fsipv6IfIndex
                Fsipv6SENDRcvPktType
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsipv6SENDRcvPktStatsTable (INT4 i4Fsipv6IfIndex,
                                                    INT4 i4Fsipv6SENDRcvPktType)
{
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (i4Fsipv6SENDRcvPktType);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsipv6SENDRcvPktStatsTable
 Input       :  The Indices
                Fsipv6IfIndex
                Fsipv6SENDRcvPktType
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFirstIndexFsipv6SENDRcvPktStatsTable (INT4 *pi4Fsipv6IfIndex,
                                            INT4 *pi4Fsipv6SENDRcvPktType)
{
    UNUSED_PARAM (pi4Fsipv6IfIndex);
    UNUSED_PARAM (pi4Fsipv6SENDRcvPktType);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsipv6SENDRcvPktStatsTable
 Input       :  The Indices
                Fsipv6IfIndex
                nextFsipv6IfIndex
                Fsipv6SENDRcvPktType
                nextFsipv6SENDRcvPktType
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsipv6SENDRcvPktStatsTable (INT4 i4Fsipv6IfIndex,
                                           INT4 *pi4NextFsipv6IfIndex,
                                           INT4 i4Fsipv6SENDRcvPktType,
                                           INT4 *pi4NextFsipv6SENDRcvPktType)
{
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (i4Fsipv6SENDRcvPktType);
    UNUSED_PARAM (pi4NextFsipv6IfIndex);
    UNUSED_PARAM (pi4NextFsipv6SENDRcvPktType);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6SENDRcvCgaOptPkts
 Input       :  The Indices
                Fsipv6IfIndex
                Fsipv6SENDRcvPktType

                The Object
                retValFsipv6SENDRcvCgaOptPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SENDRcvCgaOptPkts (INT4 i4Fsipv6IfIndex,
                               INT4 i4Fsipv6SENDRcvPktType,
                               UINT4 *pu4RetValFsipv6SENDRcvCgaOptPkts)
{
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (i4Fsipv6SENDRcvPktType);
    UNUSED_PARAM (pu4RetValFsipv6SENDRcvCgaOptPkts);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6SENDRcvCertOptPkts
 Input       :  The Indices
                Fsipv6IfIndex
                Fsipv6SENDRcvPktType

                The Object
                retValFsipv6SENDRcvCertOptPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SENDRcvCertOptPkts (INT4 i4Fsipv6IfIndex,
                                INT4 i4Fsipv6SENDRcvPktType,
                                UINT4 *pu4RetValFsipv6SENDRcvCertOptPkts)
{
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (i4Fsipv6SENDRcvPktType);
    UNUSED_PARAM (pu4RetValFsipv6SENDRcvCertOptPkts);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6SENDRcvMtuOptPkts
 Input       :  The Indices
                Fsipv6IfIndex
                Fsipv6SENDRcvPktType

                The Object
                retValFsipv6SENDRcvMtuOptPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SENDRcvMtuOptPkts (INT4 i4Fsipv6IfIndex,
                               INT4 i4Fsipv6SENDRcvPktType,
                               UINT4 *pu4RetValFsipv6SENDRcvMtuOptPkts)
{
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (i4Fsipv6SENDRcvPktType);
    UNUSED_PARAM (pu4RetValFsipv6SENDRcvMtuOptPkts);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6SENDRcvNonceOptPkts
 Input       :  The Indices
                Fsipv6IfIndex
                Fsipv6SENDRcvPktType

                The Object
                retValFsipv6SENDRcvNonceOptPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SENDRcvNonceOptPkts (INT4 i4Fsipv6IfIndex,
                                 INT4 i4Fsipv6SENDRcvPktType,
                                 UINT4 *pu4RetValFsipv6SENDRcvNonceOptPkts)
{
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (i4Fsipv6SENDRcvPktType);
    UNUSED_PARAM (pu4RetValFsipv6SENDRcvNonceOptPkts);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6SENDRcvPrefixOptPkts
 Input       :  The Indices
                Fsipv6IfIndex
                Fsipv6SENDRcvPktType

                The Object
                retValFsipv6SENDRcvPrefixOptPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SENDRcvPrefixOptPkts (INT4 i4Fsipv6IfIndex,
                                  INT4 i4Fsipv6SENDRcvPktType,
                                  UINT4 *pu4RetValFsipv6SENDRcvPrefixOptPkts)
{
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (i4Fsipv6SENDRcvPktType);
    UNUSED_PARAM (pu4RetValFsipv6SENDRcvPrefixOptPkts);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6SENDRcvRedirHdrPkts
 Input       :  The Indices
                Fsipv6IfIndex
                Fsipv6SENDRcvPktType

                The Object
                retValFsipv6SENDRcvRedirHdrPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SENDRcvRedirHdrPkts (INT4 i4Fsipv6IfIndex,
                                 INT4 i4Fsipv6SENDRcvPktType,
                                 UINT4 *pu4RetValFsipv6SENDRcvRedirHdrPkts)
{
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (i4Fsipv6SENDRcvPktType);
    UNUSED_PARAM (pu4RetValFsipv6SENDRcvRedirHdrPkts);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6SENDRcvRsaOptPkts
 Input       :  The Indices
                Fsipv6IfIndex
                Fsipv6SENDRcvPktType

                The Object
                retValFsipv6SENDRcvRsaOptPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SENDRcvRsaOptPkts (INT4 i4Fsipv6IfIndex,
                               INT4 i4Fsipv6SENDRcvPktType,
                               UINT4 *pu4RetValFsipv6SENDRcvRsaOptPkts)
{
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (i4Fsipv6SENDRcvPktType);
    UNUSED_PARAM (pu4RetValFsipv6SENDRcvRsaOptPkts);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6SENDRcvSrcLinkAddrPkts
 Input       :  The Indices
                Fsipv6IfIndex
                Fsipv6SENDRcvPktType

                The Object
                retValFsipv6SENDRcvSrcLinkAddrPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SENDRcvSrcLinkAddrPkts (INT4 i4Fsipv6IfIndex,
                                    INT4 i4Fsipv6SENDRcvPktType,
                                    UINT4
                                    *pu4RetValFsipv6SENDRcvSrcLinkAddrPkts)
{
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (i4Fsipv6SENDRcvPktType);
    UNUSED_PARAM (pu4RetValFsipv6SENDRcvSrcLinkAddrPkts);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6SENDRcvTgtLinkAddrPkts
 Input       :  The Indices
                Fsipv6IfIndex
                Fsipv6SENDRcvPktType

                The Object
                retValFsipv6SENDRcvTgtLinkAddrPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SENDRcvTgtLinkAddrPkts (INT4 i4Fsipv6IfIndex,
                                    INT4 i4Fsipv6SENDRcvPktType,
                                    UINT4
                                    *pu4RetValFsipv6SENDRcvTgtLinkAddrPkts)
{
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (i4Fsipv6SENDRcvPktType);
    UNUSED_PARAM (pu4RetValFsipv6SENDRcvTgtLinkAddrPkts);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6SENDRcvTaOptPkts
 Input       :  The Indices
                Fsipv6IfIndex
                Fsipv6SENDRcvPktType

                The Object
                retValFsipv6SENDRcvTaOptPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SENDRcvTaOptPkts (INT4 i4Fsipv6IfIndex, INT4 i4Fsipv6SENDRcvPktType,
                              UINT4 *pu4RetValFsipv6SENDRcvTaOptPkts)
{
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (i4Fsipv6SENDRcvPktType);
    UNUSED_PARAM (pu4RetValFsipv6SENDRcvTaOptPkts);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6SENDRcvTimeStampOptPkts
 Input       :  The Indices
                Fsipv6IfIndex
                Fsipv6SENDRcvPktType

                The Object
                retValFsipv6SENDRcvTimeStampOptPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SENDRcvTimeStampOptPkts (INT4 i4Fsipv6IfIndex,
                                     INT4 i4Fsipv6SENDRcvPktType,
                                     UINT4
                                     *pu4RetValFsipv6SENDRcvTimeStampOptPkts)
{
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (i4Fsipv6SENDRcvPktType);
    UNUSED_PARAM (pu4RetValFsipv6SENDRcvTimeStampOptPkts);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Fsipv6SENDDrpPktStatsTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsipv6SENDDrpPktStatsTable
 Input       :  The Indices
                Fsipv6IfIndex
                Fsipv6SENDDrpPktType
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsipv6SENDDrpPktStatsTable (INT4 i4Fsipv6IfIndex,
                                                    INT4 i4Fsipv6SENDDrpPktType)
{
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (i4Fsipv6SENDDrpPktType);
    UNUSED_PARAM (i4Fsipv6SENDDrpPktType);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsipv6SENDDrpPktStatsTable
 Input       :  The Indices
                Fsipv6IfIndex
                Fsipv6SENDDrpPktType
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsipv6SENDDrpPktStatsTable (INT4 *pi4Fsipv6IfIndex,
                                            INT4 *pi4Fsipv6SENDDrpPktType)
{
    UNUSED_PARAM (pi4Fsipv6IfIndex);
    UNUSED_PARAM (pi4Fsipv6SENDDrpPktType);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsipv6SENDDrpPktStatsTable
 Input       :  The Indices
                Fsipv6IfIndex
                nextFsipv6IfIndex
                Fsipv6SENDDrpPktType
                nextFsipv6SENDDrpPktType
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsipv6SENDDrpPktStatsTable (INT4 i4Fsipv6IfIndex,
                                           INT4 *pi4NextFsipv6IfIndex,
                                           INT4 i4Fsipv6SENDDrpPktType,
                                           INT4 *pi4NextFsipv6SENDDrpPktType)
{
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (pi4NextFsipv6IfIndex);
    UNUSED_PARAM (i4Fsipv6SENDDrpPktType);
    UNUSED_PARAM (pi4NextFsipv6SENDDrpPktType);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsipv6SENDDrpCgaOptPkts
 Input       :  The Indices
                Fsipv6IfIndex
                Fsipv6SENDDrpPktType

                The Object
                retValFsipv6SENDDrpCgaOptPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SENDDrpCgaOptPkts (INT4 i4Fsipv6IfIndex,
                               INT4 i4Fsipv6SENDDrpPktType,
                               UINT4 *pu4RetValFsipv6SENDDrpCgaOptPkts)
{
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (i4Fsipv6SENDDrpPktType);
    UNUSED_PARAM (pu4RetValFsipv6SENDDrpCgaOptPkts);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6SENDDrpCertOptPkts
 Input       :  The Indices
                Fsipv6IfIndex
                Fsipv6SENDDrpPktType

                The Object
                retValFsipv6SENDDrpCertOptPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SENDDrpCertOptPkts (INT4 i4Fsipv6IfIndex,
                                INT4 i4Fsipv6SENDDrpPktType,
                                UINT4 *pu4RetValFsipv6SENDDrpCertOptPkts)
{
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (i4Fsipv6SENDDrpPktType);
    UNUSED_PARAM (pu4RetValFsipv6SENDDrpCertOptPkts);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6SENDDrpMtuOptPkts
 Input       :  The Indices
                Fsipv6IfIndex
                Fsipv6SENDDrpPktType

                The Object
                retValFsipv6SENDDrpMtuOptPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SENDDrpMtuOptPkts (INT4 i4Fsipv6IfIndex,
                               INT4 i4Fsipv6SENDDrpPktType,
                               UINT4 *pu4RetValFsipv6SENDDrpMtuOptPkts)
{
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (i4Fsipv6SENDDrpPktType);
    UNUSED_PARAM (pu4RetValFsipv6SENDDrpMtuOptPkts);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6SENDDrpNonceOptPkts
 Input       :  The Indices
                Fsipv6IfIndex
                Fsipv6SENDDrpPktType

                The Object
                retValFsipv6SENDDrpNonceOptPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SENDDrpNonceOptPkts (INT4 i4Fsipv6IfIndex,
                                 INT4 i4Fsipv6SENDDrpPktType,
                                 UINT4 *pu4RetValFsipv6SENDDrpNonceOptPkts)
{
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (i4Fsipv6SENDDrpPktType);
    UNUSED_PARAM (pu4RetValFsipv6SENDDrpNonceOptPkts);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6SENDDrpPrefixOptPkts
 Input       :  The Indices
                Fsipv6IfIndex
                Fsipv6SENDDrpPktType

                The Object
                retValFsipv6SENDDrpPrefixOptPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SENDDrpPrefixOptPkts (INT4 i4Fsipv6IfIndex,
                                  INT4 i4Fsipv6SENDDrpPktType,
                                  UINT4 *pu4RetValFsipv6SENDDrpPrefixOptPkts)
{
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (i4Fsipv6SENDDrpPktType);
    UNUSED_PARAM (pu4RetValFsipv6SENDDrpPrefixOptPkts);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6SENDDrpRedirHdrPkts
 Input       :  The Indices
                Fsipv6IfIndex
                Fsipv6SENDDrpPktType

                The Object
                retValFsipv6SENDDrpRedirHdrPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SENDDrpRedirHdrPkts (INT4 i4Fsipv6IfIndex,
                                 INT4 i4Fsipv6SENDDrpPktType,
                                 UINT4 *pu4RetValFsipv6SENDDrpRedirHdrPkts)
{
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (i4Fsipv6SENDDrpPktType);
    UNUSED_PARAM (pu4RetValFsipv6SENDDrpRedirHdrPkts);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6SENDDrpRsaOptPkts
 Input       :  The Indices
                Fsipv6IfIndex
                Fsipv6SENDDrpPktType

                The Object
                retValFsipv6SENDDrpRsaOptPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SENDDrpRsaOptPkts (INT4 i4Fsipv6IfIndex,
                               INT4 i4Fsipv6SENDDrpPktType,
                               UINT4 *pu4RetValFsipv6SENDDrpRsaOptPkts)
{
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (i4Fsipv6SENDDrpPktType);
    UNUSED_PARAM (pu4RetValFsipv6SENDDrpRsaOptPkts);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6SENDDrpSrcLinkAddrPkts
 Input       :  The Indices
                Fsipv6IfIndex
                Fsipv6SENDDrpPktType

                The Object
                retValFsipv6SENDDrpSrcLinkAddrPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SENDDrpSrcLinkAddrPkts (INT4 i4Fsipv6IfIndex,
                                    INT4 i4Fsipv6SENDDrpPktType,
                                    UINT4
                                    *pu4RetValFsipv6SENDDrpSrcLinkAddrPkts)
{
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (i4Fsipv6SENDDrpPktType);
    UNUSED_PARAM (pu4RetValFsipv6SENDDrpSrcLinkAddrPkts);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6SENDDrpTgtLinkAddrPkts
 Input       :  The Indices
                Fsipv6IfIndex
                Fsipv6SENDDrpPktType

                The Object
                retValFsipv6SENDDrpTgtLinkAddrPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SENDDrpTgtLinkAddrPkts (INT4 i4Fsipv6IfIndex,
                                    INT4 i4Fsipv6SENDDrpPktType,
                                    UINT4
                                    *pu4RetValFsipv6SENDDrpTgtLinkAddrPkts)
{
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (i4Fsipv6SENDDrpPktType);
    UNUSED_PARAM (pu4RetValFsipv6SENDDrpTgtLinkAddrPkts);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6SENDDrpTaOptPkts
 Input       :  The Indices
                Fsipv6IfIndex
                Fsipv6SENDDrpPktType

                The Object
                retValFsipv6SENDDrpTaOptPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SENDDrpTaOptPkts (INT4 i4Fsipv6IfIndex, INT4 i4Fsipv6SENDDrpPktType,
                              UINT4 *pu4RetValFsipv6SENDDrpTaOptPkts)
{
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (i4Fsipv6SENDDrpPktType);
    UNUSED_PARAM (pu4RetValFsipv6SENDDrpTaOptPkts);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6SENDDrpTimeStampOptPkts
 Input       :  The Indices
                Fsipv6IfIndex
                Fsipv6SENDDrpPktType

                The Object
                retValFsipv6SENDDrpTimeStampOptPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SENDDrpTimeStampOptPkts (INT4 i4Fsipv6IfIndex,
                                     INT4 i4Fsipv6SENDDrpPktType,
                                     UINT4
                                     *pu4RetValFsipv6SENDDrpTimeStampOptPkts)
{
    UNUSED_PARAM (i4Fsipv6IfIndex);
    UNUSED_PARAM (i4Fsipv6SENDDrpPktType);
    UNUSED_PARAM (pu4RetValFsipv6SENDDrpTimeStampOptPkts);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Fsipv6RARouteInfoTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsipv6RARouteInfoTable
 Input       :  The Indices
                Fsipv6RARouteIfIndex
                Fsipv6RARoutePrefix
                Fsipv6RARoutePrefixLen
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsipv6RARouteInfoTable (INT4 i4Fsipv6RARouteIfIndex,
                                                tSNMP_OCTET_STRING_TYPE *
                                                pFsipv6RARoutePrefix,
                                                INT4 i4Fsipv6RARoutePrefixLen)
{
    UNUSED_PARAM (i4Fsipv6RARouteIfIndex);
    UNUSED_PARAM (pFsipv6RARoutePrefix);
    UNUSED_PARAM (i4Fsipv6RARoutePrefixLen);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsipv6RARouteInfoTable
 Input       :  The Indices
                Fsipv6RARouteIfIndex
                Fsipv6RARoutePrefix
                Fsipv6RARoutePrefixLen
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsipv6RARouteInfoTable (INT4 *pi4Fsipv6RARouteIfIndex,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pFsipv6RARoutePrefix,
                                        INT4 *pi4Fsipv6RARoutePrefixLen)
{
    UNUSED_PARAM (pi4Fsipv6RARouteIfIndex);
    UNUSED_PARAM (pFsipv6RARoutePrefix);
    UNUSED_PARAM (pi4Fsipv6RARoutePrefixLen);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsipv6RARouteInfoTable
 Input       :  The Indices
                Fsipv6RARouteIfIndex
                nextFsipv6RARouteIfIndex
                Fsipv6RARoutePrefix
                nextFsipv6RARoutePrefix
                Fsipv6RARoutePrefixLen
                nextFsipv6RARoutePrefixLen
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsipv6RARouteInfoTable (INT4 i4Fsipv6RARouteIfIndex,
                                       INT4 *pi4NextFsipv6RARouteIfIndex,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFsipv6RARoutePrefix,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pNextFsipv6RARoutePrefix,
                                       INT4 i4Fsipv6RARoutePrefixLen,
                                       INT4 *pi4NextFsipv6RARoutePrefixLen)
{
    UNUSED_PARAM (i4Fsipv6RARouteIfIndex);
    UNUSED_PARAM (pi4NextFsipv6RARouteIfIndex);
    UNUSED_PARAM (pFsipv6RARoutePrefix);
    UNUSED_PARAM (pNextFsipv6RARoutePrefix);
    UNUSED_PARAM (i4Fsipv6RARoutePrefixLen);
    UNUSED_PARAM (pi4NextFsipv6RARoutePrefixLen);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetFsipv6RARoutePreference
 Input       :  The Indices
                Fsipv6RARouteIfIndex
                Fsipv6RARoutePrefix
                Fsipv6RARoutePrefixLen

                The Object 
                retValFsipv6RARoutePreference
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6RARoutePreference (INT4 i4Fsipv6RARouteIfIndex,
                               tSNMP_OCTET_STRING_TYPE * pFsipv6RARoutePrefix,
                               INT4 i4Fsipv6RARoutePrefixLen,
                               INT4 *pi4RetValFsipv6RARoutePreference)
{
    UNUSED_PARAM (i4Fsipv6RARouteIfIndex);
    UNUSED_PARAM (pFsipv6RARoutePrefix);
    UNUSED_PARAM (i4Fsipv6RARoutePrefixLen);
    UNUSED_PARAM (pi4RetValFsipv6RARoutePreference);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6RARouteLifetime
 Input       :  The Indices
                Fsipv6RARouteIfIndex
                Fsipv6RARoutePrefix
                Fsipv6RARoutePrefixLen

                The Object 
                retValFsipv6RARouteLifetime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6RARouteLifetime (INT4 i4Fsipv6RARouteIfIndex,
                             tSNMP_OCTET_STRING_TYPE * pFsipv6RARoutePrefix,
                             INT4 i4Fsipv6RARoutePrefixLen,
                             UINT4 *pu4RetValFsipv6RARouteLifetime)
{
    UNUSED_PARAM (i4Fsipv6RARouteIfIndex);
    UNUSED_PARAM (pFsipv6RARoutePrefix);
    UNUSED_PARAM (i4Fsipv6RARoutePrefixLen);
    UNUSED_PARAM (pu4RetValFsipv6RARouteLifetime);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6RARouteRowStatus
 Input       :  The Indices
                Fsipv6RARouteIfIndex
                Fsipv6RARoutePrefix
                Fsipv6RARoutePrefixLen

                The Object 
                retValFsipv6RARouteRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6RARouteRowStatus (INT4 i4Fsipv6RARouteIfIndex,
                              tSNMP_OCTET_STRING_TYPE * pFsipv6RARoutePrefix,
                              INT4 i4Fsipv6RARoutePrefixLen,
                              INT4 *pi4RetValFsipv6RARouteRowStatus)
{
    UNUSED_PARAM (i4Fsipv6RARouteIfIndex);
    UNUSED_PARAM (pFsipv6RARoutePrefix);
    UNUSED_PARAM (i4Fsipv6RARoutePrefixLen);
    UNUSED_PARAM (pi4RetValFsipv6RARouteRowStatus);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsipv6RARoutePreference
 Input       :  The Indices
                Fsipv6RARouteIfIndex
                Fsipv6RARoutePrefix
                Fsipv6RARoutePrefixLen

                The Object 
                setValFsipv6RARoutePreference
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6RARoutePreference (INT4 i4Fsipv6RARouteIfIndex,
                               tSNMP_OCTET_STRING_TYPE * pFsipv6RARoutePrefix,
                               INT4 i4Fsipv6RARoutePrefixLen,
                               INT4 i4SetValFsipv6RARoutePreference)
{
    UNUSED_PARAM (i4Fsipv6RARouteIfIndex);
    UNUSED_PARAM (pFsipv6RARoutePrefix);
    UNUSED_PARAM (i4Fsipv6RARoutePrefixLen);
    UNUSED_PARAM (i4SetValFsipv6RARoutePreference);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6RARouteLifetime
 Input       :  The Indices
                Fsipv6RARouteIfIndex
                Fsipv6RARoutePrefix
                Fsipv6RARoutePrefixLen

                The Object 
                setValFsipv6RARouteLifetime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6RARouteLifetime (INT4 i4Fsipv6RARouteIfIndex,
                             tSNMP_OCTET_STRING_TYPE * pFsipv6RARoutePrefix,
                             INT4 i4Fsipv6RARoutePrefixLen,
                             UINT4 u4SetValFsipv6RARouteLifetime)
{
    UNUSED_PARAM (i4Fsipv6RARouteIfIndex);
    UNUSED_PARAM (pFsipv6RARoutePrefix);
    UNUSED_PARAM (i4Fsipv6RARoutePrefixLen);
    UNUSED_PARAM (u4SetValFsipv6RARouteLifetime);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6RARouteRowStatus
 Input       :  The Indices
                Fsipv6RARouteIfIndex
                Fsipv6RARoutePrefix
                Fsipv6RARoutePrefixLen

                The Object 
                setValFsipv6RARouteRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6RARouteRowStatus (INT4 i4Fsipv6RARouteIfIndex,
                              tSNMP_OCTET_STRING_TYPE * pFsipv6RARoutePrefix,
                              INT4 i4Fsipv6RARoutePrefixLen,
                              INT4 i4SetValFsipv6RARouteRowStatus)
{
    UNUSED_PARAM (i4Fsipv6RARouteIfIndex);
    UNUSED_PARAM (pFsipv6RARoutePrefix);
    UNUSED_PARAM (i4Fsipv6RARoutePrefixLen);
    UNUSED_PARAM (i4SetValFsipv6RARouteRowStatus);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */
/****************************************************************************
 Function    :  nmhTestv2Fsipv6RARoutePreference
 Input       :  The Indices
                Fsipv6RARouteIfIndex
                Fsipv6RARoutePrefix
                Fsipv6RARoutePrefixLen

                The Object 
                testValFsipv6RARoutePreference
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6RARoutePreference (UINT4 *pu4ErrorCode,
                                  INT4 i4Fsipv6RARouteIfIndex,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pFsipv6RARoutePrefix,
                                  INT4 i4Fsipv6RARoutePrefixLen,
                                  INT4 i4TestValFsipv6RARoutePreference)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4Fsipv6RARouteIfIndex);
    UNUSED_PARAM (pFsipv6RARoutePrefix);
    UNUSED_PARAM (i4Fsipv6RARoutePrefixLen);
    UNUSED_PARAM (i4TestValFsipv6RARoutePreference);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6RARouteLifetime
 Input       :  The Indices
                Fsipv6RARouteIfIndex
                Fsipv6RARoutePrefix
                Fsipv6RARoutePrefixLen

                The Object 
                testValFsipv6RARouteLifetime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6RARouteLifetime (UINT4 *pu4ErrorCode,
                                INT4 i4Fsipv6RARouteIfIndex,
                                tSNMP_OCTET_STRING_TYPE * pFsipv6RARoutePrefix,
                                INT4 i4Fsipv6RARoutePrefixLen,
                                UINT4 u4TestValFsipv6RARouteLifetime)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4Fsipv6RARouteIfIndex);
    UNUSED_PARAM (pFsipv6RARoutePrefix);
    UNUSED_PARAM (i4Fsipv6RARoutePrefixLen);
    UNUSED_PARAM (u4TestValFsipv6RARouteLifetime);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6RARouteRowStatus
 Input       :  The Indices
                Fsipv6RARouteIfIndex
                Fsipv6RARoutePrefix
                Fsipv6RARoutePrefixLen

                The Object 
                testValFsipv6RARouteRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6RARouteRowStatus (UINT4 *pu4ErrorCode,
                                 INT4 i4Fsipv6RARouteIfIndex,
                                 tSNMP_OCTET_STRING_TYPE * pFsipv6RARoutePrefix,
                                 INT4 i4Fsipv6RARoutePrefixLen,
                                 INT4 i4TestValFsipv6RARouteRowStatus)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4Fsipv6RARouteIfIndex);
    UNUSED_PARAM (pFsipv6RARoutePrefix);
    UNUSED_PARAM (i4Fsipv6RARoutePrefixLen);
    UNUSED_PARAM (i4TestValFsipv6RARouteRowStatus);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Fsipv6RARouteInfoTable
 Input       :  The Indices
                Fsipv6RARouteIfIndex
                Fsipv6RARoutePrefix
                Fsipv6RARoutePrefixLen
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsipv6RARouteInfoTable (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Fsipv6ECMPPRTTimer
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsipv6ECMPPRTTimer (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Fsipv6NdCacheTimeout
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsipv6NdCacheTimeout (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/*********************************************************************
 *** *   Function Name : Ip6EnableInvalidRoute
 *** *   Description   : When next Hop becomes reachable, static routes
 *                       are moved from Invld route list to TRIE.
 *** *   Input(s)      : pNetIp6RtInfo - Static route info
 *** *   Output(s)     : None.
 *** *   Return Values : SNMP_SUCCESS / SNMP_FAILURE
 **********************************************************************/

INT1
Ip6EnableInvalidRoute (tNetIpv6RtInfo * pNetIp6RtInfo)
{
    tSNMP_OCTET_STRING_TYPE RtDest;
    tSNMP_OCTET_STRING_TYPE RtNxtHp;
    UINT1               au1RtDestOctetList[IP6_ADDR_SIZE];
    UINT1               au1RtNxtHpOctetList[IP6_ADDR_SIZE];

    MEMSET (au1RtDestOctetList, IP6_ZERO, IP6_ADDR_SIZE);
    MEMSET (au1RtNxtHpOctetList, IP6_ZERO, IP6_ADDR_SIZE);

    RtDest.pu1_OctetList = au1RtDestOctetList;
    RtNxtHp.pu1_OctetList = au1RtNxtHpOctetList;

    MEMCPY (RtDest.pu1_OctetList, &(pNetIp6RtInfo->Ip6Dst), IP6_ADDR_SIZE);
    RtDest.i4_Length = IP6_ADDR_SIZE;
    MEMCPY (RtNxtHp.pu1_OctetList, &(pNetIp6RtInfo->NextHop), IP6_ADDR_SIZE);
    RtNxtHp.i4_Length = IP6_ADDR_SIZE;

    if (nmhSetFsipv6RouteAdminStatus
        (&RtDest, (INT4) pNetIp6RtInfo->u1Prefixlen, IP6_NETMGMT_PROTOID,
         &RtNxtHp, IP6FWD_ACTIVE) == SNMP_FAILURE)
    {
        nmhSetFsipv6RouteAdminStatus (&RtDest,
                                      (INT4) pNetIp6RtInfo->u1Prefixlen,
                                      IP6_NETMGMT_PROTOID, &RtNxtHp,
                                      IP6FWD_DESTROY);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}
