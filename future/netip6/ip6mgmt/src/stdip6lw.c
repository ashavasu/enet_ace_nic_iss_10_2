
/*******************************************************************
 *  Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *   
 *   $Id: stdip6lw.c,v 1.19 2017/12/26 13:34:22 siva Exp $
 *    
 * ****************************************************************/

#include "ip6inc.h"
#include "ip6cli.h"
#include "fsmsipcli.h"
#include "ipv6.h"

extern unsigned int EoidGetEnterpriseOid (void);
extern UINT4        FsMIIpv6IfToken[13];

/* LOW LEVEL Routines for Table : Ipv6IfTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIpv6IfTable
 Input       :  The Indices
                Ipv6IfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhValidateIndexInstanceIpv6IfTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhValidateIndexInstanceIpv6IfTable (INT4 i4Ipv6IfIndex)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6IfIndex = %d\n", i4Ipv6IfIndex); ***/
    if (Ip6ifEntryExists ((UINT4) i4Ipv6IfIndex) == IP6_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIpv6IfTable
 Input       :  The Indices
                Ipv6IfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhGetFirstIpv6IfTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFirstIndexIpv6IfTable (INT4 *pi4Ipv6IfIndex)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6IfIndex = %d\n", *pi4Ipv6IfIndex); ***/
    *pi4Ipv6IfIndex = 0;

    if (Ip6ifEntryExists ((FS_ULONG) pi4Ipv6IfIndex) == IP6_SUCCESS)
    {
        return (SNMP_SUCCESS);
    }

    if (Ip6ifGetNextIndex ((UINT4 *) pi4Ipv6IfIndex) == IP6_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetNextIndexIpv6IfTable
 Input       :  The Indices
                Ipv6IfIndex
                nextIpv6IfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
 /*** $$TRACE_PROCEDURE_NAME = nmhGetNextIpv6IfTable ***/
 /*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetNextIndexIpv6IfTable (INT4 i4Ipv6IfIndex, INT4 *pi4NextIpv6IfIndex)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6IfIndex = %d\n", *pi4Ipv6IfIndex); ***/

    if (Ip6ifGetNextIndex ((UINT4 *) &i4Ipv6IfIndex) == IP6_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4NextIpv6IfIndex = i4Ipv6IfIndex;
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIpv6IfDescr
 Input       :  The Indices
                Ipv6IfIndex

                The Object 
                retValIpv6IfDescr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpv6IfDescr ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetIpv6IfDescr (INT4 i4Ipv6IfIndex,
                   tSNMP_OCTET_STRING_TYPE * pRetValIpv6IfDescr)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6IfIndex = %d\n", i4Ipv6IfIndex); ***/
    tIp6If             *pIf6 = Ip6ifGetEntry (i4Ipv6IfIndex);

    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    STRNCPY (pRetValIpv6IfDescr->pu1_OctetList,
             (pIf6->u1Descr), sizeof (pIf6->u1Descr));
    pRetValIpv6IfDescr->i4_Length = sizeof (pIf6->u1Descr);
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetIpv6IfLowerLayer
 Input       :  The Indices
                Ipv6IfIndex

                The Object 
                retValIpv6IfLowerLayer
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpv6IfLowerLayer ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetIpv6IfLowerLayer (INT4 i4Ipv6IfIndex,
                        tSNMP_OID_TYPE * pRetValIpv6IfLowerLayer)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6IfIndex = %d\n", i4Ipv6IfIndex); ***/
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT2) i4Ipv6IfIndex);
    UINT4               au4EthOid[10] = { 1, 3, 6, 1, 2, 1, 1, 2, 2 };
#ifdef TUNNEL_WANTED
    tSNMP_OID_TYPE      CTunlPrefixOid;
    tSNMP_OID_TYPE      CTunlSuffixOid;
    tSNMP_OID_TYPE      CTunlOid;
    UINT4               au4CTunlPrefixOid[] = { 1, 3, 6, 1, 4, 1 };
    UINT4               au4CTunlSuffixOid[] = { 28, 5, 2, 0 };
    UINT4               au4CTunlOid[SNMP_MAX_OID_LENGTH];
    UINT4               au4ATunlOid[10] = { 1, 3, 6, 1, 2, 1, 55, 1, 5 };
#endif /** TUNNEL_WANTED **/

    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    if ((pIf6->u1IfType == IP6_ENET_INTERFACE_TYPE) ||
#ifdef WGS_WANTED
        (pIf6->u1IfType == IP6_L2VLAN_INTERFACE_TYPE) ||
#endif
        (pIf6->u1IfType == IP6_L3VLAN_INTERFACE_TYPE) ||
        (pIf6->u1IfType == IP6_LAGG_INTERFACE_TYPE) ||
        (pIf6->u1IfType == IP6_PSEUDO_WIRE_INTERFACE_TYPE) ||
        (pIf6->u1IfType == IP6_L3SUB_INTF_TYPE))
    {
        au4EthOid[9] = pIf6->u4Index;
        MEMCPY (pRetValIpv6IfLowerLayer->pu4_OidList, au4EthOid,
                10 * sizeof (UINT4));
        pRetValIpv6IfLowerLayer->u4_Length = 10;
        return SNMP_SUCCESS;
    }
#ifdef TUNNEL_WANTED
    else if (pIf6->u1IfType == IP6_TUNNEL_INTERFACE_TYPE)
    {
        CTunlPrefixOid.pu4_OidList = au4CTunlPrefixOid;
        CTunlPrefixOid.u4_Length = sizeof (au4CTunlPrefixOid) / sizeof (UINT4);

        CTunlSuffixOid.pu4_OidList = au4CTunlSuffixOid;
        CTunlSuffixOid.u4_Length = sizeof (au4CTunlSuffixOid) / sizeof (UINT4);

        MEMSET (au4CTunlOid, 0, sizeof (au4CTunlOid));

        CTunlOid.pu4_OidList = au4CTunlOid;
        CTunlOid.u4_Length = 0;

        if (SNMPAddEnterpriseOid (&CTunlPrefixOid, &CTunlSuffixOid,
                                  &CTunlOid) == OSIX_FAILURE)
        {
            return SNMP_FAILURE;
        }

        if ((pIf6->pTunlIf->u1TunlType == IPV6_AUTO_COMPAT) ||
            (pIf6->pTunlIf->u1TunlType == IPV6_SIX_TO_FOUR) ||
            (pIf6->pTunlIf->u1TunlType == IPV6_ISATAP_TUNNEL))
        {
            /* Automatic Tunnels */
            au4ATunlOid[9] = 0;    /* Automatic tunnel interface */
            MEMCPY (pRetValIpv6IfLowerLayer->pu4_OidList, au4ATunlOid,
                    10 * sizeof (UINT4));
            pRetValIpv6IfLowerLayer->u4_Length = 10;
            return SNMP_SUCCESS;
        }
        else
        {
            /* Configured Tunnels */
            if ((CTunlOid.u4_Length > 0)
                && (CTunlOid.u4_Length < SNMP_MAX_OID_LENGTH))
            {
                au4CTunlOid[CTunlOid.u4_Length - 1] = pIf6->u4Index;    /* Configured tunnel interface */
                MEMCPY (pRetValIpv6IfLowerLayer->pu4_OidList,
                        CTunlOid.pu4_OidList,
                        CTunlOid.u4_Length * sizeof (UINT4));
                pRetValIpv6IfLowerLayer->u4_Length = CTunlOid.u4_Length;
                return SNMP_SUCCESS;
            }
            else
            {
                return SNMP_FAILURE;
            }
        }
    }
#endif /** TUNNEL_WANTED **/

    return SNMP_FAILURE;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetIpv6IfEffectiveMtu
 Input       :  The Indices
                Ipv6IfIndex

                The Object 
                retValIpv6IfEffectiveMtu
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpv6IfEffectiveMtu ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetIpv6IfEffectiveMtu (INT4 i4Ipv6IfIndex,
                          UINT4 *pu4RetValIpv6IfEffectiveMtu)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6IfIndex = %d\n", i4Ipv6IfIndex); ***/
    tIp6If             *pIf6 = Ip6ifGetEntry (i4Ipv6IfIndex);

    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValIpv6IfEffectiveMtu = Ip6ifGetMtu (pIf6);
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetIpv6IfReasmMaxSize
 Input       :  The Indices
                Ipv6IfIndex

                The Object 
                retValIpv6IfReasmMaxSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpv6IfReasmMaxSize ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetIpv6IfReasmMaxSize (INT4 i4Ipv6IfIndex,
                          UINT4 *pu4RetValIpv6IfReasmMaxSize)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6IfIndex = %d\n", i4Ipv6IfIndex); ***/
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT2) i4Ipv6IfIndex);

    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValIpv6IfReasmMaxSize = MAX_REASM_SIZE;
    return SNMP_SUCCESS;
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetIpv6IfIdentifier
 Input       :  The Indices
                Ipv6IfIndex

                The Object 
                retValIpv6IfIdentifier
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpv6IfIdentifier ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetIpv6IfIdentifier (INT4 i4Ipv6IfIndex,
                        tSNMP_OCTET_STRING_TYPE * pRetValIpv6IfIdentifier)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6IfIndex = %d\n", i4Ipv6IfIndex); ***/
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT2) i4Ipv6IfIndex);
    UINT1               u1tokLen = 0;
    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    u1tokLen = pIf6->u1TokLen;
    u1tokLen = MEM_MAX_BYTES (pIf6->u1TokLen, IP6_EUI_ADDRESS_LEN);
    if (u1tokLen != 0)
    {
        MEMCPY (pRetValIpv6IfIdentifier->pu1_OctetList, pIf6->ifaceTok,
                u1tokLen);
        pRetValIpv6IfIdentifier->i4_Length = u1tokLen;
    }

    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetIpv6IfIdentifierLength
 Input       :  The Indices
                Ipv6IfIndex

                The Object 
                retValIpv6IfIdentifierLength
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpv6IfIdentifierLength ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetIpv6IfIdentifierLength (INT4 i4Ipv6IfIndex,
                              INT4 *pi4RetValIpv6IfIdentifierLength)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6IfIndex = %d\n", i4Ipv6IfIndex); ***/
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT2) i4Ipv6IfIndex);
    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValIpv6IfIdentifierLength = (pIf6->u1TokLen) * BYTE_LENGTH;
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetIpv6IfPhysicalAddress
 Input       :  The Indices
                Ipv6IfIndex

                The Object 
                retValIpv6IfPhysicalAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpv6IfPhysicalAddress ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetIpv6IfPhysicalAddress (INT4 i4Ipv6IfIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pRetValIpv6IfPhysicalAddress)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6IfIndex = %d\n", i4Ipv6IfIndex); ***/
    tIp6If             *pIf6 = Ip6ifGetEntry (i4Ipv6IfIndex);
    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pIf6->u1IfType != IP6_TUNNEL_INTERFACE_TYPE)
    {
        if (Ip6GetHwAddr (pIf6->u4Index,
                          pRetValIpv6IfPhysicalAddress->pu1_OctetList) ==
            IP6_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    pRetValIpv6IfPhysicalAddress->i4_Length = MAC_ADDR_LEN;
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetIpv6IfAdminStatus
 Input       :  The Indices
                Ipv6IfIndex

                The Object 
                retValIpv6IfAdminStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpv6IfAdminStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetIpv6IfAdminStatus (INT4 i4Ipv6IfIndex, INT4 *pi4RetValIpv6IfAdminStatus)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6IfIndex = %d\n", i4Ipv6IfIndex); ***/
    return (nmhGetFsipv6IfAdminStatus (i4Ipv6IfIndex,
                                       pi4RetValIpv6IfAdminStatus));
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetIpv6IfOperStatus
 Input       :  The Indices
                Ipv6IfIndex

                The Object 
                retValIpv6IfOperStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpv6IfOperStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetIpv6IfOperStatus (INT4 i4Ipv6IfIndex, INT4 *pi4RetValIpv6IfOperStatus)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6IfIndex = %d\n", i4Ipv6IfIndex); ***/
    return (nmhGetFsipv6IfOperStatus (i4Ipv6IfIndex,
                                      pi4RetValIpv6IfOperStatus));
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetIpv6IfLastChange
 Input       :  The Indices
                Ipv6IfIndex

                The Object 
                retValIpv6IfLastChange
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpv6IfLastChange ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetIpv6IfLastChange (INT4 i4Ipv6IfIndex, UINT4 *pu4RetValIpv6IfLastChange)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6IfIndex = %d\n", i4Ipv6IfIndex); ***/
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT2) i4Ipv6IfIndex);

    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValIpv6IfLastChange = pIf6->u4IfLastUpdate;
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIpv6IfDescr
 Input       :  The Indices
                Ipv6IfIndex

                The Object 
                setValIpv6IfDescr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetIpv6IfDescr ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetIpv6IfDescr (INT4 i4Ipv6IfIndex,
                   tSNMP_OCTET_STRING_TYPE * pSetValIpv6IfDescr)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6IfIndex = %d\n", i4Ipv6IfIndex); ***/
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT2) i4Ipv6IfIndex);

    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    MEMSET (pIf6->u1Descr, 0, IP6_IF_DESCR_LEN);

    STRNCPY ((pIf6->u1Descr), pSetValIpv6IfDescr->pu1_OctetList,
             pSetValIpv6IfDescr->i4_Length);
    return SNMP_SUCCESS;
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhSetIpv6IfIdentifier
 Input       :  The Indices
                Ipv6IfIndex

                The Object 
                setValIpv6IfIdentifier
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetIpv6IfIdentifier ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetIpv6IfIdentifier (INT4 i4Ipv6IfIndex,
                        tSNMP_OCTET_STRING_TYPE * pSetValIpv6IfIdentifier)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6IfIndex = %d\n", i4Ipv6IfIndex); ***/
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT2) i4Ipv6IfIndex);
    tIp6LlocalInfo     *pLLAddrInfo = NULL;
    UINT1               au1Ip6IfId[IP6_EUI_ADDRESS_LEN];
    tIp6Addr            ip6Addr;

    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

#ifdef WGS_WANTED
    if (pIf6->u1IfType == IP6_L2VLAN_INTERFACE_TYPE)
    {
        return (SNMP_SUCCESS);
    }
#endif

    /*All zero interface identifier */
    MEMSET (au1Ip6IfId, 0, IP6_EUI_ADDRESS_LEN);
    MEMSET (&ip6Addr, 0, sizeof (tIp6Addr));

    ip6Addr.u1_addr[0] = ADDR6_LLOCAL_PREFIX_1;
    ip6Addr.u1_addr[1] = ADDR6_LLOCAL_PREFIX_2;

    /* Check if the given interface identifier is all zero */
    if (MEMCMP (pSetValIpv6IfIdentifier->pu1_OctetList,
                au1Ip6IfId, IP6_EUI_ADDRESS_LEN) == 0)
    {
        MEMCPY (ip6Addr.u1_addr + (sizeof (tIp6Addr) - (pIf6->u1TokLen)),
                pIf6->ifaceTok, pIf6->u1TokLen);

        if (Ip6AddrType (&ip6Addr) == ADDR6_LLOCAL)
        {
            if (Ip6LlAddrDelete ((UINT4) i4Ipv6IfIndex, &ip6Addr)
                == IP6_FAILURE)
            {
                return SNMP_FAILURE;
            }
        }

        if (pIf6->u1OperStatus == OPER_DOWN)
        {
            if (Ip6ifFormLanLla (pIf6) != IP6_SUCCESS)
            {
                /*Creation of linklocal address with default interface 
                 * MAC address failed*/
                return (SNMP_FAILURE);
            }
        }
    }
    else
    {
        pIf6->u1TokLen = (UINT1) pSetValIpv6IfIdentifier->i4_Length;

        MEMCPY (pIf6->ifaceTok, pSetValIpv6IfIdentifier->pu1_OctetList,
                pSetValIpv6IfIdentifier->i4_Length);

        MEMCPY (ip6Addr.u1_addr + (sizeof (tIp6Addr) - (pIf6->u1TokLen)),
                pIf6->ifaceTok, pIf6->u1TokLen);

        if ((pLLAddrInfo = Ip6LlAddrCreate ((UINT4) i4Ipv6IfIndex,
                                            &ip6Addr, ADMIN_DOWN,
                                            IP6_ADDR_STATIC)) != NULL)
        {
            if (Ip6AddrUp ((UINT4) i4Ipv6IfIndex, &ip6Addr,
                           IP6_ADDR_MAX_PREFIX, NULL) == IP6_FAILURE)
            {
                Ip6LlAddrDelete ((UINT4) i4Ipv6IfIndex, &ip6Addr);
                KW_FALSEPOSITIVE_FIX (pLLAddrInfo);
                return SNMP_FAILURE;
            }
            pLLAddrInfo->u1AdminStatus = IP6FWD_ACTIVE;
        }
    }

    IncMsrForIpv6IfTable (i4Ipv6IfIndex, 's', pSetValIpv6IfIdentifier,
                          FsMIIpv6IfToken,
                          sizeof (FsMIIpv6IfToken) / sizeof (UINT4));

    OsixGetSysTime (&gIp6GblInfo.u4Ip6IfTableLastChange);
    KW_FALSEPOSITIVE_FIX (pLLAddrInfo);
    return (SNMP_SUCCESS);

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhSetIpv6IfIdentifierLength
 Input       :  The Indices
                Ipv6IfIndex

                The Object 
                setValIpv6IfIdentifierLength
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetIpv6IfIdentifierLength ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetIpv6IfIdentifierLength (INT4 i4Ipv6IfIndex,
                              INT4 i4SetValIpv6IfIdentifierLength)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6IfIndex = %d\n", i4Ipv6IfIndex); ***/
   /*** $$TRACE_LOG (ENTRY, "Ipv6IfIdentifierLength = %d\n", i4SetValIpv6IfIdentifierLength); ***/
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT2) i4Ipv6IfIndex);

    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    if ((pIf6->u1IfType == IP6_ENET_INTERFACE_TYPE) ||
#ifdef WGS_WANTED
        (pIf6->u1IfType == IP6_L2VLAN_INTERFACE_TYPE) ||
#endif
        (pIf6->u1IfType == IP6_L3VLAN_INTERFACE_TYPE) ||
        (pIf6->u1IfType == IP6_LAGG_INTERFACE_TYPE) ||
        (pIf6->u1IfType == IP6_PSEUDO_WIRE_INTERFACE_TYPE) ||
        (pIf6->u1IfType == IP6_L3SUB_INTF_TYPE))
    {
        /* length of byte * in bits */
        pIf6->u1TokLen = (UINT1) ((i4SetValIpv6IfIdentifierLength) /
                                  BYTE_LENGTH);

        return SNMP_SUCCESS;
    }
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetIpv6IfAdminStatus
 Input       :  The Indices
                Ipv6IfIndex

                The Object 
                setValIpv6IfAdminStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetIpv6IfAdminStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetIpv6IfAdminStatus (INT4 i4Ipv6IfIndex, INT4 i4SetValIpv6IfAdminStatus)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6IfIndex = %d\n", i4Ipv6IfIndex); ***/
   /*** $$TRACE_LOG (ENTRY, "Ipv6IfAdminStatus = %d\n", i4SetValIpv6IfAdminStatus); ***/

    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT2) i4Ipv6IfIndex);

    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    return (nmhSetFsipv6IfAdminStatus (i4Ipv6IfIndex,
                                       i4SetValIpv6IfAdminStatus));
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ipv6IfDescr
 Input       :  The Indices
                Ipv6IfIndex

                The Object 
                testValIpv6IfDescr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2Ipv6IfDescr ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2Ipv6IfDescr (UINT4 *pu4ErrorCode, INT4 i4Ipv6IfIndex,
                      tSNMP_OCTET_STRING_TYPE * pTestValIpv6IfDescr)
{
    /*** $$TRACE_LOG (ENTRY, "Ipv6IfIndex = %d\n", i4Ipv6IfIndex); ***/
    if (Ip6ifEntryExists (i4Ipv6IfIndex) == IP6_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (pTestValIpv6IfDescr->pu1_OctetList == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

#ifdef SNMP_2_WANTED
    /* NVT CHECK */
    if (SNMPCheckForNVTChars (pTestValIpv6IfDescr->pu1_OctetList,
                              pTestValIpv6IfDescr->i4_Length) == OSIX_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
#endif

    if (pTestValIpv6IfDescr->i4_Length > IP6_IF_DESCR_LEN)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhTestv2Ipv6IfIdentifier
 Input       :  The Indices
                Ipv6IfIndex

                The Object 
                testValIpv6IfIdentifier
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2Ipv6IfIdentifier ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2Ipv6IfIdentifier (UINT4 *pu4ErrorCode, INT4 i4Ipv6IfIndex,
                           tSNMP_OCTET_STRING_TYPE * pTestValIpv6IfIdentifier)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6IfIndex = %d\n", i4Ipv6IfIndex); ***/
    if (Ip6ifEntryExists (i4Ipv6IfIndex) == IP6_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((pTestValIpv6IfIdentifier->i4_Length) > IP6_EUI_ADDRESS_LEN)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhTestv2Ipv6IfIdentifierLength
 Input       :  The Indices
                Ipv6IfIndex

                The Object 
                testValIpv6IfIdentifierLength
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2Ipv6IfIdentifierLength ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2Ipv6IfIdentifierLength (UINT4 *pu4ErrorCode, INT4 i4Ipv6IfIndex,
                                 INT4 i4TestValIpv6IfIdentifierLength)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6IfIndex = %d\n", i4Ipv6IfIndex); ***/
   /*** $$TRACE_LOG (ENTRY, "Ipv6IfIdentifierLength = %d\n", i4TestValIpv6IfIdentifierLength); ***/
    if (Ip6ifEntryExists (i4Ipv6IfIndex) == IP6_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4TestValIpv6IfIdentifierLength > IP6_EUI_ADDRESS_LEN * BYTE_LENGTH)
        || (i4TestValIpv6IfIdentifierLength < 0))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhTestv2Ipv6IfAdminStatus
 Input       :  The Indices
                Ipv6IfIndex

                The Object 
                testValIpv6IfAdminStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2Ipv6IfAdminStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2Ipv6IfAdminStatus (UINT4 *pu4ErrorCode, INT4 i4Ipv6IfIndex,
                            INT4 i4TestValIpv6IfAdminStatus)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6IfIndex = %d\n", i4Ipv6IfIndex); ***/
   /*** $$TRACE_LOG (ENTRY, "Ipv6IfAdminStatus = %d\n", i4TestValIpv6IfAdminStatus); ***/
    return (nmhTestv2Fsipv6IfAdminStatus (pu4ErrorCode, i4Ipv6IfIndex,
                                          i4TestValIpv6IfAdminStatus));

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ipv6IfTable
 Input       :  The Indices
                Ipv6IfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ipv6IfTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ipv6IfStatsTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIpv6IfStatsTable
 Input       :  The Indices
                Ipv6IfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhValidateIndexInstanceIpv6IfStatsTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhValidateIndexInstanceIpv6IfStatsTable (INT4 i4Ipv6IfIndex)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6IfIndex = %d\n", i4Ipv6IfIndex); ***/
    if (Ip6ifEntryExists ((UINT4) i4Ipv6IfIndex) == IP6_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (Ip6ifGetEntry ((UINT2) i4Ipv6IfIndex) == NULL)
    {
        return (SNMP_FAILURE);
    }

    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIpv6IfStatsTable
 Input       :  The Indices
                Ipv6IfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhGetFirstIpv6IfStatsTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFirstIndexIpv6IfStatsTable (INT4 *pi4Ipv6IfIndex)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6IfIndex = %d\n", *pi4Ipv6IfIndex); ***/
    *pi4Ipv6IfIndex = 1;

    if (Ip6ifEntryExists (*pi4Ipv6IfIndex) == IP6_SUCCESS)
    {
        return (SNMP_SUCCESS);
    }

    if (Ip6ifGetNextIndex ((UINT4 *) pi4Ipv6IfIndex) == IP6_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetNextIndexIpv6IfStatsTable
 Input       :  The Indices
                Ipv6IfIndex
                nextIpv6IfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
 /*** $$TRACE_PROCEDURE_NAME = nmhGetNextIpv6IfStatsTable ***/
 /*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetNextIndexIpv6IfStatsTable (INT4 i4Ipv6IfIndex, INT4 *pi4NextIpv6IfIndex)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6IfIndex = %d\n", *pi4Ipv6IfIndex); ***/
    UINT4               u4Index = (i4Ipv6IfIndex) + 1;

    IP6_IF_SCAN (u4Index, (UINT4) IP6_MAX_LOGICAL_IF_INDEX)
    {
        if (Ip6ifEntryExists (u4Index) == IP6_SUCCESS)
        {
            *pi4NextIpv6IfIndex = u4Index;
            return (SNMP_SUCCESS);
        }
    }

    return (SNMP_FAILURE);

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIpv6IfStatsInReceives
 Input       :  The Indices
                Ipv6IfIndex

                The Object 
                retValIpv6IfStatsInReceives
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpv6IfStatsInReceives ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetIpv6IfStatsInReceives (INT4 i4Ipv6IfIndex,
                             UINT4 *pu4RetValIpv6IfStatsInReceives)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6IfIndex = %d\n", i4Ipv6IfIndex); ***/
    tIp6If             *pIf6 = Ip6ifGetEntry (i4Ipv6IfIndex);
    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValIpv6IfStatsInReceives = pIf6->stats.u4InRcvs;
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetIpv6IfStatsInHdrErrors
 Input       :  The Indices
                Ipv6IfIndex

                The Object 
                retValIpv6IfStatsInHdrErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpv6IfStatsInHdrErrors ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetIpv6IfStatsInHdrErrors (INT4 i4Ipv6IfIndex,
                              UINT4 *pu4RetValIpv6IfStatsInHdrErrors)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6IfIndex = %d\n", i4Ipv6IfIndex); ***/
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT2) i4Ipv6IfIndex);
    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValIpv6IfStatsInHdrErrors = pIf6->stats.u4InHdrerrs;
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetIpv6IfStatsInTooBigErrors
 Input       :  The Indices
                Ipv6IfIndex

                The Object 
                retValIpv6IfStatsInTooBigErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpv6IfStatsInTooBigErrors ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetIpv6IfStatsInTooBigErrors (INT4 i4Ipv6IfIndex,
                                 UINT4 *pu4RetValIpv6IfStatsInTooBigErrors)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6IfIndex = %d\n", i4Ipv6IfIndex); ***/
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT2) i4Ipv6IfIndex);
    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValIpv6IfStatsInTooBigErrors = pIf6->stats.u4TooBigerrs;
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetIpv6IfStatsInNoRoutes
 Input       :  The Indices
                Ipv6IfIndex

                The Object 
                retValIpv6IfStatsInNoRoutes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpv6IfStatsInNoRoutes ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetIpv6IfStatsInNoRoutes (INT4 i4Ipv6IfIndex,
                             UINT4 *pu4RetValIpv6IfStatsInNoRoutes)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6IfIndex = %d\n", i4Ipv6IfIndex); ***/
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT2) i4Ipv6IfIndex);
    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValIpv6IfStatsInNoRoutes = pIf6->stats.u4InNorts;
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetIpv6IfStatsInAddrErrors
 Input       :  The Indices
                Ipv6IfIndex

                The Object 
                retValIpv6IfStatsInAddrErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpv6IfStatsInAddrErrors ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetIpv6IfStatsInAddrErrors (INT4 i4Ipv6IfIndex,
                               UINT4 *pu4RetValIpv6IfStatsInAddrErrors)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6IfIndex = %d\n", i4Ipv6IfIndex); ***/
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT2) i4Ipv6IfIndex);
    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValIpv6IfStatsInAddrErrors = pIf6->stats.u4InAddrerrs;
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetIpv6IfStatsInUnknownProtos
 Input       :  The Indices
                Ipv6IfIndex

                The Object 
                retValIpv6IfStatsInUnknownProtos
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpv6IfStatsInUnknownProtos ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetIpv6IfStatsInUnknownProtos (INT4 i4Ipv6IfIndex,
                                  UINT4 *pu4RetValIpv6IfStatsInUnknownProtos)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6IfIndex = %d\n", i4Ipv6IfIndex); ***/
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT2) i4Ipv6IfIndex);
    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValIpv6IfStatsInUnknownProtos = pIf6->stats.u4InUnkprots;
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetIpv6IfStatsInTruncatedPkts
 Input       :  The Indices
                Ipv6IfIndex

                The Object 
                retValIpv6IfStatsInTruncatedPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpv6IfStatsInTruncatedPkts ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetIpv6IfStatsInTruncatedPkts (INT4 i4Ipv6IfIndex,
                                  UINT4 *pu4RetValIpv6IfStatsInTruncatedPkts)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6IfIndex = %d\n", i4Ipv6IfIndex); ***/
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT2) i4Ipv6IfIndex);
    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValIpv6IfStatsInTruncatedPkts = pIf6->stats.u4InTruncs;
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetIpv6IfStatsInDiscards
 Input       :  The Indices
                Ipv6IfIndex

                The Object 
                retValIpv6IfStatsInDiscards
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpv6IfStatsInDiscards ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetIpv6IfStatsInDiscards (INT4 i4Ipv6IfIndex,
                             UINT4 *pu4RetValIpv6IfStatsInDiscards)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6IfIndex = %d\n", i4Ipv6IfIndex); ***/
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT2) i4Ipv6IfIndex);
    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValIpv6IfStatsInDiscards = pIf6->stats.u4InDiscards;
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetIpv6IfStatsInDelivers
 Input       :  The Indices
                Ipv6IfIndex

                The Object 
                retValIpv6IfStatsInDelivers
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpv6IfStatsInDelivers ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetIpv6IfStatsInDelivers (INT4 i4Ipv6IfIndex,
                             UINT4 *pu4RetValIpv6IfStatsInDelivers)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6IfIndex = %d\n", i4Ipv6IfIndex); ***/
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT2) i4Ipv6IfIndex);
    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValIpv6IfStatsInDelivers = pIf6->stats.u4InDelivers;
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetIpv6IfStatsOutForwDatagrams
 Input       :  The Indices
                Ipv6IfIndex

                The Object 
                retValIpv6IfStatsOutForwDatagrams
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpv6IfStatsOutForwDatagrams ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetIpv6IfStatsOutForwDatagrams (INT4 i4Ipv6IfIndex,
                                   UINT4 *pu4RetValIpv6IfStatsOutForwDatagrams)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6IfIndex = %d\n", i4Ipv6IfIndex); ***/
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT2) i4Ipv6IfIndex);
    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValIpv6IfStatsOutForwDatagrams = pIf6->stats.u4ForwDgrams;
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetIpv6IfStatsOutRequests
 Input       :  The Indices
                Ipv6IfIndex

                The Object 
                retValIpv6IfStatsOutRequests
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpv6IfStatsOutRequests ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetIpv6IfStatsOutRequests (INT4 i4Ipv6IfIndex,
                              UINT4 *pu4RetValIpv6IfStatsOutRequests)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6IfIndex = %d\n", i4Ipv6IfIndex); ***/
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT2) i4Ipv6IfIndex);
    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValIpv6IfStatsOutRequests = pIf6->stats.u4OutReqs;
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetIpv6IfStatsOutDiscards
 Input       :  The Indices
                Ipv6IfIndex

                The Object 
                retValIpv6IfStatsOutDiscards
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpv6IfStatsOutDiscards ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetIpv6IfStatsOutDiscards (INT4 i4Ipv6IfIndex,
                              UINT4 *pu4RetValIpv6IfStatsOutDiscards)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6IfIndex = %d\n", i4Ipv6IfIndex); ***/
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT2) i4Ipv6IfIndex);
    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValIpv6IfStatsOutDiscards = pIf6->stats.u4OutDiscards;
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetIpv6IfStatsOutFragOKs
 Input       :  The Indices
                Ipv6IfIndex

                The Object 
                retValIpv6IfStatsOutFragOKs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpv6IfStatsOutFragOKs ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetIpv6IfStatsOutFragOKs (INT4 i4Ipv6IfIndex,
                             UINT4 *pu4RetValIpv6IfStatsOutFragOKs)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6IfIndex = %d\n", i4Ipv6IfIndex); ***/
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT2) i4Ipv6IfIndex);
    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValIpv6IfStatsOutFragOKs = pIf6->stats.u4Fragoks;
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetIpv6IfStatsOutFragFails
 Input       :  The Indices
                Ipv6IfIndex

                The Object 
                retValIpv6IfStatsOutFragFails
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpv6IfStatsOutFragFails ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetIpv6IfStatsOutFragFails (INT4 i4Ipv6IfIndex,
                               UINT4 *pu4RetValIpv6IfStatsOutFragFails)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6IfIndex = %d\n", i4Ipv6IfIndex); ***/
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT2) i4Ipv6IfIndex);
    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValIpv6IfStatsOutFragFails = pIf6->stats.u4Fragfails;
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetIpv6IfStatsOutFragCreates
 Input       :  The Indices
                Ipv6IfIndex

                The Object 
                retValIpv6IfStatsOutFragCreates
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpv6IfStatsOutFragCreates ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetIpv6IfStatsOutFragCreates (INT4 i4Ipv6IfIndex,
                                 UINT4 *pu4RetValIpv6IfStatsOutFragCreates)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6IfIndex = %d\n", i4Ipv6IfIndex); ***/
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT2) i4Ipv6IfIndex);
    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValIpv6IfStatsOutFragCreates = pIf6->stats.u4Fragcreates;
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetIpv6IfStatsReasmReqds
 Input       :  The Indices
                Ipv6IfIndex

                The Object 
                retValIpv6IfStatsReasmReqds
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpv6IfStatsReasmReqds ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetIpv6IfStatsReasmReqds (INT4 i4Ipv6IfIndex,
                             UINT4 *pu4RetValIpv6IfStatsReasmReqds)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6IfIndex = %d\n", i4Ipv6IfIndex); ***/
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT2) i4Ipv6IfIndex);
    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValIpv6IfStatsReasmReqds = pIf6->stats.u4Reasmreqs;
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetIpv6IfStatsReasmOKs
 Input       :  The Indices
                Ipv6IfIndex

                The Object 
                retValIpv6IfStatsReasmOKs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpv6IfStatsReasmOKs ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetIpv6IfStatsReasmOKs (INT4 i4Ipv6IfIndex,
                           UINT4 *pu4RetValIpv6IfStatsReasmOKs)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6IfIndex = %d\n", i4Ipv6IfIndex); ***/

    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT2) i4Ipv6IfIndex);
    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValIpv6IfStatsReasmOKs = pIf6->stats.u4Reasmoks;
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetIpv6IfStatsReasmFails
 Input       :  The Indices
                Ipv6IfIndex

                The Object 
                retValIpv6IfStatsReasmFails
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpv6IfStatsReasmFails ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetIpv6IfStatsReasmFails (INT4 i4Ipv6IfIndex,
                             UINT4 *pu4RetValIpv6IfStatsReasmFails)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6IfIndex = %d\n", i4Ipv6IfIndex); ***/
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT2) i4Ipv6IfIndex);
    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValIpv6IfStatsReasmFails = pIf6->stats.u4Reasmfails;
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetIpv6IfStatsInMcastPkts
 Input       :  The Indices
                Ipv6IfIndex

                The Object 
                retValIpv6IfStatsInMcastPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpv6IfStatsInMcastPkts ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetIpv6IfStatsInMcastPkts (INT4 i4Ipv6IfIndex,
                              UINT4 *pu4RetValIpv6IfStatsInMcastPkts)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6IfIndex = %d\n", i4Ipv6IfIndex); ***/
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT2) i4Ipv6IfIndex);
    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValIpv6IfStatsInMcastPkts = pIf6->stats.u4InMcasts;
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetIpv6IfStatsOutMcastPkts
 Input       :  The Indices
                Ipv6IfIndex

                The Object 
                retValIpv6IfStatsOutMcastPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpv6IfStatsOutMcastPkts ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetIpv6IfStatsOutMcastPkts (INT4 i4Ipv6IfIndex,
                               UINT4 *pu4RetValIpv6IfStatsOutMcastPkts)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6IfIndex = %d\n", i4Ipv6IfIndex); ***/
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT2) i4Ipv6IfIndex);
    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValIpv6IfStatsOutMcastPkts = pIf6->stats.u4OutMcasts;
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/* LOW LEVEL Routines for Table : Ipv6AddrPrefixTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIpv6AddrPrefixTable
 Input       :  The Indices
                Ipv6IfIndex
                Ipv6AddrPrefix
                Ipv6AddrPrefixLength
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhValidateIndexInstanceIpv6AddrPrefixTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhValidateIndexInstanceIpv6AddrPrefixTable (INT4 i4Ipv6IfIndex,
                                             tSNMP_OCTET_STRING_TYPE *
                                             pIpv6AddrPrefix,
                                             INT4 i4Ipv6AddrPrefixLength)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6IfIndex = %d\n", i4Ipv6IfIndex); ***/
   /*** $$TRACE_LOG (ENTRY, "Ipv6AddrPrefixLength = %d\n", i4Ipv6AddrPrefixLength); ***/
    if ((Ip6ifEntryExists ((UINT4) i4Ipv6IfIndex) == IP6_SUCCESS) &&
        (pIpv6AddrPrefix->pu1_OctetList != NULL) &&
        (i4Ipv6AddrPrefixLength <= V6_HOST_PREFIX_LENGTH))
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIpv6AddrPrefixTable
 Input       :  The Indices
                Ipv6IfIndex
                Ipv6AddrPrefix
                Ipv6AddrPrefixLength
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhGetFirstIpv6AddrPrefixTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFirstIndexIpv6AddrPrefixTable (INT4 *pi4Ipv6IfIndex,
                                     tSNMP_OCTET_STRING_TYPE * pIpv6AddrPrefix,
                                     INT4 *pi4Ipv6AddrPrefixLength)
{
    if (nmhGetFirstIndexFsipv6AddrTable (pi4Ipv6IfIndex,
                                         pIpv6AddrPrefix,
                                         pi4Ipv6AddrPrefixLength) ==
        SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;

    }
    else
    {
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhGetNextIndexIpv6AddrPrefixTable
 Input       :  The Indices
                Ipv6IfIndex
                nextIpv6IfIndex
                Ipv6AddrPrefix
                nextIpv6AddrPrefix
                Ipv6AddrPrefixLength
                nextIpv6AddrPrefixLength
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
 /*** $$TRACE_PROCEDURE_NAME = nmhGetNextIpv6AddrPrefixTable ***/
 /*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetNextIndexIpv6AddrPrefixTable (INT4 i4Ipv6IfIndex,
                                    INT4 *pi4NextIpv6IfIndex,
                                    tSNMP_OCTET_STRING_TYPE * pIpv6AddrPrefix,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pNextIpv6AddrPrefix,
                                    INT4 i4Ipv6AddrPrefixLength,
                                    INT4 *pi4NextIpv6AddrPrefixLength)
{
    if (nmhGetNextIndexFsipv6AddrTable (i4Ipv6IfIndex, pi4NextIpv6IfIndex,
                                        pIpv6AddrPrefix, pNextIpv6AddrPrefix,
                                        i4Ipv6AddrPrefixLength,
                                        pi4NextIpv6AddrPrefixLength) ==
        SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIpv6AddrPrefixOnLinkFlag
 Input       :  The Indices
                Ipv6IfIndex
                Ipv6AddrPrefix
                Ipv6AddrPrefixLength

                The Object 
                retValIpv6AddrPrefixOnLinkFlag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpv6AddrPrefixOnLinkFlag ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetIpv6AddrPrefixOnLinkFlag (INT4 i4Ipv6IfIndex,
                                tSNMP_OCTET_STRING_TYPE * pIpv6AddrPrefix,
                                INT4 i4Ipv6AddrPrefixLength,
                                INT4 *pi4RetValIpv6AddrPrefixOnLinkFlag)
{
    tIp6Addr            Ipv6Address;
    tIp6AddrInfo       *pAddrInfo = NULL;
    tIp6LlocalInfo     *pAddrLL = NULL;

    Ip6AddrCopy (&Ipv6Address, (tIp6Addr *) (VOID *)
                 pIpv6AddrPrefix->pu1_OctetList);
    if (IS_ADDR_LLOCAL (Ipv6Address))
    {
        pAddrLL
            = Ip6AddrTblGetLlocalEntry ((UINT2) i4Ipv6IfIndex,
                                        (tIp6Addr *) & Ipv6Address);
        if (pAddrLL == NULL)
        {
            return SNMP_FAILURE;
        }
        *pi4RetValIpv6AddrPrefixOnLinkFlag = TRUE;
        return SNMP_SUCCESS;
    }

    pAddrInfo =
        Ip6AddrTblGetEntry ((UINT2) i4Ipv6IfIndex,
                            (tIp6Addr *) & Ipv6Address,
                            (UINT1) i4Ipv6AddrPrefixLength);
    if (pAddrInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValIpv6AddrPrefixOnLinkFlag = TRUE;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIpv6AddrPrefixAutonomousFlag
 Input       :  The Indices
                Ipv6IfIndex
                Ipv6AddrPrefix
                Ipv6AddrPrefixLength

                The Object 
                retValIpv6AddrPrefixAutonomousFlag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpv6AddrPrefixAutonomousFlag ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetIpv6AddrPrefixAutonomousFlag (INT4 i4Ipv6IfIndex,
                                    tSNMP_OCTET_STRING_TYPE * pIpv6AddrPrefix,
                                    INT4 i4Ipv6AddrPrefixLength,
                                    INT4 *pi4RetValIpv6AddrPrefixAutonomousFlag)
{
    tIp6Addr            Ipv6Address;
    tIp6AddrInfo       *pAddrInfo = NULL;
    tIp6LlocalInfo     *pAddrLL = NULL;

    Ip6AddrCopy (&Ipv6Address, (tIp6Addr *) (VOID *) pIpv6AddrPrefix->
                 pu1_OctetList);
    if (IS_ADDR_LLOCAL (Ipv6Address))
    {
        pAddrLL
            = Ip6AddrTblGetLlocalEntry ((UINT2) i4Ipv6IfIndex,
                                        (tIp6Addr *) & Ipv6Address);
        if (pAddrLL == NULL)
        {
            return SNMP_FAILURE;
        }
        *pi4RetValIpv6AddrPrefixAutonomousFlag = TRUE;
        return SNMP_SUCCESS;
    }
    pAddrInfo =
        Ip6AddrTblGetEntry ((UINT2) i4Ipv6IfIndex,
                            (tIp6Addr *) & Ipv6Address,
                            (UINT1) i4Ipv6AddrPrefixLength);
    if (pAddrInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValIpv6AddrPrefixAutonomousFlag = TRUE;
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetIpv6AddrPrefixAdvPreferredLifetime
 Input       :  The Indices
                Ipv6IfIndex
                Ipv6AddrPrefix
                Ipv6AddrPrefixLength

                The Object 
                retValIpv6AddrPrefixAdvPreferredLifetime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpv6AddrPrefixAdvPreferredLifetime ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetIpv6AddrPrefixAdvPreferredLifetime (INT4 i4Ipv6IfIndex,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pIpv6AddrPrefix,
                                          INT4 i4Ipv6AddrPrefixLength,
                                          UINT4
                                          *pu4RetValIpv6AddrPrefixAdvPreferredLifetime)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6IfIndex = %d\n", i4Ipv6IfIndex); ***/
   /*** $$TRACE_LOG (ENTRY, "Ipv6AddrPrefixLength = %d\n", i4Ipv6AddrPrefixLength); ***/
    INT4                i4AddrProfileIndex;
    tIp6Addr            Ipv6Address;

    Ip6AddrCopy (&Ipv6Address, (tIp6Addr *) (VOID *) pIpv6AddrPrefix->
                 pu1_OctetList);
    i4AddrProfileIndex =
        Ip6GetProfileIndex (i4Ipv6IfIndex, Ipv6Address, i4Ipv6AddrPrefixLength);

    if ((i4AddrProfileIndex < 0)
        || (i4AddrProfileIndex >= (INT4) gIp6GblInfo.u4MaxAddrProfileLimit) ||
        (i4AddrProfileIndex >= MAX_IP6_ADDR_PROFILES_LIMIT))
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6AddrProfilePreferredTime
            ((UINT4) i4AddrProfileIndex,
             pu4RetValIpv6AddrPrefixAdvPreferredLifetime));

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetIpv6AddrPrefixAdvValidLifetime
 Input       :  The Indices
                Ipv6IfIndex
                Ipv6AddrPrefix
                Ipv6AddrPrefixLength

                The Object 
                retValIpv6AddrPrefixAdvValidLifetime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpv6AddrPrefixAdvValidLifetime ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetIpv6AddrPrefixAdvValidLifetime (INT4 i4Ipv6IfIndex,
                                      tSNMP_OCTET_STRING_TYPE * pIpv6AddrPrefix,
                                      INT4 i4Ipv6AddrPrefixLength,
                                      UINT4
                                      *pu4RetValIpv6AddrPrefixAdvValidLifetime)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6IfIndex = %d\n", i4Ipv6IfIndex); ***/
   /*** $$TRACE_LOG (ENTRY, "Ipv6AddrPrefixLength = %d\n", i4Ipv6AddrPrefixLength); ***/
    INT4                i4AddrProfileIndex;
    tIp6Addr            Ipv6Address;

    Ip6AddrCopy (&Ipv6Address, (tIp6Addr *) (VOID *) pIpv6AddrPrefix->
                 pu1_OctetList);
    i4AddrProfileIndex =
        Ip6GetProfileIndex (i4Ipv6IfIndex, Ipv6Address, i4Ipv6AddrPrefixLength);

    if ((i4AddrProfileIndex < 0)
        || (i4AddrProfileIndex >= (INT4) gIp6GblInfo.u4MaxAddrProfileLimit) ||
        (i4AddrProfileIndex >= MAX_IP6_ADDR_PROFILES_LIMIT))
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsipv6AddrProfileValidTime
            ((UINT4) i4AddrProfileIndex,
             pu4RetValIpv6AddrPrefixAdvValidLifetime));

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/* LOW LEVEL Routines for Table : Ipv6AddrTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIpv6AddrTable
 Input       :  The Indices
                Ipv6IfIndex
                Ipv6AddrAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhValidateIndexInstanceIpv6AddrTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhValidateIndexInstanceIpv6AddrTable (INT4 i4Ipv6IfIndex,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pIpv6AddrAddress)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6IfIndex = %d\n", i4Ipv6IfIndex); ***/
    tIp6Addr            ip6addr;
    UNUSED_PARAM (i4Ipv6IfIndex);
    if (pIpv6AddrAddress->pu1_OctetList == NULL)
    {
        return SNMP_FAILURE;
    }
    Ip6AddrCopy (&ip6addr,
                 (tIp6Addr *) (VOID *) pIpv6AddrAddress->pu1_OctetList);
    if ((IS_ADDR_MULTI (ip6addr)) || (IS_ADDR_UNSPECIFIED (ip6addr)))
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIpv6AddrTable
 Input       :  The Indices
                Ipv6IfIndex
                Ipv6AddrAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhGetFirstIpv6AddrTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFirstIndexIpv6AddrTable (INT4 *pi4Ipv6IfIndex,
                               tSNMP_OCTET_STRING_TYPE * pIpv6AddrAddress)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6IfIndex = %d\n", *pi4Ipv6IfIndex); ***/
    INT4                i4PrefLen = 0;
    *pi4Ipv6IfIndex = 0;
    if (nmhGetFirstIndexFsipv6AddrTable (pi4Ipv6IfIndex,
                                         pIpv6AddrAddress, &i4PrefLen) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pIpv6AddrAddress->i4_Length = IP6_ADDR_SIZE;
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetNextIndexIpv6AddrTable
 Input       :  The Indices
                Ipv6IfIndex
                nextIpv6IfIndex
                Ipv6AddrAddress
                nextIpv6AddrAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
 /*** $$TRACE_PROCEDURE_NAME = nmhGetNextIpv6AddrTable ***/
 /*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetNextIndexIpv6AddrTable (INT4 i4Ipv6IfIndex, INT4 *pi4NextIpv6IfIndex,
                              tSNMP_OCTET_STRING_TYPE * pIpv6AddrAddress,
                              tSNMP_OCTET_STRING_TYPE * pNextIpv6AddrAddress)
{
    tIp6If             *pIf6 = NULL;
    INT4                i4PfxLen = 0, i4NextPfxLen = 0;
    tIp6Addr            Ipv6Address;
    INT1                i1RetVal = 0;

    pIf6 = Ip6ifGetEntry (i4Ipv6IfIndex);
    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }
    Ip6AddrCopy (&Ipv6Address, (tIp6Addr *) (VOID *) pIpv6AddrAddress->
                 pu1_OctetList);
    if (Ip6GetAddrPrefixLen (i4Ipv6IfIndex, (tIp6Addr *) & Ipv6Address,
                             &i4PfxLen) == IP6_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhGetNextIndexFsipv6AddrTable (i4Ipv6IfIndex,
                                               pi4NextIpv6IfIndex,
                                               pIpv6AddrAddress,
                                               pNextIpv6AddrAddress,
                                               i4PfxLen, &i4NextPfxLen);
    return i1RetVal;

   /*** $$TRACE_LOG (ENTRY, "Ipv6IfIndex = %d\n", *pi4Ipv6IfIndex); ***/
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIpv6AddrPfxLength
 Input       :  The Indices
                Ipv6IfIndex
                Ipv6AddrAddress

                The Object 
                retValIpv6AddrPfxLength
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpv6AddrPfxLength ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetIpv6AddrPfxLength (INT4 i4Ipv6IfIndex,
                         tSNMP_OCTET_STRING_TYPE * pIpv6AddrAddress,
                         INT4 *pi4RetValIpv6AddrPfxLength)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6IfIndex = %d\n", i4Ipv6IfIndex); ***/
    tIp6Addr            Ipv6Address;
    INT4                i4PfxLen = 0;
    Ip6AddrCopy (&Ipv6Address, (tIp6Addr *) (VOID *) pIpv6AddrAddress->
                 pu1_OctetList);
    if (Ip6GetAddrPrefixLen
        (i4Ipv6IfIndex, (tIp6Addr *) & Ipv6Address, &i4PfxLen) == IP6_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValIpv6AddrPfxLength = i4PfxLen;
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetIpv6AddrType
 Input       :  The Indices
                Ipv6IfIndex
                Ipv6AddrAddress

                The Object 
                retValIpv6AddrType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpv6AddrType ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetIpv6AddrType (INT4 i4Ipv6IfIndex,
                    tSNMP_OCTET_STRING_TYPE * pIpv6AddrAddress,
                    INT4 *pi4RetValIpv6AddrType)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6IfIndex = %d\n", i4Ipv6IfIndex); ***/
    tIp6Addr            Ipv6Address;

    UNUSED_PARAM (i4Ipv6IfIndex);
    Ip6AddrCopy (&Ipv6Address, (tIp6Addr *) (VOID *) pIpv6AddrAddress->
                 pu1_OctetList);
    if (Ip6AddrType (&Ipv6Address) == ADDR6_LLOCAL)
    {
        *pi4RetValIpv6AddrType = IP6_ADDR_TYPE_STATELESS;
    }
    else
    {
        *pi4RetValIpv6AddrType = IP6_ADDR_TYPE_STATEFUL;
    }
    return SNMP_SUCCESS;
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetIpv6AddrAnycastFlag
 Input       :  The Indices
                Ipv6IfIndex
                Ipv6AddrAddress

                The Object 
                retValIpv6AddrAnycastFlag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpv6AddrAnycastFlag ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetIpv6AddrAnycastFlag (INT4 i4Ipv6IfIndex,
                           tSNMP_OCTET_STRING_TYPE * pIpv6AddrAddress,
                           INT4 *pi4RetValIpv6AddrAnycastFlag)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6IfIndex = %d\n", i4Ipv6IfIndex); ***/
    tIp6AddrInfo       *pAddrInfo = NULL;
    tIp6Addr            Ipv6Address;
    UINT1               u1PfxLen = 0;
    INT4                i4Prefix = 0;

    Ip6AddrCopy (&Ipv6Address, (tIp6Addr *) (VOID *) pIpv6AddrAddress->
                 pu1_OctetList);
    if (Ip6AddrType (&Ipv6Address) == ADDR6_LLOCAL)
    {
        *pi4RetValIpv6AddrAnycastFlag = IP6_FALSE;
        return SNMP_SUCCESS;
    }
    if (Ip6GetAddrPrefixLen (i4Ipv6IfIndex, (tIp6Addr *) & Ipv6Address,
                             &i4Prefix) == IP6_FAILURE)
    {
        return SNMP_FAILURE;
    }
    u1PfxLen = (UINT1) i4Prefix;
    pAddrInfo =
        Ip6AddrTblGetEntry ((UINT2) i4Ipv6IfIndex,
                            (tIp6Addr *) & Ipv6Address, u1PfxLen);
    if (pAddrInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pAddrInfo->u1AddrType == ADDR6_ANYCAST)
    {
        *pi4RetValIpv6AddrAnycastFlag = IP6_TRUE;
    }
    else
    {
        *pi4RetValIpv6AddrAnycastFlag = IP6_FALSE;
    }
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetIpv6AddrStatus
 Input       :  The Indices
                Ipv6IfIndex
                Ipv6AddrAddress

                The Object 
                retValIpv6AddrStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpv6AddrStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetIpv6AddrStatus (INT4 i4Ipv6IfIndex,
                      tSNMP_OCTET_STRING_TYPE * pIpv6AddrAddress,
                      INT4 *pi4RetValIpv6AddrStatus)
{

    if (Ip6GetIpv6AddrStatus (i4Ipv6IfIndex, pIpv6AddrAddress,
                              pi4RetValIpv6AddrStatus) == IP6_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/* LOW LEVEL Routines for Table : Ipv6RouteTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIpv6RouteTable
 Input       :  The Indices
                Ipv6RouteDest
                Ipv6RoutePfxLength
                Ipv6RouteIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhValidateIndexInstanceIpv6RouteTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhValidateIndexInstanceIpv6RouteTable (tSNMP_OCTET_STRING_TYPE *
                                        pIpv6RouteDest,
                                        INT4 i4Ipv6RoutePfxLength,
                                        UINT4 u4Ipv6RouteIndex)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RoutePfxLength = %d\n", i4Ipv6RoutePfxLength); ***/
   /*** $$TRACE_LOG (ENTRY,"Ipv6RouteIndex = %u\n", u4Ipv6RouteIndex); ***/
    UNUSED_PARAM (u4Ipv6RouteIndex);
    if ((pIpv6RouteDest != NULL)
        && (i4Ipv6RoutePfxLength <= V6_HOST_PREFIX_LENGTH))
    {
        return SNMP_SUCCESS;
    }

    if (gIp6GblInfo.pIp6CurrCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    return SNMP_FAILURE;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIpv6RouteTable
 Input       :  The Indices
                Ipv6RouteDest
                Ipv6RoutePfxLength
                Ipv6RouteIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIpv6RouteTable (tSNMP_OCTET_STRING_TYPE * pIpv6RouteDest,
                                INT4 *pi4Ipv6RoutePfxLength,
                                UINT4 *pu4Ipv6RouteIndex)
{
    tNetIpv6RtInfo      NetIpv6RtInfo;
    UINT4               u4ContextId = 0;

    if (gIp6GblInfo.pIp6CurrCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    u4ContextId = gIp6GblInfo.pIp6CurrCxt->u4ContextId;
    IP6_TASK_UNLOCK ();
    RTM6_TASK_LOCK ();

    MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));
    /* Get the first route from the TRIE. */
    if (Rtm6ApiTrieGetFirstBestRtInCxt (u4ContextId,
                                        &NetIpv6RtInfo) == NETIPV6_FAILURE)
    {
        RTM6_TASK_UNLOCK ();
        IP6_TASK_LOCK ();
        return SNMP_FAILURE;
    }
    RTM6_TASK_UNLOCK ();
    IP6_TASK_LOCK ();

    Ip6AddrCopy ((tIp6Addr *) (VOID *) pIpv6RouteDest->pu1_OctetList,
                 &NetIpv6RtInfo.Ip6Dst);
    pIpv6RouteDest->i4_Length = IP6_ADDR_SIZE;
    *pi4Ipv6RoutePfxLength = NetIpv6RtInfo.u1Prefixlen;
    *pu4Ipv6RouteIndex = 1;
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetNextIndexIpv6RouteTable
 Input       :  The Indices
                Ipv6RouteDest
                nextIpv6RouteDest
                Ipv6RoutePfxLength
                nextIpv6RoutePfxLength
                Ipv6RouteIndex
                nextIpv6RouteIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
 /*** $$TRACE_PROCEDURE_NAME = nmhGetNextIpv6RouteTable ***/
 /*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetNextIndexIpv6RouteTable (tSNMP_OCTET_STRING_TYPE * pIpv6RouteDest,
                               tSNMP_OCTET_STRING_TYPE * pNextIpv6RouteDest,
                               INT4 i4Ipv6RoutePfxLength,
                               INT4 *pi4NextIpv6RoutePfxLength,
                               UINT4 u4Ipv6RouteIndex,
                               UINT4 *pu4NextIpv6RouteIndex)
{
    tNetIpv6RtInfo      NetIpv6RtInfo;
    UINT4               u4ContextId = 0;

    if (gIp6GblInfo.pIp6CurrCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    u4ContextId = gIp6GblInfo.pIp6CurrCxt->u4ContextId;
    IP6_TASK_UNLOCK ();
    RTM6_TASK_LOCK ();

    /* NOTE : If multipath best route option is supported, update this routine
     * to return the next alternate best route for the same prefix and prefix
     * len. Currently only one best route for each prefix is displayed. */

    MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));
    if (Rtm6ApiTrieGetNextBestRtInCxt (u4ContextId, (tIp6Addr *) (VOID *)
                                       pIpv6RouteDest->pu1_OctetList,
                                       (UINT1) i4Ipv6RoutePfxLength,
                                       u4Ipv6RouteIndex,
                                       &NetIpv6RtInfo) == RTM6_FAILURE)
    {
        /* No more best route. */
        RTM6_TASK_UNLOCK ();
        IP6_TASK_LOCK ();
        return SNMP_FAILURE;
    }

    Ip6AddrCopy ((tIp6Addr *) (VOID *) pNextIpv6RouteDest->pu1_OctetList,
                 &NetIpv6RtInfo.Ip6Dst);
    pNextIpv6RouteDest->i4_Length = IP6_ADDR_SIZE;
    *pi4NextIpv6RoutePfxLength = NetIpv6RtInfo.u1Prefixlen;
    *pu4NextIpv6RouteIndex = 1;
    RTM6_TASK_UNLOCK ();
    IP6_TASK_LOCK ();
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIpv6RouteIfIndex
 Input       :  The Indices
                Ipv6RouteDest
                Ipv6RoutePfxLength
                Ipv6RouteIndex

                The Object 
                retValIpv6RouteIfIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpv6RouteIfIndex ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetIpv6RouteIfIndex (tSNMP_OCTET_STRING_TYPE * pIpv6RouteDest,
                        INT4 i4Ipv6RoutePfxLength, UINT4 u4Ipv6RouteIndex,
                        INT4 *pi4RetValIpv6RouteIfIndex)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RoutePfxLength = %d\n", i4Ipv6RoutePfxLength); ***/
   /*** $$TRACE_LOG (ENTRY,"Ipv6RouteIndex = %u\n", u4Ipv6RouteIndex); ***/
    tIp6Addr            ip6Addr;
    tNetIpv6RtInfo      NetIpv6RtInfo;
    UINT4               u4ContextId = 0;

    if (gIp6GblInfo.pIp6CurrCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    u4ContextId = gIp6GblInfo.pIp6CurrCxt->u4ContextId;
    IP6_TASK_UNLOCK ();
    RTM6_TASK_LOCK ();

    Ip6AddrCopy (&ip6Addr, (tIp6Addr *) (VOID *) pIpv6RouteDest->pu1_OctetList);

    MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));
    if (Rtm6ApiGetBestRouteEntryInCxt
        (u4ContextId, &ip6Addr, (UINT1) i4Ipv6RoutePfxLength, u4Ipv6RouteIndex,
         &NetIpv6RtInfo) == RTM6_FAILURE)
    {
        RTM6_TASK_UNLOCK ();
        IP6_TASK_LOCK ();
        return SNMP_FAILURE;
    }
    RTM6_TASK_UNLOCK ();
    IP6_TASK_LOCK ();
    *pi4RetValIpv6RouteIfIndex = (INT4) NetIpv6RtInfo.u4Index;
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetIpv6RouteNextHop
 Input       :  The Indices
                Ipv6RouteDest
                Ipv6RoutePfxLength
                Ipv6RouteIndex

                The Object 
                retValIpv6RouteNextHop
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpv6RouteNextHop ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetIpv6RouteNextHop (tSNMP_OCTET_STRING_TYPE * pIpv6RouteDest,
                        INT4 i4Ipv6RoutePfxLength, UINT4 u4Ipv6RouteIndex,
                        tSNMP_OCTET_STRING_TYPE * pRetValIpv6RouteNextHop)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RoutePfxLength = %d\n", i4Ipv6RoutePfxLength); ***/
   /*** $$TRACE_LOG (ENTRY,"Ipv6RouteIndex = %u\n", u4Ipv6RouteIndex); ***/
    tIp6Addr            ip6Addr;
    tNetIpv6RtInfo      NetIpv6RtInfo;
    UINT4               u4ContextId = 0;

    if (gIp6GblInfo.pIp6CurrCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    u4ContextId = gIp6GblInfo.pIp6CurrCxt->u4ContextId;
    IP6_TASK_UNLOCK ();
    RTM6_TASK_LOCK ();

    Ip6AddrCopy (&ip6Addr, (tIp6Addr *) (VOID *) pIpv6RouteDest->pu1_OctetList);

    MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));
    if (Rtm6ApiGetBestRouteEntryInCxt
        (u4ContextId, &ip6Addr, (UINT1) i4Ipv6RoutePfxLength, u4Ipv6RouteIndex,
         &NetIpv6RtInfo) == RTM6_FAILURE)
    {
        RTM6_TASK_UNLOCK ();
        IP6_TASK_LOCK ();
        return SNMP_FAILURE;
    }
    RTM6_TASK_UNLOCK ();
    IP6_TASK_LOCK ();
    Ip6AddrCopy ((tIp6Addr *) (VOID *) pRetValIpv6RouteNextHop->pu1_OctetList,
                 &NetIpv6RtInfo.NextHop);
    pRetValIpv6RouteNextHop->i4_Length = IP6_ADDR_SIZE;
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetIpv6RouteType
 Input       :  The Indices
                Ipv6RouteDest
                Ipv6RoutePfxLength
                Ipv6RouteIndex

                The Object 
                retValIpv6RouteType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpv6RouteType ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetIpv6RouteType (tSNMP_OCTET_STRING_TYPE * pIpv6RouteDest,
                     INT4 i4Ipv6RoutePfxLength, UINT4 u4Ipv6RouteIndex,
                     INT4 *pi4RetValIpv6RouteType)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RoutePfxLength = %d\n", i4Ipv6RoutePfxLength); ***/
   /*** $$TRACE_LOG (ENTRY,"Ipv6RouteIndex = %u\n", u4Ipv6RouteIndex); ***/
    tIp6Addr            ip6Addr;
    tNetIpv6RtInfo      NetIpv6RtInfo;
    UINT4               u4ContextId = 0;

    if (gIp6GblInfo.pIp6CurrCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    u4ContextId = gIp6GblInfo.pIp6CurrCxt->u4ContextId;
    IP6_TASK_UNLOCK ();
    RTM6_TASK_LOCK ();

    Ip6AddrCopy (&ip6Addr, (tIp6Addr *) (VOID *) pIpv6RouteDest->pu1_OctetList);

    MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));
    if (Rtm6ApiGetBestRouteEntryInCxt
        (u4ContextId, &ip6Addr, (UINT1) i4Ipv6RoutePfxLength, u4Ipv6RouteIndex,
         &NetIpv6RtInfo) == RTM6_FAILURE)
    {
        RTM6_TASK_UNLOCK ();
        IP6_TASK_LOCK ();
        return SNMP_FAILURE;
    }
    RTM6_TASK_UNLOCK ();
    IP6_TASK_LOCK ();

    /* Changes for conforming to the standard mib values */
    *pi4RetValIpv6RouteType = (INT4) NetIpv6RtInfo.i1Type;
    return SNMP_SUCCESS;
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetIpv6RouteProtocol
 Input       :  The Indices
                Ipv6RouteDest
                Ipv6RoutePfxLength
                Ipv6RouteIndex

                The Object 
                retValIpv6RouteProtocol
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpv6RouteProtocol ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetIpv6RouteProtocol (tSNMP_OCTET_STRING_TYPE * pIpv6RouteDest,
                         INT4 i4Ipv6RoutePfxLength, UINT4 u4Ipv6RouteIndex,
                         INT4 *pi4RetValIpv6RouteProtocol)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RoutePfxLength = %d\n", i4Ipv6RoutePfxLength); ***/
   /*** $$TRACE_LOG (ENTRY,"Ipv6RouteIndex = %u\n", u4Ipv6RouteIndex); ***/
    tIp6Addr            ip6Addr;
    tNetIpv6RtInfo      NetIpv6RtInfo;
    UINT4               u4ContextId = 0;

    if (gIp6GblInfo.pIp6CurrCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    u4ContextId = gIp6GblInfo.pIp6CurrCxt->u4ContextId;
    IP6_TASK_UNLOCK ();
    RTM6_TASK_LOCK ();

    Ip6AddrCopy (&ip6Addr, (tIp6Addr *) (VOID *) pIpv6RouteDest->pu1_OctetList);

    MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));
    if (Rtm6ApiGetBestRouteEntryInCxt
        (u4ContextId, &ip6Addr, (UINT1) i4Ipv6RoutePfxLength, u4Ipv6RouteIndex,
         &NetIpv6RtInfo) == RTM6_FAILURE)
    {
        RTM6_TASK_UNLOCK ();
        IP6_TASK_LOCK ();
        return SNMP_FAILURE;
    }
    RTM6_TASK_UNLOCK ();
    IP6_TASK_LOCK ();
    *pi4RetValIpv6RouteProtocol = (INT4) NetIpv6RtInfo.i1Proto;
    return SNMP_SUCCESS;
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetIpv6RoutePolicy
 Input       :  The Indices
                Ipv6RouteDest
                Ipv6RoutePfxLength
                Ipv6RouteIndex

                The Object 
                retValIpv6RoutePolicy
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpv6RoutePolicy ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetIpv6RoutePolicy (tSNMP_OCTET_STRING_TYPE * pIpv6RouteDest,
                       INT4 i4Ipv6RoutePfxLength, UINT4 u4Ipv6RouteIndex,
                       INT4 *pi4RetValIpv6RoutePolicy)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RoutePfxLength = %d\n", i4Ipv6RoutePfxLength); ***/
   /*** $$TRACE_LOG (ENTRY,"Ipv6RouteIndex = %u\n", u4Ipv6RouteIndex); ***/
    tIp6Addr            ip6Addr;
    tNetIpv6RtInfo      NetIpv6RtInfo;
    UINT4               u4ContextId = 0;

    if (gIp6GblInfo.pIp6CurrCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    u4ContextId = gIp6GblInfo.pIp6CurrCxt->u4ContextId;
    IP6_TASK_UNLOCK ();
    RTM6_TASK_LOCK ();

    Ip6AddrCopy (&ip6Addr, (tIp6Addr *) (VOID *) pIpv6RouteDest->pu1_OctetList);

    MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));
    if (Rtm6ApiGetBestRouteEntryInCxt
        (u4ContextId, &ip6Addr, (UINT1) i4Ipv6RoutePfxLength, u4Ipv6RouteIndex,
         &NetIpv6RtInfo) == RTM6_FAILURE)
    {
        RTM6_TASK_UNLOCK ();
        IP6_TASK_LOCK ();
        return SNMP_FAILURE;
    }
    RTM6_TASK_UNLOCK ();
    IP6_TASK_LOCK ();
    *pi4RetValIpv6RoutePolicy = 0;
    return SNMP_SUCCESS;
  /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetIpv6RouteAge
 Input       :  The Indices
                Ipv6RouteDest
                Ipv6RoutePfxLength
                Ipv6RouteIndex

                The Object 
                retValIpv6RouteAge
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpv6RouteAge ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetIpv6RouteAge (tSNMP_OCTET_STRING_TYPE * pIpv6RouteDest,
                    INT4 i4Ipv6RoutePfxLength, UINT4 u4Ipv6RouteIndex,
                    UINT4 *pu4RetValIpv6RouteAge)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RoutePfxLength = %d\n", i4Ipv6RoutePfxLength); ***/
   /*** $$TRACE_LOG (ENTRY,"Ipv6RouteIndex = %u\n", u4Ipv6RouteIndex); ***/
    tNetIpv6RtInfo      NetIpv6RtInfo;
    tIp6Addr            ip6Addr;
    UINT4               u4SysTime = 0;
    UINT4               u4ContextId = 0;

    if (gIp6GblInfo.pIp6CurrCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    u4ContextId = gIp6GblInfo.pIp6CurrCxt->u4ContextId;
    IP6_TASK_UNLOCK ();
    RTM6_TASK_LOCK ();

    Ip6AddrCopy (&ip6Addr, (tIp6Addr *) (VOID *) pIpv6RouteDest->pu1_OctetList);

    MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));
    if (Rtm6ApiGetBestRouteEntryInCxt
        (u4ContextId, &ip6Addr, (UINT1) i4Ipv6RoutePfxLength, u4Ipv6RouteIndex,
         &NetIpv6RtInfo) == RTM6_FAILURE)
    {
        RTM6_TASK_UNLOCK ();
        IP6_TASK_LOCK ();
        return SNMP_FAILURE;
    }
    RTM6_TASK_UNLOCK ();
    IP6_TASK_LOCK ();
    if ((NetIpv6RtInfo.i1Proto == IP6_NETMGMT_PROTOID) ||
        (NetIpv6RtInfo.i1Proto == IP6_LOCAL_PROTOID))
    {
        *pu4RetValIpv6RouteAge = 0;
        return SNMP_SUCCESS;
    }

    u4SysTime = OsixGetSysUpTime ();
    *pu4RetValIpv6RouteAge = (u4SysTime - NetIpv6RtInfo.u4ChangeTime);
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetIpv6RouteNextHopRDI
 Input       :  The Indices
                Ipv6RouteDest
                Ipv6RoutePfxLength
                Ipv6RouteIndex

                The Object 
                retValIpv6RouteNextHopRDI
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpv6RouteNextHopRDI ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetIpv6RouteNextHopRDI (tSNMP_OCTET_STRING_TYPE * pIpv6RouteDest,
                           INT4 i4Ipv6RoutePfxLength, UINT4 u4Ipv6RouteIndex,
                           UINT4 *pu4RetValIpv6RouteNextHopRDI)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RoutePfxLength = %d\n", i4Ipv6RoutePfxLength); ***/
   /*** $$TRACE_LOG (ENTRY,"Ipv6RouteIndex = %u\n", u4Ipv6RouteIndex); ***/
    tIp6Addr            ip6Addr;
    tNetIpv6RtInfo      NetIpv6RtInfo;
    UINT4               u4ContextId = 0;

    if (gIp6GblInfo.pIp6CurrCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    u4ContextId = gIp6GblInfo.pIp6CurrCxt->u4ContextId;
    IP6_TASK_UNLOCK ();
    RTM6_TASK_LOCK ();

    Ip6AddrCopy (&ip6Addr, (tIp6Addr *) (VOID *) pIpv6RouteDest->pu1_OctetList);

    MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));
    if (Rtm6ApiGetBestRouteEntryInCxt
        (u4ContextId, &ip6Addr, (UINT1) i4Ipv6RoutePfxLength, u4Ipv6RouteIndex,
         &NetIpv6RtInfo) == RTM6_FAILURE)
    {
        RTM6_TASK_UNLOCK ();
        IP6_TASK_LOCK ();
        return SNMP_FAILURE;
    }
    RTM6_TASK_UNLOCK ();
    IP6_TASK_LOCK ();
    *pu4RetValIpv6RouteNextHopRDI = 0x0;
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetIpv6RouteMetric
 Input       :  The Indices
                Ipv6RouteDest
                Ipv6RoutePfxLength
                Ipv6RouteIndex

                The Object 
                retValIpv6RouteMetric
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpv6RouteMetric ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetIpv6RouteMetric (tSNMP_OCTET_STRING_TYPE * pIpv6RouteDest,
                       INT4 i4Ipv6RoutePfxLength, UINT4 u4Ipv6RouteIndex,
                       UINT4 *pu4RetValIpv6RouteMetric)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RoutePfxLength = %d\n", i4Ipv6RoutePfxLength); ***/
   /*** $$TRACE_LOG (ENTRY,"Ipv6RouteIndex = %u\n", u4Ipv6RouteIndex); ***/
    tIp6Addr            ip6Addr;
    tNetIpv6RtInfo      NetIpv6RtInfo;
    UINT4               u4ContextId = 0;

    if (gIp6GblInfo.pIp6CurrCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    u4ContextId = gIp6GblInfo.pIp6CurrCxt->u4ContextId;
    IP6_TASK_UNLOCK ();
    RTM6_TASK_LOCK ();

    MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));
    Ip6AddrCopy (&ip6Addr, (tIp6Addr *) (VOID *) pIpv6RouteDest->pu1_OctetList);
    if (Rtm6ApiGetBestRouteEntryInCxt
        (u4ContextId, &ip6Addr, (UINT1) i4Ipv6RoutePfxLength, u4Ipv6RouteIndex,
         &NetIpv6RtInfo) == RTM6_FAILURE)

    {
        RTM6_TASK_UNLOCK ();
        IP6_TASK_LOCK ();
        return SNMP_FAILURE;
    }
    RTM6_TASK_UNLOCK ();
    IP6_TASK_LOCK ();
    *pu4RetValIpv6RouteMetric = NetIpv6RtInfo.u4Metric;
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetIpv6RouteWeight
 Input       :  The Indices
                Ipv6RouteDest
                Ipv6RoutePfxLength
                Ipv6RouteIndex

                The Object 
                retValIpv6RouteWeight
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpv6RouteWeight ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetIpv6RouteWeight (tSNMP_OCTET_STRING_TYPE * pIpv6RouteDest,
                       INT4 i4Ipv6RoutePfxLength, UINT4 u4Ipv6RouteIndex,
                       UINT4 *pu4RetValIpv6RouteWeight)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RoutePfxLength = %d\n", i4Ipv6RoutePfxLength); ***/
   /*** $$TRACE_LOG (ENTRY,"Ipv6RouteIndex = %u\n", u4Ipv6RouteIndex); ***/
    tIp6Addr            ip6Addr;
    tNetIpv6RtInfo      NetIpv6RtInfo;
    UINT4               u4ContextId = 0;

    if (gIp6GblInfo.pIp6CurrCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    u4ContextId = gIp6GblInfo.pIp6CurrCxt->u4ContextId;
    IP6_TASK_UNLOCK ();
    RTM6_TASK_LOCK ();

    MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));
    Ip6AddrCopy (&ip6Addr, (tIp6Addr *) (VOID *) pIpv6RouteDest->pu1_OctetList);
    if (Rtm6ApiGetBestRouteEntryInCxt
        (u4ContextId, &ip6Addr, (UINT1) i4Ipv6RoutePfxLength, u4Ipv6RouteIndex,
         &NetIpv6RtInfo) == RTM6_FAILURE)
    {
        RTM6_TASK_UNLOCK ();
        IP6_TASK_LOCK ();
        return SNMP_FAILURE;
    }
    RTM6_TASK_UNLOCK ();
    IP6_TASK_LOCK ();
    *pu4RetValIpv6RouteWeight = 0;
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetIpv6RouteInfo
 Input       :  The Indices
                Ipv6RouteDest
                Ipv6RoutePfxLength
                Ipv6RouteIndex

                The Object 
                retValIpv6RouteInfo
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpv6RouteInfo ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetIpv6RouteInfo (tSNMP_OCTET_STRING_TYPE * pIpv6RouteDest,
                     INT4 i4Ipv6RoutePfxLength, UINT4 u4Ipv6RouteIndex,
                     tSNMP_OID_TYPE * pRetValIpv6RouteInfo)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RoutePfxLength = %d\n", i4Ipv6RoutePfxLength); ***/
   /*** $$TRACE_LOG (ENTRY,"Ipv6RouteIndex = %u\n", u4Ipv6RouteIndex); ***/
    tIp6Addr            ip6Addr;
    UINT4               au4Oid[10] = { 0, 0 };
    tNetIpv6RtInfo      NetIpv6RtInfo;
    UINT4               u4ContextId = 0;

    if (gIp6GblInfo.pIp6CurrCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    u4ContextId = gIp6GblInfo.pIp6CurrCxt->u4ContextId;
    IP6_TASK_UNLOCK ();
    RTM6_TASK_LOCK ();

    MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));
    Ip6AddrCopy (&ip6Addr, (tIp6Addr *) (VOID *) pIpv6RouteDest->pu1_OctetList);
    if (Rtm6ApiGetBestRouteEntryInCxt
        (u4ContextId, &ip6Addr, (UINT1) i4Ipv6RoutePfxLength, u4Ipv6RouteIndex,
         &NetIpv6RtInfo) == RTM6_FAILURE)
    {
        RTM6_TASK_UNLOCK ();
        IP6_TASK_LOCK ();
        return SNMP_FAILURE;
    }
    RTM6_TASK_UNLOCK ();
    IP6_TASK_LOCK ();
    MEMCPY (pRetValIpv6RouteInfo->pu4_OidList, au4Oid, 2 * sizeof (UINT4));
    pRetValIpv6RouteInfo->u4_Length = 2;
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetIpv6RouteValid
 Input       :  The Indices
                Ipv6RouteDest
                Ipv6RoutePfxLength
                Ipv6RouteIndex

                The Object 
                retValIpv6RouteValid
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpv6RouteValid ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetIpv6RouteValid (tSNMP_OCTET_STRING_TYPE * pIpv6RouteDest,
                      INT4 i4Ipv6RoutePfxLength, UINT4 u4Ipv6RouteIndex,
                      INT4 *pi4RetValIpv6RouteValid)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RoutePfxLength = %d\n", i4Ipv6RoutePfxLength); ***/
   /*** $$TRACE_LOG (ENTRY,"Ipv6RouteIndex = %u\n", u4Ipv6RouteIndex); ***/
    tIp6Addr            ip6Addr;
    tNetIpv6RtInfo      NetIpv6RtInfo;
    UINT4               u4ContextId = 0;

    if (gIp6GblInfo.pIp6CurrCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    u4ContextId = gIp6GblInfo.pIp6CurrCxt->u4ContextId;
    IP6_TASK_UNLOCK ();
    RTM6_TASK_LOCK ();

    MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));
    Ip6AddrCopy (&ip6Addr, (tIp6Addr *) (VOID *) pIpv6RouteDest->pu1_OctetList);
    if (Rtm6ApiGetBestRouteEntryInCxt
        (u4ContextId, &ip6Addr, (UINT1) i4Ipv6RoutePfxLength, u4Ipv6RouteIndex,
         &NetIpv6RtInfo) == RTM6_FAILURE)
    {
        RTM6_TASK_UNLOCK ();
        IP6_TASK_LOCK ();
        return SNMP_FAILURE;
    }
    RTM6_TASK_UNLOCK ();
    IP6_TASK_LOCK ();
    *pi4RetValIpv6RouteValid = TRUE;
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIpv6RouteValid
 Input       :  The Indices
                Ipv6RouteDest
                Ipv6RoutePfxLength
                Ipv6RouteIndex

                The Object 
                setValIpv6RouteValid
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetIpv6RouteValid ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetIpv6RouteValid (tSNMP_OCTET_STRING_TYPE * pIpv6RouteDest,
                      INT4 i4Ipv6RoutePfxLength, UINT4 u4Ipv6RouteIndex,
                      INT4 i4SetValIpv6RouteValid)
{
    tIp6Addr           *pDest = (tIp6Addr *) (VOID *) pIpv6RouteDest->
        pu1_OctetList;
    UINT4               u4ContextId = 0;
    tNetIpv6RtInfo      NetIpv6RtInfo;

    if (gIp6GblInfo.pIp6CurrCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    u4ContextId = gIp6GblInfo.pIp6CurrCxt->u4ContextId;
    IP6_TASK_UNLOCK ();
    RTM6_TASK_LOCK ();

    MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));
    if (Rtm6ApiGetBestRouteEntryInCxt
        (u4ContextId, pDest, (UINT1) i4Ipv6RoutePfxLength, u4Ipv6RouteIndex,
         &NetIpv6RtInfo) == RTM6_SUCCESS)
    {
        /* Route is present. */
        if (i4SetValIpv6RouteValid == NETIPV6_DELETE_ROUTE)
        {
            /* Invalidate the route. */
            NetIpv6RtInfo.u4RowStatus = IP6FWD_DESTROY;
            NetIpv6RtInfo.u2ChgBit = IP6_BIT_STATUS;

            Rtm6UtilIpv6LeakRoute (NETIPV6_DELETE_ROUTE, &NetIpv6RtInfo);
        }
        RTM6_TASK_UNLOCK ();
        IP6_TASK_LOCK ();
        return SNMP_SUCCESS;
    }
    RTM6_TASK_UNLOCK ();
    IP6_TASK_LOCK ();

    /* No matching route found. */
    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ipv6RouteValid
 Input       :  The Indices
                Ipv6RouteDest
                Ipv6RoutePfxLength
                Ipv6RouteIndex

                The Object 
                testValIpv6RouteValid
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2Ipv6RouteValid ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2Ipv6RouteValid (UINT4 *pu4ErrorCode,
                         tSNMP_OCTET_STRING_TYPE * pIpv6RouteDest,
                         INT4 i4Ipv6RoutePfxLength, UINT4 u4Ipv6RouteIndex,
                         INT4 i4TestValIpv6RouteValid)
{
    tIp6Addr           *pDest = (tIp6Addr *) (VOID *)
        pIpv6RouteDest->pu1_OctetList;
    tNetIpv6RtInfo      NetIpv6RtInfo;
    UINT4               u4ContextId = 0;

    if (gIp6GblInfo.pIp6CurrCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    u4ContextId = gIp6GblInfo.pIp6CurrCxt->u4ContextId;

    if ((i4TestValIpv6RouteValid != IP6_TRUE) &&
        (i4TestValIpv6RouteValid != IP6_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    IP6_TASK_UNLOCK ();
    RTM6_TASK_LOCK ();
    MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));
    if (Rtm6ApiGetBestRouteEntryInCxt
        (u4ContextId, pDest, (UINT1) i4Ipv6RoutePfxLength, u4Ipv6RouteIndex,
         &NetIpv6RtInfo) == RTM6_FAILURE)
    {
        /* No matching route found. Cant perform any operation for this
         * route. */
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        RTM6_TASK_UNLOCK ();
        IP6_TASK_LOCK ();
        return SNMP_FAILURE;
    }
    RTM6_TASK_UNLOCK ();
    IP6_TASK_LOCK ();
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ipv6RouteTable
 Input       :  The Indices
                Ipv6RouteDest
                Ipv6RoutePfxLength
                Ipv6RouteIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ipv6RouteTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ipv6NetToMediaTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIpv6NetToMediaTable
 Input       :  The Indices
                Ipv6IfIndex
                Ipv6NetToMediaNetAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhValidateIndexInstanceIpv6NetToMediaTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhValidateIndexInstanceIpv6NetToMediaTable (INT4 i4Ipv6IfIndex,
                                             tSNMP_OCTET_STRING_TYPE *
                                             pIpv6NetToMediaNetAddress)
{
    if (nmhValidateIndexInstanceFsipv6NdLanCacheTable
        (i4Ipv6IfIndex, pIpv6NetToMediaNetAddress) == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIpv6NetToMediaTable
 Input       :  The Indices
                Ipv6IfIndex
                Ipv6NetToMediaNetAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhGetFirstIpv6NetToMediaTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFirstIndexIpv6NetToMediaTable (INT4 *pi4Ipv6IfIndex,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pIpv6NetToMediaNetAddress)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6IfIndex = %d\n", *pi4Ipv6IfIndex); ***/
    if (nmhGetFirstIndexFsipv6NdLanCacheTable (pi4Ipv6IfIndex,
                                               pIpv6NetToMediaNetAddress) ==
        SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetNextIndexIpv6NetToMediaTable
 Input       :  The Indices
                Ipv6IfIndex
                nextIpv6IfIndex
                Ipv6NetToMediaNetAddress
                nextIpv6NetToMediaNetAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
 /*** $$TRACE_PROCEDURE_NAME = nmhGetNextIpv6NetToMediaTable ***/
 /*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetNextIndexIpv6NetToMediaTable (INT4 i4Ipv6IfIndex,
                                    INT4 *pi4NextIpv6IfIndex,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pIpv6NetToMediaNetAddress,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pNextIpv6NetToMediaNetAddress)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6IfIndex = %d\n", *pi4Ipv6IfIndex); ***/
    if (nmhGetNextIndexFsipv6NdLanCacheTable (i4Ipv6IfIndex,
                                              pi4NextIpv6IfIndex,
                                              pIpv6NetToMediaNetAddress,
                                              pNextIpv6NetToMediaNetAddress)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIpv6NetToMediaPhysAddress
 Input       :  The Indices
                Ipv6IfIndex
                Ipv6NetToMediaNetAddress

                The Object 
                retValIpv6NetToMediaPhysAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpv6NetToMediaPhysAddress ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetIpv6NetToMediaPhysAddress (INT4 i4Ipv6IfIndex,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pIpv6NetToMediaNetAddress,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pRetValIpv6NetToMediaPhysAddress)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6IfIndex = %d\n", i4Ipv6IfIndex); ***/

    /* Same data retrival logic is used in the invoked function */
    if (nmhGetFsipv6NdLanCachePhysAddr (i4Ipv6IfIndex,
                                        pIpv6NetToMediaNetAddress,
                                        pRetValIpv6NetToMediaPhysAddress)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetIpv6NetToMediaType
 Input       :  The Indices
                Ipv6IfIndex
                Ipv6NetToMediaNetAddress

                The Object 
                retValIpv6NetToMediaType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpv6NetToMediaType ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetIpv6NetToMediaType (INT4 i4Ipv6IfIndex,
                          tSNMP_OCTET_STRING_TYPE * pIpv6NetToMediaNetAddress,
                          INT4 *pi4RetValIpv6NetToMediaType)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6IfIndex = %d\n", i4Ipv6IfIndex); ***/
    tIp6If             *pIf6 = NULL;
    tNd6CacheEntry     *pNd6cEntry = NULL;

    pIf6 = Ip6ifGetEntry ((UINT2) i4Ipv6IfIndex);
    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }
    pNd6cEntry = Nd6IsCacheForAddr (pIf6,
                                    (tIp6Addr *) (VOID *)
                                    pIpv6NetToMediaNetAddress->pu1_OctetList);
    if (pNd6cEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    if ((pNd6cEntry->u1ReachState == ND6C_STATIC) ||
        (pNd6cEntry->u1ReachState == ND6C_STATIC_NOT_IN_SERVICE))
    {
        *pi4RetValIpv6NetToMediaType = ND6C_TYPE_STATIC;
    }
    else if (pNd6cEntry->u1ReachState == ND6C_REACHABLE)
    {
        *pi4RetValIpv6NetToMediaType = ND6C_TYPE_DYNAMIC;
    }
    else
    {
        *pi4RetValIpv6NetToMediaType = ND6C_TYPE_OTHER;
    }
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetIpv6IfNetToMediaState
 Input       :  The Indices
                Ipv6IfIndex
                Ipv6NetToMediaNetAddress

                The Object 
                retValIpv6IfNetToMediaState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpv6IfNetToMediaState ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetIpv6IfNetToMediaState (INT4 i4Ipv6IfIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pIpv6NetToMediaNetAddress,
                             INT4 *pi4RetValIpv6IfNetToMediaState)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6IfIndex = %d\n", i4Ipv6IfIndex); ***/
    tIp6If             *pIf6 = NULL;
    tNd6CacheEntry     *pNd6cEntry = NULL;

    pIf6 = Ip6ifGetEntry ((UINT2) i4Ipv6IfIndex);
    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }
    pNd6cEntry = Nd6IsCacheForAddr (pIf6,
                                    (tIp6Addr *) (VOID *)
                                    pIpv6NetToMediaNetAddress->pu1_OctetList);
    if (pNd6cEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    if (pNd6cEntry->u1ReachState == ND6C_STATIC ||
        pNd6cEntry->u1ReachState == ND6C_INCOMPLETE)
    {
        *pi4RetValIpv6IfNetToMediaState = ND6C_REACHABLE;
    }
    else
    {
        *pi4RetValIpv6IfNetToMediaState = pNd6cEntry->u1ReachState;
    }
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetIpv6IfNetToMediaLastUpdated
 Input       :  The Indices
                Ipv6IfIndex
                Ipv6NetToMediaNetAddress

                The Object 
                retValIpv6IfNetToMediaLastUpdated
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpv6IfNetToMediaLastUpdated ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetIpv6IfNetToMediaLastUpdated (INT4 i4Ipv6IfIndex,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pIpv6NetToMediaNetAddress,
                                   UINT4 *pu4RetValIpv6IfNetToMediaLastUpdated)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6IfIndex = %d\n", i4Ipv6IfIndex); ***/
    tIp6If             *pIf6 = NULL;
    tNd6CacheEntry     *pNd6cEntry = NULL;

    pIf6 = Ip6ifGetEntry ((UINT2) i4Ipv6IfIndex);
    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }
    pNd6cEntry = Nd6IsCacheForAddr (pIf6,
                                    (tIp6Addr *) (VOID *)
                                    pIpv6NetToMediaNetAddress->pu1_OctetList);
    if (pNd6cEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValIpv6IfNetToMediaLastUpdated = pNd6cEntry->u4LastUseTime;
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetIpv6NetToMediaValid
 Input       :  The Indices
                Ipv6IfIndex
                Ipv6NetToMediaNetAddress

                The Object 
                retValIpv6NetToMediaValid
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpv6NetToMediaValid ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetIpv6NetToMediaValid (INT4 i4Ipv6IfIndex,
                           tSNMP_OCTET_STRING_TYPE * pIpv6NetToMediaNetAddress,
                           INT4 *pi4RetValIpv6NetToMediaValid)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6IfIndex = %d\n", i4Ipv6IfIndex); ***/
    tIp6If             *pIf6 = NULL;
    tNd6CacheEntry     *pNd6cEntry = NULL;

    pIf6 = Ip6ifGetEntry ((UINT2) i4Ipv6IfIndex);
    if (pIf6 == NULL)
    {
        return SNMP_FAILURE;
    }
    pNd6cEntry = Nd6IsCacheForAddr (pIf6,
                                    (tIp6Addr *) (VOID *)
                                    pIpv6NetToMediaNetAddress->pu1_OctetList);
    if (pNd6cEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValIpv6NetToMediaValid = TRUE;
    return SNMP_SUCCESS;
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIpv6NetToMediaValid
 Input       :  The Indices
                Ipv6IfIndex
                Ipv6NetToMediaNetAddress

                The Object 
                setValIpv6NetToMediaValid
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetIpv6NetToMediaValid ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetIpv6NetToMediaValid (INT4 i4Ipv6IfIndex,
                           tSNMP_OCTET_STRING_TYPE * pIpv6NetToMediaNetAddress,
                           INT4 i4SetValIpv6NetToMediaValid)
{
    tNd6CacheEntry     *pNdcEntry = NULL;
    tIp6Addr           *pAddr6 =
        (tIp6Addr *) (VOID *) pIpv6NetToMediaNetAddress->pu1_OctetList;
    INT4                i4EntryType = 0;
    UINT1               au1Mac[MAC_ADDR_LEN] = { 0, 0, 0, 0, 0, 0 };
    UINT1               u1LlaLen = MAC_ADDR_LEN;
    UINT1               u1State = ND6C_TYPE_STATIC;    /*static */
    INT1                i1RetVal = SNMP_FAILURE;

    tIp6If             *pIf6 = Ip6GetIfFromIndex (i4Ipv6IfIndex);

    if (pIf6 == NULL)
    {
        return i1RetVal;
    }
    if (i4SetValIpv6NetToMediaValid == TRUE)
    {
        if ((pNdcEntry =
             Nd6CreateCache (pIf6, pAddr6, au1Mac, u1LlaLen, u1State,
                             0)) != NULL)
        {
            i4EntryType = IPVX_NET_TO_PHY_TYPE_STATIC;
            i1RetVal = SNMP_SUCCESS;
        }
    }
    else
    {
        if ((pNdcEntry = Nd6IsCacheForAddr (pIf6, pAddr6)) != NULL)
        {
            if (Nd6PurgeCache (pNdcEntry) == IP6_SUCCESS)
            {
                i4EntryType = IPVX_NET_TO_PHY_TYPE_INVALID;
                i1RetVal = SNMP_SUCCESS;
            }
        }
    }
    if (i1RetVal == SNMP_SUCCESS)
    {
        IncMsrForIpv6NetToPhyTable (i4Ipv6IfIndex, pIpv6NetToMediaNetAddress,
                                    'i', &i4EntryType,
                                    FsMIStdIpNetToPhysicalType,
                                    (sizeof (FsMIStdIpNetToPhysicalType)
                                     / sizeof (UINT4)), FALSE);
    }
    UNUSED_PARAM (pNdcEntry);
    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ipv6NetToMediaValid
 Input       :  The Indices
                Ipv6IfIndex
                Ipv6NetToMediaNetAddress

                The Object 
                testValIpv6NetToMediaValid
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2Ipv6NetToMediaValid ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2Ipv6NetToMediaValid (UINT4 *pu4ErrorCode, INT4 i4Ipv6IfIndex,
                              tSNMP_OCTET_STRING_TYPE *
                              pIpv6NetToMediaNetAddress,
                              INT4 i4TestValIpv6NetToMediaValid)
{
    tNd6CacheEntry     *pNdcEntry = NULL;
    tIp6Addr           *pAddr6 =
        (tIp6Addr *) (VOID *) pIpv6NetToMediaNetAddress->pu1_OctetList;
    tIp6If             *pIf6 = Ip6GetIfFromIndex (i4Ipv6IfIndex);

    if (pIf6 == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if (i4TestValIpv6NetToMediaValid != IP6_TRUE &&
        i4TestValIpv6NetToMediaValid != IP6_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((pNdcEntry = Nd6IsCacheForAddr (pIf6, pAddr6)) == NULL)
    {
        if (i4TestValIpv6NetToMediaValid == IP6_FALSE)
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            return SNMP_FAILURE;
        }
    }
    UNUSED_PARAM (pNdcEntry);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ipv6NetToMediaTable
 Input       :  The Indices
                Ipv6IfIndex
                Ipv6NetToMediaNetAddress
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ipv6NetToMediaTable (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIpv6Forwarding
 Input       :  The Indices

                The Object 
                retValIpv6Forwarding
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpv6Forwarding ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetIpv6Forwarding (INT4 *pi4RetValIpv6Forwarding)
{
    tIp6Cxt            *pIp6Cxt = NULL;

    pIp6Cxt = gIp6GblInfo.pIp6CurrCxt;

    if (pIp6Cxt == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValIpv6Forwarding = pIp6Cxt->u4ForwFlag;

    return SNMP_SUCCESS;
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetIpv6DefaultHopLimit
 Input       :  The Indices

                The Object 
                retValIpv6DefaultHopLimit
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpv6DefaultHopLimit ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetIpv6DefaultHopLimit (INT4 *pi4RetValIpv6DefaultHopLimit)
{
    tIp6Cxt            *pIp6Cxt = NULL;

    pIp6Cxt = gIp6GblInfo.pIp6CurrCxt;

    if (pIp6Cxt == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValIpv6DefaultHopLimit = pIp6Cxt->u4Ipv6HopLimit;
    return SNMP_SUCCESS;
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetIpv6Interfaces
 Input       :  The Indices

                The Object 
                retValIpv6Interfaces
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpv6Interfaces ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetIpv6Interfaces (UINT4 *pu4RetValIpv6Interfaces)
{
    UINT2               u2Index = 1, u2NumInterfaces = 0;
    tIp6If             *pIf6 = NULL;

    IP6_IF_SCAN (u2Index, IP6_MAX_LOGICAL_IF_INDEX)
    {
        pIf6 = gIp6GblInfo.apIp6If[u2Index];
        if ((pIf6 != NULL) && (pIf6->u1AdminStatus != ADMIN_INVALID))
        {
            u2NumInterfaces++;
        }
    }
    *pu4RetValIpv6Interfaces = u2NumInterfaces;
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetIpv6IfTableLastChange
 Input       :  The Indices

                The Object 
                retValIpv6IfTableLastChange
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpv6IfTableLastChange ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetIpv6IfTableLastChange (UINT4 *pu4RetValIpv6IfTableLastChange)
{

    *pu4RetValIpv6IfTableLastChange = gIp6GblInfo.u4Ip6IfTableLastChange;
    return SNMP_SUCCESS;
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetIpv6RouteNumber
 Input       :  The Indices

                The Object 
                retValIpv6RouteNumber
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpv6RouteNumber ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetIpv6RouteNumber (UINT4 *pu4RetValIpv6RouteNumber)
{
    UINT4               u4ContextId = 0;
    if (gIp6GblInfo.pIp6CurrCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    u4ContextId = gIp6GblInfo.pIp6CurrCxt->u4ContextId;
    Ip6GetFwdTableRouteNumInCxt (u4ContextId, pu4RetValIpv6RouteNumber);
    return SNMP_SUCCESS;
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetIpv6DiscardedRoutes
 Input       :  The Indices

                The Object 
                retValIpv6DiscardedRoutes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpv6DiscardedRoutes ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetIpv6DiscardedRoutes (UINT4 *pu4RetValIpv6DiscardedRoutes)
{
    tIp6Cxt            *pIp6Cxt = NULL;

    pIp6Cxt = gIp6GblInfo.pIp6CurrCxt;

    if (pIp6Cxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValIpv6DiscardedRoutes = pIp6Cxt->u4Ip6DiscaredRoutes;
    return SNMP_SUCCESS;
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIpv6Forwarding
 Input       :  The Indices

                The Object 
                setValIpv6Forwarding
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetIpv6Forwarding ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetIpv6Forwarding (INT4 i4SetValIpv6Forwarding)
{
    tIp6Cxt            *pIp6Cxt = NULL;
#ifdef NPAPI_WANTED
    INT4                i4RetVal = FNP_NOT_SUPPORTED;
#endif

    pIp6Cxt = gIp6GblInfo.pIp6CurrCxt;

    if (pIp6Cxt == NULL)
    {
        return SNMP_FAILURE;
    }
   /*** $$TRACE_LOG (ENTRY, "Ipv6Forwarding = %d\n", i4SetValIpv6Forwarding); ***/
    if (pIp6Cxt->u4ForwFlag == (UINT4) i4SetValIpv6Forwarding)
    {
        /* No change in forwarding Status. */
        return SNMP_SUCCESS;
    }
    Ip6SetIpv6ForwardingInCxt (pIp6Cxt, (UINT4) i4SetValIpv6Forwarding);

#ifdef NPAPI_WANTED
    Nd6AddOrDeleteCacheEntriesInCxt (pIp6Cxt->u4ContextId,
                                     i4SetValIpv6Forwarding);
    i4RetVal =
        Ipv6FsNpIpv6UcRouting (pIp6Cxt->u4ContextId,
                               (UINT4) i4SetValIpv6Forwarding);
    if (i4RetVal == FNP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    else if (i4RetVal == FNP_NOT_SUPPORTED)
    {
        Rtm6ApiAddOrDelAllRtsInCxt (pIp6Cxt->u4ContextId,
                                    i4SetValIpv6Forwarding);
    }
#endif
    IncMsrForIpvxGlbTable (pIp6Cxt->u4ContextId, i4SetValIpv6Forwarding,
                           FsMIStdIpv6IpForwarding,
                           (sizeof (FsMIStdIpv6IpForwarding) / sizeof (UINT4)));
    return SNMP_SUCCESS;
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhSetIpv6DefaultHopLimit
 Input       :  The Indices

                The Object 
                setValIpv6DefaultHopLimit
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetIpv6DefaultHopLimit ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetIpv6DefaultHopLimit (INT4 i4SetValIpv6DefaultHopLimit)
{
    tIp6Cxt            *pIp6Cxt = NULL;

    pIp6Cxt = gIp6GblInfo.pIp6CurrCxt;

    if (pIp6Cxt == NULL)
    {
        return SNMP_FAILURE;
    }
   /*** $$TRACE_LOG (ENTRY, "Ipv6DefaultHopLimit = %d\n", i4SetValIpv6DefaultHopLimit); ***/

    pIp6Cxt->u4Ipv6HopLimit = i4SetValIpv6DefaultHopLimit;
    IncMsrForIpvxGlbTable (pIp6Cxt->u4ContextId, i4SetValIpv6DefaultHopLimit,
                           FsMIStdIpv6IpDefaultHopLimit,
                           (sizeof (FsMIStdIpv6IpDefaultHopLimit) /
                            sizeof (UINT4)));
    return SNMP_SUCCESS;
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ipv6Forwarding
 Input       :  The Indices

                The Object 
                testValIpv6Forwarding
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2Ipv6Forwarding ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2Ipv6Forwarding (UINT4 *pu4ErrorCode, INT4 i4TestValIpv6Forwarding)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6Forwarding = %d\n", i4TestValIpv6Forwarding); ***/
    if ((i4TestValIpv6Forwarding != IP6_FORW_ENABLE) &&
        i4TestValIpv6Forwarding != IP6_FORW_DISABLE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_INVALID_FORWARDING_STATUS);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhTestv2Ipv6DefaultHopLimit
 Input       :  The Indices

                The Object 
                testValIpv6DefaultHopLimit
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2Ipv6DefaultHopLimit ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2Ipv6DefaultHopLimit (UINT4 *pu4ErrorCode,
                              INT4 i4TestValIpv6DefaultHopLimit)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6DefaultHopLimit = %d\n", i4TestValIpv6DefaultHopLimit); ***/
    if ((i4TestValIpv6DefaultHopLimit < IP6_MIN_HOP_LIMIT)
        || (i4TestValIpv6DefaultHopLimit > IP6_MAX_HOP_LIMIT))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ipv6Forwarding
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ipv6Forwarding (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Ipv6DefaultHopLimit
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ipv6DefaultHopLimit (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/***************************** END OF FILE **********************************/
