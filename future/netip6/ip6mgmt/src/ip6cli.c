/*******************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ip6cli.c,v 1.157 2017/12/26 13:34:22 siva Exp $
 *
 * Description: Cli related functions are handled here.
 *
 *******************************************************************/

/********************************************************/
/*            HEADER FILES                              */
/********************************************************/

#ifndef __IP6CLI_C__
#define __IP6CLI_C__
#include "ip6ipvx.h"
#ifdef LNXIP6_WANTED
#include "lip6mgin.h"
#else /* FSIP */
#include "ip6mginc.h"
#include "fsmpipv6lw.h"
#endif
#include "ip6cli.h"
#include "cli.h"
#include "vcm.h"
#include "rtm6.h"
#include "rtm6tdfs.h"
#include "rtm6frt.h"
#include "ip6cliprot.h"
#include "fsmrt6lw.h"

UINT1               gau1ZeroPing6HostName[DNS_MAX_QUERY_LEN];
tIp6Addr            gZeroIp6Addr;
#ifndef LNXIP6_WANTED            /* FSIP */
PRIVATE INT4        Ip6ShowGetPrefix (UINT1 *pu1Buffer, UINT4 u4BufLen,
                                      tShowIp6PrefixCookie * pCookie);
#endif /* !LNXIP6_WANTED */

PRIVATE INT4        Ip6ShowGetInterface (tShowIp6Interface * pShowIp6Interface,
                                         INT4 i4Ipv6IfIndex);

PRIVATE INT4        Ip6ShowGetIntfAddr (UINT1 *pu1Buffer, UINT4 u4BufLen,
                                        UINT4 u4IfIndex,
                                        tShowIp6IntfAddrCookie * pCookie);

PRIVATE VOID
 
 
 
 Fsipv6IntPrefixProfileTableInfo (tCliHandle CliHandle, INT4 i4PrefixIndex,
                                  tSNMP_OCTET_STRING_TYPE * pPrefixAddrs,
                                  INT4 i4PrefixAddrLen);

extern INT1 nmhGetFsRtm6StaticRouteDistance ARG_LIST ((INT4 *));

#ifdef LNXIP6_WANTED
PRIVATE VOID        Fsipv6IntDefPrefixProfileTableInfo (tCliHandle CliHandle);
#endif /* LNXIP6_WANTED */

PRIVATE INT4        Ip6CliLock (VOID);
PRIVATE INT4        Ip6CliUnLock (VOID);
#ifndef LNXIP6_WANTED
PUBLIC UINT4        gu4CumulativeNdHwFailCount;
extern UINT4        gNoOfNSRetrial_1;
extern UINT4        gNoOfNSRetrial_2;
extern UINT4        gNoOfNSRetrial_3;
#endif
/* Array for Tunnel Mode */
const CHR1         *patunlMode[] = {
    NULL,                        /* 0 -> Invalid */
    "other",                    /* TNL_TYPE_OTHER = 1 */
    "direct",                    /* TNL_TYPE_DIRECT = 2 */
    "gre",                        /* TNL_TYPE_GRE = 3 */
    "minimal",                    /* TNL_TYPE_MINIMAL = 4 */
    "l2tp",                        /* TNL_TYPE_L2TP = 5  */
    "pptp",                        /* TNL_TYPE_PPTP = 6 */
    "l2f",                        /* TNL_TYPE_L2F = 7 */
    "udp",                        /* TNL_TYPE_UDP = 8 */
    "atmp",                        /* TNL_TYPE_ATMP = 9 */
    "msdp",                        /* TNL_TYPE_MSDP = 10 */
    "6to4",                        /* TNL_TYPE_SIXTOFOUR = 11 */
    "6over4",                    /* TNL_TYPE_SIXOVERFOUR = 12 */
    "isatap",                    /* TNL_TYPE_ISATAP = 13 */
    "teredo",                    /* TNL_TYPE_TEREDO = 14 */
    "compat",                    /* TNL_TYPE_COMPAT = 15 */
    "ipv6ip"                    /* TNL_TYPE_IPV6IP = 16 */
};

const CHR1         *paIp6Proto[] = {
    NULL,                        /* 0 -> Invalid */
    "Other",                    /* 1 - Aggregate Routes */
    "Connected",                /* 2 - Local Interface */
    "Static",                    /* 3 - Configured Static Route */
    "Ndisc",                    /* 4 - Redirect Routes */
    "RIPNG",                    /* 5 - RIPNG Routes */
    "OSFP",                        /* 6 - OSPF Routes */
    "BGP",                        /* 7 - BGP4+ Routes */
    "IDRP",                        /* 8 - IDRP Routes */
    "IGRP"                        /* 9 - IGRP Routes */
};

const CHR1         *pNdSecureMsgType[] = {
    NULL,                        /* Invalid */
    "NS",                        /* Neighbor Solicitation */
    "NA",                        /* Neighbor Advertisement */
    "RS",                        /* Router Solicitation */
    "RA",                        /* Router Advertisement */
    "RD",                        /* Redirect Message */
    "CPS",                        /* Certificate Path Solicitation */
    "CPA"                        /* Certificate Path Advertisement */
};

PRIVATE INT4
Ip6CliLock ()
{

    Ip6GLock ();
    Ip6Lock ();
    return SNMP_SUCCESS;
}

PRIVATE INT4
Ip6CliUnLock ()
{
    Ip6UnLock ();
    Ip6GUnLock ();
    return SNMP_SUCCESS;
}

/*********************************************************************
*  Function Name : cli_process_ip6_cmd() 
*  Description   : This function processes the IPv6 Related Commands ,
*                  Pre-validates the Inputs before sending to the
*                  IP6CLI Module , Fills up the IP6 Config Params
*                  Structure with the input values given by the User,
*                  fills up the Command in the Input Message, and
*                  calls Ip6CliCmdActionRoutine() for further Processing of
*                  the User Commands/Inputs.
*  Input(s)      : CliHandle 
*                  Command 
*                  User Inputs in va_alist. 
*  Output(s)     : None.
*  Return Values : None.
*********************************************************************/

#ifdef IP6_WANTED
VOID
cli_process_ip6_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT4              *args[IP6_CLI_MAX_ARGS];
    INT1                argno = IP6_ZERO;
    INT4                i4PrefixLen = IP6_ZERO;
    INT4                i4IfaceIndex = IP6_ZERO;
    UINT4               u4ErrorCode = IP6_ZERO;
    tIp6Addr            Ip6Addr;
    tIp6Addr            Ip6AddrTwo;
    tIp6Addr            Ip6AddrThree;
    tIp6Addr            Ip6RARouteInfo;
    UINT1               au1PhyMacAddr[6];
    INT4                i4PingCount = IP6_ZERO;
    INT4                i4PingSize = IP6_ZERO;
    INT4                i4PingTimeout = IP6_ZERO;
    INT4                i4LifeTime;
    UINT1              *pu1PingData = NULL;
    tIp6Addr            PingSrcAddr;
    INT4                i4DefaultFlag = IP6_ZERO;
    INT4                i4ShowAllVlan;
    UINT4               u4PrefixPreferredTime = IP6_ADDR_PROF_DEF_PREF_LIFE;
    UINT4               u4PrefixValidTime = IP6_ADDR_PROF_DEF_VALID_LIFE;
    UINT1               u1PrefixValidTimeFlag = IP6_FIXED_TIME;
    UINT1               u1PrefixPrefTimeFlag = IP6_FIXED_TIME;
    UINT1               u1NoAdvertiseFlag = IP6_ZERO;
    UINT1               u1OnLinkStatus = IP6_ZERO;
    UINT1               u1AutoConfigStatus = IP6_ZERO;
    UINT1               u1SupportEmbeddedRp = IP6_SUPPORT_EMBD_RP_DISABLE;
    UINT1               u1ArgIndex;
    UINT1               u1VlanIdArgIndex;
    INT4                i4RetStatus = CLI_FAILURE;
    INT4                i4RetValue = 0;
    INT4                i4RetValFsipv6IfSENDSecStatus = 0;
    UINT1               au1IfName[CFA_CLI_MAX_IF_NAME_LEN];
    UINT1               u1HCFlag;
    UINT1              *pu1IpCxtName = NULL;
    UINT1               au1PingData[IP6_PING_DATA_SIZE];
    UINT1               u1Scope = 0;    /*Added for Scoped Address Architecture-RFC4007 */
    INT4                i4ZoneId = 0;    /*zone id being configured RFC4007 */
    UINT4               u4VcId = 0;
    UINT4               u4Label = 0;
    UINT4               u4Precedence = 0;
    tSNMP_OCTET_STRING_TYPE Fsipv6IfToken;
    tSNMP_OCTET_STRING_TYPE Fsipv6GetIfToken;
    tSNMP_OCTET_STRING_TYPE Fsipv6DomNameOne;
    tSNMP_OCTET_STRING_TYPE Fsipv6DomNameTwo;
    tSNMP_OCTET_STRING_TYPE Fsipv6DomNameThree;
    UINT1               au1Ip6IfId[IP6_EUI_ADDRESS_LEN];
    UINT1               au1Ip6GetIfId[IP6_EUI_ADDRESS_LEN];
    UINT1               au1ScopeZone[IP6_SCOPE_ZONE_NAME_LEN];    /* the zone name being configured RFC 4007 */
    UINT1               au1Tmp[IP6_SCOPE_ZONE_NAME_LEN + 5];
    UINT1              *pu1ScopeZone = NULL;
    UINT1              *pPingAddr = NULL;
    UINT1              *pHostName = NULL;
    UINT1              *pPingInterface = NULL;
    UINT1               u1PingIntLen = 0;
    UINT1               u1PingLen = 0;
    UINT1               u1AddrScope = 0;
    INT4                i4Inst = 0;
    UINT1               au1PingZoneId[CFA_CLI_MAX_IF_NAME_LEN];
    UINT1              *pu1PingZoneId = NULL;
    UINT2               u2Len = 0;
    UINT1               au1HostName[DNS_MAX_QUERY_LEN];
    INT1                i1PagingFlag = OSIX_FALSE;
    UINT1               au1Ipv6DomNameOneBuf[IP6_DOMAIN_NAME_LEN + 1];
    UINT1               au1Ipv6DomNameTwoBuf[IP6_DOMAIN_NAME_LEN + 1];
    UINT1               au1Ipv6DomNameThreeBuf[IP6_DOMAIN_NAME_LEN + 1];
    UINT4               i4RARoutePreference = 0;
    UINT4               u4RARouteLifetime = 0;
    INT4                i4RARoutPrefixLen = 0;
    UINT4               i4DefRoutePreference = 0;
    INT4                i4RAIfIndex = 0;
    UINT4               u4RaMtu = 0;
    CLI_SET_CMD_STATUS (CLI_SUCCESS);

    UNUSED_PARAM (i4RetStatus);
    UNUSED_PARAM (i4RetValue);

    MEMSET (au1Tmp, 0, IP6_SCOPE_ZONE_NAME_LEN);
    MEMSET (au1HostName, IP6_ZERO, DNS_MAX_QUERY_LEN);
    MEMSET (au1PingData, IP6_ZERO, IP6_PING_DATA_SIZE);
    MEMSET (au1ScopeZone, IP6_ZERO, IP6_SCOPE_ZONE_NAME_LEN);
    MEMSET (au1PingZoneId, IP6_ZERO, CFA_CLI_MAX_IF_NAME_LEN);
    MEMSET (&PingSrcAddr, 0, sizeof (tIp6Addr));

    MEMSET (au1Ipv6DomNameOneBuf, '\0', (IP6_DOMAIN_NAME_LEN + 1));
    Fsipv6DomNameOne.pu1_OctetList = au1Ipv6DomNameOneBuf;
    Fsipv6DomNameOne.i4_Length = 0;

    MEMSET (au1Ipv6DomNameTwoBuf, '\0', (IP6_DOMAIN_NAME_LEN + 1));
    Fsipv6DomNameTwo.pu1_OctetList = au1Ipv6DomNameTwoBuf;
    Fsipv6DomNameTwo.i4_Length = 0;

    MEMSET (au1Ipv6DomNameThreeBuf, '\0', (IP6_DOMAIN_NAME_LEN + 1));
    Fsipv6DomNameThree.pu1_OctetList = au1Ipv6DomNameThreeBuf;
    Fsipv6DomNameThree.i4_Length = 0;
    pu1ScopeZone = &au1Tmp[0];
    va_start (ap, u4Command);

    /* Third arguement is always InterfaceName/Index */
    i4IfaceIndex = va_arg (ap, INT4);

    /* Fourth argument is ContextName */
    pu1IpCxtName = va_arg (ap, UINT1 *);

    /* Walk through the rest of the arguements and store in args array. 
     * Store 12 arguements at the max. This is because ipv6 commands do not
     * take more than 12 inputs from the command line. Another reason to
     * store is in some cases first input may be optional but user may give 
     * second input. In that case first arg will be null and second arg only 
     * has value */

    while (1)
    {
        args[argno++] = va_arg (ap, UINT4 *);
        if (argno == IP6_CLI_MAX_ARGS)
        {
            break;
        }
    }
    va_end (ap);

    CliRegisterLock (CliHandle, Ip6CliLock, Ip6CliUnLock);

    CLI_SET_ERR (0);

    Ip6GLock ();

    if (IP6_TASK_LOCK () == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        CLI_SET_CMD_STATUS (CLI_FAILURE);
        return;
    }

    /*Get context Id from context Name */
    if (pu1IpCxtName != NULL)
    {
        if (VcmIsVrfExist (pu1IpCxtName, &u4VcId) == VCM_FALSE)
        {
            CliPrintf (CliHandle, "\r%% Invalid VRF Id\r\n");
            /* Unlock the IP6 Task */
            IP6_TASK_UNLOCK ();
            Ip6GUnLock ();
            CliUnRegisterLock (CliHandle);
            return;
        }
    }
    else
    {
#if defined(VRF_WANTED) && (LINUX_310_WANTED) && defined(LNXIP6_WANTED)
        if ((CLI_GET_IFINDEX ()) != -1)
        {
            i4IfaceIndex = CLI_GET_IFINDEX ();
            if (Ip6GetCxtId ((UINT4) i4IfaceIndex, &u4VcId) == IP6_FAILURE)
            {
                IP6_TASK_UNLOCK ();
                Ip6GUnLock ();
                CliUnRegisterLock (CliHandle);
                return;
            }
        }
        else
        {
            u4VcId = VCM_DEFAULT_CONTEXT;
        }
#else
        u4VcId = VCM_DEFAULT_CONTEXT;
#endif
    }

    /*Set Context Id for IP, which will be used by the following
       configuration commands */
    if (Ip6SelectContext (u4VcId) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Invalid VRF Id\r\n");
        IP6_TASK_UNLOCK ();
        Ip6GUnLock ();
        CliUnRegisterLock (CliHandle);
        return;
    }

    switch (u4Command)
    {
        case IP6_CLI_ROUTING:
            Ip6SetRoutingStatus (CliHandle, (INT1) CLI_ATOI (args[0]));
            break;

        case IP6_CLI_INTERFACE:
            i4IfaceIndex = CLI_GET_IFINDEX ();

            i4RetStatus = Ip6SetInterfaceAdminStatus (CliHandle,
                                                      (UINT4) i4IfaceIndex,
                                                      (INT4)
                                                      CLI_ATOI (args[0]));
            break;

        case IP6_ADDR:

            i4IfaceIndex = CLI_GET_IFINDEX ();

            /* get the string in args[0], and convert it to IP6 address format,
               copy it to the variable below */
            MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));
            if (INET_ATON6 (args[0], &Ip6Addr) == 0)
            {
                CLI_SET_ERR (CLI_IP6_INVALID_ADDRESS);
                break;
            }

            i4PrefixLen = *(INT4 *) (args[1]);
            if ((i4PrefixLen < 0) || (i4PrefixLen > 128))
            {
                CLI_SET_ERR (CLI_IP6_INVALID_PREFIXLEN);
                break;
            }

            if ((IS_ADDR_LLOCAL (Ip6Addr)) &&
                ((CLI_PTR_TO_I4 (args[2]) != ADDR6_LLOCAL)
                 && (CLI_PTR_TO_I4 (args[2]) != IP6_CLI_ADDR_LINKLOCAL_CGA)))
            {
                CliPrintf (CliHandle,
                           "Address given is Link Local IP, which is not aligning with given address type!\r\n");
                break;
            }
            nmhGetFsipv6IfSENDSecStatus (i4IfaceIndex,
                                         &i4RetValFsipv6IfSENDSecStatus);

            if (((i4RetValFsipv6IfSENDSecStatus == ND6_SECURE_ENABLE) ||
                 (i4RetValFsipv6IfSENDSecStatus == ND6_SECURE_MIXED)) &&
                ((CLI_PTR_TO_I4 (args[2]) != IP6_CLI_ADDR_CGA) &&
                 (CLI_PTR_TO_I4 (args[2]) != IP6_CLI_ADDR_LINKLOCAL_CGA)))
            {
                CLI_SET_ERR (CLI_IP6_SEND_USE_CGA);
                break;
            }

            if (((i4RetValFsipv6IfSENDSecStatus != ND6_SECURE_ENABLE) &&
                 (i4RetValFsipv6IfSENDSecStatus != ND6_SECURE_MIXED)) &&
                ((CLI_PTR_TO_I4 (args[2]) == IP6_CLI_ADDR_CGA) ||
                 (CLI_PTR_TO_I4 (args[2]) == IP6_CLI_ADDR_LINKLOCAL_CGA)))
            {
                CLI_SET_ERR (CLI_IP6_SEND_NOT_ENABLE);
                break;
            }
            Ip6SetAddr (CliHandle, (UINT4) i4IfaceIndex, Ip6Addr,
                        i4PrefixLen, CLI_PTR_TO_I4 (args[2]));
            break;

        case IP6_INTERFACE_ID:

            i4IfaceIndex = CLI_GET_IFINDEX ();

            /* get the string in args[0], and convert it to IP6 address format,
               copy it to the variable below */

            i4PrefixLen = *(INT4 *) (args[1]);
            if (i4PrefixLen > 64)
            {
                CLI_SET_ERR (CLI_IP6_INVALID_PREFIXLEN);
                break;
            }

            MEMSET (au1Ip6IfId, 0, IP6_EUI_ADDRESS_LEN);

            Fsipv6IfToken.pu1_OctetList = au1Ip6IfId;
            Fsipv6IfToken.i4_Length = IP6_EUI_ADDRESS_LEN;

            if ((*(const CHR1 *) args[0] == ':') &&
                (*((const CHR1 *) args[0] + 1) == ':'))
            {
                if (INET_ATON6 (args[0], &Ip6Addr) == 0)
                {
                    CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE_ID);
                    break;
                }
            }
            else
            {
                CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE_ID);
                break;
            }
            if (Ip6Addr.ip6_addr_u.u2ShortAddr[3] != '\0')
            {
                CLI_SET_ERR (CLI_IP6_INVALID_PREFIXLEN);
                break;
            }
            MEMCPY (Fsipv6IfToken.pu1_OctetList,
                    ((UINT1 *) (&Ip6Addr) + IP6_EUI_ADDRESS_LEN),
                    IP6_EUI_ADDRESS_LEN);
            MEMSET (au1Ip6GetIfId, 0, IP6_EUI_ADDRESS_LEN);

            Fsipv6GetIfToken.pu1_OctetList = au1Ip6GetIfId;
            Fsipv6GetIfToken.i4_Length = IP6_EUI_ADDRESS_LEN;

            if (nmhGetIpv6IfIdentifier (i4IfaceIndex, &Fsipv6GetIfToken)
                != SNMP_SUCCESS)
            {
                CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
            }

            if (MEMCMP (Fsipv6GetIfToken.pu1_OctetList, Fsipv6IfToken.
                        pu1_OctetList, IP6_EUI_ADDRESS_LEN) != 0)
            {
                if (nmhSetIpv6IfIdentifier (i4IfaceIndex, &Fsipv6IfToken)
                    != SNMP_SUCCESS)
                {
                    CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
                }
            }
            break;

        case IP6_INTERFACE_ID_DEL:

            i4IfaceIndex = CLI_GET_IFINDEX ();

            /* get the string in args[0], and convert it to IP6 address format,
               copy it to the variable below */

            i4PrefixLen = *(INT4 *) (args[1]);
            if (i4PrefixLen > 64)
            {
                CLI_SET_ERR (CLI_IP6_INVALID_PREFIXLEN);
                break;
            }

            MEMSET (au1Ip6IfId, 0, IP6_EUI_ADDRESS_LEN);

            Fsipv6IfToken.pu1_OctetList = au1Ip6IfId;
            Fsipv6IfToken.i4_Length = IP6_EUI_ADDRESS_LEN;

            if ((*(const CHR1 *) args[0] == ':') &&
                (*((const CHR1 *) args[0] + 1) == ':'))
            {
                if (INET_ATON6 (args[0], &Ip6Addr) == 0)
                {
                    CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE_ID);
                    break;
                }
            }
            else
            {
                CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE_ID);
                break;
            }

            if (nmhSetIpv6IfIdentifier (i4IfaceIndex, &Fsipv6IfToken)
                != SNMP_SUCCESS)
            {
                CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
            }
            break;

        case IP6_CLI_PING:

            u1PingIntLen = STRLEN (args[0]);
            pPingAddr = (UINT1 *) STRTOK (args[0], "%");
            if (pPingAddr == NULL)
            {
                CLI_SET_ERR (CLI_IP6_INVALID_PING_DEST);
                break;
            }
            u1PingLen = STRLEN (pPingAddr);

            MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));
            if (INET_ATON6 (pPingAddr, &Ip6Addr) == 0)
            {
                CLI_SET_ERR (CLI_IP6_INVALID_ADDRESS);
                break;
            }

            u1AddrScope = Ip6GetAddrScope (&Ip6Addr);

            if ((u1PingIntLen != u1PingLen) &&
                ((u1AddrScope == ADDR6_SCOPE_GLOBAL) ||
                 (u1AddrScope == ADDR6_SCOPE_INVALID)))
            {
                CLI_SET_ERR (CLI_IP6_INVALID_FORMAT_FOR_GLOBAL_ADDR);
                break;
            }

            if (u1PingIntLen != u1PingLen)
            {
                pPingInterface = (UINT1 *) STRTOK (NULL, "%");
                pu1PingZoneId = au1PingZoneId;
                if (pPingInterface != NULL)
                {
                    u2Len =
                        (UINT2) (STRLEN (pPingInterface) <
                                 (CFA_CLI_MAX_IF_NAME_LEN -
                                  1) ? STRLEN (pPingInterface) :
                                 CFA_CLI_MAX_IF_NAME_LEN - 1);
                    STRNCPY (pu1PingZoneId, pPingInterface, u2Len);
                    pu1PingZoneId[u2Len] = '\0';
                }

                CfaGetInterfaceIndexFromNameInCxt (u4VcId, pu1PingZoneId,
                                                   (UINT4 *) &i4Inst);
                if (i4IfaceIndex == 0)
                {
                    i4IfaceIndex = i4Inst;
                }
                else if (i4IfaceIndex != i4Inst)
                {
                    CLI_SET_ERR (CLI_IP6_MISMATCH_OF_INTERFACE);
                    break;
                }
            }

            if (args[1] != NULL)
            {
                if (STRLEN (args[1]) > (IP6_PING_DATA_SIZE - 2))
                {
                    CLI_SET_ERR (CLI_IP6_INVALID_PING_DATA);
                    break;
                }
                pu1PingData = au1PingData;

                *pu1PingData = '0';
                *(pu1PingData + 1) = 'x';
                STRCPY ((pu1PingData + 2), args[1]);
            }

            i4PingCount = (args[2] == NULL) ?
                CLI_PING6_DEFAULT_COUNT : (*(INT4 *) (args[2]));

            i4PingSize = (args[3] == NULL) ?
                CLI_PING6_DEFAULT_SIZE : (*(INT4 *) (args[3]));

            MEMSET (&PingSrcAddr, 0, sizeof (tIp6Addr));
            if (args[4] != NULL)
            {
                if (INET_ATON6 (args[4], &PingSrcAddr) == 0)
                {
                    CLI_SET_ERR (CLI_IP6_INVALID_PING_SOURCE);
                    break;
                }

            }

            i4PingTimeout = CLI_PTR_TO_I4 (args[5]);

#ifdef LNXIP6_WANTED            /* LNXIP */
            Ping6Lock ();
#endif
            CliModifyMoreFlag (CliHandle, &i1PagingFlag);
            Ip6PingInCxt (CliHandle, (UINT4) i4IfaceIndex, u4VcId, Ip6Addr,
                          pu1PingData, i4PingCount, i4PingSize, PingSrcAddr,
                          i4PingTimeout, CLI_PTR_TO_I4 (args[6]),
                          pu1PingZoneId, gau1ZeroPing6HostName);
            CliModifyMoreFlag (CliHandle, &i1PagingFlag);
#ifdef LNXIP6_WANTED            /* LNXIP */
            Ping6UnLock ();
#endif
            break;

        case IP6_HOST_PING:

            if (STRLEN (args[0]) <= DNS_MAX_QUERY_LEN)
            {
                u1PingIntLen = STRLEN (args[0]);
                pHostName = (UINT1 *) (args[0]);
                if (pHostName == NULL)
                {
                    CLI_SET_ERR (CLI_IP6_INVALID_PING_DEST);
                    break;
                }
                u1PingLen = STRLEN (pHostName);

                STRNCPY (au1HostName, pHostName, (STRLEN (pHostName)));

                au1HostName[STRLEN (pHostName)] = '\0';

                *au1HostName = UtilStrToLower ((UINT1 *) au1HostName);

                if (args[1] != NULL)
                {
                    if (STRLEN (args[1]) > (IP6_PING_DATA_SIZE - 2))
                    {
                        CLI_SET_ERR (CLI_IP6_INVALID_PING_DATA);
                        break;
                    }
                    pu1PingData = au1PingData;

                    *pu1PingData = '0';
                    *(pu1PingData + 1) = 'x';
                    STRCPY ((pu1PingData + 2), args[1]);
                }

                i4PingCount = (args[2] == NULL) ?
                    CLI_PING6_DEFAULT_COUNT : (*(INT4 *) (args[2]));

                i4PingSize = (args[3] == NULL) ?
                    CLI_PING6_DEFAULT_SIZE : (*(INT4 *) (args[3]));

                i4PingTimeout = CLI_PTR_TO_I4 (args[5]);
                Ip6PingInCxt (CliHandle, (UINT4) i4IfaceIndex, u4VcId,
                              gZeroIp6Addr, pu1PingData, i4PingCount,
                              i4PingSize, PingSrcAddr, i4PingTimeout,
                              CLI_PTR_TO_I4 (args[6]), pu1PingZoneId,
                              au1HostName);
            }
            break;

        case IP6_IF_SHOW:
            if (pu1IpCxtName == NULL)
            {
                u4VcId = VCM_INVALID_VC;
            }
            u1ArgIndex = 0;
            /* args[0] - Show all vlan flag */
            i4ShowAllVlan = CLI_PTR_TO_I4 (args[u1ArgIndex++]);
            if (i4ShowAllVlan == TRUE)
            {
                /* Vlan Id Arg Index ==> args [1]
                 */
                u1VlanIdArgIndex = u1ArgIndex;
                MEMSET (au1IfName, 0, IF_PREFIX_LEN);
                SNPRINTF ((CHR1 *) au1IfName, IF_PREFIX_LEN, "%s%d",
                          "vlan", *(INT4 *) args[u1VlanIdArgIndex]);
                Ip6IfShowForAllVlanInCxt (CliHandle,
                                          (UINT4) i4IfaceIndex, u4VcId,
                                          au1IfName);
            }
            else
            {
                Ip6IfShowInCxt (CliHandle, (UINT4) i4IfaceIndex, u4VcId);
            }
            break;

        case IP6_ND6_SHOW:
            if (pu1IpCxtName == NULL)
            {
                u4VcId = VCM_INVALID_VC;
            }

            IP6_TASK_UNLOCK ();
            i4RetStatus = Ip6Nd6ShowInCxt (CliHandle, u4VcId);
            CliPrintf (CliHandle, "\r\n");
            IP6_TASK_LOCK ();
            break;
        case IP6_ND6_SUMMARY_SHOW:
            if (pu1IpCxtName == NULL)
            {
                u4VcId = VCM_INVALID_VC;
            }
#ifndef LNXIP6_WANTED
            i4RetStatus = Ip6Nd6ShowSummaryInCxt (CliHandle, u4VcId);
#else
            i4RetStatus = CLI_SUCCESS;
#endif
            CliPrintf (CliHandle, "\r\n");
            break;

        case IP6_ENABLE_RT_ADV_STATUS:
            i4IfaceIndex = CLI_GET_IFINDEX ();

            Ip6EnableRouteAdvStatus (CliHandle, (UINT4) i4IfaceIndex,
                                     CLI_PTR_TO_I4 (args[0]));
            break;

        case IP6_ENABLE_RT_ADV_RDNSS:
            MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));
            MEMSET (&Ip6AddrTwo, 0, sizeof (tIp6Addr));
            MEMSET (&Ip6AddrThree, 0, sizeof (tIp6Addr));
            if (args[0] != NULL)
            {
                if (INET_ATON6 (args[0], &Ip6Addr) == 0)
                {
                    CLI_SET_ERR (CLI_IP6_INVALID_ADDRESS);
                    break;
                }
            }
            if (args[1] != NULL)
            {
                if (INET_ATON6 (args[1], &Ip6AddrTwo) == 0)
                {
                    CLI_SET_ERR (CLI_IP6_INVALID_ADDRESS);
                    break;
                }
            }
            if (args[2] != NULL)
            {
                if (INET_ATON6 (args[2], &Ip6AddrThree) == 0)
                {
                    CLI_SET_ERR (CLI_IP6_INVALID_ADDRESS);
                    break;
                }
            }

            i4IfaceIndex = CLI_GET_IFINDEX ();
            if (args[0] != NULL)
            {
                Ip6EnableRouteAdvRDNSS (CliHandle, (UINT4) i4IfaceIndex,
                                        Ip6Addr, CLI_PTR_TO_U4 (args[3]),
                                        CLI_PTR_TO_I4 (args[6]));
            }
            if (args[1] != NULL)
            {
                Ip6EnableRouteAdvRDNSS (CliHandle, (UINT4) i4IfaceIndex,
                                        Ip6AddrTwo, CLI_PTR_TO_U4 (args[4]),
                                        CLI_PTR_TO_I4 (args[6]));
            }
            if (args[2] != NULL)
            {
                Ip6EnableRouteAdvRDNSS (CliHandle, (UINT4) i4IfaceIndex,
                                        Ip6AddrThree, CLI_PTR_TO_U4 (args[5]),
                                        CLI_PTR_TO_I4 (args[6]));
            }

            break;

        case IP6_DISABLE_RT_ADV_RDNSS:
            MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));
            MEMSET (&Ip6AddrTwo, 0, sizeof (tIp6Addr));
            MEMSET (&Ip6AddrThree, 0, sizeof (tIp6Addr));
            if (args[0] != NULL)
            {
                if (INET_ATON6 (args[0], &Ip6Addr) == 0)
                {
                    CLI_SET_ERR (CLI_IP6_INVALID_ADDRESS);
                    break;
                }
            }
            if (args[1] != NULL)
            {
                if (INET_ATON6 (args[1], &Ip6AddrTwo) == 0)
                {
                    CLI_SET_ERR (CLI_IP6_INVALID_ADDRESS);
                    break;
                }
            }
            if (args[2] != NULL)
            {
                if (INET_ATON6 (args[2], &Ip6AddrThree) == 0)
                {
                    CLI_SET_ERR (CLI_IP6_INVALID_ADDRESS);
                    break;
                }
            }

            i4IfaceIndex = CLI_GET_IFINDEX ();
            Ip6DisableRouteAdvRDNSS (CliHandle, (UINT4) i4IfaceIndex, Ip6Addr,
                                     Ip6AddrTwo, Ip6AddrThree,
                                     CLI_PTR_TO_I4 (args[3]));
            break;

        case IP6_ENABLE_RT_ADV_RDNSS_STATUS:
            i4IfaceIndex = CLI_GET_IFINDEX ();
            Ip6EnableRouteAdvRDNSSStatus (CliHandle, (UINT4) i4IfaceIndex,
                                          CLI_PTR_TO_I4 (args[0]));
            break;

        case IP6_RA_RDNSS_PREFERENCE:
            i4IfaceIndex = CLI_GET_IFINDEX ();

            Ip6RaRDNSSPreference (CliHandle, (UINT4) i4IfaceIndex,
                                  *(INT4 *) (args[0]));

            break;

        case IP6_RA_DNS_DOMNAME_LIFETIME:

            if (args[0] != NULL)
            {
                IP6_ASCIIZ_2_OCT (&Fsipv6DomNameOne, args[0]);
            }
            else
            {
                IP6_CLEAR_OCT (&Fsipv6DomNameOne,
                               sizeof (au1Ipv6DomNameOneBuf));
            }
            if (args[1] != NULL)
            {
                IP6_ASCIIZ_2_OCT (&Fsipv6DomNameTwo, args[1]);
            }
            else
            {
                IP6_CLEAR_OCT (&Fsipv6DomNameTwo,
                               sizeof (au1Ipv6DomNameTwoBuf));
            }
            if (args[2] != NULL)
            {
                IP6_ASCIIZ_2_OCT (&Fsipv6DomNameThree, args[2]);
            }
            else
            {
                IP6_CLEAR_OCT (&Fsipv6DomNameThree,
                               sizeof (au1Ipv6DomNameThreeBuf));
            }

            i4IfaceIndex = CLI_GET_IFINDEX ();
            if (args[0] != NULL)
            {
                Ip6RaDnsDomNameLifeTime (CliHandle, (UINT4) i4IfaceIndex,
                                         &Fsipv6DomNameOne,
                                         CLI_PTR_TO_U4 (args[3]),
                                         CLI_PTR_TO_I4 (args[6]));
            }
            if (args[1] != NULL)
            {
                Ip6RaDnsDomNameLifeTime (CliHandle, (UINT4) i4IfaceIndex,
                                         &Fsipv6DomNameTwo,
                                         CLI_PTR_TO_U4 (args[4]),
                                         CLI_PTR_TO_I4 (args[6]));
            }
            if (args[2] != NULL)
            {
                Ip6RaDnsDomNameLifeTime (CliHandle, (UINT4) i4IfaceIndex,
                                         &Fsipv6DomNameThree,
                                         CLI_PTR_TO_U4 (args[5]),
                                         CLI_PTR_TO_I4 (args[6]));
            }

            break;
        case IP6_RA_NO_DNS_DOMNAME_LIFETIME:

            if (args[0] != NULL)
            {
                IP6_ASCIIZ_2_OCT (&Fsipv6DomNameOne, args[0]);
            }
            else
            {
                IP6_CLEAR_OCT (&Fsipv6DomNameOne,
                               sizeof (au1Ipv6DomNameOneBuf));
            }
            if (args[1] != NULL)
            {
                IP6_ASCIIZ_2_OCT (&Fsipv6DomNameTwo, args[1]);
            }
            else
            {
                IP6_CLEAR_OCT (&Fsipv6DomNameTwo,
                               sizeof (au1Ipv6DomNameTwoBuf));
            }
            if (args[2] != NULL)
            {
                IP6_ASCIIZ_2_OCT (&Fsipv6DomNameThree, args[2]);
            }
            else
            {
                IP6_CLEAR_OCT (&Fsipv6DomNameThree,
                               sizeof (au1Ipv6DomNameThreeBuf));
            }

            i4IfaceIndex = CLI_GET_IFINDEX ();
            Ip6RaNoDnsDomNameLifeTime (CliHandle, (UINT4) i4IfaceIndex,
                                       &Fsipv6DomNameOne, &Fsipv6DomNameTwo,
                                       &Fsipv6DomNameThree,
                                       CLI_PTR_TO_I4 (args[3]));

            break;
        case IP6_RA_DNS_LIFETIME:
            i4IfaceIndex = CLI_GET_IFINDEX ();
            i4LifeTime = 0;
            if (args[0] != NULL)
            {
                i4LifeTime = *(INT4 *) args[0];
                Ip6RaDnsLifeTime (CliHandle, (UINT4) i4IfaceIndex, i4LifeTime);
            }
            else if (args[1] != NULL)
            {
                i4LifeTime = (INT4) IP6_DNS_LIFETIME_INFINITY;
                Ip6RaDnsLifeTime (CliHandle, (UINT4) i4IfaceIndex, i4LifeTime);
            }
            else
            {
                Ip6RaDnsLifeTime (CliHandle, (UINT4) i4IfaceIndex, i4LifeTime);
            }
            break;

        case IP6_ENABLE_RT_ADV_INTERVAL:
            i4IfaceIndex = CLI_GET_IFINDEX ();
            Ip6RaAdvtInterval (CliHandle, (UINT4) i4IfaceIndex,
                               CLI_PTR_TO_I4 (args[0]));
            break;

        case IP6_ENABLE_RT_ADV_LINKLOCAL:
            i4IfaceIndex = CLI_GET_IFINDEX ();
            Ip6RaAdvLinkLocal (CliHandle, (UINT4) i4IfaceIndex,
                               CLI_PTR_TO_I4 (args[0]));
            break;
        case IP6_RA_RDNSS_LIFETIME:
            i4IfaceIndex = CLI_GET_IFINDEX ();

            Ip6RaRDNSSLifetime (CliHandle, (UINT4) i4IfaceIndex,
                                *(INT4 *) (args[0]));

            break;

        case IP6_ENABLE_RT_ADV_FLAG:
            i4IfaceIndex = CLI_GET_IFINDEX ();

            Ip6EnableRouteAdvFlags (CliHandle, (UINT4) i4IfaceIndex,
                                    CLI_PTR_TO_I4 (args[0]));
            break;

        case IP6_HOP_LIMIT:
            i4IfaceIndex = CLI_GET_IFINDEX ();

            Ip6HopLimit (CliHandle, (UINT4) i4IfaceIndex, *(INT4 *) (args[0]));

            break;

        case IP6_DEF_ROUTER_LIFE_TIME:
            i4IfaceIndex = CLI_GET_IFINDEX ();

            Ip6DefRouterLifeTime (CliHandle, (UINT4) i4IfaceIndex, *(args[0]));
            break;

        case IP6_DAD_RETRIES:
            i4IfaceIndex = CLI_GET_IFINDEX ();

            Ip6DADRetries (CliHandle, (UINT4) i4IfaceIndex,
                           *(INT4 *) (args[0]));
            break;

        case IP6_TRAFFIC_SHOW:
            if (pu1IpCxtName == NULL)
            {
                u4VcId = VCM_INVALID_VC;
            }
            u1ArgIndex = 0;
            /* args[0] - HC Flag */
            u1HCFlag = CLI_PTR_TO_U4 (args[u1ArgIndex++]);
            /* args[1] - Show all vlan flag */
            i4ShowAllVlan = CLI_PTR_TO_I4 (args[u1ArgIndex++]);
            IP6_TASK_UNLOCK ();
            if (i4IfaceIndex == IP6_MINUS_ONE)
            {
                if (i4ShowAllVlan == TRUE)
                {

                    /* Vlan Id Arg Index ==> args [2]
                     */
                    u1VlanIdArgIndex = u1ArgIndex;
                    MEMSET (au1IfName, 0, IF_PREFIX_LEN);
                    SNPRINTF ((CHR1 *) au1IfName, IF_PREFIX_LEN, "%s%d",
                              "vlan", *(INT4 *) args[u1VlanIdArgIndex]);
                    Ip6IfTrafficShowForAllVlanInCxt (CliHandle,
                                                     (UINT4) i4IfaceIndex,
                                                     u1HCFlag, u4VcId,
                                                     au1IfName);
                    CliPrintf (CliHandle, "\r\n");
                }
                else
                {
                    Ip6TrafficShowInCxt (CliHandle, u1HCFlag, u4VcId);
                    CliPrintf (CliHandle, "\r\n");
                }
            }
            else
            {
                Ip6IfTrafficShowInCxt (CliHandle, (UINT4) i4IfaceIndex,
                                       u1HCFlag, u4VcId);
                CliPrintf (CliHandle, "\r\n");
            }
            IP6_TASK_LOCK ();
            break;

        case IP6_REACHABLE_TIME:
            i4IfaceIndex = CLI_GET_IFINDEX ();

            Ip6ReachableTime (CliHandle, (UINT4) i4IfaceIndex,
                              CLI_PTR_TO_U4 (args[0]));
            break;

        case IP6_RETRANS_TIME:
            i4IfaceIndex = CLI_GET_IFINDEX ();

            Ip6RetransmitTime (CliHandle, (UINT4) i4IfaceIndex, *(args[0]));
            break;

        case IP6_RA_LINK_MTU:
            i4IfaceIndex = CLI_GET_IFINDEX ();
            if ((args[0]) == NULL)
            {
                u4RaMtu = IP6_INTERFACE_LINK_MTU;
            }
            else
            {
                u4RaMtu = *(args[0]);
            }
            IP6_TASK_UNLOCK ();
            CliSetIp6RALinkMTU (CliHandle, i4IfaceIndex, u4RaMtu);
            IP6_TASK_LOCK ();
            break;

        case IP6_ROUTEADV_INTERVAL:
            i4IfaceIndex = CLI_GET_IFINDEX ();

            Ip6RouteAdvInterval (CliHandle, (UINT4) i4IfaceIndex,
                                 *(args[1]), *(args[0]));
            break;

        case IP6_ADDR_DEL:

            i4IfaceIndex = CLI_GET_IFINDEX ();
            MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));

            /* get the string in args[0], and convert it to IP6 address format,
               copy it to the variable below */
            MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));
            if (INET_ATON6 (args[0], &Ip6Addr) == 0)
            {
                CLI_SET_ERR (CLI_IP6_INVALID_ADDRESS);
                break;
            }

            i4PrefixLen = *(INT4 *) (args[1]);
            if ((i4PrefixLen < 1) || (i4PrefixLen > 128))
            {
                CLI_SET_ERR (CLI_IP6_INVALID_PREFIXLEN);
                break;
            }

            Ip6CliAddrDelete (CliHandle, (UINT4) i4IfaceIndex,
                              Ip6Addr, i4PrefixLen, CLI_PTR_TO_I4 (args[2]));
            break;

        case IP6_CLI_TRACE:
            Ip6TraceInCxt (CliHandle, CLI_PTR_TO_U4 (args[0]), u4VcId);
            break;

        case IP6_PREFIX_CONFIG:

            i4IfaceIndex = CLI_GET_IFINDEX ();

            if (args[0])
            {
                MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));
                if (INET_ATON6 (args[0], &Ip6Addr) == 0)
                {
                    CLI_SET_ERR (CLI_IP6_INVALID_ADDRESS);
                    break;
                }
                i4PrefixLen = *(INT4 *) args[1];
                if ((!(args[3] || args[4] || args[5] || args[9])) && (args[2]))
                {
                    i4DefaultFlag = 1;
                }
            }
            else
            {
                MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));
                i4PrefixLen = 0;
                i4DefaultFlag = 1;
                u4PrefixValidTime = IP6_ADDR_PROF_DEF_VALID_LIFE;
                u4PrefixPreferredTime = IP6_ADDR_PROF_DEF_PREF_LIFE;

            }

            if (args[3] || args[4] || args[5])
            {
                /* Valid life time specifeied by the user */

                if (args[3])
                {
                    u4PrefixValidTime = *args[3];
                }
                else if (args[4])
                {
                    u4PrefixValidTime = 0xFFFFFFFF;
                }
                else
                {
                    u4PrefixValidTime = *args[5];
                    u1PrefixValidTimeFlag = IP6_VARIABLE_TIME;
                }

                /* Preferred life time specifeied by the user */

                if (args[6])
                {
                    u4PrefixPreferredTime = *(UINT4 *) args[6];
                }
                else if (args[7])
                {
                    u4PrefixPreferredTime = 0xFFFFFFFF;
                }
                else
                {
                    u4PrefixPreferredTime = *(UINT4 *) args[8];
                    u1PrefixPrefTimeFlag = IP6_VARIABLE_TIME;
                }
            }

            else if (args[9])
            {
                u1NoAdvertiseFlag = 1;
            }
#ifndef LNXIP6_WANTED
            if (args[10])
            {
                u1OnLinkStatus = IP6_ADDR_PROF_ONLINK_ADV_OFF;

            }
            if (args[11])
            {
                CliPrintf (CliHandle, "Cannot set onlink status in FSIP\n");
                break;
            }
#else
            if (args[10])
            {
                u1OnLinkStatus = IP6_ADDR_PROF_ONLINK_ADV_OFF;
            }
            if (args[11])
            {
                u1OnLinkStatus = IP6_ADDR_PROF_ONLINK_ADV_ON;
            }
#endif

            u1AutoConfigStatus = IP6_ADDR_PROF_AUTO_ADV_ON;

            u1SupportEmbeddedRp = IP6_SUPPORT_EMBD_RP_DISABLE;

            if (u1NoAdvertiseFlag == 0)
            {
                Ip6SetPrefix (CliHandle, (UINT4) i4IfaceIndex, Ip6Addr,
                              i4PrefixLen, u4PrefixValidTime,
                              u1PrefixValidTimeFlag, u4PrefixPreferredTime,
                              u1PrefixPrefTimeFlag, IP6_ADDR_PROF_PREF_ADV_ON,
                              u1OnLinkStatus, u1AutoConfigStatus,
                              u1SupportEmbeddedRp, i4DefaultFlag);
            }

            else
            {
                Ip6SetPrefix (CliHandle, (UINT4) i4IfaceIndex, Ip6Addr,
                              i4PrefixLen, u4PrefixValidTime,
                              u1PrefixValidTimeFlag, u4PrefixPreferredTime,
                              u1PrefixPrefTimeFlag, IP6_ADDR_PROF_PREF_ADV_OFF,
                              u1OnLinkStatus, u1AutoConfigStatus,
                              u1SupportEmbeddedRp, i4DefaultFlag);
            }

            break;

        case IP6_PREFIX_CONFIG_OPT:

            i4IfaceIndex = CLI_GET_IFINDEX ();

            if (args[0])
            {
                MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));
                if (INET_ATON6 (args[0], &Ip6Addr) == 0)
                {
                    CLI_SET_ERR (CLI_IP6_INVALID_ADDRESS);
                    break;
                }
                i4PrefixLen = *(INT4 *) args[1];
                if ((!
                     (args[3] || args[4] || args[5] || args[9] || args[10]
                      || args[11])) && (args[2]))
                {
                    i4DefaultFlag = 1;
                }
            }
            else
            {
                MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));
                i4PrefixLen = 0;
                i4DefaultFlag = 1;
                u4PrefixValidTime = IP6_ADDR_PROF_DEF_VALID_LIFE;
                u4PrefixPreferredTime = IP6_ADDR_PROF_DEF_PREF_LIFE;

            }

            if (args[3] || args[4] || args[5])
            {
                /* Valid life time specifeied by the user */

                if (args[3])
                {
                    u4PrefixValidTime = *args[3];
                }
                else if (args[4])
                {
                    u4PrefixValidTime = 0xFFFFFFFF;
                }
                else
                {
                    u4PrefixValidTime = *args[5];
                    u1PrefixValidTimeFlag = IP6_VARIABLE_TIME;
                }

                /* Preferred life time specifeied by the user */

                if (args[6])
                {
                    u4PrefixPreferredTime = *(UINT4 *) args[6];
                }
                else if (args[7])
                {
                    u4PrefixPreferredTime = 0xFFFFFFFF;
                }
                else
                {
                    u4PrefixPreferredTime = *(UINT4 *) args[8];
                    u1PrefixPrefTimeFlag = IP6_VARIABLE_TIME;
                }
            }

            else if (args[9])
            {
                u1NoAdvertiseFlag = 1;
            }

            if (args[10])
            {
                u1AutoConfigStatus = IP6_ADDR_PROF_AUTO_ADV_OFF;
            }
            else
            {
                u1AutoConfigStatus = IP6_ADDR_PROF_AUTO_ADV_ON;
            }

            if (args[11])
            {
                u1SupportEmbeddedRp = IP6_SUPPORT_EMBD_RP_ENABLE;
            }
            else
            {
                u1SupportEmbeddedRp = IP6_SUPPORT_EMBD_RP_DISABLE;
            }
            u1OnLinkStatus = IP6_ADDR_PROF_ONLINK_ADV_ON;

            if (u1NoAdvertiseFlag == 0)
            {
                Ip6SetPrefix (CliHandle, (UINT4) i4IfaceIndex, Ip6Addr,
                              i4PrefixLen, u4PrefixValidTime,
                              u1PrefixValidTimeFlag, u4PrefixPreferredTime,
                              u1PrefixPrefTimeFlag, IP6_ADDR_PROF_PREF_ADV_ON,
                              u1OnLinkStatus, u1AutoConfigStatus,
                              u1SupportEmbeddedRp, i4DefaultFlag);
            }

            else
            {
                Ip6SetPrefix (CliHandle, (UINT4) i4IfaceIndex, Ip6Addr,
                              i4PrefixLen, u4PrefixValidTime,
                              u1PrefixValidTimeFlag, u4PrefixPreferredTime,
                              u1PrefixPrefTimeFlag, IP6_ADDR_PROF_PREF_ADV_OFF,
                              u1OnLinkStatus, u1AutoConfigStatus,
                              u1SupportEmbeddedRp, i4DefaultFlag);
            }

            break;

        case IP6_PREFIX_CONFIG_DEL:

            i4IfaceIndex = CLI_GET_IFINDEX ();

            if (args[0])
            {
                MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));
                if (INET_ATON6 (args[0], &Ip6Addr) == 0)
                {
                    CLI_SET_ERR (CLI_IP6_INVALID_ADDRESS);
                    break;
                }
                i4PrefixLen = *(INT4 *) args[1];
            }
            else
            {
                MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));
                i4PrefixLen = 0;
            }

            Ip6DelPrefix (CliHandle, (UINT4) i4IfaceIndex, Ip6Addr,
                          i4PrefixLen);

            break;

        case IP6_PREFIX_SHOW:
            if (pu1IpCxtName == NULL)
            {
                u4VcId = VCM_INVALID_VC;
            }
            u1ArgIndex = 0;
            /* args[0] - Show all vlan flag */
            i4ShowAllVlan = CLI_PTR_TO_I4 (args[u1ArgIndex++]);
            if (i4ShowAllVlan == TRUE)
            {
                /* Vlan Id Arg Index ==> args [1]
                 */
                u1VlanIdArgIndex = u1ArgIndex;
                MEMSET (au1IfName, 0, IF_PREFIX_LEN);
                SNPRINTF ((CHR1 *) au1IfName, IF_PREFIX_LEN, "%s%d",
                          "vlan", *(INT4 *) args[u1VlanIdArgIndex]);
                Ip6ShowPrefixForAllVlanInCxt (CliHandle,
                                              (UINT4) i4IfaceIndex, u4VcId,
                                              au1IfName);
                CliPrintf (CliHandle, "\r\n");
            }
            else
            {
                Ip6ShowPrefixInCxt (CliHandle, (UINT4) i4IfaceIndex, u4VcId);
                CliPrintf (CliHandle, "\r\n");
            }
            break;
        case IP6_ND6_CACHE_ADD:

            /* get the string in args[0], and convert it to IP6 address format,
               copy it to the variable below */
            if (pu1IpCxtName == NULL)
            {
                u4VcId = VCM_INVALID_VC;
            }

            MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));
            if (INET_ATON6 (args[0], &Ip6Addr) == 0)
            {
                CLI_SET_ERR (CLI_IP6_INVALID_ADDRESS);
                break;
            }

            CliConvertDotStrToStr ((UINT1 *) args[1], au1PhyMacAddr);

            Ip6Nd6CacheAdd (CliHandle, (UINT4) i4IfaceIndex, Ip6Addr,
                            au1PhyMacAddr, u4VcId);
            break;

        case IP6_ND6_CACHE_DEL:

            /* get the string in args[0], and convert it to IP6 address format,
               copy it to the variable below */
            if (pu1IpCxtName == NULL)
            {
                u4VcId = VCM_INVALID_VC;
            }

            MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));
            if (INET_ATON6 (args[0], &Ip6Addr) == 0)
            {
                CLI_SET_ERR (CLI_IP6_INVALID_ADDRESS);
                break;
            }
            Ip6Nd6CacheDel (CliHandle, (UINT4) i4IfaceIndex, Ip6Addr, u4VcId);
            break;

        case IP6_TRACEROUTE:
            MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));
            if (INET_ATON6 (args[0], &Ip6Addr) == 0)
            {
                CLI_SET_ERR (CLI_IP6_INVALID_ADDRESS);
                break;
            }

            Ip6TraceRoute (CliHandle, Ip6Addr);
            break;

        case IP6_CLEAR_NEIGHBORS:
            if (pu1IpCxtName == NULL)
            {
                u4VcId = VCM_INVALID_VC;
            }
            MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));
            if (args[0])
            {
                if (INET_ATON6 (args[0], &Ip6Addr) == 0)
                {
                    CLI_SET_ERR (CLI_IP6_INVALID_ADDRESS);
                    break;
                }
                Ip6ClearNeighbors (CliHandle, u4VcId, &Ip6Addr);
            }
            else
            {
                Ip6ClearNeighbors (CliHandle, u4VcId, NULL);
            }
            break;

        case IP6_CLEAR_TRAFFIC:
            if (pu1IpCxtName == NULL)
            {
                u4VcId = VCM_INVALID_VC;
            }
            Ip6ClearTraffic (CliHandle, u4VcId);
            break;
        case IP6_DEFAULT_HOP_LIMIT:
            i4RetStatus =
                Ip6DefaultHopLimit (CliHandle, *(INT4 *) (args[IP6_ZERO]));
            break;

        case IP6_PATH_MTU_DISCOVER:
            i4RetStatus =
                Ip6SetPmtuDiscovery (CliHandle, IP6_PMTU_ENABLE, u4VcId);
            break;

        case IP6_NO_PATH_MTU_DISCOVER:
            i4RetStatus =
                Ip6SetPmtuDiscovery (CliHandle, IP6_PMTU_DISABLE, u4VcId);
            break;

        case IP6_PATH_MTU_ADD:
            MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));

            if (INET_ATON6 (args[0], &Ip6Addr) == 0)
            {
                CLI_SET_ERR (CLI_IP6_INVALID_ADDRESS);
                break;
            }

            i4RetStatus = Ip6SetPmtuValue (CliHandle, Ip6Addr,
                                           *(INT4 *) (args[1]),
                                           u4VcId, IP6_PMTU_ENABLE);
            break;

        case IP6_PATH_MTU_DEL:
            MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));

            i4RetStatus = Ip6SetPmtuValue (CliHandle, Ip6Addr, 0, u4VcId,
                                           IP6_PMTU_DISABLE);
            break;

        case IP6_CLI_RFC5095COMPAT:
            i4RetStatus =
                Ip6SetRFC5095Compatibilty (CliHandle, IP6_RFC5095_COMPATIBLE,
                                           u4VcId);
            break;

        case IP6_CLI_NO_RFC5095COMPAT:
            i4RetStatus =
                Ip6SetRFC5095Compatibilty (CliHandle,
                                           IP6_RFC5095_NOT_COMPATIBLE, u4VcId);
            break;
        case IP6_CLI_RFC5942COMPAT:
#ifdef LNXIP6_WANTED
            i4RetStatus =
                Ip6SetRFC5942Compatibilty (CliHandle, IP6_RFC5942_COMPATIBLE);
            break;
#else
            CliPrintf (CliHandle, "RFC5942 not supported in FSIP\r\n");
            break;
#endif

        case IP6_CLI_NO_RFC5942COMPAT:
#ifdef LNXIP6_WANTED
            i4RetStatus =
                Ip6SetRFC5942Compatibilty (CliHandle,
                                           IP6_RFC5942_NOT_COMPATIBLE);
            break;
#else
            CliPrintf (CliHandle, "RFC5942 not supported in FSIP\r\n");
            break;
#endif
        case IP6_SHOW_PMTU:
            if (pu1IpCxtName == NULL)
            {
                u4VcId = VCM_INVALID_VC;
            }
            i4RetStatus = Ip6ShowPmtu (CliHandle, u4VcId);
            break;
        case IP6_CLI_INTF_ROUTING:
            i4IfaceIndex = CLI_GET_IFINDEX ();
            Ip6CliSetIfaceRoutingStatus (CliHandle, i4IfaceIndex,
                                         (INT1) CLI_ATOI (args[0]));
            break;

        case IP6_SHOW_ADDR_SEL_POLICY:
            i4RetStatus = Ip6ShowAddrSelPolicyTable (CliHandle);
            break;

        case IP6_POLICY_PREFIX_CONFIG:

            i4IfaceIndex = CLI_GET_IFINDEX ();

            if (args[0])
            {
                MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));
                if (INET_ATON6 (args[0], &Ip6Addr) == 0)
                {
                    CLI_SET_ERR (CLI_IP6_INVALID_ADDRESS);
                    break;
                }
                i4PrefixLen = *(INT4 *) args[1];
                u4Precedence = *(INT4 *) args[2];
                u4Label = *(INT4 *) args[3];
            }
            else
            {
                MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));
                i4PrefixLen = 0;
            }
            Ip6SetPolicyPrefix (CliHandle, (UINT4) i4IfaceIndex, Ip6Addr,
                                (UINT4) i4PrefixLen, u4Label, u4Precedence,
                                CLI_PTR_TO_I4 (args[4]));
            break;
        case IP6_POLICY_PREFIX_DELETE:
            i4IfaceIndex = CLI_GET_IFINDEX ();

            if (args[0])
            {
                MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));
                if (INET_ATON6 (args[0], &Ip6Addr) == 0)
                {
                    CLI_SET_ERR (CLI_IP6_INVALID_ADDRESS);
                    break;
                }
                i4PrefixLen = *(INT4 *) args[1];
                u4Label = *(INT4 *) args[2];
                u4Precedence = *(INT4 *) args[3];
            }
            else
            {
                MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));
                i4PrefixLen = 0;
            }
            Ip6DelPolicyPrefix (CliHandle, (UINT4) i4IfaceIndex, Ip6Addr,
                                (UINT4) i4PrefixLen, u4Label, u4Precedence,
                                CLI_PTR_TO_I4 (args[4]));
            break;
            /* Added for zone configuration on an interface
               for RFC4007 */
        case IP6_ZONE_CONFIG:
#ifndef LNXIP6_WANTED            /* FSIP */
            i4IfaceIndex = CLI_GET_IFINDEX ();

            /* get the scope from arg[0] */
            u1Scope = *(UINT1 *) (args[0]);

            if ((u1Scope < ADDR6_SCOPE_INTLOCAL)
                || (u1Scope > ADDR6_SCOPE_GLOBAL))
            {
                CLI_SET_ERR (CLI_IP6_INVALID_SCOPE);
                break;
            }
            i4ZoneId = *(INT4 *) (args[1]);

            if ((i4ZoneId < 1) || (i4ZoneId > 65535))
            {
                CLI_SET_ERR (CLI_IP6_INVALID_ZONE_ID);
                break;
            }
            u2Len = (UINT2) (STRLEN (args[2]) < (IP6_SCOPE_ZONE_NAME_LEN - 1) ?
                             STRLEN (args[2]) : IP6_SCOPE_ZONE_NAME_LEN - 1);
            STRNCPY (au1ScopeZone, args[2], u2Len);
            au1ScopeZone[u2Len] = '\0';
            SPRINTF ((CHR1 *) pu1ScopeZone, "%s%u", au1ScopeZone, i4ZoneId);

            Ip6SetScopeZone (CliHandle, (UINT4) i4IfaceIndex,
                             u1Scope, pu1ScopeZone);
#else
            CliPrintf (CliHandle, "Scope-Zones not supported in Linux IP\r\n");
#endif
            break;
            /* Added for zone deletion on an interface
               for RFC4007 */
        case IP6_ZONE_DEL:
#ifndef LNXIP6_WANTED            /* FSIP */
            i4IfaceIndex = CLI_GET_IFINDEX ();

            u1Scope = *(UINT1 *) (args[0]);
            if ((u1Scope < ADDR6_SCOPE_INTLOCAL)
                || (u1Scope > ADDR6_SCOPE_GLOBAL))
            {
                CLI_SET_ERR (CLI_IP6_INVALID_SCOPE);
                break;
            }
            i4ZoneId = *(INT4 *) (args[1]);
            if ((i4ZoneId < 1) || (i4ZoneId > 65535))
            {
                CLI_SET_ERR (CLI_IP6_INVALID_ZONE_ID);
                break;
            }
            u2Len = (UINT2) (STRLEN (args[2]) < (IP6_SCOPE_ZONE_NAME_LEN - 1) ?
                             STRLEN (args[2]) : IP6_SCOPE_ZONE_NAME_LEN - 1);
            STRNCPY (au1ScopeZone, args[2], u2Len);
            au1ScopeZone[u2Len] = '\0';
            SPRINTF ((CHR1 *) pu1ScopeZone, "%s%u", au1ScopeZone, i4ZoneId);
            if (i4IfaceIndex >= 0)
            {
                Ip6CliDeleteScopeZone (CliHandle, (UINT4) i4IfaceIndex, u1Scope,
                                       pu1ScopeZone);
            }
#else
            CliPrintf (CliHandle, "Scope-Zones not supported in Linux IP\r\n");
#endif
            break;

        case IP6_DEF_ZONE_CONFIG:
            u1Scope = *(UINT1 *) (args[0]);
            if ((u1Scope < ADDR6_SCOPE_INTLOCAL)
                || (u1Scope > ADDR6_SCOPE_GLOBAL))
            {
                CLI_SET_ERR (CLI_IP6_INVALID_SCOPE);
                break;
            }
            i4ZoneId = *(UINT4 *) (args[1]);
            if ((i4ZoneId < 1) || (i4ZoneId > 65535))
            {
                CLI_SET_ERR (CLI_IP6_INVALID_ZONE_ID);
                break;
            }
            u2Len = (UINT2) (STRLEN (args[2]) < (IP6_SCOPE_ZONE_NAME_LEN - 1) ?
                             STRLEN (args[2]) : IP6_SCOPE_ZONE_NAME_LEN - 1);
            STRNCPY (au1ScopeZone, args[2], u2Len);
            au1ScopeZone[u2Len] = '\0';
            SPRINTF ((CHR1 *) pu1ScopeZone, "%s%u", au1ScopeZone, i4ZoneId);
            Ip6CliConfigDefaultScopeZone (CliHandle, pu1ScopeZone);
            break;

        case IP6_ECMP_TIMER:
            Ip6CliConfigEcmpTimeInterval (CliHandle, *((INT4 *) args[0]));
            break;

        case IP6_NO_ECMP_TIMER:
            Ip6CliConfigEcmpTimeInterval (CliHandle,
                                          IP6_ECMP6PRT_TIMER_INTERVAL);
            break;

        case IP6_SHOW_INT_SCOPE_ZONE:

#ifndef LNXIP6_WANTED            /* FSIP */
            if (pu1IpCxtName == NULL)
            {
                u4VcId = VCM_INVALID_VC;
            }
            u1ArgIndex = 0;
            i4ShowAllVlan = CLI_PTR_TO_I4 (args[u1ArgIndex++]);
            if (i4IfaceIndex == IP6_MINUS_ONE)
            {
                if (i4ShowAllVlan == TRUE)
                {

                    /* Vlan Id Arg Index ==> args [1] */
                    u1VlanIdArgIndex = u1ArgIndex;
                    MEMSET (au1IfName, 0, IF_PREFIX_LEN);
                    SNPRINTF ((CHR1 *) au1IfName, IF_PREFIX_LEN, "%s%d",
                              "vlan", *(INT4 *) args[u1VlanIdArgIndex]);
                    Ip6ShowZoneInfoForAllVlanInCxt (CliHandle,
                                                    (UINT4) i4IfaceIndex,
                                                    u4VcId, au1IfName);
                    CliPrintf (CliHandle, "\r\n");

                }
                else
                {
                    i4IfaceIndex = 0;
                    Ip6ShowZoneInCxt (CliHandle, (UINT4) i4IfaceIndex, u4VcId);
                    CliPrintf (CliHandle, "\r\n");
                }
            }
            else
            {
                Ip6ShowZoneInCxt (CliHandle, (UINT4) i4IfaceIndex, u4VcId);
                CliPrintf (CliHandle, "\r\n");
            }
#else
            CliPrintf (CliHandle, "Scope-Zones not supported in Linux IP\r\n");
#endif
            break;

        case IP6_SHOW_ZONE_INTERFACE:
            if (u4VcId == VCM_INVALID_VC)
            {
                u4VcId = IP6_DEFAULT_CONTEXT;
            }

            STRCPY (au1ScopeZone, args[0]);
            IP6ShowZoneInterfacesInCxt (CliHandle, u4VcId, au1ScopeZone);
            break;
        case IP6_SHOW_DEF_SCOPE_ZONE:

            if (pu1IpCxtName == NULL)
            {
                u4VcId = VCM_INVALID_VC;
                Ip6CliGetDefaultScopeZoneForAllCxt (CliHandle, u4VcId);
            }
            else
            {
                Ip6CliGetDefaultScopeZone (CliHandle, u4VcId);
            }
            break;
        case IP6_ERROR_CONTROL_ENABLE:
            i4IfaceIndex = CLI_GET_IFINDEX ();

            Icmp6ErrMsgRateLimit (CliHandle, (UINT4) i4IfaceIndex,
                                  *(INT4 *) (args[0]), *(INT4 *) (args[1]));

            break;

        case IP6_ERROR_CONTROL_DISABLE:
            i4IfaceIndex = CLI_GET_IFINDEX ();
            nmhSetFsipv6IfIcmpErrInterval (i4IfaceIndex, 0);
            nmhSetFsipv6IfIcmpTokenBucketSize (i4IfaceIndex, 0);
            break;

        case IP6_DEST_UNREACHABLE_DISABLE:
            i4IfaceIndex = CLI_GET_IFINDEX ();
            Ip6IfDisabeDestUnreachMsg (CliHandle, (UINT4) i4IfaceIndex,
                                       ICMP6_DEST_UNREACHABLE_DISABLE);
            break;

        case IP6_DEST_UNREACHABLE_ENABLE:
            i4IfaceIndex = CLI_GET_IFINDEX ();
            Ip6IfDisabeDestUnreachMsg (CliHandle, (UINT4) i4IfaceIndex,
                                       ICMP6_DEST_UNREACHABLE_ENABLE);
            break;

        case IP6_UNNUM_INDEX:
            i4IfaceIndex = CLI_GET_IFINDEX ();
            Ip6UnnumIf (CliHandle, i4IfaceIndex, *(INT4 *) (args[0]));
            break;

        case IP6_NO_UNNUM_INDEX:
            i4IfaceIndex = CLI_GET_IFINDEX ();
            Ip6UnnumIf (CliHandle, i4IfaceIndex, 0);
            break;

        case IP6_ICMP_REDIRECT_ENABLE:
            i4IfaceIndex = CLI_GET_IFINDEX ();
            Ip6IfIcmpRedirectMsg (CliHandle, (UINT4) i4IfaceIndex,
                                  (INT4) ICMP6_REDIRECT_ENABLE);
            break;

        case IP6_ICMP_REDIRECT_DISABLE:
            i4IfaceIndex = CLI_GET_IFINDEX ();
            Ip6IfIcmpRedirectMsg (CliHandle, (UINT4) i4IfaceIndex,
                                  (INT4) ICMP6_REDIRECT_DISABLE);
            break;

        case IP6_ND_PROXY_ENABLE:
            i4IfaceIndex = CLI_GET_IFINDEX ();
            i4RetValue = Ip6SetNDProxyAdminStatus (CliHandle, i4IfaceIndex,
                                                   ND6_IF_PROXY_ADMIN_UP);
            break;

        case IP6_ND_PROXY_DISABLE:
            i4IfaceIndex = CLI_GET_IFINDEX ();
            i4RetValue = Ip6SetNDProxyAdminStatus (CliHandle, i4IfaceIndex,
                                                   ND6_IF_PROXY_ADMIN_DOWN);
            break;

        case IP6_ND_LOCAL_PROXY_ENABLE:
            i4IfaceIndex = CLI_GET_IFINDEX ();
            i4RetValue = Ip6SetNDProxyMode (CliHandle, i4IfaceIndex,
                                            ND6_PROXY_MODE_LOCAL);
            break;

        case IP6_ND_LOCAL_PROXY_DISABLE:
            i4IfaceIndex = CLI_GET_IFINDEX ();
            i4RetValue = Ip6SetNDProxyMode (CliHandle, i4IfaceIndex,
                                            ND6_PROXY_MODE_GLOBAL);
            break;

        case IP6_ND_PROXY_UPSTREAM:
            i4IfaceIndex = CLI_GET_IFINDEX ();
            i4RetValue = Ip6SetNDProxyUpstream (CliHandle, i4IfaceIndex,
                                                ND6_PROXY_IF_UPSTREAM);
            break;

        case IP6_ND_PROXY_DOWNSTREAM:
            i4IfaceIndex = CLI_GET_IFINDEX ();
            i4RetValue = Ip6SetNDProxyUpstream (CliHandle, i4IfaceIndex,
                                                ND6_PROXY_IF_DOWNSTREAM);
            break;

        case IP6_SEND_GEN_SEC:
            i4RetValue = Ip6SetSeNDSecLevel (CliHandle, *(UINT4 *) (args[0]));
            break;

        case IP6_SEND_NBR_SEC:
            i4RetValue = Ip6SetSeNDNbrSecLevel (CliHandle,
                                                *(UINT4 *) (args[0]));
            break;

        case IP6_SEND_PRF_ENABLE:
            i4RetValue = Ip6SetSeNDPrefixChkRelax (CliHandle,
                                                   ND6_PREFIX_CHECK_ENABLE);
            break;

        case IP6_SEND_PRF_DISABLE:
            i4RetValue = Ip6SetSeNDPrefixChkRelax (CliHandle,
                                                   ND6_PREFIX_CHECK_DISABLE);
            break;

        case IP6_SEND_MIN_KEY:
            i4RetValue = Ip6SetSeNDMinKeyLength (CliHandle,
                                                 *(UINT4 *) (args[0]));
            break;

        case IP6_SEND_ENABLE:
            i4IfaceIndex = CLI_GET_IFINDEX ();
            i4RetValue = Ip6SetSeNDStatus (CliHandle, i4IfaceIndex,
                                           ND6_SECURE_ENABLE);
            break;

        case IP6_SEND_MIXED:
            i4IfaceIndex = CLI_GET_IFINDEX ();
            i4RetValue = Ip6SetSeNDStatus (CliHandle, i4IfaceIndex,
                                           ND6_SECURE_MIXED);
            break;

        case IP6_SEND_UNSECURE:
            i4IfaceIndex = CLI_GET_IFINDEX ();
            i4RetValue = Ip6SetSeNDStatus (CliHandle, i4IfaceIndex,
                                           ND6_SECURE_DISABLE);
            break;

        case IP6_SEND_TIME_DELTA:
            i4IfaceIndex = CLI_GET_IFINDEX ();
            i4RetValue = Ip6SetSeNDTimeStamp (CliHandle, i4IfaceIndex,
                                              *(UINT4 *) (args[0]),
                                              ND6_TIME_DELTA);
            break;

        case IP6_SEND_TIME_FUZZ:
            i4IfaceIndex = CLI_GET_IFINDEX ();
            i4RetValue = Ip6SetSeNDTimeStamp (CliHandle, i4IfaceIndex,
                                              *(UINT4 *) (args[0]),
                                              ND6_TIME_FUZZ);
            break;

        case IP6_SEND_TIME_DRIFT:
            i4IfaceIndex = CLI_GET_IFINDEX ();
            i4RetValue = Ip6SetSeNDTimeStamp (CliHandle, i4IfaceIndex,
                                              *(UINT4 *) (args[0]),
                                              ND6_TIME_DRIFT);
            break;

        case IP6_SEND_UNSEC_ADV_ENABLE:
            i4RetValue = Ip6SetSeNDAcceptUnsecureAdv (CliHandle,
                                                      ND6_ACCEPT_UNSEC_ADV_ENABLE);
            break;

        case IP6_SEND_UNSEC_ADV_DISABLE:
            i4RetValue = Ip6SetSeNDAcceptUnsecureAdv (CliHandle,
                                                      ND6_ACCEPT_UNSEC_ADV_DISABLE);

            break;

        case IP6_SEND_AUTH_CGA:
            i4RetValue = Ip6SetSeNDAuthType (CliHandle, ND6_AUTH_CGA);
            break;

        case IP6_SEND_AUTH_TA:
            i4RetValue = Ip6SetSeNDAuthType (CliHandle, ND6_AUTH_TA);
            break;

        case IP6_SEND_AUTH_BOTH:
            i4RetValue = Ip6SetSeNDAuthType (CliHandle, ND6_AUTH_CGA_TA);
            break;

        case IP6_SEND_AUTH_ANYONE:
            i4RetValue = Ip6SetSeNDAuthType (CliHandle, ND6_AUTH_ANYONE);
            break;

        case IP6_SHOW_SEND:
            if (pu1IpCxtName == NULL)
            {
                u4VcId = VCM_INVALID_VC;
            }
            u1ArgIndex = 0;
            IP6_TASK_UNLOCK ();
            /* args[0] - Show all vlan flag */
            i4ShowAllVlan = CLI_PTR_TO_I4 (args[u1ArgIndex++]);
            if (i4ShowAllVlan == TRUE)
            {
                /* Vlan Id Arg Index ==> args [1]
                 */
                u1VlanIdArgIndex = u1ArgIndex;
                MEMSET (au1IfName, 0, IF_PREFIX_LEN);
                SNPRINTF ((CHR1 *) au1IfName, IF_PREFIX_LEN, "%s%d",
                          "vlan", *(INT4 *) args[u1VlanIdArgIndex]);
                Ip6IfShowNdSecureForAllVlanInCxt (CliHandle,
                                                  (UINT4) i4IfaceIndex,
                                                  u4VcId, au1IfName,
                                                  CLI_PTR_TO_I4 (args
                                                                 [++u1ArgIndex]));
            }
            else
            {
                Ip6IfShowNdSecureInCxt (CliHandle, (UINT4) i4IfaceIndex, u4VcId,
                                        CLI_PTR_TO_I4 (args[++u1ArgIndex]));
            }
            IP6_TASK_LOCK ();
            break;

        case IP6_ROUTE_INFO_CONFIG:
            /* check whether the given address is a valid address */
            i4RAIfIndex = CLI_GET_IFINDEX ();
            MEMSET (&Ip6RARouteInfo, 0, sizeof (tIp6Addr));
            if (INET_ATON6 (args[0], &Ip6RARouteInfo) == 0)
            {
                CLI_SET_ERR (CLI_IP6_INVALID_ADDRESS);
                break;
            }

            /* args[1] Will contain the prefix length */
            /*]); Check for the valid prefix length */
            i4RARoutPrefixLen = *(INT4 *) (VOID *) args[1];
            if ((i4RARoutPrefixLen < 0) || (i4RARoutPrefixLen > 128))
            {
                CLI_SET_ERR (CLI_IP6_INVALID_PREFIXLEN);
                break;
            }
            i4RARoutePreference = CLI_PTR_TO_I4 (args[2]);
            if (args[3] == NULL)
            {
                u4RARouteLifetime = IP6_RA_ROUTE_MAX_LIFETIME;
            }
            else
            {
                u4RARouteLifetime = *(UINT4 *) (VOID *) (args[3]);
            }
            Ip6SetRARouteInfo (CliHandle, i4RAIfIndex, Ip6RARouteInfo,
                               i4RARoutPrefixLen, i4RARoutePreference,
                               u4RARouteLifetime);
            break;

        case IP6_ND_DEFAULT_ROUT_PREF:
            i4RAIfIndex = CLI_GET_IFINDEX ();
            i4DefRoutePreference = CLI_PTR_TO_I4 (args[0]);
            IpSetDefRoutePref (CliHandle, i4RAIfIndex, i4DefRoutePreference);
            break;

        case IP6_RA_ROUTE_SHOW:
            if (pu1IpCxtName == NULL)
            {
                u4VcId = VCM_INVALID_VC;
            }
            u1ArgIndex = 0;
            /* args[0] - Show all vlan flag */
            i4ShowAllVlan = CLI_PTR_TO_I4 (args[u1ArgIndex++]);
            if (i4ShowAllVlan == TRUE)
            {
                /* Vlan Id Arg Index ==> args [1]
                 */
                u1VlanIdArgIndex = u1ArgIndex;
                MEMSET (au1IfName, 0, IF_PREFIX_LEN);
                SNPRINTF ((CHR1 *) au1IfName, IF_PREFIX_LEN, "%s%d",
                          "vlan", *(INT4 *) args[u1VlanIdArgIndex]);
                Ip6ShowRARoutesForAllVlanInCxt (CliHandle,
                                                (UINT4) i4IfaceIndex, u4VcId,
                                                au1IfName);
                CliPrintf (CliHandle, "\r\n");
            }
            else
            {
                Ip6ShowRARoutesInCxt (CliHandle, (UINT4) i4IfaceIndex, u4VcId);
                CliPrintf (CliHandle, "\r\n");
            }
            break;

        case IP6_DEL_ROUTE_CONFIG:
            i4RAIfIndex = CLI_GET_IFINDEX ();
            MEMSET (&Ip6RARouteInfo, 0, sizeof (tIp6Addr));
            if (INET_ATON6 (args[0], &Ip6RARouteInfo) == 0)
            {
                CLI_SET_ERR (CLI_IP6_INVALID_ADDRESS);
                break;
            }

            /* args[1] Will contain the prefix length */
            /*]); Check for the valid prefix length */
            i4RARoutPrefixLen = *(INT4 *) (VOID *) args[1];
            if ((i4RARoutPrefixLen < 0) || (i4RARoutPrefixLen > 128))
            {
                CLI_SET_ERR (CLI_IP6_INVALID_PREFIXLEN);
                break;
            }
            Ip6DelRARouteInfo (CliHandle, i4RAIfIndex, Ip6RARouteInfo,
                               i4RARoutPrefixLen);
            break;

        case IP6_ND_TIMEOUT:
            Ip6NdCacheTimeout (CliHandle, *(INT4 *) (args[0]));
            break;

        case IP6_NO_ND_TIMEOUT:
            Ip6NdCacheTimeout (CliHandle, (INT4) ND_DEF_CACHE_TIMEOUT);
            break;

        default:
            CliPrintf (CliHandle, "%%Command Not Supported\r\n");
            break;
    }
    /*Reset context */
    Ip6ReleaseContext ();

    if (CLI_GET_ERR (&u4ErrorCode) == CLI_SUCCESS)
    {
        if ((u4ErrorCode > 0) && (u4ErrorCode < CLI_IP6_MAX_ERR))
        {
            CliPrintf (CliHandle, "\r%s", Ip6CliErrString[u4ErrorCode]);
            CLI_SET_CMD_STATUS (CLI_FAILURE);

        }
        CLI_SET_ERR (0);
    }

    /* Unlock the IP6 Task */
    IP6_TASK_UNLOCK ();
    Ip6GUnLock ();
    CliUnRegisterLock (CliHandle);
    return;
}
#else
VOID
cli_process_ip6_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    CliPrintf (CliHandle, "IP6  module not available\n");
}
#endif

/*********************************************************************
*  Function Name : Ip6SetRoutingStatus
*  Description   : Set Ipv6 forwarding status 
*  Input(s)      : CliHandle
*                  i1StatusFlag - Routing Status flag (enable/disable) 
*  Output(s)     :  
*  Return Values : None.
*********************************************************************/

VOID
Ip6SetRoutingStatus (tCliHandle CliHandle, INT1 i1StatusFlag)
{
    UINT4               u4ErrorCode;

    if (nmhTestv2Ipv6Forwarding (&u4ErrorCode, i1StatusFlag) == SNMP_FAILURE)
    {
        return;
    }

    if (nmhSetIpv6Forwarding (i1StatusFlag) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return;
    }

    if (i1StatusFlag == IP6_FORW_DISABLE)
    {
        CLI_SET_ERR (CLI_IP6_DISABLE_RT_PROTOCOLS);
        return;
    }
    return;
}

/*********************************************************************
*  Function Name : Ip6CliSetIfaceRoutingStatus
*  Description   : Set Ipv6 forwarding status 
*  Input(s)      : CliHandle
*                  i1StatusFlag - Routing Status flag (enable/disable) 
*  Output(s)     :  
*  Return Values : None.
*********************************************************************/

VOID
Ip6CliSetIfaceRoutingStatus (tCliHandle CliHandle, INT4 i4IfIndex,
                             INT1 i1StatusFlag)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4RetVal = 0;

    if (nmhTestv2Fsipv6IfForwarding
        (&u4ErrorCode, i4IfIndex, (INT4) i1StatusFlag) == SNMP_FAILURE)
    {
        return;
    }

    if (nmhSetFsipv6IfForwarding (i4IfIndex, (INT4) i1StatusFlag) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return;
    }

    nmhGetIpv6Forwarding (&i4RetVal);

    if ((i4RetVal == IP6_FORW_DISABLE) && (i1StatusFlag == IP6_IF_FORW_ENABLE))
    {
        CLI_SET_ERR (CLI_IP6_ROUTING_DISABLED);
    }
    return;
}

/*********************************************************************
*  Function Name : Ip6SetInterfaceAdminStatus 
*  Description   : Configuration of Ipv6 Interface 
*  Input(s)      : CliHandle
*                  u4Ip6IfIndex - Interface Index
*                  i4Ip6IfAdminStatus - Status to be set 
*  Output(s)     :  
*  Return Values : CLI_SUCCESS/CLI_FAILURE.
*********************************************************************/

INT1
Ip6SetInterfaceAdminStatus (tCliHandle CliHandle,
                            UINT4 u4Ip6IfIndex, INT4 i4Ip6IfAdminStatus)
{
    UINT4               u4ErrorCode;

    /* ADMIN STATUS */
    if (nmhTestv2Fsipv6IfAdminStatus
        (&u4ErrorCode, (INT4) u4Ip6IfIndex, i4Ip6IfAdminStatus) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IP6_INTERFACE_NOT_DISABLED);
        return CLI_FAILURE;
    }

    if (nmhSetFsipv6IfAdminStatus
        ((INT4) u4Ip6IfIndex, i4Ip6IfAdminStatus) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*********************************************************************
*  Function Name : Ip6SetAddr 
*  Description   : Configuring Ipv6 Address
*  Input(s)      : CliHandle
*                  u4Ip6IfIndex - Interface Index
*                  Ip6Addr - Ip6 Address to be set
*                  i4PrefixLen - Prefix Length
*                  i4AddrType - Adress type (Unicast,link-local,
*                               anycast,eui-based)
*  Output(s)     :  
*  Return Values : None.
*********************************************************************/

VOID
Ip6SetAddr (tCliHandle CliHandle,
            UINT4 u4Ip6IfIndex,
            tIp6Addr Ip6Addr, INT4 i4PrefixLen, INT4 i4AddrType)
{
    tSNMP_OCTET_STRING_TYPE Addr;
    tSNMP_OCTET_STRING_TYPE IfToken;
    UINT4               u4ErrorCode;
    INT4                i4Status;
    UINT1               au1AddrOctetList[IP6_ADDR_SIZE];
    UINT1               au1IfTokenOctetList[IP6_EUI_ADDRESS_LEN];
    BOOL1               b1IsCgaAddr = IP6_FALSE;

    MEMSET (au1AddrOctetList, IP6_ZERO, IP6_ADDR_SIZE);
    MEMSET (au1IfTokenOctetList, IP6_ZERO, IP6_EUI_ADDRESS_LEN);

    Addr.pu1_OctetList = au1AddrOctetList;
    MEMCPY (Addr.pu1_OctetList, &Ip6Addr, 16);
    Addr.i4_Length = 16;

    if (L2IwfIsPortInPortChannel (u4Ip6IfIndex) == CFA_SUCCESS)
    {
        CLI_SET_ERR (CLI_IP6_PORT_IN_AGG);
        return;
    }

    if (IS_ADDR_MULTI (Ip6Addr) || IS_ADDR_UNSPECIFIED (Ip6Addr))
    {
        CLI_SET_ERR (CLI_IP6_INVALID_ADDRESS);
        return;
    }

    /*
     * EUI-64 address configuration is supported only for CLI. When the
     * address type is set as EUI-64 form the fetch the interface
     * identifier value and form the complete IP6 address to be configured.
     */
    if (i4AddrType == ADDR6_UNSPECIFIED)
    {                            /* EUI-64 */
        /* Length should not be greater than 64. */
        if (i4PrefixLen != IP6_EUI_ADDRESS_PREFIX_LEN)
        {
            CLI_SET_ERR (CLI_IP6_INVALID_PREFIXLEN);
            return;
        }

        /* EUI-64 can be configured only if IPv6 is already enabled over
         * that interface */
        if ((nmhGetFsipv6IfAdminStatus
             ((INT4) u4Ip6IfIndex, &i4Status)
             == SNMP_FAILURE) || (i4Status != NETIPV6_ADMIN_UP))
        {
            CLI_SET_ERR (CLI_IP6_ADDR_NOT_CONFIGURED);
            return;
        }

        /* Form the Actual IP6 Address */
        IfToken.pu1_OctetList = au1IfTokenOctetList;
        IfToken.i4_Length = 0;

        if (nmhGetFsipv6IfToken ((INT4) u4Ip6IfIndex, &IfToken) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return;
        }

        MEMCPY (&Addr.pu1_OctetList[IP6_EUI_ADDRESS_LEN],
                IfToken.pu1_OctetList, IP6_EUI_ADDRESS_LEN);

        /* Also update the address type as UNICAST */
        i4AddrType = ADDR6_UNICAST;
    }

    if (IP6_CLI_ADDR_CGA == i4AddrType)
    {
        b1IsCgaAddr = IP6_TRUE;
        i4AddrType = IP6_ADDR_TYPE_UNICAST;
    }
    else if (IP6_CLI_ADDR_LINKLOCAL_CGA == i4AddrType)
    {
        b1IsCgaAddr = IP6_TRUE;
        i4AddrType = IP6_ADDR_TYPE_LINK_LOCAL;
    }

    /* ADMIN STATUS */
    if (nmhTestv2Fsipv6AddrAdminStatus (&u4ErrorCode,
                                        (INT4) u4Ip6IfIndex, &Addr,
                                        i4PrefixLen, CREATE_AND_WAIT)
        == SNMP_FAILURE)
    {
        return;
    }

    /* First the Admin Status Should be set to CREATE_AND_WAIT */
    if (nmhSetFsipv6AddrAdminStatus ((INT4) u4Ip6IfIndex,
                                     &Addr,
                                     i4PrefixLen,
                                     CREATE_AND_WAIT) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return;
    }

    if (b1IsCgaAddr != IP6_FALSE)
    {
        if (nmhTestv2Fsipv6AddrSENDCgaStatus (&u4ErrorCode,
                                              (INT4) u4Ip6IfIndex, &Addr,
                                              i4PrefixLen,
                                              b1IsCgaAddr) == SNMP_FAILURE)
        {
            nmhSetFsipv6AddrAdminStatus ((INT4) u4Ip6IfIndex, &Addr,
                                         i4PrefixLen, DESTROY);
            return;
        }

        if (nmhSetFsipv6AddrSENDCgaStatus ((INT4) u4Ip6IfIndex, &Addr,
                                           i4PrefixLen,
                                           b1IsCgaAddr) == SNMP_FAILURE)
        {
            nmhSetFsipv6AddrAdminStatus ((INT4) u4Ip6IfIndex, &Addr,
                                         i4PrefixLen, DESTROY);
            CLI_FATAL_ERROR (CliHandle);
            return;
        }
    }

    /* set to specified Address Type */
    if (nmhTestv2Fsipv6AddrType (&u4ErrorCode,
                                 (INT4) u4Ip6IfIndex,
                                 &Addr,
                                 i4PrefixLen, i4AddrType) == SNMP_FAILURE)
    {
        nmhSetFsipv6AddrAdminStatus ((INT4) u4Ip6IfIndex, &Addr, i4PrefixLen,
                                     DESTROY);
        return;
    }

    if (nmhSetFsipv6AddrType ((INT4) u4Ip6IfIndex,
                              &Addr, i4PrefixLen, i4AddrType) == SNMP_FAILURE)
    {
        nmhSetFsipv6AddrAdminStatus (u4Ip6IfIndex, &Addr, i4PrefixLen, DESTROY);
        CLI_FATAL_ERROR (CliHandle);
        return;
    }

    /* ADMIN STATUS */
    if (nmhTestv2Fsipv6AddrAdminStatus (&u4ErrorCode,
                                        u4Ip6IfIndex, &Addr,
                                        i4PrefixLen, ACTIVE) == SNMP_FAILURE)
    {
        nmhSetFsipv6AddrAdminStatus ((INT4) u4Ip6IfIndex, &Addr, i4PrefixLen,
                                     DESTROY);
        return;
    }

    /* First the Admin Status Should be set to 1 (up) */
    if (nmhSetFsipv6AddrAdminStatus ((INT4) u4Ip6IfIndex,
                                     &Addr,
                                     i4PrefixLen, ACTIVE) == SNMP_FAILURE)

    {
        nmhSetFsipv6AddrAdminStatus ((INT4) u4Ip6IfIndex, &Addr, i4PrefixLen,
                                     DESTROY);
        CLI_FATAL_ERROR (CliHandle);
        return;
    }

    return;
}

/*********************************************************************
*  Function Name : Ip6CliAddrDelete 
*  Description   : Deleting Ipv6 Address
*  Input(s)      : CliHandle
*                  u4Ip6IfIndex - Interface Index
*                  Ip6Addr - Ipv6 Addressto be deleted
*                  i4PrefixLen - Prefix Length
*                  i4AddrType - Address type
*  Output(s)     :  
*  Return Values : None.
*********************************************************************/

VOID
Ip6CliAddrDelete (tCliHandle CliHandle,
                  UINT4 u4Ip6IfIndex,
                  tIp6Addr Ip6Addr, UINT4 i4PrefixLen, INT4 i4AddrType)
{
    INT4                i4TempAddrType;
    UINT4               u4ErrorCode;
    tSNMP_OCTET_STRING_TYPE Addr;
    tSNMP_OCTET_STRING_TYPE IfToken;
    UINT1               au1AddrOctetList[IP6_ADDR_SIZE];
    UINT1               au1IfTokenOctetList[IP6_EUI_ADDRESS_LEN];

    MEMSET (au1AddrOctetList, IP6_ZERO, IP6_ADDR_SIZE);
    MEMSET (au1IfTokenOctetList, IP6_ZERO, IP6_EUI_ADDRESS_LEN);

    Addr.pu1_OctetList = au1AddrOctetList;
    MEMCPY (Addr.pu1_OctetList, &Ip6Addr, 16);
    Addr.i4_Length = 16;

    /*
     * EUI-64 address configuration is supported only for CLI. When the
     * address type is set as EUI-64 form the fetch the interface
     * identifier value and form the complete IP6 address to be configured.
     */
    if (i4AddrType == ADDR6_UNSPECIFIED)
    {                            /* EUI-64 */
        /* Length should not be greater than 64. */
        if (i4PrefixLen != IP6_EUI_ADDRESS_PREFIX_LEN)
        {
            CLI_SET_ERR (CLI_IP6_INVALID_PREFIXLEN);
            return;
        }

        /* Form the Actual IP6 Address */
        IfToken.pu1_OctetList = au1IfTokenOctetList;
        IfToken.i4_Length = 0;

        if (nmhGetFsipv6IfToken (u4Ip6IfIndex, &IfToken) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return;
        }

        MEMCPY (&Addr.pu1_OctetList[IP6_EUI_ADDRESS_LEN],
                IfToken.pu1_OctetList, IP6_EUI_ADDRESS_LEN);

        /* Also update the address type as UNICAST */
        i4AddrType = ADDR6_UNICAST;
    }

    if (i4AddrType != 0)
    {
        if (nmhGetFsipv6AddrType ((INT4) u4Ip6IfIndex, &Addr, i4PrefixLen,
                                  &i4TempAddrType) == SNMP_FAILURE)
        {
            CLI_SET_ERR (CLI_IP6_ADDR_ENTRY_NOT_CONFIGURED);
            return;
        }
        if (i4AddrType != i4TempAddrType)
        {
            CLI_SET_ERR (CLI_IP6_INVALID_ADDR_TYPE);
            return;
        }
    }

    /* ADMIN STATUS */
    if (nmhTestv2Fsipv6AddrAdminStatus (&u4ErrorCode,
                                        u4Ip6IfIndex, &Addr,
                                        i4PrefixLen,
                                        IP6FWD_DESTROY) == SNMP_FAILURE)
    {
        return;
    }

    /* First the Admin Status Should be set to 3 (invalid) */
    if (nmhSetFsipv6AddrAdminStatus (u4Ip6IfIndex,
                                     &Addr,
                                     i4PrefixLen,
                                     IP6FWD_DESTROY) == SNMP_FAILURE)

    {
        CLI_FATAL_ERROR (CliHandle);
        return;
    }

    return;
}

/*********************************************************************
*  Function Name : Ip6PingInCxt
*  Description   : Calls the Low level Routines from Ping table 
*  Input(s)      : CliHandle
*                  u4Ip4IfIndex - Interface Index
*                  u4VcId  - VRF Id  
*                  Ip6Addr - Ipv6 Destination to be pinged
*                  pu1PingData - Data (0x0000 - 0xffff) to be sent in
*                                the ping msg
*                  i4Count - Number of Ping msgs.
*                  i4PacketSize - Size of the msg.
*                  PingSrcAddr - Source Address to be used for the
*                                ping. Ping will go out only if
*                                this address (if given) is valid.
*                  i4Timeout 
*                  i4AddrType - Address type
*  Output(s)     :  
*  Return Values : None.
*********************************************************************/
VOID
Ip6PingInCxt (tCliHandle CliHandle,
              UINT4 u4Ip6IfIndex,
              UINT4 u4VcId,
              tIp6Addr Ip6Addr,
              UINT1 *pu1PingData,
              INT4 i4Count,
              INT4 i4PacketSize,
              tIp6Addr PingSrcAddr,
              INT4 i4Timeout, INT4 i4AddrType, UINT1 *pPingZoneId,
              UINT1 *au1HostName)
{
    UINT4               u4ErrorCode;
    tSNMP_OCTET_STRING_TYPE Addr;
    tSNMP_OCTET_STRING_TYPE Data;
    tSNMP_OCTET_STRING_TYPE SrcAddr;
    tSNMP_OCTET_STRING_TYPE ZoneId;
    tSNMP_OCTET_STRING_TYPE HostName;
    tNetIpv6RtInfo      NetIp6RtInfo;
#ifndef LNXIP6_WANTED            /* FSIP */
    tNd6CacheEntry     *pNd6c = NULL;
    tIp6If             *pIf6 = NULL;
    INT4                i4PingIndex = -1;
    UINT4               u4Index;
#else
    tNetIpv6RtInfoQueryMsg RtQuery;
    INT4                i4PingIndex = 0;
#endif
    INT4                i4Status = 0;
    UINT4               u4Ipv6PingSuccesses = 0;
    INT4                i4Ipv6PingSentCount;
    UINT4               u4Ipv6PingPercentageLoss;
    INT4                u4LocalCount;
    INT4                u4LocalPingCount = 0;
    UINT2               u2PingSuccessCount = 0;
    UINT2               u2PingLossCount = 0;
    INT4                i4RetValIpv6PingTime;
    UINT1               au1AddrOctetList[IP6_ADDR_SIZE];
    UINT1               au1SrcAddrOctetList[IP6_ADDR_SIZE];
    UINT1               au1ZoneIdOctetList[CFA_CLI_MAX_IF_NAME_LEN];
    INT1                ai1PingResponse[512];
    tDNSResolvedIpInfo  ResolvedIpInfo;
    INT4                i4RetVal = 0;

    MEMSET (&ResolvedIpInfo, 0, sizeof (tDNSResolvedIpInfo));

#ifdef LNXIP6_WANTED            /* LINUXIP */
    UNUSED_PARAM (pPingZoneId);
#endif
    MEMSET (au1AddrOctetList, IP6_ZERO, IP6_ADDR_SIZE);
    MEMSET (au1SrcAddrOctetList, IP6_ZERO, IP6_ADDR_SIZE);
    MEMSET (au1ZoneIdOctetList, IP6_ZERO, CFA_CLI_MAX_IF_NAME_LEN);

    MEMSET (&Addr, IP6_ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&HostName, IP6_ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&SrcAddr, IP6_ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&ZoneId, IP6_ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (ai1PingResponse, IP6_ZERO, 512);

    Addr.pu1_OctetList = au1AddrOctetList;
    HostName.pu1_OctetList = au1HostName;
    HostName.i4_Length = (INT4) STRLEN (au1HostName);

    MEMCPY (Addr.pu1_OctetList, &Ip6Addr, 16);
    Addr.i4_Length = 16;

    ZoneId.pu1_OctetList = au1ZoneIdOctetList;

#ifndef LNXIP6_WANTED            /* FSIP */
    for (i4PingIndex = 0; i4PingIndex < MAX_PING6_MAX_DST_LIMIT; i4PingIndex++)
    {
        i4RetVal =
            (INT4) nmhGetFsMIIpv6PingAdminStatus (i4PingIndex, &i4Status);
        if ((i4Status == IP6_PING_INVALID) || (i4RetVal == SNMP_FAILURE))
        {
            break;
        }
    }

    if (i4PingIndex == MAX_PING6_MAX_DST_LIMIT)
    {
        CliPrintf (CliHandle,
                   "\r\n maximum number of ping sessions reached\r\n");
        return;
    }
#else
    for (i4PingIndex = 0; i4PingIndex < PING6_MAX_INSTANCES; i4PingIndex++)
    {
        nmhGetFsMIIpv6PingAdminStatus (i4PingIndex, &i4Status);
        if (i4Status == IP6_PING_INVALID)
        {
            break;
        }
    }

    if (i4PingIndex == PING6_MAX_INSTANCES)
    {
        return;
    }
#endif

    if (nmhTestv2FsMIIpv6PingAdminStatus (&u4ErrorCode, i4PingIndex, 4) ==
        SNMP_FAILURE)
    {
        return;
    }

    if (nmhSetFsMIIpv6PingAdminStatus (i4PingIndex, IP6_PING_VALID) ==
        SNMP_FAILURE)
    {
        nmhSetFsMIIpv6PingAdminStatus (i4PingIndex, IP6_PING_INVALID);
        CLI_FATAL_ERROR (CliHandle);
        return;
    }

    if (nmhTestv2FsMIIpv6PingContextId (&u4ErrorCode, i4PingIndex, u4VcId) ==
        SNMP_FAILURE)
    {
        nmhSetFsMIIpv6PingAdminStatus (i4PingIndex, IP6_PING_INVALID);
        CLI_FATAL_ERROR (CliHandle);
        return;
    }

    if (nmhSetFsMIIpv6PingContextId (i4PingIndex, u4VcId) == SNMP_FAILURE)
    {
        nmhSetFsMIIpv6PingAdminStatus (i4PingIndex, IP6_PING_INVALID);
        CLI_FATAL_ERROR (CliHandle);
        return;
    }
    if (MEMCMP (&Ip6Addr, &gZeroIp6Addr, sizeof (tIp6Addr)) != 0)
    {
        if (nmhTestv2FsMIIpv6PingDest (&u4ErrorCode, i4PingIndex, &Addr) ==
            SNMP_FAILURE)
        {
            nmhSetFsMIIpv6PingAdminStatus (i4PingIndex, IP6_PING_INVALID);
            return;
        }

        if (nmhSetFsMIIpv6PingDest (i4PingIndex, &Addr) == SNMP_FAILURE)
        {
            nmhSetFsMIIpv6PingAdminStatus (i4PingIndex, IP6_PING_INVALID);
            CLI_FATAL_ERROR (CliHandle);
            return;
        }
    }
    else
    {
        if (nmhTestv2FsMIIpv6PingHostName (&u4ErrorCode, i4PingIndex,
                                           &HostName) == SNMP_FAILURE)
        {
            nmhSetFsMIIpv6PingAdminStatus (i4PingIndex, IP6_PING_INVALID);
            return;
        }

        if (nmhSetFsMIIpv6PingHostName (i4PingIndex, &HostName) == SNMP_FAILURE)
        {
            nmhSetFsMIIpv6PingAdminStatus (i4PingIndex, IP6_PING_INVALID);
            return;
        }

    }

    if (nmhTestv2FsMIIpv6PingTries (&u4ErrorCode, i4PingIndex, i4Count) ==
        SNMP_FAILURE)
    {
        nmhSetFsMIIpv6PingAdminStatus (i4PingIndex, IP6_PING_INVALID);
        return;
    }

    if (nmhSetFsMIIpv6PingTries (i4PingIndex, i4Count) == SNMP_FAILURE)
    {
        nmhSetFsMIIpv6PingAdminStatus (i4PingIndex, IP6_PING_INVALID);
        CLI_FATAL_ERROR (CliHandle);
        return;
    }

    if (nmhTestv2FsMIIpv6PingRcvTimeout (&u4ErrorCode, i4PingIndex, i4Timeout)
        == SNMP_FAILURE)
    {
        nmhSetFsMIIpv6PingAdminStatus (i4PingIndex, IP6_PING_INVALID);
        return;
    }

    if (nmhSetFsMIIpv6PingRcvTimeout (i4PingIndex, i4Timeout) == SNMP_FAILURE)
    {
        nmhSetFsMIIpv6PingAdminStatus (i4PingIndex, IP6_PING_INVALID);
        CLI_FATAL_ERROR (CliHandle);
        return;
    }

    if (nmhTestv2FsMIIpv6PingSize (&u4ErrorCode, i4PingIndex, i4PacketSize) ==
        SNMP_FAILURE)
    {
        nmhSetFsMIIpv6PingAdminStatus (i4PingIndex, IP6_PING_INVALID);
        return;
    }

    if (nmhSetFsMIIpv6PingSize (i4PingIndex, i4PacketSize) == SNMP_FAILURE)
    {
        nmhSetFsMIIpv6PingAdminStatus (i4PingIndex, IP6_PING_INVALID);
        CLI_FATAL_ERROR (CliHandle);
        return;
    }

    if (pu1PingData != NULL)
    {
        Data.pu1_OctetList = pu1PingData;
        Data.i4_Length = STRLEN (pu1PingData);

        if (nmhTestv2FsMIIpv6PingData (&u4ErrorCode, i4PingIndex, &Data) ==
            SNMP_FAILURE)
        {
            nmhSetFsMIIpv6PingAdminStatus (i4PingIndex, IP6_PING_INVALID);
            return;
        }

        if (nmhSetFsMIIpv6PingData (i4PingIndex, &Data) == SNMP_FAILURE)
        {
            nmhSetFsMIIpv6PingAdminStatus (i4PingIndex, IP6_PING_INVALID);
            CLI_FATAL_ERROR (CliHandle);
            return;
        }

    }

    if (!IS_ADDR_UNSPECIFIED (PingSrcAddr))
    {
        SrcAddr.pu1_OctetList = au1SrcAddrOctetList;

        MEMCPY (SrcAddr.pu1_OctetList, &PingSrcAddr, 16);
        SrcAddr.i4_Length = 16;

        if (nmhTestv2FsMIIpv6PingSrcAddr (&u4ErrorCode, i4PingIndex, &SrcAddr)
            == SNMP_FAILURE)
        {
            nmhSetFsMIIpv6PingAdminStatus (i4PingIndex, IP6_PING_INVALID);
            return;
        }

        if (nmhSetFsMIIpv6PingSrcAddr (i4PingIndex, &SrcAddr) == SNMP_FAILURE)
        {
            nmhSetFsMIIpv6PingAdminStatus (i4PingIndex, IP6_PING_INVALID);
            CLI_FATAL_ERROR (CliHandle);
            return;
        }

    }
    if ((MEMCMP (&Ip6Addr, &gZeroIp6Addr, sizeof (tIp6Addr)) == 0))
    {
        IP6_TASK_UNLOCK ();
        Ip6GUnLock ();

        /* here 0 denotes (2nd arguement) its a blocking call */
        i4RetVal = FsUtlIPvXResolveHostName (au1HostName,
                                             DNS_NONBLOCK, &ResolvedIpInfo);
        Ip6GLock ();
        IP6_TASK_LOCK ();

        if (i4RetVal == DNS_IN_PROGRESS)
        {
            nmhSetFsMIIpv6PingAdminStatus (i4PingIndex, IP6_PING_INVALID);
            CliPrintf (CliHandle, "\r%% Host name resolution in Progress\r\n");
            return;
        }
        else if (i4RetVal == DNS_CACHE_FULL)
        {
            nmhSetFsMIIpv6PingAdminStatus (i4PingIndex, IP6_PING_INVALID);
            CliPrintf (CliHandle,
                       "\r%% DNS cache full, cannot resolve at the moment\r\n");
            return;
        }
        else if ((i4RetVal == DNS_NOT_RESOLVED) ||
                 (ResolvedIpInfo.Resolv6Addr.u1AddrLen == 0))
        {
            nmhSetFsMIIpv6PingAdminStatus (i4PingIndex, IP6_PING_INVALID);
            CliPrintf (CliHandle, "\r%% Cannot resolve the host name\r\n");
            return;
        }

        MEMCPY (&Ip6Addr, ResolvedIpInfo.Resolv6Addr.au1Addr,
                ResolvedIpInfo.Resolv6Addr.u1AddrLen);
    }

    MEMCPY (Addr.pu1_OctetList, &Ip6Addr, IPVX_IPV6_ADDR_LEN);
    Addr.i4_Length = IPVX_IPV6_ADDR_LEN;

    if (MEMCMP (&Ip6Addr, &gZeroIp6Addr, sizeof (tIp6Addr)) != 0)
    {
        if (nmhTestv2FsMIIpv6PingDest (&u4ErrorCode, i4PingIndex, &Addr) ==
            SNMP_FAILURE)
        {
            nmhSetFsMIIpv6PingAdminStatus (i4PingIndex, IP6_PING_INVALID);
            return;
        }

        if (nmhSetFsMIIpv6PingDest (i4PingIndex, &Addr) == SNMP_FAILURE)
        {
            nmhSetFsMIIpv6PingAdminStatus (i4PingIndex, IP6_PING_INVALID);
            CLI_FATAL_ERROR (CliHandle);
            return;
        }
    }

    /* Set If Index, If Only the address is Link Local */
    if ((IS_ADDR_LLOCAL (Ip6Addr)) || (IS_ADDR_MULTI (Ip6Addr)))
    {

        if (nmhTestv2FsMIIpv6PingIfIndex
            (&u4ErrorCode, i4PingIndex, u4Ip6IfIndex) == SNMP_FAILURE)
        {
            nmhSetFsMIIpv6PingAdminStatus (i4PingIndex, IP6_PING_INVALID);
            return;
        }

        if (nmhSetFsMIIpv6PingIfIndex (i4PingIndex, u4Ip6IfIndex) ==
            SNMP_FAILURE)
        {
            nmhSetFsMIIpv6PingAdminStatus (i4PingIndex, IP6_PING_INVALID);
            CLI_FATAL_ERROR (CliHandle);
            return;
        }
#ifndef LNXIP6_WANTED
        if (pPingZoneId != NULL)
        {
            ZoneId.pu1_OctetList = pPingZoneId;
            ZoneId.i4_Length = STRLEN (pPingZoneId);

            if (nmhTestv2Fsipv6PingZoneId (&u4ErrorCode,
                                           i4PingIndex,
                                           &ZoneId) == SNMP_FAILURE)
            {
                nmhSetFsMIIpv6PingAdminStatus (i4PingIndex, IP6_PING_INVALID);
                return;
            }
            if (nmhSetFsipv6PingZoneId (i4PingIndex, &ZoneId) == SNMP_FAILURE)
            {
                nmhSetFsMIIpv6PingAdminStatus (i4PingIndex, IP6_PING_INVALID);
                CLI_FATAL_ERROR (CliHandle);
                return;
            }
        }
#endif
    }
    else if (!(IS_ADDR_LOOPBACK (Ip6Addr)))
    {
#ifndef LNXIP6_WANTED            /* FSIP */
        pIf6 = Ip6GetRouteInCxt (u4VcId, &Ip6Addr, &pNd6c, &NetIp6RtInfo);
        if (pIf6 == NULL)
        {
            SPRINTF ((CHR1 *) ai1PingResponse,
                     "\r\nping6 %s Destination Unreachable\r\n",
                     Ip6PrintNtop ((tIp6Addr *) (VOID *) Addr.pu1_OctetList));
            mmi_printf ("%s", ai1PingResponse);
            nmhSetFsMIIpv6PingAdminStatus (i4PingIndex, IP6_PING_INVALID);
            return;
        }

        u4Index = pIf6->u4Index;
        nmhSetFsMIIpv6PingIfIndex (i4PingIndex, u4Index);
#else
        MEMSET (&NetIp6RtInfo, 0, sizeof (tNetIpv6RtInfo));
        MEMSET (&RtQuery, 0, sizeof (tNetIpv6RtInfoQueryMsg));

        RtQuery.u4ContextId = u4VcId;
        RtQuery.u1QueryFlag = RTM6_QUERIED_FOR_NEXT_HOP;
        Ip6AddrCopy (&NetIp6RtInfo.Ip6Dst, &Ip6Addr);
        NetIp6RtInfo.u1Prefixlen = IP6_ADDR_MAX_PREFIX;
        if (NetIpv6GetRoute (&RtQuery, &NetIp6RtInfo) == NETIPV6_FAILURE)
        {
            SPRINTF ((CHR1 *) ai1PingResponse,
                     "\r\nping6 %s Destination Unreachable\r\n",
                     Ip6PrintNtop ((tIp6Addr *) (VOID *) Addr.pu1_OctetList));
            mmi_printf ("%s", ai1PingResponse);
            nmhSetFsMIIpv6PingAdminStatus (i4PingIndex, IP6_PING_INVALID);
            return;
        }

#endif
    }

    if (nmhTestv2FsMIIpv6PingInterval
        (&u4ErrorCode, i4PingIndex, CLI_PING6_DEFAULT_WAIT) == SNMP_FAILURE)
    {
        nmhSetFsMIIpv6PingAdminStatus (i4PingIndex, IP6_PING_INVALID);
        CLI_FATAL_ERROR (CliHandle);
        return;
    }

    if (nmhSetFsMIIpv6PingInterval
        (i4PingIndex, CLI_PING6_DEFAULT_WAIT) == SNMP_FAILURE)
    {
        nmhSetFsMIIpv6PingAdminStatus (i4PingIndex, IP6_PING_INVALID);
        CLI_FATAL_ERROR (CliHandle);
        return;
    }

#ifndef LNXIP6_WANTED            /* FSIP */
    if (nmhTestv2FsMIIpv6PingDestAddrType
        (&u4ErrorCode, i4PingIndex, i4AddrType) == SNMP_FAILURE)
    {
        nmhSetFsMIIpv6PingAdminStatus (i4PingIndex, IP6_PING_INVALID);
        CLI_FATAL_ERROR (CliHandle);
        return;
    }

    if (nmhSetFsMIIpv6PingDestAddrType
        (i4PingIndex, i4AddrType) == SNMP_FAILURE)
    {
        nmhSetFsMIIpv6PingAdminStatus (i4PingIndex, IP6_PING_INVALID);
        CLI_FATAL_ERROR (CliHandle);
        return;
    }
#else
    UNUSED_PARAM (i4AddrType);
#endif

    SPRINTF ((CHR1 *) ai1PingResponse, "ping6 %s %d bytes of data",
             Ip6PrintNtop ((tIp6Addr *) (VOID *) Addr.pu1_OctetList),
             i4PacketSize);
    mmi_printf ("%s\r\n", ai1PingResponse);
    for (u4LocalCount = 0; u4LocalCount < i4Count; u4LocalCount++)
    {
        nmhSetFsMIIpv6PingAdminStatus (i4PingIndex, IP6_PING_DISABLE);
        nmhSetFsMIIpv6PingTries (i4PingIndex, 1);
        nmhSetFsMIIpv6PingAdminStatus (i4PingIndex, IP6_PING_ENABLE);
#ifndef LNXIP6_WANTED            /* FSIP */
        IP6_TASK_UNLOCK ();
        Ip6GUnLock ();
/*         Delay for given Number of Seconds before sending another
         * ping request. */
        OsixDelayTask (CLI_PING6_DEFAULT_WAIT * SYS_NUM_OF_TIME_UNITS_IN_A_SEC);
        Ip6GLock ();
        IP6_TASK_LOCK ();
#endif
        nmhGetFsMIIpv6PingSentCount (i4PingIndex, &i4Ipv6PingSentCount);
        nmhGetFsMIIpv6PingSuccesses (i4PingIndex, &u4Ipv6PingSuccesses);
        nmhGetFsMIIpv6PingAverageTime (i4PingIndex, &i4RetValIpv6PingTime);
        if (u4Ipv6PingSuccesses == 1)
        {
            SPRINTF ((CHR1 *) ai1PingResponse,
                     "%d bytes from %s : ICMP6_seq = %u\r\n",
                     i4PacketSize,
                     Ip6PrintNtop ((tIp6Addr *) (VOID *) Addr.pu1_OctetList),
                     u4LocalCount);

            mmi_printf ("%s", ai1PingResponse);

            u4LocalPingCount++;
            u2PingSuccessCount++;
        }
        else if (u4Ipv6PingSuccesses > 1)
        {
            SPRINTF ((CHR1 *) ai1PingResponse,
                     "%d bytes from %s : ICMP6_seq = %u",
                     i4PacketSize,
                     Ip6PrintNtop ((tIp6Addr *) (VOID *) Addr.pu1_OctetList),
                     u4LocalCount);

            mmi_printf ("%s", ai1PingResponse);
            SPRINTF ((CHR1 *) ai1PingResponse, "Ping DUP...!!\r\n");

            mmi_printf ("%s", ai1PingResponse);

            u4LocalPingCount++;
            u2PingSuccessCount++;
        }
        else
        {
            SPRINTF ((CHR1 *) ai1PingResponse, "Request Timed Out\r\n");
            mmi_printf ("%s", ai1PingResponse);
            u2PingLossCount++;
            u4LocalPingCount++;
        }
    }
    u4Ipv6PingPercentageLoss = (u2PingLossCount * 100) / u4LocalPingCount;
    mmi_printf ("\r\n");
    mmi_printf (" --- Ping Statistics ---\r\n");

    mmi_printf
        ("%d Packets Transmitted, %d Packets Received, %d%% Packet Loss\r\n",
         u4LocalPingCount, u2PingSuccessCount, u4Ipv6PingPercentageLoss);
    /* Clear the Ping Entry */
    nmhSetFsMIIpv6PingAdminStatus (i4PingIndex, IP6_PING_INVALID);
    return;
}

/*********************************************************************
*  Function Name : Ip6RouteShowInCxt
*  Description   : Route Table Entries to be displayed. 
*  Input(s)      : CliHandle
*                  u4VcId -  VRF Id
*  Output(s)     :  
*  Return Values : None.
*********************************************************************/
VOID
Ip6RouteShowInCxt (tCliHandle CliHandle, UINT4 u4VcId, INT4 i4Protocol,
                   UINT1 u1IpPresent, tIp6Addr RouteDest, INT4 i4PrefixLen)
{

    INT4                i4RetVal = 0;
    INT4                i4RouteProtocol = 0;
    INT4                i4CurrDestType = INET_ADDR_TYPE_IPV4;
    INT4                i4NextDestType = 0;
    INT4                i4CurrRtHopType = INET_ADDR_TYPE_IPV4;
    INT4                i4RouteAddrType = 0;
    INT4                i4NextRtHopType = 0;
    UINT4               u4ShowAllCxt = FALSE;
    UINT4               u4Preference;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    UINT4               u4NextRtPfxLength = 0;
    UINT4               u4CurrRtPfxLength = 0;
    UINT4               u4ContextId = 0;
    UINT4               u4DisVcId = VCM_INVALID_VC;
    UINT4               u4CurrentVcId = VCM_INVALID_VC;
    UINT4               au4RtPolicy[IP6_TWO];
    UINT1               au1PrintBuf[100];
    INT1               *pi1IfName = NULL;
    UINT1               u1RouteCode = IP6_ZERO;
    UINT1               u1ECMPFlag = FALSE;
    UINT1               au1ContextName[VCMALIAS_MAX_ARRAY_LEN];
    UINT1               au1IfName[CFA_CLI_MAX_IF_NAME_LEN];
    UINT1               au1NextRouteDest[IP6_ADDR_SIZE];
    UINT1               au1CurrRouteDest[IP6_ADDR_SIZE];
    UINT1               au1NextRtNextHop[IP6_ADDR_SIZE];
    UINT1               au1CurrRtNextHop[IP6_ADDR_SIZE];
    UINT1               au1TmpRtNextHop[IP6_ADDR_SIZE];
    UINT1               u1PrintOnce = 0;
    UINT1               u1IpMatching = FALSE;
    UINT1               u1BestFlag = 0;
    INT4                i4RtPreference = 0;

    tSNMP_OCTET_STRING_TYPE NextRouteDest;
    tSNMP_OCTET_STRING_TYPE CurrRouteDest;
    tSNMP_OCTET_STRING_TYPE NextRtNextHop;
    tSNMP_OCTET_STRING_TYPE CurrRtNextHop;
    tSNMP_OCTET_STRING_TYPE TmpRtNextHop;

    tSNMP_OID_TYPE      CurrRoutePolicy;
    tSNMP_OID_TYPE      NextRoutePolicy;

    tIp6Addr            TmpNxtHop;
    tIp6RtEntry         InRtInfo;
    tIp6RtEntry        *pIp6Route = NULL;
    MEMSET (au1NextRouteDest, ZERO, IP6_ADDR_SIZE);
    MEMSET (au1CurrRouteDest, IP6_UINT1_ALL_ONE, IP6_ADDR_SIZE);
    MEMSET (au1NextRtNextHop, ZERO, IP6_ADDR_SIZE);
    MEMSET (au1CurrRtNextHop, IP6_UINT1_ALL_ONE, IP6_ADDR_SIZE);
    MEMSET (au4RtPolicy, ZERO, sizeof (au4RtPolicy));
    MEMSET (au1TmpRtNextHop, ZERO, IP6_ADDR_SIZE);

    NextRouteDest.pu1_OctetList = au1NextRouteDest;
    CurrRouteDest.pu1_OctetList = au1CurrRouteDest;
    NextRtNextHop.pu1_OctetList = au1NextRtNextHop;
    CurrRtNextHop.pu1_OctetList = au1CurrRtNextHop;
    CurrRoutePolicy.pu4_OidList = au4RtPolicy;
    NextRoutePolicy.pu4_OidList = au4RtPolicy;
    TmpRtNextHop.pu1_OctetList = au1TmpRtNextHop;

    NextRouteDest.i4_Length = 0;
    CurrRouteDest.i4_Length = IP6_ADDR_SIZE;
    NextRtNextHop.i4_Length = 0;
    CurrRtNextHop.i4_Length = IP6_ADDR_SIZE;
    CurrRoutePolicy.u4_Length = 0;
    NextRoutePolicy.u4_Length = 0;
    TmpRtNextHop.i4_Length = 0;

    if (u4VcId == VCM_INVALID_VC)
    {
        u4ShowAllCxt = TRUE;
        i4RetVal = nmhGetFirstIndexFsMIStdInetCidrRouteTable
            ((INT4 *) &u4VcId, &i4NextDestType, &NextRouteDest,
             &u4NextRtPfxLength, &NextRoutePolicy,
             &i4NextRtHopType, &NextRtNextHop);
    }
    else
    {
        if (VcmIsL3VcExist (u4VcId) == VCM_FALSE)
        {
            CliPrintf (CliHandle, "\r%% Invalid VRF \r\n");
            return;
        }
        u4ContextId = u4VcId;
        i4RetVal = nmhGetNextIndexFsMIStdInetCidrRouteTable
            ((INT4) u4VcId, (INT4 *) &u4VcId,
             i4CurrDestType, &i4NextDestType,
             &CurrRouteDest, &NextRouteDest,
             u4CurrRtPfxLength, &u4NextRtPfxLength,
             &CurrRoutePolicy, &NextRoutePolicy,
             i4CurrRtHopType, &i4NextRtHopType, &CurrRtNextHop, &NextRtNextHop);

        if (u4ContextId != u4VcId)
        {
            /*No entries in this context */
            return;
        }
    }

    MEMSET (au1IfName, 0, CFA_CLI_MAX_IF_NAME_LEN);
    pi1IfName = (INT1 *) &au1IfName[0];

    CliPrintf (CliHandle, "IPv6 Routing Table:\r\n\n");
    CliPrintf (CliHandle, "Codes : C - Connected, S - Static\r\n");
    CliPrintf (CliHandle, "O - OSPF, R - RIP, B - BGP, I - ISIS, E - ECMP\r\n");
    CliPrintf (CliHandle,
               "IA - OSPF inter area, ia - OSPF Intra Area, N1 - OSPF NSSA external type 1,\r\n");
    CliPrintf (CliHandle,
               "N2 - OSPF NSSA external type 2, E1 - OSPF external type 1,\r\n");
    CliPrintf (CliHandle,
               "E2 - OSPF external type 2 L1 - ISIS Level1, L2 - ISIS Level2, ia - ISIS Inter Area\r\n");
    while (i4RetVal == SNMP_SUCCESS)
    {
        if (i4NextDestType == INET_ADDR_TYPE_IPV6)
        {
            nmhGetFsMIStdInetCidrRouteStatus ((INT4) u4VcId, i4NextDestType,
                                              &NextRouteDest, u4NextRtPfxLength,
                                              &NextRoutePolicy, i4NextRtHopType,
                                              &NextRtNextHop, &i4RetVal);

            nmhGetFsMIStdInetCidrRouteProto ((INT4) u4VcId, i4NextDestType,
                                             &NextRouteDest, u4NextRtPfxLength,
                                             &NextRoutePolicy, i4NextRtHopType,
                                             &NextRtNextHop, &i4RouteProtocol);

            /* Check the routes for the following parameters before displaying 
             * 1. Row Status is Active
             * 2. Check if protocol is mentioned and if specified check with the
             *    route protocol 
             * 3. Check the prefix Length and if specified check with the route
             *    prefix length
             * 4. Check whether IPv6 address is provided and if specified check
             *    the destination IPv6 address
             * */

            u1IpMatching = FALSE;

            if (u1IpPresent == TRUE)
            {
                if (MEMCMP (RouteDest.u1_addr, NextRouteDest.pu1_OctetList,
                            sizeof (tIp6Addr)) == 0)
                {
                    u1IpMatching = TRUE;
                }
            }
            else
            {
                /* No need to check for Destination IP */
                u1IpMatching = TRUE;
            }

            if ((i4RetVal == IP6FWD_ACTIVE) && ((i4Protocol == i4RouteProtocol)
                                                || (i4Protocol == IP6_ZERO))
                && ((i4PrefixLen == IP6_ZERO)
                    || (i4PrefixLen == (INT4) u4NextRtPfxLength))
                && (u1IpMatching == TRUE))
            {

                if (u4DisVcId != u4VcId)
                {
                    VcmGetAliasName (u4VcId, au1ContextName);
                    CliPrintf (CliHandle, "\r\n");
                    CliPrintf (CliHandle, "VRF    Name:      %s\r\n",
                               au1ContextName);
                    CliPrintf (CliHandle, "---------------\r\n\n");

                    u4DisVcId = u4VcId;
                }

                switch (i4RouteProtocol)
                {
                    case IANAIP_ROUTE_PROTO_LOCAL:
                        i4RouteProtocol = IP6_LOCAL_PROTOID;
                        u1RouteCode = 'C';
                        break;
                    case IANAIP_ROUTE_PROTO_NETMGMT:
                        i4RouteProtocol = IP6_NETMGMT_PROTOID;
                        u1RouteCode = 'S';
                        break;
                    case IANAIP_ROUTE_PROTO_OSPF:
                        i4RouteProtocol = IP6_OSPF_PROTOID;
                        u1RouteCode = 'O';
                        break;
                    case IANAIP_ROUTE_PROTO_RIP:
                        i4RouteProtocol = IP6_RIP_PROTOID;
                        u1RouteCode = 'R';
                        break;
                    case IANAIP_ROUTE_PROTO_BGP:
                        i4RouteProtocol = IP6_BGP_PROTOID;
                        u1RouteCode = 'B';
                        break;
                    case IANAIP_ROUTE_PROTO_ISIS:
                        i4RouteProtocol = IP6_ISIS_PROTOID;
                        u1RouteCode = 'I';
                }
                nmhGetFsMIStdInetCidrRouteIfIndex ((INT4) u4VcId,
                                                   i4NextDestType,
                                                   &NextRouteDest,
                                                   u4NextRtPfxLength,
                                                   &NextRoutePolicy,
                                                   i4NextRtHopType,
                                                   &NextRtNextHop, &i4RetVal);

                CfaCliConfGetIfName ((UINT4) i4RetVal, pi1IfName);

                nmhGetFsMIStdInetCidrRouteMetric1 ((INT4) u4VcId,
                                                   i4NextDestType,
                                                   &NextRouteDest,
                                                   u4NextRtPfxLength,
                                                   &NextRoutePolicy,
                                                   i4NextRtHopType,
                                                   &NextRtNextHop, &i4RetVal);

                nmhGetFsMIIpv6Preference ((INT4) u4VcId, i4RouteProtocol,
                                          &u4Preference);

                UtilRtm6SetContext (u4VcId);
                nmhGetFsipv6RouteAddrType (&NextRouteDest,
                                           (INT4) u4NextRtPfxLength,
                                           i4RouteProtocol, &NextRtNextHop,
                                           &i4RouteAddrType);

                nmhGetFsipv6RoutePreference (&NextRouteDest,
                                             (INT4) u4NextRtPfxLength,
                                             i4RouteProtocol, &NextRtNextHop,
                                             &i4RtPreference);

                UtilRtm6ResetContext ();
                MEMCPY (&(InRtInfo.dst), NextRouteDest.pu1_OctetList,
                        sizeof (tIp6Addr));
                InRtInfo.u1Prefixlen = u4NextRtPfxLength;
                Rtm6GetBestRouteEntryInCxt (u4VcId, &InRtInfo, &pIp6Route);
                if (pIp6Route != NULL)
                {
                    if (pIp6Route->u4Flag & RTM6_ECMP_RT)
                    {
                        if ((pIp6Route->i1Proto != STATIC_ID)
                            && (pIp6Route->u4Metric == (UINT4) i4RetVal))
                        {
                            u1ECMPFlag = TRUE;
                        }
                        else if ((pIp6Route->i1Proto == STATIC_ID) &&
                                 (pIp6Route->u1Preference == i4RtPreference))

                        {
                            u1ECMPFlag = TRUE;
                        }
                        else
                        {
                            u1ECMPFlag = FALSE;
                        }
                    }
                    else
                    {
                        u1ECMPFlag = FALSE;
                    }
                    MEMSET (&(TmpNxtHop), IPVX_UINT1_ALL_ONE,
                            sizeof (tIp6Addr));
                    Ip6AddrCopy ((tIp6Addr *) & (TmpNxtHop),
                                 &(pIp6Route->nexthop));

                    MEMCPY ((TmpRtNextHop.pu1_OctetList), &(TmpNxtHop),
                            sizeof (tIp6Addr));
                    TmpRtNextHop.i4_Length = IP6_ADDR_SIZE;

                    /*Compare the best routes nexthop with the current incoming nexthop */
                    if (MEMCMP
                        (TmpRtNextHop.pu1_OctetList,
                         NextRtNextHop.pu1_OctetList, IP6_ADDR_SIZE) == 0)
                    {
                        /* set the bestflag if current destination is best route */
                        u1BestFlag = 1;
                    }
                    else if (u1ECMPFlag == TRUE)
                    {
                        /* set the bestflag for ECMP route */
                        u1BestFlag = 1;
                    }

                }
                if ((u1PrintOnce == 0) && (u1BestFlag == 1))
                {
                    if (i4RouteProtocol == IP6_ISIS_PROTOID)
                    {
                        if (pIp6Route->u1MetricType == IP_ISIS_LEVEL1)
                        {
                            CliPrintf (CliHandle, "%cL1   %s/%d   \n",
                                       u1RouteCode,
                                       Ip6PrintNtop ((tIp6Addr *) (VOID *)
                                                     NextRouteDest.
                                                     pu1_OctetList),
                                       (INT4) u4NextRtPfxLength);

                        }
                        else if (pIp6Route->u1MetricType == IP_ISIS_LEVEL2)
                        {
                            CliPrintf (CliHandle, "%cL2   %s/%d   \n",
                                       u1RouteCode,
                                       Ip6PrintNtop ((tIp6Addr *) (VOID *)
                                                     NextRouteDest.
                                                     pu1_OctetList),
                                       (INT4) u4NextRtPfxLength);
                        }
                        else if (pIp6Route->u1MetricType == IP_ISIS_INTER_AREA)
                        {
                            CliPrintf (CliHandle, "%cia   %s/%d   \n",
                                       u1RouteCode,
                                       Ip6PrintNtop ((tIp6Addr *) (VOID *)
                                                     NextRouteDest.
                                                     pu1_OctetList),
                                       (INT4) u4NextRtPfxLength);
                        }
                    }
                    else if (i4RouteProtocol == IP6_OSPF_PROTOID)
                    {
                        if (pIp6Route->u1MetricType == IPV6_OSPFV3_TYPE_1_EXT)
                        {
                            CliPrintf (CliHandle, "%c E1 %s/%d   \n",
                                       u1RouteCode,
                                       Ip6PrintNtop ((tIp6Addr *) (VOID *)
                                                     NextRouteDest.
                                                     pu1_OctetList),
                                       (INT4) u4NextRtPfxLength);

                        }
                        else if (pIp6Route->u1MetricType ==
                                 IPV6_OSPFV3_TYPE_2_EXT)
                        {
                            CliPrintf (CliHandle, "%c E2 %s/%d   \n",
                                       u1RouteCode,
                                       Ip6PrintNtop ((tIp6Addr *) (VOID *)
                                                     NextRouteDest.
                                                     pu1_OctetList),
                                       (INT4) u4NextRtPfxLength);
                        }
                        else if (pIp6Route->u1MetricType ==
                                 IPV6_OSPFV3_TYPE_1_NSSA_EXT)
                        {
                            CliPrintf (CliHandle, "%c N1 %s/%d   \n",
                                       u1RouteCode,
                                       Ip6PrintNtop ((tIp6Addr *) (VOID *)
                                                     NextRouteDest.
                                                     pu1_OctetList),
                                       (INT4) u4NextRtPfxLength);
                        }
                        else if (pIp6Route->u1MetricType ==
                                 IPV6_OSPFV3_TYPE_2_NSSA_EXT)
                        {
                            CliPrintf (CliHandle, "%c N2 %s/%d   \n",
                                       u1RouteCode,
                                       Ip6PrintNtop ((tIp6Addr *) (VOID *)
                                                     NextRouteDest.
                                                     pu1_OctetList),
                                       (INT4) u4NextRtPfxLength);
                        }
                        else if (pIp6Route->u1MetricType ==
                                 IPV6_OSPFV3_INTER_AREA)
                        {
                            CliPrintf (CliHandle, "%c IA %s/%d   \n",
                                       u1RouteCode,
                                       Ip6PrintNtop ((tIp6Addr *) (VOID *)
                                                     NextRouteDest.
                                                     pu1_OctetList),
                                       (INT4) u4NextRtPfxLength);
                        }
                        else
                        {
                            CliPrintf (CliHandle, "%c ia %s/%d   \n",
                                       u1RouteCode,
                                       Ip6PrintNtop ((tIp6Addr *) (VOID *)
                                                     NextRouteDest.
                                                     pu1_OctetList),
                                       (INT4) u4NextRtPfxLength);
                        }

                    }
                    else
                    {
                        CliPrintf (CliHandle, "%c   %s/%d   \n",
                                   u1RouteCode,
                                   Ip6PrintNtop ((tIp6Addr *) (VOID *)
                                                 NextRouteDest.pu1_OctetList),
                                   (INT4) u4NextRtPfxLength);
                    }

                }

                if (u1ECMPFlag == TRUE)
                {
                    CliPrintf (CliHandle, "[E]");
                }
                else if ((u1PrintOnce == 0) && (u1BestFlag == 1))
                {
                    CliPrintf (CliHandle, "   ");
                }
                if (u1ECMPFlag == TRUE)
                {
                    CliPrintf (CliHandle, "           [%u/%u]",
                               (UINT4) i4RetVal, i4RtPreference);

                    CliPrintf (CliHandle, "       via %s, %s",
                               Ip6PrintNtop ((tIp6Addr *) (VOID *)
                                             NextRtNextHop.pu1_OctetList),
                               pi1IfName);
                }
                else if ((u1BestFlag == 1) && (u1PrintOnce == 0))
                {
                    CfaCliConfGetIfName (pIp6Route->u4Index, pi1IfName);

                    SPRINTF ((CHR1 *) au1PrintBuf, "[%u/%u]",
                             (UINT4) pIp6Route->u4Metric,
                             pIp6Route->u1Preference);
                    CliPrintf (CliHandle, "     %-16s", au1PrintBuf);

                    if (pIp6Route->u1NullFlag == 1)
                    {
                        CliPrintf (CliHandle,
                                   "       is a summary route,[NULL0]");
                    }
                    else
                    {
                        SPRINTF ((CHR1 *) au1PrintBuf, "%s %s, %s", "via",
                                 Ip6PrintNtop ((tIp6Addr *) (VOID *)
                                               &(pIp6Route->nexthop)),
                                 pi1IfName);
                        CliPrintf (CliHandle, "%s", au1PrintBuf);
                    }
                }

                if (i4RouteAddrType == ADDR6_ANYCAST)
                {
                    CliPrintf (CliHandle, "  [ANY]");
                }
                i4RetVal = 0;
                nmhGetFsMIStdInetCidrRouteMetric5 ((INT4) u4VcId,
                                                   i4NextDestType,
                                                   &NextRouteDest,
                                                   u4NextRtPfxLength,
                                                   &NextRoutePolicy,
                                                   i4NextRtHopType,
                                                   &NextRtNextHop, &i4RetVal);
                if ((i4RetVal == IPVX_ONE)
                    && (i4RouteProtocol == IP6_NETMGMT_PROTOID))
                {
                    /*to display dhcpv6 relay pd route  */
                    CliPrintf (CliHandle, "  [DHCPv6 PD]");
                }

                if (((u1PrintOnce == 0) && (u1BestFlag == 1))
                    || (u1ECMPFlag == TRUE))
                {
                    u1PrintOnce = 1;
                    CliPrintf (CliHandle, "\r\n");
                    u4PagingStatus = CliPrintf (CliHandle, "\r\n");
                }
                if (u4PagingStatus == CLI_FAILURE)
                {
                    /* User pressed 'q' at more prompt so break */
                    break;
                }
            }
        }
        u4CurrentVcId = u4VcId;
        i4CurrDestType = i4NextDestType;
        MEMCPY (CurrRouteDest.pu1_OctetList, NextRouteDest.pu1_OctetList,
                NextRouteDest.i4_Length);
        CurrRouteDest.i4_Length = NextRouteDest.i4_Length;
        u4CurrRtPfxLength = u4NextRtPfxLength;
        MEMCPY (CurrRoutePolicy.pu4_OidList, NextRoutePolicy.pu4_OidList,
                IPVX_TWO);
        i4CurrRtHopType = i4NextRtHopType;
        MEMCPY (CurrRtNextHop.pu1_OctetList,
                NextRtNextHop.pu1_OctetList, NextRtNextHop.i4_Length);
        CurrRtNextHop.i4_Length = NextRtNextHop.i4_Length;

        i4RetVal = nmhGetNextIndexFsMIStdInetCidrRouteTable
            ((INT4) u4VcId, (INT4 *) &u4VcId,
             i4CurrDestType, &i4NextDestType,
             &CurrRouteDest, &NextRouteDest,
             u4CurrRtPfxLength, &u4NextRtPfxLength,
             &CurrRoutePolicy, &NextRoutePolicy,
             i4CurrRtHopType, &i4NextRtHopType, &CurrRtNextHop, &NextRtNextHop);
        if ((MEMCMP
             (CurrRouteDest.pu1_OctetList, NextRouteDest.pu1_OctetList,
              sizeof (tIp6Addr)) != 0) ||
            (CurrRouteDest.i4_Length != NextRouteDest.i4_Length) ||
            (u4CurrRtPfxLength != u4NextRtPfxLength) ||
            (u4CurrentVcId != u4VcId))

        {
            u1PrintOnce = 0;
            u1BestFlag = 0;
        }

        if ((u4CurrentVcId != u4VcId) && (u4ShowAllCxt == FALSE))
        {
            /*Finished displaying entries belonging to one VRF */
            break;
        }
    }                            /*end of while */

    return;

}

/*********************************************************************
 * *  Function Name : Ip6FrtRouteShowInCxt
 * *  Description   : Failed Route Table Entries to be displayed.
 * *  Input(s)      : CliHandle
 * *  Output(s)     :
 * *  Return Values : None.
 * *********************************************************************/
VOID
Ip6FrtRouteShowInCxt (tCliHandle CliHandle)
{
    INT4                i4RetVal = 0;
    INT4                i4RouteProtocol = 0;
    INT4                i4NextDestType = 0;
    INT4                i4RouteAddrType = 0;
    INT4                i4NextRtHopType = 0;
    INT4                i4PagingStatus = CLI_SUCCESS;
    UINT4               u4Preference;
    UINT4               u4NextRtPfxLength = 0;
    UINT4               u4DisVcId = VCM_INVALID_VC;
    UINT4               au4RtPolicy[IP6_TWO];
    INT1               *pi1IfName = NULL;
    UINT1               u1RouteCode = IP6_ZERO;
    UINT1               u1ECMPFlag = FALSE;
    UINT1               au1ContextName[VCMALIAS_MAX_ARRAY_LEN];
    UINT1               au1IfName[CFA_CLI_MAX_IF_NAME_LEN];
    UINT1               au1NextRouteDest[IP6_ADDR_SIZE];
    UINT1               au1CurrRouteDest[IP6_ADDR_SIZE];
    UINT1               au1NextRtNextHop[IP6_ADDR_SIZE];
    UINT1               au1CurrRtNextHop[IP6_ADDR_SIZE];
    UINT1               u1PrintOnce = 0;

    tSNMP_OCTET_STRING_TYPE NextRouteDest;
    tSNMP_OCTET_STRING_TYPE NextRtNextHop;

    tSNMP_OID_TYPE      NextRoutePolicy;
    tRtm6FrtInfo       *pRt;
    MEMSET (au1NextRouteDest, ZERO, IP6_ADDR_SIZE);
    MEMSET (au1CurrRouteDest, IP6_UINT1_ALL_ONE, IP6_ADDR_SIZE);
    MEMSET (au1NextRtNextHop, ZERO, IP6_ADDR_SIZE);
    MEMSET (au1CurrRtNextHop, IP6_UINT1_ALL_ONE, IP6_ADDR_SIZE);
    MEMSET (au4RtPolicy, ZERO, sizeof (au4RtPolicy));

    NextRouteDest.pu1_OctetList = au1NextRouteDest;
    NextRtNextHop.pu1_OctetList = au1NextRtNextHop;

    NextRouteDest.i4_Length = 0;
    NextRtNextHop.i4_Length = 0;
    UINT4               u4VcId;

    MEMSET (au1IfName, 0, CFA_CLI_MAX_IF_NAME_LEN);
    pi1IfName = (INT1 *) &au1IfName[0];

    pRt = Rtm6GetFirstFrtEntry ();
    while (pRt != NULL)
    {
        i4NextDestType = INET_ADDR_TYPE_IPV6;
        i4NextRtHopType = INET_ADDR_TYPE_IPV6;

        NextRoutePolicy.pu4_OidList = au4RtPolicy;
        NextRoutePolicy.u4_Length = 0;
        IPVX_GET_NULL_ROUTE_POLICY (&NextRoutePolicy);
        Ip6AddrCopy ((tIp6Addr *) (VOID *) &NextRouteDest.pu1_OctetList,
                     &pRt->DestPrefix);
        u4NextRtPfxLength = pRt->u1PrefixLen;
        i4RouteProtocol = pRt->i1Proto;
        Ip6AddrCopy ((tIp6Addr *) (VOID *) &NextRtNextHop.pu1_OctetList,
                     &pRt->NextHopAddr);

        u4VcId = pRt->u4CxtId;
        i4RouteAddrType = pRt->u1AddrType;

        if (u4DisVcId != u4VcId)
        {
            VcmGetAliasName (u4VcId, au1ContextName);
            CliPrintf (CliHandle, "VRF    Name:      %s\r\n", au1ContextName);
            CliPrintf (CliHandle, "---------------\r\n\n");
            CliPrintf (CliHandle, "IPv6 Routing Table:\r\n\n");
            CliPrintf (CliHandle, "Codes : C - Connected, S - Static\r\n");
            CliPrintf (CliHandle,
                       "        O - OSPF, R - RIP, B - BGP, I - ISIS\r\n");
            u4DisVcId = u4VcId;
        }

        switch (i4RouteProtocol)
        {
            case IANAIP_ROUTE_PROTO_LOCAL:
                i4RouteProtocol = IP6_LOCAL_PROTOID;
                u1RouteCode = 'C';
                break;
            case IANAIP_ROUTE_PROTO_NETMGMT:
                i4RouteProtocol = IP6_NETMGMT_PROTOID;
                u1RouteCode = 'S';
                break;
            case IANAIP_ROUTE_PROTO_OSPF:
                i4RouteProtocol = IP6_OSPF_PROTOID;
                u1RouteCode = 'O';
                break;
            case IANAIP_ROUTE_PROTO_RIP:
                i4RouteProtocol = IP6_RIP_PROTOID;
                u1RouteCode = 'R';
                break;
            case IANAIP_ROUTE_PROTO_BGP:
                i4RouteProtocol = IP6_BGP_PROTOID;
                u1RouteCode = 'B';
                break;
            case IANAIP_ROUTE_PROTO_ISIS:
                i4RouteProtocol = IP6_ISIS_PROTOID;
                u1RouteCode = 'I';
            default:
                break;
        }

        CfaCliConfGetIfName (pRt->u4Index, pi1IfName);

        nmhGetFsMIIpv6Preference ((INT4) u4VcId, i4RouteProtocol,
                                  &u4Preference);

        if (pRt->u4Flag & RTM6_ECMP_RT)
        {
            u1ECMPFlag = TRUE;
        }
        else
        {
            u1ECMPFlag = FALSE;
        }

        if (i4RouteProtocol == IP6_ISIS_PROTOID)
        {
            if (pRt->u1MetricType == IP_ISIS_LEVEL1)
            {
                CliPrintf (CliHandle, "%cL1   %s/%d   \n",
                           u1RouteCode,
                           Ip6PrintNtop ((tIp6Addr *) (VOID *)
                                         &NextRouteDest.
                                         pu1_OctetList),
                           (INT4) u4NextRtPfxLength);
            }
            else if (pRt->u1MetricType == IP_ISIS_LEVEL2)
            {
                CliPrintf (CliHandle, "%cL2   %s/%d   \n",
                           u1RouteCode,
                           Ip6PrintNtop ((tIp6Addr *) (VOID *)
                                         &NextRouteDest.
                                         pu1_OctetList),
                           (INT4) u4NextRtPfxLength);
            }
            else if (pRt->u1MetricType == IP_ISIS_INTER_AREA)
            {
                CliPrintf (CliHandle, "%cia   %s/%d   \n",
                           u1RouteCode,
                           Ip6PrintNtop ((tIp6Addr *) (VOID *)
                                         &NextRouteDest.
                                         pu1_OctetList),
                           (INT4) u4NextRtPfxLength);
            }
        }
        else
        {
            CliPrintf (CliHandle, "%c   %s/%d   \n",
                       u1RouteCode,
                       Ip6PrintNtop ((tIp6Addr *) (VOID *)
                                     &NextRouteDest.pu1_OctetList),
                       (INT4) u4NextRtPfxLength);
        }
        if (u1ECMPFlag == TRUE)
        {
            CliPrintf (CliHandle, "[E]");
        }
        else
        {
            CliPrintf (CliHandle, "   ");
        }

        if ((u1PrintOnce == 0) || (u1ECMPFlag == TRUE))
        {
            CliPrintf (CliHandle, "           [%u/%u]",
                       (UINT4) pRt->u4Metric, u4Preference);
            CliPrintf (CliHandle, "       via %s, %s",
                       Ip6PrintNtop ((tIp6Addr *) (VOID *)
                                     &NextRtNextHop.pu1_OctetList), pi1IfName);
        }

        if (i4RouteAddrType == ADDR6_ANYCAST)
        {
            CliPrintf (CliHandle, "  [ANY]");
        }

        nmhGetFsMIStdInetCidrRouteMetric5 ((INT4) u4VcId,
                                           i4NextDestType,
                                           &NextRouteDest,
                                           u4NextRtPfxLength,
                                           &NextRoutePolicy,
                                           i4NextRtHopType,
                                           &NextRtNextHop, &i4RetVal);
        if (i4RetVal == IPVX_ONE)
        {
            /*to display dhcpv6 relay pd route  */
            CliPrintf (CliHandle, "  [DHCPv6 PD]");
        }
        CliPrintf (CliHandle, "\r\n");

        i4PagingStatus = CliPrintf (CliHandle, "\r\n");
        if (i4PagingStatus == CLI_FAILURE)
        {
            /* User pressed 'q' at more prompt so break */
            break;
        }
        pRt = Rtm6GetNextFrtEntry (pRt);
        if (pRt != NULL)
        {
            u4DisVcId = pRt->u4CxtId;
        }
    }
    return;
}

/*********************************************************************
*  Function Name : Ip6ShowGetInterface
*  Description   : Retrives the Interface Info to be displayed.
*  Input(s)      : pu1Buffer - non-NULL buffer, where the Interface info
*                              are returned. This buffer must be big
*                              enough to hold atleast one interface
*                              information.
*                  u4BufLen - Length of pu1Buffer
*                  pCookie  - information about interface to be fetched are
*                             stored.
*  Output(s)     : pCookie  - information about next interface info to be
*                             fetched are stored.
*  Return Values : None.
*********************************************************************/
PRIVATE INT4
Ip6ShowGetInterface (tShowIp6Interface * pShowIp6Interface, INT4 i4Ipv6IfIndex)
{
    INT4                i4RsVal = SNMP_FAILURE;
    INT4                i4AdvFlag = 0;
    INT1                i1RetVal = SNMP_FAILURE;
#ifdef LNXIP6_WANTED
    tSNMP_OCTET_STRING_TYPE DomainNameOne;
    tSNMP_OCTET_STRING_TYPE DomainNameTwo;
    tSNMP_OCTET_STRING_TYPE DomainNameThree;
#endif

#ifdef TUNNEL_WANTED
#ifndef LNXIP6_WANTED
    tIp6If             *pIf6 = NULL;
#else
    tLip6If            *pIf6 = NULL;
#endif
#endif

#ifdef LNXIP6_WANTED
    DomainNameOne.pu1_OctetList = pShowIp6Interface->au1DomainNameOne;
    DomainNameOne.i4_Length = IP6_DOMAIN_NAME_LEN;
    DomainNameTwo.pu1_OctetList = pShowIp6Interface->au1DomainNameTwo;
    DomainNameTwo.i4_Length = IP6_DOMAIN_NAME_LEN;
    DomainNameThree.pu1_OctetList = pShowIp6Interface->au1DomainNameThree;
    DomainNameThree.i4_Length = IP6_DOMAIN_NAME_LEN;
#endif

    nmhGetFsipv6IfType (i4Ipv6IfIndex, &pShowIp6Interface->i4IfType);
    nmhGetFsipv6IfAdminStatus (i4Ipv6IfIndex,
                               &pShowIp6Interface->i4AdminStatus);
    nmhGetFsipv6IfOperStatus (i4Ipv6IfIndex, &pShowIp6Interface->i4OperStatus);
    nmhGetIpv6IfEffectiveMtu (i4Ipv6IfIndex, &pShowIp6Interface->u4Mtu);
    nmhGetFsipv6IfDestUnreachableMsg (i4Ipv6IfIndex,
                                      &pShowIp6Interface->
                                      i4Icmp6DstUnReachable);
    nmhGetFsipv6IfIcmpErrInterval (i4Ipv6IfIndex,
                                   &pShowIp6Interface->i4Icmp6ErrInterval);
    nmhGetFsipv6IfIcmpTokenBucketSize (i4Ipv6IfIndex,
                                       &pShowIp6Interface->i4Icmp6BucketSize);
    nmhGetFsipv6IfRoutingStatus (i4Ipv6IfIndex,
                                 &pShowIp6Interface->i4IfForwdStatus);

    nmhGetFsipv6IfUnnumAssocIPIf (i4Ipv6IfIndex,
                                  (INT4 *) &(pShowIp6Interface->
                                             u4UnnumAssocIPv6If));

    nmhGetFsipv6IfRedirectMsg (i4Ipv6IfIndex,
                               &pShowIp6Interface->i4Icmp6RedirectMsg);

#ifdef TUNNEL_WANTED
    if (pShowIp6Interface->i4IfType == IP6_TUNNEL_INTERFACE_TYPE)
    {
#ifndef LNXIP6_WANTED
        if (i4Ipv6IfIndex < IP6_MAX_LOGICAL_IF_INDEX + IP6_ONE)
        {
            pIf6 = gIp6GblInfo.apIp6If[i4Ipv6IfIndex];
        }
#else
        pIf6 = Lip6UtlGetIfEntry (i4Ipv6IfIndex);
#endif
        if (pIf6 != NULL)
        {
            /* Display the tunnel related parameters */
            pShowIp6Interface->u4SrcAddr =
                OSIX_NTOHL (pIf6->pTunlIf->tunlSrc.u4_addr[3]);

            pShowIp6Interface->u4DestAddr =
                OSIX_NTOHL (pIf6->pTunlIf->tunlDst.u4_addr[3]);

            pShowIp6Interface->u1TunlType = pIf6->pTunlIf->u1TunlType;
        }
    }
#endif /* TUNNEL_WANTED */

    nmhGetFsipv6IfDADRetries (i4Ipv6IfIndex, &pShowIp6Interface->i4DadRetries);
    nmhGetFsipv6IfAdvIntOpt (i4Ipv6IfIndex, &pShowIp6Interface->i4RAInterval);
    nmhGetFsipv6IfAdvSrcLLAdr (i4Ipv6IfIndex,
                               &pShowIp6Interface->i4RALinkLocalStatus);
#ifdef LNXIP6_WANTED
    nmhGetFsipv6IfDomainNameOne (i4Ipv6IfIndex, &DomainNameOne);
    MEMCPY (pShowIp6Interface->au1DomainNameOne, DomainNameOne.pu1_OctetList,
            IP6_ADDR_SIZE);
    nmhGetFsipv6IfDomainNameTwo (i4Ipv6IfIndex, &DomainNameTwo);
    MEMCPY (pShowIp6Interface->au1DomainNameTwo, DomainNameTwo.pu1_OctetList,
            IP6_ADDR_SIZE);
    nmhGetFsipv6IfDomainNameThree (i4Ipv6IfIndex, &DomainNameThree);
    MEMCPY (pShowIp6Interface->au1DomainNameThree,
            DomainNameThree.pu1_OctetList, IP6_ADDR_SIZE);
#endif

    if (i4Ipv6IfIndex < IP6_MAX_LOGICAL_IF_INDEX + IP6_ONE)
    {
        pIf6 = gIp6GblInfo.apIp6If[i4Ipv6IfIndex];
    }
    if ((pIf6 != NULL) && (pIf6->u1Ipv6IfFwdOperStatus == IP6_IF_FORW_DISABLE))
    {
        nmhGetFsipv6IfDefRouterTime (i4Ipv6IfIndex,
                                     &pShowIp6Interface->i4RouterLifeTime);
        nmhGetFsipv6IfRaRDNSSRowStatus (i4Ipv6IfIndex,
                                        &pShowIp6Interface->i4RouteAdvRDNSS);

        nmhGetFsipv6IfRadvRDNSSOpen (i4Ipv6IfIndex,
                                     &pShowIp6Interface->i4RouteAdvRDNSSStatus);

        if (nmhGetFsipv6IfRouterAdvStatus (i4Ipv6IfIndex,
                                           &pShowIp6Interface->
                                           i4RouterAdvtStatus) == SNMP_SUCCESS)
        {
            nmhGetFsipv6IfReachableTime (i4Ipv6IfIndex,
                                         &pShowIp6Interface->i4RouterReachTime);

            nmhGetFsipv6IfRetransmitTime (i4Ipv6IfIndex,
                                          &pShowIp6Interface->
                                          i4RouterRetransTime);

            nmhGetFsipv6IfMaxRouterAdvTime (i4Ipv6IfIndex,
                                            &pShowIp6Interface->
                                            i4RouterMaxAdvtTime);
            nmhGetFsipv6IfRouterAdvFlags (i4Ipv6IfIndex, &i4AdvFlag);

            if ((i4AdvFlag & IP6_IF_ROUT_ADV_M_BIT) ||
                ((i4AdvFlag & IP6_IF_ROUT_ADV_BOTH_BIT) ==
                 IP6_IF_ROUT_ADV_BOTH_BIT))
            {
                pShowIp6Interface->i4RouterAdvtManagedFlag =
                    IPVX_TRUTH_VALUE_TRUE;
            }
            else
            {
                pShowIp6Interface->i4RouterAdvtManagedFlag =
                    IPVX_TRUTH_VALUE_FALSE;
            }

            if ((i4AdvFlag & IP6_IF_ROUT_ADV_NO_BIT) == IP6_IF_ROUT_ADV_NO_BIT)
            {
                pShowIp6Interface->i4RouterAdvtOConfigFlag =
                    IPVX_TRUTH_VALUE_FALSE;
            }
            else if ((i4AdvFlag & IP6_IF_ROUT_ADV_O_BIT) ==
                     IP6_IF_ROUT_ADV_O_BIT)
            {
                pShowIp6Interface->i4RouterAdvtOConfigFlag =
                    IPVX_TRUTH_VALUE_TRUE;
            }
            else
            {
                if (i4AdvFlag == IP6_IF_ROUT_ADV_BOTH_BIT)
                {
                    pShowIp6Interface->i4RouterAdvtOConfigFlag =
                        IPVX_TRUTH_VALUE_TRUE;
                }
                else
                {
                    pShowIp6Interface->i4RouterAdvtOConfigFlag =
                        IPVX_TRUTH_VALUE_FALSE;
                }
            }

            IP6_TASK_UNLOCK ();
            nmhGetIpv6RouterAdvertLinkMTU (i4Ipv6IfIndex,
                                           (UINT4 *) &(pShowIp6Interface->
                                                       u4RaLinkMTU));
            IP6_TASK_LOCK ();
            nmhGetFsipv6IfHopLimit (i4Ipv6IfIndex,
                                    (INT4 *) &(pShowIp6Interface->
                                               u4RACurHopLimit));

        }
    }
    else
    {
        /* Get the Data form the RA Table */
        /* IPv6Lock will be taken in the IPvx Lowlevel So Relase Now and
         * before return take */
        IP6_TASK_UNLOCK ();

        i1RetVal = nmhGetIpv6RouterAdvertRowStatus (i4Ipv6IfIndex, &i4RsVal);
        if (i1RetVal == SNMP_SUCCESS)
        {
            nmhGetIpv6RouterAdvertDefaultLifetime (i4Ipv6IfIndex,
                                                   (UINT4 *)
                                                   &(pShowIp6Interface->
                                                     i4RouterLifeTime));
            nmhGetFsipv6IfRaRDNSSRowStatus (i4Ipv6IfIndex,
                                            &(pShowIp6Interface->
                                              i4RouteAdvRDNSS));

            nmhGetFsipv6IfRadvRDNSSOpen (i4Ipv6IfIndex,
                                         &(pShowIp6Interface->
                                           i4RouteAdvRDNSSStatus));
            nmhGetFsipv6IfRaRDNSSPreference (i4Ipv6IfIndex,
                                             (INT4 *) &(pShowIp6Interface->
                                                        u4RDNSSPreference));
            nmhGetFsipv6IfRaRDNSSLifetime (i4Ipv6IfIndex,
                                           &(pShowIp6Interface->
                                             u4RDNSSLifetime));

            i1RetVal = nmhGetIpv6RouterAdvertSendAdverts (i4Ipv6IfIndex,
                                                          &(pShowIp6Interface->
                                                            i4RouterAdvtStatus));
            if (i1RetVal == SNMP_SUCCESS)
            {
                nmhGetIpv6RouterAdvertOtherConfigFlag (i4Ipv6IfIndex,
                                                       &(pShowIp6Interface->
                                                         i4RouterAdvtOConfigFlag));
                nmhGetIpv6RouterAdvertManagedFlag (i4Ipv6IfIndex,
                                                   &(pShowIp6Interface->
                                                     i4RouterAdvtManagedFlag));
                nmhGetIpv6RouterAdvertReachableTime (i4Ipv6IfIndex,
                                                     (UINT4 *)
                                                     &(pShowIp6Interface->
                                                       i4RouterReachTime));

                nmhGetIpv6RouterAdvertRetransmitTime (i4Ipv6IfIndex,
                                                      (UINT4 *)
                                                      &(pShowIp6Interface->
                                                        i4RouterRetransTime));

                nmhGetIpv6RouterAdvertMinInterval (i4Ipv6IfIndex,
                                                   (UINT4 *)
                                                   &(pShowIp6Interface->
                                                     i4RouterMinAdvtTime));

                nmhGetIpv6RouterAdvertMaxInterval (i4Ipv6IfIndex,
                                                   (UINT4 *)
                                                   &(pShowIp6Interface->
                                                     i4RouterMaxAdvtTime));

                nmhGetIpv6RouterAdvertLinkMTU (i4Ipv6IfIndex,
                                               (UINT4 *) &(pShowIp6Interface->
                                                           u4RaLinkMTU));
                nmhGetIpv6RouterAdvertCurHopLimit (i4Ipv6IfIndex,
                                                   (UINT4 *)
                                                   &(pShowIp6Interface->
                                                     u4RACurHopLimit));
            }
        }
        IP6_TASK_LOCK ();
    }

    return IP6_SUCCESS;
}

/*********************************************************************
*  Function Name : Ip6ShowGetIntfAddr
*  Description   : Retrives the Interface Address Entries to be displayed. 
*  Input(s)      : pu1Buffer - non-NULL buffer, where the routes are returned.
*                              This buffer must be big enough to hold
*                              atleast one route.
*                  u4BufLen - Length of pu1Buffer
*                  pCookie  - information about route to be fetched are
*                             stored.
*  Output(s)     : pCookie  - information about next route to be fetched are
*                             stored.
*  Return Values : None.
*********************************************************************/
PRIVATE INT4
Ip6ShowGetIntfAddr (UINT1 *pu1Buffer, UINT4 u4BufLen, UINT4 u4IfIndex,
                    tShowIp6IntfAddrCookie * pCookie)
{
    tShowIp6IntfAddr   *pShowIp6IntfAddr = NULL;
    tShowIp6IntfAddrAttr *pShowIp6IntfAddrAttr = NULL;
    tIp6McastInfo      *pMcastInfo;
#ifndef LNXIP6_WANTED            /* FSIP */
    tIp6LlocalInfo     *pCurrLlocal;
    tIp6AddrInfo       *pAddr6Info;
    tTMO_SLL_NODE      *pSllInfo;
    tIp6If             *pIf6 = NULL;
#else /* Linux IP */
    tLip6AddrNode      *pAddr6Info = NULL;
    tLip6If            *pIf6Entry = NULL;
#endif
    UINT4               u4NoOfAddr = 0;
    INT4                i4IsAddrMatch = OSIX_FALSE;

#ifndef LNXIP6_WANTED
    /* Check for Valid Interface . */
    pIf6 = Ip6ifGetEntry (u4IfIndex);
    if (pIf6 == NULL)
    {
        /* Invalid Interface */
        return IP6_FAILURE;
    }
#else
    if (Lip6UtlIfEntryExists (u4IfIndex) == OSIX_FAILURE)
    {
        /* Invalid Interface */
        return IP6_FAILURE;
    }
#endif

    pShowIp6IntfAddr = (tShowIp6IntfAddr *) (VOID *) pu1Buffer;
    pShowIp6IntfAddrAttr = &pShowIp6IntfAddr->aIntfAddrAttr[0];

    MEMSET (pu1Buffer, 0, u4BufLen);
    u4NoOfAddr = (u4BufLen - ((sizeof (tShowIp6IntfAddr)) -
                              (sizeof (tShowIp6IntfAddrAttr)))) /
        (sizeof (tShowIp6IntfAddrAttr));

#ifndef LNXIP6_WANTED            /* FSIP */
    if (pCookie->u1AddrType == ADDR6_LLOCAL)
    {
        if (TMO_SLL_Count (&pIf6->lla6Ilist) > 0)
        {
            TMO_SLL_Scan (&pIf6->lla6Ilist, pSllInfo, tTMO_SLL_NODE *)
            {
                pCurrLlocal = IP6_LLADDR_PTR_FROM_SLL (pSllInfo);
                if (IS_ADDR_UNSPECIFIED (pCookie->Ip6Addr))
                {
                    i4IsAddrMatch = OSIX_TRUE;
                }

                if ((i4IsAddrMatch == OSIX_FALSE) &&
                    (Ip6AddrMatch (&pCookie->Ip6Addr, &pCurrLlocal->ip6Addr,
                                   IP6_ADDR_SIZE_IN_BITS) == TRUE))
                {
                    /* Match Address Found. Return Next Address. */
                    i4IsAddrMatch = OSIX_TRUE;
                    continue;
                }

                if (i4IsAddrMatch == OSIX_FALSE)
                {
                    continue;
                }

                /* Need to return this Address. */
                if (u4NoOfAddr == 0)
                {
                    /* Update the Cookie and return. */
                    Ip6AddrCopy (&pCookie->Ip6Addr, &pCurrLlocal->ip6Addr);
                    return IP6_SUCCESS;
                }

                Ip6AddrCopy (&pShowIp6IntfAddrAttr->Ip6Addr,
                             &pCurrLlocal->ip6Addr);
                pShowIp6IntfAddrAttr->u1Status = pCurrLlocal->u1Status;
                pShowIp6IntfAddrAttr->u1Scope = ADDR6_SCOPE_LLOCAL;
                pShowIp6IntfAddrAttr->u1CfgMethod = pCurrLlocal->u1ConfigMethod;

                /* Increment the count. */
                pShowIp6IntfAddr->u4NoOfAddr++;
                pShowIp6IntfAddrAttr++;
                u4NoOfAddr--;
            }
        }
    }
    else if (pCookie->u1AddrType == ADDR6_UNICAST)
    {
        if (TMO_SLL_Count (&pIf6->addr6Ilist) > 0)
        {
            TMO_SLL_Scan (&pIf6->addr6Ilist, pSllInfo, tTMO_SLL_NODE *)
            {
                pAddr6Info = IP6_ADDR_PTR_FROM_SLL (pSllInfo);

                if (pAddr6Info->u1AddrType != ADDR6_UNICAST)
                {
                    continue;
                }
                if ((IS_ADDR_UNSPECIFIED (pCookie->Ip6Addr)) &&
                    (pCookie->u1PrefixLen == 0))
                {
                    i4IsAddrMatch = OSIX_TRUE;
                }

                if ((i4IsAddrMatch == OSIX_FALSE) &&
                    (Ip6AddrMatch (&pCookie->Ip6Addr, &pAddr6Info->ip6Addr,
                                   IP6_ADDR_SIZE_IN_BITS) == TRUE))
                {
                    /* Match Address Found. Return Next Address. */
                    i4IsAddrMatch = OSIX_TRUE;
                    continue;
                }

                if (i4IsAddrMatch == OSIX_FALSE)
                {
                    continue;
                }

                /* Need to return this Address. */
                if (u4NoOfAddr == 0)
                {
                    /* Update the Cookie and return. */
                    Ip6AddrCopy (&pCookie->Ip6Addr, &pAddr6Info->ip6Addr);
                    pCookie->u1PrefixLen = pAddr6Info->u1PrefLen;
                    return IP6_SUCCESS;
                }

                Ip6AddrCopy (&pShowIp6IntfAddrAttr->Ip6Addr,
                             &pAddr6Info->ip6Addr);
                pShowIp6IntfAddrAttr->u1PrefixLen = pAddr6Info->u1PrefLen;
                pShowIp6IntfAddrAttr->u1Status = pAddr6Info->u1Status;
                pShowIp6IntfAddrAttr->u1Scope = pAddr6Info->u1AddrScope;
                pShowIp6IntfAddrAttr->u1CfgMethod = pAddr6Info->u1ConfigMethod;

                /* Increment the count. */
                pShowIp6IntfAddr->u4NoOfAddr++;
                pShowIp6IntfAddrAttr++;
                u4NoOfAddr--;
            }
        }
    }
    else if (pCookie->u1AddrType == ADDR6_ANYCAST)
    {
        if (TMO_SLL_Count (&pIf6->addr6Ilist) > 0)
        {
            TMO_SLL_Scan (&pIf6->addr6Ilist, pSllInfo, tTMO_SLL_NODE *)
            {
                pAddr6Info = IP6_ADDR_PTR_FROM_SLL (pSllInfo);
                if (pAddr6Info->u1AddrType != ADDR6_ANYCAST)
                {
                    continue;
                }
                if ((IS_ADDR_UNSPECIFIED (pCookie->Ip6Addr)) &&
                    (pCookie->u1PrefixLen == 0))
                {
                    i4IsAddrMatch = OSIX_TRUE;
                }

                if ((i4IsAddrMatch == OSIX_FALSE) &&
                    (Ip6AddrMatch (&pCookie->Ip6Addr, &pAddr6Info->ip6Addr,
                                   IP6_ADDR_SIZE_IN_BITS) == TRUE) &&
                    (pAddr6Info->u1AddrType == ADDR6_ANYCAST))
                {
                    /* Match Address Found. Return Next Address. */
                    i4IsAddrMatch = OSIX_TRUE;
                    continue;
                }

                if (i4IsAddrMatch == OSIX_FALSE)
                {
                    continue;
                }

                /* Need to return this Address. */
                if (u4NoOfAddr == 0)
                {
                    /* Update the Cookie and return. */
                    Ip6AddrCopy (&pCookie->Ip6Addr, &pAddr6Info->ip6Addr);
                    pCookie->u1PrefixLen = pAddr6Info->u1PrefLen;
                    return IP6_SUCCESS;
                }

                Ip6AddrCopy (&pShowIp6IntfAddrAttr->Ip6Addr,
                             &pAddr6Info->ip6Addr);
                pShowIp6IntfAddrAttr->u1PrefixLen = pAddr6Info->u1PrefLen;
                pShowIp6IntfAddrAttr->u1Status = pAddr6Info->u1Status;

                /* Increment the count. */
                pShowIp6IntfAddr->u4NoOfAddr++;
                pShowIp6IntfAddrAttr++;
                u4NoOfAddr--;
            }
        }
    }
    else if (pCookie->u1AddrType == ADDR6_MULTI)
    {
        if (TMO_SLL_Count (&pIf6->mcastIlist) > 0)
        {
            TMO_SLL_Scan (&pIf6->mcastIlist, pSllInfo, tTMO_SLL_NODE *)
            {
                pMcastInfo = (tIp6McastInfo *) (pSllInfo);
                if (IS_ADDR_UNSPECIFIED (pCookie->Ip6Addr))
                {
                    i4IsAddrMatch = OSIX_TRUE;
                }

                if ((i4IsAddrMatch == OSIX_FALSE) &&
                    (Ip6AddrMatch (&pCookie->Ip6Addr, &pMcastInfo->ip6Addr,
                                   IP6_ADDR_SIZE_IN_BITS) == TRUE))
                {
                    /* Match Address Found. Return Next Address. */
                    i4IsAddrMatch = OSIX_TRUE;
                    continue;
                }

                if (i4IsAddrMatch == OSIX_FALSE)
                {
                    continue;
                }

                /* Need to return this Address. */
                if (u4NoOfAddr == 0)
                {
                    /* Update the Cookie and return. */
                    Ip6AddrCopy (&pCookie->Ip6Addr, &pMcastInfo->ip6Addr);
                    return IP6_SUCCESS;
                }

                Ip6AddrCopy (&pShowIp6IntfAddrAttr->Ip6Addr,
                             &pMcastInfo->ip6Addr);
                pShowIp6IntfAddrAttr->u1Scope = pMcastInfo->u1AddrScope;

                /* Increment the count. */
                pShowIp6IntfAddr->u4NoOfAddr++;
                pShowIp6IntfAddrAttr++;
                u4NoOfAddr--;
            }
        }
    }
    else
    {
        /* Unsupported Address Type. */
        return IP6_FAILURE;
    }
#else /* LNXIP */
    pIf6Entry = Lip6UtlGetIfEntry (u4IfIndex);
    if (pIf6Entry == NULL)
    {
        return IP6_FAILURE;
    }

    if (pCookie->u1AddrType == ADDR6_LLOCAL)
    {
        TMO_SLL_Scan (&(pIf6Entry->Ip6LLAddrList), pAddr6Info, tLip6AddrNode *)
        {
            if ((IS_ADDR_UNSPECIFIED (pCookie->Ip6Addr)) &&
                (pCookie->u1PrefixLen == 0))
            {
                i4IsAddrMatch = OSIX_TRUE;
            }

            if ((i4IsAddrMatch == OSIX_FALSE) &&
                (Ip6AddrMatch (&pCookie->Ip6Addr, &pAddr6Info->Ip6Addr,
                               IP6_ADDR_SIZE_IN_BITS) == TRUE))
            {
                /* Match Address Found. Return Next Address. */
                i4IsAddrMatch = OSIX_TRUE;
                continue;
            }

            if (i4IsAddrMatch == OSIX_FALSE)
            {
                continue;
            }

            /* Need to return this Address. */
            if (u4NoOfAddr == 0)
            {
                /* Update the Cookie and return. */
                Ip6AddrCopy (&pCookie->Ip6Addr, &pAddr6Info->Ip6Addr);
                pCookie->u1PrefixLen = (UINT1) pAddr6Info->i4PrefixLen;
                return IP6_SUCCESS;
            }

            Ip6AddrCopy (&pShowIp6IntfAddrAttr->Ip6Addr, &pAddr6Info->Ip6Addr);
            pShowIp6IntfAddrAttr->u1PrefixLen = pAddr6Info->i4PrefixLen;
            if ((pAddr6Info->u1Status & IFA_F_TENTATIVE) == IFA_F_TENTATIVE)
            {
                pShowIp6IntfAddrAttr->u1Status = ADDR6_FAILED;
            }
            else
            {
                pShowIp6IntfAddrAttr->u1Status = ADDR6_PREFERRED;
            }

            /* Increment the count. */
            pShowIp6IntfAddr->u4NoOfAddr++;
            pShowIp6IntfAddrAttr++;
            u4NoOfAddr--;
        }
    }
    else if (pCookie->u1AddrType == ADDR6_UNICAST)
    {
        TMO_SLL_Scan (&(pIf6Entry->Ip6AddrList), pAddr6Info, tLip6AddrNode *)
        {
            if ((IS_ADDR_UNSPECIFIED (pCookie->Ip6Addr)) &&
                (pCookie->u1PrefixLen == 0))
            {
                i4IsAddrMatch = OSIX_TRUE;
            }

            if ((i4IsAddrMatch == OSIX_FALSE) &&
                (Ip6AddrMatch (&pCookie->Ip6Addr, &pAddr6Info->Ip6Addr,
                               IP6_ADDR_SIZE_IN_BITS) == TRUE))
            {
                /* Match Address Found. Return Next Address. */
                i4IsAddrMatch = OSIX_TRUE;
                continue;
            }

            if (i4IsAddrMatch == OSIX_FALSE)
            {
                continue;
            }

            /* Need to return this Address. */
            if (u4NoOfAddr == 0)
            {
                /* Update the Cookie and return. */
                Ip6AddrCopy (&pCookie->Ip6Addr, &pAddr6Info->Ip6Addr);
                pCookie->u1PrefixLen = (UINT1) pAddr6Info->i4PrefixLen;
                return IP6_SUCCESS;
            }

            Ip6AddrCopy (&pShowIp6IntfAddrAttr->Ip6Addr, &pAddr6Info->Ip6Addr);
            pShowIp6IntfAddrAttr->u1PrefixLen = pAddr6Info->i4PrefixLen;
            if ((pAddr6Info->u1Status & IFA_F_TENTATIVE) == IFA_F_TENTATIVE)
            {
                pShowIp6IntfAddrAttr->u1Status = ADDR6_FAILED;
            }
            else
            {
                pShowIp6IntfAddrAttr->u1Status = ADDR6_PREFERRED;
            }

            /* Increment the count. */
            pShowIp6IntfAddr->u4NoOfAddr++;
            pShowIp6IntfAddrAttr++;
            u4NoOfAddr--;
        }
    }
    else if (pCookie->u1AddrType == ADDR6_MULTI)
    {
        if (TMO_SLL_Count (&(pIf6Entry->mcastIlist)) > 0)
        {
            TMO_SLL_Scan (&(pIf6Entry->mcastIlist), pAddr6Info, tLip6AddrNode *)
            {
                pMcastInfo = (tIp6McastInfo *) (pAddr6Info);
                if (IS_ADDR_UNSPECIFIED (pCookie->Ip6Addr))
                {
                    i4IsAddrMatch = OSIX_TRUE;
                }

                if ((i4IsAddrMatch == OSIX_FALSE) &&
                    (Ip6AddrMatch (&pCookie->Ip6Addr, &pMcastInfo->ip6Addr,
                                   IP6_ADDR_SIZE_IN_BITS) == TRUE))
                {
                    /* Match Address Found. Return Next Address. */
                    i4IsAddrMatch = OSIX_TRUE;
                    continue;
                }

                if (i4IsAddrMatch == OSIX_FALSE)
                {
                    continue;
                }

                /* Need to return this Address. */
                if (u4NoOfAddr == 0)
                {
                    /* Update the Cookie and return. */
                    Ip6AddrCopy (&pCookie->Ip6Addr, &pMcastInfo->ip6Addr);
                    return IP6_SUCCESS;
                }

                Ip6AddrCopy (&pShowIp6IntfAddrAttr->Ip6Addr,
                             &pMcastInfo->ip6Addr);
                pShowIp6IntfAddrAttr->u1Scope = pMcastInfo->u1AddrScope;

                /* Increment the count. */
                pShowIp6IntfAddr->u4NoOfAddr++;
                pShowIp6IntfAddrAttr++;
                u4NoOfAddr--;
            }
        }
    }
#endif /* IP_WANTED */

    /* Reset the Cookie. */
    MEMSET (&pCookie->Ip6Addr, 0, sizeof (tIp6Addr));
    pCookie->u1PrefixLen = 0;
    return IP6_SUCCESS;
}

/*********************************************************************
*  Function Name : Ip6IfShowInCxt
*  Description   : Display interface configurations and Status 
*  Input(s)      : CliHandle
*                  u4IfIndex - Interface Index
*                  u4VcId  - VRF Id
*  Output(s)     :  
*  Return Values : None.
*********************************************************************/

VOID
Ip6IfShowInCxt (tCliHandle CliHandle, UINT4 u4IfIndex, UINT4 u4VcId)
{
    UINT4               u4L3ContextId = 0;
    UINT4               u4NextL3ContextId = 0;
    UINT4               au4Ip6Ifaces[IP6_MAX_LOGICAL_IFACES + 1];
    INT4                i4RetVal = VCM_FAILURE;
    INT4                i4IpForwarding = IP6_FORW_DISABLE;
    INT4                i4DefHopLimit = 0;
    INT4                i4Rfc5095Flag = 0;
    UINT1               au1ContextName[VCMALIAS_MAX_ARRAY_LEN];
    UINT2               u2Cntr = IP6_ZERO;
    UINT1               u1DisplayHdr = TRUE;

    MEMSET (au1ContextName, 0, VCMALIAS_MAX_ARRAY_LEN);

    if (u4IfIndex == 0)
    {
        if (u4VcId == VCM_INVALID_VC)
        {
            i4RetVal = VcmGetFirstActiveL3Context (&u4L3ContextId);
        }
        else
        {
            u4L3ContextId = u4VcId;
            i4RetVal = VCM_SUCCESS;
        }
        IP6_TASK_UNLOCK ();
        nmhGetFsMIStdIpv6IpForwarding ((INT4) u4L3ContextId, &i4IpForwarding);
        if (i4IpForwarding == IP6_FORW_DISABLE)
        {
            CliPrintf (CliHandle, "Forwarding operationally Disabled");
            CliPrintf (CliHandle, "\r\n");
        }
        else
        {
            CliPrintf (CliHandle, "Forwarding operationally Enabled");
            CliPrintf (CliHandle, "\r\n");
        }

        /* Default Hop Limit */
        nmhGetFsMIStdIpv6IpDefaultHopLimit ((INT4) u4L3ContextId,
                                            &i4DefHopLimit);

        if (u4L3ContextId == VCM_DEFAULT_CONTEXT)
        {
            CliPrintf (CliHandle, "Default-hop limit value is %d \r\n",
                       i4DefHopLimit);
        }
        else
        {
            CliPrintf (CliHandle,
                       "Default-hop limit value for vrf %s is %d \r\n",
                       au1ContextName, i4DefHopLimit);
        }

        IP6_TASK_LOCK ();
        nmhGetFsMIIpv6RFC5095Compatibility ((INT4) u4L3ContextId,
                                            &i4Rfc5095Flag);

        if (i4Rfc5095Flag == IP6_RFC5095_COMPATIBLE)
        {
            CliPrintf (CliHandle, "RFC5095 is compatible \r\n");
        }
        else
        {
            CliPrintf (CliHandle, "RFC5095 is not compatible \r\n");
        }

        while (i4RetVal == VCM_SUCCESS)
        {
            if (u4VcId == VCM_INVALID_VC)
            {
                i4RetVal = VcmGetNextActiveL3Context (u4L3ContextId,
                                                      &u4NextL3ContextId);
                u1DisplayHdr = TRUE;
            }
            else
            {
                i4RetVal = VCM_FAILURE;
            }

            MEMSET (au4Ip6Ifaces, IP6_ZERO,
                    (IP6_MAX_LOGICAL_IFACES + 1) * sizeof (UINT4));
            if (VcmGetCxtIpIfaceList (u4L3ContextId, au4Ip6Ifaces)
                == VCM_FAILURE)
            {
                u4L3ContextId = u4NextL3ContextId;
                continue;
            }
            VcmGetAliasName (u4L3ContextId, au1ContextName);

            for (u2Cntr = 1; ((u2Cntr <= au4Ip6Ifaces[0]) &&
                              (u2Cntr < IP6_MAX_LOGICAL_IFACES + 1)); u2Cntr++)
            {
#ifndef LNXIP6_WANTED
                /* Check for Valid Interface . */
                if (Ip6ifEntryExists (au4Ip6Ifaces[u2Cntr]) != IP6_FAILURE)
#else
                if (Lip6UtlIfEntryExists (au4Ip6Ifaces[u2Cntr]) != OSIX_FAILURE)
#endif
                {
                    if (u1DisplayHdr == TRUE)
                    {
                        CliPrintf (CliHandle, "\r\nVRF Id  : %d\r\n",
                                   u4L3ContextId);
                        CliPrintf (CliHandle, "VRF Name: %s\r\n",
                                   au1ContextName);
                        u1DisplayHdr = FALSE;
                    }

                    Ip6IfShowForIndex (CliHandle, au4Ip6Ifaces[u2Cntr]);
                }
            }
            u4L3ContextId = u4NextL3ContextId;
        }
    }
    else
    {
        /* Need to Display the detailed information for the given interface */
#ifndef LNXIP6_WANTED
        if (Ip6ifEntryExists (u4IfIndex) == IP6_FAILURE)
#else
        if (Lip6UtlIfEntryExists (u4IfIndex) == OSIX_FAILURE)
#endif
        {
            CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
            return;
        }

        VcmGetContextIdFromCfaIfIndex (u4IfIndex, &u4L3ContextId);
        if ((u4VcId != VCM_INVALID_VC) && (u4L3ContextId != u4VcId))
        {
            /*GIven interface does not belong to given VRF */
            return;
        }
        IP6_TASK_UNLOCK ();
        nmhGetFsMIStdIpv6IpForwarding ((INT4) u4L3ContextId, &i4IpForwarding);

        if (i4IpForwarding == IP6_FORW_DISABLE)
        {
            CliPrintf (CliHandle, "Forwarding operationally Disabled");
            CliPrintf (CliHandle, "\r\n");
        }
        else
        {
            CliPrintf (CliHandle, "Forwarding operationally Enabled");
            CliPrintf (CliHandle, "\r\n");
        }

        /* Default Hop Limit */
        nmhGetFsMIStdIpv6IpDefaultHopLimit ((INT4) u4L3ContextId,
                                            &i4DefHopLimit);

        if (u4L3ContextId == VCM_DEFAULT_CONTEXT)
        {
            CliPrintf (CliHandle, "Default-hop limit value is %d \r\n",
                       i4DefHopLimit);
        }
        else
        {
            CliPrintf (CliHandle,
                       "Default-hop limit value for vrf %s is %d \r\n",
                       au1ContextName, i4DefHopLimit);
        }

        IP6_TASK_LOCK ();
        nmhGetFsMIIpv6RFC5095Compatibility ((INT4) u4L3ContextId,
                                            &i4Rfc5095Flag);

        if (i4Rfc5095Flag == IP6_RFC5095_COMPATIBLE)
        {
            CliPrintf (CliHandle, "RFC5095 is compatible \r\n");
        }
        else
        {
            CliPrintf (CliHandle, "RFC5095 is not compatible \r\n");
        }

        VcmGetAliasName (u4L3ContextId, au1ContextName);
        CliPrintf (CliHandle, "VRF Id  :  %d\r\n", u4L3ContextId);
        CliPrintf (CliHandle, "VRF Name:  %s\r\n", au1ContextName);

        Ip6IfShowForIndex (CliHandle, u4IfIndex);
    }
    return;
}

/*********************************************************************
*  Function Name : Ip6RouteAdd
*  Description   : Adding a Static Route Entry to the Route Table 
*  Input(s)      : CliHandle
*                  u4RouteIndex - Interface Index over which route
*                                 is to be configured
*                  RouteDest - Destination IPv6 prefix
*                  NextHop - NextHop Prefix
*                  i4PrefixLen - Destination Prefix Length 
*                  u4Metric - Administrative distance value
*  Output(s)     :  
*  Return Values : None.
*********************************************************************/

VOID
Ip6RouteAddInCxt (tCliHandle CliHandle,
                  UINT4 u4RouteIndex,
                  UINT4 u4ContextId,
                  tIp6Addr RouteDest,
                  tIp6Addr NextHop, INT4 i4PrefixLen, INT4 i4RoutePreference,
                  INT4 i4AddrType)
{
    tSNMP_OCTET_STRING_TYPE RtDest;
    tSNMP_OCTET_STRING_TYPE RtNxtHp;
    tIp6Addr            NxtHp;
    INT4                i4Type = 0;
    INT4                i4Index = 0;
    UINT4               u4ErrorCode = 0;
    INT4                i4RtIfIndex = 0;
    INT4                i4RtRowStatus = 0;
    INT4                i4RtAddrType = 0;
    INT4                i4RtRoutePreference = 0;
    INT1                i1RtPresent = FALSE;
    UINT4               u4RtMetric = 0;
    UINT1               au1RtDestOctetList[IP6_ADDR_SIZE];
    UINT1               au1RtNxtHpOctetList[IP6_ADDR_SIZE];
    tNetIpv6RtInfo      NetIp6RtInfo;
#ifndef LNXIP6_WANTED            /* FSIP */
    UINT4               u4IfContextId;
    tNd6CacheEntry     *pNd6c = NULL;
    tIp6If             *pTempRouteIndex = NULL;
#else
    UINT1               u1NxtHpReachable = IP6_ONE;
    tNetIpv6RtInfoQueryMsg RtQuery;
#endif

    MEMSET (au1RtDestOctetList, IP6_ZERO, IP6_ADDR_SIZE);
    MEMSET (au1RtNxtHpOctetList, IP6_ZERO, IP6_ADDR_SIZE);

    RtDest.pu1_OctetList = au1RtDestOctetList;

    MEMCPY (RtDest.pu1_OctetList, &RouteDest, IP6_ADDR_SIZE);
    RtDest.i4_Length = IP6_ADDR_SIZE;

    RtNxtHp.pu1_OctetList = au1RtNxtHpOctetList;

    MEMCPY (RtNxtHp.pu1_OctetList, &NextHop, IP6_ADDR_SIZE);
    RtNxtHp.i4_Length = IP6_ADDR_SIZE;

    i4PrefixLen = i4PrefixLen;

#ifndef LNXIP6_WANTED            /* FSIP */
    if (!IS_ADDR_UNSPECIFIED (NextHop) && !IS_ADDR_LLOCAL (NextHop))
    {
        RTM6_TASK_UNLOCK ();
        IP6_TASK_LOCK ();
        pTempRouteIndex =
            Ip6GetRouteInCxt (u4ContextId, &NextHop, &pNd6c, &NetIp6RtInfo);
        IP6_TASK_UNLOCK ();
        RTM6_TASK_LOCK ();
        if (pTempRouteIndex != NULL)
        {
            if (u4RouteIndex == 0)
            {
                i4Index = (INT4) pTempRouteIndex->u4Index;
            }
            else
            {
                i4Index = (INT4) u4RouteIndex;
            }
        }
    }
    else
    {
        i4Index = (INT4) u4RouteIndex;
    }
    if (i4Index != 0)
    {
        if (VcmGetContextIdFromCfaIfIndex (i4Index, &u4IfContextId) !=
            VCM_SUCCESS)
        {
            CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
            return;

        }
        else
        {
            if (u4IfContextId != u4ContextId)
            {
                CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
                return;
            }
        }
    }
#else /*LNXIP6_WANTED */
    if (!IS_ADDR_UNSPECIFIED (NextHop) && !IS_ADDR_LLOCAL (NextHop))
    {

        MEMSET (&NetIp6RtInfo, 0, sizeof (tNetIpv6RtInfo));
        MEMSET (&RtQuery, 0, sizeof (tNetIpv6RtInfoQueryMsg));

        RtQuery.u4ContextId = u4ContextId;
        RtQuery.u1QueryFlag = RTM6_QUERIED_FOR_NEXT_HOP;
        Ip6AddrCopy (&NetIp6RtInfo.Ip6Dst, &NextHop);
        NetIp6RtInfo.u1Prefixlen = IP6_ADDR_MAX_PREFIX;
        RTM6_TASK_UNLOCK ();
        if (NetIpv6GetRoute (&RtQuery, &NetIp6RtInfo) == NETIPV6_FAILURE)
        {
            /* Next Hop is not reachable */
            u1NxtHpReachable = IP6_ZERO;
        }
        RTM6_TASK_LOCK ();

        if (u4RouteIndex == 0)
        {
            i4Index = (INT4) NetIp6RtInfo.u4Index;
        }
        else
        {
            i4Index = (INT4) u4RouteIndex;
        }
    }
    else
    {
        i4Index = (INT4) u4RouteIndex;
    }
#endif

    if (nmhGetFsipv6RouteAdminStatus (&RtDest, i4PrefixLen, IP6_NETMGMT_PROTOID,
                                      &RtNxtHp, &i4RtRowStatus) != SNMP_FAILURE)
    {
        i1RtPresent = TRUE;
    }
    nmhGetFsipv6RouteIfIndex (&RtDest, i4PrefixLen, IP6_NETMGMT_PROTOID,
                              &RtNxtHp, &i4RtIfIndex);
    nmhGetFsipv6RouteMetric (&RtDest, i4PrefixLen, IP6_NETMGMT_PROTOID,
                             &RtNxtHp, &u4RtMetric);
    nmhGetFsipv6RouteAddrType (&RtDest, i4PrefixLen, IP6_NETMGMT_PROTOID,
                               &RtNxtHp, &i4RtAddrType);
    nmhGetFsipv6RoutePreference (&RtDest, i4PrefixLen, IP6_NETMGMT_PROTOID,
                                 &RtNxtHp, &i4RtRoutePreference);
    RTM6_TASK_UNLOCK ();
    if (i1RtPresent == FALSE)
    {
        /* Route entry does not exists. Create a new entry. */
        if (nmhTestv2Fsipv6RouteAdminStatus
            (&u4ErrorCode, &RtDest, i4PrefixLen, IP6_NETMGMT_PROTOID, &RtNxtHp,
             IP6FWD_CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            RTM6_TASK_LOCK ();
            return;
        }

        if (nmhSetFsipv6RouteAdminStatus
            (&RtDest, i4PrefixLen, IP6_NETMGMT_PROTOID, &RtNxtHp,
             IP6FWD_CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            RTM6_TASK_LOCK ();
            return;
        }
    }

    if (i4RtAddrType != i4AddrType)
    {
        if (nmhTestv2Fsipv6RouteAddrType
            (&u4ErrorCode, &RtDest, i4PrefixLen, IP6_NETMGMT_PROTOID, &RtNxtHp,
             i4AddrType) == SNMP_FAILURE)
        {
            RTM6_TASK_LOCK ();
            return;
        }
        if (nmhSetFsipv6RouteAddrType
            (&RtDest, i4PrefixLen, IP6_NETMGMT_PROTOID, &RtNxtHp,
             i4AddrType) == SNMP_FAILURE)
        {
            RTM6_TASK_LOCK ();
            CLI_FATAL_ERROR (CliHandle);
            return;
        }
    }

    if ((i4RtIfIndex != i4Index) && (i4Index != 0))
    {
        /* Either new route is getting added or Route's interface have changed. */
        if (nmhTestv2Fsipv6RouteIfIndex
            (&u4ErrorCode, &RtDest, i4PrefixLen, IP6_NETMGMT_PROTOID, &RtNxtHp,
             i4Index) == SNMP_FAILURE)
        {
            nmhSetFsipv6RouteAdminStatus
                (&RtDest, i4PrefixLen, IP6_NETMGMT_PROTOID, &RtNxtHp,
                 IP6FWD_DESTROY);
            RTM6_TASK_LOCK ();
            return;
        }

        if (nmhSetFsipv6RouteIfIndex
            (&RtDest, i4PrefixLen, IP6_NETMGMT_PROTOID, &RtNxtHp, i4Index) ==
            SNMP_FAILURE)
        {
            nmhSetFsipv6RouteAdminStatus
                (&RtDest, i4PrefixLen, IP6_NETMGMT_PROTOID, &RtNxtHp,
                 IP6FWD_DESTROY);
            RTM6_TASK_LOCK ();
            CLI_FATAL_ERROR (CliHandle);
            return;
        }
    }

    if (i4RtRoutePreference != i4RoutePreference)
    {
        /* Either new route is getting added or Route's preference have changed. */
        if (nmhTestv2Fsipv6RoutePreference
            (&u4ErrorCode, &RtDest, i4PrefixLen, IP6_NETMGMT_PROTOID, &RtNxtHp,
             i4RoutePreference) == SNMP_FAILURE)
        {
            nmhSetFsipv6RouteAdminStatus
                (&RtDest, i4PrefixLen, IP6_NETMGMT_PROTOID, &RtNxtHp,
                 IP6FWD_DESTROY);
            RTM6_TASK_LOCK ();
            return;
        }

        if (nmhSetFsipv6RoutePreference
            (&RtDest, i4PrefixLen, IP6_NETMGMT_PROTOID, &RtNxtHp,
             i4RoutePreference) == SNMP_FAILURE)
        {
            nmhSetFsipv6RouteAdminStatus
                (&RtDest, i4PrefixLen, IP6_NETMGMT_PROTOID, &RtNxtHp,
                 IP6FWD_DESTROY);
            RTM6_TASK_LOCK ();
            CLI_FATAL_ERROR (CliHandle);
            return;
        }
    }

    if (i1RtPresent == FALSE)
    {
        /* Either new route is getting added or Route's metric have changed. */
        if (nmhTestv2Fsipv6RouteMetric
            (&u4ErrorCode, &RtDest, i4PrefixLen, IP6_NETMGMT_PROTOID, &RtNxtHp,
             IP6_ONE) == SNMP_FAILURE)
        {
            nmhSetFsipv6RouteAdminStatus
                (&RtDest, i4PrefixLen, IP6_NETMGMT_PROTOID, &RtNxtHp,
                 IP6FWD_DESTROY);
            RTM6_TASK_LOCK ();
            return;
        }

        if (nmhSetFsipv6RouteMetric
            (&RtDest, i4PrefixLen, IP6_NETMGMT_PROTOID, &RtNxtHp, IP6_ONE) ==
            SNMP_FAILURE)
        {
            nmhSetFsipv6RouteAdminStatus
                (&RtDest, i4PrefixLen, IP6_NETMGMT_PROTOID, &RtNxtHp,
                 IP6FWD_DESTROY);
            RTM6_TASK_LOCK ();
            CLI_FATAL_ERROR (CliHandle);
            return;
        }

        MEMCPY (&NxtHp, &NextHop, IP6_ADDR_SIZE);
        if (IS_ADDR_UNSPECIFIED (NxtHp))
        {
            i4Type = IP6_ROUTE_TYPE_DIRECT;
        }
        else
        {
            i4Type = IP6_ROUTE_TYPE_INDIRECT;
        }

        if (nmhSetFsipv6RouteType
            (&RtDest, i4PrefixLen, IP6_NETMGMT_PROTOID, &RtNxtHp, i4Type) ==
            SNMP_FAILURE)
        {
            nmhSetFsipv6RouteAdminStatus
                (&RtDest, i4PrefixLen, IP6_NETMGMT_PROTOID, &RtNxtHp,
                 IP6FWD_DESTROY);
            RTM6_TASK_LOCK ();
            CLI_FATAL_ERROR (CliHandle);
            return;
        }
    }

#ifdef LNXIP6_WANTED
    if (u1NxtHpReachable == IP6_ZERO)
    {
        return;
    }
#endif

    if (i1RtPresent == FALSE)
    {
        if (nmhTestv2Fsipv6RouteAdminStatus
            (&u4ErrorCode, &RtDest, i4PrefixLen, IP6_NETMGMT_PROTOID, &RtNxtHp,
             IP6FWD_ACTIVE) == SNMP_FAILURE)
        {
            nmhSetFsipv6RouteAdminStatus
                (&RtDest, i4PrefixLen, IP6_NETMGMT_PROTOID, &RtNxtHp,
                 IP6FWD_DESTROY);
            RTM6_TASK_LOCK ();
            return;
        }

        if (nmhSetFsipv6RouteAdminStatus
            (&RtDest, i4PrefixLen, IP6_NETMGMT_PROTOID, &RtNxtHp,
             IP6FWD_ACTIVE) == SNMP_FAILURE)
        {
            nmhSetFsipv6RouteAdminStatus
                (&RtDest, i4PrefixLen, IP6_NETMGMT_PROTOID, &RtNxtHp,
                 IP6FWD_DESTROY);
            RTM6_TASK_LOCK ();
            CLI_FATAL_ERROR (CliHandle);
            return;
        }
    }
    RTM6_TASK_LOCK ();
    return;
}

/*********************************************************************
*  Function Name : Ip6RouteDel 
*  Description   : Deletion of Route Entries 
*  Input(s)      : CliHandle
*                  RouteDest - Destination IPv6 Prefix
*                  i4PrefixLen - Prefix length
*                  NextHop - Nexthop Prefix
*  Output(s)     :  
*  Return Values : None.
*********************************************************************/

VOID
Ip6RouteDel (tCliHandle CliHandle,
             tIp6Addr RouteDest, INT4 i4PrefixLen, tIp6Addr NextHop)
{
    tSNMP_OCTET_STRING_TYPE RtDest;
    tSNMP_OCTET_STRING_TYPE RtNxtHp;
    UINT4               u4ErrorCode = 0;
    UINT1               au1RtDestOctetList[IP6_ADDR_SIZE];
    UINT1               au1RtNxtHpOctetList[IP6_ADDR_SIZE];

    MEMSET (au1RtDestOctetList, IP6_ZERO, IP6_ADDR_SIZE);
    MEMSET (au1RtNxtHpOctetList, IP6_ZERO, IP6_ADDR_SIZE);

    RtDest.pu1_OctetList = au1RtDestOctetList;
    MEMCPY (RtDest.pu1_OctetList, &RouteDest, IP6_ADDR_SIZE);
    RtDest.i4_Length = IP6_ADDR_SIZE;

    RtNxtHp.pu1_OctetList = au1RtNxtHpOctetList;
    MEMCPY (RtNxtHp.pu1_OctetList, &NextHop, IP6_ADDR_SIZE);
    RtNxtHp.i4_Length = IP6_ADDR_SIZE;

    if (nmhTestv2Fsipv6RouteAdminStatus
        (&u4ErrorCode, &RtDest, i4PrefixLen, IP6_NETMGMT_PROTOID, &RtNxtHp,
         IP6FWD_DESTROY) == SNMP_FAILURE)
    {
        return;
    }

    if (nmhSetFsipv6RouteAdminStatus
        (&RtDest, i4PrefixLen, IP6_NETMGMT_PROTOID, &RtNxtHp,
         IP6FWD_DESTROY) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return;
    }

    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : Ip6Nd6ShowInCxt                                        */
/*                                                                           */
/* Description      : This function is invoked to display ND table           */
/*                                                                           */
/* Input Parameters : 1. CliHandle - CliContext ID                           */
/*                    2. u4NdCxtId - VRF Id                                  */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/

INT4
Ip6Nd6ShowInCxt (tCliHandle CliHandle, UINT4 u4NdCxtId)
{
    INT4                i4IpNetToMediaIfIndex = 1;
    INT4                i4NextIpNetToMediaIfIndex = 0;
    UINT4               au4Ip6Ifaces[IP6_MAX_LOGICAL_IFACES + 1];
    UINT4               u4NextNdCxtId = 0;
    UINT4               u4CurrNdCxtId = 0;
    INT4                i4RetVal = VCM_FAILURE;
    INT4                i4IpNetToMediaType = 0;
    INT4                i4NetToPhysicalNetAddressType = INET_ADDR_TYPE_IPV6;
    INT4                i4NextIpNetToPhysicalNetAddressType =
        INET_ADDR_TYPE_IPV6;
    CONST CHR1         *au1Status[] = {
        "Static", "Reachable", "Incomplete", "Stale", "Delay", "Probe", "Evpn"
    };
    CONST CHR1         *au1SecStatus[] = {
        "Unsecure", "Secure"
    };
    UINT4               u4TempCxtId = VCM_INVALID_VC;
    CHR1               *pu1Address = NULL;
    INT1               *pi1InterfaceName = NULL;
    INT1                ai1InterfaceName[CFA_CLI_MAX_IF_NAME_LEN];
    UINT1               au1Address[MAX_ADDR_LEN];
    UINT1               au1PhyAddress[MAX_ADDR_LEN];
    UINT1               au1NetToPhysicalIp[IPVX_IPV6_ADDR_LEN];
    UINT1               au1NextNetToPhysicalIp[IPVX_IPV6_ADDR_LEN];
    UINT1               au1ContextName[VCMALIAS_MAX_ARRAY_LEN];
    UINT2               u2Cntr = 0;
    UINT1               u1DisplayHdr = TRUE;
    INT4                i4RetValFsipv6NdLanCacheIsSecure = 0;

    tSNMP_OCTET_STRING_TYPE OctetStrIpNetToMediaPhysAddress;
    tSNMP_OCTET_STRING_TYPE IpNetToPhysicalNetAddr;
    tSNMP_OCTET_STRING_TYPE NextIpNetToPhysicalNetAddress;

    MEMSET (au1NetToPhysicalIp, ZERO, IPVX_IPV6_ADDR_LEN);
    MEMSET (au1NextNetToPhysicalIp, ZERO, IPVX_IPV6_ADDR_LEN);

    IpNetToPhysicalNetAddr.pu1_OctetList = au1NetToPhysicalIp;
    NextIpNetToPhysicalNetAddress.pu1_OctetList = au1NextNetToPhysicalIp;

    IpNetToPhysicalNetAddr.i4_Length = 0;
    NextIpNetToPhysicalNetAddress.i4_Length = 0;

    OctetStrIpNetToMediaPhysAddress.pu1_OctetList = &au1PhyAddress[0];

    pu1Address = (CHR1 *) & au1Address[0];
    pi1InterfaceName = &ai1InterfaceName[0];

    if (u4NdCxtId != VCM_INVALID_VC)
    {
        if (VcmIsL3VcExist (u4NdCxtId) == VCM_FALSE)
        {
            CliPrintf (CliHandle, "\r%% Invalid VRF \r\n");
            return CLI_FAILURE;
        }
        i4RetVal = VCM_SUCCESS;
        u4CurrNdCxtId = u4NdCxtId;
    }
    else
    {
        i4RetVal = VcmGetFirstActiveL3Context (&u4CurrNdCxtId);
    }

    while (i4RetVal == VCM_SUCCESS)
    {
        if (u4NdCxtId == VCM_INVALID_VC)
        {
            i4RetVal = VcmGetNextActiveL3Context (u4CurrNdCxtId,
                                                  &u4NextNdCxtId);
        }
        else
        {
            i4RetVal = VCM_FAILURE;
        }

        MEMSET (au4Ip6Ifaces, IP6_ZERO,
                (IP6_MAX_LOGICAL_IFACES + 1) * sizeof (UINT4));
        if (VcmGetCxtIpIfaceList (u4CurrNdCxtId, au4Ip6Ifaces) == VCM_FAILURE)
        {
            u4CurrNdCxtId = u4NextNdCxtId;
            continue;
        }
        VcmGetAliasName (u4CurrNdCxtId, au1ContextName);
        u1DisplayHdr = TRUE;

        for (u2Cntr = 1; ((u2Cntr <= au4Ip6Ifaces[0]) &&
                          (u2Cntr < IP6_MAX_LOGICAL_IFACES + 1)); u2Cntr++)
        {
            i4IpNetToMediaIfIndex = au4Ip6Ifaces[u2Cntr];
            i4NetToPhysicalNetAddressType = INET_ADDR_TYPE_IPV6;
            MEMSET (au1NetToPhysicalIp, ZERO, IPVX_IPV6_ADDR_LEN);
            IpNetToPhysicalNetAddr.i4_Length = 0;

            while (nmhGetNextIndexFsMIStdIpNetToPhysicalTable
                   (i4IpNetToMediaIfIndex, &i4NextIpNetToMediaIfIndex,
                    i4NetToPhysicalNetAddressType,
                    &i4NextIpNetToPhysicalNetAddressType,
                    &IpNetToPhysicalNetAddr, &NextIpNetToPhysicalNetAddress)
                   == SNMP_SUCCESS)
            {
                if (i4IpNetToMediaIfIndex != i4NextIpNetToMediaIfIndex)
                {
                    break;
                }
                i4IpNetToMediaIfIndex = i4NextIpNetToMediaIfIndex;
                MEMCPY (IpNetToPhysicalNetAddr.pu1_OctetList,
                        NextIpNetToPhysicalNetAddress.pu1_OctetList,
                        NextIpNetToPhysicalNetAddress.i4_Length);
                i4NetToPhysicalNetAddressType
                    = i4NextIpNetToPhysicalNetAddressType;

                if (i4NextIpNetToPhysicalNetAddressType != INET_ADDR_TYPE_IPV6)
                {
                    continue;
                }

                nmhGetFsMIStdIpNetToPhysicalContextId
                    (i4NextIpNetToMediaIfIndex,
                     i4NextIpNetToPhysicalNetAddressType,
                     &NextIpNetToPhysicalNetAddress, (INT4 *) &u4TempCxtId);

                if ((u4NdCxtId != VCM_INVALID_VC) && (u4TempCxtId != u4NdCxtId))
                {
                    /*If ContextId of the current entry does not match with the 
                       specified one, get the next entry */
                    continue;
                }

                if (u1DisplayHdr == TRUE)
                {
                    CliPrintf (CliHandle, "\r\nVRF Id  : %d\r\n",
                               u4CurrNdCxtId);
                    CliPrintf (CliHandle, "VRF Name: %s", au1ContextName);
                    CliPrintf (CliHandle,
                               "\r\n     IPv6 Address                   Link-layer Addr     State     Interface");
                    CliPrintf (CliHandle, "      Secure/Unsecure");
                    CliPrintf (CliHandle,
                               "\r\n     ------------                   ---------------     -----     ---------");
                    CliPrintf (CliHandle, "      -------------");
                    CliPrintf (CliHandle, "\r\n");
                    u1DisplayHdr = FALSE;
                }

                MEMSET (au1Address, 0, MAX_ADDR_LEN);
                MEMSET (au1PhyAddress, 0, MAX_ADDR_LEN);
                MEMSET (ai1InterfaceName, 0, CFA_CLI_MAX_IF_NAME_LEN);
                nmhGetFsMIStdIpNetToPhysicalPhysAddress
                    (i4NextIpNetToMediaIfIndex,
                     i4NextIpNetToPhysicalNetAddressType,
                     &NextIpNetToPhysicalNetAddress,
                     &OctetStrIpNetToMediaPhysAddress);

                nmhGetFsipv6NdLanCacheState (i4NextIpNetToMediaIfIndex,
                                             &NextIpNetToPhysicalNetAddress,
                                             &i4IpNetToMediaType);

                nmhGetFsipv6NdLanCacheIsSecure (i4NextIpNetToMediaIfIndex,
                                                &NextIpNetToPhysicalNetAddress,
                                                &i4RetValFsipv6NdLanCacheIsSecure);

                /*Call CfaCliGetIfName for getting 
                 * Interface name corresponding to IfIndex */

                CfaCliGetIfName ((UINT4) i4NextIpNetToMediaIfIndex,
                                 pi1InterfaceName);

                CliPrintf (CliHandle, "%-35s  ",
                           Ip6PrintNtop ((tIp6Addr *) (VOID *)
                                         NextIpNetToPhysicalNetAddress.
                                         pu1_OctetList));

                /*Call macro for MAC Address to String format, to Print */
                MEMSET (au1Address, 0, MAX_ADDR_LEN);
                CLI_CONVERT_MAC_TO_DOT_STR (OctetStrIpNetToMediaPhysAddress.
                                            pu1_OctetList,
                                            (UINT1 *) pu1Address);

                CliPrintf (CliHandle, "%-17s  ", pu1Address);

                /* Print the Reach state */
                if ((i4IpNetToMediaType >= ND6_CACHE_ENTRY_STATIC)
                    && (i4IpNetToMediaType <= ND6_CACHE_ENTRY_EVPN))
                {
                    CliPrintf (CliHandle, "%-11s",
                               au1Status[i4IpNetToMediaType - 1]);
                }
                else
                {
                    CliPrintf (CliHandle, "%-11s", "UnKnown");
                }
                /* Print the interface name */
                CliPrintf (CliHandle, "%-15s", pi1InterfaceName);
                CliPrintf (CliHandle, "%s\r\n",
                           au1SecStatus[i4RetValFsipv6NdLanCacheIsSecure]);
            }
        }
        u4CurrNdCxtId = u4NextNdCxtId;
    }
    return (CLI_SUCCESS);

}

/*********************************************************************
 *   Function Name : Ip6EnableRouteAdvRDNSS
 *   Description   : Enables RDNSS on an interface
 *   Input(s)      : CliHandle
 *                   u4Index -Interface Index
 *                   i4RouteAdvRDNSS - value to be set
 *   Output(s)     :
 *   Return Values : None.
 ***********************************************************************/
VOID
Ip6EnableRouteAdvRDNSS (tCliHandle CliHandle, UINT4 u4Index,
                        tIp6Addr Ip6AddrOne, UINT4 u4LifeTimeOne,
                        INT4 i4RouteAdvRDNSS)
{
    UINT4               u4ErrorCode = 0;
    tSNMP_OCTET_STRING_TYPE RDNSSAddr;
    UINT1               au1RDNSSAddrOctetList[IP6_ADDR_SIZE];
    INT4                i4Index = (INT4) u4Index;
    tIp6Addr            Ip6Addr;
    tIp6Addr            Ip6TempAddr;
    MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));

    IP6_TASK_UNLOCK ();

    if (Ip6IsIfFwdEnabled (i4Index) == FALSE)
    {
        if (Ip6ifEntryExists (u4Index) == IP6_FAILURE)
        {
            IP6_TASK_LOCK ();
            CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
            return;
        }
        if (nmhTestv2Fsipv6IfRaRDNSSRowStatus
            (&u4ErrorCode, i4Index, CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            IP6_TASK_LOCK ();
            CLI_FATAL_ERROR (CliHandle);
            return;
        }
        if (nmhSetFsipv6IfRaRDNSSRowStatus (i4Index, CREATE_AND_WAIT) ==
            SNMP_FAILURE)
        {
            IP6_TASK_LOCK ();
            CLI_FATAL_ERROR (CliHandle);
            return;
        }
    }
    if (i4RouteAdvRDNSS == IP6_RA_RDNSS)
    {
        MEMSET (&Ip6TempAddr, 0, sizeof (tIp6Addr));
        RDNSSAddr.pu1_OctetList = au1RDNSSAddrOctetList;
        nmhGetFsipv6IfRaRDNSSAddrOne (i4Index, &RDNSSAddr);
        Ip6AddrCopy (&Ip6TempAddr,
                     (tIp6Addr *) (VOID *) RDNSSAddr.pu1_OctetList);

        /* The given ipv6 address is allocated only if valid lifetime is given and does'nt reach the maximum RDNSS servers
           limit */

        if ((Ip6AddrCompare (Ip6TempAddr, Ip6Addr) == IP6_ZERO)
            || (Ip6AddrCompare (Ip6TempAddr, Ip6AddrOne) == IP6_ZERO))
        {
            RDNSSAddr.pu1_OctetList = au1RDNSSAddrOctetList;
            Ip6AddrCopy ((tIp6Addr *) (VOID *) RDNSSAddr.pu1_OctetList,
                         &Ip6AddrOne);
            RDNSSAddr.i4_Length = IP6_ADDR_SIZE;
            if (nmhTestv2Fsipv6IfRaRDNSSAddrOneLife
                (&u4ErrorCode, i4Index, u4LifeTimeOne) == SNMP_FAILURE)
            {
                IP6_TASK_LOCK ();
                return;
            }

            if (nmhSetFsipv6IfRaRDNSSAddrOneLife (i4Index, u4LifeTimeOne) ==
                SNMP_FAILURE)
            {
                IP6_TASK_LOCK ();
                CLI_FATAL_ERROR (CliHandle);
                return;
            }
            if (nmhTestv2Fsipv6IfRaRDNSSAddrOne
                (&u4ErrorCode, i4Index, &RDNSSAddr) == SNMP_FAILURE)
            {
                IP6_TASK_LOCK ();
                CLI_FATAL_ERROR (CliHandle);
                return;

            }
            if (nmhSetFsipv6IfRaRDNSSAddrOne (i4Index, &RDNSSAddr) ==
                SNMP_FAILURE)
            {
                IP6_TASK_LOCK ();
                CLI_FATAL_ERROR (CliHandle);
                return;
            }
            if (i4RouteAdvRDNSS == IP6_RA_RDNSS)
            {
                if ((nmhSetFsipv6IfRaRDNSSRowStatus (i4Index, ACTIVE)) ==
                    SNMP_FAILURE)
                {
                    IP6_TASK_LOCK ();
                    CLI_FATAL_ERROR (CliHandle);
                    return;
                }
            }
            IP6_TASK_LOCK ();
            return;
        }
        MEMSET (&Ip6TempAddr, 0, sizeof (tIp6Addr));
        RDNSSAddr.pu1_OctetList = au1RDNSSAddrOctetList;
        nmhGetFsipv6IfRaRDNSSAddrTwo (i4Index, &RDNSSAddr);
        Ip6AddrCopy (&Ip6TempAddr,
                     (tIp6Addr *) (VOID *) RDNSSAddr.pu1_OctetList);

        if ((Ip6AddrCompare (Ip6TempAddr, Ip6Addr) == IP6_ZERO)
            || (Ip6AddrCompare (Ip6TempAddr, Ip6AddrOne) == IP6_ZERO))
        {
            RDNSSAddr.pu1_OctetList = au1RDNSSAddrOctetList;
            Ip6AddrCopy ((tIp6Addr *) (VOID *) RDNSSAddr.pu1_OctetList,
                         &Ip6AddrOne);
            RDNSSAddr.i4_Length = IP6_ADDR_SIZE;
            if (nmhTestv2Fsipv6IfRaRDNSSAddrTwoLife
                (&u4ErrorCode, i4Index, u4LifeTimeOne) == SNMP_FAILURE)
            {
                IP6_TASK_LOCK ();
                return;
            }

            if (nmhSetFsipv6IfRaRDNSSAddrTwoLife (i4Index, u4LifeTimeOne) ==
                SNMP_FAILURE)
            {
                IP6_TASK_LOCK ();
                CLI_FATAL_ERROR (CliHandle);
                return;
            }
            if (nmhTestv2Fsipv6IfRaRDNSSAddrTwo
                (&u4ErrorCode, i4Index, &RDNSSAddr) == SNMP_FAILURE)
            {
                IP6_TASK_LOCK ();
                CLI_FATAL_ERROR (CliHandle);
                return;
            }
            if (nmhSetFsipv6IfRaRDNSSAddrTwo (i4Index, &RDNSSAddr) ==
                SNMP_FAILURE)
            {
                IP6_TASK_LOCK ();
                CLI_FATAL_ERROR (CliHandle);
                return;
            }
            if (i4RouteAdvRDNSS == IP6_RA_RDNSS)
            {
                if ((nmhSetFsipv6IfRaRDNSSRowStatus (i4Index, ACTIVE)) ==
                    SNMP_FAILURE)
                {
                    IP6_TASK_LOCK ();
                    CLI_FATAL_ERROR (CliHandle);
                    return;
                }
            }
            IP6_TASK_LOCK ();
            return;
        }

        MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));
        RDNSSAddr.pu1_OctetList = au1RDNSSAddrOctetList;
        nmhGetFsipv6IfRaRDNSSAddrThree (i4Index, &RDNSSAddr);
        Ip6AddrCopy (&Ip6TempAddr,
                     (tIp6Addr *) (VOID *) RDNSSAddr.pu1_OctetList);

        if ((Ip6AddrCompare (Ip6TempAddr, Ip6Addr) == IP6_ZERO)
            || (Ip6AddrCompare (Ip6TempAddr, Ip6AddrOne) == IP6_ZERO))
        {
            RDNSSAddr.pu1_OctetList = au1RDNSSAddrOctetList;
            Ip6AddrCopy ((tIp6Addr *) (VOID *) RDNSSAddr.pu1_OctetList,
                         &Ip6AddrOne);
            RDNSSAddr.i4_Length = IP6_ADDR_SIZE;
            if (nmhTestv2Fsipv6IfRaRDNSSAddrThreeLife
                (&u4ErrorCode, i4Index, u4LifeTimeOne) == SNMP_FAILURE)
            {
                IP6_TASK_LOCK ();
                return;
            }

            if (nmhSetFsipv6IfRaRDNSSAddrThreeLife (i4Index, u4LifeTimeOne) ==
                SNMP_FAILURE)
            {
                IP6_TASK_LOCK ();
                CLI_FATAL_ERROR (CliHandle);
                return;
            }
            if (nmhTestv2Fsipv6IfRaRDNSSAddrThree
                (&u4ErrorCode, i4Index, &RDNSSAddr) == SNMP_FAILURE)
            {
                IP6_TASK_LOCK ();
                CLI_FATAL_ERROR (CliHandle);
                return;
            }
            if (nmhSetFsipv6IfRaRDNSSAddrThree (i4Index, &RDNSSAddr) ==
                SNMP_FAILURE)
            {
                IP6_TASK_LOCK ();
                CLI_FATAL_ERROR (CliHandle);
                return;
            }
            if (i4RouteAdvRDNSS == IP6_RA_RDNSS)
            {
                if ((nmhSetFsipv6IfRaRDNSSRowStatus (i4Index, ACTIVE)) ==
                    SNMP_FAILURE)
                {
                    IP6_TASK_LOCK ();
                    CLI_FATAL_ERROR (CliHandle);
                    return;
                }
            }
            IP6_TASK_LOCK ();
            return;
        }
    }
    /* On reaching the maximum RDNSS servers  limit ie.3 , error is thrown */
    CLI_SET_ERR (CLI_IP6_RT_ADV_MAX_RDNSS);
    return;
}

/*********************************************************************
 *   Function Name : Ip6DisableRouteAdvRDNSS
 *   Description   : Disables RDNSS on an interface
 *   Input(s)      : CliHandle
 *                   u4Index -Interface Index
 *                   i4DisRouteAdvRDNSS - value to be set
 *   Output(s)     :
 *   Return Values : None.
 ***********************************************************************/
VOID
Ip6DisableRouteAdvRDNSS (tCliHandle CliHandle, UINT4 u4Index,
                         tIp6Addr Ip6AddrOne, tIp6Addr Ip6AddrTwo,
                         tIp6Addr Ip6AddrThree, INT4 i4DisRouteAdvRDNSS)
{
    tIp6Addr            Ip6Tempaddr;
    INT4                i4Index = (INT4) u4Index;
    INT4                i4Temp = FALSE;
    IP6_TASK_UNLOCK ();
    MEMSET (&Ip6Tempaddr, 0, sizeof (tIp6Addr));

    if (i4DisRouteAdvRDNSS == IP6_RA_NO_RDNSS)

    {                            /* Removal of RDNSS address when address is mentioned in specific */

        if ((Ip6AddrCompare (Ip6Tempaddr, Ip6AddrOne) != IP6_ZERO) ||
            (Ip6AddrCompare (Ip6Tempaddr, Ip6AddrTwo) != IP6_ZERO) ||
            (Ip6AddrCompare (Ip6Tempaddr, Ip6AddrThree) != IP6_ZERO))
        {
            IP6_TASK_LOCK ();
            Ip6ValidateRDNSS (Ip6AddrOne, i4Temp, i4Index);
            Ip6ValidateRDNSS (Ip6AddrTwo, i4Temp, i4Index);
            Ip6ValidateRDNSS (Ip6AddrThree, i4Temp, i4Index);
            IP6_TASK_UNLOCK ();

        }
        else
        {
            i4Temp = TRUE;
            IP6_TASK_LOCK ();
            Ip6ValidateRDNSS (Ip6AddrOne, i4Temp, i4Index);
            Ip6ValidateRDNSS (Ip6AddrTwo, i4Temp, i4Index);
            Ip6ValidateRDNSS (Ip6AddrThree, i4Temp, i4Index);
            IP6_TASK_UNLOCK ();
            if ((nmhSetFsipv6IfRaRDNSSRowStatus (i4Index, DESTROY)) ==
                SNMP_FAILURE)
            {
                IP6_TASK_LOCK ();
                CLI_FATAL_ERROR (CliHandle);
                return;
            }

        }
    }
    IP6_TASK_LOCK ();
    return;

}

/*********************************************************************
 *   Function Name : Ip6ValidateRDNSS
 *   Description   : Resets the values on disabling RDNSS on an interface
 *   Input(s)      : CliHandle
 *                   u4Index -Interface Index
 *   Output(s)     :
 *   Return Values : None.
 ************************************************************************/
VOID
Ip6ValidateRDNSS (tIp6Addr Ip6addr, INT4 i4Flag, INT4 i4Index)
{

    tIp6Addr            Ip6AddrOne;
    tIp6Addr            Ip6AddrTwo;
    tIp6Addr            Ip6AddrThree;
    UINT4               u4ErrorCode = 0;
    tSNMP_OCTET_STRING_TYPE taddressone;
    tSNMP_OCTET_STRING_TYPE taddresstwo;
    tSNMP_OCTET_STRING_TYPE taddressthree;
    UINT1               au1RDNSSAddr[IP6_ADDR_SIZE];
    UINT1               au1RDNSSAddrTwo[IP6_ADDR_SIZE];
    UINT1               au1RDNSSAddrThree[IP6_ADDR_SIZE];
    UINT4               u4Void = 0;
    taddressone.pu1_OctetList = au1RDNSSAddr;
    taddressone.i4_Length = IP6_ADDR_SIZE;
    taddresstwo.pu1_OctetList = au1RDNSSAddrTwo;
    taddressthree.pu1_OctetList = au1RDNSSAddrThree;
    taddresstwo.i4_Length = IP6_ADDR_SIZE;
    taddressthree.i4_Length = IP6_ADDR_SIZE;
    UINT4               u4Temp = FALSE;

    IP6_TASK_UNLOCK ();
    nmhGetFsipv6IfRaRDNSSAddrOne (i4Index, (&taddressone));
    nmhGetFsipv6IfRaRDNSSAddrTwo (i4Index, &taddresstwo);
    nmhGetFsipv6IfRaRDNSSAddrThree (i4Index, &taddressthree);

    MEMSET (&Ip6AddrOne, 0, sizeof (tIp6Addr));
    Ip6AddrCopy (&Ip6AddrOne, (tIp6Addr *) (VOID *) taddressone.pu1_OctetList);
    MEMSET (&Ip6AddrTwo, 0, sizeof (tIp6Addr));
    Ip6AddrCopy (&Ip6AddrTwo, (tIp6Addr *) (VOID *) taddresstwo.pu1_OctetList);
    MEMSET (&Ip6AddrThree, 0, sizeof (tIp6Addr));
    Ip6AddrCopy (&Ip6AddrThree,
                 (tIp6Addr *) (VOID *) taddressthree.pu1_OctetList);

    /* Deletion of the given  RDNSS server  on comparing with the existing addresses configured */
    if ((Ip6AddrCompare (Ip6addr, Ip6AddrOne) == IP6_ZERO) || (i4Flag == TRUE))
    {
        MEMSET (&Ip6AddrOne, 0, sizeof (tIp6Addr));
        taddressone.pu1_OctetList = au1RDNSSAddr;
        Ip6AddrCopy ((tIp6Addr *) (VOID *) taddressone.pu1_OctetList,
                     &Ip6AddrOne);
        taddressone.i4_Length = IP6_ADDR_SIZE;

        if (nmhTestv2Fsipv6IfRaRDNSSAddrOne
            (&u4ErrorCode, i4Index, &taddressone) == SNMP_FAILURE)
        {
            IP6_TASK_LOCK ();
            return;

        }
        if (nmhSetFsipv6IfRaRDNSSAddrOne (i4Index, &taddressone) ==
            SNMP_FAILURE)
        {
            IP6_TASK_LOCK ();
            return;
        }

        if (nmhSetFsipv6IfRaRDNSSAddrOneLife (i4Index, u4Void) == SNMP_FAILURE)
        {
            IP6_TASK_LOCK ();
            return;
        }
        u4Temp = TRUE;

    }
    if ((Ip6AddrCompare (Ip6addr, Ip6AddrTwo) == IP6_ZERO) || (i4Flag == TRUE))
    {
        MEMSET (&Ip6AddrTwo, 0, sizeof (tIp6Addr));
        taddresstwo.pu1_OctetList = au1RDNSSAddrTwo;
        Ip6AddrCopy ((tIp6Addr *) (VOID *) taddresstwo.pu1_OctetList,
                     &Ip6AddrTwo);
        taddresstwo.i4_Length = IP6_ADDR_SIZE;

        if (nmhTestv2Fsipv6IfRaRDNSSAddrTwo
            (&u4ErrorCode, i4Index, &taddresstwo) == SNMP_FAILURE)
        {
            IP6_TASK_LOCK ();
            return;
        }
        if (nmhSetFsipv6IfRaRDNSSAddrTwo (i4Index, &taddresstwo) ==
            SNMP_FAILURE)
        {
            IP6_TASK_LOCK ();
            return;
        }

        if (nmhSetFsipv6IfRaRDNSSAddrTwoLife (i4Index, u4Void) == SNMP_FAILURE)
        {
            IP6_TASK_LOCK ();
            return;
        }
        u4Temp = TRUE;

    }
    if ((Ip6AddrCompare (Ip6addr, Ip6AddrThree) == IP6_ZERO)
        || (i4Flag == TRUE))
    {

        MEMSET (&Ip6AddrThree, 0, sizeof (tIp6Addr));
        taddressthree.pu1_OctetList = au1RDNSSAddrThree;
        Ip6AddrCopy ((tIp6Addr *) (VOID *) taddressthree.pu1_OctetList,
                     &Ip6AddrThree);
        taddressthree.i4_Length = IP6_ADDR_SIZE;
        if (nmhTestv2Fsipv6IfRaRDNSSAddrThree
            (&u4ErrorCode, i4Index, &taddressthree) == SNMP_FAILURE)
        {
            IP6_TASK_LOCK ();
            return;
        }
        if (nmhSetFsipv6IfRaRDNSSAddrThree (i4Index, &taddressthree) ==
            SNMP_FAILURE)
        {
            IP6_TASK_LOCK ();
            return;
        }
        if (nmhSetFsipv6IfRaRDNSSAddrThreeLife (i4Index, u4Void) ==
            SNMP_FAILURE)
        {
            IP6_TASK_LOCK ();
            return;
        }
        u4Temp = TRUE;

    }
    /* If given RDNSS server is not present, error is thrown */
    if (u4Temp == FALSE)

    {
        IP6_TASK_LOCK ();
        CLI_SET_ERR (CLI_IP6_INVALID_RT_ADV_RDNSS_ADDR);
        return;
    }

    IP6_TASK_LOCK ();
    return;
}

/*********************************************************************
 *   Function Name : Ip6EnableRouteAdvRDNSSStatus
 *   Description   : Enables RDNSS on an interface
 *   Input(s)      : CliHandle
 *                   u4Index -Interface Index
 *                   i4RouteAdvRDNSSStatus - value to be set
 *   Output(s)     :
 *   Return Values : None.
 * *********************************************************************/

VOID
Ip6EnableRouteAdvRDNSSStatus (tCliHandle CliHandle, UINT4 u4Index,
                              INT4 i4RouteAdvRDNSSStatus)
{
    UINT4               u4ErrorCode = 0;
    INT1                i1RetVal = SNMP_FAILURE;
    INT4                i4RsVal = SNMP_FAILURE;
    INT4                i4Index = (INT4) u4Index;
    /* IPv6Lock will be taken in the IPvx Lowlevel So Relase Now and
     * before return take */
    IP6_TASK_UNLOCK ();

    /* Router Adv Config */
    i1RetVal = nmhGetFsipv6IfRaRDNSSRowStatus (i4Index, &i4RsVal);
    if (i1RetVal == SNMP_FAILURE)
    {
        /* Create a New Entry in the RA Table */
        if ((nmhTestv2Fsipv6IfRaRDNSSRowStatus (&u4ErrorCode,
                                                i4Index,
                                                CREATE_AND_WAIT)) ==
            SNMP_FAILURE)
        {
            IP6_TASK_LOCK ();
            return;
        }
        if ((nmhSetFsipv6IfRaRDNSSRowStatus (i4Index, CREATE_AND_WAIT))
            == SNMP_FAILURE)
        {
            IP6_TASK_LOCK ();
            CLI_FATAL_ERROR (CliHandle);
            return;
        }
    }

    if ((nmhTestv2Fsipv6IfRadvRDNSSOpen
         (&u4ErrorCode, i4Index, i4RouteAdvRDNSSStatus)) == SNMP_FAILURE)
    {
        IP6_TASK_LOCK ();
        return;
    }

    if ((nmhSetFsipv6IfRadvRDNSSOpen (i4Index,
                                      i4RouteAdvRDNSSStatus)) == SNMP_FAILURE)
    {
        IP6_TASK_LOCK ();
        CLI_FATAL_ERROR (CliHandle);
        return;
    }
    if (i4RouteAdvRDNSSStatus == IP6_RA_RDNSS_OPEN)
    {
        if ((nmhSetFsipv6IfRaRDNSSRowStatus (i4Index, ACTIVE)) == SNMP_FAILURE)
        {
            IP6_TASK_LOCK ();
            CLI_FATAL_ERROR (CliHandle);
            return;
        }
    }
    else
    {
        if ((nmhSetFsipv6IfRaRDNSSRowStatus (i4Index, DESTROY)) == SNMP_FAILURE)
        {
            IP6_TASK_LOCK ();
            CLI_FATAL_ERROR (CliHandle);
            return;
        }
    }

    IP6_TASK_LOCK ();
    return;
}

/*********************************************************************
*  Function Name : Ip6RaRDNSSPreference
*  Description   : Setting RDNSS Preference
*  Input(s)      : CliHandle
*                  u4Index - Interface Index
*                  i4RDNSSPreference
*  Output(s)     :
*  Return Values : None.
*********************************************************************/

VOID
Ip6RaRDNSSPreference (tCliHandle CliHandle, UINT4 u4Index,
                      INT4 i4RDNSSPreference)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4RsVal = SNMP_FAILURE;
    INT1                i1RetVal = SNMP_FAILURE;
    INT4                i4Index = (INT4) u4Index;

    if (Ip6IsIfFwdEnabled (i4Index) == FALSE)
    {
        if (Ip6ifEntryExists (u4Index) == IP6_FAILURE)
        {
            CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
            return;
        }

        if (nmhTestv2Fsipv6IfRaRDNSSPreference
            (&u4ErrorCode, i4Index, i4RDNSSPreference) == SNMP_FAILURE)
        {
            return;
        }

        if (nmhSetFsipv6IfRaRDNSSPreference (i4Index, i4RDNSSPreference) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return;
        }
        return;
    }

    /* IPv6Lock will be taken in the IPvx Lowlevel So Relase Now and
     * before return take */
    IP6_TASK_UNLOCK ();

    /* Router Adv Config */
    i1RetVal = nmhGetFsipv6IfRaRDNSSRowStatus (i4Index, &i4RsVal);
    if (i1RetVal == SNMP_FAILURE)
    {
        /* Create a New Entry in the RA Table */
        i4RsVal = CREATE_AND_WAIT;
        if ((nmhTestv2Fsipv6IfRaRDNSSRowStatus (&u4ErrorCode,
                                                i4Index, i4RsVal))
            == SNMP_FAILURE)
        {
            IP6_TASK_LOCK ();
            return;
        }
        if ((nmhSetFsipv6IfRaRDNSSRowStatus (i4Index, i4RsVal)) == SNMP_FAILURE)
        {
            IP6_TASK_LOCK ();
            CLI_FATAL_ERROR (CliHandle);
            return;
        }
    }
    if ((nmhTestv2Fsipv6IfRaRDNSSPreference
         (&u4ErrorCode, i4Index, i4RDNSSPreference)) == SNMP_FAILURE)
    {
        IP6_TASK_LOCK ();
        return;
    }

    if ((nmhSetFsipv6IfRaRDNSSPreference (i4Index,
                                          i4RDNSSPreference)) == SNMP_FAILURE)
    {
        IP6_TASK_LOCK ();
        CLI_FATAL_ERROR (CliHandle);
        return;
    }
    if (i4RsVal == IP6_RA_RDNSS)
    {
        if ((nmhSetFsipv6IfRaRDNSSRowStatus (i4Index, ACTIVE)) == SNMP_FAILURE)
        {
            IP6_TASK_LOCK ();
            CLI_FATAL_ERROR (CliHandle);
            return;
        }
    }
    else
    {
        if ((nmhSetFsipv6IfRaRDNSSRowStatus (i4Index, DESTROY)) == SNMP_FAILURE)
        {
            IP6_TASK_LOCK ();
            CLI_FATAL_ERROR (CliHandle);
            return;
        }
    }
    IP6_TASK_LOCK ();
    return;

}

/*********************************************************************
 * * + *   Function Name : Ip6RaDnsDomNameLifeTime
 * * + *   Description   : Enables RDNSS on an interface
 * * + *   Input(s)      : CliHandle
 * * + *                   u4Index -Interface Index
 * * + *                   i4RouteAdvRDNSS - value to be set
 * * + *   Output(s)     :
 * * + *   Return Values : None.
 * * + ** *********************************************************************/
VOID
Ip6RaDnsDomNameLifeTime (tCliHandle CliHandle, UINT4 u4Index,
                         tSNMP_OCTET_STRING_TYPE * pDomNameOneOct,
                         UINT4 u4LifeTimeOne, INT4 i4DomLifeTime)
{
    UINT4               u4ErrorCode = 0;
    tSNMP_OCTET_STRING_TYPE DNSAddr, Fsipv6DomName, DNSAddrTwo, DNSAddrThree;
    UINT1               au1Buf[IP6_DOMAIN_NAME_LEN + 1];
    UINT1               au1Buf1[IP6_DOMAIN_NAME_LEN + 1];
    UINT1               au1Buf2[IP6_DOMAIN_NAME_LEN + 1];
    UINT1               au1Buf3[IP6_DOMAIN_NAME_LEN + 1];
    INT4                i4RetVal = 0;
    INT4                i4LifeTime = 0;
#ifdef LNXIP6_WANTED
    tLip6If            *pIf6Entry = NULL;
#endif
    IP6_INIT_OCT (Fsipv6DomName, au1Buf);
    IP6_INIT_OCT (DNSAddr, au1Buf1);
    IP6_INIT_OCT (DNSAddrTwo, au1Buf2);
    IP6_INIT_OCT (DNSAddrThree, au1Buf3);
    DNSAddr.i4_Length = IP6_DOMAIN_NAME_LEN + 1;
    DNSAddrTwo.i4_Length = IP6_DOMAIN_NAME_LEN + 1;
    DNSAddrThree.i4_Length = IP6_DOMAIN_NAME_LEN + 1;
    Fsipv6DomName.i4_Length = IP6_DOMAIN_NAME_LEN + 1;
    Fsipv6DomName = *pDomNameOneOct;

#ifdef LNXIP6_WANTED
    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) u4Index);
    if (pIf6Entry == NULL)
    {
        return;
    }

    if (i4DomLifeTime == IP6_RA_NO_DNS_DOMNAME)
    {
        if (pIf6Entry->u1RAdvStatus == IP6_ND_NO_SUPPRESS_RA)
        {
            CLI_SET_ERR (CLI_IP6_SUPPRESSRA_IN_PROGRESS);
            return;
        }
    }
#endif

    IP6_TASK_UNLOCK ();

    if (Ip6IsIfFwdEnabled ((INT4) u4Index) == FALSE)
    {
        if (Ip6ifEntryExists (u4Index) == IP6_FAILURE)
        {
            IP6_TASK_LOCK ();
            CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
            return;
        }
        if (nmhTestv2Fsipv6IfRaRDNSSRowStatus
            (&u4ErrorCode, (INT4) u4Index,
             (INT4) CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            IP6_TASK_LOCK ();
            CLI_FATAL_ERROR (CliHandle);
            return;
        }

        if (nmhSetFsipv6IfRaRDNSSRowStatus
            ((INT4) u4Index, (INT4) CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            IP6_TASK_LOCK ();
            CLI_FATAL_ERROR (CliHandle);
            return;
        }
    }
    if (i4DomLifeTime == IP6_RA_DNS_DOMNAME)
    {
        /* The given ipv6 address is allocated only if valid lifetime is given and 
           does'nt reach the maximum DNS servers limit */
#ifdef LNXIP6_WANTED
        pIf6Entry->u1RADNSStateActive = pIf6Entry->u1RAdvRDNSS;
#endif

        nmhGetFsipv6IfDomainNameOne ((INT4) u4Index, &DNSAddr);
        if ((STRCMP (DNSAddr.pu1_OctetList, Fsipv6DomName.pu1_OctetList) ==
             IP6_ZERO) || (STRLEN (DNSAddr.pu1_OctetList) == IP6_ZERO))
        {
            if (nmhTestv2Fsipv6IfDomainNameOneLife
                (&u4ErrorCode, (INT4) u4Index, u4LifeTimeOne) == SNMP_FAILURE)
            {
                IP6_TASK_LOCK ();
                return;
            }
            i4RetVal = nmhGetFsipv6IfDnsLifeTime ((INT4) u4Index, &i4LifeTime);
            if (i4RetVal == SNMP_SUCCESS)
            {
                if (u4LifeTimeOne == IP6_ND_DEF_RDNSS_LIFETIME)
                {
                    if (i4LifeTime != IP6_ZERO)
                    {
                        u4LifeTimeOne = (UINT4) i4LifeTime;
                    }
                    else
                    {
                        u4LifeTimeOne = IP6_ND_DEF_RDNSS_LIFETIME;
                    }
                }

            }
            if (nmhSetFsipv6IfDomainNameOneLife
                ((INT4) u4Index, (INT4) u4LifeTimeOne) == SNMP_FAILURE)
            {
                IP6_TASK_LOCK ();
                CLI_FATAL_ERROR (CliHandle);
                return;
            }
            if (nmhTestv2Fsipv6IfDomainNameOne
                (&u4ErrorCode, (INT4) u4Index, &Fsipv6DomName) == SNMP_FAILURE)
            {
                IP6_TASK_LOCK ();
                return;
            }
            if (nmhSetFsipv6IfDomainNameOne ((INT4) u4Index, &Fsipv6DomName) ==
                SNMP_FAILURE)
            {
                IP6_TASK_LOCK ();
                CLI_FATAL_ERROR (CliHandle);
                return;
            }
            if (i4DomLifeTime == IP6_RA_DNS_DOMNAME)
            {
                if ((nmhSetFsipv6IfRaRDNSSRowStatus ((INT4) u4Index, ACTIVE)) ==
                    SNMP_FAILURE)
                {
                    IP6_TASK_LOCK ();
                    CLI_FATAL_ERROR (CliHandle);
                    return;
                }
            }
            IP6_TASK_LOCK ();
            return;

        }

        nmhGetFsipv6IfDomainNameTwo ((INT4) u4Index, &DNSAddrTwo);
        if ((STRCMP (DNSAddrTwo.pu1_OctetList, Fsipv6DomName.pu1_OctetList) ==
             IP6_ZERO) || (STRLEN (DNSAddrTwo.pu1_OctetList) == IP6_ZERO))
        {

            if (nmhTestv2Fsipv6IfDomainNameTwoLife
                (&u4ErrorCode, (INT4) u4Index, u4LifeTimeOne) == SNMP_FAILURE)
            {
                IP6_TASK_LOCK ();
                return;
            }

            i4RetVal = nmhGetFsipv6IfDnsLifeTime ((INT4) u4Index, &i4LifeTime);
            if (i4RetVal == SNMP_SUCCESS)
            {
                if (u4LifeTimeOne == IP6_ND_DEF_RDNSS_LIFETIME)
                {
                    if (i4LifeTime != IP6_ZERO)
                    {
                        u4LifeTimeOne = (UINT4) i4LifeTime;
                    }
                    else
                    {
                        u4LifeTimeOne = IP6_ND_DEF_RDNSS_LIFETIME;
                    }
                }
            }
            if (nmhSetFsipv6IfDomainNameTwoLife
                ((INT4) u4Index, (INT4) u4LifeTimeOne) == SNMP_FAILURE)
            {
                IP6_TASK_LOCK ();
                CLI_FATAL_ERROR (CliHandle);
                return;
            }
            if (nmhTestv2Fsipv6IfDomainNameTwo
                (&u4ErrorCode, (INT4) u4Index, &Fsipv6DomName) == SNMP_FAILURE)
            {
                IP6_TASK_LOCK ();
                CLI_FATAL_ERROR (CliHandle);
                return;
            }
            if (nmhSetFsipv6IfDomainNameTwo ((INT4) u4Index, &Fsipv6DomName) ==
                SNMP_FAILURE)
            {
                IP6_TASK_LOCK ();
                CLI_FATAL_ERROR (CliHandle);
                return;
            }
            if (i4DomLifeTime == IP6_RA_DNS_DOMNAME)
            {
                if ((nmhSetFsipv6IfRaRDNSSRowStatus ((INT4) u4Index, ACTIVE)) ==
                    SNMP_FAILURE)
                {
                    IP6_TASK_LOCK ();
                    CLI_FATAL_ERROR (CliHandle);
                    return;
                }
            }
            IP6_TASK_LOCK ();
            return;
        }

        nmhGetFsipv6IfDomainNameThree ((INT4) u4Index, &DNSAddrThree);
        if ((STRCMP (DNSAddrThree.pu1_OctetList, Fsipv6DomName.pu1_OctetList) ==
             IP6_ZERO) || (STRLEN (DNSAddrThree.pu1_OctetList) == IP6_ZERO))
        {
            if (nmhTestv2Fsipv6IfDomainNameThreeLife
                (&u4ErrorCode, (INT4) u4Index, u4LifeTimeOne) == SNMP_FAILURE)
            {
                IP6_TASK_LOCK ();
                return;
            }

            i4RetVal = nmhGetFsipv6IfDnsLifeTime ((INT4) u4Index, &i4LifeTime);
            if (i4RetVal == SNMP_SUCCESS)
            {
                if (u4LifeTimeOne == IP6_ND_DEF_RDNSS_LIFETIME)
                {
                    if (i4LifeTime != IP6_ZERO)
                    {
                        u4LifeTimeOne = (UINT4) i4LifeTime;
                    }
                    else
                    {
                        u4LifeTimeOne = IP6_ND_DEF_RDNSS_LIFETIME;
                    }
                }
            }
            if (nmhSetFsipv6IfDomainNameThreeLife
                ((INT4) u4Index, (INT4) u4LifeTimeOne) == SNMP_FAILURE)
            {
                IP6_TASK_LOCK ();
                CLI_FATAL_ERROR (CliHandle);
                return;
            }
            if (nmhTestv2Fsipv6IfDomainNameThree
                (&u4ErrorCode, (INT4) u4Index, &Fsipv6DomName) == SNMP_FAILURE)
            {
                IP6_TASK_LOCK ();
                CLI_FATAL_ERROR (CliHandle);
                return;
            }

            if (nmhSetFsipv6IfDomainNameThree ((INT4) u4Index, &Fsipv6DomName)
                == SNMP_FAILURE)
            {
                IP6_TASK_LOCK ();
                CLI_FATAL_ERROR (CliHandle);
                return;
            }
            if (i4DomLifeTime == IP6_RA_DNS_DOMNAME)
            {
                if ((nmhSetFsipv6IfRaRDNSSRowStatus ((INT4) u4Index, ACTIVE)) ==
                    SNMP_FAILURE)
                {
                    IP6_TASK_LOCK ();
                    CLI_FATAL_ERROR (CliHandle);
                    return;
                }
            }
            IP6_TASK_LOCK ();
            return;
        }
    }
    /* On reaching the maximum RDNSS servers  limit ie.3 , error is thrown */
    CLI_SET_ERR (CLI_IP6_RT_ADV_MAX_RDNSS);
    return;

}

/*********************************************************************
 *   Function Name : Ip6RaNoDnsDomNameLifeTime
 *   Description   : Disables DNS on an interface
 *   Input(s)      : CliHandle
 *                   u4Index -Interface Index
 *                   i4DisRouteAdvDNS - value to be set
 *   Output(s)     :
 *   Return Values : None.
 ***********************************************************************/
VOID
Ip6RaNoDnsDomNameLifeTime (tCliHandle CliHandle, UINT4 u4Index,
                           tSNMP_OCTET_STRING_TYPE * pDomNameOneOct,
                           tSNMP_OCTET_STRING_TYPE * pDomNameTwoOct,
                           tSNMP_OCTET_STRING_TYPE * pDomNameThreeOct,
                           INT4 i4DisRouteAdvDNS)
{
    INT4                i4Index = (INT4) u4Index;
    INT4                i4Temp = FALSE;
    IP6_TASK_UNLOCK ();
    tSNMP_OCTET_STRING_TYPE Fsipv6DomName;
    tSNMP_OCTET_STRING_TYPE Fsipv6DomNameTwo;
    tSNMP_OCTET_STRING_TYPE Fsipv6DomNameThree;
    tSNMP_OCTET_STRING_TYPE DNSAddr;
    UINT1               au1Buf[IP6_DOMAIN_NAME_LEN + 1];
    UINT1               au1Buf1[IP6_DOMAIN_NAME_LEN + 1];
    UINT1               au1Buf2[IP6_DOMAIN_NAME_LEN + 1];
    UINT1               au1Buf3[IP6_DOMAIN_NAME_LEN + 1];
    IP6_INIT_OCT (Fsipv6DomName, au1Buf);
    IP6_INIT_OCT (Fsipv6DomNameTwo, au1Buf1);
    IP6_INIT_OCT (Fsipv6DomNameThree, au1Buf2);
    IP6_INIT_OCT (DNSAddr, au1Buf3);
    Fsipv6DomName = *pDomNameOneOct;
    Fsipv6DomNameTwo = *pDomNameTwoOct;
    Fsipv6DomNameThree = *pDomNameThreeOct;
    Fsipv6DomName.i4_Length = IP6_DOMAIN_NAME_LEN + 1;
    Fsipv6DomNameTwo.i4_Length = IP6_DOMAIN_NAME_LEN + 1;
    Fsipv6DomNameThree.i4_Length = IP6_DOMAIN_NAME_LEN + 1;
    DNSAddr.i4_Length = IP6_DOMAIN_NAME_LEN + 1;
#ifdef LNXIP6_WANTED
    tLip6If            *pIf6Entry = NULL;
    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) u4Index);

    if (pIf6Entry == NULL)
    {
        return;
    }

#endif

    if (i4DisRouteAdvDNS == IP6_RA_NO_DNS_DOMNAME)

    {
        /* Removal of DNS address when address is mentioned in specific */

        if ((STRCMP (DNSAddr.pu1_OctetList, Fsipv6DomName.pu1_OctetList) !=
             IP6_ZERO)
            || (STRCMP (DNSAddr.pu1_OctetList, Fsipv6DomNameTwo.pu1_OctetList)
                != IP6_ZERO)
            || (STRCMP (DNSAddr.pu1_OctetList, Fsipv6DomNameThree.pu1_OctetList)
                != IP6_ZERO))
        {
            IP6_TASK_LOCK ();
            Ip6ValidateDNS (&Fsipv6DomName, i4Temp, i4Index);
            Ip6ValidateDNS (&Fsipv6DomNameTwo, i4Temp, i4Index);
            Ip6ValidateDNS (&Fsipv6DomNameThree, i4Temp, i4Index);
            IP6_TASK_UNLOCK ();

        }
        else
        {
            i4Temp = TRUE;
            IP6_TASK_LOCK ();
            Ip6ValidateDNS (&Fsipv6DomName, i4Temp, i4Index);
            Ip6ValidateDNS (&Fsipv6DomNameTwo, i4Temp, i4Index);
            Ip6ValidateDNS (&Fsipv6DomNameThree, i4Temp, i4Index);
            IP6_TASK_UNLOCK ();
            if ((nmhSetFsipv6IfRaRDNSSRowStatus (i4Index, DESTROY)) ==
                SNMP_FAILURE)
            {
                IP6_TASK_LOCK ();
                CLI_FATAL_ERROR (CliHandle);
                return;
            }

#ifdef LNXIP6_WANTED
            pIf6Entry->u1RADNSStateActive = pIf6Entry->u1RAdvRDNSS;
#endif
        }
    }
    IP6_TASK_LOCK ();
    return;

}

/*********************************************************************
 *   Function Name : Ip6ValidateDNS
 *   Description   : Resets the values on disabling DNS on an interface
 *   Input(s)      : CliHandle
 *                   u4Index -Interface Index
 *   Output(s)     :
 *   Return Values : None.
 ************************************************************************/
VOID
Ip6ValidateDNS (tSNMP_OCTET_STRING_TYPE * pDomNameOneOct, INT4 i4Flag,
                INT4 i4Index)
{
    tSNMP_OCTET_STRING_TYPE DNSAddr;
    tSNMP_OCTET_STRING_TYPE DNSAddrTwo;
    tSNMP_OCTET_STRING_TYPE DNSAddrThree;
    tSNMP_OCTET_STRING_TYPE Fsipv6DomName;
    UINT1               au1Buf[IP6_DOMAIN_NAME_LEN + 1];
    UINT1               au1Buf1[IP6_DOMAIN_NAME_LEN + 1];
    UINT1               au1Buf2[IP6_DOMAIN_NAME_LEN + 1];
    UINT1               au1Buf3[IP6_DOMAIN_NAME_LEN + 1];
    IP6_INIT_OCT (DNSAddr, au1Buf);
    IP6_INIT_OCT (DNSAddrTwo, au1Buf1);
    IP6_INIT_OCT (DNSAddrThree, au1Buf2);
    IP6_INIT_OCT (Fsipv6DomName, au1Buf3);
    UINT4               u4Temp = FALSE;
    UINT4               u4Void = 0;
    UINT4               u4ErrorCode = 0;
    Fsipv6DomName = *pDomNameOneOct;
    DNSAddr.i4_Length = IP6_DOMAIN_NAME_LEN + 1;
    DNSAddrTwo.i4_Length = IP6_DOMAIN_NAME_LEN + 1;
    DNSAddrThree.i4_Length = IP6_DOMAIN_NAME_LEN + 1;
    Fsipv6DomName.i4_Length = IP6_DOMAIN_NAME_LEN + 1;

    /* Retrieving the DNS Server addresses stored */
    IP6_TASK_UNLOCK ();

    nmhGetFsipv6IfDomainNameOne (i4Index, &DNSAddr);
    nmhGetFsipv6IfDomainNameTwo (i4Index, &DNSAddrTwo);
    nmhGetFsipv6IfDomainNameThree (i4Index, &DNSAddrThree);

    /* Deletion of the given  DNS server  on comparing with the existing addresses configured */
    if ((STRCMP (DNSAddr.pu1_OctetList, Fsipv6DomName.pu1_OctetList) ==
         IP6_ZERO) || (i4Flag == TRUE))
    {
        MEMSET (&DNSAddr.pu1_OctetList, 0, sizeof (au1Buf));

        if (nmhTestv2Fsipv6IfDomainNameOne (&u4ErrorCode, i4Index, &DNSAddr) ==
            SNMP_FAILURE)
        {
            IP6_TASK_LOCK ();
            return;

        }
        if (nmhSetFsipv6IfDomainNameOne (i4Index, &DNSAddr) == SNMP_FAILURE)
        {
            IP6_TASK_LOCK ();
            return;
        }
        if (nmhSetFsipv6IfDomainNameOneLife (i4Index, (INT4) u4Void) ==
            SNMP_FAILURE)
        {
            IP6_TASK_LOCK ();
            return;
        }
        u4Temp = TRUE;

    }
    if ((STRCMP (DNSAddrTwo.pu1_OctetList, Fsipv6DomName.pu1_OctetList) ==
         IP6_ZERO) || (i4Flag == TRUE))
    {
        MEMSET (&DNSAddrTwo.pu1_OctetList, 0, sizeof (au1Buf1));

        if (nmhTestv2Fsipv6IfDomainNameTwo (&u4ErrorCode, i4Index, &DNSAddrTwo)
            == SNMP_FAILURE)
        {
            IP6_TASK_LOCK ();
            return;
        }
        if (nmhSetFsipv6IfDomainNameTwo (i4Index, &DNSAddrTwo) == SNMP_FAILURE)
        {
            IP6_TASK_LOCK ();
            return;
        }

        if (nmhSetFsipv6IfDomainNameTwoLife (i4Index, (INT4) u4Void) ==
            SNMP_FAILURE)
        {
            IP6_TASK_LOCK ();
            return;
        }
        u4Temp = TRUE;

    }
    if ((STRCMP (DNSAddrThree.pu1_OctetList, Fsipv6DomName.pu1_OctetList) ==
         IP6_ZERO) || (i4Flag == TRUE))
    {
        MEMSET (&DNSAddrThree.pu1_OctetList, 0, sizeof (au1Buf2));
        if (nmhTestv2Fsipv6IfDomainNameThree
            (&u4ErrorCode, i4Index, &DNSAddrThree) == SNMP_FAILURE)
        {
            IP6_TASK_LOCK ();
            return;
        }
        if (nmhSetFsipv6IfDomainNameThree (i4Index, &DNSAddrThree) ==
            SNMP_FAILURE)
        {
            IP6_TASK_LOCK ();
            return;
        }
        if (nmhSetFsipv6IfDomainNameThreeLife (i4Index, (INT4) u4Void) ==
            SNMP_FAILURE)
        {
            IP6_TASK_LOCK ();
            return;
        }
        u4Temp = TRUE;

    }
    /* If given DNS server is not present, error is thrown */
    if (u4Temp == FALSE)

    {
        IP6_TASK_LOCK ();
        CLI_SET_ERR (CLI_IP6_INVALID_RT_ADV_DNS_ADDR);
        return;
    }

    IP6_TASK_LOCK ();
    return;
}

/*********************************************************************
 *   Function Name : Ip6RaDnsLifeTime
 *   Description   : Configure DNS Lifetime on an interface
 *   Input(s)      : CliHandle
 *                   u4Index -Interface Index
 *                   i4DNSLifetime - value to be set
 *   Output(s)     :
 *   Return Values : None.
 **********************************************************************/
VOID
Ip6RaDnsLifeTime (tCliHandle CliHandle, UINT4 u4Index, INT4 i4DNSLifetime)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4RsVal = SNMP_FAILURE;
    INT1                i1RetVal = SNMP_FAILURE;

    if (Ip6IsIfFwdEnabled ((INT4) u4Index) == FALSE)
    {
        if (Ip6ifEntryExists (u4Index) == IP6_FAILURE)
        {
            CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
            return;
        }
        if (nmhTestv2Fsipv6IfDnsLifeTime
            (&u4ErrorCode, (INT4) u4Index,
             (UINT4) i4DNSLifetime) == SNMP_FAILURE)
        {
            IP6_TASK_LOCK ();
            CLI_FATAL_ERROR (CliHandle);
            return;
        }
        if (nmhSetFsipv6IfDnsLifeTime ((INT4) u4Index, i4DNSLifetime) ==
            SNMP_FAILURE)
        {
            IP6_TASK_LOCK ();
            CLI_FATAL_ERROR (CliHandle);
            return;
        }
        return;
    }
    /* IPv6Lock will be taken in the IPvx Lowlevel So Relase Now and
     *      *      * before return take */
    IP6_TASK_UNLOCK ();

    /* Router Adv Config */
    i1RetVal = nmhGetFsipv6IfRaRDNSSRowStatus ((INT4) u4Index, &i4RsVal);
    if (i1RetVal == SNMP_FAILURE)
    {
        /* Create a New Entry in the RA Table */
        i4RsVal = CREATE_AND_WAIT;
        if ((nmhTestv2Fsipv6IfRaRDNSSRowStatus (&u4ErrorCode,
                                                (INT4) u4Index, i4RsVal))
            == SNMP_FAILURE)
        {
            IP6_TASK_LOCK ();
            return;
        }
        if ((nmhSetFsipv6IfRaRDNSSRowStatus ((INT4) u4Index, i4RsVal))
            == SNMP_FAILURE)
        {
            IP6_TASK_LOCK ();
            CLI_FATAL_ERROR (CliHandle);
            return;
        }
    }

    if (nmhTestv2Fsipv6IfDnsLifeTime
        (&u4ErrorCode, (INT4) u4Index, (UINT4) i4DNSLifetime) == SNMP_FAILURE)
    {
        IP6_TASK_LOCK ();
        return;
    }

    if ((nmhSetFsipv6IfDnsLifeTime ((INT4) u4Index, i4DNSLifetime) ==
         SNMP_FAILURE))
    {
        IP6_TASK_LOCK ();
        CLI_FATAL_ERROR (CliHandle);
        return;
    }
    if (i4RsVal == IP6_RA_RDNSS)
    {
        if ((nmhSetFsipv6IfRaRDNSSRowStatus ((INT4) u4Index, ACTIVE))
            == SNMP_FAILURE)
        {
            IP6_TASK_LOCK ();
            CLI_FATAL_ERROR (CliHandle);
            return;
        }
    }
    else
    {
        if ((nmhSetFsipv6IfRaRDNSSRowStatus ((INT4) u4Index, DESTROY))
            == SNMP_FAILURE)
        {
            IP6_TASK_LOCK ();
            CLI_FATAL_ERROR (CliHandle);
            return;
        }
    }
    IP6_TASK_LOCK ();
    return;

}

/*********************************************************************
*  Function Name : Ip6RaRDNSSLifetime
*  Description   : Setting RDNSS Lifetime
*  Input(s)      : CliHandle
*                  u4Index - Interface Index
*                  i4RDNSSLifetime
*  Output(s)     :
*  Return Values : None.
*********************************************************************/

VOID
Ip6RaRDNSSLifetime (tCliHandle CliHandle, UINT4 u4Index, INT4 i4RDNSSLifetime)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4RsVal = SNMP_FAILURE;
    INT1                i1RetVal = SNMP_FAILURE;
    INT4                i4Index = (INT4) u4Index;

    if (Ip6IsIfFwdEnabled (i4Index) == FALSE)
    {
        if (Ip6ifEntryExists (u4Index) == IP6_FAILURE)
        {
            CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
            return;
        }

        if (nmhTestv2Fsipv6IfRaRDNSSLifetime
            (&u4ErrorCode, i4Index, (UINT4) i4RDNSSLifetime) == SNMP_FAILURE)
        {
            return;
        }
        if (nmhSetFsipv6IfRaRDNSSLifetime (i4Index, (UINT4) i4RDNSSLifetime) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return;
        }
        return;
    }

    /* IPv6Lock will be taken in the IPvx Lowlevel So Relase Now and
     * before return take */
    IP6_TASK_UNLOCK ();

    /* Router Adv Config */
    i1RetVal = nmhGetFsipv6IfRaRDNSSRowStatus (i4Index, &i4RsVal);
    if (i1RetVal == SNMP_FAILURE)
    {
        /* Create a New Entry in the RA Table */
        i4RsVal = CREATE_AND_WAIT;
        if ((nmhTestv2Fsipv6IfRaRDNSSRowStatus (&u4ErrorCode,
                                                i4Index, i4RsVal))
            == SNMP_FAILURE)
        {
            IP6_TASK_LOCK ();
            return;
        }
        if ((nmhSetFsipv6IfRaRDNSSRowStatus (i4Index, i4RsVal)) == SNMP_FAILURE)
        {
            IP6_TASK_LOCK ();
            CLI_FATAL_ERROR (CliHandle);
            return;
        }
    }
    if ((nmhTestv2Fsipv6IfRaRDNSSLifetime
         (&u4ErrorCode, i4Index, (UINT4) i4RDNSSLifetime)) == SNMP_FAILURE)
    {
        IP6_TASK_LOCK ();
        return;
    }

    if ((nmhSetFsipv6IfRaRDNSSLifetime (i4Index,
                                        (UINT4) i4RDNSSLifetime)) ==
        SNMP_FAILURE)
    {
        IP6_TASK_LOCK ();
        CLI_FATAL_ERROR (CliHandle);
        return;
    }
    if (i4RsVal == IP6_RA_RDNSS)
    {
        if ((nmhSetFsipv6IfRaRDNSSRowStatus (i4Index, ACTIVE)) == SNMP_FAILURE)
        {
            IP6_TASK_LOCK ();
            CLI_FATAL_ERROR (CliHandle);
            return;
        }
    }
    else
    {
        if ((nmhSetFsipv6IfRaRDNSSRowStatus (i4Index, DESTROY)) == SNMP_FAILURE)
        {
            IP6_TASK_LOCK ();
            CLI_FATAL_ERROR (CliHandle);
            return;
        }
    }
    IP6_TASK_LOCK ();
    return;

}

/*********************************************************************
*  Function Name : Ip6EnableRouteAdvStatus 
*  Description   : Enabling Router Advtisement Status 
*  Input(s)      : CliHandle
*                  u4Index - Interface Index
*                  i4RouterAdvtStatus - ENABLE/DISABLE
*  Output(s)     :  
*  Return Values : None.
*********************************************************************/

VOID
Ip6EnableRouteAdvStatus (tCliHandle CliHandle,
                         UINT4 u4Index, INT4 i4RouteAdvStatus)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4RsVal = SNMP_FAILURE;
    INT1                i1RetVal = SNMP_FAILURE;

    if (Ip6IsIfFwdEnabled (u4Index) == FALSE)
    {
        if (Ip6ifEntryExists (u4Index) == IP6_FAILURE)
        {
            CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
            return;
        }

        if (nmhTestv2Fsipv6IfRouterAdvStatus
            (&u4ErrorCode, u4Index, i4RouteAdvStatus) == SNMP_FAILURE)
        {
            return;
        }

        if (nmhSetFsipv6IfRouterAdvStatus
            (u4Index, i4RouteAdvStatus) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return;
        }
        return;
    }

    /* IPv6Lock will be taken in the IPvx Lowlevel So Relase Now and
     * before return take */
    IP6_TASK_UNLOCK ();

    /* Router Adv Config */
    i1RetVal = nmhGetIpv6RouterAdvertRowStatus ((INT4) u4Index, &i4RsVal);
    if (i1RetVal == SNMP_FAILURE)
    {
        /* Create a New Entry in the RA Table */
        i4RsVal = CREATE_AND_WAIT;
        if ((nmhTestv2Ipv6RouterAdvertRowStatus (&u4ErrorCode,
                                                 (INT4) u4Index, i4RsVal))
            == SNMP_FAILURE)
        {
            IP6_TASK_LOCK ();
            return;
        }
        if ((nmhSetIpv6RouterAdvertRowStatus ((INT4) u4Index, i4RsVal))
            == SNMP_FAILURE)
        {
            IP6_TASK_LOCK ();
            CLI_FATAL_ERROR (CliHandle);
            return;
        }
    }

    if ((nmhTestv2Ipv6RouterAdvertSendAdverts
         (&u4ErrorCode, (INT4) u4Index, i4RouteAdvStatus)) == SNMP_FAILURE)
    {
        IP6_TASK_LOCK ();
        return;
    }

    if ((nmhSetIpv6RouterAdvertSendAdverts ((INT4) u4Index,
                                            i4RouteAdvStatus)) == SNMP_FAILURE)
    {
        IP6_TASK_LOCK ();
        CLI_FATAL_ERROR (CliHandle);
        return;
    }

    if (i4RouteAdvStatus == IP6_ND_NO_SUPPRESS_RA)
    {
        /* Set ACTIVE in the Rowstatus only Enable RA */
        i4RsVal = ACTIVE;
        if ((nmhTestv2Ipv6RouterAdvertRowStatus (&u4ErrorCode,
                                                 (INT4) u4Index, i4RsVal))
            == SNMP_FAILURE)
        {
            IP6_TASK_LOCK ();
            return;
        }
        if ((nmhSetIpv6RouterAdvertRowStatus ((INT4) u4Index, i4RsVal))
            == SNMP_FAILURE)
        {
            IP6_TASK_LOCK ();
            CLI_FATAL_ERROR (CliHandle);
            return;
        }
    }
    IP6_TASK_LOCK ();
    return;
}

/*********************************************************************
*  Function Name : Ip6EnableRouteAdvFlags 
*  Description   : Enable Router Adv Flags 
*  Input(s)      : CliHandle
*                  u4Index - Interface Index
*                  i4RouteAdvFlags 
*  Output(s)     :  
*  Return Values : None.
*********************************************************************/

VOID
Ip6EnableRouteAdvFlags (tCliHandle CliHandle,
                        UINT4 u4Index, INT4 i4RouteAdvFlags)
{
    INT4                i4RsVal = SNMP_FAILURE;
    INT1                i1RetVal = SNMP_FAILURE;
    INT4                i4FlagsVal = IPVX_TRUTH_VALUE_FALSE;
    UINT4               u4ErrorCode = 0;

    if (Ip6IsIfFwdEnabled (u4Index) == FALSE)
    {
        if (Ip6ifEntryExists (u4Index) == IP6_FAILURE)
        {
            CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
            return;
        }

        if (nmhTestv2Fsipv6IfRouterAdvFlags
            (&u4ErrorCode, u4Index, i4RouteAdvFlags) == SNMP_FAILURE)
        {
            return;
        }

        if (nmhSetFsipv6IfRouterAdvFlags (u4Index, i4RouteAdvFlags) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return;
        }
        return;
    }

    /* IPv6Lock will be taken in the IPvx Lowlevel So Relase Now and
     * before return take */
    IP6_TASK_UNLOCK ();

    /* Router Adv Config */
    i1RetVal = nmhGetIpv6RouterAdvertRowStatus ((INT4) u4Index, &i4RsVal);
    if (i1RetVal == SNMP_FAILURE)
    {
        /* Create a New Entry in the RA Table */
        i4RsVal = CREATE_AND_WAIT;
        if ((nmhTestv2Ipv6RouterAdvertRowStatus (&u4ErrorCode,
                                                 (INT4) u4Index, i4RsVal))
            == SNMP_FAILURE)
        {
            IP6_TASK_LOCK ();
            return;
        }
        if ((nmhSetIpv6RouterAdvertRowStatus ((INT4) u4Index, i4RsVal))
            == SNMP_FAILURE)
        {
            IP6_TASK_LOCK ();
            CLI_FATAL_ERROR (CliHandle);
            return;
        }
    }

    /* Configure O Flag */
    if ((i4RouteAdvFlags == IP6_ND_RA_NO_O_BIT) ||
        (i4RouteAdvFlags == IP6_ND_RA_O_BIT))
    {
        if (i4RouteAdvFlags == IP6_ND_RA_O_BIT)
        {
            i4FlagsVal = IPVX_TRUTH_VALUE_TRUE;
        }

        if ((nmhTestv2Ipv6RouterAdvertOtherConfigFlag
             (&u4ErrorCode, (INT4) u4Index, i4FlagsVal)) == SNMP_FAILURE)
        {
            IP6_TASK_LOCK ();
            return;
        }

        if ((nmhSetIpv6RouterAdvertOtherConfigFlag ((INT4) u4Index,
                                                    i4FlagsVal)) ==
            SNMP_FAILURE)
        {
            IP6_TASK_LOCK ();
            CLI_FATAL_ERROR (CliHandle);
            return;
        }
    }
    /* Configure M Flag */
    else if ((i4RouteAdvFlags == IP6_ND_RA_NO_M_BIT) ||
             (i4RouteAdvFlags == IP6_ND_RA_M_BIT))
    {
        if (i4RouteAdvFlags == IP6_ND_RA_M_BIT)
        {
            i4FlagsVal = IPVX_TRUTH_VALUE_TRUE;
        }

        if ((nmhTestv2Ipv6RouterAdvertManagedFlag
             (&u4ErrorCode, (INT4) u4Index, i4FlagsVal)) == SNMP_FAILURE)
        {
            IP6_TASK_LOCK ();
            return;
        }

        if ((nmhSetIpv6RouterAdvertManagedFlag ((INT4) u4Index,
                                                i4FlagsVal)) == SNMP_FAILURE)
        {
            IP6_TASK_LOCK ();
            CLI_FATAL_ERROR (CliHandle);
            return;
        }
    }
    IP6_TASK_LOCK ();
    return;
}

/*********************************************************************
* + * *  Function Name : Ip6GetRADNSStatus
* + * *  Description   : To Get DNS Status Value
* + * *  Input(s)      : i4IfIndex - Interface Index
* + * *                  pi4RADNSStateActive
* + * *  Output(s)     :
* + * *  Return Values : None.
* + * ****************************************************************/

INT1
Ip6GetRADNSStatus (INT4 i4IfIndex, INT4 *pi4RADNSStateActive)
{
#ifndef LNXIP6_WANTED
    UNUSED_PARAM (pi4RADNSStateActive);
    UNUSED_PARAM (i4IfIndex);
    return CLI_SUCCESS;
#else
    tLip6If            *pIf6Entry = NULL;
    IP6_TASK_UNLOCK ();
    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4IfIndex);

    if (pIf6Entry == NULL)
    {
        return CLI_FAILURE;
    }
    *pi4RADNSStateActive = (INT4) pIf6Entry->u1RADNSStateActive;
    IP6_TASK_LOCK ();
    return CLI_SUCCESS;
#endif
}

/*********************************************************************
* +*  Function Name : Ip6RaAdvtInterval
* +*  Description   : Enable Router Adv Flags
* +*  Input(s)      : CliHandle
* +*                  u4Index - Interface Index
* +*                  i4RouteAdvFlags
* +*  Output(s)     :
* +*  Return Values : None.
* +*********************************************************************/

VOID
Ip6RaAdvtInterval (tCliHandle CliHandle, UINT4 u4Index, INT4 i4AdvInterval)
{
    UINT4               u4ErrorCode = 0;

#ifndef LNXIP6_WANTED
    if (Ip6ifEntryExists (u4Index) == IP6_FAILURE)
#else
    if (Lip6UtlIfEntryExists (u4Index) == OSIX_FAILURE)
#endif
    {
        CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
        return;
    }

    if (nmhTestv2Fsipv6IfAdvIntOpt
        (&u4ErrorCode, (INT4) u4Index, i4AdvInterval) == SNMP_FAILURE)
    {
        return;
    }

    if (nmhSetFsipv6IfAdvIntOpt ((INT4) u4Index, i4AdvInterval) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return;
    }

    return;
}

/*********************************************************************
*  Function Name : Ip6RaAdvLinkLocal
*  Description   : Enable Router Adv Flags
*  Input(s)      : CliHandle
*                  u4Index - Interface Index
*                  i4RouteAdvFlags
*  Output(s)     :
*  Return Values : None.
*********************************************************************/

VOID
Ip6RaAdvLinkLocal (tCliHandle CliHandle, UINT4 u4Index, INT4 i4AdvtLinkLocal)
{
    UINT4               u4ErrorCode = 0;

#ifndef LNXIP6_WANTED
    if (Ip6ifEntryExists (u4Index) == IP6_FAILURE)
#else
    if (Lip6UtlIfEntryExists (u4Index) == OSIX_FAILURE)
#endif
    {
        CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
        return;
    }

    if (nmhTestv2Fsipv6IfAdvSrcLLAdr
        (&u4ErrorCode, (INT4) u4Index, i4AdvtLinkLocal) == SNMP_FAILURE)
    {
        return;
    }

    if (nmhSetFsipv6IfAdvSrcLLAdr ((INT4) u4Index, i4AdvtLinkLocal) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return;
    }

    return;
}

/*********************************************************************
*  Function Name : Ip6HopLimit 
*  Description   : Setting HopLimit 
*  Input(s)      : CliHandle
*                  u4Index - Interface Index
*                  i4HopLimit 
*  Output(s)     :  
*  Return Values : None.
*********************************************************************/

VOID
Ip6HopLimit (tCliHandle CliHandle, UINT4 u4Index, INT4 i4HopLimit)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4RsVal = SNMP_FAILURE;
    INT1                i1RetVal = SNMP_FAILURE;

    if (Ip6IsIfFwdEnabled (u4Index) == FALSE)
    {
        if (Ip6ifEntryExists (u4Index) == IP6_FAILURE)
        {
            CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
            return;
        }

        if (nmhTestv2Fsipv6IfHopLimit
            (&u4ErrorCode, u4Index, i4HopLimit) == SNMP_FAILURE)
        {
            return;
        }

        if (nmhSetFsipv6IfHopLimit (u4Index, i4HopLimit) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return;
        }
        return;
    }

    /* IPv6Lock will be taken in the IPvx Lowlevel So Relase Now and
     * before return take */
    IP6_TASK_UNLOCK ();

    /* Router Adv Config */
    i1RetVal = nmhGetIpv6RouterAdvertRowStatus ((INT4) u4Index, &i4RsVal);
    if (i1RetVal == SNMP_FAILURE)
    {
        /* Create a New Entry in the RA Table */
        i4RsVal = CREATE_AND_WAIT;
        if ((nmhTestv2Ipv6RouterAdvertRowStatus (&u4ErrorCode,
                                                 (INT4) u4Index, i4RsVal))
            == SNMP_FAILURE)
        {
            IP6_TASK_LOCK ();
            return;
        }
        if ((nmhSetIpv6RouterAdvertRowStatus ((INT4) u4Index, i4RsVal))
            == SNMP_FAILURE)
        {
            IP6_TASK_LOCK ();
            CLI_FATAL_ERROR (CliHandle);
            return;
        }
    }

    if ((nmhTestv2Ipv6RouterAdvertCurHopLimit
         (&u4ErrorCode, (INT4) u4Index, (UINT4) i4HopLimit)) == SNMP_FAILURE)
    {
        IP6_TASK_LOCK ();
        return;
    }

    if ((nmhSetIpv6RouterAdvertCurHopLimit ((INT4) u4Index,
                                            (UINT4) i4HopLimit)) ==
        SNMP_FAILURE)
    {
        IP6_TASK_LOCK ();
        CLI_FATAL_ERROR (CliHandle);
        return;
    }
    IP6_TASK_LOCK ();
    return;
}

/*********************************************************************
*  Function Name : Ip6DefRouterLifeTime 
*  Description   : Setting Default Router Life Time
*  Input(s)      : CliHandle
*                  u4Index - Interface Index
*                  u4DefRouterTime - Router lifetime in seconds 
*  Output(s)     :  
*  Return Values : None.
*********************************************************************/

VOID
Ip6DefRouterLifeTime (tCliHandle CliHandle,
                      UINT4 u4Index, UINT4 u4DefRouterTime)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4RsVal = SNMP_FAILURE;
    INT1                i1RetVal = SNMP_FAILURE;

    if (Ip6IsIfFwdEnabled (u4Index) == FALSE)
    {
        if (Ip6ifEntryExists (u4Index) == IP6_FAILURE)
        {
            CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
            return;
        }

        if (nmhTestv2Fsipv6IfDefRouterTime
            (&u4ErrorCode, u4Index, u4DefRouterTime) == SNMP_FAILURE)
        {
            return;
        }

        if (nmhSetFsipv6IfDefRouterTime (u4Index, u4DefRouterTime) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return;
        }
        return;
    }

    /* IPv6Lock will be taken in the IPvx Lowlevel So Relase Now and
     * before return take */
    IP6_TASK_UNLOCK ();

    /* Router Adv Config */
    i1RetVal = nmhGetIpv6RouterAdvertRowStatus ((INT4) u4Index, &i4RsVal);
    if (i1RetVal == SNMP_FAILURE)
    {
        /* Create a New Entry in the RA Table */
        i4RsVal = CREATE_AND_WAIT;
        if ((nmhTestv2Ipv6RouterAdvertRowStatus (&u4ErrorCode,
                                                 (INT4) u4Index, i4RsVal))
            == SNMP_FAILURE)
        {
            IP6_TASK_LOCK ();
            return;
        }
        if ((nmhSetIpv6RouterAdvertRowStatus ((INT4) u4Index, i4RsVal))
            == SNMP_FAILURE)
        {
            IP6_TASK_LOCK ();
            CLI_FATAL_ERROR (CliHandle);
            return;
        }
    }

    if ((nmhTestv2Ipv6RouterAdvertDefaultLifetime
         (&u4ErrorCode, (INT4) u4Index, u4DefRouterTime)) == SNMP_FAILURE)
    {
        IP6_TASK_LOCK ();
        return;
    }

    if ((nmhSetIpv6RouterAdvertDefaultLifetime ((INT4) u4Index,
                                                u4DefRouterTime))
        == SNMP_FAILURE)
    {
        IP6_TASK_LOCK ();
        CLI_FATAL_ERROR (CliHandle);
        return;
    }

    IP6_TASK_LOCK ();
    return;
}

/*********************************************************************
*  Function Name : Ip6DADRetries
*  Description   : Set DAD retry count value
*  Input(s)      : CliHandle
*                  u4Index -Interface Index
*                  i4DADRetries - value to be set
*  Output(s)     :
*  Return Values : None.
*********************************************************************/

VOID
Ip6DADRetries (tCliHandle CliHandle, UINT4 u4Index, INT4 i4DADRetries)
{
    UINT4               u4ErrorCode = 0;

#ifndef LNXIP6_WANTED
    if (Ip6ifEntryExists (u4Index) == IP6_FAILURE)
#else
    if (Lip6UtlIfEntryExists (u4Index) == OSIX_FAILURE)
#endif
    {
        CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
        return;
    }

    if (nmhTestv2Fsipv6IfDADRetries
        (&u4ErrorCode, u4Index, i4DADRetries) == SNMP_FAILURE)
    {
        return;
    }

    if (nmhSetFsipv6IfDADRetries (u4Index, i4DADRetries) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return;
    }

    return;
}

/*********************************************************************
 *  Function Name : Ip6IfTrafficShowInCxt
 *  Description   : If  Statistics for Ipv4
 *  Input(s)      : CliHandle, IfIndex, HCFlag
 *  Output(s)     :  
 *  Return Values : CLI_FAILURE, CLI_SUCCESS.
 *********************************************************************/

INT4
Ip6IfTrafficShowInCxt (tCliHandle CliHandle, UINT4 u4IfIndex, UINT1 u1HCFlag,
                       UINT4 u4VcId)
{
    UINT1               au1Alias[CFA_CLI_MAX_IF_NAME_LEN];
    UINT1              *pu1IfaceInfo = NULL;
    INT4                i4IpIfStatsIPVersion = INET_ADDR_TYPE_IPV6;
    UINT4               u4ContextId = VCM_INVALID_VC;
    UINT4               u4ShowAllCxt = FALSE;
    UINT4               u4IpIfStatsInHdrErrors = IP_ZERO;
    UINT4               u4IpIfStatsInNoRoutes = IP_ZERO;
    UINT4               u4IpIfStatsInAddrErrors = IP_ZERO;
    UINT4               u4IpIfStatsInUnknownProtos = IP_ZERO;
    UINT4               u4IpIfStatsInTruncatedPkts = IP_ZERO;
    UINT4               u4IpIfStatsReasmReqds = IP_ZERO;
    UINT4               u4IpIfStatsReasmOKs = IP_ZERO;
    UINT4               u4IpIfStatsReasmFails = IP_ZERO;
    UINT4               u4IpIfStatsInDiscards = IP_ZERO;
    UINT4               u4IpIfStatsOutDiscards = IP_ZERO;
    UINT4               u4IpIfStatsOutFragReqds = IP_ZERO;
    UINT4               u4IpIfStatsOutFragOKs = IP_ZERO;
    UINT4               u4IpIfStatsOutFragFails = IP_ZERO;
    UINT4               u4IpIfStatsOutFragCreates = IP_ZERO;
    UINT4               u4IpIfStatsDiscontinuityTime = IP_ZERO;
    UINT4               u4IpIfStatsRefreshRate = IP_ZERO;
    /*Icmpv6 interface specific counters */
    UINT4               u4Ipv6IfIcmpInMsgs = IP6_ZERO;
    UINT4               u4Ipv6IfIcmpInErrors = IP6_ZERO;
    UINT4               u4Ipv6IfIcmpInDestUnreachs = IP6_ZERO;
    UINT4               u4Ipv6IfIcmpInAdminProhibs = IP6_ZERO;
    UINT4               u4Ipv6IfIcmpInTimeExcds = IP6_ZERO;
    UINT4               u4Ipv6IfIcmpInParmProblems = IP6_ZERO;
    UINT4               u4Ipv6IfIcmpInPktTooBigs = IP6_ZERO;
    UINT4               u4Ipv6IfIcmpInEchos = IP6_ZERO;
    UINT4               u4Ipv6IfIcmpInEchoReplies = IP6_ZERO;
    UINT4               u4Ipv6IfIcmpInRouterSolicits = IP6_ZERO;
    UINT4               u4Ipv6IfIcmpInRouterAdvertisements = IP6_ZERO;
    UINT4               u4Ipv6IfIcmpInNeighborSolicits = IP6_ZERO;
    UINT4               u4Ipv6IfIcmpInNeighborAdvertisements = IP6_ZERO;
    UINT4               u4Ipv6IfIcmpInRedirects = IP6_ZERO;
    UINT4               u4Ipv6IfIcmpInGroupMembQueries = IP6_ZERO;
    UINT4               u4Ipv6IfIcmpInGroupMembResponses = IP6_ZERO;
    UINT4               u4Ipv6IfIcmpInGroupMembReductions = IP6_ZERO;
    UINT4               u4Ipv6IfIcmpOutMsgs = IP6_ZERO;
    UINT4               u4Ipv6IfIcmpOutErrors = IP6_ZERO;
    UINT4               u4Ipv6IfIcmpOutDestUnreachs = IP6_ZERO;
    UINT4               u4Ipv6IfIcmpOutAdminProhibs = IP6_ZERO;
    UINT4               u4Ipv6IfIcmpOutTimeExcds = IP6_ZERO;
    UINT4               u4Ipv6IfIcmpOutParmProblems = IP6_ZERO;
    UINT4               u4Ipv6IfIcmpOutPktTooBigs = IP6_ZERO;
    UINT4               u4Ipv6IfIcmpOutEchos = IP6_ZERO;
    UINT4               u4Ipv6IfIcmpOutEchoReplies = IP6_ZERO;
    UINT4               u4Ipv6IfIcmpOutRouterSolicits = IP6_ZERO;
    UINT4               u4Ipv6IfIcmpOutRouterAdvertisements = IP6_ZERO;
    UINT4               u4Ipv6IfIcmpOutNeighborSolicits = IP6_ZERO;
    UINT4               u4Ipv6IfIcmpOutNeighborAdvertisements = IP6_ZERO;
    UINT4               u4Ipv6IfIcmpOutRedirects = IP6_ZERO;
    UINT4               u4Ipv6IfIcmpOutGroupMembQueries = IP6_ZERO;
    UINT4               u4Ipv6IfIcmpOutGroupMembResponses = IP6_ZERO;
    UINT4               u4Ipv6IfIcmpOutGroupMembReductions = IP6_ZERO;
    UINT4               u4IpIfStatsSeNDInvalidPackets = IP6_ZERO;
    UINT4               u4IpIfStatsSeNDDroppedPackets = IP6_ZERO;
    /*Icmpv6 interface specific counters */
    tSNMP_COUNTER64_TYPE RetValIpIfStatsHCInReceives;
    tSNMP_COUNTER64_TYPE RetValIpIfStatsHCInOctets;
    tSNMP_COUNTER64_TYPE RetValIpIfStatsHCInForwDatagrams;
    tSNMP_COUNTER64_TYPE RetValIpIfStatsHCInDelivers;
    tSNMP_COUNTER64_TYPE RetValIpIfStatsHCOutRequests;
    tSNMP_COUNTER64_TYPE RetValIpIfStatsHCOutForwDatagrams;
    tSNMP_COUNTER64_TYPE RetValIpIfStatsHCOutTransmits;
    tSNMP_COUNTER64_TYPE RetValIpIfStatsHCOutOctets;
    tSNMP_COUNTER64_TYPE RetValIpIfStatsHCInMcastPkts;
    tSNMP_COUNTER64_TYPE RetValIpIfStatsHCInMcastOctets;
    tSNMP_COUNTER64_TYPE RetValIpIfStatsHCOutMcastPkts;
    tSNMP_COUNTER64_TYPE RetValIpIfStatsHCOutMcastOctets;
    tSNMP_COUNTER64_TYPE RetValIpIfStatsHCInBcastPkts;
    tSNMP_COUNTER64_TYPE RetValIpIfStatsHCOutBcastPkts;

    FS_UINT8            u8IpIfStatsHCInReceives;
    FS_UINT8            u8IpIfStatsHCInOctets;
    FS_UINT8            u8IpIfStatsHCInForwDatagrams;
    FS_UINT8            u8IpIfStatsHCInDelivers;
    FS_UINT8            u8IpIfStatsHCOutRequests;
    FS_UINT8            u8IpIfStatsHCOutForwDatagrams;
    FS_UINT8            u8IpIfStatsHCOutTransmits;
    FS_UINT8            u8IpIfStatsHCOutOctets;
    FS_UINT8            u8IpIfStatsHCInMcastPkts;
    FS_UINT8            u8IpIfStatsHCInMcastOctets;
    FS_UINT8            u8IpIfStatsHCOutMcastPkts;
    FS_UINT8            u8IpIfStatsHCOutMcastOctets;
    FS_UINT8            u8IpIfStatsHCInBcastPkts;
    FS_UINT8            u8IpIfStatsHCOutBcastPkts;
    UINT1               au1HCTempCount[CFA_CLI_U8_STR_LENGTH];
    UINT1               au1HCTempCnt[CFA_CLI_U8_STR_LENGTH];

    MEMSET (&au1Alias, IP_ZERO, CFA_CLI_MAX_IF_NAME_LEN);
    pu1IfaceInfo = &au1Alias[IP_ZERO];

    MEMSET (au1HCTempCount, IP_ZERO, CFA_CLI_U8_STR_LENGTH);
    MEMSET (au1HCTempCnt, IP_ZERO, CFA_CLI_U8_STR_LENGTH);

    MEMSET (&RetValIpIfStatsHCInReceives, IP_ZERO,
            sizeof (tSNMP_COUNTER64_TYPE));
    MEMSET (&RetValIpIfStatsHCInOctets, IP_ZERO, sizeof (tSNMP_COUNTER64_TYPE));
    MEMSET (&RetValIpIfStatsHCInForwDatagrams, IP_ZERO,
            sizeof (tSNMP_COUNTER64_TYPE));
    MEMSET (&RetValIpIfStatsHCInDelivers, IP_ZERO,
            sizeof (tSNMP_COUNTER64_TYPE));
    MEMSET (&RetValIpIfStatsHCOutRequests, IP_ZERO,
            sizeof (tSNMP_COUNTER64_TYPE));
    MEMSET (&RetValIpIfStatsHCOutForwDatagrams, IP_ZERO,
            sizeof (tSNMP_COUNTER64_TYPE));
    MEMSET (&RetValIpIfStatsHCOutTransmits, IP_ZERO,
            sizeof (tSNMP_COUNTER64_TYPE));
    MEMSET (&RetValIpIfStatsHCOutOctets, IP_ZERO,
            sizeof (tSNMP_COUNTER64_TYPE));
    MEMSET (&RetValIpIfStatsHCInMcastPkts, IP_ZERO,
            sizeof (tSNMP_COUNTER64_TYPE));
    MEMSET (&RetValIpIfStatsHCOutMcastPkts, IP_ZERO,
            sizeof (tSNMP_COUNTER64_TYPE));
    MEMSET (&RetValIpIfStatsHCOutMcastOctets, IP_ZERO,
            sizeof (tSNMP_COUNTER64_TYPE));
    MEMSET (&RetValIpIfStatsHCInBcastPkts, IP_ZERO,
            sizeof (tSNMP_COUNTER64_TYPE));
    MEMSET (&RetValIpIfStatsHCOutBcastPkts, IP_ZERO,
            sizeof (tSNMP_COUNTER64_TYPE));
    MEMSET (&RetValIpIfStatsHCInMcastOctets, IP_ZERO,
            sizeof (tSNMP_COUNTER64_TYPE));

    FSAP_U8_CLR (&u8IpIfStatsHCInReceives);
    FSAP_U8_CLR (&u8IpIfStatsHCInOctets);
    FSAP_U8_CLR (&u8IpIfStatsHCInForwDatagrams);
    FSAP_U8_CLR (&u8IpIfStatsHCInDelivers);
    FSAP_U8_CLR (&u8IpIfStatsHCOutRequests);
    FSAP_U8_CLR (&u8IpIfStatsHCOutForwDatagrams);
    FSAP_U8_CLR (&u8IpIfStatsHCOutTransmits);
    FSAP_U8_CLR (&u8IpIfStatsHCOutOctets);
    FSAP_U8_CLR (&u8IpIfStatsHCInMcastPkts);
    FSAP_U8_CLR (&u8IpIfStatsHCOutMcastPkts);
    FSAP_U8_CLR (&u8IpIfStatsHCOutMcastOctets);
    FSAP_U8_CLR (&u8IpIfStatsHCInBcastPkts);
    FSAP_U8_CLR (&u8IpIfStatsHCOutBcastPkts);
    FSAP_U8_CLR (&u8IpIfStatsHCInMcastOctets);

    if (u4VcId == VCM_INVALID_VC)
    {
        u4ShowAllCxt = TRUE;

    }
    else
    {
        if (VcmIsL3VcExist (u4VcId) == VCM_FALSE)
        {
            CliPrintf (CliHandle, "\r%% Invalid VRF \r\n");
            return CLI_FAILURE;
        }
    }
    if (u4ShowAllCxt == FALSE)
    {
        if (nmhGetFsMIStdIpIfStatsContextId (i4IpIfStatsIPVersion, u4IfIndex,
                                             (INT4 *) &u4ContextId) !=
            SNMP_FAILURE)
        {
            if (u4ContextId != u4VcId)
            {
                CliPrintf (CliHandle,
                           "\rThis interface is not mapped to the specified VRF\r\n");
                return CLI_SUCCESS;
            }
        }
    }
    /* Getting the IP Related Information */

    (VOID) nmhGetFsMIStdIpIfStatsHCInReceives (i4IpIfStatsIPVersion, u4IfIndex,
                                               &RetValIpIfStatsHCInReceives);
    (VOID) nmhGetFsMIStdIpIfStatsHCInOctets (i4IpIfStatsIPVersion, u4IfIndex,
                                             &RetValIpIfStatsHCInOctets);
    (VOID) nmhGetFsMIStdIpIfStatsInHdrErrors (i4IpIfStatsIPVersion, u4IfIndex,
                                              &u4IpIfStatsInHdrErrors);
    (VOID) nmhGetFsMIStdIpIfStatsInNoRoutes (i4IpIfStatsIPVersion, u4IfIndex,
                                             &u4IpIfStatsInNoRoutes);
    (VOID) nmhGetFsMIStdIpIfStatsInAddrErrors (i4IpIfStatsIPVersion, u4IfIndex,
                                               &u4IpIfStatsInAddrErrors);
    (VOID) nmhGetFsMIStdIpIfStatsInUnknownProtos (i4IpIfStatsIPVersion,
                                                  u4IfIndex,
                                                  &u4IpIfStatsInUnknownProtos);
    (VOID) nmhGetFsMIStdIpIfStatsInTruncatedPkts (i4IpIfStatsIPVersion,
                                                  u4IfIndex,
                                                  &u4IpIfStatsInTruncatedPkts);
    (VOID) nmhGetFsMIStdIpIfStatsHCInForwDatagrams (i4IpIfStatsIPVersion,
                                                    u4IfIndex,
                                                    &RetValIpIfStatsHCInForwDatagrams);
    (VOID) nmhGetFsMIStdIpIfStatsReasmReqds (i4IpIfStatsIPVersion, u4IfIndex,
                                             &u4IpIfStatsReasmReqds);
    (VOID) nmhGetFsMIStdIpIfStatsReasmOKs (i4IpIfStatsIPVersion, u4IfIndex,
                                           &u4IpIfStatsReasmOKs);
    (VOID) nmhGetFsMIStdIpIfStatsReasmFails (i4IpIfStatsIPVersion, u4IfIndex,
                                             &u4IpIfStatsReasmFails);
    (VOID) nmhGetFsMIStdIpIfStatsInDiscards (i4IpIfStatsIPVersion, u4IfIndex,
                                             &u4IpIfStatsInDiscards);
    (VOID) nmhGetFsMIStdIpIfStatsHCInDelivers (i4IpIfStatsIPVersion, u4IfIndex,
                                               &RetValIpIfStatsHCInDelivers);
    (VOID) nmhGetFsMIStdIpIfStatsHCOutRequests (i4IpIfStatsIPVersion, u4IfIndex,
                                                &RetValIpIfStatsHCOutRequests);
    (VOID) nmhGetFsMIStdIpIfStatsHCOutForwDatagrams (i4IpIfStatsIPVersion,
                                                     u4IfIndex,
                                                     &RetValIpIfStatsHCOutForwDatagrams);
    (VOID) nmhGetFsMIStdIpIfStatsOutDiscards (i4IpIfStatsIPVersion, u4IfIndex,
                                              &u4IpIfStatsOutDiscards);
    (VOID) nmhGetFsMIStdIpIfStatsOutFragReqds (i4IpIfStatsIPVersion, u4IfIndex,
                                               &u4IpIfStatsOutFragReqds);
    (VOID) nmhGetFsMIStdIpIfStatsOutFragOKs (i4IpIfStatsIPVersion, u4IfIndex,
                                             &u4IpIfStatsOutFragOKs);
    (VOID) nmhGetFsMIStdIpIfStatsOutFragFails (i4IpIfStatsIPVersion, u4IfIndex,
                                               &u4IpIfStatsOutFragFails);
    (VOID) nmhGetFsMIStdIpIfStatsOutFragCreates (i4IpIfStatsIPVersion,
                                                 u4IfIndex,
                                                 &u4IpIfStatsOutFragCreates);
    (VOID) nmhGetFsMIStdIpIfStatsHCOutTransmits (i4IpIfStatsIPVersion,
                                                 u4IfIndex,
                                                 &RetValIpIfStatsHCOutTransmits);
    (VOID) nmhGetFsMIStdIpIfStatsHCOutOctets (i4IpIfStatsIPVersion, u4IfIndex,
                                              &RetValIpIfStatsHCOutOctets);
    (VOID) nmhGetFsMIStdIpIfStatsHCInMcastPkts (i4IpIfStatsIPVersion, u4IfIndex,
                                                &RetValIpIfStatsHCInMcastPkts);
    (VOID) nmhGetFsMIStdIpIfStatsHCInMcastOctets (i4IpIfStatsIPVersion,
                                                  u4IfIndex,
                                                  &RetValIpIfStatsHCInMcastOctets);
    (VOID) nmhGetFsMIStdIpIfStatsHCOutMcastPkts (i4IpIfStatsIPVersion,
                                                 u4IfIndex,
                                                 &RetValIpIfStatsHCOutMcastPkts);
    (VOID) nmhGetFsMIStdIpIfStatsHCOutMcastOctets (i4IpIfStatsIPVersion,
                                                   u4IfIndex,
                                                   &RetValIpIfStatsHCOutMcastOctets);
    (VOID) nmhGetFsMIStdIpIfStatsHCInBcastPkts (i4IpIfStatsIPVersion, u4IfIndex,
                                                &RetValIpIfStatsHCInBcastPkts);
    (VOID) nmhGetFsMIStdIpIfStatsHCOutBcastPkts (i4IpIfStatsIPVersion,
                                                 u4IfIndex,
                                                 &RetValIpIfStatsHCOutBcastPkts);
    (VOID) nmhGetFsMIStdIpIfStatsDiscontinuityTime (i4IpIfStatsIPVersion,
                                                    u4IfIndex,
                                                    &u4IpIfStatsDiscontinuityTime);
    (VOID) nmhGetFsMIStdIpIfStatsRefreshRate (i4IpIfStatsIPVersion, u4IfIndex,
                                              &u4IpIfStatsRefreshRate);
    (VOID) nmhGetFsMIIpv6IfStatsSENDDroppedPkts (u4IfIndex,
                                                 &u4IpIfStatsSeNDDroppedPackets);
    (VOID) nmhGetFsMIIpv6IfStatsSENDInvalidPkts (u4IfIndex,
                                                 &u4IpIfStatsSeNDInvalidPackets);
/*Icmp6 interface specific statistics*/

    nmhGetFsIpv6IfIcmpInMsgs ((INT4) u4IfIndex, &u4Ipv6IfIcmpInMsgs);
    nmhGetFsIpv6IfIcmpInErrors ((INT4) u4IfIndex, &u4Ipv6IfIcmpInErrors);
    nmhGetFsIpv6IfIcmpInDestUnreachs ((INT4) u4IfIndex,
                                      &u4Ipv6IfIcmpInDestUnreachs);

    nmhGetFsIpv6IfIcmpInAdminProhibs ((INT4) u4IfIndex,
                                      &u4Ipv6IfIcmpInAdminProhibs);
    nmhGetFsIpv6IfIcmpInTimeExcds ((INT4) u4IfIndex, &u4Ipv6IfIcmpInTimeExcds);
    nmhGetFsIpv6IfIcmpInParmProblems ((INT4) u4IfIndex,
                                      &u4Ipv6IfIcmpInParmProblems);

    nmhGetFsIpv6IfIcmpInPktTooBigs ((INT4) u4IfIndex,
                                    &u4Ipv6IfIcmpInPktTooBigs);
    nmhGetFsIpv6IfIcmpInEchos ((INT4) u4IfIndex, &u4Ipv6IfIcmpInEchos);
    nmhGetFsIpv6IfIcmpInEchoReplies ((INT4) u4IfIndex,
                                     &u4Ipv6IfIcmpInEchoReplies);

    nmhGetFsIpv6IfIcmpInRouterSolicits ((INT4) u4IfIndex,
                                        &u4Ipv6IfIcmpInRouterSolicits);
    nmhGetFsIpv6IfIcmpInRouterAdvertisements ((INT4) u4IfIndex,
                                              &u4Ipv6IfIcmpInRouterAdvertisements);
    nmhGetFsIpv6IfIcmpInNeighborSolicits ((INT4) u4IfIndex,
                                          &u4Ipv6IfIcmpInNeighborSolicits);

    nmhGetFsIpv6IfIcmpInNeighborAdvertisements ((INT4) u4IfIndex,
                                                &u4Ipv6IfIcmpInNeighborAdvertisements);
    nmhGetFsIpv6IfIcmpInRedirects ((INT4) u4IfIndex, &u4Ipv6IfIcmpInRedirects);
    nmhGetFsIpv6IfIcmpInGroupMembQueries ((INT4) u4IfIndex,
                                          &u4Ipv6IfIcmpInGroupMembQueries);

    nmhGetFsIpv6IfIcmpInGroupMembResponses ((INT4) u4IfIndex,
                                            &u4Ipv6IfIcmpInGroupMembResponses);
    nmhGetFsIpv6IfIcmpInGroupMembReductions ((INT4) u4IfIndex,
                                             &u4Ipv6IfIcmpInGroupMembReductions);
    nmhGetFsIpv6IfIcmpOutMsgs ((INT4) u4IfIndex, &u4Ipv6IfIcmpOutMsgs);

    nmhGetFsIpv6IfIcmpOutErrors ((INT4) u4IfIndex, &u4Ipv6IfIcmpOutErrors);
    nmhGetFsIpv6IfIcmpOutDestUnreachs ((INT4) u4IfIndex,
                                       &u4Ipv6IfIcmpOutDestUnreachs);
    nmhGetFsIpv6IfIcmpOutAdminProhibs ((INT4) u4IfIndex,
                                       &u4Ipv6IfIcmpOutAdminProhibs);

    nmhGetFsIpv6IfIcmpOutTimeExcds ((INT4) u4IfIndex,
                                    &u4Ipv6IfIcmpOutTimeExcds);
    nmhGetFsIpv6IfIcmpOutParmProblems ((INT4) u4IfIndex,
                                       &u4Ipv6IfIcmpOutParmProblems);
    nmhGetFsIpv6IfIcmpOutPktTooBigs ((INT4) u4IfIndex,
                                     &u4Ipv6IfIcmpOutPktTooBigs);

    nmhGetFsIpv6IfIcmpOutEchos ((INT4) u4IfIndex, &u4Ipv6IfIcmpOutEchos);
    nmhGetFsIpv6IfIcmpOutEchoReplies ((INT4) u4IfIndex,
                                      &u4Ipv6IfIcmpOutEchoReplies);
    nmhGetFsIpv6IfIcmpOutRouterSolicits ((INT4) u4IfIndex,
                                         &u4Ipv6IfIcmpOutRouterSolicits);

    nmhGetFsIpv6IfIcmpOutRouterAdvertisements ((INT4) u4IfIndex,
                                               &u4Ipv6IfIcmpOutRouterAdvertisements);
    nmhGetFsIpv6IfIcmpOutNeighborSolicits ((INT4) u4IfIndex,
                                           &u4Ipv6IfIcmpOutNeighborSolicits);
    nmhGetFsIpv6IfIcmpOutNeighborAdvertisements ((INT4) u4IfIndex,
                                                 &u4Ipv6IfIcmpOutNeighborAdvertisements);
    nmhGetFsIpv6IfIcmpOutRedirects ((INT4) u4IfIndex,
                                    &u4Ipv6IfIcmpOutRedirects);
    nmhGetFsIpv6IfIcmpOutGroupMembQueries ((INT4) u4IfIndex,
                                           &u4Ipv6IfIcmpOutGroupMembQueries);
    nmhGetFsIpv6IfIcmpOutGroupMembResponses ((INT4) u4IfIndex,
                                             &u4Ipv6IfIcmpOutGroupMembResponses);
    nmhGetFsIpv6IfIcmpOutGroupMembReductions ((INT4) u4IfIndex,
                                              &u4Ipv6IfIcmpOutGroupMembReductions);
    /*Icmp6 interface specific statistics */

    FSAP_U8_ASSIGN_HI (&u8IpIfStatsHCInReceives,
                       RetValIpIfStatsHCInReceives.msn);
    FSAP_U8_ASSIGN_LO (&u8IpIfStatsHCInReceives,
                       RetValIpIfStatsHCInReceives.lsn);
    FSAP_U8_ASSIGN_HI (&u8IpIfStatsHCInOctets, RetValIpIfStatsHCInOctets.msn);
    FSAP_U8_ASSIGN_LO (&u8IpIfStatsHCInOctets, RetValIpIfStatsHCInOctets.lsn);
    FSAP_U8_ASSIGN_HI (&u8IpIfStatsHCInForwDatagrams,
                       RetValIpIfStatsHCInForwDatagrams.msn);
    FSAP_U8_ASSIGN_LO (&u8IpIfStatsHCInForwDatagrams,
                       RetValIpIfStatsHCInForwDatagrams.lsn);
    FSAP_U8_ASSIGN_HI (&u8IpIfStatsHCInDelivers,
                       RetValIpIfStatsHCInDelivers.msn);
    FSAP_U8_ASSIGN_LO (&u8IpIfStatsHCInDelivers,
                       RetValIpIfStatsHCInDelivers.lsn);
    FSAP_U8_ASSIGN_HI (&u8IpIfStatsHCOutRequests,
                       RetValIpIfStatsHCOutRequests.msn);
    FSAP_U8_ASSIGN_LO (&u8IpIfStatsHCOutRequests,
                       RetValIpIfStatsHCOutRequests.lsn);
    FSAP_U8_ASSIGN_HI (&u8IpIfStatsHCOutForwDatagrams,
                       RetValIpIfStatsHCOutForwDatagrams.msn);
    FSAP_U8_ASSIGN_LO (&u8IpIfStatsHCOutForwDatagrams,
                       RetValIpIfStatsHCOutForwDatagrams.lsn);
    FSAP_U8_ASSIGN_HI (&u8IpIfStatsHCOutTransmits,
                       RetValIpIfStatsHCOutTransmits.msn);
    FSAP_U8_ASSIGN_LO (&u8IpIfStatsHCOutTransmits,
                       RetValIpIfStatsHCOutTransmits.lsn);
    FSAP_U8_ASSIGN_HI (&u8IpIfStatsHCOutOctets, RetValIpIfStatsHCOutOctets.msn);
    FSAP_U8_ASSIGN_LO (&u8IpIfStatsHCOutOctets, RetValIpIfStatsHCOutOctets.lsn);
    FSAP_U8_ASSIGN_HI (&u8IpIfStatsHCInMcastPkts,
                       RetValIpIfStatsHCInMcastPkts.msn);
    FSAP_U8_ASSIGN_LO (&u8IpIfStatsHCInMcastPkts,
                       RetValIpIfStatsHCInMcastPkts.lsn);
    FSAP_U8_ASSIGN_HI (&u8IpIfStatsHCOutMcastPkts,
                       RetValIpIfStatsHCOutMcastPkts.msn);
    FSAP_U8_ASSIGN_LO (&u8IpIfStatsHCOutMcastPkts,
                       RetValIpIfStatsHCOutMcastPkts.lsn);
    FSAP_U8_ASSIGN_HI (&u8IpIfStatsHCOutMcastOctets,
                       RetValIpIfStatsHCOutMcastOctets.msn);
    FSAP_U8_ASSIGN_HI (&u8IpIfStatsHCInBcastPkts,
                       RetValIpIfStatsHCInBcastPkts.msn);
    FSAP_U8_ASSIGN_LO (&u8IpIfStatsHCInBcastPkts,
                       RetValIpIfStatsHCInBcastPkts.lsn);
    FSAP_U8_ASSIGN_HI (&u8IpIfStatsHCOutBcastPkts,
                       RetValIpIfStatsHCOutBcastPkts.msn);
    FSAP_U8_ASSIGN_LO (&u8IpIfStatsHCOutBcastPkts,
                       RetValIpIfStatsHCOutBcastPkts.lsn);
    FSAP_U8_ASSIGN_HI (&u8IpIfStatsHCInMcastOctets,
                       RetValIpIfStatsHCInMcastOctets.msn);
    FSAP_U8_ASSIGN_LO (&u8IpIfStatsHCInMcastOctets,
                       RetValIpIfStatsHCInMcastOctets.lsn);

    /* After getting all the values just Print it */

    /*Display Ip related Information */
    CfaCliGetIfName (u4IfIndex, (INT1 *) pu1IfaceInfo);
    CliPrintf (CliHandle, "\r\nIPv6 %sStatistics for interface %s\r\n",
               ((u1HCFlag) ? "High Count " : ""), pu1IfaceInfo);
    CliPrintf (CliHandle,
               "----------------------------------------------------\r\n");

    if (u1HCFlag == IP6_CLI_HC_DISABLED)
    {
        CliPrintf (CliHandle,
                   "\r\n%5d\t%-15s\t%5d\t%-15s\t%5d\t%-15s\r\n",
                   RetValIpIfStatsHCInReceives.lsn, "Rcvd",
                   RetValIpIfStatsHCInOctets.lsn, "InOctets",
                   u4IpIfStatsInHdrErrors, "HdrErrors");

        CliPrintf (CliHandle,
                   "%5d\t%-15s\t%5d\t%-15s\t%5d\t%-15s\r\n",
                   u4IpIfStatsInNoRoutes, "InNoRoutes",
                   u4IpIfStatsInAddrErrors, "AddrErrors",
                   u4IpIfStatsInUnknownProtos, "UnknownProtos");

        CliPrintf (CliHandle,
                   "%5d\t%-15s\t%5d\t%-15s\t%5d\t%-15s\r\n",
                   u4IpIfStatsInTruncatedPkts, "TruncatedPkts",
                   RetValIpIfStatsHCInForwDatagrams.lsn, "FwdDatagrms",
                   u4IpIfStatsReasmReqds, "ReasmReqds");

        CliPrintf (CliHandle, "%5d\t%-15s\t%5d\t%-15s\t%5d\t%-15s\r\n",
                   u4IpIfStatsReasmOKs, "ReasmOKs",
                   u4IpIfStatsReasmFails, "ReasmFails",
                   u4IpIfStatsInDiscards, "Discards");

        CliPrintf (CliHandle,
                   "%5d\t%-15s\t%5d\t%-15s\t%5d\t%-15s\r\n",
                   RetValIpIfStatsHCInDelivers.lsn, "Delivers",
                   RetValIpIfStatsHCOutRequests.lsn, "OutRequests",
                   RetValIpIfStatsHCOutForwDatagrams.lsn, "OutFwdDgrms");

        CliPrintf (CliHandle,
                   "%5d\t%-15s\t%5d\t%-15s\t%5d\t%-15s\r\n",
                   u4IpIfStatsOutDiscards, "OutDiscards",
                   u4IpIfStatsOutFragReqds, "FragReqds",
                   u4IpIfStatsOutFragOKs, "FragOKs");

        CliPrintf (CliHandle,
                   "%5d\t%-15s\t%5d\t%-15s\t%5d\t%-15s\r\n",
                   u4IpIfStatsOutFragFails, "FragFails",
                   u4IpIfStatsOutFragCreates, "FragCreates",
                   RetValIpIfStatsHCOutTransmits.lsn, "OutTrnsmits");

        CliPrintf (CliHandle,
                   "%5d\t%-15s\t%5d\t%-15s\t%5d\t%-15s\r\n",
                   RetValIpIfStatsHCOutOctets.lsn, "OutOctets",
                   RetValIpIfStatsHCInMcastPkts.lsn, "InMcstPkts",
                   RetValIpIfStatsHCInMcastPkts.lsn, "InMcstOctets");

        CliPrintf (CliHandle,
                   "%5d\t%-15s\t%5d\t%-15s\t%5d\t%-15s\r\n",
                   RetValIpIfStatsHCOutMcastPkts.lsn, "OutMcstPkts",
                   RetValIpIfStatsHCOutMcastOctets.lsn, "OutMcstOctets",
                   RetValIpIfStatsHCInBcastPkts.lsn, "InBcstPkts");

        CliPrintf (CliHandle,
                   "%5d\t%-15s\t%5d\t%-15s\t%5d\t%-15s\r\n",
                   RetValIpIfStatsHCOutBcastPkts.lsn, "OutBcstPkts",
                   u4IpIfStatsDiscontinuityTime, "DiscntTime",
                   u4IpIfStatsRefreshRate, "RefrshRate");
        CliPrintf (CliHandle,
                   "%5d\t%-15s\t%5d\t%-15s\t\r\n",
                   u4IpIfStatsSeNDInvalidPackets, "SeNDInvalPkts",
                   u4IpIfStatsSeNDDroppedPackets, "SeNDDrpPkts");
        CliPrintf (CliHandle, "\r\nICMPv6 %sStatistics for interface %s\r\n",
                   "", pu1IfaceInfo);
        CliPrintf (CliHandle, "-------------------------------------\r\n");
        CliPrintf (CliHandle, "-------------------------------------\r\n");
        CliPrintf (CliHandle, "    Received    \r\n");
        CliPrintf (CliHandle, "-------------------------------------\r\n");
        CliPrintf (CliHandle,
                   "%5d\t%-15s\t%5d\t%-15s\r\n",
                   u4Ipv6IfIcmpInMsgs, "InMsgs",
                   u4Ipv6IfIcmpInErrors, "InErrs");
        CliPrintf (CliHandle,
                   "%5d\t%-15s\t%5d\t%-15s\r\n",
                   u4Ipv6IfIcmpInDestUnreachs, "InDstUnreach",
                   u4Ipv6IfIcmpInAdminProhibs, "InAdminProhib");
        CliPrintf (CliHandle,
                   "%5d\t%-15s\t%5d\t%-15s\r\n",
                   u4Ipv6IfIcmpInTimeExcds, "InTmexceeded",
                   u4Ipv6IfIcmpInParmProblems, "InParamprob");
        CliPrintf (CliHandle,
                   "%5d\t%-15s\t%5d\t%-15s\r\n",
                   u4Ipv6IfIcmpInPktTooBigs, "InPktToobig",
                   u4Ipv6IfIcmpInEchos, "InEchoReq");
        CliPrintf (CliHandle,
                   "%5d\t%-15s\t%5d\t%-15s\r\n",
                   u4Ipv6IfIcmpInEchoReplies, "InEchoResp",
                   u4Ipv6IfIcmpInRouterSolicits, "InRouterSol");
        CliPrintf (CliHandle,
                   "%5d\t%-15s\t%5d\t%-15s\r\n",
                   u4Ipv6IfIcmpInRouterAdvertisements, "InRouterAdv",
                   u4Ipv6IfIcmpInNeighborSolicits, "InNeighSol");
        CliPrintf (CliHandle,
                   "%5d\t%-15s\t%5d\t%-15s\r\n",
                   u4Ipv6IfIcmpInNeighborAdvertisements, "InNeighAdv",
                   u4Ipv6IfIcmpInRedirects, "InRedir");
        CliPrintf (CliHandle,
                   "%5d\t%-15s\t%5d\t%-15s\r\n",
                   u4Ipv6IfIcmpInGroupMembQueries, "InMLDQuery",
                   u4Ipv6IfIcmpInGroupMembResponses, "InMLDReport");
        CliPrintf (CliHandle,
                   "%5d\t%-15s\r\n",
                   u4Ipv6IfIcmpInGroupMembReductions, "InMLDDone");
        CliPrintf (CliHandle, "-------------------------------------\r\n");
        CliPrintf (CliHandle, "    Sent    \r\n");
        CliPrintf (CliHandle, "-------------------------------------\r\n");
        CliPrintf (CliHandle,
                   "%5d\t%-15s\t%5d\t%-15s\r\n",
                   u4Ipv6IfIcmpOutMsgs, "OutMsgs",
                   u4Ipv6IfIcmpOutErrors, "OutErrs");
        CliPrintf (CliHandle,
                   "%5d\t%-15s\t%5d\t%-15s\r\n",
                   u4Ipv6IfIcmpOutDestUnreachs, "OutDstUnreach",
                   u4Ipv6IfIcmpOutAdminProhibs, "OutAdminProhib");
        CliPrintf (CliHandle,
                   "%5d\t%-15s\t%5d\t%-15s\r\n",
                   u4Ipv6IfIcmpOutTimeExcds, "OutTmexceeded",
                   u4Ipv6IfIcmpOutParmProblems, "OutParamprob");
        CliPrintf (CliHandle,
                   "%5d\t%-15s\t%5d\t%-15s\r\n",
                   u4Ipv6IfIcmpOutPktTooBigs, "OutPktToobig",
                   u4Ipv6IfIcmpOutEchos, "OutEchoReq");
        CliPrintf (CliHandle,
                   "%5d\t%-15s\t%5d\t%-15s\r\n",
                   u4Ipv6IfIcmpOutEchoReplies, "OutEchoResp",
                   u4Ipv6IfIcmpOutRouterSolicits, "OutRouterSol");
        CliPrintf (CliHandle,
                   "%5d\t%-15s\t%5d\t%-15s\r\n",
                   u4Ipv6IfIcmpOutRouterAdvertisements, "OutRouterAdv",
                   u4Ipv6IfIcmpOutNeighborSolicits, "OutNeighSol");
        CliPrintf (CliHandle,
                   "%5d\t%-15s\t%5d\t%-15s\r\n",
                   u4Ipv6IfIcmpOutNeighborAdvertisements, "OutNeighAdv",
                   u4Ipv6IfIcmpOutRedirects, "OutRedir");
        CliPrintf (CliHandle,
                   "%5d\t%-15s\t%5d\t%-15s\r\n",
                   u4Ipv6IfIcmpOutGroupMembQueries, "OutMLDQuery",
                   u4Ipv6IfIcmpOutGroupMembResponses, "OutMLDReport");
        CliPrintf (CliHandle,
                   "%5d\t%-15s\r\n",
                   u4Ipv6IfIcmpOutGroupMembReductions, "OutMLDDone");
    }
    else
    {
        MEMSET (au1HCTempCount, IP_ZERO, CFA_CLI_U8_STR_LENGTH);
        MEMSET (au1HCTempCnt, IP_ZERO, CFA_CLI_U8_STR_LENGTH);

        FSAP_U8_2STR (&u8IpIfStatsHCInReceives, (CHR1 *) & au1HCTempCount);
        FSAP_U8_2STR (&u8IpIfStatsHCInOctets, (CHR1 *) & au1HCTempCnt);
        CliPrintf (CliHandle,
                   "%5s\t%-15s\t%5s\t%-15s\t",
                   au1HCTempCount, "InRcvs", au1HCTempCnt, "InOctets");

        MEMSET (au1HCTempCount, IP_ZERO, CFA_CLI_U8_STR_LENGTH);
        MEMSET (au1HCTempCnt, IP_ZERO, CFA_CLI_U8_STR_LENGTH);

        FSAP_U8_2STR (&u8IpIfStatsHCInForwDatagrams, (CHR1 *) & au1HCTempCount);
        FSAP_U8_2STR (&u8IpIfStatsHCInDelivers, (CHR1 *) & au1HCTempCnt);
        CliPrintf (CliHandle,
                   "%5s\t%-15s\r\n%5s\t%-15s\t",
                   au1HCTempCount, "InFwdDgrms", au1HCTempCnt, "InDelivers");

        MEMSET (au1HCTempCount, IP_ZERO, CFA_CLI_U8_STR_LENGTH);
        MEMSET (au1HCTempCnt, IP_ZERO, CFA_CLI_U8_STR_LENGTH);

        FSAP_U8_2STR (&u8IpIfStatsHCOutRequests, (CHR1 *) & au1HCTempCount);
        FSAP_U8_2STR (&u8IpIfStatsHCOutForwDatagrams, (CHR1 *) & au1HCTempCnt);
        CliPrintf (CliHandle,
                   "%5s\t%-15s\t%5s\t%-15s\r\n",
                   au1HCTempCount, "OutRequests", au1HCTempCnt, "OutFwdDgrms");

        MEMSET (au1HCTempCount, IP_ZERO, CFA_CLI_U8_STR_LENGTH);
        MEMSET (au1HCTempCnt, IP_ZERO, CFA_CLI_U8_STR_LENGTH);

        FSAP_U8_2STR (&u8IpIfStatsHCOutTransmits, (CHR1 *) & au1HCTempCount);
        FSAP_U8_2STR (&u8IpIfStatsHCOutOctets, (CHR1 *) & au1HCTempCnt);

        CliPrintf (CliHandle,
                   "%5s\t%-15s\r\n%5s\t%-15s\t",
                   au1HCTempCount, "OutTrnsmits", au1HCTempCnt, "OutOctets");

        MEMSET (au1HCTempCount, IP_ZERO, CFA_CLI_U8_STR_LENGTH);
        MEMSET (au1HCTempCnt, IP_ZERO, CFA_CLI_U8_STR_LENGTH);

        FSAP_U8_2STR (&u8IpIfStatsHCInMcastPkts, (CHR1 *) & au1HCTempCount);
        FSAP_U8_2STR (&u8IpIfStatsHCInMcastOctets, (CHR1 *) & au1HCTempCnt);
        CliPrintf (CliHandle,
                   "%5s\t%-15s\r\n%5s\t%-15s\t",
                   au1HCTempCount, "InMcstPkts", au1HCTempCnt, "InMcstOctets");

        MEMSET (au1HCTempCount, IP_ZERO, CFA_CLI_U8_STR_LENGTH);
        MEMSET (au1HCTempCnt, IP_ZERO, CFA_CLI_U8_STR_LENGTH);

        FSAP_U8_2STR (&u8IpIfStatsHCOutMcastPkts, (CHR1 *) & au1HCTempCount);
        FSAP_U8_2STR (&u8IpIfStatsHCOutMcastOctets, (CHR1 *) & au1HCTempCnt);
        CliPrintf (CliHandle,
                   "%5s\t%-15s\t%5s\t%-15s\r\n",
                   au1HCTempCount, "OutMcstPkts", au1HCTempCnt,
                   "OutMcstOctets");

        MEMSET (au1HCTempCount, IP_ZERO, CFA_CLI_U8_STR_LENGTH);
        MEMSET (au1HCTempCnt, IP_ZERO, CFA_CLI_U8_STR_LENGTH);

        FSAP_U8_2STR (&u8IpIfStatsHCInBcastPkts, (CHR1 *) & au1HCTempCount);
        FSAP_U8_2STR (&u8IpIfStatsHCOutBcastPkts, (CHR1 *) & au1HCTempCnt);
        CliPrintf (CliHandle,
                   "%5s\t%-15s\t%5s\t%-15s\r\n",
                   au1HCTempCount, "InBcast", au1HCTempCnt, "OutBcast");
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : Ip6TrafficShow                                         */
/*                                                                           */
/* Description      : This function is invoked to display IP statistics      */
/*                                                                           */
/* Input Parameters : 1. CliHandle - CliContext ID                           */
/*                    2. u1HCFlag - High count flag                          */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/

INT4
Ip6TrafficShowInCxt (tCliHandle CliHandle, UINT1 u1HCFlag, UINT4 u4VcId)
{
    INT4                i4IpSystemStatsIPVersion = INET_ADDR_TYPE_IPV6;
    UINT4               u4RetVal;
#ifndef LNXIP6_WANTED            /* FSIP */
    UINT4               u4ShowAllCxt = FALSE;
#endif
    UINT4               u4PrevVcId;
    tIp6CliInfo         Ip6CliInfo;

    UINT4               u4IpSystemStatsInDiscards = IP_ZERO;
    UINT4               u4IpSystemStatsInDelivers = IP_ZERO;
    UINT4               u4IpSystemStatsOutDiscards = IP_ZERO;
    UINT4               u4IpSystemStatsReasmFails = IP_ZERO;
    UINT4               u4IpSystemStatsOutFragCreates = IP_ZERO;
    UINT4               u4IpSystemStatsInMcastPkts = IP_ZERO;
    UINT4               u4IpSystemStatsOutMcastPkts = IP_ZERO;
    UINT4               u4IpSystemStatsInTruncatedPkts = IP_ZERO;
    UINT4               u4IpSystemStatsInOctets = IP_ZERO;
    UINT4               u4IpSystemStatsInNoRoutes = IP_ZERO;
    UINT4               u4IpSystemStatsOutForwDatagrams = IP_ZERO;
    UINT4               u4IpSystemStatsOutFragReqds = IP_ZERO;
    UINT4               u4IpSystemStatsOutTransmits = IP_ZERO;
    UINT4               u4IpSystemStatsOutOctets = IP_ZERO;
    UINT4               u4IpSystemStatsInMcastOctets = IP_ZERO;
    UINT4               u4IpSystemStatsOutMcastOctets = IP_ZERO;
    UINT4               u4IpSystemStatsInBcastPkts = IP_ZERO;
    UINT4               u4IpSystemStatsOutBcastPkts = IP_ZERO;
    UINT4               u4IpSystemStatsDiscontinuityTime = IP_ZERO;
    UINT4               u4IpSystemStatsRefreshRate = IP_ZERO;
    UINT4               u4UdpInDgrams = IP_ZERO;
    UINT4               u4UdpInNoPorts = IP_ZERO;
    UINT4               u4UdpErrPkts = IP_ZERO;
    UINT4               u4UdpOutDgrams = IP_ZERO;
    UINT4               u4InNARouterFlagSet = IP_ZERO;
    UINT4               u4InNASolicitedFlagSet = IP_ZERO;
    UINT4               u4InNAOverrideFlagSet = IP_ZERO;
    UINT4               u4OutNARouterFlagSet = IP_ZERO;
    UINT4               u4OutNASolicitedFlagSet = IP_ZERO;
    UINT4               u4OutNAOverrideFlagSet = IP_ZERO;

    tSNMP_COUNTER64_TYPE RetValIpSystemStatsHCInReceives;
    tSNMP_COUNTER64_TYPE RetValIpSystemStatsHCInOctets;
    tSNMP_COUNTER64_TYPE RetValIpSystemStatsHCInForwDatagrams;
    tSNMP_COUNTER64_TYPE RetValIpSystemStatsHCInDelivers;
    tSNMP_COUNTER64_TYPE RetValIpSystemStatsHCOutRequests;
    tSNMP_COUNTER64_TYPE RetValIpSystemStatsHCOutForwDatagrams;
    tSNMP_COUNTER64_TYPE RetValIpSystemStatsHCOutTransmits;
    tSNMP_COUNTER64_TYPE RetValIpSystemStatsHCOutOctets;
    tSNMP_COUNTER64_TYPE RetValIpSystemStatsHCInMcastPkts;
    tSNMP_COUNTER64_TYPE RetValIpSystemStatsHCOutMcastPkts;
    tSNMP_COUNTER64_TYPE RetValIpSystemStatsHCOutMcastOctets;
    tSNMP_COUNTER64_TYPE RetValIpSystemStatsHCInBcastPkts;
    tSNMP_COUNTER64_TYPE RetValIpSystemStatsHCOutBcastPkts;
    tSNMP_COUNTER64_TYPE RetValIpSystemStatsHCInMcastOctets;

    FS_UINT8            u8IpSystemStatsHCInReceives;
    FS_UINT8            u8IpSystemStatsHCInOctets;
    FS_UINT8            u8IpSystemStatsHCInForwDatagrams;
    FS_UINT8            u8IpSystemStatsHCInDelivers;
    FS_UINT8            u8IpSystemStatsHCOutRequests;
    FS_UINT8            u8IpSystemStatsHCOutForwDatagrams;
    FS_UINT8            u8IpSystemStatsHCOutTransmits;
    FS_UINT8            u8IpSystemStatsHCOutOctets;
    FS_UINT8            u8IpSystemStatsHCInMcastPkts;
    FS_UINT8            u8IpSystemStatsHCOutMcastPkts;
    FS_UINT8            u8IpSystemStatsHCOutMcastOctets;
    FS_UINT8            u8IpSystemStatsHCInBcastPkts;
    FS_UINT8            u8IpSystemStatsHCOutBcastPkts;
    FS_UINT8            u8IpSystemStatsHCInMcastOctets;
    FS_UINT8            u8UdpHCInDatagrams;
    FS_UINT8            u8UdpHCOutDatagrams;
    tSNMP_COUNTER64_TYPE RetUdpHCInDatagrams;
    tSNMP_COUNTER64_TYPE RetUdpHCOutDatagrams;

    UINT1               au1HCTempCount[CFA_CLI_U8_STR_LENGTH];
    UINT1               au1HCTempCnt[CFA_CLI_U8_STR_LENGTH];
    UINT1               au1ContextName[VCMALIAS_MAX_ARRAY_LEN];

    MEMSET (&Ip6CliInfo, IP6_ZERO, sizeof (tIp6CliInfo));
    MEMSET (&RetUdpHCInDatagrams, IP_ZERO, sizeof (tSNMP_COUNTER64_TYPE));
    MEMSET (&RetUdpHCOutDatagrams, IP_ZERO, sizeof (tSNMP_COUNTER64_TYPE));
    MEMSET (&RetValIpSystemStatsHCInReceives, IP_ZERO,
            sizeof (tSNMP_COUNTER64_TYPE));
    MEMSET (&RetValIpSystemStatsHCInOctets, IP_ZERO,
            sizeof (tSNMP_COUNTER64_TYPE));
    MEMSET (&RetValIpSystemStatsHCInForwDatagrams, IP_ZERO,
            sizeof (tSNMP_COUNTER64_TYPE));
    MEMSET (&RetValIpSystemStatsHCInDelivers, IP_ZERO,
            sizeof (tSNMP_COUNTER64_TYPE));
    MEMSET (&RetValIpSystemStatsHCOutRequests, IP_ZERO,
            sizeof (tSNMP_COUNTER64_TYPE));
    MEMSET (&RetValIpSystemStatsHCOutForwDatagrams, IP_ZERO,
            sizeof (tSNMP_COUNTER64_TYPE));
    MEMSET (&RetValIpSystemStatsHCOutTransmits, IP_ZERO,
            sizeof (tSNMP_COUNTER64_TYPE));
    MEMSET (&RetValIpSystemStatsHCOutOctets, IP_ZERO,
            sizeof (tSNMP_COUNTER64_TYPE));
    MEMSET (&RetValIpSystemStatsHCInMcastPkts, IP_ZERO,
            sizeof (tSNMP_COUNTER64_TYPE));
    MEMSET (&RetValIpSystemStatsHCOutMcastPkts, IP_ZERO,
            sizeof (tSNMP_COUNTER64_TYPE));
    MEMSET (&RetValIpSystemStatsHCOutMcastOctets, IP_ZERO,
            sizeof (tSNMP_COUNTER64_TYPE));
    MEMSET (&RetValIpSystemStatsHCInBcastPkts, IP_ZERO,
            sizeof (tSNMP_COUNTER64_TYPE));
    MEMSET (&RetValIpSystemStatsHCOutBcastPkts, IP_ZERO,
            sizeof (tSNMP_COUNTER64_TYPE));
    MEMSET (&RetValIpSystemStatsHCInMcastOctets, IP_ZERO,
            sizeof (tSNMP_COUNTER64_TYPE));

    MEMSET (au1HCTempCount, IP_ZERO, CFA_CLI_U8_STR_LENGTH);
    MEMSET (au1HCTempCnt, IP_ZERO, CFA_CLI_U8_STR_LENGTH);

    FSAP_U8_CLR (&u8IpSystemStatsHCInReceives);
    FSAP_U8_CLR (&u8IpSystemStatsHCInOctets);
    FSAP_U8_CLR (&u8IpSystemStatsHCInForwDatagrams);
    FSAP_U8_CLR (&u8IpSystemStatsHCInDelivers);
    FSAP_U8_CLR (&u8IpSystemStatsHCOutRequests);
    FSAP_U8_CLR (&u8IpSystemStatsHCOutForwDatagrams);
    FSAP_U8_CLR (&u8IpSystemStatsHCOutTransmits);
    FSAP_U8_CLR (&u8IpSystemStatsHCOutOctets);
    FSAP_U8_CLR (&u8IpSystemStatsHCInMcastPkts);
    FSAP_U8_CLR (&u8IpSystemStatsHCOutMcastPkts);
    FSAP_U8_CLR (&u8IpSystemStatsHCOutMcastOctets);
    FSAP_U8_CLR (&u8IpSystemStatsHCInBcastPkts);
    FSAP_U8_CLR (&u8IpSystemStatsHCOutBcastPkts);
    FSAP_U8_CLR (&u8IpSystemStatsHCInMcastOctets);
    FSAP_U8_CLR (&u8UdpHCInDatagrams);
    FSAP_U8_CLR (&u8UdpHCOutDatagrams);

#ifndef LNXIP6_WANTED            /* FSIP */
    if (u4VcId == VCM_INVALID_VC)
    {
        if (Ip6UtilGetFirstCxtId (&u4VcId) == IP_FAILURE)
        {
            return CLI_SUCCESS;
        }
        u4ShowAllCxt = TRUE;
    }
#else
    u4VcId = VCM_DEFAULT_CONTEXT;
#endif /* IP_WANTED */

    if (VcmIsL3VcExist (u4VcId) == VCM_FALSE)
    {
        CliPrintf (CliHandle, "\r%% Invalid VRF \r\n");
        return CLI_FAILURE;
    }
    /*If valid VcId is given, the loop gets executed only once, otherwise it loops
     * through all the VRFs*/
    do
    {
        u4PrevVcId = u4VcId;

        /* Getting the IP Related Information */
        nmhGetFsMIStdIpSystemStatsInReceives
            (u4VcId, i4IpSystemStatsIPVersion, &u4RetVal);
        Ip6CliInfo.Ip6Stats.u4In_rcvs = u4RetVal;

        nmhGetFsMIStdIpSystemStatsInHdrErrors
            (u4VcId, i4IpSystemStatsIPVersion, &u4RetVal);
        Ip6CliInfo.Ip6Stats.u4In_hdr_err = u4RetVal;

        nmhGetFsMIStdIpSystemStatsInAddrErrors
            (u4VcId, i4IpSystemStatsIPVersion, &u4RetVal);
        Ip6CliInfo.Ip6Stats.u4In_addr_err = u4RetVal;

        nmhGetFsMIStdIpSystemStatsInUnknownProtos
            (u4VcId, i4IpSystemStatsIPVersion, &u4RetVal);
        Ip6CliInfo.Ip6Stats.u4Bad_proto = u4RetVal;

        nmhGetFsMIStdIpSystemStatsReasmOKs (u4VcId, i4IpSystemStatsIPVersion,
                                            &u4RetVal);
        Ip6CliInfo.Ip6Stats.u4Reasm_oks = u4RetVal;

        nmhGetFsMIStdIpReasmTimeout (u4VcId, (INT4 *) &u4RetVal);
        Ip6CliInfo.Ip6Stats.u4Reasm_timeout = u4RetVal;

        nmhGetFsMIStdIpSystemStatsReasmReqds (u4VcId, i4IpSystemStatsIPVersion,
                                              &u4RetVal);
        Ip6CliInfo.Ip6Stats.u4Reasm_reqs = u4RetVal;

        nmhGetFsMIStdIpSystemStatsOutFragOKs (u4VcId, i4IpSystemStatsIPVersion,
                                              &u4RetVal);
        Ip6CliInfo.Ip6Stats.u4Out_frag_pkts = u4RetVal;

        nmhGetFsMIStdIpSystemStatsOutFragFails
            (u4VcId, i4IpSystemStatsIPVersion, &u4RetVal);
        Ip6CliInfo.Ip6Stats.u4Frag_fails = u4RetVal;

        nmhGetFsMIStdIpSystemStatsInForwDatagrams
            (u4VcId, i4IpSystemStatsIPVersion, &u4RetVal);
        Ip6CliInfo.Ip6Stats.u4Forw_attempts = u4RetVal;

        nmhGetFsMIStdIpSystemStatsOutRequests
            (u4VcId, i4IpSystemStatsIPVersion, &u4RetVal);
        Ip6CliInfo.Ip6Stats.u4Out_requests = u4RetVal;

        nmhGetFsMIStdIpSystemStatsOutNoRoutes
            (u4VcId, i4IpSystemStatsIPVersion, &u4RetVal);
        Ip6CliInfo.Ip6Stats.u4Out_no_routes = u4RetVal;

        nmhGetFsMIIpv6IcmpInMsgs (u4VcId, &u4RetVal);
        Ip6CliInfo.Icmp6Stats.u4IcmpInMsgs = u4RetVal;

        nmhGetFsMIIpv6IcmpInErrors (u4VcId, &u4RetVal);
        Ip6CliInfo.Icmp6Stats.u4IcmpInErr = u4RetVal;

        nmhGetFsMIStdIcmpMsgStatsInPkts
            (u4VcId, i4IpSystemStatsIPVersion, ICMP6_DEST_UNREACHABLE,
             &u4RetVal);
        Ip6CliInfo.Icmp6Stats.u4IcmpInDestUnreach = u4RetVal;

        nmhGetFsMIStdIcmpMsgStatsInPkts
            (u4VcId, i4IpSystemStatsIPVersion, ICMP6_ROUTE_REDIRECT, &u4RetVal);
        Ip6CliInfo.Icmp6Stats.u4IcmpInRedirects = u4RetVal;

        nmhGetFsMIStdIcmpMsgStatsInPkts
            (u4VcId, i4IpSystemStatsIPVersion, ICMP6_TIME_EXCEEDED, &u4RetVal);
        Ip6CliInfo.Icmp6Stats.u4IcmpInTimeExceeds = u4RetVal;

        nmhGetFsMIStdIcmpMsgStatsInPkts
            (u4VcId, i4IpSystemStatsIPVersion, ICMP6_PKT_PARAM_PROBLEM,
             &u4RetVal);
        Ip6CliInfo.Icmp6Stats.u4IcmpInParmProbs = u4RetVal;

        nmhGetFsMIStdIcmpMsgStatsInPkts
            ((INT4) u4VcId, i4IpSystemStatsIPVersion, ICMP6_PKT_TOO_BIG,
             &u4RetVal);
        Ip6CliInfo.Icmp6Stats.u4IcmpInToobig = u4RetVal;

        nmhGetFsMIStdIcmpMsgStatsInPkts
            (u4VcId, i4IpSystemStatsIPVersion, ICMP6_ECHO_REQUEST, &u4RetVal);
        Ip6CliInfo.Icmp6Stats.u4IcmpInEchos = u4RetVal;

        nmhGetFsMIStdIcmpMsgStatsInPkts
            (u4VcId, i4IpSystemStatsIPVersion, ICMP6_ECHO_REPLY, &u4RetVal);
        Ip6CliInfo.Icmp6Stats.u4IcmpInEchosReps = u4RetVal;

        nmhGetFsMIStdIcmpMsgStatsOutPkts
            (u4VcId, i4IpSystemStatsIPVersion, ICMP6_DEST_UNREACHABLE,
             &u4RetVal);
        Ip6CliInfo.Icmp6Stats.u4IcmpOutDestUnreach = u4RetVal;

        nmhGetFsMIStdIcmpMsgStatsOutPkts
            (u4VcId, i4IpSystemStatsIPVersion, ICMP6_RATE_LIMIT, &u4RetVal);
        Ip6CliInfo.Icmp6Stats.u4IcmpOutRateLimit = u4RetVal;

        nmhGetFsMIStdIcmpMsgStatsOutPkts
            (u4VcId, i4IpSystemStatsIPVersion, ICMP6_ROUTE_REDIRECT, &u4RetVal);
        Ip6CliInfo.Icmp6Stats.u4IcmpOutRedirects = u4RetVal;

        nmhGetFsMIStdIcmpMsgStatsOutPkts
            (u4VcId, i4IpSystemStatsIPVersion, ICMP6_TIME_EXCEEDED, &u4RetVal);
        Ip6CliInfo.Icmp6Stats.u4IcmpOutTimeExceeds = u4RetVal;

        nmhGetFsMIStdIcmpMsgStatsOutPkts
            (u4VcId, i4IpSystemStatsIPVersion, ICMP6_PKT_PARAM_PROBLEM,
             &u4RetVal);
        Ip6CliInfo.Icmp6Stats.u4IcmpOutParmProbs = u4RetVal;

        nmhGetFsMIStdIcmpMsgStatsOutPkts
            ((INT4) u4VcId, i4IpSystemStatsIPVersion, ICMP6_PKT_TOO_BIG,
             &u4RetVal);
        Ip6CliInfo.Icmp6Stats.u4IcmpOutTooBig = u4RetVal;

        nmhGetFsMIStdIcmpMsgStatsOutPkts
            (u4VcId, i4IpSystemStatsIPVersion, ICMP6_ECHO_REQUEST, &u4RetVal);
        Ip6CliInfo.Icmp6Stats.u4IcmpOutEchos = u4RetVal;

        nmhGetFsMIStdIcmpMsgStatsOutPkts
            (u4VcId, i4IpSystemStatsIPVersion, ICMP6_ECHO_REPLY, &u4RetVal);
        Ip6CliInfo.Icmp6Stats.u4IcmpOutEchosReps = u4RetVal;

        nmhGetFsMIStdIcmpStatsOutMsgs (u4VcId, i4IpSystemStatsIPVersion,
                                       &u4RetVal);
        Ip6CliInfo.Icmp6Stats.u4IcmpOutMsgs = u4RetVal;

        nmhGetFsMIStdIcmpStatsOutErrors (u4VcId, i4IpSystemStatsIPVersion,
                                         &u4RetVal);
        Ip6CliInfo.Icmp6Stats.u4IcmpOutErr = u4RetVal;

        nmhGetFsMIStdIcmpMsgStatsOutPkts
            (u4VcId, i4IpSystemStatsIPVersion, ICMP6_ROUTER_SOLICITATION,
             &u4RetVal);
        Ip6CliInfo.Icmp6Stats.u4IcmpOutRtSol = u4RetVal;

        nmhGetFsMIStdIcmpMsgStatsOutPkts
            (u4VcId, i4IpSystemStatsIPVersion, ICMP6_ROUTER_ADVERTISEMENT,
             &u4RetVal);
        Ip6CliInfo.Icmp6Stats.u4IcmpOutRtAdv = u4RetVal;

        nmhGetFsMIStdIcmpMsgStatsOutPkts
            (u4VcId, i4IpSystemStatsIPVersion, ICMP6_NEIGHBOUR_SOLICITATION,
             &u4RetVal);
        Ip6CliInfo.Icmp6Stats.u4IcmpOutNbSol = u4RetVal;

        nmhGetFsMIStdIcmpMsgStatsOutPkts
            (u4VcId, i4IpSystemStatsIPVersion, ICMP6_NEIGHBOUR_ADVERTISEMENT,
             &u4RetVal);
        Ip6CliInfo.Icmp6Stats.u4IcmpOutNbAdv = u4RetVal;

        nmhGetFsMIStdIcmpMsgStatsInPkts
            (u4VcId, i4IpSystemStatsIPVersion, ICMP6_ROUTER_SOLICITATION,
             &u4RetVal);
        Ip6CliInfo.Icmp6Stats.u4IcmpInRtSol = u4RetVal;

        nmhGetFsMIStdIcmpMsgStatsInPkts
            (u4VcId, i4IpSystemStatsIPVersion, ICMP6_ROUTER_ADVERTISEMENT,
             &u4RetVal);
        Ip6CliInfo.Icmp6Stats.u4IcmpInRtAdv = u4RetVal;

        nmhGetFsMIStdIcmpMsgStatsInPkts
            (u4VcId, i4IpSystemStatsIPVersion, ICMP6_NEIGHBOUR_SOLICITATION,
             &u4RetVal);
        Ip6CliInfo.Icmp6Stats.u4IcmpInNbSol = u4RetVal;

        nmhGetFsMIStdIcmpMsgStatsInPkts
            (u4VcId, i4IpSystemStatsIPVersion, ICMP6_NEIGHBOUR_ADVERTISEMENT,
             &u4RetVal);
        Ip6CliInfo.Icmp6Stats.u4IcmpInNbAdv = u4RetVal;
        /*Get the new objects */
        nmhGetFsMIIpv6IcmpInNARouterFlagSet (u4VcId, &u4InNARouterFlagSet);
        nmhGetFsMIIpv6IcmpInNASolicitedFlagSet (u4VcId,
                                                &u4InNASolicitedFlagSet);
        nmhGetFsMIIpv6IcmpInNAOverrideFlagSet (u4VcId, &u4InNAOverrideFlagSet);
        nmhGetFsMIIpv6IcmpOutNARouterFlagSet (u4VcId, &u4OutNARouterFlagSet);
        nmhGetFsMIIpv6IcmpOutNASolicitedFlagSet (u4VcId,
                                                 &u4OutNASolicitedFlagSet);
        nmhGetFsMIIpv6IcmpOutNAOverrideFlagSet (u4VcId,
                                                &u4OutNAOverrideFlagSet);

        nmhGetFsMIStdIpSystemStatsInDiscards (u4VcId,
                                              i4IpSystemStatsIPVersion,
                                              &u4IpSystemStatsInDiscards);
        nmhGetFsMIStdIpSystemStatsInDelivers (u4VcId,
                                              i4IpSystemStatsIPVersion,
                                              &u4IpSystemStatsInDelivers);
        nmhGetFsMIStdIpSystemStatsOutDiscards (u4VcId,
                                               i4IpSystemStatsIPVersion,
                                               &u4IpSystemStatsOutDiscards);
        nmhGetFsMIStdIpSystemStatsReasmFails (u4VcId,
                                              i4IpSystemStatsIPVersion,
                                              &u4IpSystemStatsReasmFails);
        nmhGetFsMIStdIpSystemStatsOutFragCreates (u4VcId,
                                                  i4IpSystemStatsIPVersion,
                                                  &u4IpSystemStatsOutFragCreates);
        nmhGetFsMIStdIpSystemStatsInMcastPkts (u4VcId,
                                               i4IpSystemStatsIPVersion,
                                               &u4IpSystemStatsInMcastPkts);
        nmhGetFsMIStdIpSystemStatsOutMcastPkts (u4VcId,
                                                i4IpSystemStatsIPVersion,
                                                &u4IpSystemStatsOutMcastPkts);
        nmhGetFsMIStdIpSystemStatsInTruncatedPkts (u4VcId,
                                                   i4IpSystemStatsIPVersion,
                                                   &u4IpSystemStatsInTruncatedPkts);
        nmhGetFsMIStdIpSystemStatsInOctets (u4VcId,
                                            i4IpSystemStatsIPVersion,
                                            &u4IpSystemStatsInOctets);
        nmhGetFsMIStdIpSystemStatsInNoRoutes (u4VcId,
                                              i4IpSystemStatsIPVersion,
                                              &u4IpSystemStatsInNoRoutes);
        nmhGetFsMIStdIpSystemStatsOutForwDatagrams (u4VcId,
                                                    i4IpSystemStatsIPVersion,
                                                    &u4IpSystemStatsOutForwDatagrams);
        nmhGetFsMIStdIpSystemStatsOutFragReqds (u4VcId,
                                                i4IpSystemStatsIPVersion,
                                                &u4IpSystemStatsOutFragReqds);
        nmhGetFsMIStdIpSystemStatsOutTransmits (u4VcId,
                                                i4IpSystemStatsIPVersion,
                                                &u4IpSystemStatsOutTransmits);
        nmhGetFsMIStdIpSystemStatsOutOctets (u4VcId,
                                             i4IpSystemStatsIPVersion,
                                             &u4IpSystemStatsOutOctets);
        nmhGetFsMIStdIpSystemStatsInMcastOctets (u4VcId,
                                                 i4IpSystemStatsIPVersion,
                                                 &u4IpSystemStatsInMcastOctets);
        nmhGetFsMIStdIpSystemStatsOutMcastOctets (u4VcId,
                                                  i4IpSystemStatsIPVersion,
                                                  &u4IpSystemStatsOutMcastOctets);
        nmhGetFsMIStdIpSystemStatsInBcastPkts (u4VcId,
                                               i4IpSystemStatsIPVersion,
                                               &u4IpSystemStatsInBcastPkts);
        nmhGetFsMIStdIpSystemStatsOutBcastPkts (u4VcId,
                                                i4IpSystemStatsIPVersion,
                                                &u4IpSystemStatsOutBcastPkts);
        nmhGetFsMIStdIpSystemStatsDiscontinuityTime (u4VcId,
                                                     i4IpSystemStatsIPVersion,
                                                     &u4IpSystemStatsDiscontinuityTime);
        nmhGetFsMIStdIpSystemStatsRefreshRate (u4VcId,
                                               i4IpSystemStatsIPVersion,
                                               &u4IpSystemStatsRefreshRate);

        /*Get IP HC counters if HC flag is set */

        if (u1HCFlag != IP6_CLI_HC_DISABLED)
        {
            nmhGetFsMIStdIpSystemStatsHCInReceives (u4VcId,
                                                    i4IpSystemStatsIPVersion,
                                                    &RetValIpSystemStatsHCInReceives);
            nmhGetFsMIStdIpSystemStatsHCInOctets (u4VcId,
                                                  i4IpSystemStatsIPVersion,
                                                  &RetValIpSystemStatsHCInOctets);
            nmhGetFsMIStdIpSystemStatsHCInForwDatagrams (u4VcId,
                                                         i4IpSystemStatsIPVersion,
                                                         &RetValIpSystemStatsHCInForwDatagrams);
            nmhGetFsMIStdIpSystemStatsHCInDelivers (u4VcId,
                                                    i4IpSystemStatsIPVersion,
                                                    &RetValIpSystemStatsHCInDelivers);
            nmhGetFsMIStdIpSystemStatsHCOutRequests (u4VcId,
                                                     i4IpSystemStatsIPVersion,
                                                     &RetValIpSystemStatsHCOutRequests);
            nmhGetFsMIStdIpSystemStatsHCOutForwDatagrams (u4VcId,
                                                          i4IpSystemStatsIPVersion,
                                                          &RetValIpSystemStatsHCOutForwDatagrams);
            nmhGetFsMIStdIpSystemStatsHCOutTransmits (u4VcId,
                                                      i4IpSystemStatsIPVersion,
                                                      &RetValIpSystemStatsHCOutTransmits);
            nmhGetFsMIStdIpSystemStatsHCOutOctets (u4VcId,
                                                   i4IpSystemStatsIPVersion,
                                                   &RetValIpSystemStatsHCOutOctets);
            nmhGetFsMIStdIpSystemStatsHCInMcastPkts (u4VcId,
                                                     i4IpSystemStatsIPVersion,
                                                     &RetValIpSystemStatsHCInMcastPkts);
            nmhGetFsMIStdIpSystemStatsHCInMcastOctets (u4VcId,
                                                       i4IpSystemStatsIPVersion,
                                                       &RetValIpSystemStatsHCInMcastOctets);
            nmhGetFsMIStdIpSystemStatsHCOutMcastPkts (u4VcId,
                                                      i4IpSystemStatsIPVersion,
                                                      &RetValIpSystemStatsHCOutMcastPkts);
            nmhGetFsMIStdIpSystemStatsHCOutMcastOctets (u4VcId,
                                                        i4IpSystemStatsIPVersion,
                                                        &RetValIpSystemStatsHCOutMcastOctets);
            nmhGetFsMIStdIpSystemStatsHCInBcastPkts (u4VcId,
                                                     i4IpSystemStatsIPVersion,
                                                     &RetValIpSystemStatsHCInBcastPkts);
            nmhGetFsMIStdIpSystemStatsHCOutBcastPkts (u4VcId,
                                                      i4IpSystemStatsIPVersion,
                                                      &RetValIpSystemStatsHCOutBcastPkts);

            FSAP_U8_ASSIGN_HI (&u8IpSystemStatsHCInReceives,
                               RetValIpSystemStatsHCInReceives.msn);
            FSAP_U8_ASSIGN_LO (&u8IpSystemStatsHCInReceives,
                               RetValIpSystemStatsHCInReceives.lsn);
            FSAP_U8_ASSIGN_HI (&u8IpSystemStatsHCInOctets,
                               RetValIpSystemStatsHCInOctets.msn);
            FSAP_U8_ASSIGN_LO (&u8IpSystemStatsHCInOctets,
                               RetValIpSystemStatsHCInOctets.lsn);
            FSAP_U8_ASSIGN_HI (&u8IpSystemStatsHCInForwDatagrams,
                               RetValIpSystemStatsHCInForwDatagrams.msn);
            FSAP_U8_ASSIGN_LO (&u8IpSystemStatsHCInForwDatagrams,
                               RetValIpSystemStatsHCInForwDatagrams.lsn);
            FSAP_U8_ASSIGN_HI (&u8IpSystemStatsHCInDelivers,
                               RetValIpSystemStatsHCInDelivers.msn);
            FSAP_U8_ASSIGN_LO (&u8IpSystemStatsHCInDelivers,
                               RetValIpSystemStatsHCInDelivers.lsn);
            FSAP_U8_ASSIGN_HI (&u8IpSystemStatsHCOutRequests,
                               RetValIpSystemStatsHCOutRequests.msn);
            FSAP_U8_ASSIGN_LO (&u8IpSystemStatsHCOutRequests,
                               RetValIpSystemStatsHCOutRequests.lsn);
            FSAP_U8_ASSIGN_HI (&u8IpSystemStatsHCOutForwDatagrams,
                               RetValIpSystemStatsHCOutForwDatagrams.msn);
            FSAP_U8_ASSIGN_LO (&u8IpSystemStatsHCOutForwDatagrams,
                               RetValIpSystemStatsHCOutForwDatagrams.lsn);
            FSAP_U8_ASSIGN_HI (&u8IpSystemStatsHCOutTransmits,
                               RetValIpSystemStatsHCOutTransmits.msn);
            FSAP_U8_ASSIGN_LO (&u8IpSystemStatsHCOutTransmits,
                               RetValIpSystemStatsHCOutTransmits.lsn);
            FSAP_U8_ASSIGN_HI (&u8IpSystemStatsHCOutOctets,
                               RetValIpSystemStatsHCOutOctets.msn);
            FSAP_U8_ASSIGN_LO (&u8IpSystemStatsHCOutOctets,
                               RetValIpSystemStatsHCOutOctets.lsn);
            FSAP_U8_ASSIGN_HI (&u8IpSystemStatsHCInMcastPkts,
                               RetValIpSystemStatsHCInMcastPkts.msn);
            FSAP_U8_ASSIGN_LO (&u8IpSystemStatsHCInMcastPkts,
                               RetValIpSystemStatsHCInMcastPkts.lsn);
            FSAP_U8_ASSIGN_HI (&u8IpSystemStatsHCOutMcastPkts,
                               RetValIpSystemStatsHCOutMcastPkts.msn);
            FSAP_U8_ASSIGN_LO (&u8IpSystemStatsHCOutMcastPkts,
                               RetValIpSystemStatsHCOutMcastPkts.lsn);
            FSAP_U8_ASSIGN_HI (&u8IpSystemStatsHCOutMcastOctets,
                               RetValIpSystemStatsHCOutMcastOctets.msn);
            FSAP_U8_ASSIGN_LO (&u8IpSystemStatsHCOutMcastOctets,
                               RetValIpSystemStatsHCOutMcastOctets.lsn);
            FSAP_U8_ASSIGN_HI (&u8IpSystemStatsHCInBcastPkts,
                               RetValIpSystemStatsHCInBcastPkts.msn);
            FSAP_U8_ASSIGN_LO (&u8IpSystemStatsHCInBcastPkts,
                               RetValIpSystemStatsHCInBcastPkts.lsn);
            FSAP_U8_ASSIGN_HI (&u8IpSystemStatsHCOutBcastPkts,
                               RetValIpSystemStatsHCOutBcastPkts.msn);
            FSAP_U8_ASSIGN_LO (&u8IpSystemStatsHCOutBcastPkts,
                               RetValIpSystemStatsHCOutBcastPkts.lsn);
            FSAP_U8_ASSIGN_HI (&u8IpSystemStatsHCInMcastOctets,
                               RetValIpSystemStatsHCInMcastOctets.msn);
            FSAP_U8_ASSIGN_LO (&u8IpSystemStatsHCInMcastOctets,
                               RetValIpSystemStatsHCInMcastOctets.lsn);
        }
        /* After getting all the values just Print it */

        VcmGetAliasName (u4VcId, au1ContextName);
        CliPrintf (CliHandle, "VRF   Name:          %s\r\n", au1ContextName);
        CliPrintf (CliHandle, "----------------\r\n");
        /*Display Ip related Information */
        CliPrintf (CliHandle, "\r\n    IPv6 Statistics    \r\n");
        CliPrintf (CliHandle, "    ***************    \r\n");

        if (u1HCFlag == IP6_CLI_HC_DISABLED)
        {

            CliPrintf (CliHandle,
                       "%10d Rcvd          %10d HdrErrors\r\n",
                       Ip6CliInfo.Ip6Stats.u4In_rcvs,
                       Ip6CliInfo.Ip6Stats.u4In_hdr_err);

            CliPrintf (CliHandle,
                       "%10d AddrErrors    %10d FwdDgrams      %10d UnknownProtos\r\n",
                       Ip6CliInfo.Ip6Stats.u4In_addr_err,
                       Ip6CliInfo.Ip6Stats.u4Forw_attempts,
                       Ip6CliInfo.Ip6Stats.u4Bad_proto);

            CliPrintf (CliHandle,
                       "%10d Discards      %10d Delivers       %10d OutRequests\r\n",
                       u4IpSystemStatsInDiscards, u4IpSystemStatsInDelivers,
                       Ip6CliInfo.Ip6Stats.u4Out_requests);

            CliPrintf (CliHandle,
                       "%10d OutDiscards   %10d OutNoRoutes    %10d ReasmReqds\r\n",
                       u4IpSystemStatsOutDiscards,
                       Ip6CliInfo.Ip6Stats.u4Out_no_routes,
                       Ip6CliInfo.Ip6Stats.u4Reasm_reqs);

            CliPrintf (CliHandle, "%10d ReasmOKs      %10d ReasmFails\r\n",
                       Ip6CliInfo.Ip6Stats.u4Reasm_oks,
                       u4IpSystemStatsReasmFails);

            CliPrintf (CliHandle, "Sent:");

            CliPrintf (CliHandle,
                       "%5d FragOKs       %10d FragFails      %10d FragCreates\r\n",
                       Ip6CliInfo.Ip6Stats.u4Out_frag_pkts,
                       Ip6CliInfo.Ip6Stats.u4Frag_fails,
                       u4IpSystemStatsOutFragCreates);

            CliPrintf (CliHandle,
                       "%10d RcvdMCastPkt  %10d SentMcastPkts  %10d TruncatedPkts\r\n",
                       u4IpSystemStatsInMcastPkts, u4IpSystemStatsOutMcastPkts,
                       u4IpSystemStatsInTruncatedPkts);

            /*Display the new mib objects */
            CliPrintf (CliHandle,
                       "%10d RcvdRedirects %10d SentRedirects \r\n",
                       Ip6CliInfo.Icmp6Stats.u4IcmpInRedirects,
                       Ip6CliInfo.Icmp6Stats.u4IcmpOutRedirects);

            CliPrintf (CliHandle,
                       "\r\n%10d %-13s %10d %-14s %10d %-15s\r\n",
                       u4IpSystemStatsInOctets, "InOctets",
                       u4IpSystemStatsInNoRoutes, "InNoRoutes",
                       u4IpSystemStatsOutForwDatagrams, "OutFwdDatgrms");

            CliPrintf (CliHandle,
                       "%10d %-13s %10d %-14s %10d %-15s\r\n",
                       u4IpSystemStatsOutFragReqds, "OutFrgRqds",
                       u4IpSystemStatsOutTransmits, "OutTrnsmit",
                       u4IpSystemStatsOutOctets, "OutOctets");

            CliPrintf (CliHandle,
                       "%10d %-13s %10d %-14s %10d %-15s\r\n",
                       u4IpSystemStatsInMcastOctets, "InMcstOctets",
                       u4IpSystemStatsOutMcastOctets, "OutMcastOctets",
                       u4IpSystemStatsInBcastPkts, "InBcstPkts");

            CliPrintf (CliHandle, "%10d %-13s %10d %-14s %10d %-15s\r\n",
                       u4IpSystemStatsOutBcastPkts, "OutBcstPkts",
                       u4IpSystemStatsDiscontinuityTime, "DiscntTime",
                       u4IpSystemStatsRefreshRate, "RefrshRate");

        }
        else
        {
            /*Display only the High capacity counters */
            MEMSET (au1HCTempCount, IP_ZERO, CFA_CLI_U8_STR_LENGTH);
            MEMSET (au1HCTempCnt, IP_ZERO, CFA_CLI_U8_STR_LENGTH);

            FSAP_U8_2STR (&u8IpSystemStatsHCInReceives,
                          (CHR1 *) & au1HCTempCount);
            FSAP_U8_2STR (&u8IpSystemStatsHCInOctets, (CHR1 *) & au1HCTempCnt);
            CliPrintf (CliHandle,
                       "%5s\t%-15s\t%5s\t%-15s\t",
                       au1HCTempCount, "InRcvs", au1HCTempCnt, "InOctets");

            MEMSET (au1HCTempCount, IP_ZERO, CFA_CLI_U8_STR_LENGTH);
            MEMSET (au1HCTempCnt, IP_ZERO, CFA_CLI_U8_STR_LENGTH);

            FSAP_U8_2STR (&u8IpSystemStatsHCInForwDatagrams,
                          (CHR1 *) & au1HCTempCount);
            FSAP_U8_2STR (&u8IpSystemStatsHCInDelivers,
                          (CHR1 *) & au1HCTempCnt);
            CliPrintf (CliHandle, "%5s\t%-15s\r\n%5s\t%-15s\t", au1HCTempCount,
                       "InFwdDgrms", au1HCTempCnt, "InDelivers");

            MEMSET (au1HCTempCount, IP_ZERO, CFA_CLI_U8_STR_LENGTH);
            MEMSET (au1HCTempCnt, IP_ZERO, CFA_CLI_U8_STR_LENGTH);

            FSAP_U8_2STR (&u8IpSystemStatsHCOutRequests,
                          (CHR1 *) & au1HCTempCount);
            FSAP_U8_2STR (&u8IpSystemStatsHCOutForwDatagrams,
                          (CHR1 *) & au1HCTempCnt);
            CliPrintf (CliHandle, "%5s\t%-15s\t%5s\t%-15s\r\n", au1HCTempCount,
                       "OutRequests", au1HCTempCnt, "OutFwdDgrms");

            MEMSET (au1HCTempCount, IP_ZERO, CFA_CLI_U8_STR_LENGTH);
            MEMSET (au1HCTempCnt, IP_ZERO, CFA_CLI_U8_STR_LENGTH);

            FSAP_U8_2STR (&u8IpSystemStatsHCOutTransmits,
                          (CHR1 *) & au1HCTempCount);
            FSAP_U8_2STR (&u8IpSystemStatsHCOutOctets, (CHR1 *) & au1HCTempCnt);

            CliPrintf (CliHandle,
                       "%5s\t%-15s\t%5s\t%-15s\t",
                       au1HCTempCount, "OutTrnsmits", au1HCTempCnt,
                       "OutOctets");

            MEMSET (au1HCTempCount, IP_ZERO, CFA_CLI_U8_STR_LENGTH);
            MEMSET (au1HCTempCnt, IP_ZERO, CFA_CLI_U8_STR_LENGTH);

            FSAP_U8_2STR (&u8IpSystemStatsHCInMcastPkts,
                          (CHR1 *) & au1HCTempCount);
            FSAP_U8_2STR (&u8IpSystemStatsHCInMcastOctets,
                          (CHR1 *) & au1HCTempCnt);
            CliPrintf (CliHandle, "%5s\t%-15s\r\n%5s\t%-15s\t", au1HCTempCount,
                       "InMcstPkts", au1HCTempCnt, "InMcstOctets");

            MEMSET (au1HCTempCount, IP_ZERO, CFA_CLI_U8_STR_LENGTH);
            MEMSET (au1HCTempCnt, IP_ZERO, CFA_CLI_U8_STR_LENGTH);

            FSAP_U8_2STR (&u8IpSystemStatsHCOutMcastPkts,
                          (CHR1 *) & au1HCTempCount);
            FSAP_U8_2STR (&u8IpSystemStatsHCOutMcastOctets,
                          (CHR1 *) & au1HCTempCnt);
            CliPrintf (CliHandle, "%5s\t%-15s\t%5s\t%-15s\r\n", au1HCTempCount,
                       "OutMcstPkts", au1HCTempCnt, "OutMcstOctets");

            MEMSET (au1HCTempCount, IP_ZERO, CFA_CLI_U8_STR_LENGTH);
            MEMSET (au1HCTempCnt, IP_ZERO, CFA_CLI_U8_STR_LENGTH);

            FSAP_U8_2STR (&u8IpSystemStatsHCInBcastPkts,
                          (CHR1 *) & au1HCTempCount);
            FSAP_U8_2STR (&u8IpSystemStatsHCOutBcastPkts,
                          (CHR1 *) & au1HCTempCnt);
            CliPrintf (CliHandle, "%5s\t%-15s\t%5s\t%-15s\r\n", au1HCTempCount,
                       "InBcast", au1HCTempCnt, "OutBcast");
        }

        /* Display Icmp related Information */

        if (u1HCFlag == IP6_CLI_HC_DISABLED)
        {

            CliPrintf (CliHandle, "\r\n    ICMP Statistics    \r\n");
            CliPrintf (CliHandle, "    ***************    \r\n");
            CliPrintf (CliHandle, "    Received :    \r\n");

            CliPrintf (CliHandle,
                       "%10d ICMPPkts     %10d ICMPErrPkt    %10d DestUnreach  %10d TimeExcds \r\n",
                       Ip6CliInfo.Icmp6Stats.u4IcmpInMsgs,
                       Ip6CliInfo.Icmp6Stats.u4IcmpInErr,
                       Ip6CliInfo.Icmp6Stats.u4IcmpInDestUnreach,
                       Ip6CliInfo.Icmp6Stats.u4IcmpInTimeExceeds);

            CliPrintf (CliHandle,
                       "%10d ParmProbs    %10d PktTooBigMsg  %10d ICMPEchoReq  %10d ICMPEchoReps \r\n",
                       Ip6CliInfo.Icmp6Stats.u4IcmpInParmProbs,
                       Ip6CliInfo.Icmp6Stats.u4IcmpInToobig,
                       Ip6CliInfo.Icmp6Stats.u4IcmpInEchos,
                       Ip6CliInfo.Icmp6Stats.u4IcmpInEchosReps);

            CliPrintf (CliHandle,
                       "%10d RouterSols   %10d RouterAdv     %10d NeighSols    %10d NeighAdv \r\n",
                       Ip6CliInfo.Icmp6Stats.u4IcmpInRtSol,
                       Ip6CliInfo.Icmp6Stats.u4IcmpInRtAdv,
                       Ip6CliInfo.Icmp6Stats.u4IcmpInNbSol,
                       Ip6CliInfo.Icmp6Stats.u4IcmpInNbAdv);

            CliPrintf (CliHandle,
                       "%10d NA_RFlagSet  %10d NA_SFlagSet   %10d NA_OFlagSet \r\n",
                       u4InNARouterFlagSet, u4InNASolicitedFlagSet,
                       u4InNAOverrideFlagSet);

            CliPrintf (CliHandle,
                       "%10d Redirects    %10d AdminProhib \r\n",
                       Ip6CliInfo.Icmp6Stats.u4IcmpInRedirects,
                       Ip6CliInfo.Icmp6Stats.u4IcmpInDestUnreach);

            CliPrintf (CliHandle, "    Sent    \r\n");
            CliPrintf (CliHandle,
                       "%10d ICMPMsgs     %10d ICMPErrMsgs      %10d DstUnReach   %10d TimeExcds \r\n",
                       Ip6CliInfo.Icmp6Stats.u4IcmpOutMsgs,
                       Ip6CliInfo.Icmp6Stats.u4IcmpOutErr,
                       Ip6CliInfo.Icmp6Stats.u4IcmpOutDestUnreach,
                       Ip6CliInfo.Icmp6Stats.u4IcmpOutTimeExceeds);

            CliPrintf (CliHandle,
                       "%10d ParmProbs    %10d PktTooBigs       %10d EchoReq      %10d EchoReply \r\n",
                       Ip6CliInfo.Icmp6Stats.u4IcmpOutParmProbs,
                       Ip6CliInfo.Icmp6Stats.u4IcmpOutTooBig,
                       Ip6CliInfo.Icmp6Stats.u4IcmpOutEchos,
                       Ip6CliInfo.Icmp6Stats.u4IcmpOutEchosReps);

            CliPrintf (CliHandle,
                       "%10d RouterSols   %10d RouterAdv        %10d NeighSols    %10d NeighborAdv \r\n",
                       Ip6CliInfo.Icmp6Stats.u4IcmpOutRtSol,
                       Ip6CliInfo.Icmp6Stats.u4IcmpOutRtAdv,
                       Ip6CliInfo.Icmp6Stats.u4IcmpOutNbSol,
                       Ip6CliInfo.Icmp6Stats.u4IcmpOutNbAdv);

            CliPrintf (CliHandle,
                       "%10d NA_RFlagSet  %10d NA_SFlagSet      %10d NA_OFlagSet \r\n",
                       u4OutNARouterFlagSet, u4OutNASolicitedFlagSet,
                       u4OutNAOverrideFlagSet);

            CliPrintf (CliHandle,
                       "%10d RedirectMsgs %10d AdminProhibMsgs  %10d Rate-limited \r\n",
                       Ip6CliInfo.Icmp6Stats.u4IcmpOutRedirects,
                       Ip6CliInfo.Icmp6Stats.u4IcmpOutDestUnreach,
                       Ip6CliInfo.Icmp6Stats.u4IcmpOutRateLimit);
        }

        /* Display UDP6 related Information */

        CliPrintf (CliHandle, "\r\n    UDP statistics\r\n");
        CliPrintf (CliHandle, "    **************\r\n");
        if (u1HCFlag == IP6_CLI_HC_DISABLED)
        {

            nmhGetFsMiUdpIpvxInDatagrams (u4VcId, &u4UdpInDgrams);
            nmhGetFsMiUdpIpvxNoPorts (u4VcId, &u4UdpInNoPorts);
            nmhGetFsMiUdpIpvxInErrors (u4VcId, &u4UdpErrPkts);
            nmhGetFsMiUdpIpvxOutDatagrams (u4VcId, &u4UdpOutDgrams);
            CliPrintf (CliHandle, "    Received :    \r\n");
            CliPrintf (CliHandle,
                       "%10d UDPDgrams    %10d UDPNoPorts       %10d UDPErrPkts \r\n",
                       u4UdpInDgrams, u4UdpInNoPorts, u4UdpErrPkts);
            CliPrintf (CliHandle, "    Sent :    \r\n");
            CliPrintf (CliHandle, "%10d UDPDgrams  \r\n", u4UdpOutDgrams);
        }
        else
        {
            /*Display only the High capacity counters */

            MEMSET (au1HCTempCount, IP6_ZERO, CFA_CLI_U8_STR_LENGTH);
            MEMSET (au1HCTempCnt, IP6_ZERO, CFA_CLI_U8_STR_LENGTH);
            nmhGetFsMiUdpIpvxHCInDatagrams (u4VcId, &RetUdpHCInDatagrams);
            nmhGetFsMiUdpIpvxHCOutDatagrams (u4VcId, &RetUdpHCOutDatagrams);

            u8UdpHCInDatagrams.u4Hi = RetUdpHCInDatagrams.msn;
            u8UdpHCInDatagrams.u4Lo = RetUdpHCInDatagrams.lsn;
            u8UdpHCOutDatagrams.u4Hi = RetUdpHCOutDatagrams.msn;
            u8UdpHCOutDatagrams.u4Lo = RetUdpHCOutDatagrams.lsn;

            FSAP_U8_2STR (&u8UdpHCInDatagrams, (CHR1 *) & au1HCTempCount);
            FSAP_U8_2STR (&u8UdpHCOutDatagrams, (CHR1 *) & au1HCTempCnt);
            CliPrintf (CliHandle, "%5s\t%-15s\t%5s\t%-15s\r\n", au1HCTempCount,
                       "HC InDatagrams", au1HCTempCnt, "HC OutDatagrams");
        }

    }
#ifndef LNXIP6_WANTED            /* FSIP */
    while ((u4ShowAllCxt == TRUE) &&
           (Ip6UtilGetNextCxtId (u4PrevVcId, &u4VcId) != IP_FAILURE));
#else
    while (0);
    UNUSED_PARAM (u4PrevVcId);
#endif
    /*end of do while */
    return (CLI_SUCCESS);
}

/*********************************************************************
*  Function Name : Ip6ReachableTime
*  Description   : Setting Reachable Time  
*  Input(s)      : CliHandle
*                  u4Index - Interface Index
*                  u4ReachableTime 
*  Output(s)     :  
*  Return Values : None.
*********************************************************************/

VOID
Ip6ReachableTime (tCliHandle CliHandle, UINT4 u4Index, UINT4 u4ReachableTime)
{
    UINT4               u4ErrorCode;
    INT4                i4RsVal = SNMP_FAILURE;
    INT1                i1RetVal = SNMP_FAILURE;

    if (Ip6IsIfFwdEnabled (u4Index) == FALSE)
    {
        if (Ip6ifEntryExists (u4Index) == IP6_FAILURE)
        {
            CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
            return;
        }

        if (nmhTestv2Fsipv6IfReachableTime
            (&u4ErrorCode, u4Index, u4ReachableTime) == SNMP_FAILURE)
        {
            return;
        }
        if (nmhSetFsipv6IfReachableTime (u4Index, u4ReachableTime) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return;
        }
        return;
    }

    /* IPv6Lock will be taken in the IPvx Lowlevel So Relase Now and
     * before return take */
    IP6_TASK_UNLOCK ();

    /* Router Adv Config */
    i1RetVal = nmhGetIpv6RouterAdvertRowStatus ((INT4) u4Index, &i4RsVal);
    if (i1RetVal == SNMP_FAILURE)
    {
        /* Create a New Entry in the RA Table */
        i4RsVal = CREATE_AND_WAIT;
        if ((nmhTestv2Ipv6RouterAdvertRowStatus (&u4ErrorCode,
                                                 (INT4) u4Index, i4RsVal))
            == SNMP_FAILURE)
        {
            IP6_TASK_LOCK ();
            return;
        }
        if ((nmhSetIpv6RouterAdvertRowStatus ((INT4) u4Index, i4RsVal))
            == SNMP_FAILURE)
        {
            IP6_TASK_LOCK ();
            CLI_FATAL_ERROR (CliHandle);
            return;
        }
    }

    if ((nmhTestv2Ipv6RouterAdvertReachableTime
         (&u4ErrorCode, (INT4) u4Index, u4ReachableTime)) == SNMP_FAILURE)
    {
        IP6_TASK_LOCK ();
        return;
    }

    if ((nmhSetIpv6RouterAdvertReachableTime ((INT4) u4Index, u4ReachableTime))
        == SNMP_FAILURE)
    {
        IP6_TASK_LOCK ();
        CLI_FATAL_ERROR (CliHandle);
        return;
    }
    IP6_TASK_LOCK ();
    return;
}

/*********************************************************************
*  Function Name : Ip6RetransmitTime
*  Description   : Setting Retransmission Time  
*  Input(s)      : CliHandle
*                  u4Index - Interface Index
*                  u4RetransmitTime 
*  Output(s)     :  
*  Return Values : None.
*********************************************************************/

VOID
Ip6RetransmitTime (tCliHandle CliHandle, UINT4 u4Index, UINT4 u4RetransmitTime)
{
    UINT4               u4ErrorCode;
    INT4                i4RsVal = SNMP_FAILURE;
    INT1                i1RetVal = SNMP_FAILURE;

    if (Ip6IsIfFwdEnabled (u4Index) == FALSE)
    {
        if (Ip6ifEntryExists (u4Index) == IP6_FAILURE)
        {
            CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
            return;
        }

        if (nmhTestv2Fsipv6IfRetransmitTime
            (&u4ErrorCode, u4Index, u4RetransmitTime) == SNMP_FAILURE)
        {
            return;
        }

        if (nmhSetFsipv6IfRetransmitTime (u4Index, u4RetransmitTime) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return;
        }
        return;
    }

    /* IPv6Lock will be taken in the IPvx Lowlevel So Relase Now and
     * before return take */
    IP6_TASK_UNLOCK ();

    /* Router Adv Config */
    i1RetVal = nmhGetIpv6RouterAdvertRowStatus ((INT4) u4Index, &i4RsVal);
    if (i1RetVal == SNMP_FAILURE)
    {
        /* Create a New Entry in the RA Table */
        i4RsVal = CREATE_AND_WAIT;
        if ((nmhTestv2Ipv6RouterAdvertRowStatus (&u4ErrorCode,
                                                 (INT4) u4Index, i4RsVal))
            == SNMP_FAILURE)
        {
            IP6_TASK_LOCK ();
            return;
        }
        if ((nmhSetIpv6RouterAdvertRowStatus ((INT4) u4Index, i4RsVal))
            == SNMP_FAILURE)
        {
            IP6_TASK_LOCK ();
            CLI_FATAL_ERROR (CliHandle);
            return;
        }
    }

    if ((nmhTestv2Ipv6RouterAdvertRetransmitTime
         (&u4ErrorCode, (INT4) u4Index, u4RetransmitTime)) == SNMP_FAILURE)
    {
        IP6_TASK_LOCK ();
        return;
    }

    if ((nmhSetIpv6RouterAdvertRetransmitTime ((INT4) u4Index,
                                               u4RetransmitTime)) ==
        SNMP_FAILURE)
    {
        IP6_TASK_LOCK ();
        CLI_FATAL_ERROR (CliHandle);
        return;
    }
    IP6_TASK_LOCK ();

    return;
}

/*********************************************************************
*  Function Name : CliSetIp6RALinkMTU
*  Description   : Setting RA Link MTU  
*  Input(s)      : CliHandle
*                  i4Index - Interface Index
*                  u4LinkMTU 
*  Output(s)     :  
*  Return Values : None.
*********************************************************************/
VOID
CliSetIp6RALinkMTU (tCliHandle CliHandle, INT4 i4Index, UINT4 u4LinkMTU)
{
    UINT4               u4ErrorCode = SNMP_FAILURE;
    INT4                i4RsVal = SNMP_FAILURE;
    INT1                i1RetVal = SNMP_FAILURE;

    /* Router Adv Config */
    i1RetVal = nmhGetIpv6RouterAdvertRowStatus (i4Index, &i4RsVal);
    if (i1RetVal == SNMP_FAILURE)
    {
        /* Create a New Entry in the RA Table */
        i4RsVal = CREATE_AND_WAIT;
        if ((nmhTestv2Ipv6RouterAdvertRowStatus (&u4ErrorCode, i4Index,
                                                 i4RsVal)) == SNMP_FAILURE)
        {
            return;
        }
        if ((nmhSetIpv6RouterAdvertRowStatus (i4Index, i4RsVal))
            == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return;
        }
    }

    if ((nmhTestv2Ipv6RouterAdvertLinkMTU
         (&u4ErrorCode, i4Index, u4LinkMTU)) == SNMP_FAILURE)
    {
        return;
    }

    if ((nmhSetIpv6RouterAdvertLinkMTU (i4Index, u4LinkMTU)) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return;
    }
    return;
}

/*********************************************************************
*  Function Name : Ip6RouteAdvInterval
*  Description   : Configuration of Router Advertisement Interval 
*  Input(s)      : CliHandle
*                  u4Index - Interface Index
*                  u4MinRAInterval - Default value 3 sec
*                  u4MaxRAInterval - Interval between two RAs
*  Output(s)     :  
*  Return Values : None.
*********************************************************************/

VOID
Ip6RouteAdvInterval (tCliHandle CliHandle,
                     UINT4 u4Index,
                     UINT4 u4MinRAInterval, UINT4 u4MaxRAInterval)
{
    UINT4               u4ErrorCode;

    if (Ip6IsIfFwdEnabled (u4Index) == FALSE)
    {
        INT4                i4CurMaxRa = 0;
        INT4                i4CurMinRa = 0;
        UINT4               u4IsMaxFirst;

        if (Ip6ifEntryExists (u4Index) == IP6_FAILURE)
        {
            CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
            return;
        }

        if (u4MinRAInterval != IP6_ND_INVLID_INTERVAL)
        {
            if (u4MaxRAInterval <= u4MinRAInterval)
            {
                /* Invalid Values */
                CLI_SET_ERR (CLI_IP6_INVALID_RA_INTERVAL);
                return;
            }
        }

        /* Get the Current MIN and MAX RA interval */
        nmhGetFsipv6IfMaxRouterAdvTime (u4Index, &i4CurMaxRa);
        nmhGetFsipv6IfMinRouterAdvTime (u4Index, &i4CurMinRa);

        /* Verify whether we should first set MIN RA interval or MAX RA interval */
        if ((i4CurMinRa > (INT4) u4MaxRAInterval) ||
            (i4CurMaxRa > (INT4) u4MinRAInterval))
        {
            /* Under these case first Set MIN RA interval and then set
             * MAX RA interval */
            u4IsMaxFirst = OSIX_FALSE;
        }
        else
        {
            /* Under these case first Set MAX RA interval and then set
             * MIN RA interval */
            u4IsMaxFirst = OSIX_TRUE;
        }

        if (u4IsMaxFirst == OSIX_TRUE)
        {
            /* Set MAX RA interval */
            if (nmhTestv2Fsipv6IfMaxRouterAdvTime
                (&u4ErrorCode, u4Index, u4MaxRAInterval) == SNMP_FAILURE)
            {
                return;
            }

            if (nmhSetFsipv6IfMaxRouterAdvTime (u4Index, u4MaxRAInterval) ==
                SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return;
            }
        }

        if (u4MinRAInterval != IP6_ND_INVLID_INTERVAL)
        {
            /* Set MIN RA interval */
            if (nmhTestv2Fsipv6IfMinRouterAdvTime
                (&u4ErrorCode, u4Index, u4MinRAInterval) == SNMP_FAILURE)
            {
                return;
            }

            if (nmhSetFsipv6IfMinRouterAdvTime (u4Index, u4MinRAInterval) ==
                SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return;
            }
        }

        if (u4IsMaxFirst == OSIX_FALSE)
        {
            /* Set MAX RA interval */
            if (nmhTestv2Fsipv6IfMaxRouterAdvTime
                (&u4ErrorCode, u4Index, u4MaxRAInterval) == SNMP_FAILURE)
            {
                return;
            }

            if (nmhSetFsipv6IfMaxRouterAdvTime (u4Index, u4MaxRAInterval) ==
                SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return;
            }
        }
    }
    else
    {
        UINT4               u4CurrMinRAInterval = 0;
        INT4                i4RsVal = SNMP_FAILURE;
        INT1                i1RetVal = SNMP_FAILURE;

        /* IPv6Lock will be taken in the IPvx Lowlevel So Relase Now and
         * before return take */
        IP6_TASK_UNLOCK ();

        /* Router Adv Config */
        i1RetVal = nmhGetIpv6RouterAdvertRowStatus ((INT4) u4Index, &i4RsVal);
        if (i1RetVal == SNMP_FAILURE)
        {
            /* Create a New Entry in the RA Table */
            i4RsVal = CREATE_AND_WAIT;
            if ((nmhTestv2Ipv6RouterAdvertRowStatus (&u4ErrorCode,
                                                     (INT4) u4Index, i4RsVal))
                == SNMP_FAILURE)
            {
                IP6_TASK_LOCK ();
                return;
            }
            if ((nmhSetIpv6RouterAdvertRowStatus ((INT4) u4Index, i4RsVal))
                == SNMP_FAILURE)
            {
                IP6_TASK_LOCK ();
                CLI_FATAL_ERROR (CliHandle);
                return;
            }
        }

        if (u4MinRAInterval == IP6_ND_INVLID_INTERVAL)
        {
            nmhGetIpv6RouterAdvertMinInterval ((INT4) u4Index,
                                               &u4CurrMinRAInterval);
            if ((nmhTestv2Ipv6RouterAdvertMaxInterval
                 (&u4ErrorCode, (INT4) u4Index,
                  u4MaxRAInterval)) == SNMP_SUCCESS)
            {
                if (u4CurrMinRAInterval <=
                    ((IP6_THREE * u4MaxRAInterval) / IP6_FOUR) &&
                    (u4CurrMinRAInterval >= IP6_IF_MIN_RA_MIN_TIME))
                {
                    if ((nmhSetIpv6RouterAdvertMaxInterval ((INT4) u4Index,
                                                            u4MaxRAInterval)) ==
                        SNMP_FAILURE)
                    {
                        IP6_TASK_LOCK ();
                        CLI_FATAL_ERROR (CliHandle);
                        return;
                    }
                }
                else
                {
                    CliPrintf (CliHandle,
                               "Current minimum ra-interval is %d and it is greater than 3/4 of Max ra-interval %d\r\n",
                               u4CurrMinRAInterval, u4MaxRAInterval);
                    IP6_TASK_LOCK ();
                    return;
                }
            }
        }
        else
        {
            nmhGetIpv6RouterAdvertMinInterval ((INT4) u4Index,
                                               &u4CurrMinRAInterval);
            if ((nmhTestv2Ipv6RouterAdvertMinInterval
                 (&u4ErrorCode, (INT4) u4Index,
                  u4MinRAInterval)) == SNMP_FAILURE)
            {
                /* When the given min RA interval is not greater than 3/4 of 
                 * the given max RA interval configure both the values */
                if ((u4ErrorCode != SNMP_ERR_WRONG_VALUE)
                    || (u4MinRAInterval < IP6_IF_MIN_RA_MIN_TIME))
                {
                    IP6_TASK_LOCK ();
                    return;
                }
                else if (u4MinRAInterval >
                         ((IP6_THREE * u4MaxRAInterval) / IP6_FOUR))
                {
                    IP6_TASK_LOCK ();
                    return;
                }
                else
                {
                    CLI_SET_ERR (0);
                    if ((nmhTestv2Ipv6RouterAdvertMaxInterval
                         (&u4ErrorCode, (INT4) u4Index,
                          u4MaxRAInterval)) == SNMP_SUCCESS)
                    {
                        if ((nmhSetIpv6RouterAdvertMaxInterval
                             ((INT4) u4Index, u4MaxRAInterval)) == SNMP_SUCCESS)
                        {
                            nmhSetIpv6RouterAdvertMinInterval ((INT4) u4Index,
                                                               u4MinRAInterval);
                            IP6_TASK_LOCK ();
                            return;
                        }
                    }
                }
            }
            /* also verify with the current configured max RA interval */
            else if (u4MinRAInterval >
                     ((IP6_THREE * u4MaxRAInterval) / IP6_FOUR))
            {
                IP6_TASK_LOCK ();
                CLI_SET_ERR (CLI_IP6_INVALID_MIN_RA_INTERVAL);
                return;
            }
            if ((nmhTestv2Ipv6RouterAdvertMaxInterval
                 (&u4ErrorCode, (INT4) u4Index,
                  u4MaxRAInterval)) == SNMP_SUCCESS)
            {
                if ((nmhSetIpv6RouterAdvertMinInterval
                     ((INT4) u4Index, u4MinRAInterval)) == SNMP_SUCCESS)
                {
                    if ((nmhSetIpv6RouterAdvertMaxInterval
                         ((INT4) u4Index, u4MaxRAInterval)) == SNMP_FAILURE)
                    {
                        if (u4CurrMinRAInterval != 0)
                        {
                            nmhSetIpv6RouterAdvertMinInterval ((INT4) u4Index,
                                                               u4CurrMinRAInterval);
                        }
                        IP6_TASK_LOCK ();
                        CLI_FATAL_ERROR (CliHandle);
                        return;
                    }
                }
            }
        }
        IP6_TASK_LOCK ();
    }
    return;
}

/*********************************************************************
*  Function Name : Ip6TraceInCxt 
*  Description   : Enabling Ipv6 Trace
*  Input(s)      : CliHandle
*                  u4DbgMap -  value to set the type of trace
*                  u4ContextId - Context Identifier
*  Output(s)     :  
*  Return Values : None.
*********************************************************************/

VOID
Ip6TraceInCxt (tCliHandle CliHandle, UINT4 u4DbgMap, UINT4 u4ContextId)
{
    UINT4               u4TraceVal = 0;

    if (u4ContextId == VCM_DEFAULT_CONTEXT)
    {
        nmhGetFsMIIpv6GlobalDebug (&u4TraceVal);
    }
    else
    {
        nmhGetFsMIIpv6ContextDebug (u4ContextId, &u4TraceVal);
    }

    switch (u4DbgMap)
    {
        case IP6_CLI_MOD_TRC:
            u4TraceVal |= IP6_MOD_TRC;
            break;
        case ICMP6_CLI_MOD_TRC:
            u4TraceVal |= ICMP6_MOD_TRC;
            break;
        case UDP6_CLI_MOD_TRC:
            u4TraceVal |= UDP6_MOD_TRC;
            break;
        case ND6_CLI_MOD_TRC:
            u4TraceVal |= ND6_MOD_TRC;
            break;
        case PING6_CLI_MOD_TRC:
            u4TraceVal |= PING6_MOD_TRC;
            break;
        case V6_OVER_V4_CLI_MOD_TRC:
            u4TraceVal |= V6_OVER_V4_TUNL;
            break;
        case MIP6_CLI_MOD_TRC:
            u4TraceVal |= MIP6_MOD_TRC;
            break;
        case IP6_CLI_NO_MOD_TRC:
            u4TraceVal &= IP6_NO_MOD_TRC;
            break;
        default:
            CLI_FATAL_ERROR (CliHandle);
            return;
    }

    if (u4ContextId == VCM_DEFAULT_CONTEXT)
    {
        nmhSetFsMIIpv6GlobalDebug (u4TraceVal);
    }
    else
    {
        nmhSetFsMIIpv6ContextDebug (u4ContextId, u4TraceVal);
    }
    return;
}

/*********************************************************************
*  Function Name : Ip6SetPrefixList
*  Description   : set the Prefix Attributes
*  Input(s)      : pu1InMsg - with the Command,follwed by the user inputs.
*  Output(s)     : ppRespMsg - filled up with error Message, if any 
*  Return Values : None.
*********************************************************************/
INT4
Ip6SetPrefixList (tCliHandle CliHandle,
                  UINT4 u4ProfileIndex,
                  UINT4 u4ValidTime,
                  UINT1 u1ValidTimeFlag,
                  UINT4 u4PrefTime,
                  UINT1 u1PrefTimeFlag,
                  UINT1 u1AdvtStatus,
                  UINT1 u1OnLinkStatus, UINT1 u1AutoConfigStatus)
{
    UINT4               u4ErrorCode = 0;

    if ((u4ValidTime < u4PrefTime) && ((u4ValidTime != 0) || (u4PrefTime != 0)))
    {
        CLI_SET_ERR (CLI_IP6_VAL_LT_PREF_LIFETIME);
        return IP6_FAILURE;
    }

    if ((u4ValidTime == 0) && (u4PrefTime == 0) && (u4ProfileIndex != 0))
    {
        u4ValidTime = IP6_ADDR_PROF_DEF_VALID_LIFE;
        u4PrefTime = IP6_ADDR_PROF_DEF_PREF_LIFE;
    }

    if (nmhTestv2Fsipv6AddrProfileStatus
        (&u4ErrorCode, u4ProfileIndex, IP6_ADDR_PROF_VALID) == SNMP_FAILURE)
    {
        return IP6_FAILURE;
    }

    nmhSetFsipv6AddrProfileStatus (u4ProfileIndex, IP6_ADDR_PROF_VALID);

    if (nmhTestv2Fsipv6AddrProfilePrefixAdvStatus
        (&u4ErrorCode, u4ProfileIndex, u1AdvtStatus) == SNMP_FAILURE)
    {
        nmhSetFsipv6AddrProfileStatus (u4ProfileIndex, IP6_ADDR_PROF_INVALID);
        return IP6_FAILURE;
    }

    if (nmhSetFsipv6AddrProfilePrefixAdvStatus
        (u4ProfileIndex, u1AdvtStatus) == SNMP_FAILURE)
    {
        nmhSetFsipv6AddrProfileStatus (u4ProfileIndex, IP6_ADDR_PROF_INVALID);
        CLI_FATAL_ERROR (CliHandle);
        return IP6_FAILURE;
    }

    if (nmhTestv2Fsipv6AddrProfileAutoConfAdvStatus
        (&u4ErrorCode, u4ProfileIndex, u1AutoConfigStatus) == SNMP_FAILURE)
    {
        nmhSetFsipv6AddrProfileStatus (u4ProfileIndex, IP6_ADDR_PROF_INVALID);
        return IP6_FAILURE;
    }

    if (nmhSetFsipv6AddrProfileAutoConfAdvStatus
        (u4ProfileIndex, u1AutoConfigStatus) == SNMP_FAILURE)
    {
        nmhSetFsipv6AddrProfileStatus (u4ProfileIndex, IP6_ADDR_PROF_INVALID);
        CLI_FATAL_ERROR (CliHandle);
        return IP6_FAILURE;
    }

    if (nmhSetFsipv6AddrProfilePreferredTime
        (u4ProfileIndex, u4PrefTime) == SNMP_FAILURE)
    {
        nmhSetFsipv6AddrProfileStatus (u4ProfileIndex, IP6_ADDR_PROF_INVALID);
        CLI_FATAL_ERROR (CliHandle);
        return IP6_FAILURE;
    }
    /* Onlinkstatus will be set only if user sets the variable via cli-command as 0n-link or off-link.
     *Default value is on-link.To set on-link RFC5942 must be enabled and preferred lifetime must not be zero*/
    if (u1OnLinkStatus != IP6_ZERO)
    {
        if (nmhTestv2Fsipv6AddrProfileOnLinkAdvStatus
            (&u4ErrorCode, u4ProfileIndex, u1OnLinkStatus) == SNMP_FAILURE)
        {
            nmhSetFsipv6AddrProfileStatus (u4ProfileIndex,
                                           IP6_ADDR_PROF_INVALID);
            return IP6_FAILURE;
        }

        if (nmhSetFsipv6AddrProfileOnLinkAdvStatus
            (u4ProfileIndex, u1OnLinkStatus) == SNMP_FAILURE)
        {
            nmhSetFsipv6AddrProfileStatus (u4ProfileIndex,
                                           IP6_ADDR_PROF_INVALID);
            CLI_FATAL_ERROR (CliHandle);
            return IP6_FAILURE;
        }
    }

    if (nmhSetFsipv6AddrProfileValidTime
        (u4ProfileIndex, u4ValidTime) == SNMP_FAILURE)
    {
        nmhSetFsipv6AddrProfileStatus (u4ProfileIndex, IP6_ADDR_PROF_INVALID);
        CLI_FATAL_ERROR (CliHandle);
        return IP6_FAILURE;
    }

    if (nmhTestv2Fsipv6AddrProfileValidLifeTimeFlag
        (&u4ErrorCode, u4ProfileIndex, u1ValidTimeFlag) == SNMP_FAILURE)
    {
        nmhSetFsipv6AddrProfileStatus (u4ProfileIndex, IP6_ADDR_PROF_INVALID);
        return IP6_FAILURE;
    }

    if (nmhSetFsipv6AddrProfileValidLifeTimeFlag
        (u4ProfileIndex, u1ValidTimeFlag) == SNMP_FAILURE)
    {
        nmhSetFsipv6AddrProfileStatus (u4ProfileIndex, IP6_ADDR_PROF_INVALID);
        CLI_FATAL_ERROR (CliHandle);
        return IP6_FAILURE;
    }

    if (nmhTestv2Fsipv6AddrProfilePreferredLifeTimeFlag
        (&u4ErrorCode, u4ProfileIndex, u1PrefTimeFlag) == SNMP_FAILURE)
    {
        nmhSetFsipv6AddrProfileStatus (u4ProfileIndex, IP6_ADDR_PROF_INVALID);
        return IP6_FAILURE;
    }

    if (nmhSetFsipv6AddrProfilePreferredLifeTimeFlag
        (u4ProfileIndex, u1PrefTimeFlag) == SNMP_FAILURE)
    {
        nmhSetFsipv6AddrProfileStatus (u4ProfileIndex, IP6_ADDR_PROF_INVALID);
        CLI_FATAL_ERROR (CliHandle);
        return IP6_FAILURE;
    }
    return IP6_SUCCESS;
}

/*********************************************************************
*  Function Name : Ip6DelPrefixList
*  Description   : Delete a Prefix profile
*  Input(s)      : CliHandle
*                  u4ProfileIndex - Profile to be deleted
*  Output(s)     : ppRespMsg - filled up with error Message, if any
*  Return Values : None.
*********************************************************************/
INT4
Ip6DelPrefixList (tCliHandle CliHandle, UINT4 u4ProfileIndex)
{
    UINT4               u4ErrorCode;

    if (u4ProfileIndex == 0)
    {
        CLI_SET_ERR (CLI_IP6_DEF_PROF_INDEX);
        return IP6_FAILURE;
    }
    if (nmhTestv2Fsipv6AddrProfileStatus
        (&u4ErrorCode, u4ProfileIndex, IP6_ADDR_PROF_INVALID) == SNMP_FAILURE)
    {
        return IP6_FAILURE;
    }

    if (nmhSetFsipv6AddrProfileStatus
        (u4ProfileIndex, IP6_ADDR_PROF_INVALID) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return IP6_FAILURE;
    }
#ifndef LNXIP6_WANTED            /* FSIP */
    if (u4ProfileIndex < gIp6GblInfo.u4NextProfileIndex)
    {
        gIp6GblInfo.u4NextProfileIndex = u4ProfileIndex;
    }
#endif

    return IP6_SUCCESS;
}

/*********************************************************************
*  Function Name : Ip6SetPrefix
*  Description   : set the Prefix to be advertised in RA
*  Input(s)      : pu1InMsg - with the Command,follwed by the user inputs.
*  Output(s)     : ppRespMsg - filled up with error Message, if any
*  Return Values : None.
*********************************************************************/
VOID
Ip6SetPrefix (tCliHandle CliHandle,
              UINT4 u4IfIndex,
              tIp6Addr Ip6Prefix,
              INT4 i4PrefixLen,
              UINT4 u4ValidTime,
              UINT1 u1PrefixValidTimeFlag,
              UINT4 u4PreferredTime,
              UINT1 u1PrefixPrefTimeFlag,
              UINT1 u1NoAdvertiseFlag,
              UINT1 u1OnLinkFlag, UINT1 u1NoAutoConfigFlag,
              UINT1 u1SupportEmbeddedRp, INT4 i4DefaultFlag)
{
    tSNMP_OCTET_STRING_TYPE PrefixAddr;
    UINT4               u4ErrorCode = 0;
    INT4                i4AdminStaus = 0;
    INT1                i1Status = 0;
    UINT1               au1PrefixAddrOctetList[IP6_ADDR_SIZE];
    UINT4               u4ProfileIndex = 0;
#ifndef LNXIP6_WANTED
    UINT4               u4Count = 0;
#endif
    MEMSET (au1PrefixAddrOctetList, IP6_ZERO, IP6_ADDR_SIZE);

    if (!IS_ADDR_UNSPECIFIED (Ip6Prefix))
    {
        PrefixAddr.pu1_OctetList = au1PrefixAddrOctetList;
        Ip6CopyAddrBits ((tIp6Addr *) (VOID *) PrefixAddr.pu1_OctetList,
                         &Ip6Prefix, i4PrefixLen);
        PrefixAddr.i4_Length = IP6_ADDR_SIZE;

        i1Status = nmhGetFsipv6PrefixAdminStatus
            ((INT4) u4IfIndex, &PrefixAddr, i4PrefixLen, &i4AdminStaus);

        if (i1Status == SNMP_FAILURE)
        {
            if (nmhTestv2Fsipv6PrefixAdminStatus
                (&u4ErrorCode, (INT4) u4IfIndex,
                 &PrefixAddr, i4PrefixLen,
                 IP6FWD_CREATE_AND_WAIT) == SNMP_FAILURE)
            {
                return;
            }

            if (nmhSetFsipv6PrefixAdminStatus
                ((INT4) u4IfIndex,
                 &PrefixAddr, i4PrefixLen,
                 IP6FWD_CREATE_AND_WAIT) == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return;
            }
        }
        if (i4DefaultFlag == 1)
        {
            u4ProfileIndex = 0;
        }

        else
        {
#ifndef LNXIP6_WANTED
            if (gIp6GblInfo.u4NextProfileIndex
                != gIp6GblInfo.u4MaxAssignedProfileIndex)
            {
                for (u4Count = gIp6GblInfo.u4NextProfileIndex;
                     u4Count < gIp6GblInfo.u4MaxAssignedProfileIndex; u4Count++)
                {
                    if (IP6_ADDR_PROF_REF_COUNT (u4Count) == 0)
                    {
                        break;
                    }
                }
                gIp6GblInfo.u4NextProfileIndex = u4Count;
            }
            else
            {
                gIp6GblInfo.u4NextProfileIndex =
                    gIp6GblInfo.u4MaxAssignedProfileIndex;
                gIp6GblInfo.u4MaxAssignedProfileIndex++;
            }

            u4ProfileIndex = gIp6GblInfo.u4NextProfileIndex;
#else
            /*check wheather any profileindex is present for the given prefix if present use that profileindex */
            if (nmhGetFsipv6PrefixProfileIndex ((INT4) u4IfIndex, &PrefixAddr,
                                                i4PrefixLen,
                                                (INT4 *) &u4ProfileIndex) ==
                SNMP_SUCCESS)
            {
                /*if profileindex not found create a new profileindex for the prefix */
                if (u4ProfileIndex == 0)
                {
                    u4ProfileIndex = gIp6GblInfo.u4MaxAssignedProfileIndex;
                    gIp6GblInfo.u4MaxAssignedProfileIndex++;
                }
            }
            if (nmhTestv2Fsipv6AddrProfIndex (&u4ErrorCode, (INT4) u4IfIndex,
                                              &PrefixAddr,
                                              i4PrefixLen,
                                              u4ProfileIndex) == SNMP_FAILURE)
            {
                nmhSetFsipv6AddrAdminStatus ((INT4) u4IfIndex, &PrefixAddr,
                                             i4PrefixLen, DESTROY);
                return;
            }
            /*set the profileindex for prefix -addresstable */
            if (nmhSetFsipv6AddrProfIndex ((INT4) u4IfIndex,
                                           &PrefixAddr, i4PrefixLen,
                                           u4ProfileIndex) == SNMP_FAILURE)
            {
                nmhSetFsipv6AddrAdminStatus ((INT4) u4IfIndex, &PrefixAddr,
                                             i4PrefixLen, DESTROY);
                CLI_FATAL_ERROR (CliHandle);
                return;
            }

#endif
        }
        /*set the profiletable vaule for the  profileindex */
        if (Ip6SetPrefixList (CliHandle, u4ProfileIndex, u4ValidTime,
                              u1PrefixValidTimeFlag, u4PreferredTime,
                              u1PrefixPrefTimeFlag, u1NoAdvertiseFlag,
                              u1OnLinkFlag, u1NoAutoConfigFlag) == IP6_FAILURE)
        {
            return;
        }
        if (nmhTestv2Fsipv6PrefixProfileIndex
            (&u4ErrorCode, (INT4) u4IfIndex,
             &PrefixAddr, i4PrefixLen, (INT4) u4ProfileIndex) == SNMP_FAILURE)
        {
            if (i1Status == SNMP_FAILURE)
            {
                nmhSetFsipv6PrefixAdminStatus
                    ((INT4) u4IfIndex,
                     &PrefixAddr, i4PrefixLen, IP6FWD_DESTROY);
            }
            return;
        }

        if (nmhSetFsipv6PrefixProfileIndex
            ((INT4) u4IfIndex, &PrefixAddr,
             i4PrefixLen, (INT4) u4ProfileIndex) == SNMP_FAILURE)
        {
            if (i1Status == SNMP_FAILURE)
            {
                nmhSetFsipv6PrefixAdminStatus
                    ((INT4) u4IfIndex,
                     &PrefixAddr, i4PrefixLen, IP6FWD_DESTROY);
            }
            CLI_FATAL_ERROR (CliHandle);
            return;
        }

        if (nmhTestv2Fsipv6PrefixAdminStatus
            (&u4ErrorCode, (INT4) u4IfIndex,
             &PrefixAddr, i4PrefixLen, IP6FWD_ACTIVE) == SNMP_FAILURE)
        {
            if (i1Status == SNMP_FAILURE)
            {
                nmhSetFsipv6PrefixAdminStatus
                    ((INT4) u4IfIndex,
                     &PrefixAddr, i4PrefixLen, IP6FWD_DESTROY);
                return;
            }
        }

        if ((u1SupportEmbeddedRp == IP6_SUPPORT_EMBD_RP_ENABLE) ||
            (u1SupportEmbeddedRp == IP6_SUPPORT_EMBD_RP_DISABLE))
        {
            if (nmhSetFsipv6SupportEmbeddedRp ((INT4) u4IfIndex,
                                               &PrefixAddr, i4PrefixLen,
                                               u1SupportEmbeddedRp) ==
                SNMP_FAILURE)
            {

                if (i1Status == SNMP_FAILURE)
                {
                    nmhSetFsipv6PrefixAdminStatus
                        ((INT4) u4IfIndex,
                         &PrefixAddr, i4PrefixLen, IP6FWD_DESTROY);
                    CLI_FATAL_ERROR (CliHandle);
                    return;
                }
            }
        }

        if (nmhSetFsipv6PrefixAdminStatus
            ((INT4) u4IfIndex,
             &PrefixAddr, i4PrefixLen, IP6FWD_ACTIVE) == SNMP_FAILURE)
        {
            if (i1Status == SNMP_FAILURE)
            {
                nmhSetFsipv6PrefixAdminStatus
                    ((INT4) u4IfIndex,
                     &PrefixAddr, i4PrefixLen, IP6FWD_DESTROY);
                CLI_FATAL_ERROR (CliHandle);
                return;
            }
        }
    }

    else
    {
        /* Default Profile values are to be modified with user input values */
        if (i4DefaultFlag == 0)
        {
            CLI_FATAL_ERROR (CliHandle);
            return;
        }

        u4ProfileIndex = 0;
        if (Ip6SetPrefixList (CliHandle, u4ProfileIndex, u4ValidTime,
                              u1PrefixValidTimeFlag, u4PreferredTime,
                              u1PrefixPrefTimeFlag, u1NoAdvertiseFlag,
                              u1OnLinkFlag, u1NoAutoConfigFlag) == IP6_FAILURE)
        {
            return;
        }
    }

    return;
}

/*********************************************************************
*  Function Name : Ip6DelPrefix
*  Description   : Deletes the Prefix advertised in RA
*  Input(s)      : CliHandle
*                  u4IfIndex - Interface Index
*                  Ip6Prefix - Prefix to be deleted
*                  i4PrefixLen - Prefix length
*  Output(s)     :  
*  Return Values : None.
*********************************************************************/
VOID
Ip6DelPrefix (tCliHandle CliHandle,
              UINT4 u4IfIndex, tIp6Addr Ip6Prefix, INT4 i4PrefixLen)
{
    tSNMP_OCTET_STRING_TYPE PrefixAddr;
    UINT4               u4ErrorCode = 0;
    INT4                i4ProfileIndex = 0;
    UINT1               au1PrefixAddrOctetList[IP6_ADDR_SIZE];

    MEMSET (au1PrefixAddrOctetList, IP6_ZERO, IP6_ADDR_SIZE);

    if (!IS_ADDR_UNSPECIFIED (Ip6Prefix))
    {
        PrefixAddr.pu1_OctetList = au1PrefixAddrOctetList;
        Ip6CopyAddrBits ((tIp6Addr *) (VOID *) PrefixAddr.pu1_OctetList,
                         &Ip6Prefix, i4PrefixLen);
        PrefixAddr.i4_Length = IP6_ADDR_SIZE;

        if (nmhTestv2Fsipv6PrefixAdminStatus
            (&u4ErrorCode, (INT4) u4IfIndex,
             &PrefixAddr, i4PrefixLen, IP6FWD_DESTROY) == SNMP_FAILURE)
        {
            return;
        }

        if (nmhGetFsipv6PrefixProfileIndex
            ((INT4) u4IfIndex,
             &PrefixAddr, i4PrefixLen, &i4ProfileIndex) == SNMP_FAILURE)
        {
            CLI_SET_ERR (CLI_IP6_PREFIX_NOT_FOUND);
            return;
        }

        if (nmhSetFsipv6PrefixAdminStatus
            ((INT4) u4IfIndex,
             &PrefixAddr, i4PrefixLen, IP6FWD_DESTROY) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return;
        }

        if (i4ProfileIndex != 0)    /* Should Not DELETE  Default Prefixlist */
        {
            if (Ip6DelPrefixList (CliHandle, (UINT4) i4ProfileIndex) != SUCCESS)
            {
                return;
            }
        }
    }
    else
    {
        i4ProfileIndex = 0;

        if (Ip6SetPrefixList (CliHandle, (UINT4) i4ProfileIndex,
                              IP6_ADDR_PROF_DEF_VALID_LIFE,
                              IP6_FIXED_TIME,
                              IP6_ADDR_PROF_DEF_PREF_LIFE,
                              IP6_FIXED_TIME,
                              IP6_ADDR_PROF_PREF_ADV_ON,
                              IP6_ADDR_PROF_ONLINK_ADV_ON,
                              IP6_ADDR_PROF_AUTO_ADV_ON) == IP6_FAILURE)
        {
            return;
        }
    }
    return;
}

/*********************************************************************
*  Function Name : Ip6ShowGetPrefix
*  Description   : Retrives the Route Entries to be displayed. 
*  Input(s)      : pu1Buffer - non-NULL buffer, where the routes are returned.
*                              This buffer must be big enough to hold
*                              atleast one route.
*                  u4BufLen - Length of pu1Buffer
*                  pCookie  - information about route to be fetched are
*                             stored.
*  Output(s)     : pCookie  - information about next route to be fetched are
*                             stored.
*  Return Values : None.
*********************************************************************/
PRIVATE INT4
Ip6ShowGetPrefix (UINT1 *pu1Buffer, UINT4 u4BufLen,
                  tShowIp6PrefixCookie * pCookie)
{
    tShowIp6PrefixAttr *pShowIp6PrefixAttr = NULL;
    tShowIp6Prefix     *pShowIp6Prefix = NULL;
#ifndef LNXIP6_WANTED
    tIp6PrefixNode     *pPrefixInfo = NULL;
    tIp6PrefixNode     *pNextPrefixInfo = NULL;
#else
    tLip6PrefixNode    *pPrefixInfo = NULL;
    tLip6PrefixNode    *pNextPrefixInfo = NULL;
#endif
    UINT4               u4NoOfPrefix = 0;
    UINT4               u4ProfileIndex = 0;
    INT4                i4Status = 0;

    pShowIp6Prefix = (tShowIp6Prefix *) (VOID *) pu1Buffer;
    pShowIp6PrefixAttr = &pShowIp6Prefix->aPrefixAttr[0];

    MEMSET (pu1Buffer, 0, u4BufLen);

    /* check for valid u4Cookie; if cookie is invalid, then this is a fresh
     * call to Ip6ShowGetPrefix - start from the begining. If cookie is
     * valid, then use to cookie to continue from where we left last time
     */
#ifndef LNXIP6_WANTED
    if ((IS_ADDR_UNSPECIFIED (pCookie->Prefix)) && (pCookie->u1PrefixLen == 0))
    {
        /* Should start from the first entry. */
        pPrefixInfo = Ip6GetFirstPrefix (pCookie->u4IfIndex);
    }
    else
    {
        /* Should Start from the specified entry. */
        pPrefixInfo = Ip6GetRAPrefix (pCookie->u4IfIndex, &pCookie->Prefix,
                                      pCookie->u1PrefixLen);
    }
#else
    if ((IS_ADDR_UNSPECIFIED (pCookie->Prefix)) && (pCookie->u1PrefixLen == 0))
    {
        /* Should start from the first entry. */
        pPrefixInfo = Lip6PrefixGetFirst (pCookie->u4IfIndex);
    }
    else
    {
        /* Should Start from the specified entry. */
        pPrefixInfo = Lip6PrefixGetEntry (pCookie->u4IfIndex, &pCookie->Prefix,
                                          pCookie->u1PrefixLen);
    }
#endif
    /* calculate the number of neighbors that could be accomodated into this
     * buffer
     */
    u4NoOfPrefix = (u4BufLen - ((sizeof (tShowIp6Prefix)) -
                                (sizeof (tShowIp6PrefixAttr)))) /
        (sizeof (tShowIp6PrefixAttr));

    while (pPrefixInfo)
    {
        if (u4NoOfPrefix == 0)
        {
            /* Update the Cookie field and return. */
            Ip6AddrCopy (&pCookie->Prefix, &pPrefixInfo->Ip6Prefix);
#ifndef LNXIP6_WANTED
            pCookie->u1PrefixLen = pPrefixInfo->u1PrefixLen;

#else
            pCookie->u1PrefixLen = pPrefixInfo->i4PrefixLen;
#endif
            pCookie->u4IfIndex = pPrefixInfo->u4IfIndex;
            return IP6_SUCCESS;
        }

        /* Copy the entry into the given buffer. */
        Ip6AddrCopy (&pShowIp6PrefixAttr->Prefix, &pPrefixInfo->Ip6Prefix);
#ifndef LNXIP6_WANTED
        pShowIp6PrefixAttr->u1PrefixLen = pPrefixInfo->u1PrefixLen;
#else
        pShowIp6PrefixAttr->u1PrefixLen = pPrefixInfo->i4PrefixLen;
#endif
        pShowIp6PrefixAttr->u4IfIndex = pPrefixInfo->u2ProfileIndex;

        u4ProfileIndex = pPrefixInfo->u2ProfileIndex;
        nmhGetFsipv6AddrProfileValidTime (u4ProfileIndex,
                                          &pShowIp6PrefixAttr->u4ValidTime);

        nmhGetFsipv6AddrProfilePreferredTime (u4ProfileIndex,
                                              &pShowIp6PrefixAttr->u4PrefTime);

        i4Status = 0;
        nmhGetFsipv6AddrProfileOnLinkAdvStatus (u4ProfileIndex, &i4Status);
        pShowIp6PrefixAttr->u1OnLinkStatus = (UINT1) i4Status;

        i4Status = 0;
        nmhGetFsipv6AddrProfileAutoConfAdvStatus (u4ProfileIndex, &i4Status);
        pShowIp6PrefixAttr->u1AutoConfigStatus = (UINT1) i4Status;

        i4Status = 0;
        nmhGetFsipv6AddrProfilePrefixAdvStatus (u4ProfileIndex, &i4Status);
        pShowIp6PrefixAttr->u1AdvtStatus = (UINT1) i4Status;

        /* Update the Counts and storage pointer */
        pShowIp6Prefix->u4NoOfPrefix++;
        pShowIp6PrefixAttr++;
        u4NoOfPrefix--;

        if (pCookie->u1AllPrefix == OSIX_FALSE)
        {
            /* No need to proceed further. */
            break;
        }
        pNextPrefixInfo = NULL;

        /* Get the Next Prefix */
#ifndef LNXIP6_WANTED
        pNextPrefixInfo = Ip6GetNextPrefix (pPrefixInfo->u4IfIndex,
                                            &pPrefixInfo->Ip6Prefix,
                                            pPrefixInfo->u1PrefixLen);
#else
        pNextPrefixInfo = Lip6PrefixGetNext (pPrefixInfo->u4IfIndex,
                                             &pPrefixInfo->Ip6Prefix,
                                             pPrefixInfo->i4PrefixLen);
#endif
        if (pNextPrefixInfo != NULL)
        {
            pPrefixInfo = pNextPrefixInfo;
        }
        else
        {
            break;
        }
    }

    /* Update the Cookie */
    SET_ADDR_UNSPECIFIED (pCookie->Prefix);
    pCookie->u1PrefixLen = 0;
    pCookie->u4IfIndex = 0;

    return IP6_SUCCESS;

}

/*********************************************************************
*  Function Name : Ip6ShowPrefixInCxt
*  Description   : Displays the Prefix to be advertised in RA
*  Input(s)      : CliHandle
*                  u4Index - Interface Index
*                  u4VcId - VRF Id
*  Output(s)     : 
*  Return Values : None.
*********************************************************************/
VOID
Ip6ShowPrefixInCxt (tCliHandle CliHandle, UINT4 u4Index, UINT4 u4VcId)
{
    CHR1               *pu1Temp = NULL;
    tShowIp6PrefixCookie Ip6PrefixCookie;
    tShowIp6Prefix     *pShowIp6Prefix = NULL;
    tShowIp6PrefixAttr *pShowIp6PrefixAttr = NULL;
    INT4                i4HdrFlag = OSIX_TRUE;
    UINT1               au1PrefixInfo[sizeof (tShowIp6Prefix) +
                                      ((IP6_SHOW_MAX_PREFIX - 1) *
                                       sizeof (tShowIp6PrefixAttr)) +
                                      (2 * sizeof (UINT4))];

    UINT1               u1AddressPrefixFlag = 0;
    tTMO_SLL_NODE      *pSllInfo = NULL;
    tIp6AddrInfo       *pInfo = NULL;
#ifndef LNXIP6_WANTED            /* FSIP */
    tIp6If             *pIf6 = NULL;
#else
    tLip6If            *pIf6 = NULL;
#endif
    UINT4               u4BufSize = 0;
    CHR1               *pu1Temp1 = NULL;
    CHR1               *pu1ValidTime = NULL;
    CHR1               *pu1PrefTime = NULL;
    UINT4               u4ContextId = 0;
    UINT1               au1ContextName[VCMALIAS_MAX_ARRAY_LEN];
    CHR1                ac1AdvtInfo[MAX_IP6_ADVT_INFO_LEN];
    CHR1                ac1OnlinkInfo[MAX_IP6_ONLINK_INFO_LEN];
    CHR1                ac1ValidTime[MAX_IP6_VALID_TIME_LEN];
    CHR1                ac1PrefTime[MAX_IP6_PREF_TIME_LEN];
    INT1                i1ShowAllCxt = FALSE;

    MEMSET (ac1AdvtInfo, IP6_ZERO, MAX_IP6_ADVT_INFO_LEN);
    MEMSET (ac1OnlinkInfo, IP6_ZERO, MAX_IP6_ONLINK_INFO_LEN);
    MEMSET (ac1ValidTime, IP6_ZERO, MAX_IP6_VALID_TIME_LEN);
    MEMSET (ac1PrefTime, IP6_ZERO, MAX_IP6_PREF_TIME_LEN);

    if (u4VcId == VCM_INVALID_VC)
    {
        i1ShowAllCxt = TRUE;
    }

    VcmGetContextIdFromCfaIfIndex (u4Index, &u4ContextId);
    if (i1ShowAllCxt == FALSE)
    {
        if (u4ContextId != u4VcId)
        {
            /*GIven interface does not belong to given VRF */
            return;
        }
    }
#ifndef LNXIP6_WANTED
    pIf6 = Ip6ifGetEntry (u4Index);
    if (pIf6 == NULL)
    {
        /* Given interface is not available in ipv6 */
        return;
    }
#else
    if (Lip6UtlIfEntryExists (u4Index) != OSIX_FAILURE)
    {
        pIf6 = Lip6UtlGetIfEntry (u4Index);
        if (pIf6 == NULL)
        {
            /* Given interface is not available in ipv6 */
            return;
        }
    }
#endif

    VcmGetAliasName (u4ContextId, au1ContextName);
    CliPrintf (CliHandle, "VRF Id  : %d\r\n", u4ContextId);
    CliPrintf (CliHandle, "VRF Name: %s\r\n", au1ContextName);

    /* Initialize the cookie */
    MEMSET (&Ip6PrefixCookie, 0, sizeof (tShowIp6PrefixCookie));
    Ip6PrefixCookie.u4IfIndex = u4Index;
    Ip6PrefixCookie.u1AllPrefix = OSIX_TRUE;
    /* Allocate Memory for Prefix Display. */
    u4BufSize = sizeof (tShowIp6Prefix) +
        ((IP6_SHOW_MAX_PREFIX - 1) * sizeof (tShowIp6PrefixAttr));

    pu1Temp = ac1AdvtInfo;

    pu1Temp1 = ac1OnlinkInfo;

    pu1ValidTime = ac1ValidTime;

    pu1PrefTime = ac1PrefTime;

    for (;;)
    {
        MEMSET (au1PrefixInfo, 0, u4BufSize);
        STRCPY (pu1Temp, "");
        STRCPY (pu1Temp1, "");
        STRCPY (pu1ValidTime, "");
        STRCPY (pu1PrefTime, "");

        Ip6ShowGetPrefix (au1PrefixInfo, u4BufSize, &Ip6PrefixCookie);

        pShowIp6Prefix = (tShowIp6Prefix *) (VOID *) au1PrefixInfo;
        pShowIp6PrefixAttr = &pShowIp6Prefix->aPrefixAttr[0];

        if (i4HdrFlag == OSIX_TRUE)
        {
            /* Display the Header information. */

            CliPrintf (CliHandle,
                       "Codes: A - Address ,  P - Prefix-Advertisement\r\n");
            CliPrintf (CliHandle,
                       "       D - Default , N - Not Advertised\r\n");

            i4HdrFlag = OSIX_FALSE;
        }

        /* Display the Neighbor Cache info */
        while (pShowIp6Prefix->u4NoOfPrefix != 0)
        {
            /* Display the Prefix Info */
            /* Check whether the Prefix is a Address Prefix or Config prefix */
#ifndef LNXIP6_WANTED
            TMO_SLL_Scan (&pIf6->addr6Ilist, pSllInfo, tTMO_SLL_NODE *)
#else
            TMO_SLL_Scan (&pIf6->Ip6AddrList, pSllInfo, tTMO_SLL_NODE *)
#endif
            {
                pInfo = IP6_ADDR_PTR_FROM_SLL (pSllInfo);

                if ((pInfo->u1Status & (UINT1) ADDR6_COMPLETE (pIf6)) &&
                    (pInfo->u1Status & ADDR6_PREFERRED) &&
                    (pInfo->u1AddrType == ADDR6_UNICAST) &&
                    (Ip6AddrMatch
                     (&(pInfo->ip6Addr), &(pShowIp6PrefixAttr->Prefix),
                      (INT4) pInfo->u1PrefLen)))
                    u1AddressPrefixFlag = 1;
            }

            if (u1AddressPrefixFlag == 1)
            {
                STRCPY (pu1Temp, "A");
                u1AddressPrefixFlag = 0;
            }

            else
            {
                STRCPY (pu1Temp, "P");
            }

            if (pShowIp6PrefixAttr->u1AdvtStatus == IP6_ADDR_PROF_PREF_ADV_ON)
            {
                if (pShowIp6PrefixAttr->u4IfIndex == 0)
                {
                    STRCAT (pu1Temp, "D");
                }
                else
                {
                    STRCAT (pu1Temp, " ");
                }
            }
            else
            {
                STRCAT (pu1Temp, "N");
            }

            STRCPY (pu1Temp1, "[");
            if (pShowIp6PrefixAttr->u1OnLinkStatus ==
                IP6_ADDR_PROF_ONLINK_ADV_ON)
            {
                STRCAT (pu1Temp1, "L");
            }

            if (pShowIp6PrefixAttr->u1AutoConfigStatus ==
                IP6_ADDR_PROF_AUTO_ADV_ON)
            {
                STRCAT (pu1Temp1, "A");
            }
            STRCAT (pu1Temp1, "]");

            if (pShowIp6PrefixAttr->u4ValidTime == IP6_ADDR_PROF_MAX_VALID_LIFE)
            {
                SNPRINTF (pu1ValidTime, 8, "%s", "Infinite");
            }
            else
            {
                SNPRINTF (pu1ValidTime, 8, "%-8d",
                          pShowIp6PrefixAttr->u4ValidTime);
            }

            if (pShowIp6PrefixAttr->u4PrefTime == IP6_ADDR_PROF_MAX_PREF_LIFE)
            {
                SNPRINTF (pu1PrefTime, 8, "%s", "Infinite");
            }
            else
            {
                SNPRINTF (pu1PrefTime, 8, "%-8d",
                          pShowIp6PrefixAttr->u4PrefTime);
            }

            CliPrintf (CliHandle,
                       "%s  %s  %d %s Valid lifetime %s, Preferred lifetime %s\r\n",
                       pu1Temp, Ip6PrintNtop (&pShowIp6PrefixAttr->Prefix),
                       pShowIp6PrefixAttr->u1PrefixLen, pu1Temp1, pu1ValidTime,
                       pu1PrefTime);

            /* Go to Next ND entry */
            pShowIp6Prefix->u4NoOfPrefix--;
            if (pShowIp6Prefix->u4NoOfPrefix)
            {
                pShowIp6PrefixAttr++;
            }
            STRCPY (pu1Temp, "");
            STRCPY (pu1Temp1, "");
            STRCPY (pu1ValidTime, "");
            STRCPY (pu1PrefTime, "");
        }

        /* Check Cookie for to find more route still exist or over. */
        if ((IS_ADDR_UNSPECIFIED (Ip6PrefixCookie.Prefix)) &&
            (Ip6PrefixCookie.u1PrefixLen == 0) &&
            (Ip6PrefixCookie.u4IfIndex == 0))
        {
            break;
        }
    }
    return;
}

/*********************************************************************
 *  Function Name : Ip6SetPolicyPrefix 
 *  Description   :  This function creates an entry in the policy table
 *  Input(s)      : CliHandle,
 *                  u4IfIndex - ifIndex for which the entry needs  
 *                              to be added
 *                  Ip6PolicyPrefix - structure having the value that
 *                                     is to be updated
 *                  u4PrefixLen - prefix len
 *                  u4Precedence - Precedence
 *                  i4AddrType - addresstype
 *  Output(s)     : None
 *  Return Values : None
 *********************************************************************/

VOID
Ip6SetPolicyPrefix (tCliHandle CliHandle, UINT4 u4IfIndex,
                    tIp6Addr Ip6PolicyPrefix, UINT4 u4PrefixLen, UINT4 u4Label,
                    UINT4 u4Precedence, INT4 i4AddrType)
{

    tSNMP_OCTET_STRING_TYPE PrefixAddr;
    UINT4               u4ErrorCode = 0;
    INT1                i1Status = 0;
    INT4                i4AdminStaus = 0;
    UINT1               au1PrefixAddrOctetList[IP6_ADDR_SIZE];
    tIp6Addr            Ip6Addr;

    MEMSET (au1PrefixAddrOctetList, IP6_ZERO, IP6_ADDR_SIZE);

    if (!IS_ADDR_UNSPECIFIED (Ip6PolicyPrefix))
    {
        PrefixAddr.pu1_OctetList = au1PrefixAddrOctetList;
        Ip6AddrCopy ((tIp6Addr *) (VOID *) PrefixAddr.pu1_OctetList,
                     &Ip6PolicyPrefix);
        PrefixAddr.i4_Length = IP6_ADDR_SIZE;

        i1Status = nmhGetFsipv6AddrSelPolicyRowStatus
            (&PrefixAddr, (INT4) u4PrefixLen, (INT4) u4IfIndex, &i4AdminStaus);

        MEMSET (&Ip6Addr, IP6_ZERO, IP6_ADDR_SIZE);
        MEMCPY (&Ip6Addr, PrefixAddr.pu1_OctetList, sizeof (tIp6Addr));

        if (IS_ADDR_UNSPECIFIED (Ip6Addr))
        {
            CliPrintf (CliHandle, "%% Invalid Prefix/Prefix length\r\n");
            return;
        }

        if (i1Status == SNMP_FAILURE)
        {
            if (nmhTestv2Fsipv6AddrSelPolicyRowStatus
                (&u4ErrorCode, &PrefixAddr, u4PrefixLen,
                 (INT4) u4IfIndex, IP6FWD_CREATE_AND_WAIT) == SNMP_FAILURE)
            {
                return;
            }

            if (nmhSetFsipv6AddrSelPolicyRowStatus
                (&PrefixAddr, u4PrefixLen, (INT4) u4IfIndex,
                 IP6FWD_CREATE_AND_WAIT) == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return;
            }
        }
        if (nmhTestv2Fsipv6AddrSelPolicyPrecedence
            (&u4ErrorCode, &PrefixAddr, u4PrefixLen,
             (INT4) u4IfIndex, (INT4) u4Precedence) == SNMP_FAILURE)
        {
            if (i1Status == SNMP_FAILURE)
            {
                nmhSetFsipv6AddrSelPolicyRowStatus
                    (&PrefixAddr, u4PrefixLen,
                     (INT4) u4IfIndex, IP6FWD_DESTROY);
            }
            return;
        }

        if (nmhSetFsipv6AddrSelPolicyPrecedence
            (&PrefixAddr,
             u4PrefixLen, (INT4) u4IfIndex,
             (INT4) u4Precedence) == SNMP_FAILURE)
        {
            if (i1Status == SNMP_FAILURE)
            {
                nmhSetFsipv6AddrSelPolicyRowStatus
                    (&PrefixAddr, u4PrefixLen, (INT4) u4IfIndex,
                     IP6FWD_DESTROY);
            }
            CLI_FATAL_ERROR (CliHandle);
            return;
        }

        if (nmhTestv2Fsipv6AddrSelPolicyLabel
            (&u4ErrorCode, &PrefixAddr, u4PrefixLen,
             (INT4) u4IfIndex, (INT4) u4Label) == SNMP_FAILURE)
        {
            if (i1Status == SNMP_FAILURE)
            {
                nmhSetFsipv6AddrSelPolicyRowStatus
                    (&PrefixAddr, u4PrefixLen,
                     (INT4) u4IfIndex, IP6FWD_DESTROY);
            }
            return;
        }

        if (nmhSetFsipv6AddrSelPolicyLabel
            (&PrefixAddr,
             u4PrefixLen, (INT4) u4IfIndex, (INT4) u4Label) == SNMP_FAILURE)
        {
            if (i1Status == SNMP_FAILURE)
            {
                nmhSetFsipv6AddrSelPolicyRowStatus
                    (&PrefixAddr, u4PrefixLen, (INT4) u4IfIndex,
                     IP6FWD_DESTROY);
            }
            CLI_FATAL_ERROR (CliHandle);
            return;
        }

        if (nmhTestv2Fsipv6AddrSelPolicyAddrType
            (&u4ErrorCode, &PrefixAddr, u4PrefixLen,
             (INT4) u4IfIndex, (INT4) i4AddrType) == SNMP_FAILURE)
        {
            if (i1Status == SNMP_FAILURE)
            {
                nmhSetFsipv6AddrSelPolicyRowStatus
                    (&PrefixAddr, u4PrefixLen,
                     (INT4) u4IfIndex, IP6FWD_DESTROY);
            }
            return;
        }

        if (nmhSetFsipv6AddrSelPolicyAddrType
            (&PrefixAddr,
             u4PrefixLen, (INT4) u4IfIndex, (INT4) i4AddrType) == SNMP_FAILURE)
        {
            if (i1Status == SNMP_FAILURE)
            {
                nmhSetFsipv6AddrSelPolicyRowStatus
                    (&PrefixAddr, u4PrefixLen, (INT4) u4IfIndex,
                     IP6FWD_DESTROY);
            }
            CLI_FATAL_ERROR (CliHandle);
            return;
        }

        if (i1Status == SNMP_FAILURE)
        {
            if (nmhTestv2Fsipv6AddrSelPolicyRowStatus
                (&u4ErrorCode, &PrefixAddr, u4PrefixLen, (INT4) u4IfIndex,
                 IP6FWD_ACTIVE) == SNMP_FAILURE)
            {
                nmhSetFsipv6AddrSelPolicyRowStatus
                    (&PrefixAddr, u4PrefixLen, (INT4) u4IfIndex,
                     IP6FWD_DESTROY);
                return;
            }

            if (nmhSetFsipv6AddrSelPolicyRowStatus
                (&PrefixAddr, u4PrefixLen, (INT4) u4IfIndex,
                 IP6FWD_ACTIVE) == SNMP_FAILURE)
            {
                nmhSetFsipv6AddrSelPolicyRowStatus
                    (&PrefixAddr, u4PrefixLen, (INT4) u4IfIndex,
                     IP6FWD_DESTROY);
                CLI_FATAL_ERROR (CliHandle);
                return;
            }
        }
    }
}

/*********************************************************************
 *  Function Name : Ip6DelPolicyPrefix
 *  Description   :  This function deletes an entry in the policy table
 *  Input(s)      : CliHandle,
 *                  u4IfIndex - ifIndex for which the entry needs  
 *                              to be deleted 
 *                  Ip6PolicyPrefix - structure having the value that
 *                                     is to be updated
 *                  u4PrefixLen - prefix len
 *                  u4Precedence - Precedence
 *                  i4AddrType - addresstype
 *  Output(s)     : None
 *  Return Values : None
 *********************************************************************/

VOID
Ip6DelPolicyPrefix (tCliHandle CliHandle, UINT4 u4IfIndex,
                    tIp6Addr Ip6PolicyPrefix, UINT4 u4PrefixLen, UINT4 u4Label,
                    UINT4 u4Precedence, INT4 i4AddrType)
{

    tSNMP_OCTET_STRING_TYPE PrefixAddr;
    UINT4               u4ErrorCode = 0;
    INT4                i4AdminStaus = 0;
    UINT1               au1PrefixAddrOctetList[IP6_ADDR_SIZE];
    INT1                i1Status = 0;

    MEMSET (au1PrefixAddrOctetList, IP6_ZERO, IP6_ADDR_SIZE);

    if (!IS_ADDR_UNSPECIFIED (Ip6PolicyPrefix))
    {
        PrefixAddr.pu1_OctetList = au1PrefixAddrOctetList;
        Ip6AddrCopy ((tIp6Addr *) (VOID *) PrefixAddr.pu1_OctetList,
                     &Ip6PolicyPrefix);
        PrefixAddr.i4_Length = IP6_ADDR_SIZE;

        i1Status = nmhGetFsipv6AddrSelPolicyRowStatus
            (&PrefixAddr, (INT4) u4PrefixLen, (INT4) u4IfIndex, &i4AdminStaus);

        if (i1Status == SNMP_FAILURE)
        {
            CLI_SET_ERR (CLI_IP6_POLICY_PREFIX_NOT_FOUND);
            return;
        }

        if (nmhTestv2Fsipv6AddrSelPolicyRowStatus
            (&u4ErrorCode, &PrefixAddr, u4PrefixLen,
             (INT4) u4IfIndex, IP6FWD_DESTROY) == SNMP_FAILURE)
        {
            return;
        }

        if (nmhSetFsipv6AddrSelPolicyRowStatus
            (&PrefixAddr, u4PrefixLen, (INT4) u4IfIndex,
             IP6FWD_DESTROY) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return;
        }
    }
    UNUSED_PARAM (u4Label);
    UNUSED_PARAM (u4Precedence);
    UNUSED_PARAM (i4AddrType);
}

/*********************************************************************
*  Function Name : Ip6Nd6CacheAdd
*  Description   : Adds a Static Neighbor Cache entry
*  Input(s)      : CliHandle
*                  u4IfIndex - Interface Index
*                  Ip6Addr - Destination IPv6 Address
*                  au1PhyMacAddr - LinkLayer Address to reach the 
*                                  destination
*  Output(s)     :  
*  Return Values : None.
*********************************************************************/
VOID
Ip6Nd6CacheAdd (tCliHandle CliHandle, UINT4 u4IfIndex,
                tIp6Addr Ip6Addr, UINT1 *au1PhyMacAddr, UINT4 u4CurrContext)
{
    tSNMP_OCTET_STRING_TYPE Addr;
    tSNMP_OCTET_STRING_TYPE PhyAddr;
    UINT4               u4ErrorCode = 0;
    UINT4               u4ContextId = 0;
    INT4                i4Status = 0;
    INT1                i1RetVal = 0;

    Addr.pu1_OctetList = (UINT1 *) &Ip6Addr;
    Addr.i4_Length = IP6_ADDR_SIZE;
    PhyAddr.pu1_OctetList = au1PhyMacAddr;
    PhyAddr.i4_Length = IP6_MAX_LLA_LEN;

    if (u4CurrContext != VCM_INVALID_VC)
    {
        VcmGetContextIdFromCfaIfIndex (u4IfIndex, &u4ContextId);

        if (u4ContextId != u4CurrContext)
        {
            CliPrintf (CliHandle, "\r\n%% Configured interface is not mapped"
                       " to the given context\r\n");
            return;
        }
    }

    i1RetVal =
        nmhGetFsipv6NdLanCacheStatus ((INT4) u4IfIndex, &Addr, &i4Status);
    /* Create the ND6 Cache Entry */
    if (nmhTestv2Fsipv6NdLanCacheStatus (&u4ErrorCode, (INT4) u4IfIndex, &Addr,
                                         ND6_LAN_CACHE_ENTRY_VALID) ==
        SNMP_FAILURE)
    {
        return;
    }

    if (nmhSetFsipv6NdLanCacheStatus ((INT4) u4IfIndex, &Addr,
                                      ND6_LAN_CACHE_ENTRY_VALID) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return;
    }

    if (nmhTestv2Fsipv6NdLanCachePhysAddr
        (&u4ErrorCode, (INT4) u4IfIndex, &Addr, &PhyAddr) == SNMP_FAILURE)
    {
        if (i1RetVal == SNMP_FAILURE)
        {

            nmhSetFsipv6NdLanCacheStatus ((INT4) u4IfIndex, &Addr,
                                          ND6_LAN_CACHE_ENTRY_INVALID);
        }
        return;
    }

    /* Set the Cache PhyAddr Value */
    if (nmhSetFsipv6NdLanCachePhysAddr ((INT4) u4IfIndex, &Addr,
                                        &PhyAddr) == SNMP_FAILURE)
    {
        if (i1RetVal == SNMP_FAILURE)
        {

            nmhSetFsipv6NdLanCacheStatus ((INT4) u4IfIndex, &Addr,
                                          ND6_LAN_CACHE_ENTRY_INVALID);
        }
        CLI_FATAL_ERROR (CliHandle);
        return;
    }

    return;
}

/*********************************************************************
*  Function Name : Ip6Nd6CacheDel
*  Description   : Deletes the Neighbor Cache entry
*  Input(s)      : CliHandle
*                  u4IfIndex - Interface Index
*                  Ip6Addr - IPv6 Prefix whose entry is to be deleted 
*                            from ND table
*  Output(s)     : 
*  Return Values : None.
*********************************************************************/
VOID
Ip6Nd6CacheDel (tCliHandle CliHandle, UINT4 u4IfIndex,
                tIp6Addr Ip6Addr, UINT4 u4CurrContext)
{
    tSNMP_OCTET_STRING_TYPE Addr;
    UINT4               u4ErrorCode = 0;
    UINT4               u4ContextId = 0;

    Addr.pu1_OctetList = (UINT1 *) &Ip6Addr;
    Addr.i4_Length = IP6_ADDR_SIZE;

    if (u4CurrContext != VCM_INVALID_VC)
    {
        VcmGetContextIdFromCfaIfIndex (u4IfIndex, &u4ContextId);

        if (u4ContextId != u4CurrContext)
        {
            CliPrintf (CliHandle, "\r\n%% Configured interface is not mapped"
                       " to the given context\r\n");
            return;
        }
    }

    /* Delete the ND6 Cache Entry */
    if (nmhTestv2Fsipv6NdLanCacheStatus (&u4ErrorCode, (INT4) u4IfIndex, &Addr,
                                         ND6_LAN_CACHE_ENTRY_INVALID) ==
        SNMP_FAILURE)
    {
        return;
    }

    if (nmhSetFsipv6NdLanCacheStatus ((INT4) u4IfIndex, &Addr,
                                      ND6_LAN_CACHE_ENTRY_INVALID) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return;
    }

    return;
}

/*********************************************************************
*  Function Name : Ip6TraceRoute
*  Description   : Traces the path of a destination
*  Input(s)      : CliHandle
*                  Ip6Addr - Destination IPv6 Prefix 
*  Output(s)     :  
*  Return Values : None
*********************************************************************/
VOID
Ip6TraceRoute (tCliHandle CliHandle, tIp6Addr Ip6Addr)
{
    UINT1               Addr[IP6_ADDR_SIZE];

    UNUSED_PARAM (CliHandle);

    MEMSET (Addr, 0, sizeof (Addr));
    MEMCPY (Addr, &Ip6Addr, sizeof (Addr));
#ifndef LNXIP6_WANTED
    TraceRoute (6, (UINT1 *) (Addr));
#endif
    return;
}

/*********************************************************************
*  Function Name : Ip6ClearNeighbors
*  Description   : Clears IPv6 ND Cache Table
*  Input(s)      : CliHandle
*                  u4VcId 
*                : pIp6Addr
*  Output(s)     :  
*  Return Values : None
*********************************************************************/
VOID
Ip6ClearNeighbors (tCliHandle CliHandle, UINT4 u4VcId, tIp6Addr * pIp6Addr)
{
    INT4                i4FirstIpv6NdIndex = 0;
    INT4                i4NextIpv6NdIndex = 0;
    tSNMP_OCTET_STRING_TYPE Ipv6AddrAddress;
    tSNMP_OCTET_STRING_TYPE NextIpv6AddrAddress;
    UINT4               au4Ip6Ifaces[IP6_MAX_LOGICAL_IFACES + 1];
    UINT2               u2Cntr = 0;
    UINT1               au1Ip6AddrOctetList[IP6_ADDR_SIZE];
    UINT1               au1NxtIp6AddrOctetList[IP6_ADDR_SIZE];
#ifndef LNXIP6_WANTED
    tIp6If             *pIf6 = NULL;
    tNd6CacheEntry     *pNd6cEntry = NULL;
#else
    tLip6If            *pIf6Entry = NULL;
    tLip6NdEntry       *pNd6cEntry = NULL;
#endif

    MEMSET (au1Ip6AddrOctetList, IP6_ZERO, IP6_ADDR_SIZE);
    MEMSET (au1NxtIp6AddrOctetList, IP6_ZERO, IP6_ADDR_SIZE);

    Ipv6AddrAddress.pu1_OctetList = au1Ip6AddrOctetList;

    Ipv6AddrAddress.i4_Length = IP6_ADDR_SIZE;
    NextIpv6AddrAddress.pu1_OctetList = au1NxtIp6AddrOctetList;
    NextIpv6AddrAddress.i4_Length = IP6_ADDR_SIZE;
    /* Reset the counter for NS Retries */
#ifndef LNXIP6_WANTED
    gNoOfNSRetrial_1 = 0;
    gNoOfNSRetrial_2 = 0;
    gNoOfNSRetrial_3 = 0;
#endif
    if (u4VcId == VCM_INVALID_VC)
    {
        if (nmhGetFirstIndexFsipv6NdLanCacheTable (&i4FirstIpv6NdIndex,
                                                   &Ipv6AddrAddress) ==
            SNMP_FAILURE)
        {
            CLI_SET_ERR (CLI_IP6_CLEARED_ND_TABLE);
            return;
        }
        MEMCPY (NextIpv6AddrAddress.pu1_OctetList,
                Ipv6AddrAddress.pu1_OctetList, IPVX_IPV6_ADDR_LEN);
        i4NextIpv6NdIndex = i4FirstIpv6NdIndex;

        do
        {
#ifndef LNXIP6_WANTED /*FSIP*/
                pIf6 = Ip6ifGetEntry ((UINT4) i4NextIpv6NdIndex);
            if (pIf6 == NULL)
            {
                return;
            }
            pNd6cEntry =
                Nd6IsCacheForAddr (pIf6,
                                   (tIp6Addr *) (VOID *) NextIpv6AddrAddress.
                                   pu1_OctetList);

#else
            pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4NextIpv6NdIndex);

            if (pIf6Entry == NULL)
            {
                return;
            }
            pNd6cEntry =
                Lip6NdGetEntry (pIf6Entry,
                                (tIp6Addr *) (VOID *) NextIpv6AddrAddress.
                                pu1_OctetList);
#endif
            if ((pNd6cEntry != NULL)
                && (pNd6cEntry->u1ReachState != ND6_CACHE_ENTRY_STATIC))
            {
#ifndef LNXIP6_WANTED /*FSIP*/
                    if (nmhSetFsipv6NdLanCacheStatus (i4NextIpv6NdIndex,
                                                      &NextIpv6AddrAddress,
                                                      ND6_LAN_CACHE_ENTRY_INVALID)
                        == SNMP_FAILURE)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    return;
                }
#else

                Lip6NetLinkNDUpdate (RTM_DELNEIGH, pNd6cEntry);

#endif

            }
            MEMCPY (Ipv6AddrAddress.pu1_OctetList,
                    NextIpv6AddrAddress.pu1_OctetList, IPVX_IPV6_ADDR_LEN);
            i4FirstIpv6NdIndex = i4NextIpv6NdIndex;
        }
        while (nmhGetNextIndexFsipv6NdLanCacheTable (i4FirstIpv6NdIndex,
                                                     &i4NextIpv6NdIndex,
                                                     &Ipv6AddrAddress,
                                                     &NextIpv6AddrAddress) ==
               SNMP_SUCCESS);

    }
    else
    {
        NextIpv6AddrAddress.pu1_OctetList = au1NxtIp6AddrOctetList;
        NextIpv6AddrAddress.i4_Length = IP6_ADDR_SIZE;
        MEMSET (au4Ip6Ifaces, IP6_ZERO,
                (IP6_MAX_LOGICAL_IFACES + 1) * sizeof (UINT4));
        if (VcmGetCxtIpIfaceList (u4VcId, au4Ip6Ifaces) == VCM_FAILURE)
        {
            CLI_SET_ERR (CLI_IP6_CLEARED_ND_TABLE);
            return;
        }
        for (u2Cntr = 1; ((u2Cntr <= au4Ip6Ifaces[0]) &&
                          (u2Cntr < IP6_MAX_LOGICAL_IFACES + 1)); u2Cntr++)
        {
            MEMSET (Ipv6AddrAddress.pu1_OctetList,
                    IP6_ZERO, IPVX_IPV6_ADDR_LEN);
            while (nmhGetNextIndexFsipv6NdLanCacheTable (au4Ip6Ifaces[u2Cntr],
                                                         &i4FirstIpv6NdIndex,
                                                         &Ipv6AddrAddress,
                                                         &NextIpv6AddrAddress)
                   == SNMP_SUCCESS)
            {
                if (au4Ip6Ifaces[u2Cntr] != (UINT4) i4FirstIpv6NdIndex)
                {
                    break;
                }
                if (pIp6Addr != NULL)
                {
                    if (MEMCMP (NextIpv6AddrAddress.pu1_OctetList, pIp6Addr,
                                IP6_ADDR_SIZE) == IP6_ZERO)
                    {

                        if (nmhSetFsipv6NdLanCacheStatus (i4FirstIpv6NdIndex,
                                                          &NextIpv6AddrAddress,
                                                          ND6_LAN_CACHE_ENTRY_INVALID)
                            == SNMP_FAILURE)
                        {
                            CLI_FATAL_ERROR (CliHandle);
                            return;
                        }
                        break;
                    }
                }
                else
                {
                    if (nmhSetFsipv6NdLanCacheStatus (i4FirstIpv6NdIndex,
                                                      &NextIpv6AddrAddress,
                                                      ND6_LAN_CACHE_ENTRY_INVALID)
                        == SNMP_FAILURE)
                    {
                        CLI_FATAL_ERROR (CliHandle);
                        return;
                    }
                }
                MEMCPY (Ipv6AddrAddress.pu1_OctetList,
                        NextIpv6AddrAddress.pu1_OctetList, IPVX_IPV6_ADDR_LEN);

            }
        }
    }
}

/*********************************************************************
*  Function Name : Ip6ClearTraffic
*  Description   : Clears Ipv6,ICMP6,UDP6 statistics 
*  Input(s)      : CliHandle
*                  u4VcId 
*  Output(s)     :  
*  Return Values : None
*********************************************************************/
VOID
Ip6ClearTraffic (tCliHandle CliHandle, UINT4 u4VcId)
{
#ifndef LNXIP6_WANTED            /* FSIP */
    tIp6If             *pIf6 = NULL;
    tIp6Cxt            *pIp6Cxt = NULL;
    UINT4               au4Ip6Ifaces[IP6_MAX_LOGICAL_IFACES + 1];
    INT4                i4Ipv6IfStatsIndex = 0;
    INT4                i4Ipv6IfStatsNextIndex = 0;
    UINT4               u4ContextId = VCM_INVALID_VC;
    UINT4               u4NextContextId = 0;
    UINT2               u2Cntr = IP6_ZERO;
    UNUSED_PARAM (CliHandle);

    if (u4VcId == VCM_INVALID_VC)
    {
        while (nmhGetNextIndexFsipv6IfStatsTable
               (i4Ipv6IfStatsIndex, &i4Ipv6IfStatsNextIndex) == SNMP_SUCCESS)
        {
            i4Ipv6IfStatsIndex = i4Ipv6IfStatsNextIndex;
            pIf6 = Ip6ifGetEntry ((UINT4) i4Ipv6IfStatsIndex);
            if (pIf6 != NULL)
            {
                MEMSET (&(pIf6->stats), 0, sizeof (tIp6IfStats));
                MEMSET (pIf6->pIfIcmp6Stats, 0, sizeof (tIcmp6Stats));
            }
        }
        while (Ip6UtilGetNextCxtId (u4ContextId, &u4NextContextId)
               == IP6_SUCCESS)
        {
            pIp6Cxt = Ipv6UtilGetCxtEntryFromCxtId (u4NextContextId);
            if (pIp6Cxt == NULL)
            {
                return;
            }
            u4ContextId = u4NextContextId;
            MEMSET (&(pIp6Cxt->Ip6SysStats), 0, sizeof (tIp6IfStats));
            MEMSET (&(pIp6Cxt->Icmp6Stats), 0, sizeof (tIcmp6Stats));
            MEMSET (&(pIp6Cxt->Udp6Stats), 0, sizeof (tIp6Udp6Stats));
        }
    }
    else
    {
        pIp6Cxt = Ipv6UtilGetCxtEntryFromCxtId (u4VcId);
        if (pIp6Cxt == NULL)
        {
            return;
        }
        MEMSET (&(pIp6Cxt->Ip6SysStats), 0, sizeof (tIp6IfStats));
        MEMSET (&(pIp6Cxt->Icmp6Stats), 0, sizeof (tIcmp6Stats));
        MEMSET (&(pIp6Cxt->Udp6Stats), 0, sizeof (tIp6Udp6Stats));
        MEMSET (au4Ip6Ifaces, IP6_ZERO,
                (IP6_MAX_LOGICAL_IFACES + 1) * sizeof (UINT4));
        if (VcmGetCxtIpIfaceList (u4VcId, au4Ip6Ifaces) == VCM_FAILURE)
        {
            return;
        }
        for (u2Cntr = 1; ((u2Cntr <= au4Ip6Ifaces[0]) &&
                          (u2Cntr < IP6_MAX_LOGICAL_IFACES + 1)); u2Cntr++)
        {
            pIf6 = Ip6ifGetEntry (au4Ip6Ifaces[u2Cntr]);
            if (pIf6 == NULL)
            {
                continue;
            }
            MEMSET (&(pIf6->stats), 0, sizeof (tIp6IfStats));
            MEMSET (pIf6->pIfIcmp6Stats, 0, sizeof (tIcmp6Stats));
        }
    }
#else /* IP_WANTED */
    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (u4VcId);
#endif /* IP_WANTED */
    return;
}

/*********************************************************************
*  Function Name : Ip6ClearRoute
*  Description   : Clears IPv6 Routing table
*  Input(s)      : CliHandle
*                  u4VcId 
*  Output(s)     :  
*  Return Values : None
*********************************************************************/
VOID
Ip6ClearRoute (tCliHandle CliHandle, UINT4 u4VcId)
{
    tSNMP_OCTET_STRING_TYPE RouteDest;
    tSNMP_OCTET_STRING_TYPE NextHop;
    tSNMP_OCTET_STRING_TYPE NextRouteDest;
    tSNMP_OCTET_STRING_TYPE NextRtHop;
    tSNMP_OID_TYPE      RoutePolicy;
    tSNMP_OID_TYPE      NextRoutePolicy;
    INT4                i4ContextId = u4VcId;
    INT4                i4NextContextId = 0;
    INT4                i4DestType = 0;
    INT4                i4PrefixLen = 0;
    INT4                i4NextPrefixLen = 0;
    INT4                i4NextHopType = 0;
    INT4                i4NextDestType = 0;
    INT4                i4NextRtHopType = 0;
    INT4                i4RetVal = 0;
    UINT1               au1RtDestOctetList[IP6_ADDR_SIZE];
    UINT1               au1NxtRtDestOctetList[IP6_ADDR_SIZE];
    UINT1               au1RtHopOctetList[IP6_ADDR_SIZE];
    UINT1               au1NxtRtHopOctetList[IP6_ADDR_SIZE];
    UINT4               au4RtPolicyOctetList[IP6_ADDR_SIZE / sizeof (UINT4)];
    UINT4               au4NxtRtPolicyOctetList[IP6_ADDR_SIZE / sizeof (UINT4)];

    UNUSED_PARAM (CliHandle);

    RouteDest.pu1_OctetList = au1RtDestOctetList;
    RouteDest.i4_Length = 16;

    NextHop.pu1_OctetList = au1RtHopOctetList;
    NextHop.i4_Length = 16;

    NextRouteDest.pu1_OctetList = au1NxtRtDestOctetList;
    NextRouteDest.i4_Length = 16;

    NextRtHop.pu1_OctetList = au1NxtRtHopOctetList;
    NextRtHop.i4_Length = 16;

    RoutePolicy.pu4_OidList = au4RtPolicyOctetList;
    RoutePolicy.u4_Length = IP6_TWO;

    NextRoutePolicy.pu4_OidList = au4NxtRtPolicyOctetList;
    NextRoutePolicy.u4_Length = IP6_TWO;

    MEMSET (RouteDest.pu1_OctetList, IP6_ZERO, RouteDest.i4_Length);
    MEMSET (NextRouteDest.pu1_OctetList, IP6_ZERO, NextRouteDest.i4_Length);
    MEMSET (NextHop.pu1_OctetList, IP6_ZERO, NextHop.i4_Length);
    MEMSET (NextRtHop.pu1_OctetList, IP6_ZERO, NextRtHop.i4_Length);
    MEMSET (RoutePolicy.pu4_OidList, IP6_ZERO, RoutePolicy.u4_Length);
    MEMSET (NextRoutePolicy.pu4_OidList, IP6_ZERO, NextRoutePolicy.u4_Length);

    if (nmhGetFirstIndexFsMIStdInetCidrRouteTable
        (&i4NextContextId, &i4NextDestType, &NextRouteDest,
         (UINT4 *) &i4NextPrefixLen, &NextRoutePolicy,
         &i4NextRtHopType, &NextRtHop) == SNMP_FAILURE)
    {
        return;
    }

    while (1)
    {
        if ((u4VcId != VCM_INVALID_VC) && (i4NextContextId > (INT4) u4VcId))
        {
            break;
        }
        i4ContextId = i4NextContextId;
        i4PrefixLen = i4NextPrefixLen;
        i4DestType = i4NextDestType;
        i4NextHopType = i4NextRtHopType;
        MEMCPY (RouteDest.pu1_OctetList, NextRouteDest.pu1_OctetList,
                NextRouteDest.i4_Length);
        MEMCPY (NextHop.pu1_OctetList, NextRtHop.pu1_OctetList,
                NextRtHop.i4_Length);
        MEMCPY (RoutePolicy.pu4_OidList, NextRoutePolicy.pu4_OidList,
                MEM_MAX_BYTES (NextRoutePolicy.u4_Length,
                               (sizeof (UINT4) * IP6_TWO)));
        RouteDest.i4_Length = NextRouteDest.i4_Length;
        NextHop.i4_Length = NextRtHop.i4_Length;
        RoutePolicy.u4_Length = NextRoutePolicy.u4_Length;

        i4RetVal = nmhGetNextIndexFsMIStdInetCidrRouteTable
            (i4ContextId, &i4NextContextId, i4DestType, &i4NextDestType,
             &RouteDest, &NextRouteDest, i4PrefixLen,
             (UINT4 *) &i4NextPrefixLen, &RoutePolicy, &NextRoutePolicy,
             i4NextHopType, &i4NextRtHopType, &NextHop, &NextRtHop);

        if (i4DestType == INET_ADDR_TYPE_IPV6)
        {
            if ((u4VcId == VCM_INVALID_VC)
                || ((u4VcId != VCM_INVALID_VC)
                    && (i4ContextId == (INT4) u4VcId)))
            {
                nmhSetFsMIStdInetCidrRouteStatus
                    (i4ContextId, i4DestType, &RouteDest,
                     i4PrefixLen, &RoutePolicy, i4NextHopType,
                     &NextHop, IP6FWD_DESTROY);
            }
        }
        if (i4RetVal == SNMP_FAILURE)
        {
            break;
        }
    }

    return;

}

/*********************************************************************
*  Function Name : Ip6RouteSummaryShowInCxt
*  Description   : Displays a summary of the routing table
*  Input(s)      : CliHandle
*  Output(s)     :  
*  Return Values : None
*********************************************************************/
VOID
Ip6RouteSummaryShowInCxt (tCliHandle CliHandle, UINT4 u4VcId)
{
    tSNMP_OCTET_STRING_TYPE FirstIndexRouteDest;
    tSNMP_OCTET_STRING_TYPE FirstIndexNextHop;
    tIp6RtEntry         InRtInfo;
    tIp6RtEntry        *pIp6BestRt = NULL;
    tIp6RtEntry        *pTmp6RtEntry = NULL;
    INT4                i4FirstIndexProtocol = 0;
    INT4                i4FirstIndexPrefixLen = 0;
    tSNMP_OCTET_STRING_TYPE NextIndexRouteDest;
    tSNMP_OCTET_STRING_TYPE NextIndexNextHop;
    INT4                i4NextIndexProtocol = 0;
    INT4                i4NextIndexPrefixLen = 0;
    INT4                i4Ipv6RouteAdminStatus = 0;
    INT4                i4RetVal = 0;

    UINT4               u4OspfCount = 0;
    UINT4               u4IsisCount = 0;
    UINT4               u4ConnectedCount = 0;
    UINT4               u4StaticCount = 0;
    UINT4               u4RipCount = 0;
    UINT4               u4BgpCount = 0;
    UINT4               au4PrefixCount[129];
    UINT4               u4Index = 0;
    UINT4               u4RouteCount = 0;
    UINT4               u4InvalidRtCount = 0;
    UINT4               u4FailedRtCount = 0;

    UINT4               u4ShowAllCxt = FALSE;
    UINT4               u4FirstVcId = VCM_INVALID_VC;
    UINT4               u4NextVcId = VCM_INVALID_VC;
    UINT1               au1ContextName[VCMALIAS_MAX_ARRAY_LEN];
    UINT1               au1FirstIndxRtDestOctetList[IP6_ADDR_SIZE];
    UINT1               au1NxtIndxRtDestOctetList[IP6_ADDR_SIZE];
    UINT1               au1FirstIndxRtHopOctetList[IP6_ADDR_SIZE];
    UINT1               au1NxtIndxRtHopOctetList[IP6_ADDR_SIZE];
    UINT1               u1ECMPRtCnt = 0;

    MEMSET (au1FirstIndxRtDestOctetList, IP6_ZERO, IP6_ADDR_SIZE);
    MEMSET (au1NxtIndxRtDestOctetList, IP6_ZERO, IP6_ADDR_SIZE);
    MEMSET (au1FirstIndxRtHopOctetList, IP6_ZERO, IP6_ADDR_SIZE);
    MEMSET (au1NxtIndxRtHopOctetList, IP6_ZERO, IP6_ADDR_SIZE);

    FirstIndexRouteDest.pu1_OctetList = au1FirstIndxRtDestOctetList;
    FirstIndexRouteDest.i4_Length = 16;

    FirstIndexNextHop.pu1_OctetList = au1FirstIndxRtHopOctetList;
    FirstIndexNextHop.i4_Length = 16;

    NextIndexRouteDest.pu1_OctetList = au1NxtIndxRtDestOctetList;
    NextIndexRouteDest.i4_Length = 16;

    NextIndexNextHop.pu1_OctetList = au1NxtIndxRtHopOctetList;
    NextIndexNextHop.i4_Length = 16;

    for (u4Index = 0; u4Index <= 128; u4Index++)
    {
        au4PrefixCount[u4Index] = 0;
    }

    if (u4VcId == VCM_INVALID_VC)
    {
        u4ShowAllCxt = TRUE;
        i4RetVal = nmhGetFirstIndexFsMIIpv6RouteTable
            ((INT4 *) &u4NextVcId, &NextIndexRouteDest,
             &i4NextIndexPrefixLen, &i4NextIndexProtocol, &NextIndexNextHop);
        MEMCPY (&(InRtInfo.dst), NextIndexRouteDest.pu1_OctetList,
                sizeof (tIp6Addr));
        InRtInfo.u1Prefixlen = (UINT1) i4NextIndexPrefixLen;
        Rtm6GetBestRouteEntryInCxt (u4NextVcId, &InRtInfo, &pIp6BestRt);
        if ((pIp6BestRt != NULL) && (pIp6BestRt->u4Flag & RTM6_ECMP_RT))
        {
            pTmp6RtEntry = pIp6BestRt;
            while ((pTmp6RtEntry != NULL)
                   && (pTmp6RtEntry->u4Metric == pIp6BestRt->u4Metric))
            {
                u1ECMPRtCnt++;
                pTmp6RtEntry = pTmp6RtEntry->pNextAlternatepath;
            }
        }

    }
    else
    {

        if (VcmIsL3VcExist (u4VcId) == VCM_FALSE)
        {
            CliPrintf (CliHandle, "\r%% Invalid VRF \r\n");
            return;
        }
        i4RetVal = nmhGetNextIndexFsMIIpv6RouteTable
            (u4VcId, (INT4 *) &u4NextVcId,
             &FirstIndexRouteDest, &NextIndexRouteDest,
             i4FirstIndexPrefixLen, &i4NextIndexPrefixLen,
             i4FirstIndexProtocol, &i4NextIndexProtocol,
             &FirstIndexNextHop, &NextIndexNextHop);

    }
    if (i4RetVal == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IP6_NO_ROUTES);
        return;
    }

    while (i4RetVal == SNMP_SUCCESS)
    {
        i4RetVal =
            nmhGetFsMIIpv6RouteAdminStatus ((INT4) u4NextVcId,
                                            &NextIndexRouteDest,
                                            i4NextIndexPrefixLen,
                                            i4NextIndexProtocol,
                                            &NextIndexNextHop,
                                            &i4Ipv6RouteAdminStatus);

        if ((i4RetVal == SNMP_SUCCESS)
            && (i4Ipv6RouteAdminStatus == IP6FWD_ACTIVE))
        {
            switch (i4NextIndexProtocol)
            {
                case IP6_LOCAL_PROTOID:
                    u4ConnectedCount++;
                    break;
                case IP6_NETMGMT_PROTOID:
                    u4StaticCount++;
                    break;
                case IP6_OSPF_PROTOID:
                    u4OspfCount++;
                    break;
                case IP6_RIP_PROTOID:
                    u4RipCount++;
                    break;
                case IP6_BGP_PROTOID:
                    u4BgpCount++;
                    break;
                case IP6_ISIS_PROTOID:
                    u4IsisCount++;
                    break;
            }

            au4PrefixCount[i4NextIndexPrefixLen]++;
        }
        else
        {
            u4InvalidRtCount++;
        }
        if (u4FirstVcId != u4NextVcId)
        {
            RTM6_TASK_UNLOCK ();
            IP6_TASK_LOCK ();
            if (Ip6SelectContext (u4NextVcId) != SNMP_FAILURE)
            {
                nmhGetIpv6RouteNumber (&u4RouteCount);
                Ip6ReleaseContext ();
            }
            IP6_TASK_UNLOCK ();
            RTM6_TASK_LOCK ();
            VcmGetAliasName (u4NextVcId, au1ContextName);
            CliPrintf (CliHandle, "\r\nVRF    Name:      %s\r\n",
                       au1ContextName);
            CliPrintf (CliHandle, "---------------\r\n\n");

        }

        u4FirstVcId = u4NextVcId;
        MEMCPY (FirstIndexRouteDest.pu1_OctetList,
                NextIndexRouteDest.pu1_OctetList, 16);
        i4FirstIndexPrefixLen = i4NextIndexPrefixLen;
        i4FirstIndexProtocol = i4NextIndexProtocol;
        MEMCPY (FirstIndexNextHop.pu1_OctetList,
                NextIndexNextHop.pu1_OctetList, 16);

        i4RetVal = nmhGetNextIndexFsMIIpv6RouteTable
            ((INT4) u4FirstVcId, (INT4 *) &u4NextVcId,
             &FirstIndexRouteDest, &NextIndexRouteDest,
             i4FirstIndexPrefixLen, &i4NextIndexPrefixLen,
             i4FirstIndexProtocol, &i4NextIndexProtocol,
             &FirstIndexNextHop, &NextIndexNextHop);

        if ((u4FirstVcId == u4NextVcId) &&
            (MEMCMP
             (FirstIndexRouteDest.pu1_OctetList,
              NextIndexRouteDest.pu1_OctetList, 16) != 0))
        {
            MEMCPY (&(InRtInfo.dst), NextIndexRouteDest.pu1_OctetList,
                    sizeof (tIp6Addr));
            InRtInfo.u1Prefixlen = (UINT1) i4NextIndexPrefixLen;
            Rtm6GetBestRouteEntryInCxt (u4NextVcId, &InRtInfo, &pIp6BestRt);
            if ((pIp6BestRt != NULL) && (pIp6BestRt->u4Flag & RTM6_ECMP_RT))
            {
                pTmp6RtEntry = pIp6BestRt;
                while ((pTmp6RtEntry != NULL)
                       && (pTmp6RtEntry->u4Metric == pIp6BestRt->u4Metric))
                {
                    u1ECMPRtCnt++;
                    pTmp6RtEntry = pTmp6RtEntry->pNextAlternatepath;
                }
            }

        }

        if ((u4FirstVcId != u4NextVcId) || (i4RetVal == SNMP_FAILURE))
        {
            /*All info of a VcId are obtained, so print them */
            /*Display Header */
            CliPrintf (CliHandle, "IPv6 Routing Table Summary - %d entries\r\n",
                       u4RouteCount - u4InvalidRtCount);
            CliPrintf (CliHandle,
                       "    %d Connected, %d Static, %d RIP, %d BGP, %d OSPF , %d ISIS\t\r\n",
                       u4ConnectedCount, u4StaticCount, u4RipCount, u4BgpCount,
                       u4OspfCount, u4IsisCount);
            CliPrintf (CliHandle, "    Number of prefixes:\r\n");
            CliPrintf (CliHandle, "    Total Number of ECMP6 routes: %d\r\n",
                       u1ECMPRtCnt);
            /* To display the failed route entries from FRT table */
            Rtm6GetFrtTableCount (&u4FailedRtCount);
            CliPrintf (CliHandle,
                       "    Total Number of IPV6 Failed routes: %d\r\n",
                       u4FailedRtCount);
            for (u4Index = 0; u4Index <= 128; u4Index++)
            {
                if (au4PrefixCount[u4Index] != 0)
                {

                    CliPrintf (CliHandle, "    /%d: %d\r\n", u4Index,
                               au4PrefixCount[u4Index]);
                }
                au4PrefixCount[u4Index] = 0;
            }
            u4RouteCount = 0;
            u4ConnectedCount = 0;
            u4StaticCount = 0;
            u4RipCount = 0;
            u4BgpCount = 0;
            u4IsisCount = 0;
            u4OspfCount = 0;

            if (u4ShowAllCxt == FALSE)
            {
                /*Finiched displaying entries of specified VRF */
                break;
            }
        }
    }                            /*end of while */

    return;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : Ipv6ShowRunningConfigInCxt                         */
/*                                                                           */
/*     DESCRIPTION      : This function  displays the IPV6  Configuration    */
/*                                                                           */
/*     INPUT            : tCliHandle-Handle to the Cli Context               */
/*                        u4Module - flag                                    */
/*                        u4ContextId - VRF ID                               */
/*                                                                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/
INT4
Ipv6ShowRunningConfigInCxt (tCliHandle CliHandle, UINT4 u4Module,
                            UINT4 u4ContextId)
{
    Ipv6ShowRunningConfigScalarsInCxt (CliHandle, u4ContextId);
    Ipv6ShowRunningConfigTablesInCxt (CliHandle, u4ContextId);
    if (u4Module == ISS_IPV6_SHOW_RUNNING_CONFIG)
    {
        Ipv6ShowRunningConfigInterfaceInCxt (CliHandle, u4ContextId);
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : Ipv6ShowRunningConfigScalarsInCxt                  */
/*                                                                           */
/*     DESCRIPTION      : This function displays scalar objects in Ipv6 for  */
/*                        show running configuration.                        */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        u4ContextId - VRF Id                               */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/
VOID
Ipv6ShowRunningConfigScalarsInCxt (tCliHandle CliHandle, UINT4 u4ContextId)
{
    INT4                i4RetVal;
    INT1                i1Return = SNMP_FAILURE;
    UINT1               au1ContextName[VCMALIAS_MAX_ARRAY_LEN];
    INT4                i4EcmpTmrValue;
    UINT1               u1BangStatus = FALSE;

    /*Ipv6 Admin Status */
    /* Forwarding is enabled by default
     * Displaying it to tell the user that the router is a ipv6 router
     * Also when its disabled since its not the default value*/

    if (u4ContextId != VCM_DEFAULT_CONTEXT)
    {
        /*Get context name from context Id */
        VcmGetAliasName (u4ContextId, au1ContextName);
    }

    nmhGetFsMIStdIpv6IpForwarding (u4ContextId, &i4RetVal);

    if (i4RetVal == IP6_FORW_DISABLE)
    {
        u1BangStatus = TRUE;
        CliPrintf (CliHandle, "no ipv6 unicast-routing");
        if (u4ContextId != VCM_DEFAULT_CONTEXT)
        {
            CliPrintf (CliHandle, " vrf %s", au1ContextName);
        }
        CliPrintf (CliHandle, "\r\n");
    }

    /* Default Hop Limit */
    i1Return = nmhGetFsMIStdIpv6IpDefaultHopLimit (u4ContextId, &i4RetVal);

    if ((i1Return == SNMP_SUCCESS) && (i4RetVal != IP6_IF_DEF_HOPLMT))
    {
        u1BangStatus = TRUE;
        if (u4ContextId == VCM_DEFAULT_CONTEXT)
        {
            CliPrintf (CliHandle, "ipv6 default-hop limit %d \r\n", i4RetVal);
        }
        else
        {
            CliPrintf (CliHandle, "ipv6 default-hop limit vrf %s %d \r\n",
                       au1ContextName, i4RetVal);
        }
    }

    if (nmhGetFsMIIpv6RFC5095Compatibility (u4ContextId, &i4RetVal)
        == SNMP_SUCCESS)
    {
        if (i4RetVal != IP6_RFC5095_COMPATIBLE)
        {
            u1BangStatus = TRUE;
            if (u4ContextId == VCM_DEFAULT_CONTEXT)
            {
                CliPrintf (CliHandle, "no ipv6 compatible rfc5095 \r\n",
                           i4RetVal);
            }
            else
            {
                CliPrintf (CliHandle, "no ipv6 compatible rfc5095 vrf %s \r\n",
                           au1ContextName);
            }
        }
    }
#ifdef LNXIP6_WANTED
    if (nmhGetFsipv6RFC5942Compatibility (&i4RetVal) == SNMP_SUCCESS)
    {
        if (i4RetVal != IP6_RFC5942_NOT_COMPATIBLE)
        {
            u1BangStatus = TRUE;
            CliPrintf (CliHandle, "ipv6 compatible rfc5942\r\n", i4RetVal);
        }
    }
#endif

    if (nmhGetFsMIIpv6SENDSecLevel ((INT4) u4ContextId, &i4RetVal)
        == SNMP_SUCCESS)
    {
        if (i4RetVal == ND6_SECURE_ENABLE)
        {
            u1BangStatus = TRUE;
            CliPrintf (CliHandle, "ipv6 cga modifier sec-level %d \r\n",
                       i4RetVal);
        }
    }

    if (nmhGetFsMIIpv6SENDNbrSecLevel ((INT4) u4ContextId, &i4RetVal)
        == SNMP_SUCCESS)
    {
        if (i4RetVal == ND6_SECURE_ENABLE)
        {
            u1BangStatus = TRUE;
            CliPrintf (CliHandle, "ipv6 nd secured sec-level %d \r\n",
                       i4RetVal);
        }
    }

    if (nmhGetFsMIIpv6SENDSecDAD ((INT4) u4ContextId, &i4RetVal)
        == SNMP_SUCCESS)
    {
        if (i4RetVal == ND6_ACCEPT_UNSEC_ADV_DISABLE)
        {
            u1BangStatus = TRUE;
            CliPrintf (CliHandle,
                       "no ipv6 nd secured accept unsecure-advertisements \r\n");
        }
    }

    if (nmhGetFsMIIpv6SENDAuthType ((INT4) u4ContextId, &i4RetVal)
        == SNMP_SUCCESS)
    {
        if (i4RetVal == ND6_AUTH_CGA_TA)
        {
            u1BangStatus = TRUE;
            CliPrintf (CliHandle, "ipv6 nd secured auth-type cga-and-ta \r\n",
                       i4RetVal);
        }
        else if (i4RetVal == ND6_AUTH_TA)
        {
            u1BangStatus = TRUE;
            CliPrintf (CliHandle, "ipv6 nd secured auth-type ta \r\n",
                       i4RetVal);
        }
        else if (i4RetVal == ND6_AUTH_ANYONE)
        {
            u1BangStatus = TRUE;
            CliPrintf (CliHandle, "ipv6 nd secured auth-type cga-or-ta \r\n",
                       i4RetVal);
        }
    }
    /*ECMP PRT TImer */
    if (nmhGetFsipv6ECMPPRTTimer (&i4EcmpTmrValue) == SNMP_SUCCESS)
    {
        if ((i4EcmpTmrValue != IP6_ECMP6PRT_TIMER_INTERVAL) &&
            (i4EcmpTmrValue != IP6_ECMP_MIN_PRT_INTERVAL))
        {
            u1BangStatus = TRUE;
            CliPrintf (CliHandle, "ipv6 ecmp-prt timer %d \r\n",
                       i4EcmpTmrValue);
        }
    }
    /*IPv6 ND cache Timeout */
    if (nmhGetFsipv6NdCacheTimeout (&i4RetVal) == SNMP_SUCCESS)
    {
        if (i4RetVal != ND_DEF_CACHE_TIMEOUT)
        {
            u1BangStatus = TRUE;
            CliPrintf (CliHandle, "ipv6 nd cache-timeout %d \r\n", i4RetVal);
        }
    }

    if (u1BangStatus == TRUE)
    {
        CliSetBangStatus (CliHandle, TRUE);
    }
    return;
}

/*****************************************************************************/
/*     FUNCTION NAME    : Ipv6ShowRunningConfigTablesInCxt                   */
/*                                                                           */
/*     DESCRIPTION      : This function displays interface objects in Ipv6   */
/*                        for show running configuration.                    */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        u4ContextId - VRF Id                               */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : VOID                                               */
/*****************************************************************************/
VOID
Ipv6ShowRunningConfigTablesInCxt (tCliHandle CliHandle, UINT4 u4ContextId)
{
#ifndef LNXIP6_WANTED
    tIp6ScopeZoneInfo  *pScopeZoneInfo = NULL;
    tIp6ScopeZoneInfo  *pNextScopeZoneInfo = NULL;
#endif
    INT1               *pi1IfName = NULL;
    INT1                ai1IfaceName[CFA_CLI_MAX_IF_NAME_LEN];
    INT4                i4RetVal = 0;
    INT4                i4MetricType5val = 0;
#ifndef LNXIP6_WANTED            /* FSIP */
    INT4                i4IpNetToMediaIfIndex = 1;
    INT4                i4NextIpNetToMediaIfIndex = 0;
    INT4                i4IpNetToMediaType = 0;
    INT4                i4IpNetToPhysicalNetAddressType = INET_ADDR_TYPE_IPV6;
    INT4                i4NextIpNetToPhysicalNetAddressType =
        INET_ADDR_TYPE_IPV6;
#endif /* IP_WANTED */
    CHR1               *pu1Address = NULL;
    UINT1               au1ContextName[VCMALIAS_MAX_ARRAY_LEN];
    UINT1               au1Address[MAX_ADDR_LEN];
    UINT1               au1PhyAddress[MAX_ADDR_LEN];
    UINT1               u1BangStatus = FALSE;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    tSNMP_OCTET_STRING_TYPE OctetStrIpNetToMediaPhysAddress;

/*For RouteTable*/
    UINT4               u4NextContextId = 0;
    UINT1               au1RouteDest[MAX_ADDR_LEN];
    UINT1               au1NextRouteDest[MAX_ADDR_LEN];
    UINT1               au1RouteNextHop[MAX_ADDR_LEN];
    UINT1               au1NextRouteNextHop[MAX_ADDR_LEN];
    INT4                i4NextRouteDestType = INET_ADDR_TYPE_IPV6;
    INT4                i4FsIpRouteProto = 0;
    INT4                i4FsIpNextRouteProto = 0;
    tSNMP_OCTET_STRING_TYPE RouteDest;
    tSNMP_OCTET_STRING_TYPE NextRouteDest;
    UINT4               u4RoutePfxLen = 0;
    UINT4               u4NextRoutePfxLen = 0;
    UINT4               u4RtPolicy[IPVX_TWO];
    UINT4               u4NextRtPolicy[IPVX_TWO];
    tSNMP_OID_TYPE      RoutePolicy;
    tSNMP_OID_TYPE      NextRoutePolicy;
    INT4                i4RouteNextHopType = 0;
    INT4                i4RouteAddrType = 0;
    tSNMP_OCTET_STRING_TYPE RouteNextHop;
    tSNMP_OCTET_STRING_TYPE NextRouteNextHop;

    /*For NetToPhysicalTable */
    UINT1               au1NetToPhysicalIp[MAX_ADDR_LEN];
    UINT1               au1NextNetToPhysicalIp[MAX_ADDR_LEN];
    tSNMP_OCTET_STRING_TYPE NextIpNetToPhysicalNetAddress;
    tSNMP_OCTET_STRING_TYPE NetToPhysicalNetAddress;

    UINT1               Ip6Address[IP6_ADDR_SIZE];
    UINT1               Ip6NextAddress[IP6_ADDR_SIZE];
    UINT4               u4ShowAllCxt = FALSE;
    INT4                i4CurrentVcId = 0;
    INT4                i4PathMtu = 0;
    INT4                i4VcId = VCM_INVALID_VC;
    INT4                i4Status = IP6_PMTU_DISABLE;
    INT4                i4Ip6AdminDistance = IP6_ZERO;
    tSNMP_OCTET_STRING_TYPE Ipv6PmtuDest;
    tSNMP_OCTET_STRING_TYPE Ipv6PmtuNextDest;

    MEMSET (u4RtPolicy, ZERO, IPVX_TWO);
    MEMSET (u4NextRtPolicy, ZERO, IPVX_TWO);
    MEMSET (au1NetToPhysicalIp, ZERO, MAX_ADDR_LEN);
    MEMSET (au1NextNetToPhysicalIp, ZERO, MAX_ADDR_LEN);

    MEMSET (au1RouteDest, IP6_UINT1_ALL_ONE, MAX_ADDR_LEN);
    MEMSET (au1NextRouteDest, ZERO, MAX_ADDR_LEN);
    MEMSET (au1RouteNextHop, IP6_UINT1_ALL_ONE, MAX_ADDR_LEN);
    MEMSET (au1NextRouteNextHop, ZERO, MAX_ADDR_LEN);
    /* Added for RFC4007 show running config details */

    NetToPhysicalNetAddress.pu1_OctetList = au1NetToPhysicalIp;
    NextIpNetToPhysicalNetAddress.pu1_OctetList = au1NextNetToPhysicalIp;

    RoutePolicy.pu4_OidList = u4RtPolicy;
    NextRoutePolicy.pu4_OidList = u4NextRtPolicy;
    RouteDest.pu1_OctetList = au1RouteDest;
    NextRouteDest.pu1_OctetList = au1NextRouteDest;
    RouteNextHop.pu1_OctetList = au1RouteNextHop;
    NextRouteNextHop.pu1_OctetList = au1NextRouteNextHop;
    /* Added for RFC4007 show running config details */

    RoutePolicy.u4_Length = IP6_TWO;
    NextRoutePolicy.u4_Length = IP6_TWO;
    NetToPhysicalNetAddress.i4_Length = IP6_ZERO;
    NextIpNetToPhysicalNetAddress.i4_Length = IP6_ZERO;

    RouteDest.i4_Length = IP6_ADDR_SIZE;
    NextRouteDest.i4_Length = IP6_ZERO;
    RouteNextHop.i4_Length = IP6_ADDR_SIZE;
    NextRouteNextHop.i4_Length = IP6_ZERO;
    /* Added for RFC4007 show running config details */

    MEMSET (au1Address, IP_ZERO, MAX_ADDR_LEN);
    MEMSET (ai1IfaceName, IP_ZERO, CFA_CLI_MAX_IF_NAME_LEN);

    pu1Address = (CHR1 *) & au1Address[IP_ZERO];
    pi1IfName = &ai1IfaceName[IP_ZERO];

    OctetStrIpNetToMediaPhysAddress.pu1_OctetList = &au1PhyAddress[IP_ZERO];

    MEMSET (&Ipv6PmtuDest, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&Ipv6PmtuNextDest, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&Ip6Address, 0, IP6_ADDR_SIZE);
    Ipv6PmtuDest.pu1_OctetList = &Ip6Address[0];
    MEMSET (&Ip6NextAddress, 0, IPVX_IPV6_ADDR_LEN);
    Ipv6PmtuNextDest.pu1_OctetList = &Ip6NextAddress[0];

    /* Get default distance of IPv6 static route for given context */
    VcmGetAliasName (u4ContextId, au1ContextName);
    nmhGetFsMIRtm6StaticRouteDistance ((INT4) u4ContextId, &i4Ip6AdminDistance);

    if (i4Ip6AdminDistance != IP6_PREFERENCE_LOCAL)
    {
        u1BangStatus = TRUE;
        /* Default administrative distance for static IPv6 route */
        if (u4ContextId == IP_DEFAULT_CONTEXT)
        {
            CliPrintf (CliHandle, "ipv6 default-distance");

        }
        else
        {
            CliPrintf (CliHandle, "ipv6 default-distance vrf %s",
                       au1ContextName);

        }
        CliPrintf (CliHandle, " %d ", i4Ip6AdminDistance);
        CliPrintf (CliHandle, "\r\n");
    }

#ifndef LNXIP6_WANTED            /* FSIP */
    if (nmhGetFirstIndexFsMIStdIpNetToPhysicalTable
        (&i4NextIpNetToMediaIfIndex,
         &i4NextIpNetToPhysicalNetAddressType,
         &NextIpNetToPhysicalNetAddress) == SNMP_SUCCESS)
    {
        do
        {
            i4IpNetToMediaIfIndex = i4NextIpNetToMediaIfIndex;
            NetToPhysicalNetAddress.i4_Length
                = NextIpNetToPhysicalNetAddress.i4_Length;
            MEMCPY (NetToPhysicalNetAddress.pu1_OctetList,
                    NextIpNetToPhysicalNetAddress.pu1_OctetList,
                    NetToPhysicalNetAddress.i4_Length);
            i4IpNetToPhysicalNetAddressType =
                i4NextIpNetToPhysicalNetAddressType;

            if (i4NextIpNetToPhysicalNetAddressType != INET_ADDR_TYPE_IPV6)
            {
                continue;
            }

            nmhGetFsMIStdIpNetToPhysicalContextId
                (i4NextIpNetToMediaIfIndex,
                 i4NextIpNetToPhysicalNetAddressType,
                 &NextIpNetToPhysicalNetAddress, (INT4 *) &u4NextContextId);

            if (u4NextContextId != u4ContextId)
            {
                continue;
            }

            nmhGetFsipv6NdLanCacheState (i4NextIpNetToMediaIfIndex,
                                         &NextIpNetToPhysicalNetAddress,
                                         &i4IpNetToMediaType);
            if (i4IpNetToMediaType != ND6C_STATIC)
            {
                continue;
            }
            nmhGetFsMIStdIpNetToPhysicalType (i4NextIpNetToMediaIfIndex,
                                              i4NextIpNetToPhysicalNetAddressType,
                                              &NextIpNetToPhysicalNetAddress,
                                              &i4RetVal);

            MEMSET (au1Address, IP_ZERO, MAX_ADDR_LEN);
            MEMSET (au1PhyAddress, IP_ZERO, MAX_ADDR_LEN);
            MEMSET (ai1IfaceName, IP_ZERO, CFA_CLI_MAX_IF_NAME_LEN);

            nmhGetFsMIStdIpNetToPhysicalPhysAddress (i4NextIpNetToMediaIfIndex,
                                                     i4NextIpNetToPhysicalNetAddressType,
                                                     &NextIpNetToPhysicalNetAddress,
                                                     &OctetStrIpNetToMediaPhysAddress);

            CfaCliConfGetIfName ((UINT4) i4NextIpNetToMediaIfIndex, pi1IfName);
            u1BangStatus = TRUE;
            if (u4ContextId == IP6_DEFAULT_CONTEXT)
            {
                CliPrintf (CliHandle, "ipv6 neighbor");
            }
            else
            {
                /*Get context name from context Id */
                VcmGetAliasName (u4ContextId, au1ContextName);
                CliPrintf (CliHandle, "ipv6 neighbor vrf %s", au1ContextName);
            }

            CliPrintf (CliHandle, " %s",
                       Ip6PrintNtop ((tIp6Addr *) (VOID *)
                                     NextIpNetToPhysicalNetAddress.
                                     pu1_OctetList));

            /* Print the interface name */

            CliPrintf (CliHandle, " %s", pi1IfName);

            /*Call macro for MAC Address to String format, to Print */
            MEMSET (au1Address, IP_ZERO, MAX_ADDR_LEN);
            CLI_CONVERT_MAC_TO_DOT_STR (OctetStrIpNetToMediaPhysAddress.
                                        pu1_OctetList, (UINT1 *) pu1Address);

            u4PagingStatus = CliPrintf (CliHandle, " %s\r\n", pu1Address);

            if (u4PagingStatus == CLI_FAILURE)
            {
                /* 
                 * User pressed 'q' at more prompt, no more print required, exit 
                 * 
                 * Setting the Count to -1 will result in not displaying the
                 * count of the entries.
                 */
                break;
            }
        }
        while (nmhGetNextIndexFsMIStdIpNetToPhysicalTable
               (i4IpNetToMediaIfIndex, &i4NextIpNetToMediaIfIndex,
                i4IpNetToPhysicalNetAddressType,
                &i4NextIpNetToPhysicalNetAddressType, &NetToPhysicalNetAddress,
                &NextIpNetToPhysicalNetAddress) != SNMP_FAILURE);

    }
#else
    UNUSED_PARAM (pu1Address);
    UNUSED_PARAM (OctetStrIpNetToMediaPhysAddress);
    UNUSED_PARAM (NextIpNetToPhysicalNetAddress);
    UNUSED_PARAM (NetToPhysicalNetAddress);
#endif /* IP_WANTED */

    if (UtilRtm6SetContext (u4ContextId) == SNMP_FAILURE)
    {
        return;
    }

    u4NextContextId = u4ContextId;
    /*Get from fsipv6RouteTable */
    while (nmhGetNextIndexFsipv6RouteTable
           (&RouteDest, &NextRouteDest,
            (INT4) u4RoutePfxLen, (INT4 *) &u4NextRoutePfxLen,
            i4FsIpRouteProto, &i4FsIpNextRouteProto,
            &RouteNextHop, &NextRouteNextHop) != SNMP_FAILURE)
    {
        /*Get from fsipv6RouteTable */
        u4RoutePfxLen = u4NextRoutePfxLen;
        i4FsIpRouteProto = i4FsIpNextRouteProto;
        MEMCPY (RoutePolicy.pu4_OidList, NextRoutePolicy.pu4_OidList, IPVX_TWO);
        MEMCPY (RouteDest.pu1_OctetList, NextRouteDest.pu1_OctetList,
                sizeof (tIp6Addr));
        MEMCPY (RouteNextHop.pu1_OctetList, NextRouteNextHop.pu1_OctetList,
                sizeof (tIp6Addr));
        RouteDest.i4_Length = NextRouteDest.i4_Length;
        RouteNextHop.i4_Length = NextRouteNextHop.i4_Length;

        nmhGetFsMIStdInetCidrRouteStatus (u4NextContextId, i4NextRouteDestType,
                                          &NextRouteDest, u4NextRoutePfxLen,
                                          &NextRoutePolicy, i4RouteNextHopType,
                                          &NextRouteNextHop, &i4RetVal);

        nmhGetFsMIStdInetCidrRouteProto (u4NextContextId, i4NextRouteDestType,
                                         &NextRouteDest, u4NextRoutePfxLen,
                                         &NextRoutePolicy, i4RouteNextHopType,
                                         &NextRouteNextHop, &i4FsIpRouteProto);

        if (i4FsIpRouteProto == CIDR_STATIC_ID)
        {
            nmhGetFsipv6RouteIfIndex (&NextRouteDest,
                                      u4NextRoutePfxLen, i4FsIpRouteProto,
                                      &NextRouteNextHop, &i4RetVal);
            CfaCliConfGetIfName ((UINT4) i4RetVal, pi1IfName);

            nmhGetFsipv6RoutePreference (&NextRouteDest,
                                         (INT4) u4NextRoutePfxLen,
                                         i4FsIpRouteProto, &NextRouteNextHop,
                                         &i4RetVal);

            nmhGetFsipv6RouteAddrType (&NextRouteDest, u4NextRoutePfxLen,
                                       i4FsIpRouteProto, &NextRouteNextHop,
                                       &i4RouteAddrType);

            nmhGetFsMIStdInetCidrRouteMetric5 ((INT4) u4NextContextId,
                                               i4NextRouteDestType,
                                               &NextRouteDest,
                                               u4NextRoutePfxLen,
                                               &NextRoutePolicy,
                                               i4RouteNextHopType,
                                               &NextRouteNextHop,
                                               &i4MetricType5val);

            if (i4MetricType5val == IPVX_ONE)
            {
                /*no need to display dhcpv6 relay pd route  */
                continue;
            }
            u1BangStatus = TRUE;
            if (u4ContextId == IP6_DEFAULT_CONTEXT)
            {
                CliPrintf (CliHandle, "ipv6 route");
            }
            else
            {
                /*Get context name from context Id */
                VcmGetAliasName (u4ContextId, au1ContextName);
                CliPrintf (CliHandle, "ipv6 route vrf %s", au1ContextName);
            }
            CliPrintf (CliHandle, " %s",
                       Ip6PrintNtop ((tIp6Addr *) (VOID *)
                                     NextRouteDest.pu1_OctetList));
            CliPrintf (CliHandle, " %d ", u4NextRoutePfxLen);

            if (i4RouteAddrType != ADDR6_ANYCAST)
            {
                if (NextRouteNextHop.pu1_OctetList[0] != '\0')
                {
                    CliPrintf (CliHandle, " %s",
                               Ip6PrintNtop ((tIp6Addr *) (VOID *)
                                             NextRouteNextHop.pu1_OctetList));
                }
                else
                {
                    CliPrintf (CliHandle, " %s", pi1IfName);
                }
            }
            else
            {
                if (NextRouteNextHop.pu1_OctetList[0] != '\0')
                {
                    CliPrintf (CliHandle, " %s",
                               Ip6PrintNtop ((tIp6Addr *) (VOID *)
                                             NextRouteNextHop.pu1_OctetList));
                }
                else
                {
                    CliPrintf (CliHandle, ":: ");
                }
                CliPrintf (CliHandle, " %s ", pi1IfName);
                CliPrintf (CliHandle, " anycast");
            }
            if (i4RetVal != i4Ip6AdminDistance)
            {
                CliPrintf (CliHandle, " %d", i4RetVal);
            }
            u4PagingStatus = CliPrintf (CliHandle, "\r\n");

        }
        if (u4PagingStatus == CLI_FAILURE)
        {
            /*
             * User pressed 'q' at more prompt, no more print required, exit
             *
             */
            break;
        }
    }
    UtilRtm6ResetContext ();

/* PMTU - get from Ipv6PmtuTable */

    if (u4ContextId == VCM_INVALID_VC)
    {
        i4RetVal = nmhGetFirstIndexFsMIIpv6ContextTable (&i4VcId);
        u4ShowAllCxt = TRUE;
    }
    else
    {
        i4VcId = (INT4) u4ContextId;
        i4RetVal = SNMP_SUCCESS;
    }
    while (i4RetVal != SNMP_FAILURE)
    {
        i4Status = IP6_PMTU_DISABLE;
        VcmGetAliasName (i4VcId, au1ContextName);
        nmhGetFsMIIpv6PmtuConfigStatus (i4VcId, &i4Status);
        i4CurrentVcId = i4VcId;
        i4RetVal = nmhGetNextIndexFsMIIpv6ContextTable (i4CurrentVcId, &i4VcId);
        if (u4ShowAllCxt == FALSE)
            break;
    }

    i4RetVal = SNMP_FAILURE;
    i4VcId = (INT4) u4ContextId;

    if (u4ContextId == VCM_INVALID_VC)
    {
        u4ShowAllCxt = TRUE;
        i4RetVal =
            nmhGetFirstIndexFsMIIpv6PmtuTable (&i4VcId, &Ipv6PmtuNextDest);
    }
    else
    {
        i4CurrentVcId = i4VcId;
        i4RetVal = nmhGetNextIndexFsMIIpv6PmtuTable
            (i4CurrentVcId, &i4VcId, &Ipv6PmtuDest, &Ipv6PmtuNextDest);
    }

    if (i4Status == IP6_PMTU_ENABLE)
    {
        while (i4RetVal == SNMP_SUCCESS)
        {
            VcmGetAliasName (i4VcId, au1ContextName);

            nmhGetFsMIIpv6Pmtu (i4VcId, &Ipv6PmtuNextDest, &i4PathMtu);
            u1BangStatus = TRUE;
            CliPrintf (CliHandle, "ipv6 path mtu %s %d\n",
                       Ip6PrintNtop ((tIp6Addr *) (VOID *) Ipv6PmtuNextDest.
                                     pu1_OctetList), i4PathMtu);

            i4CurrentVcId = i4VcId;

            MEMCPY (Ipv6PmtuDest.pu1_OctetList, Ipv6PmtuNextDest.pu1_OctetList,
                    Ipv6PmtuNextDest.i4_Length);
            Ipv6PmtuDest.i4_Length = Ipv6PmtuNextDest.i4_Length;

            i4RetVal = nmhGetNextIndexFsMIIpv6PmtuTable
                (i4CurrentVcId, &i4VcId, &Ipv6PmtuDest, &Ipv6PmtuNextDest);

            if ((i4CurrentVcId != i4VcId) && (u4ShowAllCxt == FALSE))
            {
                /*Finished displaying entries belonging to one VRF */
                break;
            }
        }                        /*end of while */
    }

#ifndef LNXIP6_WANTED
    pScopeZoneInfo =
        (tIp6ScopeZoneInfo *) RBTreeGetFirst (gIp6GblInfo.ScopeZoneTree);

    while (pScopeZoneInfo != NULL)
    {
        if ((pScopeZoneInfo->u4ContextId == u4ContextId) &&
            (pScopeZoneInfo->u1IsDefaultZone == IP6_ZONE_TRUE))
        {
            if (STRNCMP
                (pScopeZoneInfo->au1ZoneName, "global", STRLEN ("global")) != 0)
            {
                Ip6ZoneGetZoneIdFromZoneName (pScopeZoneInfo->au1ZoneName,
                                              &pScopeZoneInfo->u1Scope,
                                              &pScopeZoneInfo->i4ZoneId);
                VcmGetAliasName (u4ContextId, au1ContextName);
                if (((STRNCMP
                      (gIp6GblInfo.asIp6ScopeName[pScopeZoneInfo->u1Scope].
                       au1ZoneName, "interfacelocal",
                       STRLEN ("interfacelocal")))
                     &&
                     (STRNCMP
                      (gIp6GblInfo.asIp6ScopeName[pScopeZoneInfo->u1Scope].
                       au1ZoneName, "linklocal", STRLEN ("linklocal"))))
                    && (pScopeZoneInfo->i4ZoneId != 1))
                {
                    u1BangStatus = TRUE;
                    CliPrintf (CliHandle, "#Context: %s\r\n", au1ContextName);
                    CliPrintf (CliHandle, "ipv6 default scope-zone %s %d",
                               gIp6GblInfo.asIp6ScopeName[pScopeZoneInfo->
                                                          u1Scope].au1ZoneName,
                               pScopeZoneInfo->i4ZoneId);
                    CliPrintf (CliHandle, "\r\n");
                }
            }
        }
        if (pScopeZoneInfo->u4ContextId > u4ContextId)
            break;

        pNextScopeZoneInfo =
            (tIp6ScopeZoneInfo *) RBTreeGetNext (gIp6GblInfo.ScopeZoneTree,
                                                 (tRBElem *) pScopeZoneInfo,
                                                 NULL);
        pScopeZoneInfo = pNextScopeZoneInfo;

    }
#endif
    if (u1BangStatus == TRUE)
    {
        CliSetBangStatus (CliHandle, TRUE);
    }
    return;

}

/*****************************************************************************/
/*     FUNCTION NAME    : Fsipv6IfTableInfo                                  */
/*                                                                           */
/*     DESCRIPTION      : This function displays fsipv6IfTable  objects      */
/*                        for show running configuration.                    */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : VOID                                               */
/*****************************************************************************/
VOID
Fsipv6IfTableInfo (tCliHandle CliHandle, INT4 i4IfIndex)
{
#ifndef LNXIP6_WANTED
    tIp6If             *pIf6 = NULL;
#else
    tLip6If            *pIf6 = NULL;
#endif
    INT4                i4TableObject = 0;
    INT4                i4DefaultIfIndex = IPVX_ZERO;
    INT4                i4RAObjVal = IPVX_ZERO;
    INT4                i4NDProxyAdminStat = IPVX_ZERO;
    INT4                i4NDProxyMode = IPVX_ZERO;
    INT4                i4NDProxyUpstream = IPVX_ZERO;
    INT4                i4SeNDSecStatus = 0;
    UINT4               u4SeNDDeltaTimeStamp = 0;
    UINT4               u4SeNDDriftTimeStamp = 0;
    UINT4               u4SeNDFuzzTimeStamp = 0;
    INT4                i4DefRoutePreference = 0;
    UINT4               u4RAObjVal = IPVX_ZERO;
    UINT4               u4RAObjVal2 = IPVX_ZERO;
    INT1                i1RetVal = SNMP_FAILURE;
    tSNMP_OCTET_STRING_TYPE taddress;
    UINT1               au1RDNSSAddr[IP6_ADDR_SIZE];
    taddress.pu1_OctetList = au1RDNSSAddr;
    taddress.i4_Length = IP6_ADDR_SIZE;
    tSNMP_OCTET_STRING_TYPE DNSAddr, DNSAddr1, DNSAddr2;
    UINT1               au1Buf1[IP6_DOMAIN_NAME_LEN];
    UINT1               au1Buf2[IP6_DOMAIN_NAME_LEN];
    UINT1               au1Buf3[IP6_DOMAIN_NAME_LEN];
    UINT1               u1BangStatus = FALSE;
    IP6_INIT_OCT (DNSAddr, au1Buf1);
    IP6_INIT_OCT (DNSAddr1, au1Buf2);
    IP6_INIT_OCT (DNSAddr2, au1Buf3);
    DNSAddr.pu1_OctetList = au1Buf1;
    DNSAddr.i4_Length = IP6_DOMAIN_NAME_LEN;
    DNSAddr1.pu1_OctetList = au1Buf2;
    DNSAddr1.i4_Length = IP6_DOMAIN_NAME_LEN;
    DNSAddr2.pu1_OctetList = au1Buf3;
    DNSAddr2.i4_Length = IP6_DOMAIN_NAME_LEN;

    /* interface admin status */
    nmhGetFsipv6IfAdminStatus (i4IfIndex, &i4TableObject);
    i4DefaultIfIndex = (INT4) CfaGetDefaultVlanInterfaceIndex ();

    if ((i4TableObject == ADMIN_UP))
    {
        u1BangStatus = TRUE;
        CliPrintf (CliHandle, " ipv6 enable \r\n");
    }
    else if ((i4TableObject != ADMIN_UP) && (i4IfIndex == i4DefaultIfIndex))
    {
        u1BangStatus = TRUE;
        CliPrintf (CliHandle, " no ipv6 enable \r\n");
    }

    nmhGetFsipv6IfForwarding (i4IfIndex, &i4TableObject);

    if (i4TableObject != IP6_IF_FORW_ENABLE)
    {
        u1BangStatus = TRUE;
        CliPrintf (CliHandle, " no ipv6 unicast-routing\r\n");
    }

    nmhGetFsipv6IfNDProxyAdminStatus (i4IfIndex, &i4NDProxyAdminStat);

    if (i4NDProxyAdminStat == ND6_IF_PROXY_ADMIN_UP)
    {
        u1BangStatus = TRUE;
        CliPrintf (CliHandle, " ipv6 nd proxy \r\n");
    }

    nmhGetFsipv6IfNDProxyMode (i4IfIndex, &i4NDProxyMode);

    if (i4NDProxyMode == ND6_PROXY_MODE_LOCAL)
    {
        u1BangStatus = TRUE;
        CliPrintf (CliHandle, " ipv6 nd local-proxy \r\n");
    }

    nmhGetFsipv6IfNDProxyUpStream (i4IfIndex, &i4NDProxyUpstream);

    if (i4NDProxyUpstream == ND6_PROXY_IF_UPSTREAM)
    {
        u1BangStatus = TRUE;
        CliPrintf (CliHandle, " ipv6 nd proxy upstream \r\n");
    }

    nmhGetFsipv6IfSENDSecStatus (i4IfIndex, &i4SeNDSecStatus);
    if (i4SeNDSecStatus == ND6_SECURE_ENABLE)
    {
        u1BangStatus = TRUE;
        CliPrintf (CliHandle, " ipv6 nd secured full-secured \r\n");
    }
    if (i4SeNDSecStatus == ND6_SECURE_MIXED)
    {
        u1BangStatus = TRUE;
        CliPrintf (CliHandle, " ipv6 nd secured mixed \r\n");
    }

    nmhGetFsipv6IfSENDDeltaTimestamp (i4IfIndex, &u4SeNDDeltaTimeStamp);
    if (u4SeNDDeltaTimeStamp != ND6_SECURE_DELTA_DEFAULT)
    {
        u1BangStatus = TRUE;
        CliPrintf (CliHandle, " ipv6 nd secured timestamp delta %d \r\n",
                   u4SeNDDeltaTimeStamp);
    }

    nmhGetFsipv6IfSENDFuzzTimestamp (i4IfIndex, &u4SeNDFuzzTimeStamp);
    if (u4SeNDFuzzTimeStamp != ND6_SECURE_FUZZ_DEFAULT)
    {
        u1BangStatus = TRUE;
        CliPrintf (CliHandle, " ipv6 nd secured timestamp fuzz %d \r\n",
                   u4SeNDFuzzTimeStamp);
    }

    nmhGetFsipv6IfSENDDriftTimestamp (i4IfIndex, &u4SeNDDriftTimeStamp);
    if (u4SeNDDriftTimeStamp != ND6_SECURE_DRIFT_DEFAULT)
    {
        u1BangStatus = TRUE;
        CliPrintf (CliHandle, " ipv6 nd secured timestamp drift %d \r\n",
                   u4SeNDDriftTimeStamp);
    }

    nmhGetFsipv6IfDefRoutePreference (i4IfIndex, &i4DefRoutePreference);
    if (i4DefRoutePreference == IP6_RA_ROUTE_PREF_HIGH)
    {
        u1BangStatus = TRUE;
        CliPrintf (CliHandle, " ipv6 nd router-preference high \r\n",
                   i4DefRoutePreference);
    }
    if (i4DefRoutePreference == IP6_RA_ROUTE_PREF_LOW)
    {
        u1BangStatus = TRUE;
        CliPrintf (CliHandle, " ipv6 nd router-preference low \r\n",
                   i4DefRoutePreference);
    }
#ifndef LNXIP6_WANTED
    pIf6 = Ip6ifGetEntry ((UINT4) i4IfIndex);
#else
    pIf6 = Lip6UtlGetIfEntry (i4IfIndex);
#endif
    if (pIf6 == NULL)
    {
        return;
    }
    if (pIf6->u1IfType != IP6_LOOPBACK_INTERFACE_TYPE)
    {
        nmhGetFsipv6IfDADRetries (i4IfIndex, &i4TableObject);
        if (i4TableObject != IP6_IF_DEF_DAD_SEND)
        {
            u1BangStatus = TRUE;
            CliPrintf (CliHandle, " ipv6 nd dad attempts %d \r\n",
                       i4TableObject);
        }
    }

    if (pIf6->u1Ipv6IfFwdOperStatus == IP6_IF_FORW_DISABLE)
    {
        UNUSED_PARAM (i4RAObjVal);
        UNUSED_PARAM (i1RetVal);

        nmhGetFsipv6IfRouterAdvStatus (i4IfIndex, &i4TableObject);
        if (i4TableObject != CLI_IP6_RA_DEF_SEND_STATUS)
        {
            u1BangStatus = TRUE;
            CliPrintf (CliHandle, " no ipv6 nd suppress-ra \r\n");
        }

        nmhGetFsipv6IfRouterAdvFlags (i4IfIndex, &i4TableObject);
        if ((i4TableObject & IP6_IF_ROUT_ADV_M_BIT) ||
            ((i4TableObject & IP6_IF_ROUT_ADV_BOTH_BIT) ==
             IP6_IF_ROUT_ADV_BOTH_BIT))
        {
            u4RAObjVal = IPVX_TRUTH_VALUE_TRUE;
        }
        else
        {
            u4RAObjVal = IPVX_TRUTH_VALUE_FALSE;
        }

        if ((i4TableObject & IP6_IF_ROUT_ADV_NO_BIT) == IP6_IF_ROUT_ADV_NO_BIT)
        {
            u4RAObjVal2 = IPVX_TRUTH_VALUE_FALSE;
        }
        else if ((i4TableObject & IP6_IF_ROUT_ADV_O_BIT) ==
                 IP6_IF_ROUT_ADV_O_BIT)
        {
            u4RAObjVal2 = IPVX_TRUTH_VALUE_TRUE;
        }
        else
        {
            if (i4TableObject == IP6_IF_ROUT_ADV_BOTH_BIT)
            {
                u4RAObjVal2 = IPVX_TRUTH_VALUE_TRUE;
            }
            else
            {
                u4RAObjVal2 = IPVX_TRUTH_VALUE_FALSE;
            }
        }

        if (u4RAObjVal2 != CLI_IP6_RA_DEF_O_FLAG)
        {
            u1BangStatus = TRUE;
            CliPrintf (CliHandle, " ipv6 nd other-config flag \r\n");
        }

        if (u4RAObjVal != CLI_IP6_RA_DEF_M_FLAG)
        {
            u1BangStatus = TRUE;
            CliPrintf (CliHandle, " ipv6 nd managed-config flag \r\n");
        }

        /* Hop Limit */
        nmhGetFsipv6IfHopLimit (i4IfIndex, &i4TableObject);

        if (i4TableObject != IP6_IF_DEF_HOPLMT)
        {
            u1BangStatus = TRUE;
            CliPrintf (CliHandle, " ipv6 hop-limit %d \r\n", i4TableObject);
        }
        nmhGetFsipv6IfDefRouterTime (i4IfIndex, &i4TableObject);
        if (i4TableObject != CLI_IP6_RA_DEF_DEFTIME)
        {
            u1BangStatus = TRUE;
            CliPrintf (CliHandle, " ipv6 nd ra-lifetime %d \r\n",
                       i4TableObject);
        }

#ifndef LNXIP6_WANTED

        /* Default Reachable Time */
        nmhGetFsipv6IfReachableTime (i4IfIndex, &i4TableObject);

        if (i4TableObject != REACHABLE_TIME / 1000)
        {
            u1BangStatus = TRUE;
            CliPrintf (CliHandle, " ipv6 nd reachable-time %d \r\n",
                       i4TableObject);
        }
        nmhGetFsipv6IfRetransmitTime (i4IfIndex, &i4TableObject);
        if (i4TableObject != CLI_IP6_RA_DEF_RETTIME)
        {
            u1BangStatus = TRUE;
            CliPrintf (CliHandle, " ipv6 nd ns-interval %d \r\n",
                       i4TableObject);
        }
#else
        /* Default Reachable Time */
        nmhGetFsipv6IfReachableTime (i4IfIndex, &i4TableObject);

        if (i4TableObject != REACHABLE_TIME)
        {
            u1BangStatus = TRUE;
            CliPrintf (CliHandle, " ipv6 nd reachable-time msec %d \r\n",
                       i4TableObject);
        }

        nmhGetFsipv6IfRetransmitTime (i4IfIndex, &i4TableObject);
        if (i4TableObject != CLI_IP6_RA_DEF_RETTIME)
        {
            u1BangStatus = TRUE;
            CliPrintf (CliHandle, " ipv6 nd ns-interval %d \r\n",
                       i4TableObject);
        }

#endif

        nmhGetFsipv6IfMaxRouterAdvTime (i4IfIndex, &i4TableObject);
        if (i4TableObject != CLI_IP6_RA_DEF_MAX_RA_TIME)
        {
            u1BangStatus = TRUE;
            CliPrintf (CliHandle, " ipv6 nd ra-interval %d \r\n",
                       i4TableObject);
        }

        IP6_TASK_UNLOCK ();
        i1RetVal = nmhGetIpv6RouterAdvertLinkMTU
            (i4IfIndex, (UINT4 *) &i4TableObject);
        if (u4RAObjVal == IP6_INTERFACE_LINK_MTU)
        {
            u4RAObjVal = pIf6->u4Mtu;
        }
        IP6_TASK_LOCK ();
        if ((i1RetVal == SNMP_SUCCESS) &&
            (i4TableObject != CLI_IP6_RA_DEF_LINK_MTU))
        {
            u1BangStatus = TRUE;
            CliPrintf (CliHandle, " ipv6 nd ra-mtu %d \r\n", i4TableObject);
        }
    }
    else
    {
        /* Get the Data form the Router Configuration Objects */
        /* IPv6Lock will be taken in the IPvx Lowlevel So Relase Now and
         * before return take */
        IP6_TASK_UNLOCK ();

        i1RetVal = nmhGetIpv6RouterAdvertRowStatus (i4IfIndex, &i4RAObjVal);
        if (i1RetVal == SNMP_SUCCESS)
        {
            /* Router Adv status */
            i1RetVal =
                nmhGetIpv6RouterAdvertSendAdverts (i4IfIndex, &i4RAObjVal);
            if ((i1RetVal == SNMP_SUCCESS)
                && (i4RAObjVal != CLI_IP6_RA_DEF_SEND_STATUS))
            {
                u1BangStatus = TRUE;
                CliPrintf (CliHandle, " no ipv6 nd suppress-ra \r\n");
                i1RetVal =
                    nmhGetFsipv6IfRaRDNSSRowStatus (i4IfIndex, &i4RAObjVal);
                /*RDNSS COnfigurations */
                if ((i1RetVal == SNMP_SUCCESS) && (i4RAObjVal == IP6_RA_RDNSS))
                {
                    u1BangStatus = TRUE;
                    CliPrintf (CliHandle, " ipv6 ra rdnss ");
                    i1RetVal =
                        nmhGetFsipv6IfRaRDNSSAddrOne (i4IfIndex, (&taddress));
                    if ((i1RetVal == SNMP_SUCCESS)
                        && (taddress.pu1_OctetList[0] != '\0'))
                    {
                        CliPrintf (CliHandle, " %s",
                                   Ip6PrintNtop
                                   ((tIp6Addr *) (VOID *) taddress.
                                    pu1_OctetList));

                        i1RetVal =
                            nmhGetFsipv6IfRaRDNSSAddrOneLife (i4IfIndex,
                                                              &u4RAObjVal);
                        if ((i1RetVal == SNMP_SUCCESS)
                            && (u4RAObjVal != IP6_IF_RA_RDNSS_DEF_LIFETIME)
                            && (u4RAObjVal != IP6_ZERO))
                        {
                            CliPrintf (CliHandle, "  lifetime %u ", u4RAObjVal);
                        }
                    }
                    i1RetVal =
                        nmhGetFsipv6IfRaRDNSSAddrTwo (i4IfIndex, (&taddress));
                    if ((i1RetVal == SNMP_SUCCESS)
                        && (taddress.pu1_OctetList[0] != '\0'))
                    {
                        CliPrintf (CliHandle, " %s",
                                   Ip6PrintNtop
                                   ((tIp6Addr *) (VOID *) taddress.
                                    pu1_OctetList));
                        i1RetVal =
                            nmhGetFsipv6IfRaRDNSSAddrTwoLife (i4IfIndex,
                                                              &u4RAObjVal);
                        if ((i1RetVal == SNMP_SUCCESS)
                            && (u4RAObjVal != IP6_IF_RA_RDNSS_DEF_LIFETIME)
                            && (u4RAObjVal != IP6_ZERO))
                        {
                            CliPrintf (CliHandle, "  lifetime %u ", u4RAObjVal);
                        }

                    }
                    i1RetVal =
                        nmhGetFsipv6IfRaRDNSSAddrThree (i4IfIndex, (&taddress));
                    if ((i1RetVal == SNMP_SUCCESS)
                        && (taddress.pu1_OctetList[0] != '\0'))
                    {
                        CliPrintf (CliHandle, " %s",
                                   Ip6PrintNtop
                                   ((tIp6Addr *) (VOID *) taddress.
                                    pu1_OctetList));
                        i1RetVal =
                            nmhGetFsipv6IfRaRDNSSAddrThreeLife (i4IfIndex,
                                                                &u4RAObjVal);
                        if ((i1RetVal == SNMP_SUCCESS)
                            && (u4RAObjVal != IP6_IF_RA_RDNSS_DEF_LIFETIME)
                            && (u4RAObjVal != IP6_ZERO))
                        {
                            CliPrintf (CliHandle, " lifetime %u ", u4RAObjVal);
                        }

                    }
                    CliPrintf (CliHandle, "\n");
                }
                i1RetVal =
                    nmhGetFsipv6IfRaRDNSSRowStatus (i4IfIndex, &i4RAObjVal);
                if ((i1RetVal == SNMP_SUCCESS)
                    && (i4RAObjVal == IP6_RA_DNS_DOMNAME))

                    /*DNS COnfigurations */
                {
                    u1BangStatus = TRUE;
                    CliPrintf (CliHandle, " ipv6 nd ra dns-suffix ");
                    i1RetVal =
                        nmhGetFsipv6IfDomainNameOne (i4IfIndex, &DNSAddr);
                    if (i1RetVal == SNMP_SUCCESS)
                    {

                        CliPrintf (CliHandle, " %s", DNSAddr.pu1_OctetList);

                        i1RetVal =
                            nmhGetFsipv6IfDomainNameOneLife (i4IfIndex,
                                                             (INT4 *)
                                                             &u4RAObjVal);
                        if (i1RetVal == SNMP_SUCCESS)

                        {
                            CliPrintf (CliHandle, "  lifetime %u ", u4RAObjVal);
                        }

                    }
                    i1RetVal =
                        nmhGetFsipv6IfDomainNameTwo (i4IfIndex, &DNSAddr1);
                    if (i1RetVal == SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle, " %s", DNSAddr1.pu1_OctetList);
                        i1RetVal =
                            nmhGetFsipv6IfDomainNameTwoLife (i4IfIndex,
                                                             (INT4 *)
                                                             &u4RAObjVal);
                        if (i1RetVal == SNMP_SUCCESS)

                        {
                            CliPrintf (CliHandle, "  lifetime %u ", u4RAObjVal);
                        }

                    }
                    i1RetVal =
                        nmhGetFsipv6IfDomainNameThree (i4IfIndex, &DNSAddr2);
                    if (i1RetVal == SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle, " %s", DNSAddr2.pu1_OctetList);

                        i1RetVal =
                            nmhGetFsipv6IfDomainNameThreeLife (i4IfIndex,
                                                               (INT4 *)
                                                               &u4RAObjVal);
                        if (i1RetVal == SNMP_SUCCESS)

                        {
                            CliPrintf (CliHandle, " lifetime %u ", u4RAObjVal);
                        }
                    }
                    CliPrintf (CliHandle, "\n");
                }
            }

            /* Router Adv O Flags */
            i1RetVal = nmhGetIpv6RouterAdvertOtherConfigFlag (i4IfIndex,
                                                              &i4RAObjVal);
            if ((i1RetVal == SNMP_SUCCESS)
                && (i4RAObjVal != CLI_IP6_RA_DEF_O_FLAG))
            {
                u1BangStatus = TRUE;
                CliPrintf (CliHandle, " ipv6 nd other-config flag \r\n");
            }

            /* Router Adv M Flags */
            i1RetVal =
                nmhGetIpv6RouterAdvertManagedFlag (i4IfIndex, &i4RAObjVal);
            if ((i1RetVal == SNMP_SUCCESS)
                && (i4RAObjVal != CLI_IP6_RA_DEF_M_FLAG))
            {
                u1BangStatus = TRUE;
                CliPrintf (CliHandle, " ipv6 nd managed-config flag \r\n");
            }

            /* Hop Limit */
            i1RetVal =
                nmhGetIpv6RouterAdvertCurHopLimit (i4IfIndex, &u4RAObjVal);
            if ((i1RetVal == SNMP_SUCCESS)
                && (u4RAObjVal != CLI_IP6_RA_DEF_HOPLMT))
            {
                u1BangStatus = TRUE;
                CliPrintf (CliHandle, " ipv6 hop-limit %d \r\n", u4RAObjVal);
            }

            /* Default Router Time */
            i1RetVal = nmhGetIpv6RouterAdvertDefaultLifetime (i4IfIndex,
                                                              &u4RAObjVal);
            if ((i1RetVal == SNMP_SUCCESS) &&
                (u4RAObjVal != CLI_IP6_RA_DEF_DEFTIME))
            {
                u1BangStatus = TRUE;
                CliPrintf (CliHandle, " ipv6 nd ra-lifetime %d \r\n",
                           u4RAObjVal);
            }

            /* Reachable Time */
            i1RetVal =
                nmhGetIpv6RouterAdvertReachableTime (i4IfIndex, &u4RAObjVal);
            if ((i1RetVal == SNMP_SUCCESS)
                && (u4RAObjVal != CLI_IP6_RA_DEF_REACHTIME))
            {
                u1BangStatus = TRUE;
                CliPrintf (CliHandle, " ipv6 nd reachable-time msec %d \r\n",
                           u4RAObjVal);
            }

            /* Router Adv Retransmit time */
            i1RetVal = nmhGetIpv6RouterAdvertRetransmitTime (i4IfIndex,
                                                             &u4RAObjVal);
            if ((i1RetVal == SNMP_SUCCESS) &&
                (u4RAObjVal != CLI_IP6_RA_DEF_RETTIME))
            {
                u1BangStatus = TRUE;
                CliPrintf (CliHandle, " ipv6 nd ns-interval %d \r\n",
                           u4RAObjVal);
            }

            /* Max Router Adv Time */
            i1RetVal =
                nmhGetIpv6RouterAdvertMaxInterval (i4IfIndex, &u4RAObjVal);
            /* Min Router Adv Time */
            i1RetVal =
                nmhGetIpv6RouterAdvertMinInterval (i4IfIndex, &u4RAObjVal2);
            if (u4RAObjVal2 != CLI_IP6_RA_DEF_MIN_RA_TIME)
            {
                u1BangStatus = TRUE;
                CliPrintf (CliHandle, " ipv6 nd ra-interval %d  %d \r\n",
                           u4RAObjVal, u4RAObjVal2);
            }
            else
            {
                if (u4RAObjVal != CLI_IP6_RA_DEF_MAX_RA_TIME)
                {
                    u1BangStatus = TRUE;
                    CliPrintf (CliHandle, " ipv6 nd ra-interval %d \r\n",
                               u4RAObjVal);
                }
            }

            /* Router Advert Link MTU */
            i1RetVal = nmhGetIpv6RouterAdvertLinkMTU (i4IfIndex, &u4RAObjVal);
            if ((i1RetVal == SNMP_SUCCESS) &&
                (u4RAObjVal != CLI_IP6_RA_DEF_LINK_MTU))
            {
                u1BangStatus = TRUE;
                CliPrintf (CliHandle, " ipv6 nd ra-mtu %d \r\n", u4RAObjVal);
            }

            i1RetVal = nmhGetFsipv6IfAdvSrcLLAdr (i4IfIndex, &i4TableObject);
            if ((i1RetVal == SNMP_SUCCESS)
                && (i4TableObject == IP6_RA_ADV_LINKLOCAL))
            {
                u1BangStatus = TRUE;
                CliPrintf (CliHandle, " ipv6 ra advt-linklocal \r\n");
            }
            i1RetVal = nmhGetFsipv6IfAdvIntOpt (i4IfIndex, &i4TableObject);
            if ((i1RetVal == SNMP_SUCCESS)
                && (i4TableObject == IP6_RA_ADV_INTERVAL))
            {
                u1BangStatus = TRUE;
                CliPrintf (CliHandle, " ipv6 ra advt-interval \r\n");
            }

        }

        IP6_TASK_LOCK ();
    }
    if (u1BangStatus == TRUE)
    {
        CliSetBangStatus (CliHandle, TRUE);
    }
    return;
}

/*****************************************************************************/
/*     FUNCTION NAME    : Fsipv6PrefixProfileTableInfo                       */
/*                                                                           */
/*     DESCRIPTION      : This function displays fsipv6PrefixTable           */
/*                        and fsipv6AddrProfileTable                         */
/*                        for show running configuration.                    */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : VOID                                               */
/*****************************************************************************/
VOID
Fsipv6PrefixProfileTableInfo (tCliHandle CliHandle, INT4 i4PrefixIndex,
                              tSNMP_OCTET_STRING_TYPE * pPrefixAddrs,
                              INT4 i4PrefixAddrLen)
{
    INT4                i4TableObject = 0;
    INT4                i4ProfIndex = 0;
    INT4                i4TimeFlag = 0;
    UINT4               u4Time = 0;

    /*fsipv6PrefixTable Parameters */

    /*Ipv6 prefix admin status */
    nmhGetFsipv6PrefixAdminStatus (i4PrefixIndex,
                                   pPrefixAddrs,
                                   i4PrefixAddrLen, &i4TableObject);
    if (i4TableObject != ACTIVE)

        return;

    /*Prefix Profile Index is the index to the fsipv6AddrProfileTable */
    nmhGetFsipv6PrefixProfileIndex (i4PrefixIndex,
                                    pPrefixAddrs,
                                    i4PrefixAddrLen, &i4ProfIndex);

    /*fsipv6AddrProfileTable Parameters */

    /*Addr Profile Status */
    nmhGetFsipv6AddrProfileStatus (i4ProfIndex, &i4TableObject);
    if (i4TableObject != IP6_ADDR_PROF_VALID)

        return;

    /*Prefix address and Length */
    /* NOTE : If the prefix is set to default it cannot be printed because 
     * though the command is interface specific it is effective globally.
     * This behaviour is different from that of CISCO*/
    CliPrintf (CliHandle, " ipv6 nd prefix %s %d ",
               Ip6PrintNtop ((tIp6Addr *) (VOID *) (pPrefixAddrs->
                                                    pu1_OctetList)),
               i4PrefixAddrLen);

    /*Addr Profile Prefix Adv Status */
    /*There is a mismatch between the default values in the 
     *mib and the code*/
    nmhGetFsipv6AddrProfilePrefixAdvStatus (i4ProfIndex, &i4TableObject);
    if (i4TableObject == IP6_ADDR_PROF_PREF_ADV_OFF)
    {
        CliPrintf (CliHandle, "no-advertise ");
    }
    else
    {
        /*Addr Profile Valid Time */
        nmhGetFsipv6AddrProfileValidTime (i4ProfIndex, &u4Time);
        /*Addr Profile Valid Time Flag */
        nmhGetFsipv6AddrProfileValidLifeTimeFlag (i4ProfIndex, &i4TimeFlag);
        if (i4TimeFlag == IP6_VARIABLE_TIME)

            CliPrintf (CliHandle, "at %d ", u4Time);

        else if (u4Time == IP6_ADDR_PROF_MAX_VALID_LIFE)

            CliPrintf (CliHandle, "infinite ");

        else if (u4Time != IP6_ADDR_PROF_DEF_VALID_LIFE)

            CliPrintf (CliHandle, " %d ", u4Time);

        /*Addr Profile Prefered Time */
        nmhGetFsipv6AddrProfilePreferredTime (i4ProfIndex, &u4Time);
        /*Addr Profile Prefered Time Flag */
        nmhGetFsipv6AddrProfilePreferredLifeTimeFlag (i4ProfIndex, &i4TimeFlag);

        if (i4TimeFlag == IP6_VARIABLE_TIME)

            CliPrintf (CliHandle, "at %d ", u4Time);
        else if (u4Time == IP6_ADDR_PROF_MAX_PREF_LIFE)

            CliPrintf (CliHandle, "infinite ");

        else if (u4Time != IP6_ADDR_PROF_DEF_PREF_LIFE)

            CliPrintf (CliHandle, "%d ", u4Time);
    }

    /*Addr Profile On Link Adv Status */
    nmhGetFsipv6AddrProfileOnLinkAdvStatus (i4ProfIndex, &i4TableObject);
    if (i4TableObject == IP6_ADDR_PROF_ONLINK_ADV_OFF)
    {
        CliPrintf (CliHandle, "off-link ");
    }
    else
    {
        /*Addr Profile Auto conf Adv Status */
        /*There is a mismatch between the default values in the 
         * mib and the code*/
        nmhGetFsipv6AddrProfileAutoConfAdvStatus (i4ProfIndex, &i4TableObject);
        if (i4TableObject == IP6_ADDR_PROF_AUTO_ADV_OFF)
        {
            CliPrintf (CliHandle, "no-autoconfig ");
        }
        nmhGetFsipv6SupportEmbeddedRp (i4PrefixIndex, pPrefixAddrs,
                                       i4PrefixAddrLen, &i4TableObject);
        if (i4TableObject == IP6_SUPPORT_EMBD_RP_ENABLE)
        {
            CliPrintf (CliHandle, "embedded-rp ");
        }
    }
    CliPrintf (CliHandle, "\r\n");

    return;
}

/*****************************************************************************/
/*     FUNCTION NAME    : Fsipv6AddrTableInfo                                */
/*                                                                           */
/*     DESCRIPTION      : This function displays fsipv6AddrTable    objects  */
/*                        for show running configuration.                    */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : VOID                                               */
/*****************************************************************************/
VOID
Fsipv6AddrTableInfo (tCliHandle CliHandle, INT4 i4IfIndex,
                     tSNMP_OCTET_STRING_TYPE * pPrefixAddrs,
                     INT4 i4PrefixAddrLen)
{
    INT4                i4TableObject = 0;
    INT4                i4Flag = FALSE;
    INT4                i4CgaStatus = IP6_ZERO;
#ifndef LNXIP6_WANTED
    UINT1               u1CfgMethod = 0;
#endif

    /*Ipv6 Addr admin status */
    nmhGetFsipv6AddrAdminStatus (i4IfIndex,
                                 pPrefixAddrs, i4PrefixAddrLen, &i4TableObject);
    /*Note : Here The Link Local address will be configured by default
     * for the interfaces. However the user is also allowed to modify 
     * the same.There must be some mechanism which will let us know 
     * if the user has modified perhaps a flag? .If not default link
     * local addresses will also be displayed*/

    /*Ipv6 prefix Profile Index */
    nmhGetFsipv6AddrType (i4IfIndex,
                          pPrefixAddrs, i4PrefixAddrLen, &i4TableObject);
    nmhGetFsipv6AddrSENDCgaStatus (i4IfIndex,
                                   pPrefixAddrs, i4PrefixAddrLen, &i4CgaStatus);
#ifndef LNXIP6_WANTED
    /*Prefix Address */
    /*Get Configuration method if static then show in running config */
    Ipv6UtilGetConfigMethod (i4IfIndex, pPrefixAddrs,
                             (UINT1) i4PrefixAddrLen,
                             (UINT1) i4TableObject, &u1CfgMethod);

#endif

    if ((i4TableObject != IP6_ADDR_TYPE_LINK_LOCAL) &&
        (i4TableObject != IP6_ADDR_TYPE_UNICAST))
    {
        i4Flag = TRUE;
    }
#ifndef LNXIP6_WANTED
    else if (u1CfgMethod == IP6_ADDR_STATIC)
    {
        i4Flag = TRUE;
    }
    else
    {
        i4Flag = FALSE;
    }
#else
    i4Flag = TRUE;                /* Always TRUE for LNXIP */
#endif

    if (i4Flag == TRUE)
    {
        CliPrintf (CliHandle, " ipv6 address %s ",
                   Ip6PrintNtop ((tIp6Addr *) (VOID *) (pPrefixAddrs->
                                                        pu1_OctetList)));
        if (i4TableObject == IP6_ADDR_TYPE_UNICAST)
        {
            CliPrintf (CliHandle, "%d unicast ", i4PrefixAddrLen);
        }
        else if (i4TableObject == IP6_ADDR_TYPE_ANYCAST)
        {
            CliPrintf (CliHandle, "%d anycast ", i4PrefixAddrLen);
        }
        else if (i4TableObject == IP6_ADDR_TYPE_LINK_LOCAL)
        {
            CliPrintf (CliHandle, "link-local ");
        }

        if (i4CgaStatus == IP6_ONE)
        {
            CliPrintf (CliHandle, "cga ");
        }
        CliPrintf (CliHandle, "\r\n");
    }

    return;
}

/*****************************************************************************/
/*     FUNCTION NAME    : Fsipv6PolicyPrefixTableInfo                        */
/*                                                                           */
/*     DESCRIPTION      : This function displays fsipv6AddrTable    objects  */
/*                        for show running configuration.                    */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : VOID                                               */
/*****************************************************************************/

VOID
Fsipv6PolicyPrefixTableInfo (tCliHandle CliHandle,
                             tSNMP_OCTET_STRING_TYPE * pPrefixAddrs,
                             INT4 i4PrefixAddrLen, INT4 i4IfIndex)
{

    INT4                i4Precedence = 0;
    INT4                i4Label = 0;
    INT4                i4AddrType = 0;

    nmhGetFsipv6AddrSelPolicyPrecedence (pPrefixAddrs, i4PrefixAddrLen,
                                         i4IfIndex, &i4Precedence);

    nmhGetFsipv6AddrSelPolicyLabel (pPrefixAddrs, i4PrefixAddrLen,
                                    i4IfIndex, &i4Label);

    nmhGetFsipv6AddrSelPolicyAddrType (pPrefixAddrs, i4PrefixAddrLen,
                                       i4IfIndex, &i4AddrType);

    CliPrintf (CliHandle, " ipv6 policy-prefix %s ",
               Ip6PrintNtop ((tIp6Addr *) (VOID *) (pPrefixAddrs->
                                                    pu1_OctetList)));
    CliPrintf (CliHandle, "%d", i4PrefixAddrLen);
    if (i4Precedence != IPV6_ADDR_SEL_DEFAULT_POLICY_PRECEDENCE)
    {
        CliPrintf (CliHandle, "  precedence %d", i4Precedence);
    }
    if (i4Label != IPV6_ADDR_SEL_DEFAULT_POLICY_LABEL)
    {
        CliPrintf (CliHandle, "  label %d", i4Label);
    }
    if (i4AddrType == ADDR6_ANYCAST)
    {
        CliPrintf (CliHandle, " anycast");
    }
    else if (i4AddrType == ADDR6_UNICAST)
    {
        CliPrintf (CliHandle, " unicast");
    }
    CliPrintf (CliHandle, "\r\n");

}

/*****************************************************************************/
/*     FUNCTION NAME    : Fsipv6InterfaceScopeZoneTableInfo                  */
/*                                                                           */
/*     DESCRIPTION      : This function displays fsipv6IfScopeZOneMapTable
 *                        objects for show running configuration.            */
/*                                                     */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : VOID                                               */
/****************************************************************************/
VOID
Fsipv6InterfaceScopeZoneTableInfo (tCliHandle CliHandle, INT4 i4IfIndex)
{
#ifdef LNXIP6_WANTED
    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (i4IfIndex);
#else
    INT4                i4RowStatus = 0;
    UINT1               u1Scope = 0;
    tIp6IfZoneMapInfo  *pIp6IfZoneMapInfo = NULL;

    nmhGetFsipv6IfScopeZoneRowStatus (i4IfIndex, &i4RowStatus);
    if (i4RowStatus != IP6_ZONE_ROW_STATUS_ACTIVE)
    {
        return;
    }

    for (u1Scope = ADDR6_SCOPE_INTLOCAL; u1Scope <= ADDR6_SCOPE_GLOBAL;
         u1Scope++)
    {
        pIp6IfZoneMapInfo = Ip6ZoneGetIfScopeZoneEntry (i4IfIndex, u1Scope);
        /*show running-config: Zone Name "global","interfacelocal",linklocal"
           are initially configured. */
        if ((pIp6IfZoneMapInfo != NULL &&
             (STRNCMP (pIp6IfZoneMapInfo->au1ZoneName, "global",
                       STRLEN ("global")) != 0)) &&
            (STRNCMP (pIp6IfZoneMapInfo->au1ZoneName, "interfacelocal",
                      STRLEN ("interfacelocal")) != 0) &&
            (STRNCMP (pIp6IfZoneMapInfo->au1ZoneName, "linklocal",
                      STRLEN ("linklocal")) != 0))
        {
            if ((STRNCMP (pIp6IfZoneMapInfo->au1ZoneName, "sitelocal",
                          STRLEN ("sitelocal")) == 0))
            {
                CliPrintf (CliHandle, " ipv6 scope-zone sitelocal %d\r\n",
                           pIp6IfZoneMapInfo->i4ZoneId);
            }
            else
            {

                CliPrintf (CliHandle, " ipv6 scope-zone %s\r\n",
                           pIp6IfZoneMapInfo->au1ZoneName);
            }
        }

    }
    CliPrintf (CliHandle, "\r\n");
#endif
    return;

}

/*****************************************************************************/
/*     FUNCTION NAME    : Ipv6ShowRunningConfigInterfaceDetails              */
/*                                                                           */
/*     DESCRIPTION      : This function displays current configuration       */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        i4IfIndex  - Specified interface for               */
/*                        configuration                                      */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/

INT4
Ipv6ShowRunningConfigInterfaceDetails (tCliHandle CliHandle, INT4 i4IfIndex,
                                       UINT1 *pu1Flag)
{
    INT1                i1OutCome = 0;
    INT4                i4Index = 0;
    INT4                i4PrevIndex = 0;
    INT4                i4PrefLen = 0;
    INT4                i4PrevPrefLen = 0;
    INT4                i4PrefixLen = 0;
    INT4                i4PrevPrefixLen = 0;
    INT4                i4DestUnreachStatus = 0;
    INT4                i4Icmp6ErrInterval = 0;
    INT4                i4Icmp6BucketSize = 0;
    INT4                i4UnnumAssocIfIndex = 0;
    INT4                i4Icmp6RedirectStatus = 0;
    UINT1               au1Temp[IP6_ADDR_SIZE];
    UINT1               au1Temp1[IP6_ADDR_SIZE];
    UINT1               au1RATemp[IP6_ADDR_SIZE];
    UINT1               au1RATemp1[IP6_ADDR_SIZE];
    UINT1               au1IfTok[IP6_EUI_ADDRESS_LEN];
    UINT1               au1Macaddr[IP6_ENET_ADDR_LEN];
    UINT1               au1Ip6IfId[IP6_EUI_ADDRESS_LEN + 1];
    UINT1               u1BangStatus = FALSE;
    UINT1               u1TempBangStatus = FALSE;
    INT1               *pi1UnnumAssocIfName;
    UINT1               au1UnnumAssocIfName[CFA_CLI_MAX_IF_NAME_LEN];
    UINT1               au1NameStr[CFA_CLI_MAX_IF_NAME_LEN];
    tIp6LlocalInfo      tAddrInfo;
    tIp6LlocalInfo     *pAddrInfo = &tAddrInfo;
    tIp6Addr            Ip6IfId;
    tSNMP_OCTET_STRING_TYPE tAddress;
    tSNMP_OCTET_STRING_TYPE tPrevAddress;
    tSNMP_OCTET_STRING_TYPE tIfaceToken;
    tSNMP_OCTET_STRING_TYPE tPolicyPrefix;
    tSNMP_OCTET_STRING_TYPE tPrevPolicyPrefix;
    tSNMP_OCTET_STRING_TYPE tRARoutePref;
    tSNMP_OCTET_STRING_TYPE tPrevRARoutePref;
    INT4                i4PrevRAPrefLen = 0;
    INT4                i4RAPrefLen = 0;
    tCfaIfInfo          IfInfo;

    MEMSET (au1Macaddr, 0, IP6_ENET_ADDR_LEN);
    MEMSET (au1Ip6IfId, 0, (IP6_EUI_ADDRESS_LEN));
    MEMSET (&tAddrInfo, 0, sizeof (tAddrInfo));

    tAddress.pu1_OctetList = au1Temp;
    MEMSET (au1Temp, 0, IP6_ADDR_SIZE);

    tPrevAddress.pu1_OctetList = au1Temp1;
    MEMSET (au1Temp1, 0, IP6_ADDR_SIZE);

    tPolicyPrefix.pu1_OctetList = au1Temp;
    MEMSET (au1Temp, 0, IP6_ADDR_SIZE);

    tPrevPolicyPrefix.pu1_OctetList = au1Temp1;
    MEMSET (au1Temp1, 0, IP6_ADDR_SIZE);

    MEMSET (&tIfaceToken, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    tIfaceToken.pu1_OctetList = au1IfTok;
    MEMSET (au1IfTok, 0, IP6_EUI_ADDRESS_LEN);
    MEMSET (&Ip6IfId, 0, sizeof (tIp6Addr));
    MEMSET (au1NameStr, 0, CFA_CLI_MAX_IF_NAME_LEN);

    tRARoutePref.pu1_OctetList = au1RATemp;
    tRARoutePref.i4_Length = IP6_ADDR_SIZE;
    MEMSET (au1RATemp, 0, IP6_ADDR_SIZE);

    tPrevRARoutePref.pu1_OctetList = au1RATemp1;
    tPrevRARoutePref.i4_Length = IP6_ADDR_SIZE;
    MEMSET (au1RATemp1, 0, IP6_ADDR_SIZE);

    pi1UnnumAssocIfName = (INT1 *) &au1UnnumAssocIfName[0];

    CliRegisterLock (CliHandle, Ip6Lock, Ip6UnLock);
    IP6_TASK_LOCK ();
    /* fsipv6IfTable */
    CfaGetIfInfo ((UINT2) i4IfIndex, &IfInfo);
    MEMCPY (au1Macaddr, IfInfo.au1MacAddr, CFA_ENET_ADDR_LEN);
    GET_EUI_64_IF_ID (au1Macaddr, au1Ip6IfId);
    MEMCPY ((pAddrInfo->ip6Addr.u1_addr + 8), au1Ip6IfId, IP6_EUI_ADDRESS_LEN);
    if (nmhValidateIndexInstanceFsipv6IfTable (i4IfIndex) == SNMP_SUCCESS)
    {
        if (*pu1Flag == OSIX_FALSE)
        {
            CfaCliConfGetIfName ((UINT4) i4IfIndex, (INT1 *) au1NameStr);
            CliPrintf (CliHandle, "interface %s \r\n", au1NameStr);
            *pu1Flag = OSIX_TRUE;
        }
        Fsipv6IfTableInfo (CliHandle, i4IfIndex);
        CliGetBangStatus (CliHandle, &u1BangStatus);
        u1TempBangStatus = u1BangStatus;
        i1OutCome = nmhGetIpv6IfIdentifier (i4IfIndex, &tIfaceToken);
        if ((i1OutCome != SNMP_FAILURE) && (tIfaceToken.i4_Length > 1))
        {
            MEMCPY (((UINT1 *) (&Ip6IfId) + IP6_EUI_ADDRESS_LEN),
                    tIfaceToken.pu1_OctetList, IP6_EUI_ADDRESS_LEN);
            if (STRCMP (Ip6PrintNtop (&pAddrInfo->ip6Addr),
                        Ip6PrintNtop (&Ip6IfId)) != 0)
            {
                u1BangStatus = TRUE;
                CliPrintf (CliHandle, " ipv6 interface-identifier %s\n",
                           Ip6PrintNtop (&Ip6IfId));
            }
        }
        nmhGetFsipv6IfDestUnreachableMsg (i4IfIndex, &i4DestUnreachStatus);
        nmhGetFsipv6IfIcmpErrInterval (i4IfIndex, &i4Icmp6ErrInterval);
        nmhGetFsipv6IfIcmpTokenBucketSize (i4IfIndex, &i4Icmp6BucketSize);
        nmhGetFsipv6IfRedirectMsg (i4IfIndex, &i4Icmp6RedirectStatus);
        if ((i4Icmp6ErrInterval != ICMP6_DEF_ERR_INETRVAL) ||
            (i4Icmp6BucketSize != ICMP6_DEF_BUCKET_SIZE))
        {
            if (i4Icmp6ErrInterval == ICMP6_RATE_LIMIT_DISABLE)
            {
                u1BangStatus = TRUE;
                CliPrintf (CliHandle, " no ipv6 icmp error-interval \r\n");
            }
            else
            {
                u1BangStatus = TRUE;
                CliPrintf (CliHandle, " ipv6 icmp error-interval %d %d \r\n",
                           i4Icmp6ErrInterval, i4Icmp6BucketSize);
            }
        }
        if (i4DestUnreachStatus != ICMP6_DEST_UNREACHABLE_ENABLE)
        {
            u1BangStatus = TRUE;
            CliPrintf (CliHandle, " ipv6 icmp dest-unreachable disable \r\n");
        }
        if (i4Icmp6RedirectStatus != ICMP6_REDIRECT_DISABLE)
        {
            u1BangStatus = TRUE;
            CliPrintf (CliHandle, " ipv6 icmp redirect enable \r\n");
        }
        Fsipv6InterfaceScopeZoneTableInfo (CliHandle, i4IfIndex);
    }
    else
    {
        IP6_TASK_UNLOCK ();
        CliUnRegisterLock (CliHandle);
        return CLI_SUCCESS;
    }

    /*fsipv6AddrTable */
    i1OutCome =
        nmhGetFirstIndexFsipv6AddrTable (&i4Index, &tAddress, &i4PrefLen);
    while (i1OutCome != SNMP_FAILURE)
    {
        if (i4Index == i4IfIndex)
        {
            Fsipv6AddrTableInfo (CliHandle, i4Index, &tAddress, i4PrefLen);
        }

        i4PrevIndex = i4Index;
        tPrevAddress = tAddress;
        i4PrevPrefLen = i4PrefLen;
        i1OutCome = nmhGetNextIndexFsipv6AddrTable (i4PrevIndex, &i4Index,
                                                    &tPrevAddress, &tAddress,
                                                    i4PrevPrefLen, &i4PrefLen);
    }

    MEMSET (au1Temp, 0, IP6_ADDR_SIZE);
    MEMSET (au1Temp1, 0, IP6_ADDR_SIZE);

    if (CfaIsL3SubIfIndex ((UINT4) i4IfIndex) != CFA_TRUE)
    {
        /*fsipv6PrefixTable and fsipv6AddrProfileTable */
        i1OutCome = nmhGetFirstIndexFsipv6PrefixTable (&i4Index, &tAddress,
                                                       &i4PrefLen);
        while (i1OutCome != SNMP_FAILURE)
        {
            if (i4Index > i4IfIndex)
            {
                break;
            }
            else if (i4Index == i4IfIndex)
            {
                Fsipv6PrefixProfileTableInfo (CliHandle, i4Index, &tAddress,
                                              i4PrefLen);
            }
            i4PrevIndex = i4Index;
            tPrevAddress = tAddress;
            i4PrevPrefLen = i4PrefLen;

            i1OutCome = nmhGetNextIndexFsipv6PrefixTable (i4PrevIndex,
                                                          &i4Index,
                                                          &tPrevAddress,
                                                          &tAddress,
                                                          i4PrevPrefLen,
                                                          &i4PrefLen);
        }
    }

    i1OutCome = nmhGetFirstIndexFsipv6AddrSelPolicyTable (&tPolicyPrefix,
                                                          &i4PrefixLen,
                                                          &i4Index);
    while (i1OutCome != SNMP_FAILURE)
    {
        if (i4Index > i4IfIndex)
        {
            break;
        }
        else if (i4Index == i4IfIndex)
        {
            Fsipv6PolicyPrefixTableInfo (CliHandle, &tPolicyPrefix,
                                         i4PrefixLen, i4Index);
        }
        i4PrevIndex = i4Index;
        tPrevPolicyPrefix = tPolicyPrefix;
        i4PrevPrefixLen = i4PrefixLen;

        i1OutCome = nmhGetNextIndexFsipv6AddrSelPolicyTable (&tPrevPolicyPrefix,
                                                             &tPolicyPrefix,
                                                             i4PrevPrefixLen,
                                                             &i4PrefixLen,
                                                             i4PrevIndex,
                                                             &i4Index);
    }

    /* RARouteInfo Table - RFC 4191 Implementation */
    i1OutCome =
        nmhGetFirstIndexFsipv6RARouteInfoTable (&i4Index, &tRARoutePref,
                                                &i4RAPrefLen);
    while (i1OutCome != SNMP_FAILURE)
    {
        if (i4Index == i4IfIndex)
        {
            FsipvRARouteTableInfo (CliHandle, i4Index, &tRARoutePref,
                                   i4RAPrefLen);
        }

        i4PrevIndex = i4Index;
        tPrevRARoutePref = tRARoutePref;
        i4PrevRAPrefLen = i4RAPrefLen;
        i1OutCome =
            nmhGetNextIndexFsipv6RARouteInfoTable (i4PrevIndex, &i4Index,
                                                   &tPrevRARoutePref,
                                                   &tRARoutePref,
                                                   i4PrevRAPrefLen,
                                                   &i4RAPrefLen);
    }

    nmhGetFsipv6IfUnnumAssocIPIf (i4IfIndex, &i4UnnumAssocIfIndex);

    if (i4UnnumAssocIfIndex != 0)
    {
        u1BangStatus = TRUE;
        CfaCliConfGetIfName ((UINT4) i4UnnumAssocIfIndex, pi1UnnumAssocIfName);
        CliPrintf (CliHandle, " ipv6 unnumbered %s\r\n", pi1UnnumAssocIfName);
    }

    IP6_TASK_UNLOCK ();
    CliUnRegisterLock (CliHandle);
    if ((u1BangStatus == TRUE) || (u1TempBangStatus == TRUE)
        || (*pu1Flag == OSIX_TRUE))
    {
        CliPrintf (CliHandle, "!\r\n");
        CliSetBangStatus (CliHandle, FALSE);
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : Ipv6ShowRunningConfigInterface                     */
/*                                                                           */
/*     DESCRIPTION      : This function displays current configuration       */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/

INT4
Ipv6ShowRunningConfigInterfaceInCxt (tCliHandle CliHandle, UINT4 u4ContextId)
{
    INT1                i1OutCome = 0;
    INT4                i4IfIndex = 0;
    INT4                i4PrevIfIndex = 0;
    UINT1               u1Flag = OSIX_FALSE;
    UINT1               au1NameStr[CFA_CLI_MAX_IF_NAME_LEN];
    UINT4               u4PagingStatus = CLI_SUCCESS;
    UINT4               u4CurrContext = 0;
    INT4                i4AdminStatus = 0;

    CliRegisterLock (CliHandle, Ip6Lock, Ip6UnLock);
    IP6_TASK_LOCK ();
    /*fsipv6IfTable */
    i1OutCome = nmhGetFirstIndexFsipv6IfTable (&i4IfIndex);
    while (i1OutCome != SNMP_FAILURE)
    {
        nmhGetFsMIStdIpv6IfContextId (i4IfIndex, (INT4 *) &u4CurrContext);
        if (u4CurrContext != u4ContextId)
        {
            i4PrevIfIndex = i4IfIndex;
            i1OutCome = nmhGetNextIndexFsipv6IfTable (i4PrevIfIndex,
                                                      &i4IfIndex);
            continue;
        }
        CfaCliConfGetIfName ((UINT4) i4IfIndex, (INT1 *) au1NameStr);
        i1OutCome = nmhGetFsipv6IfAdminStatus (i4IfIndex, &i4AdminStatus);
        if (i1OutCome != SNMP_FAILURE)
        {
            if (i4AdminStatus == NETIPV6_ADMIN_UP)
            {
                u4PagingStatus =
                    CliPrintf (CliHandle, "interface %s \r\n", au1NameStr);
                u1Flag = OSIX_TRUE;
                if (u4PagingStatus == CLI_FAILURE)
                    break;
            }
        }

        IP6_TASK_UNLOCK ();
        CliUnRegisterLock (CliHandle);

        Ipv6ShowRunningConfigInterfaceDetails (CliHandle, i4IfIndex, &u1Flag);

        CliRegisterLock (CliHandle, Ip6Lock, Ip6UnLock);
        IP6_TASK_LOCK ();

        u4PagingStatus = CliPrintf (CliHandle, "\r\n");
        i4PrevIfIndex = i4IfIndex;
        i1OutCome = nmhGetNextIndexFsipv6IfTable (i4PrevIfIndex, &i4IfIndex);
    }
    IP6_TASK_UNLOCK ();
    CliUnRegisterLock (CliHandle);
    return (CLI_SUCCESS);
}

/*********************************************************************
*  Function Name : Ip6DefaultHopLimit 
*  Description   : Setting Default HopLimit 
*  Input(s)      : CliHandle
*                  i4HopLimit 
*  Output(s)     :  
*  Return Values : CLI_SUCCESS/ CLI_FAILURE.
*********************************************************************/
INT1
Ip6DefaultHopLimit (tCliHandle CliHandle, INT4 i4HopLimit)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2Ipv6DefaultHopLimit (&u4ErrorCode, i4HopLimit) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IP6_INVALID_HOPLIMIT);
        return CLI_FAILURE;
    }
    if (nmhSetIpv6DefaultHopLimit (i4HopLimit) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*********************************************************************
*  Function Name : Ip6SetPmtuDiscovery
*  Description   : Enable/Disable the Path MTU staus
*  Input(s)      : CliHandle
*                  i4Pmtu
*  Output(s)     :
*  Return Values : CLI_SUCCESS/ CLI_FAILURE.
*********************************************************************/
INT4
Ip6SetPmtuDiscovery (tCliHandle CliHandle, INT4 i4PmtuStatus, UINT4 u4VcId)
{
    UINT4               u4ErrorCode = 0;

    UNUSED_PARAM (CliHandle);
    if (nmhTestv2FsMIIpv6PmtuConfigStatus (&u4ErrorCode,
                                           (INT4) u4VcId,
                                           i4PmtuStatus) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsMIIpv6PmtuConfigStatus ((INT4) u4VcId,
                                        i4PmtuStatus) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IP6_PMTUD_FAIL);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*********************************************************************
*  Function Name : Ip6SetPmtuValue
*  Description   : Enable/Disable the Path MTU staus
*  Input(s)      : CliHandle
*                  i4Pmtu
*  Output(s)     :
*  Return Values : CLI_SUCCESS/ CLI_FAILURE.
*********************************************************************/
INT4
Ip6SetPmtuValue (tCliHandle CliHandle, tIp6Addr Ip6Addr,
                 INT4 i4PmtuValue, UINT4 u4VcId, INT4 i4AdminStatus)
{
    tSNMP_OCTET_STRING_TYPE Addr;
    UINT1               Ip6Address[IP6_ADDR_SIZE];
    UINT4               u4ErrorCode = 0;

    MEMSET (&Addr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&Ip6Address, 0, IP6_ADDR_SIZE);
    Addr.pu1_OctetList = &Ip6Address[0];
    MEMCPY (Addr.pu1_OctetList, &Ip6Addr, IP6_ADDR_SIZE);
    Addr.i4_Length = IP6_ADDR_SIZE;

    UNUSED_PARAM (CliHandle);
    if (nmhTestv2FsMIIpv6PmtuAdminStatus (&u4ErrorCode, (INT4) u4VcId,
                                          &Addr, i4AdminStatus) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsMIIpv6PmtuAdminStatus ((INT4) u4VcId, &Addr,
                                       i4AdminStatus) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IP6_INVALID_PMTU);
        return CLI_FAILURE;
    }

    if (i4AdminStatus == IP6_PMTU_ENABLE)
    {
        if (nmhTestv2FsMIIpv6Pmtu (&u4ErrorCode, (INT4) u4VcId, &Addr,
                                   i4PmtuValue) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsMIIpv6Pmtu ((INT4) u4VcId, &Addr,
                                i4PmtuValue) == SNMP_FAILURE)
        {
            CLI_SET_ERR (CLI_IP6_INVALID_PMTU);
            return CLI_FAILURE;
        }
    }
    return CLI_SUCCESS;
}

/*********************************************************************
*  Function Name : Ip6SetRFC5095Compatibilty 
*  Description   : Enable/Disables the compatibility with RFC 5095
*  Input(s)      : CliHandle
*  Output(s)     :
*  Return Values : CLI_SUCCESS/ CLI_FAILURE.
*********************************************************************/
INT4
Ip6SetRFC5095Compatibilty (tCliHandle CliHandle, INT4 i4Value, UINT4 u4VcId)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsMIIpv6RFC5095Compatibility (&u4ErrorCode, (INT4) u4VcId,
                                               i4Value) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFsMIIpv6RFC5095Compatibility ((INT4) u4VcId, i4Value)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;

}

/*********************************************************************
 * *  Function Name : Ip6SetRFC5942Compatibilty
 * *  Description   : Enable/Disables the compatibility with RFC 5942
 * *  Input(s)      : CliHandle
 * *  Output(s)     :
 * *  Return Values : CLI_SUCCESS/ CLI_FAILURE.
 * *********************************************************************/
INT4
Ip6SetRFC5942Compatibilty (tCliHandle CliHandle, INT4 i4Value)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4RetVal = 0;
    if (nmhTestv2Fsipv6RFC5942Compatibility (&u4ErrorCode,
                                             i4Value) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    i4RetVal = nmhSetFsipv6RFC5942Compatibility (i4Value);
    if (i4RetVal == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;

}

/*********************************************************************
*  Function Name : Ip6SetPmtuValue
*  Description   : Enable/Disable the Path MTU staus
*  Input(s)      : CliHandle
*                  i4Pmtu
*  Output(s)     :
*  Return Values : CLI_SUCCESS/ CLI_FAILURE.
*********************************************************************/
INT4
Ip6ShowPmtu (tCliHandle CliHandle, UINT4 u4VcId)
{
    UINT1               au1ContextName[VCMALIAS_MAX_ARRAY_LEN];
    UINT1               Ip6Address[IP6_ADDR_SIZE];
    UINT1               Ip6NextAddress[IP6_ADDR_SIZE];
    UINT4               u4ShowAllCxt = FALSE;
    INT4                i4CurrentVcId = 0;
    INT4                i4RetVal = SNMP_FAILURE;
    INT4                i4PathMtu = 0;
    INT4                i4VcId = VCM_INVALID_VC;
    INT4                i4Status = IP6_PMTU_DISABLE;
    tSNMP_OCTET_STRING_TYPE Ipv6PmtuDest;
    tSNMP_OCTET_STRING_TYPE Ipv6PmtuNextDest;

    MEMSET (&Ipv6PmtuDest, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&Ipv6PmtuNextDest, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&Ip6Address, 0, IP6_ADDR_SIZE);
    Ipv6PmtuDest.pu1_OctetList = &Ip6Address[0];
    MEMSET (&Ip6NextAddress, 0, IPVX_IPV6_ADDR_LEN);
    Ipv6PmtuNextDest.pu1_OctetList = &Ip6NextAddress[0];

    if (u4VcId == VCM_INVALID_VC)
    {
        i4RetVal = nmhGetFirstIndexFsMIIpv6ContextTable (&i4VcId);
        u4ShowAllCxt = TRUE;
    }
    else
    {
        i4VcId = (INT4) u4VcId;
        i4RetVal = SNMP_SUCCESS;
    }
    while (i4RetVal != SNMP_FAILURE)
    {
        i4Status = IP6_PMTU_DISABLE;
        VcmGetAliasName (i4VcId, au1ContextName);
        nmhGetFsMIIpv6PmtuConfigStatus (i4VcId, &i4Status);
        if (i4Status == IP6_PMTU_ENABLE)
        {
            CliPrintf (CliHandle, "PMTU discovery is enabled in %s\r\n",
                       au1ContextName);
        }

        i4CurrentVcId = i4VcId;
        i4RetVal = nmhGetNextIndexFsMIIpv6ContextTable (i4CurrentVcId, &i4VcId);
        if (u4ShowAllCxt == FALSE)
            break;
    }

    i4VcId = VCM_INVALID_VC;
    i4RetVal = SNMP_FAILURE;
    CliPrintf (CliHandle, "\r\n    Ipv6 Path MTU Table\r\n");

    CliPrintf (CliHandle, "    -----------------\r\n");

    CliPrintf (CliHandle, "\r\nVrf Name   Destination     PMTU \r\n");
    CliPrintf (CliHandle, "--------   -----------     ---- \r\n");
    i4VcId = (INT4) u4VcId;

    if (u4VcId == VCM_INVALID_VC)
    {
        u4ShowAllCxt = TRUE;
        i4RetVal =
            nmhGetFirstIndexFsMIIpv6PmtuTable (&i4VcId, &Ipv6PmtuNextDest);
    }
    else
    {
        i4CurrentVcId = i4VcId;
        i4RetVal = nmhGetNextIndexFsMIIpv6PmtuTable
            (i4CurrentVcId, &i4VcId, &Ipv6PmtuDest, &Ipv6PmtuNextDest);

        if (i4CurrentVcId != i4VcId)
        {
            return SNMP_FAILURE;
        }
    }

    while (i4RetVal == SNMP_SUCCESS)
    {
        VcmGetAliasName (i4VcId, au1ContextName);

        nmhGetFsMIIpv6Pmtu (i4VcId, &Ipv6PmtuNextDest, &i4PathMtu);
        CliPrintf (CliHandle, "\n");

        CliPrintf (CliHandle, "%-15s", au1ContextName);

        CliPrintf (CliHandle, "%-13s",
                   Ip6PrintNtop ((tIp6Addr *) (VOID *)
                                 Ipv6PmtuNextDest.pu1_OctetList));

        CliPrintf (CliHandle, "%-8d\n", i4PathMtu);

        i4CurrentVcId = i4VcId;

        MEMCPY (Ipv6PmtuDest.pu1_OctetList, Ipv6PmtuNextDest.pu1_OctetList,
                Ipv6PmtuNextDest.i4_Length);
        Ipv6PmtuDest.i4_Length = Ipv6PmtuNextDest.i4_Length;

        i4RetVal = nmhGetNextIndexFsMIIpv6PmtuTable
            (i4CurrentVcId, &i4VcId, &Ipv6PmtuDest, &Ipv6PmtuNextDest);

        if ((i4CurrentVcId != i4VcId) && (u4ShowAllCxt == FALSE))
        {
            /*Finished displaying entries belonging to one VRF */
            break;
        }
    }                            /*end of while */

    return CLI_SUCCESS;
}

/*********************************************************************
*  Function Name : Ip6ShowAddrSelPolicyTable 
*  Description   : Displays the policy table entries 
*  Input(s)      : CliHandle
*  Output(s)     : NONE
*  Return Values : CLI_SUCCESS/ CLI_FAILURE.
*********************************************************************/

INT4
Ip6ShowAddrSelPolicyTable (tCliHandle CliHandle)
{
    INT4                i1OutCome = 0;
    INT4                i4Precedence = 0;
    INT4                i4Label = 0;
    INT4                i4AddrType = 0;
    INT4                i4PrefixLen = 0;
    INT4                i4PrevPrefixLen = 0;
    INT4                i4IfIndex = 0;
    INT4                i4PrevIndex = 0;
    UINT1               au1Temp[IP6_ADDR_SIZE];
    UINT1               au1Temp1[IP6_ADDR_SIZE];
    tSNMP_OCTET_STRING_TYPE tPolicyPrefix;
    tSNMP_OCTET_STRING_TYPE tPrevPolicyPrefix;

    tPolicyPrefix.pu1_OctetList = au1Temp;
    MEMSET (au1Temp, 0, IP6_ADDR_SIZE);

    tPrevPolicyPrefix.pu1_OctetList = au1Temp1;
    MEMSET (au1Temp1, 0, IP6_ADDR_SIZE);

    CliPrintf (CliHandle,
               "\n------------------------------------------------------------------------------------\n");
    CliPrintf (CliHandle, "\r\n%-20s%-12s%-13s%-11s%-10s",
               "IP6 PREFIX", "PREFIXLEN", "PRECEDENCE", "LABEL", "ADDRTYPE");
    CliPrintf (CliHandle, "\r\n%-20s%-12s%-13s%-11s%-10s",
               "----------", "---------", "----------", "-----", "--------");
    CliPrintf (CliHandle,
               "\n------------------------------------------------------------------------------------\n");

    i1OutCome = nmhGetFirstIndexFsipv6AddrSelPolicyTable (&tPolicyPrefix,
                                                          &i4PrefixLen,
                                                          &i4IfIndex);
    while (i1OutCome != SNMP_FAILURE)
    {

        nmhGetFsipv6AddrSelPolicyPrecedence (&tPolicyPrefix, i4PrefixLen,
                                             i4IfIndex, &i4Precedence);

        nmhGetFsipv6AddrSelPolicyLabel (&tPolicyPrefix, i4PrefixLen,
                                        i4IfIndex, &i4Label);

        nmhGetFsipv6AddrSelPolicyAddrType (&tPolicyPrefix, i4PrefixLen,
                                           i4IfIndex, &i4AddrType);

        CliPrintf (CliHandle, "\r%-20s",
                   Ip6PrintNtop ((tIp6Addr *) (VOID *) (tPolicyPrefix.
                                                        pu1_OctetList)));
        CliPrintf (CliHandle, "%-12d", i4PrefixLen);

        CliPrintf (CliHandle, "%-13d", i4Precedence);

        CliPrintf (CliHandle, "%-11d", i4Label);

        if (i4AddrType == ADDR6_ANYCAST)
        {
            CliPrintf (CliHandle, "%-10s\n", "anycast");
        }
        else if (i4AddrType == ADDR6_UNICAST)
        {
            CliPrintf (CliHandle, "%-10s\n", "unicast");
        }
        else
        {
            CliPrintf (CliHandle, "%-10s\n", "unicast");
        }

        i4PrevIndex = i4IfIndex;
        tPrevPolicyPrefix = tPolicyPrefix;
        i4PrevPrefixLen = i4PrefixLen;

        i1OutCome = nmhGetNextIndexFsipv6AddrSelPolicyTable (&tPrevPolicyPrefix,
                                                             &tPolicyPrefix,
                                                             i4PrevPrefixLen,
                                                             &i4PrefixLen,
                                                             i4PrevIndex,
                                                             &i4IfIndex);
    }
    CliPrintf (CliHandle,
               "\n------------------------------------------------------------------------------------\n");
    return CLI_SUCCESS;
}

/*********************************************************************
 *  Function Name : Ip6SetScopeZone
 *  Description   : Configures a Scope-Zone on an Interface
 *  Input(s)      : CliHandle
 *                  i4IfaceIndex - Interface Index
 *                  u1Scope - Scope of the zone getting configured
 *                  au1ScopeZone - zone name
 *                  u1CreationStatus - Creation status (Manual or default)
 *                  u1ZoneRowStatus - Row status of the zone
 *  Output(s)     :
 *  Return Values : None.
 *  Comment       : Added as a part of RFC4007 Code changes
 * *********************************************************************/
VOID
Ip6SetScopeZone (tCliHandle CliHandle,
                 UINT4 u4IfIndex, UINT1 u1Scope, UINT1 *au1ScopeZone)
{
    UINT4               u4ErrorCode;
#ifndef LNXIP6_WANTED
    tIp6If             *pIf6 = NULL;
#else
    tLip6If            *pIf6 = NULL;
#endif
    tSNMP_OCTET_STRING_TYPE SetZoneName;
    tSNMP_OCTET_STRING_TYPE ZoneName;
    UINT1               au1ZoneName[IP6_SCOPE_ZONE_NAME_LEN];
    UINT1               au1TestZoneName[IP6_SCOPE_ZONE_NAME_LEN];
    UINT1               u1PrevZoneRowStatus = 0;

#ifndef LNXIP6_WANTED
    pIf6 = Ip6ifGetEntry (u4IfIndex);
#else
    pIf6 = Lip6UtlGetIfEntry (u4IfIndex);
#endif
    if (pIf6 == NULL)
    {
        CLI_FATAL_ERROR (CliHandle);
        return;
    }
    u1PrevZoneRowStatus = pIf6->u1ZoneRowStatus;
    MEMSET (au1ZoneName, IP6_ZERO, IP6_SCOPE_ZONE_NAME_LEN);
    MEMSET (au1TestZoneName, IP6_ZERO, IP6_SCOPE_ZONE_NAME_LEN);

    SetZoneName.pu1_OctetList = au1ZoneName;
    SetZoneName.i4_Length = IP6_SCOPE_ZONE_NAME_LEN;
    ZoneName.pu1_OctetList = au1TestZoneName;
    ZoneName.i4_Length = IP6_SCOPE_ZONE_NAME_LEN;
    /* Test the row status of the scope-zone on this interface */
    if (SNMP_FAILURE == nmhTestv2Fsipv6IfScopeZoneRowStatus (&u4ErrorCode,
                                                             (INT4) u4IfIndex,
                                                             IP6_ZONE_ROW_STATUS_NOT_IN_SERVICE))
    {
        CLI_SET_ERR (CLI_IP6_INVALID_ROW_STATUS_FOR_ZONE);
        return;
    }

    if (SNMP_FAILURE == nmhSetFsipv6IfScopeZoneRowStatus (u4IfIndex,
                                                          IP6_ZONE_ROW_STATUS_NOT_IN_SERVICE))
    {
        CLI_FATAL_ERROR (CliHandle);
        return;
    }

    switch (u1Scope)
    {
        case ADDR6_SCOPE_INTLOCAL:
            MEMCPY (ZoneName.pu1_OctetList, au1ScopeZone,
                    STRLEN (au1ScopeZone));
            ZoneName.i4_Length = STRLEN (au1ScopeZone);
            if (SNMP_FAILURE ==
                nmhTestv2Fsipv6ScopeZoneIndexInterfaceLocal (&u4ErrorCode,
                                                             (INT4)
                                                             u4IfIndex,
                                                             &ZoneName))
            {
                nmhSetFsipv6IfScopeZoneRowStatus (u4IfIndex,
                                                  u1PrevZoneRowStatus);
                return;
            }
            if (SNMP_FAILURE ==
                nmhSetFsipv6ScopeZoneIndexInterfaceLocal (u4IfIndex, &ZoneName))
            {
                nmhSetFsipv6IfScopeZoneRowStatus (u4IfIndex,
                                                  u1PrevZoneRowStatus);
                CLI_FATAL_ERROR (CliHandle);
                return;
            }
            break;

        case ADDR6_SCOPE_LLOCAL:
            MEMCPY (ZoneName.pu1_OctetList, au1ScopeZone,
                    STRLEN (au1ScopeZone));
            ZoneName.i4_Length = STRLEN (au1ScopeZone);
            if (SNMP_FAILURE ==
                nmhTestv2Fsipv6ScopeZoneIndexLinkLocal (&u4ErrorCode,
                                                        (INT4) u4IfIndex,
                                                        &ZoneName))
            {
                nmhSetFsipv6IfScopeZoneRowStatus (u4IfIndex,
                                                  u1PrevZoneRowStatus);
                return;
            }
            MEMCPY (SetZoneName.pu1_OctetList, au1ScopeZone,
                    STRLEN (au1ScopeZone));
            SetZoneName.i4_Length = STRLEN (au1ScopeZone);
            if (SNMP_FAILURE ==
                nmhSetFsipv6ScopeZoneIndexLinkLocal (u4IfIndex, &SetZoneName))
            {
                nmhSetFsipv6IfScopeZoneRowStatus (u4IfIndex,
                                                  IP6_ZONE_ROW_STATUS_DESTROY);
                CLI_FATAL_ERROR (CliHandle);
                return;
            }

            break;
        case ADDR6_SCOPE_SUBNETLOCAL:
            MEMCPY (ZoneName.pu1_OctetList, au1ScopeZone,
                    STRLEN (au1ScopeZone));
            ZoneName.i4_Length = STRLEN (au1ScopeZone);
            if (SNMP_FAILURE ==
                nmhTestv2Fsipv6ScopeZoneIndex3 (&u4ErrorCode,
                                                (INT4) u4IfIndex, &ZoneName))
            {
                nmhSetFsipv6IfScopeZoneRowStatus (u4IfIndex,
                                                  u1PrevZoneRowStatus);
                return;
            }
            if (SNMP_FAILURE == nmhSetFsipv6ScopeZoneIndex3 (u4IfIndex,
                                                             &ZoneName))
            {
                nmhSetFsipv6IfScopeZoneRowStatus (u4IfIndex,
                                                  u1PrevZoneRowStatus);
                CLI_FATAL_ERROR (CliHandle);
                return;
            }
            break;
        case ADDR6_SCOPE_ADMINLOCAL:
            MEMCPY (ZoneName.pu1_OctetList, au1ScopeZone,
                    STRLEN (au1ScopeZone));
            ZoneName.i4_Length = STRLEN (au1ScopeZone);
            if (SNMP_FAILURE ==
                nmhTestv2Fsipv6ScopeZoneIndexAdminLocal (&u4ErrorCode,
                                                         (INT4) u4IfIndex,
                                                         &ZoneName))
            {
                nmhSetFsipv6IfScopeZoneRowStatus (u4IfIndex,
                                                  u1PrevZoneRowStatus);
                return;
            }
            if (SNMP_FAILURE ==
                nmhSetFsipv6ScopeZoneIndexAdminLocal (u4IfIndex, &ZoneName))
            {
                nmhSetFsipv6IfScopeZoneRowStatus (u4IfIndex,
                                                  u1PrevZoneRowStatus);
                CLI_FATAL_ERROR (CliHandle);
                return;
            }
            break;
        case ADDR6_SCOPE_SITELOCAL:
            MEMCPY (ZoneName.pu1_OctetList, au1ScopeZone,
                    STRLEN (au1ScopeZone));
            ZoneName.i4_Length = STRLEN (au1ScopeZone);
            if (SNMP_FAILURE ==
                nmhTestv2Fsipv6ScopeZoneIndexSiteLocal (&u4ErrorCode,
                                                        (INT4) u4IfIndex,
                                                        &ZoneName))
            {
                nmhSetFsipv6IfScopeZoneRowStatus (u4IfIndex,
                                                  u1PrevZoneRowStatus);
                return;
            }
            if (SNMP_FAILURE ==
                nmhSetFsipv6ScopeZoneIndexSiteLocal (u4IfIndex, &ZoneName))
            {
                nmhSetFsipv6IfScopeZoneRowStatus (u4IfIndex,
                                                  u1PrevZoneRowStatus);
                CLI_FATAL_ERROR (CliHandle);
                return;
            }
            break;
        case ADDR6_SCOPE_UNASSIGN6:
            MEMCPY (ZoneName.pu1_OctetList, au1ScopeZone,
                    STRLEN (au1ScopeZone));
            ZoneName.i4_Length = STRLEN (au1ScopeZone);
            if (SNMP_FAILURE ==
                nmhTestv2Fsipv6ScopeZoneIndex6 (&u4ErrorCode,
                                                (INT4) u4IfIndex, &ZoneName))
            {
                nmhSetFsipv6IfScopeZoneRowStatus (u4IfIndex,
                                                  u1PrevZoneRowStatus);
                return;
            }
            if (SNMP_FAILURE == nmhSetFsipv6ScopeZoneIndex6 (u4IfIndex,
                                                             &ZoneName))
            {
                nmhSetFsipv6IfScopeZoneRowStatus (u4IfIndex,
                                                  u1PrevZoneRowStatus);
                CLI_FATAL_ERROR (CliHandle);
                return;
            }
            break;
        case ADDR6_SCOPE_UNASSIGN7:
            MEMCPY (ZoneName.pu1_OctetList, au1ScopeZone,
                    STRLEN (au1ScopeZone));
            ZoneName.i4_Length = STRLEN (au1ScopeZone);
            if (SNMP_FAILURE ==
                nmhTestv2Fsipv6ScopeZoneIndex7 (&u4ErrorCode,
                                                (INT4) u4IfIndex, &ZoneName))
            {
                nmhSetFsipv6IfScopeZoneRowStatus (u4IfIndex,
                                                  u1PrevZoneRowStatus);
                return;
            }
            if (SNMP_FAILURE == nmhSetFsipv6ScopeZoneIndex7 (u4IfIndex,
                                                             &ZoneName))
            {
                nmhSetFsipv6IfScopeZoneRowStatus (u4IfIndex,
                                                  u1PrevZoneRowStatus);
                CLI_FATAL_ERROR (CliHandle);
                return;
            }
            break;
        case ADDR6_SCOPE_ORGLOCAL:
            MEMCPY (ZoneName.pu1_OctetList, au1ScopeZone,
                    STRLEN (au1ScopeZone));
            ZoneName.i4_Length = STRLEN (au1ScopeZone);
            if (SNMP_FAILURE ==
                nmhTestv2Fsipv6ScopeZoneIndexOrganizationLocal
                (&u4ErrorCode, (INT4) u4IfIndex, &ZoneName))
            {
                nmhSetFsipv6IfScopeZoneRowStatus (u4IfIndex,
                                                  u1PrevZoneRowStatus);
                return;
            }
            if (SNMP_FAILURE ==
                nmhSetFsipv6ScopeZoneIndexOrganizationLocal (u4IfIndex,
                                                             &ZoneName))
            {
                nmhSetFsipv6IfScopeZoneRowStatus (u4IfIndex,
                                                  u1PrevZoneRowStatus);
                CLI_FATAL_ERROR (CliHandle);
                return;
            }
            break;
        case ADDR6_SCOPE_UNASSIGN9:
            MEMCPY (ZoneName.pu1_OctetList, au1ScopeZone,
                    STRLEN (au1ScopeZone));
            ZoneName.i4_Length = STRLEN (au1ScopeZone);
            if (SNMP_FAILURE ==
                nmhTestv2Fsipv6ScopeZoneIndex9 (&u4ErrorCode,
                                                (INT4) u4IfIndex, &ZoneName))
            {
                nmhSetFsipv6IfScopeZoneRowStatus (u4IfIndex,
                                                  u1PrevZoneRowStatus);
                return;
            }
            if (SNMP_FAILURE == nmhSetFsipv6ScopeZoneIndex9 (u4IfIndex,
                                                             &ZoneName))
            {
                nmhSetFsipv6IfScopeZoneRowStatus (u4IfIndex,
                                                  u1PrevZoneRowStatus);
                CLI_FATAL_ERROR (CliHandle);
                return;
            }
            break;
        case ADDR6_SCOPE_UNASSIGNA:
            MEMCPY (ZoneName.pu1_OctetList, au1ScopeZone,
                    STRLEN (au1ScopeZone));
            ZoneName.i4_Length = STRLEN (au1ScopeZone);
            if (SNMP_FAILURE ==
                nmhTestv2Fsipv6ScopeZoneIndexA (&u4ErrorCode,
                                                (INT4) u4IfIndex, &ZoneName))
            {
                nmhSetFsipv6IfScopeZoneRowStatus (u4IfIndex,
                                                  u1PrevZoneRowStatus);
                return;
            }
            if (SNMP_FAILURE == nmhSetFsipv6ScopeZoneIndexA (u4IfIndex,
                                                             &ZoneName))
            {
                nmhSetFsipv6IfScopeZoneRowStatus (u4IfIndex,
                                                  u1PrevZoneRowStatus);
                CLI_FATAL_ERROR (CliHandle);
                return;
            }
            break;
        case ADDR6_SCOPE_UNASSIGNB:
            MEMCPY (ZoneName.pu1_OctetList, au1ScopeZone,
                    STRLEN (au1ScopeZone));
            ZoneName.i4_Length = STRLEN (au1ScopeZone);
            if (SNMP_FAILURE ==
                nmhTestv2Fsipv6ScopeZoneIndexB (&u4ErrorCode,
                                                (INT4) u4IfIndex, &ZoneName))
            {
                nmhSetFsipv6IfScopeZoneRowStatus (u4IfIndex,
                                                  u1PrevZoneRowStatus);
                return;
            }
            if (SNMP_FAILURE == nmhSetFsipv6ScopeZoneIndexB (u4IfIndex,
                                                             &ZoneName))
            {
                nmhSetFsipv6IfScopeZoneRowStatus (u4IfIndex,
                                                  u1PrevZoneRowStatus);
                CLI_FATAL_ERROR (CliHandle);
                return;
            }
            break;
        case ADDR6_SCOPE_UNASSIGNC:
            MEMCPY (ZoneName.pu1_OctetList, au1ScopeZone,
                    STRLEN (au1ScopeZone));
            ZoneName.i4_Length = STRLEN (au1ScopeZone);
            if (SNMP_FAILURE ==
                nmhTestv2Fsipv6ScopeZoneIndexC (&u4ErrorCode,
                                                (INT4) u4IfIndex, &ZoneName))
            {
                nmhSetFsipv6IfScopeZoneRowStatus (u4IfIndex,
                                                  u1PrevZoneRowStatus);
                return;
            }
            if (SNMP_FAILURE == nmhSetFsipv6ScopeZoneIndexC (u4IfIndex,
                                                             &ZoneName))
            {
                nmhSetFsipv6IfScopeZoneRowStatus (u4IfIndex,
                                                  u1PrevZoneRowStatus);
                CLI_FATAL_ERROR (CliHandle);
                return;
            }
            break;
        case ADDR6_SCOPE_UNASSIGND:
            MEMCPY (ZoneName.pu1_OctetList, au1ScopeZone,
                    STRLEN (au1ScopeZone));
            ZoneName.i4_Length = STRLEN (au1ScopeZone);
            if (SNMP_FAILURE ==
                nmhTestv2Fsipv6ScopeZoneIndexD (&u4ErrorCode,
                                                (INT4) u4IfIndex, &ZoneName))
            {
                nmhSetFsipv6IfScopeZoneRowStatus (u4IfIndex,
                                                  u1PrevZoneRowStatus);
                return;
            }
            if (SNMP_FAILURE == nmhSetFsipv6ScopeZoneIndexD (u4IfIndex,
                                                             &ZoneName))
            {
                nmhSetFsipv6IfScopeZoneRowStatus (u4IfIndex,
                                                  u1PrevZoneRowStatus);
                CLI_FATAL_ERROR (CliHandle);
                return;
            }
            break;
        case ADDR6_SCOPE_GLOBAL:
            MEMCPY (ZoneName.pu1_OctetList, au1ScopeZone,
                    STRLEN (au1ScopeZone));
            ZoneName.i4_Length = STRLEN (au1ScopeZone);
            if (SNMP_FAILURE ==
                nmhTestv2Fsipv6ScopeZoneIndexE (&u4ErrorCode,
                                                (INT4) u4IfIndex, &ZoneName))
            {
                nmhSetFsipv6IfScopeZoneRowStatus (u4IfIndex,
                                                  u1PrevZoneRowStatus);
                return;
            }
            if (SNMP_FAILURE == nmhSetFsipv6ScopeZoneIndexE (u4IfIndex,
                                                             &ZoneName))
            {
                nmhSetFsipv6IfScopeZoneRowStatus (u4IfIndex,
                                                  u1PrevZoneRowStatus);
                CLI_FATAL_ERROR (CliHandle);
                return;
            }
            break;
    }
    if (SNMP_FAILURE == nmhTestv2Fsipv6IfScopeZoneRowStatus (&u4ErrorCode,
                                                             (INT4) u4IfIndex,
                                                             IP6_ZONE_ROW_STATUS_ACTIVE))
    {
        nmhSetFsipv6IfScopeZoneRowStatus (u4IfIndex,
                                          IP6_ZONE_ROW_STATUS_DESTROY);
        return;
    }
    /* Make the row status as up */
    if (SNMP_FAILURE == nmhSetFsipv6IfScopeZoneRowStatus (u4IfIndex,
                                                          IP6_ZONE_ROW_STATUS_ACTIVE))
    {
        nmhSetFsipv6IfScopeZoneRowStatus (u4IfIndex,
                                          IP6_ZONE_ROW_STATUS_DESTROY);
        CLI_FATAL_ERROR (CliHandle);
        return;
    }
    return;
}

/*********************************************************************
 *  Function Name : Ip6DeleteScopeZone
 *  Description   : deletes a Scope-Zone on an Interface
 *  Input(s)      : CliHandle
 *                  i4IfaceIndex - Interface Index
 *                  u1Scope - Scope of the zone getting configured
 *                  au1ScopeZone - zone name
 *                  u1ZoneRowStatus - Row status of the zone
 *  Output(s)     :
 *  Return Values : None.
 *  Comment       : Added as a part of RFC4007 Code changes
 *********************************************************************/
VOID
Ip6CliDeleteScopeZone (tCliHandle CliHandle, UINT4 u4Index, UINT1 u1Scope,
                       UINT1 *au1ScopeZone)
{

#ifdef LNXIP6_WANTED
    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (u4Index);
    UNUSED_PARAM (u1Scope);
    UNUSED_PARAM (au1ScopeZone);
#else
    tSNMP_OCTET_STRING_TYPE ZoneName;
    tIp6IfZoneMapInfo  *pIp6IfZoneMapinfo = NULL;
    UINT4               u4ErrorCode;
    UINT1               au1ZoneName[IP6_SCOPE_ZONE_NAME_LEN];
    UINT1               u1ZoneRowStatus = IP6_ZONE_ROW_STATUS_DESTROY;
    UINT1               u1ZoneLen = 0;

    u1ZoneLen = STRLEN (au1ScopeZone);
    MEMSET (au1ZoneName, IP6_ZERO, IP6_SCOPE_ZONE_NAME_LEN);
    ZoneName.pu1_OctetList = au1ZoneName;
    STRCPY (ZoneName.pu1_OctetList, au1ScopeZone);
    ZoneName.i4_Length = u1ZoneLen;

    UNUSED_PARAM (CliHandle);
    /*Check if the scope-zone exists on this interface */
    pIp6IfZoneMapinfo = Ip6ZoneGetIfScopeZoneEntry (u4Index, u1Scope);
    if (pIp6IfZoneMapinfo == NULL)
    {
        CLI_SET_ERR (CLI_IP6_ZONE_DOES_NOT_EXIST);
        return;
    }

    if (STRCMP (ZoneName.pu1_OctetList, pIp6IfZoneMapinfo->au1ZoneName) != 0)
    {
        CLI_SET_ERR (CLI_IP6_ZONE_DOES_NOT_EXIST);
        return;
    }

    if (((u1Scope == ADDR6_SCOPE_GLOBAL) ||
         (u1Scope == ADDR6_SCOPE_LLOCAL) ||
         (u1Scope == ADDR6_SCOPE_INTLOCAL)) &&
        (pIp6IfZoneMapinfo->u1ConfigStatus == IP6_ZONE_TYPE_AUTO))
    {
        CLI_SET_ERR (CLI_IP6_INVALID_AUTOCONF_ZONE_DEL);
        return;
    }

    /*Check the row status of the scope-zone on this interface */
    if (SNMP_FAILURE == nmhTestv2Fsipv6IfScopeZoneRowStatus
        (&u4ErrorCode, (INT4) u4Index, u1ZoneRowStatus))
    {
        return;
    }

    /*Deletes the scope zone */
    Ip6ScopeZoneDelete (u4Index, u1Scope, pIp6IfZoneMapinfo->i4ZoneId);
    return;
#endif
}

/*********************************************************************
 *  Function Name : Ip6CliConfigDefaultZone
 *  Description   : deletes a Scope-Zone on an Interface
 *  Input(s)      : CliHandle
 *                  i4IfaceIndex - Interface Index
 *                  u1Scope - Scope of the zone getting configured
 *                  au1ScopeZone - zone name
 *                  u1ZoneRowStatus - Row status of the zone
 *  Output(s)     :
 *  Return Values : None.
 *  Comment       : Added as a part of RFC4007 Code changes
 *********************************************************************/
VOID
Ip6CliConfigDefaultScopeZone (tCliHandle CliHandle, UINT1 *au1ScopeZone)
{
    tSNMP_OCTET_STRING_TYPE ZoneName;
    UINT4               u4ErrorCode;
    UINT1               au1ZoneName[IP6_SCOPE_ZONE_NAME_LEN];
    UINT1               u1ZoneLen = 0;
    UINT1               u1DefaultZone = IP6_ZONE_TRUE;

    u1ZoneLen = STRLEN (au1ScopeZone);
    MEMSET (au1ZoneName, IP6_ZERO, IP6_SCOPE_ZONE_NAME_LEN);
    ZoneName.pu1_OctetList = au1ZoneName;
    STRCPY (ZoneName.pu1_OctetList, au1ScopeZone);
    ZoneName.i4_Length = u1ZoneLen;

    if (SNMP_FAILURE ==
        nmhTestv2Fsipv6IsDefaultScopeZone (&u4ErrorCode, &ZoneName,
                                           (INT4) u1DefaultZone))
    {
        return;
    }

    if (SNMP_FAILURE == nmhSetFsipv6IsDefaultScopeZone (&ZoneName,
                                                        (INT4) u1DefaultZone))
    {
        CLI_FATAL_ERROR (CliHandle);
        return;
    }
    return;

}

 /*********************************************************************
 *  Function Name : Ip6CliConfigEcmpTimeInterval
 *  Description   : Updates User configured PRT Timer value
 *  Input(s)      : CliHandle
 *                  i4EcmpPRTInterval - PRT value
 *  Output(s)     :
 *  Return Values : None.
 *******************************************************************/

VOID
Ip6CliConfigEcmpTimeInterval (tCliHandle CliHandle, INT4 i4EcmpPRTInterval)
{
    UINT4               u4ErrorCode;
    if (SNMP_FAILURE ==
        nmhTestv2Fsipv6ECMPPRTTimer (&u4ErrorCode, i4EcmpPRTInterval))
    {
        CLI_FATAL_ERROR (CliHandle);
        return;
    }
    if (SNMP_FAILURE == nmhSetFsipv6ECMPPRTTimer (i4EcmpPRTInterval))
    {
        CLI_FATAL_ERROR (CliHandle);
        return;
    }
    return;

}

/*********************************************************************
 *  Function Name : Ip6CliGetDefaultScopeZoneForAllCxt
 *  Description   : deletes a Scope-Zone on an Interface
 *  Input(s)      : CliHandle
 *                  u4VcId - Context id
 *  Output(s)     :
 *  Return Values : None.
 *  Comment       : Added as a part of RFC4007 Code changes
 *******************************************************************/

VOID
Ip6CliGetDefaultScopeZoneForAllCxt (tCliHandle CliHandle, UINT4 u4VcId)
{

    INT4                i4RetVal = VCM_FAILURE;
    UINT4               u4L3ContextId = 0;
    UINT4               u4NextL3ContextId = 0;

    if (u4VcId == VCM_INVALID_VC)
    {
        i4RetVal = VcmGetFirstActiveL3Context (&u4L3ContextId);
    }
    else
    {
        u4L3ContextId = u4VcId;
        i4RetVal = VCM_SUCCESS;
    }
    while (i4RetVal == VCM_SUCCESS)
    {
        if (u4VcId == VCM_INVALID_VC)
        {
            i4RetVal = VcmGetNextActiveL3Context (u4L3ContextId,
                                                  &u4NextL3ContextId);
        }
        else
        {
            i4RetVal = VCM_FAILURE;
        }
        Ip6CliGetDefaultScopeZone (CliHandle, u4L3ContextId);
        u4L3ContextId = u4NextL3ContextId;
    }
    return;
}

/*********************************************************************
 *  Function Name : Ip6CliGetDefaultScopeZone
 *  Description   : deletes a Scope-Zone on an Interface
 *  Input(s)      : CliHandle
 *                  i4IfaceIndex - Interface Index
 *                  au1ScopeZone - zone name
 *                  u1ZoneRowStatus - Row status of the zone
 *  Output(s)     :
 *  Return Values : None.
 *  Comment       : Added as a part of RFC4007 Code changes
 *******************************************************************/
VOID
Ip6CliGetDefaultScopeZone (tCliHandle CliHandle, UINT4 u4ContextId)
{
#ifdef LNXIP6_WANTED
    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (u4ContextId);
#else
    tIp6ScopeZoneInfo  *pScopeZoneInfo = NULL;
    tIp6ScopeZoneInfo  *pNextScopeZoneInfo = NULL;
    INT4                i4ZoneId = 0;
    UINT1               u1Scope = 0;
    UINT1               au1ContextName[VCMALIAS_MAX_ARRAY_LEN];
    UINT1               u1DisplayHdr = FALSE;
    INT1                i1Length = 0;
    UINT1               au1ZoneNameDisplay[IP6_SCOPE_ZONE_NAME_LEN + 2];

    pScopeZoneInfo =
        (tIp6ScopeZoneInfo *) RBTreeGetFirst (gIp6GblInfo.ScopeZoneTree);

    while (pScopeZoneInfo != NULL)
    {
        if ((pScopeZoneInfo->u4ContextId == u4ContextId) &&
            (u1DisplayHdr == FALSE))
        {
            u1DisplayHdr = TRUE;
            VcmGetAliasName (u4ContextId, au1ContextName);
            CliPrintf (CliHandle, "\n\tScope\t   \t\t\tDefault-Scope-Zone\n");
            CliPrintf (CliHandle, "Context: %s\r\n", au1ContextName);
        }
        if ((pScopeZoneInfo->u4ContextId == u4ContextId) &&
            (pScopeZoneInfo->u1IsDefaultZone == IP6_ZONE_TRUE))
        {
/*            VcmGetAliasName (u4ContextId, au1ContextName);
            CliPrintf (CliHandle, "Context: %s\r\n", au1ContextName);*/

            Ip6ZoneGetZoneIdFromZoneName (pScopeZoneInfo->au1ZoneName, &u1Scope,
                                          &i4ZoneId);
            MEMSET (au1ZoneNameDisplay, 0, IP6_SCOPE_ZONE_NAME_LEN + 2);
            for (i1Length = 0; i1Length < IP6_SCOPE_ZONE_NAME_LEN; i1Length++)
            {
                if (pScopeZoneInfo->au1ZoneName[i1Length] >= (UINT1) '0' &&
                    pScopeZoneInfo->au1ZoneName[i1Length] <= (UINT1) '9')
                {
                    au1ZoneNameDisplay[i1Length] = (UINT1) ' ';
                    break;
                }
                au1ZoneNameDisplay[i1Length] =
                    pScopeZoneInfo->au1ZoneName[i1Length];
            }
            for (; ((i1Length < IP6_SCOPE_ZONE_NAME_LEN) &&
                    (pScopeZoneInfo->au1ZoneName[i1Length] != (UINT1) '\0'));
                 i1Length++)
            {
                au1ZoneNameDisplay[i1Length + 1] =
                    pScopeZoneInfo->au1ZoneName[i1Length];
            }
            if (u1Scope == ADDR6_SCOPE_GLOBAL)
            {
                CliPrintf (CliHandle, "\t%-32s%-44s\r\n",
                           gIp6GblInfo.asIp6ScopeName[u1Scope].au1ZoneName,
                           au1ZoneNameDisplay);
            }
            else
            {
                CliPrintf (CliHandle, "\t%-32s%-44s\r\n",
                           gIp6GblInfo.asIp6ScopeName[u1Scope].au1ZoneName,
                           au1ZoneNameDisplay);
            }
        }
        if (pScopeZoneInfo->u4ContextId > u4ContextId)
            break;

        pNextScopeZoneInfo =
            (tIp6ScopeZoneInfo *) RBTreeGetNext (gIp6GblInfo.ScopeZoneTree,
                                                 (tRBElem *) pScopeZoneInfo,
                                                 NULL);
        pScopeZoneInfo = pNextScopeZoneInfo;
    }
#endif
    return;
}

/*********************************************************************
 *  Function Name : Ip6ShowZoneInCxt 
 *  Description   : Displays the Scope-Zone on an Interface
 *  Input(s)      : CliHandle
 *                  i4IfaceIndex - Interface Index
 *                  u4VcId - Context Id
 *  Output(s)     : 
 *  Return Values : None.
 *  Comment       : Added as a part of RFC4007 Code changes
 *********************************************************************/
VOID
Ip6ShowZoneInCxt (tCliHandle CliHandle, UINT4 u4IfIndex, UINT4 u4VcId)
{
    INT4                i4RetVal = VCM_FAILURE;
    UINT4               u4L3ContextId = 0;
    UINT4               u4NextL3ContextId = 0;
    UINT4               au4Ip6Ifaces[IP6_MAX_LOGICAL_IFACES + 1];
    UINT1               au1ContextName[VCMALIAS_MAX_ARRAY_LEN];
    UINT2               u2Cntr = IP6_ZERO;
    UINT1               u1DisplayHdr = TRUE;

    if (u4IfIndex == 0)
    {
        if (u4VcId == VCM_INVALID_VC)
        {
            i4RetVal = VcmGetFirstActiveL3Context (&u4L3ContextId);
        }
        else
        {
            u4L3ContextId = u4VcId;
            i4RetVal = VCM_SUCCESS;
        }

        while (i4RetVal == VCM_SUCCESS)
        {
            if (u4VcId == VCM_INVALID_VC)
            {
                i4RetVal = VcmGetNextActiveL3Context (u4L3ContextId,
                                                      &u4NextL3ContextId);
                u1DisplayHdr = TRUE;
            }
            else
            {
                i4RetVal = VCM_FAILURE;
            }
            MEMSET (au4Ip6Ifaces, IP6_ZERO,
                    (IP6_MAX_LOGICAL_IFACES + 1) * sizeof (UINT4));
            if (VcmGetCxtIpIfaceList (u4L3ContextId, au4Ip6Ifaces)
                == VCM_FAILURE)
            {
                u4L3ContextId = u4NextL3ContextId;
                continue;
            }
            VcmGetAliasName (u4L3ContextId, au1ContextName);
            for (u2Cntr = 1; ((u2Cntr <= au4Ip6Ifaces[0]) &&
                              (u2Cntr < IP6_MAX_LOGICAL_IFACES + 1)); u2Cntr++)
            {
                /* Check for Valid Interface . */
                if (Ip6ifEntryExists (au4Ip6Ifaces[u2Cntr]) != IP6_FAILURE)
                {
                    if (u1DisplayHdr == TRUE)
                    {
                        CliPrintf (CliHandle,
                                   "\r\n   Scope-Zones Interface Map Table \r\n");
                        CliPrintf (CliHandle, "\r\nVRF Id  : %d\r\n",
                                   u4L3ContextId);
                        CliPrintf (CliHandle, "VRF Name: %s\r\n",
                                   au1ContextName);
                        CliPrintf (CliHandle, "\nInterface\t    Scope-Zones");
                        u1DisplayHdr = FALSE;

                    }

                    Ip6ShowIfZoneInfoInCxt (CliHandle, au4Ip6Ifaces[u2Cntr],
                                            u4L3ContextId);
                }
            }
            CliPrintf (CliHandle, "\n");
            u4L3ContextId = u4NextL3ContextId;
        }
    }
    else
    {
        /* Check for Valid Interface . */
        if (Ip6ifEntryExists (u4IfIndex) == IP6_FAILURE)
            /* Need to Display the detailed information for the given interface */
        {
            CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
            return;
        }

        VcmGetContextIdFromCfaIfIndex (u4IfIndex, &u4L3ContextId);
        if ((u4VcId != VCM_INVALID_VC) && (u4L3ContextId != u4VcId))
        {
            /*GIven interface does not belong to given VRF */
            return;
        }

        VcmGetAliasName (u4L3ContextId, au1ContextName);
        CliPrintf (CliHandle, "\r\n   Scope-Zones Interface Map Table \r\n");

        CliPrintf (CliHandle, "\r\nVRF Id  : %d\r\n", u4L3ContextId);
        CliPrintf (CliHandle, "VRF Name: %s\r\n", au1ContextName);
        CliPrintf (CliHandle, "\nInterface\t    Scope-Zones");

        Ip6ShowIfZoneInfoInCxt (CliHandle, u4IfIndex, u4L3ContextId);
        CliPrintf (CliHandle, "\n");
    }
    return;
}

/*********************************************************************
 *  Function Name : Ip6ShowIfZoneInfoInCxt 
 *  Description   : Displays all the scope-zone created on this given
 *                  interface.
 *  Input(s)      : CliHandle, u4IfIndex
 *  Output(s)     :
 *  Return Values : None
 *  Comment       : Added as a part of RFC4007 Code changes
 * *********************************************************************/

VOID
Ip6ShowIfZoneInfoInCxt (tCliHandle CliHandle, UINT4 u4IfIndex,
                        UINT4 u4L3ContextId)
{
#ifndef LNXIP6_WANTED
    tIp6IfZoneMapInfo  *pIp6IfZoneMapInfo = NULL;
    tTMO_SLL_NODE      *pZoneSll;
    tTMO_SLL_NODE      *pNextZoneSll;
    INT1               *pi1IfName;
    UINT1               au1IfName[CFA_CLI_MAX_IF_NAME_LEN];
    UINT1               u1Count = 0;
    UINT1               u1DisplayHdr = TRUE;
    UINT1               au1ContextName[VCMALIAS_MAX_ARRAY_LEN];

    tIp6If             *pIf6 = (tIp6If *) IP6_INTERFACE (u4IfIndex);
    if (pIf6 == NULL)
    {
        return;
    }
    pi1IfName = (INT1 *) &au1IfName[0];
    CfaCliGetIfName (u4IfIndex, pi1IfName);

    VcmGetAliasName (u4L3ContextId, au1ContextName);

    pZoneSll = TMO_SLL_First (&pIf6->zoneIlist);

    while (pZoneSll)
    {
        if (u1DisplayHdr == TRUE)
        {
            CliPrintf (CliHandle, "\r\n %s\t\t", pi1IfName);
            u1DisplayHdr = FALSE;
        }
        if (u1Count >= 5)
        {
            CliPrintf (CliHandle, "\r\n");
            u1Count = 0;
        }
        pIp6IfZoneMapInfo = IP6_SCOPE_ZONE_PTR_FROM_SLL (pZoneSll);
        pNextZoneSll = TMO_SLL_Next (&pIf6->zoneIlist, pZoneSll);
        pZoneSll = pNextZoneSll;
        CliPrintf (CliHandle, " %s ", pIp6IfZoneMapInfo->au1ZoneName);
        u1Count++;
    }
#else
    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u4L3ContextId);
#endif

}

/*********************************************************************
 *  Function Name : Ip6ShowZoneInfoForAllVlanInCxt 
 *  Description   : Displays all the scope-zone for all the 
 *                  ipv6 interface with name "pu1IfName"
 *  Input(s)      : CliHandle, IfIndex, VcId, pu1IfName
 *  Output(s)     :
 *  Return Values : None
 *  Comment       : Added as a part of RFC4007 Code changes
 * *********************************************************************/
VOID
Ip6ShowZoneInfoForAllVlanInCxt (tCliHandle CliHandle, UINT4 u4IfIndex,
                                UINT4 u4VcId, UINT1 *pu1IfName)
{
    UINT4               u4NextIfIndex = 0;
    UINT4               u4IfVcId = VCM_INVALID_VC;
    UINT4               u4ShowAllCxt = FALSE;

    if (u4VcId == VCM_INVALID_VC)
    {
        u4ShowAllCxt = TRUE;
    }
    if (CfaCliGetFirstIfIndexFromName
        (pu1IfName, &u4NextIfIndex) != OSIX_FAILURE)
    {
        do
        {
            u4IfIndex = u4NextIfIndex;
            VcmGetContextIdFromCfaIfIndex (u4IfIndex, &u4IfVcId);
            if (u4ShowAllCxt == TRUE)
            {
                u4VcId = u4IfVcId;
            }
            Ip6ShowZoneInCxt (CliHandle, u4IfIndex, u4VcId);
            if ((u4ShowAllCxt == FALSE) && (u4VcId == u4IfVcId))
            {
                return;
            }
        }
        while (CfaCliGetNextIfIndexFromName
               (pu1IfName, u4IfIndex, &u4NextIfIndex) != OSIX_FAILURE);
    }
    return;

}

/*********************************************************************
 *  Function Name : IP6ShowZoneInterfacesInCxt 
 *  Description   : Displays all the interfaces that are associated 
 *                  with this scope-zone
 *  Input(s)      : CliHandle, VcId, au1ScopeZone
 *  Output(s)     : None
 *  Return Values : None
 *  Comment       : Added as a part of RFC4007 Code changes
 *********************************************************************/

VOID
IP6ShowZoneInterfacesInCxt (tCliHandle CliHandle, UINT4 u4VcId,
                            UINT1 *au1ScopeZone)
{
    tSNMP_OCTET_STRING_TYPE ZoneName;
    tSNMP_OCTET_STRING_TYPE ZoneInterfaceList;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    UINT1               au1ZoneName[IP6_SCOPE_ZONE_NAME_LEN];
    tInterfaceList     *pInterfaceList;
    UINT1               u1ZoneLen = 0;
    UNUSED_PARAM (u4VcId);

    pInterfaceList =
        (tInterfaceList *) ((VOID *)
                            FsUtilAllocBitList (sizeof (tInterfaceList)));

    if (pInterfaceList == NULL)
    {
        CliPrintf (CliHandle, "\r%% Error in Allocating memory "
                   "for bitlist\r\n");
        return;
    }

    u1ZoneLen = STRLEN (au1ScopeZone);
    MEMSET (au1ZoneName, IP6_ZERO, IP6_SCOPE_ZONE_NAME_LEN);
    MEMSET (pInterfaceList, IP6_ZERO, sizeof (tInterfaceList));
    ZoneName.pu1_OctetList = au1ZoneName;
    STRCPY (ZoneName.pu1_OctetList, au1ScopeZone);
    ZoneName.i4_Length = u1ZoneLen;
    ZoneInterfaceList.pu1_OctetList = (UINT1 *) pInterfaceList;
    ZoneInterfaceList.i4_Length = sizeof (tInterfaceList);

    if (SNMP_FAILURE ==
        nmhValidateIndexInstanceFsipv6ScopeZoneTable (&ZoneName))
    {
        FsUtilReleaseBitList ((UINT1 *) pInterfaceList);
        return;
    }

    if (SNMP_FAILURE ==
        nmhGetFsipv6ScopeZoneInterfaceList (&ZoneName, &ZoneInterfaceList))
    {
        FsUtilReleaseBitList ((UINT1 *) pInterfaceList);
        return;
    }

    CliPrintf (CliHandle, "\tScope-Zones\t     Interface\n");
    CliPrintf (CliHandle, "\t%s\t ", ZoneName);
    if (CLI_FAILURE == IP6ShowInterfacesInZone (CliHandle,
                                                &ZoneInterfaceList,
                                                sizeof (tInterfaceList),
                                                &u4PagingStatus, 6))
    {
        FsUtilReleaseBitList ((UINT1 *) pInterfaceList);
        return;
    }
    FsUtilReleaseBitList ((UINT1 *) pInterfaceList);
    return;
}

VOID
Icmp6ErrMsgRateLimit (tCliHandle CliHandle, UINT4 i4IfaceIndex,
                      INT4 i4IcmpErrInterval, INT4 i4IcmpTokenBucketSize)
{
    UINT4               u4ErrorCode = 0;

    /*
       nmhGetFsipv6IfIcmpErrInterval(i4IfaceIndex ,
       &i4IcmpErrInterval);
       nmhGetFsipv6IfIcmpTokenBucketSize(i4IfaceIndex,
       &i4IcmpTokenBucketSize); */

    if (nmhTestv2Fsipv6IfIcmpErrInterval (&u4ErrorCode, i4IfaceIndex,
                                          i4IcmpErrInterval) == SNMP_FAILURE)
    {
        return;
    }
    if (nmhSetFsipv6IfIcmpErrInterval (i4IfaceIndex, i4IcmpErrInterval)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return;
    }

    if (nmhTestv2Fsipv6IfIcmpTokenBucketSize (&u4ErrorCode, i4IfaceIndex,
                                              i4IcmpTokenBucketSize) ==
        SNMP_FAILURE)
    {
        return;
    }

    if (nmhSetFsipv6IfIcmpTokenBucketSize (i4IfaceIndex, i4IcmpTokenBucketSize)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return;
    }
}

VOID
Ip6IfDisabeDestUnreachMsg (tCliHandle CliHandle, UINT4 i4IfaceIndex,
                           INT4 i4DestUnreachable)
{
    UINT4               u4ErrorCode = 0;

    /*
       nmhGetFsipv6IfDestUnreachableMsg(i4IfaceIndex, &i4DestUnreachable); 

     */
    if (nmhTestv2Fsipv6IfDestUnreachableMsg (&u4ErrorCode, i4IfaceIndex,
                                             i4DestUnreachable) == SNMP_FAILURE)
    {
        return;
    }

    if (nmhSetFsipv6IfDestUnreachableMsg (i4IfaceIndex, i4DestUnreachable)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return;
    }
}

/******************************************************************************
 *  Function Name : Ip6UnnumIf
 *  Description   : Set the Assossiated interface for this unnumered interface
 *  Input(s)      : CliHandle, i4IfaceIndex, i4UnnumAssocIPIf
 *  Output(s)     : None
 *  Return Values : None
 ******************************************************************************/
VOID
Ip6UnnumIf (tCliHandle CliHandle, INT4 i4IfaceIndex, INT4 i4UnnumAssocIPIf)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2Fsipv6IfUnnumAssocIPIf (&u4ErrorCode, i4IfaceIndex,
                                         i4UnnumAssocIPIf) == SNMP_FAILURE)
    {
        return;
    }

    if (nmhSetFsipv6IfUnnumAssocIPIf (i4IfaceIndex,
                                      i4UnnumAssocIPIf) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return;
    }
    return;
}

/******************************************************************************
 *  Function Name : Ip6IfIcmpRedirectMsg 
 *  Description   : Calls the Set and Test routines for ICMP redirection messages.
 *  Input(s)      : CliHandle, i4IfaceIndex, i4RedirectMsg
 *  Output(s)     : None
 *  Return Values : None
 * ******************************************************************************/

VOID
Ip6IfIcmpRedirectMsg (tCliHandle CliHandle, UINT4 i4IfaceIndex,
                      INT4 i4RedirectMsg)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2Fsipv6IfRedirectMsg (&u4ErrorCode, i4IfaceIndex,
                                      i4RedirectMsg) == SNMP_FAILURE)
    {
        return;
    }

    if (nmhSetFsipv6IfRedirectMsg (i4IfaceIndex, i4RedirectMsg) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return;
    }
}

/******************************************************************************
 *  Function Name : Ip6NdCacheTimeout
 *  Description   : Calls the Set and Test routines for ND Cache Timeout
 *  Input(s)      : CliHandle, i4NdCacheTimeout
 *  Output(s)     : None
 *  Return Values : None
 * ******************************************************************************/
VOID
Ip6NdCacheTimeout (tCliHandle CliHandle, INT4 i4NdCacheTimeout)
{
    UINT4               u4ErrorCode = 0;
    if (nmhTestv2Fsipv6NdCacheTimeout (&u4ErrorCode, i4NdCacheTimeout)
        == SNMP_FAILURE)
    {
        return;
    }

    if (nmhSetFsipv6NdCacheTimeout (i4NdCacheTimeout) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return;
    }
}

/*********************************************************************
 *  Function Name : cli_process_rtm6_cmd
 *  Description   : Configuration of RTM6 commands
 *  Input(s)      : CliHandle
  *  Output(s)     : None
 *  Return Values : CLI_SUCCESS/CLI_FAILURE.
 **********************************************************************/
VOID
cli_process_rtm6_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT1              *args[IP6_CLI_MAX_ARGS];
    INT1                argno = IP6_ZERO;
    INT4                i4PrefixLen = IP6_ZERO;
    INT4                i4IfaceIndex = IP6_ZERO;
    INT4                i4RoutePreference = IP6_ZERO;
    UINT4               u4ErrorCode = IP6_ZERO;
    tIp6Addr            NextHop;
    tIp6Addr            RouteDest;
    INT4                i4RetStatus = CLI_FAILURE;
    UINT1              *pu1IpCxtName = NULL;
    UINT4               u4VcId = 0;
    INT4                i4AddrType = ADDR6_UNICAST;
    INT4                i4Protocol = IP6_ZERO;
    UINT1               u1Flag = IP6_ZERO;
    INT4                i4Ip6DefAdminDistance = IP6_ZERO;
    UNUSED_PARAM (i4RetStatus);
    CLI_SET_CMD_STATUS (CLI_SUCCESS);

    va_start (ap, u4Command);

    /* Third arguement is always InterfaceName/Index */
    i4IfaceIndex = va_arg (ap, INT4);

    /* Fourth argument is ContextName */
    pu1IpCxtName = va_arg (ap, UINT1 *);

    /* Walk through the rest of the arguements and store in args array. 
     * Store 12 arguements at the max. This is because ipv6 commands do not
     * take more than 12 inputs from the command line. Another reason to
     * store is in some cases first input may be optional but user may give 
     * second input. In that case first arg will be null and second arg only 
     * has value */

    while (1)
    {
        args[argno++] = va_arg (ap, UINT1 *);
        if (argno == IP6_CLI_MAX_ARGS)
            break;
    }
    va_end (ap);

    CliRegisterLock (CliHandle, Rtm6Lock, Rtm6UnLock);
    CLI_SET_ERR (0);
    RTM6_TASK_LOCK ();

    /*Get context Id from context Name */
    if (pu1IpCxtName != NULL)
    {
        if (VcmIsVrfExist (pu1IpCxtName, &u4VcId) == VCM_FALSE)
        {
            CliPrintf (CliHandle, "\r%% Invalid VRF Id\r\n");
            CliUnRegisterLock (CliHandle);
            RTM6_TASK_UNLOCK ();
            return;
        }
    }
    else
    {
        u4VcId = VCM_DEFAULT_CONTEXT;
    }

    /*Set Context Id for IP6 which will be used by the following
       configuration commands */
    if (UtilRtm6SetContext (u4VcId) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Invalid VRF Id\r\n");
        CliUnRegisterLock (CliHandle);
        RTM6_TASK_UNLOCK ();
        return;
    }

    switch (u4Command)
    {
        case IP6_CLI_ROUTE_ADD:
            MEMSET (&RouteDest, 0, sizeof (tIp6Addr));

            if (INET_ATON6 (args[0], &RouteDest) == 0)
            {
                CLI_SET_ERR (CLI_IP6_INVALID_ADDRESS);
                break;
            }

            MEMSET (&NextHop, 0, sizeof (tIp6Addr));

            if (args[1] != NULL)
            {
                if (INET_ATON6 (args[1], &NextHop) == 0)
                {
                    CLI_SET_ERR (CLI_IP6_INVALID_NEXTHOP);
                    break;
                }
            }

            i4PrefixLen = *(INT4 *) (VOID *) args[2];
            if ((i4PrefixLen < 0) || (i4PrefixLen > 128))
            {
                CLI_SET_ERR (CLI_IP6_INVALID_PREFIXLEN);
                break;
            }

            if (args[3] != NULL)
            {
                i4RoutePreference = *(UINT4 *) (VOID *) (args[3]);
            }
            else
            {
                /* Get the default Adminstrative distance for given context */
                if ((nmhGetFsRtm6StaticRouteDistance (&i4RoutePreference))
                    != SNMP_SUCCESS)

                {
                    CLI_SET_ERR (CLI_IP6_DISTANCE_GET_ERR);
                    break;
                }
            }

            if (args[4] != NULL)
            {
                i4AddrType = CLI_PTR_TO_I4 (args[4]);
            }
            Ip6RouteAddInCxt (CliHandle, (UINT4) i4IfaceIndex, u4VcId,
                              RouteDest, NextHop, i4PrefixLen,
                              i4RoutePreference, i4AddrType);
            break;

        case IP6_CLI_ROUTE_DEL:
            MEMSET (&RouteDest, 0, sizeof (tIp6Addr));
            if (INET_ATON6 (args[0], &RouteDest) == 0)
            {
                CLI_SET_ERR (CLI_IP6_INVALID_ADDRESS);
                break;
            }

            i4PrefixLen = *(INT4 *) (VOID *) args[2];
            if ((i4PrefixLen < 0) || (i4PrefixLen > 128))
            {
                CLI_SET_ERR (CLI_IP6_INVALID_PREFIXLEN);
                break;
            }
            MEMSET (&NextHop, 0, sizeof (tIp6Addr));
            if (args[1])
            {
                if (INET_ATON6 (args[1], &NextHop) == 0)
                {
                    CLI_SET_ERR (CLI_IP6_INVALID_NEXTHOP);
                    break;
                }
            }
            else
            {
                if (i4IfaceIndex == 0)
                {
                    CLI_SET_ERR (CLI_IP6_INVALID_NEXTHOP);
                    break;
                }
            }

            Ip6RouteDel (CliHandle, RouteDest, i4PrefixLen, NextHop);
            break;

        case IP6_RT_SHOW:
            if (pu1IpCxtName == NULL)
            {
                u4VcId = VCM_INVALID_VC;
            }

            MEMSET (&RouteDest, 0, sizeof (tIp6Addr));

            /* Copy the Protocol ID , incase protocol is not specified '0'
             * will be passed inorder to display all the routes */
            i4Protocol = CLI_PTR_TO_I4 (args[0]);

            if (args[1] != NULL)
            {
                if (INET_ATON6 (args[1], &RouteDest) == 0)
                {
                    CLI_SET_ERR (CLI_IP6_INVALID_NEXTHOP);
                    break;
                }
                u1Flag = TRUE;
            }

            if (args[2] != NULL)
            {
                i4PrefixLen = *(INT4 *) (VOID *) args[2];
            }

            switch (i4Protocol)
            {
                case SHOW_RIP_PROTOID:
                    i4Protocol = IANAIP_ROUTE_PROTO_RIP;
                    break;
                case SHOW_OSPF_PROTOID:
                    i4Protocol = IANAIP_ROUTE_PROTO_OSPF;
                    break;
                case SHOW_BGP_PROTOID:
                    i4Protocol = IANAIP_ROUTE_PROTO_BGP;
                    break;
                case SHOW_ISIS_PROTOID:
                    i4Protocol = IANAIP_ROUTE_PROTO_ISIS;
                    break;
            }

            RTM6_TASK_UNLOCK ();
            Ip6RouteShowInCxt (CliHandle, (INT4) u4VcId, i4Protocol, u1Flag,
                               RouteDest, i4PrefixLen);

            RTM6_TASK_LOCK ();
            CliPrintf (CliHandle, "\r\n");
            break;

        case IP6_RT_SUMMARY_SHOW:
            if (pu1IpCxtName == NULL)
            {
                u4VcId = VCM_INVALID_VC;
            }
            Ip6RouteSummaryShowInCxt (CliHandle, u4VcId);
            CliPrintf (CliHandle, "\r\n");
            break;

        case IP6_RT_FAIL_SHOW:
            if (pu1IpCxtName == NULL)
            {
                u4VcId = VCM_INVALID_VC;
            }
            RTM6_TASK_UNLOCK ();
            Ip6FrtRouteShowInCxt (CliHandle);
            RTM6_TASK_LOCK ();
            CliPrintf (CliHandle, "\r\n");
            break;

        case IP6_CLEAR_ROUTE:
            if (pu1IpCxtName == NULL)
            {
                u4VcId = VCM_INVALID_VC;
            }
            RTM6_TASK_UNLOCK ();
            Ip6ClearRoute (CliHandle, u4VcId);
            RTM6_TASK_LOCK ();
            break;

        case CLI_IP6_ROUTE_DEF_DISTANCE:
            /* Default Administrative distance of default/user VRF static route is 
               passed in args[0] */
            i4Ip6DefAdminDistance = *(INT4 *) (VOID *) (args[0]);

            RTM6_TASK_UNLOCK ();
            Ip6SetDefAdminDistance (u4VcId, i4Ip6DefAdminDistance);
            RTM6_TASK_LOCK ();
            break;

        case CLI_IP6_SHOW_DEF_DISTANCE:
            if (pu1IpCxtName == NULL)
            {
                /* If no VRF/Context specified */
                u4VcId = VCM_INVALID_VC;
            }
            Ip6ShowDefAdminDistance (CliHandle, u4VcId);
            break;

        default:
            CliPrintf (CliHandle, "%%Command Not Supported\r\n");
            break;
    }

    /*Reset context */
    UtilRtm6ResetContext ();
    if (CLI_GET_ERR (&u4ErrorCode) == CLI_SUCCESS)
    {
        if ((u4ErrorCode > 0) && (u4ErrorCode < CLI_IP6_MAX_ERR))
        {
            CliPrintf (CliHandle, "\r%s", Ip6CliErrString[u4ErrorCode]);
            CLI_SET_CMD_STATUS (CLI_FAILURE);

        }
        CLI_SET_ERR (0);
    }
    CLI_SET_CMD_STATUS ((UINT4) i4RetStatus);
    CliUnRegisterLock (CliHandle);
    RTM6_TASK_UNLOCK ();
    return;
}

/*********************************************************************
 *  Function Name : Ip6IfTrafficShowForAllVlanInCxt
 *  Description   : If Statistics for all ipv6 interfaces 
 *                  with name "pu1IfName" 
 *  Input(s)      : CliHandle, IfIndex, HCFlag,VcId, pu1IfName
 *  Output(s)     :  
 *  Return Values : CLI_FAILURE, CLI_SUCCESS.
 *********************************************************************/
INT4
Ip6IfTrafficShowForAllVlanInCxt (tCliHandle CliHandle,
                                 UINT4 u4IfIndex, UINT1 u1HCFlag,
                                 UINT4 u4VcId, UINT1 *pu1IfName)
{
    INT4                i4RetVal = CLI_FAILURE;
    UINT4               u4NextIfIndex = 0;
    UINT4               u4IfVcId = VCM_INVALID_VC;
    UINT4               u4ShowAllCxt = FALSE;

    if (u4VcId == VCM_INVALID_VC)
    {
        u4ShowAllCxt = TRUE;
    }
    u4IfIndex++;
    while (CfaCliGetNextIfIndexFromName (pu1IfName, u4IfIndex,
                                         &u4NextIfIndex) != OSIX_FAILURE)
    {
        u4IfIndex = u4NextIfIndex;
        VcmGetContextIdFromCfaIfIndex (u4IfIndex, &u4IfVcId);
        if (u4ShowAllCxt == TRUE)
        {
            u4VcId = u4IfVcId;
        }
        i4RetVal = Ip6IfTrafficShowInCxt (CliHandle, u4IfIndex,
                                          u1HCFlag, u4VcId);
        if (((u4ShowAllCxt == FALSE) &&
             (u4VcId == u4IfVcId)) || (i4RetVal == CLI_FAILURE))

        {
            return i4RetVal;
        }
    }
    return i4RetVal;

}

/*********************************************************************
 *  Function Name : Ip6ShowPrefixForAllVlanInCxt
 *  Description   : Displays the Prefix to be advertised in RA
 *                 for all the ipv6 interface with name "pu1IfName" 
 *  Input(s)      : CliHandle, IfIndex, VcId, pu1IfName
 *  Output(s)     :  
 *  Return Values : None
 *********************************************************************/
VOID
Ip6ShowPrefixForAllVlanInCxt (tCliHandle CliHandle,
                              UINT4 u4IfIndex, UINT4 u4VcId, UINT1 *pu1IfName)
{
    UINT4               u4NextIfIndex = 0;
    UINT4               u4IfVcId = VCM_INVALID_VC;
    UINT4               u4ShowAllCxt = FALSE;

    if (u4VcId == VCM_INVALID_VC)
    {
        u4ShowAllCxt = TRUE;
    }
    while (CfaCliGetNextIfIndexFromName (pu1IfName, u4IfIndex,
                                         &u4NextIfIndex) != OSIX_FAILURE)
    {
        u4IfIndex = u4NextIfIndex;
        VcmGetContextIdFromCfaIfIndex (u4IfIndex, &u4IfVcId);
        if (u4ShowAllCxt == TRUE)
        {
            u4VcId = u4IfVcId;
        }
        Ip6ShowPrefixInCxt (CliHandle, u4IfIndex, u4VcId);
        if ((u4ShowAllCxt == FALSE) && (u4VcId == u4IfVcId))
        {
            return;
        }
    }
    return;
}

/*********************************************************************
 *  Function Name : Ip6IfShowForAllVlanInCxt
 *  Description   : Display interface configurations and Status 
 *                 for all the ipv6 interface with name "pu1IfName" 
 *  Input(s)      : CliHandle, IfIndex, VcId, pu1IfName
 *  Output(s)     :  
 *  Return Values : None
 *********************************************************************/
VOID
Ip6IfShowForAllVlanInCxt (tCliHandle CliHandle,
                          UINT4 u4IfIndex, UINT4 u4VcId, UINT1 *pu1IfName)
{
    UINT4               u4NextIfIndex = 0;
    UINT4               u4IfVcId = VCM_INVALID_VC;
    UINT4               u4ShowAllCxt = FALSE;

    if (u4VcId == VCM_INVALID_VC)
    {
        u4ShowAllCxt = TRUE;
    }
    while (CfaCliGetNextIfIndexFromName (pu1IfName, u4IfIndex,
                                         &u4NextIfIndex) != OSIX_FAILURE)
    {
        u4IfIndex = u4NextIfIndex;
        VcmGetContextIdFromCfaIfIndex (u4IfIndex, &u4IfVcId);
        if (u4ShowAllCxt == TRUE)
        {
            u4VcId = u4IfVcId;
        }
        Ip6IfShowInCxt (CliHandle, u4IfIndex, u4VcId);
        if ((u4ShowAllCxt == FALSE) && (u4VcId == u4IfVcId))
        {
            return;
        }
        return;
    }
    CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
    return;

}

/*********************************************************************
 *  Function Name : Ip6IfShowForIndex 
 *  Description   : Displays all the interface related configurations
 *                  for the given interface.
 *  Input(s)      : CliHandle, u4IfIndex
 *  Output(s)     :  
 *  Return Values : None
 *********************************************************************/
VOID
Ip6IfShowForIndex (tCliHandle CliHandle, UINT4 u4IfIndex)
{
    tShowIp6IntfAddrCookie Ip6InterfaceCookie;
    tShowIp6Interface   Ip6ShowInterface;
    tShowIp6Interface  *pShowIp6Interface;
    tShowIp6IntfAddr   *pShowIp6IntfAddr;
    tShowIp6IntfAddr   *pShowIp6AssocIntfAddr = NULL;
    tShowIp6IntfAddrAttr *pShowIp6IntfAddrAttr;
    tSNMP_OCTET_STRING_TYPE tAddress;
#ifdef LNXIP6_WANTED
    tSNMP_OCTET_STRING_TYPE taddress;
#endif
    tSNMP_OCTET_STRING_TYPE tPrevAddress;
    INT1                i1RetVal = SNMP_FAILURE;
    INT1                i1Val = CLI_FAILURE;
    INT4                i4Index = 0;
    INT4                i4PrefLen = 0;
    INT4                i4PrevIndex;
    INT4                i4PrevPrefLen;

    UINT1               au1InterfaceInfo[sizeof (tShowIp6IntfAddr) +
                                         ((IP6_SHOW_MAX_INTF_ADDR -
                                           1) * sizeof (tShowIp6IntfAddrAttr)) +
                                         (2 * sizeof (UINT4))];
    UINT1               au1AssocIntfInfo[sizeof (tShowIp6IntfAddr) +
                                         ((IP6_SHOW_MAX_INTF_ADDR -
                                           1) * sizeof (tShowIp6IntfAddrAttr)) +
                                         (2 * sizeof (UINT4))];
    UINT1              *pu1GlobalZone = (UINT1 *) "GLOBAL";
#ifdef TUNNEL_WANTED
    CHR1               *pi1IpAddressSrc;
    CHR1               *pi1IpAddressDest;
    CHR1                au1SrcIpAddress[IPVX_IPV6_ADDR_LEN];
#endif /* TUNNEL_WANTED */
    UINT4               u4NoOfAddr = 0;
    UINT4               u4BufSize = 0;
    INT4                i4ProxyAdStat = 0;
    INT4                i4ProxyMode = 0;
    INT4                i4ProxyUpstream = 0;
    INT4                i4ProxyOperStat = 0;
    INT4                i4SeNDStatus = IP6_ZERO;
    INT4                i4DefRoutePref = IP6_ZERO;
    INT1               *pi1IfName;
    UINT1               au1IfName[CFA_CLI_MAX_IF_NAME_LEN];
    INT1               *pi1UnnumAssocIfName;
    UINT1               au1UnnumAssocIfName[CFA_CLI_MAX_IF_NAME_LEN];
    UINT1               u1LinuxStallStatus = FALSE;
#ifdef LNXIP6_WANTED
    UINT1               au1Temp2[IP6_ADDR_SIZE];
    UINT4               u4RAObjVal;
#endif
    UINT1               au1Temp1[IP6_ADDR_SIZE];
    UINT1               au1Temp[IP6_ADDR_SIZE];
#ifdef LNXIP6_WANTED
    tShowIp6Interface   Ip6ShowAssocInterface;
    tShowIp6Interface  *pShowIp6AssocInterface;
#endif
    UINT1               u1IfType;

    /* Interfaces of type CFA_OTHER should not be displayed in Show commands */
    /* At present Connecting Interfaces used in Dual Unit Stacking are
       created with this type */
    CfaGetIfType (u4IfIndex, &u1IfType);
    if (u1IfType == CFA_OTHER)
    {
        return;
    }

    pi1IfName = (INT1 *) &au1IfName[0];
    pi1UnnumAssocIfName = (INT1 *) &au1UnnumAssocIfName[0];

    MEMSET (&Ip6ShowInterface, 0, sizeof (tShowIp6Interface));
    pShowIp6Interface = &Ip6ShowInterface;

#ifdef LNXIP6_WANTED
    MEMSET (&Ip6ShowAssocInterface, 0, sizeof (tShowIp6Interface));
    pShowIp6AssocInterface = &Ip6ShowAssocInterface;
#endif

    MEMSET (au1IfName, 0, CFA_CLI_MAX_IF_NAME_LEN);
    MEMSET (au1UnnumAssocIfName, 0, CFA_CLI_MAX_IF_NAME_LEN);

    MEMSET (au1InterfaceInfo, 0, (sizeof (tShowIp6IntfAddr) +
                                  ((IP6_SHOW_MAX_INTF_ADDR -
                                    1) * sizeof (tShowIp6IntfAddrAttr)) +
                                  (2 * sizeof (UINT4))));

    MEMSET (au1AssocIntfInfo, 0, (sizeof (tShowIp6IntfAddr) +
                                  ((IP6_SHOW_MAX_INTF_ADDR -
                                    1) * sizeof (tShowIp6IntfAddrAttr)) +
                                  (2 * sizeof (UINT4))));
    tAddress.pu1_OctetList = au1Temp;
    MEMSET (au1Temp, 0, IP6_ADDR_SIZE);

#ifdef LNXIP6_WANTED
    MEMSET (au1Temp2, 0, IP6_ADDR_SIZE);
    taddress.pu1_OctetList = au1Temp2;
#endif

    tPrevAddress.pu1_OctetList = au1Temp1;
    MEMSET (au1Temp1, 0, IP6_ADDR_SIZE);

    CfaCliGetIfName (u4IfIndex, pi1IfName);

    /* Allocate Memory for Route Display. */
    u4BufSize = sizeof (tShowIp6IntfAddr) +
        ((IP6_SHOW_MAX_INTF_ADDR - 1) * sizeof (tShowIp6IntfAddrAttr));

    Ip6ShowGetInterface (pShowIp6Interface, (INT4) u4IfIndex);
    i1Val = Ip6GetRADNSStatus ((INT4) u4IfIndex,
                               &pShowIp6Interface->i4RADNSStateActive);

#ifdef LNXIP6_WANTED
    u1LinuxStallStatus = Lip6GetStallStatus (u4IfIndex);
#endif

    if (pShowIp6Interface->i4AdminStatus == NETIPV6_ADMIN_UP)
    {

        if ((pShowIp6Interface->i4OperStatus == NETIPV6_OPER_UP)
            && (u1LinuxStallStatus == FALSE))
        {
            CliPrintf (CliHandle, "%s is up, line protocol is up\r\n",
                       pi1IfName);
        }
        else if ((pShowIp6Interface->i4OperStatus == OPER_NOIFIDENTIFIER)
                 || (u1LinuxStallStatus == TRUE))
        {
            CliPrintf (CliHandle, "%s is up, line protocol is stalled\r\n",
                       pi1IfName);
        }
        else
        {
            CliPrintf (CliHandle, "%s is up, line protocol is down\r\n",
                       pi1IfName);
        }
    }
    else
    {
        if (pShowIp6Interface->i4OperStatus == NETIPV6_OPER_UP)
        {
            CliPrintf (CliHandle, "%s is down, line protocol is up\r\n",
                       pi1IfName);
        }
        else if ((pShowIp6Interface->i4OperStatus == OPER_NOIFIDENTIFIER)
                 || (u1LinuxStallStatus == TRUE))
        {
            CliPrintf (CliHandle,
                       "%s is down, line protocol is stalled\r\n", pi1IfName);
        }
        else
        {
            CliPrintf (CliHandle, "%s is down, line protocol is down\r\n",
                       pi1IfName);
        }
    }
#ifdef TUNNEL_WANTED
    if (pShowIp6Interface->i4IfType == IP6_TUNNEL_INTERFACE_TYPE)
    {
        /* Display the tunnel related parameters */
        CLI_CONVERT_IPADDR_TO_STR (pi1IpAddressSrc,
                                   pShowIp6Interface->u4SrcAddr);
        SNPRINTF (au1SrcIpAddress, IPVX_IPV6_ADDR_LEN, "%-16s",
                  pi1IpAddressSrc);
        CLI_CONVERT_IPADDR_TO_STR (pi1IpAddressDest,
                                   pShowIp6Interface->u4DestAddr);
        CliPrintf (CliHandle,
                   "    Tunl : Source -  %s Dst - %-16s",
                   au1SrcIpAddress, pi1IpAddressDest);
        if (pShowIp6Interface->u1TunlType <= TNL_TYPE_IPV6IP)
        {
            CliPrintf (CliHandle, " Mode - %-s",
                       patunlMode[pShowIp6Interface->u1TunlType]);
        }
        CliPrintf (CliHandle, "\r\n");
    }
#endif /* TUNNEL_WANTED */
    if (pShowIp6Interface->i4IfForwdStatus == IP6_IF_FORW_ENABLE)
    {
        CliPrintf (CliHandle, "    Forwarding operationally Enabled\r\n");
    }
    else
    {
        CliPrintf (CliHandle, "    Forwarding operationally Disabled\r\n");
    }
    /* Initialize the cookie */
    MEMSET (&Ip6InterfaceCookie, 0, sizeof (tShowIp6IntfAddrCookie));

    /* Display the Link-Local Address Information */
    /* Set the Cookie Address Type as Link-Local Address To get the
     * Link-Local Address */
    Ip6InterfaceCookie.u1AddrType = ADDR6_LLOCAL;

    if (Ip6ShowGetIntfAddr (au1InterfaceInfo, u4BufSize,
                            (UINT4) u4IfIndex, &Ip6InterfaceCookie)
        == IP6_FAILURE)
    {
        /* Error in retrieving the Interface information. */
        CLI_FATAL_ERROR (CliHandle);
        return;
    }

    pShowIp6IntfAddr = (tShowIp6IntfAddr *) (VOID *) au1InterfaceInfo;

    /*  CliPrintf (CliHandle, "    IPv6 is Enabled\r\n"); */
    CliPrintf (CliHandle, "    Link local address:\r\n");

#ifndef LNXIP6_WANTED
    if (pShowIp6IntfAddr->u4NoOfAddr == 0)
#else
    if ((pShowIp6IntfAddr->u4NoOfAddr == 0) ||
        ((pShowIp6Interface->i4AdminStatus == NETIPV6_ADMIN_DOWN) &&
         (pShowIp6Interface->i4OperStatus == NETIPV6_OPER_DOWN)))
#endif
    {
        CliPrintf (CliHandle, "        Not configured.\r\n");
    }
    else
    {
        for (;;)
        {
            pShowIp6IntfAddrAttr = &pShowIp6IntfAddr->aIntfAddrAttr[0];

            while (pShowIp6IntfAddr->u4NoOfAddr != 0)
            {
                /* Display the Link-Local Address */

                if (pShowIp6IntfAddrAttr->u1Status & ADDR6_FAILED)
                {
                    CliPrintf (CliHandle,
                               "        %s   [Duplicate][scope:Linklocal]",
                               Ip6PrintNtop ((tIp6Addr *) &
                                             pShowIp6IntfAddrAttr->Ip6Addr));
                }
#ifndef LNXIP6_WANTED
                else if (pShowIp6IntfAddrAttr->u1Status & ADDR6_DOWN)
#else
                else if ((pShowIp6IntfAddrAttr->u1Status & ADDR6_DOWN) ||
                         ((pShowIp6Interface->i4AdminStatus == NETIPV6_ADMIN_UP)
                          && (pShowIp6Interface->i4OperStatus ==
                              NETIPV6_OPER_DOWN)))
#endif
                {
                    CliPrintf (CliHandle,
                               "        %s   [Down] [scope:Linklocal]",
                               Ip6PrintNtop ((tIp6Addr *) &
                                             pShowIp6IntfAddrAttr->Ip6Addr));

                }
                else if (pShowIp6IntfAddrAttr->u1Status & ADDR6_TENTATIVE)
                {
                    CliPrintf (CliHandle,
                               "        %s   [Tentative] [scope:Linklocal]",
                               Ip6PrintNtop ((tIp6Addr *) &
                                             pShowIp6IntfAddrAttr->Ip6Addr));
                }
                else if ((pShowIp6IntfAddrAttr->
                          u1Status & ADDR6_PREFERRED) == 0)
                {
                    /* Address is not Preferred */
                    CliPrintf (CliHandle,
                               "        %s   [Deprecated][scope:Linklocal]",
                               Ip6PrintNtop ((tIp6Addr *) &
                                             pShowIp6IntfAddrAttr->Ip6Addr));
                }
                else
                {
                    CliPrintf (CliHandle, "        %s [scope: Linklocal]",
                               Ip6PrintNtop
                               ((tIp6Addr *) & pShowIp6IntfAddrAttr->Ip6Addr));
                }

                if (pShowIp6IntfAddrAttr->u1CfgMethod == IP6_ADDR_VIRTUAL)
                {
                    CliPrintf (CliHandle, " Virtual\r\n");
                }
                else
                {
                    CliPrintf (CliHandle, "\r\n");
                }

                /* Go to Next Interface Address */
                pShowIp6IntfAddr->u4NoOfAddr--;
                if (pShowIp6IntfAddr->u4NoOfAddr)
                {
                    pShowIp6IntfAddrAttr++;
                }
            }

            /* Check if more address is present. */
            if ((IS_ADDR_UNSPECIFIED (Ip6InterfaceCookie.Ip6Addr)) &&
                (Ip6InterfaceCookie.u1PrefixLen == 0))
            {
                break;
            }

            /* Get next Set of Address. */
            if (Ip6ShowGetIntfAddr (au1InterfaceInfo, u4BufSize,
                                    (UINT4) u4IfIndex, &Ip6InterfaceCookie)
                == IP6_FAILURE)
            {
                /* Error in retrieving the Interface information. */
                CLI_FATAL_ERROR (CliHandle);
                return;
            }
        }
    }

    /* Display the Uni-Cast Address Information */

    /* Set the Cookie Address Type as Uni-Cast Address To get the
     * Uni-Cast Address */
    MEMSET (&Ip6InterfaceCookie, 0, sizeof (tShowIp6IntfAddrCookie));
    Ip6InterfaceCookie.u1AddrType = ADDR6_UNICAST;

    if (Ip6ShowGetIntfAddr (au1InterfaceInfo, u4BufSize, (UINT4) u4IfIndex,
                            &Ip6InterfaceCookie) == IP6_FAILURE)
    {
        /* Error in retrieving the Interface information. */
        CLI_FATAL_ERROR (CliHandle);
        return;
    }

    pShowIp6IntfAddr = (tShowIp6IntfAddr *) (VOID *) au1InterfaceInfo;

    if ((pShowIp6IntfAddr->u4NoOfAddr == 0) &&
        (pShowIp6Interface->u4UnnumAssocIPv6If == 0))
    {
        CliPrintf (CliHandle, "    Global unicast address(es):\r\n"
                   "        Not Configured.\r\n");
    }
    else if ((pShowIp6IntfAddr->u4NoOfAddr == 0) &&
             (pShowIp6Interface->u4UnnumAssocIPv6If != 0))
    {
        CfaCliGetIfName (pShowIp6Interface->u4UnnumAssocIPv6If,
                         pi1UnnumAssocIfName);
        CliPrintf (CliHandle, "    Interface is unnumbered.\r\n"
                   "    Associated ipv6 interface-index is %s.\r\n",
                   pi1UnnumAssocIfName);

        MEMSET (&Ip6InterfaceCookie, 0, sizeof (tShowIp6IntfAddrCookie));
        Ip6InterfaceCookie.u1AddrType = ADDR6_UNICAST;

        if (Ip6ShowGetIntfAddr (au1AssocIntfInfo, u4BufSize,
                                (UINT4) pShowIp6Interface->u4UnnumAssocIPv6If,
                                &Ip6InterfaceCookie) == IP6_FAILURE)
        {
            /* Error in retrieving the Interface information. */
            CLI_FATAL_ERROR (CliHandle);
            return;
        }
        pShowIp6AssocIntfAddr = (tShowIp6IntfAddr *) (VOID *) au1AssocIntfInfo;
        if (pShowIp6AssocIntfAddr->u4NoOfAddr == 0)
        {
            CliPrintf (CliHandle, "    Global unicast address is"
                       " not Configured for Associated interface.\r\n");
        }

#ifdef LNXIP6_WANTED
        Ip6ShowGetInterface (pShowIp6AssocInterface,
                             (INT4) pShowIp6Interface->u4UnnumAssocIPv6If);
#endif

    }
    if ((pShowIp6IntfAddr->u4NoOfAddr != 0) ||
        ((pShowIp6Interface->u4UnnumAssocIPv6If != 0) &&
         (pShowIp6AssocIntfAddr != NULL) &&
         (pShowIp6AssocIntfAddr->u4NoOfAddr != 0)))
    {
        if (pShowIp6Interface->u4UnnumAssocIPv6If == 0)
        {
            CliPrintf (CliHandle, "    Global unicast address(es):\r\n");
        }
        else
        {
            CliPrintf (CliHandle, "    Global unicast address(es) "
                       "of Associated interface:\r\n");
        }
        for (;;)
        {
            if (pShowIp6Interface->u4UnnumAssocIPv6If == 0)
            {
                pShowIp6IntfAddrAttr = &pShowIp6IntfAddr->aIntfAddrAttr[0];
                u4NoOfAddr = pShowIp6IntfAddr->u4NoOfAddr;
            }
            else
            {
                if (pShowIp6AssocIntfAddr == NULL)
                {
                    break;
                }
                pShowIp6IntfAddrAttr = &pShowIp6AssocIntfAddr->aIntfAddrAttr[0];
                u4NoOfAddr = pShowIp6AssocIntfAddr->u4NoOfAddr;
            }
            while (u4NoOfAddr != 0)
            {
                /* Display the Uni-Cast Address */

                if (pShowIp6IntfAddrAttr->u1Status & ADDR6_FAILED)
                {
                    CliPrintf (CliHandle,
                               "        %s/%d    [Duplicate] [Scope:%s]",
                               Ip6PrintNtop ((tIp6Addr *) &
                                             pShowIp6IntfAddrAttr->Ip6Addr),
                               pShowIp6IntfAddrAttr->u1PrefixLen,
                               pu1GlobalZone);

                }
#ifndef LNXIP6_WANTED
                else if (pShowIp6IntfAddrAttr->u1Status & ADDR6_DOWN)
#else
                else if ((pShowIp6IntfAddrAttr->u1Status & ADDR6_DOWN) ||
                         ((pShowIp6Interface->u4UnnumAssocIPv6If == 0) &&
                          (pShowIp6Interface->i4OperStatus ==
                           NETIPV6_OPER_DOWN))
                         || ((pShowIp6Interface->u4UnnumAssocIPv6If != 0)
                             && (pShowIp6AssocInterface != NULL)
                             && (pShowIp6AssocInterface->i4OperStatus ==
                                 NETIPV6_OPER_DOWN)))
#endif
                {
                    CliPrintf (CliHandle,
                               "        %s/%d    [Down]  [Scope:%s]",
                               Ip6PrintNtop
                               ((tIp6Addr *) & pShowIp6IntfAddrAttr->
                                Ip6Addr), pShowIp6IntfAddrAttr->u1PrefixLen,
                               pu1GlobalZone);
                }
                else if (pShowIp6IntfAddrAttr->u1Status & ADDR6_TENTATIVE)
                {
                    CliPrintf (CliHandle,
                               "        %s/%d    [Tentative] [Scope:%s]",
                               Ip6PrintNtop ((tIp6Addr *) &
                                             pShowIp6IntfAddrAttr->Ip6Addr),
                               pShowIp6IntfAddrAttr->u1PrefixLen,
                               pu1GlobalZone);

                }
                else if ((pShowIp6IntfAddrAttr->
                          u1Status & ADDR6_PREFERRED) == 0)
                {
                    /* Address is not Preferred */
                    CliPrintf (CliHandle,
                               "        %s/%d    [Deprecated] [Scope:%s]",
                               Ip6PrintNtop ((tIp6Addr *) &
                                             pShowIp6IntfAddrAttr->Ip6Addr),
                               pShowIp6IntfAddrAttr->u1PrefixLen,
                               pu1GlobalZone);
                }
                else
                {
                    CliPrintf (CliHandle, "        %s/%d [Scope:%s]",
                               Ip6PrintNtop
                               ((tIp6Addr *) & pShowIp6IntfAddrAttr->
                                Ip6Addr), pShowIp6IntfAddrAttr->u1PrefixLen,
                               pu1GlobalZone);
                }

                if (pShowIp6IntfAddrAttr->u1CfgMethod == IP6_ADDR_VIRTUAL)
                {
                    CliPrintf (CliHandle, " Virtual\r\n");
                }
                else
                {
                    CliPrintf (CliHandle, "\r\n");
                }

                /* Go to Next Interface Address */
                if (pShowIp6Interface->u4UnnumAssocIPv6If == 0)
                {
                    pShowIp6IntfAddr->u4NoOfAddr--;
                    u4NoOfAddr--;
                    if (pShowIp6IntfAddr->u4NoOfAddr)
                    {
                        pShowIp6IntfAddrAttr++;
                    }
                }
                else
                {
                    pShowIp6AssocIntfAddr->u4NoOfAddr--;
                    u4NoOfAddr--;
                    if (pShowIp6AssocIntfAddr->u4NoOfAddr)
                    {
                        pShowIp6IntfAddrAttr++;
                    }
                }
            }

            /* Check if more address is present. */
            if ((IS_ADDR_UNSPECIFIED (Ip6InterfaceCookie.Ip6Addr)) &&
                (Ip6InterfaceCookie.u1PrefixLen == 0))
            {
                break;
            }

            /* Get next Set of Address. */
            if (pShowIp6Interface->u4UnnumAssocIPv6If == 0)
            {
                if (Ip6ShowGetIntfAddr (au1InterfaceInfo, u4BufSize,
                                        (UINT4) u4IfIndex, &Ip6InterfaceCookie)
                    == IP6_FAILURE)
                {
                    /* Error in retrieving the Interface information. */
                    CLI_FATAL_ERROR (CliHandle);
                    return;
                }
            }
            else
            {
                if (Ip6ShowGetIntfAddr (au1AssocIntfInfo, u4BufSize,
                                        (UINT4) pShowIp6Interface->
                                        u4UnnumAssocIPv6If,
                                        &Ip6InterfaceCookie) == IP6_FAILURE)
                {
                    /* Error in retrieving the Interface information. */
                    CLI_FATAL_ERROR (CliHandle);
                    return;
                }
            }
        }
    }

    Ip6InterfaceCookie.u1AddrType = ADDR6_ANYCAST;

    if (Ip6ShowGetIntfAddr (au1InterfaceInfo, u4BufSize,
                            (UINT4) u4IfIndex, &Ip6InterfaceCookie)
        == IP6_FAILURE)
    {
        /* Error in retrieving the Interface information. */
        CLI_FATAL_ERROR (CliHandle);
        return;
    }

    pShowIp6IntfAddr = (tShowIp6IntfAddr *) (VOID *) au1InterfaceInfo;

    if (pShowIp6IntfAddr->u4NoOfAddr != 0)
    {
        for (;;)
        {
            pShowIp6IntfAddrAttr = &pShowIp6IntfAddr->aIntfAddrAttr[0];

            while (pShowIp6IntfAddr->u4NoOfAddr != 0)
            {
                /* Display the Anycast Address */
                CliPrintf (CliHandle, "        %s/%d",
                           Ip6PrintNtop
                           ((tIp6Addr *) & pShowIp6IntfAddrAttr->
                            Ip6Addr), pShowIp6IntfAddrAttr->u1PrefixLen);

                CliPrintf (CliHandle, "[Scope:%s]  [ANY]\r\n", pu1GlobalZone);

                /* Go to Next Interface Address */
                pShowIp6IntfAddr->u4NoOfAddr--;
                if (pShowIp6IntfAddr->u4NoOfAddr)
                {
                    pShowIp6IntfAddrAttr++;
                }
            }

            /* Check if more address is present. */
            if ((IS_ADDR_UNSPECIFIED (Ip6InterfaceCookie.Ip6Addr)) &&
                (Ip6InterfaceCookie.u1PrefixLen == 0))
            {
                break;
            }

            /* Get next Set of Address. */
            if (Ip6ShowGetIntfAddr (au1InterfaceInfo, u4BufSize,
                                    (UINT4) u4IfIndex, &Ip6InterfaceCookie)
                == IP6_FAILURE)
            {
                /* Error in retrieving the Interface information. */
                CLI_FATAL_ERROR (CliHandle);
                return;
            }
        }
    }
    /* Display the Multi-Cast Address Information */
    CliPrintf (CliHandle, "    Joined group address(es):\r\n");

    /* Set the Cookie Address Type as MULTI-Cast Address To get the
     * MULTI-Cast Address */
    MEMSET (&Ip6InterfaceCookie, 0, sizeof (tShowIp6IntfAddrCookie));
    Ip6InterfaceCookie.u1AddrType = ADDR6_MULTI;

    if (Ip6ShowGetIntfAddr (au1InterfaceInfo, u4BufSize, (UINT4) u4IfIndex,
                            &Ip6InterfaceCookie) == IP6_FAILURE)
    {
        /* Error in retrieving the Interface information. */
        CLI_FATAL_ERROR (CliHandle);
        return;
    }

    pShowIp6IntfAddr = (tShowIp6IntfAddr *) (VOID *) au1InterfaceInfo;
    if (pShowIp6IntfAddr->u4NoOfAddr == 0)
    {
        CliPrintf (CliHandle, "        Not Configured.\r\n");
    }
    else
    {
        for (;;)
        {
            pShowIp6IntfAddrAttr = &pShowIp6IntfAddr->aIntfAddrAttr[0];

            while (pShowIp6IntfAddr->u4NoOfAddr != 0)
            {
                /* Display the Multi-Cast Address */
#ifdef LNXIP6_WANTED
                CliPrintf (CliHandle, "        %s [Scope: Multicast]\r\n",
                           Ip6PrintNtop
                           ((tIp6Addr *) & pShowIp6IntfAddrAttr->Ip6Addr));
#else
                CliPrintf (CliHandle, "        %s Scope:[Multicast %s]\r\n",
                           Ip6PrintNtop
                           ((tIp6Addr *) & pShowIp6IntfAddrAttr->Ip6Addr),
                           gIp6GblInfo.asIp6ScopeName[pShowIp6IntfAddrAttr->
                                                      u1Scope].au1ZoneName);
#endif
                /* Go to Next Interface Address */
                pShowIp6IntfAddr->u4NoOfAddr--;
                if (pShowIp6IntfAddr->u4NoOfAddr)
                {
                    pShowIp6IntfAddrAttr++;
                }
            }

            /* Check if more address is present. */
            if ((IS_ADDR_UNSPECIFIED (Ip6InterfaceCookie.Ip6Addr)) &&
                (Ip6InterfaceCookie.u1PrefixLen == 0))
            {
                break;
            }

            /* Get next Set of Address. */
            if (Ip6ShowGetIntfAddr (au1InterfaceInfo, u4BufSize,
                                    (UINT4) u4IfIndex, &Ip6InterfaceCookie)
                == IP6_FAILURE)
            {
                /* Error in retrieving the Interface information. */
                CLI_FATAL_ERROR (CliHandle);
                return;
            }
        }
    }

    /*Default ipv6 nd prefix information */
#ifdef LNXIP6_WANTED
    Fsipv6IntDefPrefixProfileTableInfo (CliHandle);
#endif /* LNXIP6_WANTED */
    i1RetVal = nmhGetFirstIndexFsipv6PrefixTable (&i4Index, &tAddress,
                                                  &i4PrefLen);
    while (i1RetVal != SNMP_FAILURE)
    {
        if (i4Index > (INT4) u4IfIndex)
        {
            break;
        }
        else if (i4Index == (INT4) u4IfIndex)
        {
            Fsipv6IntPrefixProfileTableInfo (CliHandle, i4Index, &tAddress,
                                             i4PrefLen);
        }
        i4PrevIndex = i4Index;
        tPrevAddress = tAddress;
        i4PrevPrefLen = i4PrefLen;

        i1RetVal = nmhGetNextIndexFsipv6PrefixTable (i4PrevIndex,
                                                     &i4Index,
                                                     &tPrevAddress,
                                                     &tAddress,
                                                     i4PrevPrefLen, &i4PrefLen);
    }
    CliPrintf (CliHandle, "    MTU is %d\r\n", pShowIp6Interface->u4Mtu);

    CliPrintf (CliHandle,
               "    ND DAD is enabled, Number of DAD attempts: %d\r\n",
               pShowIp6Interface->i4DadRetries);

    if (pShowIp6Interface->i4Icmp6DstUnReachable ==
        ICMP6_DEST_UNREACHABLE_ENABLE)
    {
        CliPrintf (CliHandle,
                   "    Destination Unreachable error messages enabled\r\n");
    }
    else
    {
        CliPrintf (CliHandle,
                   "    Destination Unreachable error messages Disabled\r\n");
    }

    if (pShowIp6Interface->i4Icmp6ErrInterval > ICMP6_RATE_LIMIT_DISABLE)
    {
        CliPrintf (CliHandle, "    ICMPv6 Error Rate Limiting Enabled\r\n");
        CliPrintf (CliHandle, "    ICMPv6 Error Rate-Limit Interval: %d\r\n",
                   pShowIp6Interface->i4Icmp6ErrInterval);
        CliPrintf (CliHandle, "    ICMPv6 Error Rate-Limit Bucket Size: %d\r\n",
                   pShowIp6Interface->i4Icmp6BucketSize);
    }
    else
    {
        CliPrintf (CliHandle, "    ICMPv6 Error Rate Limiting Disabled\r\n");
    }

    if (pShowIp6Interface->i4Icmp6RedirectMsg == ICMP6_REDIRECT_ENABLE)
    {
        CliPrintf (CliHandle, "    ICMPv6 Redirects Enabled\r\n");
    }
    else
    {
        CliPrintf (CliHandle, "    ICMPv6 Redirects Disabled\r\n");
    }

    if (pShowIp6Interface->i4RouterAdvtStatus == IP6_IF_ROUT_ADV_ENABLED)
    {
        CliPrintf (CliHandle, "    ND router advertisement is enabled\r\n");

    }
    else
    {
        CliPrintf (CliHandle, "    ND router advertisement is disabled\r\n");
    }
#ifdef LNXIP6_WANTED
    if (pShowIp6Interface->i4RouteAdvRDNSS == IP6_IF_ROUT_ADV_RDNSS)
    {
        CliPrintf (CliHandle, "    RDNSS is enabled\r\n");
        CliPrintf (CliHandle, "    RDNSS Servers Configured :\r\n");
        IP6_TASK_UNLOCK ();
        i1RetVal = nmhGetFsipv6IfRaRDNSSAddrOne ((INT4) u4IfIndex, (&taddress));
        if ((i1RetVal == SNMP_SUCCESS) && (taddress.pu1_OctetList[0] != '\0'))
        {
            CliPrintf (CliHandle, "    Server One  %s\r\n",
                       Ip6PrintNtop
                       ((tIp6Addr *) (VOID *) taddress.pu1_OctetList));

            i1RetVal =
                nmhGetFsipv6IfRaRDNSSAddrOneLife ((INT4) u4IfIndex,
                                                  &u4RAObjVal);
            if (i1RetVal == SNMP_SUCCESS)

            {
                CliPrintf (CliHandle, "    Lifetime %u\r\n", u4RAObjVal);
            }
        }
        i1RetVal = nmhGetFsipv6IfRaRDNSSAddrTwo ((INT4) u4IfIndex, (&taddress));
        if ((i1RetVal == SNMP_SUCCESS) && (taddress.pu1_OctetList[0] != '\0'))
        {
            CliPrintf (CliHandle, "    Server Two  %s\r\n",
                       Ip6PrintNtop
                       ((tIp6Addr *) (VOID *) taddress.pu1_OctetList));
            i1RetVal =
                nmhGetFsipv6IfRaRDNSSAddrTwoLife ((INT4) u4IfIndex,
                                                  &u4RAObjVal);
            if (i1RetVal == SNMP_SUCCESS)
            {
                CliPrintf (CliHandle, "    Lifetime %u\r\n", u4RAObjVal);
            }
        }
        i1RetVal =
            nmhGetFsipv6IfRaRDNSSAddrThree ((INT4) u4IfIndex, (&taddress));
        if ((i1RetVal == SNMP_SUCCESS) && (taddress.pu1_OctetList[0] != '\0'))
        {
            CliPrintf (CliHandle, "    Server Three  %s\r\n",
                       Ip6PrintNtop
                       ((tIp6Addr *) (VOID *) taddress.pu1_OctetList));
            i1RetVal =
                nmhGetFsipv6IfRaRDNSSAddrThreeLife ((INT4) u4IfIndex,
                                                    &u4RAObjVal);
            if (i1RetVal == SNMP_SUCCESS)
            {
                CliPrintf (CliHandle, "    Lifetime %u\r\n", u4RAObjVal);
            }

        }
        IP6_TASK_LOCK ();
        CliPrintf (CliHandle, "\n");
    }
    else
    {
        CliPrintf (CliHandle, "    RDNSS is disabled\r\n");
    }

    if (pShowIp6Interface->i4RouteAdvRDNSSStatus ==
        IP6_IF_ROUT_ADV_RDNSS_ENABLED)
    {
        CliPrintf (CliHandle, "    RDNSS service flag is set open\r\n");
    }
    else
    {
        CliPrintf (CliHandle, "    RDNSS service flag is not set as open\r\n");
    }
    CliPrintf (CliHandle,
               "    RDNSS Preference value  %d \r\n",
               pShowIp6Interface->u4RDNSSPreference);

    CliPrintf (CliHandle,
               "    RDNSS Lifetime  value  %d \r\n",
               pShowIp6Interface->u4RDNSSLifetime);
#endif /* LNXIP6_WANTED */
    CliPrintf (CliHandle,
               "    ND reachable time is %d milliseconds\r\n",
               pShowIp6Interface->i4RouterReachTime);

#ifdef LNXIP6_WANTED
    if (i1Val == CLI_SUCCESS)
    {
        CliPrintf (CliHandle, "    DNS is enabled\r\n");
        CliPrintf (CliHandle, "    DNS Servers Configured :\r\n");
        CliPrintf (CliHandle, "    Server One  %s\r\n",
                   pShowIp6Interface->au1DomainNameOne);
        i1RetVal =
            nmhGetFsipv6IfDomainNameOneLife ((INT4) u4IfIndex,
                                             (INT4 *) &u4RAObjVal);
        if (i1RetVal == SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "    Lifetime %u\r\n", u4RAObjVal);
        }
        CliPrintf (CliHandle, "    Server Two  %s\r\n",
                   pShowIp6Interface->au1DomainNameTwo);
        i1RetVal =
            nmhGetFsipv6IfDomainNameTwoLife ((INT4) u4IfIndex,
                                             (INT4 *) &u4RAObjVal);
        if (i1RetVal == SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "    Lifetime %u\r\n", u4RAObjVal);
        }
        CliPrintf (CliHandle, "    Server Three  %s\r\n",
                   pShowIp6Interface->au1DomainNameThree);
        i1RetVal =
            nmhGetFsipv6IfDomainNameThreeLife ((INT4) u4IfIndex,
                                               (INT4 *) &u4RAObjVal);
        if (i1RetVal == SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "    Lifetime %u\r\n", u4RAObjVal);
        }

    }
    else
    {
        CliPrintf (CliHandle, "    DNS is disabled\r\n");
    }
#else
    UNUSED_PARAM (i1Val);
#endif

    CliPrintf (CliHandle,
               "    ND retransmit time is %d milliseconds\r\n",
               pShowIp6Interface->i4RouterRetransTime);

    CliPrintf (CliHandle,
               "    ND router advertisements minimum value %d seconds\r\n",
               pShowIp6Interface->i4RouterMinAdvtTime);

    CliPrintf (CliHandle,
               "    ND router advertisements maximum value %d seconds\r\n",
               pShowIp6Interface->i4RouterMaxAdvtTime);
    CliPrintf (CliHandle,
               "    ND router advertisement Life-time: %d seconds\r\n",
               pShowIp6Interface->i4RouterLifeTime);

    CliPrintf (CliHandle,
               "    ND router advertisement Link MTU %d \r\n",
               pShowIp6Interface->u4RaLinkMTU);

    CliPrintf (CliHandle,
               "    ND router advertisement hop-limit %d \r\n",
               pShowIp6Interface->u4RACurHopLimit);

    CliPrintf (CliHandle, "    ND router advertisement Flag:\r\n");
    if (pShowIp6Interface->i4RouterAdvtOConfigFlag == IPVX_TRUTH_VALUE_TRUE)
    {
        CliPrintf (CliHandle, "       Other-Stateful Flag: Enabled \r\n");
    }
    else
    {
        CliPrintf (CliHandle, "       Other-Stateful Flag: Disabled\r\n");
    }

    if (pShowIp6Interface->i4RouterAdvtManagedFlag == IPVX_TRUTH_VALUE_TRUE)
    {
        CliPrintf (CliHandle, "       Managed Address Flag: Enabled \r\n");
    }
    else
    {
        CliPrintf (CliHandle, "       Managed Address Flag: Disabled\r\n");
    }
    if ((UINT4) pShowIp6Interface->i4RALinkLocalStatus == IP6_RA_ADV_LINKLOCAL)
    {
        CliPrintf (CliHandle,
                   "    IPv6 Advertisement Link Local Address enabled\r\n");
    }
    if ((UINT4) pShowIp6Interface->i4RAInterval == IP6_RA_ADV_INTERVAL)
    {
        CliPrintf (CliHandle, "    IPv6 advt-Interval option enabled\r\n");
    }

    nmhGetFsipv6IfNDProxyAdminStatus ((INT4) u4IfIndex, &i4ProxyAdStat);

    if (i4ProxyAdStat == ND6_IF_PROXY_ADMIN_UP)
    {
        CliPrintf (CliHandle, "    ND Proxy Admin Status: Enabled \r\n");

        nmhGetFsipv6IfNDProxyMode ((INT4) u4IfIndex, &i4ProxyMode);

        if (i4ProxyMode == ND6_PROXY_MODE_LOCAL)
        {
            CliPrintf (CliHandle, "    ND Proxy Mode : Local \r\n");
        }
        else
        {
            CliPrintf (CliHandle, "    ND Proxy Mode : Global \r\n");
        }

        nmhGetFsipv6IfNDProxyUpStream ((INT4) u4IfIndex, &i4ProxyUpstream);

        if (i4ProxyUpstream == ND6_PROXY_IF_UPSTREAM)
        {
            CliPrintf (CliHandle, "    ND Proxy Upstream : Enabled \r\n");
        }
        else
        {
            CliPrintf (CliHandle, "    ND Proxy Upstream : Disabled \r\n");
        }

        nmhGetFsipv6IfNDProxyOperStatus ((INT4) u4IfIndex, &i4ProxyOperStat);

        if (i4ProxyOperStat == ND6_PROXY_OPER_UP)
        {
            CliPrintf (CliHandle,
                       "    ND Proxy Operational Status : Enabled \r\n");
        }
        else if (i4ProxyOperStat == ND6_PROXY_OPER_UP_IN_PROG)
        {
            CliPrintf (CliHandle,
                       "    ND Proxy Operational Status: In Progress \r\n");
        }
        else
        {
            CliPrintf (CliHandle,
                       "    ND Proxy Operational Status: Disabled \r\n");
        }
    }
    else
    {
        CliPrintf (CliHandle, "    ND Proxy Admin Status: Disabled \r\n");
    }
    nmhGetFsipv6IfSENDSecStatus ((INT4) u4IfIndex, &i4SeNDStatus);
    if (i4SeNDStatus == ND6_SECURE_ENABLE)
    {
        CliPrintf (CliHandle, "    Secure ND Status: Enabled \r\n");
    }
    else if (i4SeNDStatus == ND6_SECURE_MIXED)
    {
        CliPrintf (CliHandle, "    Secure ND Status: Mixed\r\n");
    }
    else
    {
        CliPrintf (CliHandle, "    Secure ND Status: Disabled\r\n");
    }
    nmhGetFsipv6IfDefRoutePreference ((INT4) u4IfIndex, &i4DefRoutePref);
    if (i4DefRoutePref == IP6_RA_ROUTE_PREF_LOW)
    {
        CliPrintf (CliHandle, "    Default Router Preference: Low\r\n");
    }
    if (i4DefRoutePref == IP6_RA_ROUTE_PREF_MED)
    {
        CliPrintf (CliHandle, "    Default Router Preference: Medium\r\n");
    }
    if (i4DefRoutePref == IP6_RA_ROUTE_PREF_HIGH)
    {
        CliPrintf (CliHandle, "    Default Router Preference: High\r\n");
    }
    return;
}

/*********************************************************************
 *** *   Function Name : IssIp6ShowUtilTrace
 *** *   Description   : Displays current IP6 Trace level configurations
 *** *   Input(s)      : CliHandle - Cli Context ID.
 *** *   Output(s)     : None.
 *** *   Return Values : None.
 *********************************************************************/

VOID
IssIp6ShowUtilTrace (tCliHandle CliHandle, UINT4 u4TraceVal)
{
    if ((u4TraceVal & IP6_MOD_TRC) == IP6_MOD_TRC)
    {
        CliPrintf (CliHandle, "IP6\n\r");
    }
    if ((u4TraceVal & ICMP6_MOD_TRC) == ICMP6_MOD_TRC)
    {
        CliPrintf (CliHandle, "ICMP\n\r");
    }
    if ((u4TraceVal & UDP6_MOD_TRC) == UDP6_MOD_TRC)
    {
        CliPrintf (CliHandle, "UDP6\n\r");
    }
    if ((u4TraceVal & ND6_MOD_TRC) == ND6_MOD_TRC)
    {
        CliPrintf (CliHandle, "ND\n\r");
    }
    if ((u4TraceVal & PING6_MOD_TRC) == PING6_MOD_TRC)
    {
        CliPrintf (CliHandle, "PING6\n\r");
    }
    if ((u4TraceVal & V6_OVER_V4_TUNL) == V6_OVER_V4_TUNL)
    {
        CliPrintf (CliHandle, "TUNNEL\n\r");
    }
    if ((u4TraceVal & MIP6_MOD_TRC) == MIP6_MOD_TRC)
    {
        CliPrintf (CliHandle, "MIP6\n\r");
    }
    return;
}

/*********************************************************************
 *** *   Function Name : IssIp6ShowDebugging
 *** *   Description   : Displays current IP6 Trace level configurations
 *** *   Input(s)      : CliHandle - Cli Context ID.
 *** *   Output(s)     : None.
 *** *   Return Values : None.
 **********************************************************************/

VOID
IssIp6ShowDebugging (tCliHandle CliHandle)
{
    UINT4               u4TraceVal = 0;
    UINT4               u4L3ContextId = 0;
    UINT4               u4NextL3ContextId = 0;
    INT4                i4RetVal = VCM_FAILURE;
    UINT1               au1ContextName[VCMALIAS_MAX_ARRAY_LEN];

    CliPrintf (CliHandle, "end\r\n");
    nmhGetFsMIIpv6GlobalDebug (&u4TraceVal);

    if (u4TraceVal != 0)
    {
        CliPrintf (CliHandle, "debug ipv6 ");
        IssIp6ShowUtilTrace (CliHandle, u4TraceVal);
    }
    i4RetVal = VcmGetFirstActiveL3Context (&u4L3ContextId);
    while (i4RetVal == VCM_SUCCESS)
    {
        nmhGetFsMIIpv6ContextDebug ((INT4) u4L3ContextId, &u4TraceVal);
        if (u4TraceVal != 0)
        {
            VcmGetAliasName (u4L3ContextId, au1ContextName);
            CliPrintf (CliHandle, "debug ipv6 vrf %s ", au1ContextName);
            IssIp6ShowUtilTrace (CliHandle, u4TraceVal);
        }
        i4RetVal = VcmGetNextActiveL3Context (u4L3ContextId,
                                              &u4NextL3ContextId);
        u4L3ContextId = u4NextL3ContextId;
    }
    CliPrintf (CliHandle, "!\r\n");
    return;
}

/*****************************************************************************/
/*     FUNCTION NAME    : Fsipv6IntPrefixProfileTableInfo                    */
/*                                                                           */
/*     DESCRIPTION      : This function displays fsipv6PrefixTable           */
/*                        and fsipv6AddrProfileTable                         */
/*                        for show ipv6 interface.                           */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : VOID                                               */
/*****************************************************************************/
PRIVATE VOID
Fsipv6IntPrefixProfileTableInfo (tCliHandle CliHandle, INT4 i4PrefixIndex,
                                 tSNMP_OCTET_STRING_TYPE * pPrefixAddrs,
                                 INT4 i4PrefixAddrLen)
{
    INT4                i4TableObject = 0;
    INT4                i4ProfIndex = 0;
    INT4                i4TimeFlag = 0;
    UINT4               u4Time = 0;

    /*fsipv6PrefixTable Parameters */

    /*Ipv6 prefix admin status */
    nmhGetFsipv6PrefixAdminStatus (i4PrefixIndex,
                                   pPrefixAddrs,
                                   i4PrefixAddrLen, &i4TableObject);
    if (i4TableObject != ACTIVE)

        return;

    /*Prefix Profile Index is the index to the fsipv6AddrProfileTable */
    nmhGetFsipv6PrefixProfileIndex (i4PrefixIndex,
                                    pPrefixAddrs,
                                    i4PrefixAddrLen, &i4ProfIndex);

    /*fsipv6AddrProfileTable Parameters */

    /*Addr Profile Status */
    nmhGetFsipv6AddrProfileStatus ((UINT4) i4ProfIndex, &i4TableObject);
    if (i4TableObject != IP6_ADDR_PROF_VALID)

        return;

    /*Prefix address and Length */
    /* NOTE : If the prefix is set to default it cannot be printed because 
     * though the command is interface specific it is effective globally.
     * This behaviour is different from that of CISCO*/
    CliPrintf (CliHandle, "    ipv6 nd prefix %s %d \n",
               Ip6PrintNtop ((tIp6Addr *) (VOID *) (pPrefixAddrs->
                                                    pu1_OctetList)),
               i4PrefixAddrLen);

    /*Addr Profile Prefix Adv Status */
    /*There is a mismatch between the default values in the 
     *mib and the code*/
    nmhGetFsipv6AddrProfilePrefixAdvStatus ((UINT4) i4ProfIndex,
                                            &i4TableObject);
    if (i4TableObject == IP6_ADDR_PROF_PREF_ADV_OFF)
    {
        CliPrintf (CliHandle, "                   no-advertise\n");
    }
    else
    {
        /*Addr Profile Valid Time */
        nmhGetFsipv6AddrProfileValidTime ((UINT4) i4ProfIndex, &u4Time);
        /*Addr Profile Valid Time Flag */
        nmhGetFsipv6AddrProfileValidLifeTimeFlag ((UINT4) i4ProfIndex,
                                                  &i4TimeFlag);
        if (i4TimeFlag == IP6_VARIABLE_TIME)
        {
            CliPrintf (CliHandle,
                       "        Addr Profile Valid Life Time Flag : Variable\r\n");
            CliPrintf (CliHandle,
                       "        Addr Profile Valid Time : %d\r\n", u4Time);
        }
        else if (u4Time == IP6_ADDR_PROF_MAX_VALID_LIFE)
        {
            CliPrintf (CliHandle,
                       "        Addr Profile Valid Life Time Flag : Fixed\r\n");
            CliPrintf (CliHandle,
                       "        Addr Profile Valid Time : infinite\r\n");
        }
        else
        {
            CliPrintf (CliHandle,
                       "        Addr Profile Valid Life Time Flag : Fixed\r\n");
            CliPrintf (CliHandle,
                       "        Addr Profile Valid Time : %d\r\n", u4Time);
        }

        /*Addr Profile Prefered Time */
        nmhGetFsipv6AddrProfilePreferredTime ((UINT4) i4ProfIndex, &u4Time);
        /*Addr Profile Prefered Time Flag */
        nmhGetFsipv6AddrProfilePreferredLifeTimeFlag ((UINT4) i4ProfIndex,
                                                      &i4TimeFlag);

        if (i4TimeFlag == IP6_VARIABLE_TIME)
        {
            CliPrintf (CliHandle,
                       "        Addr Profile Prefered Life Time Flag : Variable\r\n");
            CliPrintf (CliHandle,
                       "        Addr Profile Prefered Time : %d\r\n", u4Time);
        }
        else if (u4Time == IP6_ADDR_PROF_MAX_PREF_LIFE)
        {
            CliPrintf (CliHandle,
                       "        Addr Profile Prefered Life Time Flag : Fixed\r\n");
            CliPrintf (CliHandle,
                       "        Addr Profile Prefered Time : infinite\r\n");
        }
        else
        {
            CliPrintf (CliHandle,
                       "        Addr Profile Prefered Life Time Flag : Fixed\r\n");
            CliPrintf (CliHandle,
                       "        Addr Profile Prefered Time : %d\r\n", u4Time);
        }
    }

    /*Addr Profile On Link Adv Status */
    nmhGetFsipv6AddrProfileOnLinkAdvStatus ((UINT4) i4ProfIndex,
                                            &i4TableObject);
    if (i4TableObject == IP6_ADDR_PROF_ONLINK_ADV_OFF)
    {
        CliPrintf (CliHandle,
                   "        Addr Profile On Link Adv Status : OFF(off-link)\r\n");
    }
    else
    {
        CliPrintf (CliHandle,
                   "        Addr Profile On Link Adv Status : ON\r\n");
    }

    /*Addr Profile Auto conf Adv Status */
    /*There is a mismatch between the default values in the 
     * mib and the code*/
    nmhGetFsipv6AddrProfileAutoConfAdvStatus ((UINT4) i4ProfIndex,
                                              &i4TableObject);
    if (i4TableObject == IP6_ADDR_PROF_AUTO_ADV_OFF)
    {
        CliPrintf (CliHandle,
                   "        Addr Profile Auto conf Adv Status : OFF(no-autoconfig)\r\n");
    }
    else
    {
        CliPrintf (CliHandle,
                   "        Addr Profile Auto conf Adv Status : ON\r\n");
    }

    nmhGetFsipv6SupportEmbeddedRp (i4PrefixIndex, pPrefixAddrs, i4PrefixAddrLen,
                                   &i4TableObject);
    if (i4TableObject == IP6_SUPPORT_EMBD_RP_ENABLE)
    {
        CliPrintf (CliHandle,
                   "                   Embedded-rp support enabled\n");
    }
    else
    {
        CliPrintf (CliHandle,
                   "                   Embedded-rp support disabled\n");
    }

    CliPrintf (CliHandle, "\r\n");

    return;
}

#ifdef LNXIP6_WANTED
/*****************************************************************************/
/*     FUNCTION NAME    : Fsipv6IntDefPrefixProfileTableInfo                    */
/*                                                                           */
/*     DESCRIPTION      : This function displays default fsipv6PrefixTable           */
/*                        and default fsipv6AddrProfileTable                         */
/*                        for show ipv6 interface.                           */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : VOID                                               */
/*****************************************************************************/
PRIVATE VOID
Fsipv6IntDefPrefixProfileTableInfo (tCliHandle CliHandle)
{
    INT4                i4TableObject = 0;
    INT4                i4ProfIndex = 0;
    INT4                i4TimeFlag = 0;
    UINT4               u4Time = 0;

    if (IP6_ADDR_PROF_ADMIN (i4ProfIndex) != ACTIVE)
    {
        return;
    }

    /*fsipv6AddrProfileTable Parameters */

    /*Addr Profile Status */
    nmhGetFsipv6AddrProfileStatus ((UINT4) i4ProfIndex, &i4TableObject);
    if (i4TableObject != IP6_ADDR_PROF_VALID)

        return;

    /*Prefix address and Length */
    /* NOTE : If the prefix is set to default it cannot be printed because 
     * though the command is interface specific it is effective globally.
     * This behaviour is different from that of CISCO*/
    CliPrintf (CliHandle, "    ipv6 nd prefix default \n");

    /*Addr Profile Prefix Adv Status */
    /*There is a mismatch between the default values in the 
     *mib and the code*/
    nmhGetFsipv6AddrProfilePrefixAdvStatus ((UINT4) i4ProfIndex,
                                            &i4TableObject);
    if (i4TableObject == IP6_ADDR_PROF_PREF_ADV_OFF)
    {
        CliPrintf (CliHandle, "                   no-advertise\n");
    }
    else
    {
        /*Addr Profile Valid Time */
        nmhGetFsipv6AddrProfileValidTime (i4ProfIndex, &u4Time);
        /*Addr Profile Valid Time Flag */
        nmhGetFsipv6AddrProfileValidLifeTimeFlag (i4ProfIndex, &i4TimeFlag);
        if (i4TimeFlag == IP6_VARIABLE_TIME)
        {
            CliPrintf (CliHandle,
                       "        Addr Profile Valid Life Time Flag : Variable\r\n");
            CliPrintf (CliHandle,
                       "        Addr Profile Valid Time : %d\r\n", u4Time);
        }
        else if (u4Time == IP6_ADDR_PROF_MAX_VALID_LIFE)
        {
            CliPrintf (CliHandle,
                       "        Addr Profile Valid Life Time Flag : Fixed\r\n");
            CliPrintf (CliHandle,
                       "        Addr Profile Valid Time : infinite\r\n");
        }
        else
        {
            CliPrintf (CliHandle,
                       "        Addr Profile Valid Life Time Flag : Fixed\r\n");
            CliPrintf (CliHandle,
                       "        Addr Profile Valid Time : %d\r\n", u4Time);
        }

        /*Addr Profile Prefered Time */
        nmhGetFsipv6AddrProfilePreferredTime (i4ProfIndex, &u4Time);
        /*Addr Profile Prefered Time Flag */
        nmhGetFsipv6AddrProfilePreferredLifeTimeFlag ((UINT4) i4ProfIndex,
                                                      &i4TimeFlag);

        if (i4TimeFlag == IP6_VARIABLE_TIME)
        {
            CliPrintf (CliHandle,
                       "        Addr Profile Prefered Life Time Flag : Variable\r\n");
            CliPrintf (CliHandle,
                       "        Addr Profile Prefered Time : %d\r\n", u4Time);
        }
        else if (u4Time == IP6_ADDR_PROF_MAX_PREF_LIFE)
        {
            CliPrintf (CliHandle,
                       "        Addr Profile Prefered Life Time Flag : Fixed\r\n");
            CliPrintf (CliHandle,
                       "        Addr Profile Prefered Time : infinite\r\n");
        }
        else
        {
            CliPrintf (CliHandle,
                       "        Addr Profile Prefered Life Time Flag : Fixed\r\n");
            CliPrintf (CliHandle,
                       "        Addr Profile Prefered Time : %d\r\n", u4Time);
        }
    }

    /*Addr Profile On Link Adv Status */
    nmhGetFsipv6AddrProfileOnLinkAdvStatus ((UINT4) i4ProfIndex,
                                            &i4TableObject);
    if (i4TableObject == IP6_ADDR_PROF_ONLINK_ADV_OFF)
    {
        CliPrintf (CliHandle,
                   "        Addr Profile On Link Adv Status : OFF(off-link)\r\n");
    }
    else
    {
        CliPrintf (CliHandle,
                   "        Addr Profile On Link Adv Status : ON\r\n");
    }

    /*Addr Profile Auto conf Adv Status */
    /*There is a mismatch between the default values in the 
     * mib and the code*/
    nmhGetFsipv6AddrProfileAutoConfAdvStatus ((UINT4) i4ProfIndex,
                                              &i4TableObject);
    if (i4TableObject == IP6_ADDR_PROF_AUTO_ADV_OFF)
    {
        CliPrintf (CliHandle,
                   "        Addr Profile Auto conf Adv Status : OFF(no-autoconfig)\r\n");
    }
    else
    {
        CliPrintf (CliHandle,
                   "        Addr Profile Auto conf Adv Status : ON\r\n");
    }

    CliPrintf (CliHandle, "\r\n");
    return;
}
#endif /* LNXIP6_WANTED */
/*********************************************************************
*  Function Name : Ip6SetNDProxyAdminStatus
*  Description   : Enabling ND Proxy Admin Status
*  Input(s)      : CliHandle
*                  i4Index - Interface Index
*                  i4ProxyAdminStat - ENABLE/DISABLE
*  Output(s)     :
*  Return Values : None.
*********************************************************************/

INT4
Ip6SetNDProxyAdminStatus (tCliHandle CliHandle,
                          INT4 i4Index, INT4 i4ProxyAdminStat)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2Fsipv6IfNDProxyAdminStatus
        (&u4ErrorCode, i4Index, i4ProxyAdminStat) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsipv6IfNDProxyAdminStatus
        (i4Index, i4ProxyAdminStat) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\nProxy Admin Status cannot be Set");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*********************************************************************
*  Function Name : Ip6SetNDProxyMode
*  Description   : Enabling Local Proxy Mode
*  Input(s)      : CliHandle
*                  i4Index - Interface Index
*                  i4ProxyMode - ENABLE/DISABLE
*  Output(s)     :
*  Return Values : None.
*********************************************************************/

INT4
Ip6SetNDProxyMode (tCliHandle CliHandle, INT4 i4Index, INT4 i4ProxyMode)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2Fsipv6IfNDProxyMode
        (&u4ErrorCode, i4Index, i4ProxyMode) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsipv6IfNDProxyMode (i4Index, i4ProxyMode) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\nProxy Mode cannot be Set");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*********************************************************************
*  Function Name : Ip6SetNDProxyUpstream
*  Description   : Enabling Upstream or Downstream in Proxy interface
*  Input(s)      : CliHandle
*                  i4Index - Interface Index
*                  i4ProxyUpstream - ENABLE/DISABLE
*  Output(s)     :
*  Return Values : None.
*********************************************************************/

INT4
Ip6SetNDProxyUpstream (tCliHandle CliHandle, INT4 i4Index, INT4 i4ProxyUpstream)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2Fsipv6IfNDProxyUpStream
        (&u4ErrorCode, i4Index, i4ProxyUpstream) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsipv6IfNDProxyUpStream
        (i4Index, i4ProxyUpstream) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\nProxy Upstream cannot be Set");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : Nd6CliGetShowCmdOutputToFile                         */
/*                                                                           */
/* Description        : This function handles the execution of show commands */
/*                      and calculation of checksum with the output.         */
/*                      The calculated value is then being sent to RM.       */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : pu1FileName - The output of the show cmd is          */
/*                      redirected to this file                              */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/*****************************************************************************/
INT4
Nd6CliGetShowCmdOutputToFile (UINT1 *pu1FileName)
{
    if (FileStat ((const CHR1 *) pu1FileName) == OSIX_SUCCESS)
    {
        if (0 != FileDelete (pu1FileName))
        {
            return IP6_FAILURE;
        }
    }
    if (CliGetShowCmdOutputToFile (pu1FileName, (UINT1 *) ND6_AUDIT_SHOW_CMD)
        == CLI_FAILURE)
    {
        return IP6_FAILURE;
    }
    return IP6_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : Nd6CliCalcSwAudCheckSum                              */
/*                                                                           */
/* Description        : This function handles the Calculation of checksum    */
/*                      for the data in the given input file.                */
/*                                                                           */
/* Input(s)           : pu1FileName - The checksum is calculated for the data*/
/*                      in this file                                         */
/*                                                                           */
/* Output(s)          : pu2ChkSum - The calculated checksum is stored here   */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/*****************************************************************************/
INT4
Nd6CliCalcSwAudCheckSum (UINT1 *pu1FileName, UINT2 *pu2ChkSum)
{
    UINT4               u4Sum = 0;
    UINT4               u4Last = 0;
    INT4                i4Fd;
    UINT2               u2CkSum = 0;
    INT2                i2ReadLen;
    INT1                ai1Buf[ND6_CLI_MAX_GROUPS_LINE_LEN + 1];

    i4Fd = FileOpen ((CONST UINT1 *) pu1FileName, ND6_CLI_RDONLY);
    if (i4Fd == -1)
    {
        return IP6_FAILURE;
    }
    MEMSET (ai1Buf, 0, ND6_CLI_MAX_GROUPS_LINE_LEN + 1);
    while (Nd6CliReadLineFromFile (i4Fd, ai1Buf, ND6_CLI_MAX_GROUPS_LINE_LEN,
                                   &i2ReadLen) != ND6_CLI_EOF)

    {
        if (i2ReadLen > 0)
        {
            UtilCompCheckSum ((UINT1 *) ai1Buf, &u4Sum, &u4Last,
                              &u2CkSum, (UINT4) i2ReadLen, CKSUM_DATA);
            MEMSET (ai1Buf, '\0', ND6_CLI_MAX_GROUPS_LINE_LEN + 1);
        }
    }
    /*CheckSum for last line */
    if ((u4Sum != 0) || (i2ReadLen != 0))
    {
        UtilCompCheckSum ((UINT1 *) ai1Buf, &u4Sum, &u4Last,
                          &u2CkSum, (UINT4) i2ReadLen, CKSUM_LASTDATA);
    }
    *pu2ChkSum = u2CkSum;
    if (FileClose (i4Fd) < 0)
    {
        return IP6_FAILURE;
    }
    return IP6_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : Nd6CliReadLineFromFile                               */
/*                                                                           */
/* Description        : It is a utility to read a line from the given file   */
/*                      descriptor.                                          */
/*                                                                           */
/* Input(s)           : i4Fd - File Descriptor for the file                  */
/*                      i2MaxLen - Maximum length that can be read and store */
/*                                                                           */
/* Output(s)          : pi1Buf - Buffer to store the line read from the file */
/*                      pi2ReadLen - the total length of the data read       */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/*****************************************************************************/
INT1
Nd6CliReadLineFromFile (INT4 i4Fd, INT1 *pi1Buf, INT2 i2MaxLen,
                        INT2 *pi2ReadLen)
{
    INT1                i1Char;

    *pi2ReadLen = 0;

    while (FileRead (i4Fd, (CHR1 *) & i1Char, 1) == 1)
    {
        pi1Buf[*pi2ReadLen] = i1Char;
        (*pi2ReadLen)++;
        if (i1Char == '\n')
        {
            return (ND6_CLI_NO_EOF);
        }
        if (*pi2ReadLen == i2MaxLen)
        {
            *pi2ReadLen = 0;
            break;
        }
    }
    pi1Buf[*pi2ReadLen] = '\0';
    return (ND6_CLI_EOF);
}

/*********************************************************************
*  Function Name : Ip6SetSeNDSecLevel
*  Description   : This function validates and sets the sec level for 
*                  generating CGA address.
*  Input(s)      : CliHandle
*                  u4SecValue
*  Output(s)     :
*  Return Values : None.
*********************************************************************/

INT4
Ip6SetSeNDSecLevel (tCliHandle CliHandle, UINT4 u4SecValue)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2Fsipv6SENDSecLevel (&u4ErrorCode, u4SecValue) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\nInvalid Sec Value ");
        return CLI_FAILURE;
    }

    if (nmhSetFsipv6SENDSecLevel (u4SecValue) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\nSec Value cannot be Set");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*********************************************************************
*  Function Name : Ip6SetSeNDNbrSecLevel 
*  Description   : This function sets the sec level that is acceptable
*                  from neighbors.
*  Input(s)      : CliHandle
*                  u4SecValue
*  Output(s)     :
*  Return Values : None.
*********************************************************************/

INT4
Ip6SetSeNDNbrSecLevel (tCliHandle CliHandle, UINT4 u4SecValue)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2Fsipv6SENDNbrSecLevel
        (&u4ErrorCode, u4SecValue) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\nInvalid Sec Value ");
        return CLI_FAILURE;
    }

    if (nmhSetFsipv6SENDNbrSecLevel (u4SecValue) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\nSec Value for neighbor cannot be Set");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*********************************************************************
*  Function Name : Ip6SetSeNDPrefixChkRelax 
*  Description   : Enabling/Disabling prefix check relaxation for Certificates in
*                  Certificate Path Advertisment.
*  Input(s)      : CliHandle
*                  i4PrefixCheck - ENABLE/DISABLE
*  Output(s)     :
*  Return Values : None.
*********************************************************************/

INT4
Ip6SetSeNDPrefixChkRelax (tCliHandle CliHandle, INT4 i4PrefixCheck)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2Fsipv6SENDPrefixChk
        (&u4ErrorCode, i4PrefixCheck) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsipv6SENDPrefixChk (i4PrefixCheck) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\nPrefix Check Relaxation cannot be Set");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*********************************************************************
*  Function Name : Ip6SetSeNDMinKeyLength 
*  Description   : This function set the minimum length of the RSA key 
*  Input(s)      : CliHandle
*                  u4KeyLength - Key Length Value
*  Output(s)     :
*  Return Values : None.
*********************************************************************/

INT4
Ip6SetSeNDMinKeyLength (tCliHandle CliHandle, UINT4 u4KeyLength)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2Fsipv6SENDMinBits (&u4ErrorCode, u4KeyLength) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsipv6SENDMinBits (u4KeyLength) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n Minimum Key Length cannot be Set");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*********************************************************************
*  Function Name : Ip6SetSeNDStatus 
*  Description   : This function configures the Secure Neighbor 
                   Discovery for the interface.
*  Input (s)     : CliHandle 
*                  i4Index - Interface Index
*                  i4SecureMode - Secure Mode (secured/mixed/unsecure)
*  Output(s)     :
*  Return Values : None.
*********************************************************************/

INT4
Ip6SetSeNDStatus (tCliHandle CliHandle, INT4 i4Index, INT4 i4SecureMode)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2Fsipv6IfSENDSecStatus
        (&u4ErrorCode, i4Index, i4SecureMode) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsipv6IfSENDSecStatus (i4Index, i4SecureMode) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\nSeND Secure Mode cannot be Set");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*********************************************************************
*  Function Name : Ip6SetSeNDTimeStamp 
*  Description   : This function configures the Secure Neighbor
                   Discovery timestamp for the interface.
*  Input (s)     : CliHandle
*                  i4Index - Interface Index
*                  u4TimeStampValue - Value of the specified timestamp.
*                  i4TimeStampType - DELTA/FUZZ/DRIFT
*  Output(s)     :
*  Return Values : None.
*********************************************************************/

INT4
Ip6SetSeNDTimeStamp (tCliHandle CliHandle, INT4 i4Index,
                     UINT4 u4TimeStampValue, INT4 i4TimeStampType)
{
    UINT4               u4ErrorCode = 0;
    if (i4TimeStampType == ND6_TIME_DELTA)
    {
        if (nmhTestv2Fsipv6IfSENDDeltaTimestamp
            (&u4ErrorCode, i4Index, u4TimeStampValue) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsipv6IfSENDDeltaTimestamp
            (i4Index, u4TimeStampValue) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\nSeND Secure Mode cannot be Set");
            return CLI_FAILURE;
        }
    }

    if (i4TimeStampType == ND6_TIME_FUZZ)
    {
        if (nmhTestv2Fsipv6IfSENDFuzzTimestamp
            (&u4ErrorCode, i4Index, u4TimeStampValue) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsipv6IfSENDFuzzTimestamp
            (i4Index, u4TimeStampValue) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\nSeND Secure Mode cannot be Set");
            return CLI_FAILURE;
        }
    }

    if (i4TimeStampType == ND6_TIME_DRIFT)
    {
        if (nmhTestv2Fsipv6IfSENDDriftTimestamp
            (&u4ErrorCode, i4Index, u4TimeStampValue) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsipv6IfSENDDriftTimestamp
            (i4Index, u4TimeStampValue) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\nSeND Secure Mode cannot be Set");
            return CLI_FAILURE;
        }
    }
    return CLI_SUCCESS;
}

/*********************************************************************
*  Function Name : Ip6SetSeNDAcceptUnsecureAdv
*  Description   : This function enable or disable the acceptance of
*                  unsecured advertisements in Duplicate Address
*                  Detection for first tentative address.                  
*  Input (s)     : CliHandle
*                  i4AcceptStatus Enable/Disable
*  Output(s)     :
*  Return Values : None.
*********************************************************************/

INT4
Ip6SetSeNDAcceptUnsecureAdv (tCliHandle CliHandle, INT4 i4AcceptStatus)
{
    UINT4               u4ErrorCode = 0;
    if (nmhTestv2Fsipv6SENDSecDAD
        (&u4ErrorCode, i4AcceptStatus) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsipv6SENDSecDAD (i4AcceptStatus) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r\nUnsecure Advertisements acceptance cannot be set");
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*********************************************************************
*  Function Name : Ip6SetSeNDAuthType
*  Description   : This function configures the Authorisation type for
*                  Secure Neighbor Discovery
*  Input (s)     : CliHandle
*                  i4AuthType 
*  Output(s)     :
*  Return Values : None.
*********************************************************************/

INT4
Ip6SetSeNDAuthType (tCliHandle CliHandle, INT4 i4AuthType)
{
    UINT4               u4ErrorCode = 0;
    if (nmhTestv2Fsipv6SENDAuthType (&u4ErrorCode, i4AuthType) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsipv6SENDAuthType (i4AuthType) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\nSeND Authorisation cannot be set");
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*********************************************************************
 *  Function Name : Ip6IfShowNdSecureForAllVlanInCxt 
 *  Description   : Display interface configurations and Status
 *                 for all the ipv6 interface with name "pu1IfName"
 *  Input(s)      : CliHandle, IfIndex, VcId, pu1IfName
 *  Output(s)     :
 *  Return Values : None
 *********************************************************************/

VOID
Ip6IfShowNdSecureForAllVlanInCxt (tCliHandle CliHandle,
                                  UINT4 u4IfIndex, UINT4 u4VcId,
                                  UINT1 *pu1IfName, INT4 i4NdShowType)
{
    UINT4               u4NextIfIndex = 0;
    UINT4               u4IfVcId = VCM_INVALID_VC;
    UINT4               u4ShowAllCxt = FALSE;

    if (u4VcId == VCM_INVALID_VC)
    {
        u4ShowAllCxt = TRUE;
    }
    while (CfaCliGetNextIfIndexFromName (pu1IfName, u4IfIndex,
                                         &u4NextIfIndex) != OSIX_FAILURE)
    {
        u4IfIndex = u4NextIfIndex;
        VcmGetContextIdFromCfaIfIndex (u4IfIndex, &u4IfVcId);
        if (u4ShowAllCxt == TRUE)
        {
            u4VcId = u4IfVcId;
        }
        Ip6IfShowNdSecureInCxt (CliHandle, u4IfIndex, u4VcId, i4NdShowType);
        if ((u4ShowAllCxt == FALSE) && (u4VcId == u4IfVcId))
        {
            return;
        }
    }
    return;

}

/*********************************************************************
*  Function Name : Ip6IfShowNdSecureInCxt 
*  Description   : Display interface configurations and Status
*  Input(s)      : CliHandle
*                  u4IfIndex - Interface Index
*                  u4VcId  - VRF Id
*                  i4ShowType - Show type (cga/nonce/timestamp)
*  Output(s)     :
*  Return Values : None.
*********************************************************************/

VOID
Ip6IfShowNdSecureInCxt (tCliHandle CliHandle, UINT4 u4IfIndex, UINT4 u4VcId,
                        INT4 i4NdShowType)
{
    UINT4               u4L3ContextId = 0;
    UINT4               u4NextL3ContextId = 0;
    UINT4               au4Ip6Ifaces[IP6_MAX_LOGICAL_IFACES + 1];
    INT4                i4RetVal = VCM_FAILURE;
    UINT1               au1ContextName[VCMALIAS_MAX_ARRAY_LEN];
    UINT2               u2Cntr = IP6_ZERO;
    UINT1               u1DisplayHdr = TRUE;
    UINT1               u1DisplayNonceHdr = TRUE;

    if (u4IfIndex == 0)
    {
        if (u4VcId == VCM_INVALID_VC)
        {
            i4RetVal = VcmGetFirstActiveL3Context (&u4L3ContextId);
        }
        else
        {
            u4L3ContextId = u4VcId;
            i4RetVal = VCM_SUCCESS;
        }

        while (i4RetVal == VCM_SUCCESS)
        {
            if (u4VcId == VCM_INVALID_VC)
            {
                i4RetVal = VcmGetNextActiveL3Context (u4L3ContextId,
                                                      &u4NextL3ContextId);
                u1DisplayHdr = TRUE;
            }
            else
            {
                i4RetVal = VCM_FAILURE;
            }

            MEMSET (au4Ip6Ifaces, IP6_ZERO,
                    (IP6_MAX_LOGICAL_IFACES + 1) * sizeof (UINT4));
            if (VcmGetCxtIpIfaceList (u4L3ContextId, au4Ip6Ifaces)
                == VCM_FAILURE)
            {
                u4L3ContextId = u4NextL3ContextId;
                continue;
            }
            VcmGetAliasName (u4L3ContextId, au1ContextName);

            for (u2Cntr = 1; ((u2Cntr <= au4Ip6Ifaces[0]) &&
                              (u2Cntr < IP6_MAX_LOGICAL_IFACES + 1)); u2Cntr++)
            {
                /* Check for Valid Interface . */
                if (Ip6ifEntryExists (au4Ip6Ifaces[u2Cntr]) != IP6_FAILURE)
                {
                    if (u1DisplayHdr == TRUE)
                    {
                        CliPrintf (CliHandle, "\r\nVRF Id  : %d\r\n",
                                   u4L3ContextId);
                        CliPrintf (CliHandle, "VRF Name: %s\r\n",
                                   au1ContextName);
                        u1DisplayHdr = FALSE;
                    }

                    switch (i4NdShowType)
                    {
                        case IP6_SEND_SHOW_TIME:
                            if (u1DisplayNonceHdr == TRUE)
                            {
                                CliPrintf (CliHandle,
                                           " SeND TimeStamp Parameters\r\n");
                                CliPrintf (CliHandle,
                                           "    IPv6 Address                     Interface      RDLast        TSLast          Delta      Fuzz\r\n");
                                CliPrintf (CliHandle,
                                           "    ------------                     ---------      ------        ------          -----      ----\r\n");
                                u1DisplayNonceHdr = FALSE;
                            }
                            Ip6IfShowNd6SecureTimeStampForIndex (CliHandle,
                                                                 u4VcId,
                                                                 au4Ip6Ifaces
                                                                 [u2Cntr]);
                            break;

                        case IP6_SEND_SHOW_CGA:

                            Ip6IfShowNd6SecureCgaForIndex (CliHandle,
                                                           au4Ip6Ifaces
                                                           [u2Cntr]);
                            break;

                        case IP6_SEND_SHOW_NONCE:
                            if (u1DisplayNonceHdr == TRUE)
                            {
                                CliPrintf (CliHandle, " SeND Nonce Entry\r\n");
                                CliPrintf (CliHandle,
                                           "    IPv6 Address                     Interface      Received Nonce          Sent Nonce\r\n");
                                CliPrintf (CliHandle,
                                           "    ------------                     ---------      --------------          ----------\r\n");
                                u1DisplayNonceHdr = FALSE;
                            }

                            Ip6IfShowNd6SecureNonceForIndex (CliHandle, u4VcId,
                                                             au4Ip6Ifaces
                                                             [u2Cntr]);
                            break;

                        case IP6_SEND_SHOW_STAT:
                            Ip6IfShowNd6SecureStatistics (CliHandle,
                                                          au4Ip6Ifaces[u2Cntr]);
                            break;

                        default:
                            CliPrintf (CliHandle,
                                       "%%Command Not Supported\r\n");
                            break;
                    }
                }
            }
            u4L3ContextId = u4NextL3ContextId;
        }
    }
    else
    {
        /* Need to Display the detailed information for the given interface */

        if (Ip6ifEntryExists (u4IfIndex) == IP6_FAILURE)
        {
            CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
            return;
        }

        VcmGetContextIdFromCfaIfIndex (u4IfIndex, &u4L3ContextId);
        if ((u4VcId != VCM_INVALID_VC) && (u4L3ContextId != u4VcId))
        {
            /*GIven interface does not belong to given VRF */
            return;
        }

        VcmGetAliasName (u4L3ContextId, au1ContextName);

        CliPrintf (CliHandle, "VRF Id  :  %d\r\n", u4L3ContextId);
        CliPrintf (CliHandle, "VRF Name:  %s\r\n", au1ContextName);

        switch (i4NdShowType)
        {
            case IP6_SEND_SHOW_TIME:
                if (u1DisplayNonceHdr == TRUE)
                {
                    CliPrintf (CliHandle, " SeND TimeStamp Parameters\r\n");
                    CliPrintf (CliHandle,
                               "    IPv6 Address                     Interface      RDLast        TSLast          Delta      Fuzz\r\n");
                    CliPrintf (CliHandle,
                               "    ------------                     ---------      ------        ------          -----      ----\r\n");
                    u1DisplayNonceHdr = FALSE;
                }
                Ip6IfShowNd6SecureTimeStampForIndex (CliHandle, u4VcId,
                                                     u4IfIndex);
                break;

            case IP6_SEND_SHOW_CGA:
                Ip6IfShowNd6SecureCgaForIndex (CliHandle, u4IfIndex);
                break;

            case IP6_SEND_SHOW_NONCE:
                if (u1DisplayNonceHdr == TRUE)
                {
                    CliPrintf (CliHandle, " SeND Nonce Entry\r\n");
                    CliPrintf (CliHandle,
                               "    IPv6 Address                     Interface      Received Nonce          Sent Nonce\r\n");
                    CliPrintf (CliHandle,
                               "    ------------                     ---------      --------------          ----------\r\n");
                    u1DisplayNonceHdr = FALSE;
                }

                Ip6IfShowNd6SecureNonceForIndex (CliHandle, u4VcId, u4IfIndex);
                break;

            case IP6_SEND_SHOW_STAT:
                Ip6IfShowNd6SecureStatistics (CliHandle, u4IfIndex);
                break;

            default:
                CliPrintf (CliHandle, "%%Command Not Supported\r\n");
                break;
        }
    }
    return;
}

/*********************************************************************
 *  Function Name : Ip6IfShowNd6SecureTimeStampForIndex 
 *  Description   : Displays all the time stamp  related configurations
 *                  for the given interface in SeND.
 *  Input(s)      : CliHandle, u4IfIndex
 *  Output(s)     :
 *  Return Values : None
 *********************************************************************/
VOID
Ip6IfShowNd6SecureTimeStampForIndex (tCliHandle CliHandle, UINT4 u4NdCxtId,
                                     UINT4 u4IfIndex)
{
#ifndef LNXIP6_WANTED
    INT4                i4IpNetToMediaIfIndex = 1;
    INT4                i4NextIpNetToMediaIfIndex = 0;
    UINT4               u4TempCxtId = VCM_INVALID_VC;
    INT1               *pi1InterfaceName = NULL;
    INT1                ai1InterfaceName[CFA_CLI_MAX_IF_NAME_LEN];
    UINT1               au1Address[MAX_ADDR_LEN];
    UINT1               au1PhyAddress[MAX_ADDR_LEN];
    INT4                i4NextIpNetToPhysicalNetAddressType =
        INET_ADDR_TYPE_IPV6;
    UINT1               au1NetToPhysicalIp[IPVX_IPV6_ADDR_LEN];
    UINT1               au1NextNetToPhysicalIp[IPVX_IPV6_ADDR_LEN];
    INT4                i4NetToPhysicalNetAddressType = INET_ADDR_TYPE_IPV6;
    tIp6If             *pIf6 = NULL;
    INT1               *pi1IfName;
    UINT1               au1IfName[CFA_CLI_MAX_IF_NAME_LEN];
    tNd6CacheEntry     *pNd6cEntry = NULL;
    UINT4               u4NdSecureFuzzTimestamp;
    UINT4               u4NdSecureDeltaTimestamp;
    INT4                i4TSLast = 0;
    INT4                i4RDLast = 0;

    tSNMP_OCTET_STRING_TYPE OctetStrIpNetToMediaPhysAddress;
    tSNMP_OCTET_STRING_TYPE IpNetToPhysicalNetAddr;
    tSNMP_OCTET_STRING_TYPE NextIpNetToPhysicalNetAddress;

    MEMSET (au1NetToPhysicalIp, ZERO, IPVX_IPV6_ADDR_LEN);
    MEMSET (au1NextNetToPhysicalIp, ZERO, IPVX_IPV6_ADDR_LEN);
    pi1IfName = (INT1 *) &au1IfName[0];
    CfaCliGetIfName (u4IfIndex, pi1IfName);

    IpNetToPhysicalNetAddr.pu1_OctetList = au1NetToPhysicalIp;
    NextIpNetToPhysicalNetAddress.pu1_OctetList = au1NextNetToPhysicalIp;

    IpNetToPhysicalNetAddr.i4_Length = 0;
    NextIpNetToPhysicalNetAddress.i4_Length = 0;

    OctetStrIpNetToMediaPhysAddress.pu1_OctetList = &au1PhyAddress[0];

    pi1InterfaceName = &ai1InterfaceName[0];
    CfaCliGetIfName (u4IfIndex, pi1IfName);

    i4IpNetToMediaIfIndex = u4IfIndex;
    i4NetToPhysicalNetAddressType = INET_ADDR_TYPE_IPV6;
    MEMSET (au1NetToPhysicalIp, ZERO, IPVX_IPV6_ADDR_LEN);
    IpNetToPhysicalNetAddr.i4_Length = 0;

    while (nmhGetNextIndexFsMIStdIpNetToPhysicalTable
           (i4IpNetToMediaIfIndex, &i4NextIpNetToMediaIfIndex,
            i4NetToPhysicalNetAddressType,
            &i4NextIpNetToPhysicalNetAddressType,
            &IpNetToPhysicalNetAddr, &NextIpNetToPhysicalNetAddress)
           == SNMP_SUCCESS)
    {
        if (i4IpNetToMediaIfIndex != i4NextIpNetToMediaIfIndex)
        {
            break;
        }
        i4IpNetToMediaIfIndex = i4NextIpNetToMediaIfIndex;
        MEMCPY (IpNetToPhysicalNetAddr.pu1_OctetList,
                NextIpNetToPhysicalNetAddress.pu1_OctetList,
                NextIpNetToPhysicalNetAddress.i4_Length);
        i4NetToPhysicalNetAddressType = i4NextIpNetToPhysicalNetAddressType;

        if (i4NextIpNetToPhysicalNetAddressType != INET_ADDR_TYPE_IPV6)
        {
            continue;
        }

        nmhGetFsMIStdIpNetToPhysicalContextId
            (i4NextIpNetToMediaIfIndex,
             i4NextIpNetToPhysicalNetAddressType,
             &NextIpNetToPhysicalNetAddress, (INT4 *) &u4TempCxtId);

        if ((u4NdCxtId != VCM_INVALID_VC) && (u4TempCxtId != u4NdCxtId))
        {
            /*If ContextId of the current entry does not match with the
               specified one, get the next entry */
            continue;
        }

        MEMSET (au1Address, 0, MAX_ADDR_LEN);
        MEMSET (au1PhyAddress, 0, MAX_ADDR_LEN);
        MEMSET (ai1InterfaceName, 0, CFA_CLI_MAX_IF_NAME_LEN);
        nmhGetFsMIStdIpNetToPhysicalPhysAddress (i4NextIpNetToMediaIfIndex,
                                                 i4NextIpNetToPhysicalNetAddressType,
                                                 &NextIpNetToPhysicalNetAddress,
                                                 &OctetStrIpNetToMediaPhysAddress);

        pIf6 = Ip6ifGetEntry ((UINT4) i4NextIpNetToMediaIfIndex);
        if (pIf6 == NULL)
        {
            continue;
        }

        if (pIf6->u1SeNDStatus == ND6_SECURE_DISABLE)
        {
            continue;
        }

        pNd6cEntry = Nd6IsCacheForAddr (pIf6,
                                        (tIp6Addr *) (VOID *)
                                        NextIpNetToPhysicalNetAddress.
                                        pu1_OctetList);

        if (pNd6cEntry == NULL)
        {
            continue;
        }
        i4TSLast = (INT4) pNd6cEntry->u4SendTSLast;
        i4RDLast = (INT4) pNd6cEntry->u4SendRDLast;

        nmhGetFsipv6IfSENDDeltaTimestamp (u4IfIndex, &u4NdSecureDeltaTimestamp);
        nmhGetFsipv6IfSENDFuzzTimestamp (u4IfIndex, &u4NdSecureFuzzTimestamp);

        CfaCliGetIfName ((UINT4) i4NextIpNetToMediaIfIndex, pi1InterfaceName);
        CliPrintf (CliHandle, "%-35s  ",
                   Ip6PrintNtop ((tIp6Addr *) (VOID *)
                                 NextIpNetToPhysicalNetAddress.pu1_OctetList));
        CliPrintf (CliHandle, "%-15s", pi1InterfaceName);

        CliPrintf (CliHandle, "%-14d", i4RDLast);
        CliPrintf (CliHandle, "%-14d", i4TSLast);
        CliPrintf (CliHandle, "%4ds        %dms\r\n",
                   u4NdSecureDeltaTimestamp, u4NdSecureFuzzTimestamp);
    }
#else
    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (u4NdCxtId);
    UNUSED_PARAM (u4IfIndex);
#endif /* LNXIP6_WANTED */
    return;
}

/*********************************************************************
 *  Function Name : Ip6IfShowNd6SecureNonceForIndex 
 *  Description   : Displays all the nonce  related configurations
 *                  for the given interface in SeND.
 *  Input(s)      : CliHandle, u4IfIndex
 *  Output(s)     :
 *  Return Values : None
 *********************************************************************/
VOID
Ip6IfShowNd6SecureNonceForIndex (tCliHandle CliHandle, UINT4 u4NdCxtId,
                                 UINT4 u4IfIndex)
{
#ifndef LNXIP6_WANTED
    INT4                i4IpNetToMediaIfIndex = 1;
    INT4                i4NextIpNetToMediaIfIndex = 0;
    UINT4               u4TempCxtId = VCM_INVALID_VC;
    INT1               *pi1InterfaceName = NULL;
    INT1                ai1InterfaceName[CFA_CLI_MAX_IF_NAME_LEN];
    UINT1               au1Address[MAX_ADDR_LEN];
    UINT1               au1PhyAddress[MAX_ADDR_LEN];
    INT4                i4NextIpNetToPhysicalNetAddressType =
        INET_ADDR_TYPE_IPV6;
    UINT1               au1NetToPhysicalIp[IPVX_IPV6_ADDR_LEN];
    UINT1               au1NextNetToPhysicalIp[IPVX_IPV6_ADDR_LEN];
    INT4                i4NetToPhysicalNetAddressType = INET_ADDR_TYPE_IPV6;
    tIp6If             *pIf6 = NULL;
    INT1               *pi1IfName;
    UINT1               au1IfName[CFA_CLI_MAX_IF_NAME_LEN];
    tNd6CacheEntry     *pNd6cEntry = NULL;

    tSNMP_OCTET_STRING_TYPE OctetStrIpNetToMediaPhysAddress;
    tSNMP_OCTET_STRING_TYPE IpNetToPhysicalNetAddr;
    tSNMP_OCTET_STRING_TYPE NextIpNetToPhysicalNetAddress;

    MEMSET (au1NetToPhysicalIp, ZERO, IPVX_IPV6_ADDR_LEN);
    MEMSET (au1NextNetToPhysicalIp, ZERO, IPVX_IPV6_ADDR_LEN);
    pi1IfName = (INT1 *) &au1IfName[0];
    CfaCliGetIfName (u4IfIndex, pi1IfName);

    IpNetToPhysicalNetAddr.pu1_OctetList = au1NetToPhysicalIp;
    NextIpNetToPhysicalNetAddress.pu1_OctetList = au1NextNetToPhysicalIp;

    IpNetToPhysicalNetAddr.i4_Length = 0;
    NextIpNetToPhysicalNetAddress.i4_Length = 0;

    OctetStrIpNetToMediaPhysAddress.pu1_OctetList = &au1PhyAddress[0];

    pi1InterfaceName = &ai1InterfaceName[0];

    i4IpNetToMediaIfIndex = u4IfIndex;
    i4NetToPhysicalNetAddressType = INET_ADDR_TYPE_IPV6;
    MEMSET (au1NetToPhysicalIp, ZERO, IPVX_IPV6_ADDR_LEN);
    IpNetToPhysicalNetAddr.i4_Length = 0;

    while (nmhGetNextIndexFsMIStdIpNetToPhysicalTable
           (i4IpNetToMediaIfIndex, &i4NextIpNetToMediaIfIndex,
            i4NetToPhysicalNetAddressType,
            &i4NextIpNetToPhysicalNetAddressType,
            &IpNetToPhysicalNetAddr, &NextIpNetToPhysicalNetAddress)
           == SNMP_SUCCESS)
    {
        if (i4IpNetToMediaIfIndex != i4NextIpNetToMediaIfIndex)
        {
            break;
        }
        i4IpNetToMediaIfIndex = i4NextIpNetToMediaIfIndex;
        MEMCPY (IpNetToPhysicalNetAddr.pu1_OctetList,
                NextIpNetToPhysicalNetAddress.pu1_OctetList,
                NextIpNetToPhysicalNetAddress.i4_Length);
        i4NetToPhysicalNetAddressType = i4NextIpNetToPhysicalNetAddressType;

        if (i4NextIpNetToPhysicalNetAddressType != INET_ADDR_TYPE_IPV6)
        {
            continue;
        }

        nmhGetFsMIStdIpNetToPhysicalContextId
            (i4NextIpNetToMediaIfIndex,
             i4NextIpNetToPhysicalNetAddressType,
             &NextIpNetToPhysicalNetAddress, (INT4 *) &u4TempCxtId);

        if ((u4NdCxtId != VCM_INVALID_VC) && (u4TempCxtId != u4NdCxtId))
        {
            /*If ContextId of the current entry does not match with the
               specified one, get the next entry */
            continue;
        }

        MEMSET (au1Address, 0, MAX_ADDR_LEN);
        MEMSET (au1PhyAddress, 0, MAX_ADDR_LEN);
        MEMSET (ai1InterfaceName, 0, CFA_CLI_MAX_IF_NAME_LEN);
        nmhGetFsMIStdIpNetToPhysicalPhysAddress (i4NextIpNetToMediaIfIndex,
                                                 i4NextIpNetToPhysicalNetAddressType,
                                                 &NextIpNetToPhysicalNetAddress,
                                                 &OctetStrIpNetToMediaPhysAddress);

        pIf6 = Ip6ifGetEntry ((UINT4) i4NextIpNetToMediaIfIndex);
        if (pIf6 == NULL)
        {
            continue;
        }
        if (pIf6->u1SeNDStatus == ND6_SECURE_DISABLE)
        {
            continue;
        }

        pNd6cEntry = Nd6IsCacheForAddr (pIf6,
                                        (tIp6Addr *) (VOID *)
                                        NextIpNetToPhysicalNetAddress.
                                        pu1_OctetList);

        if (pNd6cEntry == NULL)
        {
            continue;
        }
        CfaCliGetIfName ((UINT4) i4NextIpNetToMediaIfIndex, pi1InterfaceName);

        CliPrintf (CliHandle, "%-35s  ",
                   Ip6PrintNtop ((tIp6Addr *) (VOID *)
                                 NextIpNetToPhysicalNetAddress.pu1_OctetList));
        CliPrintf (CliHandle, "%-15s", pi1InterfaceName);
        CliPrintf (CliHandle, "0x%02x%02x%02x%02x%02x%02x          ",
                   pNd6cEntry->au1SentNonce[0], pNd6cEntry->au1SentNonce[1],
                   pNd6cEntry->au1SentNonce[2], pNd6cEntry->au1SentNonce[3],
                   pNd6cEntry->au1SentNonce[4], pNd6cEntry->au1SentNonce[5]);
        CliPrintf (CliHandle, "0x%02x%02x%02x%02x%02x%02x\r\n",
                   pNd6cEntry->au1RcvdNonce[0], pNd6cEntry->au1RcvdNonce[1],
                   pNd6cEntry->au1RcvdNonce[2], pNd6cEntry->au1RcvdNonce[3],
                   pNd6cEntry->au1RcvdNonce[4], pNd6cEntry->au1RcvdNonce[5]);
    }
#else
    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (u4NdCxtId);
    UNUSED_PARAM (u4IfIndex);
#endif /* LNXIP6_WANTED */
    return;
}

/*********************************************************************
 *  Function Name : Ip6IfShowNd6SecureCgaForIndex 
 *  Description   : Displays all the time stamp  related configurations
 *                  for the given interface in SeND.
 *  Input(s)      : CliHandle, u4IfIndex
 *  Output(s)     :
 *  Return Values : None
 *********************************************************************/
VOID
Ip6IfShowNd6SecureCgaForIndex (tCliHandle CliHandle, UINT4 u4IfIndex)
{

    INT1                i1OutCome = 0;
    INT4                i4Index = 0;
    INT4                i4PrevIndex = 0;
    INT4                i4PrefLen = 0;
    INT4                i4PrevPrefLen = 0;
    UINT1               au1Temp[IP6_ADDR_SIZE];
    UINT1               au1Temp1[IP6_ADDR_SIZE];
    tSNMP_OCTET_STRING_TYPE tAddress;
    tSNMP_OCTET_STRING_TYPE tPrevAddress;

    tAddress.pu1_OctetList = au1Temp;
    MEMSET (au1Temp, 0, IP6_ADDR_SIZE);

    tPrevAddress.pu1_OctetList = au1Temp1;
    MEMSET (au1Temp1, 0, IP6_ADDR_SIZE);

    /*fsipv6AddrTable */
    i1OutCome =
        nmhGetFirstIndexFsipv6AddrTable (&i4Index, &tAddress, &i4PrefLen);
    while (i1OutCome != SNMP_FAILURE)
    {
        if (i4Index == (INT4) u4IfIndex)
        {
            Ip6Nd6SecureCgaAddrTableInfo (CliHandle, i4Index, &tAddress,
                                          i4PrefLen);
        }

        i4PrevIndex = i4Index;
        tPrevAddress = tAddress;
        i4PrevPrefLen = i4PrefLen;
        i1OutCome = nmhGetNextIndexFsipv6AddrTable (i4PrevIndex, &i4Index,
                                                    &tPrevAddress, &tAddress,
                                                    i4PrevPrefLen, &i4PrefLen);
    }
    return;
}

/*****************************************************************************/
/*     FUNCTION NAME    : Ip6Nd6SecureCgaAddrTableInfo                       */
/*                                                                           */
/*     DESCRIPTION      : This function displays fsipv6AddrTable    objects  */
/*                        for show cga database.                             */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : VOID                                               */
/*****************************************************************************/
VOID
Ip6Nd6SecureCgaAddrTableInfo (tCliHandle CliHandle, INT4 i4IfIndex,
                              tSNMP_OCTET_STRING_TYPE * pPrefixAddrs,
                              INT4 i4PrefixAddrLen)
{
    INT4                i4TableObject = 0;
    INT4                i4Flag = FALSE;
    INT4                i4CgaStatus = 0;
    UINT1               u1CfgMethod = 0;
    tSNMP_OCTET_STRING_TYPE tCgaModifier;
    INT4                i4CgaCollisionCount = 0;
    UINT1               au1AddrOctetList[IP6_ADDR_SIZE];
    INT4                i4RetValFsipv6SENDSecLevel = 0;
    INT4                i4RetValFsipv6SENDNbrSecLevel = 0;
    INT4                i4RetValFsipv6IfSENDSecStatus = 0;

    MEMSET (au1AddrOctetList, IP6_ZERO, IP6_ADDR_SIZE);
    tCgaModifier.pu1_OctetList = au1AddrOctetList;
    tCgaModifier.i4_Length = IP6_ADDR_SIZE;

    /*Ipv6 Addr admin status */
    nmhGetFsipv6AddrAdminStatus (i4IfIndex,
                                 pPrefixAddrs, i4PrefixAddrLen, &i4TableObject);

    /*Ipv6 prefix Profile Index */
    nmhGetFsipv6AddrType (i4IfIndex,
                          pPrefixAddrs, i4PrefixAddrLen, &i4TableObject);

    Ipv6UtilGetConfigMethod (i4IfIndex, pPrefixAddrs,
                             (UINT1) i4PrefixAddrLen,
                             (UINT1) i4TableObject, &u1CfgMethod);

    nmhGetFsipv6IfSENDSecStatus (i4IfIndex, &i4RetValFsipv6IfSENDSecStatus);
    if (i4RetValFsipv6IfSENDSecStatus == ND6_SECURE_DISABLE)
    {
        return;
    }

    if ((u1CfgMethod == IP6_ADDR_STATIC) || (u1CfgMethod == IP6_ADDR_AUTO_SL))
    {
        i4Flag = TRUE;
    }
    else
    {
        i4Flag = FALSE;
    }

    if (i4Flag == TRUE)
    {
        CliPrintf (CliHandle, " SeND CGA Parameters\r\n");
        nmhGetFsipv6AddrSENDCgaStatus (i4IfIndex, pPrefixAddrs,
                                       i4PrefixAddrLen, &i4CgaStatus);

        if (i4CgaStatus == IP6_TRUE)
        {
            CliPrintf (CliHandle, " ipv6 address %s ",
                       Ip6PrintNtop ((tIp6Addr *) (VOID *) (pPrefixAddrs->
                                                            pu1_OctetList)));
            if (i4TableObject == IP6_ADDR_TYPE_UNICAST)
            {
                CliPrintf (CliHandle, "%d unicast \n", i4PrefixAddrLen);
            }
            else if (i4TableObject == IP6_ADDR_TYPE_LINK_LOCAL)
            {
                CliPrintf (CliHandle, "link-local \n");
            }

            nmhGetFsipv6AddrSENDCgaModifier (i4IfIndex, pPrefixAddrs,
                                             i4PrefixAddrLen, &tCgaModifier);

            CliPrintf (CliHandle, " Modifier: %s \n",
                       Ip6PrintNtop ((tIp6Addr *) (VOID *) (tCgaModifier.
                                                            pu1_OctetList)));

            nmhGetFsipv6AddrSENDCollisionCount (i4IfIndex, pPrefixAddrs,
                                                i4PrefixAddrLen,
                                                &i4CgaCollisionCount);
            nmhGetFsipv6SENDSecLevel (&i4RetValFsipv6SENDSecLevel);
            nmhGetFsipv6SENDNbrSecLevel (&i4RetValFsipv6SENDNbrSecLevel);

            CliPrintf (CliHandle, " Collision Count: %d \n",
                       i4CgaCollisionCount);
            CliPrintf (CliHandle, " Cga Sec Level : %d \n",
                       i4RetValFsipv6SENDSecLevel);
            CliPrintf (CliHandle, " Neighbor Sec Level : %d \n",
                       i4RetValFsipv6SENDNbrSecLevel);

            CliPrintf (CliHandle, "\r\n");
        }
    }
    return;
}

/*****************************************************************************/
/*     FUNCTION NAME    : Ip6IfShowNd6SecureStatistics                       */
/*                                                                           */
/*     DESCRIPTION      : This function displays statistics of Sent packets  */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : VOID                                               */
/*****************************************************************************/
VOID
Ip6IfShowNd6SecureStatistics (tCliHandle CliHandle, UINT4 u4IfIndex)
{
    INT1                i1OutCome = 0;
    INT4                i4PrevIndex;
    INT4                i4PktType;
    INT4                i4PrevPktType;
    INT4                i4Index = 0;
    INT1               *pi1IfName = NULL;
    UINT1               au1IfName[CFA_CLI_MAX_IF_NAME_LEN];
    UINT1               u1DisplayHdr = TRUE;
    pi1IfName = (INT1 *) &au1IfName[0];

    /* For Statistics of Sent Packets */
    i1OutCome =
        nmhGetFirstIndexFsipv6SENDSentPktStatsTable (&i4Index, &i4PktType);
    while (i1OutCome != SNMP_FAILURE)
    {
        if ((UINT4) i4Index == u4IfIndex)
        {
            if (u1DisplayHdr == TRUE)
            {
                CfaCliGetIfName (i4Index, pi1IfName);
                CliPrintf (CliHandle, "SeND - Sent ND messages on %s",
                           pi1IfName);
                CliPrintf (CliHandle,
                           "\r\n        CGA    CERT   MTU   NONCE  PREFIX   RD     RSA    SLLA   TLLA   TA   TIMESTAMP ");
                CliPrintf (CliHandle,
                           "\r\n        ---    ----   ---   -----  ------   --     ---    ----   ----   --   --------\r\n");
                u1DisplayHdr = FALSE;
            }
            Ip6Nd6ShowSentPktsStatsInfo (CliHandle, i4Index, i4PktType);
        }

        i4PrevIndex = i4Index;
        i4PrevPktType = i4PktType;

        i1OutCome =
            nmhGetNextIndexFsipv6SENDSentPktStatsTable (i4PrevIndex, &i4Index,
                                                        i4PrevPktType,
                                                        &i4PktType);
    }
    u1DisplayHdr = TRUE;
    i1OutCome = 0;
    i4Index = 0;
    i4PrevPktType = 0;
    i4PrevIndex = 0;
    i4PktType = 0;

    /* For Statistics of Received Packets */
    i1OutCome =
        nmhGetFirstIndexFsipv6SENDRcvPktStatsTable (&i4Index, &i4PktType);
    while (i1OutCome != SNMP_FAILURE)
    {
        if ((UINT4) i4Index == u4IfIndex)
        {
            if (u1DisplayHdr == TRUE)
            {
                CfaCliGetIfName (i4Index, pi1IfName);
                CliPrintf (CliHandle, "SeND - Received ND messages on %s",
                           pi1IfName);
                CliPrintf (CliHandle,
                           "\r\n        CGA    CERT   MTU   NONCE  PREFIX   RD     RSA    SLLA   TLLA   TA   TIMESTAMP ");
                CliPrintf (CliHandle,
                           "\r\n        ---    ----   ---   -----  ------   --     ---    ----   ----   --   --------\r\n");
                u1DisplayHdr = FALSE;
            }
            Ip6Nd6ShowRcvPktsStatsInfo (CliHandle, i4Index, i4PktType);
        }

        i4PrevIndex = i4Index;
        i4PrevPktType = i4PktType;

        i1OutCome =
            nmhGetNextIndexFsipv6SENDRcvPktStatsTable (i4PrevIndex, &i4Index,
                                                       i4PrevPktType,
                                                       &i4PktType);
    }
    u1DisplayHdr = TRUE;
    i1OutCome = 0;
    i4Index = 0;
    i4PrevPktType = 0;
    i4PrevIndex = 0;
    i4PktType = 0;

    /* For Statistics of Dropped Packets */
    i1OutCome =
        nmhGetFirstIndexFsipv6SENDDrpPktStatsTable (&i4Index, &i4PktType);

    while (i1OutCome != SNMP_FAILURE)
    {
        if ((UINT4) i4Index == u4IfIndex)
        {
            if (u1DisplayHdr == TRUE)
            {
                CfaCliGetIfName (i4Index, pi1IfName);
                CliPrintf (CliHandle, "SeND - Dropped ND messages on %s",
                           pi1IfName);
                CliPrintf (CliHandle,
                           "\r\n        CGA    CERT   MTU   NONCE  PREFIX   RD     RSA    SLLA   TLLA   TA   TIMESTAMP ");
                CliPrintf (CliHandle,
                           "\r\n        ---    ----   ---   -----  ------   --     ---    ----   ----   --   --------\r\n");
                u1DisplayHdr = FALSE;
            }
            Ip6Nd6ShowDrpPktsStatsInfo (CliHandle, i4Index, i4PktType);
        }

        i4PrevIndex = i4Index;
        i4PrevPktType = i4PktType;

        i1OutCome =
            nmhGetNextIndexFsipv6SENDDrpPktStatsTable (i4PrevIndex, &i4Index,
                                                       i4PrevPktType,
                                                       &i4PktType);
    }
    CliPrintf (CliHandle, "\r\n");
    return;
}

/*****************************************************************************/
/*     FUNCTION NAME    : Ip6Nd6ShowSentPktsStatsInfo                        */
/*                                                                           */
/*     DESCRIPTION      : This function displays statistics of Sent packets  */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : VOID                                               */
/*****************************************************************************/

VOID
Ip6Nd6ShowSentPktsStatsInfo (tCliHandle CliHandle, INT4 i4Index, INT4 i4PktType)
{
    UINT4               u4RetValCgaOptPkts = 0;
    UINT4               u4RetValCertOptPkts = 0;
    UINT4               u4RetValMtuOptPkts = 0;
    UINT4               u4RetValNonceOptPkts = 0;
    UINT4               u4RetValPrefixOptPkts = 0;
    UINT4               u4RetValRedirOptPkts = 0;
    UINT4               u4RetValRsaOptPkts = 0;
    UINT4               u4RetValSrcLlAddrPkts = 0;
    UINT4               u4RetValTargetLlAddrPkts = 0;
    UINT4               u4RetValTaOptPkts = 0;
    UINT4               u4RetValTimeStampOptPkts = 0;
    INT4                i4RetValFsipv6IfSENDSecStatus = 0;
    CONST CHR1         *au1PktType[] = {
        "NS", "NA", "RS", "RA", "RD", "CPS", "CPA"
    };

    nmhGetFsipv6IfSENDSecStatus (i4Index, &i4RetValFsipv6IfSENDSecStatus);
    if (i4RetValFsipv6IfSENDSecStatus == ND6_SECURE_DISABLE)
    {
        return;
    }

    nmhGetFsipv6SENDSentCgaOptPkts (i4Index, i4PktType, &u4RetValCgaOptPkts);
    nmhGetFsipv6SENDSentCertOptPkts (i4Index, i4PktType, &u4RetValCertOptPkts);
    nmhGetFsipv6SENDSentMtuOptPkts (i4Index, i4PktType, &u4RetValMtuOptPkts);
    nmhGetFsipv6SENDSentNonceOptPkts (i4Index, i4PktType,
                                      &u4RetValNonceOptPkts);
    nmhGetFsipv6SENDSentPrefixOptPkts (i4Index, i4PktType,
                                       &u4RetValPrefixOptPkts);
    nmhGetFsipv6SENDSentRedirHdrPkts (i4Index, i4PktType,
                                      &u4RetValRedirOptPkts);
    nmhGetFsipv6SENDSentRsaOptPkts (i4Index, i4PktType, &u4RetValRsaOptPkts);
    nmhGetFsipv6SENDSentSrcLinkAddrPkts (i4Index, i4PktType,
                                         &u4RetValSrcLlAddrPkts);
    nmhGetFsipv6SENDSentTgtLinkAddrPkts (i4Index, i4PktType,
                                         &u4RetValTargetLlAddrPkts);
    nmhGetFsipv6SENDSentTaOptPkts (i4Index, i4PktType, &u4RetValTaOptPkts);
    nmhGetFsipv6SENDSentTimeStampOptPkts (i4Index, i4PktType,
                                          &u4RetValTimeStampOptPkts);

    CliPrintf (CliHandle,
               "%-4s %5d  %5d  %5d  %5d  %5d  %5d  %5d  %5d  %5d  %5d  %5d\r\n",
               au1PktType[i4PktType - 1], u4RetValCgaOptPkts,
               u4RetValCertOptPkts, u4RetValMtuOptPkts, u4RetValNonceOptPkts,
               u4RetValPrefixOptPkts, u4RetValRedirOptPkts, u4RetValRsaOptPkts,
               u4RetValSrcLlAddrPkts, u4RetValTargetLlAddrPkts,
               u4RetValTaOptPkts, u4RetValTimeStampOptPkts);
    return;

}

/*****************************************************************************/
/*     FUNCTION NAME    : Ip6Nd6ShowRcvPktsStatsInfo                         */
/*                                                                           */
/*     DESCRIPTION      : This function displays statistics of Sent packets  */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : VOID                                               */
/*****************************************************************************/

VOID
Ip6Nd6ShowRcvPktsStatsInfo (tCliHandle CliHandle, INT4 i4Index, INT4 i4PktType)
{
    INT1               *pi1IfName;
    UINT1               au1IfName[CFA_CLI_MAX_IF_NAME_LEN];
    UINT4               u4RetValCgaOptPkts = 0;
    UINT4               u4RetValCertOptPkts = 0;
    UINT4               u4RetValMtuOptPkts = 0;
    UINT4               u4RetValNonceOptPkts = 0;
    UINT4               u4RetValPrefixOptPkts = 0;
    UINT4               u4RetValRedirOptPkts = 0;
    UINT4               u4RetValRsaOptPkts = 0;
    UINT4               u4RetValSrcLlAddrPkts = 0;
    UINT4               u4RetValTargetLlAddrPkts = 0;
    UINT4               u4RetValTaOptPkts = 0;
    UINT4               u4RetValTimeStampOptPkts = 0;
    INT4                i4RetValFsipv6IfSENDSecStatus = 0;
    pi1IfName = (INT1 *) &au1IfName[0];
    CONST CHR1         *au1PktType[] = {
        "NS", "NA", "RS", "RA", "RD", "CPS", "CPA"
    };

    nmhGetFsipv6IfSENDSecStatus (i4Index, &i4RetValFsipv6IfSENDSecStatus);
    if (i4RetValFsipv6IfSENDSecStatus == ND6_SECURE_DISABLE)
    {
        return;
    }
    CfaCliGetIfName (i4Index, pi1IfName);

    nmhGetFsipv6SENDRcvCgaOptPkts (i4Index, i4PktType, &u4RetValCgaOptPkts);
    nmhGetFsipv6SENDRcvCertOptPkts (i4Index, i4PktType, &u4RetValCertOptPkts);
    nmhGetFsipv6SENDRcvMtuOptPkts (i4Index, i4PktType, &u4RetValMtuOptPkts);
    nmhGetFsipv6SENDRcvNonceOptPkts (i4Index, i4PktType, &u4RetValNonceOptPkts);
    nmhGetFsipv6SENDRcvPrefixOptPkts (i4Index, i4PktType,
                                      &u4RetValPrefixOptPkts);
    nmhGetFsipv6SENDRcvRedirHdrPkts (i4Index, i4PktType, &u4RetValRedirOptPkts);
    nmhGetFsipv6SENDRcvRsaOptPkts (i4Index, i4PktType, &u4RetValRsaOptPkts);
    nmhGetFsipv6SENDRcvSrcLinkAddrPkts (i4Index, i4PktType,
                                        &u4RetValSrcLlAddrPkts);
    nmhGetFsipv6SENDRcvTgtLinkAddrPkts (i4Index, i4PktType,
                                        &u4RetValTargetLlAddrPkts);
    nmhGetFsipv6SENDRcvTaOptPkts (i4Index, i4PktType, &u4RetValTaOptPkts);
    nmhGetFsipv6SENDRcvTimeStampOptPkts (i4Index, i4PktType,
                                         &u4RetValTimeStampOptPkts);

    CliPrintf (CliHandle,
               "%-4s %5d  %5d  %5d  %5d  %5d  %5d  %5d  %5d  %5d  %5d  %5d\r\n",
               au1PktType[i4PktType - 1], u4RetValCgaOptPkts,
               u4RetValCertOptPkts, u4RetValMtuOptPkts, u4RetValNonceOptPkts,
               u4RetValPrefixOptPkts, u4RetValRedirOptPkts, u4RetValRsaOptPkts,
               u4RetValSrcLlAddrPkts, u4RetValTargetLlAddrPkts,
               u4RetValTaOptPkts, u4RetValTimeStampOptPkts);
    return;
}

/*****************************************************************************/
/*     FUNCTION NAME    : Ip6Nd6ShowDrpPktsStatsInfo                         */
/*                                                                           */
/*     DESCRIPTION      : This function displays statistics of Sent packets  */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : VOID                                               */
/*****************************************************************************/
VOID
Ip6Nd6ShowDrpPktsStatsInfo (tCliHandle CliHandle, INT4 i4Index, INT4 i4PktType)
{
    INT1               *pi1IfName;
    UINT1               au1IfName[CFA_CLI_MAX_IF_NAME_LEN];
    CONST CHR1         *au1PktType[] = {
        "NS", "NA", "RS", "RA", "RD", "CPS", "CPA"
    };
    UINT4               u4RetValCgaOptPkts = 0;
    UINT4               u4RetValCertOptPkts = 0;
    UINT4               u4RetValMtuOptPkts = 0;
    UINT4               u4RetValNonceOptPkts = 0;
    UINT4               u4RetValPrefixOptPkts = 0;
    UINT4               u4RetValRedirOptPkts = 0;
    UINT4               u4RetValRsaOptPkts = 0;
    UINT4               u4RetValSrcLlAddrPkts = 0;
    UINT4               u4RetValTargetLlAddrPkts = 0;
    UINT4               u4RetValTaOptPkts = 0;
    UINT4               u4RetValTimeStampOptPkts = 0;
    INT4                i4RetValFsipv6IfSENDSecStatus = 0;
    pi1IfName = (INT1 *) &au1IfName[0];
    CfaCliGetIfName (i4Index, pi1IfName);

    nmhGetFsipv6IfSENDSecStatus (i4Index, &i4RetValFsipv6IfSENDSecStatus);
    if (i4RetValFsipv6IfSENDSecStatus == ND6_SECURE_DISABLE)
    {
        return;
    }

    nmhGetFsipv6SENDDrpCgaOptPkts (i4Index, i4PktType, &u4RetValCgaOptPkts);
    nmhGetFsipv6SENDDrpCertOptPkts (i4Index, i4PktType, &u4RetValCertOptPkts);
    nmhGetFsipv6SENDDrpMtuOptPkts (i4Index, i4PktType, &u4RetValMtuOptPkts);
    nmhGetFsipv6SENDDrpNonceOptPkts (i4Index, i4PktType, &u4RetValNonceOptPkts);
    nmhGetFsipv6SENDDrpPrefixOptPkts (i4Index, i4PktType,
                                      &u4RetValPrefixOptPkts);
    nmhGetFsipv6SENDDrpRedirHdrPkts (i4Index, i4PktType, &u4RetValRedirOptPkts);
    nmhGetFsipv6SENDDrpRsaOptPkts (i4Index, i4PktType, &u4RetValRsaOptPkts);
    nmhGetFsipv6SENDDrpSrcLinkAddrPkts (i4Index, i4PktType,
                                        &u4RetValSrcLlAddrPkts);
    nmhGetFsipv6SENDDrpTgtLinkAddrPkts (i4Index, i4PktType,
                                        &u4RetValTargetLlAddrPkts);
    nmhGetFsipv6SENDDrpTaOptPkts (i4Index, i4PktType, &u4RetValTaOptPkts);
    nmhGetFsipv6SENDDrpTimeStampOptPkts (i4Index, i4PktType,
                                         &u4RetValTimeStampOptPkts);

    CliPrintf (CliHandle,
               "%-4s %5d  %5d  %5d  %5d  %5d  %5d  %5d  %5d  %5d  %5d  %5d\r\n",
               au1PktType[i4PktType - 1], u4RetValCgaOptPkts,
               u4RetValCertOptPkts, u4RetValMtuOptPkts, u4RetValNonceOptPkts,
               u4RetValPrefixOptPkts, u4RetValRedirOptPkts, u4RetValRsaOptPkts,
               u4RetValSrcLlAddrPkts, u4RetValTargetLlAddrPkts,
               u4RetValTaOptPkts, u4RetValTimeStampOptPkts);
    return;
}

/*********************************************************************
*  Function Name : Ip6SetRARouteInfo
*  Description   : Add the given RA Route Information to the interface 
*  Input(s)      : CliHandle
*                  Ip6RARouteInfo - Route Prefix
                   i4RARoutPrefixLen - Prefix Length
                   u4RARoutePreference - Preference
                   u4RARouteLifetime - Lifetime 
*  Output(s)     : None
*  Return Values : None
*********************************************************************/
VOID
Ip6SetRARouteInfo (tCliHandle CliHandle, INT4 i4RAIfIndex,
                   tIp6Addr Ip6RARoutePrefix, INT4 i4RARoutPrefixLen,
                   INT4 i4RARoutePreference, UINT4 u4RARouteLifetime)
{
    tSNMP_OCTET_STRING_TYPE Ip6Addr;
    UINT4               u4ErrorCode;
    UINT1               au1AddrOctetList[IP6_ADDR_SIZE];

    MEMSET (au1AddrOctetList, 0, IP6_ADDR_SIZE);
    Ip6Addr.pu1_OctetList = au1AddrOctetList;

    MEMCPY (Ip6Addr.pu1_OctetList, &Ip6RARoutePrefix, IP6_ADDR_SIZE);
    Ip6Addr.i4_Length = IP6_ADDR_SIZE;

    if (Ip6ifEntryExists ((UINT4) i4RAIfIndex) == IP6_FAILURE)
    {
        CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
        return;
    }

    if (nmhTestv2Fsipv6RARouteRowStatus
        (&u4ErrorCode, i4RAIfIndex, &Ip6Addr, i4RARoutPrefixLen,
         IP6_RA_ROUTE_ROW_STATUS_CREATE_AND_WAIT) == SNMP_FAILURE)
    {
        return;
    }
    if (nmhSetFsipv6RARouteRowStatus (i4RAIfIndex, &Ip6Addr,
                                      i4RARoutPrefixLen,
                                      IP6_RA_ROUTE_ROW_STATUS_CREATE_AND_WAIT)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return;
    }

    if (nmhTestv2Fsipv6RARoutePreference (&u4ErrorCode, i4RAIfIndex,
                                          &Ip6Addr, i4RARoutPrefixLen,
                                          i4RARoutePreference) == SNMP_FAILURE)
    {
        nmhSetFsipv6RARouteRowStatus (i4RAIfIndex, &Ip6Addr,
                                      i4RARoutPrefixLen,
                                      IP6_RA_ROUTE_ROW_STATUS_DESTROY);
        return;
    }

    if (nmhSetFsipv6RARoutePreference (i4RAIfIndex, &Ip6Addr,
                                       i4RARoutPrefixLen,
                                       i4RARoutePreference) == SNMP_FAILURE)
    {
        nmhSetFsipv6RARouteRowStatus (i4RAIfIndex, &Ip6Addr,
                                      i4RARoutPrefixLen,
                                      IP6_RA_ROUTE_ROW_STATUS_DESTROY);
        CLI_FATAL_ERROR (CliHandle);
        return;
    }
    if (nmhTestv2Fsipv6RARouteLifetime (&u4ErrorCode, i4RAIfIndex,
                                        &Ip6Addr, i4RARoutPrefixLen,
                                        u4RARouteLifetime) == SNMP_FAILURE)
    {
        nmhSetFsipv6RARouteRowStatus (i4RAIfIndex, &Ip6Addr,
                                      i4RARoutPrefixLen,
                                      IP6_RA_ROUTE_ROW_STATUS_DESTROY);
        return;
    }

    if (nmhSetFsipv6RARouteLifetime (i4RAIfIndex, &Ip6Addr,
                                     i4RARoutPrefixLen, u4RARouteLifetime) ==
        SNMP_FAILURE)
    {
        nmhSetFsipv6RARouteRowStatus (i4RAIfIndex, &Ip6Addr,
                                      i4RARoutPrefixLen,
                                      IP6_RA_ROUTE_ROW_STATUS_DESTROY);
        CLI_FATAL_ERROR (CliHandle);
        return;
    }

    if (nmhSetFsipv6RARouteRowStatus (i4RAIfIndex, &Ip6Addr,
                                      i4RARoutPrefixLen,
                                      IP6_RA_ROUTE_ROW_STATUS_ACTIVE) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return;
    }

    return;
}

/*********************************************************************
*  Function Name : Ip6SetDefRoutePref
*  Description   : Add the given RA Route Information to the interface
*  Input(s)      : CliHandle
*                  i4IfIndex - Inferface index
                   i4DefRoutePreference - Default router preference
*  Output(s)     : None
*  Return Values : None
*********************************************************************/
VOID
IpSetDefRoutePref (tCliHandle CliHandle, INT4 i4IfIndex,
                   INT4 i4DefRoutePreference)
{
    UINT4               u4ErrorCode;

    if (Ip6ifEntryExists ((UINT4) i4IfIndex) == IP6_FAILURE)
    {
        CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
        return;
    }

    if (nmhTestv2Fsipv6IfDefRoutePreference (&u4ErrorCode,
                                             i4IfIndex,
                                             i4DefRoutePreference) ==
        SNMP_FAILURE)
    {
        return;
    }

    if (nmhSetFsipv6IfDefRoutePreference (i4IfIndex,
                                          i4DefRoutePreference) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return;
    }
    return;
}

/*********************************************************************
 *  Function Name : Ip6ShowRARoutesForAllVlanInCxt 
 *  Description   : Displays the RA Routes to be advertised in RA
 *                  for all the ipv6 interface with name "pu1IfName"
 *  Input(s)      : CliHandle, IfIndex, VcId, pu1IfName
 *  Output(s)     :
 *  Return Values : None
 *********************************************************************/
VOID
Ip6ShowRARoutesForAllVlanInCxt (tCliHandle CliHandle,
                                UINT4 u4IfIndex, UINT4 u4VcId, UINT1 *pu1IfName)
{
    UINT4               u4NextIfIndex = 0;
    UINT4               u4IfVcId = VCM_INVALID_VC;
    UINT4               u4ShowAllCxt = FALSE;

    if (u4VcId == VCM_INVALID_VC)
    {
        u4ShowAllCxt = TRUE;
    }
    while (CfaCliGetNextIfIndexFromName (pu1IfName, u4IfIndex,
                                         &u4NextIfIndex) != OSIX_FAILURE)
    {
        u4IfIndex = u4NextIfIndex;
        VcmGetContextIdFromCfaIfIndex (u4IfIndex, &u4IfVcId);
        if (u4ShowAllCxt == TRUE)
        {
            u4VcId = u4IfVcId;
        }
        Ip6ShowRARoutesInCxt (CliHandle, u4IfIndex, u4VcId);
        if ((u4ShowAllCxt == FALSE) && (u4VcId == u4IfVcId))
        {
            return;
        }
    }
    return;
}

/*********************************************************************
*  Function Name : Ip6ShowRARoutesInCxt 
*  Description   : Displays the RA Routes to be advertised in RA
*  Input(s)      : CliHandle
*                  u4Index - Interface Index
*                  u4VcId - VRF Id
*  Output(s)     :
*  Return Values : None.
*********************************************************************/
VOID
Ip6ShowRARoutesInCxt (tCliHandle CliHandle, UINT4 u4IfIndex, UINT4 u4VcId)
{
    UINT4               u4L3ContextId = 0;
    UINT4               u4NextL3ContextId = 0;
    UINT4               au4Ip6Ifaces[IP6_MAX_LOGICAL_IFACES + 1];
    INT4                i4RetVal = VCM_FAILURE;
    UINT1               au1ContextName[VCMALIAS_MAX_ARRAY_LEN];
    UINT2               u2Cntr = IP6_ZERO;
    UINT1               u1DisplayHdr = TRUE;

    if (u4IfIndex == 0)
    {
        if (u4VcId == VCM_INVALID_VC)
        {
            i4RetVal = VcmGetFirstActiveL3Context (&u4L3ContextId);
        }
        else
        {
            u4L3ContextId = u4VcId;
            i4RetVal = VCM_SUCCESS;
        }

        while (i4RetVal == VCM_SUCCESS)
        {
            if (u4VcId == VCM_INVALID_VC)
            {
                i4RetVal = VcmGetNextActiveL3Context (u4L3ContextId,
                                                      &u4NextL3ContextId);
                u1DisplayHdr = TRUE;
            }
            else
            {
                i4RetVal = VCM_FAILURE;
            }

            MEMSET (au4Ip6Ifaces, IP6_ZERO,
                    (IP6_MAX_LOGICAL_IFACES + 1) * sizeof (UINT4));
            if (VcmGetCxtIpIfaceList (u4L3ContextId, au4Ip6Ifaces)
                == VCM_FAILURE)
            {
                u4L3ContextId = u4NextL3ContextId;
                continue;
            }
            VcmGetAliasName (u4L3ContextId, au1ContextName);

            for (u2Cntr = 1; ((u2Cntr <= au4Ip6Ifaces[0]) &&
                              (u2Cntr < IP6_MAX_LOGICAL_IFACES + 1)); u2Cntr++)
            {
                /* Check for Valid Interface . */
                if (Ip6ifEntryExists (au4Ip6Ifaces[u2Cntr]) != IP6_FAILURE)
                {
                    if (u1DisplayHdr == TRUE)
                    {
                        CliPrintf (CliHandle, "\r\nVRF Id  : %d\r\n",
                                   u4L3ContextId);
                        CliPrintf (CliHandle, "VRF Name: %s\r\n",
                                   au1ContextName);
                        u1DisplayHdr = FALSE;
                    }
                    Ip6IfShowNd6RARoutes (CliHandle, au4Ip6Ifaces[u2Cntr]);
                }
            }
            u4L3ContextId = u4NextL3ContextId;
        }
    }
    else
    {
        /* Need to Display the detailed information for the given interface */

        if (Ip6ifEntryExists (u4IfIndex) == IP6_FAILURE)
        {
            CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
            return;
        }
        VcmGetContextIdFromCfaIfIndex (u4IfIndex, &u4L3ContextId);
        if ((u4VcId != VCM_INVALID_VC) && (u4L3ContextId != u4VcId))
        {
            /*GIven interface does not belong to given VRF */
            return;
        }

        VcmGetAliasName (u4L3ContextId, au1ContextName);

        CliPrintf (CliHandle, "VRF Id  :  %d\r\n", u4L3ContextId);
        CliPrintf (CliHandle, "VRF Name:  %s\r\n", au1ContextName);
        Ip6IfShowNd6RARoutes (CliHandle, u4IfIndex);
    }
    return;
}

/*****************************************************************************/
/*     FUNCTION NAME    : Ip6IfShowNd6RARoutes                               */
/*                                                                           */
/*     DESCRIPTION      : This function displays the RA Routes configured on */
/*                        on an interface.                                   */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        u4IfIndex - Interface index                        */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : VOID                                               */
/*****************************************************************************/
VOID
Ip6IfShowNd6RARoutes (tCliHandle CliHandle, UINT4 u4IfIndex)
{
    tSNMP_OCTET_STRING_TYPE Ip6Prefix;
    tSNMP_OCTET_STRING_TYPE Ip6PrevPrefix;
    UINT1               au1Ip6Prefix[IP6_ADDR_SIZE];
    UINT1               au1PrevIp6Prefix[IP6_ADDR_SIZE];
    INT1                i1OutCome = 0;
    INT4                i4PrevIndex = 0;
    INT4                i4PrefixLen = 0;
    INT4                i4PrevPrefixLen = 0;
    INT4                i4Index = 0;
    INT1               *pi1IfName = NULL;
    UINT1               au1IfName[CFA_CLI_MAX_IF_NAME_LEN];
    UINT1               u1DisplayHdr = TRUE;
    pi1IfName = (INT1 *) &au1IfName[0];

    MEMSET (&au1Ip6Prefix, 0, IP6_ADDR_SIZE);
    MEMSET (&au1PrevIp6Prefix, 0, IP6_ADDR_SIZE);

    Ip6Prefix.pu1_OctetList = au1Ip6Prefix;
    Ip6Prefix.i4_Length = IP6_ADDR_SIZE;
    Ip6PrevPrefix.pu1_OctetList = au1PrevIp6Prefix;
    Ip6PrevPrefix.i4_Length = IP6_ADDR_SIZE;

    i1OutCome = nmhGetFirstIndexFsipv6RARouteInfoTable (&i4Index, &Ip6Prefix,
                                                        &i4PrefixLen);
    while (i1OutCome != SNMP_FAILURE)
    {
        if ((UINT4) i4Index == u4IfIndex)
        {
            if (u1DisplayHdr == TRUE)
            {
                CfaCliGetIfName (i4Index, pi1IfName);
                u1DisplayHdr = FALSE;
            }
            Ip6Nd6ShowRARouteInfo (CliHandle, i4Index, &Ip6Prefix, i4PrefixLen);
        }
        i4PrevIndex = i4Index;
        i4PrevPrefixLen = i4PrefixLen;
        MEMCPY (Ip6PrevPrefix.pu1_OctetList, Ip6Prefix.pu1_OctetList,
                IP6_ADDR_SIZE);
        MEMSET (Ip6Prefix.pu1_OctetList, 0, IP6_ADDR_SIZE);

        i1OutCome =
            nmhGetNextIndexFsipv6RARouteInfoTable (i4PrevIndex, &i4Index,
                                                   &Ip6PrevPrefix, &Ip6Prefix,
                                                   i4PrevPrefixLen,
                                                   &i4PrefixLen);
    }
    return;
}

/*****************************************************************************/
/*     FUNCTION NAME    : Ip6Nd6ShowRARouteInfo                              */
/*                                                                           */
/*     DESCRIPTION      : This function displays the RA Routes configured on */
/*                        on an interface.                                   */
/*                                                                           */
/*     INPUT            : CliHandle   - Handle to the cli context            */
/*                        u4IfIndex   - Interface index                      */
/*                        pIp6Prefix  - Prefix                               */
/*                        i4PrefixLen - Prefix Length                  */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : VOID                                               */
/*****************************************************************************/

VOID
Ip6Nd6ShowRARouteInfo (tCliHandle CliHandle, INT4 i4Index,
                       tSNMP_OCTET_STRING_TYPE * pIp6Prefix, INT4 i4PrefixLen)
{
    tIp6Addr            Ip6RAPrefix;
    INT4                i4RoutePref = 0;
    UINT4               u4RouteLifetime = 0;
    CHR1               *pu1ValidTime = NULL;
    CHR1               *pu1Preference = NULL;
    CHR1                ac1PrefTime[MAX_IP6_PREF_TIME_LEN];
    CHR1                ac1PrefInfo[MAX_IP6_RA_PREFERENCE];

    MEMSET (ac1PrefTime, 0, MAX_IP6_PREF_TIME_LEN);
    MEMSET (ac1PrefInfo, 0, MAX_IP6_RA_PREFERENCE);
    MEMSET (&Ip6RAPrefix, 0, sizeof (tIp6Addr));
    pu1ValidTime = ac1PrefTime;
    pu1Preference = ac1PrefInfo;

    nmhGetFsipv6RARoutePreference (i4Index, pIp6Prefix, i4PrefixLen,
                                   &i4RoutePref);
    nmhGetFsipv6RARouteLifetime (i4Index, pIp6Prefix, i4PrefixLen,
                                 &u4RouteLifetime);
    /* STRCPY (pu1ValidTime, ""); */
    if (u4RouteLifetime == IP6_RA_ROUTE_MAX_LIFETIME)
    {
        SNPRINTF (pu1ValidTime, 9, "%s", "Infinite");
    }
    else
    {
        SNPRINTF (pu1ValidTime, 8, "%-8d", u4RouteLifetime);
    }

    if (i4RoutePref == IP6_RA_ROUTE_PREF_LOW)
    {
        STRCPY (pu1Preference, "Low");
    }

    if (i4RoutePref == IP6_RA_ROUTE_PREF_MED)
    {
        STRCPY (pu1Preference, "Medium");
    }

    if (i4RoutePref == IP6_RA_ROUTE_PREF_HIGH)
    {
        STRCPY (pu1Preference, "High");
    }
    Ip6CopyAddrBits (&Ip6RAPrefix,
                     (tIp6Addr *) (VOID *) pIp6Prefix->pu1_OctetList,
                     i4PrefixLen);

    CliPrintf (CliHandle,
               "%-35s  %-4d Preference %-8s Preferred lifetime %-4s\r\n",
               Ip6PrintNtop (&Ip6RAPrefix),
               i4PrefixLen, pu1Preference, pu1ValidTime);
    return;
}

/*********************************************************************
*  Function Name : Ip6DelRARouteInfo
*  Description   : Add the given RA Route Information to the interface
*  Input(s)      : CliHandle
*                  Ip6RARouteInfo - Route Prefix
                   i4RARoutPrefixLen - Prefix Length
*  Output(s)     : None
*  Return Values : None
*********************************************************************/
VOID
Ip6DelRARouteInfo (tCliHandle CliHandle, INT4 i4RAIfIndex,
                   tIp6Addr Ip6RARoutePrefix, INT4 i4RARoutPrefixLen)
{
    tSNMP_OCTET_STRING_TYPE Ip6Addr;
    UINT4               u4ErrorCode;
    UINT1               au1AddrOctetList[IP6_ADDR_SIZE];

    MEMSET (au1AddrOctetList, 0, IP6_ADDR_SIZE);
    Ip6Addr.pu1_OctetList = au1AddrOctetList;

    MEMCPY (Ip6Addr.pu1_OctetList, &Ip6RARoutePrefix, 16);
    Ip6Addr.i4_Length = 16;

    if (Ip6ifEntryExists ((UINT4) i4RAIfIndex) == IP6_FAILURE)
    {
        CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
        return;
    }

    if (nmhTestv2Fsipv6RARouteRowStatus
        (&u4ErrorCode, i4RAIfIndex, &Ip6Addr, i4RARoutPrefixLen,
         IP6_RA_ROUTE_ROW_STATUS_DESTROY) == SNMP_FAILURE)
    {
        return;
    }
    if (nmhSetFsipv6RARouteRowStatus (i4RAIfIndex, &Ip6Addr,
                                      i4RARoutPrefixLen,
                                      IP6_RA_ROUTE_ROW_STATUS_DESTROY) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return;
    }
    return;
}

/*****************************************************************************/
/*     FUNCTION NAME    : FsipvRARouteTableInfo                              */
/*                                                                           */
/*     DESCRIPTION      : This function displays FsipvRARouteTableInfo       */
/*                        objects for show running configuration.            */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        i4Index  - Interface Index                         */
/*                        pIp6Prefix - RARouteInfo Prefix                    */
/*                        i4PrefixLen - RARouteInfo Prefix Length            */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : VOID                                               */
/*****************************************************************************/
VOID
FsipvRARouteTableInfo (tCliHandle CliHandle, INT4 i4Index,
                       tSNMP_OCTET_STRING_TYPE * pIp6Prefix, INT4 i4PrefixLen)
{
    INT4                i4RoutePref = 0;
    UINT4               u4RouteLifetime = 0;
    tIp6Addr            Ip6RAPrefix;

    MEMSET (&Ip6RAPrefix, 0, sizeof (tIp6Addr));
    nmhGetFsipv6RARoutePreference (i4Index, pIp6Prefix, i4PrefixLen,
                                   &i4RoutePref);
    nmhGetFsipv6RARouteLifetime (i4Index, pIp6Prefix, i4PrefixLen,
                                 &u4RouteLifetime);

    Ip6CopyAddrBits (&Ip6RAPrefix,
                     (tIp6Addr *) (VOID *) pIp6Prefix->pu1_OctetList,
                     i4PrefixLen);
    CliPrintf (CliHandle, " ipv6 nd route-information %s ",
               Ip6PrintNtop (&Ip6RAPrefix));
    CliPrintf (CliHandle, "%d ", i4PrefixLen);

    if (i4RoutePref == IP6_RA_ROUTE_PREF_LOW)
    {
        CliPrintf (CliHandle, "preference low ");
    }

    if (i4RoutePref == IP6_RA_ROUTE_PREF_HIGH)
    {
        CliPrintf (CliHandle, "preference high ");
    }
    if (u4RouteLifetime != IP6_RA_ROUTE_MAX_LIFETIME)
    {
        CliPrintf (CliHandle, "route-lifetime  %u", u4RouteLifetime);
    }
    CliPrintf (CliHandle, "\r\n");
    return;

}

/*************************************************************************/
/*                                                                       */
/* FUNCTION NAME    : IP6ShowInterfacesInZone                            */
/*                                                                       */
/* DESCRIPTION      : This function prints  the Interfaces present       */
/*                    in specified scope-zone                            */
/*                                                                       */
/* INPUT            : pOctet           - Ports as octet list             */
/*                    u4OctetLen       - Length of port list             */
/*                    u4MaxPorts       - Max ports                       */
/*                    pu4PagingStatus  - Paging Status                   */
/*                    u1MaxPortsPerLine- Maximum Ports to be printed in  */
/*                                       a line                          */
/* OUTPUT           : CliHandle - Contains error messages                */
/*                                                                       */
/* RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                       */
/*************************************************************************/
INT4
IP6ShowInterfacesInZone (tCliHandle CliHandle,
                         tSNMP_OCTET_STRING_TYPE * pOctetStr,
                         UINT4 u4MaxPorts, UINT4 *pu4PagingStatus,
                         UINT1 u1MaxPortsPerLine)
{
    UINT1               au1NameStr[CFA_MAX_PORT_NAME_LENGTH];
    UINT4               u4Port = 0;
    UINT1               u1Counter = 0;
    UINT1               u1NewLineFlag = CLI_FALSE;
    UINT1               u1LineCount = 0;

    MEMSET (au1NameStr, 0, CFA_MAX_PORT_NAME_LENGTH);

    if (pOctetStr->pu1_OctetList)
    {
        for (u4Port = 1; u4Port <= u4MaxPorts; u4Port++)
        {
            if (CliIsMemberPort (pOctetStr->pu1_OctetList,
                                 (UINT4) pOctetStr->i4_Length,
                                 u4Port) == OSIX_SUCCESS)
            {
                if (CfaCliGetIfName (u4Port, (INT1 *) au1NameStr) ==
                    CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "%% Invalid Port %d\r\n", u4Port);
                    return CLI_FAILURE;
                }

                if (u1Counter == 0)
                {
                    u1LineCount++;
                    if (u1NewLineFlag == CLI_TRUE)
                    {
                        if (u1LineCount < 3)
                        {
                            *pu4PagingStatus = (UINT4) CliPrintf (CliHandle,
                                                                  "%34s",
                                                                  au1NameStr);
                        }
                        else
                        {
                            *pu4PagingStatus = (UINT4) CliPrintf (CliHandle,
                                                                  "%35s",
                                                                  au1NameStr);
                        }
                    }
                    else
                    {
                        /* The first line alone needs to be
                         * printed like this*/

                        *pu4PagingStatus = (UINT4) CliPrintf (CliHandle,
                                                              "%9s",
                                                              au1NameStr);
                    }
                }
                else
                {
                    *pu4PagingStatus = (UINT4) CliPrintf (CliHandle,
                                                          ", %s", au1NameStr);
                }
                u1Counter++;
                u1NewLineFlag = CLI_FALSE;

                /* Printing upto u1MaxPortsPerLine ports per line */
                if (u1Counter == u1MaxPortsPerLine)
                {
                    CliPrintf (CliHandle, "\r\n");
                    u1NewLineFlag = CLI_TRUE;
                    u1Counter = 0;
                }
            }
        }
    }

    /* Ensure a new line is printed at the end if new line is not
     * printed while coming out
     */
    if (u1NewLineFlag == CLI_FALSE)
    {
        CliPrintf (CliHandle, "\r\n");
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : Ip6SetDefAdminDistance                                  */
/*                                                                           */
/* Description      : This function is used to set the default administrative*/
/*                    distance for static IPv6 route                         */
/*                                                                           */
/*                                                                           */
/* Input Parameters : 1. CliHandle                                           */
/*                    2. u4ContextId - context ID                            */
/*                    3. i4Ip6DefAdminDistance -Default AD Value             */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/

VOID
Ip6SetDefAdminDistance (UINT4 u4ContextId, INT4 i4Ip6DefAdminDistance)
{
    UINT4               u4ErrorCode = IP_ZERO;
    /* Test whether the given Distance value is valid or not */
    if (nmhTestv2FsMIRtm6StaticRouteDistance (&u4ErrorCode,
                                              (INT4) u4ContextId,
                                              i4Ip6DefAdminDistance)
        == SNMP_FAILURE)
    {
        return;
    }

    if (nmhSetFsMIRtm6StaticRouteDistance ((INT4) u4ContextId,
                                           i4Ip6DefAdminDistance)
        == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IP6_DISTANCE_SET_ERR);
        return;
    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : Ip6ShowDefAdminDistance                                */
/*                                                                           */
/* Description      : This function is used to display the IPv6 static       */
/*                    default administrative distance configured for default */
/*                    and user VRF.                                          */
/*                                                                           */
/*                                                                           */
/* Input Parameters : 1. CliHandle                                           */
/*                    2. u4VcId - context ID                                 */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/
VOID
Ip6ShowDefAdminDistance (tCliHandle CliHandle, UINT4 u4VcId)
{
    INT4                i4Ip6ContextId = IP_ZERO;
    INT4                i4NextIp6ContextId = IP_ZERO;
    INT4                i4Ip6AdminDistance = IP_ZERO;
    INT4                i4RetVal = SNMP_SUCCESS;
    UINT1               au1ContextName[VCM_ALIAS_MAX_LEN];

    MEMSET (au1ContextName, ZERO, VCM_ALIAS_MAX_LEN);

    /* If no VRF/Context is specified, traverse through all
     * the available the contexts and display the default
     * administrative distance of static IPv6 route
     */
    if (u4VcId == VCM_INVALID_VC)
    {
        if (nmhGetFirstIndexFsMIRtm6Table (&i4Ip6ContextId) == SNMP_SUCCESS)
        {
            while (i4RetVal == SNMP_SUCCESS)
            {
                if (nmhGetFsMIRtm6StaticRouteDistance (i4Ip6ContextId,
                                                       &i4Ip6AdminDistance)
                    == SNMP_SUCCESS)
                {
                    VcmGetAliasName ((UINT4) i4Ip6ContextId, au1ContextName);
                    CliPrintf (CliHandle, "\r\n");
                    CliPrintf (CliHandle, "Vrf Name:          %s\r\n",
                               au1ContextName);
                    CliPrintf (CliHandle,
                               "IPv6 Default Administrative distance: %d\r\n",
                               i4Ip6AdminDistance);
                }
                i4RetVal = nmhGetNextIndexFsMIRtm6Table (i4Ip6ContextId,
                                                         &i4NextIp6ContextId);
                i4Ip6ContextId = i4NextIp6ContextId;
            }
        }
        return;
    }

    /* If context specified, display default administrative distance of the
     * particular context
     */
    else
    {
        if (nmhGetFsMIRtm6StaticRouteDistance ((INT4) u4VcId,
                                               &i4Ip6AdminDistance)
            == SNMP_SUCCESS)
        {
            VcmGetAliasName (u4VcId, au1ContextName);
            CliPrintf (CliHandle, "\r\n");
            CliPrintf (CliHandle, "Vrf Name:          %s\r\n", au1ContextName);
            CliPrintf (CliHandle,
                       "IPv6 Default Administrative distance: %d\r\n",
                       i4Ip6AdminDistance);
            return;
        }
    }
    return;
}

#ifndef LNXIP6_WANTED
/*****************************************************************************/
/*                                                                           */
/* Function Name    : Ip6Nd6ShowSummaryInCxt                                        */
/*                                                                           */
/* Description      : This function is invoked to display ND table           */
/*                                                                           */
/* Input Parameters : 1. CliHandle - CliContext ID                           */
/*                    2. u4NdCxtId - VRF Id                                  */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/
INT4
Ip6Nd6ShowSummaryInCxt (tCliHandle CliHandle, UINT4 u4NdCxtId)
{
    UINT4               u4ShowAllCxt = FALSE;
    UINT4               u4NDPrevCxtId = 0;
    UINT1               au1ContextName[VCMALIAS_MAX_ARRAY_LEN];

    if (u4NdCxtId != VCM_INVALID_VC)
    {
        if (VcmIsL3VcExist (u4NdCxtId) == VCM_FALSE)
        {
            CliPrintf (CliHandle, "\r%% Invalid VRF \r\n");
            return CLI_FAILURE;
        }
    }
    else
    {
        /*Display all contexts' information */
        u4ShowAllCxt = TRUE;
        if (VcmGetFirstActiveL3Context (&u4NdCxtId) == VCM_FAILURE)
        {
            return CLI_SUCCESS;
        }

    }
    do
    {
        u4NDPrevCxtId = u4NdCxtId;

        VcmGetAliasName (u4NdCxtId, au1ContextName);
        CliPrintf (CliHandle, "VRF Name: %s", au1ContextName);

        CliPrintf (CliHandle,
                   "\r\n%u Total No of IPv6 ND cache entries \r\n",
                   gNd6GblInfo.u4Nd6CacheEntries);

        CliPrintf (CliHandle,
                   "\r\n%u Reachable CacheEntries \r\n",
                   gNd6GblInfo.u4ReachNd6CacheEntries);
        CliPrintf (CliHandle,
                   "\r\n%u Incomplete CacheEntries \r\n",
                   gNd6GblInfo.u4IncompNd6CacheEntries);
        CliPrintf (CliHandle,
                   "\r\n%u Stale CacheEntries \r\n",
                   gNd6GblInfo.u4StaleNd6CacheEntries);

        CliPrintf (CliHandle,
                   "\r\n%u Delay CacheEntries \r\n",
                   gNd6GblInfo.u4DelayNd6CacheEntries);

        CliPrintf (CliHandle,
                   "\r\n%u Probe CacheEntries \r\n",
                   gNd6GblInfo.u4ProbeNd6CacheEntries);

        CliPrintf (CliHandle,
                   "\r\nNo of NS Retries 1/2/3 --> %d|%d|%d\r\n",
                   gNoOfNSRetrial_1, gNoOfNSRetrial_2, gNoOfNSRetrial_3);

        CliPrintf (CliHandle,
                   "\r\nCumulative # of Hw Failures --> %d\r\n",
                   gu4CumulativeNdHwFailCount);

        CliPrintf (CliHandle,
                   "\r\n# of Entries in HW --> %d\r\n", gu4Nd6CacheEntries);

        if (u4ShowAllCxt == FALSE)
        {
            /*Info of specified context is displayed */
            break;
        }
    }
    while (VcmGetNextActiveL3Context (u4NDPrevCxtId, &u4NdCxtId) !=
           VCM_FAILURE);
    return (CLI_SUCCESS);
}
#endif
/***********************  END OF FILE **************************************/
#endif /* __IP6CLI_C__ */
