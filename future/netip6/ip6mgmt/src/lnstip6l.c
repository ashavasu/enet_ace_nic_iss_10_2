/**************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: lnstip6l.c,v 1.15 2015/06/26 02:40:47 siva Exp $
 *
 * Description: Low level routines for stdipv6.mib (Linux IP)
 ***************************************************************************/
#include "lip6mgin.h"
#include "ip6cli.h"
#include "fsmsipcli.h"
#ifdef NPAPI_WANTED
#include "nputil.h"
#endif
/* LOW LEVEL Routines for Table : Ipv6IfTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIpv6IfTable
 Input       :  The Indices
                Ipv6IfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIpv6IfTable (INT4 i4Ipv6IfIndex)
{
    if (Lip6UtlIfEntryExists ((UINT4) i4Ipv6IfIndex) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexIpv6IfTable
 Input       :  The Indices
                Ipv6IfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIpv6IfTable (INT4 *pi4Ipv6IfIndex)
{
    *pi4Ipv6IfIndex = 0;

    if (Lip6UtlIfGetNextIndex (0, (UINT4 *) pi4Ipv6IfIndex) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexIpv6IfTable
 Input       :  The Indices
                Ipv6IfIndex
                nextIpv6IfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIpv6IfTable (INT4 i4Ipv6IfIndex, INT4 *pi4NextIpv6IfIndex)
{

    if (Lip6UtlIfGetNextIndex (i4Ipv6IfIndex, (UINT4 *) pi4NextIpv6IfIndex)
        == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIpv6IfDescr
 Input       :  The Indices
                Ipv6IfIndex

                The Object 
                retValIpv6IfDescr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpv6IfDescr (INT4 i4Ipv6IfIndex,
                   tSNMP_OCTET_STRING_TYPE * pRetValIpv6IfDescr)
{
    tLip6If            *pIf6Entry = NULL;

    pIf6Entry = Lip6UtlGetIfEntry (i4Ipv6IfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    STRNCPY (pRetValIpv6IfDescr->pu1_OctetList,
             (pIf6Entry->u1Descr), sizeof (pIf6Entry->u1Descr));
    pRetValIpv6IfDescr->i4_Length = sizeof (pIf6Entry->u1Descr);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIpv6IfLowerLayer
 Input       :  The Indices
                Ipv6IfIndex

                The Object 
                retValIpv6IfLowerLayer
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpv6IfLowerLayer (INT4 i4Ipv6IfIndex,
                        tSNMP_OID_TYPE * pRetValIpv6IfLowerLayer)
{
    tLip6If            *pIf6Entry = NULL;
    UINT4               au4EthOid[10] = { 1, 3, 6, 1, 2, 1, 1, 2, 2 };
#ifdef TUNNEL_WANTED
    UINT4               au4CTunlOid[11] = { 1, 3, 6, 1, 4, 1, 2076, 28, 5, 2 };
    UINT4               au4ATunlOid[10] = { 1, 3, 6, 1, 2, 1, 55, 1, 5 };
#endif /** TUNNEL_WANTED **/

    pIf6Entry = Lip6UtlGetIfEntry (i4Ipv6IfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    if ((pIf6Entry->u1IfType == IP6_ENET_INTERFACE_TYPE) ||
        (pIf6Entry->u1IfType == IP6_L3VLAN_INTERFACE_TYPE) ||
        (pIf6Entry->u1IfType == IP6_LAGG_INTERFACE_TYPE))
    {
        au4EthOid[9] = pIf6Entry->u4IfIndex;
        MEMCPY (pRetValIpv6IfLowerLayer->pu4_OidList, au4EthOid,
                sizeof (au4EthOid));
        pRetValIpv6IfLowerLayer->u4_Length = sizeof (au4EthOid) /
            sizeof (UINT4);
        return SNMP_SUCCESS;
    }
#ifdef TUNNEL_WANTED
    else if (pIf6Entry->u1IfType == IP6_TUNNEL_INTERFACE_TYPE)
    {
        if ((pIf6Entry->pTunlIf->u1TunlType == TNL_TYPE_COMPAT) ||
            (pIf6Entry->pTunlIf->u1TunlType == TNL_TYPE_SIXTOFOUR) ||
            (pIf6Entry->pTunlIf->u1TunlType == TNL_TYPE_ISATAP))
        {
            /* Automatic Tunnels */
            au4ATunlOid[9] = 0;    /* Automatic tunnel interface */
            MEMCPY (pRetValIpv6IfLowerLayer->pu4_OidList, au4ATunlOid,
                    10 * sizeof (UINT4));
            pRetValIpv6IfLowerLayer->u4_Length = 10;
            return SNMP_SUCCESS;
        }
        else
        {
            /* Configured Tunnels */
            au4CTunlOid[10] = pIf6Entry->u4IfIndex;    /* Configured tunnel interface */
            MEMCPY (pRetValIpv6IfLowerLayer->pu4_OidList, au4CTunlOid,
                    11 * sizeof (UINT4));
            pRetValIpv6IfLowerLayer->u4_Length = 11;
            return SNMP_SUCCESS;
        }
    }
#endif /** TUNNEL_WANTED **/

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIpv6IfEffectiveMtu
 Input       :  The Indices
                Ipv6IfIndex

                The Object 
                retValIpv6IfEffectiveMtu
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpv6IfEffectiveMtu (INT4 i4Ipv6IfIndex,
                          UINT4 *pu4RetValIpv6IfEffectiveMtu)
{
    tLip6If            *pIf6Entry = NULL;

    pIf6Entry = Lip6UtlGetIfEntry (i4Ipv6IfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValIpv6IfEffectiveMtu = Lip6UtlGetMtu (pIf6Entry);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIpv6IfReasmMaxSize
 Input       :  The Indices
                Ipv6IfIndex

                The Object 
                retValIpv6IfReasmMaxSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpv6IfReasmMaxSize (INT4 i4Ipv6IfIndex,
                          UINT4 *pu4RetValIpv6IfReasmMaxSize)
{
    tLip6If            *pIf6Entry = NULL;

    pIf6Entry = Lip6UtlGetIfEntry (i4Ipv6IfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValIpv6IfReasmMaxSize = LIP6_MAX_REASM_SIZE;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIpv6IfIdentifier
 Input       :  The Indices
                Ipv6IfIndex

                The Object 
                retValIpv6IfIdentifier
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpv6IfIdentifier (INT4 i4Ipv6IfIndex,
                        tSNMP_OCTET_STRING_TYPE * pRetValIpv6IfIdentifier)
{
    tLip6If            *pIf6Entry = NULL;
    UINT1               u1tokLen = 0;

    pIf6Entry = Lip6UtlGetIfEntry (i4Ipv6IfIndex);
    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    u1tokLen = pIf6Entry->u1TokLen;
    u1tokLen = MEM_MAX_BYTES (pIf6Entry->u1TokLen, IP6_EUI_ADDRESS_LEN);
    if (u1tokLen != 0)
        MEMCPY (pRetValIpv6IfIdentifier->pu1_OctetList, pIf6Entry->ifaceTok,
                u1tokLen);
    pRetValIpv6IfIdentifier->i4_Length = u1tokLen;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIpv6IfIdentifierLength
 Input       :  The Indices
                Ipv6IfIndex

                The Object 
                retValIpv6IfIdentifierLength
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpv6IfIdentifierLength (INT4 i4Ipv6IfIndex,
                              INT4 *pi4RetValIpv6IfIdentifierLength)
{
    tLip6If            *pIf6Entry = NULL;

    pIf6Entry = Lip6UtlGetIfEntry (i4Ipv6IfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValIpv6IfIdentifierLength = (pIf6Entry->u1TokLen) * BYTE_LENGTH;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIpv6IfPhysicalAddress
 Input       :  The Indices
                Ipv6IfIndex

                The Object 
                retValIpv6IfPhysicalAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpv6IfPhysicalAddress (INT4 i4Ipv6IfIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pRetValIpv6IfPhysicalAddress)
{
    tLip6If            *pIf6Entry = NULL;

    pIf6Entry = Lip6UtlGetIfEntry (i4Ipv6IfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pIf6Entry->u1IfType != IP6_TUNNEL_INTERFACE_TYPE)
    {
        if (Lip6PortGetHwAddr (pIf6Entry->u4IfIndex,
                               pRetValIpv6IfPhysicalAddress->pu1_OctetList) ==
            OSIX_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    pRetValIpv6IfPhysicalAddress->i4_Length = MAC_ADDR_LEN;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIpv6IfAdminStatus
 Input       :  The Indices
                Ipv6IfIndex

                The Object 
                retValIpv6IfAdminStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpv6IfAdminStatus (INT4 i4Ipv6IfIndex, INT4 *pi4RetValIpv6IfAdminStatus)
{
    return (nmhGetFsipv6IfAdminStatus (i4Ipv6IfIndex,
                                       pi4RetValIpv6IfAdminStatus));
}

/****************************************************************************
 Function    :  nmhGetIpv6IfOperStatus
 Input       :  The Indices
                Ipv6IfIndex

                The Object 
                retValIpv6IfOperStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpv6IfOperStatus (INT4 i4Ipv6IfIndex, INT4 *pi4RetValIpv6IfOperStatus)
{
    return (nmhGetFsipv6IfOperStatus (i4Ipv6IfIndex,
                                      pi4RetValIpv6IfOperStatus));
}

/****************************************************************************
 Function    :  nmhGetIpv6IfLastChange
 Input       :  The Indices
                Ipv6IfIndex

                The Object 
                retValIpv6IfLastChange
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpv6IfLastChange (INT4 i4Ipv6IfIndex, UINT4 *pu4RetValIpv6IfLastChange)
{
    tLip6If            *pIf6Entry = NULL;

    pIf6Entry = Lip6UtlGetIfEntry (i4Ipv6IfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValIpv6IfLastChange = pIf6Entry->u4IfLastUpdate;
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIpv6IfDescr
 Input       :  The Indices
                Ipv6IfIndex

                The Object 
                setValIpv6IfDescr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIpv6IfDescr (INT4 i4Ipv6IfIndex,
                   tSNMP_OCTET_STRING_TYPE * pSetValIpv6IfDescr)
{
    tLip6If            *pIf6Entry = NULL;

    pIf6Entry = Lip6UtlGetIfEntry (i4Ipv6IfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    MEMSET (pIf6Entry->u1Descr, 0, IP6_IF_DESCR_LEN);

    STRNCPY ((pIf6Entry->u1Descr), pSetValIpv6IfDescr->pu1_OctetList,
             pSetValIpv6IfDescr->i4_Length);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIpv6IfIdentifier
 Input       :  The Indices
                Ipv6IfIndex

                The Object 
                setValIpv6IfIdentifier
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIpv6IfIdentifier (INT4 i4Ipv6IfIndex,
                        tSNMP_OCTET_STRING_TYPE * pSetValIpv6IfIdentifier)
{
    tLip6If            *pIf6Entry = NULL;
    UINT1               au1Ip6IfId[IP6_EUI_ADDRESS_LEN];
    tIp6Addr            ip6Addr;

    pIf6Entry = Lip6UtlGetIfEntry (i4Ipv6IfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

#ifdef WGS_WANTED
    if (pIf6Entry->u1IfType == IP6_L2VLAN_INTERFACE_TYPE)
    {
        return (SNMP_SUCCESS);
    }
#endif

    /*All zero interface identifier */
    MEMSET (au1Ip6IfId, 0, IP6_EUI_ADDRESS_LEN);
    MEMSET (&ip6Addr, 0, sizeof (tIp6Addr));

    ip6Addr.u1_addr[0] = ADDR6_LLOCAL_PREFIX_1;
    ip6Addr.u1_addr[1] = ADDR6_LLOCAL_PREFIX_2;

    /* Check if the given interface identifier is all zero */
    if (MEMCMP (pSetValIpv6IfIdentifier->pu1_OctetList,
                au1Ip6IfId, IP6_EUI_ADDRESS_LEN) == 0)
    {
        MEMCPY (ip6Addr.u1_addr + (sizeof (tIp6Addr) - (pIf6Entry->u1TokLen)),
                pIf6Entry->ifaceTok, pIf6Entry->u1TokLen);

    }

    else
    {
        pIf6Entry->u1TokLen = (UINT1) pSetValIpv6IfIdentifier->i4_Length;

        MEMCPY (pIf6Entry->ifaceTok, pSetValIpv6IfIdentifier->pu1_OctetList,
                pSetValIpv6IfIdentifier->i4_Length);

        MEMCPY (ip6Addr.u1_addr + (sizeof (tIp6Addr) - (pIf6Entry->u1TokLen)),
                pIf6Entry->ifaceTok, pIf6Entry->u1TokLen);
    }

    OsixGetSysTime (&gIp6GblInfo.u4Ip6IfTableLastChange);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetIpv6IfIdentifierLength
 Input       :  The Indices
                Ipv6IfIndex

                The Object 
                setValIpv6IfIdentifierLength
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIpv6IfIdentifierLength (INT4 i4Ipv6IfIndex,
                              INT4 i4SetValIpv6IfIdentifierLength)
{
    tLip6If            *pIf6Entry = NULL;

    pIf6Entry = Lip6UtlGetIfEntry (i4Ipv6IfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    if ((pIf6Entry->u1IfType == IP6_ENET_INTERFACE_TYPE) ||
        (pIf6Entry->u1IfType == IP6_L3VLAN_INTERFACE_TYPE) ||
        (pIf6Entry->u1IfType == IP6_LAGG_INTERFACE_TYPE))
    {
        /* length of byte * in bits */
        pIf6Entry->u1TokLen = (UINT1) ((i4SetValIpv6IfIdentifierLength) /
                                       BYTE_LENGTH);

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetIpv6IfAdminStatus
 Input       :  The Indices
                Ipv6IfIndex

                The Object 
                setValIpv6IfAdminStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIpv6IfAdminStatus (INT4 i4Ipv6IfIndex, INT4 i4SetValIpv6IfAdminStatus)
{
    return (nmhSetFsipv6IfAdminStatus (i4Ipv6IfIndex,
                                       i4SetValIpv6IfAdminStatus));
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ipv6IfDescr
 Input       :  The Indices
                Ipv6IfIndex

                The Object 
                testValIpv6IfDescr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ipv6IfDescr (UINT4 *pu4ErrorCode, INT4 i4Ipv6IfIndex,
                      tSNMP_OCTET_STRING_TYPE * pTestValIpv6IfDescr)
{
    if (Lip6UtlIfEntryExists (i4Ipv6IfIndex) == OSIX_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (pTestValIpv6IfDescr->pu1_OctetList == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

#ifdef SNMP_2_WANTED
    /* NVT CHECK */
    if (SNMPCheckForNVTChars (pTestValIpv6IfDescr->pu1_OctetList,
                              pTestValIpv6IfDescr->i4_Length) == OSIX_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
#endif

    if (pTestValIpv6IfDescr->i4_Length > IP6_IF_DESCR_LEN)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Ipv6IfIdentifier
 Input       :  The Indices
                Ipv6IfIndex

                The Object 
                testValIpv6IfIdentifier
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ipv6IfIdentifier (UINT4 *pu4ErrorCode, INT4 i4Ipv6IfIndex,
                           tSNMP_OCTET_STRING_TYPE * pTestValIpv6IfIdentifier)
{
    if (Lip6UtlIfEntryExists (i4Ipv6IfIndex) == OSIX_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((pTestValIpv6IfIdentifier->i4_Length) > IP6_EUI_ADDRESS_LEN)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2Ipv6IfIdentifierLength
 Input       :  The Indices
                Ipv6IfIndex

                The Object 
                testValIpv6IfIdentifierLength
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ipv6IfIdentifierLength (UINT4 *pu4ErrorCode, INT4 i4Ipv6IfIndex,
                                 INT4 i4TestValIpv6IfIdentifierLength)
{
    if (Lip6UtlIfEntryExists (i4Ipv6IfIndex) == OSIX_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4TestValIpv6IfIdentifierLength > IP6_EUI_ADDRESS_LEN * BYTE_LENGTH)
        || (i4TestValIpv6IfIdentifierLength < 0))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Ipv6IfAdminStatus
 Input       :  The Indices
                Ipv6IfIndex

                The Object 
                testValIpv6IfAdminStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ipv6IfAdminStatus (UINT4 *pu4ErrorCode, INT4 i4Ipv6IfIndex,
                            INT4 i4TestValIpv6IfAdminStatus)
{
    return (nmhTestv2Fsipv6IfAdminStatus (pu4ErrorCode, i4Ipv6IfIndex,
                                          i4TestValIpv6IfAdminStatus));

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ipv6IfTable
 Input       :  The Indices
                Ipv6IfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ipv6IfTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ipv6IfStatsTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIpv6IfStatsTable
 Input       :  The Indices
                Ipv6IfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIpv6IfStatsTable (INT4 i4Ipv6IfIndex)
{
    if (Lip6UtlIfEntryExists ((UINT4) i4Ipv6IfIndex) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (Lip6UtlGetIfEntry ((UINT2) i4Ipv6IfIndex) == NULL)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexIpv6IfStatsTable
 Input       :  The Indices
                Ipv6IfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIpv6IfStatsTable (INT4 *pi4Ipv6IfIndex)
{
    *pi4Ipv6IfIndex = 0;

    if (Lip6UtlIfGetNextIndex (0, (UINT4 *) pi4Ipv6IfIndex) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexIpv6IfStatsTable
 Input       :  The Indices
                Ipv6IfIndex
                nextIpv6IfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIpv6IfStatsTable (INT4 i4Ipv6IfIndex, INT4 *pi4NextIpv6IfIndex)
{
    UINT4               u4Index = (i4Ipv6IfIndex) + 1;

    for (; u4Index <= (UINT4) IP6_MAX_LOGICAL_IFACES; u4Index++)
    {
        if (Lip6UtlIfEntryExists (u4Index) == OSIX_SUCCESS)
        {
            *pi4NextIpv6IfIndex = u4Index;
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIpv6IfStatsInReceives
 Input       :  The Indices
                Ipv6IfIndex

                The Object 
                retValIpv6IfStatsInReceives
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpv6IfStatsInReceives (INT4 i4Ipv6IfIndex,
                             UINT4 *pu4RetValIpv6IfStatsInReceives)
{
    tLip6If            *pIf6Entry = NULL;
    INT4                i4RetVal = 0;
    UINT4    u4ContextId = VCM_DEFAULT_CONTEXT;
    u4ContextId = (UINT4)UtilRtm6GetCurrCxtId ();

    pIf6Entry = Lip6UtlGetIfEntry (i4Ipv6IfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    i4RetVal = Lip6KernGetIfStat ((UINT4) i4Ipv6IfIndex, IP6_IN_RECEIVES,
                                  pu4RetValIpv6IfStatsInReceives, u4ContextId);
    if (i4RetVal == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIpv6IfStatsInHdrErrors
 Input       :  The Indices
                Ipv6IfIndex

                The Object 
                retValIpv6IfStatsInHdrErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpv6IfStatsInHdrErrors (INT4 i4Ipv6IfIndex,
                              UINT4 *pu4RetValIpv6IfStatsInHdrErrors)
{
    tLip6If            *pIf6Entry = NULL;
    INT4                i4RetVal = 0;
    UINT4    u4ContextId = VCM_DEFAULT_CONTEXT;
    u4ContextId = (UINT4)UtilRtm6GetCurrCxtId ();

    pIf6Entry = Lip6UtlGetIfEntry (i4Ipv6IfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    i4RetVal = Lip6KernGetIfStat ((UINT4) i4Ipv6IfIndex, IP6_IN_HDRERRORS,
                                  pu4RetValIpv6IfStatsInHdrErrors, u4ContextId);
    if (i4RetVal == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIpv6IfStatsInTooBigErrors
 Input       :  The Indices
                Ipv6IfIndex

                The Object 
                retValIpv6IfStatsInTooBigErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpv6IfStatsInTooBigErrors (INT4 i4Ipv6IfIndex,
                                 UINT4 *pu4RetValIpv6IfStatsInTooBigErrors)
{
    tLip6If            *pIf6Entry = NULL;
    INT4                i4RetVal = 0;
    UINT4    u4ContextId = VCM_DEFAULT_CONTEXT;
    u4ContextId = (UINT4)UtilRtm6GetCurrCxtId ();

    pIf6Entry = Lip6UtlGetIfEntry (i4Ipv6IfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    i4RetVal = Lip6KernGetIfStat ((UINT4) i4Ipv6IfIndex, IP6_IN_TOOBIGERRORS,
                                  pu4RetValIpv6IfStatsInTooBigErrors, 
                                  u4ContextId);
    if (i4RetVal == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIpv6IfStatsInNoRoutes
 Input       :  The Indices
                Ipv6IfIndex

                The Object 
                retValIpv6IfStatsInNoRoutes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpv6IfStatsInNoRoutes (INT4 i4Ipv6IfIndex,
                             UINT4 *pu4RetValIpv6IfStatsInNoRoutes)
{
    tLip6If            *pIf6Entry = NULL;
    INT4                i4RetVal = 0;
    UINT4    u4ContextId = VCM_DEFAULT_CONTEXT;
    u4ContextId = (UINT4)UtilRtm6GetCurrCxtId ();

    pIf6Entry = Lip6UtlGetIfEntry (i4Ipv6IfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    i4RetVal = Lip6KernGetIfStat ((UINT4) i4Ipv6IfIndex, IP6_IN_NOROUTES,
                                  pu4RetValIpv6IfStatsInNoRoutes,
                                  u4ContextId);
    if (i4RetVal == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIpv6IfStatsInAddrErrors
 Input       :  The Indices
                Ipv6IfIndex

                The Object 
                retValIpv6IfStatsInAddrErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpv6IfStatsInAddrErrors (INT4 i4Ipv6IfIndex,
                               UINT4 *pu4RetValIpv6IfStatsInAddrErrors)
{
    tLip6If            *pIf6Entry = NULL;
    INT4                i4RetVal = 0;
    UINT4    u4ContextId = VCM_DEFAULT_CONTEXT;
    u4ContextId = (UINT4)UtilRtm6GetCurrCxtId ();

    pIf6Entry = Lip6UtlGetIfEntry (i4Ipv6IfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    i4RetVal = Lip6KernGetIfStat ((UINT4) i4Ipv6IfIndex, IP6_IN_ADDRERRORS,
                                  pu4RetValIpv6IfStatsInAddrErrors,
                                  u4ContextId);
    if (i4RetVal == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIpv6IfStatsInUnknownProtos
 Input       :  The Indices
                Ipv6IfIndex

                The Object 
                retValIpv6IfStatsInUnknownProtos
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpv6IfStatsInUnknownProtos (INT4 i4Ipv6IfIndex,
                                  UINT4 *pu4RetValIpv6IfStatsInUnknownProtos)
{
    tLip6If            *pIf6Entry = NULL;
    INT4                i4RetVal = 0;
    UINT4    u4ContextId = VCM_DEFAULT_CONTEXT;
    u4ContextId = (UINT4)UtilRtm6GetCurrCxtId ();

    pIf6Entry = Lip6UtlGetIfEntry (i4Ipv6IfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    i4RetVal = Lip6KernGetIfStat ((UINT4) i4Ipv6IfIndex, IP6_IN_UNKNOWNPROTOS,
                                  pu4RetValIpv6IfStatsInUnknownProtos,
                                  u4ContextId);
    if (i4RetVal == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIpv6IfStatsInTruncatedPkts
 Input       :  The Indices
                Ipv6IfIndex

                The Object 
                retValIpv6IfStatsInTruncatedPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpv6IfStatsInTruncatedPkts (INT4 i4Ipv6IfIndex,
                                  UINT4 *pu4RetValIpv6IfStatsInTruncatedPkts)
{
    tLip6If            *pIf6Entry = NULL;
    INT4                i4RetVal = 0;
    UINT4    u4ContextId = VCM_DEFAULT_CONTEXT;
    u4ContextId = (UINT4)UtilRtm6GetCurrCxtId ();

    pIf6Entry = Lip6UtlGetIfEntry (i4Ipv6IfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    i4RetVal = Lip6KernGetIfStat ((UINT4) i4Ipv6IfIndex, IP6_IN_TRUNCATEDPKTS,
                                  pu4RetValIpv6IfStatsInTruncatedPkts,
                                  u4ContextId);
    if (i4RetVal == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIpv6IfStatsInDiscards
 Input       :  The Indices
                Ipv6IfIndex

                The Object 
                retValIpv6IfStatsInDiscards
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpv6IfStatsInDiscards (INT4 i4Ipv6IfIndex,
                             UINT4 *pu4RetValIpv6IfStatsInDiscards)
{
    tLip6If            *pIf6Entry = NULL;
    INT4                i4RetVal = 0;
    UINT4    u4ContextId = VCM_DEFAULT_CONTEXT;
    u4ContextId = (UINT4)UtilRtm6GetCurrCxtId ();

    pIf6Entry = Lip6UtlGetIfEntry (i4Ipv6IfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    i4RetVal = Lip6KernGetIfStat ((UINT4) i4Ipv6IfIndex, IP6_IN_DISCARDS,
                                  pu4RetValIpv6IfStatsInDiscards,
                                  u4ContextId);
    if (i4RetVal == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIpv6IfStatsInDelivers
 Input       :  The Indices
                Ipv6IfIndex

                The Object 
                retValIpv6IfStatsInDelivers
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpv6IfStatsInDelivers (INT4 i4Ipv6IfIndex,
                             UINT4 *pu4RetValIpv6IfStatsInDelivers)
{
    tLip6If            *pIf6Entry = NULL;
    INT4                i4RetVal = 0;
    UINT4    u4ContextId = VCM_DEFAULT_CONTEXT;
    u4ContextId = (UINT4)UtilRtm6GetCurrCxtId ();

    pIf6Entry = Lip6UtlGetIfEntry (i4Ipv6IfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    i4RetVal = Lip6KernGetIfStat ((UINT4) i4Ipv6IfIndex, IP6_IN_DELIVERS,
                                  pu4RetValIpv6IfStatsInDelivers,
                                  u4ContextId);
    if (i4RetVal == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIpv6IfStatsOutForwDatagrams
 Input       :  The Indices
                Ipv6IfIndex

                The Object 
                retValIpv6IfStatsOutForwDatagrams
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpv6IfStatsOutForwDatagrams (INT4 i4Ipv6IfIndex,
                                   UINT4 *pu4RetValIpv6IfStatsOutForwDatagrams)
{
    INT4                i4RetVal = 0;
    UINT4    u4ContextId = VCM_DEFAULT_CONTEXT;
    u4ContextId = (UINT4)UtilRtm6GetCurrCxtId ();

    i4RetVal = Lip6KernGetIfStat ((UINT4) i4Ipv6IfIndex, IP6_OUT_FORWDATAGRAMS,
                                  pu4RetValIpv6IfStatsOutForwDatagrams,
                                  u4ContextId);
    if (i4RetVal == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIpv6IfStatsOutRequests
 Input       :  The Indices
                Ipv6IfIndex

                The Object 
                retValIpv6IfStatsOutRequests
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpv6IfStatsOutRequests (INT4 i4Ipv6IfIndex,
                              UINT4 *pu4RetValIpv6IfStatsOutRequests)
{
    INT4                i4RetVal = 0;
    UINT4    u4ContextId = VCM_DEFAULT_CONTEXT;
    u4ContextId = (UINT4)UtilRtm6GetCurrCxtId ();

    i4RetVal = Lip6KernGetIfStat ((UINT4) i4Ipv6IfIndex, IP6_OUT_REQUESTS,
                                  pu4RetValIpv6IfStatsOutRequests,
                                  u4ContextId);
    if (i4RetVal == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIpv6IfStatsOutDiscards
 Input       :  The Indices
                Ipv6IfIndex

                The Object 
                retValIpv6IfStatsOutDiscards
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpv6IfStatsOutDiscards (INT4 i4Ipv6IfIndex,
                              UINT4 *pu4RetValIpv6IfStatsOutDiscards)
{
    INT4                i4RetVal = 0;
    UINT4    u4ContextId = VCM_DEFAULT_CONTEXT;
    u4ContextId = (UINT4)UtilRtm6GetCurrCxtId ();

    i4RetVal = Lip6KernGetIfStat ((UINT4) i4Ipv6IfIndex, IP6_OUT_DISCARDS,
                                  pu4RetValIpv6IfStatsOutDiscards,
                                  u4ContextId);
    if (i4RetVal == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIpv6IfStatsOutFragOKs
 Input       :  The Indices
                Ipv6IfIndex

                The Object 
                retValIpv6IfStatsOutFragOKs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpv6IfStatsOutFragOKs (INT4 i4Ipv6IfIndex,
                             UINT4 *pu4RetValIpv6IfStatsOutFragOKs)
{
    tLip6If            *pIf6Entry = NULL;
    INT4                i4RetVal = 0;
    UINT4    u4ContextId = VCM_DEFAULT_CONTEXT;
    u4ContextId = (UINT4)UtilRtm6GetCurrCxtId ();

    pIf6Entry = Lip6UtlGetIfEntry (i4Ipv6IfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    i4RetVal = Lip6KernGetIfStat ((UINT4) i4Ipv6IfIndex, IP6_FRAG_OKS,
                                  pu4RetValIpv6IfStatsOutFragOKs,
                                  u4ContextId);
    if (i4RetVal == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIpv6IfStatsOutFragFails
 Input       :  The Indices
                Ipv6IfIndex

                The Object 
                retValIpv6IfStatsOutFragFails
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpv6IfStatsOutFragFails (INT4 i4Ipv6IfIndex,
                               UINT4 *pu4RetValIpv6IfStatsOutFragFails)
{
    tLip6If            *pIf6Entry = NULL;
    INT4                i4RetVal = 0;
    UINT4    u4ContextId = VCM_DEFAULT_CONTEXT;
    u4ContextId = (UINT4)UtilRtm6GetCurrCxtId ();

    pIf6Entry = Lip6UtlGetIfEntry (i4Ipv6IfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValIpv6IfStatsOutFragFails = 0;
    i4RetVal = Lip6KernGetIfStat ((UINT4) i4Ipv6IfIndex, IP6_FRAG_FAILS,
                                  pu4RetValIpv6IfStatsOutFragFails,u4ContextId);
    if (i4RetVal == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIpv6IfStatsOutFragCreates
 Input       :  The Indices
                Ipv6IfIndex

                The Object 
                retValIpv6IfStatsOutFragCreates
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpv6IfStatsOutFragCreates (INT4 i4Ipv6IfIndex,
                                 UINT4 *pu4RetValIpv6IfStatsOutFragCreates)
{
    tLip6If            *pIf6Entry = NULL;
    INT4                i4RetVal = 0;
    UINT4    u4ContextId = VCM_DEFAULT_CONTEXT;
    u4ContextId = (UINT4)UtilRtm6GetCurrCxtId ();

    pIf6Entry = Lip6UtlGetIfEntry (i4Ipv6IfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    i4RetVal = Lip6KernGetIfStat ((UINT4) i4Ipv6IfIndex, IP6_FRAG_CREATES,
                                  pu4RetValIpv6IfStatsOutFragCreates,
                                  u4ContextId);
    if (i4RetVal == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIpv6IfStatsReasmReqds
 Input       :  The Indices
                Ipv6IfIndex

                The Object 
                retValIpv6IfStatsReasmReqds
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpv6IfStatsReasmReqds (INT4 i4Ipv6IfIndex,
                             UINT4 *pu4RetValIpv6IfStatsReasmReqds)
{
    tLip6If            *pIf6Entry = NULL;
    INT4                i4RetVal = 0;
    UINT4    u4ContextId = VCM_DEFAULT_CONTEXT;
    u4ContextId = (UINT4)UtilRtm6GetCurrCxtId ();

    pIf6Entry = Lip6UtlGetIfEntry (i4Ipv6IfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    i4RetVal = Lip6KernGetIfStat ((UINT4) i4Ipv6IfIndex, IP6_REASM_REQDS,
                                  pu4RetValIpv6IfStatsReasmReqds,
                                  u4ContextId);
    if (i4RetVal == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIpv6IfStatsReasmOKs
 Input       :  The Indices
                Ipv6IfIndex

                The Object 
                retValIpv6IfStatsReasmOKs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpv6IfStatsReasmOKs (INT4 i4Ipv6IfIndex,
                           UINT4 *pu4RetValIpv6IfStatsReasmOKs)
{
    tLip6If            *pIf6Entry = NULL;
    INT4                i4RetVal = 0;
    UINT4    u4ContextId = VCM_DEFAULT_CONTEXT;
    u4ContextId = (UINT4)UtilRtm6GetCurrCxtId ();

    pIf6Entry = Lip6UtlGetIfEntry (i4Ipv6IfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    i4RetVal = Lip6KernGetIfStat ((UINT4) i4Ipv6IfIndex, IP6_REASM_OKS,
                                  pu4RetValIpv6IfStatsReasmOKs,
                                  u4ContextId);
    if (i4RetVal == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIpv6IfStatsReasmFails
 Input       :  The Indices
                Ipv6IfIndex

                The Object 
                retValIpv6IfStatsReasmFails
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpv6IfStatsReasmFails (INT4 i4Ipv6IfIndex,
                             UINT4 *pu4RetValIpv6IfStatsReasmFails)
{
    tLip6If            *pIf6Entry = NULL;
    INT4                i4RetVal = 0;
    UINT4    u4ContextId = VCM_DEFAULT_CONTEXT;
    u4ContextId = (UINT4)UtilRtm6GetCurrCxtId ();

    pIf6Entry = Lip6UtlGetIfEntry (i4Ipv6IfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    i4RetVal = Lip6KernGetIfStat ((UINT4) i4Ipv6IfIndex, IP6_REASM_FAILS,
                                  pu4RetValIpv6IfStatsReasmFails,
                                  u4ContextId);
    if (i4RetVal == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIpv6IfStatsInMcastPkts
 Input       :  The Indices
                Ipv6IfIndex

                The Object 
                retValIpv6IfStatsInMcastPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpv6IfStatsInMcastPkts (INT4 i4Ipv6IfIndex,
                              UINT4 *pu4RetValIpv6IfStatsInMcastPkts)
{
    tLip6If            *pIf6Entry = NULL;
    INT4                i4RetVal = 0;
    UINT4    u4ContextId = VCM_DEFAULT_CONTEXT;
    u4ContextId = (UINT4)UtilRtm6GetCurrCxtId ();

    pIf6Entry = Lip6UtlGetIfEntry (i4Ipv6IfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    i4RetVal = Lip6KernGetIfStat ((UINT4) i4Ipv6IfIndex, IP6_IN_MCASTPKTS,
                                  pu4RetValIpv6IfStatsInMcastPkts,
                                  u4ContextId);
    if (i4RetVal == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIpv6IfStatsOutMcastPkts
 Input       :  The Indices
                Ipv6IfIndex

                The Object 
                retValIpv6IfStatsOutMcastPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpv6IfStatsOutMcastPkts (INT4 i4Ipv6IfIndex,
                               UINT4 *pu4RetValIpv6IfStatsOutMcastPkts)
{
    tLip6If            *pIf6Entry = NULL;
    INT4                i4RetVal = 0;
    UINT4    u4ContextId = VCM_DEFAULT_CONTEXT;
    u4ContextId = (UINT4)UtilRtm6GetCurrCxtId ();

    pIf6Entry = Lip6UtlGetIfEntry (i4Ipv6IfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    i4RetVal = Lip6KernGetIfStat ((UINT4) i4Ipv6IfIndex, IP6_OUT_MCASTPKTS,
                                  pu4RetValIpv6IfStatsOutMcastPkts,
                                  u4ContextId);
    if (i4RetVal == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* LOW LEVEL Routines for Table : Ipv6AddrPrefixTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIpv6AddrPrefixTable
 Input       :  The Indices
                Ipv6IfIndex
                Ipv6AddrPrefix
                Ipv6AddrPrefixLength
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIpv6AddrPrefixTable (INT4 i4Ipv6IfIndex,
                                             tSNMP_OCTET_STRING_TYPE *
                                             pIpv6AddrPrefix,
                                             INT4 i4Ipv6AddrPrefixLength)
{
    if ((Lip6UtlIfEntryExists ((UINT4) i4Ipv6IfIndex) == OSIX_SUCCESS) &&
        (pIpv6AddrPrefix->pu1_OctetList != NULL) &&
        (i4Ipv6AddrPrefixLength <= LIP6_HOST_PREFIX_LENGTH))
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexIpv6AddrPrefixTable
 Input       :  The Indices
                Ipv6IfIndex
                Ipv6AddrPrefix
                Ipv6AddrPrefixLength
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIpv6AddrPrefixTable (INT4 *pi4Ipv6IfIndex,
                                     tSNMP_OCTET_STRING_TYPE * pIpv6AddrPrefix,
                                     INT4 *pi4Ipv6AddrPrefixLength)
{
    if (nmhGetFirstIndexFsipv6AddrTable (pi4Ipv6IfIndex,
                                         pIpv6AddrPrefix,
                                         pi4Ipv6AddrPrefixLength) ==
        SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;

    }
    else
    {
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhGetNextIndexIpv6AddrPrefixTable
 Input       :  The Indices
                Ipv6IfIndex
                nextIpv6IfIndex
                Ipv6AddrPrefix
                nextIpv6AddrPrefix
                Ipv6AddrPrefixLength
                nextIpv6AddrPrefixLength
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIpv6AddrPrefixTable (INT4 i4Ipv6IfIndex,
                                    INT4 *pi4NextIpv6IfIndex,
                                    tSNMP_OCTET_STRING_TYPE * pIpv6AddrPrefix,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pNextIpv6AddrPrefix,
                                    INT4 i4Ipv6AddrPrefixLength,
                                    INT4 *pi4NextIpv6AddrPrefixLength)
{
    if (nmhGetNextIndexFsipv6AddrTable (i4Ipv6IfIndex, pi4NextIpv6IfIndex,
                                        pIpv6AddrPrefix, pNextIpv6AddrPrefix,
                                        i4Ipv6AddrPrefixLength,
                                        pi4NextIpv6AddrPrefixLength) ==
        SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIpv6AddrPrefixOnLinkFlag
 Input       :  The Indices
                Ipv6IfIndex
                Ipv6AddrPrefix
                Ipv6AddrPrefixLength

                The Object 
                retValIpv6AddrPrefixOnLinkFlag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpv6AddrPrefixOnLinkFlag (INT4 i4Ipv6IfIndex,
                                tSNMP_OCTET_STRING_TYPE * pIpv6AddrPrefix,
                                INT4 i4Ipv6AddrPrefixLength,
                                INT4 *pi4RetValIpv6AddrPrefixOnLinkFlag)
{
    tIp6Addr            Ip6Addr;
    tLip6AddrNode      *pAddrInfo = NULL;
    tLip6If            *pIf6Entry = NULL;

    MEMCPY (&Ip6Addr, pIpv6AddrPrefix->pu1_OctetList, sizeof (tIp6Addr));

    pIf6Entry = Lip6UtlGetIfEntry (i4Ipv6IfIndex);
    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    pAddrInfo = Lip6AddrGetEntry (i4Ipv6IfIndex, &Ip6Addr,
                                  i4Ipv6AddrPrefixLength);

    if (pAddrInfo != NULL)
    {
        *pi4RetValIpv6AddrPrefixOnLinkFlag = TRUE;
        return SNMP_SUCCESS;
    }

    *pi4RetValIpv6AddrPrefixOnLinkFlag = FALSE;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIpv6AddrPrefixAutonomousFlag
 Input       :  The Indices
                Ipv6IfIndex
                Ipv6AddrPrefix
                Ipv6AddrPrefixLength

                The Object 
                retValIpv6AddrPrefixAutonomousFlag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpv6AddrPrefixAutonomousFlag (INT4 i4Ipv6IfIndex,
                                    tSNMP_OCTET_STRING_TYPE * pIpv6AddrPrefix,
                                    INT4 i4Ipv6AddrPrefixLength,
                                    INT4 *pi4RetValIpv6AddrPrefixAutonomousFlag)
{
    tIp6Addr            Ip6Addr;
    tLip6AddrNode      *pAddrInfo = NULL;
    tLip6If            *pIf6Entry = NULL;
    UINT1               u1DadStatus = 0;

    MEMCPY (&Ip6Addr, pIpv6AddrPrefix->pu1_OctetList, sizeof (tIp6Addr));

    if (IS_ADDR_LLOCAL (Ip6Addr))
    {
        pIf6Entry = Lip6UtlGetIfEntry (i4Ipv6IfIndex);
        if (pIf6Entry == NULL)
        {
            return SNMP_FAILURE;
        }

        if (OSIX_FAILURE ==
            Lip6LlaGetLinkLocalAddress (pIf6Entry, &Ip6Addr, &u1DadStatus))
        {
            return SNMP_FAILURE;
        }
        *pi4RetValIpv6AddrPrefixAutonomousFlag = TRUE;
        return SNMP_SUCCESS;
    }

    pAddrInfo = Lip6AddrGetEntry (i4Ipv6IfIndex, &Ip6Addr,
                                  i4Ipv6AddrPrefixLength);

    if (pAddrInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValIpv6AddrPrefixAutonomousFlag = TRUE;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIpv6AddrPrefixAdvPreferredLifetime
 Input       :  The Indices
                Ipv6IfIndex
                Ipv6AddrPrefix
                Ipv6AddrPrefixLength

                The Object 
                retValIpv6AddrPrefixAdvPreferredLifetime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpv6AddrPrefixAdvPreferredLifetime (INT4 i4Ipv6IfIndex,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pIpv6AddrPrefix,
                                          INT4 i4Ipv6AddrPrefixLength,
                                          UINT4
                                          *pu4RetValIpv6AddrPrefixAdvPreferredLifetime)
{
    tIp6Addr            Ip6Addr;
    tLip6AddrNode      *pAddrInfo = NULL;
    tLip6If            *pIf6Entry = NULL;
    UINT1               u1DadStatus = 0;

    MEMCPY (&Ip6Addr, pIpv6AddrPrefix->pu1_OctetList, sizeof (tIp6Addr));

    if (IS_ADDR_LLOCAL (Ip6Addr))
    {
        pIf6Entry = Lip6UtlGetIfEntry (i4Ipv6IfIndex);
        if (pIf6Entry == NULL)
        {
            return SNMP_FAILURE;
        }
        if (OSIX_FAILURE ==
            Lip6LlaGetLinkLocalAddress (pIf6Entry, &Ip6Addr, &u1DadStatus))
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        pAddrInfo = Lip6AddrGetEntry (i4Ipv6IfIndex, &Ip6Addr,
                                      i4Ipv6AddrPrefixLength);

        if (pAddrInfo == NULL)
        {
            return SNMP_FAILURE;
        }
    }

    /* Always return max value, denotes Lifetime is infinity */
    *pu4RetValIpv6AddrPrefixAdvPreferredLifetime = LIP6_MAX_VALUE;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIpv6AddrPrefixAdvValidLifetime
 Input       :  The Indices
                Ipv6IfIndex
                Ipv6AddrPrefix
                Ipv6AddrPrefixLength

                The Object 
                retValIpv6AddrPrefixAdvValidLifetime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpv6AddrPrefixAdvValidLifetime (INT4 i4Ipv6IfIndex,
                                      tSNMP_OCTET_STRING_TYPE * pIpv6AddrPrefix,
                                      INT4 i4Ipv6AddrPrefixLength,
                                      UINT4
                                      *pu4RetValIpv6AddrPrefixAdvValidLifetime)
{
    tIp6Addr            Ip6Addr;
    tLip6AddrNode      *pAddrInfo = NULL;
    tLip6If            *pIf6Entry = NULL;
    UINT1               u1DadStatus = 0;

    MEMCPY (&Ip6Addr, pIpv6AddrPrefix->pu1_OctetList, sizeof (tIp6Addr));

    if (IS_ADDR_LLOCAL (Ip6Addr))
    {
        pIf6Entry = Lip6UtlGetIfEntry (i4Ipv6IfIndex);
        if (pIf6Entry == NULL)
        {
            return SNMP_FAILURE;
        }
        if (OSIX_FAILURE ==
            Lip6LlaGetLinkLocalAddress (pIf6Entry, &Ip6Addr, &u1DadStatus))
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        pAddrInfo = Lip6AddrGetEntry (i4Ipv6IfIndex, &Ip6Addr,
                                      i4Ipv6AddrPrefixLength);

        if (pAddrInfo == NULL)
        {
            return SNMP_FAILURE;
        }
    }

    /* Always return max value, denotes Lifetime is infinity */
    *pu4RetValIpv6AddrPrefixAdvValidLifetime = LIP6_MAX_VALUE;
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ipv6AddrTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIpv6AddrTable
 Input       :  The Indices
                Ipv6IfIndex
                Ipv6AddrAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIpv6AddrTable (INT4 i4Ipv6IfIndex,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pIpv6AddrAddress)
{
    tIp6Addr            Ip6Addr;

    UNUSED_PARAM (i4Ipv6IfIndex);

    if (pIpv6AddrAddress->pu1_OctetList == NULL)
    {
        return SNMP_FAILURE;
    }

    MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));
    MEMCPY (&Ip6Addr, pIpv6AddrAddress->pu1_OctetList, sizeof (tIp6Addr));

    if ((IS_ADDR_MULTI (Ip6Addr)) || (IS_ADDR_UNSPECIFIED (Ip6Addr)))
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexIpv6AddrTable
 Input       :  The Indices
                Ipv6IfIndex
                Ipv6AddrAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIpv6AddrTable (INT4 *pi4Ipv6IfIndex,
                               tSNMP_OCTET_STRING_TYPE * pIpv6AddrAddress)
{
    INT4                i4PrefLen = 0;

    *pi4Ipv6IfIndex = 0;
    if (nmhGetFirstIndexFsipv6AddrTable (pi4Ipv6IfIndex,
                                         pIpv6AddrAddress, &i4PrefLen) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pIpv6AddrAddress->i4_Length = IP6_ADDR_SIZE;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetNextIndexIpv6AddrTable
 Input       :  The Indices
                Ipv6IfIndex
                nextIpv6IfIndex
                Ipv6AddrAddress
                nextIpv6AddrAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIpv6AddrTable (INT4 i4Ipv6IfIndex, INT4 *pi4NextIpv6IfIndex,
                              tSNMP_OCTET_STRING_TYPE * pIpv6AddrAddress,
                              tSNMP_OCTET_STRING_TYPE * pNextIpv6AddrAddress)
{
    INT4                i4PfxLen = 0;
    INT4                i4NextPfxLen = 0;
    tIp6Addr            Ip6Addr;
    INT1                i1RetVal = 0;

    MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));
    MEMCPY (&Ip6Addr, pIpv6AddrAddress->pu1_OctetList, sizeof (tIp6Addr));

    if (Lip6UtlGetAddrPrefixLen (i4Ipv6IfIndex, (tIp6Addr *) & Ip6Addr,
                                 &i4PfxLen) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhGetNextIndexFsipv6AddrTable (i4Ipv6IfIndex,
                                               pi4NextIpv6IfIndex,
                                               pIpv6AddrAddress,
                                               pNextIpv6AddrAddress,
                                               i4PfxLen, &i4NextPfxLen);
    return i1RetVal;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIpv6AddrPfxLength
 Input       :  The Indices
                Ipv6IfIndex
                Ipv6AddrAddress

                The Object 
                retValIpv6AddrPfxLength
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpv6AddrPfxLength (INT4 i4Ipv6IfIndex,
                         tSNMP_OCTET_STRING_TYPE * pIpv6AddrAddress,
                         INT4 *pi4RetValIpv6AddrPfxLength)
{
    tIp6Addr            Ip6Addr;
    INT4                i4PfxLen = 0;

    MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));
    MEMCPY (&Ip6Addr, pIpv6AddrAddress->pu1_OctetList, sizeof (tIp6Addr));

    if (Lip6UtlGetAddrPrefixLen
        (i4Ipv6IfIndex, (tIp6Addr *) & Ip6Addr, &i4PfxLen) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValIpv6AddrPfxLength = i4PfxLen;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIpv6AddrType
 Input       :  The Indices
                Ipv6IfIndex
                Ipv6AddrAddress

                The Object 
                retValIpv6AddrType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpv6AddrType (INT4 i4Ipv6IfIndex,
                    tSNMP_OCTET_STRING_TYPE * pIpv6AddrAddress,
                    INT4 *pi4RetValIpv6AddrType)
{
    tLip6AddrNode      *pAddrInfo = NULL;
    tIp6Addr            Ip6Addr;
    INT4                i4Prefix = 0;

    MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));
    MEMCPY (&Ip6Addr, pIpv6AddrAddress->pu1_OctetList, sizeof (tIp6Addr));

    if (Lip6UtlGetAddrPrefixLen (i4Ipv6IfIndex, (tIp6Addr *) & Ip6Addr,
                                 &i4Prefix) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    pAddrInfo = Lip6AddrGetEntry (i4Ipv6IfIndex, &Ip6Addr, i4Prefix);
    if (pAddrInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (Ip6AddrType (&Ip6Addr) == ADDR6_LLOCAL)
    {
        *pi4RetValIpv6AddrType = IP6_ADDR_TYPE_STATELESS;
    }
    else
    {
        *pi4RetValIpv6AddrType = IP6_ADDR_TYPE_STATEFUL;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIpv6AddrAnycastFlag
 Input       :  The Indices
                Ipv6IfIndex
                Ipv6AddrAddress

                The Object 
                retValIpv6AddrAnycastFlag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpv6AddrAnycastFlag (INT4 i4Ipv6IfIndex,
                           tSNMP_OCTET_STRING_TYPE * pIpv6AddrAddress,
                           INT4 *pi4RetValIpv6AddrAnycastFlag)
{
    tLip6AddrNode      *pAddrInfo = NULL;
    tIp6Addr            Ip6Addr;
    INT4                i4Prefix = 0;

    MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));
    MEMCPY (&Ip6Addr, pIpv6AddrAddress->pu1_OctetList, sizeof (tIp6Addr));

    if (Ip6AddrType (&Ip6Addr) == ADDR6_LLOCAL)
    {
        *pi4RetValIpv6AddrAnycastFlag = IP6_FALSE;
        return SNMP_SUCCESS;
    }

    if (Lip6UtlGetAddrPrefixLen (i4Ipv6IfIndex, (tIp6Addr *) & Ip6Addr,
                                 &i4Prefix) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    pAddrInfo = Lip6AddrGetEntry (i4Ipv6IfIndex, &Ip6Addr, i4Prefix);
    if (pAddrInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pAddrInfo->u1AddrType == ADDR6_ANYCAST)
    {
        *pi4RetValIpv6AddrAnycastFlag = IP6_TRUE;
    }
    else
    {
        *pi4RetValIpv6AddrAnycastFlag = IP6_FALSE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIpv6AddrStatus
 Input       :  The Indices
                Ipv6IfIndex
                Ipv6AddrAddress

                The Object 
                retValIpv6AddrStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpv6AddrStatus (INT4 i4Ipv6IfIndex,
                      tSNMP_OCTET_STRING_TYPE * pIpv6AddrAddress,
                      INT4 *pi4RetValIpv6AddrStatus)
{
    tIp6Addr            Ip6Addr;

    MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));
    MEMCPY (&Ip6Addr, pIpv6AddrAddress->pu1_OctetList, sizeof (tIp6Addr));

    if (Lip6UtlGetAddrStatus (i4Ipv6IfIndex, &Ip6Addr,
                              pi4RetValIpv6AddrStatus) == OSIX_SUCCESS)
    {
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/* LOW LEVEL Routines for Table : Ipv6RouteTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIpv6RouteTable
 Input       :  The Indices
                Ipv6RouteDest
                Ipv6RoutePfxLength
                Ipv6RouteIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIpv6RouteTable (tSNMP_OCTET_STRING_TYPE *
                                        pIpv6RouteDest,
                                        INT4 i4Ipv6RoutePfxLength,
                                        UINT4 u4Ipv6RouteIndex)
{
    if ((u4Ipv6RouteIndex < MAX_RTM6_ROUTE_TABLE_ENTRIES) &&
        (pIpv6RouteDest != NULL)
        && (i4Ipv6RoutePfxLength <= LIP6_HOST_PREFIX_LENGTH))
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexIpv6RouteTable
 Input       :  The Indices
                Ipv6RouteDest
                Ipv6RoutePfxLength
                Ipv6RouteIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIpv6RouteTable (tSNMP_OCTET_STRING_TYPE * pIpv6RouteDest,
                                INT4 *pi4Ipv6RoutePfxLength,
                                UINT4 *pu4Ipv6RouteIndex)
{
    tNetIpv6RtInfo      NetIpv6RtInfo;
    UINT4               u4ContextId = VCM_DEFAULT_CONTEXT;

    MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));

    IP6_TASK_UNLOCK ();
    RTM6_TASK_LOCK ();

    /* Get the first route from the TRIE. */
    if (Rtm6ApiTrieGetFirstBestRtInCxt (u4ContextId,
                                        &NetIpv6RtInfo) == NETIPV6_FAILURE)
    {
        RTM6_TASK_UNLOCK ();
        IP6_TASK_LOCK ();
        return SNMP_FAILURE;
    }
    RTM6_TASK_UNLOCK ();
    IP6_TASK_LOCK ();

    MEMCPY (pIpv6RouteDest->pu1_OctetList, &NetIpv6RtInfo.Ip6Dst,
            sizeof (tIp6Addr));
    pIpv6RouteDest->i4_Length = IP6_ADDR_SIZE;
    *pi4Ipv6RoutePfxLength = NetIpv6RtInfo.u1Prefixlen;
    *pu4Ipv6RouteIndex = 1;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetNextIndexIpv6RouteTable
 Input       :  The Indices
                Ipv6RouteDest
                nextIpv6RouteDest
                Ipv6RoutePfxLength
                nextIpv6RoutePfxLength
                Ipv6RouteIndex
                nextIpv6RouteIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIpv6RouteTable (tSNMP_OCTET_STRING_TYPE * pIpv6RouteDest,
                               tSNMP_OCTET_STRING_TYPE * pNextIpv6RouteDest,
                               INT4 i4Ipv6RoutePfxLength,
                               INT4 *pi4NextIpv6RoutePfxLength,
                               UINT4 u4Ipv6RouteIndex,
                               UINT4 *pu4NextIpv6RouteIndex)
{
    tNetIpv6RtInfo      NetIpv6RtInfo;
    UINT4               u4ContextId = VCM_DEFAULT_CONTEXT;

    MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));

    IP6_TASK_UNLOCK ();
    RTM6_TASK_LOCK ();

    /* NOTE : If multipath best route option is supported, update this routine
     * to return the next alternate best route for the same prefix and prefix
     * len. Currently only one best route for each prefix is displayed. */

    if (Rtm6ApiTrieGetNextBestRtInCxt (u4ContextId, (tIp6Addr *) (VOID *)
                                       pIpv6RouteDest->pu1_OctetList,
                                       (UINT1) i4Ipv6RoutePfxLength,
                                       u4Ipv6RouteIndex,
                                       &NetIpv6RtInfo) == RTM6_FAILURE)
    {
        /* No more best route. */
        RTM6_TASK_UNLOCK ();
        IP6_TASK_LOCK ();
        return SNMP_FAILURE;
    }

    RTM6_TASK_UNLOCK ();
    IP6_TASK_LOCK ();

    MEMCPY (pNextIpv6RouteDest->pu1_OctetList, &NetIpv6RtInfo.Ip6Dst,
            sizeof (tIp6Addr));
    pNextIpv6RouteDest->i4_Length = IP6_ADDR_SIZE;
    *pi4NextIpv6RoutePfxLength = NetIpv6RtInfo.u1Prefixlen;
    *pu4NextIpv6RouteIndex = 1;
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIpv6RouteIfIndex
 Input       :  The Indices
                Ipv6RouteDest
                Ipv6RoutePfxLength
                Ipv6RouteIndex

                The Object 
                retValIpv6RouteIfIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpv6RouteIfIndex (tSNMP_OCTET_STRING_TYPE * pIpv6RouteDest,
                        INT4 i4Ipv6RoutePfxLength, UINT4 u4Ipv6RouteIndex,
                        INT4 *pi4RetValIpv6RouteIfIndex)
{
    tIp6Addr            Ip6Addr;
    tNetIpv6RtInfo      NetIpv6RtInfo;
    UINT4               u4ContextId = VCM_DEFAULT_CONTEXT;

    MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));
    MEMCPY (&Ip6Addr, pIpv6RouteDest->pu1_OctetList, sizeof (tIp6Addr));

    MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));

    IP6_TASK_UNLOCK ();
    RTM6_TASK_LOCK ();

    if (Rtm6ApiGetBestRouteEntryInCxt
        (u4ContextId, &Ip6Addr, (UINT1) i4Ipv6RoutePfxLength, u4Ipv6RouteIndex,
         &NetIpv6RtInfo) == RTM6_FAILURE)
    {
        RTM6_TASK_UNLOCK ();
        IP6_TASK_LOCK ();
        return SNMP_FAILURE;
    }

    RTM6_TASK_UNLOCK ();
    IP6_TASK_LOCK ();

    *pi4RetValIpv6RouteIfIndex = (INT4) NetIpv6RtInfo.u4Index;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIpv6RouteNextHop
 Input       :  The Indices
                Ipv6RouteDest
                Ipv6RoutePfxLength
                Ipv6RouteIndex

                The Object 
                retValIpv6RouteNextHop
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpv6RouteNextHop (tSNMP_OCTET_STRING_TYPE * pIpv6RouteDest,
                        INT4 i4Ipv6RoutePfxLength, UINT4 u4Ipv6RouteIndex,
                        tSNMP_OCTET_STRING_TYPE * pRetValIpv6RouteNextHop)
{
    tIp6Addr            Ip6Addr;
    tNetIpv6RtInfo      NetIpv6RtInfo;
    UINT4               u4ContextId = VCM_DEFAULT_CONTEXT;

    MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));
    MEMCPY (&Ip6Addr, pIpv6RouteDest->pu1_OctetList, sizeof (tIp6Addr));

    MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));

    IP6_TASK_UNLOCK ();
    RTM6_TASK_LOCK ();

    if (Rtm6ApiGetBestRouteEntryInCxt
        (u4ContextId, &Ip6Addr, (UINT1) i4Ipv6RoutePfxLength, u4Ipv6RouteIndex,
         &NetIpv6RtInfo) == RTM6_FAILURE)
    {
        RTM6_TASK_UNLOCK ();
        IP6_TASK_LOCK ();
        return SNMP_FAILURE;
    }

    RTM6_TASK_UNLOCK ();
    IP6_TASK_LOCK ();

    MEMCPY (pRetValIpv6RouteNextHop->pu1_OctetList,
            &NetIpv6RtInfo.NextHop, sizeof (tIp6Addr));

    pRetValIpv6RouteNextHop->i4_Length = IP6_ADDR_SIZE;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIpv6RouteType
 Input       :  The Indices
                Ipv6RouteDest
                Ipv6RoutePfxLength
                Ipv6RouteIndex

                The Object 
                retValIpv6RouteType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpv6RouteType (tSNMP_OCTET_STRING_TYPE * pIpv6RouteDest,
                     INT4 i4Ipv6RoutePfxLength, UINT4 u4Ipv6RouteIndex,
                     INT4 *pi4RetValIpv6RouteType)
{
    tIp6Addr            Ip6Addr;
    tNetIpv6RtInfo      NetIpv6RtInfo;
    UINT4               u4ContextId = VCM_DEFAULT_CONTEXT;

    MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));
    MEMCPY (&Ip6Addr, pIpv6RouteDest->pu1_OctetList, sizeof (tIp6Addr));

    MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));

    IP6_TASK_UNLOCK ();
    RTM6_TASK_LOCK ();

    if (Rtm6ApiGetBestRouteEntryInCxt
        (u4ContextId, &Ip6Addr, (UINT1) i4Ipv6RoutePfxLength, u4Ipv6RouteIndex,
         &NetIpv6RtInfo) == RTM6_FAILURE)
    {
        RTM6_TASK_UNLOCK ();
        IP6_TASK_LOCK ();
        return SNMP_FAILURE;
    }
    RTM6_TASK_UNLOCK ();
    IP6_TASK_LOCK ();

    /* Changes for conforming to the standard mib values */
    *pi4RetValIpv6RouteType = (INT4) NetIpv6RtInfo.i1Type;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIpv6RouteProtocol
 Input       :  The Indices
                Ipv6RouteDest
                Ipv6RoutePfxLength
                Ipv6RouteIndex

                The Object 
                retValIpv6RouteProtocol
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpv6RouteProtocol (tSNMP_OCTET_STRING_TYPE * pIpv6RouteDest,
                         INT4 i4Ipv6RoutePfxLength, UINT4 u4Ipv6RouteIndex,
                         INT4 *pi4RetValIpv6RouteProtocol)
{
    tIp6Addr            Ip6Addr;
    tNetIpv6RtInfo      NetIpv6RtInfo;
    UINT4               u4ContextId = VCM_DEFAULT_CONTEXT;

    MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));
    MEMCPY (&Ip6Addr, pIpv6RouteDest->pu1_OctetList, sizeof (tIp6Addr));

    MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));

    IP6_TASK_UNLOCK ();
    RTM6_TASK_LOCK ();

    if (Rtm6ApiGetBestRouteEntryInCxt
        (u4ContextId, &Ip6Addr, (UINT1) i4Ipv6RoutePfxLength, u4Ipv6RouteIndex,
         &NetIpv6RtInfo) == RTM6_FAILURE)
    {
        RTM6_TASK_UNLOCK ();
        IP6_TASK_LOCK ();
        return SNMP_FAILURE;
    }
    RTM6_TASK_UNLOCK ();
    IP6_TASK_LOCK ();
    *pi4RetValIpv6RouteProtocol = (INT4) NetIpv6RtInfo.i1Proto;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIpv6RoutePolicy
 Input       :  The Indices
                Ipv6RouteDest
                Ipv6RoutePfxLength
                Ipv6RouteIndex

                The Object 
                retValIpv6RoutePolicy
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpv6RoutePolicy (tSNMP_OCTET_STRING_TYPE * pIpv6RouteDest,
                       INT4 i4Ipv6RoutePfxLength, UINT4 u4Ipv6RouteIndex,
                       INT4 *pi4RetValIpv6RoutePolicy)
{
    tIp6Addr            Ip6Addr;
    tNetIpv6RtInfo      NetIpv6RtInfo;
    UINT4               u4ContextId = VCM_DEFAULT_CONTEXT;

    MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));
    MEMCPY (&Ip6Addr, pIpv6RouteDest->pu1_OctetList, sizeof (tIp6Addr));

    MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));

    IP6_TASK_UNLOCK ();
    RTM6_TASK_LOCK ();

    if (Rtm6ApiGetBestRouteEntryInCxt
        (u4ContextId, &Ip6Addr, (UINT1) i4Ipv6RoutePfxLength, u4Ipv6RouteIndex,
         &NetIpv6RtInfo) == RTM6_FAILURE)
    {
        RTM6_TASK_UNLOCK ();
        IP6_TASK_LOCK ();
        return SNMP_FAILURE;
    }
    RTM6_TASK_UNLOCK ();
    IP6_TASK_LOCK ();
    *pi4RetValIpv6RoutePolicy = 0;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIpv6RouteAge
 Input       :  The Indices
                Ipv6RouteDest
                Ipv6RoutePfxLength
                Ipv6RouteIndex

                The Object 
                retValIpv6RouteAge
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpv6RouteAge (tSNMP_OCTET_STRING_TYPE * pIpv6RouteDest,
                    INT4 i4Ipv6RoutePfxLength, UINT4 u4Ipv6RouteIndex,
                    UINT4 *pu4RetValIpv6RouteAge)
{
    tNetIpv6RtInfo      NetIpv6RtInfo;
    tIp6Addr            Ip6Addr;
    UINT4               u4SysTime = 0;
    UINT4               u4ContextId = VCM_DEFAULT_CONTEXT;

    MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));
    MEMCPY (&Ip6Addr, pIpv6RouteDest->pu1_OctetList, sizeof (tIp6Addr));

    MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));

    IP6_TASK_UNLOCK ();
    RTM6_TASK_LOCK ();

    if (Rtm6ApiGetBestRouteEntryInCxt
        (u4ContextId, &Ip6Addr, (UINT1) i4Ipv6RoutePfxLength, u4Ipv6RouteIndex,
         &NetIpv6RtInfo) == RTM6_FAILURE)
    {
        RTM6_TASK_UNLOCK ();
        IP6_TASK_LOCK ();
        return SNMP_FAILURE;
    }
    RTM6_TASK_UNLOCK ();
    IP6_TASK_LOCK ();
    if ((NetIpv6RtInfo.i1Proto == IP6_NETMGMT_PROTOID) ||
        (NetIpv6RtInfo.i1Proto == IP6_LOCAL_PROTOID))
    {
        *pu4RetValIpv6RouteAge = 0;
        return SNMP_SUCCESS;
    }

    u4SysTime = OsixGetSysUpTime ();
    *pu4RetValIpv6RouteAge = (u4SysTime - NetIpv6RtInfo.u4ChangeTime);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIpv6RouteNextHopRDI
 Input       :  The Indices
                Ipv6RouteDest
                Ipv6RoutePfxLength
                Ipv6RouteIndex

                The Object 
                retValIpv6RouteNextHopRDI
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpv6RouteNextHopRDI (tSNMP_OCTET_STRING_TYPE * pIpv6RouteDest,
                           INT4 i4Ipv6RoutePfxLength, UINT4 u4Ipv6RouteIndex,
                           UINT4 *pu4RetValIpv6RouteNextHopRDI)
{
    tIp6Addr            Ip6Addr;
    tNetIpv6RtInfo      NetIpv6RtInfo;
    UINT4               u4ContextId = VCM_DEFAULT_CONTEXT;

    MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));
    MEMCPY (&Ip6Addr, pIpv6RouteDest->pu1_OctetList, sizeof (tIp6Addr));

    MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));

    IP6_TASK_UNLOCK ();
    RTM6_TASK_LOCK ();

    if (Rtm6ApiGetBestRouteEntryInCxt
        (u4ContextId, &Ip6Addr, (UINT1) i4Ipv6RoutePfxLength, u4Ipv6RouteIndex,
         &NetIpv6RtInfo) == RTM6_FAILURE)
    {
        RTM6_TASK_UNLOCK ();
        IP6_TASK_LOCK ();
        return SNMP_FAILURE;
    }
    RTM6_TASK_UNLOCK ();
    IP6_TASK_LOCK ();
    *pu4RetValIpv6RouteNextHopRDI = 0x0;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIpv6RouteMetric
 Input       :  The Indices
                Ipv6RouteDest
                Ipv6RoutePfxLength
                Ipv6RouteIndex

                The Object 
                retValIpv6RouteMetric
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpv6RouteMetric (tSNMP_OCTET_STRING_TYPE * pIpv6RouteDest,
                       INT4 i4Ipv6RoutePfxLength, UINT4 u4Ipv6RouteIndex,
                       UINT4 *pu4RetValIpv6RouteMetric)
{
    tIp6Addr            Ip6Addr;
    tNetIpv6RtInfo      NetIpv6RtInfo;
    UINT4               u4ContextId = VCM_DEFAULT_CONTEXT;

    MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));
    MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));
    MEMCPY (&Ip6Addr, pIpv6RouteDest->pu1_OctetList, sizeof (tIp6Addr));

    IP6_TASK_UNLOCK ();
    RTM6_TASK_LOCK ();

    if (Rtm6ApiGetBestRouteEntryInCxt
        (u4ContextId, &Ip6Addr, (UINT1) i4Ipv6RoutePfxLength, u4Ipv6RouteIndex,
         &NetIpv6RtInfo) == RTM6_FAILURE)

    {
        RTM6_TASK_UNLOCK ();
        IP6_TASK_LOCK ();
        return SNMP_FAILURE;
    }
    RTM6_TASK_UNLOCK ();
    IP6_TASK_LOCK ();
    *pu4RetValIpv6RouteMetric = NetIpv6RtInfo.u4Metric;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIpv6RouteWeight
 Input       :  The Indices
                Ipv6RouteDest
                Ipv6RoutePfxLength
                Ipv6RouteIndex

                The Object 
                retValIpv6RouteWeight
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpv6RouteWeight (tSNMP_OCTET_STRING_TYPE * pIpv6RouteDest,
                       INT4 i4Ipv6RoutePfxLength, UINT4 u4Ipv6RouteIndex,
                       UINT4 *pu4RetValIpv6RouteWeight)
{
    tIp6Addr            Ip6Addr;
    tNetIpv6RtInfo      NetIpv6RtInfo;
    UINT4               u4ContextId = VCM_DEFAULT_CONTEXT;

    MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));
    MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));
    MEMCPY (&Ip6Addr, pIpv6RouteDest->pu1_OctetList, sizeof (tIp6Addr));

    IP6_TASK_UNLOCK ();
    RTM6_TASK_LOCK ();

    if (Rtm6ApiGetBestRouteEntryInCxt
        (u4ContextId, &Ip6Addr, (UINT1) i4Ipv6RoutePfxLength, u4Ipv6RouteIndex,
         &NetIpv6RtInfo) == RTM6_FAILURE)
    {
        RTM6_TASK_UNLOCK ();
        IP6_TASK_LOCK ();
        return SNMP_FAILURE;
    }
    RTM6_TASK_UNLOCK ();
    IP6_TASK_LOCK ();
    *pu4RetValIpv6RouteWeight = 0;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIpv6RouteInfo
 Input       :  The Indices
                Ipv6RouteDest
                Ipv6RoutePfxLength
                Ipv6RouteIndex

                The Object 
                retValIpv6RouteInfo
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpv6RouteInfo (tSNMP_OCTET_STRING_TYPE * pIpv6RouteDest,
                     INT4 i4Ipv6RoutePfxLength, UINT4 u4Ipv6RouteIndex,
                     tSNMP_OID_TYPE * pRetValIpv6RouteInfo)
{
    tIp6Addr            Ip6Addr;
    UINT4               au4Oid[10] = { 0, 0 };
    tNetIpv6RtInfo      NetIpv6RtInfo;
    UINT4               u4ContextId = VCM_DEFAULT_CONTEXT;

    MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));
    MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));
    MEMCPY (&Ip6Addr, pIpv6RouteDest->pu1_OctetList, sizeof (tIp6Addr));

    IP6_TASK_UNLOCK ();
    RTM6_TASK_LOCK ();

    if (Rtm6ApiGetBestRouteEntryInCxt
        (u4ContextId, &Ip6Addr, (UINT1) i4Ipv6RoutePfxLength, u4Ipv6RouteIndex,
         &NetIpv6RtInfo) == RTM6_FAILURE)
    {
        RTM6_TASK_UNLOCK ();
        IP6_TASK_LOCK ();
        return SNMP_FAILURE;
    }
    RTM6_TASK_UNLOCK ();
    IP6_TASK_LOCK ();
    MEMCPY (pRetValIpv6RouteInfo->pu4_OidList, au4Oid, 2 * sizeof (UINT4));
    pRetValIpv6RouteInfo->u4_Length = 2;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIpv6RouteValid
 Input       :  The Indices
                Ipv6RouteDest
                Ipv6RoutePfxLength
                Ipv6RouteIndex

                The Object 
                retValIpv6RouteValid
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpv6RouteValid (tSNMP_OCTET_STRING_TYPE * pIpv6RouteDest,
                      INT4 i4Ipv6RoutePfxLength, UINT4 u4Ipv6RouteIndex,
                      INT4 *pi4RetValIpv6RouteValid)
{
    tIp6Addr            Ip6Addr;
    tNetIpv6RtInfo      NetIpv6RtInfo;
    UINT4               u4ContextId = VCM_DEFAULT_CONTEXT;

    MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));
    MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));
    MEMCPY (&Ip6Addr, pIpv6RouteDest->pu1_OctetList, sizeof (tIp6Addr));

    IP6_TASK_UNLOCK ();
    RTM6_TASK_LOCK ();

    if (Rtm6ApiGetBestRouteEntryInCxt
        (u4ContextId, &Ip6Addr, (UINT1) i4Ipv6RoutePfxLength, u4Ipv6RouteIndex,
         &NetIpv6RtInfo) == RTM6_FAILURE)
    {
        RTM6_TASK_UNLOCK ();
        IP6_TASK_LOCK ();
        return SNMP_FAILURE;
    }
    RTM6_TASK_UNLOCK ();
    IP6_TASK_LOCK ();
    *pi4RetValIpv6RouteValid = TRUE;
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIpv6RouteValid
 Input       :  The Indices
                Ipv6RouteDest
                Ipv6RoutePfxLength
                Ipv6RouteIndex

                The Object 
                setValIpv6RouteValid
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIpv6RouteValid (tSNMP_OCTET_STRING_TYPE * pIpv6RouteDest,
                      INT4 i4Ipv6RoutePfxLength, UINT4 u4Ipv6RouteIndex,
                      INT4 i4SetValIpv6RouteValid)
{
    tIp6Addr           *pDest =
        (tIp6Addr *) (VOID *) pIpv6RouteDest->pu1_OctetList;
    UINT4               u4ContextId = VCM_DEFAULT_CONTEXT;
    tNetIpv6RtInfo      NetIpv6RtInfo;

    IP6_TASK_UNLOCK ();
    RTM6_TASK_LOCK ();

    MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));
    if (Rtm6ApiGetBestRouteEntryInCxt
        (u4ContextId, pDest, (UINT1) i4Ipv6RoutePfxLength, u4Ipv6RouteIndex,
         &NetIpv6RtInfo) == RTM6_SUCCESS)
    {
        /* Route is present. */
        if (i4SetValIpv6RouteValid == NETIPV6_DELETE_ROUTE)
        {
            /* Invalidate the route. */
            NetIpv6RtInfo.u4RowStatus = IP6FWD_DESTROY;
            NetIpv6RtInfo.u2ChgBit = IP6_BIT_STATUS;

            Rtm6Ipv6LeakRoute (NETIPV6_DELETE_ROUTE, &NetIpv6RtInfo);
        }
        RTM6_TASK_UNLOCK ();
        IP6_TASK_LOCK ();
        return SNMP_SUCCESS;
    }
    RTM6_TASK_UNLOCK ();
    IP6_TASK_LOCK ();

    /* No matching route found. */
    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ipv6RouteValid
 Input       :  The Indices
                Ipv6RouteDest
                Ipv6RoutePfxLength
                Ipv6RouteIndex

                The Object 
                testValIpv6RouteValid
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ipv6RouteValid (UINT4 *pu4ErrorCode,
                         tSNMP_OCTET_STRING_TYPE * pIpv6RouteDest,
                         INT4 i4Ipv6RoutePfxLength, UINT4 u4Ipv6RouteIndex,
                         INT4 i4TestValIpv6RouteValid)
{
    tIp6Addr           *pDest =
        (tIp6Addr *) (VOID *) pIpv6RouteDest->pu1_OctetList;
    tNetIpv6RtInfo      NetIpv6RtInfo;
    UINT4               u4ContextId = VCM_DEFAULT_CONTEXT;

    if ((i4TestValIpv6RouteValid != IP6_TRUE) &&
        (i4TestValIpv6RouteValid != IP6_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    IP6_TASK_UNLOCK ();
    RTM6_TASK_LOCK ();
    MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));
    if (Rtm6ApiGetBestRouteEntryInCxt
        (u4ContextId, pDest, (UINT1) i4Ipv6RoutePfxLength, u4Ipv6RouteIndex,
         &NetIpv6RtInfo) == RTM6_FAILURE)
    {
        /* No matching route found. Cant perform any operation for this
         * route. */
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        RTM6_TASK_UNLOCK ();
        IP6_TASK_LOCK ();
        return SNMP_FAILURE;
    }
    RTM6_TASK_UNLOCK ();
    IP6_TASK_LOCK ();
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ipv6RouteTable
 Input       :  The Indices
                Ipv6RouteDest
                Ipv6RoutePfxLength
                Ipv6RouteIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ipv6RouteTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ipv6NetToMediaTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIpv6NetToMediaTable
 Input       :  The Indices
                Ipv6IfIndex
                Ipv6NetToMediaNetAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIpv6NetToMediaTable (INT4 i4Ipv6IfIndex,
                                             tSNMP_OCTET_STRING_TYPE *
                                             pIpv6NetToMediaNetAddress)
{
    if (nmhValidateIndexInstanceFsipv6NdLanCacheTable
        (i4Ipv6IfIndex, pIpv6NetToMediaNetAddress) == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIpv6NetToMediaTable
 Input       :  The Indices
                Ipv6IfIndex
                Ipv6NetToMediaNetAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIpv6NetToMediaTable (INT4 *pi4Ipv6IfIndex,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pIpv6NetToMediaNetAddress)
{
    if (nmhGetFirstIndexFsipv6NdLanCacheTable (pi4Ipv6IfIndex,
                                               pIpv6NetToMediaNetAddress) ==
        SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }

}

/****************************************************************************
 Function    :  nmhGetNextIndexIpv6NetToMediaTable
 Input       :  The Indices
                Ipv6IfIndex
                nextIpv6IfIndex
                Ipv6NetToMediaNetAddress
                nextIpv6NetToMediaNetAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIpv6NetToMediaTable (INT4 i4Ipv6IfIndex,
                                    INT4 *pi4NextIpv6IfIndex,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pIpv6NetToMediaNetAddress,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pNextIpv6NetToMediaNetAddress)
{
    if (nmhGetNextIndexFsipv6NdLanCacheTable (i4Ipv6IfIndex,
                                              pi4NextIpv6IfIndex,
                                              pIpv6NetToMediaNetAddress,
                                              pNextIpv6NetToMediaNetAddress)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIpv6NetToMediaPhysAddress
 Input       :  The Indices
                Ipv6IfIndex
                Ipv6NetToMediaNetAddress

                The Object 
                retValIpv6NetToMediaPhysAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpv6NetToMediaPhysAddress (INT4 i4Ipv6IfIndex,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pIpv6NetToMediaNetAddress,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pRetValIpv6NetToMediaPhysAddress)
{

    /* Same data retrival logic is used in the invoked function */
    if (nmhGetFsipv6NdLanCachePhysAddr (i4Ipv6IfIndex,
                                        pIpv6NetToMediaNetAddress,
                                        pRetValIpv6NetToMediaPhysAddress)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIpv6NetToMediaType
 Input       :  The Indices
                Ipv6IfIndex
                Ipv6NetToMediaNetAddress

                The Object 
                retValIpv6NetToMediaType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpv6NetToMediaType (INT4 i4Ipv6IfIndex,
                          tSNMP_OCTET_STRING_TYPE * pIpv6NetToMediaNetAddress,
                          INT4 *pi4RetValIpv6NetToMediaType)
{
    UNUSED_PARAM (i4Ipv6IfIndex);
    UNUSED_PARAM (pIpv6NetToMediaNetAddress);
    UNUSED_PARAM (pi4RetValIpv6NetToMediaType);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIpv6IfNetToMediaState
 Input       :  The Indices
                Ipv6IfIndex
                Ipv6NetToMediaNetAddress

                The Object 
                retValIpv6IfNetToMediaState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpv6IfNetToMediaState (INT4 i4Ipv6IfIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pIpv6NetToMediaNetAddress,
                             INT4 *pi4RetValIpv6IfNetToMediaState)
{
    UNUSED_PARAM (i4Ipv6IfIndex);
    UNUSED_PARAM (pIpv6NetToMediaNetAddress);
    UNUSED_PARAM (pi4RetValIpv6IfNetToMediaState);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIpv6IfNetToMediaLastUpdated
 Input       :  The Indices
                Ipv6IfIndex
                Ipv6NetToMediaNetAddress

                The Object 
                retValIpv6IfNetToMediaLastUpdated
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpv6IfNetToMediaLastUpdated (INT4 i4Ipv6IfIndex,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pIpv6NetToMediaNetAddress,
                                   UINT4 *pu4RetValIpv6IfNetToMediaLastUpdated)
{
    UNUSED_PARAM (i4Ipv6IfIndex);
    UNUSED_PARAM (pIpv6NetToMediaNetAddress);
    UNUSED_PARAM (pu4RetValIpv6IfNetToMediaLastUpdated);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIpv6NetToMediaValid
 Input       :  The Indices
                Ipv6IfIndex
                Ipv6NetToMediaNetAddress

                The Object 
                retValIpv6NetToMediaValid
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpv6NetToMediaValid (INT4 i4Ipv6IfIndex,
                           tSNMP_OCTET_STRING_TYPE * pIpv6NetToMediaNetAddress,
                           INT4 *pi4RetValIpv6NetToMediaValid)
{
    UNUSED_PARAM (i4Ipv6IfIndex);
    UNUSED_PARAM (pIpv6NetToMediaNetAddress);
    UNUSED_PARAM (pi4RetValIpv6NetToMediaValid);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIpv6NetToMediaValid
 Input       :  The Indices
                Ipv6IfIndex
                Ipv6NetToMediaNetAddress

                The Object 
                setValIpv6NetToMediaValid
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIpv6NetToMediaValid (INT4 i4Ipv6IfIndex,
                           tSNMP_OCTET_STRING_TYPE * pIpv6NetToMediaNetAddress,
                           INT4 i4SetValIpv6NetToMediaValid)
{
    UNUSED_PARAM (i4Ipv6IfIndex);
    UNUSED_PARAM (pIpv6NetToMediaNetAddress);
    UNUSED_PARAM (i4SetValIpv6NetToMediaValid);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ipv6NetToMediaValid
 Input       :  The Indices
                Ipv6IfIndex
                Ipv6NetToMediaNetAddress

                The Object 
                testValIpv6NetToMediaValid
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ipv6NetToMediaValid (UINT4 *pu4ErrorCode, INT4 i4Ipv6IfIndex,
                              tSNMP_OCTET_STRING_TYPE *
                              pIpv6NetToMediaNetAddress,
                              INT4 i4TestValIpv6NetToMediaValid)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4Ipv6IfIndex);
    UNUSED_PARAM (pIpv6NetToMediaNetAddress);
    UNUSED_PARAM (i4TestValIpv6NetToMediaValid);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ipv6NetToMediaTable
 Input       :  The Indices
                Ipv6IfIndex
                Ipv6NetToMediaNetAddress
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ipv6NetToMediaTable (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIpv6Forwarding
 Input       :  The Indices

                The Object 
                retValIpv6Forwarding
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpv6Forwarding (INT4 *pi4RetValIpv6Forwarding)
{
    UINT4                u4ContextId = VCM_DEFAULT_CONTEXT;
    tLip6Cxt            *pIp6Cxt = NULL;

#if defined(VRF_WANTED) && defined(LINUX_310_WANTED)
    u4ContextId = Lip6GetCurrentContext();
#endif
    if(u4ContextId == VCM_DEFAULT_CONTEXT)
    {
	    *pi4RetValIpv6Forwarding = gIp6GblInfo.i4Ip6Status;
    }
    else
    {
	    pIp6Cxt = gIp6GblInfo.apIp6Cxt[u4ContextId];
	    if (pIp6Cxt != NULL )
	    {
		    *pi4RetValIpv6Forwarding = (INT4)pIp6Cxt->u4ForwFlag;
	    }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIpv6DefaultHopLimit
 Input       :  The Indices

                The Object 
                retValIpv6DefaultHopLimit
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpv6DefaultHopLimit (INT4 *pi4RetValIpv6DefaultHopLimit)
{
    *pi4RetValIpv6DefaultHopLimit = (INT4) gIp6GblInfo.u4DefHopLimit;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIpv6Interfaces
 Input       :  The Indices

                The Object 
                retValIpv6Interfaces
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpv6Interfaces (UINT4 *pu4RetValIpv6Interfaces)
{
    UINT2               u2Index = 0, u2NumInterfaces = 0;
    tLip6If            *pIf6Entry = NULL;
    for (u2Index = 1; u2Index <= IP6_MAX_LOGICAL_IFACES; u2Index++)
    {
        pIf6Entry = gIp6GblInfo.apIp6If[u2Index];
        if (pIf6Entry != NULL)
        {
            u2NumInterfaces++;
        }
    }
    *pu4RetValIpv6Interfaces = u2NumInterfaces;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIpv6IfTableLastChange
 Input       :  The Indices

                The Object 
                retValIpv6IfTableLastChange
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpv6IfTableLastChange (UINT4 *pu4RetValIpv6IfTableLastChange)
{

    *pu4RetValIpv6IfTableLastChange = gIp6GblInfo.u4Ip6IfTableLastChange;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIpv6RouteNumber
 Input       :  The Indices

                The Object 
                retValIpv6RouteNumber
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpv6RouteNumber (UINT4 *pu4RetValIpv6RouteNumber)
{
    UINT4               u4ContextId = VCM_DEFAULT_CONTEXT;

    Ip6GetFwdTableRouteNumInCxt (u4ContextId, pu4RetValIpv6RouteNumber);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIpv6DiscardedRoutes
 Input       :  The Indices

                The Object 
                retValIpv6DiscardedRoutes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpv6DiscardedRoutes (UINT4 *pu4RetValIpv6DiscardedRoutes)
{

    *pu4RetValIpv6DiscardedRoutes = 0;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIpv6Forwarding
 Input       :  The Indices

                The Object 
                setValIpv6Forwarding
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIpv6Forwarding (INT4 i4SetValIpv6Forwarding)
{
    UINT4           u4ContextId = VCM_DEFAULT_CONTEXT;
    tLip6Cxt        *pIp6Cxt = NULL;
#if defined(VRF_WANTED) && defined(LINUX_310_WANTED)
    u4ContextId = Lip6GetCurrentContext();
#endif
    if (u4ContextId == VCM_DEFAULT_CONTEXT)
    {
	    if (gIp6GblInfo.i4Ip6Status == i4SetValIpv6Forwarding)
	    {
		    /* No change in forwarding Status. */
		    return SNMP_SUCCESS;
	    }
    }
    else
    {
	    pIp6Cxt = gIp6GblInfo.apIp6Cxt[u4ContextId];
	    if (  pIp6Cxt != NULL && pIp6Cxt->u4ForwFlag == (UINT4) i4SetValIpv6Forwarding)
	    {
		    /* No change in forwarding Status. */
		    return SNMP_SUCCESS;
	    }
    }
    Lip6KernSetForwarding (i4SetValIpv6Forwarding);
    Lip6SetIpv6ForwardingGlobal ((UINT4)i4SetValIpv6Forwarding);

#ifdef NPAPI_WANTED
    if (Ipv6FsNpIpv6UcRouting
        (u4ContextId, (UINT4) i4SetValIpv6Forwarding) == FNP_FAILURE)
    {
        return SNMP_FAILURE;
    }
#endif
    Rtm6ApiAddOrDelAllRtsInCxt(u4ContextId,i4SetValIpv6Forwarding);
    IncMsrForIpvxGlbTable (u4ContextId, i4SetValIpv6Forwarding,
                           FsMIStdIpv6IpForwarding,
                           (sizeof (FsMIStdIpv6IpForwarding) / sizeof (UINT4)));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIpv6DefaultHopLimit
 Input       :  The Indices

                The Object 
                setValIpv6DefaultHopLimit
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIpv6DefaultHopLimit (INT4 i4SetValIpv6DefaultHopLimit)
{

    if (Lip6KernSetDefaultHopLimit (i4SetValIpv6DefaultHopLimit) !=
        OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    gIp6GblInfo.u4DefHopLimit = i4SetValIpv6DefaultHopLimit;

    IncMsrForIpvxGlbTable (VCM_DEFAULT_CONTEXT, i4SetValIpv6DefaultHopLimit,
                           FsMIStdIpv6IpDefaultHopLimit,
                           (sizeof (FsMIStdIpv6IpDefaultHopLimit) /
                            sizeof (UINT4)));
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ipv6Forwarding
 Input       :  The Indices

                The Object 
                testValIpv6Forwarding
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ipv6Forwarding (UINT4 *pu4ErrorCode, INT4 i4TestValIpv6Forwarding)
{
    if ((i4TestValIpv6Forwarding != LIP6_FORW_ENABLE) &&
        (i4TestValIpv6Forwarding != LIP6_FORW_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_INVALID_FORWARDING_STATUS);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Ipv6DefaultHopLimit
 Input       :  The Indices

                The Object 
                testValIpv6DefaultHopLimit
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ipv6DefaultHopLimit (UINT4 *pu4ErrorCode,
                              INT4 i4TestValIpv6DefaultHopLimit)
{
    if ((i4TestValIpv6DefaultHopLimit < IP6_MIN_HOP_LIMIT)
        || (i4TestValIpv6DefaultHopLimit > IP6_MAX_HOP_LIMIT))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ipv6Forwarding
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ipv6Forwarding (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Ipv6DefaultHopLimit
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ipv6DefaultHopLimit (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  Ip6GetRtrAdvtLinkMTU
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
Ip6GetRtrAdvtLinkMTU (INT4 i4Ipv6IfIndex,
                      UINT4 *pu4RetValIpv6RouterAdvertLinkMTU)
{
    tLip6If            *pIf6Entry = NULL;

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Ipv6IfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValIpv6RouterAdvertLinkMTU = pIf6Entry->u4RaMtu;

    return SNMP_SUCCESS;

}

/***************************** END OF FILE **********************************/
