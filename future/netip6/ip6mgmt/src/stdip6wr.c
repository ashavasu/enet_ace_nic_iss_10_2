
 /* $Id: stdip6wr.c,v 1.15.26.1 2018/03/13 14:12:10 siva Exp $ */
#include "lr.h"
#include "fssnmp.h"
#include "stdip6wr.h"
#include "stdip6db.h"
#ifndef LNXIP6_WANTED            /* FSIP */
#include "ip6inc.h"
#else
#include "lip6mgin.h"
#endif
#include "ip6cli.h"
#include "fsmsipcli.h"
#include "ipv6.h"

VOID
RegisterSTDIP6 ()
{
    SNMPRegisterMibWithContextIdAndLock (&stdip6OID, &stdip6Entry,
                                         Ip6Lock, Ip6UnLock,
                                         Ip6SelectContext, Ip6ReleaseContext,
                                         SNMP_MSR_TGR_FALSE);
    SNMPAddSysorEntry (&stdip6OID, (const UINT1 *) "ipv6MIB");
}

INT4
Ipv6ForwardingTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                    tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2Ipv6Forwarding (pu4Error, pMultiData->i4_SLongValue));
}

INT4
Ipv6ForwardingSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIpv6Forwarding (pMultiData->i4_SLongValue));
}

INT4
Ipv6ForwardingGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIpv6Forwarding (&pMultiData->i4_SLongValue));
}

INT4
Ipv6DefaultHopLimitTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2Ipv6DefaultHopLimit (pu4Error, pMultiData->i4_SLongValue));
}

INT4
Ipv6ForwardingDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                   tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Ipv6Forwarding (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
Ipv6DefaultHopLimitDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Ipv6DefaultHopLimit
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
Ipv6DefaultHopLimitSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIpv6DefaultHopLimit (pMultiData->i4_SLongValue));
}

INT4
Ipv6DefaultHopLimitGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIpv6DefaultHopLimit (&pMultiData->i4_SLongValue));
}

INT4
Ipv6InterfacesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIpv6Interfaces (&pMultiData->u4_ULongValue));
}

INT4
Ipv6IfTableLastChangeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIpv6IfTableLastChange (&pMultiData->u4_ULongValue));
}

INT4
Ipv6IfIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceIpv6IfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pMultiData->i4_SLongValue = pMultiIndex->pIndex[0].i4_SLongValue;
    return SNMP_SUCCESS;
}

INT4
Ipv6IfDescrTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                 tRetVal * pMultiData)
{

    return (nmhTestv2Ipv6IfDescr (pu4Error,
                                  pMultiIndex->pIndex[0].i4_SLongValue,
                                  pMultiData->pOctetStrValue));
}

INT4
Ipv6IfDescrSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    return (nmhSetIpv6IfDescr (pMultiIndex->pIndex[0].i4_SLongValue,
                               pMultiData->pOctetStrValue));
}

INT4
Ipv6IfDescrGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceIpv6IfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpv6IfDescr (pMultiIndex->pIndex[0].i4_SLongValue,
                               pMultiData->pOctetStrValue));
}

INT4
Ipv6IfLowerLayerGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceIpv6IfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpv6IfLowerLayer (pMultiIndex->pIndex[0].i4_SLongValue,
                                    pMultiData->pOidValue));
}

INT4
Ipv6IfEffectiveMtuGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceIpv6IfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpv6IfEffectiveMtu (pMultiIndex->pIndex[0].i4_SLongValue,
                                      &pMultiData->u4_ULongValue));
}

INT4
Ipv6IfReasmMaxSizeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceIpv6IfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpv6IfReasmMaxSize (pMultiIndex->pIndex[0].i4_SLongValue,
                                      &pMultiData->u4_ULongValue));
}

INT4
Ipv6IfIdentifierTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{

    return (nmhTestv2Ipv6IfIdentifier (pu4Error,
                                       pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiData->pOctetStrValue));
}

INT4
Ipv6IfIdentifierSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    return (nmhSetIpv6IfIdentifier (pMultiIndex->pIndex[0].i4_SLongValue,
                                    pMultiData->pOctetStrValue));
}

INT4
Ipv6IfIdentifierGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceIpv6IfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpv6IfIdentifier (pMultiIndex->pIndex[0].i4_SLongValue,
                                    pMultiData->pOctetStrValue));
}

INT4
Ipv6IfIdentifierLengthTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{

    return (nmhTestv2Ipv6IfIdentifierLength (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             i4_SLongValue,
                                             pMultiData->i4_SLongValue));
}

INT4
Ipv6IfIdentifierLengthSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    return (nmhSetIpv6IfIdentifierLength (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiData->i4_SLongValue));
}

INT4
Ipv6IfIdentifierLengthGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceIpv6IfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpv6IfIdentifierLength (pMultiIndex->pIndex[0].i4_SLongValue,
                                          &pMultiData->i4_SLongValue));
}

INT4
Ipv6IfPhysicalAddressGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceIpv6IfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpv6IfPhysicalAddress (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->pOctetStrValue));
}

INT4
Ipv6IfAdminStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{

    return (nmhTestv2Ipv6IfAdminStatus (pu4Error,
                                        pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiData->i4_SLongValue));
}

INT4
Ipv6IfTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Ipv6IfTable (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
Ipv6IfAdminStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    return (nmhSetIpv6IfAdminStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiData->i4_SLongValue));
}

INT4
Ipv6IfAdminStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceIpv6IfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpv6IfAdminStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                     &pMultiData->i4_SLongValue));
}

INT4
Ipv6IfOperStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceIpv6IfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpv6IfOperStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                    &pMultiData->i4_SLongValue));
}

INT4
Ipv6IfLastChangeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceIpv6IfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpv6IfLastChange (pMultiIndex->pIndex[0].i4_SLongValue,
                                    &pMultiData->u4_ULongValue));
}

INT4
Ipv6IfStatsInReceivesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceIpv6IfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpv6IfStatsInReceives (pMultiIndex->pIndex[0].i4_SLongValue,
                                         &pMultiData->u4_ULongValue));
}

INT4
Ipv6IfStatsInHdrErrorsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceIpv6IfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpv6IfStatsInHdrErrors (pMultiIndex->pIndex[0].i4_SLongValue,
                                          &pMultiData->u4_ULongValue));
}

INT4
Ipv6IfStatsInTooBigErrorsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceIpv6IfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpv6IfStatsInTooBigErrors
            (pMultiIndex->pIndex[0].i4_SLongValue, &pMultiData->u4_ULongValue));
}

INT4
Ipv6IfStatsInNoRoutesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceIpv6IfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpv6IfStatsInNoRoutes (pMultiIndex->pIndex[0].i4_SLongValue,
                                         &pMultiData->u4_ULongValue));
}

INT4
Ipv6IfStatsInAddrErrorsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceIpv6IfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpv6IfStatsInAddrErrors (pMultiIndex->pIndex[0].i4_SLongValue,
                                           &pMultiData->u4_ULongValue));
}

INT4
Ipv6IfStatsInUnknownProtosGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceIpv6IfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpv6IfStatsInUnknownProtos
            (pMultiIndex->pIndex[0].i4_SLongValue, &pMultiData->u4_ULongValue));
}

INT4
Ipv6IfStatsInTruncatedPktsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceIpv6IfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpv6IfStatsInTruncatedPkts
            (pMultiIndex->pIndex[0].i4_SLongValue, &pMultiData->u4_ULongValue));
}

INT4
Ipv6IfStatsInDiscardsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceIpv6IfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpv6IfStatsInDiscards (pMultiIndex->pIndex[0].i4_SLongValue,
                                         &pMultiData->u4_ULongValue));
}

INT4
Ipv6IfStatsInDeliversGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceIpv6IfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpv6IfStatsInDelivers (pMultiIndex->pIndex[0].i4_SLongValue,
                                         &pMultiData->u4_ULongValue));
}

INT4
Ipv6IfStatsOutForwDatagramsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceIpv6IfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpv6IfStatsOutForwDatagrams
            (pMultiIndex->pIndex[0].i4_SLongValue, &pMultiData->u4_ULongValue));
}

INT4
Ipv6IfStatsOutRequestsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceIpv6IfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpv6IfStatsOutRequests (pMultiIndex->pIndex[0].i4_SLongValue,
                                          &pMultiData->u4_ULongValue));
}

INT4
Ipv6IfStatsOutDiscardsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceIpv6IfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpv6IfStatsOutDiscards (pMultiIndex->pIndex[0].i4_SLongValue,
                                          &pMultiData->u4_ULongValue));
}

INT4
Ipv6IfStatsOutFragOKsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceIpv6IfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpv6IfStatsOutFragOKs (pMultiIndex->pIndex[0].i4_SLongValue,
                                         &pMultiData->u4_ULongValue));
}

INT4
Ipv6IfStatsOutFragFailsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceIpv6IfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpv6IfStatsOutFragFails (pMultiIndex->pIndex[0].i4_SLongValue,
                                           &pMultiData->u4_ULongValue));
}

INT4
Ipv6IfStatsOutFragCreatesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceIpv6IfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpv6IfStatsOutFragCreates
            (pMultiIndex->pIndex[0].i4_SLongValue, &pMultiData->u4_ULongValue));
}

INT4
Ipv6IfStatsReasmReqdsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceIpv6IfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpv6IfStatsReasmReqds (pMultiIndex->pIndex[0].i4_SLongValue,
                                         &pMultiData->u4_ULongValue));
}

INT4
Ipv6IfStatsReasmOKsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceIpv6IfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpv6IfStatsReasmOKs (pMultiIndex->pIndex[0].i4_SLongValue,
                                       &pMultiData->u4_ULongValue));
}

INT4
Ipv6IfStatsReasmFailsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceIpv6IfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpv6IfStatsReasmFails (pMultiIndex->pIndex[0].i4_SLongValue,
                                         &pMultiData->u4_ULongValue));
}

INT4
Ipv6IfStatsInMcastPktsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceIpv6IfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpv6IfStatsInMcastPkts (pMultiIndex->pIndex[0].i4_SLongValue,
                                          &pMultiData->u4_ULongValue));
}

INT4
Ipv6IfStatsOutMcastPktsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceIpv6IfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpv6IfStatsOutMcastPkts (pMultiIndex->pIndex[0].i4_SLongValue,
                                           &pMultiData->u4_ULongValue));
}

INT4
Ipv6AddrPrefixGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceIpv6AddrPrefixTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MEMCPY (pMultiData->pOctetStrValue->pu1_OctetList,
            pMultiIndex->pIndex[1].pOctetStrValue->pu1_OctetList,
            pMultiIndex->pIndex[1].pOctetStrValue->i4_Length);
    pMultiData->pOctetStrValue->i4_Length =
        pMultiIndex->pIndex[1].pOctetStrValue->i4_Length;
    return SNMP_SUCCESS;
}

INT4
Ipv6AddrPrefixLengthGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceIpv6AddrPrefixTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pMultiData->i4_SLongValue = pMultiIndex->pIndex[2].i4_SLongValue;
    return SNMP_SUCCESS;
}

INT4
Ipv6AddrPrefixOnLinkFlagGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceIpv6AddrPrefixTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpv6AddrPrefixOnLinkFlag
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiIndex->pIndex[2].i4_SLongValue, &pMultiData->i4_SLongValue));
}

INT4
Ipv6AddrPrefixAutonomousFlagGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceIpv6AddrPrefixTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpv6AddrPrefixAutonomousFlag
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiIndex->pIndex[2].i4_SLongValue, &pMultiData->i4_SLongValue));
}

INT4
Ipv6AddrPrefixAdvPreferredLifetimeGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceIpv6AddrPrefixTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpv6AddrPrefixAdvPreferredLifetime
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiIndex->pIndex[2].i4_SLongValue, &pMultiData->u4_ULongValue));
}

INT4
Ipv6AddrPrefixAdvValidLifetimeGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceIpv6AddrPrefixTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpv6AddrPrefixAdvValidLifetime
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiIndex->pIndex[2].i4_SLongValue, &pMultiData->u4_ULongValue));
}

INT4
Ipv6AddrAddressGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceIpv6AddrTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MEMCPY (pMultiData->pOctetStrValue->pu1_OctetList,
            pMultiIndex->pIndex[1].pOctetStrValue->pu1_OctetList,
            pMultiIndex->pIndex[1].pOctetStrValue->i4_Length);
    pMultiData->pOctetStrValue->i4_Length =
        pMultiIndex->pIndex[1].pOctetStrValue->i4_Length;
    return SNMP_SUCCESS;
}

INT4
Ipv6AddrPfxLengthGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceIpv6AddrTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpv6AddrPfxLength (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiIndex->pIndex[1].pOctetStrValue,
                                     &pMultiData->i4_SLongValue));
}

INT4
Ipv6AddrTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceIpv6AddrTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpv6AddrType (pMultiIndex->pIndex[0].i4_SLongValue,
                                pMultiIndex->pIndex[1].pOctetStrValue,
                                &pMultiData->i4_SLongValue));
}

INT4
Ipv6AddrAnycastFlagGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceIpv6AddrTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpv6AddrAnycastFlag (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].pOctetStrValue,
                                       &pMultiData->i4_SLongValue));
}

INT4
Ipv6AddrStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceIpv6AddrTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpv6AddrStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                  pMultiIndex->pIndex[1].pOctetStrValue,
                                  &pMultiData->i4_SLongValue));
}

INT4
Ipv6RouteNumberGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIpv6RouteNumber (&pMultiData->u4_ULongValue));
}

INT4
Ipv6DiscardedRoutesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIpv6DiscardedRoutes (&pMultiData->u4_ULongValue));
}

INT4
Ipv6RouteDestGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceIpv6RouteTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MEMCPY (pMultiData->pOctetStrValue->pu1_OctetList,
            pMultiIndex->pIndex[0].pOctetStrValue->pu1_OctetList,
            pMultiIndex->pIndex[0].pOctetStrValue->i4_Length);
    pMultiData->pOctetStrValue->i4_Length =
        pMultiIndex->pIndex[0].pOctetStrValue->i4_Length;
    return SNMP_SUCCESS;
}

INT4
Ipv6RoutePfxLengthGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceIpv6RouteTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pMultiData->i4_SLongValue = pMultiIndex->pIndex[1].i4_SLongValue;
    return SNMP_SUCCESS;
}

INT4
Ipv6RouteIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceIpv6RouteTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pMultiData->u4_ULongValue = pMultiIndex->pIndex[2].u4_ULongValue;
    return SNMP_SUCCESS;
}

INT4
Ipv6RouteIfIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceIpv6RouteTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpv6RouteIfIndex (pMultiIndex->pIndex[0].pOctetStrValue,
                                    pMultiIndex->pIndex[1].i4_SLongValue,
                                    pMultiIndex->pIndex[2].u4_ULongValue,
                                    &pMultiData->i4_SLongValue));
}

INT4
Ipv6RouteNextHopGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceIpv6RouteTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpv6RouteNextHop (pMultiIndex->pIndex[0].pOctetStrValue,
                                    pMultiIndex->pIndex[1].i4_SLongValue,
                                    pMultiIndex->pIndex[2].u4_ULongValue,
                                    pMultiData->pOctetStrValue));
}

INT4
Ipv6RouteTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceIpv6RouteTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpv6RouteType (pMultiIndex->pIndex[0].pOctetStrValue,
                                 pMultiIndex->pIndex[1].i4_SLongValue,
                                 pMultiIndex->pIndex[2].u4_ULongValue,
                                 &pMultiData->i4_SLongValue));
}

INT4
Ipv6RouteProtocolGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceIpv6RouteTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpv6RouteProtocol (pMultiIndex->pIndex[0].pOctetStrValue,
                                     pMultiIndex->pIndex[1].i4_SLongValue,
                                     pMultiIndex->pIndex[2].u4_ULongValue,
                                     &pMultiData->i4_SLongValue));
}

INT4
Ipv6RoutePolicyGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceIpv6RouteTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpv6RoutePolicy (pMultiIndex->pIndex[0].pOctetStrValue,
                                   pMultiIndex->pIndex[1].i4_SLongValue,
                                   pMultiIndex->pIndex[2].u4_ULongValue,
                                   &pMultiData->i4_SLongValue));
}

INT4
Ipv6RouteAgeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceIpv6RouteTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpv6RouteAge (pMultiIndex->pIndex[0].pOctetStrValue,
                                pMultiIndex->pIndex[1].i4_SLongValue,
                                pMultiIndex->pIndex[2].u4_ULongValue,
                                &pMultiData->u4_ULongValue));
}

INT4
Ipv6RouteNextHopRDIGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceIpv6RouteTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpv6RouteNextHopRDI (pMultiIndex->pIndex[0].pOctetStrValue,
                                       pMultiIndex->pIndex[1].i4_SLongValue,
                                       pMultiIndex->pIndex[2].u4_ULongValue,
                                       &pMultiData->u4_ULongValue));
}

INT4
Ipv6RouteMetricGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceIpv6RouteTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpv6RouteMetric (pMultiIndex->pIndex[0].pOctetStrValue,
                                   pMultiIndex->pIndex[1].i4_SLongValue,
                                   pMultiIndex->pIndex[2].u4_ULongValue,
                                   &pMultiData->u4_ULongValue));
}

INT4
Ipv6RouteWeightGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceIpv6RouteTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpv6RouteWeight (pMultiIndex->pIndex[0].pOctetStrValue,
                                   pMultiIndex->pIndex[1].i4_SLongValue,
                                   pMultiIndex->pIndex[2].u4_ULongValue,
                                   &pMultiData->u4_ULongValue));
}

INT4
Ipv6RouteInfoGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceIpv6RouteTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpv6RouteInfo (pMultiIndex->pIndex[0].pOctetStrValue,
                                 pMultiIndex->pIndex[1].i4_SLongValue,
                                 pMultiIndex->pIndex[2].u4_ULongValue,
                                 pMultiData->pOidValue));
}

INT4
Ipv6RouteValidTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                    tRetVal * pMultiData)
{

    return (nmhTestv2Ipv6RouteValid (pu4Error,
                                     pMultiIndex->pIndex[0].pOctetStrValue,
                                     pMultiIndex->pIndex[1].i4_SLongValue,
                                     pMultiIndex->pIndex[2].u4_ULongValue,
                                     pMultiData->i4_SLongValue));
}

INT4
Ipv6RouteTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                   tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Ipv6RouteTable (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
Ipv6RouteValidSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    return (nmhSetIpv6RouteValid (pMultiIndex->pIndex[0].pOctetStrValue,
                                  pMultiIndex->pIndex[1].i4_SLongValue,
                                  pMultiIndex->pIndex[2].u4_ULongValue,
                                  pMultiData->i4_SLongValue));
}

INT4
Ipv6RouteValidGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceIpv6RouteTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpv6RouteValid (pMultiIndex->pIndex[0].pOctetStrValue,
                                  pMultiIndex->pIndex[1].i4_SLongValue,
                                  pMultiIndex->pIndex[2].u4_ULongValue,
                                  &pMultiData->i4_SLongValue));
}

INT4
Ipv6NetToMediaNetAddressGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceIpv6NetToMediaTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MEMCPY (pMultiData->pOctetStrValue->pu1_OctetList,
            pMultiIndex->pIndex[1].pOctetStrValue->pu1_OctetList,
            pMultiIndex->pIndex[1].pOctetStrValue->i4_Length);
    pMultiData->pOctetStrValue->i4_Length =
        pMultiIndex->pIndex[1].pOctetStrValue->i4_Length;
    return SNMP_SUCCESS;
}

INT4
Ipv6NetToMediaPhysAddressGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceIpv6NetToMediaTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpv6NetToMediaPhysAddress
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiData->pOctetStrValue));
}

INT4
Ipv6NetToMediaTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceIpv6NetToMediaTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpv6NetToMediaType (pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiIndex->pIndex[1].pOctetStrValue,
                                      &pMultiData->i4_SLongValue));
}

INT4
Ipv6IfNetToMediaStateGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceIpv6NetToMediaTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpv6IfNetToMediaState (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiIndex->pIndex[1].pOctetStrValue,
                                         &pMultiData->i4_SLongValue));
}

INT4
Ipv6IfNetToMediaLastUpdatedGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceIpv6NetToMediaTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpv6IfNetToMediaLastUpdated
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             &pMultiData->u4_ULongValue));
}

INT4
Ipv6NetToMediaValidTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{

    return (nmhTestv2Ipv6NetToMediaValid (pu4Error,
                                          pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiIndex->pIndex[1].pOctetStrValue,
                                          pMultiData->i4_SLongValue));
}

INT4
Ipv6NetToMediaTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Ipv6NetToMediaTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
Ipv6NetToMediaValidSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    return (nmhSetIpv6NetToMediaValid (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].pOctetStrValue,
                                       pMultiData->i4_SLongValue));
}

INT4
Ipv6NetToMediaValidGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceIpv6NetToMediaTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpv6NetToMediaValid (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].pOctetStrValue,
                                       &pMultiData->i4_SLongValue));
}

INT4
GetNextIndexIpv6NetToMediaTable (tSnmpIndex * pFirstMultiIndex,
                                 tSnmpIndex * pNextMultiIndex)
{
    INT4                i4ipv6IfIndex;
    tSNMP_OCTET_STRING_TYPE *p1ipv6NetToMediaNetAddress;
    p1ipv6NetToMediaNetAddress = pNextMultiIndex->pIndex[1].pOctetStrValue;    /* not-accessible */
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIpv6NetToMediaTable (&i4ipv6IfIndex,
                                                 p1ipv6NetToMediaNetAddress)
            == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIpv6NetToMediaTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue, &i4ipv6IfIndex,
             pFirstMultiIndex->pIndex[1].pOctetStrValue,
             p1ipv6NetToMediaNetAddress) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    pNextMultiIndex->pIndex[0].i4_SLongValue = i4ipv6IfIndex;
    return SNMP_SUCCESS;
}

INT4
GetNextIndexIpv6RouteTable (tSnmpIndex * pFirstMultiIndex,
                            tSnmpIndex * pNextMultiIndex)
{
    tSNMP_OCTET_STRING_TYPE *p1ipv6RouteDest;
    INT4                i4ipv6RoutePfxLength;
    UINT4               u4ipv6RouteIndex;
    p1ipv6RouteDest = pNextMultiIndex->pIndex[0].pOctetStrValue;    /* not-accessible */
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIpv6RouteTable (p1ipv6RouteDest,
                                            &i4ipv6RoutePfxLength,
                                            &u4ipv6RouteIndex) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIpv6RouteTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue, p1ipv6RouteDest,
             pFirstMultiIndex->pIndex[1].i4_SLongValue, &i4ipv6RoutePfxLength,
             pFirstMultiIndex->pIndex[2].u4_ULongValue,
             &u4ipv6RouteIndex) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    pNextMultiIndex->pIndex[1].i4_SLongValue = i4ipv6RoutePfxLength;
    pNextMultiIndex->pIndex[2].u4_ULongValue = u4ipv6RouteIndex;
    return SNMP_SUCCESS;
}

INT4
GetNextIndexIpv6AddrTable (tSnmpIndex * pFirstMultiIndex,
                           tSnmpIndex * pNextMultiIndex)
{
    INT4                i4ipv6IfIndex;
    tSNMP_OCTET_STRING_TYPE *p1ipv6AddrAddress;
    p1ipv6AddrAddress = pNextMultiIndex->pIndex[1].pOctetStrValue;    /* not-accessible */
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIpv6AddrTable (&i4ipv6IfIndex,
                                           p1ipv6AddrAddress) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIpv6AddrTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue, &i4ipv6IfIndex,
             pFirstMultiIndex->pIndex[1].pOctetStrValue,
             p1ipv6AddrAddress) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    pNextMultiIndex->pIndex[0].i4_SLongValue = i4ipv6IfIndex;
    return SNMP_SUCCESS;
}

INT4
GetNextIndexIpv6AddrPrefixTable (tSnmpIndex * pFirstMultiIndex,
                                 tSnmpIndex * pNextMultiIndex)
{
    INT4                i4ipv6IfIndex;
    tSNMP_OCTET_STRING_TYPE *p1ipv6AddrPrefix;
    INT4                i4ipv6AddrPrefixLength;
    p1ipv6AddrPrefix = pNextMultiIndex->pIndex[1].pOctetStrValue;    /* not-accessible */
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIpv6AddrPrefixTable (&i4ipv6IfIndex,
                                                 p1ipv6AddrPrefix,
                                                 &i4ipv6AddrPrefixLength)
            == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIpv6AddrPrefixTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue, &i4ipv6IfIndex,
             pFirstMultiIndex->pIndex[1].pOctetStrValue, p1ipv6AddrPrefix,
             pFirstMultiIndex->pIndex[2].i4_SLongValue,
             &i4ipv6AddrPrefixLength) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    pNextMultiIndex->pIndex[0].i4_SLongValue = i4ipv6IfIndex;
    pNextMultiIndex->pIndex[2].i4_SLongValue = i4ipv6AddrPrefixLength;
    return SNMP_SUCCESS;
}

INT4
GetNextIndexIpv6IfStatsTable (tSnmpIndex * pFirstMultiIndex,
                              tSnmpIndex * pNextMultiIndex)
{
    INT4                i4ipv6IfIndex;
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIpv6IfStatsTable (&i4ipv6IfIndex) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIpv6IfStatsTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &i4ipv6IfIndex) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    pNextMultiIndex->pIndex[0].i4_SLongValue = i4ipv6IfIndex;
    return SNMP_SUCCESS;
}

INT4
GetNextIndexIpv6IfTable (tSnmpIndex * pFirstMultiIndex,
                         tSnmpIndex * pNextMultiIndex)
{
    INT4                i4ipv6IfIndex;
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIpv6IfTable (&i4ipv6IfIndex) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIpv6IfTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &i4ipv6IfIndex) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    pNextMultiIndex->pIndex[0].i4_SLongValue = i4ipv6IfIndex;
    return SNMP_SUCCESS;
}

INT4
SetIpv6IfAddress (INT4 i4Fsipv6AddrIndex,
                  tIp6Addr Ip6Addr, INT4 i4Fsipv6AddrPrefixLen)
{
    UINT4               u4ErrorCode;
    tSNMP_OCTET_STRING_TYPE Addr;
    tIp6Addr            TempAddr;
    INT4                i4AddrType;

    if (nmhTestv2Fsipv6IfAdminStatus
        (&u4ErrorCode, i4Fsipv6AddrIndex, NETIPV6_ADMIN_UP) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (nmhSetFsipv6IfAdminStatus
        (i4Fsipv6AddrIndex, NETIPV6_ADMIN_UP) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MEMSET (&TempAddr, 0, sizeof (tIp6Addr));

    Addr.pu1_OctetList = (UINT1 *) &TempAddr;
    MEMCPY (Addr.pu1_OctetList, &Ip6Addr, IP6_ADDR_SIZE);
    Addr.i4_Length = IP6_ADDR_SIZE;
    i4AddrType = ADDR6_UNICAST;

    if (nmhTestv2Fsipv6AddrAdminStatus (&u4ErrorCode,
                                        i4Fsipv6AddrIndex,
                                        &Addr,
                                        i4Fsipv6AddrPrefixLen,
                                        CREATE_AND_WAIT) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    /* First the Admin Status Should be set to CREATE_AND_WAIT */
    if (nmhSetFsipv6AddrAdminStatus (i4Fsipv6AddrIndex,
                                     &Addr,
                                     i4Fsipv6AddrPrefixLen,
                                     CREATE_AND_WAIT) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    /* ADDR TYPE - set to specified Address Type */
    if (nmhTestv2Fsipv6AddrType (&u4ErrorCode, i4Fsipv6AddrIndex,
                                 &Addr, i4Fsipv6AddrPrefixLen,
                                 i4AddrType) == SNMP_FAILURE)
    {
        nmhSetFsipv6AddrAdminStatus (i4Fsipv6AddrIndex, &Addr,
                                     i4Fsipv6AddrPrefixLen, DESTROY);
        return SNMP_FAILURE;
    }

    if (nmhSetFsipv6AddrType (i4Fsipv6AddrIndex,
                              &Addr, i4Fsipv6AddrPrefixLen,
                              i4AddrType) == SNMP_FAILURE)
    {
        nmhSetFsipv6AddrAdminStatus (i4Fsipv6AddrIndex, &Addr,
                                     i4Fsipv6AddrPrefixLen, DESTROY);
        return SNMP_FAILURE;
    }

    /* ADMIN STATUS */
    if (nmhTestv2Fsipv6AddrAdminStatus (&u4ErrorCode,
                                        i4Fsipv6AddrIndex, &Addr,
                                        i4Fsipv6AddrPrefixLen,
                                        ACTIVE) == SNMP_FAILURE)
    {
        nmhSetFsipv6AddrAdminStatus (i4Fsipv6AddrIndex, &Addr,
                                     i4Fsipv6AddrPrefixLen, DESTROY);
        return SNMP_FAILURE;
    }
    /* First the Admin Status Should be set to 1 (up) */
    if (nmhSetFsipv6AddrAdminStatus (i4Fsipv6AddrIndex,
                                     &Addr,
                                     i4Fsipv6AddrPrefixLen,
                                     ACTIVE) == SNMP_FAILURE)

    {
        nmhSetFsipv6AddrAdminStatus (i4Fsipv6AddrIndex, &Addr,
                                     i4Fsipv6AddrPrefixLen, DESTROY);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/************************************************************************
 **     Function Name    :   Ip6IsAddrPublic
 **     Description      :   This function is used to find whether the given
 **                          address ia a public address or not
 **
 **     Inputs           :    Addr1            - IPv6 address
 **     Outputs          :    None
 **     Return Value     :    NETIPV6_SUCCESS  - If the address is a public
 **                                              address
 **                           NETIPV6_FAILUREB - If the address is not a public
 **                                                     address
 ** *************************************************************************/

INT1
Ip6IsAddrPublic (tIp6Addr Addr1)
{
    tIp6Addr            Ip6Addr;
    UINT1               aPublicAddr[128] = "2000::";
    UINT1               u1PrefixLen = 3;

    MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));
    INET_ATON6 (aPublicAddr, &Ip6Addr);
    if (Ip6AddrMatch (&Ip6Addr, &Addr1, (INT4) u1PrefixLen) == TRUE)
    {
        return NETIPV6_SUCCESS;
    }
    return NETIPV6_FAILURE;

}

/******************************************************************************
 ** DESCRIPTION : Determines whether the address is mine
 ** INPUTS      : Address Pointer and Pointer to the interface structure
 ** OUTPUTS     : None
 ** RETURNS     : IP6_SUCCESS if the Address is Mine
 **               IP6_FAILURE if the Address is Not Mine
 *******************************************************************************/

#ifndef LNXIP6_WANTED
INT4
Ip6IsMyAddr (tIp6Addr * pIp6Addr, tIp6If * pIf6)
#else
INT4
Ip6IsMyAddr (tIp6Addr * pIp6Addr, tLip6If * pIf6)
#endif
{
    tIp6McastInfo      *pMcastInfo = NULL;
    tTMO_SLL_NODE      *pSllInfo = NULL;
    tIp6LlocalInfo     *pCurrLlocal = NULL;
    tIp6AddrInfo       *pAddr6Info = NULL;
    UINT1               u1Type = ADDR6_INVALID;

    if (!(pIp6Addr))
    {
        return IP6_FAILURE;
    }

    if (!(pIf6))
    {
        return IP6_FAILURE;
    }

    u1Type = Ip6AddrType (pIp6Addr);

    if (u1Type == ADDR6_MULTI)
    {
        TMO_SLL_Scan (&pIf6->mcastIlist, pMcastInfo, tIp6McastInfo *)
        {
            if (Ip6AddrMatch (&(pMcastInfo->ip6Addr), pIp6Addr,
                              IP6_ADDR_SIZE_IN_BITS) == TRUE)
            {
                return IP6_SUCCESS;
            }
        }
        return IP6_FAILURE;
    }

    else if (u1Type == ADDR6_LLOCAL)
    {
#ifndef LNXIP6_WANTED
        TMO_SLL_Scan (&pIf6->lla6Ilist, pSllInfo, tTMO_SLL_NODE *)
#else
        TMO_SLL_Scan (&pIf6->Ip6LLAddrList, pSllInfo, tTMO_SLL_NODE *)
#endif
        {
            pCurrLlocal = IP6_LLADDR_PTR_FROM_SLL (pSllInfo);

            if ((pCurrLlocal->u1Status & ADDR6_UP) &&
                (!(pCurrLlocal->u1Status & ADDR6_FAILED)) &&
                (pCurrLlocal->u1ConfigMethod != IP6_ADDR_VIRTUAL))
            {
                if (MEMCMP (pIp6Addr, &pCurrLlocal->ip6Addr,
                            sizeof (tIp6Addr)) == 0)
                {
                    return IP6_SUCCESS;
                }
            }
        }
        return IP6_FAILURE;
    }

    else if (u1Type == ADDR6_V4_COMPAT)
    {
#ifndef LNXIP6_WANTED
        TMO_SLL_Scan (&pIf6->addr6Ilist, pSllInfo, tTMO_SLL_NODE *)
#else
        TMO_SLL_Scan (&pIf6->Ip6AddrList, pSllInfo, tTMO_SLL_NODE *)
#endif
        {
            pAddr6Info = IP6_ADDR_PTR_FROM_SLL (pSllInfo);

            if (IS_ADDR_V4_COMPAT (pAddr6Info->ip6Addr))
            {
                if (MEMCMP (&pIp6Addr->u4_addr[IP6_THREE],
                            &pAddr6Info->ip6Addr.u4_addr[IP6_THREE],
                            IP6_FOUR) == 0)
                {
                    return IP6_SUCCESS;
                }
            }
        }
    }

    else
    {
#ifndef LNXIP6_WANTED
        TMO_SLL_Scan (&pIf6->addr6Ilist, pSllInfo, tTMO_SLL_NODE *)
#else
        TMO_SLL_Scan (&pIf6->Ip6AddrList, pSllInfo, tTMO_SLL_NODE *)
#endif
        {
            pAddr6Info = IP6_ADDR_PTR_FROM_SLL (pSllInfo);

            if (pAddr6Info->u1ConfigMethod == IP6_ADDR_VIRTUAL)
            {
                continue;
            }

            if (Ip6AddrMatch (&(pAddr6Info->ip6Addr), pIp6Addr,
                              IP6_ADDR_SIZE_IN_BITS) == TRUE)
            {
                return IP6_SUCCESS;
            }
            if ((pAddr6Info->u1AddrType == ADDR6_ANYCAST) &&
                (Ip6AddrMatch (&(pAddr6Info->ip6Addr), pIp6Addr,
                               pAddr6Info->u1PrefLen) == TRUE))
            {
                return IP6_SUCCESS;
            }
        }
    }

    return IP6_FAILURE;
}

/******************************************************************************
 ** DESCRIPTION : Checks for existence of an entry in the Addr Profile Table
 **
 ** INPUTS      : Index to Addr Profile Table(u2AddrprofIndex)
 **
 ** OUTPUTS     : NONE.
 **
 ** RETURNS     : IP6_SUCCESS/IP6_FAILURE
 **
 ** NOTES       :
 ******************************************************************************/

INT1
Ip6addrProfEntryExists (UINT4 u4AddrprofIndex)
{
    if (IP6_ADDR_PROF_ADMIN (u4AddrprofIndex) == IP6_ADDR_PROF_VALID)
    {
        return IP6_SUCCESS;
    }
    return IP6_FAILURE;
}

/**************************************************************************
 **  DESCRIPTION : Called to get the prefix entry from
 **                the policy prefix tree
 **
 ** INPUTS      : Interface index, pointer to Prefix address, Prefix Length
 **
 ** OUTPUTS     : None
 **
 ** RETURNS     : Prefix Info if present or NULL
 **
 ** NOTES       :
 ** ***********************************************************************/

tIp6AddrSelPolicy  *
Ip6PolicyPrefixCreate (UINT4 u4IfIndex, tIp6Addr * pPolicyPrefix,
                       UINT1 u1PrefixLen, UINT1 u1RowStatus)
{
    tIp6AddrSelPolicy  *pPolicyPrefixInfo = NULL;
    tNetIpv6RtInfo      NetIpv6Rt;
    tIp6If             *pIf6 = NULL;
    tIp6Addr           *pNextHop = NULL;
    UINT4               u4Result = 0;
#ifdef LNXIP6_WANTED
    UINT1               u1Found = 0;
    UINT1               u1Ip6LookupPreference =
        IP6_ROUTE_PREFERENCE_BEST_METRIC;
#else
    tNd6CacheEntry     *pNd6c = NULL;
#endif
    INT4                i4Status = 0;
    UINT4               u1CreateStatus = IP6_POLICY_STATUS_MANUAL;
    UINT1               u1RtType = 0;
    UINT1               u1Label = IPV6_ADDR_SEL_DEFAULT_POLICY_LABEL;
    UINT1               u1Precedence = IPV6_ADDR_SEL_DEFAULT_POLICY_PRECEDENCE;
    MEMSET (&NetIpv6Rt, 0, sizeof (tNetIpv6RtInfo));

#ifndef LNXIP6_WANTED
    pPolicyPrefixInfo = (tIp6AddrSelPolicy *) (VOID *)
        Ip6GetMem (IP6_DEFAULT_CONTEXT,
                   (UINT2) gIp6GblInfo.i4Ip6PolicyPrefixListId);
#else
    pPolicyPrefixInfo =
        (tIp6AddrSelPolicy *) MemAllocMemBlk (gIp6GblInfo.Ip6AddrselPolicy);
#endif
    if (pPolicyPrefixInfo == NULL)
    {
        return NULL;
    }

    MEMSET (pPolicyPrefixInfo, 0, sizeof (tIp6AddrSelPolicy));

    /*Assign the parameters to  the configured entries */
    Ip6AddrCopy (&(pPolicyPrefixInfo->Ip6PolicyPrefix), pPolicyPrefix);
    pPolicyPrefixInfo->u1PrefixLen = u1PrefixLen;
    pPolicyPrefixInfo->u1RowStatus = u1RowStatus;
    pPolicyPrefixInfo->u4IfIndex = u4IfIndex;
    pPolicyPrefixInfo->u1Scope =
        Ip6GetAddrScope (&(pPolicyPrefixInfo->Ip6PolicyPrefix));

    pPolicyPrefixInfo->u1Precedence = u1Precedence;
    pPolicyPrefixInfo->u1Label = u1Label;

#ifndef LNXIP6_WANTED
    i4Status =
        Ip6RtLookupInCxt (IP6_DEFAULT_CONTEXT,
                          (&pPolicyPrefixInfo->Ip6PolicyPrefix), &NetIpv6Rt);
#else
    i4Status =
        Rtm6ApiRoute6LookupInCxt (IP6_DEFAULT_CONTEXT,
                                  (&pPolicyPrefixInfo->Ip6PolicyPrefix),
                                  IP6_ADDR_MAX_PREFIX, u1Ip6LookupPreference,
                                  &u1Found, &NetIpv6Rt);
#endif
    if (i4Status != IP6_FAILURE)
    {
        pNextHop = &(NetIpv6Rt.NextHop);
        u1RtType = NetIpv6Rt.i1Type;
        pIf6 = (tIp6If *) IP6_INTERFACE (u4IfIndex);
#ifndef LNXIP6_WANTED
        pNd6c = Nd6LookupCache (pIf6, (u1RtType == DIRECT) ?
                                &pPolicyPrefixInfo->Ip6PolicyPrefix : pNextHop);
        if (pNd6c != NULL)
        {
            if (Nd6GetReachState (pNd6c) == ND6C_REACHABLE)
            {
                pPolicyPrefixInfo->u1ReachabilityStatus =
                    IP6_POLICY_ENTRY_REACHABLE;
            }
            else
            {
                pPolicyPrefixInfo->u1ReachabilityStatus =
                    IP6_POLICY_ENTRY_UNREACHABLE;
            }
        }
#else
        UNUSED_PARAM (pIf6);
        UNUSED_PARAM (pNextHop);
        UNUSED_PARAM (u1RtType);
#endif
    }
    else
    {
        pPolicyPrefixInfo->u1ReachabilityStatus = IP6_POLICY_ENTRY_UNREACHABLE;
    }
    if (Ip6IsAddrPublic (pPolicyPrefixInfo->Ip6PolicyPrefix) == NETIPV6_SUCCESS)
    {
        pPolicyPrefixInfo->u1IsPublicAddr = OSIX_TRUE;
    }
    else
    {
        pPolicyPrefixInfo->u1IsPublicAddr = OSIX_FALSE;
    }
    if (Ip6IsMyAddr
        (&pPolicyPrefixInfo->Ip6PolicyPrefix,
         gIp6GblInfo.apIp6If[u4IfIndex]) == IP6_SUCCESS)

    {
        pPolicyPrefixInfo->u1IsSelfAddr = IP6_SELF_ADDR;

    }
    else
    {
        pPolicyPrefixInfo->u1IsSelfAddr = IP6_NOT_SELF_ADDR;
    }

    pPolicyPrefixInfo->u1CreateStatus = u1CreateStatus;

    u4Result =
        RBTreeAdd (gIp6GblInfo.PolicyPrefixTree, (tRBElem *) pPolicyPrefixInfo);
    UNUSED_PARAM (u4Result);
    return pPolicyPrefixInfo;
}

/**************************************************************************
 ** DESCRIPTION : Called to get the prefix entry from
 **               the policy prefix tree
 **
 ** INPUTS      : Interface index, pointer to Prefix address, Prefix Length
 **
 ** OUTPUTS     : None
 **
 ** RETURNS     : Prefix Info if present or NULL
 **
 ** NOTES       :
 ************************************************************************/

tIp6AddrSelPolicy  *
Ip6GetPolicyPrefix (UINT4 u4IfIndex, tIp6Addr * pPolicyPrefix,
                    UINT1 u4PrefixLen)
{

    tIp6AddrSelPolicy  *pPolicyPrefixInfo = NULL;
    tIp6AddrSelPolicy   PolicyPrefixInfo;
    MEMSET (&PolicyPrefixInfo, 0, sizeof (tIp6AddrSelPolicy));
    Ip6AddrCopy (&(PolicyPrefixInfo.Ip6PolicyPrefix), pPolicyPrefix);
    PolicyPrefixInfo.u1PrefixLen = u4PrefixLen;
    PolicyPrefixInfo.u4IfIndex = u4IfIndex;

    pPolicyPrefixInfo = RBTreeGet (gIp6GblInfo.PolicyPrefixTree,
                                   (tRBElem *) & PolicyPrefixInfo);
    if (pPolicyPrefixInfo != NULL)
    {
        return pPolicyPrefixInfo;
    }
    return NULL;

}

/**********************************************************************

 * DESCRIPTION : Called to fetch the first prefix
 **               from the policy Prefix tree
 **
 ** INPUTS      : Interface index
 **
 ** OUTPUTS     : None
 **
 ** RETURNS     : Policy Prefix Info if present or NULL
 **
 ** NOTES        :
 ** *********************************************************************/

tIp6AddrSelPolicy  *
Ip6GetFirstPolicyPrefix (UINT4 u4IfIndex)
{
    tIp6AddrSelPolicy  *pPolicyPrefix = NULL;

    pPolicyPrefix = (tIp6AddrSelPolicy *)
        RBTreeGetFirst (gIp6GblInfo.PolicyPrefixTree);
    UNUSED_PARAM (u4IfIndex);
    return pPolicyPrefix;
}

/**************************************************************************
 ** DESCRIPTION : Called to delete the prefix entry from
 **               the policy prefix tree
 **
 ** INPUTS      : Interface index, pointer to Prefix address, Prefix Length
 **
 ** OUTPUTS     : Deletes the corresponding entry from the policy prefix
 **               tree
 **
 ** RETURNS     : IP6_SUCCESS/ IP6_FAILURE
 **
 ** NOTES       :
 *************************************************************************/

INT1
Ip6PolicyPrefixDelete (UINT4 u4IfIndex, tIp6Addr * pPolicyPrefix,
                       UINT1 u4PrefixLen)
{

    tIp6AddrSelPolicy  *pPolicyPrefixInfo = NULL;
    tIp6AddrSelPolicy   PolicyPrefixInfo;

    MEMSET (&PolicyPrefixInfo, 0, sizeof (tIp6AddrSelPolicy));

    Ip6AddrCopy (&(PolicyPrefixInfo.Ip6PolicyPrefix), pPolicyPrefix);
    PolicyPrefixInfo.u1PrefixLen = u4PrefixLen;
    PolicyPrefixInfo.u4IfIndex = u4IfIndex;

    pPolicyPrefixInfo = RBTreeGet (gIp6GblInfo.PolicyPrefixTree,
                                   (tRBElem *) & PolicyPrefixInfo);
    if (pPolicyPrefixInfo != NULL)
    {

        RBTreeRem (gIp6GblInfo.PolicyPrefixTree, pPolicyPrefixInfo);
#ifndef LNXIP6_WANTED
        Ip6RelMem (IP6_DEFAULT_CONTEXT,
                   (UINT2) gIp6GblInfo.i4Ip6PolicyPrefixListId,
                   (UINT1 *) pPolicyPrefixInfo);
#else
        MemReleaseMemBlock (gIp6GblInfo.Ip6AddrselPolicy,
                            (UINT1 *) pPolicyPrefixInfo);
#endif
        return IP6_SUCCESS;
    }
    return IP6_FAILURE;
}

INT4
Ip6PolicyPrefixInfoCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tIp6AddrSelPolicy  *pAddrSelPolicyInfo = pRBElem1;
    tIp6AddrSelPolicy  *pAddrSelPolicyInfoIn = pRBElem2;
    tIp6Addr            Ip6PolicyPrefix;
    tIp6Addr            Ip6PolicyPrefixIn;
    UINT1               u4PrefixLen = 0, u4PrefixLenIn = 0;
    UINT4               u4IfIndex = 0, u4IfIndexIn = 0;
    INT4                i4RetVal = 0;
    MEMSET (&Ip6PolicyPrefix, 0, sizeof (tIp6Addr));
    MEMSET (&Ip6PolicyPrefixIn, 0, sizeof (tIp6Addr));

    Ip6CopyAddrBits (&Ip6PolicyPrefix, &pAddrSelPolicyInfo->Ip6PolicyPrefix,
                     pAddrSelPolicyInfo->u1PrefixLen);
    Ip6CopyAddrBits (&Ip6PolicyPrefixIn, &pAddrSelPolicyInfoIn->Ip6PolicyPrefix,
                     pAddrSelPolicyInfoIn->u1PrefixLen);

    i4RetVal = Ip6AddrCompare (Ip6PolicyPrefix, Ip6PolicyPrefixIn);
    if (i4RetVal == IP6_ONE)
    {
        return IP6_RB_GREATER;
    }
    else if (i4RetVal == IP6_MINUS_ONE)
    {
        return IP6_RB_LESSER;
    }

    u4PrefixLen = pAddrSelPolicyInfo->u1PrefixLen;
    u4PrefixLenIn = pAddrSelPolicyInfoIn->u1PrefixLen;

    if (u4PrefixLen < u4PrefixLenIn)
    {
        return IP6_RB_LESSER;
    }
    else if (u4PrefixLen > u4PrefixLenIn)
    {
        return IP6_RB_GREATER;
    }

    u4IfIndex = pAddrSelPolicyInfo->u4IfIndex;
    u4IfIndexIn = pAddrSelPolicyInfoIn->u4IfIndex;

    if (u4IfIndex < u4IfIndexIn)
    {
        return IP6_RB_LESSER;
    }
    else if (u4IfIndex > u4IfIndexIn)
    {
        return IP6_RB_GREATER;
    }
    return IP6_RB_EQUAL;
}

/************************************************************************
 *
 ** DESCRIPTION : Called to fetch the next prefix entry from
 **               the policy prefix tree
 **
 ** INPUTS      : Interface index, pointer to Prefix address, Prefix Length
 **
 ** OUTPUTS     : None
 **
 ** RETURNS     : Prefix Info if present or NULL
 **
 ** NOTES       :
 ** **********************************************************************/

tIp6AddrSelPolicy  *
Ip6GetNextPolicyPrefix (UINT4 u4IfIndex, tIp6Addr * pPolicyPrefix,
                        UINT1 u1PrefixLen)
{

    tIp6AddrSelPolicy  *pPolicyPrefixInfo = NULL;
    tIp6AddrSelPolicy   PolicyPrefixInfo;

    MEMSET (&PolicyPrefixInfo, 0, sizeof (tIp6AddrSelPolicy));

    Ip6AddrCopy (&(PolicyPrefixInfo.Ip6PolicyPrefix), pPolicyPrefix);
    PolicyPrefixInfo.u1PrefixLen = u1PrefixLen;
    PolicyPrefixInfo.u4IfIndex = u4IfIndex;

    pPolicyPrefixInfo = RBTreeGetNext (gIp6GblInfo.PolicyPrefixTree,
                                       (tRBElem *) & PolicyPrefixInfo, NULL);
    if (pPolicyPrefixInfo != NULL)
    {
        return pPolicyPrefixInfo;
    }
    return NULL;
}

VOID
IncMsrForIp6PolicyPrefixTable (INT4 i4IfIndex,
                               tSNMP_OCTET_STRING_TYPE * pIpv6Address,
                               INT4 i4PrefixLen, CHR1 cDatatype, VOID *pSetVal,
                               UINT4 *pu4ObjectId, UINT4 u4OIdLen,
                               UINT1 IsRowStatus)
{

    UINT4               u4SeqNum = IPVX_ZERO;
    INT4                i4SetValue = IPVX_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = pu4ObjectId;
    SnmpNotifyInfo.u4OidLen = u4OIdLen;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = IsRowStatus;
    SnmpNotifyInfo.pLockPointer = Ip6Lock;
    SnmpNotifyInfo.pUnLockPointer = Ip6UnLock;
    SnmpNotifyInfo.u4Indices = IPVX_THREE;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

    switch (cDatatype)
    {
        case 'i':
            i4SetValue = *(INT4 *) pSetVal;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s %i %i %i",
                              pIpv6Address, i4PrefixLen, i4IfIndex,
                              i4SetValue));
            break;
    }
#ifndef ISS_WANTED
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (pIpv6Address);
    UNUSED_PARAM (i4PrefixLen);
#endif
    return;

}
