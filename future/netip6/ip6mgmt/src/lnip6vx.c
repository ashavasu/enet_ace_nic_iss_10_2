/*******************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: lnip6vx.c,v 1.16 2016/05/19 10:49:42 siva Exp $
 *
 * Description: This file contains IPv6 SNMP based information
 *
 *******************************************************************/

#ifndef __IP6_IPVX_C__
#define __IP6_IPVX_C__

#include "lip6mgin.h"
#include "ip6cli.h"
#include "ipvx.h"

/*  Functions for : InetCidrRouteTable       */

/****************************************************************************
 *
 *    FUNCTION NAME    : IPvxGetIpv6Stats             
 *
 *    DESCRIPTION      : This function Get the given Type of statistic value 
 *                       for a ipv6 interface or IPv6 Module level         
 *
 *    INPUT            : i4Ipv6IfIndex  - IPv6 ifindex (ifIndex level) or 
 *                                       IPVX_INVALID_IFIDX (ipv6 VR level) 
 *                     : i4ContextId    - VR id or IPVX_INVALID_IFIDX 
 *                     : u4IPvxStatsType -  statistic  Type 
 *                     : pu1Ipv6StatsObj - ptr to statistic value.
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : SNMP_SUCCESS / SNMP_FAILURE
 *
 ****************************************************************************/
INT1
IPvxGetIpv6Stats (INT4 i4Ipv6IfIndex, INT4 i4ContextId,
                  UINT4 u4IPvxStatsType, UINT1 *pu1Ipv6StatsObj)
{
    tLip6If            *pIf6Entry = NULL;
    INT1                i1RetVal = SNMP_FAILURE;
    UINT4               u4IpSysStats = IP6_ZERO;
    UINT4              *pu4Stats = (UINT4 *) (VOID *) pu1Ipv6StatsObj;
    tSNMP_COUNTER64_TYPE *pu8Stats =
        (tSNMP_COUNTER64_TYPE *) (VOID *) pu1Ipv6StatsObj;

    UNUSED_PARAM (i4ContextId);
    if (i4Ipv6IfIndex == IPVX_INVALID_IFIDX)
    {
        /* Global stats not supported in Linux IP */
        return SNMP_SUCCESS;
    }

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4Ipv6IfIndex);
    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    switch (u4IPvxStatsType)
    {
        case IPVX_STATS_IN_RECEIVES:
            i1RetVal = nmhGetFsipv6IfStatsInReceives (i4Ipv6IfIndex,
                                                      &u4IpSysStats);
            *pu4Stats = u4IpSysStats;

            break;

        case IPVX_STATS_HC_IN_RECEIVES:

            pu8Stats->msn = 0;
            i1RetVal = nmhGetFsipv6IfStatsInReceives (i4Ipv6IfIndex,
                                                      &u4IpSysStats);
            pu8Stats->lsn = u4IpSysStats;
            break;

        case IPVX_STATS_IN_OCTETS:
            *pu4Stats = 0;
            break;

        case IPVX_STATS_HC_IN_OCTETS:
            pu8Stats->lsn = 0;
            pu8Stats->msn = 0;
            break;

        case IPVX_STATS_IN_HDR_ERRS:

            i1RetVal = nmhGetFsipv6IfStatsInHdrErrors (i4Ipv6IfIndex,
                                                       &u4IpSysStats);
            *pu4Stats = u4IpSysStats;

            break;

        case IPVX_STATS_IN_NO_ROUTE:

            i1RetVal = nmhGetIpv6IfStatsInNoRoutes (i4Ipv6IfIndex,
                                                    &u4IpSysStats);
            *pu4Stats = u4IpSysStats;

            break;

        case IPVX_STATS_IN_ADDR_ERRS:

            i1RetVal = nmhGetFsipv6IfStatsInAddrErrors (i4Ipv6IfIndex,
                                                        &u4IpSysStats);
            *pu4Stats = u4IpSysStats;

            break;

        case IPVX_STATS_IN_UNKNOWN_PROTOS:

            i1RetVal = nmhGetFsipv6IfStatsInUnknownProtos (i4Ipv6IfIndex,
                                                           &u4IpSysStats);
            *pu4Stats = u4IpSysStats;

            break;

        case IPVX_STATS_IN_TRUN_PKTS:

            i1RetVal = nmhGetFsipv6IfStatsInTruncatedPkts (i4Ipv6IfIndex,
                                                           &u4IpSysStats);
            *pu4Stats = u4IpSysStats;

            break;

        case IPVX_STATS_IN_FWD_DGRAMS:
            i1RetVal = nmhGetFsipv6IfStatsForwDatagrams (i4Ipv6IfIndex,
                                                         &u4IpSysStats);
            *pu4Stats = u4IpSysStats;
            break;

        case IPVX_STATS_HC_IN_FWD_DGRAMS:

            pu8Stats->msn = 0;

            i1RetVal = nmhGetFsipv6IfStatsForwDatagrams (i4Ipv6IfIndex,
                                                         &u4IpSysStats);
            pu8Stats->lsn = u4IpSysStats;
            break;

        case IPVX_STATS_REASM_REQDS:

            i1RetVal = nmhGetFsipv6IfStatsReasmReqds (i4Ipv6IfIndex,
                                                      &u4IpSysStats);
            *pu4Stats = u4IpSysStats;
            break;

        case IPVX_STATS_REASM_OK:

            i1RetVal = nmhGetFsipv6IfStatsReasmOKs (i4Ipv6IfIndex,
                                                    &u4IpSysStats);
            *pu4Stats = u4IpSysStats;
            break;

        case IPVX_STATS_REASM_FAILS:

            i1RetVal = nmhGetFsipv6IfStatsReasmFails (i4Ipv6IfIndex,
                                                      &u4IpSysStats);
            *pu4Stats = u4IpSysStats;

            break;

        case IPVX_STATS_IN_DISCARDS:

            i1RetVal = nmhGetFsipv6IfStatsInDiscards (i4Ipv6IfIndex,
                                                      &u4IpSysStats);
            *pu4Stats = u4IpSysStats;

            break;

        case IPVX_STATS_IN_DELIVERS:

            i1RetVal = nmhGetFsipv6IfStatsInDelivers (i4Ipv6IfIndex,
                                                      &u4IpSysStats);
            *pu4Stats = u4IpSysStats;
            break;

        case IPVX_STATS_HC_IN_DELIVERS:

            pu8Stats->msn = 0;

            i1RetVal = nmhGetFsipv6IfStatsInDelivers (i4Ipv6IfIndex,
                                                      &u4IpSysStats);
            pu8Stats->lsn = u4IpSysStats;
            break;

        case IPVX_STATS_OUT_REQUESTS:

            i1RetVal = nmhGetFsipv6IfStatsOutRequests (i4Ipv6IfIndex,
                                                       &u4IpSysStats);
            *pu4Stats = u4IpSysStats;
            break;

        case IPVX_STATS_HC_OUT_REQUESTS:

            pu8Stats->msn = 0;
            i1RetVal = nmhGetFsipv6IfStatsOutRequests (i4Ipv6IfIndex,
                                                       &u4IpSysStats);
            pu8Stats->lsn = u4IpSysStats;
            break;

        case IPVX_STATS_OUT_NO_ROUTES:

            i1RetVal = nmhGetFsipv6IfStatsOutNoRoutes (i4Ipv6IfIndex,
                                                       &u4IpSysStats);
            *pu4Stats = u4IpSysStats;
            break;

        case IPVX_STATS_OUT_FWD_DGRAMS:

            *pu4Stats = 0;
            break;

        case IPVX_STATS_HC_OUT_FWD_DGRAMS:

            pu8Stats->lsn = 0;
            pu8Stats->msn = 0;
            break;

        case IPVX_STATS_OUT_DISCARDS:

            i1RetVal = nmhGetFsipv6IfStatsOutDiscards (i4Ipv6IfIndex,
                                                       &u4IpSysStats);
            *pu4Stats = u4IpSysStats;

            break;

        case IPVX_STATS_OUT_FRAG_REQDS:

            *pu4Stats = 0;
            break;

        case IPVX_STATS_OUT_FRAG_OK:

            i1RetVal = nmhGetFsipv6IfStatsFragOKs (i4Ipv6IfIndex,
                                                   &u4IpSysStats);
            *pu4Stats = u4IpSysStats;
            break;

        case IPVX_STATS_OUT_FRAG_FAILS:

            i1RetVal = nmhGetFsipv6IfStatsFragFails (i4Ipv6IfIndex,
                                                     &u4IpSysStats);
            *pu4Stats = u4IpSysStats;
            break;

        case IPVX_STATS_OUT_FRAG_CRET:

            i1RetVal = nmhGetFsipv6IfStatsFragCreates (i4Ipv6IfIndex,
                                                       &u4IpSysStats);
            *pu4Stats = u4IpSysStats;
            break;

        case IPVX_STATS_OUT_TRANS:

            *pu4Stats = 0;
            break;

        case IPVX_STATS_HC_OUT_TRANS:
            pu8Stats->lsn = 0;
            pu8Stats->msn = 0;
            break;

        case IPVX_STATS_OUT_OCTETS:
            *pu4Stats = 0;
            break;

        case IPVX_STATS_HC_OUT_OCTETS:
            pu8Stats->lsn = 0;
            pu8Stats->msn = 0;
            break;

        case IPVX_STATS_IN_MCAST_PKTS:
            i1RetVal = nmhGetFsipv6IfStatsInMcastPkts (i4Ipv6IfIndex,
                                                       &u4IpSysStats);
            *pu4Stats = u4IpSysStats;
            break;

        case IPVX_STATS_HC_IN_MCAST_PKTS:

            pu8Stats->msn = 0;
            i1RetVal = nmhGetFsipv6IfStatsInMcastPkts (i4Ipv6IfIndex,
                                                       &u4IpSysStats);
            pu8Stats->lsn = u4IpSysStats;
            break;

        case IPVX_STATS_IN_MCAST_OCTETS:

            *pu4Stats = 0;

        case IPVX_STATS_HC_IN_MCAST_OCTETS:

            pu8Stats->lsn = 0;
            pu8Stats->msn = 0;
            break;

        case IPVX_STATS_OUT_MCAST_PKTS:
            i1RetVal = nmhGetFsipv6IfStatsOutMcastPkts (i4Ipv6IfIndex,
                                                        &u4IpSysStats);
            *pu4Stats = u4IpSysStats;
            break;

        case IPVX_STATS_HC_OUT_MCAST_PKTS:
            pu8Stats->msn = 0;
            i1RetVal = nmhGetFsipv6IfStatsOutMcastPkts (i4Ipv6IfIndex,
                                                        &u4IpSysStats);
            pu8Stats->lsn = u4IpSysStats;
            break;

        case IPVX_STATS_OUT_MCAST_OCTETS:
            *pu4Stats = 0;
            break;

        case IPVX_STATS_HC_OUT_MCAST_OCTETS:
            pu8Stats->lsn = 0;
            pu8Stats->msn = 0;
            break;

        case IPVX_STATS_IN_BCAST_PKTS:
        case IPVX_STATS_OUT_BCAST_PKTS:
            *pu4Stats = IP6_ZERO;
            break;

        case IPVX_STATS_HC_IN_BCAST_PKTS:
        case IPVX_STATS_HC_OUT_BCAST_PKTS:
            pu8Stats->msn = IP6_ZERO;
            pu8Stats->lsn = IP6_ZERO;
            break;

        default:
            return SNMP_FAILURE;

    }
    UNUSED_PARAM (i1RetVal);
    *pu1Ipv6StatsObj = 0;
    return SNMP_SUCCESS;
}

/*  Functions for : IpAddressPrefixTable     */

/*  Functions for : IpAddressTable           */

/*  Functions for : IpNetToPhysicalTable     */

/****************************************************************************
 *
 *    FUNCTION NAME    : IPvxGetIPv6NetToPhyLastUpdated 
 *
 *    DESCRIPTION      : This function get ND Cache entry last updated time. 
 *
 *    INPUT            : i4Ipv6IfIndex              - IPv6 ifindex  
 *                     : pIpv6Addr                  - Ipv6 address         
 *                     : pu4IPv6NDCTblLastUpdated   - ptr to last updated time
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : SNMP_SUCCESS / SNMP_FAILURE
 *
 ****************************************************************************/
INT1
IPvxGetIPv6NetToPhyLastUpdated (INT4 i4Ipv6IfIndex,
                                tSNMP_OCTET_STRING_TYPE * pIpv6Addr,
                                UINT4 *pu4IPv6NDCTblLastUpdated)
{
    UNUSED_PARAM (i4Ipv6IfIndex);
    UNUSED_PARAM (pIpv6Addr);
    UNUSED_PARAM (pu4IPv6NDCTblLastUpdated);

    return SNMP_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : IPvxGetIPv6NetToPhyType 
 *
 *    DESCRIPTION      : This function get ND Cache entry Type. 
 *
 *    INPUT            : i4Ipv6IfIndex              - IPv6 ifindex  
 *                     : pIpv6Addr                  - Ipv6 address         
 *                     : pi4IpNetToPhysicalType     - ptr to Entry Type 
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : SNMP_SUCCESS / SNMP_FAILURE
 *
 ****************************************************************************/
INT1
IPvxGetIPv6NetToPhyType (INT4 i4Ipv6IfIndex,
                         tSNMP_OCTET_STRING_TYPE * pIpv6Addr,
                         INT4 *pi4IpNetToPhysicalType)
{

    UNUSED_PARAM (i4Ipv6IfIndex);
    UNUSED_PARAM (pIpv6Addr);
    UNUSED_PARAM (pi4IpNetToPhysicalType);
    return SNMP_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : IPvxGetIpv6NetToPhyState       
 *
 *    DESCRIPTION      : This function get ND Cache entry State. 
 *
 *    INPUT            : i4Ipv6IfIndex              - IPv6 ifindex  
 *                     : pIpv6Addr                  - Ipv6 address         
 *                     : pi4IPv6NDCTblState         - ptr to State of Entry.  
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : SNMP_SUCCESS / SNMP_FAILURE
 *
 ****************************************************************************/
INT1
IPvxGetIpv6NetToPhyState (INT4 i4Ipv6IfIndex,
                          tSNMP_OCTET_STRING_TYPE * pIpv6Addr,
                          INT4 *pi4IPv6NDCTblState)
{
    UNUSED_PARAM (i4Ipv6IfIndex);
    UNUSED_PARAM (pIpv6Addr);
    UNUSED_PARAM (pi4IPv6NDCTblState);
    return SNMP_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : IPvxGetIpv6NetToPhyRowStatus 
 *
 *    DESCRIPTION      : This function get ND Cache entry Rowstatus. 
 *
 *    INPUT            : i4Ipv6IfIndex          - IPv6 ifindex  
 *                     : pIpv6Addr              - Ipv6 address         
 *                     : pi4Ipv6GetRowStatus    - ptr to entry rowstatus
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : SNMP_SUCCESS / SNMP_FAILURE
 *
 ****************************************************************************/
INT1
IPvxGetIpv6NetToPhyRowStatus (INT4 i4Ipv6IfIndex,
                              tSNMP_OCTET_STRING_TYPE * pIpv6Addr,
                              INT4 *pi4Ipv6GetRowStatus)
{
    UNUSED_PARAM (i4Ipv6IfIndex);
    UNUSED_PARAM (pIpv6Addr);
    UNUSED_PARAM (pi4Ipv6GetRowStatus);
    return SNMP_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : IPvxSetIPv6NetToPhyPhyAddr        
 *
 *    DESCRIPTION      : This function set ND Cache entry type . 
 *
 *    INPUT            : i4Ipv6IfIndex                - IPv6 ifindex  
 *                     : pIpv6Addr                    - Ipv6 address         
 *                     : i4TestValIpNetToPhysicalType - Entry type 
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : SNMP_SUCCESS / SNMP_FAILURE
 *
 ****************************************************************************/
INT1
IPvxSetIPv6NetToPhyPhyAddr (INT4 i4Ipv6IfIndex,
                            tSNMP_OCTET_STRING_TYPE * pIpv6Addr,
                            tSNMP_OCTET_STRING_TYPE * pPhysAddr)
{
    UNUSED_PARAM (i4Ipv6IfIndex);
    UNUSED_PARAM (pIpv6Addr);
    UNUSED_PARAM (pPhysAddr);
    return SNMP_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : IPvxSetIPv6NetToPhyType        
 *
 *    DESCRIPTION      : This function set ND Cache entry type . 
 *
 *    INPUT            : i4Ipv6IfIndex                - IPv6 ifindex  
 *                     : pIpv6Addr                    - Ipv6 address         
 *                     : i4TestValIpNetToPhysicalType - Entry type 
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : SNMP_SUCCESS / SNMP_FAILURE
 *
 ****************************************************************************/
INT1
IPvxSetIPv6NetToPhyType (INT4 i4Ipv6IfIndex,
                         tSNMP_OCTET_STRING_TYPE * pIpv6Addr,
                         INT4 i4TestValIpNetToPhysicalType)
{
    UNUSED_PARAM (i4Ipv6IfIndex);
    UNUSED_PARAM (pIpv6Addr);
    UNUSED_PARAM (i4TestValIpNetToPhysicalType);
    return SNMP_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : IPvxSetIpv6NetToPhyRowStatus 
 *
 *    DESCRIPTION      : This function set ND Cache entry Rowstatus. 
 *
 *    INPUT            : i4Ipv6IfIndex          - IPv6 ifindex  
 *                     : pIpv6Addr              - Ipv6 address         
 *                     : i4Ipv6SetRowStatus     - entry rowstatus
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : SNMP_SUCCESS / SNMP_FAILURE
 *
 ****************************************************************************/
INT1
IPvxSetIpv6NetToPhyRowStatus (INT4 i4Ipv6IfIndex,
                              tSNMP_OCTET_STRING_TYPE * pIpv6Addr,
                              INT4 i4Ipv6SetRowStatus)
{
    UNUSED_PARAM (i4Ipv6IfIndex);
    UNUSED_PARAM (pIpv6Addr);
    UNUSED_PARAM (i4Ipv6SetRowStatus);

    return SNMP_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : IPvxTestIPv6NetToPhyType       
 *
 *    DESCRIPTION      : This function test ND Cache entry type . 
 *
 *    INPUT            : pu4ErrorCode                 - ptr to error code
 *                     : i4Ipv6IfIndex                - IPv6 ifindex  
 *                     : pIpv6Addr                    - Ipv6 address         
 *                     : i4TestValIpNetToPhysicalType - Entry type 
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : SNMP_SUCCESS / SNMP_FAILURE
 *
 ****************************************************************************/
INT1
IPvxTestIPv6NetToPhyType (UINT4 *pu4ErrorCode, INT4 i4Ipv6IfIndex,
                          tSNMP_OCTET_STRING_TYPE * pIpv6Addr,
                          INT4 i4TestValIpNetToPhysicalType)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4Ipv6IfIndex);
    UNUSED_PARAM (pIpv6Addr);
    UNUSED_PARAM (i4TestValIpNetToPhysicalType);
    return SNMP_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : IPvxTestIpv6NetToPhyRowStatus
 *
 *    DESCRIPTION      : This function test ND Cache entry Rowstatus. 
 *
 *    INPUT            : i4Ipv6IfIndex          - IPv6 ifindex  
 *                     : pIpv6Addr              - Ipv6 address         
 *                     : i4Ipv6TestRowStatus    - entry rowstatus
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : SNMP_SUCCESS / SNMP_FAILURE
 *
 ****************************************************************************/
INT1
IPvxTestIpv6NetToPhyRowStatus (UINT4 *pu4ErrorCode, INT4 i4Ipv6IfIndex,
                               tSNMP_OCTET_STRING_TYPE * pIpv6Addr,
                               INT4 i4Ipv6TestRowStatus)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4Ipv6IfIndex);
    UNUSED_PARAM (pIpv6Addr);
    UNUSED_PARAM (i4Ipv6TestRowStatus);
    return SNMP_SUCCESS;
}

/*  Functions for : Ipv6ScopeZoneIndexTable  -- No Functions */

/*  Functions for : IpDefaultRouterTable     */
/****************************************************************************
 *
 *    FUNCTION NAME    : IPvxValIdxInstIpv6DefRtrTbl  
 *
 *    DESCRIPTION      : This function validate the given  Router address 
 *                       is learned as default router through ND on the
 *                       given interface or not. 
 *
 *    INPUT            : pIpv6DefRtrAddr        - Ipv6 default router address 
 *                     : i4Ipv6DefRtrIfIndex    - IPv6 ifindex         
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : SNMP_SUCCESS / SNMP_FAILURE
 *
 ****************************************************************************/
INT1
IPvxValIdxInstIpv6DefRtrTbl (tSNMP_OCTET_STRING_TYPE * pIpv6DefRtrAddr,
                             INT4 i4Ipv6DefRtrIfIndex)
{

    UNUSED_PARAM (pIpv6DefRtrAddr);
    UNUSED_PARAM (i4Ipv6DefRtrIfIndex);
    return SNMP_FAILURE;

}

/****************************************************************************
 *
 *    FUNCTION NAME    : IPvxGetNxtIdxIpv6DefRtrTbl  
 *
 *    DESCRIPTION      : This function give the next default Router address 
 *                       for a given values.
 *
 *    INPUT            : pIpv6DefRtrAddr        - Ipv6 default router address 
 *                     : pNextIpv6DefRtrAddr    - next Ipv6 default router
 *                                                address 
 *                     : i4Ipv6DefRtrIfIndex    - IPv6 ifindex         
 *                     : pi4NextIpv6DefRtrIfIndex - Ipv6 ifindex  of
 *                           ' pNextIpv6DefRtrAddr '        
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : SNMP_SUCCESS / SNMP_FAILURE
 *
 ****************************************************************************/
INT1
IPvxGetNxtIdxIpv6DefRtrTbl (tSNMP_OCTET_STRING_TYPE * pIpv6DefRtrAddr,
                            tSNMP_OCTET_STRING_TYPE * pNextIpv6DefRtrAddr,
                            INT4 i4Ipv6DefRtrIfIndex,
                            INT4 *pi4NextIpv6DefRtrIfIndex)
{
    UNUSED_PARAM (pIpv6DefRtrAddr);
    UNUSED_PARAM (pNextIpv6DefRtrAddr);
    UNUSED_PARAM (i4Ipv6DefRtrIfIndex);
    UNUSED_PARAM (pi4NextIpv6DefRtrIfIndex);
    return SNMP_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : IPvxGetIpv6DefRtrLifetime   
 *
 *    DESCRIPTION      : This function give the Life time for a given 
 *                       default Router address and ifindex
 *
 *    INPUT            : pIpv6DefRtrAddr        - Ipv6 default router address 
 *                     : i4Ipv6DefRtrIfIndex    - IPv6 ifindex         
 *                     : pu4Ipv6DefRtrLifetime  - Life time of the default 
 *                           router        
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : SNMP_SUCCESS / SNMP_FAILURE
 *
 ****************************************************************************/
INT1
IPvxGetIpv6DefRtrLifetime (tSNMP_OCTET_STRING_TYPE * pIpv6DefRtrAddr,
                           INT4 i4Ipv6DefRtrIfIndex,
                           UINT4 *pu4Ipv6DefRtrLifetime)
{
    UNUSED_PARAM (pIpv6DefRtrAddr);
    UNUSED_PARAM (i4Ipv6DefRtrIfIndex);
    UNUSED_PARAM (pu4Ipv6DefRtrLifetime);
    return SNMP_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : IPvxGetIpv6DefRtrPreference 
 *
 *    DESCRIPTION      : This function give the prererence value for a given 
 *                       default Router address and ifindex
 *
 *    INPUT            : pIpv6DefRtrAddr        - Ipv6 default router address 
 *                     : i4Ipv6DefRtrIfIndex    - IPv6 ifindex         
 *                     : pu4Ipv6DefRtrPref      - Preferences value of the
 *                        default router        
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : SNMP_SUCCESS / SNMP_FAILURE
 *
 ****************************************************************************/
INT1
IPvxGetIpv6DefRtrPreference (tSNMP_OCTET_STRING_TYPE * pIpv6DefRtrAddr,
                             INT4 i4Ipv6DefRtrIfIndex, UINT4 *pu4Ipv6DefRtrPref)
{
    /* RFC 4191 - NOt Implemented so always medium (0) 
     * for Both IPV4, IPV6 */
    UNUSED_PARAM (pIpv6DefRtrAddr);
    UNUSED_PARAM (i4Ipv6DefRtrIfIndex);

    *pu4Ipv6DefRtrPref = IPVX_DEF_ROUTER_PREF_MEDIUM;

    return SNMP_SUCCESS;
}

/*  Functions for : Ipv6RouterAdvertTable    */

/*  Functions for : IcmpStatsTable  -- No Functions */

/*  Functions for : IcmpMsgStatsTable        */
/****************************************************************************
 *
 *    FUNCTION NAME    : IPvxValidateIpv6IcmpMsgType  
 *
 *    DESCRIPTION      : This function validate the Given Icmp6 message type 
 *
 *    INPUT            : i4IcmpMsgStatsType - Type of Icmp6 Message 
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : SNMP_SUCCESS / SNMP_FAILURE
 *
 ****************************************************************************/
INT1
IPvxValidateIpv6IcmpMsgType (INT4 i4IcmpMsgStatsType)
{
    switch (i4IcmpMsgStatsType)
    {
        case ICMP6_DEST_UNREACHABLE:    /*   1 */
        case ICMP6_PKT_TOO_BIG:    /*   2 */
        case ICMP6_TIME_EXCEEDED:    /*   3 */
        case ICMP6_PKT_PARAM_PROBLEM:    /*   4 */
        case ICMP6_ECHO_REQUEST:    /* 128 */
        case ICMP6_ECHO_REPLY:    /* 129 */
        case ICMP6_MLD_QUERY:    /* 130 */
        case ICMP6_MLD_REPORT:    /* 131 */
        case ICMP6_MLD_DONE:    /* 132 */
        case ICMP6_ROUTER_SOLICITATION:    /* 133 */
        case ICMP6_ROUTER_ADVERTISEMENT:    /* 134 */
        case ICMP6_NEIGHBOUR_SOLICITATION:    /* 135 */
        case ICMP6_NEIGHBOUR_ADVERTISEMENT:    /* 136 */
        case ICMP6_ROUTE_REDIRECT:    /* 137 */
        case ICMP6_RATE_LIMIT:    /* 138 */
            break;

        default:
            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : IPvxGetNxtIpv6IcmpMsgType  
 *
 *    DESCRIPTION      : This function give the next valid Icmp6 message type 
 *
 *    INPUT            : i4IcmpMsgType      - Type of Icmp6 Message 
 *                     : pi4NextIcmpMsgType - Type of next Icmp6 Message 
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : SNMP_SUCCESS / SNMP_FAILURE
 *
 ****************************************************************************/
INT1
IPvxGetNxtIpv6IcmpMsgType (INT4 i4IcmpMsgType, INT4 *pi4NextIcmpMsgType)
{

    /* This is to fill the gap between 4-128 */
    if ((i4IcmpMsgType > ICMP6_PKT_PARAM_PROBLEM) &&
        (i4IcmpMsgType < ICMP6_ECHO_REQUEST))
    {
        i4IcmpMsgType = ICMP6_PKT_PARAM_PROBLEM;
    }

    switch (i4IcmpMsgType)
    {
        case ICMP6_DEST_UNREACHABLE:    /*   1 */
            *pi4NextIcmpMsgType = ICMP6_PKT_TOO_BIG;
            break;

        case ICMP6_PKT_TOO_BIG:    /*   2 */
            *pi4NextIcmpMsgType = ICMP6_TIME_EXCEEDED;
            break;

        case ICMP6_TIME_EXCEEDED:    /*   3 */
            *pi4NextIcmpMsgType = ICMP6_PKT_PARAM_PROBLEM;
            break;

        case ICMP6_PKT_PARAM_PROBLEM:    /*   4 */
            *pi4NextIcmpMsgType = ICMP6_ECHO_REQUEST;
            break;

        case ICMP6_ECHO_REQUEST:    /* 128 */
            *pi4NextIcmpMsgType = ICMP6_ECHO_REPLY;
            break;

        case ICMP6_ECHO_REPLY:    /* 129 */
            *pi4NextIcmpMsgType = ICMP6_MLD_QUERY;
            break;

        case ICMP6_MLD_QUERY:    /* 130 */
            *pi4NextIcmpMsgType = ICMP6_MLD_REPORT;
            break;

        case ICMP6_MLD_REPORT:    /* 131 */
            *pi4NextIcmpMsgType = ICMP6_MLD_DONE;
            break;

        case ICMP6_MLD_DONE:    /* 132 */
            *pi4NextIcmpMsgType = ICMP6_ROUTER_SOLICITATION;
            break;

        case ICMP6_ROUTER_SOLICITATION:    /* 133 */
            *pi4NextIcmpMsgType = ICMP6_ROUTER_ADVERTISEMENT;
            break;

        case ICMP6_ROUTER_ADVERTISEMENT:    /* 134 */
            *pi4NextIcmpMsgType = ICMP6_NEIGHBOUR_SOLICITATION;
            break;

        case ICMP6_NEIGHBOUR_SOLICITATION:    /* 135 */
            *pi4NextIcmpMsgType = ICMP6_NEIGHBOUR_ADVERTISEMENT;
            break;

        case ICMP6_NEIGHBOUR_ADVERTISEMENT:    /* 136 */
            *pi4NextIcmpMsgType = ICMP6_ROUTE_REDIRECT;
            break;

        case ICMP6_ROUTE_REDIRECT:    /* 137 */
            *pi4NextIcmpMsgType = ICMP6_RATE_LIMIT;
            break;

        case ICMP6_RATE_LIMIT:    /* 138 */
            return SNMP_FAILURE;

        default:
            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : IPvxIpv6GetIcmpMsgStatsPkts 
 *
 *    DESCRIPTION      : This function used to get the Icmp6 Stats for the   
 *                       given counter type
 *
 *    INPUT            : i4IcmpMsgStatsType - Type of Icmp6 Message 
 *                     : u1Dir              - Direction of the Counter (IN/OUT)
 *                     : pu4RetValIcmpMsgStatsInPkts - ptr to Counter      
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : SNMP_SUCCESS / SNMP_FAILURE
 *
 ****************************************************************************/
INT1
IPvxIpv6GetIcmpMsgStatsPkts (UINT4 u4ContextId, INT4 i4IcmpMsgStatsType,
                             UINT1 u1Dir, UINT4 *pu4RetValIcmpMsgStatsInPkts)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (i4IcmpMsgStatsType);
    UNUSED_PARAM (u1Dir);
    UNUSED_PARAM (pu4RetValIcmpMsgStatsInPkts);
    return SNMP_SUCCESS;
}

/*********************************************************************
*  Function Name : IPvxSetIPv6Addr 
*  Description   : Configuring Ipv6 Address
*  Input(s)      : u4Ip6IfIndex - Interface Index
*                  Ip6Addr - Ip6 Address to be set
*                  i4PrefixLen - Prefix Length
*                  i4AddrType - Adress type (Unicast,link-local,
*                               anycast,eui-based)
*  Output(s)     :  
*  Return Values : None.
*********************************************************************/

VOID
IPvxSetIPv6Addr (UINT4 u4Ip6IfIndex, tIp6Addr Ip6Addr,
                 INT4 i4PrefixLen, INT4 i4AddrType)
{
    tSNMP_OCTET_STRING_TYPE Addr;
    tSNMP_OCTET_STRING_TYPE IfToken;
    UINT4               u4ErrorCode;
    INT4                i4Status;
    UINT1               au1AddrOctetList[IP6_ADDR_SIZE];
    UINT1               au1IfTokenOctetList[IP6_EUI_ADDRESS_LEN];

    MEMSET (au1AddrOctetList, IP6_ZERO, IP6_ADDR_SIZE);
    MEMSET (au1IfTokenOctetList, IP6_ZERO, IP6_EUI_ADDRESS_LEN);

    Addr.pu1_OctetList = au1AddrOctetList;
    MEMCPY (Addr.pu1_OctetList, &Ip6Addr, IP6_ADDR_SIZE);
    Addr.i4_Length = IP6_ADDR_SIZE;

    if (IS_ADDR_MULTI (Ip6Addr) || IS_ADDR_UNSPECIFIED (Ip6Addr))
    {
        return;
    }

    /*
     * EUI-64 address configuration is supported only for CLI. When the
     * address type is set as EUI-64 form the fetch the interface
     * identifier value and form the complete IP6 address to be configured.
     */
    if (i4AddrType == ADDR6_UNSPECIFIED)
    {                            /* EUI-64 */
        /* Length should not be greater than 64. */
        if (i4PrefixLen != IP6_EUI_ADDRESS_PREFIX_LEN)
        {
            return;
        }

        /* EUI-64 can be configured only if IPv6 is already enabled over
         * that interface */
        if ((nmhGetFsipv6IfAdminStatus
             ((INT4) u4Ip6IfIndex, &i4Status)
             == SNMP_FAILURE) || (i4Status != NETIPV6_ADMIN_UP))
        {
            return;
        }

        /* Form the Actual IP6 Address */
        IfToken.pu1_OctetList = au1IfTokenOctetList;
        IfToken.i4_Length = 0;

        if (nmhGetFsipv6IfToken (u4Ip6IfIndex, &IfToken) == SNMP_FAILURE)
        {
            return;
        }

        MEMCPY (&Addr.pu1_OctetList[IP6_EUI_ADDRESS_LEN],
                IfToken.pu1_OctetList, IP6_EUI_ADDRESS_LEN);

        /* Also update the address type as UNICAST */
        i4AddrType = ADDR6_UNICAST;
    }

    /* ADMIN STATUS */
    if (nmhTestv2Fsipv6AddrAdminStatus (&u4ErrorCode,
                                        u4Ip6IfIndex, &Addr,
                                        i4PrefixLen, CREATE_AND_WAIT)
        == SNMP_FAILURE)
    {
        return;
    }

    /* First the Admin Status Should be set to CREATE_AND_WAIT */
    if (nmhSetFsipv6AddrAdminStatus ((INT4) u4Ip6IfIndex,
                                     &Addr,
                                     i4PrefixLen,
                                     CREATE_AND_WAIT) == SNMP_FAILURE)
    {
        return;
    }

    /* ADDR TYPE - set to specified Address Type */
    if (nmhTestv2Fsipv6AddrType (&u4ErrorCode,
                                 u4Ip6IfIndex,
                                 &Addr,
                                 i4PrefixLen, i4AddrType) == SNMP_FAILURE)
    {
        nmhSetFsipv6AddrAdminStatus ((INT4) u4Ip6IfIndex, &Addr, i4PrefixLen,
                                     DESTROY);
        return;
    }

    if (nmhSetFsipv6AddrType (u4Ip6IfIndex,
                              &Addr, i4PrefixLen, i4AddrType) == SNMP_FAILURE)
    {
        nmhSetFsipv6AddrAdminStatus ((INT4) u4Ip6IfIndex, &Addr, i4PrefixLen,
                                     DESTROY);
        return;
    }

    /* ADMIN STATUS */
    if (nmhTestv2Fsipv6AddrAdminStatus (&u4ErrorCode,
                                        u4Ip6IfIndex, &Addr,
                                        i4PrefixLen, ACTIVE) == SNMP_FAILURE)
    {
        nmhSetFsipv6AddrAdminStatus ((INT4) u4Ip6IfIndex, &Addr, i4PrefixLen,
                                     DESTROY);
        return;
    }

    /* First the Admin Status Should be set to 1 (up) */
    if (nmhSetFsipv6AddrAdminStatus ((INT4) u4Ip6IfIndex,
                                     &Addr,
                                     i4PrefixLen, ACTIVE) == SNMP_FAILURE)

    {
        nmhSetFsipv6AddrAdminStatus ((INT4) u4Ip6IfIndex, &Addr, i4PrefixLen,
                                     DESTROY);
        return;
    }

    return;
}

/*********************************************************************
*  Function Name : IPvxDeleteIPv6Add 
*  Description   : Deleting Ipv6 Address
*  Input(s)      : u4Ip6IfIndex - Interface Index
*                  Ip6Addr - Ipv6 Addressto be deleted
*                  i4PrefixLen - Prefix Length
*                  i4AddrType - Address type
*  Output(s)     :  
*  Return Values : None.
*********************************************************************/

VOID
IPvxDeleteIPv6Add (UINT4 u4Ip6IfIndex, tIp6Addr Ip6Addr,
                   UINT4 i4PrefixLen, INT4 i4AddrType)
{
    UINT4               u4ErrorCode = IPVX_ZERO;
    tSNMP_OCTET_STRING_TYPE Addr;
    tSNMP_OCTET_STRING_TYPE IfToken;
    UINT1               au1AddrOctetList[IP6_ADDR_SIZE];
    UINT1               au1IfTokenOctetList[IP6_EUI_ADDRESS_LEN];

    MEMSET (au1AddrOctetList, IP6_ZERO, IP6_ADDR_SIZE);
    MEMSET (au1IfTokenOctetList, IP6_ZERO, IP6_EUI_ADDRESS_LEN);

    Addr.pu1_OctetList = au1AddrOctetList;
    MEMCPY (Addr.pu1_OctetList, &Ip6Addr, IP6_ADDR_SIZE);
    Addr.i4_Length = IP6_ADDR_SIZE;

    /*
     * EUI-64 address configuration is supported only for CLI. When the
     * address type is set as EUI-64 form the fetch the interface
     * identifier value and form the complete IP6 address to be configured.
     */
    if (i4AddrType == ADDR6_UNSPECIFIED)
    {                            /* EUI-64 */
        /* Length should not be greater than 64. */
        if (i4PrefixLen != IP6_EUI_ADDRESS_PREFIX_LEN)
        {
            return;
        }

        /* Form the Actual IP6 Address */
        IfToken.pu1_OctetList = au1IfTokenOctetList;
        IfToken.i4_Length = IP6_ZERO;

        if (nmhGetFsipv6IfToken (u4Ip6IfIndex, &IfToken) == SNMP_FAILURE)
        {
            return;
        }

        MEMCPY (&Addr.pu1_OctetList[IP6_EUI_ADDRESS_LEN],
                IfToken.pu1_OctetList, IP6_EUI_ADDRESS_LEN);

        /* Also update the address type as UNICAST */
        i4AddrType = ADDR6_UNICAST;
    }

    if (i4AddrType != IP6_ZERO)
    {
        if ((nmhGetFsipv6AddrType ((INT4) u4Ip6IfIndex, &Addr, i4PrefixLen,
                                   &i4AddrType) == SNMP_FAILURE) ||
            (i4AddrType != i4AddrType))
        {
            return;
        }
    }

    /* ADMIN STATUS */
    if (nmhTestv2Fsipv6AddrAdminStatus (&u4ErrorCode,
                                        u4Ip6IfIndex, &Addr,
                                        i4PrefixLen,
                                        IP6FWD_DESTROY) == SNMP_FAILURE)
    {
        return;
    }

    /* First the Admin Status Should be set to 3 (invalid) */
    if (nmhSetFsipv6AddrAdminStatus ((INT4) u4Ip6IfIndex,
                                     &Addr,
                                     i4PrefixLen,
                                     IP6FWD_DESTROY) == SNMP_FAILURE)

    {
        return;
    }

    return;
}

/****************************************************************************
Function    :  Ip6GetRtrAdvtDefLifeTime
Input       :  i4RtrAdvtIfIndex    - Ip6IfIndex
                pu4RtrAdvtDefLifeTime - RA Table Default Life Time 
 Output      :  None
 Returns     :  SNMP_FAILURE
****************************************************************************/
INT1
Ip6GetRtrAdvtDefLifeTime (INT4 i4RtrAdvtIfIndex, UINT4 *pu4RtrAdvtDefLifeTime)
{
    tLip6If            *pIf6Entry = NULL;

    pIf6Entry = Lip6UtlGetIfEntry (i4RtrAdvtIfIndex);
    if (pIf6Entry != NULL)
    {
        *pu4RtrAdvtDefLifeTime = (UINT4) pIf6Entry->u4DefRaLifetime;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  Ip6SetRtrAdvtDefLifeTime
 Input       :  i4RtrAdvtIfIndex    - Ip6IfIndex
                u4RtrAdvtDefLifeTime - RA Table Default Life Time 
 Output      :  None
 Returns     :  SNMP_FAILURE
****************************************************************************/
INT1
Ip6SetRtrAdvtDefLifeTime (INT4 i4RtrAdvtIfIndex, UINT4 u4RtrAdvtDefLifeTime)
{
    tLip6If            *pIf6Entry = NULL;

    pIf6Entry = Lip6UtlGetIfEntry (i4RtrAdvtIfIndex);
    if (pIf6Entry != NULL)
    {
        pIf6Entry->u4DefRaLifetime = u4RtrAdvtDefLifeTime;

        if ((pIf6Entry->u1RAdvStatus == IP6_IF_ROUT_ADV_ENABLED) &&
            (pIf6Entry->u1OperStatus == NETIPV6_OPER_UP))
        {
            if (Lip6RAConfig (pIf6Entry->u4ContextId) == OSIX_FAILURE)
            {
                return SNMP_FAILURE;
            }
        }
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  Ip6TestRtrAdvtLinkMTU
 Input       :  pu4ErrorCode         - Error Code 
                i4RtrAdvtIfIndex     - Ip6IfIndex
                u4RtrAdvtLinkMTU     - RA Table Link MTU 
 Output      :  None
 Returns     :  SNMP_FAILURE
****************************************************************************/
INT1
Ip6TestRtrAdvtLinkMTU (UINT4 *pu4ErrorCode, INT4 i4RtrAdvtIfIndex,
                       UINT4 u4RtrAdvtLinkMTU)
{
    tLip6If            *pIf6Entry = NULL;

    pIf6Entry = Lip6UtlGetIfEntry (i4RtrAdvtIfIndex);
    if (pIf6Entry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    /* Skip the range check if Interface MTU was specified to get */
    if(u4RtrAdvtLinkMTU != IP6_INTERFACE_LINK_MTU)
    {
	    /* No Range Specified */
	    if ((u4RtrAdvtLinkMTU != IP6_ZERO) &&
			    ((u4RtrAdvtLinkMTU < IP6_RA_MIN_LINK_MTU) ||
			     (u4RtrAdvtLinkMTU > IP6_RA_MAX_LINK_MTU)))
	    {
		    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
		    CLI_SET_ERR (CLI_IP6_INVALID_RA_LINK_MTU);
		    return SNMP_FAILURE;
	    }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  Ip6SetRtrAdvtLinkMTU
 Input       :  i4RtrAdvtIfIndex     - Ip6IfIndex
                u4RtrAdvtLinkMTU     - RA Table Link MTU 
 Output      :  None
 Returns     :  SNMP_FAILURE
****************************************************************************/
INT1
Ip6SetRtrAdvtLinkMTU (INT4 i4RtrAdvtIfIndex, UINT4 u4RtrAdvtLinkMTU)
{
    tLip6If            *pIf6Entry = NULL;

    pIf6Entry = Lip6UtlGetIfEntry (i4RtrAdvtIfIndex);
    if (pIf6Entry != NULL)
    {
        pIf6Entry->u4RaMtu = u4RtrAdvtLinkMTU;

        if ((pIf6Entry->u1RAdvStatus == IP6_IF_ROUT_ADV_ENABLED) &&
            (pIf6Entry->u1OperStatus == NETIPV6_OPER_UP))
        {
            if (Lip6RAConfig (pIf6Entry->u4ContextId) == OSIX_FAILURE)
            {
                return SNMP_FAILURE;
            }
        }
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  Ip6SetRtrAdvtSendAdverts
 Input       :  i4RtrAdvtIfIndex     - Ip6IfIndex
                i4RtrAdvtSendAdverts - RA Table RA Send Status
 Output      :  None
 Returns     :  SNMP_SUCCESS/SNMP_FAILURE
****************************************************************************/
INT1
Ip6SetRtrAdvtSendAdverts (INT4 i4RtrAdvtIfIndex, INT4 i4RtrAdvtSendAdverts)
{
    tLip6If            *pIf6Entry = NULL;
    UINT1               u1NewRAStatus = IP6_IF_ROUT_ADV_DISABLED;

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4RtrAdvtIfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (i4RtrAdvtSendAdverts == IPVX_TRUTH_VALUE_TRUE)
    {
        u1NewRAStatus = IP6_IF_ROUT_ADV_ENABLED;
    }

    if (pIf6Entry->u1RAdvStatus == u1NewRAStatus)
    {
        return SNMP_SUCCESS;
    }

    pIf6Entry->u1RAdvStatus = u1NewRAStatus;

    if (pIf6Entry->u1OperStatus == NETIPV6_OPER_UP)
    {
        if (Lip6RAConfig (pIf6Entry->u4ContextId) == OSIX_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 * Function    :  Ip6SetRtrAdvertRDNSSOpen
 * Input       :  i4RtrAdvtIfIndex     - Ip6IfIndex
 *                u4RtrAdvertRDNSSOpen - RA Table Max RA Tx Interval
 * Output      :  None
 * Returns     :  SNMP_FAILURE
 * *****************************************************************************/
INT1
Ip6SetRtrAdvertRDNSSOpen (INT4 i4RtrAdvtIfIndex, UINT4 u4RtrAdvertRDNSSOpen)
{
    tLip6If            *pIf6Entry = NULL;
    UINT1               u1NewRAStatus = IP6_IF_ROUT_ADV_RDNSS_DISABLED;

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4RtrAdvtIfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }
    if ((UINT1) u4RtrAdvertRDNSSOpen == IPVX_TRUTH_VALUE_TRUE)
    {
        u1NewRAStatus = IP6_RA_RDNSS_OPEN;
    }

    if (pIf6Entry->u1RAdvRDNSSStatus == u1NewRAStatus)
    {
        return SNMP_SUCCESS;
    }

    pIf6Entry->u1RAdvRDNSSStatus = u1NewRAStatus;

    if (pIf6Entry->u1OperStatus == NETIPV6_OPER_UP)
    {
        if (Lip6RAConfig (pIf6Entry->u4ContextId) == OSIX_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 * Function    :  Ip6SetRtrAdvertRDNSS
 * Input       :  i4RtrAdvtIfIndex     - Ip6IfIndex
 *                u4RtrAdvertRDNSS - RA Table Max RA Tx Interval
 * Output      :  None
 * Returns     :  SNMP_FAILURE
 * *****************************************************************************/
INT1
Ip6SetRtrAdvertRDNSS (INT4 i4RtrAdvtIfIndex, UINT4 u4RtrAdvertRDNSS)
{
    tLip6If            *pIf6Entry = NULL;
    UINT1               u1NewRAStatus = IP6_IF_ROUT_ADV_NO_RDNSS;

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4RtrAdvtIfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    if ((UINT1) u4RtrAdvertRDNSS == IPVX_TRUTH_VALUE_TRUE)
    {
        u1NewRAStatus = IP6_IF_ROUT_ADV_RDNSS;
    }

    if (pIf6Entry->u1RAdvRDNSS == u1NewRAStatus)
    {
        return SNMP_SUCCESS;
    }

    pIf6Entry->u1RAdvRDNSS = u1NewRAStatus;
    if (pIf6Entry->u1OperStatus == NETIPV6_OPER_UP)
    {
        if (Lip6RAConfig (pIf6Entry->u4ContextId) == OSIX_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  Ip6SetRtrAdvtRDNSSLifeOne
 Input       :  pu4ErrorCode        - Error Code
                i4RtrAdvtIfIndex    - Ip6IfIndex
                u4RtrRDNSSLife  - RDNSS Lifetime
 Output      :  None
 Returns     :  SNMP_FAILURE
****************************************************************************/
INT1
Ip6SetRtrAdvtRDNSSLifeOne (INT4 i4RtrAdvtIfIndex, UINT4 u4RtrRDNSSLifeOne)
{
    tLip6If            *pIf6Entry = NULL;
    INT4                i4RetVal = 0;
    UINT4               u4OldVal = 0;

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4RtrAdvtIfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pIf6Entry->u4RDNSSLifetimeOne == u4RtrRDNSSLifeOne)
    {
        return SNMP_SUCCESS;
    }
    u4OldVal = pIf6Entry->u4RDNSSLifetimeOne;
    pIf6Entry->u4RDNSSLifetimeOne = u4RtrRDNSSLifeOne;
    if (pIf6Entry->u1OperStatus == NETIPV6_OPER_UP)
    {
        i4RetVal = Lip6RAConfig (pIf6Entry->u4ContextId);
        if (i4RetVal == OSIX_FAILURE)
        {
            pIf6Entry->u4RDNSSLifetimeOne = u4OldVal;
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;
}
/****************************************************************************
 *  * Function    :  Ip6SetRtrAdvertRDNSSOpen
 *   * Input       :  i4RtrAdvtIfIndex     - Ip6IfIndex
 *    *                u4RtrAdvertRDNSSOpen - RA Table Max RA Tx Interval
 *     * Output      :  None
 *      * Returns     :  SNMP_FAILURE
 *       * *****************************************************************************/
INT1
Ip6SetRaRDNSSAddress (INT4 i4RtrAdvtIfIndex, tIp6Addr Ip6RdnssAddr)
{
    tLip6If            *pIf6Entry = NULL;
    tIp6Addr            Ip6TempRDNSSAddr;
    MEMSET (&Ip6TempRDNSSAddr, 0, sizeof (tIp6Addr));

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4RtrAdvtIfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }
    if (Ip6AddrCompare (Ip6TempRDNSSAddr, Ip6RdnssAddr) == IP6_ZERO)
    {
        return SNMP_SUCCESS;
    }

    if (pIf6Entry->u1OperStatus == NETIPV6_OPER_UP)
    {
        if (Lip6RAConfig (pIf6Entry->u4ContextId) == OSIX_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  Ip6SetRtrAdvtMaxInterval
 Input       :  i4RtrAdvtIfIndex     - Ip6IfIndex
                u4RtrAdvtMaxInterval - RA Table Max RA Tx Interval
 Output      :  None
 Returns     :  SNMP_SUCCESS/SNMP_FAILURE
****************************************************************************/
INT1
Ip6SetRtrAdvtMaxInterval (INT4 i4RtrAdvtIfIndex, UINT4 u4RtrAdvtMaxInterval)
{
    tLip6If            *pIf6Entry = NULL;
    INT4                i4RetVal = 0;
    UINT4               u4OldVal = 0;

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4RtrAdvtIfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pIf6Entry->u4MaxRaTime == u4RtrAdvtMaxInterval)
    {
        return SNMP_SUCCESS;
    }

    u4OldVal = pIf6Entry->u4MaxRaTime;
    pIf6Entry->u4MaxRaTime = u4RtrAdvtMaxInterval;

    if ((pIf6Entry->u1RAdvStatus == IP6_IF_ROUT_ADV_ENABLED) &&
        (pIf6Entry->u1OperStatus == NETIPV6_OPER_UP))
    {
        i4RetVal = Lip6RAConfig (pIf6Entry->u4ContextId);
        if (i4RetVal == OSIX_FAILURE)
        {
            pIf6Entry->u4MaxRaTime = u4OldVal;
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  Ip6SetRtrAdvtRDNSSLifeTwo
 Input       :  pu4ErrorCode        - Error Code
                i4RtrAdvtIfIndex    - Ip6IfIndex
                u4RtrRDNSSLife  - RDNSS Lifetime
 Output      :  None
 Returns     :  SNMP_FAILURE
****************************************************************************/
INT1
Ip6SetRtrAdvtRDNSSLifeTwo (INT4 i4RtrAdvtIfIndex, UINT4 u4RtrRDNSSLifeTwo)
{
    tLip6If            *pIf6Entry = NULL;
    INT4                i4RetVal = 0;
    UINT4               u4OldVal = 0;

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4RtrAdvtIfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pIf6Entry->u4RDNSSLifetimeTwo == u4RtrRDNSSLifeTwo)
    {
        return SNMP_SUCCESS;
    }
    u4OldVal = pIf6Entry->u4RDNSSLifetimeTwo;
    pIf6Entry->u4RDNSSLifetimeTwo = u4RtrRDNSSLifeTwo;
    if (pIf6Entry->u1OperStatus == NETIPV6_OPER_UP)
    {
        i4RetVal = Lip6RAConfig (pIf6Entry->u4ContextId);
        if (i4RetVal == OSIX_FAILURE)
        {
            pIf6Entry->u4RDNSSLifetimeTwo = u4OldVal;
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  Ip6SetRtrAdvtRDNSSLifeThree
 Input       :  pu4ErrorCode        - Error Code
                i4RtrAdvtIfIndex    - Ip6IfIndex
                u4RtrRDNSSLife  - RDNSS Lifetime
 Output      :  None
 Returns     :  SNMP_FAILURE
****************************************************************************/
INT1
Ip6SetRtrAdvtRDNSSLifeThree (INT4 i4RtrAdvtIfIndex, UINT4 u4RtrRDNSSLifeThree)
{
    tLip6If            *pIf6Entry = NULL;
    INT4                i4RetVal = 0;
    UINT4               u4OldVal = 0;

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4RtrAdvtIfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pIf6Entry->u4RDNSSLifetimeThree == u4RtrRDNSSLifeThree)
    {
        return SNMP_SUCCESS;
    }
    u4OldVal = pIf6Entry->u4RDNSSLifetimeThree;
    pIf6Entry->u4RDNSSLifetimeThree = u4RtrRDNSSLifeThree;
    if (pIf6Entry->u1OperStatus == NETIPV6_OPER_UP)
    {
        i4RetVal = Lip6RAConfig (pIf6Entry->u4ContextId);
        if (i4RetVal == OSIX_FAILURE)
        {
            pIf6Entry->u4RDNSSLifetimeThree = u4OldVal;
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  Ip6SetRtrAdvtMinInterval
 Input       :  i4RtrAdvtIfIndex     - Ip6IfIndex
                u4RtrAdvtMinInterval - RA Table Min RA Tx Interval
 Output      :  None
 Returns     :  SNMP_FAILURE
****************************************************************************/
INT1
Ip6SetRtrAdvtMinInterval (INT4 i4RtrAdvtIfIndex, UINT4 u4RtrAdvtMinInterval)
{
    tLip6If            *pIf6Entry = NULL;
    INT4                i4RetVal = 0;
    UINT4               u4OldVal = 0;

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4RtrAdvtIfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pIf6Entry->u4MinRaTime == u4RtrAdvtMinInterval)
    {
        return SNMP_SUCCESS;
    }

    u4OldVal = pIf6Entry->u4MinRaTime;
    pIf6Entry->u4MinRaTime = u4RtrAdvtMinInterval;

    if ((pIf6Entry->u1RAdvStatus == IP6_IF_ROUT_ADV_ENABLED) &&
        (pIf6Entry->u1OperStatus == NETIPV6_OPER_UP))
    {
        i4RetVal = Lip6RAConfig (pIf6Entry->u4ContextId);
        if (i4RetVal == OSIX_FAILURE)
        {
            pIf6Entry->u4MinRaTime = u4OldVal;
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  Ip6SetRtrAdvtMFlag
 Input       :  i4RtrAdvtIfIndex     - Ip6IfIndex
                i4RtrAdvtMFlag - RA Table M Flag Value
 Output      :  None
 Returns     :  SNMP_FAILURE
****************************************************************************/
INT1
Ip6SetRtrAdvtMFlag (INT4 i4RtrAdvtIfIndex, INT4 i4RtrAdvtMFlag)
{
    tLip6If            *pIf6Entry = NULL;
    INT4                i4RetVal = 0;
    UINT1               u1OldVal = 0;

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4RtrAdvtIfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    u1OldVal = pIf6Entry->u1RAAdvFlags;

    if (((u1OldVal & IP6_IF_M_BIT_ADV) &&
         (i4RtrAdvtMFlag == IPVX_TRUTH_VALUE_TRUE)) &&
        (!(u1OldVal & IP6_IF_M_BIT_ADV) &&
         (i4RtrAdvtMFlag == IPVX_TRUTH_VALUE_FALSE)))
    {
        return SNMP_SUCCESS;
    }

    if (i4RtrAdvtMFlag == IPVX_TRUTH_VALUE_TRUE)
    {
        pIf6Entry->u1RAAdvFlags |= IP6_IF_M_BIT_ADV;
    }
    else
    {
        pIf6Entry->u1RAAdvFlags &= ~IP6_IF_M_BIT_ADV;
    }

    if ((pIf6Entry->u1RAdvStatus == IP6_IF_ROUT_ADV_ENABLED) &&
        (pIf6Entry->u1OperStatus == NETIPV6_OPER_UP))
    {
        i4RetVal = Lip6RAConfig (pIf6Entry->u4ContextId);
        if (i4RetVal == OSIX_FAILURE)
        {
            pIf6Entry->u1RAAdvFlags = u1OldVal;
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  Ip6SetRtrAdvtOFlag
 Input       :  i4RtrAdvtIfIndex     - Ip6IfIndex
                i4RtrAdvtOFlag - RA Table O Flag Value
 Output      :  None
 Returns     :  SNMP_FAILURE
****************************************************************************/
INT1
Ip6SetRtrAdvtOFlag (INT4 i4RtrAdvtIfIndex, INT4 i4RtrAdvtOFlag)
{
    tLip6If            *pIf6Entry = NULL;
    INT4                i4RetVal = 0;
    UINT1               u1OldVal = 0;

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4RtrAdvtIfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    u1OldVal = pIf6Entry->u1RAAdvFlags;

    if (((u1OldVal & IP6_IF_O_BIT_ADV) &&
         (i4RtrAdvtOFlag == IPVX_TRUTH_VALUE_TRUE)) &&
        (!(u1OldVal & IP6_IF_O_BIT_ADV) &&
         (i4RtrAdvtOFlag == IPVX_TRUTH_VALUE_FALSE)))
    {
        return SNMP_SUCCESS;
    }

    if (i4RtrAdvtOFlag == IPVX_TRUTH_VALUE_TRUE)
    {
        pIf6Entry->u1RAAdvFlags |= IP6_IF_O_BIT_ADV;
    }
    else
    {
        pIf6Entry->u1RAAdvFlags &= ~IP6_IF_O_BIT_ADV;
    }

    if ((pIf6Entry->u1RAdvStatus == IP6_IF_ROUT_ADV_ENABLED) &&
        (pIf6Entry->u1OperStatus == NETIPV6_OPER_UP))
    {
        i4RetVal = Lip6RAConfig (pIf6Entry->u4ContextId);
        if (i4RetVal == OSIX_FAILURE)
        {
            pIf6Entry->u1RAAdvFlags = u1OldVal;
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  Ip6SetRtrAdvtReachTime
 Input       :  i4RtrAdvtIfIndex    - Ip6IfIndex
                u4RtrAdvtReachTime - RA Table Reachable Time
 Output      :  None
 Returns     :  SNMP_FAILURE
****************************************************************************/
INT1
Ip6SetRtrAdvtReachTime (INT4 i4RtrAdvtIfIndex, UINT4 u4RtrAdvtReachTime)
{
    tLip6If            *pIf6Entry = NULL;
    INT4                i4RetVal = 0;
    UINT4               u4OldVal = 0;

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4RtrAdvtIfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pIf6Entry->u4RAReachableTime == u4RtrAdvtReachTime)
    {
        return SNMP_SUCCESS;
    }

    u4OldVal = pIf6Entry->u4RAReachableTime;
    pIf6Entry->u4RAReachableTime = u4RtrAdvtReachTime;

    if ((pIf6Entry->u1RAdvStatus == IP6_IF_ROUT_ADV_ENABLED) &&
        (pIf6Entry->u1OperStatus == NETIPV6_OPER_UP))
    {
        i4RetVal = Lip6RAConfig (pIf6Entry->u4ContextId);
        if (i4RetVal == OSIX_FAILURE)
        {
            pIf6Entry->u4RAReachableTime = u4OldVal;
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  Ip6SetRtrAdvtTxTime
 Input       :  i4RtrAdvtIfIndex    - Ip6IfIndex
                u4RtrAdvtTxTime - RA Table RetTransmit Time
 Output      :  None
 Returns     :  SNMP_FAILURE
****************************************************************************/
INT1
Ip6SetRtrAdvtTxTime (INT4 i4RtrAdvtIfIndex, UINT4 u4RtrAdvtTxTime)
{
    tLip6If            *pIf6Entry = NULL;
    INT4                i4RetVal = 0;
    UINT4               u4OldVal = 0;

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4RtrAdvtIfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pIf6Entry->u4RARetransTimer == u4RtrAdvtTxTime)
    {
        return SNMP_SUCCESS;
    }

    u4OldVal = pIf6Entry->u4RARetransTimer;
    pIf6Entry->u4RARetransTimer = u4RtrAdvtTxTime;

    if ((pIf6Entry->u1RAdvStatus == IP6_IF_ROUT_ADV_ENABLED) &&
        (pIf6Entry->u1OperStatus == NETIPV6_OPER_UP))
    {
        i4RetVal = Lip6RAConfig (pIf6Entry->u4ContextId);
        if (i4RetVal == OSIX_FAILURE)
        {
            pIf6Entry->u4RARetransTimer = u4OldVal;
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  Ip6SetRtrAdvtCurHopLimit
 Input       :  i4RtrAdvtIfIndex    - Ip6IfIndex
                u4RtrAdvtCurHopLimit - RA Table HopLimit
 Output      :  None
 Returns     :  SNMP_FAILURE
****************************************************************************/
INT1
Ip6SetRtrAdvtCurHopLimit (INT4 i4RtrAdvtIfIndex, UINT4 u4RtrAdvtCurHopLimit)
{
    tLip6If            *pIf6Entry = NULL;
    INT4                i4RetVal = 0;
    UINT4               u4OldVal = 0;

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4RtrAdvtIfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pIf6Entry->u4RACurHopLimit == u4RtrAdvtCurHopLimit)
    {
        return SNMP_SUCCESS;
    }

    u4OldVal = pIf6Entry->u4RACurHopLimit;
    pIf6Entry->u4RACurHopLimit = u4RtrAdvtCurHopLimit;

    if ((pIf6Entry->u1RAdvStatus == IP6_IF_ROUT_ADV_ENABLED) &&
        (pIf6Entry->u1OperStatus == NETIPV6_OPER_UP))
    {
        i4RetVal = Lip6RAConfig (pIf6Entry->u4ContextId);
        if (i4RetVal == OSIX_FAILURE)
        {
            pIf6Entry->u4RACurHopLimit = u4OldVal;
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  Ip6SetRtrAdvtRDNSSPref
 Input       :  pu4ErrorCode        - Error Code
                i4RtrAdvtIfIndex    - Ip6IfIndex
                u4RtrRDNSSPref  - RDNSS Preference
 Output      :  None
 Returns     :  SNMP_FAILURE
****************************************************************************/
INT1
Ip6SetRtrAdvtRDNSSPref (INT4 i4RtrAdvtIfIndex, UINT4 u4RtrRDNSSPref)
{
    tLip6If            *pIf6Entry = NULL;
    INT4                i4RetVal = 0;
    UINT4               u4OldVal = 0;

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4RtrAdvtIfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pIf6Entry->u4RDNSSPreference == u4RtrRDNSSPref)
    {
        return SNMP_SUCCESS;
    }

    u4OldVal = pIf6Entry->u4RDNSSPreference;
    pIf6Entry->u4RDNSSPreference = u4RtrRDNSSPref;

    if (pIf6Entry->u1OperStatus == NETIPV6_OPER_UP)
    {
        i4RetVal = Lip6RAConfig (pIf6Entry->u4ContextId);
        if (i4RetVal == OSIX_FAILURE)
        {
            pIf6Entry->u4RDNSSPreference = u4OldVal;
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  Ip6SetRtrAdvtRDNSSLife
 Input       :  pu4ErrorCode        - Error Code
                i4RtrAdvtIfIndex    - Ip6IfIndex
                u4RtrRDNSSLife  - RDNSS Lifetime
 Output      :  None
 Returns     :  SNMP_FAILURE
****************************************************************************/
INT1
Ip6SetRtrAdvtRDNSSLife (INT4 i4RtrAdvtIfIndex, UINT4 u4RtrRDNSSLife)
{
    tLip6If            *pIf6Entry = NULL;
    INT4                i4RetVal = 0;
    UINT4               u4OldVal = 0;

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4RtrAdvtIfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pIf6Entry->u4RDNSSLifetime == u4RtrRDNSSLife)
    {
        return SNMP_SUCCESS;
    }
    u4OldVal = pIf6Entry->u4RDNSSLifetime;
    pIf6Entry->u4RDNSSLifetime = u4RtrRDNSSLife;

    if (pIf6Entry->u1OperStatus == NETIPV6_OPER_UP)
    {
        i4RetVal = Lip6RAConfig (pIf6Entry->u4ContextId);
        if (i4RetVal == OSIX_FAILURE)
        {
            pIf6Entry->u4RDNSSLifetime = u4OldVal;
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  Ip6TestRtrAdvtRowStatus
 Input       :  pu4ErrorCode        - Error Code
                i4RtrAdvtIfIndex    - Ip6IfIndex
                i4RtrAdvtRowStatus  - RA Table RowStatus Value
 Output      :  None
 Returns     :  SNMP_FAILURE
****************************************************************************/
INT1
Ip6TestRtrAdvtRowStatus (UINT4 *pu4ErrorCode, INT4 i4RtrAdvtIfIndex,
                         INT4 i4RtrAdvtRowStatus)
{
    tLip6If            *pIf6Entry = NULL;

    UNUSED_PARAM (i4RtrAdvtRowStatus);

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4RtrAdvtIfIndex);

    if (pIf6Entry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    /* No need status - if IfEntry is valid RA rowstatus can be anything */
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  Ip6SetRtrAdvtRowStatus
 Input       :  i4RtrAdvtIfIndex    - Ip6IfIndex
                i4RtrAdvtRowStatus  - RA Table RowStatus Value
 Output      :  None
 Returns     :  SNMP_FAILURE
****************************************************************************/
INT1
Ip6SetRtrAdvtRowStatus (INT4 i4RtrAdvtIfIndex, INT4 i4RtrAdvtRowStatus)
{
    tLip6If            *pIf6Entry = NULL;
    UINT4               u4OldVal = 0;
    INT4                i4RetVal = 0;

    UNUSED_PARAM (i4RtrAdvtRowStatus);
    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4RtrAdvtIfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }
    u4OldVal = pIf6Entry->u1RAdvRDNSS;
    pIf6Entry->u1RAdvRDNSS = i4RtrAdvtRowStatus;
    if (pIf6Entry->u1OperStatus == NETIPV6_OPER_UP)
    {
        i4RetVal = Lip6RAConfig (pIf6Entry->u4ContextId);
        if (i4RetVal == OSIX_FAILURE)
        {
            pIf6Entry->u1RAdvRDNSS = u4OldVal;
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  Ip6GetRtrAdvtRowStatus
 Input       :  i4RtrAdvtIfIndex    - Ip6IfIndex
                pi4RtrAdvtRowStatus  - RA Table RowStatus Value
 Output      :  None
 Returns     :  SNMP_FAILURE
****************************************************************************/
INT1
Ip6GetRtrAdvtRowStatus (INT4 i4RtrAdvtIfIndex, INT4 *pi4RtrAdvtRowStatus)
{
    tLip6If            *pIf6Entry = NULL;

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4RtrAdvtIfIndex);

    if (pIf6Entry != NULL)
    {
        *pi4RtrAdvtRowStatus = (INT4) pIf6Entry->u1RAdvStatus;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  Ip6TestRtrAdvtReachTime
 Input       :  pu4ErrorCode        - Error Code
                i4RtrAdvtIfIndex    - Ip6IfIndex
                u4RtrAdvtReachTime - RA Table Reachable Time
 Output      :  None
 Returns     :  SNMP_FAILURE
****************************************************************************/
INT1
Ip6TestRtrAdvtReachTime (UINT4 *pu4ErrorCode, INT4 i4RtrAdvtIfIndex,
                         UINT4 u4RtrAdvtReachTime)
{
    INT1                i1RetVal = 0;

    i1RetVal =
        nmhTestv2Fsipv6IfReachableTime (pu4ErrorCode,
                                        i4RtrAdvtIfIndex, u4RtrAdvtReachTime);
    return i1RetVal;
}

/****************************************************************************
 Function    :  Ip6TestRtrAdvtTxTime
 Input       :  pu4ErrorCode        - Error Code
                i4RtrAdvtIfIndex    - Ip6IfIndex
                u4RtrAdvtTxTime - RA Table RetTransmit Time
 Output      :  None
 Returns     :  SNMP_FAILURE
****************************************************************************/
INT1
Ip6TestRtrAdvtTxTime (UINT4 *pu4ErrorCode, INT4 i4RtrAdvtIfIndex,
                      UINT4 u4RtrAdvtTxTime)
{
    INT1                i1RetVal = 0;

    i1RetVal =
        nmhTestv2Fsipv6IfRetransmitTime (pu4ErrorCode,
                                         i4RtrAdvtIfIndex, u4RtrAdvtTxTime);
    return i1RetVal;
}

/****************************************************************************
 Function    :  Ip6TestRtrAdvtCurHopLimit
 Input       :  pu4ErrorCode        - Error Code
                i4RtrAdvtIfIndex    - Ip6IfIndex
                u4RtrAdvtCurHopLimit - RA Table HopLimit
 Output      :  None
 Returns     :  SNMP_FAILURE
****************************************************************************/
INT1
Ip6TestRtrAdvtCurHopLimit (UINT4 *pu4ErrorCode, INT4 i4RtrAdvtIfIndex,
                           UINT4 u4RtrAdvtCurHopLimit)
{
    tLip6If            *pIf6Entry = NULL;

    if ((INT4)u4RtrAdvtCurHopLimit < IP6_RA_MIN_HOPLMT ||
        (INT4)u4RtrAdvtCurHopLimit > IP6_RA_MAX_HOPLMT)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_INVALID_HOPLIMIT);
        return SNMP_FAILURE;
    }

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4RtrAdvtIfIndex);
    if (pIf6Entry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    : Ip6TestRtrAdvtDefLifeTime 
 Input       :  pu4ErrorCode        - Error Code 
                i4RtrAdvtIfIndex    - Ip6IfIndex
                u4RtrAdvtDefLifeTime - RA Table Default Life Time 
 Output      :  None
 Returns     :  SNMP_FAILURE
****************************************************************************/
INT1
Ip6TestRtrAdvtDefLifeTime (UINT4 *pu4ErrorCode, INT4 i4RtrAdvtIfIndex,
                           UINT4 u4RtrAdvtDefLifeTime)
{
    tLip6If            *pIf6Entry = NULL;

    pIf6Entry = Lip6UtlGetIfEntry (i4RtrAdvtIfIndex);
    if (pIf6Entry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    /* Should be 0, or (>= MAX_RA_TIME && <= MAX_DEFTIME (9000)) Sec. */
    if (!((u4RtrAdvtDefLifeTime == IP6_ZERO) ||
          ((u4RtrAdvtDefLifeTime >= pIf6Entry->u4MaxRaTime) &&
           (u4RtrAdvtDefLifeTime <= IP6_IF_MAX_DEFTIME))))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_INVALID_RA_LIFETIME);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  Ip6TestRtrAdvtSendAdverts
 Input       :  pu4ErrorCode         - Error Code
                i4RtrAdvtIfIndex     - Ip6IfIndex
                i4RtrAdvtSendAdverts - RA Table RA Send Status
 Output      :  None
 Returns     :  SNMP_FAILURE
****************************************************************************/
INT1
Ip6TestRtrAdvtSendAdverts (UINT4 *pu4ErrorCode, INT4 i4RtrAdvtIfIndex,
                           INT4 i4RtrAdvtSendAdverts)
{
    tLip6If            *pIf6Entry = NULL;

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4RtrAdvtIfIndex);

    if (pIf6Entry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    /* Should be TRUE/FALSE */
    if ((i4RtrAdvtSendAdverts != IPVX_TRUTH_VALUE_TRUE) &&
        (i4RtrAdvtSendAdverts != IPVX_TRUTH_VALUE_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_INVALID_RT_ADV_STATUS);
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  Ip6TestRtrAdvtMaxInterval
 Input       :  pu4ErrorCode         - Error Code
                i4RtrAdvtIfIndex     - Ip6IfIndex
                u4RtrAdvtMaxInterval - RA Table Max RA Tx Interval
 Output      :  None
 Returns     :  SNMP_FAILURE
****************************************************************************/
INT1
Ip6TestRtrAdvtMaxInterval (UINT4 *pu4ErrorCode, INT4 i4RtrAdvtIfIndex,
                           UINT4 u4RtrAdvtMaxInterval)
{
    INT1                i1RetVal = 0;

    i1RetVal =
        nmhTestv2Fsipv6IfMaxRouterAdvTime (pu4ErrorCode,
                                           i4RtrAdvtIfIndex,
                                           u4RtrAdvtMaxInterval);
    return i1RetVal;
}

/****************************************************************************
 Function    :  Ip6TestRtrAdvtMinInterval
 Input       :  pu4ErrorCode         - Error Code
                i4RtrAdvtIfIndex     - Ip6IfIndex
                u4RtrAdvtMinInterval - RA Table Min RA Tx Interval
 Output      :  None
 Returns     :  SNMP_FAILURE
****************************************************************************/
INT1
Ip6TestRtrAdvtMinInterval (UINT4 *pu4ErrorCode, INT4 i4RtrAdvtIfIndex,
                           UINT4 u4RtrAdvtMinInterval)
{
    INT1                i1RetVal = 0;

    i1RetVal =
        nmhTestv2Fsipv6IfMinRouterAdvTime (pu4ErrorCode,
                                           i4RtrAdvtIfIndex,
                                           u4RtrAdvtMinInterval);
    return i1RetVal;
}

/****************************************************************************
 Function    :  Ip6TestRtrAdvtMFlag
 Input       :  pu4ErrorCode         - Error Code
                i4RtrAdvtIfIndex     - Ip6IfIndex
                i4RtrAdvtMFlag - RA Table M Flag Value
 Output      :  None
 Returns     :  SNMP_FAILURE
****************************************************************************/
INT1
Ip6TestRtrAdvtMFlag (UINT4 *pu4ErrorCode, INT4 i4RtrAdvtIfIndex,
                     INT4 i4RtrAdvtMFlag)
{
    tLip6If            *pIf6Entry = NULL;

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4RtrAdvtIfIndex);
    if (pIf6Entry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
        return SNMP_FAILURE;
    }

    if ((i4RtrAdvtMFlag != IPVX_TRUTH_VALUE_TRUE) &&
        (i4RtrAdvtMFlag != IPVX_TRUTH_VALUE_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_INVALID_RT_ADV_FLAGS);
        return (SNMP_FAILURE);
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  Ip6TestRtrAdvtOFlag
 Input       :  pu4ErrorCode         - Error Code
                i4RtrAdvtIfIndex     - Ip6IfIndex
                i4RtrAdvtOFlag  - RA Table O Flag Value
 Output      :  None
 Returns     :  SNMP_FAILURE
****************************************************************************/
INT1
Ip6TestRtrAdvtOFlag (UINT4 *pu4ErrorCode, INT4 i4RtrAdvtIfIndex,
                     INT4 i4RtrAdvtOFlag)
{
    tLip6If            *pIf6Entry = NULL;

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4RtrAdvtIfIndex);
    if (pIf6Entry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
        return SNMP_FAILURE;
    }

    /* Range Specified  TRUE/FAILS */
    if ((i4RtrAdvtOFlag != IPVX_TRUTH_VALUE_TRUE) &&
        (i4RtrAdvtOFlag != IPVX_TRUTH_VALUE_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_INVALID_RT_ADV_FLAGS);
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/*****************************************************************************
FUNCTION            Ip6GetIfReTransTime
DESCRIPTION         This function give the If Retransmit time if  the RA Entry
                    is created and the value is not ZERO otherwise
                    Default RetransmitTimer for the NODE.

Input(s)            u4IfIndex   -   If Index
Output(s)           None

RETURNS             RetransmitTime in MilliSeconds
****************************************************************************/
UINT4
Ip6GetIfReTransTime (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);
    return RETRANS_TIMER;
}

/*****************************************************************************
 * FUNCTION            Ip6GetAddressInfo
 * DESCRIPTION         This function gets the address status of the given IP6
 *                     address
 *                     
 * Input(s)            u4ContextId - Context Id
 *                     pIp6Addr - Pointer to Ip6 address
 * Output(s)          *pu4IfIndex - IfIndex of the address configured
 *                     pi4Prefix  - Prefix length of the IP6 address
 *                     pu1AddrType - Address type - Unicast/Anycast
 * RETURNS             SNMP_SUCCESS/SNMP_FAILURE
 *****************************************************************************/
UINT4
Ip6GetAddressInfo (UINT4 u4ContextId, tIp6Addr * pIp6Addr,
                   UINT4 *pu4IfIndex, INT4 *pi4Prefix, UINT1 *pu1AddrType)
{

    if (Lip6UtlGetIndexForAddr (pIp6Addr, pu4IfIndex, u4ContextId) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (Lip6UtlGetAddrPrefixType (pIp6Addr, pi4Prefix, pu1AddrType) ==
        OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/*****************************************************************************
 * FUNCTION            Ip6GetAddrAdminStatus
 * DESCRIPTION         This function gets the address status of the given IP6
 *                     address
 *                     
 * Input(s)            u4ContextId - Context Id
 *                     pIp6Addr - Pointer to Ip6 address
 * Output(s)          *pi4AddrRowStatus - Address row status
 * RETURNS             SNMP_SUCCESS/SNMP_FAILURE
 *****************************************************************************/
UINT4
Ip6GetAddrAdminStatus (UINT4 u4ContextId, tIp6Addr * pIp6Addr,
                       INT4 *pi4AddrRowStatus)
{
    tLip6AddrNode      *pAddrInfo = NULL;
    UINT4               u4IfIndex = 0;
    INT4                i4Prefix = 0;

    if (Lip6UtlGetIndexForAddr (pIp6Addr, &u4IfIndex, u4ContextId) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (Lip6UtlGetAddrPrefixLen (u4IfIndex, pIp6Addr,
                                 &i4Prefix) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    pAddrInfo = Lip6AddrGetEntry (u4IfIndex, pIp6Addr, i4Prefix);
    if (pAddrInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4AddrRowStatus = pAddrInfo->u1AdminStatus;

    return SNMP_SUCCESS;
}

/*****************************************************************************
 * FUNCTION            Ip6GetAddressStatus
 * DESCRIPTION         This function gets the address status of the given IP6
 *                     address
 *                     
 * Input(s)            u4ContextId - Context Id
 *                     pIp6Addr - Pointer to Ip6 address
 * Output(s)          *pi4AddrRowStatus - Address row status
 * RETURNS             SNMP_SUCCESS/SNMP_FAILURE
 *****************************************************************************/
UINT4
Ip6GetAddressStatus (UINT4 u4ContextId, tIp6Addr * pIp6Addr,
                     INT4 *pi4AddrStatus)
{
    UINT4               u4IfIndex = 0;
    if (Lip6UtlGetIndexForAddr (pIp6Addr, &u4IfIndex, u4ContextId) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (Lip6UtlGetAddrStatus (u4IfIndex, pIp6Addr,
                              pi4AddrStatus) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/*****************************************************************************
 * FUNCTION            Ip6GetNextAddressInfo
 * DESCRIPTION         This function gets the address status of the given IP6
 *                     address
 *                     
 * Input(s)            u4ContextId - Context Id
 *                     pIp6Addr - Pointer to Ip6 address
 * Output(s)          *pi4AddrRowStatus - Address row status
 * RETURNS             SNMP_SUCCESS/SNMP_FAILURE
 *****************************************************************************/
UINT4
Ip6GetNextAddressInfo (UINT4 u4ContextId, tIp6Addr * pIp6Addr,
                       tIp6Addr * pNextAddr, UINT4 *pu4NextIndex)
{
    UINT4               u4IfIndex = 0;
    INT4                i4NextPrefix = 0;

    if (Lip6UtlGetIndexForAddr (pIp6Addr, &u4IfIndex,u4ContextId) == OSIX_FAILURE)
    {
        u4IfIndex = 0;
    }

    if (Lip6AddrGetNext (u4IfIndex, pIp6Addr, pu4NextIndex,
                         pNextAddr, &i4NextPrefix) == OSIX_SUCCESS)
    {
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/*****************************************************************************
 * FUNCTION            Ip6AddrTblGetAddrInfo
 * DESCRIPTION         This function gets the address and prefix for the given
 *                     IfIndex
 *                     
 * Input(s)            u4IfIndex - Interface Index
 * Output(s)           pIp6Addr - Pointer to Ip6 address
 *                     pu1PrefixLen - Prefix Length of the Ip6 address
 * RETURNS             SNMP_SUCCESS/SNMP_FAILURE
 *****************************************************************************/
UINT4
Ip6AddrTblGetAddrInfo (UINT4 u4IfIndex, tIp6Addr * pIp6Addr, UINT1 *pu1Prefix)
{
    tIp6Addr            ZeroIp6Addr;
    UINT4               u4NextIndex = 0;
    INT4                i4NextPrefix = 0;

    MEMSET (&ZeroIp6Addr, 0, sizeof (tIp6Addr));

    if (Lip6AddrGetNext (u4IfIndex, &ZeroIp6Addr, &u4NextIndex,
                         pIp6Addr, &i4NextPrefix) == OSIX_SUCCESS)
    {
        if (u4NextIndex != u4IfIndex)
        {
            return SNMP_FAILURE;
        }

        *pu1Prefix = i4NextPrefix;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/*****************************************************************************
 * FUNCTION            Ip6ifEntryExists
 * DESCRIPTION         This function return IP6_SUCCESS if the IfEntry for the
 *                     given IfIndex is present, IP6_FAILURE otherwise
 *                     
 * Input(s)            u4IfIndex - Interface Index
 * Output(s)           None
 * RETURNS             IP6_SUCCESS/IP6_FAILURE
 *****************************************************************************/
INT1
Ip6ifEntryExists (UINT4 u4Index)
{
    if (Lip6UtlIfEntryExists (u4Index) == OSIX_FAILURE)
    {
        return IP6_FAILURE;
    }

    return IP6_SUCCESS;
}

/*****************************************************************************
 * FUNCTION            Ip6ifGetNextIndex
 * DESCRIPTION         This function returns next valid IfIndex on which 
 *                     IP6 is configured
 *                     
 * Input(s)            pu4Index - Interface Index
 * Output(s)           pu4Index - Pointer to next ifindex
 * RETURNS             IP6_SUCCESS/IP6_FAILURE
 *****************************************************************************/
INT4
Ip6ifGetNextIndex (UINT4 *pu4Index)
{
    UINT4               u4IfIndex = *pu4Index;

    if (Lip6UtlIfGetNextIndex (u4IfIndex, pu4Index) == OSIX_FAILURE)
    {
        return IP6_FAILURE;
    }

    return IP6_SUCCESS;
}

/*****************************************************************************
 * FUNCTION            CheckIp6IsFwdEnabled
 * DESCRIPTION         This function returns SNMP_SUCCESS if unicast forwarding
 *                     is enabled on the both global & interface level
 *
 * Input(s)            i4Index - Interface Index
 * RETURNS             SNMP_SUCCESS/SNMP_FAILURE
 *****************************************************************************/
INT4
CheckIp6IsFwdEnabled (INT4 i4Index)
{
    INT4                i4Ipv6FwdVal = IP6_FORW_DISABLE;
    nmhGetIpv6Forwarding (&i4Ipv6FwdVal);
    if ((Ip6IsIfFwdEnabled (i4Index) == FALSE)
        || (i4Ipv6FwdVal == IP6_FORW_DISABLE))
    {
        CLI_SET_ERR (CLI_IP6_FWD_DIS_RA_CANT_ALLOW);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}


/*****************************************************************************
 *FUNCTION           Ip6SetAdvDefaultPreference
 *DESCRIPTION        Setting router preference 
 *                  
 * Input(s)            i4Index - Interface Index
 * RETURNS             SNMP_SUCCESS/SNMP_FAILURE
 ******************************************************************************/
INT1
Ip6SetAdvDefaultPreference(INT4 i4AdvDefPrefIfIndex, INT4 i4DefRouterPreference)
{
    tLip6If            *pIf6Entry = NULL;
    INT4                i4RetVal = 0;
    UINT4               u4OldVal = 0;

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4AdvDefPrefIfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pIf6Entry->u2Preference == (UINT2) i4DefRouterPreference)
    {
        return SNMP_SUCCESS;
    }
    u4OldVal = pIf6Entry->u2Preference;
    pIf6Entry->u2Preference = (UINT2) i4DefRouterPreference;
    
    i4RetVal = Lip6RAConfig (pIf6Entry->u4ContextId);

    if (i4RetVal == OSIX_FAILURE)
    {
        pIf6Entry->u2Preference = (UINT2) u4OldVal;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

#endif /* __IP6_IPVX_C__ */

/****************************   End of the File ***************************/
