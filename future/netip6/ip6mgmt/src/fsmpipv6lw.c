/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmpipv6lw.c,v 1.18 2015/02/12 11:55:57 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "ip6inc.h"
# include  "fssnmp.h"
# include  "ip6cli.h"
# include  "fsmpipv6lw.h"

extern UINT4        FsMIIpv6ContextDebug[13];
/* LOW LEVEL Routines for Table : FsMIIpv6ContextTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIIpv6ContextTable
 Input       :  The Indices
                FsMIStdIpContextId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIIpv6ContextTable (INT4 i4FsMIStdIpContextId)
{
    if ((i4FsMIStdIpContextId < VCM_DEFAULT_CONTEXT) ||
        ((UINT4) i4FsMIStdIpContextId >= IP6_MAX_INSTANCES))
    {
        return SNMP_FAILURE;
    }

    if (Ip6VRExists (i4FsMIStdIpContextId) == IP6_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIIpv6ContextTable
 Input       :  The Indices
                FsMIStdIpContextId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIIpv6ContextTable (INT4 *pi4FsMIStdIpContextId)
{
    if (Ip6UtilGetFirstCxtId ((UINT4 *) pi4FsMIStdIpContextId) == IP6_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIIpv6ContextTable
 Input       :  The Indices
                FsMIStdIpContextId
                nextFsMIStdIpContextId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIIpv6ContextTable (INT4 i4FsMIStdIpContextId,
                                     INT4 *pi4NextFsMIStdIpContextId)
{
    if (Ip6UtilGetNextCxtId ((UINT4) i4FsMIStdIpContextId,
                             (UINT4 *) pi4NextFsMIStdIpContextId) ==
        IP6_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIIpv6NdCacheMaxRetries
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                retValFsMIIpv6NdCacheMaxRetries
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6NdCacheMaxRetries (INT4 i4FsMIStdIpContextId,
                                 INT4 *pi4RetValFsMIIpv6NdCacheMaxRetries)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (Ip6SelectContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }

    i1RetVal =
        nmhGetFsipv6NdCacheMaxRetries (pi4RetValFsMIIpv6NdCacheMaxRetries);
    Ip6ReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6PmtuConfigStatus
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                retValFsMIIpv6PmtuConfigStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6PmtuConfigStatus (INT4 i4FsMIStdIpContextId,
                                INT4 *pi4RetValFsMIIpv6PmtuConfigStatus)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (Ip6SelectContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }

    i1RetVal = nmhGetFsipv6PmtuConfigStatus (pi4RetValFsMIIpv6PmtuConfigStatus);
    Ip6ReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6PmtuTimeOutInterval
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                retValFsMIIpv6PmtuTimeOutInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6PmtuTimeOutInterval (INT4 i4FsMIStdIpContextId,
                                   UINT4 *pu4RetValFsMIIpv6PmtuTimeOutInterval)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (Ip6SelectContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }

    i1RetVal =
        nmhGetFsipv6PmtuTimeOutInterval (pu4RetValFsMIIpv6PmtuTimeOutInterval);
    Ip6ReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6JumboEnable
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                retValFsMIIpv6JumboEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6JumboEnable (INT4 i4FsMIStdIpContextId,
                           INT4 *pi4RetValFsMIIpv6JumboEnable)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (Ip6SelectContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }

    i1RetVal = nmhGetFsipv6JumboEnable (pi4RetValFsMIIpv6JumboEnable);
    Ip6ReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6NumOfSendJumbo
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                retValFsMIIpv6NumOfSendJumbo
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6NumOfSendJumbo (INT4 i4FsMIStdIpContextId,
                              INT4 *pi4RetValFsMIIpv6NumOfSendJumbo)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (Ip6SelectContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }

    i1RetVal = nmhGetFsipv6NumOfSendJumbo (pi4RetValFsMIIpv6NumOfSendJumbo);
    Ip6ReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6NumOfRecvJumbo
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                retValFsMIIpv6NumOfRecvJumbo
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6NumOfRecvJumbo (INT4 i4FsMIStdIpContextId,
                              INT4 *pi4RetValFsMIIpv6NumOfRecvJumbo)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (Ip6SelectContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }

    i1RetVal = nmhGetFsipv6NumOfRecvJumbo (pi4RetValFsMIIpv6NumOfRecvJumbo);
    Ip6ReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6ErrJumbo
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                retValFsMIIpv6ErrJumbo
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6ErrJumbo (INT4 i4FsMIStdIpContextId,
                        INT4 *pi4RetValFsMIIpv6ErrJumbo)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (Ip6SelectContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }

    i1RetVal = nmhGetFsipv6ErrJumbo (pi4RetValFsMIIpv6ErrJumbo);
    Ip6ReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6ContextDebug
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                retValFsMIIpv6ContextDebug
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6ContextDebug (INT4 i4FsMIStdIpContextId,
                            UINT4 *pu4RetValFsMIIpv6ContextDebug)
{
    if (Ip6SelectContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (gIp6GblInfo.pIp6CurrCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsMIIpv6ContextDebug = gIp6GblInfo.pIp6CurrCxt->u4Ip6Dbg;
    Ip6ReleaseContext ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6RFC5095Compatibility
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                retValFsMIIpv6RFC5095Compatibility
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6RFC5095Compatibility (INT4 i4FsMIStdIpContextId,
                                    INT4 *pi4RetValFsMIIpv6RFC5095Compatibility)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (Ip6SelectContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }

    i1RetVal =
        nmhGetFsipv6RFC5095Compatibility
        (pi4RetValFsMIIpv6RFC5095Compatibility);

    Ip6ReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6RFC5942Compatibility
 Input       :  The Indices
                FsMIStdIpContextId

                The Object
                retValFsMIIpv6RFC5942Compatibility
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6RFC5942Compatibility (INT4 i4FsMIStdIpContextId,
                                    INT4 *pi4RetValFsMIIpv6RFC5942Compatibility)
{
    UNUSED_PARAM (i4FsMIStdIpContextId);
    UNUSED_PARAM (pi4RetValFsMIIpv6RFC5942Compatibility);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6SENDSecLevel
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                retValFsMIIpv6SENDSecLevel
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsMIIpv6SENDSecLevel(INT4 i4FsMIStdIpContextId , INT4 *pi4RetValFsMIIpv6SENDSecLevel)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (Ip6SelectContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }

    i1RetVal = nmhGetFsipv6SENDSecLevel (pi4RetValFsMIIpv6SENDSecLevel);
    Ip6ReleaseContext ();
    return i1RetVal;
}
/****************************************************************************
 Function    :  nmhGetFsMIIpv6SENDNbrSecLevel
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                retValFsMIIpv6SENDNbrSecLevel
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsMIIpv6SENDNbrSecLevel(INT4 i4FsMIStdIpContextId , INT4 *pi4RetValFsMIIpv6SENDNbrSecLevel)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (Ip6SelectContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }

    i1RetVal = nmhGetFsipv6SENDNbrSecLevel (pi4RetValFsMIIpv6SENDNbrSecLevel);
    Ip6ReleaseContext ();
    return i1RetVal;
}
/****************************************************************************
 Function    :  nmhGetFsMIIpv6SENDAuthType
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                retValFsMIIpv6SENDAuthType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsMIIpv6SENDAuthType(INT4 i4FsMIStdIpContextId , INT4 *pi4RetValFsMIIpv6SENDAuthType)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (Ip6SelectContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }

    i1RetVal = nmhGetFsipv6SENDAuthType (pi4RetValFsMIIpv6SENDAuthType);
    Ip6ReleaseContext ();
    return i1RetVal;
}
/****************************************************************************
 Function    :  nmhGetFsMIIpv6SENDMinBits
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                retValFsMIIpv6SENDMinBits
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsMIIpv6SENDMinBits(INT4 i4FsMIStdIpContextId , INT4 *pi4RetValFsMIIpv6SENDMinBits)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (Ip6SelectContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }

    i1RetVal = nmhGetFsipv6SENDMinBits (pi4RetValFsMIIpv6SENDMinBits);
    Ip6ReleaseContext ();
    return i1RetVal;
}
/****************************************************************************
 Function    :  nmhGetFsMIIpv6SENDSecDAD
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                retValFsMIIpv6SENDSecDAD
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsMIIpv6SENDSecDAD(INT4 i4FsMIStdIpContextId , INT4 *pi4RetValFsMIIpv6SENDSecDAD)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (Ip6SelectContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }

    i1RetVal = nmhGetFsipv6SENDSecDAD (pi4RetValFsMIIpv6SENDSecDAD);
    Ip6ReleaseContext ();
    return i1RetVal;
}
/****************************************************************************
 Function    :  nmhGetFsMIIpv6SENDPrefixChk
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                retValFsMIIpv6SENDPrefixChk
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsMIIpv6SENDPrefixChk(INT4 i4FsMIStdIpContextId , INT4 *pi4RetValFsMIIpv6SENDPrefixChk)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (Ip6SelectContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }

    i1RetVal = nmhGetFsipv6SENDPrefixChk (pi4RetValFsMIIpv6SENDPrefixChk);
    Ip6ReleaseContext ();
    return i1RetVal;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIIpv6NdCacheMaxRetries
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                setValFsMIIpv6NdCacheMaxRetries
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpv6NdCacheMaxRetries (INT4 i4FsMIStdIpContextId,
                                 INT4 i4SetValFsMIIpv6NdCacheMaxRetries)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (Ip6SelectContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }

    i1RetVal =
        nmhSetFsipv6NdCacheMaxRetries (i4SetValFsMIIpv6NdCacheMaxRetries);
    Ip6ReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIIpv6PmtuConfigStatus
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                setValFsMIIpv6PmtuConfigStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpv6PmtuConfigStatus (INT4 i4FsMIStdIpContextId,
                                INT4 i4SetValFsMIIpv6PmtuConfigStatus)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (Ip6SelectContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }

    i1RetVal = nmhSetFsipv6PmtuConfigStatus (i4SetValFsMIIpv6PmtuConfigStatus);
    Ip6ReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIIpv6PmtuTimeOutInterval
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                setValFsMIIpv6PmtuTimeOutInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpv6PmtuTimeOutInterval (INT4 i4FsMIStdIpContextId,
                                   UINT4 u4SetValFsMIIpv6PmtuTimeOutInterval)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (Ip6SelectContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }

    i1RetVal =
        nmhSetFsipv6PmtuTimeOutInterval (u4SetValFsMIIpv6PmtuTimeOutInterval);
    Ip6ReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIIpv6JumboEnable
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                setValFsMIIpv6JumboEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpv6JumboEnable (INT4 i4FsMIStdIpContextId,
                           INT4 i4SetValFsMIIpv6JumboEnable)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (Ip6SelectContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }

    i1RetVal = nmhSetFsipv6JumboEnable (i4SetValFsMIIpv6JumboEnable);
    Ip6ReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIIpv6ContextDebug
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                setValFsMIIpv6ContextDebug
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpv6ContextDebug (INT4 i4FsMIStdIpContextId,
                            UINT4 u4SetValFsMIIpv6ContextDebug)
{
    UINT4               u4ContextId = 0;

    if (Ip6SelectContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (gIp6GblInfo.pIp6CurrCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    gIp6GblInfo.pIp6CurrCxt->u4Ip6Dbg = u4SetValFsMIIpv6ContextDebug;
    u4ContextId = gIp6GblInfo.pIp6CurrCxt->u4ContextId;
    Ip6ReleaseContext ();
    IncMsrForIpv6ContextTable (u4ContextId, 'u', &u4SetValFsMIIpv6ContextDebug,
                               FsMIIpv6ContextDebug,
                               sizeof (FsMIIpv6ContextDebug) / sizeof (UINT4));

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMIIpv6RFC5095Compatibility
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                setValFsMIIpv6RFC5095Compatibility
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpv6RFC5095Compatibility (INT4 i4FsMIStdIpContextId,
                                    INT4 i4SetValFsMIIpv6RFC5095Compatibility)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (Ip6SelectContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }
    i1RetVal =
        nmhSetFsipv6RFC5095Compatibility (i4SetValFsMIIpv6RFC5095Compatibility);
    Ip6ReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIIpv6RFC5942Compatibility
 Input       :  The Indices
                FsMIStdIpContextId

                The Object
                setValFsMIIpv6RFC5942Compatibility
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpv6RFC5942Compatibility (INT4 i4FsMIStdIpContextId,
                                    INT4 i4SetValFsMIIpv6RFC5942Compatibility)
{
    UNUSED_PARAM (i4FsMIStdIpContextId);
    UNUSED_PARAM (i4SetValFsMIIpv6RFC5942Compatibility);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMIIpv6SENDSecLevel
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                setValFsMIIpv6SENDSecLevel
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsMIIpv6SENDSecLevel(INT4 i4FsMIStdIpContextId , INT4 i4SetValFsMIIpv6SENDSecLevel)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (Ip6SelectContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }

    i1RetVal = nmhSetFsipv6SENDSecLevel (i4SetValFsMIIpv6SENDSecLevel);
    Ip6ReleaseContext ();
    return i1RetVal;

}
/****************************************************************************
 Function    :  nmhSetFsMIIpv6SENDNbrSecLevel
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                setValFsMIIpv6SENDNbrSecLevel
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsMIIpv6SENDNbrSecLevel(INT4 i4FsMIStdIpContextId , INT4 i4SetValFsMIIpv6SENDNbrSecLevel)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (Ip6SelectContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }

    i1RetVal = nmhSetFsipv6SENDNbrSecLevel (i4SetValFsMIIpv6SENDNbrSecLevel);
    Ip6ReleaseContext ();
    return i1RetVal;
}
/****************************************************************************
 Function    :  nmhSetFsMIIpv6SENDAuthType
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                setValFsMIIpv6SENDAuthType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsMIIpv6SENDAuthType(INT4 i4FsMIStdIpContextId , INT4 i4SetValFsMIIpv6SENDAuthType)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (Ip6SelectContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }

    i1RetVal = nmhSetFsipv6SENDAuthType (i4SetValFsMIIpv6SENDAuthType);
    Ip6ReleaseContext ();
    return i1RetVal;
}
/****************************************************************************
 Function    :  nmhSetFsMIIpv6SENDMinBits
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                setValFsMIIpv6SENDMinBits
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsMIIpv6SENDMinBits(INT4 i4FsMIStdIpContextId , INT4 i4SetValFsMIIpv6SENDMinBits)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (Ip6SelectContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }

    i1RetVal = nmhSetFsipv6SENDMinBits (i4SetValFsMIIpv6SENDMinBits);
    Ip6ReleaseContext ();
    return i1RetVal;
}
/****************************************************************************
 Function    :  nmhSetFsMIIpv6SENDSecDAD
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                setValFsMIIpv6SENDSecDAD
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsMIIpv6SENDSecDAD(INT4 i4FsMIStdIpContextId , INT4 i4SetValFsMIIpv6SENDSecDAD)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (Ip6SelectContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }

    i1RetVal = nmhSetFsipv6SENDSecDAD (i4SetValFsMIIpv6SENDSecDAD);
    Ip6ReleaseContext ();
    return i1RetVal;

}
/****************************************************************************
 Function    :  nmhSetFsMIIpv6SENDPrefixChk
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                setValFsMIIpv6SENDPrefixChk
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsMIIpv6SENDPrefixChk(INT4 i4FsMIStdIpContextId , INT4 i4SetValFsMIIpv6SENDPrefixChk)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (Ip6SelectContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }

    i1RetVal = nmhSetFsipv6SENDPrefixChk (i4SetValFsMIIpv6SENDPrefixChk);
    Ip6ReleaseContext ();
    return i1RetVal;
}
/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6NdCacheMaxRetries
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                testValFsMIIpv6NdCacheMaxRetries
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpv6NdCacheMaxRetries (UINT4 *pu4ErrorCode,
                                    INT4 i4FsMIStdIpContextId,
                                    INT4 i4TestValFsMIIpv6NdCacheMaxRetries)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (Ip6SelectContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1RetVal;
    }

    i1RetVal =
        nmhTestv2Fsipv6NdCacheMaxRetries (pu4ErrorCode,
                                          i4TestValFsMIIpv6NdCacheMaxRetries);
    Ip6ReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6PmtuConfigStatus
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                testValFsMIIpv6PmtuConfigStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpv6PmtuConfigStatus (UINT4 *pu4ErrorCode,
                                   INT4 i4FsMIStdIpContextId,
                                   INT4 i4TestValFsMIIpv6PmtuConfigStatus)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (Ip6SelectContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1RetVal;
    }

    i1RetVal =
        nmhTestv2Fsipv6PmtuConfigStatus (pu4ErrorCode,
                                         i4TestValFsMIIpv6PmtuConfigStatus);
    Ip6ReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6PmtuTimeOutInterval
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                testValFsMIIpv6PmtuTimeOutInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpv6PmtuTimeOutInterval (UINT4 *pu4ErrorCode,
                                      INT4 i4FsMIStdIpContextId,
                                      UINT4
                                      u4TestValFsMIIpv6PmtuTimeOutInterval)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (Ip6SelectContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1RetVal;
    }

    i1RetVal =
        nmhTestv2Fsipv6PmtuTimeOutInterval (pu4ErrorCode,
                                            u4TestValFsMIIpv6PmtuTimeOutInterval);
    Ip6ReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6JumboEnable
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                testValFsMIIpv6JumboEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpv6JumboEnable (UINT4 *pu4ErrorCode, INT4 i4FsMIStdIpContextId,
                              INT4 i4TestValFsMIIpv6JumboEnable)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (Ip6SelectContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1RetVal;
    }

    i1RetVal =
        nmhTestv2Fsipv6JumboEnable (pu4ErrorCode, i4TestValFsMIIpv6JumboEnable);
    Ip6ReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6ContextDebug
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                testValFsMIIpv6ContextDebug
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpv6ContextDebug (UINT4 *pu4ErrorCode, INT4 i4FsMIStdIpContextId,
                               UINT4 u4TestValFsMIIpv6ContextDebug)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4FsMIStdIpContextId);
    UNUSED_PARAM (u4TestValFsMIIpv6ContextDebug);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6RFC5095Compatibility
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                testValFsMIIpv6RFC5095Compatibility
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpv6RFC5095Compatibility (UINT4 *pu4ErrorCode,
                                       INT4 i4FsMIStdIpContextId,
                                       INT4
                                       i4TestValFsMIIpv6RFC5095Compatibility)
{
    INT1                i1RetVal = SNMP_FAILURE;
    if (Ip6SelectContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1RetVal;
    }

    i1RetVal =
        nmhTestv2Fsipv6RFC5095Compatibility (pu4ErrorCode,
                                             i4TestValFsMIIpv6RFC5095Compatibility);
    Ip6ReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6RFC5942Compatibility
 Input       :  The Indices
                FsMIStdIpContextId

                The Object
                testValFsMIIpv6RFC5942Compatibility
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpv6RFC5942Compatibility (UINT4 *pu4ErrorCode,
                                       INT4 i4FsMIStdIpContextId,
                                       INT4
                                       i4TestValFsMIIpv6RFC5942Compatibility)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4FsMIStdIpContextId);
    UNUSED_PARAM (i4TestValFsMIIpv6RFC5942Compatibility);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6SENDSecLevel
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                testValFsMIIpv6SENDSecLevel
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsMIIpv6SENDSecLevel(UINT4 *pu4ErrorCode , INT4 i4FsMIStdIpContextId , INT4 i4TestValFsMIIpv6SENDSecLevel)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (Ip6SelectContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1RetVal;
    }

    i1RetVal =
        nmhTestv2Fsipv6SENDSecLevel (pu4ErrorCode, i4TestValFsMIIpv6SENDSecLevel);
    Ip6ReleaseContext ();
    return i1RetVal;
}

/* LOW LEVEL Routines for Table : FsMIIpv6IfTable. */

/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6SENDNbrSecLevel
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                testValFsMIIpv6SENDNbrSecLevel
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsMIIpv6SENDNbrSecLevel(UINT4 *pu4ErrorCode , INT4 i4FsMIStdIpContextId , INT4 i4TestValFsMIIpv6SENDNbrSecLevel)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (Ip6SelectContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1RetVal;
    }

    i1RetVal =
        nmhTestv2Fsipv6SENDSecLevel (pu4ErrorCode, i4TestValFsMIIpv6SENDNbrSecLevel);
    Ip6ReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6SENDAuthType
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                testValFsMIIpv6SENDAuthType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsMIIpv6SENDAuthType(UINT4 *pu4ErrorCode , INT4 i4FsMIStdIpContextId , INT4 i4TestValFsMIIpv6SENDAuthType)
{
INT1                i1RetVal = SNMP_FAILURE;

    if (Ip6SelectContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1RetVal;
    }

    i1RetVal =
        nmhTestv2Fsipv6SENDAuthType (pu4ErrorCode, i4TestValFsMIIpv6SENDAuthType);
    Ip6ReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6SENDMinBits
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                testValFsMIIpv6SENDMinBits
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsMIIpv6SENDMinBits(UINT4 *pu4ErrorCode , INT4 i4FsMIStdIpContextId , INT4 i4TestValFsMIIpv6SENDMinBits)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (Ip6SelectContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1RetVal;
    }

    i1RetVal =
        nmhTestv2Fsipv6SENDMinBits (pu4ErrorCode, i4TestValFsMIIpv6SENDMinBits);
    Ip6ReleaseContext ();
    return i1RetVal;

}
/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6SENDSecDAD
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                testValFsMIIpv6SENDSecDAD
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsMIIpv6SENDSecDAD(UINT4 *pu4ErrorCode , INT4 i4FsMIStdIpContextId , INT4 i4TestValFsMIIpv6SENDSecDAD)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (Ip6SelectContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1RetVal;
    }

    i1RetVal =
        nmhTestv2Fsipv6SENDSecDAD (pu4ErrorCode, i4TestValFsMIIpv6SENDSecDAD);
    Ip6ReleaseContext ();
    return i1RetVal;

}
/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6SENDPrefixChk
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                testValFsMIIpv6SENDPrefixChk
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsMIIpv6SENDPrefixChk(UINT4 *pu4ErrorCode , INT4 i4FsMIStdIpContextId , INT4 i4TestValFsMIIpv6SENDPrefixChk)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (Ip6SelectContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1RetVal;
    }

    i1RetVal =
        nmhTestv2Fsipv6SENDPrefixChk (pu4ErrorCode, i4TestValFsMIIpv6SENDPrefixChk);
    Ip6ReleaseContext ();
    return i1RetVal;
}
/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIIpv6ContextTable
 Input       :  The Indices
                FsMIStdIpContextId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIIpv6ContextTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIIpv6IfTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIIpv6IfTable
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIIpv6IfTable (INT4 i4FsMIStdIpv6InterfaceIfIndex)
{
    if (nmhValidateIndexInstanceFsipv6IfTable (i4FsMIStdIpv6InterfaceIfIndex)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIIpv6IfTable
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIIpv6IfTable (INT4 *pi4FsMIStdIpv6InterfaceIfIndex)
{
    if (nmhGetFirstIndexFsipv6IfTable (pi4FsMIStdIpv6InterfaceIfIndex)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIIpv6IfTable
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex
                nextFsMIStdIpv6InterfaceIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIIpv6IfTable (INT4 i4FsMIStdIpv6InterfaceIfIndex,
                                INT4 *pi4NextFsMIStdIpv6InterfaceIfIndex)
{
    if (nmhGetNextIndexFsipv6IfTable (i4FsMIStdIpv6InterfaceIfIndex,
                                      pi4NextFsMIStdIpv6InterfaceIfIndex) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIIpv6IfType
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex

                The Object 
                retValFsMIIpv6IfType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6IfType (INT4 i4FsMIStdIpv6InterfaceIfIndex,
                      INT4 *pi4RetValFsMIIpv6IfType)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhGetFsipv6IfType (i4FsMIStdIpv6InterfaceIfIndex,
                                   pi4RetValFsMIIpv6IfType);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6IfPortNum
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex

                The Object 
                retValFsMIIpv6IfPortNum
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6IfPortNum (INT4 i4FsMIStdIpv6InterfaceIfIndex,
                         INT4 *pi4RetValFsMIIpv6IfPortNum)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhGetFsipv6IfPortNum (i4FsMIStdIpv6InterfaceIfIndex,
                                      pi4RetValFsMIIpv6IfPortNum);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6IfCircuitNum
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex

                The Object 
                retValFsMIIpv6IfCircuitNum
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6IfCircuitNum (INT4 i4FsMIStdIpv6InterfaceIfIndex,
                            INT4 *pi4RetValFsMIIpv6IfCircuitNum)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhGetFsipv6IfCircuitNum (i4FsMIStdIpv6InterfaceIfIndex,
                                         pi4RetValFsMIIpv6IfCircuitNum);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6IfToken
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex

                The Object 
                retValFsMIIpv6IfToken
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6IfToken (INT4 i4FsMIStdIpv6InterfaceIfIndex,
                       tSNMP_OCTET_STRING_TYPE * pRetValFsMIIpv6IfToken)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhGetFsipv6IfToken (i4FsMIStdIpv6InterfaceIfIndex,
                                    pRetValFsMIIpv6IfToken);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6IfOperStatus
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex

                The Object 
                retValFsMIIpv6IfOperStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6IfOperStatus (INT4 i4FsMIStdIpv6InterfaceIfIndex,
                            INT4 *pi4RetValFsMIIpv6IfOperStatus)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhGetFsipv6IfOperStatus (i4FsMIStdIpv6InterfaceIfIndex,
                                         pi4RetValFsMIIpv6IfOperStatus);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6IfRouterAdvStatus
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex

                The Object 
                retValFsMIIpv6IfRouterAdvStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6IfRouterAdvStatus (INT4 i4FsMIStdIpv6InterfaceIfIndex,
                                 INT4 *pi4RetValFsMIIpv6IfRouterAdvStatus)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhGetFsipv6IfRouterAdvStatus (i4FsMIStdIpv6InterfaceIfIndex,
                                              pi4RetValFsMIIpv6IfRouterAdvStatus);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6IfRouterAdvFlags
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex

                The Object 
                retValFsMIIpv6IfRouterAdvFlags
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6IfRouterAdvFlags (INT4 i4FsMIStdIpv6InterfaceIfIndex,
                                INT4 *pi4RetValFsMIIpv6IfRouterAdvFlags)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhGetFsipv6IfRouterAdvFlags (i4FsMIStdIpv6InterfaceIfIndex,
                                             pi4RetValFsMIIpv6IfRouterAdvFlags);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6IfHopLimit
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex

                The Object 
                retValFsMIIpv6IfHopLimit
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6IfHopLimit (INT4 i4FsMIStdIpv6InterfaceIfIndex,
                          INT4 *pi4RetValFsMIIpv6IfHopLimit)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhGetFsipv6IfHopLimit (i4FsMIStdIpv6InterfaceIfIndex,
                                       pi4RetValFsMIIpv6IfHopLimit);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6IfDefRouterTime
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex

                The Object 
                retValFsMIIpv6IfDefRouterTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6IfDefRouterTime (INT4 i4FsMIStdIpv6InterfaceIfIndex,
                               INT4 *pi4RetValFsMIIpv6IfDefRouterTime)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhGetFsipv6IfDefRouterTime (i4FsMIStdIpv6InterfaceIfIndex,
                                            pi4RetValFsMIIpv6IfDefRouterTime);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6IfReachableTime
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex

                The Object 
                retValFsMIIpv6IfReachableTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6IfReachableTime (INT4 i4FsMIStdIpv6InterfaceIfIndex,
                               INT4 *pi4RetValFsMIIpv6IfReachableTime)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhGetFsipv6IfReachableTime (i4FsMIStdIpv6InterfaceIfIndex,
                                            pi4RetValFsMIIpv6IfReachableTime);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6IfRetransmitTime
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex

                The Object 
                retValFsMIIpv6IfRetransmitTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6IfRetransmitTime (INT4 i4FsMIStdIpv6InterfaceIfIndex,
                                INT4 *pi4RetValFsMIIpv6IfRetransmitTime)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhGetFsipv6IfRetransmitTime (i4FsMIStdIpv6InterfaceIfIndex,
                                             pi4RetValFsMIIpv6IfRetransmitTime);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6IfDelayProbeTime
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex

                The Object 
                retValFsMIIpv6IfDelayProbeTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6IfDelayProbeTime (INT4 i4FsMIStdIpv6InterfaceIfIndex,
                                INT4 *pi4RetValFsMIIpv6IfDelayProbeTime)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhGetFsipv6IfDelayProbeTime (i4FsMIStdIpv6InterfaceIfIndex,
                                             pi4RetValFsMIIpv6IfDelayProbeTime);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6IfPrefixAdvStatus
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex

                The Object 
                retValFsMIIpv6IfPrefixAdvStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6IfPrefixAdvStatus (INT4 i4FsMIStdIpv6InterfaceIfIndex,
                                 INT4 *pi4RetValFsMIIpv6IfPrefixAdvStatus)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhGetFsipv6IfPrefixAdvStatus (i4FsMIStdIpv6InterfaceIfIndex,
                                              pi4RetValFsMIIpv6IfPrefixAdvStatus);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6IfMinRouterAdvTime
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex

                The Object 
                retValFsMIIpv6IfMinRouterAdvTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6IfMinRouterAdvTime (INT4 i4FsMIStdIpv6InterfaceIfIndex,
                                  INT4 *pi4RetValFsMIIpv6IfMinRouterAdvTime)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhGetFsipv6IfMinRouterAdvTime (i4FsMIStdIpv6InterfaceIfIndex,
                                               pi4RetValFsMIIpv6IfMinRouterAdvTime);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6IfMaxRouterAdvTime
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex

                The Object 
                retValFsMIIpv6IfMaxRouterAdvTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6IfMaxRouterAdvTime (INT4 i4FsMIStdIpv6InterfaceIfIndex,
                                  INT4 *pi4RetValFsMIIpv6IfMaxRouterAdvTime)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhGetFsipv6IfMaxRouterAdvTime (i4FsMIStdIpv6InterfaceIfIndex,
                                               pi4RetValFsMIIpv6IfMaxRouterAdvTime);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6IfDADRetries
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex

                The Object 
                retValFsMIIpv6IfDADRetries
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6IfDADRetries (INT4 i4FsMIStdIpv6InterfaceIfIndex,
                            INT4 *pi4RetValFsMIIpv6IfDADRetries)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhGetFsipv6IfDADRetries (i4FsMIStdIpv6InterfaceIfIndex,
                                         pi4RetValFsMIIpv6IfDADRetries);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6IfForwarding
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex

                The Object 
                retValFsMIIpv6IfForwarding
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6IfForwarding (UINT4 u4FsMIStdIpv6InterfaceIfIndex,
                            INT4 *pi4RetValFsMIIpv6IfForwarding)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhGetFsipv6IfForwarding (u4FsMIStdIpv6InterfaceIfIndex,
                                         pi4RetValFsMIIpv6IfForwarding);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6IfRoutingStatus
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex

                The Object 
                retValFsMIIpv6IfRoutingStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6IfRoutingStatus (UINT4 u4FsMIStdIpv6InterfaceIfIndex,
                               INT4 *pi4RetValFsMIIpv6IfRoutingStatus)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhGetFsipv6IfRoutingStatus (u4FsMIStdIpv6InterfaceIfIndex,
                                            pi4RetValFsMIIpv6IfRoutingStatus);
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6IfIcmpErrInterval
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex

                The Object
                retValFsMIIpv6IfIcmpErrInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6IfIcmpErrInterval (UINT4 u4FsMIStdIpv6InterfaceIfIndex,
                                 INT4 *pi4RetValFsMIIpv6IfIcmpErrInterval)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhGetFsipv6IfIcmpErrInterval (u4FsMIStdIpv6InterfaceIfIndex,
                                              pi4RetValFsMIIpv6IfIcmpErrInterval);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6IfIcmpTokenBucketSize
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex

                The Object
                retValFsMIIpv6IfIcmpTokenBucketSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6IfIcmpTokenBucketSize (UINT4 u4FsMIStdIpv6InterfaceIfIndex,
                                     INT4
                                     *pi4RetValFsMIIpv6IfIcmpTokenBucketSize)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhGetFsipv6IfIcmpTokenBucketSize (u4FsMIStdIpv6InterfaceIfIndex,
                                                  pi4RetValFsMIIpv6IfIcmpTokenBucketSize);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6IfDestUnreachableMsg
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex

                The Object
                retValFsMIIpv6IfDestUnreachableMsg
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6IfDestUnreachableMsg (UINT4 u4FsMIStdIpv6InterfaceIfIndex,
                                    INT4 *pi4RetValFsMIIpv6IfDestUnreachableMsg)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhGetFsipv6IfDestUnreachableMsg (u4FsMIStdIpv6InterfaceIfIndex,
                                                 pi4RetValFsMIIpv6IfDestUnreachableMsg);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6IfUnnumAssocIPIf
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex

                The Object 
                retValFsMIIpv6IfUnnumAssocIPIf
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6IfUnnumAssocIPIf (UINT4 u4FsMIStdIpv6InterfaceIfIndex,
                                INT4 *pi4RetValFsMIIpv6IfUnnumAssocIPIf)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal =
        nmhGetFsipv6IfUnnumAssocIPIf ((INT4) u4FsMIStdIpv6InterfaceIfIndex,
                                      pi4RetValFsMIIpv6IfUnnumAssocIPIf);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6IfRedirectMsg
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex

                The Object
                retValFsMIIpv6IfRedirectMsg
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6IfRedirectMsg (UINT4 u4FsMIStdIpv6InterfaceIfIndex,
                             INT4 *pi4RetValFsMIIpv6IfRedirectMsg)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhGetFsipv6IfRedirectMsg ((INT4) u4FsMIStdIpv6InterfaceIfIndex,
                                          pi4RetValFsMIIpv6IfRedirectMsg);

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6IfAdvSrcLLAdr
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex

                The Object
                retValFsMIIpv6IfAdvSrcLLAdr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6IfAdvSrcLLAdr (UINT4 u4FsMIStdIpv6InterfaceIfIndex,
                             INT4 *pi4RetValFsMIIpv6IfAdvSrcLLAdr)
{
    UNUSED_PARAM (u4FsMIStdIpv6InterfaceIfIndex);
    UNUSED_PARAM (pi4RetValFsMIIpv6IfAdvSrcLLAdr);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6IfAdvIntOpt
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex

                The Object
                retValFsMIIpv6IfAdvIntOpt
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6IfAdvIntOpt (UINT4 u4FsMIStdIpv6InterfaceIfIndex,
                           INT4 *pi4RetValFsMIIpv6IfAdvIntOpt)
{
    UNUSED_PARAM (u4FsMIStdIpv6InterfaceIfIndex);
    UNUSED_PARAM (pi4RetValFsMIIpv6IfAdvIntOpt);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6IfNDProxyAdminStatus
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex

                The Object
                retValFsMIIpv6IfNDProxyAdminStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6IfNDProxyAdminStatus (INT4 i4FsMIStdIpv6InterfaceIfIndex,
                                    INT4 *pi4RetValFsMIIpv6IfNDProxyAdminStatus)
{
    INT1                i1RetVal = SNMP_FAILURE;
    i1RetVal =
        nmhGetFsipv6IfNDProxyAdminStatus (i4FsMIStdIpv6InterfaceIfIndex,
                                          pi4RetValFsMIIpv6IfNDProxyAdminStatus);
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6IfNDProxyMode
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex

                The Object
                retValFsMIIpv6IfNDProxyMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6IfNDProxyMode (INT4 i4FsMIStdIpv6InterfaceIfIndex,
                             INT4 *pi4RetValFsMIIpv6IfNDProxyMode)
{
    INT1                i1RetVal = SNMP_FAILURE;
    i1RetVal =
        nmhGetFsipv6IfNDProxyMode (i4FsMIStdIpv6InterfaceIfIndex,
                                   pi4RetValFsMIIpv6IfNDProxyMode);

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6IfNDProxyOperStatus
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex

                The Object
                retValFsMIIpv6IfNDProxyOperStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6IfNDProxyOperStatus (INT4 i4FsMIStdIpv6InterfaceIfIndex,
                                   INT4 *pi4RetValFsMIIpv6IfNDProxyOperStatus)
{
    INT1                i1RetVal = SNMP_FAILURE;
    i1RetVal =
        nmhGetFsipv6IfNDProxyOperStatus (i4FsMIStdIpv6InterfaceIfIndex,
                                         pi4RetValFsMIIpv6IfNDProxyOperStatus);
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6IfNDProxyUpStream
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex

                The Object
                retValFsMIIpv6IfNDProxyUpStream
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6IfNDProxyUpStream (INT4 i4FsMIStdIpv6InterfaceIfIndex,
                                 INT4 *pi4RetValFsMIIpv6IfNDProxyUpStream)
{
    INT1                i1RetVal = SNMP_FAILURE;
    i1RetVal =
        nmhGetFsipv6IfNDProxyUpStream (i4FsMIStdIpv6InterfaceIfIndex,
                                       pi4RetValFsMIIpv6IfNDProxyUpStream);
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6IfSENDSecStatus
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex

                The Object 
                retValFsMIIpv6IfSENDSecStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsMIIpv6IfSENDSecStatus(INT4 i4FsMIStdIpv6InterfaceIfIndex , INT4 *pi4RetValFsMIIpv6IfSENDSecStatus)
{
    INT1                i1RetVal = SNMP_FAILURE;
    i1RetVal =
        nmhGetFsipv6IfSENDSecStatus (i4FsMIStdIpv6InterfaceIfIndex,
                                         pi4RetValFsMIIpv6IfSENDSecStatus);
    return (i1RetVal);

}
/****************************************************************************
 Function    :  nmhGetFsMIIpv6IfSENDDeltaTimestamp
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex

                The Object 
                retValFsMIIpv6IfSENDDeltaTimestamp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsMIIpv6IfSENDDeltaTimestamp(INT4 i4FsMIStdIpv6InterfaceIfIndex , UINT4 *pu4RetValFsMIIpv6IfSENDDeltaTimestamp)
{
    INT1                i1RetVal = SNMP_FAILURE;
    i1RetVal =
        nmhGetFsipv6IfSENDDeltaTimestamp (i4FsMIStdIpv6InterfaceIfIndex,
                                         pu4RetValFsMIIpv6IfSENDDeltaTimestamp);
    return (i1RetVal);

}
/****************************************************************************
 Function    :  nmhGetFsMIIpv6IfSENDFuzzTimestamp
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex

                The Object 
                retValFsMIIpv6IfSENDFuzzTimestamp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsMIIpv6IfSENDFuzzTimestamp(INT4 i4FsMIStdIpv6InterfaceIfIndex , UINT4 *pu4RetValFsMIIpv6IfSENDFuzzTimestamp)
{
    INT1                i1RetVal = SNMP_FAILURE;
    i1RetVal =
        nmhGetFsipv6IfSENDFuzzTimestamp (i4FsMIStdIpv6InterfaceIfIndex,
                                         pu4RetValFsMIIpv6IfSENDFuzzTimestamp);
    return (i1RetVal);
}
/****************************************************************************
 Function    :  nmhGetFsMIIpv6IfSENDDriftTimestamp
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex

                The Object 
                retValFsMIIpv6IfSENDDriftTimestamp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsMIIpv6IfSENDDriftTimestamp(INT4 i4FsMIStdIpv6InterfaceIfIndex , UINT4 *pu4RetValFsMIIpv6IfSENDDriftTimestamp)
{
    INT1                i1RetVal = SNMP_FAILURE;
    i1RetVal =
        nmhGetFsipv6IfSENDDriftTimestamp (i4FsMIStdIpv6InterfaceIfIndex,
                                         pu4RetValFsMIIpv6IfSENDDriftTimestamp);
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6IfDefRoutePreference
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex

                The Object 
                retValFsMIIpv6IfDefRoutePreference
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsMIIpv6IfDefRoutePreference(INT4 i4FsMIStdIpv6InterfaceIfIndex , INT4 *pi4RetValFsMIIpv6IfDefRoutePreference)
{
    INT1                i1RetVal = SNMP_FAILURE;
    i1RetVal = nmhGetFsipv6IfDefRoutePreference(i4FsMIStdIpv6InterfaceIfIndex,
                                          pi4RetValFsMIIpv6IfDefRoutePreference);
    return (i1RetVal); 
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIIpv6IfToken
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex

                The Object 
                setValFsMIIpv6IfToken
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpv6IfToken (INT4 i4FsMIStdIpv6InterfaceIfIndex,
                       tSNMP_OCTET_STRING_TYPE * pSetValFsMIIpv6IfToken)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhSetFsipv6IfToken (i4FsMIStdIpv6InterfaceIfIndex,
                                    pSetValFsMIIpv6IfToken);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIIpv6IfRouterAdvStatus
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex

                The Object 
                setValFsMIIpv6IfRouterAdvStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpv6IfRouterAdvStatus (INT4 i4FsMIStdIpv6InterfaceIfIndex,
                                 INT4 i4SetValFsMIIpv6IfRouterAdvStatus)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhSetFsipv6IfRouterAdvStatus (i4FsMIStdIpv6InterfaceIfIndex,
                                              i4SetValFsMIIpv6IfRouterAdvStatus);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIIpv6IfRouterAdvFlags
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex

                The Object 
                setValFsMIIpv6IfRouterAdvFlags
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpv6IfRouterAdvFlags (INT4 i4FsMIStdIpv6InterfaceIfIndex,
                                INT4 i4SetValFsMIIpv6IfRouterAdvFlags)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhSetFsipv6IfRouterAdvFlags (i4FsMIStdIpv6InterfaceIfIndex,
                                             i4SetValFsMIIpv6IfRouterAdvFlags);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIIpv6IfHopLimit
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex

                The Object 
                setValFsMIIpv6IfHopLimit
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpv6IfHopLimit (INT4 i4FsMIStdIpv6InterfaceIfIndex,
                          INT4 i4SetValFsMIIpv6IfHopLimit)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhSetFsipv6IfHopLimit (i4FsMIStdIpv6InterfaceIfIndex,
                                       i4SetValFsMIIpv6IfHopLimit);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIIpv6IfDefRouterTime
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex

                The Object 
                setValFsMIIpv6IfDefRouterTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpv6IfDefRouterTime (INT4 i4FsMIStdIpv6InterfaceIfIndex,
                               INT4 i4SetValFsMIIpv6IfDefRouterTime)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhSetFsipv6IfDefRouterTime (i4FsMIStdIpv6InterfaceIfIndex,
                                            i4SetValFsMIIpv6IfDefRouterTime);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIIpv6IfReachableTime
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex

                The Object 
                setValFsMIIpv6IfReachableTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpv6IfReachableTime (INT4 i4FsMIStdIpv6InterfaceIfIndex,
                               INT4 i4SetValFsMIIpv6IfReachableTime)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhSetFsipv6IfReachableTime (i4FsMIStdIpv6InterfaceIfIndex,
                                            i4SetValFsMIIpv6IfReachableTime);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIIpv6IfRetransmitTime
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex

                The Object 
                setValFsMIIpv6IfRetransmitTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpv6IfRetransmitTime (INT4 i4FsMIStdIpv6InterfaceIfIndex,
                                INT4 i4SetValFsMIIpv6IfRetransmitTime)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhSetFsipv6IfRetransmitTime (i4FsMIStdIpv6InterfaceIfIndex,
                                             i4SetValFsMIIpv6IfRetransmitTime);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIIpv6IfDelayProbeTime
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex

                The Object 
                setValFsMIIpv6IfDelayProbeTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpv6IfDelayProbeTime (INT4 i4FsMIStdIpv6InterfaceIfIndex,
                                INT4 i4SetValFsMIIpv6IfDelayProbeTime)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhSetFsipv6IfDelayProbeTime (i4FsMIStdIpv6InterfaceIfIndex,
                                             i4SetValFsMIIpv6IfDelayProbeTime);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIIpv6IfPrefixAdvStatus
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex

                The Object 
                setValFsMIIpv6IfPrefixAdvStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpv6IfPrefixAdvStatus (INT4 i4FsMIStdIpv6InterfaceIfIndex,
                                 INT4 i4SetValFsMIIpv6IfPrefixAdvStatus)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhSetFsipv6IfPrefixAdvStatus (i4FsMIStdIpv6InterfaceIfIndex,
                                              i4SetValFsMIIpv6IfPrefixAdvStatus);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIIpv6IfMinRouterAdvTime
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex

                The Object 
                setValFsMIIpv6IfMinRouterAdvTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpv6IfMinRouterAdvTime (INT4 i4FsMIStdIpv6InterfaceIfIndex,
                                  INT4 i4SetValFsMIIpv6IfMinRouterAdvTime)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhSetFsipv6IfMinRouterAdvTime (i4FsMIStdIpv6InterfaceIfIndex,
                                               i4SetValFsMIIpv6IfMinRouterAdvTime);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIIpv6IfMaxRouterAdvTime
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex

                The Object 
                setValFsMIIpv6IfMaxRouterAdvTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpv6IfMaxRouterAdvTime (INT4 i4FsMIStdIpv6InterfaceIfIndex,
                                  INT4 i4SetValFsMIIpv6IfMaxRouterAdvTime)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhSetFsipv6IfMaxRouterAdvTime (i4FsMIStdIpv6InterfaceIfIndex,
                                               i4SetValFsMIIpv6IfMaxRouterAdvTime);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIIpv6IfDADRetries
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex

                The Object 
                setValFsMIIpv6IfDADRetries
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpv6IfDADRetries (INT4 i4FsMIStdIpv6InterfaceIfIndex,
                            INT4 i4SetValFsMIIpv6IfDADRetries)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhSetFsipv6IfDADRetries (i4FsMIStdIpv6InterfaceIfIndex,
                                         i4SetValFsMIIpv6IfDADRetries);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIIpv6IfForwarding
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex

                The Object 
                setValFsMIIpv6IfForwarding
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpv6IfForwarding (UINT4 u4FsMIStdIpv6InterfaceIfIndex,
                            INT4 i4SetValFsMIIpv6IfForwarding)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhSetFsipv6IfForwarding (u4FsMIStdIpv6InterfaceIfIndex,
                                         i4SetValFsMIIpv6IfForwarding);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIIpv6IfIcmpErrInterval
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex

                The Object
                setValFsMIIpv6IfIcmpErrInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpv6IfIcmpErrInterval (UINT4 u4FsMIStdIpv6InterfaceIfIndex,
                                 INT4 i4SetValFsMIIpv6IfIcmpErrInterval)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhSetFsipv6IfIcmpErrInterval (u4FsMIStdIpv6InterfaceIfIndex,
                                              i4SetValFsMIIpv6IfIcmpErrInterval);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIIpv6IfIcmpTokenBucketSize
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex

                The Object
                setValFsMIIpv6IfIcmpTokenBucketSize
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpv6IfIcmpTokenBucketSize (UINT4 u4FsMIStdIpv6InterfaceIfIndex,
                                     INT4 i4SetValFsMIIpv6IfIcmpTokenBucketSize)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhSetFsipv6IfIcmpTokenBucketSize (u4FsMIStdIpv6InterfaceIfIndex,
                                                  i4SetValFsMIIpv6IfIcmpTokenBucketSize);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIIpv6IfDestUnreachableMsg
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex

                The Object
                setValFsMIIpv6IfDestUnreachableMsg
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpv6IfDestUnreachableMsg (UINT4 u4FsMIStdIpv6InterfaceIfIndex,
                                    INT4 i4SetValFsMIIpv6IfDestUnreachableMsg)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhSetFsipv6IfDestUnreachableMsg (u4FsMIStdIpv6InterfaceIfIndex,
                                                 i4SetValFsMIIpv6IfDestUnreachableMsg);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIIpv6IfUnnumAssocIPIf
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex

                The Object 
                setValFsMIIpv6IfUnnumAssocIPIf
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpv6IfUnnumAssocIPIf (UINT4 u4FsMIStdIpv6InterfaceIfIndex,
                                INT4 i4SetValFsMIIpv6IfUnnumAssocIPIf)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal =
        nmhSetFsipv6IfUnnumAssocIPIf ((INT4) u4FsMIStdIpv6InterfaceIfIndex,
                                      i4SetValFsMIIpv6IfUnnumAssocIPIf);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIIpv6IfRedirectMsg
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex

                The Object
                setValFsMIIpv6IfRedirectMsg
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpv6IfRedirectMsg (UINT4 u4FsMIStdIpv6InterfaceIfIndex,
                             INT4 i4SetValFsMIIpv6IfRedirectMsg)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhSetFsipv6IfRedirectMsg ((INT4) u4FsMIStdIpv6InterfaceIfIndex,
                                          i4SetValFsMIIpv6IfRedirectMsg);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIIpv6IfAdvSrcLLAdr
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex

                The Object
                setValFsMIIpv6IfAdvSrcLLAdr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpv6IfAdvSrcLLAdr (UINT4 u4FsMIStdIpv6InterfaceIfIndex,
                             INT4 i4SetValFsMIIpv6IfAdvSrcLLAdr)
{
    UNUSED_PARAM (u4FsMIStdIpv6InterfaceIfIndex);
    UNUSED_PARAM (i4SetValFsMIIpv6IfAdvSrcLLAdr);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMIIpv6IfAdvIntOpt
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex

                The Object
                setValFsMIIpv6IfAdvIntOpt
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpv6IfAdvIntOpt (UINT4 u4FsMIStdIpv6InterfaceIfIndex,
                           INT4 i4SetValFsMIIpv6IfAdvIntOpt)
{
    UNUSED_PARAM (u4FsMIStdIpv6InterfaceIfIndex);
    UNUSED_PARAM (i4SetValFsMIIpv6IfAdvIntOpt);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMIIpv6IfNDProxyAdminStatus
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex

                The Object
                setValFsMIIpv6IfNDProxyAdminStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpv6IfNDProxyAdminStatus (INT4 i4FsMIStdIpv6InterfaceIfIndex,
                                    INT4 i4SetValFsMIIpv6IfNDProxyAdminStatus)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal =
        nmhSetFsipv6IfNDProxyAdminStatus (i4FsMIStdIpv6InterfaceIfIndex,
                                          i4SetValFsMIIpv6IfNDProxyAdminStatus);
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhSetFsMIIpv6IfNDProxyMode
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex

                The Object
                setValFsMIIpv6IfNDProxyMode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpv6IfNDProxyMode (INT4 i4FsMIStdIpv6InterfaceIfIndex,
                             INT4 i4SetValFsMIIpv6IfNDProxyMode)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal =
        nmhSetFsipv6IfNDProxyMode (i4FsMIStdIpv6InterfaceIfIndex,
                                   i4SetValFsMIIpv6IfNDProxyMode);
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhSetFsMIIpv6IfNDProxyUpStream
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex

                The Object
                setValFsMIIpv6IfNDProxyUpStream
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpv6IfNDProxyUpStream (INT4 i4FsMIStdIpv6InterfaceIfIndex,
                                 INT4 i4SetValFsMIIpv6IfNDProxyUpStream)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal =
        nmhSetFsipv6IfNDProxyUpStream (i4FsMIStdIpv6InterfaceIfIndex,
                                       i4SetValFsMIIpv6IfNDProxyUpStream);
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhSetFsMIIpv6IfSENDSecStatus
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex

                The Object 
                setValFsMIIpv6IfSENDSecStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsMIIpv6IfSENDSecStatus(INT4 i4FsMIStdIpv6InterfaceIfIndex , INT4 i4SetValFsMIIpv6IfSENDSecStatus)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal =
        nmhSetFsipv6IfSENDSecStatus (i4FsMIStdIpv6InterfaceIfIndex,
                                       i4SetValFsMIIpv6IfSENDSecStatus);
    return (i1RetVal);

}
/****************************************************************************
 Function    :  nmhSetFsMIIpv6IfSENDDeltaTimestamp
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex

                The Object 
                setValFsMIIpv6IfSENDDeltaTimestamp
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsMIIpv6IfSENDDeltaTimestamp(INT4 i4FsMIStdIpv6InterfaceIfIndex , UINT4 u4SetValFsMIIpv6IfSENDDeltaTimestamp)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal =
        nmhSetFsipv6IfSENDDeltaTimestamp (i4FsMIStdIpv6InterfaceIfIndex,
                                       u4SetValFsMIIpv6IfSENDDeltaTimestamp);
    return (i1RetVal);

}
/****************************************************************************
 Function    :  nmhSetFsMIIpv6IfSENDFuzzTimestamp
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex

                The Object 
                setValFsMIIpv6IfSENDFuzzTimestamp
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsMIIpv6IfSENDFuzzTimestamp(INT4 i4FsMIStdIpv6InterfaceIfIndex , UINT4 u4SetValFsMIIpv6IfSENDFuzzTimestamp)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal =
        nmhSetFsipv6IfSENDFuzzTimestamp (i4FsMIStdIpv6InterfaceIfIndex,
                                       u4SetValFsMIIpv6IfSENDFuzzTimestamp);
    return (i1RetVal);
}
/****************************************************************************
 Function    :  nmhSetFsMIIpv6IfSENDDriftTimestamp
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex

                The Object 
                setValFsMIIpv6IfSENDDriftTimestamp
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsMIIpv6IfSENDDriftTimestamp(INT4 i4FsMIStdIpv6InterfaceIfIndex , UINT4 u4SetValFsMIIpv6IfSENDDriftTimestamp)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal =
        nmhSetFsipv6IfSENDDriftTimestamp (i4FsMIStdIpv6InterfaceIfIndex,
                                       u4SetValFsMIIpv6IfSENDDriftTimestamp);
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhSetFsMIIpv6IfDefRoutePreference
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex

                The Object 
                setValFsMIIpv6IfDefRoutePreference
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
nmhSetFsMIIpv6IfDefRoutePreference(INT4 i4FsMIStdIpv6InterfaceIfIndex, 
                                   INT4 i4SetValFsMIIpv6IfDefRoutePreference)
{
     INT1 i1RetVal = SNMP_FAILURE;
     i1RetVal = nmhSetFsipv6IfDefRoutePreference(i4FsMIStdIpv6InterfaceIfIndex,
                                          i4SetValFsMIIpv6IfDefRoutePreference);
     return (i1RetVal);
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6IfToken
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex

                The Object 
                testValFsMIIpv6IfToken
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpv6IfToken (UINT4 *pu4ErrorCode,
                          INT4 i4FsMIStdIpv6InterfaceIfIndex,
                          tSNMP_OCTET_STRING_TYPE * pTestValFsMIIpv6IfToken)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhTestv2Fsipv6IfToken (pu4ErrorCode,
                                       i4FsMIStdIpv6InterfaceIfIndex,
                                       pTestValFsMIIpv6IfToken);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6IfRouterAdvStatus
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex

                The Object 
                testValFsMIIpv6IfRouterAdvStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpv6IfRouterAdvStatus (UINT4 *pu4ErrorCode,
                                    INT4 i4FsMIStdIpv6InterfaceIfIndex,
                                    INT4 i4TestValFsMIIpv6IfRouterAdvStatus)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhTestv2Fsipv6IfRouterAdvStatus (pu4ErrorCode,
                                                 i4FsMIStdIpv6InterfaceIfIndex,
                                                 i4TestValFsMIIpv6IfRouterAdvStatus);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6IfRouterAdvFlags
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex

                The Object 
                testValFsMIIpv6IfRouterAdvFlags
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpv6IfRouterAdvFlags (UINT4 *pu4ErrorCode,
                                   INT4 i4FsMIStdIpv6InterfaceIfIndex,
                                   INT4 i4TestValFsMIIpv6IfRouterAdvFlags)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhTestv2Fsipv6IfRouterAdvFlags (pu4ErrorCode,
                                                i4FsMIStdIpv6InterfaceIfIndex,
                                                i4TestValFsMIIpv6IfRouterAdvFlags);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6IfHopLimit
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex

                The Object 
                testValFsMIIpv6IfHopLimit
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpv6IfHopLimit (UINT4 *pu4ErrorCode,
                             INT4 i4FsMIStdIpv6InterfaceIfIndex,
                             INT4 i4TestValFsMIIpv6IfHopLimit)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhTestv2Fsipv6IfHopLimit (pu4ErrorCode,
                                          i4FsMIStdIpv6InterfaceIfIndex,
                                          i4TestValFsMIIpv6IfHopLimit);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6IfDefRouterTime
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex

                The Object 
                testValFsMIIpv6IfDefRouterTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpv6IfDefRouterTime (UINT4 *pu4ErrorCode,
                                  INT4 i4FsMIStdIpv6InterfaceIfIndex,
                                  INT4 i4TestValFsMIIpv6IfDefRouterTime)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhTestv2Fsipv6IfDefRouterTime (pu4ErrorCode,
                                               i4FsMIStdIpv6InterfaceIfIndex,
                                               i4TestValFsMIIpv6IfDefRouterTime);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6IfReachableTime
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex

                The Object 
                testValFsMIIpv6IfReachableTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpv6IfReachableTime (UINT4 *pu4ErrorCode,
                                  INT4 i4FsMIStdIpv6InterfaceIfIndex,
                                  INT4 i4TestValFsMIIpv6IfReachableTime)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhTestv2Fsipv6IfReachableTime (pu4ErrorCode,
                                               i4FsMIStdIpv6InterfaceIfIndex,
                                               i4TestValFsMIIpv6IfReachableTime);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6IfRetransmitTime
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex

                The Object 
                testValFsMIIpv6IfRetransmitTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpv6IfRetransmitTime (UINT4 *pu4ErrorCode,
                                   INT4 i4FsMIStdIpv6InterfaceIfIndex,
                                   INT4 i4TestValFsMIIpv6IfRetransmitTime)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhTestv2Fsipv6IfRetransmitTime (pu4ErrorCode,
                                                i4FsMIStdIpv6InterfaceIfIndex,
                                                i4TestValFsMIIpv6IfRetransmitTime);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6IfDelayProbeTime
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex

                The Object 
                testValFsMIIpv6IfDelayProbeTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpv6IfDelayProbeTime (UINT4 *pu4ErrorCode,
                                   INT4 i4FsMIStdIpv6InterfaceIfIndex,
                                   INT4 i4TestValFsMIIpv6IfDelayProbeTime)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhTestv2Fsipv6IfDelayProbeTime (pu4ErrorCode,
                                                i4FsMIStdIpv6InterfaceIfIndex,
                                                i4TestValFsMIIpv6IfDelayProbeTime);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6IfPrefixAdvStatus
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex

                The Object 
                testValFsMIIpv6IfPrefixAdvStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpv6IfPrefixAdvStatus (UINT4 *pu4ErrorCode,
                                    INT4 i4FsMIStdIpv6InterfaceIfIndex,
                                    INT4 i4TestValFsMIIpv6IfPrefixAdvStatus)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhTestv2Fsipv6IfPrefixAdvStatus (pu4ErrorCode,
                                                 i4FsMIStdIpv6InterfaceIfIndex,
                                                 i4TestValFsMIIpv6IfPrefixAdvStatus);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6IfMinRouterAdvTime
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex

                The Object 
                testValFsMIIpv6IfMinRouterAdvTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpv6IfMinRouterAdvTime (UINT4 *pu4ErrorCode,
                                     INT4 i4FsMIStdIpv6InterfaceIfIndex,
                                     INT4 i4TestValFsMIIpv6IfMinRouterAdvTime)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhTestv2Fsipv6IfMinRouterAdvTime (pu4ErrorCode,
                                                  i4FsMIStdIpv6InterfaceIfIndex,
                                                  i4TestValFsMIIpv6IfMinRouterAdvTime);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6IfMaxRouterAdvTime
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex

                The Object 
                testValFsMIIpv6IfMaxRouterAdvTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpv6IfMaxRouterAdvTime (UINT4 *pu4ErrorCode,
                                     INT4 i4FsMIStdIpv6InterfaceIfIndex,
                                     INT4 i4TestValFsMIIpv6IfMaxRouterAdvTime)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhTestv2Fsipv6IfMaxRouterAdvTime (pu4ErrorCode,
                                                  i4FsMIStdIpv6InterfaceIfIndex,
                                                  i4TestValFsMIIpv6IfMaxRouterAdvTime);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6IfDADRetries
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex

                The Object 
                testValFsMIIpv6IfDADRetries
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpv6IfDADRetries (UINT4 *pu4ErrorCode,
                               INT4 i4FsMIStdIpv6InterfaceIfIndex,
                               INT4 i4TestValFsMIIpv6IfDADRetries)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhTestv2Fsipv6IfDADRetries (pu4ErrorCode,
                                            i4FsMIStdIpv6InterfaceIfIndex,
                                            i4TestValFsMIIpv6IfDADRetries);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6IfForwarding
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex

                The Object 
                testValFsMIIpv6IfForwarding
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpv6IfForwarding (UINT4 *pu4ErrorCode,
                               UINT4 u4FsMIStdIpv6InterfaceIfIndex,
                               INT4 i4TestValFsMIIpv6IfForwarding)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhTestv2Fsipv6IfForwarding (pu4ErrorCode,
                                            u4FsMIStdIpv6InterfaceIfIndex,
                                            i4TestValFsMIIpv6IfForwarding);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6IfIcmpErrInterval
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex

                The Object
                testValFsMIIpv6IfIcmpErrInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpv6IfIcmpErrInterval (UINT4 *pu4ErrorCode,
                                    UINT4 u4FsMIStdIpv6InterfaceIfIndex,
                                    INT4 i4TestValFsMIIpv6IfIcmpErrInterval)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal =
        nmhTestv2Fsipv6IfIcmpErrInterval (pu4ErrorCode,
                                          u4FsMIStdIpv6InterfaceIfIndex,
                                          i4TestValFsMIIpv6IfIcmpErrInterval);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6IfIcmpTokenBucketSize
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex

                The Object
                testValFsMIIpv6IfIcmpTokenBucketSize
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpv6IfIcmpTokenBucketSize (UINT4 *pu4ErrorCode,
                                        UINT4 u4FsMIStdIpv6InterfaceIfIndex,
                                        INT4
                                        i4TestValFsMIIpv6IfIcmpTokenBucketSize)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal =
        nmhTestv2Fsipv6IfIcmpTokenBucketSize (pu4ErrorCode,
                                              u4FsMIStdIpv6InterfaceIfIndex,
                                              i4TestValFsMIIpv6IfIcmpTokenBucketSize);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6IfDestUnreachableMsg
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex

                The Object
                testValFsMIIpv6IfDestUnreachableMsg
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpv6IfDestUnreachableMsg (UINT4 *pu4ErrorCode,
                                       UINT4 u4FsMIStdIpv6InterfaceIfIndex,
                                       INT4
                                       i4TestValFsMIIpv6IfDestUnreachableMsg)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal =
        nmhTestv2Fsipv6IfDestUnreachableMsg (pu4ErrorCode,
                                             u4FsMIStdIpv6InterfaceIfIndex,
                                             i4TestValFsMIIpv6IfDestUnreachableMsg);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6IfUnnumAssocIPIf
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex

                The Object 
                testValFsMIIpv6IfUnnumAssocIPIf
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpv6IfUnnumAssocIPIf (UINT4 *pu4ErrorCode,
                                   UINT4 u4FsMIStdIpv6InterfaceIfIndex,
                                   INT4 i4TestValFsMIIpv6IfUnnumAssocIPIf)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal =
        nmhTestv2Fsipv6IfUnnumAssocIPIf (pu4ErrorCode,
                                         (INT4) u4FsMIStdIpv6InterfaceIfIndex,
                                         i4TestValFsMIIpv6IfUnnumAssocIPIf);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6IfRedirectMsg
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex

                The Object
                testValFsMIIpv6IfRedirectMsg
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpv6IfRedirectMsg (UINT4 *pu4ErrorCode,
                                UINT4 u4FsMIStdIpv6InterfaceIfIndex,
                                INT4 i4TestValFsMIIpv6IfRedirectMsg)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhTestv2Fsipv6IfRedirectMsg (pu4ErrorCode,
                                             (INT4)
                                             u4FsMIStdIpv6InterfaceIfIndex,
                                             i4TestValFsMIIpv6IfRedirectMsg);

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6IfAdvSrcLLAdr
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex

                The Object
                testValFsMIIpv6IfAdvSrcLLAdr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpv6IfAdvSrcLLAdr (UINT4 *pu4ErrorCode,
                                UINT4 u4FsMIStdIpv6InterfaceIfIndex,
                                INT4 i4TestValFsMIIpv6IfAdvSrcLLAdr)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4FsMIStdIpv6InterfaceIfIndex);
    UNUSED_PARAM (i4TestValFsMIIpv6IfAdvSrcLLAdr);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6IfAdvIntOpt
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex

                The Object
                testValFsMIIpv6IfAdvIntOpt
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpv6IfAdvIntOpt (UINT4 *pu4ErrorCode,
                              UINT4 u4FsMIStdIpv6InterfaceIfIndex,
                              INT4 i4TestValFsMIIpv6IfAdvIntOpt)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4FsMIStdIpv6InterfaceIfIndex);
    UNUSED_PARAM (i4TestValFsMIIpv6IfAdvIntOpt);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6IfNDProxyAdminStatus
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex

                The Object
                testValFsMIIpv6IfNDProxyAdminStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpv6IfNDProxyAdminStatus (UINT4 *pu4ErrorCode,
                                       INT4 i4FsMIStdIpv6InterfaceIfIndex,
                                       INT4
                                       i4TestValFsMIIpv6IfNDProxyAdminStatus)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal =
        nmhTestv2Fsipv6IfNDProxyAdminStatus (pu4ErrorCode,
                                             i4FsMIStdIpv6InterfaceIfIndex,
                                             i4TestValFsMIIpv6IfNDProxyAdminStatus);
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6IfNDProxyMode
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex

                The Object
                testValFsMIIpv6IfNDProxyMode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpv6IfNDProxyMode (UINT4 *pu4ErrorCode,
                                INT4 i4FsMIStdIpv6InterfaceIfIndex,
                                INT4 i4TestValFsMIIpv6IfNDProxyMode)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal =
        nmhTestv2Fsipv6IfNDProxyMode (pu4ErrorCode,
                                      i4FsMIStdIpv6InterfaceIfIndex,
                                      i4TestValFsMIIpv6IfNDProxyMode);
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6IfNDProxyUpStream
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex

                The Object
                testValFsMIIpv6IfNDProxyUpStream
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpv6IfNDProxyUpStream (UINT4 *pu4ErrorCode,
                                    INT4 i4FsMIStdIpv6InterfaceIfIndex,
                                    INT4 i4TestValFsMIIpv6IfNDProxyUpStream)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal =
        nmhTestv2Fsipv6IfNDProxyUpStream (pu4ErrorCode,
                                          i4FsMIStdIpv6InterfaceIfIndex,
                                          i4TestValFsMIIpv6IfNDProxyUpStream);
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6IfSENDSecStatus
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex

                The Object 
                testValFsMIIpv6IfSENDSecStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsMIIpv6IfSENDSecStatus(UINT4 *pu4ErrorCode , INT4 i4FsMIStdIpv6InterfaceIfIndex , INT4 i4TestValFsMIIpv6IfSENDSecStatus)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal =
        nmhTestv2Fsipv6IfSENDSecStatus (pu4ErrorCode,
                                          i4FsMIStdIpv6InterfaceIfIndex,
                                          i4TestValFsMIIpv6IfSENDSecStatus);
    return (i1RetVal);
}
/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6IfSENDDeltaTimestamp
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex

                The Object 
                testValFsMIIpv6IfSENDDeltaTimestamp
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsMIIpv6IfSENDDeltaTimestamp(UINT4 *pu4ErrorCode , INT4 i4FsMIStdIpv6InterfaceIfIndex , UINT4 u4TestValFsMIIpv6IfSENDDeltaTimestamp)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal =
        nmhTestv2Fsipv6IfSENDDeltaTimestamp (pu4ErrorCode,
                                          i4FsMIStdIpv6InterfaceIfIndex,
                                          u4TestValFsMIIpv6IfSENDDeltaTimestamp);
    return (i1RetVal);

}
/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6IfSENDFuzzTimestamp
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex

                The Object 
                testValFsMIIpv6IfSENDFuzzTimestamp
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsMIIpv6IfSENDFuzzTimestamp(UINT4 *pu4ErrorCode , INT4 i4FsMIStdIpv6InterfaceIfIndex , UINT4 u4TestValFsMIIpv6IfSENDFuzzTimestamp)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal =
       nmhTestv2Fsipv6IfSENDFuzzTimestamp (pu4ErrorCode,
                                          i4FsMIStdIpv6InterfaceIfIndex,
                                          u4TestValFsMIIpv6IfSENDFuzzTimestamp);
    return (i1RetVal);

}
/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6IfSENDDriftTimestamp
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex

                The Object 
                testValFsMIIpv6IfSENDDriftTimestamp
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsMIIpv6IfSENDDriftTimestamp(UINT4 *pu4ErrorCode , INT4 i4FsMIStdIpv6InterfaceIfIndex , UINT4 u4TestValFsMIIpv6IfSENDDriftTimestamp)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal =
       nmhTestv2Fsipv6IfSENDDriftTimestamp (pu4ErrorCode,
                                          i4FsMIStdIpv6InterfaceIfIndex,
                                          u4TestValFsMIIpv6IfSENDDriftTimestamp);
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6IfDefRoutePreference
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex

                The Object 
                testValFsMIIpv6IfDefRoutePreference
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
nmhTestv2FsMIIpv6IfDefRoutePreference(UINT4 *pu4ErrorCode, 
                                      INT4 i4FsMIStdIpv6InterfaceIfIndex, 
                                      INT4 i4TestValFsMIIpv6IfDefRoutePreference)
{
    INT1 i1RetVal = SNMP_FAILURE;
    i1RetVal = nmhTestv2Fsipv6IfDefRoutePreference(pu4ErrorCode,
                                             i4FsMIStdIpv6InterfaceIfIndex,
                                             i4TestValFsMIIpv6IfDefRoutePreference);
    return (i1RetVal);
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIIpv6IfTable
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIIpv6IfTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIIpv6IfStatsTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIIpv6IfStatsTable
 Input       :  The Indices
                FsMIStdIpIfStatsIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIIpv6IfStatsTable (INT4 i4FsMIStdIpIfStatsIfIndex)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal =
        nmhValidateIndexInstanceFsipv6IfStatsTable (i4FsMIStdIpIfStatsIfIndex);
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIIpv6IfStatsTable
 Input       :  The Indices
                FsMIStdIpIfStatsIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIIpv6IfStatsTable (INT4 *pi4FsMIStdIpIfStatsIfIndex)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhGetFirstIndexFsipv6IfStatsTable (pi4FsMIStdIpIfStatsIfIndex);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIIpv6IfStatsTable
 Input       :  The Indices
                FsMIStdIpIfStatsIfIndex
                nextFsMIStdIpIfStatsIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIIpv6IfStatsTable (INT4 i4FsMIStdIpIfStatsIfIndex,
                                     INT4 *pi4NextFsMIStdIpIfStatsIfIndex)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhGetNextIndexFsipv6IfStatsTable (i4FsMIStdIpIfStatsIfIndex,
                                                  pi4NextFsMIStdIpIfStatsIfIndex);
    return i1RetVal;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIIpv6IfStatsTooBigErrors
 Input       :  The Indices
                FsMIStdIpIfStatsIfIndex

                The Object 
                retValFsMIIpv6IfStatsTooBigErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6IfStatsTooBigErrors (INT4 i4FsMIStdIpIfStatsIfIndex,
                                   UINT4 *pu4RetValFsMIIpv6IfStatsTooBigErrors)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhGetFsipv6IfStatsTooBigErrors (i4FsMIStdIpIfStatsIfIndex,
                                                pu4RetValFsMIIpv6IfStatsTooBigErrors);
    Ip6ReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6IfStatsInRouterSols
 Input       :  The Indices
                FsMIStdIpIfStatsIfIndex

                The Object 
                retValFsMIIpv6IfStatsInRouterSols
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6IfStatsInRouterSols (INT4 i4FsMIStdIpIfStatsIfIndex,
                                   UINT4 *pu4RetValFsMIIpv6IfStatsInRouterSols)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhGetFsipv6IfStatsInRouterSols (i4FsMIStdIpIfStatsIfIndex,
                                                pu4RetValFsMIIpv6IfStatsInRouterSols);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6IfStatsInRouterAdvs
 Input       :  The Indices
                FsMIStdIpIfStatsIfIndex

                The Object 
                retValFsMIIpv6IfStatsInRouterAdvs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6IfStatsInRouterAdvs (INT4 i4FsMIStdIpIfStatsIfIndex,
                                   UINT4 *pu4RetValFsMIIpv6IfStatsInRouterAdvs)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhGetFsipv6IfStatsInRouterAdvs (i4FsMIStdIpIfStatsIfIndex,
                                                pu4RetValFsMIIpv6IfStatsInRouterAdvs);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6IfStatsInNeighSols
 Input       :  The Indices
                FsMIStdIpIfStatsIfIndex

                The Object 
                retValFsMIIpv6IfStatsInNeighSols
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6IfStatsInNeighSols (INT4 i4FsMIStdIpIfStatsIfIndex,
                                  UINT4 *pu4RetValFsMIIpv6IfStatsInNeighSols)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhGetFsipv6IfStatsInNeighSols (i4FsMIStdIpIfStatsIfIndex,
                                               pu4RetValFsMIIpv6IfStatsInNeighSols);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6IfStatsInNeighAdvs
 Input       :  The Indices
                FsMIStdIpIfStatsIfIndex

                The Object 
                retValFsMIIpv6IfStatsInNeighAdvs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6IfStatsInNeighAdvs (INT4 i4FsMIStdIpIfStatsIfIndex,
                                  UINT4 *pu4RetValFsMIIpv6IfStatsInNeighAdvs)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhGetFsipv6IfStatsInNeighAdvs (i4FsMIStdIpIfStatsIfIndex,
                                               pu4RetValFsMIIpv6IfStatsInNeighAdvs);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6IfStatsInRedirects
 Input       :  The Indices
                FsMIStdIpIfStatsIfIndex

                The Object 
                retValFsMIIpv6IfStatsInRedirects
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6IfStatsInRedirects (INT4 i4FsMIStdIpIfStatsIfIndex,
                                  UINT4 *pu4RetValFsMIIpv6IfStatsInRedirects)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhGetFsipv6IfStatsInRedirects (i4FsMIStdIpIfStatsIfIndex,
                                               pu4RetValFsMIIpv6IfStatsInRedirects);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6IfStatsOutRouterSols
 Input       :  The Indices
                FsMIStdIpIfStatsIfIndex

                The Object 
                retValFsMIIpv6IfStatsOutRouterSols
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6IfStatsOutRouterSols (INT4 i4FsMIStdIpIfStatsIfIndex,
                                    UINT4
                                    *pu4RetValFsMIIpv6IfStatsOutRouterSols)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhGetFsipv6IfStatsOutRouterSols (i4FsMIStdIpIfStatsIfIndex,
                                                 pu4RetValFsMIIpv6IfStatsOutRouterSols);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6IfStatsOutRouterAdvs
 Input       :  The Indices
                FsMIStdIpIfStatsIfIndex

                The Object 
                retValFsMIIpv6IfStatsOutRouterAdvs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6IfStatsOutRouterAdvs (INT4 i4FsMIStdIpIfStatsIfIndex,
                                    UINT4
                                    *pu4RetValFsMIIpv6IfStatsOutRouterAdvs)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhGetFsipv6IfStatsOutRouterAdvs (i4FsMIStdIpIfStatsIfIndex,
                                                 pu4RetValFsMIIpv6IfStatsOutRouterAdvs);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6IfStatsOutNeighSols
 Input       :  The Indices
                FsMIStdIpIfStatsIfIndex

                The Object 
                retValFsMIIpv6IfStatsOutNeighSols
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6IfStatsOutNeighSols (INT4 i4FsMIStdIpIfStatsIfIndex,
                                   UINT4 *pu4RetValFsMIIpv6IfStatsOutNeighSols)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhGetFsipv6IfStatsOutNeighSols (i4FsMIStdIpIfStatsIfIndex,
                                                pu4RetValFsMIIpv6IfStatsOutNeighSols);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6IfStatsOutNeighAdvs
 Input       :  The Indices
                FsMIStdIpIfStatsIfIndex

                The Object 
                retValFsMIIpv6IfStatsOutNeighAdvs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6IfStatsOutNeighAdvs (INT4 i4FsMIStdIpIfStatsIfIndex,
                                   UINT4 *pu4RetValFsMIIpv6IfStatsOutNeighAdvs)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhGetFsipv6IfStatsOutNeighAdvs (i4FsMIStdIpIfStatsIfIndex,
                                                pu4RetValFsMIIpv6IfStatsOutNeighAdvs);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6IfStatsOutRedirects
 Input       :  The Indices
                FsMIStdIpIfStatsIfIndex

                The Object 
                retValFsMIIpv6IfStatsOutRedirects
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6IfStatsOutRedirects (INT4 i4FsMIStdIpIfStatsIfIndex,
                                   UINT4 *pu4RetValFsMIIpv6IfStatsOutRedirects)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhGetFsipv6IfStatsOutRedirects (i4FsMIStdIpIfStatsIfIndex,
                                                pu4RetValFsMIIpv6IfStatsOutRedirects);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6IfStatsLastRouterAdvTime
 Input       :  The Indices
                FsMIStdIpIfStatsIfIndex

                The Object 
                retValFsMIIpv6IfStatsLastRouterAdvTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6IfStatsLastRouterAdvTime (INT4 i4FsMIStdIpIfStatsIfIndex,
                                        UINT4
                                        *pu4RetValFsMIIpv6IfStatsLastRouterAdvTime)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhGetFsipv6IfStatsLastRouterAdvTime (i4FsMIStdIpIfStatsIfIndex,
                                                     pu4RetValFsMIIpv6IfStatsLastRouterAdvTime);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6IfStatsNextRouterAdvTime
 Input       :  The Indices
                FsMIStdIpIfStatsIfIndex

                The Object 
                retValFsMIIpv6IfStatsNextRouterAdvTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6IfStatsNextRouterAdvTime (INT4 i4FsMIStdIpIfStatsIfIndex,
                                        UINT4
                                        *pu4RetValFsMIIpv6IfStatsNextRouterAdvTime)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhGetFsipv6IfStatsNextRouterAdvTime (i4FsMIStdIpIfStatsIfIndex,
                                                     pu4RetValFsMIIpv6IfStatsNextRouterAdvTime);
    return i1RetVal;
}

/****************************************************************************
 *  Function    :  nmhGetFsMIIpv6IfStatsIcmp6ErrRateLmtd
 *  Input       :  The Indices
 *                 FsMIStdIpIfStatsIfIndex
 *
 *                 The Object
 *                 retValFsMIIpv6IfStatsIcmp6ErrRateLmtd
 *  Output      :  The Get Low Lev Routine Take the Indices &
 *                 store the Value requested in the Return val.
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
INT1
nmhGetFsMIIpv6IfStatsIcmp6ErrRateLmtd (INT4 i4FsMIStdIpIfStatsIfIndex,
                                       UINT4
                                       *pu4RetValFsMIIpv6IfStatsIcmp6ErrRateLmtd)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhGetFsipv6IfStatsIcmp6ErrRateLmtd (i4FsMIStdIpIfStatsIfIndex,
                                                    pu4RetValFsMIIpv6IfStatsIcmp6ErrRateLmtd);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6IfStatsSENDDroppedPkts
 Input       :  The Indices
                FsMIStdIpIfStatsIfIndex

                The Object 
                retValFsMIIpv6IfStatsSENDDroppedPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsMIIpv6IfStatsSENDDroppedPkts(INT4 i4FsMIStdIpIfStatsIfIndex , UINT4 *pu4RetValFsMIIpv6IfStatsSENDDroppedPkts)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhGetFsipv6IfStatsSENDDroppedPkts (i4FsMIStdIpIfStatsIfIndex,
                                                     pu4RetValFsMIIpv6IfStatsSENDDroppedPkts);
    return i1RetVal;
}
/****************************************************************************
 Function    :  nmhGetFsMIIpv6IfStatsSENDInvalidPkts
 Input       :  The Indices
                FsMIStdIpIfStatsIfIndex

                The Object 
                retValFsMIIpv6IfStatsSENDInvalidPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsMIIpv6IfStatsSENDInvalidPkts(INT4 i4FsMIStdIpIfStatsIfIndex , UINT4 *pu4RetValFsMIIpv6IfStatsSENDInvalidPkts)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhGetFsipv6IfStatsSENDInvalidPkts (i4FsMIStdIpIfStatsIfIndex,
                                                     pu4RetValFsMIIpv6IfStatsSENDInvalidPkts);
    return i1RetVal;
}
/* LOW LEVEL Routines for Table : FsMIIpv6AddrTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIIpv6AddrTable
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex
                FsMIIpv6AddrAddress
                FsMIIpv6AddrPrefixLen
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIIpv6AddrTable (INT4 i4FsMIStdIpv6InterfaceIfIndex,
                                           tSNMP_OCTET_STRING_TYPE *
                                           pFsMIIpv6AddrAddress,
                                           INT4 i4FsMIIpv6AddrPrefixLen)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal =
        nmhValidateIndexInstanceFsipv6AddrTable (i4FsMIStdIpv6InterfaceIfIndex,
                                                 pFsMIIpv6AddrAddress,
                                                 i4FsMIIpv6AddrPrefixLen);
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIIpv6AddrTable
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex
                FsMIIpv6AddrAddress
                FsMIIpv6AddrPrefixLen
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIIpv6AddrTable (INT4 *pi4FsMIStdIpv6InterfaceIfIndex,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsMIIpv6AddrAddress,
                                   INT4 *pi4FsMIIpv6AddrPrefixLen)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhGetFirstIndexFsipv6AddrTable (pi4FsMIStdIpv6InterfaceIfIndex,
                                                pFsMIIpv6AddrAddress,
                                                pi4FsMIIpv6AddrPrefixLen);
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIIpv6AddrTable
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex
                nextFsMIStdIpv6InterfaceIfIndex
                FsMIIpv6AddrAddress
                nextFsMIIpv6AddrAddress
                FsMIIpv6AddrPrefixLen
                nextFsMIIpv6AddrPrefixLen
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIIpv6AddrTable (INT4 i4FsMIStdIpv6InterfaceIfIndex,
                                  INT4 *pi4NextFsMIStdIpv6InterfaceIfIndex,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pFsMIIpv6AddrAddress,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pNextFsMIIpv6AddrAddress,
                                  INT4 i4FsMIIpv6AddrPrefixLen,
                                  INT4 *pi4NextFsMIIpv6AddrPrefixLen)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhGetNextIndexFsipv6AddrTable (i4FsMIStdIpv6InterfaceIfIndex,
                                               pi4NextFsMIStdIpv6InterfaceIfIndex,
                                               pFsMIIpv6AddrAddress,
                                               pNextFsMIIpv6AddrAddress,
                                               i4FsMIIpv6AddrPrefixLen,
                                               pi4NextFsMIIpv6AddrPrefixLen);
    return i1RetVal;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIIpv6AddrAdminStatus
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex
                FsMIIpv6AddrAddress
                FsMIIpv6AddrPrefixLen

                The Object 
                retValFsMIIpv6AddrAdminStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6AddrAdminStatus (INT4 i4FsMIStdIpv6InterfaceIfIndex,
                               tSNMP_OCTET_STRING_TYPE * pFsMIIpv6AddrAddress,
                               INT4 i4FsMIIpv6AddrPrefixLen,
                               INT4 *pi4RetValFsMIIpv6AddrAdminStatus)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhGetFsipv6AddrAdminStatus (i4FsMIStdIpv6InterfaceIfIndex,
                                            pFsMIIpv6AddrAddress,
                                            i4FsMIIpv6AddrPrefixLen,
                                            pi4RetValFsMIIpv6AddrAdminStatus);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6AddrType
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex
                FsMIIpv6AddrAddress
                FsMIIpv6AddrPrefixLen

                The Object 
                retValFsMIIpv6AddrType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6AddrType (INT4 i4FsMIStdIpv6InterfaceIfIndex,
                        tSNMP_OCTET_STRING_TYPE * pFsMIIpv6AddrAddress,
                        INT4 i4FsMIIpv6AddrPrefixLen,
                        INT4 *pi4RetValFsMIIpv6AddrType)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhGetFsipv6AddrType (i4FsMIStdIpv6InterfaceIfIndex,
                                     pFsMIIpv6AddrAddress,
                                     i4FsMIIpv6AddrPrefixLen,
                                     pi4RetValFsMIIpv6AddrType);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6AddrProfIndex
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex
                FsMIIpv6AddrAddress
                FsMIIpv6AddrPrefixLen

                The Object 
                retValFsMIIpv6AddrProfIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6AddrProfIndex (INT4 i4FsMIStdIpv6InterfaceIfIndex,
                             tSNMP_OCTET_STRING_TYPE * pFsMIIpv6AddrAddress,
                             INT4 i4FsMIIpv6AddrPrefixLen,
                             INT4 *pi4RetValFsMIIpv6AddrProfIndex)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhGetFsipv6AddrProfIndex (i4FsMIStdIpv6InterfaceIfIndex,
                                          pFsMIIpv6AddrAddress,
                                          i4FsMIIpv6AddrPrefixLen,
                                          pi4RetValFsMIIpv6AddrProfIndex);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6AddrOperStatus
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex
                FsMIIpv6AddrAddress
                FsMIIpv6AddrPrefixLen

                The Object 
                retValFsMIIpv6AddrOperStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6AddrOperStatus (INT4 i4FsMIStdIpv6InterfaceIfIndex,
                              tSNMP_OCTET_STRING_TYPE * pFsMIIpv6AddrAddress,
                              INT4 i4FsMIIpv6AddrPrefixLen,
                              INT4 *pi4RetValFsMIIpv6AddrOperStatus)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhGetFsipv6AddrOperStatus (i4FsMIStdIpv6InterfaceIfIndex,
                                           pFsMIIpv6AddrAddress,
                                           i4FsMIIpv6AddrPrefixLen,
                                           pi4RetValFsMIIpv6AddrOperStatus);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6AddrContextId
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex
                FsMIIpv6AddrAddress
                FsMIIpv6AddrPrefixLen

                The Object 
                retValFsMIIpv6AddrContextId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6AddrContextId (INT4 i4FsMIStdIpv6InterfaceIfIndex,
                             tSNMP_OCTET_STRING_TYPE * pFsMIIpv6AddrAddress,
                             INT4 i4FsMIIpv6AddrPrefixLen,
                             INT4 *pi4RetValFsMIIpv6AddrContextId)
{
    UNUSED_PARAM (pFsMIIpv6AddrAddress);
    UNUSED_PARAM (i4FsMIIpv6AddrPrefixLen);

    if (Ip6GetCxtId ((UINT4) i4FsMIStdIpv6InterfaceIfIndex,
                     (UINT4 *) pi4RetValFsMIIpv6AddrContextId) == IP6_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6AddrScope
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex
                FsMIIpv6AddrAddress
                FsMIIpv6AddrPrefixLen

                The Object 
                retValFsMIIpv6AddrScope
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6AddrScope (UINT4 u4FsMIStdIpv6InterfaceIfIndex,
                         tSNMP_OCTET_STRING_TYPE * pFsMIIpv6AddrAddress,
                         INT4 i4FsMIIpv6AddrPrefixLen,
                         INT4 *pi4RetValFsMIIpv6AddrScope)
{

    INT1                i1RetVal = SNMP_FAILURE;
    i1RetVal = nmhGetFsipv6AddrScope (u4FsMIStdIpv6InterfaceIfIndex,
                                      pFsMIIpv6AddrAddress,
                                      i4FsMIIpv6AddrPrefixLen,
                                      pi4RetValFsMIIpv6AddrScope);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6AddrSENDCgaStatus
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex
                FsMIIpv6AddrAddress
                FsMIIpv6AddrPrefixLen

                The Object 
                retValFsMIIpv6AddrSENDCgaStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsMIIpv6AddrSENDCgaStatus(INT4 i4FsMIStdIpv6InterfaceIfIndex , 
                                     tSNMP_OCTET_STRING_TYPE *pFsMIIpv6AddrAddress , 
                                     INT4 i4FsMIIpv6AddrPrefixLen , 
                                     INT4 *pi4RetValFsMIIpv6AddrSENDCgaStatus)
{
    INT1                i1RetVal = SNMP_FAILURE;
    i1RetVal = nmhGetFsipv6AddrSENDCgaStatus (i4FsMIStdIpv6InterfaceIfIndex,
                                      pFsMIIpv6AddrAddress,
                                      i4FsMIIpv6AddrPrefixLen,
                                      pi4RetValFsMIIpv6AddrSENDCgaStatus);

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6AddrSENDCgaModifier
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex
                FsMIIpv6AddrAddress
                FsMIIpv6AddrPrefixLen

                The Object 
                retValFsMIIpv6AddrSENDCgaModifier
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsMIIpv6AddrSENDCgaModifier(INT4 i4FsMIStdIpv6InterfaceIfIndex , 
                                       tSNMP_OCTET_STRING_TYPE *pFsMIIpv6AddrAddress , 
                                       INT4 i4FsMIIpv6AddrPrefixLen , 
                                       tSNMP_OCTET_STRING_TYPE * pRetValFsMIIpv6AddrSENDCgaModifier)
{
    INT1                i1RetVal = SNMP_FAILURE;
    i1RetVal = nmhGetFsipv6AddrSENDCgaModifier (i4FsMIStdIpv6InterfaceIfIndex,
                                      pFsMIIpv6AddrAddress,
                                      i4FsMIIpv6AddrPrefixLen,
                                      pRetValFsMIIpv6AddrSENDCgaModifier);

    return i1RetVal;
}
/****************************************************************************
 Function    :  nmhGetFsMIIpv6AddrSENDCollisionCount
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex
                FsMIIpv6AddrAddress
                FsMIIpv6AddrPrefixLen

                The Object 
                retValFsMIIpv6AddrSENDCollisionCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsMIIpv6AddrSENDCollisionCount(INT4 i4FsMIStdIpv6InterfaceIfIndex , 
                                          tSNMP_OCTET_STRING_TYPE *pFsMIIpv6AddrAddress , 
                                          INT4 i4FsMIIpv6AddrPrefixLen , 
                                          INT4 *pi4RetValFsMIIpv6AddrSENDCollisionCount)
{
    INT1                i1RetVal = SNMP_FAILURE;
    i1RetVal = nmhGetFsipv6AddrSENDCollisionCount (i4FsMIStdIpv6InterfaceIfIndex,
                                      pFsMIIpv6AddrAddress,
                                      i4FsMIIpv6AddrPrefixLen,
                                      pi4RetValFsMIIpv6AddrSENDCollisionCount);

    return i1RetVal;

}
/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIIpv6AddrAdminStatus
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex
                FsMIIpv6AddrAddress
                FsMIIpv6AddrPrefixLen

                The Object 
                setValFsMIIpv6AddrAdminStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpv6AddrAdminStatus (INT4 i4FsMIStdIpv6InterfaceIfIndex,
                               tSNMP_OCTET_STRING_TYPE * pFsMIIpv6AddrAddress,
                               INT4 i4FsMIIpv6AddrPrefixLen,
                               INT4 i4SetValFsMIIpv6AddrAdminStatus)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhSetFsipv6AddrAdminStatus (i4FsMIStdIpv6InterfaceIfIndex,
                                            pFsMIIpv6AddrAddress,
                                            i4FsMIIpv6AddrPrefixLen,
                                            i4SetValFsMIIpv6AddrAdminStatus);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIIpv6AddrType
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex
                FsMIIpv6AddrAddress
                FsMIIpv6AddrPrefixLen

                The Object 
                setValFsMIIpv6AddrType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpv6AddrType (INT4 i4FsMIStdIpv6InterfaceIfIndex,
                        tSNMP_OCTET_STRING_TYPE * pFsMIIpv6AddrAddress,
                        INT4 i4FsMIIpv6AddrPrefixLen,
                        INT4 i4SetValFsMIIpv6AddrType)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhSetFsipv6AddrType (i4FsMIStdIpv6InterfaceIfIndex,
                                     pFsMIIpv6AddrAddress,
                                     i4FsMIIpv6AddrPrefixLen,
                                     i4SetValFsMIIpv6AddrType);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIIpv6AddrProfIndex
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex
                FsMIIpv6AddrAddress
                FsMIIpv6AddrPrefixLen

                The Object 
                setValFsMIIpv6AddrProfIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpv6AddrProfIndex (INT4 i4FsMIStdIpv6InterfaceIfIndex,
                             tSNMP_OCTET_STRING_TYPE * pFsMIIpv6AddrAddress,
                             INT4 i4FsMIIpv6AddrPrefixLen,
                             INT4 i4SetValFsMIIpv6AddrProfIndex)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhSetFsipv6AddrProfIndex (i4FsMIStdIpv6InterfaceIfIndex,
                                          pFsMIIpv6AddrAddress,
                                          i4FsMIIpv6AddrPrefixLen,
                                          i4SetValFsMIIpv6AddrProfIndex);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIIpv6AddrSENDCgaStatus
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex
                FsMIIpv6AddrAddress
                FsMIIpv6AddrPrefixLen

                The Object 
                setValFsMIIpv6AddrSENDCgaStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsMIIpv6AddrSENDCgaStatus(INT4 i4FsMIStdIpv6InterfaceIfIndex , 
                                     tSNMP_OCTET_STRING_TYPE *pFsMIIpv6AddrAddress , 
                                     INT4 i4FsMIIpv6AddrPrefixLen , 
                                     INT4 i4SetValFsMIIpv6AddrSENDCgaStatus)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhSetFsipv6AddrSENDCgaStatus (i4FsMIStdIpv6InterfaceIfIndex,
                                          pFsMIIpv6AddrAddress,
                                          i4FsMIIpv6AddrPrefixLen,
                                          i4SetValFsMIIpv6AddrSENDCgaStatus);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIIpv6AddrSENDCgaModifier
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex
                FsMIIpv6AddrAddress
                FsMIIpv6AddrPrefixLen

                The Object 
                setValFsMIIpv6AddrSENDCgaModifier
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsMIIpv6AddrSENDCgaModifier(INT4 i4FsMIStdIpv6InterfaceIfIndex , 
                                       tSNMP_OCTET_STRING_TYPE *pFsMIIpv6AddrAddress , 
                                       INT4 i4FsMIIpv6AddrPrefixLen , 
                                       tSNMP_OCTET_STRING_TYPE *pSetValFsMIIpv6AddrSENDCgaModifier)
{
   INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhSetFsipv6AddrSENDCgaModifier (i4FsMIStdIpv6InterfaceIfIndex,
                                          pFsMIIpv6AddrAddress,
                                          i4FsMIIpv6AddrPrefixLen,
                                          pSetValFsMIIpv6AddrSENDCgaModifier);
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIIpv6AddrSENDCollisionCount
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex
                FsMIIpv6AddrAddress
                FsMIIpv6AddrPrefixLen

                The Object 
                setValFsMIIpv6AddrSENDCollisionCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsMIIpv6AddrSENDCollisionCount(INT4 i4FsMIStdIpv6InterfaceIfIndex , 
                                          tSNMP_OCTET_STRING_TYPE *pFsMIIpv6AddrAddress , 
                                          INT4 i4FsMIIpv6AddrPrefixLen , 
                                          INT4 i4SetValFsMIIpv6AddrSENDCollisionCount)
{
   INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhSetFsipv6AddrSENDCollisionCount (i4FsMIStdIpv6InterfaceIfIndex,
                                          pFsMIIpv6AddrAddress,
                                          i4FsMIIpv6AddrPrefixLen,
                                          i4SetValFsMIIpv6AddrSENDCollisionCount);
    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6AddrAdminStatus
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex
                FsMIIpv6AddrAddress
                FsMIIpv6AddrPrefixLen

                The Object 
                testValFsMIIpv6AddrAdminStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpv6AddrAdminStatus (UINT4 *pu4ErrorCode,
                                  INT4 i4FsMIStdIpv6InterfaceIfIndex,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pFsMIIpv6AddrAddress,
                                  INT4 i4FsMIIpv6AddrPrefixLen,
                                  INT4 i4TestValFsMIIpv6AddrAdminStatus)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhTestv2Fsipv6AddrAdminStatus (pu4ErrorCode,
                                               i4FsMIStdIpv6InterfaceIfIndex,
                                               pFsMIIpv6AddrAddress,
                                               i4FsMIIpv6AddrPrefixLen,
                                               i4TestValFsMIIpv6AddrAdminStatus);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6AddrType
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex
                FsMIIpv6AddrAddress
                FsMIIpv6AddrPrefixLen

                The Object 
                testValFsMIIpv6AddrType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpv6AddrType (UINT4 *pu4ErrorCode,
                           INT4 i4FsMIStdIpv6InterfaceIfIndex,
                           tSNMP_OCTET_STRING_TYPE * pFsMIIpv6AddrAddress,
                           INT4 i4FsMIIpv6AddrPrefixLen,
                           INT4 i4TestValFsMIIpv6AddrType)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhTestv2Fsipv6AddrType (pu4ErrorCode,
                                        i4FsMIStdIpv6InterfaceIfIndex,
                                        pFsMIIpv6AddrAddress,
                                        i4FsMIIpv6AddrPrefixLen,
                                        i4TestValFsMIIpv6AddrType);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6AddrProfIndex
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex
                FsMIIpv6AddrAddress
                FsMIIpv6AddrPrefixLen

                The Object 
                testValFsMIIpv6AddrProfIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpv6AddrProfIndex (UINT4 *pu4ErrorCode,
                                INT4 i4FsMIStdIpv6InterfaceIfIndex,
                                tSNMP_OCTET_STRING_TYPE * pFsMIIpv6AddrAddress,
                                INT4 i4FsMIIpv6AddrPrefixLen,
                                INT4 i4TestValFsMIIpv6AddrProfIndex)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhTestv2Fsipv6AddrProfIndex (pu4ErrorCode,
                                             i4FsMIStdIpv6InterfaceIfIndex,
                                             pFsMIIpv6AddrAddress,
                                             i4FsMIIpv6AddrPrefixLen,
                                             i4TestValFsMIIpv6AddrProfIndex);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6AddrSENDCgaStatus
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex
                FsMIIpv6AddrAddress
                FsMIIpv6AddrPrefixLen

                The Object 
                testValFsMIIpv6AddrSENDCgaStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsMIIpv6AddrSENDCgaStatus(UINT4 *pu4ErrorCode , 
                                        INT4 i4FsMIStdIpv6InterfaceIfIndex , 
                                        tSNMP_OCTET_STRING_TYPE *pFsMIIpv6AddrAddress , 
                                        INT4 i4FsMIIpv6AddrPrefixLen , 
                                        INT4 i4TestValFsMIIpv6AddrSENDCgaStatus)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhTestv2Fsipv6AddrSENDCgaStatus (pu4ErrorCode,
                                             i4FsMIStdIpv6InterfaceIfIndex,
                                             pFsMIIpv6AddrAddress,
                                             i4FsMIIpv6AddrPrefixLen,
                                             i4TestValFsMIIpv6AddrSENDCgaStatus);
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6AddrSENDCgaModifier
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex
                FsMIIpv6AddrAddress
                FsMIIpv6AddrPrefixLen

                The Object 
                testValFsMIIpv6AddrSENDCgaModifier
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsMIIpv6AddrSENDCgaModifier(UINT4 *pu4ErrorCode , 
                                          INT4 i4FsMIStdIpv6InterfaceIfIndex , 
                                          tSNMP_OCTET_STRING_TYPE *pFsMIIpv6AddrAddress , 
                                          INT4 i4FsMIIpv6AddrPrefixLen , 
                                          tSNMP_OCTET_STRING_TYPE *pTestValFsMIIpv6AddrSENDCgaModifier)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhTestv2Fsipv6AddrSENDCgaModifier (pu4ErrorCode,
                                             i4FsMIStdIpv6InterfaceIfIndex,
                                             pFsMIIpv6AddrAddress,
                                             i4FsMIIpv6AddrPrefixLen,
                                             pTestValFsMIIpv6AddrSENDCgaModifier);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6AddrSENDCollisionCount
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex
                FsMIIpv6AddrAddress
                FsMIIpv6AddrPrefixLen

                The Object 
                testValFsMIIpv6AddrSENDCollisionCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsMIIpv6AddrSENDCollisionCount(UINT4 *pu4ErrorCode , 
                                             INT4 i4FsMIStdIpv6InterfaceIfIndex , 
                                             tSNMP_OCTET_STRING_TYPE *pFsMIIpv6AddrAddress , 
                                             INT4 i4FsMIIpv6AddrPrefixLen , 
                                             INT4 i4TestValFsMIIpv6AddrSENDCollisionCount)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhTestv2Fsipv6AddrSENDCollisionCount (pu4ErrorCode,
                                             i4FsMIStdIpv6InterfaceIfIndex,
                                             pFsMIIpv6AddrAddress,
                                             i4FsMIIpv6AddrPrefixLen,
                                             i4TestValFsMIIpv6AddrSENDCollisionCount);
    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIIpv6AddrTable
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex
                FsMIIpv6AddrAddress
                FsMIIpv6AddrPrefixLen
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIIpv6AddrTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIIpv6AddrProfileTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIIpv6AddrProfileTable
 Input       :  The Indices
                FsMIIpv6AddrProfileIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIIpv6AddrProfileTable (UINT4
                                                  u4FsMIIpv6AddrProfileIndex)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhValidateIndexInstanceFsipv6AddrProfileTable
        (u4FsMIIpv6AddrProfileIndex);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIIpv6AddrProfileTable
 Input       :  The Indices
                FsMIIpv6AddrProfileIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIIpv6AddrProfileTable (UINT4 *pu4FsMIIpv6AddrProfileIndex)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhGetFirstIndexFsipv6AddrProfileTable
        (pu4FsMIIpv6AddrProfileIndex);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIIpv6AddrProfileTable
 Input       :  The Indices
                FsMIIpv6AddrProfileIndex
                nextFsMIIpv6AddrProfileIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIIpv6AddrProfileTable (UINT4 u4FsMIIpv6AddrProfileIndex,
                                         UINT4 *pu4NextFsMIIpv6AddrProfileIndex)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhGetNextIndexFsipv6AddrProfileTable
        (u4FsMIIpv6AddrProfileIndex, pu4NextFsMIIpv6AddrProfileIndex);
    return i1RetVal;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIIpv6AddrProfileStatus
 Input       :  The Indices
                FsMIIpv6AddrProfileIndex

                The Object 
                retValFsMIIpv6AddrProfileStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6AddrProfileStatus (UINT4 u4FsMIIpv6AddrProfileIndex,
                                 INT4 *pi4RetValFsMIIpv6AddrProfileStatus)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhGetFsipv6AddrProfileStatus (u4FsMIIpv6AddrProfileIndex,
                                              pi4RetValFsMIIpv6AddrProfileStatus);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6AddrProfilePrefixAdvStatus
 Input       :  The Indices
                FsMIIpv6AddrProfileIndex

                The Object 
                retValFsMIIpv6AddrProfilePrefixAdvStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6AddrProfilePrefixAdvStatus (UINT4 u4FsMIIpv6AddrProfileIndex,
                                          INT4
                                          *pi4RetValFsMIIpv6AddrProfilePrefixAdvStatus)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal =
        nmhGetFsipv6AddrProfilePrefixAdvStatus (u4FsMIIpv6AddrProfileIndex,
                                                pi4RetValFsMIIpv6AddrProfilePrefixAdvStatus);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6AddrProfileOnLinkAdvStatus
 Input       :  The Indices
                FsMIIpv6AddrProfileIndex

                The Object 
                retValFsMIIpv6AddrProfileOnLinkAdvStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6AddrProfileOnLinkAdvStatus (UINT4 u4FsMIIpv6AddrProfileIndex,
                                          INT4
                                          *pi4RetValFsMIIpv6AddrProfileOnLinkAdvStatus)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal =
        nmhGetFsipv6AddrProfileOnLinkAdvStatus (u4FsMIIpv6AddrProfileIndex,
                                                pi4RetValFsMIIpv6AddrProfileOnLinkAdvStatus);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6AddrProfileAutoConfAdvStatus
 Input       :  The Indices
                FsMIIpv6AddrProfileIndex

                The Object 
                retValFsMIIpv6AddrProfileAutoConfAdvStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6AddrProfileAutoConfAdvStatus (UINT4 u4FsMIIpv6AddrProfileIndex,
                                            INT4
                                            *pi4RetValFsMIIpv6AddrProfileAutoConfAdvStatus)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal =
        nmhGetFsipv6AddrProfileAutoConfAdvStatus (u4FsMIIpv6AddrProfileIndex,
                                                  pi4RetValFsMIIpv6AddrProfileAutoConfAdvStatus);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6AddrProfilePreferredTime
 Input       :  The Indices
                FsMIIpv6AddrProfileIndex

                The Object 
                retValFsMIIpv6AddrProfilePreferredTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6AddrProfilePreferredTime (UINT4 u4FsMIIpv6AddrProfileIndex,
                                        UINT4
                                        *pu4RetValFsMIIpv6AddrProfilePreferredTime)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhGetFsipv6AddrProfilePreferredTime (u4FsMIIpv6AddrProfileIndex,
                                                     pu4RetValFsMIIpv6AddrProfilePreferredTime);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6AddrProfileValidTime
 Input       :  The Indices
                FsMIIpv6AddrProfileIndex

                The Object 
                retValFsMIIpv6AddrProfileValidTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6AddrProfileValidTime (UINT4 u4FsMIIpv6AddrProfileIndex,
                                    UINT4
                                    *pu4RetValFsMIIpv6AddrProfileValidTime)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhGetFsipv6AddrProfileValidTime (u4FsMIIpv6AddrProfileIndex,
                                                 pu4RetValFsMIIpv6AddrProfileValidTime);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6AddrProfileValidLifeTimeFlag
 Input       :  The Indices
                FsMIIpv6AddrProfileIndex

                The Object 
                retValFsMIIpv6AddrProfileValidLifeTimeFlag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6AddrProfileValidLifeTimeFlag (UINT4 u4FsMIIpv6AddrProfileIndex,
                                            INT4
                                            *pi4RetValFsMIIpv6AddrProfileValidLifeTimeFlag)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal =
        nmhGetFsipv6AddrProfileValidLifeTimeFlag (u4FsMIIpv6AddrProfileIndex,
                                                  pi4RetValFsMIIpv6AddrProfileValidLifeTimeFlag);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6AddrProfilePreferredLifeTimeFlag
 Input       :  The Indices
                FsMIIpv6AddrProfileIndex

                The Object 
                retValFsMIIpv6AddrProfilePreferredLifeTimeFlag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6AddrProfilePreferredLifeTimeFlag (UINT4
                                                u4FsMIIpv6AddrProfileIndex,
                                                INT4
                                                *pi4RetValFsMIIpv6AddrProfilePreferredLifeTimeFlag)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal =
        nmhGetFsipv6AddrProfilePreferredLifeTimeFlag
        (u4FsMIIpv6AddrProfileIndex,
         pi4RetValFsMIIpv6AddrProfilePreferredLifeTimeFlag);
    return i1RetVal;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIIpv6AddrProfileStatus
 Input       :  The Indices
                FsMIIpv6AddrProfileIndex

                The Object 
                setValFsMIIpv6AddrProfileStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpv6AddrProfileStatus (UINT4 u4FsMIIpv6AddrProfileIndex,
                                 INT4 i4SetValFsMIIpv6AddrProfileStatus)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhSetFsipv6AddrProfileStatus (u4FsMIIpv6AddrProfileIndex,
                                              i4SetValFsMIIpv6AddrProfileStatus);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIIpv6AddrProfilePrefixAdvStatus
 Input       :  The Indices
                FsMIIpv6AddrProfileIndex

                The Object 
                setValFsMIIpv6AddrProfilePrefixAdvStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpv6AddrProfilePrefixAdvStatus (UINT4 u4FsMIIpv6AddrProfileIndex,
                                          INT4
                                          i4SetValFsMIIpv6AddrProfilePrefixAdvStatus)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal =
        nmhSetFsipv6AddrProfilePrefixAdvStatus (u4FsMIIpv6AddrProfileIndex,
                                                i4SetValFsMIIpv6AddrProfilePrefixAdvStatus);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIIpv6AddrProfileOnLinkAdvStatus
 Input       :  The Indices
                FsMIIpv6AddrProfileIndex

                The Object 
                setValFsMIIpv6AddrProfileOnLinkAdvStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpv6AddrProfileOnLinkAdvStatus (UINT4 u4FsMIIpv6AddrProfileIndex,
                                          INT4
                                          i4SetValFsMIIpv6AddrProfileOnLinkAdvStatus)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal =
        nmhSetFsipv6AddrProfileOnLinkAdvStatus (u4FsMIIpv6AddrProfileIndex,
                                                i4SetValFsMIIpv6AddrProfileOnLinkAdvStatus);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIIpv6AddrProfileAutoConfAdvStatus
 Input       :  The Indices
                FsMIIpv6AddrProfileIndex

                The Object 
                setValFsMIIpv6AddrProfileAutoConfAdvStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpv6AddrProfileAutoConfAdvStatus (UINT4 u4FsMIIpv6AddrProfileIndex,
                                            INT4
                                            i4SetValFsMIIpv6AddrProfileAutoConfAdvStatus)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal =
        nmhSetFsipv6AddrProfileAutoConfAdvStatus (u4FsMIIpv6AddrProfileIndex,
                                                  i4SetValFsMIIpv6AddrProfileAutoConfAdvStatus);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIIpv6AddrProfilePreferredTime
 Input       :  The Indices
                FsMIIpv6AddrProfileIndex

                The Object 
                setValFsMIIpv6AddrProfilePreferredTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpv6AddrProfilePreferredTime (UINT4 u4FsMIIpv6AddrProfileIndex,
                                        UINT4
                                        u4SetValFsMIIpv6AddrProfilePreferredTime)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhSetFsipv6AddrProfilePreferredTime (u4FsMIIpv6AddrProfileIndex,
                                                     u4SetValFsMIIpv6AddrProfilePreferredTime);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIIpv6AddrProfileValidTime
 Input       :  The Indices
                FsMIIpv6AddrProfileIndex

                The Object 
                setValFsMIIpv6AddrProfileValidTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpv6AddrProfileValidTime (UINT4 u4FsMIIpv6AddrProfileIndex,
                                    UINT4 u4SetValFsMIIpv6AddrProfileValidTime)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhSetFsipv6AddrProfileValidTime (u4FsMIIpv6AddrProfileIndex,
                                                 u4SetValFsMIIpv6AddrProfileValidTime);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIIpv6AddrProfileValidLifeTimeFlag
 Input       :  The Indices
                FsMIIpv6AddrProfileIndex

                The Object 
                setValFsMIIpv6AddrProfileValidLifeTimeFlag
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpv6AddrProfileValidLifeTimeFlag (UINT4 u4FsMIIpv6AddrProfileIndex,
                                            INT4
                                            i4SetValFsMIIpv6AddrProfileValidLifeTimeFlag)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal =
        nmhSetFsipv6AddrProfileValidLifeTimeFlag (u4FsMIIpv6AddrProfileIndex,
                                                  i4SetValFsMIIpv6AddrProfileValidLifeTimeFlag);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIIpv6AddrProfilePreferredLifeTimeFlag
 Input       :  The Indices
                FsMIIpv6AddrProfileIndex

                The Object 
                setValFsMIIpv6AddrProfilePreferredLifeTimeFlag
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpv6AddrProfilePreferredLifeTimeFlag (UINT4
                                                u4FsMIIpv6AddrProfileIndex,
                                                INT4
                                                i4SetValFsMIIpv6AddrProfilePreferredLifeTimeFlag)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal =
        nmhSetFsipv6AddrProfilePreferredLifeTimeFlag
        (u4FsMIIpv6AddrProfileIndex,
         i4SetValFsMIIpv6AddrProfilePreferredLifeTimeFlag);
    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6AddrProfileStatus
 Input       :  The Indices
                FsMIIpv6AddrProfileIndex

                The Object 
                testValFsMIIpv6AddrProfileStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpv6AddrProfileStatus (UINT4 *pu4ErrorCode,
                                    UINT4 u4FsMIIpv6AddrProfileIndex,
                                    INT4 i4TestValFsMIIpv6AddrProfileStatus)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhTestv2Fsipv6AddrProfileStatus (pu4ErrorCode,
                                                 u4FsMIIpv6AddrProfileIndex,
                                                 i4TestValFsMIIpv6AddrProfileStatus);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6AddrProfilePrefixAdvStatus
 Input       :  The Indices
                FsMIIpv6AddrProfileIndex

                The Object 
                testValFsMIIpv6AddrProfilePrefixAdvStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpv6AddrProfilePrefixAdvStatus (UINT4 *pu4ErrorCode,
                                             UINT4 u4FsMIIpv6AddrProfileIndex,
                                             INT4
                                             i4TestValFsMIIpv6AddrProfilePrefixAdvStatus)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhTestv2Fsipv6AddrProfilePrefixAdvStatus (pu4ErrorCode,
                                                          u4FsMIIpv6AddrProfileIndex,
                                                          i4TestValFsMIIpv6AddrProfilePrefixAdvStatus);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6AddrProfileOnLinkAdvStatus
 Input       :  The Indices
                FsMIIpv6AddrProfileIndex

                The Object 
                testValFsMIIpv6AddrProfileOnLinkAdvStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpv6AddrProfileOnLinkAdvStatus (UINT4 *pu4ErrorCode,
                                             UINT4 u4FsMIIpv6AddrProfileIndex,
                                             INT4
                                             i4TestValFsMIIpv6AddrProfileOnLinkAdvStatus)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhTestv2Fsipv6AddrProfileOnLinkAdvStatus (pu4ErrorCode,
                                                          u4FsMIIpv6AddrProfileIndex,
                                                          i4TestValFsMIIpv6AddrProfileOnLinkAdvStatus);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6AddrProfileAutoConfAdvStatus
 Input       :  The Indices
                FsMIIpv6AddrProfileIndex

                The Object 
                testValFsMIIpv6AddrProfileAutoConfAdvStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpv6AddrProfileAutoConfAdvStatus (UINT4 *pu4ErrorCode,
                                               UINT4 u4FsMIIpv6AddrProfileIndex,
                                               INT4
                                               i4TestValFsMIIpv6AddrProfileAutoConfAdvStatus)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhTestv2Fsipv6AddrProfileAutoConfAdvStatus (pu4ErrorCode,
                                                            u4FsMIIpv6AddrProfileIndex,
                                                            i4TestValFsMIIpv6AddrProfileAutoConfAdvStatus);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6AddrProfilePreferredTime
 Input       :  The Indices
                FsMIIpv6AddrProfileIndex

                The Object 
                testValFsMIIpv6AddrProfilePreferredTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpv6AddrProfilePreferredTime (UINT4 *pu4ErrorCode,
                                           UINT4 u4FsMIIpv6AddrProfileIndex,
                                           UINT4
                                           u4TestValFsMIIpv6AddrProfilePreferredTime)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhTestv2Fsipv6AddrProfilePreferredTime (pu4ErrorCode,
                                                        u4FsMIIpv6AddrProfileIndex,
                                                        u4TestValFsMIIpv6AddrProfilePreferredTime);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6AddrProfileValidTime
 Input       :  The Indices
                FsMIIpv6AddrProfileIndex

                The Object 
                testValFsMIIpv6AddrProfileValidTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpv6AddrProfileValidTime (UINT4 *pu4ErrorCode,
                                       UINT4 u4FsMIIpv6AddrProfileIndex,
                                       UINT4
                                       u4TestValFsMIIpv6AddrProfileValidTime)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhTestv2Fsipv6AddrProfileValidTime (pu4ErrorCode,
                                                    u4FsMIIpv6AddrProfileIndex,
                                                    u4TestValFsMIIpv6AddrProfileValidTime);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6AddrProfileValidLifeTimeFlag
 Input       :  The Indices
                FsMIIpv6AddrProfileIndex

                The Object 
                testValFsMIIpv6AddrProfileValidLifeTimeFlag
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpv6AddrProfileValidLifeTimeFlag (UINT4 *pu4ErrorCode,
                                               UINT4 u4FsMIIpv6AddrProfileIndex,
                                               INT4
                                               i4TestValFsMIIpv6AddrProfileValidLifeTimeFlag)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhTestv2Fsipv6AddrProfileValidLifeTimeFlag (pu4ErrorCode,
                                                            u4FsMIIpv6AddrProfileIndex,
                                                            i4TestValFsMIIpv6AddrProfileValidLifeTimeFlag);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6AddrProfilePreferredLifeTimeFlag
 Input       :  The Indices
                FsMIIpv6AddrProfileIndex

                The Object 
                testValFsMIIpv6AddrProfilePreferredLifeTimeFlag
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpv6AddrProfilePreferredLifeTimeFlag (UINT4 *pu4ErrorCode,
                                                   UINT4
                                                   u4FsMIIpv6AddrProfileIndex,
                                                   INT4
                                                   i4TestValFsMIIpv6AddrProfilePreferredLifeTimeFlag)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhTestv2Fsipv6AddrProfilePreferredLifeTimeFlag (pu4ErrorCode,
                                                                u4FsMIIpv6AddrProfileIndex,
                                                                i4TestValFsMIIpv6AddrProfilePreferredLifeTimeFlag);
    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIIpv6AddrProfileTable
 Input       :  The Indices
                FsMIIpv6AddrProfileIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIIpv6AddrProfileTable (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIIpv6IcmpStatsTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIIpv6IcmpStatsTable
 Input       :  The Indices
                FsMIStdIpContextId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIIpv6IcmpStatsTable (INT4 i4FsMIStdIpContextId)
{
    if ((i4FsMIStdIpContextId < VCM_DEFAULT_CONTEXT) ||
        ((UINT4) i4FsMIStdIpContextId >= IP6_MAX_INSTANCES))
    {
        return SNMP_FAILURE;
    }

    if (Ip6VRExists (i4FsMIStdIpContextId) == IP6_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIIpv6IcmpStatsTable
 Input       :  The Indices
                FsMIStdIpContextId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIIpv6IcmpStatsTable (INT4 *pi4FsMIStdIpContextId)
{
    if (Ip6UtilGetFirstCxtId ((UINT4 *) pi4FsMIStdIpContextId) == IP6_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIIpv6IcmpStatsTable
 Input       :  The Indices
                FsMIStdIpContextId
                nextFsMIStdIpContextId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIIpv6IcmpStatsTable (INT4 i4FsMIStdIpContextId,
                                       INT4 *pi4NextFsMIStdIpContextId)
{
    if (Ip6UtilGetNextCxtId ((UINT4) i4FsMIStdIpContextId,
                             (UINT4 *) pi4NextFsMIStdIpContextId) ==
        IP6_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIIpv6IcmpInMsgs
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                retValFsMIIpv6IcmpInMsgs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6IcmpInMsgs (INT4 i4FsMIStdIpContextId,
                          UINT4 *pu4RetValFsMIIpv6IcmpInMsgs)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (Ip6SelectContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }

    i1RetVal = nmhGetFsipv6IcmpInMsgs (pu4RetValFsMIIpv6IcmpInMsgs);
    Ip6ReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6IcmpInErrors
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                retValFsMIIpv6IcmpInErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6IcmpInErrors (INT4 i4FsMIStdIpContextId,
                            UINT4 *pu4RetValFsMIIpv6IcmpInErrors)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (Ip6SelectContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }

    i1RetVal = nmhGetFsipv6IcmpInErrors (pu4RetValFsMIIpv6IcmpInErrors);
    Ip6ReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6IcmpInDestUnreachs
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                retValFsMIIpv6IcmpInDestUnreachs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6IcmpInDestUnreachs (INT4 i4FsMIStdIpContextId,
                                  UINT4 *pu4RetValFsMIIpv6IcmpInDestUnreachs)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (Ip6SelectContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }

    i1RetVal =
        nmhGetFsipv6IcmpInDestUnreachs (pu4RetValFsMIIpv6IcmpInDestUnreachs);
    Ip6ReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6IcmpInTimeExcds
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                retValFsMIIpv6IcmpInTimeExcds
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6IcmpInTimeExcds (INT4 i4FsMIStdIpContextId,
                               UINT4 *pu4RetValFsMIIpv6IcmpInTimeExcds)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (Ip6SelectContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }

    i1RetVal = nmhGetFsipv6IcmpInTimeExcds (pu4RetValFsMIIpv6IcmpInTimeExcds);
    Ip6ReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6IcmpInParmProbs
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                retValFsMIIpv6IcmpInParmProbs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6IcmpInParmProbs (INT4 i4FsMIStdIpContextId,
                               UINT4 *pu4RetValFsMIIpv6IcmpInParmProbs)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (Ip6SelectContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }

    i1RetVal = nmhGetFsipv6IcmpInParmProbs (pu4RetValFsMIIpv6IcmpInParmProbs);
    Ip6ReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6IcmpInPktTooBigs
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                retValFsMIIpv6IcmpInPktTooBigs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6IcmpInPktTooBigs (INT4 i4FsMIStdIpContextId,
                                UINT4 *pu4RetValFsMIIpv6IcmpInPktTooBigs)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (Ip6SelectContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }

    i1RetVal = nmhGetFsipv6IcmpInPktTooBigs (pu4RetValFsMIIpv6IcmpInPktTooBigs);
    Ip6ReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6IcmpInEchos
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                retValFsMIIpv6IcmpInEchos
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6IcmpInEchos (INT4 i4FsMIStdIpContextId,
                           UINT4 *pu4RetValFsMIIpv6IcmpInEchos)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (Ip6SelectContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }

    i1RetVal = nmhGetFsipv6IcmpInEchos (pu4RetValFsMIIpv6IcmpInEchos);
    Ip6ReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6IcmpInEchoReps
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                retValFsMIIpv6IcmpInEchoReps
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6IcmpInEchoReps (INT4 i4FsMIStdIpContextId,
                              UINT4 *pu4RetValFsMIIpv6IcmpInEchoReps)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (Ip6SelectContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }

    i1RetVal = nmhGetFsipv6IcmpInEchoReps (pu4RetValFsMIIpv6IcmpInEchoReps);
    Ip6ReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6IcmpInRouterSolicits
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                retValFsMIIpv6IcmpInRouterSolicits
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6IcmpInRouterSolicits (INT4 i4FsMIStdIpContextId,
                                    UINT4
                                    *pu4RetValFsMIIpv6IcmpInRouterSolicits)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (Ip6SelectContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }

    i1RetVal =
        nmhGetFsipv6IcmpInRouterSolicits
        (pu4RetValFsMIIpv6IcmpInRouterSolicits);
    Ip6ReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6IcmpInRouterAdvertisements
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                retValFsMIIpv6IcmpInRouterAdvertisements
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6IcmpInRouterAdvertisements (INT4 i4FsMIStdIpContextId,
                                          UINT4
                                          *pu4RetValFsMIIpv6IcmpInRouterAdvertisements)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (Ip6SelectContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }

    i1RetVal =
        nmhGetFsipv6IcmpInRouterAdvertisements
        (pu4RetValFsMIIpv6IcmpInRouterAdvertisements);
    Ip6ReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6IcmpInNeighborSolicits
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                retValFsMIIpv6IcmpInNeighborSolicits
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6IcmpInNeighborSolicits (INT4 i4FsMIStdIpContextId,
                                      UINT4
                                      *pu4RetValFsMIIpv6IcmpInNeighborSolicits)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (Ip6SelectContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }

    i1RetVal =
        nmhGetFsipv6IcmpInNeighborSolicits
        (pu4RetValFsMIIpv6IcmpInNeighborSolicits);
    Ip6ReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6IcmpInNeighborAdvertisements
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                retValFsMIIpv6IcmpInNeighborAdvertisements
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6IcmpInNeighborAdvertisements (INT4 i4FsMIStdIpContextId,
                                            UINT4
                                            *pu4RetValFsMIIpv6IcmpInNeighborAdvertisements)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (Ip6SelectContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }

    i1RetVal =
        nmhGetFsipv6IcmpInNeighborAdvertisements
        (pu4RetValFsMIIpv6IcmpInNeighborAdvertisements);
    Ip6ReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6IcmpInRedirects
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                retValFsMIIpv6IcmpInRedirects
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6IcmpInRedirects (INT4 i4FsMIStdIpContextId,
                               UINT4 *pu4RetValFsMIIpv6IcmpInRedirects)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (Ip6SelectContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }

    i1RetVal = nmhGetFsipv6IcmpInRedirects (pu4RetValFsMIIpv6IcmpInRedirects);
    Ip6ReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6IcmpInAdminProhib
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                retValFsMIIpv6IcmpInAdminProhib
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6IcmpInAdminProhib (INT4 i4FsMIStdIpContextId,
                                 UINT4 *pu4RetValFsMIIpv6IcmpInAdminProhib)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (Ip6SelectContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }

    i1RetVal =
        nmhGetFsipv6IcmpInAdminProhib (pu4RetValFsMIIpv6IcmpInAdminProhib);
    Ip6ReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6IcmpOutMsgs
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                retValFsMIIpv6IcmpOutMsgs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6IcmpOutMsgs (INT4 i4FsMIStdIpContextId,
                           UINT4 *pu4RetValFsMIIpv6IcmpOutMsgs)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (Ip6SelectContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }

    i1RetVal = nmhGetFsipv6IcmpOutMsgs (pu4RetValFsMIIpv6IcmpOutMsgs);
    Ip6ReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6IcmpOutErrors
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                retValFsMIIpv6IcmpOutErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6IcmpOutErrors (INT4 i4FsMIStdIpContextId,
                             UINT4 *pu4RetValFsMIIpv6IcmpOutErrors)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (Ip6SelectContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }

    i1RetVal = nmhGetFsipv6IcmpOutErrors (pu4RetValFsMIIpv6IcmpOutErrors);
    Ip6ReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6IcmpOutDestUnreachs
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                retValFsMIIpv6IcmpOutDestUnreachs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6IcmpOutDestUnreachs (INT4 i4FsMIStdIpContextId,
                                   UINT4 *pu4RetValFsMIIpv6IcmpOutDestUnreachs)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (Ip6SelectContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }

    i1RetVal =
        nmhGetFsipv6IcmpOutDestUnreachs (pu4RetValFsMIIpv6IcmpOutDestUnreachs);
    Ip6ReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6IcmpOutTimeExcds
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                retValFsMIIpv6IcmpOutTimeExcds
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6IcmpOutTimeExcds (INT4 i4FsMIStdIpContextId,
                                UINT4 *pu4RetValFsMIIpv6IcmpOutTimeExcds)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (Ip6SelectContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }

    i1RetVal = nmhGetFsipv6IcmpOutTimeExcds (pu4RetValFsMIIpv6IcmpOutTimeExcds);
    Ip6ReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6IcmpOutParmProbs
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                retValFsMIIpv6IcmpOutParmProbs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6IcmpOutParmProbs (INT4 i4FsMIStdIpContextId,
                                UINT4 *pu4RetValFsMIIpv6IcmpOutParmProbs)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (Ip6SelectContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }

    i1RetVal = nmhGetFsipv6IcmpOutParmProbs (pu4RetValFsMIIpv6IcmpOutParmProbs);
    Ip6ReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6IcmpOutPktTooBigs
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                retValFsMIIpv6IcmpOutPktTooBigs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6IcmpOutPktTooBigs (INT4 i4FsMIStdIpContextId,
                                 UINT4 *pu4RetValFsMIIpv6IcmpOutPktTooBigs)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (Ip6SelectContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }

    i1RetVal =
        nmhGetFsipv6IcmpOutPktTooBigs (pu4RetValFsMIIpv6IcmpOutPktTooBigs);
    Ip6ReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6IcmpOutEchos
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                retValFsMIIpv6IcmpOutEchos
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6IcmpOutEchos (INT4 i4FsMIStdIpContextId,
                            UINT4 *pu4RetValFsMIIpv6IcmpOutEchos)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (Ip6SelectContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }

    i1RetVal = nmhGetFsipv6IcmpOutEchos (pu4RetValFsMIIpv6IcmpOutEchos);
    Ip6ReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6IcmpOutEchoReps
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                retValFsMIIpv6IcmpOutEchoReps
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6IcmpOutEchoReps (INT4 i4FsMIStdIpContextId,
                               UINT4 *pu4RetValFsMIIpv6IcmpOutEchoReps)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (Ip6SelectContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }

    i1RetVal = nmhGetFsipv6IcmpOutEchoReps (pu4RetValFsMIIpv6IcmpOutEchoReps);
    Ip6ReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6IcmpOutRouterSolicits
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                retValFsMIIpv6IcmpOutRouterSolicits
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6IcmpOutRouterSolicits (INT4 i4FsMIStdIpContextId,
                                     UINT4
                                     *pu4RetValFsMIIpv6IcmpOutRouterSolicits)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (Ip6SelectContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }

    i1RetVal =
        nmhGetFsipv6IcmpOutRouterSolicits
        (pu4RetValFsMIIpv6IcmpOutRouterSolicits);
    Ip6ReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6IcmpOutRouterAdvertisements
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                retValFsMIIpv6IcmpOutRouterAdvertisements
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6IcmpOutRouterAdvertisements (INT4 i4FsMIStdIpContextId,
                                           UINT4
                                           *pu4RetValFsMIIpv6IcmpOutRouterAdvertisements)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (Ip6SelectContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }

    i1RetVal =
        nmhGetFsipv6IcmpOutRouterAdvertisements
        (pu4RetValFsMIIpv6IcmpOutRouterAdvertisements);
    Ip6ReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6IcmpOutNeighborSolicits
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                retValFsMIIpv6IcmpOutNeighborSolicits
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6IcmpOutNeighborSolicits (INT4 i4FsMIStdIpContextId,
                                       UINT4
                                       *pu4RetValFsMIIpv6IcmpOutNeighborSolicits)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (Ip6SelectContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }

    i1RetVal =
        nmhGetFsipv6IcmpOutNeighborSolicits
        (pu4RetValFsMIIpv6IcmpOutNeighborSolicits);
    Ip6ReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6IcmpOutNeighborAdvertisements
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                retValFsMIIpv6IcmpOutNeighborAdvertisements
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6IcmpOutNeighborAdvertisements (INT4 i4FsMIStdIpContextId,
                                             UINT4
                                             *pu4RetValFsMIIpv6IcmpOutNeighborAdvertisements)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (Ip6SelectContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }

    i1RetVal =
        nmhGetFsipv6IcmpOutNeighborAdvertisements
        (pu4RetValFsMIIpv6IcmpOutNeighborAdvertisements);
    Ip6ReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6IcmpOutRedirects
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                retValFsMIIpv6IcmpOutRedirects
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6IcmpOutRedirects (INT4 i4FsMIStdIpContextId,
                                UINT4 *pu4RetValFsMIIpv6IcmpOutRedirects)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (Ip6SelectContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }

    i1RetVal = nmhGetFsipv6IcmpOutRedirects (pu4RetValFsMIIpv6IcmpOutRedirects);
    Ip6ReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6IcmpOutAdminProhib
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                retValFsMIIpv6IcmpOutAdminProhib
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6IcmpOutAdminProhib (INT4 i4FsMIStdIpContextId,
                                  UINT4 *pu4RetValFsMIIpv6IcmpOutAdminProhib)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (Ip6SelectContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }

    i1RetVal =
        nmhGetFsipv6IcmpOutAdminProhib (pu4RetValFsMIIpv6IcmpOutAdminProhib);
    Ip6ReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6IcmpInBadCode
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                retValFsMIIpv6IcmpInBadCode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6IcmpInBadCode (INT4 i4FsMIStdIpContextId,
                             UINT4 *pu4RetValFsMIIpv6IcmpInBadCode)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (Ip6SelectContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }

    i1RetVal = nmhGetFsipv6IcmpInBadCode (pu4RetValFsMIIpv6IcmpInBadCode);
    Ip6ReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6IcmpInNARouterFlagSet
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                retValFsMIIpv6IcmpInNARouterFlagSet
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6IcmpInNARouterFlagSet (INT4 i4FsMIStdIpContextId,
                                     UINT4
                                     *pu4RetValFsMIIpv6IcmpInNARouterFlagSet)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (Ip6SelectContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }

    i1RetVal =
        nmhGetFsipv6IcmpInNARouterFlagSet
        (pu4RetValFsMIIpv6IcmpInNARouterFlagSet);
    Ip6ReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6IcmpInNASolicitedFlagSet
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                retValFsMIIpv6IcmpInNASolicitedFlagSet
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6IcmpInNASolicitedFlagSet (INT4 i4FsMIStdIpContextId,
                                        UINT4
                                        *pu4RetValFsMIIpv6IcmpInNASolicitedFlagSet)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (Ip6SelectContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }

    i1RetVal =
        nmhGetFsipv6IcmpInNASolicitedFlagSet
        (pu4RetValFsMIIpv6IcmpInNASolicitedFlagSet);
    Ip6ReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6IcmpInNAOverrideFlagSet
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                retValFsMIIpv6IcmpInNAOverrideFlagSet
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6IcmpInNAOverrideFlagSet (INT4 i4FsMIStdIpContextId,
                                       UINT4
                                       *pu4RetValFsMIIpv6IcmpInNAOverrideFlagSet)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (Ip6SelectContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }

    i1RetVal =
        nmhGetFsipv6IcmpInNAOverrideFlagSet
        (pu4RetValFsMIIpv6IcmpInNAOverrideFlagSet);
    Ip6ReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6IcmpOutNARouterFlagSet
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                retValFsMIIpv6IcmpOutNARouterFlagSet
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6IcmpOutNARouterFlagSet (INT4 i4FsMIStdIpContextId,
                                      UINT4
                                      *pu4RetValFsMIIpv6IcmpOutNARouterFlagSet)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (Ip6SelectContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }

    i1RetVal =
        nmhGetFsipv6IcmpOutNARouterFlagSet
        (pu4RetValFsMIIpv6IcmpOutNARouterFlagSet);
    Ip6ReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6IcmpOutNASolicitedFlagSet
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                retValFsMIIpv6IcmpOutNASolicitedFlagSet
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6IcmpOutNASolicitedFlagSet (INT4 i4FsMIStdIpContextId,
                                         UINT4
                                         *pu4RetValFsMIIpv6IcmpOutNASolicitedFlagSet)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (Ip6SelectContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }

    i1RetVal =
        nmhGetFsipv6IcmpOutNASolicitedFlagSet
        (pu4RetValFsMIIpv6IcmpOutNASolicitedFlagSet);
    Ip6ReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6IcmpOutNAOverrideFlagSet
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                retValFsMIIpv6IcmpOutNAOverrideFlagSet
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6IcmpOutNAOverrideFlagSet (INT4 i4FsMIStdIpContextId,
                                        UINT4
                                        *pu4RetValFsMIIpv6IcmpOutNAOverrideFlagSet)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (Ip6SelectContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }

    i1RetVal =
        nmhGetFsipv6IcmpOutNAOverrideFlagSet
        (pu4RetValFsMIIpv6IcmpOutNAOverrideFlagSet);
    Ip6ReleaseContext ();
    return i1RetVal;
}

/* LOW LEVEL Routines for Table : FsMIIpv6PmtuTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIIpv6PmtuTable
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIIpv6PmtuDest
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIIpv6PmtuTable (INT4 i4FsMIStdIpContextId,
                                           tSNMP_OCTET_STRING_TYPE *
                                           pFsMIIpv6PmtuDest)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if ((i4FsMIStdIpContextId < VCM_DEFAULT_CONTEXT) ||
        ((UINT4) i4FsMIStdIpContextId >= IP6_MAX_INSTANCES))
    {
        return SNMP_FAILURE;
    }

    if (Ip6VRExists (i4FsMIStdIpContextId) == IP6_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (Ip6SelectContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }

    i1RetVal = nmhValidateIndexInstanceFsipv6PmtuTable (pFsMIIpv6PmtuDest);
    Ip6ReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIIpv6PmtuTable
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIIpv6PmtuDest
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIIpv6PmtuTable (INT4 *pi4FsMIStdIpContextId,
                                   tSNMP_OCTET_STRING_TYPE * pFsMIIpv6PmtuDest)
{
    INT1                i1RetVal = SNMP_FAILURE;
    INT4                i4FsMIStdIpContextId;
    INT4                i4Value = IP6_FAILURE;

    i4Value = Ip6UtilGetFirstCxtId ((UINT4 *) pi4FsMIStdIpContextId);

    while (i4Value == IP6_SUCCESS)
    {
        i4FsMIStdIpContextId = *pi4FsMIStdIpContextId;
        if (Ip6SelectContext (i4FsMIStdIpContextId) == SNMP_SUCCESS)
        {
            i1RetVal = nmhGetFirstIndexFsipv6PmtuTable (pFsMIIpv6PmtuDest);
            Ip6ReleaseContext ();
        }
        if (i1RetVal == SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
        i4Value = Ip6UtilGetNextCxtId ((UINT4) i4FsMIStdIpContextId,
                                       (UINT4 *) pi4FsMIStdIpContextId);
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIIpv6PmtuTable
 Input       :  The Indices
                FsMIStdIpContextId
                nextFsMIStdIpContextId
                FsMIIpv6PmtuDest
                nextFsMIIpv6PmtuDest
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIIpv6PmtuTable (INT4 i4FsMIStdIpContextId,
                                  INT4 *pi4NextFsMIStdIpContextId,
                                  tSNMP_OCTET_STRING_TYPE * pFsMIIpv6PmtuDest,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pNextFsMIIpv6PmtuDest)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (Ip6VRExists (i4FsMIStdIpContextId) == IP6_SUCCESS)
    {
        if (Ip6SelectContext (i4FsMIStdIpContextId) == SNMP_SUCCESS)
        {
            i1RetVal = nmhGetNextIndexFsipv6PmtuTable (pFsMIIpv6PmtuDest,
                                                       pNextFsMIIpv6PmtuDest);
            Ip6ReleaseContext ();
        }
        if (i1RetVal == SNMP_SUCCESS)
        {
            *pi4NextFsMIStdIpContextId = i4FsMIStdIpContextId;
            return SNMP_SUCCESS;
        }
    }

    while ((Ip6UtilGetNextCxtId ((UINT4) i4FsMIStdIpContextId,
                                 (UINT4 *) pi4NextFsMIStdIpContextId)) ==
           IP6_SUCCESS)
    {
        if (Ip6SelectContext (*pi4NextFsMIStdIpContextId) == SNMP_SUCCESS)
        {
            i1RetVal = nmhGetFirstIndexFsipv6PmtuTable (pNextFsMIIpv6PmtuDest);
            Ip6ReleaseContext ();
        }
        if (i1RetVal == SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
        i4FsMIStdIpContextId = *pi4NextFsMIStdIpContextId;
    }

    return SNMP_FAILURE;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIIpv6Pmtu
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIIpv6PmtuDest

                The Object 
                retValFsMIIpv6Pmtu
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6Pmtu (INT4 i4FsMIStdIpContextId,
                    tSNMP_OCTET_STRING_TYPE * pFsMIIpv6PmtuDest,
                    INT4 *pi4RetValFsMIIpv6Pmtu)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (Ip6SelectContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }

    i1RetVal = nmhGetFsipv6Pmtu (pFsMIIpv6PmtuDest, pi4RetValFsMIIpv6Pmtu);
    Ip6ReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6PmtuTimeStamp
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIIpv6PmtuDest

                The Object 
                retValFsMIIpv6PmtuTimeStamp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6PmtuTimeStamp (INT4 i4FsMIStdIpContextId,
                             tSNMP_OCTET_STRING_TYPE * pFsMIIpv6PmtuDest,
                             INT4 *pi4RetValFsMIIpv6PmtuTimeStamp)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (Ip6SelectContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }

    i1RetVal =
        nmhGetFsipv6PmtuTimeStamp (pFsMIIpv6PmtuDest,
                                   pi4RetValFsMIIpv6PmtuTimeStamp);
    Ip6ReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6PmtuAdminStatus
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIIpv6PmtuDest

                The Object 
                retValFsMIIpv6PmtuAdminStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6PmtuAdminStatus (INT4 i4FsMIStdIpContextId,
                               tSNMP_OCTET_STRING_TYPE * pFsMIIpv6PmtuDest,
                               INT4 *pi4RetValFsMIIpv6PmtuAdminStatus)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (Ip6SelectContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }

    i1RetVal =
        nmhGetFsipv6PmtuAdminStatus (pFsMIIpv6PmtuDest,
                                     pi4RetValFsMIIpv6PmtuAdminStatus);
    Ip6ReleaseContext ();
    return i1RetVal;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIIpv6Pmtu
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIIpv6PmtuDest

                The Object 
                setValFsMIIpv6Pmtu
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpv6Pmtu (INT4 i4FsMIStdIpContextId,
                    tSNMP_OCTET_STRING_TYPE * pFsMIIpv6PmtuDest,
                    INT4 i4SetValFsMIIpv6Pmtu)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (Ip6SelectContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }

    i1RetVal = nmhSetFsipv6Pmtu (pFsMIIpv6PmtuDest, i4SetValFsMIIpv6Pmtu);
    Ip6ReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIIpv6PmtuAdminStatus
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIIpv6PmtuDest

                The Object 
                setValFsMIIpv6PmtuAdminStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpv6PmtuAdminStatus (INT4 i4FsMIStdIpContextId,
                               tSNMP_OCTET_STRING_TYPE * pFsMIIpv6PmtuDest,
                               INT4 i4SetValFsMIIpv6PmtuAdminStatus)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (Ip6SelectContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }

    i1RetVal =
        nmhSetFsipv6PmtuAdminStatus (pFsMIIpv6PmtuDest,
                                     i4SetValFsMIIpv6PmtuAdminStatus);
    Ip6ReleaseContext ();
    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6Pmtu
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIIpv6PmtuDest

                The Object 
                testValFsMIIpv6Pmtu
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpv6Pmtu (UINT4 *pu4ErrorCode, INT4 i4FsMIStdIpContextId,
                       tSNMP_OCTET_STRING_TYPE * pFsMIIpv6PmtuDest,
                       INT4 i4TestValFsMIIpv6Pmtu)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (Ip6SelectContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1RetVal;
    }

    i1RetVal =
        nmhTestv2Fsipv6Pmtu (pu4ErrorCode, pFsMIIpv6PmtuDest,
                             i4TestValFsMIIpv6Pmtu);
    Ip6ReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6PmtuAdminStatus
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIIpv6PmtuDest

                The Object 
                testValFsMIIpv6PmtuAdminStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpv6PmtuAdminStatus (UINT4 *pu4ErrorCode,
                                  INT4 i4FsMIStdIpContextId,
                                  tSNMP_OCTET_STRING_TYPE * pFsMIIpv6PmtuDest,
                                  INT4 i4TestValFsMIIpv6PmtuAdminStatus)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (Ip6SelectContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1RetVal;
    }

    i1RetVal =
        nmhTestv2Fsipv6PmtuAdminStatus (pu4ErrorCode, pFsMIIpv6PmtuDest,
                                        i4TestValFsMIIpv6PmtuAdminStatus);
    Ip6ReleaseContext ();
    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIIpv6PmtuTable
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIIpv6PmtuDest
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIIpv6PmtuTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIIpv6RouteTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIIpv6RouteTable
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIIpv6RouteDest
                FsMIIpv6RoutePfxLength
                FsMIIpv6RouteProtocol
                FsMIIpv6RouteNextHop
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIIpv6RouteTable (INT4 i4FsMIStdIpContextId,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pFsMIIpv6RouteDest,
                                            INT4 i4FsMIIpv6RoutePfxLength,
                                            INT4 i4FsMIIpv6RouteProtocol,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pFsMIIpv6RouteNextHop)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if ((i4FsMIStdIpContextId < VCM_DEFAULT_CONTEXT) ||
        ((UINT4) i4FsMIStdIpContextId >= IP6_MAX_INSTANCES))
    {
        return SNMP_FAILURE;
    }

    if (UtilRtm6VcmIsVcExist (i4FsMIStdIpContextId) == RTM6_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (UtilRtm6SetContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }

    i1RetVal = nmhValidateIndexInstanceFsipv6RouteTable
        (pFsMIIpv6RouteDest,
         i4FsMIIpv6RoutePfxLength,
         i4FsMIIpv6RouteProtocol, pFsMIIpv6RouteNextHop);
    UtilRtm6ResetContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIIpv6RouteTable
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIIpv6RouteDest
                FsMIIpv6RoutePfxLength
                FsMIIpv6RouteProtocol
                FsMIIpv6RouteNextHop
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIIpv6RouteTable (INT4 *pi4FsMIStdIpContextId,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsMIIpv6RouteDest,
                                    INT4 *pi4FsMIIpv6RoutePfxLength,
                                    INT4 *pi4FsMIIpv6RouteProtocol,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsMIIpv6RouteNextHop)
{
    INT4                i4FsMIStdIpContextId = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    if (UtilRtm6GetFirstCxtId ((UINT4 *) pi4FsMIStdIpContextId) == RTM6_FAILURE)
    {
        return i1RetVal;
    }
    do
    {
        i4FsMIStdIpContextId = *pi4FsMIStdIpContextId;
        if (UtilRtm6SetContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
        {
            continue;
        }

        i1RetVal = nmhGetFirstIndexFsipv6RouteTable (pFsMIIpv6RouteDest,
                                                     pi4FsMIIpv6RoutePfxLength,
                                                     pi4FsMIIpv6RouteProtocol,
                                                     pFsMIIpv6RouteNextHop);
        UtilRtm6ResetContext ();
        if (i1RetVal == SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
    }
    while (UtilRtm6GetNextCxtId ((UINT4) i4FsMIStdIpContextId,
                                 (UINT4 *) pi4FsMIStdIpContextId) !=
           RTM6_FAILURE);

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIIpv6RouteTable
 Input       :  The Indices
                FsMIStdIpContextId
                nextFsMIStdIpContextId
                FsMIIpv6RouteDest
                nextFsMIIpv6RouteDest
                FsMIIpv6RoutePfxLength
                nextFsMIIpv6RoutePfxLength
                FsMIIpv6RouteProtocol
                nextFsMIIpv6RouteProtocol
                FsMIIpv6RouteNextHop
                nextFsMIIpv6RouteNextHop
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIIpv6RouteTable (INT4 i4FsMIStdIpContextId,
                                   INT4 *pi4NextFsMIStdIpContextId,
                                   tSNMP_OCTET_STRING_TYPE * pFsMIIpv6RouteDest,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pNextFsMIIpv6RouteDest,
                                   INT4 i4FsMIIpv6RoutePfxLength,
                                   INT4 *pi4NextFsMIIpv6RoutePfxLength,
                                   INT4 i4FsMIIpv6RouteProtocol,
                                   INT4 *pi4NextFsMIIpv6RouteProtocol,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsMIIpv6RouteNextHop,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pNextFsMIIpv6RouteNextHop)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (UtilRtm6VcmIsVcExist (i4FsMIStdIpContextId) == RTM6_SUCCESS)
    {
        if (UtilRtm6SetContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
        {
            return i1RetVal;
        }

        i1RetVal = nmhGetNextIndexFsipv6RouteTable
            (pFsMIIpv6RouteDest,
             pNextFsMIIpv6RouteDest,
             i4FsMIIpv6RoutePfxLength,
             pi4NextFsMIIpv6RoutePfxLength,
             i4FsMIIpv6RouteProtocol,
             pi4NextFsMIIpv6RouteProtocol,
             pFsMIIpv6RouteNextHop, pNextFsMIIpv6RouteNextHop);
        UtilRtm6ResetContext ();
    }

    if (i1RetVal == SNMP_FAILURE)
    {
        while ((UtilRtm6GetNextCxtId ((UINT4) i4FsMIStdIpContextId,
                                      (UINT4 *) pi4NextFsMIStdIpContextId)) ==
               RTM6_SUCCESS)
        {
            if (UtilRtm6SetContext (*pi4NextFsMIStdIpContextId) == SNMP_FAILURE)
            {
                break;
            }

            i1RetVal = nmhGetFirstIndexFsipv6RouteTable
                (pNextFsMIIpv6RouteDest,
                 pi4NextFsMIIpv6RoutePfxLength,
                 pi4NextFsMIIpv6RouteProtocol, pNextFsMIIpv6RouteNextHop);

            UtilRtm6ResetContext ();
            if (i1RetVal == SNMP_SUCCESS)
            {
                break;
            }
            i4FsMIStdIpContextId = *pi4NextFsMIStdIpContextId;
        }
    }
    else
    {
        *pi4NextFsMIStdIpContextId = i4FsMIStdIpContextId;
    }

    return i1RetVal;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIIpv6RouteIfIndex
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIIpv6RouteDest
                FsMIIpv6RoutePfxLength
                FsMIIpv6RouteProtocol
                FsMIIpv6RouteNextHop

                The Object 
                retValFsMIIpv6RouteIfIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6RouteIfIndex (INT4 i4FsMIStdIpContextId,
                            tSNMP_OCTET_STRING_TYPE * pFsMIIpv6RouteDest,
                            INT4 i4FsMIIpv6RoutePfxLength,
                            INT4 i4FsMIIpv6RouteProtocol,
                            tSNMP_OCTET_STRING_TYPE * pFsMIIpv6RouteNextHop,
                            INT4 *pi4RetValFsMIIpv6RouteIfIndex)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (UtilRtm6SetContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }

    i1RetVal =
        nmhGetFsipv6RouteIfIndex (pFsMIIpv6RouteDest, i4FsMIIpv6RoutePfxLength,
                                  i4FsMIIpv6RouteProtocol,
                                  pFsMIIpv6RouteNextHop,
                                  pi4RetValFsMIIpv6RouteIfIndex);
    UtilRtm6ResetContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6RouteMetric
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIIpv6RouteDest
                FsMIIpv6RoutePfxLength
                FsMIIpv6RouteProtocol
                FsMIIpv6RouteNextHop

                The Object 
                retValFsMIIpv6RouteMetric
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6RouteMetric (INT4 i4FsMIStdIpContextId,
                           tSNMP_OCTET_STRING_TYPE * pFsMIIpv6RouteDest,
                           INT4 i4FsMIIpv6RoutePfxLength,
                           INT4 i4FsMIIpv6RouteProtocol,
                           tSNMP_OCTET_STRING_TYPE * pFsMIIpv6RouteNextHop,
                           UINT4 *pu4RetValFsMIIpv6RouteMetric)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (UtilRtm6SetContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }

    i1RetVal =
        nmhGetFsipv6RouteMetric (pFsMIIpv6RouteDest, i4FsMIIpv6RoutePfxLength,
                                 i4FsMIIpv6RouteProtocol, pFsMIIpv6RouteNextHop,
                                 pu4RetValFsMIIpv6RouteMetric);
    UtilRtm6ResetContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6RouteType
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIIpv6RouteDest
                FsMIIpv6RoutePfxLength
                FsMIIpv6RouteProtocol
                FsMIIpv6RouteNextHop

                The Object 
                retValFsMIIpv6RouteType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6RouteType (INT4 i4FsMIStdIpContextId,
                         tSNMP_OCTET_STRING_TYPE * pFsMIIpv6RouteDest,
                         INT4 i4FsMIIpv6RoutePfxLength,
                         INT4 i4FsMIIpv6RouteProtocol,
                         tSNMP_OCTET_STRING_TYPE * pFsMIIpv6RouteNextHop,
                         INT4 *pi4RetValFsMIIpv6RouteType)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (UtilRtm6SetContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }

    i1RetVal =
        nmhGetFsipv6RouteType (pFsMIIpv6RouteDest, i4FsMIIpv6RoutePfxLength,
                               i4FsMIIpv6RouteProtocol, pFsMIIpv6RouteNextHop,
                               pi4RetValFsMIIpv6RouteType);
    UtilRtm6ResetContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6RouteTag
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIIpv6RouteDest
                FsMIIpv6RoutePfxLength
                FsMIIpv6RouteProtocol
                FsMIIpv6RouteNextHop

                The Object 
                retValFsMIIpv6RouteTag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6RouteTag (INT4 i4FsMIStdIpContextId,
                        tSNMP_OCTET_STRING_TYPE * pFsMIIpv6RouteDest,
                        INT4 i4FsMIIpv6RoutePfxLength,
                        INT4 i4FsMIIpv6RouteProtocol,
                        tSNMP_OCTET_STRING_TYPE * pFsMIIpv6RouteNextHop,
                        UINT4 *pu4RetValFsMIIpv6RouteTag)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (UtilRtm6SetContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }

    i1RetVal =
        nmhGetFsipv6RouteTag (pFsMIIpv6RouteDest, i4FsMIIpv6RoutePfxLength,
                              i4FsMIIpv6RouteProtocol, pFsMIIpv6RouteNextHop,
                              pu4RetValFsMIIpv6RouteTag);
    UtilRtm6ResetContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6RouteAge
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIIpv6RouteDest
                FsMIIpv6RoutePfxLength
                FsMIIpv6RouteProtocol
                FsMIIpv6RouteNextHop

                The Object 
                retValFsMIIpv6RouteAge
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6RouteAge (INT4 i4FsMIStdIpContextId,
                        tSNMP_OCTET_STRING_TYPE * pFsMIIpv6RouteDest,
                        INT4 i4FsMIIpv6RoutePfxLength,
                        INT4 i4FsMIIpv6RouteProtocol,
                        tSNMP_OCTET_STRING_TYPE * pFsMIIpv6RouteNextHop,
                        INT4 *pi4RetValFsMIIpv6RouteAge)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (UtilRtm6SetContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }

    i1RetVal =
        nmhGetFsipv6RouteAge (pFsMIIpv6RouteDest, i4FsMIIpv6RoutePfxLength,
                              i4FsMIIpv6RouteProtocol, pFsMIIpv6RouteNextHop,
                              pi4RetValFsMIIpv6RouteAge);
    UtilRtm6ResetContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6RouteAdminStatus
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIIpv6RouteDest
                FsMIIpv6RoutePfxLength
                FsMIIpv6RouteProtocol
                FsMIIpv6RouteNextHop

                The Object 
                retValFsMIIpv6RouteAdminStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6RouteAdminStatus (INT4 i4FsMIStdIpContextId,
                                tSNMP_OCTET_STRING_TYPE * pFsMIIpv6RouteDest,
                                INT4 i4FsMIIpv6RoutePfxLength,
                                INT4 i4FsMIIpv6RouteProtocol,
                                tSNMP_OCTET_STRING_TYPE * pFsMIIpv6RouteNextHop,
                                INT4 *pi4RetValFsMIIpv6RouteAdminStatus)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (UtilRtm6SetContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }

    i1RetVal =
        nmhGetFsipv6RouteAdminStatus (pFsMIIpv6RouteDest,
                                      i4FsMIIpv6RoutePfxLength,
                                      i4FsMIIpv6RouteProtocol,
                                      pFsMIIpv6RouteNextHop,
                                      pi4RetValFsMIIpv6RouteAdminStatus);
    UtilRtm6ResetContext ();
    return i1RetVal;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIIpv6RouteIfIndex
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIIpv6RouteDest
                FsMIIpv6RoutePfxLength
                FsMIIpv6RouteProtocol
                FsMIIpv6RouteNextHop

                The Object 
                setValFsMIIpv6RouteIfIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpv6RouteIfIndex (INT4 i4FsMIStdIpContextId,
                            tSNMP_OCTET_STRING_TYPE * pFsMIIpv6RouteDest,
                            INT4 i4FsMIIpv6RoutePfxLength,
                            INT4 i4FsMIIpv6RouteProtocol,
                            tSNMP_OCTET_STRING_TYPE * pFsMIIpv6RouteNextHop,
                            INT4 i4SetValFsMIIpv6RouteIfIndex)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (UtilRtm6SetContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }

    i1RetVal =
        nmhSetFsipv6RouteIfIndex (pFsMIIpv6RouteDest, i4FsMIIpv6RoutePfxLength,
                                  i4FsMIIpv6RouteProtocol,
                                  pFsMIIpv6RouteNextHop,
                                  i4SetValFsMIIpv6RouteIfIndex);
    UtilRtm6ResetContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIIpv6RouteMetric
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIIpv6RouteDest
                FsMIIpv6RoutePfxLength
                FsMIIpv6RouteProtocol
                FsMIIpv6RouteNextHop

                The Object 
                setValFsMIIpv6RouteMetric
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpv6RouteMetric (INT4 i4FsMIStdIpContextId,
                           tSNMP_OCTET_STRING_TYPE * pFsMIIpv6RouteDest,
                           INT4 i4FsMIIpv6RoutePfxLength,
                           INT4 i4FsMIIpv6RouteProtocol,
                           tSNMP_OCTET_STRING_TYPE * pFsMIIpv6RouteNextHop,
                           UINT4 u4SetValFsMIIpv6RouteMetric)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (UtilRtm6SetContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }

    i1RetVal =
        nmhSetFsipv6RouteMetric (pFsMIIpv6RouteDest, i4FsMIIpv6RoutePfxLength,
                                 i4FsMIIpv6RouteProtocol, pFsMIIpv6RouteNextHop,
                                 u4SetValFsMIIpv6RouteMetric);
    UtilRtm6ResetContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIIpv6RouteType
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIIpv6RouteDest
                FsMIIpv6RoutePfxLength
                FsMIIpv6RouteProtocol
                FsMIIpv6RouteNextHop

                The Object 
                setValFsMIIpv6RouteType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpv6RouteType (INT4 i4FsMIStdIpContextId,
                         tSNMP_OCTET_STRING_TYPE * pFsMIIpv6RouteDest,
                         INT4 i4FsMIIpv6RoutePfxLength,
                         INT4 i4FsMIIpv6RouteProtocol,
                         tSNMP_OCTET_STRING_TYPE * pFsMIIpv6RouteNextHop,
                         INT4 i4SetValFsMIIpv6RouteType)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (UtilRtm6SetContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }

    i1RetVal =
        nmhSetFsipv6RouteType (pFsMIIpv6RouteDest, i4FsMIIpv6RoutePfxLength,
                               i4FsMIIpv6RouteProtocol, pFsMIIpv6RouteNextHop,
                               i4SetValFsMIIpv6RouteType);
    UtilRtm6ResetContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIIpv6RouteTag
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIIpv6RouteDest
                FsMIIpv6RoutePfxLength
                FsMIIpv6RouteProtocol
                FsMIIpv6RouteNextHop

                The Object 
                setValFsMIIpv6RouteTag
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpv6RouteTag (INT4 i4FsMIStdIpContextId,
                        tSNMP_OCTET_STRING_TYPE * pFsMIIpv6RouteDest,
                        INT4 i4FsMIIpv6RoutePfxLength,
                        INT4 i4FsMIIpv6RouteProtocol,
                        tSNMP_OCTET_STRING_TYPE * pFsMIIpv6RouteNextHop,
                        UINT4 u4SetValFsMIIpv6RouteTag)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (UtilRtm6SetContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }

    i1RetVal =
        nmhSetFsipv6RouteTag (pFsMIIpv6RouteDest, i4FsMIIpv6RoutePfxLength,
                              i4FsMIIpv6RouteProtocol, pFsMIIpv6RouteNextHop,
                              u4SetValFsMIIpv6RouteTag);
    UtilRtm6ResetContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIIpv6RouteAdminStatus
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIIpv6RouteDest
                FsMIIpv6RoutePfxLength
                FsMIIpv6RouteProtocol
                FsMIIpv6RouteNextHop

                The Object 
                setValFsMIIpv6RouteAdminStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpv6RouteAdminStatus (INT4 i4FsMIStdIpContextId,
                                tSNMP_OCTET_STRING_TYPE * pFsMIIpv6RouteDest,
                                INT4 i4FsMIIpv6RoutePfxLength,
                                INT4 i4FsMIIpv6RouteProtocol,
                                tSNMP_OCTET_STRING_TYPE * pFsMIIpv6RouteNextHop,
                                INT4 i4SetValFsMIIpv6RouteAdminStatus)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (UtilRtm6SetContext ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }

    i1RetVal =
        nmhSetFsipv6RouteAdminStatus (pFsMIIpv6RouteDest,
                                      i4FsMIIpv6RoutePfxLength,
                                      i4FsMIIpv6RouteProtocol,
                                      pFsMIIpv6RouteNextHop,
                                      i4SetValFsMIIpv6RouteAdminStatus);
    UtilRtm6ResetContext ();
    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6RouteIfIndex
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIIpv6RouteDest
                FsMIIpv6RoutePfxLength
                FsMIIpv6RouteProtocol
                FsMIIpv6RouteNextHop

                The Object 
                testValFsMIIpv6RouteIfIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpv6RouteIfIndex (UINT4 *pu4ErrorCode, INT4 i4FsMIStdIpContextId,
                               tSNMP_OCTET_STRING_TYPE * pFsMIIpv6RouteDest,
                               INT4 i4FsMIIpv6RoutePfxLength,
                               INT4 i4FsMIIpv6RouteProtocol,
                               tSNMP_OCTET_STRING_TYPE * pFsMIIpv6RouteNextHop,
                               INT4 i4TestValFsMIIpv6RouteIfIndex)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (UtilRtm6SetContext ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1RetVal;
    }

    i1RetVal =
        nmhTestv2Fsipv6RouteIfIndex (pu4ErrorCode, pFsMIIpv6RouteDest,
                                     i4FsMIIpv6RoutePfxLength,
                                     i4FsMIIpv6RouteProtocol,
                                     pFsMIIpv6RouteNextHop,
                                     i4TestValFsMIIpv6RouteIfIndex);
    UtilRtm6ResetContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6RouteMetric
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIIpv6RouteDest
                FsMIIpv6RoutePfxLength
                FsMIIpv6RouteProtocol
                FsMIIpv6RouteNextHop

                The Object 
                testValFsMIIpv6RouteMetric
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpv6RouteMetric (UINT4 *pu4ErrorCode, INT4 i4FsMIStdIpContextId,
                              tSNMP_OCTET_STRING_TYPE * pFsMIIpv6RouteDest,
                              INT4 i4FsMIIpv6RoutePfxLength,
                              INT4 i4FsMIIpv6RouteProtocol,
                              tSNMP_OCTET_STRING_TYPE * pFsMIIpv6RouteNextHop,
                              UINT4 u4TestValFsMIIpv6RouteMetric)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (UtilRtm6SetContext ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1RetVal;
    }

    i1RetVal =
        nmhTestv2Fsipv6RouteMetric (pu4ErrorCode, pFsMIIpv6RouteDest,
                                    i4FsMIIpv6RoutePfxLength,
                                    i4FsMIIpv6RouteProtocol,
                                    pFsMIIpv6RouteNextHop,
                                    u4TestValFsMIIpv6RouteMetric);
    UtilRtm6ResetContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6RouteType
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIIpv6RouteDest
                FsMIIpv6RoutePfxLength
                FsMIIpv6RouteProtocol
                FsMIIpv6RouteNextHop

                The Object 
                testValFsMIIpv6RouteType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpv6RouteType (UINT4 *pu4ErrorCode, INT4 i4FsMIStdIpContextId,
                            tSNMP_OCTET_STRING_TYPE * pFsMIIpv6RouteDest,
                            INT4 i4FsMIIpv6RoutePfxLength,
                            INT4 i4FsMIIpv6RouteProtocol,
                            tSNMP_OCTET_STRING_TYPE * pFsMIIpv6RouteNextHop,
                            INT4 i4TestValFsMIIpv6RouteType)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (UtilRtm6SetContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1RetVal;
    }

    i1RetVal =
        nmhTestv2Fsipv6RouteType (pu4ErrorCode, pFsMIIpv6RouteDest,
                                  i4FsMIIpv6RoutePfxLength,
                                  i4FsMIIpv6RouteProtocol,
                                  pFsMIIpv6RouteNextHop,
                                  i4TestValFsMIIpv6RouteType);
    UtilRtm6ResetContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6RouteTag
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIIpv6RouteDest
                FsMIIpv6RoutePfxLength
                FsMIIpv6RouteProtocol
                FsMIIpv6RouteNextHop

                The Object 
                testValFsMIIpv6RouteTag
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpv6RouteTag (UINT4 *pu4ErrorCode, INT4 i4FsMIStdIpContextId,
                           tSNMP_OCTET_STRING_TYPE * pFsMIIpv6RouteDest,
                           INT4 i4FsMIIpv6RoutePfxLength,
                           INT4 i4FsMIIpv6RouteProtocol,
                           tSNMP_OCTET_STRING_TYPE * pFsMIIpv6RouteNextHop,
                           UINT4 u4TestValFsMIIpv6RouteTag)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (UtilRtm6SetContext ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1RetVal;
    }

    i1RetVal =
        nmhTestv2Fsipv6RouteTag (pu4ErrorCode, pFsMIIpv6RouteDest,
                                 i4FsMIIpv6RoutePfxLength,
                                 i4FsMIIpv6RouteProtocol, pFsMIIpv6RouteNextHop,
                                 u4TestValFsMIIpv6RouteTag);
    UtilRtm6ResetContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6RouteAdminStatus
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIIpv6RouteDest
                FsMIIpv6RoutePfxLength
                FsMIIpv6RouteProtocol
                FsMIIpv6RouteNextHop

                The Object 
                testValFsMIIpv6RouteAdminStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpv6RouteAdminStatus (UINT4 *pu4ErrorCode,
                                   INT4 i4FsMIStdIpContextId,
                                   tSNMP_OCTET_STRING_TYPE * pFsMIIpv6RouteDest,
                                   INT4 i4FsMIIpv6RoutePfxLength,
                                   INT4 i4FsMIIpv6RouteProtocol,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsMIIpv6RouteNextHop,
                                   INT4 i4TestValFsMIIpv6RouteAdminStatus)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (UtilRtm6SetContext ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1RetVal;
    }

    i1RetVal =
        nmhTestv2Fsipv6RouteAdminStatus (pu4ErrorCode, pFsMIIpv6RouteDest,
                                         i4FsMIIpv6RoutePfxLength,
                                         i4FsMIIpv6RouteProtocol,
                                         pFsMIIpv6RouteNextHop,
                                         i4TestValFsMIIpv6RouteAdminStatus);
    UtilRtm6ResetContext ();
    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIIpv6RouteTable
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIIpv6RouteDest
                FsMIIpv6RoutePfxLength
                FsMIIpv6RouteProtocol
                FsMIIpv6RouteNextHop
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIIpv6RouteTable (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIIpv6PrefTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIIpv6PrefTable
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIIpv6Protocol
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIIpv6PrefTable (INT4 i4FsMIStdIpContextId,
                                           INT4 i4FsMIIpv6Protocol)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if ((i4FsMIStdIpContextId < VCM_DEFAULT_CONTEXT) ||
        ((UINT4) i4FsMIStdIpContextId >= IP6_MAX_INSTANCES))
    {
        return SNMP_FAILURE;
    }

    if (UtilRtm6VcmIsVcExist ((UINT4) i4FsMIStdIpContextId) == RTM6_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (UtilRtm6SetContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }

    i1RetVal = nmhValidateIndexInstanceFsipv6PrefTable (i4FsMIIpv6Protocol);
    UtilRtm6ResetContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIIpv6PrefTable
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIIpv6Protocol
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIIpv6PrefTable (INT4 *pi4FsMIStdIpContextId,
                                   INT4 *pi4FsMIIpv6Protocol)
{
    INT4                i4FsMIStdIpContextId;
    INT1                i1RetVal = SNMP_FAILURE;
    INT4                i4Value = RTM6_FAILURE;

    i4Value = UtilRtm6GetFirstCxtId ((UINT4 *) pi4FsMIStdIpContextId);

    while (i4Value == RTM6_SUCCESS)
    {
        i4FsMIStdIpContextId = *pi4FsMIStdIpContextId;
        if (UtilRtm6SetContext (i4FsMIStdIpContextId) == SNMP_SUCCESS)
        {
            i1RetVal = nmhGetFirstIndexFsipv6PrefTable (pi4FsMIIpv6Protocol);
            UtilRtm6ResetContext ();
            if (i1RetVal == SNMP_SUCCESS)
            {
                return SNMP_SUCCESS;
            }
        }
        i4Value = UtilRtm6GetNextCxtId ((UINT4) i4FsMIStdIpContextId,
                                        (UINT4 *) pi4FsMIStdIpContextId);
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIIpv6PrefTable
 Input       :  The Indices
                FsMIStdIpContextId
                nextFsMIStdIpContextId
                FsMIIpv6Protocol
                nextFsMIIpv6Protocol
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIIpv6PrefTable (INT4 i4FsMIStdIpContextId,
                                  INT4 *pi4NextFsMIStdIpContextId,
                                  INT4 i4FsMIIpv6Protocol,
                                  INT4 *pi4NextFsMIIpv6Protocol)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (UtilRtm6VcmIsVcExist (i4FsMIStdIpContextId) == RTM6_SUCCESS)
    {
        if (UtilRtm6SetContext (i4FsMIStdIpContextId) == SNMP_SUCCESS)
        {
            i1RetVal = nmhGetNextIndexFsipv6PrefTable (i4FsMIIpv6Protocol,
                                                       pi4NextFsMIIpv6Protocol);
            UtilRtm6ResetContext ();
        }

        if (i1RetVal == SNMP_SUCCESS)
        {
            *pi4NextFsMIStdIpContextId = i4FsMIStdIpContextId;
            return SNMP_SUCCESS;
        }
    }
    while ((UtilRtm6GetNextCxtId ((UINT4) i4FsMIStdIpContextId,
                                  (UINT4 *) pi4NextFsMIStdIpContextId))
           == RTM6_SUCCESS)
    {
        if (UtilRtm6SetContext (*pi4NextFsMIStdIpContextId) == SNMP_SUCCESS)
        {
            i1RetVal =
                nmhGetFirstIndexFsipv6PrefTable (pi4NextFsMIIpv6Protocol);

            UtilRtm6ResetContext ();
        }

        if (i1RetVal == SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
        i4FsMIStdIpContextId = *pi4NextFsMIStdIpContextId;
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIIpv6Preference
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIIpv6Protocol

                The Object 
                retValFsMIIpv6Preference
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6Preference (INT4 i4FsMIStdIpContextId, INT4 i4FsMIIpv6Protocol,
                          UINT4 *pu4RetValFsMIIpv6Preference)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (UtilRtm6SetContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }

    i1RetVal =
        nmhGetFsipv6Preference (i4FsMIIpv6Protocol,
                                pu4RetValFsMIIpv6Preference);
    UtilRtm6ResetContext ();
    return i1RetVal;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIIpv6Preference
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIIpv6Protocol

                The Object 
                setValFsMIIpv6Preference
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpv6Preference (INT4 i4FsMIStdIpContextId, INT4 i4FsMIIpv6Protocol,
                          UINT4 u4SetValFsMIIpv6Preference)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (UtilRtm6SetContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }

    i1RetVal =
        nmhSetFsipv6Preference (i4FsMIIpv6Protocol, u4SetValFsMIIpv6Preference);
    UtilRtm6ResetContext ();
    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6Preference
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIIpv6Protocol

                The Object 
                testValFsMIIpv6Preference
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpv6Preference (UINT4 *pu4ErrorCode, INT4 i4FsMIStdIpContextId,
                             INT4 i4FsMIIpv6Protocol,
                             UINT4 u4TestValFsMIIpv6Preference)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (UtilRtm6SetContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1RetVal;
    }

    i1RetVal =
        nmhTestv2Fsipv6Preference (pu4ErrorCode, i4FsMIIpv6Protocol,
                                   u4TestValFsMIIpv6Preference);
    UtilRtm6ResetContext ();
    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIIpv6PrefTable
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIIpv6Protocol
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIIpv6PrefTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIIpv6NDProxyListTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIIpv6NDProxyListTable
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIIpv6NDProxyAddr
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIIpv6NDProxyListTable (INT4 i4FsMIStdIpContextId,
                                                  tSNMP_OCTET_STRING_TYPE *
                                                  pFsMIIpv6NDProxyAddr)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if ((i4FsMIStdIpContextId < VCM_DEFAULT_CONTEXT) ||
        ((UINT4) i4FsMIStdIpContextId >= IP6_MAX_INSTANCES))
    {
        return SNMP_FAILURE;
    }

    if (Ip6VRExists (i4FsMIStdIpContextId) == IP6_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (Ip6SelectContext ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }

    i1RetVal = nmhValidateIndexInstanceFsipv6NDProxyListTable
        (pFsMIIpv6NDProxyAddr);
    Ip6ReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIIpv6NDProxyListTable
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIIpv6NDProxyAddr
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIIpv6NDProxyListTable (INT4 *pi4FsMIStdIpContextId,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pFsMIIpv6NDProxyAddr)
{
    INT4                i4FsMIStdIpContextId;
    INT1                i1RetVal = SNMP_FAILURE;
    INT4                i4Value = IP6_FAILURE;

    i4Value = Ip6UtilGetFirstCxtId ((UINT4 *) pi4FsMIStdIpContextId);

    while (i4Value == IP6_SUCCESS)
    {
        i4FsMIStdIpContextId = *pi4FsMIStdIpContextId;
        if (Ip6SelectContext ((UINT4) i4FsMIStdIpContextId) == SNMP_SUCCESS)
        {
            i1RetVal =
                nmhGetFirstIndexFsipv6NDProxyListTable (pFsMIIpv6NDProxyAddr);
            Ip6ReleaseContext ();
        }
        if (i1RetVal == SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
        i4Value = Ip6UtilGetNextCxtId ((UINT4) i4FsMIStdIpContextId,
                                       (UINT4 *) pi4FsMIStdIpContextId);
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIIpv6NDProxyListTable
 Input       :  The Indices
                FsMIStdIpContextId
                nextFsMIStdIpContextId
                FsMIIpv6NDProxyAddr
                nextFsMIIpv6NDProxyAddr
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIIpv6NDProxyListTable (INT4 i4FsMIStdIpContextId,
                                         INT4 *pi4NextFsMIStdIpContextId,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pFsMIIpv6NDProxyAddr,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pNextFsMIIpv6NDProxyAddr)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (Ip6VRExists (i4FsMIStdIpContextId) == IP6_SUCCESS)
    {
        if (Ip6SelectContext (i4FsMIStdIpContextId) == SNMP_SUCCESS)
        {
            i1RetVal = nmhGetNextIndexFsipv6NDProxyListTable
                (pFsMIIpv6NDProxyAddr, pNextFsMIIpv6NDProxyAddr);
            Ip6ReleaseContext ();
        }
        if (i1RetVal == SNMP_SUCCESS)
        {
            *pi4NextFsMIStdIpContextId = i4FsMIStdIpContextId;
            return SNMP_SUCCESS;
        }
    }

    while ((Ip6UtilGetNextCxtId ((UINT4) i4FsMIStdIpContextId,
                                 (UINT4 *) pi4NextFsMIStdIpContextId)) ==
           IP6_SUCCESS)
    {
        if (Ip6SelectContext (*pi4NextFsMIStdIpContextId) == SNMP_SUCCESS)
        {
            i1RetVal = nmhGetFirstIndexFsipv6NDProxyListTable
                (pNextFsMIIpv6NDProxyAddr);
            Ip6ReleaseContext ();
        }
        if (i1RetVal == SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
        i4FsMIStdIpContextId = *pi4NextFsMIStdIpContextId;
    }
    return SNMP_FAILURE;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIIpv6NDProxyAdminStatus
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIIpv6NDProxyAddr

                The Object 
                retValFsMIIpv6NDProxyAdminStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6NDProxyAdminStatus (INT4 i4FsMIStdIpContextId,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pFsMIIpv6NDProxyAddr,
                                  INT4 *pi4RetValFsMIIpv6NDProxyAdminStatus)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (Ip6SelectContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }

    i1RetVal =
        nmhGetFsipv6NdProxyAdminStatus (pFsMIIpv6NDProxyAddr,
                                        pi4RetValFsMIIpv6NDProxyAdminStatus);
    Ip6ReleaseContext ();
    return i1RetVal;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIIpv6NDProxyAdminStatus
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIIpv6NDProxyAddr

                The Object 
                setValFsMIIpv6NDProxyAdminStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpv6NDProxyAdminStatus (INT4 i4FsMIStdIpContextId,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pFsMIIpv6NDProxyAddr,
                                  INT4 i4SetValFsMIIpv6NDProxyAdminStatus)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (Ip6SelectContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }

    i1RetVal =
        nmhSetFsipv6NdProxyAdminStatus (pFsMIIpv6NDProxyAddr,
                                        i4SetValFsMIIpv6NDProxyAdminStatus);
    Ip6ReleaseContext ();
    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6NDProxyAdminStatus
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIIpv6NDProxyAddr

                The Object 
                testValFsMIIpv6NDProxyAdminStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpv6NDProxyAdminStatus (UINT4 *pu4ErrorCode,
                                     INT4 i4FsMIStdIpContextId,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pFsMIIpv6NDProxyAddr,
                                     INT4 i4TestValFsMIIpv6NDProxyAdminStatus)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (Ip6SelectContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1RetVal;
    }

    i1RetVal =
        nmhTestv2Fsipv6NdProxyAdminStatus (pu4ErrorCode, pFsMIIpv6NDProxyAddr,
                                           i4TestValFsMIIpv6NDProxyAdminStatus);
    Ip6ReleaseContext ();
    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIIpv6NDProxyListTable
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIIpv6NDProxyAddr
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIIpv6NDProxyListTable (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIIpv6PingTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIIpv6PingTable
 Input       :  The Indices
                FsMIIpv6PingIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIIpv6PingTable (INT4 i4FsMIIpv6PingIndex)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhValidateIndexInstanceFsipv6PingTable (i4FsMIIpv6PingIndex);
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIIpv6PingTable
 Input       :  The Indices
                FsMIIpv6PingIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIIpv6PingTable (INT4 *pi4FsMIIpv6PingIndex)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhGetFirstIndexFsipv6PingTable (pi4FsMIIpv6PingIndex);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIIpv6PingTable
 Input       :  The Indices
                FsMIIpv6PingIndex
                nextFsMIIpv6PingIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIIpv6PingTable (INT4 i4FsMIIpv6PingIndex,
                                  INT4 *pi4NextFsMIIpv6PingIndex)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhGetNextIndexFsipv6PingTable (i4FsMIIpv6PingIndex,
                                               pi4NextFsMIIpv6PingIndex);
    return i1RetVal;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIIpv6PingDest
 Input       :  The Indices
                FsMIIpv6PingIndex

                The Object 
                retValFsMIIpv6PingDest
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6PingDest (INT4 i4FsMIIpv6PingIndex,
                        tSNMP_OCTET_STRING_TYPE * pRetValFsMIIpv6PingDest)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhGetFsipv6PingDest (i4FsMIIpv6PingIndex,
                                     pRetValFsMIIpv6PingDest);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6PingIfIndex
 Input       :  The Indices
                FsMIIpv6PingIndex

                The Object 
                retValFsMIIpv6PingIfIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6PingIfIndex (INT4 i4FsMIIpv6PingIndex,
                           INT4 *pi4RetValFsMIIpv6PingIfIndex)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhGetFsipv6PingIfIndex (i4FsMIIpv6PingIndex,
                                        pi4RetValFsMIIpv6PingIfIndex);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6PingContextId
 Input       :  The Indices
                FsMIIpv6PingIndex

                The Object 
                retValFsMIIpv6PingContextId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6PingContextId (INT4 i4FsMIIpv6PingIndex,
                             INT4 *pi4RetValFsMIIpv6PingContextId)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (pPing[i4FsMIIpv6PingIndex]->u1Admin != IP6_PING_INVALID)
    {
        *pi4RetValFsMIIpv6PingContextId =
            (INT4) pPing[i4FsMIIpv6PingIndex]->u4ContextId;
        return (SNMP_SUCCESS);
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6PingAdminStatus
 Input       :  The Indices
                FsMIIpv6PingIndex

                The Object 
                retValFsMIIpv6PingAdminStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6PingAdminStatus (INT4 i4FsMIIpv6PingIndex,
                               INT4 *pi4RetValFsMIIpv6PingAdminStatus)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhGetFsipv6PingAdminStatus (i4FsMIIpv6PingIndex,
                                            pi4RetValFsMIIpv6PingAdminStatus);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6PingInterval
 Input       :  The Indices
                FsMIIpv6PingIndex

                The Object 
                retValFsMIIpv6PingInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6PingInterval (INT4 i4FsMIIpv6PingIndex,
                            INT4 *pi4RetValFsMIIpv6PingInterval)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhGetFsipv6PingInterval (i4FsMIIpv6PingIndex,
                                         pi4RetValFsMIIpv6PingInterval);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6PingRcvTimeout
 Input       :  The Indices
                FsMIIpv6PingIndex

                The Object 
                retValFsMIIpv6PingRcvTimeout
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6PingRcvTimeout (INT4 i4FsMIIpv6PingIndex,
                              INT4 *pi4RetValFsMIIpv6PingRcvTimeout)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhGetFsipv6PingRcvTimeout (i4FsMIIpv6PingIndex,
                                           pi4RetValFsMIIpv6PingRcvTimeout);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6PingTries
 Input       :  The Indices
                FsMIIpv6PingIndex

                The Object 
                retValFsMIIpv6PingTries
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6PingTries (INT4 i4FsMIIpv6PingIndex,
                         INT4 *pi4RetValFsMIIpv6PingTries)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhGetFsipv6PingTries (i4FsMIIpv6PingIndex,
                                      pi4RetValFsMIIpv6PingTries);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6PingSize
 Input       :  The Indices
                FsMIIpv6PingIndex

                The Object 
                retValFsMIIpv6PingSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6PingSize (INT4 i4FsMIIpv6PingIndex,
                        INT4 *pi4RetValFsMIIpv6PingSize)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhGetFsipv6PingSize (i4FsMIIpv6PingIndex,
                                     pi4RetValFsMIIpv6PingSize);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6PingSentCount
 Input       :  The Indices
                FsMIIpv6PingIndex

                The Object 
                retValFsMIIpv6PingSentCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6PingSentCount (INT4 i4FsMIIpv6PingIndex,
                             INT4 *pi4RetValFsMIIpv6PingSentCount)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhGetFsipv6PingSentCount (i4FsMIIpv6PingIndex,
                                          pi4RetValFsMIIpv6PingSentCount);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6PingAverageTime
 Input       :  The Indices
                FsMIIpv6PingIndex

                The Object 
                retValFsMIIpv6PingAverageTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6PingAverageTime (INT4 i4FsMIIpv6PingIndex,
                               INT4 *pi4RetValFsMIIpv6PingAverageTime)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhGetFsipv6PingAverageTime (i4FsMIIpv6PingIndex,
                                            pi4RetValFsMIIpv6PingAverageTime);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6PingMaxTime
 Input       :  The Indices
                FsMIIpv6PingIndex

                The Object 
                retValFsMIIpv6PingMaxTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6PingMaxTime (INT4 i4FsMIIpv6PingIndex,
                           INT4 *pi4RetValFsMIIpv6PingMaxTime)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhGetFsipv6PingMaxTime (i4FsMIIpv6PingIndex,
                                        pi4RetValFsMIIpv6PingMaxTime);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6PingMinTime
 Input       :  The Indices
                FsMIIpv6PingIndex

                The Object 
                retValFsMIIpv6PingMinTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6PingMinTime (INT4 i4FsMIIpv6PingIndex,
                           INT4 *pi4RetValFsMIIpv6PingMinTime)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhGetFsipv6PingMinTime (i4FsMIIpv6PingIndex,
                                        pi4RetValFsMIIpv6PingMinTime);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6PingOperStatus
 Input       :  The Indices
                FsMIIpv6PingIndex

                The Object 
                retValFsMIIpv6PingOperStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6PingOperStatus (INT4 i4FsMIIpv6PingIndex,
                              INT4 *pi4RetValFsMIIpv6PingOperStatus)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhGetFsipv6PingOperStatus (i4FsMIIpv6PingIndex,
                                           pi4RetValFsMIIpv6PingOperStatus);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6PingSuccesses
 Input       :  The Indices
                FsMIIpv6PingIndex

                The Object 
                retValFsMIIpv6PingSuccesses
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6PingSuccesses (INT4 i4FsMIIpv6PingIndex,
                             UINT4 *pu4RetValFsMIIpv6PingSuccesses)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhGetFsipv6PingSuccesses (i4FsMIIpv6PingIndex,
                                          pu4RetValFsMIIpv6PingSuccesses);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6PingPercentageLoss
 Input       :  The Indices
                FsMIIpv6PingIndex

                The Object 
                retValFsMIIpv6PingPercentageLoss
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6PingPercentageLoss (INT4 i4FsMIIpv6PingIndex,
                                  INT4 *pi4RetValFsMIIpv6PingPercentageLoss)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhGetFsipv6PingPercentageLoss (i4FsMIIpv6PingIndex,
                                               pi4RetValFsMIIpv6PingPercentageLoss);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6PingData
 Input       :  The Indices
                FsMIIpv6PingIndex

                The Object 
                retValFsMIIpv6PingData
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6PingData (INT4 i4FsMIIpv6PingIndex,
                        tSNMP_OCTET_STRING_TYPE * pRetValFsMIIpv6PingData)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhGetFsipv6PingData (i4FsMIIpv6PingIndex,
                                     pRetValFsMIIpv6PingData);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6PingSrcAddr
 Input       :  The Indices
                FsMIIpv6PingIndex

                The Object 
                retValFsMIIpv6PingSrcAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6PingSrcAddr (INT4 i4FsMIIpv6PingIndex,
                           tSNMP_OCTET_STRING_TYPE * pRetValFsMIIpv6PingSrcAddr)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhGetFsipv6PingSrcAddr (i4FsMIIpv6PingIndex,
                                        pRetValFsMIIpv6PingSrcAddr);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6PingZoneId
 Input       :  The Indices
                FsMIIpv6PingIndex

                The Object 
                retValFsMIIpv6PingZoneId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6PingZoneId (INT4 i4FsMIIpv6PingIndex,
                          tSNMP_OCTET_STRING_TYPE * pRetValFsMIIpv6PingZoneId)
{
    UNUSED_PARAM (i4FsMIIpv6PingIndex);
    UNUSED_PARAM (pRetValFsMIIpv6PingZoneId);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6PingDestAddrType
 Input       :  The Indices
                FsMIIpv6PingIndex

                The Object 
                retValFsMIIpv6PingDestAddrType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6PingDestAddrType (INT4 i4FsMIIpv6PingIndex,
                                INT4 *pi4RetValFsMIIpv6PingDestAddrType)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhGetFsipv6PingDestAddrType (i4FsMIIpv6PingIndex,
                                             pi4RetValFsMIIpv6PingDestAddrType);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6PingHostName
 Input       :  The Indices
                FsMIIpv6PingIndex                                                                      
                The Object
                retValFsMIIpv6PingHostName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
nmhGetFsMIIpv6PingHostName(INT4 i4FsMIIpv6PingIndex , 
                           tSNMP_OCTET_STRING_TYPE * pRetValFsMIIpv6PingHostName)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhGetFsipv6PingHostName (i4FsMIIpv6PingIndex, 
                                        pRetValFsMIIpv6PingHostName);
    
    return i1RetVal;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIIpv6PingDest
 Input       :  The Indices
                FsMIIpv6PingIndex

                The Object 
                setValFsMIIpv6PingDest
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpv6PingDest (INT4 i4FsMIIpv6PingIndex,
                        tSNMP_OCTET_STRING_TYPE * pSetValFsMIIpv6PingDest)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhSetFsipv6PingDest (i4FsMIIpv6PingIndex,
                                     pSetValFsMIIpv6PingDest);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIIpv6PingIfIndex
 Input       :  The Indices
                FsMIIpv6PingIndex

                The Object 
                setValFsMIIpv6PingIfIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpv6PingIfIndex (INT4 i4FsMIIpv6PingIndex,
                           INT4 i4SetValFsMIIpv6PingIfIndex)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhSetFsipv6PingIfIndex (i4FsMIIpv6PingIndex,
                                        i4SetValFsMIIpv6PingIfIndex);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIIpv6PingContextId
 Input       :  The Indices
                FsMIIpv6PingIndex

                The Object 
                setValFsMIIpv6PingContextId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpv6PingContextId (INT4 i4FsMIIpv6PingIndex,
                             INT4 i4SetValFsMIIpv6PingContextId)
{
    if (pPing[i4FsMIIpv6PingIndex]->u1Admin != IP6_PING_INVALID)
    {
        pPing[i4FsMIIpv6PingIndex]->u4ContextId =
            (UINT4) i4SetValFsMIIpv6PingContextId;
        return (SNMP_SUCCESS);
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetFsMIIpv6PingAdminStatus
 Input       :  The Indices
                FsMIIpv6PingIndex

                The Object 
                setValFsMIIpv6PingAdminStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpv6PingAdminStatus (INT4 i4FsMIIpv6PingIndex,
                               INT4 i4SetValFsMIIpv6PingAdminStatus)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhSetFsipv6PingAdminStatus (i4FsMIIpv6PingIndex,
                                            i4SetValFsMIIpv6PingAdminStatus);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIIpv6PingInterval
 Input       :  The Indices
                FsMIIpv6PingIndex

                The Object 
                setValFsMIIpv6PingInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpv6PingInterval (INT4 i4FsMIIpv6PingIndex,
                            INT4 i4SetValFsMIIpv6PingInterval)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhSetFsipv6PingInterval (i4FsMIIpv6PingIndex,
                                         i4SetValFsMIIpv6PingInterval);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIIpv6PingRcvTimeout
 Input       :  The Indices
                FsMIIpv6PingIndex

                The Object 
                setValFsMIIpv6PingRcvTimeout
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpv6PingRcvTimeout (INT4 i4FsMIIpv6PingIndex,
                              INT4 i4SetValFsMIIpv6PingRcvTimeout)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhSetFsipv6PingRcvTimeout (i4FsMIIpv6PingIndex,
                                           i4SetValFsMIIpv6PingRcvTimeout);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIIpv6PingTries
 Input       :  The Indices
                FsMIIpv6PingIndex

                The Object 
                setValFsMIIpv6PingTries
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpv6PingTries (INT4 i4FsMIIpv6PingIndex,
                         INT4 i4SetValFsMIIpv6PingTries)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhSetFsipv6PingTries (i4FsMIIpv6PingIndex,
                                      i4SetValFsMIIpv6PingTries);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIIpv6PingSize
 Input       :  The Indices
                FsMIIpv6PingIndex

                The Object 
                setValFsMIIpv6PingSize
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpv6PingSize (INT4 i4FsMIIpv6PingIndex, INT4 i4SetValFsMIIpv6PingSize)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhSetFsipv6PingSize (i4FsMIIpv6PingIndex,
                                     i4SetValFsMIIpv6PingSize);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIIpv6PingData
 Input       :  The Indices
                FsMIIpv6PingIndex

                The Object 
                setValFsMIIpv6PingData
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpv6PingData (INT4 i4FsMIIpv6PingIndex,
                        tSNMP_OCTET_STRING_TYPE * pSetValFsMIIpv6PingData)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhSetFsipv6PingData (i4FsMIIpv6PingIndex,
                                     pSetValFsMIIpv6PingData);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIIpv6PingSrcAddr
 Input       :  The Indices
                FsMIIpv6PingIndex

                The Object 
                setValFsMIIpv6PingSrcAddr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpv6PingSrcAddr (INT4 i4FsMIIpv6PingIndex,
                           tSNMP_OCTET_STRING_TYPE * pSetValFsMIIpv6PingSrcAddr)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhSetFsipv6PingSrcAddr (i4FsMIIpv6PingIndex,
                                        pSetValFsMIIpv6PingSrcAddr);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIIpv6PingZoneId
 Input       :  The Indices
                FsMIIpv6PingIndex

                The Object 
                setValFsMIIpv6PingZoneId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpv6PingZoneId (INT4 i4FsMIIpv6PingIndex,
                          tSNMP_OCTET_STRING_TYPE * pSetValFsMIIpv6PingZoneId)
{
    UNUSED_PARAM (i4FsMIIpv6PingIndex);
    UNUSED_PARAM (pSetValFsMIIpv6PingZoneId);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsMIIpv6PingDestAddrType
 Input       :  The Indices
                FsMIIpv6PingIndex

                The Object 
                setValFsMIIpv6PingDestAddrType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpv6PingDestAddrType (INT4 i4FsMIIpv6PingIndex,
                                INT4 i4SetValFsMIIpv6PingDestAddrType)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhSetFsipv6PingDestAddrType (i4FsMIIpv6PingIndex,
                                             i4SetValFsMIIpv6PingDestAddrType);
    return i1RetVal;
}

/****************************************************************************                           Function    :  nmhSetFsMIIpv6PingHostName
 Input       :  The Indices
                FsMIIpv6PingIndex

                The Object
                setValFsMIIpv6PingHostName
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.                                                             Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
nmhSetFsMIIpv6PingHostName(INT4 i4FsMIIpv6PingIndex , 
                           tSNMP_OCTET_STRING_TYPE *pSetValFsMIIpv6PingHostName)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhSetFsipv6PingHostName (i4FsMIIpv6PingIndex, 
                                         pSetValFsMIIpv6PingHostName);

    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6PingDest
 Input       :  The Indices
                FsMIIpv6PingIndex

                The Object 
                testValFsMIIpv6PingDest
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpv6PingDest (UINT4 *pu4ErrorCode, INT4 i4FsMIIpv6PingIndex,
                           tSNMP_OCTET_STRING_TYPE * pTestValFsMIIpv6PingDest)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhTestv2Fsipv6PingDest (pu4ErrorCode,
                                        i4FsMIIpv6PingIndex,
                                        pTestValFsMIIpv6PingDest);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6PingIfIndex
 Input       :  The Indices
                FsMIIpv6PingIndex

                The Object 
                testValFsMIIpv6PingIfIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpv6PingIfIndex (UINT4 *pu4ErrorCode, INT4 i4FsMIIpv6PingIndex,
                              INT4 i4TestValFsMIIpv6PingIfIndex)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhTestv2Fsipv6PingIfIndex (pu4ErrorCode,
                                           i4FsMIIpv6PingIndex,
                                           i4TestValFsMIIpv6PingIfIndex);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6PingContextId
 Input       :  The Indices
                FsMIIpv6PingIndex

                The Object 
                testValFsMIIpv6PingContextId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpv6PingContextId (UINT4 *pu4ErrorCode, INT4 i4FsMIIpv6PingIndex,
                                INT4 i4TestValFsMIIpv6PingContextId)
{
    if ((i4FsMIIpv6PingIndex >= PING6_MAX_DST) || (i4FsMIIpv6PingIndex < 0))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_PING_INDEX);
        return SNMP_FAILURE;
    }

    if (Ip6VRExists ((UINT4) i4TestValFsMIIpv6PingContextId) == IP6_SUCCESS)
    {
        return (SNMP_SUCCESS);
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    CLI_SET_ERR (CLI_IP6_INVALID_CONTEXT);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6PingAdminStatus
 Input       :  The Indices
                FsMIIpv6PingIndex

                The Object 
                testValFsMIIpv6PingAdminStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpv6PingAdminStatus (UINT4 *pu4ErrorCode, INT4 i4FsMIIpv6PingIndex,
                                  INT4 i4TestValFsMIIpv6PingAdminStatus)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhTestv2Fsipv6PingAdminStatus (pu4ErrorCode,
                                               i4FsMIIpv6PingIndex,
                                               i4TestValFsMIIpv6PingAdminStatus);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6PingInterval
 Input       :  The Indices
                FsMIIpv6PingIndex

                The Object 
                testValFsMIIpv6PingInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpv6PingInterval (UINT4 *pu4ErrorCode, INT4 i4FsMIIpv6PingIndex,
                               INT4 i4TestValFsMIIpv6PingInterval)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhTestv2Fsipv6PingInterval (pu4ErrorCode,
                                            i4FsMIIpv6PingIndex,
                                            i4TestValFsMIIpv6PingInterval);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6PingRcvTimeout
 Input       :  The Indices
                FsMIIpv6PingIndex

                The Object 
                testValFsMIIpv6PingRcvTimeout
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpv6PingRcvTimeout (UINT4 *pu4ErrorCode, INT4 i4FsMIIpv6PingIndex,
                                 INT4 i4TestValFsMIIpv6PingRcvTimeout)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhTestv2Fsipv6PingRcvTimeout (pu4ErrorCode,
                                              i4FsMIIpv6PingIndex,
                                              i4TestValFsMIIpv6PingRcvTimeout);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6PingTries
 Input       :  The Indices
                FsMIIpv6PingIndex

                The Object 
                testValFsMIIpv6PingTries
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpv6PingTries (UINT4 *pu4ErrorCode, INT4 i4FsMIIpv6PingIndex,
                            INT4 i4TestValFsMIIpv6PingTries)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhTestv2Fsipv6PingTries (pu4ErrorCode, i4FsMIIpv6PingIndex,
                                         i4TestValFsMIIpv6PingTries);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6PingSize
 Input       :  The Indices
                FsMIIpv6PingIndex

                The Object 
                testValFsMIIpv6PingSize
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpv6PingSize (UINT4 *pu4ErrorCode, INT4 i4FsMIIpv6PingIndex,
                           INT4 i4TestValFsMIIpv6PingSize)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhTestv2Fsipv6PingSize (pu4ErrorCode,
                                        i4FsMIIpv6PingIndex,
                                        i4TestValFsMIIpv6PingSize);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6PingData
 Input       :  The Indices
                FsMIIpv6PingIndex

                The Object 
                testValFsMIIpv6PingData
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpv6PingData (UINT4 *pu4ErrorCode, INT4 i4FsMIIpv6PingIndex,
                           tSNMP_OCTET_STRING_TYPE * pTestValFsMIIpv6PingData)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhTestv2Fsipv6PingData (pu4ErrorCode,
                                        i4FsMIIpv6PingIndex,
                                        pTestValFsMIIpv6PingData);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6PingSrcAddr
 Input       :  The Indices
                FsMIIpv6PingIndex

                The Object 
                testValFsMIIpv6PingSrcAddr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpv6PingSrcAddr (UINT4 *pu4ErrorCode, INT4 i4FsMIIpv6PingIndex,
                              tSNMP_OCTET_STRING_TYPE *
                              pTestValFsMIIpv6PingSrcAddr)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhTestv2Fsipv6PingSrcAddr (pu4ErrorCode,
                                           i4FsMIIpv6PingIndex,
                                           pTestValFsMIIpv6PingSrcAddr);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6PingZoneId
 Input       :  The Indices
                FsMIIpv6PingIndex

                The Object 
                testValFsMIIpv6PingZoneId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpv6PingZoneId (UINT4 *pu4ErrorCode, INT4 i4FsMIIpv6PingIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pTestValFsMIIpv6PingZoneId)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4FsMIIpv6PingIndex);
    UNUSED_PARAM (pTestValFsMIIpv6PingZoneId);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6PingDestAddrType
 Input       :  The Indices
                FsMIIpv6PingIndex

                The Object 
                testValFsMIIpv6PingDestAddrType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpv6PingDestAddrType (UINT4 *pu4ErrorCode,
                                   INT4 i4FsMIIpv6PingIndex,
                                   INT4 i4TestValFsMIIpv6PingDestAddrType)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhTestv2Fsipv6PingDestAddrType (pu4ErrorCode,
                                                i4FsMIIpv6PingIndex,
                                                i4TestValFsMIIpv6PingDestAddrType);
    return i1RetVal;
}

/****************************************************************************                           Function    :  nmhTestv2FsMIIpv6PingHostName
 Input       :  The Indices
                FsMIIpv6PingIndex

                The Object
                testValFsMIIpv6PingHostName
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.                                                        Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)                                                 SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
nmhTestv2FsMIIpv6PingHostName (UINT4 *pu4ErrorCode , 
                               INT4 i4FsMIIpv6PingIndex , 
                               tSNMP_OCTET_STRING_TYPE 
                               *pTestValFsMIIpv6PingHostName)
{
    INT1                i1RetVal = SNMP_FAILURE;
   
    i1RetVal = nmhTestv2Fsipv6PingHostName (pu4ErrorCode, i4FsMIIpv6PingIndex,
                                            pTestValFsMIIpv6PingHostName);

    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIIpv6PingTable
 Input       :  The Indices
                FsMIIpv6PingIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIIpv6PingTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIIpv6GlobalDebug
 Input       :  The Indices

                The Object 
                retValFsMIIpv6GlobalDebug
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6GlobalDebug (UINT4 *pu4RetValFsMIIpv6GlobalDebug)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhGetFsipv6GlobalDebug (pu4RetValFsMIIpv6GlobalDebug);
    return i1RetVal;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIIpv6GlobalDebug
 Input       :  The Indices

                The Object 
                setValFsMIIpv6GlobalDebug
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpv6GlobalDebug (UINT4 u4SetValFsMIIpv6GlobalDebug)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhSetFsipv6GlobalDebug (u4SetValFsMIIpv6GlobalDebug);
    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6GlobalDebug
 Input       :  The Indices

                The Object 
                testValFsMIIpv6GlobalDebug
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpv6GlobalDebug (UINT4 *pu4ErrorCode,
                              UINT4 u4TestValFsMIIpv6GlobalDebug)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal =
        nmhTestv2Fsipv6GlobalDebug (pu4ErrorCode, u4TestValFsMIIpv6GlobalDebug);
    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIIpv6GlobalDebug
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIIpv6GlobalDebug (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIIpv6AddrSelPolicyTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIIpv6AddrSelPolicyTable
 Input       :  The Indices
                FsMIIpv6AddrSelPolicyPrefix
                FsMIIpv6AddrSelPolicyPrefixLen
                FsMIIpv6AddrSelPolicyIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1                nmhValidateIndexInstanceFsMIIpv6AddrSelPolicyTable
    (tSNMP_OCTET_STRING_TYPE
     * pFsMIIpv6AddrSelPolicyPrefix,
     INT4 i4FsMIIpv6AddrSelPolicyPrefixLen, INT4 i4FsMIIpv6AddrSelPolicyIfIndex)
{
    INT1                i1RetVal = SNMP_FAILURE;
    i1RetVal =
        nmhValidateIndexInstanceFsipv6AddrSelPolicyTable
        (pFsMIIpv6AddrSelPolicyPrefix, i4FsMIIpv6AddrSelPolicyPrefixLen,
         i4FsMIIpv6AddrSelPolicyIfIndex);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIIpv6AddrSelPolicyTable
 Input       :  The Indices
                FsMIIpv6AddrSelPolicyPrefix
                FsMIIpv6AddrSelPolicyPrefixLen
                FsMIIpv6AddrSelPolicyIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIIpv6AddrSelPolicyTable (tSNMP_OCTET_STRING_TYPE *
                                            pFsMIIpv6AddrSelPolicyPrefix,
                                            INT4
                                            *pi4FsMIIpv6AddrSelPolicyPrefixLen,
                                            INT4
                                            *pi4FsMIIpv6AddrSelPolicyIfIndex)
{
    INT1                i1RetVal = SNMP_FAILURE;
    i1RetVal =
        nmhGetFirstIndexFsipv6AddrSelPolicyTable (pFsMIIpv6AddrSelPolicyPrefix,
                                                  pi4FsMIIpv6AddrSelPolicyPrefixLen,
                                                  pi4FsMIIpv6AddrSelPolicyIfIndex);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIIpv6AddrSelPolicyTable
 Input       :  The Indices
                FsMIIpv6AddrSelPolicyPrefix
                nextFsMIIpv6AddrSelPolicyPrefix
                FsMIIpv6AddrSelPolicyPrefixLen
                nextFsMIIpv6AddrSelPolicyPrefixLen
                FsMIIpv6AddrSelPolicyIfIndex
                nextFsMIIpv6AddrSelPolicyIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIIpv6AddrSelPolicyTable (tSNMP_OCTET_STRING_TYPE
                                           * pFsMIIpv6AddrSelPolicyPrefix,
                                           tSNMP_OCTET_STRING_TYPE *
                                           pNextFsMIIpv6AddrSelPolicyPrefix,
                                           INT4
                                           i4FsMIIpv6AddrSelPolicyPrefixLen,
                                           INT4
                                           *pi4NextFsMIIpv6AddrSelPolicyPrefixLen,
                                           INT4 i4FsMIIpv6AddrSelPolicyIfIndex,
                                           INT4
                                           *pi4NextFsMIIpv6AddrSelPolicyIfIndex)
{
    INT1                i1RetVal = SNMP_FAILURE;
    i1RetVal =
        nmhGetNextIndexFsipv6AddrSelPolicyTable (pFsMIIpv6AddrSelPolicyPrefix,
                                                 pNextFsMIIpv6AddrSelPolicyPrefix,
                                                 i4FsMIIpv6AddrSelPolicyPrefixLen,
                                                 pi4NextFsMIIpv6AddrSelPolicyPrefixLen,
                                                 i4FsMIIpv6AddrSelPolicyIfIndex,
                                                 pi4NextFsMIIpv6AddrSelPolicyIfIndex);
    return i1RetVal;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIIpv6AddrSelPolicyScope
 Input       :  The Indices
                FsMIIpv6AddrSelPolicyPrefix
                FsMIIpv6AddrSelPolicyPrefixLen
                FsMIIpv6AddrSelPolicyIfIndex

                The Object 
                retValFsMIIpv6AddrSelPolicyScope
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6AddrSelPolicyScope (tSNMP_OCTET_STRING_TYPE
                                  * pFsMIIpv6AddrSelPolicyPrefix,
                                  INT4 i4FsMIIpv6AddrSelPolicyPrefixLen,
                                  INT4 i4FsMIIpv6AddrSelPolicyIfIndex,
                                  INT4 *pi4RetValFsMIIpv6AddrSelPolicyScope)
{
    INT1                i1RetVal = SNMP_FAILURE;
    i1RetVal = nmhGetFsipv6AddrSelPolicyScope (pFsMIIpv6AddrSelPolicyPrefix,
                                               i4FsMIIpv6AddrSelPolicyPrefixLen,
                                               i4FsMIIpv6AddrSelPolicyIfIndex,
                                               pi4RetValFsMIIpv6AddrSelPolicyScope);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6AddrSelPolicyPrecedence
 Input       :  The Indices
                FsMIIpv6AddrSelPolicyPrefix
                FsMIIpv6AddrSelPolicyPrefixLen
                FsMIIpv6AddrSelPolicyIfIndex

                The Object 
                retValFsMIIpv6AddrSelPolicyPrecedence
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6AddrSelPolicyPrecedence (tSNMP_OCTET_STRING_TYPE *
                                       pFsMIIpv6AddrSelPolicyPrefix,
                                       INT4 i4FsMIIpv6AddrSelPolicyPrefixLen,
                                       INT4 i4FsMIIpv6AddrSelPolicyIfIndex,
                                       INT4
                                       *pi4RetValFsMIIpv6AddrSelPolicyPrecedence)
{
    INT1                i1RetVal = SNMP_FAILURE;
    i1RetVal =
        nmhGetFsipv6AddrSelPolicyPrecedence (pFsMIIpv6AddrSelPolicyPrefix,
                                             i4FsMIIpv6AddrSelPolicyPrefixLen,
                                             i4FsMIIpv6AddrSelPolicyIfIndex,
                                             pi4RetValFsMIIpv6AddrSelPolicyPrecedence);
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6AddrSelPolicyLabel
 Input       :  The Indices
                FsMIIpv6AddrSelPolicyPrefix
                FsMIIpv6AddrSelPolicyPrefixLen
                FsMIIpv6AddrSelPolicyIfIndex

                The Object 
                retValFsMIIpv6AddrSelPolicyLabel
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6AddrSelPolicyLabel (tSNMP_OCTET_STRING_TYPE *
                                  pFsMIIpv6AddrSelPolicyPrefix,
                                  INT4 i4FsMIIpv6AddrSelPolicyPrefixLen,
                                  INT4 i4FsMIIpv6AddrSelPolicyIfIndex,
                                  INT4 *pi4RetValFsMIIpv6AddrSelPolicyLabel)
{
    INT1                i1RetVal = SNMP_FAILURE;
    i1RetVal = nmhGetFsipv6AddrSelPolicyLabel (pFsMIIpv6AddrSelPolicyPrefix,
                                               i4FsMIIpv6AddrSelPolicyPrefixLen,
                                               i4FsMIIpv6AddrSelPolicyIfIndex,
                                               pi4RetValFsMIIpv6AddrSelPolicyLabel);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6AddrSelPolicyAddrType
 Input       :  The Indices
                FsMIIpv6AddrSelPolicyPrefix
                FsMIIpv6AddrSelPolicyPrefixLen
                FsMIIpv6AddrSelPolicyIfIndex

                The Object 
                retValFsMIIpv6AddrSelPolicyAddrType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6AddrSelPolicyAddrType (tSNMP_OCTET_STRING_TYPE *
                                     pFsMIIpv6AddrSelPolicyPrefix,
                                     INT4 i4FsMIIpv6AddrSelPolicyPrefixLen,
                                     INT4 i4FsMIIpv6AddrSelPolicyIfIndex,
                                     INT4
                                     *pi4RetValFsMIIpv6AddrSelPolicyAddrType)
{
    INT1                i1RetVal = SNMP_FAILURE;
    i1RetVal = nmhGetFsipv6AddrSelPolicyAddrType (pFsMIIpv6AddrSelPolicyPrefix,
                                                  i4FsMIIpv6AddrSelPolicyPrefixLen,
                                                  i4FsMIIpv6AddrSelPolicyIfIndex,
                                                  pi4RetValFsMIIpv6AddrSelPolicyAddrType);
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6AddrSelPolicyIsPublicAddr
 Input       :  The Indices
                FsMIIpv6AddrSelPolicyPrefix
                FsMIIpv6AddrSelPolicyPrefixLen
                FsMIIpv6AddrSelPolicyIfIndex

                The Object 
                retValFsMIIpv6AddrSelPolicyIsPublicAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6AddrSelPolicyIsPublicAddr (tSNMP_OCTET_STRING_TYPE *
                                         pFsMIIpv6AddrSelPolicyPrefix,
                                         INT4 i4FsMIIpv6AddrSelPolicyPrefixLen,
                                         INT4 i4FsMIIpv6AddrSelPolicyIfIndex,
                                         INT4
                                         *pi4RetValFsMIIpv6AddrSelPolicyIsPublicAddr)
{
    INT1                i1RetVal = SNMP_FAILURE;
    i1RetVal =
        nmhGetFsipv6AddrSelPolicyIsPublicAddr (pFsMIIpv6AddrSelPolicyPrefix,
                                               i4FsMIIpv6AddrSelPolicyPrefixLen,
                                               i4FsMIIpv6AddrSelPolicyIfIndex,
                                               pi4RetValFsMIIpv6AddrSelPolicyIsPublicAddr);
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6AddrSelPolicyIsSelfAddr
 Input       :  The Indices
                FsMIIpv6AddrSelPolicyPrefix
                FsMIIpv6AddrSelPolicyPrefixLen
                FsMIIpv6AddrSelPolicyIfIndex

                The Object 
                retValFsMIIpv6AddrSelPolicyIsSelfAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6AddrSelPolicyIsSelfAddr (tSNMP_OCTET_STRING_TYPE *
                                       pFsMIIpv6AddrSelPolicyPrefix,
                                       INT4 i4FsMIIpv6AddrSelPolicyPrefixLen,
                                       INT4 i4FsMIIpv6AddrSelPolicyIfIndex,
                                       INT4
                                       *pi4RetValFsMIIpv6AddrSelPolicyIsSelfAddr)
{
    INT1                i1RetVal = SNMP_FAILURE;
    i1RetVal =
        nmhGetFsipv6AddrSelPolicyIsSelfAddr (pFsMIIpv6AddrSelPolicyPrefix,
                                             i4FsMIIpv6AddrSelPolicyPrefixLen,
                                             i4FsMIIpv6AddrSelPolicyIfIndex,
                                             pi4RetValFsMIIpv6AddrSelPolicyIsSelfAddr);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6AddrSelPolicyReachabilityStatus
 Input       :  The Indices
                FsMIIpv6AddrSelPolicyPrefix
                FsMIIpv6AddrSelPolicyPrefixLen
                FsMIIpv6AddrSelPolicyIfIndex

                The Object 
                retValFsMIIpv6AddrSelPolicyReachabilityStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6AddrSelPolicyReachabilityStatus (tSNMP_OCTET_STRING_TYPE *
                                               pFsMIIpv6AddrSelPolicyPrefix,
                                               INT4
                                               i4FsMIIpv6AddrSelPolicyPrefixLen,
                                               INT4
                                               i4FsMIIpv6AddrSelPolicyIfIndex,
                                               INT4
                                               *pi4RetValFsMIIpv6AddrSelPolicyReachabilityStatus)
{
    INT1                i1RetVal = SNMP_FAILURE;
    i1RetVal =
        nmhGetFsipv6AddrSelPolicyReachabilityStatus
        (pFsMIIpv6AddrSelPolicyPrefix, i4FsMIIpv6AddrSelPolicyPrefixLen,
         i4FsMIIpv6AddrSelPolicyIfIndex,
         pi4RetValFsMIIpv6AddrSelPolicyReachabilityStatus);
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6AddrSelPolicyConfigStatus
 Input       :  The Indices
                FsMIIpv6AddrSelPolicyPrefix
                FsMIIpv6AddrSelPolicyPrefixLen
                FsMIIpv6AddrSelPolicyIfIndex

                The Object 
                retValFsMIIpv6AddrSelPolicyConfigStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6AddrSelPolicyConfigStatus (tSNMP_OCTET_STRING_TYPE *
                                         pFsMIIpv6AddrSelPolicyPrefix,
                                         INT4 i4FsMIIpv6AddrSelPolicyPrefixLen,
                                         INT4 i4FsMIIpv6AddrSelPolicyIfIndex,
                                         INT4
                                         *pi4RetValFsMIIpv6AddrSelPolicyConfigStatus)
{
    INT1                i1RetVal = SNMP_FAILURE;
    i1RetVal =
        nmhGetFsipv6AddrSelPolicyConfigStatus (pFsMIIpv6AddrSelPolicyPrefix,
                                               i4FsMIIpv6AddrSelPolicyPrefixLen,
                                               i4FsMIIpv6AddrSelPolicyIfIndex,
                                               pi4RetValFsMIIpv6AddrSelPolicyConfigStatus);
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6AddrSelPolicyRowStatus
 Input       :  The Indices
                FsMIIpv6AddrSelPolicyPrefix
                FsMIIpv6AddrSelPolicyPrefixLen
                FsMIIpv6AddrSelPolicyIfIndex

                The Object 
                retValFsMIIpv6AddrSelPolicyRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6AddrSelPolicyRowStatus (tSNMP_OCTET_STRING_TYPE *
                                      pFsMIIpv6AddrSelPolicyPrefix,
                                      INT4 i4FsMIIpv6AddrSelPolicyPrefixLen,
                                      INT4 i4FsMIIpv6AddrSelPolicyIfIndex,
                                      INT4
                                      *pi4RetValFsMIIpv6AddrSelPolicyRowStatus)
{
    INT1                i1RetVal = SNMP_FAILURE;
    i1RetVal = nmhGetFsipv6AddrSelPolicyRowStatus (pFsMIIpv6AddrSelPolicyPrefix,
                                                   i4FsMIIpv6AddrSelPolicyPrefixLen,
                                                   i4FsMIIpv6AddrSelPolicyIfIndex,
                                                   pi4RetValFsMIIpv6AddrSelPolicyRowStatus);
    return i1RetVal;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIIpv6AddrSelPolicyPrecedence
 Input       :  The Indices
                FsMIIpv6AddrSelPolicyPrefix
                FsMIIpv6AddrSelPolicyPrefixLen
                FsMIIpv6AddrSelPolicyIfIndex

                The Object 
                setValFsMIIpv6AddrSelPolicyPrecedence
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpv6AddrSelPolicyPrecedence (tSNMP_OCTET_STRING_TYPE *
                                       pFsMIIpv6AddrSelPolicyPrefix,
                                       INT4 i4FsMIIpv6AddrSelPolicyPrefixLen,
                                       INT4 i4FsMIIpv6AddrSelPolicyIfIndex,
                                       INT4
                                       i4SetValFsMIIpv6AddrSelPolicyPrecedence)
{

    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal =
        nmhSetFsipv6AddrSelPolicyPrecedence (pFsMIIpv6AddrSelPolicyPrefix,
                                             i4FsMIIpv6AddrSelPolicyPrefixLen,
                                             i4FsMIIpv6AddrSelPolicyIfIndex,
                                             i4SetValFsMIIpv6AddrSelPolicyPrecedence);

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIIpv6AddrSelPolicyLabel
 Input       :  The Indices
                FsMIIpv6AddrSelPolicyPrefix
                FsMIIpv6AddrSelPolicyPrefixLen
                FsMIIpv6AddrSelPolicyIfIndex

                The Object 
                setValFsMIIpv6AddrSelPolicyLabel
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpv6AddrSelPolicyLabel (tSNMP_OCTET_STRING_TYPE *
                                  pFsMIIpv6AddrSelPolicyPrefix,
                                  INT4 i4FsMIIpv6AddrSelPolicyPrefixLen,
                                  INT4 i4FsMIIpv6AddrSelPolicyIfIndex,
                                  INT4 i4SetValFsMIIpv6AddrSelPolicyLabel)
{

    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhSetFsipv6AddrSelPolicyLabel (pFsMIIpv6AddrSelPolicyPrefix,
                                               i4FsMIIpv6AddrSelPolicyPrefixLen,
                                               i4FsMIIpv6AddrSelPolicyIfIndex,
                                               i4SetValFsMIIpv6AddrSelPolicyLabel);

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIIpv6AddrSelPolicyAddrType
 Input       :  The Indices
                FsMIIpv6AddrSelPolicyPrefix
                FsMIIpv6AddrSelPolicyPrefixLen
                FsMIIpv6AddrSelPolicyIfIndex

                The Object 
                setValFsMIIpv6AddrSelPolicyAddrType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpv6AddrSelPolicyAddrType (tSNMP_OCTET_STRING_TYPE *
                                     pFsMIIpv6AddrSelPolicyPrefix,
                                     INT4 i4FsMIIpv6AddrSelPolicyPrefixLen,
                                     INT4 i4FsMIIpv6AddrSelPolicyIfIndex,
                                     INT4 i4SetValFsMIIpv6AddrSelPolicyAddrType)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhSetFsipv6AddrSelPolicyAddrType (pFsMIIpv6AddrSelPolicyPrefix,
                                                  i4FsMIIpv6AddrSelPolicyPrefixLen,
                                                  i4FsMIIpv6AddrSelPolicyIfIndex,
                                                  i4SetValFsMIIpv6AddrSelPolicyAddrType);

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIIpv6AddrSelPolicyRowStatus
 Input       :  The Indices
                FsMIIpv6AddrSelPolicyPrefix
                FsMIIpv6AddrSelPolicyPrefixLen
                FsMIIpv6AddrSelPolicyIfIndex

                The Object 
                setValFsMIIpv6AddrSelPolicyRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpv6AddrSelPolicyRowStatus (tSNMP_OCTET_STRING_TYPE *
                                      pFsMIIpv6AddrSelPolicyPrefix,
                                      INT4 i4FsMIIpv6AddrSelPolicyPrefixLen,
                                      INT4 i4FsMIIpv6AddrSelPolicyIfIndex,
                                      INT4
                                      i4SetValFsMIIpv6AddrSelPolicyRowStatus)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhSetFsipv6AddrSelPolicyRowStatus (pFsMIIpv6AddrSelPolicyPrefix,
                                                   i4FsMIIpv6AddrSelPolicyPrefixLen,
                                                   i4FsMIIpv6AddrSelPolicyIfIndex,
                                                   i4SetValFsMIIpv6AddrSelPolicyRowStatus);

    return i1RetVal;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6AddrSelPolicyPrecedence
 Input       :  The Indices
                FsMIIpv6AddrSelPolicyPrefix
                FsMIIpv6AddrSelPolicyPrefixLen
                FsMIIpv6AddrSelPolicyIfIndex

                The Object 
                testValFsMIIpv6AddrSelPolicyPrecedence
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpv6AddrSelPolicyPrecedence (UINT4 *pu4ErrorCode,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pFsMIIpv6AddrSelPolicyPrefix,
                                          INT4 i4FsMIIpv6AddrSelPolicyPrefixLen,
                                          INT4 i4FsMIIpv6AddrSelPolicyIfIndex,
                                          INT4
                                          i4TestValFsMIIpv6AddrSelPolicyPrecedence)
{

    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhTestv2Fsipv6AddrSelPolicyPrecedence (pu4ErrorCode,
                                                       pFsMIIpv6AddrSelPolicyPrefix,
                                                       i4FsMIIpv6AddrSelPolicyPrefixLen,
                                                       i4FsMIIpv6AddrSelPolicyIfIndex,
                                                       i4TestValFsMIIpv6AddrSelPolicyPrecedence);

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6AddrSelPolicyLabel
 Input       :  The Indices
                FsMIIpv6AddrSelPolicyPrefix
                FsMIIpv6AddrSelPolicyPrefixLen
                FsMIIpv6AddrSelPolicyIfIndex

                The Object 
                testValFsMIIpv6AddrSelPolicyLabel
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpv6AddrSelPolicyLabel (UINT4 *pu4ErrorCode,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pFsMIIpv6AddrSelPolicyPrefix,
                                     INT4 i4FsMIIpv6AddrSelPolicyPrefixLen,
                                     INT4 i4FsMIIpv6AddrSelPolicyIfIndex,
                                     INT4 i4TestValFsMIIpv6AddrSelPolicyLabel)
{

    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhTestv2Fsipv6AddrSelPolicyLabel (pu4ErrorCode,
                                                  pFsMIIpv6AddrSelPolicyPrefix,
                                                  i4FsMIIpv6AddrSelPolicyPrefixLen,
                                                  i4FsMIIpv6AddrSelPolicyIfIndex,
                                                  i4TestValFsMIIpv6AddrSelPolicyLabel);

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6AddrSelPolicyAddrType
 Input       :  The Indices
                FsMIIpv6AddrSelPolicyPrefix
                FsMIIpv6AddrSelPolicyPrefixLen
                FsMIIpv6AddrSelPolicyIfIndex

                The Object 
                testValFsMIIpv6AddrSelPolicyAddrType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpv6AddrSelPolicyAddrType (UINT4 *pu4ErrorCode,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pFsMIIpv6AddrSelPolicyPrefix,
                                        INT4 i4FsMIIpv6AddrSelPolicyPrefixLen,
                                        INT4 i4FsMIIpv6AddrSelPolicyIfIndex,
                                        INT4
                                        i4TestValFsMIIpv6AddrSelPolicyAddrType)
{

    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhTestv2Fsipv6AddrSelPolicyAddrType (pu4ErrorCode,
                                                     pFsMIIpv6AddrSelPolicyPrefix,
                                                     i4FsMIIpv6AddrSelPolicyPrefixLen,
                                                     i4FsMIIpv6AddrSelPolicyIfIndex,
                                                     i4TestValFsMIIpv6AddrSelPolicyAddrType);

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6AddrSelPolicyRowStatus
 Input       :  The Indices
                FsMIIpv6AddrSelPolicyPrefix
                FsMIIpv6AddrSelPolicyPrefixLen
                FsMIIpv6AddrSelPolicyIfIndex

                The Object 
                testValFsMIIpv6AddrSelPolicyRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpv6AddrSelPolicyRowStatus (UINT4 *pu4ErrorCode,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pFsMIIpv6AddrSelPolicyPrefix,
                                         INT4 i4FsMIIpv6AddrSelPolicyPrefixLen,
                                         INT4 i4FsMIIpv6AddrSelPolicyIfIndex,
                                         INT4
                                         i4TestValFsMIIpv6AddrSelPolicyRowStatus)
{

    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal =
        nmhTestv2Fsipv6AddrSelPolicyRowStatus (pu4ErrorCode,
                                               pFsMIIpv6AddrSelPolicyPrefix,
                                               i4FsMIIpv6AddrSelPolicyPrefixLen,
                                               i4FsMIIpv6AddrSelPolicyIfIndex,
                                               i4TestValFsMIIpv6AddrSelPolicyRowStatus);
    return i1RetVal;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIIpv6AddrSelPolicyTable
 Input       :  The Indices
                FsMIIpv6AddrSelPolicyPrefix
                FsMIIpv6AddrSelPolicyPrefixLen
                FsMIIpv6AddrSelPolicyIfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIIpv6AddrSelPolicyTable (UINT4 *pu4ErrorCode,
                                    tSnmpIndexList * pSnmpIndexList,
                                    tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIIpv6IfScopeZoneMapTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIIpv6IfScopeZoneMapTable
 Input       :  The Indices
                FsMIIpv6ScopeZoneIndexIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIIpv6IfScopeZoneMapTable (INT4
                                                     i4FsMIIpv6ScopeZoneIndexIfIndex)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal =
        nmhValidateIndexInstanceFsipv6IfScopeZoneMapTable
        (i4FsMIIpv6ScopeZoneIndexIfIndex);

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIIpv6IfScopeZoneMapTable
 Input       :  The Indices
                FsMIIpv6ScopeZoneIndexIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIIpv6IfScopeZoneMapTable (INT4
                                             *pi4FsMIIpv6ScopeZoneIndexIfIndex)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal =
        nmhGetFirstIndexFsipv6IfScopeZoneMapTable
        (pi4FsMIIpv6ScopeZoneIndexIfIndex);

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIIpv6IfScopeZoneMapTable
 Input       :  The Indices
                FsMIIpv6ScopeZoneIndexIfIndex
                nextFsMIIpv6ScopeZoneIndexIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIIpv6IfScopeZoneMapTable (INT4
                                            i4FsMIIpv6ScopeZoneIndexIfIndex,
                                            INT4
                                            *pi4NextFsMIIpv6ScopeZoneIndexIfIndex)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal =
        nmhGetNextIndexFsipv6IfScopeZoneMapTable
        (i4FsMIIpv6ScopeZoneIndexIfIndex, pi4NextFsMIIpv6ScopeZoneIndexIfIndex);
    return i1RetVal;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIIpv6ScopeZoneIndexInterfaceLocal
 Input       :  The Indices
                FsMIIpv6ScopeZoneIndexIfIndex

                The Object 
                retValFsMIIpv6ScopeZoneIndexInterfaceLocal
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6ScopeZoneIndexInterfaceLocal (INT4
                                            i4FsMIIpv6ScopeZoneIndexIfIndex,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pRetValFsMIIpv6ScopeZoneIndexInterfaceLocal)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal =
        nmhGetFsipv6ScopeZoneIndexInterfaceLocal
        (i4FsMIIpv6ScopeZoneIndexIfIndex,
         pRetValFsMIIpv6ScopeZoneIndexInterfaceLocal);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6ScopeZoneIndexLinkLocal
 Input       :  The Indices
                FsMIIpv6ScopeZoneIndexIfIndex

                The Object 
                retValFsMIIpv6ScopeZoneIndexLinkLocal
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6ScopeZoneIndexLinkLocal (INT4 i4FsMIIpv6ScopeZoneIndexIfIndex,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pRetValFsMIIpv6ScopeZoneIndexLinkLocal)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal =
        nmhGetFsipv6ScopeZoneIndexLinkLocal (i4FsMIIpv6ScopeZoneIndexIfIndex,
                                             pRetValFsMIIpv6ScopeZoneIndexLinkLocal);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6ScopeZoneIndex3
 Input       :  The Indices
                FsMIIpv6ScopeZoneIndexIfIndex

                The Object 
                retValFsMIIpv6ScopeZoneIndex3
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6ScopeZoneIndex3 (INT4 i4FsMIIpv6ScopeZoneIndexIfIndex,
                               tSNMP_OCTET_STRING_TYPE *
                               pRetValFsMIIpv6ScopeZoneIndex3)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal =
        nmhGetFsipv6ScopeZoneIndex3 (i4FsMIIpv6ScopeZoneIndexIfIndex,
                                     pRetValFsMIIpv6ScopeZoneIndex3);

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6ScopeZoneIndexAdminLocal
 Input       :  The Indices
                FsMIIpv6ScopeZoneIndexIfIndex

                The Object 
                retValFsMIIpv6ScopeZoneIndexAdminLocal
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6ScopeZoneIndexAdminLocal (INT4 i4FsMIIpv6ScopeZoneIndexIfIndex,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pRetValFsMIIpv6ScopeZoneIndexAdminLocal)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal =
        nmhGetFsipv6ScopeZoneIndexAdminLocal (i4FsMIIpv6ScopeZoneIndexIfIndex,
                                              pRetValFsMIIpv6ScopeZoneIndexAdminLocal);

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6ScopeZoneIndexSiteLocal
 Input       :  The Indices
                FsMIIpv6ScopeZoneIndexIfIndex

                The Object 
                retValFsMIIpv6ScopeZoneIndexSiteLocal
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6ScopeZoneIndexSiteLocal (INT4 i4FsMIIpv6ScopeZoneIndexIfIndex,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pRetValFsMIIpv6ScopeZoneIndexSiteLocal)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal =
        nmhGetFsipv6ScopeZoneIndexSiteLocal (i4FsMIIpv6ScopeZoneIndexIfIndex,
                                             pRetValFsMIIpv6ScopeZoneIndexSiteLocal);

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6ScopeZoneIndex6
 Input       :  The Indices
                FsMIIpv6ScopeZoneIndexIfIndex

                The Object 
                retValFsMIIpv6ScopeZoneIndex6
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6ScopeZoneIndex6 (INT4 i4FsMIIpv6ScopeZoneIndexIfIndex,
                               tSNMP_OCTET_STRING_TYPE *
                               pRetValFsMIIpv6ScopeZoneIndex6)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal =
        nmhGetFsipv6ScopeZoneIndex6 (i4FsMIIpv6ScopeZoneIndexIfIndex,
                                     pRetValFsMIIpv6ScopeZoneIndex6);

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6ScopeZoneIndex7
 Input       :  The Indices
                FsMIIpv6ScopeZoneIndexIfIndex

                The Object 
                retValFsMIIpv6ScopeZoneIndex7
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6ScopeZoneIndex7 (INT4 i4FsMIIpv6ScopeZoneIndexIfIndex,
                               tSNMP_OCTET_STRING_TYPE *
                               pRetValFsMIIpv6ScopeZoneIndex7)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal =
        nmhGetFsipv6ScopeZoneIndex7 (i4FsMIIpv6ScopeZoneIndexIfIndex,
                                     pRetValFsMIIpv6ScopeZoneIndex7);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6ScopeZoneIndexOrganizationLocal
 Input       :  The Indices
                FsMIIpv6ScopeZoneIndexIfIndex

                The Object 
                retValFsMIIpv6ScopeZoneIndexOrganizationLocal
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6ScopeZoneIndexOrganizationLocal (INT4
                                               i4FsMIIpv6ScopeZoneIndexIfIndex,
                                               tSNMP_OCTET_STRING_TYPE *
                                               pRetValFsMIIpv6ScopeZoneIndexOrganizationLocal)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal =
        nmhGetFsipv6ScopeZoneIndexOrganizationLocal
        (i4FsMIIpv6ScopeZoneIndexIfIndex,
         pRetValFsMIIpv6ScopeZoneIndexOrganizationLocal);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6ScopeZoneIndex9
 Input       :  The Indices
                FsMIIpv6ScopeZoneIndexIfIndex

                The Object 
                retValFsMIIpv6ScopeZoneIndex9
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6ScopeZoneIndex9 (INT4 i4FsMIIpv6ScopeZoneIndexIfIndex,
                               tSNMP_OCTET_STRING_TYPE *
                               pRetValFsMIIpv6ScopeZoneIndex9)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal =
        nmhGetFsipv6ScopeZoneIndex9 (i4FsMIIpv6ScopeZoneIndexIfIndex,
                                     pRetValFsMIIpv6ScopeZoneIndex9);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6ScopeZoneIndexA
 Input       :  The Indices
                FsMIIpv6ScopeZoneIndexIfIndex

                The Object 
                retValFsMIIpv6ScopeZoneIndexA
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6ScopeZoneIndexA (INT4 i4FsMIIpv6ScopeZoneIndexIfIndex,
                               tSNMP_OCTET_STRING_TYPE *
                               pRetValFsMIIpv6ScopeZoneIndexA)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal =
        nmhGetFsipv6ScopeZoneIndexA (i4FsMIIpv6ScopeZoneIndexIfIndex,
                                     pRetValFsMIIpv6ScopeZoneIndexA);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6ScopeZoneIndexB
 Input       :  The Indices
                FsMIIpv6ScopeZoneIndexIfIndex

                The Object 
                retValFsMIIpv6ScopeZoneIndexB
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6ScopeZoneIndexB (INT4 i4FsMIIpv6ScopeZoneIndexIfIndex,
                               tSNMP_OCTET_STRING_TYPE *
                               pRetValFsMIIpv6ScopeZoneIndexB)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal =
        nmhGetFsipv6ScopeZoneIndexB (i4FsMIIpv6ScopeZoneIndexIfIndex,
                                     pRetValFsMIIpv6ScopeZoneIndexB);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6ScopeZoneIndexC
 Input       :  The Indices
                FsMIIpv6ScopeZoneIndexIfIndex

                The Object 
                retValFsMIIpv6ScopeZoneIndexC
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6ScopeZoneIndexC (INT4 i4FsMIIpv6ScopeZoneIndexIfIndex,
                               tSNMP_OCTET_STRING_TYPE *
                               pRetValFsMIIpv6ScopeZoneIndexC)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal =
        nmhGetFsipv6ScopeZoneIndexC (i4FsMIIpv6ScopeZoneIndexIfIndex,
                                     pRetValFsMIIpv6ScopeZoneIndexC);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6ScopeZoneIndexD
 Input       :  The Indices
                FsMIIpv6ScopeZoneIndexIfIndex

                The Object 
                retValFsMIIpv6ScopeZoneIndexD
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6ScopeZoneIndexD (INT4 i4FsMIIpv6ScopeZoneIndexIfIndex,
                               tSNMP_OCTET_STRING_TYPE *
                               pRetValFsMIIpv6ScopeZoneIndexD)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal =
        nmhGetFsipv6ScopeZoneIndexD (i4FsMIIpv6ScopeZoneIndexIfIndex,
                                     pRetValFsMIIpv6ScopeZoneIndexD);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6ScopeZoneIndexE
 Input       :  The Indices
                FsMIIpv6ScopeZoneIndexIfIndex

                The Object 
                retValFsMIIpv6ScopeZoneIndexE
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6ScopeZoneIndexE (INT4 i4FsMIIpv6ScopeZoneIndexIfIndex,
                               tSNMP_OCTET_STRING_TYPE *
                               pRetValFsMIIpv6ScopeZoneIndexE)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal =
        nmhGetFsipv6ScopeZoneIndexE (i4FsMIIpv6ScopeZoneIndexIfIndex,
                                     pRetValFsMIIpv6ScopeZoneIndexE);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6IfScopeZoneCreationStatus
 Input       :  The Indices
                FsMIIpv6ScopeZoneIndexIfIndex

                The Object 
                retValFsMIIpv6IfScopeZoneCreationStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6IfScopeZoneCreationStatus (INT4 i4FsMIIpv6ScopeZoneIndexIfIndex,
                                         INT4
                                         *pi4RetValFsMIIpv6IfScopeZoneCreationStatus)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal =
        nmhGetFsipv6IfScopeZoneCreationStatus (i4FsMIIpv6ScopeZoneIndexIfIndex,
                                               pi4RetValFsMIIpv6IfScopeZoneCreationStatus);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6IfScopeZoneRowStatus
 Input       :  The Indices
                FsMIIpv6ScopeZoneIndexIfIndex

                The Object 
                retValFsMIIpv6IfScopeZoneRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6IfScopeZoneRowStatus (INT4 i4FsMIIpv6ScopeZoneIndexIfIndex,
                                    INT4 *pi4RetValFsMIIpv6IfScopeZoneRowStatus)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal =
        nmhGetFsipv6IfScopeZoneRowStatus (i4FsMIIpv6ScopeZoneIndexIfIndex,
                                          pi4RetValFsMIIpv6IfScopeZoneRowStatus);

    return i1RetVal;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIIpv6ScopeZoneIndexInterfaceLocal
 Input       :  The Indices
                FsMIIpv6ScopeZoneIndexIfIndex

                The Object 
                setValFsMIIpv6ScopeZoneIndexInterfaceLocal
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpv6ScopeZoneIndexInterfaceLocal (INT4
                                            i4FsMIIpv6ScopeZoneIndexIfIndex,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pSetValFsMIIpv6ScopeZoneIndexInterfaceLocal)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal =
        nmhSetFsipv6ScopeZoneIndexInterfaceLocal
        (i4FsMIIpv6ScopeZoneIndexIfIndex,
         pSetValFsMIIpv6ScopeZoneIndexInterfaceLocal);

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIIpv6ScopeZoneIndexLinkLocal
 Input       :  The Indices
                FsMIIpv6ScopeZoneIndexIfIndex

                The Object 
                setValFsMIIpv6ScopeZoneIndexLinkLocal
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpv6ScopeZoneIndexLinkLocal (INT4 i4FsMIIpv6ScopeZoneIndexIfIndex,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pSetValFsMIIpv6ScopeZoneIndexLinkLocal)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal =
        nmhSetFsipv6ScopeZoneIndexLinkLocal (i4FsMIIpv6ScopeZoneIndexIfIndex,
                                             pSetValFsMIIpv6ScopeZoneIndexLinkLocal);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIIpv6ScopeZoneIndex3
 Input       :  The Indices
                FsMIIpv6ScopeZoneIndexIfIndex

                The Object 
                setValFsMIIpv6ScopeZoneIndex3
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpv6ScopeZoneIndex3 (INT4 i4FsMIIpv6ScopeZoneIndexIfIndex,
                               tSNMP_OCTET_STRING_TYPE *
                               pSetValFsMIIpv6ScopeZoneIndex3)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhSetFsipv6ScopeZoneIndex3 (i4FsMIIpv6ScopeZoneIndexIfIndex,
                                            pSetValFsMIIpv6ScopeZoneIndex3);

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIIpv6ScopeZoneIndexAdminLocal
 Input       :  The Indices
                FsMIIpv6ScopeZoneIndexIfIndex

                The Object 
                setValFsMIIpv6ScopeZoneIndexAdminLocal
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpv6ScopeZoneIndexAdminLocal (INT4 i4FsMIIpv6ScopeZoneIndexIfIndex,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pSetValFsMIIpv6ScopeZoneIndexAdminLocal)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal =
        nmhSetFsipv6ScopeZoneIndexAdminLocal (i4FsMIIpv6ScopeZoneIndexIfIndex,
                                              pSetValFsMIIpv6ScopeZoneIndexAdminLocal);

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIIpv6ScopeZoneIndexSiteLocal
 Input       :  The Indices
                FsMIIpv6ScopeZoneIndexIfIndex

                The Object 
                setValFsMIIpv6ScopeZoneIndexSiteLocal
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpv6ScopeZoneIndexSiteLocal (INT4 i4FsMIIpv6ScopeZoneIndexIfIndex,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pSetValFsMIIpv6ScopeZoneIndexSiteLocal)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal =
        nmhSetFsipv6ScopeZoneIndexSiteLocal (i4FsMIIpv6ScopeZoneIndexIfIndex,
                                             pSetValFsMIIpv6ScopeZoneIndexSiteLocal);

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIIpv6ScopeZoneIndex6
 Input       :  The Indices
                FsMIIpv6ScopeZoneIndexIfIndex

                The Object 
                setValFsMIIpv6ScopeZoneIndex6
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpv6ScopeZoneIndex6 (INT4 i4FsMIIpv6ScopeZoneIndexIfIndex,
                               tSNMP_OCTET_STRING_TYPE *
                               pSetValFsMIIpv6ScopeZoneIndex6)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhSetFsipv6ScopeZoneIndex6 (i4FsMIIpv6ScopeZoneIndexIfIndex,
                                            pSetValFsMIIpv6ScopeZoneIndex6);

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIIpv6ScopeZoneIndex7
 Input       :  The Indices
                FsMIIpv6ScopeZoneIndexIfIndex

                The Object 
                setValFsMIIpv6ScopeZoneIndex7
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpv6ScopeZoneIndex7 (INT4 i4FsMIIpv6ScopeZoneIndexIfIndex,
                               tSNMP_OCTET_STRING_TYPE *
                               pSetValFsMIIpv6ScopeZoneIndex7)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhSetFsipv6ScopeZoneIndex7 (i4FsMIIpv6ScopeZoneIndexIfIndex,
                                            pSetValFsMIIpv6ScopeZoneIndex7);

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIIpv6ScopeZoneIndexOrganizationLocal
 Input       :  The Indices
                FsMIIpv6ScopeZoneIndexIfIndex

                The Object 
                setValFsMIIpv6ScopeZoneIndexOrganizationLocal
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpv6ScopeZoneIndexOrganizationLocal (INT4
                                               i4FsMIIpv6ScopeZoneIndexIfIndex,
                                               tSNMP_OCTET_STRING_TYPE *
                                               pSetValFsMIIpv6ScopeZoneIndexOrganizationLocal)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal =
        nmhSetFsipv6ScopeZoneIndexOrganizationLocal
        (i4FsMIIpv6ScopeZoneIndexIfIndex,
         pSetValFsMIIpv6ScopeZoneIndexOrganizationLocal);

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIIpv6ScopeZoneIndex9
 Input       :  The Indices
                FsMIIpv6ScopeZoneIndexIfIndex

                The Object 
                setValFsMIIpv6ScopeZoneIndex9
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpv6ScopeZoneIndex9 (INT4 i4FsMIIpv6ScopeZoneIndexIfIndex,
                               tSNMP_OCTET_STRING_TYPE *
                               pSetValFsMIIpv6ScopeZoneIndex9)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhSetFsipv6ScopeZoneIndex9 (i4FsMIIpv6ScopeZoneIndexIfIndex,
                                            pSetValFsMIIpv6ScopeZoneIndex9);

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIIpv6ScopeZoneIndexA
 Input       :  The Indices
                FsMIIpv6ScopeZoneIndexIfIndex

                The Object 
                setValFsMIIpv6ScopeZoneIndexA
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpv6ScopeZoneIndexA (INT4 i4FsMIIpv6ScopeZoneIndexIfIndex,
                               tSNMP_OCTET_STRING_TYPE *
                               pSetValFsMIIpv6ScopeZoneIndexA)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhSetFsipv6ScopeZoneIndexA (i4FsMIIpv6ScopeZoneIndexIfIndex,
                                            pSetValFsMIIpv6ScopeZoneIndexA);

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIIpv6ScopeZoneIndexB
 Input       :  The Indices
                FsMIIpv6ScopeZoneIndexIfIndex

                The Object 
                setValFsMIIpv6ScopeZoneIndexB
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpv6ScopeZoneIndexB (INT4 i4FsMIIpv6ScopeZoneIndexIfIndex,
                               tSNMP_OCTET_STRING_TYPE *
                               pSetValFsMIIpv6ScopeZoneIndexB)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhSetFsipv6ScopeZoneIndexB (i4FsMIIpv6ScopeZoneIndexIfIndex,
                                            pSetValFsMIIpv6ScopeZoneIndexB);

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIIpv6ScopeZoneIndexC
 Input       :  The Indices
                FsMIIpv6ScopeZoneIndexIfIndex

                The Object 
                setValFsMIIpv6ScopeZoneIndexC
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpv6ScopeZoneIndexC (INT4 i4FsMIIpv6ScopeZoneIndexIfIndex,
                               tSNMP_OCTET_STRING_TYPE *
                               pSetValFsMIIpv6ScopeZoneIndexC)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhSetFsipv6ScopeZoneIndexC (i4FsMIIpv6ScopeZoneIndexIfIndex,
                                            pSetValFsMIIpv6ScopeZoneIndexC);

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIIpv6ScopeZoneIndexD
 Input       :  The Indices
                FsMIIpv6ScopeZoneIndexIfIndex

                The Object 
                setValFsMIIpv6ScopeZoneIndexD
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpv6ScopeZoneIndexD (INT4 i4FsMIIpv6ScopeZoneIndexIfIndex,
                               tSNMP_OCTET_STRING_TYPE *
                               pSetValFsMIIpv6ScopeZoneIndexD)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhSetFsipv6ScopeZoneIndexD (i4FsMIIpv6ScopeZoneIndexIfIndex,
                                            pSetValFsMIIpv6ScopeZoneIndexD);

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIIpv6ScopeZoneIndexE
 Input       :  The Indices
                FsMIIpv6ScopeZoneIndexIfIndex

                The Object 
                setValFsMIIpv6ScopeZoneIndexE
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpv6ScopeZoneIndexE (INT4 i4FsMIIpv6ScopeZoneIndexIfIndex,
                               tSNMP_OCTET_STRING_TYPE *
                               pSetValFsMIIpv6ScopeZoneIndexE)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhSetFsipv6ScopeZoneIndexE (i4FsMIIpv6ScopeZoneIndexIfIndex,
                                            pSetValFsMIIpv6ScopeZoneIndexE);

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIIpv6IfScopeZoneRowStatus
 Input       :  The Indices
                FsMIIpv6ScopeZoneIndexIfIndex

                The Object 
                setValFsMIIpv6IfScopeZoneRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpv6IfScopeZoneRowStatus (INT4 i4FsMIIpv6ScopeZoneIndexIfIndex,
                                    INT4 i4SetValFsMIIpv6IfScopeZoneRowStatus)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal =
        nmhSetFsipv6IfScopeZoneRowStatus (i4FsMIIpv6ScopeZoneIndexIfIndex,
                                          i4SetValFsMIIpv6IfScopeZoneRowStatus);

    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6ScopeZoneIndexInterfaceLocal
 Input       :  The Indices
                FsMIIpv6ScopeZoneIndexIfIndex

                The Object 
                testValFsMIIpv6ScopeZoneIndexInterfaceLocal
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpv6ScopeZoneIndexInterfaceLocal (UINT4 *pu4ErrorCode,
                                               INT4
                                               i4FsMIIpv6ScopeZoneIndexIfIndex,
                                               tSNMP_OCTET_STRING_TYPE *
                                               pTestValFsMIIpv6ScopeZoneIndexInterfaceLocal)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhTestv2Fsipv6ScopeZoneIndexInterfaceLocal (pu4ErrorCode,
                                                            i4FsMIIpv6ScopeZoneIndexIfIndex,
                                                            pTestValFsMIIpv6ScopeZoneIndexInterfaceLocal);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6ScopeZoneIndexLinkLocal
 Input       :  The Indices
                FsMIIpv6ScopeZoneIndexIfIndex

                The Object 
                testValFsMIIpv6ScopeZoneIndexLinkLocal
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpv6ScopeZoneIndexLinkLocal (UINT4 *pu4ErrorCode,
                                          INT4 i4FsMIIpv6ScopeZoneIndexIfIndex,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pTestValFsMIIpv6ScopeZoneIndexLinkLocal)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal =
        nmhTestv2Fsipv6ScopeZoneIndexLinkLocal (pu4ErrorCode,
                                                i4FsMIIpv6ScopeZoneIndexIfIndex,
                                                pTestValFsMIIpv6ScopeZoneIndexLinkLocal);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6ScopeZoneIndex3
 Input       :  The Indices
                FsMIIpv6ScopeZoneIndexIfIndex

                The Object 
                testValFsMIIpv6ScopeZoneIndex3
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpv6ScopeZoneIndex3 (UINT4 *pu4ErrorCode,
                                  INT4 i4FsMIIpv6ScopeZoneIndexIfIndex,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pTestValFsMIIpv6ScopeZoneIndex3)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal =
        nmhTestv2Fsipv6ScopeZoneIndex3 (pu4ErrorCode,
                                        i4FsMIIpv6ScopeZoneIndexIfIndex,
                                        pTestValFsMIIpv6ScopeZoneIndex3);

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6ScopeZoneIndexAdminLocal
 Input       :  The Indices
                FsMIIpv6ScopeZoneIndexIfIndex

                The Object 
                testValFsMIIpv6ScopeZoneIndexAdminLocal
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpv6ScopeZoneIndexAdminLocal (UINT4 *pu4ErrorCode,
                                           INT4 i4FsMIIpv6ScopeZoneIndexIfIndex,
                                           tSNMP_OCTET_STRING_TYPE *
                                           pTestValFsMIIpv6ScopeZoneIndexAdminLocal)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal =
        nmhTestv2Fsipv6ScopeZoneIndexAdminLocal (pu4ErrorCode,
                                                 i4FsMIIpv6ScopeZoneIndexIfIndex,
                                                 pTestValFsMIIpv6ScopeZoneIndexAdminLocal);

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6ScopeZoneIndexSiteLocal
 Input       :  The Indices
                FsMIIpv6ScopeZoneIndexIfIndex

                The Object 
                testValFsMIIpv6ScopeZoneIndexSiteLocal
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpv6ScopeZoneIndexSiteLocal (UINT4 *pu4ErrorCode,
                                          INT4 i4FsMIIpv6ScopeZoneIndexIfIndex,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pTestValFsMIIpv6ScopeZoneIndexSiteLocal)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal =
        nmhTestv2Fsipv6ScopeZoneIndexSiteLocal (pu4ErrorCode,
                                                i4FsMIIpv6ScopeZoneIndexIfIndex,
                                                pTestValFsMIIpv6ScopeZoneIndexSiteLocal);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6ScopeZoneIndex6
 Input       :  The Indices
                FsMIIpv6ScopeZoneIndexIfIndex

                The Object 
                testValFsMIIpv6ScopeZoneIndex6
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpv6ScopeZoneIndex6 (UINT4 *pu4ErrorCode,
                                  INT4 i4FsMIIpv6ScopeZoneIndexIfIndex,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pTestValFsMIIpv6ScopeZoneIndex6)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal =
        nmhTestv2Fsipv6ScopeZoneIndex6 (pu4ErrorCode,
                                        i4FsMIIpv6ScopeZoneIndexIfIndex,
                                        pTestValFsMIIpv6ScopeZoneIndex6);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6ScopeZoneIndex7
 Input       :  The Indices
                FsMIIpv6ScopeZoneIndexIfIndex

                The Object 
                testValFsMIIpv6ScopeZoneIndex7
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpv6ScopeZoneIndex7 (UINT4 *pu4ErrorCode,
                                  INT4 i4FsMIIpv6ScopeZoneIndexIfIndex,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pTestValFsMIIpv6ScopeZoneIndex7)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal =
        nmhTestv2Fsipv6ScopeZoneIndex7 (pu4ErrorCode,
                                        i4FsMIIpv6ScopeZoneIndexIfIndex,
                                        pTestValFsMIIpv6ScopeZoneIndex7);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6ScopeZoneIndexOrganizationLocal
 Input       :  The Indices
                FsMIIpv6ScopeZoneIndexIfIndex

                The Object 
                testValFsMIIpv6ScopeZoneIndexOrganizationLocal
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpv6ScopeZoneIndexOrganizationLocal (UINT4 *pu4ErrorCode,
                                                  INT4
                                                  i4FsMIIpv6ScopeZoneIndexIfIndex,
                                                  tSNMP_OCTET_STRING_TYPE *
                                                  pTestValFsMIIpv6ScopeZoneIndexOrganizationLocal)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal =
        nmhTestv2Fsipv6ScopeZoneIndexOrganizationLocal (pu4ErrorCode,
                                                        i4FsMIIpv6ScopeZoneIndexIfIndex,
                                                        pTestValFsMIIpv6ScopeZoneIndexOrganizationLocal);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6ScopeZoneIndex9
 Input       :  The Indices
                FsMIIpv6ScopeZoneIndexIfIndex

                The Object 
                testValFsMIIpv6ScopeZoneIndex9
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpv6ScopeZoneIndex9 (UINT4 *pu4ErrorCode,
                                  INT4 i4FsMIIpv6ScopeZoneIndexIfIndex,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pTestValFsMIIpv6ScopeZoneIndex9)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal =
        nmhTestv2Fsipv6ScopeZoneIndex9 (pu4ErrorCode,
                                        i4FsMIIpv6ScopeZoneIndexIfIndex,
                                        pTestValFsMIIpv6ScopeZoneIndex9);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6ScopeZoneIndexA
 Input       :  The Indices
                FsMIIpv6ScopeZoneIndexIfIndex

                The Object 
                testValFsMIIpv6ScopeZoneIndexA
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpv6ScopeZoneIndexA (UINT4 *pu4ErrorCode,
                                  INT4 i4FsMIIpv6ScopeZoneIndexIfIndex,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pTestValFsMIIpv6ScopeZoneIndexA)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal =
        nmhTestv2Fsipv6ScopeZoneIndexA (pu4ErrorCode,
                                        i4FsMIIpv6ScopeZoneIndexIfIndex,
                                        pTestValFsMIIpv6ScopeZoneIndexA);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6ScopeZoneIndexB
 Input       :  The Indices
                FsMIIpv6ScopeZoneIndexIfIndex

                The Object 
                testValFsMIIpv6ScopeZoneIndexB
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpv6ScopeZoneIndexB (UINT4 *pu4ErrorCode,
                                  INT4 i4FsMIIpv6ScopeZoneIndexIfIndex,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pTestValFsMIIpv6ScopeZoneIndexB)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal =
        nmhTestv2Fsipv6ScopeZoneIndexB (pu4ErrorCode,
                                        i4FsMIIpv6ScopeZoneIndexIfIndex,
                                        pTestValFsMIIpv6ScopeZoneIndexB);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6ScopeZoneIndexC
 Input       :  The Indices
                FsMIIpv6ScopeZoneIndexIfIndex

                The Object 
                testValFsMIIpv6ScopeZoneIndexC
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpv6ScopeZoneIndexC (UINT4 *pu4ErrorCode,
                                  INT4 i4FsMIIpv6ScopeZoneIndexIfIndex,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pTestValFsMIIpv6ScopeZoneIndexC)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal =
        nmhTestv2Fsipv6ScopeZoneIndexC (pu4ErrorCode,
                                        i4FsMIIpv6ScopeZoneIndexIfIndex,
                                        pTestValFsMIIpv6ScopeZoneIndexC);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6ScopeZoneIndexD
 Input       :  The Indices
                FsMIIpv6ScopeZoneIndexIfIndex

                The Object 
                testValFsMIIpv6ScopeZoneIndexD
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpv6ScopeZoneIndexD (UINT4 *pu4ErrorCode,
                                  INT4 i4FsMIIpv6ScopeZoneIndexIfIndex,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pTestValFsMIIpv6ScopeZoneIndexD)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal =
        nmhTestv2Fsipv6ScopeZoneIndexD (pu4ErrorCode,
                                        i4FsMIIpv6ScopeZoneIndexIfIndex,
                                        pTestValFsMIIpv6ScopeZoneIndexD);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6ScopeZoneIndexE
 Input       :  The Indices
                FsMIIpv6ScopeZoneIndexIfIndex

                The Object 
                testValFsMIIpv6ScopeZoneIndexE
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpv6ScopeZoneIndexE (UINT4 *pu4ErrorCode,
                                  INT4 i4FsMIIpv6ScopeZoneIndexIfIndex,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pTestValFsMIIpv6ScopeZoneIndexE)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal =
        nmhTestv2Fsipv6ScopeZoneIndexE (pu4ErrorCode,
                                        i4FsMIIpv6ScopeZoneIndexIfIndex,
                                        pTestValFsMIIpv6ScopeZoneIndexE);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6IfScopeZoneRowStatus
 Input       :  The Indices
                FsMIIpv6ScopeZoneIndexIfIndex

                The Object 
                testValFsMIIpv6IfScopeZoneRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpv6IfScopeZoneRowStatus (UINT4 *pu4ErrorCode,
                                       INT4 i4FsMIIpv6ScopeZoneIndexIfIndex,
                                       INT4
                                       i4TestValFsMIIpv6IfScopeZoneRowStatus)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal =
        nmhTestv2Fsipv6IfScopeZoneRowStatus (pu4ErrorCode,
                                             i4FsMIIpv6ScopeZoneIndexIfIndex,
                                             i4TestValFsMIIpv6IfScopeZoneRowStatus);

    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIIpv6IfScopeZoneMapTable
 Input       :  The Indices
                FsMIIpv6ScopeZoneIndexIfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIIpv6IfScopeZoneMapTable (UINT4 *pu4ErrorCode,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIIpv6ScopeZoneTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIIpv6ScopeZoneTable
 Input       :  The Indices
                FsMIIpv6ScopeZoneContextId
                FsMIIpv6ScopeZoneName
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIIpv6ScopeZoneTable (INT4
                                                i4FsMIIpv6ScopeZoneContextId,
                                                tSNMP_OCTET_STRING_TYPE *
                                                pFsMIIpv6ScopeZoneName)
{

    INT1                i1RetVal = SNMP_FAILURE;

    if ((i4FsMIIpv6ScopeZoneContextId < VCM_DEFAULT_CONTEXT) ||
        ((UINT4) i4FsMIIpv6ScopeZoneContextId >= IP6_MAX_INSTANCES))
    {
        return SNMP_FAILURE;
    }

    if (Ip6VRExists (i4FsMIIpv6ScopeZoneContextId) == IP6_FAILURE)
    {
        return i1RetVal;
    }

    if (Ip6SelectContext (i4FsMIIpv6ScopeZoneContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }

    if (SNMP_FAILURE ==
        nmhValidateIndexInstanceFsipv6ScopeZoneTable (pFsMIIpv6ScopeZoneName))
    {
        Ip6ReleaseContext ();
        return i1RetVal;
    }

    Ip6ReleaseContext ();

    if (NULL ==
        Ip6ZoneGetRBTreeScopeZoneEntry (pFsMIIpv6ScopeZoneName->pu1_OctetList,
                                        (UINT4) i4FsMIIpv6ScopeZoneContextId))
    {
        return i1RetVal;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIIpv6ScopeZoneTable
 Input       :  The Indices
                FsMIIpv6ScopeZoneContextId
                FsMIIpv6ScopeZoneName
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIIpv6ScopeZoneTable (INT4 *pi4FsMIIpv6ScopeZoneContextId,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pFsMIIpv6ScopeZoneName)
{
    INT1                i1RetVal = SNMP_FAILURE;
    INT4                i4FsMIStdIpContextId;
    INT4                i4Value = IP6_FAILURE;

    i4Value = Ip6UtilGetFirstCxtId ((UINT4 *) pi4FsMIIpv6ScopeZoneContextId);

    while (i4Value == IP6_SUCCESS)
    {
        i4FsMIStdIpContextId = *pi4FsMIIpv6ScopeZoneContextId;
        if (Ip6SelectContext (i4FsMIStdIpContextId) == SNMP_SUCCESS)
        {
            i1RetVal =
                nmhGetFirstIndexFsipv6ScopeZoneTable (pFsMIIpv6ScopeZoneName);
            Ip6ReleaseContext ();
        }
        if (i1RetVal == SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
        i4Value = Ip6UtilGetNextCxtId ((UINT4) i4FsMIStdIpContextId,
                                       (UINT4 *) pi4FsMIIpv6ScopeZoneContextId);

    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIIpv6ScopeZoneTable
 Input       :  The Indices
                FsMIIpv6ScopeZoneContextId
                nextFsMIIpv6ScopeZoneContextId
                FsMIIpv6ScopeZoneName
                nextFsMIIpv6ScopeZoneName
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIIpv6ScopeZoneTable (INT4 i4FsMIIpv6ScopeZoneContextId,
                                       INT4 *pi4NextFsMIIpv6ScopeZoneContextId,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFsMIIpv6ScopeZoneName,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pNextFsMIIpv6ScopeZoneName)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (Ip6VRExists (i4FsMIIpv6ScopeZoneContextId) == IP6_SUCCESS)
    {
        if (Ip6SelectContext (i4FsMIIpv6ScopeZoneContextId) == SNMP_SUCCESS)
        {
            i1RetVal =
                nmhGetNextIndexFsipv6ScopeZoneTable (pFsMIIpv6ScopeZoneName,
                                                     pNextFsMIIpv6ScopeZoneName);
            Ip6ReleaseContext ();
        }
        if (i1RetVal == SNMP_SUCCESS)
        {
            *pi4NextFsMIIpv6ScopeZoneContextId = i4FsMIIpv6ScopeZoneContextId;
            return SNMP_SUCCESS;
        }
    }

    while ((Ip6UtilGetNextCxtId ((UINT4) i4FsMIIpv6ScopeZoneContextId,
                                 (UINT4 *) pi4NextFsMIIpv6ScopeZoneContextId))
           == IP6_SUCCESS)
    {
        if (Ip6SelectContext (*pi4NextFsMIIpv6ScopeZoneContextId) ==
            SNMP_SUCCESS)
        {
            i1RetVal =
                nmhGetFirstIndexFsipv6ScopeZoneTable (pFsMIIpv6ScopeZoneName);
            Ip6ReleaseContext ();
        }
        if (i1RetVal == SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
        i4FsMIIpv6ScopeZoneContextId = *pi4NextFsMIIpv6ScopeZoneContextId;
    }

    return SNMP_FAILURE;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIIpv6ScopeZoneIndex
 Input       :  The Indices
                FsMIIpv6ScopeZoneContextId
                FsMIIpv6ScopeZoneName

                The Object 
                retValFsMIIpv6ScopeZoneIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6ScopeZoneIndex (INT4 i4FsMIIpv6ScopeZoneContextId,
                              tSNMP_OCTET_STRING_TYPE * pFsMIIpv6ScopeZoneName,
                              UINT4 *pu4RetValFsMIIpv6ScopeZoneIndex)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (Ip6SelectContext (i4FsMIIpv6ScopeZoneContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }

    i1RetVal =
        nmhGetFsipv6ScopeZoneIndex (pFsMIIpv6ScopeZoneName,
                                    pu4RetValFsMIIpv6ScopeZoneIndex);

    Ip6ReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6ScopeZoneCreationStatus
 Input       :  The Indices
                FsMIIpv6ScopeZoneContextId
                FsMIIpv6ScopeZoneName

                The Object 
                retValFsMIIpv6ScopeZoneCreationStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6ScopeZoneCreationStatus (INT4 i4FsMIIpv6ScopeZoneContextId,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFsMIIpv6ScopeZoneName,
                                       INT4
                                       *pi4RetValFsMIIpv6ScopeZoneCreationStatus)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (Ip6SelectContext (i4FsMIIpv6ScopeZoneContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }

    i1RetVal =
        nmhGetFsipv6ScopeZoneCreationStatus (pFsMIIpv6ScopeZoneName,
                                             pi4RetValFsMIIpv6ScopeZoneCreationStatus);

    Ip6ReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6ScopeZoneInterfaceList
 Input       :  The Indices
                FsMIIpv6ScopeZoneContextId
                FsMIIpv6ScopeZoneName

                The Object 
                retValFsMIIpv6ScopeZoneInterfaceList
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6ScopeZoneInterfaceList (INT4 i4FsMIIpv6ScopeZoneContextId,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pFsMIIpv6ScopeZoneName,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pRetValFsMIIpv6ScopeZoneInterfaceList)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (Ip6SelectContext (i4FsMIIpv6ScopeZoneContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }

    i1RetVal =
        nmhGetFsipv6ScopeZoneInterfaceList (pFsMIIpv6ScopeZoneName,
                                            pRetValFsMIIpv6ScopeZoneInterfaceList);

    Ip6ReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6IsDefaultScopeZone
 Input       :  The Indices
                FsMIIpv6ScopeZoneContextId
                FsMIIpv6ScopeZoneName

                The Object 
                retValFsMIIpv6IsDefaultScopeZone
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6IsDefaultScopeZone (INT4 i4FsMIIpv6ScopeZoneContextId,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pFsMIIpv6ScopeZoneName,
                                  INT4 *pi4RetValFsMIIpv6IsDefaultScopeZone)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (Ip6SelectContext (i4FsMIIpv6ScopeZoneContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }

    i1RetVal =
        nmhGetFsipv6IsDefaultScopeZone (pFsMIIpv6ScopeZoneName,
                                        pi4RetValFsMIIpv6IsDefaultScopeZone);

    Ip6ReleaseContext ();
    return i1RetVal;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIIpv6IsDefaultScopeZone
 Input       :  The Indices
                FsMIIpv6ScopeZoneContextId
                FsMIIpv6ScopeZoneName

                The Object 
                setValFsMIIpv6IsDefaultScopeZone
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpv6IsDefaultScopeZone (INT4 i4FsMIIpv6ScopeZoneContextId,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pFsMIIpv6ScopeZoneName,
                                  INT4 i4SetValFsMIIpv6IsDefaultScopeZone)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (Ip6SelectContext (i4FsMIIpv6ScopeZoneContextId) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetFsipv6IsDefaultScopeZone (pFsMIIpv6ScopeZoneName,
                                        i4SetValFsMIIpv6IsDefaultScopeZone);
    Ip6ReleaseContext ();

    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6IsDefaultScopeZone
 Input       :  The Indices
                FsMIIpv6ScopeZoneContextId
                FsMIIpv6ScopeZoneName

                The Object 
                testValFsMIIpv6IsDefaultScopeZone
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpv6IsDefaultScopeZone (UINT4 *pu4ErrorCode,
                                     INT4 i4FsMIIpv6ScopeZoneContextId,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pFsMIIpv6ScopeZoneName,
                                     INT4 i4TestValFsMIIpv6IsDefaultScopeZone)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if ((i4FsMIIpv6ScopeZoneContextId < VCM_DEFAULT_CONTEXT) ||
        ((UINT4) i4FsMIIpv6ScopeZoneContextId >= IP6_MAX_INSTANCES))
    {
        return SNMP_FAILURE;
    }

    if (Ip6SelectContext (i4FsMIIpv6ScopeZoneContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2Fsipv6IsDefaultScopeZone (pu4ErrorCode,
                                           pFsMIIpv6ScopeZoneName,
                                           i4TestValFsMIIpv6IsDefaultScopeZone);
    Ip6ReleaseContext ();
    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIIpv6ScopeZoneTable
 Input       :  The Indices
                FsMIIpv6ScopeZoneContextId
                FsMIIpv6ScopeZoneName
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIIpv6ScopeZoneTable (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIIpv6TestRedEntryTime
 Input       :  The Indices

                The Object 
                retValFsMIIpv6TestRedEntryTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6TestRedEntryTime (INT4 *pi4RetValFsMIIpv6TestRedEntryTime)
{
    *pi4RetValFsMIIpv6TestRedEntryTime = gNd6RedGlobalInfo.i4Nd6RedEntryTime;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6TestRedExitTime
 Input       :  The Indices

                The Object 
                retValFsMIIpv6TestRedExitTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6TestRedExitTime (INT4 *pi4RetValFsMIIpv6TestRedExitTime)
{
    *pi4RetValFsMIIpv6TestRedExitTime = gNd6RedGlobalInfo.i4Nd6RedExitTime;
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIIpv6IfRaRDNSSTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIIpv6IfRaRDNSSTable
 Input       :  The Indices
                FsMIIpv6IfRaRDNSSIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIIpv6IfRaRDNSSTable (INT4 i4FsMIIpv6IfRaRDNSSIndex)
{
    UNUSED_PARAM (i4FsMIIpv6IfRaRDNSSIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIIpv6IfRaRDNSSTable
 Input       :  The Indices
                FsMIIpv6IfRaRDNSSIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIIpv6IfRaRDNSSTable (INT4 *pi4FsMIIpv6IfRaRDNSSIndex)
{
    UNUSED_PARAM (pi4FsMIIpv6IfRaRDNSSIndex);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIIpv6IfRaRDNSSTable
 Input       :  The Indices
                FsMIIpv6IfRaRDNSSIndex
                nextFsMIIpv6IfRaRDNSSIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIIpv6IfRaRDNSSTable (INT4 i4FsMIIpv6IfRaRDNSSIndex,
                                       INT4 *pi4NextFsMIIpv6IfRaRDNSSIndex)
{
    UNUSED_PARAM (i4FsMIIpv6IfRaRDNSSIndex);
    UNUSED_PARAM (pi4NextFsMIIpv6IfRaRDNSSIndex);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetFsMIIpv6IfRadvRDNSSOpen
 Input       :  The Indices
                FsMIIpv6IfRaRDNSSIndex

                The Object
                retValFsMIIpv6IfRadvRDNSSOpen
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6IfRadvRDNSSOpen (INT4 i4FsMIIpv6IfRaRDNSSIndex,
                               INT4 *pi4RetValFsMIIpv6IfRadvRDNSSOpen)
{
    UNUSED_PARAM (i4FsMIIpv6IfRaRDNSSIndex);
    UNUSED_PARAM (pi4RetValFsMIIpv6IfRadvRDNSSOpen);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6IfRaRDNSSPreference
 Input       :  The Indices
                FsMIIpv6IfRaRDNSSIndex

                The Object
                retValFsMIIpv6IfRaRDNSSPreference
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6IfRaRDNSSPreference (INT4 i4FsMIIpv6IfRaRDNSSIndex,
                                   INT4 *pi4RetValFsMIIpv6IfRaRDNSSPreference)
{
    UNUSED_PARAM (i4FsMIIpv6IfRaRDNSSIndex);
    UNUSED_PARAM (pi4RetValFsMIIpv6IfRaRDNSSPreference);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6IfRaRDNSSLifetime
 Input       :  The Indices
                FsMIIpv6IfRaRDNSSIndex

                The Object
                retValFsMIIpv6IfRaRDNSSLifetime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6IfRaRDNSSLifetime (INT4 i4FsMIIpv6IfRaRDNSSIndex,
                                 UINT4 *pu4RetValFsMIIpv6IfRaRDNSSLifetime)
{
    UNUSED_PARAM (i4FsMIIpv6IfRaRDNSSIndex);
    UNUSED_PARAM (pu4RetValFsMIIpv6IfRaRDNSSLifetime);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6IfRaRDNSSAddrOne
 Input       :  The Indices
                FsMIIpv6IfRaRDNSSIndex

                The Object
                retValFsMIIpv6IfRaRDNSSAddrOne
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6IfRaRDNSSAddrOne (INT4 i4FsMIIpv6IfRaRDNSSIndex,
                                tSNMP_OCTET_STRING_TYPE *
                                pRetValFsMIIpv6IfRaRDNSSAddrOne)
{
    UNUSED_PARAM (i4FsMIIpv6IfRaRDNSSIndex);
    UNUSED_PARAM (pRetValFsMIIpv6IfRaRDNSSAddrOne);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6IfRaRDNSSAddrTwo
 Input       :  The Indices
                FsMIIpv6IfRaRDNSSIndex

                The Object
                retValFsMIIpv6IfRaRDNSSAddrTwo
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6IfRaRDNSSAddrTwo (INT4 i4FsMIIpv6IfRaRDNSSIndex,
                                tSNMP_OCTET_STRING_TYPE *
                                pRetValFsMIIpv6IfRaRDNSSAddrTwo)
{
    UNUSED_PARAM (i4FsMIIpv6IfRaRDNSSIndex);
    UNUSED_PARAM (pRetValFsMIIpv6IfRaRDNSSAddrTwo);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6IfRaRDNSSAddrThree
 Input       :  The Indices
                FsMIIpv6IfRaRDNSSIndex

                The Object
                retValFsMIIpv6IfRaRDNSSAddrThree
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6IfRaRDNSSAddrThree (INT4 i4FsMIIpv6IfRaRDNSSIndex,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pRetValFsMIIpv6IfRaRDNSSAddrThree)
{
    UNUSED_PARAM (i4FsMIIpv6IfRaRDNSSIndex);
    UNUSED_PARAM (pRetValFsMIIpv6IfRaRDNSSAddrThree);
    return SNMP_FAILURE;
}
/****************************************************************************
 Function    :  nmhGetFsMIIpv6IfDomainNameOne
 Input       :  The Indices
                FsMIIpv6IfRaRDNSSIndex

                The Object
                retValFsMIIpv6IfDomainNameOne
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6IfDomainNameOne (INT4 i4FsMIIpv6IfRaRDNSSIndex,
                               tSNMP_OCTET_STRING_TYPE *
                               pRetValFsMIIpv6IfDomainNameOne)
{
    UNUSED_PARAM (i4FsMIIpv6IfRaRDNSSIndex);
    UNUSED_PARAM (pRetValFsMIIpv6IfDomainNameOne);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6IfDomainNameTwo
 Input       :  The Indices
                FsMIIpv6IfRaRDNSSIndex

                The Object
                retValFsMIIpv6IfDomainNameTwo
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6IfDomainNameTwo (INT4 i4FsMIIpv6IfRaRDNSSIndex,
                               tSNMP_OCTET_STRING_TYPE *
                               pRetValFsMIIpv6IfDomainNameTwo)
{
    UNUSED_PARAM (i4FsMIIpv6IfRaRDNSSIndex);
    UNUSED_PARAM (pRetValFsMIIpv6IfDomainNameTwo);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6IfDomainNameThree
 Input       :  The Indices
                FsMIIpv6IfRaRDNSSIndex

                The Object
                retValFsMIIpv6IfDomainNameThree
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6IfDomainNameThree (INT4 i4FsMIIpv6IfRaRDNSSIndex,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pRetValFsMIIpv6IfDomainNameThree)
{
    UNUSED_PARAM (i4FsMIIpv6IfRaRDNSSIndex);
    UNUSED_PARAM (pRetValFsMIIpv6IfDomainNameThree);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6IfDnsLifeTime
 Input       :  The Indices
                FsMIIpv6IfRaRDNSSIndex

                The Object
                retValFsMIIpv6IfDnsLifeTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6IfDnsLifeTime (INT4 i4FsMIIpv6IfRaRDNSSIndex,
                             INT4 *pi4RetValFsMIIpv6IfDnsLifeTime)
{
    UNUSED_PARAM (i4FsMIIpv6IfRaRDNSSIndex);
    UNUSED_PARAM (pi4RetValFsMIIpv6IfDnsLifeTime);
    return SNMP_FAILURE;
}
/****************************************************************************
 Function    :  nmhGetFsMIIpv6IfRaRDNSSAddrOneLife
 Input       :  The Indices
                FsMIIpv6IfRaRDNSSIndex

                The Object
                retValFsMIIpv6IfRaRDNSSAddrOneLife
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
nmhGetFsMIIpv6IfRaRDNSSAddrOneLife(INT4 i4FsMIIpv6IfRaRDNSSIndex , 
                                        UINT4 *pu4RetValFsMIIpv6IfRaRDNSSAddrOneLife)
{
    UNUSED_PARAM (i4FsMIIpv6IfRaRDNSSIndex);
    UNUSED_PARAM (pu4RetValFsMIIpv6IfRaRDNSSAddrOneLife);
    return SNMP_FAILURE;
}
/****************************************************************************
 Function    :  nmhGetFsMIIpv6IfRaRDNSSAddrTwoLife
 Input       :  The Indices
                FsMIIpv6IfRaRDNSSIndex

                The Object
                retValFsMIIpv6IfRaRDNSSAddrTwoLife
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
nmhGetFsMIIpv6IfRaRDNSSAddrTwoLife(INT4 i4FsMIIpv6IfRaRDNSSIndex , 
                                   UINT4 *pu4RetValFsMIIpv6IfRaRDNSSAddrTwoLife)
{
    UNUSED_PARAM (i4FsMIIpv6IfRaRDNSSIndex);
    UNUSED_PARAM (pu4RetValFsMIIpv6IfRaRDNSSAddrTwoLife);
    return SNMP_FAILURE;
}
/****************************************************************************
 Function    :  nmhGetFsMIIpv6IfRaRDNSSAddrThreeLife
 Input       :  The Indices
                FsMIIpv6IfRaRDNSSIndex

                The Object
                retValFsMIIpv6IfRaRDNSSAddrThreeLife
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
nmhGetFsMIIpv6IfRaRDNSSAddrThreeLife(INT4 i4FsMIIpv6IfRaRDNSSIndex , 
                                     UINT4 *pu4RetValFsMIIpv6IfRaRDNSSAddrThreeLife)
{
    UNUSED_PARAM (i4FsMIIpv6IfRaRDNSSIndex);
    UNUSED_PARAM (pu4RetValFsMIIpv6IfRaRDNSSAddrThreeLife);
    return SNMP_FAILURE;
}
/****************************************************************************
 Function    :  nmhGetFsMIIpv6IfDomainNameOneLife
 Input       :  The Indices
                FsMIIpv6IfRaRDNSSIndex

                The Object
                retValFsMIIpv6IfRaDNSLife
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
nmhGetFsMIIpv6IfDomainNameOneLife   (INT4 i4FsMIIpv6IfRaRDNSSIndex , 
                                     INT4 *pu4RetValFsMIIpv6IfRaDNSLife)
{
    UNUSED_PARAM (i4FsMIIpv6IfRaRDNSSIndex);
    UNUSED_PARAM (pu4RetValFsMIIpv6IfRaDNSLife);
    return SNMP_FAILURE;
}
/****************************************************************************
 Function    :  nmhGetFsMIIpv6IfDomainNameTwoLife
 Input       :  The Indices
                FsMIIpv6IfRaRDNSSIndex

                The Object
                retValFsMIIpv6IfRaDNSLife
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
nmhGetFsMIIpv6IfDomainNameTwoLife   (INT4 i4FsMIIpv6IfRaRDNSSIndex , 
                                     INT4 *pu4RetValFsMIIpv6IfRaDNSLife)
{
    UNUSED_PARAM (i4FsMIIpv6IfRaRDNSSIndex);
    UNUSED_PARAM (pu4RetValFsMIIpv6IfRaDNSLife);
    return SNMP_FAILURE;
}
/****************************************************************************
 Function    :  nmhGetFsMIIpv6IfDomainNameThreeLife
 Input       :  The Indices
                FsMIIpv6IfRaRDNSSIndex

                The Object
                retValFsMIIpv6IfRaDNSLife
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
nmhGetFsMIIpv6IfDomainNameThreeLife   (INT4 i4FsMIIpv6IfRaRDNSSIndex , 
                                     INT4 *pu4RetValFsMIIpv6IfRaDNSLife)
{
    UNUSED_PARAM (i4FsMIIpv6IfRaRDNSSIndex);
    UNUSED_PARAM (pu4RetValFsMIIpv6IfRaDNSLife);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpv6IfRaRDNSSRowStatus
 Input       :  The Indices
                FsMIIpv6IfRaRDNSSIndex

                The Object
                retValFsMIIpv6IfRaRDNSSRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpv6IfRaRDNSSRowStatus (INT4 i4FsMIIpv6IfRaRDNSSIndex,
                                  INT4 *pi4RetValFsMIIpv6IfRaRDNSSRowStatus)
{
    UNUSED_PARAM (i4FsMIIpv6IfRaRDNSSIndex);
    UNUSED_PARAM (pi4RetValFsMIIpv6IfRaRDNSSRowStatus);
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIIpv6IfRadvRDNSSOpen
 Input       :  The Indices
                FsMIIpv6IfRaRDNSSIndex

                The Object
                setValFsMIIpv6IfRadvRDNSSOpen
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpv6IfRadvRDNSSOpen (INT4 i4FsMIIpv6IfRaRDNSSIndex,
                               INT4 i4SetValFsMIIpv6IfRadvRDNSSOpen)
{
    UNUSED_PARAM (i4FsMIIpv6IfRaRDNSSIndex);
    UNUSED_PARAM (i4SetValFsMIIpv6IfRadvRDNSSOpen);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMIIpv6IfRaRDNSSPreference
 Input       :  The Indices
                FsMIIpv6IfRaRDNSSIndex

                The Object
                setValFsMIIpv6IfRaRDNSSPreference
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpv6IfRaRDNSSPreference (INT4 i4FsMIIpv6IfRaRDNSSIndex,
                                   INT4 i4SetValFsMIIpv6IfRaRDNSSPreference)
{
    UNUSED_PARAM (i4FsMIIpv6IfRaRDNSSIndex);
    UNUSED_PARAM (i4SetValFsMIIpv6IfRaRDNSSPreference);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMIIpv6IfRaRDNSSLifetime
 Input       :  The Indices
                FsMIIpv6IfRaRDNSSIndex

                The Object
                setValFsMIIpv6IfRaRDNSSLifetime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpv6IfRaRDNSSLifetime (INT4 i4FsMIIpv6IfRaRDNSSIndex,
                                 UINT4 u4SetValFsMIIpv6IfRaRDNSSLifetime)
{
    UNUSED_PARAM (i4FsMIIpv6IfRaRDNSSIndex);
    UNUSED_PARAM (u4SetValFsMIIpv6IfRaRDNSSLifetime);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMIIpv6IfRaRDNSSAddrOne
 Input       :  The Indices
                FsMIIpv6IfRaRDNSSIndex

                The Object
                setValFsMIIpv6IfRaRDNSSAddrOne
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpv6IfRaRDNSSAddrOne (INT4 i4FsMIIpv6IfRaRDNSSIndex,
                                tSNMP_OCTET_STRING_TYPE *
                                pSetValFsMIIpv6IfRaRDNSSAddrOne)
{
    UNUSED_PARAM (i4FsMIIpv6IfRaRDNSSIndex);
    UNUSED_PARAM (pSetValFsMIIpv6IfRaRDNSSAddrOne);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMIIpv6IfRaRDNSSAddrTwo
 Input       :  The Indices
                FsMIIpv6IfRaRDNSSIndex

                The Object
                setValFsMIIpv6IfRaRDNSSAddrTwo
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpv6IfRaRDNSSAddrTwo (INT4 i4FsMIIpv6IfRaRDNSSIndex,
                                tSNMP_OCTET_STRING_TYPE *
                                pSetValFsMIIpv6IfRaRDNSSAddrTwo)
{
    UNUSED_PARAM (i4FsMIIpv6IfRaRDNSSIndex);
    UNUSED_PARAM (pSetValFsMIIpv6IfRaRDNSSAddrTwo);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMIIpv6IfRaRDNSSAddrThree
 Input       :  The Indices
                FsMIIpv6IfRaRDNSSIndex

                The Object
                setValFsMIIpv6IfRaRDNSSAddrThree
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpv6IfRaRDNSSAddrThree (INT4 i4FsMIIpv6IfRaRDNSSIndex,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pSetValFsMIIpv6IfRaRDNSSAddrThree)
{
    UNUSED_PARAM (i4FsMIIpv6IfRaRDNSSIndex);
    UNUSED_PARAM (pSetValFsMIIpv6IfRaRDNSSAddrThree);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMIIpv6IfRaRDNSSRowStatus
 Input       :  The Indices
                FsMIIpv6IfRaRDNSSIndex

                The Object
                setValFsMIIpv6IfRaRDNSSRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpv6IfRaRDNSSRowStatus (INT4 i4FsMIIpv6IfRaRDNSSIndex,
                                  INT4 i4SetValFsMIIpv6IfRaRDNSSRowStatus)
{
    UNUSED_PARAM (i4FsMIIpv6IfRaRDNSSIndex);
    UNUSED_PARAM (i4SetValFsMIIpv6IfRaRDNSSRowStatus);
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetFsMIIpv6IfDomainNameOne
 Input       :  The Indices
                FsMIIpv6IfRaRDNSSIndex

                The Object
                setValFsMIIpv6IfDomainNameOne
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpv6IfDomainNameOne (INT4 i4FsMIIpv6IfRaRDNSSIndex,
                               tSNMP_OCTET_STRING_TYPE *
                               pSetValFsMIIpv6IfDomainNameOne)
{
    UNUSED_PARAM (i4FsMIIpv6IfRaRDNSSIndex);
    UNUSED_PARAM (pSetValFsMIIpv6IfDomainNameOne);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMIIpv6IfDomainNameTwo
 Input       :  The Indices
                FsMIIpv6IfRaRDNSSIndex

                The Object
                setValFsMIIpv6IfDomainNameTwo
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpv6IfDomainNameTwo (INT4 i4FsMIIpv6IfRaRDNSSIndex,
                               tSNMP_OCTET_STRING_TYPE *
                               pSetValFsMIIpv6IfDomainNameTwo)
{
    UNUSED_PARAM (i4FsMIIpv6IfRaRDNSSIndex);
    UNUSED_PARAM (pSetValFsMIIpv6IfDomainNameTwo);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMIIpv6IfDomainNameThree
 Input       :  The Indices
                FsMIIpv6IfRaRDNSSIndex

                The Object
                setValFsMIIpv6IfDomainNameThree
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpv6IfDomainNameThree (INT4 i4FsMIIpv6IfRaRDNSSIndex,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pSetValFsMIIpv6IfDomainNameThree)
{
    UNUSED_PARAM (i4FsMIIpv6IfRaRDNSSIndex);
    UNUSED_PARAM (pSetValFsMIIpv6IfDomainNameThree);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMIIpv6IfDnsLifeTime
 Input       :  The Indices
                FsMIIpv6IfRaRDNSSIndex

                The Object
                setValFsMIIpv6IfDnsLifeTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpv6IfDnsLifeTime (INT4 i4FsMIIpv6IfRaRDNSSIndex,
                             INT4 i4SetValFsMIIpv6IfDnsLifeTime)
{
    UNUSED_PARAM (i4FsMIIpv6IfRaRDNSSIndex);
    UNUSED_PARAM (i4SetValFsMIIpv6IfDnsLifeTime);
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetFsMIIpv6IfRaRDNSSAddrOneLife
 Input       :  The Indices
                FsMIIpv6IfRaRDNSSIndex

                The Object
                setValFsMIIpv6IfRaRDNSSAddrOneLife
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
nmhSetFsMIIpv6IfRaRDNSSAddrOneLife(INT4 i4FsMIIpv6IfRaRDNSSIndex , 
                                   UINT4 u4SetValFsMIIpv6IfRaRDNSSAddrOneLife)
{
    UNUSED_PARAM (i4FsMIIpv6IfRaRDNSSIndex);
    UNUSED_PARAM (u4SetValFsMIIpv6IfRaRDNSSAddrOneLife);
    return SNMP_FAILURE;
}
/****************************************************************************
 Function    :  nmhSetFsMIIpv6IfRaRDNSSAddrTwoLife
 Input       :  The Indices
                FsMIIpv6IfRaRDNSSIndex

                The Object
                setValFsMIIpv6IfRaRDNSSAddrTwoLife
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
nmhSetFsMIIpv6IfRaRDNSSAddrTwoLife(INT4 i4FsMIIpv6IfRaRDNSSIndex , 
                                   UINT4 u4SetValFsMIIpv6IfRaRDNSSAddrTwoLife)
{
    UNUSED_PARAM (i4FsMIIpv6IfRaRDNSSIndex);
    UNUSED_PARAM (u4SetValFsMIIpv6IfRaRDNSSAddrTwoLife);
    return SNMP_FAILURE;
}
/****************************************************************************
 Function    :  nmhSetFsMIIpv6IfRaRDNSSAddrThreeLife
 Input       :  The Indices
                FsMIIpv6IfRaRDNSSIndex

                The Object
                setValFsMIIpv6IfRaRDNSSAddrThreeLife
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
nmhSetFsMIIpv6IfRaRDNSSAddrThreeLife(INT4 i4FsMIIpv6IfRaRDNSSIndex , 
                                     UINT4 u4SetValFsMIIpv6IfRaRDNSSAddrThreeLife)
{
    UNUSED_PARAM (i4FsMIIpv6IfRaRDNSSIndex);
    UNUSED_PARAM (u4SetValFsMIIpv6IfRaRDNSSAddrThreeLife);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsMIIpv6IfDomainNameOneLife
 Input       :  The Indices
                FsMIIpv6IfRaRDNSSIndex

                The Object
                setValFsMIIpv6IfRaRDNSLife
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
nmhSetFsMIIpv6IfDomainNameOneLife   (INT4 i4FsMIIpv6IfRaRDNSSIndex , 
                                     INT4 u4SetValFsMIIpv6IfRaRDNSLife)
{
    UNUSED_PARAM (i4FsMIIpv6IfRaRDNSSIndex);
    UNUSED_PARAM (u4SetValFsMIIpv6IfRaRDNSLife);
    return SNMP_FAILURE;
}
/****************************************************************************
 Function    :  nmhSetFsMIIpv6IfDomainNameTwoLife
 Input       :  The Indices
                FsMIIpv6IfRaRDNSSIndex

                The Object
                setValFsMIIpv6IfRaRDNSLife
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
nmhSetFsMIIpv6IfDomainNameTwoLife   (INT4 i4FsMIIpv6IfRaRDNSSIndex , 
                                     INT4 u4SetValFsMIIpv6IfRaRDNSLife)
{
    UNUSED_PARAM (i4FsMIIpv6IfRaRDNSSIndex);
    UNUSED_PARAM (u4SetValFsMIIpv6IfRaRDNSLife);
    return SNMP_FAILURE;
}
/****************************************************************************
 Function    :  nmhSetFsMIIpv6IfDomainNameThreeLife
 Input       :  The Indices
                FsMIIpv6IfRaRDNSSIndex

                The Object
                setValFsMIIpv6IfRaRDNSLife
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
nmhSetFsMIIpv6IfDomainNameThreeLife   (INT4 i4FsMIIpv6IfRaRDNSSIndex , 
                                     INT4 u4SetValFsMIIpv6IfRaRDNSLife)
{
    UNUSED_PARAM (i4FsMIIpv6IfRaRDNSSIndex);
    UNUSED_PARAM (u4SetValFsMIIpv6IfRaRDNSLife);
    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6IfRadvRDNSSOpen
 Input       :  The Indices
                FsMIIpv6IfRaRDNSSIndex

                The Object
                testValFsMIIpv6IfRadvRDNSSOpen
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpv6IfRadvRDNSSOpen (UINT4 *pu4ErrorCode,
                                  INT4 i4FsMIIpv6IfRaRDNSSIndex,
                                  INT4 i4TestValFsMIIpv6IfRadvRDNSSOpen)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4FsMIIpv6IfRaRDNSSIndex);
    UNUSED_PARAM (i4TestValFsMIIpv6IfRadvRDNSSOpen);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6IfRaRDNSSPreference
 Input       :  The Indices
                FsMIIpv6IfRaRDNSSIndex

                The Object
                testValFsMIIpv6IfRaRDNSSPreference
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpv6IfRaRDNSSPreference (UINT4 *pu4ErrorCode,
                                      INT4 i4FsMIIpv6IfRaRDNSSIndex,
                                      INT4 i4TestValFsMIIpv6IfRaRDNSSPreference)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4FsMIIpv6IfRaRDNSSIndex);
    UNUSED_PARAM (i4TestValFsMIIpv6IfRaRDNSSPreference);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6IfRaRDNSSLifetime
 Input       :  The Indices
                FsMIIpv6IfRaRDNSSIndex

                The Object
                testValFsMIIpv6IfRaRDNSSLifetime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpv6IfRaRDNSSLifetime (UINT4 *pu4ErrorCode,
                                    INT4 i4FsMIIpv6IfRaRDNSSIndex,
                                    UINT4 u4TestValFsMIIpv6IfRaRDNSSLifetime)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4FsMIIpv6IfRaRDNSSIndex);
    UNUSED_PARAM (u4TestValFsMIIpv6IfRaRDNSSLifetime);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6IfRaRDNSSAddrOne
 Input       :  The Indices
                FsMIIpv6IfRaRDNSSIndex

                The Object
                testValFsMIIpv6IfRaRDNSSAddrOne
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpv6IfRaRDNSSAddrOne (UINT4 *pu4ErrorCode,
                                   INT4 i4FsMIIpv6IfRaRDNSSIndex,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pTestValFsMIIpv6IfRaRDNSSAddrOne)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4FsMIIpv6IfRaRDNSSIndex);
    UNUSED_PARAM (pTestValFsMIIpv6IfRaRDNSSAddrOne);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6IfRaRDNSSAddrTwo
 Input       :  The Indices
                FsMIIpv6IfRaRDNSSIndex

                The Object
                testValFsMIIpv6IfRaRDNSSAddrTwo
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpv6IfRaRDNSSAddrTwo (UINT4 *pu4ErrorCode,
                                   INT4 i4FsMIIpv6IfRaRDNSSIndex,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pTestValFsMIIpv6IfRaRDNSSAddrTwo)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4FsMIIpv6IfRaRDNSSIndex);
    UNUSED_PARAM (pTestValFsMIIpv6IfRaRDNSSAddrTwo);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6IfRaRDNSSAddrThree
 Input       :  The Indices
                FsMIIpv6IfRaRDNSSIndex

                The Object
                testValFsMIIpv6IfRaRDNSSAddrThree
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpv6IfRaRDNSSAddrThree (UINT4 *pu4ErrorCode,
                                     INT4 i4FsMIIpv6IfRaRDNSSIndex,
                                tSNMP_OCTET_STRING_TYPE *
                                     pTestValFsMIIpv6IfRaRDNSSAddrThree)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4FsMIIpv6IfRaRDNSSIndex);
    UNUSED_PARAM (pTestValFsMIIpv6IfRaRDNSSAddrThree);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6IfRaRDNSSRowStatus
 Input       :  The Indices
                FsMIIpv6IfRaRDNSSIndex

                The Object
                testValFsMIIpv6IfRaRDNSSRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpv6IfRaRDNSSRowStatus (UINT4 *pu4ErrorCode,
                                     INT4 i4FsMIIpv6IfRaRDNSSIndex,
                                     INT4 i4TestValFsMIIpv6IfRaRDNSSRowStatus)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4FsMIIpv6IfRaRDNSSIndex);
    UNUSED_PARAM (i4TestValFsMIIpv6IfRaRDNSSRowStatus);
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6IfDomainNameOne
 Input       :  The Indices
                FsMIIpv6IfRaRDNSSIndex

                The Object
                testValFsMIIpv6IfDomainNameOne
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpv6IfDomainNameOne (UINT4 *pu4ErrorCode,
                                  INT4 i4FsMIIpv6IfRaRDNSSIndex,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pTestValFsMIIpv6IfDomainNameOne)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4FsMIIpv6IfRaRDNSSIndex);
    UNUSED_PARAM (pTestValFsMIIpv6IfDomainNameOne);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6IfDomainNameTwo
 Input       :  The Indices
                FsMIIpv6IfRaRDNSSIndex

                The Object
                testValFsMIIpv6IfDomainNameTwo
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpv6IfDomainNameTwo (UINT4 *pu4ErrorCode,
                                  INT4 i4FsMIIpv6IfRaRDNSSIndex,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pTestValFsMIIpv6IfDomainNameTwo)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4FsMIIpv6IfRaRDNSSIndex);
    UNUSED_PARAM (pTestValFsMIIpv6IfDomainNameTwo);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6IfDomainNameThree
 Input       :  The Indices
                FsMIIpv6IfRaRDNSSIndex

                The Object
                testValFsMIIpv6IfDomainNameThree
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpv6IfDomainNameThree (UINT4 *pu4ErrorCode,
                                  INT4 i4FsMIIpv6IfRaRDNSSIndex,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pTestValFsMIIpv6IfDomainNameThree)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4FsMIIpv6IfRaRDNSSIndex);
    UNUSED_PARAM (pTestValFsMIIpv6IfDomainNameThree);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6IfDnsLifeTime
 Input       :  The Indices
                FsMIIpv6IfRaRDNSSIndex

                The Object
                testValFsMIIpv6IfDnsLifeTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpv6IfDnsLifeTime (UINT4 *pu4ErrorCode,
                                      INT4 i4FsMIIpv6IfRaRDNSSIndex,
                                INT4 i4TestValFsMIIpv6IfDnsLifeTime)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4FsMIIpv6IfRaRDNSSIndex);
    UNUSED_PARAM (i4TestValFsMIIpv6IfDnsLifeTime);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6IfRaRDNSSAddrOneLife
 Input       :  The Indices
                FsMIIpv6IfRaRDNSSIndex

                The Object
                testValFsMIIpv6IfRaRDNSSAddrOneLife
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpv6IfRaRDNSSAddrOneLife(UINT4 *pu4ErrorCode , INT4 i4FsMIIpv6IfRaRDNSSIndex , 
                                      UINT4 u4TestValFsMIIpv6IfRaRDNSSAddrOneLife)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4FsMIIpv6IfRaRDNSSIndex);
    UNUSED_PARAM (u4TestValFsMIIpv6IfRaRDNSSAddrOneLife);
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6IfRaRDNSSAddrTwoLife
 Input       :  The Indices
                FsMIIpv6IfRaRDNSSIndex

                The Object
                testValFsMIIpv6IfRaRDNSSAddrTwoLife
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpv6IfRaRDNSSAddrTwoLife(UINT4 *pu4ErrorCode , INT4 i4FsMIIpv6IfRaRDNSSIndex , 
                                      UINT4 u4TestValFsMIIpv6IfRaRDNSSAddrTwoLife)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4FsMIIpv6IfRaRDNSSIndex);
    UNUSED_PARAM (u4TestValFsMIIpv6IfRaRDNSSAddrTwoLife);
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6IfRaRDNSSAddrThreeLife
 Input       :  The Indices
                FsMIIpv6IfRaRDNSSIndex

                The Object
                testValFsMIIpv6IfRaRDNSSAddrThreeLife
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpv6IfRaRDNSSAddrThreeLife(UINT4 *pu4ErrorCode , INT4 i4FsMIIpv6IfRaRDNSSIndex , 
                                        UINT4 u4TestValFsMIIpv6IfRaRDNSSAddrThreeLife)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4FsMIIpv6IfRaRDNSSIndex);
    UNUSED_PARAM (u4TestValFsMIIpv6IfRaRDNSSAddrThreeLife);
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6IfDomainNameOneLife
 Input       :  The Indices
                FsMIIpv6IfRaRDNSSIndex

                The Object
                testValFsMIIpv6IfRaRDNSLife
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpv6IfDomainNameOneLife (UINT4 *pu4ErrorCode , INT4 i4FsMIIpv6IfRaRDNSSIndex , 
                                      INT4 u4TestValFsMIIpv6IfRaRDNSLife)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4FsMIIpv6IfRaRDNSSIndex);
    UNUSED_PARAM (u4TestValFsMIIpv6IfRaRDNSLife);
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6IfDomainNameTwoLife
 Input       :  The Indices
                FsMIIpv6IfRaRDNSSIndex

                The Object
                testValFsMIIpv6IfRaRDNSLife
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
nmhTestv2FsMIIpv6IfDomainNameTwoLife (UINT4 *pu4ErrorCode , INT4 i4FsMIIpv6IfRaRDNSSIndex , 
                                      INT4 u4TestValFsMIIpv6IfRaRDNSLife)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4FsMIIpv6IfRaRDNSSIndex);
    UNUSED_PARAM (u4TestValFsMIIpv6IfRaRDNSLife);
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6IfDomainNameThreeLife
 Input       :  The Indices
                FsMIIpv6IfRaRDNSSIndex

                The Object
                testValFsMIIpv6IfRaRDNSLife
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpv6IfDomainNameThreeLife (UINT4 *pu4ErrorCode , INT4 i4FsMIIpv6IfRaRDNSSIndex , 
                                      INT4 u4TestValFsMIIpv6IfRaRDNSLife)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4FsMIIpv6IfRaRDNSSIndex);
    UNUSED_PARAM (u4TestValFsMIIpv6IfRaRDNSLife);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIIpv6IfRaRDNSSTable
 Input       :  The Indices
                FsMIIpv6IfRaRDNSSIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIIpv6IfRaRDNSSTable (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIIpv6RARouteInfoTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIIpv6RARouteInfoTable
 Input       :  The Indices
                FsMIIpv6RARouteIfIndex
                FsMIIpv6RARoutePrefix
                FsMIIpv6RARoutePrefixLen
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 
nmhValidateIndexInstanceFsMIIpv6RARouteInfoTable
                           (INT4 i4FsMIIpv6RARouteIfIndex, 
                            tSNMP_OCTET_STRING_TYPE *pFsMIIpv6RARoutePrefix, 
                            INT4 i4FsMIIpv6RARoutePrefixLen)
{
    INT1 i1RetVal = SNMP_FAILURE;
    i1RetVal = nmhValidateIndexInstanceFsipv6RARouteInfoTable(i4FsMIIpv6RARouteIfIndex,
                                               pFsMIIpv6RARoutePrefix,
                                               i4FsMIIpv6RARoutePrefixLen);
    return (i1RetVal);
}
/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIIpv6RARouteInfoTable
 Input       :  The Indices
                FsMIIpv6RARouteIfIndex
                FsMIIpv6RARoutePrefix
                FsMIIpv6RARoutePrefixLen
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1 
nmhGetFirstIndexFsMIIpv6RARouteInfoTable(INT4 *pi4FsMIIpv6RARouteIfIndex, 
                            tSNMP_OCTET_STRING_TYPE * pFsMIIpv6RARoutePrefix, 
                            INT4 *pi4FsMIIpv6RARoutePrefixLen)
{
     INT1 i1RetVal = SNMP_FAILURE;
     i1RetVal = nmhGetFirstIndexFsipv6RARouteInfoTable(pi4FsMIIpv6RARouteIfIndex,
                                                       pFsMIIpv6RARoutePrefix,
                                                       pi4FsMIIpv6RARoutePrefixLen);
     return (i1RetVal);
}
/****************************************************************************
 Function    :  nmhGetNextIndexFsMIIpv6RARouteInfoTable
 Input       :  The Indices
                FsMIIpv6RARouteIfIndex
                nextFsMIIpv6RARouteIfIndex
                FsMIIpv6RARoutePrefix
                nextFsMIIpv6RARoutePrefix
                FsMIIpv6RARoutePrefixLen
                nextFsMIIpv6RARoutePrefixLen
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1 
nmhGetNextIndexFsMIIpv6RARouteInfoTable(INT4 i4FsMIIpv6RARouteIfIndex,
                             INT4 *pi4NextFsMIIpv6RARouteIfIndex, 
                             tSNMP_OCTET_STRING_TYPE *pFsMIIpv6RARoutePrefix,
                             tSNMP_OCTET_STRING_TYPE * pNextFsMIIpv6RARoutePrefix, 
                             INT4 i4FsMIIpv6RARoutePrefixLen,
                             INT4 *pi4NextFsMIIpv6RARoutePrefixLen )
{
     INT1 i1RetVal = SNMP_FAILURE;
     i1RetVal = nmhGetNextIndexFsipv6RARouteInfoTable(i4FsMIIpv6RARouteIfIndex,
                                               pi4NextFsMIIpv6RARouteIfIndex,
                                               pFsMIIpv6RARoutePrefix,
                                               pNextFsMIIpv6RARoutePrefix,
                                               i4FsMIIpv6RARoutePrefixLen,
                                               pi4NextFsMIIpv6RARoutePrefixLen);
     return (i1RetVal);
}
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIIpv6RARoutePreference
 Input       :  The Indices
                FsMIIpv6RARouteIfIndex
                FsMIIpv6RARoutePrefix
                FsMIIpv6RARoutePrefixLen

                The Object 
                retValFsMIIpv6RARoutePreference
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
nmhGetFsMIIpv6RARoutePreference(INT4 i4FsMIIpv6RARouteIfIndex, 
                                tSNMP_OCTET_STRING_TYPE *pFsMIIpv6RARoutePrefix, 
                                INT4 i4FsMIIpv6RARoutePrefixLen, 
                                INT4 *pi4RetValFsMIIpv6RARoutePreference)
{
     INT1 i1RetVal = SNMP_FAILURE;
     i1RetVal = nmhGetFsipv6RARoutePreference(i4FsMIIpv6RARouteIfIndex,
                                              pFsMIIpv6RARoutePrefix,
                                              i4FsMIIpv6RARoutePrefixLen,
                                              pi4RetValFsMIIpv6RARoutePreference);
     return (i1RetVal);
}
/****************************************************************************
 Function    :  nmhGetFsMIIpv6RARouteLifetime
 Input       :  The Indices
                FsMIIpv6RARouteIfIndex
                FsMIIpv6RARoutePrefix
                FsMIIpv6RARoutePrefixLen

                The Object 
                retValFsMIIpv6RARouteLifetime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
nmhGetFsMIIpv6RARouteLifetime(INT4 i4FsMIIpv6RARouteIfIndex, 
                       tSNMP_OCTET_STRING_TYPE *pFsMIIpv6RARoutePrefix, 
                       INT4 i4FsMIIpv6RARoutePrefixLen, 
                       UINT4 *pu4RetValFsMIIpv6RARouteLifetime)
{
     INT1 i1RetVal = SNMP_FAILURE;
     i1RetVal = nmhGetFsipv6RARouteLifetime(i4FsMIIpv6RARouteIfIndex,
                                            pFsMIIpv6RARoutePrefix,
                                            i4FsMIIpv6RARoutePrefixLen,
                                            pu4RetValFsMIIpv6RARouteLifetime);
     return (i1RetVal);
                                              
}
/****************************************************************************
 Function    :  nmhGetFsMIIpv6RARouteRowStatus
 Input       :  The Indices
                FsMIIpv6RARouteIfIndex
                FsMIIpv6RARoutePrefix
                FsMIIpv6RARoutePrefixLen

                The Object 
                retValFsMIIpv6RARouteRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
nmhGetFsMIIpv6RARouteRowStatus(INT4 i4FsMIIpv6RARouteIfIndex, 
                               tSNMP_OCTET_STRING_TYPE *pFsMIIpv6RARoutePrefix, 
                               INT4 i4FsMIIpv6RARoutePrefixLen, 
                               INT4 *pi4RetValFsMIIpv6RARouteRowStatus)
{
     INT1 i1RetVal = SNMP_FAILURE;
     i1RetVal = nmhGetFsipv6RARouteRowStatus(i4FsMIIpv6RARouteIfIndex,
                                             pFsMIIpv6RARoutePrefix,
                                             i4FsMIIpv6RARoutePrefixLen,
                                             pi4RetValFsMIIpv6RARouteRowStatus);
     return (i1RetVal);
}
/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIIpv6RARoutePreference
 Input       :  The Indices
                FsMIIpv6RARouteIfIndex
                FsMIIpv6RARoutePrefix
                FsMIIpv6RARoutePrefixLen

                The Object 
                setValFsMIIpv6RARoutePreference
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
nmhSetFsMIIpv6RARoutePreference(INT4 i4FsMIIpv6RARouteIfIndex, 
                                tSNMP_OCTET_STRING_TYPE *pFsMIIpv6RARoutePrefix, 
                                INT4 i4FsMIIpv6RARoutePrefixLen, 
                                INT4 i4SetValFsMIIpv6RARoutePreference)
{
     INT1 i1RetVal = SNMP_FAILURE;
     i1RetVal = nmhSetFsipv6RARoutePreference(i4FsMIIpv6RARouteIfIndex,
                                              pFsMIIpv6RARoutePrefix,
                                              i4FsMIIpv6RARoutePrefixLen,
                                              i4SetValFsMIIpv6RARoutePreference);
     return (i1RetVal); 
}
/****************************************************************************
 Function    :  nmhSetFsMIIpv6RARouteLifetime
 Input       :  The Indices
                FsMIIpv6RARouteIfIndex
                FsMIIpv6RARoutePrefix
                FsMIIpv6RARoutePrefixLen

                The Object 
                setValFsMIIpv6RARouteLifetime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
nmhSetFsMIIpv6RARouteLifetime(INT4 i4FsMIIpv6RARouteIfIndex, 
                              tSNMP_OCTET_STRING_TYPE *pFsMIIpv6RARoutePrefix, 
                              INT4 i4FsMIIpv6RARoutePrefixLen, 
                              UINT4 u4SetValFsMIIpv6RARouteLifetime)
{
      INT1 i1RetVal = SNMP_FAILURE;
      i1RetVal = nmhSetFsipv6RARouteLifetime(i4FsMIIpv6RARouteIfIndex,   
                                             pFsMIIpv6RARoutePrefix,
                                             i4FsMIIpv6RARoutePrefixLen,
                                             u4SetValFsMIIpv6RARouteLifetime);
      return (i1RetVal);
}
/****************************************************************************
 Function    :  nmhSetFsMIIpv6RARouteRowStatus
 Input       :  The Indices
                FsMIIpv6RARouteIfIndex
                FsMIIpv6RARoutePrefix
                FsMIIpv6RARoutePrefixLen

                The Object 
                setValFsMIIpv6RARouteRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
nmhSetFsMIIpv6RARouteRowStatus(INT4 i4FsMIIpv6RARouteIfIndex, 
                               tSNMP_OCTET_STRING_TYPE *pFsMIIpv6RARoutePrefix, 
                               INT4 i4FsMIIpv6RARoutePrefixLen, 
                               INT4 i4SetValFsMIIpv6RARouteRowStatus)
{
     INT1 i1RetVal = SNMP_FAILURE;
     i1RetVal = nmhSetFsipv6RARouteRowStatus(i4FsMIIpv6RARouteIfIndex,
                                             pFsMIIpv6RARoutePrefix,
                                             i4FsMIIpv6RARoutePrefixLen,
                                             i4SetValFsMIIpv6RARouteRowStatus);
     return (i1RetVal);
}
/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6RARoutePreference
 Input       :  The Indices
                FsMIIpv6RARouteIfIndex
                FsMIIpv6RARoutePrefix
                FsMIIpv6RARoutePrefixLen

                The Object 
                testValFsMIIpv6RARoutePreference
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
nmhTestv2FsMIIpv6RARoutePreference(UINT4 *pu4ErrorCode, 
                                   INT4 i4FsMIIpv6RARouteIfIndex, 
                                   tSNMP_OCTET_STRING_TYPE *pFsMIIpv6RARoutePrefix, 
                                   INT4 i4FsMIIpv6RARoutePrefixLen, 
                                   INT4 i4TestValFsMIIpv6RARoutePreference)
{
     INT1 i1RetVal = SNMP_FAILURE;
     i1RetVal = nmhTestv2Fsipv6RARoutePreference(pu4ErrorCode,
                                                 i4FsMIIpv6RARouteIfIndex,
                                                 pFsMIIpv6RARoutePrefix,
                                                 i4FsMIIpv6RARoutePrefixLen,
                                                 i4TestValFsMIIpv6RARoutePreference);
     return (i1RetVal);
}
/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6RARouteLifetime
 Input       :  The Indices
                FsMIIpv6RARouteIfIndex
                FsMIIpv6RARoutePrefix
                FsMIIpv6RARoutePrefixLen

                The Object 
                testValFsMIIpv6RARouteLifetime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
nmhTestv2FsMIIpv6RARouteLifetime(UINT4 *pu4ErrorCode, 
                                 INT4 i4FsMIIpv6RARouteIfIndex, 
                                 tSNMP_OCTET_STRING_TYPE *pFsMIIpv6RARoutePrefix, 
                                 INT4 i4FsMIIpv6RARoutePrefixLen, 
                                 UINT4 u4TestValFsMIIpv6RARouteLifetime)
{
     INT1 i1RetVal = SNMP_FAILURE;
     i1RetVal = nmhTestv2Fsipv6RARouteLifetime (pu4ErrorCode,
                                                i4FsMIIpv6RARouteIfIndex,
                                                pFsMIIpv6RARoutePrefix,
                                                i4FsMIIpv6RARoutePrefixLen,
                                                u4TestValFsMIIpv6RARouteLifetime);
     return (i1RetVal);
}
/****************************************************************************
 Function    :  nmhTestv2FsMIIpv6RARouteRowStatus
 Input       :  The Indices
                FsMIIpv6RARouteIfIndex
                FsMIIpv6RARoutePrefix
                FsMIIpv6RARoutePrefixLen

                The Object 
                testValFsMIIpv6RARouteRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
nmhTestv2FsMIIpv6RARouteRowStatus(UINT4 *pu4ErrorCode, 
                                  INT4 i4FsMIIpv6RARouteIfIndex, 
                                  tSNMP_OCTET_STRING_TYPE *pFsMIIpv6RARoutePrefix, 
                                  INT4 i4FsMIIpv6RARoutePrefixLen, 
                                  INT4 i4TestValFsMIIpv6RARouteRowStatus)
{
     INT1 i1RetVal = SNMP_FAILURE;
     i1RetVal = nmhTestv2Fsipv6RARouteRowStatus(pu4ErrorCode,
                                                i4FsMIIpv6RARouteIfIndex,
                                                pFsMIIpv6RARoutePrefix,
                                                i4FsMIIpv6RARoutePrefixLen,
                                                i4TestValFsMIIpv6RARouteRowStatus);
    return (i1RetVal);
}
/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIIpv6RARouteInfoTable
 Input       :  The Indices
                FsMIIpv6RARouteIfIndex
                FsMIIpv6RARoutePrefix
                FsMIIpv6RARoutePrefixLen
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FsMIIpv6RARouteInfoTable(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}
