/*****************************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: ipv6npapi.c,v 1.8 2017/11/14 07:31:14 siva Exp $
 *
 * Description: This file contains function for invoking NP calls of IPv6 module.
 ******************************************************************************/

#ifndef __IPV6_NPAPI_C__
#define __IPV6_NPAPI_C__

#include "nputil.h"

/***************************************************************************
 *                                                                          
 *    Function Name       : Ipv6FsNpIpv6Init                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv6Init
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv6Init
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
Ipv6FsNpIpv6Init ()
{
    tFsHwNp             FsHwNp;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IPV6_MOD,    /* Module ID */
                         FS_NP_IPV6_INIT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : Ipv6FsNpIpv6Deinit                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv6Deinit
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv6Deinit
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
Ipv6FsNpIpv6Deinit ()
{
    tFsHwNp             FsHwNp;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IPV6_MOD,    /* Module ID */
                         FS_NP_IPV6_DEINIT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : Ipv6FsNpIpv6NeighCacheAdd                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv6NeighCacheAdd
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv6NeighCacheAdd
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
Ipv6FsNpIpv6NeighCacheAdd (UINT4 u4VrId, UINT1 *pu1Ip6Addr, UINT4 u4IfIndex,
                           UINT1 *pu1HwAddr, UINT1 u1HwAddrLen,
                           UINT1 u1ReachStatus, UINT2 u2VlanId)
{
    tFsHwNp             FsHwNp;
    tIpv6NpModInfo     *pIpv6NpModInfo = NULL;
    tIpv6NpWrFsNpIpv6NeighCacheAdd *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IPV6_MOD,    /* Module ID */
                         FS_NP_IPV6_NEIGH_CACHE_ADD,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpv6NpModInfo = &(FsHwNp.Ipv6NpModInfo);
    pEntry = &pIpv6NpModInfo->Ipv6NpFsNpIpv6NeighCacheAdd;

    pEntry->u4VrId = u4VrId;
    pEntry->pu1Ip6Addr = pu1Ip6Addr;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->pu1HwAddr = pu1HwAddr;
    pEntry->u1HwAddrLen = u1HwAddrLen;
    pEntry->u1ReachStatus = u1ReachStatus;
    pEntry->u2VlanId = u2VlanId;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : Ipv6FsNpIpv6NeighCacheDel                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv6NeighCacheDel
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv6NeighCacheDel
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
Ipv6FsNpIpv6NeighCacheDel (UINT4 u4VrId, UINT1 *pu1Ip6Addr, UINT4 u4IfIndex)
{
    tFsHwNp             FsHwNp;
    tIpv6NpModInfo     *pIpv6NpModInfo = NULL;
    tIpv6NpWrFsNpIpv6NeighCacheDel *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IPV6_MOD,    /* Module ID */
                         FS_NP_IPV6_NEIGH_CACHE_DEL,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpv6NpModInfo = &(FsHwNp.Ipv6NpModInfo);
    pEntry = &pIpv6NpModInfo->Ipv6NpFsNpIpv6NeighCacheDel;

    pEntry->u4VrId = u4VrId;
    pEntry->pu1Ip6Addr = pu1Ip6Addr;
    pEntry->u4IfIndex = u4IfIndex;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : Ipv6FsNpIpv6UcRouteAdd                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv6UcRouteAdd
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv6UcRouteAdd
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
Ipv6FsNpIpv6UcRouteAdd (UINT4 u4VrId, UINT1 *pu1Ip6Prefix, UINT1 u1PrefixLen,
                        UINT1 *pu1NextHop, UINT4 u4NHType,
                        tFsNpIntInfo * pIntInfo)
{
    tFsHwNp             FsHwNp;
    tIpv6NpModInfo     *pIpv6NpModInfo = NULL;
    tIpv6NpWrFsNpIpv6UcRouteAdd *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IPV6_MOD,    /* Module ID */
                         FS_NP_IPV6_UC_ROUTE_ADD,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpv6NpModInfo = &(FsHwNp.Ipv6NpModInfo);
    pEntry = &pIpv6NpModInfo->Ipv6NpFsNpIpv6UcRouteAdd;

    pEntry->u4VrId = u4VrId;
    pEntry->pu1Ip6Prefix = pu1Ip6Prefix;
    pEntry->u1PrefixLen = u1PrefixLen;
    pEntry->pu1NextHop = pu1NextHop;
    pEntry->u4NHType = u4NHType;
    pEntry->pIntInfo = pIntInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : Ipv6FsNpIpv6UcRouteDelete                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv6UcRouteDelete
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv6UcRouteDelete
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
Ipv6FsNpIpv6UcRouteDelete (UINT4 u4VrId, UINT1 *pu1Ip6Prefix, UINT1 u1PrefixLen,
                           UINT1 *pu1NextHop, UINT4 u4IfIndex,
                           tFsNpRouteInfo * pRouteInfo)
{
    tFsHwNp             FsHwNp;
    tIpv6NpModInfo     *pIpv6NpModInfo = NULL;
    tIpv6NpWrFsNpIpv6UcRouteDelete *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IPV6_MOD,    /* Module ID */
                         FS_NP_IPV6_UC_ROUTE_DELETE,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpv6NpModInfo = &(FsHwNp.Ipv6NpModInfo);
    pEntry = &pIpv6NpModInfo->Ipv6NpFsNpIpv6UcRouteDelete;

    pEntry->u4VrId = u4VrId;
    pEntry->pu1Ip6Prefix = pu1Ip6Prefix;
    pEntry->u1PrefixLen = u1PrefixLen;
    pEntry->pu1NextHop = pu1NextHop;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->pRouteInfo = pRouteInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : Ipv6FsNpIpv6RtPresentInFastPath                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv6RtPresentInFastPath
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv6RtPresentInFastPath
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
Ipv6FsNpIpv6RtPresentInFastPath (UINT4 u4VrId, UINT1 *pu1Ip6Prefix,
                                 UINT1 u1PrefixLen, UINT1 *pu1NextHop,
                                 UINT4 u4IfIndex)
{
    tFsHwNp             FsHwNp;
    tIpv6NpModInfo     *pIpv6NpModInfo = NULL;
    tIpv6NpWrFsNpIpv6RtPresentInFastPath *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IPV6_MOD,    /* Module ID */
                         FS_NP_IPV6_RT_PRESENT_IN_FAST_PATH,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpv6NpModInfo = &(FsHwNp.Ipv6NpModInfo);
    pEntry = &pIpv6NpModInfo->Ipv6NpFsNpIpv6RtPresentInFastPath;

    pEntry->u4VrId = u4VrId;
    pEntry->pu1Ip6Prefix = pu1Ip6Prefix;
    pEntry->u1PrefixLen = u1PrefixLen;
    pEntry->pu1NextHop = pu1NextHop;
    pEntry->u4IfIndex = u4IfIndex;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : Ipv6FsNpIpv6UcRouting                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv6UcRouting
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv6UcRouting
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
Ipv6FsNpIpv6UcRouting (UINT4 u4VrId, UINT4 u4RoutingStatus)
{
    tFsHwNp             FsHwNp;
    tIpv6NpModInfo     *pIpv6NpModInfo = NULL;
    tIpv6NpWrFsNpIpv6UcRouting *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IPV6_MOD,    /* Module ID */
                         FS_NP_IPV6_UC_ROUTING,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpv6NpModInfo = &(FsHwNp.Ipv6NpModInfo);
    pEntry = &pIpv6NpModInfo->Ipv6NpFsNpIpv6UcRouting;

    pEntry->u4VrId = u4VrId;
    pEntry->u4RoutingStatus = u4RoutingStatus;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : Ipv6FsNpIpv6GetStats                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv6GetStats
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv6GetStats
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
Ipv6FsNpIpv6GetStats (UINT4 u4VrId, UINT4 u4IfIndex, UINT4 u4StatsFlag,
                      UINT4 *pu4StatsValue)
{
    tFsHwNp             FsHwNp;
    tIpv6NpModInfo     *pIpv6NpModInfo = NULL;
    tIpv6NpWrFsNpIpv6GetStats *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IPV6_MOD,    /* Module ID */
                         FS_NP_IPV6_GET_STATS,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpv6NpModInfo = &(FsHwNp.Ipv6NpModInfo);
    pEntry = &pIpv6NpModInfo->Ipv6NpFsNpIpv6GetStats;

    pEntry->u4VrId = u4VrId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->u4StatsFlag = u4StatsFlag;
    pEntry->pu4StatsValue = pu4StatsValue;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : Ipv6FsNpIpv6IntfStatus                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv6IntfStatus
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv6IntfStatus
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
Ipv6FsNpIpv6IntfStatus (UINT4 u4VrId, UINT4 u4IfStatus,
                        tFsNpIntInfo * pIntfInfo)
{
    tFsHwNp             FsHwNp;
    tIpv6NpModInfo     *pIpv6NpModInfo = NULL;
    tIpv6NpWrFsNpIpv6IntfStatus *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IPV6_MOD,    /* Module ID */
                         FS_NP_IPV6_INTF_STATUS,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpv6NpModInfo = &(FsHwNp.Ipv6NpModInfo);
    pEntry = &pIpv6NpModInfo->Ipv6NpFsNpIpv6IntfStatus;

    pEntry->u4VrId = u4VrId;
    pEntry->u4IfStatus = u4IfStatus;
    pEntry->pIntfInfo = pIntfInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : Ipv6FsNpIpv6AddrCreate                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv6AddrCreate
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv6AddrCreate
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
Ipv6FsNpIpv6AddrCreate (UINT4 u4VrId, UINT4 u4IfIndex, UINT4 u4AddrType,
                        UINT1 *pu1Ip6Addr, UINT1 u1PrefixLen)
{
    tFsHwNp             FsHwNp;
    tIpv6NpModInfo     *pIpv6NpModInfo = NULL;
    tIpv6NpWrFsNpIpv6AddrCreate *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IPV6_MOD,    /* Module ID */
                         FS_NP_IPV6_ADDR_CREATE,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpv6NpModInfo = &(FsHwNp.Ipv6NpModInfo);
    pEntry = &pIpv6NpModInfo->Ipv6NpFsNpIpv6AddrCreate;

    pEntry->u4VrId = u4VrId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->u4AddrType = u4AddrType;
    pEntry->pu1Ip6Addr = pu1Ip6Addr;
    pEntry->u1PrefixLen = u1PrefixLen;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : Ipv6FsNpIpv6AddrDelete                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv6AddrDelete
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv6AddrDelete
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
Ipv6FsNpIpv6AddrDelete (UINT4 u4VrId, UINT4 u4IfIndex, UINT4 u4AddrType,
                        UINT1 *pu1Ip6Addr, UINT1 u1PrefixLen)
{
    tFsHwNp             FsHwNp;
    tIpv6NpModInfo     *pIpv6NpModInfo = NULL;
    tIpv6NpWrFsNpIpv6AddrDelete *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IPV6_MOD,    /* Module ID */
                         FS_NP_IPV6_ADDR_DELETE,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpv6NpModInfo = &(FsHwNp.Ipv6NpModInfo);
    pEntry = &pIpv6NpModInfo->Ipv6NpFsNpIpv6AddrDelete;

    pEntry->u4VrId = u4VrId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->u4AddrType = u4AddrType;
    pEntry->pu1Ip6Addr = pu1Ip6Addr;
    pEntry->u1PrefixLen = u1PrefixLen;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : Ipv6FsNpIpv6TunlParamSet                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv6TunlParamSet
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv6TunlParamSet
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
Ipv6FsNpIpv6TunlParamSet (UINT4 u4VrId, UINT4 u4IfIndex, UINT4 u4TunlType,
                          UINT4 u4SrcAddr, UINT4 u4DstAddr)
{
    tFsHwNp             FsHwNp;
    tIpv6NpModInfo     *pIpv6NpModInfo = NULL;
    tIpv6NpWrFsNpIpv6TunlParamSet *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IPV6_MOD,    /* Module ID */
                         FS_NP_IPV6_TUNL_PARAM_SET,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpv6NpModInfo = &(FsHwNp.Ipv6NpModInfo);
    pEntry = &pIpv6NpModInfo->Ipv6NpFsNpIpv6TunlParamSet;

    pEntry->u4VrId = u4VrId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->u4TunlType = u4TunlType;
    pEntry->u4SrcAddr = u4SrcAddr;
    pEntry->u4DstAddr = u4DstAddr;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *
 *    Function Name       : Ipv6FsNpIpv6Enable
 *
 *    Description         : This function using its arguments populates the
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv6Enable
 *
 *    Input(s)            : Arguments of FsNpIpv6Enable
 *
 *    Output(s)           : None
 *
 *    Global Var Referred : None
 *
 *    Global Var Modified : None.
 *
 *    Use of Recursion    : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE
 *
 *****************************************************************************/

UINT1
Ipv6FsNpIpv6Enable (UINT4 u4IfIndex, UINT2 u2VlanId)
{
    tFsHwNp             FsHwNp;
    tIpv6NpModInfo     *pIpv6NpModInfo = NULL;
    tIpv6NpWrFsNpIpv6Enable *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IPV6_MOD,    /* Module ID */
                         FS_NP_IPV6_ENABLE,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpv6NpModInfo = &(FsHwNp.Ipv6NpModInfo);
    pEntry = &pIpv6NpModInfo->Ipv6NpFsNpIpv6Enable;

    pEntry->u2VlanId = u2VlanId;
    pEntry->u4IfIndex = u4IfIndex;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *
 *    Function Name       : Ipv6FsNpIpv6Disable
 *
 *    Description         : This function using its arguments populates the
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv6Disable
 *
 *    Input(s)            : Arguments of FsNpIpv6Disable
 *
 *    Output(s)           : None
 *
 *    Global Var Referred : None
 *
 *    Global Var Modified : None.
 *
 *    Use of Recursion    : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE
 *
 *****************************************************************************/

UINT1
Ipv6FsNpIpv6Disable (UINT4 u4IfIndex, UINT2 u2VlanId)
{
    tFsHwNp             FsHwNp;
    tIpv6NpModInfo     *pIpv6NpModInfo = NULL;
    tIpv6NpWrFsNpIpv6Disable *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IPV6_MOD,    /* Module ID */
                         FS_NP_IPV6_DISABLE,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpv6NpModInfo = &(FsHwNp.Ipv6NpModInfo);
    pEntry = &pIpv6NpModInfo->Ipv6NpFsNpIpv6Disable;

    pEntry->u2VlanId = u2VlanId;
    pEntry->u4IfIndex = u4IfIndex;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : Ipv6FsNpIpv6AddMcastMAC                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv6AddMcastMAC
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv6AddMcastMAC
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
Ipv6FsNpIpv6AddMcastMAC (UINT4 u4VrId, UINT4 u4IfIndex, UINT1 *pu1MacAddr,
                         UINT1 u1MacAddrLen)
{
    tFsHwNp             FsHwNp;
    tIpv6NpModInfo     *pIpv6NpModInfo = NULL;
    tIpv6NpWrFsNpIpv6AddMcastMAC *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IPV6_MOD,    /* Module ID */
                         FS_NP_IPV6_ADD_MCAST_M_A_C,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpv6NpModInfo = &(FsHwNp.Ipv6NpModInfo);
    pEntry = &pIpv6NpModInfo->Ipv6NpFsNpIpv6AddMcastMAC;

    pEntry->u4VrId = u4VrId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->pu1MacAddr = pu1MacAddr;
    pEntry->u1MacAddrLen = u1MacAddrLen;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : Ipv6FsNpIpv6DelMcastMAC                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv6DelMcastMAC
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv6DelMcastMAC
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
Ipv6FsNpIpv6DelMcastMAC (UINT4 u4VrId, UINT4 u4IfIndex, UINT1 *pu1MacAddr,
                         UINT1 u1MacAddrLen)
{
    tFsHwNp             FsHwNp;
    tIpv6NpModInfo     *pIpv6NpModInfo = NULL;
    tIpv6NpWrFsNpIpv6DelMcastMAC *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IPV6_MOD,    /* Module ID */
                         FS_NP_IPV6_DEL_MCAST_M_A_C,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpv6NpModInfo = &(FsHwNp.Ipv6NpModInfo);
    pEntry = &pIpv6NpModInfo->Ipv6NpFsNpIpv6DelMcastMAC;

    pEntry->u4VrId = u4VrId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->pu1MacAddr = pu1MacAddr;
    pEntry->u1MacAddrLen = u1MacAddrLen;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : Ipv6FsNpIpv6HandleFdbMacEntryChange                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv6HandleFdbMacEntryChange
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv6HandleFdbMacEntryChange
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
Ipv6FsNpIpv6HandleFdbMacEntryChange (UINT1 *pu1L3Nexthop, UINT4 u4IfIndex,
                                     UINT1 *pu1MacAddr, UINT4 u4IsRemoved)
{
    tFsHwNp             FsHwNp;
    tIpv6NpModInfo     *pIpv6NpModInfo = NULL;
    tIpv6NpWrFsNpIpv6HandleFdbMacEntryChange *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IPV6_MOD,    /* Module ID */
                         FS_NP_IPV6_HANDLE_FDB_MAC_ENTRY_CHANGE,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpv6NpModInfo = &(FsHwNp.Ipv6NpModInfo);
    pEntry = &pIpv6NpModInfo->Ipv6NpFsNpIpv6HandleFdbMacEntryChange;

    pEntry->pu1L3Nexthop = pu1L3Nexthop;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->pu1MacAddr = pu1MacAddr;
    pEntry->u4IsRemoved = u4IsRemoved;

    return (NpUtilHwProgram (&FsHwNp));
}

#ifdef OSPF3_WANTED

/***************************************************************************
 *                                                                          
 *    Function Name       : Ipv6FsNpOspf3Init                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpOspf3Init
 *                                                                          
 *    Input(s)            : Arguments of FsNpOspf3Init
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
Ipv6FsNpOspf3Init ()
{
    tFsHwNp             FsHwNp;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IPV6_MOD,    /* Module ID */
                         FS_NP_OSPF3_INIT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : Ipv6FsNpOspf3DeInit                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpOspf3DeInit
 *                                                                          
 *    Input(s)            : Arguments of FsNpOspf3DeInit
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
Ipv6FsNpOspf3DeInit ()
{
    tFsHwNp             FsHwNp;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IPV6_MOD,    /* Module ID */
                         FS_NP_OSPF3_DE_INIT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    return (NpUtilHwProgram (&FsHwNp));
}
#endif /* OSPF3_WANTED */
#ifdef RIP6_WANTED

/***************************************************************************
 *                                                                          
 *    Function Name       : Ipv6FsNpRip6Init                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpRip6Init
 *                                                                          
 *    Input(s)            : Arguments of FsNpRip6Init
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
Ipv6FsNpRip6Init ()
{
    tFsHwNp             FsHwNp;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IPV6_MOD,    /* Module ID */
                         FS_NP_RIP6_INIT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    return (NpUtilHwProgram (&FsHwNp));
}
#endif /* RIP6_WANTED */

/***************************************************************************
 *                                                                          
 *    Function Name       : Ipv6FsNpIpv6NeighCacheGet                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv6NeighCacheGet
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv6NeighCacheGet
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
Ipv6FsNpIpv6NeighCacheGet (tNpNDCacheInput Nd6NpInParam,
                           tNpNDCacheOutput * pNd6NpOutParam)
{
    tFsHwNp             FsHwNp;
    tIpv6NpModInfo     *pIpv6NpModInfo = NULL;
    tIpv6NpWrFsNpIpv6NeighCacheGet *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IPV6_MOD,    /* Module ID */
                         FS_NP_IPV6_NEIGH_CACHE_GET,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpv6NpModInfo = &(FsHwNp.Ipv6NpModInfo);
    pEntry = &pIpv6NpModInfo->Ipv6NpFsNpIpv6NeighCacheGet;

    pEntry->Nd6NpInParam = Nd6NpInParam;
    pEntry->pNd6NpOutParam = pNd6NpOutParam;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : Ipv6FsNpIpv6NeighCacheGetNext                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv6NeighCacheGetNext
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv6NeighCacheGetNext
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
Ipv6FsNpIpv6NeighCacheGetNext (tNpNDCacheInput Nd6NpInParam,
                               tNpNDCacheOutput * pNd6NpOutParam)
{
    tFsHwNp             FsHwNp;
    tIpv6NpModInfo     *pIpv6NpModInfo = NULL;
    tIpv6NpWrFsNpIpv6NeighCacheGetNext *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IPV6_MOD,    /* Module ID */
                         FS_NP_IPV6_NEIGH_CACHE_GET_NEXT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpv6NpModInfo = &(FsHwNp.Ipv6NpModInfo);
    pEntry = &pIpv6NpModInfo->Ipv6NpFsNpIpv6NeighCacheGetNext;

    pEntry->Nd6NpInParam = Nd6NpInParam;
    pEntry->pNd6NpOutParam = pNd6NpOutParam;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : Ipv6FsNpIpv6UcGetRoute                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv6UcGetRoute
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv6UcGetRoute
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
Ipv6FsNpIpv6UcGetRoute (tNpRtm6Input Rtm6NpInParam,
                        tNpRtm6Output * pRtm6NpOutParam)
{
    tFsHwNp             FsHwNp;
    tIpv6NpModInfo     *pIpv6NpModInfo = NULL;
    tIpv6NpWrFsNpIpv6UcGetRoute *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IPV6_MOD,    /* Module ID */
                         FS_NP_IPV6_UC_GET_ROUTE,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpv6NpModInfo = &(FsHwNp.Ipv6NpModInfo);
    pEntry = &pIpv6NpModInfo->Ipv6NpFsNpIpv6UcGetRoute;

    pEntry->Rtm6NpInParam = Rtm6NpInParam;
    pEntry->pRtm6NpOutParam = pRtm6NpOutParam;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : Ipv6FsNpIpv6CheckHitOnNDCacheEntry
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv6CheckHitOnNDCacheEntry
 *
 *                          Gets the HitBit status of the ND entry from H/W
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv6CheckHitOnNDCacheEntry
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_FALSE / FNP_TRUE
 *
 *****************************************************************************/

UINT1
Ipv6FsNpIpv6CheckHitOnNDCacheEntry (UINT4 u4VrId, UINT1 *pu1Ip6Addr,
                                    UINT4 u4IfIndex)
{
    tFsHwNp             FsHwNp;
    tIpv6NpModInfo     *pIpv6NpModInfo = NULL;
    tIpv6NpWrFsNpIpv6CheckHitOnNDCacheEntry *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IPV6_MOD,    /* Module ID */
                         FS_NP_IPV6_CHECK_HIT_ON_NDC_ENTRY,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpv6NpModInfo = &(FsHwNp.Ipv6NpModInfo);
    pEntry = &(pIpv6NpModInfo->Ipv6NpFsNpIpv6CheckHitOnNDCacheEntry);
    pEntry->u4VrId = u4VrId;
    pEntry->pu1Ip6Addr = pu1Ip6Addr;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->u1HitBitStatus = 0;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FALSE);
    }
    return (pEntry->u1HitBitStatus);
}

#ifdef MBSM_WANTED

/***************************************************************************
 *                                                                          
 *    Function Name       : Ipv6FsNpMbsmIpv6Init                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpMbsmIpv6Init
 *                                                                          
 *    Input(s)            : Arguments of FsNpMbsmIpv6Init
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
Ipv6FsNpMbsmIpv6Init (tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tIpv6NpModInfo     *pIpv6NpModInfo = NULL;
    tIpv6NpWrFsNpMbsmIpv6Init *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IPV6_MOD,    /* Module ID */
                         FS_NP_MBSM_IPV6_INIT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpv6NpModInfo = &(FsHwNp.Ipv6NpModInfo);
    pEntry = &pIpv6NpModInfo->Ipv6NpFsNpMbsmIpv6Init;

    pEntry->pSlotInfo = pSlotInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

#ifdef RIP6_WANTED

/***************************************************************************
 *                                                                          
 *    Function Name       : Ipv6FsNpMbsmRip6Init                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpMbsmRip6Init
 *                                                                          
 *    Input(s)            : Arguments of FsNpMbsmRip6Init
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
Ipv6FsNpMbsmRip6Init (tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tIpv6NpModInfo     *pIpv6NpModInfo = NULL;
    tIpv6NpWrFsNpMbsmRip6Init *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IPV6_MOD,    /* Module ID */
                         FS_NP_MBSM_RIP6_INIT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpv6NpModInfo = &(FsHwNp.Ipv6NpModInfo);
    pEntry = &pIpv6NpModInfo->Ipv6NpFsNpMbsmRip6Init;

    pEntry->pSlotInfo = pSlotInfo;

    return (NpUtilHwProgram (&FsHwNp));
}
#endif

#ifdef OSPF3_WANTED
/***************************************************************************
 *                                                                          
 *    Function Name       : Ipv6FsNpMbsmOspf3Init                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpMbsmOspf3Init
 *                                                                          
 *    Input(s)            : Arguments of FsNpMbsmOspf3Init
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
Ipv6FsNpMbsmOspf3Init (tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tIpv6NpModInfo     *pIpv6NpModInfo = NULL;
    tIpv6NpWrFsNpMbsmOspf3Init *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IPV6_MOD,    /* Module ID */
                         FS_NP_MBSM_OSPF3_INIT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpv6NpModInfo = &(FsHwNp.Ipv6NpModInfo);
    pEntry = &pIpv6NpModInfo->Ipv6NpFsNpMbsmOspf3Init;

    pEntry->pSlotInfo = pSlotInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

#endif /* OSPF3_WANTED */
/***************************************************************************
 *                                                                          
 *    Function Name       : Ipv6FsNpMbsmIpv6NeighCacheAdd                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpMbsmIpv6NeighCacheAdd
 *                                                                          
 *    Input(s)            : Arguments of FsNpMbsmIpv6NeighCacheAdd
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
Ipv6FsNpMbsmIpv6NeighCacheAdd (UINT4 u4VrId, UINT1 *pu1Ip6Addr, UINT4 u4IfIndex,
                               UINT1 *pu1HwAddr, UINT1 u1HwAddrLen,
                               UINT1 u1ReachStatus, UINT2 u2VlanId,
                               tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tIpv6NpModInfo     *pIpv6NpModInfo = NULL;
    tIpv6NpWrFsNpMbsmIpv6NeighCacheAdd *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IPV6_MOD,    /* Module ID */
                         FS_NP_MBSM_IPV6_NEIGH_CACHE_ADD,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpv6NpModInfo = &(FsHwNp.Ipv6NpModInfo);
    pEntry = &pIpv6NpModInfo->Ipv6NpFsNpMbsmIpv6NeighCacheAdd;

    pEntry->u4VrId = u4VrId;
    pEntry->pu1Ip6Addr = pu1Ip6Addr;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->pu1HwAddr = pu1HwAddr;
    pEntry->u1HwAddrLen = u1HwAddrLen;
    pEntry->u1ReachStatus = u1ReachStatus;
    pEntry->u2VlanId = u2VlanId;
    pEntry->pSlotInfo = pSlotInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : Ipv6FsNpMbsmIpv6NeighCacheDel                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpMbsmIpv6NeighCacheDel
 *                                                                          
 *    Input(s)            : Arguments of FsNpMbsmIpv6NeighCacheDel
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
Ipv6FsNpMbsmIpv6NeighCacheDel (UINT4 u4VrId, UINT1 *pu1Ip6Addr, UINT4 u4IfIndex,
                               tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tIpv6NpModInfo     *pIpv6NpModInfo = NULL;
    tIpv6NpWrFsNpMbsmIpv6NeighCacheDel *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IPV6_MOD,    /* Module ID */
                         FS_NP_MBSM_IPV6_NEIGH_CACHE_DEL,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpv6NpModInfo = &(FsHwNp.Ipv6NpModInfo);
    pEntry = &pIpv6NpModInfo->Ipv6NpFsNpMbsmIpv6NeighCacheDel;

    pEntry->u4VrId = u4VrId;
    pEntry->pu1Ip6Addr = pu1Ip6Addr;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->pSlotInfo = pSlotInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : Ipv6FsNpMbsmIpv6UcRouteAdd                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpMbsmIpv6UcRouteAdd
 *                                                                          
 *    Input(s)            : Arguments of FsNpMbsmIpv6UcRouteAdd
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
Ipv6FsNpMbsmIpv6UcRouteAdd (UINT4 u4VrId, UINT1 *pu1Ip6Prefix,
                            UINT1 u1PrefixLen, UINT1 *pu1NextHop,
                            UINT4 u4NHType, tFsNpIntInfo * pIntInfo,
                            tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tIpv6NpModInfo     *pIpv6NpModInfo = NULL;
    tIpv6NpWrFsNpMbsmIpv6UcRouteAdd *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IPV6_MOD,    /* Module ID */
                         FS_NP_MBSM_IPV6_UC_ROUTE_ADD,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpv6NpModInfo = &(FsHwNp.Ipv6NpModInfo);
    pEntry = &pIpv6NpModInfo->Ipv6NpFsNpMbsmIpv6UcRouteAdd;

    pEntry->u4VrId = u4VrId;
    pEntry->pu1Ip6Prefix = pu1Ip6Prefix;
    pEntry->u1PrefixLen = u1PrefixLen;
    pEntry->pu1NextHop = pu1NextHop;
    pEntry->u4NHType = u4NHType;
    pEntry->pIntInfo = pIntInfo;
    pEntry->pSlotInfo = pSlotInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : Ipv6FsNpMbsmIpv6RtPresentInFastPath                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpMbsmIpv6RtPresentInFastPath
 *                                                                          
 *    Input(s)            : Arguments of FsNpMbsmIpv6RtPresentInFastPath
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
Ipv6FsNpMbsmIpv6RtPresentInFastPath (UINT4 u4VrId, UINT1 *pu1Ip6Prefix,
                                     UINT1 u1PrefixLen, UINT1 *pu1NextHop,
                                     UINT4 u4Index, tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tIpv6NpModInfo     *pIpv6NpModInfo = NULL;
    tIpv6NpWrFsNpMbsmIpv6RtPresentInFastPath *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IPV6_MOD,    /* Module ID */
                         FS_NP_MBSM_IPV6_RT_PRESENT_IN_FAST_PATH,    /* Function/OpCode */
                         u4Index,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpv6NpModInfo = &(FsHwNp.Ipv6NpModInfo);
    pEntry = &pIpv6NpModInfo->Ipv6NpFsNpMbsmIpv6RtPresentInFastPath;

    pEntry->u4VrId = u4VrId;
    pEntry->pu1Ip6Prefix = pu1Ip6Prefix;
    pEntry->u1PrefixLen = u1PrefixLen;
    pEntry->pu1NextHop = pu1NextHop;
    pEntry->u4Index = u4Index;
    pEntry->pSlotInfo = pSlotInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : Ipv6FsNpMbsmIpv6IntfCreate                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpMbsmIpv6IntfCreate
 *                                                                          
 *    Input(s)            : Arguments of FsNpMbsmIpv6IntfCreate
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
Ipv6FsNpMbsmIpv6IntfCreate (UINT4 u4VrId, UINT4 u4IfIndex,
                            tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tIpv6NpModInfo     *pIpv6NpModInfo = NULL;
    tIpv6NpWrFsNpMbsmIpv6IntfCreate *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IPV6_MOD,    /* Module ID */
                         FS_NP_MBSM_IPV6_INTF_CREATE,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpv6NpModInfo = &(FsHwNp.Ipv6NpModInfo);
    pEntry = &pIpv6NpModInfo->Ipv6NpFsNpMbsmIpv6IntfCreate;

    pEntry->u4VrId = u4VrId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->pSlotInfo = pSlotInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : Ipv6FsNpMbsmIpv6IntfStatus                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpMbsmIpv6IntfStatus
 *                                                                          
 *    Input(s)            : Arguments of FsNpMbsmIpv6IntfStatus
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
Ipv6FsNpMbsmIpv6IntfStatus (UINT4 u4VrId, UINT4 u4IfStatus,
                            tFsNpIntInfo * pIntInfo, tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tIpv6NpModInfo     *pIpv6NpModInfo = NULL;
    tIpv6NpWrFsNpMbsmIpv6IntfStatus *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IPV6_MOD,    /* Module ID */
                         FS_NP_MBSM_IPV6_INTF_STATUS,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpv6NpModInfo = &(FsHwNp.Ipv6NpModInfo);
    pEntry = &pIpv6NpModInfo->Ipv6NpFsNpMbsmIpv6IntfStatus;

    pEntry->u4VrId = u4VrId;
    pEntry->u4IfStatus = u4IfStatus;
    pEntry->pIntInfo = pIntInfo;
    pEntry->pSlotInfo = pSlotInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

#endif /* MBSM_WANTED */
#ifdef CFA_WANTED
/***************************************************************************
 *
 *    Function Name       : Ipv6FsCfaHwRemoveIp6NetRcvdDlfInHash
 *
 *    Description         : This function using its arguments populates the
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsCfaHwRemoveIp6NetRcvdDlfInHash
 *
 *    Input(s)            : Arguments of Ipv6FsCfaHwRemoveIp6NetRcvdDlfInHash
 *
 *    Output(s)           : None
 *
 *    Global Var Referred : None
 *
 *    Global Var Modified : None.
 *
 *    Use of Recursion    : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE
 *
 *****************************************************************************/
UINT1
Ipv6FsCfaHwRemoveIp6NetRcvdDlfInHash (UINT4 u4ContextId, tIp6Addr Ip6Addr,
                                      UINT4 u4Prefix)
{
    tFsHwNp             FsHwNp;
    tCfaNpModInfo      *pCfaNpModInfo = NULL;
    tCfaNpWrFsCfaHwRemoveIp6NetRcvdDlfInHash *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_CFA_MOD,    /* Module ID */
                         FS_CFA_HW_REMOVE_IP6_NET_RCVD_DLF_IN_HASH,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pCfaNpModInfo = &(FsHwNp.CfaNpModInfo);
    pEntry = &pCfaNpModInfo->CfaNpFsCfaHwRemoveIp6NetRcvdDlfInHash;

    pEntry->Ip6Addr = Ip6Addr;
    pEntry->u4Prefix = u4Prefix;
    pEntry->u4ContextId = u4ContextId;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}
#endif
#endif /*__IPV6_NPAPI_C__ */
