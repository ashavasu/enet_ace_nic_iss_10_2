###########################################################################
# Copyright (C) 2009 Aricent Inc . All Rights Reserved   
#
# FILE NAME             : make.h                       
# 
# DESCRIPTION           : Specifies the options and modules to be
#                         including for building the mgmt module
#
###########################################################################

IP6MGMT_INCL_DIRS    = ${COMMON_INCLUDE_DIRS} \
                      -I ${IP6_MGMTINCD} \
                      -I ${RTM6_INCD}

IP6MGMT_DPNDS        = ${COMMON_DEPENDENCIES} \
                       ${IP6_MGMTD}/make.h \
                       ${IP6_MGMTD}/Makefile \
                       ${IP6_MGMTINCD}/stdip6wr.h \
                       ${IP6_MGMTINCD}/stdip6lw.h \
                       ${IP6_MGMTINCD}/stdip6db.h \
                       ${COMN_INCL_DIR}/cli/ip6cli.h \
                       ${COMN_INCL_DIR}/ipv6.h \
                       ${COMN_INCL_DIR}/rtm6.h

ifeq (, $(findstring DLNXIP6_WANTED,$(SYSTEM_COMPILATION_SWITCHES)))
IP6MGMT_INCL_DIRS  += -I ${FSIP6_INCD} \
                       ${IP6_GLOBAL_INCLUDES}
IP6MGMT_DPNDS      +=  ${IP6_MGMTINCD}/ip6mginc.h
else
IP6MGMT_INCL_DIRS  += -I ${IP6_BASE_DIR}/lnxip6/inc \
                      ${IP6_GLOBAL_INCLUDES}
IP6MGMT_DPNDS      +=  ${IP6_MGMTINCD}/lip6mgin.h
endif

ifeq (${IP6HOST}, YES)
INCLUDES   += -I ${IP6HOST_BASE_DIR}
endif

C_FLAGS             = $(CC_FLAGS) \
                      $(GENERAL_COMPILATION_SWITCHES) \
                      ${SYSTEM_COMPILATION_SWITCHES} \
                      ${IP6MGMT_INCL_DIRS} ${INCLUDES} 
