# Copyright (C) 2010 Aricent Inc . All Rights Reserved
# +--------------------------------------------------------------------------+
# |   FILE  NAME             : make.h                                        |
# |   $Id: make.h,v 1.8 2016/03/06 10:07:10 siva Exp $                      |
# |                                                                          |
# |   DESCRIPTION            : Make include file for netip6                  |
# +--------------------------------------------------------------------------+

###########################################################################
#               COMPILATION SWITCHES                                      #
###########################################################################

# This is used to specify the compiler option
COMPILER_TYPE = -DANSI -DUNIX

ifeq (${FSIP4}, YES) 
PROT_TASK_OPNS = -DIP6_WANTED
endif

ifeq  (,$(filter YES ,${CLI_LNXIP} ${SNMP_LNXIP}))
PING6_COMPILATION_SWITCHES += -USLI_WANTED -DBSDCOMP_SLI_WANTED \
                               -DIS_SLI_WRAPPER_MODULE
endif

GLOBAL_OPNS = ${TRACE_OPNS} ${DEBUG_OPNS} ${PROTOCOL_OPNS} \
              ${CONFIG_OPNS} ${DUMP_OPNS} ${COMPILER_TYPE} \
              ${PROT_TASK_OPNS} \
           ${GENERAL_COMPILATION_SWITCHES} ${SYSTEM_COMPILATION_SWITCHES}

############################################################################
#                         Directories                                      #
############################################################################
IP6_BASE_DIR  = ${BASE_DIR}/netip6

FSIP6_BASE_DIR  = ${IP6_BASE_DIR}/fsip6
FSIP6_INCD      = ${FSIP6_BASE_DIR}/ip6core
FSIP6_OBJD      = ${FSIP6_BASE_DIR}/obj

RTM6_DIR         = ${BASE_DIR}/netip6/rtm6
RTM6_INCD        = ${RTM6_DIR}/inc
RTM6_SRCD        = ${RTM6_DIR}/src
RTM6_OBJD        = ${RTM6_DIR}/obj

IP6_MGMTD      = ${IP6_BASE_DIR}/ip6mgmt
IP6_MGMTINCD   = ${IP6_MGMTD}/inc
IP6_MGMTSRCD   = ${IP6_MGMTD}/src
IP6_MGMTOBJD   = ${IP6_MGMTD}/obj

PING6_DIR        = ${IP6_BASE_DIR}/ping6
PING6_INCD       = ${PING6_DIR}/inc
PING6_SRCD       = ${PING6_DIR}/src
PING6_OBJD       = ${PING6_DIR}/obj

IP6HOST_BASE_DIR = ${FSIP6_BASE_DIR}/ip6host
IP6RTR_BASE_DIR  = ${IP6_BASE_DIR}/ip6rtr
TRCRT_DIR        = ${IP6_CORE_DIR}


MIP6_CMN_DIR = ${IP6_BASE_DIR}/mip6
MIP6_DIR     = ${MIP6_CMN_DIR}/cmn
HA_DIR       = ${MIP6_CMN_DIR}/ha
MN_DIR       = ${MIP6_CMN_DIR}/mnode

IP6_CMN_INC_DIR = ${BASE_DIR}/netip6/fsip6/inc

IP6_LIB_INCD  = ${BASE_DIR}/util/ip6
IP6_LIBD  = ${BASE_DIR}/util/ip6
IP6_RTMINCD   = ${RTM6_DIR}/inc
TRIE2INCD     = ${BASE_DIR}/util/trie2/inc
IP6_NPAPI_DIR = ${NPAPI_INCLUDE_DIR}
RMAP_INCDIR   = ${BASE_DIR}/netip/routemap/inc

############################################################################
##                     INCLUDE OPTIONS                                    ##
############################################################################

IP6_GLOBAL_INCLUDES  = -I${FSIP6_INCD} -I${IP6_RTMINCD}\
                      -I${TRIE2INCD} -I${IP6_MGMTINCD}

GLOBAL_INCLUDES = $(IP6_GLOBAL_INCLUDES) \
                    $(COMMON_INCLUDE_DIRS)

########################## END OF MAKE.H ####################################

