/****************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: lip6inc.h,v 1.6 2013/01/07 12:25:36 siva Exp $
 *
 * Description: Include files for IPv6 using Linux IP
 ***************************************************************************/
#ifndef __LIP6INC_H
#define __LIP6INC_H
#include "lr.h"
#include "cfa.h"
#include "vcm.h"
#include "trieinc.h"
#include "fssocket.h"

typedef __u_short u_short;
typedef __u_char u_char;

#include "ip.h"
#include "lnxip.h"
#include "ipv6.h"
#include "rtm6.h"
#include "ip6util.h"
#include "stdip6wr.h" 
#ifdef MLD_WANTED
#include "mld.h"
#endif
#include "lip6mac.h"
#include "lip6tdfs.h"
#include "lip6glob.h"
#include "lip6trc.h"
#include "lip6prot.h"
#include "lip6port.h"
#include "ip6snmp.h"

#ifdef NPAPI_WANTED
#include "npapi.h"
#include "ip6np.h"
#endif

#endif /* __LIP6INC_H */
