/****************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: lip6glob.h,v 1.6 2015/05/27 13:51:41 siva Exp $
 *
 * Description: Global variable declarations used in IPv6(Linux IP)
 ***************************************************************************/
#ifndef _LIP6GLOB_H__
#define _LIP6GLOB_H__

#ifdef _LIP6MAIN_C_
tIp6GblInfo                     gIp6GblInfo;
#else  /* _LIP6MAIN_C_ */       
extern tIp6GblInfo              gIp6GblInfo;
#endif /* _LIP6MAIN_C_ */

#ifdef _NETIPV6_C_
tNetIpv6RegTbl                  gaNetIpv6RegTable[IP6_MAX_PROTOCOLS];
tNetIpv6MCastProtoRegTbl        gaNetIpv6MCastRegTable[IP6_MAX_MCAST_PROTOCOLS];
#else  /* _NETIPV6_C_ */
extern tNetIpv6RegTbl           gaNetIpv6RegTable[IP6_MAX_PROTOCOLS];
extern tNetIpv6MCastProtoRegTbl gaNetIpv6MCastRegTable[IP6_MAX_MCAST_PROTOCOLS];
#endif /* _NETIPV6_C_ */


#ifdef _LIP6KERN_C_
/* command channel */
#if defined(VRF_WANTED) && defined(LINUX_310_WANTED)
tNlSock  gLnxVrfNetlinkCmd[SYS_DEF_MAX_NUM_CONTEXTS];
#else
tNlSock  gNetlinkCmd =         {-1, {0, 0, 0, 0}, "netlink-cmd" };
#endif
tNlSock  gNetlinkRdCmd =       {-1, {0, 0, 0, 0}, "netlink-rd-cmd" };
#else  /* _LIP6KERN_C_ */
#if defined(VRF_WANTED) && defined(LINUX_310_WANTED)
extern tNlSock  gLnxVrfNetlinkCmd[SYS_DEF_MAX_NUM_CONTEXTS];
#else
extern tNlSock  gNetlinkCmd;
#endif
extern tNlSock  gNetlinkRdCmd;
#endif /* _LIP6KERN_C_ */

#ifdef LINUX_310_WANTED
struct nlhandle {
        int fd;
        struct sockaddr_nl snl;
        UINT4 seq;
};

typedef struct _NtPktHdr
{
        struct nlmsghdr n;
        struct ifinfomsg ifi;
        INT1 buf[256];
} tNtPktHdr;
#endif

#if (defined(VRF_WANTED)) && (defined(LINUX_310_WANTED))
#ifdef _LIP6UTL_C
UINT4 gu4LnxVrfCurrContextId = VCM_DEFAULT_CONTEXT;
#else
extern UINT4 gu4LnxVrfCurrContextId;
#endif
#endif

#endif
/* END OF FILE */
