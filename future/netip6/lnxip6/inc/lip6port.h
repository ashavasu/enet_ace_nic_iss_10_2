/****************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: lip6port.h,v 1.5 2014/03/11 14:12:32 siva Exp $
 *
 * Description: Portable function prototypes
 ***************************************************************************/
#include "lip6inc.h"

#ifndef _LIP6_PORT_H__
#define _LIP6_PORT_H__

PUBLIC INT4  Lip6PortSetIfOper PROTO ((tLip6If *pIf6Entry));
PUBLIC INT4  Lip6PortIfRcvPkt PROTO ((tCfaRegInfo * pCfaIfInfo));
PUBLIC INT4  Lip6PortRegisterLL PROTO ((VOID));
PUBLIC VOID  Lip6PortNotifyLL PROTO ((VOID));
PUBLIC INT4  Lip6PortGetHwAddr PROTO ((UINT4 u4IfIndex, UINT1 *au1HwAddr));
PUBLIC INT4  Lip6PortGetIfName PROTO ((UINT4 u4IfIndex, UINT1 *au1IfName));
PUBLIC INT4  
Lip6PortGetIfIndexFromNameInCxt PROTO ((UINT4 u4L2ContextId,UINT1 *au1IfName,
                                       UINT4 *pu4IfIndex));
PUBLIC UINT1   Ip6RetAddrStatComplete  PROTO ((tLip6If *));
#ifdef TUNNEL_WANTED
PUBLIC INT4
Lip6PortGetTunnelParams (tLip6If *pIf6Entry);
PUBLIC INT4
Lip6PortCheckAutoTunlAddr (tIp6Addr * pIp6Addr, UINT4 u4IfIndex);
#endif
#endif
