/****************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: lip6trc.h,v 1.5 2011/11/14 12:22:31 siva Exp $
 *
 * Description: Macro definitions for debug traces
 ***************************************************************************/

#ifndef _LIP6TRC_H
#define _LIP6TRC_H

#ifdef TRACE_WANTED

#define  LIP6_DBG_FLAG    gIp6GblInfo.u4DebugFlag
#define  LIP6_NAME        "LIP6"        /* to be substitued for mod argument */

#define  LIP6_MOD_TRC     0x01000000   /* to be substitued for cmod argument */


/*a note on what should be passed to the arguments given below
* ------------------------------------------------------------
*
* cmod - contains module dependent masks given above for IP6,ND6,UDP6 AND RIP6
* mask - contains masks given in file BASE_DIR/inc/Trace.h 
* fmt  - format of the message
* arg1, arg2, arg3 , arg4, ...... - arguments to be passed
*/

#define LIP6_PKT_DUMP(cmod,mask,pBuf,Length) \
        {  \
             if (((LIP6_DBG_FLAG  & cmod) == cmod) && \
                 ((LIP6_DBG_FLAG  & mask) == DUMP_TRC)) { \
                 DumpPkt (pBuf, IP6_MAX_VALUE); } \
        }

#define LIP6_TRC_ARG(cmod,mask,fmt) if ((LIP6_DBG_FLAG & cmod) == cmod ) { \
   UtlTrcLog (LIP6_DBG_FLAG ,mask,LIP6_NAME,fmt);}

#define LIP6_TRC_ARG1(cmod,mask,fmt,arg1)\
               if ((LIP6_DBG_FLAG & cmod) == cmod ){\
           UtlTrcLog (LIP6_DBG_FLAG ,\
                                  mask, \
                                  LIP6_NAME, \
                                  fmt, \
                                  arg1);\
                }

#define LIP6_TRC_ARG2(cmod,mask,fmt,arg1,arg2)\
               if ((LIP6_DBG_FLAG & cmod) == cmod ){\
           UtlTrcLog (LIP6_DBG_FLAG,\
                                  mask, \
                                  LIP6_NAME, \
                                  fmt, \
                                  arg1, \
                  arg2);\
                }

#define LIP6_TRC_ARG3(cmod,mask,fmt,arg1,arg2,arg3)\
             if ((LIP6_DBG_FLAG & cmod) == cmod) {\
            UtlTrcLog (LIP6_DBG_FLAG,\
                                  mask, \
                                  LIP6_NAME, \
                                  fmt, \
                                  arg1,\
                  arg2,\
                  arg3);\
               }

#define LIP6_TRC_ARG4(cmod,mask,fmt,arg1,arg2,arg3,arg4)\
               if ((LIP6_DBG_FLAG & cmod) == cmod ){\
            UtlTrcLog (LIP6_DBG_FLAG,\
                                  mask, \
                                  LIP6_NAME, \
                                  fmt, \
                                  arg1,\
                                  arg2,\
                                  arg3,\
                                  arg4);\
                }

#define LIP6_TRC_ARG5(cmod,mask,fmt,arg1,arg2,arg3,arg4,arg5)\
              if ((LIP6_DBG_FLAG & cmod) == cmod ){\
            UtlTrcLog (LIP6_DBG_FLAG,\
                                  mask, \
                                  LIP6_NAME, \
                                  fmt, \
                                  arg1,\
                                  arg2,\
                                  arg3,\
                                  arg4,\
                  arg5);\
                }

#else
#define LIP6_PKT_DUMP(cmod,mask,pBuf,Length)
#define LIP6_TRC_ARG(cmod,mask,fmt)
#define LIP6_TRC_ARG1(cmod,mask,fmt,arg1)
#define LIP6_TRC_ARG2(cmod,mask,fmt,arg1,arg2)
#define LIP6_TRC_ARG3(cmod,mask,fmt,arg1,arg2,arg3)
#define LIP6_TRC_ARG4(cmod,mask,fmt,arg1,arg2,arg3,arg4)
#define LIP6_TRC_ARG5(cmod,mask,fmt,arg1,arg2,arg3,arg4,arg5)
#endif
#endif
/* END OF FILE */
