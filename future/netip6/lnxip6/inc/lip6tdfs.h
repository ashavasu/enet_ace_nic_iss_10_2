/**************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: lip6tdfs.h,v 1.12 2015/11/13 12:05:10 siva Exp $
 *
 * Description: Data structure definitions used in netip6/lnxip6
 ***************************************************************************/
#ifndef _LIP6TDF_H__
#define _LIP6TDF_H__

/**************************************************************************
 *                          INTERFACE DATA     
 **************************************************************************/

/* Timer Details */
  typedef struct
  {
      tTmrBlk           TimerBlk;
      UINT1             u1TmrStatus;
      UINT1             au1Reserved[3];
  }  tLip6Timer;


/*  Structure to store static ND entries */
typedef struct Lip6Nd6Entry
{

    tRBNodeEmbd    RbNode;            /* RbNode for the cache entry */
    tIp6Addr       Ip6Addr;           /* IPv6 unicast/anycast address */
    tLip6If       *pIf6Entry;         /* Pointer to Interface entry */
    tMacAddr       MacAddr;           /* Mac address corresponding to the
                                         Ipv6 address */
    UINT1          u1ReachState;      /* Reachability State of the 
                                         NDCache entry */
    UINT1          u1RowStatus;       /* Rowstatus for IPvx MIB */
    UINT4          u4LastUpdatedTime; /* last updated timestamp */
}
tLip6NdEntry;

typedef struct _IP6_ADDR_PROFILE_INFORMATION
{
    UINT4       u4ValidLife;    /* Valid lifetime of the prefix */
    UINT4       u4PrefLife;     /* Preferred lifetime of the prefix */
    UINT2       u2RefCnt;       /* Number of Prefixes using this Profile */
    UINT1       u1AdminStatus;  /* Admin Status of the entry */
    UINT1       u1RaPrefCnf;    /* Whether to advertise this prefix
                            //     * and its usage for on-link and
                              //   * address autoconfiguration */
}
tIp6AddrProfile;
/* The IP6 context information */
typedef struct _LNX_IP6_CONTEXT
{
    UINT4              u4ContextId;
    UINT4              u4ForwFlag;
    UINT1              au1HLRegTable[IP6_MAX_PROTOCOLS];
} tLip6Cxt;



/**************************************************************************
 *                          GLOBAL DATA
 **************************************************************************/
typedef struct Ip6GblInfo
{
    tTimerListId Ip6TimerListId;  /* List ID of Timer used for Radvd */
#if defined(VRF_WANTED) && defined(LINUX_310_WANTED)
    tLip6Timer     aLip6TimerInfo[LIP6_MAX_INTERFACES]; /* Timer */
#else
    tLip6Timer     Lip6TimerInfo; /* Timer */
#endif
    tLip6If      *apIp6If[LIP6_MAX_INTERFACES];
    tRBTree      Nd6CacheTable;                   /* Static ND cache */
    tRBTree      PolicyPrefixTree;
    tMemPoolId   Ip6IfPoolId;
    tLip6Cxt           *apIp6Cxt[MAX_IP6_CONTEXT_LIMIT];
    tLip6Cxt            *pIp6CurrCxt;
    tIp6AddrProfile    *apIp6AddrProfile[LIP6_MAX_ADDR_PROFILE];
#ifdef TUNNEL_WANTED
    tMemPoolId   Ip6TunlIfPoolId;
#endif
    tMemPoolId   AddrLstPoolId;
    tMemPoolId   PrefixListPoolId;
    tMemPoolId   NdEntryPoolId;
    tMemPoolId   Ip6AddrProfpoolId;
    tMemPoolId   Ip6AddrselPolicy;
    tMemPoolId   Ip6mcastId;
#if defined(VRF_WANTED) && defined(LINUX_310_WANTED)
    tMemPoolId   Ip6CxtId;
#endif
    tOsixSemId   Lip6SemId;
    INT4         i4Ip6Status;
    INT4         i4Ip6PolicyPrefixListId;
    INT4         i4Ip6AddrProfPoolId;
    INT4         i4Ip6mcastId;
    INT4         i4NdCacheTimeout;
    UINT4        u4DefHopLimit;
    INT4         i4ECMPPRTTInterval;
    UINT4        u4MaxAddrProfileLimit;
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    INT4         ai4SockFd[SYS_DEF_MAX_NUM_CONTEXTS];
#else
    INT4         i4SockFd;
#endif
    UINT4  u4NextProfileIndex;
    UINT4   u4MaxAssignedProfileIndex;
    UINT4        u4DebugFlag;
    UINT4        u4Nd6CacheEntries;
    UINT4        u4Ip6IfTableLastChange;          /* Table change timestamp */
    UINT1        u1RFC5095Compatibility;
    UINT1        u1RFC5942Compatibility;
    UINT1        au1pad[3];
    UINT4        u4TimerFlag;
}tIp6GblInfo;

/**************************************************************************
 *                          ADDRESS LIST
 ***************************************************************************/



typedef struct _IP6_MULTICAST_INFORMATION
{

    tTMO_SLL_NODE  nextAif;     /* Pointer to next multicast
                                 * address on this interface */
    /* configurable parameters  */
    tIp6Addr  ip6Addr;          /* IPv6 MCAST address */
    UINT2     u2RefCnt;         /* Reference count */
    UINT1     u1AddrScope;     /*Scope of the multicast Address- Added for RFC4007*/
    UINT1     u1Pad;
}tIp6McastInfo;

/**************************************************************************
 *                          PREFIX LIST
 ***************************************************************************/
typedef struct _LIP6_PREFIX_NODE
{
    tTMO_SLL_NODE PrefixNode;       /* SLL Node */
    tIp6Addr    Ip6Prefix;          /* IP6 Prefix Address */
    tIp6Timer   ValidTimer;         /* Variable Valid Timer */
    tIp6Timer   PreferTimer;        /* Variable Prefer Timer to be advertised */
    UINT4       u4IfIndex;          /* Interface index */
    INT4        i4PrefixLen;        /* IP6 Prefix Address Length */
    UINT2       u2ProfileIndex;     /* Profile List Index for this prefix */
    UINT1       u1AdminStatus;      /* Prefix's Administrative Status */
    UINT1       u1PrefixLen;        /* IP6 Prefix Address Length */
    UINT1       u1EmbdRpValid;      /* Prefix is a valid RP address*/
    UINT1       au1Pad[3];
}tLip6PrefixNode;



/**************************************************************************
 *                           CFA PARAMS 
 *************************************************************************/

typedef struct
{
        UINT1  au1IfName[CFA_MAX_PORT_NAME_LENGTH];
  UINT4  u4IfIndex;
  UINT4  u4Status;
  UINT4  u4Speed;
  UINT4  u4HighSpeed;
  UINT2  u2CktIndex;
  UINT1  u1IfType;
  UINT1  u1MsgType;
  /* Type can also be CFA_IF_UP & CFA_IF_DOWN which is
   *      * declared in CFA */
}
tLip6CfaParams;


/**************************************************************************
 *                    Socket interface to kernel
 **************************************************************************/

typedef struct NlSock
{
    INT4                i4Sock;
    struct sockaddr_nl  Snl;
    CONST CHR1          *pi1Name;
}tNlSock;

typedef struct NlReq
{
   struct nlmsghdr     Nl;
   struct rtmsg        Rt;
   char                buf[1024];
}
tNlReq;

typedef struct NlReqNd
{
   struct nlmsghdr     Nl;
   struct ndmsg        Ndm;
   char                buf[1024];
}
tNlReqNd;

/* Message structure. */
typedef struct message
{
   INT4                key;
   const char         *str;
} tLip6Message;


/**************************************************************************
 *                           INTERFACE INFO
 *************************************************************************/


typedef struct ifi_infmtn {
    struct ifi_info *ifi;
    struct ifi_info *head;
    struct ifi_info **tail;
}tIfInfo;

struct in6_ifreq {
   struct in6_addr ifr6_addr;
   __u32       ifr6_prefixlen;
   int     ifr6_ifindex;
};


#endif
