/********************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: netip6mc.h,v 1.4 2013/01/07 12:25:38 siva Exp $
 *
 * Description:This file holds the headerfiles and macros 
 *             for netip6mc.c
 *
 *******************************************************************/
#ifndef _NETIP6MC_H
#define _NETIP6MC_H

#define NETIP6MC_PIM_ID           0x00000002
#define NETIP6MC_ICMPV6_ID        0x00000004

#define NETIP6MC_MAX_THRESHOLD    255

#define NETIP6MC_MLD_ANCILLARY_LEN   20

#define NETIP6MC_MAX_PROTOCOLS    2 /* MLD/PIMv6 */

#define NETIP6MC_CPUPORT_MIFID    (MAXMIFS - 1)

#define NETIP6MC_MUTEX_SEMNAME   ((UINT1 *) "LN6M")

#define NETIP6MC_MUTEX_LOCK()    OsixSemTake (gNetIp6McGlobalInfo.NetIp6SemId);
#define NETIP6MC_MUTEX_UNLOCK()  OsixSemGive (gNetIp6McGlobalInfo.NetIp6SemId);

#define NETIP6MC_MIF_FREE   0xFFFF
#define NETIP6MC_MIF_ALLOC  1

#define NETIP6MC_INVALID_INDEX  0xFFFFFFFF

typedef struct _NetIp6McGlobalInfo
{
    tOsixSemId       NetIp6SemId;   
    INT4             i4NetIp6SockId; /* Multicast socket Id */
    UINT4            u4PortIndex [MAXMIFS]; /* PortIndex of MIF */
    UINT2            u2MifId [MAXMIFS];     /* Free and allocated MIF info */
}tNetIp6McGlobalInfo; 

tNetIp6McGlobalInfo gNetIp6McGlobalInfo;

#endif
