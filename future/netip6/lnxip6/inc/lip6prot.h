/****************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: lip6prot.h,v 1.18 2015/11/13 12:05:10 siva Exp $
 *
 * Description: Function prototypes
 ***************************************************************************/
#ifndef _LIP6_PROT_H__
#define _LIP6_PROT_H__

/* lip6main.c */
PUBLIC VOID 
Lip6MainActOnHLRegDereg PROTO ((UINT4 u4RegStatus, UINT4 u4Proto, UINT4 u4Mask));

#ifdef LINUX_310_WANTED
#ifdef VRRP_WANTED
PUBLIC VOID
Lip6VrrpKernelNetLinkInit PROTO ((VOID));

INT4
Lip6KernIpGetInterface (UINT1 *pu1DevName, INT4 *pi4IfIndex);
#endif
#endif

/* lip6netl.c */
PUBLIC VOID 
Lip6NetLinkSockInit PROTO ((VOID));

INT4 Lip6TimerInit PROTO ((VOID));
#define LIP6_5SECTMR__EVENT 1

PUBLIC INT4 
Lip6NetLinkRouteModify PROTO ((INT4 i4Command,
                                    tNetIpv6RtInfo * pNetRtInfo));
PUBLIC INT4
Lip6NetLinkNDUpdate PROTO ((INT4 i4Command, tLip6NdEntry * pNd6cEntry));

/* lip6if.c */
PUBLIC INT4
Lip6IfUpdateInterface (tLip6CfaParams * pIp6CfaParams);

PUBLIC INT4
Lip6IfUpdateSpeedMtu PROTO ((UINT4 u4Port, UINT4 u4Mtu, UINT4 u4Speed,
                             UINT4 u4HighSpeed));
PUBLIC INT1
Lip6IfEnable PROTO ((tLip6If *pIf6Entry));

PUBLIC INT1
Lip6IfDisable PROTO((tLip6If *pIf6Entry));

PUBLIC UINT1
Lip6GetStallStatus (UINT4 u4Index);

/* lip6kern.c */
PUBLIC INT4
Lip6KernSetForwarding PROTO ((INT4 i4SetVal));

PUBLIC INT4
Lip6KernSetDefaultHopLimit PROTO ((INT4 i4SetVal));

PUBLIC INT4
Lip6KernSetIfForwarding PROTO ((UINT1 *pu1IfName, INT4 i4SetVal));

PUBLIC INT4
Lip6KernSetIfDad (UINT1 *pu1IfName, INT4 i4SetVal);

PUBLIC INT4
Lip6KernSetIfIpv6Disable (UINT1 *pu1IfName, INT4 i4SetVal);

PUBLIC INT4
Lip6SetIpv6ForwardingGlobal PROTO ((UINT4 ));

PUBLIC INT4
Lip6KernUpdateMultiIpv6Addr PROTO ((INT4 i4IpPort,  tIp6Addr * pIp6Addr,
                            INT4 i4PrefixLen, INT4 i4Command));
PUBLIC INT4
Lip6KernUpdateIpv6Addr PROTO ((INT4 i4IpPort,  tIp6Addr * pIp6Addr,
                            INT4 i4PrefixLen, INT4 i4Command));
PUBLIC INT4
Lip6KernSetIfHopLimit PROTO ((UINT4 u4IfIndex, INT4 i4SetVal));

PUBLIC INT4
Lip6KernSetIfDadAttempts PROTO ((UINT4 u4IfIndex, INT4 i4SetVal));

PUBLIC INT4
Lip6KernGetIfStat PROTO ((UINT4 u4IfIndex, UINT1 u1Stat, UINT4 *pu4Value, UINT4 u4ContextId));


/* lip6ra.c */
PUBLIC INT4
Lip6RAInit (UINT4 u4ContextId);

PUBLIC INT4
Lip6RAConfig (UINT4 u4ContextId);

/* lip6nd.c */
PUBLIC tLip6NdEntry     *
Lip6NdCreateEntry (tLip6If *pIf6Entry, tIp6Addr * pIp6Addr);

PUBLIC INT4
Lip6NdSetReachState (tLip6NdEntry * pNd6cEntry, UINT1 u1ReachState);

PUBLIC INT4
Lip6NdDeleteEntry (tLip6NdEntry * pNd6cEntry);

tLip6NdEntry     *
Lip6NdGetEntry (tLip6If *pIf6Entry, tIp6Addr * pIp6Addr);

INT4
Lip6NdGetNextCacheEntry (INT4 i4IfIndex, INT4 *pi4IfIndex,
                         tIp6Addr ipv6Addr, tIp6Addr * pipv6Addr);

VOID
Lip6NdHandleIfStateChange (tLip6If * pIf6Entry);

VOID
Lip6NdDelelteEntryFromKernel (tLip6If * pIf6Entry, tIp6Addr * pIp6Addr, tMacAddr * pMacAddr);

/* lip6lla.c */
PUBLIC INT4
Lip6LlaGetLinkLocalAddress PROTO ((tLip6If * pIf6Entry, tIp6Addr * pIp6Addr, UINT1 *pu1DadStatus));

/* lip6addr.c */
PUBLIC INT4
Lip6AddrUpdate (UINT4 u4IfIndex, tIp6Addr *pIp6Addr, INT4 i4PrefixLen,
                INT4 i4AdminStatus);

PUBLIC tLip6AddrNode     *
Lip6AddrGetEntry (UINT4 u4IfIndex, tIp6Addr * pIp6Addr, INT4 i4PrefixLen);

PUBLIC INT4
Lip6AddrGetNext (UINT4 u4IfIndex, tIp6Addr *pIp6Addr,
                 UINT4 *pu4NextIndex, tIp6Addr * pNextIp6Addr,
                 INT4 *pi4NextPrefix);

PUBLIC INT4
Lip6AddrEnableAll (tLip6If * pIf6Entry);

PUBLIC INT4
Lip6AddrDisableAll (tLip6If * pIf6Entry);

PUBLIC INT4
Lip6AddrDeleteAll PROTO ((tLip6If *pIf6Entry));

PUBLIC tLip6AddrNode *
Lip6AddrScanAllIf PROTO ((tIp6Addr * pIp6Addr, INT4 i4PrefixLen, UINT4 u4ContextId));

PUBLIC INT4
Lip6AddrUpdateKernel (tLip6If * pIf6Entry, UINT1 u1Status);

INT4
   Lip6AddrDelLocalRoute (UINT4 u4IfIndex, tIp6Addr * pIp6Addr, INT4 i4Prefix);

PUBLIC INT4
Lip6AddrDelete (tLip6If * pIf6Entry, tIp6Addr * pIp6Addr, INT4 i4PrefixLen);
/* lip6pref.c */
PUBLIC tLip6PrefixNode     *
Lip6PrefixCreate (UINT4 u4IfIndex, tIp6Addr * pPrefix, INT4 i4PrefixLen,
                  UINT1 u1AdminStatus);

PUBLIC INT4
Lip6PrefixDelete (UINT4 u4IfIndex, tIp6Addr * pPrefix, INT4 i4PrefixLen);

PUBLIC tLip6PrefixNode     *
Lip6PrefixGetEntry (UINT4 u4IfIndex, tIp6Addr * pPrefix, INT4 i4PrefixLen);

PUBLIC tLip6PrefixNode     *
Lip6PrefixGetFirst (UINT4 u4IfIndex);

PUBLIC tLip6PrefixNode     *
Lip6PrefixGetNext (UINT4 u4IfIndex, tIp6Addr * pIp6Addr, INT4 i4PrefixLen);

PUBLIC INT4
Lip6PrefixDeleteAll (tLip6If *pIf6Entry);

PUBLIC INT4
Ip6AddrProfileInit PROTO ((VOID));


/* lip6utl.c */
PUBLIC INT1
Lip6UtlIfEntryExists PROTO ((UINT4 u4IfIndex));

PUBLIC tLip6If     *
Lip6UtlGetIfEntryForPort PROTO ((UINT4 u4IpPort));

PUBLIC INT4
Lip6UtlGetIfInfo PROTO((UINT4 u4IfIndex, tNetIpv6IfInfo * pNetIpv6IfInfo));

PUBLIC INT4
Lip6UtlGetIndexForAddr PROTO ((tIp6Addr *pIpAddr, UINT4 *pu4IfIndex, UINT4 u4ContextId));

PUBLIC UINT4
Lip6UtlGetMtu PROTO ((tLip6If * pIf6));

PUBLIC INT4
Lip6UtlGetAddrPrefixLen PROTO ((UINT4 u4IfIndex, tIp6Addr * pIpv6AddrAddress,
                                INT4 *pi4Ipv6AddrPrefixLength));

PUBLIC INT1
Lip6UtlGetAddrStatus PROTO ((UINT4 u4IfIndex, tIp6Addr *pIp6Addr,
                             INT4 *pi4RetValIpv6AddrStatus));
PUBLIC INT4
Lip6UtlGetAddrPrefixType (tIp6Addr * pIp6Addr, INT4 *pi4Prefix, UINT1 *pu1Type);

PUBLIC INT4
Lip6UtlIfGetNextIndex (UINT4 u4IfIndex, UINT4 *pu4NextIndex);

PUBLIC INT4
Ip6VRExists PROTO ((UINT4 u4ContextId));

#ifdef TUNNEL_WANTED
/* lip6tunl.c */
PUBLIC INT4 Lip6TunnelCreate (tLip6If *pIf6Entry);
PUBLIC INT4 Lip6TunnelDelete (tLip6If *pIf6Entry);
PUBLIC INT4 Lip6TunnelEnable (tLip6If *pIf6Entry);
PUBLIC INT4 Lip6TunnelDisable (tLip6If *pIf6Entry);
PUBLIC INT4
Lip6TunnelIfUpdate (UINT4 u4IfIndex, UINT1 u1TnlDir, UINT1 u1TnlDirFlag,
                    UINT1 u1EncapOption, UINT1 u1EncapLimit, UINT4 u4Mask);
#endif

/* lip6msr.c */
VOID
IncMsrForIpv6NetToPhyTable (INT4 i4IfIndex,
                            tSNMP_OCTET_STRING_TYPE * pIpAddress,
                            CHR1 cDatatype, VOID *pSetVal, UINT4 *pu4ObjectId,
                            UINT4 u4OIdLen, UINT1 IsRowStatus);
VOID
IncMsrForIpv6ContextTable (UINT4 u4ContextId, CHR1 cDatatype, VOID *pSetVal,
                           UINT4 *pu4ObjectId, UINT4 u4OIdLen);
VOID
IncMsrForIpv6IfTable PROTO ((INT4 i4IfIndex, CHR1 cDatatype, VOID *pSetVal,
                             UINT4 *pu4ObjectId, UINT4 u4OIdLen));
VOID
IncMsrForIpv6AddrTable (INT4 i4Index, tSNMP_OCTET_STRING_TYPE * Ipv6Address,
                        INT4 i4PrefixLen, CHR1 cDatatype, VOID *pSetVal,
                        UINT4 *pu4ObjectId, UINT4 u4OIdLen, UINT1 IsRowStatus);
VOID
IncMsrForIpv6AddrProfileTable (UINT4 u4ProfileIndex, CHR1 cDatatype,
                               VOID *pSetVal, UINT4 *pu4ObjectId,
                               UINT4 u4OIdLen);
VOID
IncMsrForIpv6PmtuTable (UINT4 u4ContextId, tSNMP_OCTET_STRING_TYPE * pIpAddress,
                        INT4 i4SetVal, UINT4 *pu4ObjectId, UINT4 u4OIdLen);

VOID
IncMsrForIp6PrefixTable (INT4 i4IfIndex, INT4 i4PrefixLen,
                                tSNMP_OCTET_STRING_TYPE * pIpAddress,
                                INT4 i4SetVal, UINT4 *pu4ObjectId,
                         UINT4 u4OIdLen, UINT1 IsRowStatus);

/* extern declarations */
extern INT1
IncMsrForIPVXIfTable ARG_LIST ((INT4 i4Ipv6InterfaceIfIndex,
                                INT4 i4SetVal, UINT4 *pu4ObjectId,
                                UINT4 u4OIdLen, UINT1 IsRowStatus));
extern VOID
IncMsrForIpv6RouteTable ARG_LIST ((UINT4 u4ContextId,
                                   tSNMP_OCTET_STRING_TYPE * pDestIp,
                                   INT4 i4PrefixLen,
                                   tSNMP_OCTET_STRING_TYPE * pNextHop,
                                   CHR1 cDatatype, VOID *pSetVal,
                                   UINT4 *pu4ObjectId, UINT4 u4OIdLen,
                                   UINT1 IsRowStatus));
extern INT1
IncMsrForIpvxGlbTable ARG_LIST ((UINT4 u4ContextId, INT4 i4SetVal,
                                 UINT4 *pu4ObjectId, UINT4 u4OIdLen));


/* Rtm6 utilities */
PUBLIC INT4
UtilRtm6SetContext PROTO ((UINT4 u4Rtm6CxtId));

PUBLIC INT4
UtilRtm6GetCurrCxtId PROTO ((VOID));


PUBLIC VOID
UtilRtm6ResetContext PROTO ((VOID));

#if defined(VRF_WANTED) && defined(LINUX_310_WANTED)
UINT4 
Ip6UtilChangeCxt PROTO((UINT4 u4ContextId, INT4 i4NSType, 
                        UINT1 *pu1NameSpaceName));
tLip6Cxt* LnxVrfIpv6UtilGetCxtEntryFromCxtId PROTO ((UINT4));
#endif

/* For netipvx */
UINT4
Ip6GetIfReTransTime (UINT4 u4IfIndex);

UINT4
Ip6GetIfReachTime (UINT4 u4IfIndex);
UINT4
Ip6GetAddressInfo (UINT4 u4ContextId, tIp6Addr *pIp6Addr,
                   UINT4 *pu4IfIndex, INT4 *pi4Prefix, UINT1 *pu1AddrType);
UINT4
Ip6GetAddrAdminStatus (UINT4 u4ContextId, tIp6Addr *pIp6Addr,
                       INT4 *pi4AddrRowStatus);
UINT4
Ip6GetAddressStatus (UINT4 u4ContextId, tIp6Addr *pIp6Addr,
                     INT4 *pi4AddrStatus);
UINT4
Ip6GetNextAddressInfo (UINT4 u4ContextId, tIp6Addr *pIp6Addr,
                       tIp6Addr *pNextAddr, UINT4 *pu4NextIndex);
UINT4
Ip6AddrTblGetAddrInfo (UINT4 u4IfIndex, tIp6Addr *pIp6Addr, UINT1 *pu1Prefix);

VOID
Ipv6UtilGetConfigMethod (UINT4 u4IfIndex,
                         tSNMP_OCTET_STRING_TYPE * pFsipv6AddrAddress,
                         UINT1 u1PrefixLen, UINT1 u1AddressType,
                         UINT1 *pu1CfgMethod);
INT1 Ip6UtilClearIpv6InterfaceTable PROTO ((INT4 i4IfIndex));
INT4 Lip6NetlinkParseAddrsFromLnx(UINT4 );
INT4 Lip6AddrCreateMcast (UINT4 , tIp6Addr *);
INT4 Lip6AddrDeleteMcast (UINT4 , tIp6Addr *);
INT4 Lip6AddrCreateSolicitMcast (UINT4 , tIp6Addr *pAddr);
INT4 Lip6AddrDeleteSolicitMcast (UINT4 , tIp6Addr *pAddr);
INT4 Lip6AddrDeleteMcastList (tLip6If * pIf6Entry);
INT4
Lip6AddrCreateNewMcastAddr (UINT4 u4Index, tIp6Addr * pAddr);
INT4
Lip6AddrDeleteLLAddrList (tLip6If * pIf6Entry);
INT4
Lip6Ipv6CheckHitOnNDCacheEntry (UINT4 u4IfIndex, tIp6Addr * pIp6Addr);


#endif
/* END OF FILE */
