/****************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: lip6mac.h,v 1.10 2015/10/29 07:28:35 siva Exp $
 *
 * Description: Macro definitions for IPv6 using Linux IP
 ***************************************************************************/
#ifndef _LIP6MAC_H__
#define _LIP6MAC_H__

#define LIP6_MAX_INTERFACES           IP6_MAX_LOGICAL_IF_INDEX + IP6_ONE

#define LIP6_MAX_VALUE                0xFFFFFFFF

/* Semaphore related */
#define  LIP6_SEM_NAME                 "LIP6"
#define  LIP6_GSEM_NAME                "IP6G"
#define  LIP6_SEM_CREATE_INIT_CNT         1

/*Task related */
#define  LIP6_TASK_NAME         "LI6T"

#define IP6_OFFSET(x,y)               FSAP_OFFSETOF(x,y)
/* REGISTER / DEREGISTER */
#define IP6_DEREGISTER                 0
#define IP6_REGISTER                   1

#define IP6_ADDR_TYPE_STATELESS        1
#define IP6_ADDR_TYPE_STATEFUL         2

/* Ipv6 Forwarding */
#define LIP6_FORW_ENABLE               1
#define LIP6_FORW_DISABLE              2

/* Ipv6 DAD */
#define LIP6_DAD_DISABLE               1
#define LIP6_DAD_ENABLE                2

/* Ipv6 DAD */
#define LIP6_IPV6_ENABLE               0
#define LIP6_IPV6_DISABLE              1

#define LIP6_ADDR_LEN                  16
#define LIP6_MIN_MTU                   1280
#define LIP6_MAX_ADDR_PROFILE        200

#define  IP6_ADDR_PREFIX_ADV           1
#define  IP6_ADDR_ONLINK_ADV           2
#define  IP6_ADDR_AUTO_ADV             4

# define IP6_MAX_DEFAULT_POLICY_ENTRIES 5 
#define IP6_SELF_ADDR             1   /*Address configured on the given interface*/
#define IP6_NOT_SELF_ADDR         2   /*Address not configured on the given interface*/
#define IP6_DEFAULT_POLICY_ENTRY  3   /*Default address entry */

#define IP6_POLICY_ENTRY_REACHABLE   1   /*Reachability status of the entry */
#define IP6_POLICY_ENTRY_UNREACHABLE 2

#define IP6_POLICY_STATUS_AUTO          1  /*Default entry in the table */
#define IP6_POLICY_STATUS_MANUAL        2  /*Configured entry */

#define IP6_ADDR_VALID_TIME_FLAG       8
#define IP6_ADDR_PREF_TIME_FLAG       16
#define IP6_ADDR_PROF_REF_COUNT(i)   gIp6GblInfo.apIp6AddrProfile[i]->u2RefCnt
#define IP6_ADDR_PROF_ADMIN(i)       gIp6GblInfo.apIp6AddrProfile[i]->u1AdminStatus
#define IP6_ADDR_PROF_CONF(i)        gIp6GblInfo.apIp6AddrProfile[i]->u1RaPrefCnf
#define IP6_ADDR_PREF_TIME(i)        gIp6GblInfo.apIp6AddrProfile[i]->u4PrefLife
#define IP6_ADDR_VALID_TIME(i)       gIp6GblInfo.apIp6AddrProfile[i]->u4ValidLife


#define  IP6_INTERFACE(index)       gIp6GblInfo.apIp6If[index];

#define IP6_ADDR_ON_LINK(i)     \
        ((gIp6GblInfo.apIp6AddrProfile[i]->u1RaPrefCnf) & IP6_ADDR_ONLINK_ADV)

#define IP6_ADDR_AUTO_CONFIG(i) \
        ((gIp6GblInfo.apIp6AddrProfile[i]->u1RaPrefCnf) & IP6_ADDR_AUTO_ADV)

#define IP6_ADDR_SEND_PREFIX(i) \
        ((gIp6GblInfo.apIp6AddrProfile[i]->u1RaPrefCnf) & IP6_ADDR_PREFIX_ADV)

#define IP6_ADDR_PREF_TIME_FIXED(i) \
        ((gIp6GblInfo.apIp6AddrProfile[i]->u1RaPrefCnf) & \
         IP6_ADDR_PREF_TIME_FLAG)

#define IP6_ADDR_VALID_TIME_FIXED(i)     \
        ((gIp6GblInfo.apIp6AddrProfile[i]->u1RaPrefCnf) & \
         IP6_ADDR_VALID_TIME_FLAG)
/*
#define IP6_ADDR_DEF_RA_PREF_CNF (IP6_ADDR_PREFIX_ADV | IP6_ADDR_ONLINK_ADV |\
                                 IP6_ADDR_AUTO_ADV | IP6_ADDR_VALID_TIME_FLAG |\
                                 IP6_ADDR_PREF_TIME_FLAG )

*/
#define  IP6_ADDR_PREFIX_ADV           1
#define  IP6_ADDR_ONLINK_ADV           2
#define  IP6_ADDR_AUTO_ADV             4

#define IP6_ADDR_VALID_TIME_FLAG       8
#define IP6_ADDR_PREF_TIME_FLAG       16

/* Base values */
#define BASE_MAX_RTR_ADV_INTERVAL     600
#define BASE_MIN_RTR_ADV_INTERVAL     198   /* 0.33 * MAX_RTR_ADV_INTERVAL */

#define LIP6_IF_NO_RA_ADV              8
#define LIP6_IF_RS_SEND                1
#define LIP6_IF_NO_RS_SEND             0
#define LIP6_IF_DEF_HOPLMT             64

#define LIP6_MAX_REASM_SIZE            65535
#define LIP6_HOST_PREFIX_LENGTH        128

/* Address types */
#define IPV6_ADDR_ANY                  0x0000U
#define IPV6_ADDR_UNICAST              0x0001U
#define IPV6_ADDR_MULTICAST            0x0002U
#define IPV6_ADDR_ANYCAST              0x0004U
#define IPV6_ADDR_LOOPBACK             0x0010U
#define IPV6_ADDR_LINKLOCAL            0x0020U
#define IPV6_ADDR_SITELOCAL            0x0040U
#define IPV6_ADDR_COMPATv4             0x0080U

#define  ADDR6_LLOCAL_PREFIX_1         0xFE
#define  ADDR6_LLOCAL_PREFIX_2         0x80



/* Prefix Adv */
#define LIP6_IF_PREFIX_ADV             1
#define LIP6_IF_NO_PREFIX_ADV          2

#define LIP6_PREFIX_CREATE             1
#define LIP6_PREFIX_DELETE             2

#define LIP6_POLICY_STATUS_AUTO          1  /*Default entry in the table */
#define LIP6_POLICY_STATUS_MANUAL        2  /*Configured entry */

#define DIRECT                        IP6_ROUTE_TYPE_DIRECT

#define LIP6_POLICY_ENTRY_REACHABLE   1   /*Reachability status of the entry */
#define LIP6_POLICY_ENTRY_UNREACHABLE 2
 

#define LIP6_SELF_ADDR             1   /*Address configured on the given interface*/
#define LIP6_NOT_SELF_ADDR         2   /*Address not configured on the given interface*/
#define LIP6_DEFAULT_POLICY_ENTRY  3   /*Default address entry */


#define IP6_LLADDR_PTR_FROM_SLL(sll)  \
        (tIp6LlocalInfo *) (VOID *)\
        ((UINT1 *)(sll) - IP6_OFFSET(tIp6LlocalInfo, nextLif))

#define IP6_ADDR_PTR_FROM_SLL(sll)  \
        (tIp6AddrInfo *) (VOID *)\
        ((UINT1 *)(sll) - IP6_OFFSET(tIp6AddrInfo, nextAif))



/* MTU */
#define  LIP6_MIN_MTU                  1280

/* Link Local Prefix length */
#define  IP6_ADDR_LL_PREFIX_LEN        64

/*
 * Error Handling related constants
 */

#define  ERROR_FATAL          1
#define  ERROR_MINOR          2

#define  ERROR_FATAL_STR      "FATAL"
#define  ERROR_MINOR_STR      "MINOR"

#define  ERR_BUF_ALLOC        0
#define  ERR_BUF_RELEASE      1
#define  ERR_BUF_READ         2
#define  ERR_BUF_WRITE        3
#define  ERR_BUF_PPTR         4
#define  ERR_BUF_WPTR         5
#define  ERR_BUF_WOFFSET      6

#define  ERR_MEM_CREATE       0
#define  ERR_MEM_GET          1
#define  ERR_MEM_RELEASE      2
#define  ERR_MEM_DELETE       3
#define  ERR_MEM_SET_PARAM    4
#define  ERR_MEM_GET_PARAM    5


#define  IP6_RB_GREATER               1
#define  IP6_RB_LESSER               -1
#define  IP6_RB_EQUAL                 0

/* Route operation (Netlink Related) */
#define INET_AFI_IPV6         2

/* Macros used to configure Linux IP settings */
#define LIP6_LINE_LEN             300
#define LIP6_RADVD_CMD_LEN        64
#define LIP6_RADVD_PID_LEN        24
#define LIP6_RADVD_FILE_LEN       48
#define LIP6_STATUS_STRLEN        5

#define LIP6_ND_PERMANENT         0x80

#define LIP6_RB_GREATER           1
#define LIP6_RB_LESSER           -1
#define LIP6_RB_EQUAL             0

#define LIP6_THREE_FOURTH(n)     ((3*n)/4)

#define  NETIPV6_REG_DISABLE          0

/* Macro to Scan the entries present in a ND Cache Table */
#define LIP6_ND_CACHE_SCAN(Tree, pNode, pNext, Typecast) \
for ( (pNode = RBTreeGetFirst(Tree)); ( pNext=((pNode==NULL) ? NULL : \
    ((Typecast) RBTreeGetNext (Tree, pNode, NULL))) ), pNode != NULL; \
      pNode = pNext)
#endif
/* END OF FILE */
