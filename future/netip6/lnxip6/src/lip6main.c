/**************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: lip6main.c,v 1.24 2016/03/05 12:02:37 siva Exp $
 *
 * Description: Main file for IPv6 using Linux IP. Initialize routines.
 ***************************************************************************/
#ifndef _LIP6MAIN_C_
#define _LIP6MAIN_C_

#include "lip6inc.h"
#include "stdip6lw.h"
#ifdef NPAPI_WANTED
#include "nputil.h"
#endif
tOsixSemId          gLip6LockId;
/* Private function declarations */
PRIVATE INT4 Lip6MainInitSocket PROTO ((VOID));

PRIVATE VOID Lip6MainInitRegTable PROTO ((VOID));

PRIVATE VOID Lip6MainShutDown PROTO ((VOID));

PRIVATE INT4 Lip6MainInitResource PROTO ((VOID));

PRIVATE INT4 Lip6MainRouteChangeHandle PROTO ((tIp6RtEntry * pIp6RtEntry,
                                               VOID *pAppSpecData));
PRIVATE INT4
       Lip6MainNdRBTreeCmp (tRBElem * pRBElem, tRBElem * pRBElemIn);

PRIVATE INT4 Lip6TimerEvtFunction PROTO ((VOID));

PRIVATE INT4 Ip6AddrSelPolicyInit PROTO ((VOID));
VOID Lip6NotifyContextStatus(UINT4 u4IfIndex, UINT4 u4ContextId, UINT1 u1Event);
INT4 Lip6HandleCreateContext (UINT4 u4ContextId);
INT4 Lip6HandleDeleteContext (UINT4 u4ContextId);

extern VOID RegisterFSIPV6 PROTO ((void));
extern VOID RegisterFSMPIPV6 PROTO ((void));
#if defined(VRF_WANTED) && defined(LINUX_310_WANTED)
UINT1               gau1RadvdStart[SYS_DEF_MAX_NUM_CONTEXTS];
#else
UINT1               gu1RadvdStart = OSIX_FALSE;
#endif

/*****************************************************************************
DESCRIPTION : IPv6 Protocol Initialization

INPUTS      : None

OUTPUTS     : None

RETURNS     : OSIX_SUCCESS      -  Initialization succeeds
              OSIX_FAILURE  -  Initialization fails
******************************************************************************/
PUBLIC INT4
Lip6MainInit ()
{
    tRtm6RegnId         Rtm6RegnId;
    UINT4               u4Ret;
    tOsixTaskId         Lip6TskId;
    UINT4               u4EventMask = 0;
#if defined(VRF_WANTED) && defined(LINUX_310_WANTED)
    UINT4               u4TempCxtId = IP6_ZERO;
    tVcmRegInfo         VcmRegInfo;
#endif


    if (Lip6MainInitResource () == OSIX_FAILURE)
    {
        Lip6MainShutDown ();
        lrInitComplete (OSIX_FAILURE);
        return OSIX_FAILURE;
    }

    if (Lip6MainInitSocket () == OSIX_FAILURE)
    {
        Lip6MainShutDown ();
        lrInitComplete (OSIX_FAILURE);
        return OSIX_FAILURE;
    }

    if (Lip6PortRegisterLL () == OSIX_FAILURE)
    {
        Lip6MainShutDown ();
        lrInitComplete (OSIX_FAILURE);
        return OSIX_FAILURE;
    }

    if (Lip6TimerInit () == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    Lip6MainInitRegTable ();
    gIp6GblInfo.u4TimerFlag = 0;
    if (OsixTskIdSelf (&Lip6TskId) == OSIX_FAILURE)
    {
        return (OSIX_SUCCESS);
    }

#ifdef LINUX_310_WANTED
#ifdef VRRP_WANTED
    Lip6VrrpKernelNetLinkInit ();
#endif
#endif

#ifdef NPAPI_WANTED
    /* Initialise the NPAPI Related data structures. */
    if (Ipv6FsNpIpv6Init () == FNP_FAILURE)
    {
        Lip6MainShutDown ();
        lrInitComplete (OSIX_FAILURE);
        return (OSIX_FAILURE);
    }
#endif

    if (Lip6KernSetForwarding (LIP6_FORW_ENABLE) == OSIX_FAILURE)
    {
        Lip6MainShutDown ();
        lrInitComplete (OSIX_FAILURE);
        return OSIX_FAILURE;
    }

#ifdef SNMP_2_WANTED
    RegisterFSIPV6 ();
    RegisterSTDIP6 ();
    RegisterFSMPIPV6 ();
#endif

    Lip6PortNotifyLL ();
    u4Ret = Lip6RAInit (VCM_DEFAULT_CONTEXT);
    if (u4Ret == OSIX_FAILURE)
    {
        UtlTrcLog (1, 1, "", "\n\n\r!!! radvd not installed !!!\r\n");
    }
    UNUSED_PARAM (u4Ret);
    Ip6AddrProfileInit ();
    Ip6AddrSelPolicyInit ();
    lrInitComplete (OSIX_SUCCESS);
    MEMSET (&Rtm6RegnId, 0, sizeof (tRtm6RegnId));
    /* Register with RTM6 for Default context, Local Routes. */
    Rtm6RegnId.u2ProtoId = IP6_LOCAL_PROTOID;
    Rtm6RegnId.u4ContextId = VCM_DEFAULT_CONTEXT;
    Rtm6Register (&Rtm6RegnId, 0, NULL);

    /* Register with RTM6 for Default context, Static Routes. */
    Rtm6RegnId.u2ProtoId = IP6_NETMGMT_PROTOID;
    Rtm6RegnId.u4ContextId = VCM_DEFAULT_CONTEXT;
    Rtm6Register (&Rtm6RegnId, 0, NULL);

#ifdef NPAPI_WANTED
    /* Configure the initial value of the Forward status in the
     * default virtual router in the np level */
    if (Ipv6FsNpIpv6UcRouting (VCM_DEFAULT_CONTEXT, NP_IP6_FORW_ENABLE)
            == FNP_FAILURE)
    {
        return OSIX_FAILURE;
    }
#endif

#if defined(VRF_WANTED) && defined(LINUX_310_WANTED)
    /*Register with VCM to receive context status notifications */
    MEMSET (&VcmRegInfo, 0, sizeof (tVcmRegInfo));
    VcmRegInfo.u1ProtoId = IP6_FWD_PROTOCOL_ID;
    VcmRegInfo.pIfMapChngAndCxtChng = Lip6NotifyContextStatus;
    VcmRegInfo.u1InfoMask = VCM_CONTEXT_CREATE | VCM_CONTEXT_DELETE;
    if (VcmRegisterHLProtocol (&VcmRegInfo) == VCM_FAILURE)
    {
        return OSIX_FAILURE;
    }

    for(u4TempCxtId=0;u4TempCxtId<SYS_DEF_MAX_NUM_CONTEXTS;u4TempCxtId++)
    {
        gau1RadvdRunning[u4TempCxtId] = OSIX_FALSE;
        gau1RadvdStart[u4TempCxtId] = OSIX_FALSE;
        MEMSET(&gLnxVrfNetlinkCmd[u4TempCxtId],0,sizeof(tNlSock));
        gLnxVrfNetlinkCmd[u4TempCxtId].i4Sock = -1;
        gLnxVrfNetlinkCmd[u4TempCxtId].pi1Name= "netlink-cmd";
    }
#endif
    while (1)
    {
        if (OsixEvtRecv (Lip6TskId, LIP6_5SECTMR__EVENT, OSIX_WAIT,
                    &u4EventMask) != OSIX_SUCCESS)
        {
            continue;
        }
        if (u4EventMask & LIP6_5SECTMR__EVENT)
        {
            Lip6TimerEvtFunction ();
        }
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************
* DESCRIPTION : Function for Locking LIP6 data structure
* INPUTS      : None
* OUTPUTS     : None
* RETURNS     : OSIX_SUCCESS/OSIX_FAILURE
****************************************************************************/
PUBLIC INT4
Ip6Lock (VOID)
{
    if (OsixSemTake (gIp6GblInfo.Lip6SemId) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}
/******************************************************************************
 * DESCRIPTION : IP6 Timer Initialisation
 *
 * INPUTS      : None
 *
 * OUTPUTS     : None
 *
 * RETURNS     : IP6_SUCCESS
 ******************************************************************************/
INT4
Lip6TimerInit ()
{
    if (TmrCreateTimerList ((const UINT1 *) LIP6_TASK_NAME, LIP6_5SECTMR__EVENT,
                            NULL, &gIp6GblInfo.Ip6TimerListId) == TMR_FAILURE)
    {
        return IP6_FAILURE;
    }
    return (IP6_SUCCESS);
}
/*****************************************************************************
* DESCRIPTION : Function for UnLocking LIP6 data structure
* INPUTS      : None
* OUTPUTS     : None
* RETURNS     : OSIX_SUCCESS/OSIX_FAILURE
****************************************************************************/
PUBLIC INT4
Ip6UnLock (VOID)
{
    if (OsixSemGive (gIp6GblInfo.Lip6SemId) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}
/*****************************************************************************
 * DESCRIPTION : Function for Locking IP6 Task
 * INPUTS      : None
 * OUTPUTS     : None
 * RETURNS     : SNMP_SUCCESS/SNMP_FAILURE
 ****************************************************************************/
INT4
Ip6GLock (VOID)
{
    if (OsixSemTake (gLip6LockId) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}
/*****************************************************************************
 * DESCRIPTION : Function for UnLocking IP6 Task
 * INPUTS      : None
 * OUTPUTS     : None
 * RETURNS     : SNMP_SUCCESS/SNMP_FAILURE
 ****************************************************************************/
INT4
Ip6GUnLock (VOID)
{
    if (OsixSemGive (gLip6LockId) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}


/******************************************************************************
 * DESCRIPTION : This function intimates the status of all the IP6 
 *             : interfaces as soon as the higher layer protocol
 *             : register or deregister with IP6
 * INPUTS      : u4RegStatus - IP6_REGISTER or IP6_DEREGISTER
 *               u4Proto     - Protocol ID
 *               u4Mask      - Registration Mask
 * OUTPUTS     : None
 * RETURNS     : None
 ******************************************************************************/
PUBLIC VOID
Lip6MainActOnHLRegDereg (UINT4 u4RegStatus, UINT4 u4Proto, UINT4 u4Mask)
{
    UINT4               u4Index = 0;
    tIp6Addr            SrcAddr;
    tIp6Addr            DestAddr;
    UINT1               u1SrcPrefixLen = 0;
    UINT1               u1DstPrefixLen = 0;

    if (u4RegStatus != IP6_REGISTER)
    {
        return;
    }

    if (u4Mask & NETIPV6_INTERFACE_PARAMETER_CHANGE)
    {
        for (u4Index = 0; u4Index < (UINT4) LIP6_MAX_INTERFACES; u4Index++)
        {
            if (gIp6GblInfo.apIp6If[u4Index] == NULL)
            {
                continue;
            }

            if (gIp6GblInfo.apIp6If[u4Index]->u1AdminStatus ==
                NETIPV6_ADMIN_INVALID)
            {
                continue;
            }

            if (gIp6GblInfo.apIp6If[u4Index]->u1OperStatus == NETIPV6_OPER_UP)
            {
                LIP6_TRC_ARG (LIP6_MOD_TRC, CONTROL_PLANE_TRC,
                              "NETIP6 : Invoke Interface Change - Oper UP\r\n");
                NetIpv6InvokeInterfaceStatusChange (u4Index, u4Proto,
                                                    NETIPV6_IF_UP,
                                                    NETIPV6_INTERFACE_STATUS_CHANGE);
            }
        }
    }

    if (u4Mask & NETIPV6_ROUTE_CHANGE)
    {

        LIP6_TRC_ARG (LIP6_MOD_TRC, CONTROL_PLANE_TRC,
                      "NETIP6 : Processing Route Change Registration\n");
        MEMSET (&SrcAddr, 0, sizeof (tIp6Addr));
        MEMSET (&DestAddr, 0, sizeof (tIp6Addr));

        Ip6UnLock ();
        Rtm6ApiScanRouteTableForBestRouteInCxt (VCM_DEFAULT_CONTEXT, &SrcAddr,
                                                u1SrcPrefixLen,
                                                Lip6MainRouteChangeHandle, 0,
                                                &DestAddr, &u1DstPrefixLen,
                                                (VOID *) &u4Proto);
        Ip6Lock ();
    }
    return;
}

/******************************************************************************
 *  DESCRIPTION :   Initializes the policy prefix tree
 *
 *
 *  INPUTS      :   None
 *
 *  OUTPUTS     :   None
 *
 *  RETURNS     :   None
 *
 *  NOTES       :
 *******************************************************************************/

PRIVATE INT4
Ip6AddrSelPolicyInit (VOID)
{

    /*Policy table contains five default entries. These entries should be configured initially */
    UINT1               u1Index = 0;

    tIp6Addr            Ip6Addr;

    tIp6AddrSelPolicy  *pNode = NULL;
    /*Five default prefix entries */
    UINT1
         
         
         
         
         
         
         
        aDefEntries[IP6_MAX_DEFAULT_POLICY_ENTRIES][IP6_ADDR_MAX_PREFIX] =
        { {"::1"}, {"::"}, {"2002::"}, {"::"}, {"::ffff:0:0"} };

    /*Precedence value of the default entries */
    UINT4               au4Precedence[IP6_MAX_DEFAULT_POLICY_ENTRIES] =
        { 50, 40, 30, 20, 10 };

    /*Label value of the default entries */
    UINT4               au4Label[IP6_MAX_DEFAULT_POLICY_ENTRIES] =
        { 0, 1, 2, 3, 4 };

    /*Prefix Len of the default entries */
    UINT4               au4PrefixLen[IP6_MAX_DEFAULT_POLICY_ENTRIES] =
        { 128, 0, 16, 96, 96 };

    /*Scope value of the default entries */
    UINT4               au4Scope[IP6_MAX_DEFAULT_POLICY_ENTRIES] =
        { ADDR6_SCOPE_INTLOCAL, ADDR6_SCOPE_LLOCAL,
        ADDR6_SCOPE_GLOBAL, ADDR6_SCOPE_LLOCAL,
        ADDR6_SCOPE_GLOBAL
    };

    /*Default interface index(value of 0) of the default entries */
    UINT4               au4IfIndex[IP6_MAX_DEFAULT_POLICY_ENTRIES] =
        { 0, 0, 0, 0, 0 };

    /*Address  value of the default entries */
    UINT4               au4AddrType[IP6_MAX_DEFAULT_POLICY_ENTRIES] =
        { ADDR6_UNICAST, ADDR6_UNICAST,
        ADDR6_UNICAST, ADDR6_UNICAST, ADDR6_UNICAST
    };
/*Is the address is an self address or not. The default entries does not come
       this category */
    UINT4               au4IsSelfAddr[IP6_MAX_DEFAULT_POLICY_ENTRIES] =
        { IP6_DEFAULT_POLICY_ENTRY,
        IP6_DEFAULT_POLICY_ENTRY,
        IP6_DEFAULT_POLICY_ENTRY,
        IP6_DEFAULT_POLICY_ENTRY,
        IP6_DEFAULT_POLICY_ENTRY
    };

    /*Is the address is public or not */
    UINT4               au4IsPublicAddr[IP6_MAX_DEFAULT_POLICY_ENTRIES] =
        { OSIX_FALSE, OSIX_FALSE, OSIX_TRUE,
        OSIX_FALSE, OSIX_FALSE
    };

    /*Reachabiltiy value of the default entries */
    UINT4               au4ReachabilityStatus[IP6_MAX_DEFAULT_POLICY_ENTRIES] =
        { IP6_POLICY_ENTRY_REACHABLE,
        IP6_POLICY_ENTRY_REACHABLE, IP6_POLICY_ENTRY_REACHABLE,
        IP6_POLICY_ENTRY_REACHABLE, IP6_POLICY_ENTRY_REACHABLE
    };

    /*Entry status of the default entries (default or configurable */
    UINT4               au4EntryStatus[IP6_MAX_DEFAULT_POLICY_ENTRIES] =
        { IP6_POLICY_STATUS_AUTO,
        IP6_POLICY_STATUS_AUTO,
        IP6_POLICY_STATUS_AUTO,
        IP6_POLICY_STATUS_AUTO,
        IP6_POLICY_STATUS_AUTO
    };

    /*Add the entry in the RB Tree */
    gIp6GblInfo.PolicyPrefixTree =
        RBTreeCreateEmbedded (FSAP_OFFSETOF
                              (tIp6AddrSelPolicy, PolicyPrefixRbNode),
                              Ip6PolicyPrefixInfoCmp);

    /* Check is done for any memory failures */
    if (gIp6GblInfo.PolicyPrefixTree == NULL)
    {
        return IP6_FAILURE;
    }
    /*IP6_MAX_DEFAULT_POLICY_ENTRIES refers to 5 default entries */
    for (u1Index = 0; u1Index < IP6_MAX_DEFAULT_POLICY_ENTRIES; u1Index++)
    {
        MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));
        if (INET_ATON6 (aDefEntries[u1Index], &Ip6Addr) != 0)
        {
            /*pNode =
               (tIp6AddrSelPolicy *) (VOID *) Ip6GetMem (IP6_DEFAULT_CONTEXT,
               1);
             */
            pNode =
                (tIp6AddrSelPolicy *) MemAllocMemBlk (gIp6GblInfo.
                                                      i4Ip6PolicyPrefixListId);

            if (pNode == NULL)
            {
                return IP6_FAILURE;
            }
            /*Assign the parameters of the default entries */
            pNode->u4IfIndex = au4IfIndex[u1Index];;
            Ip6CopyAddrBits (&(pNode->Ip6PolicyPrefix), &Ip6Addr,
                             IP6_ADDR_SIZE_IN_BITS);
            pNode->u1Precedence = au4Precedence[u1Index];
            pNode->u1Label = au4Label[u1Index];
            pNode->u1PrefixLen = au4PrefixLen[u1Index];
            pNode->u1Scope = au4Scope[u1Index];
            pNode->u1AddrType = au4AddrType[u1Index];
            pNode->u1IsSelfAddr = au4IsSelfAddr[u1Index];
            pNode->u1IsPublicAddr = au4IsPublicAddr[u1Index];
            pNode->u1ReachabilityStatus = au4ReachabilityStatus[u1Index];
            pNode->u1CreateStatus = au4EntryStatus[u1Index];
            RBTreeAdd (gIp6GblInfo.PolicyPrefixTree, (tRBElem *) pNode);

        }
    }
    return IP6_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : This routine is called during initialization of the IPv6
 *               protocol stack. It initializes default values to kernel.
 *
 * INPUTS      : None
 *
 * OUTPUTS     : None
 *
 * RETURNS     : OSIX_SUCCESS if the memory allocation succeeds
 *               OSIX_FAILURE if the memory allocation fails
 *
 * NOTES       :
 ******************************************************************************/
PRIVATE INT4
Lip6MainInitSocket (VOID)
{
    INT4 i4SockFd = -1;
#ifdef WTP_WANTED
    INT4 i4Discover=1;
#endif    
    i4SockFd =  socket (AF_INET6, SOCK_DGRAM, IPPROTO_IP);
    if (i4SockFd == -1)
    {
        return OSIX_FAILURE;
    }
#ifdef WTP_WANTED    
   if (setsockopt(i4SockFd, IPPROTO_IPV6, IPV6_MTU_DISCOVER,
                 (INT1*) &i4Discover, sizeof(i4Discover)) == -1) {
          LIP6_TRC_ARG (LIP6_MOD_TRC, ALL_FAILURE_TRC,
                                "Lip6Main: Path MTU Discovery option failed\r\n");
          close(i4SockFd);
          return OSIX_FAILURE;
      }
#endif  

#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    gIp6GblInfo.ai4SockFd[VCM_DEFAULT_CONTEXT] = i4SockFd;
#else
    gIp6GblInfo.i4SockFd = i4SockFd;
#endif
    /* Initialize Kernel Params with defval if required */

    return OSIX_SUCCESS;

}

/*****************************************************************************
 * DESCRIPTION : IPv6 for Linux Datastructure craetion
 *
 * INPUTS      : None
 *
 * OUTPUTS     : None
 *
 * RETURNS     : OSIX_SUCCESS  -  Initialization succeeds
 *               OSIX_FAILURE  -  Initialization fails
 *****************************************************************************/
PRIVATE INT4
Lip6MainInitResource ()
{
    if (MemCreateMemPool (sizeof (tLip6If),
                          LIP6_MAX_INTERFACES,
                          MEM_DEFAULT_MEMORY_TYPE,
                          &gIp6GblInfo.Ip6IfPoolId) == MEM_FAILURE)
    {
        LIP6_TRC_ARG (LIP6_MOD_TRC, ALL_FAILURE_TRC,
                      "Lip6Main: MemPool Creation for IF Failed\r\n");
        return (OSIX_FAILURE);
    }

#ifdef TUNNEL_WANTED
    if (MemCreateMemPool (sizeof (tIp6TunlIf),
                          MAX_IP6_TUNNEL_INTERFACES,
                          MEM_DEFAULT_MEMORY_TYPE,
                          &gIp6GblInfo.Ip6TunlIfPoolId) == MEM_FAILURE)
    {
        LIP6_TRC_ARG (LIP6_MOD_TRC, ALL_FAILURE_TRC,
                      "Lip6Main: MemPool Creation for TunnelIf Failed\r\n");
        return (OSIX_FAILURE);
    }
#endif

    if (MemCreateMemPool (sizeof (tLip6AddrNode),
                          MAX_IP6_UNICAST_ADDRESS,
                          MEM_DEFAULT_MEMORY_TYPE,
                          &gIp6GblInfo.AddrLstPoolId) == MEM_FAILURE)
    {
        LIP6_TRC_ARG (LIP6_MOD_TRC, ALL_FAILURE_TRC,
                      "Lip6Main :Address list mempool creation failed\r\n");
        return (OSIX_FAILURE);
    }

    if (MemCreateMemPool (sizeof (tLip6PrefixNode),
                          MAX_IP6_UNICAST_ADDRESS,
                          MEM_DEFAULT_MEMORY_TYPE,
                          &gIp6GblInfo.PrefixListPoolId) == MEM_FAILURE)
    {
        LIP6_TRC_ARG (LIP6_MOD_TRC, ALL_FAILURE_TRC,
                      "Lip6Main :Prefix list mempool creation failed\r\n");
        return (OSIX_FAILURE);
    }

    if (MemCreateMemPool (sizeof (tLip6NdEntry),
                          MAX_IP6_ND6_CACHE_ENTRIES,
                          MEM_DEFAULT_MEMORY_TYPE,
                          &gIp6GblInfo.NdEntryPoolId) == MEM_FAILURE)
    {
        LIP6_TRC_ARG (LIP6_MOD_TRC, ALL_FAILURE_TRC,
                      "Lip6Main :ND entry mempool creation failed\r\n");
        return (OSIX_FAILURE);
    }

    gIp6GblInfo.Nd6CacheTable =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tLip6NdEntry, RbNode),
                              Lip6MainNdRBTreeCmp);

    if (gIp6GblInfo.Nd6CacheTable == NULL)
    {
        LIP6_TRC_ARG (LIP6_MOD_TRC, ALL_FAILURE_TRC,
                      "Lip6Main :RBTree creation for ND entries failed\r\n");
        return (OSIX_FAILURE);
    }

    if (MemCreateMemPool (sizeof (tIp6AddrProfile),
                          LIP6_MAX_ADDR_PROFILE,
                          MEM_DEFAULT_MEMORY_TYPE,
                          &gIp6GblInfo.Ip6AddrProfpoolId) == MEM_FAILURE)
    {
        LIP6_TRC_ARG (LIP6_MOD_TRC, ALL_FAILURE_TRC,
                      "Lip6Main: MemPool Creation for Addr profile Failed\r\n");
        return (OSIX_FAILURE);
    }

    if (MemCreateMemPool (sizeof (tIp6AddrSelPolicy),
                          IP6_MAX_DEFAULT_POLICY_ENTRIES,
                          MEM_DEFAULT_MEMORY_TYPE,
                          &gIp6GblInfo.Ip6AddrselPolicy) == MEM_FAILURE)
    {
        LIP6_TRC_ARG (LIP6_MOD_TRC, ALL_FAILURE_TRC,
                      "Lip6Main: MemPool Creation for Addr profile Failed\r\n");
        return (OSIX_FAILURE);
    }
    if (MemCreateMemPool (sizeof (tLip6NdEntry),
                          MAX_IP6_MCAST_ADDRESS,
                          MEM_DEFAULT_MEMORY_TYPE,
                          &gIp6GblInfo.Ip6mcastId) == MEM_FAILURE)
    {
        LIP6_TRC_ARG (LIP6_MOD_TRC, ALL_FAILURE_TRC,
                      "Lip6Main :ND entry mempool creation failed\r\n");
        return (OSIX_FAILURE);
    }

#if defined(VRF_WANTED) && defined(LINUX_310_WANTED)
    if (MemCreateMemPool (sizeof (tLip6Cxt),
                          MAX_IP6_CONTEXT,
                          MEM_DEFAULT_MEMORY_TYPE,
                          &gIp6GblInfo.Ip6CxtId) == MEM_FAILURE)
    {
        LIP6_TRC_ARG (LIP6_MOD_TRC, ALL_FAILURE_TRC,
                      "Lip6Main :Context Id mempool creation failed\r\n");
        return (OSIX_FAILURE);
    }
#endif
    if (OsixSemCrt ((UINT1 *) LIP6_SEM_NAME,
                    &(gIp6GblInfo.Lip6SemId)) != OSIX_SUCCESS)
    {
        LIP6_TRC_ARG (LIP6_MOD_TRC, ALL_FAILURE_TRC,
                      "Lip6Main :Semaphore creation failed\r\n");
        return OSIX_FAILURE;
    }

    OsixSemGive (gIp6GblInfo.Lip6SemId);
    if (OsixCreateSem ((UINT1 *)LIP6_GSEM_NAME,
                       LIP6_SEM_CREATE_INIT_CNT, 0,&gLip6LockId) != OSIX_SUCCESS)
    {
        LIP6_TRC_ARG (LIP6_MOD_TRC, ALL_FAILURE_TRC,
                         "Lip6Main :Semaphore creation failed\r\n");
        return OSIX_FAILURE;
    }

    OsixSemGive (gLip6LockId);


    return OSIX_SUCCESS;
}

/******************************************************************************
 * Function Name    :   Ip6InitRegTable
 * Description      :   This routine is used to initializes the NetIpv6
 *                      Registration Table
 * Inputs           :   None
 * Outputs          :   None
 * Return Value     :   None
 ******************************************************************************/
PRIVATE VOID
Lip6MainInitRegTable (VOID)
{
    UINT4               u4Index;

    for (u4Index = 0; u4Index < IP6_MAX_PROTOCOLS; u4Index++)
    {
        gaNetIpv6RegTable[u4Index].pAppRcv = NULL;
        gaNetIpv6RegTable[u4Index].pAddrChange = NULL;
        gaNetIpv6RegTable[u4Index].pRouteChange = NULL;
        gaNetIpv6RegTable[u4Index].pIfChange = NULL;
        gaNetIpv6RegTable[u4Index].pIcmp6ErrSnd = NULL;
    }

    LIP6_TRC_ARG (LIP6_MOD_TRC, ALL_FAILURE_TRC,
                  "Ip6InitRegTable : Initialization Success !\r\n");
    return;
}

/****************************************************************************
 * Function           : Lip6MainShutDown
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : OSIX_SUCCESS/OSIX_FAILURE.
 * Description        : This function will free the Linux Ipv6 Local
 *                      data structures.
 ***************************************************************************/
PRIVATE VOID
Lip6MainShutDown ()
{
    UINT4                u4ContextId = VCM_DEFAULT_CONTEXT;
    /* Free Addr List nodes,Free If nodes and  Delete mempools */
    if (gIp6GblInfo.Ip6IfPoolId != 0)
    {
        if (MemDeleteMemPool (gIp6GblInfo.Ip6IfPoolId) == MEM_FAILURE)
        {
            LIP6_TRC_ARG (LIP6_MOD_TRC, ALL_FAILURE_TRC,
                          "Lip6MainShutDown: Ip6IfPoolId Delete failed!\r\n");
        }
    }
#ifdef TUNNEL_WANTED
    if (gIp6GblInfo.Ip6TunlIfPoolId != 0)
    {
        if (MemDeleteMemPool (gIp6GblInfo.Ip6TunlIfPoolId) == MEM_FAILURE)
        {
            LIP6_TRC_ARG (LIP6_MOD_TRC, ALL_FAILURE_TRC,
                          "Lip6MainShutDown: Ip6TunlIfPoolId Delete failed!\r\n");
        }
    }
#endif

    if (gIp6GblInfo.AddrLstPoolId != 0)
    {
        if (MemDeleteMemPool (gIp6GblInfo.AddrLstPoolId) == MEM_FAILURE)
        {
            LIP6_TRC_ARG (LIP6_MOD_TRC, ALL_FAILURE_TRC,
                          "Lip6MainShutDown: AddrLstPoolId Delete failed!\r\n");
        }
    }

    if (gIp6GblInfo.PrefixListPoolId != 0)
    {
        if (MemDeleteMemPool (gIp6GblInfo.PrefixListPoolId) == MEM_FAILURE)
        {
            LIP6_TRC_ARG (LIP6_MOD_TRC, ALL_FAILURE_TRC,
                          "Lip6MainShutDown: PrefixListPoolId Delete failed!\r\n");
        }
    }

    if (gIp6GblInfo.Ip6AddrProfpoolId != 0)
    {
        if (MemDeleteMemPool (gIp6GblInfo.Ip6AddrProfpoolId) == MEM_FAILURE)
        {
            LIP6_TRC_ARG (LIP6_MOD_TRC, ALL_FAILURE_TRC,
                          "Lip6MainShutDown: Ip6AddrProfpoolId  Delete failed!\r\n");
        }
    }

    if (gIp6GblInfo.Ip6AddrselPolicy != 0)
    {
        if (MemDeleteMemPool (gIp6GblInfo.Ip6AddrselPolicy) == MEM_FAILURE)
        {
            LIP6_TRC_ARG (LIP6_MOD_TRC, ALL_FAILURE_TRC,
                          "Lip6MainShutDown: Ip6AddrselPolicy  Delete failed!\r\n");
        }
    }
    if (gIp6GblInfo.Ip6mcastId != 0)
    {
        if (MemDeleteMemPool (gIp6GblInfo.Ip6mcastId) == MEM_FAILURE)
        {
            LIP6_TRC_ARG (LIP6_MOD_TRC, ALL_FAILURE_TRC,
                          "Lip6MainShutDown: Ip6mcastId  Delete failed!\r\n");
        }
    }

    if (gIp6GblInfo.Lip6SemId != 0)
    {
        OsixSemDel (gIp6GblInfo.Lip6SemId);
    }
    if (gLip6LockId != 0)
    {
        OsixSemDel (gLip6LockId);
    }


#ifdef NPAPI_WANTED
    Ipv6FsNpIpv6Deinit ();
#endif /* NPAPI_WANTED */

#if defined(VRF_WANTED) && defined(LINUX_310_WANTED)
    u4ContextId = Lip6GetCurrentContext();
#endif

    if(u4ContextId == VCM_DEFAULT_CONTEXT)
    { 
	    gIp6GblInfo.i4Ip6Status = OSIX_FALSE;
    }
    else
    {
	    if (gIp6GblInfo.apIp6Cxt[u4ContextId] != NULL )
	    {
		    (gIp6GblInfo.apIp6Cxt[u4ContextId])->u4ForwFlag = OSIX_FALSE; 
	    }
    }

    return;
}

/******************************************************************************
 * DESCRIPTION : This function intimates the status of all the IP6
 *             : interfaces as soon as the higher layer protocol
 *             : register or deregister with IP6
 * INPUTS      : u4RegStatus - IP6_REGISTER or IP6_DEREGISTER
 *               u4Proto     - Protocol ID
 *               u4Mask      - Registration Mask
 * OUTPUTS     : None
 * RETURNS     : None
 ******************************************************************************/
PRIVATE INT4
Lip6MainRouteChangeHandle (tIp6RtEntry * pIp6RtEntry, VOID *pAppSpecData)
{
    tNetIpv6RtInfo      NetIpv6RtInfo;
    UINT4               u4Proto = *(UINT4 *) pAppSpecData;

    /* Fill the Route information. */
    MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));
    MEMCPY (&NetIpv6RtInfo.Ip6Dst, &pIp6RtEntry->dst, sizeof (tIp6Addr));
    NetIpv6RtInfo.u1Prefixlen = pIp6RtEntry->u1Prefixlen;
    MEMCPY (&NetIpv6RtInfo.NextHop, &pIp6RtEntry->nexthop, sizeof (tIp6Addr));
    NetIpv6RtInfo.u4Index = pIp6RtEntry->u4Index;
    NetIpv6RtInfo.u4Metric = pIp6RtEntry->u4Metric;
    NetIpv6RtInfo.i1Proto = pIp6RtEntry->i1Proto;
    NetIpv6RtInfo.i1DefRtrFlag = pIp6RtEntry->i1DefRtrFlag;
    NetIpv6RtInfo.u4RowStatus = IP6FWD_ACTIVE;
    NetIpv6RtInfo.u2ChgBit = IP6_BIT_ALL;

    NetIpv6InvokeRouteChange (&NetIpv6RtInfo, u4Proto);
    return RTM6_SUCCESS;
}

/*****************************************************************************
* FUNTION     : Lip6MainNdRBTreeCmp
*
* DESCRIPTION : This function is used to compare the RBTree keys of the
*               ND6Cache RBTree.
*
* INPUTS      : pRBElem, pRBElemIn
*
* OUTPUTS     : None
*
* RETURNS     : IP6_RB_LESSER  if pRBElem <  pRBElemIn
*               IP6_RB_GREATER if pRBElem >  pRBElemIn
*               IP6_RB_EQUAL   if pRBElem == pRBElemIn
*****************************************************************************/
PRIVATE INT4
Lip6MainNdRBTreeCmp (tRBElem * pRBElem, tRBElem * pRBElemIn)
{
    tLip6NdEntry       *pNd6cEntry = pRBElem;
    tLip6NdEntry       *pNd6cEntryIn = pRBElemIn;
    INT4                i4RetVal = 0;

    if (pNd6cEntry->pIf6Entry->u4IfIndex < pNd6cEntryIn->pIf6Entry->u4IfIndex)
    {
        return LIP6_RB_LESSER;
    }
    else if (pNd6cEntry->pIf6Entry->u4IfIndex >
             pNd6cEntryIn->pIf6Entry->u4IfIndex)
    {
        return LIP6_RB_GREATER;
    }

    i4RetVal = Ip6AddrCompare (pNd6cEntry->Ip6Addr, pNd6cEntryIn->Ip6Addr);

    if (i4RetVal == IP6_ZERO)
    {
        return LIP6_RB_EQUAL;
    }
    else if (i4RetVal == IP6_ONE)
    {
        return LIP6_RB_GREATER;
    }
    else
    {
        return LIP6_RB_LESSER;
    }
}
PUBLIC INT4
Ip6AddrProfileInit (VOID)
{
    UINT2               u2Count = 0;

    for (u2Count = 0; u2Count < MAX_IP6_ADDR_PROFILES_LIMIT; u2Count++)
    {

        if ((gIp6GblInfo.apIp6AddrProfile[u2Count] =
             (tIp6AddrProfile *) MemAllocMemBlk (gIp6GblInfo.
                                                 Ip6AddrProfpoolId)) == NULL)
        {
            LIP6_TRC_ARG (LIP6_MOD_TRC, ALL_FAILURE_TRC,
                          "Mem alloc for tLip6AddrNode Failed!\n");
            return OSIX_FAILURE;
        }

        if (gIp6GblInfo.apIp6AddrProfile[u2Count] != NULL)
        {
            MEMSET (gIp6GblInfo.apIp6AddrProfile[u2Count], 0,
                    sizeof (tIp6AddrProfile));
            gIp6GblInfo.apIp6AddrProfile[u2Count]->u1RaPrefCnf =
                IP6_ADDR_DEF_RA_PREF_CNF;
            gIp6GblInfo.apIp6AddrProfile[u2Count]->u4ValidLife =
                IP6_ADDR_PROF_DEF_VALID_LIFE;
            gIp6GblInfo.apIp6AddrProfile[u2Count]->u4PrefLife =
                IP6_ADDR_PROF_DEF_PREF_LIFE;
            gIp6GblInfo.apIp6AddrProfile[u2Count]->u2RefCnt = 0;
            gIp6GblInfo.apIp6AddrProfile[u2Count]->u1AdminStatus =
                IP6_ADDR_PROF_INVALID;
        }
        else
        {
            /*IP6_GBL_TRC_ARG4 (IP6_MOD_TRC,
               CONTROL_PLANE_TRC, IP6_NAME,
               "IP6ADDR :Ip6AddrProfileInit: Fail %s mem err: "
               "Type= %d PoolId= %d Memptr= %p\n",
               ERROR_FATAL_STR, ERR_MEM_CREATE, NULL, 0); */

            return IP6_FAILURE;
        }
    }

    /* The first profile is initialized to default values */

    gIp6GblInfo.apIp6AddrProfile[0]->u1AdminStatus = IP6_ADDR_PROF_VALID;
    gIp6GblInfo.apIp6AddrProfile[0]->u1RaPrefCnf = IP6_ADDR_DEF_RA_PREF_CNF;
    gIp6GblInfo.apIp6AddrProfile[0]->u4ValidLife = IP6_ADDR_PROF_DEF_VALID_LIFE;
    gIp6GblInfo.apIp6AddrProfile[0]->u4PrefLife = IP6_ADDR_PROF_DEF_PREF_LIFE;
    gIp6GblInfo.apIp6AddrProfile[0]->u2RefCnt = 0;
    gIp6GblInfo.i4Ip6PolicyPrefixListId = 1;
    gIp6GblInfo.u4NextProfileIndex = 1;
    gIp6GblInfo.u4MaxAssignedProfileIndex = 1;
    gIp6GblInfo.u1RFC5095Compatibility = IP6_RFC5095_COMPATIBLE;
    gIp6GblInfo.u1RFC5942Compatibility = IP6_RFC5942_NOT_COMPATIBLE;
    gIp6GblInfo.u4DefHopLimit = IP6_DEF_HOP_LIMIT;

    return IP6_SUCCESS;
}

/******************************************************************************
 * Function Name    :   Lip6TimerEvtFunction
 * Description      :   This routine is used as a Event call back 
 *                      for the timer expiry 
 * Inputs           :   None
 * Outputs          :   None
 * Return Value     :   None
 ******************************************************************************/

PRIVATE INT4
Lip6TimerEvtFunction ()
{
    UINT4           u4ContextId = VCM_DEFAULT_CONTEXT;
#if defined(VRF_WANTED) && defined(LINUX_310_WANTED)
    UINT4           u4TempContextId;
    UINT4           u4FwdFlag = IP6_FORW_ENABLE;
    tLnxVrfInfo        *pLnxVrfInfo = NULL;
    if (VcmGetFirstActiveContext (&u4ContextId) != VCM_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    do
    {
        u4TempContextId = u4ContextId;
        if(u4ContextId)
        {
            pLnxVrfInfo = LnxVrfInfoGet (u4ContextId);
            if (pLnxVrfInfo == NULL)
            {
                return OSIX_FAILURE;
            }
        }
        if(Ip6UtilChangeCxt (u4ContextId, LNX_VRF_NON_DEFAULT_NS,
                             pLnxVrfInfo->au1NameSpace) == 
           (UINT4)NETIPV6_FAILURE)
        {
            return OSIX_FAILURE;
        }

        if (gau1RadvdRunning[u4ContextId] == OSIX_TRUE)
        {
            Lip6StopRadvd (u4ContextId);
            OsixTskDelay (100);
        }
       if (u4ContextId == VCM_DEFAULT_CONTEXT)
       {
          u4FwdFlag = gIp6GblInfo.i4Ip6Status;
       }
       else
       {
         if (gIp6GblInfo.apIp6Cxt[u4ContextId] != NULL)   
            u4FwdFlag = (gIp6GblInfo.apIp6Cxt[u4ContextId])->u4ForwFlag;
       }
        if ((gau1RadvdStart[u4ContextId] == OSIX_TRUE)
              && (u4FwdFlag == IP6_FORW_ENABLE))
        {
            Lip6StartRadvd (u4ContextId);
            gau1RadvdRunning[u4ContextId] = OSIX_TRUE;
        }
        else
        {
            gau1RadvdRunning[u4ContextId] = OSIX_FALSE;
        }

        /* 1 - TMR_IS_RUNNING
         * 0 - TMR_IS_NOT_RUNNNG */
        gIp6GblInfo.aLip6TimerInfo[u4ContextId].u1TmrStatus = 0;
        if(Ip6UtilChangeCxt (u4ContextId, LNX_VRF_DEFAULT_NS,
                             (UINT1 *)LNX_VRF_DEFAULT_NS_PID) == 
           (UINT4)NETIPV6_FAILURE)
        {
            return OSIX_FAILURE;
        }
        OsixTskDelay (100);
    }
    while (VcmGetNextActiveContext (u4TempContextId, &u4ContextId) ==
           VCM_SUCCESS);

#else
    if (gu1RadvdRunning == OSIX_TRUE)
    {
        Lip6StopRadvd (u4ContextId);
        OsixTskDelay (100);
    }
    if ((gu1RadvdStart == OSIX_TRUE)
        && (gIp6GblInfo.i4Ip6Status == IP6_FORW_ENABLE))
    {
        Lip6StartRadvd (u4ContextId);
        gu1RadvdRunning = OSIX_TRUE;
    }
    else
    {
        gu1RadvdRunning = OSIX_FALSE;
    }

    /* 1 - TMR_IS_RUNNING
     * 0 - TMR_IS_NOT_RUNNNG */

    gIp6GblInfo.Lip6TimerInfo.u1TmrStatus = 0;
#endif
    return OSIX_SUCCESS;
}
#if defined(VRF_WANTED) && defined(LINUX_310_WANTED)
/******************************************************************************
 * DESCRIPTION : This function de-initializes the IP6 context information
 *                for the given context.
 * INPUTS      : u4ContextId - Context to be deleted
 * OUTPUTS     : None
 * RETURNS     : IP6_SUCCESS / IP6_FAILURE;
 *****************************************************************************/
INT4
Lip6HandleDeleteContext (UINT4 u4ContextId)
{
    tLip6Cxt            *pIp6Cxt = NULL;
    tRtm6RegnId         Rtm6RegnId;
    MEMSET (&Rtm6RegnId, 0, sizeof (tRtm6RegnId));

    if (u4ContextId >= MAX_IP6_CONTEXT)
    {
        LIP6_TRC_ARG1 (IP6_MOD_TRC,
                      CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                       "Lip6HandleCreateContext: Invalid context Id"
                      "%d\n", u4ContextId);
        return IP6_FAILURE;
    }

    pIp6Cxt = gIp6GblInfo.apIp6Cxt[u4ContextId];

    if (pIp6Cxt == NULL)
    {
        LIP6_TRC_ARG1 (IP6_MOD_TRC,
                      CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                      "Lip6HandleDeleteContext: Context Information"
                      "already deleted for the context %d\n", u4ContextId);
        return IP6_FAILURE;
    }

    /* Deregister with RTM6 for Local Routes. */
    Rtm6RegnId.u2ProtoId = IP6_LOCAL_PROTOID;
    Rtm6RegnId.u4ContextId = u4ContextId;
    Rtm6Deregister (&Rtm6RegnId);

    /* Deregister with RTM6 for Static Routes. */
    Rtm6RegnId.u2ProtoId = IP6_NETMGMT_PROTOID;
    Rtm6RegnId.u4ContextId = u4ContextId;
    Rtm6Deregister (&Rtm6RegnId);

    MEMSET (pIp6Cxt, 0, sizeof (tLip6Cxt));

    MemReleaseMemBlock (gIp6GblInfo.Ip6CxtId, (UINT1 *) pIp6Cxt);
    pIp6Cxt = NULL;
    gIp6GblInfo.apIp6Cxt[u4ContextId] = NULL;
    return IP6_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : This function initializes the IP6 context information for the
 *                given context.
 * INPUTS      : u4ContextId - Context to be created
 * OUTPUTS     : None
 * RETURNS     : IP6_SUCCESS / IP6_FAILURE
 *****************************************************************************/
INT4
Lip6HandleCreateContext (UINT4 u4ContextId)
{
    tLip6Cxt            *pIp6Cxt = NULL;
    tRtm6RegnId         Rtm6RegnId;

    MEMSET (&Rtm6RegnId, 0, sizeof (tRtm6RegnId));

    if (gIp6GblInfo.apIp6Cxt[u4ContextId] != NULL)
    {
        LIP6_TRC_ARG1 (IP6_MOD_TRC,
                      CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                       "Lip6HandleCreateContext: Context Information"
                      "already exists for the context %d\n", u4ContextId);
        return IP6_FAILURE;
    }
    gIp6GblInfo.apIp6Cxt[u4ContextId] =
        MemAllocMemBlk (gIp6GblInfo.Ip6CxtId);

    if (gIp6GblInfo.apIp6Cxt[u4ContextId] == NULL)
    {
        LIP6_TRC_ARG ( IP6_MOD_TRC,
                     CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                     "Ip6HandleCreateContext: Memory allocation "
                     "failed for creation of context entry \n");
        return IP6_FAILURE;
    }

    pIp6Cxt = gIp6GblInfo.apIp6Cxt[u4ContextId];

    MEMSET (pIp6Cxt, 0, sizeof (tLip6Cxt));

    /* Initialize the Ip6 Context information */
    pIp6Cxt->u4ContextId = u4ContextId;
    pIp6Cxt->u4ForwFlag = IP6_FORW_ENABLE;

    /* Register with RTM6 for Local Routes. */
    Rtm6RegnId.u2ProtoId = IP6_LOCAL_PROTOID;
    Rtm6RegnId.u4ContextId = u4ContextId;
    Rtm6Register (&Rtm6RegnId, 0, NULL);

    /* Register with RTM6 for Static Routes. */
    Rtm6RegnId.u2ProtoId = IP6_NETMGMT_PROTOID;
    Rtm6RegnId.u4ContextId = u4ContextId;
    Rtm6Register (&Rtm6RegnId, 0, NULL);

    return IP6_SUCCESS;

}

/******************************************************************************
 * DESCRIPTION : This function initializes the IP6 context information for the
 *                given context.
 * INPUTS      : u4ContextId - Context to be created
 *               u4IfIndex - Interface Index
 *               u1Event - VCM event
 * OUTPUTS     : None
 * RETURNS     : IP6_SUCCESS / IP6_FAILURE
 *****************************************************************************/

VOID Lip6NotifyContextStatus(UINT4 u4IfIndex, UINT4 u4ContextId, UINT1 u1Event)
{
    UNUSED_PARAM (u4IfIndex);

    if(u4ContextId == VCM_DEFAULT_CONTEXT)
    {
	    if (gIp6GblInfo.i4Ip6Status == IP6_FAILURE)
	    {
		    LIP6_TRC_ARG (IP6_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
				    "Ip6NotifyContextStatus: IP6 Task Not Initialised !!!\n");
		    return;
	    }
    }
    else
    {
	    if (gIp6GblInfo.apIp6Cxt[u4ContextId] != NULL)
	    {
		    if ( (gIp6GblInfo.apIp6Cxt[u4ContextId])->u4ForwFlag != IP6_SUCCESS)
		    {
			    LIP6_TRC_ARG (IP6_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
					    "Ip6NotifyContextStatus: IP6 Task Not Initialised !!!\n");
			    return;
		    }
	    }
    }   

    if (u4ContextId > MAX_IP6_CONTEXT)
    {
        LIP6_TRC_ARG1 (IP6_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          "Lip6NotifyContextStatus: Invalid context Id %d\n",
                          u4ContextId);
        return;
    }

    if (u1Event == VCM_CONTEXT_CREATE)
    {
        IP6_TASK_LOCK ();
        Lip6HandleCreateContext (u4ContextId);
        IP6_TASK_UNLOCK ();
        return;
    }
    else 
    {
        IP6_TASK_LOCK ();
        Lip6HandleDeleteContext (u4ContextId);
        IP6_TASK_UNLOCK ();
    }
    return;
}
#endif

#endif /*_LIP6MAIN_C_ */
/* END OF FILE */
