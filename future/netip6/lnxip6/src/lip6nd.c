/**************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: lip6nd.c,v 1.13 2015/04/03 12:48:47 siva Exp $
 *
 * Description: Neighbor Discovery management routines
 ***************************************************************************/
#ifndef _LIP6ND_C
#define _LIP6ND_C

#include "lip6inc.h"
#ifdef NPAPI_WANTED
#include "nputil.h"
#endif
/*****************************************************************************
* DESCRIPTION : Creates an entry in the NDCache table
* 
* INPUTS      : pIf6Entry        -   Pointer to the interface 
*               pIp6Addr      -   Pointer to IPv6 address
*               MacAddr     -   Pointer to lower layer address
*               u1LlaLen   -   Length of lower layer address
*               u1State     -   Reachability state of entry
* 
* OUTPUTS     : None
* 
* RETURNS     : Pointer to NDCache entry, creation succeeds
*               NULL -  NDCache entry creation fails
*****************************************************************************/
PUBLIC tLip6NdEntry *
Lip6NdCreateEntry (tLip6If * pIf6Entry, tIp6Addr * pIp6Addr)
{
    tLip6NdEntry       *pNd6cEntry = NULL;

    if (gIp6GblInfo.u4Nd6CacheEntries == MAX_IP6_ND6_CACHE_ENTRIES)
    {
        LIP6_TRC_ARG (LIP6_MOD_TRC, ALL_FAILURE_TRC,
                      "Max ND entries already present\r\n");
        return (NULL);
    }

    /* Allocate space for the entry */
    pNd6cEntry = (tLip6NdEntry *) MemAllocMemBlk (gIp6GblInfo.NdEntryPoolId);

    if (pNd6cEntry == NULL)
    {
        LIP6_TRC_ARG (LIP6_MOD_TRC, ALL_FAILURE_TRC,
                      "Lip6NdCreateEntry:Memory Allocation FAILED\r\n");
        return (NULL);
    }

    /* Fill the fields of the entry */
    MEMSET (pNd6cEntry, 0, sizeof (tLip6NdEntry));
    Ip6AddrCopy (&pNd6cEntry->Ip6Addr, pIp6Addr);
    pNd6cEntry->pIf6Entry = pIf6Entry;
    pNd6cEntry->u1ReachState = ND6_CACHE_ENTRY_INCOMPLETE;

    /* Set the RowStatus */
    pNd6cEntry->u1RowStatus = NOT_IN_SERVICE;

    /* Update the NDCache last updated time for this entry */
    OsixGetSysTime ((tOsixSysTime *) & (pNd6cEntry->u4LastUpdatedTime));

    if (RBTreeAdd (gIp6GblInfo.Nd6CacheTable, (tRBElem *) pNd6cEntry)
        == RB_FAILURE)
    {
        LIP6_TRC_ARG1 (LIP6_MOD_TRC, ALL_FAILURE_TRC,
                       "Addition of the neighbor cache entry to the "
                       "RBTree FAILED for the address %s \r\n",
                       Ip6PrintNtop (pIp6Addr));
        MemReleaseMemBlock (gIp6GblInfo.NdEntryPoolId, (UINT1 *) pNd6cEntry);
        return NULL;
    }
    gIp6GblInfo.u4Nd6CacheEntries++;
    return (pNd6cEntry);
}

/*****************************************************************************
* DESCRIPTION : Set Reach State for the ND entry
* 
* INPUTS      : pNd6cEntry   -  Pointer to the NDCache Entry
* 
* OUTPUTS     : None
* 
* RETURNS     : OSIX_SUCCESS    -  ND entry deletion succeeds
*             : OSIX_FAILURE    -  Deletion fails
*****************************************************************************/
PUBLIC INT4
Lip6NdSetReachState (tLip6NdEntry * pNd6cEntry, UINT1 u1ReachState)
{
#ifdef NPAPI_WANTED
    UINT1               u1OldState = pNd6cEntry->u1ReachState;
    UINT2               u2VlanId = 0;
#endif
#ifdef NPAPI_WANTED
    if (CfaGetVlanId (pNd6cEntry->pIf6Entry->u4IfIndex,
                      &u2VlanId) == CFA_FAILURE)
    {
        return OSIX_FAILURE;
    }
#endif
    pNd6cEntry->u1ReachState = u1ReachState;
    switch (u1ReachState)
    {
        case ND6_CACHE_ENTRY_STATIC:

            /* Update Linux ND cache for Static case alone */
            Lip6NetLinkNDUpdate (RTM_NEWNEIGH, pNd6cEntry);

            /* Intentional Fallthrough */

        case ND6_CACHE_ENTRY_REACHABLE:
#ifdef NPAPI_WANTED
            Ipv6FsNpIpv6NeighCacheAdd (pNd6cEntry->pIf6Entry->u4ContextId,
                                       (UINT1 *) &pNd6cEntry->Ip6Addr,
                                       pNd6cEntry->pIf6Entry->u4IfIndex,
                                       pNd6cEntry->MacAddr, sizeof (tMacAddr),
                                       NP_IPV6_NH_REACHABLE, u2VlanId);
            /* Scan the routing table for the routes whose next hop is
             * matches with the resolved next hop and populate the routes
             * in the hardware routing table */
            Rtm6ApiAddAllRtWithNxtHopInHWInCxt (pNd6cEntry->pIf6Entry->u4ContextId,
                                                &pNd6cEntry->Ip6Addr);
#endif
            /*Need to check for ECMP routes and program the kernel */
            /*Scan the routing table and program the best path / ECMP 
             *routes in the kernel.
             */
            Rtm6ApiAddAllRtWithNxtHopInHWInLnx (pNd6cEntry->pIf6Entry->u4ContextId,
                                                &pNd6cEntry->Ip6Addr);

            break;

        case ND6_CACHE_ENTRY_STALE:

            /* Modify/Add the NP entry with the Stale State */
#ifdef NPAPI_WANTED
            Ipv6FsNpIpv6NeighCacheAdd (pNd6cEntry->pIf6Entry->u4ContextId,
                                       (UINT1 *) &pNd6cEntry->Ip6Addr,
                                       pNd6cEntry->pIf6Entry->u4IfIndex,
                                       pNd6cEntry->MacAddr, sizeof (tMacAddr),
                                       NP_IPV6_NH_STALE, u2VlanId);
#endif /* NPAPI_WANTED */
            /*TODO Delete all routes with NH in NPAPI */
            Rtm6ApiDelAllRtWithNxtHopInHWInLnx (pNd6cEntry->pIf6Entry->u4ContextId,
                                                &pNd6cEntry->Ip6Addr);

            break;

        case ND6_CACHE_ENTRY_INCOMPLETE:

            /* Modify the NP entry with the Incomplete State if an entry already
             * exists with Reachable State set */
#ifdef NPAPI_WANTED
            if (u1OldState == ND6_CACHE_ENTRY_REACHABLE)
            {
                Ipv6FsNpIpv6NeighCacheAdd (pNd6cEntry->pIf6Entry->u4ContextId,
                                           (UINT1 *) &pNd6cEntry->Ip6Addr,
                                           pNd6cEntry->pIf6Entry->u4IfIndex,
                                           pNd6cEntry->MacAddr,
                                           sizeof (tMacAddr),
                                           NP_IPV6_NH_INCOMPLETE, u2VlanId);
            }
#endif
            Rtm6ApiDelAllRtWithNxtHopInHWInLnx (pNd6cEntry->pIf6Entry->u4ContextId,
                                                &pNd6cEntry->Ip6Addr);
            break;
    }


    return (OSIX_SUCCESS);
}

/*****************************************************************************
* DESCRIPTION : Deletes the NDCache entry
* 
* INPUTS      : pNd6cEntry   -  Pointer to the NDCache Entry
* 
* OUTPUTS     : None
* 
* RETURNS     : OSIX_SUCCESS    -  ND entry deletion succeeds
*             : OSIX_FAILURE    -  Deletion fails
*****************************************************************************/
PUBLIC INT4
Lip6NdDeleteEntry (tLip6NdEntry * pNd6cEntry)
{

    if (pNd6cEntry->u1ReachState == ND6_CACHE_ENTRY_STATIC)
    {
        /* Update Linux ND cache only for Static entries. Dynamic entry deletion
         * is done in Kernel itself */
        Lip6NetLinkNDUpdate (RTM_DELNEIGH, pNd6cEntry);
    }

#ifdef NPAPI_WANTED
    Ipv6FsNpIpv6NeighCacheDel (pNd6cEntry->pIf6Entry->u4ContextId,
                               (UINT1 *) &pNd6cEntry->Ip6Addr,
                               pNd6cEntry->pIf6Entry->u4IfIndex);
#endif /* NPAPI_WANTED */
    /*Need to delete all routes with ND entry */
    Rtm6ApiDelAllRtWithNxtHopInHWInLnx (pNd6cEntry->pIf6Entry->u4ContextId,
                                        &pNd6cEntry->Ip6Addr);

    pNd6cEntry = RBTreeGet (gIp6GblInfo.Nd6CacheTable, (tRBElem *) pNd6cEntry);
    if(pNd6cEntry != NULL)
    {
    RBTreeRem (gIp6GblInfo.Nd6CacheTable, pNd6cEntry);

    if ((MemReleaseMemBlock (gIp6GblInfo.NdEntryPoolId, (UINT1 *) pNd6cEntry))
        == MEM_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    gIp6GblInfo.u4Nd6CacheEntries--;
    }
    return (OSIX_SUCCESS);
}

/*****************************************************************************
DESCRIPTION           Checks for whether a NDCache entry exists for an IPv6 
                      address and if present returns the entry, Otherwise NULL
 

INPUTS                pIf6Entry     -   Pointer to the interface structure
                      pIp6Addr      -   Pointer to Destination IPv6 address

OUTPUTS               None

RETURNS               IP6_SUCCESS  -  Packet queued on NDCache entry
                      IP6_FAILURE  -  NDCache create fails
*****************************************************************************/
tLip6NdEntry       *
Lip6NdGetEntry (tLip6If * pIf6Entry, tIp6Addr * pIp6Addr)
{
    tLip6NdEntry       *pNd6cEntry = NULL;
    tLip6NdEntry        Nd6cEntry;

    MEMSET (&Nd6cEntry, 0, sizeof (tLip6NdEntry));
    Nd6cEntry.pIf6Entry = pIf6Entry;
    Ip6AddrCopy (&(Nd6cEntry.Ip6Addr), pIp6Addr);

    pNd6cEntry = RBTreeGet (gIp6GblInfo.Nd6CacheTable, (tRBElem *) & Nd6cEntry);

    return (pNd6cEntry);
}

/*****************************************************************************
* DESCRIPTION : Returns the next ND Cache table entry present after the
*               input IF,IPv6 address entry
* 
* INPUTS      : i4IfIndex - Interface Index
*               ipv6Addr  - IPv6 Address
* 
* OUTPUTS     : pi4IfIndex - Next Interface Index
*               pipv6Addr  - Next IPv6 Address
* 
* RETURNS     : OSIX_SUCCESS    - if Cache Table has some entries after the
*                                 current entry
*             : OSIX_FAILURE    - No more entries in Cache table  
*****************************************************************************/
INT4
Lip6NdGetNextCacheEntry (INT4 i4IfIndex, INT4 *pi4IfIndex,
                         tIp6Addr ipv6Addr, tIp6Addr * pipv6Addr)
{
    UINT4               u4TempIndex = 0;
    tLip6If            *pIf6 = NULL;
    tIp6Addr            TempAddr;
    tLip6NdEntry       *pNd6cEntry = NULL;
    tLip6NdEntry        Nd6cEntry;

    MEMSET (&TempAddr, 0, IP6_ADDR_SIZE);

    if (i4IfIndex > LIP6_MAX_INTERFACES)
    {
        return OSIX_FAILURE;
    }

    pIf6 = Lip6UtlGetIfEntry (i4IfIndex);
    if (pIf6 == NULL)
    {
        /* Get the next possible entry from the IF Table */
        for (u4TempIndex = i4IfIndex + 1; u4TempIndex < LIP6_MAX_INTERFACES;
             u4TempIndex++)
        {
            if (gIp6GblInfo.apIp6If[u4TempIndex] != NULL)
            {
                pIf6 = gIp6GblInfo.apIp6If[u4TempIndex];
                break;
            }
        }
        if (u4TempIndex > LIP6_MAX_INTERFACES)
        {
            return OSIX_FAILURE;
        }
    }

    MEMSET (&Nd6cEntry, 0, sizeof (tLip6NdEntry));

    Nd6cEntry.pIf6Entry = pIf6;
    Ip6AddrCopy (&Nd6cEntry.Ip6Addr, &ipv6Addr);

    pNd6cEntry = RBTreeGetNext (gIp6GblInfo.Nd6CacheTable,
                                (tRBElem *) & Nd6cEntry, NULL);

    if (pNd6cEntry != NULL)
    {
        *pi4IfIndex = pNd6cEntry->pIf6Entry->u4IfIndex;
        Ip6AddrCopy (pipv6Addr, &pNd6cEntry->Ip6Addr);
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/*****************************************************************************
* DESCRIPTION : This function handles the interface up/down indications
*               and updates the ND Cache table entries (dynamic/static) 
*
* INPUTS      : pIf6Entry - Pointer to the IF entry
* 
* OUTPUTS     : None
* 
* RETURNS     : None
*
*****************************************************************************/
VOID
Lip6NdHandleIfStateChange (tLip6If * pIf6Entry)
{
    tLip6NdEntry       *pNdEntry = NULL;
    tLip6NdEntry       *pNextNdEntry = NULL;

    /* Logic here is to scan the entrire ND Cache table and remove the dynamic 
     * entries from the Table, NP and Kernel. Static entries are removed from 
     * NP and Kernel but are retained in Cache Table. When the Oper State goes
     * UP, these entries are used to program NP and Kernel again */

    LIP6_ND_CACHE_SCAN (gIp6GblInfo.Nd6CacheTable, pNdEntry, pNextNdEntry,
                        tLip6NdEntry *)
    {
        if (pNdEntry->pIf6Entry->u4IfIndex == pIf6Entry->u4IfIndex)
        {
            if (pIf6Entry->u1OperStatus == NETIPV6_OPER_DOWN)
            {
                if (pNdEntry->u1ReachState != ND6_CACHE_ENTRY_STATIC)
                {
                    Lip6NdDeleteEntry (pNdEntry);
                    Lip6NetLinkNDUpdate (RTM_DELNEIGH, pNdEntry);
                }
                else
                {
                    /* Update Linux ND cache */
                    Lip6NetLinkNDUpdate (RTM_DELNEIGH, pNdEntry);

#ifdef NPAPI_WANTED
                    Ipv6FsNpIpv6NeighCacheDel (pNdEntry->pIf6Entry->u4ContextId,
                                               (UINT1 *) &pNdEntry->Ip6Addr,
                                               pNdEntry->pIf6Entry->u4IfIndex);
#endif /* NPAPI_WANTED */
                }
            }
            else
            {
                /* Only Static Entries would be preset in the ND Table for this
                 * Interface. Program them into NP and Kernel */
                Lip6NdSetReachState (pNdEntry, ND6_CACHE_ENTRY_STATIC);
            }
        }
    }

    return;
}
/*****************************************************************************
* DESCRIPTION : Delete the ND entry from the kernel
*
* INPUTS      : pIf6Entry        -   Pointer to the interface
*               pIp6Addr      -   Pointer to IPv6 address
*               MacAddr     -   Pointer to lower layer address
*
* OUTPUTS     : None
*
* RETURNS     : NOne
*****************************************************************************/
VOID
Lip6NdDelelteEntryFromKernel (tLip6If * pIf6Entry, tIp6Addr * pIp6Addr, tMacAddr * pMacAddr)
{
    tLip6NdEntry       *pNd6cEntry = NULL;

    if (gIp6GblInfo.u4Nd6CacheEntries == MAX_IP6_ND6_CACHE_ENTRIES)
    {
        LIP6_TRC_ARG (LIP6_MOD_TRC, ALL_FAILURE_TRC,
                      "Max ND entries already present\r\n");
        return;
    }
    
    /* Allocate space for the entry */
    pNd6cEntry = (tLip6NdEntry *) MemAllocMemBlk (gIp6GblInfo.NdEntryPoolId);

    if (pNd6cEntry == NULL)
    {
        LIP6_TRC_ARG (LIP6_MOD_TRC, ALL_FAILURE_TRC,
                      "Lip6NdCreateEntry:Memory Allocation FAILED\r\n");
        return;
    }
    MEMSET (pNd6cEntry, 0, sizeof (tLip6NdEntry));
    Ip6AddrCopy (&pNd6cEntry->Ip6Addr, pIp6Addr);
    pNd6cEntry->pIf6Entry = pIf6Entry;
    MEMCPY (&pNd6cEntry->MacAddr, pMacAddr, sizeof (tMacAddr));

    /* Indication to kernel to delete the ND Entry */ 
    Lip6NetLinkNDUpdate (RTM_DELNEIGH, pNd6cEntry);

    MemReleaseMemBlock (gIp6GblInfo.NdEntryPoolId, (UINT1 *) pNd6cEntry);
}
#endif /* _LIP6ND_C */
/* END OF FILE */
