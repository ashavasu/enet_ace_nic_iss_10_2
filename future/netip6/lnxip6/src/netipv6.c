/****************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: netipv6.c,v 1.34 2016/02/22 12:06:24 siva Exp $
 *
 * Description: Standardised Ipv6 Interface with HL protocols
 ***************************************************************************/
#ifndef _NETIPV6_C_
#define _NETIPV6_C_

#include "lip6inc.h"
#ifdef VRRP_WANTED
#include "vrrp.h"
#endif
#ifdef NPAPI_WANTED
#include "ipnpwr.h"
#include "nputil.h"
#endif

PRIVATE INT4        LnxIpv6GetIPPort (UINT4 u4IfIndex, UINT4 *pu4IpPort);
/******************************************************************************
 * Function Name    :   NetIpv6GetIfInfo
 * Description      :   This function provides the IPv6 interface information
 *                      for the given interface index
 * Inputs           :   u4IfIndex - Interface index
 * Outputs          :   pNetIpv6IfInfo - Pointer filled with interface information
 * Return Value     :   NETIPV6_SUCCESS or NETIPV6_FAILURE
 ******************************************************************************/
PUBLIC INT4
NetIpv6GetIfInfo (UINT4 u4IfIndex, tNetIpv6IfInfo * pNetIpv6IfInfo)
{
    INT4                i4Status = OSIX_FAILURE;
    UINT4               u4ContextId = VCM_DEFAULT_CONTEXT;

#if defined(VRF_WANTED) && defined(LINUX_310_WANTED)
    Ip6GetCxtId (u4IfIndex, &u4ContextId); 
#endif
    if( u4ContextId == VCM_DEFAULT_CONTEXT )
    {
	    if (gIp6GblInfo.i4Ip6Status == OSIX_FALSE)
	    {
		    LIP6_TRC_ARG (NETIP6_MOD_TRC, ALL_FAILURE_TRC,
				    "NetIpv6GetIfInfo : IP6 Module not initialised!\r\n");
		    return NETIPV6_FAILURE;
	    }
    }
    else
    {
	    if (gIp6GblInfo.apIp6Cxt[u4ContextId] != NULL )
	    {
		    if ((gIp6GblInfo.apIp6Cxt[u4ContextId])->u4ForwFlag == OSIX_FALSE)
		    {
			    LIP6_TRC_ARG (NETIP6_MOD_TRC, ALL_FAILURE_TRC,
					    "NetIpv6GetIfInfo : IP6 Module not initialised!\r\n");
			    return NETIPV6_FAILURE;
		    }
	    }

    }

    if (pNetIpv6IfInfo == NULL)
    {
        LIP6_TRC_ARG (NETIP6_MOD_TRC, ALL_FAILURE_TRC,
                      "NetIpv6GetIfInfo : NULL argument\r\n");
        return NETIPV6_FAILURE;
    }

    Ip6Lock ();

    i4Status = Lip6UtlGetIfInfo (u4IfIndex, pNetIpv6IfInfo);

    Ip6UnLock ();

    if (i4Status == OSIX_FAILURE)
    {
        return NETIPV6_FAILURE;
    }

    return NETIPV6_SUCCESS;
}

/******************************************************************************
 * Function Name    :   NetIpv6GetFirstIfInfo
 * Description      :   This function provides the first entry information in
 *                      the interface  table
 * Inputs           :   None
 * Outputs          :   pNetIpv6IfInfo - First interface information
 * Return Value     :   NETIPV6_SUCCESS | NETIPV6_FAILURE
 ******************************************************************************/
PUBLIC INT4
NetIpv6GetFirstIfInfo (tNetIpv6IfInfo * pNetIpv6IfInfo)
{
    UINT4               u4IfIndex = 0;
    INT4                i4Status = OSIX_FAILURE;

    UINT4               u4ContextId = VCM_DEFAULT_CONTEXT;

#if defined(VRF_WANTED) && defined(LINUX_310_WANTED)
    Ip6GetCxtId ( pNetIpv6IfInfo->u4IfIndex, &u4ContextId); 
#endif
    if( u4ContextId == VCM_DEFAULT_CONTEXT )
    {

	    if (gIp6GblInfo.i4Ip6Status == OSIX_FALSE)
	    {
		    LIP6_TRC_ARG (NETIP6_MOD_TRC, ALL_FAILURE_TRC,
				    "NetIpv6GetFirstIfInfo:IP6 module Not Initialised!!\r\n");
		    return NETIPV6_FAILURE;
	    }
    }
    else
    {
	    if (gIp6GblInfo.apIp6Cxt[u4ContextId] != NULL )
	    {
		    if ((gIp6GblInfo.apIp6Cxt[u4ContextId])->u4ForwFlag == OSIX_FALSE)
		    {
			    LIP6_TRC_ARG (NETIP6_MOD_TRC, ALL_FAILURE_TRC,
					    "NetIpv6GetFirstIfInfo:IP6 module Not Initialised!!\r\n");
			    return NETIPV6_FAILURE;
		    }
	    }

    }

    Ip6Lock ();

    if (Lip6UtlIfGetNextIndex (0, &u4IfIndex) == OSIX_FAILURE)
    {
        Ip6UnLock ();
        return NETIPV6_FAILURE;
    }

    i4Status = Lip6UtlGetIfInfo (u4IfIndex, pNetIpv6IfInfo);

    Ip6UnLock ();

    if (i4Status == OSIX_FAILURE)
    {
        return NETIPV6_FAILURE;
    }

    return NETIPV6_SUCCESS;
}

/******************************************************************************
 * Function Name    :   NetIpv6GetNextIfInfo
 * Description      :   This function returns the next interface information
 *                      of the input interface index
 * Inputs           :   u4IfIndex - Interface index
 * Outputs          :   pNextNetIpv6IfInfo - Interface information of the
 *                      next interface index
 * Return Value     :   NETIPV6_SUCCESS | NETIPV6_FAILURE
 ******************************************************************************/
PUBLIC INT4
NetIpv6GetNextIfInfo (UINT4 u4IfIndex, tNetIpv6IfInfo * pNextNetIpv6IfInfo)
{
    UINT4               u4NextIndex = 0;
    INT4                i4Status = OSIX_FAILURE;

    UINT4               u4ContextId = VCM_DEFAULT_CONTEXT;

#if defined(VRF_WANTED) && defined(LINUX_310_WANTED)
    Ip6GetCxtId ( u4IfIndex, &u4ContextId);
#endif
    if( u4ContextId == VCM_DEFAULT_CONTEXT )
    {

	    if (gIp6GblInfo.i4Ip6Status == OSIX_FALSE)
	    {
		    LIP6_TRC_ARG (NETIP6_MOD_TRC, ALL_FAILURE_TRC,
				    "NetIpv6GetNextIfInfo : IP6 module Not Initialised\r\n");
		    return NETIPV6_FAILURE;
	    }
    }
    else
    {
	    if (gIp6GblInfo.apIp6Cxt[u4ContextId] != NULL )
	    {
		    if ((gIp6GblInfo.apIp6Cxt[u4ContextId])->u4ForwFlag == OSIX_FALSE)
		    {
			    LIP6_TRC_ARG (NETIP6_MOD_TRC, ALL_FAILURE_TRC,
					    "NetIpv6GetNextIfInfo : IP6 module Not Initialised\r\n");
			    return NETIPV6_FAILURE;
		    }
	    }
    }

    Ip6Lock ();

    if (Lip6UtlIfGetNextIndex (u4IfIndex, &u4NextIndex) != OSIX_SUCCESS)
    {
        Ip6UnLock ();
        LIP6_TRC_ARG (NETIP6_MOD_TRC, ALL_FAILURE_TRC,
                      "NetIpv6GetNextIfInfo:No Next Interface Index!\r\n");
        return NETIPV6_FAILURE;
    }

    i4Status = Lip6UtlGetIfInfo (u4NextIndex, pNextNetIpv6IfInfo);

    Ip6UnLock ();

    if (i4Status == OSIX_FAILURE)
    {
        return NETIPV6_FAILURE;
    }

    return NETIPV6_SUCCESS;
}

/******************************************************************************
 * Function Name    :   NetIpv6InformSpeedChange
 * Description      :   This function is used by the lower layer to inform IPv6
 *                      about the change in the speed of the link
 * Inputs           :   u4IfIndex - Interface index number
 *                  :   u4IfSpeed - Speed of the interface
 * Outputs          :   None
 * Return Value     :   None
 ******************************************************************************/
PUBLIC VOID
NetIpv6InformSpeedChange (UINT4 u4IfIndex, UINT4 u4IfSpeed)
{
    NetIpv6InvokeInterfaceStatusChange (u4IfIndex,
                                        NETIPV6_ALL_PROTO,
                                        u4IfSpeed,
                                        NETIPV6_INTERFACE_SPEED_CHANGE);
    return;
}

/******************************************************************************
 * Function Name    :   NetIpv6InformHighSpeedChange
 * Description      :   This function is used by the lower layer to inform IPv6
 *                      about the change in the speed of the link
 * Inputs           :   u4IfIndex - Interface index number
 *                  :   u4HighIfSpeed - Speed of the interface
 * Outputs          :   None
 * Return Value     :   None
 ******************************************************************************/
PUBLIC VOID
NetIpv6InformHighSpeedChange (UINT4 u4IfIndex, UINT4 u4IfHighSpeed)
{
    NetIpv6InvokeInterfaceStatusChange (u4IfIndex,
                                        NETIPV6_ALL_PROTO,
                                        u4IfHighSpeed,
                                        NETIPV6_INTERFACE_HIGH_SPEED_CHANGE);
    return;
}

/******************************************************************************
 * Function Name    :   NetIpv6GetFirstIfAddr
 * Description      :   This function provides the first address of the
 *                      interface
 * Inputs           :   u4IfIndex - Interface index
 * Outputs          :   pNetIpv6AddrInfo - First address information
 * Return Value     :   NETIPV6_SUCCESS | NETIPV6_FAILURE
 ******************************************************************************/
PUBLIC INT4
NetIpv6GetFirstIfAddr (UINT4 u4IfIndex, tNetIpv6AddrInfo * pNetIpv6AddrInfo)
{
    tLip6If            *pIf6Entry = NULL;
    tLip6AddrNode      *pIp6AddrNode = NULL;

    UINT4               u4ContextId = VCM_DEFAULT_CONTEXT;

#if defined(VRF_WANTED) && defined(LINUX_310_WANTED)
    Ip6GetCxtId ( u4IfIndex, &u4ContextId); 
#endif
    if( u4ContextId == VCM_DEFAULT_CONTEXT )
    {
	    if (gIp6GblInfo.i4Ip6Status == OSIX_FALSE)
	    {
		    LIP6_TRC_ARG (NETIP6_MOD_TRC, ALL_FAILURE_TRC, "NetIpv6GetFirstIfAddr "
				    ": IP6 module Not Initialised !\r\n");
		    return NETIPV6_FAILURE;
	    }
    }
    else
    {
	    if (gIp6GblInfo.apIp6Cxt[u4ContextId] != NULL )
	    {
		    if ((gIp6GblInfo.apIp6Cxt[u4ContextId])->u4ForwFlag == OSIX_FALSE)
		    {
			    LIP6_TRC_ARG (NETIP6_MOD_TRC, ALL_FAILURE_TRC, "NetIpv6GetFirstIfAddr "
					    ": IP6 module Not Initialised !\r\n");
			    return NETIPV6_FAILURE;
		    }
	    }
    }

    Ip6Lock ();

    if ((pIf6Entry = Lip6UtlGetIfEntry (u4IfIndex)) == NULL)
    {
        Ip6UnLock ();
        LIP6_TRC_ARG (NETIP6_MOD_TRC, ALL_FAILURE_TRC,
                      "NetIpv6GetFirstIfAddr : Invalid Interface Index !\r\n");
        return NETIPV6_FAILURE;
    }

    /* Fetch the first address assigned to the interface */

    pIp6AddrNode = (tLip6AddrNode *) TMO_SLL_First (&(pIf6Entry->Ip6AddrList));

    if (pIp6AddrNode == NULL)
    {
        /* Check if any link Local address is configured */
        pIp6AddrNode =
            (tLip6AddrNode *) TMO_SLL_First (&(pIf6Entry->Ip6LLAddrList));
        if (pIp6AddrNode == NULL)
        {
            LIP6_TRC_ARG (NETIP6_MOD_TRC, MGMT_TRC,
                          "NetIpv6GetFirstIfAddr : No Address is Configured !\r\n");
            Ip6UnLock ();
            return NETIPV6_FAILURE;
        }
    }

    MEMCPY (&pNetIpv6AddrInfo->Ip6Addr, &(pIp6AddrNode->Ip6Addr),
            sizeof (tIp6Addr));
    pNetIpv6AddrInfo->u4PrefixLength = (UINT4) pIp6AddrNode->i4PrefixLen;
    pNetIpv6AddrInfo->u4Type = ADDR6_UNICAST;

    Ip6UnLock ();
    LIP6_TRC_ARG (NETIP6_MOD_TRC, ALL_FAILURE_TRC,
                  "NetIpv6GetFirstIfAddr : No Address is Configured !\r\n");
    return NETIPV6_SUCCESS;
}

/******************************************************************************
 * Function Name    :   NetIpv6GetNextIfAddr
 * Description      :   This function provides the next address of the
 *                      interface
 * Inputs           :   u4IfIndex - Interface index
 *                  :   pNetIpv6AddrInfo - Address information
 * Outputs          :   pNetIpv6NextAddrInfo - Next address information
 * Return Value     :   NETIPV6_SUCCESS | NETIPV6_FAILURE
 ******************************************************************************/

PUBLIC INT4
NetIpv6GetNextIfAddr (UINT4 u4IfIndex,
                      tNetIpv6AddrInfo * pNetIpv6AddrInfo,
                      tNetIpv6AddrInfo * pNetIpv6NextAddrInfo)
{
    tIp6Addr            NextIp6Addr;
    UINT4               u4NextIndex = 0;
    INT4                i4NextPrefix = 0;
    UINT4               u4ContextId = VCM_DEFAULT_CONTEXT;

#if defined(VRF_WANTED) && defined(LINUX_310_WANTED)
    Ip6GetCxtId ( u4IfIndex, &u4ContextId);
#endif
    if( u4ContextId == VCM_DEFAULT_CONTEXT )
    {
	    if (gIp6GblInfo.i4Ip6Status == OSIX_FALSE)
	    {
		    LIP6_TRC_ARG (NETIP6_MOD_TRC, ALL_FAILURE_TRC,
				    "NetIpv6GetNextIfAddr : IP6 module Not Initialised\r\n");
		    return NETIPV6_FAILURE;
	    }
    }
    else
    {
	    if (gIp6GblInfo.apIp6Cxt[u4ContextId] != NULL )
	    {
		    if ((gIp6GblInfo.apIp6Cxt[u4ContextId])->u4ForwFlag == OSIX_FALSE)
		    {
			    LIP6_TRC_ARG (NETIP6_MOD_TRC, ALL_FAILURE_TRC,
					    "NetIpv6GetNextIfAddr : IP6 module Not Initialised\r\n");
			    return NETIPV6_FAILURE;
		    }
	    }
    }

    MEMSET (&NextIp6Addr, 0, sizeof (NextIp6Addr));

    Ip6Lock ();
    Lip6AddrGetNext (u4IfIndex, &pNetIpv6AddrInfo->Ip6Addr,
                     &u4NextIndex, &NextIp6Addr, &i4NextPrefix);
    Ip6UnLock ();

    if (u4IfIndex == u4NextIndex)
    {
        /* Next address present for same interface */
        MEMCPY (&pNetIpv6NextAddrInfo->Ip6Addr, &NextIp6Addr,
                sizeof (tIp6Addr));
        pNetIpv6NextAddrInfo->u4PrefixLength = (UINT4) i4NextPrefix;
        pNetIpv6NextAddrInfo->u4Type = ADDR6_UNICAST;
        return NETIPV6_SUCCESS;
    }

    return NETIPV6_FAILURE;
}

/******************************************************************************
 * Function Name    :   NetIpv6RegisterHigherLayerProtocol
 * Description      :   This function registers the protocol specified by
 *                      the application
 * Inputs           :   u4Proto - Protocol identifier
 *                  :   u4Mask  - Bit that identify the function pointer type
 *                  :   pFp     - Function pointer to be registered
 * Outputs          :   None
 * Return Value     :   NETIPV6_SUCCESS | NETIPV6_FAILURE
 ******************************************************************************/
PUBLIC INT4
NetIpv6RegisterHigherLayerProtocol (UINT4 u4Proto, UINT4 u4Mask, VOID *pFp)
{
    return (NetIpv6RegisterHigherLayerProtocolInCxt (VCM_DEFAULT_CONTEXT,
                                                     u4Proto, u4Mask, pFp));
}

/******************************************************************************
 * Function Name    :   NetIpv6RegisterHigherLayerProtocolInCxt
 * Description      :   This function registers the protocol specified by
 *                      the application for each context
 * Inputs           :   u4ContextId - context Identifier
 *                  :   u4Proto - Protocol identifier
 *                  :   u4Mask  - Bit that identify the function pointer type
 *                  :   pFp     - Function pointer to be registered
 * Outputs          :   None
 * Return Value     :   NETIPV6_SUCCESS | NETIPV6_FAILURE
 ******************************************************************************/

PUBLIC INT4
NetIpv6RegisterHigherLayerProtocolInCxt (UINT4 u4ContextId, UINT4 u4Proto,
                                         UINT4 u4Mask, VOID *pFp)
{
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    tLip6Cxt            *pLip6Cxt = NULL;
#else
    UNUSED_PARAM (u4ContextId);
#endif

    if( u4ContextId == VCM_DEFAULT_CONTEXT )
    {
	    if (gIp6GblInfo.i4Ip6Status == OSIX_FALSE)
	    {
		    return NETIPV6_FAILURE;
	    }
    }
    else
    {
	    if (gIp6GblInfo.apIp6Cxt[u4ContextId] != NULL )
	    {
		    if ((gIp6GblInfo.apIp6Cxt[u4ContextId])->u4ForwFlag == OSIX_FALSE)
		    {
			    return NETIPV6_FAILURE;
		    }
	    }
    }

    if (u4Mask == 0)
    {
        return NETIPV6_FAILURE;
    }

    Ip6Lock ();

    if (u4Proto >= IP6_MAX_PROTOCOLS)
    {
        Ip6UnLock ();
        return NETIPV6_FAILURE;
    }

    if (pFp == NULL)
    {
        LIP6_TRC_ARG (NETIP6_MOD_TRC, ALL_FAILURE_TRC,
                      "NetIpv6RegisterHigherLayerProtocol : Function Pointer "
                      "is NULL !\r\n");
        Ip6UnLock ();
        return NETIPV6_FAILURE;
    }
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    if(u4ContextId)
    {
        pLip6Cxt = LnxVrfIpv6UtilGetCxtEntryFromCxtId (u4ContextId);
        if (pLip6Cxt != NULL)
        {
            if ((pLip6Cxt->au1HLRegTable[u4Proto] & u4Mask) == u4Mask)
            {
                /* This protocol is already registered for the given
                 * application in this context */
                IP6_TASK_UNLOCK ();
                return (NETIPV6_SUCCESS);

            }
            if (pLip6Cxt->au1HLRegTable[u4Proto] == 0)
            {
                gaNetIpv6RegTable[u4Proto].u1NumOfReg++;
            }
            pLip6Cxt->au1HLRegTable[u4Proto] |= u4Mask;
        }
        else
        {
            /* Registration in invalid context */
            IP6_TASK_UNLOCK ();
            return (NETIPV6_FAILURE);
        }
    }
#endif

    /* If this is the first context registrating for this
     * proto means, update gaNetIpv6Reg*/
    if ((u4Mask & NETIPV6_APPLICATION_RECEIVE)
        && (gaNetIpv6RegTable[u4Proto].pAppRcv == NULL))
    {
        LIP6_TRC_ARG (NETIP6_MOD_TRC, ALL_FAILURE_TRC,
                      "NetIpv6RegisterHigherLayerProtocol : Application Send "
                      "and Receive Registered !\r\n");
        gaNetIpv6RegTable[u4Proto].pAppRcv =
            (VOID (*)(tNetIpv6HliParams * pNetIpv6HlParams)) pFp;
    }

    if ((u4Mask & NETIPV6_ADDRESS_CHANGE)
        && (gaNetIpv6RegTable[u4Proto].pAddrChange == NULL))
    {
        LIP6_TRC_ARG (NETIP6_MOD_TRC, ALL_FAILURE_TRC,
                      "NetIpv6RegisterHigherLayerProtocol : Address Change "
                      "Registered !\r\n");
        gaNetIpv6RegTable[u4Proto].pAddrChange =
            (VOID (*)(tNetIpv6HliParams * pNetIpv6HlParams)) pFp;
    }

    if ((u4Mask & NETIPV6_ROUTE_CHANGE)
        && (gaNetIpv6RegTable[u4Proto].pRouteChange == NULL))
    {
        LIP6_TRC_ARG (NETIP6_MOD_TRC, ALL_FAILURE_TRC,
                      "NetIpv6RegisterHigherLayerProtocol : Route Change "
                      "Registered !\r\n");
        gaNetIpv6RegTable[u4Proto].pRouteChange =
            (VOID (*)(tNetIpv6HliParams * pNetIpv6HlParams)) pFp;
        Lip6MainActOnHLRegDereg (IP6_REGISTER, u4Proto, NETIPV6_ROUTE_CHANGE);
    }

    if ((u4Mask & NETIPV6_INTERFACE_PARAMETER_CHANGE)
        && (gaNetIpv6RegTable[u4Proto].pIfChange == NULL))
    {
        LIP6_TRC_ARG (NETIP6_MOD_TRC, ALL_FAILURE_TRC,
                      "NetIpv6RegisterHigherLayerProtocol : Interface Change "
                      "Registered !\r\n");
        gaNetIpv6RegTable[u4Proto].pIfChange =
            (VOID (*)(tNetIpv6HliParams * pNetIpv6HlParams)) pFp;
        Lip6MainActOnHLRegDereg (IP6_REGISTER, u4Proto,
                                 NETIPV6_INTERFACE_PARAMETER_CHANGE);
    }
#if !(defined (VRF_WANTED) && defined (LINUX_310_WANTED))
    gaNetIpv6RegTable[u4Proto].u1NumOfReg++;
#endif

    Ip6UnLock ();
    return NETIPV6_SUCCESS;
}

/******************************************************************************
 * Function Name    :   NetIpv6DeRegisterHigherLayerProtocol
 * Description      :   This function de-registers the protocol specified by
 *                      the application
 * Inputs           :   u4Proto - Protocol identifier
 * Outputs          :   None
 * Return Value     :   NETIPV6_SUCCESS | NETIPV6_FAILURE
 ******************************************************************************/
PUBLIC INT4
NetIpv6DeRegisterHigherLayerProtocol (UINT4 u4Proto)
{
    return (NetIpv6DeRegisterHigherLayerProtocolInCxt (VCM_DEFAULT_CONTEXT,
                                                       u4Proto));
}

/******************************************************************************
 * Function Name    :   NetIpv6DeRegisterHigherLayerProtocolInCxt
 * Description      :   This function de-registers the protocol specified by
 *                      the application per context
 * Inputs           :   u4ContextId - Context Identifier
 *                  :   u4Proto - Protocol identifier
 * Outputs          :   None
 * Return Value     :   NETIPV6_SUCCESS | NETIPV6_FAILURE
 ******************************************************************************/
PUBLIC INT4
NetIpv6DeRegisterHigherLayerProtocolInCxt (UINT4 u4ContextId, UINT4 u4Proto)
{
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    tLip6Cxt            *pLip6Cxt = NULL;
#else
    UNUSED_PARAM (u4ContextId);
#endif
    if( u4ContextId == VCM_DEFAULT_CONTEXT )
    {
	    if (gIp6GblInfo.i4Ip6Status == OSIX_FALSE)
	    {
		    LIP6_TRC_ARG (NETIP6_MOD_TRC, ALL_FAILURE_TRC,
				    "NetIpv6DeRegisterHigherLayerProtocol : IP6 module Not "
				    "Initialised !\r\n");
		    return NETIPV6_FAILURE;
	    }
    }
    else
    {
	    if (gIp6GblInfo.apIp6Cxt[u4ContextId] != NULL )
	    {
		    if ((gIp6GblInfo.apIp6Cxt[u4ContextId])->u4ForwFlag == OSIX_FALSE)
		    {    
			    LIP6_TRC_ARG (NETIP6_MOD_TRC, ALL_FAILURE_TRC,
					    "NetIpv6DeRegisterHigherLayerProtocol : IP6 module Not "
					    "Initialised !\r\n");

			    return NETIPV6_FAILURE;
		    }
	    }
    }

    Ip6Lock ();

    if (u4Proto >= IP6_MAX_PROTOCOLS)
    {
        LIP6_TRC_ARG (NETIP6_MOD_TRC, ALL_FAILURE_TRC,
                      "NetIpv6DeRegisterHigherLayerProtocol : Invalid Protocol "
                      "ID !\r\n");
        Ip6UnLock ();
        return NETIPV6_FAILURE;
    }

#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    pLip6Cxt = LnxVrfIpv6UtilGetCxtEntryFromCxtId (u4ContextId);
    if (pLip6Cxt != NULL)
    {
        if (pLip6Cxt->au1HLRegTable[u4Proto] == NETIPV6_REG_DISABLE)
        {
            /* This protocol has already de-registered for the given context */
            IP6_TASK_UNLOCK ();
            return (NETIPV6_SUCCESS);

        }

        pLip6Cxt->au1HLRegTable[u4Proto] = NETIPV6_REG_DISABLE;
    }
    else
    {
        IP6_TASK_UNLOCK ();
        return (NETIPV6_FAILURE);
    }
#endif
    gaNetIpv6RegTable[u4Proto].u1NumOfReg--;
    if (gaNetIpv6RegTable[u4Proto].u1NumOfReg == 0)
    {
        gaNetIpv6RegTable[u4Proto].pAppRcv = NULL;
        gaNetIpv6RegTable[u4Proto].pAddrChange = NULL;
        gaNetIpv6RegTable[u4Proto].pRouteChange = NULL;
        gaNetIpv6RegTable[u4Proto].pIfChange = NULL;
        gaNetIpv6RegTable[u4Proto].pIcmp6ErrSnd = NULL;
        LIP6_TRC_ARG1 (NETIP6_MOD_TRC, ALL_FAILURE_TRC,
                       "NetIpv6DeRegisterHigherLayerProtocol : Protocol [%d] "
                       "DeRegistration Success !\r\n", u4Proto);
    }
    Ip6UnLock ();
    return NETIPV6_SUCCESS;
}

/******************************************************************************
 * Function Name    :   NetIpv6InvokeApplicationReceive
 * Description      :   This function invokes the registered application Send
 *                      and receive call back function to post received
 *                      IPv6 packet.
 *                      This API may not used for Linux IP since the protocols
 *                      can get the packet directly from the socket
 * Inputs           :   pBuf     - Buffer Pointer
 *                  :   u4PktLen - Packet Length of the Buffer
 *                  :   u4Index  - Interface Index
 *                  :   u4Type   - Address Type
 *                  :   u4Proto  - Protocol identifier
 * Outputs          :   None
 * Return Value     :   NETIPV6_SUCCESS | NETIPV6_FAILURE
 *****************************************************************************/

INT4
NetIpv6InvokeApplicationReceive (tCRU_BUF_CHAIN_HEADER * pBuf,
                                 UINT4 u4PktLen, UINT4 u4Index,
                                 UINT4 u4Type, UINT4 u4Proto)
{
    /* This should not have been a netip api */
    tNetIpv6HliParams   Ip6HlParams;

    LIP6_TRC_ARG (NETIP6_MOD_TRC, DATA_PATH_TRC,
                  "NetIpv6InvokeApplicationReceive : Invoke Trace !!!\n");

    if (pBuf == NULL)
    {
        LIP6_TRC_ARG (NETIP6_MOD_TRC, DATA_PATH_TRC,
                      "NetIpv6InvokeApplicationReceive : Buffer Pointer NULL !!!\n");
        return NETIPV6_FAILURE;
    }

    if (u4Proto >= IP6_MAX_PROTOCOLS)
    {
        LIP6_TRC_ARG (NETIP6_MOD_TRC, DATA_PATH_TRC,
                      "NetIpv6InvokeApplicationReceive : Invalid Protocol ID !!!\n");
        return NETIPV6_FAILURE;
    }

    MEMSET (&Ip6HlParams, ZERO, sizeof (tNetIpv6HliParams));
    if (gaNetIpv6RegTable[u4Proto].pAppRcv)
    {
        Ip6HlParams.unIpv6HlCmdType.AppRcv.pBuf = pBuf;
        Ip6HlParams.unIpv6HlCmdType.AppRcv.u4PktLength = u4PktLen;
        Ip6HlParams.unIpv6HlCmdType.AppRcv.u4Index = u4Index;
        Ip6HlParams.unIpv6HlCmdType.AppRcv.u4Type = u4Type;
        Ip6HlParams.u4Command = NETIPV6_APPLICATION_RECEIVE;
        gaNetIpv6RegTable[u4Proto].pAppRcv (&Ip6HlParams);
        return NETIPV6_SUCCESS;
    }

    return NETIPV6_FAILURE;
}

/******************************************************************************
 * Function Name    :   NetIpv6InvokeAddressChange
 * Description      :   This function invokes the registered Address Status
 *                      Change call back function
                        This should not have been a netip api.
 *                      
 * Inputs           :   pIp6Addr - Pointer to the Address
 *                  :   u4PreLen - Prefix Length of the Address
 *                  :   u4Type   - Type of the Address
 *                  :   u4Index  - Interface Index
 *                  :   u4Mask   - Type of Address Status Change
 * Outputs          :   None
 * Return Value     :   NETIPV6_SUCCESS | NETIPV6_FAILURE
 ******************************************************************************/
PUBLIC INT4
NetIpv6InvokeAddressChange (tIp6Addr * pIp6Addr,
                            UINT4 u4PreLen,
                            UINT4 u4Type, UINT4 u4Index, UINT4 u4Mask)
{
    tNetIpv6HliParams   Ip6HlParams;
    UINT4               u4Proto = 0;
#ifdef NPAPI_WANTED
    UINT4               u4NpAddrType = 0;
    UINT4               u4ContextId = VCM_DEFAULT_CONTEXT;
#endif /* NPAPI_WANTED */

    MEMSET (&Ip6HlParams, 0, sizeof (tNetIpv6HliParams));
    MEMCPY (&(Ip6HlParams.unIpv6HlCmdType.AddrChange.Ipv6AddrInfo.Ip6Addr),
            (pIp6Addr), sizeof (tIp6Addr));
    Ip6HlParams.unIpv6HlCmdType.AddrChange.Ipv6AddrInfo.u4PrefixLength =
        u4PreLen;
    Ip6HlParams.unIpv6HlCmdType.AddrChange.Ipv6AddrInfo.u4Type = u4Type;
    Ip6HlParams.unIpv6HlCmdType.AddrChange.u4Index = u4Index;
    Ip6HlParams.unIpv6HlCmdType.AddrChange.u4Mask = u4Mask;
    Ip6HlParams.u4Command = NETIPV6_ADDRESS_CHANGE;

    for (u4Proto = 0; u4Proto < IP6_MAX_PROTOCOLS; u4Proto++)
    {
        if (gaNetIpv6RegTable[u4Proto].pAddrChange)
        {
            gaNetIpv6RegTable[u4Proto].pAddrChange (&Ip6HlParams);
        }
    }

#ifdef NPAPI_WANTED
    if (Ip6GetCxtId (u4Index, &u4ContextId) == IP6_FAILURE)
    {
        return NETIPV6_FAILURE;         
    }
    
    switch (u4Type)
    {
        case ADDR6_UNICAST:
        case ADDR6_ANYCAST:
        case ADDR6_LLOCAL:
        case ADDR6_V4_COMPAT:
            u4NpAddrType = NP_IP6_UNI_ADDR;
            break;

        case ADDR6_MULTI:
            u4NpAddrType = NP_IP6_MULTI_ADDR;
            break;

        default:
            /* Other address types not supported. */
            u4NpAddrType = 0;
    }

    if (u4NpAddrType != 0)
    {
        if (u4Mask == NETIPV6_ADDRESS_ADD)
        {
            Ipv6FsNpIpv6AddrCreate (u4ContextId, u4Index, u4NpAddrType,
                                    (UINT1 *) pIp6Addr, (UINT1) u4PreLen);
        }
        else
        {
            Ipv6FsNpIpv6AddrDelete (u4ContextId, u4Index, u4NpAddrType,
                                    (UINT1 *) pIp6Addr, (UINT1) u4PreLen);
        }
    }
#endif /* NPAPI_WANTED */

    return NETIPV6_SUCCESS;
}

/******************************************************************************
 * Function Name    :   NetIpv6InvokeRouteChange
 * Description      :   This function invokes the registered Route Status
 *                      Change call back function
 * Inputs           :   pNetIpv6RtInfo - Pointer to Route Change Information
 *                      u4Proto - If equal to 0xFFFFFFFF then send the route
 *                                change notification to all the protocols.
 *                                Else send the notification to the specific
 *                                protocol.
 * Outputs          :   None
 * Return Value     :   NETIPV6_SUCCESS | NETIPV6_FAILURE
 ******************************************************************************/
PUBLIC INT4
NetIpv6InvokeRouteChange (tNetIpv6RtInfo * pNetIpv6RtInfo, UINT4 u4Proto)
{
    /* This API is called from RTM to update route change info. Update 
     * Linux ip also here */
    tNetIpv6HliParams   Ip6HlParams;
    UINT4               u4ProtoIndex = 0;
    INT4                i4RetVal = OSIX_SUCCESS;

    /* Add/Delete routes to Linux kernel. Local routes are added by default
     * when address is configured, so should not be added again */
    if (pNetIpv6RtInfo->i1Proto != IP6_LOCAL_PROTOID)
    {
        if (pNetIpv6RtInfo->u4RowStatus == RTM6_ACTIVE)
        {
            i4RetVal = Lip6NetLinkRouteModify (RTM_NEWROUTE, pNetIpv6RtInfo);
        }
        else
        {
            i4RetVal = Lip6NetLinkRouteModify (RTM_DELROUTE, pNetIpv6RtInfo);
        }

        if (i4RetVal != OSIX_FAILURE)
        {
            return NETIPV6_FAILURE;
        }
    }

    MEMSET (&Ip6HlParams, 0, sizeof (tNetIpv6HliParams));
    MEMCPY (&(Ip6HlParams.unIpv6HlCmdType.RouteChange),
            (pNetIpv6RtInfo), sizeof (tNetIpv6RtInfo));
    Ip6HlParams.u4Command = NETIPV6_ROUTE_CHANGE;

    if ((u4Proto != NETIPV6_ALL_PROTO) && (u4Proto < IP6_MAX_PROTOCOLS))
    {
        /* Verify if the protocol has registered with the IP6 module */
        if (gaNetIpv6RegTable[u4Proto].pRouteChange != NULL)
        {
            gaNetIpv6RegTable[u4Proto].pRouteChange (&Ip6HlParams);
        }
        return NETIPV6_SUCCESS;
    }

    /* Inform all registered protocols */
    for (u4ProtoIndex = 0; u4ProtoIndex < IP6_MAX_PROTOCOLS; u4ProtoIndex++)
    {
        if (gaNetIpv6RegTable[u4ProtoIndex].pRouteChange)
        {
            gaNetIpv6RegTable[u4ProtoIndex].pRouteChange (&Ip6HlParams);
        }
    }

    return NETIPV6_SUCCESS;
}

/******************************************************************************
 * Function Name    :   NetIpv6InvokeRouteChangeForEcmp
 * Description      :   This function invokes programs the kernel with ECMP routes
 *                      It does not however invoke call back functions for registered 
 *                      protocols
 * Inputs           :   pNetIpv6RtInfo - Pointer to Route Change Information
 *                      u4Proto - This variable is dummy as of now. May be used later
 * Outputs          :   None
 * Return Value     :   NETIPV6_SUCCESS | NETIPV6_FAILURE
 ******************************************************************************/
PUBLIC INT4
NetIpv6InvokeRouteChangeForEcmp (tNetIpv6RtInfo * pNetIpv6RtInfo, UINT4 u4Proto)
{
    /* This API is called from RTM to update route change info. Update 
     * Linux ip also here */
    INT4                i4RetVal = OSIX_SUCCESS;

    /* Add/Delete routes to Linux kernel. Local routes are added by default
     * when address is configured, so should not be added again */
    if (pNetIpv6RtInfo->i1Proto != IP6_LOCAL_PROTOID)
    {
        if (pNetIpv6RtInfo->u4RowStatus == RTM6_ACTIVE)
        {
            i4RetVal = Lip6NetLinkRouteModify (RTM_NEWROUTE, pNetIpv6RtInfo);
        }
        else
        {
            i4RetVal = Lip6NetLinkRouteModify (RTM_DELROUTE, pNetIpv6RtInfo);
        }

        if (i4RetVal != OSIX_FAILURE)
        {
            return NETIPV6_FAILURE;
        }
    }

    UNUSED_PARAM (u4Proto);

    return NETIPV6_SUCCESS;
}

/******************************************************************************
 * Function Name    :   NetIpv6InvokeInterfaceStatusChange
 * Description      :   This function invokes the registered Interface Status
 *                      Change call back function
 * Inputs           :   u4Index  - Interface Index
 *                  :   u4Proto  - Protocol ID
 *                  :   u4Value  - Value of Interface Status Change Parameter
 *                  :   u4Mask   - Type of Interface Status Change
 * Outputs          :   None
 * Return Value     :   NETIPV6_SUCCESS | NETIPV6_FAILURE
 ******************************************************************************/
PUBLIC INT4
NetIpv6InvokeInterfaceStatusChange (UINT4 u4Index,
                                    UINT4 u4Proto, UINT4 u4Value, UINT4 u4Mask)
{
    UINT4               u4ProtoIndex;
    UINT4               u4IpPort = 0;
    tNetIpv6HliParams   Ip6HlParams;

    MEMSET (&Ip6HlParams, 0, sizeof (tNetIpv6HliParams));
    Ip6HlParams.unIpv6HlCmdType.IfStatChange.u4Index = u4Index;

    switch (u4Mask)
    {
        case NETIPV6_INTERFACE_MTU_CHANGE:
            Ip6HlParams.unIpv6HlCmdType.IfStatChange.u4Mtu = u4Value;
            break;

        case NETIPV6_INTERFACE_SPEED_CHANGE:
            Ip6HlParams.unIpv6HlCmdType.IfStatChange.u4IfSpeed = u4Value;
            break;

        case NETIPV6_INTERFACE_HIGH_SPEED_CHANGE:
            Ip6HlParams.unIpv6HlCmdType.IfStatChange.u4IfSpeed = u4Value;
            break;

        case NETIPV6_INTERFACE_STATUS_CHANGE:
            Ip6HlParams.unIpv6HlCmdType.IfStatChange.u4IfStat = u4Value;
            LnxIpv6GetIPPort (u4Index, &u4IpPort);
            Ip6HlParams.unIpv6HlCmdType.IfStatChange.u4IpPort = u4IpPort;
            break;

        default:
            LIP6_TRC_ARG (NETIP6_MOD_TRC, ALL_FAILURE_TRC,
                          "NetIpv6InvokeInterfaceStatusChange : "
                          "Invalid Mask !\r\n");
            return NETIPV6_FAILURE;
    }

    Ip6HlParams.unIpv6HlCmdType.IfStatChange.u4Mask = u4Mask;
    Ip6HlParams.u4Command = NETIPV6_INTERFACE_PARAMETER_CHANGE;

    if ((u4Proto != NETIPV6_ALL_PROTO) && (u4Proto < IP6_MAX_PROTOCOLS))
    {
        /* Interface Status Change to be send only the specific protocol */
        if (gaNetIpv6RegTable[u4Proto].pIfChange)
        {
            gaNetIpv6RegTable[u4Proto].pIfChange (&Ip6HlParams);
        }
        return NETIPV6_SUCCESS;
    }

    for (u4ProtoIndex = IP6_ZERO; u4ProtoIndex < IP6_MAX_PROTOCOLS;
         u4ProtoIndex++)
    {
        /* Interface Status Change to be send to all registered protocol */
        if (gaNetIpv6RegTable[u4ProtoIndex].pIfChange)
        {
            gaNetIpv6RegTable[u4ProtoIndex].pIfChange (&Ip6HlParams);
        }
    }

    return NETIPV6_SUCCESS;
}

/******************************************************************************
 * Function Name    :   NetIpv6InvokeIcmpv6ErrorSend
 * Description      :   This function invokes the registered ICMPv6 Error
 *                      Send call back function
 * Inputs           :   pBuf     - Buffer Pointer
 *                  :   Ip6Addr  - Source Address of the ICMPv6 Message
 *                  :   u4Type   - ICMPv6 Message Type
 *                  :   u4Code   - ICMPv6 Message Code
 *                  :   u4PktLen - Packet Length of the Buffer
 *                  :   u4Index  - Interface Index
 *                  :   u4Proto  - Protocol identifier
 * Outputs          :   None
 * Return Value     :   NETIPV6_SUCCESS | NETIPV6_FAILURE
 *****************************************************************************/

INT4
NetIpv6InvokeIcmpv6ErrorSend (tCRU_BUF_CHAIN_HEADER * pBuf,
                              tIp6Addr * Ip6Addr,
                              UINT4 u4Type,
                              UINT4 u4Code,
                              UINT4 u4PktLen, UINT4 u4Index, UINT4 u4Proto)
{
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (Ip6Addr);
    UNUSED_PARAM (u4Type);
    UNUSED_PARAM (u4Code);
    UNUSED_PARAM (u4PktLen);
    UNUSED_PARAM (u4Index);
    UNUSED_PARAM (u4Proto);

    return NETIPV6_SUCCESS;
}

/******************************************************************************
 * Function Name    :   NetIpv6LeakRoute
 * Description      :   This function, depending on the value of u1CmdType Adds
 *                      (or) Deletes (or) modifies the given Route Information
 *                      stored in Ip6FwdTable accordingly.
 * Inputs           :   u1CmdType - Command to ADD | Delete | Modify the Route.
 *                      pNetIpv6RtInfo - Route to be Added | Deleted | Modified.
 * Outputs          :   None.
 * Return Value     :   NETIPV6_SUCCESS | NETIPV6_FAILURE.
 ******************************************************************************/
PUBLIC INT4
NetIpv6LeakRoute (UINT1 u1CmdType, tNetIpv6RtInfo * pNetIpv6RtInfo)
{
    INT4                i4RetVal = NETIPV6_FAILURE;
    UINT4              u4ContextId = VCM_DEFAULT_CONTEXT;

#if defined(VRF_WANTED) && defined(LINUX_310_WANTED)
    Ip6GetCxtId ( pNetIpv6RtInfo->u4Index, &u4ContextId);
#endif 

    if( u4ContextId == VCM_DEFAULT_CONTEXT )
    {
	    if (gIp6GblInfo.i4Ip6Status == OSIX_FALSE)
	    {
		    LIP6_TRC_ARG (NETIP6_MOD_TRC, ALL_FAILURE_TRC,
				    "NetIpv6LeakRoute : IP6 module Not Initialised !\r\n");
		    return NETIPV6_FAILURE;
	    }
    }
    else
    {
	    if (gIp6GblInfo.apIp6Cxt[u4ContextId] != NULL )
	    {
		    if ((gIp6GblInfo.apIp6Cxt[u4ContextId])->u4ForwFlag == OSIX_FALSE)
		    {    
			    LIP6_TRC_ARG (NETIP6_MOD_TRC, ALL_FAILURE_TRC,
					    "NetIpv6LeakRoute : IP6 module Not Initialised !\r\n");

			    return NETIPV6_FAILURE;
		    }
	    }
    }
    if (pNetIpv6RtInfo == NULL)
    {
        return (i4RetVal);
    }

    /* Update RTM about the route. RTM will update Linux kernel and higher
     * layer modules about the routec= change */
    if (Rtm6ApiIpv6LeakRoute (u1CmdType, pNetIpv6RtInfo) == RTM6_FAILURE)
    {
        return NETIPV6_FAILURE;
    }

    if (i4RetVal == OSIX_FAILURE)
    {
        return NETIPV6_FAILURE;
    }

    return NETIPV6_SUCCESS;
}

/******************************************************************************
 * Function Name    :   NetIpv6GetRoute
 * Description      :   This function provides the Route (either Exact Route or
 *                      Best route) for a given destination and Mask based on
 *                      the incoming request.
 * Inputs           :   pNetIpv6RtQuery - Infomation about the route to be
 *                                 retrieved.
 * Outputs          :   pNetIpv6RtInfo - Information about the requested route.
 * Return Value     :   NETIPV6_SUCCESS - if the route is present.
 *                      NETIPV6_FAILURE - if the route is not present.
 *****************************************************************************/
PUBLIC INT4
NetIpv6GetRoute (tNetIpv6RtInfoQueryMsg * pNetIpv6RtQuery,
                 tNetIpv6RtInfo * pNetIpv6RtInfo)
{
    UINT4              u4ContextId = VCM_DEFAULT_CONTEXT;

#if defined(VRF_WANTED) && defined(LINUX_310_WANTED)
    Ip6GetCxtId ( pNetIpv6RtInfo->u4Index, &u4ContextId);
#endif 

    if( u4ContextId == VCM_DEFAULT_CONTEXT )
    {
	    if (gIp6GblInfo.i4Ip6Status == OSIX_FALSE)
	    {
		    LIP6_TRC_ARG (NETIP6_MOD_TRC,
				    ALL_FAILURE_TRC,
				    "NetIpv6GetRoute : IP6 module Not Initialised !\r\n");
		    return NETIPV6_FAILURE;
	    }
    }
    else
    {
	    if (gIp6GblInfo.apIp6Cxt[u4ContextId] != NULL )
	    {
		    if ((gIp6GblInfo.apIp6Cxt[u4ContextId])->u4ForwFlag == OSIX_FALSE)
		    {                               
			    LIP6_TRC_ARG (NETIP6_MOD_TRC,
					    ALL_FAILURE_TRC,
					    "NetIpv6GetRoute : IP6 module Not Initialised !\r\n");
			    return NETIPV6_FAILURE;
		    }
	    }
    }

    if (Rtm6ApiNetIpv6GetRoute (pNetIpv6RtQuery, pNetIpv6RtInfo) ==
        RTM6_FAILURE)
    {
        return NETIPV6_FAILURE;
    }

    return NETIPV6_SUCCESS;
}

/******************************************************************************
 * Function Name    :   NetIpv6GetFwdTableRouteEntry 
 * Description      :   This function provides the Route (Best route) for a
 *                      given destination and Mask.
 * Inputs           :   pIp6DestAddr - The Destination Address.
 *                      u1PrefixLen - Prefix len for the given Destination
 *                                    Address.
 * Outputs          :   pNetIpv6RtInfo - The Route Information for the given
 *                      Destination and Mask.
 * Return Value     :   NETIPV6_SUCCESS | NETIPV6_FAILURE.
 ******************************************************************************/
PUBLIC INT4
NetIpv6GetFwdTableRouteEntry (tIp6Addr * pIp6DestAddr,
                              UINT1 u1PrefixLength,
                              tNetIpv6RtInfo * pNetIpv6RtInfo)
{
    /* For Default context */
    return (NetIpv6GetFwdTableRouteEntryInCxt (VCM_DEFAULT_CONTEXT,
                                               pIp6DestAddr, u1PrefixLength,
                                               pNetIpv6RtInfo));
}

/******************************************************************************
 * Function Name    :   NetIpv6GetFwdTableRouteEntryInCxt 
 * Description      :   This function provides the Route (Best route) for a
 *                      given destination and Mask.
 * Inputs           :   pIp6DestAddr - The Destination Address.
 *                      u1PrefixLen - Prefix len for the given Destination
 *                                    Address.
 * Outputs          :   pNetIpv6RtInfo - The Route Information for the given
 *                      Destination and Mask.
 * Return Value     :   NETIPV6_SUCCESS | NETIPV6_FAILURE.
 ******************************************************************************/
PUBLIC INT4
NetIpv6GetFwdTableRouteEntryInCxt (UINT4 u4ContextId,
                                   tIp6Addr * pIp6DestAddr,
                                   UINT1 u1PrefixLength,
                                   tNetIpv6RtInfo * pNetIpv6RtInfo)
{
    if (Rtm6ApiGetFwdRouteEntryInCxt (u4ContextId, pIp6DestAddr,
                                      u1PrefixLength, pNetIpv6RtInfo)
        == RTM6_FAILURE)
    {
        return NETIPV6_FAILURE;
    }

    return NETIPV6_SUCCESS;
}

/*
 ******************************************************************************
 * Function Name    :   NetIpv6GetFirstFwdTableRouteEntry
 * Description      :   This function provides the First Route Entry present in
 *                      the Ip6 Forwarding Table.
 * Inputs           :   None.
 * Outputs          :   pNetIpv6RtInfo - First Route Entry of the Ip6 Forwarding
 *                      Table.
 * Return Value     :   NETIPV6_SUCCESS | NETIPV6_FAILURE.
 *******************************************************************************
 */
PUBLIC INT4
NetIpv6GetFirstFwdTableRouteEntry (tNetIpv6RtInfo * pNetIpv6RtInfo)
{
    /* For Default context */
    return (NetIpv6GetFirstFwdTableRouteEntryInCxt (VCM_DEFAULT_CONTEXT,
                                                    pNetIpv6RtInfo));
}

/*
 ******************************************************************************
 * Function Name    :   NetIpv6GetFirstFwdTableRouteEntryInCxt
 * Description      :   This function provides the First Route Entry present in
 *                      the Ip6 Forwarding Table in the given context.
 * Inputs           :   u4ContextId - Context identifier.
 * Outputs          :   pNetIpv6RtInfo - First Route Entry of the Ip6 Forwarding
 *                      Table.
 * Return Value     :   NETIPV6_SUCCESS | NETIPV6_FAILURE.
 *******************************************************************************
 */
PUBLIC INT4
NetIpv6GetFirstFwdTableRouteEntryInCxt (UINT4 u4ContextId,
                                        tNetIpv6RtInfo * pNetIpv6RtInfo)
{
    UNUSED_PARAM (pNetIpv6RtInfo);

    if( u4ContextId == VCM_DEFAULT_CONTEXT )
    {

	    if (gIp6GblInfo.i4Ip6Status == OSIX_FALSE)
	    {
		    LIP6_TRC_ARG (NETIP6_MOD_TRC, ALL_FAILURE_TRC,
				    "NetIpv6GetFirstFwdTableRouteEntry: IP6 module"
				    " Not Initialised !\r\n");
		    return NETIPV6_FAILURE;
	    }
    }
    else
    {
	    if (gIp6GblInfo.apIp6Cxt[u4ContextId] != NULL )
	    {
		    if ((gIp6GblInfo.apIp6Cxt[u4ContextId])->u4ForwFlag == OSIX_FALSE)
		    {
			    LIP6_TRC_ARG (NETIP6_MOD_TRC, ALL_FAILURE_TRC,
					    "NetIpv6GetFirstFwdTableRouteEntry: IP6 module"
					    " Not Initialised !\r\n");
			    return NETIPV6_FAILURE;
		    }
	    }
    }

    if (Rtm6ApiTrieGetFirstEntryInCxt (u4ContextId,
                                       pNetIpv6RtInfo) == RTM6_FAILURE)
    {
        return NETIPV6_FAILURE;
    }

    return NETIPV6_SUCCESS;
}

/*
 ******************************************************************************
 * Function Name    :   NetIpv6GetNextFwdTableRouteEntry
 * Description      :   This function returns the Next Route Entry in the Ip6
 *                      Forwarding Table for a given Route Entry.
 * Inputs           :   pNetIpv6RtInfo - Route Entry for which the Next Entry is
 *                      required.
 * Outputs          :   pNextNetRtInfo - Next Route Entry in the Ip Forwarding
 *                      Table.
 * Return Value     :   NETIPV6_SUCCESS | NETIPV6_FAILURE.
 *******************************************************************************
 */

PUBLIC INT4
NetIpv6GetNextFwdTableRouteEntry (tNetIpv6RtInfo * pNetIpv6RtInfo,
                                  tNetIpv6RtInfo * pNextNetIpv6RtInfo)
{
    UINT4              u4ContextId = VCM_DEFAULT_CONTEXT;
    UNUSED_PARAM (pNextNetIpv6RtInfo);
    #if defined(VRF_WANTED) && defined(LINUX_310_WANTED)
       Ip6GetCxtId ( pNetIpv6RtInfo->u4Index, &u4ContextId);
    #endif 
       if( u4ContextId == VCM_DEFAULT_CONTEXT )
       {
	       if (gIp6GblInfo.i4Ip6Status == OSIX_FALSE)
	       {
		       LIP6_TRC_ARG (NETIP6_MOD_TRC, ALL_FAILURE_TRC,
				       "NetIpv6GetNextFwdTableRouteEntry: "
				       "IP6 module Not Initialised !\r\n");
		       return NETIPV6_FAILURE;
	       }
       }
       else
       {
	       if (gIp6GblInfo.apIp6Cxt[u4ContextId] != NULL )
	       {
		       if ((gIp6GblInfo.apIp6Cxt[u4ContextId])->u4ForwFlag == OSIX_FALSE)
		       {
			       LIP6_TRC_ARG (NETIP6_MOD_TRC, ALL_FAILURE_TRC,
					       "NetIpv6GetNextFwdTableRouteEntry: "
					       "IP6 module Not Initialised !\r\n");
			       return NETIPV6_FAILURE;
		       }
	       }
       }

    if (Rtm6ApiTrieGetNextEntry (pNetIpv6RtInfo,
                                 pNextNetIpv6RtInfo) == RTM6_FAILURE)
    {
        LIP6_TRC_ARG (NETIP6_MOD_TRC, ALL_FAILURE_TRC,
                      "NetIpv6GetNextFwdTableRouteEntry: "
                      "Rtm6ApiTrieGetNextEntry failed!\r\n");
        return NETIPV6_FAILURE;
    }

    return NETIPV6_SUCCESS;
}

/*
 ******************************************************************************
 * Function Name    :   NetIpv6McastJoin
 * Description      :   This function register the provided MultiCast Address
 *                      to the specified interface.
 * Inputs           :   pMcastAddr     - Multicast Address to be registered.
 *                      u4IfIndex      - Interface over which this address
 *                                       needs to be registered.
 * Outputs          :   None.
 * Return Value     :   NETIPV6_SUCCESS | NETIPV6_FAILURE.
 *******************************************************************************
 */

PUBLIC INT4
NetIpv6McastJoin (UINT4 u4IfIndex, tIp6Addr * pMcastAddr)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pMcastAddr);

    /* For linux ip, the protocols should do a setsockopt IPV6_ADD_MEMBERSHIP
     * on the socket to receive control packets */

    return NETIPV6_SUCCESS;
}

/*******************************************************************************
 * Function Name    :   NetIpv6McastJoinOnSocket
 * Description      :   This function register the provided MultiCast Address
 *                      to the specified interface.
 * Inputs           :   pMcastAddr     - Multicast Address to be registered.
 *                      u4IfIndex      - Interface over which this address
 *                                       needs to be registered.
 * Outputs          :   None.
 * Return Value     :   NETIPV6_SUCCESS | NETIPV6_FAILURE.
 *******************************************************************************/

PUBLIC INT4
NetIpv6McastJoinOnSocket (INT4 i4SocketId, UINT4 u4IfIndex,
                          tIp6Addr * pMcastGroupAddr)
{
    struct ipv6_mreq    ip6Mreq;

    MEMCPY (&(ip6Mreq.ipv6mr_multiaddr), pMcastGroupAddr, IPVX_IPV6_ADDR_LEN);
    ip6Mreq.ipv6mr_interface = CfaGetIfIpPort (u4IfIndex);
    if (setsockopt
        (i4SocketId, IPPROTO_IPV6, IPV6_JOIN_GROUP,
         &(ip6Mreq), sizeof (struct ipv6_mreq)) < 0)
    {
        return NETIPV6_FAILURE;
    }
    return NETIPV6_SUCCESS;
}

/*****************************************************************************
 * Function Name    :   NetIpv6McastLeaveOnSocket
 * Description      :   This function deregisters the provided MultiCast
 *                      address from the specified interface.
 * Inputs           :   pMcastAddr     - Multicast Address to be deregistered.
 *                      u4IfIndex      - Interface over which this address
 *                                       needs to be deregistered.
 * Outputs          :   None.
 * Return Value     :   NETIPV6_SUCCESS | NETIPV6_FAILURE.
 ****************************************************************************/

PUBLIC INT4
NetIpv6McastLeaveOnSocket (INT4 i4SocketId, UINT4 u4IfIndex,
                           tIp6Addr * pMcastGroupAddr)
{
    struct ipv6_mreq    ip6Mreq;

    MEMCPY (&(ip6Mreq.ipv6mr_multiaddr), pMcastGroupAddr, IPVX_IPV6_ADDR_LEN);
    ip6Mreq.ipv6mr_interface = CfaGetIfIpPort (u4IfIndex);
    if (setsockopt
        (i4SocketId, IPPROTO_IPV6, IPV6_LEAVE_GROUP,
         &(ip6Mreq), sizeof (struct ipv6_mreq)) < 0)
    {
        return NETIPV6_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name    :   NetIpv6McastLeave
 * Description      :   This function deregisters the provided MultiCast
 *                      address from the specified interface.
 * Inputs           :   pMcastAddr     - Multicast Address to be deregistered.
 *                      u4IfIndex      - Interface over which this address
 *                                       needs to be deregistered.
 * Outputs          :   None.
 * Return Value     :   NETIPV6_SUCCESS | NETIPV6_FAILURE.
 ****************************************************************************/

PUBLIC INT4
NetIpv6McastLeave (UINT4 u4IfIndex, tIp6Addr * pMcastAddr)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pMcastAddr);

    /* For linux ip, the protocols should do a setsockopt IPV6_DROP_MEMBERSHIP 
     * on the socket to receive control packets */
    return NETIPV6_SUCCESS;
}

/*
 ******************************************************************************
 * Function Name    :   NetIpv6GetSrcForDest
 * Description      :   Get a global unicast address which is not tentative
 *                      for the given destination address
 * Inputs           :   u4IfIndex      - Interface Index
 *                      pDstAddr       - Destination Address
 * Outputs          :   pSrcAddr       - Soruce Address
 * Return Value     :   NETIPV6_SUCCESS | NETIPV6_FAILURE
 *******************************************************************************
 */

PUBLIC INT4
NetIpv6GetSrcForDest (UINT4 u4IfIndex, tIp6Addr * pDstAddr, tIp6Addr * pSrcAddr)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pDstAddr);
    UNUSED_PARAM (pSrcAddr);

    return NETIPV6_SUCCESS;
}

/******************************************************************************
 * Function Name    :   NetIpv6IsOurAddressInCxt
 * Description      :   Check whether the given address is our addess and
 *                      fetch the interface index of that address
 * Inputs           :   pAddr          - IPv6 Address
 * Outputs          :   pu4IfIndex     - Interface Index
 * Return Value     :   NETIPV6_SUCCESS | NETIPV6_FAILURE
 *****************************************************************************/

PUBLIC INT4
NetIpv6IsOurAddressInCxt (UINT4 u4ContextId, tIp6Addr * pAddr,
                          UINT4 *pu4IfIndex)
{
    UINT4               u4IfIndex = 0;

    UNUSED_PARAM (u4ContextId);

    Ip6Lock ();

    if (Lip6UtlGetIndexForAddr (pAddr, &u4IfIndex, u4ContextId) != OSIX_SUCCESS)
    {
        Ip6UnLock ();
        return NETIPV6_FAILURE;
    }
    *pu4IfIndex = u4IfIndex;

    Ip6UnLock ();
    return NETIPV6_SUCCESS;
}

/******************************************************************************
 * Function Name    :   NetIpv6IsOurAddress
 * Description      :   Check wether the given address is our addess and
 *                      fetch the interface index of that address
 * Inputs           :   pAddr          - IPv6 Address
 * Outputs          :   pu4IfIndex     - Interface Index
 * Return Value     :   NETIPV6_SUCCESS | NETIPV6_FAILURE
 *****************************************************************************/

PUBLIC INT4
NetIpv6IsOurAddress (tIp6Addr * pAddr, UINT4 *pu4IfIndex)
{
    UINT4 u4ContextId = VCM_DEFAULT_CONTEXT;
#if defined(VRF_WANTED) && defined(LINUX_310_WANTED)
    u4ContextId = UtilRtm6GetCurrCxtId ();
#endif
    return (NetIpv6IsOurAddressInCxt (u4ContextId, pAddr, pu4IfIndex));
}

/******************************************************************************
 * Description         : This functions gets the context id associated with the 
 *                      the given IPv6 Interface Index
 *
 * Input(s)           : u4IfIndex - Ipv6 Interface Index
 *
 * Output(s)          : *pu4ContextId - The VR to which the interface is mapped
 *
 * Returns            : NETIPV6_SUCCESS
 ******************************************************************************/
PUBLIC INT4
NetIpv6GetCxtId (UINT4 u4IfIndex, UINT4 *pu4ContextId)
{
#if defined(VRF_WANTED) && defined(LINUX_310_WANTED)
    INT4                i4RetVal = NETIPV6_FAILURE;
    if (IP6_TASK_LOCK () == SNMP_FAILURE)
    {
        LIP6_TRC_ARG (NETIP6_MOD_TRC, ALL_FAILURE_TRC,
                         "NetIpv6GetCxtId : IP6 Task Lock Failed !!!\n");
        return i4RetVal;
    }

    if (Ip6GetCxtId (u4IfIndex, pu4ContextId) == IP6_SUCCESS)
    {
        i4RetVal = NETIPV6_SUCCESS;
    }
    IP6_TASK_UNLOCK ();
    return i4RetVal;

#else
    UNUSED_PARAM (u4IfIndex);
    /* Always default context */
    *pu4ContextId = VCM_DEFAULT_CONTEXT;
    return NETIPV6_SUCCESS;
#endif
}

/******************************************************************************
 * Function           : NetIp6SetIPv6Addr
 * Description        : Set IPv6 Address on an interface
 * Input(s)           : u4Ip6IfIndex - Ipv6 Interface Index
 *                      pAddrInfo    - Pointer to IP6Addr information
 * Output(s)          : None
 * Returns            : NETIPV6_SUCCESS /NETIPV6_FAILURE
 ******************************************************************************/
PUBLIC INT4
NetIp6SetIPv6Addr (UINT4 u4Ip6IfIndex, tNetIpv6AddrInfo * pNetIp6Addr)
{
    INT4                i4RetVal = 0;
    tLip6AddrNode      *pAddrNode = NULL;

    if ((pNetIp6Addr->u4Type != ADDR6_UNICAST) &&
        (pNetIp6Addr->u4Type != ADDR6_ANYCAST))
    {
        /* Unsupported address type */
        return NETIPV6_FAILURE;
    }

    Ip6Lock ();

    i4RetVal = Lip6AddrUpdate (u4Ip6IfIndex, &pNetIp6Addr->Ip6Addr,
                               pNetIp6Addr->u4PrefixLength,
                               IP6FWD_CREATE_AND_WAIT);
    if (i4RetVal != OSIX_SUCCESS)
    {
        Ip6UnLock ();
        return NETIPV6_FAILURE;
    }

    i4RetVal = Lip6AddrUpdate (u4Ip6IfIndex, &pNetIp6Addr->Ip6Addr,
                               (INT4) pNetIp6Addr->u4PrefixLength,
                               IP6FWD_ACTIVE);
    if (i4RetVal != OSIX_SUCCESS)
    {
        Ip6UnLock ();
        return NETIPV6_FAILURE;
    }

    pAddrNode = Lip6AddrGetEntry (u4Ip6IfIndex, &pNetIp6Addr->Ip6Addr,
                                  pNetIp6Addr->u4PrefixLength);

    if (pAddrNode == NULL)
    {
        Ip6UnLock ();
        return NETIPV6_FAILURE;
    }

    pAddrNode->u1AddrType = (UINT1) pNetIp6Addr->u4Type;

    Ip6UnLock ();

    return NETIPV6_SUCCESS;
}

/******************************************************************************
 * Function           : NetIp6DeleteIPv6Addr
 * Description        : Delete IPv6 Address on an interface
 * Input(s)           : u4Ip6IfIndex - Ipv6 Interface Index
 *                      Ip6Addr      - Ipv6 Address
 *                      i4PrefixLen  - Ipv6 prefix length
 *                      i4AddrType    -
 * Output(s)          : None
 * Returns            : NETIPV6_SUCCESS /NETIPV6_FAILURE
 ******************************************************************************/
PUBLIC INT4
NetIp6DeleteIPv6Addr (UINT4 u4Ip6IfIndex, tNetIpv6AddrInfo * pNetIp6Addr)
{
    INT4                i4RetVal = 0;

    Ip6Lock ();

    i4RetVal = Lip6AddrUpdate (u4Ip6IfIndex, &pNetIp6Addr->Ip6Addr,
                               (INT4) pNetIp6Addr->u4PrefixLength,
                               IP6FWD_DESTROY);

    Ip6UnLock ();

    if (i4RetVal != OSIX_SUCCESS)
    {
        return NETIPV6_FAILURE;
    }

    return NETIPV6_SUCCESS;
}

/******************************************************************************
 * Function Name    :   NetIpv6GetIfIndexFromNameInCxt
 * Description      :   This function returns the Interface index for the
 *                      given Interface name
 * Inputs           :   u4L2ContextId - Layer 2 context Id for the Interface
 *                  :   au1IfName - Interface name
 * Outputs          :   u4IfIndex - Interface index of the given Ipv6 address
 * Return Value     :   NETIPV6_SUCCESS | NETIPV6_FAILURE
 *****************************************************************************/
PUBLIC INT4
NetIpv6GetIfIndexFromNameInCxt (UINT4 u4L2ContextId, UINT1 *au1IfName,
                                UINT4 *pu4Index)
{
	UINT4              u4ContextId = VCM_DEFAULT_CONTEXT;

#if defined(VRF_WANTED) && defined(LINUX_310_WANTED)
    u4ContextId = Lip6GetCurrentContext();
#endif 
    if( u4ContextId == VCM_DEFAULT_CONTEXT )
    {
	    if (gIp6GblInfo.i4Ip6Status == OSIX_FALSE)
	    {
		    LIP6_TRC_ARG (NETIP6_MOD_TRC, ALL_FAILURE_TRC,
				    "NetIpv6GetIfIndexFromName : IP6 module Not Initialised !\r\n");
		    return NETIPV6_FAILURE;
	    }
    }
    else
    {
	    if (gIp6GblInfo.apIp6Cxt[u4ContextId] != NULL )
	    {
		    if ((gIp6GblInfo.apIp6Cxt[u4ContextId])->u4ForwFlag == OSIX_FALSE)
		    {
			    LIP6_TRC_ARG (NETIP6_MOD_TRC, ALL_FAILURE_TRC,
					    "NetIpv6GetIfIndexFromName : IP6 module Not Initialised !\r\n");
			    return NETIPV6_FAILURE;
		    }
	    }
    }
    Ip6Lock ();

    if (Lip6PortGetIfIndexFromNameInCxt (u4L2ContextId, au1IfName, pu4Index) ==
        OSIX_SUCCESS)
    {
        Ip6UnLock ();
        return NETIPV6_SUCCESS;
    }
    LIP6_TRC_ARG (NETIP6_MOD_TRC, ALL_FAILURE_TRC,
                  "NetIpv6GetIfIndexFromName : Failed to Get Index From Name !\r\n");
    Ip6UnLock ();
    return NETIPV6_FAILURE;
}

/*****************************************************************************
 * Function Name    : NetIpv6RegisterHLProtocolForMCastPkts
 * Description      :   This function registers the protocol specified by
 *                      the application
 * Inputs           :   pAppRcv - Function pointer to be called when mulitcast
 *                                packet needs to be passed to higher layer.
 * Outputs          :   None
 * Return Value     :   NETIPV6_FAILURE or Registered Application Id
 *****************************************************************************/

INT4
NetIpv6RegisterHLProtocolForMCastPkts (VOID (*pAppRcv)
                                       (tCRU_BUF_CHAIN_HEADER *))
{
    INT4                i4RetVal = NETIPV6_FAILURE;
    UINT1               u1Index = 0;

    for (u1Index = 0; u1Index < IP6_MAX_MCAST_PROTOCOLS; u1Index++)
    {
        if (gaNetIpv6MCastRegTable[u1Index].u1RegFlag == IP6_DEREGISTER)
        {
            break;
        }
    }

    i4RetVal = NetIpv6RegisterHLProtocolForMCastPktsInCxt (VCM_DEFAULT_CONTEXT,
                                                           u1Index, pAppRcv);
    if (i4RetVal == NETIPV6_FAILURE)
    {
        return NETIPV6_FAILURE;
    }
    else
    {
        return u1Index;
    }
}

/*
 ******************************************************************************
 * Function Name    : NetIpv6RegisterHLProtocolForMCastPktsInCxt
 * Description      :   This function registers the protocol specified by
 *                      the application
 * Inputs           : pAppRcv - Function pointer to be called when mulitcast 
 *                    packet needs to be passed to higher layer.
 * Outputs          :   None
 * Return Value     :   NETIPV6_FAILURE or Registered Application Id
 *******************************************************************************
 */

INT4
NetIpv6RegisterHLProtocolForMCastPktsInCxt (UINT4 u4ContextId, UINT1 u1Proto,
                                            VOID (*pAppRcv)
                                            (tCRU_BUF_CHAIN_HEADER *))
{
    UNUSED_PARAM (u4ContextId);

    if (pAppRcv == NULL)
    {
        return (NETIPV6_FAILURE);
    }

    if (u1Proto >= IP6_MAX_MCAST_PROTOCOLS)
    {
        return NETIPV6_FAILURE;
    }

    if (gaNetIpv6MCastRegTable[u1Proto].u1RegCntr == 0)
    {
        gaNetIpv6MCastRegTable[u1Proto].u1RegFlag = IP6_REGISTER;
        gaNetIpv6MCastRegTable[u1Proto].pAppRcv =
            (VOID (*)(tIP_BUF_CHAIN_HEADER *)) pAppRcv;
    }

    gaNetIpv6MCastRegTable[u1Proto].u1RegCntr++;

    return NETIPV6_SUCCESS;
}

/*
 ******************************************************************************
 * Function Name    :   NetIpv6DeRegisterHLProtocolForMCastPkts
 * Description      :   This function de-registers the protocol specified by
 *                      the application
 * Inputs           :   u1AppId- Application Id to be de-registered
 * Outputs          :   None
 * Return Value     :   NETIPV6_SUCCESS | NETIPV6_FAILURE
 *******************************************************************************
 */

INT4
NetIpv6DeRegisterHLProtocolForMCastPkts (UINT1 u1AppId)
{
    return (NetIpv6DeRegisterHLProtocolForMCastPktsInCxt (VCM_DEFAULT_CONTEXT,
                                                          u1AppId));
}

/*
 ******************************************************************************
 * Function Name    :   NetIpv6DeRegisterHLProtocolForMCastPktsInCxt
 * Description      :   This function de-registers the protocol specified by
 *                      the application
 * Inputs           :   u1AppId- Application Id to be de-registered
 * Outputs          :   None
 * Return Value     :   NETIPV6_SUCCESS | NETIPV6_FAILURE
 *******************************************************************************
 */

INT4
NetIpv6DeRegisterHLProtocolForMCastPktsInCxt (UINT4 u4ContextId, UINT1 u1AppId)
{
    UNUSED_PARAM (u4ContextId);

    if (u1AppId >= IP6_MAX_MCAST_PROTOCOLS)
    {
        return NETIPV6_FAILURE;
    }

    gaNetIpv6MCastRegTable[u1AppId].u1RegCntr--;
    if (gaNetIpv6MCastRegTable[u1AppId].u1RegCntr == 0)
    {
        gaNetIpv6MCastRegTable[u1AppId].u1RegFlag = IP6_DEREGISTER;
        gaNetIpv6MCastRegTable[u1AppId].pAppRcv = NULL;
    }

    return NETIPV6_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : This function can be used by external modules to get the
 *               source address corresponding to the destination address
 *               it will be used while sending the packet.
 * INPUTS      : Context id (u4ContextId)
 *               IPv6 destination addr ptr  (pDstAddr) and
 *               IPv6 source addr ptr (pSrcAddr),
 * OUTPUTS     : None
 * RETURNS     : NETIPV6_SUCCESS / NETIPV6_FAILURE
 * NOTES       :
 ******************************************************************************/
INT4
NetIpv6GetSrcAddrForDestAddr (UINT4 u4ContextId, tIp6Addr * pDstAddr,
                              tIp6Addr * pSrcAddr)
{
    tNetIpv6RtInfo      NetIp6RtInfo;
    tNetIpv6RtInfoQueryMsg RtQuery;
    tLip6If            *pIf6Entry = NULL;
    tLip6AddrNode      *pSearchNode = NULL;
    tTMO_SLL           *pIp6AddrList = NULL;

    MEMSET (&NetIp6RtInfo, 0, sizeof (tNetIpv6RtInfo));
    MEMSET (&RtQuery, 0, sizeof (tNetIpv6RtInfoQueryMsg));

    RtQuery.u4ContextId = u4ContextId;
    RtQuery.u1QueryFlag = RTM6_QUERIED_FOR_NEXT_HOP;
    Ip6AddrCopy (&NetIp6RtInfo.Ip6Dst, pDstAddr);
    NetIp6RtInfo.u1Prefixlen = IP6_ADDR_MAX_PREFIX;

    if (NetIpv6GetRoute (&RtQuery, &NetIp6RtInfo) == NETIPV6_FAILURE)
    {
        return NETIPV6_FAILURE;
    }

    Ip6Lock ();

    pIf6Entry = Lip6UtlGetIfEntry (NetIp6RtInfo.u4Index);
    if (pIf6Entry == NULL)
    {
        Ip6UnLock ();
        return NETIPV6_FAILURE;
    }

    /* Get the address list to be used based on the address type */
    if (Ip6AddrType (pDstAddr) != ADDR6_LLOCAL)
    {
        pIp6AddrList = &(pIf6Entry->Ip6AddrList);
    }
    else
    {
        pIp6AddrList = &(pIf6Entry->Ip6LLAddrList);
    }

    TMO_SLL_Scan (pIp6AddrList, pSearchNode, tLip6AddrNode *)
    {
        if ((Ip6AddrMatch (&(pSearchNode->Ip6Addr), pDstAddr,
                           (pSearchNode->i4PrefixLen)) == TRUE))
        {
            Ip6AddrCopy (pSrcAddr, &(pSearchNode->Ip6Addr));
            Ip6UnLock ();
            return NETIPV6_SUCCESS;
        }
    }

    pSearchNode = (tLip6AddrNode *) TMO_SLL_First (pIp6AddrList);

    if (pSearchNode != NULL)
    {
        Ip6AddrCopy (pSrcAddr, &(pSearchNode->Ip6Addr));
        Ip6UnLock ();
        return NETIPV6_SUCCESS;
    }

    Ip6UnLock ();
    return NETIPV6_FAILURE;
}

/******************************************************************************
 * Function Name    :   NetIpv6UpdateNDCache
 * Description      :   This function gets the ND indications from kernel (via
 *                      Netlink sockets) and updates the ND Cache Table
 * Inputs           :   u4IfIndex   - Interface index
 *                      pIp6Addr    - IP Address
 *                      pMacAddr    - Dynamically learnt MAC
 *                      u1NdState   - State of the entry
 *                      u2NlMsgType - NetLink Message Type
 * Outputs          :   None
 * Return Value     :   None
 ******************************************************************************/

PUBLIC VOID
NetIpv6UpdateNDCache (UINT4 u4IfIndex, tIp6Addr * pIp6Addr, tMacAddr * pMacAddr,
                      UINT1 u1NdState, UINT2 u2NlMsgType)
{
    tLip6If            *pIf6Entry = NULL;
    tLip6NdEntry       *pNd6cEntry = NULL;
    tUtlIn6Addr         InAddr;
    UINT4               u4ContextId = VCM_DEFAULT_CONTEXT;

#if defined(VRF_WANTED) && defined(LINUX_310_WANTED)
    u4ContextId = Lip6GetCurrentContext();
#endif 
    if( u4ContextId == VCM_DEFAULT_CONTEXT )
    {

	    if (gIp6GblInfo.i4Ip6Status == OSIX_FALSE)
	    {
		    LIP6_TRC_ARG (NETIP6_MOD_TRC, ALL_FAILURE_TRC,
				    "NetIpv6GetIfInfo : IP6 Module not initialised!\r\n");
		    return;
	    }
    }
    else
    {
	    if (gIp6GblInfo.apIp6Cxt[u4ContextId] != NULL )
	    {
		    if ((gIp6GblInfo.apIp6Cxt[u4ContextId])->u4ForwFlag == OSIX_FALSE)
		    {
			    LIP6_TRC_ARG (NETIP6_MOD_TRC, ALL_FAILURE_TRC,
					    "NetIpv6GetIfInfo : IP6 Module not initialised!\r\n");
			    return;
		    }
	    }
    }
    if ((u2NlMsgType == RTM_GETADDR) || (u2NlMsgType == RTM_DELADDR)
        || (u2NlMsgType == RTM_NEWADDR))
    {
        switch (u2NlMsgType)
        {
            case RTM_NEWADDR:
                Lip6NetlinkParseAddrsFromLnx (u4IfIndex);
                break;
            case RTM_DELADDR:
                break;
            case RTM_GETADDR:
                break;
            default:
                break;
        }
        return;
    }
    pIf6Entry = Lip6UtlGetIfEntry (u4IfIndex);
    if (pIf6Entry == NULL)
    {
        LIP6_TRC_ARG (LIP6_MOD_TRC, CONTROL_PLANE_TRC,
                      "Ipv6GetIfInfo : Invalid Interface Index !!!\n");
        return;
    }

    MEMCPY (&InAddr, pIp6Addr, sizeof (tIp6Addr));

    Ip6Lock ();

    pNd6cEntry = Lip6NdGetEntry (pIf6Entry, pIp6Addr);

    switch (u1NdState)
    {
        case NUD_PERMANENT:
            /* Handled in Low Level Code */
            break;

        case NUD_REACHABLE:
            LIP6_TRC_ARG1 (LIP6_MOD_TRC, CONTROL_PLANE_TRC,
                           "ARP notification received for %s as"
                           "NUD_REACHABLE state.\n", INET_NTOA6 (InAddr));

            if (pNd6cEntry == NULL)
            {
                if ((pNd6cEntry =
                     Lip6NdCreateEntry (pIf6Entry, pIp6Addr)) == NULL)
                {
                    break;
                }
            }

	    if(pNd6cEntry->u1ReachState != ND6_CACHE_ENTRY_REACHABLE)
	    {
		    MEMCPY (&pNd6cEntry->MacAddr, pMacAddr, sizeof (tMacAddr));
		    Lip6NdSetReachState (pNd6cEntry, ND6_CACHE_ENTRY_REACHABLE);
	    }
        /* Resetting the NDRetry count when the ND state become reachable */
            break;

        case NUD_INCOMPLETE:
            LIP6_TRC_ARG1 (LIP6_MOD_TRC, CONTROL_PLANE_TRC,
                           "ARP notification received for %s as"
                           "NUD_INCOMPLETE state.\n", INET_NTOA6 (InAddr));

            if (pNd6cEntry == NULL)
            {
                Lip6NdCreateEntry (pIf6Entry, pIp6Addr);
            }
            break;

        case NUD_STALE:

            /* This state is notified for two cases
             * 1.Dyanamically deleted Entries.
             * 2.Entries learned Automatically from the incoming packets */

            LIP6_TRC_ARG1 (LIP6_MOD_TRC, CONTROL_PLANE_TRC,
                           "ARP notification received for %s as"
                           "NUD_STALE state.\n", INET_NTOA6 (InAddr));
            if (pNd6cEntry != NULL)
            {
                /* Delete the entry in the kernel neighbour cache
                   (u2NlMsgType == RTM_NEWNEIGH) Added for work around patch for the kernel bug. kernel
                   does not give indication to ISS at the time of relearning the neighbour entry */
                if ((u2NlMsgType == RTM_DELNEIGH)
                    || (u2NlMsgType == RTM_NEWNEIGH))
                {
           if (pNd6cEntry->u1ReachState != ND6_CACHE_ENTRY_STATIC)
                    {
               if (Lip6Ipv6CheckHitOnNDCacheEntry (u4IfIndex, pIp6Addr) == IP6_SUCCESS)
                    {
                        Lip6NetLinkNDUpdate (RTM_DELNEIGH, pNd6cEntry);
                    }
               else
                    {
                            Lip6NetLinkNDUpdate (RTM_DELNEIGH, pNd6cEntry);
                            Lip6NdDeleteEntry (pNd6cEntry);
                    }
                    }
                    break;
                }
            }
            else
            {
                /* Delete the entry in the kernel ND cache
                   This is work around patch for the kernel bug.
                   kernel does not give indication to ISS at the time of relearning
                   the ND entry */
                Lip6NdDelelteEntryFromKernel(pIf6Entry, pIp6Addr, pMacAddr);
            }
            break;

        case NUD_FAILED:
            LIP6_TRC_ARG1 (LIP6_MOD_TRC, CONTROL_PLANE_TRC,
                           "ARP notification received for %s as"
                           "NUD_FAILED state.\n", INET_NTOA6 (InAddr));
        if (pNd6cEntry != NULL)
        {
           if (pNd6cEntry->u1ReachState != ND6_CACHE_ENTRY_STATIC)
           {
               pNd6cEntry = RBTreeGet (gIp6GblInfo.Nd6CacheTable, (tRBElem *) pNd6cEntry);
               if (pNd6cEntry != NULL)
               {
                    if (Lip6Ipv6CheckHitOnNDCacheEntry (u4IfIndex, pIp6Addr) == IP6_SUCCESS)
                    {
                           Lip6NdDeleteEntry (pNd6cEntry);
                        Lip6NlResolveNd (u4IfIndex, pIp6Addr);
                    }
               }
               else
               {
                   Lip6NdDeleteEntry (pNd6cEntry);
               }
           }
        }
            break;
        default:
            break;
    }

    Ip6UnLock ();

    return;
}

/**************************************************************************
* Description      :  This function checks for the presence of duplicate address
*                    by comparing and setting the Address status as Duplicate.
*
* Inputs            : u4IfIndex   - Interface index
*                    pIp6Addr    - IP Address
*                    pMacAddr    - Dynamically learnt MAC
*                    u1AddrState - Status of Address
* Outputs          :   None
* Return Value     :   None
* ******************************************************************************/

PUBLIC VOID
NetIpv6UpdateIntfAddrStatus (UINT4 u4IfIndex, tIp6Addr Ip6Addr,
                             UINT1 u1AddrState)
{
    tLip6If            *pIf6Entry = NULL;
    tLip6AddrNode      *pSearchNode = NULL;
    tIp6Addr            OutPrefix;

    pIf6Entry = Lip6UtlGetIfEntry (u4IfIndex);
    if (pIf6Entry == NULL)
    {
        LIP6_TRC_ARG (LIP6_MOD_TRC, CONTROL_PLANE_TRC,
                      "Ipv6GetIfInfo : Invalid Interface Index !!!\n");
        return;
    }

    TMO_SLL_Scan (&(pIf6Entry->Ip6LLAddrList), pSearchNode, tLip6AddrNode *)
    {
        if (Ip6AddrCompare (Ip6Addr, pSearchNode->Ip6Addr) == IP6_ZERO)
        {
            pSearchNode->u1Status = u1AddrState;
            if (pIf6Entry->u1AdminStatus == NETIPV6_ADMIN_UP)
            {
                if ((u1AddrState & IFA_F_PERMANENT) == IFA_F_PERMANENT)
                {
                    if (Lip6KernSetIfForwarding
                        (pIf6Entry->au1IfName,
                         LIP6_FORW_ENABLE) != OSIX_SUCCESS)
                    {
                        return;
                    }
                    pIf6Entry->u1Ipv6IfFwdOperStatus = LIP6_FORW_ENABLE;

                    /* Program configured Ipv6 addresses to kernel */
                    if (Lip6AddrEnableAll (pIf6Entry) == OSIX_FAILURE)
                    {
                        return;
                    }

                    Lip6NdHandleIfStateChange (pIf6Entry);

                    /* Indicate to the Higher layer that an interface is UP */
                    NetIpv6InvokeInterfaceStatusChange (pIf6Entry->u4IfIndex,
                                                        NETIPV6_ALL_PROTO,
                                                        NETIPV6_IF_UP,
                                                        NETIPV6_INTERFACE_STATUS_CHANGE);
                    pIf6Entry->u1OperStatus = NETIPV6_OPER_UP;
                }
                if ((u1AddrState & IFA_F_TENTATIVE) == IFA_F_TENTATIVE)
                {
                    NetIpv6InvokeInterfaceStatusChange (pIf6Entry->u4IfIndex,
                                                        NETIPV6_ALL_PROTO,
                                                        NETIPV6_IF_DOWN,
                                                        NETIPV6_INTERFACE_STATUS_CHANGE);
                    pIf6Entry->u1OperStatus = NETIPV6_OPER_DOWN;
                }
                if (pIf6Entry->u1IfType != IP6_TUNNEL_INTERFACE_TYPE)
                {
                    /* Update RA after deleting the prefixes */
                    if (pIf6Entry->u1RAdvStatus == IP6_IF_ROUT_ADV_ENABLED)
                    {
                        Lip6RAConfig (pIf6Entry->u4ContextId);
                    }
                }

            }
            break;
        }
    }
    TMO_SLL_Scan (&(pIf6Entry->Ip6AddrList), pSearchNode, tLip6AddrNode *)
    {
        if (Ip6AddrCompare (Ip6Addr, pSearchNode->Ip6Addr) == IP6_ZERO)
        {
            pSearchNode->u1Status = u1AddrState;
            if ((u1AddrState & IFA_F_TENTATIVE) == IFA_F_TENTATIVE)
            {
                MEMSET (&OutPrefix, 0, sizeof (tIp6Addr));
                Ip6CopyAddrBits (&OutPrefix, &pSearchNode->Ip6Addr,
                                 pSearchNode->i4PrefixLen);
                Lip6AddrDelLocalRoute (pIf6Entry->u4IfIndex, &OutPrefix,
                                       pSearchNode->i4PrefixLen);
            }
            break;
        }
    }
    return;
}

/*-------------------------------------------------------------------+
 *  * Function           : NetIpvi6GetCfaIfIndexFromPort
 *  * *                   Provides the CFA IfIndex Corresponding to IP Port No
 * *
 * * Input(s)           : u4Port - IP Port Number
 * * Output(s)          : CFA Interface Index
 * * Returns            : NETIPV6_SUCCESS or NETIPV6_FAILURE
 * *
 * * Action             : Provides CFA IFIndex Corresponding to IP Port No
 *         ------------------------------------------------------------------- */
INT4
NetIpv6GetCfaIfIndexFromPort (UINT4 u4Port, UINT4 *pu4IfIndex)
{
    INT4                i4RetVal = NETIPV6_FAILURE;
    tLip6If            *pIf6 = NULL;

    *pu4IfIndex = CFA_INVALID_INDEX;

    pIf6 = Lip6UtlGetIfEntryForPort (u4Port);

    if (NULL != pIf6)
    {
        *pu4IfIndex = pIf6->u4IfIndex;
        i4RetVal = NETIPV6_SUCCESS;

    }

    return (i4RetVal);
}

/*-----------------------------------------------------------------------------+
 * Function           : NetIpv6GetPortFromCfaIfIndex
 *
 * Description        : Provides the IP Port Number for the CFA IfIndex
 *
 * Input(s)           : u4IfIndex - CFA Interface Index
 *
 * Output(s)          : *pu4Port  - IP Port Number
 *
 * Returns            : NETIPV6_SUCCESS or NETIPV6_FAILURE
 *----------------------------------------------------------------------------- */
INT4
NetIpv6GetPortFromCfaIfIndex (UINT4 u4IfIndex, UINT4 *pu4Port)
{
    return (LnxIpv6GetIPPort (u4IfIndex, pu4Port));
}

/******************************************************************************
 * Function Name    :   LnxIpv6GetIPPort
 * Description      :   This function provides the IP port number 
 *                      for the given interface index
 * Inputs           :   u4IfIndex - Interface index
 * Outputs          :   pu4IpPort- IP port for the given
 *                      interface index
 * Return Value     :   NETIPV6_SUCCESS or NETIPV6_FAILURE
 ******************************************************************************/
PRIVATE INT4
LnxIpv6GetIPPort (UINT4 u4IfIndex, UINT4 *pu4IpPort)
{
    tLip6If            *pIf6 = NULL;

    pIf6 = Lip6UtlGetIfEntry (u4IfIndex);
    if (pIf6 == NULL)
    {
        return NETIPV6_FAILURE;
    }
    else
    {
        *pu4IpPort = pIf6->u4IpPort;
        return NETIPV6_SUCCESS;
    }
}

/******************************************************************************
 * Function Name    :   NetIpv6GetIfIndexFromIp6Address
 * Description      :   This function can be used by external modules to get
 *                      the Interface Index using the given IPv6 address
 * Inputs           :   u4ContextId - Context id
 * Outputs          :   pIpAddr    - IPv6 address pointer
 * Return Value     :   NETIPV6_SUCCESS or NETIPV6_FAILURE
*******************************************************************************/
INT4
NetIpv6GetIfIndexFromIp6Address (tSNMP_OCTET_STRING_TYPE * pIpAddressAddr,
                                 UINT4 *u4IfIndex)
{
    UNUSED_PARAM (pIpAddressAddr);
    UNUSED_PARAM (u4IfIndex);
    return NETIPV6_SUCCESS;
}

/******************************************************************************
 * Function Name    :   NetIpv6ValidateUnicastPrefix
 * Description      :   This function can be used to validate 
 *                      the Unicast IPv6 prefix embedded in the 
 *                      given multicast ipv6 address.
 * Inputs           :   u4ContextId - Context id
 *                      u4IfIndex   - Interface index
 *                      pMcastAddr  - IPv6 multicast address 
 * Outputs          :   None
 * Return Value     :   NETIPV6_SUCCESS or NETIPV6_FAILURE
 *******************************************************************************/
INT4
NetIpv6ValidateUnicastPrefix (UINT4 u4ContextId, UINT4 u4IfIndex,
                              tIp6Addr * pMcastAddr)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pMcastAddr);
    return NETIPV6_SUCCESS;
}

/******************************************************************************
 * Function Name    :   NetIpv6ValidateRpPrefixInCxt
 * Description      :   This function can be used by external modules
 *                      to validate the RP address
 *                      embedded in the given multicast ipv6 address.
 * Inputs           :   u4ContextId - Context id
 *                      u4IfIndex   - Interface index
 *                      pMcastAddr  - IPv6 multicast address
 * Outputs          :   pIp6RpAddrInfo  - RP Address related information
 * Return Value     :   NETIPV6_SUCCESS or NETIPV6_FAILURE
 *******************************************************************************/
INT4
NetIpv6ValidateRpPrefixInCxt (UINT4 u4ContextId, UINT4 u4IfIndex,
                              tIp6Addr * pMcastAddr)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pMcastAddr);
    return NETIPV6_SUCCESS;
}

/******************************************************************************
 * Function Name    :   NetIpv6GetUnicastRpPrefix
 * Description      :   This function can be used by external modules
 *                      to validate and retrieve the RP address
 *                      embedded in the given multicast ipv6 address.
 * Inputs           :   u4ContextId - Context id
 *                      u4IfIndex   - Interface index
 *                      pMcastAddr  - IPv6 multicast address
 * Outputs          :   pIp6RpAddrInfo  - RP Address related information
 * Return Value     :   NETIPV6_SUCCESS or NETIPV6_FAILURE
 *******************************************************************************/
INT4
NetIpv6GetUnicastRpPrefix (UINT4 u4ContextId, tIp6Addr * pMcastAddr,
                           tIp6RpAddrInfo * pIp6RpAddrInfo)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (pIp6RpAddrInfo);
    UNUSED_PARAM (pMcastAddr);
    return NETIPV6_SUCCESS;
}

/**********************************************************************
 *   Function Name    :   NetIpv6SelectSrcAddrForDstAddr
 *   Description      :   This function can be used by external modules to
 *                        select the source address for the given destination
 *                        address by applying a set of rules
 *   Inputs           :   pIp6DestAddr    - IPv6 destination address pointer
 *                        pIf6            - IPv6 interface pointer
 *   Outputs          :   None
 *   Return Value     :   The address (tIp6Addr *) or NULL
 * ********************************************************************/

tIp6Addr           *
NetIpv6SelectSrcAddrForDstAddr (tIp6Addr * pIp6DestAddr, tLip6If * pIf6)
{
    UNUSED_PARAM (pIf6);
    return pIp6DestAddr;
}

/****************************************************************************
 *   Function Name    :   NetIpv6SelectDestAddr
 *   Description      :   This function can be used by external modules
 *                        to select  the destinaition address by
 *                         applying a set of rules
 *   Inputs           :   pDstInfo    - IPv6 destination info pointer
 *   Outputs          :   None
 *   Return Value     :   The required destination address
 *
 * **************************************************************************/

INT4
NetIpv6SelectDestAddr (tIp6DstInfo ** pDstInfo, UINT1 u1Count)
{
    UNUSED_PARAM (pDstInfo);
    UNUSED_PARAM (u1Count);
    return NETIPV6_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : This API gives the zone Index corresponding to the scope
 *               configured on an interface when the application requests
 *               for it.
 *
 * INPUTS      : scope     - Scope Configured on an interface
 *               u4IfIndex - Interface Index
 *
 * OUTPUT      : *pi4ZoneIndex - Pointer to the zone index.
 *               Valid zoneindex value if success else updated with
 *               invalid value
 *
 * RETURNS     : ZONE_INDEX(If Valid ZoneIndex)
 *              or IP6_ZONE_INVALID(InValid ZoneIndex)
 *
 * NOTES       : This functions is added as a part of RFC4007 Implementation
 ******************************************************************************/
INT4
NetIpv6GetIfScopeZoneIndex (UINT1 u1Scope, UINT4 u4IfIndex)
{
    UNUSED_PARAM (u1Scope);
    UNUSED_PARAM (u4IfIndex);
    return IP6_ZONE_INVALID;
}

/************************************************************************
 * DESCRIPTION : This functions tests if the name and the zone id       *
 *               valid one or not.                                      *
 *                                                                      *
 * INPUTS      :                                                        *
 *               au1ScopeZone - Pointer to the Scope-Zone name,         *
 *                                                                      *
 * OUTPUTS     : *pu1ErrVal - the output error value                    *
 *                                                                      *
 * RETURNS     : NETIPV6_SUCCESS (If validation is successful)          *
 *               NETIPV6_FAILURE (If validation fails)                  *
 *                                                                      *
 * NOTES       : This function is added as a part of RFC4007 code       *
 *               changes                                                *
 ************************************************************************/

INT4
NetIpv6ValidateZoneName (UINT1 *pu1ScopeZone, UINT1 *pu1ErrVal)
{
    UNUSED_PARAM (pu1ErrVal);
    UNUSED_PARAM (pu1ScopeZone);
    return NETIPV6_SUCCESS;
}

/******************************************************************************
 * Function Name    :   NetIpv6NotifySelfIfOnLink
 * Description      :   This API is called by OSPF to intimate the onlink
 *                      detection or modification of interfaces
 * Inputs           :   u1ProtocolId - Protocol from which this API is called
 *                      pMultIfPtr  - Pointer to the message
 * Outputs          :   None
 * Return Value     :   NETIPV6_SUCCESS or NETIPV6_FAILURE
 *******************************************************************************/
VOID
NetIpv6NotifySelfIfOnLink (UINT1 u1ProtocolId, tIp6MultIfToIp6Link * pMultIfPtr)
{
    UNUSED_PARAM (u1ProtocolId);
    UNUSED_PARAM (pMultIfPtr);
    return;
}

/******************************************************************************
 * DESCRIPTION : This API is for checking the existence of a scope-zone on IPv6
 *               interface with the given scope.
 *               The application gives Scope,context-Id and the scope-zone name
 *               as input
 *
 * INPUTS      : u4ContextId - Context from which scope-zone info needs to be
 *                             verified
 *               u1Scope - Comparison to be done for the given scope
 *               pu1ZoneName - Scope zone name
 *
 * OUTPUT      : pi4ZoneIndex - Scope Zone Index
 *
 * RETURNS     : NETIPV6_SUCCESS - If zone exists on this interface
 *               or NETIPV6_FAILURE - If zone doesn't exist on this interface
 *
 * NOTES       : This functions is added as a part of RFC4007 implementation
 *
 ******************************************************************************/

INT4
NetIpv6FindZoneIndex (UINT4 u4ContextId, UINT1 u1Scope,
                      UINT1 *pu1ZoneName, INT4 *pi4ZoneIndex)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u1Scope);
    UNUSED_PARAM (pu1ZoneName);
    UNUSED_PARAM (pi4ZoneIndex);
    return NETIPV6_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : This API is for checking the existence of a scope-zone on IPv6
 *              interface with the given scope.
 *              The application gives Scope and context-Id as input
 *
 * INPUTS      : u4ContextId - Context from which scope-zone infoto be verified
 *
 *               u1Scope - Comparison to be done for the given scope
 *
 *
 *
 * OUTPUT      : pau1ZoneName -Scope Zone name
 *               pi4ZoneId - Scope Zone Index
 *
 * RETURNS     : NETIPV6_SUCCESS - If zone exists on this interface
 *               or NETIPV6_FAILURE - If zone doesn't exist on this interface
 *
 * NOTES       : This functions is added as a part of RFC4007 implementation
 *
 ******************************************************************************/

INT4
NetIpv6CheckIfAnyZoneExistForGivenScope (UINT4 u4ContextId, UINT1 u1Scope,
                                         UINT1 *pau1ZoneName,
                                         INT4 *pi4ZoneIndex)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u1Scope);
    UNUSED_PARAM (pau1ZoneName);
    UNUSED_PARAM (pi4ZoneIndex);
    return NETIPV6_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : This API gets the scope-zone information from the interface
 *               scope-zone structure. It also passes an auction based on the
 *               action comparison it retrieves the other parameters.
 *               The application gives zone index ,ifIndex and u1Action
 *               as input
 *
 * INPUTS      : u4IfIndex - Interface index from which scope-zone info to be
 *                           obtained
 *               u1Action - Comparison to be done for the field in pInScopeInfo.
 *                          The possible fields that are compared are
 *                          IP6_SCOPE_ZONE_INDEX
 *                          IP6_SCOPE_ZONE_NAME
 *                          IP6_SCOPE_ZONE_SCOPE
 *               pInScopeInfo - Structure having the scope zone info related to
 *                              the u1Action
 *
 * OUTPUT      : pOutScopeInfo -Scope Zone info updated based on the u1Action
 *
 * RETURNS     : NETIPV6_SUCCESS - If zone exists on this interface
 *               or NETIPV6_FAILURE - If zone doesn't exist on this interface
 *
 * NOTES       : This functions is added as a part of RFC4007 implementation
 *
 ******************************************************************************/
INT4
NetIpv6GetIfScopeZoneInfo (UINT4 u4IfIndex, UINT1 u1Action,
                           tIp6ZoneInfo * pInScopeInfo,
                           tIp6ZoneInfo * pOutScopeInfo)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1Action);
    UNUSED_PARAM (pInScopeInfo);
    UNUSED_PARAM (pOutScopeInfo);
    return NETIPV6_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : This API is to get the scope info from ScopeZone-name
 *              interface with the given scope. 
 *              The application gives Scope and ScopeZone-name
 *
 * INPUTS      : ScopeZone-name- Scope name from which scope-zone info 
 *                               to be verified 
 *              
 *
 *
 * OUTPUT      : u1Scope - Comparison to be done for the given scope 
 *
 * RETURNS     : NETIPV6_SUCCESS - If zone exists on this interface
 *               or NETIPV6_FAILURE - If zone doesn't exist on this interface
 *  
 * NOTES       : This functions is added as a part of RFC4007 implementation
 *
 ******************************************************************************/

INT4
NetIpv6GetScopeId (UINT1 *pu1ZoneName, UINT1 *pu1Scope)
{
    UNUSED_PARAM (pu1ZoneName);
    UNUSED_PARAM (pu1Scope);
    return NETIPV6_SUCCESS;

}

/****************************************************************************
* FUNCTION NAME    : NetIpv6McastSetMcStatusOnIface 
* DESCRIPTION      : Enables/Disables multicast routing on an interface
*                    according to u4McastStatus. 
* INPUT            : pNetIp6McastInfo: contains the ipport and the 
*                    corresponding multicast routing information 
*                    u4McastStatus : NETIPV6_ENABLED/NETIPV6_DISABLED
* OUTPUT           : None
* RETURNS          : NETIPV6_SUCCESS/NETIPV6_FAILURE
****************************************************************************/
#ifndef PIMV6_WANTED
INT4
NetIpv6McastSetMcStatusOnIface (tNetIp6McastInfo * pNetIp6McastInfo,
                                UINT4 u4McastStatus)
{
    UNUSED_PARAM (pNetIp6McastInfo);
    UNUSED_PARAM (u4McastStatus);

    return NETIPV6_SUCCESS;
}
#endif

/*-------------------------------------------------------------------+
 * Function           :  Ip6HandlePathStatusChange
 *
 * Description        : This functions indicates that there is a
 *                      BFD session state change and the same
 *                      has to be posted as an event for ND6
 *                      cache entry deletion.
 * Input(s)           : u4ContextId - Context id of the interface
 *                      pIpNbrInfo  - Next hop details to clear
 *                                    route and arp cache.
 * Output(s)          : NONE
 * Returns            : NETIPV4_SUCCESS or NETIPV4_FAILURE
------------------------------------------------------------------- */

INT1
Ip6HandlePathStatusChange (UINT4 u4ContextId,
                           tClientNbrIp6PathInfo * pIpNbrInfo)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (pIpNbrInfo);

    return NETIPV6_SUCCESS;
}


/*-------------------------------------------------------------------+
 * Function           :  Ip6ProcessPathStatusNotification
 *
 *Description         : This functions process the session state
 *                      change notification from BFD and moves the
 *                      ND6 Cache entry to stale.
 *
 * Input(s)           : u4ContextId - Context id of the interface
 *                      pIpNbrInfo  - Next hop details to clear
 *                                    route and arp cache.
 * Output(s)          : NONE
 * Returns            : NETIPV4_SUCCESS or NETIPV4_FAILURE
------------------------------------------------------------------- */

VOID Ip6ProcessPathStatusNotification (tCRU_BUF_CHAIN_HEADER * pBuf)
{

    UNUSED_PARAM (pBuf);
    return;
}

#ifdef VRRP_WANTED
/*-------------------------------------------------------------------+
 * Function           : NetIpv6CreateVrrpInterface
 *                     
 * Description        : Enables VRRP capability for the IP interface or
 *                      adds address as virtual address to the IP
 *                      interface.
 *
 * Input(s)           : pVrrpNwIntf    - Pointer to VRRP Interface
 *                                       Information.
 *
 * Output(s)          : None
 *
 * Returns            : NETIPV6_SUCCESS or NETIPV6_FAILURE
------------------------------------------------------------------- */
INT4
NetIpv6CreateVrrpInterface (tVrrpNwIntf * pVrrpNwIntf)
{
    tIp6Addr            Ip6Addr;

    MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));

    if (pVrrpNwIntf->u1Action == VRRP_NW_INTF_CREATE)
    {
        if (Lip6VrrpVifConfigInLnx (pVrrpNwIntf) == NETIPV6_FAILURE)
        {
            return NETIPV6_FAILURE;
        }
#ifdef NPAPI_WANTED
        if (IpFsNpVrrpHwProgram (VRRP_NP_CREATE_INTERFACE,
                                 pVrrpNwIntf) == FNP_FAILURE)
        {
            return NETIPV6_FAILURE;
        }
#endif

    }
    else if (pVrrpNwIntf->u1Action == VRRP_NW_INTF_SECONDARY_CREATE)
    {
        /* This action is not handled now */
    }
    else if (pVrrpNwIntf->u1Action == VRRP_NW_INTF_MCAST_CREATE)
    {
        MEMCPY (Ip6Addr.u1_addr, pVrrpNwIntf->IpvXAddr.au1Addr,
                IPVX_IPV6_ADDR_LEN);

        Lip6AddrCreateSolicitMcast (pVrrpNwIntf->u4IfIndex, &Ip6Addr);
    }
    else if (pVrrpNwIntf->u1Action == VRRP_NW_INTF_ACCEPT_ADD)
    {
        Lip6VrrpDelDropFilter (pVrrpNwIntf->IpvXAddr);
    }

    return NETIPV6_SUCCESS;
}

/*-------------------------------------------------------------------+
 * Function           : NetIpv6DeleteVrrpInterface
 *                     
 * Description        : Disables VRRP capability for the IP interface or
 *                      deletes virtual address from the IP interface.
 *
 * Input(s)           : pVrrpNwIntf    - Pointer to VRRP Interface
 *                                       Information.
 *
 * Output(s)          : None
 *
 * Returns            : NETIPV6_SUCCESS or NETIPV6_FAILURE
------------------------------------------------------------------- */
INT4
NetIpv6DeleteVrrpInterface (tVrrpNwIntf * pVrrpNwIntf)
{
    tIp6Addr            Ip6Addr;

    MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));

    if (pVrrpNwIntf->u1Action == VRRP_NW_INTF_DELETE)
    {
        if (Lip6VrrpVifDeleteInLnx (pVrrpNwIntf) == NETIPV6_FAILURE)
        {
            return NETIPV6_FAILURE;
        }
#ifdef NPAPI_WANTED
        if (IpFsNpVrrpHwProgram (VRRP_NP_DELETE_INTERFACE,
                                 pVrrpNwIntf) == FNP_FAILURE)
        {
            return NETIPV6_FAILURE;
        }
#endif
    }
    else if (pVrrpNwIntf->u1Action == VRRP_NW_INTF_SECONDARY_DELETE)
    {
        /* This action is not handled now */
    }
    else if (pVrrpNwIntf->u1Action == VRRP_NW_INTF_MCAST_DELETE)
    {
        MEMCPY (Ip6Addr.u1_addr, pVrrpNwIntf->IpvXAddr.au1Addr,
                IPVX_IPV6_ADDR_LEN);

        Lip6AddrDeleteSolicitMcast (pVrrpNwIntf->u4IfIndex, &Ip6Addr);
    }
    else if (pVrrpNwIntf->u1Action == VRRP_NW_INTF_ACCEPT_DEL)
    {
        Lip6VrrpAddDropFilter (pVrrpNwIntf->IpvXAddr);
    }

    return NETIPV6_SUCCESS;
}

/*-------------------------------------------------------------------+
 * Function           : NetIpv6GetVrrpInterface
 *            
 * Description        : Retrieves VRRP information corresponding to
 *                      IP interface from NP.
 *
 *                      This NetIp API also handles the below actions
 *                            1. To Change Network action.
 *                            2. To Check Primary IP against Associated
 *                               IP.
 *                      The above actions are only applicable for
 *                      Linux IP.
 *
 * Input(s)           : pVrrpNwInIntf    - Pointer to VRRP Interface
 *                                       Information.
 *
 * Output(s)          : pVrrpNwOutIntf   - Pointer to VRRP Interface
 *                                       Information.
 *
 * Returns            : NETIPV6_SUCCESS or NETIPV6_FAILURE
------------------------------------------------------------------- */
INT4
NetIpv6GetVrrpInterface (tVrrpNwIntf * pVrrpNwInIntf,
                         tVrrpNwIntf * pVrrpNwOutIntf)
{
    if (pVrrpNwInIntf->b1IsChgNwActionReqd == TRUE)
    {
        if (pVrrpNwInIntf->u1Action == VRRP_NW_INTF_SECONDARY_CREATE)
        {
            if (pVrrpNwInIntf->b1IsAcceptConf == TRUE)
            {
                pVrrpNwInIntf->u1Action = VRRP_NW_INTF_ACCEPT_ADD;
            }
        }
        else if (pVrrpNwInIntf->u1Action == VRRP_NW_INTF_SECONDARY_DELETE)
        {
            if (pVrrpNwInIntf->b1IsAcceptConf == TRUE)
            {
                pVrrpNwInIntf->u1Action = VRRP_NW_INTF_ACCEPT_DEL;
            }
        }

        MEMCPY (pVrrpNwOutIntf, pVrrpNwInIntf, sizeof (tVrrpNwIntf));

        return NETIPV6_SUCCESS;
    }
    else if (pVrrpNwInIntf->b1IsCheckIpReqd == TRUE)
    {
        if (((pVrrpNwInIntf->u1Action == VRRP_NW_INTF_SECONDARY_CREATE) ||
             (pVrrpNwInIntf->u1Action == VRRP_NW_INTF_SECONDARY_DELETE)) &&
            (IPVX_ADDR_COMPARE (pVrrpNwInIntf->CheckIp,
                                pVrrpNwInIntf->IpvXAddr) == 0))
        {
            return NETIPV6_FAILURE;
        }

        MEMCPY (pVrrpNwOutIntf, pVrrpNwInIntf, sizeof (tVrrpNwIntf));

        return NETIPV6_SUCCESS;
    }

#ifdef NPAPI_WANTED
    if (IpFsNpVrrpHwProgram (VRRP_NP_GET_INTERFACE,
                             pVrrpNwInIntf) == FNP_FAILURE)
    {
        return NETIPV6_FAILURE;
    }

    MEMCPY (pVrrpNwOutIntf, pVrrpNwInIntf, sizeof (tVrrpNwIntf));
#endif

    return NETIPV6_SUCCESS;
}

#endif
/***********************************************************************************************
 * Function           : Lip6GetNdStateForRouteNH
 * Description        : This function returns the ND state for the next hop of the route entry.
 * Input(s)           : u4ContextId - RTM context ID
 *                      pIp6RtEntry-  Route for whose NH the ND cache needs to be checkec
 * Output(s)          : None
 * Returns            : None
 ***********************************************************************************************/
INT4
Lip6GetNdStateForRouteNH (UINT4 u4ContextId, tIp6RtEntry * pIp6RtEntry)
{

    INT4                i4NdReachState = FALSE;
    tLip6If            *pIf6Entry = NULL;
    tLip6NdEntry       *pNd6cEntry = NULL;
    tIp6Addr            InAddr;

    pIf6Entry = Lip6UtlGetIfEntry (pIp6RtEntry->u4Index);
    if (pIf6Entry == NULL)
    {
        return i4NdReachState;
    }
    MEMCPY (&InAddr, &pIp6RtEntry->nexthop, sizeof (tIp6Addr));

    pNd6cEntry = Lip6NdGetEntry (pIf6Entry, &InAddr);

    if (pNd6cEntry != NULL)
    {

        if ((pNd6cEntry->u1ReachState == ND6_CACHE_ENTRY_REACHABLE)
            || (pNd6cEntry->u1ReachState == ND6_CACHE_ENTRY_STATIC))
        {

            i4NdReachState = (INT4) TRUE;
        }
    }


    UNUSED_PARAM (u4ContextId);
    return i4NdReachState;
}

/*-------------------------------------------------------------------+
 * Function           : NetIpv6ArpResolve
 *
 *Description         : This function returns next hop mac address
 *                      for next hop IP.
 *
 * Input(s)           : u4DstIntfIdx - destination interface index 
 *                      pDstAddr6 - destination IPv6 address
 *
 * Output(s)          : pu1HwAddr - next hop mac address
 * Returns            : OSIX_SUCCESS/OSIX_FAILURE
------------------------------------------------------------------- */

INT4
NetIpv6Nd6Resolve(UINT4 u4DstIntfIdx, 
                  tIp6Addr *pDstAddr6,
                  UINT1 *pu1HwAddr)
{
    tLip6NdEntry        *pNd6CacheEntry = NULL;
    UINT4               u4Index= 0;
    tLip6If             *pIf6 = NULL;


    pIf6 = Lip6UtlGetIfEntry(u4DstIntfIdx);
    if ( NULL == pIf6 )
    {
        return NETIPV6_FAILURE;
    }

    u4Index = pIf6->u4IfIndex;
    if (pIf6->u4UnnumAssocIPv6If != 0)
    {
        /*This is a unnumbered interface use peer mac address as the destination */
       CfaGetIfUnnumPeerMac (u4Index, pu1HwAddr);
        return NETIPV6_SUCCESS;
    }

    pNd6CacheEntry = Lip6NdGetEntry(pIf6, pDstAddr6);
    if ( (!pNd6CacheEntry) ||
         (pNd6CacheEntry->u1ReachState == ND6_CACHE_ENTRY_INCOMPLETE) )
    {
        if ( OSIX_FAILURE == Lip6NlResolveNd (u4Index, pDstAddr6) )
        {
            return NETIPV6_FAILURE;
        }
        else
        {
            return NETIPV6_SUCCESS;
        }
    }

    if ( (pNd6CacheEntry != NULL) &&
         ( (pNd6CacheEntry->u1ReachState == ND6_CACHE_ENTRY_STATIC) ||
           (pNd6CacheEntry->u1ReachState == ND6_CACHE_ENTRY_REACHABLE) ) )
    {
        /* get MAC address from the Neighbor cache entry */
        MEMCPY(pu1HwAddr, pNd6CacheEntry->MacAddr, sizeof(tMacAddr));
        return NETIPV6_SUCCESS;
    }

    return NETIPV6_FAILURE;
}

/******************************************************************************
 * Function Name : NetIpv6GetNbrllAddrFromIfIndex
 * Description   : This function is used to get the link local address
 * of the neighbor for given index
 * Inputs        : u4IfIndex - Interface index
 * Outputs       : pNd6cEntry - Pointer to Cache entry
 * Return Value  : None
 ******************************************************************************/

PUBLIC INT4
NetIpv6GetNbrllAddrFromIfIndex (UINT4 u4IfIndex, tIp6Addr * pIp6LLAddr)
{

	UNUSED_PARAM(u4IfIndex);
	UNUSED_PARAM(pIp6LLAddr);

	return NETIPV6_SUCCESS;
}

/*-------------------------------------------------------------------+
 * Function           : NetIpv6GetIpForwardingInCxt 
 *
 *Description         : This function returns forwarding status for
 *                       the given contextid
 *
 * Input(s)           : u4ContextId - ContextId
 *
 * Returns            : 
------------------------------------------------------------------- */
UINT4 NetIpv6GetIpForwardingInCxt(UINT4 u4ContextId)
{

   if (u4ContextId == VCM_DEFAULT_CONTEXT)
   { 
      return (UINT4)gIp6GblInfo.i4Ip6Status;
   }
   else
   {
    if (gIp6GblInfo.apIp6Cxt[u4ContextId] != NULL )
       return (gIp6GblInfo.apIp6Cxt[u4ContextId])->u4ForwFlag;
    else
       return IP6_FORW_ENABLE;
   }
}
#ifdef EVPN_VXLAN_WANTED
/******************************************************************************
 * Function Name    :   Ip6UpdateCacheForAddrFromEvpn
 * Description      :   This function checks if the ND cache exists for the next hop.
 *
 * Inputs           :   u4IfIndex  - Interface Index
 *                      pDest  - Dest IPv6 ADDR
 * Return Value     :   IP6_SUCCESS or IP6_FAILURE
 *******************************************************************************/

INT4
Ip6UpdateCacheForAddrFromEvpn (UINT4 u4IfIndex, tIp6Addr * pDest,
                               UINT1 *pau1Lladdr)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pDest);
    UNUSED_PARAM (pau1Lladdr);
    return IP6_SUCCESS;
}
#endif
#endif /* _NETIPV6_C_ */
/* END OF FILE */
