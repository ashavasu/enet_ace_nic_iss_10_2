/**************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: lip6addr.c,v 1.24 2015/11/20 10:36:01 siva Exp $
 *
 * Description: IPv6 Address configuration routines
 ***************************************************************************/
#ifndef _LIP6ADDR_C
#define _LIP6ADDR_C
#include "lip6inc.h"
#ifdef NPAPI_WANTED
#include "nputil.h"
#endif
PRIVATE INT4
 
 
   Lip6AddrCreate (tLip6If * pIf6Entry, tIp6Addr * pIp6Addr, INT4 i4PrefixLen);

PRIVATE INT4
 
 
 
 Lip6AddrUpdateStatus (tLip6If * pIf6Entry, tIp6Addr * pIp6Addr,
                       INT4 i4PrefixLen, INT4 i4Status);
PRIVATE INT4
 
 
   Lip6AddrActive (tLip6If * pIf6Entry, tIp6Addr * pIp6Addr, INT4 i4PrefixLen);

PRIVATE INT4
 
 
 
 Lip6AddrNotActive (tLip6If * pIf6Entry, tIp6Addr * pIp6Addr, INT4 i4PrefixLen);

PRIVATE INT4
 
 
   Lip6AddrAddLocalRoute (UINT4 u4IfIndex, tIp6Addr * pIp6Addr, INT4 i4Prefix);

INT4 Lip6FormSrcAddrList PROTO ((tTMO_SLL *, tLip6If * pIf6));

/******************************************************************************
 * FUNCTION    : Lip6AddrUpdate
 * DESCRIPTION : Set/Delete IPv6 address on an interface
 *
 * INPUTS      : u4IfIndex   - IfIndex of the iIP interface
 *               pIp6Addr    - Ip6 Address
 *               i4PrefixLen - Prefix length
 *               i4Status    - Admin status of Address info
 *
 * OUTPUTS     : None
 *
 * RETURNS     : OSIX_SUCCESS/OSIX_FAILURE
 ******************************************************************************/
PUBLIC INT4
Lip6AddrUpdate (UINT4 u4IfIndex, tIp6Addr * pIp6Addr, INT4 i4PrefixLen,
                INT4 i4Status)
{
    tLip6If            *pIf6Entry = NULL;

    LIP6_TRC_ARG1 (LIP6_MOD_TRC, CONTROL_PLANE_TRC,
                   "Lip6AddrUpdate:IF %d \r\n", u4IfIndex);

    pIf6Entry = Lip6UtlGetIfEntry (u4IfIndex);
    if (pIf6Entry == NULL)
    {
        /* Interface does not exist in IP6 */
        return OSIX_FAILURE;
    }

    switch (i4Status)
    {
        case IP6FWD_CREATE_AND_WAIT:
            if (Lip6AddrCreate (pIf6Entry, pIp6Addr, i4PrefixLen) ==
                OSIX_FAILURE)
            {
                return OSIX_FAILURE;
            }
            break;

        case IP6FWD_ACTIVE:
            if (Lip6AddrUpdateStatus (pIf6Entry, pIp6Addr,
                                      i4PrefixLen, IP6FWD_ACTIVE) ==
                OSIX_FAILURE)
            {
                return OSIX_FAILURE;
            }
            break;

        case IP6FWD_NOT_IN_SERVICE:
            if (Lip6AddrUpdateStatus (pIf6Entry, pIp6Addr,
                                      i4PrefixLen, IP6FWD_NOT_IN_SERVICE) ==
                OSIX_FAILURE)
            {
                return OSIX_FAILURE;
            }
            break;

        case IP6FWD_DESTROY:
            if (Lip6AddrDelete (pIf6Entry, pIp6Addr, i4PrefixLen) ==
                OSIX_FAILURE)
            {
                return OSIX_FAILURE;
            }
            break;

        default:
            return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : Returns address entry from the IPv6 Address Table
 *               (unicast)
 *
 * INPUTS      : Pointer to Interface Index (u4IfIndex), Ip6Addr (pIp6Addr)
 *               Prefix Length (pi4Prefixlen)
 *
 * OUTPUTS     : NONE.
 *
 * RETURNS     : OSIX_SUCCESS/OSIX_FAILURE
 *****************************************************************************/
PUBLIC tLip6AddrNode *
Lip6AddrGetEntry (UINT4 u4IfIndex, tIp6Addr * pIp6Addr, INT4 i4PrefixLen)
{
    tLip6If            *pIf6Entry = NULL;
    tLip6AddrNode      *pAddrInfo = NULL;
    tTMO_SLL           *pIp6AddrList = NULL;

    pIf6Entry = Lip6UtlGetIfEntry (u4IfIndex);

    if (pIf6Entry == NULL)
    {
        return NULL;
    }

    /* Get the address list to be used based on the address type */
    if (Ip6AddrType (pIp6Addr) != ADDR6_LLOCAL)
    {
        pIp6AddrList = &(pIf6Entry->Ip6AddrList);
    }
    else
    {
        pIp6AddrList = &(pIf6Entry->Ip6LLAddrList);
    }

    TMO_SLL_Scan (pIp6AddrList, pAddrInfo, tLip6AddrNode *)
    {
        if ((Ip6AddrMatch (&(pAddrInfo->Ip6Addr), pIp6Addr,
                           IP6_ADDR_SIZE_IN_BITS) == TRUE) &&
            (i4PrefixLen == pAddrInfo->i4PrefixLen))

        {
            /* Matching Entry found */
            return pAddrInfo;
        }

    }
    return NULL;
}

/******************************************************************************
 * DESCRIPTION : Returns next address entry from the IPv6 Address Table
 *               (unicast)
 *
 * INPUTS      : u4IfIndex - Interface Index
 *               pIp6Addr   - Input IP6 Address
 *
 * OUTPUTS     : pu4NextIndex - IfIndex of next IPv6 address
 *               pNextIp6Addr - Next IP6 address
 *               pi4NextPrefix - Prefix length of next IP6 address
 *
 * RETURNS     : OSIX_SUCCESS/OSIX_FAILURE
 *****************************************************************************/
PUBLIC INT4
Lip6AddrGetNext (UINT4 u4IfIndex, tIp6Addr * pIp6Addr, UINT4 *pu4NextIndex,
                 tIp6Addr * pNextIp6Addr, INT4 *pi4NextPrefix)
{
    tLip6If            *pIf6Entry = NULL;
    tLip6AddrNode      *pAddrInfo = NULL;
    UINT4               u4Index = 0;

    pIf6Entry = Lip6UtlGetIfEntry (u4IfIndex);

    if (pIf6Entry != NULL)
    {
        if (Ip6AddrType (pIp6Addr) != ADDR6_LLOCAL)
        {
            /* If the previous address is link local, the next one cannot be a Normal
             * Address since Link local addresses are maintained after the Normal
             * Address */
            TMO_SLL_Scan (&(pIf6Entry->Ip6AddrList), pAddrInfo, tLip6AddrNode *)
            {
                /* This utility description is not correct. It returns IP6_SUCCESS
                 * when 2nd address is greater */
                if (Ip6IsAddrGreater (pIp6Addr, &pAddrInfo->Ip6Addr) ==
                    IP6_SUCCESS)
                {
                    MEMCPY (pNextIp6Addr, &pAddrInfo->Ip6Addr,
                            sizeof (tIp6Addr));
                    *pi4NextPrefix = pAddrInfo->i4PrefixLen;
                    *pu4NextIndex = pIf6Entry->u4IfIndex;
                    return OSIX_SUCCESS;
                }
            }
        }
        else
        {
            /* There are no more ip6Addresses left OR
             * The previous address is a link local address */

            TMO_SLL_Scan (&(pIf6Entry->Ip6LLAddrList), pAddrInfo,
                          tLip6AddrNode *)
            {
                /* This utility description is not correct. It returns IP6_SUCCESS
                 * when 2nd address is greater */
                if (Ip6IsAddrGreater (pIp6Addr, &pAddrInfo->Ip6Addr) ==
                    IP6_SUCCESS)
                {
                    MEMCPY (pNextIp6Addr, &pAddrInfo->Ip6Addr,
                            sizeof (tIp6Addr));
                    *pi4NextPrefix = pAddrInfo->i4PrefixLen;
                    *pu4NextIndex = pIf6Entry->u4IfIndex;
                    return OSIX_SUCCESS;
                }
            }
        }
    }

    u4Index = u4IfIndex + 1;

    for (; u4Index < LIP6_MAX_INTERFACES; u4Index++)
    {
        if ((pIf6Entry = Lip6UtlGetIfEntry (u4Index)) == NULL)
        {
            continue;
        }
        pAddrInfo = (tLip6AddrNode *) TMO_SLL_First (&(pIf6Entry->Ip6AddrList));

        if (pAddrInfo == NULL)
        {
            /* There is no entry in the Ip6AddrList. Scan for Linklocal
             * Addresses*/
            pAddrInfo =
                (tLip6AddrNode *) TMO_SLL_First (&(pIf6Entry->Ip6LLAddrList));
            if (pAddrInfo == NULL)
            {
                /* No address present in both lists. Check next interface */
                continue;
            }
        }
        MEMCPY (pNextIp6Addr, &pAddrInfo->Ip6Addr, sizeof (tIp6Addr));
        *pi4NextPrefix = pAddrInfo->i4PrefixLen;
        *pu4NextIndex = u4Index;
        return OSIX_SUCCESS;
    }

    return OSIX_FAILURE;
}

/******************************************************************************
 * FUNCTION    : Lip6AddrDeleteAll
 * DESCRIPTION : Delete all IPv6 address entries for an interface
 *
 * INPUTS      : pIf6Entry - Pointer to interface info
 *               Ip6Addr - IP6 address to add
 *               i4PrefixLen - Prefix Length
 *
 * OUTPUTS     : None
 *
 * RETURNS     : OSIX_SUCCESS/OSIX_FAILURE
 ******************************************************************************/
PUBLIC INT4
Lip6AddrDeleteAll (tLip6If * pIf6Entry)
{
    tLip6AddrNode      *pIp6AddrNode = NULL;
    tLip6NdEntry       *pNdEntry = NULL;
    tLip6NdEntry       *pNextNdEntry = NULL;

    /* Delete all entries in the Address list */
    pIp6AddrNode = (tLip6AddrNode *) TMO_SLL_First (&pIf6Entry->Ip6AddrList);

    while (pIp6AddrNode != NULL)
    {
        TMO_SLL_Delete (&(pIf6Entry->Ip6AddrList), &(pIp6AddrNode->SllNode));
        MemReleaseMemBlock (gIp6GblInfo.AddrLstPoolId, (UINT1 *) pIp6AddrNode);
        pIp6AddrNode =
            (tLip6AddrNode *) TMO_SLL_First (&(pIf6Entry->Ip6AddrList));
    }

    /* Delete all entries in the Address list */
    pIp6AddrNode = (tLip6AddrNode *) TMO_SLL_First (&pIf6Entry->Ip6LLAddrList);

    while (pIp6AddrNode != NULL)
    {
        TMO_SLL_Delete (&(pIf6Entry->Ip6LLAddrList), &(pIp6AddrNode->SllNode));
        MemReleaseMemBlock (gIp6GblInfo.AddrLstPoolId, (UINT1 *) pIp6AddrNode);
        pIp6AddrNode =
            (tLip6AddrNode *) TMO_SLL_First (&(pIf6Entry->Ip6LLAddrList));
    }

    /* Remove the  ND entries from the Table */
    LIP6_ND_CACHE_SCAN (gIp6GblInfo.Nd6CacheTable, pNdEntry, pNextNdEntry,
                        tLip6NdEntry *)
    {
        if (pNdEntry->pIf6Entry->u4IfIndex == pIf6Entry->u4IfIndex)
        {
            if (pIf6Entry->u1OperStatus == NETIPV6_OPER_DOWN)
            {
                Lip6NdDeleteEntry (pNdEntry);
            }
        }
    }

    return OSIX_SUCCESS;
}

/******************************************************************************
 * FUNCTION    : Lip6AddrEnableAll
 * DESCRIPTION : Adds all IPv6 address entries configured on the interface to
 *               Linux kernel and NP. Also enable radvd for the interface if
 *               required.
 *               This function will be called when IPv6 is enabled on the
 *               interface
 *
 * INPUTS      : pIf6Entry - Pointer to interface info
 *
 * OUTPUTS     : None
 *
 * RETURNS     : OSIX_SUCCESS/OSIX_FAILURE
 ******************************************************************************/
PUBLIC INT4
Lip6AddrEnableAll (tLip6If * pIf6Entry)
{
    tLip6AddrNode      *pIp6AddrNode = NULL;
    tIp6Addr            DefLLAddr;
    INT4                i4RetVal = OSIX_SUCCESS;
    UINT1               u1DadStatus = 0;

    MEMSET (&DefLLAddr, 0, sizeof (tIp6Addr));
    /* Get link-local address from linux */
    Lip6LlaGetLinkLocalAddress (pIf6Entry, &DefLLAddr, &u1DadStatus);

    /* Enable the Link Local addresses */
    TMO_SLL_Scan (&(pIf6Entry->Ip6LLAddrList), pIp6AddrNode, tLip6AddrNode *)
    {
        /* Do not program the default link local address that is already present
         * in linux */
        if (Ip6AddrCompare (DefLLAddr, pIp6AddrNode->Ip6Addr) != IP6_ZERO)
        {
            if (Lip6KernUpdateIpv6Addr (pIf6Entry->u4IpPort,
                                        &pIp6AddrNode->Ip6Addr,
                                        pIp6AddrNode->i4PrefixLen,
                                        OSIX_TRUE) == OSIX_FAILURE)
            {
                return OSIX_FAILURE;
            }

#ifdef NPAPI_WANTED
    FsNpIpv6AddrCreate (pIf6Entry->u4ContextId, pIf6Entry->u4IfIndex,
                            NP_IP6_UNI_ADDR,
                            (UINT1 *) &pIp6AddrNode->Ip6Addr,
                            (UINT1) pIp6AddrNode->i4PrefixLen);
#endif

        }
    }

    TMO_SLL_Scan (&(pIf6Entry->Ip6AddrList), pIp6AddrNode, tLip6AddrNode *)
    {
        if (Lip6KernUpdateIpv6Addr (pIf6Entry->u4IpPort,
                                    &pIp6AddrNode->Ip6Addr,
                                    pIp6AddrNode->i4PrefixLen,
                                    OSIX_TRUE) == OSIX_FAILURE)
        {
            return OSIX_FAILURE;
        }

#ifdef NPAPI_WANTED
        FsNpIpv6AddrCreate (pIf6Entry->u4ContextId, pIf6Entry->u4IfIndex,
                                NP_IP6_UNI_ADDR,
                                (UINT1 *) &pIp6AddrNode->Ip6Addr,
                                (UINT1) pIp6AddrNode->i4PrefixLen);
#endif
    if (Lip6AddrAddLocalRoute (pIf6Entry->u4IfIndex,
                    &pIp6AddrNode->Ip6Addr,
                    pIp6AddrNode->i4PrefixLen) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    }

    /* Indicate RTM to activate routes over this interface */
    IP6_TASK_UNLOCK ();
    Rtm6ApiActOnIpv6IfChange (RTM6_SCAN_INTF_UP, pIf6Entry->u4IfIndex);
    IP6_TASK_LOCK ();

    if (pIf6Entry->u1IfType != IP6_TUNNEL_INTERFACE_TYPE)
    {
        /* Update RA after deleting the prefixes */
        if (pIf6Entry->u1RAdvStatus == IP6_IF_ROUT_ADV_ENABLED)
        {
        i4RetVal = Lip6RAConfig (pIf6Entry->u4ContextId);
        }
    }

    return i4RetVal;
}

/******************************************************************************
 * FUNCTION    : Lip6AddrDisableAll
 * DESCRIPTION : Deletes all IPv6 address entries from the Linux kernel 
 *               for an interface and reconfigures radvd if required
 *               This function will be called when IPv6 is disabled
 *               on an interface
 *
 * INPUTS      : pIf6Entry - Pointer to interface info
 *
 * OUTPUTS     : None
 *
 * RETURNS     : OSIX_SUCCESS/OSIX_FAILURE
 ******************************************************************************/
PUBLIC INT4
Lip6AddrDisableAll (tLip6If * pIf6Entry)
{
    tLip6AddrNode      *pIp6AddrNode = NULL;

    TMO_SLL_Scan (&(pIf6Entry->Ip6AddrList), pIp6AddrNode, tLip6AddrNode *)
    {
        if (Lip6AddrDelLocalRoute (pIf6Entry->u4IfIndex,
                                   &pIp6AddrNode->Ip6Addr,
                                   pIp6AddrNode->i4PrefixLen) == OSIX_FAILURE)
        {
            return OSIX_FAILURE;
        }

        Lip6KernUpdateIpv6Addr (pIf6Entry->u4IpPort,
                                &pIp6AddrNode->Ip6Addr,
                                pIp6AddrNode->i4PrefixLen, OSIX_FALSE);

#ifdef NPAPI_WANTED
        Ipv6FsNpIpv6AddrDelete (pIf6Entry->u4ContextId, pIf6Entry->u4IfIndex,
                                NP_IP6_UNI_ADDR,
                                (UINT1 *) &pIp6AddrNode->Ip6Addr,
                                (UINT1) pIp6AddrNode->i4PrefixLen);
#endif
    }

    /* Disable all the link local addresses */
    TMO_SLL_Scan (&(pIf6Entry->Ip6LLAddrList), pIp6AddrNode, tLip6AddrNode *)
    {
        /* We need not delete the local routes since we are not creating them */

        Lip6KernUpdateIpv6Addr (pIf6Entry->u4IpPort,
                                &pIp6AddrNode->Ip6Addr,
                                pIp6AddrNode->i4PrefixLen, OSIX_FALSE);

#ifdef NPAPI_WANTED
        Ipv6FsNpIpv6AddrDelete (pIf6Entry->u4ContextId, pIf6Entry->u4IfIndex,
                                NP_IP6_UNI_ADDR,
                                (UINT1 *) &pIp6AddrNode->Ip6Addr,
                                (UINT1) pIp6AddrNode->i4PrefixLen);
#endif
    }

    /* Indicate RTM to delete all routes over this interface */
    IP6_TASK_UNLOCK ();
    Rtm6ApiActOnIpv6IfChange (RTM6_SCAN_INTF_DOWN, pIf6Entry->u4IfIndex);
    IP6_TASK_LOCK ();

    if (pIf6Entry->u1IfType != IP6_TUNNEL_INTERFACE_TYPE)
    {
        if (pIf6Entry->u1Ipv6IfFwdOperStatus == LIP6_FORW_DISABLE)
        {
            /* Delete all prefices as well */
            Lip6PrefixDeleteAll (pIf6Entry);
        }
    }

    return OSIX_SUCCESS;
}

/******************************************************************************
* DESCRIPTION : Scan the IPv6 Address Table for the give address and prefix len
*
* INPUTS      : Ip6Addr (pAddr6), Prefix Length (i4PrefixLen)
*
* OUTPUTS     : NONE.
*
* RETURNS     : Pionter to the address node, NULL Otherwise.
*
******************************************************************************/
PUBLIC tLip6AddrNode *
Lip6AddrScanAllIf (tIp6Addr * pIp6Addr, INT4 i4PrefixLen,UINT4 u4ContextId)
{
    UINT4               u4Index = 0;
    tLip6If            *pIf6Entry = NULL;
    tLip6AddrNode      *pSearchNode = NULL;
    INT4               i4MinPrefix = 0;
    for (; u4Index < LIP6_MAX_INTERFACES; u4Index++)
    {
        pIf6Entry = Lip6UtlGetIfEntry (u4Index);
        if ((pIf6Entry == NULL) || (pIf6Entry->u4ContextId != u4ContextId))
        {
            continue;
        }

        TMO_SLL_Scan (&(pIf6Entry->Ip6AddrList), pSearchNode, tLip6AddrNode *)
        {
        /* Multiple address with same prefix cannot be assigned */
            i4MinPrefix = ((pSearchNode->i4PrefixLen) < i4PrefixLen) ? (pSearchNode->i4PrefixLen) : i4PrefixLen;
            if (Ip6AddrMatch(&(pSearchNode->Ip6Addr), pIp6Addr, i4MinPrefix))

            {
                return pSearchNode;
            }
        }

        /* Scan the link local address table */
        TMO_SLL_Scan (&(pIf6Entry->Ip6LLAddrList), pSearchNode, tLip6AddrNode *)
        {
            if ((MEMCMP (&(pSearchNode->Ip6Addr), pIp6Addr, sizeof (tIp6Addr))
                 == 0) && (pSearchNode->i4PrefixLen == i4PrefixLen))
            {
                return pSearchNode;
            }
        }

    }

    return NULL;
}

/******************************************************************************
 * FUNCTION    : Lip6AddrCreate
 * DESCRIPTION : Create IPv6 address entry in the address list
 *
 * INPUTS      : pIf6Entry - Pointer to interface info
 *               pIp6Addr - IP6 address to add
 *               i4PrefixLen - Prefix Length
 *
 * OUTPUTS     : None
 *
 * RETURNS     : OSIX_SUCCESS/OSIX_FAILURE
 ******************************************************************************/
PRIVATE INT4
Lip6AddrCreate (tLip6If * pIf6Entry, tIp6Addr * pIp6Addr, INT4 i4PrefixLen)
{
    tLip6AddrNode      *pIp6AddrNode = NULL;
    tLip6AddrNode      *pSearchNode = NULL;
    tTMO_SLL_NODE      *pPrevNode = NULL;
    tTMO_SLL           *pIp6AddrList = NULL;
    UINT1               u1AddrType = 0;

    /* Get the address list to be used based on the address type */
    if (Ip6AddrType (pIp6Addr) != ADDR6_LLOCAL)
    {
        pIp6AddrList = &(pIf6Entry->Ip6AddrList);
        u1AddrType = ADDR6_UNICAST;
    }
    else
    {
        pIp6AddrList = &(pIf6Entry->Ip6LLAddrList);
        u1AddrType = ADDR6_LLOCAL;
    }

    /* Add address to list */
    if ((pIp6AddrNode =
         (tLip6AddrNode *) MemAllocMemBlk (gIp6GblInfo.AddrLstPoolId)) == NULL)
    {
        LIP6_TRC_ARG (LIP6_MOD_TRC, ALL_FAILURE_TRC,
                      "Mem alloc for tLip6AddrNode Failed!\n");
        return OSIX_FAILURE;
    }

    TMO_SLL_Init_Node (&(pIp6AddrNode->SllNode));
    MEMCPY (&(pIp6AddrNode->Ip6Addr), pIp6Addr, sizeof (tIp6Addr));
    pIp6AddrNode->i4PrefixLen = i4PrefixLen;
    pIp6AddrNode->u1AdminStatus = IP6FWD_CREATE_AND_WAIT;
    pIp6AddrNode->u1AddrType = u1AddrType;    /* Default address type */

    /* Scan and insert in ascending order */
    TMO_SLL_Scan (pIp6AddrList, pSearchNode, tLip6AddrNode *)
    {
        /*while creating new addressnode  check wheather our address having a prefix is already associated to a profileindex */
        if ((Ip6AddrMatch (&(pSearchNode->Ip6Addr), pIp6Addr,
                           i4PrefixLen) == TRUE)
            && (i4PrefixLen == pSearchNode->i4PrefixLen))
        {
            pIp6AddrNode->u2Addr6Profile = pSearchNode->u2Addr6Profile;
        }

        /* This utility description is not correct. It returns IP6_SUCCESS
         * when 2nd address is greater */
        if (Ip6IsAddrGreater (pIp6Addr, &(pSearchNode->Ip6Addr)) == IP6_SUCCESS)
        {
            /* Insert before pSearchNode */
            break;
        }
        pPrevNode = &(pSearchNode->SllNode);
    }

    TMO_SLL_Insert (pIp6AddrList, pPrevNode, &(pIp6AddrNode->SllNode));
    return OSIX_SUCCESS;
}

/******************************************************************************
 * FUNCTION    : Lip6AddrUpdateStatus
 * DESCRIPTION : Update Status of the IPv6 address entry
 *
 * INPUTS      : pIf6Entry - Pointer to interface info
 *               pIp6Addr - IP6 address to add
 *               i4PrefixLen - Prefix Length
 *               i4Status - IP6FWD_ACTIVE/IP6FWD_NOT_IN_SERVICE
 *
 * OUTPUTS     : None
 *
 * RETURNS     : OSIX_SUCCESS/OSIX_FAILURE
 ******************************************************************************/
PRIVATE INT4
Lip6AddrUpdateStatus (tLip6If * pIf6Entry, tIp6Addr * pIp6Addr,
                      INT4 i4PrefixLen, INT4 i4Status)
{
    tTMO_SLL           *pIp6AddrList = NULL;
    tLip6AddrNode      *pSearchNode = NULL;
    INT4                i4EntryFound = OSIX_FALSE;
    INT4                i4RetVal = OSIX_SUCCESS;

    /* Get the address list to be used based on the address type */
    if (Ip6AddrType (pIp6Addr) != ADDR6_LLOCAL)
    {
        pIp6AddrList = &(pIf6Entry->Ip6AddrList);
    }
    else
    {
        pIp6AddrList = &(pIf6Entry->Ip6LLAddrList);
    }

    /* Scan and get address node */
    TMO_SLL_Scan (pIp6AddrList, pSearchNode, tLip6AddrNode *)
    {
        if ((MEMCMP (&(pSearchNode->Ip6Addr), pIp6Addr, sizeof (tIp6Addr)) == 0)
            && (pSearchNode->i4PrefixLen == i4PrefixLen))
        {
            i4EntryFound = OSIX_TRUE;
            break;
        }
    }

    if (i4EntryFound == OSIX_FALSE)
    {
        /* Incorrect address for updation */
        return OSIX_FAILURE;
    }

    if (pSearchNode->u1AdminStatus == i4Status)
    {
        /* No Change required */
        return OSIX_SUCCESS;
    }
    pSearchNode->u1AdminStatus = i4Status;

    if (i4Status == IP6FWD_ACTIVE)
    {
        i4RetVal = Lip6AddrActive (pIf6Entry, pIp6Addr, i4PrefixLen);
    }
    else
    {
        i4RetVal = Lip6AddrNotActive (pIf6Entry, pIp6Addr, i4PrefixLen);
    }

    return i4RetVal;
}

/******************************************************************************
 * FUNCTION    : Lip6AddrActive
 * DESCRIPTION : Create prefix entry, add to kernel and NP
 *
 * INPUTS      : pIf6Entry - Pointer to interface info
 *               pIp6Addr - IP6 address to add
 *               i4PrefixLen - Prefix Length
 *
 * OUTPUTS     : None
 *
 * RETURNS     : OSIX_SUCCESS/OSIX_FAILURE
 ******************************************************************************/
PRIVATE INT4
Lip6AddrActive (tLip6If * pIf6Entry, tIp6Addr * pIp6Addr, INT4 i4PrefixLen)
{

    if (pIf6Entry->u1OperStatus == NETIPV6_OPER_DOWN)
    {
        return OSIX_SUCCESS;
    }

    if (Lip6KernUpdateIpv6Addr (pIf6Entry->u4IpPort, pIp6Addr,
                                i4PrefixLen, OSIX_TRUE) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

#ifdef NPAPI_WANTED
    Ipv6FsNpIpv6AddrCreate (pIf6Entry->u4ContextId, pIf6Entry->u4IfIndex,
                            NP_IP6_UNI_ADDR,
                            (UINT1 *) pIp6Addr, (UINT1) i4PrefixLen);
#endif

    if (Ip6AddrType (pIp6Addr) != ADDR6_LLOCAL)
    {
        /* We need not add routes for link local addresses */
        if (Lip6AddrAddLocalRoute (pIf6Entry->u4IfIndex, pIp6Addr, i4PrefixLen)
            == OSIX_FAILURE)
        {
            return OSIX_FAILURE;
        }
    }

    return OSIX_SUCCESS;
}

/******************************************************************************
 * FUNCTION    : Lip6AddrNotActive
 * DESCRIPTION : Delete prefix entry if present, delete from kernel and NP
 *
 * INPUTS      : pIf6Entry - Pointer to interface info
 *               pIp6Addr - IP6 address to add
 *               i4PrefixLen - Prefix Length
 *
 * OUTPUTS     : None
 *
 * RETURNS     : OSIX_SUCCESS/OSIX_FAILURE
 ******************************************************************************/
PRIVATE INT4
Lip6AddrNotActive (tLip6If * pIf6Entry, tIp6Addr * pIp6Addr, INT4 i4PrefixLen)
{
    tIp6Addr            IpPrefix;

    if (pIf6Entry->u1OperStatus == NETIPV6_OPER_DOWN)
    {
        return OSIX_SUCCESS;
    }

    if (pIf6Entry->u1IfType != IP6_TUNNEL_INTERFACE_TYPE)
    {
        MEMSET (&IpPrefix, 0, sizeof (tIp6Addr));
        Ip6CopyAddrBits (&IpPrefix, pIp6Addr, i4PrefixLen);
        /* Delete entry from prefix table added in create */
        if (Lip6PrefixGetEntry (pIf6Entry->u4IfIndex, &IpPrefix, i4PrefixLen)
            != NULL)
        {
            if (Lip6PrefixDelete (pIf6Entry->u4IfIndex, &IpPrefix, i4PrefixLen)
                == OSIX_FAILURE)
            {
                return OSIX_FAILURE;
            }
        }
    }

    if (Lip6KernUpdateIpv6Addr (pIf6Entry->u4IpPort, pIp6Addr,
                                i4PrefixLen, OSIX_FALSE) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

#ifdef NPAPI_WANTED
    Ipv6FsNpIpv6AddrDelete (pIf6Entry->u4ContextId, pIf6Entry->u4IfIndex,
                            NP_IP6_UNI_ADDR,
                            (UINT1 *) pIp6Addr, (UINT1) i4PrefixLen);
#endif

    if (Ip6AddrType (pIp6Addr) != ADDR6_LLOCAL)
    {
        /* We need not delete routes for link local addresses.
         * We are not adding them.*/
        if (Lip6AddrDelLocalRoute (pIf6Entry->u4IfIndex, pIp6Addr, i4PrefixLen)
            == OSIX_FAILURE)
        {
            return OSIX_FAILURE;
        }
    }

    return OSIX_SUCCESS;
}

/******************************************************************************
 * FUNCTION    : Lip6AddrDelete
 * DESCRIPTION : Delete IPv6 address entry from the address list
 *
 * INPUTS      : pIf6Entry - Pointer to interface info
 *               pIp6Addr - IP6 address to add
 *               i4PrefixLen - Prefix Length
 *
 * OUTPUTS     : None
 *
 * RETURNS     : OSIX_SUCCESS/OSIX_FAILURE
 ******************************************************************************/
PUBLIC INT4
Lip6AddrDelete (tLip6If * pIf6Entry, tIp6Addr * pIp6Addr, INT4 i4PrefixLen)
{
    tTMO_SLL_NODE      *pTempNode = NULL;
    tTMO_SLL           *pIp6AddrList = NULL;
    tLip6AddrNode      *pSearchNode = NULL;
    tIp6Addr            Prefix;
    INT4                i4EntryFound = OSIX_FALSE;

    /* Get the address list to be used based on the address type */
    if (Ip6AddrType (pIp6Addr) != ADDR6_LLOCAL)
    {
        pIp6AddrList = &(pIf6Entry->Ip6AddrList);
    }
    else
    {
        pIp6AddrList = &(pIf6Entry->Ip6LLAddrList);
    }

    /* Scan and delete address */
    TMO_SLL_Scan (pIp6AddrList, pSearchNode, tLip6AddrNode *)
    {
        if ((MEMCMP (&(pSearchNode->Ip6Addr), pIp6Addr, sizeof (tIp6Addr)) == 0)
            && (pSearchNode->i4PrefixLen == i4PrefixLen))
        {
            pTempNode = &(pSearchNode->SllNode);
            i4EntryFound = OSIX_TRUE;
            break;
        }
    }

    if (i4EntryFound == OSIX_FALSE)
    {
        /* Incorrect address for deletion */
        return OSIX_FAILURE;
    }

    /* If previous status is active, delete address from kernel */
    if ((pSearchNode->u1AdminStatus == IP6FWD_ACTIVE) &&
        (pIf6Entry->u1OperStatus == NETIPV6_OPER_UP))
    {
        if (Ip6AddrType (pIp6Addr) != ADDR6_LLOCAL)
        {
            /* We need not delete routes for link local addresses.
             * We are not adding them.

            * Incase of DAD failure, local route is deleted while updating the
            * address status, so skipping the local route deletion for Duplicate
            * address alone*/
#ifdef LINUX_310_WANTED
            if ((pSearchNode->u1Status & IFA_F_DADFAILED) != IFA_F_DADFAILED)
            {

             if (Lip6AddrDelLocalRoute
                (pIf6Entry->u4IfIndex, pIp6Addr, i4PrefixLen) == OSIX_FAILURE)
             { 
                return OSIX_FAILURE;
             } 
            }
#endif
        }

        if (Lip6KernUpdateIpv6Addr (pIf6Entry->u4IpPort, pIp6Addr,
                                    i4PrefixLen, OSIX_FALSE) == OSIX_FAILURE)
        {
            return OSIX_FAILURE;
        }
#ifdef NPAPI_WANTED
        Ipv6FsNpIpv6AddrDelete (pIf6Entry->u4ContextId, pIf6Entry->u4IfIndex,
                                NP_IP6_UNI_ADDR,
                                (UINT1 *) pIp6Addr, (UINT1) i4PrefixLen);
#endif
    }

    if (Ip6AddrType (pIp6Addr) != ADDR6_LLOCAL)
    {
        /* Delete entry from prefix table added in create */
        MEMSET (&Prefix, 0, sizeof (tIp6Addr));
        Ip6CopyAddrBits (&Prefix, pIp6Addr, i4PrefixLen);

        if (Lip6PrefixGetEntry (pIf6Entry->u4IfIndex, &Prefix, i4PrefixLen) !=
            NULL)
        {
            if (Lip6PrefixDelete (pIf6Entry->u4IfIndex, &Prefix, i4PrefixLen) ==
                OSIX_FAILURE)
            {
                return OSIX_FAILURE;
            }
        }
    }

    TMO_SLL_Delete (pIp6AddrList, pTempNode);
    MemReleaseMemBlock (gIp6GblInfo.AddrLstPoolId, (UINT1 *) pSearchNode);
    if (pIp6AddrList->u4_Count > 0)
    {
        TMO_SLL_Scan (pIp6AddrList, pSearchNode, tLip6AddrNode *)
        {
            if (Lip6AddrAddLocalRoute
                (pIf6Entry->u4IfIndex, &(pSearchNode->Ip6Addr),
                 pSearchNode->i4PrefixLen) == OSIX_FAILURE)
            {
                return OSIX_FAILURE;
            }
        }
    }
    return OSIX_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : Called whenever a address is configured on an interface
 *               to add a LOCAL route 
 *
 * INPUTS      : The logical interface (u4Index),the ptr to IPv6 address 
 *               (pIp6Addr), prefix length (i4Prefix)
 *
 * OUTPUTS     : None
 *
 * RETURNS     : OSIX_SUCCESS/OSIX_FAILURE
 ******************************************************************************/
PRIVATE INT4
Lip6AddrAddLocalRoute (UINT4 u4IfIndex, tIp6Addr * pIp6Addr, INT4 i4Prefix)
{
    tNetIpv6RtInfo      NetIpv6RtInfo;
    tIp6Addr            NextHop;

    LIP6_TRC_ARG3 (LIP6_MOD_TRC, CONTROL_PLANE_TRC,
                   "IP6: Adding Local Route,If = %d Prefixlen = %d Address = %s\n",
                   u4IfIndex, i4Prefix, Ip6PrintAddr (pIp6Addr));
    MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));
    MEMSET (&NextHop, 0, sizeof (tIp6Addr));
    Ip6CopyAddrBits (&NetIpv6RtInfo.Ip6Dst, pIp6Addr, i4Prefix);
    NetIpv6RtInfo.u1Prefixlen = (UINT4) i4Prefix;
    NetIpv6RtInfo.u4Index = u4IfIndex;
    NetIpv6RtInfo.u4Metric = 1;    /* For local route metric is always 1 */
    NetIpv6RtInfo.i1Proto = IP6_LOCAL_PROTOID;
    NetIpv6RtInfo.i1Type = IP6_ROUTE_TYPE_DIRECT;
    NetIpv6RtInfo.u4RowStatus = IP6FWD_ACTIVE;
    NetIpv6RtInfo.u2ChgBit = IP6_BIT_ALL;
    Ip6GetCxtId (u4IfIndex, &NetIpv6RtInfo.u4ContextId);
    if (Rtm6ApiIpv6LeakRoute (NETIPV6_ADD_ROUTE, &NetIpv6RtInfo) ==
        RTM6_FAILURE)
    {
        /* Adding Local Route Fails. */
        return OSIX_FAILURE;
    }

    Rtm6UpdateInvdRouteList (u4IfIndex, pIp6Addr, i4Prefix, NETIPV6_ADD_ROUTE);
    return OSIX_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : Called when the interface address is deleted to delete all
 *               local routes from RTM
 *
 * INPUTS      : Ptr to IPv6 address (pIp6Addr)
 *
 * OUTPUTS     : None
 *
 * RETURNS     : OSIX_SUCCESS/OSIX_FAILURE
 ******************************************************************************/
INT4
Lip6AddrDelLocalRoute (UINT4 u4IfIndex, tIp6Addr * pIp6Addr, INT4 i4Prefix)
{
    tNetIpv6RtInfo      NetIpv6RtInfo;

    MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));
    Ip6CopyAddrBits (&NetIpv6RtInfo.Ip6Dst, pIp6Addr, i4Prefix);
    NetIpv6RtInfo.u1Prefixlen = (UINT1) i4Prefix;
    NetIpv6RtInfo.u4Index = u4IfIndex;
    NetIpv6RtInfo.i1Proto = IP6_LOCAL_PROTOID;
    NetIpv6RtInfo.u4RowStatus = IP6FWD_DESTROY;
    NetIpv6RtInfo.u2ChgBit = IP6_BIT_STATUS;
    Ip6GetCxtId (u4IfIndex, &NetIpv6RtInfo.u4ContextId);

    if (Rtm6ApiIpv6LeakRoute (NETIPV6_DELETE_ROUTE, &NetIpv6RtInfo) ==
        RTM6_FAILURE)
    {
        /* Deleting Static Route Fails. */
        LIP6_TRC_ARG (LIP6_MOD_TRC, ALL_FAILURE_TRC,
                      "IP6: Deleting Local route FAILED!!!\n");
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/***********************************************************************
 *  Function Name    :   Lip6FormSrcAddrList
 *
 *  Description      :   This function forms the source address list
 *    
 *  Inputs           :   pSrcAddrlist - Source address list pointer
 *      
 *  Outputs          :   pSrcAddrList - Source address list after 
 *                       gathering the source addresses
 *        
 *  Return Value     :    NETIPV6_SUCCESS
 *
 ************************************************************************/

INT4
Lip6FormSrcAddrList (tTMO_SLL * pSrcAddrlist, tLip6If * pIf6)
{
    tLip6If            *pIf6OnLink = NULL;
    tLip6If            *pIf6Out = NULL;
    tLip6If            *pIf6ActiveIf = NULL;
    tLip6AddrNode      *pNode = NULL;
    tIp6SrcAddr        *pSrcAddrInfo = NULL;
    UINT4               u4Index = 0;
    UINT4               u4OnLinkIfIndex = 0;
    UINT1               u1Counter = 0;
    UINT4               u4Array[IPV6_MAX_IF_OVER_LINK];
    /*Initializing the source address list */
    TMO_SLL_Init (pSrcAddrlist);

    /*Scan the Ip6LLAddrList to form the candidate set
       of source addresses and copy the
       addresses into a new list source address list */
    u4Array[0] = pIf6->u4IfIndex;

    if (pIf6->u2OnlinkIfCount > 0)
    {
        pIf6ActiveIf = IP6_INTERFACE (pIf6->u4OnLinkActiveIfId);

        if (pIf6ActiveIf != NULL)
        {
            for (u4Index = 0; ((u4Index < pIf6->u2OnlinkIfCount) &&
                               (u1Counter < IPV6_MAX_IF_OVER_LINK)); u4Index++)
            {
                u4OnLinkIfIndex = pIf6ActiveIf->au1OnLinkIfaces[u4Index];
                pIf6OnLink = IP6_INTERFACE (u4OnLinkIfIndex);
                if (pIf6OnLink != NULL)
                {
                    u4Array[u1Counter] = pIf6OnLink->u4IfIndex;
                    u1Counter++;
                }
            }
        }
    }
    u4Index = 0;
    do
    {
        pIf6Out = IP6_INTERFACE (u4Array[u4Index]);
        TMO_SLL_Scan (&pIf6Out->Ip6LLAddrList, pNode, tLip6AddrNode *)
        {
            if ((pNode->u1Status & IFA_F_TENTATIVE) == IFA_F_TENTATIVE)
            {
                continue;
            }
            pSrcAddrInfo =
                (tIp6SrcAddr *) MemAllocMemBlk (gIp6GblInfo.AddrLstPoolId);
            if (pSrcAddrInfo != NULL)
            {
                MEMCPY (&(pSrcAddrInfo->srcaddr),
                        &(pNode->Ip6Addr), sizeof (tIp6Addr));

                pSrcAddrInfo->u4IfIndex = pIf6Out->u4IfIndex;
                pSrcAddrInfo->u1CumulativeScore = 0;
                TMO_SLL_Add (pSrcAddrlist, &(pSrcAddrInfo->SrcNode));
            }
        }
        TMO_SLL_Scan (&pIf6Out->Ip6AddrList, pNode, tLip6AddrNode *)
        {

            if ((pNode->u1AddrType == ADDR6_ANYCAST)
                || (pNode->u1AddrType == ADDR6_MULTI)
                || (pNode->u1AddrType == ADDR6_LOOPBACK)
                || (pNode->u1AddrType == ADDR6_UNSPECIFIED)
                || ((pNode->u1Status & IFA_F_TENTATIVE) == IFA_F_TENTATIVE))
            {
                continue;
            }
            pSrcAddrInfo =
                (tIp6SrcAddr *) MemAllocMemBlk (gIp6GblInfo.AddrLstPoolId);

            if (pSrcAddrInfo != NULL)
            {
                MEMCPY (&(pSrcAddrInfo->srcaddr),
                        &(pNode->Ip6Addr), sizeof (tIp6Addr));
                pSrcAddrInfo->u4IfIndex = pIf6Out->u4IfIndex;
                pSrcAddrInfo->u1CumulativeScore = 0;
                TMO_SLL_Add (pSrcAddrlist, &(pSrcAddrInfo->SrcNode));
            }

        }
        u4Index++;
    }
    while (u4Index < pIf6->u2OnlinkIfCount);

    return IP6_SUCCESS;

}

/******************************************************************************
 * DESCRIPTION : This function, when called returns the address state
 *
 * INPUTS      : pIf6 - Neighbour Advertisement related informations
 *
 * OUTPUTS     : Returns 0x0a or 0x02 
 *
 * RETURNS     : returns Address State
 *
 * NOTES       :
 ******************************************************************************/
UINT1
Ip6RetAddrStatComplete (tLip6If * pIf6)
{
    if (pIf6->u1Ipv6IfFwdOperStatus == IP6_IF_FORW_ENABLE)
    {
        return (0x0a);
    }
    else
    {
        return (0x02);
    }
}

/******************************************************************************
 * DESCRIPTION : This function is to create a new Multicast Address
 *                 
 * INPUTS      : u4Index   - Interface Index
 *               pAddr     - Address to be created
 *
 * OUTPUTS     : None
 * RETURNS     : returns IP6_SUCCESS or IP6_FAILURE
 * NOTES       :
 ******************************************************************************/
INT4
Lip6AddrCreateMcast (UINT4 u4Index, tIp6Addr * pAddr)
{
    tIp6McastInfo      *pAddrInfo = NULL;
    tIp6McastInfo      *pPrevAddrInfo = NULL;
    tLip6If            *pIf6 = NULL;

    /* Verify whether the same MCAST Address is already present or not.
     * If present, then increment the RefCount. Else create a new
     * address and add to the list. */
    pIf6 = Lip6UtlGetIfEntry (u4Index);
    if (pIf6 == NULL)
    {
        return IP6_FAILURE;
    }
    TMO_SLL_Scan (&pIf6->mcastIlist, pAddrInfo, tIp6McastInfo *)
    {
        if (Ip6AddrMatch (&(pAddrInfo->ip6Addr), pAddr,
                          IP6_ADDR_SIZE_IN_BITS) == TRUE)
        {
            /* Matching Address Present. Increment the Ref Count */
            pAddrInfo->u2RefCnt++;
            return IP6_SUCCESS;
        }

        if (Ip6IsAddrGreater (pAddr, &pAddrInfo->ip6Addr) == SUCCESS)
        {
            /* Address are stored in the Ascending order. New Address
             * is smaller than existing address. So add the new
             * address before this address */
            break;
        }
        pPrevAddrInfo = pAddrInfo;

    }
    /* Allocate memory and add the new MCAST Address */
    pAddrInfo = (tIp6McastInfo *) MemAllocMemBlk (gIp6GblInfo.Ip6mcastId);

    if (pAddrInfo == NULL)
    {
        return IP6_FAILURE;
    }
    MEMSET (pAddrInfo, 0, sizeof (tIp6McastInfo));
    MEMCPY (&(pAddrInfo->ip6Addr), pAddr, sizeof (tIp6Addr));

    /* Add the address to the list of addresses on the interface */
    TMO_SLL_Insert (&pIf6->mcastIlist, &(pPrevAddrInfo->nextAif),
                    &(pAddrInfo->nextAif));

    /* Set the Ref Count */
    pAddrInfo->u2RefCnt = 1;
    pAddrInfo->u1AddrScope = Ip6GetAddrScope (pAddr);

    NetIpv6InvokeAddressChange (pAddr, IP6_ADDR_MAX_PREFIX, ADDR6_MULTI,
                                pIf6->u4IfIndex, NETIPV6_ADDRESS_ADD);
    return IP6_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : This function is to delete the existing Multicast Address
 *                 
 * INPUTS      : u4Index   - Interface Index
 *               pAddr     - Address to be Deleted
 *
 * OUTPUTS     : None
 * RETURNS     : returns IP6_SUCCESS or IP6_FAILURE
 * NOTES       :
 ******************************************************************************/

INT4
Lip6AddrDeleteMcast (UINT4 u4IfIndex, tIp6Addr * ip6Addr)
{

    tIp6McastInfo      *pAddr6Info = NULL;
    tLip6If            *pIf6 = NULL;

    /* Scan through the Mcast List for the given interface and delete
     *      * the matching entry. */
    pIf6 = Lip6UtlGetIfEntry (u4IfIndex);
    if (pIf6 == NULL)
    {
        return IP6_FAILURE;
    }

    TMO_SLL_Scan (&pIf6->mcastIlist, pAddr6Info, tIp6McastInfo *)
    {
        if (Ip6AddrMatch (&(pAddr6Info->ip6Addr), ip6Addr,
                          IP6_ADDR_SIZE_IN_BITS) == TRUE)

        {
            /* Node is listening for the Multi-Cast address over
             * this interface. Decrement the Reference Count. And if RefCnt
             * is 0, release the MCAST Address */
            pAddr6Info->u2RefCnt--;
            if (pAddr6Info->u2RefCnt > 0)
            {
                return IP6_SUCCESS;
            }
            break;
        }
    }

    if (pAddr6Info == NULL)
    {
        return IP6_FAILURE;
    }

    NetIpv6InvokeAddressChange (ip6Addr, IP6_ADDR_MAX_PREFIX, ADDR6_MULTI,
                                u4IfIndex, NETIPV6_ADDRESS_DELETE);

    TMO_SLL_Delete (&pIf6->mcastIlist, &(pAddr6Info->nextAif));

    if (MemReleaseMemBlock (gIp6GblInfo.Ip6mcastId, (UINT1 *) pAddr6Info) ==
        MEM_FAILURE)
    {
        LIP6_TRC_ARG (LIP6_MOD_TRC, CONTROL_PLANE_TRC,
                      "Lip6AddrDeleteMcast:MemReleaseMemBlock failed for pAddr6Info\r\n");
        return OSIX_FAILURE;
    }
    return IP6_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : Called when an solicated multicast address is to be created
 *
 * INPUTS      : Interface index, pointer to unicast address.
 *
 * OUTPUTS     : None
 *
 * RETURNS     : IP6_SUCCESS/IP6_FAILURE
 *
 * NOTES       : For every unicast address, there should exist a corresponding
 *               solicated multicast address. So derive the solicated multicast
 *               address for the given unicast address and add it to the mcast
 *               list supported for this interface.
 ******************************************************************************/
INT4
Lip6AddrCreateSolicitMcast (UINT4 u4Index, tIp6Addr * pAddr)
{
    INT4                i4RetVal = 0;
    tIp6Addr            tmpAddr;

    MEMSET (&tmpAddr, 0, sizeof (tIp6Addr));
    tmpAddr.u1_addr[0] = IP6_UINT1_ALL_ONE;
    tmpAddr.u1_addr[1] = 0x02;
    tmpAddr.u1_addr[11] = 0x01;
    tmpAddr.u1_addr[12] = IP6_UINT1_ALL_ONE;
    tmpAddr.u1_addr[13] = pAddr->u1_addr[13];
    tmpAddr.u1_addr[14] = pAddr->u1_addr[14];
    tmpAddr.u1_addr[15] = pAddr->u1_addr[15];

    i4RetVal = Lip6AddrCreateNewMcastAddr (u4Index, &tmpAddr);
    return i4RetVal;
}

/******************************************************************************
 * DESCRIPTION : Called when an solicated multicast address is to be deleted
 *
 * INPUTS      : Interface index, pointer to unicast address.
 *
 * OUTPUTS     : None
 *
 * RETURNS     : IP6_SUCCESS/IP6_FAILURE
 *
 * NOTES       : For every unicast address, there should exist a corresponding
 *               solicated multicast address. So derive the solicated multicast
 *               address for the given unicast address and delete it from the
 *               mcast list supported for this interface.
 ******************************************************************************/
INT4
Lip6AddrDeleteSolicitMcast (UINT4 u4Index, tIp6Addr * pAddr)
{
    INT4                i4RetVal = 0;
    tIp6Addr            tmpAddr;

    MEMSET (&tmpAddr, 0, sizeof (tIp6Addr));
    tmpAddr.u1_addr[0] = IP6_UINT1_ALL_ONE;
    tmpAddr.u1_addr[1] = 0x02;
    tmpAddr.u1_addr[11] = 0x01;
    tmpAddr.u1_addr[12] = IP6_UINT1_ALL_ONE;
    tmpAddr.u1_addr[13] = pAddr->u1_addr[13];
    tmpAddr.u1_addr[14] = pAddr->u1_addr[14];
    tmpAddr.u1_addr[15] = pAddr->u1_addr[15];

    i4RetVal = Lip6AddrDeleteMcast (u4Index, &tmpAddr);
    return i4RetVal;
}


/******************************************************************************
 * DESCRIPTION : Called when an multicast address list is to be deleted
 *
 * INPUTS      : Pointer to the interface structure 
 *
 * OUTPUTS     : None
 *
 * RETURNS     : IP6_SUCCESS/IP6_FAILURE
 *
 ******************************************************************************/
INT4
Lip6AddrDeleteMcastList (tLip6If * pIf6Entry)
{
    tIp6McastInfo *pAddrInfo = NULL;
    tIp6McastInfo *pTmpAddrInfo = NULL;
    TMO_DYN_SLL_Scan(&(pIf6Entry->mcastIlist), pAddrInfo, pTmpAddrInfo,  tIp6McastInfo * )
    {
    	NetIpv6InvokeAddressChange (&pAddrInfo->ip6Addr, IP6_ADDR_MAX_PREFIX, ADDR6_MULTI,
    			pIf6Entry->u4IfIndex, NETIPV6_ADDRESS_DELETE);
    
    	TMO_SLL_Delete (&pIf6Entry->mcastIlist, &(pAddrInfo->nextAif));
    	if (MemReleaseMemBlock (gIp6GblInfo.Ip6mcastId, (UINT1 *) pAddrInfo) ==
    			MEM_FAILURE)
    	{
    		LIP6_TRC_ARG (LIP6_MOD_TRC, CONTROL_PLANE_TRC,
    				"Lip6AddrDeleteMcastList:MemReleaseMemBlock failed for pAddrInfo\r\n");
    		return OSIX_FAILURE;
    	}
    }
    return OSIX_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : Called when an multicast address list is to be created
 *
 * INPUTS      : Interface index, pointer to unicast address.
 *
 * OUTPUTS     : None
 *
 * RETURNS     : IP6_SUCCESS/IP6_FAILURE
 *
 ******************************************************************************/

INT4
Lip6AddrCreateNewMcastAddr (UINT4 u4Index, tIp6Addr * pAddr)
{
    tIp6McastInfo      *pAddrInfo = NULL;
    tIp6McastInfo      *pPrevAddrInfo = NULL;
    tLip6If            *pIf6 = NULL;

    /* Verify whether the same MCAST Address is already present or not.
     * If present, then increment the RefCount. Else create a new
     * address and add to the list. */
    pIf6 = Lip6UtlGetIfEntry (u4Index);
    if (pIf6 == NULL)
    {
        return IP6_FAILURE;
    }
    TMO_SLL_Scan (&pIf6->mcastIlist, pAddrInfo, tIp6McastInfo *)
    {
        if (Ip6AddrMatch (&(pAddrInfo->ip6Addr), pAddr,
                          IP6_ADDR_SIZE_IN_BITS) == TRUE)
        {
            /* Matching Address Present. Increment the Ref Count */
            return IP6_SUCCESS;
        }

        if (Ip6IsAddrGreater (pAddr, &pAddrInfo->ip6Addr) == SUCCESS)
        {
            /* Address are stored in the Ascending order. New Address
             * is smaller than existing address. So add the new
             * address before this address */
            break;
        }
        pPrevAddrInfo = pAddrInfo;

    }
    /* Allocate memory and add the new MCAST Address */
    pAddrInfo = (tIp6McastInfo *) MemAllocMemBlk (gIp6GblInfo.Ip6mcastId);

    if (pAddrInfo == NULL)
    {
        return IP6_FAILURE;
    }
    MEMSET (pAddrInfo, 0, sizeof (tIp6McastInfo));
    MEMCPY (&(pAddrInfo->ip6Addr), pAddr, sizeof (tIp6Addr));

    /* Add the address to the list of addresses on the interface */
    TMO_SLL_Insert (&pIf6->mcastIlist, &(pPrevAddrInfo->nextAif),
                    &(pAddrInfo->nextAif));

    /* Set the Ref Count */
    pAddrInfo->u1AddrScope = Ip6GetAddrScope (pAddr);

    NetIpv6InvokeAddressChange (pAddr, IP6_ADDR_MAX_PREFIX, ADDR6_MULTI,
                                pIf6->u4IfIndex, NETIPV6_ADDRESS_ADD);
    return IP6_SUCCESS;
}

  /******************************************************************************
  * DESCRIPTION : Called when an LinkLocal address list is to be deleted
  *               from NPAPI.
  * INPUTS      : Pointer to the interface structure
  *
  * OUTPUTS     : None
  *
  * RETURNS     : OSIX_SUCCESS
  *
  ******************************************************************************/
 INT4
 Lip6AddrDeleteLLAddrList (tLip6If * pIf6Entry)
 {
     tLip6AddrNode      *pSearchNode = NULL;
     tLip6AddrNode      *pTmpSearchNode = NULL;
     TMO_DYN_SLL_Scan(&(pIf6Entry->Ip6LLAddrList), pSearchNode, pTmpSearchNode,  tLip6AddrNode * )
     {
         NetIpv6InvokeAddressChange (&(pSearchNode->Ip6Addr), IP6_ADDR_MAX_PREFIX, ADDR6_LLOCAL,
                 pIf6Entry->u4IfIndex, NETIPV6_ADDRESS_DELETE);
     }
     return OSIX_SUCCESS;
 }

#endif /* _LIP6ADDR_C */
/* END OF FILE */
