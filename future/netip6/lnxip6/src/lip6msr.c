/********************************************************************
 * Copyright (C) 2008 Aricent Inc . All Rights Reserved
 *
 * $Id: lip6msr.c
 *
 * Description: IPv6 incremental MSR routines for Linux IP.
 * *********************************************************************/

#include "lip6inc.h"
#include "ipvx.h"
#include "rmgr.h"

/****************************************************************************
 *  Function    :  IncMsrForIpv6NetToPhyTable
 *
 *   Input      :   i4IfIndex   - The interface index where the entry is added
 *                  u4IpAddress - IP address of the cache entry
 *                  cDataType   - Datatype of the configured value.
 *                  pSetVal     - Configured value that has to be notified.
 *                  pu4ObjectId - Object ID.
 *                  u4OIDLen    - Object Id length.
 *                  IsRowStatus - Rowstatus (TRUE/FALSE)
 *
 * Description :  This function sends notifications to MSR and RM
 *                 for the NetToMedia Table.
 *
 * Output      :  None.
 *
 * Returns     :  None                            
 *
 * ****************************************************************************/
VOID
IncMsrForIpv6NetToPhyTable (INT4 i4IfIndex,
                            tSNMP_OCTET_STRING_TYPE * pIpAddress,
                            CHR1 cDatatype, VOID *pSetVal, UINT4 *pu4ObjectId,
                            UINT4 u4OIdLen, UINT1 IsRowStatus)
{
    UINT4               u4SeqNum = IPVX_ZERO;
    INT4                i4SetValue = IPVX_ZERO;
    tSNMP_OCTET_STRING_TYPE *pSetString;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = pu4ObjectId;
    SnmpNotifyInfo.u4OidLen = u4OIdLen;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = IsRowStatus;
    SnmpNotifyInfo.pLockPointer = Ip6Lock;
    SnmpNotifyInfo.pUnLockPointer = Ip6UnLock;
    SnmpNotifyInfo.u4Indices = IPVX_THREE;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

    switch (cDatatype)
    {
        case 'i':
            i4SetValue = *(INT4 *) pSetVal;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %s %i",
                              i4IfIndex, INET_ADDR_TYPE_IPV6,
                              pIpAddress, i4SetValue));
            break;
        case 's':
            pSetString = (tSNMP_OCTET_STRING_TYPE *) pSetVal;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %s %s",
                              i4IfIndex, INET_ADDR_TYPE_IPV6,
                              pIpAddress, pSetString));
            break;
    }
#ifndef ISS_WANTED
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (pIpAddress);
#endif
    return;
}

/****************************************************************************
 *  Function    :  IncMsrForIpv6ContextTable
 *
 *   Input      :   u4ContextId - The context Id 
 *                  cDataType   - Datatype of the configured value.
 *                  pSetVal     - Configured value that has to be notified.
 *                  pu4ObjectId - Object ID.
 *                  u4OIDLen    - Object Id length.
 *
 * Description :  This function sends notifications to MSR and RM
 *                 for the IPV6 Context Table.
 *
 * Output      :  None.
 *
 * Returns     :  IPVX_SUCCESS or IPVX_FAILURE
 *
 * ****************************************************************************/
VOID
IncMsrForIpv6ContextTable (UINT4 u4ContextId, CHR1 cDatatype, VOID *pSetVal,
                           UINT4 *pu4ObjectId, UINT4 u4OIdLen)
{
    UINT4               u4SeqNum = IPVX_ZERO;
    INT4                i4SetValue = IPVX_ZERO;
    UINT4               u4SetValue = IPVX_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = pu4ObjectId;
    SnmpNotifyInfo.u4OidLen = u4OIdLen;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = Ip6Lock;
    SnmpNotifyInfo.pUnLockPointer = Ip6UnLock;
    SnmpNotifyInfo.u4Indices = IP6_ONE;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

    switch (cDatatype)
    {
        case 'i':
            i4SetValue = *(INT4 *) pSetVal;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                              u4ContextId, i4SetValue));
            break;
        case 'u':
            u4SetValue = *(UINT4 *) pSetVal;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u",
                              u4ContextId, u4SetValue));
            break;
    }
#ifndef ISS_WANTED
    UNUSED_PARAM (u4ContextId);
#endif
    return;
}

/****************************************************************************
 *  Function    :  IncMsrForIpv6AddrTable
 *
 *   Input      :   i4IfIndex   - The interface index where the entry is added
 *                  Ipv6Address - IP address
 *                  i4PrefixLen - Length of IPV6 address 
 *                  cDataType   - Datatype of the configured value.
 *                  pSetVal     - Configured value that has to be notified.
 *                  pu4ObjectId - Object ID.
 *                  u4OIDLen    - Object Id length.
 *                  IsRowStatus - Rowstatus (TRUE/FALSE)
 *
 * Description :  This function sends notifications to MSR and RM
 *                 for the Ipv6Addr Table.
 *
 * Output      :  None.
 *
 * Returns     :  IPVX_SUCCESS or IPVX_FAILURE
 *
 * ****************************************************************************/
VOID
IncMsrForIpv6AddrTable (INT4 i4Index, tSNMP_OCTET_STRING_TYPE * Ipv6Address,
                        INT4 i4PrefixLen, CHR1 cDatatype, VOID *pSetVal,
                        UINT4 *pu4ObjectId, UINT4 u4OIdLen, UINT1 IsRowStatus)
{
    UINT4               u4SeqNum = IPVX_ZERO;
    INT4                i4SetValue = IPVX_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = pu4ObjectId;
    SnmpNotifyInfo.u4OidLen = u4OIdLen;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = IsRowStatus;
    SnmpNotifyInfo.pLockPointer = Ip6Lock;
    SnmpNotifyInfo.pUnLockPointer = Ip6UnLock;
    SnmpNotifyInfo.u4Indices = IPVX_THREE;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

    switch (cDatatype)
    {
        case 'i':
            i4SetValue = *(INT4 *) pSetVal;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %s %i %i",
                              i4Index, Ipv6Address, i4PrefixLen, i4SetValue));
            break;
    }
#ifndef ISS_WANTED
    UNUSED_PARAM (i4Index);
    UNUSED_PARAM (Ipv6Address);
    UNUSED_PARAM (i4PrefixLen);
#endif
    return;
}

/****************************************************************************
 *  Function    :  IncMsrForIpv6AddrProfileTable 
 *
 *   Input      :   u4ProfileIndex - Index of AddrProfileTable
 *                  cDataType   - Datatype of the configured value.
 *                  pSetVal     - Configured value that has to be notified.
 *                  pu4ObjectId - Object ID.
 *                  u4OIDLen    - Object Id length.
 *                  IsRowStatus - Rowstatus (TRUE/FALSE)
 *
 * Description :  This function sends notifications to MSR and RM
 *                 for the Ipv6AddrProfile Table.
 *
 * Output      :  None.
 *
 * Returns     :  IPVX_SUCCESS or IPVX_FAILURE
 *
 * ****************************************************************************/
VOID
IncMsrForIpv6AddrProfileTable (UINT4 u4ProfileIndex, CHR1 cDatatype,
                               VOID *pSetVal, UINT4 *pu4ObjectId,
                               UINT4 u4OIdLen)
{
    UINT4               u4SeqNum = IP6_ZERO;
    INT4                i4SetValue = IP6_ZERO;
    UINT4               u4SetValue = IP6_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = pu4ObjectId;
    SnmpNotifyInfo.u4OidLen = u4OIdLen;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = Ip6Lock;
    SnmpNotifyInfo.pUnLockPointer = Ip6UnLock;
    SnmpNotifyInfo.u4Indices = IP6_ONE;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

    switch (cDatatype)
    {
        case 'i':
            i4SetValue = *(INT4 *) pSetVal;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %i",
                              u4ProfileIndex, i4SetValue));
            break;
        case 'u':
            u4SetValue = *(INT4 *) pSetVal;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u",
                              u4ProfileIndex, u4SetValue));
            break;
    }
#ifndef ISS_WANTED
    UNUSED_PARAM (u4ProfileIndex);
#endif
    return;
}

/****************************************************************************
 *  Function    :  IncMsrForIpv6PmtuTable     
 *
 *   Input      :   u4ContextId - The context in which the pmtu entry is added
 *                  pPmtuDest   - Pmtu Entry Destination address
 *                  i4SetVal    - Configured value that has to be notified.
 *                  pu4ObjectId - Object ID.
 *                  u4OIDLen    - Object Id length.
 *
 * Description :  This function sends notifications to MSR and RM
 *                 for the FsMIIpv6PmtuTable.
 *
 * Output      :  None.
 *
 * Returns     :  None                            
 *
 * ****************************************************************************/
VOID
IncMsrForIpv6PmtuTable (UINT4 u4ContextId, tSNMP_OCTET_STRING_TYPE * pIpAddress,
                        INT4 i4SetVal, UINT4 *pu4ObjectId, UINT4 u4OIdLen)
{
    UINT4               u4SeqNum = IP6_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = pu4ObjectId;
    SnmpNotifyInfo.u4OidLen = u4OIdLen;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = Ip6Lock;
    SnmpNotifyInfo.pUnLockPointer = Ip6UnLock;
    SnmpNotifyInfo.u4Indices = IP6_TWO;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %s %i",
                      (INT4) u4ContextId, pIpAddress, i4SetVal));
#ifndef ISS_WANTED
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (pIpAddress);
    UNUSED_PARAM (i4SetVal);
#endif
    return;
}

/****************************************************************************
 *  Function    :  IncMsrForIpv6IfTable
 *
 *   Input      :   i4IfIndex   - The interface index where the entry is added
 *                  cDataType   - Datatype of the configured value.
 *                  pSetVal     - Configured value that has to be notified.
 *                  pu4ObjectId - Object ID.
 *                  u4OIDLen    - Object Id length.
 *
 * Description :  This function sends notifications to MSR and RM
 *                 for the Ipv6 IfTable.
 *
 * Output      :  None.
 *
 * Returns     :  None                            
 *
 * ****************************************************************************/
VOID
IncMsrForIpv6IfTable (INT4 i4IfIndex, CHR1 cDatatype, VOID *pSetVal,
                      UINT4 *pu4ObjectId, UINT4 u4OIdLen)
{
    tSNMP_OCTET_STRING_TYPE *pSetString;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = IP6_ZERO;
    INT4                i4SetValue;

    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = pu4ObjectId;
    SnmpNotifyInfo.u4OidLen = u4OIdLen;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = Ip6Lock;
    SnmpNotifyInfo.pUnLockPointer = Ip6UnLock;
    SnmpNotifyInfo.u4Indices = IP6_ONE;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

    switch (cDatatype)
    {
        case 'i':
            i4SetValue = *(INT4 *) pSetVal;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", i4IfIndex, i4SetValue));
            break;
        case 's':
            pSetString = (tSNMP_OCTET_STRING_TYPE *) pSetVal;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %s", i4IfIndex, pSetString));
            break;
    }
#ifndef ISS_WANTED
    UNUSED_PARAM (i4IfIndex);
#endif
    return;
}

/****************************************************************************
 *  Function    :  IncMsrForIp6PrefixTable
 *
 *   Input      :   i4IfIndex   - The interface index where the entry is added
 *                  i4PrefixLen - Prefix address length
 *                  pIpAddress  - IP address of the prefix entry
 *                  i4SetVal    - Configured value that has to be notified.
 *                  pu4ObjectId - Object ID.
 *                  u4OIDLen    - Object Id length.
 *                  IsRowStatus - Rowstatus (TRUE/FALSE)
 *
 * Description :  This function sends notifications to MSR and RM
 *                 for the fsipv6PrefixTable
 *
 * Output      :  None.
 *
 * Returns     :  None                            
 *
 * ****************************************************************************/

VOID
IncMsrForIp6PrefixTable (INT4 i4IfIndex, INT4 i4PrefixLen,
                         tSNMP_OCTET_STRING_TYPE * pIpAddress,
                         INT4 i4SetVal, UINT4 *pu4ObjectId,
                         UINT4 u4OIdLen, UINT1 IsRowStatus)
{
    UINT4               u4SeqNum = IP6_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = pu4ObjectId;
    SnmpNotifyInfo.u4OidLen = u4OIdLen;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = IsRowStatus;
    SnmpNotifyInfo.pLockPointer = Ip6Lock;
    SnmpNotifyInfo.pUnLockPointer = Ip6UnLock;
    SnmpNotifyInfo.u4Indices = IPVX_THREE;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %s %i %i",
                      i4IfIndex, pIpAddress, i4PrefixLen, i4SetVal));
#ifndef ISS_WANTED
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (pIpAddress);
    UNUSED_PARAM (i4PrefixLen);
    UNUSED_PARAM (i4SetVal);
#endif
    return;
}

/* END OF FILE */
