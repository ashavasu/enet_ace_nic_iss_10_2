/*****************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: lip6netl.c,v 1.14 2016/03/05 12:03:05 siva Exp $
 *
 * Description: Routines that configures Linux networking thru Netlink sockets
 *
 ****************************************************************************/

#ifndef _LIP6NETL_C_
#define _LIP6NETL_C_

#include "lip6inc.h"

PRIVATE INT4        Lip6NetLinkSockCreate (tNlSock * pNlSock, UINT4 u4Groups);
PRIVATE VOID        Lip6NetLinkRdSockInit (UINT4 u4ContextId);
PRIVATE INT4        Lip6NetLinkAddAddrAttr (struct nlmsghdr *pNlHdr,
                                            INT4 i4MaxLen, INT4 i4Type,
                                            VOID *pData, INT4 i4Len);
PRIVATE INT4        Lip6NetLinkAddIntAttr (struct nlmsghdr *pNlHdr,
                                           INT4 i4MaxLen, INT4 i4Type,
                                           INT4 i4Metric);
PRIVATE INT4        Lip6NetLinkTalk (struct nlmsghdr *pNlMsgHdr,
                                     tNlSock * pNlSock, UINT2 u2Afi);
PRIVATE INT4
 
 
 
 Lip6NetLinkParseInfo (INT4 (*pFilter) (struct sockaddr_nl *, struct nlmsghdr *,
                                        UINT2, VOID *),
                       tNlSock * pNlSock, UINT2 u2Afi, VOID *pFilterArg);
PRIVATE INT4
 
 
 
 Lip6NetLinkTalkFilter (struct sockaddr_nl *Snl, struct nlmsghdr *pNlHdr,
                        UINT2 u2Afi, VOID *pArg);

PRIVATE const char *Lip6NetLinkLookup (struct message *mes, INT4 key);

#ifdef LINUX_310_WANTED

CHR1               *ll6_kind = "macvlan";
PRIVATE INT4        Lip6NetClose (tNlSock * pNlSock);
PRIVATE VOID
 
 
 
 Lip6ParseRtattr (struct rtattr **tb, INT4 i4MaxLen, struct rtattr *rta,
                  INT4 i4Len);

PRIVATE INT4
       Lip6NetRequest (tNlSock * pNlSock, INT4 i4Family, INT4 i4Type);

PRIVATE INT4
 
 
 
 Lip6NetIfAddFilter (struct sockaddr_nl *Snl, struct nlmsghdr *pNlHdr,
                     UINT2 u2Afi, VOID *pArg);

PRIVATE INT4        Lip6NetAddLookup (VOID);

PRIVATE INT4        Lip6NetSetBlock (tNlSock * pNlSock, INT4 *pi4Flags);

PRIVATE INT4
 
 
 
 Lip6AddAttr32 (struct nlmsghdr *pNlHdr, INT4 i4Maxlen, INT4 i4Type,
                UINT4 u4data);
PRIVATE INT4        Lip6NetLinkSetVmacAddr (tVrrpNwIntf * pVrrpNwIntf);

PRIVATE INT4        Lip6NetLinkSetMode (tVrrpNwIntf * pVrrpNwIntf);

PRIVATE INT4        Lip6NetLinkUp (tVrrpNwIntf * pVrrpNwIntf);

PRIVATE INT4
       Lip6NetLinkCreateVif (UINT1 *pu1DevName, tVrrpNwIntf * pVrrpNwIntf);

PRIVATE INT4        Lip6NetLinkDelVmac (tVrrpNwIntf * pVrrpNwIntf);

extern INT4         CfaGetIfName (UINT4 u4IfIndex, UINT1 *au1IfName);
#endif

struct message      nlmsg_str[] = {
    {RTM_NEWROUTE, "RTM_NEWROUTE"},
    {RTM_DELROUTE, "RTM_DELROUTE"},
    {RTM_GETROUTE, "RTM_GETROUTE"},
    {RTM_NEWNEIGH, "RTM_NEWNEIGH"},
    {RTM_DELNEIGH, "RTM_DELNEIGH"},
    {RTM_GETNEIGH, "RTM_GETNEIGH"},
    {RTM_NEWADDR, "RTM_NEWADDR"},
    {RTM_DELADDR, "RTM_DELADDR"},
    {RTM_GETADDR, "RTM_GETADDR"},
    {0, NULL}
};
PRIVATE UINT1       gbuf[4096];
/******************************************************************************
 * FUNCTION    : Lip6NetLinkSockInit
 * DESCRIPTION : Routine to initialize Netlink socket that will be used
 *               for interface and route configuration in Linux IP
 * INPUTS      : None
 * OUTPUTS     : None
 * RETURNS     : OSIX_SUCCESS
 *               OSIX_FAILURE
 ******************************************************************************/
PUBLIC VOID
Lip6NetLinkSockInit (VOID)
{
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    Lip6NetLinkSockCreate (&gLnxVrfNetlinkCmd[VCM_DEFAULT_CONTEXT], 0);
#else
    Lip6NetLinkSockCreate (&gNetlinkCmd, 0);
#endif
    return;
}

/******************************************************************************
 * FUNCTION    : Lip6NetLinkRouteModify
 * DESCRIPTION : Routine to modify IPv6 route entry in Linux IP
 * INPUTS      : i4Command  - RTM_NEWROUTE/RTM_DELROUTE/RTM_NEWROUTE
 *               pNetRtInfo - IPv6 Route Info
 * OUTPUTS     : None
 * RETURNS     : OSIX_SUCCESS
 *               OSIX_FAILURE
 ******************************************************************************/
PUBLIC INT4
Lip6NetLinkRouteModify (INT4 i4Command, tNetIpv6RtInfo * pNetRtInfo)
{
    struct sockaddr_nl  SockAddrNl;
    tNlSock            *pNlSock = NULL;
    tNlReq              NlReq;
    tIp6Addr            AnyIp;
    tLip6If            *pIf6Entry = NULL;
    INT4                i4ByteLen = 0;
    INT4                i4RetVal = OSIX_FAILURE;
#if defined(VRF_WANTED) && defined (LINUX_310_WANTED)
    UINT4               u4ContextId = VCM_DEFAULT_CONTEXT;
    UINT4               u4CfaIfIndex = 0;
    INT4                i4SockFd = -1;
    tLnxVrfEventInfo  LnxVrfEventInfo;
#endif

    MEMSET (&NlReq, 0, sizeof (NlReq));
    MEMSET (&AnyIp, 0, sizeof (tIp6Addr));

#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    u4ContextId = pNetRtInfo->u4ContextId;
    NetIpv6GetCfaIfIndexFromPort(pNetRtInfo->u4Index,&u4CfaIfIndex);
    LnxVrfEventInfo.u4ContextId = u4ContextId;
    LnxVrfEventInfo.i4Sockdomain = AF_NETLINK;
    LnxVrfEventInfo.i4SockType=SOCK_RAW;
    LnxVrfEventInfo.i4SockProto = NETLINK_ROUTE;
    LnxVrfEventInfo.u4IfIndex = u4CfaIfIndex;
    LnxVrfEventInfo.u1MsgType =LNX_VRF_OPEN_SOCKET ;

    if (gLnxVrfNetlinkCmd[u4ContextId].i4Sock < 0)
    {
        LnxVrfSockLock();
        LnxVrfEventHandling(&LnxVrfEventInfo,&i4SockFd);
        LnxVrfSockUnLock();
        gLnxVrfNetlinkCmd[u4ContextId].i4Sock = i4SockFd;
        Lip6NetLinkSockCreate (&gLnxVrfNetlinkCmd[u4ContextId], 0);
    }
#else
    if (gNetlinkCmd.i4Sock < 0)
    {
        Lip6NetLinkRdSockInit (VCM_DEFAULT_CONTEXT);
    }
#endif

    i4ByteLen = LIP6_ADDR_LEN;

    NlReq.Nl.nlmsg_len = NLMSG_LENGTH (sizeof (struct rtmsg));
    if (i4Command == RTM_NEWROUTE)
    {
        NlReq.Nl.nlmsg_flags = NLM_F_CREATE | NLM_F_REQUEST;
    }
    else
    {
        NlReq.Nl.nlmsg_flags = NLM_F_CREATE | NLM_F_REQUEST | NLM_F_REPLACE;
    }

    NlReq.Nl.nlmsg_type = i4Command;
    NlReq.Rt.rtm_family = AF_INET6;
    NlReq.Rt.rtm_table = RT_TABLE_MAIN;
    NlReq.Rt.rtm_dst_len = pNetRtInfo->u1Prefixlen;

    if (i4Command == RTM_NEWROUTE)
    {
        /*NlReq.Rt.rtm_protocol = RTPROT_ZEBRA; */
        NlReq.Rt.rtm_protocol = RT_FS_PROTO;
        NlReq.Rt.rtm_scope = RT_SCOPE_UNIVERSE;
        NlReq.Rt.rtm_type = RTN_UNICAST;
    }

    i4RetVal = Lip6NetLinkAddAddrAttr (&NlReq.Nl, sizeof NlReq, RTA_DST,
                                       pNetRtInfo->Ip6Dst.u1_addr, i4ByteLen);
    if (i4RetVal == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Metric */
    i4RetVal = Lip6NetLinkAddIntAttr (&NlReq.Nl, sizeof NlReq, RTA_PRIORITY,
                                      (INT4) pNetRtInfo->u4Metric);
    if (i4RetVal == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* NextHop */
    if (MEMCMP (&pNetRtInfo->NextHop, &AnyIp, sizeof (tIp6Addr)) != 0)
    {
        i4RetVal = Lip6NetLinkAddAddrAttr (&NlReq.Nl, sizeof NlReq, RTA_GATEWAY,
                                           pNetRtInfo->NextHop.u1_addr,
                                           i4ByteLen);
        if (i4RetVal == OSIX_FAILURE)
        {
            return OSIX_FAILURE;
        }
    }

    if (pNetRtInfo->u4Index > 0)
    {
        pIf6Entry = Lip6UtlGetIfEntry (pNetRtInfo->u4Index);
        if (pIf6Entry == NULL)
        {
            return OSIX_FAILURE;
        }

        i4RetVal = Lip6NetLinkAddIntAttr (&NlReq.Nl, sizeof NlReq, RTA_OIF,
                                          (INT4) pIf6Entry->u4IpPort);
        if (i4RetVal == OSIX_FAILURE)
        {
            return OSIX_FAILURE;
        }
    }

    /* Configure metric */
    i4RetVal = Lip6NetLinkAddIntAttr (&NlReq.Nl, sizeof NlReq, RTA_METRICS,
                                      (INT4) pNetRtInfo->u4Metric);
    if (i4RetVal == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Destination netlink address. */
    MEMSET (&SockAddrNl, 0, sizeof SockAddrNl);
    SockAddrNl.nl_family = AF_NETLINK;

#if defined(VRF_WANTED) && defined (LINUX_310_WANTED)
    u4ContextId = pNetRtInfo->u4ContextId;
    NetIpv6GetCfaIfIndexFromPort(pNetRtInfo->u4Index,&u4CfaIfIndex);
    LnxVrfEventInfo.u4ContextId = u4ContextId;
    LnxVrfEventInfo.i4Sockdomain = AF_NETLINK;
    LnxVrfEventInfo.i4SockType=SOCK_RAW;
    LnxVrfEventInfo.i4SockProto = NETLINK_ROUTE;
    LnxVrfEventInfo.u4IfIndex = u4CfaIfIndex;
    LnxVrfEventInfo.u1MsgType =LNX_VRF_OPEN_SOCKET ;

    if (gLnxVrfNetlinkCmd[u4ContextId].i4Sock < 0)
    {
        LnxVrfSockLock();
        LnxVrfEventHandling(&LnxVrfEventInfo,&i4SockFd);
        LnxVrfSockUnLock();
        gLnxVrfNetlinkCmd[u4ContextId].i4Sock = i4SockFd;
        Lip6NetLinkRdSockInit (u4ContextId);
    }
    pNlSock = &gLnxVrfNetlinkCmd[u4ContextId];
#else
    if (gNetlinkCmd.i4Sock < 0)
    {
        Lip6NetLinkRdSockInit (VCM_DEFAULT_CONTEXT);
    }
    pNlSock = &gNetlinkCmd;

#endif


    /* Talk to netlink socket. */
    return (Lip6NetLinkTalk (&NlReq.Nl, pNlSock, INET_AFI_IPV6));

}

/******************************************************************************
 * FUNCTION    : Lip6NlResolveNd
 * DESCRIPTION : Routine to resolve NH for ECMP routes
 * INPUTS      : u4IfIndex- Index of the next hop interface
 *               pNextHop - pointer to next hop to be resolved
 * OUTPUTS     : None
 * RETURNS     : OSIX_SUCCESS
 *               OSIX_FAILURE
 ******************************************************************************/

PUBLIC INT4
Lip6NlResolveNd (UINT4 u4IfIndex, tIp6Addr * pNextHop)
{

    /*Trying to use raw socket over IPv6 to trigger ND */

    /*Create a raw socket */

    INT4                i4RawSock;
    INT4                i4Count;
    INT4                i4Value;
    struct msghdr       msg;
    struct iovec        iov[1];
    struct in6_pktinfo *pIpPktInfo = NULL;
    struct sockaddr_in6 destAddr;
    struct sockaddr_in6 srcAddr;
    struct cmsghdr     *pCmsgInfo;
    UINT1               au1CmsgInfo[112];
    UINT1               au1RawPkt[200] = { "This is a test packet" };
    UINT1              *pu1RawPkt = au1RawPkt;
    tLip6If            *pIf6Entry = NULL;
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED) && defined (LNXIP4_WANTED)
    tLnxVrfEventInfo    LnxVrfEventInfo;
    tCfaIfInfo          CfaIfInfo;
    UINT4               u4ContextId = VCM_DEFAULT_CONTEXT;
    UINT4               u4CfaIfIndex = CFA_INVALID_INDEX;
#endif


    MEMSET (&srcAddr, 0, sizeof (tIp6Addr));
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED) && defined (LNXIP4_WANTED)
    if (NetIpv4GetCfaIfIndexFromPort (u4IfIndex,
                                      &u4CfaIfIndex) == NETIPV4_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (CfaGetIfInfo (u4CfaIfIndex, &CfaIfInfo) == CFA_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if(VcmGetContextIdFromCfaIfIndex(u4CfaIfIndex, &u4ContextId) == VCM_FAILURE)
    {
        return OSIX_FAILURE;
    }
    LnxVrfEventInfo.u4ContextId = u4ContextId;
    LnxVrfEventInfo.i4Sockdomain = AF_INET6;
    LnxVrfEventInfo.i4SockType = SOCK_RAW;
    LnxVrfEventInfo.i4SockProto = IPPROTO_IPV6;
    LnxVrfEventInfo.u4IfIndex = u4CfaIfIndex;
    LnxVrfEventInfo.u1MsgType = LNX_VRF_OPEN_SOCKET;
    LnxVrfSockLock();
    LnxVrfEventHandling(&LnxVrfEventInfo,&i4RawSock);
    LnxVrfSockUnLock();
#else
    i4RawSock = socket (AF_INET6, SOCK_RAW, IPPROTO_ICMPV6);
#endif

    if (i4RawSock < 0)
    {
        return OSIX_FAILURE;
    }

    i4Value = 2;
    i4Count = setsockopt (i4RawSock, SOL_RAW, IPV6_CHECKSUM, &i4Value,
                          sizeof (INT4));
    if (i4Count < 0)
    {
        close (i4RawSock);
        return OSIX_FAILURE;
    }

    /*Use the source IPv6 addresss and Dest IPv6 address */

    MEMSET (&destAddr, 0, sizeof (struct sockaddr_in6));
    destAddr.sin6_family = AF_INET6;
    /*destAddr.sin6_port = htons(42); */
    MEMCPY (&destAddr.sin6_addr, pNextHop, sizeof (struct in6_addr));

    iov[0].iov_base = (VOID *) pu1RawPkt;
    iov[0].iov_len = 21;        /*This is length of the payload */
    msg.msg_name = ((void *) &destAddr);
    msg.msg_namelen = sizeof (struct sockaddr_in6);
    msg.msg_iov = (void *) iov;
    msg.msg_iovlen = 1;
    msg.msg_control = (void *) &au1CmsgInfo;
    msg.msg_controllen = CMSG_SPACE (sizeof (struct in6_pktinfo));
    msg.msg_flags = 0;

    pCmsgInfo = CMSG_FIRSTHDR (&msg);
    pCmsgInfo->cmsg_level = IPPROTO_IPV6;
    pCmsgInfo->cmsg_type = IPV6_PKTINFO;
    pCmsgInfo->cmsg_len = CMSG_LEN (sizeof (struct in6_pktinfo));

    pIpPktInfo = (struct in6_pktinfo *) (VOID *) CMSG_DATA (pCmsgInfo);
    MEMCPY (&(pIpPktInfo->ipi6_addr), &srcAddr, sizeof (tIp6Addr));

    pIf6Entry = Lip6UtlGetIfEntry (u4IfIndex);
    pIpPktInfo->ipi6_ifindex = (UINT2) pIf6Entry->u4IpPort;

    i4Count = sendmsg (i4RawSock, &msg, 0);
    if (i4Count < 0)
    {
        close (i4RawSock);
        return OSIX_FAILURE;

    }

    close (i4RawSock);
    return OSIX_SUCCESS;
}

/******************************************************************************
 * FUNCTION    : Lip6NetLinkNDUpdate
 * DESCRIPTION : Routine to add/delete static ND entry in Linux IP
 * INPUTS      : i4Command  - RTM_NEWNEIGH/RTM_DELNEIGH
 *               pNetRtInfo - IPv6 Route Info
 * OUTPUTS     : None
 * RETURNS     : OSIX_SUCCESS
 *               OSIX_FAILURE
 ******************************************************************************/
PUBLIC INT4
Lip6NetLinkNDUpdate (INT4 i4Command, tLip6NdEntry * pNd6cEntry)
{
    struct sockaddr_nl  SockAddrNl;
    tNlSock            *pNlSock = NULL;
    tNlReqNd            NlReq;
    INT4                i4ByteLen = 0;
    INT4                i4RetVal = OSIX_FAILURE;
#if defined(VRF_WANTED) && defined (LINUX_310_WANTED)
    UINT4               u4ContextId = VCM_DEFAULT_CONTEXT;
    UINT4               u4CfaIfIndex = 0;
    INT4                i4SockFd = -1;
    tLnxVrfEventInfo  LnxVrfEventInfo;
#endif

    MEMSET (&NlReq, 0, sizeof NlReq);

    i4ByteLen = LIP6_ADDR_LEN;

    NlReq.Nl.nlmsg_len = NLMSG_LENGTH (sizeof (struct ndmsg));

    if (i4Command == RTM_NEWNEIGH)
    {
        NlReq.Nl.nlmsg_flags = NLM_F_CREATE | NLM_F_REQUEST | NLM_F_REPLACE;
    }
    else                        /* RTM_DELNEIGH */
    {
        NlReq.Nl.nlmsg_flags = NLM_F_REQUEST;
    }

    NlReq.Nl.nlmsg_type = i4Command;
    NlReq.Ndm.ndm_family = AF_INET6;
    NlReq.Ndm.ndm_state = NUD_PERMANENT;

    NlReq.Ndm.ndm_ifindex = pNd6cEntry->pIf6Entry->u4IpPort;

    i4RetVal = Lip6NetLinkAddAddrAttr (&NlReq.Nl, sizeof NlReq, NDA_DST,
                                       pNd6cEntry->Ip6Addr.u1_addr, i4ByteLen);
    if (i4RetVal == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    i4RetVal = Lip6NetLinkAddAddrAttr (&NlReq.Nl, sizeof NlReq, NDA_LLADDR,
                                       pNd6cEntry->MacAddr, sizeof (tMacAddr));
    if (i4RetVal == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Destination netlink address. */
    MEMSET (&SockAddrNl, 0, sizeof SockAddrNl);
    SockAddrNl.nl_family = AF_NETLINK;

#if defined(VRF_WANTED) && defined (LINUX_310_WANTED)
    u4ContextId = pNd6cEntry->pIf6Entry->u4ContextId;
    NetIpv6GetCfaIfIndexFromPort(pNd6cEntry->pIf6Entry->u4IfIndex,&u4CfaIfIndex);
    LnxVrfEventInfo.u4ContextId = u4ContextId;
    LnxVrfEventInfo.i4Sockdomain = AF_NETLINK;
    LnxVrfEventInfo.i4SockType=SOCK_RAW;
    LnxVrfEventInfo.i4SockProto = NETLINK_ROUTE;
    LnxVrfEventInfo.u4IfIndex = u4CfaIfIndex;
    LnxVrfEventInfo.u1MsgType =LNX_VRF_OPEN_SOCKET ;

    if (gLnxVrfNetlinkCmd[u4ContextId].i4Sock < 0)
    {
        LnxVrfSockLock();
        LnxVrfEventHandling(&LnxVrfEventInfo,&i4SockFd);
        LnxVrfSockUnLock();
        gLnxVrfNetlinkCmd[u4ContextId].i4Sock = i4SockFd;
        Lip6NetLinkRdSockInit (u4ContextId);
    }
    pNlSock = &gLnxVrfNetlinkCmd[u4ContextId];
#else
    if (gNetlinkCmd.i4Sock < 0)
    {
        Lip6NetLinkRdSockInit (VCM_DEFAULT_CONTEXT);
    }
    pNlSock = &gNetlinkCmd;
#endif

    /* Talk to netlink socket. */
    return Lip6NetLinkTalk (&NlReq.Nl, pNlSock, INET_AFI_IPV6);

}

/******************************************************************************
 * FUNCTION    : Lip6NetLinkSockCreate
 * DESCRIPTION : Create Netlink socket using the given info
 * INPUTS      : pNlSock   - Netlink socket to create
 *               u4Groups  -
 * OUTPUTS     : None
 * RETURNS     : OSIX_SUCCESS
 *               OSIX_FAILURE
 ******************************************************************************/
PRIVATE INT4
Lip6NetLinkSockCreate (tNlSock * pNlSock, UINT4 u4Groups)
{
    INT4                i4RetVal = 0;
    struct sockaddr_nl  SockAddrNl;
    INT4                i4Sock = 0;
    INT4                i4NameLen = 0;

    if(pNlSock->i4Sock <= 0)
    {
        i4Sock = socket (AF_NETLINK, SOCK_RAW, NETLINK_ROUTE);
        if (i4Sock < 0)
        {
            return OSIX_FAILURE;
        }
    }
    else
    {
        i4Sock = pNlSock->i4Sock;
    }

    i4RetVal = fcntl (i4Sock, F_SETFL, O_NONBLOCK);
    if (i4RetVal < 0)
    {
        close (i4Sock);
        return OSIX_FAILURE;
    }

    MEMSET (&SockAddrNl, 0, sizeof SockAddrNl);
    SockAddrNl.nl_family = AF_NETLINK;
    SockAddrNl.nl_groups = u4Groups;

    /* Bind the socket to the netlink structure for anything. */
    i4RetVal =
        bind (i4Sock, (struct sockaddr *) &SockAddrNl, sizeof SockAddrNl);
    if (i4RetVal < 0)
    {
        close (i4Sock);
        return OSIX_FAILURE;
    }

    /* Multiple netlink sockets will have different nl_pid */
    i4NameLen = sizeof SockAddrNl;
    i4RetVal = getsockname (i4Sock, (struct sockaddr *) &SockAddrNl,
                            (socklen_t *) & i4NameLen);
    if ((i4RetVal < 0) || (i4NameLen != sizeof SockAddrNl))
    {
        close (i4Sock);
        return OSIX_FAILURE;
    }

    pNlSock->Snl = SockAddrNl;
    pNlSock->i4Sock = i4Sock;
    return OSIX_SUCCESS;
}

/******************************************************************************
 * FUNCTION    : Lip6NetLinkRdSockInit
 * DESCRIPTION : Initialize Netlink rd socket
 * INPUTS      : None
 * OUTPUTS     : None
 * RETURNS     : None
 ******************************************************************************/
PRIVATE VOID
Lip6NetLinkRdSockInit (UINT4 u4ContextId)
{
#if defined(VRF_WANTED) && defined (LINUX_310_WANTED)
    Lip6NetLinkSockCreate (&gLnxVrfNetlinkCmd[u4ContextId], 0);
#else
    Lip6NetLinkSockCreate (&gNetlinkCmd, 0);
    UNUSED_PARAM(u4ContextId);
#endif
    return;
}

/******************************************************************************
 * FUNCTION    : Lip6NetLinkAddAddrAttr
 * DESCRIPTION : Add address attribute to Netlink msg header
 * INPUTS      : pNlHdr   - Pointer to Netlink message header
 *               i4MaxLen - Max Message length
 *               i4Type   - Type of data in route
 *               pData    - Pointer to route data
 *               i4Len    - Data length
 * OUTPUTS     : None
 * RETURNS     : OSIX_SUCCESS
 *               OSIX_FAILURE
 ******************************************************************************/
PRIVATE INT4
Lip6NetLinkAddAddrAttr (struct nlmsghdr *pNlHdr, INT4 i4MaxLen, INT4 i4Type,
                        VOID *pData, INT4 i4Len)
{
    INT4                i4ByteLen = 0;
    struct rtattr      *pRtA = NULL;

    i4ByteLen = RTA_LENGTH (i4Len);

    if ((NLMSG_ALIGN (pNlHdr->nlmsg_len) + i4ByteLen) > (UINT4) i4MaxLen)
    {
        return OSIX_FAILURE;
    }

    pRtA = (struct rtattr *) (VOID *) (((INT1 *) pNlHdr) +
                                       NLMSG_ALIGN (pNlHdr->nlmsg_len));
    /* Construct TLV */
    /* Type of data in the route */
    pRtA->rta_type = i4Type;
    /* Data len */
    pRtA->rta_len = i4ByteLen;
    /* Value  */
    MEMCPY (RTA_DATA (pRtA), pData, i4Len);

    /* total packet length = prev length + added length */
    pNlHdr->nlmsg_len = NLMSG_ALIGN (pNlHdr->nlmsg_len) + i4ByteLen;

    return OSIX_SUCCESS;
}

/******************************************************************************
 * FUNCTION    : Lip6NetLinkAddIntAttr
 * DESCRIPTION : Add Metric attribute to Netlink msg header
 * INPUTS      : pNlHdr   - Pointer to Netlink message header
 *               i4MaxLen - Max Message length
 *               i4Type   - Type of data in route
 *               i4Metric - Metric value
 * OUTPUTS     : None
 * RETURNS     : OSIX_SUCCESS
 *               OSIX_FAILURE
 ******************************************************************************/
PRIVATE INT4
Lip6NetLinkAddIntAttr (struct nlmsghdr *pNlHdr, INT4 i4MaxLen,
                       INT4 i4Type, INT4 i4Metric)
{
    struct rtattr      *pRtA = NULL;
    INT4                i4ByteLen = 0;

    i4ByteLen = RTA_LENGTH (4);

    if (NLMSG_ALIGN (pNlHdr->nlmsg_len) + i4ByteLen > (UINT4) i4MaxLen)
    {
        return OSIX_FAILURE;
    }

    pRtA =
        (struct rtattr *) (VOID *) (((INT1 *) pNlHdr) +
                                    NLMSG_ALIGN (pNlHdr->nlmsg_len));
    /* Construct TLV */
    /* Type of data in the route */
    pRtA->rta_type = i4Type;
    /* Data len */
    pRtA->rta_len = i4ByteLen;
    /* VAlue  */
    MEMCPY (RTA_DATA (pRtA), &i4Metric, 4);

    /* total packet length = prev length + added length */
    pNlHdr->nlmsg_len = NLMSG_ALIGN (pNlHdr->nlmsg_len) + i4ByteLen;

    return OSIX_SUCCESS;
}

/******************************************************************************
 * FUNCTION    : Lip6NetLinkTalk
 * DESCRIPTION : Send to instruction and data to Kernel via netlink socket
 * INPUTS      : pNlMsgHdr - Netlink message header
 *               pNlSock   - Netlink socket structure to use
 * OUTPUTS     : None
 * RETURNS     : OSIX_SUCCESS
 *               OSIX_FAILURE
 ******************************************************************************/
PRIVATE INT4
Lip6NetLinkTalk (struct nlmsghdr *pNlMsgHdr, tNlSock * pNlSock, UINT2 u2Afi)
{
    struct sockaddr_nl  SockAddrNl;
    struct iovec        Iov = { (void *) NULL, 0 };
    struct msghdr       Msg =
        { (void *) NULL, sizeof SockAddrNl, (void *) NULL, 1, NULL, 0, 0 };
    INT4                i4Status = 0;
    INT4                i4Flags = 0;

    Iov.iov_base = (void *) pNlMsgHdr;
    Iov.iov_len = pNlMsgHdr->nlmsg_len;
    Msg.msg_name = (void *) &SockAddrNl;
    Msg.msg_iov = (void *) &Iov;
    Msg.msg_flags |= MSG_DONTWAIT;

    MEMSET (&SockAddrNl, 0, sizeof SockAddrNl);
    SockAddrNl.nl_family = AF_NETLINK;

    pNlMsgHdr->nlmsg_seq = 0;
    pNlMsgHdr->nlmsg_pid = pNlSock->Snl.nl_pid;

    /* Request an acknowledgement by setting NLM_F_ACK */
    pNlMsgHdr->nlmsg_flags |= NLM_F_ACK;

    /* Send message to netlink interface. */
    i4Status = sendmsg (pNlSock->i4Sock, &Msg, 0);
    if (i4Status < 0)
    {
        return OSIX_FAILURE;
    }

    /* Change socket i4Flags for blocking I/O.
     * This ensures we wait for a reply in Lip6NetLinkParseInfo() */

    if ((i4Flags = fcntl (pNlSock->i4Sock, F_GETFL, 0)) < 0)
    {
        LIP6_TRC_ARG3 (LIP6_MOD_TRC, ALL_FAILURE_TRC,
                       "%s:%i F_GETFL error: %s",
                       __FUNCTION__, __LINE__, strerror (errno));
    }
    i4Flags &= ~O_NONBLOCK;
    if (fcntl (pNlSock->i4Sock, F_SETFL, i4Flags) < 0)
    {
        LIP6_TRC_ARG3 (LIP6_MOD_TRC, ALL_FAILURE_TRC,
                       "%s:%i F_SETFL error: %s",
                       __FUNCTION__, __LINE__, strerror (errno));
    }

    /* Get reply from netlink socket.
     * The reply should either be an acknowlegement or an error */
    i4Status =
        Lip6NetLinkParseInfo (Lip6NetLinkTalkFilter, pNlSock, u2Afi, NULL);

    /* Restore socket i4Flags for nonblocking I/O */
    i4Flags |= O_NONBLOCK;
    if (fcntl (pNlSock->i4Sock, F_SETFL, i4Flags) < 0)
    {
        LIP6_TRC_ARG3 (LIP6_MOD_TRC, ALL_FAILURE_TRC,
                       "%s:%i F_SETFL error: %s",
                       __FUNCTION__, __LINE__, strerror (errno));
    }

    if (i4Status < 0)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/* Receive message from netlink interface and pass those information
 * to the given function. */
PRIVATE INT4
Lip6NetLinkParseInfo (INT4 (*pFilter) (struct sockaddr_nl *, struct nlmsghdr *,
                                       UINT2, VOID *),
                      tNlSock * pNlSock, UINT2 u2Afi, VOID *pFilterArg)
{
    INT4                i4Status;
    INT4                ret = 0;
    INT4                error;

    while (1)
    {
        char                buf[4096];
        struct iovec        iov = { NULL, 0 };
        struct sockaddr_nl  Snl;
        struct msghdr       msg =
            { (void *) NULL, sizeof Snl, (void *) NULL, 1, NULL,
            0, 0
        };
        struct nlmsghdr    *pNlHdr;

        iov.iov_base = (void *) buf;
        iov.iov_len = sizeof (buf);
        msg.msg_name = (void *) &Snl;
        msg.msg_iov = (void *) &iov;

        i4Status = recvmsg (pNlSock->i4Sock, &msg, 0);

        if (i4Status < 0)
        {
            if (errno == EINTR)
                continue;
            if (errno == EWOULDBLOCK)
                break;
            LIP6_TRC_ARG1 (LIP6_MOD_TRC, CONTROL_PLANE_TRC,
                           "%s recvmsg overrun", pNlSock->pi1Name);
            continue;
        }
        if (i4Status == 0)
        {
            LIP6_TRC_ARG1 (LIP6_MOD_TRC, CONTROL_PLANE_TRC,
                           "%s EOF", pNlSock->pi1Name);
            return -1;
        }

        if (msg.msg_namelen != sizeof Snl)
        {
            LIP6_TRC_ARG2 (LIP6_MOD_TRC, ALL_FAILURE_TRC,
                           "%s sender address length error: length %d",
                           pNlSock->pi1Name, msg.msg_namelen);
            return -1;
        }

        for (pNlHdr = (struct nlmsghdr *) (VOID *) buf;
             NLMSG_OK (pNlHdr, (UINT4) i4Status);
             pNlHdr = ((i4Status) -= NLMSG_ALIGN ((pNlHdr)->nlmsg_len),
                       (struct nlmsghdr *) (VOID *) (((char *) (pNlHdr)) +
                                                     NLMSG_ALIGN ((pNlHdr)->
                                                                  nlmsg_len))))

        {
            /* Finish of reading. */
            if (pNlHdr->nlmsg_type == NLMSG_DONE)
                return ret;

            /* Error handling. */
            if (pNlHdr->nlmsg_type == NLMSG_ERROR)
            {
                struct nlmsgerr    *err =
                    (struct nlmsgerr *) NLMSG_DATA (pNlHdr);

                /* If the error field is zero, then this is an ACK */
                if (err->error == 0)
                {
                    /* return if not a multipart message, otherwise continue
                     *                      *                      */
                    if (!(pNlHdr->nlmsg_flags & NLM_F_MULTI))
                    {
                        return 0;
                    }
                    continue;
                }
                if (pNlHdr->nlmsg_len < NLMSG_LENGTH (sizeof (struct nlmsgerr)))
                {
                    LIP6_TRC_ARG1 (LIP6_MOD_TRC, ALL_FAILURE_TRC,
                                   "%s error: message truncated",
                                   pNlSock->pi1Name);
                    return -1;
                }
                LIP6_TRC_ARG5 (LIP6_MOD_TRC, ALL_FAILURE_TRC,
                               "%s error: %s, type=%s(%u), seq=%u",
                               pNlSock->pi1Name, strerror (-err->error),
                               Lip6NetLinkLookup (nlmsg_str,
                                                  err->msg.nlmsg_type),
                               err->msg.nlmsg_type, err->msg.nlmsg_seq);

                return -1;
            }

            /* OK we got netlink message. */
            LIP6_TRC_ARG5 (LIP6_MOD_TRC, CONTROL_PLANE_TRC,
                           "Lip6NetLinkParseInfo: %s type %s(%u), seq=%u,"
                           "pid=%d",
                           pNlSock->pi1Name,
                           Lip6NetLinkLookup (nlmsg_str, pNlHdr->nlmsg_type),
                           pNlHdr->nlmsg_type, pNlHdr->nlmsg_seq,
                           pNlHdr->nlmsg_pid);

            /* skip unsolicited messages originating from command socket */
#if defined(VRF_WANTED) && defined (LINUX_310_WANTED)
            if (pNlSock != &gLnxVrfNetlinkCmd[VCM_DEFAULT_CONTEXT]
                && pNlHdr->nlmsg_pid == gLnxVrfNetlinkCmd[VCM_DEFAULT_CONTEXT].Snl.nl_pid)
#else
            if (pNlSock != &gNetlinkCmd
                && pNlHdr->nlmsg_pid == gNetlinkCmd.Snl.nl_pid)
#endif
            {
#if !(defined(VRF_WANTED) && defined (LINUX_310_WANTED))
                LIP6_TRC_ARG2 (LIP6_MOD_TRC, CONTROL_PLANE_TRC,
                               "Lip6NetLinkParseInfo: %s packet comes from %s",
                               pNlSock->pi1Name, gNetlinkCmd.pi1Name);
#endif
                continue;
            }
            error = (*pFilter) (&Snl, pNlHdr, u2Afi, pFilterArg);
            if (error < 0)
            {
                LIP6_TRC_ARG1 (LIP6_MOD_TRC, ALL_FAILURE_TRC,
                               "%s pFilter function error", pNlSock->pi1Name);
                ret = error;
            }
        }

        /* After error care. */
        if (msg.msg_flags & MSG_TRUNC)
        {
            LIP6_TRC_ARG1 (LIP6_MOD_TRC, ALL_FAILURE_TRC,
                           "%s error: message truncated", pNlSock->pi1Name);
            continue;
        }
        if (i4Status)
        {
            LIP6_TRC_ARG2 (LIP6_MOD_TRC, ALL_FAILURE_TRC,
                           "%s error: data remnant size %d", pNlSock->pi1Name,
                           i4Status);
            return -1;
        }
    }
    return ret;
}

PRIVATE INT4
Lip6NetLinkTalkFilter (struct sockaddr_nl *Snl, struct nlmsghdr *pNlHdr,
                       UINT2 u2Afi, VOID *pArg)
{
    UNUSED_PARAM (Snl);
    UNUSED_PARAM (pArg);
    UNUSED_PARAM (u2Afi);
    LIP6_TRC_ARG1 (LIP6_MOD_TRC, CONTROL_PLANE_TRC,
                   "netlink_talk: ignoring message type 0x%04x",
                   pNlHdr->nlmsg_type);
    return 0;
}

/* Message lookup function. */
PRIVATE const char *
Lip6NetLinkLookup (struct message *mes, INT4 key)
{
    struct message     *pnt;

    for (pnt = mes; pnt->key != 0; pnt++)
        if (pnt->key == key)
            return (pnt->str);

    return ("");
}

#ifdef LINUX_310_WANTED
/*******************************************************************************
 * Function Name   : Lip6ParseRtattr
 * Description     : Function to parse Rta attributes
 * Global Varibles :
 * Inputs          : tb        - Pointer to Table B
 *                   i4MaxLen  - Max Len
 *                   rta       - Pointer to Rta Attribute
 *                   i4Len     - Length  
 * Output          : None.
 * Returns         : None.
 *
 ******************************************************************************/

VOID
Lip6ParseRtattr (struct rtattr **tb, INT4 i4MaxLen, struct rtattr *rta,
                 INT4 i4Len)
{
    while (RTA_OK (rta, i4Len))
    {
        if (rta->rta_type <= i4MaxLen)
        {
            tb[rta->rta_type] = rta;
        }

        rta = RTA_NEXT (rta, i4Len);
    }

    return;
}

/*******************************************************************************
 * Function Name   : Lip6NetClose
 * Description     : Function to close a netlink socket
 * Global Varibles :
 * Inputs          : pNlSock   - Netlink socket to close
 * Output          : None.
 * Returns         : NETIPV6_SUCCESS.
 *
 ******************************************************************************/
INT4
Lip6NetClose (tNlSock * pNlSock)
{
    close (pNlSock->i4Sock);

    return NETIPV6_SUCCESS;
}

/*******************************************************************************
 * Function Name   : Lip6VrrpKernelNetLinkInit
 * Description     : Function to initialize netlink socket for VRRP
 * Global Varibles :
 * Inputs          :
 * Output          : None.
 * Returns         : OSIX_SUCCESS.
 *                   OSIX_FAILURE.
 ******************************************************************************/
VOID
Lip6VrrpKernelNetLinkInit (VOID)
{
    CHR1                ac1Command[LIP6_LINE_LEN];

    /* First remove the IP Tables already created by us. Do the following to
     * remove the ip table entries.
     *    1. Flush the IP Table Chain "ARICENT" to remove rules created.
     *    2. Delink the IP Table Chain "ARICENT" from standard "INPUT" chain.
     *    3. Delete the Chain
     */
    MEMSET (ac1Command, 0, LIP6_LINE_LEN);

    SNPRINTF (ac1Command, sizeof (ac1Command), "%s", LIP6_IPTBL_FLUSH_CHAIN);
    system (ac1Command);

    MEMSET (ac1Command, 0, LIP6_LINE_LEN);

    SNPRINTF (ac1Command, sizeof (ac1Command), "%s", LIP6_IPTBL_DELINK_CHAIN);
    system (ac1Command);

    MEMSET (ac1Command, 0, LIP6_LINE_LEN);

    SNPRINTF (ac1Command, sizeof (ac1Command), "%s", LIP6_IPTBL_DELETE_CHAIN);
    system (ac1Command);

    /* Now, Create the IP Table Chain for ARICENT. IP Tables to be added
     * only on this chain. Do the followin to create chain.
     *    1. Create Chain
     *    2. Link the chain with standard "INPUT" chain.
     */
    MEMSET (ac1Command, 0, LIP6_LINE_LEN);

    SNPRINTF (ac1Command, sizeof (ac1Command), "%s", LIP6_IPTBL_CREATE_CHAIN);
    system (ac1Command);

    MEMSET (ac1Command, 0, LIP6_LINE_LEN);

    SNPRINTF (ac1Command, sizeof (ac1Command), "%s", LIP6_IPTBL_LINK_CHAIN);
    system (ac1Command);

    /* Start with a netlink address lookup */
    Lip6NetAddLookup ();

    /*
     * Prepare netlink kernel broadcast channel
     * subscribtion. We subscribe to LINK and ADDR
     * netlink broadcast messages.
     */
#if defined(VRF_WANTED) && defined (LINUX_310_WANTED)
    if (gLnxVrfNetlinkCmd[VCM_DEFAULT_CONTEXT].i4Sock < 0)
    {
        Lip6NetLinkSockCreate (&gLnxVrfNetlinkCmd[VCM_DEFAULT_CONTEXT], 0);
    }
#else
    if (gNetlinkCmd.i4Sock < 0)
    {
        Lip6NetLinkSockInit ();
    }
#endif
    return;
}

/*******************************************************************************
 * Function Name   : Lip6NetRequest
 * Description     : Function to Fetch a specific type information
 *                   from netlink kernel
 * Global Varibles :
 * Inputs          : pNlSock - Socket Information
 *                   i4Family  - Family
 *                   i4Type    - Type        
 * Output          : None.
 * Returns         : NETIPV6_SUCCESS.
 *                   NETIPV6_FAILURE.
 ******************************************************************************/
INT4
Lip6NetRequest (tNlSock * pNlSock, INT4 i4Family, INT4 i4Type)
{
    struct sockaddr_nl  snl;
    struct
    {
        struct nlmsghdr     nlh;
        struct rtgenmsg     g;
    } NlReq;
    INT4                i4RetVal = 0;

    MEMSET (&snl, 0, sizeof (snl));
    snl.nl_family = AF_NETLINK;

    NlReq.nlh.nlmsg_len = sizeof (NlReq);
    NlReq.nlh.nlmsg_type = i4Type;
    NlReq.nlh.nlmsg_flags = NLM_F_ROOT | NLM_F_MATCH | NLM_F_REQUEST;
    NlReq.nlh.nlmsg_pid = pNlSock->Snl.nl_pid;
    NlReq.nlh.nlmsg_seq = 0;
    NlReq.g.rtgen_family = i4Family;

    i4RetVal =
        sendto (pNlSock->i4Sock, (VOID *) &NlReq, sizeof (NlReq), 0,
                (struct sockaddr *) &snl, sizeof (snl));

    if (i4RetVal < 0)
    {
        perror ("Netlink: sendto() failed");

        return NETIPV6_FAILURE;
    }

    return NETIPV6_SUCCESS;
}

/*******************************************************************************
 * Function Name   : Lip6NetIfAddFilter
 * Description     : Function to use Netlink interface address lookup filter
 *
 * Global Varibles :
 * Inputs          : Snl           - Socket Netlink Layer Address
 *                   pNlHdr        - Pointer to Netlink Header 
 * Output          : None.
 * Returns         : NETIPV6_SUCCESS.
 *                   NETIPV6_FAILURE.
 ******************************************************************************/
INT4
Lip6NetIfAddFilter (struct sockaddr_nl *Snl, struct nlmsghdr *pNlHdr,
                    UINT2 u2Afi, VOID *pArg)
{
    struct ifaddrmsg   *ifa;
    struct rtattr      *tb[IFA_MAX + 1];
    INT4                i4Len = 0;
    VOID               *addr = NULL;

    UNUSED_PARAM (Snl);
    UNUSED_PARAM (u2Afi);
    UNUSED_PARAM (pArg);

    ifa = NLMSG_DATA (pNlHdr);

    /* Only IPV6 are valid us */
    if (ifa->ifa_family != AF_INET && ifa->ifa_family != AF_INET6)
    {
        return NETIPV6_SUCCESS;
    }

    if (pNlHdr->nlmsg_type != RTM_NEWADDR && pNlHdr->nlmsg_type != RTM_DELADDR)
    {
        return NETIPV6_SUCCESS;
    }

    i4Len = pNlHdr->nlmsg_len - NLMSG_LENGTH (sizeof (struct ifaddrmsg));

    if (i4Len < 0)
    {
        return NETIPV6_FAILURE;
    }

    MEMSET (tb, 0, sizeof (tb));
    Lip6ParseRtattr (tb, IFA_MAX, IFA_RTA (ifa), i4Len);

    if (tb[IFA_LOCAL] == NULL)
    {
        tb[IFA_LOCAL] = tb[IFA_ADDRESS];
    }

    if (tb[IFA_ADDRESS] == NULL)
    {
        tb[IFA_ADDRESS] = tb[IFA_LOCAL];
    }

    /* local interface address */
    addr = (tb[IFA_LOCAL] ? RTA_DATA (tb[IFA_LOCAL]) : NULL);

    if (addr == NULL)
    {
        return NETIPV6_FAILURE;
    }

    return NETIPV6_SUCCESS;
}

/*******************************************************************************
 * Function Name   : Lip6NetAddLookup
 * Description     : Function to Adresses lookup bootstrap function
 * Global Varibles :
 * Inputs          :
 * Output          : None.
 * Returns         : NETIPV6_SUCCESS.
 *                   NETIPV6_FAILURE.
 ******************************************************************************/
INT4
Lip6NetAddLookup (VOID)
{
    tNlSock             NlSock;
    INT4                status = 0;
    INT4                i4Retval, i4Flags;

    MEMSET (&NlSock, 0, sizeof (NlSock));
    if (Lip6NetLinkSockCreate (&NlSock, 0) < 0)
    {
        return NETIPV6_FAILURE;
    }

    /* Set blocking flag */
    i4Retval = Lip6NetSetBlock (&NlSock, &i4Flags);

    if (i4Retval < 0)
    {
        perror ("Setting Netlink socket to Blocking mode failed\r\n");
    }
    /* IPv4 Address lookup */
    if (Lip6NetRequest (&NlSock, AF_INET, RTM_GETADDR) < 0)
    {
        Lip6NetClose (&NlSock);
        return NETIPV6_FAILURE;
    }

    status =
        Lip6NetLinkParseInfo (Lip6NetIfAddFilter, &NlSock, INET_AFI_IPV6, NULL);

    /* IPv6 Address lookup */
    if (Lip6NetRequest (&NlSock, AF_INET6, RTM_GETADDR) < 0)
    {
        Lip6NetClose (&NlSock);
        return NETIPV6_FAILURE;
    }

    status =
        Lip6NetLinkParseInfo (Lip6NetIfAddFilter, &NlSock, INET_AFI_IPV6, NULL);

    return status;
}

/*******************************************************************************
 * Function Name   : Lip6NetSetBlock
 * Description     : Function to Set netlink socket channel as blocking
 * Global Varibles :
 * Inputs          : pNlSock   - Socket Info 
 *                   *pi4Flags - Flags
 * Output          : None.
 * Returns         : NETIPV6_SUCCESS.
 *                   NETIPV6_FAILURE.
 ******************************************************************************/
INT4
Lip6NetSetBlock (tNlSock * pNlSock, INT4 *pi4Flags)
{
    if ((*pi4Flags = fcntl (pNlSock->i4Sock, F_GETFL, 0)) < 0)
    {
        perror ("Netlink: Cannot F_GETFL socket");

        return NETIPV6_FAILURE;
    }
    *pi4Flags &= ~O_NONBLOCK;
    if (fcntl (pNlSock->i4Sock, F_SETFL, *pi4Flags) < 0)
    {
        perror ("Netlink: Cannot F_SETFL socket ");

        return NETIPV6_FAILURE;
    }

    return NETIPV6_SUCCESS;
}

/*******************************************************************************
 * Function Name   : Lip6AddAttr32
 * Description     : Function to iproute2 utility function
 * Global Varibles :
 * Inputs          : pNlHdr      - Pointer to Netlink Header
 *                   i4MaxLen    - Max Len
 *                   i4Type      - Type
 *                   u4Data      - Data 
 * Output          : None.
 * Returns         : NETIPV6_SUCCESS.
 *                   NETIPV6_FAILURE.
 ******************************************************************************/
INT4
Lip6AddAttr32 (struct nlmsghdr *pNlHdr, INT4 i4Maxlen, INT4 i4Type,
               UINT4 u4data)
{
    struct rtattr      *pRta = NULL;
    INT4                i4Len = RTA_LENGTH (4);

    if (((INT4) NLMSG_ALIGN (pNlHdr->nlmsg_len)) + i4Len > i4Maxlen)
    {
        return NETIPV6_FAILURE;
    }

    pRta = (struct rtattr *)
        (((CHR1 *) pNlHdr) + NLMSG_ALIGN (pNlHdr->nlmsg_len));

    pRta->rta_type = i4Type;
    pRta->rta_len = i4Len;

    MEMCPY (RTA_DATA (pRta), &u4data, sizeof (UINT4));

    pNlHdr->nlmsg_len = NLMSG_ALIGN (pNlHdr->nlmsg_len) + i4Len;

    return NETIPV6_SUCCESS;
}

/*******************************************************************************
 * Function Name   : Lip6NetLinkSetVmacAddr
 * Description     : Function to set the VMAC address on VIF(Link layer handling)
 * Global Varibles :
 * Inputs          : pVrrpNwIntf    - Pointer to Vrrp Network Interface Info 
 * Output          : None.
 * Returns         : NETIPV6_SUCCESS.
 *                   NETIPV6_FAILURE.
 ******************************************************************************/
INT4
Lip6NetLinkSetVmacAddr (tVrrpNwIntf * pVrrpNwIntf)
{
    tNtPktHdr           NlReq;
    UINT1               au1VMac[MAC_ADDR_LEN] =
        { 0x00, 0x00, 0x5e, 0x00, 0x02, 0x00 };

    MEMSET (&NlReq, 0, sizeof (NlReq));

    au1VMac[MAC_ADDR_LEN - 1] = (UINT1) (pVrrpNwIntf->u4VrId);

    NlReq.n.nlmsg_len = NLMSG_LENGTH (sizeof (struct ifinfomsg));
    NlReq.n.nlmsg_flags = NLM_F_REQUEST;
    NlReq.n.nlmsg_type = RTM_NEWLINK;
    NlReq.ifi.ifi_family = AF_INET6;
    NlReq.ifi.ifi_index = pVrrpNwIntf->u4LnxIpPortNum;

    Lip6NetLinkAddAddrAttr (&NlReq.n, sizeof (NlReq), IFLA_ADDRESS,
                            au1VMac, MAC_ADDR_LEN);
#if defined(VRF_WANTED) && defined(LINUX_310_WANTED)
    if (Lip6NetLinkTalk (&NlReq.n, &gLnxVrfNetlinkCmd[pVrrpNwIntf->u4VrId], INET_AFI_IPV6) < 0)
#else
    if (Lip6NetLinkTalk (&NlReq.n, &gNetlinkCmd, INET_AFI_IPV6) < 0)
#endif
    {
        return NETIPV6_FAILURE;
    }

    return NETIPV6_SUCCESS;
}

/*******************************************************************************
 * Function Name   : Lip6NetLinkSetMode
 * Description     : Function to Set the MAC mode
 * Global Varibles :
 * Inputs          : pVrrpNwIntf    - Pointer to Vrrp Network Interface Info 
 * Output          : None.
 * Returns         : NETIPV6_SUCCESS.
 *                   NETIPV6_FAILURE.
 ******************************************************************************/
INT4
Lip6NetLinkSetMode (tVrrpNwIntf * pVrrpNwIntf)
{
    tNtPktHdr           NlReq;
    struct rtattr      *pLinkinfo = NULL;
    struct rtattr      *pData = NULL;

    MEMSET (&NlReq, 0, sizeof (NlReq));

    NlReq.n.nlmsg_len = NLMSG_LENGTH (sizeof (struct ifinfomsg));
    NlReq.n.nlmsg_flags = NLM_F_REQUEST;
    NlReq.n.nlmsg_type = RTM_NEWLINK;
    NlReq.ifi.ifi_family = AF_INET6;
    NlReq.ifi.ifi_index = pVrrpNwIntf->u4LnxIpPortNum;

    pLinkinfo = NLMSG_TAIL (&NlReq.n);

    Lip6NetLinkAddAddrAttr (&NlReq.n, sizeof (NlReq), IFLA_LINKINFO, NULL, 0);
    Lip6NetLinkAddAddrAttr (&NlReq.n, sizeof (NlReq), IFLA_INFO_KIND,
                            ((VOID *) ll6_kind), STRLEN (ll6_kind));

    pData = NLMSG_TAIL (&NlReq.n);

    Lip6NetLinkAddAddrAttr (&NlReq.n, sizeof (NlReq), IFLA_INFO_DATA, NULL, 0);

    Lip6AddAttr32 (&NlReq.n, sizeof (NlReq), IFLA_MACVLAN_MODE,
                   MACVLAN_MODE_PRIVATE);

    pData->rta_len = (VOID *) NLMSG_TAIL (&NlReq.n) - (VOID *) pData;
    pLinkinfo->rta_len = (VOID *) NLMSG_TAIL (&NlReq.n) - (VOID *) pLinkinfo;
#if defined(VRF_WANTED) && defined(LINUX_310_WANTED)
    if (Lip6NetLinkTalk (&NlReq.n, &gLnxVrfNetlinkCmd[pVrrpNwIntf->u4VrId], INET_AFI_IPV6) < 0)
#else
    if (Lip6NetLinkTalk (&NlReq.n, &gNetlinkCmd, INET_AFI_IPV6) < 0)
#endif
    {
        return NETIPV6_FAILURE;
    }

    return NETIPV6_SUCCESS;
}

/*******************************************************************************
 * Function Name   : Lip6NetLinkUp
 * Description     : Function to make VIF up in kernel
 * Global Varibles :
 * Inputs          : pVrrpNwIntf    - Pointer to Vrrp Network Interface Info
 * Output          : None.
 * Returns         : NETIPV6_SUCCESS.
 *                   NETIPV6_FAILURE.
 ******************************************************************************/
INT4
Lip6NetLinkUp (tVrrpNwIntf * pVrrpNwIntf)
{
    tNtPktHdr           NlReq;

    MEMSET (&NlReq, 0, sizeof (NlReq));

    NlReq.n.nlmsg_len = NLMSG_LENGTH (sizeof (struct ifinfomsg));
    NlReq.n.nlmsg_flags = NLM_F_REQUEST;
    NlReq.n.nlmsg_type = RTM_NEWLINK;
    NlReq.ifi.ifi_family = AF_UNSPEC;
    NlReq.ifi.ifi_index = pVrrpNwIntf->u4LnxIpPortNum;
    NlReq.ifi.ifi_change |= IFF_UP;
    NlReq.ifi.ifi_flags |= IFF_UP;

#if defined(VRF_WANTED) && defined(LINUX_310_WANTED)
    if (Lip6NetLinkTalk (&NlReq.n, &gLnxVrfNetlinkCmd[pVrrpNwIntf->u4VrId], INET_AFI_IPV6) < 0)
#else
    if (Lip6NetLinkTalk (&NlReq.n, &gNetlinkCmd, INET_AFI_IPV6) < 0)
#endif
    {
        return NETIPV6_FAILURE;
    }
    return NETIPV6_SUCCESS;
}

/*******************************************************************************
 * Function Name   : Lip6NetLinkCreateVif
 * Description     : Function to Create VIF in Linux
 * Global Varibles :
 * Inputs          : pVrrpNwIntf    - Pointer to Vrrp Network Interface Info 
 * Output          : pu1DevName     - Device Name created for VRRP Interface.
 * Returns         : NETIPV6_SUCCESS.
 *                   NETIPV6_FAILURE.
 ******************************************************************************/
INT4
Lip6NetLinkCreateVif (UINT1 *pu1DevName, tVrrpNwIntf * pVrrpNwIntf)
{
    tNtPktHdr           NlReq;
    struct rtattr      *pLinkInfo = NULL;
    CHR1                ac1IfName[IP_PORT_NAME_LEN];
    UINT1               au1IfName[IP_PORT_NAME_LEN];

    MEMSET (&NlReq, 0, sizeof (NlReq));
    MEMSET (ac1IfName, 0, IP_PORT_NAME_LEN);
    MEMSET (au1IfName, 0, IP_PORT_NAME_LEN);

    CfaGetIfName (pVrrpNwIntf->u4IfIndex, au1IfName);

    SNPRINTF (ac1IfName, IP_PORT_NAME_LEN, "vrrp.%d.%d",
              pVrrpNwIntf->u4VrId, pVrrpNwIntf->u1AddrType);

    NlReq.n.nlmsg_len = NLMSG_LENGTH (sizeof (struct ifinfomsg));
    NlReq.n.nlmsg_flags = NLM_F_REQUEST | NLM_F_CREATE | NLM_F_EXCL;
    NlReq.n.nlmsg_type = RTM_NEWLINK;
    NlReq.ifi.ifi_family = AF_INET6;

    pLinkInfo = NLMSG_TAIL (&NlReq.n);

    Lip6NetLinkAddAddrAttr (&NlReq.n, sizeof (NlReq), IFLA_LINKINFO, NULL, 0);

    Lip6NetLinkAddAddrAttr (&NlReq.n, sizeof (NlReq), IFLA_INFO_KIND,
                            ((VOID *) ll6_kind), STRLEN (ll6_kind));

    pLinkInfo->rta_len = (VOID *) NLMSG_TAIL (&NlReq.n) - (VOID *) pLinkInfo;

    Lip6NetLinkAddAddrAttr (&NlReq.n, sizeof (NlReq), IFLA_LINK,
                            &pVrrpNwIntf->u4Port, sizeof (UINT4));

    Lip6NetLinkAddAddrAttr (&NlReq.n, sizeof (NlReq), IFLA_IFNAME, ac1IfName,
                            STRLEN (ac1IfName));

    MEMCPY (pu1DevName, ac1IfName, sizeof (ac1IfName));

#if defined(VRF_WANTED) && defined(LINUX_310_WANTED)
    if (Lip6NetLinkTalk (&NlReq.n, &gLnxVrfNetlinkCmd[pVrrpNwIntf->u4VrId], INET_AFI_IPV6) < 0)
#else
    if (Lip6NetLinkTalk (&NlReq.n, &gNetlinkCmd, INET_AFI_IPV6) < 0)
#endif
    {
        return NETIPV6_FAILURE;
    }

    return NETIPV6_SUCCESS;
}

/*******************************************************************************
 * Function Name   : Lip6NetLinkDelVmac
 * Description     : Function to unregister the VIF with ethernet
 * Global Varibles :
 * Inputs          : pVrrpNwIntf    - Pointer to Vrrp Network Interface Info 
 * Output          : None.
 * Returns         : NETIPV6_SUCCESS.
 *                   NETIPV6_FAILURE.
 ******************************************************************************/
INT4
Lip6NetLinkDelVmac (tVrrpNwIntf * pVrrpNwIntf)
{
    tNtPktHdr           NlReq;

    MEMSET (&NlReq, 0, sizeof (NlReq));

    NlReq.n.nlmsg_len = NLMSG_LENGTH (sizeof (struct ifinfomsg));
    NlReq.n.nlmsg_flags = NLM_F_REQUEST;
    NlReq.n.nlmsg_type = RTM_DELLINK;
    NlReq.ifi.ifi_family = AF_INET6;
    NlReq.ifi.ifi_index = pVrrpNwIntf->u4LnxIpPortNum;

#if defined(VRF_WANTED) && defined(LINUX_310_WANTED)
    if (Lip6NetLinkTalk (&NlReq.n, &gLnxVrfNetlinkCmd[pVrrpNwIntf->u4VrId], INET_AFI_IPV6) < 0)
#else
    if (Lip6NetLinkTalk (&NlReq.n, &gNetlinkCmd, INET_AFI_IPV6) < 0)
#endif
    {
        return NETIPV6_FAILURE;
    }

    return NETIPV6_SUCCESS;

}
#endif

/*******************************************************************************
 * Function Name   : Lip6VrrpVifConfigInLnx
 * Description     : Function to to add VRRP Virtual interface and MAC to Kernel 
 * Global Varibles :
 * Inputs          : pVrrpNwIntf    - Pointer to Vrrp Network Interface Info 
 * Output          : None.
 * Returns         : NETIPV6_SUCCESS.
 *                   NETIPV6_FAILURE.
 ******************************************************************************/
INT4
Lip6VrrpVifConfigInLnx (tVrrpNwIntf * pVrrpNwIntf)
{
#ifdef LINUX_310_WANTED
    tIp6Addr            Ip6Addr;
    tIp6Addr            SrcIp;
    tIp6Addr            TmpAddr;
    CHR1                ac1Command[LIP6_LINE_LEN];
    UINT1               au1IfName[IP_PORT_NAME_LEN];
    UINT1               au1DevName[IP_PORT_NAME_LEN];
    INT4                i4VifPort = 0;
    INT1                i1Type = 0;

#if defined (VRF_WANTED)
    tLnxVrfIfInfo *pLnxVrfIfInfo = NULL;
    tLnxVrfInfo        *pLnxVrfInfo = NULL;
    UINT4               u4ContextId =VCM_DEFAULT_CONTEXT;
#endif

    MEMSET (au1IfName, 0, IP_PORT_NAME_LEN);
    MEMSET (ac1Command, 0, LIP6_LINE_LEN);
    MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));
    MEMSET (&SrcIp, 0, sizeof (tIp6Addr));
    MEMSET (&TmpAddr, 0, sizeof (tIp6Addr));

    if (Lip6NetLinkCreateVif (au1DevName, pVrrpNwIntf) != NETIPV6_SUCCESS)
    {
        return NETIPV6_FAILURE;
    }

    SNPRINTF ((CHR1 *) au1IfName, sizeof (au1IfName), "%s", au1DevName);

    if (Lip6KernIpGetInterface (au1IfName, &i4VifPort) != OSIX_SUCCESS)
    {
        return NETIPV6_FAILURE;
    }

    pVrrpNwIntf->u4LnxIpPortNum = i4VifPort;

    Lip6NetLinkSetVmacAddr (pVrrpNwIntf);

    Lip6NetLinkUp (pVrrpNwIntf);

    MEMCPY (&Ip6Addr.u1_addr, pVrrpNwIntf->VirtualIp.au1Addr,
            IPVX_IPV6_ADDR_LEN);

    if (Lip6KernUpdateIpv6Addr ((INT4) pVrrpNwIntf->u4LnxIpPortNum, &Ip6Addr,
                                IP6_ADDR_LL_PREFIX_LEN,
                                OSIX_TRUE) == OSIX_FAILURE)
    {
        return NETIPV6_FAILURE;
    }

    CfaGetIfName (pVrrpNwIntf->u4IfIndex, au1IfName);

    if (pVrrpNwIntf->b1IsIpvXOwner == TRUE)
    {
        SNPRINTF (ac1Command, sizeof (ac1Command),
                  "%s %s %s %s %s %s",
                  LIP6_IPTBL_ADD, LIP6_IPTBL_PROT_ICMP6,
                  LIP6_IPTBL_ICMP_TYPE_NS, LIP6_IPTBL_INTF, au1IfName,
                  LIP6_IPTBL_DROP);
        system (ac1Command);

        i1Type = Ip6AddrType (&Ip6Addr);

        if (i1Type == ADDR6_UNICAST)
        {
            MEMCPY (&Ip6Addr.u1_addr, pVrrpNwIntf->IntfAddr.au1Addr,
                    IPVX_IPV6_ADDR_LEN);

            if (Lip6KernUpdateIpv6Addr
                ((INT4) pVrrpNwIntf->u4LnxIpPortNum, &Ip6Addr,
                 IP6_ADDR_LL_PREFIX_LEN, OSIX_TRUE) == OSIX_FAILURE)
            {
                return NETIPV6_FAILURE;
            }

            MEMSET (ac1Command, 0, LIP6_LINE_LEN);

            SNPRINTF (ac1Command, sizeof (ac1Command),
                      "%s %s %s %s %s %s %s %s",
                      LIP6_IPTBL_ADD, LIP6_IPTBL_PROT_ICMP6,
                      LIP6_IPTBL_ICMP_TYPE_NS, LIP6_IPTBL_INTF, au1DevName,
                      LIP6_IPTBL_DEST, Ip6PrintAddr (&Ip6Addr),
                      LIP6_IPTBL_DROP);
            system (ac1Command);
        }
    }
    else
    {
        /* IP6 Address contain Virtual Ip. Do not accept NS coming for
         * DAD resolution (Dest: Virtual IP, Src: Unspecified */
        TmpAddr.u1_addr[0] = IP6_UINT1_ALL_ONE;
        TmpAddr.u1_addr[1] = 0x02;
        TmpAddr.u1_addr[11] = 0x01;
        TmpAddr.u1_addr[12] = IP6_UINT1_ALL_ONE;
        TmpAddr.u1_addr[13] = Ip6Addr.u1_addr[13];
        TmpAddr.u1_addr[14] = Ip6Addr.u1_addr[14];
        TmpAddr.u1_addr[15] = Ip6Addr.u1_addr[15];

        SNPRINTF (ac1Command, sizeof (ac1Command),
                  "%s %s %s %s %s %s %s %s",
                  LIP6_IPTBL_ADD, LIP6_IPTBL_PROT_ICMP6,
                  LIP6_IPTBL_ICMP_TYPE_NS,
                  LIP6_IPTBL_DEST, Ip6PrintAddr (&TmpAddr),
                  LIP6_IPTBL_SRC, Ip6PrintAddr (&SrcIp), LIP6_IPTBL_DROP);
        system (ac1Command);

        MEMSET (ac1Command, 0, LIP6_LINE_LEN);
#if defined (VRF_WANTED) 
        pLnxVrfIfInfo = LnxVrfIfInfoGet(au1DevName);
        if(pLnxVrfIfInfo != NULL)
        {
            u4ContextId = pLnxVrfIfInfo->u4ContextId;
        }
        if(u4ContextId != VCM_DEFAULT_CONTEXT)
        {
            pLnxVrfInfo = LnxVrfInfoGet (u4ContextId);
            if (pLnxVrfInfo == NULL)
            {
                return OSIX_FAILURE;
            }
        }

        if(Ip6UtilChangeCxt (u4ContextId, LNX_VRF_NON_DEFAULT_NS, 
                             pLnxVrfInfo->au1NameSpace) == (UINT4)NETIPV6_FAILURE)
        {
            return OSIX_FAILURE;
        }
#endif
        /* On the virtual interface disable DAD and DAD attempts. */
        SNPRINTF (ac1Command, sizeof (ac1Command),
                  "echo 0 > %s/%s/%s",
                  LIP6_PROC_CONF, au1DevName, LIP6_PROC_DADENABLE);
        system (ac1Command);

            MEMSET (ac1Command, 0, LIP6_LINE_LEN);

            SNPRINTF (ac1Command, sizeof (ac1Command),
                    "echo 0 > %s/%s/%s",
                    LIP6_PROC_CONF, au1DevName, LIP6_PROC_DADATTEMPT);
            system (ac1Command);

#if defined (VRF_WANTED) 
        if(Ip6UtilChangeCxt (u4ContextId, LNX_VRF_DEFAULT_NS,
                             (UINT1 *)LNX_VRF_DEFAULT_NS_PID) == (UINT4)NETIPV6_FAILURE)
        {
            return OSIX_FAILURE;
        }
#endif

        MEMCPY (&Ip6Addr.u1_addr, pVrrpNwIntf->IntfAddr.au1Addr,
                IPVX_IPV6_ADDR_LEN);

        if (Lip6KernUpdateIpv6Addr
            ((INT4) pVrrpNwIntf->u4LnxIpPortNum, &Ip6Addr,
             IP6_ADDR_LL_PREFIX_LEN, OSIX_TRUE) == OSIX_FAILURE)
        {
            return NETIPV6_FAILURE;
        }

        MEMSET (ac1Command, 0, LIP6_LINE_LEN);

        SNPRINTF (ac1Command, sizeof (ac1Command),
                  "%s %s %s %s %s %s %s %s",
                  LIP6_IPTBL_ADD, LIP6_IPTBL_PROT_ICMP6,
                  LIP6_IPTBL_ICMP_TYPE_NS, LIP6_IPTBL_INTF, au1DevName,
                  LIP6_IPTBL_DEST, Ip6PrintAddr (&Ip6Addr), LIP6_IPTBL_DROP);
        system (ac1Command);
    }

    if ((pVrrpNwIntf->b1IsIpvXOwner == FALSE) &&
        (pVrrpNwIntf->u1AcceptMode == FALSE))
    {
        Lip6VrrpAddDropFilter (pVrrpNwIntf->VirtualIp);
    }

    Lip6NetLinkSetMode (pVrrpNwIntf);
#else
    UNUSED_PARAM (pVrrpNwIntf);
#endif

    return NETIPV6_SUCCESS;
}

/*******************************************************************************
 * Function Name   : Lip6VrrpVifDeleteInLnx
 * Description     : Function to remove VRRP Virtual Interface from kernel 
 * Global Varibles :
 * Inputs          : pVrrpNwIntf    - Pointer to Vrrp Network Interface Info 
 * Output          : None.
 * Returns         : NETIPV6_SUCCESS.
 *                   NETIPV6_FAILURE.
 ******************************************************************************/
INT4
Lip6VrrpVifDeleteInLnx (tVrrpNwIntf * pVrrpNwIntf)
{
#ifdef LINUX_310_WANTED
    CHR1                ac1Command[LIP6_LINE_LEN];
    UINT1               au1IfName[IP_PORT_NAME_LEN];
    tIp6Addr            Ip6Addr;
    tIp6Addr            SrcIp;
    tIp6Addr            TmpAddr;
    INT1                i1Type = 0;

    MEMSET (au1IfName, 0, IP_PORT_NAME_LEN);
    MEMSET (ac1Command, 0, LIP6_LINE_LEN);
    MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));
    MEMSET (&SrcIp, 0, sizeof (tIp6Addr));
    MEMSET (&TmpAddr, 0, sizeof (tIp6Addr));

    if ((pVrrpNwIntf->b1IsIpvXOwner == FALSE) &&
        (pVrrpNwIntf->u1AcceptMode == FALSE))
    {
        Lip6VrrpDelDropFilter (pVrrpNwIntf->VirtualIp);
    }

    CfaGetIfName (pVrrpNwIntf->u4IfIndex, au1IfName);

    if (pVrrpNwIntf->b1IsIpvXOwner == TRUE)
    {
        SNPRINTF (ac1Command, sizeof (ac1Command),
                  "%s %s %s %s %s %s",
                  LIP6_IPTBL_DEL, LIP6_IPTBL_PROT_ICMP6,
                  LIP6_IPTBL_ICMP_TYPE_NS, LIP6_IPTBL_INTF, au1IfName,
                  LIP6_IPTBL_DROP);
        system (ac1Command);

        MEMCPY (&Ip6Addr.u1_addr, pVrrpNwIntf->VirtualIp.au1Addr,
                IPVX_IPV6_ADDR_LEN);

        i1Type = Ip6AddrType (&Ip6Addr);

        if (i1Type == ADDR6_UNICAST)
        {
            MEMCPY (&Ip6Addr.u1_addr, pVrrpNwIntf->IntfAddr.au1Addr,
                    IPVX_IPV6_ADDR_LEN);

            MEMSET (ac1Command, 0, LIP6_LINE_LEN);

            SNPRINTF (ac1Command, sizeof (ac1Command),
                      "%s %s %s %s vrrp.%d.%d %s %s %s",
                      LIP6_IPTBL_DEL, LIP6_IPTBL_PROT_ICMP6,
                      LIP6_IPTBL_ICMP_TYPE_NS, LIP6_IPTBL_INTF,
                      pVrrpNwIntf->u4VrId, pVrrpNwIntf->u1AddrType,
                      LIP6_IPTBL_DEST, Ip6PrintAddr (&Ip6Addr),
                      LIP6_IPTBL_DROP);
            system (ac1Command);
        }
    }
    else
    {
        MEMCPY (&Ip6Addr.u1_addr, pVrrpNwIntf->IntfAddr.au1Addr,
                IPVX_IPV6_ADDR_LEN);

        SNPRINTF (ac1Command, sizeof (ac1Command),
                  "%s %s %s %s vrrp.%d.%d %s %s %s",
                  LIP6_IPTBL_DEL, LIP6_IPTBL_PROT_ICMP6,
                  LIP6_IPTBL_ICMP_TYPE_NS, LIP6_IPTBL_INTF,
                  pVrrpNwIntf->u4VrId, pVrrpNwIntf->u1AddrType,
                  LIP6_IPTBL_DEST, Ip6PrintAddr (&Ip6Addr), LIP6_IPTBL_DROP);
        system (ac1Command);

        MEMCPY (&Ip6Addr.u1_addr, pVrrpNwIntf->VirtualIp.au1Addr,
                IPVX_IPV6_ADDR_LEN);

        TmpAddr.u1_addr[0] = IP6_UINT1_ALL_ONE;
        TmpAddr.u1_addr[1] = 0x02;
        TmpAddr.u1_addr[11] = 0x01;
        TmpAddr.u1_addr[12] = IP6_UINT1_ALL_ONE;
        TmpAddr.u1_addr[13] = Ip6Addr.u1_addr[13];
        TmpAddr.u1_addr[14] = Ip6Addr.u1_addr[14];
        TmpAddr.u1_addr[15] = Ip6Addr.u1_addr[15];

        MEMSET (ac1Command, 0, LIP6_LINE_LEN);

        SNPRINTF (ac1Command, sizeof (ac1Command),
                  "%s %s %s %s %s %s %s %s",
                  LIP6_IPTBL_DEL, LIP6_IPTBL_PROT_ICMP6,
                  LIP6_IPTBL_ICMP_TYPE_NS,
                  LIP6_IPTBL_DEST, Ip6PrintAddr (&TmpAddr),
                  LIP6_IPTBL_SRC, Ip6PrintAddr (&SrcIp), LIP6_IPTBL_DROP);
        system (ac1Command);
    }

    if (Lip6NetLinkDelVmac (pVrrpNwIntf) != NETIPV6_SUCCESS)
    {
        return NETIPV6_FAILURE;
    }
#else
    UNUSED_PARAM (pVrrpNwIntf);
#endif
    return NETIPV6_SUCCESS;
}

/*******************************************************************************
 * Function Name   : Lip6VrrpAddDropFilter
 * Description     : This function adds a filter to drop IP packets for the 
 *                   IP mentioned.
 * Global Varibles :
 * Inputs          : IpAddr   - IP Address
 * Output          : None.
 * Returns         : None.
 ******************************************************************************/
VOID
Lip6VrrpAddDropFilter (tIPvXAddr IpAddr)
{
#ifdef LINUX_310_WANTED
    tIp6Addr            Ip6Addr;
    CHR1                ac1Command[LIP6_LINE_LEN];

    MEMSET (ac1Command, 0, LIP6_LINE_LEN);
    MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));

    MEMCPY (&Ip6Addr.u1_addr, IpAddr.au1Addr, IPVX_IPV6_ADDR_LEN);

    SNPRINTF (ac1Command, sizeof (ac1Command), "%s %s %s %s",
              LIP6_IPTBL_ADD, LIP6_IPTBL_DEST,
              Ip6PrintAddr (&Ip6Addr), LIP6_IPTBL_DROP);
    system (ac1Command);
#else
    UNUSED_PARAM (IpAddr);
#endif

    return;
}

/*******************************************************************************
 * Function Name   : Lip6VrrpDelDropFilter
 * Description     : This function deletes a filter to drop IP packets for the 
 *                   IP mentioned.
 * Global Varibles :
 * Inputs          : IpAddr   - IP Address
 * Output          : None.
 * Returns         : None.
 ******************************************************************************/
VOID
Lip6VrrpDelDropFilter (tIPvXAddr IpAddr)
{
#ifdef LINUX_310_WANTED
    tIp6Addr            Ip6Addr;
    CHR1                ac1Command[LIP6_LINE_LEN];

    MEMSET (ac1Command, 0, LIP6_LINE_LEN);
    MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));

    MEMCPY (&Ip6Addr.u1_addr, IpAddr.au1Addr, IPVX_IPV6_ADDR_LEN);

    SNPRINTF (ac1Command, sizeof (ac1Command), "%s %s %s %s",
              LIP6_IPTBL_DEL, LIP6_IPTBL_DEST,
              Ip6PrintAddr (&Ip6Addr), LIP6_IPTBL_DROP);
    system (ac1Command);
#else
    UNUSED_PARAM (IpAddr);
#endif

    return;
}

INT4
Lip6NetlinkParseAddrsFromLnx (UINT4 u4IfIndex)
{
    struct
    {
        struct nlmsghdr     NlHdr;
        struct ifaddrmsg    IfMsg;
    } req;

    struct in6_addr    *pIp6Addr;
    struct nlmsghdr    *nlmp;
    struct ifaddrmsg   *rtmp;
    struct rtattr      *rtatp;
    tIp6Addr            Ip6Addr;
    INT4                i4RtLen;
    UINT4               u4Status;
    INT4                i4PrefixLen;
    UINT4               u4TempIfIndex;
    INT4                fd = -1; /*socket (PF_NETLINK, SOCK_DGRAM, NETLINK_ROUTE);*/
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    tLnxVrfEventInfo    LnxVrfEventInfo;
    tCfaIfInfo          CfaIfInfo;
    UINT4               u4ContextId = VCM_DEFAULT_CONTEXT;
    UINT4               u4CfaIfIndex = CFA_INVALID_INDEX;
#endif

#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    if (NetIpv4GetCfaIfIndexFromPort (u4IfIndex,
                                      &u4CfaIfIndex) == NETIPV4_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (CfaGetIfInfo (u4CfaIfIndex, &CfaIfInfo) == CFA_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if(VcmGetContextIdFromCfaIfIndex(u4CfaIfIndex, &u4ContextId) == VCM_FAILURE)
    {
        return OSIX_FAILURE;
    }

    LnxVrfEventInfo.u4ContextId = u4ContextId;
    LnxVrfEventInfo.i4Sockdomain = PF_NETLINK;
    LnxVrfEventInfo.i4SockType = SOCK_DGRAM;
    LnxVrfEventInfo.i4SockProto = NETLINK_ROUTE;
    LnxVrfEventInfo.u4IfIndex = u4CfaIfIndex;
    LnxVrfEventInfo.u1MsgType = LNX_VRF_OPEN_SOCKET;
    LnxVrfSockLock();
    LnxVrfEventHandling(&LnxVrfEventInfo,&fd);
    LnxVrfSockUnLock();
#else
    fd = socket (PF_NETLINK, SOCK_DGRAM, NETLINK_ROUTE);
#endif

    if (fd < 0)
    {
        return OSIX_FAILURE;
    }
    memset (&req, 0, sizeof (req));
    req.NlHdr.nlmsg_len = NLMSG_LENGTH (sizeof (struct ifaddrmsg));
    req.NlHdr.nlmsg_flags = NLM_F_REQUEST | NLM_F_ROOT;
    req.NlHdr.nlmsg_type = RTM_GETADDR;
    req.IfMsg.ifa_family = AF_INET6;

    /* Time to send and recv the message from kernel */

    u4Status = send (fd, &req, req.NlHdr.nlmsg_len, 0);
    if ((INT4) u4Status < 0)
    {
        perror ("send");
        return 1;
    }
    u4Status = recv (fd, gbuf, sizeof (gbuf), 0);
    if ((INT4) u4Status < 0)
    {
        perror ("recv");
        return 1;
    }

    /* Typically the message is stored in buf, so we need to parse the message to *
     * get the required data for our display. */
    for (nlmp = (struct nlmsghdr *) ((void *) gbuf); u4Status > sizeof (*nlmp);)
    {
        int                 len = nlmp->nlmsg_len;
        int                 req_len = len - sizeof (*nlmp);

        if (req_len < 0 || len > (int) u4Status)
        {
            return -1;
        }

        if (!NLMSG_OK (nlmp, u4Status))
        {
            return 1;
        }

        rtmp = (struct ifaddrmsg *) NLMSG_DATA (nlmp);
        rtatp =
            ((struct rtattr *) ((void *) (((char *) (rtmp))) +
                                NLMSG_ALIGN (sizeof (struct ifaddrmsg))));
        /* rtatp = (struct rtattr *) IFA_RTA (rtmp); */
        /* Start displaying the index of the interface */
        NetIpv4GetCfaIfIndexFromPort ((UINT4) rtmp->ifa_index, &u4TempIfIndex);
        if (u4IfIndex == u4TempIfIndex)
        {
            i4RtLen = IFA_PAYLOAD (nlmp);
            for (; RTA_OK (rtatp, i4RtLen);
                 rtatp =
                 (((i4RtLen) -=
                   RTA_ALIGN ((rtatp)->rta_len),
                   (struct rtattr *) ((VOID *) (((char *) (rtatp))) +
                                      RTA_ALIGN ((rtatp)->rta_len)))))
            {
                /* NOTE: All the commented code below can be used as it is for ipv4 table */
                if (rtatp->rta_type == IFA_ADDRESS)
                {
                    pIp6Addr = (struct in6_addr *) RTA_DATA (rtatp);
                    i4PrefixLen = (INT4) rtmp->ifa_prefixlen;
                    MEMCPY (&Ip6Addr, (pIp6Addr->s6_addr16), sizeof (tIp6Addr));
                    if (!IS_ADDR_LLOCAL (Ip6Addr))
                    {
                        Ip6SetIfAddr (u4IfIndex, Ip6Addr, i4PrefixLen,
                                      ADDR6_UNICAST);
                    }
                }
            }
        }
        u4Status -= NLMSG_ALIGN (len);
        nlmp =
            (struct nlmsghdr *) ((void *) ((char *) nlmp + NLMSG_ALIGN (len)));

    }
    return 0;
}
#endif /* _LIP6NETL_C_ */
/* END OF FILE */
