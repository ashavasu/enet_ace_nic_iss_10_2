/*****************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: lip6ra.c,v 1.14 2016/05/19 10:49:42 siva Exp $
 *
 * Description: Routines that configure radvd daemon
 ****************************************************************************/

#ifndef _LIP6RA_C
#define _LIP6RA_C

#include "lip6inc.h"

PRIVATE INT4        Lip6RAWriteIfConfig (tLip6If * pIf6Entry, INT4 i4Fd);
PRIVATE INT4        Lip6RAWritePrefixConfig (tTMO_SLL * pPrefixList, INT4 i4Fd);

#if defined(VRF_WANTED) && defined(LINUX_310_WANTED)
PRIVATE VOID        Lip6TruncateRaConf(UINT4 u4ContextId);
UINT1               gaRadConfFile[LIP6_RADVD_FILE_LEN];
extern UINT1        gau1RadvdStart[SYS_DEF_MAX_NUM_CONTEXTS];
#else
extern UINT1        gu1RadvdStart;
#endif

/****************************************************************************
 * Function    : Lip6RAInit
 * Description : First truncate radvd.conf file and write afresh
 * Input       : None
 * Output      : None
 * Returns     : OSIX_SUCCESS/OSIX_FAILURE
 ***************************************************************************/
PUBLIC INT4
Lip6RAInit (UINT4 u4ContextId)
{
    INT4                i4Fd = 0;
#if defined(VRF_WANTED) && defined(LINUX_310_WANTED)
    if(u4ContextId != VCM_DEFAULT_CONTEXT)
    {
       SPRINTF((CHR1 *)gaRadConfFile, "/etc/radvd_ns%d.conf", u4ContextId);
    }
    else
    {
       SPRINTF((CHR1 *)gaRadConfFile, (const CHR1 *)LIP6_RADVD_CONF_FILE);
    }
    i4Fd = FileOpen (gaRadConfFile, OSIX_FILE_TR| OSIX_FILE_CR);
#else
    i4Fd = FileOpen (LIP6_RADVD_CONF_FILE, OSIX_FILE_TR);
    UNUSED_PARAM(u4ContextId);
#endif

    if (i4Fd < 0)
    {
        LIP6_TRC_ARG (LIP6_MOD_TRC, ALL_FAILURE_TRC,
                      "FileOpen for TRUNCATE failed for RADVD CONF File\r\n");
        return OSIX_FAILURE;
    }
    FileClose (i4Fd);

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    : Lip6RAConfig
 * Description : Configure radvd.conf for all interfaces and restart radvd
 * Input       : None
 * Output      : None
 * Returns     : OSIX_SUCCESS/OSIX_FAILURE
 ***************************************************************************/
PUBLIC INT4
Lip6RAConfig (UINT4 u4ContextId)
{
    tLip6If            *pIf6Entry = NULL;
    INT4                i4Fd = 0;
    UINT4               u4Loop = 0;
    UINT4               u4FwdFlag = LIP6_FORW_ENABLE;
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    tLnxVrfInfo        *pLnxVrfInfo = NULL;
#endif
    if (u4ContextId == VCM_DEFAULT_CONTEXT)
    {
	    u4FwdFlag = (UINT4)gIp6GblInfo.i4Ip6Status;
    }
    else
    {
	    if (gIp6GblInfo.apIp6Cxt[u4ContextId] != NULL )
		    u4FwdFlag = (gIp6GblInfo.apIp6Cxt[u4ContextId])->u4ForwFlag;
    }

#if defined(VRF_WANTED) && defined(LINUX_310_WANTED)
    gau1RadvdStart[u4ContextId] = OSIX_FALSE;
    if(u4ContextId != VCM_DEFAULT_CONTEXT)
    {
        pLnxVrfInfo = LnxVrfInfoGet (u4ContextId);
        if (pLnxVrfInfo == NULL)
        {
            return OSIX_FAILURE;
        }


        if (Ip6UtilChangeCxt (u4ContextId, LNX_VRF_NON_DEFAULT_NS,
                              pLnxVrfInfo->au1NameSpace) == (UINT4)NETIPV6_FAILURE)
        {
            return OSIX_FAILURE;
        }
    }
    if (Lip6RAInit (u4ContextId) != OSIX_SUCCESS)
    {
        if(Ip6UtilChangeCxt (u4ContextId, LNX_VRF_DEFAULT_NS,
                             (UINT1 *)LNX_VRF_DEFAULT_NS_PID) == (UINT4)NETIPV6_FAILURE)
        {
            return OSIX_FAILURE;
        }
        return OSIX_FAILURE;
    }

    i4Fd = FileOpen (gaRadConfFile, OSIX_FILE_RW);
    if (i4Fd < 0)
    {
        LIP6_TRC_ARG (LIP6_MOD_TRC, ALL_FAILURE_TRC,
                      "FileOpen for WRITE failed for RADVD CONF File\r\n");
        if(Ip6UtilChangeCxt (u4ContextId, LNX_VRF_DEFAULT_NS,
                             (UINT1 *)LNX_VRF_DEFAULT_NS_PID) == (UINT4)NETIPV6_FAILURE)
        {
            return OSIX_FAILURE;
        }
        return OSIX_FAILURE;
    }

    for (u4Loop = 0; u4Loop < LIP6_MAX_INTERFACES; u4Loop++)
    {
        pIf6Entry = gIp6GblInfo.apIp6If[u4Loop];

        if (pIf6Entry == NULL)
        {
            continue;
        }

        if ((pIf6Entry->u1RAdvStatus == IP6_IF_ROUT_ADV_DISABLED) ||
            (pIf6Entry->u1OperStatus == NETIPV6_OPER_DOWN) ||
            (pIf6Entry->u4ContextId != u4ContextId))
        {
            continue;
        }

        if (Lip6RAWriteIfConfig (pIf6Entry, i4Fd) == OSIX_FAILURE)
        {
            FileClose (i4Fd);
            if(Ip6UtilChangeCxt (u4ContextId, LNX_VRF_DEFAULT_NS,
                                 (UINT1 *)LNX_VRF_DEFAULT_NS_PID) == (UINT4)NETIPV6_FAILURE)
            {
                return OSIX_FAILURE;
            }
            return OSIX_FAILURE;
        }

        gau1RadvdStart[u4ContextId] = OSIX_TRUE;
    }

    FileClose (i4Fd);

    if (gIp6GblInfo.aLip6TimerInfo[u4ContextId].u1TmrStatus == 1)
    {
        gIp6GblInfo.u4TimerFlag = OSIX_TRUE;
    }
    else
    {

        if (gau1RadvdRunning[u4ContextId] == OSIX_TRUE)
        {
            Lip6StopRadvd (u4ContextId);
            OsixTskDelay (100);

        }
        if ((gau1RadvdStart[u4ContextId] == OSIX_TRUE)
             && (u4FwdFlag == IP6_FORW_ENABLE))
        {
            Lip6StartRadvd (u4ContextId);
            gau1RadvdRunning[u4ContextId] = OSIX_TRUE;
            if (TmrStart (gIp6GblInfo.Ip6TimerListId,
                          &gIp6GblInfo.aLip6TimerInfo[u4ContextId].TimerBlk,
                          LIP6_5SECTMR__EVENT, 5, 0) == TMR_FAILURE)


            {
                if(Ip6UtilChangeCxt (u4ContextId, LNX_VRF_DEFAULT_NS,
                                     (UINT1 *)LNX_VRF_DEFAULT_NS_PID) == (UINT4)NETIPV6_FAILURE)
                {
                    return OSIX_FAILURE;
                }
                return (OSIX_FAILURE);
            }
            /* 1 - TMR_IS_RUNNING
             * 0 - TMR_IS_NOT_RUNNNG */

            gIp6GblInfo.aLip6TimerInfo[u4ContextId].u1TmrStatus = 1;
        }
        else
        {

            gau1RadvdRunning[u4ContextId] = OSIX_FALSE;
        }
    }
    if(Ip6UtilChangeCxt (u4ContextId, LNX_VRF_DEFAULT_NS,
                         (UINT1 *)LNX_VRF_DEFAULT_NS_PID) == (UINT4)NETIPV6_FAILURE)
    {
        return OSIX_FAILURE;
    }

#else
    gu1RadvdStart = OSIX_FALSE;
    if (Lip6RAInit (u4ContextId) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    i4Fd = FileOpen (LIP6_RADVD_CONF_FILE, OSIX_FILE_RW);
    if (i4Fd < 0)
    {
        LIP6_TRC_ARG (LIP6_MOD_TRC, ALL_FAILURE_TRC,
                      "FileOpen for WRITE failed for RADVD CONF File\r\n");
        return OSIX_FAILURE;
    }

    for (u4Loop = 0; u4Loop < LIP6_MAX_INTERFACES; u4Loop++)
    {
        pIf6Entry = gIp6GblInfo.apIp6If[u4Loop];

        if (pIf6Entry == NULL)
        {
            continue;
        }

        if ((pIf6Entry->u1RAdvStatus == IP6_IF_ROUT_ADV_DISABLED) ||
            (pIf6Entry->u1OperStatus == NETIPV6_OPER_DOWN) ||
            (pIf6Entry->u4ContextId != u4ContextId))
        {
            continue;
        }
        if (Lip6RAWriteIfConfig (pIf6Entry, i4Fd) == OSIX_FAILURE)
        {
            FileClose (i4Fd);
            return OSIX_FAILURE;
        }

        gu1RadvdStart = OSIX_TRUE;
    }

    FileClose (i4Fd);

    if (gIp6GblInfo.Lip6TimerInfo.u1TmrStatus == 1)
    {
        gIp6GblInfo.u4TimerFlag = OSIX_TRUE;
    }
    else
    {
        if (gu1RadvdRunning == OSIX_TRUE)
        {
            Lip6StopRadvd (u4ContextId);
            OsixTskDelay (100);

        }
        if ((gu1RadvdStart == OSIX_TRUE)
            && (gIp6GblInfo.i4Ip6Status == IP6_FORW_ENABLE))
        {
            Lip6StartRadvd (u4ContextId);
            gu1RadvdRunning = OSIX_TRUE;
            if (TmrStart (gIp6GblInfo.Ip6TimerListId,
                          &gIp6GblInfo.Lip6TimerInfo.TimerBlk,
                          LIP6_5SECTMR__EVENT, 5, 0) == TMR_FAILURE)


            {
                return (OSIX_FAILURE);
            }
            /* 1 - TMR_IS_RUNNING
             * 0 - TMR_IS_NOT_RUNNNG */
            gIp6GblInfo.Lip6TimerInfo.u1TmrStatus = 1;
        }
        else
        {
            gu1RadvdRunning = OSIX_FALSE;
        }
    }

#endif
    UNUSED_PARAM(u4FwdFlag);
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    : Lip6RAWriteIfConfig
 * Description : Write configuration for an interface for which RA is enabled
 *               to radvd.conf
 * Input       : pIf6Entry - Interface info Pointer
 *               i4Fd - File descriptor of radvd.conf
 * Output      : None
 * Returns     : OSIX_SUCCESS/OSIX_FAILURE
 ***************************************************************************/
PRIVATE INT4
Lip6RAWriteIfConfig (tLip6If * pIf6Entry, INT4 i4Fd)
{
    tIp6Addr            Ip6TempRDNSSAddr;
    CHR1                ac1LineBuf[LIP6_LINE_LEN];
    CHR1                ac1On[] = "on";
    CHR1                ac1Off[] = "off";
    CHR1               *pc1MFlag = &ac1Off[0];
    CHR1               *pc1OFlag = &ac1Off[0];
    UINT4               u4Len = 0;
    CHR1                ac1DNSStrt[] = "DNSSL";
    CHR1                ac1DNSMdl[] = "{";
    CHR1                ac1DNSEnd[] = "};";
    CHR1               *pc1DNSStrt = NULL;
    CHR1               *pc1DNSMdl = NULL;
    CHR1               *pc1DNSEnd = NULL;
    CHR1               advrouterpref[MAX_IP6_RA_PREFERENCE];

    MEMSET (advrouterpref,0,sizeof (advrouterpref));

    MEMSET (&Ip6TempRDNSSAddr, 0, sizeof (tIp6Addr));
    if (pIf6Entry->u1RAAdvFlags & IP6_IF_M_BIT_ADV)
    {
        pc1MFlag = &ac1On[0];
    }

    if (pIf6Entry->u1RAAdvFlags & IP6_IF_O_BIT_ADV)
    {
        pc1OFlag = &ac1On[0];
    }

    if (pIf6Entry->u1RAdvStatus == ACTIVE)
    {
        pc1DNSStrt = &ac1DNSStrt[0];
        pc1DNSMdl = &ac1DNSMdl[0];
        pc1DNSEnd = &ac1DNSEnd[0];
    }

    if (pIf6Entry->u2Preference == IP6_RA_ROUTE_PREF_LOW)
    {
        STRCPY(advrouterpref,"low");
    }
    else if(pIf6Entry->u2Preference == IP6_RA_ROUTE_PREF_HIGH)
    {

        STRCPY(advrouterpref,"high");
    }
    else
    {
        STRCPY(advrouterpref,"medium");
    }


    MEMSET (ac1LineBuf, 0, sizeof (ac1LineBuf));
    SNPRINTF (ac1LineBuf, LIP6_LINE_LEN,
              "interface %s\n"
              "{\n"
              "\tAdvSendAdvert on;\n"
              "\tMinRtrAdvInterval %u;\n"
              "\tMaxRtrAdvInterval %u;\n"
              "\tAdvDefaultLifetime %u;\n"
              "\tAdvManagedFlag %s;\n"
              "\tAdvOtherConfigFlag %s;\n"
              "\tAdvDefaultPreference %s;\n",
              pIf6Entry->au1IfName,
              pIf6Entry->u4MinRaTime,
              pIf6Entry->u4MaxRaTime, pIf6Entry->u4DefRaLifetime,
              pc1MFlag, pc1OFlag,advrouterpref);

    u4Len = STRLEN (ac1LineBuf);
    if ((FileWrite (i4Fd, ac1LineBuf, u4Len)) == 0)
    {
        return OSIX_FAILURE;
    }

    if (pIf6Entry->u4RAReachableTime != 0)
    {
        MEMSET (ac1LineBuf, 0, sizeof (ac1LineBuf));
        SNPRINTF (ac1LineBuf, LIP6_LINE_LEN,
                  "\tAdvReachableTime %u;\n", pIf6Entry->u4RAReachableTime);

        u4Len = STRLEN (ac1LineBuf);
        if ((FileWrite (i4Fd, ac1LineBuf, u4Len)) == 0)
        {
            return OSIX_FAILURE;
        }
    }
    if(pIf6Entry->u4RARetransTimer == 0)
    {
       MEMSET (ac1LineBuf, 0, sizeof (ac1LineBuf));
       SNPRINTF (ac1LineBuf, LIP6_LINE_LEN,
	         "\tAdvRetransTimer   0;\n"
		);

       u4Len = STRLEN (ac1LineBuf);
       if ((FileWrite (i4Fd, ac1LineBuf, u4Len)) == 0)
       {
	       return OSIX_FAILURE;
       }
    }
    else
    {
       MEMSET (ac1LineBuf, 0, sizeof (ac1LineBuf));
       SNPRINTF (ac1LineBuf, LIP6_LINE_LEN,
		 "\tAdvRetransTimer %u;\n", pIf6Entry->u4RARetransTimer);
       u4Len = STRLEN (ac1LineBuf);
       if ((FileWrite (i4Fd, ac1LineBuf, u4Len)) == 0)
       {
	       return OSIX_FAILURE;
       }
    }

    if (pIf6Entry->u4RaMtu == IP6_INTERFACE_LINK_MTU)
    {
        MEMSET (ac1LineBuf, 0, sizeof (ac1LineBuf));
        SNPRINTF (ac1LineBuf, LIP6_LINE_LEN,
                  "\tAdvLinkMTU %u;\n", pIf6Entry->u4Mtu);

        u4Len = STRLEN (ac1LineBuf);
        if ((FileWrite (i4Fd, ac1LineBuf, (UINT4) u4Len)) == 0)
        {
            return OSIX_FAILURE;
        }
    }
    else
    {
        MEMSET (ac1LineBuf, 0, sizeof (ac1LineBuf));
        SNPRINTF (ac1LineBuf, LIP6_LINE_LEN,
                "\tAdvLinkMTU %u;\n", pIf6Entry->u4RaMtu);

        u4Len = STRLEN (ac1LineBuf);
        if ((FileWrite (i4Fd, ac1LineBuf, u4Len)) == 0)
        {
            return OSIX_FAILURE;
        }
    }

    if (pIf6Entry->u1RALinkLocalStatus == IP6_RA_ADV_LINKLOCAL)
    {
        MEMSET (ac1LineBuf, 0, sizeof (ac1LineBuf));
        SNPRINTF (ac1LineBuf, LIP6_LINE_LEN, "\tAdvSourceLLAddress on;\n");
        u4Len = STRLEN (ac1LineBuf);

        if ((FileWrite (i4Fd, ac1LineBuf, u4Len)) == 0)
        {
            return OSIX_FAILURE;
        }
    }
    else if (pIf6Entry->u1RALinkLocalStatus == IP6_RA_NO_ADV_LINKLOCAL)
    {
        MEMSET (ac1LineBuf, 0, sizeof (ac1LineBuf));
        SNPRINTF (ac1LineBuf, LIP6_LINE_LEN, "\tAdvSourceLLAddress off;\n");
        u4Len = STRLEN (ac1LineBuf);

        if ((FileWrite (i4Fd, ac1LineBuf, u4Len)) == 0)
        {
            return OSIX_FAILURE;
        }
    }

    if (pIf6Entry->u1RAInterval == IP6_RA_ADV_INTERVAL)
    {
        MEMSET (ac1LineBuf, 0, sizeof (ac1LineBuf));
        SNPRINTF (ac1LineBuf, LIP6_LINE_LEN, "\tAdvIntervalOpt on;\n");
        u4Len = STRLEN (ac1LineBuf);

        if ((FileWrite (i4Fd, ac1LineBuf, u4Len)) == 0)
        {
            return OSIX_FAILURE;
        }
    }
    else if (pIf6Entry->u1RAInterval == IP6_RA_NO_ADV_INTERVAL)
    {
        MEMSET (ac1LineBuf, 0, sizeof (ac1LineBuf));
        SNPRINTF (ac1LineBuf, LIP6_LINE_LEN, "\tAdvIntervalOpt off;\n");
        u4Len = STRLEN (ac1LineBuf);

        if ((FileWrite (i4Fd, ac1LineBuf, u4Len)) == 0)
        {
            return OSIX_FAILURE;
        }
    }

    if (pIf6Entry->u4RACurHopLimit != IP6_IF_DEF_HOPLMT)
    {
        MEMSET (ac1LineBuf, 0, sizeof (ac1LineBuf));
        SNPRINTF (ac1LineBuf, LIP6_LINE_LEN,
                  "\tAdvCurHopLimit %u;\n", pIf6Entry->u4RACurHopLimit);

        u4Len = STRLEN (ac1LineBuf);
        if ((FileWrite (i4Fd, ac1LineBuf, u4Len)) == 0)
        {
            return OSIX_FAILURE;
        }
    }

    if (pIf6Entry->u1PrefAdv == IP6_IF_PREFIX_ADV_ENABLED)
    {
        /* Write Prefix configurations if enabled */
        if (Lip6RAWritePrefixConfig (&pIf6Entry->PrefixList, i4Fd) ==
            OSIX_FAILURE)
        {
            return OSIX_FAILURE;
        }
    }
    if (pIf6Entry->u1RAdvStatus == ACTIVE)
    {
	    if (STRLEN(pIf6Entry->au1DomainNameOne) != IP6_ZERO)
	    {
		    MEMSET (ac1LineBuf, 0, sizeof (ac1LineBuf));
                    if ( STRLEN(pIf6Entry->au1DomainNameOne) >= 20 )
                       {
                          pIf6Entry->au1DomainNameOne[20] = '\0';
                       }
		    SNPRINTF (ac1LineBuf, LIP6_LINE_LEN,
				    "\t %s %s", pc1DNSStrt, pIf6Entry->au1DomainNameOne);
		    u4Len = STRLEN (ac1LineBuf);
		    if ((FileWrite (i4Fd, ac1LineBuf, u4Len)) == 0)
		    {
			    return OSIX_FAILURE;
		    }   
		    MEMSET (ac1LineBuf, 0, sizeof (ac1LineBuf));
		    SNPRINTF (ac1LineBuf, LIP6_LINE_LEN, "\n\t\%s\n", pc1DNSMdl);
		    u4Len = STRLEN (ac1LineBuf);
		    if ((FileWrite (i4Fd, ac1LineBuf, u4Len)) == 0)
		    {
			    return OSIX_FAILURE;
		    }
		    if((pIf6Entry->u4DnsLifeTimeOne) == 0)
		    {
			    MEMSET (ac1LineBuf, 0, sizeof (ac1LineBuf));
			    SNPRINTF (ac1LineBuf, LIP6_LINE_LEN,
					    "\t\tAdvDNSSLifetime  0;\n\t\n"
				     );

			    u4Len = STRLEN (ac1LineBuf);
			    if ((FileWrite (i4Fd, ac1LineBuf, u4Len)) == 0)
			    {
				    return OSIX_FAILURE;
			    }
		    }
		    else
		    {
			    MEMSET (ac1LineBuf, 0, sizeof (ac1LineBuf));
			    SNPRINTF (ac1LineBuf, LIP6_LINE_LEN,
					    "\t\tAdvDNSSLLifetime %u;\n\t\n"
					    ,pIf6Entry->u4DnsLifeTimeOne);

			    u4Len = STRLEN (ac1LineBuf);
			    if ((FileWrite (i4Fd, ac1LineBuf, u4Len)) == 0)
			    {
				    return OSIX_FAILURE;
			    }
		    }
		    MEMSET (ac1LineBuf, 0, sizeof (ac1LineBuf));
		    SNPRINTF (ac1LineBuf, LIP6_LINE_LEN, "\t\%s\n", pc1DNSEnd);
		    u4Len = STRLEN (ac1LineBuf);
		    if ((FileWrite (i4Fd, ac1LineBuf, u4Len)) == 0)
		    {
			    return OSIX_FAILURE;
		    }
	    } 
	    if (STRLEN(pIf6Entry->au1DomainNameTwo) != IP6_ZERO)
	    {
		    MEMSET (ac1LineBuf, 0, sizeof (ac1LineBuf));
                    if ( STRLEN(pIf6Entry->au1DomainNameTwo) >= 20 )
                       {
                          pIf6Entry->au1DomainNameTwo[20] = '\0';
                       }
		    SNPRINTF (ac1LineBuf, LIP6_LINE_LEN,
				    "\t%s %s", pc1DNSStrt, pIf6Entry->au1DomainNameTwo);
		    u4Len = STRLEN (ac1LineBuf);
		    if ((FileWrite (i4Fd, ac1LineBuf, u4Len)) == 0)
		    {
			    return OSIX_FAILURE;
		    }

		    MEMSET (ac1LineBuf, 0, sizeof (ac1LineBuf));
		    SNPRINTF (ac1LineBuf, LIP6_LINE_LEN, "\n\t%s\n", pc1DNSMdl);
		    u4Len = STRLEN (ac1LineBuf);
		    if ((FileWrite (i4Fd, ac1LineBuf, u4Len)) == 0)
		    {
			    return OSIX_FAILURE;
		    }
		    if((pIf6Entry->u4DnsLifeTimeTwo) == 0)
		    {
			    MEMSET (ac1LineBuf, 0, sizeof (ac1LineBuf));

			    SNPRINTF (ac1LineBuf, LIP6_LINE_LEN,
					    "\t\tAdvDNSSLifetime  0;\n\t\n"
				     );
			    u4Len = STRLEN (ac1LineBuf);
			    if ((FileWrite (i4Fd, ac1LineBuf, u4Len)) == 0)
			    {
				    return OSIX_FAILURE;
			    }


		    }
		    else
		    {
			    MEMSET (ac1LineBuf, 0, sizeof (ac1LineBuf));
			    SNPRINTF (ac1LineBuf, LIP6_LINE_LEN,
					    "\t\tAdvDNSSLLifetime %u;\n\t\n"
					    ,pIf6Entry->u4DnsLifeTimeTwo);
			    u4Len = STRLEN (ac1LineBuf);
			    if ((FileWrite (i4Fd, ac1LineBuf, u4Len)) == 0)
			    {
				    return OSIX_FAILURE;
			    }


		    }
		    MEMSET (ac1LineBuf, 0, sizeof (ac1LineBuf));
		    SNPRINTF (ac1LineBuf, LIP6_LINE_LEN, "\t\%s\n", pc1DNSEnd);
		    u4Len = STRLEN (ac1LineBuf);
		    if ((FileWrite (i4Fd, ac1LineBuf, u4Len)) == 0)
		    {
			    return OSIX_FAILURE;
		    }
	    }
	    if ((STRLEN(pIf6Entry->au1DomainNameThree)) != IP6_ZERO)
	    {
		    MEMSET (ac1LineBuf, 0, sizeof (ac1LineBuf));
                    if ( STRLEN(pIf6Entry->au1DomainNameThree) >= 20 )
                       {
                          pIf6Entry->au1DomainNameThree[20] = '\0';
                       }
		    SNPRINTF (ac1LineBuf, LIP6_LINE_LEN,
				    "\t%s %s", pc1DNSStrt, pIf6Entry->au1DomainNameThree);
		    u4Len = STRLEN (ac1LineBuf);
		    if ((FileWrite (i4Fd, ac1LineBuf, u4Len)) == 0)
		    {
			    return OSIX_FAILURE;
		    }
		    MEMSET (ac1LineBuf, 0, sizeof (ac1LineBuf));
		    SNPRINTF (ac1LineBuf, LIP6_LINE_LEN, "\n\t%s\n", pc1DNSMdl);
		    u4Len = STRLEN (ac1LineBuf);
		    if ((FileWrite (i4Fd, ac1LineBuf, u4Len)) == 0)
		    {
			    return OSIX_FAILURE;
		    }
		    if((pIf6Entry->u4DnsLifeTimeThree) == 0)
		    {
			    MEMSET (ac1LineBuf, 0, sizeof (ac1LineBuf));
			    SNPRINTF (ac1LineBuf, LIP6_LINE_LEN,
					    "\t\tAdvDNSSLifetime  0;\n\t\n"
				     );

			    u4Len = STRLEN (ac1LineBuf);
			    if ((FileWrite (i4Fd, ac1LineBuf, u4Len)) == 0)
			    {
				    return OSIX_FAILURE;
			    }
		    }
		    else
		    {
			    MEMSET (ac1LineBuf, 0, sizeof (ac1LineBuf));
			    SNPRINTF (ac1LineBuf, LIP6_LINE_LEN,
					    "\t\tAdvDNSSLLifetime %u;\n\t\n"
					    ,pIf6Entry->u4DnsLifeTimeThree);


			    u4Len = STRLEN (ac1LineBuf);
			    if ((FileWrite (i4Fd, ac1LineBuf, u4Len)) == 0)
			    {
				    return OSIX_FAILURE;
			    }
		    }

		    MEMSET (ac1LineBuf, 0, sizeof (ac1LineBuf));
		    SNPRINTF (ac1LineBuf, LIP6_LINE_LEN, "\t\%s\n", pc1DNSEnd);
		    u4Len = STRLEN (ac1LineBuf);
		    if ((FileWrite (i4Fd, ac1LineBuf, u4Len)) == 0)
		    {
			    return OSIX_FAILURE;
		    }
	    }
    }
    if ((pIf6Entry->u1RAdvRDNSS == ACTIVE)
		    || (Ip6AddrCompare (Ip6TempRDNSSAddr, pIf6Entry->Ip6RdnssAddrOne) !=
            IP6_ZERO))
    {

	    if (Ip6AddrCompare (Ip6TempRDNSSAddr, pIf6Entry->Ip6RdnssAddrOne) !=
			    IP6_ZERO)
	    {
        MEMSET (ac1LineBuf, 0, sizeof (ac1LineBuf));
        SNPRINTF (ac1LineBuf, LIP6_LINE_LEN,
                  "\n\tRDNSS %s ", Ip6PrintNtop (&pIf6Entry->Ip6RdnssAddrOne));

        u4Len = STRLEN (ac1LineBuf);
        if ((FileWrite (i4Fd, ac1LineBuf, u4Len)) == 0)
        {
            return OSIX_FAILURE;
        }
		    MEMSET (ac1LineBuf, 0, sizeof (ac1LineBuf));
		    SNPRINTF (ac1LineBuf, LIP6_LINE_LEN, "\n\t{\n");
		    u4Len = STRLEN (ac1LineBuf);
		    if ((FileWrite (i4Fd, ac1LineBuf, u4Len)) == 0)
		    {
			    return OSIX_FAILURE;
		    }
		    if (pIf6Entry->u1RAdvRDNSSStatus == IP6_RA_NO_RDNSS_OPEN)
		    {
			    MEMSET (ac1LineBuf, 0, sizeof (ac1LineBuf));
			    SNPRINTF (ac1LineBuf, LIP6_LINE_LEN, "\t\tAdvRDNSSOpen off;\n");

			    u4Len = STRLEN (ac1LineBuf);
			    if ((FileWrite (i4Fd, ac1LineBuf, u4Len)) == 0)
			    {
				    return OSIX_FAILURE;
			    }
		    }
		    else
		    {
			    MEMSET (ac1LineBuf, 0, sizeof (ac1LineBuf));
			    SNPRINTF (ac1LineBuf, LIP6_LINE_LEN, "\t\tAdvRDNSSOpen on;\n");

			    u4Len = STRLEN (ac1LineBuf);
			    if ((FileWrite (i4Fd, ac1LineBuf, u4Len)) == 0)
			    {
				    return OSIX_FAILURE;
			    }
		    }
		    if (pIf6Entry->u4RDNSSPreference == 0)
		    {
			    MEMSET (ac1LineBuf, 0, sizeof (ac1LineBuf));
			    SNPRINTF (ac1LineBuf, LIP6_LINE_LEN,
					    "\t\tAdvRDNSSPreference %u;\n", IP6_IF_RA_RDNSS_DEF_PREF);
			    u4Len = STRLEN (ac1LineBuf);
			    if ((FileWrite (i4Fd, ac1LineBuf, u4Len)) == 0)
			    {
				    return OSIX_FAILURE;
			    }
		    }
		    else
		    {
			    MEMSET (ac1LineBuf, 0, sizeof (ac1LineBuf));
			    SNPRINTF (ac1LineBuf, LIP6_LINE_LEN,
					    "\t\tAdvRDNSSPreference %u;\n",
					    pIf6Entry->u4RDNSSPreference);
			    u4Len = STRLEN (ac1LineBuf);
			    if ((FileWrite (i4Fd, ac1LineBuf, u4Len)) == 0)
			    {
				    return OSIX_FAILURE;
			    }
		    }

		    if(pIf6Entry->u4RDNSSLifetimeOne == 0)
        {
            MEMSET (ac1LineBuf, 0, sizeof (ac1LineBuf));
            SNPRINTF (ac1LineBuf, LIP6_LINE_LEN,
					    "\t\tAdvRDNSSLifetime  0;\n\t};\n"
					     );

            u4Len = STRLEN (ac1LineBuf);
            if ((FileWrite (i4Fd, ac1LineBuf, u4Len)) == 0)
            {
                return OSIX_FAILURE;
            }
        }
		    else
		    {
			    MEMSET (ac1LineBuf, 0, sizeof (ac1LineBuf));
			    SNPRINTF (ac1LineBuf, LIP6_LINE_LEN,
					    "\t\tAdvRDNSSLifetime %u;\n\t};\n", pIf6Entry->u4RDNSSLifetimeOne);
			    u4Len = STRLEN (ac1LineBuf);
			    if ((FileWrite (i4Fd, ac1LineBuf, u4Len)) == 0)
			    {
				    return OSIX_FAILURE;
			    }
		    }
	    }
	    if (Ip6AddrCompare (Ip6TempRDNSSAddr, pIf6Entry->Ip6RdnssAddrTwo) !=
            IP6_ZERO)
        {
            MEMSET (ac1LineBuf, 0, sizeof (ac1LineBuf));
            SNPRINTF (ac1LineBuf, LIP6_LINE_LEN,
				    "\n\tRDNSS %s ", Ip6PrintNtop (&pIf6Entry->Ip6RdnssAddrTwo));


            u4Len = STRLEN (ac1LineBuf);
            if ((FileWrite (i4Fd, ac1LineBuf, u4Len)) == 0)
            {
                return OSIX_FAILURE;
            }
        MEMSET (ac1LineBuf, 0, sizeof (ac1LineBuf));
        SNPRINTF (ac1LineBuf, LIP6_LINE_LEN, "\n\t{\n");
        u4Len = STRLEN (ac1LineBuf);
        if ((FileWrite (i4Fd, ac1LineBuf, u4Len)) == 0)
        {
            return OSIX_FAILURE;
        }

        if (pIf6Entry->u1RAdvRDNSSStatus == IP6_RA_NO_RDNSS_OPEN)
        {
            MEMSET (ac1LineBuf, 0, sizeof (ac1LineBuf));
            SNPRINTF (ac1LineBuf, LIP6_LINE_LEN, "\t\tAdvRDNSSOpen off;\n");

            u4Len = STRLEN (ac1LineBuf);
            if ((FileWrite (i4Fd, ac1LineBuf, u4Len)) == 0)
            {
                return OSIX_FAILURE;
            }
        }
        else
        {
            MEMSET (ac1LineBuf, 0, sizeof (ac1LineBuf));
            SNPRINTF (ac1LineBuf, LIP6_LINE_LEN, "\t\tAdvRDNSSOpen on;\n");

            u4Len = STRLEN (ac1LineBuf);
            if ((FileWrite (i4Fd, ac1LineBuf, u4Len)) == 0)
            {
                return OSIX_FAILURE;
            }
        }
        if (pIf6Entry->u4RDNSSPreference == 0)
        {
            MEMSET (ac1LineBuf, 0, sizeof (ac1LineBuf));
            SNPRINTF (ac1LineBuf, LIP6_LINE_LEN,
                      "\t\tAdvRDNSSPreference %u;\n", IP6_IF_RA_RDNSS_DEF_PREF);
            u4Len = STRLEN (ac1LineBuf);
            if ((FileWrite (i4Fd, ac1LineBuf, u4Len)) == 0)
            {
                return OSIX_FAILURE;
            }
        }
        else
        {
            MEMSET (ac1LineBuf, 0, sizeof (ac1LineBuf));
            SNPRINTF (ac1LineBuf, LIP6_LINE_LEN,
                      "\t\tAdvRDNSSPreference %u;\n",
                      pIf6Entry->u4RDNSSPreference);
            u4Len = STRLEN (ac1LineBuf);
            if ((FileWrite (i4Fd, ac1LineBuf, u4Len)) == 0)
            {
                return OSIX_FAILURE;
            }
        }

		    if (pIf6Entry->u4RDNSSLifetimeTwo == 0)
        {
            MEMSET (ac1LineBuf, 0, sizeof (ac1LineBuf));
            SNPRINTF (ac1LineBuf, LIP6_LINE_LEN,
					    "\t\tAdvRDNSSLifetime 0;\n\t};\n"
			             );

            u4Len = STRLEN (ac1LineBuf);
            if ((FileWrite (i4Fd, ac1LineBuf, u4Len)) == 0)
            {
                return OSIX_FAILURE;
            }
        }
        else
        {
            MEMSET (ac1LineBuf, 0, sizeof (ac1LineBuf));
            SNPRINTF (ac1LineBuf, LIP6_LINE_LEN,
					    "\t\tAdvRDNSSLifetime %u;\n\t};\n", pIf6Entry->u4RDNSSLifetimeTwo);
            u4Len = STRLEN (ac1LineBuf);
            if ((FileWrite (i4Fd, ac1LineBuf, u4Len)) == 0)
            {
                return OSIX_FAILURE;
            }
        }
	    }
	    if (Ip6AddrCompare (Ip6TempRDNSSAddr, pIf6Entry->Ip6RdnssAddrThree) !=
			    IP6_ZERO)
	    {
		    MEMSET (ac1LineBuf, 0, sizeof (ac1LineBuf));
		    SNPRINTF (ac1LineBuf, LIP6_LINE_LEN,
				    "\n\tRDNSS %s ", Ip6PrintNtop (&pIf6Entry->Ip6RdnssAddrThree));

		    u4Len = STRLEN (ac1LineBuf);
		    if ((FileWrite (i4Fd, ac1LineBuf, u4Len)) == 0)
		    {
			    return OSIX_FAILURE;
		    }
        MEMSET (ac1LineBuf, 0, sizeof (ac1LineBuf));
		    SNPRINTF (ac1LineBuf, LIP6_LINE_LEN, "\n\t{\n");
        u4Len = STRLEN (ac1LineBuf);
		    if ((FileWrite (i4Fd, ac1LineBuf, u4Len)) == 0)
		    {
			    return OSIX_FAILURE;
		    }

		    if (pIf6Entry->u1RAdvRDNSSStatus == IP6_RA_NO_RDNSS_OPEN)
		    {
			    MEMSET (ac1LineBuf, 0, sizeof (ac1LineBuf));
			    SNPRINTF (ac1LineBuf, LIP6_LINE_LEN, "\t\tAdvRDNSSOpen off;\n");

			    u4Len = STRLEN (ac1LineBuf);
        if ((FileWrite (i4Fd, ac1LineBuf, u4Len)) == 0)
        {
            return OSIX_FAILURE;
        }
		    }
		    else
		    {
			    MEMSET (ac1LineBuf, 0, sizeof (ac1LineBuf));
			    SNPRINTF (ac1LineBuf, LIP6_LINE_LEN, "\t\tAdvRDNSSOpen on;\n");

			    u4Len = STRLEN (ac1LineBuf);
			    if ((FileWrite (i4Fd, ac1LineBuf, u4Len)) == 0)
			    {
				    return OSIX_FAILURE;
			    }
		    }
		    if (pIf6Entry->u4RDNSSPreference == 0)
		    {
			    MEMSET (ac1LineBuf, 0, sizeof (ac1LineBuf));
			    SNPRINTF (ac1LineBuf, LIP6_LINE_LEN,
					    "\t\tAdvRDNSSPreference %u;\n", IP6_IF_RA_RDNSS_DEF_PREF);
			    u4Len = STRLEN (ac1LineBuf);
			    if ((FileWrite (i4Fd, ac1LineBuf, u4Len)) == 0)
			    {
				    return OSIX_FAILURE;
			    }
		    }
		    else
		    {
			    MEMSET (ac1LineBuf, 0, sizeof (ac1LineBuf));
			    SNPRINTF (ac1LineBuf, LIP6_LINE_LEN,
					    "\t\tAdvRDNSSPreference %u;\n",
					    pIf6Entry->u4RDNSSPreference);
			    u4Len = STRLEN (ac1LineBuf);
			    if ((FileWrite (i4Fd, ac1LineBuf, u4Len)) == 0)
			    {
				    return OSIX_FAILURE;
			    }
		    }

		    if (pIf6Entry->u4RDNSSLifetimeThree == 0)
		    {
			    MEMSET (ac1LineBuf, 0, sizeof (ac1LineBuf));
			    SNPRINTF (ac1LineBuf, LIP6_LINE_LEN,
					    "\t\tAdvRDNSSLifetime 0;\n\t};\n"
			              );

			    u4Len = STRLEN (ac1LineBuf);
			    if ((FileWrite (i4Fd, ac1LineBuf, u4Len)) == 0)
			    {
				    return OSIX_FAILURE;
			    }
		    }
		    else
		    {
			    MEMSET (ac1LineBuf, 0, sizeof (ac1LineBuf));
			    SNPRINTF (ac1LineBuf, LIP6_LINE_LEN,
					    "\t\tAdvRDNSSLifetime %u;\n\t};\n", pIf6Entry->u4RDNSSLifetimeThree);
			    u4Len = STRLEN (ac1LineBuf);
			    if ((FileWrite (i4Fd, ac1LineBuf, u4Len)) == 0)
			    {
				    return OSIX_FAILURE;
			    }
		    }

	    }
    }
    MEMSET (ac1LineBuf, 0, sizeof (ac1LineBuf));
    SNPRINTF (ac1LineBuf, LIP6_LINE_LEN, "};\n");
    u4Len = STRLEN (ac1LineBuf);

    if ((FileWrite (i4Fd, ac1LineBuf, u4Len)) == 0)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
       
}

/****************************************************************************
 * Function    : Lip6RAWritePrefixConfig
 * Description : Write RA configuration for a prefix list of an interface
 *               to radvd.conf
 * Input       : PrefixList - Prefix SLL list
 *               i4Fd - File descriptor of radvd.conf
 * Output      : None
 * Returns     : OSIX_SUCCESS/OSIX_FAILURE
 ***************************************************************************/
PRIVATE INT4
Lip6RAWritePrefixConfig (tTMO_SLL * pPrefixList, INT4 i4Fd)
{
    tLip6PrefixNode    *pPrefixInfo = NULL;
    CHR1                ac1LineBuf[LIP6_LINE_LEN];
    UINT4               u4Len = 0;

    TMO_SLL_Scan (pPrefixList, pPrefixInfo, tLip6PrefixNode *)
    {
        if (pPrefixInfo->u1AdminStatus != IP6FWD_ACTIVE)
        {
            continue;
        }

        MEMSET (ac1LineBuf, 0, sizeof (ac1LineBuf));
        SNPRINTF (ac1LineBuf, LIP6_LINE_LEN,
                  "\tprefix %s/%d\n"
                  "\t{\n",
                  Ip6PrintNtop (&pPrefixInfo->Ip6Prefix),
                  pPrefixInfo->i4PrefixLen);
        u4Len = STRLEN (ac1LineBuf);
        if ((FileWrite (i4Fd, ac1LineBuf, u4Len)) == 0)
        {
            return OSIX_FAILURE;
        }
        MEMSET (ac1LineBuf, 0, sizeof (ac1LineBuf));
        if ((gIp6GblInfo.u1RFC5942Compatibility) == IP6_RFC5942_COMPATIBLE)
        {
            if ((IP6_ADDR_ON_LINK (pPrefixInfo->u2ProfileIndex))
                && IP6_ADDR_PREF_TIME (pPrefixInfo->u2ProfileIndex))
            {
                SNPRINTF (ac1LineBuf, LIP6_LINE_LEN, "\t\tAdvOnLink on;\n");
                u4Len = STRLEN (ac1LineBuf);

                if ((FileWrite (i4Fd, ac1LineBuf, u4Len)) == 0)
                {
                    return OSIX_FAILURE;
                }
            }
            if ((!IP6_ADDR_ON_LINK (pPrefixInfo->u2ProfileIndex))
                || (!IP6_ADDR_PREF_TIME (pPrefixInfo->u2ProfileIndex)))
            {
                SNPRINTF (ac1LineBuf, LIP6_LINE_LEN, "\t\tAdvOnLink off;\n");
                u4Len = STRLEN (ac1LineBuf);

                if ((FileWrite (i4Fd, ac1LineBuf, u4Len)) == 0)
                {
                    return OSIX_FAILURE;
                }
            }
        }
        else
        {
            SNPRINTF (ac1LineBuf, LIP6_LINE_LEN, "\t\tAdvOnLink on;\n");
            u4Len = STRLEN (ac1LineBuf);

            if ((FileWrite (i4Fd, ac1LineBuf, u4Len)) == 0)
            {
                return OSIX_FAILURE;
            }

        }

        MEMSET (ac1LineBuf, 0, sizeof (ac1LineBuf));
        SNPRINTF (ac1LineBuf, LIP6_LINE_LEN,
                  "\t\tAdvAutonomous on;\n"
                  "\t\tAdvRouterAddr off;\n"
                  "\t\tAdvValidLifetime %u;\n"
                  "\t\tAdvPreferredLifetime %u;\n"
                  "\t};\n",
                  IP6_ADDR_VALID_TIME (pPrefixInfo->u2ProfileIndex),
                  IP6_ADDR_PREF_TIME (pPrefixInfo->u2ProfileIndex));
        u4Len = STRLEN (ac1LineBuf);

        if ((FileWrite (i4Fd, ac1LineBuf, u4Len)) == 0)
        {
            return OSIX_FAILURE;
        }
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    : Lip6StartRadvd
 * Description : API to start radvd with the configurations present 
 *               in radvd.conf
 * Input       : None
 * Output      : None
 * Returns     : None
 ***************************************************************************/
PUBLIC VOID
Lip6StartRadvd (UINT4 u4ContextId)
{
#if defined(VRF_WANTED) && defined(LINUX_310_WANTED)
    UINT1             aRadvdCmd[LIP6_RADVD_CMD_LEN];
    UINT1             aRadvdPid[LIP6_RADVD_PID_LEN];
    UINT1             aRadConfFile[LIP6_RADVD_FILE_LEN];
    MEMSET(aRadvdCmd,0,LIP6_RADVD_CMD_LEN);
    MEMSET(aRadvdPid,0,LIP6_RADVD_PID_LEN);
    if(u4ContextId != VCM_DEFAULT_CONTEXT)
    {
        SPRINTF((CHR1 *)aRadvdPid,"rm -rf /var/run/pid_ns%d",u4ContextId);
        system((CHR1 *)aRadvdPid);
        SPRINTF((CHR1 *)aRadConfFile, "/etc/radvd_ns%d.conf", u4ContextId);
        SPRINTF((CHR1 *)aRadvdCmd,"radvd -C %s -p /var/run/pid_ns%d",aRadConfFile,u4ContextId);
    }
    else
    {
        SPRINTF((CHR1 *)aRadvdCmd,"radvd");
    }
    system ((CHR1 *)aRadvdCmd);
#else
    system ("radvd");
    UNUSED_PARAM (u4ContextId);
#endif
}

/****************************************************************************
 * Function    : Lip6StopRadvd
 * Description : API to stop radvd which is currently running
 *               
 * Input       : u4ContextId - VrfId
 * Output      : None
 * Returns     : None
 ***************************************************************************/
PUBLIC VOID
Lip6StopRadvd (UINT4 u4ContextId)
{
#if defined(VRF_WANTED) && defined(LINUX_310_WANTED)
    FILE               *pFd;
    UINT1             aRadvdCmd[LIP6_RADVD_CMD_LEN];
    UINT1             aRadvdPid[LIP6_RADVD_PID_LEN];
    UINT4              u4Pid;

    MEMSET(aRadvdCmd,0,LIP6_RADVD_CMD_LEN);
    MEMSET(aRadvdPid,0,LIP6_RADVD_PID_LEN);
    SPRINTF((CHR1 *)aRadvdPid,"/var/run/pid_ns%d",u4ContextId);
    if(u4ContextId != VCM_DEFAULT_CONTEXT)
    {
        if ((pFd = FOPEN ((CHR1 *)aRadvdPid,"r")) != NULL)
        {
            fscanf(pFd,"%d",&u4Pid);
            if(u4Pid != 0)
            {
                SPRINTF((CHR1 *)aRadvdCmd,"kill -9 %d",u4Pid);
            }
            fclose(pFd);
        }
    }
    else
    {
        SPRINTF((CHR1 *)aRadvdCmd,"killall -q radvd");
    }
    system ((CHR1 *)aRadvdCmd);
#else
    system ("rm -rf /var/run/radvd/radvd.pid");
    UNUSED_PARAM(u4ContextId);
#endif
}
#if defined(VRF_WANTED) && defined(LINUX_310_WANTED)
/****************************************************************************
 * Function    : Lip6TruncateRaConf
 * Description : API to truncate configuration file
 *
 * Input       : u4ContextId - ContextId
 * Output      : None
 * Returns     : None
 ***************************************************************************/
VOID Lip6TruncateRaConf(UINT4 u4ContextId)
{
   INT4                i4Fd = 0;
   if(u4ContextId != VCM_DEFAULT_CONTEXT)
   {
       SPRINTF((CHR1 *)gaRadConfFile, "/etc/radvd_ns%d.conf", u4ContextId);
   }
   else
   {
       SPRINTF((CHR1 *)gaRadConfFile, (const CHR1 *)LIP6_RADVD_CONF_FILE);
   }
   i4Fd = FileOpen (gaRadConfFile, OSIX_FILE_TR);
   if (i4Fd < 0)
   {
       LIP6_TRC_ARG (LIP6_MOD_TRC, ALL_FAILURE_TRC,
               "FileOpen for TRUNCATE failed for RADVD CONF File\r\n");
       return;
   }
   FileClose (i4Fd);
   return;
}


/****************************************************************************
 * Function    : Lip6StopAllRadvd
 * Description : API to stop all radvd which are currently running
 *
 * Input       : None
 * Output      : None
 * Returns     : None
 ***************************************************************************/
PUBLIC VOID
Lip6StopAllRadvd ()
{
    UINT4               u4ContextId = VCM_DEFAULT_CONTEXT;
    UINT4               u4TempContextId = 0;

    if (VcmGetFirstActiveContext (&u4ContextId) != VCM_SUCCESS)
    {
        return;
    }
    do
    {
        Lip6StopRadvd (u4ContextId);
        Lip6TruncateRaConf(u4ContextId);
        u4TempContextId = u4ContextId;
    }
    while (VcmGetNextActiveContext (u4TempContextId, &u4ContextId) ==
           VCM_SUCCESS);

}
#endif

#endif /* _LIP6RA_C */
/* END OF FILE */
