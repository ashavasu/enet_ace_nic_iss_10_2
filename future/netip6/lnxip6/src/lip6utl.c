/**************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: lip6utl.c,v 1.16 2015/11/13 12:05:11 siva Exp $
 *
 * Description: 
 ***************************************************************************/
#ifndef _LIP6UTL_C
#define _LIP6UTL_C

#include "lip6inc.h"

/******************************************************************************
 * DESCRIPTION :  For a given the index, check whether the valid ip6If
 *                entry exists or not
 *
 * INPUTS      : u4IfIndex
 *
 * OUTPUTS     : NONE.
 *
 * RETURNS     : OSIX_SUCCESS/OSIX_FAILURE
 *
 *
 * NOTES       :
 ******************************************************************************/
PUBLIC INT1
Lip6UtlIfEntryExists (UINT4 u4IfIndex)
{
    if (Lip6UtlGetIfEntry (u4IfIndex) == NULL)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : Get the ip6If entry if it exists for the given index
 *
 * INPUTS      : u4IfIndex
 *
 * OUTPUTS     : NONE.
 *
 * RETURNS     : Pointer to the if_entry on Success. NULL on Failure.
 ******************************************************************************/
PUBLIC tLip6If     *
Lip6UtlGetIfEntry (UINT4 u4IfIndex)
{
    tLip6If            *pIf6Entry = NULL;
    UINT4               u4ArrayIndex = 0;
#ifndef LNXIP4_WANTED  /* FOR_IP6_NETIP6*/    /*IPv6 Enables */
    u4ArrayIndex = u4IfIndex - BRG_MAX_PHY_PLUS_LOG_PORTS - 1;
#else
    u4ArrayIndex = u4IfIndex;
#endif
    /* If the derived index is more than max, return NULL */
    if (u4ArrayIndex >= LIP6_MAX_INTERFACES)
    {
        return NULL;
    }

    pIf6Entry = gIp6GblInfo.apIp6If[u4ArrayIndex];

    return pIf6Entry;
}

/******************************************************************************
 * DESCRIPTION : Get the ip6If entry if it exists for the given IpPort
 *
 * INPUTS      : u4IpPort
 *
 * OUTPUTS     : NONE.
 *
 * RETURNS     : Pointer to the if_entry on Success. NULL on Failure.
 ******************************************************************************/
PUBLIC tLip6If     *
Lip6UtlGetIfEntryForPort (UINT4 u4IpPort)
{
    tLip6If            *pIf6Entry = NULL;
    UINT4               u4ArrayIndex = 0;

    for (u4ArrayIndex = 0; u4ArrayIndex < LIP6_MAX_INTERFACES; u4ArrayIndex++)
    {
        pIf6Entry = gIp6GblInfo.apIp6If[u4ArrayIndex];

        if (pIf6Entry == NULL)
        {
            continue;
        }

        if (pIf6Entry->u4IpPort == u4IpPort)
        {
            return pIf6Entry;
        }
    }

    return NULL;
}

/******************************************************************************
 * Function Name    :   Lip6UtlGetIfInfo
 * Description      :   This function provides the interface related
 *                      information for the given interface index
 * Inputs           :   u4IfIndex - Interface index
 * Outputs          :   pNetIpv6IfInfo - Interface information for the given
 *                      interface index
 * Return Value     :   OSIX_SUCCESS or OSIX_FAILURE
 ******************************************************************************/
PUBLIC INT4
Lip6UtlGetIfInfo (UINT4 u4IfIndex, tNetIpv6IfInfo * pNetIpv6IfInfo)
{
    tLip6If            *pIf6Entry = NULL;
    UINT1               u1DadStatus = 0;

    if (Lip6PortGetIfName (u4IfIndex, pNetIpv6IfInfo->au1IfName) ==
        OSIX_FAILURE)
    {
        LIP6_TRC_ARG (LIP6_MOD_TRC, CONTROL_PLANE_TRC,
                      "Ipv6GetIfInfo : Invalid Interface Index !!!\n");
        return OSIX_FAILURE;
    }

    pIf6Entry = Lip6UtlGetIfEntry (u4IfIndex);
    if (pIf6Entry == NULL)
    {
        LIP6_TRC_ARG (LIP6_MOD_TRC, CONTROL_PLANE_TRC,
                      "Ipv6GetIfInfo : Invalid Interface Index !!!\n");
        return OSIX_FAILURE;
    }

    /* Get the link local address for the interface from linux */
    Lip6LlaGetLinkLocalAddress (pIf6Entry, &(pNetIpv6IfInfo->Ip6Addr),
                                &u1DadStatus);

    SNPRINTF ((CHR1 *) pNetIpv6IfInfo->au1IfName, IP6_MAX_IF_NAME_LEN,
              "%s", pIf6Entry->au1IfName);

    pNetIpv6IfInfo->u4IfIndex = u4IfIndex;
    pNetIpv6IfInfo->u4IfSpeed = pIf6Entry->u4IfSpeed;
    pNetIpv6IfInfo->u4IfHighSpeed = pIf6Entry->u4IfHighSpeed;
    pNetIpv6IfInfo->u4Admin = pIf6Entry->u1AdminStatus;
    pNetIpv6IfInfo->u4Oper = pIf6Entry->u1OperStatus;
    pNetIpv6IfInfo->u4InterfaceType = pIf6Entry->u1IfType;
    pNetIpv6IfInfo->u4Mtu = pIf6Entry->u4Mtu;
    pNetIpv6IfInfo->u4IpPort = pIf6Entry->u4IpPort;
    pNetIpv6IfInfo->u4AddressLessIf = pIf6Entry->u4UnnumAssocIPv6If;

    return OSIX_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : Gets the index of the next entry in the ipv6IfTable for a
 *               given a current entry.The current entry need not be a
 *               valid entry however.
 *
 * INPUTS      : pu4IfIndex
 *
 * OUTPUTS     : Next entry(pu4IfIndex)
 *
 * RETURNS     : OSIX_SUCCESS/OSIX_FAILURE
 *
 * NOTES       :
 ******************************************************************************/
PUBLIC INT4
Lip6UtlIfGetNextIndex (UINT4 u4IfIndex, UINT4 *pu4NextIndex)
{
    UINT4               u4ArrayIndex = 0;
    UINT4               u4TmpIfIndex = 0;

#ifndef LNXIP4_WANTED /* FOR_IP6_NETIP6*/    /*IPv6 Enables */
    if (u4IfIndex < BRG_MAX_PHY_PLUS_LOG_PORTS)
    {
        u4TmpIfIndex = BRG_MAX_PHY_PLUS_LOG_PORTS + 1;
        u4ArrayIndex = 1;
    }
    else
    {
        u4TmpIfIndex = u4IfIndex + 1;
        u4ArrayIndex = u4TmpIfIndex - BRG_MAX_PHY_PLUS_LOG_PORTS;
    }
#else
    u4ArrayIndex = u4IfIndex;
    u4TmpIfIndex = u4IfIndex + 1;
#endif

    for (; u4ArrayIndex <= (UINT4) IP6_MAX_LOGICAL_IFACES; u4ArrayIndex++)
    {
        if (Lip6UtlIfEntryExists (u4TmpIfIndex) == OSIX_SUCCESS)
        {
            *pu4NextIndex = u4TmpIfIndex;
            return OSIX_SUCCESS;
        }
        u4TmpIfIndex++;
    }

    return OSIX_FAILURE;
}

/******************************************************************************
 * FUNCTION    : Lip6UtlGetIndexForAddr
 * DESCRIPTION : Check whether given IP6 address is configured in any of the
 *               interface and if yes, return ifindex
 *
 * INPUTS      : pIp6Addr - Pointer to IP address
 *
 * OUTPUTS     : pu4IfIndex - Corresponding Interface index for the IP6 address
 *
 * RETURNS     : OSIX_SUCCESS/OSIX_FAILURE
 ******************************************************************************/
PUBLIC INT4
Lip6UtlGetIndexForAddr (tIp6Addr * pIp6Addr, UINT4 *pu4IfIndex, UINT4 u4ContextId)
{
    UINT4               u4ArrayIndex = 0;
    tLip6If            *pIf6Entry = NULL;
    tLip6AddrNode      *pSearchNode = NULL;

    for (u4ArrayIndex = 0; u4ArrayIndex < LIP6_MAX_INTERFACES; u4ArrayIndex++)
    {
        pIf6Entry = gIp6GblInfo.apIp6If[u4ArrayIndex];

        if ((pIf6Entry == NULL) || 
             (gIp6GblInfo.apIp6If[u4ArrayIndex]->u4ContextId != u4ContextId))
        {
            continue;
        }

        TMO_SLL_Scan (&(pIf6Entry->Ip6AddrList), pSearchNode, tLip6AddrNode *)
        {
            if ((MEMCMP (&(pSearchNode->Ip6Addr), pIp6Addr,
                        sizeof (tIp6Addr)) == 0) &&
                (pIf6Entry->u4ContextId == u4ContextId))
            {
                *pu4IfIndex = pIf6Entry->u4IfIndex;
                return OSIX_SUCCESS;
            }
        }

        TMO_SLL_Scan (&(pIf6Entry->Ip6LLAddrList), pSearchNode, tLip6AddrNode *)
        {
            if ((MEMCMP (&(pSearchNode->Ip6Addr), pIp6Addr,
                        sizeof (tIp6Addr)) == 0) &&
                (pIf6Entry->u4ContextId == u4ContextId))
            {
                *pu4IfIndex = pIf6Entry->u4IfIndex;
                return OSIX_SUCCESS;
            }
        }
    }

    return OSIX_FAILURE;
}

/******************************************************************************
 * FUNCTION    : Lip6UtlGetAddrPrefixType
 * DESCRIPTION : Check whether given IP6 address is configured in any of the
 *               interface and if yes, return PrefixLen and AddressType
 *
 * INPUTS      : pIp6Addr - Pointer to IP address
 *
 * OUTPUTS     : *pi4Prefix - Pointer to Prefix Len
 *               *pu1Type - Pointer to Address type
 *
 * RETURNS     : OSIX_SUCCESS/OSIX_FAILURE
 ******************************************************************************/
PUBLIC INT4
Lip6UtlGetAddrPrefixType (tIp6Addr * pIp6Addr, INT4 *pi4Prefix, UINT1 *pu1Type)
{
    UINT4               u4ArrayIndex = 0;
    tLip6If            *pIf6Entry = NULL;
    tLip6AddrNode      *pSearchNode = NULL;

    for (u4ArrayIndex = 0; u4ArrayIndex < LIP6_MAX_INTERFACES; u4ArrayIndex++)
    {
        pIf6Entry = gIp6GblInfo.apIp6If[u4ArrayIndex];

        if (pIf6Entry == NULL)
        {
            continue;
        }
        if (Ip6AddrType (pIp6Addr) != ADDR6_LLOCAL)
        {
            TMO_SLL_Scan (&(pIf6Entry->Ip6AddrList), pSearchNode,
                          tLip6AddrNode *)
            {
                if (MEMCMP (&(pSearchNode->Ip6Addr), pIp6Addr,
                            sizeof (tIp6Addr)) == 0)
                {
                    *pi4Prefix = pSearchNode->i4PrefixLen;
                    *pu1Type = pSearchNode->u1AddrType;
                    return OSIX_SUCCESS;
                }
            }
        }
        else
        {
            TMO_SLL_Scan (&(pIf6Entry->Ip6LLAddrList), pSearchNode,
                          tLip6AddrNode *)
            {
                if (MEMCMP (&(pSearchNode->Ip6Addr), pIp6Addr,
                            sizeof (tIp6Addr)) == 0)
                {
                    *pi4Prefix = pSearchNode->i4PrefixLen;
                    *pu1Type = pSearchNode->u1AddrType;
                    return OSIX_SUCCESS;
                }
            }
        }
    }

    return OSIX_FAILURE;
}

/******************************************************************************
 * DESCRIPTION : Obtain the MTU of the interface.
 *
 * INPUTS      : The interface pointer (pIf6Entry)
 *
 * OUTPUTS     : None
 *
 * RETURNS     : The MTU of the interface
 ******************************************************************************/
PUBLIC UINT4
Lip6UtlGetMtu (tLip6If * pIf6Entry)
{
#ifdef TUNNEL_WANTED
    UINT4               u4DefMtu;

    if (pIf6Entry->u1IfType == IP6_TUNNEL_INTERFACE_TYPE)
    {
        /* Configured Tunnel. */
        u4DefMtu = pIf6Entry->u4Mtu;    /* Over tunnels Pmtu is discovered
                                         * using V4 PMtud if Pmtu is not
                                         * there need to IPv6MIN MTU.
                                         */
        if (u4DefMtu <= LIP6_MIN_MTU)
        {
            u4DefMtu = LIP6_MIN_MTU;
        }
    }
    else
    {
        u4DefMtu = pIf6Entry->u4Mtu;
    }
    return u4DefMtu;
#else
    return pIf6Entry->u4Mtu;
#endif
}

/******************************************************************************
 * DESCRIPTION : Get the AddrPrefixlen of the given Ip6 address
 *
 * INPUTS      : Ip6Address,Ip6IfIndex
 *
 * OUTPUTS     : NONE.
 *
 * RETURNS     : OSIX_SUCCESS,OSIX_FAILURE otherwise.
 *
 * NOTES       :
 *****************************************************************************/
PUBLIC INT4
Lip6UtlGetAddrPrefixLen (UINT4 u4IfIndex, tIp6Addr * pIpv6AddrAddress,
                         INT4 *pi4Ipv6AddrPrefixLength)
{
    tLip6If            *pIf6Entry = NULL;
    tLip6AddrNode      *pSearchNode = NULL;

    pIf6Entry = Lip6UtlGetIfEntry (u4IfIndex);

    if (pIf6Entry == NULL)
    {
        return OSIX_FAILURE;
    }

    if (Ip6AddrType (pIpv6AddrAddress) == ADDR6_LLOCAL)
    {
        *pi4Ipv6AddrPrefixLength = IP6_ADDR_LL_PREFIX_LEN;
        return OSIX_SUCCESS;
    }
    else
    {
        TMO_SLL_Scan (&(pIf6Entry->Ip6AddrList), pSearchNode, tLip6AddrNode *)
        {
            if (MEMCMP (&pSearchNode->Ip6Addr, pIpv6AddrAddress,
                        sizeof (tIp6Addr)) == 0)
            {
                *pi4Ipv6AddrPrefixLength = pSearchNode->i4PrefixLen;
                return OSIX_SUCCESS;
            }
        }
    }

    return OSIX_FAILURE;
}

/******************************************************************************
 * DESCRIPTION : Get Admin status of the address.
 *
 * INPUTS      : u4IfIndex - If index over which the address is assigned.
 *               pIp6Addr      - Pointer to the address
 *
 * OUTPUTS     : pi4RetValIpv6AddrStatus - Pointer to the status of the 
 *                                         address.
 *                                         IP6_ADDR_STAT_PREFERRED   
 *                                         IP6_ADDR_STAT_DEPRECATE   
 *                                         IP6_ADDR_STAT_INVALID     
 *                                         IP6_ADDR_STAT_INACCESSIBLE
 *                                         IP6_ADDR_STAT_UNKNOWN     
 *
 * RETURNS     : OSIX_SUCCESS/OSIX_FAILURE
 ******************************************************************************/
PUBLIC INT1
Lip6UtlGetAddrStatus (UINT4 u4IfIndex, tIp6Addr * pIp6Addr,
                      INT4 *pi4RetValIpv6AddrStatus)
{
    tLip6AddrNode      *pAddrInfo = NULL;
    INT4                i4PrefixLen = 0;

    if (Lip6UtlGetAddrPrefixLen (u4IfIndex, pIp6Addr,
                                 &i4PrefixLen) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ip6AddrType (pIp6Addr) == ADDR6_LLOCAL)
    {
        *pi4RetValIpv6AddrStatus = IP6_ADDR_STAT_PREFERRED;
        return OSIX_SUCCESS;
    }

    pAddrInfo = Lip6AddrGetEntry (u4IfIndex, pIp6Addr, i4PrefixLen);

    if (pAddrInfo == NULL)
    {
        *pi4RetValIpv6AddrStatus = IP6_ADDR_STAT_INACCESSIBLE;
        return OSIX_SUCCESS;
    }

    *pi4RetValIpv6AddrStatus = IP6_ADDR_STAT_PREFERRED;
    return OSIX_SUCCESS;
}

/*****************************************************************************
* DESCRIPTION : Function returns the Interface Type for the given index
* INPUTS      : Interface Index
* OUTPUTS     : None 
* RETURNS     : Interface Type - if IfIndex is valid
*               0 - Otherwise
****************************************************************************/
UINT1
Ip6GetIfType (UINT4 u4IfIndex)
{
    tLip6If            *pIf6Entry = NULL;

    pIf6Entry = Lip6UtlGetIfEntry (u4IfIndex);

    if (pIf6Entry == NULL)
    {
        return 0;
    }

    return (pIf6Entry->u1IfType);
}

/******************************************************************************
 *Description         : This functions gets the context id associated with the 
 *                      the given IPv6 Interface Index
 *
 * Input(s)           : u4IfIndex - Ipv6 Interface Index
 *
 * Output(s)          : *pu4ContextId - The VR to which the interface is mapped
 *
 * Returns            : IP6_SUCCESS or IP6_FAILURE
 ******************************************************************************/
INT4
Ip6GetCxtId (UINT4 u4IfIndex, UINT4 *pu4ContextId)
{
#if defined(VRF_WANTED) && (LINUX_310_WANTED)
    tLip6If            *pIf6Entry = NULL;

    if ((pIf6Entry = Lip6UtlGetIfEntry (u4IfIndex)) == NULL)
    {
        return IP6_FAILURE;
    }

    *pu4ContextId = pIf6Entry->u4ContextId;

#else
    UNUSED_PARAM (u4IfIndex);
    *pu4ContextId = VCM_DEFAULT_CONTEXT;
#endif
    return IP6_SUCCESS;
}

/* Dummy function definitions, common for fsip and lnxip */
INT4
Ip6SelectContext (UINT4 u4ContextId)
{
#if defined(VRF_WANTED) && (LINUX_310_WANTED)
    if(u4ContextId >= SYS_DEF_MAX_NUM_CONTEXTS)
    {
        return SNMP_FAILURE;
    }
    gu4LnxVrfCurrContextId = u4ContextId;
#else
    UNUSED_PARAM (u4ContextId);
#endif
    return SNMP_SUCCESS;
}

UINT4
Lip6GetCurrentContext (VOID)
{
#if defined(VRF_WANTED) && (LINUX_310_WANTED)
    return gu4LnxVrfCurrContextId;
#else
    return SNMP_SUCCESS; 
#endif
}

VOID
Ip6ReleaseContext (VOID)
{
#if defined(VRF_WANTED) && (LINUX_310_WANTED)
    gu4LnxVrfCurrContextId = 0;
#endif
    return;
}

tIp6Udp6Stats      *
Udp6GetStatEntry (VOID)
{
    return NULL;
}

INT4
Ip6VRExists (UINT4 u4ContextId)
{
    if (VcmIsVcExist (u4ContextId) == VCM_FALSE)
    {
        return IP6_FAILURE;
    }
    return IP6_SUCCESS;
}

UINT4
Ip6GetIfReachTime (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);
    return REACHABLE_TIME;
}

/****************************************************************************
 * DESCRIPTION : This routine is used by SLI to register the Path MTU handler
 *               with IP6
 * INPUTS      : A function Pointer with Destination Ip6 Address and
 *               new PathMTU value as its input parameters
 *
 * OUTPUTS     : Updates the Global variable gIp6PmtuHandler, if not null
 *
 * RETURNS     : IP6_SUCCESS/IP6_FAILURE
 *
 * NOTES       : The registered function is called whenever there is a change in *               the Path MTU
 *****************************************************************************/
INT4
Ip6PmtuRegHl (INT4  (*PmtuHandler) (UINT4, tIp6Addr, UINT4))
{
    UNUSED_PARAM (PmtuHandler);
    return IP6_SUCCESS;
}

/****************************************************************************
 * DESCRIPTION : This routine enrols an application to UDP port. Only after
 *               this call application can be allowed to send data through
 *               UDP port.
 *
 * INPUTS      : Port number                 (UINT2 u2UPort)  &
 *               Application task identifier (UINT2 u2_task_id) &
 *               Queue identifier            (UINT2 u2_rcv_q_id) &
                 Context Identifier          (UINT4 u4ContextId) &
                 Socket Mode                 (UINT1 u1FdMode)
 *                 
 * OUTPUTS     : None
 *
 * RETURNS     : IP6_SUCCESS - if application is attached to one of the control 
 *                         blocks
 *               IP6_FAILURE - otherwise
 * NOTES       : Presently only RIP6 can be enrolled as an application
 *****************************************************************************/
INT4
Udp6OpenInCxt (UINT4 u4ContextId, UINT1 u1Mode,
               INT4 i4SockDesc, UINT2 *pu2UPort,
               UINT1 au1TaskName[4], UINT1 au1QName[4],
               tIp6Addr * pIp6Addr, UINT2 *pu2RPort,
               tIp6Addr * pIp6RemAddr, VOID (*rcv_fnc))
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u1Mode);
    UNUSED_PARAM (i4SockDesc);
    UNUSED_PARAM (pu2UPort);
    UNUSED_PARAM (au1TaskName);
    UNUSED_PARAM (au1QName);
    UNUSED_PARAM (pIp6Addr);
    UNUSED_PARAM (pu2RPort);
    UNUSED_PARAM (pIp6RemAddr);
    UNUSED_PARAM (rcv_fnc);

    return IP6_SUCCESS;
}

/****************************************************************************
* Function     : Ipv6UtilGetConfigMethod
*
* Description  : This function gives the configuration method for the 
*                 given link local address.
*
* Input        : u4IfIndex   - The interface on which the address is configured
*                pFsipv6AddrAddress - The link local address  
*
* Output       : pu1CfgMethod - The configuration method.
*
* Returns      : Debug Value.
*
***************************************************************************/

VOID
Ipv6UtilGetConfigMethod (UINT4 u4IfIndex,
                         tSNMP_OCTET_STRING_TYPE * pFsipv6AddrAddress,
                         UINT1 u1PrefixLen, UINT1 u1AddressType,
                         UINT1 *pu1CfgMethod)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pFsipv6AddrAddress);
    UNUSED_PARAM (u1PrefixLen);
	UNUSED_PARAM (u1AddressType);
    UNUSED_PARAM (pu1CfgMethod);
    return;
}

/******************************************************************************
 * DESCRIPTION : Get the unnumnumbered interface entry if it exists for the given index
 *
 * INPUTS      : u4IfIndex
 *
 * OUTPUTS     : NONE.
 *
 * RETURNS     : Pointer to the if_entry on Success. NULL on Failure.
 ******************************************************************************/
PUBLIC INT1
Ip6UtlGetUnnumIfEntry (UINT4 u4IfIndex)
{
    tLip6If            *pIf6Entry = NULL;
    UINT4               u4ArrayIndex = 0;

    for (u4ArrayIndex = 0; u4ArrayIndex < LIP6_MAX_INTERFACES; u4ArrayIndex++)
    {
        pIf6Entry = gIp6GblInfo.apIp6If[u4ArrayIndex];

        if (pIf6Entry == NULL)
        {
            continue;
        }

        if (pIf6Entry->u4UnnumAssocIPv6If == u4IfIndex)
        {
            return OSIX_FAILURE;
        }
    }

    return OSIX_SUCCESS;
}

/******************************************************************************
 * Function Name    :   Ip6UtilClearIpv6InterfaceTable
 * Description      :   This function clears the Ipv6Interface entry
 *                      configurations done for the given interface.
 *
 * Inputs           :   i4IfIndex - Interface Id
 * Return Value     :   IP6_SUCCESS or IP6_FAILURE
 *******************************************************************************/
INT1
Ip6UtilClearIpv6InterfaceTable (INT4 i4IfIndex)
{
    tLip6If            *pIf6Entry = NULL;

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4IfIndex);

    if (pIf6Entry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pIf6Entry->u1AdminStatus == ADMIN_DOWN)
    {
        /* No change in the Status. */
        return SNMP_SUCCESS;
    }

    /* IPv6 needs to disabled over this interface. */
    if (Lip6IfDisable (pIf6Entry) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}
/******************************************************************************
 * Function Name    :   Lip6KernelDeleteAddrForIndex
 * Description      :   This function clears the Ipv6Interface entry
 *                      configurations done for the given interface.
 *
 * Inputs           :   u4Index - Interface Id
 * Return Value     :   None
 *******************************************************************************/
VOID
Lip6KernelDeleteAddrForIndex(UINT4 u4Index)
{

    tLip6If            *pIf6Entry = NULL;

    if ((pIf6Entry = Lip6UtlGetIfEntry (u4Index)) == NULL)
    {
        return;
    }
    Lip6IfDisable(pIf6Entry);
    Lip6AddrDeleteAll(pIf6Entry);
    pIf6Entry->u1TokLen = 0;
    MEMSET (pIf6Entry->ifaceTok, 0, IP6_EUI_ADDRESS_LEN);

    pIf6Entry->u4HopLimit = IP6_IF_DEF_HOPLMT;
    pIf6Entry->u4DadAttempts = IP6_IF_DEF_DAD_SEND;
    pIf6Entry->u1PrefAdv = IP6_IF_PREFIX_ADV;
    pIf6Entry->u4MaxRaTime = IP6_IF_DEF_MAX_RA_TIME;
    pIf6Entry->u4MinRaTime = IP6_IF_DEF_MIN_RA_TIME;
    pIf6Entry->u4DefRaLifetime = IP6_RA_DEF_DEFTIME;
    pIf6Entry->u1RAdvStatus = IP6_IF_ROUT_ADV_DISABLED;
    pIf6Entry->u1RAAdvFlags = IP6_IF_NO_RA_ADV;
    pIf6Entry->u4RaMtu = IP6_RA_DEF_LINK_MTU;
    pIf6Entry->u4RACurHopLimit = IP6_IF_DEF_HOPLMT;
    pIf6Entry->u4Mtu = IP6_DEFAULT_MTU;
    pIf6Entry->u4RARetransTimer = IP6_IF_DEF_RETTIME;
    pIf6Entry->u4RAReachableTime = IP6_IF_DEF_REACHTIME;
    pIf6Entry->Icmp6ErrRLInfo.i4Icmp6DstUnReachable =
        ICMP6_DEST_UNREACHABLE_ENABLE;
    pIf6Entry->Icmp6ErrRLInfo.i4Icmp6ErrInterval = ICMP6_DEF_ERR_INETRVAL;
    pIf6Entry->Icmp6ErrRLInfo.i4Icmp6BucketSize = ICMP6_DEF_BUCKET_SIZE;
    pIf6Entry->Icmp6ErrRLInfo.i4Icmp6RedirectMsg = ICMP6_REDIRECT_DISABLE;
    pIf6Entry->u1RALinkLocalStatus = IP6_RA_NO_ADV_LINKLOCAL;
    pIf6Entry->u1RAInterval = IP6_RA_NO_ADV_INTERVAL;
    pIf6Entry->u1Ipv6IfFwdOperStatus = LIP6_FORW_ENABLE;
    return;
}

/**************************************************************************************
*  Function Name             :  Lip6Ipv6CheckHitOnNDCacheEntry
*  Description               :  This function reads the hit bit on the ND Cache entry
*  Input(s)                  :  pIp6Addr  -- Ipv6 Destination Address whose Cache
*                                             has to be checked
*                               u4IfIndex -- Interface Index
*  Output(s)                 :  None
*  Global Variables Referred :  None
*  Global variables Modified :  None
*  Exceptions                :  None
*  Use of Recursion          :  None
*  Returns                   :  FNP_FAILURE(0) on failure
*                               FNP_SUCCESS(1) on success
***************************************************************************************/
PUBLIC INT4
Lip6Ipv6CheckHitOnNDCacheEntry (UINT4 u4IfIndex, tIp6Addr * pIp6Addr)
{
    UINT4 u4ContextId = (UINT4)UtilRtm6GetCurrCxtId();
    INT4                i4RetVal = 0;
#ifdef NPAPI_WANTED
    i4RetVal = FsNpIpv6CheckHitOnNDCacheEntry (u4ContextId, (UINT1*) &pIp6Addr, u4IfIndex);
    if (i4RetVal == FNP_FALSE)
    {
        return IP6_FAILURE;
    }
    return IP6_SUCCESS;
#else
UNUSED_PARAM (u4IfIndex);
UNUSED_PARAM (pIp6Addr);
UNUSED_PARAM (u4ContextId);
UNUSED_PARAM (i4RetVal);
#endif

    return IP6_FAILURE;
}


#if defined(VRF_WANTED) && defined(LINUX_310_WANTED)
/******************************************************************************
 * Function Name    :   Ip6UtilChangeCxt
 * Description      :   This function changes the context in linux
 *
 * Inputs           :   u4Index - Interface Id
 * Return Value     :   None
 *******************************************************************************/
UINT4 Ip6UtilChangeCxt (UINT4 u4ContextId, INT4 i4NSType, UINT1 *pu1NameSpaceName)
{
    if(u4ContextId != VCM_DEFAULT_CONTEXT)
    {
        if(LnxVrfChangeCxt (i4NSType,(CHR1 *) pu1NameSpaceName) == NETIPV4_FAILURE)
        {
            return NETIPV6_FAILURE;
        }
    }
    return NETIPV6_SUCCESS;

}
/******************************************************************************
 * Function Name    :   LnxVrfIpv6Initializations
 * Description      :   This function De initializes the configurations
 *
 * Inputs           :   u4VcmCxtId - VRF ID
 * Return Value     :   None
 *******************************************************************************/
VOID LnxVrfIpv6Initializations(UINT4 u4VcmCxtId)
{

    gIp6GblInfo.ai4SockFd[u4VcmCxtId] = -1;
}

/******************************************************************************
 * Function Name    :   LnxVrfIpv6DeInitializations
 * Description      :   This function De initializes the configuration
 *
 * Inputs           :   u4VcmCxtId - VRF ID
 * Return Value     :   None
 *******************************************************************************/
VOID LnxVrfIpv6DeInitializations(UINT4 u4VcmCxtId)
{
    if(gIp6GblInfo.ai4SockFd[u4VcmCxtId] != -1)
    {
        close(gIp6GblInfo.ai4SockFd[u4VcmCxtId]);
        gIp6GblInfo.ai4SockFd[u4VcmCxtId] = -1;
    }
}

/******************************************************************************
 * Function Name    :   LnxVrfIpv6UpdateGlobSock
 * Description      :   This function De initializes the configuration
 *
 * Inputs           :   u4VcmCxtId - VRF ID
 *                      i4SockFd - socketId to be updated
 * Return Value     :   None
 *******************************************************************************/
VOID LnxVrfIpv6UpdateGlobSock (UINT4 u4VcmCxtId, INT4 i4SockFd)
{
    gIp6GblInfo.ai4SockFd[u4VcmCxtId] = i4SockFd;
}
/******************************************************************************
 * Function Name    :   LnxVrfIpv6UpdateGlobSock
 * Description      :   This function De initializes the configuration
 *
 * Inputs           :   u4VcmCxtId - VRF ID
 * Return Value     :   None
 *******************************************************************************/
VOID LnxVrfUpdateCxtIdforIpv6Int (UINT4 u4VcmCxtId, INT4 u4IfIndex,INT4 i4PortNo)
{
    tLip6If            *pIf6Entry = NULL;
    Ip6Lock();
    if ((pIf6Entry = Lip6UtlGetIfEntry (u4IfIndex)) == NULL)
    {
        return;
    }
    pIf6Entry->u4ContextId = u4VcmCxtId;
    if(pIf6Entry->u1IfType == CFA_LOOPBACK)
    {
        pIf6Entry->u4IpPort = (UINT4) i4PortNo;
    }
    Ip6UnLock();

}
/******************************************************************************
 * Function Name    :   LnxVrfEnableIpv6Forwarding
 * Description      :   This function enables the IPv6 forwarding
 *
 * Inputs           :   None
 * Return Value     :   None
 *******************************************************************************/
VOID LnxVrfEnableIpv6Forwarding (VOID)
{
    Lip6KernSetForwarding(LIP6_FORW_ENABLE);
    return;
}

/****************************************************************************
 *  Function    :  LnxVrfIpv6UtilGetCxtEntryFromCxtId
 *
 *   Input      :   u4ContextId - The context Id
 *
 * Description :  This function to get IP6 context entry from context id
 *
 * Output      :  None.
 *
 * Returns     :  pLip6CxtEntry - Pointer to the IP6 context entry
 *
 * ****************************************************************************/

tLip6Cxt            *
LnxVrfIpv6UtilGetCxtEntryFromCxtId (UINT4 u4ContextId)
{
    tLip6Cxt            *pLip6CxtEntry = NULL;

    if (u4ContextId >= SYS_DEF_MAX_NUM_CONTEXTS)
    {
        return NULL;
    }
    pLip6CxtEntry = gIp6GblInfo.apIp6Cxt[u4ContextId];
    return pLip6CxtEntry;
}

#endif /* VRF_WANTED */

#endif /* _LIP6UTL_C */
/* END OF FILE */
