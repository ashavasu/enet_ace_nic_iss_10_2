/**************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: lip6pref.c,v 1.8 2015/03/09 13:09:20 siva Exp $
 *
 * Description: IPV6 Prefix list management routines
 ***************************************************************************/
#ifndef _LIP6PREF_C
#define _LIP6PREF_C
#include "lip6inc.h"

/******************************************************************************
 * DESCRIPTION : Called when an prefix advertised in the RA message is to be
 *               created 
 *
 * INPUTS      : u4IfIndex - Interface index
 *               pPrefix   - Pointer to Prefix address
 *               i4PrefixLen - Prefix Length
 *               u1AdminStatus - Prefix status (ACTIVE or NOT_READY)
 *
 * OUTPUTS     : None
 *
 * RETURNS     : Create Prefix Info or NULL
 ******************************************************************************/
PUBLIC tLip6PrefixNode *
Lip6PrefixCreate (UINT4 u4IfIndex, tIp6Addr * pPrefix, INT4 i4PrefixLen,
                  UINT1 u1AdminStatus)
{
    tLip6PrefixNode    *pTempPrefixInfo = NULL;
    tLip6PrefixNode    *pPrevPrefixInfo = NULL;
    tLip6PrefixNode    *pPrefixInfo = NULL;
    tLip6If            *pIf6Entry = NULL;

    pIf6Entry = Lip6UtlGetIfEntry (u4IfIndex);
    if (pIf6Entry == NULL)
    {
        return NULL;
    }

    pPrefixInfo = (tLip6PrefixNode *)
        MemAllocMemBlk (gIp6GblInfo.PrefixListPoolId);

    if (pPrefixInfo == NULL)
    {
        return NULL;
    }

    MEMSET (pPrefixInfo, 0, sizeof (tLip6PrefixNode));

    Ip6CopyAddrBits (&(pPrefixInfo->Ip6Prefix), pPrefix, i4PrefixLen);
    pPrefixInfo->i4PrefixLen = i4PrefixLen;

    pPrefixInfo->u1AdminStatus = u1AdminStatus;
    pPrefixInfo->u4IfIndex = u4IfIndex;

    /* Add the address to the list of addresses on the interface in the
     * ascending order of the prefix */
    TMO_SLL_Scan (&pIf6Entry->PrefixList, pTempPrefixInfo, tLip6PrefixNode *)
    {
        /* This utility description is not correct. It returns IP6_SUCCESS
         * when 2nd address is greater */
        if (Ip6IsAddrGreater (pPrefix, &(pTempPrefixInfo->Ip6Prefix)) ==
            IP6_SUCCESS)
        {
            break;
        }
        pPrevPrefixInfo = pTempPrefixInfo;
    }

    TMO_SLL_Insert (&pIf6Entry->PrefixList, &(pPrevPrefixInfo->PrefixNode),
                    &(pPrefixInfo->PrefixNode));
    return pPrefixInfo;
}

/******************************************************************************
 * DESCRIPTION : Called when an Prefix advertised over an interface is to be
 *               deleted 
 *
 * INPUTS      : u4IfIndex - Interface index
 *               pPrefix   - Pointer to Prefix address
 *               i4PrefixLen - Prefix Length
 *
 * OUTPUTS     : None
 *
 * RETURNS     : OSIX_SUCCESS / OSIX_FAILURE
 ******************************************************************************/
PUBLIC INT4
Lip6PrefixDelete (UINT4 u4IfIndex, tIp6Addr * pPrefix, INT4 i4PrefixLen)
{

    tLip6PrefixNode    *pPrefixInfo = NULL;
    tLip6If            *pIf6Entry = NULL;

    /* Scan through the Prefix List for the given interface and delete
     * the matching entry. */
    pIf6Entry = Lip6UtlGetIfEntry (u4IfIndex);
    if (pIf6Entry == NULL)
    {
        return OSIX_FAILURE;
    }

    TMO_SLL_Scan (&pIf6Entry->PrefixList, pPrefixInfo, tLip6PrefixNode *)
    {
        if ((Ip6AddrMatch (&(pPrefixInfo->Ip6Prefix), pPrefix,
                           i4PrefixLen) == TRUE) &&
            (i4PrefixLen == pPrefixInfo->i4PrefixLen))

        {
            /* Matching Entry found */
            TMO_SLL_Delete (&pIf6Entry->PrefixList, &(pPrefixInfo->PrefixNode));
            break;
        }
    }

    if (pPrefixInfo == NULL)
    {
        return OSIX_FAILURE;
    }

    /* Free the memory. */
    MemReleaseMemBlock (gIp6GblInfo.PrefixListPoolId, (UINT1 *) pPrefixInfo);

    return OSIX_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : Called when an prefix advertised in the RA message is to be
 *               retrieved.
 *
 * INPUTS      : Interface index, pointer to Prefix address, Prefix Length
 *
 * OUTPUTS     : None
 *
 * RETURNS     : Prefix Info if present or NULL
 ******************************************************************************/
PUBLIC tLip6PrefixNode *
Lip6PrefixGetEntry (UINT4 u4IfIndex, tIp6Addr * pPrefix, INT4 i4PrefixLen)
{
    tLip6PrefixNode    *pPrefixInfo = NULL;
    tLip6If            *pIf6Entry = NULL;
    pIf6Entry = Lip6UtlGetIfEntry (u4IfIndex);
    if (pIf6Entry == NULL)
    {
        return NULL;
    }
    TMO_SLL_Scan (&pIf6Entry->PrefixList, pPrefixInfo, tLip6PrefixNode *)
    {

        if ((Ip6AddrMatch (&(pPrefixInfo->Ip6Prefix), pPrefix,
                           i4PrefixLen) == TRUE) &&
            (i4PrefixLen == pPrefixInfo->i4PrefixLen))
        {
            break;
        }
    }
    if (NULL == pPrefixInfo)
    {
        return NULL;
    }

    return pPrefixInfo;
}

/******************************************************************************
 * DESCRIPTION : Called to fetch the first prefix from the Prefix List 
 *               advertised over the given interface.
 *
 * INPUTS      : Interface index
 *
 * OUTPUTS     : None
 *
 * RETURNS     : Prefix Info if present or NULL
 ******************************************************************************/
PUBLIC tLip6PrefixNode *
Lip6PrefixGetFirst (UINT4 u4IfIndex)
{
    tLip6PrefixNode    *pPrefix = NULL;
    tLip6If            *pIf6Entry = NULL;

    pIf6Entry = Lip6UtlGetIfEntry (u4IfIndex);

    if (pIf6Entry == NULL)
    {
        return NULL;
    }

    pPrefix = (tLip6PrefixNode *) TMO_SLL_First (&pIf6Entry->PrefixList);
    return pPrefix;
}

/******************************************************************************
 * DESCRIPTION : Called to fetch the next prefix from the Prefix List 
 *               advertised over the given interface.
 *
 * INPUTS      : Interface index, pointer to Prefix address, Prefix Length
 *
 * OUTPUTS     : None
 *
 * RETURNS     : Prefix Info if present or NULL
 ******************************************************************************/
PUBLIC tLip6PrefixNode *
Lip6PrefixGetNext (UINT4 u4IfIndex, tIp6Addr * pIp6Addr, INT4 i4PrefixLen)
{
    tLip6PrefixNode    *pPrefix = NULL;
    tLip6PrefixNode    *pNextPrefix = NULL;
    tLip6If            *pIf6Entry = NULL;

    pIf6Entry = Lip6UtlGetIfEntry (u4IfIndex);
    if (pIf6Entry == NULL)
    {
        return NULL;
    }

    pPrefix = Lip6PrefixGetEntry (u4IfIndex, pIp6Addr, i4PrefixLen);

    /* If current prefix entry is null, retrieve the first valid entry
     * for that interface */
    pNextPrefix = (tLip6PrefixNode *)
        TMO_SLL_Next ((&pIf6Entry->PrefixList), &(pPrefix->PrefixNode));

    if (pNextPrefix != NULL)
    {
        return pNextPrefix;
    }

    return NULL;
}

/******************************************************************************
 * DESCRIPTION : Deletes all prefices configured on the interface and updates
 *               radvd
 *
 * INPUTS      : pIf6Entry - Pointer to Interface structure
 *
 * OUTPUTS     : None
 *
 * RETURNS     : OSIX_SUCCESS/FAILURE
 ******************************************************************************/
PUBLIC INT4
Lip6PrefixDeleteAll (tLip6If * pIf6Entry)
{
    tLip6PrefixNode    *pIp6PrefixNode = NULL;
    INT4                i4RetVal = OSIX_SUCCESS;

    /* Delete all entries in Prefix List */
    pIp6PrefixNode = (tLip6PrefixNode *) TMO_SLL_First (&pIf6Entry->PrefixList);

    while (pIp6PrefixNode != NULL)
    {
        TMO_SLL_Delete (&pIf6Entry->PrefixList, &(pIp6PrefixNode->PrefixNode));
        MemReleaseMemBlock (gIp6GblInfo.PrefixListPoolId,
                            (UINT1 *) pIp6PrefixNode);
        pIp6PrefixNode =
            (tLip6PrefixNode *) TMO_SLL_First (&pIf6Entry->PrefixList);
    }

    /* Update RA after deleting the prefixes */
    if (pIf6Entry->u1RAdvStatus == IP6_IF_ROUT_ADV_ENABLED)
    {
        i4RetVal = Lip6RAConfig (pIf6Entry->u4ContextId);
    }

    return i4RetVal;
}

#endif /* _LIP6PREF_C */
/* END OF FILE */
