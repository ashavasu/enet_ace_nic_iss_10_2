/**************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: lip6lla.c,v 1.7 2015/03/09 13:09:20 siva Exp $
 *
 * Description: Link local address calculation and assignment routines
 *              for Linux IPv6
 ***************************************************************************/
#include "lip6inc.h"

#ifndef _LIP6LLA_C
#define _LIP6LLA_C

/******************************************************************************
 * DESCRIPTION : Get link-local address for the interface from Linux IP
 *
 * INPUTS      : The interface pointer (pIf6Entry)
 *
 * OUTPUTS     : None
 *
 * RETURNS     : OSIX_SUCCESS/OSIX_FAILURE
 ******************************************************************************/
PUBLIC INT4
Lip6LlaGetLinkLocalAddress (tLip6If * pIf6Entry, tIp6Addr * pIp6Addr,
                            UINT1 *pu1Status)
{
    tIp6Addr            Ip6Addr;
    UINT1               au1DevName[IP6_MAX_IF_NAME_LEN];
    FILE               *pFd = 0;
    INT4                i4Plen = 0;
    INT4                i4Scope = 0;
    INT4                i4DadStatus = 0;
    INT4                i4IfIndex = 0;
    UINT4                       u4ContextId = VCM_DEFAULT_CONTEXT;
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    tLnxVrfIfInfo *pLnxVrfIfInfo = NULL;
    tLnxVrfInfo        *pLnxVrfInfo = NULL;
#endif

#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)

    pLnxVrfIfInfo = LnxVrfIfInfoGet(pIf6Entry->au1IfName);
    if(pLnxVrfIfInfo != NULL)
    {
        u4ContextId = pLnxVrfIfInfo->u4ContextId;
    }
    if(u4ContextId != VCM_DEFAULT_CONTEXT)
    {
        pLnxVrfInfo = LnxVrfInfoGet (u4ContextId);
        if (pLnxVrfInfo == NULL)
        {
            return OSIX_FAILURE;
        }
    }

#endif


    MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));
    MEMSET (pIp6Addr, 0, sizeof (tIp6Addr));
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    if(Ip6UtilChangeCxt (u4ContextId, LNX_VRF_NON_DEFAULT_NS,
                pLnxVrfInfo->au1NameSpace) == (UINT4)NETIPV6_FAILURE)
    {
        return OSIX_FAILURE;
    }
#endif
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    system("cat /proc/net/if_inet6 > Local_if_inet6");
    pFd = FOPEN ("Local_if_inet6", "r");
#else
    pFd = FOPEN (LIP6_PROC_IFINET6, "r");
#endif
    if(pFd != NULL)
    {
        while (fscanf (pFd, "%08x%08x%08x%08x %x %02x %02x %02x %20s\n",
                    &Ip6Addr.u4_addr[0], &Ip6Addr.u4_addr[1],
                    &Ip6Addr.u4_addr[2], &Ip6Addr.u4_addr[3],
                    &i4IfIndex, &i4Plen, &i4Scope, &i4DadStatus,
                    au1DevName) != EOF)
        {
            if ((STRCMP (au1DevName, pIf6Entry->au1IfName) == 0) &&
                    (i4Scope == IPV6_ADDR_LINKLOCAL))
            {
                /* Entry Found */
                pIp6Addr->u4_addr[0] = OSIX_HTONL (Ip6Addr.u4_addr[0]);
                pIp6Addr->u4_addr[1] = OSIX_HTONL (Ip6Addr.u4_addr[1]);
                pIp6Addr->u4_addr[2] = OSIX_HTONL (Ip6Addr.u4_addr[2]);
                pIp6Addr->u4_addr[3] = OSIX_HTONL (Ip6Addr.u4_addr[3]);
                *pu1Status = (UINT1) i4DadStatus;
                fclose (pFd);
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
                if(Ip6UtilChangeCxt (u4ContextId, LNX_VRF_DEFAULT_NS,
                            (UINT1 *)LNX_VRF_DEFAULT_NS_PID) == (UINT4)NETIPV6_FAILURE)
                {
                    unlink("Local_if_inet6");
                    return OSIX_FAILURE;
                }
#endif

                unlink("Local_if_inet6");
                return OSIX_SUCCESS;
            }
        }
        fclose (pFd);
        unlink("Local_if_inet6");
    }
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    if(Ip6UtilChangeCxt (u4ContextId, LNX_VRF_DEFAULT_NS,
                (UINT1 *)LNX_VRF_DEFAULT_NS_PID) == (UINT4)NETIPV6_FAILURE)
    {
        return OSIX_FAILURE;
    }
#else
    UNUSED_PARAM(u4ContextId);
#endif
    return OSIX_FAILURE;
}

#endif /* _LIP6LLA_C */
/********************************* END OF FILE *******************************/
