/***************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: lip6if.c,v 1.30 2015/10/23 07:38:28 siva Exp $
 *
 * Description: Interface management routines for Linux IPv6
 ***************************************************************************/
#ifndef _LIP6IF_C
#define _LIP6IF_C
#include "lip6inc.h"

#ifdef NPAPI_WANTED
#include "nputil.h"
#endif
PRIVATE INT4        Lip6IfInitIfEntry (tLip6If * pIf6Entry, UINT1 *pTok,
                                       UINT1 u1Tokl);
PRIVATE INT1        Lip6IfCreate (UINT4 u4IfIndex,
                                  tLip6CfaParams * pIp6CfaParams,
                                  UINT1 u1Status);
PRIVATE INT1        Lip6IfDelete (tLip6If * pIf6Entry);
PRIVATE tLip6If    *Lip6IfAllocMem (UINT4 u4IfIndex);
PRIVATE INT4        Lip6IfHandleOperUp (tLip6If * pIf6Entry);
PRIVATE INT4        Lip6IfHandleOperDown (tLip6If * pIf6Entry);
PRIVATE INT4        Lip6IfCreateTunnel (tLip6If * pIf6Entry);

/******************************************************************************
 * FUNCTION    : Lip6IfUpdateInterface
 * DESCRIPTION : Creates/deletes interface information in IPv6 and updates
 *               interface details
 * INPUTS      : pIp6CfaParams - Pointer to interface information
 * OUTPUTS     : None
 * RETURNS     : None
******************************************************************************/
PUBLIC INT4
Lip6IfUpdateInterface (tLip6CfaParams * pIp6CfaParams)
{
    tLip6If            *pIf6Entry = NULL;
    UINT4               u4IfIndex = 0;
    UINT1               u1Status = 0;
    UINT1               u1PrevStatus = 0;
    INT1                i1RetVal = 0;

    LIP6_TRC_ARG2 (LIP6_MOD_TRC, CONTROL_PLANE_TRC,
                   "Lip6ControlMsg: Rcvd msg from CFA, Type= 0x%X "
                   "IfType= 0x%X ",
                   pIp6CfaParams->u1MsgType, pIp6CfaParams->u1IfType);

    u4IfIndex = pIp6CfaParams->u4IfIndex;
    pIf6Entry = Lip6UtlGetIfEntry (u4IfIndex);

    switch (pIp6CfaParams->u1MsgType)
    {
        case IP6_LANIF_ENTRY_UP:
        case IP6_LANIF_ENTRY_VALID:
            /* Lower layer Interface is administratively enabled. But
             * interface can be operationally UP or DOWN. */
            if (pIp6CfaParams->u4Status == IP6_LANIF_OPER_UP)
            {
                u1Status = NETIPV6_OPER_UP;
            }
            else
            {
                u1Status = NETIPV6_OPER_DOWN;
            }

            if (pIf6Entry == NULL)
            {
                /* Lower layer indicates a new interface is created and there
                 * is no matching interface in IP6. So create Ip6 logical
                 * interface */
                i1RetVal = (INT1) Lip6IfCreate (u4IfIndex,
                                                pIp6CfaParams, u1Status);

                if (i1RetVal != OSIX_SUCCESS)
                {
                    LIP6_TRC_ARG1 (LIP6_MOD_TRC,
                                   CONTROL_PLANE_TRC,
                                   "Lip6IfUpdateInterface: Interface [%d] "
                                   "creation FAILED\r\n", u4IfIndex);
                    return OSIX_FAILURE;
                }

                if (u4IfIndex == CFA_DEFAULT_ROUTER_VLAN_IFINDEX)
                {
                    pIf6Entry = Lip6UtlGetIfEntry (u4IfIndex);

                    if (Lip6IfEnable (pIf6Entry) == OSIX_FAILURE)
                    {
                        LIP6_TRC_ARG1 (LIP6_MOD_TRC,
                                       CONTROL_PLANE_TRC,
                                       "Lip6IfUpdateInterface: Default Interface"
                                       " [%d] enable FAILED\r\n", u4IfIndex);
                        return OSIX_FAILURE;
                    }
                }
            }
            else
            {
                /* Valid Interface already exists. May be event describing
                 * some change in the status of the interface. */

                if (u1Status == pIf6Entry->u1OperStatus)
                {
                    /* No change in operation status or the IPv6 is not enabled
                     * over this interface. No need for any action. */
                    return OSIX_SUCCESS;
                }

                if (u1Status == NETIPV6_OPER_UP)
                {
                    /* Check if MTU of the interface is greater than minimum
                     * MTU and then make the interface oper up */
                    if (pIf6Entry->u4Mtu < LIP6_MIN_MTU)
                    {
                        /* Ignore event and return success */
                        return OSIX_SUCCESS;
                    }

                    if (pIf6Entry->u1AdminStatus == NETIPV6_ADMIN_DOWN)
                    {
                        if (pIf6Entry->u1IfType == IP6_TUNNEL_INTERFACE_TYPE)
                        {
                            return OSIX_SUCCESS;
                        }
                        /* If Ipv6 is disabled on the interface and
                         * oper status changes as UP, disable IPv6 in Linux
                         * explicitly since it gets enabled automatically when
                         * interface becomes up and return */
                        if (Lip6KernSetIfForwarding (pIf6Entry->au1IfName,
                                                     LIP6_FORW_DISABLE) ==
                            OSIX_FAILURE)
                        {
                            LIP6_TRC_ARG (LIP6_MOD_TRC, CONTROL_PLANE_TRC,
                                          "Lip6IfUpdateInterface: Could not disable "
                                          "IPv6 in Linux\r\n");
                            return OSIX_FAILURE;
                        }
                        pIf6Entry->u1Ipv6IfFwdOperStatus = LIP6_FORW_DISABLE;
                        return OSIX_SUCCESS;
                    }
                }
                Ip6Lock ();
                u1PrevStatus = pIf6Entry->u1OperStatus;
                pIf6Entry->u1OperStatus = u1Status;

                /* IPv6 is enabled over this interface and Lower Layer indicates
                 * change in Operational Status. */
                if (u1Status == NETIPV6_OPER_UP)
                {
                    /* Fix for Bug# 4529.Introduce a task delay of 80 ticks
                     * in order to
                     * get the link local address configured 
                     * in the kernel when interface is made up */
                    OsixTskDelay (80);
                    if (Lip6IfHandleOperUp (pIf6Entry) == OSIX_FAILURE)
                    {
                        pIf6Entry->u1OperStatus = u1PrevStatus;
                        Ip6UnLock ();
                        return OSIX_FAILURE;
                    }
                }
                else
                {
                    if (Lip6IfHandleOperDown (pIf6Entry) == OSIX_FAILURE)
                    {
                        pIf6Entry->u1OperStatus = u1PrevStatus;
                        Ip6UnLock ();
                        return OSIX_FAILURE;
                    }
                }
                pIf6Entry->u1OperStatus = u1Status;

                if (pIf6Entry->u1IfType != IP6_TUNNEL_INTERFACE_TYPE)
                {
                    /* Update RA after deleting the prefixes */
                    if (pIf6Entry->u1RAdvStatus == IP6_IF_ROUT_ADV_ENABLED)
                    {
                        Lip6RAConfig (pIf6Entry->u4ContextId);
                    }
                }

            }
            Ip6UnLock ();
            break;

        case IP6_LANIF_ENTRY_INVALID:
            /* Lower layer Interface is administratively deleted. */
            if (pIf6Entry == NULL)
            {
                return OSIX_FAILURE;
            }

            /*  Indicating the Higher layer modules change */
            if (pIf6Entry->u1OperStatus == NETIPV6_IF_DELETE)
            {
                NetIpv6InvokeInterfaceStatusChange (u4IfIndex,
                                                    NETIPV6_ALL_PROTO,
                                                    NETIPV6_IF_DELETE,
                                                    NETIPV6_INTERFACE_STATUS_CHANGE);
            }

            Lip6IfDelete (pIf6Entry);
            break;

        case IP6_LANIF_MTU_CHANGE:
            /* Interface MTU Change has triggered a change in Interface
             * Operational Status */
            if (pIf6Entry == NULL)
            {
                /* No matching valid interface available in IP6. */
                return OSIX_FAILURE;
            }

            u1Status = pIf6Entry->u1OperStatus;
            Lip6PortSetIfOper (pIf6Entry);

            /*  Higher layer modules need not be indicated when there is no
             *  change */
            if (u1Status != pIf6Entry->u1OperStatus)
            {
                if (pIf6Entry->u1OperStatus != NETIPV6_OPER_DOWN)
                {
                    NetIpv6InvokeInterfaceStatusChange (u4IfIndex,
                                                        NETIPV6_ALL_PROTO,
                                                        NETIPV6_IF_DOWN,
                                                        NETIPV6_INTERFACE_STATUS_CHANGE);
                }
                else
                {
                    NetIpv6InvokeInterfaceStatusChange (u4IfIndex,
                                                        NETIPV6_ALL_PROTO,
                                                        NETIPV6_IF_UP,
                                                        NETIPV6_INTERFACE_STATUS_CHANGE);
                }
            }
            break;

        default:
            LIP6_TRC_ARG1 (LIP6_MOD_TRC,
                           CONTROL_PLANE_TRC,
                           "Lip6IfUpdateInterface: Invalid message 0x%X from "
                           "CFA\r\n", pIp6CfaParams->u1MsgType);
            break;
    }
    return OSIX_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : This function is called from Lower Layer to intimate any
 *               changes in mtu and Speed.
 *
 * INPUTS      : u4IfIndex   - Interface Index
 *               u4Mtu       - MTU of the interface
 *               u4Speed     - Speed in bps
 *               u4HighSpeed - Speed in mbps
 *
 * OUTPUTS     : None
 *
 * RETURNS     : OSIX_SUCCESS/OSIX_FAILURE
 *
 *
 * NOTES       :
 ******************************************************************************/
PUBLIC INT4
Lip6IfUpdateSpeedMtu (UINT4 u4IfIndex, UINT4 u4Mtu, UINT4 u4Speed,
                      UINT4 u4HighSpeed)
{
    tLip6If            *pIf6Entry = NULL;
    UINT4              u4ContextId = VCM_DEFAULT_CONTEXT;

#if defined(VRF_WANTED) && defined(LINUX_310_WANTED)
    Ip6GetCxtId (u4IfIndex, &u4ContextId);
#endif   

    if( u4ContextId == VCM_DEFAULT_CONTEXT )
    {

	    if (gIp6GblInfo.i4Ip6Status == OSIX_FALSE)
	    {
		    LIP6_TRC_ARG (LIP6_MOD_TRC, CONTROL_PLANE_TRC,
				    "Ip6LanifUpdate: IP6 module Not Initialised !\r\n");
		    return OSIX_FAILURE;
	    }
    }
    else
    {
	    if (gIp6GblInfo.apIp6Cxt[u4ContextId] != NULL)
	    {
		    if ( (gIp6GblInfo.apIp6Cxt[u4ContextId])->u4ForwFlag == OSIX_FALSE)
		    {
			    LIP6_TRC_ARG (LIP6_MOD_TRC, CONTROL_PLANE_TRC,
					    "Ip6LanifUpdate: IP6 module Not Initialised !\r\n");
			    return OSIX_FAILURE;
		    }
	    }
    }

    pIf6Entry = Lip6UtlGetIfEntry (u4IfIndex);
    if (pIf6Entry == NULL)
    {
        return OSIX_FAILURE;
    }

    if (u4Mtu != pIf6Entry->u4Mtu)
    {
        /* Ip must have updated MTU, so just maintain in local data structure
         * and no need to update to kernel */
        if (!((pIf6Entry->u1IfType == IP6_LOOPBACK_INTERFACE_TYPE) &&
              (u4Mtu == 0)))
        {
            pIf6Entry->u4Mtu = u4Mtu;
            NetIpv6InvokeInterfaceStatusChange (pIf6Entry->u4IfIndex,
                                                NETIPV6_ALL_PROTO,
                                                pIf6Entry->u4Mtu,
                                                NETIPV6_INTERFACE_MTU_CHANGE);
        }
    }

    /* Indicates change in Speed Value. */
    if ((u4Speed != pIf6Entry->u4IfSpeed) && (u4HighSpeed == 0))
    {
        pIf6Entry->u4IfSpeed = u4Speed;
        NetIpv6InformSpeedChange (u4IfIndex, u4Speed);
    }

    if ((u4HighSpeed != pIf6Entry->u4IfHighSpeed)
        && (u4Speed == LIP6_MAX_VALUE))
    {
        pIf6Entry->u4IfHighSpeed = u4HighSpeed;
        NetIpv6InformHighSpeedChange (u4IfIndex, u4HighSpeed);
    }
    return OSIX_SUCCESS;
}

/******************************************************************************
 * FUNCTION    : Lip6IfCreate
 * DESCRIPTION : This routine is called to create an IPv6 logical interface.
 *               The routine is called from SNMP upon SET on an IPv6 interface
 *               to create it. The SNMP routines would have already checked the
 *               validity of the various parameters.
 *               This routine will initialize the parameters of the IPv6
 *               interface by calling other procedures. Registered tasks will be
 *               notified of the interface creation. If the interface is Admin
 *               UP and if corresponding data-link layer is operational, a
 *               routine will be called to take suitable actions to start the
 *               DAD process [RFC 1971] if required.
 *
 * INPUTS      : The index of the interface (u2Index), type of the interface
 *               (u1Type), interface number and circuit number (u1If and
 *               u2Ckt), Oper Status (u1Status), Interface token and token
 *               length if configured (pTok and u1Tokl)
 *
 * OUTPUTS     : None
 *
 * RETURNS     : OSIX_SUCCESS/OSIX_FAILURE
 ******************************************************************************/
PRIVATE INT1
Lip6IfCreate (UINT4 u4IfIndex, tLip6CfaParams * pIp6CfaParams, UINT1 u1Status)
{
    tLip6If            *pIf6Entry = NULL;

    UNUSED_PARAM (u1Status);
    pIf6Entry = Lip6IfAllocMem (u4IfIndex);

    if (pIf6Entry == NULL)
    {
        return OSIX_FAILURE;
    }

#ifdef TUNNEL_WANTED
    pIf6Entry->pTunlIf = NULL;

    if (pIp6CfaParams->u1IfType == IP6_TUNNEL_INTERFACE_TYPE)
    {
        /* Initialize Tunnel If Data structures */
        if (Lip6IfCreateTunnel (pIf6Entry) == OSIX_FAILURE)
        {
            /* Release interface memory */
            MemReleaseMemBlock (gIp6GblInfo.Ip6IfPoolId, (UINT1 *) pIf6Entry);
            return OSIX_FAILURE;
        }
    }
#endif /* TUNNEL_WANTED */

    TMO_SLL_Init (&(pIf6Entry->Ip6AddrList));
    TMO_SLL_Init (&(pIf6Entry->PrefixList));
    TMO_SLL_Init (&(pIf6Entry->Ip6LLAddrList));
    TMO_SLL_Init (&(pIf6Entry->mcastIlist));

    SNPRINTF ((CHR1 *) pIf6Entry->au1IfName, IP6_MAX_IF_NAME_LEN, "%s",
              pIp6CfaParams->au1IfName);
    pIf6Entry->u4IfIndex = u4IfIndex;
    pIf6Entry->u1IfType = pIp6CfaParams->u1IfType;
    pIf6Entry->u2CktIndex = pIp6CfaParams->u2CktIndex;
    pIf6Entry->u1TokLen = 0;
    MEMSET (pIf6Entry->ifaceTok, 0, IP6_EUI_ADDRESS_LEN);

    pIf6Entry->u4HopLimit = IP6_IF_DEF_HOPLMT;
    pIf6Entry->u4DadAttempts = IP6_IF_DEF_DAD_SEND;
    pIf6Entry->u1PrefAdv = IP6_IF_PREFIX_ADV;
    pIf6Entry->u4MaxRaTime = IP6_IF_DEF_MAX_RA_TIME;
    pIf6Entry->u4MinRaTime = IP6_IF_DEF_MIN_RA_TIME;
    pIf6Entry->u4DefRaLifetime = IP6_RA_DEF_DEFTIME;
    pIf6Entry->u1RAdvStatus = IP6_IF_ROUT_ADV_DISABLED;
    pIf6Entry->u1RAAdvFlags = IP6_IF_NO_RA_ADV;
    pIf6Entry->u4RaMtu = IP6_RA_DEF_LINK_MTU;
    pIf6Entry->u4RACurHopLimit = IP6_IF_DEF_HOPLMT;
    pIf6Entry->u4Mtu = IP6_DEFAULT_MTU;
    pIf6Entry->u4RARetransTimer = IP6_IF_DEF_RETTIME;
    pIf6Entry->u4RAReachableTime = IP6_IF_DEF_REACHTIME;
    pIf6Entry->u4IfSpeed = pIp6CfaParams->u4Speed;
    pIf6Entry->u4IfHighSpeed = pIp6CfaParams->u4HighSpeed;
    pIf6Entry->u4IpPort = CfaGetIfIpPort ((UINT2) u4IfIndex);
    pIf6Entry->u4ContextId = VCM_DEFAULT_CONTEXT;
    pIf6Entry->Icmp6ErrRLInfo.i4Icmp6DstUnReachable =
        ICMP6_DEST_UNREACHABLE_ENABLE;
    pIf6Entry->Icmp6ErrRLInfo.i4Icmp6ErrInterval = ICMP6_DEF_ERR_INETRVAL;
    pIf6Entry->Icmp6ErrRLInfo.i4Icmp6BucketSize = ICMP6_DEF_BUCKET_SIZE;
    pIf6Entry->Icmp6ErrRLInfo.i4Icmp6RedirectMsg = ICMP6_REDIRECT_DISABLE;
    pIf6Entry->u1RALinkLocalStatus = IP6_RA_NO_ADV_LINKLOCAL;
    pIf6Entry->u1RAInterval = IP6_RA_NO_ADV_INTERVAL;

    /* IPv6 status is default disabled when interface is created */
    pIf6Entry->u1AdminStatus = NETIPV6_ADMIN_DOWN;
    pIf6Entry->u1OperStatus = NETIPV6_OPER_DOWN;
    if (Lip6KernSetIfDad (pIf6Entry->au1IfName, LIP6_DAD_ENABLE)
        != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    if (Lip6KernSetIfForwarding (pIf6Entry->au1IfName, LIP6_FORW_ENABLE)
        != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    pIf6Entry->u1Ipv6IfFwdOperStatus = LIP6_FORW_ENABLE;

    return OSIX_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : This routine is called to delete an IPv6 logical interface.
 *               The routine is called from SNMP upon SET on an IPv6 interface
 *               to delete it. The SNMP routines would have already checked
 *               that the interface is present.
 *               The routine will inform ND6 Module about the deletion of the
 *               interface, post an event to RIP6 to notify the interface
 *               deletion and take action for all addresses on the interface.
 *               The link-local address will be deleted, the interface removed
 *               from the hash table and marked as not in use.
 *
 * INPUTS      : The index of the interface (u4IfIndex)
 *
 * OUTPUTS     : None
 *
 * RETURNS     : OSIX_SUCCESS/OSIX_FAILURE
 *
 * NOTES       :
 ******************************************************************************/
PRIVATE INT1
Lip6IfDelete (tLip6If * pIf6Entry)
{
    UINT4               u4ArrayIndex = 0;

    /* Since interface in Linux Kernel must already have been deleted by netip
     * module, no need to disable IPv6 or routes explicitly in linux.
     * Just update lnxip6 data structures alone */
    LIP6_TRC_ARG1 (LIP6_MOD_TRC, CONTROL_PLANE_TRC,
                   "Lip6IfDelete:Deleting IP6 IF = %d\r\n",
                   pIf6Entry->u4IfIndex);

    /* Indicate RTM to delete all routes learnt over this interface */
    IP6_TASK_UNLOCK ();
    Rtm6ApiActOnIpv6IfChange (RTM6_SCAN_INTF_DEL, pIf6Entry->u4IfIndex);
    IP6_TASK_LOCK ();

    if (Lip6AddrDeleteAll (pIf6Entry) == OSIX_FAILURE)
    {
        LIP6_TRC_ARG (LIP6_MOD_TRC, CONTROL_PLANE_TRC,
                      "Lip6IfDelete:Delete all address failed\r\n");
        return OSIX_FAILURE;
    }

    if (Lip6PrefixDeleteAll (pIf6Entry) == OSIX_FAILURE)
    {
        LIP6_TRC_ARG (LIP6_MOD_TRC, CONTROL_PLANE_TRC,
                      "Lip6IfDelete:Delete all prefix failed\r\n");
        return OSIX_FAILURE;
    }

#ifdef TUNNEL_WANTED
    /* In case of tunnel interface take care to delete 
     * appropriate data structures.*/
    if (pIf6Entry->u1IfType == IP6_TUNNEL_INTERFACE_TYPE)
    {
        if ((pIf6Entry->u1AdminStatus == NETIPV6_ADMIN_UP) &&
            (pIf6Entry->pTunlIf->u1TunlType == IPV6_SIX_TO_FOUR))
        {
            /* Disable tunnel in Linux */
            if (Lip6TunnelDisable (pIf6Entry) == OSIX_FAILURE)
            {
                return OSIX_FAILURE;
            }

            /* Delete tunnel in Linux */
            if (Lip6TunnelDelete (pIf6Entry) == OSIX_FAILURE)
            {
                return OSIX_FAILURE;
            }
        }
        /* Free Memory for tunnel interface */
        if (MemReleaseMemBlock (gIp6GblInfo.Ip6TunlIfPoolId,
                                (UINT1 *) pIf6Entry->pTunlIf) == MEM_FAILURE)
        {
            LIP6_TRC_ARG (LIP6_MOD_TRC, CONTROL_PLANE_TRC,
                          "Lip6IfDelete:MemReleaseMemBlock failed for "
                          "TunlIfEntry\r\n");
            return OSIX_FAILURE;
        }
    }
#endif /* TUNNEL_WANTED */

    /* Free interface structure memory */
    if (MemReleaseMemBlock (gIp6GblInfo.Ip6IfPoolId, (UINT1 *) pIf6Entry) ==
        MEM_FAILURE)
    {
        LIP6_TRC_ARG (LIP6_MOD_TRC, CONTROL_PLANE_TRC,
                      "Lip6IfDelete:MemReleaseMemBlock failed for IfEntry\r\n");
        return OSIX_FAILURE;
    }

    u4ArrayIndex = pIf6Entry->u4IfIndex;

    gIp6GblInfo.apIp6If[u4ArrayIndex] = NULL;

    return OSIX_SUCCESS;
}

/******************************************************************************
 * FUNCTION    : Lip6IfDisable
 * DESCRIPTION : This routine is called when IPv6 has been disabled on
 *               an existing IPv6 logical interface.
 *               The routine will make the Admin Status and Operationa Status
 *               as DOWN and then perform other actions such as inform ND6
 *               Module and higher layer about the disabling of the interface,
 *               stopping the timers for the addresses and so on.
 *
 * INPUTS      : The index of the interface (u4IfIndex)
 *
 * OUTPUTS     : None
 *
 * RETURNS     : OSIX_SUCCESS/OSIX_FAILURE
 *
 * NOTES       : Admin Status is set to DOWN in the low level routine, before
                 this routine is called
 ******************************************************************************/
PUBLIC INT1
Lip6IfDisable (tLip6If * pIf6Entry)
{
    UINT1           u1Status = FALSE;
    LIP6_TRC_ARG1 (LIP6_MOD_TRC,
                   CONTROL_PLANE_TRC, "Lip6IfDisable:Disabling IP6 IF = %d\r\n",
                   pIf6Entry->u4IfIndex);

#ifdef TUNNEL_WANTED
    if (pIf6Entry->u1IfType == IP6_TUNNEL_INTERFACE_TYPE)
    {
        if (pIf6Entry->pTunlIf->u1TunlType != IPV6_SIX_TO_FOUR)
        {
            LIP6_TRC_ARG1 (LIP6_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                           "Lip6IfDisable: Tunnel Type [%d] "
                           "Not Supported\n", pIf6Entry->pTunlIf->u1TunlType);
            return OSIX_FAILURE;
        }

        /* Disable tunnel in Linux */
        if (Lip6TunnelDisable (pIf6Entry) == OSIX_FAILURE)
        {
            return OSIX_FAILURE;
        }
    }
#endif /* TUNNEL_WANTED */

    if(pIf6Entry->u1OperStatus == NETIPV6_OPER_DOWN)
    {
        u1Status = TRUE;
    }
    pIf6Entry->u1OperStatus = NETIPV6_OPER_DOWN;
    pIf6Entry->u1AdminStatus = NETIPV6_ADMIN_DOWN;

    /* Disable IPv6 forwarding for the interface */
    if (Lip6KernSetIfForwarding (pIf6Entry->au1IfName, LIP6_FORW_DISABLE) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    pIf6Entry->u1Ipv6IfFwdOperStatus = LIP6_FORW_DISABLE;

#ifdef TUNNEL_WANTED
    if (pIf6Entry->u1IfType == IP6_TUNNEL_INTERFACE_TYPE)
    {
        /* Delete tunnel in Linux */
        if (Lip6TunnelDelete (pIf6Entry) == OSIX_FAILURE)
        {
            return OSIX_FAILURE;
        }
    }
#endif

    Lip6NdHandleIfStateChange (pIf6Entry);

    /* Indicate the Higher layer that the interface is deleted to the
     * higher layer. */
    NetIpv6InvokeInterfaceStatusChange (pIf6Entry->u4IfIndex,
                                        NETIPV6_ALL_PROTO,
                                        NETIPV6_IF_DOWN,
                                        NETIPV6_INTERFACE_STATUS_CHANGE);
    if (Lip6KernSetIfIpv6Disable (pIf6Entry->au1IfName, LIP6_IPV6_DISABLE) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if(u1Status == FALSE)
    {
        if (Lip6AddrDisableAll (pIf6Entry) == OSIX_FAILURE)
        {
            return OSIX_FAILURE;
        }
    }
    if (Lip6AddrDeleteLLAddrList (pIf6Entry) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Lip6AddrDeleteMcastList (pIf6Entry) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/******************************************************************************
 * FUNCTION    : Lip6IfEnable
 * DESCRIPTION : This routine is called when IPv6 is enabled on an Interface.
 *               Basically called when manager explicitly enable IPv6 on the
 *               interface or when an IPv6 address is assigned to an Interface
 *               whose status is DOWN.
 *               The routine will make the Admin as UP and check and form the
 *               link-local address. Then, if corresponding data-link layer is
 *               operational, a routine will be called to take suitable actions
 *               to start the DAD process if required.
 *
 * INPUTS      : The index of the interface (u4IfIndex)
 *
 * OUTPUTS     : None
 *
 * RETURNS     : OSIX_SUCCESS or
 *               OSIX_FAILURE if link-local address check failed
 *
 * NOTES       :
 ******************************************************************************/
PUBLIC INT1
Lip6IfEnable (tLip6If * pIf6Entry)
{
    tCfaIfInfo          CfaIfInfo;
    tIp6Addr            Ip6Addr;
	tLip6AddrNode      *pIp6AddrNode = NULL;
	tIp6Addr            McastIp6Addr;
    tLip6AddrNode      *pSearchNode = NULL;
    tTMO_SLL_NODE      *pPrevNode = NULL;
    BOOL1               b1IsNodePresent = OSIX_FALSE;
    UINT1               u1DadStatus = 0;

    MEMSET (&CfaIfInfo, 0, sizeof (tCfaIfInfo));

    LIP6_TRC_ARG1 (LIP6_MOD_TRC, CONTROL_PLANE_TRC,
                   "Lip6IfEnable:Enabling IP6 IF = %d\r\n",
                   pIf6Entry->u4IfIndex);

    pIf6Entry->u1AdminStatus = NETIPV6_ADMIN_UP;

#ifdef TUNNEL_WANTED
    if (pIf6Entry->u1IfType == IP6_TUNNEL_INTERFACE_TYPE)
    {
        /* Initialise Tunnel interface parameters */
        if (Lip6PortGetTunnelParams (pIf6Entry) == OSIX_FAILURE)
        {
            /* Initialising Tunnel Interface Fails. */
            LIP6_TRC_ARG1 (LIP6_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                           "Lip6IfEnable: Tunnel Interface [%d] "
                           "Initialization FAILED\n", pIf6Entry->u4IfIndex);
            return OSIX_FAILURE;
        }

        if (pIf6Entry->pTunlIf->u1TunlType != IPV6_SIX_TO_FOUR)
        {
            LIP6_TRC_ARG1 (LIP6_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                           "Lip6IfEnable: Tunnel Type [%d] "
                           "Not Supported\n", pIf6Entry->pTunlIf->u1TunlType);
            return OSIX_FAILURE;
        }

        /* Create tunnel in Linux. Only here we will know the tunnel mode and
         * source ip needed for tunnel creation */
        if (Lip6TunnelCreate (pIf6Entry) == OSIX_FAILURE)
        {
            return OSIX_FAILURE;
        }

        /* Enable tunnel in Linux */
        if (Lip6TunnelEnable (pIf6Entry) == OSIX_FAILURE)
        {
            return OSIX_FAILURE;
        }
    }
#endif /* TUNNEL_WANTED */

    /* Get link-local address from linux */
    Lip6LlaGetLinkLocalAddress (pIf6Entry, &Ip6Addr, &u1DadStatus);

    /* Scan and insert in ascending order */
    TMO_SLL_Scan (&(pIf6Entry->Ip6LLAddrList), pSearchNode, tLip6AddrNode *)
    {
        /* This utility description is not correct. It returns IP6_SUCCESS
         * when 2nd address is greater */
        if (Ip6IsAddrGreater (&Ip6Addr, &(pSearchNode->Ip6Addr)) == IP6_SUCCESS)
        {
            /* Insert before pSearchNode */
            break;
        }
        if (Ip6AddrCompare (Ip6Addr, pSearchNode->Ip6Addr) == IP6_ZERO)
        {
            b1IsNodePresent = OSIX_TRUE;
            break;
        }
        pPrevNode = &(pSearchNode->SllNode);
    }

    if ((b1IsNodePresent != OSIX_TRUE)
        && (IS_ADDR_LLOCAL (Ip6Addr) == OSIX_TRUE))
    {
        /* Add address to list */
        if ((pIp6AddrNode =
             (tLip6AddrNode *) MemAllocMemBlk (gIp6GblInfo.AddrLstPoolId)) ==
            NULL)
        {
            LIP6_TRC_ARG (LIP6_MOD_TRC, ALL_FAILURE_TRC,
                          "Mem alloc for tLip6AddrNode Failed!\n");
            return OSIX_FAILURE;
        }

        MEMCPY (&(pIp6AddrNode->Ip6Addr), &Ip6Addr, sizeof (tIp6Addr));
        pIp6AddrNode->i4PrefixLen = IP6_ADDR_LL_PREFIX_LEN;
        pIp6AddrNode->u1AdminStatus = IP6FWD_ACTIVE;    /* Set it to active since linux
                                                           has already configured this */
        pIp6AddrNode->u1AddrType = ADDR6_LLOCAL;
        pIp6AddrNode->u1Status = u1DadStatus;

        TMO_SLL_Insert (&(pIf6Entry->Ip6LLAddrList), pPrevNode,
                        &(pIp6AddrNode->SllNode));

        NetIpv6InvokeAddressChange (&Ip6Addr,
                                   IP6_ADDR_MAX_PREFIX, ADDR6_LLOCAL,
                                   pIf6Entry->u4IfIndex, NETIPV6_ADDRESS_ADD);


    }

    if (Lip6PortSetIfOper (pIf6Entry) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if ((u1DadStatus & IFA_F_TENTATIVE) == IFA_F_TENTATIVE)
    {
        NetIpv6InvokeInterfaceStatusChange (pIf6Entry->u4IfIndex,
                                            NETIPV6_ALL_PROTO,
                                            NETIPV6_IF_DOWN,
                                            NETIPV6_INTERFACE_STATUS_CHANGE);
        pIf6Entry->u1OperStatus = NETIPV6_OPER_DOWN;

    }

    if (Lip6KernSetIfIpv6Disable (pIf6Entry->au1IfName, LIP6_IPV6_ENABLE) ==                                                                       OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (pIf6Entry->u1OperStatus == NETIPV6_OPER_UP)
    {
        /* Program configured Ipv6 addresses to kernel */
        if (Lip6AddrEnableAll (pIf6Entry) == OSIX_FAILURE)
        {
            return OSIX_FAILURE;
        }
        if (Lip6KernSetIfForwarding (pIf6Entry->au1IfName, LIP6_FORW_ENABLE)
            != OSIX_SUCCESS)
        {
            return OSIX_FAILURE;
        }
		pIf6Entry->u1Ipv6IfFwdOperStatus = LIP6_FORW_ENABLE;
		Lip6LlaGetLinkLocalAddress (pIf6Entry, &Ip6Addr, &u1DadStatus);

		if (((u1DadStatus & IFA_F_TENTATIVE) != IFA_F_TENTATIVE) && (u1DadStatus != 0))
		{
			if (IS_ADDR_LLOCAL (Ip6Addr) == OSIX_TRUE)
			{
				Lip6AddrCreateSolicitMcast (pIf6Entry->u4IfIndex, &Ip6Addr);
			}
			SET_ALL_NODES_MULTI (McastIp6Addr);
			Lip6AddrCreateNewMcastAddr (pIf6Entry->u4IfIndex, &McastIp6Addr);
			if (pIf6Entry->u1Ipv6IfFwdOperStatus == IP6_IF_FORW_ENABLE)
			{
				/* Routing is enabled. Ensure that ALL ROUTER Multi-Cast address
				 *          * is disabled for this interface. */
				SET_ALL_ROUTERS_MULTI (McastIp6Addr);
				Lip6AddrCreateNewMcastAddr (pIf6Entry->u4IfIndex, &McastIp6Addr);
			}

		}

		Lip6NdHandleIfStateChange (pIf6Entry);

        /* Indicate to the Higher layer that an interface is UP */
        NetIpv6InvokeInterfaceStatusChange (pIf6Entry->u4IfIndex,
                                            NETIPV6_ALL_PROTO,
                                            NETIPV6_IF_UP,
                                            NETIPV6_INTERFACE_STATUS_CHANGE);
    }
    else
    {
        pIf6Entry->u1Ipv6IfFwdOperStatus = LIP6_FORW_DISABLE;

        /* Program configured Ipv6 addresses to kernel */
        if (Lip6AddrEnableAll (pIf6Entry) == OSIX_FAILURE)
        {
            return OSIX_FAILURE;
        }
        pIf6Entry->u1Ipv6IfFwdOperStatus = LIP6_FORW_ENABLE;
        Lip6NdHandleIfStateChange (pIf6Entry);

        /* Indicate to the Higher layer that an interface is enabled
         * but the status is DOWN. */
        NetIpv6InvokeInterfaceStatusChange (pIf6Entry->u4IfIndex,
                                            NETIPV6_ALL_PROTO,
                                            NETIPV6_IF_DOWN,
                                            NETIPV6_INTERFACE_STATUS_CHANGE);
    }

#if defined (NPAPI_WANTED) && defined (TUNNEL_WANTED)
    if (pIf6Entry->u1IfType == IP6_TUNNEL_INTERFACE_TYPE)
    {
        Ipv6FsNpIpv6TunlParamSet (pIf6Entry->u4ContextId, pIf6Entry->u4IfIndex,
                                  pIf6Entry->pTunlIf->u1TunlType,
                                  OSIX_HTONL ((pIf6Entry->pTunlIf->tunlSrc).
                                              u4_addr[3]),
                                  OSIX_HTONL ((pIf6Entry->pTunlIf->tunlDst).
                                              u4_addr[3]));
    }
#endif /* NPAPI_WANTED && TUNNEL_WANTED */

    /* Get link-local address from linux */
    u1DadStatus = 0;
    Lip6LlaGetLinkLocalAddress (pIf6Entry, &Ip6Addr, &u1DadStatus);

    if (Lip6IfInitIfEntry
        (pIf6Entry, &(Ip6Addr.u1_addr[8]),
         IP6_ADDR_LL_PREFIX_LEN) != OSIX_SUCCESS)
    {
        pIf6Entry->u1AdminStatus = NETIPV6_ADMIN_DOWN;
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : Allocate Memory for the interface structure
 *
 * INPUTS      : u4IfIndex
 *
 * OUTPUTS     : NONE.
 *
 * RETURNS     : OSIX_SUCCESS/OSIX_FAILURE
 ******************************************************************************/
PRIVATE tLip6If    *
Lip6IfAllocMem (UINT4 u4IfIndex)
{
    UINT4               u4ArrayIndex = 0;

    u4ArrayIndex = u4IfIndex;
    /* If the derived index is more than max, return NULL */
    if (u4ArrayIndex >= LIP6_MAX_INTERFACES)
    {
        return NULL;
    }

    if ((gIp6GblInfo.apIp6If[u4ArrayIndex] =
         (tLip6If *) MemAllocMemBlk (gIp6GblInfo.Ip6IfPoolId)) == NULL)
    {
        LIP6_TRC_ARG (LIP6_MOD_TRC, ALL_FAILURE_TRC,
                      "Mem alloc for tLip6If Failed!\r\n");
        return NULL;
    }

    MEMSET (gIp6GblInfo.apIp6If[u4ArrayIndex], 0, sizeof (tLip6If));

    return gIp6GblInfo.apIp6If[u4ArrayIndex];
}

/******************************************************************************
 * FUNCTION    : Lip6IfInitIfEntry
 * DESCRIPTION : This routine initializes the IPv6 interface upon enabling.
 *               It forms the link-local address, adds to Hash table etc. based
 *               on the type of the interface.
 *
 * INPUTS      : The interface pointer (pIf6Entry), the interface token (pTok)
 *               and the token length (u1Tokl). Note that the token can be
 *               NULL.
 *
 * OUTPUTS     : None
 *
 * RETURNS     : OSIX_SUCCESS/OSIX_FAILURE
 ******************************************************************************/
PRIVATE INT4
Lip6IfInitIfEntry (tLip6If * pIf6Entry, UINT1 *pTok, UINT1 u1Tokl)
{
    switch (pIf6Entry->u1IfType)
    {

#ifdef TUNNEL_WANTED
        case IP6_TUNNEL_INTERFACE_TYPE:
            break;
#endif /* TUNNEL_WANTED */

        case IP6_PPP_INTERFACE_TYPE:
            break;

        case IP6_ENET_INTERFACE_TYPE:
        case IP6_L3VLAN_INTERFACE_TYPE:
        case IP6_LAGG_INTERFACE_TYPE:
        case IP6_X25_INTERFACE_TYPE:
        case IP6_FR_INTERFACE_TYPE:
        case IP6_LOOPBACK_INTERFACE_TYPE:
            if (u1Tokl != 0)
            {
                u1Tokl = MEM_MAX_BYTES (u1Tokl, IP6_EUI_ADDRESS_LEN);
                pIf6Entry->u1TokLen = u1Tokl;
                MEMCPY (pIf6Entry->ifaceTok, pTok, u1Tokl);

            }
            break;

        default:
            LIP6_TRC_ARG1 (LIP6_MOD_TRC,
                           CONTROL_PLANE_TRC,
                           "Ip6ifInitialize Unknown Type %d\r\n",
                           pIf6Entry->u1IfType);
            return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;

}

/******************************************************************************
 * FUNCTION    : Lip6IfHandleOperUp
 * DESCRIPTION : This routine enables ipv6 in linux, programs configured 
 *               addresses and routes to linux, indicates higher layer
 *               modules
 *
 * INPUTS      : pIf6Entry - Pointer to Interface info
 *
 * OUTPUTS     : None
 *
 * RETURNS     : OSIX_SUCCESS/OSIX_FAILURE
 ******************************************************************************/
PRIVATE INT4
Lip6IfHandleOperUp (tLip6If * pIf6Entry)
{
    tIp6Addr            Ip6Addr;
    tIp6Addr            McastIp6Addr;
    tLip6AddrNode      *pIp6AddrNode = NULL;
    tLip6AddrNode      *pSearchNode = NULL;
    tTMO_SLL_NODE      *pPrevNode = NULL;
    BOOL1               b1IsNodePresent = OSIX_FALSE;
    UINT1               u1DadStatus = 0;

    /* Get link-local address from linux */
    Lip6LlaGetLinkLocalAddress (pIf6Entry, &Ip6Addr, &u1DadStatus);

    /* Scan and insert in ascending order */
    TMO_SLL_Scan (&(pIf6Entry->Ip6LLAddrList), pSearchNode, tLip6AddrNode *)
    {
        /* This utility description is not correct. It returns IP6_SUCCESS
         * when 2nd address is greater */
        if (Ip6IsAddrGreater (&Ip6Addr, &(pSearchNode->Ip6Addr)) == IP6_SUCCESS)
        {
            /* Insert before pSearchNode */
            break;
        }
        if (Ip6AddrCompare (Ip6Addr, pSearchNode->Ip6Addr) == IP6_ZERO)
        {
            b1IsNodePresent = OSIX_TRUE;
            break;
        }
        pPrevNode = &(pSearchNode->SllNode);
    }

    if ((b1IsNodePresent != OSIX_TRUE)
        && (IS_ADDR_LLOCAL (Ip6Addr) == OSIX_TRUE))
    {
        /* Add address to list */
        if ((pIp6AddrNode =
             (tLip6AddrNode *) MemAllocMemBlk (gIp6GblInfo.AddrLstPoolId)) ==
            NULL)
        {
            LIP6_TRC_ARG (LIP6_MOD_TRC, ALL_FAILURE_TRC,
                          "Mem alloc for tLip6AddrNode Failed!\n");
            return OSIX_FAILURE;
        }

        MEMCPY (&(pIp6AddrNode->Ip6Addr), &Ip6Addr, sizeof (tIp6Addr));
        pIp6AddrNode->i4PrefixLen = LIP6_HOST_PREFIX_LENGTH / 2;
        pIp6AddrNode->u1AdminStatus = IP6FWD_ACTIVE;    /* Set it to active since linux
                                                           has already configured this */
        pIp6AddrNode->u1AddrType = ADDR6_LLOCAL;
        pIp6AddrNode->u1Status = u1DadStatus;

        TMO_SLL_Insert (&(pIf6Entry->Ip6LLAddrList), pPrevNode,
                        &(pIp6AddrNode->SllNode));
    }

    NetIpv6InvokeAddressChange (&Ip6Addr,
                                IP6_ADDR_MAX_PREFIX, ADDR6_LLOCAL,
                                pIf6Entry->u4IfIndex, NETIPV6_ADDRESS_ADD);

    if ((u1DadStatus & IFA_F_TENTATIVE) == IFA_F_TENTATIVE)
    {
        return OSIX_FAILURE;
    }
    if (pIf6Entry->u1Ipv6IfFwdOperStatus == IP6_IF_FORW_ENABLE)
    {
        /* Configure all IPv6 address in Linux */
        if (Lip6AddrEnableAll (pIf6Entry) == OSIX_FAILURE)
        {
            return OSIX_FAILURE;
        }
    }

    Lip6NdHandleIfStateChange (pIf6Entry);

    NetIpv6InvokeInterfaceStatusChange (pIf6Entry->u4IfIndex,
                                        NETIPV6_ALL_PROTO,
                                        NETIPV6_IF_UP,
                                        NETIPV6_INTERFACE_STATUS_CHANGE);

    /* Get link-local address from linux */
    u1DadStatus = 0;
    Lip6LlaGetLinkLocalAddress (pIf6Entry, &Ip6Addr, &u1DadStatus);

    if (Lip6IfInitIfEntry
        (pIf6Entry, &(Ip6Addr.u1_addr[8]),
         IP6_ADDR_LL_PREFIX_LEN) != OSIX_SUCCESS)
    {
        pIf6Entry->u1AdminStatus = NETIPV6_ADMIN_DOWN;
        return OSIX_FAILURE;
    }
    if (IS_ADDR_LLOCAL (Ip6Addr) == OSIX_TRUE)
    {
        Lip6AddrCreateSolicitMcast (pIf6Entry->u4IfIndex, &Ip6Addr);
    }
    SET_ALL_NODES_MULTI (McastIp6Addr);
    Lip6AddrCreateNewMcastAddr (pIf6Entry->u4IfIndex, &McastIp6Addr);
    if (pIf6Entry->u1Ipv6IfFwdOperStatus == IP6_IF_FORW_ENABLE)
    {
        /* Routing is enabled. Ensure that ALL ROUTER Multi-Cast address
         * is disabled for this interface. */
        SET_ALL_ROUTERS_MULTI (McastIp6Addr);
        Lip6AddrCreateNewMcastAddr (pIf6Entry->u4IfIndex, &McastIp6Addr);
    }

    return OSIX_SUCCESS;
}

/******************************************************************************
 * FUNCTION    : Lip6IfHandleOperDown
 * DESCRIPTION : This routine disables ipv6 in linux, indicates to RTM and
 *               indicates to higher layer modules
 *
 * INPUTS      : pIf6Entry - Pointer to Interface info
 *
 * OUTPUTS     : None
 *
 * RETURNS     : OSIX_SUCCESS/OSIX_FAILURE
 ******************************************************************************/
PRIVATE INT4
Lip6IfHandleOperDown (tLip6If * pIf6Entry)
{
    tIp6Addr            McastIp6Addr;
    UINT1               u1DadStatus = 0;

    if (Lip6AddrDisableAll (pIf6Entry) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    Lip6NdHandleIfStateChange (pIf6Entry);

    NetIpv6InvokeInterfaceStatusChange (pIf6Entry->u4IfIndex,
                                        NETIPV6_ALL_PROTO,
                                        NETIPV6_IF_DOWN,
                                        NETIPV6_INTERFACE_STATUS_CHANGE);
    if (pIf6Entry->u1AdminStatus == ADMIN_DOWN)
    {
        /* Admin is already Down. So interface would have not been enabled
         * earlier. So no need for any processing. */
        pIf6Entry->u1OperStatus = OPER_DOWN;
        return (OSIX_SUCCESS);
    }
	Lip6LlaGetLinkLocalAddress (pIf6Entry, &McastIp6Addr, &u1DadStatus);
	if (Lip6AddrDeleteMcastList (pIf6Entry) == OSIX_FAILURE)
	{
		return OSIX_FAILURE;
	}
	return OSIX_SUCCESS;
}

/******************************************************************************
 * FUNCTION    : Lip6IfCreateTunnel   
 * DESCRIPTION : This routine allocates memory for tunnel interface and   
 *               initializes it
 *
 * INPUTS      : pIf6Entry - Pointer to Interface info
 *
 * OUTPUTS     : None
 *
 * RETURNS     : OSIX_SUCCESS/OSIX_FAILURE
 ******************************************************************************/
PRIVATE INT4
Lip6IfCreateTunnel (tLip6If * pIf6Entry)
{
    if ((pIf6Entry->pTunlIf =
         (tIp6TunlIf *) MemAllocMemBlk (gIp6GblInfo.Ip6TunlIfPoolId)) == NULL)
    {
        LIP6_TRC_ARG (LIP6_MOD_TRC, ALL_FAILURE_TRC,
                      "Mem alloc for tIp6TunlIf Failed!\r\n");
        return OSIX_FAILURE;
    }

    MEMSET (pIf6Entry->pTunlIf, 0, sizeof (tIp6TunlIf));

    return OSIX_SUCCESS;
}

/****************************************************************************
 *  Function    :  Ip6IsIfFwdEnabled
 *  Description :  This function returns TRUE/FALSE depending on
 * whether forwarding is enabled or disabled on a particular interface
 * Input       :  i4IfIndex
 * Output      :  None
 * Returns     :  TRUE/FALSE
 * **************************************************************************/
UINT1
Ip6IsIfFwdEnabled (INT4 i4IfIndex)
{
    tLip6If            *pIf6Entry = NULL;

    pIf6Entry = Lip6UtlGetIfEntry ((UINT4) i4IfIndex);

    if (pIf6Entry == NULL)
    {
        return FALSE;
    }

    if (pIf6Entry->u1Ipv6IfFwdOperStatus == IP6_IF_FORW_DISABLE)
    {
        return FALSE;
    }
    return TRUE;
}

/****************************************************************************
 *  Function    :  Lip6GetStallStatus
 *  Description :  This function returns TRUE/FALSE depending on
 * whether the disable_ipv6 set to true in kernel.
 * Input       :  i4IfIndex
 * Output      :  None
 * Returns     :  TRUE/FALSE
 * **************************************************************************/
PUBLIC UINT1
Lip6GetStallStatus (UINT4 u4Index)
{
    UINT4               u4Status;
    tLip6If            *pIf6Entry = NULL;
    FILE               *pFd = 0;
    CHR1                ac1Command[LIP6_LINE_LEN];
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    UINT4               u4ContextId = VCM_DEFAULT_CONTEXT;
    tLnxVrfIfInfo *pLnxVrfIfInfo = NULL;
    tLnxVrfInfo        *pLnxVrfInfo = NULL;
#endif


    if ((pIf6Entry = Lip6UtlGetIfEntry (u4Index)) == NULL)
    {
        return FALSE;
    }
    MEMSET (ac1Command, 0, sizeof (ac1Command));

#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)

    pLnxVrfIfInfo = LnxVrfIfInfoGet(pIf6Entry->au1IfName);
    if(pLnxVrfIfInfo != NULL)
    {
        u4ContextId = pLnxVrfIfInfo->u4ContextId;
    }
    if(u4ContextId != VCM_DEFAULT_CONTEXT)
    {
        pLnxVrfInfo = LnxVrfInfoGet (u4ContextId);
        if (pLnxVrfInfo == NULL)
        {
            return FALSE;
        }
    }

    if(Ip6UtilChangeCxt (u4ContextId, LNX_VRF_NON_DEFAULT_NS, 
                         pLnxVrfInfo->au1NameSpace) == (UINT4)NETIPV6_FAILURE)
    {
        return FALSE;
    }
#endif

    SNPRINTF (ac1Command, sizeof (ac1Command),
              "%s/%s/%s",
              LIP6_PROC_CONF, pIf6Entry->au1IfName, LIP6_PROC_DISABLEIPV6);

    if ((pFd = FOPEN (ac1Command, "r")) != NULL)
    {
        fscanf (pFd, "%d\n", &u4Status);
        fclose (pFd);

        if (u4Status == 1)
        {
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
            if(Ip6UtilChangeCxt (u4ContextId, LNX_VRF_DEFAULT_NS,
                                 (UINT1 *)LNX_VRF_DEFAULT_NS_PID) == (UINT4)NETIPV6_FAILURE)
            {
                return FALSE;
            }
#endif

            return TRUE;
        }
        else
        {
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
            if(Ip6UtilChangeCxt (u4ContextId, LNX_VRF_DEFAULT_NS,
                                 (UINT1 *)LNX_VRF_DEFAULT_NS_PID) == (UINT4)NETIPV6_FAILURE)
            {
                return FALSE;
            }
#endif

            return FALSE;
        }
    }
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    if(Ip6UtilChangeCxt (u4ContextId, LNX_VRF_DEFAULT_NS,
                         (UINT1 *)LNX_VRF_DEFAULT_NS_PID) == (UINT4)NETIPV4_FAILURE)
    {
        return FALSE;
    }
#endif


    return FALSE;
}

#endif /* _LIP6IF_C */
/* END OF FILE */
