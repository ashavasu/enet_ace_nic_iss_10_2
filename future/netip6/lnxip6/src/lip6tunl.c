/**************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: lip6tunl.c,v 1.3 2015/03/09 13:09:20 siva Exp $
 *
 * Description: IPv6 Tunnel configuration routines
 ***************************************************************************/
#include "lip6inc.h"

#ifndef _LIP6TUNL_C
#define _LIP6TUNL_C
/******************************************************************************
 * DESCRIPTION : This routine creates tunnel interface in Linux
 *
 * INPUTS      : pIf6Entry - Pointer to IPv6 interface structure
 *
 * OUTPUTS     : NONE
 *
 * RETURNS     : OSIX_SUCCESS / OSIX_FAILURE
 ******************************************************************************/
PUBLIC INT4
Lip6TunnelCreate (tLip6If * pIf6Entry)
{
    tCfaIfInfo          CfaIfInfo;
    tUtlInAddr          UtlIpAddr;
    struct ifreq        if_req;
    CHR1                ac1Command[LIP6_LINE_LEN];
    CHR1               *pIp4Addr;
    INT4                i4Error = 0;

    /* Create tunnel */
    MEMSET (ac1Command, 0, sizeof (ac1Command));

    UtlIpAddr.u4Addr = pIf6Entry->pTunlIf->tunlSrc.u4_addr[3];
    pIp4Addr = UtlInetNtoa (UtlIpAddr);

    SNPRINTF (ac1Command, sizeof (ac1Command),
              "ip tunnel add %s mode sit ttl 64 remote any local %s",
              pIf6Entry->au1IfName, pIp4Addr);
    system (ac1Command);

    /* Get IP port number and update */
    MEMSET (&if_req, 0, sizeof (if_req));
    STRCPY (if_req.ifr_ifrn.ifrn_name, pIf6Entry->au1IfName);

#if defined (VRF_WANTED) && defined (LINUX_310_WANTED) && defined (LNXIP4_WANTED)
    i4Error = ioctl (gIp6GblInfo.ai4SockFd[pIf6Entry->u4ContextId], SIOCGIFINDEX, &if_req);
#else
    i4Error = ioctl (gIp6GblInfo.i4SockFd, SIOCGIFINDEX, &if_req);
#endif
    if (i4Error < 0)
    {
        return OSIX_FAILURE;
    }

    pIf6Entry->u4IpPort = if_req.ifr_ifindex;

    /* Update CFA with the Ip Port */
    MEMSET (&CfaIfInfo, 0, sizeof (tCfaIfInfo));
    CfaIfInfo.i4IpPort = pIf6Entry->u4IpPort;
    CfaSetIfInfo (IF_IP_PORTNUM, pIf6Entry->u4IfIndex, &CfaIfInfo);

    return OSIX_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : This routine deletes tunnel interface in Linux
 *
 * INPUTS      : pIf6Entry - Pointer to IPv6 interface structure
 *
 * OUTPUTS     : NONE
 *
 * RETURNS     : OSIX_SUCCESS / OSIX_FAILURE
 ******************************************************************************/
PUBLIC INT4
Lip6TunnelDelete (tLip6If * pIf6Entry)
{
    CHR1                ac1Command[LIP6_LINE_LEN];

    /* Delete tunnel interface */
    MEMSET (ac1Command, 0, sizeof (ac1Command));
    SNPRINTF (ac1Command, sizeof (ac1Command),
              "ip tunnel del %s", pIf6Entry->au1IfName);
    system (ac1Command);

    return OSIX_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : This routine makes the tunnel as up in Linux
 *
 * INPUTS      : pIf6Entry - Pointer to IPv6 interface structure
 *
 * OUTPUTS     : NONE
 *
 * RETURNS     : OSIX_SUCCESS / OSIX_FAILURE
 ******************************************************************************/
PUBLIC INT4
Lip6TunnelEnable (tLip6If * pIf6Entry)
{
    CHR1                ac1Command[LIP6_LINE_LEN];

    /* Set interface up */
    MEMSET (ac1Command, 0, sizeof (ac1Command));
    SNPRINTF (ac1Command, sizeof (ac1Command),
              "ip link set dev %s up", pIf6Entry->au1IfName);
    system (ac1Command);

#if 0
    /* Derive and add local 6to4 address to interface */
    MEMSET (ac1Command, 0, sizeof (ac1Command));
    SNPRINTF (ac1Command, sizeof (ac1Command),
              "ip -6 addr add 2002:%02x:%02x:%02x:%02x::1/16 dev %s",
              pIf6Entry->pTunlIf->tunlSrc.u1_addr[12],
              pIf6Entry->pTunlIf->tunlSrc.u1_addr[13],
              pIf6Entry->pTunlIf->tunlSrc.u1_addr[14],
              pIf6Entry->pTunlIf->tunlSrc.u1_addr[15], pIf6Entry->au1IfName);
    system (ac1Command);

    /* Add default route to global IPv6 network using all-6to4-routers IPv4
     * anycast address */
    MEMSET (ac1Command, 0, sizeof (ac1Command));
    SNPRINTF (ac1Command, sizeof (ac1Command),
              "ip -6 route add 2000::/3 via ::192.88.99.1 dev %s metric 1",
              pIf6Entry->au1IfName);
    system (ac1Command);
#endif

    return OSIX_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : This routine disables tunnel interface in Linux
 *
 * INPUTS      : pIf6Entry - Pointer to IPv6 interface structure
 *
 * OUTPUTS     : NONE
 *
 * RETURNS     : OSIX_SUCCESS / OSIX_FAILURE
 ******************************************************************************/
PUBLIC INT4
Lip6TunnelDisable (tLip6If * pIf6Entry)
{
    CHR1                ac1Command[LIP6_LINE_LEN];

    /* Remove all routes on this dev */
    MEMSET (ac1Command, 0, sizeof (ac1Command));
    SNPRINTF (ac1Command, sizeof (ac1Command),
              "ip -6 route flush dev %s", pIf6Entry->au1IfName);
    system (ac1Command);

    /* Shutdown interface */
    MEMSET (ac1Command, 0, sizeof (ac1Command));
    SNPRINTF (ac1Command, sizeof (ac1Command),
              "ip link set dev %s down", pIf6Entry->au1IfName);
    system (ac1Command);

    return OSIX_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : This routine handles changes in tunnel interface parameters
 *
 * INPUTS      : pCfaInfo - Pointer to CFA information structure passed by CFA
 *
 * OUTPUTS     : pu4IfIndex - Interface Index
 *
 * RETURNS     : OSIX_SUCCESS / OSIX_FAILURE
 ******************************************************************************/
PUBLIC INT4
Lip6TunnelIfUpdate (UINT4 u4IfIndex, UINT1 u1TnlDir, UINT1 u1TnlDirFlag,
                    UINT1 u1EncapOption, UINT1 u1EncapLimit, UINT4 u4Mask)
{
    tLip6If            *pIf6Entry = NULL;

    pIf6Entry = Lip6UtlGetIfEntry (u4IfIndex);
    if (pIf6Entry == NULL)
    {
        return OSIX_FAILURE;
    }

    if (pIf6Entry->u1IfType != IP6_TUNNEL_INTERFACE_TYPE)
    {
        LIP6_TRC_ARG1 (LIP6_MOD_TRC,
                       CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                       "Lip6TunlifUpdate: Not Tunnel Interface %d\n",
                       u4IfIndex);
        return OSIX_FAILURE;
    }

    if (u4Mask & IP6_TNL_CHG_MASK_DIR)
    {
        pIf6Entry->pTunlIf->u1TunlDir = u1TnlDir;
    }

    if (u4Mask & IP6_TNL_CHG_MASK_DIR_FLAG)
    {
        pIf6Entry->pTunlIf->u1TunlFlag = u1TnlDirFlag;
    }

    if (u4Mask & IP6_TNL_CHG_MASK_ENCAP_OPT)
    {
        pIf6Entry->pTunlIf->u1EncapFlag = u1EncapOption;
    }

    if (u4Mask & IP6_TNL_CHG_MASK_ENCAP_LMT)
    {
        pIf6Entry->pTunlIf->u1TunlEncaplmt = u1EncapLimit;
    }

    return OSIX_SUCCESS;
}

#endif /* _LIP6TUNL_C */
