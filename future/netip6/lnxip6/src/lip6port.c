/********************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: lip6port.c,v 1.7 2015/06/26 02:40:47 siva Exp $
 *
 * Description: Functions that access the external module (link layer,
 *              IPv4 layer, etc) data structure.
 *********************************************************************/
#ifndef _LIP6PORT_C
#define _LIP6PORT_C

#include "lip6inc.h"

PRIVATE INT4 Lip6PortIfCreate PROTO ((tCfaRegInfo * pCfaInfo));
PRIVATE INT4 Lip6PortIfDelete PROTO ((tCfaRegInfo * pCfaInfo));
PRIVATE INT4 Lip6PortIfUpdate PROTO ((tCfaRegInfo * pCfaInfo));
PRIVATE INT4 Lip6PortIfOperStChg PROTO ((tCfaRegInfo * pCfaInfo));

extern UINT1       *CfaGddGetLnxIntfnameForPort (UINT2 u2Index);
#ifdef TUNNEL_WANTED
PRIVATE INT4 Lip6PortTnlIfUpdate PROTO ((tCfaRegInfo * pCfaInfo));
#endif

/******************************************************************************
 * DESCRIPTION : This routine gets Operational status for the L3 interface
 *               from CFA and assigns it on the interface structure based on
 *               the MTU.
 *
 * INPUTS      : u4IfIndex - Interface Index
 *
 * OUTPUTS     : None
 *
 * RETURNS     : NETIPV6_OPER_UP if the lower layer is UP
 *               NETIPV6_OPER_DOWN if the lower layer is DOWN
 *
 ******************************************************************************/
PUBLIC INT4
Lip6PortSetIfOper (tLip6If * pIf6Entry)
{
    tCfaIfInfo          CfaIfInfo;
	tIp6Addr            Ip6Addr;
	UINT1               u1DadStatus = 0;
	tIp6Addr            McastIp6Addr;


    if (CfaGetIfInfo (pIf6Entry->u4IfIndex, &CfaIfInfo) == CFA_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* If the lower layer indicates that the interface is
     * active and if the interface MTU value is greater than
     * 1280, then Ip6 interface operation status can be
     * set as active */
    if ((pIf6Entry->u1AdminStatus == NETIPV6_ADMIN_UP) &&
        (CfaIfInfo.u1IfOperStatus == CFA_IF_UP) &&
        (pIf6Entry->u4Mtu >= LIP6_MIN_MTU))
    {
        pIf6Entry->u1OperStatus = NETIPV6_OPER_UP;
	Lip6LlaGetLinkLocalAddress (pIf6Entry, &Ip6Addr, &u1DadStatus); 
	if (((u1DadStatus & IFA_F_TENTATIVE) != IFA_F_TENTATIVE) && (u1DadStatus != 0))
	{
		if (IS_ADDR_LLOCAL (Ip6Addr) == OSIX_TRUE)
		{
			Lip6AddrCreateSolicitMcast (pIf6Entry->u4IfIndex, &Ip6Addr);
		}
			SET_ALL_NODES_MULTI (McastIp6Addr);
			Lip6AddrCreateNewMcastAddr (pIf6Entry->u4IfIndex, &McastIp6Addr);
			if (pIf6Entry->u1Ipv6IfFwdOperStatus == IP6_IF_FORW_ENABLE)
			{
				/* Routing is enabled. Ensure that ALL ROUTER Multi-Cast address
				 *          * is disabled for this interface. */
				SET_ALL_ROUTERS_MULTI (McastIp6Addr);
				Lip6AddrCreateNewMcastAddr (pIf6Entry->u4IfIndex, &McastIp6Addr);
			}

	}
        
        return OSIX_SUCCESS;
    }

    pIf6Entry->u1OperStatus = NETIPV6_OPER_DOWN;
    return OSIX_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : This routine will register IPv6 with lower layer
 * 
 * INPUTS      : None
 *
 * OUTPUTS     : None
 *
 * RETURNS     : OSIX_SUCCESS/OSIX_FAILURE;
 ******************************************************************************/
PUBLIC INT4
Lip6PortRegisterLL (VOID)
{
    tCfaRegParams       CfaRegParams;

    CfaRegParams.u2LenOrType = CFA_ENET_IPV6;
    CfaRegParams.u2RegMask = CFA_IF_CREATE | CFA_IF_DELETE | CFA_IF_UPDATE |
        CFA_IF_OPER_ST_CHG | CFA_IF_RCV_PKT | CFA_TNL_IF_UPDATE;
    CfaRegParams.pIfCreate = Lip6PortIfCreate;
    CfaRegParams.pIfDelete = Lip6PortIfDelete;
    CfaRegParams.pIfUpdate = Lip6PortIfUpdate;
#ifdef TUNNEL_WANTED
    CfaRegParams.pTnlIfUpdate = Lip6PortTnlIfUpdate;
#else
    CfaRegParams.pTnlIfUpdate = NULL;
#endif
    CfaRegParams.pIfOperStChg = Lip6PortIfOperStChg;
    CfaRegParams.pIfRcvPkt = NULL;
    CfaRegisterHL (&CfaRegParams);

    return OSIX_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : This routine notifies the lower layer that the IP6 module is
 *               UP and READY
 *
 * INPUTS      : None
 *
 * OUTPUTS     : None
 *
 * RETURNS     : NONE
 ******************************************************************************/
VOID
Lip6PortNotifyLL (VOID)
{
#ifndef IP_WANTED
#ifdef CFA_WANTED
    CfaNotifyIpUp ();
#endif
#endif
    return;
}

/******************************************************************************
 * DESCRIPTION : This routine retrives the MAC Address for given Interface
 *
 * INPUTS      : u4IfIndex - Interface whose MAC Address is required.
 *
 * OUTPUTS     : au1HwAddr - MAC Address of the interface
 *
 * RETURNS     : OSIX_SUCCESS / OSIX_FAILURE
 ******************************************************************************/
INT4
Lip6PortGetHwAddr (UINT4 u4IfIndex, UINT1 *au1HwAddr)
{
    tCfaIfInfo          IfInfo;
    if (CfaGetIfInfo ((UINT2) u4IfIndex, &IfInfo) == CFA_FAILURE)
    {
        return OSIX_FAILURE;
    }
    MEMCPY (au1HwAddr, IfInfo.au1MacAddr, CFA_ENET_ADDR_LEN);

    return OSIX_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : This routine retrives the name for given Interface
 *
 * INPUTS      : u4IfIndex - Interface whose name is required.
 *
 * OUTPUTS     : au1IfName - Interface Name
 *
 * RETURNS     : OSIX_SUCCESS / OSIX_FAILURE
 ******************************************************************************/
INT4
Lip6PortGetIfName (UINT4 u4IfIndex, UINT1 *au1IfName)
{
    if (CfaGetInterfaceNameFromIndex (u4IfIndex, au1IfName) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : This routine retrives the Index for given Interface name
 *
 * INPUTS      : u4L2ContextId - Layer 2 context Id for the Interface
 *             : au1IfName - Interface Name whose index is required.
 *
 * OUTPUTS     : pu4IfIndex - Interface Index
 *
 * RETURNS     : OSIX_SUCCESS / OSIX_FAILURE
 ******************************************************************************/
INT4
Lip6PortGetIfIndexFromNameInCxt (UINT4 u4L2ContextId, UINT1 *au1IfName,
                                 UINT4 *pu4IfIndex)
{
    if (CfaGetInterfaceIndexFromNameInCxt (u4L2ContextId,
                                           au1IfName,
                                           pu4IfIndex) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : The below routines are the call backs routines registered with
 *               CFA to get the notification for interface creation, deletion
 *               status changes, IPv6 Packet reception.
 *
 * INPUTS      : pCfaIfInfo - Cfa Information
 *
 * OUTPUTS     : None.
 *
 * RETURNS     : OSIX_SUCCESS / OSIX_FAILURE
 ******************************************************************************/
PRIVATE INT4
Lip6PortIfCreate (tCfaRegInfo * pCfaInfo)
{
    tLip6CfaParams      Ip6Params;
    UINT4               u4ContextId = VCM_DEFAULT_CONTEXT;

#if defined(VRF_WANTED) && defined(LINUX_310_WANTED)
    Ip6GetCxtId (pCfaInfo->u4IfIndex, &u4ContextId);
#endif

    if( u4ContextId == VCM_DEFAULT_CONTEXT )
    {

	    if (gIp6GblInfo.i4Ip6Status == OSIX_FALSE)
	    {
		    LIP6_TRC_ARG (LIP6_MOD_TRC, CONTROL_PLANE_TRC,
				    "IP6 module Not Initialised !\r\n");
		    return OSIX_FAILURE;
	    }
    }
    else
    {
	    if (gIp6GblInfo.apIp6Cxt[u4ContextId] != NULL)
	    {
		    if ( (gIp6GblInfo.apIp6Cxt[u4ContextId])->u4ForwFlag == OSIX_FALSE )
		    {
			    LIP6_TRC_ARG (LIP6_MOD_TRC, CONTROL_PLANE_TRC,
					    "IP6 module Not Initialised !\r\n");
			    return OSIX_FAILURE;
		    }
	    }
    }
    MEMSET (&Ip6Params, 0, sizeof (tLip6CfaParams));

    SNPRINTF ((CHR1 *) Ip6Params.au1IfName, CFA_MAX_PORT_NAME_LENGTH, "%s",
              pCfaInfo->CfaIntfInfo.au1IfName);
    Ip6Params.u4IfIndex = pCfaInfo->u4IfIndex;
    Ip6Params.u4Speed = pCfaInfo->CfaIntfInfo.u4IfSpeed;
    Ip6Params.u4HighSpeed = pCfaInfo->CfaIntfInfo.u4IfHighSpeed;
    Ip6Params.u1IfType = pCfaInfo->CfaIntfInfo.u1IfType;
    Ip6Params.u4Status = pCfaInfo->CfaIntfInfo.u1IfOperStatus;
    Ip6Params.u1MsgType = IP6_LANIF_ENTRY_VALID;

    Ip6Lock ();
    if (Lip6IfUpdateInterface (&Ip6Params) == OSIX_SUCCESS)
    {
        if (pCfaInfo->CfaIntfInfo.u1PrefixLen != 0)
        {
            if (SetIpv6IfAddress (pCfaInfo->u4IfIndex,
                                  pCfaInfo->CfaIntfInfo.IfIp6Addr,
                                  pCfaInfo->CfaIntfInfo.u1PrefixLen) ==
                SNMP_SUCCESS)
            {
                Ip6UnLock ();
                return OSIX_SUCCESS;
            }
        }
    }
    Ip6UnLock ();

    LIP6_TRC_ARG1 (LIP6_MOD_TRC,
                   CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                   "ERROR Could not create the Ip6 "
                   "Interface Index : %d\n", pCfaInfo->u4IfIndex);
    return OSIX_FAILURE;
}

/******************************************************************************
 * DESCRIPTION : This routine handles interface delete indication from CFA.
 *               This function's pointer is registered with CFA and CFA will
 *               call this when there is a oper status change.
 *
 * INPUTS      : pCfaInfo - Pointer to CFA information structure passed by CFA
 *
 * OUTPUTS     : pu4IfIndex - Interface Index
 *
 * RETURNS     : OSIX_SUCCESS / OSIX_FAILURE
 ******************************************************************************/
PRIVATE INT4
Lip6PortIfDelete (tCfaRegInfo * pCfaInfo)
{
    tLip6CfaParams      Ip6Params;
    UINT4               u4ContextId = VCM_DEFAULT_CONTEXT;

#if defined(VRF_WANTED) && defined(LINUX_310_WANTED)
    Ip6GetCxtId (pCfaInfo->u4IfIndex, &u4ContextId);
#endif

    if( u4ContextId == VCM_DEFAULT_CONTEXT )
    {
	    if (gIp6GblInfo.i4Ip6Status == OSIX_FALSE)
	    {
		    LIP6_TRC_ARG (LIP6_MOD_TRC, CONTROL_PLANE_TRC,
				    "IP6 module Not Initialised !\r\n");
		    return OSIX_FAILURE;
	    }
    }
    else
    {
	    if (gIp6GblInfo.apIp6Cxt[u4ContextId] != NULL)
	    {
		    if ( (gIp6GblInfo.apIp6Cxt[u4ContextId])->u4ForwFlag == OSIX_FALSE )
		    {
			    LIP6_TRC_ARG (LIP6_MOD_TRC, CONTROL_PLANE_TRC,
					    "IP6 module Not Initialised !\r\n");
			    return OSIX_FAILURE;
		    }
	    }
    }

    MEMSET (&Ip6Params, 0, sizeof (tLip6CfaParams));

    Ip6Params.u4IfIndex = pCfaInfo->u4IfIndex;
    Ip6Params.u4Speed = pCfaInfo->CfaIntfInfo.u4IfSpeed;
    Ip6Params.u4HighSpeed = pCfaInfo->CfaIntfInfo.u4IfHighSpeed;
    Ip6Params.u1IfType = pCfaInfo->CfaIntfInfo.u1IfType;
    Ip6Params.u4Status = IP6_LANIF_OPER_DOWN;
    Ip6Params.u1MsgType = IP6_LANIF_ENTRY_INVALID;

    Ip6Lock ();
    if (Lip6IfUpdateInterface (&Ip6Params) == OSIX_SUCCESS)
    {
        Ip6UnLock ();
        return OSIX_SUCCESS;
    }
    Ip6UnLock ();

    LIP6_TRC_ARG1 (LIP6_MOD_TRC,
                   CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                   "ERROR ! Couldnot delete  the Ip6 Interface : Index -%d\n",
                   pCfaInfo->u4IfIndex);
    return OSIX_FAILURE;
}

/******************************************************************************
 * DESCRIPTION : This routine handles changes in interface parameters from CFA.
 *               This function's pointer is registered with CFA and CFA will
 *               call this when there is a oper status change.
 *
 * INPUTS      : pCfaInfo - Pointer to CFA information structure passed by CFA
 *
 * OUTPUTS     : pu4IfIndex - Interface Index
 *
 * RETURNS     : OSIX_SUCCESS / OSIX_FAILURE
 ******************************************************************************/
PRIVATE INT4
Lip6PortIfUpdate (tCfaRegInfo * pCfaInfo)
{
    Ip6Lock ();
    if (Lip6IfUpdateSpeedMtu (pCfaInfo->u4IfIndex,
                              pCfaInfo->CfaIntfInfo.u4IfMtu,
                              pCfaInfo->CfaIntfInfo.u4IfSpeed,
                              pCfaInfo->CfaIntfInfo.u4IfHighSpeed) ==
        OSIX_SUCCESS)
    {
        Ip6UnLock ();
        return OSIX_SUCCESS;
    }
    Ip6UnLock ();

    LIP6_TRC_ARG1 (LIP6_MOD_TRC,
                   CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                   "ERROR ! Couldnot Update the Ip6 Interface : Index -%d\n",
                   pCfaInfo->u4IfIndex);
    return OSIX_FAILURE;
}

#ifdef TUNNEL_WANTED
/******************************************************************************
 * DESCRIPTION : This function gets the parameters associated with the given
 *               tunnel interface and updates the given Tunnel interface
 *               structure.
 *
 * INPUTS      : u4IfIndex - Index pointing to the Tunnel Interface.
 *
 * OUTPUTS     : pIf6Entry - Updated with Tunnel Interface information
 *
 * RETURNS     : OSIX_SUCCESS/OSIX_FAILURE
 *
 ******************************************************************************/
PUBLIC INT4
Lip6PortGetTunnelParams (tLip6If * pIf6Entry)
{
    tTnlIfEntry         TnlIfEntry;
    tIp6TunlIf         *pIp6TunlIf = NULL;

    if (CfaGetTnlEntryFromIfIndex (pIf6Entry->u4IfIndex, &TnlIfEntry) ==
        CFA_FAILURE)
    {
        return OSIX_FAILURE;
    }

    pIf6Entry->u4Mtu = TnlIfEntry.u4Mtu;
    pIp6TunlIf = pIf6Entry->pTunlIf;
    pIp6TunlIf->u1TunlType = (UINT1) TnlIfEntry.u4EncapsMethod;
    pIp6TunlIf->u1TunlFlag = TnlIfEntry.u1DirFlag;
    pIp6TunlIf->u1TunlDir = TnlIfEntry.u1Direction;
    SET_ADDR_UNSPECIFIED (pIp6TunlIf->tunlSrc);
    SET_ADDR_UNSPECIFIED (pIp6TunlIf->tunlDst);
    PTR_ASSIGN4 (&(pIp6TunlIf->tunlSrc.u1_addr[12]),
                 TnlIfEntry.LocalAddr.Ip4TnlAddr);
    PTR_ASSIGN4 (&(pIp6TunlIf->tunlDst.u1_addr[12]),
                 TnlIfEntry.RemoteAddr.Ip4TnlAddr);
    pIp6TunlIf->u1TunlEncaplmt = (UINT1) TnlIfEntry.u4EncapLmt;
    pIp6TunlIf->u1EncapFlag = TnlIfEntry.u1EncapOption;
    return OSIX_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : This routine handles changes in tunnel interface parameters
 *               This function's pointer is registered with CFA and CFA will
 *               call this when there is a oper status change.
 *
 * INPUTS      : pCfaInfo - Pointer to CFA information structure passed by CFA
 *
 * OUTPUTS     : pu4IfIndex - Interface Index
 *
 * RETURNS     : OSIX_SUCCESS / OSIX_FAILURE
 ******************************************************************************/
PRIVATE INT4
Lip6PortTnlIfUpdate (tCfaRegInfo * pCfaInfo)
{
    UINT4               u4Mask = 0;
    UINT4               u4ContextId = VCM_DEFAULT_CONTEXT;

#if defined(VRF_WANTED) && defined(LINUX_310_WANTED)
    Ip6GetCxtId (pCfaInfo->u4IfIndex, &u4ContextId); 
#endif

    if( u4ContextId == VCM_DEFAULT_CONTEXT )
    {

	    if (gIp6GblInfo.i4Ip6Status == OSIX_FALSE)
	    {
		    LIP6_TRC_ARG (LIP6_MOD_TRC, CONTROL_PLANE_TRC,
				    "IP6 module Not Initialised !\r\n");
		    return OSIX_FAILURE;
	    }
    }
    else
    {
	    if (gIp6GblInfo.apIp6Cxt[u4ContextId] != NULL)
	    {
		    if ( (gIp6GblInfo.apIp6Cxt[u4ContextId])->u4ForwFlag == OSIX_FALSE )
		    {
			    LIP6_TRC_ARG (LIP6_MOD_TRC, CONTROL_PLANE_TRC,
					    "IP6 module Not Initialised !\r\n");
			    return OSIX_FAILURE;
		    }
	    }
    }
    if (pCfaInfo->CfaTnlIfInfo.u4TnlMask & TNL_DIRECTION_CHG)
        u4Mask |= IP6_TNL_CHG_MASK_DIR;

    if (pCfaInfo->CfaTnlIfInfo.u4TnlMask & TNL_DIR_FLAG_CHG)
        u4Mask |= IP6_TNL_CHG_MASK_DIR_FLAG;

    if (pCfaInfo->CfaTnlIfInfo.u4TnlMask & TNL_ENCAP_OPT_CHG)
        u4Mask |= IP6_TNL_CHG_MASK_ENCAP_OPT;

    if (pCfaInfo->CfaTnlIfInfo.u4TnlMask & TNL_ENCAP_LIMIT_CHG)
        u4Mask |= IP6_TNL_CHG_MASK_ENCAP_LMT;

    Ip6Lock ();
    if (Lip6TunnelIfUpdate (pCfaInfo->u4IfIndex,
                            (UINT1) pCfaInfo->CfaTnlIfInfo.u4TnlDir,
                            (UINT1) pCfaInfo->CfaTnlIfInfo.u4TnlDirFlag,
                            (UINT1) pCfaInfo->CfaTnlIfInfo.u4TnlEncapOpt,
                            (UINT1) pCfaInfo->CfaTnlIfInfo.u4TnlEncapLmt,
                            u4Mask) == OSIX_FAILURE)
    {
        Ip6UnLock ();
        return OSIX_FAILURE;
    }
    Ip6UnLock ();
    return OSIX_SUCCESS;
}

/*****************************************************************************
* DESCRIPTION : Checks the tunnel interface address compatibility with
*               the underlying layers v4 address
* 
* INPUTS      : Tunnel interface v6 address, tunnel interface index
* 
* OUTPUTS     : None
* 
* RETURNS     : OSIX_SUCCESS or OSIX_FAILURE 
*****************************************************************************/
PUBLIC INT4
Lip6PortCheckAutoTunlAddr (tIp6Addr * pIp6Addr, UINT4 u4IfIndex)
{
    tLip6If            *pIf6Entry;
    tTnlIfEntry         TnlIfEntry;

    pIf6Entry = Lip6UtlGetIfEntry (u4IfIndex);

    if (pIf6Entry == NULL)
    {
        return OSIX_FAILURE;
    }
    /* Check the interface is Tunnel */
    if (pIf6Entry->u1IfType == IP6_TUNNEL_INTERFACE_TYPE)
    {

        if (CfaGetTnlEntryFromIfIndex (u4IfIndex, &TnlIfEntry) == CFA_FAILURE)
        {
            return OSIX_FAILURE;
        }
        TnlIfEntry.LocalAddr.Ip4TnlAddr =
            OSIX_HTONL (TnlIfEntry.LocalAddr.Ip4TnlAddr);

        if (TnlIfEntry.u4EncapsMethod == IPV6_SIX_TO_FOUR)
        {
            if (IS_ADDR_6to4 (*pIp6Addr))
            {
                if (MEMCMP (&(TnlIfEntry.LocalAddr.Ip4TnlAddr),
                            &(pIp6Addr->u1_addr[2]), 4) != 0)
                    return OSIX_FAILURE;
            }
        }
        if (TnlIfEntry.u4EncapsMethod == IPV6_ISATAP_TUNNEL)
        {
            if (IS_ADDR_ISATAP (*pIp6Addr))
            {
                if (MEMCMP (&(TnlIfEntry.LocalAddr.Ip4TnlAddr),
                            &(pIp6Addr->u1_addr[12]), 4) != 0)
                {
                    return OSIX_FAILURE;
                }
            }
        }
    }
    return OSIX_SUCCESS;
}
#endif

/******************************************************************************
 * DESCRIPTION : This routine handles Oper status change indication from CFA.
 *               This function's pointer is registered with CFA and CFA will
 *               call this when there is a oper status change.
 *
 * INPUTS      : pCfaInfo - Pointer to CFA information structure passed by CFA
 *
 * OUTPUTS     : pu4IfIndex - Interface Index
 *
 * RETURNS     : OSIX_SUCCESS / OSIX_FAILURE
 ******************************************************************************/
PRIVATE INT4
Lip6PortIfOperStChg (tCfaRegInfo * pCfaInfo)
{
    tLip6CfaParams      Ip6Params;
    UINT1              *pu1PortName = NULL;
    UINT4               u4ContextId = VCM_DEFAULT_CONTEXT;

#if defined(VRF_WANTED) && defined(LINUX_310_WANTED)
    Ip6GetCxtId (pCfaInfo->u4IfIndex, &u4ContextId);
#endif

    if( u4ContextId == VCM_DEFAULT_CONTEXT )
    {
	    if (gIp6GblInfo.i4Ip6Status == OSIX_FALSE)
	    {
		    LIP6_TRC_ARG (LIP6_MOD_TRC, CONTROL_PLANE_TRC,
				    "IP6 module Not Initialised !\r\n");
		    return OSIX_FAILURE;
	    }
    }
    else
    {
	    if (gIp6GblInfo.apIp6Cxt[u4ContextId] != NULL)
	    {
		    if ( (gIp6GblInfo.apIp6Cxt[u4ContextId])->u4ForwFlag == OSIX_FALSE )
		    {
			    LIP6_TRC_ARG (LIP6_MOD_TRC, CONTROL_PLANE_TRC,
					    "IP6 module Not Initialised !\r\n");
			    return OSIX_FAILURE;
		    }
	    }
    }

    MEMSET (&Ip6Params, 0, sizeof (tLip6CfaParams));

    Ip6Params.u4IfIndex = pCfaInfo->u4IfIndex;
    Ip6Params.u4Speed = pCfaInfo->CfaIntfInfo.u4IfSpeed;
    Ip6Params.u4HighSpeed = pCfaInfo->CfaIntfInfo.u4IfHighSpeed;
    Ip6Params.u1IfType = pCfaInfo->CfaIntfInfo.u1IfType;
    Ip6Params.u4Status = pCfaInfo->CfaIntfInfo.u1IfOperStatus;
    Ip6Params.u1MsgType = IP6_LANIF_ENTRY_UP;

    if (pCfaInfo->CfaIntfInfo.u1IfType == CFA_ENET)
    {
        pu1PortName = CfaGddGetLnxIntfnameForPort ((UINT2) pCfaInfo->u4IfIndex);

        if (pu1PortName != NULL)
        {
            /* get ethX interface flags */
            STRCPY (Ip6Params.au1IfName, pu1PortName);
        }
    }

    Ip6Lock ();
    if (Lip6IfUpdateInterface (&Ip6Params) == OSIX_SUCCESS)
    {
        Ip6UnLock ();
        return OSIX_SUCCESS;
    }
    Ip6UnLock ();

    LIP6_TRC_ARG1 (LIP6_MOD_TRC,
                   CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                   "Could not Change the Operstatus"
                   " of the Ip6 Interface : Index -%d\n", pCfaInfo->u4IfIndex);
    return OSIX_FAILURE;
}

#endif /* _LIP6PORT_C */
/* END OF FILE */
