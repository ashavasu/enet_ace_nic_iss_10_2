/********************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: netip6mc.c,v 1.11 2013/12/05 12:52:31 siva Exp $
 *
 * Description:This file holds the APIs for IPv6 multicast routing 
 *             support with LinuxIp6.
 *
 *******************************************************************/
#ifndef __NETIP6MC_C__
#define __NETIP6MC_C__
#include "lip6inc.h"

/* Reset the option added from pack.h (#pragma pack(1)) to avoid
 * structure misalignment issues */
#pragma pack()
#if __GLIBC__ >= 2 && __GLIBC_MINOR__ >= 9
#include "fssocket.h"
#else
#include <linux/mroute6.h>
#endif
#include "netip6mc.h"
#include "cust.h"

/* Private prototypes */
PRIVATE INT4        LnxIp6McastInit (VOID);
PRIVATE VOID        LnxIp6McastDeInit (VOID);
PRIVATE INT4        LnxIp6McastEnableOnIface (tNetIp6McastInfo *
                                              pNetIp6McastInfo);
PRIVATE INT4        LnxIp6McastDisableOnIface (tNetIp6McastInfo *
                                               pNetIp6McastInfo);
PRIVATE INT4        LnxIpv6McastFindFreeMifId (UINT2 *pu2MifId);
PRIVATE INT4        LnxIp6McastAddRoute (tNetIp6McRouteInfo * pIp6McastRoute);
PRIVATE INT4        LnxIp6McastDelRoute (tNetIp6McRouteInfo * pIp6McastRoute);
PRIVATE VOID        LnxIp6McastHandlePacket (VOID);
PRIVATE VOID        LnxIp6McastPostPkt (tCRU_BUF_CHAIN_HEADER * pBuf,
                                        UINT4 u4Len, tIp6Addr * pSrcAddr,
                                        UINT4 u4IfIndex);
PRIVATE INT4        LnxIpv6McastAddCpuPort (tNetIp6McRouteInfo *
                                            pIp6McastRoute);

/****************************************************************************
* FUNCTION NAME    : LnxIp6McastTaskMain
* DESCRIPTION      : Main task Calls the intitialization routines and give 
*                    indication to the main task.
* INPUT            : pParam - Unused
* OUTPUT           : None
* RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
****************************************************************************/
INT4
LnxIp6McastTaskMain (INT1 *pParam)
{
    UNUSED_PARAM (pParam);

    if (LnxIp6McastInit () == OSIX_FAILURE)
    {
        lrInitComplete (OSIX_FAILURE);
        return OSIX_FAILURE;
    }

    lrInitComplete (OSIX_SUCCESS);

    /* Receive multicast packets from the MLD socket */
    while (1)
    {
        LnxIp6McastHandlePacket ();
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
* FUNCTION NAME    : NetIpv6McastSetMcStatusOnIface 
* DESCRIPTION      : Enables/Disables multicast routing on an interface
*                    according to u4McastStatus. 
* INPUT            : pNetIp6McastInfo: contains the ipport and the 
*                    corresponding multicast routing information 
*                    u4McastStatus : NETIPV6_ENABLED/NETIPV6_DISABLED
* OUTPUT           : None
* RETURNS          : NETIPV6_SUCCESS/NETIPV6_FAILURE
****************************************************************************/
INT4
NetIpv6McastSetMcStatusOnIface (tNetIp6McastInfo * pNetIp6McastInfo,
                                UINT4 u4McastStatus)
{
    INT4                i4RetVal = NETIPV6_FAILURE;

    if (pNetIp6McastInfo == NULL)
    {
        return i4RetVal;
    }

    NETIP6MC_MUTEX_LOCK ();

    if (u4McastStatus == NETIPV6_ENABLED)
    {
        i4RetVal = LnxIp6McastEnableOnIface (pNetIp6McastInfo);
    }

    if (u4McastStatus == NETIPV6_DISABLED)
    {
        i4RetVal = LnxIp6McastDisableOnIface (pNetIp6McastInfo);
    }

    NETIP6MC_MUTEX_UNLOCK ();

    return i4RetVal;
}

/****************************************************************************
* FUNCTION NAME    : NetIpv6McastRouteUpdate
* DESCRIPTION      : Adds/deletes multicast route as per u4RouteFlag.
* INPUT            : pIp6McastRoute: Route information
*                    u4RouteFlag : NETIPV6_ADD_ROUTE/NETIPV6_DELETE_ROUTE
* OUTPUT           : None.
* RETURNS          : NETIPV6_SUCCESS/NETIPV6_FAILURE
****************************************************************************/
INT4
NetIpv6McastRouteUpdate (tNetIp6McRouteInfo * pIp6McastRoute, UINT4 u4RouteFlag)
{
    INT4                i4RetVal = NETIPV6_FAILURE;

    if (pIp6McastRoute == NULL)
    {
        return i4RetVal;
    }

    NETIP6MC_MUTEX_LOCK ();

    if (u4RouteFlag == NETIPV6_ADD_ROUTE)
    {
        i4RetVal = LnxIp6McastAddRoute (pIp6McastRoute);
    }

    if (u4RouteFlag == NETIPV6_DELETE_ROUTE)
    {
        i4RetVal = LnxIp6McastDelRoute (pIp6McastRoute);
    }

    NETIP6MC_MUTEX_UNLOCK ();

    return i4RetVal;
}

/****************************************************************************
* FUNCTION NAME    : NetIpv6McastUpdateCpuPortStatus
* DESCRIPTION      : Creates/Deletes CPU Port into/from LinuxIp6 when PIM
*                    is enabled/disabled, as per the value of u4CpuPortStatus.
* INPUT            : u4CpuPortStatus :
*                    ENABLED - Creates MIF in LnxIp6 for CpuPort.
*                    DISABLED -Deletes MIF for CPUPort from LnxIp6.
* OUTPUT           : None
* RETURNS          : NETIPV6_SUCCESS/NETIPV6_FAILURE
****************************************************************************/
INT4
NetIpv6McastUpdateCpuPortStatus (UINT4 u4CpuPortStatus)
{
    struct mif6ctl      MifEntry;
    mifi_t              MIfIndex = NETIP6MC_CPUPORT_MIFID;
    INT4                i4RetVal = NETIPV6_SUCCESS;

    MEMSET (&MifEntry, 0, sizeof (struct mif6ctl));

    NETIP6MC_MUTEX_LOCK ();

    if (u4CpuPortStatus == NETIPV6_ENABLED)
    {
        MifEntry.mif6c_mifi = MIfIndex;
        MifEntry.mif6c_flags = MIFF_REGISTER;

        if (setsockopt (gNetIp6McGlobalInfo.i4NetIp6SockId, IPPROTO_IPV6,
                        MRT6_ADD_MIF, (void *) &MifEntry,
                        sizeof (struct mif6ctl)) != 0)
        {
            perror ("NetIpv6McastUpdateCpuPortStatus-MRT6_ADD_MIF Failed");
            i4RetVal = NETIPV6_FAILURE;
        }
    }

    if (u4CpuPortStatus == NETIPV6_DISABLED)
    {
        if (setsockopt (gNetIp6McGlobalInfo.i4NetIp6SockId, IPPROTO_IPV6,
                        MRT6_DEL_MIF, (void *) &MIfIndex, sizeof (mifi_t)) != 0)
        {
            perror ("NetIpv6McastUpdateCpuPortStatus-MRT6_DEL_MIF Failed");
            i4RetVal = NETIPV6_FAILURE;
        }
    }

    NETIP6MC_MUTEX_UNLOCK ();

    return i4RetVal;
}

/****************************************************************************
* FUNCTION NAME    : NetIpv6McastUpdateRouteCpuPort
* DESCRIPTION      : Adds/Deletes CPU Port into/from LinuxIp6 route entry
*                    Oif list as per the value of u4CpuPortStatus.
* INPUT            : tNetIp6McRouteInfo * : Multicast route info
*                    UINT4 : CPU port status. 
* OUTPUT           : None
* RETURNS          : NETIPV6_SUCCESS/NETIPV6_FAILURE
****************************************************************************/
INT4
NetIpv6McastUpdateRouteCpuPort (tNetIp6McRouteInfo * pIp6McastRoute,
                                UINT4 u4CpuPortStatus)
{
    INT4                i4RetVal = NETIPV6_FAILURE;

    if (pIp6McastRoute == NULL)
    {
        return i4RetVal;
    }

    NETIP6MC_MUTEX_LOCK ();

    if (u4CpuPortStatus == NETIPV6_ADD_ROUTE)
    {
        i4RetVal = LnxIpv6McastAddCpuPort (pIp6McastRoute);
    }

    if (u4CpuPortStatus == NETIPV6_DELETE_ROUTE)
    {
        /* In LinuxIp6, any change in route needs addition of whole
         * route information to ip6_mr_cache. Therefore, deleting the CPUPort
         * means adding the route again to LinuxIp6, which automatically will
         * delete the cpuport.
         */

        i4RetVal = LnxIp6McastAddRoute (pIp6McastRoute);
    }

    NETIP6MC_MUTEX_UNLOCK ();

    return i4RetVal;
}

/****************************************************************************
* FUNCTION NAME    : LnxIp6McastInit 
* DESCRIPTION      : Initialises multicast routing in linuxip.
* INPUT            : None
* OUTPUT           : None
* RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
****************************************************************************/
PRIVATE INT4
LnxIp6McastInit (VOID)
{
    INT4                u4OptVal = TRUE;
    INT4                i4SockId = 0;
    INT4                i4RetVal = 0;
    INT1                i1Index = 0;

    /* Create semaphore */
    if (OsixSemCrt (NETIP6MC_MUTEX_SEMNAME, &(gNetIp6McGlobalInfo.NetIp6SemId))
        == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    OsixSemGive (gNetIp6McGlobalInfo.NetIp6SemId);

    /* Create socket with ICMPV6 proto. MLD is part of ICMPv6 */
    i4SockId = socket (AF_INET6, SOCK_RAW, IPPROTO_ICMPV6);
    if (i4SockId < 0)
    {
        perror ("LnxIp6McastInit-socket creation failed.");
        OsixSemDel (gNetIp6McGlobalInfo.NetIp6SemId);
        return OSIX_FAILURE;
    }

    gNetIp6McGlobalInfo.i4NetIp6SockId = i4SockId;

    /* set socket option as MRT6_INIT, initalize mrouting v6 */
    i4RetVal = setsockopt (i4SockId, IPPROTO_IPV6, MRT6_INIT, &u4OptVal,
                           sizeof (u4OptVal));
    if (i4RetVal != 0)
    {
        perror ("LnxIp6McastInit-MRT6_INIT failed");
        LnxIp6McastDeInit ();
        return OSIX_FAILURE;
    }

    /* set socket option as MRT6_ASSERT */
    if (setsockopt (i4SockId, IPPROTO_IPV6, MRT6_ASSERT, &u4OptVal,
                    sizeof (u4OptVal)) != 0)
    {
        perror ("LnxIp6McastInit-MRT6_ASSERT failed");
        LnxIp6McastDeInit ();
        return OSIX_FAILURE;
    }

    /* set socket option as MRT6_PIM */
    if (setsockopt (i4SockId, IPPROTO_IPV6, MRT6_PIM, &u4OptVal,
                    sizeof (u4OptVal)) != 0)
    {
        perror ("LnxIp6McastInit-MRT6_PIM failed");
        LnxIp6McastDeInit ();
        return OSIX_FAILURE;
    }

    /* Initialize all the globale variables  */
    for (i1Index = 0; i1Index < MAXMIFS; i1Index++)
    {
        gNetIp6McGlobalInfo.u2MifId[i1Index] = NETIP6MC_MIF_FREE;
        gNetIp6McGlobalInfo.u4PortIndex[i1Index] = NETIP6MC_INVALID_INDEX;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
* FUNCTION NAME    : LnxIp6McastDeInit
* DESCRIPTION      : Deinitialises multicast routing in linuxip.
*                    and deletes the semaphore.
* INPUT            : None
* OUTPUT           : None
* RETURNS          : None
****************************************************************************/
PRIVATE VOID
LnxIp6McastDeInit (VOID)
{
    NETIP6MC_MUTEX_LOCK ();
    close (gNetIp6McGlobalInfo.i4NetIp6SockId);

    gNetIp6McGlobalInfo.i4NetIp6SockId = -1;
    NETIP6MC_MUTEX_UNLOCK ();

    OsixSemDel (gNetIp6McGlobalInfo.NetIp6SemId);
}

/****************************************************************************
* FUNCTION NAME    : LnxIp6McastEnableOnIface
* DESCRIPTION      : Enables multicasting on a Mif and adds to ip_mr_Mif.
* INPUT            : pNetIp6McastInfo: contains the iface number on which
*                    multicasting is enabled and the multicast protocol
*                    (PIMv6,ICMPv6) to be enabled on the interface.
* OUTPUT           : None
* RETURNS          : NETIPV6_SUCCESS/NETIPV6_FAILURE
****************************************************************************/
PRIVATE INT4
LnxIp6McastEnableOnIface (tNetIp6McastInfo * pNetIp6McastInfo)
{
    struct mif6ctl      MifEntry;
    tLip6If            *pIf6Entry = NULL;
    UINT2               u2FreeMifId = 0;
    UINT2               u2McastProtocol = 0;

    MEMSET (&MifEntry, 0, sizeof (struct mif6ctl));

    u2McastProtocol = pNetIp6McastInfo->u2McastProtocol;

    /* Get the Interface entry */
    Ip6Lock ();
    pIf6Entry = Lip6UtlGetIfEntry (pNetIp6McastInfo->u4IfIndex);
    Ip6UnLock ();

    if (pIf6Entry == NULL)
    {
        return NETIPV6_FAILURE;
    }

    if ((u2McastProtocol != PIM_ID) && (u2McastProtocol != ICMPV6_PROTOCOL_ID))
    {
        return NETIPV6_FAILURE;
    }

    /* pIf6Entry->u4McProtocols is bit mask. If it's value is zero
     * no protocol is enabled on the port. So create the Mif for it.*/

    if (pIf6Entry->u4McProtocols == 0)
    {
        /* Find a free MifId */
        if (LnxIpv6McastFindFreeMifId (&u2FreeMifId) == NETIPV6_SUCCESS)
        {
            pIf6Entry->u2Mif = u2FreeMifId;
            gNetIp6McGlobalInfo.u2MifId[u2FreeMifId] = NETIP6MC_MIF_ALLOC;
            gNetIp6McGlobalInfo.u4PortIndex[u2FreeMifId] = pIf6Entry->u4IpPort;

            /* Add the mif to ip_mr_mif */
            MifEntry.mif6c_mifi = u2FreeMifId;
            MifEntry.mif6c_pifi = pIf6Entry->u4IpPort;
            MifEntry.mif6c_flags = 0;

            if (setsockopt (gNetIp6McGlobalInfo.i4NetIp6SockId, IPPROTO_IPV6,
                            MRT6_ADD_MIF, &MifEntry, sizeof (struct mif6ctl))
                != 0)
            {
                perror ("LnxIp6McastEnableOnIface-MRT6_ADD_MIF Failed");
                return NETIPV6_FAILURE;
            }
        }
        else
        {
            return NETIPV6_FAILURE;
        }
    }

    /* Update pIf6Entry with  u2McastProtocol */
    if (u2McastProtocol == PIM_ID)
    {
        pIf6Entry->u4McProtocols |= NETIP6MC_PIM_ID;
    }
    else
    {
        pIf6Entry->u4McProtocols |= NETIP6MC_ICMPV6_ID;
    }

    return NETIPV6_SUCCESS;
}

/****************************************************************************
* FUNCTION NAME    : LnxIp6McastDisableOnIface
* DESCRIPTION      : Disables multicasting from a Mif and removes 
*                    from ip_mr_Mif 
* INPUT            : pNetIp6McastInfo: contains the port number on which
*                    multicasting is disabled, and the multicast protocol
*                    (PIMv6, ICMPv6) to be disabled on the i/f
* OUTPUT           : None
* RETURNS          : NETIPV6_SUCCESS/NETIPV6_FAILURE 
****************************************************************************/
PRIVATE INT4
LnxIp6McastDisableOnIface (tNetIp6McastInfo * pNetIp6McastInfo)
{
    tLip6If            *pIf6Entry = NULL;
    mifi_t              MIfIndex;
    UINT2               u2McastProtocol = 0;

    MEMSET (&MIfIndex, 0, sizeof (mifi_t));

    u2McastProtocol = pNetIp6McastInfo->u2McastProtocol;

    /* Get the Interface node */
    Ip6Lock ();
    pIf6Entry = Lip6UtlGetIfEntry (pNetIp6McastInfo->u4IfIndex);
    Ip6UnLock ();
    if (pIf6Entry == NULL)
    {
        return NETIPV6_FAILURE;
    }

    /* No ptotocol is enabled on the interface */
    if (pIf6Entry->u4McProtocols == 0)
    {
        return NETIPV6_FAILURE;
    }

    if (u2McastProtocol == PIM_ID)
    {
        pIf6Entry->u4McProtocols &= ~NETIP6MC_PIM_ID;
    }
    else if (u2McastProtocol == ICMPV6_PROTOCOL_ID)
    {
        pIf6Entry->u4McProtocols &= ~NETIP6MC_ICMPV6_ID;
    }
    else
    {
        /* Invalid McastProtocol */
        return NETIPV6_FAILURE;
    }

    /* pIf6Entry->u4McProtocols is bit mask. If it's value is zero
     * not protocol is enabled on the port.So can delete the Mif.
     * */
    if (pIf6Entry->u4McProtocols == 0)
    {
        MIfIndex = pIf6Entry->u2Mif;

        gNetIp6McGlobalInfo.u2MifId[MIfIndex] = NETIP6MC_MIF_FREE;
        gNetIp6McGlobalInfo.u4PortIndex[MIfIndex] = NETIP6MC_INVALID_INDEX;

        if (setsockopt (gNetIp6McGlobalInfo.i4NetIp6SockId, IPPROTO_IPV6,
                        MRT6_DEL_MIF, &MIfIndex, sizeof (mifi_t)) != 0)
        {
            perror ("LnxIp6McastDisableOnIface-MRT6_DEL_MIF Failed:");
            return NETIPV6_FAILURE;
        }
    }

    return NETIPV6_SUCCESS;
}

/****************************************************************************
* FUNCTION NAME    : LnxIp6McastAddRoute
* DESCRIPTION      : Adds multicast route entry into linuxip6.
* INPUT            : pIp6McastRoute : Multicast route info.
* OUTPUT           : None 
* RETURNS          : NETIPV6_SUCCESS/NETIPV6_FAILURE
****************************************************************************/
PRIVATE INT4
LnxIp6McastAddRoute (tNetIp6McRouteInfo * pIp6McastRoute)
{
    UINT4               u4OifCnt = 0;
    UINT2               u2Index = 0;
    tLip6If            *pIf6Entry = NULL;
    tLip6If            *pIf6OifEntry = NULL;
    tNetIp6Oif         *pDsIf = NULL;
    tNetIp6Oif         *pOIf = NULL;
    struct mf6cctl      McRtEntry;

    MEMSET (&McRtEntry, 0, sizeof (struct mf6cctl));

    Ip6Lock ();
    pIf6Entry = Lip6UtlGetIfEntry (pIp6McastRoute->u4Iif);
    Ip6UnLock ();
    if (pIf6Entry == NULL)
    {
        return NETIPV6_FAILURE;
    }

    McRtEntry.mf6cc_origin.sin6_family = AF_INET6;
    MEMCPY (&McRtEntry.mf6cc_origin.sin6_addr, &(pIp6McastRoute->SrcAddr),
            sizeof (tIp6Addr));
    McRtEntry.mf6cc_mcastgrp.sin6_family = AF_INET6;
    MEMCPY (&McRtEntry.mf6cc_mcastgrp.sin6_addr, &(pIp6McastRoute->GrpAddr),
            sizeof (tIp6Addr));
    McRtEntry.mf6cc_parent = pIf6Entry->u2Mif;

    u4OifCnt = pIp6McastRoute->u4OifCnt;
    pOIf = pIp6McastRoute->pOIf;

    /* Update the OifList */
    pDsIf = pOIf;
    for (u2Index = 0; u2Index < u4OifCnt; u2Index++)
    {
        pIf6OifEntry = NULL;

        /*Get Mif from IfIndex */
        Ip6Lock ();
        pIf6OifEntry = Lip6UtlGetIfEntry (pDsIf[u2Index].u4IfIndex);
        Ip6UnLock ();
        if (pIf6OifEntry == NULL)
        {
            return NETIPV6_FAILURE;
        }

        /* Set out interface */
        IF_SET (pIf6OifEntry->u2Mif, &McRtEntry.mf6cc_ifset);
    }

    /* Add the multicast entry into linuxip. */

    if (setsockopt (gNetIp6McGlobalInfo.i4NetIp6SockId, IPPROTO_IPV6,
                    MRT6_ADD_MFC, (VOID *) &McRtEntry,
                    sizeof (struct mf6cctl)) < 0)
    {
        perror ("LnxIp6McastAddRoute- MRT6_ADD_MFC Failed.\r\n");
        return NETIPV6_FAILURE;
    }
    return NETIPV6_SUCCESS;
}

/****************************************************************************
* FUNCTION NAME    : LnxIp6McastDelRoute
* DESCRIPTION      : Deletes multicast route from linuxip. 
* INPUT            : pIp6McastRoute : Multicast route info.
* OUTPUT           : None
* RETURNS          : NETIPV6_SUCCESS/NETIPV6_FAILURE
****************************************************************************/
PRIVATE INT4
LnxIp6McastDelRoute (tNetIp6McRouteInfo * pIp6McastRoute)
{
    struct mf6cctl      McRtEntry;

    MEMSET (&McRtEntry, 0, sizeof (struct mf6cctl));

    McRtEntry.mf6cc_origin.sin6_family = AF_INET6;
    MEMCPY (&McRtEntry.mf6cc_origin.sin6_addr, &(pIp6McastRoute->SrcAddr),
            sizeof (tIp6Addr));
    McRtEntry.mf6cc_mcastgrp.sin6_family = AF_INET6;
    MEMCPY (&McRtEntry.mf6cc_mcastgrp.sin6_addr, &(pIp6McastRoute->GrpAddr),
            sizeof (tIp6Addr));

    if (setsockopt (gNetIp6McGlobalInfo.i4NetIp6SockId, IPPROTO_IPV6,
                    MRT6_DEL_MFC, (VOID *) &McRtEntry,
                    sizeof (struct mf6cctl)) < 0)
    {
        perror ("LnxIp6McastDelRoute- MRT6_DEL_MFC Failed.\r\n");
        return NETIPV6_FAILURE;
    }

    return NETIPV6_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LnxIpv6McastFindFreeMifId
 *
 *    DESCRIPTION      : returns a free MifId
 *
 *    INPUT            : None
 *
 *    OUTPUT           : *pu2MifId
 *
 *    RETURNS          : NETIPV6_SUCCESS/NETIPV6_FAILURE
 *
 ****************************************************************************/
PRIVATE INT4
LnxIpv6McastFindFreeMifId (UINT2 *pu2MifId)
{
    UINT2               u2Index = 0;

    *pu2MifId = NETIP6MC_MIF_FREE;

    for (u2Index = 0; u2Index < (MAXMIFS - 1); u2Index++)
    {
        if (gNetIp6McGlobalInfo.u2MifId[u2Index] == NETIP6MC_MIF_FREE)
        {
            *pu2MifId = u2Index;
            return NETIPV6_SUCCESS;
        }
    }

    return NETIPV6_FAILURE;
}

/****************************************************************************
* FUNCTION NAME    : LnxIp6McastHandlePacket
* DESCRIPTION      : This function receives MLD packet from socket and 
*                    posts to registered modules
* INPUT            : NONE
* OUTPUT           : NONE
* RETURNS          : NETIPV6_SUCCESS/NETIPV6_FAILURE
****************************************************************************/
PRIVATE VOID
LnxIp6McastHandlePacket (VOID)
{
    UINT1               au1Cmsg[IP6_DEFAULT_MTU];
    struct sockaddr_in6 Recv_Node;
    struct iovec        Iov;
    struct msghdr       PktInfo;
    struct cmsghdr     *pCmsgInfo = NULL;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    struct in6_pktinfo *pIp6PktInfo = NULL;
    UINT1              *pu1RecvPkt = NULL;
    UINT1               au1RecvPkt[CFA_ENET_MTU];
    INT4                i4RecvBytes = 0, i4Option = 0;
    tIp6Addr            Src6Addr;
    UINT4               u4IfIndex;

    MEMSET (au1Cmsg, 0, sizeof (au1Cmsg));
    MEMSET (&Recv_Node, 0, sizeof (struct sockaddr_in6));
    MEMSET (&PktInfo, 0, sizeof (struct msghdr));
    MEMSET (&Src6Addr, 0, sizeof (tIp6Addr));

    pu1RecvPkt = au1RecvPkt;

    PktInfo.msg_name = (void *) &Recv_Node;
    PktInfo.msg_namelen = sizeof (struct sockaddr_in6);
    Iov.iov_base = pu1RecvPkt;
    Iov.iov_len = CFA_ENET_MTU;
    PktInfo.msg_iov = &Iov;
    PktInfo.msg_iovlen = 1;
    PktInfo.msg_control = (void *) &au1Cmsg;
    PktInfo.msg_controllen = CFA_ENET_MTU;

    i4Option = 1;
    NETIP6MC_MUTEX_LOCK ();
    if (setsockopt (gNetIp6McGlobalInfo.i4NetIp6SockId, IPPROTO_IPV6,
                    IPV6_RECVPKTINFO, &i4Option, sizeof (i4Option)) < 0)
    {
        perror ("LnxIp6McastHandlePacket-IPV6_RECVPKTINFO Failed.\r\n");
        NETIP6MC_MUTEX_UNLOCK ();
        return;
    }

    NETIP6MC_MUTEX_UNLOCK ();

    pCmsgInfo = CMSG_FIRSTHDR (&PktInfo);
    pCmsgInfo->cmsg_level = IPPROTO_IPV6;
    pCmsgInfo->cmsg_type = IPV6_RECVPKTINFO;
    pCmsgInfo->cmsg_len = sizeof (au1Cmsg);

    i4RecvBytes = 0;

    while ((i4RecvBytes =
            recvmsg (gNetIp6McGlobalInfo.i4NetIp6SockId, &PktInfo, 0)) > 0)
    {
        struct cmsghdr     *pCmsgHdr = NULL;
        pCmsgHdr = CMSG_FIRSTHDR (&PktInfo);

        if (pCmsgHdr == NULL)
        {

            MEMSET (au1Cmsg, 0, NETIP6MC_MLD_ANCILLARY_LEN);
            MEMSET (&Recv_Node, 0, sizeof (struct sockaddr_in6));
            MEMSET (&PktInfo, 0, sizeof (struct msghdr));
            MEMSET (&Src6Addr, 0, sizeof (tIp6Addr));

            pu1RecvPkt = au1RecvPkt;

            PktInfo.msg_name = (void *) &Recv_Node;
            PktInfo.msg_namelen = sizeof (struct sockaddr_in6);
            Iov.iov_base = pu1RecvPkt;
            Iov.iov_len = CFA_ENET_MTU;
            PktInfo.msg_iov = &Iov;
            PktInfo.msg_iovlen = 1;
            PktInfo.msg_control = (void *) &au1Cmsg;
            PktInfo.msg_controllen = CFA_ENET_MTU;

            i4Option = 1;
            i4RecvBytes = 0;
            continue;
        }

        pIp6PktInfo =
            (struct in6_pktinfo *) (VOID *)
            CMSG_DATA (CMSG_FIRSTHDR (&PktInfo));

        /* Copy the recvd linear buf to CRU buf */
        pBuf = CRU_BUF_Allocate_MsgBufChain (i4RecvBytes, 0);
        if (pBuf == NULL)
        {
            return;
        }

        CRU_BUF_Copy_OverBufChain (pBuf, pu1RecvPkt, 0, i4RecvBytes);

        /* Copy the source address */
        MEMCPY (&Src6Addr, &Recv_Node.sin6_addr, sizeof (tIp6Addr));

        NetIpv6GetCfaIfIndexFromPort ((UINT4) pIp6PktInfo->ipi6_ifindex,
                                      &u4IfIndex);

        /* Post packet to registered modules */
        LnxIp6McastPostPkt (pBuf, i4RecvBytes, &Src6Addr, u4IfIndex);

        MEMSET (au1Cmsg, 0, NETIP6MC_MLD_ANCILLARY_LEN);
        MEMSET (&Recv_Node, 0, sizeof (struct sockaddr_in6));
        MEMSET (&PktInfo, 0, sizeof (struct msghdr));
        MEMSET (&Src6Addr, 0, sizeof (tIp6Addr));

        pu1RecvPkt = au1RecvPkt;

        PktInfo.msg_name = (void *) &Recv_Node;
        PktInfo.msg_namelen = sizeof (struct sockaddr_in6);
        Iov.iov_base = pu1RecvPkt;
        Iov.iov_len = CFA_ENET_MTU;
        PktInfo.msg_iov = &Iov;
        PktInfo.msg_iovlen = 1;
        PktInfo.msg_control = (void *) &au1Cmsg;
        PktInfo.msg_controllen = CFA_ENET_MTU;

        i4Option = 1;
        i4RecvBytes = 0;
    }
}

/****************************************************************************
* FUNCTION NAME    : LnxIp6McastPostPkt
* DESCRIPTION      : This function posts the received multicast packets
*                    to registered functions
* INPUT            : pBuf - Pointer to packet buffer
*                    u4Len - buffer length
*                    pSrcAddr - Source IPv6 address
*                    u4IfIndex -Received interface index.
* OUTPUT           : NONE
* RETURNS          : NETIPV6_SUCCESS/NETIPV6_FAILURE
****************************************************************************/
PRIVATE VOID
LnxIp6McastPostPkt (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4Len,
                    tIp6Addr * pSrcAddr, UINT4 u4IfIndex)
{
    struct mrt6msg      IcmpHdr;
    tNetIpv6HliParams   Ip6HlParams;
    tMODULE_DATA       *pModuleData = NULL;
    tCRU_BUF_CHAIN_HEADER *pDupBuf = NULL;
    struct mrt6msg     *pIcmpHdr = NULL;
    UINT1               u1Index = 0;

    pIcmpHdr = (struct mrt6msg *) (VOID *)
        CRU_BUF_Get_DataPtr_IfLinear (pBuf, 0, sizeof (struct mrt6msg));

    if (pIcmpHdr == NULL)
    {
        /* The header is not contiguous in the buffer */
        MEMSET (&IcmpHdr, 0, sizeof (struct mrt6msg));
        pIcmpHdr = &IcmpHdr;
        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) pIcmpHdr, 0,
                                   sizeof (struct mrt6msg));
    }

    /* Post packet to registered modules */
    Ip6Lock ();

    if (pIcmpHdr->im6_mbz == 0)
    {
        /* Packet is data packet. Post it to the IPv6 multicast routing 
           protocols (PIMv6) */

        pModuleData = &pBuf->ModuleData;

        /* In case of the MRT6MSG_NOCAHE and MRT6MSG_WRONGMIF incomming 
         * interface index is derived from MIF. Otherwise the incomming
         * interface index is given as zero by IPV6_RECVPKTINFO */

        /* In case of the MRT6MSG_WHOLEPKT also incomming
         * interface index is derived from MIF. */
        NETIP6MC_MUTEX_LOCK ();
        pModuleData->InterfaceId.u4IfIndex =
            gNetIp6McGlobalInfo.u4PortIndex[pIcmpHdr->im6_mif];
        NETIP6MC_MUTEX_UNLOCK ();

        while ((u1Index < IP6_MAX_MCAST_PROTOCOLS) &&
               (gaNetIpv6MCastRegTable[u1Index].u1RegFlag == IP6_REGISTER) &&
               (gaNetIpv6MCastRegTable[u1Index].pAppRcv != NULL))
        {
            /* Dulpicate the buffer and give it to all the registered 
             * protocols */
            pDupBuf = CRU_BUF_Duplicate_BufChain (pBuf);

            if (pDupBuf != NULL)
            {
                gaNetIpv6MCastRegTable[u1Index].pAppRcv (pDupBuf);
            }

            u1Index++;
        }

        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
    }
    else
    {
        /* Packet is control packet. Post it to MLD */
        MEMSET (&Ip6HlParams, 0, sizeof (tNetIpv6HliParams));

        Ip6HlParams.u4Command = NETIPV6_APPLICATION_RECEIVE;
        Ip6HlParams.unIpv6HlCmdType.AppRcv.pBuf = pBuf;
        Ip6HlParams.unIpv6HlCmdType.AppRcv.u4PktLength = u4Len;
        Ip6HlParams.unIpv6HlCmdType.AppRcv.u4Index = u4IfIndex;
        MEMCPY (&(Ip6HlParams.unIpv6HlCmdType.AppRcv.Ip6SrcAddr), pSrcAddr,
                sizeof (tIp6Addr));

        if (gaNetIpv6RegTable[ICMPV6_PROTOCOL_ID].pAppRcv != NULL)
        {
            gaNetIpv6RegTable[ICMPV6_PROTOCOL_ID].pAppRcv (&Ip6HlParams);
        }
    }

    Ip6UnLock ();
    return;
}

/****************************************************************************
* FUNCTION NAME  : LnxIpv6McastAddCpuPort
* DESCRIPTION    : Adds CPUPort into LinuxIp6 for the input route entry
* INPUT          : pIpMcastRoute : Multicast route info as follows:
* OUTPUT         : None
* RETURNS        : NETIPV6_SUCCESS/NETIPV6_FAILURE
****************************************************************************/
PRIVATE INT4
LnxIpv6McastAddCpuPort (tNetIp6McRouteInfo * pIp6McastRoute)
{
    struct mf6cctl      McRtEntry;
    tLip6If            *pIf6Entry = NULL;
    tLip6If            *pIf6OifEntry = NULL;
    tNetIp6Oif         *pDsIf = NULL;
    tNetIp6Oif         *pOIf = NULL;
    UINT4               u4OifCnt = 0;
    UINT2               u2Index = 0;

    MEMSET (&McRtEntry, 0, sizeof (struct mf6cctl));

    /* Get the interface Node for the Iif */
    Ip6Lock ();
    pIf6Entry = Lip6UtlGetIfEntry (pIp6McastRoute->u4Iif);
    Ip6UnLock ();
    if (pIf6Entry == NULL)
    {
        return NETIPV6_FAILURE;
    }

    McRtEntry.mf6cc_origin.sin6_family = AF_INET6;
    MEMCPY (&McRtEntry.mf6cc_origin.sin6_addr, &(pIp6McastRoute->SrcAddr),
            sizeof (tIp6Addr));
    McRtEntry.mf6cc_mcastgrp.sin6_family = AF_INET6;
    MEMCPY (&McRtEntry.mf6cc_mcastgrp.sin6_addr, &(pIp6McastRoute->GrpAddr),
            sizeof (tIp6Addr));
    McRtEntry.mf6cc_parent = pIf6Entry->u2Mif;

    u4OifCnt = pIp6McastRoute->u4OifCnt;
    pOIf = pIp6McastRoute->pOIf;

    /* update the OifList */
    pDsIf = pOIf;
    for (u2Index = 0; u2Index < u4OifCnt; u2Index++)
    {
        pIf6OifEntry = NULL;

        Ip6Lock ();
        pIf6OifEntry = Lip6UtlGetIfEntry (pDsIf[u2Index].u4IfIndex);
        Ip6UnLock ();
        if (pIf6OifEntry == NULL)
        {
            return NETIPV6_FAILURE;
        }

        /* Set out interface */
        IF_SET (pIf6OifEntry->u2Mif, &McRtEntry.mf6cc_ifset);
    }

    /* Add the CPU port now */
    IF_SET (NETIP6MC_CPUPORT_MIFID, &McRtEntry.mf6cc_ifset);

    /* Add the multicast entry into linuxip. */

    if (setsockopt (gNetIp6McGlobalInfo.i4NetIp6SockId, IPPROTO_IPV6,
                    MRT6_ADD_MFC, (VOID *) &McRtEntry,
                    sizeof (struct mf6cctl)) < 0)
    {
        perror ("LnxIpv6McastAddCpuPort - MRT6_ADD_MFC Failed.\r\n");
        return NETIPV6_FAILURE;
    }

    return NETIPV6_SUCCESS;
}
#endif /* __NETIP6MC_C__ */
/*-----------------------------------------------------------------------*/
/*                       End of the file  netip6mc.c                     */
/*-----------------------------------------------------------------------*/
