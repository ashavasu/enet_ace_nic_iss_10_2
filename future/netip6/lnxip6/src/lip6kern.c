/*****************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: lip6kern.c,v 1.14 2015/11/20 10:36:01 siva Exp $
 *
 * Description: Routines that configures Linux networking
 *
 ****************************************************************************/

#ifndef _LIP6KERN_C_
#define _LIP6KERN_C_

#include "lip6inc.h"
#include "lip6stat.h"
#include "fsutil.h"

/****************************************************************************
 * Function    : Lip6KernSetForwarding
 * Description : Enable/Disable Ip6 Forwarding in Linux IP globally
 * Input       : u1Status - Status to set (LIP6_FORW_ENABLE/LIP6_FORW_DISABLE)
 * OutPut      : None
 * Returns     : OSIX_SUCCESS/OSIX_FAILURE
 ***************************************************************************/
PUBLIC INT4
Lip6KernSetForwarding (INT4 i4SetVal)
{
    CHR1                ac1Command[LIP6_LINE_LEN];
    UINT1               u1LnxSetVal = 0;
    tLip6Cxt            *pIp6Cxt = NULL;
    UINT4                u4ContextId = VCM_DEFAULT_CONTEXT;
#if defined(VRF_WANTED) && defined(LINUX_310_WANTED)
    tLnxVrfInfo          *pLnxVrfInfo = NULL;
#endif

    if (i4SetVal == LIP6_FORW_ENABLE)
    {
        u1LnxSetVal = 1;
    }
    else if (i4SetVal == LIP6_FORW_DISABLE)
    {
        u1LnxSetVal = 0;
    }
    else
    {
        return OSIX_FAILURE;
    }
#if defined(VRF_WANTED) && defined(LINUX_310_WANTED)
    u4ContextId = Lip6GetCurrentContext();
    if(u4ContextId != VCM_DEFAULT_CONTEXT)
    {
        pLnxVrfInfo = LnxVrfInfoGet (u4ContextId);
        if (pLnxVrfInfo == NULL)
        {
            return OSIX_FAILURE;
        }

        if (Ip6UtilChangeCxt (u4ContextId, LNX_VRF_NON_DEFAULT_NS,
                              pLnxVrfInfo->au1NameSpace) == (UINT4)NETIPV6_FAILURE)
        {
            return OSIX_FAILURE;
        }
    }

#endif

    MEMSET (ac1Command, 0, sizeof (ac1Command));

    SNPRINTF (ac1Command, sizeof (ac1Command),
              "echo %u > %s/%s/%s",
              u1LnxSetVal, LIP6_PROC_CONF, "all", LIP6_PROC_FWD);
    system (ac1Command);

    /* Enable source route by default */
    MEMSET (ac1Command, 0, sizeof (ac1Command));
    SNPRINTF (ac1Command, sizeof (ac1Command),
              "echo %u > %s/%s/%s",
              u1LnxSetVal, LIP6_PROC_CONF, "default", LIP6_SRC_ROUTE);
    system (ac1Command);
#if defined(VRF_WANTED) && defined(LINUX_310_WANTED)
    if (Ip6UtilChangeCxt (u4ContextId, LNX_VRF_DEFAULT_NS,
                          (UINT1 *)LNX_VRF_DEFAULT_NS_PID) == (UINT4)NETIPV6_FAILURE)

    {
        return OSIX_FAILURE;
    }
#endif

    if(u4ContextId != VCM_DEFAULT_CONTEXT)
    {
	    pIp6Cxt = gIp6GblInfo.apIp6Cxt[u4ContextId];
	    if (pIp6Cxt!= NULL)
	    {
		    pIp6Cxt->u4ForwFlag = (UINT4)i4SetVal; 
	    }
    }
    else
    {
	    gIp6GblInfo.i4Ip6Status = i4SetVal;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    : Lip6KernSetDefaultHopLimit

 Description : Set Default IPv6 HopLimit for all interfaces

 Input       : i4SetVal - HopLimit value to be set

 OutPut      : None

 Returns     : OSIX_SUCCESS/OSIX_FAILURE
****************************************************************************/
PUBLIC INT4
Lip6KernSetDefaultHopLimit (INT4 i4SetVal)
{
    CHR1                ac1Command[LIP6_LINE_LEN];

    MEMSET (ac1Command, 0, sizeof (ac1Command));
    SNPRINTF (ac1Command, sizeof (ac1Command),
              "echo %d >%s/%s/%s",
              i4SetVal, LIP6_PROC_CONF, "all", LIP6_PROC_HOPLIMIT);

    system ((const char *) ac1Command);

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    : Lip6KernSetIfDad

 Description : Enable/Disable Interface specific Ipv6 DAD

 Input       : pu1IfName - Interface name in Linux
               i4SetVal - Status to be set

 OutPut      : None

 Returns     : OSIX_SUCCESS/OSIX_FAILURE
****************************************************************************/
PUBLIC INT4
Lip6KernSetIfDad (UINT1 *pu1IfName, INT4 i4SetVal)
{
    CHR1                ac1Command[LIP6_LINE_LEN];
    UINT4               u4ContextId = VCM_DEFAULT_CONTEXT;
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    tLnxVrfIfInfo *pLnxVrfIfInfo = NULL;
    tLnxVrfInfo        *pLnxVrfInfo = NULL;
#endif


    if ((i4SetVal != LIP6_DAD_ENABLE) && (i4SetVal != LIP6_DAD_DISABLE))
    {
        return OSIX_FAILURE;
    }

    MEMSET (ac1Command, 0, sizeof (ac1Command));

#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    pLnxVrfIfInfo = LnxVrfIfInfoGet(pu1IfName);
    if(pLnxVrfIfInfo != NULL)
    {
        u4ContextId = pLnxVrfIfInfo->u4ContextId;
    }
    if(u4ContextId != VCM_DEFAULT_CONTEXT)
    {
        pLnxVrfInfo = LnxVrfInfoGet (u4ContextId);
        if (pLnxVrfInfo == NULL)
        {
            return OSIX_FAILURE;
        }
    }

    if(Ip6UtilChangeCxt (u4ContextId, LNX_VRF_NON_DEFAULT_NS, 
                         pLnxVrfInfo->au1NameSpace) == (UINT4)NETIPV6_FAILURE)
    {
        return OSIX_FAILURE;
    }
#endif

    SNPRINTF (ac1Command, sizeof (ac1Command),
              "echo %d > %s/%s/%s",
              i4SetVal, LIP6_PROC_CONF, pu1IfName, LIP6_PROC_DADENABLE);

    system (ac1Command);
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    if(Ip6UtilChangeCxt (u4ContextId, LNX_VRF_DEFAULT_NS,
                         (UINT1 *)LNX_VRF_DEFAULT_NS_PID) == (UINT4)NETIPV6_FAILURE)
    {
        return OSIX_FAILURE;
    }
#else
    UNUSED_PARAM(u4ContextId);
#endif

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    : Lip6KernSetIfIpv6Disable

 Description : Enable/Disable Interface specific Ipv6 Disable

 Input       : pu1IfName - Interface name in Linux
               i4SetVal - Status to be set

 OutPut      : None

 Returns     : OSIX_SUCCESS/OSIX_FAILURE
****************************************************************************/
PUBLIC INT4
Lip6KernSetIfIpv6Disable (UINT1 *pu1IfName, INT4 i4SetVal)
{
    CHR1                ac1Command[LIP6_LINE_LEN];
    UINT4               u4ContextId = VCM_DEFAULT_CONTEXT;
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    tLnxVrfIfInfo *pLnxVrfIfInfo = NULL;
    tLnxVrfInfo        *pLnxVrfInfo = NULL;
#endif

    if ((i4SetVal != LIP6_IPV6_ENABLE) && (i4SetVal != LIP6_IPV6_DISABLE))
    {
        return OSIX_FAILURE;
    }

    MEMSET (ac1Command, 0, sizeof (ac1Command));
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)

    pLnxVrfIfInfo = LnxVrfIfInfoGet(pu1IfName);
    if(pLnxVrfIfInfo != NULL)
    {
        u4ContextId = pLnxVrfIfInfo->u4ContextId;
    }
    if(u4ContextId != VCM_DEFAULT_CONTEXT)
    {
        pLnxVrfInfo = LnxVrfInfoGet (u4ContextId);
        if (pLnxVrfInfo == NULL)
        {
            return OSIX_FAILURE;
        }
    }

    if(Ip6UtilChangeCxt (u4ContextId, LNX_VRF_NON_DEFAULT_NS,
                         pLnxVrfInfo->au1NameSpace) == (UINT4)NETIPV6_FAILURE)
    {
        return OSIX_FAILURE;
    }
#endif

    SNPRINTF (ac1Command, sizeof (ac1Command),
              "echo %d > %s/%s/%s",
              i4SetVal, LIP6_PROC_CONF, pu1IfName, LIP6_PROC_DISABLEIPV6);

    system (ac1Command);
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    if(Ip6UtilChangeCxt (u4ContextId, LNX_VRF_DEFAULT_NS,
                         (UINT1 *)LNX_VRF_DEFAULT_NS_PID) == (UINT4)NETIPV6_FAILURE)
    {
        return OSIX_FAILURE;
    }
#else
   UNUSED_PARAM(u4ContextId); 
#endif

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    : Lip6KernSetIfForwarding

 Description : Enable/Disable Interface specific Ipv6 Forwarding

 Input       : pu1IfName - Interface name in Linux
               i4SetVal - Status to be set

 OutPut      : None

 Returns     : OSIX_SUCCESS/OSIX_FAILURE
****************************************************************************/
PUBLIC INT4
Lip6KernSetIfForwarding (UINT1 *pu1IfName, INT4 i4SetVal)
{
    CHR1                ac1Command[LIP6_LINE_LEN];
    UINT1               u1LnxSetVal = 0;
    UINT4               u4ContextId = VCM_DEFAULT_CONTEXT;
    UINT4               u4RetValue = OSIX_FAILURE;
    UINT4               u4IfIndex = 0;

#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    tLnxVrfIfInfo *pLnxVrfIfInfo = NULL;
    tLnxVrfInfo        *pLnxVrfInfo = NULL;
#endif


    if (i4SetVal == LIP6_FORW_ENABLE)
    {
        u1LnxSetVal = 1;
    }
    else if (i4SetVal == LIP6_FORW_DISABLE)
    {
        u1LnxSetVal = 0;
    }
    else
    {
        return OSIX_FAILURE;
    }
    if (pu1IfName == NULL)
    {
       return OSIX_FAILURE;
    }
    u4RetValue = CfaGetInterfaceIndexFromName (pu1IfName, &u4IfIndex);
    if ((u4RetValue == OSIX_FAILURE) || (Lip6UtlGetIfEntry (u4IfIndex) == NULL))
    {
       return OSIX_FAILURE;
    }


    MEMSET (ac1Command, 0, sizeof (ac1Command));
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)

    pLnxVrfIfInfo = LnxVrfIfInfoGet(pu1IfName);
    if(pLnxVrfIfInfo != NULL)
    {
        u4ContextId = pLnxVrfIfInfo->u4ContextId;
    }
    if(u4ContextId != VCM_DEFAULT_CONTEXT)
    {
        pLnxVrfInfo = LnxVrfInfoGet (u4ContextId);
        if (pLnxVrfInfo == NULL)
        {
            return OSIX_FAILURE;
        }
    }

    if(Ip6UtilChangeCxt (u4ContextId, LNX_VRF_NON_DEFAULT_NS, 
                         pLnxVrfInfo->au1NameSpace) == (UINT4)NETIPV6_FAILURE)
    {
        return OSIX_FAILURE;
    }
#endif

    SNPRINTF (ac1Command, sizeof (ac1Command),
              "echo %d > %s/%s/%s",
              u1LnxSetVal, LIP6_PROC_CONF, pu1IfName, LIP6_PROC_FWD);

    system (ac1Command);

#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    if(Ip6UtilChangeCxt (u4ContextId, LNX_VRF_DEFAULT_NS,
                         (UINT1 *)LNX_VRF_DEFAULT_NS_PID) == (UINT4)NETIPV6_FAILURE)
    {
        return OSIX_FAILURE;
    }
#else
    UNUSED_PARAM(u4ContextId);
#endif

    return OSIX_SUCCESS;
}

/******************************************************************************
 * FUNCTION    : Lip6KernUpdateMultiIpv6Addr
 * DESCRIPTION : Routine to set an IPv6 address on an interface in Linux IP
 * INPUTS      : i4IpPort   - IP Port number of the interface for which address
 *                            has to be added/deleted
 *               pIp6Addr   - IPv6 address to add/delete
 *               i4PrefixLen - Prefix Len to set
 *               i4Command  - OSIX_TRUE/OSIX_FALSE
 * OUTPUTS     : None
 * RETURNS     : OSIX_SUCCESS
 *               OSIX_FAILURE
 ******************************************************************************/
PUBLIC INT4
Lip6KernUpdateMultiIpv6Addr (INT4 i4IpPort, tIp6Addr * pIp6Addr,
                             INT4 i4PrefixLen, INT4 i4Command)
{
    INT4                i4RetVal = 0;
    INT4                i4SockFd = -1;
    struct in6_ifreq    ifr6;
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED) && defined (LNXIP4_WANTED)
    tCfaIfInfo          CfaIfInfo;
    UINT4               u4ContextId = VCM_DEFAULT_CONTEXT;
    UINT4               u4CfaIfIndex = CFA_INVALID_INDEX;
#endif

    MEMSET (&ifr6, 0, sizeof (ifr6));
    MEMCPY (&ifr6.ifr6_addr, pIp6Addr, 16);
    ifr6.ifr6_ifindex = i4IpPort;

#if defined (VRF_WANTED) && defined (LINUX_310_WANTED) && defined (LNXIP4_WANTED)
    MEMSET (&CfaIfInfo, 0, sizeof (tCfaIfInfo));
    if (NetIpv4GetCfaIfIndexFromPort ((UINT4) i4IpPort,
                                      &u4CfaIfIndex) == NETIPV4_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (CfaGetIfInfo (u4CfaIfIndex, &CfaIfInfo) == CFA_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if(VcmGetContextIdFromCfaIfIndex(u4CfaIfIndex, &u4ContextId) == VCM_FAILURE)
    {
        return OSIX_FAILURE;
    }
    i4SockFd = gIp6GblInfo.ai4SockFd[u4ContextId];
#else
    i4SockFd = gIp6GblInfo.i4SockFd;
#endif

    if (i4PrefixLen != 0)
    {
        ifr6.ifr6_prefixlen = i4PrefixLen;
    }

    if (i4Command == OSIX_TRUE)
    {
        i4RetVal = ioctl (i4SockFd, SIOCDELMULTI, &ifr6);
        i4RetVal = ioctl (i4SockFd, SIOCADDMULTI, &ifr6);
    }
    else
    {
        i4RetVal = ioctl (i4SockFd, SIOCDELMULTI, &ifr6);
    }

    if (i4RetVal < 0)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/******************************************************************************
 * FUNCTION    : Lip6KernUpdateIpv6Addr
 * DESCRIPTION : Routine to set an IPv6 address on an interface in Linux IP
 * INPUTS      : i4IpPort   - IP Port number of the interface for which address
 *                            has to be added/deleted
 *               pIp6Addr   - IPv6 address to add/delete
 *               i4PrefixLen - Prefix Len to set
 *               i4Command  - OSIX_TRUE/OSIX_FALSE
 * OUTPUTS     : None
 * RETURNS     : OSIX_SUCCESS
 *               OSIX_FAILURE
 ******************************************************************************/
PUBLIC INT4
Lip6KernUpdateIpv6Addr (INT4 i4IpPort, tIp6Addr * pIp6Addr,
                        INT4 i4PrefixLen, INT4 i4Command)
{
    INT4                i4RetVal = 0;
    struct in6_ifreq    ifr6;
    INT4                i4SockFd = -1;
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED) && defined (LNXIP4_WANTED)
    tCfaIfInfo          CfaIfInfo;
    UINT4               u4ContextId = VCM_DEFAULT_CONTEXT;
    UINT4               u4CfaIfIndex = CFA_INVALID_INDEX;
#endif

    MEMSET (&ifr6, 0, sizeof (ifr6));
    MEMCPY (&ifr6.ifr6_addr, pIp6Addr, 16);
    ifr6.ifr6_ifindex = i4IpPort;
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED) && defined (LNXIP4_WANTED)
    MEMSET (&CfaIfInfo, 0, sizeof (tCfaIfInfo));
    if (NetIpv4GetCfaIfIndexFromPort ((UINT4) i4IpPort,
                                      &u4CfaIfIndex) == NETIPV4_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (CfaGetIfInfo (u4CfaIfIndex, &CfaIfInfo) == CFA_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if(VcmGetContextIdFromCfaIfIndex(u4CfaIfIndex, &u4ContextId) == VCM_FAILURE)
    {
        return OSIX_FAILURE;
    }
    i4SockFd = gIp6GblInfo.ai4SockFd[u4ContextId];
#else
    i4SockFd = gIp6GblInfo.i4SockFd;
#endif

    if (i4PrefixLen != 0)
    {
	    ifr6.ifr6_prefixlen = i4PrefixLen;
    }
    if (i4Command == OSIX_TRUE)
    {
	    i4RetVal = ioctl (i4SockFd, SIOCDIFADDR, &ifr6);
	    i4RetVal = ioctl (i4SockFd, SIOCSIFADDR, &ifr6);
    }
    else
    {
	    i4RetVal = ioctl (i4SockFd, SIOCDIFADDR, &ifr6);
    }

    if (i4RetVal < 0)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    : Lip6KernSetIfHopLimit

 Description : Configure IPv6 HopLimit for an interface

 Input       : u4IfIndex - Interface Index
               i4SetVal - HopLimit value to be set

 OutPut      : None

 Returns     : OSIX_SUCCESS/OSIX_FAILURE
****************************************************************************/
PUBLIC INT4
Lip6KernSetIfHopLimit (UINT4 u4IfIndex, INT4 i4SetVal)
{
    tLip6If            *pIf6Entry = NULL;
    CHR1                ac1Command[LIP6_LINE_LEN];
    UINT4               u4ContextId = VCM_DEFAULT_CONTEXT;
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    tLnxVrfIfInfo *pLnxVrfIfInfo = NULL;
    tLnxVrfInfo        *pLnxVrfInfo = NULL;
#endif

    MEMSET (ac1Command, 0, sizeof (ac1Command));

    pIf6Entry = Lip6UtlGetIfEntry (u4IfIndex);

#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)

    pLnxVrfIfInfo = LnxVrfIfInfoGet(pIf6Entry->au1IfName);
    if(pLnxVrfIfInfo != NULL)
    {
        u4ContextId = pLnxVrfIfInfo->u4ContextId;
    }
    if(u4ContextId != VCM_DEFAULT_CONTEXT)
    {
        pLnxVrfInfo = LnxVrfInfoGet (u4ContextId);
        if (pLnxVrfInfo == NULL)
        {
            return OSIX_FAILURE;
        }
    }

    if(Ip6UtilChangeCxt (u4ContextId, LNX_VRF_NON_DEFAULT_NS,
                         pLnxVrfInfo->au1NameSpace) == (UINT4)NETIPV6_FAILURE)
    {
        return OSIX_FAILURE;
    }
#endif
    SNPRINTF (ac1Command, sizeof (ac1Command),
              "echo %d >%s/%s/%s",
              i4SetVal, LIP6_PROC_CONF, pIf6Entry->au1IfName,
              LIP6_PROC_HOPLIMIT);

    system ((const char *) ac1Command);
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    if(Ip6UtilChangeCxt (u4ContextId, LNX_VRF_DEFAULT_NS,
                         (UINT1 *)LNX_VRF_DEFAULT_NS_PID) == (UINT4)NETIPV6_FAILURE)
    {
        return OSIX_FAILURE;
    }
#else
    UNUSED_PARAM(u4ContextId);
#endif

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    : Lip6KernSetIfDadAttempts

 Description : Configure DAD attempts value for an interface

 Input       : u4IfIndex - Interface Index
               i4SetVal - DAD attempt value to set

 OutPut      : None

 Returns     : OSIX_SUCCESS/OSIX_FAILURE
****************************************************************************/
PUBLIC INT4
Lip6KernSetIfDadAttempts (UINT4 u4IfIndex, INT4 i4SetVal)
{
    CHR1                ac1Command[LIP6_LINE_LEN];
    tLip6If            *pIf6Entry = NULL;
    UINT4               u4ContextId = VCM_DEFAULT_CONTEXT;
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    tLnxVrfIfInfo *pLnxVrfIfInfo = NULL;
    tLnxVrfInfo        *pLnxVrfInfo = NULL;
#endif

    MEMSET (ac1Command, 0, sizeof (ac1Command));
    pIf6Entry = Lip6UtlGetIfEntry (u4IfIndex);

#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)

    pLnxVrfIfInfo = LnxVrfIfInfoGet(pIf6Entry->au1IfName);
    if(pLnxVrfIfInfo != NULL)
    {
        u4ContextId = pLnxVrfIfInfo->u4ContextId;
    }
    if(u4ContextId != VCM_DEFAULT_CONTEXT)
    {
        pLnxVrfInfo = LnxVrfInfoGet (u4ContextId);
        if (pLnxVrfInfo == NULL)
        {
            return OSIX_FAILURE;
        }
    }

    if(Ip6UtilChangeCxt (u4ContextId, LNX_VRF_NON_DEFAULT_NS, 
                         pLnxVrfInfo->au1NameSpace) == (UINT4)NETIPV6_FAILURE)
    {
        return OSIX_FAILURE;
    }
#endif

    SNPRINTF (ac1Command, sizeof (ac1Command),
              "echo %d >%s/%s/%s",
              i4SetVal, LIP6_PROC_CONF, pIf6Entry->au1IfName,
              LIP6_PROC_DADATTEMPT);

    system ((const char *) ac1Command);
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    if(Ip6UtilChangeCxt (u4ContextId, LNX_VRF_DEFAULT_NS,
                         (UINT1 *)LNX_VRF_DEFAULT_NS_PID) == (UINT4)NETIPV6_FAILURE)
    {
        return OSIX_FAILURE;
    }
#else
    UNUSED_PARAM(u4ContextId);
#endif

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    : Lip6KernGetIfStat
 * Description : Get a If Stat from Linux for an interface
 * Input       : u4IfIndex - Interface Index
 *               u1Stat - Statistics to get
 * OutPut      : pu4Value - Statistics value obtained
 * Returns     : OSIX_SUCCESS/OSIX_FAILURE
****************************************************************************/
PUBLIC INT4
Lip6KernGetIfStat (UINT4 u4IfIndex, UINT1 u1Stat, UINT4 *pu4Value, UINT4 u4ContextId)
{
    UINT1               au1FileName[LIP6_LINE_LEN];
    INT1                ai1LineBuf[LIP6_LINE_LEN];
    CHR1               *pCmpBuf = NULL;
    tLip6If            *pIf6Entry = NULL;
    UINT1               au1TempIfName[IP6_MAX_IF_NAME_LEN];
    INT4                i4Fd = 0;
    INT4                i4RetVal = 0;
    INT4                i4EntryFound = OSIX_FALSE;
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    tLnxVrfInfo        *pLnxVrfInfo = NULL;
    UINT1               au1VrfCmd[LIP6_LINE_LEN];
#endif

    if (u1Stat >= IP6_MAX_STATS)
    {
        LIP6_TRC_ARG1 (LIP6_MOD_TRC, ALL_FAILURE_TRC,
                       "Invalid Statistics - %d\r\n", u1Stat);
        return OSIX_FAILURE;
    }
    pIf6Entry = Lip6UtlGetIfEntry (u4IfIndex);
    if (pIf6Entry == NULL)
    {
        LIP6_TRC_ARG1 (LIP6_MOD_TRC, ALL_FAILURE_TRC,
                       "Invalid Interface - %d\r\n", u4IfIndex);
        return OSIX_FAILURE;
    }

    MEMSET (au1FileName, 0, sizeof (au1FileName));
    MEMSET (au1TempIfName, 0, IP6_MAX_IF_NAME_LEN);
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    MEMSET (au1VrfCmd, 0, sizeof (au1VrfCmd));
#endif

    if (STRCMP (pIf6Entry->au1IfName, au1TempIfName) == 0)
    {
        LIP6_TRC_ARG (LIP6_MOD_TRC, ALL_FAILURE_TRC,
                      "Invalid Interface name \r\n");
        *pu4Value = 0;
        return OSIX_SUCCESS;
    }

    SNPRINTF ((CHR1 *) au1FileName, LIP6_LINE_LEN, "%s/%s",
              LIP6_PROC_DEVSNMP, pIf6Entry->au1IfName);

#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)

    if(u4ContextId != VCM_DEFAULT_CONTEXT)
    {
        pLnxVrfInfo = LnxVrfInfoGet (u4ContextId);
        if (pLnxVrfInfo == NULL)
        {
            return OSIX_FAILURE;
        }
    }

    if(Ip6UtilChangeCxt (u4ContextId, LNX_VRF_NON_DEFAULT_NS,
                         pLnxVrfInfo->au1NameSpace) == (UINT4)NETIPV6_FAILURE)
    {
        return FALSE;
    }

    SPRINTF((CHR1 *)au1VrfCmd,"cat %s > Local_DevSnmp6",au1FileName);
    system((CHR1 *)au1VrfCmd);

    i4Fd = FileOpen ((UINT1 *)"Local_DevSnmp6", OSIX_FILE_RO);

#else 
    i4Fd = FileOpen (au1FileName, OSIX_FILE_RO);
#endif

    if (i4Fd < 0)
    {
        LIP6_TRC_ARG (LIP6_MOD_TRC, ALL_FAILURE_TRC,
                      "FileOpen failed for statistics\r\n");
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
        if(Ip6UtilChangeCxt (u4ContextId, LNX_VRF_DEFAULT_NS,
                             (UINT1 *)LNX_VRF_DEFAULT_NS_PID) == (UINT4)NETIPV4_FAILURE)
        {
            unlink("Local_DevSnmp6");
            return OSIX_FAILURE;
        }
#endif

        return OSIX_FAILURE;
    }

    MEMSET (ai1LineBuf, 0, sizeof (ai1LineBuf));
    i4RetVal = FsUtlReadLine (i4Fd, ai1LineBuf);

    while (i4RetVal != OSIX_FAILURE)
    {
        pCmpBuf = STRSTR (ai1LineBuf, gau1Ip6Stats[u1Stat]);

        if (pCmpBuf != NULL)
        {
            i4EntryFound = OSIX_TRUE;
            break;
        }

        MEMSET (ai1LineBuf, 0, sizeof (ai1LineBuf));
        i4RetVal = FsUtlReadLine (i4Fd, ai1LineBuf);
    }

    FileClose (i4Fd);
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    unlink("Local_DevSnmp6");
    if(Ip6UtilChangeCxt (u4ContextId, LNX_VRF_DEFAULT_NS,
                         (UINT1 *)LNX_VRF_DEFAULT_NS_PID) == (UINT4)NETIPV4_FAILURE)
    {
        return OSIX_FAILURE;
    }
#else
    UNUSED_PARAM(u4ContextId);
#endif
    if (i4EntryFound == OSIX_FALSE)
    {
        LIP6_TRC_ARG (LIP6_MOD_TRC, ALL_FAILURE_TRC,
                      "Statistics not found\r\n");
        *pu4Value = 0;
        return OSIX_SUCCESS;
    }

    /* Read the statistics count from the buffer */
    pCmpBuf = pCmpBuf + STRLEN (gau1Ip6Stats[u1Stat]);
    while (*pCmpBuf == ' ')
    {
        pCmpBuf++;
    }

    *pu4Value = (UINT4) ATOI (pCmpBuf);
    return OSIX_SUCCESS;
}

PUBLIC INT4
Lip6SetIpv6ForwardingGlobal (UINT4 u4ForwStatus)
{
    UINT4               u4Index = 0;
    tLip6If            *pIf6Entry = NULL;
    INT4                i4RetVal = OSIX_SUCCESS;

    if (u4ForwStatus == LIP6_FORW_DISABLE)
    {
        for (; u4Index < LIP6_MAX_INTERFACES; u4Index++)
        {
            if (((pIf6Entry = Lip6UtlGetIfEntry (u4Index)) == NULL)
                || (pIf6Entry->u1Ipv6IfFwdOperStatus == LIP6_FORW_DISABLE))
            {
                continue;
            }
            if (Lip6KernSetIfForwarding (pIf6Entry->au1IfName, u4ForwStatus) !=
                OSIX_SUCCESS)
            {
                i4RetVal = OSIX_FAILURE;
            }
        }
    }
    else
    {
        for (; u4Index < LIP6_MAX_INTERFACES; u4Index++)
        {
            if (((pIf6Entry = Lip6UtlGetIfEntry (u4Index)) == NULL))
            {
                continue;
            }
            if (Lip6KernSetIfForwarding
                (pIf6Entry->au1IfName,
                 pIf6Entry->u1Ipv6IfFwdOperStatus) != OSIX_SUCCESS)
            {
                i4RetVal = OSIX_FAILURE;
            }
        }
    }
    return i4RetVal;
}

#ifdef LINUX_310_WANTED
/*****************************************************************************/
/*                                                                           */
/*    Function Name       : Lip6KernIpGetInterface                           */
/*                                                                           */
/*    Description         : This function gets the Interface index for an    */
/*                                                                           */
/*    Input(s)            : pu1DevName   -  Device name                      */
/*                                                                           */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT4
Lip6KernIpGetInterface (UINT1 *pu1DevName, INT4 *pi4IfIndex)
{
    INT4                i4Err = -1;
    INT4		        i4SockFd = -1;
    struct ifreq        ifr;

    MEMSET (&ifr, 0, sizeof (ifr));
    STRCPY (ifr.ifr_ifrn.ifrn_name, pu1DevName);

    /* Workaround : this will work for only default context, need to be fixed*/
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED) && defined (LNXIP4_WANTED)
    i4SockFd = gIp6GblInfo.ai4SockFd[VCM_DEFAULT_CONTEXT];
#else
    i4SockFd = gIp6GblInfo.i4SockFd;
#endif

    i4Err = ioctl (i4SockFd, SIOCGIFINDEX, &ifr);

    if (i4Err < 0)
    {
        perror ("IF Index get");
        return OSIX_FAILURE;
    }

    *pi4IfIndex = ifr.ifr_ifindex;
    return OSIX_SUCCESS;
}
#endif
#endif /* _LIP6KERN_C_ */
/* END OF FILE */
