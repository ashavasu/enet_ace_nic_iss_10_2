/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsrtm6db.h,v 1.6 2015/10/19 12:22:10 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSRTM6DB_H
#define _FSRTM6DB_H

UINT1 FsRrd6ControlTableINDEX [] = {SNMP_FIXED_LENGTH_OCTET_STRING ,16 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsRrd6RoutingProtoTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};

UINT4 fsrtm6 [] ={1,3,6,1,4,1,2076,92};
tSNMP_OID_TYPE fsrtm6OID = {8, fsrtm6};


UINT4 FsRrd6RouterId [ ] ={1,3,6,1,4,1,2076,92,1,1};
UINT4 FsRrd6FilterByOspfTag [ ] ={1,3,6,1,4,1,2076,92,1,2};
UINT4 FsRrd6FilterOspfTag [ ] ={1,3,6,1,4,1,2076,92,1,3};
UINT4 FsRrd6FilterOspfTagMask [ ] ={1,3,6,1,4,1,2076,92,1,4};
UINT4 FsRrd6RouterASNumber [ ] ={1,3,6,1,4,1,2076,92,1,5};
UINT4 FsRrd6AdminStatus [ ] ={1,3,6,1,4,1,2076,92,1,6};
UINT4 FsRrd6Trace [ ] ={1,3,6,1,4,1,2076,92,1,7};
UINT4 FsRrd6ThrotLimit [ ] ={1,3,6,1,4,1,2076,92,1,8};
UINT4 FsRrd6MaximumBgpRoutes [ ] ={1,3,6,1,4,1,2076,92,1,9};
UINT4 FsRrd6MaximumOspfRoutes [ ] ={1,3,6,1,4,1,2076,92,1,10};
UINT4 FsRrd6MaximumRipRoutes [ ] ={1,3,6,1,4,1,2076,92,1,11};
UINT4 FsRrd6MaximumStaticRoutes [ ] ={1,3,6,1,4,1,2076,92,1,12};
UINT4 FsRrd6MaximumISISRoutes [ ] ={1,3,6,1,4,1,2076,92,1,13};
UINT4 FsRtm6StaticRouteDistance [ ] ={1,3,6,1,4,1,2076,92,1,14};
UINT4 FsRrd6ControlDestIpAddress [ ] ={1,3,6,1,4,1,2076,92,2,1,1};
UINT4 FsRrd6ControlNetMaskLen [ ] ={1,3,6,1,4,1,2076,92,2,1,2};
UINT4 FsRrd6ControlSourceProto [ ] ={1,3,6,1,4,1,2076,92,2,1,3};
UINT4 FsRrd6ControlDestProto [ ] ={1,3,6,1,4,1,2076,92,2,1,4};
UINT4 FsRrd6ControlRouteExportFlag [ ] ={1,3,6,1,4,1,2076,92,2,1,5};
UINT4 FsRrd6ControlRowStatus [ ] ={1,3,6,1,4,1,2076,92,2,1,6};
UINT4 FsRrd6RoutingProtoId [ ] ={1,3,6,1,4,1,2076,92,3,1,1};
UINT4 FsRrd6RoutingRegnId [ ] ={1,3,6,1,4,1,2076,92,3,1,2};
UINT4 FsRrd6RoutingProtoTaskIdent [ ] ={1,3,6,1,4,1,2076,92,3,1,3};
UINT4 FsRrd6RoutingProtoQueueIdent [ ] ={1,3,6,1,4,1,2076,92,3,1,4};
UINT4 FsRrd6AllowOspfAreaRoutes [ ] ={1,3,6,1,4,1,2076,92,3,1,5};
UINT4 FsRrd6AllowOspfExtRoutes [ ] ={1,3,6,1,4,1,2076,92,3,1,6};
UINT4 FsRtm6RedEntryTime [ ] ={1,3,6,1,4,1,2076,92,4,1};
UINT4 FsRtm6RedExitTime [ ] ={1,3,6,1,4,1,2076,92,4,2};




tMbDbEntry fsrtm6MibEntry[]= {

{{10,FsRrd6RouterId}, NULL, FsRrd6RouterIdGet, FsRrd6RouterIdSet, FsRrd6RouterIdTest, FsRrd6RouterIdDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsRrd6FilterByOspfTag}, NULL, FsRrd6FilterByOspfTagGet, FsRrd6FilterByOspfTagSet, FsRrd6FilterByOspfTagTest, FsRrd6FilterByOspfTagDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{10,FsRrd6FilterOspfTag}, NULL, FsRrd6FilterOspfTagGet, FsRrd6FilterOspfTagSet, FsRrd6FilterOspfTagTest, FsRrd6FilterOspfTagDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsRrd6FilterOspfTagMask}, NULL, FsRrd6FilterOspfTagMaskGet, FsRrd6FilterOspfTagMaskSet, FsRrd6FilterOspfTagMaskTest, FsRrd6FilterOspfTagMaskDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "-1"},

{{10,FsRrd6RouterASNumber}, NULL, FsRrd6RouterASNumberGet, FsRrd6RouterASNumberSet, FsRrd6RouterASNumberTest, FsRrd6RouterASNumberDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{10,FsRrd6AdminStatus}, NULL, FsRrd6AdminStatusGet, FsRrd6AdminStatusSet, FsRrd6AdminStatusTest, FsRrd6AdminStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{10,FsRrd6Trace}, NULL, FsRrd6TraceGet, FsRrd6TraceSet, FsRrd6TraceTest, FsRrd6TraceDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsRrd6ThrotLimit}, NULL, FsRrd6ThrotLimitGet, FsRrd6ThrotLimitSet, FsRrd6ThrotLimitTest, FsRrd6ThrotLimitDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "1000"},

{{10,FsRrd6MaximumBgpRoutes}, NULL, FsRrd6MaximumBgpRoutesGet, FsRrd6MaximumBgpRoutesSet, FsRrd6MaximumBgpRoutesTest, FsRrd6MaximumBgpRoutesDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsRrd6MaximumOspfRoutes}, NULL, FsRrd6MaximumOspfRoutesGet, FsRrd6MaximumOspfRoutesSet, FsRrd6MaximumOspfRoutesTest, FsRrd6MaximumOspfRoutesDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsRrd6MaximumRipRoutes}, NULL, FsRrd6MaximumRipRoutesGet, FsRrd6MaximumRipRoutesSet, FsRrd6MaximumRipRoutesTest, FsRrd6MaximumRipRoutesDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsRrd6MaximumStaticRoutes}, NULL, FsRrd6MaximumStaticRoutesGet, FsRrd6MaximumStaticRoutesSet, FsRrd6MaximumStaticRoutesTest, FsRrd6MaximumStaticRoutesDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsRrd6MaximumISISRoutes}, NULL, FsRrd6MaximumISISRoutesGet, FsRrd6MaximumISISRoutesSet, FsRrd6MaximumISISRoutesTest, FsRrd6MaximumISISRoutesDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsRtm6StaticRouteDistance}, NULL, FsRtm6StaticRouteDistanceGet, FsRtm6StaticRouteDistanceSet, FsRtm6StaticRouteDistanceTest, FsRtm6StaticRouteDistanceDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,FsRrd6ControlDestIpAddress}, GetNextIndexFsRrd6ControlTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsRrd6ControlTableINDEX, 2, 0, 0, NULL},

{{11,FsRrd6ControlNetMaskLen}, GetNextIndexFsRrd6ControlTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsRrd6ControlTableINDEX, 2, 0, 0, NULL},

{{11,FsRrd6ControlSourceProto}, GetNextIndexFsRrd6ControlTable, FsRrd6ControlSourceProtoGet, FsRrd6ControlSourceProtoSet, FsRrd6ControlSourceProtoTest, FsRrd6ControlTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsRrd6ControlTableINDEX, 2, 0, 0, "0"},

{{11,FsRrd6ControlDestProto}, GetNextIndexFsRrd6ControlTable, FsRrd6ControlDestProtoGet, FsRrd6ControlDestProtoSet, FsRrd6ControlDestProtoTest, FsRrd6ControlTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsRrd6ControlTableINDEX, 2, 0, 0, "0"},

{{11,FsRrd6ControlRouteExportFlag}, GetNextIndexFsRrd6ControlTable, FsRrd6ControlRouteExportFlagGet, FsRrd6ControlRouteExportFlagSet, FsRrd6ControlRouteExportFlagTest, FsRrd6ControlTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsRrd6ControlTableINDEX, 2, 0, 0, NULL},

{{11,FsRrd6ControlRowStatus}, GetNextIndexFsRrd6ControlTable, FsRrd6ControlRowStatusGet, FsRrd6ControlRowStatusSet, FsRrd6ControlRowStatusTest, FsRrd6ControlTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsRrd6ControlTableINDEX, 2, 0, 1, NULL},

{{11,FsRrd6RoutingProtoId}, GetNextIndexFsRrd6RoutingProtoTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsRrd6RoutingProtoTableINDEX, 1, 0, 0, NULL},

{{11,FsRrd6RoutingRegnId}, GetNextIndexFsRrd6RoutingProtoTable, FsRrd6RoutingRegnIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsRrd6RoutingProtoTableINDEX, 1, 0, 0, NULL},

{{11,FsRrd6RoutingProtoTaskIdent}, GetNextIndexFsRrd6RoutingProtoTable, FsRrd6RoutingProtoTaskIdentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsRrd6RoutingProtoTableINDEX, 1, 0, 0, NULL},

{{11,FsRrd6RoutingProtoQueueIdent}, GetNextIndexFsRrd6RoutingProtoTable, FsRrd6RoutingProtoQueueIdentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsRrd6RoutingProtoTableINDEX, 1, 0, 0, NULL},

{{11,FsRrd6AllowOspfAreaRoutes}, GetNextIndexFsRrd6RoutingProtoTable, FsRrd6AllowOspfAreaRoutesGet, FsRrd6AllowOspfAreaRoutesSet, FsRrd6AllowOspfAreaRoutesTest, FsRrd6RoutingProtoTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsRrd6RoutingProtoTableINDEX, 1, 0, 0, "2"},

{{11,FsRrd6AllowOspfExtRoutes}, GetNextIndexFsRrd6RoutingProtoTable, FsRrd6AllowOspfExtRoutesGet, FsRrd6AllowOspfExtRoutesSet, FsRrd6AllowOspfExtRoutesTest, FsRrd6RoutingProtoTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsRrd6RoutingProtoTableINDEX, 1, 0, 0, "2"},

{{10,FsRtm6RedEntryTime}, NULL, FsRtm6RedEntryTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsRtm6RedExitTime}, NULL, FsRtm6RedExitTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},
};
tMibData fsrtm6Entry = { sizeof(fsrtm6MibEntry)/sizeof(fsrtm6MibEntry[0]), fsrtm6MibEntry };
#endif /* _FSRTM6DB_H */

