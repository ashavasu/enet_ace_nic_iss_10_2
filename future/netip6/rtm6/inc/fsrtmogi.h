
# ifndef fsrtmOGP_H
# define fsrtmOGP_H

 /* The Definitions of the OGP Index Constants.  */

# define SNMP_OGP_INDEX_FSRRD6SCALAR                                 (0)
# define SNMP_OGP_INDEX_FSRRD6CONTROLTABLE                           (1)
# define SNMP_OGP_INDEX_FSRRD6ROUTINGPROTOTABLE                      (2)

#endif /*  fsrtm6OGP_H  */
