/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rtm6extn.h,v 1.5 2012/12/31 10:28:46 siva Exp $
 *
 * Description:This file contains extern definitions used in
 *             the RTM6 module.                              
 *                               
 *******************************************************************/
#ifndef _RTM6_EXTN_H
#define _RTM6_EXTN_H

extern tRtm6SystemSize       gRtm6SystemSize;
extern tRtm6GlobalInfo       gRtm6GlobalInfo;

extern tRtm6RedGlobalInfo    gRtm6RedGlobalInfo;

#endif /* _RTM6_EXTN_H */

