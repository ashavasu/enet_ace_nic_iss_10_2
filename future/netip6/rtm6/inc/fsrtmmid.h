
/*  Prototype for Get Test & Set for fsrrd6Scalar.  */
tSNMP_VAR_BIND*
fsrrd6ScalarGet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, UINT1));
INT4
fsrrd6ScalarSet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));
INT4
fsrrd6ScalarTest ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));


/*  Prototype for Get Test & Set for fsRrd6ControlTable.  */
tSNMP_VAR_BIND*
fsRrd6ControlEntryGet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, UINT1));
INT4
fsRrd6ControlEntrySet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));
INT4
fsRrd6ControlEntryTest ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));


/*  Prototype for Get Test & Set for fsRrd6RoutingProtoTable.  */
tSNMP_VAR_BIND*
fsRrd6RoutingProtoEntryGet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, UINT1));
INT4
fsRrd6RoutingProtoEntrySet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));
INT4
fsRrd6RoutingProtoEntryTest ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));

