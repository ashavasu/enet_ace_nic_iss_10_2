/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rtm6port.h,v 1.6 2017/12/26 13:34:22 siva Exp $
 *
 * Description:This file contains porting related           
 *             defintions used in RTM6 module.               
 *
 *******************************************************************/
#ifndef _RTM6_PORT_H
#define _RTM6_PORT_H
#endif /* _RTM6_PORT_H */
#define   RTM6_MAIN_TASK_NAME    "RT6"

/* Includes LOCAL, STATIC, RIP, OSPF and BGP routes */

#define   RTM6_MESSAGE_ARRIVAL_Q_DEPTH      20
#define   RTM6_SNMP_IF_Q_DEPTH              10
#define   RTM6_RT_MSG_Q_DEPTH               50000

#define   RTM6_MESSAGE_ARRIVAL_Q_NAME      "RT6Q"
#define   RTM6_MESSAGE_ARRIVAL_Q_MODE      OSIX_GLOBAL
#define   RTM6_MESSAGE_ARRIVAL_Q_NODE_ID   SELF

#define   RTM6_SNMP_IF_Q_NAME              "R6SQ"
#define   RTM6_SNMP_IF_Q_MODE              OSIX_GLOBAL
#define   RTM6_SNMP_IF_Q_NODE_ID           SELF

#define   RTM6_RT_MSG_Q_NAME              "R6RTQ"
#define   RTM6_RT_MSG_Q_MODE              OSIX_GLOBAL
#define   RTM6_RT_MSG_Q_NODE_ID           SELF

#define   RTM6_RM_PKT_ARRIVAL_Q_DEPTH     10
#define   RTM6_RM_PKT_ARRIVAL_Q_NAME      "RTM6RMQ"

#define   RTM6_EVENT_WAIT_FLAGS            (OSIX_WAIT | OSIX_EV_ANY)

#define   RTM6_EVENT_WAIT_TIMEOUT                0

#define RTM6_INIT_COMPLETE(u4Status)    lrInitComplete(u4Status)

#define   RTM6_MAX_PROTOMASK_ROUTES             1000

