/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmrt6db.h,v 1.5 2015/10/19 12:22:09 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSMRT6DB_H
#define _FSMRT6DB_H

UINT1 FsMIRtm6TableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMIRrd6ControlTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_FIXED_LENGTH_OCTET_STRING ,16 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMIRrd6RoutingProtoTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER};

UINT4 fsmirt6 [] ={1,3,6,1,4,1,29601,2,32};
tSNMP_OID_TYPE fsmirt6OID = {9, fsmirt6};


UINT4 FsMIRtm6GlobalTrace [ ] ={1,3,6,1,4,1,29601,2,32,1,1};
UINT4 FsMIRtm6ThrotLimit [ ] ={1,3,6,1,4,1,29601,2,32,1,2};
UINT4 FsMIRtm6MaximumBgpRoutes [ ] ={1,3,6,1,4,1,29601,2,32,1,3};
UINT4 FsMIRtm6MaximumOspfRoutes [ ] ={1,3,6,1,4,1,29601,2,32,1,4};
UINT4 FsMIRtm6MaximumRipRoutes [ ] ={1,3,6,1,4,1,29601,2,32,1,5};
UINT4 FsMIRtm6MaximumStaticRoutes [ ] ={1,3,6,1,4,1,29601,2,32,1,6};
UINT4 FsMIRtm6MaximumISISRoutes [ ] ={1,3,6,1,4,1,29601,2,32,1,7};
UINT4 FsMIRtm6ContextId [ ] ={1,3,6,1,4,1,29601,2,32,2,1,1};
UINT4 FsMIRrd6RouterId [ ] ={1,3,6,1,4,1,29601,2,32,2,1,2};
UINT4 FsMIRrd6FilterByOspfTag [ ] ={1,3,6,1,4,1,29601,2,32,2,1,3};
UINT4 FsMIRrd6FilterOspfTag [ ] ={1,3,6,1,4,1,29601,2,32,2,1,4};
UINT4 FsMIRrd6FilterOspfTagMask [ ] ={1,3,6,1,4,1,29601,2,32,2,1,5};
UINT4 FsMIRrd6RouterASNumber [ ] ={1,3,6,1,4,1,29601,2,32,2,1,6};
UINT4 FsMIRrd6AdminStatus [ ] ={1,3,6,1,4,1,29601,2,32,2,1,7};
UINT4 FsMIRrd6Trace [ ] ={1,3,6,1,4,1,29601,2,32,2,1,8};
UINT4 FsMIRtm6StaticRouteDistance [ ] ={1,3,6,1,4,1,29601,2,32,2,1,9};
UINT4 FsMIRrd6ControlDestIpAddress [ ] ={1,3,6,1,4,1,29601,2,32,3,1,1};
UINT4 FsMIRrd6ControlNetMaskLen [ ] ={1,3,6,1,4,1,29601,2,32,3,1,2};
UINT4 FsMIRrd6ControlSourceProto [ ] ={1,3,6,1,4,1,29601,2,32,3,1,3};
UINT4 FsMIRrd6ControlDestProto [ ] ={1,3,6,1,4,1,29601,2,32,3,1,4};
UINT4 FsMIRrd6ControlRouteExportFlag [ ] ={1,3,6,1,4,1,29601,2,32,3,1,5};
UINT4 FsMIRrd6ControlRowStatus [ ] ={1,3,6,1,4,1,29601,2,32,3,1,6};
UINT4 FsMIRrd6RoutingProtoId [ ] ={1,3,6,1,4,1,29601,2,32,4,1,1};
UINT4 FsMIRrd6RoutingRegnId [ ] ={1,3,6,1,4,1,29601,2,32,4,1,2};
UINT4 FsMIRrd6RoutingProtoTaskIdent [ ] ={1,3,6,1,4,1,29601,2,32,4,1,3};
UINT4 FsMIRrd6RoutingProtoQueueIdent [ ] ={1,3,6,1,4,1,29601,2,32,4,1,4};
UINT4 FsMIRrd6AllowOspfAreaRoutes [ ] ={1,3,6,1,4,1,29601,2,32,4,1,5};
UINT4 FsMIRrd6AllowOspfExtRoutes [ ] ={1,3,6,1,4,1,29601,2,32,4,1,6};
UINT4 FsMIRtm6RedEntryTime [ ] ={1,3,6,1,4,1,29601,2,32,5,1};
UINT4 FsMIRtm6RedExitTime [ ] ={1,3,6,1,4,1,29601,2,32,5,2};


tMbDbEntry fsmirt6MibEntry[]= {

{{11,FsMIRtm6GlobalTrace}, NULL, FsMIRtm6GlobalTraceGet, FsMIRtm6GlobalTraceSet, FsMIRtm6GlobalTraceTest, FsMIRtm6GlobalTraceDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,FsMIRtm6ThrotLimit}, NULL, FsMIRtm6ThrotLimitGet, FsMIRtm6ThrotLimitSet, FsMIRtm6ThrotLimitTest, FsMIRtm6ThrotLimitDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "1000"},

{{11,FsMIRtm6MaximumBgpRoutes}, NULL, FsMIRtm6MaximumBgpRoutesGet, FsMIRtm6MaximumBgpRoutesSet, FsMIRtm6MaximumBgpRoutesTest, FsMIRtm6MaximumBgpRoutesDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,FsMIRtm6MaximumOspfRoutes}, NULL, FsMIRtm6MaximumOspfRoutesGet, FsMIRtm6MaximumOspfRoutesSet, FsMIRtm6MaximumOspfRoutesTest, FsMIRtm6MaximumOspfRoutesDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,FsMIRtm6MaximumRipRoutes}, NULL, FsMIRtm6MaximumRipRoutesGet, FsMIRtm6MaximumRipRoutesSet, FsMIRtm6MaximumRipRoutesTest, FsMIRtm6MaximumRipRoutesDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,FsMIRtm6MaximumStaticRoutes}, NULL, FsMIRtm6MaximumStaticRoutesGet, FsMIRtm6MaximumStaticRoutesSet, FsMIRtm6MaximumStaticRoutesTest, FsMIRtm6MaximumStaticRoutesDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,FsMIRtm6MaximumISISRoutes}, NULL, FsMIRtm6MaximumISISRoutesGet, FsMIRtm6MaximumISISRoutesSet, FsMIRtm6MaximumISISRoutesTest, FsMIRtm6MaximumISISRoutesDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{12,FsMIRtm6ContextId}, GetNextIndexFsMIRtm6Table, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIRtm6TableINDEX, 1, 0, 0, NULL},

{{12,FsMIRrd6RouterId}, GetNextIndexFsMIRtm6Table, FsMIRrd6RouterIdGet, FsMIRrd6RouterIdSet, FsMIRrd6RouterIdTest, FsMIRtm6TableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, FsMIRtm6TableINDEX, 1, 0, 0, NULL},

{{12,FsMIRrd6FilterByOspfTag}, GetNextIndexFsMIRtm6Table, FsMIRrd6FilterByOspfTagGet, FsMIRrd6FilterByOspfTagSet, FsMIRrd6FilterByOspfTagTest, FsMIRtm6TableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIRtm6TableINDEX, 1, 0, 0, "2"},

{{12,FsMIRrd6FilterOspfTag}, GetNextIndexFsMIRtm6Table, FsMIRrd6FilterOspfTagGet, FsMIRrd6FilterOspfTagSet, FsMIRrd6FilterOspfTagTest, FsMIRtm6TableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIRtm6TableINDEX, 1, 0, 0, NULL},

{{12,FsMIRrd6FilterOspfTagMask}, GetNextIndexFsMIRtm6Table, FsMIRrd6FilterOspfTagMaskGet, FsMIRrd6FilterOspfTagMaskSet, FsMIRrd6FilterOspfTagMaskTest, FsMIRtm6TableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIRtm6TableINDEX, 1, 0, 0, "-1"},

{{12,FsMIRrd6RouterASNumber}, GetNextIndexFsMIRtm6Table, FsMIRrd6RouterASNumberGet, FsMIRrd6RouterASNumberSet, FsMIRrd6RouterASNumberTest, FsMIRtm6TableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIRtm6TableINDEX, 1, 0, 0, "0"},

{{12,FsMIRrd6AdminStatus}, GetNextIndexFsMIRtm6Table, FsMIRrd6AdminStatusGet, FsMIRrd6AdminStatusSet, FsMIRrd6AdminStatusTest, FsMIRtm6TableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIRtm6TableINDEX, 1, 0, 0, "2"},

{{12,FsMIRrd6Trace}, GetNextIndexFsMIRtm6Table, FsMIRrd6TraceGet, FsMIRrd6TraceSet, FsMIRrd6TraceTest, FsMIRtm6TableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIRtm6TableINDEX, 1, 0, 0, NULL},

{{12,FsMIRtm6StaticRouteDistance}, GetNextIndexFsMIRtm6Table, FsMIRtm6StaticRouteDistanceGet, FsMIRtm6StaticRouteDistanceSet, FsMIRtm6StaticRouteDistanceTest, FsMIRtm6TableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIRtm6TableINDEX, 1, 0, 0, NULL},

{{12,FsMIRrd6ControlDestIpAddress}, GetNextIndexFsMIRrd6ControlTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsMIRrd6ControlTableINDEX, 3, 0, 0, NULL},

{{12,FsMIRrd6ControlNetMaskLen}, GetNextIndexFsMIRrd6ControlTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIRrd6ControlTableINDEX, 3, 0, 0, NULL},

{{12,FsMIRrd6ControlSourceProto}, GetNextIndexFsMIRrd6ControlTable, FsMIRrd6ControlSourceProtoGet, FsMIRrd6ControlSourceProtoSet, FsMIRrd6ControlSourceProtoTest, FsMIRrd6ControlTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIRrd6ControlTableINDEX, 3, 0, 0, "0"},

{{12,FsMIRrd6ControlDestProto}, GetNextIndexFsMIRrd6ControlTable, FsMIRrd6ControlDestProtoGet, FsMIRrd6ControlDestProtoSet, FsMIRrd6ControlDestProtoTest, FsMIRrd6ControlTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIRrd6ControlTableINDEX, 3, 0, 0, "0"},

{{12,FsMIRrd6ControlRouteExportFlag}, GetNextIndexFsMIRrd6ControlTable, FsMIRrd6ControlRouteExportFlagGet, FsMIRrd6ControlRouteExportFlagSet, FsMIRrd6ControlRouteExportFlagTest, FsMIRrd6ControlTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIRrd6ControlTableINDEX, 3, 0, 0, NULL},

{{12,FsMIRrd6ControlRowStatus}, GetNextIndexFsMIRrd6ControlTable, FsMIRrd6ControlRowStatusGet, FsMIRrd6ControlRowStatusSet, FsMIRrd6ControlRowStatusTest, FsMIRrd6ControlTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIRrd6ControlTableINDEX, 3, 0, 1, NULL},

{{12,FsMIRrd6RoutingProtoId}, GetNextIndexFsMIRrd6RoutingProtoTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIRrd6RoutingProtoTableINDEX, 2, 0, 0, NULL},

{{12,FsMIRrd6RoutingRegnId}, GetNextIndexFsMIRrd6RoutingProtoTable, FsMIRrd6RoutingRegnIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIRrd6RoutingProtoTableINDEX, 2, 0, 0, NULL},

{{12,FsMIRrd6RoutingProtoTaskIdent}, GetNextIndexFsMIRrd6RoutingProtoTable, FsMIRrd6RoutingProtoTaskIdentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMIRrd6RoutingProtoTableINDEX, 2, 0, 0, NULL},

{{12,FsMIRrd6RoutingProtoQueueIdent}, GetNextIndexFsMIRrd6RoutingProtoTable, FsMIRrd6RoutingProtoQueueIdentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMIRrd6RoutingProtoTableINDEX, 2, 0, 0, NULL},

{{12,FsMIRrd6AllowOspfAreaRoutes}, GetNextIndexFsMIRrd6RoutingProtoTable, FsMIRrd6AllowOspfAreaRoutesGet, FsMIRrd6AllowOspfAreaRoutesSet, FsMIRrd6AllowOspfAreaRoutesTest, FsMIRrd6RoutingProtoTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIRrd6RoutingProtoTableINDEX, 2, 0, 0, "2"},

{{12,FsMIRrd6AllowOspfExtRoutes}, GetNextIndexFsMIRrd6RoutingProtoTable, FsMIRrd6AllowOspfExtRoutesGet, FsMIRrd6AllowOspfExtRoutesSet, FsMIRrd6AllowOspfExtRoutesTest, FsMIRrd6RoutingProtoTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIRrd6RoutingProtoTableINDEX, 2, 0, 0, "2"},

{{11,FsMIRtm6RedEntryTime}, NULL, FsMIRtm6RedEntryTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsMIRtm6RedExitTime}, NULL, FsMIRtm6RedExitTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},
};
tMibData fsmirt6Entry = { sizeof(fsmirt6MibEntry)/sizeof(fsmirt6MibEntry[0]) , fsmirt6MibEntry };
#endif /* _FSMRT6DB_H */

