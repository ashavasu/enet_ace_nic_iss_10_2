/********************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: rtm6frt.h,v 1.1 2015/04/24 12:01:36 siva Exp $
 *
 * Description: This file contains all macro definitions and 
 *              function prototypes for RTM Failed route module.
 *              
 *******************************************************************/
#ifndef __RTM6_FRT_H
#define __RTM6_FRT_H

INT4 Rtm6RBTreeFrtEntryCmp (tRBElem * pRBElem, tRBElem * pRBElemIn);
PUBLIC INT4 Rtm6FrtInitGlobalInfo (VOID);
INT4 Rtm6FrtAddInfo (tIp6RtEntry * pRt6Info, UINT4 u4CxtId, UINT1 u1Type);
tRtm6FrtInfo *Rtm6FrtGetInfo (tIp6RtEntry  * pRtInfo, UINT4 u4CxtId);
INT4 Rtm6FrtDeleteEntry (tRtm6FrtInfo *pRtm6FrtNode);
VOID Rtm6GetFrtCtxId(tRtm6FrtInfo *pRtm6FrtNode,UINT4 *pCtxId);
tRtm6FrtInfo *Rtm6GetNextFrtEntry(tRtm6FrtInfo *pRtm6FrtNode);
tRtm6FrtInfo *Rtm6GetFirstFrtEntry(VOID);
INT4 Rtm6FrtDeInitGlobalInfo (VOID);
VOID Rtm6GetFrtTableCount(UINT4 *pFrtCount);
#define MAX_RTM6_RETRY_ROUTE_COUNT 10
#endif 
