/*******************************************************************
 *  * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *   *
 *    * $Id: rtm6rmap.h,v 1.3 2009/08/24 13:12:07 prabuc Exp $
 *     *
 *      * Description:This file contains prototype declarations in 
 *       *             Route Map functionality.                                  
 *        *                               
 *         *******************************************************************/
#ifndef _RTM6_RMAP_H
#define _RTM6_RMAP_H

/* Macros */
#define RTM6_NOT_REGISTERED_WITH_RMAP     1
#define RTM6_REGISTERED_WITH_RMAP         2

/* Proto types */
INT4 Rtm6CheckIsRouteMapConfiguredInCxt (tRtm6Cxt *pRtm6Cxt, UINT2 u2Proto);
VOID Rtm6ApplyRouteMapRuleAndRedistributeInCxt (tRtm6Cxt * pRtm6Cxt , UINT2 u2SrcProtoId, 
                    tIp6RtEntry *pRtInfo, UINT1 u1IsRtBest, UINT2 u2ChgBit,
                    UINT2 u2DstProtoMask);
INT4 Rtm6HandleRouteMapChgsInRts (tIp6RtEntry *pRtInfo, VOID *pAppSpecData);
VOID Rtm6ProcessRouteMapChanges (UINT1 *pu1RMapName);
VOID Rtm6ResetRouteMapBitMaskInCxt (tRtm6Cxt * pRtm6Cxt, 
                                    UINT2 *pu2DestProtoMask);
VOID Rtm6SetRouteMapBitMaskInCxt (tRtm6Cxt * pRtm6Cxt, 
                             UINT2 *pu2DestProtoMask);

INT4 Rtm6SendRouteMapUpdateMsg (UINT1 *pu1RMapName , 
                                UINT4 u4Status);


#endif

