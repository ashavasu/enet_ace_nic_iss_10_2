/*******************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rtm6prot.h,v 1.27 2017/12/26 13:34:22 siva Exp $
 *
 * Description:This file contains prototype declarations in 
 *             RTM6 module.                                  
 *                               
 *******************************************************************/
#ifndef _RTM6_PROTOS_H
#define _RTM6_PROTOS_H

/* Contains prototypes of functions defined in RTM6 module */
/* rtm6main.c */
VOID                Rtm6GetSizingParams (VOID);
UINT2               Rtm6GetRegnIndexFromRegnIdInCxt (tRtm6Cxt *pRtm6Cxt, 
                                                     tRtm6RegnId *pRegnId);
VOID                Rtm6ProcessOneSecTimerExpiry (VOID);
VOID                Rtm6ProcessFRTTimerExpiry (VOID);
VOID                Rtm6ProcessTick (VOID);
INT4                Rtm6AddRedisEventInCxt (tRtm6Cxt *pRtm6Cxt, 
                                       tRtm6RegnId *pRegnId, UINT4 u4ProtoMask,
                                       UINT4 u4Event);
INT4                Rtm6StartGRTmr (tRtm6RegnId *pRegnId, tIp6Buf *pBuf);
VOID                Rtm6StopGRTmr (tRtm6RegnId *pRegnId);
VOID                Rtm6ProcessGRTimerExpiry (tRtm6RegnId *pRegnId);
VOID                Rtm6InitControlTableToDenyModeInCxt (tRtm6Cxt *pRtm6Cxt);
VOID                Rtm6ReInitRtInCxt (tRtm6Cxt *pRtm6Cxt,
                                       tRtm6RegnId *pRegnId);
UINT2               Rtm6GetFreeIndexInRt (tRtm6RegnId *pRegnId);
INT1                Rtm6InitTrieForCurrentApp (tRtm6RegnId *pRegnId);
INT4                Rtm6ProcessRegistration (tRtm6RegnMsg RegnMsg);
VOID                Rtm6HandleRrd6EnableMsg (tRtm6RegnId *pRegnId,
                                             tCRU_BUF_CHAIN_HEADER * pBuf);
VOID                Rtm6HandleRrd6DisableMsg (tRtm6RegnId *pRegnId,
                                              tCRU_BUF_CHAIN_HEADER * pBuf);
INT1                Rtm6SendAckToRpInCxt (tRtm6Cxt * pRtm6Cxt,
                                          tRtm6RegnId *pRegnId);
INT4                Rtm6ProcessProtoMaskEvent (tRtm6RedisNode *pRtm6RedisNode);
VOID                Rtm6RouteRedistributionInCxt (tRtm6Cxt *pRtm6Cxt, 
                                                  tIp6RtEntry *pRtUpdate,
                                            UINT1 u1IsRtBest, UINT2 u2ChgBit,
                                            UINT2 u2DestProtoMask);
VOID                Rtm6DuplicateAndSendToRpsInCxt (tRtm6Cxt * pRtm6Cxt,
                                                    tRtm6MsgHdr *pMsgHdr,
                                              tNetIpv6RtInfo *pNetRtInfo,
                                              tRtm6RegnId *pSrcRegnId,
                                              UINT2 *pu2DestProtoMask);
VOID                Rtm6ClearProtoRouteFromTrieInCxt (tRtm6Cxt * pRtm6Cxt,
                                                 tRtm6RegnId *pRegnId);
INT4                Rtm6MemInit (VOID);
VOID                Rtm6MemDeInit (VOID);
INT4                Rtm6CreateCxt (UINT4 u4ContextId);
VOID                Rtm6DeleteCxt (UINT4 u4ContextId);
VOID                Rtm6VcmMsgHandler (tCRU_BUF_CHAIN_HEADER * pBuf);    
VOID                Rtm6VcmCallBackFn (UINT4 u4IpIfIndex, 
                                       UINT4 u4VcmCxtId, UINT1 u1BitMap);
VOID                Rtm6ProcessTimerExpiry (VOID);


/* rtm6filt.c */
VOID                Rtm6CheckRouteForRedistributionInCxt (tRtm6Cxt *pRmt6Cxt, 
                                                    tRtm6RegnId *pRegnId,
                                                    tIp6RtEntry * pRtInfo,
                                                    UINT2 *pu2DestProtoMask);
VOID                Rtm6UpdateDestMaskInCxt (tRtm6Cxt *pRmt6Cxt,
                                             tIp6RtEntry * pRtInfo,
                                        tRtm6RegnId *pSrcRegnId,
                                        UINT2 *pu2DestProtoMask);
VOID                Rtm6CheckForOspfIntOrExtInCxt (tRtm6Cxt *pRmt6Cxt,
                                              tRtm6RegnId *pRegnId, UINT4 u4Tag,
                                              UINT2 *pu2DestProtoMask);
VOID                Rtm6FilterCheckInCxt (tRtm6Cxt *pRmt6Cxt,
                                     tRtm6RegnId *pSrcRegnId, tIp6RtEntry * pRtInfo,
                                     UINT2 *pu2DestProtoMask);
VOID                Rtm6HandleChgInDefMode (tRrd6ControlInfo * pNewFilter,
                                           UINT1 u1Status);
VOID                Rtm6HandleChgInFilterTable (tRrd6ControlInfo * pNewFilter,
                                               UINT1 u1Status);
INT4                Rtm6HandleOspfTypeRouteRedistribution (tIp6RtEntry *pRtInfo,
                                                          VOID *pAppSpecData);
VOID                Rtm6HandleChgInOspfType (tRtm6RegnId *pRegnId,
                                             UINT1 u1OspfType,
                                             UINT1 u1PermitOrDeny);
VOID                Rtm6DeleteAnEntryFromControlTable(tRrd6ControlInfo *pControlTableEntry);
VOID                Rtm6HandleNewEntryToControlTable(tRrd6ControlInfo *pNewFilter);
VOID                Rtm6GetRoutesFromCRTToRPs(tRrd6ControlInfo *pNewFilter);
INT4                Rtm6HandleFilteredRouteRedistribution (tIp6RtEntry *pRtInfo,
                                                           VOID *pAppSpecData);
VOID                Rtm6FilterRedistributedRoutes (tRrd6ControlInfo * pNewFilter);
VOID                Rtm6AddrMask (tIp6Addr * pOutPrefix, tIp6Addr * pPrefix,
                                  UINT4 u4PrefLen);
INT4 Rtm6CheckForMatch (UINT2 u2RedisProtoMask, UINT1 u1MatchType);
INT4 Rtm6CheckForLevel (UINT2 u2RedisProtoMask, UINT1 u1MatchType);


/* rtm6tskmg.c */
INT4                Rtm6HandleExportMaskEvent (tIp6RtEntry *pRtInfo,
                                               VOID *pAppSpecData);

INT1                Rtm6EnqueuePktToRpsInCxt (tRtm6Cxt * pRtm6Cxt,
                                         tRtm6RespInfo *pRespInfo,
                                         tRtm6MsgHdr *pMsgHdr,
                                         tRtm6RegnId *pRegnId);

/* rtm6trie.c */
INT4    Ip6ForwardingTableAddRouteInCxt PROTO ((tRtm6Cxt * pRtm6Cxt,
                                                tIp6RtEntry * pIp6Route));
INT4    Ip6ForwardingTableModifyRouteInCxt PROTO ((tRtm6Cxt * pRtm6Cxt,
                                                   tIp6RtEntry * pIp6Route));
INT4    Ip6ForwardingTableDeleteRouteInCxt PROTO ((tRtm6Cxt * pRtm6Cxt,
                                                   tIp6RtEntry * pIp6Route));

INT4    Rtm6TrieCreateInCxt PROTO ((tRtm6Cxt *pRtm6Cxt, tRtm6RegnId *pRegnId));
VOID    Rtm6TrieCbDelete (VOID *pInput);
INT4    Rtm6TrieCbClearRtEntry (VOID *pDelParams, VOID **ppAppSpecPtr,
                                tKey Key);
VOID   *Rtm6TrieCbClearTrieInit (tInputParams *pInputParams,
                                 VOID (AppSpec) (VOID *),
                                 VOID *pOutputParams);
VOID    Rtm6TrieCbClearTrieDeInit (VOID *pDelParams);
INT4    Rtm6TrieAddEntryInCxt (tRtm6Cxt *pRtm6Cxt, 
                               tIp6RtEntry *pNewRtEntry);
INT4    Rtm6TrieCbAddEntry (tInputParams *pInputParams, VOID *pOutputParams,
                            VOID **ppAppSpecInfo, VOID *pNewAppSpecInfo);
INT4    Rtm6TrieDeleteEntryInCxt (tRtm6Cxt *pRtm6Cxt, tIp6RtEntry *pDelRtEntry);
INT4    Rtm6TrieCbDeleteEntry (tInputParams *pInputParams,
                               VOID **ppAppSpecInfoList,
                               VOID *pOutputParams, VOID *pAppSpecInfo,
                               tKey Key);
INT4    Rtm6TrieCbSearchEntry (tInputParams *pInputParams, VOID *pOutParams,
                               VOID *pAppSpecInfo);
INT4    Rtm6TrieCbLookupEntry  (tInputParams * pInputParams,
                               VOID * pOutputParams, VOID *pAppSpecInfo,
                               UINT2 u2KeySize, tKey Key);
INT4    Rtm6TrieCbBestMatch (UINT2 u2KeySize, tInputParams * pInputParams,
                             VOID *pOutputParams, VOID *pAppPtr, tKey Key);
INT4    Rtm6TrieUpdateEntryInCxt (tRtm6Cxt * pRtm6Cxt, 
                                  tIp6RtEntry  *pIP6RtEntry, UINT4 u4Metric);
INT4    Rtm6TrieCbUpdateEntry (tInputParams *pInputParams, VOID *pOutputParams,
                               VOID **ppAppPtr, VOID *pAppSpecInfo,
                               UINT4 u4NewMetric);

VOID    Ip6ScanRouteTableForBestRouteInCxt PROTO ((tRtm6Cxt *pRtm6Cxt, 
                                              tIp6Addr   *pIp6InAddr,
                                              UINT1      u1InAddrPrefixLen,
                                              INT4 (*pAppSpecFunc)
                                                     (tIp6RtEntry *pIp6RtEntry,
                                                      VOID *pAppSpecData),
                                              UINT4 u4MaxRouteCnt,
                                              tIp6Addr   *pIp6OutAddr,
                                              UINT1      *pu1OutAddrPrefixLen,
                                              VOID *pAppSpecData));
/* Route redistribution specific */
VOID    Rtm6HandleRouteUpdatesInCxt   PROTO ((tRtm6Cxt *pRtm6Cxt, tIp6RtEntry *pRtUpdate,
                                         UINT1 u1IsRtBest,
                                         UINT2 u2ChgBit));
INT4    Rtm6TrieDeleteInCxt (tRtm6Cxt * pRtm6Cxt, tRtm6RegnId *pRegnId);
VOID    Rtm6InitPreferenceInCxt (tRtm6Cxt *pRtm6Cxt);
INT4    Rtm6TrieSearchExactEntryInCxt PROTO ((tRtm6Cxt * pRtm6Cxt, 
                                         tIp6RtEntry   *pRtEntry,
                                         tIp6RtEntry  **ppRtEntryNode,
                                         VOID         **ppRibNode));
INT4    Rtm6TrieGetFirstEntryInCxt    PROTO ((tRtm6Cxt * pRtm6Cxt, 
                                         tIp6RtEntry **ppFirstRtEntry,
                                         VOID **pRibNode));
INT4    Rtm6TrieGetNextEntryInCxt     PROTO ((tRtm6Cxt * pRtm6Cxt, 
                                         tIp6RtEntry *pRtEntry,
                                         tIp6RtEntry **ppNextRtEntry, 
                                         VOID **ppRibNode));
INT4    Ip6GetBestRouteEntryInCxt     PROTO ((tRtm6Cxt * pRtm6Cxt,
                                              tIp6Addr *pDest,
                                         UINT1 u1PrefixLen,
                                         tIp6RtEntry **ppIp6OutRtInfo));
INT4    Ip6GetFwdRouteEntryInCxt      PROTO ((tRtm6Cxt * pRtm6Cxt, 
                                              tIp6Addr *pDest,
                                         UINT1 u1PrefixLen,
                                         tIp6RtEntry **ppIp6OutRtInfo));
INT4    Ip6GetNextBestRouteEntryInCxt PROTO ((tRtm6Cxt *pRtm6Cxt, 
                                              tIp6Addr *pDest, UINT1 u1PrefixLen,
                                         tIp6RtEntry **ppIp6OutRtInfo));

/* Fn PROTO TYPE Definition for rtm6cxtutl.c */
PUBLIC INT4      UtilRtm6GetVcmSystemMode PROTO ((UINT2 u2ProtocolId));
PUBLIC INT4      UtilRtm6GetVcmSystemModeExt PROTO ((UINT2 u2ProtocolId));
PUBLIC INT4      UtilRtm6VcmIsVcExist PROTO ((UINT4 u4Rtm6CxtId));
PUBLIC tRtm6Cxt * UtilRtm6GetCxt PROTO ((UINT4 u4Rtm6CxtId));
PUBLIC INT4      UtilRtm6GetFirstCxtId PROTO ((UINT4 *pu4Rtm6CxtId));
PUBLIC INT4      UtilRtm6GetNextCxtId PROTO ((UINT4 u4Rtm6CxtId,
                                             UINT4 *pu4NextRtm6CxtId));
PUBLIC INT4      UtilRtm6SetContext PROTO ((UINT4 u4Rtm6CxtId));
PUBLIC VOID      UtilRtm6ResetContext PROTO ((VOID));
PUBLIC INT4      UtilRtm6IsValidCxtId PROTO ((UINT4 u4Rtm6CxtId));
PUBLIC INT4      UtilRtm6GetCxtIdFromCxtName PROTO ((UINT1 *, UINT4 *));
PUBLIC INT4      UtilRtm6GetVcmAliasName PROTO((UINT4,UINT1 *));
PUBLIC VOID
UtilRtm6IncMsrForRtm6Scalar PROTO ((UINT4 u4SetVal, UINT4 *pu4ObjectId,
                                    UINT4 u4OidLength));
PUBLIC VOID
UtilRtm6IncMsrForRtm6Table PROTO ((UINT4 u4Rtm6CxtId, INT4 i4SetVal, UINT4 *pu4ObjectId,
                                   UINT4 u4OidLength, CHR1 c1Type));
PUBLIC VOID
UtilRtm6IncMsrForRrd6ControlTable PROTO ((UINT4 u4Rtm6CxtId,
                                 tSNMP_OCTET_STRING_TYPE *pDest,
                                 INT4 i4PrefixLength,
                          INT4 i4SetVal, UINT4 *pu4ObjectId,
                          UINT4 u4OidLength, UINT1 u1RowStatus));
PUBLIC VOID
UtilRtm6IncMsrForRrd6ProtoTable PROTO ((UINT4 u4Rtm6CxtId, INT4 i4ProtoId,
                          INT4 i4SetVal, UINT4 *pu4ObjectId,
                          UINT4 u4OidLength));
    
PUBLIC INT4
UtilRtm6GetCurrCxtId PROTO ((VOID));
    
PUBLIC INT4     Rtm6ClearContextEntries PROTO ((UINT4 u4ContextId));
    
    
/*ECMPv6 specific functions*/
VOID
Rtm6UtilSetFlagforRoutesInNH(tRtm6Cxt *pRtm6Cxt, tPNh6Node * pPNh6Entry, UINT4 u4ReachFlag);

INT4                                                                                             Rtm6UtilRetryNpPrgForFailedRoute (tRtm6FrtInfo * pRtm6FrtInfo);   

VOID
Rtm6UpdateNHFromEcmp6PRT (tRtm6Cxt *pRtm6Cxt, tPNh6Node *pPNh6Entry);

VOID
Rtm6DelAllEcmp6RtFromEcmp6PrtInCxt (tRtm6Cxt * pRtm6Cxt, tIp6RtEntry * pEcmpRt);

INT4
Rtm6DeleteRtFromECMP6PRTInCxt (tRtm6Cxt * pRtm6Cxt, tIp6RtEntry * pEcmp6Rt);

VOID
Rtm6AddAllEcmp6RtToEcmp6PrtInCxt (tRtm6Cxt * pRtm6Cxt, tIp6RtEntry * pEcmp6Rt);

INT4
Rtm6AddRtToECMP6PRTInCxt (tRtm6Cxt * pRtm6Cxt, tIp6RtEntry * pEcmp6Rt);
VOID
Rtm6ProcessECMP6PRTTimerExpiryInCxt (VOID);

VOID
Rtm6HandleECMP6BestRoutesInCxt (tRtm6Cxt  * pRtm6Cxt, tIp6RtEntry  * pBestRtEntry,
                                UINT4  u4InMetric, UINT1 u1Operation);
VOID
Rtm6HandleECMP6RtInCxt (tRtm6Cxt * pRtm6Cxt,tIp6RtEntry * pRt,tIp6RtEntry * pBestRt,
                        UINT1 u1Operation, UINT1 u1Command);
VOID
Rtm6HandleECMP6RtAdditionInCxt (tRtm6Cxt * pRtm6Cxt, tIp6RtEntry * pRt,
                              tIp6RtEntry * pNotifiedRt, UINT1 u1RtCount);
VOID
Rtm6HandleECMP6RtDeletionInCxt (tRtm6Cxt * pRtm6Cxt, tIp6RtEntry * pRt,
                              UINT1 u1RtCount, UINT1 u1Command);
VOID
Rtm6NotifyNewECMP6RouteInCxt (tRtm6Cxt * pRtmCxt, tIp6RtEntry * pRt, INT4 i4Metric);

VOID
Rtm6DelRtFromECMP6PRTInCxt (tRtm6Cxt * pRtm6Cxt, tIp6Addr *pDst, UINT1 u1PrefixLen,
                          tIp6Addr *pNextHop, UINT4 u4Metric, INT1 i1RtProto);
VOID
Rtm6GetBestRouteCount (tIp6RtEntry * pRt, UINT4 u4Metric, UINT2 *pRtCount);

VOID 
Rtm6GetInstalledRouteCount(tIp6RtEntry *pIp6Route, UINT1 *pu1RtCount);
VOID 
Rtm6GetInstalledRouteCountStatus (tRtm6Cxt *,tIp6RtEntry *pIp6Route, UINT1 *pu1RtCount);

INT4
Rtm6DeleteRtFromPRTInCxt (tRtm6Cxt * pRtm6Cxt, tIp6RtEntry * pPRoute);

VOID
Rtm6DelRtFromPRTInCxt (tRtm6Cxt * pRtm6Cxt, tIp6Addr *pDst, UINT1 u1PrefixLen,
                          tIp6Addr *pNextHop, UINT4 u4Metric, INT1 i1RtProto);

INT4 Rtm6AddRtToPRTInCxt (tRtm6Cxt * pRtm6Cxt, tIp6RtEntry * pPRoute);

PUBLIC INT4
Rtm6TmrSetTimer (tTmrBlk * pTimer, UINT1 u1TimerId, INT4 i4Duration);

INT4
Rtm6ApiAddOrDelRtsInCxt (UINT4 u4ContextId, tIp6RtEntry *pIpRtEntry, UINT1 u1RtCommand);

INT4
Rtm6NDEvtSend (UINT4 u4ContextId, tIp6Addr *pNextHop);
INT4
Rtm6UpdateResolvedNH6EntryInCxt (tRtm6Cxt * pRtm6Cxt, tIp6Addr *pNextHop,
                                UINT1 u1Operation);
VOID
Rtm6HandleBestRouteInCxt (tRtm6Cxt * pRtm6Cxt, tIp6RtEntry * pIp6RtEntry, UINT1 u1Operation);

VOID
Rtm6StopECMPTmr (tRtm6Cxt *pRtm6Cxt);

VOID
Rtm6DelAllECMP6RtInCxt (tRtm6Cxt * pRtm6Cxt, tIp6RtEntry * pBestRt, UINT4 u4Metric);

INT4
Rtm6ApiAddOrDelRtsInNP (UINT4 u4ContextId, tIp6RtEntry *pIp6RtEntry,  UINT1 u1RtCommand);

INT4
Rtm6ApiAddOrDelEcmpRtsInCxt (UINT4 u4ContextId, tIp6RtEntry *pIp6RtEntry, tIp6RtEntry *pIp6BestRtEntry,  UINT1 u1RtCommand);

VOID
Rtm6AddAllECMP6RtInCxt (tRtm6Cxt * pRtm6Cxt, tIp6RtEntry *pBestRt);

VOID
Rtm6HandleAllEcmpRoutes(tRtm6Cxt *pRtm6Cxt, tIp6RtEntry *pNewBestRt);


VOID
Rtm6SetReachStateForRoute(UINT4 u4ContextId, tIp6RtEntry *pIp6Route);

VOID
Rtm6SetReachState (UINT4 u4ContextId, tIp6RtEntry *pIp6Route);

INT4
Rtm6AddRtToECMP6PRT(tRtm6Cxt *pRtm6Cxt, tIp6RtEntry *pEcmp6Rt);

INT4 
Rtm6DeleteRtFromECMP6PRT(tRtm6Cxt * pRtm6Cxt, tIp6RtEntry *pEcmp6Rt);

INT4
Rtm6GetNextHopCount(UINT4 u4ContextId,  tIp6RtEntry *pEcmpIp6Route);


VOID
Rtm6ApiHandleEcmpRtInNp(UINT4 u4ContextId, tIp6RtEntry *pIp6RtEntry, UINT1 u1RtCommand);

INT4
Rtm6UtilGetNdStateForRouteNH(UINT4 u4ContextId, tIp6RtEntry *pIp6RtEntry);


/* Fn PROTO TYPE Definition for rtm6utl.c */
INT4 Rtm6UtilFwdTblGetBestRouteInCxt (tRtm6Cxt * pRtm6Cxt, 
                                  tIp6RtEntry * pIp6RtEntryHead,
                                  tIp6RtEntry ** ppIp6BestRtEntry);
INT4 Rtm6UtilFwdTblGetBestAnycastRouteInCxt (tRtm6Cxt * pRtm6Cxt, 
                                  tIp6RtEntry * pIp6RtEntryHead,
                                  tIp6RtEntry ** ppIp6BestRtEntry);
VOID Rtm6UtilCopyRtInfoInCxt (tRtm6Cxt * pRtm6Cxt,
                         tIp6RtEntry *pIp6RtEntry, 
                         tNetIpv6RtInfo *pNetIpv6RtInfo);
INT4 Rtm6UtilCbHandleIfStatusChange (tRtm6TrieScanOutParams * pScanParams);

tIp6RtEntry  *Rtm6UtilDefRtLookupInCxt (tRtm6Cxt * pRtm6Cxt,
                                   tIp6RtEntry * pDefHeadNode);

INT4 Rtm6HandleProtoDown (tRtm6TrieScanOutParams * pScanParams);

tIp6RtEntry  *Rtm6GetDefRouteInCxt (tRtm6Cxt * pRtm6Cxt, INT1 i1Proto);
INT4   Rtm6UtilRoute6LookupInCxt (tRtm6Cxt * pRtm6Cxt,
                     tIp6Addr * pAddr, UINT1 u1PrefixLen,
                     UINT1 u1Preference, UINT1 *pu1FoundFlag,
                     tNetIpv6RtInfo *pNetIpv6RtInfo);

INT4
Rtm6TrieLookupEntryInCxt (
                     tRtm6Cxt * pRtm6Cxt,
                     tIp6RtEntry * pRtEntry,
                     tIp6RtEntry ** ppRtEntryNode, VOID **ppRibNode);

INT4
Rtm6UtilRoutePurgeInCxt (tRtm6Cxt * pRtm6Cxt,
                    tIp6RtEntry * pRt);
INT4
Rtm6UtilDeregister (tRtm6Cxt * pRtm6Cxt, tRtm6RegnId * pRegnId);
VOID    Rtm6TrieProcessGRRouteCleanUp (tRtm6RegnId * pRegnId, INT1 i1GRCompFlag);
INT4 RTM6GetForwardingStateInCxt (UINT4);
INT4 Rtm6GetIgpConvergenceStateInCxt (UINT4);
VOID Rtm6CxtNotifyStaticRtDeletion (UINT4 u4ContextId, tIp6RtEntry * pRt);
PUBLIC UINT4      Rtm6UtilGetTraceFlag PROTO ((UINT4 u4OspfCxtId));
PUBLIC UINT1      Rtm6UtilGetGblRrd6Status PROTO ((VOID));
INT4 Rtm6RBComparePNh (tRBElem * e1, tRBElem * e2);
INT4 Rtm6RBCompareRNh (tRBElem * e1, tRBElem * e2);

INT4  Rtm6RBTreeRedEntryCmp PROTO ((tRBElem *, tRBElem *));
INT4  Rtm6RedInitGlobalInfo PROTO ((VOID));
INT4  Rtm6RedRmRegisterProtocols PROTO ((tRmRegParams *pRmReg));
INT4  Rtm6RedRmDeRegisterProtocols PROTO ((VOID));
INT4  Rtm6RedRmCallBack PROTO ((UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen));
VOID  Rtm6RedHandleRmEvents PROTO ((VOID));
VOID  Rtm6RedHandleGoActive PROTO ((VOID));
VOID  Rtm6RedHandleGoStandby PROTO ((tRtm6RmMsg * pMsg));
VOID  Rtm6RedHandleStandbyToActive PROTO ((VOID));
VOID  Rtm6RedHandleActiveToStandby PROTO ((VOID));
VOID  Rtm6RedHandleIdleToActive PROTO ((VOID));
VOID  Rtm6RedHandleIdleToStandby PROTO ((VOID));
VOID  Rtm6RedProcessPeerMsgAtActive PROTO ((tRmMsg * pMsg, UINT2 u2DataLen));
VOID  Rtm6RedProcessPeerMsgAtStandby PROTO ((tRmMsg * pMsg, UINT2 u2DataLen));
UINT4 Rtm6RedRmHandleProtocolEvent PROTO ((tRmProtoEvt *pEvt));
INT4  Rtm6RedRmReleaseMemoryForMsg PROTO ((UINT1 *pu1Block));
INT4  Rtm6RedSendMsgToRm PROTO ((tRmMsg * pMsg , UINT2 u2Length ));
VOID  Rtm6RedSendBulkReqMsg PROTO ((VOID));
INT4  Rtm6RedDeInitGlobalInfo PROTO ((VOID));
VOID  Rtm6RedSendBulkUpdTailMsg PROTO ((VOID));
VOID  Rtm6RedProcessBulkTailMsg PROTO ((tRmMsg * pMsg, UINT2 *pu2OffSet));
VOID  Rtm6RedAddDynamicInfo PROTO ((tIp6RtEntry *pRtm6Info, UINT4 u4CxtId));
VOID  Rtm6RedSendDynamicInfo PROTO ((VOID));
VOID  Rtm6RedSendBulkUpdMsg PROTO ((VOID));
VOID  Rtm6RedProcessBulkInfo (tRmMsg * pMsg, UINT2 *pu2OffSet);
VOID  Rtm6RedProcessDynamicInfo (tRmMsg * pMsg, UINT2 *pu2OffSet);
VOID  Rtm6RedHwAudit PROTO ((VOID));
INT4  Rtm6RedSendBulkInfo (UINT1 *pu1BulkUpdPendFlg);
VOID Rtm6RedSyncFrtInfo (tIp6RtEntry * pIp6RtEntry, UINT4 u4CxtId,
                         UINT1 u1Type ,UINT1 u1Flag);
VOID  Rtm6RedProcessFrtBulkInfo (tRmMsg * pMsg, UINT2 *pu2OffSet);
VOID  Rtm6RedProcessSyncFrtInfo (tRmMsg * pMsg, UINT2 *pu2OffSet);
INT4  Rtm6RedSendFrtBulkInfo (UINT1 *pu1BulkUpdPendFlg);
VOID  Rtm6RedSendFrtBulkUpdMsg PROTO ((VOID));

INT4  Rtm6RedUpdateRtParams (tIp6RtEntry *, UINT4);
INT4  Rtm6AddOrDelRtInFIBInCxt (UINT4 u4ContextId, tIp6RtEntry * pIp6RtEntry,UINT1 u1RtCommand);

INT4
Rtm6RmEnqChkSumMsgToRm PROTO ((UINT2, UINT2 ));

INT4
Rtm6GetShowCmdOutputAndCalcChkSum (UINT2 *);
INT4
Rtm6CliGetShowCmdOutputToFile (UINT1 *);

INT4
Rtm6CliCalcSwAudCheckSum (UINT1 *, UINT2 *);

INT1
Rtm6CliReadLineFromFile (INT4 i4Fd, INT1 *pi1Buf, INT2 i2MaxLen, INT2 *pi2ReadLen);
INT4 
Rtm6AddNextHopForRoute (tRtm6Cxt * pRtm6Cxt, tIp6RtEntry * pIp6Route);
INT4
Rtm6DeleteNextHopforRoute (tRtm6Cxt * pRtm6Cxt, tIp6RtEntry * pIp6Route);

INT4 Rtm6SetMaxRoute (UINT1, UINT4);
INT4      Rtm6UtilHandleProtocolShutdown(UINT4 u4ContextId,UINT1 u1Protocol);
INT4      Rtm6UtilHandleProtocolIfShutdown(UINT4 u4ContextId,UINT1 u1Protocol,
        UINT4 u4IfIndex);
VOID      Rtm6ClearProtoSpecificRouteFromTrieInCxt( tRtm6Cxt *pRtmCxt, UINT1 u1ProtoId);

#endif /* _RTM6_PROTOS_H */

