
# ifndef fsrtmOCON_H
# define fsrtmOCON_H
/*
 *  The Constant Declarations for
 *  fsrrd6Scalar
 */

# define FSRRD6ROUTERID                                    (1)
# define FSRRD6FILTERBYOSPFTAG                             (2)
# define FSRRD6FILTEROSPFTAG                               (3)
# define FSRRD6FILTEROSPFTAGMASK                           (4)
# define FSRRD6ROUTERASNUMBER                              (5)
# define FSRRD6ADMINSTATUS                                 (6)
# define FSRRD6TRACE                                       (7)
# define FSRRD6THROTLIMIT                                  (8)

/*
 *  The Constant Declarations for
 *  fsRrd6ControlTable
 */

# define FSRRD6CONTROLDESTIPADDRESS                        (1)
# define FSRRD6CONTROLNETMASKLEN                           (2)
# define FSRRD6CONTROLSOURCEPROTO                          (3)
# define FSRRD6CONTROLDESTPROTO                            (4)
# define FSRRD6CONTROLROUTEEXPORTFLAG                      (5)
# define FSRRD6CONTROLROWSTATUS                            (6)

/*
 *  The Constant Declarations for
 *  fsRrd6RoutingProtoTable
 */

# define FSRRD6ROUTINGPROTOID                              (1)
# define FSRRD6ROUTINGREGNID                               (2)
# define FSRRD6ROUTINGPROTOTASKIDENT                       (3)
# define FSRRD6ROUTINGPROTOQUEUEIDENT                      (4)
# define FSRRD6ALLOWOSPFAREAROUTES                         (5)
# define FSRRD6ALLOWOSPFEXTROUTES                          (6)

#endif /*  fsrtm6OCON_H  */
