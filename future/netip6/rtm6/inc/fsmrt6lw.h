/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmrt6lw.h,v 1.5 2015/10/19 12:22:09 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIRtm6GlobalTrace ARG_LIST((UINT4 *));

INT1
nmhGetFsMIRtm6ThrotLimit ARG_LIST((UINT4 *));

INT1
nmhGetFsMIRtm6MaximumBgpRoutes ARG_LIST((UINT4 *));

INT1
nmhGetFsMIRtm6MaximumOspfRoutes ARG_LIST((UINT4 *));

INT1
nmhGetFsMIRtm6MaximumRipRoutes ARG_LIST((UINT4 *));

INT1
nmhGetFsMIRtm6MaximumStaticRoutes ARG_LIST((UINT4 *));

INT1
nmhGetFsMIRtm6MaximumISISRoutes ARG_LIST((UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIRtm6GlobalTrace ARG_LIST((UINT4 ));

INT1
nmhSetFsMIRtm6ThrotLimit ARG_LIST((UINT4 ));

INT1
nmhSetFsMIRtm6MaximumBgpRoutes ARG_LIST((UINT4 ));

INT1
nmhSetFsMIRtm6MaximumOspfRoutes ARG_LIST((UINT4 ));

INT1
nmhSetFsMIRtm6MaximumRipRoutes ARG_LIST((UINT4 ));

INT1
nmhSetFsMIRtm6MaximumStaticRoutes ARG_LIST((UINT4 ));

INT1
nmhSetFsMIRtm6MaximumISISRoutes ARG_LIST((UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIRtm6GlobalTrace ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FsMIRtm6ThrotLimit ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FsMIRtm6MaximumBgpRoutes ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FsMIRtm6MaximumOspfRoutes ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FsMIRtm6MaximumRipRoutes ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FsMIRtm6MaximumStaticRoutes ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FsMIRtm6MaximumISISRoutes ARG_LIST((UINT4 *  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIRtm6GlobalTrace ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMIRtm6ThrotLimit ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMIRtm6MaximumBgpRoutes ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMIRtm6MaximumOspfRoutes ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMIRtm6MaximumRipRoutes ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMIRtm6MaximumStaticRoutes ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMIRtm6MaximumISISRoutes ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIRtm6Table. */
INT1
nmhValidateIndexInstanceFsMIRtm6Table ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIRtm6Table  */

INT1
nmhGetFirstIndexFsMIRtm6Table ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIRtm6Table ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIRrd6RouterId ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIRrd6FilterByOspfTag ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIRrd6FilterOspfTag ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIRrd6FilterOspfTagMask ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIRrd6RouterASNumber ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIRrd6AdminStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIRrd6Trace ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIRtm6StaticRouteDistance ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIRrd6RouterId ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsMIRrd6FilterByOspfTag ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIRrd6FilterOspfTag ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIRrd6FilterOspfTagMask ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIRrd6RouterASNumber ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIRrd6AdminStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIRrd6Trace ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsMIRtm6StaticRouteDistance ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIRrd6RouterId ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsMIRrd6FilterByOspfTag ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIRrd6FilterOspfTag ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIRrd6FilterOspfTagMask ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIRrd6RouterASNumber ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIRrd6AdminStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIRrd6Trace ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsMIRtm6StaticRouteDistance ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIRtm6Table ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIRrd6ControlTable. */
INT1
nmhValidateIndexInstanceFsMIRrd6ControlTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIRrd6ControlTable  */

INT1
nmhGetFirstIndexFsMIRrd6ControlTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE *  , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIRrd6ControlTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIRrd6ControlSourceProto ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

INT1
nmhGetFsMIRrd6ControlDestProto ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

INT1
nmhGetFsMIRrd6ControlRouteExportFlag ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

INT1
nmhGetFsMIRrd6ControlRowStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIRrd6ControlSourceProto ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

INT1
nmhSetFsMIRrd6ControlDestProto ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

INT1
nmhSetFsMIRrd6ControlRouteExportFlag ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

INT1
nmhSetFsMIRrd6ControlRowStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIRrd6ControlSourceProto ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

INT1
nmhTestv2FsMIRrd6ControlDestProto ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

INT1
nmhTestv2FsMIRrd6ControlRouteExportFlag ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

INT1
nmhTestv2FsMIRrd6ControlRowStatus ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIRrd6ControlTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIRrd6RoutingProtoTable. */
INT1
nmhValidateIndexInstanceFsMIRrd6RoutingProtoTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIRrd6RoutingProtoTable  */

INT1
nmhGetFirstIndexFsMIRrd6RoutingProtoTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIRrd6RoutingProtoTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIRrd6RoutingRegnId ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIRrd6RoutingProtoTaskIdent ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIRrd6RoutingProtoQueueIdent ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIRrd6AllowOspfAreaRoutes ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIRrd6AllowOspfExtRoutes ARG_LIST((INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIRrd6AllowOspfAreaRoutes ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsMIRrd6AllowOspfExtRoutes ARG_LIST((INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIRrd6AllowOspfAreaRoutes ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsMIRrd6AllowOspfExtRoutes ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIRrd6RoutingProtoTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIRtm6RedEntryTime ARG_LIST((INT4 *));

INT1
nmhGetFsMIRtm6RedExitTime ARG_LIST((INT4 *));
