
/*****
  fsrtmmdb.h . This File Contains the Mib DataBase.
*****/

#ifndef _FSRTMMDB_H
#define _FSRTMMDB_H

/*  The NULL FUNCTION POINTER.  */
# define NULLF ARG_LIST(((INT4 (*)(tSNMP_OID_TYPE *,\
                                          tSNMP_OID_TYPE *,\
                                          UINT1,\
                                          tSNMP_MULTI_DATA_TYPE *)) NULL))
/*  NULL VARBIND FUNCTION POINTER . */
# define NULLVBF  ARG_LIST(((tSNMP_VAR_BIND* (*)(tSNMP_OID_TYPE *,\
                                           tSNMP_OID_TYPE *,\
                                           UINT1,\
                                           UINT1))NULL))
# include "fsrtmmid.h"
# include "fsrtmcon.h"
# include "fsrtmogi.h"

/*  The Declaration of the Group Arrays. */

UINT4 au4_fsrtm6_snmp_TABLE1[] = {1,3,6,1,4,1,2076,92,2,1};
UINT4 au4_fsrtm6_snmp_TABLE2[] = {1,3,6,1,4,1,2076,92,3,1};
UINT4 au4_fsrtm6_snmp_TABLE3[] = {1,3,6,1,4,1,2076,92,1};

/*  The Declaration of the SubGroup Arrays. */

UINT4  au4_SNMP_OGP_FSRRD6SCALAR_OID [] ={0};
UINT4  au4_SNMP_OGP_FSRRD6CONTROLTABLE_OID [] ={0};
UINT4  au4_SNMP_OGP_FSRRD6ROUTINGPROTOTABLE_OID [] ={0};

/*  Declaration of Group OID Table. */
/* Each entry contains Length of Group OID, Pointer to the Group OID,
   Priority of the registering subagent, Timeout for response,
   and Number of Subgroups in that order */

 tSNMP_GroupOIDType fsrtm6_FMAS_GroupOIDTable[] =
{
   {10 , au4_fsrtm6_snmp_TABLE1 , 1 , 10 , 1},
   {10 , au4_fsrtm6_snmp_TABLE2 , 1 , 10 , 1},
   {9 , au4_fsrtm6_snmp_TABLE3 , 1 , 10 , 1}
};
/*  Declaration of Base OID Table. */
/* Each entry contains Length of Base OID, Pointer to the Base OID,
   Middle level get function pointer, Middle level test function pointer,
   Middle level set function pointer and Number of objects in that table
   in that order */

 tSNMP_BaseOIDType  fsrtm6_FMAS_BaseOIDTable[] = {
{
0,
au4_SNMP_OGP_FSRRD6CONTROLTABLE_OID,
fsRrd6ControlEntryGet,
fsRrd6ControlEntryTest,
fsRrd6ControlEntrySet,
6
},
{
0,
au4_SNMP_OGP_FSRRD6ROUTINGPROTOTABLE_OID,
fsRrd6RoutingProtoEntryGet,
fsRrd6RoutingProtoEntryTest,
fsRrd6RoutingProtoEntrySet,
6
},
{
0,
au4_SNMP_OGP_FSRRD6SCALAR_OID,
fsrrd6ScalarGet,
fsrrd6ScalarTest,
fsrrd6ScalarSet,
8
}
};
/* Declaration of MIB Object Table. */
/* Each entry contains Name of the table, Permissions for
   the object and Object name in that order */

 tSNMP_MIBObjectDescrType  fsrtm6_FMAS_MIBObjectTable[] = {
{
 SNMP_OGP_INDEX_FSRRD6CONTROLTABLE,
 NO_ACCESS,
 FSRRD6CONTROLDESTIPADDRESS
},
{
 SNMP_OGP_INDEX_FSRRD6CONTROLTABLE,
 NO_ACCESS,
 FSRRD6CONTROLNETMASKLEN
},
{
 SNMP_OGP_INDEX_FSRRD6CONTROLTABLE,
 READ_WRITE,
 FSRRD6CONTROLSOURCEPROTO
},
{
 SNMP_OGP_INDEX_FSRRD6CONTROLTABLE,
 READ_WRITE,
 FSRRD6CONTROLDESTPROTO
},
{
 SNMP_OGP_INDEX_FSRRD6CONTROLTABLE,
 READ_WRITE,
 FSRRD6CONTROLROUTEEXPORTFLAG
},
{
 SNMP_OGP_INDEX_FSRRD6CONTROLTABLE,
 READ_WRITE,
 FSRRD6CONTROLROWSTATUS
},
{
 SNMP_OGP_INDEX_FSRRD6ROUTINGPROTOTABLE,
 NO_ACCESS,
 FSRRD6ROUTINGPROTOID
},
{
 SNMP_OGP_INDEX_FSRRD6ROUTINGPROTOTABLE,
 READ_ONLY,
 FSRRD6ROUTINGREGNID
},
{
 SNMP_OGP_INDEX_FSRRD6ROUTINGPROTOTABLE,
 READ_ONLY,
 FSRRD6ROUTINGPROTOTASKIDENT
},
{
 SNMP_OGP_INDEX_FSRRD6ROUTINGPROTOTABLE,
 READ_ONLY,
 FSRRD6ROUTINGPROTOQUEUEIDENT
},
{
 SNMP_OGP_INDEX_FSRRD6ROUTINGPROTOTABLE,
 READ_WRITE,
 FSRRD6ALLOWOSPFAREAROUTES
},
{
 SNMP_OGP_INDEX_FSRRD6ROUTINGPROTOTABLE,
 READ_WRITE,
 FSRRD6ALLOWOSPFEXTROUTES
},
{
 SNMP_OGP_INDEX_FSRRD6SCALAR,
 READ_WRITE,
 FSRRD6ROUTERID
},
{
 SNMP_OGP_INDEX_FSRRD6SCALAR,
 READ_WRITE,
 FSRRD6FILTERBYOSPFTAG
},
{
 SNMP_OGP_INDEX_FSRRD6SCALAR,
 READ_WRITE,
 FSRRD6FILTEROSPFTAG
},
{
 SNMP_OGP_INDEX_FSRRD6SCALAR,
 READ_WRITE,
 FSRRD6FILTEROSPFTAGMASK
},
{
 SNMP_OGP_INDEX_FSRRD6SCALAR,
 READ_WRITE,
 FSRRD6ROUTERASNUMBER
},
{
 SNMP_OGP_INDEX_FSRRD6SCALAR,
 READ_WRITE,
 FSRRD6ADMINSTATUS
},
{
 SNMP_OGP_INDEX_FSRRD6SCALAR,
 READ_WRITE,
 FSRRD6TRACE
},
{
 SNMP_OGP_INDEX_FSRRD6SCALAR,
 READ_WRITE,
 FSRRD6THROTLIMIT
}
};

 tSNMP_GLOBAL_STRUCT fsrtm6_FMAS_Global_data =
{sizeof (fsrtm6_FMAS_GroupOIDTable) / sizeof (tSNMP_GroupOIDType),
 sizeof (fsrtm6_FMAS_BaseOIDTable) / sizeof (tSNMP_BaseOIDType)};

 int fsrtm6_MAX_OBJECTS =
(sizeof (fsrtm6_FMAS_MIBObjectTable) / sizeof (tSNMP_MIBObjectDescrType));

#endif  /* _FSRTMMDB_H */
