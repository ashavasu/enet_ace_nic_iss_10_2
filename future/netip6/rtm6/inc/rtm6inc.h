/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rtm6inc.h,v 1.14 2014/04/10 13:27:07 siva Exp $
 *
 * Description:This file contains common includes in        
 *             the RTM6 module.                              
 *                               
 *******************************************************************/

#include "lr.h"
#include "ipv6.h"
#include "ip.h"
#include "cfa.h"
#include "trieinc.h"
#include "rmgr.h"
#include "iss.h"

#include "snmctdfs.h"
#include "rtm6port.h"
#include "rtm6.h"
#include "rmap.h"
#include "rtm6defn.h"
#include "rtm6tdfs.h"
#include "rtm6prot.h"
#include "rtm6extn.h"
#include "rtm6rmap.h"

#include "snmccons.h"
#include "ip.h"
#include "arp.h"
#include "ip6util.h"
#ifdef NPAPI_WANTED
#ifndef LNXIP6_WANTED /* FSIP */
#include "ip6if.h"
#endif
#include "ip6np.h"
#include "npapi.h"
#include "ipv6npwr.h"
#endif /* NPAPI_WANTED */
#include "bgp.h"
#include "ospf.h"
#include "vcm.h"
#include "rtm6sz.h"

#ifdef MBSM_WANTED /* IP6_CHASSIS_MERGE */
#include "mbsm.h"
#endif

#ifdef LNXIP6_WANTED
#include "fssocket.h"
#endif
