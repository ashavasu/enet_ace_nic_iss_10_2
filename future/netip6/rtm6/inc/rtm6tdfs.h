/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rtm6tdfs.h,v 1.23.2.1 2018/03/09 13:40:47 siva Exp $
 *
 * Description:This file contains type definitions used     
 *             in the RTM6 module. This file also contains   
 *             message structures used for the interaction 
 *             with routing protocols.
 *
 *******************************************************************/
#ifndef _RTM6TDFS_H
#define _RTM6TDFS_H
   /* RTM6 Control Node structure */

typedef struct Rrd6ControlInfo
{
    tTMO_SLL_NODE       pNext;          /* Pointer to the next node in list */
    struct Rtm6Cxt     *pRtm6Cxt;       /* pointer to the RTM6 vrf/context to which the RRD Control
                                         * information is associated */

    tIp6Addr            DestNet;        /* Destination net */
    UINT4               u4RouteTag;     /* Route tag to be used for this route
                                         * in case of manual configuration.
                                         */
    UINT2               u2DestProtocolMask;
                                        /* Destination protocols to which this
                                         * route can be exported or not.
                                         */
    UINT1               u1SrcProtocolId;/* The Routing protocol ID which learned
                                         * this route.
                                         */
    UINT1               u1RtExportFlag; /* For host specific routes - to allow
                                         * single route in a large set of deny
                                         * entries.
                                         */
    UINT1               u1Prefixlen;    /* Range of subnets */
    UINT1               u1RowStatus;    /* Status of this entry. */
    UINT1               au1AlignmentByte[2];
}
tRrd6ControlInfo;

typedef struct Rtm6OspfTypeChg
{
    struct Rtm6Cxt   *pRtm6Cxt;   /* RTM6 Context Pointer */
    UINT2             u2ProtoId;
    UINT1             u1OspfRtType;
    UINT1             u1OspfStatus;
}
tRtm6OspfTypeChg;

typedef struct Rtm6RtMsg
{
    union{
    tNetIpv6RtInfo RtInfo;
    }unRtm6Msg;
    UINT1 u1MsgType;
    UINT1 u1PadBits[3];
} tRtm6RtMsg;


/* RTM6 Graceful Restart Timer Data Structure */
typedef struct Rtm6GRTimer
{
    tTmrBlk          Rtm6GRTmrBlk;    /* Timer used to indicate that the
                                         maximum graceful restart interval
                                         of a particular routing protocol
                                         has elapsed */
    UINT4            u4GRTmrInterval; /* Maximum restart interval
                                         of the routing protocol */
}
tRtm6GRTimer;

/* RTM6 Registration Table */
typedef struct Rtm6RegistrationInfo
{
    struct Rtm6Cxt      *pRtm6Cxt;   /* RTM6 Context Pointer */
    INT4                (*DeliverToApplication)(tRtm6RespInfo *pRespInfo,
                                                tRtm6MsgHdr *Rtm6Header);
    UINT1               au1TaskName[RTM6_MAX_TASK_NAME_LEN]; /* Task name of the
                                                             * registered
                                                             * routing protocol
                                                             */
    UINT1               au1QName[RTM6_MAX_Q_NAME_LEN];  /* Queue name to which
                                                        * messages to be posted
                                                        */
    UINT1               au1RMapName[RMAP_MAX_NAME_LEN + 4];
    tRtm6GRTimer        Rtm6GRTmr;              /* Pointer to the GR Timer
                                                   of a routing protocol */
    UINT4               u4Event;                /* Event to be given to RP */
    UINT2               u2RoutingProtocolId;    /* Routing Protocol ID */
    UINT2               u2NextRegId;            /* Next Reg ID in the RTM6 RT */
    UINT2               u2ExportProtoMask;      /* Protocols from which
                                                 * routes are exported to
                                                 * this protocols
                                                 */
    UINT1               u1AllowOspfInternals;   /* Allow or Deny all OSPF
                                                 * inter-area and
                                                 * intra-area address-mask
                                                 * pairs
                                                 */
    UINT1               u1AllowOspfExternals;   /* Allow or Deny all 
                                                 * OSPF external type 1
                                                 * and type 2 routes 
                                                 */
    UINT1               u1BitMask;              /* Bit mask to indicate 
                                                 * that the RP requires 
                                                 * route to be redistributed
                                                 * or not. If set as 0x80,
                                                 * then the routes will be
                                                 * redistributed and a
                                                 * registration acknowledge
                                                 * message will be sent. Else
                                                 * route will not be
                                                 * redistributed to this
                                                 * protocol.
                                                 */
    UINT1               u1State;                /* Processing State of this
                                                 * entry. Says whether any 
                                                 * event is in progress or
                                                 * configuration is stable.
                                                 */
    #define         RTM6_MASK_STABLE                0x01
    #define         RTM6_MASK_ENABLE_INPROGRESS     0x02
    #define         RTM6_MASK_DISABLE_INPROGRESS    0x04
    UINT1               u1RestartState;

#define   RTM6_IGP_CONVERGED           1
#define   RTM6_IGP_RESTARTING          2
    UINT1               au1AlignmentByte[1];    /* Byte for alignment */
}
tRtm6RegistrationInfo;

   /* RTM6 Redistribution Process Node */
typedef struct Rtm6RedisNode
{
    tTMO_SLL_NODE       NextNode;
    tRtm6RegnId         RegId;
    tIp6Addr            EnableInitPrefix;
    tIp6Addr            DisableInitPrefix;
    UINT4               u4EnableProtoMask;
    UINT4               u4EnablePendProtoMask;
    UINT4               u4DisableProtoMask;
    UINT1               u1EnableInitPrefixLen;
    UINT1               u1DisableInitPrefixLen;
    UINT1               u1Event;
#define             RTM6_ENABLE_PROTO_MASK          0x01
#define             RTM6_DISABLE_PROTO_MASK         0x02
    UINT1               u1Pad;
}
tRtm6RedisNode;
   /* RTM6 Configuration Table */
typedef struct Rtm6ConfigInfo
{
    UINT4               u4OspfTagValue;
    UINT4               u4OspfTagMask;
    UINT2               u2AlignmentWord;
    UINT1               u1Rrd6FilterByOspfTag;
    UINT1               u1AlignmentByte;
}
tRtm6ConfigInfo;

   /* This structure is exchanged in Module data between
      SNMP and RTM6. This structure is passed from low level routine to RTM6 if
      a new filter is added or deleted in RRD6 control table 
      or if there is a change in the allow or deny of OSPF internal 
      (intra-area or inter-area) and external (type 1 or type 2) routes. */

typedef struct Rtm6SnmpIfMsg
{
    tRrd6ControlInfo    *pRtm6Filter;
    tRtm6RegnId          RegnId;
    UINT1                u1Cmd;
    UINT1                u1OspfRtType;
    UINT1                u1PermitOrDeny;
    UINT1                u1AlignmentByte;
}
tRtm6SnmpIfMsg;

typedef struct Rtm6InitRoute
{
    struct Rtm6Cxt   *pRtm6Cxt;       /* Pointer to Rtm6 Context */
    UINT2            u2DestProto;
    UINT2            u2Data;
    UINT2            u2Event;
    UINT2            u2AlignmentByte;
}
tRtm6InitRoute;

typedef struct _Rtm6FrtInfo {
    tRBNodeEmbd  RbNode;  /* RbNode for the entry */
    tIp6Addr     DestPrefix; /* Destination Prefix */
    tIp6Addr     NextHopAddr;   /* Next hop address */
    UINT4        u4Index;
    UINT4        u4CxtId;   /* Context Id */
    UINT4        u4Flag;
    UINT4        u4Metric;
    UINT1        u1MetricType;
    UINT1        u1AddrType;
    INT1         i1Proto;      /* The protocol id through which the route
                                    is learnt */
    UINT1        u1RtCount;
    UINT1         u1Type;
    UINT1        u1PrefixLen; /* Next Hop Ip address */
    UINT1        u1Reserved[2];
}tRtm6FrtInfo;
/* Global Variable typedef Declaration */
typedef struct Rtm6GlobalInfo
{
    struct Rtm6Cxt         *apRtm6Cxt[MAX_RTM6_CONTEXT_LIMIT]; /* Pointers to RTM6Context */
    struct Rtm6Cxt         *pRtm6Cxt;       /* Pointer to Current Rtm6 Context */
    tTMO_SLL                Rtm6RedisInitList;
                                      /* Singly linked list to hold the
                                       * Route Redistribution Initialisation
                                       * nodes. */
    tTimerListId            Rtm6TmrListId;
    tRBTree                 pPRT6RBRoot; /* Root of the RBTree Maintined for
         Pending Routes(Best Routes having
         unresolved NextHop) */

    tRBTree                 pRNHT6RBRoot; /* Root of the RBTree Maintined for
           Resolved Next Hop Table(RNHT) */
 tRBTree                 pECMP6PRTRBRoot; /*Root of the RBTree for ECMPPRT routes*/
 tRBTree                 pNextHopRBRoot; /*Root of the RBTree for all NextHop entries of routes*/
 tMemPoolId              Rtm6PNextHopPoolId; /* MemPool for NextHop Entries
              in PRT*/

 tMemPoolId              Rtm6URRtPoolId;  /* MemPool for Nodes that hold the
              route with unresolved Next Hop */

 tMemPoolId              Rtm6RNextHopPoolId; /* MemPool for NextHop Entries in
              Resolved Next Hop Table */
 tMemPoolId              Rtm6RouteNextHopPoolId; /* MemPool for NextHop Entries                                                              that are being used by a route*/

    tTmrBlk                 Rtm6OneSecTmr;
    tTmrBlk                 Rtm6ECMP6PRTTmr; /*Timer to check periodically, the
                                              rechability of ECMP routes learned*/
    tMemPoolId              Rtm6RtTblPoolId;
    tMemPoolId              Rtm6CxtPoolId;
    tMemPoolId              Rtm6RrdCrtlPoolId;
    tMemPoolId              Rtm6RedisNodePoolId; /* MemPool for redist node */
    tMemPoolId              Rtm6RtMsgHdrPoolId; /* Mempool for route update messages to RTM*/
    tMemPoolId              Rtm6RmapCommunityPoolId;
    tOsixSemId              gSemId;
    tOsixQId                Rtm6RmPktQId;        /* Queue for receiving the
                                                  events sent from RM module */
    tMemPoolId              Rtm6RedDynMsgPoolId; /* MemPool for the dynamic
                                                  messages received from RM */
    tMemPoolId              Rtm6RedRMPoolId;    /* Mempool for the RM events
                                                received from RM. These events
                                                are posted to RTM module
                                                using RmPktQ */
    tMemPoolId              Rtm6FrtPoolId;    /*Mempool for the Failed route entry*/
    tMemPoolId              Rtm6RtMsgPoolId;  /* MemPool for Route update messages. */
    tRBTree                 Rtm6RedTable;    /* RBTree for holding informations
                                             learnt through RM messages. This
                                             RBTree is also used by active node
                                             to send dynamic update from a
                                             single point */
    tRBTree                 Rtm6FrtTable;  /* RBTree for holding ipv6 failed route entry.*/
    tTmrBlk                 Rtm6FRTTmr; /*Timer to check periodically, the
                                               progrmaming of faild routes*/
    tRtm6FrtInfo            Rtm6FrtInfo; 

    UINT4                   u4MaxContextLimit; 
                                /* Current Value of Maximum allowed RTM6 context 
                                 * This is minimum of MAX_RTM6_CONTEXT_LIMIT and 
                                 * FsRTM6SizingParams[MAX_RTM6_CONTEXT_SIZING_ID].u4PreAllocatedUnits */
    UINT4                   u4Ip6FwdTblRtNum;
                                      /* Number of routes in all the
                                       * RTMv6 context Routing Table */
    UINT4                   u4GlobalTrcFlag; /* Global Trace level for RTMv6 */
    UINT4                   u4ThrotLimit; /* Throttle limit for number
                                           * of routes that can be redistributed
                                           * before relinquishing the RTMv6 Task */

    UINT4                   u4StaticRts;
                                      /* Total Number of Static Routes in all
                                       * the RTM Context. */
    UINT4                   u4BgpRts;
    UINT4                   u4RipRts;
    UINT4                   u4OspfRts;
    UINT4                   u4IsisRts;
 /*Max number of protocol routes to be programmed in RTM*/
    UINT4                   u4MaxRTM6BgpRoute;
    UINT4                   u4MaxRTM6RipRoute;
    UINT4                   u4MaxRTM6OspfRoute;
    UINT4                   u4MaxRTM6StaticRoute;
    UINT4                   u4MaxRTM6IsisRoute;

    UINT1                   u1OneSecTimerStart;
    UINT1                   u1OneSecTmrStartReq;
    BOOLEAN                 bRrd6Status;
    UINT1                   u1AlignByte;
    tOsixSemId              RELSemId; /* Route Entry List SemId */    
}
tRtm6GlobalInfo;

/* RTM6 Context data structure */
typedef struct Rtm6Cxt
{
    VOID                   *pIp6RtTblRoot;
                                      /* Pointer to the Root node of
                                       * IP6 Routing Table. */
    tRtm6RegistrationInfo   aRtm6RegnTable[IP6_MAX_ROUTING_PROTOCOLS];
                                      /* RTM6 registration table */
    tRtm6ConfigInfo         Rtm6ConfigInfo;
                                      /* RTM6 configuration Parameters */
    tTMO_SLL                Rtm6CtrlList;
                                      /* Singly linked list to hold the
                                       * RRD6 Control Table. */
    tTMO_SLL                Rtm6InvdRtList; 
          /* Singly linked list to hold the
           * invalid routes i.e not reachable routes */
    
    UINT4                   au4RtPref[IP6_MAX_ROUTING_PROTOCOLS]; /* Route preference
                                                                     based on routing protocol */
    UINT4                   u4TraceFlag; /* Trace level for RTMv6 Context */
    UINT4                   u4ContextId; /* VRF Id of the RTM6 context */
    UINT4                   u4RouterId;
    UINT4                   u4Ip6FwdTblRouteNum;
                                      /* Number of rotues in the IP6
                                       * Routing Table. */
    UINT2                   u2AsNumber;
    UINT2                   u2Rtm6RtStartIndex;
                                      /* RTM6 Registration Table start index */
    UINT2                   u2Rtm6RtLastIndex;
                                      /* RTM6 Registration Table last index */
    UINT1                   u1Rrd6AdminStatus;
    UINT1                   u1AlignmentByte;
    UINT1                   u1Ip6DefaultDistance;  /*Static IPv6 Default Adiministrative distance */
    UINT1                   u1Reserved[3];
}
tRtm6Cxt;

/* Next Hop Entry in the pending Routing table (PRT) and ECMPv6 pending Routing Table (ECMP6PRT)*/
typedef struct PNextHop6Node {
    tRtm6Cxt             *pRtm6Cxt;   /* RTM6 Context Pointer */
    tIp6Addr              NextHop;  /* UnResolved next hop */
    tTMO_SLL            routeEntryList;  /* List of best routes having
                                            unresolved next hop(NextHop) */
    UINT4                u4IfIndex;
}tPNh6Node;

/*This structure is used to check if the next-hop entry is being used by a route*/
typedef struct PNextHop6ForRouteNode {
    tRtm6Cxt             *pRtm6Cxt;   /* RTM6 Context Pointer */
    tIp6Addr              NextHop;  /* Next hop */
    tTMO_SLL            routeEntryList;
    UINT4                 NextHopUsersCount;
}tPNHopHitRtNode;

/* Pending route entry in PRT and ECMP6PRT*/
typedef struct PRt6Entry
{
  tTMO_SLL_NODE   nextRtEntry;
  tIp6RtEntry     *pPend6Rt; /* Poniter to the Route Entry Added to Trie */
  tRtm6Cxt        *pRtm6Cxt;   /* RTM6 Context Pointer */
}tPRt6Entry;


/* Next Hop Entry in the Resolved next hop table */
typedef struct RNextHop6Node {
    tRtm6Cxt             *pRtm6Cxt;   /* RTM6 Context Pointer */
    tIp6Addr            NextHop;  /* A Resolved next hop */
    UINT4               u4RouteCount;
}tRNh6Node;

typedef struct _Rtm6RmCtrlMsg {
    tRmMsg           *pData;     /* RM message pointer */
    UINT2             u2DataLen; /* Length of RM message */
    UINT1             u1Event;   /* RM event */
    UINT1             au1Pad[1];
}tRtm6RmCtrlMsg;

typedef struct _Rtm6RedTable {
    tRBNodeEmbd  RbNode;  /* RbNode for the entry */
    tIp6Addr     DestPrefix; /* Destination Prefix */
    tIp6Addr     NextHopAddr;   /* Next hop address */
    UINT4        u4CxtId;   /* Context Id */
    UINT1        u1PrefixLen; /* Next Hop Ip address */
    UINT1        u1HwStatus; /* Hardware status - Present in NP or not */
    UINT1        u1DelFlag; /* delete flag - to delete the entry or not */
    INT1         i1Proto;      /* The protocol id through which the route
                                    is learnt */
    UINT4        u4HwIntfId[2];/*Used for storing hardware Interface Id of Host*/
    UINT1        u1EcmpCount;/*Used for evaluating ECMP route count*/
    UINT1        u1Reserved[3];
}tRtm6RedTable;

typedef struct _Rtm6RmMsg
{
     tRtm6RmCtrlMsg RmCtrlMsg; /* Control message from RM */
} tRtm6RmMsg;

typedef struct _Rtm6HARedMarker
{
    tIp6Addr     DestPrefix; /* Destination Prefix */
    tIp6Addr     NextHopAddr;   /* Next hop address */
    UINT4        u4CxtId;      /* Context id */
    UINT4        u4HwIntfId[2];/*Used for storing hardware Interface Id of Host*/
    UINT1        u1PrefixLen; /* Next Hop Ip address */
    INT1         i1Proto;
    INT1         i1Pad[2];
}tRtm6HARedMarker;

typedef struct _Rtm6HARedFrtMarker
{
    tIp6Addr     DestPrefix; /* Destination Prefix */
    tIp6Addr     NextHopAddr;   /* Next hop address */
    UINT4        u4CxtId;      /* Context id */
    UINT4        u4HwIntfId[2];/*Used for storing hardware Interface Id of Host*/
    UINT1        u1PrefixLen; /* Next Hop Ip address */
    INT1         i1Proto;
    INT1         i1NHType;
    INT1         i1Pad;
}tRtm6HARedFrtMarker;

typedef struct _sRtm6RedGlobalInfo{
    tRtm6HARedMarker   Rtm6HARedMarker;/* Holds the keys for the RtmTable
                                           * entry which is marked for batch
                                           * processing module.*/
    tRtm6HARedFrtMarker Rtm6HARedFrtMarker;

    INT4                i4Rtm6RedEntryTime;  /* Entry time of standby to active
                                                function */
    INT4                i4Rtm6RedExitTime;  /* Exit time of standby to active
                                                function */
    UINT1               u1FrtBulkUpdStatus;
    UINT1               u1BulkUpdStatus;/* bulk update status
                                         * RTM_HA_UPD_NOT_STARTED 0
                                         * RTM_HA_UPD_COMPLETED 1
                                         * RTM_HA_UPD_IN_PROGRESS 2,
                                         * RTM_HA_UPD_ABORTED 3 */
    UINT1               u1DynUpdStatus;/* dynamic update status
                                        * RTM_HA_UPD_NOT_STARTED 0
                                        * RTM_HA_UPD_COMPLETED 1
                                        * RTM_HA_UPD_IN_PROGRESS 2,
                                        * RTM_HA_UPD_ABORTED 3 */

    UINT1   u1NodeStatus;     /* Node status(RM_INIT/RM_ACTIVE/RM_STANDBY). */
    UINT1   u1NumPeersUp;     /* Indicates number of standby nodes
                                 that are up. */
    UINT1   bBulkReqRcvd;     /* To check whether bulk request recieved
                                 from standby before RM_STANDBY_UP event. */
    UINT1   u1RedStatus;      /* To check whether redundancy is enabled 
                                 or not */
    UINT1   au1Padding[1];    /* Reserved */
}tRtm6RedGlobalInfo;

/* Macro definitions. */
#ifdef RRD6_WANTED
#define RTM6_GLB_STATUS   OSIX_TRUE
#else
#define RTM6_GLB_STATUS   OSIX_FALSE
#endif

#define RTM6_CIDR_RT_TYPE_IN_CXT(pRtm6Cxt)\
                  (IPV6_ROUTE_TABLE | (((UINT4)pRtm6Cxt->u4ContextId) << 16))
#define MALLOC_RTM6_COMM_ENTRY(pCommunity) \
            pCommunity = (tRt6RmapComm *)MemAllocMemBlk((tMemPoolId)(gRtm6GlobalInfo.Rtm6RmapCommunityPoolId))
#define  IP_RTM6_COMM_FREE(pCommunity) \
             MemReleaseMemBlock((gRtm6GlobalInfo.Rtm6RmapCommunityPoolId), (UINT1 *)(pCommunity))
#define MALLOC_IP6_ROUTE_ENTRY(pRt) \
            pRt = (tIp6RtEntry *)MemAllocMemBlk((tMemPoolId)(gRtm6GlobalInfo.Rtm6RtTblPoolId))

#define  IP6_RT_FREE(pRt) \
             MemReleaseMemBlock((gRtm6GlobalInfo.Rtm6RtTblPoolId), (UINT1 *)(pRt))

#define RTM6_CONTROL_INFO_ALLOC(pu1Block)\
            (pu1Block = (tRrd6ControlInfo *) (MemAllocMemBlk ((tMemPoolId)gRtm6GlobalInfo.Rtm6RrdCrtlPoolId)))
#define RTM6_CONTROL_INFO_FREE(pu1Block)\
             MemReleaseMemBlock(gRtm6GlobalInfo.Rtm6RrdCrtlPoolId, (UINT1 *)(pu1Block))

#define RTM6_REDIST_NODE_ALLOC(pu1Block)\
            (pu1Block = (tRtm6RedisNode *) (MemAllocMemBlk ((tMemPoolId)gRtm6GlobalInfo.Rtm6RedisNodePoolId)))
#define RTM6_REDIST_NODE_FREE(pu1Block)\
             MemReleaseMemBlock(gRtm6GlobalInfo.Rtm6RedisNodePoolId, (UINT1 *)(pu1Block))
#define RTM6_CONTEXT_ALLOC(pu1Block)\
            (pu1Block = (tRtm6Cxt *) (MemAllocMemBlk ((tMemPoolId)gRtm6GlobalInfo.Rtm6CxtPoolId)))
#define RTM6_CONTEXT_FREE(pu1Block)\
             MemReleaseMemBlock(gRtm6GlobalInfo.Rtm6CxtPoolId, (UINT1 *)(pu1Block))


#define RTM6_ROUTE_MSG_ENTRY_ALLOC(pRt) \
             (pRt = (tRtm6RtMsg *)MemAllocMemBlk(gRtm6GlobalInfo.Rtm6RtMsgPoolId))

#define  RTM6_ROUTE_MSG_FREE(pRt) \
              MemReleaseMemBlock(gRtm6GlobalInfo.Rtm6RtMsgPoolId, (UINT1 *)(pRt))



#ifdef L2RED_WANTED
#define RTM6_RM_MSG_POOL_ID gRtm6GlobalInfo.Rtm6RedRMPoolId
#define RTM6_DYN_MSG_POOL_ID gRtm6GlobalInfo.Rtm6RedDynMsgPoolId
#endif
#define RTM6_FRT_MSG_POOL_ID gRtm6GlobalInfo.Rtm6FrtPoolId

/* RTM6 Macro used to delete the node while scanning. */
#define RTM6_SLL_DYN_Scan(pList, pNode, pTempNode, TypeCast) \
        for(((pNode) = (TypeCast)(TMO_SLL_First ((pList)))),   \
            ((pTempNode) = ((pNode == NULL) ? NULL : \
                           ((TypeCast)    \
                            (TMO_SLL_Next((pList),  \
                                          ((tTMO_SLL_NODE *)(VOID *)(pNode))))))); \
            (pNode) != NULL;  \
            (pNode) = (pTempNode),  \
            ((pTempNode) = ((pNode == NULL) ? NULL :  \
                           ((TypeCast)  \
                            (TMO_SLL_Next((pList),  \
                                          ((tTMO_SLL_NODE *)((VOID *)pNode))))))))

#endif      /* _RTM6TDFS_H */
