/*  $Id: fsrtmlow.h,v 1.5 2015/10/19 12:22:10 siva Exp $ */

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsRrd6RouterId ARG_LIST((UINT4 *));

INT1
nmhGetFsRrd6FilterByOspfTag ARG_LIST((INT4 *));

INT1
nmhGetFsRrd6FilterOspfTag ARG_LIST((INT4 *));

INT1
nmhGetFsRrd6FilterOspfTagMask ARG_LIST((INT4 *));

INT1
nmhGetFsRrd6RouterASNumber ARG_LIST((INT4 *));

INT1
nmhGetFsRrd6AdminStatus ARG_LIST((INT4 *));

INT1
nmhGetFsRrd6Trace ARG_LIST((UINT4 *));

INT1
nmhGetFsRrd6ThrotLimit ARG_LIST((UINT4 *));

INT1
nmhGetFsRrd6MaximumBgpRoutes ARG_LIST((UINT4 *));

INT1
nmhGetFsRrd6MaximumOspfRoutes ARG_LIST((UINT4 *));

INT1
nmhGetFsRrd6MaximumRipRoutes ARG_LIST((UINT4 *));

INT1
nmhGetFsRrd6MaximumStaticRoutes ARG_LIST((UINT4 *));

INT1
nmhGetFsRrd6MaximumISISRoutes ARG_LIST((UINT4 *));

INT1 
nmhGetFsRtm6StaticRouteDistance ARG_LIST((INT4 *));
/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsRrd6RouterId ARG_LIST((UINT4 ));

INT1
nmhSetFsRrd6FilterByOspfTag ARG_LIST((INT4 ));

INT1
nmhSetFsRrd6FilterOspfTag ARG_LIST((INT4 ));

INT1
nmhSetFsRrd6FilterOspfTagMask ARG_LIST((INT4 ));

INT1
nmhSetFsRrd6RouterASNumber ARG_LIST((INT4 ));

INT1
nmhSetFsRrd6AdminStatus ARG_LIST((INT4 ));

INT1
nmhSetFsRrd6Trace ARG_LIST((UINT4 ));

INT1
nmhSetFsRrd6ThrotLimit ARG_LIST((UINT4 ));

INT1
nmhSetFsRrd6MaximumBgpRoutes ARG_LIST((UINT4 ));

INT1
nmhSetFsRrd6MaximumOspfRoutes ARG_LIST((UINT4 ));

INT1
nmhSetFsRrd6MaximumRipRoutes ARG_LIST((UINT4 ));

INT1
nmhSetFsRrd6MaximumStaticRoutes ARG_LIST((UINT4 ));

INT1
nmhSetFsRrd6MaximumISISRoutes ARG_LIST((UINT4 ));

INT1
nmhSetFsRtm6StaticRouteDistance ARG_LIST((INT4 ));


/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsRrd6RouterId ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FsRrd6FilterByOspfTag ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsRrd6FilterOspfTag ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsRrd6FilterOspfTagMask ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsRrd6RouterASNumber ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsRrd6AdminStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsRrd6Trace ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FsRrd6ThrotLimit ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FsRrd6MaximumBgpRoutes ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FsRrd6MaximumOspfRoutes ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FsRrd6MaximumRipRoutes ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FsRrd6MaximumStaticRoutes ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FsRrd6MaximumISISRoutes ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FsRtm6StaticRouteDistance ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsRrd6RouterId ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsRrd6FilterByOspfTag ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsRrd6FilterOspfTag ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsRrd6FilterOspfTagMask ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsRrd6RouterASNumber ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsRrd6AdminStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsRrd6Trace ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsRrd6ThrotLimit ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsRrd6MaximumBgpRoutes ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsRrd6MaximumOspfRoutes ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsRrd6MaximumRipRoutes ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsRrd6MaximumStaticRoutes ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsRrd6MaximumISISRoutes ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsRtm6StaticRouteDistance ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsRrd6ControlTable. */
INT1
nmhValidateIndexInstanceFsRrd6ControlTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsRrd6ControlTable  */

INT1
nmhGetFirstIndexFsRrd6ControlTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *  , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsRrd6ControlTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsRrd6ControlSourceProto ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

INT1
nmhGetFsRrd6ControlDestProto ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

INT1
nmhGetFsRrd6ControlRouteExportFlag ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

INT1
nmhGetFsRrd6ControlRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsRrd6ControlSourceProto ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

INT1
nmhSetFsRrd6ControlDestProto ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

INT1
nmhSetFsRrd6ControlRouteExportFlag ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

INT1
nmhSetFsRrd6ControlRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsRrd6ControlSourceProto ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

INT1
nmhTestv2FsRrd6ControlDestProto ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

INT1
nmhTestv2FsRrd6ControlRouteExportFlag ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

INT1
nmhTestv2FsRrd6ControlRowStatus ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsRrd6ControlTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsRrd6RoutingProtoTable. */
INT1
nmhValidateIndexInstanceFsRrd6RoutingProtoTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsRrd6RoutingProtoTable  */

INT1
nmhGetFirstIndexFsRrd6RoutingProtoTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsRrd6RoutingProtoTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsRrd6RoutingRegnId ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsRrd6RoutingProtoTaskIdent ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsRrd6RoutingProtoQueueIdent ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsRrd6AllowOspfAreaRoutes ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsRrd6AllowOspfExtRoutes ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsRrd6AllowOspfAreaRoutes ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsRrd6AllowOspfExtRoutes ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsRrd6AllowOspfAreaRoutes ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsRrd6AllowOspfExtRoutes ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsRrd6RoutingProtoTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsRtm6RedEntryTime ARG_LIST((INT4 *));

INT1
nmhGetFsRtm6RedExitTime ARG_LIST((INT4 *));
