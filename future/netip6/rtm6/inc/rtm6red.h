/********************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: rtm6red.h,v 1.3 2015/04/24 11:59:52 siva Exp $
 *
 * Description: This file contains all macro definitions and 
 *              function prototypes for RTM6 Server module.
 *              
 *******************************************************************/
#ifndef __RTM6_RED_H
#define __RTM6_RED_H

#include "rtm6inc.h"

/* Represents the message types encoded in the update messages */
typedef enum {
    RTM6_RED_BULK_REQ_MESSAGE      = RM_BULK_UPDT_REQ_MSG,
    RTM6_RED_BULK_UPD_TAIL_MESSAGE = RM_BULK_UPDT_TAIL_MSG,
    RTM6_RED_BULK_INFO,
    RTM6_RED_DYN_INFO,
    RTM6_RED_SYNC_FRT_INFO,
    RTM6_RED_FRT_BULK_INFO,
    RTM6_RED_INFO
}eRtm6RedRmMsgType;

typedef enum{
    RTM6_HA_UPD_NOT_STARTED = 1,/* 1 */
    RTM6_HA_UPD_IN_PROGRESS,    /* 2 */
    RTM6_HA_UPD_COMPLETED,      /* 3 */
    RTM6_HA_UPD_ABORTED,        /* 4 */
    RTM6_HA_MAX_BLK_UPD_STATUS
} eRtm6HaBulkUpdStatus;

VOID Rtm6RedHandleDynSyncAudit (VOID);
VOID Rtm6ExecuteCmdAndCalculateChkSum  (VOID);

/* Macro Definitions for RTM6 Server Redundancy */


#define RTM6_RM_GET_NUM_STANDBY_NODES_UP() \
          gRtm6RedGlobalInfo.u1NumPeersUp = RmGetStandbyNodeCount ()

#define RTM6_NUM_STANDBY_NODES() gRtm6RedGlobalInfo.u1NumPeersUp

#define RTM6_RM_BULK_REQ_RCVD() gRtm6RedGlobalInfo.bBulkReqRcvd

#define RTM6_IS_STANDBY_UP() \
          ((gRtm6RedGlobalInfo.u1NumPeersUp > 0) ? OSIX_TRUE : OSIX_FALSE)

/* RM wanted */

#define RTM6_RM_PUT_1_BYTE(pMsg, pu4Offset, u1MesgType) \
do { \
    RM_DATA_ASSIGN_1_BYTE (pMsg, *(pu4Offset), u1MesgType); \
        *(pu4Offset) += 1;\
}while (0)

#define RTM6_RM_PUT_2_BYTE(pMsg, pu4Offset, u2MesgType) \
do { \
    RM_DATA_ASSIGN_2_BYTE (pMsg, *(pu4Offset), u2MesgType); \
        *(pu4Offset) += 2;\
}while (0)

#define RTM6_RM_PUT_4_BYTE(pMsg, pu4Offset, u4MesgType) \
do { \
    RM_DATA_ASSIGN_4_BYTE (pMsg, *(pu4Offset), u4MesgType); \
        *(pu4Offset) += 4;\
}while (0)

#define RTM6_RM_PUT_N_BYTE(pMsg, pu4Offset, psrc, u4Size) \
do { \
    RM_COPY_TO_OFFSET (pMsg, psrc, *(pu4Offset), u4Size); \
        *(pu4Offset) += u4Size;\
}while (0)

#define RTM6_RM_GET_1_BYTE(pMsg, pu4Offset, u1MesgType) \
do { \
        RM_GET_DATA_1_BYTE (pMsg, *(pu4Offset), u1MesgType); \
                *(pu4Offset) += 1;\
}while (0)

#define RTM6_RM_GET_2_BYTE(pMsg, pu4Offset, u2MesgType) \
do { \
        RM_GET_DATA_2_BYTE (pMsg, *(pu4Offset), u2MesgType); \
                *(pu4Offset) += 2;\
}while (0)

#define RTM6_RM_GET_4_BYTE(pMsg, pu4Offset, u4MesgType) \
do { \
        RM_GET_DATA_4_BYTE (pMsg, *(pu4Offset), u4MesgType); \
                *(pu4Offset) += 4;\
}while (0)

#define RTM6_RM_GET_N_BYTE(pMsg, pu4Offset, psrc, u4Size) \
do { \
        RM_GET_DATA_N_BYTE (pMsg, psrc, *(pu4Offset), u4Size); \
                *(pu4Offset) += u4Size;\
}while (0)

#define RTM6_RED_MAX_MSG_SIZE        1500
#define RTM6_RED_TYPE_FIELD_SIZE     1
#define RTM6_RED_LEN_FIELD_SIZE      2
#define RTM6_RED_MIM_MSG_SIZE        (1 + 2 + 2)
#define RTM6_RED_DYN_INFO_SIZE       (4 + 4 + 4 + 16 + 16 + 1 + 1 + 1)
#define RTM6_RED_FRT_INFO_SIZE       (4 + 16 + 16 + 1 + 1 + 1 + 1)
#define RTM6_ONE_BYTE                1
#define RTM6_TWO_BYTES               2
#define RTM6_FOUR_BYTES              4

#define RTM6_RED_BULK_UPD_TAIL_MSG_SIZE       3
#define RTM6_RED_BULK_REQ_MSG_SIZE            3

#define RTM6_RED_BULQ_REQ_SIZE       3

#define RTM6_RED_DEL         1
#define RTM6_RED_DONT_DEL    0

/* Function prototypes for RTM6 Redundancy */

#endif /* __RTM6_RED_H */
