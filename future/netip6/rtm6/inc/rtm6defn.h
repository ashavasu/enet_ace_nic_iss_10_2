/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rtm6defn.h,v 1.21.22.1 2018/03/09 13:40:47 siva Exp $
 *
 * Description: This file contains #definitions used in the 
 *              RTM6 module. 
 *
 *******************************************************************/
#ifndef _RTM6_DEFN_H
#define _RTM6_DEFN_H

/* Contains definitions used in RTM6 */

#define  RTM6_TASK_SEM_NAME              "RT6L"
#define  RTM6_SEM_CREATE_INIT_CNT        1

#define  RTM6_REL_SEM_NAME               "RELL"   /* Route Entry List Lock */

#define  MBSM_RTM6                       "MbsmRtm6"   
#define  RTM6_VRID                             0

#define   RTM6_ROUTE_PERMIT                    1
#define   RTM6_ROUTE_DENY                      2
#define   RTM6_ROUTE_UNKNOWN_MODE              3

#define   RTM6_PERMIT_TABLE         RTM6_ROUTE_PERMIT
#define   RTM6_DENY_TABLE           RTM6_ROUTE_DENY
#define   RTM6_UNKNOWN_TABLE_MODE   RTM6_ROUTE_UNKNOWN_MODE
#define   RTM6_RB_GREATER 1
#define   RTM6_RB_EQUAL   0
#define   RTM6_RB_LESS   -1

/* RTM6 Timer Related Constants */
#define   RTM6_ONESEC_TIMER                        1
#define   RTM6_GR_TIMER          2
#define   RTM6_ECMP6PRT_TIMER   4 /*Timer to initiate ND resolution for ECMP6 route*/

#define   RTM6_ECMP6PRT_TIMER_INTERVAL 10 /*Time Interval between two successive neighbor solicitations.*/
#define   RTM6_FRT_TIMER           5
#define   RTM6_FRT_TIMER_INTERVAL 60
#define   RTM6_ADMIN_STATUS_ENABLED                1
#define   RTM6_ADMIN_STATUS_DISABLED               2

#define   RTM6_FILTER_ENABLED                      1
#define   RTM6_FILTER_DISABLED                     2

#define   RTM6_REDISTRIBUTION_ENABLED              1
#define   RTM6_REDISTRIBUTION_DISABLED             2

#define   RTM6_AUTOMATIC_BIT_SET          0x80000000

#define   RTM6_OSPF_INTERNAL_ROUTES                1
#define   RTM6_OSPF_EXTERNAL_ROUTES                2

/* Possible cmd values from SNMP to RTM6 */
#define   RTM6_SET_RT_REACHABLE         1
#define   RTM6_DONT_SET_RT_REACHABLE    2
#define RTM6_CHG_IN_RRD6_CONTROL_TABLE     1
#define RTM6_OSPF_INT_EXT_CHG             2
#define RTM6_CHG_IN_RRD6_DEF_CTRL_MODE     3

#define   RTM6_INVALID_REGN_ID               0xff
#define   NULL_STR                          "\0"
#define   RTM6_INVALID_ROUTING_PROTOCOL_ID   0xff

#define   RTM6_MAX_MESSAGE_SIZE               2800

#define   RTM6_ROUTE_REDISTRIBUTED               1
#define   RTM6_ROUTE_TO_BE_REDISTRIBUTED         2
#define   RTM6_ROUTE_TO_BE_DELETED               3

#define   RTM6_DONT_REDISTRIBUTE                 0

#define   RTM6_ROUTE_DONT_DELETE                 1
#define   RTM6_ROUTE_DELETE                      2

#define   RTM6_SEND_ROUTE_UPDATE                 1
#define   RTM6_SEND_ROUTE_CHG_NOTIFY             2

#define   RTM6_MAX_TRIE_NUM_ENTRIES     50

#define   RTM6_ANY_PROTOCOL              0
#define   RTM6_MAX_KEY_SIZE              8

#define   RTM6_NO_CHG_IN_EXPORT_LIST     1
#define   RTM6_CHG_IN_EXPORT_LIST        2

#define   RTM6_SEND_RT_CHG_MSG           1
#define   RTM6_DONT_SEND_RT_CHG_MSG      2

#define DEFAULT_ROUTE            0x00000000

#define RTM6_LEFT_MOST_NIBBLE_TAG_MASK    0xf0000000
#define RTM6_BGP_DENY_TAG1        0xa0000000
#define RTM6_BGP_DENY_TAG2        0xe0000000
#define RTM6_ROUTESNUM_TO_SCAN 20 /* No Logical reason for this no */

/* Value for RTM6 Trace. */
#define RTM6_INIT_SHUT_TRC              0x00000001
#define RTM6_MANAGEMENT_TRC             0x00000002
#define RTM6_DATA_PATH_TRC              0x00000004
#define RTM6_CTRL_PATH_TRC              0x00000008
#define RTM6_OS_RESOURCE_TRC            0x00000010
#define RTM6_ALL_FAILURE_TRC            0x00000020
#define RTM6_ECMP6_FAILURE_TRC            0x00000040

#define   RTM6_AUDIT_SHOW_CMD    "snmpwalk mib name fsMIStdInetCidrRouteTable > "
#define   RTM6_CLI_MAX_GROUPS_LINE_LEN    200
#define   RTM6_CLI_EOF                  2
#define   RTM6_CLI_NO_EOF                 1
#define   RTM6_CLI_RDONLY               OSIX_FILE_RO
#define   RTM6_CLI_WRONLY               OSIX_FILE_WO

#define MAX_ECMP6_PATHS 0
#define RTM6_AUDIT_FILE_ACTIVE "/tmp/rtm6_output_file_active"
#define RTM6_AUDIT_FILE_STDBY "/tmp/rtm6_output_file_stdby"


/* Trace definitions */
#define  RTM6_MOD_NAME                  "RTM6"

#ifdef TRACE_WANTED

#define  RTM6_TRC_FLAG(CxtId)  Rtm6UtilGetTraceFlag (CxtId)

#define  CONTEXT_INFO(x, CxtId) "Context-%d:" x, CxtId
 

#define  RTM6_PKT_DUMP                  MOD_PKT_DUMP

#define RTM6_TRC(CxtId, Value, Mod_Name, Fmt)\
{\
    UtlTrcLog(RTM6_TRC_FLAG (CxtId), \
                      Value, Mod_Name, CONTEXT_INFO (Fmt, CxtId));\
}
#define RTM6_GBL_TRC(CxtId, Value, Mod_Name, Fmt)\
{\
    UtlTrcLog(RTM6_TRC_FLAG (CxtId), \
                      Value, Mod_Name, Fmt);\
}

#define RTM6_TRC_ARG1(CxtId, Value, Mod_Name, Fmt, Arg)\
{\
    UtlTrcLog(RTM6_TRC_FLAG (CxtId), \
                      Value, Mod_Name, CONTEXT_INFO (Fmt, CxtId), Arg);\
}
#define RTM6_GBL_TRC1(CxtId, Value, Mod_Name, Fmt, Arg)\
{\
    UtlTrcLog(RTM6_TRC_FLAG (CxtId), \
                      Value, Mod_Name, Fmt, Arg);\
}

#define RTM6_TRC_ARG2(CxtId, Value,Mod_Name, Fmt, Arg1, Arg2)\
{\
    UtlTrcLog(RTM6_TRC_FLAG (CxtId), \
                   Value, Mod_Name, CONTEXT_INFO (Fmt, CxtId), Arg1, Arg2);\
}
#define RTM6_GBL_TRC2(CxtId, Value, Mod_Name,  Fmt, Arg1, Arg2)\
{\
    UtlTrcLog(RTM6_TRC_FLAG (CxtId), \
                      Value, Mod_Name, Fmt, Arg1, Arg2);\
}

#define RTM6_TRC_ARG3(CxtId, Value, Mod_Name, Fmt, Arg1, Arg2, Arg3)\
{\
    UtlTrcLog(RTM6_TRC_FLAG (CxtId), \
                      Value, Mod_Name, \
        CONTEXT_INFO (Fmt, CxtId), Arg1, Arg2, Arg3);\
}

#define RTM6_GBL_TRC3(CxtId, Value, Mod_Name, Fmt, Arg1, Arg2, Arg3)\
{\
    UtlTrcLog(RTM6_TRC_FLAG (CxtId), \
                      Value, Mod_Name,  Fmt, Arg1, Arg2, Arg3);\
}

#define RTM6_TRC_ARG4(CxtId, Value, Mod_Name, Fmt, Arg1, Arg2, Arg3, Arg4)\
{\
    UtlTrcLog(RTM6_TRC_FLAG (CxtId), \
                      Value, Mod_Name, \
        CONTEXT_INFO (Fmt, CxtId), Arg1, Arg2, Arg3, Arg4);\
}

#define RTM6_GBL_TRC4(CxtId, Value, Mod_Name, Fmt, Arg1, Arg2, Arg3, Arg4)\
{\
    UtlTrcLog(RTM6_TRC_FLAG (CxtId), \
                      Value, Mod_Name, Fmt, Arg1, Arg2, Arg3, Arg4);\
}

#define RTM6_TRC_ARG5(CxtId, Value, Mod_Name, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5)    \
{\
    UtlTrcLog(RTM6_TRC_FLAG (CxtId), \
                      Value, Mod_Name, \
                   CONTEXT_INFO (Fmt, CxtId), Arg1, Arg2, Arg3, Arg4, Arg5);\
}

#define RTM6_GBL_TRC5(CxtId, Value, Mod_Name, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5)    \
{\
    UtlTrcLog(RTM6_TRC_FLAG (CxtId), \
                      Value, Mod_Name , Fmt, Arg1, Arg2, Arg3, Arg4, Arg5);\
}

#define RTM6_TRC_ARG6(CxtId, Value, Mod_Name,  Fmt, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6)  \
{\
    UtlTrcLog(RTM6_TRC_FLAG (CxtId), \
                    Value, Mod_Name, CONTEXT_INFO (Fmt, CxtId),\
                    Arg1, Arg2, Arg3, Arg4, Arg5, Arg6);\
}
#define RTM6_GBL_TRC6(CxtId, Value, Mod_Name,  Fmt, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6)  \
{\
    UtlTrcLog(RTM6_TRC_FLAG (CxtId), \
                      Value, Mod_Name, \
        Fmt, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6);\
}
#else /* TRACE_WANTED */

#define  RTM6_TRC_FLAG
#define  RTM6_PKT_DUMP 
#define  RTM6_TRC(CxtId, Value, Mod_Name, Fmt)                                        
#define  RTM6_TRC_ARG1(CxtId, Value, Mod_Name, Fmt, Arg)                                  
#define  RTM6_TRC_ARG2(CxtId, Value, Mod_Name, Fmt, Arg1, Arg2)                           
#define  RTM6_TRC_ARG3(CxtId, Value, Mod_Name, Fmt, Arg1, Arg2, Arg3)                     
#define  RTM6_TRC_ARG4(CxtId, Value, Mod_Name, Fmt, Arg1, Arg2, Arg3, Arg4)               
#define  RTM6_TRC_ARG5(CxtId, Value, Mod_Name, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5)         
#define  RTM6_TRC_ARG6(CxtId, Value, Mod_Name, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6)   

#define  RTM6_GBL_TRC(CxtId, Value, Mod_Name, Fmt)                                        
#define  RTM6_GBL_TRC1(CxtId, Value, Mod_Name, Fmt, Arg)                                  
#define  RTM6_GBL_TRC2(CxtId, Value, Mod_Name, Fmt, Arg1, Arg2)                           
#define  RTM6_GBL_TRC3(CxtId, Value, Mod_Name, Fmt, Arg1, Arg2, Arg3)                     
#define  RTM6_GBL_TRC4(CxtId, Value, Mod_Name, Fmt, Arg1, Arg2, Arg3, Arg4)               
#define  RTM6_GBL_TRC5(CxtId, Value, Mod_Name, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5)         
#define  RTM6_GBL_TRC6(CxtId, Value, Mod_Name, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6)   

#endif /* TRACE_WANTED */



#define  RTM6_MORE                      0x01
#define  RTM6_COMPLETE                  0x02

#define RTM6_MIN_ROUTER_ID              1
#define RTM6_MAX_ROUTER_ID              0xFFFFFFFF
#define  RTM6_OFFSET(x,y)    FSAP_OFFSETOF(x,y)
#define  ECMP_ENABLED(pRt)  ((pRt->u4Flag & RTM6_ECMP_RT) ? RTM6_SUCCESS : RTM6_FAILURE)
/* Calloc */
#define  RTM6_MAX_TRIE_KEY_LEN             32       /* sizeof (UINT2) * IP6_ADDR_SIZE */ 

#define  RTM6_ROUTE_PREFERENCE_BEST_METRIC  1
#define  RTM6_ROUTE_PREFERENCE_STATIC       2
#define  RTM6_ROUTE_PREFERENCE_LOCAL        3
#define  RTM6_ROUTE_PREFERENCE_RIPNG        4
#define  RTM6_ROUTE_PREFERENCE_OSPF         5
#define  RTM6_ROUTE_PREFERENCE_BGP          6

/*MACROS FOR REDISTRIBUTE MATCH TYPE FOR OSPF */
#define RTM6_MATCH_EXT_TYPE      0x1000 /* external ospf routes */
#define RTM6_MATCH_INT_TYPE      0x2000 /* internal ospf routes */
#define RTM6_MATCH_NSSA_TYPE     0x4000 /* NSSA  ospf routes*/
#define RTM6_MATCH_ALL_TYPE      0x7000 /* both internal & external */

/* OSPF route types */

#define IP_OSPF_INTRA_AREA    1
#define IP_OSPF_INTER_AREA    2
#define IP_OSPF_TYPE_1_EXT    3
#define IP_OSPF_TYPE_2_EXT    4
#define IP_OSPF_NSSA_1_EXT    5
#define IP_OSPF_NSSA_2_EXT    6



#define RTM6_GREATER      1
#define RTM6_LESSER      -1
#define RTM6_EQUAL        0

#define RTM6_ONE          1

#define   NP_PRESENT            1
#define   NP_NOT_PRESENT        2

#ifdef L2RED_WANTED
#define RTM6_GET_NODE_STATUS()  gRtm6RedGlobalInfo.u1NodeStatus
#else
#define RTM6_GET_NODE_STATUS()  RM_ACTIVE
#endif

#ifdef L2RED_WANTED
#define RTM6_GET_RMNODE_STATUS()  RmGetNodeState ()
#else
#define RTM6_GET_RMNODE_STATUS()  RM_ACTIVE
#endif

#ifdef RM_WANTED
#define   RTM6_IS_NP_PROGRAMMING_ALLOWED() \
        ((L2RED_IS_NP_PROGRAMMING_ALLOWED () == OSIX_TRUE) ? RTM6_SUCCESS : RTM6_FAILURE)
#else
#define   RTM6_IS_NP_PROGRAMMING_ALLOWED() RTM6_SUCCESS
#endif


#endif /* _RTM6_DEFN_H */
