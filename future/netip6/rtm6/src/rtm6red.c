/********************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: rtm6red.c,v 1.20 2017/12/26 13:34:23 siva Exp $
 *
 * Description: This file contains RTM6 Redundancy related 
 *              routines
 *           
 *******************************************************************/
#ifndef __RTM6RED_C
#define __RTM6RED_C

#include "rtm6inc.h"
#include "rtm6red.h"
#include "rtm6frt.h"

/************************************************************************/
/*  Function Name   : Rtm6RedInitGlobalInfo                             */
/*                                                                      */
/*  Description     : This function is invoked by the RTM6 module to    */
/*                    register itself with the RM module. This          */
/*                    registration is required to send/receive peer     */
/*                    synchronization messages and control events       */
/*                                                                      */
/*  Input(s)        : None.                                             */
/*                                                                      */
/*  Output(s)       : None.                                             */
/*                                                                      */
/*  Returns         : OSIX_SUCCESS / OSIX_FAILURE                       */
/************************************************************************/

PUBLIC INT4
Rtm6RedInitGlobalInfo (VOID)
{
    tRmRegParams        RmRegParams;

    gRtm6RedGlobalInfo.u1RedStatus = IP6_TRUE;
    gRtm6GlobalInfo.Rtm6RedTable = RBTreeCreateEmbedded
        (FSAP_OFFSETOF (tRtm6RedTable, RbNode), Rtm6RBTreeRedEntryCmp);

    if (gRtm6GlobalInfo.Rtm6RedTable == NULL)
    {
        return RTM6_FAILURE;
    }

    /* Create the RM Packet arrival Q */
    if (OsixCreateQ ((const UINT1 *) RTM6_RM_PKT_ARRIVAL_Q_NAME,
                     RTM6_RM_PKT_ARRIVAL_Q_DEPTH,
                     OSIX_GLOBAL,
                     &gRtm6GlobalInfo.Rtm6RmPktQId) != OSIX_SUCCESS)
    {
        return RTM6_FAILURE;
    }

    MEMSET (&RmRegParams, 0, sizeof (tRmRegParams));

    RmRegParams.u4EntId = RM_RTM6_APP_ID;
    RmRegParams.pFnRcvPkt = Rtm6RedRmCallBack;

    /* Registering RTM6 with RM */
    if (Rtm6RedRmRegisterProtocols (&RmRegParams) == OSIX_FAILURE)
    {

        return OSIX_FAILURE;
    }
    RTM6_GET_NODE_STATUS () = RM_INIT;
    RTM6_NUM_STANDBY_NODES () = 0;
    gRtm6RedGlobalInfo.bBulkReqRcvd = OSIX_FALSE;
    /* BulkReqRcvd is set as FALSE initially. When bulk request is received
     * the flag is set to true. This is checked for sending bulk update
     * message to the standby node */

    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : Rtm6RedRmRegisterProtocols                        */
/*                                                                      */
/*  Description     : This function is invoked by the RTM6 module to    */
/*                    register itself with the RM module. This          */
/*                    registration is required to send/receive peer     */
/*                    synchronization messages and control events       */
/*                                                                      */
/*  Input(s)        : pRmReg -- Pointer to structure tRmRegParams       */
/*                                                                      */
/*  Output(s)       : None.                                             */
/*                                                                      */
/*  Returns         : OSIX_SUCCESS / OSIX_FAILURE                       */
/************************************************************************/

PUBLIC INT4
Rtm6RedRmRegisterProtocols (tRmRegParams * pRmReg)
{

    /* Registering RTM6 protocol with RM */
    if (RmRegisterProtocols (pRmReg) == RM_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/************************************************************************/
/* Function Name      : Rtm6RedDeInitGlobalInfo                         */
/*                                                                      */
/* Description        : This function is invoked by the RTM6 module     */
/*                      during module shutdown and this function        */
/*                      deinitializes the redundancy global variables   */
/*                      and de-register RTM6 with RM.                   */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/************************************************************************/

INT4
Rtm6RedDeInitGlobalInfo (VOID)
{
    if (gRtm6GlobalInfo.Rtm6RmPktQId != IP_ZERO)
    {
        OsixDeleteQ (SELF, (const UINT1 *) RTM6_RM_PKT_ARRIVAL_Q_NAME);
        gRtm6GlobalInfo.Rtm6RmPktQId = IP_ZERO;
    }
    if (gRtm6GlobalInfo.Rtm6RedTable != NULL)
    {
        RBTreeDelete (gRtm6GlobalInfo.Rtm6RedTable);
        gRtm6GlobalInfo.Rtm6RedTable = NULL;
    }

    if (Rtm6RedRmDeRegisterProtocols () == OSIX_FAILURE)
    {

        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : Rtm6RmDeRegisterProtocols                         */
/*                                                                      */
/*  Description     : This function is invoked by the RTM6 module to    */
/*                    de-register itself with the RM module.            */
/*                                                                      */
/*  Input(s)        : None.                                             */
/*                                                                      */
/*  Output(s)       : None.                                             */
/*                                                                      */
/*  Returns         : OSIX_SUCCESS / OSIX_FAILURE                       */
/************************************************************************/

PUBLIC INT4
Rtm6RedRmDeRegisterProtocols (VOID)
{

    /* Deregiserting RTM6 from RM */
    if (RmDeRegisterProtocols (RM_RTM6_APP_ID) == RM_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/************************************************************************/
/* Function Name      : Rtm6RedRmCallBack                               */
/*                                                                      */
/* Description        : This function will be invoked by the RM module  */
/*                      to enque events and post messages to RTM6       */
/*                                                                      */
/* Input(s)           : u1Event   - Event type given by RM module       */
/*                      pData     - RM Message to enqueue               */
/*                      u2DataLen - Message size                        */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/************************************************************************/

PUBLIC INT4
Rtm6RedRmCallBack (UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen)
{
    tRtm6RmMsg         *pMsg = NULL;

    /* Callback function for RM events. The event and the message is sent as 
     * parameters to this function */

    if ((u1Event != RM_MESSAGE) &&
        (u1Event != GO_ACTIVE) &&
        (u1Event != GO_STANDBY) &&
        (u1Event != RM_STANDBY_UP) &&
        (u1Event != RM_STANDBY_DOWN) &&
        (u1Event != L2_INITIATE_BULK_UPDATES) &&
        (u1Event != RM_CONFIG_RESTORE_COMPLETE) &&
        (u1Event != RM_DYNAMIC_SYNCH_AUDIT))
    {
        return OSIX_FAILURE;
    }

    if (((u1Event == RM_MESSAGE) || (u1Event == RM_STANDBY_UP) ||
         (u1Event == RM_STANDBY_DOWN)) && (pData == NULL))
    {
        /* Queue message associated with the event is not present */
        return OSIX_FAILURE;
    }

    if ((pMsg =
         (tRtm6RmMsg *) MemAllocMemBlk ((tMemPoolId) RTM6_RM_MSG_POOL_ID)) ==
        NULL)
    {

        if (u1Event == RM_MESSAGE)
        {
            /* RM CRU Buffer Memory Release */
            RM_FREE (pData);
        }
        else if ((u1Event == RM_STANDBY_UP) || (u1Event == RM_STANDBY_DOWN))
        {
            /* Call RM API to release memory of data of these events */
            Rtm6RedRmReleaseMemoryForMsg ((UINT1 *) pData);
        }
        return OSIX_FAILURE;
    }
    MEMSET (pMsg, 0, sizeof (tRtm6RmMsg));

    pMsg->RmCtrlMsg.pData = pData;
    pMsg->RmCtrlMsg.u1Event = u1Event;
    pMsg->RmCtrlMsg.u2DataLen = u2DataLen;

    /* Send the message associated with the event to RTM6 module in a queue */
    if (OsixSendToQ (SELF,
                     (const UINT1 *) RTM6_RM_PKT_ARRIVAL_Q_NAME,
                     (tOsixMsg *) pMsg, OSIX_MSG_NORMAL) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock ((tMemPoolId) RTM6_RM_MSG_POOL_ID, (UINT1 *) pMsg);

        if (u1Event == RM_MESSAGE)
        {
            /* RM CRU Buffer Memory Release */
            RM_FREE (pData);
        }
        else if ((u1Event == RM_STANDBY_UP) || (u1Event == RM_STANDBY_DOWN))
        {
            /* Call RM API to release memory of data of these events */
            RmReleaseMemoryForMsg ((UINT1 *) pData);
        }

        return OSIX_FAILURE;
    }

    /* Post a event to RTM6 to process RM events */
    OsixSendEvent (SELF, (const UINT1 *) RTM6_MAIN_TASK_NAME,
                   RTM6_RM_PKT_EVENT);

    return OSIX_SUCCESS;
}

/************************************************************************/
/* Function Name      : Rtm6RedHandleRmEvents                           */
/*                                                                      */
/* Description        : This function is invoked by the RTM6 module to  */
/*                      process all the events and messages posted by   */
/*                      the RM module                                   */
/*                                                                      */
/* Input(s)           : pMsg -- Pointer to the RTM6 Q Msg               */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
Rtm6RedHandleRmEvents ()
{
    tRtm6RmMsg         *pMsg = NULL;
    tRmNodeInfo        *pData = NULL;
    tRmProtoAck         ProtoAck;
    tRmProtoEvt         ProtoEvt;
    UINT4               u4SeqNum = 0;

    MEMSET (&ProtoAck, 0, sizeof (tRmProtoAck));
    MEMSET (&ProtoEvt, 0, sizeof (tRmProtoEvt));

    while (OsixReceiveFromQ (SELF,
                             (const UINT1 *) RTM6_RM_PKT_ARRIVAL_Q_NAME,
                             OSIX_NO_WAIT, 0,
                             (tOsixMsg **) (VOID *) &pMsg) == OSIX_SUCCESS)
    {
        switch (pMsg->RmCtrlMsg.u1Event)
        {
            case GO_ACTIVE:

                Rtm6RedHandleGoActive ();
                break;
            case GO_STANDBY:
                Rtm6RedHandleGoStandby (pMsg);
                /* pMsg is passed as a argument to GoStandby function and
                 * it is released there. If the transformation is from active 
                 * to standby then there is no need for releasing the mempool
                 * as we are calling DeInit function. This function clears
                 * all the mempools, so it is returned here instead of break.*/
                break;
            case RM_STANDBY_UP:
                /* Standby up event, number of standby node is updated. 
                 * BulkReqRcvd flag is checked, if it is true, then Bulk update
                 * message is sent to the standby node and the flag is reset */

                pData = (tRmNodeInfo *) pMsg->RmCtrlMsg.pData;
                RTM6_NUM_STANDBY_NODES () = pData->u1NumStandby;
                Rtm6RedRmReleaseMemoryForMsg ((UINT1 *) pData);
                if (RTM6_RM_BULK_REQ_RCVD () == OSIX_TRUE)
                {
                    RTM6_RM_BULK_REQ_RCVD () = OSIX_FALSE;
                    gRtm6RedGlobalInfo.u1BulkUpdStatus =
                        RTM6_HA_UPD_NOT_STARTED;
                    gRtm6RedGlobalInfo.u1FrtBulkUpdStatus =
                        RTM6_HA_UPD_NOT_STARTED;
                    Rtm6RedSendBulkUpdMsg ();
                }
                break;
            case RM_STANDBY_DOWN:
                /* Standby down event, number of standby nodes is updated */
                pData = (tRmNodeInfo *) pMsg->RmCtrlMsg.pData;
                RTM6_NUM_STANDBY_NODES () = pData->u1NumStandby;
                Rtm6RedRmReleaseMemoryForMsg ((UINT1 *) pData);
                break;
            case RM_MESSAGE:
                /* Read the sequence number from the RM Header */
                RM_PKT_GET_SEQNUM (pMsg->RmCtrlMsg.pData, &u4SeqNum);
                /* Remove the RM Header */
                RM_STRIP_OFF_RM_HDR (pMsg->RmCtrlMsg.pData,
                                     pMsg->RmCtrlMsg.u2DataLen);
                ProtoAck.u4AppId = RM_RTM6_APP_ID;
                ProtoAck.u4SeqNumber = u4SeqNum;

                if (gRtm6RedGlobalInfo.u1NodeStatus == RM_ACTIVE)
                {
                    /* Process the message at active */
                    Rtm6RedProcessPeerMsgAtActive (pMsg->RmCtrlMsg.pData,
                                                   pMsg->RmCtrlMsg.u2DataLen);
                }
                else if (gRtm6RedGlobalInfo.u1NodeStatus == RM_STANDBY)
                {
                    /* Process the message at standby */
                    Rtm6RedProcessPeerMsgAtStandby (pMsg->RmCtrlMsg.pData,
                                                    pMsg->RmCtrlMsg.u2DataLen);
                }

                RM_FREE (pMsg->RmCtrlMsg.pData);
                RmApiSendProtoAckToRM (&ProtoAck);
                break;
            case RM_CONFIG_RESTORE_COMPLETE:
                /* Config restore complete event is sent by the RM on completing
                 * the static configurations. If the node status is init, 
                 * and the rm state is standby, then the RTM6 status is changed 
                 * from idle to standby */
                if (gRtm6RedGlobalInfo.u1NodeStatus == RM_INIT)
                {
                    if (RTM6_GET_RMNODE_STATUS () == RM_STANDBY)
                    {
                        Rtm6RedHandleIdleToStandby ();

                        ProtoEvt.u4Event = RM_STANDBY_EVT_PROCESSED;

                        RmApiHandleProtocolEvent (&ProtoEvt);
                    }
                }
                break;
            case L2_INITIATE_BULK_UPDATES:
                /* L2 Initiate bulk update is sent by RM to the standby node 
                 * to send a bulk update request to the active node */
                Rtm6RedSendBulkReqMsg ();
                break;
            case RM_DYNAMIC_SYNCH_AUDIT:
                Rtm6RedHandleDynSyncAudit ();
                break;

        }
        MemReleaseMemBlock (RTM6_RM_MSG_POOL_ID, (UINT1 *) pMsg);
    }
    return;
}

/************************************************************************/
/* Function Name      : Rtm6RedHandleGoActive                           */
/*                                                                      */
/* Description        : This function is invoked by the RTM6 upon       */
/*                      receiving the GO_ACTIVE indication from RM      */
/*                      module. And this function responds to RM with an*/
/*                      acknowledgement.                                */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
Rtm6RedHandleGoActive (VOID)
{
    tRmProtoEvt         ProtoEvt;

    ProtoEvt.u4AppId = RM_RTM6_APP_ID;
    ProtoEvt.u4Error = RM_NONE;

    /* Bulk update status is set to not started. */
    gRtm6RedGlobalInfo.u1BulkUpdStatus = RTM6_HA_UPD_NOT_STARTED;
    gRtm6RedGlobalInfo.u1FrtBulkUpdStatus = RTM6_HA_UPD_NOT_STARTED;

    if (RTM6_GET_NODE_STATUS () == RM_ACTIVE)
    {
        /* Go active received by the active node, so ignore */
        return;
    }
    if (RTM6_GET_NODE_STATUS () == RM_INIT)
    {
        /* Go active received by idle node, so state changed to active */

        Rtm6RedHandleIdleToActive ();
        ProtoEvt.u4Event = RM_IDLE_TO_ACTIVE_EVT_PROCESSED;
    }
    if (RTM6_GET_NODE_STATUS () == RM_STANDBY)
    {
        /* Go active received by standby node, 
         * Do hardware audit, and start the timers for all the rtm6 entries 
         * and change the state to active */
        Rtm6RedHandleStandbyToActive ();
        ProtoEvt.u4Event = RM_STANDBY_TO_ACTIVE_EVT_PROCESSED;
    }
    if (RTM6_RM_BULK_REQ_RCVD () == OSIX_TRUE)
    {
        /* If bulk update req received flag is true, send the bulk updates */
        RTM6_RM_BULK_REQ_RCVD () = OSIX_FALSE;
        gRtm6RedGlobalInfo.u1BulkUpdStatus = RTM6_HA_UPD_NOT_STARTED;
        gRtm6RedGlobalInfo.u1FrtBulkUpdStatus = RTM6_HA_UPD_NOT_STARTED;
        Rtm6RedSendBulkUpdMsg ();
    }
    RmApiHandleProtocolEvent (&ProtoEvt);

    return;
}

/************************************************************************/
/* Function Name      : Rtm6RedHandleGoStandby                          */
/*                                                                      */
/* Description        : This function is invoked by the RTM6 upon       */
/*                      receiving the GO_STANBY indication from RM      */
/*                      module. And this function responds to RM module */
/*                      with an acknowledgement.                        */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
Rtm6RedHandleGoStandby (tRtm6RmMsg * pMsg)
{
    tRmProtoEvt         ProtoEvt;

    ProtoEvt.u4AppId = RM_RTM6_APP_ID;
    ProtoEvt.u4Error = RM_NONE;
    UNUSED_PARAM (pMsg);

    /* Bulk update status is set to Update not started */
    gRtm6RedGlobalInfo.u1BulkUpdStatus = RTM6_HA_UPD_NOT_STARTED;
    gRtm6RedGlobalInfo.u1FrtBulkUpdStatus = RTM6_HA_UPD_NOT_STARTED;

    if (RTM6_GET_NODE_STATUS () == RM_STANDBY)
    {
        /* Go standby received by standby node. So ignore */
        return;
    }
    if (RTM6_GET_NODE_STATUS () == RM_INIT)
    {

        /* GO_STANDBY event is not processed here. It is done when
         * CONFIG_RESTORE_COMPLETE event is received. Since static bulk
         * update will be completed only by then, the acknowledgement can be
         * sent during CONFIG_RESTORE_COMPLETE handling, which will trigger RM
         * to send dynamic bulk update event to modules.
         */

        return;
    }
    else
    {
        /* Go standby received by active event. Clear all the dynamic data and
         * populate again by bulk updates */
        Rtm6RedHandleActiveToStandby ();
        ProtoEvt.u4Event = RM_STANDBY_EVT_PROCESSED;

        RmApiHandleProtocolEvent (&ProtoEvt);
    }
    return;
}

/************************************************************************/
/* Function Name      : Rtm6RedHandleIdleToActive                       */
/*                                                                      */
/* Description        : This function updates the node status to ACTIVE */
/*                      on transition from Idle to Active and also      */
/*                      updates the number of Standby node count.       */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
Rtm6RedHandleIdleToActive (VOID)
{

    /* Node status is set to active and the number of standby nodes 
     * are updated */
    RTM6_GET_NODE_STATUS () = RM_ACTIVE;
    RTM6_RM_GET_NUM_STANDBY_NODES_UP ();

    return;
}

/************************************************************************/
/* Function Name      : Rtm6RedHandleIdleToStandby                      */
/*                                                                      */
/* Description        : This function updates the node status to STANDBY*/
/*                      on transition from Idle to Standby and also     */
/*                      updates the number of Standby node count to 0   */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
Rtm6RedHandleIdleToStandby (VOID)
{

    /* the node status is set to standby and no of standby nodes is set to 0 */
    RTM6_GET_NODE_STATUS () = RM_STANDBY;
    RTM6_NUM_STANDBY_NODES () = 0;

    return;
}

/************************************************************************/
/* Function Name      : Rtm6RedHandleStandbyToActive                    */
/*                                                                      */
/* Description        : This function handles the Standby node to Active*/
/*                      node transistion. The following action are      */
/*                      performed during this transistion.              */
/*                      1.Update the Node Status and Standby node count.*/
/*                      2.Start the timers and enable the module.       */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
Rtm6RedHandleStandbyToActive (VOID)
{
    UINT4               u4CurrentTime;

    OsixGetSysTime ((tOsixSysTime *) & u4CurrentTime);
    gRtm6RedGlobalInfo.i4Rtm6RedEntryTime = (INT4) u4CurrentTime;
    /* Hardware audit is done and the hardware and software entries are 
     * checked for synchronization */

    Rtm6RedHwAudit ();

    RTM6_GET_NODE_STATUS () = RM_ACTIVE;
    RTM6_RM_GET_NUM_STANDBY_NODES_UP ();

    OsixGetSysTime ((tOsixSysTime *) & u4CurrentTime);
    gRtm6RedGlobalInfo.i4Rtm6RedExitTime = (INT4) u4CurrentTime;
    return;
}

/************************************************************************/
/* Function Name      : Rtm6RedHandleActiveToStandby                    */
/*                                                                      */
/* Description        : This function handles the Active node to Standby*/
/*                      node transistion. The following action are      */
/*                      performed during this transistion               */
/*                      1.Update the Node Status and Standby node count */
/*                      2.Disable and Enable the module                 */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
Rtm6RedHandleActiveToStandby (VOID)
{
    tIp6RtEntry        *pRtEntry = NULL;
    tIp6RtEntry        *pPrevRtInfo = NULL;
    tIp6RtEntry        *pNxtRtEntry = NULL;
    tRtm6Cxt           *pRtm6Cxt = NULL;
    VOID               *pRibNode = NULL;
    UINT4               u4CxtId = 0;
    UINT4               u4NxtCxtId = 0;

    /*update the statistics */
    RTM6_GET_NODE_STATUS () = RM_STANDBY;
    RTM6_RM_GET_NUM_STANDBY_NODES_UP ();
    if (UtilRtm6GetFirstCxtId (&u4CxtId) == RTM6_FAILURE)
    {
        return;
    }

    pRtm6Cxt = UtilRtm6GetCxt (u4CxtId);

    while (pRtm6Cxt != NULL)
    {
        if (Rtm6TrieGetFirstEntryInCxt (pRtm6Cxt,
                                        &pRtEntry, &pRibNode) == IP6_FAILURE)
        {
            return;
        }

        while (pRtEntry != NULL)
        {
            do
            {
                if ((pRtEntry->i1Proto == IP6_NETMGMT_PROTOID) ||
                    (pRtEntry->i1Proto == BGP6_ID) ||
                    (pRtEntry->i1Proto == RIPNG_ID))
                {
                    pRtEntry->u1HwStatus = 0;
                }
                pPrevRtInfo = pRtEntry;
                pRtEntry = pRtEntry->pNextAlternatepath;

            }
            while (pRtEntry != NULL);

            if (Rtm6TrieGetNextEntryInCxt (pRtm6Cxt,
                                           pPrevRtInfo, &pNxtRtEntry,
                                           &pRibNode) == RTM6_FAILURE)
            {
                break;
            }
            pRtEntry = pNxtRtEntry;
            pNxtRtEntry = NULL;
        }
        if (UtilRtm6GetNextCxtId (u4CxtId, &u4NxtCxtId) == RTM6_FAILURE)
        {
            break;
        }
        pRtm6Cxt = UtilRtm6GetCxt (u4NxtCxtId);
        u4CxtId = u4NxtCxtId;
    }
    return;
}

/************************************************************************/
/* Function Name      : Rtm6RedProcessPeerMsgAtActive                   */
/*                                                                      */
/* Description        : This routine handles messages from the other    */
/*                      node (Standby) at Active. The messages that are */
/*                      handled in Active node are                      */
/*                      1. RM_BULK_UPDT_REQ_MSG                         */
/*                                                                      */
/* Input(s)           : pMsg - RM Data buffer holding message           */
/*                      u2DataLen - Length of data in buffer.           */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
Rtm6RedProcessPeerMsgAtActive (tRmMsg * pMsg, UINT2 u2DataLen)
{
    tRmProtoEvt         ProtoEvt;
    UINT2               u2OffSet = 0;
    UINT2               u2Length = 0;
    UINT1               u1MsgType = 0;

    ProtoEvt.u4AppId = RM_RTM6_APP_ID;

    RTM6_RM_GET_1_BYTE (pMsg, &u2OffSet, u1MsgType);
    RTM6_RM_GET_2_BYTE (pMsg, &u2OffSet, u2Length);

    if (u2OffSet != u2DataLen)
    {
        /* Currently, the only RM packet expected to be processed at
         * active node is Bulk Request message which has only Type and
         * length. Hence this validation is done.
         */
        ProtoEvt.u4Error = RM_PROCESS_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
    }

    if (u2Length != RTM6_RED_BULK_REQ_MSG_SIZE)
    {
        ProtoEvt.u4Error = RM_PROCESS_FAIL;

        RmApiHandleProtocolEvent (&ProtoEvt);
    }

    if (u1MsgType == RTM6_RED_BULK_REQ_MESSAGE)
    {
        gRtm6RedGlobalInfo.u1BulkUpdStatus = RTM6_HA_UPD_NOT_STARTED;
        gRtm6RedGlobalInfo.u1FrtBulkUpdStatus = RTM6_HA_UPD_NOT_STARTED;
        if (!RTM6_IS_STANDBY_UP ())
        {
            /* This is a special case, where bulk request msg from
             * standby is coming before RM_STANDBY_UP. So no need to
             * process the bulk request now. Bulk updates will be send
             * on RM_STANDBY_UP event.
             */
            gRtm6RedGlobalInfo.bBulkReqRcvd = OSIX_TRUE;

            return;
        }
        Rtm6RedSendBulkUpdMsg ();
    }

    return;
}

/************************************************************************/
/* Function Name      : Rtm6RedProcessPeerMsgAtStandby                  */
/*                                                                      */
/* Description        : This routine handles messages from the other    */
/*                      node (Active) at standby. The messages that are */
/*                      handled in Standby node are                     */
/*                      1. RM_BULK_UPDT_TAIL_MSG                        */
/*                      2. Dynamic sync-up messages                     */
/*                      3. Dynamic bulk messages                        */
/*                                                                      */
/* Input(s)           : pMsg - RM Data buffer holding message           */
/*                      u2DataLen - Length of data in buffer.           */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
Rtm6RedProcessPeerMsgAtStandby (tRmMsg * pMsg, UINT2 u2DataLen)
{
    tRmProtoEvt         ProtoEvt;
    UINT2               u2OffSet = 0;
    UINT2               u2Length = 0;
    UINT2               u2RemMsgLen = 0;
    UINT2               u2MinLen = 0;
    UINT1               u1MsgType = 0;

    ProtoEvt.u4AppId = RM_RTM6_APP_ID;
    u2MinLen = RTM6_RED_TYPE_FIELD_SIZE + RTM6_RED_LEN_FIELD_SIZE;

    RTM6_RM_GET_1_BYTE (pMsg, &u2OffSet, u1MsgType);

    RTM6_RM_GET_2_BYTE (pMsg, &u2OffSet, u2Length);

    if (u2Length < u2MinLen)
    {
        /* If length is less that the minimum length, then ignore the message
         * and send error to RM */
        ProtoEvt.u4Error = RM_PROCESS_FAIL;

        RmApiHandleProtocolEvent (&ProtoEvt);

        return;
    }

    u2RemMsgLen = (UINT2) (u2Length - u2MinLen);

    if ((u2OffSet + u2RemMsgLen) != u2DataLen)
    {
        /* If there is a length mismatch between the message and the given 
         * length, ignore the message and send error to RM */
        ProtoEvt.u4Error = RM_PROCESS_FAIL;

        RmApiHandleProtocolEvent (&ProtoEvt);
        return;
    }
    switch (u1MsgType)
    {
        case RTM6_RED_BULK_UPD_TAIL_MESSAGE:
            if (u2Length != RTM6_RED_BULK_UPD_TAIL_MSG_SIZE)
            {
                ProtoEvt.u4Error = RM_PROCESS_FAIL;

                RmApiHandleProtocolEvent (&ProtoEvt);
                return;
            }
            Rtm6RedProcessBulkTailMsg (pMsg, &u2OffSet);
            break;
        case RTM6_RED_BULK_INFO:
            Rtm6RedProcessBulkInfo (pMsg, &u2OffSet);
            break;
        case RTM6_RED_FRT_BULK_INFO:
            Rtm6RedProcessFrtBulkInfo (pMsg, &u2OffSet);
            break;
        case RTM6_RED_DYN_INFO:
            Rtm6RedProcessDynamicInfo (pMsg, &u2OffSet);
            break;
        case RTM6_RED_SYNC_FRT_INFO:
            Rtm6RedProcessSyncFrtInfo (pMsg, &u2OffSet);
            break;
    }

    return;
}

/************************************************************************/
/* Function Name      : Rtm6RedRmReleaseMemoryForMsg                    */
/*                                                                      */
/* Description        : This function is invoked by the RTM6 module to  */
/*                      release the allocated memory by calling the RM  */
/*                      module.                                         */
/*                                                                      */
/* Input(s)           : pu1Block -- Memory Block                        */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/************************************************************************/

PUBLIC INT4
Rtm6RedRmReleaseMemoryForMsg (UINT1 *pu1Block)
{
    if (RmReleaseMemoryForMsg (pu1Block) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/************************************************************************/
/* Function Name      : Rtm6RedSendMsgToRm                              */
/*                                                                      */
/* Description        : This routine enqueues the Message to RM. If the */
/*                      sending fails, it frees the memory.             */
/*                                                                      */
/* Input(s)           : pMsg - RM Message Data Buffer                   */
/*                      u2Length - Length of the message                */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/************************************************************************/

PUBLIC INT4
Rtm6RedSendMsgToRm (tRmMsg * pMsg, UINT2 u2Length)
{
    UINT4               u4RetVal = 0;

    u4RetVal = RmEnqMsgToRmFromAppl (pMsg, u2Length,
                                     RM_RTM6_APP_ID, RM_RTM6_APP_ID);

    if (u4RetVal != RM_SUCCESS)
    {
        RM_FREE (pMsg);
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/************************************************************************/
/* Function Name      : Rtm6RedSendBulkReqMsg                           */
/*                                                                      */
/* Description        : This function sends the bulk request message    */
/*                      from standby node to Active node to initiate the*/
/*                      bulk update process.                            */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
Rtm6RedSendBulkReqMsg (VOID)
{
    tRmProtoEvt         ProtoEvt;
    tRmMsg             *pMsg = NULL;
    UINT2               u2OffSet = 0;

    ProtoEvt.u4AppId = RM_RTM6_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    /*
     *    RTM6 Bulk Request message
     *
     *    <- 1 byte ->|<- 2 bytes->|
     *    --------------------------
     *    | Msg. Type |  Length    |
     *    |           |            |
     *    |-------------------------
     */

    if ((pMsg = RM_ALLOC_TX_BUF (RTM6_RED_BULK_REQ_MSG_SIZE)) == NULL)
    {
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
        return;
    }

    RTM6_RM_PUT_1_BYTE (pMsg, &u2OffSet, RTM6_RED_BULK_REQ_MESSAGE);
    RTM6_RM_PUT_2_BYTE (pMsg, &u2OffSet, RTM6_RED_BULK_REQ_MSG_SIZE);

    if (Rtm6RedSendMsgToRm (pMsg, u2OffSet) != OSIX_SUCCESS)
    {
        ProtoEvt.u4Error = RM_SENDTO_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
        return;
    }

    return;
}

/************************************************************************/
/* Function Name      : Rtm6RedSendBulkUpdMsg                           */
/*                                                                      */
/* Description        : This function sends the binding table dynamic   */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
Rtm6RedSendBulkUpdMsg (VOID)
{
    UINT1               u1BulkUpdPendFlg = OSIX_FALSE;

    if (RTM6_IS_STANDBY_UP () == OSIX_FALSE)
    {
        return;
    }

    if (gRtm6RedGlobalInfo.u1BulkUpdStatus == RTM6_HA_UPD_NOT_STARTED)
    {
        /* If bulk update is not started, reset the marker and set the status
         * to in progress */
        gRtm6RedGlobalInfo.u1BulkUpdStatus = RTM6_HA_UPD_IN_PROGRESS;
        MEMSET (&gRtm6RedGlobalInfo.Rtm6HARedMarker, 0,
                sizeof (tRtm6HARedMarker));
    }
    if (gRtm6RedGlobalInfo.u1BulkUpdStatus == RTM6_HA_UPD_IN_PROGRESS)
    {
        /* If bulk update flag is in progress, sent the bulk Info */
        if (Rtm6RedSendBulkInfo (&u1BulkUpdPendFlg) != RTM6_SUCCESS)
        {
            /* If there is any error in sending bulk update, set the 
             * bulk update flag to aborted */
            gRtm6RedGlobalInfo.u1BulkUpdStatus = RTM6_HA_UPD_ABORTED;
        }
        else
        {
            RmSetBulkUpdatesStatus (RM_RTM6_APP_ID);
            if (u1BulkUpdPendFlg == OSIX_TRUE)
            {
                /* Send an event to the RTM6 main task indicating that there
                 * are pending entries in the RTM6 database to be synced */

                OsixSendEvent (SELF, (const UINT1 *) RTM6_MAIN_TASK_NAME,
                               RTM6_HA_PEND_BLKUPD_EVENT);
            }
            else
            {
                /* Send the tail msg to indicate the completion of Bulk
                 * update process.
                 */
                gRtm6RedGlobalInfo.u1BulkUpdStatus = RTM6_HA_UPD_COMPLETED;
                MEMSET (&gRtm6RedGlobalInfo.Rtm6HARedMarker,
                        0, sizeof (tRtm6HARedMarker));
                Rtm6RedSendFrtBulkUpdMsg ();
            }
        }
    }

    return;

}

/***********************************************************************
 * Function Name      : Rtm6RedSendBulkInfo                         
 *                                                                      
 * Description        : This function sends the Bulk update messages to 
 *                      the peer standby RTM6.                           
 *                                                                      
 * Input(s)           : None                                            
 *                                                                      
 * Output(s)          : pu1BulkUpdPendFlg                               
 *                                                                      
 * Returns            : RTM6_SUCCESS/RTM6_FAILURE                         
 ***********************************************************************/

PUBLIC INT4
Rtm6RedSendBulkInfo (UINT1 *pu1BulkUpdPendFlg)
{
    tRmMsg             *pMsg = NULL;
    tRtm6HARedMarker   *pRtm6RedMarker = NULL;
    tIp6RtEntry        *pRtEntry = NULL;
    tIp6RtEntry        *pNxtRtEntry = NULL;
    tIp6RtEntry        *pPrevRtInfo = NULL;
    tRtm6Cxt           *pRtm6Cxt = NULL;
    VOID               *pRibNode = NULL;
    tRtm6HARedMarker    Rtm6TempMarker;
    tIp6RtEntry         Ip6Route;
    tRmProtoEvt         ProtoEvt;
    INT4                i4RetVal = 0;
    UINT4               u4CxtId = 0;
    UINT4               u4NxtCxtId = 0;
    UINT2               u2MsgPacked = 0;
    UINT2               u2MsgLen = 0;
    UINT2               u2OffSet = 0;
    UINT2               u2LenOffSet = 0;
    UINT2               u2NoOffset = 0;

    MEMSET (&Rtm6TempMarker, 0, sizeof (tRtm6HARedMarker));

    MEMSET (&Ip6Route, 0, sizeof (tIp6RtEntry));
    ProtoEvt.u4AppId = RM_RTM6_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    /*
     *    RTM6 Bulk Update message
     *
     *    <- 1 byte ->|<- 2 B->|<- 2 B ->|<- 4 B ->|<- 4 B ->|<- 4 B ->|    
     *    ---------------------------------------------------------------    
     *    | Msg. Type | Length | No of   | Dest    | Mask    | NextHop |    
     *    |           |        | Entries | Network | Length  | Address |    
     *    ---------------------------------------------------------------    
     *
     *    |<-2 B->|<-1 B->|<- 1 B ->|
     *    ---------------------------
     *    | Route | Route | Hw      |
     *    | Proto | Flag  | Status  |
     *    ---------------------------
     */

    if ((pMsg = RM_ALLOC_TX_BUF (RTM6_RED_MAX_MSG_SIZE)) == NULL)
    {
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
        return RTM6_FAILURE;
    }

    pRtm6RedMarker = &(gRtm6RedGlobalInfo.Rtm6HARedMarker);

    RTM6_RM_PUT_1_BYTE (pMsg, &u2OffSet, RTM6_RED_BULK_INFO);

    /* INITIALLY SET THE LENGTH TO MAXIMUM LENGTH */
    u2LenOffSet = u2OffSet;
    RTM6_RM_PUT_2_BYTE (pMsg, &u2OffSet, u2MsgLen);
    u2NoOffset = u2OffSet;
    RTM6_RM_PUT_2_BYTE (pMsg, &u2OffSet, u2MsgPacked);

    u4CxtId = pRtm6RedMarker->u4CxtId;
    pRtm6Cxt = UtilRtm6GetCxt (u4CxtId);
    if (pRtm6Cxt == NULL)
    {
        RM_FREE (pMsg);
        return RTM6_FAILURE;;
    }

    MEMCPY (&Ip6Route.dst, &(pRtm6RedMarker->DestPrefix), IPVX_IPV6_ADDR_LEN);
    MEMCPY (&Ip6Route.nexthop, &(pRtm6RedMarker->NextHopAddr),
            IPVX_IPV6_ADDR_LEN);
    Ip6Route.u1Prefixlen = pRtm6RedMarker->u1PrefixLen;
    Ip6Route.u4HwIntfId[0] = pRtm6RedMarker->u4HwIntfId[0];
    Ip6Route.u4HwIntfId[1] = pRtm6RedMarker->u4HwIntfId[1];
    if (MEMCMP (pRtm6RedMarker, &Rtm6TempMarker, sizeof (tRtm6HARedMarker))
        == IP6_ZERO)
    {
        i4RetVal = Rtm6TrieGetFirstEntryInCxt (pRtm6Cxt, &pRtEntry, &pRibNode);
    }
    else
    {
        i4RetVal =
            Rtm6TrieGetNextEntryInCxt (pRtm6Cxt, &Ip6Route, &pRtEntry,
                                       &pRibNode);
    }
    while (pRtm6Cxt != NULL)
    {
        while (pRtEntry != NULL)
        {
            do
            {
                if ((u2OffSet + RTM6_RED_DYN_INFO_SIZE) > RTM6_RED_MAX_MSG_SIZE)
                {
                    /* If message size is greater than MAX_SIZE, 
                     * set the pending flag as true and break */
                    *pu1BulkUpdPendFlg = OSIX_TRUE;
                    break;
                }

                if ((pRtEntry->i1Proto == IP6_NETMGMT_PROTOID) ||
                    (pRtEntry->i1Proto == BGP6_ID) ||
                    (pRtEntry->i1Proto == RIPNG_ID))
                {
                    RTM6_RM_PUT_4_BYTE (pMsg, &u2OffSet, u4CxtId);
                    RTM6_RM_PUT_4_BYTE (pMsg, &u2OffSet,
                                        pRtEntry->u4HwIntfId[0]);
                    RTM6_RM_PUT_4_BYTE (pMsg, &u2OffSet,
                                        pRtEntry->u4HwIntfId[1]);
                    RTM6_RM_PUT_N_BYTE (pMsg, &u2OffSet,
                                        (pRtEntry->dst.u1_addr),
                                        IPVX_IPV6_ADDR_LEN);
                    RTM6_RM_PUT_N_BYTE (pMsg, &u2OffSet,
                                        (pRtEntry->nexthop.u1_addr),
                                        IPVX_IPV6_ADDR_LEN);
                    RTM6_RM_PUT_1_BYTE (pMsg, &u2OffSet, pRtEntry->u1Prefixlen);
                    RTM6_RM_PUT_1_BYTE (pMsg, &u2OffSet, pRtEntry->i1Proto);
                    RTM6_RM_PUT_1_BYTE (pMsg, &u2OffSet, pRtEntry->u1HwStatus);
                    u2MsgPacked += 1;
                }
                pPrevRtInfo = pRtEntry;
                pRtEntry = pRtEntry->pNextAlternatepath;
            }
            while (pRtEntry != NULL);

            if (Rtm6TrieGetNextEntryInCxt (pRtm6Cxt, pPrevRtInfo,
                                           &pNxtRtEntry,
                                           &pRibNode) == RTM6_FAILURE)
            {
                break;
            }
            pRtEntry = pNxtRtEntry;
            pNxtRtEntry = NULL;
        }

        if (UtilRtm6GetNextCxtId (u4CxtId, &u4NxtCxtId) == RTM6_FAILURE)
        {
            break;
        }
        u4CxtId = u4NxtCxtId;
        pRtm6Cxt = UtilRtm6GetCxt (u4CxtId);
    }

    RTM6_RM_PUT_2_BYTE (pMsg, &u2LenOffSet, u2OffSet);
    RTM6_RM_PUT_2_BYTE (pMsg, &u2NoOffset, u2MsgPacked);

    if (u2MsgPacked == 0)
    {
        /* If message packet is 0, then do not send the message */
        RM_FREE (pMsg);
        return RTM6_SUCCESS;
    }

    if (Rtm6RedSendMsgToRm (pMsg, u2OffSet) != OSIX_SUCCESS)
    {
        *pu1BulkUpdPendFlg = OSIX_FALSE;
        ProtoEvt.u4Error = RM_SENDTO_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
        return RTM6_FAILURE;
    }

    MEMCPY (&(pRtm6RedMarker->DestPrefix), &(pPrevRtInfo->dst),
            IPVX_IPV6_ADDR_LEN);
    MEMCPY (&(pRtm6RedMarker->NextHopAddr), &(pPrevRtInfo->nexthop),
            IPVX_IPV6_ADDR_LEN);
    pRtm6RedMarker->u1PrefixLen = pPrevRtInfo->u1Prefixlen;
    pRtm6RedMarker->i1Proto = pPrevRtInfo->i1Proto;
    pRtm6RedMarker->u4CxtId = u4CxtId;
    pRtm6RedMarker->u4HwIntfId[0] = pPrevRtInfo->u4HwIntfId[0];
    pRtm6RedMarker->u4HwIntfId[1] = pPrevRtInfo->u4HwIntfId[1];
    /* Rtm6RedMarker is set to the last updated entry */
    UNUSED_PARAM (i4RetVal);
    return RTM6_SUCCESS;
}

/************************************************************************/
/* Function Name      : Rtm6RedSendBulkUpdTailMsg                       */
/*                                                                      */
/* Description        : This function sends the binding table dynamic   */
/*                      sync up message to Standby node from the Active */
/*                      node, when the RTM6 offers an IP address        */
/*                      to the RTM6 client and dynamically update the   */
/*                      binding table entries.                          */
/*                                                                      */
/* Input(s)           : pRtm6BindingInfo - binding entry to be synced up*/
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/************************************************************************/

VOID
Rtm6RedSendBulkUpdTailMsg (VOID)
{
    tRmMsg             *pMsg = NULL;
    tRmProtoEvt         ProtoEvt;
    UINT2               u2OffSet = 0;

    ProtoEvt.u4AppId = RM_RTM6_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    /* Form a bulk update tail message.

     *        <------------1 Byte----------><---2 Byte--->
     *****************************************************
     *        *                             *            *
     * RM Hdr *  RTM6_RED_BULK_UPD_TAIL_MSG  * Msg Length *
     *        *                             *            *
     *****************************************************

     * The RM Hdr shall be included by RM.
     */

    if ((pMsg = RM_ALLOC_TX_BUF (RTM6_RED_BULK_UPD_TAIL_MSG_SIZE)) == NULL)
    {
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
        return;
    }

    u2OffSet = 0;

    RTM6_RM_PUT_1_BYTE (pMsg, &u2OffSet, RTM6_RED_BULK_UPD_TAIL_MESSAGE);
    RTM6_RM_PUT_2_BYTE (pMsg, &u2OffSet, RTM6_RED_BULK_UPD_TAIL_MSG_SIZE);

    /* This routine sends the message to RM and in case of failure
     * releases the RM buffer memory
     */
    if (Rtm6RedSendMsgToRm (pMsg, u2OffSet) != OSIX_SUCCESS)
    {
        ProtoEvt.u4Error = RM_SENDTO_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
        return;
    }
    return;
}

/************************************************************************/
/* Function Name      : Rtm6RedProcessBulkTailMsg                       */
/*                                                                      */
/* Description        : This function process the bulk update tail      */
/*                      message and send bulk updates                   */
/*                                                                      */
/* Input(s)           : pMsg - RM Data buffer holding messges           */
/*                      u2DataLen - Length of data in buffer            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
Rtm6RedProcessBulkTailMsg (tRmMsg * pMsg, UINT2 *pu2OffSet)
{
    tRmProtoEvt         ProtoEvt;

    ProtoEvt.u4AppId = RM_RTM6_APP_ID;
    UNUSED_PARAM (pMsg);
    UNUSED_PARAM (pu2OffSet);

    ProtoEvt.u4Error = RM_NONE;
    ProtoEvt.u4Event = RM_PROTOCOL_BULK_UPDT_COMPLETION;

    RmApiHandleProtocolEvent (&ProtoEvt);

    return;
}

/************************************************************************
 * Function Name      : Rtm6RedSendDynamicInfo 
 *                                                
 * Description        : This function sends the binding table dynamic  
 *                      sync up message to Standby node from the Active 
 *                      node, when the RTM6 offers an IP address 
 *                      to the RTM6 client and dynamically update the   
 *                      binding table entries.                        
 *                                                                   
 * Input(s)           : None
 *                                                                    
 * Output(s)          : None                                         
 *                                                                  
 * Returns            : OSIX_SUCCESS / OSIX_FAILURE                
 ************************************************************************/

PUBLIC VOID
Rtm6RedSendDynamicInfo ()
{
    tRmMsg             *pMsg = NULL;
    tRtm6RedTable      *pRtm6RedInfo;
    tRtm6RedTable       Rtm6RedInfo;
    tRmProtoEvt         ProtoEvt;
    UINT4               u4MsgAllocSize = 0;
    UINT4               u4Count = 0;
    UINT2               u2MsgLen = 0;
    UINT2               u2OffSet = 0;
    UINT2               u2LenOffSet = 0;
    UINT2               u2MsgPacked = 0;
    UINT2               u2NoOffset = 0;

    ProtoEvt.u4AppId = RM_RTM6_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    RBTreeCount (gRtm6GlobalInfo.Rtm6RedTable, &u4Count);
    u4MsgAllocSize = RTM6_RED_MIM_MSG_SIZE;
    u4MsgAllocSize += (u4Count * (RTM6_RED_DYN_INFO_SIZE));
    u4MsgAllocSize = (u4MsgAllocSize < RTM6_RED_MAX_MSG_SIZE) ?
        u4MsgAllocSize : RTM6_RED_MAX_MSG_SIZE;

    /* Message is allcoated upto required size. The number of entries in the 
     * RBTree is got and then it is multiplied with the size of a single 
     * dynamic update to get the total count. If that length is greater than
     * MAX size, then the message is allocated upto max size */

    /*
     *    RTM6 Dynamic Update message
     *
     *    <- 1 byte ->|<- 2 B->|<- 2 B ->|<- 4 B ->|<- 4 B ->|<- 4 B ->|    
     *    ---------------------------------------------------------------    
     *    | Msg. Type | Length | No of   | Dest    | Mask    | NextHop |    
     *    |           |        | Entries | Network | Length  | Address |    
     *    ---------------------------------------------------------------    
     *
     *    |<-2 B->|<-1 B->|<- 1 B ->|
     *    ---------------------------
     *    | Route | Route | Hw      |
     *    | Proto | Flag  | Status  |
     *    ---------------------------
     */

    if ((pMsg = RM_ALLOC_TX_BUF (u4MsgAllocSize)) == NULL)
    {
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);

        return;
    }

    RTM6_RM_PUT_1_BYTE (pMsg, &u2OffSet, RTM6_RED_DYN_INFO);

    /* INITIALLY SET THE LENGTH TO MAXIMUM LENGTH */
    u2LenOffSet = u2OffSet;
    RTM6_RM_PUT_2_BYTE (pMsg, &u2OffSet, u2MsgLen);
    u2NoOffset = u2OffSet;
    RTM6_RM_PUT_2_BYTE (pMsg, &u2OffSet, u2MsgPacked);

    pRtm6RedInfo = RBTreeGetFirst (gRtm6GlobalInfo.Rtm6RedTable);
    while (pRtm6RedInfo != NULL)
    {
        if ((UINT4) (u2OffSet + RTM6_RED_DYN_INFO_SIZE) > u4MsgAllocSize)
        {
            /* If there message size is greater than the allocated size, then
             * the remaining entries are sent in another update. */
            break;
        }

        RTM6_RM_PUT_4_BYTE (pMsg, &u2OffSet, pRtm6RedInfo->u4CxtId);
        RTM6_RM_PUT_4_BYTE (pMsg, &u2OffSet, pRtm6RedInfo->u4HwIntfId[0]);
        RTM6_RM_PUT_4_BYTE (pMsg, &u2OffSet, pRtm6RedInfo->u4HwIntfId[1]);
        RTM6_RM_PUT_N_BYTE (pMsg, &u2OffSet, (pRtm6RedInfo->DestPrefix.u1_addr),
                            IPVX_IPV6_ADDR_LEN);
        RTM6_RM_PUT_N_BYTE (pMsg, &u2OffSet,
                            (pRtm6RedInfo->NextHopAddr.u1_addr),
                            IPVX_IPV6_ADDR_LEN);
        RTM6_RM_PUT_1_BYTE (pMsg, &u2OffSet, pRtm6RedInfo->u1PrefixLen);
        RTM6_RM_PUT_1_BYTE (pMsg, &u2OffSet, pRtm6RedInfo->i1Proto);
        RTM6_RM_PUT_1_BYTE (pMsg, &u2OffSet, pRtm6RedInfo->u1HwStatus);
        u2MsgPacked += 1;

        MEMSET (&Rtm6RedInfo, 0, sizeof (tRtm6RedTable));
        Rtm6RedInfo.u4CxtId = pRtm6RedInfo->u4CxtId;
        Rtm6RedInfo.u4HwIntfId[0] = pRtm6RedInfo->u4HwIntfId[0];
        Rtm6RedInfo.u4HwIntfId[1] = pRtm6RedInfo->u4HwIntfId[1];
        Rtm6RedInfo.i1Proto = pRtm6RedInfo->i1Proto;
        Rtm6RedInfo.DestPrefix = pRtm6RedInfo->DestPrefix;
        Rtm6RedInfo.u1PrefixLen = pRtm6RedInfo->u1PrefixLen;
        Rtm6RedInfo.NextHopAddr = pRtm6RedInfo->NextHopAddr;
        pRtm6RedInfo = RBTreeGetNext (gRtm6GlobalInfo.Rtm6RedTable,
                                      (tRBElem *) & Rtm6RedInfo, NULL);
    }

    RTM6_RM_PUT_2_BYTE (pMsg, &u2LenOffSet, u2OffSet);
    RTM6_RM_PUT_2_BYTE (pMsg, &u2NoOffset, u2MsgPacked);

    if (u2MsgPacked == 0)
    {
        /* If there are no new items to sent, then it the message is not sent */
        RM_FREE (pMsg);
        return;
    }

    if (Rtm6RedSendMsgToRm (pMsg, u2OffSet) != OSIX_SUCCESS)
    {
        ProtoEvt.u4Error = RM_SENDTO_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
        return;
    }
    pRtm6RedInfo =
        RBTreeGet (gRtm6GlobalInfo.Rtm6RedTable, (tRBElem *) & Rtm6RedInfo);
    if (pRtm6RedInfo != NULL)
    {
        pRtm6RedInfo->u1DelFlag = RTM6_RED_DEL;
    }
    /* The last updated message's delete flag is set */
    pRtm6RedInfo = RBTreeGetFirst (gRtm6GlobalInfo.Rtm6RedTable);
    while (pRtm6RedInfo != NULL)
    {
        RBTreeRem (gRtm6GlobalInfo.Rtm6RedTable, pRtm6RedInfo);
        if (pRtm6RedInfo->u1DelFlag == RTM6_RED_DEL)
        {
            MemReleaseMemBlock (RTM6_DYN_MSG_POOL_ID, (UINT1 *) pRtm6RedInfo);
            break;
        }
        MemReleaseMemBlock (RTM6_DYN_MSG_POOL_ID, (UINT1 *) pRtm6RedInfo);

        MEMSET (&Rtm6RedInfo, 0, sizeof (tRtm6RedTable));
        Rtm6RedInfo.u4CxtId = pRtm6RedInfo->u4CxtId;
        Rtm6RedInfo.u4HwIntfId[0] = pRtm6RedInfo->u4HwIntfId[0];
        Rtm6RedInfo.u4HwIntfId[1] = pRtm6RedInfo->u4HwIntfId[1];
        Rtm6RedInfo.i1Proto = pRtm6RedInfo->i1Proto;
        Rtm6RedInfo.DestPrefix = pRtm6RedInfo->DestPrefix;
        Rtm6RedInfo.u1PrefixLen = pRtm6RedInfo->u1PrefixLen;
        Rtm6RedInfo.NextHopAddr = pRtm6RedInfo->NextHopAddr;
        pRtm6RedInfo = RBTreeGetNext (gRtm6GlobalInfo.Rtm6RedTable,
                                      (tRBElem *) & Rtm6RedInfo, NULL);
    }

    return;
}

/************************************************************************/
/* Function Name      : Rtm6RedProcessBulkInfo                          */
/*                                                                      */
/* Description        : This function decodes the sync-up message from  */
/*                      Active node and updates the binding information */
/*                      at the Standby node.                            */
/*                                                                      */
/* Input(s)           : pMsg - RM Sync-up message                       */
/*                      u2RemMsgLen - Length of value of Message        */
/*                                                                      */
/* Output(s)          : u2OffSet - OffSet value                         */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

VOID
Rtm6RedProcessBulkInfo (tRmMsg * pMsg, UINT2 *pu2OffSet)
{
    tIp6RtEntry        *pRt6Info = NULL;
    tRtm6Cxt           *pRtm6Cxt = NULL;
    VOID               *pRibNode = NULL;
    tIp6RtEntry         Ip6RtEntry;
    tIp6Addr            NextHopAddr;
    UINT4               u4CxtId = 0;
    UINT2               u2NoOfEntries = 0;
    UINT1               u1HwStatus;
    INT1                i1Proto;

    MEMSET (&NextHopAddr, 0, sizeof (tIp6Addr));
    RTM6_RM_GET_2_BYTE (pMsg, pu2OffSet, u2NoOfEntries);
    while (u2NoOfEntries > 0)
    {
        MEMSET (&Ip6RtEntry, 0, sizeof (tIp6RtEntry));

        /* The number of entries is got from the message and it is processed */
        u2NoOfEntries--;
        RTM6_RM_GET_4_BYTE (pMsg, pu2OffSet, u4CxtId);
        RTM6_RM_GET_4_BYTE (pMsg, pu2OffSet, Ip6RtEntry.u4HwIntfId[0]);
        RTM6_RM_GET_4_BYTE (pMsg, pu2OffSet, Ip6RtEntry.u4HwIntfId[1]);
        RTM6_RM_GET_N_BYTE (pMsg, pu2OffSet, (Ip6RtEntry.dst.u1_addr),
                            IPVX_IPV6_ADDR_LEN);
        RTM6_RM_GET_N_BYTE (pMsg, pu2OffSet, NextHopAddr.u1_addr,
                            IPVX_IPV6_ADDR_LEN);
        RTM6_RM_GET_1_BYTE (pMsg, pu2OffSet, Ip6RtEntry.u1Prefixlen);
        RTM6_RM_GET_1_BYTE (pMsg, pu2OffSet, i1Proto);
        RTM6_RM_GET_1_BYTE (pMsg, pu2OffSet, u1HwStatus);
        pRtm6Cxt = UtilRtm6GetCxt (u4CxtId);
        if (pRtm6Cxt == NULL)
        {
            RM_FREE (pMsg);
            return;
        }

        if (Rtm6TrieSearchExactEntryInCxt (pRtm6Cxt, &Ip6RtEntry,
                                           &pRt6Info, &pRibNode) == IP6_SUCCESS)
        {

            while (pRt6Info != NULL)
            {
                if (Ip6AddrCompare (pRt6Info->nexthop, NextHopAddr) == IP6_ZERO)
                {
                    pRt6Info->u1HwStatus = u1HwStatus;
                    break;
                }
                pRt6Info = pRt6Info->pNextAlternatepath;
            }
        }
        else if (u1HwStatus == NP_PRESENT)
        {
            Ip6RtEntry.u1HwStatus = u1HwStatus;
            Ip6RtEntry.i1Proto = i1Proto;
            MEMCPY (Ip6RtEntry.nexthop.u1_addr, NextHopAddr.u1_addr,
                    IPVX_IPV6_ADDR_LEN);
            Rtm6RedAddDynamicInfo (&Ip6RtEntry, u4CxtId);
        }
    }

    return;
}

/************************************************************************/
/* Function Name      : Rtm6RedProcessDynamicInfo                       */
/*                                                                      */
/* Description        : This function decodes the sync-up message from  */
/*                      Active node and updates the binding information */
/*                      at the Standby node.                            */
/*                                                                      */
/* Input(s)           : pMsg - RM Sync-up message                       */
/*                      u2RemMsgLen - Length of value of Message        */
/*                                                                      */
/* Output(s)          : u2OffSet - OffSet value                         */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

VOID
Rtm6RedProcessDynamicInfo (tRmMsg * pMsg, UINT2 *pu2OffSet)
{
    tRtm6RedTable       Rtm6RedInfo;
    tRtm6RedTable      *pRtm6RedInfo = NULL;
    tIp6RtEntry        *pRt6Info = NULL;
    tRtm6Cxt           *pRtm6Cxt = NULL;
    VOID               *pRibNode = NULL;
    tIp6RtEntry         Ip6RtEntry;
    tIp6Addr            NextHopAddr;
    UINT4               u4CxtId = 0;
    UINT2               u2NoOfEntries = 0;
    UINT1               u1HwStatus;
    INT1                i1Proto;

    MEMSET (&NextHopAddr, 0, sizeof (tIp6Addr));
    MEMSET (&Rtm6RedInfo, 0, sizeof (tRtm6RedTable));
    RTM6_RM_GET_2_BYTE (pMsg, pu2OffSet, u2NoOfEntries);

    while (u2NoOfEntries > 0)
    {
        MEMSET (&Ip6RtEntry, 0, sizeof (tIp6RtEntry));
        /* The number of entries is got from the message and it is processed */
        RTM6_RM_GET_4_BYTE (pMsg, pu2OffSet, u4CxtId);
        RTM6_RM_GET_N_BYTE (pMsg, pu2OffSet, (Ip6RtEntry.dst.u1_addr),
                            IPVX_IPV6_ADDR_LEN);
        RTM6_RM_GET_N_BYTE (pMsg, pu2OffSet, NextHopAddr.u1_addr,
                            IPVX_IPV6_ADDR_LEN);
        RTM6_RM_GET_1_BYTE (pMsg, pu2OffSet, Ip6RtEntry.u1Prefixlen);
        RTM6_RM_GET_1_BYTE (pMsg, pu2OffSet, i1Proto);
        RTM6_RM_GET_1_BYTE (pMsg, pu2OffSet, u1HwStatus);
        pRtm6Cxt = UtilRtm6GetCxt (u4CxtId);
        if (pRtm6Cxt == NULL)
        {
            RM_FREE (pMsg);
            return;
        }

        if (Rtm6TrieSearchExactEntryInCxt (pRtm6Cxt, &Ip6RtEntry,
                                           &pRt6Info, &pRibNode) == IP6_SUCCESS)
        {
            while (pRt6Info != NULL)
            {
                if (Ip6AddrCompare (pRt6Info->nexthop, NextHopAddr) == IP6_ZERO)
                {
                    pRt6Info->u1HwStatus = u1HwStatus;
                    break;
                }
                pRt6Info = pRt6Info->pNextAlternatepath;
            }
        }
        else if (u1HwStatus == NP_PRESENT)
        {
            Ip6RtEntry.u1HwStatus = u1HwStatus;
            Ip6RtEntry.i1Proto = i1Proto;
            MEMCPY (Ip6RtEntry.nexthop.u1_addr, NextHopAddr.u1_addr,
                    IPVX_IPV6_ADDR_LEN);
            Rtm6RedAddDynamicInfo (&Ip6RtEntry, u4CxtId);
        }
        if (pRt6Info != NULL)
        {
            Rtm6RedInfo.DestPrefix = pRt6Info->dst;
            Rtm6RedInfo.NextHopAddr = pRt6Info->nexthop;
            Rtm6RedInfo.u1PrefixLen = pRt6Info->u1Prefixlen;
            Rtm6RedInfo.u1HwStatus = pRt6Info->u1HwStatus;
            Rtm6RedInfo.i1Proto = pRt6Info->i1Proto;
            Rtm6RedInfo.u4HwIntfId[0] = pRt6Info->u4HwIntfId[1];
            Rtm6RedInfo.u4HwIntfId[1] = pRt6Info->u4HwIntfId[0];
            if (u1HwStatus == NP_PRESENT)
            {
                pRtm6RedInfo = RBTreeGet (gRtm6GlobalInfo.Rtm6RedTable,
                                          (tRBElem *) & Rtm6RedInfo);
                if (pRtm6RedInfo != NULL)
                {
                    RBTreeRem (gRtm6GlobalInfo.Rtm6RedTable, pRtm6RedInfo);
                    MemReleaseMemBlock (RTM6_DYN_MSG_POOL_ID,
                                        (UINT1 *) pRtm6RedInfo);
                }
            }
            else if (u1HwStatus == NP_NOT_PRESENT)
            {
                Ip6RtEntry.u1HwStatus = u1HwStatus;
                Ip6RtEntry.i1Proto = i1Proto;
                MEMCPY (Ip6RtEntry.nexthop.u1_addr, NextHopAddr.u1_addr,
                        IPVX_IPV6_ADDR_LEN);
                Rtm6RedAddDynamicInfo (&Ip6RtEntry, u4CxtId);
            }
        }
        u2NoOfEntries--;
    }

    return;
}

/************************************************************************/
/* Function Name      : Rtm6RedAddDynamicInfo                           */
/*                                                                      */
/* Description        : This function adds the dynamic entries to the   */
/*                      global tree and this tree is scanned everytime  */
/*                      for sending the dynamic entry.                  */
/*                                                                      */
/* Input(s)           : pRt6Info - Rtm6 Route  message                  */
/*                      u1Operation - add or delete entry               */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/
VOID
Rtm6RedAddDynamicInfo (tIp6RtEntry * pRt6Info, UINT4 u4CxtId)
{
    tRtm6RedTable      *pRtm6RedNode = NULL;
    tRtm6RedTable      *pTmpRtm6RedNode = NULL;
    tRtm6RedTable       Rtm6RedNode;
    UINT4               u4Result = 0;

    if (pRt6Info == NULL)
    {
        return;
    }
    if ((pRt6Info->i1Proto != IP6_NETMGMT_PROTOID) &&
        (pRt6Info->i1Proto != BGP6_ID) && (pRt6Info->i1Proto != RIPNG_ID))
    {
        return;
    }
    if ((RTM6_GET_NODE_STATUS () == RM_ACTIVE) &&
        ((gRtm6RedGlobalInfo.u1BulkUpdStatus == RTM6_HA_UPD_NOT_STARTED) ||
         (RTM6_IS_STANDBY_UP () == OSIX_FALSE)))
    {
        /* If bulk update is not started, then do not add the entry in the
         * RBtree as this will be taken care through bulk update */
        return;
    }

    /* Check whether the entry is already present in the RBtree, if present
     * just update the fields. If not present, allocate memory and add the 
     * entry to the RBtree */
    Rtm6RedNode.u4CxtId = u4CxtId;
    Rtm6RedNode.u4HwIntfId[0] = pRt6Info->u4HwIntfId[0];
    Rtm6RedNode.u4HwIntfId[1] = pRt6Info->u4HwIntfId[1];
    MEMCPY (&(Rtm6RedNode.DestPrefix), &(pRt6Info->dst), IPVX_IPV6_ADDR_LEN);
    MEMCPY (&(Rtm6RedNode.NextHopAddr), &(pRt6Info->nexthop),
            IPVX_IPV6_ADDR_LEN);
    Rtm6RedNode.u1PrefixLen = pRt6Info->u1Prefixlen;
    Rtm6RedNode.i1Proto = pRt6Info->i1Proto;

    pTmpRtm6RedNode = RBTreeGet (gRtm6GlobalInfo.Rtm6RedTable,
                                 (tRBElem *) & Rtm6RedNode);
    if (pTmpRtm6RedNode == NULL)
    {
        pRtm6RedNode =
            (tRtm6RedTable *) MemAllocMemBlk ((tMemPoolId)
                                              RTM6_DYN_MSG_POOL_ID);
        if (pRtm6RedNode == NULL)
        {
            return;
        }
        MEMSET (pRtm6RedNode, 0, sizeof (tRtm6RedTable));
        pRtm6RedNode->u4CxtId = u4CxtId;
        MEMCPY (&(pRtm6RedNode->DestPrefix), &(pRt6Info->dst),
                IPVX_IPV6_ADDR_LEN);
        pRtm6RedNode->u1PrefixLen = pRt6Info->u1Prefixlen;
        MEMCPY (&(pRtm6RedNode->NextHopAddr), &(pRt6Info->nexthop),
                IPVX_IPV6_ADDR_LEN);
        pRtm6RedNode->i1Proto = pRt6Info->i1Proto;
        pRtm6RedNode->u1HwStatus = pRt6Info->u1HwStatus;
        pRtm6RedNode->u4HwIntfId[0] = pRt6Info->u4HwIntfId[0];
        pRtm6RedNode->u4HwIntfId[1] = pRt6Info->u4HwIntfId[1];
        u4Result = RBTreeAdd (gRtm6GlobalInfo.Rtm6RedTable, pRtm6RedNode);
    }
    else
    {
        pRtm6RedNode = pTmpRtm6RedNode;
    }
    pRtm6RedNode->u1HwStatus = pRt6Info->u1HwStatus;

    UNUSED_PARAM (u4Result);
    return;
}

/************************************************************************/
/* Function Name      : Rtm6RedHwAudit                                   */
/*                                                                      */
/* Description        : This function does the hardware audit in two    */
/*                      approches.                                      */
/*                                                                      */
/*                      First:                                          */
/*                      When there is a transaction between standby and */
/*                      active node, the Rtm6RedTable is walked, if     */
/*                      there  are any entries in the table, they are   */
/*                      verified with the hardware, if the entry is     */
/*                      present in the hardware, then the entry is      */
/*                      added to the sofware. If not, the entry is      */
/*                      deleted.                                        */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/
VOID
Rtm6RedHwAudit ()
{
#ifdef NPAPI_WANTED
    tRtm6RedTable      *pRtm6RedInfo = NULL;
    tIp6RtEntry        *pRt6Info = NULL;
    tIp6RtEntry         Rt6Info;
    tRtm6Cxt           *pRtm6Cxt = NULL;
    VOID               *pRibNode = NULL;
    tNpRtm6Input        Rtm6NpInParam;
    tNpRtm6Output       Rtm6NpOutParam;
    tIp6RtEntry         Ip6RtEntry;
    tIp6Addr            NextHopAddr;
    tFsNpIntInfo        IntInfo;
    tFsNpRouteInfo      RouteInfo;
    UINT4               u4Index = 0;
    UINT4               u4NHType = 0;
    INT4                i4TrieVal = 0;

    MEMSET (&Rtm6NpInParam, 0, sizeof (tNpRtm6Input));
    MEMSET (&Rtm6NpOutParam, 0, sizeof (tNpRtm6Output));
    MEMSET (&Rt6Info, 0, sizeof (tIp6RtEntry));

    /* If the audit level is optimized(applicable only in async NP calls) the
     * temporary RBtree is scanned and the entries in the RBtree are checked
     * in the hardware. If present, then it is added in the software. If not
     * then the entry is ignored. In sync NP calls, there wont be any need to
     * do the hardware audit in this manner as the entries are added to the
     * standby database only after hardware call is success */

    pRtm6RedInfo = RBTreeGetFirst (gRtm6GlobalInfo.Rtm6RedTable);

    while (pRtm6RedInfo != NULL)
    {
        Rtm6NpInParam.u4CxtId = pRtm6RedInfo->u4CxtId;
        MEMCPY (&(Rtm6NpInParam.Ip6DestAddr), &(pRtm6RedInfo->DestPrefix),
                IPVX_IPV6_ADDR_LEN);
        MEMCPY (&(Rtm6NpInParam.Ip6NextHopAddr), &(pRtm6RedInfo->NextHopAddr),
                IPVX_IPV6_ADDR_LEN);
        pRtm6Cxt = UtilRtm6GetCxt (pRtm6RedInfo->u4CxtId);
        if (pRtm6Cxt == NULL)
        {
            RBTreeRem (gRtm6GlobalInfo.Rtm6RedTable, pRtm6RedInfo);
            MemReleaseMemBlock (RTM6_DYN_MSG_POOL_ID, (UINT1 *) pRtm6RedInfo);
            pRtm6RedInfo = RBTreeGetFirst (gRtm6GlobalInfo.Rtm6RedTable);
            continue;
        }
        MEMSET (&Ip6RtEntry, 0, sizeof (tIp6RtEntry));

        MEMCPY (Ip6RtEntry.dst.u1_addr, &(pRtm6RedInfo->DestPrefix),
                IPVX_IPV6_ADDR_LEN);
        MEMCPY (NextHopAddr.u1_addr, &(Rtm6NpOutParam.Ip6NextHopAddr),
                IPVX_IPV6_ADDR_LEN);
        Ip6RtEntry.u1Prefixlen = pRtm6RedInfo->u1PrefixLen;
        Ip6RtEntry.u4HwIntfId[0] = pRtm6RedInfo->u4HwIntfId[0];
        Ip6RtEntry.u4HwIntfId[1] = pRtm6RedInfo->u4HwIntfId[1];

        i4TrieVal = Rtm6TrieSearchExactEntryInCxt (pRtm6Cxt, &Ip6RtEntry,
                                                   &pRt6Info, &pRibNode);
        if (Ipv6FsNpIpv6UcGetRoute (Rtm6NpInParam, &Rtm6NpOutParam) ==
            FNP_SUCCESS)
        {
            if (i4TrieVal == IP6_SUCCESS)
            {
                while (pRt6Info != NULL)
                {
                    if (Ip6AddrCompare (pRt6Info->nexthop, NextHopAddr)
                        == IP6_ZERO)
                    {
                        pRt6Info->u1HwStatus = NP_PRESENT;
                        break;
                    }
                    pRt6Info = pRt6Info->pNextAlternatepath;
                }
            }
            else
            {
                /*Passing ECMP count to HW inorder to program ECMProutes */
                RouteInfo.u1RouteCount = pRtm6RedInfo->u1EcmpCount;
                RouteInfo.u4HwIntfId[0] = pRtm6RedInfo->u4HwIntfId[0];
                if (Ipv6FsNpIpv6UcRouteDelete (pRtm6Cxt->u4ContextId,
                                               (UINT1 *) &pRtm6RedInfo->
                                               DestPrefix,
                                               pRtm6RedInfo->u1PrefixLen,
                                               (UINT1 *) &pRtm6RedInfo->
                                               NextHopAddr,
                                               u4Index,
                                               &RouteInfo) == FNP_FAILURE)
                {

                    RTM6_TRC (pRtm6Cxt->u4ContextId, RTM6_ALL_FAILURE_TRC |
                              RTM6_OS_RESOURCE_TRC, RTM6_MOD_NAME,
                              "\tNP Route Deletion Failed \n");
                }
            }
        }
        else
        {
            if (i4TrieVal == IP6_SUCCESS)
            {
                MEMSET (&IntInfo, 0, sizeof (tFsNpIntInfo));
                IntInfo.u4PhyIfIndex = pRt6Info->u4Index;
                IntInfo.u1IfType = Ip6GetIfType (pRt6Info->u4Index);
                if ((IntInfo.u1IfType == CFA_L3IPVLAN)
                    || (IntInfo.u1IfType == CFA_L3SUB_INTF))
                {
                    IntInfo.u4PhyIfIndex = pRt6Info->u4Index;
                }
                if (IntInfo.u1IfType == CFA_PSEUDO_WIRE)
                {
                    IntInfo.u4PhyIfIndex = pRt6Info->u4Index;
                }

                if (IntInfo.u1IfType != IP6_ENET_INTERFACE_TYPE)
                {
                    if (CfaGetVlanId (IntInfo.u4PhyIfIndex, &(IntInfo.u2VlanId))
                        != CFA_SUCCESS)
                    {
                        RBTreeRem (gRtm6GlobalInfo.Rtm6RedTable, pRtm6RedInfo);
                        MemReleaseMemBlock (RTM6_DYN_MSG_POOL_ID,
                                            (UINT1 *) pRtm6RedInfo);
                        pRtm6RedInfo =
                            RBTreeGetFirst (gRtm6GlobalInfo.Rtm6RedTable);
                        continue;

                    }
                }
                else
                {
                    IntInfo.u4PhyIfIndex = pRt6Info->u4Index;
                    IntInfo.u2VlanId = 0;
                }
                /*checking Reachability and Passing ECMP count to HW inorder to program ECMProutes */
                Rtm6SetReachState (pRtm6Cxt->u4ContextId, pRt6Info);
                Rtm6GetInstalledRouteCount (pRt6Info, &pRt6Info->u1EcmpCount);

                IntInfo.u1RouteCount = pRt6Info->u1EcmpCount;
                if (Ipv6FsNpIpv6UcRouteAdd (pRtm6Cxt->u4ContextId,
                                            (UINT1 *) &pRtm6RedInfo->DestPrefix,
                                            pRtm6RedInfo->u1PrefixLen,
                                            (UINT1 *) &pRtm6RedInfo->
                                            NextHopAddr, u4NHType,
                                            &IntInfo) == FNP_FAILURE)
                {
                    RTM6_TRC (pRtm6Cxt->u4ContextId, RTM6_ALL_FAILURE_TRC |
                              RTM6_OS_RESOURCE_TRC, RTM6_MOD_NAME,
                              "\tNP Route Addition Failed \n");
                    if (Rtm6FrtAddInfo
                        (pRt6Info, pRtm6Cxt->u4ContextId,
                         (UINT1) u4NHType) == RTM6_SUCCESS)
                    {
                        Rtm6RedSyncFrtInfo (pRt6Info, pRtm6Cxt->u4ContextId,
                                            (UINT1) u4NHType, RTM6_ADD_ROUTE);

                    }
                    else
                    {
                        RTM6_TRC_ARG2 (pRtm6Cxt->u4ContextId,
                                       RTM6_ALL_FAILURE_TRC |
                                       RTM6_OS_RESOURCE_TRC, RTM6_MOD_NAME,
                                       "ERROR[RTM]: IPV6  Failed Route Addition Failed%s/%d   \n",
                                       Ip6PrintNtop (&pRt6Info->dst),
                                       (INT4) pRt6Info->u1Prefixlen);
                    }
                }

                pRt6Info->u4HwIntfId[0] = IntInfo.u4HwIntfId[0];

            }
        }
        RBTreeRem (gRtm6GlobalInfo.Rtm6RedTable, pRtm6RedInfo);
        MemReleaseMemBlock (RTM6_DYN_MSG_POOL_ID, (UINT1 *) pRtm6RedInfo);
        pRtm6RedInfo = RBTreeGetFirst (gRtm6GlobalInfo.Rtm6RedTable);
    }

#endif
    return;
}

/*-------------------------------------------------------------------+
 * Function           : Rtm6RBTreeRedEntryCmp
 *
 * Input(s)           : pRBElem, pRBElemIn
 *
 * Output(s)          : None.
 *
 * Returns            : -1  if pRBElem <  pRBElemIn
 *                      1   if pRBElem >  pRBElemIn
 *                      0   if pRBElem == pRBElemIn
 * Action :
 * This procedure compares the two Redundancy entries in lexicographic
 * order of the indices of the Redundancy entry.
+-------------------------------------------------------------------*/

INT4
Rtm6RBTreeRedEntryCmp (tRBElem * pRBElem, tRBElem * pRBElemIn)
{
    tRtm6RedTable      *pRtm6Red = pRBElem;
    tRtm6RedTable      *pRtm6RedIn = pRBElemIn;
    INT1                i4RetVal;

    if (pRtm6Red->u4CxtId < pRtm6RedIn->u4CxtId)
    {
        return IP6_RB_LESSER;
    }
    else if (pRtm6Red->u4CxtId > pRtm6RedIn->u4CxtId)
    {
        return IP6_RB_GREATER;
    }

    i4RetVal = Ip6AddrCompare (pRtm6Red->DestPrefix, pRtm6RedIn->DestPrefix);

    if (i4RetVal == IP6_MINUS_ONE)
    {
        return IP6_RB_LESSER;
    }
    else if (i4RetVal == IP6_ONE)
    {
        return IP6_RB_GREATER;
    }

    if (pRtm6Red->u1PrefixLen < pRtm6RedIn->u1PrefixLen)
    {
        return IP6_RB_LESSER;
    }
    else if (pRtm6Red->u1PrefixLen > pRtm6RedIn->u1PrefixLen)
    {
        return IP6_RB_GREATER;
    }

    i4RetVal = Ip6AddrCompare (pRtm6Red->NextHopAddr, pRtm6RedIn->NextHopAddr);

    if (i4RetVal == IP6_MINUS_ONE)
    {
        return IP6_RB_LESSER;
    }
    else if (i4RetVal == IP6_ONE)
    {
        return IP6_RB_GREATER;
    }

    if (pRtm6Red->i1Proto < pRtm6RedIn->i1Proto)
    {
        return IP6_RB_LESSER;
    }
    else if (pRtm6Red->i1Proto > pRtm6RedIn->i1Proto)
    {
        return IP6_RB_GREATER;
    }

    return IP6_RB_EQUAL;
}

/*-------------------------------------------------------------------+
 * Function           : Rtm6RedUpdateRtParams
 *
 * Input(s)           : pRt6Info, u4Context
 *
 * Output(s)          : Updated pRt6Info
 *
 * Returns            : RTM_SUCCESS/RTM_FAILURE
 *
 * Action :
 * This procedure searches for the route node in the temporary RTM RED
 * table and if a node is found, the route's flag information is updated
 * and the node is removed from RED table.
+-------------------------------------------------------------------*/
INT4
Rtm6RedUpdateRtParams (tIp6RtEntry * pRt6Info, UINT4 u4Context)
{
    tRtm6RedTable       Rtm6RedNode;
    tRtm6RedTable      *pRtm6RedNode = NULL;

    Rtm6RedNode.u4CxtId = u4Context;
    MEMCPY (&(Rtm6RedNode.DestPrefix), &(pRt6Info->dst), IPVX_IPV6_ADDR_LEN);
    MEMCPY (&(Rtm6RedNode.NextHopAddr), &(pRt6Info->nexthop),
            IPVX_IPV6_ADDR_LEN);
    Rtm6RedNode.u1PrefixLen = pRt6Info->u1Prefixlen;
    Rtm6RedNode.i1Proto = pRt6Info->i1Proto;
    Rtm6RedNode.u4HwIntfId[0] = pRt6Info->u4HwIntfId[0];
    Rtm6RedNode.u4HwIntfId[1] = pRt6Info->u4HwIntfId[1];
    pRtm6RedNode = RBTreeGet (gRtm6GlobalInfo.Rtm6RedTable,
                              (tRBElem *) & Rtm6RedNode);
    if (pRtm6RedNode != NULL)
    {
        RBTreeRem (gRtm6GlobalInfo.Rtm6RedTable, pRtm6RedNode);
        pRt6Info->u1HwStatus = pRtm6RedNode->u1HwStatus;
        MemReleaseMemBlock (RTM6_DYN_MSG_POOL_ID, (UINT1 *) pRtm6RedNode);
        return RTM6_SUCCESS;
    }
    return RTM6_FAILURE;
}

/*****************************************************************************/
/* Function Name      : Rtm6RedHandleDynSyncAudit                             */
/*                                                                           */
/* Description        : This function Handles the Dynamic Sync-up Audit      */
/*                      event. This event is used to start the audit         */
/*                      between the active and standby units for the         */
/*                      dynamic sync-up happened                             */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
Rtm6RedHandleDynSyncAudit (VOID)
{
/*On receiving this event, RTM should execute show cmd and calculate checksum*/
    Rtm6ExecuteCmdAndCalculateChkSum ();
}

/*****************************************************************************/
/* Function Name      : Rtm6ExecuteCmdAndCalculateChkSum                     */
/*                                                                           */
/* Description        : This function Handles the execution of show commands */
/*                      and calculation of checksum with the output.         */
/*                      The calculated value is then being sent to RM.       */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
Rtm6ExecuteCmdAndCalculateChkSum (VOID)
{
    /*Execute CLI commands and calculate checksum */
    UINT2               u2AppId = RM_RTM6_APP_ID;
    UINT2               u2ChkSum = 0;

    RTM6_TASK_UNLOCK ();
    if (Rtm6GetShowCmdOutputAndCalcChkSum (&u2ChkSum) == RTM6_FAILURE)
    {
        RTM6_TRC (RTM6_DEFAULT_CXT_ID, RTM6_ALL_FAILURE_TRC,
                  RTM6_MOD_NAME, "\tChecksum of calculation failed for RTM6\n");
        RTM6_TASK_LOCK ();
        return;
    }
#ifdef L2RED_WANTED
    if (Rtm6RmEnqChkSumMsgToRm (u2AppId, u2ChkSum) == RTM6_FAILURE)
    {
        RTM6_TRC (RTM6_DEFAULT_CXT_ID, RTM6_ALL_FAILURE_TRC,
                  RTM6_MOD_NAME, "\tSending checkum to RM failed\n");
        RTM6_TASK_LOCK ();
        return;
    }
#else
    UNUSED_PARAM (u2AppId);
#endif
    RTM6_TASK_LOCK ();
    return;
}

/************************************************************************/
/* Function Name      : Rtm6RedSendFrtBulkUpdMsg                         */
/*                                                                      */
/* Description        : This function sends the binding table dynamic   */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
Rtm6RedSendFrtBulkUpdMsg (VOID)
{
    UINT1               u1BulkUpdPendFlg = OSIX_FALSE;

    if (RTM6_IS_STANDBY_UP () == OSIX_FALSE)
    {
        return;
    }

    if (gRtm6RedGlobalInfo.u1FrtBulkUpdStatus == RTM6_HA_UPD_NOT_STARTED)
    {
        /* If bulk update is not started, reset the marker and set the status
         * to in progress */
        gRtm6RedGlobalInfo.u1FrtBulkUpdStatus = RTM6_HA_UPD_IN_PROGRESS;
        MEMSET (&gRtm6RedGlobalInfo.Rtm6HARedFrtMarker, 0,
                sizeof (tRtm6HARedFrtMarker));
    }
    if (gRtm6RedGlobalInfo.u1FrtBulkUpdStatus == RTM6_HA_UPD_IN_PROGRESS)
    {
        /* If bulk update flag is in progress, sent the bulk Info */
        if (Rtm6RedSendFrtBulkInfo (&u1BulkUpdPendFlg) != RTM6_SUCCESS)
        {
            /* If there is any error in sending bulk update, set the 
             * bulk update flag to aborted */
            gRtm6RedGlobalInfo.u1FrtBulkUpdStatus = RTM6_HA_UPD_ABORTED;
        }
        else
        {
            if (u1BulkUpdPendFlg == OSIX_TRUE)
            {
                /* Send an event to the RTM main task indicating that there
                 * are pending entries in the RTM database to be synced */

                OsixSendEvent (SELF, (const UINT1 *) RTM6_MAIN_TASK_NAME,
                               RTM6_HA_FRT_PEND_BLKUPD_EVENT);
            }
            else
            {
                /* Send the tail msg to indicate the completion of Bulk
                 * update process.
                 */
                gRtm6RedGlobalInfo.u1BulkUpdStatus = RTM6_HA_UPD_COMPLETED;
                MEMSET (&gRtm6RedGlobalInfo.Rtm6HARedFrtMarker, 0,
                        sizeof (tRtm6HARedFrtMarker));
                RmSetBulkUpdatesStatus (RM_RTM6_APP_ID);

                Rtm6RedSendBulkUpdTailMsg ();
            }
        }
    }

    return;

}

/***********************************************************************
 * Function Name      : Rtm6RedSendFrtBulkInfo                         
 *                                                                      
 * Description        : This function sends the FRT Bulk update messages to 
 *                      the peer standby RTM.                           
 *                                                                      
 * Input(s)           : None                                            
 *                                                                      
 * Output(s)          : pu1BulkUpdPendFlg                               
 *                                                                      
 * Returns            : RTM_SUCCESS/RTM_FAILURE                         
 ***********************************************************************/

PUBLIC INT4
Rtm6RedSendFrtBulkInfo (UINT1 *pu1BulkUpdPendFlg)
{

    tRmMsg             *pMsg = NULL;
    tRtm6HARedFrtMarker *pRtm6RedFrtMarker = NULL;
    tRtm6FrtInfo       *pPrevRtm6FrtInfo = NULL;
    tRtm6FrtInfo        Rtm6FrtInfo;
    tRtm6FrtInfo       *pNextRtm6FrtInfo = NULL;
    tRmProtoEvt         ProtoEvt;
    UINT2               u2MsgPacked = 0;
    UINT2               u2MsgLen = 0;
    UINT2               u2OffSet = 0;
    UINT4               u4OffSet = 0;
    UINT4               u4LenOffSet = 0;
    UINT4               u4NoOffset = 0;

    ProtoEvt.u4AppId = RM_RTM6_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    /*
     *    RTM6 Bulk Update message
     *
     *    <- 1 byte ->|<- 2 B->|<- 2 B ->|<- 4 B ->|<- 4 B ->|<- 4 B ->|    
     *    ---------------------------------------------------------------    
     *    | Msg. Type | Length | No of   | Dest    | Mask    | NextHop |    
     *    |           |        | Entries | Network | Length  | Address |    
     *    ---------------------------------------------------------------    
     *
     *    |<-2 B->|<-1 B->|<- 1 B ->|<- 1 B ->|
     *    ----------------------------------------------
     *    | Route | Route |  NH     | Route  |
     *    | Proto | Flag  |  Type    | Count  |
     *    ---------------------------------------------
     */

    if ((pMsg = RM_ALLOC_TX_BUF (RTM6_RED_MAX_MSG_SIZE)) == NULL)
    {
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
        return RTM6_FAILURE;
    }

    pRtm6RedFrtMarker = &(gRtm6RedGlobalInfo.Rtm6HARedFrtMarker);

    RTM6_RM_PUT_1_BYTE (pMsg, &u4OffSet, RTM6_RED_FRT_BULK_INFO);

    /* INITIALLY SET THE LENGTH TO MAXIMUM LENGTH */
    u4LenOffSet = u4OffSet;
    RTM6_RM_PUT_2_BYTE (pMsg, &u4OffSet, u2MsgLen);
    u4NoOffset = u4OffSet;
    RTM6_RM_PUT_2_BYTE (pMsg, &u4OffSet, u2MsgPacked);
    MEMSET (&Rtm6FrtInfo, 0, sizeof (tRtm6FrtInfo));
    Rtm6FrtInfo.u4CxtId = pRtm6RedFrtMarker->u4CxtId;
    MEMCPY (&(Rtm6FrtInfo.DestPrefix), &(pRtm6RedFrtMarker->DestPrefix),
            IPVX_IPV6_ADDR_LEN);
    MEMCPY (&(Rtm6FrtInfo.NextHopAddr), &(pRtm6RedFrtMarker->NextHopAddr),
            IPVX_IPV6_ADDR_LEN);
    Rtm6FrtInfo.u1PrefixLen = pRtm6RedFrtMarker->u1PrefixLen;
    Rtm6FrtInfo.i1Proto = pRtm6RedFrtMarker->i1Proto;
    pNextRtm6FrtInfo = Rtm6GetNextFrtEntry (&Rtm6FrtInfo);
    while (pNextRtm6FrtInfo != NULL)
    {
        if ((u4OffSet + RTM6_RED_FRT_INFO_SIZE) > RTM6_RED_MAX_MSG_SIZE)
        {
            /* If message size is greater than MAX_SIZE, set the pending
             * flag as true and break */
            *pu1BulkUpdPendFlg = OSIX_TRUE;
            break;
        }

        if ((pNextRtm6FrtInfo->i1Proto == OSPF6_ID) ||
            (pNextRtm6FrtInfo->i1Proto == IP6_NETMGMT_PROTOID) ||
            (pNextRtm6FrtInfo->i1Proto == BGP6_ID) ||
            (pNextRtm6FrtInfo->i1Proto == RIPNG_ID))
        {
            RTM6_RM_PUT_4_BYTE (pMsg, &u4OffSet, pNextRtm6FrtInfo->u4CxtId);
            RTM6_RM_PUT_N_BYTE (pMsg, &u4OffSet,
                                (pNextRtm6FrtInfo->DestPrefix.u1_addr),
                                IPVX_IPV6_ADDR_LEN);
            RTM6_RM_PUT_N_BYTE (pMsg, &u4OffSet,
                                (pNextRtm6FrtInfo->NextHopAddr.u1_addr),
                                IPVX_IPV6_ADDR_LEN);
            RTM6_RM_PUT_1_BYTE (pMsg, &u4OffSet, pNextRtm6FrtInfo->u1PrefixLen);
            RTM6_RM_PUT_1_BYTE (pMsg, &u4OffSet, pNextRtm6FrtInfo->i1Proto);
            RTM6_RM_PUT_1_BYTE (pMsg, &u4OffSet, pNextRtm6FrtInfo->u1Type);
            RTM6_RM_PUT_1_BYTE (pMsg, &u4OffSet, pNextRtm6FrtInfo->u1RtCount);
            pPrevRtm6FrtInfo = pNextRtm6FrtInfo;
            u2MsgPacked = (UINT2) (u2MsgPacked + 1);
        }

        MEMSET (&Rtm6FrtInfo, 0, sizeof (tRtm6FrtInfo));
        Rtm6FrtInfo.u4CxtId = pNextRtm6FrtInfo->u4CxtId;
        MEMCPY (&(Rtm6FrtInfo.DestPrefix), &(pNextRtm6FrtInfo->DestPrefix),
                IPVX_IPV6_ADDR_LEN);
        MEMCPY (&(Rtm6FrtInfo.NextHopAddr), &(pNextRtm6FrtInfo->NextHopAddr),
                IPVX_IPV6_ADDR_LEN);
        Rtm6FrtInfo.u1PrefixLen = pNextRtm6FrtInfo->u1PrefixLen;
        Rtm6FrtInfo.i1Proto = pNextRtm6FrtInfo->i1Proto;
        pNextRtm6FrtInfo = Rtm6GetNextFrtEntry (&Rtm6FrtInfo);
    }
    u2OffSet = (UINT2) u4OffSet;
    RTM6_RM_PUT_2_BYTE (pMsg, &u4LenOffSet, u2OffSet);
    RTM6_RM_PUT_2_BYTE (pMsg, &u4NoOffset, u2MsgPacked);

    if (u2MsgPacked == 0)
    {
        /* If message packet is 0, then do not send the message */
        RM_FREE (pMsg);
        return RTM6_SUCCESS;
    }

    if (Rtm6RedSendMsgToRm (pMsg, u2OffSet) != OSIX_SUCCESS)
    {
        *pu1BulkUpdPendFlg = OSIX_FALSE;
        ProtoEvt.u4Error = RM_SENDTO_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
        return RTM6_FAILURE;
    }

    pRtm6RedFrtMarker->u4CxtId = pPrevRtm6FrtInfo->u4CxtId;
    MEMCPY (&(pRtm6RedFrtMarker->DestPrefix), &(pPrevRtm6FrtInfo->DestPrefix),
            IPVX_IPV6_ADDR_LEN);
    MEMCPY (&(pRtm6RedFrtMarker->NextHopAddr), &(pPrevRtm6FrtInfo->NextHopAddr),
            IPVX_IPV6_ADDR_LEN);
    pRtm6RedFrtMarker->u1PrefixLen = pPrevRtm6FrtInfo->u1PrefixLen;
    pRtm6RedFrtMarker->i1Proto = pPrevRtm6FrtInfo->i1Proto;
    /* RtmRedMarker is set to the last updated entry */
    return RTM6_SUCCESS;
}

/************************************************************************
 * Function Name      : Rtm6RedSyncFrtInfo 
 *                                                
 * Description        : This function sends the binding table dynamic  
 *                      sync up message to Standby node from the Active 
 *                      node, when the RTM offers an IP address 
 *                      to the RTM client and dynamically update the   
 *                      binding table entries.                        
 *                                                                   
 * Input(s)           : None
 *                                                                    
 * Output(s)          : None                                         
 *                                                                  
 * Returns            : OSIX_SUCCESS / OSIX_FAILURE                
 ************************************************************************/

PUBLIC VOID
Rtm6RedSyncFrtInfo (tIp6RtEntry * pIp6RtEntry, UINT4 u4CxtId, UINT1 u1Type,
                    UINT1 u1Flag)
{
    tRmProtoEvt         ProtoEvt;
    tRmMsg             *pMsg = NULL;
    UINT4               u4OffSet = 0;
    UINT4               u4MsgAllocSize = 0;
    UINT4               u4LenOffSet = 0;
    UINT4               u4NoOffset = 0;
    UINT2               u2MsgPacked = 0;
    UINT2               u2OffSet = 0;
    UINT2               u2MsgLen = 0;

    ProtoEvt.u4AppId = RM_RTM6_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    if (RTM6_GET_NODE_STATUS () != RM_ACTIVE)
    {
        return;
    }
    u4MsgAllocSize = RTM6_RED_FRT_INFO_SIZE;

    /* Message is allcoated upto required size. The number of entries in the
     * RBTree is got and then it is multiplied with the size of a single
     * dynamic update to get the total count. If that length is greater than
     * MAX size, then the message is allocated upto max size */

    /*
     *    RTM Dynamic Update message
     *
     *    <- 1 byte ->|<- 2 B->|<- 2 B ->|<- 4 B ->|<- 4 B ->|<- 4 B ->|
     *    ---------------------------------------------------------------
     *    | Msg. Type | Length | No of   | Dest    | Mask    | NextHop |
     *    |           |        | Entries | Network | Length  | Address |
     *    ---------------------------------------------------------------
     *
     *    |<-2 B->|<-1 B->|<-4 B ->|<- 1 B ->|<- 1 B ->
     *    --------------------------------------------
     *    | Route | Route | Metric | Bit     | Route  |
     *    | Proto | Flag  | Type   | Mask    | Count  |
     *    --------------------------------------------
     */
    u4MsgAllocSize = u4MsgAllocSize + 9 + 1;
    if ((pMsg = RM_ALLOC_TX_BUF (u4MsgAllocSize)) == NULL)
    {
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);

        return;
    }
    else
    {
        u2OffSet = 0;

        RTM6_RM_PUT_1_BYTE (pMsg, &u4OffSet, RTM6_RED_SYNC_FRT_INFO);
        u4LenOffSet = u4OffSet;
        RTM6_RM_PUT_2_BYTE (pMsg, &u4OffSet, u2MsgLen);
        u4NoOffset = u4OffSet;
        RTM6_RM_PUT_2_BYTE (pMsg, &u4OffSet, u2MsgPacked);

        RTM6_RM_PUT_4_BYTE (pMsg, &u4OffSet, u4CxtId);
        RTM6_RM_PUT_N_BYTE (pMsg, &u4OffSet, (pIp6RtEntry->dst.u1_addr),
                            IPVX_IPV6_ADDR_LEN);
        RTM6_RM_PUT_N_BYTE (pMsg, &u4OffSet, (pIp6RtEntry->nexthop.u1_addr),
                            IPVX_IPV6_ADDR_LEN);
        RTM6_RM_PUT_1_BYTE (pMsg, &u4OffSet, pIp6RtEntry->u1Prefixlen);
        RTM6_RM_PUT_1_BYTE (pMsg, &u4OffSet, pIp6RtEntry->i1Proto);
        RTM6_RM_PUT_1_BYTE (pMsg, &u4OffSet, u1Type);
        RTM6_RM_PUT_1_BYTE (pMsg, &u4OffSet, pIp6RtEntry->u1EcmpCount);
        RTM6_RM_PUT_1_BYTE (pMsg, &u4OffSet, u1Flag);
        u2MsgPacked = (UINT2) (u2MsgPacked + 1);
        u2OffSet = (UINT2) u4OffSet;
        RTM6_RM_PUT_2_BYTE (pMsg, &u4LenOffSet, u2OffSet);
        RTM6_RM_PUT_2_BYTE (pMsg, &u4NoOffset, u2MsgPacked);

        if (u2MsgPacked == 0)
        {
            /* If there are no new items to sent, then it the message is not sent */
            RM_FREE (pMsg);
            return;
        }

        if (Rtm6RedSendMsgToRm (pMsg, u2OffSet) != OSIX_SUCCESS)
        {
            ProtoEvt.u4Error = RM_SENDTO_FAIL;
            RmApiHandleProtocolEvent (&ProtoEvt);
            return;
        }

    }
    return;
}

/************************************************************************/
/* Function Name      : Rtm6RedProcessFrtBulkInfo                       */
/*                                                                      */
/* Description        : This function decodes the sync-up message from  */
/*                      Active node and updates the binding information */
/*                      at the Standby node.                            */
/*                                                                      */
/* Input(s)           : pMsg - RM Sync-up message                       */
/*                      u2RemMsgLen - Length of value of Message        */
/*                                                                      */
/* Output(s)          : u2OffSet - OffSet value                         */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

VOID
Rtm6RedProcessFrtBulkInfo (tRmMsg * pMsg, UINT2 *pu2OffSet)
{
    tRtm6FrtInfo        Rtm6FrtNode;
    tRtm6FrtInfo       *pRtm6FrtNode = NULL;
    tIp6RtEntry        *pRt6Info = NULL;
    tRtm6Cxt           *pRtm6Cxt = NULL;
    VOID               *pRibNode = NULL;
    tIp6RtEntry         Ip6RtEntry;
    tIp6Addr            NextHopAddr;
    UINT4               u4CxtId = 0;
    UINT2               u2NoOfEntries = 0;
    UINT1               u1NHType = 0;
    INT1                i1Proto = 0;
    UINT4               u4TempOffSet = (UINT4) *pu2OffSet;

    MEMSET (&NextHopAddr, 0, sizeof (tIp6Addr));

    RTM6_RM_GET_2_BYTE (pMsg, &u4TempOffSet, u2NoOfEntries);
    while (u2NoOfEntries > 0)
    {
        /* The number of entries is got from the message and it is processed */
        MEMSET (&Ip6RtEntry, 0, sizeof (tIp6RtEntry));
        u2NoOfEntries--;
        RTM6_RM_GET_4_BYTE (pMsg, &u4TempOffSet, u4CxtId);
        RTM6_RM_GET_N_BYTE (pMsg, &u4TempOffSet, (Ip6RtEntry.dst.u1_addr),
                            IPVX_IPV6_ADDR_LEN);
        RTM6_RM_GET_N_BYTE (pMsg, &u4TempOffSet, NextHopAddr.u1_addr,
                            IPVX_IPV6_ADDR_LEN);
        RTM6_RM_GET_1_BYTE (pMsg, &u4TempOffSet, Ip6RtEntry.u1Prefixlen);
        RTM6_RM_GET_1_BYTE (pMsg, &u4TempOffSet, i1Proto);
        RTM6_RM_GET_1_BYTE (pMsg, &u4TempOffSet, u1NHType);
        RTM6_RM_GET_1_BYTE (pMsg, &u4TempOffSet, Ip6RtEntry.u1EcmpCount);
        pRtm6Cxt = UtilRtm6GetCxt (u4CxtId);
        if (pRtm6Cxt == NULL)
        {
            RM_FREE (pMsg);
            return;
        }

        if (Rtm6TrieSearchExactEntryInCxt (pRtm6Cxt, &Ip6RtEntry,
                                           &pRt6Info, &pRibNode) == IP6_SUCCESS)
        {

            while (pRt6Info != NULL)
            {
                if (Ip6AddrCompare (pRt6Info->nexthop, NextHopAddr) == IP6_ZERO)
                {
                    break;
                }
                pRt6Info = pRt6Info->pNextAlternatepath;
            }
        }

        if (pRt6Info != NULL)
        {
            if (pRt6Info->u1HwStatus == NP_PRESENT)
            {
                MEMSET (&Rtm6FrtNode, 0, sizeof (tRtm6FrtInfo));
                Rtm6FrtNode.u4CxtId = u4CxtId;
                MEMCPY (&(Rtm6FrtNode.DestPrefix), &(pRt6Info->dst),
                        IPVX_IPV6_ADDR_LEN);
                MEMCPY (&(Rtm6FrtNode.NextHopAddr), &(pRt6Info->nexthop),
                        IPVX_IPV6_ADDR_LEN);
                Rtm6FrtNode.u1PrefixLen = pRt6Info->u1Prefixlen;
                Rtm6FrtNode.i1Proto = pRt6Info->i1Proto;
                pRtm6FrtNode = RBTreeGet (gRtm6GlobalInfo.Rtm6FrtTable,
                                          (tRBElem *) & Rtm6FrtNode);
                /* If the hardware status is NP_PRESENT, then the corresponding
                 * entry is deleted from the temporary RBTree and added to the
                 * software. If the hardware status is NP_NOT_PRESENT, then the
                 * corresponding entry is added to the temporary data 
                 * structure */

                if (pRtm6FrtNode != NULL)
                {
                    RBTreeRem (gRtm6GlobalInfo.Rtm6FrtTable, pRtm6FrtNode);
                    MemReleaseMemBlock (RTM6_FRT_MSG_POOL_ID,
                                        (VOID *) pRtm6FrtNode);
                }
            }
            else
            {
                Ip6RtEntry.i1Proto = i1Proto;
                MEMCPY (Ip6RtEntry.nexthop.u1_addr, NextHopAddr.u1_addr,
                        IPVX_IPV6_ADDR_LEN);
                if (Rtm6FrtAddInfo (&Ip6RtEntry, u4CxtId, u1NHType) ==
                    RTM6_SUCCESS)
                {
                    Rtm6RedSyncFrtInfo (&Ip6RtEntry, pRtm6Cxt->u4ContextId,
                                        u1NHType, RTM6_ADD_ROUTE);

                }
                else
                {
                    RTM6_TRC_ARG2 (pRtm6Cxt->u4ContextId, RTM6_ALL_FAILURE_TRC |
                                   RTM6_OS_RESOURCE_TRC, RTM6_MOD_NAME,
                                   "ERROR[RTM]: IPV6  Failed Route Addition Failed%s/%d   \n",
                                   Ip6PrintNtop (&Ip6RtEntry.dst),
                                   (INT4) Ip6RtEntry.u1Prefixlen);
                }
            }
        }
    }
    *pu2OffSet = (UINT2) u4TempOffSet;
    return;
}

/************************************************************************/
/* Function Name      : Rtm6RedProcessSyncFrtInfo                        */
/*                                                                      */
/* Description        : This function decodes the sync-up message from  */
/*                      Active node and updates the binding information */
/*                      at the Standby node.                            */
/*                                                                      */
/* Input(s)           : pMsg - RM Sync-up message                       */
/*                      u2RemMsgLen - Length of value of Message        */
/*                                                                      */
/* Output(s)          : u2OffSet - OffSet value                         */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

VOID
Rtm6RedProcessSyncFrtInfo (tRmMsg * pMsg, UINT2 *pu2OffSet)
{
    tRtm6FrtInfo        Rtm6FrtNode;
    tRtm6FrtInfo       *pRtm6FrtNode = NULL;
    tIp6RtEntry        *pRt6Info = NULL;
    tRtm6Cxt           *pRtm6Cxt = NULL;
    VOID               *pRibNode = NULL;
    tIp6RtEntry         Ip6RtEntry;
    tIp6Addr            NextHopAddr;
    UINT4               u4CxtId = 0;
    UINT1               u1NHType = 0;
    UINT1               u1Flag = 0;
    UINT2               u2NoOfEntries = 0;
    INT1                i1Proto = 0;
    UINT4               u4TempOffSet = (UINT4) *pu2OffSet;

    MEMSET (&NextHopAddr, 0, sizeof (tIp6Addr));
    MEMSET (&Rtm6FrtNode, 0, sizeof (tRtm6FrtInfo));
    MEMSET (&Ip6RtEntry, 0, sizeof (tIp6RtEntry));
    RTM6_RM_GET_2_BYTE (pMsg, &u4TempOffSet, u2NoOfEntries);
    RTM6_RM_GET_4_BYTE (pMsg, &u4TempOffSet, u4CxtId);
    RTM6_RM_GET_N_BYTE (pMsg, &u4TempOffSet, (Ip6RtEntry.dst.u1_addr),
                        IPVX_IPV6_ADDR_LEN);
    RTM6_RM_GET_N_BYTE (pMsg, &u4TempOffSet, NextHopAddr.u1_addr,
                        IPVX_IPV6_ADDR_LEN);
    RTM6_RM_GET_1_BYTE (pMsg, &u4TempOffSet, Ip6RtEntry.u1Prefixlen);
    RTM6_RM_GET_1_BYTE (pMsg, &u4TempOffSet, i1Proto);
    RTM6_RM_GET_1_BYTE (pMsg, &u4TempOffSet, u1NHType);
    RTM6_RM_GET_1_BYTE (pMsg, &u4TempOffSet, Ip6RtEntry.u1EcmpCount);
    RTM6_RM_GET_1_BYTE (pMsg, &u4TempOffSet, u1Flag);
    pRtm6Cxt = UtilRtm6GetCxt (u4CxtId);
    if (pRtm6Cxt == NULL)
    {
        RM_FREE (pMsg);
        return;
    }
    if (Rtm6TrieSearchExactEntryInCxt (pRtm6Cxt, &Ip6RtEntry,
                                       &pRt6Info, &pRibNode) == IP6_SUCCESS)
    {

        while (pRt6Info != NULL)
        {
            if (Ip6AddrCompare (pRt6Info->nexthop, NextHopAddr) == IP6_ZERO)
            {
                break;
            }
            pRt6Info = pRt6Info->pNextAlternatepath;
        }
    }

    Ip6RtEntry.i1Proto = i1Proto;
    MEMCPY (Ip6RtEntry.nexthop.u1_addr, NextHopAddr.u1_addr,
            IPVX_IPV6_ADDR_LEN);
    if (u1Flag == RTM6_ADD_ROUTE)
    {
        if (pRt6Info != NULL)
        {
            if (Rtm6FrtAddInfo (&Ip6RtEntry, u4CxtId, u1NHType) != RTM6_SUCCESS)
            {
                RTM6_TRC_ARG2 (pRtm6Cxt->u4ContextId, RTM6_ALL_FAILURE_TRC |
                               RTM6_OS_RESOURCE_TRC, RTM6_MOD_NAME,
                               "ERROR[RTM]: IPV6  Failed Route Addition Failed%s/%d   \n",
                               Ip6PrintNtop (&Ip6RtEntry.dst),
                               (INT4) Ip6RtEntry.u1Prefixlen);
            }
        }
    }
    else
    {
        Rtm6FrtNode.u4CxtId = u4CxtId;
        if (pRt6Info != NULL)
        {
            MEMCPY (&(Rtm6FrtNode.DestPrefix), &(pRt6Info->dst),
                    IPVX_IPV6_ADDR_LEN);
            MEMCPY (&(Rtm6FrtNode.NextHopAddr), &(pRt6Info->nexthop),
                    IPVX_IPV6_ADDR_LEN);
            Rtm6FrtNode.u1PrefixLen = pRt6Info->u1Prefixlen;
            Rtm6FrtNode.i1Proto = pRt6Info->i1Proto;
            pRtm6FrtNode = RBTreeGet (gRtm6GlobalInfo.Rtm6FrtTable,
                                      (tRBElem *) & Rtm6FrtNode);
            /* If the hardware status is NP_PRESENT, then the corresponding
             * entry is deleted from the temporary RBTree and added to the
             * software. If the hardware status is NP_NOT_PRESENT, then the
             * corresponding entry is added to the temporary data 
             * structure */

            if (pRtm6FrtNode != NULL)
            {
                RBTreeRem (gRtm6GlobalInfo.Rtm6FrtTable, pRtm6FrtNode);
                MemReleaseMemBlock (RTM6_FRT_MSG_POOL_ID,
                                    (VOID *) pRtm6FrtNode);
            }
        }
    }

    return;
}
#endif
