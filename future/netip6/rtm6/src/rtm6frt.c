
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rtm6frt.c,v 1.1 2015/04/24 12:01:08 siva Exp $
 *
 * Description:This file contains functions needed for failed route 
               implementation.Routes that are failed are stored in  
               Failed Route Table(FRT).Routines that access this 
               Failed route are maintained in this File.               
 *******************************************************************/
#include "rtm6inc.h"
#include "rtm6frt.h"
/************************************************************************/
/*  Function Name   : Rtm6FrtInitGlobalInfo                              */
/*                                                                      */
/*  Description     : This function is invoked by the RTM6 module while  */
/*                    task initialisation. It initialises the failed    */
/*                    route global variables.                           */
/*                                                                      */
/*  Input(s)        : None.                                             */
/*                                                                      */
/*  Output(s)       : None.                                             */
/*                                                                      */
/*  Returns         : OSIX_SUCCESS / OSIX_FAILURE                       */
/************************************************************************/
PUBLIC INT4
Rtm6FrtInitGlobalInfo (VOID)
{
    gRtm6GlobalInfo.Rtm6FrtTable = RBTreeCreateEmbedded
        (FSAP_OFFSETOF (tRtm6FrtInfo, RbNode), Rtm6RBTreeFrtEntryCmp);

    if (gRtm6GlobalInfo.Rtm6FrtTable == NULL)
    {
        return RTM6_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*-------------------------------------------------------------------+
 * Function           : Rtm6RBTreeFrtEntryCmp
 * 
 * Input(s)           : pRBElem, pRBElemIn
 *
 * Output(s)          : None.
 *     
 * Returns            : -1  if pRBElem <  pRBElemIn
 *                       1   if pRBElem >  pRBElemIn
 *                       0   if pRBElem == pRBElemIn
 * Action             : This procedure compares the two FRT entries 
 *                      in lexicographic order of the indices of the 
 *                      FRT entry.
 *-------------------------------------------------------------------*/
INT4
Rtm6RBTreeFrtEntryCmp (tRBElem * pRBElem, tRBElem * pRBElemIn)
{
    tRtm6FrtInfo       *pRtm6Frt = pRBElem;
    tRtm6FrtInfo       *pRtm6FrtIn = pRBElemIn;
    INT4                i4RetVal;

    if (pRtm6Frt->u4CxtId < pRtm6FrtIn->u4CxtId)
    {
        return IP6_RB_LESSER;
    }
    else if (pRtm6Frt->u4CxtId > pRtm6FrtIn->u4CxtId)
    {
        return IP6_RB_GREATER;
    }

    i4RetVal = Ip6AddrCompare (pRtm6Frt->DestPrefix, pRtm6FrtIn->DestPrefix);

    if (i4RetVal == IP6_MINUS_ONE)
    {
        return IP6_RB_LESSER;
    }
    else if (i4RetVal == IP6_ONE)
    {
        return IP6_RB_GREATER;
    }
    if (pRtm6Frt->u1PrefixLen < pRtm6FrtIn->u1PrefixLen)
    {
        return IP6_RB_LESSER;
    }
    else if (pRtm6Frt->u1PrefixLen > pRtm6FrtIn->u1PrefixLen)
    {
        return IP6_RB_GREATER;
    }

    i4RetVal = Ip6AddrCompare (pRtm6Frt->NextHopAddr, pRtm6FrtIn->NextHopAddr);

    if (i4RetVal == IP6_MINUS_ONE)
    {
        return IP6_RB_LESSER;
    }
    else if (i4RetVal == IP6_ONE)
    {
        return IP6_RB_GREATER;
    }

    if (pRtm6Frt->i1Proto < pRtm6FrtIn->i1Proto)
    {
        return IP6_RB_LESSER;
    }
    else if (pRtm6Frt->i1Proto > pRtm6FrtIn->i1Proto)
    {
        return IP6_RB_GREATER;
    }
    return IP6_RB_EQUAL;
}

/************************************************************************/
/* Function Name      : Rtm6FrtAddInfo                                   */
/*                                                                      */
/* Description        : This function adds the NPAPI Failed route       */
/*                      entries to the global tree.                     */
/*                                                                      */
/* Input(s)           : pRtInfo - Rtm6 Route  message                    */
/*                      u4CxtId - Context Id of the entry               */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/
INT4
Rtm6FrtAddInfo (tIp6RtEntry * pRt6Info, UINT4 u4CxtId,UINT1 u1Type)
{
    tRtm6FrtInfo       *pRtm6FrtNode = NULL;
    tRtm6FrtInfo       *pTmpRtm6FrtNode = NULL;
    tRtm6FrtInfo        Rtm6FrtNode;
    UINT4               u4Result = 0;
    if (pRt6Info == NULL)
    {
        return RTM6_FAILURE;
    }
   /* Check whether the entry is already present in the RBtree, if present
    * just update the fields. If not present, allocate memory and add the
    * entry to the RBtree */
    Rtm6FrtNode.u4CxtId = u4CxtId;
    MEMCPY (&(Rtm6FrtNode.DestPrefix), &(pRt6Info->dst), IPVX_IPV6_ADDR_LEN);
    MEMCPY (&(Rtm6FrtNode.NextHopAddr), &(pRt6Info->nexthop),
            IPVX_IPV6_ADDR_LEN);
    Rtm6FrtNode.u1PrefixLen = pRt6Info->u1Prefixlen;
    Rtm6FrtNode.i1Proto = pRt6Info->i1Proto;

    pTmpRtm6FrtNode = RBTreeGet (gRtm6GlobalInfo.Rtm6FrtTable,
                                 (tRBElem *) & Rtm6FrtNode);
    if (pTmpRtm6FrtNode == NULL)
    {
        pRtm6FrtNode =
            (tRtm6FrtInfo *) MemAllocMemBlk ((tMemPoolId)RTM6_FRT_MSG_POOL_ID);
        if (pRtm6FrtNode == NULL)
        {
            UtlTrcLog (1, 1, "",
                           "ERROR[RTM]: IPV6 Failed Route Entry Table Allocation Failure\n");
            return RTM6_FAILURE;
        }
        MEMSET (pRtm6FrtNode, 0, sizeof (tRtm6FrtInfo));
        pRtm6FrtNode->u4CxtId = u4CxtId;
        MEMCPY (&(pRtm6FrtNode->DestPrefix), &(pRt6Info->dst),
                IPVX_IPV6_ADDR_LEN);
        pRtm6FrtNode->u1PrefixLen = pRt6Info->u1Prefixlen;
        MEMCPY (&(pRtm6FrtNode->NextHopAddr), &(pRt6Info->nexthop),
                IPVX_IPV6_ADDR_LEN);
        pRtm6FrtNode->i1Proto = pRt6Info->i1Proto;
        pRtm6FrtNode->u4Metric =  pRt6Info->u4Metric;
        pRtm6FrtNode->u1MetricType =  pRt6Info->u1MetricType;
        pRtm6FrtNode->u4Flag =  pRt6Info->u4Flag;
        pRtm6FrtNode->u1AddrType = pRt6Info->u1AddrType;
        pRtm6FrtNode->u4Index = pRt6Info->u4Index;
        Rtm6FrtNode.u1RtCount = pRt6Info->u1EcmpCount;
        Rtm6FrtNode.u1Type = u1Type;
        u4Result = RBTreeAdd (gRtm6GlobalInfo.Rtm6FrtTable, pRtm6FrtNode);
    }
    else
    {
        pRtm6FrtNode = pTmpRtm6FrtNode;
    }
    UNUSED_PARAM (u4Result);
    return RTM6_SUCCESS;
}

/************************************************************************/
/* Function Name      : Rtm6FrtGetInfo                                   */
/*                                                                      */
/* Description        : This function gets the NPAPI Failed route       */
/*                      entries to the global tree.                     */
/*                                                                      */
/* Input(s)           : pRtInfo - Rtm6 Route  message                    */
/*                      u4CxtId - Context Id of the entry               */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

tRtm6FrtInfo *
Rtm6FrtGetInfo (tIp6RtEntry* pRt6Info, UINT4 u4CxtId)
{
    tRtm6FrtInfo        Rtm6FrtNode;
    tRtm6FrtInfo       *pRtm6FrtNode = NULL;
    /* Check whether the entry is already present in the RBtree, if present
     * just update the fields.*/
    Rtm6FrtNode.u4CxtId = u4CxtId;
    MEMCPY (&(Rtm6FrtNode.DestPrefix), &(pRt6Info->dst), IPVX_IPV6_ADDR_LEN);
    MEMCPY (&(Rtm6FrtNode.NextHopAddr), &(pRt6Info->nexthop),
            IPVX_IPV6_ADDR_LEN);
    Rtm6FrtNode.u1PrefixLen = pRt6Info->u1Prefixlen;
    Rtm6FrtNode.i1Proto = pRt6Info->i1Proto;

    pRtm6FrtNode = RBTreeGet (gRtm6GlobalInfo.Rtm6FrtTable,(tRBElem *) & Rtm6FrtNode); 
    if(pRtm6FrtNode == NULL)
    {
        return NULL;
    }
    return pRtm6FrtNode;
}

/************************************************************************/
/* Function Name      : Rtm6FrtDeleteEntry                               */
/*                                                                      */
/* Description        : This function deletes the NPAPI Failed route    */
/*                      entries to the global tree.                     */
/*                                                                      */
/* Input(s)           : pRtm6FrtNode - Rtm failed route  node            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/
INT4
Rtm6FrtDeleteEntry (tRtm6FrtInfo *pRtm6FrtNode)
{
    /* Delete the route entry already present in the RBtree*/
    if (RBTreeRem (gRtm6GlobalInfo.Rtm6FrtTable, (tRBElem *) pRtm6FrtNode ) == NULL)
    {
         return RTM6_FAILURE;
    }

    if (MEM_FAILURE == MemReleaseMemBlock (RTM6_FRT_MSG_POOL_ID,
                                               (VOID *) pRtm6FrtNode))
    {
            return RTM6_FAILURE;
    }
    return RTM6_SUCCESS;
}

/************************************************************************/
/* Function Name      : Rtm6GetFirstFrtEntry                             */
/*                                                                      */
/* Description        : This function get the first NPAPI Failed route  */
/*                      entries from the FRT table                      */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

tRtm6FrtInfo *
Rtm6GetFirstFrtEntry()
{
   tRtm6FrtInfo *pRtm6FrtNode;
   pRtm6FrtNode = RBTreeGetFirst (gRtm6GlobalInfo.Rtm6FrtTable);
   if(pRtm6FrtNode == NULL)
   {
      return NULL;
   }
   return pRtm6FrtNode;
}


/************************************************************************/
/* Function Name      : Rtm6GetNextFrtEntry                              */
/*                                                                      */
/* Description        : This function get the next NPAPI Failed route  */
/*                      entries from the FRT table                      */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

tRtm6FrtInfo *
Rtm6GetNextFrtEntry(tRtm6FrtInfo *pRtm6FrtNode)
{
    tRtm6FrtInfo *pRtm6NextFrtNode;
    pRtm6NextFrtNode = RBTreeGetNext (gRtm6GlobalInfo.Rtm6FrtTable,
                                     (tRBElem *)pRtm6FrtNode, NULL);
    if(pRtm6NextFrtNode == NULL)
    {
        return NULL;
    }
    return pRtm6NextFrtNode;
}

/************************************************************************/
/* Function Name      : Rtm6GetFrtTableCount                             */
/*                                                                      */
/* Description        : This function gets the total number of entries  */
/*                      in FRT table                                    */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/
VOID
Rtm6GetFrtTableCount(UINT4 *pFrtCount)
{
   UINT4 u4Count=0;
   tRtm6FrtInfo *pRtm6FrtNode;
   pRtm6FrtNode = Rtm6GetFirstFrtEntry();
   while(pRtm6FrtNode != NULL)
   {
       u4Count++;
       pRtm6FrtNode = Rtm6GetNextFrtEntry(pRtm6FrtNode); 
   }   
   *pFrtCount = u4Count;
}

/************************************************************************/
/* Function Name      : Rtm6FrtDeInitGlobalInfo                          */
/*                                                                      */
/* Description        : This function is invoked by the RTM6 module      */
/*                      during module shutdown and this function        */
/*                      deinitializes the FRT tree                      */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/************************************************************************/
INT4
Rtm6FrtDeInitGlobalInfo (VOID)
{
    if (gRtm6GlobalInfo.Rtm6FrtTable != NULL)
    {
        RBTreeDelete (gRtm6GlobalInfo.Rtm6FrtTable);
        gRtm6GlobalInfo.Rtm6FrtTable = NULL;
    }
    return OSIX_SUCCESS;
}

