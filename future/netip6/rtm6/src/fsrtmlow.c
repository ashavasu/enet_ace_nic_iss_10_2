/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fsrtmlow.c,v 1.19 2015/10/19 12:22:10 siva Exp $
 *
 * Description:This file contains low level routines of the 
 *             RTM6 module.                                  
 *
 *******************************************************************/
#include   "rtm6inc.h"
# include  "fsrtmlow.h"

# include  "include.h"
# include  "fsrtmcon.h"
# include  "fsrtmogi.h"
# include  "midconst.h"
# include  "rtm6cli.h"
# include  "fsmirt6cli.h"

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsRrd6RouterId
 Input       :  The Indices

                The Object 
                retValFsRrd6RouterId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRrd6RouterId (UINT4 *pu4RetValFsRrd6RouterId)
{
    tRtm6Cxt           *pRtm6Cxt = gRtm6GlobalInfo.pRtm6Cxt;

    if (Rtm6UtilGetGblRrd6Status () == OSIX_FALSE)
    {
        return SNMP_SUCCESS;
    }

    if (pRtm6Cxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsRrd6RouterId = pRtm6Cxt->u4RouterId;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRrd6FilterByOspfTag
 Input       :  The Indices

                The Object 
                retValFsRrd6FilterByOspfTag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRrd6FilterByOspfTag (INT4 *pi4RetValFsRrd6FilterByOspfTag)
{
    tRtm6Cxt           *pRtm6Cxt = gRtm6GlobalInfo.pRtm6Cxt;

    if (Rtm6UtilGetGblRrd6Status () == OSIX_FALSE)
    {
        return SNMP_SUCCESS;
    }

    if (pRtm6Cxt == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsRrd6FilterByOspfTag =
        (INT4) pRtm6Cxt->Rtm6ConfigInfo.u1Rrd6FilterByOspfTag;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRrd6FilterOspfTag
 Input       :  The Indices

                The Object 
                retValFsRrd6FilterOspfTag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRrd6FilterOspfTag (INT4 *pi4RetValFsRrd6FilterOspfTag)
{
    tRtm6Cxt           *pRtm6Cxt = gRtm6GlobalInfo.pRtm6Cxt;

    if (Rtm6UtilGetGblRrd6Status () == OSIX_FALSE)
    {
        return SNMP_SUCCESS;
    }

    if (pRtm6Cxt == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsRrd6FilterOspfTag =
        (INT4) pRtm6Cxt->Rtm6ConfigInfo.u4OspfTagValue;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRrd6FilterOspfTagMask
 Input       :  The Indices

                The Object 
                retValFsRrd6FilterOspfTagMask
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRrd6FilterOspfTagMask (INT4 *pi4RetValFsRrd6FilterOspfTagMask)
{
    tRtm6Cxt           *pRtm6Cxt = gRtm6GlobalInfo.pRtm6Cxt;

    if (Rtm6UtilGetGblRrd6Status () == OSIX_FALSE)
    {
        return SNMP_SUCCESS;
    }

    if (pRtm6Cxt == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsRrd6FilterOspfTagMask =
        (INT4) pRtm6Cxt->Rtm6ConfigInfo.u4OspfTagMask;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRrd6RouterASNumber
 Input       :  The Indices

                The Object 
                retValFsRrd6RouterASNumber
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRrd6RouterASNumber (INT4 *pi4RetValFsRrd6RouterASNumber)
{
    tRtm6Cxt           *pRtm6Cxt = gRtm6GlobalInfo.pRtm6Cxt;

    if (Rtm6UtilGetGblRrd6Status () == OSIX_FALSE)
    {
        return SNMP_SUCCESS;
    }

    if (pRtm6Cxt == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsRrd6RouterASNumber = (INT4) pRtm6Cxt->u2AsNumber;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRrd6AdminStatus
 Input       :  The Indices

                The Object 
                retValFsRrd6AdminStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRrd6AdminStatus (INT4 *pi4RetValFsRrd6AdminStatus)
{
    tRtm6Cxt           *pRtm6Cxt = gRtm6GlobalInfo.pRtm6Cxt;

    if (Rtm6UtilGetGblRrd6Status () == OSIX_FALSE)
    {
        return SNMP_SUCCESS;
    }

    if (pRtm6Cxt == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsRrd6AdminStatus = (INT4) pRtm6Cxt->u1Rrd6AdminStatus;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRrd6Trace
 Input       :  The Indices

                The Object 
                retValFsRrd6Trace
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRrd6Trace (UINT4 *pu4RetValFsRrd6Trace)
{
    tRtm6Cxt           *pRtm6Cxt = gRtm6GlobalInfo.pRtm6Cxt;

    if (Rtm6UtilGetGblRrd6Status () == OSIX_FALSE)
    {
        return SNMP_SUCCESS;
    }

    if (pRtm6Cxt == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsRrd6Trace = pRtm6Cxt->u4TraceFlag;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRrd6ThrotLimit
 Input       :  The Indices

                The Object 
                retValFsRrd6ThrotLimit
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRrd6ThrotLimit (UINT4 *pu4RetValFsRrd6ThrotLimit)
{
    if (Rtm6UtilGetGblRrd6Status () == OSIX_FALSE)
    {
        return SNMP_SUCCESS;
    }

    *pu4RetValFsRrd6ThrotLimit = gRtm6GlobalInfo.u4ThrotLimit;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRrd6MaximumBgpRoutes
 Input       :  The Indices

                The Object
                retValFsRrd6MaximumBgpRoutes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsRrd6MaximumBgpRoutes(UINT4 *pu4RetValFsRrd6MaximumBgpRoutes)
{

   *pu4RetValFsRrd6MaximumBgpRoutes = (UINT4) gRtm6GlobalInfo.u4MaxRTM6BgpRoute;
   return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhGetFsRrd6MaximumOspfRoutes
 Input       :  The Indices

                The Object
                retValFsRrd6MaximumOspfRoutes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsRrd6MaximumOspfRoutes(UINT4 *pu4RetValFsRrd6MaximumOspfRoutes)
{

   *pu4RetValFsRrd6MaximumOspfRoutes = (UINT4) gRtm6GlobalInfo.u4MaxRTM6OspfRoute;
   return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhGetFsRrd6MaximumRipRoutes
 Input       :  The Indices

                The Object
                retValFsRrd6MaximumRipRoutes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsRrd6MaximumRipRoutes(UINT4 *pu4RetValFsRrd6MaximumRipRoutes)
{

  *pu4RetValFsRrd6MaximumRipRoutes = (UINT4) gRtm6GlobalInfo.u4MaxRTM6RipRoute;
  return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhGetFsRrd6MaximumStaticRoutes
 Input       :  The Indices

                The Object
                retValFsRrd6MaximumStaticRoutes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsRrd6MaximumStaticRoutes(UINT4 *pu4RetValFsRrd6MaximumStaticRoutes)
{

   *pu4RetValFsRrd6MaximumStaticRoutes = (UINT4) gRtm6GlobalInfo.u4MaxRTM6StaticRoute;
   return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsRrd6MaximumISISRoutes
 Input       :  The Indices

                The Object
                retValFsRrd6MaximumISISRoutes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsRrd6MaximumISISRoutes(UINT4 *pu4RetValFsRrd6MaximumISISRoutes)
{

   *pu4RetValFsRrd6MaximumISISRoutes = (UINT4) gRtm6GlobalInfo.u4MaxRTM6IsisRoute;
   return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsRtm6StaticRouteDistance
 Input       :  The Indices

                The Object
                retValFsRtm6StaticRouteDistance
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsRtm6StaticRouteDistance(INT4 *pi4RetValFsRtm6StaticRouteDistance)
{
    tRtm6Cxt           *pRtm6Cxt = gRtm6GlobalInfo.pRtm6Cxt;

    if (pRtm6Cxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsRtm6StaticRouteDistance = (INT4) pRtm6Cxt->u1Ip6DefaultDistance;
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsRrd6RouterId
 Input       :  The Indices

                The Object 
                setValFsRrd6RouterId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRrd6RouterId (UINT4 u4SetValFsRrd6RouterId)
{
    tRtm6Cxt           *pRtm6Cxt = gRtm6GlobalInfo.pRtm6Cxt;

    if (Rtm6UtilGetGblRrd6Status () == OSIX_FALSE)
    {
        return SNMP_SUCCESS;
    }

    if (pRtm6Cxt == NULL)
    {
        return SNMP_FAILURE;
    }
    pRtm6Cxt->u4RouterId = (UINT4) u4SetValFsRrd6RouterId;
    UtilRtm6IncMsrForRtm6Table (pRtm6Cxt->u4ContextId,
                                u4SetValFsRrd6RouterId,
                                FsMIRrd6RouterId,
                                (sizeof (FsMIRrd6RouterId) /
                                 sizeof (UINT4)), 'p');
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsRrd6FilterByOspfTag
 Input       :  The Indices

                The Object 
                setValFsRrd6FilterByOspfTag
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRrd6FilterByOspfTag (INT4 i4SetValFsRrd6FilterByOspfTag)
{
    tRtm6Cxt           *pRtm6Cxt = gRtm6GlobalInfo.pRtm6Cxt;

    if (Rtm6UtilGetGblRrd6Status () == OSIX_FALSE)
    {
        return SNMP_SUCCESS;
    }

    if (pRtm6Cxt == NULL)
    {
        return SNMP_FAILURE;
    }
    pRtm6Cxt->Rtm6ConfigInfo.u1Rrd6FilterByOspfTag =
        (UINT1) i4SetValFsRrd6FilterByOspfTag;
    UtilRtm6IncMsrForRtm6Table (pRtm6Cxt->u4ContextId,
                                i4SetValFsRrd6FilterByOspfTag,
                                FsMIRrd6FilterByOspfTag,
                                (sizeof (FsMIRrd6FilterByOspfTag) /
                                 sizeof (UINT4)), 'i');
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsRrd6FilterOspfTag
 Input       :  The Indices

                The Object 
                setValFsRrd6FilterOspfTag
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRrd6FilterOspfTag (INT4 i4SetValFsRrd6FilterOspfTag)
{
    tRtm6Cxt           *pRtm6Cxt = gRtm6GlobalInfo.pRtm6Cxt;

    if (Rtm6UtilGetGblRrd6Status () == OSIX_FALSE)
    {
        return SNMP_SUCCESS;
    }

    if (pRtm6Cxt == NULL)
    {
        return SNMP_FAILURE;
    }
    pRtm6Cxt->Rtm6ConfigInfo.u4OspfTagValue =
        (UINT4) i4SetValFsRrd6FilterOspfTag;
    UtilRtm6IncMsrForRtm6Table (pRtm6Cxt->u4ContextId,
                                i4SetValFsRrd6FilterOspfTag,
                                FsMIRrd6FilterOspfTag,
                                (sizeof (FsMIRrd6FilterOspfTag) /
                                 sizeof (UINT4)), 'i');
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsRrd6FilterOspfTagMask
 Input       :  The Indices

                The Object 
                setValFsRrd6FilterOspfTagMask
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRrd6FilterOspfTagMask (INT4 i4SetValFsRrd6FilterOspfTagMask)
{
    tRtm6Cxt           *pRtm6Cxt = gRtm6GlobalInfo.pRtm6Cxt;

    if (Rtm6UtilGetGblRrd6Status () == OSIX_FALSE)
    {
        return SNMP_SUCCESS;
    }

    if (pRtm6Cxt == NULL)
    {
        return SNMP_FAILURE;
    }
    pRtm6Cxt->Rtm6ConfigInfo.u4OspfTagMask =
        (UINT4) i4SetValFsRrd6FilterOspfTagMask;
    UtilRtm6IncMsrForRtm6Table (pRtm6Cxt->u4ContextId,
                                i4SetValFsRrd6FilterOspfTagMask,
                                FsMIRrd6FilterOspfTagMask,
                                (sizeof (FsMIRrd6FilterOspfTagMask) /
                                 sizeof (UINT4)), 'i');
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsRrd6RouterASNumber
 Input       :  The Indices

                The Object 
                setValFsRrd6RouterASNumber
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRrd6RouterASNumber (INT4 i4SetValFsRrd6RouterASNumber)
{
    tRtm6Cxt           *pRtm6Cxt = gRtm6GlobalInfo.pRtm6Cxt;

    if (Rtm6UtilGetGblRrd6Status () == OSIX_FALSE)
    {
        return SNMP_SUCCESS;
    }

    if (pRtm6Cxt == NULL)
    {
        return SNMP_FAILURE;
    }
    pRtm6Cxt->u2AsNumber = (UINT2) i4SetValFsRrd6RouterASNumber;
    UtilRtm6IncMsrForRtm6Table (pRtm6Cxt->u4ContextId,
                                i4SetValFsRrd6RouterASNumber,
                                FsMIRrd6RouterASNumber,
                                (sizeof (FsMIRrd6RouterASNumber) /
                                 sizeof (UINT4)), 'i');
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsRrd6AdminStatus
 Input       :  The Indices

                The Object 
                setValFsRrd6AdminStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRrd6AdminStatus (INT4 i4SetValFsRrd6AdminStatus)
{
    tRtm6RegnId         RegnId;
    UINT2               u2Index = 0;
    INT1                i1RetVal = 0;
    tRtm6Cxt           *pRtm6Cxt = gRtm6GlobalInfo.pRtm6Cxt;

    MEMSET (&RegnId, 0, sizeof (tRtm6RegnId));

    if (Rtm6UtilGetGblRrd6Status () == OSIX_FALSE)
    {
        return SNMP_SUCCESS;
    }

    if (pRtm6Cxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pRtm6Cxt->u1Rrd6AdminStatus == RTM6_ADMIN_STATUS_ENABLED)
    {
        if (i4SetValFsRrd6AdminStatus == RTM6_ADMIN_STATUS_ENABLED)
        {
            return SNMP_SUCCESS;
        }
        else
        {
            /* RTM6 already enabled. */
            RTM6_TRC (pRtm6Cxt->u4ContextId, RTM6_ALL_FAILURE_TRC |
                      RTM6_MANAGEMENT_TRC, RTM6_MOD_NAME,
                      "\tDisabling RTM6 FAILED!!! - Disabling RTM6 not "
                      "supported.\n");
            return SNMP_FAILURE;
        }
    }

    pRtm6Cxt->u1Rrd6AdminStatus = (UINT1) i4SetValFsRrd6AdminStatus;
    /* check whether any RP's had registered with RTM6 */
    /* If so start sending the Registration ACK messages to them. */

    for (u2Index = pRtm6Cxt->u2Rtm6RtStartIndex;
         u2Index < IP6_MAX_ROUTING_PROTOCOLS;
         u2Index = pRtm6Cxt->aRtm6RegnTable[u2Index].u2NextRegId)
    {
        RegnId.u2ProtoId =
            pRtm6Cxt->aRtm6RegnTable[u2Index].u2RoutingProtocolId;
        RegnId.u4ContextId = pRtm6Cxt->u4ContextId;
        i1RetVal = Rtm6SendAckToRpInCxt (pRtm6Cxt, &RegnId);
        if (RTM6_FAILURE == i1RetVal)
        {
            RTM6_TRC (pRtm6Cxt->u4ContextId, RTM6_ALL_FAILURE_TRC |
                      RTM6_MANAGEMENT_TRC, RTM6_MOD_NAME,
                      "\tRtm6SendAckToRpInCxt failed!\n");
            return SNMP_FAILURE;
        }
    }
    UtilRtm6IncMsrForRtm6Table (pRtm6Cxt->u4ContextId,
                                i4SetValFsRrd6AdminStatus,
                                FsMIRrd6AdminStatus,
                                (sizeof (FsMIRrd6AdminStatus) /
                                 sizeof (UINT4)), 'i');
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsRrd6Trace
 Input       :  The Indices

                The Object 
                setValFsRrd6Trace
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRrd6Trace (UINT4 u4SetValFsRrd6Trace)
{
    tRtm6Cxt           *pRtm6Cxt = gRtm6GlobalInfo.pRtm6Cxt;

    if (Rtm6UtilGetGblRrd6Status () == OSIX_FALSE)
    {
        return SNMP_SUCCESS;
    }

    if (pRtm6Cxt == NULL)
    {
        return SNMP_FAILURE;
    }
    pRtm6Cxt->u4TraceFlag |= u4SetValFsRrd6Trace;
    UtilRtm6IncMsrForRtm6Table (pRtm6Cxt->u4ContextId,
                                u4SetValFsRrd6Trace,
                                FsMIRrd6Trace,
                                (sizeof (FsMIRrd6Trace) / sizeof (UINT4)), 'u');
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsRrd6ThrotLimit
 Input       :  The Indices

                The Object 
                setValFsRrd6ThrotLimit
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRrd6ThrotLimit (UINT4 u4SetValFsRrd6ThrotLimit)
{
    if (Rtm6UtilGetGblRrd6Status () == OSIX_FALSE)
    {
        return SNMP_SUCCESS;
    }

    gRtm6GlobalInfo.u4ThrotLimit = u4SetValFsRrd6ThrotLimit;
    UtilRtm6IncMsrForRtm6Scalar (u4SetValFsRrd6ThrotLimit,
                                 FsMIRtm6ThrotLimit,
                                 (sizeof (FsMIRtm6ThrotLimit) /
                                  sizeof (UINT4)));
    return SNMP_SUCCESS;
}


/****************************************************************************
 Function    :  nmhSetFsRrd6MaximumBgpRoutes
 Input       :  The Indices

                The Object
                setValFsRrd6MaximumBgpRoutes
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsRrd6MaximumBgpRoutes(UINT4 u4SetValFsRrd6MaximumBgpRoutes)
{

    if (Rtm6UtilGetGblRrd6Status () == OSIX_FALSE)
    {
        return SNMP_SUCCESS;
    }

    gRtm6GlobalInfo.u4MaxRTM6BgpRoute = u4SetValFsRrd6MaximumBgpRoutes;
    UtilRtm6IncMsrForRtm6Scalar (u4SetValFsRrd6MaximumBgpRoutes,
                                 FsMIRtm6MaximumBgpRoutes,
                                 (sizeof (FsMIRtm6MaximumBgpRoutes) /
                                  sizeof (UINT4)));

    return SNMP_SUCCESS;


}
/****************************************************************************
 Function    :  nmhSetFsRrd6MaximumOspfRoutes
 Input       :  The Indices

                The Object
                setValFsRrd6MaximumOspfRoutes
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsRrd6MaximumOspfRoutes(UINT4 u4SetValFsRrd6MaximumOspfRoutes)
{

    if (Rtm6UtilGetGblRrd6Status () == OSIX_FALSE)
    {
        return SNMP_SUCCESS;
    }

    gRtm6GlobalInfo.u4MaxRTM6OspfRoute = u4SetValFsRrd6MaximumOspfRoutes;
    UtilRtm6IncMsrForRtm6Scalar (u4SetValFsRrd6MaximumOspfRoutes,
                                 FsMIRtm6MaximumOspfRoutes,
                                 (sizeof (FsMIRtm6MaximumOspfRoutes) /
                                  sizeof (UINT4)));

    return SNMP_SUCCESS;


}
/****************************************************************************
 Function    :  nmhSetFsRrd6MaximumRipRoutes
 Input       :  The Indices

                The Object
                setValFsRrd6MaximumRipRoutes
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsRrd6MaximumRipRoutes(UINT4 u4SetValFsRrd6MaximumRipRoutes)
{

    if (Rtm6UtilGetGblRrd6Status () == OSIX_FALSE)
    {
        return SNMP_SUCCESS;
    }

    gRtm6GlobalInfo.u4MaxRTM6RipRoute = u4SetValFsRrd6MaximumRipRoutes;
    UtilRtm6IncMsrForRtm6Scalar (u4SetValFsRrd6MaximumRipRoutes,
                                 FsMIRtm6MaximumRipRoutes,
                                 (sizeof (FsMIRtm6MaximumRipRoutes) /
                                  sizeof (UINT4)));

    return SNMP_SUCCESS;


}
/****************************************************************************
 Function    :  nmhSetFsRrd6MaximumStaticRoutes
 Input       :  The Indices

                The Object
                setValFsRrd6MaximumStaticRoutes
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsRrd6MaximumStaticRoutes(UINT4 u4SetValFsRrd6MaximumStaticRoutes)
{

    if (Rtm6UtilGetGblRrd6Status () == OSIX_FALSE)
    {
        return SNMP_SUCCESS;
    }

    gRtm6GlobalInfo.u4MaxRTM6StaticRoute = u4SetValFsRrd6MaximumStaticRoutes;
    UtilRtm6IncMsrForRtm6Scalar (u4SetValFsRrd6MaximumStaticRoutes,
                                 FsMIRtm6MaximumStaticRoutes,
                                 (sizeof (FsMIRtm6MaximumStaticRoutes) /
                                  sizeof (UINT4)));

    return SNMP_SUCCESS;



}
/****************************************************************************
 Function    :  nmhSetFsRrd6MaximumISISRoutes
 Input       :  The Indices

                The Object
                setValFsRrd6MaximumISISRoutes
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsRrd6MaximumISISRoutes(UINT4 u4SetValFsRrd6MaximumISISRoutes)
{

    if (Rtm6UtilGetGblRrd6Status () == OSIX_FALSE)
    {
        return SNMP_SUCCESS;
    }

    gRtm6GlobalInfo.u4MaxRTM6IsisRoute = u4SetValFsRrd6MaximumISISRoutes;
    UtilRtm6IncMsrForRtm6Scalar (u4SetValFsRrd6MaximumISISRoutes,
                                 FsMIRtm6MaximumISISRoutes,
                                 (sizeof (FsMIRtm6MaximumISISRoutes) /
                                  sizeof (UINT4)));

    return SNMP_SUCCESS;



}

/****************************************************************************
 Function    :  nmhSetFsRtm6StaticRouteDistance
 Input       :  The Indices

                The Object
                setValFsRtm6StaticRouteDistance
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsRtm6StaticRouteDistance(INT4 i4SetValFsRtm6StaticRouteDistance)
{
    tRtm6Cxt           *pRtm6Cxt = gRtm6GlobalInfo.pRtm6Cxt;

    pRtm6Cxt->u1Ip6DefaultDistance = (UINT1) i4SetValFsRtm6StaticRouteDistance;

    UtilRtm6IncMsrForRtm6Table (pRtm6Cxt->u4ContextId,
                                i4SetValFsRtm6StaticRouteDistance,
                                FsMIRtm6StaticRouteDistance,
                                (sizeof (FsMIRtm6StaticRouteDistance) / sizeof (UINT4)), 'i');
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsRrd6RouterId
 Input       :  The Indices

                The Object 
                testValFsRrd6RouterId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRrd6RouterId (UINT4 *pu4ErrorCode, UINT4 u4TestValFsRrd6RouterId)
{
    tRtm6Cxt           *pRtm6Cxt = gRtm6GlobalInfo.pRtm6Cxt;

    if (Rtm6UtilGetGblRrd6Status () == OSIX_FALSE)
    {
        return SNMP_SUCCESS;
    }

    if (pRtm6Cxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if ((u4TestValFsRrd6RouterId < RTM6_MIN_ROUTER_ID) ||
        (u4TestValFsRrd6RouterId > RTM6_MAX_ROUTER_ID))
    {
        /* Invalid router id */
        RTM6_TRC (pRtm6Cxt->u4ContextId, RTM6_ALL_FAILURE_TRC |
                  RTM6_MANAGEMENT_TRC, RTM6_MOD_NAME,
                  "\tTesting Router Id Failed - Invalid Router Identifier.\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if (NetIpv4IfIsOurAddressInCxt (pRtm6Cxt->u4ContextId,
                                    u4TestValFsRrd6RouterId) != NETIPV4_SUCCESS)
    {
        RTM6_TRC (pRtm6Cxt->u4ContextId, RTM6_ALL_FAILURE_TRC |
                  RTM6_MANAGEMENT_TRC, RTM6_MOD_NAME,
                  "\tTesting Router Id Failed - Invalid Router Identifier.\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /* Router ID can be set to a valid value if and only if RRD6 Admin Status is
       Disabled. */
    if (pRtm6Cxt->u1Rrd6AdminStatus != RTM6_ADMIN_STATUS_DISABLED)
    {
        RTM6_TRC (pRtm6Cxt->u4ContextId, RTM6_ALL_FAILURE_TRC |
                  RTM6_MANAGEMENT_TRC, RTM6_MOD_NAME,
                  "\tTesting Router Id Failed - RTM6 Admin Status enabled.\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRrd6FilterByOspfTag
 Input       :  The Indices

                The Object 
                testValFsRrd6FilterByOspfTag
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRrd6FilterByOspfTag (UINT4 *pu4ErrorCode,
                                INT4 i4TestValFsRrd6FilterByOspfTag)
{
    tRtm6Cxt           *pRtm6Cxt = gRtm6GlobalInfo.pRtm6Cxt;

    if (Rtm6UtilGetGblRrd6Status () == OSIX_FALSE)
    {
        return SNMP_SUCCESS;
    }

    if (pRtm6Cxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if ((i4TestValFsRrd6FilterByOspfTag != RTM6_FILTER_DISABLED) &&
        (i4TestValFsRrd6FilterByOspfTag != RTM6_FILTER_ENABLED))
    {
        RTM6_TRC (pRtm6Cxt->u4ContextId, RTM6_ALL_FAILURE_TRC |
                  RTM6_MANAGEMENT_TRC, RTM6_MOD_NAME,
                  "\tTesting Ospf Tag Filtering Failed - Invalid Filter "
                  "Action.\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRrd6FilterOspfTag
 Input       :  The Indices

                The Object 
                testValFsRrd6FilterOspfTag
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRrd6FilterOspfTag (UINT4 *pu4ErrorCode, INT4
                              i4TestValFsRrd6FilterOspfTag)
{
    tRtm6Cxt           *pRtm6Cxt = gRtm6GlobalInfo.pRtm6Cxt;

    if (Rtm6UtilGetGblRrd6Status () == OSIX_FALSE)
    {
        return SNMP_SUCCESS;
    }

    if (pRtm6Cxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    /* Filtering on Manual TAG should not be allowed */
    if (i4TestValFsRrd6FilterOspfTag & RTM6_AUTOMATIC_BIT_SET)
    {
        return SNMP_SUCCESS;
    }

    RTM6_TRC (pRtm6Cxt->u4ContextId, RTM6_ALL_FAILURE_TRC |
              RTM6_MANAGEMENT_TRC, RTM6_MOD_NAME,
              "\tTesting Ospf Filter Tag Failed - Invalid Tag value.\n");
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsRrd6FilterOspfTagMask
 Input       :  The Indices

                The Object 
                testValFsRrd6FilterOspfTagMask
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRrd6FilterOspfTagMask (UINT4 *pu4ErrorCode,
                                  INT4 i4TestValFsRrd6FilterOspfTagMask)
{
    tRtm6Cxt           *pRtm6Cxt = gRtm6GlobalInfo.pRtm6Cxt;

    if (Rtm6UtilGetGblRrd6Status () == OSIX_FALSE)
    {
        return SNMP_SUCCESS;
    }

    if (pRtm6Cxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    /* Filtering on Manual TAG should not be allowed */
    if (i4TestValFsRrd6FilterOspfTagMask & RTM6_AUTOMATIC_BIT_SET)
    {
        return SNMP_SUCCESS;
    }

    RTM6_TRC (pRtm6Cxt->u4ContextId, RTM6_ALL_FAILURE_TRC |
              RTM6_MANAGEMENT_TRC, RTM6_MOD_NAME,
              "\tTesting Ospf Filter Tag Mask Failed - Invalid Tag Mask "
              "value.\n");
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsRrd6RouterASNumber
 Input       :  The Indices

                The Object 
                testValFsRrd6RouterASNumber
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRrd6RouterASNumber (UINT4 *pu4ErrorCode,
                               INT4 i4TestValFsRrd6RouterASNumber)
{
    tRtm6Cxt           *pRtm6Cxt = gRtm6GlobalInfo.pRtm6Cxt;

    if (Rtm6UtilGetGblRrd6Status () == OSIX_FALSE)
    {
        return SNMP_SUCCESS;
    }

    if (pRtm6Cxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    /* AS Number can be set to a valid value if and only if RRD6 Admin Status is
       Disabled. */
    if (pRtm6Cxt->u1Rrd6AdminStatus != RTM6_ADMIN_STATUS_DISABLED)
    {
        RTM6_TRC (pRtm6Cxt->u4ContextId, RTM6_ALL_FAILURE_TRC |
                  RTM6_MANAGEMENT_TRC, RTM6_MOD_NAME,
                  "\tTesting AS number Failed - RTM6 Admin Status enabled.\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsRrd6RouterASNumber < IPV6_MIN_AS_NUM) ||
        (i4TestValFsRrd6RouterASNumber > IPV6_MAX_AS_NUM))
    {
        RTM6_TRC (pRtm6Cxt->u4ContextId, RTM6_ALL_FAILURE_TRC |
                  RTM6_MANAGEMENT_TRC, RTM6_MOD_NAME,
                  "\tTesting AS number Failed - Invalid AS number.\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRrd6AdminStatus
 Input       :  The Indices

                The Object 
                testValFsRrd6AdminStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRrd6AdminStatus (UINT4 *pu4ErrorCode,
                            INT4 i4TestValFsRrd6AdminStatus)
{
    tRtm6Cxt           *pRtm6Cxt = gRtm6GlobalInfo.pRtm6Cxt;

    if (Rtm6UtilGetGblRrd6Status () == OSIX_FALSE)
    {
        return SNMP_SUCCESS;
    }

    if (pRtm6Cxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    /* RTM6 Admin status can be set from DISBALED TO ENABLED only once 
       Before setting the admin status to enabled the administrator 
       should have configured values for AS Number and Router ID */

    if (i4TestValFsRrd6AdminStatus != RTM6_ADMIN_STATUS_ENABLED)
    {
        RTM6_TRC (pRtm6Cxt->u4ContextId, RTM6_ALL_FAILURE_TRC |
                  RTM6_MANAGEMENT_TRC, RTM6_MOD_NAME,
                  "\tTesting RTM6 Admin Status Failed - Invalid Status.\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (pRtm6Cxt->u2AsNumber == 0)
    {
        RTM6_TRC (pRtm6Cxt->u4ContextId, RTM6_ALL_FAILURE_TRC |
                  RTM6_MANAGEMENT_TRC, RTM6_MOD_NAME,
                  "\tTesting RTM6 Admin Status Failed - AS number not "
                  "configured.\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (pRtm6Cxt->u4RouterId == 0)
    {
        /* Router-id not configrued. */
        RTM6_TRC (pRtm6Cxt->u4ContextId, RTM6_ALL_FAILURE_TRC |
                  RTM6_MANAGEMENT_TRC, RTM6_MOD_NAME,
                  "\tTesting RTM6 Admin Status Failed - Router Id not "
                  "configured.\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRrd6Trace
 Input       :  The Indices

                The Object 
                testValFsRrd6Trace
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRrd6Trace (UINT4 *pu4ErrorCode, UINT4 u4TestValFsRrd6Trace)
{
    tRtm6Cxt           *pRtm6Cxt = gRtm6GlobalInfo.pRtm6Cxt;

    UNUSED_PARAM (u4TestValFsRrd6Trace);
    if (Rtm6UtilGetGblRrd6Status () == OSIX_FALSE)
    {
        return SNMP_SUCCESS;
    }

    if (pRtm6Cxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRrd6ThrotLimit
 Input       :  The Indices

                The Object 
                testValFsRrd6ThrotLimit
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRrd6ThrotLimit (UINT4 *pu4ErrorCode, UINT4 u4TestValFsRrd6ThrotLimit)
{
    if (Rtm6UtilGetGblRrd6Status () == OSIX_FALSE)
    {
        return SNMP_SUCCESS;
    }

    if (u4TestValFsRrd6ThrotLimit > 0)
    {
        return SNMP_SUCCESS;
    }

    RTM6_TRC (RTM6_INVALID_CXT_ID, RTM6_ALL_FAILURE_TRC |
              RTM6_MANAGEMENT_TRC, RTM6_MOD_NAME,
              "\tTesting RTM6 Throttle Value Failed - Invalid Throttle "
              "value.\n");
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    CLI_SET_ERR (CLI_RRD6_INVALID_INPUT);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsRrd6MaximumBgpRoutes
 Input       :  The Indices

                The Object
                testValFsRrd6MaximumBgpRoutes
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsRrd6MaximumBgpRoutes(UINT4 *pu4ErrorCode , UINT4 u4TestValFsRrd6MaximumBgpRoutes)
{

    UINT4 u4TotalRTMMemRes = 0;

    if (Rtm6UtilGetGblRrd6Status () == OSIX_FALSE)
    {
        return SNMP_SUCCESS;
    }


    u4TotalRTMMemRes = MAX_RTM6_ROUTE_TABLE_ENTRIES- (SYS_DEF_MAX_INTERFACES + gRtm6GlobalInfo.u4MaxRTM6RipRoute+
                       gRtm6GlobalInfo.u4MaxRTM6OspfRoute + gRtm6GlobalInfo.u4MaxRTM6StaticRoute +
                       gRtm6GlobalInfo.u4MaxRTM6IsisRoute) ;


    if ( gRtm6GlobalInfo.u4MaxRTM6BgpRoute >= u4TestValFsRrd6MaximumBgpRoutes )
    {
         /* If already installed route count it more than new value then return failure*/
        if (gRtm6GlobalInfo.u4BgpRts > u4TestValFsRrd6MaximumBgpRoutes)
        {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE ;
                 CLI_SET_ERR(CLI_RRD6_ROUTE_EXIST);
                return SNMP_FAILURE;
        }
        return SNMP_SUCCESS;
    }
    else
    {   /* memory reservation is increased for BGP routes*/
            if (u4TestValFsRrd6MaximumBgpRoutes <= u4TotalRTMMemRes)
            {
                    return SNMP_SUCCESS;
            }
            else
            {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    CLI_SET_ERR (CLI_RRD6_MEMORY_OVERRUN);
                    return SNMP_FAILURE;
            }
    }


}
/****************************************************************************
 Function    :  nmhTestv2FsRrd6MaximumOspfRoutes
 Input       :  The Indices

                The Object
                testValFsRrd6MaximumOspfRoutes
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsRrd6MaximumOspfRoutes(UINT4 *pu4ErrorCode , UINT4 u4TestValFsRrd6MaximumOspfRoutes)
{

    UINT4 u4TotalRTMMemRes = 0;

    if (Rtm6UtilGetGblRrd6Status () == OSIX_FALSE)
    {
        return SNMP_SUCCESS;
    }


    u4TotalRTMMemRes = MAX_RTM6_ROUTE_TABLE_ENTRIES- (SYS_DEF_MAX_INTERFACES + gRtm6GlobalInfo.u4MaxRTM6RipRoute+
                       gRtm6GlobalInfo.u4MaxRTM6BgpRoute + gRtm6GlobalInfo.u4MaxRTM6StaticRoute + 
                       gRtm6GlobalInfo.u4MaxRTM6IsisRoute) ;


    if ( gRtm6GlobalInfo.u4MaxRTM6OspfRoute >= u4TestValFsRrd6MaximumOspfRoutes )
    {
         /* If already installed route count it more than new value then return failure*/
        if (gRtm6GlobalInfo.u4OspfRts > u4TestValFsRrd6MaximumOspfRoutes)
        {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE ;
                 CLI_SET_ERR(CLI_RRD6_ROUTE_EXIST);
                return SNMP_FAILURE;
        }
        return SNMP_SUCCESS;
    }
    else
    {   /* memory reservation is increased for BGP routes*/
            if (u4TestValFsRrd6MaximumOspfRoutes <= u4TotalRTMMemRes)
            {
                    return SNMP_SUCCESS;
            }
            else
            {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    CLI_SET_ERR (CLI_RRD6_MEMORY_OVERRUN);
                    return SNMP_FAILURE;
            }
    }


}
/****************************************************************************
 Function    :  nmhTestv2FsRrd6MaximumRipRoutes
 Input       :  The Indices

                The Object
                testValFsRrd6MaximumRipRoutes
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsRrd6MaximumRipRoutes(UINT4 *pu4ErrorCode , UINT4 u4TestValFsRrd6MaximumRipRoutes)
{

    UINT4 u4TotalRTMMemRes = 0;

    if (Rtm6UtilGetGblRrd6Status () == OSIX_FALSE)
    {
        return SNMP_SUCCESS;
    }


    u4TotalRTMMemRes = MAX_RTM6_ROUTE_TABLE_ENTRIES- (SYS_DEF_MAX_INTERFACES + gRtm6GlobalInfo.u4MaxRTM6BgpRoute+
                       gRtm6GlobalInfo.u4MaxRTM6OspfRoute + gRtm6GlobalInfo.u4MaxRTM6StaticRoute +
                       gRtm6GlobalInfo.u4MaxRTM6IsisRoute) ;


    if ( gRtm6GlobalInfo.u4MaxRTM6RipRoute >= u4TestValFsRrd6MaximumRipRoutes )
    {
         /* If already installed route count it more than new value then return failure*/
        if (gRtm6GlobalInfo.u4RipRts > u4TestValFsRrd6MaximumRipRoutes)
        {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE ;
                 CLI_SET_ERR(CLI_RRD6_ROUTE_EXIST);
                return SNMP_FAILURE;
        }
        return SNMP_SUCCESS;
    }
    else
    {   /* memory reservation is increased for BGP routes*/
            if (u4TestValFsRrd6MaximumRipRoutes <= u4TotalRTMMemRes)
            {
                    return SNMP_SUCCESS;
            }
            else
            {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    CLI_SET_ERR (CLI_RRD6_MEMORY_OVERRUN);
                    return SNMP_FAILURE;
            }
    }


}
/****************************************************************************
 Function    :  nmhTestv2FsRrd6MaximumStaticRoutes
 Input       :  The Indices

                The Object
                testValFsRrd6MaximumStaticRoutes
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsRrd6MaximumStaticRoutes(UINT4 *pu4ErrorCode , UINT4 u4TestValFsRrd6MaximumStaticRoutes)
{

    UINT4 u4TotalRTMMemRes = 0;

    if (Rtm6UtilGetGblRrd6Status () == OSIX_FALSE)
    {
        return SNMP_SUCCESS;
    }


    u4TotalRTMMemRes = MAX_RTM6_ROUTE_TABLE_ENTRIES- (SYS_DEF_MAX_INTERFACES + gRtm6GlobalInfo.u4MaxRTM6RipRoute+
                       gRtm6GlobalInfo.u4MaxRTM6OspfRoute + gRtm6GlobalInfo.u4MaxRTM6BgpRoute +
                       gRtm6GlobalInfo.u4MaxRTM6IsisRoute ) ;


    if ( gRtm6GlobalInfo.u4MaxRTM6StaticRoute >= u4TestValFsRrd6MaximumStaticRoutes )
    {
         /* If already installed route count it more than new value then return failure*/
        if (gRtm6GlobalInfo.u4StaticRts > u4TestValFsRrd6MaximumStaticRoutes)
        {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE ;
                 CLI_SET_ERR(CLI_RRD6_ROUTE_EXIST);
                return SNMP_FAILURE;
        }
        return SNMP_SUCCESS;
    }
    else
    {   /* memory reservation is increased for BGP routes*/
            if (u4TestValFsRrd6MaximumStaticRoutes <= u4TotalRTMMemRes)
            {
                    return SNMP_SUCCESS;
            }
            else
            {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    CLI_SET_ERR (CLI_RRD6_MEMORY_OVERRUN);
                    return SNMP_FAILURE;
            }
    }


}

/****************************************************************************
 Function    :  nmhTestv2FsRrd6MaximumISISRoutes
 Input       :  The Indices

                The Object
                testValFsRrd6MaximumISISRoutes
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsRrd6MaximumISISRoutes(UINT4 *pu4ErrorCode , UINT4 u4TestValFsRrd6MaximumISISRoutes)
{

    UINT4 u4TotalRTMMemRes = 0;

    if (Rtm6UtilGetGblRrd6Status () == OSIX_FALSE)
    {
        return SNMP_SUCCESS;
    }


    u4TotalRTMMemRes = MAX_RTM6_ROUTE_TABLE_ENTRIES- (SYS_DEF_MAX_INTERFACES + gRtm6GlobalInfo.u4MaxRTM6RipRoute+
                       gRtm6GlobalInfo.u4MaxRTM6OspfRoute + gRtm6GlobalInfo.u4MaxRTM6BgpRoute + 
                       gRtm6GlobalInfo.u4MaxRTM6StaticRoute) ;


    if ( gRtm6GlobalInfo.u4MaxRTM6IsisRoute >= u4TestValFsRrd6MaximumISISRoutes )
    {
         /* If already installed route count it more than new value then return failure*/
        if (gRtm6GlobalInfo.u4IsisRts > u4TestValFsRrd6MaximumISISRoutes)
        {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE ;
                 CLI_SET_ERR(CLI_RRD6_ROUTE_EXIST);
                return SNMP_FAILURE;
        }
        return SNMP_SUCCESS;
    }
    else
    {   /* memory reservation is increased for BGP routes*/
            if (u4TestValFsRrd6MaximumISISRoutes <= u4TotalRTMMemRes)
            {
                    return SNMP_SUCCESS;
            }
            else
            {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    CLI_SET_ERR (CLI_RRD6_MEMORY_OVERRUN);
                    return SNMP_FAILURE;
            }
    }


}

/****************************************************************************
 Function    :  nmhTestv2FsRtm6StaticRouteDistance
 Input       :  The Indices

                The Object
                testValFsRtm6StaticRouteDistance
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsRtm6StaticRouteDistance(UINT4 *pu4ErrorCode , INT4 i4TestValFsRtm6StaticRouteDistance)
{
    tRtm6Cxt            *pRtm6Cxt = gRtm6GlobalInfo.pRtm6Cxt;

    if (pRtm6Cxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsRtm6StaticRouteDistance > RTM6_IP6_ROUTE_MAX_DISTANCE) ||
        (i4TestValFsRtm6StaticRouteDistance < RTM6_IP6_ROUTE_MIN_DISTANCE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsRrd6RouterId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRrd6RouterId (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsRrd6FilterByOspfTag
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRrd6FilterByOspfTag (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsRrd6FilterOspfTag
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRrd6FilterOspfTag (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsRrd6FilterOspfTagMask
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRrd6FilterOspfTagMask (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsRrd6RouterASNumber
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRrd6RouterASNumber (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsRrd6AdminStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRrd6AdminStatus (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsRrd6Trace
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRrd6Trace (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsRrd6ThrotLimit
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRrd6ThrotLimit (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsRrd6MaximumBgpRoutes
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FsRrd6MaximumBgpRoutes(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
        UNUSED_PARAM(pu4ErrorCode);
        UNUSED_PARAM(pSnmpIndexList);
        UNUSED_PARAM(pSnmpVarBind);
        return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhDepv2FsRrd6MaximumOspfRoutes
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FsRrd6MaximumOspfRoutes(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
        UNUSED_PARAM(pu4ErrorCode);
        UNUSED_PARAM(pSnmpIndexList);
        UNUSED_PARAM(pSnmpVarBind);
        return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsRrd6MaximumRipRoutes
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FsRrd6MaximumRipRoutes(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
        UNUSED_PARAM(pu4ErrorCode);
        UNUSED_PARAM(pSnmpIndexList);
        UNUSED_PARAM(pSnmpVarBind);
        return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhDepv2FsRrd6MaximumStaticRoutes
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FsRrd6MaximumStaticRoutes(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
        UNUSED_PARAM(pu4ErrorCode);
        UNUSED_PARAM(pSnmpIndexList);
        UNUSED_PARAM(pSnmpVarBind);
        return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsRrd6MaximumISISRoutes
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FsRrd6MaximumISISRoutes(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
        UNUSED_PARAM(pu4ErrorCode);
        UNUSED_PARAM(pSnmpIndexList);
        UNUSED_PARAM(pSnmpVarBind);
        return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsRtm6StaticRouteDistance
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FsRtm6StaticRouteDistance(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
    UNUSED_PARAM(pu4ErrorCode);
    UNUSED_PARAM(pSnmpIndexList);
    UNUSED_PARAM(pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsRrd6ControlTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsRrd6ControlTable
 Input       :  The Indices
                FsRrd6ControlDestIpAddress
                FsRrd6ControlNetMaskLen
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsRrd6ControlTable (tSNMP_OCTET_STRING_TYPE *
                                            pFsRrd6ControlDestIpAddress,
                                            INT4 i4FsRrd6ControlNetMaskLen)
{
    tRrd6ControlInfo   *pRtm6CtrlInfo = NULL;
    UINT1               u1TempIpAddr[IP6_ADDR_SIZE];
    tRtm6Cxt           *pRtm6Cxt = gRtm6GlobalInfo.pRtm6Cxt;

    if (Rtm6UtilGetGblRrd6Status () == OSIX_FALSE)
    {
        return SNMP_SUCCESS;
    }

    if (pRtm6Cxt == NULL)
    {
        return SNMP_FAILURE;
    }

    MEMSET (u1TempIpAddr, 0, IP6_ADDR_SIZE);

    if ((pFsRrd6ControlDestIpAddress->i4_Length != IP6_ADDR_SIZE) ||
        ((MEMCMP (pFsRrd6ControlDestIpAddress->pu1_OctetList, u1TempIpAddr,
                  IP6_ADDR_SIZE) == 0) &&
         (i4FsRrd6ControlNetMaskLen != IP6_ADDR_MAX_PREFIX)))
    {
        RTM6_TRC (pRtm6Cxt->u4ContextId, RTM6_ALL_FAILURE_TRC, RTM6_MOD_NAME,
                  "\tInvalid Control Table Index.\n");
        return SNMP_FAILURE;
    }

    TMO_SLL_Scan (&(pRtm6Cxt->Rtm6CtrlList), pRtm6CtrlInfo, tRrd6ControlInfo *)
    {
        /* Check whether the IP address and mask match */
        if ((MEMCMP (pRtm6CtrlInfo->DestNet.u1_addr,
                     pFsRrd6ControlDestIpAddress->pu1_OctetList,
                     pFsRrd6ControlDestIpAddress->i4_Length) == 0) &&
            (pRtm6CtrlInfo->u1Prefixlen == (UINT1) i4FsRrd6ControlNetMaskLen))
        {
            /* We have this entry in the RRD control table */

            return SNMP_SUCCESS;
        }
    }
    /* No match found */
    RTM6_TRC (pRtm6Cxt->u4ContextId, RTM6_ALL_FAILURE_TRC, RTM6_MOD_NAME,
              "\tInvalid Control Table Index. No matching entry found.\n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsRrd6ControlTable
 Input       :  The Indices
                FsRrd6ControlDestIpAddress
                FsRrd6ControlNetMaskLen
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */
INT1
nmhGetFirstIndexFsRrd6ControlTable (tSNMP_OCTET_STRING_TYPE *
                                    pFsRrd6ControlDestIpAddress,
                                    INT4 *pi4FsRrd6ControlNetMaskLen)
{
    tRrd6ControlInfo   *pRtm6CtrlInfo = NULL;
    tRtm6Cxt           *pRtm6Cxt = gRtm6GlobalInfo.pRtm6Cxt;

    if (Rtm6UtilGetGblRrd6Status () == OSIX_FALSE)
    {
        return SNMP_FAILURE;
    }

    if (pRtm6Cxt == NULL)
    {
        return SNMP_FAILURE;
    }
    pRtm6CtrlInfo =
        (tRrd6ControlInfo *) TMO_SLL_First (&(pRtm6Cxt->Rtm6CtrlList));
    if (pRtm6CtrlInfo == NULL)
    {
        /* No entry is there in the RRD6 Control table */
        RTM6_TRC (pRtm6Cxt->u4ContextId, RTM6_ALL_FAILURE_TRC, RTM6_MOD_NAME,
                  "\tNo Control Table entry is Present.\n");
        return (SNMP_FAILURE);
    }

    MEMCPY (pFsRrd6ControlDestIpAddress->pu1_OctetList,
            pRtm6CtrlInfo->DestNet.u1_addr, IP6_ADDR_SIZE);
    pFsRrd6ControlDestIpAddress->i4_Length = IP6_ADDR_SIZE;
    *pi4FsRrd6ControlNetMaskLen = pRtm6CtrlInfo->u1Prefixlen;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsRrd6ControlTable
 Input       :  The Indices
                FsRrd6ControlDestIpAddress
                nextFsRrd6ControlDestIpAddress
                FsRrd6ControlNetMaskLen
                nextFsRrd6ControlNetMaskLen
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsRrd6ControlTable (tSNMP_OCTET_STRING_TYPE *
                                   pFsRrd6ControlDestIpAddress,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pNextFsRrd6ControlDestIpAddress,
                                   INT4 i4FsRrd6ControlNetMaskLen,
                                   INT4 *pi4NextFsRrd6ControlNetMaskLen)
{
    tRrd6ControlInfo   *pRtm6CtrlInfo = NULL;
    tRrd6ControlInfo   *pNextRtm6CtrlInfo;
    tRrd6ControlInfo   *pTempRtm6CtrlInfo = NULL;
    tRtm6Cxt           *pRtm6Cxt = gRtm6GlobalInfo.pRtm6Cxt;

    if (Rtm6UtilGetGblRrd6Status () == OSIX_FALSE)
    {
        return SNMP_FAILURE;
    }

    if (pRtm6Cxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if (i4FsRrd6ControlNetMaskLen < 0)
    {
        return SNMP_FAILURE;
    }

    TMO_SLL_Scan (&(pRtm6Cxt->Rtm6CtrlList), pRtm6CtrlInfo, tRrd6ControlInfo *)
    {
        /* Check whether the IP address and mask match */
        if ((MEMCMP (pRtm6CtrlInfo->DestNet.u1_addr,
                     pFsRrd6ControlDestIpAddress->pu1_OctetList,
                     pFsRrd6ControlDestIpAddress->i4_Length) == 0) &&
            (pRtm6CtrlInfo->u1Prefixlen == (UINT1) i4FsRrd6ControlNetMaskLen))

        {
            /* We found the current node in the list */
            break;
        }

        /* If the given entry is not matching, store the
         * entry which is greater than the given one */
        if ((MEMCMP (pRtm6CtrlInfo->DestNet.u1_addr,
                     pFsRrd6ControlDestIpAddress->pu1_OctetList,
                     sizeof (tIp6Addr)) >= 0) &&
            (pRtm6CtrlInfo->u1Prefixlen > (UINT1) i4FsRrd6ControlNetMaskLen))
        {
            /* We found next node in the list */
            pTempRtm6CtrlInfo = pRtm6CtrlInfo;
            break;
        }
    }

    if (pRtm6CtrlInfo == NULL)
    {
        RTM6_TRC (pRtm6Cxt->u4ContextId, RTM6_ALL_FAILURE_TRC, RTM6_MOD_NAME,
                  "\tNo Matching Control Table entry is Present.\n");
        return SNMP_FAILURE;
    }

    if (pTempRtm6CtrlInfo == NULL)
    {
        pNextRtm6CtrlInfo =
            (tRrd6ControlInfo *) TMO_SLL_Next (&(pRtm6Cxt->Rtm6CtrlList),
                                               &(pRtm6CtrlInfo->pNext));
    }
    else
    {
        pNextRtm6CtrlInfo = pTempRtm6CtrlInfo;
    }

    if (pNextRtm6CtrlInfo == NULL)
    {
        /* No further entries in the table */
        RTM6_TRC (pRtm6Cxt->u4ContextId, RTM6_ALL_FAILURE_TRC, RTM6_MOD_NAME,
                  "\tNext Control Table entry is not Present.\n");
        return SNMP_FAILURE;
    }

    /* Fill up the next index */
    MEMCPY (pNextFsRrd6ControlDestIpAddress->pu1_OctetList,
            pNextRtm6CtrlInfo->DestNet.u1_addr, IP6_ADDR_SIZE);
    pNextFsRrd6ControlDestIpAddress->i4_Length = IP6_ADDR_SIZE;
    *pi4NextFsRrd6ControlNetMaskLen = pNextRtm6CtrlInfo->u1Prefixlen;
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsRrd6ControlSourceProto
 Input       :  The Indices
                FsRrd6ControlDestIpAddress
                FsRrd6ControlNetMaskLen

                The Object 
                retValFsRrd6ControlSourceProto
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRrd6ControlSourceProto (tSNMP_OCTET_STRING_TYPE *
                                pFsRrd6ControlDestIpAddress,
                                INT4 i4FsRrd6ControlNetMaskLen,
                                INT4 *pi4RetValFsRrd6ControlSourceProto)
{
    tRrd6ControlInfo   *pRtm6CtrlInfo = NULL;
    tRtm6Cxt           *pRtm6Cxt = gRtm6GlobalInfo.pRtm6Cxt;

    if (Rtm6UtilGetGblRrd6Status () == OSIX_FALSE)
    {
        return SNMP_SUCCESS;
    }

    if (pRtm6Cxt == NULL)
    {
        return SNMP_FAILURE;
    }

    TMO_SLL_Scan (&(pRtm6Cxt->Rtm6CtrlList), pRtm6CtrlInfo, tRrd6ControlInfo *)
    {
        /* Check whether the IP address and mask match */
        if ((MEMCMP (pRtm6CtrlInfo->DestNet.u1_addr,
                     pFsRrd6ControlDestIpAddress->pu1_OctetList,
                     pFsRrd6ControlDestIpAddress->i4_Length) == 0) &&
            (pRtm6CtrlInfo->u1Prefixlen == (UINT1) i4FsRrd6ControlNetMaskLen))
        {
            /* We found the current node in the list */
            break;
        }
    }

    if (pRtm6CtrlInfo == NULL)
    {
        /* No such index */
        RTM6_TRC (pRtm6Cxt->u4ContextId, RTM6_ALL_FAILURE_TRC, RTM6_MOD_NAME,
                  "\tNo Matching Control Table entry is Present.\n");
        return SNMP_FAILURE;
    }
    *pi4RetValFsRrd6ControlSourceProto = (INT4) pRtm6CtrlInfo->u1SrcProtocolId;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRrd6ControlDestProto
 Input       :  The Indices
                FsRrd6ControlDestIpAddress
                FsRrd6ControlNetMaskLen

                The Object 
                retValFsRrd6ControlDestProto
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRrd6ControlDestProto (tSNMP_OCTET_STRING_TYPE *
                              pFsRrd6ControlDestIpAddress,
                              INT4 i4FsRrd6ControlNetMaskLen,
                              INT4 *pi4RetValFsRrd6ControlDestProto)
{
    tRrd6ControlInfo   *pRtm6CtrlInfo = NULL;
    tRtm6Cxt           *pRtm6Cxt = gRtm6GlobalInfo.pRtm6Cxt;

    if (Rtm6UtilGetGblRrd6Status () == OSIX_FALSE)
    {
        return SNMP_SUCCESS;
    }

    if (pRtm6Cxt == NULL)
    {
        return SNMP_FAILURE;
    }

    TMO_SLL_Scan (&(pRtm6Cxt->Rtm6CtrlList), pRtm6CtrlInfo, tRrd6ControlInfo *)
    {
        /* Check whether the IP address and mask match */
        if ((MEMCMP (pRtm6CtrlInfo->DestNet.u1_addr,
                     pFsRrd6ControlDestIpAddress->pu1_OctetList,
                     pFsRrd6ControlDestIpAddress->i4_Length) == 0) &&
            (pRtm6CtrlInfo->u1Prefixlen == (UINT1) i4FsRrd6ControlNetMaskLen))
        {
            /* We found the current node in the list */
            break;
        }
    }

    if (pRtm6CtrlInfo == NULL)
    {
        /* No such index */
        RTM6_TRC (pRtm6Cxt->u4ContextId, RTM6_ALL_FAILURE_TRC, RTM6_MOD_NAME,
                  "\tNo Matching Control Table entry is Present.\n");
        return SNMP_FAILURE;
    }
    *pi4RetValFsRrd6ControlDestProto = (INT4) pRtm6CtrlInfo->u2DestProtocolMask;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRrd6ControlRouteExportFlag
 Input       :  The Indices
                FsRrd6ControlDestIpAddress
                FsRrd6ControlNetMaskLen

                The Object 
                retValFsRrd6ControlRouteExportFlag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRrd6ControlRouteExportFlag (tSNMP_OCTET_STRING_TYPE *
                                    pFsRrd6ControlDestIpAddress,
                                    INT4 i4FsRrd6ControlNetMaskLen,
                                    INT4 *pi4RetValFsRrd6ControlRouteExportFlag)
{
    tRrd6ControlInfo   *pRtm6CtrlInfo = NULL;
    tRtm6Cxt           *pRtm6Cxt = gRtm6GlobalInfo.pRtm6Cxt;

    if (Rtm6UtilGetGblRrd6Status () == OSIX_FALSE)
    {
        return SNMP_SUCCESS;
    }

    if (pRtm6Cxt == NULL)
    {
        return SNMP_FAILURE;
    }

    TMO_SLL_Scan (&(pRtm6Cxt->Rtm6CtrlList), pRtm6CtrlInfo, tRrd6ControlInfo *)
    {
        /* Check whether the IP address and mask match */
        if ((MEMCMP (pRtm6CtrlInfo->DestNet.u1_addr,
                     pFsRrd6ControlDestIpAddress->pu1_OctetList,
                     pFsRrd6ControlDestIpAddress->i4_Length) == 0) &&
            (pRtm6CtrlInfo->u1Prefixlen == (UINT1) i4FsRrd6ControlNetMaskLen))
        {
            /* We found the current node in the list */
            break;
        }
    }

    if (pRtm6CtrlInfo == NULL)
    {
        /* No such index */
        RTM6_TRC (pRtm6Cxt->u4ContextId, RTM6_ALL_FAILURE_TRC, RTM6_MOD_NAME,
                  "\tNo Matching Control Table entry is Present.\n");
        return SNMP_FAILURE;
    }
    *pi4RetValFsRrd6ControlRouteExportFlag =
        (INT4) pRtm6CtrlInfo->u1RtExportFlag;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRrd6ControlRowStatus
 Input       :  The Indices
                FsRrd6ControlDestIpAddress
                FsRrd6ControlNetMaskLen

                The Object 
                retValFsRrd6ControlRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRrd6ControlRowStatus (tSNMP_OCTET_STRING_TYPE *
                              pFsRrd6ControlDestIpAddress,
                              INT4 i4FsRrd6ControlNetMaskLen,
                              INT4 *pi4RetValFsRrd6ControlRowStatus)
{
    tRrd6ControlInfo   *pRtm6CtrlInfo = NULL;
    tRtm6Cxt           *pRtm6Cxt = gRtm6GlobalInfo.pRtm6Cxt;

    if (Rtm6UtilGetGblRrd6Status () == OSIX_FALSE)
    {
        return SNMP_SUCCESS;
    }

    if (pRtm6Cxt == NULL)
    {
        return SNMP_FAILURE;
    }

    TMO_SLL_Scan (&(pRtm6Cxt->Rtm6CtrlList), pRtm6CtrlInfo, tRrd6ControlInfo *)
    {
        /* Check whether the IP address and mask match */
        if ((MEMCMP (pRtm6CtrlInfo->DestNet.u1_addr,
                     pFsRrd6ControlDestIpAddress->pu1_OctetList,
                     pFsRrd6ControlDestIpAddress->i4_Length) == 0) &&
            (pRtm6CtrlInfo->u1Prefixlen == (UINT1) i4FsRrd6ControlNetMaskLen))
        {
            /* We found the current node in the list */
            break;
        }
    }

    if (pRtm6CtrlInfo == NULL)
    {
        /* No such index */
        RTM6_TRC (pRtm6Cxt->u4ContextId, RTM6_ALL_FAILURE_TRC, RTM6_MOD_NAME,
                  "\tNo Matching Control Table entry is Present.\n");
        return SNMP_FAILURE;
    }
    *pi4RetValFsRrd6ControlRowStatus = (INT4) pRtm6CtrlInfo->u1RowStatus;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsRrd6ControlSourceProto
 Input       :  The Indices
                FsRrd6ControlDestIpAddress
                FsRrd6ControlNetMaskLen

                The Object 
                setValFsRrd6ControlSourceProto
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRrd6ControlSourceProto (tSNMP_OCTET_STRING_TYPE *
                                pFsRrd6ControlDestIpAddress,
                                INT4 i4FsRrd6ControlNetMaskLen,
                                INT4 i4SetValFsRrd6ControlSourceProto)
{
    tRrd6ControlInfo   *pRtm6CtrlInfo;
    tRtm6Cxt           *pRtm6Cxt = gRtm6GlobalInfo.pRtm6Cxt;

    if (Rtm6UtilGetGblRrd6Status () == OSIX_FALSE)
    {
        return SNMP_SUCCESS;
    }

    if (pRtm6Cxt == NULL)
    {
        return SNMP_FAILURE;
    }

    TMO_SLL_Scan (&(pRtm6Cxt->Rtm6CtrlList), pRtm6CtrlInfo, tRrd6ControlInfo *)
    {
        /* Check whether the IP address and mask match */
        if ((MEMCMP (pRtm6CtrlInfo->DestNet.u1_addr,
                     pFsRrd6ControlDestIpAddress->pu1_OctetList,
                     pFsRrd6ControlDestIpAddress->i4_Length) == 0) &&
            (pRtm6CtrlInfo->u1Prefixlen == (UINT1) i4FsRrd6ControlNetMaskLen))
        {
            /* We found the current node in the list */
            break;
        }
    }

    if (pRtm6CtrlInfo == NULL)
    {
        /* No such index */
        RTM6_TRC (pRtm6Cxt->u4ContextId, RTM6_ALL_FAILURE_TRC, RTM6_MOD_NAME,
                  "\tNo Matching Control Table entry is Present.\n");
        return SNMP_FAILURE;
    }

    if (pRtm6CtrlInfo->u1SrcProtocolId ==
        (UINT1) i4SetValFsRrd6ControlSourceProto)
    {
        return SNMP_SUCCESS;
    }

    pRtm6CtrlInfo->u1SrcProtocolId = (UINT1) i4SetValFsRrd6ControlSourceProto;
    UtilRtm6IncMsrForRrd6ControlTable (pRtm6Cxt->u4ContextId,
                                       pFsRrd6ControlDestIpAddress,
                                       i4FsRrd6ControlNetMaskLen,
                                       i4SetValFsRrd6ControlSourceProto,
                                       FsMIRrd6ControlSourceProto,
                                       (sizeof (FsMIRrd6ControlSourceProto) /
                                        sizeof (UINT4)), FALSE);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsRrd6ControlDestProto
 Input       :  The Indices
                FsRrd6ControlDestIpAddress
                FsRrd6ControlNetMaskLen

                The Object 
                setValFsRrd6ControlDestProto
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRrd6ControlDestProto (tSNMP_OCTET_STRING_TYPE *
                              pFsRrd6ControlDestIpAddress,
                              INT4 i4FsRrd6ControlNetMaskLen,
                              INT4 i4SetValFsRrd6ControlDestProto)
{
    tRrd6ControlInfo   *pRtm6CtrlInfo = NULL;
    tRtm6Cxt           *pRtm6Cxt = gRtm6GlobalInfo.pRtm6Cxt;

    if (Rtm6UtilGetGblRrd6Status () == OSIX_FALSE)
    {
        return SNMP_SUCCESS;
    }

    if (pRtm6Cxt == NULL)
    {
        return SNMP_FAILURE;
    }

    TMO_SLL_Scan (&(pRtm6Cxt->Rtm6CtrlList), pRtm6CtrlInfo, tRrd6ControlInfo *)
    {
        /* Check whether the IP address and mask match */
        if ((MEMCMP (pRtm6CtrlInfo->DestNet.u1_addr,
                     pFsRrd6ControlDestIpAddress->pu1_OctetList,
                     pFsRrd6ControlDestIpAddress->i4_Length) == 0) &&
            (pRtm6CtrlInfo->u1Prefixlen == (UINT1) i4FsRrd6ControlNetMaskLen))
        {
            /* We found the current node in the list */
            break;
        }
    }

    if (pRtm6CtrlInfo == NULL)
    {
        /* No such index */
        RTM6_TRC (pRtm6Cxt->u4ContextId, RTM6_ALL_FAILURE_TRC, RTM6_MOD_NAME,
                  "\tNo Matching Control Table entry is Present.\n");
        return SNMP_FAILURE;
    }

    if (pRtm6CtrlInfo->u2DestProtocolMask
        == (UINT2) i4SetValFsRrd6ControlDestProto)
    {
        return SNMP_SUCCESS;
    }

    pRtm6CtrlInfo->u2DestProtocolMask = (UINT2) i4SetValFsRrd6ControlDestProto;
    UtilRtm6IncMsrForRrd6ControlTable (pRtm6Cxt->u4ContextId,
                                       pFsRrd6ControlDestIpAddress,
                                       i4FsRrd6ControlNetMaskLen,
                                       i4SetValFsRrd6ControlDestProto,
                                       FsMIRrd6ControlDestProto,
                                       (sizeof (FsMIRrd6ControlDestProto) /
                                        sizeof (UINT4)), FALSE);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsRrd6ControlRouteExportFlag
 Input       :  The Indices
                FsRrd6ControlDestIpAddress
                FsRrd6ControlNetMaskLen

                The Object 
                setValFsRrd6ControlRouteExportFlag
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRrd6ControlRouteExportFlag (tSNMP_OCTET_STRING_TYPE *
                                    pFsRrd6ControlDestIpAddress,
                                    INT4 i4FsRrd6ControlNetMaskLen,
                                    INT4 i4SetValFsRrd6ControlRouteExportFlag)
{
    tRrd6ControlInfo   *pRtm6CtrlInfo = NULL;
    tIp6Buf            *pBuf = NULL;
    tRtm6SnmpIfMsg     *pRtm6Parms = NULL;
    UINT1               u1TempIpAddr[IP6_ADDR_SIZE];
    tRtm6Cxt           *pRtm6Cxt = gRtm6GlobalInfo.pRtm6Cxt;

    if (Rtm6UtilGetGblRrd6Status () == OSIX_FALSE)
    {
        return SNMP_SUCCESS;
    }

    if (pRtm6Cxt == NULL)
    {
        return SNMP_FAILURE;
    }

    MEMSET (u1TempIpAddr, 0, IP6_ADDR_SIZE);

    TMO_SLL_Scan (&(pRtm6Cxt->Rtm6CtrlList), pRtm6CtrlInfo, tRrd6ControlInfo *)
    {
        /* Check whether the IP address and mask match */
        if ((MEMCMP (pRtm6CtrlInfo->DestNet.u1_addr,
                     pFsRrd6ControlDestIpAddress->pu1_OctetList,
                     pFsRrd6ControlDestIpAddress->i4_Length) == 0) &&
            (pRtm6CtrlInfo->u1Prefixlen == (UINT1) i4FsRrd6ControlNetMaskLen))
        {
            /* We found the current node in the list */
            break;
        }
    }

    if (pRtm6CtrlInfo == NULL)
    {
        /* No such index */
        RTM6_TRC (pRtm6Cxt->u4ContextId, RTM6_ALL_FAILURE_TRC, RTM6_MOD_NAME,
                  "\tNo Matching Control Table entry is Present.\n");
        return SNMP_FAILURE;
    }

    if ((MEMCMP (pRtm6CtrlInfo->DestNet.u1_addr,
                 u1TempIpAddr, IP6_ADDR_SIZE) == 0) &&
        (i4FsRrd6ControlNetMaskLen == IP6_ADDR_MAX_PREFIX) &&
        (pRtm6CtrlInfo->u1RowStatus == RTM6_ACTIVE))
    {
        /* Change in the Control Table mode for ALL ROUTE Entry. Post a
         * message to RTM6 Task to handle this event. */
        if ((pBuf = IP6_ALLOCATE_BUF (sizeof (UINT4), 0)) == NULL)
        {
            RTM6_TRC (pRtm6Cxt->u4ContextId, RTM6_ALL_FAILURE_TRC |
                      RTM6_MANAGEMENT_TRC | RTM6_OS_RESOURCE_TRC, RTM6_MOD_NAME,
                      "\tMemory Allocation for Posting RTM6 Control Table "
                      "Export Flag Change for ALL ROUTE Entry FAILED!!!\n");
            return SNMP_FAILURE;
        }

        pRtm6Parms = (tRtm6SnmpIfMsg *) IP6_GET_MODULE_DATA_PTR (pBuf);
        pRtm6Parms->u1Cmd = RTM6_CHG_IN_RRD6_DEF_CTRL_MODE;
        pRtm6Parms->pRtm6Filter = pRtm6CtrlInfo;

        switch (i4SetValFsRrd6ControlRouteExportFlag)
        {
            case RTM6_ROUTE_PERMIT:
                pRtm6Parms->u1PermitOrDeny = RTM6_ROUTE_PERMIT;
                break;

            case RTM6_ROUTE_DENY:
                /* The row will be actually destroyed in RTM6 after
                   route redistribution to the routing protocols. */

                pRtm6Parms->u1PermitOrDeny = RTM6_ROUTE_DENY;
                break;

            default:
                IP6_RELEASE_BUF (pBuf, FALSE);
                return SNMP_FAILURE;
        }

        if (OsixSendToQ (SELF,
                         (const UINT1 *) RTM6_SNMP_IF_Q_NAME,
                         pBuf, OSIX_MSG_NORMAL) != OSIX_SUCCESS)
        {
            RTM6_TRC (pRtm6Cxt->u4ContextId, RTM6_ALL_FAILURE_TRC |
                      RTM6_MANAGEMENT_TRC, RTM6_MOD_NAME,
                      "\tPosting RTM6 Control Table Export Flag Change for "
                      "ALL ROUTE Entry FAILED!!!\n");
            IP6_RELEASE_BUF (pBuf, FALSE);
            return (SNMP_FAILURE);
        }

        OsixSendEvent (SELF, (const UINT1 *) RTM6_MAIN_TASK_NAME,
                       RTM6_FILTER_ADDED_EVENT);
        return SNMP_SUCCESS;
    }
    else
    {
        pRtm6CtrlInfo->u1RtExportFlag =
            (UINT1) i4SetValFsRrd6ControlRouteExportFlag;
    }
    UtilRtm6IncMsrForRrd6ControlTable (pRtm6Cxt->u4ContextId,
                                       pFsRrd6ControlDestIpAddress,
                                       i4FsRrd6ControlNetMaskLen,
                                       i4SetValFsRrd6ControlRouteExportFlag,
                                       FsMIRrd6ControlRouteExportFlag,
                                       (sizeof (FsMIRrd6ControlRouteExportFlag)
                                        / sizeof (UINT4)), FALSE);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsRrd6ControlRowStatus
 Input       :  The Indices
                FsRrd6ControlDestIpAddress
                FsRrd6ControlNetMaskLen

                The Object 
                setValFsRrd6ControlRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRrd6ControlRowStatus (tSNMP_OCTET_STRING_TYPE *
                              pFsRrd6ControlDestIpAddress,
                              INT4 i4FsRrd6ControlNetMaskLen,
                              INT4 i4SetValFsRrd6ControlRowStatus)
{
    tRrd6ControlInfo   *pRtm6CtrlInfo = NULL;
    tRrd6ControlInfo   *pPrevRtm6Info = NULL;
    tRrd6ControlInfo   *pCurrRtm6Info = NULL;
    tIp6Buf            *pBuf = NULL;
    tRtm6SnmpIfMsg     *pRtm6Parms = NULL;
    tRtm6Cxt           *pRtm6Cxt = gRtm6GlobalInfo.pRtm6Cxt;

    if (Rtm6UtilGetGblRrd6Status () == OSIX_FALSE)
    {
        return SNMP_SUCCESS;
    }

    if (pRtm6Cxt == NULL)
    {
        return SNMP_FAILURE;
    }

    TMO_SLL_Scan (&(pRtm6Cxt->Rtm6CtrlList), pRtm6CtrlInfo, tRrd6ControlInfo *)
    {
        /* Check whether the IP address and mask match */
        if ((MEMCMP (pRtm6CtrlInfo->DestNet.u1_addr,
                     pFsRrd6ControlDestIpAddress->pu1_OctetList,
                     pFsRrd6ControlDestIpAddress->i4_Length) == 0) &&
            (pRtm6CtrlInfo->u1Prefixlen == (UINT1) i4FsRrd6ControlNetMaskLen))
        {
            /* We found the current node in the list */
            break;
        }
    }

    if (pRtm6CtrlInfo == NULL)
    {
        /* The Row status could be CREATE AND WAIT */
        /* we need to create the node and add it to the list */

        if (RTM6_CONTROL_INFO_ALLOC (pRtm6CtrlInfo) == NULL)
        {
            /* Malloc failed */
            RTM6_TRC (pRtm6Cxt->u4ContextId, RTM6_ALL_FAILURE_TRC |
                      RTM6_MANAGEMENT_TRC | RTM6_OS_RESOURCE_TRC, RTM6_MOD_NAME,
                      "\tMemory Allocation for Creating RTM6 Control Table "
                      "Entry FAILED!!!\n");
            return SNMP_FAILURE;
        }

        /* Initialise all the variables in the RRD6 Control info */
        MEMSET (pRtm6CtrlInfo->DestNet.u1_addr, 0, IP6_ADDR_SIZE);
        MEMCPY (pRtm6CtrlInfo->DestNet.u1_addr,
                pFsRrd6ControlDestIpAddress->pu1_OctetList,
                pFsRrd6ControlDestIpAddress->i4_Length);
        pRtm6CtrlInfo->u1Prefixlen = (UINT1) i4FsRrd6ControlNetMaskLen;
        pRtm6CtrlInfo->u1SrcProtocolId = RTM6_ANY_PROTOCOL;
        pRtm6CtrlInfo->u2DestProtocolMask = RTM6_ANY_PROTOCOL;
        pRtm6CtrlInfo->u1RtExportFlag = RTM6_ROUTE_UNKNOWN_MODE;
        pRtm6CtrlInfo->u4RouteTag = 0;
        pRtm6CtrlInfo->pRtm6Cxt = pRtm6Cxt;
        pRtm6CtrlInfo->u1RowStatus = RTM6_NOT_IN_SERVICE;

        /* Maintain the linked list in the sorted order - starting from
         * Highest Dest value to Lowest Dest Value. This will ensure that
         * if a match is found while scanning through the control table,
         * longest prefix match will be hit first. */
        TMO_SLL_Scan (&(pRtm6Cxt->Rtm6CtrlList), pCurrRtm6Info,
                      tRrd6ControlInfo *)
        {
            if (Ip6IsAddrGreater (&pCurrRtm6Info->DestNet,
                                  &pRtm6CtrlInfo->DestNet) == IP6_SUCCESS)
            {
                break;
            }

            if ((Ip6AddrMatch (&pRtm6CtrlInfo->DestNet,
                               &pCurrRtm6Info->DestNet,
                               IP6_ADDR_SIZE_IN_BITS) == TRUE) &&
                (pRtm6CtrlInfo->u1Prefixlen < pCurrRtm6Info->u1Prefixlen))
            {
                /* Address Matches and Existing control table entry is
                 * a super set of the new entry. So add the new entry
                 * before the current one. */
                break;
            }
            pPrevRtm6Info = pCurrRtm6Info;
        }

        TMO_SLL_Insert (&(pRtm6Cxt->Rtm6CtrlList),
                        &(pPrevRtm6Info->pNext), &(pRtm6CtrlInfo->pNext));

    }
    else
    {
        /* The entry already exists. */
        /* Either the administrator wants to delete the row or to make
         * the row active. */

        /* Send an event to the RTM6 task indicating the 
           addition of new filter. This is done to update the 
           export list of the protocols. Export list updation is
           a costly operation and it involves scanning through
           the routing table and export list. If it is updated 
           here then the SNMP manager may timeout before getting
           the response for the set operation. */

        if ((pBuf = IP6_ALLOCATE_BUF (sizeof (UINT4), 0)) == NULL)
        {
            RTM6_TRC (pRtm6Cxt->u4ContextId, RTM6_ALL_FAILURE_TRC |
                      RTM6_MANAGEMENT_TRC | RTM6_OS_RESOURCE_TRC, RTM6_MOD_NAME,
                      "\tMemory Allocation for Posting RTM6 Control Table "
                      "Entry Status Change FAILED!!!\n");
            return SNMP_FAILURE;
        }

        pRtm6Parms = (tRtm6SnmpIfMsg *) IP6_GET_MODULE_DATA_PTR (pBuf);
        pRtm6Parms->u1Cmd = RTM6_CHG_IN_RRD6_CONTROL_TABLE;
        pRtm6Parms->pRtm6Filter = pRtm6CtrlInfo;

        switch (i4SetValFsRrd6ControlRowStatus)
        {
            case RTM6_ACTIVE:
                pRtm6Parms->u1PermitOrDeny = RTM6_ACTIVE;
                break;

            case RTM6_DESTROY:
                /* The row will be actually destroyed in RTM6 after
                   route redistribution to the routing protocols. */
                pRtm6Parms->u1PermitOrDeny = RTM6_DESTROY;
                break;

            default:
                IP6_RELEASE_BUF (pBuf, FALSE);
                return SNMP_FAILURE;
        }

        if (OsixSendToQ (SELF,
                         (const UINT1 *) RTM6_SNMP_IF_Q_NAME,
                         pBuf, OSIX_MSG_NORMAL) != OSIX_SUCCESS)
        {
            RTM6_TRC (pRtm6Cxt->u4ContextId, RTM6_ALL_FAILURE_TRC |
                      RTM6_MANAGEMENT_TRC, RTM6_MOD_NAME,
                      "\tPosting RTM6 Control Table Entry Status Change "
                      "FAILED!!!\n");
            IP6_RELEASE_BUF (pBuf, FALSE);
            return (SNMP_FAILURE);
        }

        OsixSendEvent (SELF, (const UINT1 *) RTM6_MAIN_TASK_NAME,
                       RTM6_FILTER_ADDED_EVENT);
    }
    UtilRtm6IncMsrForRrd6ControlTable (pRtm6Cxt->u4ContextId,
                                       pFsRrd6ControlDestIpAddress,
                                       i4FsRrd6ControlNetMaskLen,
                                       i4SetValFsRrd6ControlRowStatus,
                                       FsMIRrd6ControlRowStatus,
                                       (sizeof (FsMIRrd6ControlRowStatus) /
                                        sizeof (UINT4)), TRUE);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsRrd6ControlSourceProto
 Input       :  The Indices
                FsRrd6ControlDestIpAddress
                FsRrd6ControlNetMaskLen

                The Object 
                testValFsRrd6ControlSourceProto
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRrd6ControlSourceProto (UINT4 *pu4ErrorCode,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsRrd6ControlDestIpAddress,
                                   INT4 i4FsRrd6ControlNetMaskLen,
                                   INT4 i4TestValFsRrd6ControlSourceProto)
{
    tRrd6ControlInfo   *pRtm6CtrlInfo = NULL;
    INT1                i1RetVal = SNMP_FAILURE;
    tRtm6Cxt           *pRtm6Cxt = gRtm6GlobalInfo.pRtm6Cxt;

    if (Rtm6UtilGetGblRrd6Status () == OSIX_FALSE)
    {
        return SNMP_SUCCESS;
    }

    if (pRtm6Cxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_RRD6_NOENTRY_CTRL_TABLE);
        return SNMP_FAILURE;
    }

    TMO_SLL_Scan (&(pRtm6Cxt->Rtm6CtrlList), pRtm6CtrlInfo, tRrd6ControlInfo *)
    {
        /* Check whether the IP address and mask match */
        if ((MEMCMP (pRtm6CtrlInfo->DestNet.u1_addr,
                     pFsRrd6ControlDestIpAddress->pu1_OctetList,
                     pFsRrd6ControlDestIpAddress->i4_Length) == 0) &&
            (pRtm6CtrlInfo->u1Prefixlen == (UINT1) i4FsRrd6ControlNetMaskLen))
        {
            /* We found the correct node in the list */
            break;
        }
    }

    if (pRtm6CtrlInfo == NULL)
    {
        /* No such index */
        RTM6_TRC (pRtm6Cxt->u4ContextId, RTM6_ALL_FAILURE_TRC |
                  RTM6_MANAGEMENT_TRC, RTM6_MOD_NAME,
                  "\tTesting RTM6 Control Table Source Proto Failed - "
                  "No matching entry found.\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_RRD6_NOENTRY_CTRL_TABLE);
        return SNMP_FAILURE;
    }

    /* Source protocol should be local, static, RIP, OSPF or BGP */
    switch (i4TestValFsRrd6ControlSourceProto)
    {
        case RTM6_ANY_PROTOCOL:
        case IP6_LOCAL_PROTOID:
        case IP6_NETMGMT_PROTOID:
        case RIPNG_ID:
        case OSPF6_ID:
        case BGP6_ID:
            i1RetVal = SNMP_SUCCESS;
            break;

        default:
            RTM6_TRC (pRtm6Cxt->u4ContextId, RTM6_ALL_FAILURE_TRC |
                      RTM6_MANAGEMENT_TRC, RTM6_MOD_NAME,
                      "\tTesting RTM6 Control Table Source Proto Failed - "
                      "Invalid Source Protocol.\n");
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_RRD6_INVALID_SRCPROTO);
            break;
    }
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsRrd6ControlDestProto
 Input       :  The Indices
                FsRrd6ControlDestIpAddress
                FsRrd6ControlNetMaskLen

                The Object 
                testValFsRrd6ControlDestProto
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRrd6ControlDestProto (UINT4 *pu4ErrorCode,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pFsRrd6ControlDestIpAddress,
                                 INT4 i4FsRrd6ControlNetMaskLen,
                                 INT4 i4TestValFsRrd6ControlDestProto)
{
    tRrd6ControlInfo   *pRtm6CtrlInfo = NULL;
    INT1                i1RetVal = SNMP_FAILURE;
    tRtm6Cxt           *pRtm6Cxt = gRtm6GlobalInfo.pRtm6Cxt;

    if (Rtm6UtilGetGblRrd6Status () == OSIX_FALSE)
    {
        return SNMP_SUCCESS;
    }

    if (pRtm6Cxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_RRD6_NOENTRY_CTRL_TABLE);
        return SNMP_FAILURE;
    }

    TMO_SLL_Scan (&(pRtm6Cxt->Rtm6CtrlList), pRtm6CtrlInfo, tRrd6ControlInfo *)
    {
        /* Check whether the IP address and mask match */
        if ((MEMCMP (pRtm6CtrlInfo->DestNet.u1_addr,
                     pFsRrd6ControlDestIpAddress->pu1_OctetList,
                     pFsRrd6ControlDestIpAddress->i4_Length) == 0) &&
            (pRtm6CtrlInfo->u1Prefixlen == (UINT1) i4FsRrd6ControlNetMaskLen))
        {
            /* We found the correct node in the list */
            break;
        }
    }

    if (pRtm6CtrlInfo == NULL)
    {
        /* No such index */
        RTM6_TRC (pRtm6Cxt->u4ContextId, RTM6_ALL_FAILURE_TRC |
                  RTM6_MANAGEMENT_TRC, RTM6_MOD_NAME,
                  "\tTesting RTM6 Control Table Destination Proto Failed - "
                  "No matching entry found.\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_RRD6_NOENTRY_CTRL_TABLE);
        return SNMP_FAILURE;
    }

    /* Destination Protocol can be RIP or OSPF or BGP or ANY */
    if ((i4TestValFsRrd6ControlDestProto == RTM6_ANY_PROTOCOL) ||
        (i4TestValFsRrd6ControlDestProto & RTM6_ALL_RPS_MASK) ||
        (i4TestValFsRrd6ControlDestProto & RTM6_OSPF_AND_RIP_MASK) ||
        (i4TestValFsRrd6ControlDestProto & RTM6_BGP_AND_OSPF_MASK) ||
        (i4TestValFsRrd6ControlDestProto & RTM6_BGP_AND_RIP_MASK))
    {
        i1RetVal = SNMP_SUCCESS;
    }
    else
    {
        RTM6_TRC (pRtm6Cxt->u4ContextId, RTM6_ALL_FAILURE_TRC |
                  RTM6_MANAGEMENT_TRC, RTM6_MOD_NAME,
                  "\tTesting RTM6 Control Table Destination Proto Failed - "
                  "Invalid Destination Protocol Mask.\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_RRD6_INVALID_DESTPROTO);
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsRrd6ControlRouteExportFlag
 Input       :  The Indices
                FsRrd6ControlDestIpAddress
                FsRrd6ControlNetMaskLen

                The Object 
                testValFsRrd6ControlRouteExportFlag
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRrd6ControlRouteExportFlag (UINT4 *pu4ErrorCode,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFsRrd6ControlDestIpAddress,
                                       INT4 i4FsRrd6ControlNetMaskLen,
                                       INT4
                                       i4TestValFsRrd6ControlRouteExportFlag)
{
    tRrd6ControlInfo   *pRtm6CtrlInfo = NULL;
    INT1                i1RetVal = SNMP_FAILURE;
    UINT1               u1TempIpAddr[IP6_ADDR_SIZE];
    tRtm6Cxt           *pRtm6Cxt = gRtm6GlobalInfo.pRtm6Cxt;

    if (Rtm6UtilGetGblRrd6Status () == OSIX_FALSE)
    {
        return SNMP_SUCCESS;
    }

    if (pRtm6Cxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_RRD6_NOENTRY_CTRL_TABLE);
        return SNMP_FAILURE;
    }

    MEMSET (u1TempIpAddr, 0, IP6_ADDR_SIZE);

    TMO_SLL_Scan (&(pRtm6Cxt->Rtm6CtrlList), pRtm6CtrlInfo, tRrd6ControlInfo *)
    {
        /* Check whether the IP address and mask match */
        if ((MEMCMP (pRtm6CtrlInfo->DestNet.u1_addr,
                     pFsRrd6ControlDestIpAddress->pu1_OctetList,
                     pFsRrd6ControlDestIpAddress->i4_Length) == 0) &&
            (pRtm6CtrlInfo->u1Prefixlen == (UINT1) i4FsRrd6ControlNetMaskLen))
        {
            /* We found the correct node in the list */
            break;
        }
    }

    if (pRtm6CtrlInfo == NULL)
    {
        /* No such index */
        RTM6_TRC (pRtm6Cxt->u4ContextId, RTM6_ALL_FAILURE_TRC |
                  RTM6_MANAGEMENT_TRC, RTM6_MOD_NAME,
                  "\tTesting RTM6 Control Table Export Flag Failed - "
                  "No matching entry found.\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_RRD6_NOENTRY_CTRL_TABLE);
        return SNMP_FAILURE;
    }

    if ((i4TestValFsRrd6ControlRouteExportFlag == RTM6_ROUTE_PERMIT) ||
        (i4TestValFsRrd6ControlRouteExportFlag == RTM6_ROUTE_DENY))
    {
        /* the values are ok */
        /* check whether the Route Export flag is set or not. For all entries
         * other than the ALL ROUTE entry, setting Export Flag is possible
         * only once.
         */
        if ((MEMCMP (u1TempIpAddr,
                     pFsRrd6ControlDestIpAddress->pu1_OctetList,
                     pFsRrd6ControlDestIpAddress->i4_Length) != 0) &&
            (pRtm6CtrlInfo->u1RtExportFlag != RTM6_ROUTE_UNKNOWN_MODE))
        {
            /* Entry is already configured. */
            if (pRtm6CtrlInfo->u1RtExportFlag ==
                i4TestValFsRrd6ControlRouteExportFlag)
            {
                return SNMP_SUCCESS;
            }
            else
            {
                RTM6_TRC (pRtm6Cxt->u4ContextId, RTM6_ALL_FAILURE_TRC |
                          RTM6_MANAGEMENT_TRC, RTM6_MOD_NAME,
                          "\tTesting RTM6 Control Table Export Flag Failed - "
                          "Entry Mode already configured.\n");
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
        }
        else
        {
            i1RetVal = SNMP_SUCCESS;
        }
    }
    else
    {
        RTM6_TRC (pRtm6Cxt->u4ContextId, RTM6_ALL_FAILURE_TRC |
                  RTM6_MANAGEMENT_TRC, RTM6_MOD_NAME,
                  "\tTesting RTM6 Control Table Export Flag Failed - "
                  "Invalid Export Flag.\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_RRD6_INVALID_INPUT);
    }
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsRrd6ControlRowStatus
 Input       :  The Indices
                FsRrd6ControlDestIpAddress
                FsRrd6ControlNetMaskLen

                The Object 
                testValFsRrd6ControlRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRrd6ControlRowStatus (UINT4 *pu4ErrorCode,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pFsRrd6ControlDestIpAddress,
                                 INT4 i4FsRrd6ControlNetMaskLen,
                                 INT4 i4TestValFsRrd6ControlRowStatus)
{
    tRrd6ControlInfo   *pRtm6CtrlInfo = NULL;
    UINT4               u4EntryCnt = 0;
    INT1                i1RetVal = SNMP_FAILURE;
    tRtm6Cxt           *pRtm6Cxt = gRtm6GlobalInfo.pRtm6Cxt;

    if (Rtm6UtilGetGblRrd6Status () == OSIX_FALSE)
    {
        return SNMP_SUCCESS;
    }

    if (pRtm6Cxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    /* Validate the Mask Length */
    if ((i4FsRrd6ControlNetMaskLen < 0) ||
        (i4FsRrd6ControlNetMaskLen > IP6_ADDR_SIZE_IN_BITS))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_RRD6_INVALID_INPUT);
        return SNMP_FAILURE;
    }

    TMO_SLL_Scan (&(pRtm6Cxt->Rtm6CtrlList), pRtm6CtrlInfo, tRrd6ControlInfo *)
    {
        u4EntryCnt++;
        /* Check whether the IP address and mask match */
        if ((MEMCMP (pRtm6CtrlInfo->DestNet.u1_addr,
                     pFsRrd6ControlDestIpAddress->pu1_OctetList,
                     pFsRrd6ControlDestIpAddress->i4_Length) == 0) &&
            (pRtm6CtrlInfo->u1Prefixlen == (UINT1) i4FsRrd6ControlNetMaskLen))
        {
            /* We found the correct node in the list */
            break;
        }
    }

    if (pRtm6CtrlInfo == NULL)
    {
        /* check whether the Row status is create and wait */
        if (i4TestValFsRrd6ControlRowStatus == RTM6_CREATE_AND_WAIT)
        {
            i1RetVal = SNMP_SUCCESS;
        }
        else
        {
            RTM6_TRC (pRtm6Cxt->u4ContextId, RTM6_ALL_FAILURE_TRC |
                      RTM6_MANAGEMENT_TRC, RTM6_MOD_NAME,
                      "\tTesting RTM6 Control Table Export Flag Failed - "
                      "Invalid Row Status value.\n");
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_RRD6_NOENTRY_CTRL_TABLE);
        }
    }
    else
    {
        /* The entry already exists */
        /* either the status should be active or destroy */
        if ((i4TestValFsRrd6ControlRowStatus == RTM6_ACTIVE) ||
            (i4TestValFsRrd6ControlRowStatus == RTM6_DESTROY))
        {
            i1RetVal = SNMP_SUCCESS;
        }
        else
        {
            RTM6_TRC (pRtm6Cxt->u4ContextId, RTM6_ALL_FAILURE_TRC |
                      RTM6_MANAGEMENT_TRC, RTM6_MOD_NAME,
                      "\tTesting RTM6 Control Table Export Flag Failed - "
                      "Invalid Row Status value.\n");
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_RRD6_INVALID_INPUT);
        }
    }
    return i1RetVal;
}

/* LOW LEVEL Routines for Table : FsRrd6RoutingProtoTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsRrd6RoutingProtoTable
 Input       :  The Indices
                FsRrd6RoutingProtoId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsRrd6RoutingProtoTable (INT4 i4FsRrd6RoutingProtoId)
{
    tRtm6RegnId         RegnId;
    UINT2               u2Index = 0;
    tRtm6Cxt           *pRtm6Cxt = gRtm6GlobalInfo.pRtm6Cxt;

    if (Rtm6UtilGetGblRrd6Status () == OSIX_FALSE)
    {
        return SNMP_SUCCESS;
    }

    if (pRtm6Cxt == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Check whether this routing protocol has registered with RTM6 or not */
    RegnId.u2ProtoId = (UINT2) i4FsRrd6RoutingProtoId;
    RegnId.u4ContextId = pRtm6Cxt->u4ContextId;
    u2Index = Rtm6GetRegnIndexFromRegnIdInCxt (pRtm6Cxt, &RegnId);

    if (u2Index == RTM6_INVALID_REGN_ID)
    {
        RTM6_TRC (pRtm6Cxt->u4ContextId, RTM6_ALL_FAILURE_TRC |
                  RTM6_MANAGEMENT_TRC, RTM6_MOD_NAME,
                  "\tInvalid Routing Proto Table Index.\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsRrd6ControlTable
 Input       :  The Indices
                FsRrd6ControlDestIpAddress
                FsRrd6ControlNetMaskLen
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRrd6ControlTable (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsRrd6RoutingProtoTable
 Input       :  The Indices
                FsRrd6RoutingProtoId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsRrd6RoutingProtoTable (INT4 *pi4FsRrd6RoutingProtoId)
{
    tRtm6Cxt           *pRtm6Cxt = gRtm6GlobalInfo.pRtm6Cxt;

    if (Rtm6UtilGetGblRrd6Status () == OSIX_FALSE)
    {
        return SNMP_FAILURE;
    }

    if (pRtm6Cxt == NULL)
    {
        return SNMP_FAILURE;
    }
    /* Check whether this routing protocol has registered with RTM6 or not */
    /* RTM6 assumes the registration of LOCAL or DIRECT ROUTES First and 
       no one will deregister the local route. Local route will be registered
       first and hence there will not be any updation in the start index */

    if (pRtm6Cxt->u2Rtm6RtStartIndex < IP6_MAX_ROUTING_PROTOCOLS)
    {
        if (pRtm6Cxt->aRtm6RegnTable[pRtm6Cxt->u2Rtm6RtStartIndex].
            u2RoutingProtocolId != RTM6_INVALID_ROUTING_PROTOCOL_ID)
        {
            *pi4FsRrd6RoutingProtoId =
                pRtm6Cxt->aRtm6RegnTable[pRtm6Cxt->u2Rtm6RtStartIndex].
                u2RoutingProtocolId;

            return SNMP_SUCCESS;
        }
    }

    /* No routing protocol has registered with RTM6 till now */
    RTM6_TRC (pRtm6Cxt->u4ContextId, RTM6_ALL_FAILURE_TRC |
              RTM6_MANAGEMENT_TRC, RTM6_MOD_NAME,
              "\tNo matching entry found in Routing Proto Table.\n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsRrd6RoutingProtoTable
 Input       :  The Indices
                FsRrd6RoutingProtoId
                nextFsRrd6RoutingProtoId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsRrd6RoutingProtoTable (INT4 i4FsRrd6RoutingProtoId,
                                        INT4 *pi4NextFsRrd6RoutingProtoId)
{
    UINT2               u2Index;
    UINT2               u2NextRpId = IP6_MAX_ROUTING_PROTOCOLS;
    tRtm6Cxt           *pRtm6Cxt = gRtm6GlobalInfo.pRtm6Cxt;

    if (Rtm6UtilGetGblRrd6Status () == OSIX_FALSE)
    {
        return SNMP_FAILURE;
    }

    if (pRtm6Cxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if (i4FsRrd6RoutingProtoId < 0)
    {
        return SNMP_FAILURE;
    }

    /* Check whether this routing protocol has registered with RTM6 or not */

    for (u2Index = pRtm6Cxt->u2Rtm6RtStartIndex;
         u2Index < IP6_MAX_ROUTING_PROTOCOLS;
         u2Index = pRtm6Cxt->aRtm6RegnTable[u2Index].u2NextRegId)
    {
        if (pRtm6Cxt->aRtm6RegnTable[u2Index].u2RoutingProtocolId
            > i4FsRrd6RoutingProtoId)
        {
            /* this routing protocol has registered with RTM6 */
            if (pRtm6Cxt->aRtm6RegnTable[u2Index].u2RoutingProtocolId
                < u2NextRpId)
            {
                u2NextRpId =
                    pRtm6Cxt->aRtm6RegnTable[u2Index].u2RoutingProtocolId;
            }
        }
    }

    if (u2NextRpId != IP6_MAX_ROUTING_PROTOCOLS)
    {
        *pi4NextFsRrd6RoutingProtoId = (INT4) u2NextRpId;
        return SNMP_SUCCESS;
    }

    RTM6_TRC (pRtm6Cxt->u4ContextId, RTM6_ALL_FAILURE_TRC |
              RTM6_MANAGEMENT_TRC, RTM6_MOD_NAME,
              "\tNo more matching entry found in Routing Proto Table.\n");
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsRrd6RoutingRegnId
 Input       :  The Indices
                FsRrd6RoutingProtoId

                The Object 
                retValFsRrd6RoutingRegnId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRrd6RoutingRegnId (INT4 i4FsRrd6RoutingProtoId,
                           INT4 *pi4RetValFsRrd6RoutingRegnId)
{
    /* Check whether this routing protocol has registered with RTM6 or not */
    tRtm6RegnId         RegnId;
    tRtm6Cxt           *pRtm6Cxt = gRtm6GlobalInfo.pRtm6Cxt;

    if (Rtm6UtilGetGblRrd6Status () == OSIX_FALSE)
    {
        return SNMP_SUCCESS;
    }

    if (pRtm6Cxt == NULL)
    {
        return SNMP_FAILURE;
    }

    RegnId.u2ProtoId = (UINT2) i4FsRrd6RoutingProtoId;
    RegnId.u4ContextId = pRtm6Cxt->u4ContextId;
    *pi4RetValFsRrd6RoutingRegnId =
        (INT4) Rtm6GetRegnIndexFromRegnIdInCxt (pRtm6Cxt, &RegnId);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRrd6RoutingProtoTaskIdent
 Input       :  The Indices
                FsRrd6RoutingProtoId

                The Object 
                retValFsRrd6RoutingProtoTaskIdent
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRrd6RoutingProtoTaskIdent (INT4 i4FsRrd6RoutingProtoId,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pRetValFsRrd6RoutingProtoTaskIdent)
{
    tRtm6RegnId         RegnId;
    UINT2               u2Index;
    tRtm6Cxt           *pRtm6Cxt = gRtm6GlobalInfo.pRtm6Cxt;

    if (Rtm6UtilGetGblRrd6Status () == OSIX_FALSE)
    {
        return SNMP_SUCCESS;
    }

    if (pRtm6Cxt == NULL)
    {
        return SNMP_FAILURE;
    }

    RegnId.u2ProtoId = (UINT2) i4FsRrd6RoutingProtoId;
    RegnId.u4ContextId = pRtm6Cxt->u4ContextId;
    u2Index = Rtm6GetRegnIndexFromRegnIdInCxt (pRtm6Cxt, &RegnId);
    if (u2Index == RTM6_INVALID_REGN_ID)
    {
        return SNMP_FAILURE;
    }
    STRNCPY (pRetValFsRrd6RoutingProtoTaskIdent->pu1_OctetList,
             pRtm6Cxt->aRtm6RegnTable[u2Index].au1TaskName,
             RTM6_MAX_TASK_NAME_LEN);

    pRetValFsRrd6RoutingProtoTaskIdent->i4_Length = RTM6_MAX_TASK_NAME_LEN;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRrd6RoutingProtoQueueIdent
 Input       :  The Indices
                FsRrd6RoutingProtoId

                The Object 
                retValFsRrd6RoutingProtoQueueIdent
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRrd6RoutingProtoQueueIdent (INT4 i4FsRrd6RoutingProtoId,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pRetValFsRrd6RoutingProtoQueueIdent)
{
    tRtm6RegnId         RegnId;
    UINT2               u2Index = 0;
    tRtm6Cxt           *pRtm6Cxt = gRtm6GlobalInfo.pRtm6Cxt;

    if (Rtm6UtilGetGblRrd6Status () == OSIX_FALSE)
    {
        return SNMP_SUCCESS;
    }

    if (pRtm6Cxt == NULL)
    {
        return SNMP_FAILURE;
    }

    RegnId.u2ProtoId = (UINT2) i4FsRrd6RoutingProtoId;
    RegnId.u4ContextId = pRtm6Cxt->u4ContextId;
    u2Index = Rtm6GetRegnIndexFromRegnIdInCxt (pRtm6Cxt, &RegnId);
    if (u2Index == RTM6_INVALID_REGN_ID)
    {
        return SNMP_FAILURE;
    }
    STRNCPY (pRetValFsRrd6RoutingProtoQueueIdent->pu1_OctetList,
             pRtm6Cxt->aRtm6RegnTable[u2Index].au1QName, RTM6_MAX_Q_NAME_LEN);

    pRetValFsRrd6RoutingProtoQueueIdent->i4_Length = RTM6_MAX_Q_NAME_LEN;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRrd6AllowOspfAreaRoutes
 Input       :  The Indices
                FsRrd6RoutingProtoId

                The Object 
                retValFsRrd6AllowOspfAreaRoutes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRrd6AllowOspfAreaRoutes (INT4 i4FsRrd6RoutingProtoId,
                                 INT4 *pi4RetValFsRrd6AllowOspfAreaRoutes)
{
    tRtm6RegnId         RegnId;
    UINT2               u2Index = 0;
    tRtm6Cxt           *pRtm6Cxt = gRtm6GlobalInfo.pRtm6Cxt;

    if (Rtm6UtilGetGblRrd6Status () == OSIX_FALSE)
    {
        return SNMP_SUCCESS;
    }

    if (pRtm6Cxt == NULL)
    {
        return SNMP_FAILURE;
    }

    RegnId.u2ProtoId = (UINT2) i4FsRrd6RoutingProtoId;
    RegnId.u4ContextId = pRtm6Cxt->u4ContextId;
    u2Index = Rtm6GetRegnIndexFromRegnIdInCxt (pRtm6Cxt, &RegnId);
    if (u2Index == RTM6_INVALID_REGN_ID)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsRrd6AllowOspfAreaRoutes =
        (INT4) pRtm6Cxt->aRtm6RegnTable[u2Index].u1AllowOspfInternals;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRrd6AllowOspfExtRoutes
 Input       :  The Indices
                FsRrd6RoutingProtoId

                The Object 
                retValFsRrd6AllowOspfExtRoutes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRrd6AllowOspfExtRoutes (INT4 i4FsRrd6RoutingProtoId,
                                INT4 *pi4RetValFsRrd6AllowOspfExtRoutes)
{
    tRtm6RegnId         RegnId;
    UINT2               u2Index = 0;
    tRtm6Cxt           *pRtm6Cxt = gRtm6GlobalInfo.pRtm6Cxt;

    if (Rtm6UtilGetGblRrd6Status () == OSIX_FALSE)
    {
        return SNMP_SUCCESS;
    }

    if (pRtm6Cxt == NULL)
    {
        return SNMP_FAILURE;
    }

    RegnId.u4ContextId = pRtm6Cxt->u4ContextId;
    RegnId.u2ProtoId = (UINT2) i4FsRrd6RoutingProtoId;
    u2Index = Rtm6GetRegnIndexFromRegnIdInCxt (pRtm6Cxt, &RegnId);
    if (u2Index == RTM6_INVALID_REGN_ID)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsRrd6AllowOspfExtRoutes =
        (INT4) pRtm6Cxt->aRtm6RegnTable[u2Index].u1AllowOspfExternals;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsRrd6AllowOspfAreaRoutes
 Input       :  The Indices
                FsRrd6RoutingProtoId

                The Object 
                setValFsRrd6AllowOspfAreaRoutes
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRrd6AllowOspfAreaRoutes (INT4 i4FsRrd6RoutingProtoId,
                                 INT4 i4SetValFsRrd6AllowOspfAreaRoutes)
{
    tIp6Buf            *pBuf = NULL;
    tRtm6SnmpIfMsg     *pRtm6Parms = NULL;
    tRtm6Cxt           *pRtm6Cxt = gRtm6GlobalInfo.pRtm6Cxt;

    if (Rtm6UtilGetGblRrd6Status () == OSIX_FALSE)
    {
        return SNMP_SUCCESS;
    }

    if (pRtm6Cxt == NULL)
    {
        return SNMP_FAILURE;
    }

    /* the set routine will be called whenever there is change in the allow
       of internal routes - enabled to disabled or disabled to enabled */

    if ((pBuf = IP6_ALLOCATE_BUF (sizeof (UINT4), 0)) == NULL)
    {
        RTM6_TRC (pRtm6Cxt->u4ContextId, RTM6_ALL_FAILURE_TRC |
                  RTM6_MANAGEMENT_TRC | RTM6_OS_RESOURCE_TRC, RTM6_MOD_NAME,
                  "\tMemory Allocation for Rrd6AllowOspfAreaRoute Event "
                  "FAILED!!!\n");
        return SNMP_FAILURE;
    }

    pRtm6Parms = (tRtm6SnmpIfMsg *) IP6_GET_MODULE_DATA_PTR (pBuf);
    pRtm6Parms->u1Cmd = RTM6_OSPF_INT_EXT_CHG;
    pRtm6Parms->RegnId.u2ProtoId = (UINT1) i4FsRrd6RoutingProtoId;
    pRtm6Parms->RegnId.u4ContextId = pRtm6Cxt->u4ContextId;
    pRtm6Parms->u1OspfRtType = RTM6_OSPF_INTERNAL_ROUTES;    /* can be inter area
                                                               or inter area */
    pRtm6Parms->u1PermitOrDeny = (UINT1) i4SetValFsRrd6AllowOspfAreaRoutes;

    if (OsixSendToQ (SELF, (const UINT1 *) RTM6_SNMP_IF_Q_NAME,
                     pBuf, OSIX_MSG_NORMAL) != OSIX_SUCCESS)
    {
        RTM6_TRC (pRtm6Cxt->u4ContextId, RTM6_ALL_FAILURE_TRC |
                  RTM6_MANAGEMENT_TRC, RTM6_MOD_NAME,
                  "\tPosting message for Rrd6AllowOspfAreaRoute Event "
                  "FAILED!!!\n");
        IP6_RELEASE_BUF (pBuf, FALSE);
        return (SNMP_FAILURE);
    }

    OsixSendEvent (SELF, (const UINT1 *) RTM6_MAIN_TASK_NAME,
                   RTM6_FILTER_ADDED_EVENT);

    UtilRtm6IncMsrForRrd6ProtoTable (pRtm6Cxt->u4ContextId,
                                     i4FsRrd6RoutingProtoId,
                                     i4SetValFsRrd6AllowOspfAreaRoutes,
                                     FsMIRrd6AllowOspfAreaRoutes,
                                     (sizeof (FsMIRrd6AllowOspfAreaRoutes) /
                                      sizeof (UINT4)));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsRrd6AllowOspfExtRoutes
 Input       :  The Indices
                FsRrd6RoutingProtoId

                The Object 
                setValFsRrd6AllowOspfExtRoutes
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRrd6AllowOspfExtRoutes (INT4 i4FsRrd6RoutingProtoId,
                                INT4 i4SetValFsRrd6AllowOspfExtRoutes)
{
    tIp6Buf            *pBuf = NULL;
    tRtm6SnmpIfMsg     *pRtm6Parms = NULL;
    tRtm6Cxt           *pRtm6Cxt = gRtm6GlobalInfo.pRtm6Cxt;

    if (Rtm6UtilGetGblRrd6Status () == OSIX_FALSE)
    {
        return SNMP_SUCCESS;
    }

    if (pRtm6Cxt == NULL)
    {
        return SNMP_FAILURE;
    }

    /* the set routine will be called whenever there is change in the allow
       of internal routes - enabled to disabled or disabled to enabled */

    if ((pBuf = IP6_ALLOCATE_BUF (sizeof (UINT4), 0)) == NULL)
    {
        RTM6_TRC (pRtm6Cxt->u4ContextId, RTM6_ALL_FAILURE_TRC |
                  RTM6_MANAGEMENT_TRC | RTM6_OS_RESOURCE_TRC, RTM6_MOD_NAME,
                  "\tMemory Allocation for Rrd6AllowOspfExtRoute Event "
                  "FAILED!!!\n");
        return SNMP_FAILURE;
    }

    pRtm6Parms = (tRtm6SnmpIfMsg *) IP6_GET_MODULE_DATA_PTR (pBuf);
    pRtm6Parms->u1Cmd = RTM6_OSPF_INT_EXT_CHG;
    pRtm6Parms->RegnId.u2ProtoId = (UINT1) i4FsRrd6RoutingProtoId;
    pRtm6Parms->RegnId.u4ContextId = pRtm6Cxt->u4ContextId;
    pRtm6Parms->u1OspfRtType = RTM6_OSPF_EXTERNAL_ROUTES;    /* can be inter area
                                                               or inter area */
    pRtm6Parms->u1PermitOrDeny = (UINT1) i4SetValFsRrd6AllowOspfExtRoutes;

    if (OsixSendToQ (SELF, (const UINT1 *) RTM6_SNMP_IF_Q_NAME,
                     pBuf, OSIX_MSG_NORMAL) != OSIX_SUCCESS)
    {
        RTM6_TRC (pRtm6Cxt->u4ContextId, RTM6_ALL_FAILURE_TRC |
                  RTM6_MANAGEMENT_TRC, RTM6_MOD_NAME,
                  "\tPosting message for Rrd6AllowOspfExtRoute Event "
                  "FAILED!!!\n");
        IP6_RELEASE_BUF (pBuf, FALSE);
        return (SNMP_FAILURE);
    }

    OsixSendEvent (SELF, (const UINT1 *) RTM6_MAIN_TASK_NAME,
                   RTM6_FILTER_ADDED_EVENT);

    UtilRtm6IncMsrForRrd6ProtoTable (pRtm6Cxt->u4ContextId,
                                     i4FsRrd6RoutingProtoId,
                                     i4SetValFsRrd6AllowOspfExtRoutes,
                                     FsMIRrd6AllowOspfExtRoutes,
                                     (sizeof (FsMIRrd6AllowOspfExtRoutes) /
                                      sizeof (UINT4)));
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsRrd6AllowOspfAreaRoutes
 Input       :  The Indices
                FsRrd6RoutingProtoId

                The Object 
                testValFsRrd6AllowOspfAreaRoutes
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRrd6AllowOspfAreaRoutes (UINT4 *pu4ErrorCode,
                                    INT4 i4FsRrd6RoutingProtoId,
                                    INT4 i4TestValFsRrd6AllowOspfAreaRoutes)
{
    tRtm6RegnId         RegnId;
    UINT2               u2Index = 0;
    INT1                i1RetVal = SNMP_FAILURE;
    tRtm6Cxt           *pRtm6Cxt = gRtm6GlobalInfo.pRtm6Cxt;

    if (Rtm6UtilGetGblRrd6Status () == OSIX_FALSE)
    {
        return SNMP_SUCCESS;
    }

    if (pRtm6Cxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsRrd6AllowOspfAreaRoutes != RTM6_REDISTRIBUTION_ENABLED) &&
        (i4TestValFsRrd6AllowOspfAreaRoutes != RTM6_REDISTRIBUTION_DISABLED))
    {
        RTM6_TRC (pRtm6Cxt->u4ContextId, RTM6_ALL_FAILURE_TRC |
                  RTM6_MANAGEMENT_TRC, RTM6_MOD_NAME,
                  "\tTesting Rrd6AllowOspfAreaRoute FAILED. Invalid "
                  "Value.\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_RRD6_INVALID_INPUT);
        return i1RetVal;
    }

    RegnId.u2ProtoId = (UINT2) i4FsRrd6RoutingProtoId;
    RegnId.u4ContextId = pRtm6Cxt->u4ContextId;
    u2Index = Rtm6GetRegnIndexFromRegnIdInCxt (pRtm6Cxt, &RegnId);

    if (u2Index == RTM6_INVALID_REGN_ID)
    {
        /* this protocol not registered with RTM6 */
        RTM6_TRC (pRtm6Cxt->u4ContextId, RTM6_ALL_FAILURE_TRC |
                  RTM6_MANAGEMENT_TRC, RTM6_MOD_NAME,
                  "\tTesting Rrd6AllowOspfAreaRoute FAILED. Protocol "
                  "not registered.\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_RRD6_NO_REDISTRIBUTE);
        return SNMP_FAILURE;
    }

    switch (i4FsRrd6RoutingProtoId)
    {
        case RIPNG_ID:
        case BGP6_ID:
            i1RetVal = SNMP_SUCCESS;
            break;
        default:
            RTM6_TRC (pRtm6Cxt->u4ContextId, RTM6_ALL_FAILURE_TRC |
                      RTM6_MANAGEMENT_TRC, RTM6_MOD_NAME,
                      "\tTesting Rrd6AllowOspfAreaRoute FAILED. Invalid "
                      "Protocol.\n");
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_RRD6_INVALID_DESTPROTO);
            break;
    }
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsRrd6AllowOspfExtRoutes
 Input       :  The Indices
                FsRrd6RoutingProtoId

                The Object 
                testValFsRrd6AllowOspfExtRoutes
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRrd6AllowOspfExtRoutes (UINT4 *pu4ErrorCode,
                                   INT4 i4FsRrd6RoutingProtoId,
                                   INT4 i4TestValFsRrd6AllowOspfExtRoutes)
{
    tRtm6RegnId         RegnId;
    UINT2               u2Index = 0;
    INT1                i1RetVal = SNMP_FAILURE;
    tRtm6Cxt           *pRtm6Cxt = gRtm6GlobalInfo.pRtm6Cxt;

    if (Rtm6UtilGetGblRrd6Status () == OSIX_FALSE)
    {
        return SNMP_SUCCESS;
    }

    if (pRtm6Cxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsRrd6AllowOspfExtRoutes != RTM6_REDISTRIBUTION_ENABLED) &&
        (i4TestValFsRrd6AllowOspfExtRoutes != RTM6_REDISTRIBUTION_DISABLED))
    {
        RTM6_TRC (pRtm6Cxt->u4ContextId, RTM6_ALL_FAILURE_TRC |
                  RTM6_MANAGEMENT_TRC, RTM6_MOD_NAME,
                  "\tTesting Rrd6AllowOspfExtRoute FAILED. Invalid "
                  "Value.\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_RRD6_NO_REDISTRIBUTE);
        return i1RetVal;
    }

    RegnId.u2ProtoId = (UINT2) i4FsRrd6RoutingProtoId;
    RegnId.u4ContextId = pRtm6Cxt->u4ContextId;
    u2Index = Rtm6GetRegnIndexFromRegnIdInCxt (pRtm6Cxt, &RegnId);

    if (u2Index == RTM6_INVALID_REGN_ID)
    {
        /* this protocol not registered with RTM6 */
        RTM6_TRC (pRtm6Cxt->u4ContextId, RTM6_ALL_FAILURE_TRC |
                  RTM6_MANAGEMENT_TRC, RTM6_MOD_NAME,
                  "\tTesting Rrd6AllowOspfExtRoute FAILED. Protocol "
                  "not registered.\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_RRD6_PROTO_NOREG);
        return SNMP_FAILURE;
    }

    switch (i4FsRrd6RoutingProtoId)
    {
        case RIPNG_ID:
        case BGP6_ID:
            i1RetVal = SNMP_SUCCESS;
            break;
        default:
            RTM6_TRC (pRtm6Cxt->u4ContextId, RTM6_ALL_FAILURE_TRC |
                      RTM6_MANAGEMENT_TRC, RTM6_MOD_NAME,
                      "\tTesting Rrd6AllowOspfExtRoute FAILED. Invalid "
                      "Protocol.\n");
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_RRD6_INVALID_DESTPROTO);
            break;
    }
    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsRrd6RoutingProtoTable
 Input       :  The Indices
                FsRrd6RoutingProtoId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRrd6RoutingProtoTable (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsRtm6RedEntryTime
 Input       :  The Indices

                The Object 
                retValFsRtm6RedEntryTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRtm6RedEntryTime (INT4 *pi4RetValFsRtm6RedEntryTime)
{
    *pi4RetValFsRtm6RedEntryTime = gRtm6RedGlobalInfo.i4Rtm6RedEntryTime;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRtm6RedExitTime
 Input       :  The Indices

                The Object 
                retValFsRtm6RedExitTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRtm6RedExitTime (INT4 *pi4RetValFsRtm6RedExitTime)
{
    *pi4RetValFsRtm6RedExitTime = gRtm6RedGlobalInfo.i4Rtm6RedExitTime;
    return SNMP_SUCCESS;
}
