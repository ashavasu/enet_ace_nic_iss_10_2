/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rtm6main.c,v 1.58.2.1 2018/03/09 13:40:47 siva Exp $
 *
 * Description:This file contains functions which process   
 *             the messages from the routing protocols.     
 *
 *******************************************************************/

#include "rtm6inc.h"
#include "rtm6red.h"
#include "rtm6frt.h"

tRtm6SystemSize     gRtm6SystemSize;
                        /* RTM6 System Size Parameter */
tRtm6GlobalInfo     gRtm6GlobalInfo;
                        /* RTM6 Global Structure */
tRtm6RedGlobalInfo  gRtm6RedGlobalInfo;
extern UINT4        gu4ECMP6TmrFlag;

/*-------------------------------------------------------------------+
 *
 * Function           : Rtm6Init 
 *
 * Input(s)           : None 
 *
 * Output(s)          : None
 *
 * Returns            : None
 *
 * Action             : This function initialises the global data structures of
 *                      of RTM6.
 *         
+-------------------------------------------------------------------*/
VOID
Rtm6Init ()
{
    /* Initialise the Route Redistribution Handler List */
    TMO_SLL_Init (&gRtm6GlobalInfo.Rtm6RedisInitList);
    gRtm6GlobalInfo.u4ThrotLimit = RTM6_MAX_PROTOMASK_ROUTES;
    gRtm6GlobalInfo.u4GlobalTrcFlag = 0;
    gRtm6GlobalInfo.u1OneSecTimerStart = FALSE;
    gRtm6GlobalInfo.u1OneSecTmrStartReq = FALSE;
    gRtm6GlobalInfo.Rtm6RtTblPoolId = 0;
    gRtm6GlobalInfo.Rtm6CxtPoolId = 0;
    gRtm6GlobalInfo.Rtm6RrdCrtlPoolId = 0;
    gRtm6GlobalInfo.Rtm6RtMsgPoolId = 0;
    gRtm6GlobalInfo.bRrd6Status = RTM6_GLB_STATUS;
    gRtm6GlobalInfo.Rtm6CxtPoolId = RTM6MemPoolIds[MAX_RTM6_CONTEXT_SIZING_ID];
    gRtm6GlobalInfo.Rtm6RmapCommunityPoolId =
        RTM6MemPoolIds[MAX_RTM6_COMM_SIZING_ID];
    gRtm6GlobalInfo.Rtm6RtTblPoolId =
        RTM6MemPoolIds[MAX_RTM6_ROUTE_TABLE_ENTRIES_SIZING_ID];
    gRtm6GlobalInfo.Rtm6RrdCrtlPoolId =
        RTM6MemPoolIds[MAX_RTM6_RRD6_CONTROL_INFO_SIZING_ID];
    gRtm6GlobalInfo.Rtm6RedisNodePoolId =
        RTM6MemPoolIds[MAX_RTM6_REDIST_NODE_SIZING_ID];
    gRtm6GlobalInfo.Rtm6RedRMPoolId =
        RTM6MemPoolIds[MAX_RTM6_RM_QUE_SIZE_SIZING_ID];
    gRtm6GlobalInfo.Rtm6RedDynMsgPoolId =
        RTM6MemPoolIds[MAX_RTM6_DYN_MSG_SIZE_SIZING_ID];
    gRtm6GlobalInfo.Rtm6PNextHopPoolId =
        RTM6MemPoolIds[MAX_RTM6_PEND_NEXTHOP_SIZING_ID];
    gRtm6GlobalInfo.Rtm6RNextHopPoolId =
        RTM6MemPoolIds[MAX_RTM6_RESLV_NEXTHOP_SIZING_ID];
    gRtm6GlobalInfo.Rtm6URRtPoolId =
        RTM6MemPoolIds[MAX_RTM6_PEND_RT_ENTRIES_SIZING_ID];
    gRtm6GlobalInfo.Rtm6FrtPoolId =
        RTM6MemPoolIds[MAX_RTM6_FRT_MSG_SIZE_SIZING_ID];
    gRtm6GlobalInfo.Rtm6RouteNextHopPoolId =
        RTM6MemPoolIds[MAX_RTM6_ALL_NEXTHOP_ENTRIES_SIZING_ID];
    gRtm6GlobalInfo.Rtm6RtMsgPoolId =
        RTM6MemPoolIds[MAX_RTM6_ROUTE_MSG_SIZING_ID];

    gRtm6GlobalInfo.u4MaxRTM6BgpRoute = DEF_RTM6_BGP_ROUTE_ENTRIES;
    gRtm6GlobalInfo.u4MaxRTM6OspfRoute = DEF_RTM6_OSPF_ROUTE_ENTRIES;
    gRtm6GlobalInfo.u4MaxRTM6StaticRoute = DEF_RTM6_STATIC_ROUTE_ENTRIES;
    gRtm6GlobalInfo.u4MaxRTM6RipRoute = DEF_RTM6_RIP_ROUTE_ENTRIES;
    gRtm6GlobalInfo.u4MaxRTM6IsisRoute = DEF_RTM6_ISIS_ROUTE_ENTRIES;

    gRtm6GlobalInfo.u4BgpRts = IP6_ZERO;
    gRtm6GlobalInfo.u4RipRts = IP6_ZERO;
    gRtm6GlobalInfo.u4OspfRts = IP6_ZERO;
    gRtm6GlobalInfo.u4StaticRts = IP6_ZERO;
    gRtm6GlobalInfo.u4IsisRts = IP6_ZERO;

    /* For initializing the redundancy related variables and creating the
     * queues and timers for redundancy */
    Rtm6RedInitGlobalInfo ();
    Rtm6FrtInitGlobalInfo ();
    return;
}

/*-------------------------------------------------------------------+
 *
 * Function           : Rtm6InitControlTableToDenyModeInCxt
 *
 * Input(s)           : pRtm6Cxt - RTM6 Context pointer
 *
 * Output(s)          : None
 *
 * Returns            : None
 *
 * Action             : This function initialises the Rtm6 Control table to 
 *                      Deny mode allowing all the routes for RRD6 except 
 *                      0.0.0.0 route.
 *         
+-------------------------------------------------------------------*/
VOID
Rtm6InitControlTableToDenyModeInCxt (tRtm6Cxt * pRtm6Cxt)
{
    tRrd6ControlInfo   *pRtm6CtrlInfo = NULL;

    if (RTM6_CONTROL_INFO_ALLOC (pRtm6CtrlInfo) == NULL)
    {
        /* Malloc failed */
        RTM6_TRC (pRtm6Cxt->u4ContextId,
                  RTM6_ALL_FAILURE_TRC | RTM6_OS_RESOURCE_TRC, RTM6_MOD_NAME,
                  "\tMemory Allocation for Initializing RTM6 Control Table "
                  "FAILED!!!\n");
        return;
    }
    /* Initialise all the variables in the RRD6 Control info */
    MEMSET (&pRtm6CtrlInfo->DestNet, 0, sizeof (tIp6Addr));
    pRtm6CtrlInfo->u1Prefixlen = IP6_ADDR_MAX_PREFIX;
    pRtm6CtrlInfo->u1SrcProtocolId = RTM6_ANY_PROTOCOL;
    pRtm6CtrlInfo->u2DestProtocolMask = RTM6_ANY_PROTOCOL;
    pRtm6CtrlInfo->u1RtExportFlag = RTM6_ROUTE_PERMIT;
    pRtm6CtrlInfo->u4RouteTag = 0;
    pRtm6CtrlInfo->u1RowStatus = RTM6_ACTIVE;
    pRtm6CtrlInfo->pRtm6Cxt = pRtm6Cxt;

    TMO_SLL_Insert (&(pRtm6Cxt->Rtm6CtrlList),
                    (tTMO_SLL_NODE *) NULL, &(pRtm6CtrlInfo->pNext));

    return;
}

/*-------------------------------------------------------------------+
 *
 * Function           : Rtm6ProcessOneSecTimerExpiry
 *
 * Input(s)           : None 
 *
 * Output(s)          : None
 *
 * Returns            : None
 *
 * Action             : This function process the One second timer expiry
 *                      event.
 *         
+-------------------------------------------------------------------*/
VOID
Rtm6ProcessOneSecTimerExpiry (VOID)
{
    /* Check if there is any work is pending or not. If any work is
     * pending then process it. */
    Rtm6ProcessTick ();
}

/*-------------------------------------------------------------------+
 *
 * Function           : Rtm6ProcessTick
 *
 * Input(s)           : None 
 *
 * Output(s)          : None
 *
 * Returns            : None.
 *
 * Action             : This function is RTM6 work horse function. RTM6
 *                      has got many functionalities to be performed
 *                      which will hog the CPU for a long time. Such
 *                      functions should be broken up such that the
 *                      whole functionality should be performed in
 *                      small chuncks and thus avoiding CPU hog. These
 *                      function can be performed as a part of the
 *                      RTM6 Workhorse functions.
 *         
+-------------------------------------------------------------------*/
VOID
Rtm6ProcessTick (VOID)
{
    tRtm6RedisNode     *pNode = NULL;
    tRtm6RedisNode     *pTmpNode = NULL;
    tRtm6RegnId         RegnId;
    UINT2               u2RegnId = 0;
    tRtm6Cxt           *pRtm6Cxt = NULL;

    /* The various task that needs to be throttled are listed below:
     * 1) ProtoMask Enable or Disable */

    /* Process the ProtoMask Enable/Disable Event. */
    RTM6_SLL_DYN_Scan (&gRtm6GlobalInfo.Rtm6RedisInitList, pNode, pTmpNode,
                       tRtm6RedisNode *)
    {
        if ((pRtm6Cxt = UtilRtm6GetCxt (pNode->RegId.u4ContextId)) == NULL)
        {
            TMO_SLL_Delete (&gRtm6GlobalInfo.Rtm6RedisInitList,
                            &(pNode->NextNode));
            RTM6_REDIST_NODE_FREE (pNode);
        }
        else
        {
            if (Rtm6ProcessProtoMaskEvent (pNode) == RTM6_COMPLETE)
            {
                /* Completed processing the ProtoMask Event for this protocol.
                 * Remove the node from the list and reset the status of the
                 * protocol registration entry. */
                RegnId = pNode->RegId;
                TMO_SLL_Delete (&gRtm6GlobalInfo.Rtm6RedisInitList,
                                &(pNode->NextNode));

                u2RegnId = Rtm6GetRegnIndexFromRegnIdInCxt (pRtm6Cxt, &RegnId);
                RTM6_REDIST_NODE_FREE (pNode);
                if (u2RegnId < RTM6_INVALID_REGN_ID)
                {
                    pRtm6Cxt->aRtm6RegnTable[u2RegnId].u1State =
                        RTM6_MASK_STABLE;
                }
            }
            else
            {
                /* Starting the one-second timer */
                gRtm6GlobalInfo.u1OneSecTmrStartReq = TRUE;
            }
        }
    }
}

/*-------------------------------------------------------------------+
 *
 * Function           : Rtm6AddRedisEventInCxt
 *
 * Input(s)           : pRtm6Cxt - RTM6 context Pointer
 *                      pRegnId - Registriation Id for protocol for which
 *                                redistribution to be performed.
 *                      u4ProtoMask - Protocol Mask to be enabled or
 *                                    disabled.
 *                      u4Event - Enable or Disable Event.
 *
 * Output(s)          : None
 *
 * Returns            : RTM6_SUCCESS or RTM6_FAILURE
 *
 * Action             : This function will update the Redistribution list
 *                      for processing the given request.
 *         
+-------------------------------------------------------------------*/
INT4
Rtm6AddRedisEventInCxt (tRtm6Cxt * pRtm6Cxt, tRtm6RegnId * pRegnId,
                        UINT4 u4ProtoMask, UINT4 u4Event)
{
    tRtm6RedisNode     *pNode = NULL;
    UINT4               u4PendMask = 0;
    UINT2               u2RegnId = 0;

    u2RegnId = Rtm6GetRegnIndexFromRegnIdInCxt (pRtm6Cxt, pRegnId);
    if (u2RegnId == RTM6_INVALID_REGN_ID)
    {
        return RTM6_FAILURE;
    }

    if (pRtm6Cxt->aRtm6RegnTable[u2RegnId].u1State == RTM6_MASK_STABLE)
    {
        /* Allocate the Redistribute node and add it to the Redistribution
         * Handling List. Also reset the RTM6_MASK_STABLE flag */
        if (RTM6_REDIST_NODE_ALLOC (pNode) == NULL)
        {
            return RTM6_FAILURE;
        }
        TMO_SLL_Init_Node (&pNode->NextNode);
        pNode->RegId.u2ProtoId = pRegnId->u2ProtoId;
        pNode->RegId.u4ContextId = pRtm6Cxt->u4ContextId;
        TMO_SLL_Add (&gRtm6GlobalInfo.Rtm6RedisInitList, &(pNode->NextNode));
        pRtm6Cxt->aRtm6RegnTable[u2RegnId].u1State = 0;

        if (u4Event == RTM6_ENABLE_PROTO_MASK)
        {
            pRtm6Cxt->aRtm6RegnTable[u2RegnId].u1State =
                RTM6_MASK_ENABLE_INPROGRESS;
            pNode->u4EnableProtoMask = u4ProtoMask;
            pNode->u1Event = RTM6_ENABLE_PROTO_MASK;
        }
        else
        {
            pRtm6Cxt->aRtm6RegnTable[u2RegnId].u1State =
                RTM6_MASK_DISABLE_INPROGRESS;
            pNode->u4DisableProtoMask = u4ProtoMask;
            pNode->u1Event = RTM6_DISABLE_PROTO_MASK;
        }
    }
    else
    {
        /* Already Redistribution is in progress for some event. Try and get
         * the node from the list. */
        TMO_SLL_Scan (&gRtm6GlobalInfo.Rtm6RedisInitList, pNode,
                      tRtm6RedisNode *)
        {
            if ((pNode->RegId.u2ProtoId == pRegnId->u2ProtoId) &&
                (pNode->RegId.u4ContextId == pRegnId->u4ContextId))
            {
                break;
            }
        }

        if (pNode == NULL)
        {
            /* some error - Node should present in list but not present. */
            return RTM6_FAILURE;
        }

        if (u4Event == RTM6_ENABLE_PROTO_MASK)
        {
            if ((pNode->u4EnableProtoMask & u4ProtoMask) == u4ProtoMask)
            {
                /* Protocol Mask already enabled and operation in progress
                 * so no need for any operation. */
                return RTM6_SUCCESS;
            }

            /* If any protocol is pending for enable, no need to process it. */
            u4ProtoMask &= ~pNode->u4EnablePendProtoMask;

            /* Check whether Disabling is happening for any of these protocols.
             * If so, keep the enabling event in pending and process once the
             * the disable event is completed.
             */
            u4PendMask = pNode->u4DisableProtoMask & u4ProtoMask;
            if (u4PendMask != 0)
            {
                /* Disable is going on for these protocol. So keep the enable
                 * event as pending.
                 */
                pNode->u4EnablePendProtoMask |= u4PendMask;
                u4ProtoMask &= ~u4PendMask;
            }

            if (u4ProtoMask == 0)
            {
                /* No protocol needs to be enabled. Probably protocol enable
                 * is pending. */
                return RTM6_SUCCESS;
            }

            /* Add the new mask to the enable Mask */
            pNode->u4EnableProtoMask |= u4ProtoMask;

            /* Reset the Enable Init Prefix and Prefix len, so that the 
             * iteration starts all over again. */
            MEMSET (&pNode->EnableInitPrefix, 0, sizeof (tIp6Addr));
            pNode->u1EnableInitPrefixLen = 0;

            /* Set the Enable in progress flag in the registration structure */
            pRtm6Cxt->aRtm6RegnTable[u2RegnId].u1State |=
                RTM6_MASK_ENABLE_INPROGRESS;
        }
        else
        {
            if ((pNode->u4DisableProtoMask & u4ProtoMask) == u4ProtoMask)
            {
                /* Protocol Mask already disabled and operation in progress
                 * so no need for any operation. */
                return RTM6_SUCCESS;
            }
            u4PendMask = pNode->u4EnablePendProtoMask & u4ProtoMask;
            if (u4PendMask != 0)
            {
                /* Some Pending protocols are disabled. */
                pNode->u4EnablePendProtoMask &= ~u4PendMask;
                u4ProtoMask &= ~u4PendMask;
                if (u4ProtoMask == 0)
                {
                    /* No need for any more operation. */
                    return RTM6_SUCCESS;
                }
            }

            /* Check whether Enabling is happening for these protocols.
             * If so stop them. */
            pNode->u4EnableProtoMask &= ~u4ProtoMask;
            if (pNode->u4EnableProtoMask == 0)
            {
                /* No protocol going through Enable event. */
                pRtm6Cxt->aRtm6RegnTable[u2RegnId].u1State &=
                    ~RTM6_MASK_ENABLE_INPROGRESS;
            }

            /* Add the new mask to the disable Mask */
            pNode->u4DisableProtoMask |= u4ProtoMask;

            /* Reset the Disable Init Prefix and Prefix len, so that the 
             * iteration starts all over again. */
            MEMSET (&pNode->DisableInitPrefix, 0, sizeof (tIp6Addr));
            pNode->u1DisableInitPrefixLen = 0;

            /* Set the Disable in progress flag in the registration structure */
            pRtm6Cxt->aRtm6RegnTable[u2RegnId].u1State |=
                RTM6_MASK_DISABLE_INPROGRESS;
        }
    }

    return RTM6_SUCCESS;
}

/*-------------------------------------------------------------------+
 *
 * Function           : Rtm6ReInitRtInCxt 
 *
 * Input(s)           : pRtm6Cxt - RTM6 context Pointer
 *                    : pRegnId - Registration Id
 *
 * Output(s)          : None
 *
 * Returns            : None
 *
 * Action             : This function reinitialises the specified entry in the 
 *                      registration table of RTM6.
 *         
+-------------------------------------------------------------------*/
VOID
Rtm6ReInitRtInCxt (tRtm6Cxt * pRtm6Cxt, tRtm6RegnId * pRegnId)
{
    UINT2               u2RegnId = 0;

    u2RegnId = Rtm6GetFreeIndexInRt (pRegnId);
    if (u2RegnId == IP6_MAX_ROUTING_PROTOCOLS)
    {
        return;
    }

    STRCPY (pRtm6Cxt->aRtm6RegnTable[u2RegnId].au1TaskName, NULL_STR);
    STRCPY (pRtm6Cxt->aRtm6RegnTable[u2RegnId].au1QName, NULL_STR);
    pRtm6Cxt->aRtm6RegnTable[u2RegnId].u2RoutingProtocolId =
        RTM6_INVALID_ROUTING_PROTOCOL_ID;
    pRtm6Cxt->aRtm6RegnTable[u2RegnId].u2ExportProtoMask = 0;
    pRtm6Cxt->aRtm6RegnTable[u2RegnId].u2NextRegId = IP6_MAX_ROUTING_PROTOCOLS;
    pRtm6Cxt->aRtm6RegnTable[u2RegnId].u1AllowOspfInternals =
        RTM6_REDISTRIBUTION_DISABLED;
    pRtm6Cxt->aRtm6RegnTable[u2RegnId].u1AllowOspfExternals =
        RTM6_REDISTRIBUTION_DISABLED;
    pRtm6Cxt->aRtm6RegnTable[u2RegnId].u1State = RTM6_MASK_STABLE;
    pRtm6Cxt->aRtm6RegnTable[u2RegnId].pRtm6Cxt = pRtm6Cxt;
    return;
}

/*-------------------------------------------------------------------+
 *
 * Function           : Rtm6GetFreeIndexInRt
 *
 * Input(s)           : pRegnId - Registration Information.
 *
 * Output(s)          : None
 *
 * Returns            : Returns a free index / IP6_MAX_ROUTING_PROTOCOLS
 *
 * Action             : This function finds a free place in the RTM6 registration
 *                      table.
 *         
+-------------------------------------------------------------------*/
UINT2
Rtm6GetFreeIndexInRt (tRtm6RegnId * pRegnId)
{
    if ((pRegnId->u2ProtoId > 0)
        && (pRegnId->u2ProtoId <= IP6_MAX_ROUTING_PROTOCOLS))
    {
        /* Protocol Id - 1 is used as the registration Id. This
         * will enable faster lookup. */
        return (UINT2) (pRegnId->u2ProtoId - 1);
    }

    /* Protocol not supported. */
    return IP6_MAX_ROUTING_PROTOCOLS;
}

/*-------------------------------------------------------------------+
 *
 * Function           : Rtm6InitTrieForCurrentApp 
 *
 * Input(s)           : pRegnId  - Registration Info
 *
 * Output(s)          : None
 *
 * Returns            : RTM6_SUCCESS/RTM6_FAILURE
 *
 * Action             : This function creates trie instance for the given  
 *                      routing protocol.
 *         
+-------------------------------------------------------------------*/
INT1
Rtm6InitTrieForCurrentApp (tRtm6RegnId * pRegnId)
{
    tRtm6Cxt           *pRtm6Cxt = NULL;

    pRtm6Cxt = UtilRtm6GetCxt (pRegnId->u4ContextId);

    if (pRtm6Cxt == NULL)
    {
        return RTM6_FAILURE;
    }
    /* Initialise the TRIE2 for the given RegnId */
    return ((INT1) Rtm6TrieCreateInCxt (pRtm6Cxt, pRegnId));
}

/*-------------------------------------------------------------------+
 *
 * Function           : Rtm6ProcessRegistration
 *
 * Input(s)           : RegnMsg - Registration message information.
 *
 * Output(s)          : None
 *
 * Returns            : RTM6_SUCCESS - If the registration is successful
 *                      else other failure values.
 *
 * Action             : This function process the registration message from
 *                      the routing protocols. Sends acknowledgement when the 
 *                      registration is successful.
 *         
+-------------------------------------------------------------------*/
INT4
Rtm6ProcessRegistration (tRtm6RegnMsg RegnMsg)
{
    tRtm6RegnId         RegnId;
    UINT2               u2RegnId = 0;
    INT1                i1RetVal = 0;
    tRtm6Cxt           *pRtm6Cxt = NULL;

    RegnId = RegnMsg.RegnId;
    pRtm6Cxt = UtilRtm6GetCxt (RegnId.u4ContextId);
    if (pRtm6Cxt == NULL)
    {
        return RTM6_FAILURE;
    }

    u2RegnId = Rtm6GetFreeIndexInRt (&RegnId);
    if (u2RegnId == IP6_MAX_ROUTING_PROTOCOLS)
    {
        /* Registration table is FULL */
        RTM6_TRC_ARG1 (pRtm6Cxt->u4ContextId, RTM6_ALL_FAILURE_TRC |
                       RTM6_OS_RESOURCE_TRC, RTM6_MOD_NAME,
                       "\tProtocol [%d] Registration FAILED!!! - "
                       "No Free Index\n", RegnId.u2ProtoId);
        return RTM6_NO_RESOURCE;
    }

    if (pRtm6Cxt->aRtm6RegnTable[u2RegnId].u2RoutingProtocolId
        == RegnMsg.RegnId.u2ProtoId)
    {
        /* Protocol has already been registered */
        return RTM6_SUCCESS;
    }

    i1RetVal = Rtm6InitTrieForCurrentApp (&RegnId);
    if (i1RetVal == RTM6_FAILURE)
    {
        /* TRIE init failied for this APP ID */
        RTM6_TRC_ARG1 (pRtm6Cxt->u4ContextId, RTM6_ALL_FAILURE_TRC |
                       RTM6_OS_RESOURCE_TRC, RTM6_MOD_NAME,
                       "\tProtocol [%d] Registration FAILED!!! - "
                       "TRIE Init fails.\n", RegnId.u2ProtoId);
        Rtm6ReInitRtInCxt (pRtm6Cxt, &RegnId);
        return RTM6_NO_RESOURCE;
    }

    /* Fill the Registration Information. */
    if (STRLEN (RegnMsg.au1TaskName) != 0)
    {
        MEMCPY (pRtm6Cxt->aRtm6RegnTable[u2RegnId].au1TaskName,
                RegnMsg.au1TaskName, RTM6_MAX_TASK_NAME_LEN);
    }

    if (STRLEN (RegnMsg.au1QName) != 0)
    {
        MEMCPY (pRtm6Cxt->aRtm6RegnTable[u2RegnId].au1QName,
                RegnMsg.au1QName, RTM6_MAX_Q_NAME_LEN);
    }

    pRtm6Cxt->aRtm6RegnTable[u2RegnId].u4Event = RegnMsg.u4Event;
    pRtm6Cxt->aRtm6RegnTable[u2RegnId].u1BitMask = RegnMsg.u1BitMask;
    pRtm6Cxt->aRtm6RegnTable[u2RegnId].u2RoutingProtocolId =
        RegnMsg.RegnId.u2ProtoId;
    pRtm6Cxt->aRtm6RegnTable[u2RegnId].DeliverToApplication =
        RegnMsg.DeliverToApplication;
    if (RegnMsg.u1BitMask == RTM6_ACK_REQUIRED)
    {
        /* If route needs to be redistributed to the registering protocol
         * set the OSPF INTERNAL and EXTERNAL route as ENABLE. */
        pRtm6Cxt->aRtm6RegnTable[u2RegnId].u1AllowOspfInternals =
            RTM6_REDISTRIBUTION_ENABLED;
        pRtm6Cxt->aRtm6RegnTable[u2RegnId].u1AllowOspfExternals =
            RTM6_REDISTRIBUTION_ENABLED;
    }

    /* RTM6 Send REGISTRATION ACKNOWLEDGEMENT to the Routing protocol if the
       RRD6 Admin status is enabled. */

    if (pRtm6Cxt->u1Rrd6AdminStatus == RTM6_ADMIN_STATUS_ENABLED)
    {
        if (Rtm6SendAckToRpInCxt (pRtm6Cxt, &RegnId) != RTM6_SUCCESS)
        {
            /* Failure in sending the ACK message to the RP. So reinit the
               registration table. */
            RTM6_TRC_ARG1 (pRtm6Cxt->u4ContextId, RTM6_ALL_FAILURE_TRC |
                           RTM6_OS_RESOURCE_TRC, RTM6_MOD_NAME,
                           "\tProtocol [%d] Registration FAILED!!! - "
                           "Sending ACK FAILS.\n", RegnId.u2ProtoId);
            Rtm6ReInitRtInCxt (pRtm6Cxt, &RegnId);
            return RTM6_ACK_SEND_FAIL;
        }
    }

    if (pRtm6Cxt->u2Rtm6RtStartIndex == IP6_MAX_ROUTING_PROTOCOLS)
    {
        pRtm6Cxt->u2Rtm6RtStartIndex = u2RegnId;
        pRtm6Cxt->u2Rtm6RtLastIndex = u2RegnId;
    }
    else
    {
        /* we should not update for the first regn id zero */
        if (pRtm6Cxt->u2Rtm6RtLastIndex < IP6_MAX_ROUTING_PROTOCOLS)
        {
            pRtm6Cxt->aRtm6RegnTable[pRtm6Cxt->u2Rtm6RtLastIndex].u2NextRegId =
                u2RegnId;
            pRtm6Cxt->u2Rtm6RtLastIndex = u2RegnId;
        }
    }

    return RTM6_SUCCESS;
}

/*-------------------------------------------------------------------+
 *
 * Function           : Rtm6SendAckToRpInCxt
 *
 * Input(s)           : pRtm6Cxt- RTM6 Context Pointer 
 *                    : pRegnId - Registration ID to which the ACK message has
 *                      to be sent.
 *
 * Output(s)          : None
 *
 * Returns            : RTM6_SUCCESS - If ACK sent successfully.
 *                      RTM6_FAILURE - If ACK Not sent successfully. 
 *
 * Action             : This function sends the registration ack message to the
 *                      given routing protocol.
 *         
+-------------------------------------------------------------------*/
INT1
Rtm6SendAckToRpInCxt (tRtm6Cxt * pRtm6Cxt, tRtm6RegnId * pRegnId)
{
    tRtm6RespInfo       Rtm6RespInfo;
    tRtm6RegnAckMsg     Rtm6AckMsg;
    tRtm6MsgHdr         MsgHdr;
    UINT2               u2RegnId;

    u2RegnId = Rtm6GetRegnIndexFromRegnIdInCxt (pRtm6Cxt, pRegnId);
    if (u2RegnId == RTM6_INVALID_REGN_ID)
    {
        return RTM6_FAILURE;
    }

    if (!(pRtm6Cxt->aRtm6RegnTable[u2RegnId].u1BitMask & RTM6_ACK_REQUIRED))
    {
        return RTM6_SUCCESS;
    }

    MEMSET ((UINT1 *) &Rtm6AckMsg, 0, sizeof (tRtm6RegnAckMsg));
    Rtm6RespInfo.pRegnAck = &Rtm6AckMsg;

    /* Fill the RTM6 Registration Ack message. */
    Rtm6RespInfo.pRegnAck->u2ASNumber = pRtm6Cxt->u2AsNumber;
    Rtm6RespInfo.pRegnAck->u2Reserved = 0;
    Rtm6RespInfo.pRegnAck->u4RouterId = pRtm6Cxt->u4RouterId;
    Rtm6RespInfo.pRegnAck->u4MaxMessageSize = RTM6_MAX_MESSAGE_SIZE;
    Rtm6RespInfo.pRegnAck->u4ContextId = pRtm6Cxt->u4ContextId;

    MsgHdr.RegnId = *pRegnId;
    MsgHdr.u1MessageType = RTM6_REGISTRATION_ACK_MESSAGE;
    MsgHdr.u2MsgLen = (UINT2) sizeof (tRtm6RegnAckMsg);

    if (Rtm6EnqueuePktToRpsInCxt (pRtm6Cxt, &Rtm6RespInfo,
                                  &MsgHdr, pRegnId) != RTM6_SUCCESS)
    {
        RTM6_TRC_ARG1 (pRtm6Cxt->u4ContextId, RTM6_ALL_FAILURE_TRC,
                       RTM6_MOD_NAME,
                       "\tSending Ack To Routing Protocol [%d] failed!!!\n",
                       pRegnId->u2ProtoId);
        return RTM6_FAILURE;
    }

    return RTM6_SUCCESS;
}

/*-------------------------------------------------------------------+
 *
 * Function           : Rtm6HandleRrd6EnableMsg 
 *
 * Input(s)           : pRegnID  - Registration Information.
 *                      pBuf - Pointer to the buffer received from the routing 
 *                      protocol.
 *
 * Output(s)          : None
 *
 * Returns            : None
 *
 * Action             : This function handles the Route Redistribution Enable
 *                      message from the routing protocols. This function scans
 *                      through the Routing Table, collects the request
 *                      protocol routes and sends the update message to RP's. 
 *         
+-------------------------------------------------------------------*/
VOID
Rtm6HandleRrd6EnableMsg (tRtm6RegnId * pRegnId, tIp6Buf * pBuf)
{
    UINT2               u2RegnId = 0;
    UINT2               u2DestProtoMask = 0;    /* Destination Protocol Mask */
    UINT2               u2TempDestMask = 0;
    UINT1               au1RMapName[RMAP_MAX_NAME_LEN + IP6_FOUR];
    tRtm6Cxt           *pRtm6Cxt = NULL;

    pRtm6Cxt = UtilRtm6GetCxt (pRegnId->u4ContextId);
    if (pRtm6Cxt == NULL)
    {
        IP6_RELEASE_BUF (pBuf, FALSE);
        return;
    }
    /* extract proto mask from offset 0 */
    if (IP6_COPY_FROM_BUF (pBuf, &u2DestProtoMask, 0, sizeof (UINT2))
        != sizeof (UINT2))
    {
        IP6_RELEASE_BUF (pBuf, FALSE);
        return;
    }

    /* extract rmap name from offset 2 */
    MEMSET (au1RMapName, 0, RMAP_MAX_NAME_LEN + IP6_FOUR);
    if (IP6_COPY_FROM_BUF (pBuf, au1RMapName, sizeof (UINT2), RMAP_MAX_NAME_LEN)
        != RMAP_MAX_NAME_LEN)
    {
        IP6_RELEASE_BUF (pBuf, FALSE);
        return;
    }

    IP6_RELEASE_BUF (pBuf, FALSE);

    RTM6_TRC_ARG2 (pRtm6Cxt->u4ContextId, RTM6_CTRL_PATH_TRC, RTM6_MOD_NAME,
                   "\tProcessing Redistribution enable message from Protocol "
                   "[%d] for mask [%d]\n", pRegnId->u2ProtoId, u2DestProtoMask);

    /* Get the protocols, registration ID. */
    u2RegnId = Rtm6GetRegnIndexFromRegnIdInCxt (pRtm6Cxt, pRegnId);
    if (u2RegnId == RTM6_INVALID_REGN_ID)
    {
        return;
    }

    if (pRtm6Cxt->aRtm6RegnTable[u2RegnId].u1BitMask != RTM6_ACK_REQUIRED)
    {
        /* No need for redistributing routes to this protocol. */
        return;
    }

    if (STRLEN (au1RMapName) != 0)
    {
        /* Store the Route Map Name in the Registration Table */
        STRCPY (pRtm6Cxt->aRtm6RegnTable[u2RegnId].au1RMapName, au1RMapName);
    }

    RTM6_SET_BIT (u2TempDestMask, pRegnId->u2ProtoId);

    if (((u2TempDestMask & u2DestProtoMask) == u2TempDestMask) ||
        ((u2DestProtoMask &
          pRtm6Cxt->aRtm6RegnTable[u2RegnId].u2ExportProtoMask) ==
         u2DestProtoMask))
    {
        /* Invalid protomask or protomask already enabled. */
        RTM6_TRC_ARG2 (pRtm6Cxt->u4ContextId, RTM6_CTRL_PATH_TRC, RTM6_MOD_NAME,
                       "\tEnabling Redistribution for Protocol "
                       "[%d] with mask [%d] FAILED!!!\n",
                       pRegnId->u2ProtoId, u2DestProtoMask);
        return;
    }

    /* Find the difference between the already exported protocol mask and the
       new one becos we would have redistributed routes from some protocols 
       previously */

    u2DestProtoMask =
        (UINT2) (u2DestProtoMask &
                 ~(pRtm6Cxt->aRtm6RegnTable[u2RegnId].u2ExportProtoMask));

    /* Update the Destination Protocol mask in the RTM6 Registration Table */
    if ((u2DestProtoMask & RTM6_ISISL1L2_MASK))
    {
        pRtm6Cxt->aRtm6RegnTable[u2RegnId].u2ExportProtoMask =
            (pRtm6Cxt->aRtm6RegnTable[u2RegnId].u2ExportProtoMask) |
            (u2DestProtoMask | RTM6_ISIS_MASK);
    }
    else
    {
        pRtm6Cxt->aRtm6RegnTable[u2RegnId].u2ExportProtoMask =
            pRtm6Cxt->aRtm6RegnTable[u2RegnId].
            u2ExportProtoMask | u2DestProtoMask;
    }
    /* Add the protocol Mask to the ProtoMask Redistribution Handle list. */
    Rtm6AddRedisEventInCxt (pRtm6Cxt, pRegnId,
                            u2DestProtoMask, RTM6_ENABLE_PROTO_MASK);

    /* Check and if necessary start the one second Timer Task */
    if (gRtm6GlobalInfo.u1OneSecTimerStart == FALSE)
    {
        /* Timer not running. Start the one second Timer. */
        if (TmrStart (gRtm6GlobalInfo.Rtm6TmrListId,
                      &(gRtm6GlobalInfo.Rtm6OneSecTmr), RTM6_ONESEC_TIMER,
                      1, 0) == TMR_SUCCESS)
        {
            gRtm6GlobalInfo.u1OneSecTimerStart = TRUE;
        }
    }
    return;
}

/*-------------------------------------------------------------------+
 *
 * Function           : Rtm6HandleRrd6DisableMsg 
 *
 * Input(s)           : pRegnID  - Deregistration Information.
 *                      pBuf - Pointer to the incoming Buffer.
 *
 * Output(s)          : None.
 *
 * Returns            : None.
 *
 * Action             : This function handles the redistribute disable    
 *                      message from the routing protocols.
 * 
+-------------------------------------------------------------------*/
VOID
Rtm6HandleRrd6DisableMsg (tRtm6RegnId * pRegnId, tCRU_BUF_CHAIN_HEADER * pBuf)
{
    UINT2               u2RegnId = 0;
    UINT2               u2DestProtoMask = 0;    /* Destination Protocol Mask */
    UINT2               u2TempDestMask = 0;
    UINT1               au1RMapName[RMAP_MAX_NAME_LEN + IP6_FOUR];
    tRtm6Cxt           *pRtm6Cxt = NULL;

    pRtm6Cxt = UtilRtm6GetCxt (pRegnId->u4ContextId);
    if (pRtm6Cxt == NULL)
    {
        IP6_RELEASE_BUF (pBuf, FALSE);
        return;
    }

    u2RegnId = Rtm6GetRegnIndexFromRegnIdInCxt (pRtm6Cxt, pRegnId);
    if (u2RegnId == RTM6_INVALID_REGN_ID)
    {
        IP6_RELEASE_BUF (pBuf, FALSE);
        return;
    }

    if (IP6_COPY_FROM_BUF (pBuf, &u2DestProtoMask, 0, sizeof (UINT2))
        != sizeof (UINT2))
    {
        IP6_RELEASE_BUF (pBuf, FALSE);
        return;
    }

    /* extract rmap name from offset 2 */
    MEMSET (au1RMapName, 0, RMAP_MAX_NAME_LEN + IP6_FOUR);
    if (IP6_COPY_FROM_BUF (pBuf, au1RMapName, sizeof (UINT2), RMAP_MAX_NAME_LEN)
        != RMAP_MAX_NAME_LEN)
    {
        IP6_RELEASE_BUF (pBuf, FALSE);
        return;
    }

    IP6_RELEASE_BUF (pBuf, FALSE);

    RTM6_TRC_ARG2 (pRtm6Cxt->u4ContextId, RTM6_CTRL_PATH_TRC, RTM6_MOD_NAME,
                   "\tProcessing Redistribution disable message from Protocol "
                   "[%d] for mask [%d]\n", pRegnId->u2ProtoId, u2DestProtoMask);

    RTM6_SET_BIT (u2TempDestMask, pRegnId->u2ProtoId);

    if ((u2TempDestMask & u2DestProtoMask) == u2TempDestMask)
    {
        /* Invalid Proto Mask */
        RTM6_TRC_ARG2 (pRtm6Cxt->u4ContextId, RTM6_CTRL_PATH_TRC, RTM6_MOD_NAME,
                       "\tDisabling Redistribution for Protocol "
                       "[%d] with mask [%d] FAILED!!!\n",
                       pRegnId->u2ProtoId, u2DestProtoMask);
        return;
    }

    /* Find  the already enabled exported protocol mask and the
       disable only those protocols */
    u2DestProtoMask =
        u2DestProtoMask & (pRtm6Cxt->aRtm6RegnTable[u2RegnId].
                           u2ExportProtoMask);

    if (u2DestProtoMask == 0)
    {
        /* Protocols are not enabled earlier. */
        RTM6_TRC_ARG2 (pRtm6Cxt->u4ContextId, RTM6_CTRL_PATH_TRC, RTM6_MOD_NAME,
                       "\tDisabling Redistribution for Protocol "
                       "[%d] with mask [%d] FAILED!!!\n",
                       pRegnId->u2ProtoId, u2DestProtoMask);
        return;
    }

    /* Update the Destination Protocol mask in the RTM6 Registration Table */

    pRtm6Cxt->aRtm6RegnTable[u2RegnId].u2ExportProtoMask = (UINT2)
        (pRtm6Cxt->aRtm6RegnTable[u2RegnId].
         u2ExportProtoMask & ~u2DestProtoMask);

    /*ISIS level1 and Level2 both unmasked, so unmask ISIS_MASK */
    if (!
        (pRtm6Cxt->aRtm6RegnTable[u2RegnId].
         u2ExportProtoMask & RTM6_ISISL1L2_MASK))
    {
        pRtm6Cxt->aRtm6RegnTable[u2RegnId].u2ExportProtoMask =
            (pRtm6Cxt->aRtm6RegnTable[u2RegnId].
             u2ExportProtoMask & ~(RTM6_ISIS_MASK));
    }

    if (au1RMapName[0] == '\0')
    {
        MEMSET (pRtm6Cxt->aRtm6RegnTable[u2RegnId].au1RMapName, 0,
                (RMAP_MAX_NAME_LEN + IP_FOUR));
    }

    /* Add the protocol Mask to the ProtoMask Redistribution Handle list. */
    Rtm6AddRedisEventInCxt (pRtm6Cxt, pRegnId, u2DestProtoMask,
                            RTM6_DISABLE_PROTO_MASK);

    /* Check and if necessary start the one second Timer Task */
    if (gRtm6GlobalInfo.u1OneSecTimerStart == FALSE)
    {
        /* Timer not running. Start the one second Timer. */
        if (TmrStart (gRtm6GlobalInfo.Rtm6TmrListId,
                      &(gRtm6GlobalInfo.Rtm6OneSecTmr), RTM6_ONESEC_TIMER,
                      1, 0) == TMR_SUCCESS)
        {
            gRtm6GlobalInfo.u1OneSecTimerStart = TRUE;
        }
    }
    return;
}

/*-------------------------------------------------------------------+
 *
 * Function           : Rtm6ProcessProtoMaskEvent
 *
 * Input(s)           : pRtm6RedisNode - Info about the protomask event
 *                                       to be processed.
 *
 * Output(s)          : None
 *
 * Returns            : RTM6_COMPLETE - If processing is complete
 *                      RTM6_MORE - If more processing is to be done
 *
 * Action             : This function process the protomask enable/disable
 *                      event. Then constructs the update message by scanning
 *                      the routing table and sends it to the specified Routing
 *                      Protocol.
 *         
+-------------------------------------------------------------------*/
INT4
Rtm6ProcessProtoMaskEvent (tRtm6RedisNode * pRtm6RedisNode)
{
    /* Need to Process Route redistribution */
    tRtm6InitRoute      RtmInitInfo;
    tIp6Addr            InAddr;
    tIp6Addr            OutAddr;
    tIp6Addr            TempAddr;
    UINT1               u1InPrefixLen = 0;
    UINT1               u1OutPrefixLen = 0;
    UINT2               u2RegnId = 0;
    UINT2               u2RouteCnt = 0;
    tRtm6Cxt           *pRtm6Cxt = NULL;

    pRtm6Cxt = UtilRtm6GetCxt (pRtm6RedisNode->RegId.u4ContextId);

    if (pRtm6Cxt == NULL)
    {
        return RTM6_COMPLETE;
    }

    MEMSET (&TempAddr, 0, sizeof (tIp6Addr));
    u2RouteCnt = (UINT2) (((pRtm6RedisNode->u1Event & RTM6_ENABLE_PROTO_MASK) &&
                           (pRtm6RedisNode->u1Event & RTM6_DISABLE_PROTO_MASK))
                          ? gRtm6GlobalInfo.u4ThrotLimit / IP6_TWO :
                          (gRtm6GlobalInfo.u4ThrotLimit));

    if (pRtm6RedisNode->u1Event & RTM6_ENABLE_PROTO_MASK)
    {
        MEMCPY (&InAddr, &pRtm6RedisNode->EnableInitPrefix, sizeof (tIp6Addr));
        u1InPrefixLen = pRtm6RedisNode->u1EnableInitPrefixLen;

        MEMSET (&OutAddr, 0, sizeof (tIp6Addr));
        u1OutPrefixLen = 0;

        u2RegnId = Rtm6GetRegnIndexFromRegnIdInCxt (pRtm6Cxt,
                                                    &pRtm6RedisNode->RegId);
        if (u2RegnId == RTM6_INVALID_REGN_ID)
        {
            return RTM6_COMPLETE;
        }

        /* Scan through the routing table and redistribute the routes to the
         * specified protocol.
         */
        RtmInitInfo.u2Data = (UINT2) pRtm6RedisNode->u4EnableProtoMask;
        RtmInitInfo.u2DestProto = pRtm6RedisNode->RegId.u2ProtoId;
        RtmInitInfo.u2Event = RTM6_ENABLE_PROTO_MASK;
        RtmInitInfo.pRtm6Cxt = pRtm6Cxt;

        Ip6ScanRouteTableForBestRouteInCxt (pRtm6Cxt,
                                            &InAddr, u1InPrefixLen,
                                            Rtm6HandleExportMaskEvent,
                                            u2RouteCnt, &OutAddr,
                                            &u1OutPrefixLen,
                                            (VOID *) &(RtmInitInfo));

        if ((MEMCMP (&OutAddr, &TempAddr, sizeof (tIp6Addr)) == 0) &&
            (u1OutPrefixLen == 0))
        {
            /* Finished with protocol enable event. */
            MEMSET (&pRtm6RedisNode->EnableInitPrefix, 0, sizeof (tIp6Addr));
            pRtm6RedisNode->u1EnableInitPrefixLen = 0;
            pRtm6RedisNode->u1Event &= ~RTM6_ENABLE_PROTO_MASK;
            pRtm6RedisNode->u4EnableProtoMask = 0;
        }
        else
        {
            /* Update the Enable Init Prefix Info */
            MEMCPY (&pRtm6RedisNode->EnableInitPrefix, &OutAddr,
                    sizeof (tIp6Addr));
            pRtm6RedisNode->u1EnableInitPrefixLen = u1OutPrefixLen;
        }
    }

    if (pRtm6RedisNode->u1Event & RTM6_DISABLE_PROTO_MASK)
    {
        MEMCPY (&InAddr, &pRtm6RedisNode->DisableInitPrefix, sizeof (tIp6Addr));
        u1InPrefixLen = pRtm6RedisNode->u1DisableInitPrefixLen;

        MEMSET (&OutAddr, 0, sizeof (tIp6Addr));
        u1OutPrefixLen = 0;

        u2RegnId = Rtm6GetRegnIndexFromRegnIdInCxt (pRtm6Cxt,
                                                    &pRtm6RedisNode->RegId);
        if (u2RegnId == RTM6_INVALID_REGN_ID)
        {
            return RTM6_COMPLETE;
        }

        /* Scan through the routing table and redistribute the routes to the
         * specified protocol.
         */
        RtmInitInfo.u2Data = (UINT2) pRtm6RedisNode->u4DisableProtoMask;
        RtmInitInfo.u2DestProto = pRtm6RedisNode->RegId.u2ProtoId;
        RtmInitInfo.u2Event = RTM6_DISABLE_PROTO_MASK;
        RtmInitInfo.pRtm6Cxt = pRtm6Cxt;

        Ip6ScanRouteTableForBestRouteInCxt (pRtm6Cxt,
                                            &InAddr, u1InPrefixLen,
                                            Rtm6HandleExportMaskEvent,
                                            u2RouteCnt, &OutAddr,
                                            &u1OutPrefixLen,
                                            (VOID *) &(RtmInitInfo));

        if ((MEMCMP (&OutAddr, &TempAddr, sizeof (tIp6Addr)) == 0) &&
            (u1OutPrefixLen == 0))
        {
            /* Finished with protocol disable event. */
            MEMSET (&pRtm6RedisNode->DisableInitPrefix, 0, sizeof (tIp6Addr));
            pRtm6RedisNode->u1DisableInitPrefixLen = 0;
            pRtm6RedisNode->u1Event &= ~RTM6_DISABLE_PROTO_MASK;
            pRtm6RedisNode->u4DisableProtoMask = 0;
        }
        else
        {
            /* Update the Disable Init Prefix Info */
            MEMCPY (&pRtm6RedisNode->DisableInitPrefix, &OutAddr,
                    sizeof (tIp6Addr));
            pRtm6RedisNode->u1DisableInitPrefixLen = u1OutPrefixLen;
        }
    }

    if (pRtm6RedisNode->u1Event == 0)
    {
        if (pRtm6RedisNode->u4EnablePendProtoMask == 0)
        {
            /* Route redistribution complete */
            return RTM6_COMPLETE;
        }
        else
        {
            /* Some protocols are pending for Enable Event. Handle it. */
            pRtm6RedisNode->u1Event = RTM6_ENABLE_PROTO_MASK;
            pRtm6RedisNode->u4EnableProtoMask |=
                pRtm6RedisNode->u4EnablePendProtoMask;
            pRtm6RedisNode->u4EnablePendProtoMask = 0;
        }
    }

    return RTM6_MORE;
}

/*-------------------------------------------------------------------+
 *
 * Function           : Rtm6RouteRedistributionInCxt
 *
 * Input(s)           : pRtm6Cxt - RTM6 Context Pointer
 *                      pRtUpdate - Info about best Route to be redistributed.
 *                      u1IsRtBest - Info whether the route is best or not.
 *                      u2ChgBit   - Info about the Route attributes whose
 *                                   value have been changed.
 *                      u2DestProtoMask - Protocols to which the route is
 *                                        either redistributed or withdrawn
 *
 * Output(s)          : Redistributes the route to the registered protocols.
 *
 * Returns            : None
 *
 * Action             : This function will be called whenever the RTM6 needs to
 *                      redistribute/withdraw routes to specific protocols.
 *                      
+-------------------------------------------------------------------*/
VOID
Rtm6RouteRedistributionInCxt (tRtm6Cxt * pRtm6Cxt,
                              tIp6RtEntry * pRtUpdate, UINT1 u1IsRtBest,
                              UINT2 u2ChgBit, UINT2 u2DestProtoMask)
{
    tNetIpv6RtInfo      RtUpdate;
    tRtm6MsgHdr         MsgHdr;
    tRtm6RegnId         RegnId;
    UINT1               u1MsgType = RTM6_ROUTE_UPDATE_MESSAGE;

    MEMSET (&RtUpdate, 0, (sizeof (tNetIpv6RtInfo)));

    if (pRtUpdate->pCommunity != NULL)
    {
        /*allocate memnory for the community in netipv6rtinfo*
         *based on the community count of rmap info*/
        if ((MALLOC_RTM6_COMM_ENTRY (RtUpdate.pCommunity)) == NULL)
        {
            /*memory allocation failed */
            return;
        }
        MEMSET (RtUpdate.pCommunity, 0, sizeof (tRt6RmapComm));
    }
    if (u2DestProtoMask != RTM6_DONT_REDISTRIBUTE)
    {
        RegnId.u2ProtoId = (UINT2) pRtUpdate->i1Proto;
        RegnId.u4ContextId = pRtm6Cxt->u4ContextId;

        RTM6_COPY_ROUTE_INFO_TO_UPDATE_MSG (pRtUpdate, RtUpdate);

        RtUpdate.u2ChgBit = u2ChgBit;
        RtUpdate.u4ContextId = pRtm6Cxt->u4ContextId;

        if ((u1IsRtBest == FALSE) || (RtUpdate.u4RowStatus != RTM6_ACTIVE))
        {
            /* Route is either not Active or earlier best route but
             * not best route now. Need to withdraw this route from
             * the routing protocols. */
            RtUpdate.u4RowStatus = RTM6_DESTROY;
            u1MsgType = RTM6_ROUTE_CHANGE_NOTIFY_MESSAGE;
        }
        else
        {
            /* Route is best route and should be redistributed. But route is
             * either redistributed for first time or can be replacement
             * route. Set the message type accordingly. */
            RtUpdate.u4RowStatus = RTM6_ACTIVE;
            if (u2ChgBit == IP6_BIT_ALL)
            {
                /* All attributes are changed means route is getting
                 * redistributed for the first time */
                u1MsgType = RTM6_ROUTE_UPDATE_MESSAGE;
            }
            else
            {
                /* Route is a replacment route. */
                u1MsgType = RTM6_ROUTE_CHANGE_NOTIFY_MESSAGE;
            }
        }

        MsgHdr.u1MessageType = u1MsgType;
        MsgHdr.RegnId.u2ProtoId = (UINT2) pRtUpdate->i1Proto;
        MsgHdr.RegnId.u4ContextId = pRtm6Cxt->u4ContextId;
        MsgHdr.u2MsgLen = sizeof (tNetIpv6RtInfo);

        Rtm6DuplicateAndSendToRpsInCxt (pRtm6Cxt,
                                        &MsgHdr, &RtUpdate, &RegnId,
                                        &u2DestProtoMask);

        /* Update the route's redistribution Flag */
        if (RtUpdate.u4RowStatus == RTM6_DESTROY)
        {
            pRtUpdate->u2RedisMask &= ~u2DestProtoMask;
        }
        else
        {
            pRtUpdate->u2RedisMask |= u2DestProtoMask;
        }
        /*release memory for the community in tNetIpv6RtInfo 
         *based on the community count of rmap info*/
        if (RtUpdate.pCommunity != NULL)
        {
            IP_RTM6_COMM_FREE (RtUpdate.pCommunity);
        }

    }
    return;
}

/*-------------------------------------------------------------------+
 *
 * Function           : Rtm6HandleRouteUpdatesInCxt
 *
 * Input(s)           : pRtm6Cxt - RTM6 Context Pointer
 *                      pRtUpdate - Info about best Route to be redistributed.
 *                      u1IsRtBest - Info whether the route is best or not.
 *                      u2ChgBit   - Info about the Route attributes whose
 *                                   value have been changed.
 *
 * Output(s)          : Redistributes the route to the registered protocols.
 *
 * Returns            : None
 *
 * Action             : This function will be called whenever the RTM6 receives
 *                      a route that is selected as best route in IP FWD needs
 *                      to be redistributed or the earlier selected best route
 *                      has been removed. This function verifies whether
 *                      the route can be redistributed or not. If yes, then
 *                      the route is redistributed to the registered routing
 *                      protocols.
 *                      
 *NOTE : This function is the basic input function for handling route
         redistribution. If RTM6 needs to be ported with any other IP stack
         then this function needs to be called for redistributing routes
         to the registered routing protocols.
+-------------------------------------------------------------------*/
VOID
Rtm6HandleRouteUpdatesInCxt (tRtm6Cxt * pRtm6Cxt, tIp6RtEntry * pRtUpdate,
                             UINT1 u1IsRtBest, UINT2 u2ChgBit)
{
    tRtm6RegnId         RegnId;
    UINT2               u2DestProtoMask = RTM6_DONT_REDISTRIBUTE;
#ifdef NPAPI_WANTED
    UINT4               u4NHType = 0;
    tFsNpIntInfo        IntInfo;
    tFsNpRouteInfo      RouteInfo;
#ifdef TUNNEL_WANTED
    UINT4               u4Addr = 0;
    tTnlIfEntry         TnlIfEntry;
#endif /* TUNNEL_WANTED */
    INT1                i1Type = pRtUpdate->i1Type;
#endif /* NPAPI_WANTED */
#ifdef NPAPI_WANTED
    MEMSET (&RouteInfo, 0, sizeof (tFsNpRouteInfo));
#endif /* NPAPI_WANTED */

    /* This function will not check whether redistribution is enabled in
       the destination protocol or not. It should be taken care while
       sending the updates. */
    RegnId.u2ProtoId = (UINT2) pRtUpdate->i1Proto;
    RegnId.u4ContextId = pRtm6Cxt->u4ContextId;

    /* After deriving the u2DestProtoMask, reset the mask for the RPs where
     * Route Map is enabled. For a particulur RP, filter rule and Route
     * Map rule cannot be applied, Route Map rule superceds the fileter
     * rule */

    Rtm6CheckRouteForRedistributionInCxt (pRtm6Cxt, &RegnId,
                                          pRtUpdate, &u2DestProtoMask);

    Rtm6ResetRouteMapBitMaskInCxt (pRtm6Cxt, &u2DestProtoMask);

    if (u2DestProtoMask != RTM6_DONT_REDISTRIBUTE)
    {
        Rtm6RouteRedistributionInCxt (pRtm6Cxt,
                                      pRtUpdate, u1IsRtBest, u2ChgBit,
                                      u2DestProtoMask);
    }

    /* apply Route Map rule  on the route and redistribute to the RPs
     * which are configured with Route Map */

    Rtm6SetRouteMapBitMaskInCxt (pRtm6Cxt, &u2DestProtoMask);

    Rtm6ApplyRouteMapRuleAndRedistributeInCxt (pRtm6Cxt,
                                               RegnId.u2ProtoId, pRtUpdate,
                                               u1IsRtBest, u2ChgBit,
                                               u2DestProtoMask);
    Rtm6GetInstalledRouteCount (pRtUpdate, &pRtUpdate->u1EcmpCount);
    if (u1IsRtBest == TRUE)
    {
        if ((pRtUpdate->u1EcmpCount == 0) &&
            ((pRtUpdate->u4Flag & RTM6_ECMP_RT) != RTM6_ECMP_RT))
        {
            Rtm6AddRtToPRTInCxt (pRtm6Cxt, pRtUpdate);
            Rtm6AddRtToECMP6PRT (pRtm6Cxt, pRtUpdate);

        }
    }
    else
    {
        if ((pRtUpdate->u1EcmpCount == 0) &&
            ((pRtUpdate->u4Flag & RTM6_ECMP_RT) != RTM6_ECMP_RT))
        {
            Rtm6DeleteRtFromPRTInCxt (pRtm6Cxt, pRtUpdate);
            Rtm6DeleteRtFromECMP6PRT (pRtm6Cxt, pRtUpdate);

        }

    }

#ifdef NPAPI_WANTED
    MEMSET (&IntInfo, 0, sizeof (tFsNpIntInfo));

    if (u1IsRtBest == TRUE)
    {
        IntInfo.u1IfType = Ip6GetIfType (pRtUpdate->u4Index);
        if ((IntInfo.u1IfType == CFA_L3IPVLAN)
            || (IntInfo.u1IfType == CFA_L3SUB_INTF))
        {
            IntInfo.u4PhyIfIndex = pRtUpdate->u4Index;
        }
        else if (IntInfo.u1IfType == IP6_PSEUDO_WIRE_INTERFACE_TYPE)
        {
            IntInfo.u4PhyIfIndex = pRtUpdate->u4Index;
        }

#ifdef TUNNEL_WANTED
        else if (IntInfo.u1IfType == CFA_TUNNEL)
        {
            MEMSET (&TnlIfEntry, 0, sizeof (tTnlIfEntry));

            /* fetch the Tunnel interface's Physical Interface index */
            if (CfaGetTnlEntryFromIfIndex (pRtUpdate->u4Index, &TnlIfEntry) ==
                CFA_FAILURE)
            {
                return;
            }
            IntInfo.u4PhyIfIndex = TnlIfEntry.u4PhyIfIndex;
            IntInfo.TunlInfo.tunlSrc.u4_addr[IP6_THREE] =
                TnlIfEntry.LocalAddr.Ip4TnlAddr;
            IntInfo.TunlInfo.u1TunlType = (UINT4) TnlIfEntry.u4EncapsMethod;
            IntInfo.TunlInfo.u1TunlFlag = TnlIfEntry.u1DirFlag;
            IntInfo.TunlInfo.u1TunlDir = TnlIfEntry.u1Direction;

            if ((TnlIfEntry.u4EncapsMethod == IPV6_SIX_TO_FOUR) ||
                (TnlIfEntry.u4EncapsMethod == IPV6_ISATAP_TUNNEL) ||
                (TnlIfEntry.u4EncapsMethod == IPV6_AUTO_COMPAT))
            {
                if (IpGetIpv4AddrFromArpTable
                    ((INT4) IntInfo.u4PhyIfIndex, &u4Addr) == IP_FAILURE)
                {
                    return;
                }
                IntInfo.TunlInfo.tunlDst.u4_addr[IP6_THREE] = u4Addr;
            }

            if ((TnlIfEntry.u4EncapsMethod == IPV6_OVER_IPV4_TUNNEL) ||
                (TnlIfEntry.u4EncapsMethod == IPV6_GRE_TUNNEL))
            {
                IntInfo.TunlInfo.tunlDst.u4_addr[IP6_THREE] =
                    TnlIfEntry.RemoteAddr.Ip4TnlAddr;
            }

        }
#endif
        if (IntInfo.u1IfType != IP6_ENET_INTERFACE_TYPE)
        {
            if (CfaGetVlanId (IntInfo.u4PhyIfIndex, &(IntInfo.u2VlanId)) ==
                CFA_FAILURE)
            {
                return;
            }
        }
        else
        {
            IntInfo.u4PhyIfIndex = pRtUpdate->u4Index;
            IntInfo.u2VlanId = 0;
        }
#ifdef TUNNEL_WANTED
        if (IntInfo.u1IfType == IP6_TUNNEL_INTERFACE_TYPE)
        {
            u4NHType = NH_TUNNEL;
        }
        else
#endif
            /* New route is Added */
        if (i1Type == IP6_ROUTE_TYPE_DIRECT)
        {
            u4NHType = NH_DIRECT;
        }
        else if (i1Type == IP6_ROUTE_TYPE_DISCARD)
        {
            u4NHType = NH_DISCARD;
        }
        else if (i1Type == IP6_ROUTE_TYPE_INDIRECT)
        {
            u4NHType = NH_REMOTE;
        }
        else
        {
            /* Unknown Route Type. */
            u4NHType = NH_SENDTO_CP;
        }

        if (RTM6_GET_NODE_STATUS () == RM_ACTIVE)
        {
            pRtUpdate->u1HwStatus = NP_NOT_PRESENT;
            Rtm6RedAddDynamicInfo (pRtUpdate, pRtm6Cxt->u4ContextId);
            Rtm6RedSendDynamicInfo ();
        }
        if (RTM6_IS_NP_PROGRAMMING_ALLOWED () == RTM6_SUCCESS)
        {
            /*Set the reachability state for routes and passing the no of 
             *routes to HW*/
            Rtm6SetReachState (pRtm6Cxt->u4ContextId, pRtUpdate);
            IntInfo.u1RouteCount = pRtUpdate->u1EcmpCount;
            if (Ipv6FsNpIpv6UcRouteAdd (pRtm6Cxt->u4ContextId,
                                        (UINT1 *) &pRtUpdate->dst,
                                        pRtUpdate->u1Prefixlen,
                                        (UINT1 *) &pRtUpdate->nexthop, u4NHType,
                                        &IntInfo) == FNP_FAILURE)
            {
                RTM6_TRC_ARG2 (pRtm6Cxt->u4ContextId, RTM6_ALL_FAILURE_TRC |
                               RTM6_OS_RESOURCE_TRC, RTM6_MOD_NAME,
                               "ERROR[RTM]: IPV6 NP Route   Add Fail%s/%d   \n",
                               Ip6PrintNtop (&pRtUpdate->dst),
                               (INT4) pRtUpdate->u1Prefixlen);
                if (Rtm6FrtAddInfo
                    (pRtUpdate, pRtm6Cxt->u4ContextId,
                     (UINT1) u4NHType) == RTM6_SUCCESS)
                {
                    Rtm6RedSyncFrtInfo (pRtUpdate, pRtm6Cxt->u4ContextId,
                                        (UINT1) u4NHType, RTM6_ADD_ROUTE);
                }
                else
                {
                    RTM6_TRC_ARG2 (pRtm6Cxt->u4ContextId, RTM6_ALL_FAILURE_TRC |
                                   RTM6_OS_RESOURCE_TRC, RTM6_MOD_NAME,
                                   "ERROR[RTM]: IPV6  Failed Route Addition Failed%s/%d   \n",
                                   Ip6PrintNtop (&pRtUpdate->dst),
                                   (INT4) pRtUpdate->u1Prefixlen);
                }
                return;
            }
            pRtUpdate->u4HwIntfId[0] = IntInfo.u4HwIntfId[0];

        }
        if (RTM6_GET_NODE_STATUS () == RM_ACTIVE)
        {
            pRtUpdate->u1HwStatus = NP_PRESENT;
            Rtm6RedAddDynamicInfo (pRtUpdate, pRtm6Cxt->u4ContextId);
            Rtm6RedSendDynamicInfo ();
        }
    }
    else
    {
        if (pRtUpdate->i1Type == IP6_ROUTE_TYPE_DIRECT)
        {
            RouteInfo.u1NHType = NH_DIRECT;
        }
        else if (pRtUpdate->i1Type == IP6_ROUTE_TYPE_DISCARD)
        {
            RouteInfo.u1NHType = NH_DISCARD;
        }

        /* Old route is Deleted */
        if (RTM6_GET_NODE_STATUS () == RM_ACTIVE)
        {
            pRtUpdate->u1HwStatus = NP_PRESENT;
            Rtm6RedAddDynamicInfo (pRtUpdate, pRtm6Cxt->u4ContextId);
            Rtm6RedSendDynamicInfo ();
        }
        if (RTM6_IS_NP_PROGRAMMING_ALLOWED () == RTM6_SUCCESS)
        {
            RouteInfo.u1RouteCount = pRtUpdate->u1EcmpCount;
            RouteInfo.u4HwIntfId[0] = pRtUpdate->u4HwIntfId[0];
            if (Ipv6FsNpIpv6UcRouteDelete (pRtm6Cxt->u4ContextId,
                                           (UINT1 *) &pRtUpdate->dst,
                                           pRtUpdate->u1Prefixlen,
                                           (UINT1 *) &pRtUpdate->nexthop,
                                           pRtUpdate->u4Index,
                                           &RouteInfo) == FNP_FAILURE)
            {
                return;
            }
        }
        if (RTM6_GET_NODE_STATUS () == RM_ACTIVE)
        {
            pRtUpdate->u1HwStatus = NP_NOT_PRESENT;
            Rtm6RedAddDynamicInfo (pRtUpdate, pRtm6Cxt->u4ContextId);
            Rtm6RedSendDynamicInfo ();
        }
    }
#endif /* NPAPI_WANTED */
    return;
}

/*-------------------------------------------------------------------+
 *
 * Function           : Rtm6DuplicateAndSendToRpsInCxt
 *
 * Input(s)           : pRtm6Cxt - Rtm6 Conte4xt Pointer 
 *                      pMsgHdr - Pointer to the Rtm6 Response message header
 *                      pNetRtInfo - Pointer to the route info to be
 *                      redistributed.
 *                      pSrcRegnId - Information about the Source of this route.
 *                      pu2DestProtoMask - Protocol mask to which the route
 *                                         needs to be sent.
 *
 * Output(s)          : pu2DestProtoMask - Protocol mask to which the route
 *                                         has been sent.
 *
 * Returns            : None.
 *
 * Action             : This function will be called whenever RTM6 wants to
 *                      send a packet to multiple RP's. This function will 
 *                      duplicate and send the packet to all RP's. This 
 *                      also updates the export list of the routing protocols.
 * 
+-------------------------------------------------------------------*/
VOID
Rtm6DuplicateAndSendToRpsInCxt (tRtm6Cxt * pRtm6Cxt,
                                tRtm6MsgHdr * pMsgHdr,
                                tNetIpv6RtInfo * pNetRtInfo,
                                tRtm6RegnId * pSrcRegnId,
                                UINT2 *pu2DestProtoMask)
{
    tRtm6RegnId         DestRegnId;
    tRtm6RespInfo       Rtm6RespInfo;
    UINT2               u2RegnIndex = 0;
    UINT2               u2SrcProtoMask = 0;
    UINT2               u2DestMask = 0;
    UINT2               u2RedisMask = 0;

    RTM6_SET_BIT (u2SrcProtoMask, pSrcRegnId->u2ProtoId);

    for (u2RegnIndex = pRtm6Cxt->u2Rtm6RtStartIndex;
         u2RegnIndex < IP6_MAX_ROUTING_PROTOCOLS;
         u2RegnIndex = pRtm6Cxt->aRtm6RegnTable[u2RegnIndex].u2NextRegId)
    {
        DestRegnId.u2ProtoId =
            pRtm6Cxt->aRtm6RegnTable[u2RegnIndex].u2RoutingProtocolId;
        u2DestMask = 0;

        if (pRtm6Cxt->aRtm6RegnTable[u2RegnIndex].u1BitMask !=
            RTM6_ACK_REQUIRED)
        {
            /* Protocol does not required any message. */
            continue;
        }

        /* Route can be redistributed to this protocol. */
        RTM6_SET_BIT (u2DestMask,
                      pRtm6Cxt->aRtm6RegnTable[u2RegnIndex].
                      u2RoutingProtocolId);

        if ((*pu2DestProtoMask & u2DestMask) != u2DestMask)
        {
            /* Route need not be redistributed to this protocol. */
            continue;
        }

        if (pRtm6Cxt->aRtm6RegnTable[u2RegnIndex].u2ExportProtoMask &
            u2SrcProtoMask)
        {
            /* Redistribution is enabled for this protocol */
            Rtm6RespInfo.pRtInfo = pNetRtInfo;
            Rtm6EnqueuePktToRpsInCxt (pRtm6Cxt, &Rtm6RespInfo,
                                      pMsgHdr, &DestRegnId);
            u2RedisMask |= u2DestMask;
        }
    }

    *pu2DestProtoMask = u2RedisMask;
    return;
}

/**************************************************************************/
/*   Function Name   : Rtm6ClearProtoRouteFromTrieInCxt                   */
/*   Description     : This function will remove all the route learnt     */
/*                     from the given protocol from the TRIE.             */
/*   Input(s)        : pRtm6Cxt - RTM6 context Pointer                    */
/*                   : pRegnID - Protocol Info whose route need to cleared*/
/*   Output(s)       : None                                               */
/*   Return Value    : None                                               */
/**************************************************************************/

VOID
Rtm6ClearProtoRouteFromTrieInCxt (tRtm6Cxt * pRtm6Cxt, tRtm6RegnId * pRegnId)
{
    /* Clear all the routes recevied from the protocol */
    Rtm6TrieDeleteInCxt (pRtm6Cxt, pRegnId);
    return;
}

/**************************************************************************/
/*   Function Name   : Rtm6GetSizingParams                                */
/*   Description     : This function get the Sizing parameters            */
/*   Input(s)        : None                                               */
/*   Output(s)       : None                                               */
/*   Return Value    : None                                               */
/**************************************************************************/

void
Rtm6GetSizingParams (void)
{
    /* Initialise the sizing parameters */
    gRtm6SystemSize.u4Rtm6MaxRoutes = 0;
    GetRtm6SizingParams (&gRtm6SystemSize);
}

/*-------------------------------------------------------------------+
 *
 * Function           : Rtm6CreateCxt
 *
 * Input(s)           : u4ContextId - RTM6 Context Id
 *
 * Output(s)          : None
 *
 * Returns            : RTM6_SUCCESS/RTM6_FAILURE
 *
 * Action             : This function initialises the global data structures of
 *                      of RTM6.
 *         
+-------------------------------------------------------------------*/
INT4
Rtm6CreateCxt (UINT4 u4ContextId)
{
    tRtm6RegnId         RegnId;
    tRtm6Cxt           *pRtm6Cxt = NULL;
    UINT2               u2Index1 = 0;

    if (u4ContextId >= gRtm6GlobalInfo.u4MaxContextLimit)
    {
        return RTM6_FAILURE;
    }

    pRtm6Cxt = UtilRtm6GetCxt (u4ContextId);
    if (pRtm6Cxt != NULL)
    {
        /* Rtm6 Context is already created */

        return RTM6_SUCCESS;
    }

    if (RTM6_CONTEXT_ALLOC (pRtm6Cxt) == NULL)
    {
        /* Rtm6 Context memory allocation failed */
        RTM6_GBL_TRC (RTM6_INVALID_CXT_ID,
                      RTM6_ALL_FAILURE_TRC | RTM6_OS_RESOURCE_TRC,
                      RTM6_MOD_NAME,
                      "\tRTM6 Context memory allocation Failed !!!\n");

        return RTM6_FAILURE;
    }
    MEMSET (pRtm6Cxt, 0, sizeof (tRtm6Cxt));

    pRtm6Cxt->u4ContextId = u4ContextId;
    gRtm6GlobalInfo.apRtm6Cxt[u4ContextId] = pRtm6Cxt;
    pRtm6Cxt->u2Rtm6RtStartIndex = IP6_MAX_ROUTING_PROTOCOLS;
    pRtm6Cxt->u2Rtm6RtLastIndex = IP6_MAX_ROUTING_PROTOCOLS;
    pRtm6Cxt->u4RouterId = 0;
    pRtm6Cxt->u2AsNumber = 0;
    pRtm6Cxt->u1Rrd6AdminStatus = RTM6_ADMIN_STATUS_DISABLED;
    pRtm6Cxt->u4TraceFlag = RTM6_ECMP6_FAILURE_TRC;
    pRtm6Cxt->u1Ip6DefaultDistance = IP6_PREFERENCE_LOCAL;

    /* Initialise the RRD6 Control List */
    TMO_SLL_Init (&(pRtm6Cxt->Rtm6CtrlList));

    /* Initialize temporary route list */
    TMO_SLL_Init (&(pRtm6Cxt->Rtm6InvdRtList));

    /* Initialise the RTM6 Registration Table */
    for (u2Index1 = 0; u2Index1 < IP6_MAX_ROUTING_PROTOCOLS; u2Index1++)
    {
        RegnId.u2ProtoId = (UINT2) (u2Index1 + 1);
        RegnId.u4ContextId = u4ContextId;
        Rtm6ReInitRtInCxt (pRtm6Cxt, &RegnId);
    }

    pRtm6Cxt->u4Ip6FwdTblRouteNum = 0;
    pRtm6Cxt->pIp6RtTblRoot = NULL;

    pRtm6Cxt->Rtm6ConfigInfo.u4OspfTagValue = 0;
    pRtm6Cxt->Rtm6ConfigInfo.u4OspfTagMask = 0;
    pRtm6Cxt->Rtm6ConfigInfo.u1Rrd6FilterByOspfTag = RTM6_FILTER_DISABLED;
    Rtm6InitPreferenceInCxt (pRtm6Cxt);
    /*  Initialise Rtm6Control table to Deny mode by adding a PERMIT_ALL entry
       in the table */
    Rtm6InitControlTableToDenyModeInCxt (pRtm6Cxt);

    return RTM6_SUCCESS;
}

/*-------------------------------------------------------------------+
 *
 * Function           : Rtm6VcmMsgHandler
 *
 * Input              : pBuf - Pointer to the buffer having VCM context
 *                      creation and delete indication
 *
 * Output(s)          : None.
 *
 * Returns            : None
 *
 * Action             : This routine is called whenever a L3 Context is
 *                      added/deleted.
+-------------------------------------------------------------------*/
VOID
Rtm6VcmMsgHandler (tCRU_BUF_CHAIN_HEADER * pBuf)
{
    UINT4               u4ContextId = 0;
    UINT1               u1BitMap = 0;

    /* copy the  VCM change indication */
    if (IP6_COPY_FROM_BUF (pBuf, &u1BitMap, 0, sizeof (UINT1)) == CRU_FAILURE)
    {
        IP6_RELEASE_BUF (pBuf, FALSE);
        return;
    }

    /* copy the  VCM change indication */
    if (IP_COPY_FROM_BUF (pBuf, &u4ContextId,
                          sizeof (UINT4), sizeof (UINT4)) == CRU_FAILURE)
    {
        IP6_RELEASE_BUF (pBuf, FALSE);
        return;
    }
    if (u1BitMap == VCM_CONTEXT_DELETE)
    {
        Rtm6DeleteCxt (u4ContextId);
    }

    IP6_RELEASE_BUF (pBuf, FALSE);
    return;
}

/*-------------------------------------------------------------------+
 *
 * Function           : Rtm6VcmCallBackFn
 *
 * Description        : Function to Indicate Context mapping/deletion to 
 *                      RTM6. This fucntion is registered with VCM module         
 *                      as a call back function  
 *
 * Input              : u4IfIndex   - Ip Interface Index                           
 *                      u4VcmCxtId  - Context Id                                   
 *                      u1BitMap    - Bit Map to identify the change               
 *
 * Output(s)          : None.
 *
 * Returns            : None
 *
 * Action             : This routine is called whenever a L3 Context is
 *                      added/deleted.
+-------------------------------------------------------------------*/
VOID
Rtm6VcmCallBackFn (UINT4 u4IpIfIndex, UINT4 u4VcmCxtId, UINT1 u1BitMap)
{
    tOsixMsg           *pRtm6Msg = NULL;
    tRtm6MsgHdr        *pRtm6MsgHdr = NULL;

    UNUSED_PARAM (u4IpIfIndex);

    /* only the Context Create/delete indication is needed */
    if ((u1BitMap & (VCM_CONTEXT_CREATE | VCM_CONTEXT_DELETE)) == 0)
    {
        return;
    }

    if (u1BitMap & (VCM_CONTEXT_CREATE))
    {
        RTM6_TASK_LOCK ();
        Rtm6CreateCxt (u4VcmCxtId);
        RTM6_TASK_UNLOCK ();
        return;
    }

    if ((pRtm6Msg =
         CRU_BUF_Allocate_MsgBufChain ((IP6_TWO * sizeof (UINT4)), 0)) == NULL)
    {
        return;
    }

    pRtm6MsgHdr = (tRtm6MsgHdr *) CRU_BUF_Get_ModuleData (pRtm6Msg);

    /* fill the message header */
    pRtm6MsgHdr->u1MessageType = RTM6_VCM_MSG_RCVD;
    pRtm6MsgHdr->RegnId.u4ContextId = u4VcmCxtId;

    /* copy the  VCM change indication */
    if (IP6_COPY_TO_BUF (pRtm6Msg, &u1BitMap, 0, sizeof (UINT1)) == CRU_FAILURE)
    {
        IP6_RELEASE_BUF (pRtm6Msg, FALSE);
        return;
    }

    /* copy the  VCM change indication */
    if (IP6_COPY_TO_BUF (pRtm6Msg, &u4VcmCxtId,
                         sizeof (UINT4), sizeof (UINT4)) == CRU_FAILURE)
    {

        IP6_RELEASE_BUF (pRtm6Msg, FALSE);
        return;
    }

    /* Clear the static configurations done for this context */
    RTM6_TASK_LOCK ();
    Rtm6ClearContextEntries (u4VcmCxtId);
    RTM6_TASK_UNLOCK ();

    /* Send the buffer to RTM */
    if (OsixSendToQ (SELF,
                     (const UINT1 *) RTM6_MESSAGE_ARRIVAL_Q_NAME,
                     pRtm6Msg, OSIX_MSG_NORMAL) != OSIX_SUCCESS)
    {
        IP6_RELEASE_BUF (pRtm6Msg, FALSE);
        return;
    }
    OsixSendEvent (SELF, (const UINT1 *) RTM6_MAIN_TASK_NAME,
                   RTM6_MESSAGE_ARRIVAL_EVENT);

    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rtm6DeleteCxt                                              */
/*                                                                           */
/* Description  : Deletes Rtm6 context                                       */
/*                                                                           */
/* Input        : u4ContextId,                                               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RTM6_SUCCESS/RTM6_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
VOID
Rtm6DeleteCxt (UINT4 u4ContextId)
{
    tIp6Addr            AddrMask;
    tIp6RtEntry        *pRtEntry = NULL;
    tRtm6Cxt           *pRtm6Cxt = NULL;
    VOID               *pRibNode = NULL;
    tInputParams        InParams;
    tRrd6ControlInfo   *pRtm6CtrlInfo = NULL;
    tRrd6ControlInfo   *pRtm6NextCtrlInfo = NULL;
    tIp6RtEntry        *pRtm6InvdRt = NULL;
    tIp6RtEntry        *pRtm6NextInvdRt = NULL;
    tRtm6RedisNode     *pNode = NULL;
    tRtm6RedisNode     *pTmpNode = NULL;
    tRtm6RegnId         RegnId;
    INT4                i4Status = TRIE_FAILURE;
#ifdef NPAPI_WANTED
    tIp6RtEntry        *pIp6BestRtEntry = NULL;
    tPRt6Entry         *pPRt6Curr = NULL;
    tPRt6Entry         *pPRt6 = NULL;
    tPRt6Entry         *pPRt6NextEntry = NULL;
    tPNh6Node          *pPNh6Entry = NULL;
    tPNh6Node          *pNextPNh6Entry = NULL;
    tRNh6Node          *pRNh6Entry = NULL;
    tRNh6Node          *pNextRNh6Entry = NULL;
    tFsNpRouteInfo      RouteInfo;
    INT4                i4OutCome = RTM6_FAILURE;
#endif
    UINT1               au1TrieKey[RTM6_MAX_TRIE_KEY_LEN];

    MEMSET (au1TrieKey, IP6_ZERO, RTM6_MAX_TRIE_KEY_LEN);
    MEMSET ((UINT1 *) &AddrMask, 0xFF, sizeof (tIp6Addr));

    if ((u4ContextId < gRtm6GlobalInfo.u4MaxContextLimit) &&
        (gRtm6GlobalInfo.apRtm6Cxt[u4ContextId] != NULL))
    {
        pRtm6Cxt = gRtm6GlobalInfo.apRtm6Cxt[u4ContextId];
    }
    else
    {
        return;
    }

    if (pRtm6Cxt == NULL)
    {
        return;
    }
    InParams.pRoot = pRtm6Cxt->pIp6RtTblRoot;
    InParams.i1AppId = ALL_PROTO_ID;
    InParams.pLeafNode = NULL;
    i4Status = TrieGetFirstNode (&InParams, (VOID *) &pRtEntry, &pRibNode);

    InParams.Key.pKey = au1TrieKey;
    MEMSET (InParams.Key.pKey, 0, RTM6_MAX_TRIE_KEY_LEN);

    while (i4Status != TRIE_FAILURE)
    {
        Ip6CopyAddrBits ((tIp6Addr *) (VOID *) InParams.Key.pKey,
                         &pRtEntry->dst, pRtEntry->u1Prefixlen);
        Ip6CopyAddrBits ((tIp6Addr *) (VOID *)
                         (InParams.Key.pKey + IP6_ADDR_SIZE),
                         &AddrMask, pRtEntry->u1Prefixlen);
        InParams.u1PrefixLen = (UINT1) pRtEntry->u1Prefixlen;
#ifdef NPAPI_WANTED
        pIp6BestRtEntry = NULL;
        if (pRtEntry->u1AddrType == ADDR6_ANYCAST)
        {
            i4OutCome =
                Rtm6UtilFwdTblGetBestAnycastRouteInCxt (pRtm6Cxt, pRtEntry,
                                                        &pIp6BestRtEntry);
        }
        else
        {
            i4OutCome =
                Rtm6UtilFwdTblGetBestRouteInCxt (pRtm6Cxt, pRtEntry,
                                                 &pIp6BestRtEntry);
        }
        if (i4OutCome == IP6_SUCCESS)
        {
            if (RTM6_GET_NODE_STATUS () == RM_ACTIVE)
            {
                pRtEntry->u1HwStatus = NP_NOT_PRESENT;
                Rtm6RedAddDynamicInfo (pRtEntry, pRtm6Cxt->u4ContextId);
                Rtm6RedSendDynamicInfo ();
            }
            if (RTM6_IS_NP_PROGRAMMING_ALLOWED () == RTM6_SUCCESS)
            {
                RouteInfo.u1RouteCount = pRtEntry->u1EcmpCount;
                RouteInfo.u4HwIntfId[0] = pRtEntry->u4HwIntfId[0];
                i4OutCome = Ipv6FsNpIpv6UcRouteDelete (pRtm6Cxt->u4ContextId,
                                                       (UINT1 *)
                                                       &pIp6BestRtEntry->dst,
                                                       pIp6BestRtEntry->
                                                       u1Prefixlen,
                                                       (UINT1 *) &pRtEntry->
                                                       nexthop,
                                                       pRtEntry->u4Index,
                                                       &RouteInfo);
            }
            if (i4OutCome == FNP_SUCCESS)
            {
                if (RTM6_GET_NODE_STATUS () == RM_ACTIVE)
                {
                    pRtEntry->u1HwStatus = NP_PRESENT;
                    Rtm6RedAddDynamicInfo (pRtEntry, pRtm6Cxt->u4ContextId);
                    Rtm6RedSendDynamicInfo ();
                }
            }
        }
        /* Deleting the entry from the Pend Route Table */
        pPNh6Entry = RBTreeGetFirst (gRtm6GlobalInfo.pPRT6RBRoot);

        if (pPNh6Entry != NULL)
        {
            while ((pNextPNh6Entry = RBTreeGetNext (gRtm6GlobalInfo.pPRT6RBRoot,
                                                    pPNh6Entry, NULL)) != NULL)
            {
                if (pPNh6Entry->pRtm6Cxt->u4ContextId == pRtm6Cxt->u4ContextId)
                {
                    TMO_DYN_SLL_Scan (&pPNh6Entry->routeEntryList, pPRt6,
                                      pPRt6NextEntry, tPRt6Entry *)
                    {
                        /* Delete the pending route entry from the list */
                        TMO_SLL_Delete (&pPNh6Entry->routeEntryList,
                                        (tTMO_SLL_NODE *) pPRt6Curr);
                        MemReleaseMemBlock (gRtm6GlobalInfo.Rtm6URRtPoolId,
                                            (UINT1 *) pPRt6Curr);
                    }
                    /* Removing the Node Entry from RB Tree */
                    RBTreeRem (gRtm6GlobalInfo.pPRT6RBRoot, pPNh6Entry);
                    MemReleaseMemBlock (gRtm6GlobalInfo.Rtm6PNextHopPoolId,
                                        (UINT1 *) pPNh6Entry);
                }
                pPNh6Entry = pNextPNh6Entry;
            }
        }

        /* Deleting the entry from the Resolved Next Hop */
        pRNh6Entry = RBTreeGetFirst (gRtm6GlobalInfo.pRNHT6RBRoot);

        if (pRNh6Entry != NULL)
        {
            while ((pNextRNh6Entry =
                    RBTreeGetNext (gRtm6GlobalInfo.pRNHT6RBRoot, pRNh6Entry,
                                   NULL)) != NULL)
            {
                if (pRNh6Entry->pRtm6Cxt->u4ContextId == pRtm6Cxt->u4ContextId)
                {
                    /* Removing the Node Entry from RB Tree */
                    RBTreeRem (gRtm6GlobalInfo.pRNHT6RBRoot, pRNh6Entry);
                    MemReleaseMemBlock (gRtm6GlobalInfo.Rtm6RNextHopPoolId,
                                        (UINT1 *) pRNh6Entry);

                }
                pRNh6Entry = pNextRNh6Entry;
            }
        }
        /* Deleting the entry from the ECMP6 PRT */
        pPNh6Entry = RBTreeGetFirst (gRtm6GlobalInfo.pECMP6PRTRBRoot);

        if (pPNh6Entry != NULL)
        {
            while ((pNextPNh6Entry =
                    RBTreeGetNext (gRtm6GlobalInfo.pECMP6PRTRBRoot, pPNh6Entry,
                                   NULL)) != NULL)
            {
                if (pPNh6Entry->pRtm6Cxt->u4ContextId == pRtm6Cxt->u4ContextId)
                {
                    TMO_DYN_SLL_Scan (&pPNh6Entry->routeEntryList, pPRt6,
                                      pPRt6NextEntry, tPRt6Entry *)
                    {
                        /* Delete the pending route entry from the list */
                        TMO_SLL_Delete (&pPNh6Entry->routeEntryList,
                                        (tTMO_SLL_NODE *) pPRt6);
                        MemReleaseMemBlock (gRtm6GlobalInfo.Rtm6URRtPoolId,
                                            (UINT1 *) pPRt6);
                    }
                    /* Removing the ECMP  Node Entry from RB Tree */
                    RBTreeRem (gRtm6GlobalInfo.pECMP6PRTRBRoot, pPNh6Entry);
                    MemReleaseMemBlock (gRtm6GlobalInfo.Rtm6PNextHopPoolId,
                                        (UINT1 *) pPNh6Entry);
                }
                pPNh6Entry = pNextPNh6Entry;
            }
        }

        /* End of L3 RED Support */

        if (pRtEntry->u1AddrType == ADDR6_ANYCAST)
        {
            i4OutCome =
                Rtm6UtilFwdTblGetBestAnycastRouteInCxt (pRtm6Cxt, pRtEntry,
                                                        &pIp6BestRtEntry);
        }
        else
        {
            i4OutCome =
                Rtm6UtilFwdTblGetBestRouteInCxt (pRtm6Cxt, pRtEntry,
                                                 &pIp6BestRtEntry);
        }
        if (i4OutCome == IP6_SUCCESS)
        {
            if (RTM6_GET_NODE_STATUS () == RM_ACTIVE)
            {
                pRtEntry->u1HwStatus = NP_PRESENT;
                Rtm6RedAddDynamicInfo (pRtEntry, pRtm6Cxt->u4ContextId);
                Rtm6RedSendDynamicInfo ();
            }
            if (RTM6_IS_NP_PROGRAMMING_ALLOWED () == RTM6_SUCCESS)
            {
                RouteInfo.u1RouteCount = pRtEntry->u1EcmpCount;
                RouteInfo.u4HwIntfId[0] = pRtEntry->u4HwIntfId[0];
                i4OutCome = Ipv6FsNpIpv6UcRouteDelete (pRtm6Cxt->u4ContextId,
                                                       (UINT1 *)
                                                       &pIp6BestRtEntry->dst,
                                                       pIp6BestRtEntry->
                                                       u1Prefixlen,
                                                       (UINT1 *) &pRtEntry->
                                                       nexthop,
                                                       pRtEntry->u4Index,
                                                       &RouteInfo);
            }
            if (i4OutCome == FNP_SUCCESS)
            {
                if (RTM6_GET_NODE_STATUS () == RM_ACTIVE)
                {
                    pRtEntry->u1HwStatus = NP_NOT_PRESENT;
                    Rtm6RedAddDynamicInfo (pRtEntry, pRtm6Cxt->u4ContextId);
                    Rtm6RedSendDynamicInfo ();
                }
            }

        }
#endif
        i4Status = Rtm6TrieDeleteEntryInCxt (pRtm6Cxt, pRtEntry);
        if (i4Status == TRIE_FAILURE)
        {
            RTM6_TRC (pRtm6Cxt->u4ContextId,
                      RTM6_ALL_FAILURE_TRC, RTM6_MOD_NAME,
                      "\tRoute deletion from RTM6 trie " "FAILED!!!\n");
        }
        gRtm6GlobalInfo.u4Ip6FwdTblRtNum--;
        pRtm6Cxt->u4Ip6FwdTblRouteNum--;
        IP6_RT_FREE (pRtEntry);
        pRtEntry = NULL;
        i4Status =
            TrieGetNextNode (&InParams, pRibNode, (VOID *) &pRtEntry,
                             &pRibNode);
    }

    while (pRtm6Cxt->u2Rtm6RtStartIndex != IP6_MAX_ROUTING_PROTOCOLS)
    {
        RegnId.u2ProtoId = (UINT2) (pRtm6Cxt->u2Rtm6RtStartIndex + 1);
        RegnId.u4ContextId = pRtm6Cxt->u4ContextId;
        Rtm6UtilDeregister (pRtm6Cxt, &RegnId);
    }

    TMO_DYN_SLL_Scan (&(pRtm6Cxt->Rtm6CtrlList), pRtm6CtrlInfo,
                      pRtm6NextCtrlInfo, tRrd6ControlInfo *)
    {
        TMO_SLL_Delete (&(pRtm6Cxt->Rtm6CtrlList), &(pRtm6CtrlInfo->pNext));
        RTM6_CONTROL_INFO_FREE (pRtm6CtrlInfo);
    }

    TMO_DYN_SLL_Scan (&(pRtm6Cxt->Rtm6InvdRtList), pRtm6InvdRt,
                      pRtm6NextInvdRt, tIp6RtEntry *)
    {
        TMO_SLL_Delete (&(pRtm6Cxt->Rtm6InvdRtList), &(pRtm6InvdRt->NextEntry));
        IP6_RT_FREE (pRtm6InvdRt);
    }

    pRtm6InvdRt = NULL;
    pRtm6NextInvdRt = NULL;

    TMO_DYN_SLL_Scan (&(gRtm6GlobalInfo.Rtm6RedisInitList), pNode, pTmpNode,
                      tRtm6RedisNode *)
    {
        if (pRtm6Cxt->u4ContextId == pNode->RegId.u4ContextId)
        {
            TMO_SLL_Delete (&gRtm6GlobalInfo.Rtm6RedisInitList,
                            &(pNode->NextNode));
            RTM6_REDIST_NODE_FREE (pNode);
        }
    }
    gRtm6GlobalInfo.apRtm6Cxt[pRtm6Cxt->u4ContextId] = NULL;
    RTM6_CONTEXT_FREE (pRtm6Cxt);
    return;
}

/*-------------------------------------------------------------------+
 *
 * Function           : Rtm6ProcessGRTimerExpiry
 *
 * Input(s)           : None 
 *
 * Output(s)          : None
 *
 * Returns            : None
 *
 * Action             : This function processes the GR Timer 
 *                      Expiry
 *         
+-------------------------------------------------------------------*/
VOID
Rtm6ProcessGRTimerExpiry (tRtm6RegnId * pRegnId)
{
    UINT2               u2RegnId = RTM6_INVALID_REGN_ID;
    tRtm6Cxt           *pRtm6Cxt = NULL;

    pRtm6Cxt = UtilRtm6GetCxt (pRegnId->u4ContextId);
    if (pRtm6Cxt == NULL)
    {
        return;
    }

    u2RegnId = Rtm6GetRegnIndexFromRegnIdInCxt (pRtm6Cxt, pRegnId);
    if (u2RegnId == RTM6_INVALID_REGN_ID)
    {
        return;
    }

    /* Process GR Timer expiry. Perform route cleanup for the specified 
       protocol and deregister the routing protocol from the 
       registration list as the protocol doesn`t come up
       within the specified graceful time period. */
    Rtm6TrieProcessGRRouteCleanUp (pRegnId, IP6_FAILURE);
    Rtm6UtilDeregister (pRtm6Cxt, pRegnId);
}

/*-------------------------------------------------------------------+
 * Function           : Rtm6StartGRTmr
 *
 * Input(s)           : pRegnID  - Router protcol ID and instance.
 *                      pBuf - Pointer to the incoming Buffer.
 *
 * Output(s)          : None
 *
 * Returns            : RTM_SUCCESS or RTM_FAILURE
 *
 * Action             : This function initialises and starts the GR 
 *                      Timer data structure for the particular 
 *                      routing protocol instance.
+-------------------------------------------------------------------*/

INT4
Rtm6StartGRTmr (tRtm6RegnId * pRegnId, tIp6Buf * pBuf)
{
    UINT4               u4RtmGRTimer = 0;
    UINT2               u2RegnId = RTM6_INVALID_REGN_ID;
    tRtm6Cxt           *pRtm6Cxt = NULL;

    pRtm6Cxt = UtilRtm6GetCxt (pRegnId->u4ContextId);
    if (pRtm6Cxt == NULL)
    {
        IP6_RELEASE_BUF (pBuf, FALSE);
        return (RTM6_FAILURE);
    }

    u2RegnId = Rtm6GetRegnIndexFromRegnIdInCxt (pRtm6Cxt, pRegnId);
    if (u2RegnId == RTM6_INVALID_REGN_ID)
    {
        IP6_RELEASE_BUF (pBuf, FALSE);
        return (RTM6_FAILURE);
    }

    if (IP6_COPY_FROM_BUF (pBuf, &u4RtmGRTimer, 0,
                           sizeof (UINT4)) != sizeof (UINT4))
    {
        IP6_RELEASE_BUF (pBuf, FALSE);
        return (RTM6_FAILURE);
    }

    IP6_RELEASE_BUF (pBuf, FALSE);

    pRtm6Cxt->aRtm6RegnTable[u2RegnId].Rtm6GRTmr.Rtm6GRTmrBlk.u1TimerId
        = RTM6_GR_TIMER;
    pRtm6Cxt->aRtm6RegnTable[u2RegnId].Rtm6GRTmr.u4GRTmrInterval = u4RtmGRTimer;

    /* Remove the following code, Once 1 sec timer is moved to TmrBlk */
    pRtm6Cxt->aRtm6RegnTable[u2RegnId].Rtm6GRTmr.
        Rtm6GRTmrBlk.TimerNode.u4Data = RTM6_GR_TIMER;

    if (TmrStart (gRtm6GlobalInfo.Rtm6TmrListId,
                  &(pRtm6Cxt->aRtm6RegnTable[u2RegnId].Rtm6GRTmr.
                    Rtm6GRTmrBlk), RTM6_GR_TIMER, u4RtmGRTimer,
                  0) != TMR_SUCCESS)
    {
        return (RTM6_FAILURE);
    }
    pRtm6Cxt->aRtm6RegnTable[u2RegnId].u1RestartState = RTM6_IGP_RESTARTING;
    return (RTM6_SUCCESS);
}

/*-------------------------------------------------------------------+
 * Function           : Rtm6StopGRTmr
 *
 * Input(s)           : pRegnID  - Registration Information.
 *
 * Output(s)          : None
 *
 * Returns            : None
 *
 * Action             : This function stops the running GR
 *                      Timer data structure of a particular 
 *                      routing protocol
+-------------------------------------------------------------------*/
VOID
Rtm6StopGRTmr (tRtm6RegnId * pRegnId)
{
    UINT2               u2RegnId = RTM6_INVALID_REGN_ID;
    tRtm6Cxt           *pRtm6Cxt = NULL;

    pRtm6Cxt = UtilRtm6GetCxt (pRegnId->u4ContextId);
    if (pRtm6Cxt == NULL)
    {
        return;
    }

    u2RegnId = Rtm6GetRegnIndexFromRegnIdInCxt (pRtm6Cxt, pRegnId);
    if (u2RegnId == RTM6_INVALID_REGN_ID)
    {
        return;
    }

    TmrStop (gRtm6GlobalInfo.Rtm6TmrListId,
             &(pRtm6Cxt->aRtm6RegnTable[u2RegnId].Rtm6GRTmr.Rtm6GRTmrBlk));

    return;
}

/*-------------------------------------------------------------------+
 *
 * Function           : Rtm6ProcessTimerExpiry
 *
 * Input(s)           : None
 *
 * Output(s)          : None
 *
 * Returns            : None
 *
 * Action             : This function process the expired timers in RTM.
 *
+-------------------------------------------------------------------*/
VOID
Rtm6ProcessTimerExpiry (VOID)
{
    tTmrAppTimer       *pTmr = NULL;
    tRtm6RegistrationInfo *pRtm6RegInfo = NULL;
    tRtm6GRTimer       *pRtm6GRTmr = NULL;
    tRtm6RegnId         RegnId;
    INT2                i2Offset = 0;
    UINT1               u1TimerId = 0;

    while ((pTmr =
            TmrGetNextExpiredTimer (gRtm6GlobalInfo.Rtm6TmrListId)) != NULL)
    {
        u1TimerId = ((tTmrBlk *) pTmr)->u1TimerId;
        switch (u1TimerId)
        {
            case RTM6_ONESEC_TIMER:

                /* One Second Timer has expired. Process it. */
                gRtm6GlobalInfo.u1OneSecTimerStart = FALSE;
                Rtm6ProcessOneSecTimerExpiry ();
                break;
            case RTM6_GR_TIMER:
                i2Offset = RTM6_OFFSET (tRtm6GRTimer, Rtm6GRTmrBlk);
                pRtm6GRTmr = (tRtm6GRTimer *) (VOID *)
                    ((UINT1 *) pTmr - i2Offset);
                i2Offset = 0;
                i2Offset = RTM6_OFFSET (tRtm6RegistrationInfo, Rtm6GRTmr);
                pRtm6RegInfo =
                    (tRtm6RegistrationInfo *) (VOID *)
                    ((UINT1 *) pRtm6GRTmr - i2Offset);

                MEMSET (&RegnId, 0, sizeof (tRtm6RegnId));
                RegnId.u2ProtoId = pRtm6RegInfo->u2RoutingProtocolId;
                RegnId.u4ContextId = pRtm6RegInfo->pRtm6Cxt->u4ContextId;
                Rtm6ProcessGRTimerExpiry (&RegnId);
                break;
            case RTM6_ECMP6PRT_TIMER:
                Rtm6ProcessECMP6PRTTimerExpiryInCxt ();
                break;

            case RTM6_FRT_TIMER:
                Rtm6ProcessFRTTimerExpiry ();
                break;

        }
    }
    return;
}

/*-------------------------------------------------------------------+
 *
 * Function           : Rtm6ProcessGRRouteCleanUp
 *
 * Input(s)           : pRegnID -      Protocol Info whose routes needs to be
 *                                     cleared.
 *                      i1GRCompFlag - GR Process competion Flag
 *                                     (RTM6_SUCCESS/RTM6_FAILURE)
 *
 * Output(s)          : None
 *
 * Returns            : None
 *
 * Action             : This function will cleanup all the old routes
 *                      updated before graceful restart, and resets the
 *                      IP6_GR_BIT updated during GR process.
+-------------------------------------------------------------------*/

VOID
Rtm6ProcessGRRouteCleanUp (tRtm6RegnId * pRegnId, INT1 i1GRCompFlag)
{
    RTM6_TASK_LOCK ();
    Rtm6TrieProcessGRRouteCleanUp (pRegnId, i1GRCompFlag);
    RTM6_TASK_UNLOCK ();
    return;
}

/* -------------------------------------------------------------------+
 * Function           : Rtm6GetIgpConvergenceState
 *
 * Input(s)           : u4ContextId.
 *
 * Output(s)          : None.
 *
 * Returns            : RTM6_SUCCESS/RTM6_FAILURE
 *
 * Action             : Returns the IGP converged state 
+-------------------------------------------------------------------*/

INT4
Rtm6GetIgpConvergenceStateInCxt (UINT4 u4ContextId)
{
    UINT2               u2Index = 0;
    tRtm6Cxt           *pRtm6Cxt = NULL;

    pRtm6Cxt = UtilRtm6GetCxt (u4ContextId);

    if (pRtm6Cxt == NULL)
    {
        return RTM6_FAILURE;
    }

    /* Initialise the RTM6 Registration Table */
    for (u2Index = 0; u2Index < IP6_MAX_ROUTING_PROTOCOLS; u2Index++)
    {
        /* If any IGP supports Graceful restarts need to be added in this check */
        if (u2Index == (OSPF6_ID - 1))
        {
            /* If the IGP is restarting then return failure */
            if (pRtm6Cxt->aRtm6RegnTable[u2Index].u1RestartState ==
                RTM6_IGP_RESTARTING)
            {
                return RTM6_FAILURE;
            }
        }
    }                            /* End of For */
    return RTM6_SUCCESS;
}

/* -------------------------------------------------------------------+
 * Function           : RTM6GetForwardingStateInCxt
 *
 * Input(s)           : u4ContextId.
 *
 * Output(s)          : None.
 *
 * Returns            : RTM6_SUCCESS/RTM6_FAILURE
 *
 * Action             : Returns the forwarding state
+-------------------------------------------------------------------*/

INT4
RTM6GetForwardingStateInCxt (UINT4 u4ContextId)
{
    UNUSED_PARAM (u4ContextId);
    return RTM6_SUCCESS;
}

/************************************************************************/
/* Function           : Rtm6ProcessECMP6PRTTimerExpiryInCxt             */
/*                                                                      */
/* Input(s)           : pRtm6Cxt - RTM6 Context Pointer                 */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/*                                                                      */
/* Action             : This function processes the expiry of  ECMP6PRT */
/*                      Timer.This timer verifies the reachability of   */
/*                      ECMP6 routes available in ECMP6PRT periodically */
/************************************************************************/
VOID
Rtm6ProcessECMP6PRTTimerExpiryInCxt (VOID)
{
    tPNh6Node          *pPNHNode = NULL;
    tPNh6Node           ExstNhNode;
    UINT4               u4Cnt;
    UINT4               u4IfIndex;
    tNetIpv6RtInfo      NetIpv6RtInfo;
    tNetIpv6RtInfoQueryMsg RtQuery;
    BOOL1               b1LLFlag = FALSE;

    /* RB Tree to resolve for all the next hops in PRT and for ECMP */
    MEMSET (&ExstNhNode, 0, sizeof (tPNh6Node));
    pPNHNode = (tPNh6Node *) RBTreeGetFirst (gRtm6GlobalInfo.pECMP6PRTRBRoot);

    while (pPNHNode != NULL)
    {
        /*Need to try to resolve ND for the entries */
        /*Need to check if entry is resolved. If it is not resolved, try to resolve it.
         * If entry is resolved, then remove it from ECMP6 PRT.
         */

        MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));
        MEMSET (&RtQuery, 0, sizeof (tNetIpv6RtInfoQueryMsg));
        RtQuery.u4ContextId = pPNHNode->pRtm6Cxt->u4ContextId;
        RtQuery.u1QueryFlag = RTM6_QUERIED_FOR_NEXT_HOP;
        Ip6AddrCopy (&NetIpv6RtInfo.Ip6Dst, &pPNHNode->NextHop);
        NetIpv6RtInfo.u1Prefixlen = IP6_ADDR_MAX_PREFIX;

        if (IS_ADDR_LLOCAL (NetIpv6RtInfo.Ip6Dst) == TRUE)
        {
            b1LLFlag = TRUE;
        }

        if ((Rtm6NetIpv6GetRoute (&RtQuery, &NetIpv6RtInfo) == RTM6_SUCCESS)
            || (b1LLFlag == TRUE))
        {

            if (b1LLFlag == TRUE)
            {
                u4IfIndex = pPNHNode->u4IfIndex;
            }
            else
            {

                u4IfIndex = NetIpv6RtInfo.u4Index;
            }

#ifdef LNXIP6_WANTED
            /* Rtm6 lock has been taken in this thread already in Rtm6mainTsk 
             * and will be take inside this Lip6NlResolveNd API as well. 
             * To avoid deadlock this lock has been released here 
             * and acquired after calling this API*/
            /*Need to resolve ND */
            RTM6_TASK_UNLOCK ();
            Lip6NlResolveNd (u4IfIndex, &pPNHNode->NextHop);
            RTM6_TASK_LOCK ();

#else
            Rtm6NDEvtSend (u4IfIndex, &pPNHNode->NextHop);
#endif

        }
        ExstNhNode.NextHop = pPNHNode->NextHop;
        ExstNhNode.pRtm6Cxt = pPNHNode->pRtm6Cxt;
        pPNHNode =
            RBTreeGetNext (gRtm6GlobalInfo.pECMP6PRTRBRoot, &ExstNhNode, NULL);
    }

    /*If ECMP6 PRT count not null, Process timer */
    RBTreeCount (gRtm6GlobalInfo.pECMP6PRTRBRoot, &u4Cnt);

    if ((u4Cnt != 0) && (gu4ECMP6TmrFlag == 0))
    {
        Rtm6TmrSetTimer (&(gRtm6GlobalInfo.Rtm6ECMP6PRTTmr),
                         RTM6_ECMP6PRT_TIMER, RTM6_ECMP6PRT_TIMER_INTERVAL);
    }
    else
    {
        gu4ECMP6TmrFlag = 1;
    }

}

/*-------------------------------------------------------------------+
 *
 * Function           : Rtm6ProcessFRTTimerExpiry
 *
 * Input(s)           : None 
 *
 * Output(s)          : None
 *
 * Returns            : None
 *
 * Action             : This function processes the One Second Timer 
 *                      Expiry
 *         
+-------------------------------------------------------------------*/
VOID
Rtm6ProcessFRTTimerExpiry (VOID)
{
    tRtm6FrtInfo        Rtm6FrtInfo;
    tRtm6FrtInfo       *pNextRtm6FrtInfo = NULL;
    tIp6RtEntry         Ip6RtEntry;
    UINT1               u1RetryRtCount = 0;

    MEMSET (&Rtm6FrtInfo, 0, sizeof (tRtm6FrtInfo));
    MEMSET (&Ip6RtEntry, 0, sizeof (tIp6RtEntry));
    Rtm6FrtInfo.u4CxtId = gRtm6GlobalInfo.Rtm6FrtInfo.u4CxtId;
    MEMCPY (&(Rtm6FrtInfo.DestPrefix),
            &(gRtm6GlobalInfo.Rtm6FrtInfo.DestPrefix), IPVX_IPV6_ADDR_LEN);
    MEMCPY (&(Rtm6FrtInfo.NextHopAddr),
            &(gRtm6GlobalInfo.Rtm6FrtInfo.NextHopAddr), IPVX_IPV6_ADDR_LEN);
    Rtm6FrtInfo.u1PrefixLen = gRtm6GlobalInfo.Rtm6FrtInfo.u1PrefixLen;
    pNextRtm6FrtInfo = Rtm6GetNextFrtEntry (&Rtm6FrtInfo);
    while (pNextRtm6FrtInfo != NULL)
    {
        MEMSET (&Rtm6FrtInfo, 0, sizeof (tRtm6FrtInfo));
        /* Np reprogramming must be tried only in active. As Standby progrmaming
         * will be taken care via IPC call*/
        if (RTM6_GET_NODE_STATUS () == RM_ACTIVE)
        {
            if (Rtm6UtilRetryNpPrgForFailedRoute (pNextRtm6FrtInfo) ==
                RTM6_SUCCESS)
            {
                if (Rtm6FrtDeleteEntry (pNextRtm6FrtInfo) == RTM6_SUCCESS)
                {
                    MEMCPY (&(Ip6RtEntry.dst), &(pNextRtm6FrtInfo->DestPrefix),
                            IPVX_IPV6_ADDR_LEN);
                    MEMCPY (&(Ip6RtEntry.nexthop),
                            &(pNextRtm6FrtInfo->NextHopAddr),
                            IPVX_IPV6_ADDR_LEN);
                    Ip6RtEntry.u1Prefixlen = pNextRtm6FrtInfo->u1PrefixLen;
                    Ip6RtEntry.i1Proto = pNextRtm6FrtInfo->i1Proto;
                    Ip6RtEntry.u1EcmpCount = pNextRtm6FrtInfo->u1RtCount;
                    Rtm6RedSyncFrtInfo (&Ip6RtEntry, pNextRtm6FrtInfo->u4CxtId,
                                        (UINT1) pNextRtm6FrtInfo->u1Type,
                                        RTM6_DELETE_ROUTE);
                }
            }
        }
        u1RetryRtCount++;
        if (u1RetryRtCount > MAX_RTM6_RETRY_ROUTE_COUNT)
        {
            break;                /* Store the node info in global var and Restart the timer */
        }
        Rtm6FrtInfo.u4CxtId = pNextRtm6FrtInfo->u4CxtId;
        MEMCPY (&(Rtm6FrtInfo.DestPrefix), &(pNextRtm6FrtInfo->DestPrefix),
                IPVX_IPV6_ADDR_LEN);
        MEMCPY (&(Rtm6FrtInfo.NextHopAddr), &(pNextRtm6FrtInfo->NextHopAddr),
                IPVX_IPV6_ADDR_LEN);
        Rtm6FrtInfo.u1PrefixLen = gRtm6GlobalInfo.Rtm6FrtInfo.u1PrefixLen;
        pNextRtm6FrtInfo = Rtm6GetNextFrtEntry (&Rtm6FrtInfo);
    }
    if (pNextRtm6FrtInfo != NULL)
    {
        gRtm6GlobalInfo.Rtm6FrtInfo.u4CxtId = pNextRtm6FrtInfo->u4CxtId;
        MEMCPY (&(gRtm6GlobalInfo.Rtm6FrtInfo.DestPrefix),
                &(pNextRtm6FrtInfo->DestPrefix), IPVX_IPV6_ADDR_LEN);
        MEMCPY (&(gRtm6GlobalInfo.Rtm6FrtInfo.NextHopAddr),
                &(pNextRtm6FrtInfo->NextHopAddr), IPVX_IPV6_ADDR_LEN);
        gRtm6GlobalInfo.Rtm6FrtInfo.u1PrefixLen = pNextRtm6FrtInfo->u1PrefixLen;
    }
    else
    {
        MEMSET (&gRtm6GlobalInfo.Rtm6FrtInfo, 0, sizeof (tRtm6FrtInfo));
        MEMSET (&(gRtm6GlobalInfo.Rtm6FrtInfo.DestPrefix), 0,
                IPVX_IPV6_ADDR_LEN);
        MEMSET (&(gRtm6GlobalInfo.Rtm6FrtInfo.NextHopAddr), 0,
                IPVX_IPV6_ADDR_LEN);
    }

    Rtm6TmrSetTimer (&(gRtm6GlobalInfo.Rtm6FRTTmr), RTM6_FRT_TIMER,
                     RTM6_FRT_TIMER_INTERVAL);
}

/*-------------------------------------------------------------------+
* Function           : Rtm6StopECMPTmr
*
* Input(s)           : pRtm6Cxt -Pointer to the RTM6 Context
*
* Output(s)          : None
*
* Returns            : None
*
* Action             : This function stops the running ECMP 
*                      Timer data structure. 
+-------------------------------------------------------------------*/
#if 0
VOID
Rtm6StopECMPTmr (tRtm6Cxt * pRtm6Cxt)
{

    if (pRtm6Cxt == NULL)
    {
        return;
    }

    if (TmrStop (gRtm6GlobalInfo.Rtm6TmrListId, &(pRtm6Cxt->Rtm6ECMP6PRTTmr)) ==
        TMR_SUCCESS)
    {
        gu4ECMP6TmrFlag = 1;
    }

    return;
}
#endif
/******************************************************************************
* FUNCTION    : Rtm6NDEvtSend           
* DESCRIPTION : This function is used to send an event to IP6 task to
*               resolve the next hop of the ECMPv6 route
* INPUTS      : pPNHNode - Pending Next Hop Node
*              
* OUTPUTS     : None
* RETURNS     : OSIX_SUCCESS/OSIX_FAILURE
* NOTES       :
******************************************************************************/
#ifndef LNXIP6_WANTED
INT4
Rtm6NDEvtSend (UINT4 u4IfIndex, tIp6Addr * pNextHop)
{

    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    tNdData            *pNdData = NULL;

    pBuf = IP6_ALLOCATE_BUF (sizeof (tNdData), 0);
    if (pBuf == NULL)
    {
        return OSIX_FAILURE;
    }
    pNdData = (tNdData *) IP6_GET_MODULE_DATA_PTR (pBuf);
    pNdData->u1LinkType = IP6_L3VLAN_INTERFACE_TYPE;
    pNdData->u4IfIndex = u4IfIndex;
    MEMCPY (&pNdData->NextHop, pNextHop, sizeof (tIp6Addr));

    Ipv6NdEventSend (pBuf);

    return OSIX_SUCCESS;
}

#endif
