/*******************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rtm6cli.c,v 1.19 2016/03/24 10:27:41 siva Exp $
 *
 * Description: Action routines of IP module specific commands
 *
 *******************************************************************/
#ifndef __RTM6CLI_C__
#define __RTM6CLI_C__

#include "rtm6inc.h"
#include "rtm6cli.h"
#include "vcm.h"
#include "fsrtmlow.h"
#include "fsmrt6lw.h"

const CHR1         *paRrd6ProtoTable[] = {
    "all",
    "other",
    "local",
    "static",
    "ndisc",
    "rip",
    "ospf",
    "bgp",
    "idrp",
    "igrp"
};

/**************************************************************************/
/*  Function Name   : cli_process_rrd6_cmd                                */
/*                                                                        */
/*  Description     : Protocol CLI message handler function               */
/*                                                                        */
/*  Input(s)        : CliHandle - CliContext ID                           */
/*                    u4Command - Command identifier                      */
/*                    ... -Variable command argument list                 */
/*                                                                        */
/*  Output(s)       : None                                                */
/*                                                                        */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                             */
/**************************************************************************/
INT4
cli_process_rrd6_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    tIp6Addr            DestAddr;
    UINT4              *args[RRD6_CLI_MAX_ARGS];
    UINT4               u4ErrCode = 0;
    UINT4               u4MaxRoute = 0;
    UINT4               u4Rtm6CxtId = VCM_INVALID_VC;
    INT4                i4IfaceIndex = 0;
    INT4                i4CtrlFlag = 0;
    INT4                i4RetStatus = CLI_SUCCESS;
    UINT2               u2SrcProto = 0;
    UINT1               u1ExportFlag = 0;
    UINT1              *pu1Rtm6CxtName = NULL;
    UINT1               u1ProtoType = 0;
    INT1                i1argno = 0;
    INT1                i1IsShowCommand = FALSE;

    UNUSED_PARAM (i4IfaceIndex);

    va_start (ap, u4Command);

    /* third argument is always InterfaceName/Index */
    i4IfaceIndex = va_arg (ap, INT4);

    /* Fourth argument is always ContextName */
    pu1Rtm6CxtName = va_arg (ap, UINT1 *);

    /* Walk through the rest of the arguments and store in args array. 
     * Store RRD_CLI_MAX_ARGS arguements at the max. 
     */

    while (1)
    {
        args[i1argno++] = va_arg (ap, UINT4 *);
        if (i1argno == RRD6_CLI_MAX_ARGS)
        {
            break;
        }
    }
    va_end (ap);

    CliRegisterLock (CliHandle, Rtm6Lock, Rtm6UnLock);

    RTM6_TASK_LOCK ();

    /*Get context Id from context Name */
    if ((pu1Rtm6CxtName != NULL) && (u4Command != CLI_RRD6_RED_DYN_INFO))
    {
        if (UtilRtm6GetCxtIdFromCxtName (pu1Rtm6CxtName,
                                         &u4Rtm6CxtId) == RTM6_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Invalid VRF Id\r\n");
            CliUnRegisterLock (CliHandle);
            RTM6_TASK_UNLOCK ();
            return CLI_FAILURE;
        }
    }
    if (Rtm6CliGetCommandType (u4Command, &i1IsShowCommand) == CLI_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Invalid Command Identifier\r\n");
        CliUnRegisterLock (CliHandle);
        RTM6_TASK_UNLOCK ();
        return CLI_FAILURE;
    }
    if (i1IsShowCommand == TRUE)
    {
        if (pu1Rtm6CxtName == NULL)
        {
            /* Show command should display for all the
               Context */
            u4Rtm6CxtId = VCM_INVALID_VC;
        }

        switch (u4Command)
        {

            case CLI_RRD6_SHOW_CONTROL_INFO:
                i4RetStatus = ShowRtm6ControlInfoInCxt (CliHandle, u4Rtm6CxtId);
                break;

            case CLI_RRD6_SHOW_STATUS:
                i4RetStatus = ShowRrd6StatusInCxt (CliHandle, u4Rtm6CxtId);
                break;

            case CLI_RRD6_SHOW_MEM_RES:
                i4RetStatus = ShowRtm6RouteMemReservation(CliHandle);
                break;

#ifdef RM_WANTED
            case CLI_RRD6_RED_DYN_INFO:
                i4RetStatus = ShowRtm6RedDynInfo (CliHandle);
                break;
#endif

            default:
                CliPrintf (CliHandle, "\r%Invalid Command\r\n");
                i4RetStatus = CLI_FAILURE;
                break;
        }
    }
    else
    {
        if (pu1Rtm6CxtName == NULL)
        {
            /* IF context name is not provided then configuration should be
             * done in default RTM context */
            u4Rtm6CxtId = RTM6_DEFAULT_CXT_ID;
        }
        /* Set Context Id for RTM6, which will be used by the following
           configuration commands */
        if (UtilRtm6SetContext (u4Rtm6CxtId) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Invalid Rtm6 Context Id\r\n");
            CliUnRegisterLock (CliHandle);
            RTM6_TASK_UNLOCK ();
            return CLI_FAILURE;
        }
        switch (u4Command)
        {
            case CLI_RRD6_DEF_CONTROL_STATUS:
                /* args[0] - Flag Indicating Permit/Deny */
                i4CtrlFlag = CLI_PTR_TO_I4 (args[0]);
                i4RetStatus = CliRrd6DefControlEntry (CliHandle, i4CtrlFlag);
                break;

            case CLI_RRD6_REDISTRIBUTE_POLICY:
                /* args[0] - Flag Indicating Permit/Deny */
                /* args[1] - Destination Network */
                /* args[2] -  Address Range */
                /* args[3] - Source Protocol ID */
                /* args[4] - Destination Protocol Mask */
                if (INET_ATON6 (args[1], &DestAddr) == 0)
                {
                    CliPrintf (CliHandle, "\r% Invalid IP6 Address\r\n");

                    RTM6_TASK_UNLOCK ();
                    CliUnRegisterLock (CliHandle);
                    return (CLI_FAILURE);
                }
                u1ExportFlag = (UINT1) CLI_PTR_TO_U4 (args[0]);
                u2SrcProto = (UINT2) CLI_PTR_TO_I4 (args[3]);
                i4RetStatus = CliRrd6AddControlTable (CliHandle, u1ExportFlag,
                                                      &DestAddr,
                                                      *args[2],
                                                      u2SrcProto,
                                                      CLI_PTR_TO_I4 (args[4]));
                break;

            case CLI_RRD6_NO_REDISTRIBUTE_POLICY:
                /* args[0] - Destination Network */
                /* args[1] -  Address Range */
                if (INET_ATON6 (args[0], &DestAddr) == 0)
                {
                    CliPrintf (CliHandle, "\r% Invalid IP6 Address\r\n");

                    RTM6_TASK_UNLOCK ();
                    CliUnRegisterLock (CliHandle);
                    return (CLI_FAILURE);
                }
                i4RetStatus = CliRrd6DelControlTable (CliHandle, &DestAddr,
                                                      *args[1]);
                break;

            case CLI_RRD6_EXPORT_OSPF:
                /* args[0] - Flag Indicating Area Route/External Route */
                /* args[1] - Destination Protocol */
                if (CLI_PTR_TO_U4 (args[0]) == CLI_RRD6_OSPF_AREA_RT)
                {
                    i4RetStatus = CliRrd6OspfAreaStatus (CliHandle,
                                                         CLI_PTR_TO_I4 (args
                                                                        [1]),
                                                         CLI_ENABLE);
                }
                else if (CLI_PTR_TO_U4 (args[0]) == CLI_RRD6_OSPF_EXT_RT)
                {
                    i4RetStatus = CliRrd6OspfExtStatus (CliHandle,
                                                        CLI_PTR_TO_I4 (args[1]),
                                                        CLI_ENABLE);
                }
                break;

            case CLI_RRD6_NO_EXPORT_OSPF:
                /* args[0] - Flag Indicating Area Route/External Route */
                /* args[1] - Destination Protocol */
                if (CLI_PTR_TO_U4 (args[0]) == CLI_RRD6_OSPF_AREA_RT)
                {
                    i4RetStatus = CliRrd6OspfAreaStatus (CliHandle,
                                                         CLI_PTR_TO_I4 (args
                                                                        [1]),
                                                         CLI_DISABLE);
                }
                else if (CLI_PTR_TO_U4 (args[0]) == CLI_RRD6_OSPF_EXT_RT)
                {
                    i4RetStatus = CliRrd6OspfExtStatus (CliHandle,
                                                        CLI_PTR_TO_I4 (args[1]),
                                                        CLI_DISABLE);
                }
                break;

            case CLI_RRD6_THROT_VALUE:
                i4RetStatus = CliRrd6ThrotValue (CliHandle, *args[0]);
                break;

            case CLI_RRD6_MAX_MEM_RES:
               /* args [0] --> Protocol type
                * args [1] --> Value to be configured
                * args [2] --> Flag to reset memory to ZERO Blocks
                */
               u1ProtoType = (UINT1) CLI_PTR_TO_U4(args[0]);
              if (CLI_PTR_TO_U4(args[2]) == DISABLED )
              {
                      switch ( u1ProtoType )
                      {
                              case IP6_NETMGMT_PROTOID: u4MaxRoute = DEF_RTM6_STATIC_ROUTE_ENTRIES;
                                                        break;
                              case BGP6_ID: u4MaxRoute = DEF_RTM6_BGP_ROUTE_ENTRIES;
                                            break;
                              case OSPF6_ID: u4MaxRoute = DEF_RTM6_OSPF_ROUTE_ENTRIES;
                                             break;
                              case RIPNG_ID: u4MaxRoute = DEF_RTM6_RIP_ROUTE_ENTRIES;
                                             break;
                              case ISIS6_ID: u4MaxRoute = DEF_RTM6_ISIS_ROUTE_ENTRIES;
                                             break;
                              default:
                                             break;
                                        
                      }
              }
              else
              {
                      u4MaxRoute = (*(UINT4 *) (args[1]));
              }

               i4RetStatus = Rtm6SetMaxRoute (u1ProtoType, u4MaxRoute);

                break;


            default:
                CliPrintf (CliHandle, "\r%Invalid Command\r\n");
                i4RetStatus = CLI_FAILURE;
                break;
        }
        /* Reset RTM6 Context */
        UtilRtm6ResetContext ();
    }

    if ((i4RetStatus == (INT4) CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode > 0) && (u4ErrCode < CLI_RRD6_MAX_ERR))
        {
            CliPrintf (CliHandle, "%s", Rrd6CliErrString[u4ErrCode]);
        }
        CLI_SET_ERR (0);
    }
    CLI_SET_CMD_STATUS (i4RetStatus);

    RTM6_TASK_UNLOCK ();

    CliUnRegisterLock (CliHandle);

    return i4RetStatus;
}


/*****************************************************************************/
/*                                                                           */
/* Function Name    : Rtm6SetMaxRoute                                       */
/*                                                                           */
/* Description      : This function configures the maximum number of routes  */
/*                    to be programmed in RTM6 on per protocol basis          */
/* Input Parameters :                                                        */
/*                    1. u1ProtoType - Protocol type                         */
/*                    2. u4RouteDest - Maximum number of routes              */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/
INT4 Rtm6SetMaxRoute (UINT1 u1ProtoType, UINT4 u4MaxRTM6Route)
{
       UINT4 u4ErrCode = 0;

       switch (u1ProtoType)
       {
               case BGP6_ID:
                           if (nmhTestv2FsRrd6MaximumBgpRoutes(&u4ErrCode , u4MaxRTM6Route) == SNMP_FAILURE)
                             {
                                    return CLI_FAILURE;
                             }
                           if(nmhSetFsRrd6MaximumBgpRoutes(u4MaxRTM6Route) == SNMP_FAILURE)
                             {
                                    return CLI_FAILURE;
                             }
                       break;

               case OSPF6_ID:

                           if (nmhTestv2FsRrd6MaximumOspfRoutes(&u4ErrCode , u4MaxRTM6Route) == SNMP_FAILURE)
                             {
                                    return CLI_FAILURE;
                             }
                           if(nmhSetFsRrd6MaximumOspfRoutes(u4MaxRTM6Route)== SNMP_FAILURE)
                             {
                                    return CLI_FAILURE;
                             }
                       break;

               case RIPNG_ID:

                           if (nmhTestv2FsRrd6MaximumRipRoutes(&u4ErrCode , u4MaxRTM6Route) == SNMP_FAILURE)
                             {
                                    return CLI_FAILURE;
                             }
                           if (nmhSetFsRrd6MaximumRipRoutes(u4MaxRTM6Route)== SNMP_FAILURE)
                             {
                                    return CLI_FAILURE;
                             }

                       break;
               case IP6_NETMGMT_PROTOID:

                           if (nmhTestv2FsRrd6MaximumStaticRoutes(&u4ErrCode , u4MaxRTM6Route) == SNMP_FAILURE)
                             {
                                    return CLI_FAILURE;
                             }
                           if (nmhSetFsRrd6MaximumStaticRoutes(u4MaxRTM6Route) == SNMP_FAILURE)
                             {
                                    return CLI_FAILURE;
                             }

                       break;
               case IP6_ISIS_PROTOID:

                           if (nmhTestv2FsRrd6MaximumISISRoutes(&u4ErrCode , u4MaxRTM6Route) == SNMP_FAILURE)
                             {
                                    return CLI_FAILURE;
                             }
                           if (nmhSetFsRrd6MaximumISISRoutes(u4MaxRTM6Route) == SNMP_FAILURE)
                             {
                                    return CLI_FAILURE;
                             }

                       break;
            default:
                       break;

       }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : ShowRtm6RouteMemReservation                           */
/*                                                                           */
/* Description      : This function configures the maximum number of routes  */
/*                    to be programmed in RTM on per protocol basis          */
/* Input Parameters :                                                        */
/*                    1. CliHandle - CliContext ID                           */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/
INT4 ShowRtm6RouteMemReservation ( tCliHandle CliHandle)
{

  UINT4 u4TotalBlks = 0;
  UINT4 u4BlksReserved = 0;
  UINT4 u4Temp = 0;
  INT1  i1Status = SNMP_FAILURE;

  u4TotalBlks = FsRTM6SizingParams [MAX_RTM6_ROUTE_TABLE_ENTRIES_SIZING_ID].u4PreAllocatedUnits;

  CliPrintf (CliHandle, "Protocol Type     Blocks reserved\r\n");
  CliPrintf (CliHandle, "--------------------------------------\r\n");
  CliPrintf (CliHandle, "Total             %-32d\r\n",u4TotalBlks);
  CliPrintf (CliHandle, "connected         %-32d\r\n",SYS_DEF_MAX_INTERFACES);
  i1Status = nmhGetFsRrd6MaximumStaticRoutes (&u4Temp);
  CliPrintf (CliHandle, "static            %-32d\r\n",u4Temp);
  u4BlksReserved += u4Temp;
  u4Temp = 0;
  i1Status = nmhGetFsRrd6MaximumRipRoutes (&u4Temp);
  CliPrintf (CliHandle, "rip6              %-32d\r\n",u4Temp);
  u4BlksReserved += u4Temp;
  u4Temp = 0;
  i1Status = nmhGetFsRrd6MaximumBgpRoutes (&u4Temp );
  CliPrintf (CliHandle, "bgp6              %-32d\r\n",u4Temp);
  u4BlksReserved += u4Temp;
  u4Temp = 0;
  i1Status = nmhGetFsRrd6MaximumOspfRoutes (&u4Temp);
  CliPrintf (CliHandle, "ospf6             %-32d\r\n",u4Temp);
  u4BlksReserved += u4Temp;
  u4Temp = 0;
  i1Status = nmhGetFsRrd6MaximumISISRoutes (&u4Temp );
  CliPrintf (CliHandle, "isis6             %-32d\r\n",u4Temp);
  u4BlksReserved += u4Temp;
  u4Temp = 0;
  u4BlksReserved += SYS_DEF_MAX_INTERFACES;
  u4Temp = u4TotalBlks - u4BlksReserved;
  CliPrintf (CliHandle, "Free Blocks       %-32d\r\n\r\n",u4Temp);

  UNUSED_PARAM(i1Status);
  return CLI_SUCCESS;

}


/*********************************************************************
*  Function Name : CliRrd6DefControlEntry
*  Description   : Calls Low level Set Routines for Configuring RRD6
*                  control table default entry.
*                  
*  Input(s)      : CliHandle - CliContext ID                         
*                : i4Flag - Default Permit/Deny Entry
*  Output(s)     : None 
*  Returns       : CLI_SUCCESS
**********************************************************************/
INT4
CliRrd6DefControlEntry (tCliHandle CliHandle, INT4 i4Flag)
{
    CliRrd6ModDefCtrlEntry (CliHandle, i4Flag);
    return CLI_SUCCESS;
}

/*********************************************************************
*  Function Name : CliRrd6AddControlTable
*  Description   : Calls Low level Set Routines for adding entries into RRD6
*                  control table.
*                  
*  Input(s)      : CliHandle - CliContext ID                           
*                : u1ExportFlag - To Identify either Permit or Deny Entry
*                : pIp6DestAddr - Destination Network  
*                : u4DestRange  - Address Range
*                : i4SrcProcol  - Source Protocol  
*                : i4DestProtoMask - Destination Protocol Mask
*  Output(s)     : None. 
*  Returns       : CLI_SUCCESS/CLI_FAILURE               
**********************************************************************/
INT4
CliRrd6AddControlTable (tCliHandle CliHandle, UINT1 u1ExportFlag,
                        tIp6Addr * pIp6DestAddr, UINT4 u4DestRange,
                        UINT2 u2SrcProtocol, INT4 i4DestProtoMask)
{
    tSNMP_OCTET_STRING_TYPE DestIpAddr;
    UINT1               u1TempAddr[IP6_ADDR_SIZE];
    UINT4               u4ErrorCode = 0;
    UINT2               u2SrcProto = 0;

    MEMSET (u1TempAddr, 0, IP6_ADDR_SIZE);
    DestIpAddr.pu1_OctetList = u1TempAddr;
    DestIpAddr.i4_Length = IP6_ADDR_SIZE;
    MEMCPY (DestIpAddr.pu1_OctetList, pIp6DestAddr->u1_addr, IP6_ADDR_SIZE);

    if (u2SrcProtocol == RRD6_CLI_STATIC)
    {
        u2SrcProto = IP6_NETMGMT_PROTOID;
    }
    else if (u2SrcProtocol == RRD6_CLI_LOCAL)
    {
        u2SrcProto = IP6_LOCAL_PROTOID;
    }
    else if (u2SrcProtocol == RRD6_CLI_RIP)
    {
        u2SrcProto = RIPNG_ID;
    }
    else if (u2SrcProtocol == RRD6_CLI_OSPF)
    {
        u2SrcProto = OSPF6_ID;
    }
    else if (u2SrcProtocol == RRD6_CLI_ALL)
    {
        u2SrcProto = RTM6_ANY_PROTOCOL;
    }

    if (nmhTestv2FsRrd6ControlRowStatus (&u4ErrorCode, &DestIpAddr, u4DestRange,
                                         CREATE_AND_WAIT) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetFsRrd6ControlRowStatus (&DestIpAddr, u4DestRange,
                                      CREATE_AND_WAIT) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    u4ErrorCode = 0;
    if (nmhTestv2FsRrd6ControlSourceProto (&u4ErrorCode, &DestIpAddr,
                                           u4DestRange,
                                           u2SrcProto) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetFsRrd6ControlSourceProto (&DestIpAddr, u4DestRange, u2SrcProto) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    u4ErrorCode = 0;
    if (nmhTestv2FsRrd6ControlDestProto (&u4ErrorCode, &DestIpAddr, u4DestRange,
                                         i4DestProtoMask) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetFsRrd6ControlDestProto (&DestIpAddr, u4DestRange, i4DestProtoMask)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    u4ErrorCode = 0;
    if (nmhTestv2FsRrd6ControlRouteExportFlag (&u4ErrorCode, &DestIpAddr,
                                               u4DestRange,
                                               u1ExportFlag) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetFsRrd6ControlRouteExportFlag
        (&DestIpAddr, u4DestRange, u1ExportFlag) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    u4ErrorCode = 0;
    if (nmhTestv2FsRrd6ControlRowStatus (&u4ErrorCode, &DestIpAddr, u4DestRange,
                                         ACTIVE) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetFsRrd6ControlRowStatus (&DestIpAddr, u4DestRange, ACTIVE)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    return (CLI_SUCCESS);
}

/*********************************************************************
*  Function Name : CliRrd6DelControlTable
*  Description   : Calls Low level Set Routines for deleting Entries from RRD6
*                  control table.
*                  
*  Input(s)      : CliHandle - CliContext ID                           
*                : pIp6DestAddr   - Destination Address 
*                : u4DestRange - Address Range
*  Output(s)     : None. 
*  Returns       : CLI_SUCCESS/CLI_FAILURE                             
**********************************************************************/
INT4
CliRrd6DelControlTable (tCliHandle CliHandle, tIp6Addr * pIp6DestAddr,
                        UINT4 u4DestRange)
{
    tSNMP_OCTET_STRING_TYPE DestIpAddr;
    UINT1               u1TempAddr[IP6_ADDR_SIZE];
    UINT4               u4ErrorCode = 0;

    MEMSET (u1TempAddr, 0, IP6_ADDR_SIZE);
    DestIpAddr.pu1_OctetList = u1TempAddr;
    DestIpAddr.i4_Length = IP6_ADDR_SIZE;
    MEMCPY (DestIpAddr.pu1_OctetList, pIp6DestAddr->u1_addr, IP6_ADDR_SIZE);

    if (nmhTestv2FsRrd6ControlRowStatus (&u4ErrorCode, &DestIpAddr, u4DestRange,
                                         DESTROY) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetFsRrd6ControlRowStatus (&DestIpAddr, u4DestRange,
                                      DESTROY) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    return (CLI_SUCCESS);
}

/*********************************************************************
*  Function Name : CliRrd6OspfAreaStatus
*  Description   : Calls Low level Set Routines to set the status of
                    Ospf Area routes redistribution for protocol.
*  Input(s)      : CliHandle - CliContext ID                         
*                : i4DestProtoId - Destination Protocol ID
*                : i4OspfRtStatus -  Enable/Disable Status
*  Output(s)     : None
*  Returns       : CLI_SUCCESS/CLI_FAILURE                             
**********************************************************************/
INT4
CliRrd6OspfAreaStatus (tCliHandle CliHandle, INT4 i4DestProtoId,
                       INT4 i4OspfRtStatus)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsRrd6AllowOspfAreaRoutes (&u4ErrorCode, i4DestProtoId,
                                            i4OspfRtStatus) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetFsRrd6AllowOspfAreaRoutes (i4DestProtoId, i4OspfRtStatus)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    return (CLI_SUCCESS);
}

/*********************************************************************
*  Function Name : CliRrd6OspfExtStatus
*  Description   : Calls Low level Set Routines to set the status of
                    Ospf External routes redistribution for protocol.
*                  
*  Input(s)      : i4DestProtoId - Destination Protocol ID
*                  i4OspfRtStatus -  Enable/Disable Status
*  Output(s)     : None 
*  Returns       : CLI_SUCCESS/CLI_FAILURE                            
**********************************************************************/
INT4
CliRrd6OspfExtStatus (tCliHandle CliHandle, INT4 i4DestProtoId,
                      INT4 i4OspfRtStatus)
{
    UINT4               u4ErrorCode;

    if (nmhTestv2FsRrd6AllowOspfExtRoutes (&u4ErrorCode, i4DestProtoId,
                                           i4OspfRtStatus) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetFsRrd6AllowOspfExtRoutes (i4DestProtoId, i4OspfRtStatus)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    return (CLI_SUCCESS);
}

/*********************************************************************
*  Function Name : ShowRtm6ControlInfoInCxt
*  Description   : Calls Low level GetFirst/Next Routines and
                    displays fsRrd6ControlTable.
*                  
*  Input(s)      : CliHandle - CliContext ID                          
*  Output(s)     : None 
*  Return Values : None. 
*  Returns       : CLI_SUCCESS/CLI_FAILURE                             
**********************************************************************/
INT4
ShowRtm6ControlInfoInCxt (tCliHandle CliHandle, UINT4 u4Rtm6CxtId)
{
    tSNMP_OCTET_STRING_TYPE FsRrd6ControlDestIpAddress;
    tSNMP_OCTET_STRING_TYPE FsRrd6NextControlDestIpAddress;
    UINT4               u4NextRtm6CxtId = 0;
    UINT4               u4Range = 0;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    UINT4               u4ShowAllCxt = FALSE;
    INT4                i4SrcProto = 0, i4DestProto = 0;
    INT4                i4Flag = 0, i4RowStatus = 0;
    INT4                i4RetVal = SNMP_FAILURE;
    UINT1               u1MoreProto = TRUE;
    UINT1               u1PrintHdr = FALSE;
    UINT1               u1Ip6Addr[IP6_ADDR_SIZE];
    UINT1               u1NextIp6Addr[IP6_ADDR_SIZE];
    UINT1               au1ContextName[VCM_ALIAS_MAX_LEN];
    tIp6Addr            Ip6Addr;

    MEMSET (u1Ip6Addr, 0, IP6_ADDR_SIZE);
    MEMSET (u1NextIp6Addr, 0, IP6_ADDR_SIZE);
    FsRrd6ControlDestIpAddress.pu1_OctetList = &u1Ip6Addr[0];
    FsRrd6ControlDestIpAddress.i4_Length = 0;
    FsRrd6NextControlDestIpAddress.pu1_OctetList = &u1NextIp6Addr[0];
    FsRrd6NextControlDestIpAddress.i4_Length = 0;

    if (u4Rtm6CxtId == RTM6_INVALID_CXT_ID)
    {
        u4ShowAllCxt = TRUE;
        i4RetVal = nmhGetFirstIndexFsMIRrd6ControlTable
            ((INT4 *) &u4NextRtm6CxtId,
             &FsRrd6NextControlDestIpAddress, (INT4 *) &u4Range);
    }
    else
    {
        i4RetVal = nmhGetNextIndexFsMIRrd6ControlTable
            ((INT4) u4Rtm6CxtId, (INT4 *) &u4NextRtm6CxtId,
             &FsRrd6ControlDestIpAddress, &FsRrd6NextControlDestIpAddress,
             (INT4) u4Range, (INT4 *) &u4Range);

        if (u4Rtm6CxtId != u4NextRtm6CxtId)
        {
            /*No entries for this context */
            return CLI_SUCCESS;
        }
    }
    u1PrintHdr = TRUE;

    while (i4RetVal == SNMP_SUCCESS)
    {

        u4Rtm6CxtId = u4NextRtm6CxtId;
        if (u1PrintHdr == TRUE)
        {
            u1PrintHdr = FALSE;
            UtilRtm6GetVcmAliasName (u4Rtm6CxtId, au1ContextName);
            CliPrintf (CliHandle, "\r\n\nVRF  Name:  %s\r\n", au1ContextName);
            /*display the header */
            CliPrintf (CliHandle,
                       "\rDestination      Range               SrcProto    DestProto          Flag  \r\n");

            CliPrintf (CliHandle,
                       "-----------      -----               --------    -----------        ----  \r\n");
        }

        MEMCPY (&Ip6Addr.u1_addr, FsRrd6NextControlDestIpAddress.pu1_OctetList,
                IP6_ADDR_SIZE);
        MEMCPY (FsRrd6ControlDestIpAddress.pu1_OctetList,
                FsRrd6NextControlDestIpAddress.pu1_OctetList, IP6_ADDR_SIZE);
        FsRrd6ControlDestIpAddress.i4_Length =
            FsRrd6NextControlDestIpAddress.i4_Length;

        nmhGetFsMIRrd6ControlSourceProto (u4Rtm6CxtId,
                                          &FsRrd6ControlDestIpAddress, u4Range,
                                          &i4SrcProto);

        nmhGetFsMIRrd6ControlDestProto (u4Rtm6CxtId,
                                        &FsRrd6ControlDestIpAddress, u4Range,
                                        &i4DestProto);

        nmhGetFsMIRrd6ControlRouteExportFlag (u4Rtm6CxtId,
                                              &FsRrd6ControlDestIpAddress,
                                              u4Range, &i4Flag);

        nmhGetFsMIRrd6ControlRowStatus (u4Rtm6CxtId,
                                        &FsRrd6ControlDestIpAddress, u4Range,
                                        &i4RowStatus);

        CliPrintf (CliHandle, "%-16s", Ip6PrintNtop ((tIp6Addr *) (&Ip6Addr)));

        CliPrintf (CliHandle, "    %-12d", u4Range);

        CliPrintf (CliHandle, "    %-12s", paRrd6ProtoTable[i4SrcProto]);

        if (i4DestProto == 0)
        {
            CliPrintf (CliHandle, "%-14s", "others");
        }

        if (i4DestProto == RTM6_ALL_RPS_MASK)
        {
            CliPrintf (CliHandle, "%-14s", "all");
        }
        else
        {
            if (i4DestProto & RTM6_RIP_MASK)
            {
                CliPrintf (CliHandle, "%-7s", "rip");
                u1MoreProto = FALSE;
            }

            if (i4DestProto & RTM6_OSPF_MASK)
            {
                CliPrintf (CliHandle, "%-7s", "ospf");
                if (u1MoreProto == FALSE)
                {
                    u1MoreProto = TRUE;
                }
                else
                {
                    u1MoreProto = FALSE;
                }
            }
            if (u1MoreProto == FALSE)
            {
                CliPrintf (CliHandle, "%-7s", "");
            }
        }

        u4PagingStatus = CliPrintf (CliHandle,
                                    "     %-6s\r\n",
                                    ((i4Flag == 1) ? "Allow" : "Deny"));

        if (u4PagingStatus == CLI_FAILURE)
        {
            /* User pressed 'q' at more prompt so break */
            break;
        }

        i4RetVal = nmhGetNextIndexFsMIRrd6ControlTable
            ((INT4) u4Rtm6CxtId, (INT4 *) &u4NextRtm6CxtId,
             &FsRrd6ControlDestIpAddress, &FsRrd6NextControlDestIpAddress,
             (INT4) u4Range, (INT4 *) &u4Range);

        if ((u4Rtm6CxtId != u4NextRtm6CxtId) && (u4ShowAllCxt == FALSE))
        {
            /*Finished displaying entries belonging to one VRF */
            break;
        }
        if (u4Rtm6CxtId != u4NextRtm6CxtId)
        {
            u1PrintHdr = TRUE;
        }

    }                            /*end of while */

    CliPrintf (CliHandle, "\r\n");
    return (CLI_SUCCESS);
}

#ifdef RM_WANTED
/*********************************************************************
 * *  Function Name : ShowRtm6RedDynInfo
 * *  Description   : Reads the gloabal RTM6 table and gets the
 *                    routing information updated dynamically
 * *
 * *  Input(s)      : CliHandle   - CdliContext ID
 * *  Output(s)     : None
 * *  Return Values : CLI_SUCCESS/CLI_FAILURE.
 * **********************************************************************/

INT4
ShowRtm6RedDynInfo (tCliHandle CliHandle)
{

    tRtm6RedTable      *pRtm6RedInfo = NULL;

    CliPrintf (CliHandle,
               "%-5s    %-5s    %-5s    %-5s    %-5s     %-5s    %-5s\r\n",
               "DestPrf", "NextHop", "ContextId", "PrefixLen", "HwStat",
               "DelFlg", "Protocol");
    CliPrintf (CliHandle,
               "-------    -------    ---------    ---------    ------     ------    --------\r\n");

    pRtm6RedInfo = RBTreeGetFirst (gRtm6GlobalInfo.Rtm6RedTable);
    if (pRtm6RedInfo == NULL)
    {
        CliPrintf (CliHandle, "%-5s", "No entries in RTM table\n");
    }

    while (pRtm6RedInfo != NULL)
    {
        CliPrintf (CliHandle, "%5s", Ip6PrintAddr (&pRtm6RedInfo->DestPrefix));
        CliPrintf (CliHandle, "%13s",
                   Ip6PrintAddr (&pRtm6RedInfo->NextHopAddr));
        CliPrintf (CliHandle, "%5d", pRtm6RedInfo->u4CxtId);
        CliPrintf (CliHandle, "%15d", pRtm6RedInfo->u1PrefixLen);
        CliPrintf (CliHandle, "%10d", pRtm6RedInfo->u1HwStatus);
        CliPrintf (CliHandle, "%13d", pRtm6RedInfo->u1DelFlag);
        CliPrintf (CliHandle, "%10d\r\n", pRtm6RedInfo->i1Proto);

        pRtm6RedInfo = RBTreeGetNext (gRtm6GlobalInfo.Rtm6RedTable,
                                      (tRBElem *) pRtm6RedInfo, NULL);
    }
    return CLI_SUCCESS;
}
#endif
/*********************************************************************
*  Function Name : ShowRrd6StatusInCxt
*  Description   : Calls Low level GetFirst/Next Routines and
                    displays fsRrd6RegnTable.
*                  
*  Input(s)      : CliHandle - CliContext ID                         
*                  u4Rtm6CxtId - VRF Id
*  Output(s)     : ppRespMsg - filled up table entries
*  Return Values : None. 
*  Returns       : CLI_SUCCESS/CLI_FAILURE                           
**********************************************************************/
INT4
ShowRrd6StatusInCxt (tCliHandle CliHandle, UINT4 u4Rtm6CxtId)
{
    INT4                i4RtProtoId = 0;
    INT4                i4NextRtProtoId = 0;
    INT4                i4AreaAllow = 0, i4ExtAllow = 0;
    tSNMP_OCTET_STRING_TYPE octectStrProtoName;
    INT4                i4RTMStatus = 0;
    INT4                i4RetVal = SNMP_FAILURE;
    UINT4               u4ShowAllCxt = FALSE;
    UINT4               u4NextRtm6CxtId = 0;
    UINT4               u4ThrottleLimit = 0;
    UINT1               u1PrintHdr = FALSE;
    UINT1               au1ContextName[VCM_ALIAS_MAX_LEN];
    UINT1               au1StrProtoNameOctetList[RRD6_MAX_ADDR_BUFFER];

    MEMSET (au1StrProtoNameOctetList, IP6_ZERO, RRD6_MAX_ADDR_BUFFER);

    octectStrProtoName.pu1_OctetList = au1StrProtoNameOctetList;

    u1PrintHdr = TRUE;
    if (u4Rtm6CxtId == RTM6_INVALID_CXT_ID)
    {
        u4ShowAllCxt = TRUE;
        i4RetVal = nmhGetFirstIndexFsMIRrd6RoutingProtoTable
            ((INT4 *) &u4NextRtm6CxtId, &i4NextRtProtoId);
    }
    else
    {
        i4RetVal = nmhGetNextIndexFsMIRrd6RoutingProtoTable
            ((INT4) u4Rtm6CxtId, (INT4 *) &u4NextRtm6CxtId,
             i4RtProtoId, &i4NextRtProtoId);

        if (u4Rtm6CxtId != u4NextRtm6CxtId)
        {
            /*No entries for this context */
            return CLI_SUCCESS;
        }
    }

    if (i4RetVal == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    nmhGetFsMIRtm6ThrotLimit (&u4ThrottleLimit);
    CliPrintf (CliHandle, "\r\nCurrent Throttle Limit:  %u", u4ThrottleLimit);
    while (i4RetVal == SNMP_SUCCESS)
    {

        u4Rtm6CxtId = u4NextRtm6CxtId;
        if (u1PrintHdr == TRUE)
        {
            u1PrintHdr = FALSE;
            UtilRtm6GetVcmAliasName (u4Rtm6CxtId, au1ContextName);
            CliPrintf (CliHandle, "\r\n\nVRF  Name:  %s\r\n", au1ContextName);
            CliPrintf (CliHandle, "\rCurrent State is");
            nmhGetFsMIRrd6AdminStatus (u4Rtm6CxtId, &i4RTMStatus);

            CliPrintf (CliHandle, " %-14s\r\n",
                       ((i4RTMStatus == 1) ? "enabled" : "disabled"));
            /*display the header */
            CliPrintf (CliHandle,
                       "\r\nProtoName    OspfAreaRoutes    OspfExtRoutes\r\n");
            CliPrintf (CliHandle,
                       "---------    --------------    -------------\r\n");

        }

        i4RtProtoId = i4NextRtProtoId;

        nmhGetFsMIRrd6RoutingProtoTaskIdent (u4Rtm6CxtId, i4RtProtoId,
                                             &octectStrProtoName);

        nmhGetFsMIRrd6AllowOspfAreaRoutes (u4Rtm6CxtId, i4RtProtoId,
                                           &i4AreaAllow);

        nmhGetFsMIRrd6AllowOspfExtRoutes (u4Rtm6CxtId, i4RtProtoId,
                                          &i4ExtAllow);

        CliPrintf (CliHandle, "%-13s", paRrd6ProtoTable[i4RtProtoId]);
        CliPrintf (CliHandle, "%-18s",
                   ((i4AreaAllow == 1) ? "Enable" : "Disable"));
        CliPrintf (CliHandle, "%-18s\r\n",
                   ((i4ExtAllow == 1) ? "Enable" : "Disable"));

        i4RetVal = nmhGetNextIndexFsMIRrd6RoutingProtoTable
            ((INT4) u4Rtm6CxtId, (INT4 *) &u4NextRtm6CxtId,
             i4RtProtoId, &i4NextRtProtoId);

        if (u4Rtm6CxtId != u4NextRtm6CxtId && u4ShowAllCxt == FALSE)
        {
            /*Finished displaying entries belonging to one VRF */
            break;
        }
        if (u4Rtm6CxtId != u4NextRtm6CxtId)
        {
            u1PrintHdr = TRUE;
        }

    }                            /*end of while */

    CliPrintf (CliHandle, "\r\n");
    return (CLI_SUCCESS);
}

/*********************************************************************
*  Function Name : CliRrd6ModDefCtrEntry
*  Description   : Calls Low level Set Routines for modifying RRD6
*                  control table default entry.
*                  
*  Input(s)      : CliHandle - CliContext ID                         
*                : i4ControlFlag -Status to be set for Default Entry 
*  Output(s)     : ppRespMsg - filled up with error Message, if any. 
*  Return Values : None. 
**********************************************************************/
VOID
CliRrd6ModDefCtrlEntry (tCliHandle CliHandle, INT4 i4ControlFlag)
{
    INT4                i4Flag = 0;

    tSNMP_OCTET_STRING_TYPE FsRrd6ControlDestIpAddress;
    UINT1               u1TempAddr[IP6_ADDR_SIZE];

    MEMSET (u1TempAddr, 0, IP6_ADDR_SIZE);
    FsRrd6ControlDestIpAddress.pu1_OctetList = u1TempAddr;
    FsRrd6ControlDestIpAddress.i4_Length = IP6_ADDR_SIZE;

    if (nmhGetFsRrd6ControlRouteExportFlag (&FsRrd6ControlDestIpAddress,
                                            RRD6_CLI_DEF_RANGE,
                                            &i4Flag) == SNMP_FAILURE)
    {

        i4Flag = 0;
        if (nmhSetFsRrd6ControlRowStatus (&FsRrd6ControlDestIpAddress,
                                          RRD6_CLI_DEF_RANGE,
                                          CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            if (i4ControlFlag == CLI_RRD6_PERMIT)
            {
                CliPrintf (CliHandle,
                           "\r% Creating entry in Permit mode failed\r\n");
            }
            else
            {
                CliPrintf (CliHandle,
                           "\r% Creating entry in Deny mode failed\r\n");
            }
            return;
        }
    }

    if (i4ControlFlag != i4Flag)
    {
        nmhSetFsRrd6ControlRouteExportFlag (&FsRrd6ControlDestIpAddress,
                                            RRD6_CLI_DEF_RANGE, i4ControlFlag);
        nmhSetFsRrd6ControlRowStatus (&FsRrd6ControlDestIpAddress,
                                      RRD6_CLI_DEF_RANGE, ACTIVE);
    }
    return;
}

/*********************************************************************
*  Function Name : CliRrd6ThrotVal
*  Description   : Calls Low level Set Routine for setting maximum
*                  number of routes to be processed for each iteration.
*  Input(s)      : CliHandle - CliContext ID                          
*                : u4ThrotVal - Throttle value
*  Output(s)     : ppRespMsg - filled up with error Message, if any. 
*  Return Values : None. 
**********************************************************************/
INT4
CliRrd6ThrotValue (tCliHandle CliHandle, UINT4 u4ThrotVal)
{
    UINT4               u4ErrorCode = 0;

    UNUSED_PARAM (CliHandle);

    if (nmhTestv2FsRrd6ThrotLimit (&u4ErrorCode, u4ThrotVal) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    nmhSetFsRrd6ThrotLimit (u4ThrotVal);
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*     FUNCTION NAME    : Rtm6CliGetCommandType                               */
/*                                                                           */
/*     DESCRIPTION      : This function returns the command type as          */
/*                        configuration command or show command              */
/*                                                                           */
/*     INPUT            : uCommand - command identifer                       */
/*                                                                           */
/*     OUTPUT           : pi1IsShowCommand                                   */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
Rtm6CliGetCommandType (UINT4 u4Command, INT1 *pi1IsShowCommand)
{
    INT4                i4RetStatus = 0;

    switch (u4Command)
    {

        case CLI_RRD6_SHOW_CONTROL_INFO:
        case CLI_RRD6_SHOW_MEM_RES:
        case CLI_RRD6_SHOW_STATUS:
#ifdef RM_WANTED
        case CLI_RRD6_RED_DYN_INFO:
#endif
            /* Set show command as true */
            *pi1IsShowCommand = TRUE;
            i4RetStatus = CLI_SUCCESS;
            break;

        case CLI_RRD6_DEF_CONTROL_STATUS:
        case CLI_RRD6_REDISTRIBUTE_POLICY:
        case CLI_RRD6_NO_REDISTRIBUTE_POLICY:
        case CLI_RRD6_EXPORT_OSPF:
        case CLI_RRD6_MAX_MEM_RES:
        case CLI_RRD6_NO_EXPORT_OSPF:
        case CLI_RRD6_THROT_VALUE:
            /* Set show command as False */
            *pi1IsShowCommand = FALSE;
            i4RetStatus = CLI_SUCCESS;
            break;
        default:
            /* Invalid Command identifier */
            i4RetStatus = CLI_FAILURE;
            break;
    }
    return i4RetStatus;
}

/*****************************************************************************/
/*     FUNCTION NAME    : Rtm6ShowScalarsInCxt                               */
/*                                                                           */
/*     DESCRIPTION      : This function returns the command type as          */
/*                        configuration command or show command              */
/*                                                                           */
/*     INPUT            : uCommand - command identifer                       */
/*                                                                           */
/*     OUTPUT           : pi1IsShowCommand                                   */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
VOID Rtm6ShowScalarsInCxt (tCliHandle CliHandle)
{
     UINT4   u4RetVal = 0;
     UINT1   u1BangStatus = FALSE;


     if( nmhGetFsRrd6MaximumBgpRoutes(&u4RetVal)== SNMP_SUCCESS)
     {
           if (u4RetVal != DEF_RTM6_BGP_ROUTE_ENTRIES)
            {
                u1BangStatus = TRUE;
                CliPrintf (CliHandle, "ipv6 maximum-routes bgp %d\r\n",
                           u4RetVal);
            }
     }
     u4RetVal = 0;
     if( nmhGetFsRrd6MaximumOspfRoutes(&u4RetVal)== SNMP_SUCCESS)
     {
           if (u4RetVal != DEF_RTM6_OSPF_ROUTE_ENTRIES)
            {
                u1BangStatus = TRUE;
                CliPrintf (CliHandle, "ipv6 maximum-routes ospf %d\r\n",
                           u4RetVal);
            }
     }
     u4RetVal = 0;
     if( nmhGetFsRrd6MaximumRipRoutes(&u4RetVal)== SNMP_SUCCESS)
     {
           if (u4RetVal != DEF_RTM6_RIP_ROUTE_ENTRIES)
            {
                u1BangStatus = TRUE;
                CliPrintf (CliHandle, "ipv6 maximum-routes rip %d\r\n",
                           u4RetVal);
            }
     }
     u4RetVal = 0;
     if( nmhGetFsRrd6MaximumStaticRoutes(&u4RetVal)== SNMP_SUCCESS)
     {
           if (u4RetVal != DEF_RTM6_STATIC_ROUTE_ENTRIES)
            {
                u1BangStatus = TRUE;
                CliPrintf (CliHandle, "ipv6 maximum-routes static %d\r\n",
                           u4RetVal);
            }
     }
     u4RetVal = 0;
     if( nmhGetFsRrd6MaximumISISRoutes(&u4RetVal)== SNMP_SUCCESS)
     {
           if (u4RetVal != DEF_RTM6_ISIS_ROUTE_ENTRIES)
            {
                u1BangStatus = TRUE;
                CliPrintf (CliHandle, "ipv6 maximum-routes isis %d\r\n",
                           u4RetVal);
            }
     }
     if(u1BangStatus == TRUE)
     {
         CliSetBangStatus(CliHandle, TRUE);
     }

}


/*****************************************************************************/
/* Function Name      : Rtm6CliGetShowCmdOutputToFile                         */
/*                                                                           */
/* Description        : This function handles the execution of show commands */
/*                      and calculation of checksum with the output.         */
/*                      The calculated value is then being sent to RM.       */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : pu1FileName - The output of the show cmd is          */
/*                      redirected to this file                              */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : RTM6_SUCCESS/RTM6_FAILURE                              */
/*****************************************************************************/
INT4
Rtm6CliGetShowCmdOutputToFile (UINT1 *pu1FileName)
{
    if (FileStat ((const CHR1 *) pu1FileName) == OSIX_SUCCESS)
    {
        if (0 != FileDelete (pu1FileName))
        {
            return RTM6_FAILURE;
        }
    }
    if (CliGetShowCmdOutputToFile (pu1FileName, (UINT1 *) RTM6_AUDIT_SHOW_CMD)
        == CLI_FAILURE)
    {
        return RTM6_FAILURE;
    }
    return RTM6_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : Rtm6CliCalcSwAudCheckSum                              */
/*                                                                           */
/* Description        : This function handles the Calculation of checksum    */
/*                      for the data in the given input file.                */
/*                                                                           */
/* Input(s)           : pu1FileName - The checksum is calculated for the data*/
/*                      in this file                                         */
/*                                                                           */
/* Output(s)          : pu2ChkSum - The calculated checksum is stored here   */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : RTM6_SUCCESS/RTM6_FAILURE                              */
/*****************************************************************************/
INT4
Rtm6CliCalcSwAudCheckSum (UINT1 *pu1FileName, UINT2 *pu2ChkSum)
{
    UINT4               u4Sum = 0;
    UINT4               u4Last = 0;
    INT4                i4Fd;
    UINT2               u2CkSum = 0;
    INT2                i2ReadLen;
    INT1                ai1Buf[RTM6_CLI_MAX_GROUPS_LINE_LEN + 1];

    i4Fd = FileOpen ((CONST UINT1 *) pu1FileName, RTM6_CLI_RDONLY);
    if (i4Fd == -1)
    {
        return RTM6_FAILURE;
    }
    MEMSET (ai1Buf, 0, RTM6_CLI_MAX_GROUPS_LINE_LEN + 1);
    while (Rtm6CliReadLineFromFile (i4Fd, ai1Buf, RTM6_CLI_MAX_GROUPS_LINE_LEN,
                                    &i2ReadLen) != RTM6_CLI_EOF)

    {
        if (i2ReadLen > 0)
        {
            UtilCompCheckSum ((UINT1 *) ai1Buf, &u4Sum, &u4Last,
                              &u2CkSum, (UINT4) i2ReadLen, CKSUM_DATA);

            MEMSET (ai1Buf, '\0', RTM6_CLI_MAX_GROUPS_LINE_LEN + 1);
        }
    }
    /*CheckSum for last line */
    if ((u4Sum != 0) || (i2ReadLen != 0))
    {
        UtilCompCheckSum ((UINT1 *) ai1Buf, &u4Sum, &u4Last,
                          &u2CkSum, (UINT4) i2ReadLen, CKSUM_LASTDATA);

    }
    *pu2ChkSum = u2CkSum;
    if (FileClose (i4Fd) < 0)
    {
        return RTM6_FAILURE;
    }
    return RTM6_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : Rtm6CliReadLineFromFile                               */
/*                                                                           */
/* Description        : It is a utility to read a line from the given file   */
/*                      descriptor.                                          */
/*                                                                           */
/* Input(s)           : i4Fd - File Descriptor for the file                  */
/*                      i2MaxLen - Maximum length that can be read and store */
/*                                                                           */
/* Output(s)          : pi1Buf - Buffer to store the line read from the file */
/*                      pi2ReadLen - the total length of the data read       */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : RTM6_SUCCESS/RTM6_FAILURE                              */
/*****************************************************************************/
INT1
Rtm6CliReadLineFromFile (INT4 i4Fd, INT1 *pi1Buf, INT2 i2MaxLen,
                         INT2 *pi2ReadLen)
{
    INT1                i1Char;

    *pi2ReadLen = 0;

    while (FileRead (i4Fd, (CHR1 *) & i1Char, 1) == 1)
    {
        pi1Buf[*pi2ReadLen] = i1Char;
        (*pi2ReadLen)++;
        if (i1Char == '\n')
        {
            return (RTM6_CLI_NO_EOF);
        }
        if (*pi2ReadLen == i2MaxLen)
        {
            *pi2ReadLen = 0;
            break;
        }
    }
    pi1Buf[*pi2ReadLen] = '\0';
    return (RTM6_CLI_EOF);
}
#endif
