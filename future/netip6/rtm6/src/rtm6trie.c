/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rtm6trie.c,v 1.53.2.1 2018/03/09 13:40:48 siva Exp $
 *
 * Description:This file contains IP interface functions for updating/
 *             Querying the Trie.
 *
 *******************************************************************/

#include "rtm6inc.h"
#include "iss.h"
#include "rtm6frt.h"

struct TrieAppFns   Rtm6TrieLibFns = {
    (INT4 (*)(tInputParams * pInputParams, VOID *pOutputParams,
              VOID **ppAppPtr, VOID *pAppSpecInfo)) Rtm6TrieCbAddEntry,
    (INT4 (*)(tInputParams * pInputParams, VOID **ppAppPtr,
              VOID *pOutputParams, VOID *pAppPtr, tKey Key))
        Rtm6TrieCbDeleteEntry,
    (INT4 (*)(tInputParams * pInputParams, VOID *pOutputParams,
              VOID *pAppPtr)) Rtm6TrieCbSearchEntry,
    (INT4 (*)(tInputParams * pInputParams, VOID *pOutputParams,
              VOID *pAppPtr, UINT2, tKey)) Rtm6TrieCbLookupEntry,
    (VOID *(*)(tInputParams *, VOID (*)(VOID *), VOID *)) NULL,
    (VOID (*)(VOID *)) NULL,
    (INT4 (*)(VOID *, VOID **, tKey)) NULL,
    (INT4 (*)(VOID *, VOID *, tKey)) NULL,
    (INT4 (*)(tInputParams *, VOID *, VOID **, VOID *, UINT4))
        Rtm6TrieCbUpdateEntry,
    (VOID *(*)(tInputParams * pInputParams, VOID (*pAppSpecFunc) (VOID *),
               VOID *pOutputParams)) Rtm6TrieCbClearTrieInit,
    (VOID (*)(VOID *pDummy)) Rtm6TrieCbClearTrieDeInit,
    (INT4 (*)(VOID *pInputParams, VOID **ppAppPtr, tKey Key))
        Rtm6TrieCbClearRtEntry,
    (INT4 (*)(tInputParams *, VOID *)) NULL,
    (INT4 (*)(UINT2, tInputParams *, VOID *, VOID *, tKey)) Rtm6TrieCbBestMatch,
    (INT4 (*)(tInputParams * pInputParams, VOID *pOutputParams,
              VOID *pAppPtr, tKey Key)) NULL,
    (INT4 (*)(tInputParams * pInputParams, VOID *pOutputParams,
              VOID *pAppPtr, UINT2 u2Keysize, tKey Key)) NULL
};

/*****************************************************************************/
/* Function Name : Rtm6TrieCreateInCxt                                       */
/* Description   : This function calls TRIE specific API to create IPv6 TRIE */
/* Input(s)      : pRtm6Cxt - Rtm6 Context Pointer                           */
/*               : pRegnId - Pointer to RTM6 Protocol registration info      */
/* Output(s)     : None.                                                     */
/* Return(s)     : RTM6_SUCCESS/RTM6_FAILURE.                                */
/*****************************************************************************/
INT4
Rtm6TrieCreateInCxt (tRtm6Cxt * pRtm6Cxt, tRtm6RegnId * pRegnId)
{
    tTrieCrtParams      CreateParams;
    tRtm6SystemSize     Rtm6SystemSize;
    tRadixNodeHead     *pRadix = NULL;

    GetRtm6SizingParams (&Rtm6SystemSize);

    /* key is address + mask */
    CreateParams.u2KeySize = sizeof (UINT2) * IP6_ADDR_SIZE;
    CreateParams.u4Type = RTM6_CIDR_RT_TYPE_IN_CXT (pRtm6Cxt);
    CreateParams.u1AppId = (UINT1) pRegnId->u2ProtoId;
    CreateParams.AppFns = &(Rtm6TrieLibFns);
    CreateParams.u4NoofRoutes = Rtm6SystemSize.u4Rtm6MaxRoutes;
    CreateParams.u4NumRadixNodes = Rtm6SystemSize.u4Rtm6MaxRoutes + 1;
    CreateParams.u4NumLeafNodes = Rtm6SystemSize.u4Rtm6MaxRoutes;
    CreateParams.bPoolPerInst = OSIX_FALSE;
    CreateParams.bSemPerInst = OSIX_FALSE;
    CreateParams.bValidateType = OSIX_TRUE;
    pRadix = TrieCreateInstance (&(CreateParams));

    if ((pRtm6Cxt->pIp6RtTblRoot != NULL) &&
        (pRtm6Cxt->pIp6RtTblRoot != pRadix))
    {
        RTM6_TRC_ARG1 (pRtm6Cxt->u4ContextId, RTM6_ALL_FAILURE_TRC |
                       RTM6_OS_RESOURCE_TRC, RTM6_MOD_NAME,
                       "\tTrie Initialisation for Protocol [%d] FAILED!!!\n",
                       pRegnId->u2ProtoId);
        return RTM6_FAILURE;
    }

    pRtm6Cxt->pIp6RtTblRoot = (VOID *) pRadix;
    return RTM6_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Rtm6TrieDeleteInCxt                                       */
/* Description   : This function calls TRIE specific API to delete IPv6 TRIE */
/* Input(s)      : pRtm6Cxt - Rtm6 Context Pointer                           */
/*               : pRegnId - Pointer to RTM6 Protocol registration info      */
/* Output(s)     : None.                                                     */
/* Return(s)     : RTM6_SUCCESS/RTM6_FAILURE.                                */
/*****************************************************************************/
INT4
Rtm6TrieDeleteInCxt (tRtm6Cxt * pRtm6Cxt, tRtm6RegnId * pRegnId)
{
    tInputParams        InParams;
    tRtm6TrieDelOutParams OutParams;
    INT4                i4Status = TRIE_FAILURE;

    InParams.pRoot = pRtm6Cxt->pIp6RtTblRoot;
    InParams.i1AppId = (INT1) ((pRegnId->u2ProtoId == 0) ?
                               ALL_PROTO_ID : pRegnId->u2ProtoId);
    InParams.pLeafNode = NULL;
    InParams.Key.pKey = NULL;
    OutParams.u4NoOfRoutes = 0;
    OutParams.i1ProtoId = InParams.i1AppId;

    i4Status = TrieDel (&InParams, Rtm6TrieCbDelete, (VOID *) &OutParams);

    if (i4Status == TRIE_SUCCESS)
    {
        gRtm6GlobalInfo.u4BgpRts = IP6_ZERO;
        gRtm6GlobalInfo.u4RipRts = IP6_ZERO;
        gRtm6GlobalInfo.u4OspfRts = IP6_ZERO;
        gRtm6GlobalInfo.u4StaticRts = IP6_ZERO;
        gRtm6GlobalInfo.u4IsisRts = IP6_ZERO;
    }

    return ((i4Status >= TRIE_SUCCESS) ? (RTM6_SUCCESS) : (RTM6_FAILURE));
}

/*****************************************************************************/
/* Function Name : Rtm6TrieCbDelete                                          */
/* Description   : This is a call back function for TRIE Delete              */
/* Input(s)      : pInput - Input Parameter.                                 */
/* Output(s)     : None.                                                     */
/* Return(s)     : None.                                                     */
/*****************************************************************************/
VOID
Rtm6TrieCbDelete (VOID *pInput)
{
    UNUSED_PARAM (pInput);
    return;
}

/*****************************************************************************/
/* Function Name : Rtm6TrieCbClearRtEntry                                    */
/* Description   : This function calls Application Specific clear routine    */
/*                 from IPv6 TRIE.                                           */
/*                 (Registered with TRIE library during TRIE creation)       */
/* Input(s)      : pParams -       Delete Params Structure based on which    */
/*               :                 app specific information are deleted.     */
/*               : ppAppSpecPtr - Pointer to the application specific data.  */
/*               : Key - Key corresponding the Application specific data.    */
/* Output(s)     : None.                                                     */
/* Return(s)     : TRIE_SUCCESS/TRIE_FAILURE.                                */
/*****************************************************************************/
INT4
Rtm6TrieCbClearRtEntry (VOID *pParams, VOID **ppAppSpecPtr, tKey Key)
{
    tRtm6TrieDelOutParams *pDelParams = NULL;
    tIp6RtEntry        *pIp6RtEntry = NULL;
    tIp6RtEntry        *pNextProtoIp6RtEntry = NULL;
    tIp6RtEntry        *pNextAltIp6RtEntry = NULL;
    UINT1               u1IsRouteDeleted = FALSE;

    UNUSED_PARAM (Key);
    pDelParams = (tRtm6TrieDelOutParams *) pParams;
    pIp6RtEntry = (tIp6RtEntry *) (*ppAppSpecPtr);

    while (pIp6RtEntry)
    {
        u1IsRouteDeleted = FALSE;
        if (pDelParams->i1ProtoId == ALL_PROTO_ID)
        {
            /* Need to clear all the routes from the TRIE. */
            u1IsRouteDeleted = TRUE;
        }
        else
        {
            /* Need to clear only specific protocol route. Routes
             * are sorted in the ascending order of the protocol value. */
            if (pIp6RtEntry->i1Proto == pDelParams->i1ProtoId)
            {
                /* Protocol matches. Delete the route. */
                u1IsRouteDeleted = TRUE;
            }
            else if (pIp6RtEntry->i1Proto > pDelParams->i1ProtoId)
            {
                /* No need to remove any more routes from TRIE. */
                break;
            }
            else
            {
                /* Go to next protocol routes */
                pIp6RtEntry = pIp6RtEntry->pNextRtEntry;
                continue;
            }
        }

        if (u1IsRouteDeleted == TRUE)
        {
            /* Need to delete all the routes from this protocol */
            pNextAltIp6RtEntry = NULL;
            pNextProtoIp6RtEntry = pIp6RtEntry->pNextRtEntry;

            while (pIp6RtEntry)
            {
                pNextAltIp6RtEntry = pIp6RtEntry->pNextAlternatepath;
                pIp6RtEntry->pNextAlternatepath = NULL;

                pIp6RtEntry->i1Refcnt--;

                /* Release the memory for the route. */
                IP6_RT_FREE (pIp6RtEntry);

                /* Process the next route. */
                pIp6RtEntry = pNextAltIp6RtEntry;
                pNextAltIp6RtEntry = NULL;
                pDelParams->u4NoOfRoutes++;
            }

            /* First route list in the leaf node is cleared. Update the
             * leaf node. */
            (*ppAppSpecPtr) = (VOID *) pNextProtoIp6RtEntry;

            pIp6RtEntry = pNextProtoIp6RtEntry;
            pNextProtoIp6RtEntry = NULL;
        }
    }

    return TRIE_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Rtm6TrieCbClearTrieInit                                   */
/* Description   : This function callback function for initializing Trie     */
/*                 Delete.                                                   */
/*                 (Registered with TRIE library during TRIE creation)       */
/* Input(s)      : pInputParams - Pointer to the Input parameter             */
/*                 AppSpec - Call back function                              */
/* Output(s)     : pOutputParams - Pointer to the updated OutPut Parameter   */
/* Return(s)     : Pointer to the App Spec Del Params for deleting App spec  */
/*                 info from the TRIE                                        */
/*****************************************************************************/
VOID               *
Rtm6TrieCbClearTrieInit (tInputParams * pInputParams,
                         VOID (AppSpec) (VOID *), VOID *pOutputParams)
{
    AppSpec ((VOID *) pInputParams);
    return (pOutputParams);
}

/*****************************************************************************/
/* Function Name : Rtm6TrieCbClearTrieDeInit                                 */
/* Description   : This function callback function for de-initializing Trie  */
/*                 Delete.                                                   */
/* Input(s)      : pDelParams - Input Params                                 */
/* Output(s)     : None.                                                     */
/* Return(s)     : None.                                                     */
/*****************************************************************************/
VOID
Rtm6TrieCbClearTrieDeInit (VOID *pDelParams)
{
    UNUSED_PARAM (pDelParams);
    return;
}

/*****************************************************************************/
/* Function Name : Rtm6TrieAddEntryInCxt                                     */
/* Description   : This function calls TRIE specific API to add a route      */
/*                 profile entry into RIB                                    */
/* Input(s)      : pRtm6Cxt - Rtm6 Context Pointer                           */
/*               : pNewRtEntry   - pointer to the new application specific   */
/*                                 info that is going to be added            */
/* Output(s)     : None.                                                     */
/* Return(s)     : RTM6_SUCCESS/RTM6_FAILURE.                                */
/*****************************************************************************/
INT4
Rtm6TrieAddEntryInCxt (tRtm6Cxt * pRtm6Cxt, tIp6RtEntry * pNewRtEntry)
{
    tInputParams        InParams;
    tRtm6TrieOutParams  OutParams;
    tIp6Addr            AddrMask;
    UINT1               au1TrieKey[RTM6_MAX_TRIE_KEY_LEN];

    MEMSET ((UINT1 *) &AddrMask, IP6_UINT1_ALL_ONE, sizeof (tIp6Addr));

    InParams.pRoot = pRtm6Cxt->pIp6RtTblRoot;
    InParams.i1AppId = (INT1) (pNewRtEntry->i1Proto - 1);
    InParams.pLeafNode = NULL;

    /* Fill the Key. */
    InParams.Key.pKey = au1TrieKey;
    MEMSET (InParams.Key.pKey, 0, RTM6_MAX_TRIE_KEY_LEN);

    Ip6CopyAddrBits ((tIp6Addr *) (VOID *) InParams.Key.pKey, &pNewRtEntry->dst,
                     pNewRtEntry->u1Prefixlen);
    Ip6CopyAddrBits ((tIp6Addr *) (VOID *) (InParams.Key.pKey + IP6_ADDR_SIZE),
                     &AddrMask, pNewRtEntry->u1Prefixlen);
    InParams.u1PrefixLen = (UINT1) pNewRtEntry->u1Prefixlen;

    if (TrieAdd (&InParams, pNewRtEntry, &OutParams) == TRIE_FAILURE)
    {
        RTM6_TRC_ARG2 (pRtm6Cxt->u4ContextId, RTM6_ALL_FAILURE_TRC,
                       RTM6_MOD_NAME,
                       "\tAdding Route [%s/%d] to TRIE FAILED!!!\n",
                       Ip6PrintAddr (&pNewRtEntry->dst),
                       pNewRtEntry->u1Prefixlen);
        return RTM6_FAILURE;
    }
    else
    {
        switch (pNewRtEntry->i1Proto)
        {
            case RIPNG_ID:
                gRtm6GlobalInfo.u4BgpRts += 1;
                break;
            case OSPF6_ID:
                gRtm6GlobalInfo.u4OspfRts += 1;
                break;
            case BGP6_ID:
                gRtm6GlobalInfo.u4BgpRts += 1;
                break;
            case ISIS6_ID:
                gRtm6GlobalInfo.u4IsisRts += 1;
                break;
            case IP6_NETMGMT_PROTOID:
                gRtm6GlobalInfo.u4StaticRts += 1;
                break;
            default:
                break;
        }

    }
    /* Route has been successfully added to the Trie. */
    return RTM6_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Rtm6TrieCbAddEntry                                        */
/* Description   : This is a call back function to add a route profile entry */
/*                 into RIB                                                  */
/*                 (Registered with TRIE library during TRIE creation)       */
/* Input(s)      : pInputParams - void pointer used by TRIE                  */
/*                 ppAppSpecInfo - address of the Application (RTM6) specific*/
/*                                 info present in TRIE leaf node            */
/*                 pNewAppSpecInfo - pointer to the new application specific */
/*                                 info that is going to be added            */
/* Output(s)     : ppAppSpecInfo - address of the updated application        */
/*                                 specific info pointer                     */
/*                 pOutputParams - void pointer (used by RTM6)               */
/* Return(s)     : TRIE_SUCCESS/TRIE_FAILURE.                                */
/*****************************************************************************/
INT4
Rtm6TrieCbAddEntry (tInputParams * pInputParams, VOID *pOutputParams,
                    VOID **ppAppSpecInfo, VOID *pNewAppSpecInfo)
{
    tRtm6TrieOutParams *pOutParams = NULL;
    tIp6RtEntry        *pIp6RtList = NULL;
    tIp6RtEntry        *pPrevProtoIp6RtEntry = NULL;
    tIp6RtEntry        *pPrevAltIp6RtEntry = NULL;
    tIp6RtEntry        *pNewIp6RtEntry = NULL;

    UNUSED_PARAM (pInputParams);
    pOutParams = (tRtm6TrieOutParams *) pOutputParams;

    pNewIp6RtEntry = (tIp6RtEntry *) pNewAppSpecInfo;
    pNewIp6RtEntry->pNextAlternatepath = NULL;
    pNewIp6RtEntry->pNextRtEntry = NULL;
    pIp6RtList = (tIp6RtEntry *) * ppAppSpecInfo;

    /* Routes are stored in the ascending order of the protocol
     * value and alternate routes from same protocol are
     * sorted in the ascending order of the Metrics. */
    for (pPrevProtoIp6RtEntry = NULL;
         ((pIp6RtList != NULL) &&
          (pIp6RtList->i1Proto < pNewIp6RtEntry->i1Proto));
         pPrevProtoIp6RtEntry = pIp6RtList,
         pIp6RtList = pIp6RtList->pNextRtEntry)
    {
        /* empty */
    }

    if ((pIp6RtList != NULL) &&
        (pIp6RtList->i1Proto == pNewIp6RtEntry->i1Proto))
    {
        if (pIp6RtList->i1Proto == STATIC_ID)
        {
            for (pPrevAltIp6RtEntry = NULL;
                 ((pIp6RtList != NULL) &&
                  (pIp6RtList->u1Preference <= pNewIp6RtEntry->u1Preference));
                 pPrevAltIp6RtEntry = pIp6RtList,
                 pIp6RtList = pIp6RtList->pNextAlternatepath)
            {
                /* empty */
            }
        }
        else
        {
            for (pPrevAltIp6RtEntry = NULL;
                 ((pIp6RtList != NULL) &&
                  (pIp6RtList->u4Metric <= pNewIp6RtEntry->u4Metric));
                 pPrevAltIp6RtEntry = pIp6RtList,
                 pIp6RtList = pIp6RtList->pNextAlternatepath)
            {
                /* empty */
            }

        }
    }

    if (pPrevAltIp6RtEntry != NULL)
    {
        /* Route should be added next to the pPrevAltIp6RtEntry */
        pNewIp6RtEntry->pNextAlternatepath =
            pPrevAltIp6RtEntry->pNextAlternatepath;
        pPrevAltIp6RtEntry->pNextAlternatepath = pNewIp6RtEntry;
    }
    else
    {
        if (pPrevProtoIp6RtEntry == NULL)
        {
            /* Route should be added as the first route in leaf node */
            pIp6RtList = (tIp6RtEntry *) (*ppAppSpecInfo);
        }
        else
        {
            /* Route should be added next to the pPrevProtoIp6RtEntry */
            pIp6RtList = pPrevProtoIp6RtEntry->pNextRtEntry;
        }

        if ((pIp6RtList != NULL) &&
            (pIp6RtList->i1Proto == pNewIp6RtEntry->i1Proto))
        {
            pNewIp6RtEntry->pNextRtEntry = pIp6RtList->pNextRtEntry;
            pIp6RtList->pNextRtEntry = NULL;
            pNewIp6RtEntry->pNextAlternatepath = pIp6RtList;
        }
        else
        {
            pNewIp6RtEntry->pNextRtEntry = pIp6RtList;
        }

        if (pPrevProtoIp6RtEntry == NULL)
        {
            /* Route should be added as the first route in leaf node */
            (*ppAppSpecInfo) = (VOID *) pNewIp6RtEntry;
        }
        else
        {
            /* Route should be added next to the pPrevProtoIp6RtEntry */
            pPrevProtoIp6RtEntry->pNextRtEntry = pNewIp6RtEntry;
        }
    }

    pNewIp6RtEntry->i1Refcnt++;

    /* Update the output param */
    pOutParams->pRtEntry = pNewIp6RtEntry;
    return TRIE_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Rtm6TrieDeleteEntryInCxt                                  */
/* Description   : This function calls TRIE specific API to delete the route */
/*                 profile entry from RIB                                    */
/* Input(s)      : pRtm6Cxt - Rtm6 Context Pointer                           */
/*               : pDelRtEntry   - pointer to the application specific       */
/*                                 info that is going to be deleted          */
/* Output(s)     : None.                                                     */
/* Return(s)     : RTM6_SUCCESS/RTM6_FAILURE.                                */
/*****************************************************************************/
INT4
Rtm6TrieDeleteEntryInCxt (tRtm6Cxt * pRtm6Cxt, tIp6RtEntry * pDelRtEntry)
{
    tInputParams        InParams;
    tRtm6TrieOutParams  OutParams;
    tIp6Addr            AddrMask;
    UINT1               au1TrieKey[RTM6_MAX_TRIE_KEY_LEN];

    MEMSET ((UINT1 *) &AddrMask, IP6_UINT1_ALL_ONE, sizeof (tIp6Addr));

    InParams.pRoot = pRtm6Cxt->pIp6RtTblRoot;
    InParams.i1AppId = (INT1) (pDelRtEntry->i1Proto - 1);
    InParams.pLeafNode = NULL;

    /* Fill the Key. */
    InParams.Key.pKey = au1TrieKey;
    MEMSET (InParams.Key.pKey, 0, RTM6_MAX_TRIE_KEY_LEN);

    Ip6CopyAddrBits ((tIp6Addr *) (VOID *) InParams.Key.pKey, &pDelRtEntry->dst,
                     pDelRtEntry->u1Prefixlen);
    Ip6CopyAddrBits ((tIp6Addr *) (VOID *) (InParams.Key.pKey + IP6_ADDR_SIZE),
                     &AddrMask, pDelRtEntry->u1Prefixlen);
    InParams.u1PrefixLen = (UINT1) pDelRtEntry->u1Prefixlen;

    if (TrieRemove (&InParams, &OutParams, (VOID *) pDelRtEntry) ==
        TRIE_FAILURE)
    {
        RTM6_TRC_ARG2 (pRtm6Cxt->u4ContextId, RTM6_ALL_FAILURE_TRC,
                       RTM6_MOD_NAME,
                       "\tDeleting Route [%s/%d] from TRIE FAILED!!!\n",
                       Ip6PrintAddr (&pDelRtEntry->dst),
                       pDelRtEntry->u1Prefixlen);
        return RTM6_FAILURE;
    }
    else
    {
        switch (pDelRtEntry->i1Proto)
        {
            case RIPNG_ID:
                gRtm6GlobalInfo.u4BgpRts -= 1;
                break;
            case OSPF6_ID:
                gRtm6GlobalInfo.u4OspfRts -= 1;
                break;
            case BGP6_ID:
                gRtm6GlobalInfo.u4BgpRts -= 1;
                break;
            case ISIS6_ID:
                gRtm6GlobalInfo.u4IsisRts -= 1;
                break;
            case IP6_NETMGMT_PROTOID:
                gRtm6GlobalInfo.u4StaticRts -= 1;
                break;
            default:
                break;

        }
    }

    /* Route has been successfully deleted from the Trie. */
    return RTM6_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Rtm6TrieCbDeleteEntry                                     */
/* Description   : This is a call back function to delete a route profile    */
/*                 entry from RIB                                            */
/*                 (Registered with TRIE library during TRIE creation)       */
/* Input(s)      : pInputParams - Pointer used by TRIE                       */
/*                 ppAppSpecInfoList - address of the Application specific   */
/*                                 info present in TRIE leaf node            */
/*                 pAppSpecInfo - pointer to the application specific info   */
/*                                 that is going to be deleted               */
/* Output(s)     : ppAppSpecInfoList - address of the updated application    */
/*                                 specific info pointer                     */
/*                 pOutputParams - void pointer (used by RTM6)               */
/* Return(s)     : TRIE_SUCCESS/TRIE_FAILURE.                                */
/*****************************************************************************/
INT4
Rtm6TrieCbDeleteEntry (tInputParams * pInputParams, VOID **ppAppSpecInfoList,
                       VOID *pOutputParams, VOID *pAppSpecInfo, tKey Key)
{
    tRtm6TrieOutParams *pOutParams = NULL;
    tIp6RtEntry        *pIp6RtEntry = NULL;
    tIp6RtEntry        *pPrevProtoIp6RtEntry = NULL;
    tIp6RtEntry        *pPrevAltIp6RtEntry = NULL;
    tIp6RtEntry        *pDelIp6RtEntry = NULL;

    UNUSED_PARAM (pInputParams);
    UNUSED_PARAM (Key);
    pOutParams = (tRtm6TrieOutParams *) pOutputParams;

    /* Routes are matched based on Prefix/len, protocol and nexthop. */
    pIp6RtEntry = (tIp6RtEntry *) pAppSpecInfo;
    pDelIp6RtEntry = (tIp6RtEntry *) * ppAppSpecInfoList;

    /* Routes are stored in the ascending order of the protocol
     * value and alternate routes from same protocol are
     * sorted in the ascending order of the Metrics. Routes
     * are matched based on the protocol and nexthop. */
    for (pPrevProtoIp6RtEntry = NULL;
         ((pDelIp6RtEntry != NULL) &&
          (pDelIp6RtEntry->i1Proto != pIp6RtEntry->i1Proto));
         pPrevProtoIp6RtEntry = pDelIp6RtEntry,
         pDelIp6RtEntry = pDelIp6RtEntry->pNextRtEntry)
    {
        /* empty */
    }

    if (pDelIp6RtEntry == NULL)
    {
        /* Route is not present. */
        return TRIE_FAILURE;
    }

    for (pPrevAltIp6RtEntry = NULL;
         ((pDelIp6RtEntry != NULL) &&
          (MEMCMP (&pDelIp6RtEntry->nexthop, &pIp6RtEntry->nexthop,
                   sizeof (tIp6Addr)) != 0));
         pPrevAltIp6RtEntry = pDelIp6RtEntry,
         pDelIp6RtEntry = pDelIp6RtEntry->pNextAlternatepath)
    {
        /* empty */
    }

    if (pDelIp6RtEntry == NULL)
    {
        /* Route is not present. */
        return TRIE_FAILURE;
    }

    if (pPrevAltIp6RtEntry != NULL)
    {
        /* Intermediate route in protocol SLL is removed. */
        pPrevAltIp6RtEntry->pNextAlternatepath =
            pDelIp6RtEntry->pNextAlternatepath;
    }
    else
    {
        /* First route in protocol SLL is removed. */
        if (pPrevProtoIp6RtEntry == NULL)
        {
            if (pDelIp6RtEntry->pNextAlternatepath)
            {
                /* update the tree node with the alternate best route from
                 * the same protocol. */
                pDelIp6RtEntry->pNextAlternatepath->pNextRtEntry =
                    pDelIp6RtEntry->pNextRtEntry;
                *(ppAppSpecInfoList) =
                    (VOID *) pDelIp6RtEntry->pNextAlternatepath;
            }
            else
            {
                *(ppAppSpecInfoList) = (VOID *) pDelIp6RtEntry->pNextRtEntry;
            }
        }
        else
        {
            if (pDelIp6RtEntry->pNextAlternatepath)
            {
                pDelIp6RtEntry->pNextAlternatepath->pNextRtEntry =
                    pDelIp6RtEntry->pNextRtEntry;
                pPrevProtoIp6RtEntry->pNextRtEntry =
                    pDelIp6RtEntry->pNextAlternatepath;
            }
            else
            {
                pPrevProtoIp6RtEntry->pNextRtEntry =
                    pDelIp6RtEntry->pNextRtEntry;
            }
        }
    }

    pDelIp6RtEntry->pNextAlternatepath = NULL;
    pDelIp6RtEntry->pNextRtEntry = NULL;

    pDelIp6RtEntry->i1Refcnt--;

    /* Update the output param */
    pOutParams->pRtEntry = pDelIp6RtEntry;
    /* Free the route memory from the caller function. */
    return TRIE_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Rtm6TrieSearchExactEntryInCxt                             */
/* Description   : This function searches for a route entry in RIB           */
/*                 (Exact Match)                                             */
/* Description   : This function calls TRIE specific API to search a route   */
/*                 profile entry from RIB                                    */
/* Input(s)      : pRtm6Cxt - Rtm6 Context Pointer                           */
/*               : pRtEntry  - pointer to the application specific info that */
/*                             is going to be searched in RIB.               */
/* Output(s)     : ppRtEntryNode - Pointer to the matching application spec  */
/*                                 node.                                     */
/*                 ppRibNode    - Pointer to the Trie Node where this entry  */
/*                                present. Application is not expected to    */
/*                                process this return value and this may be  */
/*                                used in getnext routine for faster lookup  */
/* Return(s)     : RTM6_SUCCESS/RTM6_FAILURE.                                */
/*****************************************************************************/
INT4
Rtm6TrieSearchExactEntryInCxt (tRtm6Cxt * pRtm6Cxt,
                               tIp6RtEntry * pRtEntry,
                               tIp6RtEntry ** ppRtEntryNode, VOID **ppRibNode)
{
    tInputParams        InParams;
    tRtm6TrieOutParams  OutParams;
    tIp6Addr            AddrMask;
    UINT1               au1TrieKey[RTM6_MAX_TRIE_KEY_LEN];

    MEMSET ((UINT1 *) &AddrMask, IP6_UINT1_ALL_ONE, sizeof (tIp6Addr));
    MEMSET (&InParams, 0, sizeof (tInputParams));

    InParams.pRoot = pRtm6Cxt->pIp6RtTblRoot;
    InParams.i1AppId = (INT1) (pRtEntry->i1Proto - 1);
    InParams.pLeafNode = NULL;

    /* Fill the Key. */
    InParams.Key.pKey = au1TrieKey;
    MEMSET (InParams.Key.pKey, 0, RTM6_MAX_TRIE_KEY_LEN);

    Ip6CopyAddrBits ((tIp6Addr *) (VOID *) InParams.Key.pKey, &pRtEntry->dst,
                     pRtEntry->u1Prefixlen);
    Ip6CopyAddrBits ((tIp6Addr *) (VOID *) (InParams.Key.pKey + IP6_ADDR_SIZE),
                     &AddrMask, pRtEntry->u1Prefixlen);
    InParams.u1PrefixLen = (UINT1) pRtEntry->u1Prefixlen;

    if (TrieSearch (&InParams, &OutParams, ppRibNode) == TRIE_FAILURE)
    {
        RTM6_TRC_ARG2 (pRtm6Cxt->u4ContextId, RTM6_ALL_FAILURE_TRC,
                       RTM6_MOD_NAME,
                       "\tSearching Route [%s/%d] in TRIE FAILED!!!\n",
                       Ip6PrintAddr (&pRtEntry->dst), pRtEntry->u1Prefixlen);
        *ppRibNode = NULL;
        return RTM6_FAILURE;
    }

    /* Route is present in Trie. */
    *ppRtEntryNode = OutParams.pRtEntry;
    return RTM6_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Rtm6TrieCbSearchEntry                                     */
/* Description   : This is a call back function to search for a route profile*/
/*                 entry in RIB (Exact Match)                                */
/*                 (Registered with TRIE library during TRIE creation)       */
/* Input(s)      : pInputParams - void pointer used by TRIE                  */
/*                 pAppSpecInfo - address of the Application (RTM6) specific */
/*                                 info present in TRIE leaf node            */
/* Output(s)     : pOutputParams - void pointer (used by RTM6)               */
/* Return(s)     : TRIE_SUCCESS/TRIE_FAILURE.                                */
/*****************************************************************************/
INT4
Rtm6TrieCbSearchEntry (tInputParams * pInputParams,
                       VOID *pOutParams, VOID *pAppSpecInfo)
{
    tRtm6TrieOutParams *pTrieOutParams = NULL;
    tIp6RtEntry        *pIp6RtEntry = NULL;

    pTrieOutParams = (tRtm6TrieOutParams *) pOutParams;

    if (pInputParams->i1AppId == ALL_PROTO_ID)
    {
        /* Get the first route */
        pTrieOutParams->pRtEntry = (tIp6RtEntry *) pAppSpecInfo;
    }
    else
    {
        /* Select the specific protocol route. i1AppId holds (protoid - 1) */
        pIp6RtEntry = (tIp6RtEntry *) pAppSpecInfo;
        while (pIp6RtEntry)
        {
            if (pIp6RtEntry->i1Proto == (pInputParams->i1AppId + 1))
            {
                break;
            }
            pIp6RtEntry = pIp6RtEntry->pNextRtEntry;
        }

        if (pIp6RtEntry == NULL)
        {
            /* Specific Protocol route is not present. */
            pTrieOutParams->pRtEntry = NULL;
            return TRIE_FAILURE;
        }

        pTrieOutParams->pRtEntry = pIp6RtEntry;
    }

    return TRIE_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Rtm6TrieLookupEntryInCxt                                  */
/* Description   : This function calls TRIE specific API to lookup a route   */
/*                 profile entry from RIB (Longest Match)                    */
/* Input(s)      : pRtm6Cxt - Rtm6 Context Pointer                           */
/*               : pRtEntry  - pointer to the application specific info that */
/*                             is going to be searched in RIB.               */
/* Output(s)     : ppRtEntryNode - Pointer to the matching application spec  */
/*                                 node.                                     */
/*                 ppRibNode    - Pointer to the Trie Node where this entry  */
/*                                present. Application is not expected to    */
/*                                process this return value and this may be  */
/*                                used in getnext routine for faster lookup  */
/* Return(s)     : RTM6_SUCCESS/RTM6_FAILURE.                                */
/*****************************************************************************/
INT4
Rtm6TrieLookupEntryInCxt (tRtm6Cxt * pRtm6Cxt,
                          tIp6RtEntry * pRtEntry,
                          tIp6RtEntry ** ppRtEntryNode, VOID **ppRibNode)
{
    tInputParams        InParams;
    tRtm6TrieOutParams  OutParams;
    tIp6Addr            AddrMask;
    UINT1               au1TrieKey[RTM6_MAX_TRIE_KEY_LEN];

    MEMSET ((UINT1 *) &AddrMask, IP6_UINT1_ALL_ONE, sizeof (tIp6Addr));

    InParams.pRoot = pRtm6Cxt->pIp6RtTblRoot;
    InParams.i1AppId = (INT1) (pRtEntry->i1Proto - 1);
    InParams.pLeafNode = NULL;

    /* Fill the Key. */
    InParams.Key.pKey = au1TrieKey;
    MEMSET (InParams.Key.pKey, 0, RTM6_MAX_TRIE_KEY_LEN);

    Ip6CopyAddrBits ((tIp6Addr *) (VOID *) InParams.Key.pKey, &pRtEntry->dst,
                     pRtEntry->u1Prefixlen);
    Ip6CopyAddrBits ((tIp6Addr *) (VOID *) (InParams.Key.pKey + IP6_ADDR_SIZE),
                     &AddrMask, pRtEntry->u1Prefixlen);
    InParams.u1PrefixLen = (UINT1) pRtEntry->u1Prefixlen;

    if (TrieLookup (&InParams, &OutParams, ppRibNode) == TRIE_FAILURE)
    {
        RTM6_TRC_ARG2 (pRtm6Cxt->u4ContextId, RTM6_ALL_FAILURE_TRC,
                       RTM6_MOD_NAME,
                       "\tLookup for Route [%s/%d] in TRIE FAILED!!!\n",
                       Ip6PrintAddr (&pRtEntry->dst), pRtEntry->u1Prefixlen);
        *ppRibNode = NULL;
        return RTM6_FAILURE;
    }

    /* Route has been successfully added to the Trie. */
    *ppRtEntryNode = OutParams.pRtEntry;
    return RTM6_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Rtm6TrieCbLookupEntry                                     */
/* Description   : This is a call back function to look up for the route     */
/*                 entry in RIB (Long Match)                                 */
/*                 (Registered with TRIE library during TRIE creation)       */
/* Input(s)      : pInputParams - void pointer used by TRIE                  */
/*                 pAppSpecInfo - address of the Application (RTM6) specific */
/*                                 info present in TRIE leaf node            */
/*                 u2Keysize    - size of the key                            */
/*                 Key          - Key Value                                  */
/* Output(s)     : pOutputParams - void pointer (used by RTM6)               */
/* Return(s)     : TRIE_SUCCESS/TRIE_FAILURE.                                */
/*****************************************************************************/
INT4
Rtm6TrieCbLookupEntry (tInputParams * pInputParams, VOID *pOutputParams,
                       VOID *pAppSpecInfo, UINT2 u2KeySize, tKey Key)
{
    tRtm6TrieOutParams *pTrieOutParams = NULL;
    tIp6RtEntry        *pIp6RtEntry = NULL;

    UNUSED_PARAM (pInputParams);
    UNUSED_PARAM (u2KeySize);
    UNUSED_PARAM (Key);

    pTrieOutParams = (tRtm6TrieOutParams *) pOutputParams;

    if (pInputParams->i1AppId == ALL_PROTO_ID)
    {
        /* Get the first route */
        pTrieOutParams->pRtEntry = (tIp6RtEntry *) pAppSpecInfo;
    }
    else
    {
        /* Select the specific protocol route. i1AppId holds (protoid - 1) */
        pIp6RtEntry = (tIp6RtEntry *) pAppSpecInfo;
        while (pIp6RtEntry)
        {
            if (pIp6RtEntry->i1Proto == (pInputParams->i1AppId + 1))
            {
                break;
            }
            pIp6RtEntry = pIp6RtEntry->pNextRtEntry;
        }

        if (pIp6RtEntry == NULL)
        {
            /* Specific Protocol route is not present. */
            pTrieOutParams->pRtEntry = NULL;
            return TRIE_FAILURE;
        }

        pTrieOutParams->pRtEntry = pIp6RtEntry;
    }

    return TRIE_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Rtm6TrieCbBestMatch                                       */
/* Description   : This function gets the best route from the given node.    */
/* Input(s)      : pInputParams - void pointer used by TRIE                  */
/*                 pAppPtr      - address of the Application (RTM6) specific */
/*                                info present in TRIE leaf node             */
/*                 u2Keysize    - size of the key                            */
/*                 Key          - Key Value                                  */
/* Output(s)     : pOutputParams                                             */
/* Return(s)     : TRIE_SUCCESS/TRIE_FAILURE.                                */
/*****************************************************************************/
INT4
Rtm6TrieCbBestMatch (UINT2 u2KeySize, tInputParams * pInputParams,
                     VOID *pOutputParams, VOID *pAppPtr, tKey Key)
{
    tRtm6TrieOutParams *pTrieOutParams = NULL;
    tIp6RtEntry        *pIp6RtEntry = NULL;

    UNUSED_PARAM (pInputParams);
    UNUSED_PARAM (u2KeySize);
    UNUSED_PARAM (Key);

    /* This callback routine is used to find the best route in the given
     * node. But this routine is currently called only as part of the
     * TrieLookup and TrieLookup always returns the first route from the
     * node. If this routine is altered to give the best route, then
     * TrieLookUp functionality will get affected. So this routine also
     * returns the first route from the node and to get the best route
     * use the function provided by the underlying IP6 over the returned
     * route information.
     */
    pTrieOutParams = (tRtm6TrieOutParams *) pOutputParams;

    if (pInputParams->i1AppId == ALL_PROTO_ID)
    {
        /* Get the first route */
        pTrieOutParams->pRtEntry = (tIp6RtEntry *) pAppPtr;
    }
    else
    {
        /* Select the specific protocol route. i1AppId holds (protoid - 1) */
        pIp6RtEntry = (tIp6RtEntry *) pAppPtr;
        while (pIp6RtEntry)
        {
            if (pIp6RtEntry->i1Proto == (pInputParams->i1AppId + 1))
            {
                break;
            }
            pIp6RtEntry = pIp6RtEntry->pNextRtEntry;
        }

        if (pIp6RtEntry == NULL)
        {
            /* Specific Protocol route is not present. */
            pTrieOutParams->pRtEntry = NULL;
            return TRIE_FAILURE;
        }

        pTrieOutParams->pRtEntry = pIp6RtEntry;
    }

    return TRIE_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Rtm6TrieUpdateEntryInCxt                                  */
/* Description   : This function updates the route entry in the RIB with the */
/*                 given metric and if necessary rearrange the route         */
/* Input(s)      : pRtm6Cxt - Rtm6 Context Pointer                           */
/*               : pIp6RtEntry  - pointer to the application specific info   */
/*                                that is modified.                          */
/*                 u4NewMetric  - New metric value.                          */
/* Output(s)     : None.                                                     */
/* Return(s)     : RTM6_SUCCESS/RTM6_FAILURE.                                */
/*****************************************************************************/
INT4
Rtm6TrieUpdateEntryInCxt (tRtm6Cxt * pRtm6Cxt, tIp6RtEntry * pIp6RtEntry,
                          UINT4 u4NewMetric)
{
    tInputParams        InParams;
    tRtm6TrieOutParams  OutParams;
    tIp6Addr            AddrMask;
    UINT1               au1TrieKey[RTM6_MAX_TRIE_KEY_LEN];

    MEMSET ((UINT1 *) &AddrMask, IP6_UINT1_ALL_ONE, sizeof (tIp6Addr));

    InParams.pRoot = pRtm6Cxt->pIp6RtTblRoot;
    /* The update should be for specific protocol route only. */
    InParams.i1AppId = (INT1) (pIp6RtEntry->i1Proto - 1);
    InParams.pLeafNode = NULL;

    /* Fill the Key. */
    InParams.Key.pKey = au1TrieKey;
    MEMSET (InParams.Key.pKey, 0, RTM6_MAX_TRIE_KEY_LEN);

    Ip6CopyAddrBits ((tIp6Addr *) (VOID *) InParams.Key.pKey, &pIp6RtEntry->dst,
                     pIp6RtEntry->u1Prefixlen);
    Ip6CopyAddrBits ((tIp6Addr *) (VOID *) (InParams.Key.pKey + IP6_ADDR_SIZE),
                     &AddrMask, pIp6RtEntry->u1Prefixlen);
    InParams.u1PrefixLen = (UINT1) pIp6RtEntry->u1Prefixlen;

    if (TrieRevise (&InParams, pIp6RtEntry, u4NewMetric, &OutParams) ==
        TRIE_FAILURE)
    {
        RTM6_TRC_ARG2 (pRtm6Cxt->u4ContextId, RTM6_ALL_FAILURE_TRC,
                       RTM6_MOD_NAME,
                       "\tUpdating Route [%s/%d] in TRIE FAILED!!!\n",
                       Ip6PrintAddr (&pIp6RtEntry->dst),
                       pIp6RtEntry->u1Prefixlen);
        return RTM6_FAILURE;
    }

    return RTM6_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Rtm6TrieCbUpdateEntry                                     */
/* Description   : This is a call back function to look up for the route     */
/*                 entry in RIB (Long Match)                                 */
/*                 (Registered with TRIE library during TRIE creation)       */
/* Input(s)      : pInputParams - void pointer used by TRIE                  */
/*                 ppAppPtr - Pointer to the Tree node.                      */
/*                 pAppSpecInfo - address of the Application (RTM6) specific */
/*                                 info present in TRIE leaf node            */
/*                 u4NewMetric  - New metric value                           */
/* Output(s)     : pOutputParams - void pointer (used by RTM6)               */
/*                 ppAppPtr - Pointer to the updated Tree node.              */
/* Return(s)     : TRIE_SUCCESS/TRIE_FAILURE.                                */
/*****************************************************************************/
INT4
Rtm6TrieCbUpdateEntry (tInputParams * pInputParams, VOID *pOutputParams,
                       VOID **ppAppPtr, VOID *pAppSpecInfo, UINT4 u4NewMetric)
{
    tIp6RtEntry        *pUpdIp6RtEntry = NULL;
    tIp6RtEntry        *pPrevProtoIp6RtEntry = NULL;
    tIp6RtEntry        *pPrevAltIp6RtEntry = NULL;
    tIp6RtEntry        *pFirstAltIp6RtEntry = NULL;
    tIp6RtEntry        *pTmpIp6RtEntry = NULL;

    UNUSED_PARAM (pInputParams);
    UNUSED_PARAM (pOutputParams);

    /* Routes are matched based on Prefix/len, protocol and ifindex. */
    pUpdIp6RtEntry = (tIp6RtEntry *) pAppSpecInfo;
    pTmpIp6RtEntry = (tIp6RtEntry *) (*ppAppPtr);

    /* Routes are stored in the ascending order of the protocol
     * value and alternate routes from same protocol are
     * sorted in the ascending order of the Metrics. */

    for (pPrevProtoIp6RtEntry = NULL;
         ((pTmpIp6RtEntry != NULL) &&
          (pTmpIp6RtEntry->i1Proto != pUpdIp6RtEntry->i1Proto));
         pPrevProtoIp6RtEntry = pTmpIp6RtEntry,
         pTmpIp6RtEntry = pTmpIp6RtEntry->pNextRtEntry)
    {
        /* empty */
    }

    if (pTmpIp6RtEntry == NULL)
    {
        /* Route is not present. */
        return TRIE_FAILURE;
    }

    pFirstAltIp6RtEntry = pTmpIp6RtEntry;
    /* Have reached same protocol route list */
    if ((pFirstAltIp6RtEntry == pUpdIp6RtEntry) &&
        (pUpdIp6RtEntry->pNextAlternatepath == NULL))
    {
        /* There is only one route from this protocol and there
         * is no alternative route. So just update the metric and
         * return. */
        pUpdIp6RtEntry->u4Metric = u4NewMetric;
        return TRIE_SUCCESS;
    }

    for (pPrevAltIp6RtEntry = NULL;
         ((pTmpIp6RtEntry != NULL) && (pTmpIp6RtEntry != pUpdIp6RtEntry));
         pPrevAltIp6RtEntry = pTmpIp6RtEntry,
         pTmpIp6RtEntry = pTmpIp6RtEntry->pNextAlternatepath)
    {
        /* empty */
    }

    if (pTmpIp6RtEntry == NULL)
    {
        /* Route is not present. */
        return TRIE_FAILURE;
    }

    /* Route has been found. Update the New Metric */
    pUpdIp6RtEntry->u4Metric = u4NewMetric;

    if (pPrevAltIp6RtEntry != NULL)
    {
        /* The route is somewhere in the middle of the SLL. Delink the
         * route. */
        pPrevAltIp6RtEntry->pNextAlternatepath =
            pUpdIp6RtEntry->pNextAlternatepath;
    }
    else
    {
        /* Route is in the beginning of the SLL. Check whether the
         * metric is greater than the next route. If yes, then need to
         * re-order. Else no need for any change. */
        if (pUpdIp6RtEntry->u4Metric <=
            pUpdIp6RtEntry->pNextAlternatepath->u4Metric)
        {
            /* No need for any change. */
            return TRIE_SUCCESS;
        }
        else
        {
            /* Delink the route, update the first route */
            pFirstAltIp6RtEntry = pUpdIp6RtEntry->pNextAlternatepath;
            pFirstAltIp6RtEntry->pNextRtEntry = pUpdIp6RtEntry->pNextRtEntry;

            /* Also update teh previous protocol routes next. */
            if (pPrevProtoIp6RtEntry == NULL)
            {
                (*ppAppPtr) = (VOID *) pFirstAltIp6RtEntry;
            }
            else
            {
                pPrevProtoIp6RtEntry->pNextRtEntry = pFirstAltIp6RtEntry;
            }
        }
    }

    pUpdIp6RtEntry->pNextRtEntry = NULL;
    pUpdIp6RtEntry->pNextAlternatepath = NULL;

    /* Add the route back. */
    pTmpIp6RtEntry = pFirstAltIp6RtEntry;
    for (pPrevAltIp6RtEntry = NULL;
         ((pTmpIp6RtEntry != NULL) &&
          (pTmpIp6RtEntry->u4Metric <= pUpdIp6RtEntry->u4Metric));
         pPrevAltIp6RtEntry = pTmpIp6RtEntry,
         pTmpIp6RtEntry = pTmpIp6RtEntry->pNextAlternatepath)
    {
        /* empty */
    }

    if (pPrevAltIp6RtEntry != NULL)
    {
        /* Route should be added next to the pPrevAltIp6RtEntry */
        pUpdIp6RtEntry->pNextAlternatepath =
            pPrevAltIp6RtEntry->pNextAlternatepath;
        pPrevAltIp6RtEntry->pNextAlternatepath = pUpdIp6RtEntry;
    }
    else
    {
        if (pPrevProtoIp6RtEntry == NULL)
        {
            /* Route should be added as the first route in leaf node */
            pTmpIp6RtEntry = (tIp6RtEntry *) (*ppAppPtr);
        }
        else
        {
            /* Route should be added next to the pPrevProtoIp6RtEntry */
            pTmpIp6RtEntry = pPrevProtoIp6RtEntry->pNextRtEntry;
        }

        pUpdIp6RtEntry->pNextRtEntry = pTmpIp6RtEntry->pNextRtEntry;
        pTmpIp6RtEntry->pNextRtEntry = NULL;
        pUpdIp6RtEntry->pNextAlternatepath = pTmpIp6RtEntry;

        if (pPrevProtoIp6RtEntry == NULL)
        {
            /* Route should be added as the first route in leaf node */
            (*ppAppPtr) = (VOID *) pUpdIp6RtEntry;
        }
        else
        {
            /* Route should be added next to the pPrevProtoIp6RtEntry */
            pPrevProtoIp6RtEntry->pNextRtEntry = pUpdIp6RtEntry;
        }
    }
    return TRIE_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Rtm6TrieGetFirstEntryInCxt                                */
/* Description   : This function gets the first route entry in RIB           */
/* Input(s)      : pRtm6Cxt - Rtm6 Context Pointer                           */
/* Output(s)     : ppFirstRtEntry - address of the first Application specific*/
/*                                  info present in TRIE leaf node           */
/*                 ppRibNode    - Pointer to the Trie Node where this entry  */
/*                                present. Application is not expected to    */
/*                                process this return value and this may be  */
/*                                used in getnext routine for faster lookup  */
/* Return(s)     : RTM6_SUCCESS/RTM6_FAILURE.                                */
/*****************************************************************************/
INT4
Rtm6TrieGetFirstEntryInCxt (tRtm6Cxt * pRtm6Cxt, tIp6RtEntry ** ppFirstRtEntry,
                            VOID **pRibNode)
{
    tInputParams        InParams;
    INT4                i4Status = 0;

    InParams.pRoot = pRtm6Cxt->pIp6RtTblRoot;
    InParams.i1AppId = ALL_PROTO_ID;
    InParams.pLeafNode = NULL;

    i4Status = TrieGetFirstNode (&InParams, (VOID *) ppFirstRtEntry, pRibNode);
    if (i4Status == TRIE_FAILURE)
    {
        *pRibNode = NULL;
        return RTM6_FAILURE;
    }
    return RTM6_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Rtm6TrieGetNextEntryInCxt                                 */
/* Description   : This function will give you the next route entry present  */
/*                 in RIB for a specified route entry by traversing the RIB  */
/*                 for specified route and then gives the next entry         */
/* Input(s)      : pRtm6Cxt - Rtm6 Context Pointer                           */
/*               : pRtEntry  - route entry info                              */
/*                 ppRibNode - route entry RIB node address                  */
/* Output(s)     : ppNextRtEntry - address of the next Application specific  */
/*                                  info present in TRIE leaf node           */
/*                 ppRibNode    - Pointer to the Trie Node where this entry  */
/*                                present. Application is not expected to    */
/*                                process this return value and this may be  */
/*                                used in getnext routine for faster lookup  */
/* Return(s)     : RTM6_SUCCESS/RTM6_FAILURE.                                */
/*****************************************************************************/
INT4
Rtm6TrieGetNextEntryInCxt (tRtm6Cxt * pRtm6Cxt,
                           tIp6RtEntry * pRtEntry, tIp6RtEntry ** ppNextRtEntry,
                           VOID **ppRibNode)
{
    tInputParams        InParams;
    tIp6Addr            AddrMask;
    INT4                i4Status = 0;
    UINT1               au1TrieKey[RTM6_MAX_TRIE_KEY_LEN];

    MEMSET ((UINT1 *) &AddrMask, IP6_UINT1_ALL_ONE, sizeof (tIp6Addr));

    InParams.pRoot = pRtm6Cxt->pIp6RtTblRoot;
    InParams.i1AppId = ALL_PROTO_ID;
    InParams.pLeafNode = NULL;

    /* Fill the Key. */

    InParams.Key.pKey = au1TrieKey;
    MEMSET (InParams.Key.pKey, 0, RTM6_MAX_TRIE_KEY_LEN);

    Ip6CopyAddrBits ((tIp6Addr *) (VOID *) InParams.Key.pKey, &pRtEntry->dst,
                     pRtEntry->u1Prefixlen);
    Ip6CopyAddrBits ((tIp6Addr *) (VOID *) (InParams.Key.pKey + IP6_ADDR_SIZE),
                     &AddrMask, pRtEntry->u1Prefixlen);
    InParams.u1PrefixLen = (UINT1) pRtEntry->u1Prefixlen;

    i4Status = TrieGetNextNode (&InParams, *ppRibNode, (VOID *) ppNextRtEntry,
                                ppRibNode);
    if (i4Status == TRIE_FAILURE)
    {
        *ppRibNode = NULL;
        return RTM6_FAILURE;
    }
    return RTM6_SUCCESS;
}

VOID
Rtm6SetReachStateForRoute (UINT4 u4ContextId, tIp6RtEntry * pIp6Route)
{

    INT4                i4NdReachState = FALSE;
    tIp6RtEntry        *pTmp6Rt = NULL;
    pTmp6Rt = pIp6Route;

    while (pTmp6Rt != NULL)
    {
#ifdef LNXIP6_WANTED
        i4NdReachState = Lip6GetNdStateForRouteNH (u4ContextId, pTmp6Rt);
#else
        i4NdReachState = Rtm6UtilGetNdStateForRouteNH (u4ContextId, pTmp6Rt);
#endif

        if (i4NdReachState == TRUE)
        {
            pTmp6Rt->u4Flag |= RTM6_RT_REACHABLE;
        }
        else
        {
            pTmp6Rt->u4Flag &= ~(UINT4) RTM6_RT_REACHABLE;
        }
        pTmp6Rt = pTmp6Rt->pNextAlternatepath;
    }

    return;
}

VOID
Rtm6SetReachState (UINT4 u4ContextId, tIp6RtEntry * pIp6RtEntry)
{

    INT4                i4NdReachState = FALSE;
    tPNh6Node          *pNh6Entry = NULL, ExstNh6Node;

    tIp6RtEntry        *pTmp6Rt = NULL;

    MEMSET (&ExstNh6Node, 0, sizeof (tPNh6Node));
    pTmp6Rt = pIp6RtEntry;

    while (pTmp6Rt != NULL)
    {
        ExstNh6Node.NextHop = pTmp6Rt->nexthop;
        ExstNh6Node.pRtm6Cxt = UtilRtm6GetCxt (u4ContextId);

        pNh6Entry = RBTreeGet (gRtm6GlobalInfo.pRNHT6RBRoot, &ExstNh6Node);
        if (pNh6Entry != NULL)
        {
            i4NdReachState = TRUE;
        }
        else
        {
            i4NdReachState = FALSE;
        }

        if (i4NdReachState == TRUE)
        {
            pTmp6Rt->u4Flag |= RTM6_RT_REACHABLE;
        }
        else
        {

            pTmp6Rt->u4Flag &= ~(UINT4) RTM6_RT_REACHABLE;
        }
        pTmp6Rt = pTmp6Rt->pNextAlternatepath;
    }

    return;
}

VOID
Rtm6ApiHandleEcmpRtInNp (UINT4 u4ContextId, tIp6RtEntry * pIp6RtEntry,
                         UINT1 u1RtCommand)
{
    tRtm6Cxt           *pRtm6Cxt = NULL;
    tNetIpv6RtInfo      NetIpv6RtInfo;
    tRtm6FrtInfo       *pRtm6FrtNode = NULL;
#ifdef NPAPI_WANTED
    tFsNpIntInfo        IntInfo;
    tFsNpRouteInfo      RouteInfo;
    UINT4               u4NHType;
#endif

    pRtm6Cxt = UtilRtm6GetCxt (u4ContextId);
    if (pRtm6Cxt == NULL)
    {
        return;
    }

    if (u1RtCommand == IP6_ROUTE_ADD)
    {
#ifdef NPAPI_WANTED
        MEMSET (&IntInfo, 0, sizeof (tFsNpIntInfo));

        IntInfo.u1IfType = Ip6GetIfType (pIp6RtEntry->u4Index);
        if ((IntInfo.u1IfType == CFA_L3IPVLAN)
            || (IntInfo.u1IfType == CFA_L3SUB_INTF))
        {
            IntInfo.u4PhyIfIndex = pIp6RtEntry->u4Index;
        }
        if (IntInfo.u1IfType == CFA_PSEUDO_WIRE)
        {
            IntInfo.u4PhyIfIndex = pIp6RtEntry->u4Index;
        }

        if (IntInfo.u1IfType != IP6_ENET_INTERFACE_TYPE)
        {
            if (CfaGetVlanId (IntInfo.u4PhyIfIndex, &(IntInfo.u2VlanId))
                == CFA_FAILURE)
            {
            }
        }
        else
        {
            IntInfo.u4PhyIfIndex = pIp6RtEntry->u4Index;
            IntInfo.u2VlanId = 0;
        }
        /* check for the type before programming */
        if (pIp6RtEntry->i1Type == IP6_ROUTE_TYPE_DIRECT)
        {
            u4NHType = NH_DIRECT;
        }
        else if (pIp6RtEntry->i1Type == IP6_ROUTE_TYPE_DISCARD)
        {
            u4NHType = NH_DISCARD;
        }
        else if (pIp6RtEntry->i1Type == IP6_ROUTE_TYPE_INDIRECT)
        {
            u4NHType = NH_REMOTE;
        }
        else
        {
            /* Unknown Route Type. */
            u4NHType = NH_SENDTO_CP;
        }

        if (RTM6_IS_NP_PROGRAMMING_ALLOWED () == RTM6_SUCCESS)
        {
#ifdef LNXIP6_WANTED
            if ((NetIpv6GetIpForwardingInCxt (u4ContextId) == IP6_FORW_ENABLE)
                || (pIp6RtEntry->i1Proto == CIDR_LOCAL_ID))
#endif
                /*Setting Reachable state for the route Entry */
                /*Getting no of route counts and sending it to NPAPI */
                Rtm6SetReachState (pRtm6Cxt->u4ContextId, pIp6RtEntry);
            Rtm6GetInstalledRouteCount (pIp6RtEntry, &pIp6RtEntry->u1EcmpCount);

            IntInfo.u1RouteCount = pIp6RtEntry->u1EcmpCount;
            if (Ipv6FsNpIpv6UcRouteAdd (pRtm6Cxt->u4ContextId,
                                        (UINT1 *) &pIp6RtEntry->dst,
                                        pIp6RtEntry->u1Prefixlen,
                                        (UINT1 *) &pIp6RtEntry->nexthop,
                                        u4NHType, &IntInfo) == FNP_FAILURE)
            {
                RTM6_TRC (u4ContextId, RTM6_ALL_FAILURE_TRC |
                          RTM6_OS_RESOURCE_TRC, RTM6_MOD_NAME,
                          "\tNP Route Addition Failed \n");
                if (Rtm6FrtAddInfo (pIp6RtEntry, u4ContextId, (UINT1) u4NHType)
                    == RTM6_SUCCESS)
                {
                    Rtm6RedSyncFrtInfo (pIp6RtEntry, u4ContextId,
                                        (UINT1) u4NHType, RTM6_ADD_ROUTE);

                }
                else
                {
                    RTM6_TRC_ARG2 (pRtm6Cxt->u4ContextId, RTM6_ALL_FAILURE_TRC |
                                   RTM6_OS_RESOURCE_TRC, RTM6_MOD_NAME,
                                   "ERROR[RTM]: IPV6  Failed Route Addition Failed%s/%d   \n",
                                   Ip6PrintNtop (&pIp6RtEntry->dst),
                                   (INT4) pIp6RtEntry->u1Prefixlen);
                }
            }
            pIp6RtEntry->u4HwIntfId[0] = IntInfo.u4HwIntfId[0];
        }
#endif

        /* program route in kernel. */
        RTM6_COPY_ECMP6_ROUTE_INFO_TO_UPDATE_MSG (pIp6RtEntry, NetIpv6RtInfo);

        NetIpv6RtInfo.u4ContextId = pRtm6Cxt->u4ContextId;
        NetIpv6RtInfo.u4RowStatus = IP6FWD_ACTIVE;
        NetIpv6InvokeRouteChangeForEcmp (&NetIpv6RtInfo, NETIPV6_ALL_PROTO);

    }
    else
    {
        /*RTM6 delete on HW */
        pRtm6FrtNode = Rtm6FrtGetInfo (pIp6RtEntry, pRtm6Cxt->u4ContextId);
        if (pRtm6FrtNode != NULL)
        {
            if (Rtm6FrtDeleteEntry (pRtm6FrtNode) == RTM6_SUCCESS)
            {
                Rtm6RedSyncFrtInfo (pIp6RtEntry, pRtm6Cxt->u4ContextId,
                                    pRtm6FrtNode->u1Type, RTM6_DELETE_ROUTE);
            }
        }
#ifdef NPAPI_WANTED
        if (RTM6_IS_NP_PROGRAMMING_ALLOWED () == RTM6_SUCCESS)
        {
            /*Getting no of route counts and sending it to NPAPI */
            RouteInfo.u1RouteCount = pIp6RtEntry->u1EcmpCount;
            RouteInfo.u4HwIntfId[0] = pIp6RtEntry->u4HwIntfId[0];
            Ipv6FsNpIpv6UcRouteDelete (pRtm6Cxt->u4ContextId,
                                       (UINT1 *) &pIp6RtEntry->dst,
                                       pIp6RtEntry->u1Prefixlen,
                                       (UINT1 *) &pIp6RtEntry->nexthop,
                                       pIp6RtEntry->u4Index, &RouteInfo);
        }

#endif
        /* program route in kernel. */
        RTM6_COPY_ECMP6_ROUTE_INFO_TO_UPDATE_MSG (pIp6RtEntry, NetIpv6RtInfo);

        NetIpv6RtInfo.u4ContextId = pRtm6Cxt->u4ContextId;
        NetIpv6RtInfo.u2ChgBit = IP6_BIT_ALL;
        NetIpv6RtInfo.u4RowStatus = IP6FWD_DESTROY;
        NetIpv6InvokeRouteChangeForEcmp (&NetIpv6RtInfo, NETIPV6_ALL_PROTO);

    }

    return;

}

/******************************************************************************
 * Function           : Rtm6ApiAddOrDelRtsInCxt
 * Description        : This function programs or deletes the route entries in 
 *                      Forwarding plane based on the Best NextHop. 
 * Input(s)           : u4ContextId  - Context Id                              
 *                      u1RtCommand -  RTM status Add/Delete
 * Output(s)          : None                             
 * Returns            : None
 ******************************************************************************/
INT4
Rtm6ApiAddOrDelEcmpRtsInCxt (UINT4 u4ContextId, tIp6RtEntry * pIp6RtEntry,
                             tIp6RtEntry * pIp6BestRtEntry, UINT1 u1RtCommand)
{
    tRtm6Cxt           *pRtm6Cxt = NULL;
    tNetIpv6RtInfo      NetIpv6RtInfo;
    INT4                i4NdReachState = FALSE;
    INT4                i4Reachable = 0;
    tIp6RtEntry        *pTempRoute = NULL;
    tRtm6FrtInfo       *pRtm6FrtNode = NULL;
#ifdef NPAPI_WANTED
    tFsNpIntInfo        IntInfo;
    tFsNpRouteInfo      RouteInfo;
    UINT4               u4NHType;
#endif

    pRtm6Cxt = UtilRtm6GetCxt (u4ContextId);
    if (pRtm6Cxt == NULL)
    {
        return RTM6_FAILURE;
    }
#ifdef NPAPI_WANTED
    RouteInfo.u1NHType = NH_REMOTE;
#endif
/*Need to program ECMP routes to kernel only if the next hop is resolved. It should also be notifiec
 * only if the best route is not reachable. If a notified route already exists and is reachable,
 * then another reachable route need not be notified. 
 * If there is ADD operation and the route is not reachable, then it should be added
 * to ECMP PRT.
 * In case of delete operation, if the route is present in NP/KERNEL, it should be removed. If it is
 * not present in NP/KERNEL, then next hop should be removed from ECMP PRT in case no other route 
 * with the next hop is ECMP.
 */

    /*Check ND state */

    Rtm6SetReachStateForRoute (u4ContextId, pIp6BestRtEntry);
#ifdef LNXIP6_WANTED
    i4NdReachState = Lip6GetNdStateForRouteNH (u4ContextId, pIp6RtEntry);
#else
    /*To Code for FSIP */
    /*i4NdReachState = TRUE; */
    i4NdReachState = Rtm6UtilGetNdStateForRouteNH (u4ContextId, pIp6RtEntry);
#endif

    if (u1RtCommand == IP6_ROUTE_ADD)
    {
        if (i4NdReachState == TRUE)
        {
            /*Set reachable flag on the route */
            pIp6RtEntry->u4Flag |= RTM6_RT_REACHABLE;
#ifdef NPAPI_WANTED
            MEMSET (&IntInfo, 0, sizeof (tFsNpIntInfo));

            IntInfo.u1IfType = Ip6GetIfType (pIp6RtEntry->u4Index);
            if ((IntInfo.u1IfType == CFA_L3IPVLAN)
                || (IntInfo.u1IfType == CFA_L3SUB_INTF))
            {
                IntInfo.u4PhyIfIndex = pIp6RtEntry->u4Index;
            }
            if (IntInfo.u1IfType == CFA_PSEUDO_WIRE)
            {
                IntInfo.u4PhyIfIndex = pIp6RtEntry->u4Index;
            }

            if (IntInfo.u1IfType != IP6_ENET_INTERFACE_TYPE)
            {
                if (CfaGetVlanId (IntInfo.u4PhyIfIndex, &(IntInfo.u2VlanId))
                    == CFA_FAILURE)
                {
                }
            }
            else
            {
                IntInfo.u4PhyIfIndex = pIp6RtEntry->u4Index;
                IntInfo.u2VlanId = 0;
            }
            /* check for the type before programming */
            if (pIp6RtEntry->i1Type == IP6_ROUTE_TYPE_DIRECT)
            {
                u4NHType = NH_DIRECT;
            }
            else if (pIp6RtEntry->i1Type == IP6_ROUTE_TYPE_DISCARD)
            {
                u4NHType = NH_DISCARD;
            }
            else if (pIp6RtEntry->i1Type == IP6_ROUTE_TYPE_INDIRECT)
            {
                u4NHType = NH_REMOTE;
            }
            else
            {
                /* Unknown Route Type. */
                u4NHType = NH_SENDTO_CP;
            }

            if (RTM6_IS_NP_PROGRAMMING_ALLOWED () == RTM6_SUCCESS)
            {
#ifdef LNXIP6_WANTED
                if ((NetIpv6GetIpForwardingInCxt (u4ContextId) ==
                     IP6_FORW_ENABLE)
                    || (pIp6RtEntry->i1Proto == CIDR_LOCAL_ID))
#endif
                    /*copy ecmp count to hardware info */
                    Rtm6SetReachState (pRtm6Cxt->u4ContextId, pIp6BestRtEntry);
                Rtm6GetInstalledRouteCount (pIp6BestRtEntry,
                                            &pIp6BestRtEntry->u1EcmpCount);
                IntInfo.u1RouteCount = pIp6BestRtEntry->u1EcmpCount;
                if (Ipv6FsNpIpv6UcRouteAdd (pRtm6Cxt->u4ContextId,
                                            (UINT1 *) &pIp6RtEntry->dst,
                                            pIp6RtEntry->u1Prefixlen,
                                            (UINT1 *) &pIp6RtEntry->nexthop,
                                            u4NHType, &IntInfo) == FNP_FAILURE)
                {
                    RTM6_TRC (u4ContextId, RTM6_ALL_FAILURE_TRC |
                              RTM6_OS_RESOURCE_TRC, RTM6_MOD_NAME,
                              "\tNP Route Addition Failed \n");
                    if (Rtm6FrtAddInfo
                        (pIp6RtEntry, u4ContextId,
                         (UINT1) u4NHType) == RTM6_SUCCESS)
                    {
                        Rtm6RedSyncFrtInfo (pIp6RtEntry, u4ContextId,
                                            (UINT1) u4NHType, RTM6_ADD_ROUTE);

                    }
                    else
                    {
                        RTM6_TRC_ARG2 (pRtm6Cxt->u4ContextId,
                                       RTM6_ALL_FAILURE_TRC |
                                       RTM6_OS_RESOURCE_TRC, RTM6_MOD_NAME,
                                       "ERROR[RTM]: IPV6  Failed Route Addition Failed%s/%d   \n",
                                       Ip6PrintNtop (&pIp6RtEntry->dst),
                                       (INT4) pIp6RtEntry->u1Prefixlen);
                    }
                }
                pIp6RtEntry->u4HwIntfId[0] = IntInfo.u4HwIntfId[0];
            }
#endif
            /* program route in kernel. */
            RTM6_COPY_ECMP6_ROUTE_INFO_TO_UPDATE_MSG (pIp6RtEntry,
                                                      NetIpv6RtInfo);

            NetIpv6RtInfo.u4ContextId = pRtm6Cxt->u4ContextId;
            NetIpv6RtInfo.u4RowStatus = IP6FWD_ACTIVE;
            NetIpv6InvokeRouteChangeForEcmp (&NetIpv6RtInfo, NETIPV6_ALL_PROTO);

/* Check if best route is reachable and count of reachable routes. 
 * If count of reachable routes is 1, then remove the best route
 * from NP kernel and add to ECMP6PRT.
 */
            pTempRoute = pIp6BestRtEntry;
            i4Reachable = 0;
            while (pTempRoute != NULL)
            {
                if ((pTempRoute->u4Flag & RTM6_RT_REACHABLE) ==
                    RTM6_RT_REACHABLE)
                {
                    i4Reachable++;

                }
                pTempRoute = pTempRoute->pNextAlternatepath;
            }

            if (i4Reachable == 1)
            {
                /*Remove the best route from NP / KERNEL and add to ECMP6 PRT */
                /*RTM6 delete on HW */
#ifdef NPAPI_WANTED
                if (RTM6_IS_NP_PROGRAMMING_ALLOWED () == RTM6_SUCCESS)
                {
                    RouteInfo.u1RouteCount = pIp6BestRtEntry->u1EcmpCount;
                    RouteInfo.u4HwIntfId[0] = pIp6BestRtEntry->u4HwIntfId[0];
                    Ipv6FsNpIpv6UcRouteDelete (pRtm6Cxt->u4ContextId,
                                               (UINT1 *) &pIp6BestRtEntry->dst,
                                               pIp6BestRtEntry->u1Prefixlen,
                                               (UINT1 *) &pIp6BestRtEntry->
                                               nexthop,
                                               pIp6BestRtEntry->u4Index,
                                               &RouteInfo);
                }

#endif
                /* program route in kernel. */
                RTM6_COPY_ECMP6_ROUTE_INFO_TO_UPDATE_MSG (pIp6BestRtEntry,
                                                          NetIpv6RtInfo);

                NetIpv6RtInfo.u4ContextId = pRtm6Cxt->u4ContextId;
                NetIpv6RtInfo.u2ChgBit = IP6_BIT_ALL;
                NetIpv6RtInfo.u4RowStatus = IP6FWD_DESTROY;
                NetIpv6InvokeRouteChangeForEcmp (&NetIpv6RtInfo,
                                                 NETIPV6_ALL_PROTO);

                Rtm6AddRtToECMP6PRT (pRtm6Cxt, pIp6BestRtEntry);
            }

        }
        else
        {

            pIp6RtEntry->u4Flag &= ~(UINT4) RTM6_RT_REACHABLE;
            pTempRoute = pIp6BestRtEntry;
            i4Reachable = 0;
            while (pTempRoute != NULL)
            {
                if ((pTempRoute->u4Flag & RTM6_RT_REACHABLE) !=
                    RTM6_RT_REACHABLE)
                {
                    if ((pTempRoute->u4Flag & RTM6_RT_IN_ECMPPRT) !=
                        RTM6_RT_IN_ECMPPRT)
                    {
                        Rtm6AddRtToECMP6PRT (pRtm6Cxt, pTempRoute);
                    }
                }
                pTempRoute = pTempRoute->pNextAlternatepath;
            }

        }
    }
    else
    {
        if (i4NdReachState == TRUE)
        {
            /*RTM6 delete on HW */
            pRtm6FrtNode = Rtm6FrtGetInfo (pIp6RtEntry, pRtm6Cxt->u4ContextId);
            if (pRtm6FrtNode != NULL)
            {
                if (Rtm6FrtDeleteEntry (pRtm6FrtNode) == RTM6_SUCCESS)
                {
                    Rtm6RedSyncFrtInfo (pIp6RtEntry, pRtm6Cxt->u4ContextId,
                                        pRtm6FrtNode->u1Type,
                                        RTM6_DELETE_ROUTE);
                }
            }
#ifdef NPAPI_WANTED
            if (RTM6_IS_NP_PROGRAMMING_ALLOWED () == RTM6_SUCCESS)
            {
                RouteInfo.u1RouteCount = pIp6RtEntry->u1EcmpCount;
                RouteInfo.u4HwIntfId[0] = pIp6RtEntry->u4HwIntfId[0];
                Ipv6FsNpIpv6UcRouteDelete (pRtm6Cxt->u4ContextId,
                                           (UINT1 *) &pIp6RtEntry->dst,
                                           pIp6RtEntry->u1Prefixlen,
                                           (UINT1 *) &pIp6RtEntry->nexthop,
                                           pIp6RtEntry->u4Index, &RouteInfo);
            }

#endif
            /* program route in kernel. */
            RTM6_COPY_ECMP6_ROUTE_INFO_TO_UPDATE_MSG (pIp6RtEntry,
                                                      NetIpv6RtInfo);

            NetIpv6RtInfo.u4ContextId = pRtm6Cxt->u4ContextId;
            NetIpv6RtInfo.u2ChgBit = IP6_BIT_ALL;
            NetIpv6RtInfo.u4RowStatus = IP6FWD_DESTROY;
            NetIpv6InvokeRouteChangeForEcmp (&NetIpv6RtInfo, NETIPV6_ALL_PROTO);
/*Get the count of reachable routes. If count is >= 1 then
 * do nothing. If the count becomes zero after deletion,
 * then need to program the best route in kernel / NP.
 */
            pTempRoute = pIp6BestRtEntry;
            i4Reachable = 0;
            while (pTempRoute != NULL)
            {
                if ((pTempRoute->u4Flag & RTM6_RT_REACHABLE) ==
                    RTM6_RT_REACHABLE)
                {
                    i4Reachable++;

                }
                pTempRoute = pTempRoute->pNextAlternatepath;
            }

            if (i4Reachable == 0)
            {
#ifdef NPAPI_WANTED
                MEMSET (&IntInfo, 0, sizeof (tFsNpIntInfo));

                IntInfo.u1IfType = Ip6GetIfType (pIp6BestRtEntry->u4Index);
                if ((IntInfo.u1IfType == CFA_L3IPVLAN)
                    || (IntInfo.u1IfType == CFA_L3SUB_INTF))
                {
                    IntInfo.u4PhyIfIndex = pIp6BestRtEntry->u4Index;
                }
                if (IntInfo.u1IfType == CFA_PSEUDO_WIRE)
                {
                    IntInfo.u4PhyIfIndex = pIp6BestRtEntry->u4Index;
                }

                if (IntInfo.u1IfType != IP6_ENET_INTERFACE_TYPE)
                {
                    if (CfaGetVlanId (IntInfo.u4PhyIfIndex, &(IntInfo.u2VlanId))
                        == CFA_FAILURE)
                    {
                    }
                }
                else
                {
                    IntInfo.u4PhyIfIndex = pIp6BestRtEntry->u4Index;
                    IntInfo.u2VlanId = 0;
                }
                /* check for the type before programming */
                if (pIp6BestRtEntry->i1Type == IP6_ROUTE_TYPE_DIRECT)
                {
                    u4NHType = NH_DIRECT;
                }
                else if (pIp6BestRtEntry->i1Type == IP6_ROUTE_TYPE_DISCARD)
                {
                    u4NHType = NH_DISCARD;
                }
                else if (pIp6BestRtEntry->i1Type == IP6_ROUTE_TYPE_INDIRECT)
                {
                    u4NHType = NH_REMOTE;
                }
                else
                {
                    /* Unknown Route Type. */
                    u4NHType = NH_SENDTO_CP;
                }
                if (RTM6_IS_NP_PROGRAMMING_ALLOWED () == RTM6_SUCCESS)
                {
#ifdef LNXIP6_WANTED
                    if ((NetIpv6GetIpForwardingInCxt (u4ContextId) ==
                         IP6_FORW_ENABLE)
                        || (pIp6RtEntry->i1Proto == CIDR_LOCAL_ID))
#endif
                        /*check for reachability and get the installed route count
                         *send it to the hardware*/
                        Rtm6SetReachState (pRtm6Cxt->u4ContextId, pIp6RtEntry);
                    Rtm6GetInstalledRouteCount (pIp6RtEntry,
                                                &pIp6RtEntry->u1EcmpCount);
                    IntInfo.u1RouteCount = pIp6RtEntry->u1EcmpCount;
                    if (Ipv6FsNpIpv6UcRouteAdd (pRtm6Cxt->u4ContextId,
                                                (UINT1 *) &pIp6BestRtEntry->dst,
                                                pIp6RtEntry->u1Prefixlen,
                                                (UINT1 *) &pIp6BestRtEntry->
                                                nexthop, u4NHType,
                                                &IntInfo) == FNP_FAILURE)
                    {
                        RTM6_TRC (u4ContextId, RTM6_ALL_FAILURE_TRC |
                                  RTM6_OS_RESOURCE_TRC, RTM6_MOD_NAME,
                                  "\tNP Route Addition Failed \n");
                        if (Rtm6FrtAddInfo
                            (pIp6RtEntry, u4ContextId,
                             (UINT1) u4NHType) == RTM6_SUCCESS)
                        {
                            Rtm6RedSyncFrtInfo (pIp6RtEntry, u4ContextId,
                                                (UINT1) u4NHType,
                                                RTM6_ADD_ROUTE);

                        }
                        else
                        {
                            RTM6_TRC_ARG2 (pRtm6Cxt->u4ContextId,
                                           RTM6_ALL_FAILURE_TRC |
                                           RTM6_OS_RESOURCE_TRC, RTM6_MOD_NAME,
                                           "ERROR[RTM]: IPV6  Failed Route Addition Failed%s/%d   \n",
                                           Ip6PrintNtop (&pIp6RtEntry->dst),
                                           (INT4) pIp6RtEntry->u1Prefixlen);
                        }
                    }
                    pIp6RtEntry->u4HwIntfId[0] = IntInfo.u4HwIntfId[0];
                }
#endif

                /* program route in kernel. */
                RTM6_COPY_ECMP6_ROUTE_INFO_TO_UPDATE_MSG (pIp6BestRtEntry,
                                                          NetIpv6RtInfo);

                NetIpv6RtInfo.u4ContextId = pRtm6Cxt->u4ContextId;
                NetIpv6RtInfo.u4RowStatus = IP6FWD_ACTIVE;
                NetIpv6InvokeRouteChangeForEcmp (&NetIpv6RtInfo,
                                                 NETIPV6_ALL_PROTO);

            }

        }
        else
        {

/*If the route is not reachable,this is also not the best route. 
 * Hence it may need to be removed from the ECMP6PRT.
 */

            pIp6RtEntry->u4Flag &= ~(UINT4) RTM6_RT_REACHABLE;
            /*Need to delete from ECMP PRT */
            Rtm6DeleteRtFromECMP6PRT (pRtm6Cxt, pIp6RtEntry);
        }

    }

    return RTM6_SUCCESS;
}

/*
 *******************************************************************************
 *  Function            :   Ip6ForwardingTableAddRouteInCxt
 *  Description         :   This function is interface to add route to the
 *                          forwarding table. This function actually checks for
 *                          the route entry based on destination address, mask,
 *                          nexthop and TOS. If it is already present updates
 *                          the rowstatus and metric. If it is not there adds
 *                          the route entry in the forwarding table.                       
 *  Input parameters    :   pRtm6Cxt - Rtm6 Context Pointer                           
 *                      :   pIp6Route - Route entry to be added.
 *  Output parameters   :   None.
 *  Global variables
 *  Affected            :   Routing Table.
 *  Return value        :   RTM6_SUCCESS/RTM6_FAILURE
 *****************************************************************************
**/
INT4
Ip6ForwardingTableAddRouteInCxt (tRtm6Cxt * pRtm6Cxt, tIp6RtEntry * pIp6Route)
{
    tNetIpv6RtInfo      NetIpv6RtInfo;
    tIp6RtEntry        *pTmpIp6RtEntry = NULL;
    tIp6RtEntry        *pNewBestIp6RtEntry = NULL;
    tIp6RtEntry        *pOldBestIp6RtEntry = NULL;
    VOID               *pRibNode = NULL;
    INT4                i4OutCome = RTM6_FAILURE;
    UINT2               u2RtCount = 0;
    INT1                i1Flag = RTM6_SUCCESS;

    MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));

    Ip6GetBestRouteEntryInCxt (pRtm6Cxt, &pIp6Route->dst,
                               pIp6Route->u1Prefixlen, &pOldBestIp6RtEntry);
    if (MAX_ECMP6_PATHS != 0)
    {
        if (pOldBestIp6RtEntry != NULL)
        {
            Rtm6GetBestRouteCount (pOldBestIp6RtEntry,
                                   pOldBestIp6RtEntry->u4Metric, &u2RtCount);
            if (u2RtCount > MAX_ECMP6_PATHS)
            {
                /* If for ECMP6 group is more than 64, Deny adding to Trie */
                RTM6_TRC_ARG2 (pRtm6Cxt->u4ContextId,
                               RTM6_ALL_FAILURE_TRC | RTM6_ECMP6_FAILURE_TRC,
                               RTM6_MOD_NAME,
                               "\tMax ECMPv6 routes exist for [%s/%d] in TRIE FAILED!!!\n",
                               Ip6PrintAddr (&pIp6Route->dst),
                               pIp6Route->u1Prefixlen);

                return RTM6_FAILURE;
            }

        }
    }

    i4OutCome = Rtm6TrieSearchExactEntryInCxt (pRtm6Cxt, pIp6Route,
                                               &pTmpIp6RtEntry, &pRibNode);

    if (i4OutCome == RTM6_SUCCESS)
    {
        /*
         * Route entry is available, check for the matching route entry 
         * based on nexthop, by traversing through all alternate paths. 
         */
        if (((pTmpIp6RtEntry->i1Proto == ISIS_ID)
             && (pIp6Route->i1Proto == ISIS_ID))
            && ((pTmpIp6RtEntry->u1MetricType == IP_ISIS_LEVEL2)
                && (pIp6Route->u1MetricType == IP_ISIS_LEVEL1)))
        {
            i4OutCome =
                Ip6ForwardingTableDeleteRouteInCxt (pRtm6Cxt, pTmpIp6RtEntry);
            if (i4OutCome == RTM6_SUCCESS)
            {
                i4OutCome =
                    Ip6ForwardingTableAddRouteInCxt (pRtm6Cxt, pIp6Route);
            }
            return i4OutCome;
        }
        else if (((pTmpIp6RtEntry->i1Proto == ISIS_ID)
                  && (pIp6Route->i1Proto == ISIS_ID))
                 && ((pTmpIp6RtEntry->u1MetricType == IP_ISIS_LEVEL1)
                     && (pIp6Route->u1MetricType == IP_ISIS_LEVEL2)))
        {
            return i4OutCome;
        }
        while (pTmpIp6RtEntry != NULL)
        {
            if (MEMCMP (&pTmpIp6RtEntry->nexthop, &pIp6Route->nexthop,
                        sizeof (tIp6Addr)) == 0)
            {
                break;
            }
            pTmpIp6RtEntry = pTmpIp6RtEntry->pNextAlternatepath;
        }
    }

    if ((i4OutCome != RTM6_SUCCESS) || (pTmpIp6RtEntry == NULL))
    {
        /* Route entry is not there, so add the new route */
        switch (pIp6Route->i1Proto)
        {
            case RIPNG_ID:
                if (gRtm6GlobalInfo.u4MaxRTM6RipRoute <
                    (gRtm6GlobalInfo.u4RipRts + 1))
                {
                    i1Flag = RTM6_FAILURE;
                }
                break;
            case OSPF6_ID:
                if (gRtm6GlobalInfo.u4MaxRTM6OspfRoute <
                    (gRtm6GlobalInfo.u4OspfRts + 1))
                {
                    i1Flag = RTM6_FAILURE;
                }
                break;
            case BGP6_ID:
                if (gRtm6GlobalInfo.u4MaxRTM6BgpRoute <
                    (gRtm6GlobalInfo.u4BgpRts + 1))
                {
                    i1Flag = RTM6_FAILURE;
                }
                break;
            case IP6_NETMGMT_PROTOID:
                if (gRtm6GlobalInfo.u4MaxRTM6StaticRoute <
                    (gRtm6GlobalInfo.u4StaticRts + 1))
                {
                    i1Flag = RTM6_FAILURE;
                }
                break;
            default:
                break;

        }

        if (i1Flag != RTM6_FAILURE)
        {
            if (RTM6_GET_NODE_STATUS () == RM_STANDBY)
            {
                Rtm6RedUpdateRtParams (pIp6Route, pRtm6Cxt->u4ContextId);
            }
            i4OutCome = Rtm6TrieAddEntryInCxt (pRtm6Cxt, pIp6Route);
            if (i4OutCome == RTM6_SUCCESS)
            {
                pRtm6Cxt->u4Ip6FwdTblRouteNum++;
                if (pIp6Route->i1Proto != IP6_LOCAL_PROTOID)
                {
                    Rtm6AddNextHopForRoute (pRtm6Cxt, pIp6Route);
                }
                if (pIp6Route->u4RowStatus == RTM6_ACTIVE)
                {
                    Ip6GetBestRouteEntryInCxt (pRtm6Cxt,
                                               &pIp6Route->dst,
                                               pIp6Route->u1Prefixlen,
                                               &pNewBestIp6RtEntry);
                    if ((pNewBestIp6RtEntry != NULL)
                        && (pOldBestIp6RtEntry != pNewBestIp6RtEntry))
                    {
                        /*Case, best route has changed */
                        if (pIp6Route == pNewBestIp6RtEntry)
                        {
                            /*Incoming Route is the best route */
                            if (pOldBestIp6RtEntry != NULL)
                            {
                                /*If incoming route is the first route to be added
                                 * for the destination pOldBestIp6RtEntry will be
                                 * NULL*/
                                if (pOldBestIp6RtEntry->u4Flag & RTM6_ECMP_RT)
                                {
                                    /*If old Best  route is the ECMP route delete all
                                     * the ECMP routes*/

                                    Rtm6DelAllECMP6RtInCxt (pRtm6Cxt,
                                                            pOldBestIp6RtEntry,
                                                            pOldBestIp6RtEntry->
                                                            u4Metric);
                                }

                                /*Delete the old best route */
                                Rtm6HandleRouteUpdatesInCxt (pRtm6Cxt,
                                                             pOldBestIp6RtEntry,
                                                             FALSE,
                                                             IP6_BIT_STATUS);

                                /* Advertised route change to registered protocol */
                                RTM6_COPY_ROUTE_INFO_TO_UPDATE_MSG
                                    (pOldBestIp6RtEntry, NetIpv6RtInfo);
                                NetIpv6RtInfo.u4ContextId =
                                    pRtm6Cxt->u4ContextId;
                                NetIpv6RtInfo.u2ChgBit = IP6_BIT_STATUS;
                                NetIpv6RtInfo.u4RowStatus = IP6FWD_DESTROY;
                                NetIpv6InvokeRouteChange (&NetIpv6RtInfo,
                                                          NETIPV6_ALL_PROTO);

                            }

                            /* Old Best route is not the ECMP route.
                             * Add old route in H/W  */
                            /*Set reach state for the best route */
                            Rtm6HandleRouteUpdatesInCxt (pRtm6Cxt, pIp6Route,
                                                         TRUE, IP6_BIT_ALL);
                            /* Advertised route change to registered protocol */
                            RTM6_COPY_ROUTE_INFO_TO_UPDATE_MSG (pIp6Route,
                                                                NetIpv6RtInfo);

                            NetIpv6RtInfo.u4ContextId = pRtm6Cxt->u4ContextId;
                            NetIpv6RtInfo.u2ChgBit = IP6_BIT_ALL;
                            NetIpv6RtInfo.u4RowStatus = IP6FWD_ACTIVE;
                            NetIpv6InvokeRouteChange (&NetIpv6RtInfo,
                                                      NETIPV6_ALL_PROTO);

                            /* Add to ECMP prt table to periodically refresh ND */
                            Rtm6AddRtToECMP6PRT (pRtm6Cxt, pIp6Route);
                        }
                        /*Handle ECMP routes based on new best route */
                        Rtm6SetReachState (pRtm6Cxt->u4ContextId,
                                           pNewBestIp6RtEntry);
                        Rtm6GetInstalledRouteCount (pNewBestIp6RtEntry,
                                                    &pNewBestIp6RtEntry->
                                                    u1EcmpCount);
                        Rtm6AddAllECMP6RtInCxt (pRtm6Cxt, pNewBestIp6RtEntry);

                    }
                    else if ((pOldBestIp6RtEntry != NULL)
                             && (pOldBestIp6RtEntry == pNewBestIp6RtEntry))
                    {
                        Rtm6SetReachState (pRtm6Cxt->u4ContextId,
                                           pOldBestIp6RtEntry);
                        Rtm6GetInstalledRouteCount (pOldBestIp6RtEntry,
                                                    &pOldBestIp6RtEntry->
                                                    u1EcmpCount);
                        /*Case Best route has not changed. One route has been added. Check for ECMP and handle it */
                        if ((pOldBestIp6RtEntry->u4Flag & RTM6_ECMP_RT) ==
                            RTM6_ECMP_RT)
                        {
                            /*Case : Best route is already ECMP. Need to check if incoming route is equal in metric
                             *and install the route*/
                            if ((pIp6Route->u4RowStatus == RTM6_ACTIVE)
                                && (pIp6Route->u4Metric ==
                                    pOldBestIp6RtEntry->u4Metric))
                            {
                                Rtm6ApiAddOrDelEcmpRtsInCxt (pRtm6Cxt->
                                                             u4ContextId,
                                                             pIp6Route,
                                                             pOldBestIp6RtEntry,
                                                             IP6_ROUTE_ADD);

                            }

                        }
                        else
                        {
                            /*Case : Best route was not ECMP. Based on the addition of the new route, it may become ECMP */
                            Rtm6AddAllECMP6RtInCxt (pRtm6Cxt,
                                                    pOldBestIp6RtEntry);

                        }

                    }

                }

                return RTM6_SUCCESS;
            }
            else
            {
                /* Couldn't add the entry in the forwarding data base */
                return RTM6_FAILURE;
            }
        }
        else
        {
            /* Memory Unavilable to store route */
            RTM6_TRC_ARG1 (pRtm6Cxt->u4ContextId, RTM6_ALL_FAILURE_TRC |
                           RTM6_OS_RESOURCE_TRC, RTM6_MOD_NAME,
                           "\t Memory Unavailable for protocol type [%d]!!!\n",
                           pIp6Route->i1Proto);
            if (pIp6Route->i1Proto == IP6_NETMGMT_PROTOID)
            {
                return RTM6_NO_RESOURCE;
            }
            return RTM6_FAILURE;
        }
    }
    else
    {
        /* Route entry is available, check for the matching route entry 
         * by traversing through all alternate paths. 
         */
        i4OutCome = Ip6ForwardingTableModifyRouteInCxt (pRtm6Cxt, pIp6Route);
    }
    return i4OutCome;
}

/*
 *****************************************************************************
 *  Function            :   Ip6ForwardingTableModifyRouteInCxt
 *  Description         :   This function is interface to modify the route in
 *                          the forwarding table. This function actually checks
 *                          for the route entry based on destination address,
 *                          mask, nexthop and TOS. If it is already there
 *                          updates the rowstatus and metric. If it is not
 *                          there returns RTM6_ROUTE_NOT_FOUND.
 *  Input parameters    :   pRtm6Cxt - Rtm6 Context Pointer                           
 *                      :   pIp6Route : Route entry to be modified.
 *  Output parameters   :   None.
 *  Global variables
 *  Affected            :   Routing Table.
 *  Return value        :   RTM6_SUCCESS/RTM6_FAILURE/RTM6_NO_ROOM
 ***************************************************************************/
INT4
Ip6ForwardingTableModifyRouteInCxt (tRtm6Cxt * pRtm6Cxt,
                                    tIp6RtEntry * pIp6Route)
{
    tNetIpv6RtInfo      NetIpv6RtInfo;
    tIp6RtEntry        *pOldBestRt = NULL;
    tIp6RtEntry        *pTmpIp6RtEntry = NULL;
    tIp6RtEntry        *pNewBestRt = NULL;
    tIp6RtEntry        *pModIp6RtEntry = NULL;
    VOID               *pRibNode = NULL;
    INT4                i4OutCome = RTM6_SUCCESS;
    UINT4               u4OldMetric = 0;
    UINT2               u2ChgBit = 0;
    UINT2               u2BestRtCount = 0;
    UINT1               u1RtCount = 1;
    UINT1               u1OldEcmp = FALSE;
    INT1                i1Flag = RTM6_SUCCESS;

    MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));

    i4OutCome = Rtm6TrieSearchExactEntryInCxt (pRtm6Cxt, pIp6Route,
                                               &pModIp6RtEntry, &pRibNode);
    if (i4OutCome == RTM6_SUCCESS)
    {
        /* Route entry is available, check for the matching route entry 
         * based on nexthop, by traversing through all alternate paths. 
         */
        while (pModIp6RtEntry != NULL)
        {
            if (MEMCMP (&pModIp6RtEntry->nexthop, &pIp6Route->nexthop,
                        sizeof (tIp6Addr)) == 0)
            {
                break;
            }
            pModIp6RtEntry = pModIp6RtEntry->pNextAlternatepath;
        }
    }

    if ((i4OutCome != RTM6_SUCCESS) || (pModIp6RtEntry == NULL))
    {
        /* Route entry is not there, so add the new route */
        switch (pIp6Route->i1Proto)
        {
            case RIPNG_ID:
                if (gRtm6GlobalInfo.u4MaxRTM6RipRoute <
                    (gRtm6GlobalInfo.u4RipRts + 1))
                {
                    i1Flag = RTM6_FAILURE;
                }
                break;
            case OSPF6_ID:
                if (gRtm6GlobalInfo.u4MaxRTM6OspfRoute <
                    (gRtm6GlobalInfo.u4OspfRts + 1))
                {
                    i1Flag = RTM6_FAILURE;
                }
                break;
            case BGP6_ID:
                if (gRtm6GlobalInfo.u4MaxRTM6BgpRoute <
                    (gRtm6GlobalInfo.u4BgpRts + 1))
                {
                    i1Flag = RTM6_FAILURE;
                }
                break;
            case IP6_NETMGMT_PROTOID:
                if (gRtm6GlobalInfo.u4MaxRTM6StaticRoute <
                    (gRtm6GlobalInfo.u4StaticRts + 1))
                {
                    i1Flag = RTM6_FAILURE;
                }
                break;
            default:
                break;

        }

        if (i1Flag != RTM6_FAILURE)
        {
            i4OutCome = Rtm6TrieAddEntryInCxt (pRtm6Cxt, pIp6Route);
            if (i4OutCome == RTM6_SUCCESS)
            {
                pRtm6Cxt->u4Ip6FwdTblRouteNum++;
                if (pIp6Route->i1Proto != IP6_LOCAL_PROTOID)
                {
                    Rtm6AddNextHopForRoute (pRtm6Cxt, pIp6Route);
                }
                if (pIp6Route->u4RowStatus == RTM6_ACTIVE)
                {
                    Ip6GetBestRouteEntryInCxt (pRtm6Cxt,
                                               &pIp6Route->dst,
                                               pIp6Route->u1Prefixlen,
                                               &pModIp6RtEntry);
                    if (pIp6Route == pModIp6RtEntry)
                    {
                        /* Check whether Resolved Next hop entry with this next hop exists */
                        /* New route added is the best route. */
                        Rtm6HandleRouteUpdatesInCxt (pRtm6Cxt, pIp6Route,
                                                     TRUE, IP6_BIT_ALL);

                        /* Advertised route change to registered protocol */
                        RTM6_COPY_ROUTE_INFO_TO_UPDATE_MSG (pIp6Route,
                                                            NetIpv6RtInfo);
                        NetIpv6RtInfo.u4ContextId = pRtm6Cxt->u4ContextId;
                        NetIpv6RtInfo.u2ChgBit = IP6_BIT_ALL;
                        NetIpv6RtInfo.u4RowStatus = IP6FWD_ACTIVE;
                        NetIpv6InvokeRouteChange (&NetIpv6RtInfo,
                                                  NETIPV6_ALL_PROTO);
                    }
                }
                return RTM6_SUCCESS;
            }
            else
            {
                /* Couldn't add the entry in the forwarding data base */
                return RTM6_FAILURE;
            }
        }
        else
        {
            /* Memory Unavilable to store route */
            RTM6_TRC_ARG1 (pRtm6Cxt->u4ContextId, RTM6_ALL_FAILURE_TRC |
                           RTM6_OS_RESOURCE_TRC, RTM6_MOD_NAME,
                           "\t Memory Unavailable for protocol type [%d]!!!\n",
                           pIp6Route->i1Proto);
            return RTM6_FAILURE;

        }
    }
    else
    {
        /* Get the current Best route. */
        Ip6GetBestRouteEntryInCxt (pRtm6Cxt,
                                   &pIp6Route->dst, pIp6Route->u1Prefixlen,
                                   &pOldBestRt);

        /* Modify the existing route */
        if (MEMCMP (&pIp6Route->nexthop, &pModIp6RtEntry->nexthop,
                    IP6_ADDR_SIZE) != 0)
        {
            /* Route nexthop is modified */
            IPV6_SET_BIT_FOR_CHANGED_PARAM (u2ChgBit, IP6_BIT_NXTHOP);
            MEMCPY (&pModIp6RtEntry->nexthop, &pIp6Route->nexthop,
                    IP6_ADDR_SIZE);
        }

        if (pIp6Route->i1Type != pModIp6RtEntry->i1Type)
        {
            /* Rotue Type is not matching */
            IPV6_SET_BIT_FOR_CHANGED_PARAM (u2ChgBit, IP6_BIT_RT_TYPE);
            pModIp6RtEntry->i1Type = pIp6Route->i1Type;
        }

        if ((pModIp6RtEntry->i1Proto == BGP6_ID) &&
            (pModIp6RtEntry->u1Flag & RTM6_GR_RT))
        {
            pModIp6RtEntry->u1Flag &= ~RTM6_GR_RT;
            u2ChgBit = RTM6_GR_RT;
            Rtm6TrieUpdateEntryInCxt (pRtm6Cxt, pModIp6RtEntry,
                                      pIp6Route->u4Metric);
        }

        if ((pModIp6RtEntry->i1Proto == RIPNG_ID) &&
            (pModIp6RtEntry->u1Flag & RTM6_GR_RT))
        {
            pModIp6RtEntry->u1Flag &= (UINT4) (~RTM6_GR_RT);
            u2ChgBit = RTM6_GR_RT;
            Rtm6TrieUpdateEntryInCxt (pRtm6Cxt, pModIp6RtEntry,
                                      pIp6Route->u4Metric);
        }
        /* Set GR flag for the routes updated by routing protocol */
        if ((pModIp6RtEntry->u1Flag & RTM6_GR_RT) &&
            (pIp6Route->u1Flag != pModIp6RtEntry->u1Flag) &&
            (pModIp6RtEntry->i1Proto == ISIS_ID))
        {
            Rtm6TrieUpdateEntryInCxt (pRtm6Cxt, pIp6Route, pIp6Route->u4Metric);
        }

        /* Set GR flag for the routes updated by routing protocol */
        if (pIp6Route->u1Flag & RTM6_GR_RT)
        {
            pModIp6RtEntry->u1Flag |= RTM6_GR_RT;
        }
        if (pIp6Route->u4Metric != pModIp6RtEntry->u4Metric)
        {
            /* Rotue Type is not matching */
            IPV6_SET_BIT_FOR_CHANGED_PARAM (u2ChgBit, IP6_BIT_METRIC);
            /* Copy the old metric for ECMP handling */
            u4OldMetric = pModIp6RtEntry->u4Metric;
            Rtm6TrieUpdateEntryInCxt (pRtm6Cxt, pModIp6RtEntry,
                                      pIp6Route->u4Metric);
        }

        if (pIp6Route->u4RowStatus != pModIp6RtEntry->u4RowStatus)
        {
            IPV6_SET_BIT_FOR_CHANGED_PARAM (u2ChgBit, IP6_BIT_STATUS);
            pModIp6RtEntry->u4RowStatus = pIp6Route->u4RowStatus;
        }

        if ((u2ChgBit != 0)
            || ((pModIp6RtEntry != NULL)
                && (pModIp6RtEntry->u1Preference != pIp6Route->u1Preference)))
        {
            /* This Bitmask will be set only in the case any change 
             * is there for the route. So avoids the unnecessary 
             * notifications to the registered protocols */
            pModIp6RtEntry->u1Preference = pIp6Route->u1Preference;
            Ip6GetBestRouteEntryInCxt (pRtm6Cxt,
                                       &pIp6Route->dst, pIp6Route->u1Prefixlen,
                                       &pNewBestRt);

            if (pOldBestRt == pNewBestRt)
            {
                if (pNewBestRt == pModIp6RtEntry)
                {
                    if ((u2ChgBit & IP6_BIT_METRIC) == IP6_BIT_METRIC)
                    {
                        if ((pOldBestRt->u4Flag) & (RTM6_ECMP_RT))
                        {
                            /*Delete the ECMP routes for the old best route */
                            Rtm6DelAllECMP6RtInCxt (pRtm6Cxt, pOldBestRt,
                                                    u4OldMetric);
                        }
                        RTM6_COPY_ROUTE_INFO_TO_UPDATE_MSG (pModIp6RtEntry,
                                                            NetIpv6RtInfo);
                        NetIpv6RtInfo.u4Metric = u4OldMetric;
                        NetIpv6RtInfo.u4ContextId = pRtm6Cxt->u4ContextId;
                        NetIpv6RtInfo.u2ChgBit = IP6_BIT_STATUS;
                        NetIpv6RtInfo.u4RowStatus = IP6FWD_DESTROY;
                        NetIpv6InvokeRouteChange (&NetIpv6RtInfo,
                                                  NETIPV6_ALL_PROTO);

                    }

                    /* Best route is modified but still it remains
                     * the best route. */
                    Rtm6HandleRouteUpdatesInCxt (pRtm6Cxt, pModIp6RtEntry,
                                                 TRUE, u2ChgBit);

                    /* Advertised route change to registered protocol */
                    RTM6_COPY_ROUTE_INFO_TO_UPDATE_MSG (pModIp6RtEntry,
                                                        NetIpv6RtInfo);
                    NetIpv6RtInfo.u4ContextId = pRtm6Cxt->u4ContextId;
                    NetIpv6RtInfo.u2ChgBit = u2ChgBit;
                    NetIpv6RtInfo.u4RowStatus = IP6FWD_ACTIVE;
                    NetIpv6InvokeRouteChange (&NetIpv6RtInfo,
                                              NETIPV6_ALL_PROTO);

                    /*Handle all ECMP routes for the new best route */
                    Rtm6AddAllECMP6RtInCxt (pRtm6Cxt, pNewBestRt);
                    Rtm6AddRtToECMP6PRT (pRtm6Cxt, pNewBestRt);

                }
                else
                {
                    /*A non best route entry is getting modified. Check for ECMP */
                    pTmpIp6RtEntry = pNewBestRt;
                    u2BestRtCount = 0;
                    while (pTmpIp6RtEntry != NULL)
                    {
                        if (pTmpIp6RtEntry == pModIp6RtEntry)
                        {
                            /*Store the old ECMP STATUS of the modified non best route */
                            if ((pModIp6RtEntry->u4Flag & RTM6_ECMP_RT) ==
                                RTM6_ECMP_RT)
                            {
                                u1OldEcmp = TRUE;
                            }
                            else
                            {
                                u1OldEcmp = FALSE;
                            }

                            if ((pTmpIp6RtEntry->u4Metric ==
                                 pNewBestRt->u4Metric)
                                && (pTmpIp6RtEntry->i1Proto != STATIC_ID))
                            {
                                if (pModIp6RtEntry->u4RowStatus ==
                                    IP6FWD_ACTIVE)
                                {
                                    if (((pTmpIp6RtEntry->i1Proto == ISIS_ID)
                                         && (pNewBestRt->i1Proto == ISIS_ID)
                                         && (pTmpIp6RtEntry->u1MetricType ==
                                             pNewBestRt->u1MetricType))
                                        || ((pTmpIp6RtEntry->i1Proto != ISIS_ID)
                                            && (pNewBestRt->i1Proto !=
                                                ISIS_ID)))
                                    {
                                        if (u2BestRtCount > 0)
                                        {
                                            pTmpIp6RtEntry->u4Flag |=
                                                RTM6_ECMP_RT;
                                            u2BestRtCount++;
                                        }
                                    }
                                }
                            }
                            else if ((pTmpIp6RtEntry->u1Preference ==
                                      pNewBestRt->u1Preference)
                                     && (pTmpIp6RtEntry->i1Proto == STATIC_ID))
                            {
                                if (pModIp6RtEntry->u4RowStatus ==
                                    IP6FWD_ACTIVE)
                                {
                                    if (u2BestRtCount > 0)
                                    {
                                        pTmpIp6RtEntry->u4Flag |= RTM6_ECMP_RT;
                                        u2BestRtCount++;
                                    }
                                }
                            }
                            else
                            {
                                pTmpIp6RtEntry->u4Flag &= ~(UINT4) RTM6_ECMP_RT;
                            }
                        }
                        else
                        {

                            if ((pTmpIp6RtEntry->u4Metric ==
                                 pNewBestRt->u4Metric)
                                && (pTmpIp6RtEntry->i1Proto != STATIC_ID))
                            {
                                if (((pTmpIp6RtEntry->i1Proto == ISIS_ID)
                                     && (pNewBestRt->i1Proto == ISIS_ID)
                                     && (pTmpIp6RtEntry->u1MetricType ==
                                         pNewBestRt->u1MetricType))
                                    || ((pTmpIp6RtEntry->i1Proto != ISIS_ID)
                                        && (pNewBestRt->i1Proto != ISIS_ID)))
                                {
                                    u2BestRtCount++;
                                }
                            }
                            else if ((pTmpIp6RtEntry->u1Preference ==
                                      pNewBestRt->u1Preference)
                                     && (pTmpIp6RtEntry->i1Proto == STATIC_ID))
                            {
                                u2BestRtCount++;
                            }
                        }

                        Rtm6SetReachState (pRtm6Cxt->u4ContextId,
                                           pTmpIp6RtEntry);
                        pTmpIp6RtEntry = pTmpIp6RtEntry->pNextAlternatepath;
                    }

                    if (pNewBestRt != NULL)
                    {
                        /*If the bestroute count is greater than zero,it is ECMP.So passing 
                         *the no of routes which is reachable to the ECMP count which is 
                         *used for programming in NPAPI*/

                        if (u2BestRtCount > 0)
                        {
                            Rtm6GetInstalledRouteCount (pNewBestRt, &u1RtCount);
                            pModIp6RtEntry->u1EcmpCount = u1RtCount;

                        }
                    }

                    /*If the best route becomes ECMP due to modification of another route then handle it.
                       if the best route is already ECMP then need to do anything */
                    if ((u2BestRtCount > 1)
                        && ((pNewBestRt->u4Flag & RTM6_ECMP_RT) == 0))
                    {
                        pNewBestRt->u4Flag |= RTM6_ECMP_RT;

                        if ((pNewBestRt->u4Flag & RTM6_RT_REACHABLE) == 0)
                        {
                            Rtm6DeleteRtFromPRTInCxt (pRtm6Cxt, pNewBestRt);
                            /*Rtm6AddRtToECMP6PRTInCxt (pRtm6Cxt, pNewBestRt); */
                        }

                    }
                    else if (u2BestRtCount == 1)
                    {
                        pNewBestRt->u4Flag &= ~(UINT4) RTM6_ECMP_RT;
                    }

                    /*To check if metric is changed or the route is made INACTIVE */
                    if ((u1OldEcmp == FALSE)
                        && ((pModIp6RtEntry->u4Flag & RTM6_ECMP_RT) ==
                            RTM6_ECMP_RT))
                    {
                        Rtm6ApiAddOrDelEcmpRtsInCxt (pRtm6Cxt->u4ContextId,
                                                     pModIp6RtEntry, pNewBestRt,
                                                     IP6_ROUTE_ADD);
                    }
                    else if ((u1OldEcmp == TRUE)
                             && ((pModIp6RtEntry->u4Flag & RTM6_ECMP_RT) == 0))
                    {
                        Rtm6ApiAddOrDelEcmpRtsInCxt (pRtm6Cxt->u4ContextId,
                                                     pModIp6RtEntry, pNewBestRt,
                                                     IP6_ROUTE_DEL);
                        pModIp6RtEntry->u4Flag &= ~(UINT4) RTM6_ECMP_RT;
                    }
                    else if ((u1OldEcmp == TRUE)
                             && ((pModIp6RtEntry->u4Flag & RTM6_ECMP_RT) ==
                                 RTM6_ECMP_RT))
                    {

                        /*TODO Need to handle this case */
                    }

                }
            }
            else
            {
                /* Change in Best route. Remove the old best route
                 * and redistribute the new best route. */
                if (pOldBestRt != NULL)
                {
                    Rtm6HandleRouteUpdatesInCxt (pRtm6Cxt, pOldBestRt,
                                                 FALSE, IP6_BIT_STATUS);

                    /* Advertised route change to registered protocol */
                    RTM6_COPY_ROUTE_INFO_TO_UPDATE_MSG (pOldBestRt,
                                                        NetIpv6RtInfo);
                    NetIpv6RtInfo.u4ContextId = pRtm6Cxt->u4ContextId;
                    NetIpv6RtInfo.u2ChgBit = IP6_BIT_STATUS;
                    NetIpv6RtInfo.u4RowStatus = IP6FWD_DESTROY;
                    NetIpv6RtInfo.u4Metric = u4OldMetric;
                    NetIpv6InvokeRouteChange (&NetIpv6RtInfo,
                                              NETIPV6_ALL_PROTO);
                    /*If old best route had ECMP routes then need to remove them */
                    if ((pOldBestRt->u4Flag) & (RTM6_ECMP_RT))
                    {

                        Rtm6SetReachState (pRtm6Cxt->u4ContextId, pOldBestRt);
                        Rtm6GetInstalledRouteCount (pOldBestRt, &u1RtCount);
                        pOldBestRt->u1EcmpCount = u1RtCount;
                        Rtm6DelAllECMP6RtInCxt (pRtm6Cxt, pOldBestRt,
                                                pOldBestRt->u4Metric);
                    }
                }
                if (pNewBestRt != NULL)
                {
                    /*Check ND state  and set reachability */
                    Rtm6SetReachState (pRtm6Cxt->u4ContextId, pNewBestRt);
                    Rtm6GetInstalledRouteCount (pNewBestRt, &u1RtCount);
                    pNewBestRt->u1EcmpCount = u1RtCount;
                    if (u1RtCount > 0)
                    {
                        Rtm6NotifyNewECMP6RouteInCxt (pRtm6Cxt,
                                                      pNewBestRt,
                                                      pNewBestRt->u4Metric);
                    }

                    /* Check whether Resolved Next hop entry with this next hop exists */

                    /* Advertised route change to registered protocol */
                    Rtm6HandleRouteUpdatesInCxt (pRtm6Cxt, pNewBestRt, TRUE,
                                                 IP6_BIT_ALL);

                    /* Advertised route change to registered protocol */
                    RTM6_COPY_ROUTE_INFO_TO_UPDATE_MSG (pNewBestRt,
                                                        NetIpv6RtInfo);
                    NetIpv6RtInfo.u4ContextId = pRtm6Cxt->u4ContextId;
                    NetIpv6RtInfo.u2ChgBit = IP6_BIT_ALL;
                    NetIpv6RtInfo.u4RowStatus = IP6FWD_ACTIVE;
                    NetIpv6InvokeRouteChange (&NetIpv6RtInfo,
                                              NETIPV6_ALL_PROTO);

                    Rtm6AddAllECMP6RtInCxt (pRtm6Cxt, pNewBestRt);
                    /*  When the Route's RowStatus is made active , it needs
                     *  to be added to PRT Table so that NextHop resolution
                     *  will happen periodically */
                    Rtm6AddRtToECMP6PRT (pRtm6Cxt, pNewBestRt);

                    /*To move the below functionality inside Rtm6AddAllECMP6RtInCxt */

                }
            }

        }

        /* This case exists when the route entry already present
         * and we made modifications to the existing route entry,
         * this allocated entry is used for only temperary allocation,
         * So need to be released.
         */
        IP6_RT_FREE (pIp6Route);
    }

    return RTM6_SUCCESS;

}

/*
 *******************************************************************************
 *  Function            :   Ip6ForwardingTableDeleteRouteInCxt
 *  Description         :   This function is interface for the routing protocols
 *                          to delete the route in the forwarding table. This 
 *                          function actually checks for the route entry based 
 *                          on destination address, mask, nexthop and TOS. If it
 *                          is there deletes the route entry. 
 *                          If it is not there returns RTM6_ROUTE_NOT_FOUND.                       
 *  Input parameters    :   pRtm6Cxt - Rtm6 Context Pointer                           
 *                      :   pIp6Route : Route entry to be deleted.
 *  Output parameters   :   None.
 *  Global variables
 *  Affected            :   Routing Table.
 *  Return value        :   RTM6_SUCCESS/RTM6_FAILURE/RTM6_ROUTE_NOT_FOUND.
 *****************************************************************************
*/
INT4
Ip6ForwardingTableDeleteRouteInCxt (tRtm6Cxt * pRtm6Cxt,
                                    tIp6RtEntry * pIp6Route)
{
    tNetIpv6RtInfo      NetIpv6RtInfo;
    tIp6RtEntry        *pDelIp6RtEntry = NULL;
    tIp6RtEntry        *pBestRt = NULL;
    tIp6RtEntry        *pTmpIp6RtEntry = NULL;
    VOID               *pRibNode = NULL;
    INT4                i4OutCome = RTM6_SUCCESS;
    UINT2               u2BestRtCount = 0;
    INT4                i4ReachCount = 0;
    UINT1               u1ReachCountstatus = 0;
    MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));

    i4OutCome = Rtm6TrieSearchExactEntryInCxt (pRtm6Cxt,
                                               pIp6Route, &pDelIp6RtEntry,
                                               &pRibNode);
    if (i4OutCome == RTM6_SUCCESS)
    {

        /* route count for non best route */
        Rtm6GetInstalledRouteCountStatus (pRtm6Cxt, pDelIp6RtEntry,
                                          &pDelIp6RtEntry->u1EcmpCount);

        u1ReachCountstatus = pDelIp6RtEntry->u1EcmpCount;

        /* route count for best route */
        pDelIp6RtEntry->u1EcmpCount = 0;

        Rtm6GetInstalledRouteCount (pDelIp6RtEntry,
                                    &pDelIp6RtEntry->u1EcmpCount);
        if (pDelIp6RtEntry->i1MetricType5 != pIp6Route->i1MetricType5)
        {
            if (MEMCMP (&pDelIp6RtEntry->nexthop, &pIp6Route->nexthop,
                        sizeof (tIp6Addr)) == 0)
            {
                return RTM6_FAILURE;
            }
        }
        /* Route entry is available, check for the matching route entry 
         * based on nexthop, by traversing through all alternate paths.
         */
        while (pDelIp6RtEntry != NULL)
        {
            if (MEMCMP (&pDelIp6RtEntry->nexthop, &pIp6Route->nexthop,
                        sizeof (tIp6Addr)) == 0)
            {
                if (((pDelIp6RtEntry->i1Proto == ISIS_ID)
                     && (pIp6Route->i1Proto == ISIS_ID))
                    && (pIp6Route->u1MetricType !=
                        pDelIp6RtEntry->u1MetricType))
                {
                    pDelIp6RtEntry = pDelIp6RtEntry->pNextAlternatepath;
                    continue;
                }
                break;
            }
            pDelIp6RtEntry = pDelIp6RtEntry->pNextAlternatepath;
        }
    }

    if ((i4OutCome != RTM6_SUCCESS) || (pDelIp6RtEntry == NULL))
    {
        /* Route is not present in the TRIE */
        return RTM6_FAILURE;
    }

    /* Get the current best route. */
    Ip6GetBestRouteEntryInCxt (pRtm6Cxt, &pIp6Route->dst,
                               pIp6Route->u1Prefixlen, &pBestRt);

    /*if entry to be deleted is the current best route, then remove ECMP routes */

    if (pBestRt != NULL)
    {
        Rtm6GetInstalledRouteCount (pBestRt, &pBestRt->u1EcmpCount);

        Rtm6GetInstalledRouteCount (pIp6Route, &pIp6Route->u1EcmpCount);

        if ((pBestRt->u1EcmpCount == pIp6Route->u1EcmpCount)
            && ((pBestRt->u4Flag & RTM6_ECMP_RT) == RTM6_ECMP_RT)
            && ((pIp6Route->u4Flag & RTM6_ECMP_RT) == RTM6_ECMP_RT))
        {
            Rtm6DelAllECMP6RtInCxt (pRtm6Cxt, pDelIp6RtEntry,
                                    pBestRt->u4Metric);
            /*Unset the ECMP flag for the best route */
            pBestRt->u4Flag &= (UINT4) (~RTM6_ECMP_RT);
        }

        while (pDelIp6RtEntry != NULL)
        {
            if (MEMCMP (&pDelIp6RtEntry->nexthop, &pIp6Route->nexthop,
                        sizeof (tIp6Addr)) == 0)
            {
                i4ReachCount = TRUE;
                break;
            }
            pDelIp6RtEntry = pDelIp6RtEntry->pNextAlternatepath;
        }

        i4OutCome = Rtm6TrieDeleteEntryInCxt (pRtm6Cxt, pDelIp6RtEntry);
        if (i4OutCome == RTM6_SUCCESS)
        {
            if (pIp6Route->i1Proto != IP6_LOCAL_PROTOID)
            {
                Rtm6DeleteNextHopforRoute (pRtm6Cxt, pIp6Route);
            }
            pRtm6Cxt->u4Ip6FwdTblRouteNum--;
            /* Set the row-status to destroy and set the 
             * bitmask to status change to notify  
             */
            pDelIp6RtEntry->u4RowStatus = IP6FWD_NOT_IN_SERVICE;
            /* Check for Redistribution */
            if (pDelIp6RtEntry == pBestRt)
            {
                /* Route removed is the old best route. */

                pDelIp6RtEntry->u1EcmpCount = u1ReachCountstatus;
                Rtm6HandleRouteUpdatesInCxt (pRtm6Cxt, pDelIp6RtEntry,
                                             FALSE, IP6_BIT_STATUS);

                /* Advertised route change to registered protocol */
                RTM6_COPY_ROUTE_INFO_TO_UPDATE_MSG (pDelIp6RtEntry,
                                                    NetIpv6RtInfo);
                NetIpv6RtInfo.u4ContextId = pRtm6Cxt->u4ContextId;
                NetIpv6RtInfo.u2ChgBit = IP6_BIT_STATUS;
                NetIpv6RtInfo.u4RowStatus = IP6FWD_DESTROY;
                NetIpv6InvokeRouteChange (&NetIpv6RtInfo, NETIPV6_ALL_PROTO);

                /* Get the New best route if present. */
                Ip6GetBestRouteEntryInCxt (pRtm6Cxt,
                                           &pIp6Route->dst,
                                           pIp6Route->u1Prefixlen, &pBestRt);
                if (pBestRt != NULL)
                {
                    /*One of the ECMP rotue got deleted so remove the route from PRT */
                    Rtm6DeleteRtFromECMP6PRT (pRtm6Cxt, pDelIp6RtEntry);
                    Rtm6HandleRouteUpdatesInCxt (pRtm6Cxt, pBestRt,
                                                 TRUE, IP6_BIT_ALL);

                    /* Advertised route change to registered protocol */
                    RTM6_COPY_ROUTE_INFO_TO_UPDATE_MSG (pBestRt, NetIpv6RtInfo);
                    NetIpv6RtInfo.u4ContextId = pRtm6Cxt->u4ContextId;
                    NetIpv6RtInfo.u2ChgBit = IP6_BIT_ALL;
                    NetIpv6RtInfo.u4RowStatus = IP6FWD_ACTIVE;
                    NetIpv6InvokeRouteChange (&NetIpv6RtInfo,
                                              NETIPV6_ALL_PROTO);

                    /*Need to calculate ECMP routes based on new best route */
                    /*Handle ECMP routes based on new best route */
                    Rtm6SetReachState (pRtm6Cxt->u4ContextId, pBestRt);
                    Rtm6GetInstalledRouteCount (pBestRt, &pBestRt->u1EcmpCount);
                    Rtm6AddAllECMP6RtInCxt (pRtm6Cxt, pBestRt);
                    if ((pBestRt->u4Flag & RTM6_ECMP_RT) != RTM6_ECMP_RT)
                    {
                        if (pBestRt->u1EcmpCount == 0)
                        {
                            /* Best route is not an ecmp route so adding the route to PRT list */
                            Rtm6AddRtToPRTInCxt (pRtm6Cxt, pBestRt);
                        }
                    }
                }
            }

            else
            {
                /*handle non best route deletion if it is ECMP */
                if (pDelIp6RtEntry->u4Flag & RTM6_ECMP_RT
                    || (i4ReachCount == TRUE))
                {
                    pDelIp6RtEntry->u1EcmpCount = u1ReachCountstatus;
                    RTM6_COPY_ROUTE_INFO_TO_UPDATE_MSG (pDelIp6RtEntry,
                                                        NetIpv6RtInfo);
                    NetIpv6RtInfo.u4ContextId = pRtm6Cxt->u4ContextId;
                    NetIpv6RtInfo.u2ChgBit = IP6_BIT_STATUS;
                    NetIpv6RtInfo.u4RowStatus = IP6FWD_DESTROY;
                    NetIpv6InvokeRouteChange (&NetIpv6RtInfo,
                                              NETIPV6_ALL_PROTO);
                    Rtm6ApiAddOrDelEcmpRtsInCxt (pRtm6Cxt->u4ContextId,
                                                 pDelIp6RtEntry, pBestRt,
                                                 IP6_ROUTE_DEL);

                    pTmpIp6RtEntry = pBestRt->pNextAlternatepath;
                    u2BestRtCount = 0;
                    while ((pTmpIp6RtEntry != NULL) &&
                           (pTmpIp6RtEntry->u4Metric == pBestRt->u4Metric) &&
                           (pTmpIp6RtEntry->u4RowStatus == IP6FWD_ACTIVE))
                    {
                        u2BestRtCount++;
                        pTmpIp6RtEntry = pTmpIp6RtEntry->pNextAlternatepath;
                    }
                    if (u2BestRtCount == 0)
                    {

                        pBestRt->u4Flag &= (UINT4) (~RTM6_ECMP_RT);
                        Rtm6SetReachStateForRoute (pRtm6Cxt->u4ContextId,
                                                   pBestRt);
                        Rtm6GetInstalledRouteCount (pBestRt,
                                                    &pBestRt->u1EcmpCount);
                        if (pBestRt->u1EcmpCount == 0)
                        {
                            /* Now the best route changed to single best route, so adding the route
                             * to PRT list*/
                            Rtm6AddRtToPRTInCxt (pRtm6Cxt, pBestRt);
                        }
                    }
                }
            }
        }
    }
    else
    {

        i4OutCome = Rtm6TrieDeleteEntryInCxt (pRtm6Cxt, pDelIp6RtEntry);
        if (i4OutCome == RTM6_SUCCESS)
        {
            if (pIp6Route->i1Proto != IP6_LOCAL_PROTOID)
            {
                Rtm6DeleteNextHopforRoute (pRtm6Cxt, pIp6Route);
            }

            pRtm6Cxt->u4Ip6FwdTblRouteNum--;
        }
    }

    /* Also free the route entry structure stored in the TRIE. Ensure that this
     * route is removed from all other associate data structure before this
     * point. */
    Rtm6DeleteRtFromPRTInCxt (pRtm6Cxt, pDelIp6RtEntry);
    IP6_RT_FREE (pDelIp6RtEntry);

    return i4OutCome;
}

/*****************************************************************************
 * Function           : Ip6GetFwdRouteEntryInCxt                                 
 * Input(s)          :  pRtm6Cxt - Rtm6 Context Pointer
 *                   :  pDest - The destination address for which the         
 *                              route has to be found                         
 *                      u1PrefixLen - The prefix Length of the route         
 *                                    to be found                            
 * Output(s)          : Pointer to Best Route Entry (ppOutIp6RtInfo)         
 * Returns            : RTM6_SUCCESS / RTM6_FAILURE                            
 * Action :                                                                  
 *   Fetches the best Route Entry matching (longest prefix match) for the
 *   given IpAddress & prefix Len.      
 ****************************************************************************
*/

INT4
Ip6GetFwdRouteEntryInCxt (tRtm6Cxt * pRtm6Cxt,
                          tIp6Addr * pDest, UINT1 u1PrefixLen,
                          tIp6RtEntry ** ppIp6OutRtInfo)
{
    tIp6RtEntry         Ip6RtEntry;
    tIp6RtEntry        *pIp6RtEntry = NULL;
    tIp6RtEntry        *pIp6BestRtEntry = NULL;
    VOID               *pRibNode = NULL;
    INT4                i4OutCome = RTM6_FAILURE;
    INT4                i4RetVal = RTM6_FAILURE;

    MEMSET (&Ip6RtEntry, 0, sizeof (tIp6RtEntry));

    MEMCPY (&Ip6RtEntry.dst, pDest, IP6_ADDR_SIZE);
    Ip6RtEntry.u1Prefixlen = u1PrefixLen;
    Ip6RtEntry.i1Proto = ALL_PROTO_ID + 1;

    i4OutCome = Rtm6TrieLookupEntryInCxt (pRtm6Cxt, &Ip6RtEntry,
                                          &pIp6RtEntry, &pRibNode);
    if (i4OutCome == RTM6_SUCCESS)
    {
        /* Get the Best route. */
        if (pIp6RtEntry->u1AddrType == ADDR6_ANYCAST)
        {
            i4RetVal =
                Rtm6UtilFwdTblGetBestAnycastRouteInCxt (pRtm6Cxt, pIp6RtEntry,
                                                        &pIp6BestRtEntry);
        }
        else
        {
            i4RetVal =
                Rtm6UtilFwdTblGetBestRouteInCxt (pRtm6Cxt, pIp6RtEntry,
                                                 &pIp6BestRtEntry);
        }
        if (i4RetVal == IP6_SUCCESS)
        {
            *ppIp6OutRtInfo = pIp6BestRtEntry;
            return RTM6_SUCCESS;
        }
    }

    *ppIp6OutRtInfo = NULL;
    return RTM6_FAILURE;
}

/*****************************************************************************
 * Function           : Ip6GetBestRouteEntryInCxt                                 
 * Input(s)          :  pRtm6Cxt - Rtm6 Context Pointer
 *                   :  pDest - The destination address for which the         
 *                              route has to be found                         
 *                      u1PrefixLen - The prefix Length of the route         
 *                                    to be found                            
 * Output(s)          : Pointer to Best Exact Route Entry (ppOutIp6RtInfo)         
 * Returns            : RTM6_SUCCESS / RTM6_FAILURE                            
 * Action :                                                                  
 *   Fetches the best Route Entry matching exactly the given IpAddress &
 *   prefix Len.      
 ****************************************************************************
*/

INT4
Ip6GetBestRouteEntryInCxt (tRtm6Cxt * pRtm6Cxt,
                           tIp6Addr * pDest, UINT1 u1PrefixLen,
                           tIp6RtEntry ** ppIp6OutRtInfo)
{
    tIp6RtEntry         Ip6RtEntry;
    tIp6RtEntry        *pIp6RtEntry = NULL;
    tIp6RtEntry        *pIp6BestRtEntry = NULL;
    VOID               *pRibNode = NULL;
    INT4                i4OutCome = RTM6_FAILURE;

    MEMSET (&Ip6RtEntry, 0, sizeof (tIp6RtEntry));

    MEMCPY (&Ip6RtEntry.dst, pDest, IP6_ADDR_SIZE);
    Ip6RtEntry.u1Prefixlen = u1PrefixLen;
    Ip6RtEntry.i1Proto = ALL_PROTO_ID + 1;

    i4OutCome = Rtm6TrieSearchExactEntryInCxt (pRtm6Cxt, &Ip6RtEntry,
                                               &pIp6RtEntry, &pRibNode);
    if (i4OutCome == RTM6_SUCCESS)
    {
        /* Get the Best route. */
        if (pIp6RtEntry->u1AddrType == ADDR6_ANYCAST)
        {
            if (Rtm6UtilFwdTblGetBestAnycastRouteInCxt (pRtm6Cxt, pIp6RtEntry,
                                                        &pIp6BestRtEntry) ==
                IP6_SUCCESS)
            {
                *ppIp6OutRtInfo = pIp6BestRtEntry;
                return RTM6_SUCCESS;
            }

        }
        else
        {
            if (Rtm6UtilFwdTblGetBestRouteInCxt (pRtm6Cxt, pIp6RtEntry,
                                                 &pIp6BestRtEntry) ==
                IP6_SUCCESS)
            {
                *ppIp6OutRtInfo = pIp6BestRtEntry;
                return RTM6_SUCCESS;
            }
        }
    }

    *ppIp6OutRtInfo = NULL;
    return RTM6_FAILURE;
}

/*****************************************************************************
 * Function           : Ip6GetNextBestRouteEntryInCxt                              
 * Input(s)          :  pRtm6Cxt - Rtm6 Context Pointer
 *                   :  pDest - The destination address for which the next    
 *                             route has to be found                         
 *                      u1PrefixLen - The prefix Length of the route         
 * Output(s)          : Pointer to Next Route Entry (ppOutIp6RtInfo)         
 * Returns            : RTM6_SUCCESS / RTM6_FAILURE                            
 * Action :                                                                  
 *   Fetches the Next Best Route Entry For the given IpAddress & PrefixLen.  
 ****************************************************************************
*/

INT4
Ip6GetNextBestRouteEntryInCxt (tRtm6Cxt * pRtm6Cxt,
                               tIp6Addr * pDest, UINT1 u1PrefixLen,
                               tIp6RtEntry ** ppIp6OutRtInfo)
{
    tIp6RtEntry         Ip6RtEntry;
    tIp6RtEntry        *pIp6RtEntry = NULL;
    tIp6RtEntry        *pIp6BestRtEntry = NULL;
    VOID               *pRibNode = NULL;
    INT4                i4OutCome = RTM6_SUCCESS;

    MEMSET (&Ip6RtEntry, 0, sizeof (tIp6RtEntry));

    MEMCPY (&Ip6RtEntry.dst, pDest, IP6_ADDR_SIZE);
    Ip6RtEntry.u1Prefixlen = u1PrefixLen;
    Ip6RtEntry.i1Proto = ALL_PROTO_ID + 1;

    i4OutCome = Rtm6TrieGetNextEntryInCxt (pRtm6Cxt, &Ip6RtEntry,
                                           &pIp6RtEntry, &pRibNode);
    while (i4OutCome == RTM6_SUCCESS)
    {
        /* Get the Best route. */
        if (pIp6RtEntry->u1AddrType == ADDR6_ANYCAST)
        {
            if (Rtm6UtilFwdTblGetBestAnycastRouteInCxt (pRtm6Cxt, pIp6RtEntry,
                                                        &pIp6BestRtEntry) ==
                IP6_SUCCESS)
            {
                *ppIp6OutRtInfo = pIp6BestRtEntry;
                return RTM6_SUCCESS;
            }
        }
        else
        {
            if (Rtm6UtilFwdTblGetBestRouteInCxt (pRtm6Cxt, pIp6RtEntry,
                                                 &pIp6BestRtEntry) ==
                IP6_SUCCESS)
            {
                *ppIp6OutRtInfo = pIp6BestRtEntry;
                return RTM6_SUCCESS;
            }
        }

        /* No best route for this entry. Try and get the next best route. */
        MEMCPY (&Ip6RtEntry.dst, &pIp6RtEntry->dst, IP6_ADDR_SIZE);
        Ip6RtEntry.u1Prefixlen = pIp6RtEntry->u1Prefixlen;
        Ip6RtEntry.i1Proto = ALL_PROTO_ID + 1;
        pIp6RtEntry = NULL;
        i4OutCome = Rtm6TrieGetNextEntryInCxt (pRtm6Cxt, &Ip6RtEntry,
                                               &pIp6RtEntry, &pRibNode);
    }

    *ppIp6OutRtInfo = NULL;
    return RTM6_FAILURE;
}

/*
 ****************************************************************************
 * Function           : Ip6ScanRouteTableForBestRouteInCxt
 * Input(s)          :  pRtm6Cxt - Rtm6 Context Pointer
 *                    : pIp6InAddr   - Information about the IP6 address
 *                                     used for scanning the Route Table
 *                      u1InAddrPrefixLen - Prefix Len of the input address
 *                      pAppSpecFunc - Call back function for processing
 *                                     Application specific information
 *                      u4MaxRouteCnt- Number of route entries to be scanned.
 *                                     If Set as 0, then need to scan the
 *                                     entire Routing Table.
 * Output(s)          : pIp6OutAddr  - Updated Information about the next
 *                                     route to be scanned.
 *                      pu1OutAddrPrefixLen - Prefix length of the next route
 * Returns            : None
 * Action             : This function scans throught the Route Table for the
 *                      given number of route and the callback function is
 *                      called for each route entry. If the maximum entries
 *                      has been processed, the next route entry to be
 *                      processed is stored in the pOutParams structure.
 ****************************************************************************
*/
VOID
Ip6ScanRouteTableForBestRouteInCxt (tRtm6Cxt * pRtm6Cxt,
                                    tIp6Addr * pIp6InAddr,
                                    UINT1 u1InAddrPrefixLen,
                                    INT4 (*pAppSpecFunc) (tIp6RtEntry *
                                                          pIp6RtEntry,
                                                          VOID *pAppSpecData),
                                    UINT4 u4MaxRouteCnt, tIp6Addr * pIp6OutAddr,
                                    UINT1 *pu1OutAddrPrefixLen,
                                    VOID *pAppSpecData)
{
    tIp6RtEntry         InIp6RtEntry;
    tIp6RtEntry        *pIp6RtEntry = NULL;
    tIp6RtEntry        *pIp6BestRtEntry = NULL;
    tIp6Addr            Ip6Addr;
    VOID               *pRibNode = NULL;
    UINT4               u4RtCnt = 0;
    INT4                i4OutCome = 0;
    UINT1               u1CheckRtCnt = TRUE;

    MEMSET ((UINT1 *) &InIp6RtEntry, 0, sizeof (tIp6RtEntry));
    MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));

    if (u4MaxRouteCnt == 0)
    {
        u1CheckRtCnt = FALSE;
    }
    else
    {
        u1CheckRtCnt = TRUE;
    }

    /* Fill the Input route. */
    MEMCPY (&InIp6RtEntry.dst, pIp6InAddr, IP6_ADDR_SIZE);
    InIp6RtEntry.u1Prefixlen = u1InAddrPrefixLen;
    InIp6RtEntry.i1Proto = ALL_PROTO_ID + 1;

    i4OutCome = Rtm6TrieSearchExactEntryInCxt (pRtm6Cxt,
                                               &InIp6RtEntry, &pIp6RtEntry,
                                               &pRibNode);
    if (i4OutCome == RTM6_FAILURE)
    {
        if (MEMCMP (&Ip6Addr, &InIp6RtEntry.dst, IP6_ADDR_SIZE) == 0)
        {
            /* Get the first route. */
            i4OutCome = Rtm6TrieGetFirstEntryInCxt (pRtm6Cxt,
                                                    &pIp6RtEntry, &pRibNode);
        }
        else
        {
            /* First entry is not present. Try and get the next entry if
             * present. */
            i4OutCome = Rtm6TrieGetNextEntryInCxt (pRtm6Cxt,
                                                   &InIp6RtEntry, &pIp6RtEntry,
                                                   &pRibNode);
        }
    }

    while (i4OutCome == RTM6_SUCCESS)
    {
        pIp6BestRtEntry = NULL;
        if (Rtm6UtilFwdTblGetBestRouteInCxt (pRtm6Cxt,
                                             pIp6RtEntry,
                                             &pIp6BestRtEntry) == IP6_SUCCESS)
        {
            u4RtCnt++;
            /* Call back routine */
            pAppSpecFunc (pIp6BestRtEntry, pAppSpecData);
        }

        /* update the index here */
        MEMCPY (&InIp6RtEntry.dst, &pIp6RtEntry->dst, IP6_ADDR_SIZE);
        InIp6RtEntry.u1Prefixlen = pIp6RtEntry->u1Prefixlen;
        pIp6RtEntry = NULL;

        /* this operation should be stopped when get next fails as
         * get next failure represents normal termination.
         */
        i4OutCome = Rtm6TrieGetNextEntryInCxt (pRtm6Cxt,
                                               &InIp6RtEntry, &pIp6RtEntry,
                                               &pRibNode);
        if (i4OutCome == RTM6_FAILURE)
        {
            /* No more route exist. */
            break;
        }

        /* If we have finished processing more than expected number of route
         * entries then return. */
        if ((u1CheckRtCnt == TRUE) && (u4RtCnt > u4MaxRouteCnt))
        {
            MEMCPY (pIp6OutAddr, &pIp6RtEntry->dst, IP6_ADDR_SIZE);
            *pu1OutAddrPrefixLen = pIp6RtEntry->u1Prefixlen;
            return;
        }
    }

    /* No more route is present in the Route Table. Update the Output Prefix
     * to reflect this and return. */
    MEMSET (pIp6OutAddr, 0, IP6_ADDR_SIZE);
    *pu1OutAddrPrefixLen = 0;
    return;
}

/*
 ****************************************************************************
 * Function           : Ip6TrieScan
 * Input(s)           : pInputParams -  Information about the Input parameter
 *                                      used for scanning the Route Table
 *                      pAppSpecFunc -  Call back function for processing
 *                                      Application specific information
 * Output(s)          : pScanOutParams - Updated Information about the next
 *                                       route to be scanned.
 * Returns            : RTM6_SUCCESS/RTM6_FAILURE
 * Action             : This function scans throught the entire Route Table
 *                      and the callback function is called for each route
 *                      entry.
 ****************************************************************************
*/
INT4
Ip6TrieScan (tInputParams * pInputParams,
             INT4 (*pAppSpecScanFunc) (tRtm6TrieScanOutParams *),
             tRtm6TrieScanOutParams * pOutParams)
{
    tIp6RtEntry         InIp6RtEntry;
    tIp6RtEntry        *pIp6RtEntry = NULL;
    tIp6RtEntry        *pNextIp6RtEntry = NULL;
    tRtm6Cxt           *pRtm6Cxt = NULL;
    VOID               *pRibNode = NULL;
    INT4                i4OutCome = 0;

    UNUSED_PARAM (pInputParams);

    pRtm6Cxt = UtilRtm6GetCxt (pOutParams->u4ContextId);

    if (pRtm6Cxt == NULL)
    {
        return RTM6_FAILURE;
    }

    MEMSET ((UINT1 *) &InIp6RtEntry, 0, sizeof (tIp6RtEntry));

    /* Fill the Input route. */
    MEMSET (&InIp6RtEntry.dst, 0, IP6_ADDR_SIZE);
    InIp6RtEntry.u1Prefixlen = 0;
    InIp6RtEntry.i1Proto = ALL_PROTO_ID + 1;

    /* Scan through the entire TRIE */
    i4OutCome = Rtm6TrieGetFirstEntryInCxt (pRtm6Cxt, &pIp6RtEntry, &pRibNode);

    while (i4OutCome == RTM6_SUCCESS)
    {
        /* update to find next index here */
        MEMCPY (&InIp6RtEntry.dst, &pIp6RtEntry->dst, IP6_ADDR_SIZE);
        InIp6RtEntry.u1Prefixlen = pIp6RtEntry->u1Prefixlen;

        /* Get the next entry. */
        i4OutCome = Rtm6TrieGetNextEntryInCxt (pRtm6Cxt,
                                               &InIp6RtEntry, &pNextIp6RtEntry,
                                               &pRibNode);

        /* Call back routine for processing the current route. */
        pOutParams->pRtEntry = pIp6RtEntry;
        pAppSpecScanFunc (pOutParams);

        /* this operation should be stopped when get next fails as
         * get next failure represents normal termination.
         */
        if (i4OutCome == RTM6_FAILURE)
        {
            /* No more route exist. */
            break;
        }

        /* Process the next route. */
        pIp6RtEntry = pNextIp6RtEntry;
        pNextIp6RtEntry = NULL;
    }

    return RTM6_SUCCESS;
}

/**************************************************************************/
/*   Function Name   : Rtm6InitPreferenceInCxt                            */
/*   Description     : This function initializes the Protocol Preference  */
/*   Input(s)        : pRtm6Cxt -RtmContext pointer                        */
/*   Output(s)       : None                                               */
/*   Return Value    : None                                               */
/**************************************************************************/
VOID
Rtm6InitPreferenceInCxt (tRtm6Cxt * pRtm6Cxt)
{
    /* Initialize Protocol Preference */
    pRtm6Cxt->au4RtPref[IP6_OTHER_PROTOID - 1] = IP6_PREFERENCE_OTHERS;
    pRtm6Cxt->au4RtPref[IP6_LOCAL_PROTOID - 1] = IP6_PREFERENCE_LOCAL;
    pRtm6Cxt->au4RtPref[IP6_NETMGMT_PROTOID - 1] = IP6_PREFERENCE_NETMGMT;
    pRtm6Cxt->au4RtPref[IP6_NDISC_PROTOID - 1] = IP6_PREFERENCE_NDISC;
    pRtm6Cxt->au4RtPref[IP6_RIP_PROTOID - 1] = IP6_PREFERENCE_RIP;
    pRtm6Cxt->au4RtPref[IP6_OSPF_PROTOID - 1] = IP6_PREFERENCE_OSPF;
    pRtm6Cxt->au4RtPref[IP6_BGP_PROTOID - 1] = IP6_PREFERENCE_BGP;
    pRtm6Cxt->au4RtPref[IP6_IDRP_PROTOID - 1] = IP6_PREFERENCE_IDRP;
    pRtm6Cxt->au4RtPref[IP6_IGRP_PROTOID - 1] = IP6_PREFERENCE_IGRP;
}

/**************************************************************************/
/*   Function Name   : Rtm6GRStaleRtMark                                  */
/*   Description     : This function will mark the stale routes           */
/*                                  during GR process.                       */
/*   Input(s)        : pRegnID - Protocol Info whose routes needs to be   */
/*                     marked as stale routes                             */
/*                     (IP_SUCCESS/IP_FAILURE)                            */
/*   Output(s)       : None                                               */
/*   Return Value    : None                                               */
/**************************************************************************/

VOID
Rtm6GRStaleRtMark (tRtm6RegnId * pRegnId)
{
    tIp6RtEntry        *pIp6RtEntry = NULL;
    tIp6RtEntry        *pTmpRtEntry = NULL;
    tIp6RtEntry        *pNxtRtEntry = NULL;
    VOID               *pRibNode = NULL;
    tIp6Addr            AddrMask;
    tInputParams        InParams;
    INT4                i4Status;
    UINT2               u2RegnId;
    tRtm6Cxt           *pRtm6Cxt = NULL;
    UINT1               au1TrieKey[RTM6_MAX_TRIE_KEY_LEN];

    pRtm6Cxt = UtilRtm6GetCxt (pRegnId->u4ContextId);
    if (pRtm6Cxt == NULL)
    {
        return;
    }

    MEMSET ((UINT1 *) &AddrMask, 0xFF, sizeof (tIp6Addr));

    u2RegnId = Rtm6GetRegnIndexFromRegnIdInCxt (pRtm6Cxt, pRegnId);
    if (u2RegnId == RTM6_INVALID_REGN_ID)
    {
        return;
    }
    InParams.pRoot = pRtm6Cxt->pIp6RtTblRoot;
    InParams.i1AppId = pRegnId->u2ProtoId;
    InParams.pLeafNode = NULL;
    InParams.Key.pKey = au1TrieKey;
    MEMSET (InParams.Key.pKey, 0, RTM6_MAX_TRIE_KEY_LEN);

    i4Status = TrieGetFirstNode (&InParams, (VOID *) &pIp6RtEntry, &pRibNode);
    if (i4Status == TRIE_FAILURE)
    {
        return;
    }

    while (i4Status != TRIE_FAILURE)
    {
        InParams.pRoot = pRtm6Cxt->pIp6RtTblRoot;
        InParams.i1AppId = pRegnId->u2ProtoId;
        InParams.pLeafNode = NULL;
        Ip6CopyAddrBits ((tIp6Addr *) (VOID *) InParams.Key.pKey,
                         &pIp6RtEntry->dst, pIp6RtEntry->u1Prefixlen);
        Ip6CopyAddrBits ((tIp6Addr *) (VOID *)
                         (InParams.Key.pKey + IP6_ADDR_SIZE),
                         &AddrMask, pIp6RtEntry->u1Prefixlen);
        InParams.u1PrefixLen = (UINT1) pIp6RtEntry->u1Prefixlen;

        if (pIp6RtEntry->i1Proto == (UINT1) pRegnId->u2ProtoId)
        {
            /* GR process completed. If the GR bit is set ,reset it on successful
               GR completion , else remove the 
               route entry as it is no longer valid. */

            /* Find the number of alternate paths */
            pNxtRtEntry = pIp6RtEntry;
            while (pNxtRtEntry != NULL)
            {
                pTmpRtEntry = pNxtRtEntry;
                pNxtRtEntry = pNxtRtEntry->pNextAlternatepath;
                pTmpRtEntry->u1Flag |= RTM6_GR_RT;
                Rtm6TrieUpdateEntryInCxt (pRtm6Cxt, pTmpRtEntry,
                                          pTmpRtEntry->u4Metric);
            }
        }

        i4Status = TrieGetNextNode (&InParams, pRibNode, (VOID *) &pIp6RtEntry,
                                    &pRibNode);
    }
    return;
}

/**************************************************************************/
/*   Function Name   : Rtm6TrieProcessGRRouteCleanUp                      */
/*   Description     : This function will cleanup all the old routes      */
/*                     updated before graceful restart, and resets the    */
/*                     IP6_GR_BIT updated during GR process.              */
/*   Input(s)        : pRegnID - Protocol Info whose routes needs to be   */
/*                     cleared                                            */
/*                     i1GRCompFlag - GR Process completion Flag          */
/*                     (IP6_SUCCESS/IP6_FAILURE). This flag is used to    */
/*                     distinguish Timer Expiry and successful GR         */
/*                     completion of protocol                             */
/*   Output(s)       : None                                               */
/*   Return Value    : None                                               */
/**************************************************************************/

VOID
Rtm6TrieProcessGRRouteCleanUp (tRtm6RegnId * pRegnId, INT1 i1GRCompFlag)
{
    tIp6RtEntry        *pIp6RtEntry = NULL;
    tIp6RtEntry        *pTmpRtEntry = NULL;
    tIp6RtEntry        *pNxtRtEntry = NULL;
    VOID               *pRibNode = NULL;
    tIp6Addr            AddrMask;
    tInputParams        InParams;
    INT4                i4Status;
    UINT2               u2RegnId;
    tRtm6Cxt           *pRtm6Cxt = NULL;
    UINT1               au1TrieKey[RTM6_MAX_TRIE_KEY_LEN];

    pRtm6Cxt = UtilRtm6GetCxt (pRegnId->u4ContextId);
    if (pRtm6Cxt == NULL)
    {
        return;
    }

    MEMSET ((UINT1 *) &AddrMask, 0xFF, sizeof (tIp6Addr));

    u2RegnId = Rtm6GetRegnIndexFromRegnIdInCxt (pRtm6Cxt, pRegnId);
    if (u2RegnId == RTM6_INVALID_REGN_ID)
    {
        return;
    }
    Rtm6StopGRTmr (pRegnId);
    InParams.pRoot = pRtm6Cxt->pIp6RtTblRoot;
    InParams.i1AppId = pRegnId->u2ProtoId;
    InParams.pLeafNode = NULL;
    InParams.Key.pKey = au1TrieKey;
    MEMSET (InParams.Key.pKey, 0, RTM6_MAX_TRIE_KEY_LEN);

    i4Status = TrieGetFirstNode (&InParams, (VOID *) &pIp6RtEntry, &pRibNode);
    if (i4Status == TRIE_FAILURE)
    {
        pRtm6Cxt->aRtm6RegnTable[u2RegnId].u1RestartState = RTM6_IGP_CONVERGED;
        return;
    }

    while (i4Status != TRIE_FAILURE)
    {
        InParams.pRoot = pRtm6Cxt->pIp6RtTblRoot;
        InParams.i1AppId = pRegnId->u2ProtoId;
        InParams.pLeafNode = NULL;
        Ip6CopyAddrBits ((tIp6Addr *) (VOID *) InParams.Key.pKey,
                         &pIp6RtEntry->dst, pIp6RtEntry->u1Prefixlen);
        Ip6CopyAddrBits ((tIp6Addr *) (VOID *) (InParams.Key.pKey +
                                                IP6_ADDR_SIZE),
                         &AddrMask, pIp6RtEntry->u1Prefixlen);
        InParams.u1PrefixLen = (UINT1) pIp6RtEntry->u1Prefixlen;

        if (pIp6RtEntry->i1Proto == (UINT1) pRegnId->u2ProtoId)
        {
            /* GR process completed. If the GR bit is set ,reset it on successful
               GR completion , else remove the 
               route entry as it is no longer valid. */

            /* Find the number of alternate paths */
            pNxtRtEntry = pIp6RtEntry;
            while (pNxtRtEntry != NULL)
            {
                pTmpRtEntry = pNxtRtEntry;
                /* Existing route entry gets deleted(pIp6RtEntry),
                 * if the GR bit is not set. Hence store the next 
                 * alternate path route information pointer in pNextRt */
                pNxtRtEntry = pNxtRtEntry->pNextAlternatepath;

                /* For OSPF routes, RTM_GR_RT flag will be set for routes added 
                 * during graceful restart. If a route is present without this
                 * flag during GR RouteCleanup, then it is a stale route and it
                 * should be removed.
                 *
                 * For BGP/RIP routes, RTM_GR_RT flag will be set for all routes 
                 * before graceful shutdown or after transitioning from Standby 
                 * to Active state. During restart, routes will be updated with
                 * the Flag reset. During GR RouteCleanup, if a route is present 
                 * with this flag, then this is a stale route and the route
                 * should be deleted.
                 */

                if ((pTmpRtEntry->u1Flag & RTM6_GR_RT)
                    && (i1GRCompFlag == IP6_SUCCESS))
                {
                    if ((pTmpRtEntry->i1Proto == BGP6_ID) ||
                        (pTmpRtEntry->i1Proto == RIPNG_ID))
                    {
                        /* Delete the remaining stale routes that 
                         * belong to BGP during successful exit*/
                        Ip6ForwardingTableDeleteRouteInCxt (pRtm6Cxt,
                                                            pTmpRtEntry);
                    }
                    else
                    {
                        pTmpRtEntry->u1Flag &= ~RTM6_GR_RT;
                    }
                }
                else
                {
                    /* Delete all the stale routes from RTM 
                     * only in failure case */

                    if (((pTmpRtEntry->i1Proto != BGP6_ID) &&
                         (pTmpRtEntry->i1Proto != RIPNG_ID)) ||
                        ((i1GRCompFlag == IP6_FAILURE) &&
                         ((pTmpRtEntry->u1Flag) & RTM6_GR_RT)))
                    {
                        Ip6ForwardingTableDeleteRouteInCxt (pRtm6Cxt,
                                                            pTmpRtEntry);
                    }
                }
            }
        }

        i4Status = TrieGetNextNode (&InParams, pRibNode, (VOID *) &pIp6RtEntry,
                                    &pRibNode);
    }
    pRtm6Cxt->aRtm6RegnTable[u2RegnId].u1RestartState = RTM6_IGP_CONVERGED;
    return;
}

/**************************************************************************/
/*   Function Name   : Rtm6IsForwPlanPreserved                            */
/*   Description     : This function will query the RTM6 and check whether */
/*                        mentioned Protocol routes are available or not  */
/*   Input(s)        : pRegnID - Protocol Info whose routes needs to be   */
/*                     checked                                            */
/*   Output(s)       : None                                               */
/*   Return Value    : ISIS_TRUE -Success                                 */
/*                     ISIS_FALSE - Failure                               */
/**************************************************************************/

INT4
Rtm6IsForwPlanPreserved (tRtm6RegnId * pRegnId)
{

    tIp6RtEntry        *ppRtEntry = NULL;
    VOID               *ppRibNode = NULL;
    tInputParams        InParams;
    UINT2               u2RegnId = 0;
    INT4                i4Status = 0;
    tRtm6Cxt           *pRtm6Cxt = NULL;
    UINT1               au1TrieKey[RTM6_MAX_TRIE_KEY_LEN];

    pRtm6Cxt = UtilRtm6GetCxt (pRegnId->u4ContextId);
    if (pRtm6Cxt == NULL)
    {
        return IP6_FAILURE;
    }

    u2RegnId = Rtm6GetRegnIndexFromRegnIdInCxt (pRtm6Cxt, pRegnId);
    if (u2RegnId == RTM6_INVALID_REGN_ID)
    {
        return IP6_FAILURE;
    }

    InParams.pRoot = pRtm6Cxt->pIp6RtTblRoot;
    InParams.i1AppId = (INT1) u2RegnId;
    InParams.pLeafNode = NULL;
    InParams.Key.pKey = au1TrieKey;
    MEMSET (InParams.Key.pKey, 0, RTM6_MAX_TRIE_KEY_LEN);

    i4Status = TrieGetFirstNode (&InParams, (VOID *) &ppRtEntry, &ppRibNode);
    if (i4Status == TRIE_FAILURE)
    {
        return IP6_FAILURE;
    }
    return IP6_SUCCESS;

}

/*****************************************************************************/
/* Function           : Rtm6DelAllECMP6RtInCxt                               */
/* Action             : This function will delete all routes with are  ECMP*/
/*                      for a given metric                                                     */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Returns            : None                                                 */
/*                                                                           */
/*************************************************************************** */
VOID
Rtm6DelAllECMP6RtInCxt (tRtm6Cxt * pRtm6Cxt, tIp6RtEntry * pBestRt,
                        UINT4 u4Metric)
{
    tIp6RtEntry        *pTmpRtEntry = NULL;

    pTmpRtEntry = pBestRt->pNextAlternatepath;
    while (pTmpRtEntry != NULL)
    {
        if ((pTmpRtEntry->u4Metric == u4Metric) &&
            (pTmpRtEntry->u4RowStatus == IPFWD_ACTIVE))
        {
            /*Need to remove from ECMP6 PRT */
            /* Function that delete the route from NP and kernal */
            Rtm6ApiAddOrDelEcmpRtsInCxt (pRtm6Cxt->u4ContextId, pTmpRtEntry,
                                         pBestRt, IP6_ROUTE_DEL);
            pTmpRtEntry->u4Flag &= ~(UINT4) RTM6_ECMP_RT;
        }
        pTmpRtEntry = pTmpRtEntry->pNextAlternatepath;
    }
    /*Remove Best route from PRT */

    /*Rtm6DeleteRtFromECMP6PRT(pRtm6Cxt, pBestRt); */

    return;
}

/*****************************************************************************/
/* Function           : Rtm6AddAllECMP6RtInCxt                               */
/* Action             : This function will install all ECMP routes for give best route*/
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Returns            : None                                                 */
/*                                                                           */
/*************************************************************************** */

VOID
Rtm6AddAllECMP6RtInCxt (tRtm6Cxt * pRtm6Cxt, tIp6RtEntry * pBestRt)
{
    tIp6RtEntry        *pTmpIp6RtEntry = NULL;
    UINT2               u2BestRtCount = 0;
    UINT1               u1SetEcmp = FALSE;

    pTmpIp6RtEntry = pBestRt;
    while ((pTmpIp6RtEntry != NULL) &&
           (pTmpIp6RtEntry->u4Metric == pBestRt->u4Metric))
    {
        if (pTmpIp6RtEntry->u4RowStatus != IP6FWD_ACTIVE)
        {
            pTmpIp6RtEntry = pTmpIp6RtEntry->pNextAlternatepath;
            continue;
        }

        if ((pTmpIp6RtEntry->i1Proto == STATIC_ID)
            && (pBestRt->i1Proto == STATIC_ID))
        {
            if (pTmpIp6RtEntry->u1Preference == pBestRt->u1Preference)
            {
                u1SetEcmp = TRUE;
            }
        }
        else if (((pTmpIp6RtEntry->i1Proto == ISIS_ID)
                  && (pBestRt->i1Proto == ISIS_ID)
                  && (pTmpIp6RtEntry->u1MetricType == pBestRt->u1MetricType))
                 || ((pTmpIp6RtEntry->i1Proto != ISIS_ID)
                     && (pBestRt->i1Proto != ISIS_ID)))
        {
            u1SetEcmp = TRUE;
        }

        if (u1SetEcmp == TRUE)
        {
            u1SetEcmp = FALSE;
            if (u2BestRtCount >= 1)
            {
                pTmpIp6RtEntry->u4Flag |= RTM6_ECMP_RT;
                Rtm6ApiAddOrDelEcmpRtsInCxt (pRtm6Cxt->u4ContextId,
                                             pTmpIp6RtEntry, pBestRt,
                                             IP6_ROUTE_ADD);
            }
            u2BestRtCount++;
        }

        pTmpIp6RtEntry = pTmpIp6RtEntry->pNextAlternatepath;
    }
    if (u2BestRtCount > 1)
    {
        pBestRt->u4Flag |= RTM6_ECMP_RT;

    }
    else
    {
        pBestRt->u4Flag &= ~(UINT4) RTM6_ECMP_RT;
    }

    return;
}

/*****************************************************************************
 * Function Name    :   Rtm6NotifyNewECMP6RouteInCxt
 *
 * Description      :   This function checks whether the  best route for the destnation
 *                      is already notified or not
 *
 * Inputs           :   pRt - Start pointer of ECMP Lists
 *                      i4Metric - Metric to be refered for ECMP List
 *                      pRtm6Cxt - RTM Context Pointer
 *
 * Return Value     :   TRUE - if the notified route is present
 *                      FALSE - if the notified route is not present in ECMP List
 *****************************************************************************/
VOID
Rtm6NotifyNewECMP6RouteInCxt (tRtm6Cxt * pRtm6Cxt, tIp6RtEntry * pRt,
                              INT4 i4Metric)
{
    UNUSED_PARAM (pRtm6Cxt);
    UNUSED_PARAM (pRt);
    UNUSED_PARAM (i4Metric);
    /*To be discussed */
}

 /*****************************************************************************
 * Function Name    :   Rtm6GetBestRouteCount
 *
 * Description      :   Provides the number of best routes with the given metric
 *                      in the provided list
 *
 * Inputs           :   pRt - Start pointer of ECMP Lists
 *                      i4Metric - Metric to be refered for ECMP List
 *
 * Return Value     :  pRtCount : No of best routes in the ECMP List
 *****************************************************************************/
VOID
Rtm6GetBestRouteCount (tIp6RtEntry * pRt, UINT4 u4Metric, UINT2 *pRtCount)
{
    UINT1               u1ECMPRtCount = 0;
    tIp6RtEntry        *pTmpRt = NULL;

    pTmpRt = pRt;
    while ((pTmpRt != NULL) && (pTmpRt->u4Metric == u4Metric))
    {
        u1ECMPRtCount++;
        pTmpRt = pTmpRt->pNextAlternatepath;
    }
    *pRtCount = u1ECMPRtCount;
}

 /*****************************************************************************
 * Function Name    :   Rtm6GetInstalledRouteCount
 *
 * Description      :   Provides the number of routes installed in the hardware with 
 *            the given route metric for the given destination
 *
 * Inputs           :   pIp6Route - Start pointer of ECMP6 Lists
 *
 * Return Value     :  No. of Resolved Best Route counts installed in the hw
 *****************************************************************************/
VOID
Rtm6GetInstalledRouteCount (tIp6RtEntry * pIp6Route, UINT1 *pu1RtCount)
{
    tIp6RtEntry        *pTmp6Rt = NULL;
    UINT1               u1RtCount = 0;

    pTmp6Rt = pIp6Route;
    while ((pTmp6Rt != NULL) && (pTmp6Rt->u4Metric == pIp6Route->u4Metric))
    {
        if (pTmp6Rt->u4Flag & RTM6_RT_REACHABLE)
        {
            u1RtCount++;
        }
        pTmp6Rt = pTmp6Rt->pNextAlternatepath;
    }
    *pu1RtCount = u1RtCount;
}

/*****************************************************************************
 * Function     : RtmDeleteRtFromPRT                                         *
 * Description  : Delete the Route from the routeEntryList of the NextHop    *
 *                node                                 *
 * Input        : pPRoute - Pointer to the Route Added to trie               *
 *                pRtm6Cxt -RTM Context Pointer                               *
 * Output       : None                                                       *
 * Returns      : IP6_SUCCESS/IP6_FAILURE                                      *
 *****************************************************************************/

INT4
Rtm6DeleteRtFromPRTInCxt (tRtm6Cxt * pRtm6Cxt, tIp6RtEntry * pPRoute)
{

    tPNh6Node           Exst6NhNode;
    tPNh6Node          *pPNh6Node = NULL;
    tPRt6Entry         *pRtCur6Entry = NULL;

    Exst6NhNode.pRtm6Cxt = pRtm6Cxt;
    MEMCPY (&(Exst6NhNode.NextHop), &(pPRoute->nexthop), sizeof (tIp6Addr));

    pPNh6Node = RBTreeGet (gRtm6GlobalInfo.pPRT6RBRoot, &Exst6NhNode);
    if (pPNh6Node == NULL)
    {
        return IP6_FAILURE;
    }

    TMO_SLL_Scan (&pPNh6Node->routeEntryList, pRtCur6Entry, tPRt6Entry *)
    {
        if ((pRtCur6Entry->pPend6Rt == pPRoute) &&
            (pRtCur6Entry->pRtm6Cxt->u4ContextId == pRtm6Cxt->u4ContextId))
        {
            break;
        }
    }
    /* Route doesnt exist in PRT */
    if (pRtCur6Entry == NULL)
    {
        return IP6_FAILURE;
    }

    /* Delete the pending route entry from the list */
    TMO_SLL_Delete (&pPNh6Node->routeEntryList, (tTMO_SLL_NODE *) pRtCur6Entry);

    /* Reset the flag that indicates whether route exists in
     * PRT or not */
    pRtCur6Entry->pPend6Rt->u4Flag &= (UINT4) (~RTM6_RT_IN_PRT);

    MemReleaseMemBlock (gRtm6GlobalInfo.Rtm6URRtPoolId, (UINT1 *) pRtCur6Entry);

    if (TMO_SLL_Count (&(pPNh6Node->routeEntryList)) == 0)
    {
        /* No pending route exists in the list.Delete the next hop
         * entry from RB Tree */
        if (RBTreeRemove (gRtm6GlobalInfo.pPRT6RBRoot, pPNh6Node) == RB_FAILURE)
        {
            return IP6_FAILURE;
        }
        MemReleaseMemBlock (gRtm6GlobalInfo.Rtm6PNextHopPoolId,
                            (UINT1 *) pPNh6Node);
    }

    return IP6_SUCCESS;
}

/*******************************************************************************
Function            :   Rtm6DelRtFromPRTInCxt
Description         :   This function is used to Delete the route from
                        the PRT List when this route is not available
                        in Trie Datastructure
Input parameters    :   pDst : Destination network.
                    :   u1PrefixLen : Prefix Length
                    :   pNextHop : Next Hop .
                    :   u4Metric : Metric value.
                    :   i1RtProto : Id through which the route is learnt
                    :   pRtm6Cxt  : Rtm Context Pointer.

Output parameters   :   None.
Global variables
Affected            :   None.
Return value        :   None.

*******************************************************************************/

VOID
Rtm6DelRtFromPRTInCxt (tRtm6Cxt * pRtm6Cxt, tIp6Addr * pDst, UINT1 u1PrefixLen,
                       tIp6Addr * pNextHop, UINT4 u4Metric, INT1 i1RtProto)
{
    tPNh6Node          *pNh6Entry = NULL, ExstNh6Node;
    tPRt6Entry         *pRt6Curr = NULL;
    tPRt6Entry         *pTmp6Entry = NULL;
    UNUSED_PARAM (u4Metric);

    MEMCPY (&ExstNh6Node.NextHop, pNextHop, sizeof (tIp6Addr));
    ExstNh6Node.pRtm6Cxt = pRtm6Cxt;
    pNh6Entry = RBTreeGet (gRtm6GlobalInfo.pPRT6RBRoot, &ExstNh6Node);

    if (pNh6Entry == NULL)
    {
        return;
    }
    UTL_SLL_OFFSET_SCAN (&(pNh6Entry->routeEntryList), pRt6Curr, pTmp6Entry,
                         tPRt6Entry *)
    {
        if ((MEMCMP (&(pRt6Curr->pPend6Rt->dst), pDst, sizeof (tIp6Addr)) == 0)
            &&
            (MEMCMP
             (&(pRt6Curr->pPend6Rt->nexthop), pNextHop, sizeof (tIp6Addr)) == 0)
            && (pRt6Curr->pPend6Rt->u1Prefixlen == u1PrefixLen)
            && (pRt6Curr->pPend6Rt->i1Proto == i1RtProto))
        {
            Rtm6DeleteRtFromPRTInCxt (pRtm6Cxt, pRt6Curr->pPend6Rt);
        }
    }
    return;

}

/************************************************************************/
/* Function           : Rtm6TmrSetTimer                                  */
/*                                                                      */
/* Input(s)           : pTimer - Pointer to Timer block                 */
/*                    : u1TimerId - Timer Id                            */
/*                    : i4Duration - Time in seconds for which the      */
/*                    :              timer is started                   */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : TMR_SUCCESS/TMR_FAILURE                         */
/*                                                                      */
/************************************************************************/
PUBLIC INT4
Rtm6TmrSetTimer (tTmrBlk * pTimer, UINT1 u1TimerId, INT4 i4Duration)
{
    if ((u1TimerId == 4) || (u1TimerId == 5))
        if (TmrStart (gRtm6GlobalInfo.Rtm6TmrListId, pTimer, u1TimerId,
                      (UINT4) i4Duration, 0) != TMR_SUCCESS)
        {
            return (INT4) TMR_FAILURE;
        }
    return TMR_SUCCESS;
}

/******************************************************************************
 * Function           : Rtm6ApiAddOrDelRtsInCxt
 * Description        : This function programs or deletes the route entries in 
 *                      Forwarding plane based on the Best NextHop. 
 * Input(s)           : u4ContextId  - Context Id                              
 *                      u1RtCommand -  RTM status Add/Delete
 * Output(s)          : None                             
 * Returns            : None
 ******************************************************************************/
INT4
Rtm6ApiAddOrDelRtsInCxt (UINT4 u4ContextId, tIp6RtEntry * pIp6RtEntry,
                         UINT1 u1RtCommand)
{
#ifdef NPAPI_WANTED
    tRtm6Cxt           *pRtm6Cxt = NULL;
    tIp6RtEntry        *pIp6BestRtEntry = NULL;
    tFsNpIntInfo        IntInfo;
    tFsNpRouteInfo      RouteInfo;
    UINT4               u4NHType;
    INT4                i4RetValue = RTM6_FAILURE;
#endif

#ifndef NPAPI_WANTED
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u1RtCommand);
    UNUSED_PARAM (pIp6RtEntry);
    return RTM6_SUCCESS;
#else

    RTM6_TASK_LOCK ();

    pRtm6Cxt = UtilRtm6GetCxt (u4ContextId);
    if (pRtm6Cxt == NULL)
    {
        RTM6_TASK_UNLOCK ();
        return RTM6_FAILURE;
    }

    pIp6BestRtEntry = NULL;
    if (pIp6RtEntry->u1AddrType == ADDR6_ANYCAST)
    {
        i4RetValue =
            Rtm6UtilFwdTblGetBestAnycastRouteInCxt (pRtm6Cxt, pIp6RtEntry,
                                                    &pIp6BestRtEntry);
    }
    else
    {
        i4RetValue =
            Rtm6UtilFwdTblGetBestRouteInCxt (pRtm6Cxt, pIp6RtEntry,
                                             &pIp6BestRtEntry);
    }
    if (i4RetValue == IP6_SUCCESS)
    {
        /*RTM6 Add on HW */
        if (u1RtCommand == IP6_ROUTE_ADD)
        {
            MEMSET (&IntInfo, 0, sizeof (tFsNpIntInfo));

            IntInfo.u1IfType = Ip6GetIfType (pIp6BestRtEntry->u4Index);
            if ((IntInfo.u1IfType == CFA_L3IPVLAN)
                || (IntInfo.u1IfType == CFA_L3SUB_INTF))
            {
                IntInfo.u4PhyIfIndex = pIp6BestRtEntry->u4Index;
            }
            if (IntInfo.u1IfType == CFA_PSEUDO_WIRE)
            {
                IntInfo.u4PhyIfIndex = pIp6BestRtEntry->u4Index;
            }

            if (IntInfo.u1IfType != IP6_ENET_INTERFACE_TYPE)
            {
                if (CfaGetVlanId (IntInfo.u4PhyIfIndex, &(IntInfo.u2VlanId))
                    == CFA_FAILURE)
                {
                }
            }
            else
            {
                IntInfo.u4PhyIfIndex = pIp6BestRtEntry->u4Index;
                IntInfo.u2VlanId = 0;
            }
            /* check for the type before programming */
            if (pIp6BestRtEntry->i1Type == IP6_ROUTE_TYPE_DIRECT)
            {
                u4NHType = NH_DIRECT;
            }
            else if (pIp6BestRtEntry->i1Type == IP6_ROUTE_TYPE_DISCARD)
            {
                u4NHType = NH_DISCARD;
            }
            else if (pIp6BestRtEntry->i1Type == IP6_ROUTE_TYPE_INDIRECT)
            {
                u4NHType = NH_REMOTE;
            }
            else
            {
                /* Unknown Route Type. */
                u4NHType = NH_SENDTO_CP;
            }

            if (RTM6_IS_NP_PROGRAMMING_ALLOWED () == RTM6_SUCCESS)
            {
#ifdef LNXIP6_WANTED
                if ((NetIpv6GetIpForwardingInCxt (u4ContextId) ==
                     IP6_FORW_ENABLE)
                    || (pIp6BestRtEntry->i1Proto == CIDR_LOCAL_ID))
#endif
                    /*check for reachability and get the installed route count
                     *send it to the hardware*/

                    Rtm6SetReachState (pRtm6Cxt->u4ContextId, pIp6BestRtEntry);
                Rtm6GetInstalledRouteCount (pIp6BestRtEntry,
                                            &pIp6BestRtEntry->u1EcmpCount);

                IntInfo.u1RouteCount = pIp6BestRtEntry->u1EcmpCount;
                if (Ipv6FsNpIpv6UcRouteAdd (pRtm6Cxt->u4ContextId,
                                            (UINT1 *) &pIp6BestRtEntry->dst,
                                            pIp6BestRtEntry->u1Prefixlen,
                                            (UINT1 *) &pIp6BestRtEntry->nexthop,
                                            u4NHType, &IntInfo) == FNP_FAILURE)
                {
                    RTM6_TRC (pRtm6Cxt->u4ContextId, RTM6_ALL_FAILURE_TRC |
                              RTM6_OS_RESOURCE_TRC, RTM6_MOD_NAME,
                              "\tNP Route Addition Failed \n");
                    if (Rtm6FrtAddInfo
                        (pIp6BestRtEntry, pRtm6Cxt->u4ContextId,
                         (UINT1) u4NHType) == RTM6_SUCCESS)
                    {
                        Rtm6RedSyncFrtInfo (pIp6BestRtEntry,
                                            pRtm6Cxt->u4ContextId,
                                            (UINT1) u4NHType, RTM6_ADD_ROUTE);

                    }
                    else
                    {
                        RTM6_TRC_ARG2 (pRtm6Cxt->u4ContextId,
                                       RTM6_ALL_FAILURE_TRC |
                                       RTM6_OS_RESOURCE_TRC, RTM6_MOD_NAME,
                                       "ERROR[RTM]: IPV6  Failed Route Addition Failed%s/%d   \n",
                                       Ip6PrintNtop (&pIp6BestRtEntry->dst),
                                       (INT4) pIp6BestRtEntry->u1Prefixlen);
                    }
                }
                pIp6BestRtEntry->u4HwIntfId[0] = IntInfo.u4HwIntfId[0];

            }
        }
        else
        {
            /*RTM6 delete on HW */
            if (RTM6_IS_NP_PROGRAMMING_ALLOWED () == RTM6_SUCCESS)
            {
                /*passing ECMPcount to the hardware */
                RouteInfo.u1RouteCount = pIp6BestRtEntry->u1EcmpCount;
                RouteInfo.u4HwIntfId[0] = pIp6BestRtEntry->u4HwIntfId[0];
                Ipv6FsNpIpv6UcRouteDelete (pRtm6Cxt->u4ContextId,
                                           (UINT1 *) &pIp6BestRtEntry->dst,
                                           pIp6BestRtEntry->u1Prefixlen,
                                           (UINT1 *) &pIp6BestRtEntry->nexthop,
                                           pIp6BestRtEntry->u4Index,
                                           &RouteInfo);
            }
        }

    }

    RTM6_TASK_UNLOCK ();
    return RTM6_SUCCESS;
#endif /* NPAPI_WANTED */
}

/******************************************************************************
 * Function           : Rtm6AddOrDelRtInFIBInCxt
 * Description        : This function programs or deletes the route entries in 
 *                      Forwarding plane based on the Best NextHop. 
 * Input(s)           : u4ContextId  - Context Id                              
 *                      u1RtCommand -  RTM status Add/Delete
 * Output(s)          : None                             
 * Returns            : None
 ******************************************************************************/
INT4
Rtm6AddOrDelRtInFIBInCxt (UINT4 u4ContextId, tIp6RtEntry * pIp6RtEntry,
                          UINT1 u1RtCommand)
{
#ifdef NPAPI_WANTED
    tRtm6Cxt           *pRtm6Cxt = NULL;
    tFsNpIntInfo        IntInfo;
    tFsNpRouteInfo      RouteInfo;
    UINT4               u4NHType;
#endif

#ifndef NPAPI_WANTED
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u1RtCommand);
    UNUSED_PARAM (pIp6RtEntry);
    return RTM6_SUCCESS;
#else
    pRtm6Cxt = UtilRtm6GetCxt (u4ContextId);
    if (pRtm6Cxt == NULL)
    {
        return RTM6_FAILURE;
    }

    /*RTM6 Add on HW */
    if (u1RtCommand == IP6_ROUTE_ADD)
    {
        MEMSET (&IntInfo, 0, sizeof (tFsNpIntInfo));

        IntInfo.u1IfType = Ip6GetIfType (pIp6RtEntry->u4Index);
        if ((IntInfo.u1IfType == CFA_L3IPVLAN)
            || (IntInfo.u1IfType == CFA_L3SUB_INTF))
        {
            IntInfo.u4PhyIfIndex = pIp6RtEntry->u4Index;
        }
        if (IntInfo.u1IfType == CFA_PSEUDO_WIRE)
        {
            IntInfo.u4PhyIfIndex = pIp6RtEntry->u4Index;
        }

        if (IntInfo.u1IfType != IP6_ENET_INTERFACE_TYPE)
        {
            if (CfaGetVlanId (IntInfo.u4PhyIfIndex, &(IntInfo.u2VlanId))
                == CFA_FAILURE)
            {
            }
        }
        else
        {
            IntInfo.u4PhyIfIndex = pIp6RtEntry->u4Index;
            IntInfo.u2VlanId = 0;
        }
        /* check for the type before programming */
        if (pIp6RtEntry->i1Type == IP6_ROUTE_TYPE_DIRECT)
        {
            u4NHType = NH_DIRECT;
        }
        else if (pIp6RtEntry->i1Type == IP6_ROUTE_TYPE_DISCARD)
        {
            u4NHType = NH_DISCARD;
        }
        else if (pIp6RtEntry->i1Type == IP6_ROUTE_TYPE_INDIRECT)
        {
            u4NHType = NH_REMOTE;
        }
        else
        {
            /* Unknown Route Type. */
            u4NHType = NH_SENDTO_CP;
        }

        if (RTM6_IS_NP_PROGRAMMING_ALLOWED () == RTM6_SUCCESS)
        {
            /*check for reachability and get the installed route count
             *send it to the hardware*/
            Rtm6SetReachState (pRtm6Cxt->u4ContextId, pIp6RtEntry);
            Rtm6GetInstalledRouteCount (pIp6RtEntry, &pIp6RtEntry->u1EcmpCount);
            IntInfo.u1RouteCount = pIp6RtEntry->u1EcmpCount;

            if (Ipv6FsNpIpv6UcRouteAdd (pRtm6Cxt->u4ContextId,
                                        (UINT1 *) &pIp6RtEntry->dst,
                                        pIp6RtEntry->u1Prefixlen,
                                        (UINT1 *) &pIp6RtEntry->nexthop,
                                        u4NHType, &IntInfo) == FNP_FAILURE)
            {
                RTM6_TRC (pRtm6Cxt->u4ContextId, RTM6_ALL_FAILURE_TRC |
                          RTM6_OS_RESOURCE_TRC, RTM6_MOD_NAME,
                          "\tNP Route Addition Failed \n");
                if (Rtm6FrtAddInfo
                    (pIp6RtEntry, pRtm6Cxt->u4ContextId,
                     (UINT1) u4NHType) == RTM6_SUCCESS)
                {
                    Rtm6RedSyncFrtInfo (pIp6RtEntry, pRtm6Cxt->u4ContextId,
                                        (UINT1) u4NHType, RTM6_ADD_ROUTE);

                }
                else
                {
                    RTM6_TRC_ARG2 (pRtm6Cxt->u4ContextId, RTM6_ALL_FAILURE_TRC |
                                   RTM6_OS_RESOURCE_TRC, RTM6_MOD_NAME,
                                   "ERROR[RTM]: IPV6  Failed Route Addition Failed%s/%d   \n",
                                   Ip6PrintNtop (&pIp6RtEntry->dst),
                                   (INT4) pIp6RtEntry->u1Prefixlen);
                }
            }
            pIp6RtEntry->u4HwIntfId[0] = IntInfo.u4HwIntfId[0];
        }
    }
    else
    {
        /*RTM6 delete on HW */
        if (RTM6_IS_NP_PROGRAMMING_ALLOWED () == RTM6_SUCCESS)
        {
            MEMSET (&RouteInfo, 0, sizeof (tFsNpRouteInfo));
            /*Copying the ECMP count to the structure passing to NPAPI */
            RouteInfo.u1RouteCount = pIp6RtEntry->u1EcmpCount;
            RouteInfo.u4HwIntfId[0] = pIp6RtEntry->u4HwIntfId[0];
            Ipv6FsNpIpv6UcRouteDelete (pRtm6Cxt->u4ContextId,
                                       (UINT1 *) &pIp6RtEntry->dst,
                                       pIp6RtEntry->u1Prefixlen,
                                       (UINT1 *) &pIp6RtEntry->nexthop,
                                       pIp6RtEntry->u4Index, &RouteInfo);
            /*if the entry is a drop route , delete the drop entry */
            RouteInfo.u1NHType = NH_DISCARD;
            Ipv6FsNpIpv6UcRouteDelete (pRtm6Cxt->u4ContextId,
                                       (UINT1 *) &pIp6RtEntry->dst,
                                       pIp6RtEntry->u1Prefixlen,
                                       (UINT1 *) &pIp6RtEntry->nexthop,
                                       pIp6RtEntry->u4Index, &RouteInfo);
        }
    }

    return RTM6_SUCCESS;
#endif /* NPAPI_WANTED */
}

/***********************************************************************************************
 * Function           :Rtm6GetNextHopCount
 * Description        : This function returns the ND state for the next hop of the route entry.
 * Input(s)           : u4ContextId - RTM context ID
 *                      nexthop-  Route for whose NH the ND cache needs to be checkec
 * Output(s)          : count of usage of next hop for ECMP
 * Returns            : None
 **********************************************************************************************/
INT4
Rtm6GetNextHopCount (UINT4 u4ContextId, tIp6RtEntry * pEcmpIp6Route)
{
    tIp6RtEntry        *pIp6Route = NULL;
    tIp6RtEntry        *pIp6BestRoute = NULL;
    tIp6RtEntry        *pIp6TmpRoute = NULL;
    tIp6RtEntry        *pNextRt = NULL;
    INT4                i4Count = 0;
    VOID               *pRibNode = NULL;
    tRtm6Cxt           *pRtm6Cxt = NULL;
    tIp6Addr           *pNextHopIpAddr;

    pRtm6Cxt = UtilRtm6GetCxt (u4ContextId);
    if (pRtm6Cxt == NULL)
    {
        return 0;
    }

    pNextHopIpAddr = &pEcmpIp6Route->nexthop;
    if (Rtm6TrieGetFirstEntryInCxt (pRtm6Cxt,
                                    &pIp6Route, &pRibNode) == IP6_FAILURE)
    {
        return 0;
    }

/*Get the best route. And if there are ECMP routes
 * matching the next hop, and if it is not already 
 * reachable, program the routes in the kernel.
 */

    do
    {

        if (Rtm6UtilFwdTblGetBestRouteInCxt
            (pRtm6Cxt, pIp6Route, &pIp6BestRoute) == IP_SUCCESS)
        {
            pIp6TmpRoute = pIp6BestRoute;
            while (pIp6TmpRoute != NULL)
            {

                /* Check for next hop */
                if (Ip6AddrMatch (pNextHopIpAddr, &pIp6TmpRoute->nexthop,
                                  IP6_ADDR_SIZE_IN_BITS))
                {
                    if (((pIp6TmpRoute->u4Flag & RTM6_ECMP_RT) == RTM6_ECMP_RT)
                        && pIp6TmpRoute != pEcmpIp6Route)
                    {

                        i4Count++;
                    }

                }

                pIp6TmpRoute = pIp6TmpRoute->pNextAlternatepath;
            }
            /*Check the reachablity of other routes and add to ECMP6 PRT if required */

        }

        if (Rtm6TrieGetNextEntryInCxt (pRtm6Cxt,
                                       pIp6Route, &pNextRt,
                                       &pRibNode) == RTM6_FAILURE)
        {
            /* No more route present. Set the cookie to indicate it */
            break;
        }
        pIp6Route = pNextRt;
        pNextRt = NULL;

    }
    while (pIp6Route != NULL);

    return i4Count;
}

 /*****************************************************************************
 * Function Name    :   Rtm6GetInstalledRouteCountStatus
 *
 * Description      :   Provides the number of routes installed in the hardware with
 *            the given route metric for the given destination
 *
 * Inputs           :   pIp6Route - Start pointer of ECMP6 Lists
 *                      pRtm6Cxt  - Rtm Context Pointer
 *
 * Return Value     :  No. of Resolved Best Route counts installed in the hw
 *****************************************************************************/

VOID
Rtm6GetInstalledRouteCountStatus (tRtm6Cxt * pRtm6Cxt,
                                  tIp6RtEntry * pDelIp6RtEntry,
                                  UINT1 *pu1RtCount)
{

    tIp6RtEntry        *pTmpIp6RtEntry = NULL;
    VOID               *pRibNode = NULL;
    UINT1               u1RouteCount = 0;
    INT4                i4OutCome = 0;

    i4OutCome = Rtm6TrieSearchExactEntryInCxt (pRtm6Cxt, pDelIp6RtEntry,
                                               &pTmpIp6RtEntry, &pRibNode);

    if (i4OutCome == RTM6_SUCCESS)
    {
        while (pTmpIp6RtEntry != NULL)
        {
            if (pTmpIp6RtEntry->u4Flag & RTM6_RT_REACHABLE)
            {
                u1RouteCount++;
            }
            pTmpIp6RtEntry = pTmpIp6RtEntry->pNextAlternatepath;
        }
        *pu1RtCount = u1RouteCount;
    }
}
