/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rtm6cxtutl.c,v 1.17 2018/02/01 11:02:29 siva Exp $
 *
 * Description:This file contains APIs provided by RTM6   
 *
 *******************************************************************/
#include "rtm6inc.h"
#include "fsmrt6lw.h"
#include "rmgr.h"
#include "fsmsipcli.h"

PRIVATE VOID        Rtm6ClearRrdRoutingProtoTable (UINT4 u4ContextId);
PRIVATE VOID        Rtm6ClearRrdControlTable (UINT4 u4ContextId);
PRIVATE VOID        Rtm6ClearProtoIfRouteFromTrieInCxt (tRtm6Cxt * pRtm6Cxt,
                                                        UINT1 u1Protocol,
                                                        UINT2 u2Index);
/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : UtilRtm6GetVcmSystemMode                            */
/*                                                                           */
/*     DESCRIPTION      : This function calls the VCM Module to get the      */
/*                        mode of the system (SI / MI).                      */
/*                                                                           */
/*     INPUT            : u2ProtocolId - Protocol Identifier                 */
/*                                                                           */
/*     OUTPUT           : pu1Alias       - Switch Alias Name.                */
/*                                                                           */
/*     RETURNS          : VCM_MI_MODE/VCM_SI_MODE                            */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
UtilRtm6GetVcmSystemMode (UINT2 u2ProtocolId)
{
    return (VcmGetSystemMode (u2ProtocolId));
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : UtilRtm6GetVcmSystemModeExt                        */
/*                                                                           */
/*     DESCRIPTION      : This function calls the VCM Module to get the      */
/*                        mode of the system (SI / MI).                      */
/*                                                                           */
/*     INPUT            : u2ProtocolId - Protocol Identifier                 */
/*                                                                           */
/*     OUTPUT           : pu1Alias       - Switch Alias Name.                */
/*                                                                           */
/*     RETURNS          : VCM_MI_MODE/VCM_SI_MODE                            */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
UtilRtm6GetVcmSystemModeExt (UINT2 u2ProtocolId)
{
    return (VcmGetSystemModeExt (u2ProtocolId));
}

/*****************************************************************************/
/*                                                                           */
/* Function     : UtilRtm6VcmIsVcExist                                       */
/*                                                                           */
/* Description  : This function finds and checks whether the given           */
/*               context ID is valid and available in VCM                    */
/*                                                                           */
/* Input        :  u4Rtm6CxtId  : Rtm6 Context Id                            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RTM6_SUCCESS/RTM6_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
UtilRtm6VcmIsVcExist (UINT4 u4Rtm6CxtId)
{
    if (UtilRtm6GetVcmSystemMode (RTM6_PROTOCOL_ID) == VCM_MI_MODE)
    {
        if (VcmIsL3VcExist (u4Rtm6CxtId) == VCM_FALSE)
        {
            return RTM6_FAILURE;
        }
    }
    else
    {
        if (u4Rtm6CxtId != RTM6_DEFAULT_CXT_ID)
        {
            return RTM6_FAILURE;
        }
    }
    return RTM6_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : UtilRtm6GetCxt                                             */
/*                                                                           */
/* Description  : This function finds and returns the pointer to the         */
/*                RTM6 context structure for the given RTM6 context ID       */
/*                                                                           */
/* Input        :  u4Rtm6CxtId  : Rtm6 Context Id                            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Pointer to RTM6  context structure, if context is found    */
/*                NULL, if not found                                         */
/*                                                                           */
/*****************************************************************************/

PUBLIC tRtm6Cxt    *
UtilRtm6GetCxt (UINT4 u4Rtm6CxtId)
{
    if ((UtilRtm6VcmIsVcExist (u4Rtm6CxtId) == RTM6_FAILURE) ||
        (gRtm6GlobalInfo.apRtm6Cxt[u4Rtm6CxtId] == NULL))
    {
        return NULL;
    }
    return (gRtm6GlobalInfo.apRtm6Cxt[u4Rtm6CxtId]);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : UtilRtm6GetNextCxtId                                       */
/*                                                                           */
/* Description  : This function finds the first valid ospf context ID        */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : pu4Rtm6CxtId                                               */
/*                                                                           */
/* Returns      : RTM6_FAILURE/RTM6_SUCCESS.                                 */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
UtilRtm6GetFirstCxtId (UINT4 *pu4Rtm6CxtId)
{
    UINT4               u4Rtm6CxtId = 0;

    for (u4Rtm6CxtId = 0; u4Rtm6CxtId < gRtm6GlobalInfo.u4MaxContextLimit;
         u4Rtm6CxtId++)
    {
        if ((gRtm6GlobalInfo.apRtm6Cxt[u4Rtm6CxtId]) != NULL)
        {
            *pu4Rtm6CxtId = u4Rtm6CxtId;
            return RTM6_SUCCESS;

        }
    }
    return RTM6_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : UtilRtm6GetNextCxtId                                       */
/*                                                                           */
/* Description  : This function finds the next valid ospf context ID         */
/*                                                                           */
/* Input        : u4Rtm6CxtId  : Rtm6 Context Id                             */
/*                                                                           */
/* Output       : pu4NextRtm6CxtId                                           */
/*                                                                           */
/* Returns      : RTM6_FAILURE/RTM6_SUCCESS.                                 */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
UtilRtm6GetNextCxtId (UINT4 u4Rtm6CxtId, UINT4 *pu4NextRtm6CxtId)
{
    UINT4               u4CurRtm6CxtId = 0;

    u4Rtm6CxtId++;
    for (u4CurRtm6CxtId = u4Rtm6CxtId;
         ((u4CurRtm6CxtId < gRtm6GlobalInfo.u4MaxContextLimit) &&
          (gRtm6GlobalInfo.apRtm6Cxt[u4CurRtm6CxtId] == NULL));
         u4CurRtm6CxtId++);
    if (u4CurRtm6CxtId >= gRtm6GlobalInfo.u4MaxContextLimit)
    {
        return RTM6_FAILURE;
    }
    else
    {
        *pu4NextRtm6CxtId = u4CurRtm6CxtId;
        return RTM6_SUCCESS;
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : UtilRtm6SetContext                                          */
/*                                                                           */
/* Description  : This function sets the current RTM                         */
/*                context pointer (gRtm6GlobalInfo.pRtmCxt)                   */
/*                 for the given RTM6 Context ID                              */
/*                                                                           */
/* Input        :  u4Rtm6CxtId  : RTM6 Context Id                              */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : SNMP_FAILURE/SNMP_SUCCESS                                  */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
UtilRtm6SetContext (UINT4 u4Rtm6CxtId)
{
    if (UtilRtm6GetVcmSystemModeExt (RTM6_PROTOCOL_ID) == VCM_SI_MODE)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        /* Set the Context pointer for VRF Wanted case */
        if ((UtilRtm6VcmIsVcExist (u4Rtm6CxtId) == RTM6_FAILURE))
        {
            return SNMP_FAILURE;
        }

        if ((gRtm6GlobalInfo.pRtm6Cxt =
             gRtm6GlobalInfo.apRtm6Cxt[u4Rtm6CxtId]) == NULL)
        {
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : UtilRtm6ResetContext                                       */
/*                                                                           */
/* Description  : This function resets the current Rtm6                      */
/*                context pointer (gRtm6GlobalInfo.pRtm6Cxt)                   */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
UtilRtm6ResetContext ()
{
    if (UtilRtm6GetVcmSystemModeExt (RTM6_PROTOCOL_ID) == VCM_MI_MODE)
    {
        gRtm6GlobalInfo.pRtm6Cxt = NULL;
    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : UtilRtm6IsValidCxtId                                       */
/*                                                                           */
/* Description  : This function checks whether the given context ID          */
/*                is valid RTM6 CONTEXT ID  or INVALID CONTEXT ID            */
/*                                                                           */
/* Input        : u4Rtm6CxtId  : Rtm6 Context Id                             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RTM6_FAILURE/RTM6_SUCCESS.                                 */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
UtilRtm6IsValidCxtId (UINT4 u4Rtm6CxtId)
{
    if (u4Rtm6CxtId < gRtm6GlobalInfo.u4MaxContextLimit)
    {
        if ((gRtm6GlobalInfo.apRtm6Cxt[u4Rtm6CxtId]) != NULL)
        {
            return RTM6_SUCCESS;
        }
    }
    return RTM6_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : UtilRtm6GetCxtIdFromCxtName                         */
/*                                                                           */
/*     DESCRIPTION      : This function check whether the entry is present   */
/*                        for the correponding Vrf-name in VCM.              */
/*                        if yes it will                                     */
/*                        return the Context-Id for the respective vrf-name  */
/*                                                                           */
/*     INPUT            : pu1Alias   - Name of the Switch.                   */
/*                                                                           */
/*     OUTPUT           : pu4VcNum   - Context-Id of the switch              */
/*                                                                           */
/*     RETURNS          : RTM6_SUCCESS/RTM6_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
UtilRtm6GetCxtIdFromCxtName (UINT1 *pu1Alias, UINT4 *pu4VcNum)
{
    if (VcmIsVrfExist (pu1Alias, pu4VcNum) == VCM_FALSE)
    {
        return RTM6_FAILURE;
    }
    return RTM6_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : UtilRtm6GetVcmAliasName                            */
/*                                                                           */
/*     DESCRIPTION      : This function will return the alias name of the    */
/*                        given context.                                     */
/*                                                                           */
/*     INPUT            : u4ContextId    - Context-Id.                       */
/*                                                                           */
/*     OUTPUT           : pu1Alias       - Vrf Name.                         */
/*                                                                           */
/*     RETURNS          : RTM6_SUCCESS / RTM6_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
UtilRtm6GetVcmAliasName (UINT4 u4ContextId, UINT1 *pu1Alias)
{
    if (VcmGetAliasName (u4ContextId, pu1Alias) == VCM_FAILURE)
    {
        return RTM6_FAILURE;
    }
    return RTM6_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : UtilRtm6GetCurrCxtId                                       */
/*                                                                           */
/* Description  : This function returns the current RTM context Id           */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Current context Id                                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
UtilRtm6GetCurrCxtId ()
{
    UINT4               u4ContextId = 0;
    if (gRtm6GlobalInfo.pRtm6Cxt != NULL)
    {
        u4ContextId = gRtm6GlobalInfo.pRtm6Cxt->u4ContextId;
    }
    return u4ContextId;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : UtilRtm6IncMsrForRtm6Scalar                                */
/*                                                                           */
/* Description  : This function performs Inc Msr functionality for           */
/*                MI Scalar                                                  */
/*                                                                           */
/* Input        : u4SetVal    : Set Value                                    */
/*                pu4ObjectId : Object Id                                   */
/*                u4OidLength : Oid Length                                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                     */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
UtilRtm6IncMsrForRtm6Scalar (UINT4 u4SetVal, UINT4 *pu4ObjectId,
                             UINT4 u4OidLength)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;

    RM_GET_SEQ_NUM (&u4SeqNum);
    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.pu4ObjectId = pu4ObjectId;
    SnmpNotifyInfo.u4OidLen = u4OidLength;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = Rtm6Lock;
    SnmpNotifyInfo.pUnLockPointer = Rtm6UnLock;
    SnmpNotifyInfo.u4Indices = 0;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u", u4SetVal));
#if !defined (ISS_WANTED) && !defined (RM_WANTED)
    UNUSED_PARAM (u4SetVal);
    UNUSED_PARAM (pu4ObjectId);
    UNUSED_PARAM (u4OidLength);
#endif
}

/*****************************************************************************/
/*                                                                          */
/* Function     : UtilRtm6IncMsrForRtm6Table                                */
/*                                                                          */
/* Description  : This function performs Inc Msr functionality for          */
/*                MI RTM6 Table                                             */
/*                                                                          */
/* Input        : u4Rtm6CxtId  : Rtm6 Context Id                            */
/*                i4SetVal    : Set Value                                   */
/*                pu4ObjectId : Object Id                                   */
/*                u4OidLength : Oid Length                                  */
/*                c1Type      : set value type                              */
/*                                                                          */
/* Output       : None                                                      */
/*                                                                          */
/* Returns      : None                                                      */
/*                                                                          */
/*****************************************************************************/
PUBLIC VOID
UtilRtm6IncMsrForRtm6Table (UINT4 u4Rtm6CxtId, INT4 i4SetVal,
                            UINT4 *pu4ObjectId, UINT4 u4OidLength, CHR1 c1Type)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;

    RM_GET_SEQ_NUM (&u4SeqNum);
    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.pu4ObjectId = pu4ObjectId;
    SnmpNotifyInfo.u4OidLen = u4OidLength;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = Rtm6Lock;
    SnmpNotifyInfo.pUnLockPointer = Rtm6UnLock;
    SnmpNotifyInfo.u4Indices = 1;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    switch (c1Type)
    {
        case 'p':
        {
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p", u4Rtm6CxtId, i4SetVal));
            break;
        }
        case 'i':
        {
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", u4Rtm6CxtId, i4SetVal));
            break;
        }
        case 'u':
        {
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u", u4Rtm6CxtId, i4SetVal));
            break;
        }
    }
#if !defined (ISS_WANTED) && !defined (RM_WANTED)
    UNUSED_PARAM (u4Rtm6CxtId);
    UNUSED_PARAM (i4SetVal);
    UNUSED_PARAM (pu4ObjectId);
    UNUSED_PARAM (u4OidLength);
    UNUSED_PARAM (c1Type);
#endif
}

/*****************************************************************************/
/*                                                                           */
/* Function     : UtilRtm6IncMsrForRrd6ControlTable                          */
/*                                                                           */
/* Description  : This function performs Inc Msr functionality for           */
/*                MI RRD6 Control Table                                      */
/*                                                                           */
/* Input        : u4Rtm6CxtId  : Rtm6 Context Id                             */
/*                pDest : RRD6 Destination Address                           */
/*                i4PrefixLength: RRD6 dest prefix length                    */
/*                i4SetVal    : Set Value                                    */
/*                pu4ObjectId : Object Id                                    */
/*                u4OidLength : Oid Length                                   */
/*                u1RowStatus : u1RowStatus                                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
UtilRtm6IncMsrForRrd6ControlTable (UINT4 u4Rtm6CxtId,
                                   tSNMP_OCTET_STRING_TYPE * pDest,
                                   INT4 i4PrefixLength,
                                   INT4 i4SetVal, UINT4 *pu4ObjectId,
                                   UINT4 u4OidLength, UINT1 u1RowStatus)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;

    RM_GET_SEQ_NUM (&u4SeqNum);
    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.pu4ObjectId = pu4ObjectId;
    SnmpNotifyInfo.u4OidLen = u4OidLength;
    SnmpNotifyInfo.u1RowStatus = u1RowStatus;
    SnmpNotifyInfo.pLockPointer = Rtm6Lock;
    SnmpNotifyInfo.pUnLockPointer = Rtm6UnLock;
    SnmpNotifyInfo.u4Indices = IP6_THREE;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %s %i %i", u4Rtm6CxtId,
                      pDest, i4PrefixLength, i4SetVal));
#if !defined (ISS_WANTED) && !defined (RM_WANTED)
    UNUSED_PARAM (u4Rtm6CxtId);
    UNUSED_PARAM (pDest);
    UNUSED_PARAM (i4PrefixLength);
    UNUSED_PARAM (i4SetVal);
    UNUSED_PARAM (pu4ObjectId);
    UNUSED_PARAM (u4OidLength);
    UNUSED_PARAM (u1RowStatus);
#endif
}

/*****************************************************************************/
/*                                                                           */
/* Function     : UtilRtm6IncMsrForRrd6ProtoTable                            */
/*                                                                           */
/* Description  : This function performs Inc Msr functionality for           */
/*                MI RRD6 Proto Table                                        */
/*                                                                           */
/* Input        : u4Rtm6CxtId  : Rtm6 Context Id                             */
/*                i4ProtoId   : Routing protocols Id                         */
/*                i4SetVal    : Set Value                                    */
/*                pu4ObjectId : Object Id                                    */
/*                u4OidLength : Oid Length                                   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
UtilRtm6IncMsrForRrd6ProtoTable (UINT4 u4Rtm6CxtId, INT4 i4ProtoId,
                                 INT4 i4SetVal, UINT4 *pu4ObjectId,
                                 UINT4 u4OidLength)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;

    RM_GET_SEQ_NUM (&u4SeqNum);
    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.pu4ObjectId = pu4ObjectId;
    SnmpNotifyInfo.u4OidLen = u4OidLength;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = Rtm6Lock;
    SnmpNotifyInfo.pUnLockPointer = Rtm6UnLock;
    SnmpNotifyInfo.u4Indices = IP6_TWO;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i", u4Rtm6CxtId,
                      i4ProtoId, i4SetVal));
#if !defined (ISS_WANTED) && !defined (RM_WANTED)
    UNUSED_PARAM (u4Rtm6CxtId);
    UNUSED_PARAM (i4ProtoId);
    UNUSED_PARAM (i4SetVal);
    UNUSED_PARAM (pu4ObjectId);
    UNUSED_PARAM (u4OidLength);
#endif
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : Rtm6ClearContextEntries                            */
/*                                                                           */
/*     DESCRIPTION      : This function clears the context based             */
/*                        configurations done for the given context.         */
/*                                                                           */
/*     INPUT            : u4ContextId    - Context-Id.                       */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : RTM6_SUCCESS / RTM6_FAILURE                        */
/*                                                                           */
/*****************************************************************************/

INT4
Rtm6ClearContextEntries (UINT4 u4ContextId)
{
    if (nmhValidateIndexInstanceFsMIRtm6Table ((INT4) u4ContextId)
        == SNMP_FAILURE)
    {
        return RTM6_FAILURE;
    }
    nmhSetFsMIRrd6FilterByOspfTag ((INT4) u4ContextId, RTM6_FILTER_DISABLED);
    nmhSetFsMIRrd6RouterASNumber ((INT4) u4ContextId, IPV6_MIN_AS_NUM);
    nmhSetFsMIRrd6AdminStatus ((INT4) u4ContextId, RTM6_ADMIN_STATUS_DISABLED);
    nmhSetFsMIRrd6Trace ((INT4) u4ContextId, 0);

    Rtm6ClearRrdControlTable (u4ContextId);
    Rtm6ClearRrdRoutingProtoTable (u4ContextId);
    return RTM6_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : Rtm6ClearRrdControlTable                           */
/*                                                                           */
/*     DESCRIPTION      : This function clears the RRD control table         */
/*                        configurations done for the given context.         */
/*                                                                           */
/*     INPUT            : u4ContextId    - Context-Id.                       */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/

VOID
Rtm6ClearRrdControlTable (UINT4 u4ContextId)
{
    tSNMP_OCTET_STRING_TYPE IpAddress;
    tSNMP_OCTET_STRING_TYPE NxtIpAddress;
    INT4                i4NxtContextId = 0;
    INT4                i4NetMask = 0;
    INT4                i4NxtNetMask = 0;
    UINT1               au1IpAddr[IP6_ADDR_SIZE];
    UINT1               au1NxtIpAddr[IP6_ADDR_SIZE];

    MEMSET (au1NxtIpAddr, 0, IP6_ADDR_SIZE);
    MEMSET (au1IpAddr, 0, IP6_ADDR_SIZE);
    IpAddress.i4_Length = 0;
    IpAddress.pu1_OctetList = au1IpAddr;
    NxtIpAddress.i4_Length = 0;
    NxtIpAddress.pu1_OctetList = au1NxtIpAddr;

    while (nmhGetNextIndexFsMIRrd6ControlTable ((INT4) u4ContextId,
                                                &i4NxtContextId, &IpAddress,
                                                &NxtIpAddress, i4NetMask,
                                                &i4NxtNetMask) == SNMP_SUCCESS)
    {
        if (i4NxtContextId > (INT4) u4ContextId)
        {
            break;
        }
        i4NetMask = i4NxtNetMask;
        IpAddress.i4_Length = NxtIpAddress.i4_Length;
        MEMCPY (IpAddress.pu1_OctetList, NxtIpAddress.pu1_OctetList,
                NxtIpAddress.i4_Length);
        nmhSetFsMIRrd6ControlRowStatus (i4NxtContextId, &NxtIpAddress,
                                        i4NxtNetMask, RTM6_DESTROY);
    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : Rtm6ClearRrdRoutingProtoTable                      */
/*                                                                           */
/*     DESCRIPTION      : This function clears the RRD Routing Proto Table   */
/*                        configurations done for the given context.         */
/*                                                                           */
/*     INPUT            : u4ContextId    - Context-Id.                       */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/

VOID
Rtm6ClearRrdRoutingProtoTable (UINT4 u4ContextId)
{
    INT4                i4NxtContextId = 0;
    INT4                i4Proto = 0;
    INT4                i4NxtProto = 0;

    while (nmhGetNextIndexFsMIRrd6RoutingProtoTable ((INT4) u4ContextId,
                                                     &i4NxtContextId,
                                                     i4Proto, &i4NxtProto)
           == SNMP_SUCCESS)
    {
        if (i4NxtContextId > (INT4) u4ContextId)
        {
            break;
        }
        i4Proto = i4NxtProto;
        nmhSetFsMIRrd6AllowOspfAreaRoutes (i4NxtContextId, i4NxtProto,
                                           RTM6_REDISTRIBUTION_DISABLED);
        nmhSetFsMIRrd6AllowOspfExtRoutes (i4NxtContextId, i4NxtProto,
                                          RTM6_REDISTRIBUTION_DISABLED);
    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : Rtm6CxtNotifyStaticRtDeletion                      */
/*                                                                           */
/*     DESCRIPTION      : This function sends notification to the MSR        */
/*                        whenever a static route is deleted because of      */
/*                        interface deletion.                                */
/*                                                                           */
/*     INPUT            : u4ContextId    - Context-Id.                       */
/*                        pRt            - The static route entry            */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/

VOID
Rtm6CxtNotifyStaticRtDeletion (UINT4 u4ContextId, tIp6RtEntry * pRt)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tSNMP_OCTET_STRING_TYPE Dest;
    tSNMP_OCTET_STRING_TYPE RouteNextHop;
    tSNMP_OID_TYPE      RoutePolicy;
    UINT4               u4PrefixLen = 0;
    UINT4               u4RtPolicy[IP6_TWO];
    UINT1               au1Dest[IP6_ADDR_SIZE];
    UINT1               au1RouteHop[IP6_ADDR_SIZE];

    MEMSET (au1Dest, IP6_ZERO, IP6_ADDR_SIZE);
    MEMSET (au1RouteHop, IP6_ZERO, IP6_ADDR_SIZE);
    MEMSET (u4RtPolicy, IP6_ZERO, (IP6_TWO * sizeof (UINT4)));
    Dest.i4_Length = IP6_ADDR_SIZE;
    RouteNextHop.i4_Length = IP6_ADDR_SIZE;
    Dest.pu1_OctetList = au1Dest;
    RouteNextHop.pu1_OctetList = au1RouteHop;
    RoutePolicy.u4_Length = IP6_TWO;
    RoutePolicy.pu4_OidList = u4RtPolicy;

    MEMCPY (Dest.pu1_OctetList, &pRt->dst, IP6_ADDR_SIZE);
    MEMCPY (RouteNextHop.pu1_OctetList, &pRt->nexthop, IP6_ADDR_SIZE);
    u4PrefixLen = pRt->u1Prefixlen;

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIStdInetCidrRouteStatus, 0,
                          TRUE, NULL, NULL, 7, SNMP_SUCCESS);

    SNMP_MSR_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %s %u %o %i %s %i",
                          (INT4) u4ContextId, ADDR_TYPE_IPV6,
                          &Dest, u4PrefixLen, &RoutePolicy,
                          ADDR_TYPE_IPV6, &RouteNextHop, DESTROY));
    return;

}

/************************************************************************
 *  Function Name   : Rtm6UtilGetFirstFwdTableRtEntry
 *  Description     : Function to get the first entry of forwarding route table
 *  Input           : u4ContextId - context
 *                    pNetIpv6RtInfo - pointer to NetIpv6RtInfo
 *  Output          : None
 *  Returns         : RTM6_SUCCESS/RTM6_FAILURE
 ************************************************************************/

INT4
Rtm6UtilGetFirstFwdTableRtEntry (UINT4 u4ContextId,
                                 tNetIpv6RtInfo * pNetIpv6RtInfo)
{
    if (Rtm6ApiTrieGetFirstRtWithLeastNHInCxt (u4ContextId, pNetIpv6RtInfo)
        == RTM6_FAILURE)
    {
        /* No IPV6 Route present. */
        return RTM6_FAILURE;
    }

    return RTM6_SUCCESS;
}

/************************************************************************
 *  Function Name   : Rtm6UtilGetNextFwdTableRtEntry
 *  Description     : Function to get next of the given entry of forwarding route table
 *  Input           : u4ContextId - context
 *                    pFsipv6RouteDest - dest addr
 *                    i4Fsipv6RoutePfxLength - prefix length
 *                    i4Fsipv6RouteProtocol - protocol
 *                    pFsipv6RouteNextHop - next hop addr
 *                    pNetIpv6RtInfo - pointer to NetIpv6RtInfo
 *  Output          : None
 *  Returns         : RTM6_SUCCESS/RTM6_FAILURE
 ************************************************************************/

INT4
Rtm6UtilGetNextFwdTableRtEntry (UINT4 u4ContextId,
                                tIp6Addr * pFsipv6RouteDest,
                                INT4 i4Fsipv6RoutePfxLength,
                                INT4 i4Fsipv6RouteProtocol,
                                tIp6Addr * pFsipv6RouteNextHop,
                                tNetIpv6RtInfo * pNetIpv6RtInfo)
{
    /* Get the first route from the TRIE. */
    if (Rtm6ApiTrieGetNextRtWithLeastNHInCxt
        (u4ContextId, pFsipv6RouteDest,
         i4Fsipv6RoutePfxLength, i4Fsipv6RouteProtocol,
         pFsipv6RouteNextHop, pNetIpv6RtInfo) == RTM6_FAILURE)
    {
        /* No IPV6 Route present. */
        return RTM6_FAILURE;
    }
    return RTM6_SUCCESS;
}

/************************************************************************
 *  Function Name   : Rtm6UtilGetFirstInActiveTableRtEntry
 *  Description     : Function to get the first entry of inactive route table
 *  Input           : pNetIpv6RtInfo - pointer to NetIpv6RtInfo
 *  Output          : None
 *  Returns         : RTM6_SUCCESS/RTM6_FAILURE
 ************************************************************************/

INT4
Rtm6UtilGetFirstInActiveTableRtEntry (tNetIpv6RtInfo * pNetIpv6RtInfo)
{
    tRtm6Cxt           *pRtm6Cxt = NULL;
    tIp6RtEntry        *pIp6InvdRtEntry = NULL;
    UINT4               u4Context = IP6_ZERO;

    u4Context = (UINT4) UtilRtm6GetCurrCxtId ();

    pRtm6Cxt = UtilRtm6GetCxt (u4Context);
    if (pRtm6Cxt == NULL)
    {
        return RTM6_FAILURE;
    }
    pIp6InvdRtEntry =
        (tIp6RtEntry *) TMO_SLL_First (&(pRtm6Cxt->Rtm6InvdRtList));

    if (pIp6InvdRtEntry != NULL)
    {
        MEMCPY (&pNetIpv6RtInfo->Ip6Dst, &pIp6InvdRtEntry->dst, IP6_ADDR_SIZE);
        MEMCPY (&pNetIpv6RtInfo->NextHop, &pIp6InvdRtEntry->nexthop,
                IP6_ADDR_SIZE);
        pNetIpv6RtInfo->u1Prefixlen = pIp6InvdRtEntry->u1Prefixlen;
        pNetIpv6RtInfo->i1Proto = pIp6InvdRtEntry->i1Proto;
        return RTM6_SUCCESS;
    }
    else
    {
        return RTM6_FAILURE;
    }
}

/************************************************************************
 *  Function Name   : Rtm6UtilGetNextInActiveTableRtEntry
 *  Description     : Function to get next to the given entry of inactive route table
 *  Input           : pNetIpv6RtInfo - pointer to NetIpv6RtInfo
 *                    pIp6Addr - dest addr
 *                    i4PrefixLen - prefix length
 *                    pIp6NextHopAddr - next hop addr
 *  Output          : None
 *  Returns         : RTM6_SUCCESS/RTM6_FAILURE
 ************************************************************************/

INT4
Rtm6UtilGetNextInActiveTableRtEntry (tIp6Addr * pIp6Addr,
                                     INT4 i4PrefixLen, tIp6Addr *
                                     pIp6NextHopAddr,
                                     tNetIpv6RtInfo * pNetIpv6RtInfo)
{
    tRtm6Cxt           *pRtm6Cxt = NULL;
    tIp6RtEntry        *pIp6InvdRtEntry = NULL;
    tIp6RtEntry        *pTmpIp6RtEntry = NULL;
    tNetIpv6RtInfo      Rtm6InRtInfo;
    tNetIpv6RtInfo      TmpIp6RtInfo;
    tIp6Addr            Ip6InvdAddr;
    tIp6Addr            Ip6InvdNextHop;
    UINT4               u4Context = IP6_ZERO;
    UINT1               au1Ipaddr[IP6_ADDR_SIZE];

    MEMSET (&TmpIp6RtInfo, IP6_ZERO, sizeof (tNetIpv6RtInfo));
    MEMSET (&Rtm6InRtInfo, IP6_ZERO, sizeof (tNetIpv6RtInfo));
    MEMSET (&Ip6InvdAddr, 0, sizeof (tIp6Addr));
    MEMSET (&Ip6InvdNextHop, 0, sizeof (tIp6Addr));
    MEMSET (au1Ipaddr, IP6_UINT1_ALL_ONE, IP6_ADDR_SIZE);

    u4Context = (UINT4) UtilRtm6GetCurrCxtId ();

    pRtm6Cxt = UtilRtm6GetCxt (u4Context);

    if (pRtm6Cxt == NULL)
    {
        return RTM6_FAILURE;
    }
    if ((MEMCMP (pIp6Addr, au1Ipaddr, IP6_ADDR_SIZE)) == 0)
    {
        pIp6InvdRtEntry =
            (tIp6RtEntry *) TMO_SLL_First (&(pRtm6Cxt->Rtm6InvdRtList));
        if (pIp6InvdRtEntry != NULL)
        {
            MEMCPY (&pNetIpv6RtInfo->Ip6Dst, &pIp6InvdRtEntry->dst,
                    IP6_ADDR_SIZE);
            MEMCPY (&pNetIpv6RtInfo->NextHop, &pIp6InvdRtEntry->nexthop,
                    IP6_ADDR_SIZE);
            pNetIpv6RtInfo->u1Prefixlen = pIp6InvdRtEntry->u1Prefixlen;
            pNetIpv6RtInfo->i1Proto = pIp6InvdRtEntry->i1Proto;
            return RTM6_SUCCESS;
        }
    }
    else
    {
        MEMCPY (&Rtm6InRtInfo.Ip6Dst, pIp6Addr, sizeof (tIp6Addr));
        MEMCPY (&Rtm6InRtInfo.NextHop, pIp6NextHopAddr, sizeof (tIp6Addr));
        Rtm6InRtInfo.u1Prefixlen = (UINT1) i4PrefixLen;

        TMO_SLL_Scan (&(pRtm6Cxt->Rtm6InvdRtList), pTmpIp6RtEntry,
                      tIp6RtEntry *)
        {
            if (pTmpIp6RtEntry != NULL)
            {
                MEMCPY (&TmpIp6RtInfo.Ip6Dst, &pTmpIp6RtEntry->dst,
                        sizeof (tIp6Addr));
                MEMCPY (&TmpIp6RtInfo.NextHop, &pTmpIp6RtEntry->nexthop,
                        sizeof (tIp6Addr));
                TmpIp6RtInfo.u1Prefixlen = pTmpIp6RtEntry->u1Prefixlen;
                if (Rtm6UtilCheckIsRouteGreater (&TmpIp6RtInfo, &Rtm6InRtInfo)
                    == RTM6_SUCCESS)
                {
                    MEMCPY (&pNetIpv6RtInfo->Ip6Dst, &pTmpIp6RtEntry->dst,
                            IP6_ADDR_SIZE);
                    MEMCPY (&pNetIpv6RtInfo->NextHop, &pTmpIp6RtEntry->nexthop,
                            IP6_ADDR_SIZE);
                    pNetIpv6RtInfo->u1Prefixlen = pTmpIp6RtEntry->u1Prefixlen;
                    pNetIpv6RtInfo->i1Proto = pTmpIp6RtEntry->i1Proto;
                    return RTM6_SUCCESS;
                }
            }
        }
    }
    return RTM6_FAILURE;
}

/************************************************************************
 *  Function Name   : Rtm6UtilGetInActiveTableRtEntry
 *  Description     : Function to get the given entry of inactive route table
 *  Input           : pNetIpv6RtInfo - pointer to NetIpv6RtInfo
 *                    pIp6Addr - dest addr
 *                    i4PrefixLen - prefix length
 *                    pIp6NextHopAddr - next hop addr
 *  Output          : None
 *  Returns         : RTM6_SUCCESS/RTM6_FAILURE
 ************************************************************************/

INT4
Rtm6UtilGetInActiveTableRtEntry (tIp6Addr * pIp6Addr,
                                 INT4 i4PrefixLen, tIp6Addr *
                                 pIp6NextHopAddr,
                                 tNetIpv6RtInfo * pNetIpv6RtInfo)
{
    tNetIpv6RtInfo      NetIp6RtInfo;
    tIp6RtEntry        *pIp6InvdRtEntry = NULL;
    tRtm6Cxt           *pRtm6Cxt = NULL;
    UINT4               u4Context = IP6_ZERO;

    MEMSET (&NetIp6RtInfo, 0, sizeof (tNetIpv6RtInfo));

    u4Context = (UINT4) UtilRtm6GetCurrCxtId ();
    pRtm6Cxt = UtilRtm6GetCxt (u4Context);

    if (pRtm6Cxt == NULL)
    {
        return RTM6_FAILURE;
    }

    TMO_SLL_Scan (&(pRtm6Cxt->Rtm6InvdRtList), pIp6InvdRtEntry, tIp6RtEntry *)
    {
        if ((Ip6AddrMatch (&(pIp6InvdRtEntry->dst),
                           pIp6Addr, i4PrefixLen) == TRUE) &&
            (Ip6AddrMatch (&(pIp6InvdRtEntry->nexthop),
                           pIp6NextHopAddr, IPVX_IPV6_ADDR_LEN) == TRUE))
        {
            MEMCPY (&pNetIpv6RtInfo->Ip6Dst, &pIp6InvdRtEntry->dst,
                    IP6_ADDR_SIZE);
            MEMCPY (&pNetIpv6RtInfo->NextHop, &pIp6InvdRtEntry->nexthop,
                    IP6_ADDR_SIZE);
            pNetIpv6RtInfo->u1Prefixlen = pIp6InvdRtEntry->u1Prefixlen;
            pNetIpv6RtInfo->i1Proto = pIp6InvdRtEntry->i1Proto;
            pNetIpv6RtInfo->u4Index = pIp6InvdRtEntry->u4Index;
            pNetIpv6RtInfo->u4Metric = pIp6InvdRtEntry->u4Metric;
            pNetIpv6RtInfo->u4RouteTag = pIp6InvdRtEntry->u4RouteTag;

            pNetIpv6RtInfo->i1Type = pIp6InvdRtEntry->i1Type;
            pNetIpv6RtInfo->i1DefRtrFlag = pIp6InvdRtEntry->i1DefRtrFlag;
            pNetIpv6RtInfo->u4RowStatus = pIp6InvdRtEntry->u4RowStatus;
            pNetIpv6RtInfo->u1Preference = pIp6InvdRtEntry->u1Preference;
            pNetIpv6RtInfo->u1MetricType = pIp6InvdRtEntry->u1MetricType;
            pNetIpv6RtInfo->u1AddrType = pIp6InvdRtEntry->u1AddrType;

            return RTM6_SUCCESS;
        }
    }
    return RTM6_FAILURE;
}

/*
 ******************************************************************************
 * Function           : Rtm6UpdateRouteInInvdRouteList
 * Input(s)           : pNetIp6RtInfo - Static route details to be added in invalid list
 * Output(s)          : None
 * Returns            : NONE
 * Action             : This function adds the static route to Invalid route list.
 *         
 ******************************************************************************
 */
INT4
Rtm6UpdateRouteInInvdRouteList (tIp6Addr * pIp6Addr,
                                INT4 i4PrefixLen,
                                tIp6Addr * pIp6NextHopAddr,
                                tNetIpv6RtInfo * pNetIp6RtInfo)
{
    tRtm6Cxt           *pRtm6Cxt = NULL;
    tIp6RtEntry        *pIp6InvdRtEntry = NULL;
    tIp6Addr            Ip6InvdAddr;
    tIp6Addr            Ip6InvdNextHop;
    UINT4               u4Context = IP6_ZERO;
    UINT1               au1Ipaddr[IP6_ADDR_SIZE];

    MEMSET (&Ip6InvdAddr, 0, sizeof (tIp6Addr));
    MEMSET (&Ip6InvdNextHop, 0, sizeof (tIp6Addr));
    MEMSET (au1Ipaddr, IP6_UINT1_ALL_ONE, IP6_ADDR_SIZE);

    u4Context = (UINT4) UtilRtm6GetCurrCxtId ();
    pRtm6Cxt = UtilRtm6GetCxt (u4Context);

    if (pRtm6Cxt == NULL)
    {
        return RTM6_FAILURE;
    }

    TMO_SLL_Scan (&(pRtm6Cxt->Rtm6InvdRtList), pIp6InvdRtEntry, tIp6RtEntry *)
    {
        MEMCPY (&Ip6InvdAddr, &pIp6InvdRtEntry->dst, IP6_ADDR_SIZE);
        MEMCPY (&Ip6InvdNextHop, &pIp6InvdRtEntry->nexthop, IP6_ADDR_SIZE);
        if ((Ip6AddrMatch (&Ip6InvdAddr, pIp6Addr, i4PrefixLen) == TRUE) &&
            (Ip6AddrMatch (&Ip6InvdNextHop, pIp6NextHopAddr, IPVX_IPV6_ADDR_LEN)
             == TRUE))
        {
            if (pNetIp6RtInfo->u4Index != 0)
            {
                pIp6InvdRtEntry->u4Index = pNetIp6RtInfo->u4Index;
            }
            if (pNetIp6RtInfo->u4Metric != 0)
            {
                pIp6InvdRtEntry->u4Metric = pNetIp6RtInfo->u4Metric;
            }
            if (pNetIp6RtInfo->i1Type != 0)
            {
                pIp6InvdRtEntry->i1Type = pNetIp6RtInfo->i1Type;
            }
            if (pNetIp6RtInfo->u4RouteTag != 0)
            {
                pIp6InvdRtEntry->u4RouteTag = pNetIp6RtInfo->u4RouteTag;
            }
            if (pNetIp6RtInfo->u1AddrType != 0)
            {
                pIp6InvdRtEntry->u1AddrType = pNetIp6RtInfo->u1AddrType;
            }
            if (pNetIp6RtInfo->u1Preference != 0)
            {
                pIp6InvdRtEntry->u1Preference = pNetIp6RtInfo->u1Preference;
            }

            return RTM6_SUCCESS;
        }
    }
    return RTM6_FAILURE;
}

/*
 ******************************************************************************
 * Function           : Rtm6DeleteInvdRouteNode
 * Input(s)           : pIp6Addr - dest addr
 *                      i4PrefixLen - prefix length
 *                      pIp6NextHopAddr - next hop addr
 * Output(s)          : None
 * Returns            : NONE
 * Action             : This function to delete the given node to Invalid route list.
 *
 ******************************************************************************
 */

INT4
Rtm6DeleteInvdRouteNode (tIp6Addr * pIp6Addr,
                         INT4 i4PrefixLen, tIp6Addr * pIp6NextHopAddr)
{
    tNetIpv6RtInfo      NetIp6RtInfo;
    tIp6RtEntry        *pIp6InvdRtEntry = NULL;
    tRtm6Cxt           *pRtm6Cxt = NULL;
    UINT4               u4Context = IP6_ZERO;

    MEMSET (&NetIp6RtInfo, 0, sizeof (tNetIpv6RtInfo));

    u4Context = (UINT4) UtilRtm6GetCurrCxtId ();
    pRtm6Cxt = UtilRtm6GetCxt (u4Context);

    if (pRtm6Cxt == NULL)
    {
        return RTM6_FAILURE;
    }

    TMO_SLL_Scan (&(pRtm6Cxt->Rtm6InvdRtList), pIp6InvdRtEntry, tIp6RtEntry *)
    {
        if ((Ip6AddrMatch (&(pIp6InvdRtEntry->dst),
                           pIp6Addr, i4PrefixLen) == TRUE) &&
            (Ip6AddrMatch (&(pIp6InvdRtEntry->nexthop),
                           pIp6NextHopAddr, IPVX_IPV6_ADDR_LEN) == TRUE))
        {
            TMO_SLL_Delete (&(pRtm6Cxt->Rtm6InvdRtList),
                            &(pIp6InvdRtEntry->NextEntry));
            IP6_RT_FREE (pIp6InvdRtEntry);
            break;
        }
    }
    return RTM6_SUCCESS;
}

/*****************************************************************************
 * Function     : RtmUtilHandleProtocolShutdown                              *
 * Description  : Scans PRT for the Resolved NextHop and deletes them        *
 * Input        : u4NextHopAddr - NextHop Address                            *
 *                i1Flag - Indicates state of the ARP                        *
 *                u4ContextId - RTM Context ID                               *
 *                u1Operation - Inicates whether ARP addition/deletion       *
 * Output       : None                                                       *
 * Returns      : IP_SUCCESS/IP_FAILURE                                      *
 *****************************************************************************/
INT4
Rtm6UtilHandleProtocolShutdown (UINT4 u4ContextId, UINT1 u1Protocol)
{
    tRtm6RegnId         RegnId;
    tRtm6Cxt           *pRtm6Cxt = NULL;
    MEMSET (&RegnId, 0, sizeof (tRtm6RegnId));

    pRtm6Cxt = UtilRtm6GetCxt (u4ContextId);
    if (pRtm6Cxt == NULL)
    {
        return RTM6_FAILURE;
    }
    RegnId.u2ProtoId = (UINT2) u1Protocol;
    RegnId.u4ContextId = u4ContextId;
    Rtm6ClearProtoSpecificRouteFromTrieInCxt (pRtm6Cxt,
                                              (UINT1) RegnId.u2ProtoId);
    return RTM6_SUCCESS;
}

/**************************************************************************/
/*   Function Name   : Rtm6ClearProtoIfRouteFromTrie                         */
/*   Description     : This function will remove all the route learnt     */
/*                     from the given protocol IfIndexfrom the TRIE.             */
/*   Input(s)        : pRegnID - Protocol Info whose route need to cleared*/
/*                   : pRtmCxt - Rtm Context Pointer                      */
/*   Output(s)       : None                                               */
/*   Return Value    : None                                               */
/**************************************************************************/
VOID
Rtm6ClearProtoSpecificRouteFromTrieInCxt (tRtm6Cxt * pRtm6Cxt, UINT1 u1Protocol)
{

    tIp6Addr            AddrMask;
    tIp6RtEntry        *pRtEntry = NULL;
    tIp6RtEntry        *pAltRtEntry = NULL;
    tIp6RtEntry        *pTempRtEntry = NULL;
    tIp6RtEntry         Ip6RtEntry;
    tIp6RtEntry        *pIp6RtEntry = NULL;
    VOID               *pRibNode = NULL;
    VOID               *pProtoRibNode = NULL;
    tInputParams        InParams;
    INT4                i4Status = TRIE_FAILURE;
    UINT1               au1TrieKey[RTM6_MAX_TRIE_KEY_LEN];

    MEMSET (au1TrieKey, IP6_ZERO, RTM6_MAX_TRIE_KEY_LEN);
    MEMSET ((UINT1 *) &AddrMask, 0xFF, sizeof (tIp6Addr));

    InParams.pRoot = pRtm6Cxt->pIp6RtTblRoot;
    InParams.i1AppId = (INT1) (u1Protocol - 1);
    InParams.pLeafNode = NULL;
    i4Status = TrieGetFirstNode (&InParams, (VOID *) &pRtEntry, &pRibNode);

    InParams.Key.pKey = au1TrieKey;
    MEMSET (InParams.Key.pKey, 0, RTM6_MAX_TRIE_KEY_LEN);

    while ((i4Status != TRIE_FAILURE) && (pRtEntry != NULL))
    {
        Ip6CopyAddrBits ((tIp6Addr *) (VOID *) InParams.Key.pKey,
                         &pRtEntry->dst, pRtEntry->u1Prefixlen);
        Ip6CopyAddrBits ((tIp6Addr *) (VOID *)
                         (InParams.Key.pKey + IP6_ADDR_SIZE),
                         &AddrMask, pRtEntry->u1Prefixlen);
        InParams.u1PrefixLen = (UINT1) pRtEntry->u1Prefixlen;

        pTempRtEntry = NULL;
        i4Status =
            TrieGetNextNode (&InParams, pRibNode, (VOID *) &pTempRtEntry,
                             &pRibNode);

        /* To handle ECMP case */
        do
        {
            /* scan all the alternate path and delete in case of ecmp */
            pAltRtEntry = pRtEntry->pNextAlternatepath;
            if (pRtEntry->i1Proto == (INT1) u1Protocol)
            {
                Ip6ForwardingTableDeleteRouteInCxt (pRtm6Cxt, pRtEntry);
            }
            else
            {
                /*Deleting non best routes */
                MEMSET (&Ip6RtEntry, 0, sizeof (tIp6RtEntry));

                MEMCPY (&Ip6RtEntry.dst, &pRtEntry->dst, IP6_ADDR_SIZE);
                Ip6RtEntry.u1Prefixlen = pRtEntry->u1Prefixlen;
                Ip6RtEntry.i1Proto = (INT1) u1Protocol;

                if (Rtm6TrieSearchExactEntryInCxt
                    (pRtm6Cxt, &Ip6RtEntry, &pIp6RtEntry,
                     &pProtoRibNode) == RTM6_SUCCESS)
                {
                    if (pIp6RtEntry->i1Proto == (INT1) u1Protocol)
                    {
                        Ip6ForwardingTableDeleteRouteInCxt (pRtm6Cxt,
                                                            pIp6RtEntry);
                    }
                }
            }

            pRtEntry = pAltRtEntry;
        }
        while (pRtEntry != NULL);

        pRtEntry = pTempRtEntry;
    }

    return;
}

/*****************************************************************************
 * Function     : Rtm6UtilHandleProtocolIfShutdown                              *
 * Description  : Scans PRT for the Resolved NextHop and deletes them        *
 * Input        : u4NextHopAddr - NextHop Address                            *
 *                i1Flag - Indicates state of the ARP                        *
 *                u4ContextId - RTM Context ID                               *
 *                u1Operation - Inicates whether ARP addition/deletion       *
 * Output       : None                                                       *
 * Returns      : RTM6_SUCCESS / RTM6_FAILURE                                *
 *****************************************************************************/
INT4
Rtm6UtilHandleProtocolIfShutdown (UINT4 u4ContextId, UINT1 u1Protocol,
                                  UINT4 u4IfIndex)
{
    tRtm6Cxt           *pRtm6Cxt = NULL;

    pRtm6Cxt = UtilRtm6GetCxt (u4ContextId);
    if (pRtm6Cxt == NULL)
    {
        return RTM6_FAILURE;
    }
    Rtm6ClearProtoIfRouteFromTrieInCxt (pRtm6Cxt, u1Protocol,
                                        (UINT2) u4IfIndex);
    return RTM6_SUCCESS;
}

/**************************************************************************/
/*   Function Name   : Rtm6ClearProtoIfRouteFromTrie                         */
/*   Description     : This function will remove all the route learnt     */
/*                     from the given protocol IfIndexfrom the TRIE.             */
/*   Input(s)        : pRegnID - Protocol Info whose route need to cleared*/
/*                   : pRtmCxt - Rtm Context Pointer                      */
/*   Output(s)       : None                                               */
/*   Return Value    : None                                               */
/**************************************************************************/
VOID
Rtm6ClearProtoIfRouteFromTrieInCxt (tRtm6Cxt * pRtm6Cxt, UINT1 u1Protocol,
                                    UINT2 u2Index)
{

    tIp6Addr            AddrMask;
    tIp6RtEntry        *pRtEntry = NULL;
    tIp6RtEntry        *pAltRtEntry = NULL;
    tIp6RtEntry        *pTempRtEntry = NULL;
    tIp6RtEntry         Ip6RtEntry;
    tIp6RtEntry        *pIp6RtEntry = NULL;
    VOID               *pRibNode = NULL;
    VOID               *pProtoRibNode = NULL;
    tInputParams        InParams;
    INT4                i4Status = TRIE_FAILURE;
    INT4                i4RetVal = TRIE_FAILURE;
    UINT1               au1TrieKey[RTM6_MAX_TRIE_KEY_LEN];

    MEMSET (au1TrieKey, IP6_ZERO, RTM6_MAX_TRIE_KEY_LEN);
    MEMSET ((UINT1 *) &AddrMask, 0xFF, sizeof (tIp6Addr));

    InParams.pRoot = pRtm6Cxt->pIp6RtTblRoot;
    InParams.i1AppId = (INT1) (u1Protocol - 1);
    InParams.pLeafNode = NULL;
    i4Status = TrieGetFirstNode (&InParams, (VOID *) &pRtEntry, &pRibNode);

    InParams.Key.pKey = au1TrieKey;
    MEMSET (InParams.Key.pKey, 0, RTM6_MAX_TRIE_KEY_LEN);

    while ((i4Status != TRIE_FAILURE) && (pRtEntry != NULL))
    {
        Ip6CopyAddrBits ((tIp6Addr *) (VOID *) InParams.Key.pKey,
                         &pRtEntry->dst, pRtEntry->u1Prefixlen);
        Ip6CopyAddrBits ((tIp6Addr *) (VOID *)
                         (InParams.Key.pKey + IP6_ADDR_SIZE),
                         &AddrMask, pRtEntry->u1Prefixlen);
        InParams.u1PrefixLen = (UINT1) pRtEntry->u1Prefixlen;

        pTempRtEntry = NULL;
        i4Status =
            TrieGetNextNode (&InParams, pRibNode, (VOID *) &pTempRtEntry,
                             &pRibNode);

        /* To handle ECMP case */
        do
        {
            /* scan all the alternate path and delete in case of ecmp */
            pAltRtEntry = pRtEntry->pNextAlternatepath;
            if ((pRtEntry->i1Proto == (INT1) u1Protocol) &&
                (pRtEntry->u4Index == (UINT4) u2Index))
            {
                i4RetVal =
                    Ip6ForwardingTableDeleteRouteInCxt (pRtm6Cxt, pRtEntry);
            }
            else
            {
                /*Deleting non best routes */
                MEMSET (&Ip6RtEntry, 0, sizeof (tIp6RtEntry));

                MEMCPY (&Ip6RtEntry.dst, &pRtEntry->dst, IP6_ADDR_SIZE);
                Ip6RtEntry.u1Prefixlen = pRtEntry->u1Prefixlen;
                Ip6RtEntry.i1Proto = (INT1) u1Protocol;

                if (Rtm6TrieSearchExactEntryInCxt
                    (pRtm6Cxt, &Ip6RtEntry, &pIp6RtEntry,
                     &pProtoRibNode) == RTM6_SUCCESS)
                {
                    if ((pIp6RtEntry->i1Proto == (INT1) u1Protocol) &&
                        ((pIp6RtEntry->u4Index == (UINT4) u2Index)))
                    {
                        i4RetVal =
                            Ip6ForwardingTableDeleteRouteInCxt (pRtm6Cxt,
                                                                pIp6RtEntry);
                    }
                }
            }

            pRtEntry = pAltRtEntry;
        }
        while (pRtEntry != NULL);

        pRtEntry = pTempRtEntry;
    }
    UNUSED_PARAM (i4RetVal);
    return;
}
