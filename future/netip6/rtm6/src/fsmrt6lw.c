/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmrt6lw.c,v 1.6 2015/10/19 12:22:10 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "lr.h"
# include  "fssnmp.h"
# include  "fsmrt6lw.h"
#include "rtm6inc.h"
# include  "fsrtmlow.h"
# include  "fsmirt6cli.h"

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIRtm6GlobalTrace
 Input       :  The Indices

                The Object 
                retValFsMIRtm6GlobalTrace
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRtm6GlobalTrace (UINT4 *pu4RetValFsMIRtm6GlobalTrace)
{
    *pu4RetValFsMIRtm6GlobalTrace = gRtm6GlobalInfo.u4GlobalTrcFlag;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIRtm6ThrotLimit
 Input       :  The Indices

                The Object 
                retValFsMIRtm6ThrotLimit
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRtm6ThrotLimit (UINT4 *pu4RetValFsMIRtm6ThrotLimit)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return = nmhGetFsRrd6ThrotLimit (pu4RetValFsMIRtm6ThrotLimit);
    return i1Return;
}


/****************************************************************************
 Function    :  nmhGetFsMIRtm6MaximumBgpRoutes
 Input       :  The Indices

                The Object 
                retValFsMIRtm6MaximumBgpRoutes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsMIRtm6MaximumBgpRoutes(UINT4 *pu4RetValFsMIRtm6MaximumBgpRoutes)
{

     return nmhGetFsRrd6MaximumBgpRoutes(pu4RetValFsMIRtm6MaximumBgpRoutes);

}
/****************************************************************************
 Function    :  nmhGetFsMIRtm6MaximumOspfRoutes
 Input       :  The Indices

                The Object 
                retValFsMIRtm6MaximumOspfRoutes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsMIRtm6MaximumOspfRoutes(UINT4 *pu4RetValFsMIRtm6MaximumOspfRoutes)
{

   return nmhGetFsRrd6MaximumOspfRoutes(pu4RetValFsMIRtm6MaximumOspfRoutes);

}
/****************************************************************************
 Function    :  nmhGetFsMIRtm6MaximumRipRoutes
 Input       :  The Indices

                The Object 
                retValFsMIRtm6MaximumRipRoutes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsMIRtm6MaximumRipRoutes(UINT4 *pu4RetValFsMIRtm6MaximumRipRoutes)
{

  return nmhGetFsRrd6MaximumRipRoutes(pu4RetValFsMIRtm6MaximumRipRoutes);

}
/****************************************************************************
 Function    :  nmhGetFsMIRtm6MaximumStaticRoutes
 Input       :  The Indices

                The Object 
                retValFsMIRtm6MaximumStaticRoutes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsMIRtm6MaximumStaticRoutes(UINT4 *pu4RetValFsMIRtm6MaximumStaticRoutes)
{

   return nmhGetFsRrd6MaximumStaticRoutes (pu4RetValFsMIRtm6MaximumStaticRoutes);

}
/****************************************************************************
 Function    :  nmhGetFsMIRtm6MaximumISISRoutes
 Input       :  The Indices

                The Object 
                retValFsMIRtm6MaximumISISRoutes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsMIRtm6MaximumISISRoutes(UINT4 *pu4RetValFsMIRtm6MaximumISISRoutes)
{

   return nmhGetFsRrd6MaximumISISRoutes (pu4RetValFsMIRtm6MaximumISISRoutes);

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIRtm6GlobalTrace
 Input       :  The Indices

                The Object 
                setValFsMIRtm6GlobalTrace
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRtm6GlobalTrace (UINT4 u4SetValFsMIRtm6GlobalTrace)
{

    gRtm6GlobalInfo.u4GlobalTrcFlag = u4SetValFsMIRtm6GlobalTrace;
    UtilRtm6IncMsrForRtm6Scalar (u4SetValFsMIRtm6GlobalTrace,
                                 FsMIRtm6GlobalTrace,
                                 (sizeof (FsMIRtm6GlobalTrace) /
                                  sizeof (UINT4)));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMIRtm6ThrotLimit
 Input       :  The Indices

                The Object 
                setValFsMIRtm6ThrotLimit
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRtm6ThrotLimit (UINT4 u4SetValFsMIRtm6ThrotLimit)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return = nmhSetFsRrd6ThrotLimit (u4SetValFsMIRtm6ThrotLimit);
    return i1Return;
}



/****************************************************************************
 Function    :  nmhSetFsMIRtm6MaximumBgpRoutes
 Input       :  The Indices

                The Object 
                setValFsMIRtm6MaximumBgpRoutes
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsMIRtm6MaximumBgpRoutes(UINT4 u4SetValFsMIRtm6MaximumBgpRoutes)
{

 return nmhSetFsRrd6MaximumBgpRoutes (u4SetValFsMIRtm6MaximumBgpRoutes);

}
/****************************************************************************
 Function    :  nmhSetFsMIRtm6MaximumOspfRoutes
 Input       :  The Indices

                The Object 
                setValFsMIRtm6MaximumOspfRoutes
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsMIRtm6MaximumOspfRoutes(UINT4 u4SetValFsMIRtm6MaximumOspfRoutes)
{

  return nmhSetFsRrd6MaximumOspfRoutes(u4SetValFsMIRtm6MaximumOspfRoutes);

}
/****************************************************************************
 Function    :  nmhSetFsMIRtm6MaximumRipRoutes
 Input       :  The Indices

                The Object 
                setValFsMIRtm6MaximumRipRoutes
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsMIRtm6MaximumRipRoutes(UINT4 u4SetValFsMIRtm6MaximumRipRoutes)
{

  return nmhSetFsRrd6MaximumRipRoutes(u4SetValFsMIRtm6MaximumRipRoutes);

}
/****************************************************************************
 Function    :  nmhSetFsMIRtm6MaximumStaticRoutes
 Input       :  The Indices

                The Object 
                setValFsMIRtm6MaximumStaticRoutes
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsMIRtm6MaximumStaticRoutes(UINT4 u4SetValFsMIRtm6MaximumStaticRoutes)
{

  return nmhSetFsRrd6MaximumStaticRoutes(u4SetValFsMIRtm6MaximumStaticRoutes);

}
/****************************************************************************
 Function    :  nmhSetFsMIRtm6MaximumISISRoutes
 Input       :  The Indices

                The Object 
                setValFsMIRtm6MaximumISISRoutes
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsMIRtm6MaximumISISRoutes(UINT4 u4SetValFsMIRtm6MaximumISISRoutes)
{

  return nmhSetFsRrd6MaximumISISRoutes(u4SetValFsMIRtm6MaximumISISRoutes);

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIRtm6GlobalTrace
 Input       :  The Indices

                The Object 
                testValFsMIRtm6GlobalTrace
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRtm6GlobalTrace (UINT4 *pu4ErrorCode,
                              UINT4 u4TestValFsMIRtm6GlobalTrace)
{
    UNUSED_PARAM (u4TestValFsMIRtm6GlobalTrace);
    UNUSED_PARAM (pu4ErrorCode);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIRtm6ThrotLimit
 Input       :  The Indices

                The Object 
                testValFsMIRtm6ThrotLimit
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRtm6ThrotLimit (UINT4 *pu4ErrorCode,
                             UINT4 u4TestValFsMIRtm6ThrotLimit)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhTestv2FsRrd6ThrotLimit (pu4ErrorCode, u4TestValFsMIRtm6ThrotLimit);
    return i1Return;
}


/****************************************************************************
 Function    :  nmhTestv2FsMIRtm6MaximumBgpRoutes
 Input       :  The Indices

                The Object 
                testValFsMIRtm6MaximumBgpRoutes
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsMIRtm6MaximumBgpRoutes(UINT4 *pu4ErrorCode , UINT4 u4TestValFsMIRtm6MaximumBgpRoutes)
{

  return nmhTestv2FsRrd6MaximumBgpRoutes(pu4ErrorCode,u4TestValFsMIRtm6MaximumBgpRoutes);

}
/****************************************************************************
 Function    :  nmhTestv2FsMIRtm6MaximumOspfRoutes
 Input       :  The Indices

                The Object 
                testValFsMIRtm6MaximumOspfRoutes
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsMIRtm6MaximumOspfRoutes(UINT4 *pu4ErrorCode , UINT4 u4TestValFsMIRtm6MaximumOspfRoutes)
{

  return nmhTestv2FsRrd6MaximumOspfRoutes(pu4ErrorCode,u4TestValFsMIRtm6MaximumOspfRoutes);

}
/****************************************************************************
 Function    :  nmhTestv2FsMIRtm6MaximumRipRoutes
 Input       :  The Indices

                The Object 
                testValFsMIRtm6MaximumRipRoutes
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsMIRtm6MaximumRipRoutes(UINT4 *pu4ErrorCode , UINT4 u4TestValFsMIRtm6MaximumRipRoutes)
{

   return nmhTestv2FsRrd6MaximumRipRoutes(pu4ErrorCode,u4TestValFsMIRtm6MaximumRipRoutes);

}
/****************************************************************************
 Function    :  nmhTestv2FsMIRtm6MaximumStaticRoutes
 Input       :  The Indices

                The Object 
                testValFsMIRtm6MaximumStaticRoutes
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsMIRtm6MaximumStaticRoutes(UINT4 *pu4ErrorCode , UINT4 u4TestValFsMIRtm6MaximumStaticRoutes)
{

  return nmhTestv2FsRrd6MaximumStaticRoutes(pu4ErrorCode,u4TestValFsMIRtm6MaximumStaticRoutes);

}
/****************************************************************************
 Function    :  nmhTestv2FsMIRtm6MaximumISISRoutes
 Input       :  The Indices

                The Object 
                testValFsMIRtm6MaximumISISRoutes
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsMIRtm6MaximumISISRoutes(UINT4 *pu4ErrorCode , UINT4 u4TestValFsMIRtm6MaximumISISRoutes)
{

   return nmhTestv2FsRrd6MaximumISISRoutes(pu4ErrorCode,u4TestValFsMIRtm6MaximumISISRoutes);
  
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIRtm6GlobalTrace
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIRtm6GlobalTrace (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMIRtm6ThrotLimit
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIRtm6ThrotLimit (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMIRtm6MaximumBgpRoutes
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FsMIRtm6MaximumBgpRoutes(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhDepv2FsMIRtm6MaximumOspfRoutes
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FsMIRtm6MaximumOspfRoutes(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhDepv2FsMIRtm6MaximumRipRoutes
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FsMIRtm6MaximumRipRoutes(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhDepv2FsMIRtm6MaximumStaticRoutes
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FsMIRtm6MaximumStaticRoutes(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhDepv2FsMIRtm6MaximumISISRoutes
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FsMIRtm6MaximumISISRoutes(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}
/* LOW LEVEL Routines for Table : FsMIRtm6Table. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIRtm6Table
 Input       :  The Indices
                FsMIRtm6ContextId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIRtm6Table (INT4 i4FsMIRtm6ContextId)
{
    if ((i4FsMIRtm6ContextId < RTM6_DEFAULT_CXT_ID) ||
        (i4FsMIRtm6ContextId >= (INT4) gRtm6GlobalInfo.u4MaxContextLimit))
    {
        return SNMP_FAILURE;
    }

    if (UtilRtm6VcmIsVcExist (i4FsMIRtm6ContextId) == RTM6_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (UtilRtm6IsValidCxtId (i4FsMIRtm6ContextId) == RTM6_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIRtm6Table
 Input       :  The Indices
                FsMIRtm6ContextId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIRtm6Table (INT4 *pi4FsMIRtm6ContextId)
{
    if (UtilRtm6GetFirstCxtId ((UINT4 *) pi4FsMIRtm6ContextId) == RTM6_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIRtm6Table
 Input       :  The Indices
                FsMIRtm6ContextId
                nextFsMIRtm6ContextId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIRtm6Table (INT4 i4FsMIRtm6ContextId,
                              INT4 *pi4NextFsMIRtm6ContextId)
{
    if (UtilRtm6GetNextCxtId ((UINT4) i4FsMIRtm6ContextId,
                              (UINT4 *) pi4NextFsMIRtm6ContextId) ==
        RTM6_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIRrd6RouterId
 Input       :  The Indices
                FsMIRtm6ContextId

                The Object 
                retValFsMIRrd6RouterId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRrd6RouterId (INT4 i4FsMIRtm6ContextId,
                        UINT4 *pu4RetValFsMIRrd6RouterId)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtm6SetContext (i4FsMIRtm6ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFsRrd6RouterId (pu4RetValFsMIRrd6RouterId);
    UtilRtm6ResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIRrd6FilterByOspfTag
 Input       :  The Indices
                FsMIRtm6ContextId

                The Object 
                retValFsMIRrd6FilterByOspfTag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRrd6FilterByOspfTag (INT4 i4FsMIRtm6ContextId,
                               INT4 *pi4RetValFsMIRrd6FilterByOspfTag)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtm6SetContext (i4FsMIRtm6ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFsRrd6FilterByOspfTag (pi4RetValFsMIRrd6FilterByOspfTag);
    UtilRtm6ResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIRrd6FilterOspfTag
 Input       :  The Indices
                FsMIRtm6ContextId

                The Object 
                retValFsMIRrd6FilterOspfTag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRrd6FilterOspfTag (INT4 i4FsMIRtm6ContextId,
                             INT4 *pi4RetValFsMIRrd6FilterOspfTag)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtm6SetContext (i4FsMIRtm6ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFsRrd6FilterOspfTag (pi4RetValFsMIRrd6FilterOspfTag);
    UtilRtm6ResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIRrd6FilterOspfTagMask
 Input       :  The Indices
                FsMIRtm6ContextId

                The Object 
                retValFsMIRrd6FilterOspfTagMask
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRrd6FilterOspfTagMask (INT4 i4FsMIRtm6ContextId,
                                 INT4 *pi4RetValFsMIRrd6FilterOspfTagMask)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtm6SetContext (i4FsMIRtm6ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFsRrd6FilterOspfTagMask (pi4RetValFsMIRrd6FilterOspfTagMask);
    UtilRtm6ResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIRrd6RouterASNumber
 Input       :  The Indices
                FsMIRtm6ContextId

                The Object 
                retValFsMIRrd6RouterASNumber
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRrd6RouterASNumber (INT4 i4FsMIRtm6ContextId,
                              INT4 *pi4RetValFsMIRrd6RouterASNumber)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtm6SetContext (i4FsMIRtm6ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFsRrd6RouterASNumber (pi4RetValFsMIRrd6RouterASNumber);
    UtilRtm6ResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIRrd6AdminStatus
 Input       :  The Indices
                FsMIRtm6ContextId

                The Object 
                retValFsMIRrd6AdminStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRrd6AdminStatus (INT4 i4FsMIRtm6ContextId,
                           INT4 *pi4RetValFsMIRrd6AdminStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtm6SetContext (i4FsMIRtm6ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFsRrd6AdminStatus (pi4RetValFsMIRrd6AdminStatus);
    UtilRtm6ResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIRrd6Trace
 Input       :  The Indices
                FsMIRtm6ContextId

                The Object 
                retValFsMIRrd6Trace
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRrd6Trace (INT4 i4FsMIRtm6ContextId, UINT4 *pu4RetValFsMIRrd6Trace)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtm6SetContext (i4FsMIRtm6ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFsRrd6Trace (pu4RetValFsMIRrd6Trace);
    UtilRtm6ResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIRtm6StaticRouteDistance
 Input       :  The Indices
                FsMIRtm6ContextId

                The Object
                retValFsMIRtm6StaticRouteDistance
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsMIRtm6StaticRouteDistance(INT4 i4FsMIRtm6ContextId , INT4 *pi4RetValFsMIRtm6StaticRouteDistance)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtm6SetContext ((UINT4)i4FsMIRtm6ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFsRtm6StaticRouteDistance (pi4RetValFsMIRtm6StaticRouteDistance);
    UtilRtm6ResetContext ();
    return i1Return;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIRrd6RouterId
 Input       :  The Indices
                FsMIRtm6ContextId

                The Object 
                setValFsMIRrd6RouterId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRrd6RouterId (INT4 i4FsMIRtm6ContextId,
                        UINT4 u4SetValFsMIRrd6RouterId)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtm6SetContext (i4FsMIRtm6ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFsRrd6RouterId (u4SetValFsMIRrd6RouterId);
    UtilRtm6ResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIRrd6FilterByOspfTag
 Input       :  The Indices
                FsMIRtm6ContextId

                The Object 
                setValFsMIRrd6FilterByOspfTag
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRrd6FilterByOspfTag (INT4 i4FsMIRtm6ContextId,
                               INT4 i4SetValFsMIRrd6FilterByOspfTag)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtm6SetContext (i4FsMIRtm6ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFsRrd6FilterByOspfTag (i4SetValFsMIRrd6FilterByOspfTag);
    UtilRtm6ResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIRrd6FilterOspfTag
 Input       :  The Indices
                FsMIRtm6ContextId

                The Object 
                setValFsMIRrd6FilterOspfTag
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRrd6FilterOspfTag (INT4 i4FsMIRtm6ContextId,
                             INT4 i4SetValFsMIRrd6FilterOspfTag)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtm6SetContext (i4FsMIRtm6ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFsRrd6FilterOspfTag (i4SetValFsMIRrd6FilterOspfTag);
    UtilRtm6ResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIRrd6FilterOspfTagMask
 Input       :  The Indices
                FsMIRtm6ContextId

                The Object 
                setValFsMIRrd6FilterOspfTagMask
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRrd6FilterOspfTagMask (INT4 i4FsMIRtm6ContextId,
                                 INT4 i4SetValFsMIRrd6FilterOspfTagMask)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtm6SetContext (i4FsMIRtm6ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetFsRrd6FilterOspfTagMask (i4SetValFsMIRrd6FilterOspfTagMask);
    UtilRtm6ResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIRrd6RouterASNumber
 Input       :  The Indices
                FsMIRtm6ContextId

                The Object 
                setValFsMIRrd6RouterASNumber
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRrd6RouterASNumber (INT4 i4FsMIRtm6ContextId,
                              INT4 i4SetValFsMIRrd6RouterASNumber)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtm6SetContext (i4FsMIRtm6ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFsRrd6RouterASNumber (i4SetValFsMIRrd6RouterASNumber);
    UtilRtm6ResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIRrd6AdminStatus
 Input       :  The Indices
                FsMIRtm6ContextId

                The Object 
                setValFsMIRrd6AdminStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRrd6AdminStatus (INT4 i4FsMIRtm6ContextId,
                           INT4 i4SetValFsMIRrd6AdminStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtm6SetContext (i4FsMIRtm6ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFsRrd6AdminStatus (i4SetValFsMIRrd6AdminStatus);
    UtilRtm6ResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIRrd6Trace
 Input       :  The Indices
                FsMIRtm6ContextId

                The Object 
                setValFsMIRrd6Trace
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRrd6Trace (INT4 i4FsMIRtm6ContextId, UINT4 u4SetValFsMIRrd6Trace)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtm6SetContext (i4FsMIRtm6ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFsRrd6Trace (u4SetValFsMIRrd6Trace);
    UtilRtm6ResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIRtm6StaticRouteDistance
 Input       :  The Indices
                FsMIRtm6ContextId

                The Object
                setValFsMIRtm6StaticRouteDistance
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsMIRtm6StaticRouteDistance(INT4 i4FsMIRtm6ContextId , INT4 i4SetValFsMIRtm6StaticRouteDistance)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtm6SetContext ((UINT4)i4FsMIRtm6ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFsRtm6StaticRouteDistance (i4SetValFsMIRtm6StaticRouteDistance);
    UtilRtm6ResetContext ();
    return i1Return;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIRrd6RouterId
 Input       :  The Indices
                FsMIRtm6ContextId

                The Object 
                testValFsMIRrd6RouterId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRrd6RouterId (UINT4 *pu4ErrorCode, INT4 i4FsMIRtm6ContextId,
                           UINT4 u4TestValFsMIRrd6RouterId)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtm6SetContext (i4FsMIRtm6ContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2FsRrd6RouterId (pu4ErrorCode, u4TestValFsMIRrd6RouterId);
    UtilRtm6ResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIRrd6FilterByOspfTag
 Input       :  The Indices
                FsMIRtm6ContextId

                The Object 
                testValFsMIRrd6FilterByOspfTag
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRrd6FilterByOspfTag (UINT4 *pu4ErrorCode, INT4 i4FsMIRtm6ContextId,
                                  INT4 i4TestValFsMIRrd6FilterByOspfTag)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtm6SetContext (i4FsMIRtm6ContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2FsRrd6FilterByOspfTag (pu4ErrorCode,
                                        i4TestValFsMIRrd6FilterByOspfTag);
    UtilRtm6ResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIRrd6FilterOspfTag
 Input       :  The Indices
                FsMIRtm6ContextId

                The Object 
                testValFsMIRrd6FilterOspfTag
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRrd6FilterOspfTag (UINT4 *pu4ErrorCode, INT4 i4FsMIRtm6ContextId,
                                INT4 i4TestValFsMIRrd6FilterOspfTag)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtm6SetContext (i4FsMIRtm6ContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2FsRrd6FilterOspfTag (pu4ErrorCode,
                                      i4TestValFsMIRrd6FilterOspfTag);
    UtilRtm6ResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIRrd6FilterOspfTagMask
 Input       :  The Indices
                FsMIRtm6ContextId

                The Object 
                testValFsMIRrd6FilterOspfTagMask
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRrd6FilterOspfTagMask (UINT4 *pu4ErrorCode,
                                    INT4 i4FsMIRtm6ContextId,
                                    INT4 i4TestValFsMIRrd6FilterOspfTagMask)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtm6SetContext (i4FsMIRtm6ContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2FsRrd6FilterOspfTagMask (pu4ErrorCode,
                                          i4TestValFsMIRrd6FilterOspfTagMask);
    UtilRtm6ResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIRrd6RouterASNumber
 Input       :  The Indices
                FsMIRtm6ContextId

                The Object 
                testValFsMIRrd6RouterASNumber
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRrd6RouterASNumber (UINT4 *pu4ErrorCode, INT4 i4FsMIRtm6ContextId,
                                 INT4 i4TestValFsMIRrd6RouterASNumber)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtm6SetContext (i4FsMIRtm6ContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2FsRrd6RouterASNumber (pu4ErrorCode,
                                       i4TestValFsMIRrd6RouterASNumber);
    UtilRtm6ResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIRrd6AdminStatus
 Input       :  The Indices
                FsMIRtm6ContextId

                The Object 
                testValFsMIRrd6AdminStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRrd6AdminStatus (UINT4 *pu4ErrorCode, INT4 i4FsMIRtm6ContextId,
                              INT4 i4TestValFsMIRrd6AdminStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtm6SetContext (i4FsMIRtm6ContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2FsRrd6AdminStatus (pu4ErrorCode, i4TestValFsMIRrd6AdminStatus);
    UtilRtm6ResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIRrd6Trace
 Input       :  The Indices
                FsMIRtm6ContextId

                The Object 
                testValFsMIRrd6Trace
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRrd6Trace (UINT4 *pu4ErrorCode, INT4 i4FsMIRtm6ContextId,
                        UINT4 u4TestValFsMIRrd6Trace)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtm6SetContext (i4FsMIRtm6ContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return = nmhTestv2FsRrd6Trace (pu4ErrorCode, u4TestValFsMIRrd6Trace);
    UtilRtm6ResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIRtm6StaticRouteDistance
 Input       :  The Indices
                FsMIRtm6ContextId

                The Object
                testValFsMIRtm6StaticRouteDistance
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsMIRtm6StaticRouteDistance(UINT4 *pu4ErrorCode , INT4 i4FsMIRtm6ContextId , 
                                                    INT4 i4TestValFsMIRtm6StaticRouteDistance)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtm6SetContext ((UINT4)i4FsMIRtm6ContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return = nmhTestv2FsRtm6StaticRouteDistance(pu4ErrorCode, i4TestValFsMIRtm6StaticRouteDistance);
    UtilRtm6ResetContext ();
    return i1Return;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIRtm6Table
 Input       :  The Indices
                FsMIRtm6ContextId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIRtm6Table (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIRrd6ControlTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIRrd6ControlTable
 Input       :  The Indices
                FsMIRtm6ContextId
                FsMIRrd6ControlDestIpAddress
                FsMIRrd6ControlNetMaskLen
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIRrd6ControlTable (INT4 i4FsMIRtm6ContextId,
                                              tSNMP_OCTET_STRING_TYPE *
                                              pFsMIRrd6ControlDestIpAddress,
                                              INT4 i4FsMIRrd6ControlNetMaskLen)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if ((i4FsMIRtm6ContextId < RTM6_DEFAULT_CXT_ID) ||
        (i4FsMIRtm6ContextId >= (INT4) gRtm6GlobalInfo.u4MaxContextLimit))
    {
        return i1RetVal;
    }

    if (UtilRtm6VcmIsVcExist (i4FsMIRtm6ContextId) == RTM6_FAILURE)
    {
        return i1RetVal;
    }

    if (UtilRtm6IsValidCxtId (i4FsMIRtm6ContextId) == RTM6_FAILURE)
    {
        return i1RetVal;
    }
    if (UtilRtm6SetContext (i4FsMIRtm6ContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }
    i1RetVal =
        nmhValidateIndexInstanceFsRrd6ControlTable
        (pFsMIRrd6ControlDestIpAddress, i4FsMIRrd6ControlNetMaskLen);
    UtilRtm6ResetContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIRrd6ControlTable
 Input       :  The Indices
                FsMIRtm6ContextId
                FsMIRrd6ControlDestIpAddress
                FsMIRrd6ControlNetMaskLen
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIRrd6ControlTable (INT4 *pi4FsMIRtm6ContextId,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pFsMIRrd6ControlDestIpAddress,
                                      INT4 *pi4FsMIRrd6ControlNetMaskLen)
{

    INT1                i1RetVal = SNMP_FAILURE;
    INT4                i4FsMIRtm6ContextId = 0;

    if (UtilRtm6GetFirstCxtId ((UINT4 *) pi4FsMIRtm6ContextId) == RTM6_FAILURE)
    {
        return i1RetVal;
    }
    do
    {
        i4FsMIRtm6ContextId = *pi4FsMIRtm6ContextId;
        if (UtilRtm6SetContext (*pi4FsMIRtm6ContextId) == SNMP_FAILURE)
        {
            return i1RetVal;
        }
        i1RetVal =
            nmhGetFirstIndexFsRrd6ControlTable (pFsMIRrd6ControlDestIpAddress,
                                                pi4FsMIRrd6ControlNetMaskLen);
        UtilRtm6ResetContext ();
        if (i1RetVal == SNMP_SUCCESS)
        {
            return i1RetVal;
        }
    }
    while (UtilRtm6GetNextCxtId ((UINT4) i4FsMIRtm6ContextId,
                                 (UINT4 *) pi4FsMIRtm6ContextId) !=
           RTM6_FAILURE);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIRrd6ControlTable
 Input       :  The Indices
                FsMIRtm6ContextId
                nextFsMIRtm6ContextId
                FsMIRrd6ControlDestIpAddress
                nextFsMIRrd6ControlDestIpAddress
                FsMIRrd6ControlNetMaskLen
                nextFsMIRrd6ControlNetMaskLen
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIRrd6ControlTable (INT4 i4FsMIRtm6ContextId,
                                     INT4 *pi4NextFsMIRtm6ContextId,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pFsMIRrd6ControlDestIpAddress,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pNextFsMIRrd6ControlDestIpAddress,
                                     INT4 i4FsMIRrd6ControlNetMaskLen,
                                     INT4 *pi4NextFsMIRrd6ControlNetMaskLen)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (UtilRtm6IsValidCxtId (i4FsMIRtm6ContextId) == RTM6_SUCCESS)
    {
        if (UtilRtm6SetContext (i4FsMIRtm6ContextId) == SNMP_FAILURE)
        {
            return i1RetVal;
        }
        i1RetVal =
            nmhGetNextIndexFsRrd6ControlTable (pFsMIRrd6ControlDestIpAddress,
                                               pNextFsMIRrd6ControlDestIpAddress,
                                               i4FsMIRrd6ControlNetMaskLen,
                                               pi4NextFsMIRrd6ControlNetMaskLen);
        UtilRtm6ResetContext ();
    }

    if (i1RetVal == SNMP_FAILURE)
    {
        while ((UtilRtm6GetNextCxtId ((UINT4) i4FsMIRtm6ContextId,
                                      (UINT4 *) pi4NextFsMIRtm6ContextId))
               == RTM6_SUCCESS)
        {
            if (UtilRtm6SetContext (*pi4NextFsMIRtm6ContextId) == SNMP_FAILURE)
            {
                return i1RetVal;
            }
            i1RetVal =
                nmhGetFirstIndexFsRrd6ControlTable
                (pNextFsMIRrd6ControlDestIpAddress,
                 pi4NextFsMIRrd6ControlNetMaskLen);
            UtilRtm6ResetContext ();
            if (i1RetVal == SNMP_SUCCESS)
            {
                break;
            }
            i4FsMIRtm6ContextId = *pi4NextFsMIRtm6ContextId;
        }
    }
    else
    {
        *pi4NextFsMIRtm6ContextId = i4FsMIRtm6ContextId;
    }
    return i1RetVal;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIRrd6ControlSourceProto
 Input       :  The Indices
                FsMIRtm6ContextId
                FsMIRrd6ControlDestIpAddress
                FsMIRrd6ControlNetMaskLen

                The Object 
                retValFsMIRrd6ControlSourceProto
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRrd6ControlSourceProto (INT4 i4FsMIRtm6ContextId,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pFsMIRrd6ControlDestIpAddress,
                                  INT4 i4FsMIRrd6ControlNetMaskLen,
                                  INT4 *pi4RetValFsMIRrd6ControlSourceProto)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtm6SetContext (i4FsMIRtm6ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFsRrd6ControlSourceProto (pFsMIRrd6ControlDestIpAddress,
                                        i4FsMIRrd6ControlNetMaskLen,
                                        pi4RetValFsMIRrd6ControlSourceProto);
    UtilRtm6ResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIRrd6ControlDestProto
 Input       :  The Indices
                FsMIRtm6ContextId
                FsMIRrd6ControlDestIpAddress
                FsMIRrd6ControlNetMaskLen

                The Object 
                retValFsMIRrd6ControlDestProto
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRrd6ControlDestProto (INT4 i4FsMIRtm6ContextId,
                                tSNMP_OCTET_STRING_TYPE *
                                pFsMIRrd6ControlDestIpAddress,
                                INT4 i4FsMIRrd6ControlNetMaskLen,
                                INT4 *pi4RetValFsMIRrd6ControlDestProto)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtm6SetContext (i4FsMIRtm6ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFsRrd6ControlDestProto (pFsMIRrd6ControlDestIpAddress,
                                      i4FsMIRrd6ControlNetMaskLen,
                                      pi4RetValFsMIRrd6ControlDestProto);
    UtilRtm6ResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIRrd6ControlRouteExportFlag
 Input       :  The Indices
                FsMIRtm6ContextId
                FsMIRrd6ControlDestIpAddress
                FsMIRrd6ControlNetMaskLen

                The Object 
                retValFsMIRrd6ControlRouteExportFlag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRrd6ControlRouteExportFlag (INT4 i4FsMIRtm6ContextId,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pFsMIRrd6ControlDestIpAddress,
                                      INT4 i4FsMIRrd6ControlNetMaskLen,
                                      INT4
                                      *pi4RetValFsMIRrd6ControlRouteExportFlag)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtm6SetContext (i4FsMIRtm6ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFsRrd6ControlRouteExportFlag (pFsMIRrd6ControlDestIpAddress,
                                            i4FsMIRrd6ControlNetMaskLen,
                                            pi4RetValFsMIRrd6ControlRouteExportFlag);
    UtilRtm6ResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIRrd6ControlRowStatus
 Input       :  The Indices
                FsMIRtm6ContextId
                FsMIRrd6ControlDestIpAddress
                FsMIRrd6ControlNetMaskLen

                The Object 
                retValFsMIRrd6ControlRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRrd6ControlRowStatus (INT4 i4FsMIRtm6ContextId,
                                tSNMP_OCTET_STRING_TYPE *
                                pFsMIRrd6ControlDestIpAddress,
                                INT4 i4FsMIRrd6ControlNetMaskLen,
                                INT4 *pi4RetValFsMIRrd6ControlRowStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtm6SetContext (i4FsMIRtm6ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFsRrd6ControlRowStatus (pFsMIRrd6ControlDestIpAddress,
                                      i4FsMIRrd6ControlNetMaskLen,
                                      pi4RetValFsMIRrd6ControlRowStatus);
    UtilRtm6ResetContext ();
    return i1Return;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIRrd6ControlSourceProto
 Input       :  The Indices
                FsMIRtm6ContextId
                FsMIRrd6ControlDestIpAddress
                FsMIRrd6ControlNetMaskLen

                The Object 
                setValFsMIRrd6ControlSourceProto
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRrd6ControlSourceProto (INT4 i4FsMIRtm6ContextId,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pFsMIRrd6ControlDestIpAddress,
                                  INT4 i4FsMIRrd6ControlNetMaskLen,
                                  INT4 i4SetValFsMIRrd6ControlSourceProto)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtm6SetContext (i4FsMIRtm6ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetFsRrd6ControlSourceProto (pFsMIRrd6ControlDestIpAddress,
                                        i4FsMIRrd6ControlNetMaskLen,
                                        i4SetValFsMIRrd6ControlSourceProto);
    UtilRtm6ResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIRrd6ControlDestProto
 Input       :  The Indices
                FsMIRtm6ContextId
                FsMIRrd6ControlDestIpAddress
                FsMIRrd6ControlNetMaskLen

                The Object 
                setValFsMIRrd6ControlDestProto
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRrd6ControlDestProto (INT4 i4FsMIRtm6ContextId,
                                tSNMP_OCTET_STRING_TYPE *
                                pFsMIRrd6ControlDestIpAddress,
                                INT4 i4FsMIRrd6ControlNetMaskLen,
                                INT4 i4SetValFsMIRrd6ControlDestProto)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtm6SetContext (i4FsMIRtm6ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetFsRrd6ControlDestProto (pFsMIRrd6ControlDestIpAddress,
                                      i4FsMIRrd6ControlNetMaskLen,
                                      i4SetValFsMIRrd6ControlDestProto);
    UtilRtm6ResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIRrd6ControlRouteExportFlag
 Input       :  The Indices
                FsMIRtm6ContextId
                FsMIRrd6ControlDestIpAddress
                FsMIRrd6ControlNetMaskLen

                The Object 
                setValFsMIRrd6ControlRouteExportFlag
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRrd6ControlRouteExportFlag (INT4 i4FsMIRtm6ContextId,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pFsMIRrd6ControlDestIpAddress,
                                      INT4 i4FsMIRrd6ControlNetMaskLen,
                                      INT4
                                      i4SetValFsMIRrd6ControlRouteExportFlag)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtm6SetContext (i4FsMIRtm6ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetFsRrd6ControlRouteExportFlag (pFsMIRrd6ControlDestIpAddress,
                                            i4FsMIRrd6ControlNetMaskLen,
                                            i4SetValFsMIRrd6ControlRouteExportFlag);
    UtilRtm6ResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIRrd6ControlRowStatus
 Input       :  The Indices
                FsMIRtm6ContextId
                FsMIRrd6ControlDestIpAddress
                FsMIRrd6ControlNetMaskLen

                The Object 
                setValFsMIRrd6ControlRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRrd6ControlRowStatus (INT4 i4FsMIRtm6ContextId,
                                tSNMP_OCTET_STRING_TYPE *
                                pFsMIRrd6ControlDestIpAddress,
                                INT4 i4FsMIRrd6ControlNetMaskLen,
                                INT4 i4SetValFsMIRrd6ControlRowStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtm6SetContext (i4FsMIRtm6ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetFsRrd6ControlRowStatus (pFsMIRrd6ControlDestIpAddress,
                                      i4FsMIRrd6ControlNetMaskLen,
                                      i4SetValFsMIRrd6ControlRowStatus);
    UtilRtm6ResetContext ();
    return i1Return;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIRrd6ControlSourceProto
 Input       :  The Indices
                FsMIRtm6ContextId
                FsMIRrd6ControlDestIpAddress
                FsMIRrd6ControlNetMaskLen

                The Object 
                testValFsMIRrd6ControlSourceProto
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRrd6ControlSourceProto (UINT4 *pu4ErrorCode,
                                     INT4 i4FsMIRtm6ContextId,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pFsMIRrd6ControlDestIpAddress,
                                     INT4 i4FsMIRrd6ControlNetMaskLen,
                                     INT4 i4TestValFsMIRrd6ControlSourceProto)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtm6SetContext (i4FsMIRtm6ContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2FsRrd6ControlSourceProto (pu4ErrorCode,
                                           pFsMIRrd6ControlDestIpAddress,
                                           i4FsMIRrd6ControlNetMaskLen,
                                           i4TestValFsMIRrd6ControlSourceProto);
    UtilRtm6ResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIRrd6ControlDestProto
 Input       :  The Indices
                FsMIRtm6ContextId
                FsMIRrd6ControlDestIpAddress
                FsMIRrd6ControlNetMaskLen

                The Object 
                testValFsMIRrd6ControlDestProto
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRrd6ControlDestProto (UINT4 *pu4ErrorCode,
                                   INT4 i4FsMIRtm6ContextId,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsMIRrd6ControlDestIpAddress,
                                   INT4 i4FsMIRrd6ControlNetMaskLen,
                                   INT4 i4TestValFsMIRrd6ControlDestProto)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtm6SetContext (i4FsMIRtm6ContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2FsRrd6ControlDestProto (pu4ErrorCode,
                                         pFsMIRrd6ControlDestIpAddress,
                                         i4FsMIRrd6ControlNetMaskLen,
                                         i4TestValFsMIRrd6ControlDestProto);
    UtilRtm6ResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIRrd6ControlRouteExportFlag
 Input       :  The Indices
                FsMIRtm6ContextId
                FsMIRrd6ControlDestIpAddress
                FsMIRrd6ControlNetMaskLen

                The Object 
                testValFsMIRrd6ControlRouteExportFlag
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRrd6ControlRouteExportFlag (UINT4 *pu4ErrorCode,
                                         INT4 i4FsMIRtm6ContextId,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pFsMIRrd6ControlDestIpAddress,
                                         INT4 i4FsMIRrd6ControlNetMaskLen,
                                         INT4
                                         i4TestValFsMIRrd6ControlRouteExportFlag)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtm6SetContext (i4FsMIRtm6ContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2FsRrd6ControlRouteExportFlag (pu4ErrorCode,
                                               pFsMIRrd6ControlDestIpAddress,
                                               i4FsMIRrd6ControlNetMaskLen,
                                               i4TestValFsMIRrd6ControlRouteExportFlag);
    UtilRtm6ResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIRrd6ControlRowStatus
 Input       :  The Indices
                FsMIRtm6ContextId
                FsMIRrd6ControlDestIpAddress
                FsMIRrd6ControlNetMaskLen

                The Object 
                testValFsMIRrd6ControlRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRrd6ControlRowStatus (UINT4 *pu4ErrorCode,
                                   INT4 i4FsMIRtm6ContextId,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsMIRrd6ControlDestIpAddress,
                                   INT4 i4FsMIRrd6ControlNetMaskLen,
                                   INT4 i4TestValFsMIRrd6ControlRowStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtm6SetContext (i4FsMIRtm6ContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2FsRrd6ControlRowStatus (pu4ErrorCode,
                                         pFsMIRrd6ControlDestIpAddress,
                                         i4FsMIRrd6ControlNetMaskLen,
                                         i4TestValFsMIRrd6ControlRowStatus);
    UtilRtm6ResetContext ();
    return i1Return;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIRrd6ControlTable
 Input       :  The Indices
                FsMIRtm6ContextId
                FsMIRrd6ControlDestIpAddress
                FsMIRrd6ControlNetMaskLen
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIRrd6ControlTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIRrd6RoutingProtoTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIRrd6RoutingProtoTable
 Input       :  The Indices
                FsMIRtm6ContextId
                FsMIRrd6RoutingProtoId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIRrd6RoutingProtoTable (INT4 i4FsMIRtm6ContextId,
                                                   INT4
                                                   i4FsMIRrd6RoutingProtoId)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if ((i4FsMIRtm6ContextId < RTM6_DEFAULT_CXT_ID) ||
        (i4FsMIRtm6ContextId >= (INT4) gRtm6GlobalInfo.u4MaxContextLimit))
    {
        return i1RetVal;
    }

    if (UtilRtm6VcmIsVcExist (i4FsMIRtm6ContextId) == RTM6_FAILURE)
    {
        return i1RetVal;
    }

    if (UtilRtm6IsValidCxtId (i4FsMIRtm6ContextId) == RTM6_FAILURE)
    {
        return i1RetVal;
    }
    if (UtilRtm6SetContext (i4FsMIRtm6ContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }
    i1RetVal =
        nmhValidateIndexInstanceFsRrd6RoutingProtoTable
        (i4FsMIRrd6RoutingProtoId);
    UtilRtm6ResetContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIRrd6RoutingProtoTable
 Input       :  The Indices
                FsMIRtm6ContextId
                FsMIRrd6RoutingProtoId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIRrd6RoutingProtoTable (INT4 *pi4FsMIRtm6ContextId,
                                           INT4 *pi4FsMIRrd6RoutingProtoId)
{
    INT1                i1RetVal = SNMP_FAILURE;
    INT4                i4FsMIRtm6ContextId = 0;

    if (UtilRtm6GetFirstCxtId ((UINT4 *) pi4FsMIRtm6ContextId) == RTM6_FAILURE)
    {
        return i1RetVal;
    }
    do
    {
        i4FsMIRtm6ContextId = *pi4FsMIRtm6ContextId;
        if (UtilRtm6SetContext (*pi4FsMIRtm6ContextId) == SNMP_FAILURE)
        {
            return i1RetVal;
        }
        i1RetVal =
            nmhGetFirstIndexFsRrd6RoutingProtoTable (pi4FsMIRrd6RoutingProtoId);
        UtilRtm6ResetContext ();
        if (i1RetVal == SNMP_SUCCESS)
        {
            return i1RetVal;
        }
    }
    while (UtilRtm6GetNextCxtId ((UINT4) i4FsMIRtm6ContextId,
                                 (UINT4 *) pi4FsMIRtm6ContextId) !=
           RTM6_FAILURE);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIRrd6RoutingProtoTable
 Input       :  The Indices
                FsMIRtm6ContextId
                nextFsMIRtm6ContextId
                FsMIRrd6RoutingProtoId
                nextFsMIRrd6RoutingProtoId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIRrd6RoutingProtoTable (INT4 i4FsMIRtm6ContextId,
                                          INT4 *pi4NextFsMIRtm6ContextId,
                                          INT4 i4FsMIRrd6RoutingProtoId,
                                          INT4 *pi4NextFsMIRrd6RoutingProtoId)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (UtilRtm6IsValidCxtId (i4FsMIRtm6ContextId) == RTM6_SUCCESS)
    {
        if (UtilRtm6SetContext (i4FsMIRtm6ContextId) == SNMP_FAILURE)
        {
            return i1RetVal;
        }
        i1RetVal =
            nmhGetNextIndexFsRrd6RoutingProtoTable (i4FsMIRrd6RoutingProtoId,
                                                    pi4NextFsMIRrd6RoutingProtoId);
        UtilRtm6ResetContext ();
    }

    if (i1RetVal == SNMP_FAILURE)
    {
        while ((UtilRtm6GetNextCxtId ((UINT4) i4FsMIRtm6ContextId,
                                      (UINT4 *) pi4NextFsMIRtm6ContextId))
               == RTM6_SUCCESS)
        {
            if (UtilRtm6SetContext (*pi4NextFsMIRtm6ContextId) == SNMP_FAILURE)
            {
                return i1RetVal;
            }
            i1RetVal =
                nmhGetFirstIndexFsRrd6RoutingProtoTable
                (pi4NextFsMIRrd6RoutingProtoId);
            UtilRtm6ResetContext ();
            if (i1RetVal == SNMP_SUCCESS)
            {
                break;
            }
            i4FsMIRtm6ContextId = *pi4NextFsMIRtm6ContextId;
        }
    }
    else
    {
        *pi4NextFsMIRtm6ContextId = i4FsMIRtm6ContextId;
    }
    return i1RetVal;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIRrd6RoutingRegnId
 Input       :  The Indices
                FsMIRtm6ContextId
                FsMIRrd6RoutingProtoId

                The Object 
                retValFsMIRrd6RoutingRegnId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRrd6RoutingRegnId (INT4 i4FsMIRtm6ContextId,
                             INT4 i4FsMIRrd6RoutingProtoId,
                             INT4 *pi4RetValFsMIRrd6RoutingRegnId)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtm6SetContext (i4FsMIRtm6ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFsRrd6RoutingRegnId (i4FsMIRrd6RoutingProtoId,
                                   pi4RetValFsMIRrd6RoutingRegnId);
    UtilRtm6ResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIRrd6RoutingProtoTaskIdent
 Input       :  The Indices
                FsMIRtm6ContextId
                FsMIRrd6RoutingProtoId

                The Object 
                retValFsMIRrd6RoutingProtoTaskIdent
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRrd6RoutingProtoTaskIdent (INT4 i4FsMIRtm6ContextId,
                                     INT4 i4FsMIRrd6RoutingProtoId,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pRetValFsMIRrd6RoutingProtoTaskIdent)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtm6SetContext (i4FsMIRtm6ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFsRrd6RoutingProtoTaskIdent (i4FsMIRrd6RoutingProtoId,
                                           pRetValFsMIRrd6RoutingProtoTaskIdent);
    UtilRtm6ResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIRrd6RoutingProtoQueueIdent
 Input       :  The Indices
                FsMIRtm6ContextId
                FsMIRrd6RoutingProtoId

                The Object 
                retValFsMIRrd6RoutingProtoQueueIdent
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRrd6RoutingProtoQueueIdent (INT4 i4FsMIRtm6ContextId,
                                      INT4 i4FsMIRrd6RoutingProtoId,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pRetValFsMIRrd6RoutingProtoQueueIdent)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtm6SetContext (i4FsMIRtm6ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFsRrd6RoutingProtoQueueIdent (i4FsMIRrd6RoutingProtoId,
                                            pRetValFsMIRrd6RoutingProtoQueueIdent);
    UtilRtm6ResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIRrd6AllowOspfAreaRoutes
 Input       :  The Indices
                FsMIRtm6ContextId
                FsMIRrd6RoutingProtoId

                The Object 
                retValFsMIRrd6AllowOspfAreaRoutes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRrd6AllowOspfAreaRoutes (INT4 i4FsMIRtm6ContextId,
                                   INT4 i4FsMIRrd6RoutingProtoId,
                                   INT4 *pi4RetValFsMIRrd6AllowOspfAreaRoutes)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtm6SetContext (i4FsMIRtm6ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFsRrd6AllowOspfAreaRoutes (i4FsMIRrd6RoutingProtoId,
                                         pi4RetValFsMIRrd6AllowOspfAreaRoutes);
    UtilRtm6ResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIRrd6AllowOspfExtRoutes
 Input       :  The Indices
                FsMIRtm6ContextId
                FsMIRrd6RoutingProtoId

                The Object 
                retValFsMIRrd6AllowOspfExtRoutes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRrd6AllowOspfExtRoutes (INT4 i4FsMIRtm6ContextId,
                                  INT4 i4FsMIRrd6RoutingProtoId,
                                  INT4 *pi4RetValFsMIRrd6AllowOspfExtRoutes)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtm6SetContext (i4FsMIRtm6ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFsRrd6AllowOspfExtRoutes (i4FsMIRrd6RoutingProtoId,
                                        pi4RetValFsMIRrd6AllowOspfExtRoutes);
    UtilRtm6ResetContext ();
    return i1Return;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIRrd6AllowOspfAreaRoutes
 Input       :  The Indices
                FsMIRtm6ContextId
                FsMIRrd6RoutingProtoId

                The Object 
                setValFsMIRrd6AllowOspfAreaRoutes
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRrd6AllowOspfAreaRoutes (INT4 i4FsMIRtm6ContextId,
                                   INT4 i4FsMIRrd6RoutingProtoId,
                                   INT4 i4SetValFsMIRrd6AllowOspfAreaRoutes)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtm6SetContext (i4FsMIRtm6ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetFsRrd6AllowOspfAreaRoutes (i4FsMIRrd6RoutingProtoId,
                                         i4SetValFsMIRrd6AllowOspfAreaRoutes);
    UtilRtm6ResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIRrd6AllowOspfExtRoutes
 Input       :  The Indices
                FsMIRtm6ContextId
                FsMIRrd6RoutingProtoId

                The Object 
                setValFsMIRrd6AllowOspfExtRoutes
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRrd6AllowOspfExtRoutes (INT4 i4FsMIRtm6ContextId,
                                  INT4 i4FsMIRrd6RoutingProtoId,
                                  INT4 i4SetValFsMIRrd6AllowOspfExtRoutes)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtm6SetContext (i4FsMIRtm6ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetFsRrd6AllowOspfExtRoutes (i4FsMIRrd6RoutingProtoId,
                                        i4SetValFsMIRrd6AllowOspfExtRoutes);
    UtilRtm6ResetContext ();
    return i1Return;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIRrd6AllowOspfAreaRoutes
 Input       :  The Indices
                FsMIRtm6ContextId
                FsMIRrd6RoutingProtoId

                The Object 
                testValFsMIRrd6AllowOspfAreaRoutes
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRrd6AllowOspfAreaRoutes (UINT4 *pu4ErrorCode,
                                      INT4 i4FsMIRtm6ContextId,
                                      INT4 i4FsMIRrd6RoutingProtoId,
                                      INT4 i4TestValFsMIRrd6AllowOspfAreaRoutes)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtm6SetContext (i4FsMIRtm6ContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2FsRrd6AllowOspfAreaRoutes (pu4ErrorCode,
                                            i4FsMIRrd6RoutingProtoId,
                                            i4TestValFsMIRrd6AllowOspfAreaRoutes);
    UtilRtm6ResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIRrd6AllowOspfExtRoutes
 Input       :  The Indices
                FsMIRtm6ContextId
                FsMIRrd6RoutingProtoId

                The Object 
                testValFsMIRrd6AllowOspfExtRoutes
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRrd6AllowOspfExtRoutes (UINT4 *pu4ErrorCode,
                                     INT4 i4FsMIRtm6ContextId,
                                     INT4 i4FsMIRrd6RoutingProtoId,
                                     INT4 i4TestValFsMIRrd6AllowOspfExtRoutes)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtm6SetContext (i4FsMIRtm6ContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2FsRrd6AllowOspfExtRoutes (pu4ErrorCode,
                                           i4FsMIRrd6RoutingProtoId,
                                           i4TestValFsMIRrd6AllowOspfExtRoutes);
    UtilRtm6ResetContext ();
    return i1Return;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIRrd6RoutingProtoTable
 Input       :  The Indices
                FsMIRtm6ContextId
                FsMIRrd6RoutingProtoId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIRrd6RoutingProtoTable (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIRtm6RedEntryTime
 Input       :  The Indices

                The Object 
                retValFsMIRtm6RedEntryTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRtm6RedEntryTime (INT4 *pi4RetValFsMIRtm6RedEntryTime)
{
    INT1                i1Return = SNMP_FAILURE;
    i1Return = nmhGetFsRtm6RedEntryTime (pi4RetValFsMIRtm6RedEntryTime);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIRtm6RedExitTime
 Input       :  The Indices

                The Object 
                retValFsMIRtm6RedExitTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRtm6RedExitTime (INT4 *pi4RetValFsMIRtm6RedExitTime)
{
    INT1                i1Return = SNMP_FAILURE;
    i1Return = nmhGetFsRtm6RedExitTime (pi4RetValFsMIRtm6RedExitTime);
    return i1Return;
}
