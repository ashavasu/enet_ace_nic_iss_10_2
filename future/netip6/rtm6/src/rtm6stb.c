/********************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: rtm6stb.c,v 1.3 2015/04/24 11:59:53 siva Exp $
 *
 * Description: This file contains RTM6 Redundancy related 
 *              routines
 *           
 *******************************************************************/
#ifndef __RTM6STB_C
#define __RTM6STB_C

#include "rtm6inc.h"
#include "rtm6red.h"

/************************************************************************/
/*  Function Name   : Rtm6RedInitGlobalInfo                             */
/*                                                                      */
/*  Description     : This function is invoked by the RTM6 module to    */
/*                    register itself with the RM module. This          */
/*                    registration is required to send/receive peer     */
/*                    synchronization messages and control events       */
/*                                                                      */
/*  Input(s)        : None.                                             */
/*                                                                      */
/*  Output(s)       : None.                                             */
/*                                                                      */
/*  Returns         : OSIX_SUCCESS / OSIX_FAILURE                       */
/************************************************************************/

PUBLIC INT4
Rtm6RedInitGlobalInfo (VOID)
{
    gRtm6RedGlobalInfo.u1RedStatus = IP6_FALSE;
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : Rtm6RedRmRegisterProtocols                        */
/*                                                                      */
/*  Description     : This function is invoked by the RTM6 module to    */
/*                    register itself with the RM module. This          */
/*                    registration is required to send/receive peer     */
/*                    synchronization messages and control events       */
/*                                                                      */
/*  Input(s)        : pRmReg -- Pointer to structure tRmRegParams       */
/*                                                                      */
/*  Output(s)       : None.                                             */
/*                                                                      */
/*  Returns         : OSIX_SUCCESS / OSIX_FAILURE                       */
/************************************************************************/

PUBLIC INT4
Rtm6RedRmRegisterProtocols (tRmRegParams * pRmReg)
{
    UNUSED_PARAM (pRmReg);
    return OSIX_SUCCESS;
}

/************************************************************************/
/* Function Name      : Rtm6RedDeInitGlobalInfo                         */
/*                                                                      */
/* Description        : This function is invoked by the RTM6 module     */
/*                      during module shutdown and this function        */
/*                      deinitializes the redundancy global variables   */
/*                      and de-register RTM6 with RM.                   */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/************************************************************************/

INT4
Rtm6RedDeInitGlobalInfo (VOID)
{
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : Rtm6RmDeRegisterProtocols                         */
/*                                                                      */
/*  Description     : This function is invoked by the RTM6 module to    */
/*                    de-register itself with the RM module.            */
/*                                                                      */
/*  Input(s)        : None.                                             */
/*                                                                      */
/*  Output(s)       : None.                                             */
/*                                                                      */
/*  Returns         : OSIX_SUCCESS / OSIX_FAILURE                       */
/************************************************************************/

PUBLIC INT4
Rtm6RedRmDeRegisterProtocols (VOID)
{
    return OSIX_SUCCESS;
}

/************************************************************************/
/* Function Name      : Rtm6RedRmCallBack                               */
/*                                                                      */
/* Description        : This function will be invoked by the RM module  */
/*                      to enque events and post messages to RTM6       */
/*                                                                      */
/* Input(s)           : u1Event   - Event type given by RM module       */
/*                      pData     - RM Message to enqueue               */
/*                      u2DataLen - Message size                        */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/************************************************************************/

PUBLIC INT4
Rtm6RedRmCallBack (UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen)
{
    UNUSED_PARAM (u1Event);
    UNUSED_PARAM (pData);
    UNUSED_PARAM (u2DataLen);
    return OSIX_SUCCESS;
}

/************************************************************************/
/* Function Name      : Rtm6RedHandleRmEvents                           */
/*                                                                      */
/* Description        : This function is invoked by the RTM6 module to  */
/*                      process all the events and messages posted by   */
/*                      the RM module                                   */
/*                                                                      */
/* Input(s)           : pMsg -- Pointer to the RTM6 Q Msg               */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
Rtm6RedHandleRmEvents ()
{
    return;
}

/************************************************************************/
/* Function Name      : Rtm6RedHandleGoActive                           */
/*                                                                      */
/* Description        : This function is invoked by the RTM6 upon       */
/*                      receiving the GO_ACTIVE indication from RM      */
/*                      module. And this function responds to RM with an*/
/*                      acknowledgement.                                */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
Rtm6RedHandleGoActive (VOID)
{
    return;
}

/************************************************************************/
/* Function Name      : Rtm6RedHandleGoStandby                          */
/*                                                                      */
/* Description        : This function is invoked by the RTM6 upon       */
/*                      receiving the GO_STANBY indication from RM      */
/*                      module. And this function responds to RM module */
/*                      with an acknowledgement.                        */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
Rtm6RedHandleGoStandby (tRtm6RmMsg * pMsg)
{
    UNUSED_PARAM (pMsg);
    return;
}

/************************************************************************/
/* Function Name      : Rtm6RedHandleIdleToActive                       */
/*                                                                      */
/* Description        : This function updates the node status to ACTIVE */
/*                      on transition from Idle to Active and also      */
/*                      updates the number of Standby node count.       */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
Rtm6RedHandleIdleToActive (VOID)
{
    return;
}

/************************************************************************/
/* Function Name      : Rtm6RedHandleIdleToStandby                      */
/*                                                                      */
/* Description        : This function updates the node status to STANDBY*/
/*                      on transition from Idle to Standby and also     */
/*                      updates the number of Standby node count to 0   */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
Rtm6RedHandleIdleToStandby (VOID)
{
    return;
}

/************************************************************************/
/* Function Name      : Rtm6RedHandleStandbyToActive                    */
/*                                                                      */
/* Description        : This function handles the Standby node to Active*/
/*                      node transistion. The following action are      */
/*                      performed during this transistion.              */
/*                      1.Update the Node Status and Standby node count.*/
/*                      2.Start the timers and enable the module.       */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
Rtm6RedHandleStandbyToActive (VOID)
{
    return;
}

/************************************************************************/
/* Function Name      : Rtm6RedHandleActiveToStandby                    */
/*                                                                      */
/* Description        : This function handles the Active node to Standby*/
/*                      node transistion. The following action are      */
/*                      performed during this transistion               */
/*                      1.Update the Node Status and Standby node count */
/*                      2.Disable and Enable the module                 */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
Rtm6RedHandleActiveToStandby (VOID)
{
    return;
}

/************************************************************************/
/* Function Name      : Rtm6RedProcessPeerMsgAtActive                   */
/*                                                                      */
/* Description        : This routine handles messages from the other    */
/*                      node (Standby) at Active. The messages that are */
/*                      handled in Active node are                      */
/*                      1. RM_BULK_UPDT_REQ_MSG                         */
/*                                                                      */
/* Input(s)           : pMsg - RM Data buffer holding message           */
/*                      u2DataLen - Length of data in buffer.           */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
Rtm6RedProcessPeerMsgAtActive (tRmMsg * pMsg, UINT2 u2DataLen)
{
    UNUSED_PARAM (pMsg);
    UNUSED_PARAM (u2DataLen);
    return;
}

/************************************************************************/
/* Function Name      : Rtm6RedProcessPeerMsgAtStandby                  */
/*                                                                      */
/* Description        : This routine handles messages from the other    */
/*                      node (Active) at standby. The messages that are */
/*                      handled in Standby node are                     */
/*                      1. RM_BULK_UPDT_TAIL_MSG                        */
/*                      2. Dynamic sync-up messages                     */
/*                      3. Dynamic bulk messages                        */
/*                                                                      */
/* Input(s)           : pMsg - RM Data buffer holding message           */
/*                      u2DataLen - Length of data in buffer.           */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
Rtm6RedProcessPeerMsgAtStandby (tRmMsg * pMsg, UINT2 u2DataLen)
{
    UNUSED_PARAM (pMsg);
    UNUSED_PARAM (u2DataLen);
    return;
}

/************************************************************************/
/* Function Name      : Rtm6RedRmReleaseMemoryForMsg                    */
/*                                                                      */
/* Description        : This function is invoked by the RTM6 module to  */
/*                      release the allocated memory by calling the RM  */
/*                      module.                                         */
/*                                                                      */
/* Input(s)           : pu1Block -- Memory Block                        */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/************************************************************************/

PUBLIC INT4
Rtm6RedRmReleaseMemoryForMsg (UINT1 *pu1Block)
{
    UNUSED_PARAM (pu1Block);
    return OSIX_SUCCESS;
}

/************************************************************************/
/* Function Name      : Rtm6RedSendMsgToRm                              */
/*                                                                      */
/* Description        : This routine enqueues the Message to RM. If the */
/*                      sending fails, it frees the memory.             */
/*                                                                      */
/* Input(s)           : pMsg - RM Message Data Buffer                   */
/*                      u2Length - Length of the message                */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/************************************************************************/

PUBLIC INT4
Rtm6RedSendMsgToRm (tRmMsg * pMsg, UINT2 u2Length)
{
    UNUSED_PARAM (pMsg);
    UNUSED_PARAM (u2Length);
    return OSIX_SUCCESS;
}

/************************************************************************/
/* Function Name      : Rtm6RedSendBulkReqMsg                           */
/*                                                                      */
/* Description        : This function sends the bulk request message    */
/*                      from standby node to Active node to initiate the*/
/*                      bulk update process.                            */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
Rtm6RedSendBulkReqMsg (VOID)
{
    return;
}

/************************************************************************/
/* Function Name      : Rtm6RedSendBulkUpdMsg                           */
/*                                                                      */
/* Description        : This function sends the binding table dynamic   */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
Rtm6RedSendBulkUpdMsg (VOID)
{
    return;

}

/***********************************************************************
 * Function Name      : Rtm6RedSendBulkInfo                         
 *                                                                      
 * Description        : This function sends the Bulk update messages to 
 *                      the peer standby RTM6.                           
 *                                                                      
 * Input(s)           : None                                            
 *                                                                      
 * Output(s)          : pu1BulkUpdPendFlg                               
 *                                                                      
 * Returns            : RTM6_SUCCESS/RTM6_FAILURE                         
 ***********************************************************************/

PUBLIC INT4
Rtm6RedSendBulkInfo (UINT1 *pu1BulkUpdPendFlg)
{
    UNUSED_PARAM (pu1BulkUpdPendFlg);
    return RTM6_SUCCESS;
}

/************************************************************************/
/* Function Name      : Rtm6RedSendBulkUpdTailMsg                       */
/*                                                                      */
/* Description        : This function sends the binding table dynamic   */
/*                      sync up message to Standby node from the Active */
/*                      node, when the RTM6 offers an IP address        */
/*                      to the RTM6 client and dynamically update the   */
/*                      binding table entries.                          */
/*                                                                      */
/* Input(s)           : pRtm6BindingInfo - binding entry to be synced up*/
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/************************************************************************/

VOID
Rtm6RedSendBulkUpdTailMsg (VOID)
{
    return;
}

/************************************************************************/
/* Function Name      : Rtm6RedProcessBulkTailMsg                       */
/*                                                                      */
/* Description        : This function process the bulk update tail      */
/*                      message and send bulk updates                   */
/*                                                                      */
/* Input(s)           : pMsg - RM Data buffer holding messges           */
/*                      u2DataLen - Length of data in buffer            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
Rtm6RedProcessBulkTailMsg (tRmMsg * pMsg, UINT2 *pu2OffSet)
{
    UNUSED_PARAM (pMsg);
    UNUSED_PARAM (pu2OffSet);
    return;
}

/************************************************************************
 * Function Name      : Rtm6RedSendDynamicInfo 
 *                                                
 * Description        : This function sends the binding table dynamic  
 *                      sync up message to Standby node from the Active 
 *                      node, when the RTM6 offers an IP address 
 *                      to the RTM6 client and dynamically update the   
 *                      binding table entries.                        
 *                                                                   
 * Input(s)           : None
 *                                                                    
 * Output(s)          : None                                         
 *                                                                  
 * Returns            : OSIX_SUCCESS / OSIX_FAILURE                
 ************************************************************************/

PUBLIC VOID
Rtm6RedSendDynamicInfo ()
{
    return;
}

/************************************************************************/
/* Function Name      : Rtm6RedProcessBulkInfo                          */
/*                                                                      */
/* Description        : This function decodes the sync-up message from  */
/*                      Active node and updates the binding information */
/*                      at the Standby node.                            */
/*                                                                      */
/* Input(s)           : pMsg - RM Sync-up message                       */
/*                      u2RemMsgLen - Length of value of Message        */
/*                                                                      */
/* Output(s)          : u2OffSet - OffSet value                         */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

VOID
Rtm6RedProcessBulkInfo (tRmMsg * pMsg, UINT2 *pu2OffSet)
{
    UNUSED_PARAM (pMsg);
    UNUSED_PARAM (pu2OffSet);
    return;
}

/************************************************************************/
/* Function Name      : Rtm6RedProcessDynamicInfo                       */
/*                                                                      */
/* Description        : This function decodes the sync-up message from  */
/*                      Active node and updates the binding information */
/*                      at the Standby node.                            */
/*                                                                      */
/* Input(s)           : pMsg - RM Sync-up message                       */
/*                      u2RemMsgLen - Length of value of Message        */
/*                                                                      */
/* Output(s)          : u2OffSet - OffSet value                         */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

VOID
Rtm6RedProcessDynamicInfo (tRmMsg * pMsg, UINT2 *pu2OffSet)
{
    UNUSED_PARAM (pMsg);
    UNUSED_PARAM (pu2OffSet);
    return;
}

/************************************************************************/
/* Function Name      : Rtm6RedAddDynamicInfo                           */
/*                                                                      */
/* Description        : This function adds the dynamic entries to the   */
/*                      global tree and this tree is scanned everytime  */
/*                      for sending the dynamic entry.                  */
/*                                                                      */
/* Input(s)           : pRt6Info - Rtm6 Route  message                  */
/*                      u1Operation - add or delete entry               */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/
VOID
Rtm6RedAddDynamicInfo (tIp6RtEntry * pRt6Info, UINT4 u4CxtId)
{
    UNUSED_PARAM (pRt6Info);
    UNUSED_PARAM (u4CxtId);
    return;
}

/*-------------------------------------------------------------------+
 * Function           : Rtm6RBTreeRedEntryCmp
 *
 * Input(s)           : pRBElem, pRBElemIn
 *
 * Output(s)          : None.
 *
 * Returns            : -1  if pRBElem <  pRBElemIn
 *                      1   if pRBElem >  pRBElemIn
 *                      0   if pRBElem == pRBElemIn
 * Action :
 * This procedure compares the two Redundancy entries in lexicographic
 * order of the indices of the Redundancy entry.
+-------------------------------------------------------------------*/

INT4
Rtm6RBTreeRedEntryCmp (tRBElem * pRBElem, tRBElem * pRBElemIn)
{
    UNUSED_PARAM (pRBElem);
    UNUSED_PARAM (pRBElemIn);
    return IP6_RB_EQUAL;
}

/*-------------------------------------------------------------------+
 * Function           : Rtm6RedUpdateRtParams
 *
 * Input(s)           : pRt6Info, u4Context
 *
 * Output(s)          : Updated pRt6Info
 *
 * Returns            : RTM_SUCCESS/RTM_FAILURE
 *
 * Action :
 * This procedure searches for the route node in the temporary RTM RED
 * table and if a node is found, the route's flag information is updated
 * and the node is removed from RED table.
+-------------------------------------------------------------------*/
INT4
Rtm6RedUpdateRtParams (tIp6RtEntry * pRt6Info, UINT4 u4Context)
{
    UNUSED_PARAM (pRt6Info);
    UNUSED_PARAM (u4Context);
    return RTM6_FAILURE;
}

/************************************************************************/
/* Function Name      : Rtm6RedSendFrtBulkUpdMsg                         */
/*                                                                      */
/* Description        : This function sends the binding table dynamic   */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/
VOID
Rtm6RedSendFrtBulkUpdMsg (VOID)
{
    return;
}
/***********************************************************************
 * Function Name      : Rtm6RedSendFrtBulkInfo                         
 *                                                                      
 * Description        : This function sends the FRT Bulk update messages to 
 *                      the peer standby RTM.                           
 *                                                                      
 * Input(s)           : None                                            
 *                                                                      
 * Output(s)          : pu1BulkUpdPendFlg                               
 *                                                                      
 * Returns            : RTM_SUCCESS/RTM_FAILURE                         
 ***********************************************************************/
INT4
Rtm6RedSendFrtBulkInfo (UINT1 *pu1BulkUpdPendFlg)
{
    UNUSED_PARAM (pu1BulkUpdPendFlg);
    return RTM6_SUCCESS;
}
/************************************************************************
 * Function Name      : Rtm6RedSyncFrtInfo 
 *                                                
 * Description        : This function sends the binding table dynamic  
 *                      sync up message to Standby node from the Active 
 *                      node, when the RTM offers an IP address 
 *                      to the RTM client and dynamically update the   
 *                      binding table entries.                        
 *                                                                   
 * Input(s)           : None
 *                                                                    
 * Output(s)          : None                                         
 *                                                                  
 * Returns            : OSIX_SUCCESS / OSIX_FAILURE                
 ************************************************************************/
VOID
Rtm6RedSyncFrtInfo (tIp6RtEntry * pIp6RtEntry, UINT4 u4CxtId, UINT1 u1Type ,
                    UINT1 u1Flag)
{
      UNUSED_PARAM(pIp6RtEntry);
      UNUSED_PARAM(u4CxtId);
      UNUSED_PARAM(u1Type);
      UNUSED_PARAM(u1Flag);
      return;
}
/************************************************************************/
/* Function Name      : Rtm6RedProcessFrtBulkInfo                       */
/*                                                                      */
/* Description        : This function decodes the sync-up message from  */
/*                      Active node and updates the binding information */
/*                      at the Standby node.                            */
/*                                                                      */
/* Input(s)           : pMsg - RM Sync-up message                       */
/*                      u2RemMsgLen - Length of value of Message        */
/*                                                                      */
/* Output(s)          : u2OffSet - OffSet value                         */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

VOID
Rtm6RedProcessFrtBulkInfo (tRmMsg * pMsg, UINT2 *pu2OffSet)
{
      UNUSED_PARAM(pMsg);
      UNUSED_PARAM(pu2OffSet);
      return;
}
/************************************************************************/
/* Function Name      : Rtm6RedProcessSyncFrtInfo                        */
/*                                                                      */
/* Description        : This function decodes the sync-up message from  */
/*                      Active node and updates the binding information */
/*                      at the Standby node.                            */
/*                                                                      */
/* Input(s)           : pMsg - RM Sync-up message                       */
/*                      u2RemMsgLen - Length of value of Message        */
/*                                                                      */
/* Output(s)          : u2OffSet - OffSet value                         */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

VOID
Rtm6RedProcessSyncFrtInfo (tRmMsg * pMsg, UINT2 *pu2OffSet)
{
      UNUSED_PARAM(pMsg);
      UNUSED_PARAM(pu2OffSet);
      return;
}
#endif
