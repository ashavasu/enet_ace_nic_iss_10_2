
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rtm6filt.c,v 1.8 2014/06/24 12:46:23 siva Exp $
 *
 * Description:This file contains functions which process   
 *             the change in the rrd6 filter table and       
 *             functions which checks whether a route can be
 *             can be redistributed or not.                 
 *******************************************************************/

#include "rtm6inc.h"

/*-------------------------------------------------------------------------+
 *
 * Function           : Rtm6CheckRouteForRedistributionInCxt 
 *
 * Input(s)           : pRegnId - Registration ID for protocol from which
 *                                this route is learnt.
 *                      pRtInfo - Pointer to the route in the routing table.
 *                      pRtm6Cxt - RTM6 Context Pointer.
 *
 * Output(s)          : pu2DestProtoMask - Pointer to the destination protocol
 *                      mask to which this route can be redistributed. 
 *
 * Returns            : None. 
 *
 * Action             : This function handles the addition or deletion of
 *                      in the RRD6 Control table.  
 *         
+--------------------------------------------------------------------------*/
VOID
Rtm6CheckRouteForRedistributionInCxt (tRtm6Cxt * pRtm6Cxt,
                                      tRtm6RegnId * pRegnId,
                                      tIp6RtEntry * pRtInfo,
                                      UINT2 *pu2DestProtoMask)
{
    tRtm6RegnId         DestRegnId;
    UINT2               u2DestProtoMask = 0;
    UINT2               u2RegnId = 0;

    *pu2DestProtoMask = 0;

    /* Check whether any allow inter/intra area routes is enabled
     * if the route is from OSPF.
     */
    if (pRegnId->u2ProtoId == OSPF6_ID)
    {
        /* This route is to be redistributed only to these mib enabled
         * routing protocols.
         */
        for (u2RegnId = pRtm6Cxt->u2Rtm6RtStartIndex;
             u2RegnId < IP6_MAX_ROUTING_PROTOCOLS;
             u2RegnId = pRtm6Cxt->aRtm6RegnTable[u2RegnId].u2NextRegId)
        {
            if (pRtm6Cxt->aRtm6RegnTable[u2RegnId].u2RoutingProtocolId
                == OSPF6_ID)
            {
                continue;
            }

            DestRegnId.u2ProtoId =
                pRtm6Cxt->aRtm6RegnTable[u2RegnId].u2RoutingProtocolId;
            DestRegnId.u4ContextId = pRtm6Cxt->u4ContextId;
            /* Set the DestProtoMask for this Routing Protocol.
             * If route is not to be redistributed to this protocol
             * then the mask will be resetted. */
            RTM6_SET_BIT (*pu2DestProtoMask,
                          pRtm6Cxt->aRtm6RegnTable[u2RegnId].
                          u2RoutingProtocolId);
            if (Rtm6CheckForMatch
                (pRtm6Cxt->aRtm6RegnTable[u2RegnId].u2ExportProtoMask,
                 pRtInfo->u1MetricType) == RTM6_FAILURE)
            {
                RTM6_CLEAR_BIT (*pu2DestProtoMask,
                                pRtm6Cxt->aRtm6RegnTable[u2RegnId].
                                u2RoutingProtocolId);
            }
            Rtm6CheckForOspfIntOrExtInCxt (pRtm6Cxt, &DestRegnId,
                                           pRtInfo->u4RouteTag,
                                           pu2DestProtoMask);
        }

        if (*pu2DestProtoMask == 0)
        {
            /* Route redistribution is not permitted. */
            return;
        }
    }
    if (pRegnId->u2ProtoId == ISIS6_ID)
    {
        /* This route is to be redistributed only to these mib enabled
         * routing protocols.
         */
        for (u2RegnId = pRtm6Cxt->u2Rtm6RtStartIndex;
             u2RegnId < IP6_MAX_ROUTING_PROTOCOLS;
             u2RegnId = pRtm6Cxt->aRtm6RegnTable[u2RegnId].u2NextRegId)
        {
            if (pRtm6Cxt->aRtm6RegnTable[u2RegnId].u2RoutingProtocolId ==
                ISIS6_ID)
            {
                continue;
            }
            if (Rtm6CheckForLevel
                (pRtm6Cxt->aRtm6RegnTable[u2RegnId].u2ExportProtoMask,
                 pRtInfo->u1MetricType) == RTM6_FAILURE)
            {
                RTM6_SET_BIT (*pu2DestProtoMask,
                              pRtm6Cxt->aRtm6RegnTable[u2RegnId].
                              u2RoutingProtocolId);
            }
        }
    }

    Rtm6FilterCheckInCxt (pRtm6Cxt, pRegnId, pRtInfo, &u2DestProtoMask);

    if (u2DestProtoMask != 0)
    {
        Rtm6UpdateDestMaskInCxt (pRtm6Cxt, pRtInfo, pRegnId, &u2DestProtoMask);
    }

    if (pRegnId->u2ProtoId == OSPF6_ID)
    {
        *pu2DestProtoMask &= u2DestProtoMask;
    }
    else
    {
        *pu2DestProtoMask |= u2DestProtoMask;
    }
    return;
}

/*-------------------------------------------------------------------+
 *
 * Function           : Rtm6UpdateDestMaskInCxt
 *
 * Input(s)           : pRtInfo - Pointer to the route in the routing table.
 *                      pSrcRegnId - Info about Source of this route.
 *                      pRtm6Cxt - RTM6 Context Pointer
 *
 * Output(s)          : pu2DestProtoMask - Pointer to the destination protocol
 *                      mask to which this route can be redistributed. 
 *
 * Returns            : None. 
 *
 * Action             : This function handles the filtering based on the
 *                      tag and AS number.
 *         
+-------------------------------------------------------------------*/
VOID
Rtm6UpdateDestMaskInCxt (tRtm6Cxt * pRtm6Cxt, tIp6RtEntry * pRtInfo,
                         tRtm6RegnId * pSrcRegnId, UINT2 *pu2DestProtoMask)
{
    tRtm6RegnId         DestRegnId;
    UINT2               u2RegnIdOfRp = 0;

    switch (pSrcRegnId->u2ProtoId)
    {
        case IP6_NETMGMT_PROTOID:
            /* no need to check for any more thing */
            break;

        case RIPNG_ID:
            /* Check for TAG values */
            if (pRtm6Cxt->Rtm6ConfigInfo.u1Rrd6FilterByOspfTag ==
                RTM6_FILTER_ENABLED)
            {
                /* Check the subfields of the TAG */

                if ((pRtInfo->u4RouteTag &
                     pRtm6Cxt->Rtm6ConfigInfo.u4OspfTagMask) ==
                    (pRtm6Cxt->Rtm6ConfigInfo.u4OspfTagValue &
                     pRtm6Cxt->Rtm6ConfigInfo.u4OspfTagMask))
                {
                    /* This route should not be exported to any one else */
                    *pu2DestProtoMask = 0;
                }

            }
            break;

        case OSPF6_ID:
            /* Check for filtering based on OSPF TAG */
            if (pRtm6Cxt->Rtm6ConfigInfo.u1Rrd6FilterByOspfTag ==
                RTM6_FILTER_ENABLED)
            {
                /* Check the subfields of the TAG */

                if ((pRtInfo->u4RouteTag &
                     pRtm6Cxt->Rtm6ConfigInfo.u4OspfTagMask) ==
                    (pRtm6Cxt->Rtm6ConfigInfo.u4OspfTagValue &
                     pRtm6Cxt->Rtm6ConfigInfo.u4OspfTagMask))
                {
                    /* This route should not be exported to any one else */
                    *pu2DestProtoMask = 0;
                }
                break;
            }

            /* check for the filtering based on OSPF internal or external 
               routes */
            /* OSPF routes can be redistributed to RIP and BGP only */
            if (*pu2DestProtoMask & RTM6_RIP_MASK)
            {
                DestRegnId.u2ProtoId = RIPNG_ID;
                DestRegnId.u4ContextId = pRtm6Cxt->u4ContextId;
                u2RegnIdOfRp = Rtm6GetRegnIndexFromRegnIdInCxt (pRtm6Cxt,
                                                                &DestRegnId);
                if (u2RegnIdOfRp == RTM6_INVALID_REGN_ID)
                {
                    /* RIP not registered. No need to redistribute. */
                    *pu2DestProtoMask =
                        (UINT2) (*pu2DestProtoMask & ~RTM6_RIP_MASK);
                }
            }                    /* RIP_MASK */

            if (*pu2DestProtoMask & RTM6_BGP_MASK)
            {
                if (((pRtInfo->u4RouteTag & RTM6_LEFT_MOST_NIBBLE_TAG_MASK)
                     == RTM6_BGP_DENY_TAG1) ||
                    ((pRtInfo->u4RouteTag & RTM6_LEFT_MOST_NIBBLE_TAG_MASK)
                     == RTM6_BGP_DENY_TAG2))
                {
                    *pu2DestProtoMask =
                        (UINT2) (*pu2DestProtoMask & ~RTM6_BGP_MASK);
                    break;
                }
                DestRegnId.u2ProtoId = BGP6_ID;
                DestRegnId.u4ContextId = pRtm6Cxt->u4ContextId;

                u2RegnIdOfRp =
                    Rtm6GetRegnIndexFromRegnIdInCxt (pRtm6Cxt, &DestRegnId);
                if (u2RegnIdOfRp == RTM6_INVALID_REGN_ID)
                {
                    /* BGP not registered. No need to redistribute. */
                    *pu2DestProtoMask =
                        (UINT2) (*pu2DestProtoMask & ~RTM6_BGP_MASK);
                }
            }                    /* BGP_MASK */

            break;

        case BGP6_ID:
            /* Check for the BIT MASK received from BGP */
            /* If a route with bitmask = 0x80 is received from BGP, then this
               route should not be redistributed to other RP's. This route is
               filtered based on AS number */
            if (pRtInfo->u1BitMask & RTM6_FILTER_BASED_ON_AS_MASK)
            {
                *pu2DestProtoMask = 0;
            }
            break;

        default:
            break;

    }                            /* End of SWITCH */
}

/*-------------------------------------------------------------------+
 *
 * Function           : Rtm6CheckForOspfIntOrExtInCxt
 *
 * Input(s)           : pRegnId - Registration Info of the routing protocol
 *                      to which this route can be exported or not.
 *                      u4Tag        - Value of the route tag.
 *                      pRtm6Cxt - RTM6 Context Pointer
 *
 * Output(s)          : *pu2DestProtoMask - Clears the bit if this route should
 *                      not be redistributed. 
 *
 * Returns            : None. 
 *
 * Action             : This function updates the mask and filters destinations
 *                      based on OSPF internal or external.
 *         
+-------------------------------------------------------------------*/
VOID
Rtm6CheckForOspfIntOrExtInCxt (tRtm6Cxt * pRtm6Cxt, tRtm6RegnId * pRegnId,
                               UINT4 u4Tag, UINT2 *pu2DestProtoMask)
{
    UINT2               u2RegnIdOfRp = 0;
    UINT2               u2Mask = 0;

    u2RegnIdOfRp = Rtm6GetRegnIndexFromRegnIdInCxt (pRtm6Cxt, pRegnId);

    if (u2RegnIdOfRp == RTM6_INVALID_REGN_ID)
    {
        return;
    }

    RTM6_SET_BIT (u2Mask,
                  pRtm6Cxt->aRtm6RegnTable[u2RegnIdOfRp].u2RoutingProtocolId);

    if (u4Tag == 0)
    {
        /* This is a OSPF internal route */
        /* Check whether internal routes are allowed to this RP or Not */
        if (pRtm6Cxt->aRtm6RegnTable[u2RegnIdOfRp].u1AllowOspfInternals ==
            RTM6_REDISTRIBUTION_DISABLED)
        {
            /* This RP dont want OSPF inter area or intra area routes */
            *pu2DestProtoMask = (UINT2) (*pu2DestProtoMask & ~u2Mask);
        }
    }
    else
    {
        /* This is OSPF external route */
        if (pRtm6Cxt->aRtm6RegnTable[u2RegnIdOfRp].u1AllowOspfExternals ==
            RTM6_REDISTRIBUTION_DISABLED)
        {
            /* This RP dont want OSPF external type 1 or type 2 routes */
            *pu2DestProtoMask = (UINT2) (*pu2DestProtoMask & ~u2Mask);
        }
    }

}

/*-------------------------------------------------------------------+
 *
 * Function           : Rtm6FilterCheckInCxt
 *
 * Input(s)           : pSrcRegnId - Registration Info about the Routing Protocol
 *                                   from which this route is learnt.
 *                      pRtInfo - Pointer to the route info structure.
 *                      pRtm6Cxt - RTM6 Context Pointer
 *
 * Output(s)          : *pu2DestProtoMask - Bit mask of the routing protocols
 *                      to which this route can be redistributed. 
 *
 * Returns            : None. 
 *
 * Action             : This function checks the filters configured by the     
 *                      administrator in the RTM6 Control table and allows
 *                      or denies this route to the routing protocols based on
 *                      the configuration.
 *         
+-------------------------------------------------------------------*/
VOID
Rtm6FilterCheckInCxt (tRtm6Cxt * pRtm6Cxt, tRtm6RegnId * pSrcRegnId,
                      tIp6RtEntry * pRtInfo, UINT2 *pu2DestProtoMask)
{
    tRrd6ControlInfo   *pRtm6CtrlInfo = NULL;
    tIp6Addr            Ip6AddrAny;
    tIp6Addr            Ip6AddrRange;

    MEMSET (&Ip6AddrAny, 0, sizeof (tIp6Addr));
    MEMSET (&Ip6AddrRange, 0, sizeof (tIp6Addr));
    *pu2DestProtoMask = 0;

    /* All entries in the Control Table are sorted based on DestNet, in
     * decending order. So if the given route matches with any entry, just
     * return because this will be the longest possible prefix match and no
     * need to process any other entry.
     */
    TMO_SLL_Scan (&(pRtm6Cxt->Rtm6CtrlList), pRtm6CtrlInfo, tRrd6ControlInfo *)
    {
        if (pRtm6CtrlInfo->u1RowStatus == RTM6_ACTIVE)
        {
            /* See whether the destination net occurs in the specified range
               or not */
            if (Ip6AddrMatch (&pRtm6CtrlInfo->DestNet, &Ip6AddrAny,
                              IP6_ADDR_MAX_PREFIX) == TRUE)
            {
                MEMSET (&Ip6AddrRange, IP6_UINT1_ALL_ONE, IP6_ADDR_SIZE);
            }
            else
            {
                Rtm6AddrMask (&Ip6AddrRange, &pRtm6CtrlInfo->DestNet,
                              pRtm6CtrlInfo->u1Prefixlen);
            }

            if (((Ip6AddrMatch (&pRtInfo->dst, &pRtm6CtrlInfo->DestNet,
                                pRtm6CtrlInfo->u1Prefixlen) == TRUE) &&
                 (pRtInfo->u1Prefixlen == pRtm6CtrlInfo->u1Prefixlen)) ||
                (Ip6AddrMatch (&pRtInfo->dst, &Ip6AddrRange,
                               IP6_ADDR_MAX_PREFIX) == TRUE) ||
                ((Ip6IsAddrGreater (&pRtInfo->dst, &pRtm6CtrlInfo->DestNet) ==
                  IP6_FAILURE) &&
                 (pRtInfo->u1Prefixlen <= pRtm6CtrlInfo->u1Prefixlen) &&
                 (Ip6IsAddrGreater (&Ip6AddrRange, &pRtInfo->dst) ==
                  IP6_FAILURE)))
            {
                /* Route occurs in the specified range */
                /* Check whether source protocol ID in the filter is any 
                   protocol (0) or the specified protocol */
                if ((pSrcRegnId->u2ProtoId == pRtm6CtrlInfo->u1SrcProtocolId) ||
                    (pRtm6CtrlInfo->u1SrcProtocolId == RTM6_ANY_PROTOCOL))
                {
                    /* Set the Destination Protocol Mask appropriately */
                    if (pRtm6CtrlInfo->u1RtExportFlag == RTM6_ROUTE_PERMIT)
                    {
                        if (pRtm6CtrlInfo->u2DestProtocolMask ==
                            RTM6_ANY_PROTOCOL)
                        {
                            *pu2DestProtoMask |= RTM6_ALL_RPS_MASK;
                            RTM6_CLEAR_BIT (*pu2DestProtoMask,
                                            pSrcRegnId->u2ProtoId);
                        }
                        else
                        {
                            *pu2DestProtoMask |=
                                pRtm6CtrlInfo->u2DestProtocolMask;
                            RTM6_CLEAR_BIT (*pu2DestProtoMask,
                                            pSrcRegnId->u2ProtoId);
                        }
                    }
                    else
                    {
                        /* Set Destination Protocol Mask appropriately */
                        if (pRtm6CtrlInfo->u2DestProtocolMask ==
                            RTM6_ANY_PROTOCOL)
                        {
                            *pu2DestProtoMask = 0;
                        }
                        else
                        {
                            *pu2DestProtoMask = RTM6_ALL_RPS_MASK;
                            *pu2DestProtoMask &=
                                ~pRtm6CtrlInfo->u2DestProtocolMask;
                            RTM6_CLEAR_BIT (*pu2DestProtoMask,
                                            pSrcRegnId->u2ProtoId);
                        }
                    }
                    /* Longest Prefix Match is found. */
                    return;
                }                /* Source Protocol ID match */
            }
        }                        /* ACTIVE/DELETE IN-PROGRESS ENTRY */
    }                            /* Scan thru the filter list */
    return;
}

/*-------------------------------------------------------------------+
 *
 * Function           : Rtm6HandleChgInDefMode
 *
 * Input(s)           : pNewFilter - Pointer to the changed filter.
 *                      u1Status   - New Status of this entry.
 *
 * Output(s)          : None
 *
 * Returns            : None
 *
 * Action             : This function handles the change in the mode of the
 *                      RRD6 Control table.  
 *         
+-------------------------------------------------------------------*/
VOID
Rtm6HandleChgInDefMode (tRrd6ControlInfo * pNewFilter, UINT1 u1Status)
{
    tRrd6ControlInfo   *pRtm6CtrlInfo = NULL;

    TMO_SLL_Scan (&(pNewFilter->pRtm6Cxt->Rtm6CtrlList), pRtm6CtrlInfo,
                  tRrd6ControlInfo *)
    {
        /* Check whether the IP address and mask match */
        if ((Ip6AddrMatch (&pRtm6CtrlInfo->DestNet, &pNewFilter->DestNet,
                           IP6_ADDR_MAX_PREFIX) == TRUE) &&
            (pRtm6CtrlInfo->u1Prefixlen == pNewFilter->u1Prefixlen))
        {
            /* We found the correct node in the list */
            break;
        }
    }

    if (pRtm6CtrlInfo == NULL)
    {
        /* Entry is not present */
        return;
    }

    pNewFilter->u1RtExportFlag = u1Status;

    Rtm6HandleNewEntryToControlTable (pNewFilter);
    return;
}

/*-------------------------------------------------------------------+
 *
 * Function           : Rtm6HandleChgInFilterTable
 *
 * Input(s)           : pNewFilter - Pointer to the changed filter.
 *                      u1Status   - New Status of this entry.
 *
 * Output(s)          : None
 *
 * Returns            : None
 *
 * Action             : This function handles the addition or deletion of
 *                      in the RRD6 Control table.  
 *         
+-------------------------------------------------------------------*/
VOID
Rtm6HandleChgInFilterTable (tRrd6ControlInfo * pNewFilter, UINT1 u1Status)
{
    tRrd6ControlInfo   *pRtm6CtrlInfo = NULL;

    TMO_SLL_Scan (&(pNewFilter->pRtm6Cxt->Rtm6CtrlList), pRtm6CtrlInfo,
                  tRrd6ControlInfo *)
    {
        /* Check whether the IP address and mask match */
        if ((Ip6AddrMatch (&pRtm6CtrlInfo->DestNet, &pNewFilter->DestNet,
                           IP6_ADDR_MAX_PREFIX) == TRUE) &&
            (pRtm6CtrlInfo->u1Prefixlen == pNewFilter->u1Prefixlen))
        {
            /* We found the correct node in the list */
            if (pRtm6CtrlInfo->u1RowStatus == u1Status)
            {
                /* No change in the status of this entry. */
                return;
            }
            else
            {
                break;
            }
        }
    }

    if (pRtm6CtrlInfo == NULL)
    {
        /* Entry is not present */
        return;
    }

    pNewFilter->u1RowStatus = u1Status;

    if (pNewFilter->u1RowStatus == RTM6_ACTIVE)
    {
        Rtm6HandleNewEntryToControlTable (pNewFilter);
    }
    else if (pNewFilter->u1RowStatus == RTM6_DESTROY)
    {
        Rtm6DeleteAnEntryFromControlTable (pNewFilter);
        /* Dont process the Filter Table entry beyond this point, because
         * it is already freed. */
    }

    return;
}

/*-------------------------------------------------------------------+
 *
 * Function           : Rtm6HandleOspfTypeRouteRedistribution
 *
 * Input(s)           : pRtInfo - Info about the best route to be 
 *                                redistributed.
 *
 * Output(s)          : None
 *
 * Returns            : RTM6_SUCCESS/RTM6_FAILURE
 *
 * Action             : This function is the call back function to 
 *                      handle the route redistribution of the given
 *                      best route when ever there is change in the
 *                      Ospf Route Advt Policy Change.
 *
+-------------------------------------------------------------------*/
INT4
Rtm6HandleOspfTypeRouteRedistribution (tIp6RtEntry * pRtInfo,
                                       VOID *pAppSpecData)
{
    tRtm6OspfTypeChg   *pOspfTypeChg = NULL;

    if (pRtInfo->i1Proto != OSPF6_ID)
    {
        return RTM6_SUCCESS;
    }

    pOspfTypeChg = (tRtm6OspfTypeChg *) pAppSpecData;

    if (pOspfTypeChg->u1OspfRtType == RTM6_OSPF_INTERNAL_ROUTES)
    {
        /* Change in OSPF Internal Route policy */
        if (pRtInfo->u4RouteTag != 0)
        {
            /* AS External Route */
            return RTM6_SUCCESS;
        }
    }
    else
    {
        /* Change in OSPF External Route policy */
        if (pRtInfo->u4RouteTag == 0)
        {
            /* AS Internal Route */
            return RTM6_SUCCESS;
        }
    }

    if (pOspfTypeChg->u1OspfStatus == RTM6_REDISTRIBUTION_ENABLED)
    {
        /* New Policy is to advertise */
        Rtm6HandleRouteUpdatesInCxt (pOspfTypeChg->pRtm6Cxt,
                                     pRtInfo, TRUE, IP6_BIT_ALL);
    }
    else
    {
        /* New Policy is to withdraw. */
        Rtm6HandleRouteUpdatesInCxt (pOspfTypeChg->pRtm6Cxt,
                                     pRtInfo, FALSE, IP6_BIT_STATUS);
    }

    return RTM6_SUCCESS;
}

/*-------------------------------------------------------------------+
 *
 * Function           : Rtm6HandleChgInOspfType
 *
 * Input(s)           : pRegnId - Info about Routing protocol in which there is
 *                      a change in the allow of OSPF internal or external
 *                      routes.
 *                      u1OspfType - Indicates whether OSPF internal or
 *                      external.
 *                      u1PermitOrDeny - Indicates redistribution enabled or
 *                      disabled.
 *
 * Output(s)          : None.
 *
 * Returns            : None. 
 *
 * Action             : This function will be called whenever there is a change
 *                      in the allow/deny of OSPF routes to this routing 
 *                      protocol. This function calls appropriate routines to
 *                      send appropriate messages. 
 *         
+-------------------------------------------------------------------*/
VOID
Rtm6HandleChgInOspfType (tRtm6RegnId * pRegnId, UINT1 u1OspfType,
                         UINT1 u1PermitOrDeny)
{
    tRtm6OspfTypeChg    OspfTypeChg;
    tIp6Addr            InAddr;
    tIp6Addr            OutAddr;
    UINT1               u1InPrefixLen = 0;
    UINT1               u1OutPrefixLen = 0;
    UINT2               u2RegnId = 0;
    tRtm6Cxt           *pRtm6Cxt = NULL;

    pRtm6Cxt = UtilRtm6GetCxt (pRegnId->u4ContextId);

    if (pRtm6Cxt == NULL)
    {
        return;
    }

    /* The destination protocol ID can be RIP or BGP only */
    u2RegnId = Rtm6GetRegnIndexFromRegnIdInCxt (pRtm6Cxt, pRegnId);

    if (u2RegnId == RTM6_INVALID_REGN_ID)
    {
        return;
    }

    if (u1PermitOrDeny == RTM6_REDISTRIBUTION_ENABLED)
    {
        if (u1OspfType == RTM6_OSPF_INTERNAL_ROUTES)
        {
            pRtm6Cxt->aRtm6RegnTable[u2RegnId].u1AllowOspfInternals
                = u1PermitOrDeny;
        }
        else
        {
            pRtm6Cxt->aRtm6RegnTable[u2RegnId].u1AllowOspfExternals
                = u1PermitOrDeny;
        }
    }

    OspfTypeChg.u2ProtoId = pRegnId->u2ProtoId;
    OspfTypeChg.u1OspfRtType = u1OspfType;
    OspfTypeChg.u1OspfStatus = u1PermitOrDeny;
    OspfTypeChg.pRtm6Cxt = pRtm6Cxt;

    /* This function scans thru the routing table for OSPF routes
       and redistributes the route as per the new policy */
    MEMSET (&InAddr, 0, sizeof (tIp6Addr));
    MEMSET (&OutAddr, 0, sizeof (tIp6Addr));
    u1InPrefixLen = 0;
    u1OutPrefixLen = 0;

    Ip6ScanRouteTableForBestRouteInCxt (pRtm6Cxt, &InAddr, u1InPrefixLen,
                                        Rtm6HandleOspfTypeRouteRedistribution,
                                        0, &OutAddr, &u1OutPrefixLen,
                                        (VOID *) &OspfTypeChg);

    if (u1PermitOrDeny == RTM6_REDISTRIBUTION_DISABLED)
    {
        if (u1OspfType == RTM6_OSPF_INTERNAL_ROUTES)
        {
            pRtm6Cxt->aRtm6RegnTable[u2RegnId].u1AllowOspfInternals
                = u1PermitOrDeny;
        }
        else
        {
            pRtm6Cxt->aRtm6RegnTable[u2RegnId].u1AllowOspfExternals
                = u1PermitOrDeny;
        }
    }

    return;
}

/*-------------------------------------------------------------------+
 *
 * Function           : Rtm6DeleteAnEntryFromControlTable
 *
 * Input(s)           : pControlTableEntry- Entry to be deleted from the control
 *                      table.
 *
 * Output(s)          : None.
 *
 * Returns            : None. 
 *
 * Action             : This function deletes an entry from the control table.
 *                      When the control table is in Permit mode, delete the 
 *                      routes from Export list and inform the routing protocol
 *                      When the control table is in Deny mode, add the routes 
 *                      to export list, picking from CRT.
 *         
+-------------------------------------------------------------------*/
VOID
Rtm6DeleteAnEntryFromControlTable (tRrd6ControlInfo * pControlTableEntry)
{
    tRrd6ControlInfo   *pRtm6CtrlInfo = NULL;

    /* Delete the entry from the control list before processing
     * the routes. */
    TMO_SLL_Scan (&(pControlTableEntry->pRtm6Cxt->Rtm6CtrlList), pRtm6CtrlInfo,
                  tRrd6ControlInfo *)
    {
        if ((Ip6AddrMatch (&pRtm6CtrlInfo->DestNet,
                           &pControlTableEntry->DestNet,
                           IP6_ADDR_MAX_PREFIX) == TRUE) &&
            (pRtm6CtrlInfo->u1Prefixlen == pControlTableEntry->u1Prefixlen))
        {
            TMO_SLL_Delete (&(pControlTableEntry->pRtm6Cxt->Rtm6CtrlList),
                            &(pRtm6CtrlInfo->pNext));
            break;
        }
    }

    if (pRtm6CtrlInfo == NULL)
    {
        /* Entry not found. */
        return;
    }

    if (pControlTableEntry->u1RtExportFlag == RTM6_ROUTE_PERMIT)
    {
        /* 
         *  Collect the redistributed routes, that falls in this range
         *  and send the delete message to Routing Protocols. 
         */
        Rtm6FilterRedistributedRoutes (pControlTableEntry);
    }
    else if (pControlTableEntry->u1RtExportFlag == RTM6_ROUTE_DENY)
    {
        /* 
         *  Collect the routes, that falls in this range and
         *  if necessary redistribute the route to Routing Protocols. 
         */
        Rtm6GetRoutesFromCRTToRPs (pControlTableEntry);
    }

    /* Free the control table entry */
    RTM6_CONTROL_INFO_FREE (pRtm6CtrlInfo);
    return;
}

/*-------------------------------------------------------------------+
 *
 * Function           : Rtm6HandleNewEntryToControlTable
 *
 * Input(s)           : pNewFilter- Entry to be handled for the control
 *                      table.
 *
 * Output(s)          : None.
 *
 * Returns            : None. 
 *
 * Action             : This function adds the routes from CRT to export list
 *                      if the filter is permit mode for Permit table; else 
 *                      deletes the routes fall on this filter, from the export 
 *                      list and informs the routing protocols.
+-------------------------------------------------------------------*/
VOID
Rtm6HandleNewEntryToControlTable (tRrd6ControlInfo * pNewFilter)
{
    if (pNewFilter->u1RtExportFlag == RTM6_ROUTE_DENY)
    {
        /* Adding a new deny filter. Get the matching routes from RIB and if
         * already redistributed then withdrawn them. */
        Rtm6FilterRedistributedRoutes (pNewFilter);
    }
    else
    {
        /* Adding a new permit filter. Get the matching routes from RIB and if
         * already not redistributed, redistribute them. */
        Rtm6GetRoutesFromCRTToRPs (pNewFilter);
    }
    return;
}

/*-------------------------------------------------------------------+
 *
 * Function           : Rtm6HandleFilteredRouteRedistribution
 *
 * Input(s)           : pRtInfo - Info about the best route to be 
 *                                redistributed.
 *
 * Output(s)          : None
 *
 * Returns            : RTM6_SUCCESS/RTM6_FAILURE
 *
 * Action             : This function is the call back function to 
 *                      handle the route redistribution of the given
 *                      best route when ever there is change in the
 *                      filter policy.
 *
+-------------------------------------------------------------------*/
INT4
Rtm6HandleFilteredRouteRedistribution (tIp6RtEntry * pRtInfo,
                                       VOID *pAppSpecData)
{
    tRrd6ControlInfo   *pNewFilter = NULL;
    tRtm6RegnId         RegnId;
    tIp6Addr            Ip6AddrAny;
    tIp6Addr            Ip6AddrRange;
    UINT2               u2SrcProtoMask = 0;
    UINT2               u2FilterSrcProtoMask = 0;
    UINT2               u2DestProtoMask = RTM6_DONT_REDISTRIBUTE;
    UINT2               u2SendProtoMask = 0;
    UINT2               u2DelProtoMask = 0;

    MEMSET (&Ip6AddrAny, 0, sizeof (tIp6Addr));
    MEMSET (&Ip6AddrRange, 0, sizeof (tIp6Addr));

    pNewFilter = (tRrd6ControlInfo *) pAppSpecData;

    if (pNewFilter->u1SrcProtocolId == RTM6_ANY_PROTOCOL)
    {
        u2FilterSrcProtoMask = (UINT2) (~RTM6_ANY_PROTOCOL);
    }
    else
    {
        RTM6_SET_BIT (u2FilterSrcProtoMask, pNewFilter->u1SrcProtocolId);
    }

    RTM6_SET_BIT (u2SrcProtoMask, pRtInfo->i1Proto);

    if (Ip6AddrMatch (&pNewFilter->DestNet, &Ip6AddrAny, IP6_ADDR_MAX_PREFIX)
        == TRUE)
    {
        MEMSET (&Ip6AddrRange, 0xFF, IP6_ADDR_SIZE);
    }
    else
    {
        Rtm6AddrMask (&Ip6AddrRange, &pNewFilter->DestNet,
                      pNewFilter->u1Prefixlen);
    }

    if (((Ip6AddrMatch (&pRtInfo->dst, &pNewFilter->DestNet,
                        pNewFilter->u1Prefixlen) == TRUE) &&
         (pRtInfo->u1Prefixlen == pNewFilter->u1Prefixlen)) ||
        (Ip6AddrMatch (&pRtInfo->dst, &Ip6AddrRange,
                       IP6_ADDR_MAX_PREFIX) == TRUE) ||
        ((Ip6IsAddrGreater (&pRtInfo->dst, &pNewFilter->DestNet) ==
          IP6_FAILURE) &&
         (pRtInfo->u1Prefixlen <= pNewFilter->u1Prefixlen) &&
         ((Ip6IsAddrGreater (&Ip6AddrRange, &pRtInfo->dst)) == IP6_FAILURE)))
    {
        /* Route matches the configured control entry. */
        /* Need to redistribute this route */
        RegnId.u2ProtoId = (UINT2) pRtInfo->i1Proto;
        RegnId.u4ContextId = pNewFilter->pRtm6Cxt->u4ContextId;

        /* Check whether the route can be redistributed. */
        Rtm6CheckRouteForRedistributionInCxt (pNewFilter->pRtm6Cxt, &RegnId,
                                              pRtInfo, &u2DestProtoMask);
        if (u2DestProtoMask != RTM6_DONT_REDISTRIBUTE)
        {
            /* Route can be redistributed to the protocols specified by
             * u2DestProtoMask. Check whether the route is already 
             * redistributed to these protocols or not. If not redistributed
             * then send it. If already redistributed and cannot be
             * redistributed now, then withdraw it. */
            u2SendProtoMask = (UINT2) (u2DestProtoMask &
                                       (~
                                        (u2DestProtoMask & pRtInfo->
                                         u2RedisMask)));
            u2DelProtoMask =
                (UINT2) (pRtInfo->
                         u2RedisMask &
                         (~(u2DestProtoMask & pRtInfo->u2RedisMask)));
            if (u2SendProtoMask != 0)
            {
                /* Redistribute the route to these protocols */
                Rtm6RouteRedistributionInCxt (pNewFilter->pRtm6Cxt, pRtInfo,
                                              TRUE, IP6_BIT_ALL,
                                              u2SendProtoMask);
            }
            if (u2DelProtoMask != 0)
            {
                /* Withdraw the route from these protocols */
                Rtm6RouteRedistributionInCxt (pNewFilter->pRtm6Cxt, pRtInfo,
                                              FALSE, IP6_BIT_ALL,
                                              u2DelProtoMask);
            }
        }
        else
        {
            /* Route need not be redistributed as per current Control Table.
             * Check whether the route is already redistributed to any other
             * protocol. If yes, the withdraw them. */
            if (pRtInfo->u2RedisMask != 0)
            {
                Rtm6RouteRedistributionInCxt (pNewFilter->pRtm6Cxt, pRtInfo,
                                              FALSE, IP6_BIT_ALL,
                                              pRtInfo->u2RedisMask);
            }
        }
    }

    return RTM6_SUCCESS;
}

/*-------------------------------------------------------------------+
 *
 * Function           : Rtm6GetRoutesFromCRTToRPs
 *
 * Input(s)           : pNewFilter- Control table Entry used to get 
 *                      the routes falls in this range, from CRT table.
 *
 * Output(s)          : None.
 *
 * Returns            : None. 
 *
 * Action             : Gets the routes falling in the filter and adds to 
 *                      the export list.
+-------------------------------------------------------------------*/
VOID
Rtm6GetRoutesFromCRTToRPs (tRrd6ControlInfo * pNewFilter)
{
    tIp6Addr            InAddr;
    tIp6Addr            OutAddr;
    UINT1               u1InPrefixLen = 0;
    UINT1               u1OutPrefixLen = 0;

    /* This function will take care of collecting the route to be deleted and
       sending the delete message to RP's */
    MEMSET (&InAddr, 0, sizeof (tIp6Addr));
    MEMSET (&OutAddr, 0, sizeof (tIp6Addr));
    u1InPrefixLen = 0;
    u1OutPrefixLen = 0;

    Ip6ScanRouteTableForBestRouteInCxt (pNewFilter->pRtm6Cxt, &InAddr,
                                        u1InPrefixLen,
                                        Rtm6HandleFilteredRouteRedistribution,
                                        0, &OutAddr, &u1OutPrefixLen,
                                        (VOID *) pNewFilter);
    return;
}

/*-------------------------------------------------------------------+
 *
 * Function           : Rtm6FilterRedistributedRoutes
 *
 * Input(s)           : pNewFilter- Control table Entry used to delete 
 *                      the routes falls in this range.
 *
 * Output(s)          : None.
 *
 * Returns            : None. 
 *
 * Action             : This function withdraws the redistributed routes
 *                      falls in the range from the respective routing
 *                      protocols.
+-------------------------------------------------------------------*/
VOID
Rtm6FilterRedistributedRoutes (tRrd6ControlInfo * pNewFilter)
{
    tIp6Addr            InAddr;
    tIp6Addr            OutAddr;
    UINT1               u1InPrefixLen = 0;
    UINT1               u1OutPrefixLen = 0;

    /* This function will take care of collecting the route to be deleted and
       sending the delete message to RP's */
    MEMSET (&InAddr, 0, sizeof (tIp6Addr));
    MEMSET (&OutAddr, 0, sizeof (tIp6Addr));
    u1InPrefixLen = 0;
    u1OutPrefixLen = 0;

    Ip6ScanRouteTableForBestRouteInCxt (pNewFilter->pRtm6Cxt, &InAddr,
                                        u1InPrefixLen,
                                        Rtm6HandleFilteredRouteRedistribution,
                                        0, &OutAddr, &u1OutPrefixLen,
                                        (VOID *) pNewFilter);
    return;
}

/*****************************************************************************
* DESCRIPTION : Bitwise Masks the given Prefix with the mask derived out of
*               the given prefix length.         
* INPUTS      : pPrefix     -  Pointer to address from which
*                              bits are copied
*               u4PrefLen  -  Prefix Length from which mask is derived.
* OUTPUTS     : pOutPrefix - Pointer to the masked address information
*
* RETURNS     : None
****************************************************************************/
VOID
Rtm6AddrMask (tIp6Addr * pOutPrefix, tIp6Addr * pPrefix, UINT4 u4PrefLen)
{
    UINT4               u4B1 = 0;
    UINT4               u4Mask = 0;
    UINT4               u4SetBit = 0;
    UINT4               u4Bit = 0;
    UINT4               u4Dw = 0;

    MEMCPY (pOutPrefix, pPrefix, IP6_ADDR_SIZE);

    for (u4SetBit = 1; u4SetBit <= u4PrefLen; u4SetBit++)
    {
        u4Dw = u4B1 = u4Mask = 0;
        u4Bit = IP6_ADDR_MAX_PREFIX - u4SetBit;

        u4Dw = u4Bit >> 0x05;    /* Gives the byte position for the given bit */

        u4B1 = pPrefix->u4_addr[u4Dw];

        u4Bit = ~u4Bit;
        u4Bit &= 0x1f;
        u4Mask = OSIX_HTONL (1 << u4Bit);

        pOutPrefix->u4_addr[u4Dw] |= (u4B1 | u4Mask);
    }
}

/*-------------------------------------------------------------------+
 *
 * Function           : Rtm6FormLevelOrMatchMask 
 *
 * Input(s)           : u2RedisProtoMask -- Protocol to be Redistributed.
 *
 * Output(s)          : None
 *                     
 *
 * Returns            : RTM_SUCCESS/RTM_FAILURE 
 *
 * Action             : This function handles redistribution of best ISIS
 *                      routes for the corresponding Match type in ISIS.
 *                      The Match type can be ISIS Level-1 or Level-2 Routes 
+-------------------------------------------------------------------*/
INT4
Rtm6CheckForLevel (UINT2 u2RedisProtoMask, UINT1 i1MetricType)
{
    if ((u2RedisProtoMask & RTM6_ISISL1L2_MASK) == 0)
    {
        return RTM6_SUCCESS;
    }
    else
    {
        switch (i1MetricType)
        {
            case IP_ISIS_LEVEL1:
                if ((u2RedisProtoMask & RTM6_ISISL1_MASK) == RTM6_ISISL1_MASK)
                {
                    return RTM6_SUCCESS;
                }
                break;

            case IP_ISIS_LEVEL2:
                if ((u2RedisProtoMask & RTM6_ISISL2_MASK) == RTM6_ISISL2_MASK)
                {
                    return RTM6_SUCCESS;
                }
                break;

            default:
                if (i1MetricType == 0)
                {
                    /* For NON OSPF types i1MetricType will be zero
                     * So Match need not be done */
                    return RTM6_SUCCESS;
                }
                break;
        }
    }
    return RTM6_FAILURE;
}

/*-------------------------------------------------------------------+
 *
 * Function           : Rtm6CheckForMatch
 *
 * Input(s)           : u2RedisProtoMask -- Protocol to be Redistributed.
 *
 * Output(s)          : None
 *
 *
 * Returns            : RTM6_SUCCESS/RTM6_FAILURE
 *
 * Action             : This function handles redistribution of best OSPF
 *                      routes for the corresponding Match type in OSPF.
 *                      The Match type can be internal ospf routes or
 *                      external ospf routes or in the combination
 *                      of this too.
 *
+-------------------------------------------------------------------*/
INT4
Rtm6CheckForMatch (UINT2 u2RedisProtoMask, UINT1 i1MetricType)
{
    if ((u2RedisProtoMask & RTM6_MATCH_ALL_TYPE) == 0)
    {
        return RTM6_SUCCESS;
    }
    else
    {
        switch (i1MetricType)
        {
            case IP_OSPF_INTRA_AREA:
            case IP_OSPF_INTER_AREA:

                if ((u2RedisProtoMask & RTM6_MATCH_INT_TYPE) ==
                    RTM6_MATCH_INT_TYPE)
                {
                    return RTM6_SUCCESS;
                }
                break;

            case IP_OSPF_TYPE_1_EXT:
            case IP_OSPF_TYPE_2_EXT:

                if ((u2RedisProtoMask & RTM6_MATCH_EXT_TYPE) ==
                    RTM6_MATCH_EXT_TYPE)
                {
                    return RTM6_SUCCESS;
                }
                break;

            case IP_OSPF_NSSA_1_EXT:
            case IP_OSPF_NSSA_2_EXT:

                if ((u2RedisProtoMask & RTM6_MATCH_NSSA_TYPE) ==
                    RTM6_MATCH_NSSA_TYPE)
                {
                    return RTM6_SUCCESS;
                }
                break;

            default:
                if (i1MetricType == 0)
                {
                    /* For NON OSPF types i1MetricType will be zero
                     * So Match need not be done */
                    return RTM6_SUCCESS;
                }
                break;
        }
    }
    return RTM6_FAILURE;
}
