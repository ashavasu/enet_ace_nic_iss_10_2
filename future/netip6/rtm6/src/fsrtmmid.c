# include  "include.h"
# include  "fsrtmmid.h"
# include  "fsrtmlow.h"
# include  "fsrtmcon.h"
# include  "fsrtmogi.h"
# include  "extern.h"
# include  "midconst.h"

/****************************************************************************
 Function   : fsrrd6ScalarGet
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db       : The OID as formed by the SNMP Agent
                              from the static MIB database.
              p_incoming    : The OID as it is send by the manager
                              in the SNMP PDU.
              u1_arg        : The position of the variable in the MIB group.
              u1_search_type: Indicates whether this routine is called
                              as a GET or GETNEXT.
 Output     : The actual OID for which the operation is performed is
              returned in p_in_db.
 Returns    : Returns a pointer to tSNMP_VAR_BIND structure, with either
              the corresponding value, or NULL if the GET/GETNEXT
              operation could not be performed successfully.
****************************************************************************/

/* Prototype declarations for get_routine. */
tSNMP_VAR_BIND     *
fsrrd6ScalarGet (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                 UINT1 u1_arg, UINT1 u1_search_type)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for Scalar get routine. */

    UINT1               i1_ret_val = FALSE;
    INT4                LEN_fsrrd6Scalar_INDEX;

    /*  The Declaration of the Vars which are passed to FormVarbind Fn. */
    INT4                i4_return_val = FALSE;
    UINT4               u4_counter_val = FALSE;
    tSNMP_COUNTER64_TYPE u8_counter_val = { 0, 0 };
    INT2                i2_type;

    /*
     *  The Declaration of the Octet String Array which is used for
     *  conversion and The Pointer of tSNMP_OCTET_STRING_TYPE.
     */
    UINT1               u1_octet_string[MAX_OID_LENGTH] = NULL_STRING;
    tSNMP_OCTET_STRING_TYPE *poctet_string = NULL;
    tSNMP_OID_TYPE     *pOidValue = NULL;

    /*
     *  This Variable is declared for being used in the
     *  FOR Loop for extracting Indices from OID Given.
     */

    UINT4               u4_addr_ret_val_fsRrd6RouterId;

/*** DECLARATION_END ***/

    LEN_fsrrd6Scalar_INDEX = p_in_db->u4_Length;

    /*  Incrementing the Length for the Extract of Scalar Tables. */
    LEN_fsrrd6Scalar_INDEX++;
    if (u1_search_type == SNMP_SEARCH_TYPE_EXACT)
    {
        if ((LEN_fsrrd6Scalar_INDEX != (INT4) p_incoming->u4_Length)
            || (p_incoming->pu4_OidList[p_incoming->u4_Length - 1] != 0))
        {
            return ((tSNMP_VAR_BIND *) NULL);
        }
    }
    else
    {
        /*  Get Next Operation on the Scalar Variable.  */
        if ((INT4) p_incoming->u4_Length >= LEN_fsrrd6Scalar_INDEX)
        {
            return ((tSNMP_VAR_BIND *) NULL);
        }
    }
    switch (u1_arg)
    {
        case FSRRD6ROUTERID:
        {
            i1_ret_val = nmhGetFsRrd6RouterId (&u4_addr_ret_val_fsRrd6RouterId);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_IP_ADDR_PRIM;
                /* This part of the Code converts the ADDR to Octet String. */
                *((UINT4 *) (u1_octet_string)) =
                    OSIX_HTONL (u4_addr_ret_val_fsRrd6RouterId);
                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSRRD6FILTERBYOSPFTAG:
        {
            i1_ret_val = nmhGetFsRrd6FilterByOspfTag (&i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSRRD6FILTEROSPFTAG:
        {
            i1_ret_val = nmhGetFsRrd6FilterOspfTag (&i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSRRD6FILTEROSPFTAGMASK:
        {
            i1_ret_val = nmhGetFsRrd6FilterOspfTagMask (&i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSRRD6ROUTERASNUMBER:
        {
            i1_ret_val = nmhGetFsRrd6RouterASNumber (&i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSRRD6ADMINSTATUS:
        {
            i1_ret_val = nmhGetFsRrd6AdminStatus (&i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSRRD6TRACE:
        {
            i1_ret_val = nmhGetFsRrd6Trace (&u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_UNSIGNED32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSRRD6THROTLIMIT:
        {
            i1_ret_val = nmhGetFsRrd6ThrotLimit (&u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_UNSIGNED32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);
    }                            /* End Of SWITCH Case for All Objects. */

    /* Incrementing the Length of the p_in_db. */
    p_in_db->u4_Length++;
    /* Adding the .0 to the p_in_db for scalar Objects. */
    p_in_db->pu4_OidList[p_in_db->u4_Length - 1] = ZERO;

    return (SNMP_AGT_FormVarBind
            (p_in_db, i2_type, u4_counter_val, i4_return_val, poctet_string,
             pOidValue, u8_counter_val));

}                                /*   THE GET FUNCTION GETS OVER . */

/****************************************************************************
 Function   : fsrrd6ScalarSet
 Description: This routine returns the value of the requestedMIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Output     : The actual OID for which the operation is
              performed is returned in p_in_db.
 Returns    : Returns one of the following error codes
           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for set_routine. */
INT4
fsrrd6ScalarSet (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                 UINT1 u1_arg, tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for set routine */

    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_size_offset = FALSE;
    /*
     *  This Variable is used to extract the Value in The
     *  MULTI DATA TYPE which is sent by the Manager.
     */
    UINT1               u1_octet_string[MAX_OID_LENGTH] = NULL_STRING;

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */
    UINT4               u4_addr_val_fsRrd6RouterId;

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    if (p_incoming->u4_Length <= p_in_db->u4_Length)
    {
        return (SNMP_ERR_WRONG_LENGTH);
    }
    else
    {
        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;
    }                            /* End Of Else Condition. */
    switch (u1_arg)
    {
        case FSRRD6ROUTERID:
        {
            for (i4_offset = FALSE; i4_offset < ADDR_LEN; i4_offset++)
                u1_octet_string[i4_offset] =
                    p_value->pOctetStrValue->pu1_OctetList[i4_offset];
            u4_addr_val_fsRrd6RouterId =
                OSIX_NTOHL (*((UINT4 *) (u1_octet_string)));
            i1_ret_val = nmhSetFsRrd6RouterId (u4_addr_val_fsRrd6RouterId);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSRRD6FILTERBYOSPFTAG:
        {
            i1_ret_val = nmhSetFsRrd6FilterByOspfTag (p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSRRD6FILTEROSPFTAG:
        {
            i1_ret_val = nmhSetFsRrd6FilterOspfTag (p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSRRD6FILTEROSPFTAGMASK:
        {
            i1_ret_val = nmhSetFsRrd6FilterOspfTagMask (p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSRRD6ROUTERASNUMBER:
        {
            i1_ret_val = nmhSetFsRrd6RouterASNumber (p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSRRD6ADMINSTATUS:
        {
            i1_ret_val = nmhSetFsRrd6AdminStatus (p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSRRD6TRACE:
        {
            i1_ret_val = nmhSetFsRrd6Trace (p_value->u4_ULongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSRRD6THROTLIMIT:
        {
            i1_ret_val = nmhSetFsRrd6ThrotLimit (p_value->u4_ULongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        default:
            return (SNMP_ERR_INCONSISTENT_NAME);
    }                            /* End of Switch */
}                                /*   THE SET ROUTINES GET OVER. */

/****************************************************************************
 Function   : fsrrd6ScalarTest
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Returns    : Returns one of the following error codes

           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_INCONSISTNT_VALUE
           SNMP_ERR_WRONG_VALUE
           SNMP_ERR_WRONG_LENGTH
           SNMP_ERR_NOT_WRITABLE
           SNMP_ERR_NO_CREATION
           SNMP_ERR_WRONG_TYPE
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for test_routine */

INT4
fsrrd6ScalarTest (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                  UINT1 u1_arg, tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for Test routine . */

    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_size_offset = FALSE;
    UINT4               u4ErrorCode = 0;
    /*
     *  This Variable is used to extract the Value in The
     *  Multi Data Type which is sent by the Manager
     */
    UINT1               u1_octet_string[MAX_OID_LENGTH] = NULL_STRING;

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */
    UINT4               u4_addr_val_fsRrd6RouterId;

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    if (p_incoming->u4_Length <= p_in_db->u4_Length)
    {
        return (SNMP_ERR_WRONG_LENGTH);
    }
    else
    {
        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;
    }
    if (p_incoming->pu4_OidList[p_incoming->u4_Length - 1] != 0)
    {
        return (SNMP_ERR_GEN_ERR);
    }
    switch (u1_arg)
    {

        case FSRRD6ROUTERID:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_IP_ADDR_PRIM)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }
            for (i4_offset = FALSE; i4_offset < ADDR_LEN; i4_offset++)
                u1_octet_string[i4_offset] =
                    p_value->pOctetStrValue->pu1_OctetList[i4_offset];
            u4_addr_val_fsRrd6RouterId =
                OSIX_NTOHL (*((UINT4 *) (u1_octet_string)));

            i1_ret_val =
                nmhTestv2FsRrd6RouterId (&u4ErrorCode,
                                         u4_addr_val_fsRrd6RouterId);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSRRD6FILTERBYOSPFTAG:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2FsRrd6FilterByOspfTag (&u4ErrorCode,
                                                p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSRRD6FILTEROSPFTAG:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2FsRrd6FilterOspfTag (&u4ErrorCode,
                                              p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSRRD6FILTEROSPFTAGMASK:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2FsRrd6FilterOspfTagMask (&u4ErrorCode,
                                                  p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSRRD6ROUTERASNUMBER:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2FsRrd6RouterASNumber (&u4ErrorCode,
                                               p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSRRD6ADMINSTATUS:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2FsRrd6AdminStatus (&u4ErrorCode,
                                            p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSRRD6TRACE:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_UNSIGNED32)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2FsRrd6Trace (&u4ErrorCode, p_value->u4_ULongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSRRD6THROTLIMIT:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_UNSIGNED32)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2FsRrd6ThrotLimit (&u4ErrorCode,
                                           p_value->u4_ULongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }
            /*  Read Only Variables */

            return (SNMP_ERR_NOT_WRITABLE);

        default:
            return (SNMP_ERR_INCONSISTENT_NAME);
    }                            /* End of Switch */
}                                /*  THE TEST ROUTINES ARE OVER . */

/****************************************************************************
 Function   : fsRrd6ControlEntryGet
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db       : The OID as formed by the SNMP Agent
                              from the static MIB database.
              p_incoming    : The OID as it is send by the manager
                              in the SNMP PDU.
              u1_arg        : The position of the variable in the MIB group.
              u1_search_type: Indicates whether this routine is called
                              as a GET or GETNEXT.
 Output     : The actual OID for which the operation is performed is
              returned in p_in_db.
 Returns    : Returns a pointer to tSNMP_VAR_BIND structure, with either
              the corresponding value, or NULL if the GET/GETNEXT
              operation could not be performed successfully.
****************************************************************************/

/* Prototype declarations for get_routine. */
tSNMP_VAR_BIND     *
fsRrd6ControlEntryGet (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                       UINT1 u1_arg, UINT1 u1_search_type)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for get routine . */
    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_partial_index_len = FALSE;
    INT4                i4_partial_index_flag = TRUE;
    INT4                i4_size_offset = FALSE;
    INT4                LEN_fsRrd6ControlTable_INDEX;

    /*  The Declaration of the Vars which are passed to FormVarbind Fn. */
    INT4                i4_return_val = FALSE;
    UINT4               u4_counter_val = FALSE;
    tSNMP_COUNTER64_TYPE u8_counter_val = { 0, 0 };
    INT2                i2_type;

    /*
     *  The Declaration of the Octet String Array which is used for
     *  conversion and The Pointer of tSNMP_OCTET_STRING_TYPE.
     */
    tSNMP_OCTET_STRING_TYPE *poctet_string = NULL;
    tSNMP_OID_TYPE     *pOidValue = NULL;

    /*
     *  These Variable are declared for being used in the
     *  FOR Loop for extracting Indices from the given OID.
     */
    INT4                i4_count = FALSE;
    /*
     *  The tSNMP_OCTET_STRING_TYPE Which Store
     *  the Length and the Octet String.
     */
    INT4                i4_len_Octet_index_fsRrd6ControlDestIpAddress = FALSE;
    tSNMP_OCTET_STRING_TYPE *poctet_fsRrd6ControlDestIpAddress = NULL;
    tSNMP_OCTET_STRING_TYPE *poctet_next_fsRrd6ControlDestIpAddress = NULL;

    INT4                i4_fsRrd6ControlNetMaskLen = FALSE;
    INT4                i4_next_fsRrd6ControlNetMaskLen = FALSE;

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    switch (u1_search_type)
    {

        case SNMP_SEARCH_TYPE_EXACT:
        {
            /*
             *  Initializing the i4_size_offset Variable to the Length of the
             *  OID of the Object i.e the Length present in the p_in_db.
             */
            i4_size_offset = p_in_db->u4_Length;
            i4_len_Octet_index_fsRrd6ControlDestIpAddress =
                p_incoming->pu4_OidList[i4_size_offset++];
            i4_size_offset += i4_len_Octet_index_fsRrd6ControlDestIpAddress;
            i4_size_offset += INTEGER_LEN;

            if (p_incoming->u4_Length != (UINT4) i4_size_offset)
            {
                return (NULL);
            }
            /*
             *  Finding the length of the Index for extracting
             *  Index from the structure given by the Manager.
             */
            /*
             *  If LEN_OF_VARIABLE_LEN_INDEX is Present refer Header File
             *  where it is Defined for Explanation of the Variable.
             */
            LEN_fsRrd6ControlTable_INDEX =
                p_in_db->u4_Length +
                i4_len_Octet_index_fsRrd6ControlDestIpAddress +
                LEN_OF_VARIABLE_LEN_INDEX + INTEGER_LEN;

            if (LEN_fsRrd6ControlTable_INDEX == (INT4) p_incoming->u4_Length)
            {
                /*  Allocating Memory for the Get Exact Octet String Index. */
                poctet_fsRrd6ControlDestIpAddress =
                    (tSNMP_OCTET_STRING_TYPE *)
                    allocmem_octetstring
                    (i4_len_Octet_index_fsRrd6ControlDestIpAddress);
                if ((poctet_fsRrd6ControlDestIpAddress == NULL))
                {
                    free_octetstring (poctet_fsRrd6ControlDestIpAddress);
                    return ((tSNMP_VAR_BIND *) NULL);
                }
                /*
                 *  This is to Increment the Array Pointer by one Which
                 *  Contains the Length of the Index.
                 */
                i4_offset++;
                /*  The FOR LOOP for extracting Octet_str Index from the Given OID. */
                for (i4_count = FALSE;
                     i4_count < i4_len_Octet_index_fsRrd6ControlDestIpAddress;
                     i4_count++, i4_offset++)
                    poctet_fsRrd6ControlDestIpAddress->pu1_OctetList[i4_count] =
                        (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                        i4_offset];

                /* Extracting The Integer Index. */
                i4_fsRrd6ControlNetMaskLen =
                    (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                   i4_offset];
                i4_offset++;

                /*  Low Level Routine Which Validates the Indices. */
                if ((i1_ret_val =
                     nmhValidateIndexInstanceFsRrd6ControlTable
                     (poctet_fsRrd6ControlDestIpAddress,
                      i4_fsRrd6ControlNetMaskLen)) != SNMP_SUCCESS)
                {
                    free_octetstring (poctet_fsRrd6ControlDestIpAddress);
                    return ((tSNMP_VAR_BIND *) NULL);
                }
                /*  Storing the Extracted Index in p_in_db. */
                /*  Storing the Length of the Octet String Get Exact.  */
                p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                    poctet_fsRrd6ControlDestIpAddress->i4_Length;
                /*  FOR Loop for storing the value from get first to p_in_db. */
                for (i4_count = FALSE;
                     i4_count < poctet_fsRrd6ControlDestIpAddress->i4_Length;
                     i4_count++)
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) poctet_fsRrd6ControlDestIpAddress->
                        pu1_OctetList[i4_count];

                p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                    (UINT4) i4_fsRrd6ControlNetMaskLen;
                i4_partial_index_flag = FALSE;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case SNMP_SEARCH_TYPE_NEXT:
        {
            /*  The Manager Has Not Given the Indices. */
            if (p_incoming->u4_Length <= p_in_db->u4_Length)
            {
                /*  Allocating Memory for the Get First Octet String Index. */
                poctet_fsRrd6ControlDestIpAddress =
                    (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (17);
                if ((poctet_fsRrd6ControlDestIpAddress == NULL))
                {
                    free_octetstring (poctet_fsRrd6ControlDestIpAddress);
                    return ((tSNMP_VAR_BIND *) NULL);
                }
                /*  Flag is Set to Indicate Low Level Get First Operation. */
                i4_partial_index_flag = FALSE;
                if ((i1_ret_val
                     =
                     nmhGetFirstIndexFsRrd6ControlTable
                     (poctet_fsRrd6ControlDestIpAddress,
                      &i4_fsRrd6ControlNetMaskLen)) == SNMP_SUCCESS)
                {
                    /*  Storing the Length. */
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        poctet_fsRrd6ControlDestIpAddress->i4_Length;
                    /*  FOR Loop for storing the value from get first to p_in_db. */
                    for (i4_count = FALSE;
                         i4_count <
                         poctet_fsRrd6ControlDestIpAddress->i4_Length;
                         i4_count++)
                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            (UINT4) poctet_fsRrd6ControlDestIpAddress->
                            pu1_OctetList[i4_count];

                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_fsRrd6ControlNetMaskLen;
                }
                else
                {
                    free_octetstring (poctet_fsRrd6ControlDestIpAddress);
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else if (p_incoming->u4_Length > p_in_db->u4_Length)
            {
                /*  Flag is Set to Indicate Get First Operation. */
                i4_partial_index_flag = TRUE;

                /*  Initializing the Partial Index Length. */
                i4_partial_index_len = p_in_db->u4_Length;

                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)
                {
                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_len_Octet_index_fsRrd6ControlDestIpAddress =
                        p_incoming->pu4_OidList[i4_partial_index_len++];
                    i4_partial_index_len +=
                        i4_len_Octet_index_fsRrd6ControlDestIpAddress;
                    /*  Allocating Memory for The Octet Str Get Next Index. */
                    poctet_fsRrd6ControlDestIpAddress =
                        (tSNMP_OCTET_STRING_TYPE *)
                        allocmem_octetstring
                        (i4_len_Octet_index_fsRrd6ControlDestIpAddress);
                    poctet_next_fsRrd6ControlDestIpAddress =
                        (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (17);

                    /*  Checking for the malloc failure.  */
                    if ((poctet_fsRrd6ControlDestIpAddress == NULL)
                        || (poctet_next_fsRrd6ControlDestIpAddress == NULL))
                    {
                        /*  Freeing the Current Index. */
                        free_octetstring (poctet_fsRrd6ControlDestIpAddress);
                        free_octetstring
                            (poctet_next_fsRrd6ControlDestIpAddress);
                        return ((tSNMP_VAR_BIND *) NULL);
                    }

                    /*
                     *  This is to Increment the Array Pointer by one Which
                     *  Contains the Length of the Index.
                     */
                    i4_offset++;
                    /*  The FOR LOOP for extracting Octet_str Index from the Given OID. */
                    for (i4_count = FALSE;
                         i4_count <
                         i4_len_Octet_index_fsRrd6ControlDestIpAddress;
                         i4_count++, i4_offset++)
                        poctet_fsRrd6ControlDestIpAddress->
                            pu1_OctetList[i4_count] =
                            (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                            i4_offset];

                }
                else
                {
                    /*
                     *  Memory Allocation of the (Partial) Index of type OID
                     *  and Octet string which is not given by the Manager.
                     */
                    poctet_fsRrd6ControlDestIpAddress =
                        allocmem_octetstring (17);
                    poctet_next_fsRrd6ControlDestIpAddress =
                        allocmem_octetstring (17);
                    if ((poctet_fsRrd6ControlDestIpAddress == NULL)
                        || (poctet_next_fsRrd6ControlDestIpAddress == NULL))
                    {
                        free_octetstring (poctet_fsRrd6ControlDestIpAddress);
                        free_octetstring
                            (poctet_next_fsRrd6ControlDestIpAddress);
                        return ((tSNMP_VAR_BIND *) NULL);
                    }
                }
                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)
                {
                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += INTEGER_LEN;
                    /* Extracting The Integer Index. */
                    i4_fsRrd6ControlNetMaskLen =
                        (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                       i4_offset];
                    i4_offset++;

                }

                /*
                 *  Get the value of the variable with the new index
                 *  By Calling the Low Level GET_NEXT routine.
                 */
                if ((i1_ret_val
                     =
                     nmhGetNextIndexFsRrd6ControlTable
                     (poctet_fsRrd6ControlDestIpAddress,
                      poctet_next_fsRrd6ControlDestIpAddress,
                      i4_fsRrd6ControlNetMaskLen,
                      &i4_next_fsRrd6ControlNetMaskLen)) == SNMP_SUCCESS)
                {
                    /*  Storing the Value of the Len of Octet Str in p_in_db. */
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        poctet_next_fsRrd6ControlDestIpAddress->i4_Length;
                    for (i4_count = FALSE;
                         i4_count <
                         poctet_next_fsRrd6ControlDestIpAddress->i4_Length;
                         i4_count++)
                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            (UINT4) poctet_next_fsRrd6ControlDestIpAddress->
                            pu1_OctetList[i4_count];
                    free_octetstring (poctet_fsRrd6ControlDestIpAddress);
                    poctet_fsRrd6ControlDestIpAddress =
                        poctet_next_fsRrd6ControlDestIpAddress;
                    i4_fsRrd6ControlNetMaskLen =
                        i4_next_fsRrd6ControlNetMaskLen;
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_next_fsRrd6ControlNetMaskLen;
                }
                else
                {
                    free_octetstring (poctet_fsRrd6ControlDestIpAddress);
                    free_octetstring (poctet_next_fsRrd6ControlDestIpAddress);
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);

    }                            /* End of SWITCH statement. */

    /*
     *  The SWITCH CASE statement for calling the Low Level fn
     *  for all objects Low Level Routines for all Objects.
     */
    switch (u1_arg)
    {
        case FSRRD6CONTROLDESTIPADDRESS:
        {
            i2_type = SNMP_DATA_TYPE_OCTET_PRIM;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == SNMP_SEARCH_TYPE_EXACT)
                || (i4_partial_index_flag == FALSE))
            {
                poctet_string = poctet_fsRrd6ControlDestIpAddress;
            }
            else
            {
                poctet_string = poctet_next_fsRrd6ControlDestIpAddress;
            }
            break;
        }
        case FSRRD6CONTROLNETMASKLEN:
        {
            i2_type = SNMP_DATA_TYPE_INTEGER;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == SNMP_SEARCH_TYPE_EXACT)
                || (i4_partial_index_flag == FALSE))
            {
                i4_return_val = i4_fsRrd6ControlNetMaskLen;
            }
            else
            {
                i4_return_val = i4_next_fsRrd6ControlNetMaskLen;
            }
            free_octetstring (poctet_fsRrd6ControlDestIpAddress);
            break;
        }
        case FSRRD6CONTROLSOURCEPROTO:
        {
            i1_ret_val =
                nmhGetFsRrd6ControlSourceProto
                (poctet_fsRrd6ControlDestIpAddress, i4_fsRrd6ControlNetMaskLen,
                 &i4_return_val);
            free_octetstring (poctet_fsRrd6ControlDestIpAddress);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSRRD6CONTROLDESTPROTO:
        {
            i1_ret_val =
                nmhGetFsRrd6ControlDestProto (poctet_fsRrd6ControlDestIpAddress,
                                              i4_fsRrd6ControlNetMaskLen,
                                              &i4_return_val);
            free_octetstring (poctet_fsRrd6ControlDestIpAddress);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSRRD6CONTROLROUTEEXPORTFLAG:
        {
            i1_ret_val =
                nmhGetFsRrd6ControlRouteExportFlag
                (poctet_fsRrd6ControlDestIpAddress, i4_fsRrd6ControlNetMaskLen,
                 &i4_return_val);
            free_octetstring (poctet_fsRrd6ControlDestIpAddress);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSRRD6CONTROLROWSTATUS:
        {
            i1_ret_val =
                nmhGetFsRrd6ControlRowStatus (poctet_fsRrd6ControlDestIpAddress,
                                              i4_fsRrd6ControlNetMaskLen,
                                              &i4_return_val);
            free_octetstring (poctet_fsRrd6ControlDestIpAddress);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            free_octetstring (poctet_fsRrd6ControlDestIpAddress);
            return ((tSNMP_VAR_BIND *) NULL);
    }                            /* End Of SWITCH Case for All Objects. */

    return (SNMP_AGT_FormVarBind
            (p_in_db, i2_type, u4_counter_val, i4_return_val, poctet_string,
             pOidValue, u8_counter_val));

}                                /*   THE GET FUNCTION GETS OVER . */

/****************************************************************************
 Function   : fsRrd6ControlEntrySet
 Description: This routine returns the value of the requestedMIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Output     : The actual OID for which the operation is
              performed is returned in p_in_db.
 Returns    : Returns one of the following error codes
           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for set_routine. */
INT4
fsRrd6ControlEntrySet (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                       UINT1 u1_arg, tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for set routine */

    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_size_offset = FALSE;
    /*
     *  This Variable is used to extract the Value in The
     *  MULTI DATA TYPE which is sent by the Manager.
     */

    INT4                i4_count = FALSE;
    /*
     *  The tSNMP_OCTET_STRING_TYPE Which Store
     *  the Length and the Octet String.
     */
    INT4                i4_len_Octet_index_fsRrd6ControlDestIpAddress = FALSE;
    tSNMP_OCTET_STRING_TYPE *poctet_fsRrd6ControlDestIpAddress = NULL;

    INT4                i4_fsRrd6ControlNetMaskLen = FALSE;

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    if (p_incoming->u4_Length <= p_in_db->u4_Length)
    {
        return (SNMP_ERR_WRONG_LENGTH);
    }
    else
    {
        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;
        i4_len_Octet_index_fsRrd6ControlDestIpAddress =
            p_incoming->pu4_OidList[i4_size_offset++];
        i4_size_offset += i4_len_Octet_index_fsRrd6ControlDestIpAddress;
        i4_size_offset += INTEGER_LEN;
        /*  Allocating Memory for the Get Exact Octet String Index. */
        poctet_fsRrd6ControlDestIpAddress =
            (tSNMP_OCTET_STRING_TYPE *)
            allocmem_octetstring
            (i4_len_Octet_index_fsRrd6ControlDestIpAddress);
        if ((poctet_fsRrd6ControlDestIpAddress == NULL))
        {
            free_octetstring (poctet_fsRrd6ControlDestIpAddress);
            return (SNMP_ERR_GEN_ERR);
        }
        /*
         *  This is to Increment the Array Pointer by one Which
         *  Contains the Length of the Index.
         */
        i4_offset++;
        /*  The FOR LOOP for extracting Octet_str Index from the Given OID. */
        for (i4_count = FALSE;
             i4_count < i4_len_Octet_index_fsRrd6ControlDestIpAddress;
             i4_count++, i4_offset++)
            poctet_fsRrd6ControlDestIpAddress->pu1_OctetList[i4_count] =
                (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];

        /* Extracting The Integer Index. */
        i4_fsRrd6ControlNetMaskLen =
            (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];
        i4_offset++;

    }                            /* End Of Else Condition. */
    switch (u1_arg)
    {
        case FSRRD6CONTROLSOURCEPROTO:
        {
            i1_ret_val =
                nmhSetFsRrd6ControlSourceProto
                (poctet_fsRrd6ControlDestIpAddress, i4_fsRrd6ControlNetMaskLen,
                 p_value->i4_SLongValue);

            free_octetstring (poctet_fsRrd6ControlDestIpAddress);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSRRD6CONTROLDESTPROTO:
        {
            i1_ret_val =
                nmhSetFsRrd6ControlDestProto (poctet_fsRrd6ControlDestIpAddress,
                                              i4_fsRrd6ControlNetMaskLen,
                                              p_value->i4_SLongValue);

            free_octetstring (poctet_fsRrd6ControlDestIpAddress);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSRRD6CONTROLROUTEEXPORTFLAG:
        {
            i1_ret_val =
                nmhSetFsRrd6ControlRouteExportFlag
                (poctet_fsRrd6ControlDestIpAddress, i4_fsRrd6ControlNetMaskLen,
                 p_value->i4_SLongValue);

            free_octetstring (poctet_fsRrd6ControlDestIpAddress);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSRRD6CONTROLROWSTATUS:
        {
            i1_ret_val =
                nmhSetFsRrd6ControlRowStatus (poctet_fsRrd6ControlDestIpAddress,
                                              i4_fsRrd6ControlNetMaskLen,
                                              p_value->i4_SLongValue);

            free_octetstring (poctet_fsRrd6ControlDestIpAddress);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
            /*  Read Only Variables. */
        case FSRRD6CONTROLDESTIPADDRESS:
            /*  Read Only Variables. */
        case FSRRD6CONTROLNETMASKLEN:
            free_octetstring (poctet_fsRrd6ControlDestIpAddress);
            return (SNMP_ERR_NOT_WRITABLE);

        default:
            free_octetstring (poctet_fsRrd6ControlDestIpAddress);
            return (SNMP_ERR_INCONSISTENT_NAME);
    }                            /* End of Switch */
}                                /*   THE SET ROUTINES GET OVER. */

/****************************************************************************
 Function   : fsRrd6ControlEntryTest
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Returns    : Returns one of the following error codes

           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_INCONSISTNT_VALUE
           SNMP_ERR_WRONG_VALUE
           SNMP_ERR_WRONG_LENGTH
           SNMP_ERR_NOT_WRITABLE
           SNMP_ERR_NO_CREATION
           SNMP_ERR_WRONG_TYPE
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for test_routine */

INT4
fsRrd6ControlEntryTest (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                        UINT1 u1_arg, tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for Test routine . */

    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_size_offset = FALSE;
    UINT4               u4ErrorCode = 0;
    /*
     *  This Variable is used to extract the Value in The
     *  Multi Data Type which is sent by the Manager
     */

    INT4                i4_count = FALSE;
    /*
     *  The tSNMP_OCTET_STRING_TYPE Which Store
     *  the Length and the Octet String.
     */
    INT4                i4_len_Octet_index_fsRrd6ControlDestIpAddress = FALSE;
    tSNMP_OCTET_STRING_TYPE *poctet_fsRrd6ControlDestIpAddress = NULL;

    INT4                i4_fsRrd6ControlNetMaskLen = FALSE;

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    if (p_incoming->u4_Length <= p_in_db->u4_Length)
    {
        return (SNMP_ERR_WRONG_LENGTH);
    }
    else
    {
        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;
        i4_len_Octet_index_fsRrd6ControlDestIpAddress =
            p_incoming->pu4_OidList[i4_size_offset++];
        i4_size_offset += i4_len_Octet_index_fsRrd6ControlDestIpAddress;
        i4_size_offset += INTEGER_LEN;

        if (p_incoming->u4_Length != (UINT4) i4_size_offset)
        {
            return (SNMP_ERR_WRONG_LENGTH);
        }
        /*  Allocating Memory for the Get Exact Octet String Index. */
        poctet_fsRrd6ControlDestIpAddress =
            (tSNMP_OCTET_STRING_TYPE *)
            allocmem_octetstring
            (i4_len_Octet_index_fsRrd6ControlDestIpAddress);
        if ((poctet_fsRrd6ControlDestIpAddress == NULL))
        {
            free_octetstring (poctet_fsRrd6ControlDestIpAddress);
            return (SNMP_ERR_GEN_ERR);
        }
        /*
         *  This is to Increment the Array Pointer by one Which
         *  Contains the Length of the Index.
         */
        i4_offset++;
        /*  The FOR LOOP for extracting Octet_str Index from the Given OID. */
        for (i4_count = FALSE;
             i4_count < i4_len_Octet_index_fsRrd6ControlDestIpAddress;
             i4_count++, i4_offset++)
            poctet_fsRrd6ControlDestIpAddress->pu1_OctetList[i4_count] =
                (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];

        /* Extracting The Integer Index. */
        i4_fsRrd6ControlNetMaskLen =
            (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];
        i4_offset++;

    }
    /*  Low Level Routine Which Validates the Indices. */
    /* COMMENT FOLLOWING CHECK TO ENABLE ROW CREATION 

       if((i1_ret_val = nmhValidateIndexInstanceFsRrd6ControlTable(poctet_fsRrd6ControlDestIpAddress , i4_fsRrd6ControlNetMaskLen)) != SNMP_SUCCESS) {
       free_octetstring(poctet_fsRrd6ControlDestIpAddress);
       return SNMP_ERR_INCONSISTENT_NAME;
       } */
    switch (u1_arg)
    {

        case FSRRD6CONTROLSOURCEPROTO:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2FsRrd6ControlSourceProto (&u4ErrorCode,
                                                   poctet_fsRrd6ControlDestIpAddress,
                                                   i4_fsRrd6ControlNetMaskLen,
                                                   p_value->i4_SLongValue);

            free_octetstring (poctet_fsRrd6ControlDestIpAddress);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSRRD6CONTROLDESTPROTO:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2FsRrd6ControlDestProto (&u4ErrorCode,
                                                 poctet_fsRrd6ControlDestIpAddress,
                                                 i4_fsRrd6ControlNetMaskLen,
                                                 p_value->i4_SLongValue);

            free_octetstring (poctet_fsRrd6ControlDestIpAddress);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSRRD6CONTROLROUTEEXPORTFLAG:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2FsRrd6ControlRouteExportFlag (&u4ErrorCode,
                                                       poctet_fsRrd6ControlDestIpAddress,
                                                       i4_fsRrd6ControlNetMaskLen,
                                                       p_value->i4_SLongValue);

            free_octetstring (poctet_fsRrd6ControlDestIpAddress);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSRRD6CONTROLROWSTATUS:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2FsRrd6ControlRowStatus (&u4ErrorCode,
                                                 poctet_fsRrd6ControlDestIpAddress,
                                                 i4_fsRrd6ControlNetMaskLen,
                                                 p_value->i4_SLongValue);

            free_octetstring (poctet_fsRrd6ControlDestIpAddress);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }
            /*  Read Only Variables */

        case FSRRD6CONTROLDESTIPADDRESS:
        case FSRRD6CONTROLNETMASKLEN:
            free_octetstring (poctet_fsRrd6ControlDestIpAddress);
            return (SNMP_ERR_NOT_WRITABLE);

        default:
            free_octetstring (poctet_fsRrd6ControlDestIpAddress);
            return (SNMP_ERR_INCONSISTENT_NAME);
    }                            /* End of Switch */
}                                /*  THE TEST ROUTINES ARE OVER . */

/****************************************************************************
 Function   : fsRrd6RoutingProtoEntryGet
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db       : The OID as formed by the SNMP Agent
                              from the static MIB database.
              p_incoming    : The OID as it is send by the manager
                              in the SNMP PDU.
              u1_arg        : The position of the variable in the MIB group.
              u1_search_type: Indicates whether this routine is called
                              as a GET or GETNEXT.
 Output     : The actual OID for which the operation is performed is
              returned in p_in_db.
 Returns    : Returns a pointer to tSNMP_VAR_BIND structure, with either
              the corresponding value, or NULL if the GET/GETNEXT
              operation could not be performed successfully.
****************************************************************************/

/* Prototype declarations for get_routine. */
tSNMP_VAR_BIND     *
fsRrd6RoutingProtoEntryGet (tSNMP_OID_TYPE * p_in_db,
                            tSNMP_OID_TYPE * p_incoming, UINT1 u1_arg,
                            UINT1 u1_search_type)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for get routine . */
    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_partial_index_len = FALSE;
    INT4                i4_partial_index_flag = TRUE;
    INT4                i4_size_offset = FALSE;
    INT4                LEN_fsRrd6RoutingProtoTable_INDEX;

    /*  The Declaration of the Vars which are passed to FormVarbind Fn. */
    INT4                i4_return_val = FALSE;
    UINT4               u4_counter_val = FALSE;
    tSNMP_COUNTER64_TYPE u8_counter_val = { 0, 0 };
    INT2                i2_type;

    /*
     *  The Declaration of the Octet String Array which is used for
     *  conversion and The Pointer of tSNMP_OCTET_STRING_TYPE.
     */
    tSNMP_OCTET_STRING_TYPE *poctet_string = NULL;
    tSNMP_OID_TYPE     *pOidValue = NULL;

    /*
     *  These Variable are declared for being used in the
     *  FOR Loop for extracting Indices from the given OID.
     */
    INT4                i4_fsRrd6RoutingProtoId = FALSE;
    INT4                i4_next_fsRrd6RoutingProtoId = FALSE;

    tSNMP_OCTET_STRING_TYPE *poctet_retval_fsRrd6RoutingProtoTaskIdent = NULL;
    tSNMP_OCTET_STRING_TYPE *poctet_retval_fsRrd6RoutingProtoQueueIdent = NULL;

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    switch (u1_search_type)
    {

        case SNMP_SEARCH_TYPE_EXACT:
        {
            /*
             *  Initializing the i4_size_offset Variable to the Length of the
             *  OID of the Object i.e the Length present in the p_in_db.
             */
            i4_size_offset = p_in_db->u4_Length;
            i4_size_offset += INTEGER_LEN;

            if (p_incoming->u4_Length != (UINT4) i4_size_offset)
            {
                return (NULL);
            }
            /*
             *  Finding the length of the Index for extracting
             *  Index from the structure given by the Manager.
             */
            /*
             *  If LEN_OF_VARIABLE_LEN_INDEX is Present refer Header File
             *  where it is Defined for Explanation of the Variable.
             */
            LEN_fsRrd6RoutingProtoTable_INDEX =
                p_in_db->u4_Length + INTEGER_LEN;

            if (LEN_fsRrd6RoutingProtoTable_INDEX ==
                (INT4) p_incoming->u4_Length)
            {
                /* Extracting The Integer Index. */
                i4_fsRrd6RoutingProtoId =
                    (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                   i4_offset];
                i4_offset++;

                /*  Low Level Routine Which Validates the Indices. */
                if ((i1_ret_val =
                     nmhValidateIndexInstanceFsRrd6RoutingProtoTable
                     (i4_fsRrd6RoutingProtoId)) != SNMP_SUCCESS)
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
                /*  Storing the Extracted Index in p_in_db. */
                p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                    (UINT4) i4_fsRrd6RoutingProtoId;
                i4_partial_index_flag = FALSE;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case SNMP_SEARCH_TYPE_NEXT:
        {
            /*  The Manager Has Not Given the Indices. */
            if (p_incoming->u4_Length <= p_in_db->u4_Length)
            {
                /*  Flag is Set to Indicate Low Level Get First Operation. */
                i4_partial_index_flag = FALSE;
                if ((i1_ret_val
                     =
                     nmhGetFirstIndexFsRrd6RoutingProtoTable
                     (&i4_fsRrd6RoutingProtoId)) == SNMP_SUCCESS)
                {
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_fsRrd6RoutingProtoId;
                }
                else
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else if (p_incoming->u4_Length > p_in_db->u4_Length)
            {
                /*  Flag is Set to Indicate Get First Operation. */
                i4_partial_index_flag = TRUE;

                /*  Initializing the Partial Index Length. */
                i4_partial_index_len = p_in_db->u4_Length;

                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)
                {
                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += INTEGER_LEN;
                    /* Extracting The Integer Index. */
                    i4_fsRrd6RoutingProtoId =
                        (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                       i4_offset];
                    i4_offset++;

                }

                /*
                 *  Get the value of the variable with the new index
                 *  By Calling the Low Level GET_NEXT routine.
                 */
                if ((i1_ret_val
                     =
                     nmhGetNextIndexFsRrd6RoutingProtoTable
                     (i4_fsRrd6RoutingProtoId,
                      &i4_next_fsRrd6RoutingProtoId)) == SNMP_SUCCESS)
                {
                    i4_fsRrd6RoutingProtoId = i4_next_fsRrd6RoutingProtoId;
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_next_fsRrd6RoutingProtoId;
                }
                else
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);

    }                            /* End of SWITCH statement. */

    /*
     *  The SWITCH CASE statement for calling the Low Level fn
     *  for all objects Low Level Routines for all Objects.
     */
    switch (u1_arg)
    {
        case FSRRD6ROUTINGPROTOID:
        {
            i2_type = SNMP_DATA_TYPE_INTEGER;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == SNMP_SEARCH_TYPE_EXACT)
                || (i4_partial_index_flag == FALSE))
            {
                i4_return_val = i4_fsRrd6RoutingProtoId;
            }
            else
            {
                i4_return_val = i4_next_fsRrd6RoutingProtoId;
            }
            break;
        }
        case FSRRD6ROUTINGREGNID:
        {
            i1_ret_val =
                nmhGetFsRrd6RoutingRegnId (i4_fsRrd6RoutingProtoId,
                                           &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSRRD6ROUTINGPROTOTASKIDENT:
        {
            poctet_retval_fsRrd6RoutingProtoTaskIdent =
                (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (256);
            if (poctet_retval_fsRrd6RoutingProtoTaskIdent == NULL)
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            i1_ret_val =
                nmhGetFsRrd6RoutingProtoTaskIdent (i4_fsRrd6RoutingProtoId,
                                                   poctet_retval_fsRrd6RoutingProtoTaskIdent);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_OCTET_PRIM;

                /*  Assigning the Octet Str Ptr Got From Low Level Rtns. */
                poctet_string = poctet_retval_fsRrd6RoutingProtoTaskIdent;
            }
            else
            {
                free_octetstring (poctet_retval_fsRrd6RoutingProtoTaskIdent);
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSRRD6ROUTINGPROTOQUEUEIDENT:
        {
            poctet_retval_fsRrd6RoutingProtoQueueIdent =
                (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (256);
            if (poctet_retval_fsRrd6RoutingProtoQueueIdent == NULL)
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            i1_ret_val =
                nmhGetFsRrd6RoutingProtoQueueIdent (i4_fsRrd6RoutingProtoId,
                                                    poctet_retval_fsRrd6RoutingProtoQueueIdent);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_OCTET_PRIM;

                /*  Assigning the Octet Str Ptr Got From Low Level Rtns. */
                poctet_string = poctet_retval_fsRrd6RoutingProtoQueueIdent;
            }
            else
            {
                free_octetstring (poctet_retval_fsRrd6RoutingProtoQueueIdent);
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSRRD6ALLOWOSPFAREAROUTES:
        {
            i1_ret_val =
                nmhGetFsRrd6AllowOspfAreaRoutes (i4_fsRrd6RoutingProtoId,
                                                 &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSRRD6ALLOWOSPFEXTROUTES:
        {
            i1_ret_val =
                nmhGetFsRrd6AllowOspfExtRoutes (i4_fsRrd6RoutingProtoId,
                                                &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);
    }                            /* End Of SWITCH Case for All Objects. */

    return (SNMP_AGT_FormVarBind
            (p_in_db, i2_type, u4_counter_val, i4_return_val, poctet_string,
             pOidValue, u8_counter_val));

}                                /*   THE GET FUNCTION GETS OVER . */

/****************************************************************************
 Function   : fsRrd6RoutingProtoEntrySet
 Description: This routine returns the value of the requestedMIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Output     : The actual OID for which the operation is
              performed is returned in p_in_db.
 Returns    : Returns one of the following error codes
           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for set_routine. */
INT4
fsRrd6RoutingProtoEntrySet (tSNMP_OID_TYPE * p_in_db,
                            tSNMP_OID_TYPE * p_incoming, UINT1 u1_arg,
                            tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for set routine */

    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_size_offset = FALSE;
    /*
     *  This Variable is used to extract the Value in The
     *  MULTI DATA TYPE which is sent by the Manager.
     */

    INT4                i4_fsRrd6RoutingProtoId = FALSE;

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    if (p_incoming->u4_Length <= p_in_db->u4_Length)
    {
        return (SNMP_ERR_WRONG_LENGTH);
    }
    else
    {
        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;
        i4_size_offset += INTEGER_LEN;
        /* Extracting The Integer Index. */
        i4_fsRrd6RoutingProtoId =
            (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];
        i4_offset++;

    }                            /* End Of Else Condition. */
    switch (u1_arg)
    {
        case FSRRD6ALLOWOSPFAREAROUTES:
        {
            i1_ret_val =
                nmhSetFsRrd6AllowOspfAreaRoutes (i4_fsRrd6RoutingProtoId,
                                                 p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSRRD6ALLOWOSPFEXTROUTES:
        {
            i1_ret_val =
                nmhSetFsRrd6AllowOspfExtRoutes (i4_fsRrd6RoutingProtoId,
                                                p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
            /*  Read Only Variables. */
        case FSRRD6ROUTINGPROTOID:
            /*  Read Only Variables. */
        case FSRRD6ROUTINGREGNID:
            /*  Read Only Variables. */
        case FSRRD6ROUTINGPROTOTASKIDENT:
            /*  Read Only Variables. */
        case FSRRD6ROUTINGPROTOQUEUEIDENT:
            return (SNMP_ERR_NOT_WRITABLE);

        default:
            return (SNMP_ERR_INCONSISTENT_NAME);
    }                            /* End of Switch */
}                                /*   THE SET ROUTINES GET OVER. */

/****************************************************************************
 Function   : fsRrd6RoutingProtoEntryTest
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Returns    : Returns one of the following error codes

           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_INCONSISTNT_VALUE
           SNMP_ERR_WRONG_VALUE
           SNMP_ERR_WRONG_LENGTH
           SNMP_ERR_NOT_WRITABLE
           SNMP_ERR_NO_CREATION
           SNMP_ERR_WRONG_TYPE
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for test_routine */

INT4
fsRrd6RoutingProtoEntryTest (tSNMP_OID_TYPE * p_in_db,
                             tSNMP_OID_TYPE * p_incoming, UINT1 u1_arg,
                             tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for Test routine . */

    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_size_offset = FALSE;
    UINT4               u4ErrorCode = 0;
    /*
     *  This Variable is used to extract the Value in The
     *  Multi Data Type which is sent by the Manager
     */

    INT4                i4_fsRrd6RoutingProtoId = FALSE;

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    if (p_incoming->u4_Length <= p_in_db->u4_Length)
    {
        return (SNMP_ERR_WRONG_LENGTH);
    }
    else
    {
        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;
        i4_size_offset += INTEGER_LEN;

        if (p_incoming->u4_Length != (UINT4) i4_size_offset)
        {
            return (SNMP_ERR_WRONG_LENGTH);
        }
        /* Extracting The Integer Index. */
        i4_fsRrd6RoutingProtoId =
            (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];
        i4_offset++;

    }
    /*  Low Level Routine Which Validates the Indices. */
    /* COMMENT FOLLOWING CHECK TO ENABLE ROW CREATION 

       if((i1_ret_val = nmhValidateIndexInstanceFsRrd6RoutingProtoTable(i4_fsRrd6RoutingProtoId)) != SNMP_SUCCESS) {
       return SNMP_ERR_INCONSISTENT_NAME;
       } */
    switch (u1_arg)
    {

        case FSRRD6ALLOWOSPFAREAROUTES:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2FsRrd6AllowOspfAreaRoutes (&u4ErrorCode,
                                                    i4_fsRrd6RoutingProtoId,
                                                    p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSRRD6ALLOWOSPFEXTROUTES:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2FsRrd6AllowOspfExtRoutes (&u4ErrorCode,
                                                   i4_fsRrd6RoutingProtoId,
                                                   p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }
            /*  Read Only Variables */

        case FSRRD6ROUTINGPROTOID:
        case FSRRD6ROUTINGREGNID:
        case FSRRD6ROUTINGPROTOTASKIDENT:
        case FSRRD6ROUTINGPROTOQUEUEIDENT:
            return (SNMP_ERR_NOT_WRITABLE);

        default:
            return (SNMP_ERR_INCONSISTENT_NAME);
    }                            /* End of Switch */
}                                /*  THE TEST ROUTINES ARE OVER . */
