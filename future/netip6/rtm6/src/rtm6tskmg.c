/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rtm6tskmg.c,v 1.26 2017/12/26 13:34:23 siva Exp $
 *
 * Description: Task related functions are handled here. 
 *
 *******************************************************************/

#include "rtm6inc.h"
#include "rtm6red.h"
#include "rtm6defn.h"
#include "fsmrt6wr.h"
#include "ospf3.h"
#include "ripv6.h"
#include "isis.h"
#include "bgp.h"
#define MAX_RTM6_PROCESS_ROUTE_COUNT 100
extern VOID         RegisterFSRTM6 (void);
/*-------------------------------------------------------------------+
 *
 * Function           : Rtm6TaskMain 
 *
 * Input(s)           : None 
 *
 * Output(s)          : None
 *
 * Returns            : None
 *
 * Action             : This function initializes the global data structures
 *                      of the RTM6 Module. This function also waits for the 
 *                      messages from the routing protocols.
 *         
+-------------------------------------------------------------------*/
VOID
Rtm6TaskMain (INT1 *pi1TaskParam)
{
    tOsixQId            Rtm6MsgQId;
    tIp6Buf            *pBuf = NULL;
    tRtm6MsgHdr        *pRtm6MsgHdr = NULL;
    tRtm6RegnMsg        RegnMsg;
    tRtm6SnmpIfMsg     *pSnmpIfMsg = NULL;
    tRtm6RegnId         RegnId;
    tRtm6Cxt           *pRtm6Cxt = NULL;
    UINT4               u4EventMask = 0;
    UINT4               u4EntryCount = 0;
    UINT4               u4MaxNH = 0;
    UINT1               au1RMapName[RMAP_MAX_NAME_LEN + IP6_FOUR];
    tVcmRegInfo         VcmRegInfo;
    UINT4               u4Result = 0;
    UNUSED_PARAM (pi1TaskParam);

#ifdef LINUX_IPV6_WANTED
    /* Initialize the Registration Table and Trie Structure */
    Ip6InitRegTable ();
#endif

    Rtm6GetSizingParams ();

    if (OsixCreateSem ((const UINT1 *) RTM6_TASK_SEM_NAME,
                       1, 0, &gRtm6GlobalInfo.gSemId) != OSIX_SUCCESS)
    {
        RTM6_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    if (OsixCreateSem ((const UINT1 *) RTM6_REL_SEM_NAME,
                       1, 0, &gRtm6GlobalInfo.RELSemId) != OSIX_SUCCESS)
    {
        OsixDeleteSem (SELF, (CONST UINT1 *) RTM6_TASK_SEM_NAME);
        RTM6_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    /* Initialises the RTM6 Global Info Structure */
    if (Rtm6SizingMemCreateMemPools () == OSIX_FAILURE)
    {
        OsixDeleteSem (SELF, (CONST UINT1 *) RTM6_TASK_SEM_NAME);
        OsixDeleteSem (SELF, (CONST UINT1 *) RTM6_REL_SEM_NAME);
        RTM6_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }
    Rtm6Init ();

    /* Update the Current Value of Maximum allowed RTM6 context 
     * This is minimum of MAX_RTM6_CONTEXT_LIMIT and 
     * FsRTM6SizingParams[MAX_RTM6_CONTEXT_SIZING_ID].u4PreAllocatedUnits */
    gRtm6GlobalInfo.u4MaxContextLimit =
        (MAX_RTM6_CONTEXT_LIMIT <
         FsRTM6SizingParams[MAX_RTM6_CONTEXT_SIZING_ID].
         u4PreAllocatedUnits ? MAX_RTM6_CONTEXT_LIMIT :
         FsRTM6SizingParams[MAX_RTM6_CONTEXT_SIZING_ID].u4PreAllocatedUnits);

    /*Create the RBTree for PRT */
    u4MaxNH =
        FsRTM6SizingParams[MAX_RTM6_PEND_NEXTHOP_SIZING_ID].u4PreAllocatedUnits;
    gRtm6GlobalInfo.pPRT6RBRoot = RBTreeCreate (u4MaxNH, Rtm6RBComparePNh);

    if (gRtm6GlobalInfo.pPRT6RBRoot == NULL)
    {
        return;
    }
    /*Create the RBTree for ECMPPRT */
    u4MaxNH =
        FsRTM6SizingParams[MAX_RTM6_PEND_NEXTHOP_SIZING_ID].u4PreAllocatedUnits;
    gRtm6GlobalInfo.pECMP6PRTRBRoot = RBTreeCreate (u4MaxNH, Rtm6RBComparePNh);

    if (gRtm6GlobalInfo.pECMP6PRTRBRoot == NULL)
    {
        return;
    }

    /*Create the RBTree for Resolved Next Hop Table */
    u4MaxNH =
        FsRTM6SizingParams[MAX_RTM6_RESLV_NEXTHOP_SIZING_ID].
        u4PreAllocatedUnits;
    gRtm6GlobalInfo.pRNHT6RBRoot = RBTreeCreate (u4MaxNH, Rtm6RBCompareRNh);
    if (gRtm6GlobalInfo.pRNHT6RBRoot == NULL)
    {
        return;
    }
    /*Create the RBTree for All next hops of routes */
    u4MaxNH =
        FsRTM6SizingParams[MAX_RTM6_ALL_NEXTHOP_ENTRIES_SIZING_ID].
        u4PreAllocatedUnits;
    gRtm6GlobalInfo.pNextHopRBRoot = RBTreeCreate (u4MaxNH, Rtm6RBComparePNh);

    if (gRtm6GlobalInfo.pNextHopRBRoot == NULL)
    {
        return;
    }

    if (OsixCreateQ ((const UINT1 *) RTM6_MESSAGE_ARRIVAL_Q_NAME,
                     RTM6_MESSAGE_ARRIVAL_Q_DEPTH,
                     RTM6_MESSAGE_ARRIVAL_Q_MODE, &Rtm6MsgQId) != OSIX_SUCCESS)
    {
        OsixDeleteSem (SELF, (CONST UINT1 *) RTM6_TASK_SEM_NAME);
        OsixDeleteSem (SELF, (CONST UINT1 *) RTM6_REL_SEM_NAME);
        Rtm6SizingMemDeleteMemPools ();
        RTM6_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    if (OsixCreateQ ((const UINT1 *) RTM6_SNMP_IF_Q_NAME,
                     RTM6_SNMP_IF_Q_DEPTH,
                     RTM6_SNMP_IF_Q_MODE, &Rtm6MsgQId) != OSIX_SUCCESS)
    {
        OsixDeleteSem (SELF, (CONST UINT1 *) RTM6_TASK_SEM_NAME);
        OsixDeleteSem (SELF, (CONST UINT1 *) RTM6_REL_SEM_NAME);
        OsixDeleteQ (SELF, (const UINT1 *) RTM6_MESSAGE_ARRIVAL_Q_NAME);
        Rtm6SizingMemDeleteMemPools ();
        RTM6_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }
    if (OsixCreateQ ((const UINT1 *) RTM6_RT_MSG_Q_NAME,
                     RTM6_RT_MSG_Q_DEPTH,
                     RTM6_RT_MSG_Q_MODE, &Rtm6MsgQId) != OSIX_SUCCESS)
    {
        OsixDeleteSem (SELF, (CONST UINT1 *) RTM6_TASK_SEM_NAME);
        OsixDeleteSem (SELF, (CONST UINT1 *) RTM6_REL_SEM_NAME);
        OsixDeleteQ (SELF, (const UINT1 *) RTM6_MESSAGE_ARRIVAL_Q_NAME);
        OsixDeleteQ (SELF, (const UINT1 *) RTM6_SNMP_IF_Q_NAME);
        Rtm6SizingMemDeleteMemPools ();
        RTM6_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }
    /* Create Timer-Lists for RTM Timers */
    if (TmrCreateTimerList ((const UINT1 *) RTM6_MAIN_TASK_NAME,
                            RTM6_TMR_EVENT, NULL,
                            &gRtm6GlobalInfo.Rtm6TmrListId) != TMR_SUCCESS)
    {
        OsixDeleteSem (SELF, (CONST UINT1 *) RTM6_TASK_SEM_NAME);
        OsixDeleteSem (SELF, (CONST UINT1 *) RTM6_REL_SEM_NAME);
        OsixDeleteQ (SELF, (const UINT1 *) RTM6_MESSAGE_ARRIVAL_Q_NAME);
        OsixDeleteQ (SELF, (const UINT1 *) RTM6_SNMP_IF_Q_NAME);
        OsixDeleteQ (SELF, (const UINT1 *) RTM6_RT_MSG_Q_NAME);
        Rtm6SizingMemDeleteMemPools ();
        RTM6_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }
    MEMSET (&gRtm6GlobalInfo.Rtm6FrtInfo, 0, sizeof (tRtm6FrtInfo));
    MEMSET ((UINT1 *) &VcmRegInfo, 0, sizeof (tVcmRegInfo));
    Rtm6TmrSetTimer (&(gRtm6GlobalInfo.Rtm6FRTTmr), RTM6_FRT_TIMER,
                     RTM6_FRT_TIMER_INTERVAL);
    VcmRegInfo.pIfMapChngAndCxtChng = Rtm6VcmCallBackFn;
    VcmRegInfo.u1InfoMask |= (VCM_CONTEXT_CREATE | VCM_CONTEXT_DELETE);
    VcmRegInfo.u1ProtoId = RTM6_PROTOCOL_ID;
    VcmRegisterHLProtocol (&VcmRegInfo);

    if (Rtm6CreateCxt (RTM6_DEFAULT_CXT_ID) == RTM6_FAILURE)
    {
        OsixDeleteSem (SELF, (CONST UINT1 *) RTM6_TASK_SEM_NAME);
        OsixDeleteSem (SELF, (CONST UINT1 *) RTM6_REL_SEM_NAME);
        OsixDeleteQ (SELF, (const UINT1 *) RTM6_MESSAGE_ARRIVAL_Q_NAME);
        OsixDeleteQ (SELF, (const UINT1 *) RTM6_SNMP_IF_Q_NAME);
        OsixDeleteQ (SELF, (const UINT1 *) RTM6_RT_MSG_Q_NAME);
        Rtm6SizingMemDeleteMemPools ();
        RTM6_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }
    pRtm6Cxt = UtilRtm6GetCxt (RTM6_DEFAULT_CXT_ID);
    if (pRtm6Cxt == NULL)
    {
        OsixDeleteSem (SELF, (CONST UINT1 *) RTM6_TASK_SEM_NAME);
        OsixDeleteSem (SELF, (CONST UINT1 *) RTM6_REL_SEM_NAME);
        OsixDeleteQ (SELF, (const UINT1 *) RTM6_MESSAGE_ARRIVAL_Q_NAME);
        OsixDeleteQ (SELF, (const UINT1 *) RTM6_SNMP_IF_Q_NAME);
        OsixDeleteQ (SELF, (const UINT1 *) RTM6_RT_MSG_Q_NAME);
        Rtm6SizingMemDeleteMemPools ();
        RTM6_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }
    if (UtilRtm6GetVcmSystemModeExt (RTM6_PROTOCOL_ID) == VCM_SI_MODE)
    {
        gRtm6GlobalInfo.pRtm6Cxt = pRtm6Cxt;
    }
#ifdef SNMP_2_WANTED
    RegisterFSRTM6 ();
    RegisterFSMIRT6 ();
#endif

#ifdef ROUTEMAP_WANTED
    /* register callback for route map updates */
    RMapRegister (RMAP_APP_RTM6, Rtm6SendRouteMapUpdateMsg);
#endif

#ifdef ISS_WANTED
    RTM6_INIT_COMPLETE (OSIX_SUCCESS);
#endif
    for (;;)
    {
        u4EventMask = 0;

        OsixReceiveEvent (RTM6_MESSAGE_ARRIVAL_EVENT | RTM6_FILTER_ADDED_EVENT |
                          RTM6_KERNEL_UPD_EVENT | RTM6_TMR_EVENT |
                          RTM6_PROTOCOL_STATUS_EVENT | RTM6_RM_PKT_EVENT |
                          RTM6_RED_TIMER_EVENT | RTM6_ROUTE_MSG_EVENT |
                          RTM6_HA_PEND_BLKUPD_EVENT |
                          RTM6_HA_FRT_PEND_BLKUPD_EVENT, RTM6_EVENT_WAIT_FLAGS,
                          RTM6_EVENT_WAIT_TIMEOUT, &u4EventMask);

        if (RTM6_TASK_LOCK () == SNMP_FAILURE)
        {
            continue;
        }
        if (u4EventMask & RTM6_ROUTE_MSG_EVENT)
        {
            RTM6_Process_Route_Msg_Event ();
        }
        if ((u4EventMask & RTM6_MESSAGE_ARRIVAL_EVENT) ||
            (u4EventMask & RTM6_PROTOCOL_STATUS_EVENT))
        {
            while (OsixReceiveFromQ (RTM6_MESSAGE_ARRIVAL_Q_NODE_ID,
                                     (const UINT1 *)
                                     RTM6_MESSAGE_ARRIVAL_Q_NAME, OSIX_NO_WAIT,
                                     0, &pBuf) == OSIX_SUCCESS)
            {
                /* The message header given by the routing protocols will be
                   there in the module data of the message. */

                pRtm6MsgHdr = (tRtm6MsgHdr *) IP6_GET_MODULE_DATA_PTR (pBuf);

                switch (pRtm6MsgHdr->u1MessageType)
                {
                    case RTM6_REGISTRATION_MESSAGE:

                        MEMSET (&RegnMsg, 0, sizeof (tRtm6RegnMsg));

                        RegnMsg.RegnId = pRtm6MsgHdr->RegnId;
                        if (pRtm6MsgHdr->pDeliverToApplication != NULL)
                        {
                            RegnMsg.DeliverToApplication =
                                (INT4 (*)(tRtm6RespInfo *, tRtm6MsgHdr *))
                                pRtm6MsgHdr->pDeliverToApplication;
                        }

                        RegnMsg.u1BitMask = pRtm6MsgHdr->u1BitMask;

                        if (Rtm6ProcessRegistration (RegnMsg) != RTM6_SUCCESS)
                        {
                            RTM6_TRC_ARG1 (pRtm6MsgHdr->RegnId.u4ContextId,
                                           RTM6_CTRL_PATH_TRC, RTM6_MOD_NAME,
                                           "\tProtocol [%d] Registration is not Successful\n",
                                           pRtm6MsgHdr->RegnId.u2ProtoId);
                        }

                        IP6_RELEASE_BUF (pBuf, FALSE);
                        break;
                    case RTM6_DEREGISTRATION_MESSAGE:
                        pRtm6Cxt =
                            UtilRtm6GetCxt (pRtm6MsgHdr->RegnId.u4ContextId);

                        if (pRtm6Cxt != NULL)
                        {
                            Rtm6UtilDeregister (pRtm6Cxt, &pRtm6MsgHdr->RegnId);
                        }
                        IP6_RELEASE_BUF (pBuf, FALSE);
                        break;

                    case RTM6_REDISTRIBUTE_ENABLE_MESSAGE:

                        RegnId = pRtm6MsgHdr->RegnId;
                        Rtm6HandleRrd6EnableMsg (&RegnId, pBuf);
                        break;

                    case RTM6_REDISTRIBUTE_DISABLE_MESSAGE:

                        RegnId = pRtm6MsgHdr->RegnId;
                        Rtm6HandleRrd6DisableMsg (&RegnId, pBuf);
                        break;

                    case RTM6_ROUTEMAP_UPDATE_MSG:
                        MEMSET (au1RMapName, 0, sizeof (au1RMapName));
                        /* copy map name from offset 4 */
                        if (IP_COPY_FROM_BUF (pBuf, au1RMapName, IP6_FOUR,
                                              RMAP_MAX_NAME_LEN) !=
                            (INT4) RMAP_MAX_NAME_LEN)
                        {
                            IP6_RELEASE_BUF (pBuf, FALSE);
                            break;
                        }

                        Rtm6ProcessRouteMapChanges (au1RMapName);

                        IP6_RELEASE_BUF (pBuf, FALSE);

                        break;

#ifdef MBSM_WANTED                /* IP6_CHASSIS_MERGE */
                    case MBSM_MSG_CARD_INSERT:

                        Rtm6MbsmUpdateRtTable (pBuf, MBSM_MSG_CARD_INSERT);
                        break;

                    case MBSM_MSG_CARD_REMOVE:

                        Rtm6MbsmUpdateRtTable (pBuf, MBSM_MSG_CARD_REMOVE);
                        break;
#endif
                    case RTM6_VCM_MSG_RCVD:

                        Rtm6VcmMsgHandler (pBuf);
                        break;
                    case RTM6_GR_NOTIFY_MSG:
                        RegnId.u2ProtoId = pRtm6MsgHdr->RegnId.u2ProtoId;

                        if ((RegnId.u2ProtoId == BGP6_ID) ||
                            (RegnId.u2ProtoId == RIPNG_ID))
                        {
                            Rtm6GRStaleRtMark (&RegnId);
                        }
                        if (RegnId.u2ProtoId != RIPNG_ID)
                        {
                            Rtm6StartGRTmr (&pRtm6MsgHdr->RegnId, pBuf);
                        }
                        break;
                    case RTM6_PROTOCOL_SHUTDOWN:
                        Rtm6UtilHandleProtocolShutdown (pRtm6MsgHdr->RegnId.
                                                        u4ContextId,
                                                        (UINT1) pRtm6MsgHdr->
                                                        RegnId.u2ProtoId);
                        IP6_RELEASE_BUF (pBuf, FALSE);
                        break;
                    case RTM6_INTERFACE_SHUTDOWN:

                        Rtm6UtilHandleProtocolIfShutdown (pRtm6MsgHdr->RegnId.
                                                          u4ContextId,
                                                          (UINT1) pRtm6MsgHdr->
                                                          RegnId.u2ProtoId,
                                                          pRtm6MsgHdr->u4Index);
                        IP6_RELEASE_BUF (pBuf, FALSE);
                        break;
                    case RTM6_GR_CLEANUP:

                        MEMSET (&RegnId, 0, sizeof (tRtm6RegnId));
                        RegnId.u2ProtoId = pRtm6MsgHdr->RegnId.u2ProtoId;
                        RegnId.u4ContextId = pRtm6MsgHdr->RegnId.u4ContextId;
                        Rtm6TrieProcessGRRouteCleanUp (&RegnId,
                                                       pRtm6MsgHdr->i1GrFlag);
                        IP6_RELEASE_BUF (pBuf, FALSE);
                        break;
                    default:
                        IP6_RELEASE_BUF (pBuf, FALSE);
                        break;

                }
            }                    /* While */
        }                        /* Message Arrival Event Processed */

        if (u4EventMask & RTM6_FILTER_ADDED_EVENT)
        {
            while (OsixReceiveFromQ (RTM6_SNMP_IF_Q_NODE_ID,
                                     (const UINT1 *) RTM6_SNMP_IF_Q_NAME,
                                     OSIX_NO_WAIT, 0, &pBuf) == OSIX_SUCCESS)
            {

                pSnmpIfMsg = (tRtm6SnmpIfMsg *) IP6_GET_MODULE_DATA_PTR (pBuf);

                switch (pSnmpIfMsg->u1Cmd)
                {
                    case RTM6_CHG_IN_RRD6_CONTROL_TABLE:
                        Rtm6HandleChgInFilterTable (pSnmpIfMsg->pRtm6Filter,
                                                    pSnmpIfMsg->u1PermitOrDeny);
                        break;

                    case RTM6_CHG_IN_RRD6_DEF_CTRL_MODE:
                        Rtm6HandleChgInDefMode (pSnmpIfMsg->pRtm6Filter,
                                                pSnmpIfMsg->u1PermitOrDeny);
                        break;

                    case RTM6_OSPF_INT_EXT_CHG:
                        Rtm6HandleChgInOspfType (&(pSnmpIfMsg->RegnId),
                                                 pSnmpIfMsg->u1OspfRtType,
                                                 pSnmpIfMsg->u1PermitOrDeny);
                        break;
                    default:
                        break;
                }
                IP6_RELEASE_BUF (pBuf, FALSE);
            }

        }                        /* Filter Added Event Processed */

        if (u4EventMask & RTM6_TMR_EVENT)
        {
            Rtm6ProcessTimerExpiry ();

        }
        if (u4EventMask & RTM6_RM_PKT_EVENT)
        {
            Rtm6RedHandleRmEvents ();
        }

        if (u4EventMask & RTM6_HA_PEND_BLKUPD_EVENT)
        {
            Rtm6RedSendBulkUpdMsg ();
        }

        if (u4EventMask & RTM6_HA_FRT_PEND_BLKUPD_EVENT)
        {
            Rtm6RedSendFrtBulkUpdMsg ();
        }

        if (gRtm6RedGlobalInfo.u1RedStatus == IP6_TRUE)
        {
            if ((gRtm6RedGlobalInfo.u1NodeStatus == RM_ACTIVE) &&
                (RTM6_IS_STANDBY_UP () == OSIX_TRUE))
            {
                u4EntryCount = 0;
                RBTreeCount (gRtm6GlobalInfo.Rtm6RedTable, &u4EntryCount);
                if (u4EntryCount > 0)
                {
                    Rtm6RedSendDynamicInfo ();
                }
            }
        }

        /* Check whether the 1SEC Timer needed to be restarted or not. */
        if (gRtm6GlobalInfo.u1OneSecTmrStartReq == TRUE)
        {
            /* Timer needs to be started. Check whether the timer is running
             * or not. */
            if (gRtm6GlobalInfo.u1OneSecTimerStart == FALSE)
            {
                /* Start the one second Timer. */
                u4Result = TmrStart (gRtm6GlobalInfo.Rtm6TmrListId,
                                     &(gRtm6GlobalInfo.Rtm6OneSecTmr),
                                     RTM6_ONESEC_TIMER, 1, 0);

                gRtm6GlobalInfo.u1OneSecTimerStart = TRUE;
            }
            gRtm6GlobalInfo.u1OneSecTmrStartReq = FALSE;
        }

        RTM6_TASK_UNLOCK ();
    }
    /* FOREVER */
    UNUSED_PARAM (u4Result);
}

/*-------------------------------------------------------------------+
 *
 * Function           : Rtm6EnqueuePktToRpsInCxt
 *
 * Input(s)           : pRtm6Cxt  - RTM6 Context Pointer.
 *                    : pRespInfo - Pointer to the RTM6 Response info.
 *                      Rtm6Header - Pointer to the RTM6 Response Header message
 *                      pRegnId - Register ID.
 *
 * Output(s)          : None
 *
 * Returns            : SUCCESS, FAILURE
 *
 * Action             : This function initializes the global data structures
                        of the RTM6 Module. This function also waits for the 
                        messages from the routing protocols.
 *         
+-------------------------------------------------------------------*/
INT1
Rtm6EnqueuePktToRpsInCxt (tRtm6Cxt * pRtm6Cxt,
                          tRtm6RespInfo * pRespInfo, tRtm6MsgHdr * pRtm6Header,
                          tRtm6RegnId * pRegnId)
{
    INT4                i4ReturnStatus = RTM6_SUCCESS;
    UINT2               u2RegnId = 0;

    u2RegnId = Rtm6GetRegnIndexFromRegnIdInCxt (pRtm6Cxt, pRegnId);
    if (u2RegnId == RTM6_INVALID_REGN_ID)
    {
        i4ReturnStatus = RTM6_FAILURE;
        return ((INT1) i4ReturnStatus);
    }

    if (pRtm6Cxt->aRtm6RegnTable[u2RegnId].DeliverToApplication != NULL)
    {
        i4ReturnStatus =
            pRtm6Cxt->aRtm6RegnTable[u2RegnId].DeliverToApplication (pRespInfo,
                                                                     pRtm6Header);
    }

    return ((INT1) i4ReturnStatus);
}

/*-------------------------------------------------------------------+
 *
 * Function           : Rtm6GetRegnIndexFromRegnIdInCxt
 *
 * Input(s)           : pRtm6Cxt   - RTM6 Context Pointer.
 *                    : pRegnId    - Registration Information.
 *
 * Output(s)          : None
 *
 * Returns            : Registraion Table Index
 *
 * Action             : This function scans thru the RTM6 registration table 
 *                      and finds the registration ID for the matching      
 *                      protocol ID.
 *         
+-------------------------------------------------------------------*/
UINT2
Rtm6GetRegnIndexFromRegnIdInCxt (tRtm6Cxt * pRtm6Cxt, tRtm6RegnId * pRegnId)
{
    UINT2               u2Index = 0;

    /* Registration Id is (Protocol Id - 1) */
    u2Index = (UINT2) (pRegnId->u2ProtoId - 1);
    if (u2Index < IP6_MAX_ROUTING_PROTOCOLS)
    {
        if (pRtm6Cxt->aRtm6RegnTable[u2Index].u2RoutingProtocolId
            == pRegnId->u2ProtoId)
        {
            return u2Index;
        }
    }
    /* Protocol is not registered. */
    return RTM6_INVALID_REGN_ID;
}

/*-------------------------------------------------------------------+
 *
 * Function           : RpsEnqueuePktToRtm6
 *
 * Input(s)           : pBuf - Pointer to the buffer to be enqueued.
 *
 * Output(s)          : None
 *
 * Returns            : SUCCESS, FAILURE
 *
 * Action             : This function enqueues the packet to the RTM6 task
                        and then sends event to it.
 *
+-------------------------------------------------------------------*/
INT1
RpsEnqueuePktToRtm6 (tCRU_BUF_CHAIN_HEADER * pBuf)
{
    if (OsixSendToQ (SELF,
                     (const UINT1 *) RTM6_MESSAGE_ARRIVAL_Q_NAME,
                     pBuf, OSIX_MSG_NORMAL) != OSIX_SUCCESS)
    {
        IP6_RELEASE_BUF (pBuf, FALSE);
        return (RTM6_FAILURE);
    }

    OsixSendEvent (SELF, (const UINT1 *) RTM6_MAIN_TASK_NAME,
                   RTM6_MESSAGE_ARRIVAL_EVENT);

    return (RTM6_SUCCESS);
}

/*-------------------------------------------------------------------+
 *
 * Function           : Rtm6HandleExportMaskEvent
 *
 * Input(s)           : pRtInfo - Info about the best route to be 
 *                                redistributed.
 *
 * Output(s)          : None
 *
 * Returns            : RTM6_SUCCESS/RTM_FAILURE
 *
 * Action             : This function is the call back function to 
 *                      handle the route redistribution when RP request
 *                      for redistribution of any specific protocol routes.
 *
+-------------------------------------------------------------------*/
INT4
Rtm6HandleExportMaskEvent (tIp6RtEntry * pRtInfo, VOID *pAppSpecData)
{
    tRtm6RegnId         RegnId;
    tRtm6InitRoute     *pRtm6InitInfo = (tRtm6InitRoute *) pAppSpecData;
    UINT2               u2SrcProtoMask = 0;
    UINT2               u2DestProtoMask = RTM6_DONT_REDISTRIBUTE;
    UINT2               u2RedisProtoMask = 0;
    INT4                i4RetVal = RTM6_SUCCESS;

    /* Check whether this best route needs to be redistributed */
    RTM6_SET_BIT (u2SrcProtoMask, pRtInfo->i1Proto);
    RTM6_SET_BIT (u2RedisProtoMask, pRtm6InitInfo->u2DestProto);

    if (pRtm6InitInfo->u2Data & RTM6_MATCH_ALL_TYPE)
    {
        pRtm6InitInfo->u2Data |= RTM6_OSPF_MASK;
    }
    if (pRtm6InitInfo->u2Data & RTM6_ISISL1L2_MASK)
    {
        pRtm6InitInfo->u2Data |= RTM6_ISIS_MASK;
    }
    if (pRtInfo->i1Proto == OSPF6_ID)
    {
        i4RetVal =
            Rtm6CheckForMatch (pRtm6InitInfo->u2Data, pRtInfo->u1MetricType);
    }
    else if (pRtInfo->i1Proto == ISIS6_ID)
    {
        i4RetVal =
            Rtm6CheckForLevel (pRtm6InitInfo->u2Data, pRtInfo->u1MetricType);
    }

    if (((pRtm6InitInfo->u2Data & u2SrcProtoMask) == u2SrcProtoMask) &&
        (i4RetVal == RTM6_SUCCESS))
    {
        /* Route needs to be redistributed. */
        if (pRtm6InitInfo->u2Event == RTM6_ENABLE_PROTO_MASK)
        {
            RegnId.u2ProtoId = (UINT2) pRtInfo->i1Proto;
            RegnId.u4ContextId = pRtm6InitInfo->pRtm6Cxt->u4ContextId;

            /* check if rmap is registered in RTM4, i.e. redistribution is via rmap */
            if (Rtm6CheckIsRouteMapConfiguredInCxt (pRtm6InitInfo->pRtm6Cxt,
                                                    pRtm6InitInfo->
                                                    u2DestProto) ==
                RTM6_FAILURE)
            {

                /* no rmap, redistribute as is */
                Rtm6CheckRouteForRedistributionInCxt (pRtm6InitInfo->pRtm6Cxt,
                                                      &RegnId, pRtInfo,
                                                      &u2DestProtoMask);
                if ((u2DestProtoMask != RTM6_DONT_REDISTRIBUTE)
                    && ((u2DestProtoMask & u2RedisProtoMask) ==
                        u2RedisProtoMask))
                {
                    /* Route can be redistributed to the registering protocol. */
                    Rtm6RouteRedistributionInCxt (pRtm6InitInfo->pRtm6Cxt,
                                                  pRtInfo, TRUE, IP6_BIT_ALL,
                                                  u2RedisProtoMask);
                }

            }
            else
            {
                Rtm6ApplyRouteMapRuleAndRedistributeInCxt (pRtm6InitInfo->
                                                           pRtm6Cxt,
                                                           RegnId.u2ProtoId,
                                                           pRtInfo, TRUE,
                                                           IP6_BIT_ALL,
                                                           u2RedisProtoMask);
            }

        }
        else
        {
            /* It is expected that routing protocol will take care of removing
             * the redistributed routes. So just reset the Redistribute Flag */
            pRtInfo->u2RedisMask &= ~u2RedisProtoMask;
        }
    }

    return RTM6_SUCCESS;
}

/*****************************************************************************
* DESCRIPTION : Function for Locking Rtm6 Task
* INPUTS      : None 
* OUTPUTS     : None
* RETURNS     : SNMP_SUCCESS/SNMP_FAILURE
****************************************************************************/
INT4
Rtm6Lock (VOID)
{
    if (OsixTakeSem (SELF, (const UINT1 *) RTM6_TASK_SEM_NAME,
                     OSIX_WAIT, 0) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/*****************************************************************************
* DESCRIPTION : Function for UnLocking Rtm6 Task
* INPUTS      : None 
* OUTPUTS     : None
* RETURNS     : SNMP_SUCCESS/SNMP_FAILURE
****************************************************************************/
INT4
Rtm6UnLock (VOID)
{
    if (OsixGiveSem (SELF, (const UINT1 *) RTM6_TASK_SEM_NAME) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/*****************************************************************************
* DESCRIPTION : Function for Locking Route Entry List
* INPUTS      : None
* OUTPUTS     : None
* RETURNS     : SNMP_SUCCESS/SNMP_FAILURE
****************************************************************************/
INT4
Rtm6RelLock (VOID)
{
    if (OsixTakeSem (SELF, (const UINT1 *) RTM6_REL_SEM_NAME,
                     OSIX_WAIT, 0) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/*****************************************************************************
* DESCRIPTION : Function for UnLocking Route Entry List 
* INPUTS      : None
* OUTPUTS     : None
* RETURNS     : SNMP_SUCCESS/SNMP_FAILURE
****************************************************************************/
INT4
Rtm6RelUnLock (VOID)
{
    if (OsixGiveSem (SELF, (const UINT1 *) RTM6_REL_SEM_NAME) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/*-------------------------------------------------------------------+
 * *
 * * Function           : RTM6_Process_Route_Msg_Event
 * *
 * * Input(s)           : None
 * *
 * * Output(s)          : None
 * *
 * * Returns            : None
 * *
 * * Action             : This function Proceeses the Message posted for
 * *                      route updates.
 * *-------------------------------------------------------------------*/
VOID
RTM6_Process_Route_Msg_Event (VOID)
{
    tRtm6RtMsg         *pRtm6RtMsg = NULL;
    UINT4               u4Count = 0;

    while (OsixReceiveFromQ (RTM6_RT_MSG_Q_NODE_ID,
                             (const UINT1 *) RTM6_RT_MSG_Q_NAME,
                             OSIX_NO_WAIT, 0,
                             (tOsixMsg **) (VOID *) &pRtm6RtMsg) ==
           OSIX_SUCCESS)
    {
        switch (pRtm6RtMsg->u1MsgType)
        {
            case RTM6_ADD_ROUTE:
            case RTM6_DELETE_ROUTE:
            case RTM6_MODIFY_ROUTE:
                Rtm6UtilIpv6LeakRoute (pRtm6RtMsg->u1MsgType,
                                       &pRtm6RtMsg->unRtm6Msg.RtInfo);

                RTM6_ROUTE_MSG_FREE (pRtm6RtMsg);
                u4Count++;
                if (u4Count > MAX_RTM6_PROCESS_ROUTE_COUNT)
                {
                    u4Count = 0;
                    OsixSendEvent (SELF, (const UINT1 *) RTM6_MAIN_TASK_NAME,
                                   RTM6_ROUTE_MSG_EVENT);
#ifdef OSPF3_WANTED
                    OsixSendEvent (SELF, (const UINT1 *) "OSV3",
                                   OSPFV3_RTM6_ROUTE_PROCESS_EVENT);
#endif
#ifdef RIP6_WANTED
                    OsixSendEvent (SELF, (const UINT1 *) "RIP6",
                                   RIP6_RTM6_ROUTE_PROCESS_EVENT);
#endif
#ifdef BGP4_IPV6_WANTED
                    OsixSendEvent (SELF, (const UINT1 *) "Bgp",
                                   BGP4_RTM6_ROUTE_PROCESS_EVENT);
#endif
                    OsixSendEvent (SELF, (const UINT1 *) "ISIS",
                                   ISIS_RTM6_ROUTE_PROCESS_EVENT);
                    return;
                }
                break;
            default:
                RTM6_ROUTE_MSG_FREE (pRtm6RtMsg);
                break;
        }

    }
}
