/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rtm6api.c,v 1.56 2018/02/01 11:02:29 siva Exp $
 *
 * Description:This file contains APIs provided by RTM6   
 *
 *******************************************************************/

#include "rtm6inc.h"
#include "ipv6.h"
#include "ipvx.h"
#include "rtm6frt.h"
#include "ospf3.h"
#include "ripv6.h"
#include "isis.h"
#include "bgp.h"

UINT1               gu1Rtm6RouteCxtFlag = OSIX_TRUE;
/* private function */
#ifdef NPAPI_WANTED
VOID
 
 
 
 
 
 
 
 Rtm6FillIntInfo (tIp6RtEntry * pIp6RtInfo, tFsNpIntInfo * pIntInfo,
                  UINT4 *pu4NHType);
#endif

#ifdef LNXIP6_WANTED
/*
******************************************************************************
* Function           : Rtm6CheckRtExistsInInvdRouteList
* Input(s)           : pIp6Addr - IPv6 address of the static route to be searched
*                      i4PrefixLen - Prefix length of the static route
* Output(s)          : None
* Returns            : if Found, returns RTM6_SUCCESS otherwise RTM6_FAILURE
* Action             : This function checks whether the route present in invalid list or not.
******************************************************************************
*/

INT4
Rtm6CheckRtExistsInInvdRouteList (tIp6Addr * pIp6Addr, INT4 i4PrefixLen,
                                  tIp6Addr * pIp6Address)
{
    tIp6Addr            Ip6InvdAddr;
    tIp6Addr            Ip6InvdNextHop;
    tRtm6Cxt           *pRtm6Cxt = NULL;
    tIp6RtEntry        *pIp6InvdRtEntry = NULL;
    UINT4               u4Context = IP6_ZERO;

    MEMSET (&Ip6InvdAddr, 0, sizeof (tIp6Addr));

    pRtm6Cxt = UtilRtm6GetCxt (u4Context);
    if (pRtm6Cxt == NULL)
    {
        return RTM6_FAILURE;
    }

    TMO_SLL_Scan (&(pRtm6Cxt->Rtm6InvdRtList), pIp6InvdRtEntry, tIp6RtEntry *)
    {
        MEMCPY (&Ip6InvdAddr, &(pIp6InvdRtEntry->dst), IP6_ADDR_SIZE);
        MEMCPY (&Ip6InvdNextHop, &(pIp6InvdRtEntry->nexthop), IP6_ADDR_SIZE);
        if ((Ip6AddrCompare (Ip6InvdNextHop, *pIp6Address) == IP6_ZERO) &&
            (Ip6AddrMatch (&Ip6InvdAddr, pIp6Addr, i4PrefixLen) == TRUE))
        {
            return RTM6_SUCCESS;
        }
    }
    return RTM6_FAILURE;
}

/*
******************************************************************************
* Function           : Rtm6UpdateInvdRouteList
* Input(s)           : u4Index - Interface Index to reach next hop address
*                      pIp6Addr - IPv6 address of the interface
*                      i4PrefixLen - Prefix length
*                      u1CmdType - Specifies add or delete call
* Output(s)          : None
* Returns            : NONE
* Action             : This function checks whether the next hop is reachable
*                       for any static routes in Invalid route list with the newly
*                       updated interface
******************************************************************************
*/
VOID
Rtm6UpdateInvdRouteList (UINT4 u4Index, tIp6Addr * pIp6Addr, INT4 i4PrefixLen,
                         UINT1 u1CmdType)
{
    tIp6Addr            Ip6InvdAddr;
    tNetIpv6RtInfo      NetIp6RtInfo;
    tIp6RtEntry        *pIp6InvdRtEntry = NULL;
    tIp6RtEntry        *pIp6TempInvdRtEntry = NULL;
    tRtm6Cxt           *pRtm6Cxt = NULL;
    UINT4               u4Context = IP6_ZERO;

    MEMSET (&NetIp6RtInfo, 0, sizeof (tNetIpv6RtInfo));
    MEMSET (&Ip6InvdAddr, 0, sizeof (tIp6Addr));

    pRtm6Cxt = UtilRtm6GetCxt (u4Context);

    if (pRtm6Cxt == NULL)
    {
        return;
    }

    if (u1CmdType == NETIPV6_ADD_ROUTE)
    {
        TMO_DYN_SLL_Scan (&(pRtm6Cxt->Rtm6InvdRtList), pIp6InvdRtEntry,
                          pIp6TempInvdRtEntry, tIp6RtEntry *)
        {
            MEMCPY (&Ip6InvdAddr, &pIp6InvdRtEntry->nexthop, IP6_ADDR_SIZE);

            if (Ip6AddrMatch (&Ip6InvdAddr, pIp6Addr, i4PrefixLen) == TRUE)
            {
                MEMCPY (&NetIp6RtInfo.Ip6Dst, &pIp6InvdRtEntry->dst,
                        IP6_ADDR_SIZE);
                NetIp6RtInfo.u1Prefixlen = pIp6InvdRtEntry->u1Prefixlen;
                MEMCPY (&NetIp6RtInfo.NextHop, &pIp6InvdRtEntry->nexthop,
                        IP6_ADDR_SIZE);
                NetIp6RtInfo.u4Index = u4Index;
                NetIp6RtInfo.u4Metric = pIp6InvdRtEntry->u4Metric;
                NetIp6RtInfo.u4RouteTag = pIp6InvdRtEntry->u4RouteTag;
                NetIp6RtInfo.i1Proto = pIp6InvdRtEntry->i1Proto;
                NetIp6RtInfo.i1Type = pIp6InvdRtEntry->i1Type;
                NetIp6RtInfo.i1DefRtrFlag = pIp6InvdRtEntry->i1DefRtrFlag;
                NetIp6RtInfo.u4RowStatus = pIp6InvdRtEntry->u4RowStatus;
                NetIp6RtInfo.u1Preference = pIp6InvdRtEntry->u1Preference;
                NetIp6RtInfo.u1MetricType = pIp6InvdRtEntry->u1MetricType;
                NetIp6RtInfo.u1AddrType = pIp6InvdRtEntry->u1AddrType;

                if ((NetIp6RtInfo.u1Prefixlen == 0) &&
                    (IS_ADDR_UNSPECIFIED (NetIp6RtInfo.Ip6Dst)))
                {
                    NetIp6RtInfo.i1DefRtrFlag = IP6_DEF_RTR_FLAG;
                }
                else
                {
                    NetIp6RtInfo.i1DefRtrFlag = 0;
                }

                NetIp6RtInfo.u2ChgBit = IP6_BIT_ALL;
                if (Rtm6UtilIpv6LeakRoute (NETIPV6_ADD_ROUTE, &NetIp6RtInfo) ==
                    RTM6_FAILURE)
                {
                    return;
                }

                if (Ip6EnableInvalidRoute (&NetIp6RtInfo) == SNMP_SUCCESS)
                {
                    TMO_SLL_Delete (&(pRtm6Cxt->Rtm6InvdRtList),
                                    &(pIp6InvdRtEntry->NextEntry));
                    IP6_RT_FREE (pIp6InvdRtEntry);
                }
            }
        }
    }
    else if (u1CmdType == NETIPV6_DELETE_ROUTE)
    {
        TMO_SLL_Scan (&(pRtm6Cxt->Rtm6InvdRtList), pIp6InvdRtEntry,
                      tIp6RtEntry *)
        {
            MEMCPY (&Ip6InvdAddr, &pIp6InvdRtEntry->dst, IP6_ADDR_SIZE);
            if (Ip6AddrMatch (&Ip6InvdAddr, pIp6Addr, i4PrefixLen) == TRUE)
            {
                TMO_SLL_Delete (&(pRtm6Cxt->Rtm6InvdRtList),
                                &(pIp6InvdRtEntry->NextEntry));
                IP6_RT_FREE (pIp6InvdRtEntry);
                break;
            }
        }
    }

    return;
}

/*
******************************************************************************
* Function           : Rtm6InsertRouteInInvdRouteList
* Input(s)           : pNetIp6RtInfo - Static route details to be added in invalid list
* Output(s)          : None
* Returns            : NONE
* Action             : This function adds the static route to Invalid route list.
*                       Allocates memory from gRtm6GlobalInfo.Rtm6RtTblPoolId
******************************************************************************
*/
INT4
Rtm6InsertRouteInInvdRouteList (tNetIpv6RtInfo * pNetIp6RtInfo)
{
    tIp6RtEntry        *pIp6InvdRtEntry = NULL;
    tIp6RtEntry        *pTmpIp6RtEntry = NULL;
    tNetIpv6RtInfo      TmpIp6RtInfo;
    tIp6RtEntry        *pPrevIp6RtInfo = NULL;
    tRtm6Cxt           *pRtm6Cxt = NULL;

    MEMSET (&TmpIp6RtInfo, IP_ZERO, sizeof (tNetIpv6RtInfo));
    pRtm6Cxt = UtilRtm6GetCxt (pNetIp6RtInfo->u4ContextId);

    if (pRtm6Cxt == NULL)
    {
        /* Memory allocation to add new route fails. */
        RTM6_TRC_ARG2 (RTM6_INVALID_CXT_ID, RTM6_ALL_FAILURE_TRC |
                       RTM6_OS_RESOURCE_TRC, RTM6_MOD_NAME,
                       "\tRTM6 Context Not Available for the updating route [%s/%d] "
                       "FAILED!!!\n", Ip6PrintAddr (&pNetIp6RtInfo->Ip6Dst),
                       pNetIp6RtInfo->u1Prefixlen);
        return RTM6_FAILURE;
    }

    if ((MALLOC_IP6_ROUTE_ENTRY (pIp6InvdRtEntry)) == NULL)
    {
        /* Memory allocation to add new route fails. */
        RTM6_TRC_ARG2 (pRtm6Cxt->u4ContextId, RTM6_ALL_FAILURE_TRC |
                       RTM6_OS_RESOURCE_TRC, RTM6_MOD_NAME,
                       "\tMemory Allocation for Route [%s/%d] "
                       "FAILED!!!\n", Ip6PrintAddr (&pNetIp6RtInfo->Ip6Dst),
                       pNetIp6RtInfo->u1Prefixlen);
        return RTM6_FAILURE;
    }

    MEMSET (pIp6InvdRtEntry, 0, sizeof (tIp6RtEntry));
    MEMCPY (&pIp6InvdRtEntry->dst, &pNetIp6RtInfo->Ip6Dst, IP6_ADDR_SIZE);
    MEMCPY (&pIp6InvdRtEntry->nexthop, &pNetIp6RtInfo->NextHop, IP6_ADDR_SIZE);

    pIp6InvdRtEntry->u1Prefixlen = pNetIp6RtInfo->u1Prefixlen;
    pIp6InvdRtEntry->u4Index = pNetIp6RtInfo->u4Index;
    pIp6InvdRtEntry->u4Metric = pNetIp6RtInfo->u4Metric;
    pIp6InvdRtEntry->u4RouteTag = pNetIp6RtInfo->u4RouteTag;
    pIp6InvdRtEntry->i1Proto = pNetIp6RtInfo->i1Proto;
    pIp6InvdRtEntry->i1Type = pNetIp6RtInfo->i1Type;
    pIp6InvdRtEntry->i1DefRtrFlag = pNetIp6RtInfo->i1DefRtrFlag;
    pIp6InvdRtEntry->u4RowStatus = pNetIp6RtInfo->u4RowStatus;
    pIp6InvdRtEntry->u1Preference = pNetIp6RtInfo->u1Preference;
    pIp6InvdRtEntry->u1MetricType = pNetIp6RtInfo->u1MetricType;
    pIp6InvdRtEntry->u1AddrType = pNetIp6RtInfo->u1AddrType;

    TMO_SLL_Scan (&(pRtm6Cxt->Rtm6InvdRtList), pTmpIp6RtEntry, tIp6RtEntry *)
    {
        if (pTmpIp6RtEntry != NULL)
        {
            MEMCPY (&TmpIp6RtInfo.Ip6Dst, &pTmpIp6RtEntry->dst,
                    sizeof (tIp6Addr));
            MEMCPY (&TmpIp6RtInfo.NextHop, &pTmpIp6RtEntry->nexthop,
                    sizeof (tIp6Addr));
            TmpIp6RtInfo.u1Prefixlen = pTmpIp6RtEntry->u1Prefixlen;

            if (Rtm6UtilCheckIsRouteGreater (&TmpIp6RtInfo, pNetIp6RtInfo) ==
                RTM6_SUCCESS)
            {
                break;
            }
            else
            {
                pPrevIp6RtInfo = pTmpIp6RtEntry;
            }
        }
    }
    if (pPrevIp6RtInfo == NULL)
    {
        /* Node to be inserted in the first position */
        TMO_SLL_Insert (&(pRtm6Cxt->Rtm6InvdRtList), NULL,
                        (tTMO_SLL_NODE *) & (pIp6InvdRtEntry->NextEntry));
    }
    else
    {

        TMO_SLL_Insert (&(pRtm6Cxt->Rtm6InvdRtList),
                        (tTMO_SLL_NODE *) & (pPrevIp6RtInfo->
                                             NextEntry),
                        (tTMO_SLL_NODE *) & (pIp6InvdRtEntry->NextEntry));
    }
    return RTM6_SUCCESS;
}
#endif

/*
 ******************************************************************************
 * Function           : Rtm6Ipv6LeakRoute
 * Input(s)           : u1CmdType - Command to ADD | Delete | Modify the Route.
 *                      pNetIp6RtInfo  - Route Information.
 * Output(s)          : None
 * Returns            : RTM6_SUCCESS or RTM6_FAILURE
 * Action             : This function either add/modify/delete the route
 *                      from RTM6 Trie and redistributes
 *                      the route to the registered routing protocols
 ******************************************************************************
 */
INT4
Rtm6Ipv6LeakRoute (UINT1 u1CmdType, tNetIpv6RtInfo * pNetIp6RtInfo)
{
    INT4                i4RetVal = RTM6_FAILURE;
    UINT4               u4EventMask = 0;
    tRtm6RtMsg         *pRtm6Msg = NULL;
    tOsixTaskId         ospf3TskId = 0;
    tOsixTaskId         rip6TskId = 0;
    tOsixTaskId         bgp4TskId = 0;
    tOsixTaskId         isisv6TskId = 0;

    /* The below changes are done for programming route in hardware in
       RTM context instead of protocol context. If route is programmed
       using protocol context, the SDK makes priority inversion of the task
       resulting in the protocol task to run in lower proiority. This results
       in failures during scalability testing as sufficient time is not given
       to the protocol task. This is achieved by using a queue to post
       the route details and handling the same. This will be followed
       when gu1Rtm6RouteCxtFlag is set to OSIX_TRUE */

    if (((gu1Rtm6RouteCxtFlag == OSIX_TRUE) &&
         (pNetIp6RtInfo->i1Proto != IP6_NETMGMT_PROTOID)) ||
        (pNetIp6RtInfo->u4IsPDRoute == OSIX_TRUE))
    {
        if (RTM6_ROUTE_MSG_ENTRY_ALLOC (pRtm6Msg) == NULL)
        {
            UtlTrcLog (1, 1, "",
                       "INFO[RTM6]: Route Entry Message Allocation Failure - Processing the RTM QUEUE Entries\n");
            if (OsixGetTaskId (SELF, (const UINT1 *) "OSV3", &ospf3TskId) ==
                OSIX_FAILURE)
            {
                return RTM6_FAILURE;
            }
            if (OsixGetTaskId (SELF, (const UINT1 *) "RIP6", &rip6TskId) ==
                OSIX_FAILURE)
            {
                return RTM6_FAILURE;
            }
            if (OsixGetTaskId (SELF, (const UINT1 *) "Bgp", &bgp4TskId) ==
                OSIX_FAILURE)
            {
                return RTM6_FAILURE;
            }
            if (OsixGetTaskId (SELF, (const UINT1 *) "ISIS", &isisv6TskId) ==
                OSIX_FAILURE)
            {
                return RTM6_FAILURE;
            }
            if (OsixGetCurTaskId () == isisv6TskId)
            {
                while (1)
                {
                    OsixEvtRecv (isisv6TskId, ISIS_RTM6_ROUTE_PROCESS_EVENT,
                                 (OSIX_WAIT | OSIX_EV_ANY), &u4EventMask);
                    if (MemGetFreeUnits (gRtm6GlobalInfo.Rtm6RtMsgPoolId) > 0)
                    {
                        break;
                    }
                    OsixDelay (1, OSIX_MILLI_SECONDS);
                }
            }
            else if (OsixGetCurTaskId () == ospf3TskId)
            {
                while (1)
                {
                    OsixEvtRecv (ospf3TskId, OSPFV3_RTM6_ROUTE_PROCESS_EVENT,
                                 (OSIX_WAIT | OSIX_EV_ANY), &u4EventMask);
                    if (MemGetFreeUnits (gRtm6GlobalInfo.Rtm6RtMsgPoolId) > 0)
                    {
                        break;
                    }
                    OsixDelay (1, OSIX_MILLI_SECONDS);
                }
            }
            else if (OsixGetCurTaskId () == rip6TskId)
            {
                while (1)
                {
                    OsixEvtRecv (rip6TskId, RIP6_RTM6_ROUTE_PROCESS_EVENT,
                                 (OSIX_WAIT | OSIX_EV_ANY), &u4EventMask);
                    if (MemGetFreeUnits (gRtm6GlobalInfo.Rtm6RtMsgPoolId) > 0)
                    {
                        break;
                    }
                    OsixDelay (1, OSIX_MILLI_SECONDS);
                }
            }
            else if (OsixGetCurTaskId () == bgp4TskId)
            {
                while (1)
                {
                    OsixEvtRecv (bgp4TskId, BGP4_RTM6_ROUTE_PROCESS_EVENT,
                                 (OSIX_WAIT | OSIX_EV_ANY), &u4EventMask);
                    if (MemGetFreeUnits (gRtm6GlobalInfo.Rtm6RtMsgPoolId) > 0)
                    {
                        break;
                    }
                    OsixDelay (1, OSIX_MILLI_SECONDS);
                }
            }
            if (RTM6_ROUTE_MSG_ENTRY_ALLOC (pRtm6Msg) == NULL)
            {
                UtlTrcLog (1, 1, "",
                           "ERROR[RTM6]: Route Entry Message Allocation Failure\n");
                return IP_FAILURE;
            }
        }
        pRtm6Msg->u1MsgType = u1CmdType;

        if (MEMCPY (&(pRtm6Msg->unRtm6Msg.RtInfo), pNetIp6RtInfo,
                    sizeof (tNetIpv6RtInfo)) == NULL)
        {
            RTM6_ROUTE_MSG_FREE (pRtm6Msg);
            UtlTrcLog (1, 1, "",
                       "ERROR[RTM6]: Route Entry Message Creation Failure \n");
            return RTM6_FAILURE;
        }
        /* Send the buffer to RTM6 */
        if (OsixSendToQ (SELF,
                         (const UINT1 *) RTM6_RT_MSG_Q_NAME,
                         (tOsixMsg *) (VOID *) pRtm6Msg,
                         OSIX_MSG_NORMAL) != OSIX_SUCCESS)
        {
            RTM6_ROUTE_MSG_FREE (pRtm6Msg);
            return (RTM6_FAILURE);
        }

        if (OsixSendEvent (SELF, (const UINT1 *) RTM6_MAIN_TASK_NAME,
                           RTM6_ROUTE_MSG_EVENT) == OSIX_FAILURE)
        {
            return RTM6_FAILURE;
        }

        return RTM6_SUCCESS;

    }
    else
    {
        RTM6_TASK_LOCK ();
        i4RetVal = Rtm6UtilIpv6LeakRoute (u1CmdType, pNetIp6RtInfo);
        RTM6_TASK_UNLOCK ();
    }

    return i4RetVal;
}

/*
 ******************************************************************************
 * Function           : Rtm6UtilIpv6LeakRoute 
 * Input(s)           : u1CmdType - Command to ADD | Delete | Modify the Route.
 *                      pNetIp6RtInfo  - Route Information.
 * Output(s)          : None
 * Returns            : RTM6_SUCCESS or RTM6_FAILURE
 * Action             : This function either add/modify/delete the route
 *                      from RTM6 Trie and redistributes
 *                      the route to the registered routing protocols
 ******************************************************************************
 */
INT4
Rtm6UtilIpv6LeakRoute (UINT1 u1CmdType, tNetIpv6RtInfo * pNetIp6RtInfo)
{
    tIp6RtEntry        *pIp6RtEntry;
    tIp6RtEntry         Ip6RtEntry;
    tRtm6Cxt           *pRtm6Cxt = NULL;
    tRtm6FrtInfo       *pRtm6FrtNode = NULL;
    INT4                i4RetVal = RTM6_FAILURE;

    /* This is an RTM6 util function. To invoke LeakRoute from other tasks
     * use the function Rtm6ApiIpv6LeakRoute */

    pRtm6Cxt = UtilRtm6GetCxt (pNetIp6RtInfo->u4ContextId);

    if (pRtm6Cxt == NULL)
    {
        /* Memory allocation to add new route fails. */
        RTM6_TRC_ARG2 (RTM6_INVALID_CXT_ID, RTM6_ALL_FAILURE_TRC |
                       RTM6_OS_RESOURCE_TRC, RTM6_MOD_NAME,
                       "\tRTM6 Context Not Available for the updating route [%s/%d] "
                       "FAILED!!!\n", Ip6PrintAddr (&pNetIp6RtInfo->Ip6Dst),
                       pNetIp6RtInfo->u1Prefixlen);
        return RTM6_FAILURE;
    }

    if (u1CmdType == RTM6_DELETE_ROUTE)
    {
        pIp6RtEntry = &Ip6RtEntry;
    }
    else
    {
        if ((MALLOC_IP6_ROUTE_ENTRY (pIp6RtEntry)) == NULL)
        {
            /* Memory allocation to add new route fails. */
            RTM6_TRC_ARG2 (pRtm6Cxt->u4ContextId, RTM6_ALL_FAILURE_TRC |
                           RTM6_OS_RESOURCE_TRC, RTM6_MOD_NAME,
                           "\tMemory Allocation for Route [%s/%d] "
                           "FAILED!!!\n", Ip6PrintAddr (&pNetIp6RtInfo->Ip6Dst),
                           pNetIp6RtInfo->u1Prefixlen);
            return RTM6_FAILURE;
        }
    }

    MEMSET (pIp6RtEntry, 0, sizeof (tIp6RtEntry));
    MEMCPY (&pIp6RtEntry->dst, &pNetIp6RtInfo->Ip6Dst, IP6_ADDR_SIZE);
    pIp6RtEntry->u1Prefixlen = pNetIp6RtInfo->u1Prefixlen;
    MEMCPY (&pIp6RtEntry->nexthop, &pNetIp6RtInfo->NextHop, IP6_ADDR_SIZE);
    pIp6RtEntry->u4Index = pNetIp6RtInfo->u4Index;
    pIp6RtEntry->u4Metric = pNetIp6RtInfo->u4Metric;
    pIp6RtEntry->u4RouteTag = pNetIp6RtInfo->u4RouteTag;
    pIp6RtEntry->i1Proto = pNetIp6RtInfo->i1Proto;
    pIp6RtEntry->i1Type = pNetIp6RtInfo->i1Type;
    pIp6RtEntry->i1DefRtrFlag = pNetIp6RtInfo->i1DefRtrFlag;
    pIp6RtEntry->u4RowStatus = pNetIp6RtInfo->u4RowStatus;
    pIp6RtEntry->u1Preference = pNetIp6RtInfo->u1Preference;
    pIp6RtEntry->u1MetricType = pNetIp6RtInfo->u1MetricType;
    pIp6RtEntry->u1AddrType = pNetIp6RtInfo->u1AddrType;
    pIp6RtEntry->u1NullFlag = pNetIp6RtInfo->u1NullFlag;
    if (pNetIp6RtInfo->u2ChgBit & IP6_BIT_GR)
    {
        pIp6RtEntry->u1Flag = pIp6RtEntry->u1Flag | RTM6_GR_RT;
    }
    if (pNetIp6RtInfo->i1MetricType5 == 0x0)
    {
        /* set to default value -1 */
        pIp6RtEntry->i1MetricType5 = -1;
    }
    else
    {
        pIp6RtEntry->i1MetricType5 = pNetIp6RtInfo->i1MetricType5;

    }
    switch (u1CmdType)
    {
        case RTM6_ADD_ROUTE:
        {
            i4RetVal = Ip6ForwardingTableAddRouteInCxt (pRtm6Cxt, pIp6RtEntry);
            break;
        }
        case RTM6_DELETE_ROUTE:
        {
            i4RetVal =
                Ip6ForwardingTableDeleteRouteInCxt (pRtm6Cxt, pIp6RtEntry);
            /* Delete the route entry if entry present
             * in NPAPI failed route entry table*/
            if (i4RetVal == RTM6_SUCCESS)
            {
                pRtm6FrtNode =
                    Rtm6FrtGetInfo (pIp6RtEntry, pRtm6Cxt->u4ContextId);
                if (pRtm6FrtNode != NULL)
                {
                    if (Rtm6FrtDeleteEntry (pRtm6FrtNode) == RTM6_SUCCESS)
                    {
                        Rtm6RedSyncFrtInfo (&Ip6RtEntry, pRtm6Cxt->u4ContextId,
                                            pRtm6FrtNode->u1Type,
                                            RTM6_DELETE_ROUTE);
                    }
                }
            }
            break;
        }
        case RTM6_MODIFY_ROUTE:
        {
            i4RetVal =
                Ip6ForwardingTableModifyRouteInCxt (pRtm6Cxt, pIp6RtEntry);
            break;
        }
        default:
            i4RetVal = RTM6_FAILURE;
    }

    if ((i4RetVal == RTM6_FAILURE) && (u1CmdType != RTM6_DELETE_ROUTE))
    {
        /* Free the memory allocated for the route structure. */
        IP6_RT_FREE (pIp6RtEntry);
    }
    return (i4RetVal);
}

/*
 *******************************************************************************
 *  Function            :   Ip6GetFwdTableRouteNumInCxt
 *  Description         :   This function returns the number of route present
 *                          in the IP6 Fwd Table
 *  Input(s)            :   u4contextId - RTM6 Context Id                             
 *  Output parameters   :   pu4Ip6FwdTblRouteNum - number of routes in IP6 Fwd
 *                                                 table.
 *  Global variables
 *  Affected            :   None. 
 *  Return value        :   None.
 ******************************************************************************
*/
VOID
Ip6GetFwdTableRouteNumInCxt (UINT4 u4ContextId, UINT4 *pu4Ip6FwdTblRouteNum)
{
    tRtm6Cxt           *pRtm6Cxt = NULL;

    RTM6_TASK_LOCK ();

    pRtm6Cxt = UtilRtm6GetCxt (u4ContextId);
    if (pRtm6Cxt != NULL)
    {
        *pu4Ip6FwdTblRouteNum = pRtm6Cxt->u4Ip6FwdTblRouteNum;
    }

    RTM6_TASK_UNLOCK ();
    return;
}

/*-------------------------------------------------------------------+
 *
 * Function           : Rtm6Register
 *
 * Input(s)           : pRegnId - Registration Info about the protocol
 *                                registering with RTM6
 *                      u1BitMask   - Value indicating whether route needs to
 *                                    be redistributed or not.
 *                      SendToApplication - Callback Function for handling
 *                                          message from RTM6. 
 *
 * Output(s)          : None
 *
 * Returns            : RTM6_SUCCESS - If the registration is successful
 *                      else other failure values.
 *
 * Action             : This function handles the registration by the routing 
 *                      protocols. Sends acknowledgement when the 
 *                      registration is successful. 
 *         
+-------------------------------------------------------------------*/
INT4
Rtm6Register (tRtm6RegnId * pRegnId,
              UINT1 u1BitMask,
              INT4 (*SendToApplication) (tRtm6RespInfo * pRespInfo,
                                         tRtm6MsgHdr * Rtm6Header))
{
    INT4                i4RetVal = 0;

    i4RetVal = Rtm6ApiHandleProtocolStatusInCxt (pRegnId->u4ContextId,
                                                 (UINT1) pRegnId->u2ProtoId,
                                                 RTM6_REGISTRATION_MESSAGE, 0,
                                                 u1BitMask,
                                                 (VOID *) SendToApplication, 0);

    return i4RetVal;
}

/*-------------------------------------------------------------------+
 *
 * Function           : Rtm6Deregister
 *
 * Input(s)           : pRegnId - DeRegistraion Info of the routing protocol
 *                      which sent this DeRegistration message.
 *
 * Output(s)          : Updation of Registration table 
 *
 * Returns            : RTM6_SUCCESS/RTM6_FAILURE
 *
 * Action             : This function handles the DeRegistration message  
 *                      from the routing protocols.
 * 
+-------------------------------------------------------------------*/
INT4
Rtm6Deregister (tRtm6RegnId * pRegnId)
{
    Rtm6ApiHandleProtocolStatusInCxt (pRegnId->u4ContextId,
                                      (UINT1) pRegnId->u2ProtoId,
                                      RTM6_DEREGISTRATION_MESSAGE, 0, 0, NULL,
                                      0);
    return RTM6_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : Does the route lookup in the RTM6 routing table based on 
 *               the passed preferences. This API takes RTM6 protocol lock, 
 *               calls RTM6 Trie and releases the lock.
 *
 * INPUTS      :  u4ContextId - RTM6 Context ID
 *             :  pAddr - Dest for the IPv6 route
 *                      u1Prefixlen - Prefix length for the route destination
 *                      u1Preference - Route preference
 * Output(s)          : pNetIpv6RtInfo - Route information is returned
 *                      pu1FoundFlag - flag to indicate the route is found or not
 * RETURNS     : returns RTM6_SUCCESS, if route found , else returns RTM6_FAILURE
 *
 * NOTES       : The modules which are calling this API, not required to take 
 *               RTM6 protocol lock.
 ******************************************************************************/
INT4
Rtm6ApiRoute6LookupInCxt (UINT4 u4ContextId,
                          tIp6Addr * pAddr, UINT1 u1PrefixLen,
                          UINT1 u1Preference, UINT1 *pu1FoundFlag,
                          tNetIpv6RtInfo * pNetIpv6RtInfo)
{
    INT4                i4Status = RTM6_FAILURE;
    tRtm6Cxt           *pRtm6Cxt = NULL;

    RTM6_TASK_LOCK ();
    pRtm6Cxt = UtilRtm6GetCxt (u4ContextId);
    if (pRtm6Cxt == NULL)
    {
        RTM6_TASK_UNLOCK ();
        return i4Status;
    }
    i4Status = Rtm6UtilRoute6LookupInCxt (pRtm6Cxt, pAddr, u1PrefixLen,
                                          u1Preference, pu1FoundFlag,
                                          pNetIpv6RtInfo);
    RTM6_TASK_UNLOCK ();
    return i4Status;
}

/******************************************************************************
* DESCRIPTION : This function scans throught the entire Route Table,
*               checks any route present for the given interface and
*               posts an event to the registered routing protocols for
*               the route change.
*                                                                         .
*
* INPUTS      : u1Event, u4Index
*
* OUTPUTS     : None
*
* RETURNS     : RTM6_SUCCESS | RTM6_FAILURE
*
* NOTES       :  
******************************************************************************/

INT4
Rtm6ApiActOnIpv6IfChange (UINT1 u1Event, UINT4 u4Index)
{
    tInputParams        InParams;
    tRtm6TrieScanOutParams OutParams;
    tRtm6Cxt           *pRtm6Cxt = NULL;
#ifdef LNXIP6_WANTED
    tLip6If            *pIf6 = NULL;
    tLip6AddrNode      *pAddr6Info = NULL;
#endif
    UINT4               u4ContextId = 0;

    MEMSET (&InParams, 0, sizeof (tInputParams));
    MEMSET (&OutParams, 0, sizeof (tRtm6TrieScanOutParams));

    /* Need to scan all the route and delete all the route
     * learnt over the given interface. */
    if (Ip6GetCxtId (u4Index, &u4ContextId) == IP6_FAILURE)
    {
        return RTM6_FAILURE;
    }
    RTM6_TASK_LOCK ();

    pRtm6Cxt = UtilRtm6GetCxt (u4ContextId);
    if (pRtm6Cxt == NULL)
    {
        RTM6_TASK_UNLOCK ();
        return RTM6_FAILURE;
    }

    OutParams.u4ScanEvent = u1Event;
    OutParams.u4IfIndex = u4Index;
    OutParams.u4ContextId = u4ContextId;
    Ip6TrieScan (&InParams, Rtm6UtilCbHandleIfStatusChange, &OutParams);

    RTM6_TASK_UNLOCK ();
#if LNXIP6_WANTED
    if (u1Event == RTM6_SCAN_INTF_UP)
    {
        pIf6 = Lip6UtlGetIfEntry (u4Index);
        if (pIf6 == NULL)
        {
            return RTM6_FAILURE;
        }

        TMO_SLL_Scan (&pIf6->Ip6AddrList, pAddr6Info, tLip6AddrNode *)
        {
            Rtm6UpdateInvdRouteList (u4Index, &(pAddr6Info->Ip6Addr),
                                     pAddr6Info->i4PrefixLen,
                                     NETIPV6_ADD_ROUTE);
        }
    }
#endif
    return RTM6_SUCCESS;

}

/******************************************************************************
 * DESCRIPTION : Purges routes dynamic routes , learnt from i1Proto.
 *
 * INPUTS      : u4ContextId - RTM6 Context ID
 *             : i1Proto - protocol Id
 *
 * OUTPUTS     : None
 *
 * RETURNS     : None
 *
 * NOTES       :
 ******************************************************************************/

VOID
Rtm6ApiPurgeDynamicInCxt (UINT4 u4ContextId, INT1 i1Proto)
{
    tInputParams        InParams;
    tRtm6TrieScanOutParams OutParams;

    MEMSET (&InParams, 0, sizeof (tInputParams));
    MEMSET (&OutParams, 0, sizeof (tRtm6TrieScanOutParams));

    /* Need to scan all the route and delete all the route
     * learnt over the given interface. */
    OutParams.u4ScanEvent = RTM6_SCAN_PROTO_DEL;
    OutParams.i1ProtoId = i1Proto;
    OutParams.u4ContextId = u4ContextId;

    RTM6_TASK_LOCK ();

    Ip6TrieScan (&InParams, Rtm6HandleProtoDown, &OutParams);

    RTM6_TASK_UNLOCK ();
    return;
}

/******************************************************************************
 * DESCRIPTION : This function provides the Route (either Exact Route or
 *               Best route) for a given destination and Mask based on
 *               the incoming request.
 *
 * INPUTS      : pNetIpv6RtQuery - Infomation about the route to be
 *                                 retrieved.
 *
 * OUTPUTS     : pNetIpv6RtInfo - Information about the requested route. 
 *
 * RETURNS     : RTM6_SUCCESS/RTM6_FAILURE
 *
 * NOTES       :
 ******************************************************************************/
INT4
Rtm6ApiNetIpv6GetRoute (tNetIpv6RtInfoQueryMsg * pNetIpv6RtQuery,
                        tNetIpv6RtInfo * pNetIpv6RtInfo)
{

    RTM6_TASK_LOCK ();

    if (Rtm6NetIpv6GetRoute (pNetIpv6RtQuery, pNetIpv6RtInfo) == RTM6_FAILURE)
    {
        RTM6_TASK_UNLOCK ();
        return RTM6_FAILURE;
    }

    RTM6_TASK_UNLOCK ();
    return RTM6_SUCCESS;
}

#ifdef NPAPI_WANTED

/******************************************************************************
 * DESCRIPTION : This function scan Ipv6 routing Table to add 
 *               route with specified Next Hop IP addres till 
 *               the H/W table is Full or No Route to add.    
 *
 * INPUTS      : u4ContextId - RTM6 Context ID
 *             : pNextHopIpAddr-- Infomation about the route to be
 *                                 retrieved.
 *
 * OUTPUTS     : None. 
 *
 * RETURNS     : None
 *
 * NOTES       :
 ******************************************************************************/
VOID
Rtm6ApiAddAllRtWithNxtHopInHWInCxt (UINT4 u4ContextId,
                                    tIp6Addr * pNextHopIpAddr)
{
    tIp6RtEntry        *pIp6Route = NULL, *pRtEntry = NULL;
    tIp6RtEntry        *pNextProtoRt = NULL;
    tIp6RtEntry        *pNextRt = NULL;
    tIp6RtEntry        *pTmpIp6Route = NULL;
    tIp6RtEntry        *pIp6BestRoute = NULL;
    VOID               *pRibNode = NULL;
    tRtm6Cxt           *pRtm6Cxt = NULL;
    tFsNpIntInfo        IntInfo;
    tTnlIfEntry         TnlIfEntry;
    tFsNpRouteInfo      RouteInfo;
#ifdef MPLS_IPV6_WANTED
    tGenU4Addr          DestNet;

    MEMSET (&DestNet, 0, sizeof (tGenU4Addr));
#endif
    UINT4               u4RetValue = SNMP_SUCCESS;
    UINT1               u1IsBestRoute = IP6_ZERO;

    RTM6_TASK_LOCK ();
    pRtm6Cxt = UtilRtm6GetCxt (u4ContextId);
    if (pRtm6Cxt == NULL)
    {

        RTM6_TASK_UNLOCK ();
        return;
    }

    if (Rtm6TrieGetFirstEntryInCxt (pRtm6Cxt,
                                    &pIp6Route, &pRibNode) == IP6_FAILURE)
    {
        RTM6_TASK_UNLOCK ();
        return;
    }
    do
    {
#ifdef MPLS_IPV6_WANTED
        MEMCPY (&(DestNet.Addr.Ip6Addr), &(pIp6Route->dst), sizeof (tIp6Addr));;
        DestNet.u2AddrType = MPLS_IPV6_ADDR_TYPE;
        if (MplsIsFtnExist (&DestNet, (UINT4) (pIp6Route->u1Prefixlen)) == TRUE)
        {
            if (Rtm6TrieGetNextEntryInCxt (pRtm6Cxt,
                                           pIp6Route, &pNextRt,
                                           &pRibNode) == RTM6_FAILURE)
            {
                /* No more route present. Set the cookie to indicate it */
                break;
            }
            pIp6Route = pNextRt;
            pNextRt = NULL;
            continue;
        }
#endif
        pNextProtoRt = pIp6Route->pNextRtEntry;
        pRtEntry = pIp6Route;
        while (pIp6Route != NULL)
        {
            /* Check for next hop */
            if (Ip6AddrMatch (pNextHopIpAddr, &pIp6Route->nexthop,
                              IP6_ADDR_SIZE_IN_BITS))
            {
                MEMSET (&IntInfo, 0, sizeof (tFsNpIntInfo));
                MEMSET (&TnlIfEntry, 0, sizeof (tTnlIfEntry));
                u4RetValue = (UINT4) Ip6GetIfType (pIp6Route->u4Index);
                if (u4RetValue != SNMP_FAILURE)
                {
                    IntInfo.u1IfType = Ip6GetIfType (pIp6Route->u4Index);
                }
                else
                {
                    if (Rtm6TrieGetNextEntryInCxt (pRtm6Cxt,
                                                   pIp6Route, &pNextRt,
                                                   &pRibNode) == SNMP_FAILURE)
                    {
                        break;
                    }
                    pIp6Route = pNextRt;
                    pNextRt = NULL;
                    continue;
                }
                if ((IntInfo.u1IfType == IP6_L3VLAN_INTERFACE_TYPE) ||
                    (IntInfo.u1IfType == IP6_LAGG_INTERFACE_TYPE) ||
                    (IntInfo.u1IfType == IP6_PSEUDO_WIRE_INTERFACE_TYPE) ||
                    (IntInfo.u1IfType == IP6_L3SUB_INTF_TYPE))
                {
                    IntInfo.u4PhyIfIndex = pIp6Route->u4Index;
                }
#ifdef TUNNEL_WANTED
                else if (IntInfo.u1IfType == IP6_TUNNEL_INTERFACE_TYPE)
                {
                    /* fetch the Tunnel interface's Physical Interface index */
                    if (CfaGetTnlEntryFromIfIndex (pIp6Route->u4Index,
                                                   &TnlIfEntry) == CFA_FAILURE)
                    {
                        RTM6_TASK_UNLOCK ();
                        return;
                    }
                    IntInfo.u4PhyIfIndex = TnlIfEntry.u4PhyIfIndex;

                    IntInfo.TunlInfo.tunlSrc.u4_addr[IPVX_THREE] =
                        TnlIfEntry.LocalAddr.Ip4TnlAddr;
                    IntInfo.TunlInfo.tunlDst.u4_addr[IPVX_THREE] =
                        TnlIfEntry.RemoteAddr.Ip4TnlAddr;
                    IntInfo.TunlInfo.u1TunlType = TnlIfEntry.u4EncapsMethod;
                    IntInfo.TunlInfo.u1TunlFlag = TnlIfEntry.u1DirFlag;
                    IntInfo.TunlInfo.u1TunlDir = TnlIfEntry.u1Direction;

                }
#endif
                if (IntInfo.u1IfType != IP6_ENET_INTERFACE_TYPE)
                {
                    if (CfaGetVlanId
                        (IntInfo.u4PhyIfIndex,
                         &(IntInfo.u2VlanId)) == CFA_FAILURE)
                    {
                        RTM6_TASK_UNLOCK ();
                        return;
                    }
                }
                else
                {
                    IntInfo.u4PhyIfIndex = pIp6Route->u4Index;
                    IntInfo.u2VlanId = 0;
                }
                if (RTM6_GET_NODE_STATUS () == RM_ACTIVE)
                {
                    pIp6Route->u1HwStatus = NP_NOT_PRESENT;
                    Rtm6RedAddDynamicInfo (pIp6Route, pRtm6Cxt->u4ContextId);
                    Rtm6RedSendDynamicInfo ();
                }
                if (RTM6_IS_NP_PROGRAMMING_ALLOWED () == RTM6_SUCCESS)
                {
                    /*Passing ECMP count to HW */
                    MEMSET (&RouteInfo, 0, sizeof (tFsNpRouteInfo));
                    Rtm6SetReachState (u4ContextId, pRtEntry);
                    Rtm6GetInstalledRouteCount (pRtEntry,
                                                &pRtEntry->u1EcmpCount);
                    IntInfo.u1RouteCount = pRtEntry->u1EcmpCount;
                    RouteInfo.u1RouteCount = pRtEntry->u1EcmpCount;

                    /* program the best route in Hw and skip all toher routes */
                    Ip6GetBestRouteEntryInCxt (pRtm6Cxt,
                                               &(pIp6Route->dst),
                                               pIp6Route->u1Prefixlen,
                                               &pIp6BestRoute);
                    u1IsBestRoute = IP6_ZERO;
                    if (pIp6BestRoute != NULL)
                    {
                        pTmpIp6Route = pIp6BestRoute;
                        while (pTmpIp6Route != NULL)
                        {
                            if ((pIp6Route == pTmpIp6Route))
                            {
                                u1IsBestRoute = 1;
                                break;
                            }
                            pTmpIp6Route = pTmpIp6Route->pNextAlternatepath;
                        }
                    }

                    if (u1IsBestRoute == 1)
                    {
                        /* if route is connected route then skip it, because nexthop
                         * will not be present for connected routes
                         */
                        if ((pIp6Route->i1Proto != IP6_LOCAL_PROTOID) &&
                            (pIp6Route->i1Type != IP6_ROUTE_TYPE_DIRECT))
                        {

                            if (RouteInfo.u1RouteCount == 1)
                            {

                                /* delete the local route and add normal route */
                                /*The route is installed as local route in the Data plane
                                 * we are removing the local route from the data plane and
                                 * programming  reachable route in dataplane */
                                RouteInfo.u1NHType = NH_DIRECT;
                                Ipv6FsNpIpv6UcRouteDelete (pRtm6Cxt->
                                                           u4ContextId,
                                                           (UINT1 *)
                                                           &pIp6Route->dst,
                                                           pIp6Route->
                                                           u1Prefixlen,
                                                           (UINT1 *)
                                                           &pIp6Route->nexthop,
                                                           pIp6Route->u4Index,
                                                           &RouteInfo);

                                /* delete only when DROP ROUTE is present in Hardware */
                                RouteInfo.u1NHType = NH_DISCARD;
                                Ipv6FsNpIpv6UcRouteDelete (pRtm6Cxt->
                                                           u4ContextId,
                                                           (UINT1 *)
                                                           &pIp6Route->dst,
                                                           pIp6Route->
                                                           u1Prefixlen,
                                                           (UINT1 *)
                                                           &pIp6Route->nexthop,
                                                           pIp6Route->u4Index,
                                                           &RouteInfo);

                            }
                            if (Ipv6FsNpIpv6UcRouteAdd (u4ContextId,
                                                        (UINT1 *) &pIp6Route->
                                                        dst,
                                                        pIp6Route->u1Prefixlen,
                                                        (UINT1 *) &pIp6Route->
                                                        nexthop,
                                                        NP_IPV6_NH_REACHABLE,
                                                        &IntInfo) ==
                                FNP_FAILURE)
                            {
                                RTM6_TRC (u4ContextId, RTM6_ALL_FAILURE_TRC |
                                          RTM6_OS_RESOURCE_TRC, RTM6_MOD_NAME,
                                          "\tNP Route Addition Failed \n");
                                if (Rtm6FrtAddInfo
                                    (pIp6Route, u4ContextId,
                                     (UINT1) NP_IPV6_NH_REACHABLE) ==
                                    RTM6_SUCCESS)
                                {
                                    Rtm6RedSyncFrtInfo (pIp6Route, u4ContextId,
                                                        (UINT1)
                                                        NP_IPV6_NH_REACHABLE,
                                                        RTM6_ADD_ROUTE);

                                }
                                else
                                {
                                    RTM6_TRC_ARG2 (pRtm6Cxt->u4ContextId,
                                                   RTM6_ALL_FAILURE_TRC |
                                                   RTM6_OS_RESOURCE_TRC,
                                                   RTM6_MOD_NAME,
                                                   "ERROR[RTM]: IPV6  Failed Route Addition Failed%s/%d   \n",
                                                   Ip6PrintNtop (&pIp6Route->
                                                                 dst),
                                                   (INT4) pIp6Route->
                                                   u1Prefixlen);
                                }
                            }
                            pIp6Route->u4HwIntfId[0] = IntInfo.u4HwIntfId[0];
                        }
                    }
                    if (RTM6_GET_NODE_STATUS () == RM_ACTIVE)
                    {
                        pIp6Route->u1HwStatus = NP_PRESENT;
                        Rtm6RedAddDynamicInfo (pIp6Route,
                                               pRtm6Cxt->u4ContextId);
                        Rtm6RedSendDynamicInfo ();
                    }
                }
            }
            /* Need to Display all the route. */
            if (pIp6Route->pNextAlternatepath != NULL)
            {
                pIp6Route = pIp6Route->pNextAlternatepath;
            }
            else
            {
                if (pNextProtoRt != NULL)
                {
                    pIp6Route = pNextProtoRt;
                    pNextProtoRt = pIp6Route->pNextRtEntry;
                }
                else
                {
                    /* No more route for this prefix. */
                    break;
                }
            }
        }

        if (Rtm6TrieGetNextEntryInCxt (pRtm6Cxt,
                                       pIp6Route, &pNextRt,
                                       &pRibNode) == RTM6_FAILURE)
        {
            /* No more route present. Set the cookie to indicate it */
            break;
        }
        pIp6Route = pNextRt;
        pNextRt = NULL;
    }
    while (pIp6Route != NULL);

    RTM6_TASK_UNLOCK ();
    return;
}
#endif

/******************************************************************************
 * DESCRIPTION : This function scan Ipv6 routing Table to add 
 *               route with specified Next Hop IP addres 
 *
 * INPUTS      : u4ContextId - RTM6 Context ID
 *             : pNextHopIpAddr-- Infomation about the route to be
 *                                 retrieved.
 *
 * OUTPUTS     : None. 
 *
 * RETURNS     : None
 *
 * NOTES       :
 ******************************************************************************/
VOID
Rtm6ApiAddAllRtWithNxtHopInHWInLnx (UINT4 u4ContextId,
                                    tIp6Addr * pNextHopIpAddr)
{
    tIp6RtEntry        *pIp6Route = NULL;
    tIp6RtEntry        *pIp6BestRoute = NULL;
    tIp6RtEntry        *pIp6TmpRoute = NULL;
    tIp6RtEntry        *pTempRoute = NULL;
    tIp6RtEntry        *pNextRt = NULL;
    INT4                i4Reachable = 0;
    VOID               *pRibNode = NULL;
    tRtm6Cxt           *pRtm6Cxt = NULL;
    tNetIpv6RtInfo      NetIpv6RtInfo;

    RTM6_TASK_LOCK ();
    pRtm6Cxt = UtilRtm6GetCxt (u4ContextId);
    if (pRtm6Cxt == NULL)
    {

        RTM6_TASK_UNLOCK ();
        return;
    }

    if (Rtm6TrieGetFirstEntryInCxt (pRtm6Cxt,
                                    &pIp6Route, &pRibNode) == IP6_FAILURE)
    {
        RTM6_TASK_UNLOCK ();
        return;
    }

/*Get the best route. And if there are ECMP routes
 * matching the next hop, and if it is not already 
 * reachable, program the routes in the kernel.
 */

    do
    {

        if (Rtm6UtilFwdTblGetBestRouteInCxt
            (pRtm6Cxt, pIp6Route, &pIp6BestRoute) == IP_SUCCESS)
        {
            pIp6TmpRoute = pIp6BestRoute;
            while (pIp6TmpRoute != NULL)
            {

                /* Check for next hop */
                if (Ip6AddrMatch (pNextHopIpAddr, &pIp6TmpRoute->nexthop,
                                  IP6_ADDR_SIZE_IN_BITS))
                {
                    /*Route Matched with reachable next hop
                     * Need to install this in kernel.
                     */

                    /* program route in kernel. */

                    if (((pIp6TmpRoute->u4Flag & RTM6_ECMP_RT) == RTM6_ECMP_RT)
                        && (pIp6TmpRoute->u4RowStatus == IP6FWD_ACTIVE))
                    {

                        if (pIp6TmpRoute != pIp6BestRoute)
                        {
                            RTM6_COPY_ECMP6_ROUTE_INFO_TO_UPDATE_MSG
                                (pIp6TmpRoute, NetIpv6RtInfo);

                            NetIpv6RtInfo.u4ContextId = pRtm6Cxt->u4ContextId;
                            NetIpv6RtInfo.u4RowStatus = IP6FWD_ACTIVE;
                            NetIpv6InvokeRouteChangeForEcmp (&NetIpv6RtInfo,
                                                             NETIPV6_ALL_PROTO);
                        }
                        else
                        {
                            RTM6_COPY_ECMP6_ROUTE_INFO_TO_UPDATE_MSG
                                (pIp6TmpRoute, NetIpv6RtInfo);

                            NetIpv6RtInfo.u4ContextId = pRtm6Cxt->u4ContextId;
                            NetIpv6RtInfo.u4RowStatus = IP6FWD_ACTIVE;
                            NetIpv6InvokeRouteChange (&NetIpv6RtInfo,
                                                      NETIPV6_ALL_PROTO);

                        }

                        Rtm6DeleteRtFromECMP6PRT (pRtm6Cxt, pIp6TmpRoute);
                    }
                    pIp6TmpRoute->u4Flag |= (UINT4) RTM6_RT_REACHABLE;

                    pIp6TmpRoute = pIp6TmpRoute->pNextAlternatepath;
                    if (pIp6TmpRoute == NULL)
                    {

                        pTempRoute = pIp6BestRoute;
                        i4Reachable = 0;
                        while (pTempRoute != NULL)
                        {
                            if ((pTempRoute->u4Flag & RTM6_RT_REACHABLE) ==
                                RTM6_RT_REACHABLE)
                            {
                                i4Reachable++;

                            }
                            pTempRoute = pTempRoute->pNextAlternatepath;
                        }

                        if ((pIp6BestRoute->u4Flag & RTM6_ECMP_RT) !=
                            RTM6_ECMP_RT)
                        {
                            /* Not an ECMP route hence deleting the route from PRT lists */
                            Rtm6DeleteRtFromECMP6PRT (pRtm6Cxt, pIp6BestRoute);
                            Rtm6DeleteRtFromPRTInCxt (pRtm6Cxt, pIp6BestRoute);
                        }

                        /* Once the reachable route count is 0, then we need to add Best route in NP */
                        if ((i4Reachable == 0) &&
                            ((pIp6BestRoute->u4Flag & RTM6_ECMP_RT) ==
                             RTM6_ECMP_RT))
                        {

                            Rtm6ApiHandleEcmpRtInNp (pRtm6Cxt->u4ContextId,
                                                     pIp6BestRoute,
                                                     IP6_ROUTE_ADD);
                            Rtm6DeleteRtFromECMP6PRT (pRtm6Cxt, pIp6BestRoute);
                        }
                        else
                        {
                            /*Add not reachable routes to PRT table */

                            pTempRoute = pIp6BestRoute;
                            while (pTempRoute != NULL)
                            {

                                if (((pTempRoute->u4Flag & RTM6_RT_REACHABLE) !=
                                     RTM6_RT_REACHABLE)
                                    && ((pTempRoute->u4Flag & RTM6_ECMP_RT) ==
                                        RTM6_ECMP_RT))
                                {
                                    Rtm6AddRtToECMP6PRT (pRtm6Cxt, pTempRoute);

                                }
                                pTempRoute = pTempRoute->pNextAlternatepath;
                            }

                        }
                    }
                }
                else
                {
                    pIp6TmpRoute = pIp6TmpRoute->pNextAlternatepath;
                }
            }
        }

        if (Rtm6TrieGetNextEntryInCxt (pRtm6Cxt,
                                       pIp6Route, &pNextRt,
                                       &pRibNode) == RTM6_FAILURE)
        {
            /* No more route present. Set the cookie to indicate it */
            break;
        }
        pIp6Route = pNextRt;
        pNextRt = NULL;
    }
    while (pIp6Route != NULL);

    RTM6_TASK_UNLOCK ();
    return;
}

/******************************************************************************
 * DESCRIPTION : This function scan Ipv6 routing Table to delete ECMP 
 *               route with specified Next Hop IP addres. 
 *               
 *
 * INPUTS      : u4ContextId - RTM6 Context ID
 *             : pNextHopIpAddr-- Infomation about the route to be
 *                                 retrieved.
 *
 * OUTPUTS     : None. 
 *
 * RETURNS     : None
 *
 * NOTES       :
 ******************************************************************************/

VOID
Rtm6ApiDelAllRtWithNxtHopInHWInLnx (UINT4 u4ContextId,
                                    tIp6Addr * pNextHopIpAddr)
{
    tIp6RtEntry        *pIp6Route = NULL;
    tIp6RtEntry        *pIp6BestRoute = NULL;
    tIp6RtEntry        *pIp6TmpRoute = NULL;
    tIp6RtEntry        *pTempRoute = NULL;
    INT4                i4Reachable = 0;
    tRtm6Cxt           *pRtm6Cxt = NULL;
    tNetIpv6RtInfo      NetIpv6RtInfo;
#ifndef MPLS_IPV6_WANTED
    tPNHopHitRtNode     Exst6NhNode;
    tPNHopHitRtNode    *pNh6Entry = NULL;
    tPRt6Entry         *pRtCur6Entry = NULL;
#endif

#ifdef MPLS_IPV6_WANTED
    tGenU4Addr          DestNet;
    tIp6RtEntry        *pNextRt = NULL;
    VOID               *pRibNode = NULL;
    MEMSET (&DestNet, 0, sizeof (tGenU4Addr));
#endif

    RTM6_TASK_LOCK ();
    pRtm6Cxt = UtilRtm6GetCxt (u4ContextId);
    if (pRtm6Cxt == NULL)
    {

        RTM6_TASK_UNLOCK ();
        return;
    }

#ifdef MPLS_IPV6_WANTED
    if (Rtm6TrieGetFirstEntryInCxt (pRtm6Cxt,
                                    &pIp6Route, &pRibNode) == IP6_FAILURE)
    {
        RTM6_TASK_UNLOCK ();
        return;
    }

    /*Get the best route. And if there are ECMP routes
     * matching the next hop, and if it is not already 
     * reachable, program the routes in the kernel.
     */

    do
    {
        MEMCPY (&(DestNet.Addr.Ip6Addr), &(pIp6Route->dst), sizeof (tIp6Addr));;
        DestNet.u2AddrType = MPLS_IPV6_ADDR_TYPE;
        if (MplsIsFtnExist (&DestNet, (UINT4) (pIp6Route->u1Prefixlen)) == TRUE)
        {
            if (Rtm6TrieGetNextEntryInCxt (pRtm6Cxt,
                                           pIp6Route, &pNextRt,
                                           &pRibNode) == RTM6_FAILURE)
            {
                /* No more route present. Set the cookie to indicate it */
                break;
            }
            pIp6Route = pNextRt;
            pNextRt = NULL;
            continue;
        }
#else
    MEMSET (&Exst6NhNode, 0, sizeof (tPNHopHitRtNode));
    Exst6NhNode.pRtm6Cxt = pRtm6Cxt;
    MEMCPY (&(Exst6NhNode.NextHop), pNextHopIpAddr, sizeof (tIp6Addr));

    /*Get the best route. And if there are ECMP routes
     *      * matching the next hop, and if it is not already
     *           * reachable, program the routes in the kernel.
     *                */

    pNh6Entry = RBTreeGet (gRtm6GlobalInfo.pNextHopRBRoot, &Exst6NhNode);
    if (pNh6Entry == NULL)
    {
        RTM6_TASK_UNLOCK ();
        return;
    }
    Rtm6RelLock ();                /* Take RouteEntryList lock */
    TMO_SLL_Scan (&(pNh6Entry->routeEntryList), pRtCur6Entry, tPRt6Entry *)
    {
        pIp6Route = pRtCur6Entry->pPend6Rt;

#endif
        if (Rtm6UtilFwdTblGetBestRouteInCxt
            (pRtm6Cxt, pIp6Route, &pIp6BestRoute) == IP_SUCCESS)
        {
            pIp6TmpRoute = pIp6BestRoute;
            while (pIp6TmpRoute != NULL)
            {

                /* Check for next hop */
                if (Ip6AddrMatch (pNextHopIpAddr, &pIp6TmpRoute->nexthop,
                                  IP6_ADDR_SIZE_IN_BITS))
                {
                    /*Route Matched with reachable next hop
                     * Need to install this in kernel.
                     */

                    /* program route in kernel. */
                    if (((pIp6TmpRoute->u4Flag & RTM6_ECMP_RT) == RTM6_ECMP_RT)
                        && (pIp6TmpRoute->u4RowStatus == IP6FWD_ACTIVE))
                    {

                        if (pIp6TmpRoute != pIp6BestRoute)
                        {

                            RTM6_COPY_ECMP6_ROUTE_INFO_TO_UPDATE_MSG
                                (pIp6TmpRoute, NetIpv6RtInfo);

                            NetIpv6RtInfo.u4ContextId = pRtm6Cxt->u4ContextId;
                            NetIpv6RtInfo.u4RowStatus = IP6FWD_DESTROY;
                            NetIpv6InvokeRouteChangeForEcmp (&NetIpv6RtInfo,
                                                             NETIPV6_ALL_PROTO);
                        }
                        else
                        {
                            RTM6_COPY_ECMP6_ROUTE_INFO_TO_UPDATE_MSG
                                (pIp6TmpRoute, NetIpv6RtInfo);

                            NetIpv6RtInfo.u4ContextId = pRtm6Cxt->u4ContextId;
                            NetIpv6RtInfo.u4RowStatus = IP6FWD_DESTROY;
                            NetIpv6InvokeRouteChange (&NetIpv6RtInfo,
                                                      NETIPV6_ALL_PROTO);

                        }
                    }
                    pIp6TmpRoute->u4Flag &= ~(UINT4) RTM6_RT_REACHABLE;

                    pTempRoute = pIp6BestRoute;
                    i4Reachable = 0;
                    while (pTempRoute != NULL)
                    {
                        if ((pTempRoute->u4Flag & RTM6_RT_REACHABLE) ==
                            RTM6_RT_REACHABLE)
                        {
                            i4Reachable++;

                        }
                        pTempRoute = pTempRoute->pNextAlternatepath;
                    }

                    /* Once the reachable route count is 0, then we need to remove Best route in NP */
                    if ((i4Reachable > 0) &&
                        ((pIp6TmpRoute->u4Flag & RTM6_ECMP_RT) == RTM6_ECMP_RT))
                    {
                        /* The value 1 is added as */
                        pIp6TmpRoute->u1EcmpCount = i4Reachable + 1;
                        Rtm6ApiHandleEcmpRtInNp (pRtm6Cxt->u4ContextId,
                                                 pIp6TmpRoute, IP6_ROUTE_DEL);

                        Rtm6DeleteRtFromECMP6PRT (pRtm6Cxt, pIp6TmpRoute);

                    }
                    else if (i4Reachable == 0)
                    {
                        Rtm6AddRtToECMP6PRT (pRtm6Cxt, pIp6TmpRoute);
                        /* If the route is not an ECMP route, add the route in PRT list */
                        if ((pIp6TmpRoute->u4Flag & RTM6_ECMP_RT) !=
                            RTM6_ECMP_RT)
                        {
                            Rtm6AddRtToPRTInCxt (pRtm6Cxt, pIp6TmpRoute);
                        }
                    }
                }
                pIp6TmpRoute = pIp6TmpRoute->pNextAlternatepath;

            }
            /*Check the reachablity of other routes and add to ECMP6 PRT if required */

        }

#ifndef MPLS_IPV6_WANTED
    }
#else
        if (Rtm6TrieGetNextEntryInCxt (pRtm6Cxt,
                                       pIp6Route, &pNextRt,
                                       &pRibNode) == RTM6_FAILURE)
        {
            /* No more route present. Set the cookie to indicate it */
            break;
        }
        pIp6Route = pNextRt;
        pNextRt = NULL;

    }
    while (pIp6Route != NULL);
#endif
    Rtm6RelUnLock ();            /* Unlock Route Entry List Lock */
    RTM6_TASK_UNLOCK ();
    return;

}

/******************************************************************************
 * DESCRIPTION : This function scans the Resolved next hop table and populate *
 *               the routes in the H/W routing table.                         *
 *                                                                            *
 * INPUTS      : u4ContextId - RTM6 Context ID                                *
 *             : pNextHopIpAddr-- Infomation about the route to be            *
 *                                 retrieved.                                 *
 *                                                                            *
 * OUTPUTS     : None.                                                        *
 *                                                                            *
 * RETURNS     : None                                                         *
 *                                                                            *
 ******************************************************************************/
VOID
Rtm6ApiAddRtInHWInCxt (UINT4 u4ContextId, tIp6Addr * pNextHopIpAddr)
{

    tRNh6Node          *pRNH6Entry = NULL, ExstNh6Node;
    tPNh6Node          *pPNH6Entry = NULL;
    tPRt6Entry         *pPRt6Curr = NULL;
    tPRt6Entry         *pPRt6Next = NULL;
    tRtm6Cxt           *pRtm6Cxt = NULL;
    pRtm6Cxt = UtilRtm6GetCxt (u4ContextId);
    if (pRtm6Cxt == NULL)
    {
        return;
    }

    /* Check if already the entry exists in the Resolved Next hop entry */
    ExstNh6Node.NextHop = *pNextHopIpAddr;
    ExstNh6Node.pRtm6Cxt = pRtm6Cxt;

    pRNH6Entry = RBTreeGet (gRtm6GlobalInfo.pRNHT6RBRoot, &ExstNh6Node);

    if (pRNH6Entry == NULL)
    {
        /* Check if the entry exists in Pend Next Hop table, if it exists
         * remove that entry and add it to the Resolved next hop entry */

        pPNH6Entry = RBTreeGet (gRtm6GlobalInfo.pPRT6RBRoot, &ExstNh6Node);
        if (pPNH6Entry != NULL)
        {
            TMO_DYN_SLL_Scan (&pPNH6Entry->routeEntryList, pPRt6Curr, pPRt6Next,
                              tPRt6Entry *)
            {
                if (pPRt6Curr->pPend6Rt->i1Type == IP6_ROUTE_TYPE_DISCARD)
                {

                    Rtm6AddOrDelRtInFIBInCxt (pRtm6Cxt->u4ContextId,
                                              pPRt6Curr->pPend6Rt,
                                              IP6_ROUTE_DEL);
                    pPRt6Curr->pPend6Rt->i1Type = IP6_ROUTE_TYPE_INDIRECT;
                    Rtm6AddOrDelRtInFIBInCxt (pRtm6Cxt->u4ContextId,
                                              pPRt6Curr->pPend6Rt,
                                              IP6_ROUTE_ADD);

                    RTM6_TRC_ARG2 (u4ContextId, RTM6_CTRL_PATH_TRC,
                                   RTM6_MOD_NAME,
                                   "Route %s %s in PRT Changed from drop "
                                   "route to normal\n",
                                   Ip6PrintAddr (&pPRt6Curr->pPend6Rt->dst),
                                   Ip6PrintAddr (&pPRt6Curr->pPend6Rt->
                                                 nexthop));
                }

                pPRt6Curr->pPend6Rt->u4Flag &= (UINT4) ~RTM6_RT_IN_PRT;
                pPRt6Curr->pPend6Rt->u4Flag |= RTM6_RT_REACHABLE;
                TMO_SLL_Delete (&pPNH6Entry->routeEntryList,
                                (tTMO_SLL_NODE *) pPRt6Curr);
                MemReleaseMemBlock (gRtm6GlobalInfo.Rtm6URRtPoolId,
                                    (UINT1 *) pPRt6Curr);
            }

            RBTreeRem (gRtm6GlobalInfo.pPRT6RBRoot, pPNH6Entry);
            MemReleaseMemBlock (gRtm6GlobalInfo.Rtm6PNextHopPoolId,
                                (UINT1 *) pPNH6Entry);
        }

        /* Allocate the Resolved Next hop node */
        if ((pRNH6Entry = (tRNh6Node *)
             MemAllocMemBlk (gRtm6GlobalInfo.Rtm6RNextHopPoolId)) == NULL)
        {
            return;
        }
        pRNH6Entry->NextHop = *pNextHopIpAddr;
        pRNH6Entry->pRtm6Cxt = pRtm6Cxt;

        /* Add the Resolved next hop Entry to RBTree */
        if (RBTreeAdd (gRtm6GlobalInfo.pRNHT6RBRoot, pRNH6Entry) == RB_FAILURE)
        {
            MemReleaseMemBlock (gRtm6GlobalInfo.Rtm6RNextHopPoolId,
                                (UINT1 *) pRNH6Entry);

            return;
        }
    }
}

/******************************************************************************
 * DESCRIPTION : This function scans the Resolved next hop table and deletes  *
 *               the next hop.                                                *
 *                                                                            *
 * INPUTS      : u4ContextId - RTM6 Context ID                                *
 *             : pNextHopIpAddr-- Infomation about the route to be            *
 *                                 retrieved.                                 *
 *                                                                            *
 * OUTPUTS     : None.                                                        *
 *                                                                            *
 * RETURNS     : None                                                         *
 *                                                                            *
 ******************************************************************************/
VOID
Rtm6ApiDelNextHopInCxt (UINT4 u4ContextId, tIp6Addr * pNextHopIpAddr)
{
    tRNh6Node          *pRNH6Entry = NULL, ExstNh6Node;
    tRtm6Cxt           *pRtm6Cxt = NULL;

    pRtm6Cxt = UtilRtm6GetCxt (u4ContextId);
    if (pRtm6Cxt == NULL)
    {
        return;
    }

    /* Check if already the entry exists in the Resolved Next hop entry */
    ExstNh6Node.NextHop = *pNextHopIpAddr;
    ExstNh6Node.pRtm6Cxt = pRtm6Cxt;
    pRNH6Entry = RBTreeGet (gRtm6GlobalInfo.pRNHT6RBRoot, &ExstNh6Node);

    if (pRNH6Entry != NULL)
    {
        RBTreeRem (gRtm6GlobalInfo.pRNHT6RBRoot, pRNH6Entry);
        MemReleaseMemBlock (gRtm6GlobalInfo.Rtm6RNextHopPoolId,
                            (UINT1 *) pRNH6Entry);
        return;
    }
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : Rtm6ApiValIdxInstIpv6RouteTable
 *
 *    DESCRIPTION      : This function validate the given Ipvx Route Index
 *                       in the Ipv6 route table            
 *
 *    INPUT            : u4ContextId   - RTM6 Contextid
 *                     : pIpv6Dest     - Dest ipv6 address.
 *                     : u4IPv6PfxLen  - Dest ipv6 address Prefix Len.
 *                     : pIpv6NextHop  - Next hop ipv6 address.
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : RTM6_SUCCESS / RTM6_FAILURE
 *                       
 *    NOTE             : This function should be called betwwen RTM6_TASK_LOCK()
 *                        and  RTM6_TASK_UNLOCK ()
 ****************************************************************************/
INT1
Rtm6ApiValIdxInstIpv6RouteTableInCxt (UINT4 u4ContextId,
                                      tSNMP_OCTET_STRING_TYPE * pIpv6Dest,
                                      UINT4 u4IPv6PfxLen,
                                      tSNMP_OCTET_STRING_TYPE * pIpv6NextHop)
{
    tIp6RtEntry         Ip6RtEntry;
    tIp6RtEntry        *pIp6RtEntry = NULL;
    VOID               *pRibNode = NULL;
    tRtm6Cxt           *pRtm6Cxt = NULL;

    pRtm6Cxt = UtilRtm6GetCxt (u4ContextId);
    if (pRtm6Cxt == NULL)
    {
        return RTM6_FAILURE;
    }

    MEMSET (&Ip6RtEntry, IP6_ZERO, sizeof (tIp6RtEntry));

    if (pIpv6Dest != NULL)
    {
        MEMCPY (&(Ip6RtEntry.dst), pIpv6Dest->pu1_OctetList, IP6_ADDR_SIZE);
    }

    Ip6RtEntry.u1Prefixlen = (UINT1) u4IPv6PfxLen;

    if (pIpv6NextHop != NULL)
    {
        MEMCPY (&(Ip6RtEntry.nexthop), pIpv6NextHop->pu1_OctetList,
                IP6_ADDR_SIZE);
    }

    Ip6RtEntry.i1Proto = ALL_PROTO_ID + IP6_ONE;

    if (Rtm6TrieSearchExactEntryInCxt (pRtm6Cxt, &Ip6RtEntry,
                                       &pIp6RtEntry, &pRibNode) == IP6_FAILURE)
    {
        return RTM6_FAILURE;
    }

    for (; pIp6RtEntry != NULL; pIp6RtEntry = pIp6RtEntry->pNextAlternatepath)
    {
        if (MEMCMP (&pIp6RtEntry->nexthop, &(Ip6RtEntry.nexthop),
                    sizeof (tIp6Addr)) == IP6_ZERO)
        {
            /* Match route found. */
            break;
        }
    }

    if (pIp6RtEntry == NULL)
    {
        return (RTM6_FAILURE);
    }

    return (RTM6_SUCCESS);
}

/****************************************************************************
 *
 *    FUNCTION NAME    : Rtm6ApiGetNextIpv6RouteTableEntryInCxt
 *
 *    DESCRIPTION      : This function Give the next ipv6 route entry for a
 *                       given Ipvx Route Index in the Ipv6 route table         
 *
 *    INPUT            : u4ContextId     - RTM6 Context ID   
 *                     : pIpv6Dest       - Dest ipv6 address.
 *                     : pIpv6NxtDest    - next Dest ipv6 address.
 *                     : u4IPv6PfxLen    - Dest ipv6 address Prefix Len.
 *                     : pu4NxtIPv6PfxLen- next Dest ipv6 address Prefix Len.
 *                     : pIpv6NextHop    - Next hop ipv6 address.
 *                     : pNxtIpv6NextHop - next Next hop ipv6 address.
 *                     : u1IsFirst       - TRUE / FALSE               
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : RTM6_SUCCESS / RTM6_FAILURE
 *
 *    NOTE             : This function should be called betwwen RTM6_TASK_LOCK()
 *                       and RTM6_TASK_UNLOCK ()
 ****************************************************************************/
INT1
Rtm6ApiGetNextIpv6RouteTableEntryInCxt (UINT4 u4ContextId,
                                        tSNMP_OCTET_STRING_TYPE * pIpv6Dest,
                                        tSNMP_OCTET_STRING_TYPE * pIpv6NxtDest,
                                        UINT4 u4IPv6PfxLen,
                                        UINT4 *pu4NxtIPv6PfxLen,
                                        tSNMP_OCTET_STRING_TYPE * pIpv6NextHop,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pNxtIpv6NextHop, UINT1 u1IsFirst)
{
    tIp6RtEntry        *pNextRtEntry = NULL;
    tIp6RtEntry        *pBestRoute = NULL;
    tIp6RtEntry        *pTmpRt6 = NULL;
    VOID               *pRibNode = NULL;
    tIp6Addr            TmpNxtHop;
    tIp6Addr            DstAddr;
    INT4                i4RetVal = RTM6_FAILURE;
    INT4                i4OutCome = RTM6_FAILURE;
    UINT1               u1FoundFlag = FALSE;
    tRtm6Cxt           *pRtm6Cxt = NULL;

    pRtm6Cxt = UtilRtm6GetCxt (u4ContextId);
    if (pRtm6Cxt == NULL)
    {
        return RTM6_FAILURE;
    }

    if (u1IsFirst == TRUE)
    {
        i4RetVal = Rtm6TrieGetFirstEntryInCxt (pRtm6Cxt,
                                               &pNextRtEntry, &pRibNode);
        if (i4RetVal == RTM6_FAILURE)
        {
            /* Route is not present. */
            return (RTM6_FAILURE);
        }
        if (pNextRtEntry->u1AddrType == ADDR6_ANYCAST)
        {
            i4OutCome =
                Rtm6UtilFwdTblGetBestAnycastRouteInCxt (pRtm6Cxt, pNextRtEntry,
                                                        &pBestRoute);
        }
        else
        {
            i4OutCome =
                Rtm6UtilFwdTblGetBestRouteInCxt (pRtm6Cxt, pNextRtEntry,
                                                 &pBestRoute);
        }
        if (i4OutCome == RTM6_SUCCESS)
        {
            pTmpRt6 = pBestRoute;

            *pu4NxtIPv6PfxLen = pBestRoute->u1Prefixlen;
            Ip6AddrCopy ((tIp6Addr *) (VOID *) pIpv6NxtDest->pu1_OctetList,
                         &(pBestRoute->dst));
            pIpv6NxtDest->i4_Length = IP6_ADDR_SIZE;

            MEMSET (&(TmpNxtHop), IPVX_UINT1_ALL_ONE, sizeof (tIp6Addr));

            /* Get the Route Entry with least nexthop */
            for (; pTmpRt6 != NULL; pTmpRt6 = pTmpRt6->pNextAlternatepath)
            {
                if ((MEMCMP (&pTmpRt6->nexthop, &(TmpNxtHop),
                             sizeof (tIp6Addr)) < IP6_ZERO))
                {
                    Ip6AddrCopy ((tIp6Addr *) & (TmpNxtHop),
                                 &(pTmpRt6->nexthop));
                    u1FoundFlag = TRUE;
                }
            }

            if (u1FoundFlag == TRUE)
            {
                /* Match route found. */
                Ip6AddrCopy ((tIp6Addr *) (VOID *) pNxtIpv6NextHop->
                             pu1_OctetList, &(TmpNxtHop));
                pNxtIpv6NextHop->i4_Length = IP6_ADDR_SIZE;

                return (RTM6_SUCCESS);
            }
        }
        else
        {
            /* Give chance to atleast get the next entry after consulting
             * with Ip6GetNextBestRouteEntryInCxt.So initialise
             * pIpv6Dest with current known pNextRtEntry */
            MEMCPY (&(DstAddr), &(pNextRtEntry->dst), sizeof (tIp6Addr));
            u4IPv6PfxLen = pNextRtEntry->u1Prefixlen;
        }
    }
    else
    {
        if (pIpv6Dest != NULL)
        {
            MEMCPY (&(DstAddr), pIpv6Dest->pu1_OctetList, IP6_ADDR_SIZE);
        }
        else
        {
            MEMSET (&(DstAddr), IP6_ZERO, sizeof (tIp6Addr));
        }

        if ((Ip6GetBestRouteEntryInCxt (pRtm6Cxt,
                                        &(DstAddr), (UINT1) u4IPv6PfxLen,
                                        &pBestRoute)) == RTM6_SUCCESS)
        {
            pTmpRt6 = pBestRoute;

            *pu4NxtIPv6PfxLen = pBestRoute->u1Prefixlen;
            Ip6AddrCopy ((tIp6Addr *) (VOID *) pIpv6NxtDest->pu1_OctetList,
                         &(pBestRoute->dst));
            pIpv6NxtDest->i4_Length = IP6_ADDR_SIZE;

            /* For anycast routes, we already get the best route from amongst
             * the alternate paths from Ip6GetBestRouteEntryInCxt. There is no
             * need to iterate any further */
            if (pBestRoute->u1AddrType != ADDR6_ANYCAST)
            {
                /* Get the Route Entry for the Same Dest withe Diff nexthop */
                for (; pTmpRt6 != NULL; pTmpRt6 = pTmpRt6->pNextAlternatepath)
                {
                    if (u1FoundFlag == FALSE)
                    {
                        if (MEMCMP (&pTmpRt6->nexthop,
                                    pIpv6NextHop->pu1_OctetList,
                                    sizeof (tIp6Addr)) > IP6_ZERO)
                        {
                            Ip6AddrCopy ((tIp6Addr *) & (TmpNxtHop),
                                         &(pTmpRt6->nexthop));
                            u1FoundFlag = TRUE;
                        }
                    }
                    else
                    {
                        if ((MEMCMP (&pTmpRt6->nexthop,
                                     pIpv6NextHop->pu1_OctetList,
                                     sizeof (tIp6Addr)) > IP6_ZERO) &&
                            (MEMCMP (&pTmpRt6->nexthop, &(TmpNxtHop),
                                     sizeof (tIp6Addr)) < IP6_ZERO))
                        {
                            Ip6AddrCopy ((tIp6Addr *) & (TmpNxtHop),
                                         &(pTmpRt6->nexthop));
                        }
                    }
                }
                if (u1FoundFlag == TRUE)
                {
                    /* Match route found. */
                    Ip6AddrCopy ((tIp6Addr *) (VOID *) pNxtIpv6NextHop->
                                 pu1_OctetList, &(TmpNxtHop));
                    pNxtIpv6NextHop->i4_Length = IP6_ADDR_SIZE;

                    return (RTM6_SUCCESS);
                }
            }
        }
    }
    /* No alternate route for the Dest get next entry */
    if ((Ip6GetNextBestRouteEntryInCxt (pRtm6Cxt,
                                        &(DstAddr), (UINT1) u4IPv6PfxLen,
                                        &pBestRoute)) == RTM6_SUCCESS)
    {
        pTmpRt6 = pBestRoute;

        *pu4NxtIPv6PfxLen = pBestRoute->u1Prefixlen;
        Ip6AddrCopy ((tIp6Addr *) (VOID *) pIpv6NxtDest->pu1_OctetList,
                     &(pBestRoute->dst));
        pIpv6NxtDest->i4_Length = IP6_ADDR_SIZE;
        if (pBestRoute->u1AddrType == ADDR6_ANYCAST)
        {
            /* There is no need to check alternate paths for anycast 
             * route entries as Ip6GetNextBestRouteEntryInCxt gives the
             * definitive best route entry. */
            Ip6AddrCopy ((tIp6Addr *) (VOID *)
                         pNxtIpv6NextHop->pu1_OctetList, &(pTmpRt6->nexthop));
            pNxtIpv6NextHop->i4_Length = IP6_ADDR_SIZE;
            return (RTM6_SUCCESS);
        }

        /* Get the Route Entry for the Same Dest withe Diff nexthop */
        for (; pTmpRt6 != NULL; pTmpRt6 = pTmpRt6->pNextAlternatepath)
        {
            if (u1FoundFlag == FALSE)
            {
                Ip6AddrCopy ((tIp6Addr *) & (TmpNxtHop), &(pTmpRt6->nexthop));
                u1FoundFlag = TRUE;
            }
            else
            {
                if (MEMCMP (&pTmpRt6->nexthop, &(TmpNxtHop),
                            sizeof (tIp6Addr)) < IP6_ZERO)
                {
                    Ip6AddrCopy ((tIp6Addr *) & (TmpNxtHop),
                                 &(pTmpRt6->nexthop));
                }
            }
        }

        Ip6AddrCopy ((tIp6Addr *) (VOID *) pNxtIpv6NextHop->pu1_OctetList,
                     &(TmpNxtHop));
        pNxtIpv6NextHop->i4_Length = IP6_ADDR_SIZE;
        return (RTM6_SUCCESS);
    }
    return (RTM6_FAILURE);
}

/****************************************************************************
 *
 *    FUNCTION NAME    : Rtm6ApiGetIpv6RouteTableEntryObj
 *
 *    DESCRIPTION      : This function Get the given Obj value for the
 *                       given Ipvx Route Index in the Ipv6 route table         
 *
 *    INPUT            : u4ContextId     - u4ContextId.
 *                     : pIpv6Dest       - Dest ipv6 address.
 *                     : u4IPv6PfxLen    - Dest ipv6 address Prefix Len.
 *                     : pIpv6NextHop    - Next hop ipv6 address.
 *                     : u4ObjName       - name of the Object. 
 *                     : pu1ObjVal       - values of the given Object.
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : RTM6_SUCCESS / RTM6_FAILURE
 *
 *    NOTE             : This function should be called betwwen RTM6_TASK_LOCK()
 *                       and RTM6_TASK_UNLOCK ()
 ****************************************************************************/
INT1
Rtm6ApiGetIpv6RouteTableEntryObjInCxt (UINT4 u4ContextId,
                                       tSNMP_OCTET_STRING_TYPE * pIpv6Dest,
                                       UINT4 u4IPv6PfxLen,
                                       tSNMP_OCTET_STRING_TYPE * pIpv6NextHop,
                                       UINT4 u4ObjName, UINT1 *pu1ObjVal)
{
    tIp6RtEntry         Ip6RtEntry;
    tIp6RtEntry        *pIp6RtEntry = NULL;
    tIp6RtEntry        *pTempIp6RtEntry = NULL;
    tIp6RtEntry        *pTmpIp6RtEntry = NULL;
    VOID               *pRibNode = NULL;
    tRtm6Cxt           *pRtm6Cxt = NULL;
    INT4                i4TempProto = 0;
    UINT1               u1Found = 0;

    pRtm6Cxt = UtilRtm6GetCxt (u4ContextId);
    if (pRtm6Cxt == NULL)
    {
        return RTM6_FAILURE;
    }

    MEMSET (&Ip6RtEntry, IP6_ZERO, sizeof (tIp6RtEntry));

    MEMCPY (&(Ip6RtEntry.dst), pIpv6Dest->pu1_OctetList,
            MEM_MAX_BYTES ((UINT4) pIpv6Dest->i4_Length,
                           sizeof (Ip6RtEntry.dst)));

    Ip6RtEntry.u1Prefixlen = (UINT1) u4IPv6PfxLen;

    MEMCPY (&(Ip6RtEntry.nexthop), pIpv6NextHop->pu1_OctetList,
            MEM_MAX_BYTES ((UINT4) pIpv6NextHop->i4_Length,
                           sizeof (Ip6RtEntry.nexthop)));

    Ip6RtEntry.i1Proto = ALL_PROTO_ID + IP6_ONE;

    if (Rtm6TrieSearchExactEntryInCxt (pRtm6Cxt, &Ip6RtEntry,
                                       &pIp6RtEntry, &pRibNode) == IP6_FAILURE)
    {
        /* No route from this protocol. */
        return (RTM6_FAILURE);
    }

    /* Get the Best route. */
    pTempIp6RtEntry = pIp6RtEntry;
    for (pTmpIp6RtEntry = pIp6RtEntry; pTmpIp6RtEntry != NULL;
         pTmpIp6RtEntry = pIp6RtEntry->pNextRtEntry)
    {
        if (pTmpIp6RtEntry->i1Proto == IP6_LOCAL_PROTOID)
        {
            pIp6RtEntry = pTmpIp6RtEntry;
            u1Found = 1;
            break;
        }
        while (pTmpIp6RtEntry != NULL)
        {
            if (MEMCMP (&(pTmpIp6RtEntry->nexthop),
                        (tIp6Addr *) (VOID *) pIpv6NextHop->pu1_OctetList,
                        16) == 0)
            {
                pIp6RtEntry = pTmpIp6RtEntry;
                u1Found = 1;
                break;
            }
            pTmpIp6RtEntry = pTmpIp6RtEntry->pNextAlternatepath;
        }
        if (u1Found)
        {
            break;
        }
    }
    if ((u1Found == 0) && (pIp6RtEntry == NULL))
    {
        pIp6RtEntry = pTempIp6RtEntry;
    }

    /* Get the Value for the Object */
    switch (u4ObjName)
    {
        case IPVX_IPV6_RT_IF_INDEX:
            *(UINT4 *) (VOID *) pu1ObjVal = pIp6RtEntry->u4Index;
            break;

        case IPVX_IPV6_RT_ROUTE_TYPE:
            *(INT4 *) (VOID *) pu1ObjVal = pIp6RtEntry->i1Type;
            break;

        case IPVX_IPV6_RT_ROUTE_PROTO:
            i4TempProto = pIp6RtEntry->i1Proto;

            /* map Fsip proto to Ipvx proto */
            switch (i4TempProto)
            {
                case IP6_LOCAL_PROTOID:
                    i4TempProto = IANAIP_ROUTE_PROTO_LOCAL;
                    break;
                case IP6_NETMGMT_PROTOID:
                    i4TempProto = IANAIP_ROUTE_PROTO_NETMGMT;
                    break;
                case IP6_RIP_PROTOID:
                    i4TempProto = IANAIP_ROUTE_PROTO_RIP;
                    break;
                case IP6_OSPF_PROTOID:
                    i4TempProto = IANAIP_ROUTE_PROTO_OSPF;
                    break;
                case IP6_BGP_PROTOID:
                    i4TempProto = IANAIP_ROUTE_PROTO_BGP;
                    break;
                case IP6_ISIS_PROTOID:
                    i4TempProto = IANAIP_ROUTE_PROTO_ISIS;
                    break;
                default:
                    i4TempProto = IANAIP_ROUTE_PROTO_OTHER;
                    break;
            }

            *(INT4 *) (VOID *) pu1ObjVal = i4TempProto;
            break;

        case IPVX_IPV6_RT_ROUTE_AGE:
            *(UINT4 *) (VOID *) pu1ObjVal = OsixGetSysUpTime ();
            *(UINT4 *) (VOID *) pu1ObjVal -= pIp6RtEntry->u4ChangeTime;
            break;

        case IPVX_IPV6_RT_ROUTE_TAG:
            *(UINT4 *) (VOID *) pu1ObjVal = pIp6RtEntry->u4RouteTag;
            break;

        case IPVX_IPV6_RT_METRIC_1:
            *(UINT4 *) (VOID *) pu1ObjVal = pIp6RtEntry->u4Metric;
            break;

        case IPVX_IPV6_RT_ROWSTATUS:
            *(UINT4 *) (VOID *) pu1ObjVal = pIp6RtEntry->u4RowStatus;
            break;

        case IPVX_IPV6_RT_ADDR_TYPE:
            *(UINT4 *) (VOID *) pu1ObjVal = pIp6RtEntry->u1AddrType;
            break;

        case IPVX_IPV6_RT_PREFERENCE:
            *(UINT4 *) (VOID *) pu1ObjVal = pIp6RtEntry->u1Preference;
            break;

        default:
            break;
    }

    return (RTM6_SUCCESS);
}

/* rtm6api.c */
/******************************************************************************
* DESCRIPTION : This function gets route6 entry for this particular destination 
*               and copies Ip6 route info from tIp6RtEntry structure
*               to tNetIpv6RtInfo
*
* INPUTS      : u4ContextId - RTM^ Context ID
*             : pDest - Pointer to the destination address
*               u1PrefixLen - length of prefix
*               
* OUTPUTS     : pNetIpv6RtInfo
*
* RETURNS     : RTM6_SUCCESS/RTM6_FAILURE
*
* NOTES       :
******************************************************************************/
INT4
Rtm6ApiGetFwdRouteEntryInCxt (UINT4 u4ContextId,
                              tIp6Addr * pDest, UINT1 u1PrefixLen,
                              tNetIpv6RtInfo * pNetIpv6RtInfo)
{
    tIp6RtEntry        *pBestRoute = NULL;
    tRtm6Cxt           *pRtm6Cxt = NULL;

    RTM6_TASK_LOCK ();
    pRtm6Cxt = UtilRtm6GetCxt (u4ContextId);
    if (pRtm6Cxt == NULL)
    {
        RTM6_TASK_UNLOCK ();
        return RTM6_FAILURE;
    }

    if (Ip6GetFwdRouteEntryInCxt (pRtm6Cxt, pDest,
                                  u1PrefixLen, &pBestRoute) == RTM6_FAILURE)
    {
        RTM6_TASK_UNLOCK ();
        return RTM6_FAILURE;
    }
    Rtm6UtilCopyRtInfoInCxt (pRtm6Cxt, pBestRoute, pNetIpv6RtInfo);
    RTM6_TASK_UNLOCK ();
    return RTM6_SUCCESS;
}

/******************************************************************************
* DESCRIPTION : This function gets FIRST route6 entry from RTM 
*               and copies Ip6 route info from tIp6RtEntry structure
*               to tNetIpv6RtInfo

* INPUTS      : u4ContextId - RTM6 Context ID
*
* OUTPUTS     : pNetIpv6RtInfo
*
* RETURNS     : RTM6_SUCCESS/RTM6_FAILURE
*
* NOTES       :
******************************************************************************/
INT4
Rtm6ApiTrieGetFirstEntryInCxt (UINT4 u4ContextId,
                               tNetIpv6RtInfo * pNetIpv6RtInfo)
{
    VOID               *pRibNode = NULL;
    tIp6RtEntry        *pRoute = NULL;
    tRtm6Cxt           *pRtm6Cxt = NULL;

    RTM6_TASK_LOCK ();
    pRtm6Cxt = UtilRtm6GetCxt (u4ContextId);
    if (pRtm6Cxt == NULL)
    {
        RTM6_TASK_UNLOCK ();
        return RTM6_FAILURE;
    }
    if (Rtm6TrieGetFirstEntryInCxt (pRtm6Cxt, &pRoute,
                                    &pRibNode) == RTM6_FAILURE)
    {
        RTM6_TASK_UNLOCK ();
        return RTM6_FAILURE;
    }
    Rtm6UtilCopyRtInfoInCxt (pRtm6Cxt, pRoute, pNetIpv6RtInfo);

    RTM6_TASK_UNLOCK ();
    return RTM6_SUCCESS;
}

/******************************************************************************
* DESCRIPTION : This function gets NEXT route6 entry for pNetIpv6RtInfo 
*               and copies Ip6 route info from tIp6RtEntry structure
*               to tNetIpv6RtInfo

* INPUTS      : pNetIpv6RtInfo - IPV6 route entry
*
* OUTPUTS     : pNextNetIpv6RtInfo - Next IPV6 route entry
*
* RETURNS     : RTM6_SUCCESS/RTM6_FAILURE
*
* NOTES       :
******************************************************************************/
INT4
Rtm6ApiTrieGetNextEntry (tNetIpv6RtInfo * pNetIpv6RtInfo,
                         tNetIpv6RtInfo * pNextNetIpv6RtInfo)
{
    VOID               *pRibNode = NULL;
    tIp6RtEntry         Route;
    tIp6RtEntry        *pRoute = NULL;
    tRtm6Cxt           *pRtm6Cxt = NULL;

    MEMSET (&Route, 0, sizeof (tIp6RtEntry));
    MEMCPY (&Route.dst, &pNetIpv6RtInfo->Ip6Dst, sizeof (tIp6Addr));
    Route.u1Prefixlen = pNetIpv6RtInfo->u1Prefixlen;
    RTM6_TASK_LOCK ();
    pRtm6Cxt = UtilRtm6GetCxt (pNetIpv6RtInfo->u4ContextId);
    if (pRtm6Cxt == NULL)
    {
        RTM6_TASK_UNLOCK ();
        return RTM6_FAILURE;
    }
    if (Rtm6TrieGetNextEntryInCxt (pRtm6Cxt, &Route,
                                   &pRoute, &pRibNode) == RTM6_FAILURE)
    {
        RTM6_TASK_UNLOCK ();
        return RTM6_FAILURE;
    }
    Rtm6UtilCopyRtInfoInCxt (pRtm6Cxt, pRoute, pNextNetIpv6RtInfo);
    RTM6_TASK_UNLOCK ();
    return RTM6_SUCCESS;

}

/******************************************************************************
* DESCRIPTION : This function gets first BEST route6 entry 

* INPUTS      : u4ContextId - RTM6 Context ID
*
* OUTPUTS     : pNetIpv6RtInfo - pointer to the first best route entry
*
* RETURNS     : RTM6_SUCCESS/RTM6_FAILURE
*
* NOTES       : No RTM6_TASK_LOCK taken inside this function, modules who is 
*               calling this function should take the RTM6_TASK_LOCK and 
*               release the lock using RTM6_TASK_UNLOCK
******************************************************************************/
INT4
Rtm6ApiTrieGetFirstBestRtInCxt (UINT4 u4ContextId,
                                tNetIpv6RtInfo * pNetIpv6RtInfo)
{
    VOID               *pRibNode = NULL;
    tIp6RtEntry        *pIp6OutRtInfo = NULL;
    tIp6RtEntry        *pBestRoute = NULL;
    tRtm6Cxt           *pRtm6Cxt = NULL;

    pRtm6Cxt = UtilRtm6GetCxt (u4ContextId);
    if (pRtm6Cxt == NULL)
    {
        return RTM6_FAILURE;
    }

    /* Get the first route from the TRIE. */
    if (Rtm6TrieGetFirstEntryInCxt (pRtm6Cxt, &pBestRoute, &pRibNode)
        == RTM6_FAILURE)
    {
        /* No IPV6 Route present. */
        return RTM6_FAILURE;
    }
    Ip6GetBestRouteEntryInCxt (pRtm6Cxt, &pBestRoute->dst,
                               pBestRoute->u1Prefixlen, &pIp6OutRtInfo);
    Rtm6UtilCopyRtInfoInCxt (pRtm6Cxt, pIp6OutRtInfo, pNetIpv6RtInfo);
    return RTM6_SUCCESS;
}

/******************************************************************************
* DESCRIPTION : This function gets next BEST route6 entry for this
*               destination address
*
* INPUTS      : u4ContextId - RTM6 Context ID
*             : pDest - Pointer to Ip6 address
*               u1PrefixLen - length of prefix
*               u4Ipv6RouteIndex - router index
*
* OUTPUTS     : pNetIpv6RtInfo -  Pointer to the next best entry
*
* RETURNS     : RTM6_SUCCESS/RTM6_FAILURE
*
* NOTES       : No RTM6_TASK_LOCK taken inside this function, modules who is 
*               calling this function should take the RTM6_TASK_LOCK and 
*               release the lock using RTM6_TASK_UNLOCK
******************************************************************************/
INT4
Rtm6ApiTrieGetNextBestRtInCxt (UINT4 u4ContextId,
                               tIp6Addr * pDest, UINT1 u1PrefixLen,
                               UINT4 u4Ipv6RouteIndex,
                               tNetIpv6RtInfo * pNetIpv6RtInfo)
{
    tIp6RtEntry        *ppIp6OutRtInfo = NULL;
    tRtm6Cxt           *pRtm6Cxt = NULL;

    UNUSED_PARAM (u4Ipv6RouteIndex);

    pRtm6Cxt = UtilRtm6GetCxt (u4ContextId);
    if (pRtm6Cxt == NULL)
    {
        return RTM6_FAILURE;
    }
    if (Ip6GetNextBestRouteEntryInCxt (pRtm6Cxt, pDest, u1PrefixLen,
                                       &ppIp6OutRtInfo) == RTM6_FAILURE)
    {
        /* No more best route. */
        return RTM6_FAILURE;
    }

    Rtm6UtilCopyRtInfoInCxt (pRtm6Cxt, ppIp6OutRtInfo, pNetIpv6RtInfo);
    return RTM6_SUCCESS;

}

/******************************************************************************
* DESCRIPTION : This function gets  BEST route6 entry for this
*               destination address
*
** INPUTS      : u4ContextId - RTM6 Context Id 
**             : pDest - Pointer to Ip6 address
*               u1PrefixLen - length of prefix
*               u4Ipv6RouteIndex - router index

* OUTPUTS     : pNetIpv6RtInfo - pointer to the best router entry
*
* RETURNS     : RTM6_SUCCESS/RTM6_FAILURE 
*
* NOTES       : No RTM6_TASK_LOCK taken inside this function, modules who is 
*               calling this function should take the RTM6_TASK_LOCK and 
*               release the lock using RTM6_TASK_UNLOCK
******************************************************************************/
INT4
Rtm6ApiGetBestRouteEntryInCxt (UINT4 u4ContextId,
                               tIp6Addr * pIp6Dest, UINT1 u1PrefixLen,
                               UINT4 u4Ipv6RouteIndex,
                               tNetIpv6RtInfo * pNetIpv6RtInfo)
{
    tIp6RtEntry        *pIp6OutRtInfo = NULL;
    tRtm6Cxt           *pRtm6Cxt = NULL;

    UNUSED_PARAM (u4Ipv6RouteIndex);
    pRtm6Cxt = UtilRtm6GetCxt (u4ContextId);
    if (pRtm6Cxt == NULL)
    {
        return RTM6_FAILURE;
    }
    Ip6GetBestRouteEntryInCxt (pRtm6Cxt, pIp6Dest, u1PrefixLen, &pIp6OutRtInfo);
    if (pIp6OutRtInfo == NULL)
    {
        return RTM6_FAILURE;
    }
    Rtm6UtilCopyRtInfoInCxt (pRtm6Cxt, pIp6OutRtInfo, pNetIpv6RtInfo);
    return RTM6_SUCCESS;
}

/******************************************************************************
* DESCRIPTION : This function gets first ipv6 route entry with least nexthop
*
** INPUTS      : u4ContextId - RTM6 Context Id

* OUTPUTS     : pNetIpv6RtInfo - Ipv6 route info
*
* RETURNS     : RTM6_SUCCESS/RTM6_FAILURE
*
* NOTES       : No RTM6_TASK_LOCK taken inside this function, modules who is 
*               calling this function should take the RTM6_TASK_LOCK and 
*               release the lock using RTM6_TASK_UNLOCK
******************************************************************************/

INT4
Rtm6ApiTrieGetFirstRtWithLeastNHInCxt (UINT4 u4ContextId,
                                       tNetIpv6RtInfo * pNetIpv6RtInfo)
{
    VOID               *pRibNode = NULL;
    tIp6RtEntry        *pFirstRtEntry = NULL;
    tIp6RtEntry        *pTempEntry = NULL;
    tRtm6Cxt           *pRtm6Cxt = NULL;

    pRtm6Cxt = UtilRtm6GetCxt (u4ContextId);

    if (pRtm6Cxt == NULL)
    {
        return RTM6_FAILURE;
    }

    if (Rtm6TrieGetFirstEntryInCxt (pRtm6Cxt, &pFirstRtEntry,
                                    &pRibNode) == RTM6_FAILURE)
    {
        return RTM6_FAILURE;
    }
    /* In the same protocol id, get the least nexthop address */
    for (; pFirstRtEntry != NULL;
         pFirstRtEntry = pFirstRtEntry->pNextAlternatepath)
    {
        if ((pTempEntry == NULL) ||
            (Ip6IsAddrGreater (&pFirstRtEntry->nexthop, &pTempEntry->nexthop)
             == SUCCESS))
        {
            pTempEntry = pFirstRtEntry;
        }
    }

    Rtm6UtilCopyRtInfoInCxt (pRtm6Cxt, pTempEntry, pNetIpv6RtInfo);
    return RTM6_SUCCESS;
}

/******************************************************************************
* DESCRIPTION : This function gets the next ipv6 route entry for a given 
*               destination, prefix, protocol and nexthop
*
** INPUTS      : u4ContextId - RTM6 Context Id
                 pFsipv6RouteDest - Ipv6 route destination
                 i4Fsipv6RoutePfxLength - Prefix Length
                 i4Fsipv6RouteProtocol - routing protocol
                 pFsipv6RouteNextHop - Ipv6 nexthop

* OUTPUTS     : pNetIpv6RtInfo - Ipv6 route info
*
* RETURNS     : RTM6_SUCCESS/RTM6_FAILURE
*
* NOTES       : No RTM6_TASK_LOCK taken inside this function, modules who is 
*               calling this function should take the RTM6_TASK_LOCK and 
*               release the lock using RTM6_TASK_UNLOCK
******************************************************************************/

INT4
Rtm6ApiTrieGetNextRtWithLeastNHInCxt (UINT4 u4ContextId,
                                      tIp6Addr * pFsipv6RouteDest,
                                      INT4 i4Fsipv6RoutePfxLength,
                                      INT4 i4Fsipv6RouteProtocol,
                                      tIp6Addr * pFsipv6RouteNextHop,
                                      tNetIpv6RtInfo * pNetIpv6RtInfo)
{
    tIp6RtEntry        *pProtoRtEntry = NULL;
    tIp6RtEntry        *pRtEntry = NULL;
    tIp6RtEntry        *pNextRtEntry = NULL;
    tIp6RtEntry        *pTmpRtEntry = NULL;
    VOID               *pRibNode = NULL;
    tIp6RtEntry         Ip6RtEntry;
    tRtm6Cxt           *pRtm6Cxt = NULL;

    pRtm6Cxt = UtilRtm6GetCxt (u4ContextId);

    if (pRtm6Cxt == NULL)
    {
        return RTM6_FAILURE;
    }

    if ((i4Fsipv6RoutePfxLength < 0) || (i4Fsipv6RouteProtocol < 0))
    {
        return (RTM6_FAILURE);
    }

    MEMSET (&Ip6RtEntry, 0, sizeof (tIp6RtEntry));
    MEMCPY (&Ip6RtEntry.dst, pFsipv6RouteDest, sizeof (tIp6Addr));
    Ip6RtEntry.u1Prefixlen = (UINT1) i4Fsipv6RoutePfxLength;
    Ip6RtEntry.i1Proto = 0;        /* Try and get the first route entry for this
                                 * prefix. */

    if (Rtm6TrieSearchExactEntryInCxt (pRtm6Cxt,
                                       &Ip6RtEntry, &pTmpRtEntry,
                                       &pRibNode) == RTM6_FAILURE)
    {
        /* Given route is not present. */
        if (Rtm6TrieGetFirstEntryInCxt (pRtm6Cxt, &pTmpRtEntry,
                                        &pRibNode) == RTM6_FAILURE)
        {
            return RTM6_FAILURE;
        }
    }
    /* Check whether the next route is present in the same node or not */
    while (1)
    {
        /* Find the protocol route. */
        for (pProtoRtEntry = pTmpRtEntry; pProtoRtEntry != NULL;
             pProtoRtEntry = pProtoRtEntry->pNextRtEntry)
        {
            if ((INT4) pProtoRtEntry->i1Proto >= i4Fsipv6RouteProtocol)
            {
                break;
            }
        }

        if (pProtoRtEntry == NULL)
        {
            /* No matching route. */
            break;
        }

        if ((INT4) pProtoRtEntry->i1Proto > i4Fsipv6RouteProtocol)
        {
            /* Matching route not present. But have found Next route. */
            pNextRtEntry = pProtoRtEntry;
            break;
        }
        pTmpRtEntry = NULL;
        for (pRtEntry = pProtoRtEntry; pRtEntry != NULL;
             pRtEntry = pRtEntry->pNextAlternatepath)
        {
            if (Ip6AddrMatch
                (&pRtEntry->nexthop, (tIp6Addr *) pFsipv6RouteNextHop,
                 IP6_ADDR_SIZE_IN_BITS) == TRUE)
            {
                pTmpRtEntry = pRtEntry->pNextAlternatepath;
                break;
            }
        }

        if (pTmpRtEntry != NULL)
        {
            pNextRtEntry = pTmpRtEntry;
        }
        else
        {
            /* No matching or next route from this protocol */
            pNextRtEntry = pProtoRtEntry->pNextRtEntry;
        }
        break;
    }

    if (pNextRtEntry == NULL)
    {
        /* No more route present in the given prefix. Get the next route from
         * TRIE */
        pTmpRtEntry = NULL;
        if (Rtm6TrieGetNextEntryInCxt (pRtm6Cxt,
                                       &Ip6RtEntry, &pNextRtEntry,
                                       &pRibNode) == RTM6_FAILURE)
        {
            /* Next route is not present. */
            return RTM6_FAILURE;
        }
    }

    Rtm6UtilCopyRtInfoInCxt (pRtm6Cxt, pNextRtEntry, pNetIpv6RtInfo);
    return RTM6_SUCCESS;
}

/******************************************************************************
* DESCRIPTION : This function finds number of free units which are currently 
*               free to add route entries
*
* INPUTS      : None
*                 
* OUTPUTS     : u4Count - Number of free blocks
*
* RETURNS     : None
*
* NOTES       : None
*               
******************************************************************************/

VOID
Rtm6ApiFreeRouteCount (UINT4 *u4Count)
{
    /* Get the number of free units currently available */
    *u4Count = MEM_FREE_POOL_UNIT_COUNT (gRtm6GlobalInfo.Rtm6RtTblPoolId);
    return;
}

/******************************************************************************
* DESCRIPTION : This function findsthe ipv6 route entry for a given 
*               destination, prefix, protocol and nexthop
*
** INPUTS      : pIp6Dest- Ipv6 route destination
                 u1PrefixLen - Prefix Length
                 i1Proto- routing protocol
                 pNextHop- Ipv6 nexthop

* OUTPUTS     : pNetIpv6RtInfo - Ipv6 route info
*
* RETURNS     : RTM6_SUCCESS/RTM6_FAILURE
*
* NOTES       : No RTM6_TASK_LOCK taken inside this function, modules who is 
*               calling this function should take the RTM6_TASK_LOCK and 
*               release the lock using RTM6_TASK_UNLOCK
******************************************************************************/

INT4
Rtm6ApiFindRtEntryInCxt (UINT4 u4ContextId, tIp6Addr * pIp6Dest,
                         UINT1 u1PrefixLen, INT1 i1Proto,
                         tIp6Addr * pNextHop, tNetIpv6RtInfo * pNetIpv6RtInfo)
{
    /* API for Ip6RouteFindEntry */
    tIp6RtEntry        *pIp6RtEntry = NULL;
    tIp6RtEntry         RtEntry;
    VOID               *pRibNode = NULL;
    tRtm6Cxt           *pRtm6Cxt = NULL;

    pRtm6Cxt = UtilRtm6GetCxt (u4ContextId);

    if (pRtm6Cxt == NULL)
    {
        return RTM6_FAILURE;
    }

    MEMSET (&RtEntry, 0, sizeof (tIp6RtEntry));
    MEMCPY (&RtEntry.dst, pIp6Dest, sizeof (tIp6Addr));
    RtEntry.u1Prefixlen = u1PrefixLen;
    RtEntry.i1Proto = i1Proto;

    if (Rtm6TrieSearchExactEntryInCxt (pRtm6Cxt, &RtEntry,
                                       &pIp6RtEntry, &pRibNode) == IP6_FAILURE)
    {
        /* No route from this protocol. */
        return RTM6_FAILURE;
    }

    for (; pIp6RtEntry != NULL; pIp6RtEntry = pIp6RtEntry->pNextAlternatepath)
    {
        if (MEMCMP (&pIp6RtEntry->nexthop, pNextHop, sizeof (tIp6Addr)) == 0)
        {
            /* Match route found. */
            break;
        }
    }

    if (pIp6RtEntry == NULL)
    {
        return RTM6_FAILURE;
    }

    Rtm6UtilCopyRtInfoInCxt (pRtm6Cxt, pIp6RtEntry, pNetIpv6RtInfo);
    return RTM6_SUCCESS;
}

/******************************************************************************
* DESCRIPTION : This function sets the IP6 route index, route tag and route 
*               metric
*
** INPUTS      : u4ContextId - RTM6 Context ID
                 pIp6Dest- Ipv6 route destination
                 u1PrefixLen - Prefix Length
                 i1Proto- routing protocol
                 pNextHop- Ipv6 nexthop
                 u1ObjType - Ip6 route parameters to set route index/route 
                 tag/route metric
                 u4ObjValue - Ip6 route parameter value

* OUTPUTS     : None 
*
* RETURNS     : RTM6_SUCCESS/RTM6_FAILURE
*
* NOTES       : No RTM6_TASK_LOCK taken inside this function, modules who is 
*               calling this function should take the RTM6_TASK_LOCK and 
*               release the lock using RTM6_TASK_UNLOCK
******************************************************************************/
INT4
Rtm6ApiSetRtParamsInCxt (UINT4 u4ContextId, tIp6Addr * pIp6Dest,
                         UINT1 u1PrefixLen, INT1 i1Proto,
                         tIp6Addr * pNextHop, UINT1 u1ObjType, UINT4 u4ObjValue)
{
    tIp6RtEntry        *pIp6RtEntry = NULL;
    tIp6RtEntry         RtEntry;
    VOID               *pRibNode = NULL;
    tRtm6Cxt           *pRtm6Cxt = NULL;

    pRtm6Cxt = UtilRtm6GetCxt (u4ContextId);

    if (pRtm6Cxt == NULL)
    {
        return RTM6_FAILURE;
    }

    MEMSET (&RtEntry, 0, sizeof (tIp6RtEntry));
    MEMCPY (&RtEntry.dst, pIp6Dest, sizeof (tIp6Addr));
    RtEntry.u1Prefixlen = u1PrefixLen;
    RtEntry.i1Proto = i1Proto;

    if (Rtm6TrieSearchExactEntryInCxt (pRtm6Cxt, &RtEntry,
                                       &pIp6RtEntry, &pRibNode) == IP6_FAILURE)
    {
        /* No route from this protocol. */
        return RTM6_FAILURE;
    }
    for (; pIp6RtEntry != NULL; pIp6RtEntry = pIp6RtEntry->pNextAlternatepath)
    {
        if (MEMCMP (&pIp6RtEntry->nexthop, pNextHop, sizeof (tIp6Addr)) == 0)
        {
            /* Match route found. */
            break;
        }
    }
    if (pIp6RtEntry == NULL)
    {
        /* No route present */
        return RTM6_FAILURE;
    }

    switch (u1ObjType)
    {
        case RTM6_ROUTE_INDEX:
            /* RouteIndex */
            pIp6RtEntry->u4Index = u4ObjValue;
            break;
        case RTM6_ROUTE_TYPE:
            /* Router Type */
            pIp6RtEntry->i1Type = (INT1) u4ObjValue;
            break;

        case RTM6_ROUTE_TAG:
            /* RouteTag */
            pIp6RtEntry->u4RouteTag = (UINT4) u4ObjValue;
            break;

        case RTM6_ROUTE_ADDR_TYPE:
            pIp6RtEntry->u1AddrType = (UINT1) u4ObjValue;
            break;

        default:
            break;
    }
    return RTM6_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : This routine is called to find a default route for the outgoing
 *               packet. This routine first tries to fetch a reachable or
 *               probably reachable default router. If none of the entries in
 *               the list are reachable it tries to get a default router in a
 *               round - robin fashion, so that all the routers are probed one
 *               after other.If the list is empty it returns NULL.
 *
 * INPUTS      : u4ContextId - RTM6 Context Id
 *
 * OUTPUTS     : pNetIp6RtInfo.
 *
 * RETURNS     : RTM6_SUCCESS/RTM6_FAILURE.
 *
 * NOTES       :
 ******************************************************************************/
INT4
Rtm6ApiDefRtLookupInCxt (UINT4 u4ContextId, tNetIpv6RtInfo * pNetIp6RtInfo)
{
    tIp6RtEntry        *pIp6RtEntry = NULL;
    tRtm6Cxt           *pRtm6Cxt = NULL;

    RTM6_TASK_LOCK ();
    pRtm6Cxt = UtilRtm6GetCxt (u4ContextId);

    if (pRtm6Cxt == NULL)
    {
        RTM6_TASK_UNLOCK ();
        return RTM6_FAILURE;
    }

    if (pNetIp6RtInfo == NULL)
    {
        RTM6_TASK_UNLOCK ();
        return RTM6_FAILURE;
    }

    pIp6RtEntry = Rtm6UtilDefRtLookupInCxt (pRtm6Cxt, NULL);

    if (pIp6RtEntry == NULL)
    {
        RTM6_TASK_UNLOCK ();
        return RTM6_FAILURE;
    }

    Rtm6UtilCopyRtInfoInCxt (pRtm6Cxt, pIp6RtEntry, pNetIp6RtInfo);
    RTM6_TASK_UNLOCK ();
    return RTM6_SUCCESS;
}

/******************************************************************************
 * Function           : Rtm6ApiIpv6LeakRoute
 * Input(s)           : u1CmdType - Command to ADD | Delete | Modify the Route.
 *                      pNetIp6RtInfo  - Route Information.
 * Output(s)          : None
 * Returns            : RTM6_SUCCESS or RTM6_FAILURE
 * Action             : This function invokes Rtm6Ipv6LeakRoute to modify 
 *                       the given route.
 ******************************************************************************/
INT4
Rtm6ApiIpv6LeakRoute (UINT1 u1CmdType, tNetIpv6RtInfo * pNetIp6RtInfo)
{
    INT4                i4RetValue = RTM6_FAILURE;

    i4RetValue = Rtm6Ipv6LeakRoute (u1CmdType, pNetIp6RtInfo);

    return i4RetValue;
}

/******************************************************************************
 * Function           : Rtm6Ip6IsFreeRtTblEntriesAvailable
 * Input(s)           : None
 *
 * Returns            : RTM6_SUCCESS or RTM6_FAILURE
 * Action             : This function is invokes to check whether rtm6 table
 *                      enrtries are maximum
 ******************************************************************************/
INT4
Rtm6Ip6IsFreeRtTblEntriesAvailable ()
{
    tRtm6Cxt           *pRtm6Cxt = NULL;
    UINT4               u4ContextId = 0;

    pRtm6Cxt = UtilRtm6GetCxt (u4ContextId);

    if (pRtm6Cxt == NULL)
    {
        return IP6_FAILURE;
    }

    if (pRtm6Cxt->u4Ip6FwdTblRouteNum < MAX_RTM6_ROUTE_TABLE_ENTRIES)
    {
        return IP6_SUCCESS;
    }
    else
    {
        return IP6_FAILURE;
    }
}

VOID
Rtm6ApiScanRouteTableForBestRouteInCxt (UINT4 u4ContextId,
                                        tIp6Addr * pIp6InAddr,
                                        UINT1 u1InAddrPrefixLen,
                                        INT4 (*pAppSpecFunc) (tIp6RtEntry *
                                                              pIp6RtEntry,
                                                              VOID
                                                              *pAppSpecData),
                                        UINT4 u4MaxRouteCnt,
                                        tIp6Addr * pIp6OutAddr,
                                        UINT1 *pu1OutAddrPrefixLen,
                                        VOID *pAppSpecData)
{
    tRtm6Cxt           *pRtm6Cxt = NULL;
    RTM6_TASK_LOCK ();
    if ((pRtm6Cxt = UtilRtm6GetCxt (u4ContextId)) == NULL)
    {
        RTM6_TASK_UNLOCK ();
        return;
    }

    Ip6ScanRouteTableForBestRouteInCxt (pRtm6Cxt, pIp6InAddr, u1InAddrPrefixLen,
                                        pAppSpecFunc, u4MaxRouteCnt,
                                        pIp6OutAddr, pu1OutAddrPrefixLen,
                                        pAppSpecData);

    RTM6_TASK_UNLOCK ();
    return;
}

/******************************************************************************
 * Function           : Rtm6ApiGetProtocolPrefInCxt
 * Input(s)           : u4ContextId  - Context Id                              
 *                      u4Proto      - Protocol Id for which preference is set
 * Output(s)          : pu4PrefValue - Preference value
 * Returns            : RTM6_SUCCESS or RTM6_FAILURE
 * Notes              : This function is invoked only by SNMP task after 
 *                       taking the RTM6 lock. Hence, the lock is not taken
 *                       here.
 ******************************************************************************/
INT4
Rtm6ApiGetProtocolPrefInCxt (UINT4 u4ContextId, UINT4 u4Proto,
                             UINT4 *pu4PrefValue)
{
    tRtm6Cxt           *pRtm6Cxt = NULL;

    pRtm6Cxt = UtilRtm6GetCxt (u4ContextId);
    if (pRtm6Cxt == NULL)
    {
        return RTM6_FAILURE;
    }
    *pu4PrefValue = pRtm6Cxt->au4RtPref[u4Proto - 1];
    return RTM6_SUCCESS;
}

/******************************************************************************
 * Function           : Rtm6ApiSetProtocolPrefInCxt
 * Input(s)           : u4ContextId  - Context Id                              
 *                      u4Proto      - Protocol Id for which preference is set
 *                      u4PrefValue  - Preference value
 * Output(s)          : None                             
 * Returns            : RTM6_SUCCESS or RTM6_FAILURE
 * Notes              : This function is invoked only by SNMP task after 
 *                       taking the RTM6 lock. Hence, the lock is not taken
 *                       here.
 ******************************************************************************/
INT4
Rtm6ApiSetProtocolPrefInCxt (UINT4 u4ContextId, UINT4 u4Proto,
                             UINT4 u4PrefValue)
{
    tRtm6Cxt           *pRtm6Cxt = NULL;

    pRtm6Cxt = UtilRtm6GetCxt (u4ContextId);
    if (pRtm6Cxt == NULL)
    {
        return RTM6_FAILURE;
    }
    pRtm6Cxt->au4RtPref[u4Proto - 1] = u4PrefValue;
    return RTM6_SUCCESS;
}

/******************************************************************************
 * Function           : Rtm6ApiAddDropRouteInNP
 * Description        : This function installs the given route as Drop route
 * Input(s)           : u4ContextId  - Context Id                              
 *                      tNetIpv6RtInfo - Route info
 * Output(s)          : None                             
 * Returns            : VOID
 ******************************************************************************/
VOID
Rtm6ApiAddDropRouteInNP (UINT4 u4ContextId, tNetIpv6RtInfo * pIp6RtInfo)
{
#ifdef NPAPI_WANTED
    tRtm6Cxt           *pRtm6Cxt = NULL;
    tFsNpIntInfo        IntInfo;
    tFsNpRouteInfo      RouteInfo;
    UINT4               u4NHType;
#endif

#ifndef NPAPI_WANTED
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (pIp6RtInfo);
    return;
#else

    RTM6_TASK_LOCK ();

    pRtm6Cxt = UtilRtm6GetCxt (u4ContextId);
    if (pRtm6Cxt == NULL)
    {
        RTM6_TASK_UNLOCK ();
        return;
    }

    MEMSET (&IntInfo, 0, sizeof (tFsNpIntInfo));

    IntInfo.u1IfType = Ip6GetIfType (pIp6RtInfo->u4Index);
    if ((IntInfo.u1IfType == CFA_L3IPVLAN)
        || (IntInfo.u1IfType == CFA_L3SUB_INTF))
    {
        IntInfo.u4PhyIfIndex = pIp6RtInfo->u4Index;
    }
    if (IntInfo.u1IfType == CFA_PSEUDO_WIRE)
    {
        IntInfo.u4PhyIfIndex = pIp6RtInfo->u4Index;
    }
    if (IntInfo.u1IfType != IP6_ENET_INTERFACE_TYPE)
    {
        if (CfaGetVlanId (IntInfo.u4PhyIfIndex, &(IntInfo.u2VlanId)) ==
            CFA_FAILURE)
        {
            RTM6_TASK_UNLOCK ();
            return;
        }
    }
    else
    {
        IntInfo.u4PhyIfIndex = pIp6RtInfo->u4Index;
        IntInfo.u2VlanId = 0;
    }

    /* Remove existing local route and re install the route 
     * as a drop route */
    u4NHType = NH_DISCARD;
    if (RTM6_IS_NP_PROGRAMMING_ALLOWED () == RTM6_SUCCESS)
    {
        /*Passing ECMP count to HW inorder to program ECMProutes */
        RouteInfo.u1RouteCount = pIp6RtInfo->u1EcmpCount;
        RouteInfo.u4HwIntfId[0] = pIp6RtInfo->u4HwIntfId[0];
        Ipv6FsNpIpv6UcRouteDelete (pRtm6Cxt->u4ContextId,
                                   (UINT1 *) &pIp6RtInfo->Ip6Dst,
                                   pIp6RtInfo->u1Prefixlen,
                                   (UINT1 *) &pIp6RtInfo->NextHop,
                                   pIp6RtInfo->u4Index, &RouteInfo);

        /*Passing ECMP count to HW inorder to program ECMProutes */
        IntInfo.u1RouteCount = pIp6RtInfo->u1EcmpCount;
        if (Ipv6FsNpIpv6UcRouteAdd (pRtm6Cxt->u4ContextId,
                                    (UINT1 *) &pIp6RtInfo->Ip6Dst,
                                    pIp6RtInfo->u1Prefixlen,
                                    (UINT1 *) &pIp6RtInfo->NextHop,
                                    u4NHType, &IntInfo) == FNP_FAILURE)
        {
            RTM6_TRC (pRtm6Cxt->u4ContextId, RTM6_ALL_FAILURE_TRC |
                      RTM6_OS_RESOURCE_TRC, RTM6_MOD_NAME,
                      "\tNP Route Addition Failed \n");
        }
        pIp6RtInfo->u4HwIntfId[0] = IntInfo.u4HwIntfId[0];
        RTM6_TRC_ARG2 (u4ContextId, RTM6_CTRL_PATH_TRC, RTM6_MOD_NAME,
                       "RTM6: Drop route added - "
                       "Network %s, Next Hop %s\r\n",
                       Ip6PrintAddr (&pIp6RtInfo->Ip6Dst),
                       Ip6PrintAddr (&pIp6RtInfo->NextHop));
    }

    RTM6_TASK_UNLOCK ();
    return;
#endif /* NPAPI_WANTED */
}

/******************************************************************************
 * Function           : Rtm6ApiAddLocalRouteInNP  
 * Description        : This function install the routes with given
 *                      unresolved next hop as local route
 * Input(s)           : u4ContextId  - Context ID
 *                      pNextHopIpAddr - Next hop IP address
 *
 * Output(s)          : NONE 
 * Returns            : VOID
 ******************************************************************************/
VOID
Rtm6ApiAddLocalRouteInNP (UINT4 u4ContextId, tIp6Addr * pNextHopIpAddr)
{
    tRNh6Node          *pRNH6Entry = NULL, ExstNh6Node;
    tPNh6Node          *pPNH6Entry = NULL;
    tPRt6Entry         *pPRt6 = NULL, *pPRt6NextEntry = NULL;
    tRtm6Cxt           *pRtm6Cxt = NULL;
    tIp6RtEntry        *pIp6RtEntry = NULL;
    tNetIpv6RtInfo      NetIp6RtInfo;
#ifdef NPAPI_WANTED
    UINT4               u4NHType = 0;
    tFsNpIntInfo        IntInfo;
    tFsNpRouteInfo      RouteInfo;
    MEMSET (&IntInfo, 0, sizeof (tFsNpIntInfo));
#else
    UNUSED_PARAM (pIp6RtEntry);
#endif

    MEMSET (&NetIp6RtInfo, 0, sizeof (tNetIpv6RtInfo));
    MEMSET (&ExstNh6Node, 0, sizeof (tRNh6Node));

    RTM6_TASK_LOCK ();
    pRtm6Cxt = UtilRtm6GetCxt (u4ContextId);
    if (pRtm6Cxt == NULL)
    {
        RTM6_TASK_UNLOCK ();
        return;
    }

    ExstNh6Node.pRtm6Cxt = pRtm6Cxt;
    Ip6AddrCopy (&(ExstNh6Node.NextHop), pNextHopIpAddr);

    /* Check if already the entry exists in the Resolved Next hop entry 
     * If so, skip this step */
    pRNH6Entry = RBTreeGet (gRtm6GlobalInfo.pRNHT6RBRoot, &ExstNh6Node);

    if (pRNH6Entry == NULL)
    {
        /* Check if the entry exists in Pend Next Hop table, if it exists
         * check for drop-route flag and restore it as local route */

        pPNH6Entry = RBTreeGet (gRtm6GlobalInfo.pPRT6RBRoot, &ExstNh6Node);
        if (pPNH6Entry != NULL)
        {
            TMO_DYN_SLL_Scan (&(pPNH6Entry->routeEntryList), pPRt6,
                              pPRt6NextEntry, tPRt6Entry *)
            {
                pIp6RtEntry = pPRt6->pPend6Rt;
#ifdef NPAPI_WANTED
                if ((IP6_ROUTE_TYPE_DISCARD == pIp6RtEntry->i1Type)
                    && (NP_PRESENT == pIp6RtEntry->u1HwStatus))
                {
                    Rtm6FillIntInfo (pIp6RtEntry, &IntInfo, &u4NHType);
                    pIp6RtEntry->i1Type = IP6_ROUTE_TYPE_INDIRECT;
                    if (RTM6_IS_NP_PROGRAMMING_ALLOWED () == RTM6_SUCCESS)
                    {
                        /*Passing ECMP count to HW inorder to program ECMProutes */
                        IntInfo.u1RouteCount = pIp6RtEntry->u1EcmpCount;
                        RouteInfo.u1RouteCount = pIp6RtEntry->u1EcmpCount;
                        RouteInfo.u4HwIntfId[0] = pIp6RtEntry->u4HwIntfId[0];
                        Ipv6FsNpIpv6UcRouteDelete (pRtm6Cxt->u4ContextId,
                                                   (UINT1 *) &(pIp6RtEntry->
                                                               dst),
                                                   pIp6RtEntry->u1Prefixlen,
                                                   (UINT1 *) &(pIp6RtEntry->
                                                               nexthop),
                                                   pIp6RtEntry->u4Index,
                                                   &RouteInfo);
                        if (Ipv6FsNpIpv6UcRouteAdd (pRtm6Cxt->u4ContextId,
                                                    (UINT1 *) &(pIp6RtEntry->
                                                                dst),
                                                    pIp6RtEntry->u1Prefixlen,
                                                    (UINT1 *) &(pIp6RtEntry->
                                                                nexthop),
                                                    u4NHType,
                                                    &IntInfo) == FNP_FAILURE)
                        {
                            RTM6_TRC (pRtm6Cxt->u4ContextId,
                                      RTM6_ALL_FAILURE_TRC |
                                      RTM6_OS_RESOURCE_TRC, RTM6_MOD_NAME,
                                      "\tNP Route Addition Failed \n");
                            if (Rtm6FrtAddInfo
                                (pIp6RtEntry, pRtm6Cxt->u4ContextId,
                                 (UINT1) u4NHType) == RTM6_SUCCESS)
                            {
                                Rtm6RedSyncFrtInfo (pIp6RtEntry,
                                                    pRtm6Cxt->u4ContextId,
                                                    (UINT1) u4NHType,
                                                    RTM6_ADD_ROUTE);

                            }
                            else
                            {
                                RTM6_TRC_ARG2 (pRtm6Cxt->u4ContextId,
                                               RTM6_ALL_FAILURE_TRC |
                                               RTM6_OS_RESOURCE_TRC,
                                               RTM6_MOD_NAME,
                                               "ERROR[RTM]: IPV6  Failed Route Addition Failed%s/%d   \n",
                                               Ip6PrintNtop (&pIp6RtEntry->dst),
                                               (INT4) pIp6RtEntry->u1Prefixlen);
                            }
                        }
                        pIp6RtEntry->u4HwIntfId[0] = IntInfo.u4HwIntfId[0];

                        RTM6_TRC_ARG2 (u4ContextId, RTM6_CTRL_PATH_TRC,
                                       RTM6_MOD_NAME,
                                       "RTM6: Drop route Re-added as Local Route - "
                                       "Network %s, Next Hop %s\r\n",
                                       Ip6PrintAddr (&pIp6RtEntry->dst),
                                       Ip6PrintAddr (&pIp6RtEntry->nexthop));
                        RTM6_TASK_UNLOCK ();
                        return;
                    }
                }                /* if Discard */
#endif
            }                    /* DLL scan */
        }
    }
    RTM6_TASK_UNLOCK ();
    return;
}

#ifdef NPAPI_WANTED
/******************************************************************************
 * Function           : Rtm6FillIntInfo
 * Description        : This function installs the given route as Drop route
 * Input(s)           : pIp6RtInfo  - The route
 *
 * Output(s)          : pIntInfo - The interface info
 *                      pu4NHType - Next Hop type
 * Returns            : RTM6_SUCCESS or RTM6_FAILURE
 ******************************************************************************/
VOID
Rtm6FillIntInfo (tIp6RtEntry * pIp6RtInfo, tFsNpIntInfo * pIntInfo,
                 UINT4 *pu4NHType)
{

    MEMSET (pIntInfo, 0, sizeof (tFsNpIntInfo));

    pIntInfo->u1IfType = Ip6GetIfType (pIp6RtInfo->u4Index);
    if ((pIntInfo->u1IfType == CFA_L3IPVLAN)
        || (pIntInfo->u1IfType == CFA_L3SUB_INTF))
    {
        pIntInfo->u4PhyIfIndex = pIp6RtInfo->u4Index;
    }
    if (pIntInfo->u1IfType == CFA_PSEUDO_WIRE)
    {
        pIntInfo->u4PhyIfIndex = pIp6RtInfo->u4Index;
    }
    if (pIntInfo->u1IfType != IP6_ENET_INTERFACE_TYPE)
    {
        if (CfaGetVlanId (pIntInfo->u4PhyIfIndex, &(pIntInfo->u2VlanId)) ==
            CFA_FAILURE)
        {
            return;
        }
    }
    else
    {
        pIntInfo->u4PhyIfIndex = pIp6RtInfo->u4Index;
        pIntInfo->u2VlanId = 0;
    }

    /* check for the type before programming */
    if (pIp6RtInfo->i1Type == IP6_ROUTE_TYPE_DIRECT)
    {
        *pu4NHType = NH_DIRECT;
    }
    else if (pIp6RtInfo->i1Type == IP6_ROUTE_TYPE_DISCARD)
    {
        /* Route type if set as discard type previously is
         * now reverted to DIRECT Type */
        *pu4NHType = NH_DIRECT;
    }
    else if (pIp6RtInfo->i1Type == IP6_ROUTE_TYPE_INDIRECT)
    {
        *pu4NHType = NH_REMOTE;
    }
    else
    {
        *pu4NHType = NH_SENDTO_CP;
    }
}
#endif /* NPAPI_WANTED */
/******************************************************************************
 * Function           : Rtm6ApiAddOrDelAllRtsInCxt
 * Description        : This function programs or deletes the route entries in 
 *                      Forwarding plane based on the IPV6 Forwarding enable/disable
 * Input(s)           : u4ContextId  - Context Id                              
 *                      i4ForwardStatus - IPv6 Forwarding status enable/disable
 * Output(s)          : None                             
 * Returns            : None
 ******************************************************************************/
VOID
Rtm6ApiAddOrDelAllRtsInCxt (UINT4 u4ContextId, INT4 i4ForwardStatus)
{
#ifdef NPAPI_WANTED
    tRtm6Cxt           *pRtm6Cxt = NULL;
    tIp6RtEntry        *pIp6RtEntry = NULL;
    tIp6RtEntry        *pNextIp6RtEntry = NULL;
    tIp6RtEntry        *pIp6BestRtEntry = NULL;
    tFsNpIntInfo        IntInfo;
    tFsNpRouteInfo      RouteInfo;
#ifdef TUNNEL_WANTED
    tTnlIfEntry         TnlIfEntry;
    UINT4               u4Addr;
#endif
    UINT4               u4NHType;
    VOID               *pRibNode = NULL;
    INT4                i4RetValue = RTM6_FAILURE;
#endif

#ifndef NPAPI_WANTED
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (i4ForwardStatus);
    return;
#else

    RTM6_TASK_LOCK ();

    pRtm6Cxt = UtilRtm6GetCxt (u4ContextId);
    if (pRtm6Cxt == NULL)
    {
        RTM6_TASK_UNLOCK ();
        return;
    }

    /* Get the first route. */
    if (Rtm6TrieGetFirstEntryInCxt (pRtm6Cxt,
                                    &pIp6RtEntry, &pRibNode) == RTM6_FAILURE)
    {
        RTM6_TASK_UNLOCK ();
        return;
    }

    do
    {
        pIp6BestRtEntry = NULL;
        if (pIp6RtEntry->u1AddrType == ADDR6_ANYCAST)
        {
            i4RetValue =
                Rtm6UtilFwdTblGetBestAnycastRouteInCxt (pRtm6Cxt, pIp6RtEntry,
                                                        &pIp6BestRtEntry);
        }
        else
        {
            i4RetValue =
                Rtm6UtilFwdTblGetBestRouteInCxt (pRtm6Cxt, pIp6RtEntry,
                                                 &pIp6BestRtEntry);
        }
        if (i4RetValue == IP6_SUCCESS)
        {
            if (i4ForwardStatus == IP6_FORW_ENABLE)
            {
                MEMSET (&IntInfo, 0, sizeof (tFsNpIntInfo));

                IntInfo.u1IfType = Ip6GetIfType (pIp6BestRtEntry->u4Index);
                if ((IntInfo.u1IfType == CFA_L3IPVLAN)
                    || (IntInfo.u1IfType == CFA_L3SUB_INTF))
                {
                    IntInfo.u4PhyIfIndex = pIp6BestRtEntry->u4Index;
                }
                if (IntInfo.u1IfType == CFA_PSEUDO_WIRE)
                {
                    IntInfo.u4PhyIfIndex = pIp6BestRtEntry->u4Index;
                }

#ifdef TUNNEL_WANTED
                else if (IntInfo.u1IfType == CFA_TUNNEL)
                {
                    MEMSET (&TnlIfEntry, 0, sizeof (tTnlIfEntry));

                    /* fetch the Tunnel interface's Physical Interface index */
                    if (CfaGetTnlEntryFromIfIndex
                        (pIp6BestRtEntry->u4Index, &TnlIfEntry) == CFA_FAILURE)
                    {
                        continue;
                    }
                    IntInfo.u4PhyIfIndex = TnlIfEntry.u4PhyIfIndex;
                    IntInfo.TunlInfo.tunlSrc.u4_addr[IP6_THREE] =
                        TnlIfEntry.LocalAddr.Ip4TnlAddr;
                    IntInfo.TunlInfo.u1TunlType = TnlIfEntry.u4EncapsMethod;
                    IntInfo.TunlInfo.u1TunlFlag = TnlIfEntry.u1DirFlag;
                    IntInfo.TunlInfo.u1TunlDir = TnlIfEntry.u1Direction;

                    if ((TnlIfEntry.u4EncapsMethod == IPV6_SIX_TO_FOUR) ||
                        (TnlIfEntry.u4EncapsMethod == IPV6_ISATAP_TUNNEL) ||
                        (TnlIfEntry.u4EncapsMethod == IPV6_AUTO_COMPAT))
                    {
                        if (IpGetIpv4AddrFromArpTable
                            (IntInfo.u4PhyIfIndex, &u4Addr) == IP_FAILURE)
                        {
                            continue;
                        }
                        IntInfo.TunlInfo.tunlDst.u4_addr[IP6_THREE] = u4Addr;
                    }

                    if ((TnlIfEntry.u4EncapsMethod == IPV6_OVER_IPV4_TUNNEL) ||
                        (TnlIfEntry.u4EncapsMethod == IPV6_GRE_TUNNEL))
                    {
                        IntInfo.TunlInfo.tunlDst.u4_addr[IP6_THREE] =
                            TnlIfEntry.RemoteAddr.Ip4TnlAddr;
                    }

                }
#endif
                if ((IntInfo.u1IfType != IP6_ENET_INTERFACE_TYPE) &&
                    (IntInfo.u1IfType != IP6_LOOPBACK_INTERFACE_TYPE))
                {
                    if (CfaGetVlanId (IntInfo.u4PhyIfIndex, &(IntInfo.u2VlanId))
                        == CFA_FAILURE)
                    {
                        continue;
                    }
                }
                else
                {
                    IntInfo.u4PhyIfIndex = pIp6BestRtEntry->u4Index;
                    IntInfo.u2VlanId = 0;
                }
                /* New route is Added */
                if (pIp6BestRtEntry->i1Type == IP6_ROUTE_TYPE_DIRECT)
                {
                    u4NHType = NH_DIRECT;
                }
                else if (pIp6BestRtEntry->i1Type == IP6_ROUTE_TYPE_DISCARD)
                {
                    u4NHType = NH_DISCARD;
                }
                else if (pIp6BestRtEntry->i1Type == IP6_ROUTE_TYPE_INDIRECT)
                {
                    u4NHType = NH_REMOTE;
                }
                else
                {
                    /* Unknown Route Type. */
                    u4NHType = NH_SENDTO_CP;
                }
#ifdef TUNNEL_WANTED
                if (IntInfo.u1IfType == IP6_TUNNEL_INTERFACE_TYPE)
                {
                    u4NHType = NH_TUNNEL;
                }
#endif

                if (RTM6_GET_NODE_STATUS () == RM_ACTIVE)
                {
                    pIp6BestRtEntry->u1HwStatus = NP_NOT_PRESENT;
                    Rtm6RedAddDynamicInfo (pIp6BestRtEntry,
                                           pRtm6Cxt->u4ContextId);
                    Rtm6RedSendDynamicInfo ();
                }
                if (RTM6_IS_NP_PROGRAMMING_ALLOWED () == RTM6_SUCCESS)
                {
#ifdef LNXIP6_WANTED
                    if ((NetIpv6GetIpForwardingInCxt (u4ContextId) ==
                         IP6_FORW_ENABLE)
                        || (pIp6BestRtEntry->i1Proto == CIDR_LOCAL_ID))
#endif
                        /*Passing ECMP count to HW inorder to program ECMProutes */
                        IntInfo.u1RouteCount = pIp6BestRtEntry->u1EcmpCount;
                    if (Ipv6FsNpIpv6UcRouteAdd (pRtm6Cxt->u4ContextId,
                                                (UINT1 *) &pIp6BestRtEntry->dst,
                                                pIp6BestRtEntry->u1Prefixlen,
                                                (UINT1 *) &pIp6BestRtEntry->
                                                nexthop, u4NHType,
                                                &IntInfo) == FNP_FAILURE)
                    {
                        RTM6_TRC (pRtm6Cxt->u4ContextId, RTM6_ALL_FAILURE_TRC |
                                  RTM6_OS_RESOURCE_TRC, RTM6_MOD_NAME,
                                  "\tNP Route Addition Failed \n");
                        if (Rtm6FrtAddInfo
                            (pIp6BestRtEntry, pRtm6Cxt->u4ContextId,
                             (UINT1) u4NHType) == RTM6_SUCCESS)
                        {
                            Rtm6RedSyncFrtInfo (pIp6BestRtEntry,
                                                pRtm6Cxt->u4ContextId,
                                                (UINT1) u4NHType,
                                                RTM6_ADD_ROUTE);

                        }
                        else
                        {
                            RTM6_TRC_ARG2 (pRtm6Cxt->u4ContextId,
                                           RTM6_ALL_FAILURE_TRC |
                                           RTM6_OS_RESOURCE_TRC, RTM6_MOD_NAME,
                                           "ERROR[RTM]: IPV6  Failed Route Addition Failed%s/%d   \n",
                                           Ip6PrintNtop (&pIp6BestRtEntry->dst),
                                           (INT4) pIp6BestRtEntry->u1Prefixlen);
                        }
                    }
                    pIp6BestRtEntry->u4HwIntfId[0] = IntInfo.u4HwIntfId[0];
                }
                if (RTM6_GET_NODE_STATUS () == RM_ACTIVE)
                {
                    pIp6BestRtEntry->u1HwStatus = NP_PRESENT;
                    Rtm6RedAddDynamicInfo (pIp6BestRtEntry,
                                           pRtm6Cxt->u4ContextId);
                    Rtm6RedSendDynamicInfo ();
                }
            }
            else
            {
                /* Forwarding Status disabled - IP6_FORW_ENABLE */
                if (RTM6_GET_NODE_STATUS () == RM_ACTIVE)
                {
                    pIp6BestRtEntry->u1HwStatus = NP_PRESENT;
                    Rtm6RedAddDynamicInfo (pIp6BestRtEntry,
                                           pRtm6Cxt->u4ContextId);
                    Rtm6RedSendDynamicInfo ();
                }
                if (RTM6_IS_NP_PROGRAMMING_ALLOWED () == RTM6_SUCCESS)
                {
#ifdef LNXIP6_WANTED
                    if ((NetIpv6GetIpForwardingInCxt (u4ContextId) ==
                         IP6_FORW_ENABLE)
                        || (pIp6BestRtEntry->i1Proto != CIDR_LOCAL_ID))
#endif
                        /*Passing ECMP count to HW inorder to program ECMProutes */
                        RouteInfo.u1RouteCount = pIp6BestRtEntry->u1EcmpCount;
                    RouteInfo.u4HwIntfId[0] = pIp6BestRtEntry->u4HwIntfId[0];
                    Ipv6FsNpIpv6UcRouteDelete (pRtm6Cxt->u4ContextId,
                                               (UINT1 *) &pIp6BestRtEntry->dst,
                                               pIp6BestRtEntry->u1Prefixlen,
                                               (UINT1 *) &pIp6BestRtEntry->
                                               nexthop,
                                               pIp6BestRtEntry->u4Index,
                                               &RouteInfo);
                }
                if (RTM6_GET_NODE_STATUS () == RM_ACTIVE)
                {
                    pIp6BestRtEntry->u1HwStatus = NP_NOT_PRESENT;
                    Rtm6RedAddDynamicInfo (pIp6BestRtEntry,
                                           pRtm6Cxt->u4ContextId);
                    Rtm6RedSendDynamicInfo ();
                }
            }

        }
        if (Rtm6TrieGetNextEntryInCxt (pRtm6Cxt,
                                       pIp6RtEntry, &pNextIp6RtEntry,
                                       &pRibNode) == RTM6_FAILURE)
        {
            /* No more route present. Set the cookie to indicate it */
            break;
        }
        pIp6RtEntry = pNextIp6RtEntry;
        pNextIp6RtEntry = NULL;
    }
    while (pIp6RtEntry != NULL);

    RTM6_TASK_UNLOCK ();
    return;
#endif /* NPAPI_WANTED */
}

/******************************************************************************
 * FUNCTION NAME    :  ClearInetCidrIpv6Route
 *
 * DESCRIPTION      : This function clear the Stats of the
 *                    InetCidrIpv6Route
 *
 * INPUT            : u4ContextId
 *
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 ******************************************************************************/
VOID
ClearInetCidrIpv6Route (UINT4 u4ContextId)
{

    tRtm6Cxt           *pRtm6Cxt = NULL;
    INT4                i4Result = 0;

    i4Result = Ip6SelectContext (u4ContextId);
    pRtm6Cxt = UtilRtm6GetCxt (u4ContextId);
    if (pRtm6Cxt == NULL)
    {
        return;
    }
    RTM6_TASK_LOCK ();
    pRtm6Cxt->u4Ip6FwdTblRouteNum = 0;
    RTM6_TASK_UNLOCK ();
    UNUSED_PARAM (i4Result);
    return;

}

/******************************************************************************
 * FUNCTION NAME    :  Rtm6TstPopulateInetCidrIpv6Route
 *
 * DESCRIPTION      : This function clear the Stats of the
 *                    InetCidrIpv6Route
 *
 * INPUT            : u4ContextId
 *
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 ******************************************************************************/
VOID
Rtm6TstPopulateInetCidrIpv6Route (UINT4 u4ContextId)
{

    tRtm6Cxt           *pRtm6Cxt = NULL;
    UINT4               u4Value = 5;
    INT4                i4Result = 0;

    i4Result = Ip6SelectContext (u4ContextId);
    pRtm6Cxt = UtilRtm6GetCxt (u4ContextId);
    if (pRtm6Cxt == NULL)
    {
        return;
    }
    RTM6_TASK_LOCK ();
    pRtm6Cxt->u4Ip6FwdTblRouteNum = u4Value;
    RTM6_TASK_UNLOCK ();
    UNUSED_PARAM (i4Result);
    return;

}

/******************************************************************************
 * FUNCTION NAME    :  Rtm6GetBestRouteEntryInCxt
 *
 * DESCRIPTION      : This function gets the best route entry for the given 
 *                    destination
 *
 * INPUT            : u4RtmCxtId - RTM6 Context Id 
 *                    InRtInfo - Route Entry that contains destination and prefix len info
 *
 *
 * OUTPUT           : ppOutRtInfo - Best route entry
 * RETURNS          : None
 *
 *
 ******************************************************************************/
INT4
Rtm6GetBestRouteEntryInCxt (UINT4 u4RtmCxtId,
                            tIp6RtEntry * pInRtInfo, tIp6RtEntry ** ppOutRtInfo)
{
    INT4                i4RetVal = IP_FAILURE;
    tRtm6Cxt           *pRtm6Cxt = NULL;

    pRtm6Cxt = UtilRtm6GetCxt (u4RtmCxtId);
    if (pRtm6Cxt == NULL)
    {
        return i4RetVal;
    }
    i4RetVal =
        Ip6GetBestRouteEntryInCxt (pRtm6Cxt, &(pInRtInfo->dst),
                                   pInRtInfo->u1Prefixlen, ppOutRtInfo);
    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : Rtm6GetShowCmdOutputAndCalcChkSum                     */
/*                                                                           */
/* Description        : This funcion handles the execution of show commands  */
/*                      and calculation of checksum with the output.         */
/*                      The calculated value is then being sent to RM.       */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : pu2SwAudChkSum                                       */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : RTM6_SUCESS/RTM6_FAILURE                             */
/*****************************************************************************/
INT4
Rtm6GetShowCmdOutputAndCalcChkSum (UINT2 *pu2SwAudChkSum)
{
#if (defined CLI_WANTED && defined RM_WANTED)

    if (RTM6_GET_NODE_STATUS () == RM_ACTIVE)
    {
        if (Rtm6CliGetShowCmdOutputToFile ((UINT1 *) RTM6_AUDIT_FILE_ACTIVE) !=
            RTM6_SUCCESS)
        {
            RTM6_TRC (RTM6_DEFAULT_CXT_ID, RTM6_ALL_FAILURE_TRC,
                      RTM6_MOD_NAME, "\tGetShRunFile Failed\n");
            return RTM6_FAILURE;
        }
        if (Rtm6CliCalcSwAudCheckSum
            ((UINT1 *) RTM6_AUDIT_FILE_ACTIVE, pu2SwAudChkSum) != RTM6_SUCCESS)
        {
            RTM6_TRC (RTM6_DEFAULT_CXT_ID, RTM6_ALL_FAILURE_TRC,
                      RTM6_MOD_NAME, "\tCalcSwAudChkSum Failed\n");
            return RTM6_FAILURE;
        }
    }
    else if (RTM6_GET_NODE_STATUS () == RM_STANDBY)
    {
        if (Rtm6CliGetShowCmdOutputToFile ((UINT1 *) RTM6_AUDIT_FILE_STDBY) !=
            RTM6_SUCCESS)
        {
            RTM6_TRC (RTM6_DEFAULT_CXT_ID, RTM6_ALL_FAILURE_TRC,
                      RTM6_MOD_NAME, "\tGetShRunFile Failed\n");
            return RTM6_FAILURE;
        }
        if (Rtm6CliCalcSwAudCheckSum
            ((UINT1 *) RTM6_AUDIT_FILE_STDBY, pu2SwAudChkSum) != RTM6_SUCCESS)
        {
            RTM6_TRC (RTM6_DEFAULT_CXT_ID, RTM6_ALL_FAILURE_TRC,
                      RTM6_MOD_NAME, "\tCalcSwAudChkSum Failed\n");
            return RTM6_FAILURE;
        }
    }
    else
    {
        return RTM6_FAILURE;
    }
#else
    UNUSED_PARAM (pu2SwAudChkSum);
#endif
    return RTM6_SUCCESS;
}

/************************************************************************
 *  Function Name   : Rtm6UtilCheckIsRouteGreater
 *  Description     : An util to check whether the first route is
 *                    greater than the second route.
 *  Input           : pIp6RtInfo1 - First route
 *                  : pIp6RtInfo2 - Second route
 *  Output          : returns RTM6_SUCCESS if the pIp6RtInfo1 > pIp6RtInfo2.
 *                    returns RTM6_FAILURE otherwise.
 *
 *  Returns         : RTM6_SUCCESS or RTM6_FAILURE 
 ************************************************************************/
INT1
Rtm6UtilCheckIsRouteGreater (tNetIpv6RtInfo * pIp6RtInfo1,
                             tNetIpv6RtInfo * pIp6RtInfo2)
{
    /* Check destination address */
    if (Ip6AddrCompare (pIp6RtInfo1->Ip6Dst, pIp6RtInfo2->Ip6Dst) ==
        IP6_RB_GREATER)
    {
        return RTM6_SUCCESS;
    }
    if (Ip6AddrCompare (pIp6RtInfo1->Ip6Dst, pIp6RtInfo2->Ip6Dst) ==
        IP6_RB_LESSER)
    {
        return RTM6_FAILURE;
    }

    /* Check Prefix Length */
    if (pIp6RtInfo1->u1Prefixlen > pIp6RtInfo2->u1Prefixlen)
    {
        return RTM6_SUCCESS;
    }
    if (pIp6RtInfo1->u1Prefixlen < pIp6RtInfo2->u1Prefixlen)
    {
        return RTM6_FAILURE;
    }

    /* Check next hop */
    if (Ip6AddrCompare (pIp6RtInfo1->NextHop, pIp6RtInfo2->NextHop) ==
        IP6_RB_GREATER)
    {
        return RTM6_SUCCESS;
    }
    if (Ip6AddrCompare (pIp6RtInfo1->NextHop, pIp6RtInfo2->NextHop) ==
        IP6_RB_LESSER)
    {
        return RTM6_FAILURE;
    }
    return RTM6_FAILURE;
}

/*****************************************************************************
 * Function     : Rtm6ApiHandleProtocolStatusInCxt                            *
 * Description  : Posts event to RTM of given type                           *
 * Input        : u4ContextId - RTM Context ID                                * 
 *                u1Protocol  - Proto ID                                     *
 *                u1Msgtype   - type of message to be posted to RTM          *
 *                u4IfIndex   - Interface index                              *
 *                u1BitMask   - Bitmask to be registered with RTM            *
 *                pSendToApplication - Callback Function for handling message*
 *                                    from RTM6                              *
 *
 * Output       : None                                                       *
 * Returns      : RTM6_SUCCESS/RTM6_FAILURE                                   *
 *****************************************************************************/
PUBLIC INT4
Rtm6ApiHandleProtocolStatusInCxt (UINT4 u4ContextId,
                                  UINT1 u1Protocol, UINT1 u1Msgtype,
                                  UINT4 u4IfIndex, UINT1 u1BitMask,
                                  VOID *pSendToApplication, INT1 i1GrFlag)
{

    tOsixMsg           *pRtm6Msg = NULL;
    tRtm6MsgHdr        *pRtm6MsgHdr = NULL;

    if ((pRtm6Msg =
         CRU_BUF_Allocate_MsgBufChain (sizeof (tRtm6MsgHdr), 0)) == NULL)
    {
        UtlTrcLog (1, 1, "",
                   "INFO[RTM]: Route Entry Message Allocation for Rtm6ApiHandleProtocolStatusInCxt failure - Processing the RTM QUEUE Entries\n");
        return RTM6_SUCCESS;
    }

    pRtm6MsgHdr = (tRtm6MsgHdr *) CRU_BUF_Get_ModuleData (pRtm6Msg);

    /* fill the message header */
    pRtm6MsgHdr->u1MessageType = u1Msgtype;
    pRtm6MsgHdr->RegnId.u4ContextId = u4ContextId;
    pRtm6MsgHdr->RegnId.u2ProtoId = (UINT2) u1Protocol;
    pRtm6MsgHdr->u1BitMask = u1BitMask;
    pRtm6MsgHdr->u4Index = u4IfIndex;
    pRtm6MsgHdr->pDeliverToApplication = pSendToApplication;
    pRtm6MsgHdr->i1GrFlag = i1GrFlag;

    if (OsixSendToQ (SELF,
                     (const UINT1 *) RTM6_MESSAGE_ARRIVAL_Q_NAME,
                     pRtm6Msg, OSIX_MSG_NORMAL) != OSIX_SUCCESS)
    {
        IP6_RELEASE_BUF (pRtm6Msg, FALSE);
        UtlTrcLog (1, 1, "",
                   "ERROR[RTM]: Route Entry Message for Rtm6ApiHandleProtocolStatusInCxt Updation Send to Queue Failure\n");
        return (RTM6_FAILURE);
    }
    if (OsixSendEvent (SELF, (const UINT1 *) RTM6_MAIN_TASK_NAME,
                       RTM6_PROTOCOL_STATUS_EVENT) != OSIX_SUCCESS)
    {
        return RTM6_FAILURE;
    }

    UNUSED_PARAM (u4ContextId);
    return RTM6_SUCCESS;
}
