/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: rtm6rmap.c,v 1.12 2017/12/01 13:08:20 siva Exp $
*
* Description:This file contains functions which process   
*             the messages from the routing protocols.     
*
*******************************************************************/

#include "rtm6inc.h"
#include "rmap.h"

/**************************************************************************/
/*   Function Name   : Rtm6CheckIsRouteMapConfiguredInCxt                 */
/*   Description     : This function checks whether RouteMap is configured*/
/*                     for the given destionation RP                      */
/*   Input(s)        : pRtm6Cxt - RTM6 Context Pointer                    */
/*                   : u2Proto - Destination RP                           */
/*   Output(s)       : None                                               */
/*   Return Value    : RTM6_SUCCESS if Route Map configured               */
/*                     RTM6_FAILURE otherwise                             */
/**************************************************************************/
INT4
Rtm6CheckIsRouteMapConfiguredInCxt (tRtm6Cxt * pRtm6Cxt, UINT2 u2Proto)
{
    UINT2               u2RegnId = 0;

    for (u2RegnId = pRtm6Cxt->u2Rtm6RtStartIndex;
         u2RegnId < IP6_MAX_ROUTING_PROTOCOLS;
         u2RegnId = pRtm6Cxt->aRtm6RegnTable[u2RegnId].u2NextRegId)
    {
        if ((pRtm6Cxt->aRtm6RegnTable[u2RegnId].u2RoutingProtocolId == u2Proto)
            && (STRLEN (pRtm6Cxt->aRtm6RegnTable[u2RegnId].au1RMapName) != 0))
        {
            /* Route Map configured */
            return RTM6_SUCCESS;
        }
    }
    /* Route Map not configured */
    return RTM6_FAILURE;
}

/**************************************************************************/
/*   Function Name   : Rtm6ResetRouteMapBitMaskInCxt                      */
/*   Description     : This function checks whether RouteMap is configured*/
/*                     for the given destionation RP, if its configured   */
/*                     resets the Bit Mask, so that filter rules cannot be*/
/*                     applied on the Destination RP where RouteMap is    */
/*                     enabled                                            */
/*   Input(s)        : pRtm6Cxt - RTM6 Context Pointer                    */
/*                   : pu2DestProtoMask - Destination ProtoMask for the   */
/*                     RPs for Filter rule is configured                  */
/*   Output(s)       : None                                               */
/*   Return Value    : None                                               */
/**************************************************************************/
VOID
Rtm6ResetRouteMapBitMaskInCxt (tRtm6Cxt * pRtm6Cxt, UINT2 *pu2DestProtoMask)
{
    UINT2               u2Cnt = 0;
    UINT2               u2BitMask = RTM6_MASK_STABLE;

    for (u2Cnt = 0; u2Cnt < IP6_MAX_ROUTING_PROTOCOLS; u2Cnt++)
    {
        if ((u2BitMask & *pu2DestProtoMask) &&
            (STRLEN (pRtm6Cxt->aRtm6RegnTable[u2Cnt].au1RMapName) != 0))
        {
            RTM6_CLEAR_BIT (*pu2DestProtoMask, (u2Cnt + 1));
        }
        u2BitMask = (UINT2) (u2BitMask << 1);
    }
}

/**************************************************************************/
/*   Function Name   : Rtm6SetRouteMapBitMaskInCxt                        */
/*   Description     : This function checks whether RouteMap is configured*/
/*                     for the given destionation RP, if its configured   */
/*                     sets the Bit Mask                                  */
/*   Input(s)        : pRtm6Cxt - RTM6 Context Pointer                    */
/*                   : pu2DestProtoMask - Destination ProtoMask for the   */
/*                     RPs for Filter rule is configured                  */
/*   Output(s)       : None                                               */
/*   Return Value    : None                                               */
/**************************************************************************/
VOID
Rtm6SetRouteMapBitMaskInCxt (tRtm6Cxt * pRtm6Cxt, UINT2 *pu2DestProtoMask)
{
    UINT2               u2Cnt = 0;
    UINT2               u2BitMask = 0x01;
    *pu2DestProtoMask = 0;

    for (u2Cnt = 0; u2Cnt < IP6_MAX_ROUTING_PROTOCOLS; u2Cnt++)
    {
        if (STRLEN (pRtm6Cxt->aRtm6RegnTable[u2Cnt].au1RMapName) != 0)
        {
            RTM6_SET_BIT (*pu2DestProtoMask, (u2Cnt + 1));
        }
        u2BitMask = (UINT2) (u2BitMask << 1);
    }
}

/**************************************************************************/
/*   Function Name   : Rtm6ApplyRouteMapRuleAndRedistributeInCxt           */
/*   Description     : This function checks whether RouteMap is configured */
/*                     for the given destionation RP, If configured calls */
/*                     the routeMap module to apply the rule              */
/*   Input(s)        : pRtm6Cxt - RTM6 Context Pointer                    */
/*                   : pRegnId  - Destination Routing Protocols           */
/*                     registration ID                                    */
/*                     pRtInfo - Route in which Route Map Rule should be  */
/*                     applied and redistributed                          */
/*                     u1IsRtBest - Info whether the route is best or not */
/*                     u2ChgBit   - Info about the Route attributes whose */
/*                                   value have been changed.             */
/*                     u2DestProtoMask - Protocols to which the route is  */
/*                                      either redistributed or withdrawn */
/*   Output(s)       : None                                               */
/*   Return Value    : None                                               */
/**************************************************************************/
VOID
Rtm6ApplyRouteMapRuleAndRedistributeInCxt (tRtm6Cxt * pRtm6Cxt,
                                           UINT2 u2SrcProtoId,
                                           tIp6RtEntry * pRtInfo,
                                           UINT1 u1IsRtBest, UINT2 u2ChgBit,
                                           UINT2 u2DstProtoMask)
{
#ifndef ROUTEMAP_WANTED
    UNUSED_PARAM (pRtm6Cxt);
    UNUSED_PARAM (u2SrcProtoId);
    UNUSED_PARAM (pRtInfo);
    UNUSED_PARAM (u1IsRtBest);
    UNUSED_PARAM (u2ChgBit);
    UNUSED_PARAM (u2DstProtoMask);
#else
    tIp6RtEntry         OutRtInfo;
    UINT2               u2TmpMask = 0;
    UINT2               u2RegnId = 0;
    UINT1               u1Status = 0;
    tRtMapInfo          InMapInfo, OutMapInfo;
    UNUSED_PARAM (u2SrcProtoId);

    MEMSET (&OutRtInfo, 0, sizeof (tIp6RtEntry));
    /* This route is to be redistributed only to these mib enabled
     *  * routing protocols.  */
    for (u2RegnId = pRtm6Cxt->u2Rtm6RtStartIndex;
         u2RegnId < IP6_MAX_ROUTING_PROTOCOLS;
         u2RegnId = pRtm6Cxt->aRtm6RegnTable[u2RegnId].u2NextRegId)
    {
        /* if rmap exist for this proto */
        /* if this proto is in dest mask */
        /* if this is not src proto */
        if ((STRLEN (pRtm6Cxt->aRtm6RegnTable[u2RegnId].au1RMapName) != 0) &&
            ((RTM6_TEST_BIT (u2DstProtoMask,
                             pRtm6Cxt->aRtm6RegnTable[u2RegnId].
                             u2RoutingProtocolId)) != 0))

        {
            /* prepare mask */
            u2TmpMask = 0;
            RTM6_SET_BIT (u2TmpMask,
                          pRtm6Cxt->aRtm6RegnTable[u2RegnId].
                          u2RoutingProtocolId);

            /* convert structure */
            RMapTIp6RtEntry2TRtMapInfo (&InMapInfo, pRtInfo, 0);

            /* Release RTM6_TASK_LOCK to avoid deadlock 
             * during RMap trap generation */
            RTM6_TASK_UNLOCK ();

            /* modify structure by applying MATCH/SET, get result PERMIT/DENY */
            u1Status = RMapApplyRule (&InMapInfo, &OutMapInfo,
                                      pRtm6Cxt->aRtm6RegnTable[u2RegnId].
                                      au1RMapName);

            /* Take RTM6_TASK_LOCK again */
            RTM6_TASK_LOCK ();

            /* these two structure should be the same */
            MEMCPY (&OutRtInfo, pRtInfo, sizeof (tIp6RtEntry));
            /* modify some route attributes, preserve others */
            RMapTRtMapInfo2TIp6RtEntry (&OutRtInfo, &OutMapInfo);

            if (u1Status == RMAP_ROUTE_PERMIT)
            {
                Rtm6RouteRedistributionInCxt
                    (pRtm6Cxt, &OutRtInfo, u1IsRtBest, u2ChgBit, u2TmpMask);

                /* ?? */
                pRtInfo->u2RedisMask = OutRtInfo.u2RedisMask;
            }

        }
    }
#endif /*ROUTEMAP_WANTED */
}

/**************************************************************************/
/*   Function Name   : Rtm6ProcessRouteMapChanges                          */
/*   Description     : This function handles Route Map update msg from    */
/*                     Route Map module, with the given route map name,   */
/*                     it gets the Rotuing protocol which are registered  */
/*                     for this Route Map, and applies this Route Map rule*/
/*                     on all the routes in RTM and decides wether this   */
/*                     should redistributed or withdrawn                  */
/*   Input(s)        : pu1RMapName - Route Map Name                       */
/*   Output(s)       : None                                               */
/*   Return Value    : None                                               */
/**************************************************************************/
VOID
Rtm6ProcessRouteMapChanges (UINT1 *pu1RMapName)
{
#ifndef ROUTEMAP_WANTED
    UNUSED_PARAM (pu1RMapName);
    return;
#else
    UINT2               u2RegnId;
    tIp6Addr            InAddr;
    tIp6Addr            OutAddr;
    UINT1               u1InPrefixLen;
    UINT1               u1OutPrefixLen;
    tRtm6RegnId         RegnId;
    tRtm6Cxt           *pRtm6Cxt = NULL;
    UINT4               u4ContextId;
    UINT4               u4NextContextId;

    if (UtilRtm6GetFirstCxtId (&u4ContextId) != RTM6_FAILURE)
    {
        pRtm6Cxt = UtilRtm6GetCxt (u4ContextId);
    }

    while (pRtm6Cxt != NULL)
    {

        /* clear input ip6-adr, prefix-len */
        MEMSET (&InAddr, 0, sizeof (tIp6Addr));
        u1InPrefixLen = 0;

        /* clear output ip6-adr, prefix-len for any case */
        MEMSET (&OutAddr, 0, sizeof (tIp6Addr));
        u1OutPrefixLen = 0;

        /* scan route table */
        for (u2RegnId = pRtm6Cxt->u2Rtm6RtStartIndex;
             u2RegnId < IP6_MAX_ROUTING_PROTOCOLS;
             u2RegnId = pRtm6Cxt->aRtm6RegnTable[u2RegnId].u2NextRegId)
        {
            /* if redistribution with rmap is configured - apply rmap */
            if ((STRLEN (pRtm6Cxt->aRtm6RegnTable[u2RegnId].au1RMapName) != 0)
                &&
                (STRCMP
                 (pu1RMapName,
                  pRtm6Cxt->aRtm6RegnTable[u2RegnId].au1RMapName) == 0))
            {
                /*ROUTE_TBL_LOCK (); !!! */

                /* This function will take care of collecting the route
                 * to be redistributed and sending the update message to RP's */
                MEMSET (&RegnId, 0, sizeof (tRtm6RegnId));
                RegnId.u2ProtoId =
                    pRtm6Cxt->aRtm6RegnTable[u2RegnId].u2RoutingProtocolId;
                RegnId.u4ContextId = pRtm6Cxt->u4ContextId;
                Ip6ScanRouteTableForBestRouteInCxt (pRtm6Cxt,
                                                    &InAddr, u1InPrefixLen,
                                                    Rtm6HandleRouteMapChgsInRts,
                                                    0, &OutAddr,
                                                    &u1OutPrefixLen,
                                                    (VOID *) &RegnId
                                                    /*2nd param of callback */
                    );

                /*ROUTE_TBL_UNLOCK (); !!! */

            }
        }
        pRtm6Cxt = NULL;

        if (UtilRtm6GetNextCxtId (u4ContextId, &u4NextContextId) !=
            RTM6_FAILURE)
        {
            pRtm6Cxt = UtilRtm6GetCxt (u4NextContextId);
            u4ContextId = u4NextContextId;
        }

    }                            /* Scan for all the RTM context */
    return;
#endif /*ROUTEMAP_WANTED */
}

/**************************************************************************/
/*   Function Name   : Rtm6HandleRouteMapChgsInRts                        */
/*   Description     : This function handles Route Map update msg from    */
/*                     Route Map module, with the given route map name,   */
/*                     it gets the Rotuing protocol which are registered  */
/*                     for this Route Map, and applies this Route Map rule*/
/*                     on all the routes in RTM and decides wether this   */
/*                     should redistributed or withdrqwn                  */
/*                                                                        */
/*   Input(s)        : pRtInfo - Route in which changed route map rule    */
/*                               should be applied                        */
/*                     pAppSpecData - Destination Routing Protocol's Regn */
/*                     Id                                                 */
/*   Output(s)       : None                                               */
/*   Return Value    : RTM6_SUCCESS/RTM6_FAILURE                          */
/**************************************************************************/
INT4
Rtm6HandleRouteMapChgsInRts (tIp6RtEntry * pRtInfo, VOID *pAppSpecData)
{
#ifndef ROUTEMAP_WANTED
    UNUSED_PARAM (pRtInfo);
    UNUSED_PARAM (pAppSpecData);
    return RTM6_SUCCESS;
#else
    tRtm6RegnId        *pRtm6RegnId = (tRtm6RegnId *) pAppSpecData;
    tRtm6Cxt           *pRtm6Cxt = NULL;
    UINT2               u2RegnId;
    UINT2               u2DestProtoMask = 0;
    UINT2               u2SendProtoMask = 0;
    UINT1               u1Status;
    tRtMapInfo          InMapInfo, OutMapInfo;

    if ((pRtm6Cxt = UtilRtm6GetCxt (pRtm6RegnId->u4ContextId)) == NULL)
    {
        return RTM6_FAILURE;
    }

    /* convert structure */
    if ((u2RegnId = Rtm6GetRegnIndexFromRegnIdInCxt (pRtm6Cxt, pRtm6RegnId))
        == RTM6_INVALID_REGN_ID)
    {
        return RTM6_FAILURE;
    }
    RMapTIp6RtEntry2TRtMapInfo (&InMapInfo, pRtInfo, 0);

    /* Release RTM6_TASK_LOCK to avoid deadlock 
     * during RMap trap generation */
    RTM6_TASK_UNLOCK ();

    /* modify structure by applying MATCH/SET, get result PERMIT/DENY */
    u1Status = RMapApplyRule
        (&InMapInfo, &OutMapInfo,
         pRtm6Cxt->aRtm6RegnTable[u2RegnId].au1RMapName);

    /* Take RTM6_TASK_LOCK again */
    RTM6_TASK_LOCK ();
    /* modify some route attributes, preserve others */
    RMapTRtMapInfo2TIp6RtEntry (pRtInfo, &OutMapInfo);

    if (u1Status == RMAP_ROUTE_PERMIT)
    {
        RTM6_SET_BIT (u2DestProtoMask,
                      pRtm6Cxt->aRtm6RegnTable[u2RegnId].u2RoutingProtocolId);
    }
    else if (u1Status == RMAP_ROUTE_DENY)
    {
        u2DestProtoMask = RTM6_DONT_REDISTRIBUTE;
    }

    if (u2DestProtoMask != RTM6_DONT_REDISTRIBUTE)
    {
        /* Route can be redistributed to the protocols specified by
         * u2DestProtoMask. Check whether the route is already
         * redistributed to these protocols or not. If not redistributed
         * then send it. If already redistributed and cannot be
         * redistributed now, then withdraw it. */
        u2SendProtoMask = u2DestProtoMask &
            (~(u2DestProtoMask & pRtInfo->u2RedisMask));

        if (u2SendProtoMask != 0)
        {
            /* Redistribute the route to these protocols */
            Rtm6RouteRedistributionInCxt (pRtm6Cxt,
                                          pRtInfo, TRUE, IP6_BIT_ALL,
                                          u2SendProtoMask);
        }
    }
    else
    {
        /* Route need not be redistributed as per current Route Map rule.
         * Check whether the route is already redistributed to any other
         * protocol. If yes, the withdraw them. */
        if (pRtInfo->u2RedisMask != 0)
        {
            Rtm6RouteRedistributionInCxt (pRtm6Cxt,
                                          pRtInfo, FALSE, IP6_BIT_ALL,
                                          pRtInfo->u2RedisMask);
        }
    }
    return RTM6_SUCCESS;
#endif /*ROUTEMAP_WANTED */
}

/*****************************************************************************/
/* Function     : Rtm6SendRouteMapUpdateMsg                                  */
/*                                                                           */
/* Description  : This function processes Route Map Update Message from      */
/*                Route Map module, this function posts an event to RTM6     */
/*                                                                           */
/* Input        : pu1RMapName - RouteMap Name                                */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RTM_SUCCESS if Route Map update Msg Sent to RTM            */
/*                successfully                                               */
/*                RTM_FAILURE otherwise                                      */
/*****************************************************************************/
INT4
Rtm6SendRouteMapUpdateMsg (UINT1 *pu1RMapName, UINT4 u4Status)
{
#ifndef ROUTEMAP_WANTED
    UNUSED_PARAM (pu1RMapName);
    UNUSED_PARAM (u4Status);
    return RTM6_SUCCESS;
#else
    tIpBuf             *pBuf;
    tRtm6MsgHdr        *pHdr;
    tRtm6MsgHdr         RtmHdr;
    INT4                i4OutCome;
    UINT4               u4Size;
    UINT1               au1NameBuf[RMAP_MAX_NAME_LEN + IP6_FOUR];
    UINT2               u2Len = 0;

    pHdr = &RtmHdr;
    u4Size = RMAP_MAX_NAME_LEN + sizeof (u4Status);    /* message size is hdr+ size of
                                                     * RouteMap Name+Status */

    /* enshure there is no garbage behind map name */
    MEMSET (au1NameBuf, 0, sizeof (au1NameBuf));
    u2Len = (UINT2) (STRLEN (pu1RMapName) < (RMAP_MAX_NAME_LEN + IP6_FOUR - 1) ?
                     STRLEN (pu1RMapName) : (RMAP_MAX_NAME_LEN + IP6_FOUR - 1));
    STRNCPY (au1NameBuf, pu1RMapName, u2Len);
    au1NameBuf[u2Len] = '\0';
    pBuf = IP6_ALLOCATE_BUF (u4Size, 0);
    if (pBuf == NULL)
    {
        return RTM6_FAILURE;
    }

    /*** copy message to be sent to buffer ***/
    pHdr = (tRtm6MsgHdr *) IP6_GET_MODULE_DATA_PTR (pBuf);

    pHdr->u1MessageType = RTM6_ROUTEMAP_UPDATE_MSG;
    pHdr->u2MsgLen = (UINT2) u4Size;

    /*** copy status to offset 0  ***/
    if ((i4OutCome = IP6_COPY_TO_BUF (pBuf, &u4Status, 0,
                                      sizeof (u4Status))) != CRU_SUCCESS)
    {

        IP6_RELEASE_BUF (pBuf, FALSE);
        return RTM6_FAILURE;
    }

    /*** copy map name to offset 4  ***/
    if ((i4OutCome = IP6_COPY_TO_BUF (pBuf, au1NameBuf, sizeof (u4Status),
                                      RMAP_MAX_NAME_LEN)) != CRU_SUCCESS)
    {

        IP6_RELEASE_BUF (pBuf, FALSE);
        return RTM6_FAILURE;
    }

    /*** post to RTM queue ***/
    i4OutCome = RpsEnqueuePktToRtm6 (pBuf);

    return i4OutCome;
#endif /*ROUTEMAP_WANTED */
}
