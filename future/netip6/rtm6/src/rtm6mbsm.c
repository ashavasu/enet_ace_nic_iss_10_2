/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rtm6mbsm.c,v 1.11 2017/12/19 13:41:56 siva Exp $
 *                  
 *
 *******************************************************************/

#ifdef MBSM_WANTED                /* IP6_CHASSIS_MERGE */
#include "rtm6inc.h"

/*****************************************************************************/
/*                                                                           */
/* Function     : Rtm6MbsmUpdateCardStatus                                   */
/*                                                                           */
/* Description  : Allocates a CRU buffer and enqueues the line card          */
/*                change status to the RTM6 task                             */
/*                                                                           */
/* Input        : pProtoMsg - Contains the slot and port information         */
/*                i1Status  - Line card Up/Down status                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : MBSM_SUCCESS or MBSM_FAILURE                               */
/*                                                                           */
/*****************************************************************************/

INT4
Rtm6MbsmUpdateCardStatus (tMbsmProtoMsg * pProtoMsg, UINT1 u1Cmd)
{
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    tRtm6MsgHdr        *pRtm6MsgHdr = NULL;

    pBuf = IP6_ALLOCATE_BUF ((sizeof (tMbsmProtoMsg)), 0);
    if (pBuf == NULL)
    {
        MBSM_DBG (MBSM_PROTO_TRC, MBSM_RTM6, "CRU Buffer Allocation Failed\n");
        return MBSM_FAILURE;
    }

    if (IP6_COPY_TO_BUF (pBuf, (UINT1 *) pProtoMsg, 0,
                         sizeof (tMbsmProtoMsg)) != CRU_SUCCESS)
    {
        IP6_RELEASE_BUF (pBuf, FALSE);
        MBSM_DBG (MBSM_PROTO_TRC, MBSM_RTM6, "Copy Over CRU Buffer Failed\n");
        return MBSM_FAILURE;
    }

    pRtm6MsgHdr = (tRtm6MsgHdr *) CRU_BUF_Get_ModuleData (pBuf);
    pRtm6MsgHdr->u1MessageType = u1Cmd;

    if (OsixSendToQ (SELF,
                     (const UINT1 *) RTM6_MESSAGE_ARRIVAL_Q_NAME,
                     pBuf, OSIX_MSG_NORMAL) != OSIX_SUCCESS)
    {
        IP6_RELEASE_BUF (pBuf, FALSE);
        MBSM_DBG (MBSM_PROTO_TRC, MBSM_RTM6, "Send To IP6 Q Failed\n");
        return MBSM_FAILURE;
    }

    OsixSendEvent (SELF, (const UINT1 *) RTM6_MAIN_TASK_NAME,
                   RTM6_MESSAGE_ARRIVAL_EVENT);

    return MBSM_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rtm6MbsmUpdateRtTable                                      */
/*                                                                           */
/* Description  : Updates the route table in the NP, based on the            */
/*                line card status received                                  */
/*                                                                           */
/* Input        : pBuf     - Buffer containing the protocol message info.    */
/*                u1Status - Line card status (UP/DOWN)                      */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

VOID
Rtm6MbsmUpdateRtTable (tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 u1Cmd)
{
    INT4                i4RetStatus = 0;
    UINT4               u4NHType = 0;
    tMbsmSlotInfo      *pSlotInfo = NULL;
    VOID               *pRibNode = NULL;
    tIp6RtEntry        *pFRte = NULL;
    tIp6RtEntry         PRte;
    tMbsmProtoMsg       ProtoMsg;
    tMbsmProtoAckMsg    ProtoAckMsg;
    tFsNpIntInfo        IntInfo;
    tRtm6Cxt           *pRtm6Cxt = NULL;
#ifdef TUNNEL_WANTED
    tTnlIfEntry         TnlIfEntry;
#endif /* TUNNEL_WANTED */
    UINT4               u4ContextId;
    UINT4               u4NextContextId;

    MEMSET (&ProtoMsg, 0, sizeof (tMbsmProtoMsg));
    MEMSET (&ProtoAckMsg, 0, sizeof (tMbsmProtoAckMsg));
    MEMSET (&IntInfo, 0, sizeof (tFsNpIntInfo));

    if ((IP6_COPY_FROM_BUF (pBuf, (UINT1 *) &ProtoMsg, 0,
                            sizeof (tMbsmProtoMsg))) == CRU_FAILURE)
    {
        IP6_RELEASE_BUF (pBuf, FALSE);
        MBSM_DBG (MBSM_PROTO_TRC, MBSM_RTM6, "Copy From CRU Buffer Failed\n");
        i4RetStatus = MBSM_FAILURE;
    }

    if (i4RetStatus != MBSM_FAILURE)
    {
        pSlotInfo = &(ProtoMsg.MbsmSlotInfo);

        if ((u1Cmd == MBSM_MSG_CARD_INSERT) &&
            (!MBSM_SLOT_INFO_ISPORTMSG (pSlotInfo)))
        {

            /* Get the first valid context ID and RTM context pointer */
            if (UtilRtm6GetFirstCxtId (&u4NextContextId) != RTM6_FAILURE)
            {
                pRtm6Cxt = UtilRtm6GetCxt (u4NextContextId);
            }

            while (pRtm6Cxt != NULL)
            {

                Rtm6TrieGetFirstEntryInCxt (pRtm6Cxt, &pFRte, &pRibNode);
                if ((pFRte != NULL) && (pRibNode != NULL))
                {
                    MEMSET (&PRte, 0, sizeof (tIp6RtEntry));
                    do
                    {
                        IntInfo.u1IfType = Ip6GetIfType (pFRte->u4Index);
                        if ((IntInfo.u1IfType == CFA_L3IPVLAN)
                            || (IntInfo.u1IfType == CFA_L3SUB_INTF))
                        {
                            IntInfo.u4PhyIfIndex = pFRte->u4Index;
                        }
#ifdef TUNNEL_WANTED
                        else if (IntInfo.u1IfType == CFA_TUNNEL)
                        {
                            MEMSET (&TnlIfEntry, 0, sizeof (tTnlIfEntry));
                            /* fetch the Tunnel interface's Physical Intf index */
                            if (CfaGetTnlEntryFromIfIndex (pFRte->u4Index,
                                                           &TnlIfEntry) ==
                                CFA_FAILURE)
                            {
                                i4RetStatus = MBSM_FAILURE;
                            }
                            IntInfo.u4PhyIfIndex = TnlIfEntry.u4PhyIfIndex;
                            IntInfo.TunlInfo.tunlSrc.u4_addr[IP6_THREE] =
                                TnlIfEntry.LocalAddr.Ip4TnlAddr;
                            IntInfo.TunlInfo.tunlDst.u4_addr[IP6_THREE] =
                                TnlIfEntry.RemoteAddr.Ip4TnlAddr;
                            IntInfo.TunlInfo.u1TunlType =
                                TnlIfEntry.u4EncapsMethod;
                            IntInfo.TunlInfo.u1TunlFlag = TnlIfEntry.u1DirFlag;
                            IntInfo.TunlInfo.u1TunlDir = TnlIfEntry.u1Direction;
                        }
#endif
                        if (IntInfo.u1IfType != IP6_ENET_INTERFACE_TYPE)
                        {
                            if (CfaGetVlanId (IntInfo.u4PhyIfIndex,
                                              &(IntInfo.u2VlanId)) ==
                                CFA_FAILURE)
                            {
                                i4RetStatus = MBSM_FAILURE;
                            }
                        }
                        else
                        {
                            IntInfo.u4PhyIfIndex = pFRte->u4Index;
                            IntInfo.u2VlanId = 0;
                        }
#ifdef TUNNEL_WANTED
                        if (Ip6GetIfType (pFRte->u4Index) ==
                            IP6_TUNNEL_INTERFACE_TYPE)
                        {
                            u4NHType = NH_TUNNEL;
                        }
                        else
#endif
                        if (pFRte->i1Type == IP6_ROUTE_TYPE_DIRECT)
                        {
                            u4NHType = NH_DIRECT;
                        }
                        else if (pFRte->i1Type == IP6_ROUTE_TYPE_DISCARD)
                        {
                            u4NHType = NH_DISCARD;
                        }
                        else if (pFRte->i1Type == IP6_ROUTE_TYPE_INDIRECT)
                        {
                            u4NHType = NH_REMOTE;
                        }
                        else
                        {
                            u4NHType = NH_SENDTO_CP;
                        }

                        if (Ipv6FsNpMbsmIpv6UcRouteAdd
                            (pRtm6Cxt->u4ContextId, ((UINT1 *) (&(pFRte->dst))),
                             pFRte->u1Prefixlen,
                             ((UINT1 *) (&(pFRte->nexthop))), u4NHType,
                             &IntInfo, pSlotInfo) == FNP_FAILURE)
                        {
                            i4RetStatus = MBSM_FAILURE;
                        }

                        MEMCPY (&PRte, pFRte, sizeof (tIp6RtEntry));
                    }
                    while (Rtm6TrieGetNextEntryInCxt
                           (pRtm6Cxt, &PRte, &pFRte,
                            &pRibNode) != RTM6_FAILURE);
                }
                u4ContextId = u4NextContextId;
                pRtm6Cxt = NULL;
                /* Get the next valid RTM context pointer */
                if (UtilRtm6GetNextCxtId (u4ContextId, &u4NextContextId) !=
                    RTM6_FAILURE)
                {
                    pRtm6Cxt = UtilRtm6GetCxt (u4NextContextId);
                }

            }                    /* End of RTM6 Context Loop */

        }
        else
        {
            MBSM_DBG (MBSM_PROTO_TRC, MBSM_RTM6,
                      "MBSM_CARD_REMOVE - No Support\n");
            i4RetStatus = MBSM_SUCCESS;
        }
    }

    ProtoAckMsg.i4RetStatus = i4RetStatus;
    ProtoAckMsg.i4SlotId = pSlotInfo->i4SlotId;
    ProtoAckMsg.i4ProtoCookie = ProtoMsg.i4ProtoCookie;
    MbsmSendAckFromProto (&ProtoAckMsg);
    IP6_RELEASE_BUF (pBuf, FALSE);
}
#endif
