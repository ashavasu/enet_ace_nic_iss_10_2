/*  $Id: fsrtm6wr.c,v 1.7 2015/10/19 12:22:10 siva Exp $ */
# include  "lr.h"
# include  "fssnmp.h"
# include  "fsrtmlow.h"
# include  "fsrtm6wr.h"
# include  "fsrtm6db.h"
#include   "rtm6inc.h"

VOID
RegisterFSRTM6 ()
{
    SNMPRegisterMibWithContextIdAndLock (&fsrtm6OID, &fsrtm6Entry, Rtm6Lock,
                                         Rtm6UnLock, UtilRtm6SetContext,
                                         UtilRtm6ResetContext,
                                         SNMP_MSR_TGR_FALSE);
    SNMPAddSysorEntry (&fsrtm6OID, (const UINT1 *) "fsrtm6");
}

VOID
UnRegisterFSRTM6 ()
{
    SNMPUnRegisterMib (&fsrtm6OID, &fsrtm6Entry);
    SNMPDelSysorEntry (&fsrtm6OID, (const UINT1 *) "fsrtm6");
}
INT4 FsRrd6MaximumBgpRoutesGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetFsRrd6MaximumBgpRoutes(&(pMultiData->u4_ULongValue)));
}
INT4 FsRrd6MaximumOspfRoutesGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetFsRrd6MaximumOspfRoutes(&(pMultiData->u4_ULongValue)));
}
INT4 FsRrd6MaximumRipRoutesGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetFsRrd6MaximumRipRoutes(&(pMultiData->u4_ULongValue)));
}
INT4 FsRrd6MaximumStaticRoutesGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetFsRrd6MaximumStaticRoutes(&(pMultiData->u4_ULongValue)));
}
INT4 FsRrd6MaximumISISRoutesGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetFsRrd6MaximumISISRoutes(&(pMultiData->u4_ULongValue)));
}

INT4 FsRtm6StaticRouteDistanceGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    return(nmhGetFsRtm6StaticRouteDistance(&(pMultiData->i4_SLongValue)));
}

INT4
FsRrd6RouterIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsRrd6RouterId (&(pMultiData->u4_ULongValue)));
}

INT4
FsRrd6FilterByOspfTagGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsRrd6FilterByOspfTag (&(pMultiData->i4_SLongValue)));
}

INT4
FsRrd6FilterOspfTagGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsRrd6FilterOspfTag (&(pMultiData->i4_SLongValue)));
}

INT4
FsRrd6FilterOspfTagMaskGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsRrd6FilterOspfTagMask (&(pMultiData->i4_SLongValue)));
}

INT4
FsRrd6RouterASNumberGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsRrd6RouterASNumber (&(pMultiData->i4_SLongValue)));
}

INT4
FsRrd6AdminStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsRrd6AdminStatus (&(pMultiData->i4_SLongValue)));
}

INT4
FsRrd6TraceGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsRrd6Trace (&(pMultiData->u4_ULongValue)));
}


INT4 FsRrd6MaximumBgpRoutesSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhSetFsRrd6MaximumBgpRoutes(pMultiData->u4_ULongValue));
}


INT4 FsRrd6MaximumOspfRoutesSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhSetFsRrd6MaximumOspfRoutes(pMultiData->u4_ULongValue));
}


INT4 FsRrd6MaximumRipRoutesSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhSetFsRrd6MaximumRipRoutes(pMultiData->u4_ULongValue));
}


INT4 FsRrd6MaximumStaticRoutesSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhSetFsRrd6MaximumStaticRoutes(pMultiData->u4_ULongValue));
}


INT4 FsRrd6MaximumISISRoutesSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhSetFsRrd6MaximumISISRoutes(pMultiData->u4_ULongValue));
}

INT4 FsRtm6StaticRouteDistanceSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    return(nmhSetFsRtm6StaticRouteDistance(pMultiData->i4_SLongValue));
}

INT4
FsRrd6ThrotLimitGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsRrd6ThrotLimit (&(pMultiData->u4_ULongValue)));
}

INT4
FsRrd6RouterIdSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsRrd6RouterId (pMultiData->u4_ULongValue));
}

INT4
FsRrd6FilterByOspfTagSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsRrd6FilterByOspfTag (pMultiData->i4_SLongValue));
}

INT4
FsRrd6FilterOspfTagSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsRrd6FilterOspfTag (pMultiData->i4_SLongValue));
}

INT4
FsRrd6FilterOspfTagMaskSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsRrd6FilterOspfTagMask (pMultiData->i4_SLongValue));
}

INT4
FsRrd6RouterASNumberSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsRrd6RouterASNumber (pMultiData->i4_SLongValue));
}

INT4
FsRrd6AdminStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsRrd6AdminStatus (pMultiData->i4_SLongValue));
}

INT4
FsRrd6TraceSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsRrd6Trace (pMultiData->u4_ULongValue));
}

INT4
FsRrd6ThrotLimitSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsRrd6ThrotLimit (pMultiData->u4_ULongValue));
}

INT4
FsRrd6RouterIdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                    tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsRrd6RouterId (pu4Error, pMultiData->u4_ULongValue));
}

INT4
FsRrd6FilterByOspfTagTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsRrd6FilterByOspfTag
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsRrd6FilterOspfTagTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsRrd6FilterOspfTag (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsRrd6FilterOspfTagMaskTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsRrd6FilterOspfTagMask
            (pu4Error, pMultiData->i4_SLongValue));
}


INT4 FsRrd6MaximumBgpRoutesTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhTestv2FsRrd6MaximumBgpRoutes(pu4Error, pMultiData->u4_ULongValue));
}


INT4 FsRrd6MaximumOspfRoutesTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhTestv2FsRrd6MaximumOspfRoutes(pu4Error, pMultiData->u4_ULongValue));
}


INT4 FsRrd6MaximumRipRoutesTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhTestv2FsRrd6MaximumRipRoutes(pu4Error, pMultiData->u4_ULongValue));
}


INT4 FsRrd6MaximumStaticRoutesTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhTestv2FsRrd6MaximumStaticRoutes(pu4Error, pMultiData->u4_ULongValue));
}


INT4 FsRrd6MaximumISISRoutesTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhTestv2FsRrd6MaximumISISRoutes(pu4Error, pMultiData->u4_ULongValue));
}

INT4 FsRtm6StaticRouteDistanceTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    return(nmhTestv2FsRtm6StaticRouteDistance(pu4Error, pMultiData->i4_SLongValue));
}


INT4
FsRrd6RouterASNumberTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsRrd6RouterASNumber
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsRrd6AdminStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsRrd6AdminStatus (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsRrd6TraceTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                 tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsRrd6Trace (pu4Error, pMultiData->u4_ULongValue));
}

INT4
FsRrd6ThrotLimitTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsRrd6ThrotLimit (pu4Error, pMultiData->u4_ULongValue));
}

INT4
FsRrd6RouterIdDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                   tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsRrd6RouterId (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsRrd6FilterByOspfTagDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsRrd6FilterByOspfTag
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsRrd6FilterOspfTagDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsRrd6FilterOspfTag
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsRrd6FilterOspfTagMaskDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsRrd6FilterOspfTagMask
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsRrd6RouterASNumberDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsRrd6RouterASNumber
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsRrd6AdminStatusDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsRrd6AdminStatus
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsRrd6TraceDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsRrd6Trace (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsRrd6ThrotLimitDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsRrd6ThrotLimit (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsRrd6ControlTable (tSnmpIndex * pFirstMultiIndex,
                                tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsRrd6ControlTable
            (pNextMultiIndex->pIndex[0].pOctetStrValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsRrd6ControlTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[0].pOctetStrValue,
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}


INT4 FsRrd6MaximumBgpRoutesDep(UINT4 *pu4Error,tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2FsRrd6MaximumBgpRoutes(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4 FsRrd6MaximumOspfRoutesDep(UINT4 *pu4Error,tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2FsRrd6MaximumOspfRoutes(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4 FsRrd6MaximumRipRoutesDep(UINT4 *pu4Error,tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2FsRrd6MaximumRipRoutes(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4 FsRrd6MaximumStaticRoutesDep(UINT4 *pu4Error,tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2FsRrd6MaximumStaticRoutes(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4 FsRrd6MaximumISISRoutesDep(UINT4 *pu4Error,tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2FsRrd6MaximumISISRoutes(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4 FsRtm6StaticRouteDistanceDep(UINT4 *pu4Error,tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
    return(nmhDepv2FsRtm6StaticRouteDistance(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}


INT4
FsRrd6ControlSourceProtoGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRrd6ControlTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRrd6ControlSourceProto
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsRrd6ControlDestProtoGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRrd6ControlTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRrd6ControlDestProto (pMultiIndex->pIndex[0].pOctetStrValue,
                                          pMultiIndex->pIndex[1].i4_SLongValue,
                                          &(pMultiData->i4_SLongValue)));

}

INT4
FsRrd6ControlRouteExportFlagGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRrd6ControlTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRrd6ControlRouteExportFlag
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsRrd6ControlRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRrd6ControlTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRrd6ControlRowStatus (pMultiIndex->pIndex[0].pOctetStrValue,
                                          pMultiIndex->pIndex[1].i4_SLongValue,
                                          &(pMultiData->i4_SLongValue)));

}

INT4
FsRrd6ControlSourceProtoSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRrd6ControlSourceProto
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsRrd6ControlDestProtoSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRrd6ControlDestProto (pMultiIndex->pIndex[0].pOctetStrValue,
                                          pMultiIndex->pIndex[1].i4_SLongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
FsRrd6ControlRouteExportFlagSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRrd6ControlRouteExportFlag
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsRrd6ControlRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRrd6ControlRowStatus (pMultiIndex->pIndex[0].pOctetStrValue,
                                          pMultiIndex->pIndex[1].i4_SLongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
FsRrd6ControlSourceProtoTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2FsRrd6ControlSourceProto (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               pOctetStrValue,
                                               pMultiIndex->pIndex[1].
                                               i4_SLongValue,
                                               pMultiData->i4_SLongValue));

}

INT4
FsRrd6ControlDestProtoTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2FsRrd6ControlDestProto (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             pOctetStrValue,
                                             pMultiIndex->pIndex[1].
                                             i4_SLongValue,
                                             pMultiData->i4_SLongValue));

}

INT4
FsRrd6ControlRouteExportFlagTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhTestv2FsRrd6ControlRouteExportFlag (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   pOctetStrValue,
                                                   pMultiIndex->pIndex[1].
                                                   i4_SLongValue,
                                                   pMultiData->i4_SLongValue));

}

INT4
FsRrd6ControlRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2FsRrd6ControlRowStatus (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             pOctetStrValue,
                                             pMultiIndex->pIndex[1].
                                             i4_SLongValue,
                                             pMultiData->i4_SLongValue));

}

INT4
FsRrd6ControlTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsRrd6ControlTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsRrd6RoutingProtoTable (tSnmpIndex * pFirstMultiIndex,
                                     tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsRrd6RoutingProtoTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsRrd6RoutingProtoTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsRrd6RoutingRegnIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRrd6RoutingProtoTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRrd6RoutingRegnId (pMultiIndex->pIndex[0].i4_SLongValue,
                                       &(pMultiData->i4_SLongValue)));

}

INT4
FsRrd6RoutingProtoTaskIdentGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRrd6RoutingProtoTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRrd6RoutingProtoTaskIdent
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
FsRrd6RoutingProtoQueueIdentGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRrd6RoutingProtoTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRrd6RoutingProtoQueueIdent
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
FsRrd6AllowOspfAreaRoutesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRrd6RoutingProtoTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRrd6AllowOspfAreaRoutes
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsRrd6AllowOspfExtRoutesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRrd6RoutingProtoTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRrd6AllowOspfExtRoutes
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsRrd6AllowOspfAreaRoutesSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRrd6AllowOspfAreaRoutes
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsRrd6AllowOspfExtRoutesSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRrd6AllowOspfExtRoutes
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsRrd6AllowOspfAreaRoutesTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2FsRrd6AllowOspfAreaRoutes (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                i4_SLongValue,
                                                pMultiData->i4_SLongValue));

}

INT4
FsRrd6AllowOspfExtRoutesTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2FsRrd6AllowOspfExtRoutes (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               i4_SLongValue,
                                               pMultiData->i4_SLongValue));

}

INT4
FsRrd6RoutingProtoTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsRrd6RoutingProtoTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsRtm6RedEntryTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsRtm6RedEntryTime (&(pMultiData->i4_SLongValue)));
}

INT4
FsRtm6RedExitTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsRtm6RedExitTime (&(pMultiData->i4_SLongValue)));
}
