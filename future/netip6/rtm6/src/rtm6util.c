/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rtm6util.c,v 1.40 2017/12/26 13:34:23 siva Exp $
 *
 * Description:This file contains utility functions provided by RTM6   
 * module
 *******************************************************************/

#include "rtm6inc.h"
#include "iss.h"
#include "rtm6frt.h"
#ifdef LNXIP6_WANTED
PUBLIC INT4
    Lip6NetLinkRouteModify PROTO ((INT4 i4Command,
                                   tNetIpv6RtInfo * pNetRtInfo));
#endif
extern tOsixTaskId  gu4Ip6LockTaskId;
/******************************************************************************
* DESCRIPTION : This fuction gets the best route from the list of routes.
*
* INPUTS      : pRtm6Cxt - RTM6 Context Pointer
*             : pIp6RtEntryHead - Route List Head
*
* OUTPUTS     : pIp6BestRtEntry - Best route entry in the list
*
* RETURNS     : IP6_SUCCESS | IP6_FAILURE
*
* NOTES       : RTM6 data structures are accessed without RTM6 protocol lock. 
*               So whoever calling this function should take lock.
******************************************************************************/
INT4
Rtm6UtilFwdTblGetBestRouteInCxt (tRtm6Cxt * pRtm6Cxt,
                                 tIp6RtEntry * pIp6RtEntryHead,
                                 tIp6RtEntry ** ppIp6BestRtEntry)
{
    tIp6RtEntry        *pIp6RtEntry = NULL;
    tIp6RtEntry        *pTempBestRoute = NULL;
    tIp6RtEntry        *pBestRoute = NULL;
    tIp6RtEntry        *pBestNullRoute = NULL;
    tIp6RtEntry        *pRt6One = NULL;
    tIp6RtEntry        *pSt6Rt = NULL;
    UINT1               u1MinPreference = IP_PREFERENCE_INFINITY;
    UINT1               u1MinNullPref = IP_PREFERENCE_INFINITY;
    UINT1               u1Pref = IP_PREFERENCE_INFINITY;

    /* In case of Default route, the default route with Neighbor Cache in
     * complete state is selected. Else other default routes are selected
     * in round robin method to ensure that data packets are forwarded
     * uniformly across all paths.
     */
    if (pIp6RtEntryHead->i1DefRtrFlag == IP6_DEF_RTR_FLAG)
    {
        pIp6RtEntry = (tIp6RtEntry *) Rtm6UtilDefRtLookupInCxt (pRtm6Cxt,
                                                                pIp6RtEntryHead);
        if (pIp6RtEntry == NULL)
        {
            *ppIp6BestRtEntry = NULL;
            pIp6RtEntryHead->pBestRoute = NULL;
            return RTM6_FAILURE;
        }
        else
        {
            *ppIp6BestRtEntry = pIp6RtEntry;
            pIp6RtEntryHead->pBestRoute = pIp6RtEntry;
            return RTM6_SUCCESS;
        }
    }

    /* Route preference algorithm - 
     * 1. Local Route has got the highest priority. 
     * 2. If local route is not present then the route 
     *    with lowest preference value
     *    is selected as the best route.
     */
    for (pIp6RtEntry = pIp6RtEntryHead; pIp6RtEntry != NULL;
         pIp6RtEntry = pIp6RtEntry->pNextRtEntry)
    {
        if (pIp6RtEntry->i1Proto == IP6_LOCAL_PROTOID)
        {
            /* Local Route. */

            *ppIp6BestRtEntry = pIp6RtEntry;
            pIp6RtEntryHead->pBestRoute = pIp6RtEntry;
            return RTM6_SUCCESS;
        }
        if (pIp6RtEntry->i1Proto == IP6_NETMGMT_PROTOID)
        {
            /* Static Route */
            pSt6Rt = pIp6RtEntry;
        }
        pTempBestRoute = pIp6RtEntry;
        while (pTempBestRoute != NULL)
        {
            if (pTempBestRoute->u4RowStatus == IP6FWD_ACTIVE)
            {
                if (pTempBestRoute->u1Preference != 0)
                {
                    u1Pref = pTempBestRoute->u1Preference;
                }
                else
                {
                    u1Pref = (UINT1)
                        (pRtm6Cxt->au4RtPref[pTempBestRoute->i1Proto - 1]);
                }
                if (pTempBestRoute->u1NullFlag != RTM6_ONE)
                {

                    if (u1Pref < u1MinPreference)
                    {
                        u1MinPreference = u1Pref;
                        pBestRoute = pTempBestRoute;
                    }
                }

                else
                {
                    if (u1Pref < u1MinNullPref)
                    {
                        u1MinNullPref = u1Pref;
                        pBestNullRoute = pTempBestRoute;
                    }
                }
            }
            if (((pTempBestRoute != NULL)
                 && (pTempBestRoute->u1NullFlag != RTM6_ONE))
                && (pTempBestRoute->pNextAlternatepath != NULL)
                && (pTempBestRoute->i1Proto == ISIS_ID))

            {
                pRt6One = (tIp6RtEntry *) pTempBestRoute->pNextAlternatepath;
                if (pRt6One->i1Proto == ISIS_ID)
                {
                    if (pTempBestRoute->u1MetricType != pRt6One->u1MetricType)
                    {
                        if (((pTempBestRoute->u1MetricType == IP_ISIS_LEVEL1)
                             && (pRt6One->u1MetricType == IP_ISIS_INTER_AREA))
                            || ((pTempBestRoute->u1MetricType == IP_ISIS_LEVEL1)
                                && (pRt6One->u1MetricType == IP_ISIS_LEVEL2))
                            || ((pTempBestRoute->u1MetricType == IP_ISIS_LEVEL2)
                                && (pRt6One->u1MetricType ==
                                    IP_ISIS_INTER_AREA)))
                        {
                            pBestRoute = pTempBestRoute;
                        }
                        else if (((pRt6One->u1MetricType == IP_ISIS_LEVEL1)
                                  && (pTempBestRoute->u1MetricType ==
                                      IP_ISIS_INTER_AREA))
                                 || ((pRt6One->u1MetricType == IP_ISIS_LEVEL1)
                                     && (pTempBestRoute->u1MetricType ==
                                         IP_ISIS_LEVEL2))
                                 || ((pRt6One->u1MetricType == IP_ISIS_LEVEL2)
                                     && (pTempBestRoute->u1MetricType ==
                                         IP_ISIS_INTER_AREA)))
                        {
                            pBestRoute = pRt6One;
                        }
                    }
                }
            }
            pTempBestRoute = pTempBestRoute->pNextAlternatepath;
        }                        /* endwhile (pTempBestRoute != NULL) */
        if (((pSt6Rt != NULL) && (pSt6Rt->u1NullFlag != RTM6_ONE))
            && (pSt6Rt->u4RowStatus == IP6FWD_ACTIVE)
            && (pSt6Rt->u1Preference <= pIp6RtEntry->u1Preference)
            && (pSt6Rt->u1Preference != IP_PREFERENCE_INFINITY))
        {
            pBestRoute = pSt6Rt;
        }
    }                            /* endfor(... ; pIp6RtEntry != NULL; ...) */

    if (pBestRoute == NULL)
    {
        if (pBestNullRoute == NULL)
        {
            pIp6RtEntryHead->pBestRoute = NULL;
            *ppIp6BestRtEntry = NULL;
            return RTM6_FAILURE;
        }
        else
        {
            pIp6RtEntryHead->pBestRoute = pBestNullRoute;
            *ppIp6BestRtEntry = pBestNullRoute;
            return RTM6_SUCCESS;
        }
    }

    pIp6RtEntryHead->pBestRoute = pBestRoute;
    *ppIp6BestRtEntry = pBestRoute;
    return RTM6_SUCCESS;
}

/******************************************************************************
* DESCRIPTION : This fuction gets the best anycast route from the list of routes.
*
* INPUTS      : pRtm6Cxt - RTM6 Context Pointer
*             : pIp6RtEntryHead - Route List Head
*
* OUTPUTS     : pIp6BestRtEntry - Best anycast route entry in the list
*
* RETURNS     : IP6_SUCCESS | IP6_FAILURE
*
* NOTES       : RTM6 data structures are accessed without RTM6 protocol lock. 
*               So whoever calling this function should take lock.
******************************************************************************/
INT4
Rtm6UtilFwdTblGetBestAnycastRouteInCxt (tRtm6Cxt * pRtm6Cxt,
                                        tIp6RtEntry * pIp6RtEntryHead,
                                        tIp6RtEntry ** ppIp6BestRtEntry)
{
    tIp6RtEntry        *pIp6RtEntry = NULL;
    tIp6RtEntry        *pTempBestRoute = NULL;
    tIp6RtEntry        *pFinalBestRoute = NULL;
    tIp6RtEntry        *pAnycastRoute = NULL;
    tIp6RtEntry        *pBestRoute = NULL;
    tIp6DstInfo         aIp6DstInfo[MAX_IP6_ANYCAST_DST];
    tIp6DstInfo        *pIp6DstInfo[MAX_IP6_ANYCAST_DST] = { NULL };
    UINT1               u1Count = 0;
    UINT1               u1RtFound = 0;

    MEMSET (&(aIp6DstInfo[0]), 0, sizeof (tIp6DstInfo));
    /* In case of Default route, the default route with Neighbor Cache in
     * complete state is selected. Else other default routes are selected
     * in round robin method to ensure that data packets are forwarded
     * uniformly across all paths.
     */
    if (pIp6RtEntryHead->i1DefRtrFlag == IP6_DEF_RTR_FLAG)
    {
        pIp6RtEntry = (tIp6RtEntry *) Rtm6UtilDefRtLookupInCxt (pRtm6Cxt,
                                                                pIp6RtEntryHead);
        if (pIp6RtEntry == NULL)
        {
            *ppIp6BestRtEntry = NULL;
            pIp6RtEntryHead->pBestRoute = NULL;
            return RTM6_FAILURE;
        }
        else
        {
            *ppIp6BestRtEntry = pIp6RtEntry;
            pIp6RtEntryHead->pBestRoute = pIp6RtEntry;
            return RTM6_SUCCESS;
        }
    }

    /* Route preference algorithm - 
     * 1. Local Route has got the highest priority. 
     * 2. If local route is not present then the route 
     *    with lowest preference value
     *    is selected as the best route.
     */
    for (pIp6RtEntry = pIp6RtEntryHead; pIp6RtEntry != NULL;
         pIp6RtEntry = pIp6RtEntry->pNextRtEntry)
    {
        if (pIp6RtEntry->i1Proto == IP6_LOCAL_PROTOID)
        {
            /* Local Route. */
            *ppIp6BestRtEntry = pIp6RtEntry;
            pIp6RtEntryHead->pBestRoute = pIp6RtEntry;
            return RTM6_SUCCESS;
        }
        pTempBestRoute = pIp6RtEntry;
        while (pTempBestRoute != NULL)
        {
            if (u1RtFound == 0)
            {
                pAnycastRoute = pTempBestRoute;
                u1RtFound = 1;
            }
            else
            {
                pAnycastRoute->pNextRtEntry = pTempBestRoute;
            }
            MEMSET (&(aIp6DstInfo[u1Count]), 0, sizeof (tIp6DstInfo));
            Ip6AddrCopy (&(aIp6DstInfo[u1Count].dstaddr),
                         &(pTempBestRoute->dst));
            aIp6DstInfo[u1Count].u4IfIndex = pTempBestRoute->u4Index;
            aIp6DstInfo[u1Count].u1PrefixLen = pTempBestRoute->u1Prefixlen;
            aIp6DstInfo[u1Count].u1CumulativeScore =
                pTempBestRoute->u1NHpreference;

            pIp6DstInfo[u1Count] = &(aIp6DstInfo[u1Count]);
            u1Count++;

            pTempBestRoute = pTempBestRoute->pNextAlternatepath;

        }                        /* endwhile (pTempBestRoute != NULL) */
    }                            /* endfor(... ; pIp6RtEntry != NULL; ...) */

    RTM6_TASK_UNLOCK ();
    IP6_TASK_UNLOCK ();
    NetIpv6SelectDestAddr (&(pIp6DstInfo[0]), u1Count);
    IP6_TASK_LOCK ();
    RTM6_TASK_LOCK ();

    /* We have the dst addr, prefix length and interface index of the best
     * route. We now need to find the route entry that matches these 
     * indices. If there is more than one route with these 3 indices, the 
     * route entry with the highest next hop address will be returned. */
    pFinalBestRoute = NULL;
    pTempBestRoute = NULL;
    for (pBestRoute = pAnycastRoute; pBestRoute != NULL;
         pBestRoute = pBestRoute->pNextRtEntry)
    {
        pTempBestRoute = pBestRoute;
        while (pTempBestRoute != NULL)
        {
            if (((MEMCMP (&(pIp6DstInfo[0]->dstaddr), &(pTempBestRoute->dst),
                          IP6_ADDR_SIZE)) == 0) &&
                (pTempBestRoute->u1Prefixlen == pIp6DstInfo[0]->u1PrefixLen) &&
                (pTempBestRoute->u4Index == pIp6DstInfo[0]->u4IfIndex))
            {
                if ((pFinalBestRoute == NULL) ||
                    (MEMCMP (&(pFinalBestRoute->nexthop),
                             &(pTempBestRoute->nexthop), sizeof (tIp6Addr))
                     <= IP6_ZERO))
                {
                    pFinalBestRoute = pTempBestRoute;
                }
            }
            pTempBestRoute = pTempBestRoute->pNextAlternatepath;
        }
    }

    if (pFinalBestRoute == NULL)
    {
        pIp6RtEntryHead->pBestRoute = NULL;
        *ppIp6BestRtEntry = NULL;
        return RTM6_FAILURE;
    }

    *ppIp6BestRtEntry = pFinalBestRoute;
    pIp6RtEntryHead->pBestRoute = pFinalBestRoute;
    return RTM6_SUCCESS;
}

/******************************************************************************
* DESCRIPTION : This fuction copies Ip6 route info from tIp6RtEntry structure 
*               to tNetIpv6RtInfo
* 
* INPUTS      : pRtm6Cxt- RTM6 Context Pointer 
*             : pIp6RtEntry - Pointer to Ip6 Route Entry
*
* OUTPUTS     : pNetIpv6RtInfo 
*
* RETURNS     : None
*
* NOTES       :  
******************************************************************************/

VOID
Rtm6UtilCopyRtInfoInCxt (tRtm6Cxt * pRtm6Cxt,
                         tIp6RtEntry * pIp6RtEntry,
                         tNetIpv6RtInfo * pNetIpv6RtInfo)
{

    UINT2               u2AppId = 0;

    if ((pIp6RtEntry == NULL) || (pNetIpv6RtInfo == NULL))
    {
        return;
    }
/*     
    for (pTempIp6RtEntry = pIp6RtEntry; pTempIp6RtEntry != NULL;
             pTempIp6RtEntry = pTempIp6RtEntry->pNextRtEntry)
    {
        i1ProtoId = pTempIp6RtEntry->i1Proto;
        if (((pRtQuery->u2AppIds >> (i1ProtoId - 1)) & 0x0001) == 1)
        {
            u2AppId &= (~(0x0001 << (i1ProtoId - 1)));
        }
    }
    */

    MEMCPY (&pNetIpv6RtInfo->Ip6Dst, &pIp6RtEntry->dst, sizeof (tIp6Addr));
    MEMCPY (&pNetIpv6RtInfo->NextHop, &pIp6RtEntry->nexthop, sizeof (tIp6Addr));

    pNetIpv6RtInfo->u4Index = pIp6RtEntry->u4Index;
    pNetIpv6RtInfo->u4Metric = pIp6RtEntry->u4Metric;
    pNetIpv6RtInfo->u4RowStatus = pIp6RtEntry->u4RowStatus;
    pNetIpv6RtInfo->u4RouteTag = pIp6RtEntry->u4RouteTag;
    pNetIpv6RtInfo->u1Prefixlen = pIp6RtEntry->u1Prefixlen;
    pNetIpv6RtInfo->i1Proto = pIp6RtEntry->i1Proto;
    pNetIpv6RtInfo->u2ChgBit = u2AppId;
    pNetIpv6RtInfo->i1MetricType5 = pIp6RtEntry->i1MetricType5;
    pNetIpv6RtInfo->i1Type = pIp6RtEntry->i1Type;
    pNetIpv6RtInfo->i1DefRtrFlag = pIp6RtEntry->i1DefRtrFlag;
    pNetIpv6RtInfo->u1Preference = pIp6RtEntry->u1Preference;
    pNetIpv6RtInfo->u1MetricType = pIp6RtEntry->u1MetricType;
    pNetIpv6RtInfo->u1AddrType = pIp6RtEntry->u1AddrType;
    pNetIpv6RtInfo->u1HwStatus = pIp6RtEntry->u1HwStatus;
    pNetIpv6RtInfo->u4ContextId = pRtm6Cxt->u4ContextId;
    pNetIpv6RtInfo->u1HwStatus = pIp6RtEntry->u1HwStatus;
    return;
}

/******************************************************************************
 * DESCRIPTION : This routine is called to find a default route for the outgoing
 *               packet. This routine first tries to fetch a reachable or 
 *               probably reachable default router. If none of the entries in 
 *               the list are reachable it tries to get a default router in a 
 *               round - robin fashion, so that all the routers are probed one 
 *               after other.If the list is empty it returns NULL.
 *
 * INPUTS      : None. 
 *
 * OUTPUTS     : None.
 *
 * RETURNS     : pRt - Poniter to the default router.
 *
 * NOTES       :
 ******************************************************************************/
tIp6RtEntry        *
Rtm6UtilDefRtLookupInCxt (tRtm6Cxt * pRtm6Cxt, tIp6RtEntry * pDefHeadNode)
{
    tIp6RtEntry        *pRtEntry = NULL;
    tIp6RtEntry        *pTempEntry = NULL;
#ifndef LNXIP6_WANTED
    tIp6RtEntry        *pCurrProtoEntry = NULL;
    UINT4               u4ChgTime = 0;
    INT1                u1ReachState = IP6_FAILURE;
    INT1                i1IsLockReq = FALSE;
#endif
    INT1                i1Proto = 0;
    if (pDefHeadNode == NULL)
    {
        pRtEntry = Rtm6GetDefRouteInCxt (pRtm6Cxt, i1Proto);
        if (pRtEntry == NULL)
        {
            /* no default route entry, so exit */
            return NULL;
        }
    }
    else
    {
        pRtEntry = pDefHeadNode;
    }

#ifndef LNXIP6_WANTED
    /* Get the first default route whose Nd cache entry is in incomplete state.
     * Host should always prefer a route entry whose reachability is
     * confirmed or probably confirmed rather selecting a router whose
     * reachability is suspect.
     */
    for (pCurrProtoEntry = pRtEntry; pRtEntry != NULL;
         pRtEntry = pCurrProtoEntry->pNextRtEntry, pCurrProtoEntry = pRtEntry)
    {
        for (; pRtEntry != NULL; pRtEntry = pRtEntry->pNextAlternatepath)
        {
            if (pRtEntry->u4RowStatus != IP6FWD_ACTIVE)
            {
                continue;
            }
            /* As this API is invoked from different RTM6 APIs,
               check the gu4Ip6LockTaskId with the current TaskId 
               to confirm whether the same thread acquired the 
               IP6LOCK() before.
               If so do a UNLOCK() and LOCK().
             */
            if (gu4Ip6LockTaskId == OsixGetCurTaskId ())
            {
                IP6_TASK_UNLOCK ();
                i1IsLockReq = TRUE;
            }

            u1ReachState = Nd6IsCacheReachable (pRtEntry->u4Index,
                                                &pRtEntry->nexthop);
            if (i1IsLockReq == TRUE)
            {
                IP6_TASK_LOCK ();
            }

            if (u1ReachState == IP6_SUCCESS)
            {
                /* Found a default route in Complete Status. */
                return pRtEntry;
            }

            /* No router is known to be reachable or probably reachable so 
             * select a router in round-robin fashion.
             */
            if ((pTempEntry == NULL) || (pRtEntry->u4ChangeTime < u4ChgTime))
            {
                u4ChgTime = pRtEntry->u4ChangeTime;
                pTempEntry = pRtEntry;
            }
        }
    }
#else
    /* Incase of Linux IP, we will not have the ND cache, so return
     * the first available route */
    pTempEntry = pRtEntry;
#endif
    return pTempEntry;
}

/******************************************************************************
* DESCRIPTION : This function process the change in the interface status. When
*               the interface goes down/delete, all the static routes learnt
*               over that interface will be made as inactive. When the
*               interface becomes active, then all the static route reachable
*               via this interface will be made active.
* INPUTS      : Structure holding the information about the interface and the
*               route list head in TRIE.
* OUTPUTS     : None
* RETURNS     : TRIE_SUCCESS/TRIE_FAILURE
* NOTES       :
* *****************************************************************************/

INT4
Rtm6UtilCbHandleIfStatusChange (tRtm6TrieScanOutParams * pScanParams)
{
    tIp6RtEntry        *pIp6RtEntry = NULL;
    tIp6RtEntry        *pCurrProtoEntry = NULL;
    tIp6RtEntry        *pCurrEntry = NULL;
    tIp6RtEntry        *pNextEntry = NULL;
    tIp6RtEntry        *pOldBestRt = NULL;
    tIp6RtEntry        *pNewBestRt = NULL;
    tRtm6Cxt           *pRtm6Cxt = NULL;
    tIp6Addr            Ip6Addr;
    tNetIpv6RtInfo      NetIpv6RtInfo;
    UINT4               u4IfIndex = 0;
#ifdef LNXIP6_WANTED
    INT4                i4RetVal = 0;
#endif
    UINT1               u1IsRtChange = OSIX_FALSE;
    UINT1               u1PrefixLen = 0;
    INT4                i4TempVal = IP6_ZERO;

    MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));
    pIp6RtEntry = pScanParams->pRtEntry;
    u4IfIndex = pScanParams->u4IfIndex;
    pRtm6Cxt = UtilRtm6GetCxt (pScanParams->u4ContextId);
    if (pRtm6Cxt == NULL)
    {
        return TRIE_FAILURE;
    }
    Rtm6UtilFwdTblGetBestRouteInCxt (pRtm6Cxt, pIp6RtEntry, &pOldBestRt);
    Ip6AddrCopy (&Ip6Addr, &(pIp6RtEntry->dst));
    u1PrefixLen = pIp6RtEntry->u1Prefixlen;

    pCurrProtoEntry = pIp6RtEntry;
    while (pCurrProtoEntry != NULL)
    {
        /* For interface down/delete event disable only the static route
         * entries reachable via this interface. For interface up
         * event re-enable all the static routes. If there are any
         * routes learnt via this interface by NDISC protocol, then those
         * routes will be removed. */
        if ((pCurrProtoEntry->i1Proto != IP6_NETMGMT_PROTOID) &&
            (pCurrProtoEntry->i1Proto != IP6_NDISC_PROTOID))
        {
            /* Route is not STATIC/NDISC. */
            pCurrProtoEntry = pCurrProtoEntry->pNextRtEntry;
            continue;
        }

        /* Process all the routes learnt from this protocol. */
        pCurrEntry = pCurrProtoEntry;

        /* Scan through the static routes to find
         * any route reachable via this interface. */
        while (pCurrEntry != NULL)
        {
            pNextEntry = pCurrEntry->pNextAlternatepath;

            if (pCurrEntry->u4Index == u4IfIndex)
            {
                if (pScanParams->u4ScanEvent == RTM6_SCAN_INTF_UP)
                {
                    /*
                     * Interface coming up. Check for route status
                     * If status NOT_IN_SERVICE, then set the status
                     * as ACTIVE
                     */
                    if (pCurrEntry->u4RowStatus == IP6FWD_NOT_IN_SERVICE)
                    {
                        pCurrEntry->u4RowStatus = IP6FWD_ACTIVE;
                    }
                    /*Need to handle ECMP routes */
                    /*Incoming index may be best route or non best route. Need to handle these */
/******/

                    Ip6GetBestRouteEntryInCxt (pRtm6Cxt,
                                               &Ip6Addr, u1PrefixLen,
                                               &pNewBestRt);
                    if (pNewBestRt == NULL)
                    {
                        return TRIE_FAILURE;
                    }
                    if ((pOldBestRt != NULL)
                        && ((pNewBestRt == pOldBestRt)
                            && (pCurrEntry != pOldBestRt)
                            && (pOldBestRt->u4Metric == pCurrEntry->u4Metric)
                            && (pOldBestRt->i1Proto == pCurrEntry->i1Proto)))
                    {

                        Rtm6ApiAddOrDelEcmpRtsInCxt (pRtm6Cxt->u4ContextId,
                                                     pCurrEntry, pNewBestRt,
                                                     IP6_ROUTE_ADD);
                        pCurrEntry->u4Flag |= RTM6_ECMP_RT;
                        if (!(pOldBestRt->u4Flag & RTM6_ECMP_RT))
                        {

                            pOldBestRt->u4Flag |= RTM6_ECMP_RT;

                        }
                    }
/*******/

                }
                else if (pScanParams->u4ScanEvent == RTM6_SCAN_INTF_DOWN)
                {
                    /*
                     * Interface is going down. Reset the status
                     * of all the ACTIVE routes.
                     */
                    if (pCurrEntry->i1Proto == IP6_NETMGMT_PROTOID)
                    {
                        if (pCurrEntry->u4RowStatus == IP6FWD_ACTIVE)
                        {
                            /* Copying the Ecmp count in the current entry 
                             *  to a temporary variable*/
                            i4TempVal = pCurrEntry->u1EcmpCount;
                            /*Setting Reachstate for the CurrentEntry and 
                             * getting the total no of routes installed
                             * inorder to indicate it to teh hardware for Deletion*/

                            Rtm6SetReachState (pRtm6Cxt->u4ContextId,
                                               pOldBestRt);
                            Rtm6GetInstalledRouteCountStatus (pRtm6Cxt,
                                                              pOldBestRt,
                                                              &pOldBestRt->
                                                              u1EcmpCount);
                            Rtm6SetReachState (pRtm6Cxt->u4ContextId,
                                               pCurrEntry);
                            Rtm6GetInstalledRouteCountStatus (pRtm6Cxt,
                                                              pCurrEntry,
                                                              &pCurrEntry->
                                                              u1EcmpCount);
                            /*Handle ECMP route */
                            if (((pOldBestRt->u4Flag & RTM6_ECMP_RT) ==
                                 RTM6_ECMP_RT)
                                && ((pCurrEntry->u4Flag & RTM6_ECMP_RT) ==
                                    RTM6_ECMP_RT)
                                && (pOldBestRt->i1Proto == pCurrEntry->i1Proto))
                            {
                                /* While deleting a non - best ecmp route, Hw should be indicate 
                                 * to delete the non-best route though best route remains same*/
                                if ((pCurrEntry != pOldBestRt)
                                    || (pCurrEntry->u1EcmpCount !=
                                        pOldBestRt->u1EcmpCount))
                                {
                                    /*Delete the ECMP route with pCurrEntry */

                                    Rtm6ApiAddOrDelEcmpRtsInCxt (pRtm6Cxt->
                                                                 u4ContextId,
                                                                 pCurrEntry,
                                                                 pOldBestRt,
                                                                 IP6_ROUTE_DEL);
                                    /*Copying the previous ECMP Count after Deletion */
                                    pCurrEntry->u1EcmpCount = i4TempVal;
                                    pCurrEntry->u4Flag &= ~(UINT4) RTM6_ECMP_RT;

                                    {
                                        tIp6RtEntry        *pTmpIp6RtEntry =
                                            NULL;
                                        UINT2               u2BestRtCount = 0;

                                        pTmpIp6RtEntry = pOldBestRt;
                                        while (pTmpIp6RtEntry != NULL)
                                        {
                                            if ((pTmpIp6RtEntry->u4Metric ==
                                                 pOldBestRt->u4Metric)
                                                && (pTmpIp6RtEntry->
                                                    u4RowStatus ==
                                                    IP6FWD_ACTIVE))
                                            {
                                                u2BestRtCount++;
                                            }

                                            pTmpIp6RtEntry =
                                                pTmpIp6RtEntry->
                                                pNextAlternatepath;
                                        }
                                        if (u2BestRtCount > 1)
                                        {
                                            pOldBestRt->u4Flag |= RTM6_ECMP_RT;

                                        }
                                        else
                                        {
                                            pOldBestRt->u4Flag &=
                                                ~(UINT4) RTM6_ECMP_RT;
                                        }

                                    }
                                }
                            }
                            Rtm6ApiDelNextHopInCxt (pRtm6Cxt->u4ContextId,
                                                    &pCurrEntry->nexthop);
                            pCurrEntry->u4RowStatus = IP6FWD_NOT_IN_SERVICE;
#ifdef LNXIP6_WANTED
                            RTM6_COPY_ROUTE_INFO_TO_UPDATE_MSG (pCurrEntry,
                                                                NetIpv6RtInfo);
                            NetIpv6RtInfo.u4ContextId = pRtm6Cxt->u4ContextId;
                            NetIpv6RtInfo.u2ChgBit = IP6_BIT_ALL;

                            Rtm6TrieDeleteEntryInCxt (pRtm6Cxt, pCurrEntry);

                            /*Decrement the count of u4Ip6FwdTblRouteNum, 
                             *since the route is deleted from RTM6*/

                            pRtm6Cxt->u4Ip6FwdTblRouteNum--;
                            IP6_RT_FREE (pCurrEntry);
                            pCurrEntry = NULL;

                            /*Add the route with invalid next-hop to Invalid route list. */
                            i4RetVal =
                                Rtm6InsertRouteInInvdRouteList (&NetIpv6RtInfo);
                            UNUSED_PARAM (i4RetVal);
#endif
                        }
                    }
                    else
                    {
                        /* Route learnt via NDISC protocol. Delete it. */
                        if (pCurrEntry == pIp6RtEntry)
                        {
                            /* First Route in Trie Node is getting removed. */
                            u1IsRtChange = OSIX_TRUE;
                        }
                        Rtm6UtilRoutePurgeInCxt (pRtm6Cxt, pCurrEntry);
                    }
                }
                else
                {
                    /* Interface is Delted. Delete the Route. */

                    Rtm6UtilRoutePurgeInCxt (pRtm6Cxt, pCurrEntry);
                }
            }
            pCurrEntry = pNextEntry;
            pNextEntry = NULL;
        }

        if ((pScanParams->u4ScanEvent == RTM6_SCAN_INTF_UP) ||
            (pScanParams->u4ScanEvent == RTM6_SCAN_INTF_DOWN))
        {
            /* Get the new Best Route. If there is any change in the new best
             * route, then redistribute the Old Best as withdrawn and if present
             * redistribute the new best route. */
            if (u1IsRtChange == OSIX_TRUE)
            {
                /* First Route in the Trie Node has changed. So scan through
                 * the RIB and get the new Best route. */
                Ip6GetBestRouteEntryInCxt (pRtm6Cxt,
                                           &Ip6Addr, u1PrefixLen, &pNewBestRt);
            }
            else
            {

                Rtm6UtilFwdTblGetBestRouteInCxt (pRtm6Cxt,
                                                 pIp6RtEntry, &pNewBestRt);
            }

            if (pOldBestRt != pNewBestRt)
            {
                /* Change in Best route. Remove the old best route
                 * and redistribute the new best route. */
                if (pOldBestRt != NULL)
                {

                    if (pScanParams->u4ScanEvent == RTM6_SCAN_INTF_DOWN)
                    {
                        if (pNewBestRt != NULL)
                        {
                            Rtm6SetReachState (pRtm6Cxt->u4ContextId,
                                               pNewBestRt);
                            Rtm6GetInstalledRouteCount (pNewBestRt,
                                                        &pOldBestRt->
                                                        u1EcmpCount);
                        }
                        /* Reachability will be gone for the route to be deleted, Hence*                                   
                         * route  count is incremented by one before passing to NP*/
                        pOldBestRt->u1EcmpCount++;
                    }
                    Rtm6HandleRouteUpdatesInCxt (pRtm6Cxt,
                                                 pOldBestRt, FALSE,
                                                 IP6_BIT_STATUS);
                    if (pScanParams->u4ScanEvent == RTM6_SCAN_INTF_DOWN)
                    {
                        pOldBestRt->u1EcmpCount--;
                    }

                    /* Advertised route change to registered protocol */
                    RTM6_COPY_ROUTE_INFO_TO_UPDATE_MSG (pOldBestRt,
                                                        NetIpv6RtInfo);
                    NetIpv6RtInfo.u2ChgBit = IP6_BIT_STATUS;
                    NetIpv6RtInfo.u4RowStatus = IP6FWD_DESTROY;
                    NetIpv6RtInfo.u4ContextId = pRtm6Cxt->u4ContextId;
                    NetIpv6InvokeRouteChange (&NetIpv6RtInfo,
                                              NETIPV6_ALL_PROTO);

                    /*Need to delete all ECMP routes based on old best route */
                    Rtm6DelAllECMP6RtInCxt (pRtm6Cxt, pOldBestRt,
                                            pOldBestRt->u4Metric);
                }
                if (pNewBestRt != NULL)
                {
                    Rtm6HandleRouteUpdatesInCxt (pRtm6Cxt,
                                                 pNewBestRt, TRUE, IP6_BIT_ALL);

                    /* Advertised route change to registered protocol */
                    RTM6_COPY_ROUTE_INFO_TO_UPDATE_MSG (pNewBestRt,
                                                        NetIpv6RtInfo);
                    NetIpv6RtInfo.u2ChgBit = IP6_BIT_ALL;
                    NetIpv6RtInfo.u4RowStatus = IP6FWD_ACTIVE;
                    NetIpv6RtInfo.u4ContextId = pRtm6Cxt->u4ContextId;
                    NetIpv6InvokeRouteChange (&NetIpv6RtInfo,
                                              NETIPV6_ALL_PROTO);
                    /*Need to install all ECMP routes based on new best route */
                    Rtm6AddAllECMP6RtInCxt (pRtm6Cxt, pNewBestRt);
                }
                if ((pNewBestRt != NULL) && (pOldBestRt == NULL))
                {
                    /* Not an ECMP route */
                    Rtm6SetReachStateForRoute (pRtm6Cxt->u4ContextId,
                                               pNewBestRt);
                    Rtm6GetInstalledRouteCount (pNewBestRt,
                                                &pNewBestRt->u1EcmpCount);
                    Rtm6HandleRouteUpdatesInCxt (pRtm6Cxt, pNewBestRt, TRUE,
                                                 IP6_BIT_ALL);
                }
            }
#ifdef LNXIP6_WANTED
            else
            {
                /* Add the route to Linux IP even if there is no change in best
                 * route. If there is a change, it gets added from
                 * NetIpv6InvokeRouteChange () */
                if (pNewBestRt != NULL)
                {
                    RTM6_COPY_ROUTE_INFO_TO_UPDATE_MSG (pNewBestRt,
                                                        NetIpv6RtInfo);
                    if (NetIpv6RtInfo.i1Proto != IP6_LOCAL_PROTOID)
                    {
                        NetIpv6RtInfo.u2ChgBit = IP6_BIT_ALL;
                        NetIpv6RtInfo.u4RowStatus = IP6FWD_ACTIVE;
                        NetIpv6RtInfo.u4ContextId = pRtm6Cxt->u4ContextId;
                        Lip6NetLinkRouteModify (RTM_NEWROUTE, &NetIpv6RtInfo);
                    }
                }
            }
#endif /* LNXIP6_WANTED */
        }

        /* Finished processing all the static routes. */
        break;
    }
    return TRIE_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : Handles the Protocol Down Event. This routine will delete all
 *               the routes learnt through this protocol.                 
 * 
 * INPUTS      : Structure holding the information about the protocol and the
 *               route list head in TRIE.
 *
 * OUTPUTS     : None
 *
 * RETURNS     : TRIE_SUCCESS/TRIE_FAILURE
 *
 * NOTES       :
 ******************************************************************************/
INT4
Rtm6HandleProtoDown (tRtm6TrieScanOutParams * pScanParams)
{
    tIp6RtEntry        *pIp6RtEntry = NULL;
    tIp6RtEntry        *pCurrProtoEntry = NULL;
    tIp6RtEntry        *pCurrEntry = NULL;
    tIp6RtEntry        *pNextEntry = NULL;
    tRtm6Cxt           *pRtm6Cxt = NULL;
    INT1                i1ProtoId = 0;

    pIp6RtEntry = pScanParams->pRtEntry;
    i1ProtoId = pScanParams->i1ProtoId;
    pRtm6Cxt = UtilRtm6GetCxt (pScanParams->u4ContextId);
    if (pRtm6Cxt == NULL)
    {
        return TRIE_FAILURE;
    }

    pCurrProtoEntry = pIp6RtEntry;
    while (pCurrProtoEntry != NULL)
    {
        if (pCurrProtoEntry->i1Proto != i1ProtoId)
        {
            pCurrProtoEntry = pCurrProtoEntry->pNextRtEntry;
            continue;
        }

        /* Delete all the routes learnt from this protocol. */
        pCurrEntry = pCurrProtoEntry;

        while (pCurrEntry != NULL)
        {
            pNextEntry = pCurrEntry->pNextAlternatepath;

            Rtm6UtilRoutePurgeInCxt (pRtm6Cxt, pCurrEntry);

            pCurrEntry = pNextEntry;
            pNextEntry = NULL;
        }
        break;
    }
    return TRIE_SUCCESS;
}

/*****************************************************************************
* DESCRIPTION : Gets the Default route list from RTM6 table
*
* INPUTS      : Protocol From which the default route is learnt. If 0, then
*               returns the first default route entry. If non-zero, returns
*               the corresponding protocols default route.
*               pRtm6Cxt - RTM6 Context pointer
*
* OUTPUTS     : First default route entry
*
* RETURNS     : None
*****************************************************************************/
tIp6RtEntry        *
Rtm6GetDefRouteInCxt (tRtm6Cxt * pRtm6Cxt, INT1 i1Proto)
{
    tIp6RtEntry         Route;
    tIp6RtEntry        *pDefRoute = NULL;
    VOID               *pRibNode = NULL;

    MEMSET (&Route.dst, 0, sizeof (tIp6Addr));
    Route.u1Prefixlen = 0;
    Route.i1Proto = i1Proto;

    if (Rtm6TrieSearchExactEntryInCxt (pRtm6Cxt, &Route, &pDefRoute, &pRibNode)
        == RTM6_FAILURE)
    {
        return NULL;
    }
    return pDefRoute;
}

/*
 ****************************************************************************
 * Function           : Rtm6UtilRoute6LookupInCxt
 * Input(s)           : pRtm6Cxt -  Rtm6 Context Pointer
 *                      pAddr - Dest for the IPv6 route
 *                      u1Prefixlen - Prefix length for the route destination
 *                      u1Preference - Route preference
 * Output(s)          : pNetIpv6RtInfo - Route information is returned
 *                      pu1FoundFlag - flag to indicate the route is found or not
 * Returns            : RTM6_SUCCESS/RTM6_FAILURE
 * Action             : This function searches abd returns the route information
 ****************************************************************************
*/
INT4
Rtm6UtilRoute6LookupInCxt (tRtm6Cxt * pRtm6Cxt,
                           tIp6Addr * pAddr, UINT1 u1PrefixLen,
                           UINT1 u1Preference, UINT1 *pu1FoundFlag,
                           tNetIpv6RtInfo * pNetIpv6RtInfo)
{

    tIp6RtEntry         Ip6Entry;
    tIp6RtEntry        *pFirstEntry = NULL;
    tIp6RtEntry        *pCurrEntry = NULL;
    tIp6RtEntry        *pIp6RtEntry = NULL;
    tIp6RtEntry        *pIp6BestRtEntry = NULL;
    VOID               *pRibNode = NULL;
    INT4                i4Status = 0;
    INT4                i4OutCome = 0;

    RTM6_TRC (pRtm6Cxt->u4ContextId,
              RTM6_ALL_FAILURE_TRC | RTM6_DATA_PATH_TRC, RTM6_MOD_NAME,
              "Rtm6UtilRoute6LookupInCxt : Entry");

    RTM6_TRC_ARG2 (pRtm6Cxt->u4ContextId,
                   RTM6_ALL_FAILURE_TRC | RTM6_DATA_PATH_TRC, RTM6_MOD_NAME,
                   "RTM6: Rtm6UtilRoute6LookupInCxt for address = %s Preference = %d\n",
                   Ip6PrintAddr (pAddr), u1Preference);

    if (pNetIpv6RtInfo == NULL)
    {
        return RTM6_FAILURE;
    }

    MEMSET (&Ip6Entry, 0, sizeof (tIp6RtEntry));
    MEMCPY (&Ip6Entry.dst, pAddr, sizeof (tIp6Addr));
    Ip6Entry.u1Prefixlen = u1PrefixLen;
    Ip6Entry.i1Proto = (ALL_PROTO_ID + 1);
    Ip6Entry.u1NHpreference = 1;

    i4Status = Rtm6TrieLookupEntryInCxt (pRtm6Cxt, &Ip6Entry,
                                         &pFirstEntry, &pRibNode);

    if (i4Status == RTM6_FAILURE)
    {
        RTM6_TRC (pRtm6Cxt->u4ContextId,
                  RTM6_ALL_FAILURE_TRC | RTM6_DATA_PATH_TRC, RTM6_MOD_NAME,
                  "Rtm6UtilRoute6LookupInCxt : Failed");
        return RTM6_FAILURE;
    }

    switch (u1Preference)
    {
        case RTM6_ROUTE_PREFERENCE_BEST_METRIC:

            if (pFirstEntry->pBestRoute != NULL)
            {
                *pu1FoundFlag = TRUE;
                Rtm6UtilCopyRtInfoInCxt (pRtm6Cxt, pFirstEntry->pBestRoute,
                                         pNetIpv6RtInfo);
                return RTM6_SUCCESS;
            }
            *pu1FoundFlag = FALSE;
            RTM6_TRC (pRtm6Cxt->u4ContextId,
                      RTM6_ALL_FAILURE_TRC | RTM6_DATA_PATH_TRC, RTM6_MOD_NAME,
                      "Rtm6UtilRoute6LookupInCxt : Failed");
            return RTM6_FAILURE;

        case RTM6_ROUTE_PREFERENCE_STATIC:
            for (pIp6RtEntry = pFirstEntry; pIp6RtEntry != NULL;
                 pIp6RtEntry = pIp6RtEntry->pNextRtEntry)
            {
                /* Get the STATIC Route */
                if (pIp6RtEntry->i1Proto != IP6_NETMGMT_PROTOID)
                {
                    continue;
                }

                for (pCurrEntry = pIp6RtEntry; pCurrEntry != NULL;
                     pCurrEntry = pCurrEntry->pNextAlternatepath)
                {
                    if (pCurrEntry->u4RowStatus != IP6FWD_ACTIVE)
                    {
                        continue;
                    }

                    /* got a matching entry as per preference */
                    *pu1FoundFlag = TRUE;
                    Rtm6UtilCopyRtInfoInCxt (pRtm6Cxt, pCurrEntry,
                                             pNetIpv6RtInfo);
                    return RTM6_SUCCESS;
                }
            }
            break;

        case RTM6_ROUTE_PREFERENCE_LOCAL:
            for (pIp6RtEntry = pFirstEntry; pIp6RtEntry != NULL;
                 pIp6RtEntry = pIp6RtEntry->pNextRtEntry)
            {
                /* Get the Local Route */
                if (pIp6RtEntry->i1Proto != IP6_LOCAL_PROTOID)
                {
                    continue;
                }

                /* got a matching entry as per preference */
                *pu1FoundFlag = TRUE;
                Rtm6UtilCopyRtInfoInCxt (pRtm6Cxt, pIp6RtEntry, pNetIpv6RtInfo);
                return RTM6_SUCCESS;
            }
            break;

        case RTM6_ROUTE_PREFERENCE_RIPNG:
            for (pIp6RtEntry = pFirstEntry; pIp6RtEntry != NULL;
                 pIp6RtEntry = pIp6RtEntry->pNextRtEntry)
            {
                /* Get the RIPNG Route */
                if (pIp6RtEntry->i1Proto != IP6_RIP_PROTOID)
                {
                    continue;
                }

                /* got a matching entry as per preference */
                *pu1FoundFlag = TRUE;
                Rtm6UtilCopyRtInfoInCxt (pRtm6Cxt, pIp6RtEntry, pNetIpv6RtInfo);
                return RTM6_SUCCESS;
            }
            break;

        case RTM6_ROUTE_PREFERENCE_OSPF:
            for (pIp6RtEntry = pFirstEntry; pIp6RtEntry != NULL;
                 pIp6RtEntry = pIp6RtEntry->pNextRtEntry)
            {
                /* Get the OSPF Route */
                if (pIp6RtEntry->i1Proto != IP6_OSPF_PROTOID)
                {
                    continue;
                }

                /* got a matching entry as per preference */
                *pu1FoundFlag = TRUE;
                Rtm6UtilCopyRtInfoInCxt (pRtm6Cxt, pIp6RtEntry, pNetIpv6RtInfo);
                return RTM6_SUCCESS;
            }
            break;

        case RTM6_ROUTE_PREFERENCE_BGP:
            for (pIp6RtEntry = pFirstEntry; pIp6RtEntry != NULL;
                 pIp6RtEntry = pIp6RtEntry->pNextRtEntry)
            {
                /* Get the BGP Route */
                if (pIp6RtEntry->i1Proto != IP6_BGP_PROTOID)
                {
                    continue;
                }

                /* got a matching entry as per preference */
                *pu1FoundFlag = TRUE;
                Rtm6UtilCopyRtInfoInCxt (pRtm6Cxt, pIp6RtEntry, pNetIpv6RtInfo);
                return RTM6_SUCCESS;
            }
            break;

        default:
            *pu1FoundFlag = FALSE;
            RTM6_TRC (pRtm6Cxt->u4ContextId,
                      RTM6_ALL_FAILURE_TRC | RTM6_DATA_PATH_TRC, RTM6_MOD_NAME,
                      "Rtm6UtilRoute6LookupInCxt : Failed");
            return RTM6_FAILURE;
    }

    if (pFirstEntry->u1AddrType == ADDR6_ANYCAST)
    {
        i4OutCome =
            Rtm6UtilFwdTblGetBestAnycastRouteInCxt (pRtm6Cxt, pFirstEntry,
                                                    &pIp6BestRtEntry);
    }
    else
    {
        Rtm6UtilFwdTblGetBestRouteInCxt (pRtm6Cxt, pFirstEntry,
                                         &pIp6BestRtEntry);
    }
    *pu1FoundFlag = FALSE;
    Rtm6UtilCopyRtInfoInCxt (pRtm6Cxt, pIp6BestRtEntry, pNetIpv6RtInfo);
    UNUSED_PARAM (i4OutCome);
    return RTM6_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : Purges out a route from the routing table
 *
 * INPUTS      : pRtm6Cxt - RTM6 Context Pointer                       
 *             : pRt -The routing table entry ptr to be deleted (pRt)
 *
 * OUTPUTS     : None
 *
 * RETURNS     : IP6_SUCCESS - if successful in purging out the route
 *               IP6_FAILURE - otherwise
 *
 * NOTES       : Tries to optimize the delete by recursively deleting the 
 *               parent of the deleted node 
 ******************************************************************************/

INT4
Rtm6UtilRoutePurgeInCxt (tRtm6Cxt * pRtm6Cxt, tIp6RtEntry * pRt)
{
    tNetIpv6RtInfo      NetIpv6RtInfo;
    tRtm6FrtInfo       *pRtm6FrtNode = NULL;

    MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));
    MEMCPY (&NetIpv6RtInfo.Ip6Dst, &pRt->dst, sizeof (tIp6Addr));
    NetIpv6RtInfo.u1Prefixlen = pRt->u1Prefixlen;
    MEMCPY (&NetIpv6RtInfo.NextHop, &pRt->nexthop, sizeof (tIp6Addr));
    NetIpv6RtInfo.u4Index = pRt->u4Index;
    NetIpv6RtInfo.i1Proto = pRt->i1Proto;
    NetIpv6RtInfo.i1DefRtrFlag = pRt->i1DefRtrFlag;
    NetIpv6RtInfo.u4RowStatus = IP6FWD_DESTROY;
    NetIpv6RtInfo.u2ChgBit = IP6_BIT_STATUS;
    NetIpv6RtInfo.u4ContextId = pRtm6Cxt->u4ContextId;

    if (Rtm6UtilIpv6LeakRoute (RTM6_DELETE_ROUTE, &NetIpv6RtInfo) ==
        RTM6_FAILURE)
    {
        return IP6_FAILURE;
    }
    pRtm6FrtNode = Rtm6FrtGetInfo (pRt, pRtm6Cxt->u4ContextId);
    if (pRtm6FrtNode != NULL)
    {
        if (Rtm6FrtDeleteEntry (pRtm6FrtNode) == RTM6_SUCCESS)
        {
            Rtm6RedSyncFrtInfo (pRt, pRtm6Cxt->u4ContextId,
                                pRtm6FrtNode->u1Type, RTM6_DELETE_ROUTE);
        }
    }
    if (pRt->i1Proto == IP6_NETMGMT_PROTOID)
    {
        Rtm6CxtNotifyStaticRtDeletion (pRtm6Cxt->u4ContextId, pRt);
    }

    return IP6_SUCCESS;
}

/*
 ******************************************************************************
 * Function Name    :   Rtm6NetIpv6GetRoute
 * Description      :   This function provides the Route (either Exact Route or
 *                      Best route) for a given destination and Mask based on
 *                      the incoming request.
 * Inputs           :   pRtQuery - Infomation about the route to be
 *                                 retrieved.
 * Outputs          :   pNetIp6RtInfo - Information about the requested route.
 * Return Value     :   RTM6_SUCCESS - if the route is present.
 *                      RTM6_FAILURE - if the route is not present.
 ******************************************************************************
 */
INT4
Rtm6NetIpv6GetRoute (tNetIpv6RtInfoQueryMsg * pRtQuery,
                     tNetIpv6RtInfo * pNetIp6RtInfo)
{
    tIp6RtEntry         Ip6RtEntry;
    tIp6RtEntry        *pIp6RtEntry = NULL;
    tIp6RtEntry        *pIp6BestRtEntry = NULL;
    tIp6RtEntry        *pTempIp6RtEntry = NULL;
    tRtm6Cxt           *pRtm6Cxt = NULL;
    VOID               *pRibNode = NULL;
    INT4                i4OutCome = RTM6_FAILURE;
    INT4                i4RetVal = RTM6_FAILURE;
    UINT2               u2AppId = 0;
    INT1                i1ProtoId = 0;

    pRtm6Cxt = UtilRtm6GetCxt (pRtQuery->u4ContextId);

    if (pRtm6Cxt == NULL)
    {
        return RTM6_FAILURE;
    }

    MEMSET (&Ip6RtEntry, 0, sizeof (tIp6RtEntry));

    MEMCPY (&Ip6RtEntry.dst, &pNetIp6RtInfo->Ip6Dst, IP6_ADDR_SIZE);
    Ip6RtEntry.u1Prefixlen = pNetIp6RtInfo->u1Prefixlen;
    Ip6RtEntry.i1Proto = ALL_PROTO_ID + 1;

    if (pRtQuery->u1QueryFlag == RTM6_QUERIED_FOR_NEXT_HOP)
    {
        /* Query is for Next Hop */
        i4OutCome =
            Rtm6TrieLookupEntryInCxt (pRtm6Cxt, &Ip6RtEntry, &pIp6RtEntry,
                                      &pRibNode);
    }
    else
    {
        /* Query is for exact destination */
        i4OutCome =
            Rtm6TrieSearchExactEntryInCxt (pRtm6Cxt, &Ip6RtEntry, &pIp6RtEntry,
                                           &pRibNode);
    }

    if (i4OutCome == RTM6_SUCCESS)
    {
        for (pTempIp6RtEntry = pIp6RtEntry; pTempIp6RtEntry != NULL;
             pTempIp6RtEntry = pTempIp6RtEntry->pNextRtEntry)
        {
            i1ProtoId = pTempIp6RtEntry->i1Proto;

            if (((pRtQuery->u2AppIds >> (i1ProtoId - 1)) & 0x0001) == 1)
            {
                u2AppId &= (~(0x0001 << (i1ProtoId - 1)));
            }
        }
    }
    else
    {
        /* Route is not present. */
        return RTM6_FAILURE;
    }

    /* Get the Best route  and fill the route structure. */

    if (pIp6RtEntry->u1AddrType == ADDR6_ANYCAST)
    {
        i4RetVal =
            Rtm6UtilFwdTblGetBestAnycastRouteInCxt (pRtm6Cxt, pIp6RtEntry,
                                                    &pIp6BestRtEntry);
    }
    else
    {
        i4RetVal =
            Rtm6UtilFwdTblGetBestRouteInCxt (pRtm6Cxt, pIp6RtEntry,
                                             &pIp6BestRtEntry);
    }
    if (i4RetVal == IP6_FAILURE)
    {
        /* Best Route is not present. */
        return RTM6_FAILURE;
    }

    MEMCPY (&pNetIp6RtInfo->Ip6Dst, &pIp6RtEntry->dst, IP6_ADDR_SIZE);
    pNetIp6RtInfo->u1Prefixlen = pIp6RtEntry->u1Prefixlen;
    MEMCPY (&pNetIp6RtInfo->NextHop, &pIp6RtEntry->nexthop, IP6_ADDR_SIZE);
    pNetIp6RtInfo->u4Index = pIp6RtEntry->u4Index;
    pNetIp6RtInfo->u4Metric = pIp6RtEntry->u4Metric;
    pNetIp6RtInfo->u4RouteTag = pIp6RtEntry->u4RouteTag;
    pNetIp6RtInfo->i1Proto = pIp6RtEntry->i1Proto;
    pNetIp6RtInfo->u1Preference = pIp6RtEntry->u1Preference;
    pNetIp6RtInfo->u4RowStatus = pIp6RtEntry->u4RowStatus;

    /* Update the BIT field with the AppId field */
    pNetIp6RtInfo->u2ChgBit = u2AppId;
    pNetIp6RtInfo->u4ContextId = pRtm6Cxt->u4ContextId;

    return RTM6_SUCCESS;
}

/*-------------------------------------------------------------------+
 *
 * Function           : Rtm6UtilDeregister
 *
 * Input(s)           : pRtm6Cxt - Rtm6 Context Pointer
 *                    : pRegnId - DeRegistraion Info of the routing protocol
 *                      which sent this DeRegistration message.
 *
 * Output(s)          : Updation of Registration table 
 *
 * Returns            : RTM6_SUCCESS/RTM6_FAILURE
 *
 * Action             : This function handles the DeRegistration message  
 *                      from the routing protocols.
 * 
+-------------------------------------------------------------------*/
INT4
Rtm6UtilDeregister (tRtm6Cxt * pRtm6Cxt, tRtm6RegnId * pRegnId)
{
    UINT2               u2RegnId = 0;
    UINT2               u2Index = 0;
    UINT2               u2PrevIndex = 0;

    u2RegnId = Rtm6GetRegnIndexFromRegnIdInCxt (pRtm6Cxt, pRegnId);

    if (u2RegnId == RTM6_INVALID_REGN_ID)
    {
        return RTM6_FAILURE;
    }

    /* Reinitialise the registration table and update start and last index */
    if (pRtm6Cxt->u2Rtm6RtStartIndex == u2RegnId)
    {
        if (pRtm6Cxt->u2Rtm6RtLastIndex == u2RegnId)
        {
            pRtm6Cxt->u2Rtm6RtLastIndex =
                pRtm6Cxt->aRtm6RegnTable[u2RegnId].u2NextRegId;
        }
        pRtm6Cxt->u2Rtm6RtStartIndex =
            pRtm6Cxt->aRtm6RegnTable[u2RegnId].u2NextRegId;
    }
    else
    {
        /* this routing protocol sits in between other routing protocols or it
         * is  at the last */
        for (u2Index = pRtm6Cxt->u2Rtm6RtStartIndex,
             u2PrevIndex = pRtm6Cxt->u2Rtm6RtStartIndex;
             u2Index < IP6_MAX_ROUTING_PROTOCOLS;
             u2Index = pRtm6Cxt->aRtm6RegnTable[u2Index].u2NextRegId)
        {
            if (u2Index == u2RegnId)
            {
                if (pRtm6Cxt->u2Rtm6RtLastIndex == u2RegnId)
                {
                    pRtm6Cxt->u2Rtm6RtLastIndex = u2PrevIndex;
                }
                pRtm6Cxt->aRtm6RegnTable[u2PrevIndex].u2NextRegId =
                    pRtm6Cxt->aRtm6RegnTable[u2Index].u2NextRegId;
                break;
            }
            u2PrevIndex = u2Index;
        }                        /* For loop */

    }

    /* Clear the Protocol route from TRIE */
    Rtm6ClearProtoRouteFromTrieInCxt (pRtm6Cxt, pRegnId);

    Rtm6ReInitRtInCxt (pRtm6Cxt, pRegnId);
    RTM6_TRC_ARG1 (pRtm6Cxt->u4ContextId, RTM6_CTRL_PATH_TRC, RTM6_MOD_NAME,
                   "\tProtocol [%d] De-Registration Successful\n",
                   pRegnId->u2ProtoId);
    return RTM6_SUCCESS;
}

/*-------------------------------------------------------------------+
 *
 * Function           : Rtm6UtilGetTraceFlag
 *
 * Input(s)           : u4Rtm6CxtId - Rtm6 Context ID
 *
 * Output(s)          : TraceFlag of the Context-Id
 *
 * Returns            : u4TraceFlag
 *
 * Action             : This function returns trace option for the 
 *                       context-ID.
 * 
+-------------------------------------------------------------------*/

PUBLIC UINT4
Rtm6UtilGetTraceFlag (UINT4 u4Rtm6CxtId)
{
    if (u4Rtm6CxtId == RTM6_INVALID_CXT_ID)
    {
        return (gRtm6GlobalInfo.u4GlobalTrcFlag);
    }

    if (u4Rtm6CxtId >= gRtm6GlobalInfo.u4MaxContextLimit)
    {
        return 0;
    }
    if ((UtilRtm6VcmIsVcExist (u4Rtm6CxtId) == RTM6_FAILURE) ||
        (gRtm6GlobalInfo.apRtm6Cxt[u4Rtm6CxtId] == NULL))
    {
        return 0;
    }
    return (gRtm6GlobalInfo.apRtm6Cxt[u4Rtm6CxtId])->u4TraceFlag;
}

/*-------------------------------------------------------------------+
 *
 * Function           : Rtm6UtilGetGblRrd6Status
 *
 * Input(s)           : None
 *
 * Output(s)          : None
 *
 * Returns            : Global RRD6 status
 *
 * Action             : This function returns true if RRD6_WANTED
 *                      is defined otherwise returns false. 
 * 
+-------------------------------------------------------------------*/

PUBLIC UINT1
Rtm6UtilGetGblRrd6Status (VOID)
{
    return ((UINT1) gRtm6GlobalInfo.bRrd6Status);
}

/*-------------------------------------------------------------------+
 *
 * Function           : Rtm6UtilSetFlagforRoutesInNH
 *
 * Input(s)           : pRtm6Cxt - Pointer to RTM6 context
 *                      pPNh6Entry - Pointer to the Next Hop Entry
 *                      u4ReachFlag - RTM6_SET_RT_REACHABLE\RTM6_DONT_SET_RT_REACHABLE
 *
 * Output(s)          : None
 *
 * Returns            : None
 *
 * Action             : This function returns true if RRD6_WANTED
 *                      is defined otherwise returns false. 
 * 
+-------------------------------------------------------------------*/
VOID
Rtm6UtilSetFlagforRoutesInNH (tRtm6Cxt * pRtm6Cxt, tPNh6Node * pPNh6Entry,
                              UINT4 u4ReachFlag)
{
    tPRt6Entry         *pPRt6 = NULL;
    tPRt6Entry         *pPRt6NextEntry = NULL;

    if (u4ReachFlag == RTM6_SET_RT_REACHABLE)
    {
        if (pPNh6Entry != NULL)
        {
            Rtm6RelLock ();        /* Route Entry List Lock */
            if (pPNh6Entry->pRtm6Cxt->u4ContextId == pRtm6Cxt->u4ContextId)
            {
                TMO_DYN_SLL_Scan (&pPNh6Entry->routeEntryList, pPRt6,
                                  pPRt6NextEntry, tPRt6Entry *)
                {
                    pPRt6->pPend6Rt->u4Flag |= RTM6_RT_REACHABLE;
                }
            }
            Rtm6RelUnLock ();    /* Route Entry List UnLock */
        }
    }
    if (u4ReachFlag == RTM6_DONT_SET_RT_REACHABLE)
    {
        if (pPNh6Entry != NULL)
        {
            Rtm6RelLock ();        /* Route Entry List Lock */
            if (pPNh6Entry->pRtm6Cxt->u4ContextId == pRtm6Cxt->u4ContextId)
            {
                TMO_DYN_SLL_Scan (&pPNh6Entry->routeEntryList, pPRt6,
                                  pPRt6NextEntry, tPRt6Entry *)
                {
                    /* Delete the pending route entry from the list */
                    pPRt6->pPend6Rt->u4Flag &= (UINT4) (~RTM6_RT_REACHABLE);
                }
            }
            Rtm6RelUnLock ();    /* Route Entry List UnLock */
        }
    }

}

/************************************************************************
 *  Function Name   : Rtm6UtilRetryNpPrgForFailedRoute
 *  Description     : An util to retry rogramming the given route 
                      in hardware.
 *  Input           : pRtInfo -  route to be inserted
 *  Output          : None
 *  Returns         : None
 ************************************************************************/
INT4
Rtm6UtilRetryNpPrgForFailedRoute (tRtm6FrtInfo * pRtm6FrtInfo)
{
    INT4                i4OutCome = RTM6_FAILURE;
#ifdef NPAPI_WANTED
    tRtm6Cxt           *pRtm6Cxt = NULL;
    tIp6Addr            AddrMask;
    tIp6RtEntry         RtEntry;
    tIp6RtEntry        *pIp6Rt = NULL;
    tFsNpIntInfo        IntInfo;
    VOID               *pRibNode = NULL;
    UINT4               u4NHType = 0;

    MEMSET ((UINT1 *) &AddrMask, IP6_UINT1_ALL_ONE, sizeof (tIp6Addr));
    MEMSET (&IntInfo, 0, sizeof (tFsNpIntInfo));
    pRtm6Cxt = UtilRtm6GetCxt (pRtm6FrtInfo->u4CxtId);
    if (pRtm6Cxt == NULL)
    {
        i4OutCome = RTM6_FAILURE;
        return i4OutCome;
    }

    MEMSET (&RtEntry, 0, sizeof (tIp6RtEntry));
    MEMCPY (&RtEntry.dst, &pRtm6FrtInfo->DestPrefix, sizeof (tIp6Addr));
    RtEntry.u1Prefixlen = pRtm6FrtInfo->u1PrefixLen;
    RtEntry.i1Proto = pRtm6FrtInfo->i1Proto;

    if (Rtm6TrieSearchExactEntryInCxt (pRtm6Cxt, &RtEntry,
                                       &pIp6Rt, &pRibNode) != IP6_FAILURE)
    {
        /* Route entry is available, check for the matching route entry 
         * based on nexthop and tos, by traversing through all 
         * alternate paths. */

        /* Next-hop identifies the exact route */

        while (pIp6Rt != NULL)
        {
            if (MEMCMP
                (&(pIp6Rt->nexthop), &(pRtm6FrtInfo->NextHopAddr),
                 sizeof (tIp6Addr)) == 0)
            {
                IntInfo.u1IfType = Ip6GetIfType (pIp6Rt->u4Index);
                if ((IntInfo.u1IfType == CFA_L3IPVLAN)
                    || (IntInfo.u1IfType == CFA_L3SUB_INTF))
                {
                    IntInfo.u4PhyIfIndex = pIp6Rt->u4Index;
                }
                else if (IntInfo.u1IfType == IP6_PSEUDO_WIRE_INTERFACE_TYPE)
                {
                    IntInfo.u4PhyIfIndex = pIp6Rt->u4Index;
                }
                if (IntInfo.u1IfType != IP6_ENET_INTERFACE_TYPE)
                {
                    if (CfaGetVlanId (IntInfo.u4PhyIfIndex, &(IntInfo.u2VlanId))
                        == CFA_FAILURE)
                    {
                        i4OutCome = RTM6_FAILURE;
                        break;
                    }
                }
                else
                {
                    IntInfo.u4PhyIfIndex = pIp6Rt->u4Index;
                    IntInfo.u2VlanId = 0;
                }
                if (pIp6Rt->i1Type == IP6_ROUTE_TYPE_DIRECT)
                {
                    u4NHType = NH_DIRECT;
                }
                else if (pIp6Rt->i1Type == IP6_ROUTE_TYPE_DISCARD)
                {
                    u4NHType = NH_DISCARD;
                }
                else if (pIp6Rt->i1Type == IP6_ROUTE_TYPE_INDIRECT)
                {
                    u4NHType = NH_REMOTE;
                }
                else
                {
                    /* Unknown Route Type. */
                    u4NHType = NH_SENDTO_CP;
                }

                if (RTM6_GET_NODE_STATUS () == RM_ACTIVE)
                {
                    pIp6Rt->u1HwStatus = NP_NOT_PRESENT;
                    Rtm6RedAddDynamicInfo (pIp6Rt, pRtm6Cxt->u4ContextId);
                    Rtm6RedSendDynamicInfo ();
                }
                if (RTM6_IS_NP_PROGRAMMING_ALLOWED () == RTM6_SUCCESS)
                {
                    /*Set the reachability state for routes and passing the no of 
                     *routes to HW*/
                    IntInfo.u1RouteCount = pRtm6FrtInfo->u1RtCount;
                    if (Ipv6FsNpIpv6UcRouteAdd (pRtm6Cxt->u4ContextId,
                                                (UINT1 *) &pRtm6FrtInfo->
                                                DestPrefix,
                                                pRtm6FrtInfo->u1PrefixLen,
                                                (UINT1 *) &pRtm6FrtInfo->
                                                NextHopAddr, u4NHType,
                                                &IntInfo) == FNP_FAILURE)
                    {
                        RTM6_TRC_ARG2 (pRtm6Cxt->u4ContextId,
                                       RTM6_ALL_FAILURE_TRC |
                                       RTM6_OS_RESOURCE_TRC, RTM6_MOD_NAME,
                                       "ERROR[RTM]: IPV6 Failed Route Retry Failed%s/%d   \n",
                                       Ip6PrintNtop (&pIp6Rt->dst),
                                       (INT4) pIp6Rt->u1Prefixlen);

                        ;
                        i4OutCome = RTM6_FAILURE;
                    }
                    pIp6Rt->u4HwIntfId[0] = IntInfo.u4HwIntfId[0];

                }
                if (RTM6_GET_NODE_STATUS () == RM_ACTIVE)
                {
                    pIp6Rt->u1HwStatus = NP_PRESENT;
                    Rtm6RedAddDynamicInfo (pIp6Rt, pRtm6Cxt->u4ContextId);
                    Rtm6RedSendDynamicInfo ();
                }
                i4OutCome = RTM6_SUCCESS;
                break;

            }
            pIp6Rt = (pIp6Rt)->pNextAlternatepath;
        }
    }
#else
    UNUSED_PARAM (pRtm6FrtInfo);
    i4OutCome = RTM6_SUCCESS;
#endif
    return i4OutCome;
}

#ifndef LNXIP6_WANTED
/*-------------------------------------------------------------------+
 *
 * Function           : Rtm6UtilGetNdStateForRouteNH
 *
 * Input(s)           : u4ContextId- Pointer to RTM6 context
 *                      pIp6RtEntry- Pointer to the Route entry for which NH has to be checked 
 *
 * Output(s)          : None
 *
 * Returns            : TRUE/FALSE
 *
 * Action             : This function checks if the NH is REACHABLE(orSTATIC) - TRUE 
 *                      else FALSE
 * 
+-------------------------------------------------------------------*/

INT4
Rtm6UtilGetNdStateForRouteNH (UINT4 u4ContextId, tIp6RtEntry * pIp6RtEntry)
{

    /*This functions finds the reach state for the NH in the route entry.
     * It queries the ND6 module of IPv6 and returns TRUE if the NH is reachable.
     */
    UINT4               u4IfIndex;
    tNetIpv6RtInfo      NetIpv6RtInfo;
    tNetIpv6RtInfoQueryMsg RtQuery;
    tIp6If             *pIf6 = NULL;
    tPNh6Node          *pPNh6Entry = NULL, ExstNh6Node;

    MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));
    MEMSET (&RtQuery, 0, sizeof (tNetIpv6RtInfoQueryMsg));
    RtQuery.u4ContextId = u4ContextId;
    RtQuery.u1QueryFlag = RTM6_QUERIED_FOR_NEXT_HOP;
    Ip6AddrCopy (&(NetIpv6RtInfo.Ip6Dst), &(pIp6RtEntry->nexthop));
    NetIpv6RtInfo.u1Prefixlen = IP6_ADDR_MAX_PREFIX;

    ExstNh6Node.NextHop = pIp6RtEntry->nexthop;
    ExstNh6Node.pRtm6Cxt = UtilRtm6GetCxt (u4ContextId);
    if (!(IS_ADDR_LLOCAL (pIp6RtEntry->nexthop)))
    {
        if (Rtm6NetIpv6GetRoute (&RtQuery, &NetIpv6RtInfo) == RTM6_SUCCESS)
        {
            u4IfIndex = NetIpv6RtInfo.u4Index;

            pIf6 = Ip6ifGetEntry (u4IfIndex);
            if (pIf6 == NULL)
            {
                return FALSE;
            }

            pPNh6Entry = RBTreeGet (gRtm6GlobalInfo.pPRT6RBRoot, &ExstNh6Node);
            if (pPNh6Entry == NULL)
            {
                /*Search in the pNRT table */
                pPNh6Entry =
                    RBTreeGet (gRtm6GlobalInfo.pRNHT6RBRoot, &ExstNh6Node);
                if (pPNh6Entry != NULL)
                {
                    return TRUE;
                }
                else
                {
                    return FALSE;
                }
            }
        }
    }
    else
    {
        /*Search in the pNRT table */
        pPNh6Entry = RBTreeGet (gRtm6GlobalInfo.pRNHT6RBRoot, &ExstNh6Node);
        if (pPNh6Entry != NULL)
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }
    return FALSE;

}
#endif
/*-------------------------------------------------------------------+
 *
 * Function           : Rtm6AddNextHopForRoute
 *
 * Input(s)           : pRtm6Cxt- Pointer to RTM6 context
 *                      pNextHop- Pointer to the Next Hop entry
 *
 * Output(s)          : None
 *
 * Returns            : IP6_FAILURE/IP6_SUCCESS
 *
 * Action             : This function adds the next-hops of all routes other that connected routes.
 *                      This is used for HitBit implementation.
 * 
+-------------------------------------------------------------------*/

INT4
Rtm6AddNextHopForRoute (tRtm6Cxt * pRtm6Cxt, tIp6RtEntry * pIp6RtEntry)
{

    tPNHopHitRtNode    *pNh6Entry = NULL;
    tPNHopHitRtNode     Exst6NhNode;
    tPRt6Entry         *pPRt6 = NULL;

    /* Allocate for Route node */
    if ((pPRt6 = (tPRt6Entry *) MemAllocMemBlk (gRtm6GlobalInfo.Rtm6URRtPoolId))
        == NULL)
    {
        return RTM6_FAILURE;
    }

    /*Check whether Next hop entry already exists */
    MEMSET (&Exst6NhNode, 0, sizeof (tPNHopHitRtNode));
    MEMCPY (&(Exst6NhNode.NextHop), &pIp6RtEntry->nexthop, sizeof (tIp6Addr));
    Exst6NhNode.pRtm6Cxt = pRtm6Cxt;

    pNh6Entry = RBTreeGet (gRtm6GlobalInfo.pNextHopRBRoot, &Exst6NhNode);
    if (pNh6Entry == NULL)
    {
        if ((pNh6Entry = (tPNHopHitRtNode *) MemAllocMemBlk
             (gRtm6GlobalInfo.Rtm6RouteNextHopPoolId)) == NULL)
        {
            MemReleaseMemBlock (gRtm6GlobalInfo.Rtm6URRtPoolId,
                                (UINT1 *) pPRt6);
            return IP6_FAILURE;
        }
        MEMCPY (&pNh6Entry->NextHop, &pIp6RtEntry->nexthop, sizeof (tIp6Addr));
        pNh6Entry->pRtm6Cxt = pRtm6Cxt;
        pNh6Entry->NextHopUsersCount = 1;

        /* Add the  next hop Entry to RBTree */
        if (RBTreeAdd (gRtm6GlobalInfo.pNextHopRBRoot, pNh6Entry) == RB_FAILURE)
        {
            MemReleaseMemBlock (gRtm6GlobalInfo.Rtm6URRtPoolId,
                                (UINT1 *) pPRt6);
            MemReleaseMemBlock (gRtm6GlobalInfo.Rtm6RouteNextHopPoolId,
                                (UINT1 *) pNh6Entry);
            return IP6_FAILURE;
        }
        TMO_SLL_Init (&(pNh6Entry->routeEntryList));
    }
    else
    {
        pNh6Entry->NextHopUsersCount++;
    }
    TMO_SLL_Init_Node (((tTMO_SLL_NODE *) pPRt6));
    pPRt6->pPend6Rt = pIp6RtEntry;
    pPRt6->pRtm6Cxt = pRtm6Cxt;

    Rtm6RelLock ();                /* Route Entry List Lock */
    /* Insert the route entry list to the RBTree */
    TMO_SLL_Insert (&(pNh6Entry->routeEntryList), NULL,
                    ((tTMO_SLL_NODE *) pPRt6));
    Rtm6RelUnLock ();            /* Route Entry List UnLock */

    return IP6_SUCCESS;
}

/*-------------------------------------------------------------------+
 *
 * Function           : Rtm6DeleteNextHopforRoute
 *
 * Input(s)           : pRtm6Cxt- Pointer to RTM6 context
 *                      pNextHop- Pointer to the Next Hop entry
 *
 * Output(s)          : None
 *
 * Returns            : IP6_FAILURE/IP6_SUCCESS
 *
 * Action             : This function removes the next-hops of routes from pNextHopRBRoot.
 *                      This is used for HitBit implementation.
 * 
+-------------------------------------------------------------------*/
INT4
Rtm6DeleteNextHopforRoute (tRtm6Cxt * pRtm6Cxt, tIp6RtEntry * pIp6RtEntry)
{
    tPNHopHitRtNode     Exst6NhNode;
    tPNHopHitRtNode    *pNh6Entry = NULL;
    tPRt6Entry         *pRtCur6Entry = NULL;

    MEMSET (&Exst6NhNode, 0, sizeof (tPNHopHitRtNode));
    Exst6NhNode.pRtm6Cxt = pRtm6Cxt;
    MEMCPY (&(Exst6NhNode.NextHop), &pIp6RtEntry->nexthop, sizeof (tIp6Addr));

    pNh6Entry = RBTreeGet (gRtm6GlobalInfo.pNextHopRBRoot, &Exst6NhNode);
    if (pNh6Entry == NULL)
    {
        return IP6_FAILURE;
    }

    Rtm6RelLock ();                /* Route Entry List Lock */
    TMO_SLL_Scan (&(pNh6Entry->routeEntryList), pRtCur6Entry, tPRt6Entry *)
    {
        if (pRtCur6Entry->pRtm6Cxt->u4ContextId == pRtm6Cxt->u4ContextId)
        {
            if ((MEMCMP
                 (&(pRtCur6Entry->pPend6Rt->dst), &(pIp6RtEntry->dst),
                  sizeof (tIp6Addr)) == IP6_ZERO)
                && (pRtCur6Entry->pPend6Rt->i1Proto == pIp6RtEntry->i1Proto))
            {
                break;
            }
        }
    }

    if (pRtCur6Entry == NULL)
    {
        Rtm6RelUnLock ();        /* Route Entry List UnLock */
        return IP6_FAILURE;
    }

/* Delete the pending route entry from the list */
    TMO_SLL_Delete (&(pNh6Entry->routeEntryList),
                    (tTMO_SLL_NODE *) pRtCur6Entry);
    MemReleaseMemBlock (gRtm6GlobalInfo.Rtm6URRtPoolId, (UINT1 *) pRtCur6Entry);
    Rtm6RelUnLock ();            /* Route Entry List UnLock */

    if (TMO_SLL_Count (&(pNh6Entry->routeEntryList)) == 0)
    {
        /* No pending route exists in the list.Delete the next hop
         * entry from RB Tree */
        if (RBTreeRemove (gRtm6GlobalInfo.pNextHopRBRoot, pNh6Entry) ==
            RB_FAILURE)
        {
            return IP6_FAILURE;
        }
        MemReleaseMemBlock (gRtm6GlobalInfo.Rtm6RouteNextHopPoolId,
                            (UINT1 *) pNh6Entry);
    }

    else
    {
        pNh6Entry->NextHopUsersCount--;
    }

    return IP6_SUCCESS;

}

/*-------------------------------------------------------------------+
 *
 * Function           : Rtm6GetNextReachableNextHopInCxt
 *
 * Input(s)           : u4ContextId- Pointer to RTM6 context
 *                      pNextHopAddr6 - Next Hop Addr
 *
 * Output(s)          : None
 *
 * Returns            : TRUE/FALSE
 *
 * Action             : This function checks if the NH is REACHABLE(orSTATIC) - TRUE
 *                      else FALSE
 *
+-------------------------------------------------------------------*/
INT4
Rtm6GetReachableNextHopInCxt (UINT4 u4ContextId, tIp6Addr * pNextHop)
{
    tPNHopHitRtNode     Exst6NhNode;
    tPNHopHitRtNode    *pNh6Entry = NULL;
    tRtm6Cxt           *pRtm6Cxt = NULL;

    pRtm6Cxt = UtilRtm6GetCxt (u4ContextId);
    MEMSET (&Exst6NhNode, 0, sizeof (tPNHopHitRtNode));
    Exst6NhNode.pRtm6Cxt = pRtm6Cxt;

    MEMCPY (&(Exst6NhNode.NextHop), pNextHop, sizeof (tIp6Addr));

    pNh6Entry = RBTreeGet (gRtm6GlobalInfo.pNextHopRBRoot, &Exst6NhNode);
    if (pNh6Entry == NULL)
    {
        return FALSE;
    }
    return TRUE;
}
