/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rtm6prt.c,v 1.9.22.1 2018/03/09 13:40:48 siva Exp $
 *
 * Description:This file contains functions needed for Route Driven 
               implementation.Routes that are having Invalid NextHop
               (i.e UnResolved NextHop)  are maintained in Pending 
                Route Table.Routines that access this PRT are 
                maintained in this File.               
 *******************************************************************/
#include "rtm6inc.h"
UINT4               gu4ECMP6TmrFlag = 1;
/*****************************************************************************/
/* Function     : Rtm6RBComparePNh                                            */
/*                                                                           */
/* Description  : Compare the Next hop entries in PRT                        */
/*                                                                           */
/* Input        : e1        Pointer to Next hop Node node1                   */
/*                e2        Pointer to Next hop Node node2                   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : 1. RB_EQUAL if keys of both the elements are same.         */
/*                2. RB_LESS key of first element is less than second        */
/*                element key.                                               */
/*                3. RB_GREATER if key of first element is greater than      */
/*                second element key                                         */
/*****************************************************************************/
INT4
Rtm6RBComparePNh (tRBElem * e1, tRBElem * e2)
{
    tPNh6Node          *pPNhEntry1 = e1;
    tPNh6Node          *pPNhEntry2 = e2;
    INT4                i4RetVal = 0;

    if (pPNhEntry1->pRtm6Cxt->u4ContextId > pPNhEntry2->pRtm6Cxt->u4ContextId)
    {
        return RTM6_RB_GREATER;
    }
    else if (pPNhEntry1->pRtm6Cxt->u4ContextId <
             pPNhEntry2->pRtm6Cxt->u4ContextId)
    {
        return RTM6_RB_LESS;
    }

    /* Context ID is equal, so validate the nexthop */

    i4RetVal = Ip6AddrCompare (pPNhEntry1->NextHop, pPNhEntry2->NextHop);
    if (i4RetVal == IP6_ZERO)
    {
        return RTM6_RB_EQUAL;
    }
    else if (i4RetVal == IP6_ONE)
    {
        return RTM6_RB_GREATER;
    }
    return RTM6_RB_LESS;
}

/*****************************************************************************
 * Function     : Rtm6AddNHToPRTInCxt                       
 * Description  : Add the Next Hop in RBTree maintained  for PRT.                                
 * Input        : pIp6Addr - Next Hop IP Addr
 *                u4ContextId -Context ID
 * Output       : None                                      
 * Returns      : RTM6_SUCCESS/RTM6_FAILURE                
 *****************************************************************************/
INT4
Rtm6AddNhToPRTInCxt (UINT4 u4ContextId, tIp6Addr * pIp6Addr)
{
    tPNh6Node          *pPNh6Entry = NULL, ExstNh6Node;
    tRtm6Cxt           *pRtm6Cxt = NULL;

    pRtm6Cxt = UtilRtm6GetCxt (u4ContextId);
    if (pRtm6Cxt == NULL)
    {
        return RTM6_FAILURE;
    }

    Ip6AddrCopy (&ExstNh6Node.NextHop, pIp6Addr);
    ExstNh6Node.pRtm6Cxt = pRtm6Cxt;

    pPNh6Entry = RBTreeGet (gRtm6GlobalInfo.pPRT6RBRoot, &ExstNh6Node);

    if (pPNh6Entry == NULL)
    {
        /* Add the entry to the Pending next hop entry */

        /* Allocate the Pending Next hop node */
        if ((pPNh6Entry = (tPNh6Node *)
             MemAllocMemBlk (gRtm6GlobalInfo.Rtm6PNextHopPoolId)) == NULL)
        {
            return RTM6_FAILURE;
        }
        Ip6AddrCopy (&(pPNh6Entry->NextHop), pIp6Addr);
        pPNh6Entry->pRtm6Cxt = pRtm6Cxt;

        /* Add the Pending next hop Entry to RBTree */
        if (RBTreeAdd (gRtm6GlobalInfo.pPRT6RBRoot, pPNh6Entry) == RB_FAILURE)
        {
            MemReleaseMemBlock (gRtm6GlobalInfo.Rtm6PNextHopPoolId,
                                (UINT1 *) pPNh6Entry);
            return RTM6_FAILURE;
        }
        TMO_SLL_Init (&(pPNh6Entry->routeEntryList));
    }

    return RTM6_SUCCESS;
}

/*****************************************************************************
 * Function     : Rtm6AddRtToPRTInCxt                                         *
 * Description  : Add the Route pointer in Trie to the RBTree maintained     *
 *                for PRT.                                                   *
 * Input        : pPRoute - Pointer to the Route Added to trie               *
 *                pRtmCxt -RTM Context Pointer                               *
 *                u1DropRoute - drop route flag
 * Output       : None                                                       *
 * Returns      : RTM6_SUCCESS/RTM6_FAILURE                                      *
 *****************************************************************************/
INT4
Rtm6AddRtToPRTInCxt (tRtm6Cxt * pRtm6Cxt, tIp6RtEntry * pPRoute)
{
    tPNh6Node          *pPNh6Entry = NULL, ExstNh6Node;
    tPRt6Entry         *pPRt6 = NULL;
    tIp6Addr            tTmpAddr;

    /*If the route is having only directly connected routes or there
     *is no next Hop, nothing needs to be added to PRT*/
    MEMSET (&tTmpAddr, 0, sizeof (tIp6Addr));
    if (MEMCMP (&pPRoute->nexthop, &tTmpAddr, sizeof (tIp6Addr)) == 0)
    {
        return IP6_SUCCESS;
    }
    if (pPRoute == NULL)
    {
        return RTM6_FAILURE;
    }
    if ((pPRoute->u4Flag & RTM6_RT_IN_PRT) == RTM6_RT_IN_PRT)
    {
        /* Route is already present in PRT. So don't add it to SLL again */
        return RTM6_SUCCESS;
    }

    /* Allocate for Pending Route node */
    if ((pPRt6 = (tPRt6Entry *) MemAllocMemBlk (gRtm6GlobalInfo.Rtm6URRtPoolId))
        == NULL)
    {
        return RTM6_FAILURE;
    }

    /* Check if already the Pending Next hop entry has the current entry */
    ExstNh6Node.NextHop = pPRoute->nexthop;
    ExstNh6Node.pRtm6Cxt = pRtm6Cxt;

    pPNh6Entry = RBTreeGet (gRtm6GlobalInfo.pPRT6RBRoot, &ExstNh6Node);

    if (pPNh6Entry == NULL)
    {
        /* Allocate the Pending Next hop node */
        if ((pPNh6Entry = (tPNh6Node *)
             MemAllocMemBlk (gRtm6GlobalInfo.Rtm6PNextHopPoolId)) == NULL)
        {
            MemReleaseMemBlock (gRtm6GlobalInfo.Rtm6URRtPoolId,
                                (UINT1 *) pPRt6);
            return RTM6_FAILURE;
        }
        Ip6AddrCopy (&(pPNh6Entry->NextHop), &(pPRoute->nexthop));
        pPNh6Entry->pRtm6Cxt = pRtm6Cxt;

        /* Add the Pending next hop Entry to RBTree */
        if (RBTreeAdd (gRtm6GlobalInfo.pPRT6RBRoot, pPNh6Entry) == RB_FAILURE)
        {
            MemReleaseMemBlock (gRtm6GlobalInfo.Rtm6URRtPoolId,
                                (UINT1 *) pPRt6);
            MemReleaseMemBlock (gRtm6GlobalInfo.Rtm6PNextHopPoolId,
                                (UINT1 *) pPNh6Entry);
            return RTM6_FAILURE;
        }
        TMO_SLL_Init (&(pPNh6Entry->routeEntryList));
    }

    TMO_SLL_Init_Node (((tTMO_SLL_NODE *) pPRt6));
    pPRoute->i1Type = IP6_ROUTE_TYPE_DISCARD;
    pPRoute->u4Flag |= RTM6_RT_IN_PRT;
    pPRt6->pPend6Rt = pPRoute;
    pPRt6->pRtm6Cxt = pRtm6Cxt;

    /* Insert the route entry list to the RBTree */
    TMO_SLL_Insert (&(pPNh6Entry->routeEntryList), NULL,
                    ((tTMO_SLL_NODE *) pPRt6));

    return RTM6_SUCCESS;
}

/*****************************************************************************/
/* Function     : Rtm6RBCompareRNh                                            */
/*                                                                           */
/* Description  : Compare the Next hop entries in Resolved next hop table    */
/*                                                                           */
/* Input        : e1        Pointer to Next hop Node node1                   */
/*                e2        Pointer to Next hop Node node2                   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : 1. RB6_EQUAL if keys of both the elements are same.         */
/*                2. RB6_LESS key of first element is less than second        */
/*                element key.                                               */
/*                3. RB6_GREATER if key of first element is greater than      */
/*                second element key                                         */
/*****************************************************************************/
INT4
Rtm6RBCompareRNh (tRBElem * e1, tRBElem * e2)
{
    tRNh6Node          *pNhEntry1 = e1;
    tRNh6Node          *pNhEntry2 = e2;
    INT4                i4RetVal = 0;

    if (pNhEntry1->pRtm6Cxt->u4ContextId > pNhEntry2->pRtm6Cxt->u4ContextId)
    {
        return RTM6_RB_GREATER;
    }
    else if (pNhEntry1->pRtm6Cxt->u4ContextId <
             pNhEntry2->pRtm6Cxt->u4ContextId)
    {
        return RTM6_RB_LESS;
    }
    /* Context ID is same, so validate the nexthop */
    i4RetVal = Ip6AddrCompare (pNhEntry1->NextHop, pNhEntry2->NextHop);
    if (i4RetVal == IP6_ZERO)
    {
        return RTM6_RB_EQUAL;
    }
    else if (i4RetVal == IP6_ONE)
    {
        return RTM6_RB_GREATER;
    }
    return RTM6_RB_LESS;
}

/*****************************************************************************
 * Function     : Rtm6DeleteRtFromECMP6PRT                              *
 * Description  : Delete the Node with Route Pointer from  ECMP6PRT          *
 * Input        : pEcmp6Rt - Pointer to the Route Added                      * 
 *                pRtm6Cxt - Pointer to the RTM6 context                     *
 * Output       : None                                                       *
 *                pRtm6Cxt -RTM6 Context Pointer                             *
 * Returns      : IP6_SUCCESS/IP6_FAILURE                                    *
 *****************************************************************************/

INT4
Rtm6DeleteRtFromECMP6PRT (tRtm6Cxt * pRtm6Cxt, tIp6RtEntry * pEcmp6Rt)
{
    tPNh6Node           Exst6NhNode;
    tPNh6Node          *pNh6Entry = NULL;
    UINT4               u4URNextHopCnt;
    INT4                i4Count = 0;

    Exst6NhNode.pRtm6Cxt = pRtm6Cxt;
    MEMCPY (&(Exst6NhNode.NextHop), &pEcmp6Rt->nexthop, sizeof (tIp6Addr));

    /*Checking whether the Entry for this next hop exists in
     * ECMP6PRT already*/
    pNh6Entry = RBTreeGet (gRtm6GlobalInfo.pECMP6PRTRBRoot, &Exst6NhNode);
    if (pNh6Entry == NULL)
    {
        return IP6_FAILURE;
    }

    /*Some optimization may be required here to remove from ECMP PRT */
    /*Check if some other route also using the NH entry. if not then remove the entry */
    /* No pending route exists in the list.Delete the next hop
     * entry from RB Tree */
    i4Count = Rtm6GetNextHopCount (pRtm6Cxt->u4ContextId, pEcmp6Rt);

    if (i4Count == 0)
    {
        if (RBTreeRemove (gRtm6GlobalInfo.pECMP6PRTRBRoot, pNh6Entry) ==
            RB_FAILURE)
        {
            return IP6_FAILURE;
        }
        pEcmp6Rt->u4Flag = (pEcmp6Rt->u4Flag) & (~(UINT4) (RTM6_RT_IN_ECMPPRT));
        MemReleaseMemBlock (gRtm6GlobalInfo.Rtm6PNextHopPoolId,
                            (UINT1 *) pNh6Entry);
    }
    RBTreeCount (gRtm6GlobalInfo.pECMP6PRTRBRoot, &u4URNextHopCnt);

    return IP6_SUCCESS;

}

 /*********************************************************************************
 * Function Name    :   Rtm6AddRtToECMP6PRT
 *
 * Description      :   Function to add the route entry from Trie  with unresolved 
 *                      next hop to the RBTree maintained for ECMP6 routes
 *
 * Inputs           :  pRtm6Cxt - Pointer to RTM6 context
 *                     pEcmp6Rt - Route Entry to be added to ECMP6PRT
 *                      
 *
 * Return Value     :  IP6_FAILURE/IP6_SUCCESS
 ***********************************************************************************/

INT4
Rtm6AddRtToECMP6PRT (tRtm6Cxt * pRtm6Cxt, tIp6RtEntry * pEcmp6Rt)
{

    tPNh6Node           Exst6NhNode;
    tPNh6Node          *pNh6Entry = NULL;
    tIp6Addr            tTmpAddr;

    /*If the route is having only directly connected routes or there
     * is no next Hop, nothing needs to be added to ECMP6 PRT*/
    MEMSET (&tTmpAddr, 0, sizeof (tIp6Addr));
    if (MEMCMP (&pEcmp6Rt->nexthop, &tTmpAddr, sizeof (tIp6Addr)) == 0)
    {
        return IP6_SUCCESS;
    }
    /* If the route entry is already present in ECMP6 PRT */
    if (pEcmp6Rt->u4Flag & RTM6_RT_IN_ECMPPRT)
    {
        /*If the route is in PRT dont add it to ECMP6 PRT */
        return IP6_SUCCESS;
    }
    /*Check whether Pending Next hop entry with this next hop exists */
    MEMCPY (&(Exst6NhNode.NextHop), &pEcmp6Rt->nexthop, sizeof (tIp6Addr));
    Exst6NhNode.pRtm6Cxt = pRtm6Cxt;

    pNh6Entry = RBTreeGet (gRtm6GlobalInfo.pECMP6PRTRBRoot, &Exst6NhNode);
    if (pNh6Entry == NULL)
    {
        /* Allocate the Ecmp6Rt Next hope node */
        if ((pNh6Entry = (tPNh6Node *) MemAllocMemBlk
             (gRtm6GlobalInfo.Rtm6PNextHopPoolId)) == NULL)
        {
            /*MemReleaseMemBlock (gRtm6GlobalInfo.Rtm6URRtPoolId,
               (UINT1 *) pEcmp6RtEntry); */
            return IP6_FAILURE;
        }
        MEMCPY (&pNh6Entry->NextHop, &pEcmp6Rt->nexthop, sizeof (tIp6Addr));
        pNh6Entry->pRtm6Cxt = pRtm6Cxt;
        pNh6Entry->u4IfIndex = pEcmp6Rt->u4Index;

        /* Add the ECMP6 next hop Entry to RBTree */
        if (RBTreeAdd (gRtm6GlobalInfo.pECMP6PRTRBRoot, pNh6Entry) ==
            RB_FAILURE)
        {
            MemReleaseMemBlock (gRtm6GlobalInfo.Rtm6PNextHopPoolId,
                                (UINT1 *) pNh6Entry);
            return IP6_FAILURE;
        }
        pEcmp6Rt->u4Flag |= RTM6_RT_IN_ECMPPRT;
        TMO_SLL_Init (&(pNh6Entry->routeEntryList));
    }

    /*Managing the routes need to be done */
    /*If timer is not running, then start the timer */

    if (gu4ECMP6TmrFlag == 1)
    {

        if (Rtm6TmrSetTimer
            (&(gRtm6GlobalInfo.Rtm6ECMP6PRTTmr), RTM6_ECMP6PRT_TIMER,
             RTM6_ECMP6PRT_TIMER_INTERVAL) == TMR_SUCCESS)
        {
            gu4ECMP6TmrFlag = 0;
        }
    }

    return IP6_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : Rtm6RmEnqChkSumMsgToRm                                */
/*                                                                           */
/* Description        : This function calls the RM Module to enqueue the     */
/*                      message from applications to RM task.                */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : RTM6_SUCCESS/RTM6_FAILURE                              */
/*                                                                           */
/*****************************************************************************/
INT4
Rtm6RmEnqChkSumMsgToRm (UINT2 u2AppId, UINT2 u2ChkSum)
{
#ifdef RM_WANTED
    if ((RmEnqChkSumMsgToRmFromApp (u2AppId, u2ChkSum)) == RM_FAILURE)
    {
        return RTM6_FAILURE;
    }
#else
    UNUSED_PARAM (u2AppId);
    UNUSED_PARAM (u2ChkSum);
#endif
    return RTM6_SUCCESS;
}
