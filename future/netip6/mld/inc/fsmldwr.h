
#ifndef _FSMLDWRAP_H 
#define _FSMLDWRAP_H 
VOID RegisterFSMLD(VOID);
INT4 FsmldNoOfCacheEntriesTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsmldNoOfCacheEntriesSet (tSnmpIndex *, tRetVal *);
INT4 FsmldNoOfCacheEntriesGet (tSnmpIndex *, tRetVal *);
INT4 FsmldNoOfRoutingProtocolsTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsmldNoOfRoutingProtocolsSet (tSnmpIndex *, tRetVal *);
INT4 FsmldNoOfRoutingProtocolsGet (tSnmpIndex *, tRetVal *);
INT4 FsmldTraceDebugTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsmldTraceDebugSet (tSnmpIndex *, tRetVal *);
INT4 FsmldTraceDebugGet (tSnmpIndex *, tRetVal *);
INT4 FsmldModeTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsmldModeSet (tSnmpIndex *, tRetVal *);
INT4 FsmldModeGet (tSnmpIndex *, tRetVal *);
INT4 FsmldProtocolUpDownTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsmldProtocolUpDownSet (tSnmpIndex *, tRetVal *);
INT4 FsmldProtocolUpDownGet (tSnmpIndex *, tRetVal *);
INT4 FsmldNoOfCacheEntriesDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsmldNoOfRoutingProtocolsDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsmldTraceDebugDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsmldModeDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsmldProtocolUpDownDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);


 /*  GetNext Function Prototypes */
#endif
