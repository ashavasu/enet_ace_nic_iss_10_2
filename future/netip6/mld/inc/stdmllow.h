
/* Proto Validate Index Instance for MldInterfaceTable. */
INT1
nmhValidateIndexInstanceMldInterfaceTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for MldInterfaceTable  */

INT1
nmhGetFirstIndexMldInterfaceTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexMldInterfaceTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMldInterfaceQueryInterval ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetMldInterfaceStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetMldInterfaceVersion ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetMldInterfaceQuerier ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetMldInterfaceQueryMaxResponseDelay ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetMldInterfaceJoins ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetMldInterfaceGroups ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetMldInterfaceRobustness ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetMldInterfaceLastListenQueryIntvl ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetMldInterfaceProxyIfIndex ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetMldInterfaceQuerierUpTime ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetMldInterfaceQuerierExpiryTime ARG_LIST((INT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetMldInterfaceQueryInterval ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetMldInterfaceStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetMldInterfaceVersion ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetMldInterfaceQueryMaxResponseDelay ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetMldInterfaceRobustness ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetMldInterfaceLastListenQueryIntvl ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetMldInterfaceProxyIfIndex ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2MldInterfaceQueryInterval ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2MldInterfaceStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2MldInterfaceVersion ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2MldInterfaceQueryMaxResponseDelay ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2MldInterfaceRobustness ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2MldInterfaceLastListenQueryIntvl ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2MldInterfaceProxyIfIndex ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2MldInterfaceTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for MldCacheTable. */
INT1
nmhValidateIndexInstanceMldCacheTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4 ));

/* Proto Type for Low Level GET FIRST fn for MldCacheTable  */

INT1
nmhGetFirstIndexMldCacheTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *  , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexMldCacheTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMldCacheSelf ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

INT1
nmhGetMldCacheLastReporter ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetMldCacheUpTime ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4 ,UINT4 *));

INT1
nmhGetMldCacheExpiryTime ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4 ,UINT4 *));

INT1
nmhGetMldCacheStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetMldCacheSelf ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

INT1
nmhSetMldCacheStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2MldCacheSelf ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

INT1
nmhTestv2MldCacheStatus ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2MldCacheTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
