
# ifndef stdmlOCON_H
# define stdmlOCON_H
/*
 *  The Constant Declarations for
 *  mldInterfaceTable
 */

# define MLDINTERFACEIFINDEX                               (1)
# define MLDINTERFACEQUERYINTERVAL                         (2)
# define MLDINTERFACESTATUS                                (3)
# define MLDINTERFACEVERSION                               (4)
# define MLDINTERFACEQUERIER                               (5)
# define MLDINTERFACEQUERYMAXRESPONSEDELAY                 (6)
# define MLDINTERFACEJOINS                                 (7)
# define MLDINTERFACEGROUPS                                (8)
# define MLDINTERFACEROBUSTNESS                            (9)
# define MLDINTERFACELASTLISTENQUERYINTVL                  (10)
# define MLDINTERFACEPROXYIFINDEX                          (11)
# define MLDINTERFACEQUERIERUPTIME                         (12)
# define MLDINTERFACEQUERIEREXPIRYTIME                     (13)

/*
 *  The Constant Declarations for
 *  mldCacheTable
 */

# define MLDCACHEADDRESS                                   (1)
# define MLDCACHEIFINDEX                                   (2)
# define MLDCACHESELF                                      (3)
# define MLDCACHELASTREPORTER                              (4)
# define MLDCACHEUPTIME                                    (5)
# define MLDCACHEEXPIRYTIME                                (6)
# define MLDCACHESTATUS                                    (7)

#endif /*  stdmldOCON_H  */
