
# ifndef fsmldOCON_H
# define fsmldOCON_H
/*
 *  The Constant Declarations for
 *  fsmldScalars
 */

# define FSMLDNOOFCACHEENTRIES                             (1)
# define FSMLDNOOFROUTINGPROTOCOLS                         (2)
# define FSMLDTRACEDEBUG                                   (3)
# define FSMLDMODE                                         (4)
# define FSMLDPROTOCOLUPDOWN                               (5)

#endif /*  fsmldOCON_H  */
