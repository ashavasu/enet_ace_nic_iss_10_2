
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: mldexterns.h,v 1.3 2010/08/02 06:56:58 prabuc Exp $
 *
 * Description:
 *
 *******************************************************************/
#ifndef _MLDEXTERNS_H
#define _MLDEXTERNS_H

extern tTimerListId        gMldTimerListId;
extern tMLDInterfaceEntry  *gMLDInterfaceTable;
extern tMLDCacheEntry      *gMLDCacheTable;
extern tMLDHostEntry       *gMLDHostTable;
extern tMLDHlReg           *gMLDHlReg;
extern tMldSystemSize gMldSizingParams;

extern UINT4               gu4MLDGlobalStatus;
#endif /* _MLDEXTERNS_H*/
