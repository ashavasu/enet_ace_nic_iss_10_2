
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: mldport.h,v 1.4 2010/11/18 14:05:34 siva Exp $
 *
 * Description:
 *
 *******************************************************************/
#ifndef _MLDPORT_H
#define _MLDPORT_H

#define  MLDCreateTask        OsixCreateTask
#define  MLDCreateQ           OsixCreateQ
#define  MLDCreateTimerList   TmrCreateTimerList

#define  MLDDeleteTask        OsixDeleteTask
#define  MLDDeleteQ           OsixDeleteQ
#define  MLDDeleteTimerList   TmrDeleteTimerList

#define  MLDReceiveEvt        OsixReceiveEvent
#define  MLDSendEvent         OsixSendEvent

#define  MLDSendToQ           OsixSendToQ
#define  MLDRcvFromQ          OsixReceiveFromQ

#define  MLDStartTmr          TmrStartTimer
#define  MLDStopTmr           TmrStopTimer
#define  MLDGetRemainingTime  TmrGetRemainingTime
#define  MLDParams(pBuf)      &(pBuf->ModuleData)

#define  MLDRelBuf            CRU_BUF_Release_MsgBufChain
#define  MLDAllocBuf          CRU_BUF_Allocate_MsgBufChain

#define  MLD_RANDOM           Ip6Random

#define  MLD_IP6_ENTRY_EXISTS Ip6ifEntryExists
#endif /* _MLDPORT_H */
