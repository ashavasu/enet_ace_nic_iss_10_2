
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: mldtrace.h,v 1.3 2010/08/02 06:56:58 prabuc Exp $
 *
 * Description:
 *
 *******************************************************************/
#ifndef _MLDTRACE_H_
#define _MLDTRACE_H_

extern UINT4  gu4MldDbgMap; 

 /* Common bitmasks for various events. */

#define  MLD_INIT_SHUT_TRC   INIT_SHUT_TRC     
#define  MLD_MGMT_TRC        MGMT_TRC          
#define  MLD_DATA_PATH_TRC   DATA_PATH_TRC     
#define  MLD_DUMP_TRC        DUMP_TRC          
#define  MLD_OS_RES_TRC      OS_RESOURCE_TRC   
#define  MLD_ALL_FAIL_TRC    ALL_FAILURE_TRC   
#define  MLD_BUF_TRC         BUFFER_TRC        
#define  MLD_ENTRY_EXIT_TRC  0x00000100
#define  MLD_SEM_TRC         0x00000200        

#define  MLD_DBG_MAP         gu4MldDbgMap      


 /* General Macros */

#define  MLD_PKT_DUMP      MOD_PKT_DUMP 
#define  MLD_MOD_TRC       MOD_TRC      
#define  MLD_MOD_TRC_ARG1  MOD_TRC_ARG1
#define  MLD_MOD_TRC_ARG2  MOD_TRC_ARG2
#define  MLD_MOD_TRC_ARG3  MOD_TRC_ARG3 
#define  MLD_MOD_TRC_ARG4  MOD_TRC_ARG4 
#define  MLD_MOD_TRC_ARG5  MOD_TRC_ARG5 
#define  MLD_MOD_TRC_ARG6  MOD_TRC_ARG6 

#endif /* _MLDTRACE_H_ */

