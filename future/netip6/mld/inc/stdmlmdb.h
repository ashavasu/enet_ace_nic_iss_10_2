
/*****
  stdmlmdb.h . This File Contains the Mib DataBase.
*****/

#ifndef _STDMLMDB_H
#define _STDMLMDB_H

/*  The NULL FUNCTION POINTER.  */
# define NULLF ARG_LIST(((INT4 (*)(tSNMP_OID_TYPE *,\
                                          tSNMP_OID_TYPE *,\
                                          UINT1,\
                                          tSNMP_MULTI_DATA_TYPE *)) NULL))
/*  NULL VARBIND FUNCTION POINTER . */
# define NULLVBF  ARG_LIST(((tSNMP_VAR_BIND* (*)(tSNMP_OID_TYPE *,\
                                           tSNMP_OID_TYPE *,\
                                           UINT1,\
                                           UINT1))NULL))
# include "stdmlmid.h"
# include "stdmlcon.h"
# include "stdmlogi.h"

/*  The Declaration of the Group Arrays. */

UINT4 au4_stdmld_snmp_TABLE1[] = {1,3,6,1,2,1,91,1};

/*  The Declaration of the SubGroup Arrays. */

UINT4  au4_SNMP_OGP_MLDINTERFACETABLE_OID [] ={1,1};
UINT4  au4_SNMP_OGP_MLDCACHETABLE_OID [] ={2,1};

/*  Declaration of Group OID Table. */
/* Each entry contains Length of Group OID, Pointer to the Group OID,
   Priority of the registering subagent, Timeout for response,
   and Number of Subgroups in that order */

 tSNMP_GroupOIDType stdmld_FMAS_GroupOIDTable[] =
{
   {8 , au4_stdmld_snmp_TABLE1 , 1 , 10 , 2}
};
/*  Declaration of Base OID Table. */
/* Each entry contains Length of Base OID, Pointer to the Base OID,
   Middle level get function pointer, Middle level test function pointer,
   Middle level set function pointer and Number of objects in that table
   in that order */

 tSNMP_BaseOIDType  stdmld_FMAS_BaseOIDTable[] = {
{
2,
au4_SNMP_OGP_MLDINTERFACETABLE_OID,
mldInterfaceEntryGet,
mldInterfaceEntryTest,
mldInterfaceEntrySet,
13
},
{
2,
au4_SNMP_OGP_MLDCACHETABLE_OID,
mldCacheEntryGet,
mldCacheEntryTest,
mldCacheEntrySet,
7
}
};
/* Declaration of MIB Object Table. */
/* Each entry contains Name of the table, Permissions for
   the object and Object name in that order */

 tSNMP_MIBObjectDescrType  stdmld_FMAS_MIBObjectTable[] = {
{
 SNMP_OGP_INDEX_MLDINTERFACETABLE,
 NO_ACCESS,
 MLDINTERFACEIFINDEX
},
{
 SNMP_OGP_INDEX_MLDINTERFACETABLE,
 READ_CREATE,
 MLDINTERFACEQUERYINTERVAL
},
{
 SNMP_OGP_INDEX_MLDINTERFACETABLE,
 READ_CREATE,
 MLDINTERFACESTATUS
},
{
 SNMP_OGP_INDEX_MLDINTERFACETABLE,
 READ_CREATE,
 MLDINTERFACEVERSION
},
{
 SNMP_OGP_INDEX_MLDINTERFACETABLE,
 READ_ONLY,
 MLDINTERFACEQUERIER
},
{
 SNMP_OGP_INDEX_MLDINTERFACETABLE,
 READ_CREATE,
 MLDINTERFACEQUERYMAXRESPONSEDELAY
},
{
 SNMP_OGP_INDEX_MLDINTERFACETABLE,
 READ_ONLY,
 MLDINTERFACEJOINS
},
{
 SNMP_OGP_INDEX_MLDINTERFACETABLE,
 READ_ONLY,
 MLDINTERFACEGROUPS
},
{
 SNMP_OGP_INDEX_MLDINTERFACETABLE,
 READ_CREATE,
 MLDINTERFACEROBUSTNESS
},
{
 SNMP_OGP_INDEX_MLDINTERFACETABLE,
 READ_CREATE,
 MLDINTERFACELASTLISTENQUERYINTVL
},
{
 SNMP_OGP_INDEX_MLDINTERFACETABLE,
 READ_CREATE,
 MLDINTERFACEPROXYIFINDEX
},
{
 SNMP_OGP_INDEX_MLDINTERFACETABLE,
 READ_ONLY,
 MLDINTERFACEQUERIERUPTIME
},
{
 SNMP_OGP_INDEX_MLDINTERFACETABLE,
 READ_ONLY,
 MLDINTERFACEQUERIEREXPIRYTIME
},
{
 SNMP_OGP_INDEX_MLDCACHETABLE,
 NO_ACCESS,
 MLDCACHEADDRESS
},
{
 SNMP_OGP_INDEX_MLDCACHETABLE,
 NO_ACCESS,
 MLDCACHEIFINDEX
},
{
 SNMP_OGP_INDEX_MLDCACHETABLE,
 READ_CREATE,
 MLDCACHESELF
},
{
 SNMP_OGP_INDEX_MLDCACHETABLE,
 READ_ONLY,
 MLDCACHELASTREPORTER
},
{
 SNMP_OGP_INDEX_MLDCACHETABLE,
 READ_ONLY,
 MLDCACHEUPTIME
},
{
 SNMP_OGP_INDEX_MLDCACHETABLE,
 READ_ONLY,
 MLDCACHEEXPIRYTIME
},
{
 SNMP_OGP_INDEX_MLDCACHETABLE,
 READ_CREATE,
 MLDCACHESTATUS
}
};

 tSNMP_GLOBAL_STRUCT stdmld_FMAS_Global_data =
{sizeof (stdmld_FMAS_GroupOIDTable) / sizeof (tSNMP_GroupOIDType),
 sizeof (stdmld_FMAS_BaseOIDTable) / sizeof (tSNMP_BaseOIDType)};

 int stdmld_MAX_OBJECTS =
(sizeof (stdmld_FMAS_MIBObjectTable) / sizeof (tSNMP_MIBObjectDescrType));

#endif  /* _STDMLMDB_H */
