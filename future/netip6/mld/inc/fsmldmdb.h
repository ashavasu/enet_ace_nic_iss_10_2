
/*****
  fsmldmdb.h . This File Contains the Mib DataBase.
*****/

#ifndef _FSMLDMDB_H
#define _FSMLDMDB_H

/*  The NULL FUNCTION POINTER.  */
# define NULLF ARG_LIST(((INT4 (*)(tSNMP_OID_TYPE *,\
                                          tSNMP_OID_TYPE *,\
                                          UINT1,\
                                          tSNMP_MULTI_DATA_TYPE *)) NULL))
/*  NULL VARBIND FUNCTION POINTER . */
# define NULLVBF  ARG_LIST(((tSNMP_VAR_BIND* (*)(tSNMP_OID_TYPE *,\
                                           tSNMP_OID_TYPE *,\
                                           UINT1,\
                                           UINT1))NULL))
# include "fsmldmid.h"
# include "fsmldcon.h"
# include "fsmldogi.h"

/*  The Declaration of the Group Arrays. */

UINT4 au4_fsmld_snmp_TABLE1[] = {1,3,6,1,4,1,2076,70};

/*  The Declaration of the SubGroup Arrays. */

UINT4  au4_SNMP_OGP_FSMLDSCALARS_OID [] ={1};

/*  Declaration of Group OID Table. */
/* Each entry contains Length of Group OID, Pointer to the Group OID,
   Priority of the registering subagent, Timeout for response,
   and Number of Subgroups in that order */

 tSNMP_GroupOIDType fsmld_FMAS_GroupOIDTable[] =
{
   {8 , au4_fsmld_snmp_TABLE1 , 1 , 10 , 1}
};
/*  Declaration of Base OID Table. */
/* Each entry contains Length of Base OID, Pointer to the Base OID,
   Middle level get function pointer, Middle level test function pointer,
   Middle level set function pointer and Number of objects in that table
   in that order */

 tSNMP_BaseOIDType  fsmld_FMAS_BaseOIDTable[] = {
{
1,
au4_SNMP_OGP_FSMLDSCALARS_OID,
fsmldScalarsGet,
fsmldScalarsTest,
fsmldScalarsSet,
5
}
};
/* Declaration of MIB Object Table. */
/* Each entry contains Name of the table, Permissions for
   the object and Object name in that order */

 tSNMP_MIBObjectDescrType  fsmld_FMAS_MIBObjectTable[] = {
{
 SNMP_OGP_INDEX_FSMLDSCALARS,
 READ_WRITE,
 FSMLDNOOFCACHEENTRIES
},
{
 SNMP_OGP_INDEX_FSMLDSCALARS,
 READ_WRITE,
 FSMLDNOOFROUTINGPROTOCOLS
},
{
 SNMP_OGP_INDEX_FSMLDSCALARS,
 READ_WRITE,
 FSMLDTRACEDEBUG
},
{
 SNMP_OGP_INDEX_FSMLDSCALARS,
 READ_WRITE,
 FSMLDMODE
},
{
 SNMP_OGP_INDEX_FSMLDSCALARS,
 READ_WRITE,
 FSMLDPROTOCOLUPDOWN
}
};

 tSNMP_GLOBAL_STRUCT fsmld_FMAS_Global_data =
{sizeof (fsmld_FMAS_GroupOIDTable) / sizeof (tSNMP_GroupOIDType),
 sizeof (fsmld_FMAS_BaseOIDTable) / sizeof (tSNMP_BaseOIDType)};

 int fsmld_MAX_OBJECTS =
(sizeof (fsmld_FMAS_MIBObjectTable) / sizeof (tSNMP_MIBObjectDescrType));

#endif  /* _FSMLDMDB_H */
