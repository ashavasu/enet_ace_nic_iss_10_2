
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: mldtypdfs.h,v 1.3 2010/08/02 06:56:58 prabuc Exp $
 *
 * Description:
 *
 *******************************************************************/
#ifndef _MLDTYPDFS_H_
#define _MLDTYPDFS_H_


typedef struct MLDTmr{
    tTmrAppTimer  node;
    tIp6Addr      McastAddr;
    UINT4         u4IfIndex;
}tMLDTmr;

typedef struct MLDParms{
    tIp6Addr      SrcAddr;
    UINT4         u4IfIndex;
    UINT2         u2Len;
    UINT2         u2Padding;
}tMLDParms;

typedef struct MLDHdr{
    UINT1     u1Type;
    UINT1     u1Code;
    UINT2     u2ChkSum;
    UINT2     u2MaxRespDelay;
    UINT2     u2Reserved;
    tIp6Addr  McastAddr;
}tMLDHdr;

typedef struct MLDHlReg
{
    MLDHlRegFn fnptr;
    UINT2 u2Protocol;
    UINT2 u2Padding;
}tMLDHlReg;

typedef struct MLDCacheEntry{
    tIp6Addr  McastAddr;         /* Multicast address */
    tIp6Addr  LastReporter;      /* Address from where last report was received*/
    tMLDTmr   RxmtTmr;           /* Retransmission timer */
    tMLDTmr   ListenerTmr;      /* Tmr to be started when a report/done is recd */ 
    tCRU_BUF_CHAIN_HEADER *pBuf; /*Last mas pkt's Mld header sent for this group*/
    VOID     *pParams;
    UINT4     u4LastListenerCount;/*Last Listener Query Count */
    UINT4     u4CreateTime;     /* Time when this entry was created */ 
    UINT4     u4IfIndex;         /* Interface id      */ 
    INT4      i4RowStatus;      /* Row Status */
    UINT1     u1Mode;            /* INVALID/Querier/NonQuereir */ 
    UINT1     u1State;           /* LIST_PRE/NO_LIST/CHK_LIST */
    UINT1     u1Self;            /* Indication of whether local system is
                                    a member of this group */
    UINT1     u1Pad;             /* One byte padding */
}tMLDCacheEntry;


typedef struct MLDInterfaceEntry{
    tIp6Addr IfQuerier;          /* Querier on this interface */
    tMLDTmr  tGenQueryTmr;       /* General Query Timer */
    tMLDTmr  tOtherQueryTmr;     /* Other Query Present Timer */
    tCRU_BUF_CHAIN_HEADER *pBuf; /* Last query's MLD hdr sent for this interface */
    VOID     *pParams;
    UINT4    u4RobustVar;      /* Robustness Variable */ 
    UINT4    u4StartUpQueryCount;/* Startup Query Count */
    UINT4    u4QueryInterval;     /* Query Interval */
    UINT4    u4LastListenerVal;  /* Last Listener Query Interval */
    UINT4    u4QuerierUpTime;    /* Time last updated */
    UINT4    u4Joins;            /* No of group membership on this interface */
    UINT4    u4IfIndex;        /* Interface id */
    INT4     i4RowStatus;      /* Row Status */
    UINT2    u2GenQueRespVal;    /* Query Response Interval */
    UINT1    u1Mode;           /* INVALID/Querier/NonQuerier */ 
    UINT1    au1Pad;            /* 1-byte padding */
}tMLDInterfaceEntry;

typedef struct MLDHostEntry
{
	tIp6Addr  McastAddr;/*Multicast address */
	tMLDTmr   tRepTmr;   /* Report delay timer */  
	VOID *pBuf;/* Last MLD packet sent */
   VOID      *pParams;
	UINT4     u4IfIndex;   /*Interface Id */ 
	UINT2     u2UsageCount;/* No. of applications using this group */ 
	UINT1     u1State;     /* NO_LISTENERS/DELAY_LISTEER/IDLE_LISTENER */
	UINT1     u1Flag;      /* Flag used for sending done */
}tMLDHostEntry;


#define MLD_INTERFACE_IFINDEX(X) \
            gMLDInterfaceTable[X].u4IfIndex

#define MLD_INTERFACE_MODE(X) \
            gMLDInterfaceTable[X].u1Mode

#define MLD_INTERFACE_RS(X) \
            gMLDInterfaceTable[X].i4RowStatus

#define MLD_INTERFACE_ROBUSTVAR(X) \
            gMLDInterfaceTable[X].u4RobustVar

#define MLD_INTERFACE_STARTUPQUERYCOUNT(X) \
            gMLDInterfaceTable[X].u4StartUpQueryCount

#define MLD_INTERFACE_QUERYINTERVAL(X) \
            gMLDInterfaceTable[X].u4QueryInterval

#define MLD_INTERFACE_GENQUERESPVAL(X) \
            gMLDInterfaceTable[X].u2GenQueRespVal

#define MLD_INTERFACE_LASTLISTENERVAL(X) \
            gMLDInterfaceTable[X].u4LastListenerVal

#define MLD_INTERFACE_GENQUETMR(X) \
            gMLDInterfaceTable[X].tGenQueryTmr

#define MLD_INTERFACE_OTHERQUETMR(X) \
            gMLDInterfaceTable[X].tOtherQueryTmr

#define MLD_INTERFACE_QUERIERUPTIME(X) \
            gMLDInterfaceTable[X].u4QuerierUpTime

#define MLD_INTERFACE_JOINS(X) \
            gMLDInterfaceTable[X].u4Joins

#define MLD_INTERFACE_IFQUERIER(X) \
            gMLDInterfaceTable[X].IfQuerier

#define MLD_INTERFACE_BUF(X) \
            gMLDInterfaceTable[X].pBuf



#define McastListenerVal(X) ((MLD_INTERFACE_ROBUSTVAR(X) *\
                           MLD_INTERFACE_QUERYINTERVAL(X)) +\
                                  MLD_INTERFACE_GENQUERESPVAL(X))

#define MLDOtherQueryTmrVal(X) (MLD_INTERFACE_ROBUSTVAR(X) *\
                                MLD_INTERFACE_QUERYINTERVAL(X)) +\
                              (MLD_INTERFACE_GENQUERESPVAL(X)/2)

#define MLD_CACHE_IFINDEX(X) \
            gMLDCacheTable[X].u4IfIndex

#define MLD_CACHE_MCASTADDR(X) \
            gMLDCacheTable[X].McastAddr

#define MLD_CACHE_MODE(X) \
            gMLDCacheTable[X].u1Mode

#define MLD_CACHE_STATE(X) \
            gMLDCacheTable[X].u1State

#define MLD_CACHE_LASTREPORTER(X) \
            gMLDCacheTable[X].LastReporter

#define MLD_CACHE_RXMTTMR(X) \
            gMLDCacheTable[X].RxmtTmr

#define MLD_CACHE_LISTENERTMR(X) \
            gMLDCacheTable[X].ListenerTmr

#define MLD_CACHE_LASTLISTENERCOUNT(X) \
            gMLDCacheTable[X].u4LastListenerCount

#define MLD_CACHE_SELF(X) \
            gMLDCacheTable[X].u1Self

#define MLD_CACHE_RS(X) \
            gMLDCacheTable[X].i4RowStatus

#define MLD_CACHE_CREATETIME(X) \
            gMLDCacheTable[X].u4CreateTime

#define MLD_CACHE_BUF(X) \
            gMLDCacheTable[X].pBuf

#define MLD_HOST_IFINDEX(X) \
            gMLDHostTable[X].u4IfIndex

#define MLD_HOST_MCASTADDR(X) \
            gMLDHostTable[X].McastAddr

#define MLD_HOST_STATE(X) \
            gMLDHostTable[X].u1State

#define MLD_HOST_REPORTTIMER(X) \
            gMLDHostTable[X].tRepTmr

#define MLD_HOST_USAGECOUNT(X) \
            gMLDHostTable[X].u2UsageCount

#define MLD_HOST_FLAG(X) \
            gMLDHostTable[X].u1Flag

#define MLD_HOST_BUF(X) \
            gMLDHostTable[X].pBuf

#endif /* _MLDTYPDFS_H_ */
