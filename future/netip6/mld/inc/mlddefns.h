
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: mlddefns.h,v 1.5 2015/10/08 10:37:48 siva Exp $
 *
 * Description:
 *
 *******************************************************************/
#ifndef _MLDDEFNS_H_
#define _MLDDEFNS_H_


#define  MLD_TASK_NODE_ID           SELF                   
#define  MLD_TASK_NAME              (const UINT1 *)"MLD"
#define  MLD_TASK_PRIORITY          40
#define  MLD_TASK_STACK_SIZE        20000
#define  MLD_TASK_MAIN_FUNCTION     MLDTaskMain
#define  MLD_TASK_START_ARGS        NULL
#define  MLD_TASK_MODE              OSIX_DEFAULT_TASK_MODE

#define  IP6_TO_MLD_Q_NODE_ID       SELF                   
#define  IP6_TO_MLD_Q_NAME          (const UINT1 *)"MLDQ"                 
#define  IP6_TO_MLD_Q_DEPTH         10                     
#define  IP6_TO_MLD_Q_MODE          OSIX_GLOBAL            
#define  IP6_TO_MLD_Q_TIMEOUT       0                      

#define  MLD_MAX_IF_ENTRIES         SYS_MAX_INTERFACES
#define  MLD_MAX_HOST_ENTRIES       50
#define  MLD_MAX_CACHE_ENTRIES      gMldSizingParams.u4MldMaxCacheEntries
#define  MLD_MAX_ROUTING_PROTOCOLS  gMldSizingParams.u4MldMaxRoutingProtocol

#define  MLD_FAILURE                -1
#define  MLD_SUCCESS                0


#define  MLD_INVALID                0
#define  MLD_QUERIER                1 
#define  MLD_NON_QUERIER            2 
#define  MLD_NON_QUERIER_WAIT             3
#define  MLD_QUERIER_WAIT                 4

#define  MLD_NO_LISTENERS                1                      
#define  MLD_LISTENERS_PRE               2                      
#define  MLD_CHK_LISTENERS               3                      

#define  MLD_NO_LISTENER                1                      
#define  MLD_DELAY_LISTENER              2                      
#define  MLD_IDLE_LISTENER               3                      

#define  MLD_PKT_RECD               0x00000001             
#define  MLD_TIMER_EVENT            0x00000010             

#define  MLD_GEN_QUERY_TMR          1                      
#define  MLD_OTHER_QUERY_TMR        2                      
#define  MLD_LISTENER_TMR           3                      
#define  MLD_REXMT_TMR              4                      
#define  MLD_REPORT_TMR             4096                     

#define  MLD_ADD                    1                      
#define  MLD_DELETE                 2                      

#define  MLD_DEFAULT_VERSION        1                      
#define  MLD_DEF_ROBUSTNESS         2                      
#define  IF_DOWN                    1                      
#define  IF_DELETE                  2                      

#define  MLD_QUERY                  130                    
#define  MLD_REPORT                 131                    
#define  MLD_DONE                   132                    

#define  MLD_DEF_QUERY_INTERVAL     125   /* 125 seconds */
#define  MLD_DEF_QUE_RESP_INTERVAL   10   /*  10 seconds */
#define  MLD_DEF_LAST_LISTENER_VAL    1   /*   1 seconds */
#define  MLD_UNSOLICITED_REPORT_INTERVAL 10   /*   10 seconds */

#define  MLD_RS_ACTIVE         1  
#define  MLD_RS_NOTINSERVICE   2  
#define  MLD_RS_NOTREADY       3  
#define  MLD_RS_CREATEANDGO    4  
#define  MLD_RS_CREATEANDWAIT  5  
#define  MLD_RS_DESTROY        6  

#define  MLD_PROTOCOL          ICMPV6_PROTOCOL_ID 

#define  NH_ICMP6              58     /* ICMP6 header */

#define  MLD_MAX_PKT_LEN       24 

#define MLD_ZERO               0
#define MLD_ONE                1
#define MLD_INIT               1 
#define MLD_SHUTDOWN           2
#define MLD_SET        1
#define MLD_RESET      0
#define MLD_ROUTER    0x00000001
#define MLD_HOST      0x00000002
#define MLD_THOUSAND  1000

#define SET_ADDR_MULTI(a)       {(a).u4_addr[0] = CRU_HTONL(0xFF020000);\
                                 (a).u4_addr[1] = 0x0;\
                                 (a).u4_addr[2] = 0x0;\
                                 (a).u4_addr[3] = CRU_HTONL(0x01);}

#define MLD_SYS_TICKS  SYS_NUM_OF_TIME_UNITS_IN_A_SEC

#define MLD_LINK_SCOPE_MCASTADDR(a)    {(a).u4_addr[0] = CRU_HTONL(0xFF020000);\
                                        (a).u4_addr[1] = 0x0;\
                                        (a).u4_addr[2] = 0x0;\
                                        (a).u4_addr[3] = CRU_HTONL(0x02);}



#define MLD_INIT_COMPLETE(u4Status)     lrInitComplete(u4Status)

#define MLD_ANCILLARY_DATA_LEN          100
#endif /* _MLDDEFNS_H_ */
