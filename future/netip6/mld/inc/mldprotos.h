
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: mldprotos.h,v 1.5 2010/11/18 14:05:34 siva Exp $
 *
 * Description:
 *
 *******************************************************************/
#ifndef _MLDPROTOS_H
#define _MLDPROTOS_H
 
 /* mldmain.c */

INT1 MLDProtoInit      PROTO ((VOID));

VOID MLDShutdown       PROTO ((VOID));
INT4 MLDSendPktToIpv6 PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf,
                              tHlToIp6Params * pParams));

/*mldcore.c */
VOID MLDProcessPkt          PROTO((tCRU_BUF_CHAIN_HEADER *pBuf));

INT1 MLDMASQueryReceived    PROTO((tMLDCacheEntry *pCacheEntry,
                                UINT2 u2MaxRespDelay));
                                
INT1 MLDReportReceived      PROTO((tMLDCacheEntry *pCacheEntry));

INT1 MLDDoneReceived        PROTO((tMLDCacheEntry *pCacheEntry));

VOID MLDLowIPQueryReceived  PROTO((UINT4 u4IfIndex));

INT1 MLDInterfaceModeChange PROTO((UINT4 u4IfIndex,UINT1 u1Mode));


tMLDCacheEntry *MLDGetCacheEntry         PROTO((tIp6Addr *Ip6Addr,
                                                UINT4 u4IfIndex));

tMLDCacheEntry *MLDAddCacheEntry         PROTO((tIp6Addr *Ip6Addr,
                                                UINT4 u4IfIndex));

tMLDInterfaceEntry *MLDGetInterfaceEntry PROTO((UINT4 u4IfIndex));

tMLDInterfaceEntry *MLDAddInterfaceEntry PROTO((UINT4 u4IfIndex));

VOID MLDDeleteInterfaceEntry             PROTO((UINT4 u4IfIndex));

VOID MLDHostMASQueryReceived   PROTO((tMLDHostEntry * pHostEntry,
                                      UINT2 u2MaxRespDelay));

VOID MLDHostGenQueryReceived   PROTO(( UINT2 u2MaxRespDelay));

VOID MLDHostReportReceived     PROTO((tMLDHostEntry * pHostEntry));

tMLDHostEntry *MLDAddHostEntry PROTO((tIp6Addr * Ip6Addr, UINT4 u4IfIndex));

tMLDHostEntry *MLDGetHostEntry PROTO((UINT4 u4IfIndex,tIp6Addr * Ip6Addr));

/* mldio.c */

INT1 MLDIfUpdate        PROTO ((UINT4 u4IfIndex,UINT2 u2Status));

INT1 MLDSendQuery       PROTO ((UINT4 u4IfIndex,
                               tIp6Addr *McastAddr));
                               
VOID MLDFormHdr         PROTO((UINT1 u1Type, UINT4 u4IfIndex,
                               tIp6Addr McastAddr, tMLDHdr *pMLDHdr));

VOID MLDNotifyRouting   PROTO((UINT2 u2event,tIp6Addr *McastAddr,
                               UINT4 u4IfIndex));

INT1 MLDSendReport      PROTO((tMLDHostEntry *pHostEntry));

INT1 MLDSendDone        PROTO((tMLDHostEntry *pHostEntry));

/* mldtmr.c */
VOID MLDProcessTmrEvt       PROTO((VOID));

VOID MLDQueryTmrExpiry      PROTO((UINT4 u4IfIndex));

VOID MLDOtherQueryTmrExpiry PROTO((UINT4 u4IfIndex));

VOID MLDListenerTmrExpiry   PROTO((tIp6Addr *McastAddr,UINT4 u4IfIndex));

VOID MLDRexmtTmrExpiry      PROTO((tIp6Addr *McastAddr,UINT4 u4IfIndex));

VOID MLDReportTmrExpiry     PROTO((tIp6Addr * McastAddr, UINT4 u4IfIndex));

VOID MLDInitTmr             PROTO((tMLDTmr *pTmr,UINT2 u2TimerId,
                                   UINT4 u4IfIndex, tIp6Addr *McastAddr));

VOID MLDReStartTmr          PROTO((tTmrAppTimer* pTmr, UINT4 u4Duration));

INT4  GET_CACHE_INDEX      PROTO((UINT4 u4IfIndex,tIp6Addr *McastAddr)); 

/* mldport.c */
INT4  MLDSendPkt PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf,
                         tHlToIp6Params * pParams));
INT4  MLDCreateSocket PROTO((VOID));

INT4  MLDSetSendSockOptions PROTO((VOID));

VOID  MLDCloseSocket PROTO((VOID));


/* mldcli.c*/

INT4
MldSetInterfaceStatus PROTO ((tCliHandle CliHandle, INT4 i4IfaceIndex,
                                 INT4 i4StatusFlag));

VOID
MldShowStatus PROTO ((tCliHandle CliHandle));
#endif /* _MLDPROTOS_H */

