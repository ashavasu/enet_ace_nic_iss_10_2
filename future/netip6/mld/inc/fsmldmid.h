
/*  Prototype for Get Test & Set for fsmldScalars.  */
tSNMP_VAR_BIND*
fsmldScalarsGet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, UINT1));
INT4
fsmldScalarsSet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));
INT4
fsmldScalarsTest ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));

