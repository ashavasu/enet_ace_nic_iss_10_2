
# ifndef stdmlOGP_H
# define stdmlOGP_H

 /* The Definitions of the OGP Index Constants.  */

# define SNMP_OGP_INDEX_MLDINTERFACETABLE                            (0)
# define SNMP_OGP_INDEX_MLDCACHETABLE                                (1)

#endif /*  stdmldOGP_H  */
