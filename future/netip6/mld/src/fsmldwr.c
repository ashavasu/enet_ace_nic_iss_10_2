#include "lr.h"
#include "fssnmp.h"
#include "fsmldwr.h"
#include "fsmldlow.h"
#include "fsmlddb.h"

VOID
RegisterFSMLD ()
{
    SNMPRegisterMib (&fsmldOID, &fsmldEntry, SNMP_MSR_TGR_TRUE);
    SNMPAddSysorEntry (&fsmldOID, (const UINT1 *) "futuremld");
}

INT4
FsmldNoOfCacheEntriesTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsmldNoOfCacheEntries
            (pu4Error, pMultiData->u4_ULongValue));
}

INT4
FsmldNoOfCacheEntriesSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsmldNoOfCacheEntries (pMultiData->u4_ULongValue));
}

INT4
FsmldNoOfCacheEntriesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsmldNoOfCacheEntries (&pMultiData->u4_ULongValue));
}

INT4
FsmldNoOfRoutingProtocolsTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsmldNoOfRoutingProtocols
            (pu4Error, pMultiData->u4_ULongValue));
}

INT4
FsmldNoOfRoutingProtocolsSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsmldNoOfRoutingProtocols (pMultiData->u4_ULongValue));
}

INT4
FsmldNoOfRoutingProtocolsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsmldNoOfRoutingProtocols (&pMultiData->u4_ULongValue));
}

INT4
FsmldTraceDebugTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsmldTraceDebug (pu4Error, pMultiData->u4_ULongValue));
}

INT4
FsmldTraceDebugSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsmldTraceDebug (pMultiData->u4_ULongValue));
}

INT4
FsmldTraceDebugGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsmldTraceDebug (&pMultiData->u4_ULongValue));
}

INT4
FsmldModeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsmldMode (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsmldModeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsmldMode (pMultiData->i4_SLongValue));
}

INT4
FsmldModeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsmldMode (&pMultiData->i4_SLongValue));
}

INT4
FsmldProtocolUpDownTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsmldProtocolUpDown (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsmldProtocolUpDownSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsmldProtocolUpDown (pMultiData->i4_SLongValue));
}

INT4
FsmldProtocolUpDownGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsmldProtocolUpDown (&pMultiData->i4_SLongValue));
}

INT4
FsmldNoOfCacheEntriesDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsmldNoOfCacheEntries
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsmldNoOfRoutingProtocolsDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsmldNoOfRoutingProtocols
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsmldTraceDebugDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                    tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsmldTraceDebug (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsmldModeDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
              tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsmldMode (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsmldProtocolUpDownDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsmldProtocolUpDown
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}
