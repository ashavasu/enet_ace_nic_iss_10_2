
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: mldio.c,v 1.5 2010/11/18 14:05:35 siva Exp $
 *
 * Description:
 *
 *******************************************************************/
#include "mldinc.h"

/****************************************************************************
 Function    :  MLDInput
 Input       :  tNetIpv6HliParams * - required values from the Netipv6
 Output      :  None.
 Description :  This function is call back function registered with IP6.
                IP6 calls this function when an MLD Packet is received.
 Returns     :  MLD_SUCCESS/MLD_FAILURE
****************************************************************************/
VOID
MLDInput (tNetIpv6HliParams * pIp6HliParams)
{
    tMLDParms          *pParms = NULL;

    MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                 "Entering MLDInput\n ");

    pParms = (tMLDParms *)
        MLDParams (pIp6HliParams->unIpv6HlCmdType.AppRcv.pBuf);

    pParms->u2Len = (UINT2) pIp6HliParams->unIpv6HlCmdType.AppRcv.u4PktLength;
    pParms->u4IfIndex = pIp6HliParams->unIpv6HlCmdType.AppRcv.u4Index;

    MEMCPY (&pParms->SrcAddr,
            &(pIp6HliParams->unIpv6HlCmdType.AppRcv.Ip6SrcAddr),
            sizeof (tIp6Addr));

    if (MLDSendToQ (IP6_TO_MLD_Q_NODE_ID, IP6_TO_MLD_Q_NAME,
                    pIp6HliParams->unIpv6HlCmdType.AppRcv.pBuf,
                    OSIX_MSG_NORMAL) != OSIX_SUCCESS)
    {
        MLDRelBuf (pIp6HliParams->unIpv6HlCmdType.AppRcv.pBuf, FALSE);
        MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC | MLD_OS_RES_TRC, "MLD",
                     "Exiting MLDInput: QueueOverflow\n ");
        return;
    }

    MLD_MOD_TRC (MLD_DBG_MAP, MLD_OS_RES_TRC, "MLD",
                 "Successfully enqueued buffer to MLD Queue\n ");

    if (MLDSendEvent (MLD_TASK_NODE_ID, MLD_TASK_NAME,
                      MLD_PKT_RECD) != OSIX_SUCCESS)
    {
        MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC | MLD_OS_RES_TRC, "MLD",
                     "Exiting MLDInput: Send Event Failure\n ");
        MLDRelBuf (pIp6HliParams->unIpv6HlCmdType.AppRcv.pBuf, FALSE);
        return;
    }

    MLD_MOD_TRC (MLD_DBG_MAP, MLD_OS_RES_TRC, "MLD",
                 "Successfully sent Event to MLD Task\n ");

    MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                 "Exiting MLDInput:Successfully \n");
    return;
}

/****************************************************************************
 Function    :  MLDIfUpdate
 Input       :  Interface
                Status 
 Output      :  None.
 Description :  This function is a call back function registered with IP6.
                IP6 calls this function whenever there is change in the 
                     interface status.
 Returns     :  MLD_SUCCESS/MLD_FAILURE
****************************************************************************/
INT1
MLDIfUpdate (UINT4 u4IfIndex, UINT2 u2Status)
{
    tNetIp6McastInfo    NetIp6McastInfo;

    MEMSET (&NetIp6McastInfo, 0, sizeof (tNetIp6McastInfo));

    MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                 "Entering MLDIfUpdate\n ");

    switch (u2Status)
    {
        case IF_DOWN:
        case IF_DELETE:
            MLDDeleteInterfaceEntry (u4IfIndex);

            NetIp6McastInfo.u4IfIndex = u4IfIndex;
            NetIp6McastInfo.u2McastProtocol = ICMPV6_PROTOCOL_ID;
            NetIpv6McastSetMcStatusOnIface (&NetIp6McastInfo, NETIPV6_DISABLED);
            break;
        default:
            break;

    }

    MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                 "Exiting MLDIfUpdate\n ");
    return MLD_SUCCESS;
}

/****************************************************************************
 Function    :  MLDSendQuery
 Input       :  Interface to send the packet.
                Multicast address(for mas query)
 Output      :  None.
 Description :  This function sends a QUERY.  If the MLD Hdr is 
                not updated in the entry, this function forms an MLD Hdr 
                and updates it in the entry. The CRU Buffer is duplicated
                and the mld header is copied in the buffer and calls the 
                a function (MLDSendPkt()) to send the packet. 
 Returns     :  MLD_SUCCESS/MLD_FAILURE
****************************************************************************/
INT1
MLDSendQuery (UINT4 u4IfIndex, tIp6Addr * McastAddr)
{

    tMLDCacheEntry     *pCacheEntry = NULL;
    tHlToIp6Params      Params, *pParams;
    tCRU_BUF_CHAIN_HEADER *pDupBuf = NULL;
    tMLDInterfaceEntry *pIfEntry = NULL;
    tMLDHdr             MldHdr;
    tIp6Addr            McastTempAddr;
    tNetIpv6IfInfo      NetIpv6IfInfo;
    UINT1               u1MldZero = MLD_ZERO;

    MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                 "Entering MLDSendQuery\n");

    MEMSET (&Params, 0, sizeof (tHlToIp6Params));
    if (McastAddr != NULL)
    {
        /* Sending a M-A-S query */
        pCacheEntry = MLDGetCacheEntry (McastAddr, u4IfIndex);
        if (pCacheEntry == NULL)
        {
            MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                         "Exiting MLDSendQuery:Cannot find the entry!!!\n");
            return MLD_FAILURE;
        }
        if (pCacheEntry->pBuf == NULL)
        {
            /* Sending packet for the first time */
            MLDFormHdr (MLD_QUERY, pCacheEntry->u4IfIndex,
                        pCacheEntry->McastAddr, &MldHdr);

            pCacheEntry->pBuf = MLDAllocBuf (sizeof (tMLDHdr), MLD_ZERO);

            if (pCacheEntry->pBuf == NULL)
            {
                MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                             "Exiting MLDSendQuery:Failure in Allocating"
                             "Buffer!!!\n");
                return MLD_FAILURE;
            }

            if (CRU_BUF_Copy_OverBufChain (pCacheEntry->pBuf, (UINT1 *) &MldHdr,
                                           MLD_ZERO,
                                           sizeof (tMLDHdr)) == CRU_FAILURE)
            {
                MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                             "Exiting MLDSendQuery:Failure in Copying in"
                             "Buffer!!!\n");
                MLDRelBuf (pCacheEntry->pBuf, FALSE);
                return MLD_FAILURE;

            }

            /* Updating the params to send to FutureIPv6 */
            Params.u1Cmd = IP6_LAYER4_DATA;
            Params.u1Proto = NH_ICMP6;
            Params.u4Len = sizeof (tMLDHdr);
            Params.u1Hlim = MLD_ONE;
            Params.u4Index = pCacheEntry->u4IfIndex;
            if (NetIpv6GetIfInfo (pCacheEntry->u4IfIndex, &NetIpv6IfInfo)
                != NETIPV6_SUCCESS)
            {
                return MLD_FAILURE;
            }

            MEMCPY (&Params.Ip6SrcAddr, &NetIpv6IfInfo.Ip6Addr,
                    sizeof (tIp6Addr));
            MEMCPY (&Params.Ip6DstAddr, &pCacheEntry->McastAddr,
                    sizeof (tIp6Addr));
            pCacheEntry->pParams = MEM_MALLOC (sizeof (tHlToIp6Params),
                                               tHlToIp6Params);
            if (pCacheEntry->pParams == NULL)
            {
                MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                             "Exiting MLDSendQuery:Failure in "
                             "Allocating Buffer!!!\n");
                return MLD_FAILURE;
            }

            MEMCPY ((tHlToIp6Params *) pCacheEntry->pParams, &Params,
                    sizeof (tHlToIp6Params));

        }

        /* Duplicating the existing buffer to send to FutureIPv6 module */
        pDupBuf = CRU_BUF_Duplicate_BufChain (pCacheEntry->pBuf);

        if (pDupBuf == NULL)
        {
            MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                         "Exiting MLDSendQuery:Failure in"
                         " Allocating Buffer!!!\n");
            return MLD_FAILURE;
        }
        pParams = pCacheEntry->pParams;

    }
    else
    {
        /* Sending a general query */
        SET_ADDR_UNSPECIFIED (McastTempAddr);
        pIfEntry = MLDGetInterfaceEntry (u4IfIndex);
        if (pIfEntry == NULL)
        {
            MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                         "Exiting MLDSendQuery:Unable to find entry\n");
            return MLD_FAILURE;
        }

        if (pIfEntry->pBuf == NULL)
        {
            /* Sending packet for the first time */
            MLDFormHdr (MLD_QUERY, pIfEntry->u4IfIndex, McastTempAddr, &MldHdr);

            pIfEntry->pBuf = MLDAllocBuf (sizeof (tMLDHdr), MLD_ZERO);

            if (pIfEntry->pBuf == NULL)
            {
                MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                             "Exiting MLDSendQuery:Failure in"
                             " Allocating Buffer!!!\n");
                return MLD_FAILURE;
            }

            if (CRU_BUF_Copy_OverBufChain (pIfEntry->pBuf, (UINT1 *) &MldHdr,
                                           MLD_ZERO,
                                           sizeof (tMLDHdr)) == CRU_FAILURE)
            {
                MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                             "Exiting MLDSendQuery:Failure in"
                             " Copying in  Buffer!!!\n");
                MLDRelBuf (pIfEntry->pBuf, FALSE);
                return MLD_FAILURE;

            }

            Params.u1Cmd = IP6_LAYER4_DATA;
            Params.u1Proto = NH_ICMP6;
            Params.u4Len = sizeof (tMLDHdr);
            Params.u1Hlim = MLD_ONE;
            Params.u4Index = pIfEntry->u4IfIndex;
            if (NetIpv6GetIfInfo (pIfEntry->u4IfIndex, &NetIpv6IfInfo)
                != NETIPV6_SUCCESS)
            {
                return MLD_FAILURE;
            }

            MEMCPY (&Params.Ip6SrcAddr, &NetIpv6IfInfo.Ip6Addr,
                    sizeof (tIp6Addr));
            SET_ADDR_MULTI (Params.Ip6DstAddr);

            pIfEntry->pParams = MEM_MALLOC (sizeof (tHlToIp6Params),
                                            tHlToIp6Params);
            if (pIfEntry->pParams == NULL)
            {
                MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                             "Exiting MLDSendQuery:Failure in "
                             "Allocating Buffer!!!\n");
                return MLD_FAILURE;
            }
            MEMCPY ((tHlToIp6Params *) pIfEntry->pParams, &Params,
                    sizeof (tHlToIp6Params));

        }

        pDupBuf = CRU_BUF_Duplicate_BufChain (pIfEntry->pBuf);
        if (pDupBuf == NULL)
        {
            MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                         "Exiting MLDSendQuery:Failure in "
                         "Allocating Buffer!!!\n");
            return MLD_FAILURE;
        }
        pParams = pIfEntry->pParams;

    }
    /* if the packet is not being sent for the first time,checksum
     * field is already updated. The checksum should be set to ZERO
     * explicitly for all packets from MLD.
     */

    CRU_BUF_Copy_OverBufChain (pDupBuf, &u1MldZero, 2, sizeof (UINT2));

    if (MLDSendPkt (pDupBuf, (tHlToIp6Params *) pParams) == MLD_FAILURE)
    {
        MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                     "Exiting MLDSendQuery:Could not send"
                     " packet to IP6 module\n");
    }
    else
    {
        MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                     "Exiting MLDSendQuery:Successfully\n");
    }

    return MLD_SUCCESS;

}

/****************************************************************************
 Function    :  MLDNotifyRouting
 Input       :  Event (ADD/DELETE)            
                Multicast address
                Interface
 Output      :  None.
 Description :  This function is used to update the the mutlicast queries/done
                to the routing protocols.
 Returns     :  None.
****************************************************************************/
VOID
MLDNotifyRouting (UINT2 u2event, tIp6Addr * McastAddr, UINT4 u4IfIndex)
{
    UINT2               u2Count;

    MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                 "Entering MLDNotifyRouting\n");

    for (u2Count = MLD_ZERO; u2Count < MLD_MAX_ROUTING_PROTOCOLS; u2Count++)
    {
        if (gMLDHlReg[u2Count].fnptr != NULL)
        {
            gMLDHlReg[u2Count].fnptr (u2event, *McastAddr, u4IfIndex);
        }
    }

    MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                 "Exiting MLDNotifyRouting\n");
    return;
}

/****************************************************************************
 Function    :  MLDFormHdr
 Input       :  u1Type
                u4IfIndex
                McastAddr
 Output      :  pMLDHdr
 Description :  This function updates pMLDHdr with the MLD header values provided
                as parameters to this function.
 Returns     :  None.
****************************************************************************/
VOID
MLDFormHdr (UINT1 u1Type, UINT4 u4IfIndex, tIp6Addr McastAddr,
            tMLDHdr * pMLDHdr)
{

    MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                 "Entering MLDFormHdr\n");

    pMLDHdr->u1Type = u1Type;
    pMLDHdr->u1Code = MLD_ZERO;
    pMLDHdr->u2ChkSum = MLD_ZERO;
    pMLDHdr->u2Reserved = MLD_ZERO;
    MEMCPY (&pMLDHdr->McastAddr, &McastAddr, sizeof (tIp6Addr));

    if (u1Type == MLD_QUERY)
    {
        /* multiply by Thousand as Max. response delay should be
         * updated in milliseconds.
         */

        pMLDHdr->u2MaxRespDelay =
            (UINT2) (MLD_INTERFACE_GENQUERESPVAL (u4IfIndex) * MLD_THOUSAND);
    }
    else
    {
        pMLDHdr->u2MaxRespDelay = MLD_ZERO;
    }

    MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                 "Exiting MLDFormHdr\n");
    return;
}

/****************************************************************************
 Function    :  MLDSendReport
 Input       :  Pointer to the host table.
 Output      :  None.
 Description :  This function sends a REPORT.  If the MLD Hdr is 
                not updated in the entry, this function forms an MLD Hdr 
                and updates it in the entry.  The CRU Buffer is duplicated
                and the mld header is copied in the buffer and calls the 
                a function (MLDSendPkt()) to send the packet. 
 Returns     :  MLD_SUCCESS/MLD_FAILURE
****************************************************************************/
INT1
MLDSendReport (tMLDHostEntry * pHostEntry)
{

    tHlToIp6Params      Params;
    tCRU_BUF_CHAIN_HEADER *pDupBuf = NULL;
    tNetIpv6IfInfo      NetIpv6IfInfo;

    MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                 "Entering MLDSendReport \n");

    MEMSET (&Params, 0, sizeof (tHlToIp6Params));
    if (pHostEntry->pBuf == NULL)
    {
        /* Sending report for this group for the first time.
         * So update the buffer 
         */
        pHostEntry->pBuf = MEM_MALLOC (sizeof (tMLDHdr), tMLDHdr);
        if (pHostEntry->pBuf == NULL)
        {
            MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                         "Exiting MLDSendReport:Failure in"
                         " Allocating Buffer!!!\n");
            return MLD_FAILURE;
        }

        MLDFormHdr (MLD_REPORT, pHostEntry->u4IfIndex, pHostEntry->McastAddr,
                    (tMLDHdr *) pHostEntry->pBuf);

        Params.u1Cmd = IP6_LAYER4_DATA;
        Params.u1Proto = NH_ICMP6;
        Params.u4Len = sizeof (tMLDHdr);
        Params.u1Hlim = MLD_ONE;
        Params.u4Index = pHostEntry->u4IfIndex;
        if (NetIpv6GetIfInfo (pHostEntry->u4IfIndex, &NetIpv6IfInfo)
            != NETIPV6_SUCCESS)
        {
            return MLD_FAILURE;
        }

        MEMCPY (&Params.Ip6SrcAddr, &NetIpv6IfInfo.Ip6Addr, sizeof (tIp6Addr));
        MEMCPY (&Params.Ip6DstAddr, &pHostEntry->McastAddr, sizeof (tIp6Addr));
        pHostEntry->pParams = MEM_MALLOC (sizeof (tHlToIp6Params),
                                          tHlToIp6Params);
        if (pHostEntry->pParams == NULL)
        {
            MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                         "Exiting MLDSendQuery:Failure in "
                         "Allocating Buffer!!!\n");
            return MLD_FAILURE;
        }

        MEMCPY ((tHlToIp6Params *) pHostEntry->pParams, &Params,
                sizeof (tHlToIp6Params));
    }

    pDupBuf = MLDAllocBuf (sizeof (tMLDHdr), MLD_ZERO);
    if (pDupBuf == NULL)
    {
        MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                     "Exiting MLDSendReport:Failure in Allocating Buffer!!!\n");
        return MLD_FAILURE;
    }

    if (CRU_BUF_Copy_OverBufChain (pDupBuf, (UINT1 *) pHostEntry->pBuf,
                                   MLD_ZERO, sizeof (tMLDHdr)) == CRU_FAILURE)
    {
        MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                     "Exiting MLDSendReport:Failure in Copying"
                     " in  Buffer!!!\n");
        MLDRelBuf (pDupBuf, FALSE);
        return MLD_FAILURE;

    }

    if (MLDSendPkt (pDupBuf, (tHlToIp6Params *) pHostEntry->pParams)
        == MLD_FAILURE)
    {
        MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                     "Exiting MLDSendReport:Could not send"
                     " packet to IP6 module\n");
    }
    else
    {
        MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                     "Exiting MLDSendReport:Successfully\n");
    }

    return MLD_SUCCESS;
}

/****************************************************************************
 Function    :  MLDSendDone
 Input       :  Pointer to the host table.
 Output      :  None.
 Description :  This function sends a DONE.  If the MLD Hdr is 
                not updated in the entry, this function forms an MLD Hdr 
                and updates it in the entry.  The CRU Buffer is duplicated
                and the mld header is copied in the buffer and calls the 
                a function (MLDSendPkt()) to send the packet. 
 Returns     :  MLD_SUCCESS/MLD_FAILURE
****************************************************************************/
INT1
MLDSendDone (tMLDHostEntry * pHostEntry)
{

    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    tIp6Addr            Ip6DstAddr;

    MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                 "Entering MLDSendDone \n");

    if (pHostEntry->pBuf == NULL)
    {
        MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                     "Exiting MLDSendDone!!!\n");
        return MLD_FAILURE;
    }

    pBuf = MLDAllocBuf (sizeof (tMLDHdr), MLD_ZERO);
    if (pBuf == NULL)
    {
        MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                     "Exiting MLDSendDone:Failure in Allocating Buffer!!!\n");
        return MLD_FAILURE;
    }

    MLDFormHdr (MLD_DONE, pHostEntry->u4IfIndex, pHostEntry->McastAddr,
                (tMLDHdr *) pHostEntry->pBuf);
    if (CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) pHostEntry->pBuf,
                                   MLD_ZERO, sizeof (tMLDHdr)) == CRU_FAILURE)
    {
        MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                     "Exiting MLDSendDone:Failure in Copying"
                     " in  Buffer!!!\n");
        MLDRelBuf (pBuf, FALSE);
        return MLD_FAILURE;

    }

    MLD_LINK_SCOPE_MCASTADDR (Ip6DstAddr);
    MEMCPY (&(((tHlToIp6Params *) pHostEntry->pParams)->Ip6DstAddr),
            &Ip6DstAddr, sizeof (tIp6Addr));

    if (MLDSendPkt (pBuf, (tHlToIp6Params *) pHostEntry->pParams)
        == MLD_FAILURE)
    {
        MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                     "Exiting MLDSendDone:Could not send"
                     " packet to IP6 module\n");
    }
    else
    {
        MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                     "Exiting MLDSendDone:Successfully\n");
    }

    MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                 "Exiting MLDSendDone\n");

    return MLD_SUCCESS;
}
