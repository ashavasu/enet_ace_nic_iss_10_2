
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: mldtmr.c,v 1.4 2010/11/18 14:05:35 siva Exp $
 *
 * Description:
 *
 *******************************************************************/

#include "mldinc.h"

/****************************************************************************
 Function    :  MLDProcessTmrEvt 
 Input       :  None.
 Output      :  None.
 Description :  This function is called only from MLDProcessPkt(). It is
                called when any timer expiries.The timer expired is found
                     and based on the timer expired, other functions are invoked. 
 Returns     :  None.
****************************************************************************/
VOID
MLDProcessTmrEvt ()
{

    tTmrAppTimer       *pExpTmr = NULL;
    UINT4               u4IfIndex;
    tIp6Addr            McastAddr;

    MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                 "Entering MLDProcessTmrEvt\n");

    pExpTmr = TmrGetNextExpiredTimer (gMldTimerListId);

    while (pExpTmr != NULL)
    {
        u4IfIndex = ((tMLDTmr *) pExpTmr)->u4IfIndex;
        McastAddr = ((tMLDTmr *) pExpTmr)->McastAddr;

        switch (pExpTmr->u4Data)
        {
            case MLD_GEN_QUERY_TMR:
                MLDQueryTmrExpiry (u4IfIndex);
                break;

            case MLD_OTHER_QUERY_TMR:
                MLDOtherQueryTmrExpiry (u4IfIndex);
                break;

            case MLD_LISTENER_TMR:
                MLDListenerTmrExpiry (&McastAddr, u4IfIndex);
                break;

            case MLD_REXMT_TMR:
                MLDRexmtTmrExpiry (&McastAddr, u4IfIndex);
                break;

            case MLD_REPORT_TMR:
                MLDReportTmrExpiry (&McastAddr, u4IfIndex);
                break;

            default:
                break;

        }                        /*end of switch */

        pExpTmr = TmrGetNextExpiredTimer (gMldTimerListId);

    }                            /* end of while */

    MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                 "Exiting MLDProcessTmrEvt\n");
    return;
}

/****************************************************************************
 Function    :  MLDQueryTmrExpiry 
 Input       :  Interface
 Output      :  None.
 Description :  This function is called when the general query timer expires.
                This function sends a general query and starts the general
                     query timer again.
 Returns     :  None.
****************************************************************************/
VOID
MLDQueryTmrExpiry (UINT4 u4IfIndex)
{

    tMLDInterfaceEntry *pIfEntry = NULL;
    UINT4               u4Duration = MLD_ZERO;

    MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                 "Entering MLDQueryTmrExpiry\n");

    MLD_MOD_TRC (MLD_DBG_MAP, MLD_OS_RES_TRC, "MLD", "Query Timer Expired\n");

    pIfEntry = MLDGetInterfaceEntry (u4IfIndex);
    if (pIfEntry == NULL)
    {
        MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                     "Exiting MLDQueryTmrExpiry: FAILURE\n");
        return;
    }

    MLDSendQuery (u4IfIndex, NULL);

    if (MLD_INTERFACE_STARTUPQUERYCOUNT (u4IfIndex) != MLD_ZERO)
    {
        u4Duration = MLD_INTERFACE_QUERYINTERVAL (u4IfIndex) / 4;
        MLD_INTERFACE_STARTUPQUERYCOUNT (u4IfIndex)--;
        MLD_MOD_TRC (MLD_DBG_MAP, MLD_OS_RES_TRC, "MLD",
                     "Starting StartUp Query Timer \n");
    }
    else
    {
        u4Duration = MLD_INTERFACE_QUERYINTERVAL (u4IfIndex);
        MLD_MOD_TRC (MLD_DBG_MAP, MLD_OS_RES_TRC, "MLD",
                     "Starting General Query Timer \n");
    }

    MLDStartTmr (gMldTimerListId, &((MLD_INTERFACE_GENQUETMR (u4IfIndex)).node),
                 u4Duration * MLD_SYS_TICKS);

    MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                 "Exiting MLDQueryTmrExpiry: Successfully\n");

    return;

}

/****************************************************************************
 Function    :  MLDOtherQueryTmrExpiry 
 Input       :  Interface
 Output      :  None.
 Description :  This function is called when Other query timer expires.The 
                mode of the interface is changed to QUERIER. General query is
                     started and a general query is sent. 
 Returns     :  None.
****************************************************************************/
VOID
MLDOtherQueryTmrExpiry (UINT4 u4IfIndex)
{
    tNetIpv6IfInfo      NetIpv6IfInfo;

    MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                 "Entering MLDOtherQueryTmrExpiry\n");
    MLD_MOD_TRC (MLD_DBG_MAP, MLD_OS_RES_TRC, "MLD",
                 "Other Query Tmr Expired\n");

    if (NetIpv6GetIfInfo (u4IfIndex, &NetIpv6IfInfo) != NETIPV6_SUCCESS)
    {
        return;
    }

    MEMCPY (&MLD_INTERFACE_IFQUERIER (u4IfIndex),
            &(NetIpv6IfInfo.Ip6Addr), sizeof (tIp6Addr));
    OsixGetSysTime (&MLD_INTERFACE_QUERIERUPTIME (u4IfIndex));

    MLD_MOD_TRC_ARG1 (MLD_DBG_MAP, MLD_SEM_TRC, "MLD",
                      "Changing interface %d to MLD_QUERIER\n",
                      MLD_INTERFACE_IFINDEX (u4IfIndex));
    MLD_INTERFACE_MODE (u4IfIndex) = MLD_QUERIER;

    MLDInterfaceModeChange (u4IfIndex, MLD_QUERIER);

    MLDSendQuery (u4IfIndex, NULL);

    /* Going to QUERIER mode, so start the General Query Tmr */
    MLD_MOD_TRC (MLD_DBG_MAP, MLD_OS_RES_TRC, "MLD",
                 "Starting General Query Timer \n");
    MLDStartTmr (gMldTimerListId, &((MLD_INTERFACE_GENQUETMR (u4IfIndex)).node),
                 MLD_INTERFACE_QUERYINTERVAL (u4IfIndex) * MLD_SYS_TICKS);

    MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                 "Exiting MLDOtherQueryTmrExpiry\n");
    return;
}

/****************************************************************************
 Function    :  MLDListenerTmrExpiry 
 Input       :  Multicast Address
                Interface
 Output      :  None.
 Description :  This function is called when the Listener timer expires.
                The Routing protocol is notified that one multicast group 
                     is deleted. The retrasmission timer if started is deleted. 
 Returns     :  None.
****************************************************************************/
VOID
MLDListenerTmrExpiry (tIp6Addr * McastAddr, UINT4 u4IfIndex)
{
    tMLDCacheEntry     *pCacheEntry = NULL;
    UINT1               u1State;

    MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                 "Entering MLDListenerTmrExpiry\n");
    MLD_MOD_TRC (MLD_DBG_MAP, MLD_OS_RES_TRC, "MLD",
                 "Listener Timer Expired\n");

    pCacheEntry = MLDGetCacheEntry (McastAddr, u4IfIndex);
    if (pCacheEntry == NULL)
    {
        MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                     "Exiting MLDListenerTmrExpiry:Failure\n");
        return;
    }

    u1State = pCacheEntry->u1State;
    switch (u1State)
    {
        case MLD_NO_LISTENERS:
            break;

        case MLD_LISTENERS_PRE:
            MLDNotifyRouting (MLD_DELETE, McastAddr, u4IfIndex);

            MLD_MOD_TRC_ARG2 (MLD_DBG_MAP, MLD_SEM_TRC, "MLD",
                              "Changing from MLD_LISTENERS_PRE to"
                              "MLD_NO_LISTENERS for interface %d "
                              "and address %s \n ",
                              pCacheEntry->u4IfIndex,
                              Ip6PrintAddr (&pCacheEntry->McastAddr));
            pCacheEntry->u1State = MLD_NO_LISTENERS;
            MEMSET (pCacheEntry, MLD_ZERO, sizeof (tMLDCacheEntry));
            pCacheEntry->u1Mode = MLD_INVALID;
            break;

        case MLD_CHK_LISTENERS:
            MLDNotifyRouting (MLD_DELETE, McastAddr, u4IfIndex);
            MLDStopTmr (gMldTimerListId, &((pCacheEntry->RxmtTmr).node));

            MLD_MOD_TRC_ARG2 (MLD_DBG_MAP, MLD_SEM_TRC, "MLD",
                              "Changing from MLD_CHK_LISTENERS to"
                              "MLD_NO_LISTENERS for interface %d"
                              "and address %s \n ",
                              pCacheEntry->u4IfIndex,
                              Ip6PrintAddr (&pCacheEntry->McastAddr));
            pCacheEntry->u1State = MLD_NO_LISTENERS;
            MEMSET (pCacheEntry, MLD_ZERO, sizeof (tMLDCacheEntry));
            pCacheEntry->u1Mode = MLD_INVALID;
            break;

        default:
            break;
    }

    MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                 "Exiting MLDListenerTmrExpiry:Successfully\n");
    return;
}

/****************************************************************************
 Function    :  MLDRexmtTmrExpiry 
 Input       :  Multicast Address
                Interface
 Output      :  None.
 Description :  This function is called when the retrasmission timer expires.
                It will send "lastquerycount" queries then inform the 
                     routing protocol. 
 Returns     :  None.
****************************************************************************/
VOID
MLDRexmtTmrExpiry (tIp6Addr * McastAddr, UINT4 u4IfIndex)
{
    tMLDCacheEntry     *pCacheEntry = NULL;
    UINT1               u1State;
    UINT4               u4Duration;

    MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                 "Entering MLDRexmtTmrExpiry\n");
    MLD_MOD_TRC (MLD_DBG_MAP, MLD_OS_RES_TRC, "MLD",
                 "Retransmission Tmr Expired\n");

    pCacheEntry = MLDGetCacheEntry (McastAddr, u4IfIndex);
    if (pCacheEntry == NULL)
    {
        MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                     "Exiting MLDRexmtTmrExpiry:Failure\n");
        return;
    }

    u1State = pCacheEntry->u1State;

    MLDSendQuery (u4IfIndex, McastAddr);
    u4Duration = MLD_INTERFACE_LASTLISTENERVAL (pCacheEntry->u4IfIndex);

    MLD_MOD_TRC (MLD_DBG_MAP, MLD_OS_RES_TRC, "MLD",
                 "Starting Retransmission Timer\n");
    MLDStartTmr (gMldTimerListId,
                 &((pCacheEntry->RxmtTmr).node), u4Duration * MLD_SYS_TICKS);

    MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                 "Exiting MLDRexmtTmrExpiry\n");
    return;

}

/****************************************************************************
 Function    :  MLDReportTmrExpiry 
 Input       :  Multicast Address
                Interface
 Output      :  None.
 Description :  This function is called when the retrasmission timer expires.
                It will send "lastquerycount" queries then inform the 
                     routing protocol. 
 Returns     :  None.
****************************************************************************/
VOID
MLDReportTmrExpiry (tIp6Addr * McastAddr, UINT4 u4IfIndex)
{
    tMLDHostEntry      *pHostEntry = NULL;

    MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                 "Entering MLDReportTmrExpiry\n");

    MLD_MOD_TRC (MLD_DBG_MAP, MLD_OS_RES_TRC, "MLD",
                 "Report Delay Tmr Expired\n");

    pHostEntry = MLDGetHostEntry (u4IfIndex, McastAddr);
    if (pHostEntry == NULL)
    {
        MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                     "Exiting MLDReportTmrExpiry: Cannot find entry!!!\n");
        return;
    }

    switch (pHostEntry->u1State)
    {
        case MLD_NO_LISTENERS:
        case MLD_IDLE_LISTENER:
            break;
        case MLD_DELAY_LISTENER:

            MLD_MOD_TRC (MLD_DBG_MAP, MLD_SEM_TRC, "MLD",
                         "Sending Report packet\n");
            MLDSendReport (pHostEntry);

            pHostEntry->u1Flag = MLD_SET;

            MLD_MOD_TRC_ARG2 (MLD_DBG_MAP, MLD_SEM_TRC, "MLD",
                              "Changing from MLD_DELAY_LISTENER to"
                              "MLD_IDLE_LISTENER for interface %d and"
                              "address %s\n",
                              pHostEntry->u4IfIndex,
                              Ip6PrintAddr (&pHostEntry->McastAddr));
            pHostEntry->u1State = MLD_IDLE_LISTENER;
            break;
        default:
            break;
    }

    MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                 "Exiting MLDReportTmrExpiry\n");
    return;
}

/****************************************************************************
 Function    :  MLDInitTmr 
 Input       :  Pointer to the timer
                Timer Id
                Interface
                Multicast Address
 Output      :  None.
 Description :  This function initializes some timer with the interface and 
                multicast address.
 Returns     :  None.
****************************************************************************/
VOID
MLDInitTmr (tMLDTmr * pTmr, UINT2 u2TimerId,
            UINT4 u4IfIndex, tIp6Addr * McastAddr)
{
    MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                 "Entering MLDInitTmr\n");

    pTmr->node.u4Data = u2TimerId;
    pTmr->u4IfIndex = u4IfIndex;

    if (McastAddr != NULL)
    {
        MEMCPY (&pTmr->McastAddr, McastAddr, sizeof (tIp6Addr));
    }
    else
    {
        MEMSET (&pTmr->McastAddr, MLD_ZERO, sizeof (tIp6Addr));
    }

    MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                 "Exiting MLDInitTmr\n");
    return;
}

/****************************************************************************
 Function    :  MLDReStartTmr 
 Input       :  Pointer to the timer node
                Duration
 Output      :  None.
 Description :  This function stops the timer and starts the timer.
 Returns     :  None.
****************************************************************************/
VOID
MLDReStartTmr (tTmrAppTimer * pTmr, UINT4 u4Duration)
{
    MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                 "Entering MLDReStartTmr\n");

    MLDStopTmr (gMldTimerListId, pTmr);

    MLDStartTmr (gMldTimerListId, pTmr, u4Duration * MLD_SYS_TICKS);

    MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                 "Exiting MLDReStartTmr\n");

    return;
}
