/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: stdmllow.c,v 1.7 2010/11/18 15:14:37 siva Exp $
 *
 * Declaration of private functions used in this file */
/*******************************************************************/
# include  "include.h"
# include  "stdmllow.h"
# include  "stdmlcon.h"
# include  "stdmlogi.h"
# include  "midconst.h"
#include "mldinc.h"
#define MLD_TRUE      1
#define MLD_FALSE     2
/* LOW LEVEL Routines for Table : MldInterfaceTable. */
/****************************************************************************
 Function    :  nmhValidateIndexInstanceMldInterfaceTable
 Input       :  The Indices
                MldInterfaceIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */
INT1
nmhValidateIndexInstanceMldInterfaceTable (INT4 i4MldInterfaceIfIndex)
{
    tMLDInterfaceEntry *pIfEntry = NULL;

    if (i4MldInterfaceIfIndex >= MLD_MAX_IF_ENTRIES)
    {
        return SNMP_FAILURE;
    }

    /* gMLDInterfaceTable cannot be accessed if MLD is down */
    if (gMLDInterfaceTable == NULL)
        return SNMP_FAILURE;
    pIfEntry = MLDGetInterfaceEntry (i4MldInterfaceIfIndex);
    if (pIfEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    if (MLD_INTERFACE_RS (i4MldInterfaceIfIndex) != MLD_RS_ACTIVE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexMldInterfaceTable
 Input       :  The Indices
                MldInterfaceIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */
INT1
nmhGetFirstIndexMldInterfaceTable (INT4 *pi4MldInterfaceIfIndex)
{
    UINT4               u4Count = MLD_ZERO;

    /* gMLDInterfaceTable cannot be accessed if MLD is down */
    if (gMLDInterfaceTable == NULL)
        return SNMP_FAILURE;

    for (u4Count = MLD_ZERO; u4Count < MLD_MAX_IF_ENTRIES; u4Count++)
    {
        if ((MLD_INTERFACE_MODE (u4Count) != MLD_INVALID)
            && (MLD_INTERFACE_RS (u4Count) == MLD_RS_ACTIVE))
        {
            break;
        }
    }
    if (u4Count == MLD_MAX_IF_ENTRIES)
    {
        return SNMP_FAILURE;
    }
    *pi4MldInterfaceIfIndex = MLD_INTERFACE_IFINDEX (u4Count);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexMldInterfaceTable
 Input       :  The Indices
                MldInterfaceIfIndex
                nextMldInterfaceIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexMldInterfaceTable (INT4 i4MldInterfaceIfIndex,
                                  INT4 *pi4NextMldInterfaceIfIndex)
{
    UINT4               u4Count;

    /* gMLDInterfaceTable cannot be accessed if MLD is down */
    if (gMLDInterfaceTable == NULL)
        return SNMP_FAILURE;

    for (u4Count = i4MldInterfaceIfIndex + MLD_ONE;
         u4Count < MLD_MAX_IF_ENTRIES; u4Count++)
    {
        if ((MLD_INTERFACE_MODE (u4Count) != MLD_INVALID)
            && (MLD_INTERFACE_RS (u4Count) == MLD_RS_ACTIVE))
        {
            break;
        }
    }
    if (u4Count < MLD_MAX_IF_ENTRIES)
    {
        *pi4NextMldInterfaceIfIndex = MLD_INTERFACE_IFINDEX (u4Count);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMldInterfaceQueryInterval
 Input       :  The Indices
                MldInterfaceIfIndex

                The Object 
                retValMldInterfaceQueryInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMldInterfaceQueryInterval (INT4 i4MldInterfaceIfIndex,
                                 UINT4 *pu4RetValMldInterfaceQueryInterval)
{
    *pu4RetValMldInterfaceQueryInterval =
        MLD_INTERFACE_QUERYINTERVAL (i4MldInterfaceIfIndex);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetMldInterfaceStatus
 Input       :  The Indices
                MldInterfaceIfIndex

                The Object 
                retValMldInterfaceStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMldInterfaceStatus (INT4 i4MldInterfaceIfIndex,
                          INT4 *pi4RetValMldInterfaceStatus)
{
    *pi4RetValMldInterfaceStatus = MLD_INTERFACE_RS (i4MldInterfaceIfIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMldInterfaceVersion
 Input       :  The Indices
                MldInterfaceIfIndex

                The Object 
                retValMldInterfaceVersion
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMldInterfaceVersion (INT4 i4MldInterfaceIfIndex,
                           UINT4 *pu4RetValMldInterfaceVersion)
{
    UNUSED_PARAM (i4MldInterfaceIfIndex);
    *pu4RetValMldInterfaceVersion = MLD_DEFAULT_VERSION;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMldInterfaceQuerier
 Input       :  The Indices
                MldInterfaceIfIndex

                The Object 
                retValMldInterfaceQuerier
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMldInterfaceQuerier (INT4 i4MldInterfaceIfIndex,
                           tSNMP_OCTET_STRING_TYPE * pRetValMldInterfaceQuerier)
{

    MEMCPY (pRetValMldInterfaceQuerier->pu1_OctetList,
            &MLD_INTERFACE_IFQUERIER (i4MldInterfaceIfIndex),
            sizeof (tIp6Addr));
    pRetValMldInterfaceQuerier->i4_Length = sizeof (tIp6Addr);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMldInterfaceQueryMaxResponseDelay
 Input       :  The Indices
                MldInterfaceIfIndex

                The Object 
                retValMldInterfaceQueryMaxResponseDelay
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMldInterfaceQueryMaxResponseDelay (INT4 i4MldInterfaceIfIndex,
                                         UINT4
                                         *pu4RetValMldInterfaceQueryMaxResponseDelay)
{
    *pu4RetValMldInterfaceQueryMaxResponseDelay =
        MLD_INTERFACE_GENQUERESPVAL (i4MldInterfaceIfIndex);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetMldInterfaceJoins
 Input       :  The Indices
                MldInterfaceIfIndex

                The Object 
                retValMldInterfaceJoins
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMldInterfaceJoins (INT4 i4MldInterfaceIfIndex,
                         UINT4 *pu4RetValMldInterfaceJoins)
{
    *pu4RetValMldInterfaceJoins = MLD_INTERFACE_JOINS (i4MldInterfaceIfIndex);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetMldInterfaceGroups
 Input       :  The Indices
                MldInterfaceIfIndex

                The Object 
                retValMldInterfaceGroups
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMldInterfaceGroups (INT4 i4MldInterfaceIfIndex,
                          UINT4 *pu4RetValMldInterfaceGroups)
{
    UINT4               u4Count, u4TotGrps = MLD_ZERO;
    for (u4Count = MLD_ZERO; u4Count < MLD_MAX_CACHE_ENTRIES; u4Count++)
    {
        if (gMLDCacheTable[u4Count].u4IfIndex == (UINT4) i4MldInterfaceIfIndex)
        {
            u4TotGrps++;
        }
    }
    *pu4RetValMldInterfaceGroups = u4TotGrps;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetMldInterfaceRobustness
 Input       :  The Indices
                MldInterfaceIfIndex

                The Object 
                retValMldInterfaceRobustness
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMldInterfaceRobustness (INT4 i4MldInterfaceIfIndex,
                              UINT4 *pu4RetValMldInterfaceRobustness)
{

    *pu4RetValMldInterfaceRobustness =
        MLD_INTERFACE_ROBUSTVAR (i4MldInterfaceIfIndex);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetMldInterfaceLastListenQueryIntvl
 Input       :  The Indices
                MldInterfaceIfIndex

                The Object 
                retValMldInterfaceLastListenQueryIntvl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMldInterfaceLastListenQueryIntvl (INT4 i4MldInterfaceIfIndex,
                                        UINT4
                                        *pu4RetValMldInterfaceLastListenQueryIntvl)
{
    *pu4RetValMldInterfaceLastListenQueryIntvl =
        MLD_INTERFACE_LASTLISTENERVAL (i4MldInterfaceIfIndex);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetMldInterfaceProxyIfIndex
 Input       :  The Indices
                MldInterfaceIfIndex

                The Object 
                retValMldInterfaceProxyIfIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMldInterfaceProxyIfIndex (INT4 i4MldInterfaceIfIndex,
                                INT4 *pi4RetValMldInterfaceProxyIfIndex)
{
    UNUSED_PARAM (i4MldInterfaceIfIndex);
    *pi4RetValMldInterfaceProxyIfIndex = MLD_ZERO;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMldInterfaceQuerierUpTime
 Input       :  The Indices
                MldInterfaceIfIndex

                The Object 
                retValMldInterfaceQuerierUpTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMldInterfaceQuerierUpTime (INT4 i4MldInterfaceIfIndex,
                                 UINT4 *pu4RetValMldInterfaceQuerierUpTime)
{
    *pu4RetValMldInterfaceQuerierUpTime =
        MLD_INTERFACE_QUERIERUPTIME (i4MldInterfaceIfIndex);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetMldInterfaceQuerierExpiryTime
 Input       :  The Indices
                MldInterfaceIfIndex

                The Object 
                retValMldInterfaceQuerierExpiryTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMldInterfaceQuerierExpiryTime (INT4 i4MldInterfaceIfIndex,
                                     UINT4
                                     *pu4RetValMldInterfaceQuerierExpiryTime)
{

    if (MLD_INTERFACE_MODE (i4MldInterfaceIfIndex) == MLD_QUERIER)
    {
        *pu4RetValMldInterfaceQuerierExpiryTime = MLD_ZERO;
    }
    else
    {
        if (MLDGetRemainingTime (gMldTimerListId,
                                 &MLD_INTERFACE_OTHERQUETMR
                                 (i4MldInterfaceIfIndex).node,
                                 pu4RetValMldInterfaceQuerierExpiryTime) ==
            TMR_FAILURE)
        {
            *pu4RetValMldInterfaceQuerierExpiryTime = MLD_ZERO;
        }
    }
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetMldInterfaceQueryInterval
 Input       :  The Indices
                MldInterfaceIfIndex

                The Object 
                setValMldInterfaceQueryInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMldInterfaceQueryInterval (INT4 i4MldInterfaceIfIndex,
                                 UINT4 u4SetValMldInterfaceQueryInterval)
{
    MLD_INTERFACE_QUERYINTERVAL (i4MldInterfaceIfIndex) =
        u4SetValMldInterfaceQueryInterval;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetMldInterfaceStatus
 Input       :  The Indices
                MldInterfaceIfIndex

                The Object 
                setValMldInterfaceStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMldInterfaceStatus (INT4 i4MldInterfaceIfIndex,
                          INT4 i4SetValMldInterfaceStatus)
{
    tMLDInterfaceEntry *pIfEntry = NULL;
    UINT4               u4Duration;
    tNetIp6McastInfo    NetIp6McastInfo;
    UINT4               u4Status = 0xFF;
    tNetIpv6IfInfo      NetIpv6IfInfo;

    MEMSET (&NetIp6McastInfo, 0, sizeof (tNetIp6McastInfo));

    switch (i4SetValMldInterfaceStatus)
    {
        case MLD_RS_ACTIVE:
            pIfEntry = MLDGetInterfaceEntry (i4MldInterfaceIfIndex);
            if (pIfEntry == NULL)
            {
                return SNMP_FAILURE;
            }

            if (NetIpv6GetIfInfo (pIfEntry->u4IfIndex, &NetIpv6IfInfo)
                != NETIPV6_SUCCESS)
            {
                return SNMP_FAILURE;
            }

            if (MLD_INTERFACE_RS (i4MldInterfaceIfIndex) != MLD_RS_ACTIVE)
            {
                if ((gu4MLDGlobalStatus & MLD_ROUTER) == MLD_ROUTER)
                {
                    u4Duration =
                        MLD_INTERFACE_QUERYINTERVAL (i4MldInterfaceIfIndex) / 4;

                    MLD_MOD_TRC_ARG1 (MLD_DBG_MAP, MLD_OS_RES_TRC, "MLD",
                                      "Starting Startup Timer for Interface:%d\n",
                                      i4MldInterfaceIfIndex);
                    MLDStartTmr (gMldTimerListId,
                                 &((MLD_INTERFACE_GENQUETMR
                                    (i4MldInterfaceIfIndex)).node),
                                 u4Duration * MLD_SYS_TICKS);

                    MLD_INTERFACE_STARTUPQUERYCOUNT (i4MldInterfaceIfIndex)--;
                    MLDSendQuery (i4MldInterfaceIfIndex, NULL);
                }
                MLD_INTERFACE_RS (i4MldInterfaceIfIndex) = MLD_RS_ACTIVE;
            }
            u4Status = NETIPV6_ENABLED;
            break;

        case MLD_RS_NOTINSERVICE:

            pIfEntry = MLDGetInterfaceEntry (i4MldInterfaceIfIndex);
            if (pIfEntry != NULL)
            {
                u4Status = NETIPV6_DISABLED;
            }

            MLD_INTERFACE_RS (i4MldInterfaceIfIndex) = MLD_RS_NOTINSERVICE;
            break;

        case MLD_RS_CREATEANDGO:
            pIfEntry = MLDGetInterfaceEntry (i4MldInterfaceIfIndex);
            if (pIfEntry != NULL)
            {
                return SNMP_FAILURE;
            }

            pIfEntry = MLDAddInterfaceEntry (i4MldInterfaceIfIndex);
            if (pIfEntry == NULL)
            {
                return SNMP_FAILURE;
            }

            if (NetIpv6GetIfInfo (pIfEntry->u4IfIndex, &NetIpv6IfInfo)
                != NETIPV6_SUCCESS)
            {
                return SNMP_FAILURE;
            }

            MLD_INTERFACE_RS (i4MldInterfaceIfIndex) = MLD_RS_ACTIVE;
            if ((gu4MLDGlobalStatus & MLD_ROUTER) == MLD_ROUTER)
            {

                u4Duration =
                    MLD_INTERFACE_QUERYINTERVAL (i4MldInterfaceIfIndex) / 4;

                MLD_MOD_TRC_ARG1 (MLD_DBG_MAP, MLD_OS_RES_TRC, "MLD",
                                  "Starting Startup Timer for Interface: %d\n",
                                  i4MldInterfaceIfIndex);
                MLDStartTmr (gMldTimerListId,
                             &((MLD_INTERFACE_GENQUETMR
                                (i4MldInterfaceIfIndex)).node),
                             u4Duration * MLD_SYS_TICKS);

                MLD_INTERFACE_STARTUPQUERYCOUNT (i4MldInterfaceIfIndex)--;
                MLDSendQuery (i4MldInterfaceIfIndex, NULL);
            }
            u4Status = NETIPV6_DISABLED;
            break;

        case MLD_RS_CREATEANDWAIT:

            pIfEntry = MLDGetInterfaceEntry (i4MldInterfaceIfIndex);
            if (pIfEntry != NULL)
            {
                return SNMP_FAILURE;
            }

            pIfEntry = MLDAddInterfaceEntry (i4MldInterfaceIfIndex);
            if (pIfEntry == NULL)
            {
                return SNMP_FAILURE;
            }
            MLD_INTERFACE_RS (i4MldInterfaceIfIndex) = MLD_RS_NOTINSERVICE;
            break;

        case MLD_RS_DESTROY:
            MLDDeleteInterfaceEntry (i4MldInterfaceIfIndex);
            u4Status = NETIPV6_DISABLED;
            break;

        default:
            return SNMP_FAILURE;
    }                            /* end of switch */

    if (u4Status != 0XFF)
    {
        NetIp6McastInfo.u4IfIndex = i4MldInterfaceIfIndex;
        NetIp6McastInfo.u2McastProtocol = ICMPV6_PROTOCOL_ID;
        NetIpv6McastSetMcStatusOnIface (&NetIp6McastInfo, u4Status);
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetMldInterfaceVersion
 Input       :  The Indices
                MldInterfaceIfIndex

                The Object 
                setValMldInterfaceVersion
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMldInterfaceVersion (INT4 i4MldInterfaceIfIndex,
                           UINT4 u4SetValMldInterfaceVersion)
{
    UNUSED_PARAM (i4MldInterfaceIfIndex);
    UNUSED_PARAM (u4SetValMldInterfaceVersion);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetMldInterfaceQueryMaxResponseDelay
 Input       :  The Indices
                MldInterfaceIfIndex

                The Object 
                setValMldInterfaceQueryMaxResponseDelay
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMldInterfaceQueryMaxResponseDelay (INT4 i4MldInterfaceIfIndex,
                                         UINT4
                                         u4SetValMldInterfaceQueryMaxResponseDelay)
{
    MLD_INTERFACE_GENQUERESPVAL (i4MldInterfaceIfIndex) =
        (UINT2) u4SetValMldInterfaceQueryMaxResponseDelay;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetMldInterfaceRobustness
 Input       :  The Indices
                MldInterfaceIfIndex

                The Object 
                setValMldInterfaceRobustness
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMldInterfaceRobustness (INT4 i4MldInterfaceIfIndex,
                              UINT4 u4SetValMldInterfaceRobustness)
{
    MLD_INTERFACE_ROBUSTVAR (i4MldInterfaceIfIndex) =
        u4SetValMldInterfaceRobustness;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetMldInterfaceLastListenQueryIntvl
 Input       :  The Indices
                MldInterfaceIfIndex

                The Object 
                setValMldInterfaceLastListenQueryIntvl
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMldInterfaceLastListenQueryIntvl (INT4 i4MldInterfaceIfIndex,
                                        UINT4
                                        u4SetValMldInterfaceLastListenQueryIntvl)
{
    MLD_INTERFACE_LASTLISTENERVAL (i4MldInterfaceIfIndex) =
        u4SetValMldInterfaceLastListenQueryIntvl;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetMldInterfaceProxyIfIndex
 Input       :  The Indices
                MldInterfaceIfIndex

                The Object 
                setValMldInterfaceProxyIfIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMldInterfaceProxyIfIndex (INT4 i4MldInterfaceIfIndex,
                                INT4 i4SetValMldInterfaceProxyIfIndex)
{
    UNUSED_PARAM (i4MldInterfaceIfIndex);
    UNUSED_PARAM (i4SetValMldInterfaceProxyIfIndex);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2MldInterfaceQueryInterval
 Input       :  The Indices
                MldInterfaceIfIndex

                The Object 
                testValMldInterfaceQueryInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MldInterfaceQueryInterval (UINT4 *pu4ErrorCode,
                                    INT4 i4MldInterfaceIfIndex,
                                    UINT4 u4TestValMldInterfaceQueryInterval)
{
    UNUSED_PARAM (i4MldInterfaceIfIndex);
    /* gMLDInterfaceTable cannot be accessed if MLD is down */
    if (gMLDInterfaceTable == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (u4TestValMldInterfaceQueryInterval == 0)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2MldInterfaceStatus
 Input       :  The Indices
                MldInterfaceIfIndex

                The Object 
                testValMldInterfaceStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MldInterfaceStatus (UINT4 *pu4ErrorCode,
                             INT4 i4MldInterfaceIfIndex,
                             INT4 i4TestValMldInterfaceStatus)
{
    UNUSED_PARAM (i4MldInterfaceIfIndex);
    /* gMLDInterfaceTable cannot be accessed if MLD is down */
    if (gMLDInterfaceTable == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4TestValMldInterfaceStatus == MLD_RS_ACTIVE) ||
        (i4TestValMldInterfaceStatus == MLD_RS_CREATEANDWAIT) ||
        (i4TestValMldInterfaceStatus == MLD_RS_CREATEANDGO) ||
        (i4TestValMldInterfaceStatus == MLD_RS_DESTROY) ||
        (i4TestValMldInterfaceStatus == MLD_RS_NOTINSERVICE))
    {
        return SNMP_SUCCESS;
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2MldInterfaceVersion
 Input       :  The Indices
                MldInterfaceIfIndex

                The Object 
                testValMldInterfaceVersion
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MldInterfaceVersion (UINT4 *pu4ErrorCode,
                              INT4 i4MldInterfaceIfIndex,
                              UINT4 u4TestValMldInterfaceVersion)
{
    UNUSED_PARAM (i4MldInterfaceIfIndex);
    /* gMLDInterfaceTable cannot be accessed if MLD is down */
    if (gMLDInterfaceTable == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    /* Presently only RFC2710 is being supported, for which version is 1 */
    if (u4TestValMldInterfaceVersion != MLD_DEFAULT_VERSION)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2MldInterfaceQueryMaxResponseDelay
 Input       :  The Indices
                MldInterfaceIfIndex

                The Object 
                testValMldInterfaceQueryMaxResponseDelay
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MldInterfaceQueryMaxResponseDelay (UINT4 *pu4ErrorCode,
                                            INT4 i4MldInterfaceIfIndex,
                                            UINT4
                                            u4TestValMldInterfaceQueryMaxResponseDelay)
{
    UNUSED_PARAM (i4MldInterfaceIfIndex);
    UNUSED_PARAM (u4TestValMldInterfaceQueryMaxResponseDelay);
    /* gMLDInterfaceTable cannot be accessed if MLD is down */
    if (gMLDInterfaceTable == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2MldInterfaceRobustness
 Input       :  The Indices
                MldInterfaceIfIndex

                The Object 
                testValMldInterfaceRobustness
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MldInterfaceRobustness (UINT4 *pu4ErrorCode,
                                 INT4 i4MldInterfaceIfIndex,
                                 UINT4 u4TestValMldInterfaceRobustness)
{
    UNUSED_PARAM (i4MldInterfaceIfIndex);
    /* gMLDInterfaceTable cannot be accessed if MLD is down */
    if (gMLDInterfaceTable == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (u4TestValMldInterfaceRobustness < MLD_DEF_ROBUSTNESS)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2MldInterfaceLastListenQueryIntvl
 Input       :  The Indices
                MldInterfaceIfIndex

                The Object 
                testValMldInterfaceLastListenQueryIntvl
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MldInterfaceLastListenQueryIntvl (UINT4 *pu4ErrorCode,
                                           INT4 i4MldInterfaceIfIndex,
                                           UINT4
                                           u4TestValMldInterfaceLastListenQueryIntvl)
{
    UNUSED_PARAM (i4MldInterfaceIfIndex);
    /* gMLDInterfaceTable cannot be accessed if MLD is down */
    if (gMLDInterfaceTable == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (u4TestValMldInterfaceLastListenQueryIntvl == 0)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2MldInterfaceProxyIfIndex
 Input       :  The Indices
                MldInterfaceIfIndex

                The Object 
                testValMldInterfaceProxyIfIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MldInterfaceProxyIfIndex (UINT4 *pu4ErrorCode,
                                   INT4 i4MldInterfaceIfIndex,
                                   INT4 i4TestValMldInterfaceProxyIfIndex)
{
    UNUSED_PARAM (i4MldInterfaceIfIndex);
    /* gMLDInterfaceTable cannot be accessed if MLD is down */
    if (gMLDInterfaceTable == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (i4TestValMldInterfaceProxyIfIndex < MLD_ONE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2MldInterfaceTable
 Input       :  The Indices
                MldInterfaceIfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2MldInterfaceTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : MldCacheTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceMldCacheTable
 Input       :  The Indices
                MldCacheAddress
                MldCacheIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */
INT1
nmhValidateIndexInstanceMldCacheTable (tSNMP_OCTET_STRING_TYPE *
                                       pMldCacheAddress, INT4 i4MldCacheIfIndex)
{
    tMLDCacheEntry     *pCacheEntry = NULL;

    if (i4MldCacheIfIndex >= (INT4) MLD_MAX_CACHE_ENTRIES)
    {
        return SNMP_FAILURE;
    }
    /* gMLDCacheTable cannot be accessed if MLD is down */
    if (gMLDCacheTable == NULL)
    {
        return SNMP_FAILURE;
    }

    pCacheEntry =
        MLDGetCacheEntry ((tIp6Addr *) (VOID *) pMldCacheAddress->pu1_OctetList,
                          i4MldCacheIfIndex);
    if (pCacheEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    if (pCacheEntry->i4RowStatus != MLD_RS_ACTIVE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexMldCacheTable
 Input       :  The Indices
                MldCacheAddress
                MldCacheIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */
INT1
nmhGetFirstIndexMldCacheTable (tSNMP_OCTET_STRING_TYPE * pMldCacheAddress,
                               INT4 *pi4MldCacheIfIndex)
{
    UINT4               u4Count = MLD_ZERO;

    /* gMLDCacheTable cannot be accessed if MLD is down */
    if (gMLDCacheTable == NULL)
    {
        return SNMP_FAILURE;
    }

    for (u4Count = MLD_ZERO; u4Count < MLD_MAX_CACHE_ENTRIES; u4Count++)
    {
        if ((gMLDCacheTable[u4Count].u1Mode != MLD_INVALID)
            && (gMLDCacheTable[u4Count].i4RowStatus == MLD_RS_ACTIVE))
        {
            break;
        }
    }
    if (u4Count == MLD_MAX_CACHE_ENTRIES)
    {
        return SNMP_FAILURE;
    }
    *pi4MldCacheIfIndex = gMLDCacheTable[u4Count].u4IfIndex;
    MEMCPY (pMldCacheAddress->pu1_OctetList,
            &gMLDCacheTable[u4Count].McastAddr, sizeof (tIp6Addr));
    pMldCacheAddress->i4_Length = sizeof (tIp6Addr);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetNextIndexMldCacheTable
 Input       :  The Indices
                MldCacheAddress
                nextMldCacheAddress
                MldCacheIfIndex
                nextMldCacheIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexMldCacheTable (tSNMP_OCTET_STRING_TYPE * pMldCacheAddress,
                              tSNMP_OCTET_STRING_TYPE * pNextMldCacheAddress,
                              INT4 i4MldCacheIfIndex,
                              INT4 *pi4NextMldCacheIfIndex)
{
    UINT4               u4Count;
    INT4                i4Index;

    /* gMLDCacheTable cannot be accessed if MLD is down */
    if (gMLDCacheTable == NULL)
    {
        return SNMP_FAILURE;
    }

    i4Index = GET_CACHE_INDEX (i4MldCacheIfIndex,
                               (tIp6Addr *) (VOID *)
                               pMldCacheAddress->pu1_OctetList);
    if (i4Index == MLD_FAILURE)
    {
        MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                     "Exiting : Cannot find entry!!!\n");
        return SNMP_FAILURE;
    }

    for (u4Count = i4Index + MLD_ONE;
         u4Count < MLD_MAX_CACHE_ENTRIES; u4Count++)
    {
        if ((gMLDCacheTable[u4Count].u1Mode != MLD_INVALID)
            && (gMLDCacheTable[u4Count].i4RowStatus == MLD_RS_ACTIVE))
        {
            break;
        }
    }
    if (u4Count < MLD_MAX_CACHE_ENTRIES)
    {
        *pi4NextMldCacheIfIndex = gMLDCacheTable[u4Count].u4IfIndex;
        MEMCPY (pNextMldCacheAddress->pu1_OctetList,
                &gMLDCacheTable[u4Count].McastAddr, sizeof (tIp6Addr));
        pNextMldCacheAddress->i4_Length = sizeof (tIp6Addr);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMldCacheSelf
 Input       :  The Indices
                MldCacheAddress
                MldCacheIfIndex

                The Object 
                retValMldCacheSelf
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMldCacheSelf (tSNMP_OCTET_STRING_TYPE * pMldCacheAddress,
                    INT4 i4MldCacheIfIndex, INT4 *pi4RetValMldCacheSelf)
{
    UINT4               u4Index;
    u4Index = GET_CACHE_INDEX (i4MldCacheIfIndex,
                               (tIp6Addr *) (VOID *)
                               pMldCacheAddress->pu1_OctetList);
    *pi4RetValMldCacheSelf = MLD_CACHE_SELF (u4Index);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMldCacheLastReporter
 Input       :  The Indices
                MldCacheAddress
                MldCacheIfIndex

                The Object 
                retValMldCacheLastReporter
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMldCacheLastReporter (tSNMP_OCTET_STRING_TYPE * pMldCacheAddress,
                            INT4 i4MldCacheIfIndex,
                            tSNMP_OCTET_STRING_TYPE *
                            pRetValMldCacheLastReporter)
{
    UINT4               u4Index;
    u4Index = GET_CACHE_INDEX (i4MldCacheIfIndex,
                               (tIp6Addr *) (VOID *)
                               pMldCacheAddress->pu1_OctetList);
    MEMCPY (pRetValMldCacheLastReporter->pu1_OctetList,
            &MLD_CACHE_LASTREPORTER (u4Index), sizeof (tIp6Addr));
    pRetValMldCacheLastReporter->i4_Length = sizeof (tIp6Addr);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMldCacheUpTime
 Input       :  The Indices
                MldCacheAddress
                MldCacheIfIndex

                The Object 
                retValMldCacheUpTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMldCacheUpTime (tSNMP_OCTET_STRING_TYPE * pMldCacheAddress,
                      INT4 i4MldCacheIfIndex, UINT4 *pu4RetValMldCacheUpTime)
{
    UINT4               u4Index;
    UINT4               u4PresentTime;
    u4Index = GET_CACHE_INDEX (i4MldCacheIfIndex,
                               (tIp6Addr *) (VOID *)
                               pMldCacheAddress->pu1_OctetList);

    OsixGetSysTime (&u4PresentTime);
    *pu4RetValMldCacheUpTime = u4PresentTime - MLD_CACHE_CREATETIME (u4Index);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMldCacheExpiryTime
 Input       :  The Indices
                MldCacheAddress
                MldCacheIfIndex

                The Object 
                retValMldCacheExpiryTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMldCacheExpiryTime (tSNMP_OCTET_STRING_TYPE * pMldCacheAddress,
                          INT4 i4MldCacheIfIndex,
                          UINT4 *pu4RetValMldCacheExpiryTime)
{
    UINT4               u4Index;
    u4Index = GET_CACHE_INDEX (i4MldCacheIfIndex,
                               (tIp6Addr *) (VOID *)
                               pMldCacheAddress->pu1_OctetList);
    if (MLDGetRemainingTime (gMldTimerListId,
                             &MLD_CACHE_LISTENERTMR (u4Index).node,
                             pu4RetValMldCacheExpiryTime) == TMR_FAILURE)
    {
        *pu4RetValMldCacheExpiryTime = MLD_ZERO;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMldCacheStatus
 Input       :  The Indices
                MldCacheAddress
                MldCacheIfIndex

                The Object 
                retValMldCacheStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMldCacheStatus (tSNMP_OCTET_STRING_TYPE * pMldCacheAddress,
                      INT4 i4MldCacheIfIndex, INT4 *pi4RetValMldCacheStatus)
{
    UINT4               u4Index;
    u4Index = GET_CACHE_INDEX (i4MldCacheIfIndex,
                               (tIp6Addr *) (VOID *)
                               pMldCacheAddress->pu1_OctetList);
    *pi4RetValMldCacheStatus = MLD_CACHE_RS (u4Index);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetMldCacheSelf
 Input       :  The Indices
                MldCacheAddress
                MldCacheIfIndex

                The Object 
                setValMldCacheSelf
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMldCacheSelf (tSNMP_OCTET_STRING_TYPE * pMldCacheAddress,
                    INT4 i4MldCacheIfIndex, INT4 i4SetValMldCacheSelf)
{
    INT4                i4Index;
    i4Index = GET_CACHE_INDEX (i4MldCacheIfIndex,
                               (tIp6Addr *) (VOID *)
                               pMldCacheAddress->pu1_OctetList);
    if (i4Index == MLD_FAILURE)
        return SNMP_FAILURE;
    MLD_CACHE_SELF (i4Index) = (UINT1) i4SetValMldCacheSelf;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetMldCacheStatus
 Input       :  The Indices
                MldCacheAddress
                MldCacheIfIndex

                The Object 
                setValMldCacheStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMldCacheStatus (tSNMP_OCTET_STRING_TYPE * pMldCacheAddress,
                      INT4 i4MldCacheIfIndex, INT4 i4SetValMldCacheStatus)
{
    tMLDCacheEntry     *pCacheEntry = NULL;
    INT4                i4Index;
    UINT4               u4Duration;
    i4Index = GET_CACHE_INDEX (i4MldCacheIfIndex,
                               (tIp6Addr *) (VOID *)
                               pMldCacheAddress->pu1_OctetList);
    switch (i4SetValMldCacheStatus)
    {
        case MLD_RS_ACTIVE:
            if (i4Index == MLD_FAILURE)
                return SNMP_FAILURE;
            pCacheEntry = MLDGetCacheEntry ((tIp6Addr *) (VOID *)
                                            pMldCacheAddress->pu1_OctetList,
                                            i4MldCacheIfIndex);
            if (pCacheEntry == NULL)
            {
                return SNMP_FAILURE;
            }
            if (MLD_CACHE_RS (i4Index) != MLD_RS_ACTIVE)
            {
                u4Duration = McastListenerVal (pCacheEntry->u4IfIndex);

                MLD_MOD_TRC_ARG1 (MLD_DBG_MAP, MLD_OS_RES_TRC, "MLD",
                                  "Starting Listener Timer for Interface: %d\n",
                                  i4MldCacheIfIndex);
                MLDReStartTmr (&((pCacheEntry->ListenerTmr).node), u4Duration);
            }

            MLD_CACHE_STATE (i4Index) = MLD_LISTENERS_PRE;
            MLD_CACHE_RS (i4Index) = MLD_RS_ACTIVE;
            break;
        case MLD_RS_NOTINSERVICE:
            if (i4Index == MLD_FAILURE)
                return SNMP_FAILURE;
            MLD_CACHE_RS (i4Index) = MLD_RS_NOTINSERVICE;
            break;
        case MLD_RS_CREATEANDGO:
            pCacheEntry = MLDGetCacheEntry ((tIp6Addr *) (VOID *)
                                            pMldCacheAddress->pu1_OctetList,
                                            i4MldCacheIfIndex);
            if (pCacheEntry != NULL)
            {
                return SNMP_FAILURE;
            }

            pCacheEntry = MLDAddCacheEntry ((tIp6Addr *) (VOID *)
                                            pMldCacheAddress->pu1_OctetList,
                                            i4MldCacheIfIndex);
            if (pCacheEntry == NULL)
            {
                return SNMP_FAILURE;
            }

            pCacheEntry->i4RowStatus = MLD_RS_ACTIVE;
            u4Duration = McastListenerVal (pCacheEntry->u4IfIndex);
            MLD_MOD_TRC_ARG1 (MLD_DBG_MAP, MLD_OS_RES_TRC, "MLD",
                              "Starting Listener Timer for Interface: %d\n",
                              i4MldCacheIfIndex);
            MLDReStartTmr (&((pCacheEntry->ListenerTmr).node), u4Duration);
            pCacheEntry->u1State = MLD_LISTENERS_PRE;
            break;
        case MLD_RS_CREATEANDWAIT:
            pCacheEntry = MLDGetCacheEntry ((tIp6Addr *) (VOID *)
                                            pMldCacheAddress->pu1_OctetList,
                                            i4MldCacheIfIndex);
            if (pCacheEntry != NULL)
            {
                return SNMP_FAILURE;
            }

            pCacheEntry = MLDAddCacheEntry ((tIp6Addr *) (VOID *)
                                            pMldCacheAddress->pu1_OctetList,
                                            i4MldCacheIfIndex);
            if (pCacheEntry == NULL)
            {
                return SNMP_FAILURE;
            }
            pCacheEntry->i4RowStatus = MLD_RS_NOTINSERVICE;
            break;
        case MLD_RS_DESTROY:
            if (i4Index == MLD_FAILURE)
                return SNMP_FAILURE;
            pCacheEntry = MLDGetCacheEntry ((tIp6Addr *) (VOID *)
                                            pMldCacheAddress->pu1_OctetList,
                                            i4MldCacheIfIndex);
            if (pCacheEntry == NULL)
            {
                return SNMP_FAILURE;
            }
            MLDStopTmr (gMldTimerListId, &((pCacheEntry->RxmtTmr).node));
            MLDStopTmr (gMldTimerListId, &((pCacheEntry->ListenerTmr).node));

            if (gMLDCacheTable[i4Index].pBuf)
                MLDRelBuf (gMLDCacheTable[i4Index].pBuf, FALSE);
            if (gMLDCacheTable[i4Index].pParams)
                MEM_FREE (gMLDCacheTable[i4Index].pParams);
            MEMSET (&gMLDCacheTable[i4Index], MLD_ZERO,
                    sizeof (tMLDCacheEntry));
            gMLDCacheTable[i4Index].u1Mode = MLD_INVALID;
            break;
        default:
            return SNMP_FAILURE;
    }                            /* end of switch */
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2MldCacheSelf
 Input       :  The Indices
                MldCacheAddress
                MldCacheIfIndex

                The Object 
                testValMldCacheSelf
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MldCacheSelf (UINT4 *pu4ErrorCode,
                       tSNMP_OCTET_STRING_TYPE * pMldCacheAddress,
                       INT4 i4MldCacheIfIndex, INT4 i4TestValMldCacheSelf)
{
    UNUSED_PARAM (pMldCacheAddress);
    UNUSED_PARAM (i4MldCacheIfIndex);
    /* gMLDCacheTable cannot be accessed if MLD is down */
    if (gMLDCacheTable == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4TestValMldCacheSelf == MLD_TRUE) ||
        (i4TestValMldCacheSelf == MLD_FALSE))
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2MldCacheStatus
 Input       :  The Indices
                MldCacheAddress
                MldCacheIfIndex

                The Object 
                testValMldCacheStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MldCacheStatus (UINT4 *pu4ErrorCode,
                         tSNMP_OCTET_STRING_TYPE * pMldCacheAddress,
                         INT4 i4MldCacheIfIndex, INT4 i4TestValMldCacheStatus)
{
    UNUSED_PARAM (pMldCacheAddress);
    UNUSED_PARAM (i4MldCacheIfIndex);
    /* gMLDCacheTable cannot be accessed if MLD is down */
    if (gMLDCacheTable == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((gu4MLDGlobalStatus & MLD_ROUTER) == MLD_ROUTER)
    {
        if ((i4TestValMldCacheStatus == MLD_RS_ACTIVE) ||
            (i4TestValMldCacheStatus == MLD_RS_CREATEANDWAIT) ||
            (i4TestValMldCacheStatus == MLD_RS_CREATEANDGO) ||
            (i4TestValMldCacheStatus == MLD_RS_DESTROY) ||
            (i4TestValMldCacheStatus == MLD_RS_NOTINSERVICE))
        {
            return SNMP_SUCCESS;
        }
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2MldCacheTable
 Input       :  The Indices
                MldCacheAddress
                MldCacheIfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2MldCacheTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
