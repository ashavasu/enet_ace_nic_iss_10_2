/*  ---------------------------------------------------------------------------
 * |  FILE NAME             : mldcli.c                                         |
 * |                                                                           |
 * |  PRINCIPAL AUTHOR      : Aricent Inc.                                     |
 * |                                                                           |
 * |  SUBSYSTEM NAME        : MLD                                              |
 * |                                                                           |
 * |  MODULE NAME           : MLD INTERFACE                                    |
 * |                                                                           |
 * |  LANGUAGE              : C                                                |
 * |                                                                           |
 * |  DESCRIPTION           : Action routines for MLD specific CLI             |
 * |                          commands.                                        |
 * |                                                                           |
 *  ---------------------------------------------------------------------------
 */
#ifndef __MLDCLL_C__
#define __MLDCLL_C__
#include "mldinc.h"
#include "mldcli.h"

#ifdef MLD_WANTED
/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : cli_process_mld_cmd                                */
/*                                                                           */
/*     DESCRIPTION      : This function takes in variable no. of arguments   */
/*                        and process the commands for the MLD module        */
/*                                                                           */
/*     INPUT            : CliHandle -  CLIHandler                            */
/*                        u4Command -  Command Identifier                    */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/
INT4
cli_process_mld_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT1              *args[CLI_MAX_ARGS];
    INT1                argno = 0;
    INT4                i4Inst = 0;  
    INT4                i4IfaceIndex = 0;
    INT4                i4RetStatus = CLI_SUCCESS;

    va_start (ap, u4Command);

    /* third arguement is always interface name/index */
    i4Inst = va_arg (ap, INT4);
    
    if (i4Inst != 0)
    {
        i4IfaceIndex = (UINT4) i4Inst;
    }
    else
    {
        i4IfaceIndex = CLI_GET_IFINDEX ();
    }

    while (1)
    {
        args[argno++] = va_arg (ap, UINT1 *);
        if (argno == CLI_MAX_ARGS)
            break;
    }
    va_end (ap);


    switch (u4Command)
    {
        case MLD_CLI_INTF_STATUS:
            i4RetStatus = MldSetInterfaceStatus (CliHandle, i4IfaceIndex,
                                                 CLI_PTR_TO_I4 (args[0]));
            break;          
    
        case MLD_CLI_SHOW:
            MldShowStatus (CliHandle);
            break; 
    }

    CLI_SET_CMD_STATUS (i4RetStatus);
    return i4RetStatus;
}

/*********************************************************************
*  Function Name : MldSetInterfaceStatus
*  Description   : Enable/Disable MLD status in particular interface
*  Input(s)      : CliHandle - CLIHandler
*                  i4IfaceIndex - Interface Index
*                  i1StatusFlag - MLD Status flag (enable/disable) 
*  Output(s)     : None 
*  Return Values : CLI_SUCCESS/CLI_FAILURE
*********************************************************************/
INT4
MldSetInterfaceStatus (tCliHandle CliHandle, INT4 i4IfaceIndex,
                                 INT4 i4StatusFlag)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4Status = 0;

    if (i4StatusFlag == CLI_ENABLE)
    {
        i4Status = MLD_RS_CREATEANDGO;
    }
    else
    {
        i4Status = MLD_RS_DESTROY;
    }  

    if (nmhTestv2MldInterfaceStatus (&u4ErrorCode,
                                     i4IfaceIndex,
                                     i4Status) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetMldInterfaceStatus (i4IfaceIndex,
                                  i4Status) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*********************************************************************
*  Function Name : MldShowStatus
*  Description   : Displays mld status
*  Input(s)      : CliHandle - CLIHandler
*  Output(s)     : None 
*  Return Values : None
*********************************************************************/
VOID
MldShowStatus (tCliHandle CliHandle)
{
    INT4   i4RetVal = SNMP_FAILURE;
    INT4   i4IfIndex = 0;
    INT4   i4NextIfIndex = 0;
    INT4   i4MldInterfaceStatus = 0;
    UINT1  au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    
    i4RetVal = nmhGetFirstIndexMldInterfaceTable (&i4NextIfIndex);
    if (i4RetVal == SNMP_FAILURE)
    {
       return;
    }

    while (i4RetVal != SNMP_FAILURE)
    {
        i4IfIndex = i4NextIfIndex;
       
        MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
        CfaCliGetIfName ((UINT4) i4IfIndex, (INT1 *) au1IfName);

        nmhGetMldInterfaceStatus (i4IfIndex,
                                  &i4MldInterfaceStatus);
         
        if (i4MldInterfaceStatus == MLD_RS_ACTIVE)
        {
            CliPrintf (CliHandle, 
                 "\r\n MLD status is enabled in %s\r\n",
                                                   au1IfName);
        }
        else
        {
            CliPrintf (CliHandle, 
                 "\r\n MLD status is disabled in %s\r\n",
                                                   au1IfName);
        }  
        i4RetVal = nmhGetNextIndexMldInterfaceTable (i4IfIndex,
                                                   &i4NextIfIndex);
    };
    return;
}
#endif
#endif
