/********************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: mldport.c,v 1.4 2012/12/13 14:32:25 siva Exp $
 *
 * Description: Files contains API needs to be ported for diffrent OS 
 *
 *******************************************************************/
#include "mldinc.h"
#include "fssocket.h"

INT4                gi4MLDSockId = -1;
/****************************************************************************
 Function    :  MLDCreateSocket
 Input       :  None
 Output      :  None
 Description :  Creates the socket which is used to send the MLD Pkts out
 Returns     :  MLD_SUCCESS/MLD_FAILURE.
****************************************************************************/
INT4
MLDCreateSocket (VOID)
{
    gi4MLDSockId = socket (AF_INET6, SOCK_RAW, IPPROTO_ICMPV6);

    if (gi4MLDSockId < 0)
    {
        MLD_MOD_TRC (MLD_DBG_MAP, MLD_OS_RES_TRC, "MLD", "ICMPv6 Socket "
                     "creation - FAILED \n");
        return MLD_FAILURE;
    }

    return MLD_SUCCESS;
}

/****************************************************************************
 Function    :  MLDSetSendSockOptions
 Input       :  None
 Output      :  None
 Description :  Set the socket options to the MLD socket
 Returns     :  MLD_SUCCESS/MLD_FAILURE.
 *****************************************************************************/
INT4
MLDSetSendSockOptions (VOID)
{
    INT4                i4Option = FALSE;
    INT4                i4RetVal = MLD_SUCCESS;

    i4RetVal = setsockopt (gi4MLDSockId, IPPROTO_IPV6, IPV6_MULTICAST_LOOP,
                           (INT4 *) &i4Option, sizeof (INT4));
    if (i4RetVal != 0)
    {
        perror ("MLD - IPV6_MULTICAST_LOOP ");
        MLD_MOD_TRC (MLD_DBG_MAP, MLD_OS_RES_TRC, "MLD", "IPV6_MULTICAST_LOOP "
                     "set socket option - FAILED \n");
        i4RetVal = MLD_FAILURE;
    }

    return i4RetVal;
}

/****************************************************************************
 Function    :  MLDCloseSocket
 Input       :  None 
 Output      :  None
 Description :  Closes the MLD socket. 
 Returns     :  None
****************************************************************************/
VOID
MLDCloseSocket (VOID)
{
    close (gi4MLDSockId);
    gi4MLDSockId = -1;
}

#ifdef LNXIP6_WANTED
/****************************************************************************
 Function    :  MLDSendPktToIpv6
 Input       :  tCRU_BUF_CHAIN_HEADER * pBuf MLD data packet
                tHlToIp6Params * pParams - Params required to send the packet
                out
 Output      :  None 
 Description :  Copies the MLD data from the CRU buffer to linear buffer and 
                sends over the socket.
 Returns     :  MLD_SUCCESS/MLD_FAILURE.
****************************************************************************/
INT4
MLDSendPktToIpv6 (tCRU_BUF_CHAIN_HEADER * pBuf, tHlToIp6Params * pParams)
{
    INT4                i4HopLimit = 0;
    INT4                i4RetVal = 0;
    UINT1              *pu1MldPkt = NULL;
    struct sockaddr_in6 Dest6Addr;
    struct cmsghdr     *pCmsgInfo = NULL;
    struct iovec        Iov;
    struct in6_pktinfo *pIpPktInfo = NULL;
    struct msghdr       PktInfo;
    tCfaIfInfo          IfInfo;
    UINT1               au1Cmsg[MLD_ANCILLARY_DATA_LEN];

    MEMSET (&Iov, 0, sizeof (struct iovec));
    MEMSET (&PktInfo, 0, sizeof (struct msghdr));
    MEMSET (&au1Cmsg, 0, MLD_ANCILLARY_DATA_LEN);
    MEMSET (&Dest6Addr, 0, sizeof (struct sockaddr_in6));
    MEMSET (&IfInfo, 0, sizeof (tCfaIfInfo));

    /* Allocate the memory for linear buffer */
    pu1MldPkt = MEM_MALLOC (pParams->u4Len, UINT1);
    if (pu1MldPkt == NULL)
    {
        MLD_MOD_TRC (MLD_DBG_MAP, MLD_OS_RES_TRC, "MLD", "Memory allocation "
                     "failed for linear buffer \n");
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return MLD_FAILURE;
    }

    /* Copy the Data from CRU buffer to linear buffer */
    i4RetVal = CRU_BUF_Copy_FromBufChain (pBuf, pu1MldPkt, 0, pParams->u4Len);
    if (i4RetVal == CRU_FAILURE)
    {
        MLD_MOD_TRC (MLD_DBG_MAP, MLD_OS_RES_TRC, "MLD", "Copy from CRU Buff "
                     "to linear buffer failed \n");
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        MEM_FREE (pu1MldPkt);
        return MLD_FAILURE;
    }

    /* Release the CRU buffer */
    CRU_BUF_Release_MsgBufChain (pBuf, FALSE);

    i4HopLimit = pParams->u1Hlim;
    i4RetVal = setsockopt (gi4MLDSockId, IPPROTO_IPV6, IPV6_MULTICAST_HOPS,
                           (INT4 *) &i4HopLimit, sizeof (INT4));
    if (i4RetVal < 0)
    {
        perror ("MLD - IPV6_MULTICAST_HOPS ");
        MLD_MOD_TRC (MLD_DBG_MAP, MLD_OS_RES_TRC, "MLD", "IPV6_MULTICAST_HOPS "
                     "set socket option - FAILED \n");
        MEM_FREE (pu1MldPkt);
        return MLD_FAILURE;
    }

    Dest6Addr.sin6_family = AF_INET6;
    MEMCPY (&(Dest6Addr.sin6_addr), &(pParams->Ip6DstAddr), sizeof (tIp6Addr));

    Iov.iov_base = (void *) &pu1MldPkt;
    Iov.iov_len = pParams->u4Len;

    PktInfo.msg_name = (void *) &Dest6Addr;
    PktInfo.msg_namelen = sizeof (struct sockaddr_in6);
    PktInfo.msg_iov = &Iov;
    PktInfo.msg_iovlen = 1;
    PktInfo.msg_control = (void *) &au1Cmsg;
    PktInfo.msg_controllen = CMSG_SPACE (sizeof (struct in6_pktinfo));

    pCmsgInfo = CMSG_FIRSTHDR (&PktInfo);
    pCmsgInfo->cmsg_level = IPPROTO_IPV6;
    pCmsgInfo->cmsg_type = IPV6_PKTINFO;
    pCmsgInfo->cmsg_len = CMSG_LEN (sizeof (struct in6_pktinfo));

    pIpPktInfo = (struct in6_pktinfo *) CMSG_DATA (pCmsgInfo);
    MEMCPY (&(pIpPktInfo->ipi6_addr), &(pParams->Ip6SrcAddr),
            sizeof (tIp6Addr));
    /* Get the interface port number */
    CfaGetIfInfo (pParams->u4Index, &IfInfo);
    pIpPktInfo->ipi6_ifindex = IfInfo.i4IpPort;

    i4RetVal = sendmsg (gi4MLDSockId, &PktInfo, 0);

    if (i4RetVal < 0)
    {
        perror ("MLD - sendmsg ");
        MLD_MOD_TRC (MLD_DBG_MAP, MLD_OS_RES_TRC, "MLD", "sendmsg - FAILED \n");
        MEM_FREE (pu1MldPkt);
        return MLD_FAILURE;
    }

    MEM_FREE (pu1MldPkt);
    return MLD_SUCCESS;
}
#endif

/****************************************************************************
 Function    :  MLDSendPkt
 Input       :  tCRU_BUF_CHAIN_HEADER * pBuf MLD data packet
                tHlToIp6Params * pParams - Params required to send the packet
 Output      :  None.
 Description :  This function sends a MLD packet to the IP layer.
 Returns     :  MLD_SUCCESS/MLD_FAILURE
****************************************************************************/
INT4
MLDSendPkt (tCRU_BUF_CHAIN_HEADER * pBuf, tHlToIp6Params * pParams)
{

#ifdef LNXIP6_WANTED
    if (MLDSendPktToIpv6 (pBuf, pParams) == MLD_FAILURE)
    {
        return MLD_FAILURE;
    }
#else
    if (Ip6RcvFromHl (pBuf, pParams) == IP6_FAILURE)
    {
        return MLD_FAILURE;
    }
#endif

    return MLD_SUCCESS;
}
