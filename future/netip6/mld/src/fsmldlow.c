# include  "include.h"
# include  "fsmldlow.h"
# include  "fsmldcon.h"
# include  "fsmldogi.h"
# include  "midconst.h"

#include "mldinc.h"

extern UINT4        stdmld[7];
extern UINT4        fsmld[8];
extern UINT4        FsmldProtocolUpDown[10];
PRIVATE VOID        MldNotifyProtocolShutdownStatus (VOID);
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsmldNoOfCacheEntries
 Input       :  The Indices

                The Object 
                retValFsmldNoOfCacheEntries
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsmldNoOfCacheEntries (UINT4 *pu4RetValFsmldNoOfCacheEntries)
{
    *pu4RetValFsmldNoOfCacheEntries = MLD_MAX_CACHE_ENTRIES;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsmldNoOfRoutingProtocols
 Input       :  The Indices

                The Object 
                retValFsmldNoOfRoutingProtocols
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsmldNoOfRoutingProtocols (UINT4 *pu4RetValFsmldNoOfRoutingProtocols)
{
    *pu4RetValFsmldNoOfRoutingProtocols = MLD_MAX_ROUTING_PROTOCOLS;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsmldTraceDebug
 Input       :  The Indices

                The Object 
                retValFsmldTraceDebug
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsmldTraceDebug (UINT4 *pu4RetValFsmldTraceDebug)
{
    *pu4RetValFsmldTraceDebug = gu4MldDbgMap;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsmldMode
 Input       :  The Indices

                The Object 
                retValFsmldMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsmldMode (INT4 *pi4RetValFsmldMode)
{
    *pi4RetValFsmldMode = gu4MLDGlobalStatus;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsmldProtocolUpDown
 Input       :  The Indices

                The Object 
                retValFsmldProtocolUpDown
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsmldProtocolUpDown (INT4 *pi4RetValFsmldProtocolUpDown)
{
    /* To find whether the MLD task is active, the global variable 
     * can be checked to wheather memory is allocated for it 
     */
    if (gMLDCacheTable == NULL)
        *pi4RetValFsmldProtocolUpDown = MLD_SHUTDOWN;
    else
        *pi4RetValFsmldProtocolUpDown = MLD_INIT;

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsmldNoOfCacheEntries
 Input       :  The Indices

                The Object 
                setValFsmldNoOfCacheEntries
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsmldNoOfCacheEntries (UINT4 u4SetValFsmldNoOfCacheEntries)
{
    tMldSystemSize      MldNewSizingParams;
    GetMldSizingParams (&MldNewSizingParams);
    MldNewSizingParams.u4MldMaxCacheEntries = u4SetValFsmldNoOfCacheEntries;
    SetMldSizingParams (&MldNewSizingParams);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsmldNoOfRoutingProtocols
 Input       :  The Indices

                The Object 
                setValFsmldNoOfRoutingProtocols
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsmldNoOfRoutingProtocols (UINT4 u4SetValFsmldNoOfRoutingProtocols)
{
    tMldSystemSize      MldNewSizingParams;
    GetMldSizingParams (&MldNewSizingParams);
    MldNewSizingParams.u4MldMaxRoutingProtocol =
        u4SetValFsmldNoOfRoutingProtocols;
    SetMldSizingParams (&MldNewSizingParams);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsmldTraceDebug
 Input       :  The Indices

                The Object 
                setValFsmldTraceDebug
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsmldTraceDebug (UINT4 u4SetValFsmldTraceDebug)
{
    gu4MldDbgMap = u4SetValFsmldTraceDebug;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsmldMode
 Input       :  The Indices

                The Object 
                setValFsmldMode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsmldMode (INT4 i4SetValFsmldMode)
{
    gu4MLDGlobalStatus = i4SetValFsmldMode;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsmldProtocolUpDown
 Input       :  The Indices

                The Object 
                setValFsmldProtocolUpDown
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsmldProtocolUpDown (INT4 i4SetValFsmldProtocolUpDown)
{
    if (i4SetValFsmldProtocolUpDown == MLD_SHUTDOWN)
    {
        if (gMLDCacheTable == NULL)
        {
            return SNMP_SUCCESS;
        }
        MLDShutdown ();
        /* Notify MSR with the mld oids */
        MldNotifyProtocolShutdownStatus ();
    }
    else
    {
        /* call MLDInit only if MLD task is not UP */
        if (gMLDInterfaceTable != NULL)
            MLDInit ();
    }

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsmldNoOfCacheEntries
 Input       :  The Indices

                The Object 
                testValFsmldNoOfCacheEntries
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsmldNoOfCacheEntries (UINT4 *pu4ErrorCode,
                                UINT4 u4TestValFsmldNoOfCacheEntries)
{
    UNUSED_PARAM (u4TestValFsmldNoOfCacheEntries);
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2FsmldNoOfRoutingProtocols
 Input       :  The Indices

                The Object 
                testValFsmldNoOfRoutingProtocols
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsmldNoOfRoutingProtocols (UINT4 *pu4ErrorCode,
                                    UINT4 u4TestValFsmldNoOfRoutingProtocols)
{
    UNUSED_PARAM (u4TestValFsmldNoOfRoutingProtocols);
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsmldTraceDebug
 Input       :  The Indices

                The Object 
                testValFsmldTraceDebug
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsmldTraceDebug (UINT4 *pu4ErrorCode, UINT4 u4TestValFsmldTraceDebug)
{

    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4TestValFsmldTraceDebug);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsmldMode
 Input       :  The Indices

                The Object 
                testValFsmldMode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsmldMode (UINT4 *pu4ErrorCode, INT4 i4TestValFsmldMode)
{
    if ((i4TestValFsmldMode == MLD_ROUTER) ||
        (i4TestValFsmldMode == MLD_HOST) ||
        (i4TestValFsmldMode == (MLD_ROUTER | MLD_HOST)))
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsmldProtocolUpDown
 Input       :  The Indices

                The Object 
                testValFsmldProtocolUpDown
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsmldProtocolUpDown (UINT4 *pu4ErrorCode,
                              INT4 i4TestValFsmldProtocolUpDown)
{
    if ((i4TestValFsmldProtocolUpDown == MLD_SHUTDOWN) ||
        (i4TestValFsmldProtocolUpDown == MLD_INIT))
        return SNMP_SUCCESS;

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

    return SNMP_FAILURE;
}                                /* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsmldNoOfCacheEntries
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsmldNoOfCacheEntries (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsmldNoOfRoutingProtocols
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsmldNoOfRoutingProtocols (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsmldTraceDebug
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsmldTraceDebug (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsmldMode
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsmldMode (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsmldProtocolUpDown
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsmldProtocolUpDown (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  MldNotifyProtocolShutdownStatus
 Input       :  None
 Output      :  None
 Returns     :  None
****************************************************************************/

VOID
MldNotifyProtocolShutdownStatus (VOID)
{
#ifdef ISS_WANTED
    UINT1               au1ObjectOid[3][SNMP_MAX_OID_LENGTH];

    /* The oid list contains the list of Oids that has been registered 
     * for the protocol + the oid of the SystemControl object. */
    SNMPGetOidString (fsmld, (sizeof (fsmld) / sizeof (UINT4)),
                      au1ObjectOid[0]);
    SNMPGetOidString (stdmld, (sizeof (stdmld) / sizeof (UINT4)),
                      au1ObjectOid[1]);
    SNMPGetOidString (FsmldProtocolUpDown,
                      (sizeof (FsmldProtocolUpDown) / sizeof (UINT4)),
                      au1ObjectOid[2]);

    /* Send a notification to MSR to process the mld shutdown, with 
     * mld oids and its system control object 
     * As the protocol does not have MI support, invalid context id is sent */
    MSR_NOTIFY_PROTOCOL_SHUT (au1ObjectOid, 3, MSR_INVALID_CNTXT);
#endif
    return;
}
