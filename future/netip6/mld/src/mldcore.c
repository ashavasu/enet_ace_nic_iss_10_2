
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: mldcore.c,v 1.4 2010/11/18 14:05:35 siva Exp $
 *
 * Description:
 *
 *******************************************************************/
#include "mldinc.h"

/****************************************************************************
 Function    :  MLDProcessPkt
 Input       :  Pointer to the Buffer received from IP6            
 Output      :  None.
 Description :  This function will be called from MLDTaskMain() only.
                It will first validate the MLD Packet. The source of the 
                packet should be a link local address, the length should 
                be at least 24 octets and the multicast address in the 
                mld header should be a valid multicast address or unspecified.
                Based on the packet type 'query','report' or 'done', it will 
                call the corresponding functions.
 Returns     :  None.
****************************************************************************/
VOID
MLDProcessPkt (tCRU_BUF_CHAIN_HEADER * pBuf)
{
    UINT4               u4IfIndex;
    UINT2               u2Len;
    UINT1               u1Type;
    tMLDHdr             MldHdr;
    tMLDCacheEntry     *pCacheEntry = NULL;
    tMLDHostEntry      *pHostEntry = NULL;
    tIp6Addr            SrcAddr;
    tMLDParms          *pParms = NULL;
    tNetIpv6IfInfo      NetIpv6IfInfo;

    MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                 "Entering MLDProcessPkt\n ");

    pParms = (tMLDParms *) MLDParams (pBuf);
    u4IfIndex = pParms->u4IfIndex;
    u2Len = pParms->u2Len;
    MEMCPY (&SrcAddr, &(pParms->SrcAddr), sizeof (tIp6Addr));

    /* MLD Messages must come from a link-local IPv6 Source address. */
    if (IS_ADDR_LLOCAL (SrcAddr) == FALSE)
    {
        MLDRelBuf (pBuf, FALSE);
        MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                     "exiting MLDProcessPkt Failure:Source address"
                     "is not link local\n");
        return;
    }

    /* MLD Messages must be at least 24 octets long */
    if (u2Len < MLD_MAX_PKT_LEN)
    {
        MLDRelBuf (pBuf, FALSE);
        MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                     "Exiting MLDProcessPkt Failure:Packet size"
                     "received is less\n ");
        return;
    }

    /* Read the MLD Header from the buffer */
    if (CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &MldHdr,
                                   MLD_ZERO, sizeof (tMLDHdr)) == CRU_FAILURE)
    {
        MLDRelBuf (pBuf, FALSE);
        MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                     "Exiting MLDProcessPkt Failure:Could not"
                     "read the mld hdr\n ");
        return;
    }

    if ((IS_ADDR_MULTI (MldHdr.McastAddr) == FALSE) &&
        (IS_ADDR_UNSPECIFIED (MldHdr.McastAddr) == FALSE))
    {
        MLDRelBuf (pBuf, FALSE);
        MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                     "Exiting MLDProcessPkt Failure:Not Multicast address"
                     "or zero\n");
        return;
    }

    u1Type = MldHdr.u1Type;
    switch (u1Type)
    {
        case MLD_QUERY:
            /* Received a MLD Query */
            if (IS_ADDR_MULTI (MldHdr.McastAddr))
            {
                /* Received a MLD m-a-s Query */
                if ((gu4MLDGlobalStatus & MLD_ROUTER) == MLD_ROUTER)
                {
                    /* Router functionality */
                    pCacheEntry = MLDGetCacheEntry (&MldHdr.McastAddr,
                                                    u4IfIndex);
                    if (pCacheEntry != NULL)
                    {
                        MLDMASQueryReceived (pCacheEntry,
                                             MldHdr.u2MaxRespDelay);
                    }
                }

                if ((gu4MLDGlobalStatus & MLD_HOST) == MLD_HOST)
                {
                    /* Host functionality */
                    pHostEntry = MLDGetHostEntry (u4IfIndex, &MldHdr.McastAddr);
                    if (pHostEntry != NULL)
                    {
                        MLDHostMASQueryReceived (pHostEntry,
                                                 MldHdr.u2MaxRespDelay);
                    }
                }
            }
            else
            {
                /* General Query received */
                if ((gu4MLDGlobalStatus & MLD_HOST) == MLD_HOST)
                {
                    MLDHostGenQueryReceived (MldHdr.u2MaxRespDelay);
                }
            }

            if ((gu4MLDGlobalStatus & MLD_ROUTER) == MLD_ROUTER)
            {
                /* Router functionality - for both general and mas query */
                if (NetIpv6GetIfInfo (u4IfIndex, &NetIpv6IfInfo)
                    != NETIPV6_SUCCESS)
                {
                    break;
                }
                if (MEMCMP (&SrcAddr, &NetIpv6IfInfo.Ip6Addr, sizeof (tIp6Addr))
                    < MLD_ZERO)
                {
                    /* Received a query with low IP Address */
                    MLDLowIPQueryReceived (u4IfIndex);
                    MEMCPY (&MLD_INTERFACE_IFQUERIER (u4IfIndex),
                            &SrcAddr, sizeof (tIp6Addr));
                    OsixGetSysTime (&MLD_INTERFACE_QUERIERUPTIME (u4IfIndex));
                }
            }

            break;

        case MLD_REPORT:
            if (IS_ADDR_MULTI (MldHdr.McastAddr) == TRUE)
            {
                if ((gu4MLDGlobalStatus & MLD_ROUTER) == MLD_ROUTER)
                {
                    pCacheEntry = MLDGetCacheEntry (&MldHdr.McastAddr,
                                                    u4IfIndex);
                    if (pCacheEntry == NULL)
                    {
                        /* Received a REPORT from a multicast group
                         * for the first time 
                         */
                        pCacheEntry = MLDAddCacheEntry (&MldHdr.McastAddr,
                                                        u4IfIndex);
                    }
                    if (pCacheEntry != NULL)
                    {
                        MLDReportReceived (pCacheEntry);
                        MEMCPY (&pCacheEntry->LastReporter, &SrcAddr,
                                sizeof (tIp6Addr));
                    }
                }

                if ((gu4MLDGlobalStatus & MLD_HOST) == MLD_HOST)
                {
                    pHostEntry = MLDGetHostEntry (u4IfIndex, &MldHdr.McastAddr);
                    if (pHostEntry != NULL)
                    {
                        MLDHostReportReceived (pHostEntry);
                    }
                }
            }

            break;

        case MLD_DONE:
            if (IS_ADDR_MULTI (MldHdr.McastAddr) == TRUE)
            {
                if ((gu4MLDGlobalStatus & MLD_ROUTER) == MLD_ROUTER)
                {
                    pCacheEntry = MLDGetCacheEntry (&MldHdr.McastAddr,
                                                    u4IfIndex);
                    if (pCacheEntry != NULL)
                    {
                        MLDDoneReceived (pCacheEntry);
                    }
                }
            }

            break;

        default:
            break;

    }                            /* end of switch */

    MLD_MOD_TRC (MLD_DBG_MAP, MLD_OS_RES_TRC, "MLD", "Releasing memory!!!\n");
    MLDRelBuf (pBuf, FALSE);

    MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                 "Exiting MLDProcessPkt Successfully!!!\n");
    return;
}

/****************************************************************************
 Function    :  MLDMASQueryReceived
 Input       :  Pointer to the Cache Entry
                Maximum Response Delay 
 Output      :  None.
 Description :  This function will be called from MLDProcessPkt() when a MLD
                M-A-S query is received. If the cache entry state is 
                     MLD_LISTENERS_PRE it will restart the listener's timer.
 Returns     :  MLD_SUCCESS/MLD_FAILURE
****************************************************************************/
INT1
MLDMASQueryReceived (tMLDCacheEntry * pCacheEntry, UINT2 u2MaxRespDelay)
{
    UINT4               u4Duration;

    MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                 "Entering MLDMASQueryReceived\n");

    if (pCacheEntry->u1Mode != MLD_NON_QUERIER)
    {
        MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                     "Exiting MLDMASQueryReceived:Failure\n");
        return MLD_FAILURE;
    }

    if (pCacheEntry->u1State == MLD_LISTENERS_PRE)
    {

        /* u2MaxRespDelay is in ms, so convert it to seconds 
         * before starting the timer
         * Check that u2MaxRespDelay is not less the granurality
         * of the timer. If it is less, start the timer
         * for one tick. (minimum granurality) 
         */
        if (u2MaxRespDelay < (MLD_THOUSAND / MLD_SYS_TICKS))
            u4Duration = MLD_ONE;
        else
        {
            u4Duration = (u2MaxRespDelay * MLD_SYS_TICKS) / MLD_THOUSAND;
        }

        u4Duration = u4Duration * pCacheEntry->u4LastListenerCount;

        MLD_MOD_TRC (MLD_DBG_MAP, MLD_OS_RES_TRC, "MLD",
                     "Stopping Listener Timer\n ");
        MLDStopTmr (gMldTimerListId, &(pCacheEntry->ListenerTmr.node));

        MLD_MOD_TRC (MLD_DBG_MAP, MLD_OS_RES_TRC, "MLD",
                     "Starting Listener Timer\n ");
        MLDStartTmr (gMldTimerListId, &(pCacheEntry->ListenerTmr.node),
                     u4Duration);

        MLD_MOD_TRC_ARG2 (MLD_DBG_MAP, MLD_SEM_TRC, "MLD",
                          "Changing from MLD_LISTENERS_PRE to"
                          "MLD_CHK_LISTENERS for interface %d"
                          "and address %s\n",
                          pCacheEntry->u4IfIndex,
                          Ip6PrintAddr (&pCacheEntry->McastAddr));
        pCacheEntry->u1State = MLD_CHK_LISTENERS;
    }

    MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                 "Exiting MLDMASQueryReceived:Successfully\n");
    return MLD_SUCCESS;

}

/****************************************************************************
 Function    :  MLDReportReceived
 Input       :  Pointer to the Cache Entry
 Output      :  None.
 Description :  This function will be called from MLDProcessPkt() when a 
                report is received. When the report is received for the 
                     first time, the same is notified to Routing Protocols.
                if the report is received for entry in MLD_CHK_LISTENERS
                     i.e a done was received previously, the retransmission timer
                     is stopped and state changed to MLD_LISTENERS_PRE. The
                     listener's timer is restarted.
 Returns     :  MLD_SUCCESS/MLD_FAILURE
****************************************************************************/
INT1
MLDReportReceived (tMLDCacheEntry * pCacheEntry)
{
    UINT1               u1State;
    UINT4               u4Duration;
    INT4                i4Index;

    MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                 "Entering MLDReportReceived\n");

    i4Index = GET_CACHE_INDEX (pCacheEntry->u4IfIndex, &pCacheEntry->McastAddr);
    if (i4Index == MLD_FAILURE)
    {
        MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                     "Exiting MLDReportReceived: Cannot find entry!!!\n");
        return MLD_FAILURE;
    }

    u1State = pCacheEntry->u1State;
    switch (u1State)
    {
        case MLD_NO_LISTENERS:
            MLDNotifyRouting (MLD_ADD, &MLD_CACHE_MCASTADDR (i4Index),
                              MLD_CACHE_IFINDEX (i4Index));

            MLD_MOD_TRC_ARG2 (MLD_DBG_MAP, MLD_SEM_TRC, "MLD",
                              "Changing from MLD_NO_LISTENERS to"
                              "MLD_LISTENERS_PRE for interface %d"
                              "and address %s\n",
                              MLD_CACHE_IFINDEX (i4Index),
                              Ip6PrintAddr (&MLD_CACHE_MCASTADDR (i4Index)));

            MLD_CACHE_STATE (i4Index) = MLD_LISTENERS_PRE;

            break;
        case MLD_LISTENERS_PRE:
            break;
        case MLD_CHK_LISTENERS:
            /* In case done messages were received for this address
             * last listener count would have been decremented. After 
             * receiving the report it should be updated to default
             */
            MLD_CACHE_LASTLISTENERCOUNT (i4Index)
                = MLD_INTERFACE_ROBUSTVAR (pCacheEntry->u4IfIndex);

            MLD_MOD_TRC (MLD_DBG_MAP, MLD_OS_RES_TRC, "MLD",
                         "Stopping Retransmission Timer\n ");
            MLDStopTmr (gMldTimerListId, &(MLD_CACHE_RXMTTMR (i4Index).node));

            MLD_MOD_TRC_ARG2 (MLD_DBG_MAP, MLD_SEM_TRC, "MLD",
                              "Changing from MLD_CHK_LISTENERS to"
                              "MLD_LISTENERS_PRE for interface %d"
                              "and address %s\n",
                              MLD_CACHE_IFINDEX (i4Index),
                              Ip6PrintAddr (&MLD_CACHE_MCASTADDR (i4Index)));

            MLD_CACHE_STATE (i4Index) = MLD_LISTENERS_PRE;

            if (MLD_CACHE_MODE (i4Index) == MLD_QUERIER_WAIT)
            {
                MLD_MOD_TRC_ARG2 (MLD_DBG_MAP, MLD_SEM_TRC, "MLD",
                                  "Changing MODE to MLD_NON_QUERIER"
                                  "for interface %d & address %s\n",
                                  MLD_CACHE_IFINDEX (i4Index),
                                  Ip6PrintAddr (&MLD_CACHE_MCASTADDR
                                                (i4Index)));

                MLD_CACHE_MODE (i4Index) = MLD_NON_QUERIER;
            }
            else if (MLD_CACHE_MODE (i4Index) == MLD_NON_QUERIER_WAIT)
            {
                MLD_MOD_TRC_ARG2 (MLD_DBG_MAP, MLD_SEM_TRC, "MLD",
                                  "Changing MODE to MLD_QUERIER for interface"
                                  "%d & address %s\n",
                                  MLD_CACHE_IFINDEX (i4Index),
                                  Ip6PrintAddr (&MLD_CACHE_MCASTADDR
                                                (i4Index)));

                MLD_CACHE_MODE (i4Index) = MLD_QUERIER;
            }
            break;

        default:
            break;

    }                            /* end of switch */

    u4Duration = McastListenerVal (MLD_CACHE_IFINDEX (i4Index));

    MLD_MOD_TRC (MLD_DBG_MAP, MLD_OS_RES_TRC, "MLD",
                 "ReStarting Listener Timer\n ");
    MLDReStartTmr (&((MLD_CACHE_LISTENERTMR (i4Index)).node), u4Duration);

    MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                 "Exiting MLDReportReceived\n");
    return MLD_SUCCESS;

}

/****************************************************************************
 Function    :  MLDDoneReceived
 Input       :  Pointer to the Cache Entry
 Output      :  None.
 Description :  This function is called from MLDProcessPkt() when a done 
                message is received. If received for interfaces in NON_QUERIER
                     mode, the function returns failure. If received in 
                     MLD_LISTENERS_PRE state, the funtion starts the listeners 
                     and retransmission timer, sends a  m-a-s query & the state of 
                     the cache entry is changed to MLD_CHK_LISTENERS.
 Returns     :  MLD_SUCCESS/MLD_FAILURE
****************************************************************************/
INT1
MLDDoneReceived (tMLDCacheEntry * pCacheEntry)
{
    UINT1               u1State;
    UINT4               u4Duration;

    MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                 "Entering MLDDoneReceived\n");

    if (pCacheEntry->u1Mode == MLD_NON_QUERIER)
    {
        MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                     "Exiting MLDDoneReceived:Failure Interface"
                     "in NON_QUERIER mode\n");
        return MLD_FAILURE;
    }

    u1State = pCacheEntry->u1State;
    switch (u1State)
    {
        case MLD_NO_LISTENERS:
        case MLD_CHK_LISTENERS:
            break;

        case MLD_LISTENERS_PRE:
            u4Duration =
                MLD_INTERFACE_LASTLISTENERVAL (pCacheEntry->u4IfIndex) *
                pCacheEntry->u4LastListenerCount;

            MLD_MOD_TRC (MLD_DBG_MAP, MLD_OS_RES_TRC, "MLD",
                         "ReStarting Listener Timer \n");
            MLDReStartTmr (&((pCacheEntry->ListenerTmr).node), u4Duration);

            MLD_MOD_TRC (MLD_DBG_MAP, MLD_OS_RES_TRC, "MLD",
                         "Starting Retransmission Timer\n ");
            MLDStartTmr (gMldTimerListId, &((pCacheEntry->RxmtTmr).node),
                         MLD_INTERFACE_LASTLISTENERVAL (pCacheEntry->u4IfIndex)
                         * MLD_SYS_TICKS);

            MLDSendQuery (pCacheEntry->u4IfIndex, &pCacheEntry->McastAddr);
            pCacheEntry->u4LastListenerCount--;

            MLD_MOD_TRC_ARG2 (MLD_DBG_MAP, MLD_SEM_TRC, "MLD",
                              "Changing from MLD_LISTENERS_PRE to"
                              "MLD_CHK_LISTENERS for interface %d"
                              "and address %s\n",
                              pCacheEntry->u4IfIndex,
                              Ip6PrintAddr (&pCacheEntry->McastAddr));
            pCacheEntry->u1State = MLD_CHK_LISTENERS;

            break;

        default:
            break;

    }                            /* end of switch */

    MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                 "Exiting MLDDoneReceived:Successfully\n");
    return MLD_SUCCESS;
}

/****************************************************************************
 Function    :  MLDLowIPQueryReceived
 Input       :  Pointer to the Interface Entry
 Output      :  None.
 Description :  This function is called when a query with low IP address is
                received. If the interface on which we received the packet is 
                     in QUERIER mode, the mode is changed to NON_QUERIER mode, 
                     general query timer is stopped. The other query timer is 
                     started.
 Returns     :  MLD_SUCCESS/MLD_FAILURE
****************************************************************************/
VOID
MLDLowIPQueryReceived (UINT4 u4IfIndex)
{
    UINT4               u4Duration;

    MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                 "Entering MLDLowIPQueryReceived\n");

    if (MLD_INTERFACE_MODE (u4IfIndex) == MLD_INVALID)
    {
        MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                     "Exiting MLDLowIPQueryReceived:Failure\n");
        return;
    }

    if (MLD_INTERFACE_MODE (u4IfIndex) == MLD_QUERIER)
    {
        MLD_MOD_TRC_ARG1 (MLD_DBG_MAP, MLD_SEM_TRC, "MLD",
                          "Changing interface %d to MLD_NON_QUERIER\n",
                          u4IfIndex);
        MLD_INTERFACE_MODE (u4IfIndex) = MLD_NON_QUERIER;

        MLDInterfaceModeChange (u4IfIndex, MLD_NON_QUERIER);

        /* Stop the General Query Timer */
        MLD_MOD_TRC (MLD_DBG_MAP, MLD_OS_RES_TRC, "MLD",
                     "Stopping the General Query Timer \n");
        MLDStopTmr (gMldTimerListId,
                    &((MLD_INTERFACE_GENQUETMR (u4IfIndex)).node));
    }

    u4Duration = MLDOtherQueryTmrVal (u4IfIndex);

    MLD_MOD_TRC (MLD_DBG_MAP, MLD_OS_RES_TRC, "MLD",
                 "ReStarting Other Query Timer \n");
    MLDReStartTmr (&((MLD_INTERFACE_OTHERQUETMR (u4IfIndex)).node), u4Duration);

    MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                 "Exiting MLDLowIPQueryReceived\n");
    return;
}

/****************************************************************************
 Function    :  MLDInterfaceModeChange
 Input       :  Interface                     
                Mode
 Output      :  None.
 Description :  This function is called when a query with LOW IP address is 
                received. All cache entries for the interface on which the 
                     query was received are changed to NON_QUERIER mode.
                     if the state of a cache entry is in MLD_CHK_LISTENERS, the
                    state is changed to some intermediate one...MLD_QUERIER_WAIT 
                     or MLD_NON_QUERIER_WAIT based on the present state of the 
                     entry.
 Returns     :  MLD_SUCCESS/MLD_FAILURE
****************************************************************************/
INT1
MLDInterfaceModeChange (UINT4 u4IfIndex, UINT1 u1Mode)
{
    tMLDInterfaceEntry *pIfEntry = NULL;
    UINT4               u4Count;

    MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                 "Entering MLDInterfaceModeChange\n");

    pIfEntry = MLDGetInterfaceEntry (u4IfIndex);
    if (pIfEntry == NULL)
    {
        MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                     "Exiting MLDInterfaceModeChange:Failure\n");
        return MLD_FAILURE;
    }

    for (u4Count = MLD_ZERO; u4Count < MLD_MAX_CACHE_ENTRIES; u4Count++)
    {
        if (MLD_CACHE_MODE (u4Count) == MLD_INVALID)
        {
            continue;
        }

        if (MLD_CACHE_IFINDEX (u4Count) == u4IfIndex)
        {
            if (MLD_CACHE_STATE (u4Count) != MLD_CHK_LISTENERS)
            {
                MLD_MOD_TRC_ARG3 (MLD_DBG_MAP, MLD_SEM_TRC, "MLD",
                                  "Changing MODE to %d of interface %d"
                                  " & address %s\n", u1Mode, u4IfIndex,
                                  Ip6PrintAddr (&MLD_CACHE_MCASTADDR
                                                (u4Count)));
                MLD_CACHE_MODE (u4Count) = u1Mode;
            }

            else if ((MLD_CACHE_MODE (u4Count) == MLD_NON_QUERIER)
                     && (u1Mode == MLD_QUERIER))
            {
                MLD_MOD_TRC_ARG2 (MLD_DBG_MAP, MLD_SEM_TRC, "MLD",
                                  "Changing MODE to MLD_NON_QUERIER_WAIT"
                                  "for interface %d & address %s\n",
                                  u4IfIndex,
                                  Ip6PrintAddr (&MLD_CACHE_MCASTADDR
                                                (u4Count)));
                MLD_CACHE_MODE (u4Count) = MLD_NON_QUERIER_WAIT;
            }

            else if ((MLD_CACHE_MODE (u4Count) == MLD_QUERIER)
                     && (u1Mode == MLD_NON_QUERIER))
            {
                MLD_MOD_TRC_ARG2 (MLD_DBG_MAP, MLD_SEM_TRC, "MLD",
                                  "Changing MODE to MLD_QUERIER_WAIT"
                                  "for interface %d & address %s\n", u4IfIndex,
                                  Ip6PrintAddr (&MLD_CACHE_MCASTADDR
                                                (u4Count)));
                MLD_CACHE_MODE (u4Count) = MLD_QUERIER_WAIT;
            }
        }
    }

    MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                 "Exiting MLDInterfaceModeChange:Successfully\n");
    return MLD_SUCCESS;
}

/****************************************************************************
 Function    :  MLDGetCacheEntry
 Input       :  Multicast address
                interface
 Output      :  None.
 Description :  This function returns the pointer to the cache entry if exists,
                else returns NULL.
 Returns     :  Pointer to the cache entry/NULL
****************************************************************************/
tMLDCacheEntry     *
MLDGetCacheEntry (tIp6Addr * Ip6Addr, UINT4 u4IfIndex)
{
    UINT4               u4Count;
    MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                 "Entering MLDGetCacheEntry\n");

    for (u4Count = MLD_ZERO; u4Count < MLD_MAX_CACHE_ENTRIES; u4Count++)
    {
        if (MLD_CACHE_MODE (u4Count) == MLD_INVALID)
        {
            continue;
        }

        if ((MLD_CACHE_IFINDEX (u4Count) == u4IfIndex) &&
            (!(MEMCMP
               (&MLD_CACHE_MCASTADDR (u4Count), Ip6Addr, sizeof (tIp6Addr)))))
        {
            MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                         "Exiting MLDGetCacheEntry:Successfully\n");
            return &gMLDCacheTable[u4Count];
        }
    }

    MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                 "Exiting MLDGetCacheEntry:Failure\n");
    return NULL;
}

/****************************************************************************
 Function    :  MLDAddCacheEntry
 Input       :  Multicast address
                interface
 Output      :  None.
 Description :  This function adds a new entry in the cache table if it does
                not exists. It returns the pointer to the entry added if 
                     success else returns NULL.
 Returns     :  Pointer to the cache entry/NULL
****************************************************************************/
tMLDCacheEntry     *
MLDAddCacheEntry (tIp6Addr * Ip6Addr, UINT4 u4IfIndex)
{
    UINT4               u4Count = MLD_ZERO;

    MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                 "Entering MLDAddCacheEntry\n");

    if (MLD_INTERFACE_MODE (u4IfIndex) == MLD_INVALID)
    {
        MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                     "Exiting  MLDAddCacheEntry: Failure\n");
        return NULL;
    }

    if (!IS_ADDR_MULTI (*Ip6Addr))
    {
        MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                     "Exiting  MLDAddCacheEntry: Failure\n");
        return NULL;
    }

    for (u4Count = MLD_ZERO; u4Count < MLD_MAX_CACHE_ENTRIES; u4Count++)
    {
        if (gMLDCacheTable[u4Count].u1Mode == MLD_INVALID)
        {
            break;
        }
    }

    if (u4Count == MLD_MAX_CACHE_ENTRIES)
    {
        MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                     "Exiting MLDAddCacheEntry:Failure\n");
        return NULL;
    }

    MEMCPY (&MLD_CACHE_MCASTADDR (u4Count), Ip6Addr, sizeof (tIp6Addr));
    MLD_CACHE_IFINDEX (u4Count) = u4IfIndex;

    MLD_MOD_TRC_ARG3 (MLD_DBG_MAP, MLD_SEM_TRC, "MLD",
                      "Changing Mode to %d for interface %d and address %s\n",
                      MLD_INTERFACE_MODE (u4IfIndex), u4IfIndex,
                      Ip6PrintAddr (Ip6Addr));
    MLD_CACHE_MODE (u4Count) = MLD_INTERFACE_MODE (u4IfIndex);

    MLD_CACHE_RS (u4Count) = MLD_RS_ACTIVE;

    MLD_INTERFACE_JOINS (MLD_CACHE_IFINDEX (u4Count))++;

    MLD_MOD_TRC_ARG2 (MLD_DBG_MAP, MLD_SEM_TRC, "MLD",
                      "Changing to MLD_NO_LISTENERS for interface"
                      "%d and address %s\n", u4IfIndex, Ip6PrintAddr (Ip6Addr));
    MLD_CACHE_STATE (u4Count) = MLD_NO_LISTENERS;

    MLD_CACHE_LASTLISTENERCOUNT (u4Count) = MLD_INTERFACE_ROBUSTVAR (u4IfIndex);

    MLD_CACHE_SELF (u4Count) = TRUE;

    MLD_CACHE_BUF (u4Count) = NULL;

    SET_ADDR_UNSPECIFIED (MLD_CACHE_LASTREPORTER (u4Count));

    MLDInitTmr (&MLD_CACHE_RXMTTMR (u4Count), MLD_REXMT_TMR, u4IfIndex,
                Ip6Addr);

    MLDInitTmr (&MLD_CACHE_LISTENERTMR (u4Count), MLD_LISTENER_TMR, u4IfIndex,
                Ip6Addr);

    OsixGetSysTime (&MLD_CACHE_CREATETIME (u4Count));

    MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                 "Exiting MLDAddCacheEntry:Successfully\n");
    return (&gMLDCacheTable[u4Count]);

}

/****************************************************************************
 Function    :  MLDGetInterfaceEntry
 Input       :  Interface                     
 Output      :  None.
 Description :  This function returns the pointer to the entry from the 
                Interface table if exists else returns NULL.
 Returns     :  Pointer to the interface entry/NULL
****************************************************************************/
tMLDInterfaceEntry *
MLDGetInterfaceEntry (UINT4 u4IfIndex)
{
    MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                 "Entering MLDGetInterfaceEntry\n");

    if (MLD_INTERFACE_MODE (u4IfIndex) == MLD_INVALID)
    {
        MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                     "Exiting MLDGetInterfaceEntry:Interface " " not UP\n");
        return NULL;
    }
    else
    {
        MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                     "Exiting MLDGetInterfaceEntry:Successfully\n");
        return &gMLDInterfaceTable[u4IfIndex];
    }
}

/****************************************************************************
 Function    :  MLDAddInterfaceEntry
 Input       :  Interface
 Output      :  None.
 Description :  This function adds an entry in the Interface table if not
                existing. It returns the pointer to the entry added if
                     success else returns NULL.
 Returns     :  Pointer to the Interface entry/NULL
****************************************************************************/
tMLDInterfaceEntry *
MLDAddInterfaceEntry (UINT4 u4IfIndex)
{
    tNetIpv6IfInfo      NetIpv6IfInfo;

    MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                 "Entering MLDAddInterfaceEntry\n");

    if (MLD_INTERFACE_MODE (u4IfIndex) != MLD_INVALID)
    {
        MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                     "Exiting MLDAddInterfaceEntry:Failure\n");
        return NULL;
    }

    if (NetIpv6GetIfInfo (u4IfIndex, &NetIpv6IfInfo) != NETIPV6_SUCCESS)
    {
        MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                     "Exiting MLDAddInterfaceEntry:Failure\n");
        return NULL;
    }

    MLD_INTERFACE_IFINDEX (u4IfIndex) = u4IfIndex;

    MLD_MOD_TRC_ARG1 (MLD_DBG_MAP, MLD_SEM_TRC, "MLD",
                      "Changing Mode to QUERIER for interface %d\n", u4IfIndex);
    MLD_INTERFACE_MODE (u4IfIndex) = MLD_QUERIER;

    MEMCPY (&MLD_INTERFACE_IFQUERIER (u4IfIndex), &NetIpv6IfInfo.Ip6Addr,
            sizeof (tIp6Addr));

    MLD_INTERFACE_ROBUSTVAR (u4IfIndex) = MLD_DEF_ROBUSTNESS;

    MLD_INTERFACE_STARTUPQUERYCOUNT (u4IfIndex) = MLD_DEF_ROBUSTNESS;

    MLD_INTERFACE_QUERYINTERVAL (u4IfIndex) = MLD_DEF_QUERY_INTERVAL;

    MLD_INTERFACE_GENQUERESPVAL (u4IfIndex) = MLD_DEF_QUE_RESP_INTERVAL;

    MLD_INTERFACE_LASTLISTENERVAL (u4IfIndex) = MLD_DEF_LAST_LISTENER_VAL;

    OsixGetSysTime (&MLD_INTERFACE_QUERIERUPTIME (u4IfIndex));

    MLDInitTmr (&MLD_INTERFACE_GENQUETMR (u4IfIndex), MLD_GEN_QUERY_TMR,
                u4IfIndex, NULL);

    MLDInitTmr (&MLD_INTERFACE_OTHERQUETMR (u4IfIndex), MLD_OTHER_QUERY_TMR,
                u4IfIndex, NULL);

    MLD_INTERFACE_JOINS (u4IfIndex) = MLD_ZERO;

    MLD_INTERFACE_BUF (u4IfIndex) = NULL;

    MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                 "Exiting MLDAddInterfaceEntry:Successfully\n");
    return &gMLDInterfaceTable[u4IfIndex];
}

/****************************************************************************
 Function    :  MLDDeleteInterfaceEntry
 Input       :  Interface
 Output      :  None.
 Description :  This function deletes an entry in the Interface table.
 Returns     :  None.
****************************************************************************/
VOID
MLDDeleteInterfaceEntry (UINT4 u4IfIndex)
{
    UINT4               u4Count = MLD_ZERO;
    MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                 "Entering MLDDeleteInterfaceEntry\n");

    if (MLD_INTERFACE_MODE (u4IfIndex) == MLD_INVALID)
    {
        MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                     "Exiting MLDDeleteInterfaceEntry:Failure\n");
        return;
    }
    MLDStopTmr (gMldTimerListId, &(MLD_INTERFACE_GENQUETMR (u4IfIndex).node));
    MLDStopTmr (gMldTimerListId, &(MLD_INTERFACE_OTHERQUETMR (u4IfIndex).node));

    if (gMLDInterfaceTable[u4IfIndex].pBuf)
        MLDRelBuf (gMLDInterfaceTable[u4IfIndex].pBuf, FALSE);
    if (gMLDInterfaceTable[u4IfIndex].pParams)
        MEM_FREE (gMLDInterfaceTable[u4IfIndex].pParams);
    MEMSET (&gMLDInterfaceTable[u4IfIndex], MLD_ZERO,
            sizeof (tMLDInterfaceEntry));

    MLD_INTERFACE_MODE (u4IfIndex) = MLD_INVALID;

    for (u4Count = MLD_ZERO; u4Count < MLD_MAX_CACHE_ENTRIES; u4Count++)
    {
        if (MLD_CACHE_IFINDEX (u4Count) == u4IfIndex)
        {
            MLDStopTmr (gMldTimerListId, &(MLD_CACHE_RXMTTMR (u4Count).node));
            MLDStopTmr (gMldTimerListId,
                        &(MLD_CACHE_LISTENERTMR (u4Count).node));

            if (gMLDCacheTable[u4Count].pBuf)
                MLDRelBuf (gMLDCacheTable[u4Count].pBuf, FALSE);
            if (gMLDCacheTable[u4Count].pParams)
                MEM_FREE (gMLDCacheTable[u4Count].pParams);
            MEMSET (&gMLDCacheTable[u4Count], MLD_ZERO,
                    sizeof (tMLDCacheEntry));
            MLD_CACHE_MODE (u4Count) = MLD_INVALID;
        }
    }
    MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                 "Exiting MLDDeleteInterfaceEntry:Successfully\n");
    return;

}

/****************************************************************************
 Function    :  GET_CACHE_INDEX
 Input       :  Interface
                Multicast address
 Output      :  None.
 Description :  This function finds an index to the array in CacheTable.
 Returns     :  None.
****************************************************************************/
INT4
GET_CACHE_INDEX (UINT4 u4IfIndex, tIp6Addr * McastAddr)
{
    UINT4               u4Count = MLD_ZERO;
    for (u4Count = MLD_ZERO; u4Count < MLD_MAX_CACHE_ENTRIES; u4Count++)
    {
        if ((gMLDCacheTable[u4Count].u4IfIndex == u4IfIndex)
            && (MEMCMP (&gMLDCacheTable[u4Count].McastAddr, McastAddr,
                        sizeof (tIp6Addr)) == MLD_ZERO))
        {
            return u4Count;
        }
    }
    return MLD_FAILURE;
}

/****************************************************************************
 Function    :  MLDHostMASQueryReceived
 Input       :  Pointer to the Host Entry
                Maximum Response Delay 
 Output      :  None.
 Description :  This function will be called from MLDProcessPkt() when a MLD
                M-A-S query is received. 
 Returns     :  MLD_SUCCESS/MLD_FAILURE
****************************************************************************/
VOID
MLDHostMASQueryReceived (tMLDHostEntry * pHostEntry, UINT2 u2MaxRespDelay)
{

    UINT1               u1State;
    UINT4               u4RemainingTime = MLD_ZERO;
    UINT4               u4Duration;

    MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                 "Entering MLDHostMASQueryReceived\n");

    u1State = pHostEntry->u1State;

    switch (u1State)
    {
        case MLD_NO_LISTENER:
            break;

        case MLD_DELAY_LISTENER:
            if (MLDGetRemainingTime (gMldTimerListId, &pHostEntry->tRepTmr.node,
                                     &u4RemainingTime) == TMR_FAILURE)
            {
                u4RemainingTime = MLD_ZERO;
            }

            if (u2MaxRespDelay < u4RemainingTime)
            {
                u4Duration = MLD_RANDOM (MLD_ZERO, u2MaxRespDelay);

                /* u2MaxRespDelay is in ms, so convert it to seconds 
                 * before starting the timer
                 * Check that u4Duration is not less the granurality
                 * of the timer. If it is less, start the timer
                 * for one tick.(minimum)
                 */

                if (u4Duration < (MLD_THOUSAND / MLD_SYS_TICKS))
                {
                    u4Duration = MLD_ONE;
                }
                else
                {
                    u4Duration = (u4Duration * MLD_SYS_TICKS) / MLD_THOUSAND;
                }

                MLD_MOD_TRC (MLD_DBG_MAP, MLD_OS_RES_TRC, "MLD",
                             "Stopping Report Delay Timer\n ");
                MLDStopTmr (gMldTimerListId, &(pHostEntry->tRepTmr.node));

                MLD_MOD_TRC (MLD_DBG_MAP, MLD_OS_RES_TRC, "MLD",
                             "Starting Report Delay Timer\n ");
                MLDStartTmr (gMldTimerListId, &(pHostEntry->tRepTmr.node),
                             u4Duration);
            }
            break;

        case MLD_IDLE_LISTENER:
            if (u2MaxRespDelay != MLD_ZERO)
            {
                u4Duration = MLD_RANDOM (MLD_ZERO, u2MaxRespDelay);

                /* u2MaxRespDelay is in ms, so convert it to seconds 
                 * before starting the timer
                 * Check that u4Duration is not less the granurality
                 * of the timer. If it is less, start the timer
                 * with one tick. (minimum granularity) 
                 */
                if (u4Duration < (MLD_THOUSAND / MLD_SYS_TICKS))
                {
                    u4Duration = MLD_ONE;
                }
                else
                {
                    u4Duration = (u4Duration * MLD_SYS_TICKS) / MLD_THOUSAND;
                }

                MLD_MOD_TRC (MLD_DBG_MAP, MLD_OS_RES_TRC, "MLD",
                             "Stopping Report Delay Timer\n ");
                MLDStopTmr (gMldTimerListId, &(pHostEntry->tRepTmr.node));

                MLD_MOD_TRC (MLD_DBG_MAP, MLD_OS_RES_TRC, "MLD",
                             "Starting Report Delay Timer\n ");
                MLDStartTmr (gMldTimerListId, &(pHostEntry->tRepTmr.node),
                             u4Duration);

            }
            else
            {
                u4Duration = 0;

                MLD_MOD_TRC (MLD_DBG_MAP, MLD_OS_RES_TRC, "MLD",
                             "Stopping the Report Timer \n");
                MLDStopTmr (gMldTimerListId, &(pHostEntry->tRepTmr.node));

                MLD_MOD_TRC (MLD_DBG_MAP, MLD_OS_RES_TRC, "MLD",
                             "Starting Report Delay Timer\n ");
                MLDStartTmr (gMldTimerListId, &(pHostEntry->tRepTmr.node),
                             u4Duration);

            }

            MLD_MOD_TRC_ARG2 (MLD_DBG_MAP, MLD_SEM_TRC, "MLD",
                              "Changing from MLD_IDLE_LISTENER to"
                              "MLD_DELAY_LISTENER for interface %d and"
                              "address %s\n",
                              pHostEntry->u4IfIndex,
                              Ip6PrintAddr (&pHostEntry->McastAddr));
            pHostEntry->u1State = MLD_DELAY_LISTENER;
            break;

        default:
            break;

    }

    MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                 "Exiting MLDHostMASQueryReceived\n");

    return;
}

/****************************************************************************
 Function    :  MLDHostGenQueryReceived
 Input       :  Pointer to the Host Entry
                Maximum Response Delay 
 Output      :  None.
 Description :  This function will be called from MLDProcessPkt() when a MLD
                M-A-S query is received. 
 Returns     :  MLD_SUCCESS/MLD_FAILURE
****************************************************************************/
VOID
MLDHostGenQueryReceived (UINT2 u2MaxRespDelay)
{

    UINT4               u4Count;

    MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                 "Entering MLDHostGenQueryReceived\n");

    for (u4Count = MLD_ZERO; u4Count < MLD_MAX_HOST_ENTRIES; u4Count++)
    {
        if (MLD_HOST_STATE (u4Count) != MLD_INVALID)
        {
            MLDHostMASQueryReceived (&gMLDHostTable[u4Count], u2MaxRespDelay);
        }
    }

    MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                 "Exiting MLDHostGenQueryReceived\n");
    return;
}

/****************************************************************************
 Function    :  MLDHostReportReceived
 Input       :  Pointer to the Host Entry
                Maximum Response Delay 
 Output      :  None.
 Description :  This function will be called from MLDProcessPkt() when a MLD
                M-A-S query is received. 
 Returns     :  MLD_SUCCESS/MLD_FAILURE
****************************************************************************/
VOID
MLDHostReportReceived (tMLDHostEntry * pHostEntry)
{

    UINT1               u1State;

    MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                 "Entering MLDHostReportReceived\n");

    u1State = pHostEntry->u1State;

    switch (u1State)
    {
        case MLD_NO_LISTENER:
        case MLD_IDLE_LISTENER:
            break;

        case MLD_DELAY_LISTENER:
            MLD_MOD_TRC (MLD_DBG_MAP, MLD_OS_RES_TRC, "MLD",
                         "Stoping Report Delay Timer\n ");
            MLDStopTmr (gMldTimerListId, &(pHostEntry->tRepTmr.node));

            pHostEntry->u1Flag = MLD_RESET;

            MLD_MOD_TRC_ARG2 (MLD_DBG_MAP, MLD_SEM_TRC, "MLD",
                              "Changing from MLD_DELAY_LISTENER to"
                              "MLD_IDLE_LISTENER for interface %d and"
                              "address %s\n",
                              pHostEntry->u4IfIndex,
                              Ip6PrintAddr (&pHostEntry->McastAddr));
            pHostEntry->u1State = MLD_IDLE_LISTENER;
            break;

        default:
            break;

    }

    MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                 "Exiting MLDHostReportReceived\n");
    return;
}

/****************************************************************************
 Function    :  MLDAddHostEntry
 Input       :  Multicast address
                interface
 Output      :  None.
 Description :  This function adds a new entry in the host table if it does
                not exists. It returns the pointer to the entry added if 
                success else returns NULL.
 Returns     :  Pointer to the host entry/NULL
****************************************************************************/
tMLDHostEntry      *
MLDAddHostEntry (tIp6Addr * Ip6Addr, UINT4 u4IfIndex)
{
    UINT4               u4Count = MLD_ZERO;
    tMLDInterfaceEntry *pIfEntry = NULL;

    MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                 "Entering MLDAddHostEntry\n");

    if (!IS_ADDR_MULTI (*Ip6Addr))
    {
        MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                     "Exiting  MLDAddHostEntry: Ip6 address should be"
                     "multicast address\n");
        return NULL;
    }

    pIfEntry = MLDGetInterfaceEntry (u4IfIndex);
    if (pIfEntry == NULL)
    {
        MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                     "Exiting  MLDAddHostEntry: MLD not enabled"
                     "on the interface\n");
        return NULL;
    }

    for (u4Count = MLD_ZERO; u4Count < MLD_MAX_HOST_ENTRIES; u4Count++)
    {
        if (MLD_HOST_STATE (u4Count) == MLD_INVALID)
        {
            break;
        }
    }

    if (u4Count == MLD_MAX_HOST_ENTRIES)
    {
        MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                     "Exiting MLDAddHostEntry: Failure\n");
        return NULL;
    }

    MLD_HOST_STATE (u4Count) = MLD_NO_LISTENER;

    MLD_HOST_IFINDEX (u4Count) = u4IfIndex;

    MEMCPY (&MLD_HOST_MCASTADDR (u4Count), Ip6Addr, sizeof (tIp6Addr));

    MLDInitTmr (&MLD_HOST_REPORTTIMER (u4Count), MLD_REPORT_TMR, u4IfIndex,
                Ip6Addr);

    MLD_HOST_USAGECOUNT (u4Count) = MLD_ZERO;

    MLD_HOST_FLAG (u4Count) = MLD_RESET;

    MLD_HOST_BUF (u4Count) = NULL;

    MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                 "Exiting MLDAddHostEntry\n");

    return &gMLDHostTable[u4Count];
}

/****************************************************************************
 Function    :  MLDGetHostEntry
 Input       :  Multicast address
                interface
 Output      :  None.
 Description :  This function returns the pointer to the cache entry if exists,
                else returns NULL.
 Returns     :  Pointer to the cache entry/NULL
****************************************************************************/
tMLDHostEntry      *
MLDGetHostEntry (UINT4 u4IfIndex, tIp6Addr * Ip6Addr)
{
    UINT4               u4Count;

    MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                 "Entering MLDGetHostEntry\n");

    for (u4Count = MLD_ZERO; u4Count < MLD_MAX_HOST_ENTRIES; u4Count++)
    {
        if (MLD_HOST_STATE (u4Count) == MLD_INVALID)
        {
            continue;
        }

        if ((MLD_HOST_IFINDEX (u4Count) == u4IfIndex) &&
            (!(MEMCMP
               (&MLD_HOST_MCASTADDR (u4Count), Ip6Addr, sizeof (tIp6Addr)))))
        {
            MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                         "Exiting MLDGetHostEntry:Successfully\n");
            return &gMLDHostTable[u4Count];
        }
    }

    MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                 "Exiting MLDGetHostEntry: Failure\n");
    return NULL;
}

/****************************************************************************
 Function    :  MLDIsCacheEntryPresent
 Input       :  Multicast address
                interface
 Output      :  None.
 Description :  This function returns the TRUE based on if the  cache entry 
                exists, else returns FALSE.
 Returns     :  TRUE/FALSE 
****************************************************************************/
INT1
MLDIsCacheEntryPresent (tIp6Addr * Ip6Addr, UINT4 u4IfIndex)
{

    if (MLDGetCacheEntry (Ip6Addr, u4IfIndex) == NULL)
        return FALSE;
    else
        return TRUE;
}
