
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: mldmain.c,v 1.4 2010/11/18 14:05:35 siva Exp $
 *
 * Description:
 *
 *******************************************************************/
#include "mldinc.h"
#ifndef LNXIP6_WANTED
#include "ip6proto.h"
#endif

 /* GLOBAL DECLARATIONS */
tTimerListId        gMldTimerListId;
tMLDInterfaceEntry *gMLDInterfaceTable = NULL;
tMLDCacheEntry     *gMLDCacheTable = NULL;
tMLDHostEntry      *gMLDHostTable = NULL;
tMLDHlReg          *gMLDHlReg = NULL;
UINT4               gu4MldDbgMap = 0;
tMldSystemSize      gMldSizingParams;
UINT4               gu4MLDGlobalStatus = MLD_ROUTER;

#ifdef SNMP_WANTED
#include "include.h"
#include "stdmlmdb.h"
#include "fsmldmdb.h"
INT4 RegisterStdMLDwithFutureSNMP PROTO ((void));
INT4 RegisterFSMLDwithFutureSNMP PROTO ((void));
#endif /* SNMP_WANTED */

#ifdef SNMP_2_WANTED
#include "stdmldwr.h"
#include "fsmldwr.h"
#endif

#ifdef IP6_WANTED
VOID                MldIfRegFn (tNetIpv6HliParams * pNetIpv6HlParams);
VOID                MldAddrRegFn (tNetIpv6HliParams * pNetIpv6HlParams);
#endif

VOID                MLDFreeTables (void);

/****************************************************************************
 Function    :  MLDInit
 Input       :  None.
 Output      :  None.
 Description :  This function creates the MLD Task.
 Returns     :  MLD_SUCCESS/MLD_FAILURE
****************************************************************************/
INT1
MLDInit ()
{
    tOsixTaskId         gMldTaskId;

    MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC | MLD_INIT_SHUT_TRC, "MLD",
                 "Entering MLDInit\n");

    GetMldSizingParams (&gMldSizingParams);
    if (MLDCreateTask (MLD_TASK_NAME, MLD_TASK_PRIORITY,
                       MLD_TASK_STACK_SIZE, MLD_TASK_MAIN_FUNCTION,
                       MLD_TASK_START_ARGS, MLD_TASK_MODE,
                       &gMldTaskId) != OSIX_SUCCESS)
    {
        MLD_MOD_TRC (MLD_DBG_MAP, MLD_ALL_FAIL_TRC |
                     MLD_ENTRY_EXIT_TRC | MLD_OS_RES_TRC, "MLD",
                     "Exiting MLDInit:Task creation Failed\n ");
        return MLD_FAILURE;
    }

    MLD_MOD_TRC (MLD_DBG_MAP, MLD_OS_RES_TRC, "MLD",
                 "MLD Task Created Successfully\n");

    MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                 "Exiting MLDInit Successfully\n");
    return MLD_SUCCESS;
}

#ifdef IP6_WANTED
/****************************************************************************
 Function    :  MldIfRegFn
 Input       :  pIfInfo
 Output      :  None.
 Returns     :  None.
 Description :  Receives the Interface status change event
****************************************************************************/
VOID
MldIfRegFn (tNetIpv6HliParams * pIfInfo)
{
    UINT4               u4Status = 0;
    UINT4               u4Mask = 0;

    u4Mask = pIfInfo->unIpv6HlCmdType.IfStatChange.u4Mask;

    /* Only Interface Down/Delete events are processed. Other events are
     * not processed. */
    if ((u4Mask == NETIPV6_INTERFACE_STATUS_CHANGE) &&
        (pIfInfo->unIpv6HlCmdType.IfStatChange.u4IfStat == NETIPV6_IF_DELETE))
    {
        u4Status = IF_DELETE;
    }
    else if ((u4Mask == NETIPV6_INTERFACE_STATUS_CHANGE) &&
             (pIfInfo->unIpv6HlCmdType.IfStatChange.u4OperStatus ==
              NETIPV6_IF_DOWN))
    {
        u4Status = IF_DOWN;
    }
    else
    {
        return;
    }

    MLDIfUpdate (pIfInfo->unIpv6HlCmdType.IfStatChange.u4Index,
                 (UINT2) u4Status);
    return;
}

/****************************************************************************
 Function    :  MldAddrRegFn
 Input       :  pIfInfo
 Output      :  None.
 Returns     :  None.
 Description :  Receives the Address status change event
****************************************************************************/
VOID
MldAddrRegFn (tNetIpv6HliParams * pIfInfo)
{
    tMLDInterfaceEntry *pIfEntry = NULL;
    UINT4               u4IfIndex = 0;
    UINT4               u4Type = 0;
    UINT4               u4Duration = 0;

    u4Type = pIfInfo->unIpv6HlCmdType.AddrChange.Ipv6AddrInfo.u4Type;
    if (u4Type != ADDR6_LLOCAL)
        return;
    u4IfIndex = pIfInfo->unIpv6HlCmdType.AddrChange.u4Index;
    pIfEntry = MLDGetInterfaceEntry (u4IfIndex);
    if (pIfEntry == NULL)
    {
        return;
    }
    MEMCPY (&(pIfEntry->IfQuerier),
            &(pIfInfo->unIpv6HlCmdType.AddrChange.Ipv6AddrInfo.Ip6Addr),
            sizeof (tIp6Addr));

    OsixGetSysTime (&MLD_INTERFACE_QUERIERUPTIME (u4IfIndex));

    MLDInitTmr (&MLD_INTERFACE_GENQUETMR (u4IfIndex), MLD_GEN_QUERY_TMR,
                u4IfIndex, NULL);

    MLDInitTmr (&MLD_INTERFACE_OTHERQUETMR (u4IfIndex), MLD_OTHER_QUERY_TMR,
                u4IfIndex, NULL);
    if ((gu4MLDGlobalStatus & MLD_ROUTER) == MLD_ROUTER)
    {
        u4Duration = MLD_INTERFACE_QUERYINTERVAL (u4IfIndex) / 4;

        MLD_MOD_TRC_ARG1 (MLD_DBG_MAP, MLD_OS_RES_TRC, "MLD",
                          "Starting Startup Timer for Interface:%d\n",
                          u4IfIndex);
        MLDStartTmr (gMldTimerListId,
                     &((MLD_INTERFACE_GENQUETMR
                        (u4IfIndex)).node), u4Duration * MLD_SYS_TICKS);

        MLD_INTERFACE_STARTUPQUERYCOUNT (u4IfIndex)--;
        MLDSendQuery (u4IfIndex, NULL);
        MLD_INTERFACE_RS (u4IfIndex) = MLD_RS_ACTIVE;
    }

    return;
}
#endif
/****************************************************************************
 Function    :  MLDProtoInit
 Input       :  None.
 Output      :  None.
 Description :  This function initializes all the tables(interface & cache).
                Creates the queue and timer.
 Returns     :  MLD_SUCCESS/MLD_FAILURE
****************************************************************************/
INT1
MLDProtoInit ()
{

    tOsixQId            gIp6MldQId;
    UINT2               u2Count;
    MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC | MLD_INIT_SHUT_TRC, "MLD",
                 "Entering MLDProtoInit\n");

    if (MLDCreateQ (IP6_TO_MLD_Q_NAME, IP6_TO_MLD_Q_DEPTH,
                    IP6_TO_MLD_Q_MODE, &gIp6MldQId) != OSIX_SUCCESS)
    {
        MLD_MOD_TRC (MLD_DBG_MAP, MLD_ALL_FAIL_TRC | MLD_ENTRY_EXIT_TRC, "MLD",
                     "Exiting MLDProtoInit:Queue creation Failed \n");
        return MLD_FAILURE;
    }

    MLD_MOD_TRC (MLD_DBG_MAP, MLD_OS_RES_TRC, "MLD",
                 "Creation of Queue Successful \n");

    if (MLDCreateTimerList (MLD_TASK_NAME, MLD_TIMER_EVENT,
                            NULL, &gMldTimerListId) != OSIX_SUCCESS)
    {
        MLD_MOD_TRC (MLD_DBG_MAP, MLD_ALL_FAIL_TRC | MLD_ENTRY_EXIT_TRC, "MLD",
                     "Exiting MLDProtoInit:Tmr Creation Failed\n ");
        return MLD_FAILURE;
    }

    MLD_MOD_TRC (MLD_DBG_MAP, MLD_OS_RES_TRC, "MLD",
                 "Creation of Timer Successful \n");

    GetMldSizingParams (&gMldSizingParams);

    gMLDCacheTable =
        MEM_MALLOC (sizeof (tMLDCacheEntry) * MLD_MAX_CACHE_ENTRIES,
                    tMLDCacheEntry);

    gMLDInterfaceTable =
        MEM_MALLOC (sizeof (tMLDInterfaceEntry) * MLD_MAX_IF_ENTRIES,
                    tMLDInterfaceEntry);

    gMLDHostTable =
        MEM_MALLOC (sizeof (tMLDHostEntry) * MLD_MAX_HOST_ENTRIES,
                    tMLDHostEntry);

    gMLDHlReg =
        MEM_MALLOC (sizeof (tMLDHlReg) * MLD_MAX_ROUTING_PROTOCOLS, tMLDHlReg);

    if ((gMLDCacheTable == NULL) || (gMLDInterfaceTable == NULL) ||
        (gMLDHostTable == NULL) || (gMLDHlReg == NULL))
    {
        MLD_MOD_TRC (MLD_DBG_MAP, MLD_ALL_FAIL_TRC | MLD_ENTRY_EXIT_TRC,
                     "MLD",
                     "Exiting MLDProtoInit:Memory allocation Failed!!\n ");
        MLDFreeTables ();
        return MLD_FAILURE;
    }

    MEMSET (gMLDCacheTable, MLD_ZERO,
            (sizeof (tMLDCacheEntry) * MLD_MAX_CACHE_ENTRIES));

    MEMSET (gMLDInterfaceTable, MLD_ZERO,
            (sizeof (tMLDInterfaceEntry) * MLD_MAX_IF_ENTRIES));

    MEMSET (gMLDHostTable, MLD_ZERO,
            (sizeof (tMLDHostEntry) * MLD_MAX_HOST_ENTRIES));

    for (u2Count = MLD_ZERO; u2Count < MLD_MAX_ROUTING_PROTOCOLS; u2Count++)
    {
        gMLDHlReg[u2Count].u2Protocol = MLD_ZERO;
        gMLDHlReg[u2Count].fnptr = NULL;
    }

#ifdef IP6_WANTED

    /* MLD Registartion for MLD Packets receiving */
    if (NetIpv6RegisterHigherLayerProtocol (MLD_PROTOCOL,
                                            NETIPV6_APPLICATION_RECEIVE,
                                            (VOID *) MLDInput)
        == NETIPV6_FAILURE)
    {
        MLD_MOD_TRC (MLD_DBG_MAP, MLD_ALL_FAIL_TRC | MLD_ENTRY_EXIT_TRC, "MLD",
                     "Exiting MLDProtoInit:If Status change Registration "
                     "Failed\n ");
        MLDFreeTables ();
        return MLD_FAILURE;
    }

    /* MLD Registration for Interface Status Change. */
    if (NetIpv6RegisterHigherLayerProtocol (MLD_PROTOCOL,
                                            NETIPV6_INTERFACE_PARAMETER_CHANGE,
                                            (VOID *) MldIfRegFn) ==
        NETIPV6_FAILURE)
    {
        MLD_MOD_TRC (MLD_DBG_MAP, MLD_ALL_FAIL_TRC | MLD_ENTRY_EXIT_TRC, "MLD",
                     "Exiting MLDProtoInit:If Status change Registration "
                     "Failed\n ");
        MLDFreeTables ();
        return MLD_FAILURE;
    }
    /* MLD Registration for Address Status Change. */
    if (NetIpv6RegisterHigherLayerProtocol (MLD_PROTOCOL,
                                            NETIPV6_ADDRESS_CHANGE,
                                            (VOID *) MldAddrRegFn) ==
        NETIPV6_FAILURE)
    {
        MLD_MOD_TRC (MLD_DBG_MAP, MLD_ALL_FAIL_TRC | MLD_ENTRY_EXIT_TRC, "MLD",
                     "Exiting MLDProtoInit:Address Status change Registration "
                     "Failed\n ");
        MLDFreeTables ();
        return MLD_FAILURE;
    }

#endif

#ifdef LNXIP6_WANTED
    if (MLDCreateSocket () == MLD_FAILURE)
    {
        MLD_MOD_TRC (MLD_DBG_MAP, MLD_ALL_FAIL_TRC | MLD_ENTRY_EXIT_TRC, "MLD",
                     "Exiting MLDProtoInit: Mld Socket Options Creation "
                     "Failed\n ");
        MLDFreeTables ();
        return MLD_FAILURE;
    }

    if (MLDSetSendSockOptions () == MLD_FAILURE)
    {
        MLD_MOD_TRC (MLD_DBG_MAP, MLD_ALL_FAIL_TRC | MLD_ENTRY_EXIT_TRC, "MLD",
                     "Exiting MLDProtoInit:Mld Socket Options Settings "
                     "Failed\n ");
        MLDFreeTables ();
        return MLD_FAILURE;
    }

#endif

#ifdef SNMP_2_WANTED
    RegisterSTDMLD ();
    RegisterFSMLD ();
#endif
#ifdef NPAPI_WANTED
    FsMldHwEnableMld ();
#endif
    MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                 "Exiting MLDProtoInit Successfully\n");
    return MLD_SUCCESS;

}

/****************************************************************************
 Function    :  MLDFreeTables
 Input       :  None.
 Output      :  None.
 Description :  This function frees the global Tables allocated.
 Returns     :  None.                    
****************************************************************************/
VOID
MLDFreeTables ()
{
    if (gMLDCacheTable)
        MEM_FREE (gMLDCacheTable);
    if (gMLDInterfaceTable)
        MEM_FREE (gMLDInterfaceTable);
    if (gMLDHostTable)
        MEM_FREE (gMLDHostTable);
    if (gMLDHlReg)
        MEM_FREE (gMLDHlReg);

    return;
}

/****************************************************************************
 Function    :  MLDShutdown
 Input       :  None.
 Output      :  None.
 Description :  This function shutdown all the tables(interface & cache).
 Returns     :  None.                    
****************************************************************************/
VOID
MLDShutdown ()
{

    MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC | MLD_INIT_SHUT_TRC, "MLD",
                 "Entering MLDShutdown\n");

#ifdef LNXIP6_WANTED
    MLDCloseSocket ();
#endif

    if (MLDDeleteQ (MLD_ZERO, IP6_TO_MLD_Q_NAME) != OSIX_SUCCESS)
    {
        MLD_MOD_TRC (MLD_DBG_MAP, MLD_ALL_FAIL_TRC | MLD_ENTRY_EXIT_TRC, "MLD",
                     "Exiting MLDShutdown:Queue deletion Failed \n");
        return;
    }

    MLD_MOD_TRC (MLD_DBG_MAP, MLD_OS_RES_TRC, "MLD",
                 "Deletion of Queue Successful \n");

    if (MLDDeleteTimerList (gMldTimerListId) != OSIX_SUCCESS)
    {
        MLD_MOD_TRC (MLD_DBG_MAP, MLD_ALL_FAIL_TRC | MLD_ENTRY_EXIT_TRC, "MLD",
                     "Exiting MLDShutdown:Tmr Deletion Failed\n ");
        return;
    }

    MLD_MOD_TRC (MLD_DBG_MAP, MLD_OS_RES_TRC, "MLD",
                 "Deletion of Timer Successful \n");

#ifdef IP6_WANTED
    NetIpv6DeRegisterHigherLayerProtocol (MLD_PROTOCOL);

    MLD_MOD_TRC (MLD_DBG_MAP, MLD_OS_RES_TRC, "MLD",
                 "Successfully Deregistered with IP6 \n");
#endif
    MEM_FREE (gMLDCacheTable);
    MEM_FREE (gMLDInterfaceTable);
    MEM_FREE (gMLDHostTable);
    MEM_FREE (gMLDHlReg);

    gMLDCacheTable = NULL;
    gMLDInterfaceTable = NULL;
    gMLDHostTable = NULL;
    gMLDHlReg = NULL;

#ifdef NPAPI_WANTED
    FsMldHwDisableMld ();
#endif
    MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                 "Exiting MLDShutdown Successfully\n");
    MLDDeleteTask (MLD_ZERO, MLD_TASK_NAME);
    return;

}

/****************************************************************************
 Function    :  MLDTaskMain
 Input       :  None.
 Output      :  None.
 Description :  This is the entry point function of MLD Task. It calls 
                MLDProtoInit() to initialize and then waits for events
                     from IP6/TMR.
 Returns     :  None.
****************************************************************************/
VOID
MLDTaskMain (INT1 *pDummy)
{
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    UINT4               u4Event = MLD_ZERO;

    UNUSED_PARAM (pDummy);
    MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                 "Entering MLDTaskMain\n");

    if (MLDProtoInit () == MLD_FAILURE)
    {
        MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                     "Exiting MLDTaskMain:Failure in Initialization\n");
#ifdef ISS_WANTED
        MLD_INIT_COMPLETE (OSIX_FAILURE);
#endif
        return;
    }

#ifdef ISS_WANTED
    MLD_INIT_COMPLETE (OSIX_SUCCESS);
#endif

    for (;;)
    {

        MLDReceiveEvt ((MLD_PKT_RECD | MLD_TIMER_EVENT),
                       OSIX_WAIT, MLD_ZERO, &u4Event);

        switch (u4Event)
        {
            case MLD_PKT_RECD:
                MLD_MOD_TRC (MLD_DBG_MAP, MLD_OS_RES_TRC,
                             "MLD", "Received pkt from IP6\n");

                while (MLDRcvFromQ (IP6_TO_MLD_Q_NODE_ID,
                                    IP6_TO_MLD_Q_NAME,
                                    OSIX_NO_WAIT,
                                    IP6_TO_MLD_Q_TIMEOUT,
                                    &pBuf) == OSIX_SUCCESS)
                {
                    MLDProcessPkt (pBuf);
                }
                break;

            case MLD_TIMER_EVENT:
                MLD_MOD_TRC (MLD_DBG_MAP, MLD_OS_RES_TRC,
                             "MLD", "Timer expired\n");
                MLDProcessTmrEvt ();
                break;

            default:
                break;

        }                        /*end of switch case */

    }                            /*end of while */
}

/****************************************************************************
 Function    :  MLDHlReg
 Input       :  Protocol Id
                Function pointer
 Output      :  None.
 Description :  This function updates the call back fn. registered by 
                Routing protocols.
 Returns     :  MLD_SUCCESS/MLD_FAILURE.
****************************************************************************/
INT1
MLDHlReg (UINT2 u2Proto, MLDHlRegFn fnptr)
{
    UINT2               u2Count = MLD_ZERO;
    UINT4               u4Index = MLD_ZERO;

    MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD", "Entering MLDHlReg\n");

    if (fnptr == NULL)
    {
        MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                     "Exiting MLDHlReg:Failure\n");
        return MLD_FAILURE;
    }

    for (u2Count = MLD_ZERO; u2Count < MLD_MAX_ROUTING_PROTOCOLS; u2Count++)
    {
        if (gMLDHlReg[u2Count].fnptr == NULL)
        {
            break;
        }
    }

    if (u2Count == MLD_MAX_ROUTING_PROTOCOLS)
    {
        MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                     "Exiting MLDHlReg:Failure\n");
        return MLD_FAILURE;
    }

    gMLDHlReg[u2Count].u2Protocol = u2Proto;
    gMLDHlReg[u2Count].fnptr = fnptr;

    for (u4Index = MLD_ZERO; u4Index < MLD_MAX_CACHE_ENTRIES; u4Index++)
    {
        if (MLD_CACHE_MODE (u4Index) == MLD_INVALID)
            continue;
        gMLDHlReg[u2Count].fnptr (MLD_ADD, MLD_CACHE_MCASTADDR (u4Index),
                                  MLD_CACHE_IFINDEX (u4Index));
    }

    MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                 "Exiting MLDHlReg:Successfully\n");
    return MLD_SUCCESS;
}

/****************************************************************************
 Function    :  MLDHlDeReg
 Input       :  Protocol Id
 Output      :  None.
 Description :  This function is used to deregistered some call back funtion
                already registered by Routing Protocols.
 Returns     :  MLD_SUCCESS/MLD_FAILURE.
****************************************************************************/
INT1
MLDHlDeReg (UINT2 u2Proto)
{
    UINT2               u2Count;

    MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                 "Entering MLDHlDeReg\n");

    for (u2Count = MLD_ZERO; u2Count < MLD_MAX_ROUTING_PROTOCOLS; u2Count++)
    {
        if (gMLDHlReg[u2Count].u2Protocol == u2Proto)
        {
            gMLDHlReg[u2Count].u2Protocol = MLD_ZERO;
            gMLDHlReg[u2Count].fnptr = NULL;
            MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                         "Exiting MLDHlDeReg:Successfully\n");
            return MLD_SUCCESS;
        }
    }

    MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                 "Exiting MLDHlDeReg:Failure\n");
    return MLD_FAILURE;
}

/****************************************************************************
 Function    :  MLDStartListening
 Input       :  u4IfIndex
                Multicast address
 Output      :  None.
 Description :  This function is called by any application when they want to
                join some multicast group.
 Returns     :  MLD_SUCCESS/MLD_FAILURE.
****************************************************************************/
INT4
MLDStartListening (UINT4 u4IfIndex, tIp6Addr * McastAddr)
{
    tMLDHostEntry      *pHostEntry = NULL;

    MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                 "Entering MLDStartListening\n");

    if ((gu4MLDGlobalStatus & MLD_HOST) != MLD_HOST)
    {
        MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                     "Exiting MLDStartListening: Failure Host not enabled\n");
        return MLD_FAILURE;
    }

    pHostEntry = MLDGetHostEntry (u4IfIndex, McastAddr);
    if (pHostEntry != NULL)
    {
        /* Entry already has been added 
         * so just increment the usage count
         */
        pHostEntry->u2UsageCount++;
        MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                     "Exiting MLDStartListening: Successfully\n");
        return MLD_SUCCESS;
    }
#ifdef IP6_WANTED
    if (NetIpv6McastJoin (u4IfIndex, McastAddr) == NETIPV6_FAILURE)
    {
        MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                     "Exiting MLDStartListening: Failure in "
                     "adding multicast address at IP6 level\n");
        return MLD_FAILURE;
    }
#endif

    pHostEntry = MLDAddHostEntry (McastAddr, u4IfIndex);
    if (pHostEntry == NULL)
    {
        MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                     "Exiting MLDStartListening: Failure in " "adding entry\n");
        return MLD_FAILURE;
    }

    MLD_MOD_TRC (MLD_DBG_MAP, MLD_SEM_TRC, "MLD", "Sending Report packet\n");
    MLDSendReport (pHostEntry);
    pHostEntry->u1Flag = MLD_SET;
    pHostEntry->u1State = MLD_DELAY_LISTENER;
    pHostEntry->u2UsageCount++;

    MLD_MOD_TRC (MLD_DBG_MAP, MLD_OS_RES_TRC, "MLD",
                 "Starting Report Delay Timer with"
                 "Unsolicited report interval\n ");
    MLDStartTmr (gMldTimerListId, &((pHostEntry->tRepTmr).node),
                 MLD_UNSOLICITED_REPORT_INTERVAL * MLD_SYS_TICKS);

    MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                 "Exiting MLDStartListening: Successfully\n");
    return MLD_SUCCESS;
}

/****************************************************************************
 Function    :  MLDStopListening
 Input       :  u4IfIndex
                Multicast address
 Output      :  None.
 Description :  This function is called by any application when they want to
                leave some multicast group.
 Returns     :  MLD_SUCCESS/MLD_FAILURE.
****************************************************************************/
INT4
MLDStopListening (UINT4 u4IfIndex, tIp6Addr * McastAddr)
{
    tMLDHostEntry      *pHostEntry = NULL;

    MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                 "Entering MLDStopListening\n");

    pHostEntry = MLDGetHostEntry (u4IfIndex, McastAddr);
    if (pHostEntry == NULL)
    {
        MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                     "Exiting MLDStopListening: Failure Could not"
                     "find entry\n");
        return MLD_FAILURE;
    }

    if (pHostEntry->u2UsageCount != MLD_ONE)
    {
        /* Some other application is listening to the same group */
        pHostEntry->u2UsageCount--;
        MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                     "Exiting MLDStopListening: Successfully\n");
        return MLD_SUCCESS;
    }
#ifdef IP6_WANTED
    if (NetIpv6McastLeave (u4IfIndex, McastAddr) == NETIPV6_FAILURE)
    {
        MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                     "Exiting MLDStopListening: Failure in"
                     "deleting multicast address at IP6 level\n");
        return MLD_FAILURE;
    }
#endif

    if (pHostEntry->u1Flag == MLD_SET)
    {
        MLD_MOD_TRC (MLD_DBG_MAP, MLD_SEM_TRC, "MLD", "Sending Done packet\n");
        MLDSendDone (pHostEntry);
    }

    MLD_MOD_TRC (MLD_DBG_MAP, MLD_OS_RES_TRC, "MLD", "Releasing memory!!!\n");

    MLD_MOD_TRC (MLD_DBG_MAP, MLD_OS_RES_TRC, "MLD",
                 "Stopping the Report Timer \n");
    MLDStopTmr (gMldTimerListId, &(pHostEntry->tRepTmr.node));
    if (pHostEntry->pBuf)
        MEM_FREE (pHostEntry->pBuf);
    if (pHostEntry->pParams)
        MEM_FREE (pHostEntry->pParams);

    MEMSET (pHostEntry, MLD_ZERO, sizeof (tMLDHostEntry));

    MLD_MOD_TRC (MLD_DBG_MAP, MLD_ENTRY_EXIT_TRC, "MLD",
                 "Exiting MLDStopListening:Successfully\n");
    return MLD_SUCCESS;
}

#ifdef SNMP_WANTED

INT4
RegisterStdMLDwithFutureSNMP ()
{
    if (SNMP_AGT_RegisterMib
        ((tSNMP_GroupOIDType *) & stdmld_FMAS_GroupOIDTable,
         (tSNMP_BaseOIDType *) & stdmld_FMAS_BaseOIDTable,
         (tSNMP_MIBObjectDescrType *) & stdmld_FMAS_MIBObjectTable,
         stdmld_FMAS_Global_data, (INT4) stdmld_MAX_OBJECTS) != SNMP_SUCCESS)
    {
        return FAILURE;
    }
    return SUCCESS;
}

INT4
RegisterFSMLDwithFutureSNMP ()
{
    if (SNMP_AGT_RegisterMib ((tSNMP_GroupOIDType *) & fsmld_FMAS_GroupOIDTable,
                              (tSNMP_BaseOIDType *) & fsmld_FMAS_BaseOIDTable,
                              (tSNMP_MIBObjectDescrType *) &
                              fsmld_FMAS_MIBObjectTable,
                              fsmld_FMAS_Global_data,
                              (INT4) fsmld_MAX_OBJECTS) != SNMP_SUCCESS)
    {
        return FAILURE;
    }
    return SUCCESS;
}

#endif /* SNMP_WANTED */
