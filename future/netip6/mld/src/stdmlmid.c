# include  "include.h"
# include  "stdmlmid.h"
# include  "stdmllow.h"
# include  "stdmlcon.h"
# include  "stdmlogi.h"
# include  "extern.h"
# include  "midconst.h"
# include  "stdmldcli.h"

/****************************************************************************
 Function   : mldInterfaceEntryGet
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db       : The OID as formed by the SNMP Agent
                              from the static MIB database.
              p_incoming    : The OID as it is send by the manager
                              in the SNMP PDU.
              u1_arg        : The position of the variable in the MIB group.
              u1_search_type: Indicates whether this routine is called
                              as a GET or GETNEXT.
 Output     : The actual OID for which the operation is performed is
              returned in p_in_db.
 Returns    : Returns a pointer to tSNMP_VAR_BIND structure, with either
              the corresponding value, or NULL if the GET/GETNEXT
              operation could not be performed successfully.
****************************************************************************/

/* Prototype declarations for get_routine. */
tSNMP_VAR_BIND     *
mldInterfaceEntryGet (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                      UINT1 u1_arg, UINT1 u1_search_type)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for get routine . */
    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_partial_index_len = FALSE;
    INT4                i4_partial_index_flag = TRUE;
    INT4                i4_size_offset = FALSE;
    INT4                LEN_mldInterfaceTable_INDEX;

    /*  The Declaration of the Vars which are passed to FormVarbind Fn. */
    INT4                i4_return_val = FALSE;
    UINT4               u4_counter_val = FALSE;
    tSNMP_COUNTER64_TYPE u8_counter_val = { 0, 0 };
    INT2                i2_type;

    /*
     *  The Declaration of the Octet String Array which is used for
     *  conversion and The Pointer of tSNMP_OCTET_STRING_TYPE.
     */
    tSNMP_OCTET_STRING_TYPE *poctet_string = NULL;
    tSNMP_OID_TYPE     *pOidValue = NULL;

    /*
     *  These Variable are declared for being used in the
     *  FOR Loop for extracting Indices from the given OID.
     */
    INT4                i4_mldInterfaceIfIndex = FALSE;
    INT4                i4_next_mldInterfaceIfIndex = FALSE;

    tSNMP_OCTET_STRING_TYPE *poctet_retval_mldInterfaceQuerier = NULL;

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    switch (u1_search_type)
    {

        case SNMP_SEARCH_TYPE_EXACT:
        {
            /*
             *  Initializing the i4_size_offset Variable to the Length of the
             *  OID of the Object i.e the Length present in the p_in_db.
             */
            i4_size_offset = p_in_db->u4_Length;
            i4_size_offset += INTEGER_LEN;

            if (p_incoming->u4_Length != (UINT4) i4_size_offset)
            {
                return (NULL);
            }
            /*
             *  Finding the length of the Index for extracting
             *  Index from the structure given by the Manager.
             */
            /*
             *  If LEN_OF_VARIABLE_LEN_INDEX is Present refer Header File
             *  where it is Defined for Explanation of the Variable.
             */
            LEN_mldInterfaceTable_INDEX = p_in_db->u4_Length + INTEGER_LEN;

            if (LEN_mldInterfaceTable_INDEX == (INT4) p_incoming->u4_Length)
            {
                /* Extracting The Integer Index. */
                i4_mldInterfaceIfIndex =
                    (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                   i4_offset];
                i4_offset++;

                /*  Low Level Routine Which Validates the Indices. */
                if ((i1_ret_val =
                     nmhValidateIndexInstanceMldInterfaceTable
                     (i4_mldInterfaceIfIndex)) != SNMP_SUCCESS)
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
                /*  Storing the Extracted Index in p_in_db. */
                p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                    (UINT4) i4_mldInterfaceIfIndex;
                i4_partial_index_flag = FALSE;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case SNMP_SEARCH_TYPE_NEXT:
        {
            /*  The Manager Has Not Given the Indices. */
            if (p_incoming->u4_Length <= p_in_db->u4_Length)
            {
                /*  Flag is Set to Indicate Low Level Get First Operation. */
                i4_partial_index_flag = FALSE;
                if ((i1_ret_val
                     =
                     nmhGetFirstIndexMldInterfaceTable
                     (&i4_mldInterfaceIfIndex)) == SNMP_SUCCESS)
                {
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_mldInterfaceIfIndex;
                }
                else
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else if (p_incoming->u4_Length > p_in_db->u4_Length)
            {
                /*  Flag is Set to Indicate Get First Operation. */
                i4_partial_index_flag = TRUE;

                /*  Initializing the Partial Index Length. */
                i4_partial_index_len = p_in_db->u4_Length;

                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)
                {
                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += INTEGER_LEN;
                    /* Extracting The Integer Index. */
                    i4_mldInterfaceIfIndex =
                        (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                       i4_offset];
                    i4_offset++;

                }

                /*
                 *  Get the value of the variable with the new index
                 *  By Calling the Low Level GET_NEXT routine.
                 */
                if ((i1_ret_val
                     =
                     nmhGetNextIndexMldInterfaceTable (i4_mldInterfaceIfIndex,
                                                       &i4_next_mldInterfaceIfIndex))
                    == SNMP_SUCCESS)
                {
                    i4_mldInterfaceIfIndex = i4_next_mldInterfaceIfIndex;
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_next_mldInterfaceIfIndex;
                }
                else
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);

    }                            /* End of SWITCH statement. */

    /*
     *  The SWITCH CASE statement for calling the Low Level fn
     *  for all objects Low Level Routines for all Objects.
     */
    switch (u1_arg)
    {
        case MLDINTERFACEIFINDEX:
        {
            i2_type = SNMP_DATA_TYPE_INTEGER;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == SNMP_SEARCH_TYPE_EXACT)
                || (i4_partial_index_flag == FALSE))
            {
                i4_return_val = i4_mldInterfaceIfIndex;
            }
            else
            {
                i4_return_val = i4_next_mldInterfaceIfIndex;
            }
            break;
        }
        case MLDINTERFACEQUERYINTERVAL:
        {
            i1_ret_val =
                nmhGetMldInterfaceQueryInterval (i4_mldInterfaceIfIndex,
                                                 &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_UNSIGNED32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case MLDINTERFACESTATUS:
        {
            i1_ret_val =
                nmhGetMldInterfaceStatus (i4_mldInterfaceIfIndex,
                                          &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case MLDINTERFACEVERSION:
        {
            i1_ret_val =
                nmhGetMldInterfaceVersion (i4_mldInterfaceIfIndex,
                                           &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_UNSIGNED32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case MLDINTERFACEQUERIER:
        {
            poctet_retval_mldInterfaceQuerier =
                (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (17);
            if (poctet_retval_mldInterfaceQuerier == NULL)
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            i1_ret_val =
                nmhGetMldInterfaceQuerier (i4_mldInterfaceIfIndex,
                                           poctet_retval_mldInterfaceQuerier);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_OCTET_PRIM;

                /*  Assigning the Octet Str Ptr Got From Low Level Rtns. */
                poctet_string = poctet_retval_mldInterfaceQuerier;
            }
            else
            {
                free_octetstring (poctet_retval_mldInterfaceQuerier);
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case MLDINTERFACEQUERYMAXRESPONSEDELAY:
        {
            i1_ret_val =
                nmhGetMldInterfaceQueryMaxResponseDelay (i4_mldInterfaceIfIndex,
                                                         &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_UNSIGNED32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case MLDINTERFACEJOINS:
        {
            i1_ret_val =
                nmhGetMldInterfaceJoins (i4_mldInterfaceIfIndex,
                                         &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_COUNTER32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case MLDINTERFACEGROUPS:
        {
            i1_ret_val =
                nmhGetMldInterfaceGroups (i4_mldInterfaceIfIndex,
                                          &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_GAUGE32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case MLDINTERFACEROBUSTNESS:
        {
            i1_ret_val =
                nmhGetMldInterfaceRobustness (i4_mldInterfaceIfIndex,
                                              &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_UNSIGNED32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case MLDINTERFACELASTLISTENQUERYINTVL:
        {
            i1_ret_val =
                nmhGetMldInterfaceLastListenQueryIntvl (i4_mldInterfaceIfIndex,
                                                        &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_UNSIGNED32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case MLDINTERFACEPROXYIFINDEX:
        {
            i1_ret_val =
                nmhGetMldInterfaceProxyIfIndex (i4_mldInterfaceIfIndex,
                                                &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case MLDINTERFACEQUERIERUPTIME:
        {
            i1_ret_val =
                nmhGetMldInterfaceQuerierUpTime (i4_mldInterfaceIfIndex,
                                                 &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_TIME_TICKS;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case MLDINTERFACEQUERIEREXPIRYTIME:
        {
            i1_ret_val =
                nmhGetMldInterfaceQuerierExpiryTime (i4_mldInterfaceIfIndex,
                                                     &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_TIME_TICKS;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);
    }                            /* End Of SWITCH Case for All Objects. */

    return (SNMP_AGT_FormVarBind
            (p_in_db, i2_type, u4_counter_val, i4_return_val, poctet_string,
             pOidValue, u8_counter_val));

}                                /*   THE GET FUNCTION GETS OVER . */

/****************************************************************************
 Function   : mldInterfaceEntrySet
 Description: This routine returns the value of the requestedMIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Output     : The actual OID for which the operation is
              performed is returned in p_in_db.
 Returns    : Returns one of the following error codes
           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for set_routine. */
INT4
mldInterfaceEntrySet (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                      UINT1 u1_arg, tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for set routine */

    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_size_offset = FALSE;
    /*
     *  This Variable is used to extract the Value in The
     *  MULTI DATA TYPE which is sent by the Manager.
     */

    INT4                i4_mldInterfaceIfIndex = FALSE;

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    if (p_incoming->u4_Length <= p_in_db->u4_Length)
    {
        return (SNMP_ERR_WRONG_LENGTH);
    }
    else
    {
        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;
        i4_size_offset += INTEGER_LEN;
        /* Extracting The Integer Index. */
        i4_mldInterfaceIfIndex =
            (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];
        i4_offset++;

    }                            /* End Of Else Condition. */
    switch (u1_arg)
    {
        case MLDINTERFACEQUERYINTERVAL:
        {
            i1_ret_val =
                nmhSetMldInterfaceQueryInterval (i4_mldInterfaceIfIndex,
                                                 p_value->u4_ULongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case MLDINTERFACESTATUS:
        {
            i1_ret_val =
                nmhSetMldInterfaceStatus (i4_mldInterfaceIfIndex,
                                          p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case MLDINTERFACEVERSION:
        {
            i1_ret_val =
                nmhSetMldInterfaceVersion (i4_mldInterfaceIfIndex,
                                           p_value->u4_ULongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case MLDINTERFACEQUERYMAXRESPONSEDELAY:
        {
            i1_ret_val =
                nmhSetMldInterfaceQueryMaxResponseDelay (i4_mldInterfaceIfIndex,
                                                         p_value->
                                                         u4_ULongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case MLDINTERFACEROBUSTNESS:
        {
            i1_ret_val =
                nmhSetMldInterfaceRobustness (i4_mldInterfaceIfIndex,
                                              p_value->u4_ULongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case MLDINTERFACELASTLISTENQUERYINTVL:
        {
            i1_ret_val =
                nmhSetMldInterfaceLastListenQueryIntvl (i4_mldInterfaceIfIndex,
                                                        p_value->u4_ULongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case MLDINTERFACEPROXYIFINDEX:
        {
            i1_ret_val =
                nmhSetMldInterfaceProxyIfIndex (i4_mldInterfaceIfIndex,
                                                p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
            /*  Read Only Variables. */
        case MLDINTERFACEIFINDEX:
            /*  Read Only Variables. */
        case MLDINTERFACEQUERIER:
            /*  Read Only Variables. */
        case MLDINTERFACEJOINS:
            /*  Read Only Variables. */
        case MLDINTERFACEGROUPS:
            /*  Read Only Variables. */
        case MLDINTERFACEQUERIERUPTIME:
            /*  Read Only Variables. */
        case MLDINTERFACEQUERIEREXPIRYTIME:
            return (SNMP_ERR_NOT_WRITABLE);

        default:
            return (SNMP_ERR_INCONSISTENT_NAME);
    }                            /* End of Switch */
}                                /*   THE SET ROUTINES GET OVER. */

/****************************************************************************
 Function   : mldInterfaceEntryTest
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Returns    : Returns one of the following error codes

           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_INCONSISTNT_VALUE
           SNMP_ERR_WRONG_VALUE
           SNMP_ERR_WRONG_LENGTH
           SNMP_ERR_NOT_WRITABLE
           SNMP_ERR_NO_CREATION
           SNMP_ERR_WRONG_TYPE
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for test_routine */

INT4
mldInterfaceEntryTest (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                       UINT1 u1_arg, tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for Test routine . */

    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_size_offset = FALSE;
    UINT4               u4ErrorCode = 0;
    /*
     *  This Variable is used to extract the Value in The
     *  Multi Data Type which is sent by the Manager
     */

    INT4                i4_mldInterfaceIfIndex = FALSE;

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    if (p_incoming->u4_Length <= p_in_db->u4_Length)
    {
        return (SNMP_ERR_WRONG_LENGTH);
    }
    else
    {
        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;
        i4_size_offset += INTEGER_LEN;

        if (p_incoming->u4_Length != (UINT4) i4_size_offset)
        {
            return (SNMP_ERR_WRONG_LENGTH);
        }
        /* Extracting The Integer Index. */
        i4_mldInterfaceIfIndex =
            (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];
        i4_offset++;

    }
    /*  Low Level Routine Which Validates the Indices. */
    /* COMMENT FOLLOWING CHECK TO ENABLE ROW CREATION 

       if((i1_ret_val = nmhValidateIndexInstanceMldInterfaceTable(i4_mldInterfaceIfIndex)) != SNMP_SUCCESS) {
       return SNMP_ERR_INCONSISTENT_NAME;
       } */
    switch (u1_arg)
    {

        case MLDINTERFACEQUERYINTERVAL:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_UNSIGNED32)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2MldInterfaceQueryInterval (&u4ErrorCode,
                                                    i4_mldInterfaceIfIndex,
                                                    p_value->u4_ULongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case MLDINTERFACESTATUS:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2MldInterfaceStatus (&u4ErrorCode,
                                             i4_mldInterfaceIfIndex,
                                             p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case MLDINTERFACEVERSION:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_UNSIGNED32)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2MldInterfaceVersion (&u4ErrorCode,
                                              i4_mldInterfaceIfIndex,
                                              p_value->u4_ULongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case MLDINTERFACEQUERYMAXRESPONSEDELAY:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_UNSIGNED32)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2MldInterfaceQueryMaxResponseDelay (&u4ErrorCode,
                                                            i4_mldInterfaceIfIndex,
                                                            p_value->
                                                            u4_ULongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case MLDINTERFACEROBUSTNESS:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_UNSIGNED32)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2MldInterfaceRobustness (&u4ErrorCode,
                                                 i4_mldInterfaceIfIndex,
                                                 p_value->u4_ULongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case MLDINTERFACELASTLISTENQUERYINTVL:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_UNSIGNED32)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2MldInterfaceLastListenQueryIntvl (&u4ErrorCode,
                                                           i4_mldInterfaceIfIndex,
                                                           p_value->
                                                           u4_ULongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case MLDINTERFACEPROXYIFINDEX:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2MldInterfaceProxyIfIndex (&u4ErrorCode,
                                                   i4_mldInterfaceIfIndex,
                                                   p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }
            /*  Read Only Variables */

        case MLDINTERFACEIFINDEX:
        case MLDINTERFACEQUERIER:
        case MLDINTERFACEJOINS:
        case MLDINTERFACEGROUPS:
        case MLDINTERFACEQUERIERUPTIME:
        case MLDINTERFACEQUERIEREXPIRYTIME:
            return (SNMP_ERR_NOT_WRITABLE);

        default:
            return (SNMP_ERR_INCONSISTENT_NAME);
    }                            /* End of Switch */
}                                /*  THE TEST ROUTINES ARE OVER . */

/****************************************************************************
 Function   : mldCacheEntryGet
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db       : The OID as formed by the SNMP Agent
                              from the static MIB database.
              p_incoming    : The OID as it is send by the manager
                              in the SNMP PDU.
              u1_arg        : The position of the variable in the MIB group.
              u1_search_type: Indicates whether this routine is called
                              as a GET or GETNEXT.
 Output     : The actual OID for which the operation is performed is
              returned in p_in_db.
 Returns    : Returns a pointer to tSNMP_VAR_BIND structure, with either
              the corresponding value, or NULL if the GET/GETNEXT
              operation could not be performed successfully.
****************************************************************************/

/* Prototype declarations for get_routine. */
tSNMP_VAR_BIND     *
mldCacheEntryGet (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                  UINT1 u1_arg, UINT1 u1_search_type)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for get routine . */
    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_partial_index_len = FALSE;
    INT4                i4_partial_index_flag = TRUE;
    INT4                i4_size_offset = FALSE;
    INT4                LEN_mldCacheTable_INDEX;

    /*  The Declaration of the Vars which are passed to FormVarbind Fn. */
    INT4                i4_return_val = FALSE;
    UINT4               u4_counter_val = FALSE;
    tSNMP_COUNTER64_TYPE u8_counter_val = { 0, 0 };
    INT2                i2_type;

    /*
     *  The Declaration of the Octet String Array which is used for
     *  conversion and The Pointer of tSNMP_OCTET_STRING_TYPE.
     */
    tSNMP_OCTET_STRING_TYPE *poctet_string = NULL;
    tSNMP_OID_TYPE     *pOidValue = NULL;

    /*
     *  These Variable are declared for being used in the
     *  FOR Loop for extracting Indices from the given OID.
     */
    INT4                i4_count = FALSE;
    /*
     *  The tSNMP_OCTET_STRING_TYPE Which Store
     *  the Length and the Octet String.
     */
    INT4                i4_len_Octet_index_mldCacheAddress = FALSE;
    tSNMP_OCTET_STRING_TYPE *poctet_mldCacheAddress = NULL;
    tSNMP_OCTET_STRING_TYPE *poctet_next_mldCacheAddress = NULL;

    INT4                i4_mldCacheIfIndex = FALSE;
    INT4                i4_next_mldCacheIfIndex = FALSE;

    tSNMP_OCTET_STRING_TYPE *poctet_retval_mldCacheLastReporter = NULL;

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    switch (u1_search_type)
    {

        case SNMP_SEARCH_TYPE_EXACT:
        {
            /*
             *  Initializing the i4_size_offset Variable to the Length of the
             *  OID of the Object i.e the Length present in the p_in_db.
             */
            i4_size_offset = p_in_db->u4_Length;
            i4_len_Octet_index_mldCacheAddress =
                p_incoming->pu4_OidList[i4_size_offset++];
            i4_size_offset += i4_len_Octet_index_mldCacheAddress;
            i4_size_offset += INTEGER_LEN;

            if (p_incoming->u4_Length != (UINT4) i4_size_offset)
            {
                return (NULL);
            }
            /*
             *  Finding the length of the Index for extracting
             *  Index from the structure given by the Manager.
             */
            /*
             *  If LEN_OF_VARIABLE_LEN_INDEX is Present refer Header File
             *  where it is Defined for Explanation of the Variable.
             */
            LEN_mldCacheTable_INDEX =
                p_in_db->u4_Length + i4_len_Octet_index_mldCacheAddress +
                LEN_OF_VARIABLE_LEN_INDEX + INTEGER_LEN;

            if (LEN_mldCacheTable_INDEX == (INT4) p_incoming->u4_Length)
            {
                /*  Allocating Memory for the Get Exact Octet String Index. */
                poctet_mldCacheAddress =
                    (tSNMP_OCTET_STRING_TYPE *)
                    allocmem_octetstring (i4_len_Octet_index_mldCacheAddress);
                if ((poctet_mldCacheAddress == NULL))
                {
                    free_octetstring (poctet_mldCacheAddress);
                    return ((tSNMP_VAR_BIND *) NULL);
                }
                /*
                 *  This is to Increment the Array Pointer by one Which
                 *  Contains the Length of the Index.
                 */
                i4_offset++;
                /*  The FOR LOOP for extracting Octet_str Index from the Given OID. */
                for (i4_count = FALSE;
                     i4_count < i4_len_Octet_index_mldCacheAddress;
                     i4_count++, i4_offset++)
                    poctet_mldCacheAddress->pu1_OctetList[i4_count] =
                        (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                        i4_offset];

                /* Extracting The Integer Index. */
                i4_mldCacheIfIndex =
                    (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                   i4_offset];
                i4_offset++;

                /*  Low Level Routine Which Validates the Indices. */
                if ((i1_ret_val =
                     nmhValidateIndexInstanceMldCacheTable
                     (poctet_mldCacheAddress,
                      i4_mldCacheIfIndex)) != SNMP_SUCCESS)
                {
                    free_octetstring (poctet_mldCacheAddress);
                    return ((tSNMP_VAR_BIND *) NULL);
                }
                /*  Storing the Extracted Index in p_in_db. */
                /*  Storing the Length of the Octet String Get Exact.  */
                p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                    poctet_mldCacheAddress->i4_Length;
                /*  FOR Loop for storing the value from get first to p_in_db. */
                for (i4_count = FALSE;
                     i4_count < poctet_mldCacheAddress->i4_Length; i4_count++)
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) poctet_mldCacheAddress->pu1_OctetList[i4_count];

                p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                    (UINT4) i4_mldCacheIfIndex;
                i4_partial_index_flag = FALSE;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case SNMP_SEARCH_TYPE_NEXT:
        {
            /*  The Manager Has Not Given the Indices. */
            if (p_incoming->u4_Length <= p_in_db->u4_Length)
            {
                /*  Allocating Memory for the Get First Octet String Index. */
                poctet_mldCacheAddress =
                    (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (17);
                if ((poctet_mldCacheAddress == NULL))
                {
                    free_octetstring (poctet_mldCacheAddress);
                    return ((tSNMP_VAR_BIND *) NULL);
                }
                /*  Flag is Set to Indicate Low Level Get First Operation. */
                i4_partial_index_flag = FALSE;
                if ((i1_ret_val
                     =
                     nmhGetFirstIndexMldCacheTable (poctet_mldCacheAddress,
                                                    &i4_mldCacheIfIndex)) ==
                    SNMP_SUCCESS)
                {
                    /*  Storing the Length. */
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        poctet_mldCacheAddress->i4_Length;
                    /*  FOR Loop for storing the value from get first to p_in_db. */
                    for (i4_count = FALSE;
                         i4_count < poctet_mldCacheAddress->i4_Length;
                         i4_count++)
                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            (UINT4) poctet_mldCacheAddress->
                            pu1_OctetList[i4_count];

                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_mldCacheIfIndex;
                }
                else
                {
                    free_octetstring (poctet_mldCacheAddress);
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else if (p_incoming->u4_Length > p_in_db->u4_Length)
            {
                /*  Flag is Set to Indicate Get First Operation. */
                i4_partial_index_flag = TRUE;

                /*  Initializing the Partial Index Length. */
                i4_partial_index_len = p_in_db->u4_Length;

                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)
                {
                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_len_Octet_index_mldCacheAddress =
                        p_incoming->pu4_OidList[i4_partial_index_len++];
                    i4_partial_index_len += i4_len_Octet_index_mldCacheAddress;
                    /*  Allocating Memory for The Octet Str Get Next Index. */
                    poctet_mldCacheAddress =
                        (tSNMP_OCTET_STRING_TYPE *)
                        allocmem_octetstring
                        (i4_len_Octet_index_mldCacheAddress);
                    poctet_next_mldCacheAddress =
                        (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (17);

                    /*  Checking for the malloc failure.  */
                    if ((poctet_mldCacheAddress == NULL)
                        || (poctet_next_mldCacheAddress == NULL))
                    {
                        /*  Freeing the Current Index. */
                        free_octetstring (poctet_mldCacheAddress);
                        free_octetstring (poctet_next_mldCacheAddress);
                        return ((tSNMP_VAR_BIND *) NULL);
                    }

                    /*
                     *  This is to Increment the Array Pointer by one Which
                     *  Contains the Length of the Index.
                     */
                    i4_offset++;
                    /*  The FOR LOOP for extracting Octet_str Index from the Given OID. */
                    for (i4_count = FALSE;
                         i4_count < i4_len_Octet_index_mldCacheAddress;
                         i4_count++, i4_offset++)
                        poctet_mldCacheAddress->pu1_OctetList[i4_count] =
                            (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                            i4_offset];

                }
                else
                {
                    /*
                     *  Memory Allocation of the (Partial) Index of type OID
                     *  and Octet string which is not given by the Manager.
                     */
                    poctet_mldCacheAddress = allocmem_octetstring (17);
                    poctet_next_mldCacheAddress = allocmem_octetstring (17);
                    if ((poctet_mldCacheAddress == NULL)
                        || (poctet_next_mldCacheAddress == NULL))
                    {
                        free_octetstring (poctet_mldCacheAddress);
                        free_octetstring (poctet_next_mldCacheAddress);
                        return ((tSNMP_VAR_BIND *) NULL);
                    }
                }
                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)
                {
                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += INTEGER_LEN;
                    /* Extracting The Integer Index. */
                    i4_mldCacheIfIndex =
                        (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                       i4_offset];
                    i4_offset++;

                }

                /*
                 *  Get the value of the variable with the new index
                 *  By Calling the Low Level GET_NEXT routine.
                 */
                if ((i1_ret_val
                     =
                     nmhGetNextIndexMldCacheTable (poctet_mldCacheAddress,
                                                   poctet_next_mldCacheAddress,
                                                   i4_mldCacheIfIndex,
                                                   &i4_next_mldCacheIfIndex)) ==
                    SNMP_SUCCESS)
                {
                    /*  Storing the Value of the Len of Octet Str in p_in_db. */
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        poctet_next_mldCacheAddress->i4_Length;
                    for (i4_count = FALSE;
                         i4_count < poctet_next_mldCacheAddress->i4_Length;
                         i4_count++)
                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            (UINT4) poctet_next_mldCacheAddress->
                            pu1_OctetList[i4_count];
                    free_octetstring (poctet_mldCacheAddress);
                    poctet_mldCacheAddress = poctet_next_mldCacheAddress;
                    i4_mldCacheIfIndex = i4_next_mldCacheIfIndex;
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_next_mldCacheIfIndex;
                }
                else
                {
                    free_octetstring (poctet_mldCacheAddress);
                    free_octetstring (poctet_next_mldCacheAddress);
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);

    }                            /* End of SWITCH statement. */

    /*
     *  The SWITCH CASE statement for calling the Low Level fn
     *  for all objects Low Level Routines for all Objects.
     */
    switch (u1_arg)
    {
        case MLDCACHEADDRESS:
        {
            i2_type = SNMP_DATA_TYPE_OCTET_PRIM;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == SNMP_SEARCH_TYPE_EXACT)
                || (i4_partial_index_flag == FALSE))
            {
                poctet_string = poctet_mldCacheAddress;
            }
            else
            {
                poctet_string = poctet_next_mldCacheAddress;
            }
            break;
        }
        case MLDCACHEIFINDEX:
        {
            i2_type = SNMP_DATA_TYPE_INTEGER;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == SNMP_SEARCH_TYPE_EXACT)
                || (i4_partial_index_flag == FALSE))
            {
                i4_return_val = i4_mldCacheIfIndex;
            }
            else
            {
                i4_return_val = i4_next_mldCacheIfIndex;
            }
            free_octetstring (poctet_mldCacheAddress);
            break;
        }
        case MLDCACHESELF:
        {
            i1_ret_val =
                nmhGetMldCacheSelf (poctet_mldCacheAddress, i4_mldCacheIfIndex,
                                    &i4_return_val);
            free_octetstring (poctet_mldCacheAddress);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case MLDCACHELASTREPORTER:
        {
            poctet_retval_mldCacheLastReporter =
                (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (17);
            if (poctet_retval_mldCacheLastReporter == NULL)
            {
                free_octetstring (poctet_mldCacheAddress);
                return ((tSNMP_VAR_BIND *) NULL);
            }
            i1_ret_val =
                nmhGetMldCacheLastReporter (poctet_mldCacheAddress,
                                            i4_mldCacheIfIndex,
                                            poctet_retval_mldCacheLastReporter);
            free_octetstring (poctet_mldCacheAddress);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_OCTET_PRIM;

                /*  Assigning the Octet Str Ptr Got From Low Level Rtns. */
                poctet_string = poctet_retval_mldCacheLastReporter;
            }
            else
            {
                free_octetstring (poctet_retval_mldCacheLastReporter);
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case MLDCACHEUPTIME:
        {
            i1_ret_val =
                nmhGetMldCacheUpTime (poctet_mldCacheAddress,
                                      i4_mldCacheIfIndex, &u4_counter_val);
            free_octetstring (poctet_mldCacheAddress);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_TIME_TICKS;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case MLDCACHEEXPIRYTIME:
        {
            i1_ret_val =
                nmhGetMldCacheExpiryTime (poctet_mldCacheAddress,
                                          i4_mldCacheIfIndex, &u4_counter_val);
            free_octetstring (poctet_mldCacheAddress);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_TIME_TICKS;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case MLDCACHESTATUS:
        {
            i1_ret_val =
                nmhGetMldCacheStatus (poctet_mldCacheAddress,
                                      i4_mldCacheIfIndex, &i4_return_val);
            free_octetstring (poctet_mldCacheAddress);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            free_octetstring (poctet_mldCacheAddress);
            return ((tSNMP_VAR_BIND *) NULL);
    }                            /* End Of SWITCH Case for All Objects. */

    return (SNMP_AGT_FormVarBind
            (p_in_db, i2_type, u4_counter_val, i4_return_val, poctet_string,
             pOidValue, u8_counter_val));

}                                /*   THE GET FUNCTION GETS OVER . */

/****************************************************************************
 Function   : mldCacheEntrySet
 Description: This routine returns the value of the requestedMIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Output     : The actual OID for which the operation is
              performed is returned in p_in_db.
 Returns    : Returns one of the following error codes
           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for set_routine. */
INT4
mldCacheEntrySet (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                  UINT1 u1_arg, tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for set routine */

    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_size_offset = FALSE;
    /*
     *  This Variable is used to extract the Value in The
     *  MULTI DATA TYPE which is sent by the Manager.
     */

    INT4                i4_count = FALSE;
    /*
     *  The tSNMP_OCTET_STRING_TYPE Which Store
     *  the Length and the Octet String.
     */
    INT4                i4_len_Octet_index_mldCacheAddress = FALSE;
    tSNMP_OCTET_STRING_TYPE *poctet_mldCacheAddress = NULL;

    INT4                i4_mldCacheIfIndex = FALSE;

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    if (p_incoming->u4_Length <= p_in_db->u4_Length)
    {
        return (SNMP_ERR_WRONG_LENGTH);
    }
    else
    {
        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;
        i4_len_Octet_index_mldCacheAddress =
            p_incoming->pu4_OidList[i4_size_offset++];
        i4_size_offset += i4_len_Octet_index_mldCacheAddress;
        i4_size_offset += INTEGER_LEN;
        /*  Allocating Memory for the Get Exact Octet String Index. */
        poctet_mldCacheAddress =
            (tSNMP_OCTET_STRING_TYPE *)
            allocmem_octetstring (i4_len_Octet_index_mldCacheAddress);
        if ((poctet_mldCacheAddress == NULL))
        {
            free_octetstring (poctet_mldCacheAddress);
            return (SNMP_ERR_GEN_ERR);
        }
        /*
         *  This is to Increment the Array Pointer by one Which
         *  Contains the Length of the Index.
         */
        i4_offset++;
        /*  The FOR LOOP for extracting Octet_str Index from the Given OID. */
        for (i4_count = FALSE; i4_count < i4_len_Octet_index_mldCacheAddress;
             i4_count++, i4_offset++)
            poctet_mldCacheAddress->pu1_OctetList[i4_count] =
                (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];

        /* Extracting The Integer Index. */
        i4_mldCacheIfIndex =
            (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];
        i4_offset++;

    }                            /* End Of Else Condition. */
    switch (u1_arg)
    {
        case MLDCACHESELF:
        {
            i1_ret_val =
                nmhSetMldCacheSelf (poctet_mldCacheAddress, i4_mldCacheIfIndex,
                                    p_value->i4_SLongValue);

            free_octetstring (poctet_mldCacheAddress);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case MLDCACHESTATUS:
        {
            i1_ret_val =
                nmhSetMldCacheStatus (poctet_mldCacheAddress,
                                      i4_mldCacheIfIndex,
                                      p_value->i4_SLongValue);

            free_octetstring (poctet_mldCacheAddress);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
            /*  Read Only Variables. */
        case MLDCACHEADDRESS:
            /*  Read Only Variables. */
        case MLDCACHEIFINDEX:
            /*  Read Only Variables. */
        case MLDCACHELASTREPORTER:
            /*  Read Only Variables. */
        case MLDCACHEUPTIME:
            /*  Read Only Variables. */
        case MLDCACHEEXPIRYTIME:
            free_octetstring (poctet_mldCacheAddress);
            return (SNMP_ERR_NOT_WRITABLE);

        default:
            free_octetstring (poctet_mldCacheAddress);
            return (SNMP_ERR_INCONSISTENT_NAME);
    }                            /* End of Switch */
}                                /*   THE SET ROUTINES GET OVER. */

/****************************************************************************
 Function   : mldCacheEntryTest
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Returns    : Returns one of the following error codes

           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_INCONSISTNT_VALUE
           SNMP_ERR_WRONG_VALUE
           SNMP_ERR_WRONG_LENGTH
           SNMP_ERR_NOT_WRITABLE
           SNMP_ERR_NO_CREATION
           SNMP_ERR_WRONG_TYPE
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for test_routine */

INT4
mldCacheEntryTest (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                   UINT1 u1_arg, tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for Test routine . */

    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_size_offset = FALSE;
    UINT4               u4ErrorCode = 0;
    /*
     *  This Variable is used to extract the Value in The
     *  Multi Data Type which is sent by the Manager
     */

    INT4                i4_count = FALSE;
    /*
     *  The tSNMP_OCTET_STRING_TYPE Which Store
     *  the Length and the Octet String.
     */
    INT4                i4_len_Octet_index_mldCacheAddress = FALSE;
    tSNMP_OCTET_STRING_TYPE *poctet_mldCacheAddress = NULL;

    INT4                i4_mldCacheIfIndex = FALSE;

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    if (p_incoming->u4_Length <= p_in_db->u4_Length)
    {
        return (SNMP_ERR_WRONG_LENGTH);
    }
    else
    {
        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;
        i4_len_Octet_index_mldCacheAddress =
            p_incoming->pu4_OidList[i4_size_offset++];
        i4_size_offset += i4_len_Octet_index_mldCacheAddress;
        i4_size_offset += INTEGER_LEN;

        if (p_incoming->u4_Length != (UINT4) i4_size_offset)
        {
            return (SNMP_ERR_WRONG_LENGTH);
        }
        /*  Allocating Memory for the Get Exact Octet String Index. */
        poctet_mldCacheAddress =
            (tSNMP_OCTET_STRING_TYPE *)
            allocmem_octetstring (i4_len_Octet_index_mldCacheAddress);
        if ((poctet_mldCacheAddress == NULL))
        {
            free_octetstring (poctet_mldCacheAddress);
            return (SNMP_ERR_GEN_ERR);
        }
        /*
         *  This is to Increment the Array Pointer by one Which
         *  Contains the Length of the Index.
         */
        i4_offset++;
        /*  The FOR LOOP for extracting Octet_str Index from the Given OID. */
        for (i4_count = FALSE; i4_count < i4_len_Octet_index_mldCacheAddress;
             i4_count++, i4_offset++)
            poctet_mldCacheAddress->pu1_OctetList[i4_count] =
                (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];

        /* Extracting The Integer Index. */
        i4_mldCacheIfIndex =
            (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];
        i4_offset++;

    }
    /*  Low Level Routine Which Validates the Indices. */
    /* COMMENT FOLLOWING CHECK TO ENABLE ROW CREATION 

       if((i1_ret_val = nmhValidateIndexInstanceMldCacheTable(poctet_mldCacheAddress , i4_mldCacheIfIndex)) != SNMP_SUCCESS) {
       free_octetstring(poctet_mldCacheAddress);
       return SNMP_ERR_INCONSISTENT_NAME;
       } */
    switch (u1_arg)
    {

        case MLDCACHESELF:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2MldCacheSelf (&u4ErrorCode, poctet_mldCacheAddress,
                                       i4_mldCacheIfIndex,
                                       p_value->i4_SLongValue);

            free_octetstring (poctet_mldCacheAddress);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case MLDCACHESTATUS:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2MldCacheStatus (&u4ErrorCode, poctet_mldCacheAddress,
                                         i4_mldCacheIfIndex,
                                         p_value->i4_SLongValue);

            free_octetstring (poctet_mldCacheAddress);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }
            /*  Read Only Variables */

        case MLDCACHEADDRESS:
        case MLDCACHEIFINDEX:
        case MLDCACHELASTREPORTER:
        case MLDCACHEUPTIME:
        case MLDCACHEEXPIRYTIME:
            free_octetstring (poctet_mldCacheAddress);
            return (SNMP_ERR_NOT_WRITABLE);

        default:
            free_octetstring (poctet_mldCacheAddress);
            return (SNMP_ERR_INCONSISTENT_NAME);
    }                            /* End of Switch */
}                                /*  THE TEST ROUTINES ARE OVER . */
