# include  "include.h"
# include  "fsmldmid.h"
# include  "fsmldlow.h"
# include  "fsmldcon.h"
# include  "fsmldogi.h"
# include  "extern.h"
# include  "midconst.h"
# include  "fsmldcli.h"

/****************************************************************************
 Function   : fsmldScalarsGet
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db       : The OID as formed by the SNMP Agent
                              from the static MIB database.
              p_incoming    : The OID as it is send by the manager
                              in the SNMP PDU.
              u1_arg        : The position of the variable in the MIB group.
              u1_search_type: Indicates whether this routine is called
                              as a GET or GETNEXT.
 Output     : The actual OID for which the operation is performed is
              returned in p_in_db.
 Returns    : Returns a pointer to tSNMP_VAR_BIND structure, with either
              the corresponding value, or NULL if the GET/GETNEXT
              operation could not be performed successfully.
****************************************************************************/

/* Prototype declarations for get_routine. */
tSNMP_VAR_BIND     *
fsmldScalarsGet (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                 UINT1 u1_arg, UINT1 u1_search_type)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for Scalar get routine. */

    UINT1               i1_ret_val = FALSE;
    INT4                LEN_fsmldScalars_INDEX;

    /*  The Declaration of the Vars which are passed to FormVarbind Fn. */
    INT4                i4_return_val = FALSE;
    UINT4               u4_counter_val = FALSE;
    tSNMP_COUNTER64_TYPE u8_counter_val = { 0, 0 };
    INT2                i2_type;

    /*
     *  The Declaration of the Octet String Array which is used for
     *  conversion and The Pointer of tSNMP_OCTET_STRING_TYPE.
     */
    tSNMP_OCTET_STRING_TYPE *poctet_string = NULL;
    tSNMP_OID_TYPE     *pOidValue = NULL;

    /*
     *  This Variable is declared for being used in the
     *  FOR Loop for extracting Indices from OID Given.
     */

/*** DECLARATION_END ***/

    LEN_fsmldScalars_INDEX = p_in_db->u4_Length;

    /*  Incrementing the Length for the Extract of Scalar Tables. */
    LEN_fsmldScalars_INDEX++;
    if (u1_search_type == SNMP_SEARCH_TYPE_EXACT)
    {
        if ((LEN_fsmldScalars_INDEX != (INT4) p_incoming->u4_Length)
            || (p_incoming->pu4_OidList[p_incoming->u4_Length - 1] != 0))
        {
            return ((tSNMP_VAR_BIND *) NULL);
        }
    }
    else
    {
        /*  Get Next Operation on the Scalar Variable.  */
        if ((INT4) p_incoming->u4_Length >= LEN_fsmldScalars_INDEX)
        {
            return ((tSNMP_VAR_BIND *) NULL);
        }
    }
    switch (u1_arg)
    {
        case FSMLDNOOFCACHEENTRIES:
        {
            i1_ret_val = nmhGetFsmldNoOfCacheEntries (&u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_UNSIGNED32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSMLDNOOFROUTINGPROTOCOLS:
        {
            i1_ret_val = nmhGetFsmldNoOfRoutingProtocols (&u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_UNSIGNED32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSMLDTRACEDEBUG:
        {
            i1_ret_val = nmhGetFsmldTraceDebug (&u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_UNSIGNED32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSMLDMODE:
        {
            i1_ret_val = nmhGetFsmldMode (&i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSMLDPROTOCOLUPDOWN:
        {
            i1_ret_val = nmhGetFsmldProtocolUpDown (&i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);
    }                            /* End Of SWITCH Case for All Objects. */

    /* Incrementing the Length of the p_in_db. */
    p_in_db->u4_Length++;
    /* Adding the .0 to the p_in_db for scalar Objects. */
    p_in_db->pu4_OidList[p_in_db->u4_Length - 1] = ZERO;

    return (SNMP_AGT_FormVarBind
            (p_in_db, i2_type, u4_counter_val, i4_return_val, poctet_string,
             pOidValue, u8_counter_val));

}                                /*   THE GET FUNCTION GETS OVER . */

/****************************************************************************
 Function   : fsmldScalarsSet
 Description: This routine returns the value of the requestedMIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Output     : The actual OID for which the operation is
              performed is returned in p_in_db.
 Returns    : Returns one of the following error codes
           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for set_routine. */
INT4
fsmldScalarsSet (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                 UINT1 u1_arg, tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for set routine */

    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_size_offset = FALSE;
    /*
     *  This Variable is used to extract the Value in The
     *  MULTI DATA TYPE which is sent by the Manager.
     */

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    if (p_incoming->u4_Length <= p_in_db->u4_Length)
    {
        return (SNMP_ERR_WRONG_LENGTH);
    }
    else
    {
        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;
    }                            /* End Of Else Condition. */
    switch (u1_arg)
    {
        case FSMLDNOOFCACHEENTRIES:
        {
            i1_ret_val = nmhSetFsmldNoOfCacheEntries (p_value->u4_ULongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSMLDNOOFROUTINGPROTOCOLS:
        {
            i1_ret_val =
                nmhSetFsmldNoOfRoutingProtocols (p_value->u4_ULongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSMLDTRACEDEBUG:
        {
            i1_ret_val = nmhSetFsmldTraceDebug (p_value->u4_ULongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSMLDMODE:
        {
            i1_ret_val = nmhSetFsmldMode (p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSMLDPROTOCOLUPDOWN:
        {
            i1_ret_val = nmhSetFsmldProtocolUpDown (p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        default:
            return (SNMP_ERR_INCONSISTENT_NAME);
    }                            /* End of Switch */
}                                /*   THE SET ROUTINES GET OVER. */

/****************************************************************************
 Function   : fsmldScalarsTest
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Returns    : Returns one of the following error codes

           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_INCONSISTNT_VALUE
           SNMP_ERR_WRONG_VALUE
           SNMP_ERR_WRONG_LENGTH
           SNMP_ERR_NOT_WRITABLE
           SNMP_ERR_NO_CREATION
           SNMP_ERR_WRONG_TYPE
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for test_routine */

INT4
fsmldScalarsTest (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                  UINT1 u1_arg, tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for Test routine . */

    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_size_offset = FALSE;
    UINT4               u4ErrorCode = 0;
    /*
     *  This Variable is used to extract the Value in The
     *  Multi Data Type which is sent by the Manager
     */

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    if (p_incoming->u4_Length <= p_in_db->u4_Length)
    {
        return (SNMP_ERR_WRONG_LENGTH);
    }
    else
    {
        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;
    }
    switch (u1_arg)
    {

        case FSMLDNOOFCACHEENTRIES:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_UNSIGNED32)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2FsmldNoOfCacheEntries (&u4ErrorCode,
                                                p_value->u4_ULongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSMLDNOOFROUTINGPROTOCOLS:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_UNSIGNED32)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2FsmldNoOfRoutingProtocols (&u4ErrorCode,
                                                    p_value->u4_ULongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSMLDTRACEDEBUG:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_UNSIGNED32)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2FsmldTraceDebug (&u4ErrorCode, p_value->u4_ULongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSMLDMODE:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2FsmldMode (&u4ErrorCode, p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSMLDPROTOCOLUPDOWN:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2FsmldProtocolUpDown (&u4ErrorCode,
                                              p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        default:
            return (SNMP_ERR_INCONSISTENT_NAME);
    }                            /* End of Switch */
}                                /*  THE TEST ROUTINES ARE OVER . */
