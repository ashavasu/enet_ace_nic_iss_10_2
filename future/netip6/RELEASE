Copyright (C) 2008 Aricent Inc . All Rights Reserved
--------------------------------------------------
PRODUCT NAME   :  Aricentnetip6
-------------------------------
     __________________________________________________________________
    |    RELEASE NUMBER  : NETIP6_4-1-0-2                              |
    |    RELEASE DATE    : February 08 2008                            |
    |__________________________________________________________________|

     Known problems in this release:

     o One warning specific to DIAB compiler is present in fsm6low.c 
     o Support for multicast is not comprehensively addressed in mobile 
       IPv6 "draft-ietf-mobile-ipv6-17.txt". Hence there is a limitation 
       in working together of Aricent MLD and Aricent MobileIPv6. 
       Therefore both should not be enabled at same time.
     o The current version of Aricent Home Agent can support only 250 
       tunnels at any time. Therefore only 250 mobile nodes can move to 
       other foreign networks.

    ______ CHANGELOG: _________________________________________________
     **  Modified for recvfrom() system call is not called with proper 
     **  argument.

      + fsip6/ip6core/traceroute.c
                                                            [RFC: 21978]
     __________________________________________________________________  
     ** Modified printf and scanf format specifier for 64 bit migration.

      + fsip6/ip6core/ip6cli.c
      + fsip6/ip6core/ip6frag.c
      + fsip6/ip6core/ip6if.c
      + fsip6/ip6core/ip6main.c
                                                            [RFC: 23837]
     __________________________________________________________________  
     **  Fix for 'genErr'  was observed for some MIB objects in IPv6 
     **  module.

      + fsip6/ip6core/fsip6low.c
                                                            [RFC: 25328]
     __________________________________________________________________
    (______________________________END_________________________________)



   __________________________________________________________________
    |    RELEASE NUMBER  : NETIP6_4-0-3                                |
    |    RELEASE DATE    : October 29 2007                             |
    '__________________________________________________________________'

    ...... OBSOLETED: .................................................
      + FILES.mld
   
    ______ CHANGELOG: _________________________________________________
    ** Copyright changes

      + rtm6/src/fsrtmlow.c
                                                            [RFC: 18911]
    _________________________________________________________________
    **  Modified the tIp6Params mem pool creation with HEAP memory type.

      + fsip6/ip6core/ip6lib.c
                                                            [RFC: 12119]
   __________________________________________________________________
     **  Modified for problems found during Silver Creek Test Suite
     **  execution.

      + fsip6/mld/stdmllow.c
                                                            [RFC: 12301]
     __________________________________________________________________
    (______________________________END_________________________________)

    __________________________________________________________________
    |    RELEASE NUMBER  : NETIP6_4-0-2                                |
    |    RELEASE DATE    : Aug 31 2007                                 |
    '__________________________________________________________________'

    ______ CHANGELOG: _________________________________________________

     **  NETIP6 Maintanance baseline merges to ISS TOS

      + netip6/fsip6/ip6core/ip6addr.c
      + netip6/fsip6/ip6core/ip6exhdr.c
      + netip6/fsip6/ip6core/ip6lib.c
      + netip6/fsip6/ip6core/ip6snmp.h
      + netip6/fsip6/ip6core/ip6sys.h
      + netip6/fsip6/ip6rtr/nd6rtr.c
      + netip6/fsip6/mld/stdmllow.c
      + netip6/rtm6/src/fsrtmlow.c
                                                            [RFC: 22955]
     __________________________________________________________________
    (______________________________END_________________________________)
     __________________________________________________________________
    |    RELEASE NUMBER  : NETIP6_4-0-1                                |
    |    RELEASE DATE    : July 20 2007                                |
    '__________________________________________________________________'

    ...... ADDED: .................................................
     **

    ______ CHANGELOG: _________________________________________________
     **  Corrected the usage of u4Flags of OsixReceiveEvent in protocol
     **  modules

      + fsip6/ip6core/ip6main.c
      + fsip6/ip6core/ping6.c
                                                            [RFC: 19844]
     __________________________________________________________________
     **  FSAP Name to Id changes done for ISS, LR, CFA, CLI, npapi, RMON,
     **  RM, SSL, SSH, UTIL and L2 modules.

      + rtm6/src/rtm6tskmg.c
                                                            [RFC: 20031]
     __________________________________________________________________
    (______________________________END_________________________________)

     __________________________________________________________________
    |    RELEASE NUMBER  : NETIP6_4-0-0-4                              |
    |    RELEASE DATE    : February 23 2007                            |
    '__________________________________________________________________'

    ______ CHANGELOG: _________________________________________________
     **  PIMv6 code merge. 

      + fsip6/ip6core/ip6main.c
      + fsip6/ip6core/ip6sys.h
      + fsip6/ip6core/ip6in.c
      + fsip6/mld/mldinc.h
      + fsip6/ip6core/ip6proto.h
      + fsip6/ip6core/ip6out.c
      + fsip6/ip6core/ip6hli.c
      + fsip6/ip6core/ip6if.c
      + fsip6/mld/mldmain.c
      + fsip6/ip6core/netipv6.c
                                                            [RFC: 16117]
     __________________________________________________________________
     **  Ip6ChkEntryInMACFilter check removed in Ip6TaskEnqueuePkt 

      + fsip6/ip6core/ip6main.c
                                                            [RFC: 16244]
     __________________________________________________________________
    (______________________________END_________________________________)
     __________________________________________________________________
    |    RELEASE NUMBER  : IP6_4-0-0-3                                 |
    |    RELEASE DATE    : April 7th 2006                              |
    '__________________________________________________________________|


     ______ CHANGELOG: ________________________________________________
     ------------------------------------------------------------------
     **  Cleanup of L2 modules including removal of unwanted switches.
     **  This has been done to simplify the porting process.

                                                           [RFC: 14146]
     -------------------------------------------------------------------
     **  Modified for errors found in Tahi conformance testcases for IPv6
     **  router functionality.
         
                                                           [RFC: 14162]
     ------------------------------------------------------------------
      **  Unused features are removed as part of cleanup.
             * PPP support in CFA.
             * IPOA support in CFA.
             * ATM support in CFA.
             * Runtime sizing parameter change support.

                                                           [RFC: 14224]
     ------------------------------------------------------------------
     **  Removed Back-reference to protocol code from ipv6 npapi files.
         LinuxIP support provided for fsipv6 tunnels.
         SLI support for RIP6 with LinuxIP and Fsipv6

                                                            [RFC: 14243]
     -------------------------------------------------------------------
     **  Modified to avoid direct access of cfa data structure in
         Get Hardware address call, Instead used provide api CfaGetIfInfo.

                                                            [RFC: 14249]
     -------------------------------------------------------------------
     **  Modified to calculate payload of icmp6 error message properply.
         Addressed buffer corruption problem in nd6 redirect message.
         syntax for ipv6 route and ipv6 ping is updated.
             
                                                            [RFC: 14319]
     -------------------------------------------------------------------
     **  Modified for the Compliation issues when SLI and LINUX_SLI not
     **  defined.

                                                            [RFC: 14482]
     -------------------------------------------------------------------                          
     **  Added Support for Ipv6 Auto Tunnels in LNXIP4-FSIP6 combination.
     **  Modified Memory corruption in ipv6 address prefix copy. 

                                                            [RFC: 14503]
     --------------------------------------------------------------------  
     **  Modified the link local address sorting issue in ipv6.

                                                             [RFC: 14782]
     --------------------------------------------------------------------
     _________________________________________________________________
    (______________________________END_________________________________)
 



    ____________________________________________________________________
    |    RELEASE NUMBER  : PA5-4-0-0_2                                 |
    |    RELEASE DATE    : December 6 2004                             |
    '__________________________________________________________________|

     NOTE:
     PA tag is provided for this release for the following reasons:
        ** All faults raised by PA are addressed.
        ** Silver Creek tested for IPv6 mibs
        ** NPAPI hooks added.
        ** Tag released to get PA approval
        ** Updated the products baseline from V6R

     KNOWN ISSUES:
        **  Link Local address change will not initiate RIP6 triggered updates
        with metric 16 to the peer with the the old link local address. As a
        result, the peer will converge with the new link local address as the
        next hop only after 3 min and 30 sec.

        ** RIPv6 on auto tunnels not supported. RFC not clear on the implementation 
        of RIPv6 over auto tunnels. 

        ** MIP6 is not supported in this release

     ______ CHANGELOG: ________________________________________________
        ** Updated the code for all the FAULTS raised during V6R AT
     __________________________________________________________________
    (______________________________END_________________________________)
     __________________________________________________________________
    |    RELEASE NUMBER  : PA5-4-0-0                                   |
    |    RELEASE DATE    : October 11 2004                             |
    '__________________________________________________________________'

     NOTE:
     PA tag is provided for this release for the following reasons:
        ** Completed Phase 1 ST of V6R Solution
        ** Updated the products baseline from V6R

     KNOWN ISSUES:
        ** Faults raised by PA team during PRE-PA phase are not closed
        ** MIP6 is not supported in this release

     ______ CHANGELOG: ________________________________________________
     **  Updated the code for all the FAULTS raised during V6R ST
     __________________________________________________________________
    (______________________________END_________________________________)
 
     __________________________________________________________________
    |    RELEASE NUMBER  : temp5-4-0-0                                 |
    |    RELEASE DATE    : August 06 2004                              |
    '__________________________________________________________________'

     NOTE:

     Temporary tag is provided for this release
     for the following reasons:
        ** ST is done as a part of V6R solution
        ** Latest IPv6 to be ported on ISS for DELTA

     ______ CHANGELOG: ________________________________________________
     **  Updated the directory structure for NETIPv6.
     **  NETIPv6 Implementation.
     **  RTM6 Implementation.
     **  Common TRIE2 Interface.
     **  Single Interface Table for both Tunnel and Physical interface.
     **  Implementation of ISATAP and GRE Tunnels.
     **  Standardization of the Interface with Tunnel Manager and CFA.
     **  Default Route Add and Delete CLI to be removed.
     **  IPv6 CLI, Route Add and DEL should support default route.
     **  ND Clearing the routes in INCOMPLETE state.
     **  Address related macros moved to future/inc/ipv6.h
     **  Updated Best Route Calculation Algorithm.
     **  Route Updation while interface disable and delete event.
     **  FS mib's routing table index - replaced next hop with interface index.
     **  Removal of direct RIPv6 reference from the IPv6 code.
     **  UINT1 casting for ND Timer removed.
     **  Row status implemented for Address table in FS mib.
     **  Hook provided to add/delete a multicast address for an interface.
     __________________________________________________________________
    (______________________________END_________________________________)
     __________________________________________________________________
    |    RELEASE NUMBER  : REL5-3-1-1                                  |
    |    RELEASE DATE    : February 23 2004                            |
    '__________________________________________________________________'

    ______ CHANGELOG: _________________________________________________
     **  Changed BOOLEAN to BOOL1. BOOLEAN is deprecated. 

      + ip6core/traceroute.c
     __________________________________________________________________
     **  Modified to convert timers in to ticks before comparison. 

      + ip6core/ip6pmtu.c       ip6core/nd6main.c
                                                            [RFC: 7463]
     __________________________________________________________________
     **  Fixed issues raised by PA on IPv6 eval copy & IPv6 CLI related
     **  issues. 

      + ip6rtr/nd6rtr.c         ip6core/fsip6low.c      ip6core/ip6if.c
      + ip6core/ip6proto.h      ip6core/ip6sys.h        ip6core/ip6addr.c
      + ip6core/ip6main.c       mld/mldmain.c           ip6core/ip6snmp.h
      + ip6host/nd6host.c
                                                            [RFC: 7784]
     __________________________________________________________________
    (______________________________END_________________________________)
     __________________________________________________________________
    |    RELEASE NUMBER  : REL5-3-1-0                                  |
    |    RELEASE DATE    : November 27 2003                            |
    '__________________________________________________________________'

     Known problems in this release:

     1. Warnings are present during compilation.
        However, it is not known to affect the functionality of the code.


    ______ CHANGELOG: _________________________________________________
     **  Added New API for getting cfa interface index. 

      + ip6core/ip6hli.c
                                                            [RFC: 7140]
     __________________________________________________________________
     **  Updated Ip6Shutdown () to avoid the crash. 

      + ip6core/ip6main.c
                                                            [RFC: 7140]
     __________________________________________________________________
     **  Added check to avoid the crash when IP6 is not initialised. 

      + ip6core/fsip6low.c
                                                            [RFC: 7140]
     __________________________________________________________________
     **  Added Deregister function. 

      + ip6core/ip6port.h
                                                            [RFC: 7140]
     __________________________________________________________________
     **  Added TUNNEL_WANTED switch to avoid undefined reference. 

      + ip6core/ip6if.c
                                                            [RFC: 7140]
     __________________________________________________________________
     **  Added extern declarations for Ip6Shutdown(). 

      + ip6core/ip6ext.h
                                                            [RFC: 7140]
     __________________________________________________________________
     **  Modified Nd6ProcessPrefixOption() to check the prefixlen above 128
     **  bits. 

      + ip6host/nd6host.c
                                                            [RFC: 7140]
     __________________________________________________________________
    (______________________________END_________________________________)
     __________________________________________________________________
    |    RELEASE NUMBER  : REL5-3-0-5                                  |
    |    RELEASE DATE    : October 07 2003                             |
    '__________________________________________________________________'

    ______ CHANGELOG: _________________________________________________
     **  Modified Ip6RcvRtChgNotification return value to INT4. 

      + ip6core/ip6hli.c
                                                            [RFC: 6515]
     __________________________________________________________________
     **  Removed function Ip6IfGetPseudoInterface. 

      + ip6core/ip6proto.h
                                                            [RFC: 6515]
     __________________________________________________________________
    (______________________________END_________________________________)
     __________________________________________________________________
    |    RELEASE NUMBER  : REL5-3-0-4                                  |
    |    RELEASE DATE    : September 30 2003                           |
    '__________________________________________________________________'

    ______ CHANGELOG: _________________________________________________
     **  Removed Diab compilation warnings. 

      + ip6core/icmp6.c         ip6core/ip6fwdtb.c      mip6/cmn/fsm6low.c
      + ip6core/stdip6low.c     ip6core/ip6fscli.c
                                                            [RFC: 6349]
     __________________________________________________________________
     **  Fixed :HDC 21687 To avoid crash when adding the same route with
     **  different prefix Len. 

      + ip6core/ip6fwdtb.c
                                                            [RFC: 6365]
     __________________________________________________________________
     **  Added Ip6Rip6StaticRouteAdd. 

      + ip6core/ip6hli.c        ip6core/ip6fscli.c
                                                            [RFC: 6365]
     __________________________________________________________________
     **  Added Prototype of Ip6Rip6StaticRouteAdd. 

      + ip6core/ip6proto.h
                                                            [RFC: 6365]
     __________________________________________________________________
     **  Removed Compilation errors when SLI_WANTED switch is disabled . 

      + mip6/cmn/mip6out.c      ip6core/ip6port.h       ip6core/ip6inc.h
      + ip6core/ip6pmtu.c
                                                            [RFC: 6421]
     __________________________________________________________________
     **  Updated for 6to4 interface changes 

      + ip6core/ip6cli.c        ip6core/fsip6low.c
                                                            [RFC: 6493]
     __________________________________________________________________
     **  Updated for re-initialising  Lan If Statistics 

      + ip6core/ip6if.c
                                                            [RFC: 6493]
     __________________________________________________________________
     **  Updated Send Router Solicitations in Nd6SendRouterSol() 

      + ip6host/nd6host.c
                                                            [RFC: 6493]
     __________________________________________________________________
     **  Added Check for validating Ipv6 PrefixLen. 

      + ip6core/ip6fscli.c
                                                            [RFC: 6493]
     __________________________________________________________________
     **  Modified to avoid crash when Ip6 delete a route for same
     **  destinations with different prefixlen. 

      + ip6core/ip6fwdtb.c
                                                            [RFC: 6505]

     __________________________________________________________________
    (______________________________END_________________________________)

     _________________________________________________________________
    |    RELEASE NUMBER  : REL5-3-0-3                                  |
    |    RELEASE DATE    : September 17 2003                           |
    '__________________________________________________________________'


    ______ CHANGELOG: _________________________________________________
     **  Added Support for configuring IPv6 default routes. 

      + ip6core/ip6fwdtb.c      ip6core/ip6.h           ip6core/ip6proto.h
      + ip6core/ip6rtcmds.def   ip6core/ip6cmds.def     ip6core/ip6rt.h
      + ip6core/ip6cliprot.h    ip6core/ip6cli.c        ip6host/nd6host.c
      + ip6core/ip6snif.c       ip6core/ip6fscli.c
                                                            [RFC: 6236]
     __________________________________________________________________
     **  Modified Ip6RoutePurgeEntry to handle 30,000 routes. 

      + ip6core/ip6fwdtb.c
                                                            [RFC: 6315]
     __________________________________________________________________
     **  Changed to notify RIP6 for local routes after DAD success. 

      + ip6core/ip6hli.c        ip6core/ip6addr.c
                                                            [RFC: 6315]
     __________________________________________________________________
     **  Added SNMP_2 Registration functions. 

      + mip6/cmn/mip6in.c
                                                            [RFC: 6315]
     __________________________________________________________________
     **  Updated snmp_2 wrapper files for mobile ipv6 support. 

      + Makefile.diab           Makefile
                                                            [RFC: 6315]
     __________________________________________________________________
     **  New Proto type added for local route notification to RIP6. 

      + ip6core/ip6proto.h
                                                            [RFC: 6315]
     __________________________________________________________________
     **  Included Interface Index in ip6RouteDel command. 

      + ip6core/ip6cmds.def     ip6core/ip6fscli.c
                                                            [RFC: 5395]
     __________________________________________________________________
     **  Modified IfIndex in Ip6RcvRtChgNotification. 

      + ip6core/ip6hli.c
                                                            [RFC: 5645]
     __________________________________________________________________
     **  Removed RIP6_MAX_ROUTE_ENTRIES macro. 

      + ip6core/ip6sys.h
                                                            [RFC: 5936]
     __________________________________________________________________
     **  Removed Incorrect Error Message in CLI Commands. 

      + ip6core/ip6cli.c
                                                            [RFC: 5936]
     __________________________________________________________________
     **  Added Error message when invalid prefix is given by the user. 

      + ip6core/ip6fscli.c
                                                            [RFC: 5936]
     __________________________________________________________________
     **  Modified RIP6_MAX_ROUTE_ENTRIES to IP6_MAX_ROUTE_ENTRIES. 

      + ip6core/stdip6low.c     ip6core/ip6main.c
                                                            [RFC: 5972]
     __________________________________________________________________
     **  Fixed Bug: while displaying std ipv6addr table. 

      + ip6core/stdip6low.c
                                                            [RFC: 5972]
     __________________________________________________________________
     **  Added Ip6GetAddrPrefixLen() prototype 

      + ip6core/ip6proto.h
                                                            [RFC: 5972]
     __________________________________________________________________
     **  Fixed Bug: To return valid ipv6 addr status. 

      + ip6rtr/ip6rsnif.c
                                                            [RFC: 5972]
     __________________________________________________________________
     **  Added Ip6GetAddrPrefixLen() 

      + ip6core/ip6snif.c
                                                            [RFC: 5972]
     __________________________________________________________________
     **  Modified RIP6_MAX_ROUTE_ENTRIES to IP6_MAX_ROUTE_ENTRIES macro. 

      + ip6core/stdip6low.c
                                                            [RFC: 5976]
     __________________________________________________________________
     **  Fixed :  Pkt_2_big_gen_mtu testcase problem 

      + ip6core/icmp6.c
                                                            [RFC: 6033]
     __________________________________________________________________
     **  Added rules for CLI and snmp_2 related files. 

      + Makefile.diab           Makefile
                                                            [RFC: 6070]
     __________________________________________________________________
     **  Modified for portability changes.Changed CFLAGS to C_FLAGS. 

      + Makefile
                                                            [RFC: 6073]
     __________________________________________________________________
     **  Modified Ip6 Route Table for RIP6. 

      + ip6core/ip6fwdtb.c      ip6core/ip6hli.c        ip6core/fsip6low.c
      + ip6core/ip6snmp.h       ip6core/ip6snif.c
                                                            [RFC: 6133]
     __________________________________________________________________
     ** Removed Compilation Error when MIP6 and IP6_HOST enabled.
     
      + Makefile
                                                            [RFC: 6148]
     __________________________________________________________________
    (______________________________END_________________________________)
    __________________________________________________________________
    |    RELEASE NUMBER  : REL5-3-0-2                                  |
    |    RELEASE DATE    : July 14 2003                                |
    '__________________________________________________________________'

    ______ CHANGELOG: _________________________________________________
     **  Handled icmp6 exceptions according to RFC 2463. 

      + ip6core/icmp6.c
                                                            [RFC: 5236]
     __________________________________________________________________
     **  Standardised macros ROUTE ADD/DEL/MODIFY. 

      + ip6core/ip6fwdtb.c
                                                            [RFC: 5236]
     __________________________________________________________________
     **  Provided function to check reachability of NextHop and check if a
     **  prefix belongs to one of our subnet. 

      + ip6core/ip6hli.c
                                                            [RFC: 5236]
     __________________________________________________________________
     **  Modified tLlToIp6Params to hold Dos Flag. 

      + ip6core/ip6sys.h
                                                            [RFC: 5236]
     __________________________________________________________________
     **  Modified to prevent SRC_LL option with NA msg. 

      + ip6core/nd6out.c
                                                            [RFC: 5236]
     __________________________________________________________________
     **  Moved ROUTE ADD/DEL/MODIFY to ipv6.h to share it between Routing
     **  Protocols. 

      + ip6core/ip6rt.h
                                                            [RFC: 5236]
     __________________________________________________________________
     **  Modified to handle icmp6 exceptions according to RFC 2463. 

      + ip6core/ip6main.c
                                                            [RFC: 5236]
     __________________________________________________________________
     **  Modified to check Mcast MAC Address. 

      + ip6core/ip6main.c
                                                            [RFC: 5272]
     __________________________________________________________________
     **  Modified to use link local address as the source if the
     **  destination is link local address in Udp6Send. 

      + ip6core/udp6.c
                                                            [RFC: 5279]
     __________________________________________________________________
     **  Modified SNMP_SUCCESS to IP6_SUCCESS 

      + ip6core/stdip6low.c
                                                            [RFC: 5309]
     __________________________________________________________________
     **  Modified Ip6GetIpv6AddrStatus() to return the addr status. 

      + ip6rtr/ip6rsnif.c
                                                            [RFC: 5309]
     __________________________________________________________________
     **  Removed compilation warnings. 

      + ip6core/ip6fscli.c
                                                            [RFC: 5309]
     __________________________________________________________________
     **  Added NULL check for the address in LL option. 

      + ip6core/nd6out.c
                                                            [RFC: 5365]
     __________________________________________________________________
     **  Removed 2nd Route Updates to RP's in Ip6AddrCreate. 

      + ip6core/ip6addr.c
                                                            [RFC: 5365]
     __________________________________________________________________
    (______________________________END_________________________________)
     __________________________________________________________________
    |    RELEASE NUMBER  : REL5-3-0-1                                  |
    |    RELEASE DATE    : May 21 2003                                 |
    '__________________________________________________________________'
 
    ...... ADDED:......................................................
    **  Added file to support for snmp_2.

      + ip6core/ip6fscli.c

    ______ CHANGELOG: _________________________________________________
     **  Updated for snmp_2 

      + Makefile
                                                            [RFC: 4555]
     __________________________________________________________________
     **  Modified to move cli related ip6 files to ip6 dir. 

      + Makefile                ip6core/ip6cmds.def
                                                            [RFC: 4991]
     __________________________________________________________________
    (______________________________END_________________________________)

PRODUCT NAME            : IP6

RELEASE NUMBER          : 5-3-0-0

RELEASE DATE            : April 17 2003

---------------------------------------------------
Known problems in this release:

o One warning specific to DIAB compiler is present in fsm6low.c 
o Support for multicast is not comprehensively addressed in mobile IPv6
  "draft-ietf-mobile-ipv6-17.txt". Hence there is a limitation in working
  together of Aricent MLD and Aricent MobileIPv6. Therefore both should
  not be enabled at same time.
o The current version of Aricent Home Agent can support only 250 tunnels at
  any time. Therefore only 250 mobile nodes can move to other foreign
  networks.

------------------------------------------
Internet Drafts supported in this release
------------------------------------------
AricentIPv6 5.3.0.0 supports the following RFCs. 

1.  2460
2.  2461
3.  2462
4.  2463
5.  2464
6.  1981
7.  2675
8.  2465
9.  2893
10. 2710
11. 2473
12. draft-ietf-mobileip-ipv6-17.txt

-------------------------------------------------------------
                    [END OF 5-3-0-0 CHANGES]

