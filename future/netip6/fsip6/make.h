#####################################################################
#    Copyright (C) 2006 Aricent Inc . All Rights Reserved
#    FILE NAME               ::  make.h                 
#    $Id: make.h,v 1.5 2016/03/18 13:16:44 siva Exp $
#
#####################################################################

###########################################################################
#               COMPILATION SWITCHES                                      #
###########################################################################

# This switch enables the needed protocols to get compiled with IP
PROTOCOL_OPNS =

# This is used to specify the compiler option
COMPILER_TYPE = -DGNU_CC -DANSI -DUNIX -DINCLUDE_IN_OSS

GLOBAL_OPNS = ${TRACE_OPNS} ${DEBUG_OPNS} ${PROTOCOL_OPNS} \
           ${COMPILER_TYPE} ${PACKING_OPNS}   \
           ${GENERAL_COMPILATION_SWITCHES} ${SYSTEM_COMPILATION_SWITCHES}
 
GLOBAL_OPNS += -DOPTIMIZED_HW_AUDIT_WANTED

ifeq (DPING6_LNXIP_WANTED, $(findstring DPING6_LNXIP_WANTED,$(SYSTEM_COMPILATION_SWITCHES)))
PING6_COMPILATION_SWITCHES += -USLI_WANTED -DBSDCOMP_SLI_WANTED \
                               -DIS_SLI_WRAPPER_MODULE
else
PING6_COMPILATION_SWITCHES = 
endif
############################################################################
#                         Directories                                      #
############################################################################

IP6_BASE_DIR     = ${BASE_DIR}/netip6/fsip6
IP6_CORE_DIR     = ${BASE_DIR}/netip6/fsip6/ip6core
IP6_CORE_DIR     = ${BASE_DIR}/netip6/fsip6/ip6core
IP6RTR_BASE_DIR  = ${IP6_BASE_DIR}/ip6rtr
IP6_OBJ_DIR      = ${IP6_BASE_DIR}/obj
MLD_DIR          = ${IP6_BASE_DIR}/mld
RTM6_DIR         = ${BASE_DIR}/netip6/rtm6
RTM6_SRCD        = ${BASE_DIR}/netip6/rtm6/src
RTM6_OBJD        = ${RTM6_DIR}/obj

IP6_MGMTD      = ${BASE_DIR}/netip6/ip6mgmt
IP6_MGMTINCD   = ${IP6_MGMTD}/inc

MIP6_CMN_DIR = ${IP6_BASE_DIR}/mip6
MIP6_DIR     = ${MIP6_CMN_DIR}/cmn
HA_DIR       = ${MIP6_CMN_DIR}/ha
MN_DIR       = ${MIP6_CMN_DIR}/mnode

IP6_CMN_INC_DIR = ${BASE_DIR}/netip6/fsip6/inc

IP6_LIB_INCD  = ${BASE_DIR}/util/ip6
IP6_LIBD  = ${BASE_DIR}/util/ip6
IP6_RTMINCD   = ${RTM6_DIR}/inc
TRIE2INCD     = ${BASE_DIR}/util/trie2/inc
IP6_NPAPI_DIR = ${NPAPI_INCLUDE_DIR}
RMAP_INCDIR   = ${BASE_DIR}/netip/routemap/inc

IP6_DPNDS     =   $(COMMON_DEPENDENCIES) \
                  ${BASE_DIR}/netip6/Makefile \
                  ${BASE_DIR}/netip6/make.h \
                  ${IP6_CORE_DIR}/ip6.h \
                  ${COMN_INCL_DIR}/ipv6.h \
                  ${IP6_CORE_DIR}/ip6inc.h \
                  ${IP6_CORE_DIR}/ip6sys.h \
                  ${IP6_CORE_DIR}/ip6if.h \
                  ${IP6_CORE_DIR}/ip6addr.h \
                  ${IP6_CORE_DIR}/ip6frag.h \
                  ${IP6_CORE_DIR}/icmp6.h \
                  ${IP6_CORE_DIR}/nd6.h \
                  ${IP6_CORE_DIR}/ip6rt.h \
                  ${IP6_CORE_DIR}/udp6.h \
                  ${IP6_CORE_DIR}/ping6.h \
                  ${IP6_CORE_DIR}/ip6ext.h \
                  ${IP6_CORE_DIR}/ip6proto.h \
                  ${IP6_CORE_DIR}/ip6pmtu.h \
                  $(IP6_CORE_DIR)/nd6red.h \
                  ${IP6_LIB_INCD}/ip6util.h \
                  ${IP6_MGMTINCD}/ip6snmp.h \
                  ${IP6_CORE_DIR}/ip6dbg.h \
                  ${IP6_MGMTINCD}/ip6ipvx.h

MIP6_DPNDS   = ${COMMON_DEPENDENCIES} \
               ${BASE_DIR}/netip6/Makefile \
               ${BASE_DIR}/netip6/make.h \
               ${IP6_CMN_INC_DIR}/mipv6.h\
               ${MIP6_DIR}/mobilecmn.h \
               ${MIP6_DIR}/mip6.h \
               ${MIP6_DIR}/hmagnt.h \
               ${MIP6_DIR}/fsm6con.h\
               ${MIP6_DIR}/fsm6low.h\
               ${MIP6_DIR}/fsm6ogi.h\
               ${MIP6_DIR}/bind.h \
               ${MIP6_DIR}/halist.h \
               ${MIP6_DIR}/mobilecmn.h \
               ${MIP6_DIR}/cnrrp.h\
               ${MIP6_DIR}/fsm6mid.h\
               ${MIP6_DIR}/fsmip6snmp.h

MN_DPNDS     = ${COMMON_DEPENDENCIES} \
               ${BASE_DIR}/netip6/Makefile \
               ${BASE_DIR}/netip6/make.h \
               ${MN_DIR}/mncmn.h \
               ${MN_DIR}/mnrrp.h \
               ${MIP6_DIR}/bulist.h \
               ${MN_DIR}/mnmvdet.h\
               ${MN_DIR}/mninc.h\
               ${MN_DIR}/mnproto.h\
               ${MN_DIR}/mndefns.h

HA_DPNDS     = ${COMMON_DEPENDENCIES} \
               ${BASE_DIR}/netip6/Makefile \
               ${BASE_DIR}/netip6/make.h \
               ${HA_DIR}/haproto.h\
               ${HA_DIR}/hminc.h

MLD_DPNDS    =    ${COMMON_DEPENDENCIES} \
                  ${BASE_DIR}/netip6/Makefile \
                  ${BASE_DIR}/netip6/make.h \
                  ${MLD_DIR}/mldinc.h \
                  ${MLD_DIR}/mldtypdfs.h \
                  ${MLD_DIR}/mldtrace.h \
                  ${MLD_DIR}/mlddefns.h \
                  ${MLD_DIR}/mldexterns.h \
                  ${MLD_DIR}/mldport.h \
                  ${MLD_DIR}/mldprotos.h\
                  ${MLD_DIR}/stdmlmdb.h \
                  ${MLD_DIR}/stdmllow.h \
                  ${MLD_DIR}/stdmlmid.h \
                  ${MLD_DIR}/stdmlogi.h \
                  ${MLD_DIR}/fsmldcon.h \
                  ${MLD_DIR}/fsmldmdb.h \
                  ${MLD_DIR}/fsmldlow.h \
                  ${MLD_DIR}/fsmldmid.h \
                  ${MLD_DIR}/fsmldogi.h

NETIPV6_DPNDS =   ${COMN_INCL_DIR}/rtm6.h
                  
RTM6_DPNDS = ${COMMON_DEPENDENCIES} \
             ${BASE_DIR}/netip6/Makefile \
             ${BASE_DIR}/netip6/make.h \
             ${COMN_INCL_DIR}/lr.h \
             ${COMN_INCL_DIR}/ipv6.h \
             ${COMN_INCL_DIR}/rtm6.h \
             ${IP6_RTMINCD}/rtm6port.h \
             ${IP6_RTMINCD}/rtm6rmap.h 

ifeq (DNPAPI_WANTED, $(findstring DNPAPI_WANTED,$(SYSTEM_COMPILATION_SWITCHES)))
IP6_DPNDS  += $(IP6_NPAPI_DIR)/ip6np.h
RTM6_DPNDS += $(IP6_NPAPI_DIR)/ip6np.h
endif
########################## END OF MAKE.H ####################################

