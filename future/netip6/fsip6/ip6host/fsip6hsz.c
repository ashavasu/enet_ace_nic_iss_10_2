#define _IP6HOSTSZ_C
#include "ip6inc.h"
INT4
Ip6hostSizingMemCreateMemPools ()
{
    INT4                i4RetVal;
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < IP6HOST_MAX_SIZING_ID; i4SizingId++)
    {
        i4RetVal =
            MemCreateMemPool (FsIP6HOSTSizingParams[i4SizingId].u4StructSize,
                              FsIP6HOSTSizingParams[i4SizingId].
                              u4PreAllocatedUnits, MEM_DEFAULT_MEMORY_TYPE,
                              &(IP6HOSTMemPoolIds[i4SizingId]));
        if (i4RetVal == (INT4) MEM_FAILURE)
        {
            Ip6hostSizingMemDeleteMemPools ();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

INT4
Ip6hostSzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, FsIP6HOSTSizingParams);
    return OSIX_SUCCESS;
}

VOID
Ip6hostSizingMemDeleteMemPools ()
{
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < IP6HOST_MAX_SIZING_ID; i4SizingId++)
    {
        if (IP6HOSTMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (IP6HOSTMemPoolIds[i4SizingId]);
            IP6HOSTMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
