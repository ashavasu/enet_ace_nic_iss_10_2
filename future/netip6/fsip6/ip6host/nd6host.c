/*
 *----------------------------------------------------------------------------- 
 *    FILE NAME                    :    nd6host.c
 *
 *    PRINCIPAL AUTHOR             :    Jayabharathi R.
 *
 *    SUBSYSTEM NAME               :    IPv6
 *
 *    MODULE NAME                  :    IP6 ND Submodule for HOST
 *
 *    LANGUAGE                     :    C
 *
 *    TARGET ENVIRONMENT           :    UNIX
 *
 *    DATE OF FIRST RELEASE        :    DDD-MM-1996
 *
 *    DESCRIPTION                  :    This file contains routines for 
 *                                      processing Neighbor discovery messages 
 *                                      of an IPV6-host.
 *
 *   $Id: nd6host.c,v 1.22 2017/12/19 13:41:55 siva Exp $
 *----------------------------------------------------------------------------- 
 */

#include "ip6inc.h"

#ifdef MN_WANTED
#include "mnmvdet.h"
extern tMNMvDetInfo **MoveDetInfo;
#endif
PRIVATE INT4        Nd6FillRouterSol
PROTO ((UINT1 u1Flag, tIp6If * pIf6, tCRU_BUF_CHAIN_HEADER * pBuf));

tTMO_SLL            Nd6RouteList;
INT4                gi4ND6RouteId;    /* Pool Id for ND routes */

PRIVATE INT4        Nd6DelRtEntryInCxt (tIp6Cxt *, tIp6Addr *,
                                        UINT1, tIp6Addr *);
PRIVATE tNd6RtEntry *ND6GetRouteEntry (tIp6Addr *, UINT1, tIp6Addr *, UINT4);

PRIVATE INT4        Nd6AddRtEntry (tIp6Addr * pDstAddr, UINT1 u1Prefixlen,
                                   tIp6Addr * pNextHop, UINT4 u4IfIndex,
                                   UINT2 u2LifeTime);

/*****************************************************************************
DESCRIPTION         Processes the received Router Advertisement 
                    Messages. 
                    
                    pIp6        -   Pointer to the IPv6 header of
                                     the received packet
                    u2Len       -   Length of the ND payload in the
                                     received IPv6 packet
                    pBuf        -   Pointer to the received
                                     IPv6 packet

OUTPUTS             None

RETURNS             IP6_SUCCESS      - Sending of RA succeeds
                    IP6_FAILURE  - Processing of RA fails 
****************************************************************************/

INT4
Nd6RcvRoutAdv (tIp6Hdr * pIp6, tIp6If * pIf6, UINT2 u2Len,
               tCRU_BUF_CHAIN_HEADER * pBuf)
{
    INT4                i4RetVal = IP6_FAILURE;
    UINT1               u1Copy = 1;
    UINT4               u4Nd6Offset = 0;
    UINT4               u4ExtnsLen = 0;
    tNd6RoutAdv         nd6RAdv, *pNd6RAdv;
    tIp6Cxt            *pIp6Cxt = pIf6->pIp6Cxt;
#ifdef MN_WANTED
    tHaListEntry       *pHaEntry;
    UINT1               u1HAInfoOpt = 0;
#endif

    /* Extract RA Message from the buffer */
    u4Nd6Offset = IP6_BUF_READ_OFFSET (pBuf) - sizeof (tIcmp6PktHdr);
    IP6_BUF_READ_OFFSET (pBuf) -= sizeof (tIcmp6PktHdr);

    if ((pNd6RAdv = (tNd6RoutAdv *) Ip6BufRead
         (pBuf, (UINT1 *) &nd6RAdv, u4Nd6Offset,
          sizeof (tNd6RoutAdv), u1Copy)) == NULL)
    {
        IP6_TRC_ARG2 (pIp6Cxt->u4ContextId, ND6_MOD_TRC, BUFFER_TRC, ND6_NAME,
                      "ND6: Nd6RcvRoutAdv:Receive RA - BufRead Failed! \
                       BufPtr %p Offset %d\n", pBuf, u4Nd6Offset);
        IP6_TRC_ARG3 (pIp6Cxt->u4ContextId, ND6_MOD_TRC, BUFFER_TRC, IP6_NAME,
                      "%s buffer error occurred, Type = %d Module = 0x%X "
                      "Bufptr = %p\n", ERROR_FATAL_STR, ERR_BUF_READ, pBuf);
        return (IP6_FAILURE);
    }

    /* Validate the RA message */
    if (Nd6ValidateRoutAdvInCxt (pIp6Cxt, pIp6, pNd6RAdv, u2Len) == IP6_FAILURE)
    {
        /* Increment the count on ICMP bad packets received */
        ICMP6_INC_IN_ERRS (pIp6Cxt->Icmp6Stats);
        return (IP6_FAILURE);
    }

#ifdef MN_WANTED
    if ((i4RetVal = MNProcessRoutAdv (&pIp6->srcAddr, pIf6)) == TERMINATE)
    {
        IP6_TRC_ARG (pIp6Cxt->u4ContextId, ND6_MOD_TRC, ALL_FAILURE_TRC,
                     ND6_NAME, "Nd6RcvRoutAdv:RA process terminated ...\n");
        return IP6_FAILURE;
    }
    else if (i4RetVal == IP6_FAILURE)
    {
        IP6_TRC_ARG (pIp6Cxt->u4ContextId, ND6_MOD_TRC, ALL_FAILURE_TRC,
                     ND6_NAME,
                     "RA process terminated - Could not get RA type...\n");
        return IP6_FAILURE;
    }
#endif
    i4RetVal = Nd6ProcessRouterAdv (pIf6, pIp6, pNd6RAdv);
#ifdef MN_WANTED
    if (!
        (pNd6RAdv->
         u1StfulFlag & ND6_H_BIT_STFUL_FLAG /* if  H Bit is not set */ ))
    {
        if ((pHaEntry = Mip6GetHaEntry (&pIp6->srcAddr, pIf6->u4Index)) != NULL)
        {
            /* Delete This Entry From the HA List */
            Mip6DeleteHaEntry (&pIp6->srcAddr, pIf6->u4Index);
        }
    }
    else                        /* the entry is not present already */
    {
        /*
         * Create a New HA List Entry passing this pIp6->pSrcAddr,
         * interface index, set preference value to zero here
         * initially- but when  HA info Option is present, the
         * value for preference would be updated from that.
         */

        /*
         * hence, an entry would be created here in the HA List.
         * The initial contents of the entry are u4IfIndex &
         * ip6HaLlAddr.  u1Pref would be updated if  HA information
         * option is present in the function Nd6ProcessRoutAdvExtns( )
         */

        /* Create this entry */
        if (pNd6RAdv->u2Lifetime != 0)
        {
            Mip6UpdateHaEntry (&pIp6->srcAddr, pIf6->u4Index, 0,
                               (UINT2) OSIX_NTOHS (pNd6RAdv->u2Lifetime));
        }
    }

#endif

    /* Process the Extension Options */
    u4ExtnsLen = u2Len - sizeof (tNd6RoutAdv);
#ifndef MIP6_WANTED
    if (Nd6ProcessRoutAdvExtns (pIp6, pIf6, u4ExtnsLen, pBuf, 0, 0,
                                NULL, NULL) == IP6_FAILURE)
#else
    if (Nd6ProcessRoutAdvExtns
        (pIp6, pIf6, u4ExtnsLen, pBuf, 0, pNd6RAdv->u2Lifetime,
         &u1HAInfoOpt, NULL) == IP6_FAILURE)
#endif
    {
        return (IP6_FAILURE);
    }
#ifdef MN_WANTED
    else if (pNd6RAdv->
             u1StfulFlag & ND6_H_BIT_STFUL_FLAG /* if  H Bit is set */ )
    {
        if ((pHaEntry = Mip6GetHaEntry (&pIp6->srcAddr, pIf6->u4Index)) != NULL)
        {
            if ((pNd6RAdv->u2Lifetime == 0)
                && (u1HAInfoOpt == HOME_AGT_INFO_NO_OPT))
            {
                /* Delete this entry from HA LIST   */
                Mip6DeleteHaEntry (&pIp6->srcAddr, pIf6->u4Index);
            }
        }
    }
#endif
    /* RA has been received, So need not send any more RSs if RS sent 
     * is more or equal to 1.
     */
    pIf6->u1RsFlag = IP6_IF_NO_RS_SEND;

    if (Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE, ND6_MODULE) !=
        IP6_SUCCESS)
    {
        IP6_TRC_ARG (pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                     BUFFER_TRC | ALL_FAILURE_TRC, ND6_NAME,
                     "ND6: Nd6RcvRoutAdv:Rcv RA - BufRelease Failed\n");
        IP6_TRC_ARG3 (pIp6Cxt->u4ContextId, ND6_MOD_TRC, BUFFER_TRC, IP6_NAME,
                      "%s buffer error occurred, Type = %d Module = 0x%X Bufptr = %p ",
                      ERROR_FATAL_STR, ERR_BUF_RELEASE, pBuf);
    }
    return (IP6_SUCCESS);

}                                /*end of Nd6RcvRoutAdv */

/*****************************************************************************
DESCRIPTION         This routine validates the received RA message
                    Messages. 
                    
                    pIp6        -   Pointer to the IPv6 header of
                                     the received packet
                    pNd6RAdv    -   Pointer to the Router Adv.
                    u2Len       -   Length of the ND payload in the
                                     received IPv6 packet

OUTPUTS             None

RETURNS             IP6_SUCCESS      - if Validation of RA succeeds
                    IP6_FAILURE  - if Validation of RA fails 
****************************************************************************/
INT4
Nd6ValidateRoutAdvInCxt (tIp6Cxt * pIp6Cxt, tIp6Hdr * pIp6,
                         tNd6RoutAdv * pNd6RAdv, UINT2 u2Len)
{
    tIcmp6PktHdr        icmp6Hdr;

    icmp6Hdr = pNd6RAdv->icmp6Hdr;

    /* Source should be a link local address for hosts to 
     * identify a router uniquely
     */

    if (!(IS_ADDR_LLOCAL (pIp6->srcAddr)))
    {
        IP6_TRC_ARG1 (pIp6Cxt->u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC, ND6_NAME, "ND6:Nd6ValidateRoutAdv: Validation of ND MsgHdr Failed -\
                      Incorrect source address %s\n",
                      Ip6PrintAddr (&(pIp6->srcAddr)));
        return IP6_FAILURE;
    }

    if (Nd6ValidateMsgHdrInCxt (pIp6Cxt, pIp6, &icmp6Hdr) == IP6_FAILURE)
    {
        return (IP6_FAILURE);
    }

    /* ICMP Length MUST be 16 or more octets */
    if (u2Len < sizeof (tNd6RoutAdv))
    {
        IP6_TRC_ARG1 (pIp6Cxt->u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC | ALL_FAILURE_TRC, ND6_NAME, "ND6: Nd6ValidateRoutAdv:Validation of RA Failed - \
Incorrect Len %d\n", u2Len);
        return (IP6_FAILURE);
    }

    return (IP6_SUCCESS);

}                                /*end of Nd6ValidateRoutAdv */

/*****************************************************************************
DESCRIPTION         This routine processes the RA Extension Options and acts
                    accordingly
                    
                    pIp6        -   Pointer to the IPv6 header of
                                     the received packet
                    u2ExtnsLen  -   Length of the ND extentions in the
                                     received IPv6 packet
                    pBuf        -   Pointer to the received
                                     IPv6 packet

OUTPUTS             None

RETURNS             IP6_SUCCESS      - if Validation of RA succeeds
                    IP6_FAILURE  - if Validation of RA fails 
****************************************************************************/
INT4
Nd6ProcessRoutAdvExtns (tIp6Hdr * pIp6, tIp6If * pIf6, UINT4 u4ExtnsLen,
                        tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 u1PrefAdvFlag,
                        UINT2 u2RtrLife, UINT1 *pu1HaInfoPresent, NULL)
/* u1PrefAdvFlag - when set called for processing extentions when a prefix adv is recd.*/
{
    UINT1               u1Copy = 0;
    UINT4               u4ExtOffset = 0;
    tNd6ExtHdr         *pNd6Ext;
    tNd6MtuExt          nd6Mtu, *p_nd6_mtu;
    tNd6PrefixExt       nd6Prefix, *pNd6Prefix;
    tCgaOptions         CgaOptions;
    tUtlSysPreciseTime  SysPreciseTime;
    tNd6CacheEntry     *pNd6cEntry = NULL;
    UINT4               u4TSNew = 0;
    UINT4               u4TempOffset = 0;
    UINT4               u4SeNDOptCount = 0;
    UINT4               u4SeNDCountCheck = 0;
    UINT1               au1PubKey[IP6_SEND_CGA_BYTES];
#ifdef MN_WANTED
    tNd6HaInfo         *pNd6HaInfo;
    tNd6HaInfo          nd6HAInfo;
#endif
    UNUSED_PARAM (u1PrefAdvFlag);
    UNUSED_PARAM (u2RtrLife);
    UNUSED_PARAM (pu1HaInfoPresent);

    MEMSET (&CgaOptions, 0, sizeof (tCgaOptions));
    MEMSET (au1PubKey, 0, IP6_SEND_CGA_BYTES);
    MEMSET (&SysPreciseTime, 0, sizeof (tUtlSysPreciseTime));

    u4TempOffset = IP6_BUF_READ_OFFSET (pBuf) - sizeof (tNd6RoutAdv);
    CgaOptions.pu1DerPubKey = au1PubKey;

    if ((IS_ALL_NODES_MULTI (pIp6->dstAddr)))
    {
        u4SeNDCountCheck = ND6_SEND_UNSOL_OPT_COUNT;
    }
    else
    {
        u4SeNDCountCheck = ND6_SEND_SOL_OPT_COUNT;
    }

    /* Extract the Extension Options */
    while (u4ExtnsLen)
    {
        if ((pNd6Ext = Nd6ExtractExtnHdrInCxt (pIf6->pIp6Cxt, pBuf)) == NULL)
        {
            return (IP6_FAILURE);
        }
        u4ExtOffset = IP6_BUF_READ_OFFSET (pBuf) - IP6_TYPE_LEN_BYTES;
        IP6_BUF_READ_OFFSET (pBuf) = u4ExtOffset;

        switch (pNd6Ext->u1Type)
        {

            case ND6_MTU_EXT:
            {
                if ((u4ExtnsLen < (UINT4) pNd6Ext->u1Len * 8) ||
                    (pNd6Ext->u1Len != ND6_MTU_EXT_LEN))

                {
                    IP6_TRC_ARG1 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                                  DATA_PATH_TRC, ND6_NAME,
                                  "ND6: Nd6ProcessRoutAdvExtns:NS SRC LLA "
                                  "Extn - Invalid Len %d\n", pNd6Ext->u1Len);
                    return (IP6_FAILURE);
                }

                if ((p_nd6_mtu = (tNd6MtuExt *) Ip6BufRead
                     (pBuf, (UINT1 *) &nd6Mtu, u4ExtOffset,
                      sizeof (tNd6MtuExt), u1Copy)) == NULL)
                {
                    IP6_TRC_ARG2 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                                  BUFFER_TRC, ND6_NAME,
                                  "ND6: Nd6ProcessRoutAdvExtns:Process NS Extn"
                                  " - BufRead Failed! BufPtr %p Offset %d\n",
                                  pBuf, u4ExtOffset);
                    IP6_TRC_ARG3 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                                  BUFFER_TRC, IP6_NAME,
                                  "%s buffer error occurred, Type = %d Module ="
                                  " 0x%X Bufptr = %p ", ERROR_FATAL_STR,
                                  ERR_BUF_READ, pBuf);
                    return (IP6_FAILURE);
                }

                if (HTONL (p_nd6_mtu->u4Mtu) < IP6_MIN_MTU)
                {

                    IP6_TRC_ARG1 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                                  DATA_PATH_TRC, ND6_NAME,
                                  "ND6: Nd6ProcessRoutAdvExtns:RA MTU Extn - "
                                  "Invalid MTU %d\n", p_nd6_mtu->u4Mtu);

                    return (IP6_FAILURE);

                }
                /* Copying the MTU from Router advertisement */

#ifdef MN_WANTED
                if (!(u1PrefAdvFlag))
                {
                    /* MTU option included with prefix adv does not 
                     * have any meaning. So other validity checks can
                     * be done. MTU need not be copied*/
#endif
                    if (pIf6->u4Mtu > NTOHL (p_nd6_mtu->u4Mtu))
                    {
                        pIf6->u4Mtu = NTOHL (p_nd6_mtu->u4Mtu);
                    }
#ifdef MN_WANTED
                }
#endif
                u4ExtnsLen -= sizeof (tNd6MtuExt);
            }
                break;
            case ND6_PREFIX_EXT:

                if ((u4ExtnsLen < (UINT4) pNd6Ext->u1Len * 8) ||
                    (pNd6Ext->u1Len != ND6_PREFIX_EXT_LEN))

                {
                    IP6_TRC_ARG1 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                                  DATA_PATH_TRC, ND6_NAME,
                                  "ND6: Nd6ProcessRoutAdvExtns:RS PREFIX Extn "
                                  "- Invalid Len %d\n", pNd6Ext->u1Len);
                    return (IP6_FAILURE);
                }

                if ((pNd6Prefix = (tNd6PrefixExt *) Ip6BufRead
                     (pBuf, (UINT1 *) &nd6Prefix, u4ExtOffset,
                      sizeof (tNd6PrefixExt), u1Copy)) == NULL)
                {
                    IP6_TRC_ARG2 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                                  BUFFER_TRC, ND6_NAME,
                                  "ND6: Nd6ProcessRoutAdvExtns:Process RS Extn "
                                  "- BufRead Failed!BufPtr %p Offset%d\n", pBuf,
                                  u4ExtOffset);
                    IP6_TRC_ARG3 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                                  BUFFER_TRC, IP6_NAME,
                                  "%s buffer error occurred, Type = %d Module ="
                                  " 0x%X Bufptr = %p ", ERROR_FATAL_STR,
                                  ERR_BUF_READ, pBuf);
                    return (IP6_FAILURE);
                }

#ifdef MIP6_WANTED
                if (u1PrefAdvFlag)
                {
                    if (MNProcessPrefixOption (pNd6Prefix, pIp6, pIf6) ==
                        IP6_FAILURE)
                        return IP6_FAILURE;
                }
                else
#endif
                if (Nd6ProcessPrefixOption (pNd6Prefix, pIf6, NULL) ==
                        IP6_FAILURE)
                {
                    return IP6_FAILURE;
                }
#ifdef MIP6_WANTED
                if (Mip6AddGlobAddrHAList (pNd6Prefix, pIf6, pIp6)
                    == IP6_FAILURE)
                {
                    return IP6_FAILURE;
                }
#endif
                u4ExtnsLen -= sizeof (tNd6PrefixExt);
                break;

            case ND6_SRC_LLA_EXT:

                if (Nd6ProcessSLLAOption (u4ExtnsLen, pNd6Ext, pIp6, pIf6, pBuf)
                    == IP6_FAILURE)
                {
                    IP6_TRC_ARG (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                                 DATA_PATH_TRC, ND6_NAME,
                                 "ND6: Nd6ProcessRoutAdvExtns:Validation of NS "
                                 "Failed - Invalid SLLA \n");
                    return IP6_FAILURE;
                }

                u4ExtnsLen -= sizeof (tNd6AddrExt);
                break;
            case ND6_ADV_INTERVAL_OPT:
#ifdef MN_WANTED

                if (Nd6ProcessAdvIntervalOpt (pBuf,
                                              IP6_BUF_READ_OFFSET (pBuf)) ==
                    IP6_FAILURE)
                {
                    IP6_TRC_ARG (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                                 DATA_PATH_TRC, ND6_NAME,
                                 "ND6: Nd6ProcessRoutAdvExtns:Invalid Advertisement Interval Option \n");
                    return IP6_FAILURE;
                }
#endif
                u4ExtnsLen -= sizeof (tNd6AdvInterval);

                break;
            case HA_INFO_OPTION:
#ifdef MN_WANTED
                if ((u4ExtnsLen < (UINT4) (pNd6Ext->u1Len * 8))
                    || (pNd6Ext->u1Len != (UINT1) ND6_HA_INFO_OPTION_LEN))
                {
                    return (IP6_FAILURE);
                }

                if ((pNd6HaInfo = (tNd6HaInfo *) Ip6BufRead (pBuf,
                                                             (UINT1 *)
                                                             &nd6HAInfo,
                                                             u4ExtOffset,
                                                             sizeof
                                                             (tNd6HaInfo),
                                                             u1Copy)) == NULL)
                {
                    return (IP6_FAILURE);
                }

                /* If the RA Contains a home agent Information option,
                 * then the lifetime is taken from the HA lifetime
                 * field in the option
                 */

                /*
                 * if the link-local addr of the home agent
                 * sending this RA is already present in the
                 * receiving home agent's HA List, Reset its
                 * lifetime & preference to the values determined above
                 */

                Mip6ProcessHAInfoOpt (pIf6, pIp6, pNd6HaInfo);
                *pu1HaInfoPresent = HOME_AGT_INFO_OPT;
#endif
                u4ExtnsLen -= sizeof (tNd6HaInfo);
                break;

            case ND6_SEND_CGA_OPT:

                if (IP6_TRUE == IF_SEND_ENABLED (pIf6))
                {
                    if (Nd6SeNDProcessCgaOption (pIf6, pIp6, pBuf,
                                                 pNd6Ext->u1Len,
                                                 &CgaOptions) == IP6_FAILURE)
                    {
                        IP6_TRC_ARG (pIf6->pIp6Cxt->u4ContextId,
                                     ND6_MOD_TRC, BUFFER_TRC,
                                     ND6_NAME, "Nd6ProcessRoutAdvExtns:"
                                     " Cga processing failed\n");
                        return IP6_FAILURE;
                    }
                    u4SeNDOptCount++;
                }
                /* extension length is in unit of 8 octets */
                u4ExtnsLen -= (pNd6Ext->u1Len * BYTES_IN_OCTECT);
                break;

            case ND6_SEND_TIMESTAMP_OPT:

                if (IP6_TRUE == IF_SEND_ENABLED (pIf6))
                {
                    if (Nd6SeNDProcessTSOption (pIf6, pIp6, pBuf,
                                                &u4TSNew) == IP6_FAILURE)
                    {
                        IP6_TRC_ARG (pIf6->pIp6Cxt->u4ContextId,
                                     ND6_MOD_TRC, BUFFER_TRC,
                                     ND6_NAME, "Nd6ProcessRoutAdvExtns:"
                                     " Timestamp processing failed\n");
                        return IP6_FAILURE;
                    }
                    u4SeNDOptCount++;
                }
                /* extension length is in unit of 8 octets */
                u4ExtnsLen -= (pNd6Ext->u1Len * BYTES_IN_OCTECT);
                break;

            case ND6_SEND_NONCE_OPT:

                if (IP6_TRUE == IF_SEND_ENABLED (pIf6))
                {
                    if (Nd6SeNDProcessNonceOption (pIf6, pIp6, pBuf,
                                                   NULL) == IP6_FAILURE)
                    {

                        IP6_TRC_ARG (pIf6->pIp6Cxt->u4ContextId,
                                     ND6_MOD_TRC, BUFFER_TRC,
                                     ND6_NAME, "Nd6ProcessRoutAdvExtns:"
                                     " Nonce processing failed\n");
                        return IP6_FAILURE;
                    }
                    u4SeNDOptCount++;
                }
                /* extension length is in unit of 8 octets */
                u4ExtnsLen -= (pNd6Ext->u1Len * BYTES_IN_OCTECT);
                break;

            case ND6_SEND_RSA_SIGN_OPT:

                if (IP6_TRUE == IF_SEND_ENABLED (pIf6))
                {
                    if (Nd6SeNDProcessRsaOption (pIf6, pIp6, pBuf,
                                                 pNd6Ext->u1Len, u4TempOffset,
                                                 &CgaOptions) == IP6_FAILURE)
                    {
                        IP6_TRC_ARG (pIf6->pIp6Cxt->u4ContextId,
                                     ND6_MOD_TRC, BUFFER_TRC,
                                     ND6_NAME, "Nd6ProcessRoutAdvExtns:"
                                     " RSA processing failed\n");
                        return IP6_FAILURE;
                    }
                    u4SeNDOptCount++;
                }
                /* extension length is in unit of 8 octets */
                u4ExtnsLen -= (pNd6Ext->u1Len * BYTES_IN_OCTECT);
                break;

            default:
                /* Any other options should just be ignored  */
                return IP6_SUCCESS;
        }
    }

    /*
     * If SeND is enabled in full secure mode then all the four options
     * should present, and in mixed mode either all options should present
     * or no SeND options should present in the packet.
     * if it is not then discard the packet
     */
    if (((ND6_SECURE_ENABLE == pIf6->u1SeNDStatus)
         && u4SeNDOptCount != u4SeNDCountCheck)
        || ((ND6_SECURE_MIXED == pIf6->u1SeNDStatus)
            && ((u4SeNDOptCount != 0) || (u4SeNDOptCount != u4SeNDCountCheck))))
    {
        (IP6_IF_STATS (pIf6))->u4NdSecureInvalidPkts++;
        return (IP6_FAILURE);
    }

    return (IP6_SUCCESS);

}                                /*end of Nd6ProcessRoutAdvExtns */

/******************************************************************************
DESCRIPTION         This routine processes the RA and acts accordingly.
                    
INPUTS              pIf6        -   Ip6 interface over which this RA is received

                    pIp6        -   Pointer to the IPv6 header of
                                     the received packet

                    pBuf        -   Pointer to the received
                                     IPv6 packet

OUTPUTS             None

RETURNS             IP6_SUCCESS      - if Validation of RA succeeds
                    IP6_FAILURE  - if Validation of RA fails 

*******************************************************************************/
INT4
Nd6ProcessRouterAdv (tIp6If * pIf6, tIp6Hdr * pIp6, tNd6RoutAdv * pNd6RAdv)
{
    UINT4               u4ReachTimeRcd = NTOHL (pNd6RAdv->u4ReachTime);
    UINT2               u2LifeTime = NTOHS (pNd6RAdv->u2Lifetime);

    if (!IS_ADDR_UNSPECIFIED (pIp6->srcAddr))
    {
        /* Source address of this RA is not an unspecified address ,
         * So it can be added in the default router list.
         */

        Nd6AddDefRouterList (pIf6, &pIp6->srcAddr, u2LifeTime);

    }
    if (pNd6RAdv->u1HopLmt != 0)
    {
        /* Update the current hop limit to the received value . */

        pIf6->u1Hoplmt = pNd6RAdv->u1HopLmt;
    }

    /*Recd value is in msec */
    u4ReachTimeRcd = NTOHL (pNd6RAdv->u4ReachTime);

    if ((u4ReachTimeRcd != 0) && (pIf6->u4Reachtime != u4ReachTimeRcd))
    {
        /* Update base reachable time and recompute the 
         * new random reachable timer value */
        Nd6CalNewReachTime (pIf6, u4ReachTimeRcd);
    }

    if ((NTOHL (pNd6RAdv->u4RetransTime)) != 0)
    {
        pIf6->u4Rettime = NTOHL (pNd6RAdv->u4RetransTime);
    }

    return IP6_SUCCESS;

}

/******************************************************************************
DESCRIPTION         This routine adds the source address of RA to default 
                    router list based on its life time.
                    
INPUTS              pIf6        -   Ip6 interface over which this RA is received

                    pSrcAddr        Pointer to the source address of the router
                                    from which Advertisement has been received.

                    u2LifeTime  -   Life time advertised by the router.

OUTPUTS             DefRtList

RETURNS             Void. 
*******************************************************************************/
VOID
Nd6AddDefRouterList (tIp6If * pIf6, tIp6Addr * pSrcAddr, UINT2 u2Lifetime)
{
    tNd6RtEntry        *pNd6RtEntry = NULL;
    tNetIpv6RtInfo      NetIpv6RtInfo;
    tIp6Addr            IpAddr;
    INT4                i4Status = 0;
    UINT1               u1DefRtCnt = 0;

    MEMSET (&IpAddr, 0, sizeof (tIp6Addr));

    /* Default route is learnt via Neighbor Discovery protocol. */
    pNd6RtEntry = ND6GetRouteEntry (&IpAddr, 0, pSrcAddr, 0);

    if (u2Lifetime == 0)
    {
        if (pNd6RtEntry != NULL)
        {
            Nd6TimeOutDefRoute (pNd6RtEntry);
        }
        return;
    }

    /* ND route for the given next hop doesnot exist */
    if (pNd6RtEntry == NULL)
    {
        /* If max. no of ND routes are learnt, purge the stalest route */
        u1DefRtCnt = TMO_SLL_Count (&Nd6RouteList);
        if (u1DefRtCnt ==
            FsIP6HOSTSizingParams[MAX_IP6HOST_ND6_ROUTES_SIZING_ID].
            u4PreAllocatedUnits)
        {
            Nd6PurgeStalestDefRt ();
        }

        if (Nd6AddRtEntry (&IpAddr, 0, pSrcAddr,
                           pIf6->u4Index, u2Lifetime) == IP6_FAILURE)
        {
            IP6_TRC_ARG (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                         ALL_FAILURE_TRC, ND6_NAME,
                         "Nd6AddDefRouterList: Adding a new ND Route Entry"
                         " FAILED\n");
            return;
        }

        /* Add the Default route */
        MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));
        MEMCPY (&NetIpv6RtInfo.NextHop, pSrcAddr, sizeof (tIp6Addr));
        NetIpv6RtInfo.u4Index = pIf6->u4Index;
        /* For Default route metric is always 0 */
        NetIpv6RtInfo.u4Metric = 0;
        NetIpv6RtInfo.i1Proto = IP6_NDISC_PROTOID;
        NetIpv6RtInfo.i1DefRtrFlag = IP6_DEF_RTR_FLAG;
        NetIpv6RtInfo.i1Type = INDIRECT;
        NetIpv6RtInfo.u4RouteTag = 0;
        NetIpv6RtInfo.u4RowStatus = IP6FWD_ACTIVE;
        NetIpv6RtInfo.u2ChgBit = IP6_BIT_ALL;
        NetIpv6RtInfo.u4ContextId = pIf6->pIp6Cxt->u4ContextId;

        IP6_TASK_UNLOCK ();
        i4Status = Rtm6ApiIpv6LeakRoute (NETIPV6_ADD_ROUTE, &NetIpv6RtInfo);
        IP6_TASK_LOCK ();

        if (i4Status == RTM6_FAILURE)
        {
            Nd6DelRtEntryInCxt (pIf6->pIp6Cxt, &IpAddr, 0, pSrcAddr);
            return;
        }

        /* Initiate address resolution to determine link layer address
         * of this default router.
         */
#ifdef TUNNEL_WANTED
        if (pIf6->u1IfType != IP6_TUNNEL_INTERFACE_TYPE)
        {
            Nd6Resolve (NULL, pIf6, pSrcAddr, NULL);
        }
#else
        Nd6Resolve (NULL, pIf6, pSrcAddr, NULL);
#endif

        return;
    }

    /* ND6 route already available */
    /* Update  the life time of existing entry */
    Ip6TmrRestart (pIf6->pIp6Cxt->u4ContextId,
                   IP6_DEF_RTR_LIFE_TIMER_ID, gIp6GblInfo.Ip6TimerListId,
                   (tTmrAppTimer *) & pNd6RtEntry->RtEntryTimer.
                   appTimer, u2Lifetime);

    return;
}

/******************************************************************************
DESCRIPTION         This routine deletes the default router from the list.
                    
INPUTS              *pDefRtEntry - The default route entry to be deleted.
 
OUTPUTS             DefRtList

RETURNS             Void. 
*******************************************************************************/
VOID
Nd6TimeOutDefRoute (tNd6RtEntry * pNd6RtEntry)
{
    tNetIpv6RtInfo      NetIpv6RtInfo;
    UINT4               u4ContextId = 0;

    Ip6GetCxtId (pNd6RtEntry->u4IfIndex, &u4ContextId);
    if (pNd6RtEntry->RtEntryTimer.u1Id != 0)
    {
        Ip6TmrStop (u4ContextId, IP6_DEF_RTR_LIFE_TIMER_ID,
                    gIp6GblInfo.Ip6TimerListId,
                    &pNd6RtEntry->RtEntryTimer.appTimer);
    }

    /* Before releasing Rt Entry to pool save the details 
     * in local variable for further processing .
     */

    MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));
    MEMCPY (&NetIpv6RtInfo.NextHop, &pNd6RtEntry->NextHop, sizeof (tIp6Addr));
    MEMCPY (&NetIpv6RtInfo.Ip6Dst, &pNd6RtEntry->Dst, sizeof (tIp6Addr));
    NetIpv6RtInfo.u1Prefixlen = pNd6RtEntry->u1PrefLen;
    NetIpv6RtInfo.u4Index = pNd6RtEntry->u4IfIndex;
    NetIpv6RtInfo.i1Proto = IP6_NDISC_PROTOID;
    NetIpv6RtInfo.i1DefRtrFlag = IP6_DEF_RTR_FLAG;
    NetIpv6RtInfo.u4RowStatus = IP6FWD_DESTROY;
    NetIpv6RtInfo.u2ChgBit = IP6_BIT_STATUS;
    NetIpv6RtInfo.u4ContextId = u4ContextId;

    IP6_TASK_UNLOCK ();
    Rtm6ApiIpv6LeakRoute (NETIPV6_DELETE_ROUTE, &NetIpv6RtInfo);
    IP6_TASK_LOCK ();

    TMO_SLL_Delete (&Nd6RouteList, (tTMO_SLL_NODE *) pNd6RtEntry);
    MEMSET (pNd6RtEntry, 0, sizeof (tNd6RtEntry));
    Ip6RelMem (NetIpv6RtInfo.u4ContextId, gi4ND6RouteId, (UINT1 *) pNd6RtEntry);

    return;
}

/******************************************************************************
DESCRIPTION         This routine calculates the new reachable time upon 
                    receiving router advertisement or after the base reach
                    timer expiry.
                    
INPUTS              pIf6   - Interface for which the reachtime needs to be 
                             calculated . If it is NULL the reach needs to be 
                             calculated for all the interfaces.
OUTPUTS            ip6If[]->u4Reachtime 

RETURNS             Void. 
*******************************************************************************/
VOID
Nd6CalNewReachTime (tIp6If * pIf6, UINT4 u4ReachTime)
{
    UINT4               u4MaxVal;
    UINT4               u4MinVal;
    UINT4               u4If = 1;
    /* Compute uniform random reachable time value from the given 
     * base timer value*/

    u4MaxVal = u4ReachTime * MAX_RANDOM_FACTOR;
    u4MinVal = u4ReachTime * MIN_RANDOM_FACTOR;

    if (pIf6 == NULL)
    {
        IP6_IF_SCAN (u4If, (UINT4) IP6_MAX_LOGICAL_IF_INDEX);
        {
            if (Ip6ifEntryExists (u4If) == IP6_SUCCESS)
            {
                if ((u4ReachTime = Ip6Random (u4MinVal, u4MaxVal)) != 0)
                {
                    if (gIp6GblInfo.apIp6If[u4If] != NULL)
                    {
                        gIp6GblInfo.apIp6If[u4If]->u4Reachtime = u4ReachTime;
                    }
                }
            }
        }
    }
    else if ((u4ReachTime = Ip6Random (u4MinVal, u4MaxVal)) != 0)
    {
        pIf6->u4Reachtime = u4ReachTime;
    }
    return;
}

/*****************************************************************************
DESCRIPTION        This routine performs processing of router redirect message
                   It first does validation of redirect message, If the message
                   is valid the destination cache is updated for the particular
                   destination address for which message has been sent. If the
                   Redirect message contains Target link layer option updates 
                   the ND cache entry for the particular destination.
INPUTS              pIp6        -   Pointer to the Ip6 header.
                    u2Len       -   Length of the ND payload in the
                                     received IPv6 packet
                    pBuf        -   Pointer to the Ipv6 Packet received.

OUTPUTS             None.

RETURNS             SUCCESS or FAILURE.
****************************************************************************/
INT4
Nd6RcvRouterRedirect (tIp6If * pIf6, tIp6Hdr * pIp6, UINT2 u2Len,
                      tCRU_BUF_CHAIN_HEADER * pBuf)
{
    UINT1               u1Copy = 0;
    UINT4               u4ExtnsLen;
    UINT4               u4Nd6Offset = 0;
    tNd6Redirect        nd6Rdirect, *pNd6Rdirect;

    if (!(IS_ADDR_LLOCAL (pIp6->srcAddr)))
    {

        IP6_TRC_ARG1 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC,
                      ND6_NAME,
                      "ND6: Nd6RcvRouterRedirect:Invalid src address for "
                      "redirect message from %s\n",
                      Ip6PrintAddr (&pIp6->srcAddr));
        return IP6_FAILURE;
    }

    u4Nd6Offset = IP6_BUF_READ_OFFSET (pBuf) - sizeof (tIcmp6PktHdr);
    IP6_BUF_READ_OFFSET (pBuf) -= sizeof (tIcmp6PktHdr);

    if ((pNd6Rdirect = (tNd6Redirect *) Ip6BufRead
         (pBuf, (UINT1 *) &nd6Rdirect, u4Nd6Offset,
          sizeof (tNd6Redirect), u1Copy)) == NULL)
    {
        IP6_TRC_ARG2 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC, BUFFER_TRC,
                      ND6_NAME,
                      "ND6:Nd6RcvRouterRedirect:Receive RD - BufRead Failed! "
                      "BufPtr %p Offset %d\n", pBuf, u4Nd6Offset);
        IP6_TRC_ARG3 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC, BUFFER_TRC,
                      IP6_NAME,
                      "%s buffer error occurred, Type = %d Module = 0x%X "
                      "Bufptr = %p ", ERROR_FATAL_STR, ERR_BUF_READ, pBuf);
        return (IP6_FAILURE);
    }

    /* Validating the received Redirect message */
    if ((Nd6ValidateRedirectMsgInCxt (pIf6->pIp6Cxt, pIp6, pNd6Rdirect, u2Len))
        == IP6_FAILURE)
    {
        /* A host receving invalid redirect messages should silently 
         * discard the message.
         */
        return IP6_FAILURE;
    }

    /* Update the destination cache entry */
    Nd6UpdateDestCacheEntry (pIf6, pIp6, pNd6Rdirect);

    /* All included options length must be more than 0 */
    /* Check if any options are included in the message */
    /* If target link layer option is there update the Nd cache entry. */

    /* Process the Extension Options */
    u4ExtnsLen = u2Len - sizeof (tNd6Redirect);
    if (Nd6ProcessRedirectExtns (pIp6, pIf6, pNd6Rdirect, pBuf, u4ExtnsLen)
        == IP6_FAILURE)
    {
        return (IP6_FAILURE);
    }

    if (Ip6BufRelease (pIf6->pIp6Cxt->u4ContextId, pBuf, FALSE, ND6_MODULE) !=
        IP6_SUCCESS)
    {
        IP6_TRC_ARG (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                     BUFFER_TRC | ALL_FAILURE_TRC, ND6_NAME,
                     "ND6: Nd6RcvRouterRedirect:Rcv Redirect - BufRelease "
                     "Failed\n");
        IP6_TRC_ARG3 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC, BUFFER_TRC,
                      IP6_NAME,
                      "%s buffer error occurred, Type = %d Module = 0x%X "
                      "Bufptr = %p ", ERROR_FATAL_STR, ERR_BUF_RELEASE, pBuf);
    }
    return (IP6_SUCCESS);

}

/*****************************************************************************
DESCRIPTION         This routine validates the redirect message.

INPUTS              pIp6        -   Pointer to the Ip6 header.
 
                    pNd6Rdirect -   Pointer to the redirect message.

                    u2Len       -   Length of the ND payload in the
                                     received IPv6 packet

OUTPUTS             None.

RETURNS             SUCCESS or FAILURE.
****************************************************************************/
INT4
Nd6ValidateRedirectMsgInCxt (tIp6Cxt * pIp6Cxt, tIp6Hdr * pIp6,
                             tNd6Redirect * pNd6Rdirect, UINT2 u2Len)
{
    tIcmp6PktHdr        icmp6Hdr;

    /* Extract Icmp hdr */
    icmp6Hdr = pNd6Rdirect->icmp6Hdr;

    if (Nd6ValidateMsgHdrInCxt (pIp6Cxt, pIp6, &icmp6Hdr) == IP6_FAILURE)
    {
        return (IP6_FAILURE);
    }

    /* ICMP Length MUST be 40 or more octets */
    if (u2Len < sizeof (tNd6Redirect))
    {
        IP6_TRC_ARG1 (pIp6Cxt->u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC,
                      ND6_NAME,
                      "ND6:Nd6ValidateRedirectMsg: Validation of Redirect "
                      "failed - Incorrect Len %d\n", u2Len);
        return (IP6_FAILURE);
    }

    /* Destination address field in icmp redirect MUST not be Multicast 
     * address.
     */
    if (IS_ADDR_MULTI (pNd6Rdirect->destAddr6))
    {
        IP6_TRC_ARG1 (pIp6Cxt->u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC,
                      ND6_NAME,
                      "ND6: Nd6ValidateRedirectMsg:Validation of Redirect "
                      "Failed -  Dest Address %s MUST not be MULTICAST\n",
                      Ip6ErrPrintAddr (ERROR_MINOR, &pNd6Rdirect->destAddr6));
        return (IP6_FAILURE);
    }

    /* Target should either be linklocal or same as icmp destination address */

    if (!((IS_ADDR_LLOCAL (pNd6Rdirect->targAddr6)) ||
          (MEMCMP (&pNd6Rdirect->targAddr6, &pNd6Rdirect->destAddr6,
                   sizeof (tIp6Addr)) == 0)))
    {
        IP6_TRC_ARG2 (pIp6Cxt->u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC,
                      IP6_NAME,
                      "ND6:Nd6ValidateRedirectMsg: Invalid REDIRECT Msg ,"
                      "target = %s != Icmp dest = %s\n",
                      Ip6PrintAddr (&pNd6Rdirect->targAddr6),
                      Ip6PrintAddr (&pNd6Rdirect->destAddr6));
        return (IP6_FAILURE);
    }

    return IP6_SUCCESS;
}

/*****************************************************************************
DESCRIPTION         This routine updates the destination entry if exists other
                    wise creates one.

INPUTS              pIf6        -   Pointer to the Ip6 interface.
 
                    pNd6Rdirect -   Pointer to the redirect message.

OUTPUTS             Routing  table.

RETURNS             None
****************************************************************************/
VOID
Nd6UpdateDestCacheEntry (tIp6If * pIf6, tIp6Hdr * pIp6,
                         tNd6Redirect * pNd6Rdirect)
{
    tIp6Addr           *pDstAddr6 = NULL;
    tIp6Addr            DstAddr;
    tIp6Addr            NextHop;
    tNetIpv6RtInfo      NetIpv6RtInfo;
    UINT4               u4ContextId = 0;
    INT4                i4Status = 0;
    UINT1               u1Prefixlen = 128;
    INT1                i1Type;
    tNd6CacheEntry     *pNd6c = NULL;

    u4ContextId = pIf6->pIp6Cxt->u4ContextId;
    pIf6 = Ip6GetRouteInCxt (u4ContextId, &pNd6Rdirect->destAddr6,
                             &pNd6c, &NetIpv6RtInfo);

    if (pIf6 == NULL)
    {
        IP6_TRC_ARG1 (u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                      "ND6: Nd6UpdateDestCacheEntry:Route not found for "
                      "the given destination address %s\n",
                      Ip6PrintAddr (&pNd6Rdirect->destAddr6));
        return;
    }

    /* Source address of the IP packet should be same as current 
     * first-hop router for the specified destination address.
     */
    i1Type = NetIpv6RtInfo.i1Type;

    if (i1Type == DIRECT)
    {
        pDstAddr6 = &pNd6Rdirect->destAddr6;
    }
    else
    {
        Ip6AddrCopy (&DstAddr, &NetIpv6RtInfo.NextHop);
        pDstAddr6 = &DstAddr;
    }

    if (MEMCMP (&pIp6->srcAddr, pDstAddr6, sizeof (tIp6Addr)) != 0)
    {
        IP6_TRC_ARG2 (u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                      "ND6: Nd6ValidateRedirectMsg:Invalid REDIRECT Msg, "
                      "Source = %s != Icmp dest = %s\n",
                      Ip6PrintAddr (&pIp6->srcAddr), Ip6PrintAddr (pDstAddr6));
        return;
    }

    /* If the earlier route exactly matches the new redirectly route and if
     * is learnt via NDISC, then delete the old route and add the new route.
     */
    if ((MEMCMP (&NetIpv6RtInfo.Ip6Dst, &pNd6Rdirect->destAddr6,
                 sizeof (tIp6Addr)) == 0)
        && (NetIpv6RtInfo.i1Proto == IP6_NDISC_PROTOID))
    {
        /* Delete old route and re-add the new route. Else we might still
         * be using the old route for forwarding. */
        Nd6DelRtEntryInCxt (pIf6->pIp6Cxt, &NetIpv6RtInfo.Ip6Dst,
                            NetIpv6RtInfo.u1Prefixlen, &NetIpv6RtInfo.NextHop);

        IP6_TASK_UNLOCK ();
        i4Status = Rtm6ApiIpv6LeakRoute (NETIPV6_DELETE_ROUTE, &NetIpv6RtInfo);
        IP6_TASK_LOCK ();

        if (i4Status == RTM6_FAILURE)
        {
            return;
        }
    }

    if (MEMCMP (&pNd6Rdirect->targAddr6, &pNd6Rdirect->destAddr6,
                sizeof (tIp6Addr)) == 0)
    {
        /* Destination is a neighbor , so add a direct route */
        i1Type = DIRECT;
        SET_ADDR_UNSPECIFIED (NextHop);
    }
    else
    {
        /* target address is a better first hop router, next hop  */
        i1Type = INDIRECT;
        MEMCPY (&NextHop, &pNd6Rdirect->targAddr6, sizeof (tIp6Addr));
    }

    if (Nd6AddRtEntry (&pNd6Rdirect->destAddr6, u1Prefixlen,
                       &NextHop, pIf6->u4Index, -1) == IP6_FAILURE)
    {
        IP6_TRC_ARG3 (u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                      "ND6: Nd6UpdateDestCacheEntry:Dest cache update Failed "
                      "Dst Addr = %s Preflen=0x%X, NextHop = %s\n",
                      Ip6PrintAddr (&pNd6Rdirect->destAddr6), u1Prefixlen,
                      Ip6PrintAddr (&NextHop));
        return;
    }

    MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));
    MEMCPY (&NetIpv6RtInfo.NextHop, &NextHop, sizeof (tIp6Addr));
    MEMCPY (&NetIpv6RtInfo.Ip6Dst, &pNd6Rdirect->destAddr6, sizeof (tIp6Addr));
    NetIpv6RtInfo.u1Prefixlen = u1Prefixlen;
    NetIpv6RtInfo.u4Index = pIf6->u4Index;
    NetIpv6RtInfo.u4Metric = 0;
    NetIpv6RtInfo.i1Proto = IP6_NDISC_PROTOID;
    NetIpv6RtInfo.i1Type = i1Type;
    NetIpv6RtInfo.u4RouteTag = 0;
    NetIpv6RtInfo.i1DefRtrFlag = 0;
    NetIpv6RtInfo.u4RowStatus = IP6FWD_ACTIVE;
    NetIpv6RtInfo.u2ChgBit = IP6_BIT_ALL;
    NetIpv6RtInfo.u4ContextId = u4ContextId;

    IP6_TASK_UNLOCK ();
    i4Status = Rtm6ApiIpv6LeakRoute (NETIPV6_ADD_ROUTE, &NetIpv6RtInfo);
    IP6_TASK_LOCK ();

    if (i4Status == RTM6_FAILURE)
    {
        Nd6DelRtEntryInCxt (pIf6->pIp6Cxt, &pNd6Rdirect->destAddr6,
                            u1Prefixlen, &NextHop);
        IP6_TRC_ARG3 (u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                      "ND6: Nd6UpdateDestCacheEntry:Dest cache update Failed "
                      "Dst Addr = %s Preflen=0x%X, NextHop = %s\n",
                      Ip6PrintAddr (&pNd6Rdirect->destAddr6), u1Prefixlen,
                      Ip6PrintAddr (&NextHop));
    }
#ifdef TUNNEL_WANTED
    if (pIf6->u1IfType != IP6_TUNNEL_INTERFACE_TYPE)
    {
        Nd6Resolve (NULL, pIf6, &NextHop, NULL);
    }
#else
    Nd6Resolve (NULL, pIf6, &NextHop, NULL);
#endif

    return;
}

/*****************************************************************************
DESCRIPTION         This routine processes the redirect message extentions

INPUTS              pIp6        -   Pointer to the Ip6 header.
 
                    pNd6Rdirect -   Pointer to the redirect message.
                
                    pBuf        -   Pointer to the message buffer.

                    u2ExtnsLen  -   Extention message length.     

OUTPUTS             ND cache table.

RETURNS             IP6_SUCCESS or IP6_FAILURE
****************************************************************************/
INT4
Nd6ProcessRedirectExtns (tIp6Hdr * pIp6, tIp6If * pIf6,
                         tNd6Redirect * pNd6Rdirect,
                         tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4ExtnsLen)
{
    UINT4               u4ExtOffset = 0;
    UINT4               u4AdvFlag = 0;
    tNd6ExtHdr         *pNd6Ext;
    tCgaOptions         CgaOptions;
    tUtlSysPreciseTime  SysPreciseTime;
    tNd6CacheEntry     *pNd6cEntry = NULL;
    UINT4               u4TSNew = 0;
    UINT4               u4ExtOffset = 0;
    UINT4               u4AdvFlag = 0;
    UINT4               u4TempOffset = 0;
    UINT4               u4SeNDOptCount = 0;
    UINT1               au1PubKey[IP6_SEND_CGA_BYTES];
    UINT1               au1Nonce[ND6_NONCE_LENGTH];

    MEMSET (&CgaOptions, 0, sizeof (tCgaOptions));
    MEMSET (au1PubKey, 0, IP6_SEND_CGA_BYTES);
    MEMSET (au1Nonce, 0, ND6_NONCE_LENGTH);
    MEMSET (&SysPreciseTime, 0, sizeof (tUtlSysPreciseTime));

    u4TempOffset = IP6_BUF_READ_OFFSET (pBuf) - sizeof (tNd6Redirect);
    CgaOptions.pu1DerPubKey = au1PubKey;

    /* Extract the Extension Options */
    while (u4ExtnsLen)
    {
        if ((pNd6Ext = Nd6ExtractExtnHdrInCxt (pIf6->pIp6Cxt, pBuf)) == NULL)
        {
            return (IP6_FAILURE);
        }
        u4ExtOffset = IP6_BUF_READ_OFFSET (pBuf) - IP6_TYPE_LEN_BYTES;
        IP6_BUF_READ_OFFSET (pBuf) = u4ExtOffset;

        switch (pNd6Ext->u1Type)
        {

            case ND6_TARG_LLA_EXT:

                /* Setting the override flag */

                u4AdvFlag |= ND6_OVERRIDE_FLAG;
                if ((MEMCMP (&pNd6Rdirect->targAddr6, &pNd6Rdirect->destAddr6,
                             sizeof (tIp6Addr)) != 0))
                {
                    /* Target address and destination address are not equal , 
                     * so target is a better first hop router , 
                     * so set router flag.
                     */

                    u4AdvFlag |= ND6_DEFAULT_ROUTER;
                }

                u4AdvFlag |= ND6_REDIRECT;

                if (Nd6ProcessTLLAOption (u4ExtnsLen, u4AdvFlag,
                                          &pNd6Rdirect->targAddr6, pNd6Ext,
                                          pIp6, pIf6, pBuf) != IP6_SUCCESS)
                {
                    IP6_TRC_ARG (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                                 DATA_PATH_TRC, ND6_NAME,
                                 "ND6:Nd6ProcessRedirectExtns: Validation of NA"
                                 " Failed - Invalid TLLA \n");
                    (IP6_SEND_IF_DROP_STATS
                     (pIf6, COUNT_RD)).u4TgtLinkAddrPkts++;
                    (IP6_IF_STATS (pIf6))->u4NdSecureDroppedPkts++;
                    return IP6_FAILURE;
                }

                u4ExtnsLen -= sizeof (tNd6AddrExt);
                (IP6_SEND_IF_IN_STATS (pIf6, COUNT_RD)).u4TgtLinkAddrPkts++;
                break;

            case ND6_REDIRECT_EXT:

                /* Redirected header option is the last one in the 
                 * redirected message so return success.
                 */
                IP6_BUF_READ_OFFSET (pBuf) +=
                    (pNd6Ext->u1Len * BYTES_IN_OCTECT);
                u4ExtnsLen -= (pNd6Ext->u1Len * BYTES_IN_OCTECT);
                (IP6_SEND_IF_IN_STATS (pIf6, COUNT_RD)).u4RedirHrPkts++;
                break;

            case ND6_SEND_CGA_OPT:

                if (IP6_TRUE == IF_SEND_ENABLED (pIf6))
                {
                    if (Nd6SeNDProcessCgaOption (pIf6, pIp6, pBuf,
                                                 pNd6Ext->u1Len,
                                                 &CgaOptions) == IP6_FAILURE)
                    {
                        IP6_TRC_ARG (pIf6->pIp6Cxt->u4ContextId,
                                     ND6_MOD_TRC, BUFFER_TRC,
                                     ND6_NAME, "Nd6ProcessRoutSolExtns:"
                                     " Cga processing failed\n");
                        return IP6_FAILURE;
                    }
                    u4SeNDOptCount++;
                }
                /* extension length is in unit of 8 octets */
                u4ExtnsLen -= (pNd6Ext->u1Len * BYTES_IN_OCTECT);
                break;

            case ND6_SEND_TIMESTAMP_OPT:

                if (IP6_TRUE == IF_SEND_ENABLED (pIf6))
                {
                    if (Nd6SeNDProcessTSOption (pIf6, pIp6, pBuf, &u4TSNew)
                        == IP6_FAILURE)
                    {
                        IP6_TRC_ARG (pIf6->pIp6Cxt->u4ContextId,
                                     ND6_MOD_TRC, BUFFER_TRC,
                                     ND6_NAME, "Nd6ProcessRoutSolExtns:"
                                     " Timestamp processing failed\n");
                        return IP6_FAILURE;
                    }
                    u4SeNDOptCount++;
                }
                /* extension length is in unit of 8 octets */
                u4ExtnsLen -= (pNd6Ext->u1Len * BYTES_IN_OCTECT);
                break;

            case ND6_SEND_RSA_SIGN_OPT:

                if (IP6_TRUE == IF_SEND_ENABLED (pIf6))
                {
                    if (Nd6SeNDProcessRsaOption (pIf6, pIp6, pBuf,
                                                 pNd6Ext->u1Len, u4TempOffset,
                                                 &CgaOptions) == IP6_FAILURE)
                    {
                        IP6_TRC_ARG (pIf6->pIp6Cxt->u4ContextId,
                                     ND6_MOD_TRC, BUFFER_TRC,
                                     ND6_NAME, "Nd6ProcessRoutSolExtns:"
                                     " RSA processing failed\n");
                        return IP6_FAILURE;
                    }
                    u4SeNDOptCount++;
                }
                /* extension length is in unit of 8 octets */
                u4ExtnsLen -= (pNd6Ext->u1Len * BYTES_IN_OCTECT);
                break;

            default:
                /* Any other options should silently be ignored */
                return IP6_SUCCESS;
        }
    }

    /*
     * If SeND is enabled in full secure mode then all the four options
     * should present, and in mixed mode either all options should present
     * or no SeND options should present in the packet.
     * if it is not then discard the packet
     */
    if (((ND6_SECURE_ENABLE == pIf6->u1SeNDStatus)
         && u4SeNDOptCount != ND6_SEND_SOL_OPT_COUNT)
        || ((ND6_SECURE_MIXED == pIf6->u1SeNDStatus)
            && ((u4SeNDOptCount != 0)
                || (u4SeNDOptCount != ND6_SEND_SOL_OPT_COUNT))))
    {
        (IP6_IF_STATS (pIf6))->u4NdSecureInvalidPkts++;
        return (IP6_FAILURE);
    }
    return (IP6_SUCCESS);
}

/*****************************************************************************
DESCRIPTION         This routine performs the processing of prefix option.

INPUTS              pNd6Prefix  -   Pointer to the prefix information.
      
                    pIf6        -   Pointer to the Ip6 interface

OUTPUTS             address list.

RETURNS             SUCCESS or FAILURE.
****************************************************************************/
INT1
Nd6ProcessPrefixOption (tNd6PrefixExt * pNd6Prefix, tIp6If * pIf6,
                        tIp6Hdr * pIp6Hdr)
{
    tIp6Addr            Ip6Addr;
    tIp6Addr            NextHop;
    tNd6RtEntry        *pNd6RtEntry = NULL;
    tNetIpv6RtInfo      NetIpv6RtInfo;
    tIp6AddrInfo       *pAddrInfo = NULL;
    tTMO_SLL_NODE      *pAddrSll = NULL;
    UINT2               u2ProfIndx = 0;
    UINT4               u4RcvdLifeTime = 0;
    UINT4               u4StoredLifeTime = 0;
    INT4                i4RetVal = IP6_FAILURE;
    INT4                i4Status = 0;
#ifdef TUNNEL_WANTED
    UINT4               u4IpAddr = 0;
    UINT4               u4IsaTapId = 0;
#endif
    INT1                i1Type = 0;

    UNUSED_PARAM (pIp6Hdr);
    SET_ADDR_UNSPECIFIED (Ip6Addr);

    /* if the prefix received is a link local prefix a host 
     * should ignore this option and proceed with processing
     * remaining message. This is common for both On-Link and Autonous Flag.
     */
    if ((IS_ADDR_LLOCAL (pNd6Prefix->prefAddr6)))
    {
        return IP6_SUCCESS;
    }

    /* If the ON_LINK Flag is set, then update the Prefix List
     * (Routing Table) as per RFC 2462 */
    if (IS_ONLINK_FLAG_SET (pNd6Prefix->u1PrefFlag))
    {
        /* Check whether the prefix is already present in the prefix list
         * or not. */
        Ip6CopyAddrBits (&Ip6Addr, &pNd6Prefix->prefAddr6,
                         pNd6Prefix->u1PrefLen);

        /* On-Link Flag is set. So next-hop is in the same link. */
        SET_ADDR_UNSPECIFIED (NextHop);
        i1Type = DIRECT;

        u4RcvdLifeTime = NTOHL (pNd6Prefix->u4ValidTime);

        if (u4RcvdLifeTime == 0)
        {
            if (Nd6DelRtEntryInCxt (pIf6->pIp6Cxt, &Ip6Addr,
                                    pNd6Prefix->u1PrefLen, &NextHop)
                == IP6_SUCCESS)
            {
                MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));
                MEMCPY (&NetIpv6RtInfo.NextHop, &NextHop, sizeof (tIp6Addr));
                MEMCPY (&NetIpv6RtInfo.Ip6Dst, &Ip6Addr, sizeof (tIp6Addr));
                NetIpv6RtInfo.u1Prefixlen = pNd6Prefix->u1PrefLen;
                NetIpv6RtInfo.u4Index = pIf6->u4Index;
                NetIpv6RtInfo.i1Proto = IP6_NDISC_PROTOID;
                NetIpv6RtInfo.u4RowStatus = IP6FWD_DESTROY;
                NetIpv6RtInfo.u2ChgBit = IP6_BIT_STATUS;
                Ip6GetCxtId (pIf6->u4Index, &NetIpv6RtInfo.u4ContextId);

                IP6_TASK_UNLOCK ();
                Rtm6ApiIpv6LeakRoute (NETIPV6_DELETE_ROUTE, &NetIpv6RtInfo);
                IP6_TASK_LOCK ();
            }
        }
        else
        {
            pNd6RtEntry = ND6GetRouteEntry (&Ip6Addr, pNd6Prefix->u1PrefLen,
                                            &NextHop, 0);
            if (pNd6RtEntry == NULL)
            {
                if (Nd6AddRtEntry (&Ip6Addr, pNd6Prefix->u1PrefLen, &NextHop,
                                   pIf6->u4Index, (UINT2) u4RcvdLifeTime)
                    == IP6_FAILURE)
                {
                    IP6_TRC_ARG (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                                 ALL_FAILURE_TRC, ND6_NAME,
                                 "Adding a new ND Route Entry FAILED\n");
                    return IP6_FAILURE;
                }

                MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));
                MEMCPY (&NetIpv6RtInfo.NextHop, &NextHop, sizeof (tIp6Addr));
                MEMCPY (&NetIpv6RtInfo.Ip6Dst, &Ip6Addr, sizeof (tIp6Addr));
                NetIpv6RtInfo.u1Prefixlen = pNd6Prefix->u1PrefLen;
                NetIpv6RtInfo.u4Index = pIf6->u4Index;
                NetIpv6RtInfo.u4Metric = 1;
                NetIpv6RtInfo.i1Proto = IP6_NDISC_PROTOID;
                NetIpv6RtInfo.i1Type = i1Type;
                NetIpv6RtInfo.u4RouteTag = 0;
                NetIpv6RtInfo.i1DefRtrFlag = 0;
                NetIpv6RtInfo.u4RowStatus = IP6FWD_ACTIVE;
                NetIpv6RtInfo.u2ChgBit = IP6_BIT_ALL;
                Ip6GetCxtId (pIf6->u4Index, &NetIpv6RtInfo.u4ContextId);

                IP6_TASK_UNLOCK ();
                i4Status = Rtm6ApiIpv6LeakRoute (NETIPV6_ADD_ROUTE,
                                                 &NetIpv6RtInfo);
                IP6_TASK_LOCK ();

                if (i4Status == RTM6_FAILURE)
                {
                    Nd6DelRtEntryInCxt (pIf6->pIp6Cxt, &Ip6Addr,
                                        pNd6Prefix->u1PrefLen, &NextHop);
                    return IP6_FAILURE;
                }
#ifdef TUNNEL_WANTED
                if (pIf6->u1IfType != IP6_TUNNEL_INTERFACE_TYPE)
                {
                    Nd6Resolve (NULL, pIf6, &NextHop, NULL);
                }
#else
                Nd6Resolve (NULL, pIf6, &NextHop, NULL);
#endif
            }
            else
            {
                Ip6TmrRestart (NetIpv6RtInfo.u4ContextId,
                               IP6_DEF_RTR_LIFE_TIMER_ID,
                               gIp6GblInfo.Ip6TimerListId,
                               (tTmrAppTimer *) & pNd6RtEntry->RtEntryTimer.
                               appTimer, (UINT2) u4RcvdLifeTime);
            }
        }
    }

    /* If autonomous flag is set then this prefix option can be used for
     * deriving the automatic stateless address for this interface as per
     * the RFC 2462. If autonomous flag is not set then this prefix option
     * can be used to form the prefix list as per RFC 2461.
     */
    if (IS_AUTONOMOUS_FLAG_SET (pNd6Prefix->u1PrefFlag))
    {
        /* If the preferred lifetime is greater than the valid lifetime silently
         * ignore the option. */
        if (NTOHL (pNd6Prefix->u4PrefTime) > NTOHL (pNd6Prefix->u4ValidTime))
        {
            IP6_TRC_ARG (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                         DATA_PATH_TRC | ALL_FAILURE_TRC, ND6_NAME,
                         "ND6: Nd6ProcessPrefixOption: Preference Time > Valid Time"
                         ". Ignoring Option.\n");
            return IP6_SUCCESS;
        }

        /* Check whether the Received Prefix is already present or not. */
        TMO_SLL_Scan (&pIf6->addr6Ilist, pAddrSll, tTMO_SLL_NODE *)
        {
            pAddrInfo = IP6_ADDR_PTR_FROM_SLL (pAddrSll);

            if ((Ip6AddrMatch (&pNd6Prefix->prefAddr6, &pAddrInfo->ip6Addr,
                               pNd6Prefix->u1PrefLen) == TRUE) &&
                (pAddrInfo->u1PrefLen == pNd6Prefix->u1PrefLen))
            {
                /* Matching Entry Found. */
                break;
            }
        }

        if (pAddrSll == NULL)
        {
            /* Received Prefix Does not match the existing Address Prefix. */
            if (pNd6Prefix->u4ValidTime == 0)
            {
                return IP6_SUCCESS;
            }

#ifdef TUNNEL_WANTED
            /* If the interface is isatap tunnel interface form the isatap
             * address using the prefix */
            if ((pIf6->u1IfType == IP6_TUNNEL_INTERFACE_TYPE) &&
                (pIf6->pTunlIf->u1TunlType == IPV6_ISATAP_TUNNEL))
            {
                if (pNd6Prefix->u1PrefLen != IP6_ISATAP_PREFIX_LEN)
                {
                    IP6_TRC_ARG (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                                 ALL_FAILURE_TRC, ND6_NAME,
                                 "ND6: Nd6ProcessPrefixOption: Sum of Prefix Len and "
                                 "interface id len != 128. Ignoring Option.\n");
                    return IP6_SUCCESS;
                }
                /* Form the isatap address using prefix
                 * isatap address = prefix(64) isatapid(32) v4addr(32) */
                MEMCPY ((UINT1 *) &u4IpAddr,
                        (UINT1 *) &pIf6->pTunlIf->tunlSrc.u1_addr[12], 4);

                MEMCPY (&Ip6Addr, &pNd6Prefix->prefAddr6, IP6_EUI_ADDRESS_LEN);

                u4IsaTapId =
                    ((IS_IP_ADDR_PRIVATE (u4IpAddr)) == OSIX_TRUE) ?
                    IP6_ISATAP_PRIVATE_ID : IP6_ISATAP_GLOBAL_ID;
                u4IsaTapId = OSIX_HTONL (u4IsaTapId);
                MEMCPY (&Ip6Addr.u4_addr[2], &u4IsaTapId, sizeof (UINT4));

                MEMCPY (&Ip6Addr.u4_addr[3], &u4IpAddr, sizeof (UINT4));
            }
            else
#endif
            {
                if ((pNd6Prefix->u1PrefLen + (IP6_EUI_ADDRESS_LEN *
                                              IP6_EUI_ADDRESS_LEN)) !=
                    IP6_ADDR_MAX_PREFIX)
                {
                    /* If the sum of prefix len of received prefix and
                     * interface identifier length is not equal to 128,
                     * ignore the option. */
                    IP6_TRC_ARG (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                                 ALL_FAILURE_TRC, ND6_NAME,
                                 "ND6: Nd6ProcessPrefixOption: Sum of Prefix "
                                 "Len and interface id len != 128. Ignoring Option.\n");
                    return IP6_SUCCESS;
                }

                /* Form the address from the received prefix and 
                 * interface id. */
                MEMCPY (&Ip6Addr, &pNd6Prefix->prefAddr6, sizeof (tIp6Addr));

                MEMCPY (&Ip6Addr.u4_addr[2], IP6_IF_TOKEN (pIf6->u4Index),
                        IP6_EUI_ADDRESS_LEN);
            }

            /* Add the address to the interface address list. */
            i4RetVal = Ip6CreateNewProfileIndex (pNd6Prefix, pIf6);
            if (i4RetVal == IP6_FAILURE)
            {
                IP6_TRC_ARG (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                             DATA_PATH_TRC, ND6_NAME,
                             "ND6: Nd6ProcessPrefixOption:"
                             "No free Address profile\n");
                return IP6_FAILURE;
            }
            u2ProfIndx = (UINT2) i4RetVal;

            if ((pAddrInfo = Ip6AddrCreate (pIf6->u4Index, &(Ip6Addr),
                                            pNd6Prefix->u1PrefLen,
                                            ADMIN_DOWN, IP6_ADDR_TYPE_UNICAST,
                                            u2ProfIndx, IP6_ADDR_AUTO_SL))
                == NULL)
            {
                IP6_TRC_ARG (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                             DATA_PATH_TRC, ND6_NAME,
                             "ND6: Nd6ProcessPrefixOption:"
                             "Creating Address profile FAILED\n");
                return IP6_FAILURE;
            }

            if (Ip6AddrUp (pIf6->u4Index, &(Ip6Addr), pNd6Prefix->u1PrefLen,
                           pAddrInfo) == IP6_FAILURE)
            {
                IP6_TRC_ARG (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                             DATA_PATH_TRC, ND6_NAME,
                             "ND6: Nd6ProcessPrefixOption:"
                             "Activating Address profile FAILED\n");
                Ip6AddrDelete (pIf6->u4Index, &Ip6Addr, pNd6Prefix->u1PrefLen,
                               TRUE);
                return IP6_FAILURE;
            }

            pAddrInfo->u1AdminStatus = IP6FWD_ACTIVE;

            /* Start the valid life timer for this prefix. */
            pAddrInfo->AddrValidTimer.u1Id = IP6_VALID_LIFETIME_TIMER_ID;
            if (NTOHL (pNd6Prefix->u4ValidTime) != IP6_ADDR_PROF_MAX_VALID_LIFE)
            {
                Ip6TmrStart (pIf6->pIp6Cxt->u4ContextId,
                             pAddrInfo->AddrValidTimer.u1Id,
                             gIp6GblInfo.Ip6TimerListId,
                             &pAddrInfo->AddrValidTimer.appTimer,
                             NTOHL (pNd6Prefix->u4ValidTime));
            }

            /* Start the preffered life timer for this prefix. */
            if (NTOHL (pNd6Prefix->u4PrefTime) == 0)
            {
                /* Set the address status to deprecated */
                pAddrInfo->u1Status &= ADDR6_DEPRECATED;
            }
            else
            {
                if (NTOHL (pNd6Prefix->u4PrefTime) !=
                    IP6_ADDR_PROF_MAX_PREF_LIFE)
                {
                    Ip6TmrStart (pIf6->pIp6Cxt->u4ContextId,
                                 IP6_PREF_LIFETIME_TIMER_ID,
                                 gIp6GblInfo.Ip6TimerListId,
                                 &pAddrInfo->AddrPrefTimer.appTimer,
                                 NTOHL (pNd6Prefix->u4PrefTime));
                }
                /*Set the address status to prefered */
                pAddrInfo->u1Status |= ADDR6_PREFERRED;
            }
        }
        else
        {
#ifdef MIP6_WANTED
            if (pAddrInfo->u1Status & ADDR6_UNKNOWN)
            {
                return IP6_SUCCESS;
            }
#endif
            if (pAddrInfo->u1ConfigMethod == IP6_ADDR_STATIC)
            {
                /* Matching Address is STATICALLY configured Address. */
                return IP6_SUCCESS;
            }

            /* This condition exists when the address corresponding 
             * to the prefix already exists. So update the preferred and
             * valid life time based on the values received. */
            u2ProfIndx = pAddrInfo->u2Addr6Profile;
            if (u2ProfIndx < gIp6GblInfo.u4MaxAddrProfileLimit)
            {
                u4RcvdLifeTime = NTOHL (pNd6Prefix->u4ValidTime);
                u4StoredLifeTime = IP6_ADDR_VALID_TIME (u2ProfIndx);

                if ((u4RcvdLifeTime > u4StoredLifeTime) ||
                    (u4RcvdLifeTime >
                     IP6_RA_DEF_VALID_LIFE_TIME /* 2 hours */ ))
                {
                    /* Update the Stored Life time of the address */
                    IP6_ADDR_VALID_TIME (u2ProfIndx) = u4RcvdLifeTime;
                    IP6_ADDR_PREF_TIME (u2ProfIndx) =
                        NTOHL (pNd6Prefix->u4PrefTime);
                }
                else if ((u4StoredLifeTime <= IP6_RA_DEF_VALID_LIFE_TIME) &&
                         (u4RcvdLifeTime <= u4StoredLifeTime))
                {
                    /* If the sender is authenticated, then update the stored Life
                     * time. Else ignore the packet. */
                    /* NOTE : Currently just ignoring the packet. When
                     * Authentication validation is performed this part of the
                     * code needs to be modified.
                     */
                    return IP6_SUCCESS;
                }
                else
                {
                    /* Reseting the valid life time to 2 hrs */
                    IP6_ADDR_VALID_TIME (u2ProfIndx) =
                        IP6_RA_DEF_VALID_LIFE_TIME;
                    IP6_ADDR_PREF_TIME (u2ProfIndx) = IP6_RA_DEF_PREF_LIFE_TIME;
                }

                /* Start the invalidation timer for this prefix */
                if (pAddrInfo->AddrValidTimer.u1Id != 0)
                {
                    Ip6TmrStop (pIf6->pIp6Cxt->u4ContextId,
                                IP6_VALID_LIFETIME_TIMER_ID,
                                gIp6GblInfo.Ip6TimerListId,
                                &(pAddrInfo->AddrValidTimer.appTimer));
                }

                pAddrInfo->AddrValidTimer.u1Id = IP6_VALID_LIFETIME_TIMER_ID;
                Ip6TmrStart (pIf6->pIp6Cxt->u4ContextId,
                             IP6_VALID_LIFETIME_TIMER_ID,
                             gIp6GblInfo.Ip6TimerListId,
                             &pAddrInfo->AddrValidTimer.appTimer,
                             IP6_ADDR_VALID_TIME (u2ProfIndx));

                /* Start the preffered timer for this prefix. */
                if (pAddrInfo->AddrPrefTimer.u1Id != 0)
                {
                    Ip6TmrStop (pIf6->pIp6Cxt->u4ContextId,
                                IP6_PREF_LIFETIME_TIMER_ID,
                                gIp6GblInfo.Ip6TimerListId,
                                &(pAddrInfo->AddrPrefTimer.appTimer));
                }

                if (NTOHL (pNd6Prefix->u4PrefTime) == 0)
                {
                    /* Set the address status to deprecated */
                    pAddrInfo->u1Status &= ADDR6_DEPRECATED;
                }
                else
                {
                    pAddrInfo->AddrPrefTimer.u1Id = IP6_PREF_LIFETIME_TIMER_ID;
                    Ip6TmrStart (pIf6->pIp6Cxt->u4ContextId,
                                 IP6_PREF_LIFETIME_TIMER_ID,
                                 gIp6GblInfo.Ip6TimerListId,
                                 &pAddrInfo->AddrPrefTimer.appTimer,
                                 IP6_ADDR_PREF_TIME (u2ProfIndx));
                    /* Set the address status to preferred */
                    pAddrInfo->u1Status |= ADDR6_PREFERRED;
                }
            }
        }
    }

    return IP6_SUCCESS;
}

/*****************************************************************************
DESCRIPTION         This routine deletes the stalest default route for 
                    creating room for new entries.

INPUTS              None.
      
OUTPUTS             None. 

RETURNS             IP6_SUCCESS or IP6_FAILURE.
****************************************************************************/
INT4
Nd6PurgeStalestDefRt (VOID)
{
    tNd6RtEntry        *pNd6RtEntry = NULL;
    tNd6RtEntry        *pNd6DelRtEntry = NULL;
    tIp6Addr            Ip6Addr;
    tTMO_SLL_NODE      *pNode = NULL;
    INT4                i4RemainingTime = 0;
    INT4                i4TempRemainingTime = 0;
    UINT1               u1RtEntrtyCount = 0;
    UINT1               u1Cntr = 0;

    MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));
    u1RtEntrtyCount = TMO_SLL_Count (&Nd6RouteList);

    for (u1Cntr = 0; u1Cntr < u1RtEntrtyCount; u1Cntr++)
    {
        pNode = TMO_SLL_Next (&Nd6RouteList, pNd6RtEntry);
        pNd6RtEntry = IP6_GET_DEF_RTR_LST_FROM_ROUTE (pNode);
        /* As the destination address is 0::0 the prefix len is ignored */
        if (MEMCMP (&(pNd6RtEntry->Dst), &Ip6Addr, sizeof (tIp6Addr)) == 0)
        {
            if (TmrGetRemainingTime (gIp6GblInfo.Ip6TimerListId,
                                     (tTmrAppTimer *)
                                     & pNd6RtEntry->RtEntryTimer.appTimer,
                                     (UINT4 *) &i4TempRemainingTime)
                == TMR_FAILURE)
            {
                continue;
            }

            if (i4TempRemainingTime > i4RemainingTime)
            {
                i4RemainingTime = i4TempRemainingTime;
                pNd6DelRtEntry = pNd6RtEntry;
            }
        }
    }

    if (pNd6DelRtEntry != NULL)
    {
        Nd6TimeOutDefRoute (pNd6DelRtEntry);
    }

    return IP6_SUCCESS;
}

/*****************************************************************************
DESCRIPTION         This routine is called when ever preffered timer of an 
                    Ipv6 address prefix expires. It changes the state of the 
                    address to deprecated.

INPUTS              pTimerNode - pointer to the preffred timer which has been 
                                 expired.

OUTPUTS             None.

RETURNS             None.
****************************************************************************/
VOID
Nd6AddrPrefLifeTimeHandler (tIp6Timer * pTimerNode)
{
    tIp6AddrInfo       *pAddrInfo;

    /* Get address from the timer */
    pAddrInfo = IP6_ADDR_FROM_PREF_TIMER (pTimerNode);

    Ip6TmrStop (pAddrInfo->pIf6->pIp6Cxt->u4ContextId,
                IP6_PREF_LIFETIME_TIMER_ID, gIp6GblInfo.Ip6TimerListId,
                &(pAddrInfo->AddrPrefTimer.appTimer));

    /* Set the address status to deprecated */
    pAddrInfo->u1Status &= ADDR6_DEPRECATED;

    return;
}

/*****************************************************************************
DESCRIPTION         This routine is called when ever valid timer of an 
                    Ipv6 address prefix expires. It deletes the address 
                    prefix on the interface.

INPUTS              pTimerNode - pointer to the valid timer which has been 
                                 expired.

OUTPUTS             None.

RETURNS             None.
****************************************************************************/
VOID
Nd6AddrValidLifeTimeHandler (tIp6Timer * pTimerNode)
{
    tIp6AddrInfo       *pAddrInfo;

    /* Get address from the timer */
    pAddrInfo = IP6_ADDR_FROM_VALID_TIMER (pTimerNode);

    /* Address is no more valid so delete the address */
    Ip6AddrDelete (pAddrInfo->pIf6->u4Index, &pAddrInfo->ip6Addr,
                   pAddrInfo->u1PrefLen, 1);

    return;
}

/*****************************************************************************
DESCRIPTION         This routine is called when ever an Nd cache entry is going
                    to be purged. This function deletes the destination cache 
                    entries which are are pointing to this ND cache entries, 
                    so that the unreachable next-hop will not be probed 
                    unnecessarily there after. If it becomes reachable that 
                    will be learnt by succeding RAs or Redirects. 

INPUTS              pNd6cEntry - pointer to the ND cache entry which is going 
                                 to be purged.

OUTPUTS             None.

RETURNS             None.
****************************************************************************/
VOID
Ip6UpdateRoutingDataBase (tNd6CacheEntry * pNd6cEntry)
{
    tNd6RtEntry        *pNd6RtEntry = NULL;
    tIp6If             *pIf6 = NULL;
    tIp6Addr            Ip6Addr;
    UINT4               u4Index = 0;

    /* When ever a NDcache entry is purged the destination cache should also 
     * be purged, other wise the unreachable nexthop will always be probed
     * resulting unnecessary NSs and at the same time the packets destined 
     * to that destination are always last even there are other paths to 
     * reach the destination (say a default router).
     */
    pIf6 = pNd6cEntry->pIf6;
    u4Index = pIf6->u4Index;
    MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));

    pNd6RtEntry = ND6GetRouteEntry (&Ip6Addr, 0, &(pNd6cEntry->addr6), u4Index);

    if (pNd6RtEntry == NULL)
    {
        IP6_TRC_ARG1 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC,
                      ND6_NAME,
                      "Ip6UpdateRoutingDataBase: No default routes exist"
                      " for the address %s\n",
                      Ip6PrintAddr (&(pNd6cEntry->addr6)));
        return;
    }
    Nd6TimeOutDefRoute (pNd6RtEntry);

    return;
}

/*****************************************************************************
* DESCRIPTION : This function forms and sends router solicitation
*
* INPUTS      : pIf6    - Pointer to interface structure over which RS needs 
*                         to be sent
* OUTPUTS     : None. 
* RETURNS     : IP6_SUCCESS or IP6_FAILURE.
*****************************************************************************/
INT4
Nd6SendRouterSol (tIp6If * pIf6, tIp6Addr * pDest)
    /* pDest - Used when RS needs to be sent to unicast destination
     * address of a router. It is useful in router discovery*/
{
    /* Flag indicating to include SLLA option */
    UINT1               u1Flag = 0;
    UINT4               u4Woffset;
    UINT4               u4Nd6Size;
    UINT1               u1SeNDFlag = 0;
    UINT4               u4TotalSize;
    tNd6CacheEntry     *pNd6cEntry = NULL;
    tIp6Addr           *pSrcAddr6;
    tIp6Addr            SrcAddr6;
    tIp6Addr            DstAddr6;
    tIp6Cxt            *pIp6Cxt = NULL;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;

#ifndef MN_WANTED
    UNUSED_PARAM (pDest);
#endif
    if (pIf6 == NULL)
    {
        IP6_GBL_TRC_ARG (ND6_MOD_TRC, ALL_FAILURE_TRC | BUFFER_TRC, ND6_NAME,
                         "ND6: Nd6SendRouterSol:Send RS - Wrong If is specified \n");
        return IP6_FAILURE;
    }

    u4Woffset = Ip6BufWoffset (ND6_MODULE);
#ifdef MN_WANTED
    if (pDest != NULL)
    {
        if ((pNd6cEntry = Nd6IsCacheForAddr (pIf6, pDest)) == NULL)
        {
            return IP6_FAILURE;
        }
        MEMCPY (&DstAddr6, pDest, sizeof (tIp6Addr));
    }
    else
#endif
        SET_ALL_ROUTERS_MULTI (DstAddr6);

    /* Allocate a Buffer for the IPv6 packet size of RS message */
    u4Nd6Size = sizeof (tNd6RoutSol) + sizeof (tNd6AddrExt);

    u4TotalSize = u4Woffset + u4Nd6Size;

    /*
     * If SeND is enabled, increase the pBuf size to accommodate
     * all the SeND options
     */
    if (IP6_TRUE == IF_SEND_ENABLED (pIf6))
    {
        u4TotalSize += ND6_OPTIONS_LENGTH;
        u1SeNDFlag = ND6_SEND_MSGS;
    }

    if ((pBuf = Ip6BufAlloc (pIf6->pIp6Cxt->u4ContextId, u4TotalSize,
                             u4Woffset, ND6_MODULE)) == NULL)
    {
        IP6_TRC_ARG (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                     BUFFER_TRC | ALL_FAILURE_TRC, ND6_NAME,
                     "ND6: Nd6SendRouterSol:Send RS - BufAlloc Failed\n");
        IP6_TRC_ARG3 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC, BUFFER_TRC,
                      IP6_NAME, "%s buf err: Type = %d  Bufptr = %p",
                      ERROR_FATAL_STR, ERR_BUF_ALLOC, NULL);
        return (IP6_FAILURE);
    }

    /* Source Address of RS should be either the Global unicast address or
     * unspecified Address. */
    pSrcAddr6 = Ip6GetGlobalAddr (pIf6->u4Index, &DstAddr6);

    /* In case no address is available source address should 
     * be set to unspecified.
     */
    if ((pSrcAddr6 != NULL) && (!IS_ADDR_UNSPECIFIED (*pSrcAddr6)))
    {
        /* Address is not unspecified , so we can include SLLA */
        u1Flag = 1;
    }
    else
    {
        u4Nd6Size -= sizeof (tNd6AddrExt);
        pSrcAddr6 = &SrcAddr6;
        SET_ADDR_UNSPECIFIED (*pSrcAddr6);
    }
    Nd6FillRouterSol (u1Flag, pIf6, pBuf);

    IP6_TRC_ARG1 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC,
                  IP6_NAME, "SEND RS to %s\n", Ip6PrintAddr (&DstAddr6));

    pIp6Cxt = pIf6->pIp6Cxt;
    (IP6_IF_STATS (pIf6))->u4OutRoutSols++;
    ICMP6_INC_OUT_RSOLS (pIp6Cxt->Icmp6Stats);

#ifdef TUNNEL_WANTED
    /* In case of tunnel interface pkt need to be enqueued to V4 task */
    if (pIf6->u1IfType == IP6_TUNNEL_INTERFACE_TYPE)
    {
        return (Ip6SendNdMsgOverTunl (pIf6, pNd6cEntry, pSrcAddr6,
                                      &DstAddr6, u4Nd6Size, pBuf));
    }
    else
#endif
    {
        return (Ip6SendNdMsg
                (pIf6, pNd6cEntry, pSrcAddr6, &DstAddr6, u4Nd6Size,
                 pBuf, u1SeNDFlag));
    }
}

/*****************************************************************************
* DESCRIPTION : Fill the RS message fields 
*
* INPUTS      : u1Flag      -  Flag indicating whether to include SLLA or not.
*               pIf6        -  Pointer to the interface over whcih RS needs 
*                              to be sent.
*               pBuf        - Pointer to the message buffer. 
*
* OUTPUTS     : None.
* 
* RETURNS     : IP6_SUCCESS or IP6_FAILURE.
*****************************************************************************/
PRIVATE INT4
Nd6FillRouterSol (UINT1 u1Flag, tIp6If * pIf6, tCRU_BUF_CHAIN_HEADER * pBuf)
{
    UINT1               u1Copy = 1;
    UINT1               u1LlaLen = 0, lladdr[IP6_MAX_LLA_LEN];
    tNd6RoutSol         nd6Rsol, *pNd6Rsol;

    pNd6Rsol = (tNd6RoutSol *) & nd6Rsol;
    pNd6Rsol->icmp6Hdr.u1Type = ND6_ROUTER_SOLICITATION;
    pNd6Rsol->icmp6Hdr.u1Code = ND6_RSVD_CODE;    /* Should be 0 */
    pNd6Rsol->icmp6Hdr.u2Chksum = 0;
    pNd6Rsol->u4Rsvd = 0;

    if (Ip6BufWrite (pBuf,
                     (UINT1 *) pNd6Rsol, IP6_BUF_WRITE_OFFSET (pBuf),
                     sizeof (tNd6RoutSol), u1Copy) == IP6_FAILURE)
    {
        IP6_TRC_ARG2 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC,
                      ND6_NAME,
                      "ND6:Nd6FillRouterSol: Fill RS - BufWrite Failed! "
                      "BufPtr %p Offset %d\n", pBuf,
                      IP6_BUF_WRITE_OFFSET (pBuf));
        IP6_TRC_ARG3 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC, BUFFER_TRC,
                      IP6_NAME, "%s buf err: Type = %d  Bufptr = %p",
                      ERROR_FATAL_STR, ERR_BUF_WRITE, pBuf);
        Ip6BufRelease (pIf6->pIp6Cxt->u4ContextId, pBuf, FALSE, ND6_MODULE);
        return (IP6_FAILURE);
    }
    /*
     * Here a check is to be made, such that - if the v6 Source Address
     * is unspecified, do not use the link-layer address option.
     * So, added additional check so that, The LL Address option would
     * be filled only if the Source address is not Unspecified.
     */
    if (u1Flag)
    {
        Ip6ifGetEthLladdr (pIf6, lladdr, &u1LlaLen);
        if (Nd6FillLladdrInCxt (pIf6->pIp6Cxt, lladdr,
                                ND6_SRC_LLA_EXT, pBuf) == IP6_FAILURE)
            return (IP6_FAILURE);
    }

    return (IP6_SUCCESS);

}

/*****************************************************************************
* DESCRIPTION : Receives the RS message 
*
* INPUTS      : pIp6        -  Pointer to IP6 Header 
*               pIf6        -  Pointer to the interface over whcih RS needs 
*                              to be sent.
*               u2Len       - Length of the Packet
*               pBuf        - Pointer to the message buffer. 
*
* OUTPUTS     : None.
* 
* RETURNS     : IP6_SUCCESS or IP6_FAILURE.
*****************************************************************************/
INT4
Nd6RcvRoutSol (tIp6If * pIf6, tIp6Hdr * pIp6, UINT2 u2Len,
               tCRU_BUF_CHAIN_HEADER * pBuf)
{
    /* HOST - Doesn't process RS, Buffer release is done 
     * in the calling function.
     */
    UNUSED_PARAM (pIf6);
    UNUSED_PARAM (pIp6);
    UNUSED_PARAM (u2Len);
    UNUSED_PARAM (pBuf);
    return IP6_FAILURE;
}

/*****************************************************************************
* DESCRIPTION : ND timer handler routine
*                       
* INPUTS      : u1TimerId  -   Timer Id
*               pNd6Timer  -   Pointer to ND timer node
* 
* OUTPUTS     : None
* 
* RETURNS     : None
*****************************************************************************/
VOID
Nd6Timeout (UINT1 u1TimerId, tIp6Timer * pNd6Timer)
{
    tIp6If             *pIf6;
    tNd6CacheLanInfo   *pNd6cLinfo;

    switch (u1TimerId)
    {
        case ND6_RETRANS_TIMER_ID:
        case ND6_REACH_TIMER_ID:
        case ND6_DELAY_PROBE_TIMER_ID:
            pNd6cLinfo = ND6C_LAN_INFO_FROM_TIMER (pNd6Timer);
            Nd6CacheTimeout (u1TimerId, pNd6cLinfo->pNd6cEntry);
            break;
        case ND6_ROUT_ADV_SOL_TIMER_ID:
            pIf6 = IP6_IF_PTR_FROM_TIMER (pNd6Timer);
            Nd6RsTimeout (pIf6);
            break;
        default:
            break;
    }

}

/*****************************************************************************
* DESCRIPTION : Performs time-out actions specific
*               to Router Solicitation timer expiry
* 
* INPUTS      : pIf6        -  Pointer to the interface 
*
* OUTPUTS     : None
* 
* RETURNS     : None
*****************************************************************************/

VOID
Nd6RsTimeout (tIp6If * pIf6)
{

    if (pIf6 == NULL)
        return;

    if (pIf6->u1AdminStatus != ADMIN_UP)
        return;

    if (pIf6->u1RsFlag == IP6_IF_NO_RS_SEND)
    {
        /* RA has been received already, so must desist to send further RSs */
        return;
    }

    /*
     * Sends RS and schedules the next RS by starting the RA&RS timer
     * for the computed random time
     */
    if (Nd6SendRouterSol (pIf6, NULL) == IP6_FAILURE)
    {
        IP6_TRC_ARG1 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC,
                      ND6_NAME,
                      "ND6:Nd6RsTimeout: RS Timeout - Send RS failed on IF %d\n",
                      pIf6->u4Index);
        return;
    }
    (IP6_SEND_IF_OUT_STATS (pIf6, COUNT_RS)).u4CgaOptPkts++;
    (IP6_SEND_IF_OUT_STATS (pIf6, COUNT_RS)).u4RsaOptPkts++;
    (IP6_SEND_IF_OUT_STATS (pIf6, COUNT_RS)).u4TimeStampOptPkts++;
    (IP6_SEND_IF_OUT_STATS (pIf6, COUNT_RS)).u4NonceOptPkts++;
    Nd6SchedNextRoutSol (pIf6);

}

/*****************************************************************************
* DESCRIPTION : 
* 
* INPUTS      : pIf6        -  Pointer to the interface 
*
* OUTPUTS     : None
* 
* RETURNS     : None
*****************************************************************************/
VOID
Nd6SchedNextRoutSol (tIp6If * pIf6)
{
    tIp6Timer          *pRsTimer;
    UINT4               u4CurrentTime;

    /* Check whether any RA has been received , if so no further RS 
     * needs to be sent.
     */
    if (pIf6->u1RsFlag == IP6_IF_NO_RS_SEND)
    {
        /* RA has been received already, so must desist to send further RSs */
        return;
    }

    /*
     * Checks whether the number of RSs sent is less than MAX INITIAL RSS
     * and increments the count, initialises the RS timer to RS interval.
     */
    if (IP6_IF_RS_INITIAL_CNT (pIf6) < MAX_RTR_SOLICITATIONS)
    {
        IP6_IF_RS_INITIAL_CNT (pIf6)++;
    }

    if (IP6_IF_RS_INITIAL_CNT (pIf6) != MAX_RTR_SOLICITATIONS)
    {
#ifdef MIP6_WANTED
        IP6_TRC_ARG1 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC,
                      ND6_NAME,
                      "ND6:Nd6SchedNextRoutSol: Schedule Next RS - RS time %d\n",
                      (RTR_SOLICITATION_INTERVAL (pIf6)));
#else
        IP6_TRC_ARG1 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC,
                      ND6_NAME,
                      "ND6:Nd6SchedNextRoutSol: Schedule Next RS - RS time %d\n",
                      RTR_SOLICITATION_INTERVAL);
#endif

        /* Start the RA timer for the computed RA time period */
        pRsTimer = IP6_IF_RS_TIMER (pIf6);
        Ip6TmrStart (pIf6->pIp6Cxt->u4ContextId, ND6_ROUT_ADV_SOL_TIMER_ID,
                     gIp6GblInfo.Ip6TimerListId, &(pRsTimer->appTimer),
#ifdef MIP6_WANTED
                     RTR_SOLICITATION_INTERVAL (pIf6));
#else
                     RTR_SOLICITATION_INTERVAL);
#endif
        OsixGetSysTime (&u4CurrentTime);
        IP6_IF_RS_SCHED_TIME (pIf6) = u4CurrentTime +
#ifdef MIP6_WANTED
            RTR_SOLICITATION_INTERVAL (pIf6);
#else
            RTR_SOLICITATION_INTERVAL;
#endif
    }
}

/*****************************************************************************
* DESCRIPTION : Decides on sending of ND messages based on the
*               interface status, handles the Router Advertisement
*               timer and purges the NDCache entries of the interface
*               upon interface down/delete indication
*                    
* INPUTS      : pIf6        -   Pointer to the interface 
*               u1Status    -   Status of the interface
* 
* OUTPUTS     : None
* 
* RETURNS     : None
*****************************************************************************/
VOID
Nd6ActOnIfstatus (tIp6If * pIf6, UINT1 u1Status)
{

    IP6_TRC_ARG3 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC,
                  ND6_NAME,
                  "ND6: Nd6ActOnIfstatus:Indication for IF %d Type %d Status %d\n",
                  pIf6->u4Index, pIf6->u1IfType, u1Status);

    if (pIf6->u1IfType != IP6_ENET_INTERFACE_TYPE &&
        pIf6->u1IfType != IP6_L3VLAN_INTERFACE_TYPE &&
        pIf6->u1IfType != IP6_LAGG_INTERFACE_TYPE &&
        pIf6->u1IfType != IP6_PSEUDO_WIRE_INTERFACE_TYPE &&
        pIf6->u1IfType != IP6_X25_INTERFACE_TYPE &&
        (pIf6->u1IfType != IP6_L3SUB_INTF_TYPE) &&
#ifdef TUNNEL_WANTED
        pIf6->u1IfType != IP6_TUNNEL_INTERFACE_TYPE &&
#endif /* TUNNEL_WANTED */
#ifdef WGS_WANTED
        (pIf6->u1IfType != IP6_L2VLAN_INTERFACE_TYPE) &&
#endif
        pIf6->u1IfType != IP6_FR_INTERFACE_TYPE)
    {

        return;
    }

    switch (u1Status)
    {
        case IP6_IF_UP:
            /*
             * Oper Up indication is received for LAN interfaces
             * only upon link-local address being a VALID address
             * assigned (DAD completed) on the interface
             */
            if ((pIf6->u1IfType == IP6_ENET_INTERFACE_TYPE) ||
#ifdef TUNNEL_WANTED
                (pIf6->u1IfType == IP6_TUNNEL_INTERFACE_TYPE) ||
#endif /* TUNNEL_WANTED */
#ifdef WGS_WANTED
                (pIf6->u1IfType == IP6_L2VLAN_INTERFACE_TYPE) ||
#endif
                (pIf6->u1IfType == IP6_PSEUDO_WIRE_INTERFACE_TYPE) ||
                (pIf6->u1IfType == IP6_L3VLAN_INTERFACE_TYPE) ||
                (pIf6->u1IfType == IP6_LAGG_INTERFACE_TYPE) ||
                (pIf6->u1IfType == IP6_L3SUB_INTF_TYPE))
            {
                /*  Send the router solicitation and schedule
                 * the sending of next router solicitation  */
                Nd6SendRouterSol (pIf6, NULL);
                Nd6SchedNextRoutSol (pIf6);

            }
            break;

        case IP6_IF_DOWN:
            /*
             * Upon Admin/Oper down indication, the dynamic NDCache entries 
             * on that interface are purged out. For LAN interfaces, RA
             * periodic timer is stopped and RA is sent with lifetime Zero.
             * Also stop the interface's RA/RS timer.
             */
            if ((IP6_IF_RS_TIMER (pIf6))->u1Id != 0)
            {
                Ip6TmrStop (pIf6->pIp6Cxt->u4ContextId,
                            ND6_ROUT_ADV_SOL_TIMER_ID,
                            gIp6GblInfo.Ip6TimerListId,
                            &(IP6_IF_RS_TIMER (pIf6))->appTimer);
            }

            Nd6PurgeCacheOnIface (pIf6, ND6C_PURGE_DYNAMIC);
            break;

        case IP6_IF_DELETE:
            /*
             * Upon Interface Delete indication, all the NDCache 
             * static/dynamic entries on that interface are purged out.
             * Also stop the interface's RA/RS timer.
             */
            if ((IP6_IF_RS_TIMER (pIf6))->u1Id != 0)
            {
                Ip6TmrStop (pIf6->pIp6Cxt->u4ContextId,
                            ND6_ROUT_ADV_SOL_TIMER_ID,
                            gIp6GblInfo.Ip6TimerListId,
                            &(IP6_IF_RS_TIMER (pIf6))->appTimer);
            }

            Nd6PurgeCacheOnIface (pIf6, ND6C_PURGE_ALL);
            break;

        case IP6_IF_UNMAP:
            /*
             * When an interface is unmapped, the ND cache entries
             * learnt on that interface in the previous virtual router have
             * to be purged. The RA configurations done on the interface
             * are not dependent on the virtual router, hence they are not
             * cleared.
             */
            Nd6PurgeCacheOnIface (pIf6, ND6C_PURGE_ALL);
            break;

        default:
            break;

    }

}

/*****************************************************************************
* DESCRIPTION : Deletes the NDCache entry and releases
*               the queued IPv6 packets if any
* 
* INPUTS      : pNd6cEntry   -  Pointer to the NDCache Entry
* 
* OUTPUTS     : None
* 
* RETURNS     : IP6_SUCCESS         -  ND entry deletion succeeds
*             : IP6_FAILURE     -  Deletion fails
*****************************************************************************/

INT4
Nd6PurgeCache (tNd6CacheEntry * pNd6cEntry)
{

    tIp6If             *pIf6 = NULL;
    tNd6CacheLanInfo   *pNd6cLinfo = NULL;
    UINT4               u4ContextId = 0;

    if (pNd6cEntry == NULL)
        return (IP6_FAILURE);

    pIf6 = pNd6cEntry->pIf6;
    u4ContextId = pIf6->pIp6Cxt->u4ContextId;
    IP6_TRC_ARG1 (u4ContextId, ND6_MOD_TRC, MGMT_TRC, ND6_NAME,
                  "ND6: Nd6PurgeCache:Purging Cache entry %p\n", pNd6cEntry);

    /*
     * Stop the ND Timer running and release the queued packets if any
     * on dynamic LAN Cache entry
     */

    /* ND cache entries can exist for either tunnel or ethernet interface */

    if ((pIf6->u1IfType == IP6_ENET_INTERFACE_TYPE)
        || (pIf6->u1IfType == IP6_L3VLAN_INTERFACE_TYPE)
        || (pIf6->u1IfType == IP6_LAGG_INTERFACE_TYPE)
        || (pIf6->u1IfType == IP6_PSEUDO_WIRE_INTERFACE_TYPE)
        || (pIf6->u1IfType == IP6_L3SUB_INTF_TYPE)
#ifdef TUNNEL_WANTED
        || (pIf6->u1IfType == IP6_TUNNEL_INTERFACE_TYPE)
#endif
#ifdef WGS_WANTED
        || (pIf6->u1IfType == IP6_L2VLAN_INTERFACE_TYPE)
#endif
        )
    {
        pNd6cLinfo = (tNd6CacheLanInfo *) pNd6cEntry->pNd6cInfo;
        if ((pNd6cEntry->u1ReachState != ND6C_STATIC) &&
            (pNd6cEntry->u1ReachState != ND6C_STATIC_NOT_IN_SERVICE))
        {
            Nd6CancelCacheTimer (pNd6cEntry);
            Nd6PurgePendQue (pNd6cEntry);
        }
    }

    Ip6UpdateRoutingDataBase (pNd6cEntry);

#ifdef NPAPI_WANTED
    Ipv6FsNpIpv6NeighCacheDel (u4ContextId, (UINT1 *) &pNd6cEntry->addr6,
                               pIf6->u4Index);
#endif /* NPAPI_WANTED */

    RBTreeRem (gNd6GblInfo.Nd6CacheTable, pNd6cEntry);

    if (Ip6RelMem
        (u4ContextId, (UINT2) gNd6GblInfo.i4Nd6CacheId,
         (UINT1 *) pNd6cEntry) == IP6_FAILURE)
    {
        return (IP6_FAILURE);
    }

    /* ND cache entries can exist for either tunnel or ethernet interface */
    if ((pIf6->u1IfType == IP6_ENET_INTERFACE_TYPE)
        || (pIf6->u1IfType == IP6_L3VLAN_INTERFACE_TYPE)
        || (pIf6->u1IfType == IP6_LAGG_INTERFACE_TYPE)
        || (pIf6->u1IfType == IP6_PSEUDO_WIRE_INTERFACE_TYPE)
        || (pIf6->u1IfType == IP6_L3SUB_INTF_TYPE)
#ifdef TUNNEL_WANTED
        || (pIf6->u1IfType == IP6_TUNNEL_INTERFACE_TYPE)
#endif
#ifdef WGS_WANTED
        || (pIf6->u1IfType == IP6_L2VLAN_INTERFACE_TYPE)
#endif
        )
    {
        if (Ip6RelMem (pIf6->pIp6Cxt->u4ContextId,
                       (UINT2) gNd6GblInfo.i4Nd6cLanId,
                       (UINT1 *) pNd6cLinfo) == IP6_FAILURE)
            return (IP6_FAILURE);
    }

    gNd6GblInfo.u4Nd6CacheEntries--;

    return (IP6_SUCCESS);

}

/*****************************************************************************
FUNCTION            Ip6GetIfReTransTime 
DESCRIPTION         This function give the If Retransmit time from IfTabl 
                    Default Retransmit Time for the NODE if not.

Input(s)            u4IfIndex   -   If Index 
Output(s)           None

RETURNS             Reachable Time in MilliSeconds
****************************************************************************/
UINT4
Ip6GetIfReTransTime (UINT4 u4IfIndex)
{
    /* Get the ReTrans Time from the If Table */
    tIp6If             *pIf6 = NULL;

    pIf6 = Ip6ifGetEntry (u4IfIndex);
    if (pIf6 == NULL)
    {
        return (RETRANS_TIMER);
    }
    return ((pIf6->u4Rettime / 1000));
}

/*****************************************************************************
FUNCTION            Ip6GetIfReachTime   
DESCRIPTION         This function give the If Reachable time from IfTabl 
                    Default Reachable for the NODE if not.

Input(s)            u4IfIndex   -   If Index 
Output(s)           None

RETURNS             Reachable Time in MilliSeconds
****************************************************************************/
UINT4
Ip6GetIfReachTime (UINT4 u4IfIndex)
{
    /* Get the Reachable Time from the If Table */
    tIp6If             *pIf6 = NULL;

    pIf6 = Ip6ifGetEntry (u4IfIndex);
    if (pIf6 == NULL)
    {
        return (REACHABLE_TIME);
    }
    return (pIf6->u4Reachtime);
}

/***************************************************************************
FUNCTION            ND6RouteInit        
DESCRIPTION         This function allocates memory for the ND route entries.
Input(s)            None                        
Output(s)           None
RETURNS             IP6_SUCCESS / IP6_FAILURE
****************************************************************************/
INT4
ND6RouteInit ()
{
    TMO_SLL_Init (&Nd6RouteList);

    if (Ip6hostSizingMemCreateMemPools () == OSIX_FAILURE)
    {
        IP6_GBL_TRC_ARG (ND6_MOD_TRC, MGMT_TRC | ALL_FAILURE_TRC, ND6_NAME,
                         "Nd6HostInit: Creation of ND Route MemPool Failed!!\n");
        return (IP6_FAILURE);
    }

    gi4ND6RouteId = IP6HOSTMemPoolIds[MAX_IP6HOST_ND6_ROUTES_SIZING_ID];
    return (IP6_SUCCESS);
}

/***************************************************************************
FUNCTION            ND6GetRouteEntry    
DESCRIPTION         This function retrives the ND route entry added for the    
                     given destination.
Input(s)            pDstAddr  - Destination address of the route
                    u1Prefixlen - Prefix Length
                    pNextHop - NextHop of the ND route.
                    u4Index - Interface Index
Output(s)           None
RETURNS             ND RouteEntry / NULL            
****************************************************************************/
PRIVATE tNd6RtEntry *
ND6GetRouteEntry (tIp6Addr * pDstAddr, UINT1 u1Prefixlen,
                  tIp6Addr * pNextHop, UINT4 u4Index)
{
    tNd6RtEntry        *pNd6RtEntry = NULL;
    tTMO_SLL_NODE      *pNode = NULL;
    UINT1               u1RtEntrtyCount = 0;
    UINT1               u1Cntr = 0;

    u1RtEntrtyCount = TMO_SLL_Count (&Nd6RouteList);

    for (u1Cntr = 0; u1Cntr < u1RtEntrtyCount; u1Cntr++)
    {
        pNode = TMO_SLL_Next (&Nd6RouteList, pNd6RtEntry);
        if (pNode == NULL)
        {
            break;
        }
        pNd6RtEntry = IP6_GET_DEF_RTR_LST_FROM_ROUTE (pNode);
        if ((MEMCMP (&(pNd6RtEntry->NextHop), pNextHop,
                     sizeof (tIp6Addr)) == 0) &&
            (MEMCMP (&(pNd6RtEntry->Dst), pDstAddr, sizeof (tIp6Addr)) == 0) &&
            (pNd6RtEntry->u1PrefLen == u1Prefixlen))
        {
            if ((u4Index == 0) || (pNd6RtEntry->u4IfIndex == u4Index))
            {
                return pNd6RtEntry;
            }
        }
    }

    return NULL;
}

/***************************************************************************
FUNCTION            Nd6AddRtEntry       
DESCRIPTION         This function adds a new ND route to the route list and 
                     starts the timer for the route for the given time.
Input(s)            pDstAddr  - Destination address of the route
                    u1Prefixlen - Prefix Length
                    pNextHop - NextHop of the ND route.
                    u4IfIndex - Interface on which the route is learnt
                    u2LifeTime - Valid lifetime for the route
Output(s)           None
RETURNS             IP6_SUCCESS / IP6_FAILURE
****************************************************************************/
PRIVATE INT4
Nd6AddRtEntry (tIp6Addr * pDstAddr, UINT1 u1Prefixlen, tIp6Addr * pNextHop,
               UINT4 u4IfIndex, UINT2 u2LifeTime)
{
    tNd6RtEntry        *pNd6RtEntry = NULL;
    tNd6RtEntry        *pNd6PrevRtEntry = NULL;
    tNd6RtEntry        *pNd6CurrRtEntry = NULL;
    tTMO_SLL_NODE      *pNode = NULL;
    UINT4               u4ContextId;
    INT1                i1RetVal = 0;
    UINT1               u1RtEntrtyCount = 0;
    UINT1               u1Cntr = 0;

    Ip6GetCxtId (u4IfIndex, &u4ContextId);
    pNd6RtEntry = (tNd6RtEntry *) Ip6GetMem (u4ContextId, gi4ND6RouteId);
    if (pNd6RtEntry == NULL)
    {
        IP6_TRC_ARG (u4ContextId, ND6_MOD_TRC, ALL_FAILURE_TRC, ND6_NAME,
                     "Nd6AddRtEntry: Creating ND RtEntry Failed "
                     " - Memory Allocation FAILED\n");
        return IP6_FAILURE;
    }

    MEMSET (pNd6RtEntry, 0, sizeof (tNd6RtEntry));
    Ip6AddrCopy (&(pNd6RtEntry->Dst), pDstAddr);
    pNd6RtEntry->u1PrefLen = u1Prefixlen;
    Ip6AddrCopy (&(pNd6RtEntry->NextHop), pNextHop);
    pNd6RtEntry->u4IfIndex = u4IfIndex;

    u1RtEntrtyCount = TMO_SLL_Count (&Nd6RouteList);

    for (u1Cntr = 0; u1Cntr < u1RtEntrtyCount; u1Cntr++)
    {
        pNode = TMO_SLL_Next (&Nd6RouteList, pNd6PrevRtEntry);
        if (pNode == NULL)
        {
            break;
        }
        pNd6CurrRtEntry = IP6_GET_DEF_RTR_LST_FROM_ROUTE (pNode);
        i1RetVal = MEMCMP (&(pNd6CurrRtEntry->NextHop), pNextHop,
                           sizeof (tIp6Addr));
        if (((i1RetVal == 0) && (pNd6CurrRtEntry->u4IfIndex > u4IfIndex)) ||
            (i1RetVal > 0))
        {
            break;
        }
        pNd6PrevRtEntry = pNd6CurrRtEntry;
    }

    TMO_SLL_Insert (&Nd6RouteList, (tTMO_SLL_NODE *) pNd6PrevRtEntry,
                    (tTMO_SLL_NODE *) pNd6RtEntry);

    /* start the invalidation timer */
    if (u2LifeTime != (UINT2) IP6_DEF_ROUTERS_LFE_TIME_INFINITY)
    {
        Ip6TmrStart (u4ContextId, IP6_DEF_RTR_LIFE_TIMER_ID,
                     gIp6GblInfo.Ip6TimerListId,
                     (tTmrAppTimer *) & pNd6RtEntry->RtEntryTimer.appTimer,
                     u2LifeTime);
    }
    return IP6_SUCCESS;
}

/***************************************************************************
FUNCTION            Nd6DelRtEntry       
DESCRIPTION         This function deletes the ND route present in the route
                     list.                      
Input(s)            pDstAddr  - Destination address of the route
                    u1Prefixlen - Prefix Length
                    pNextHop - NextHop of the ND route.
Output(s)           None
RETURNS             IP6_SUCCESS / IP6_FAILURE
****************************************************************************/
PRIVATE INT4
Nd6DelRtEntryInCxt (tIp6Cxt * pIp6Cxt, tIp6Addr * pDstAddr,
                    UINT1 u1Prefixlen, tIp6Addr * pNextHop)
{
    tNd6RtEntry        *pNd6RtEntry = NULL;

    pNd6RtEntry = ND6GetRouteEntry (pDstAddr, u1Prefixlen, pNextHop, 0);
    if (pNd6RtEntry == NULL)
    {
        IP6_TRC_ARG1 (pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                      MGMT_TRC | ALL_FAILURE_TRC, ND6_NAME,
                      "Nd6DelRtEntry: No ND route entry exists for the "
                      "address %s\n", Ip6PrintAddr (pDstAddr));
        return IP6_FAILURE;
    }
    TMO_SLL_Delete (&Nd6RouteList, (tTMO_SLL_NODE *) pNd6RtEntry);
    if (pNd6RtEntry->RtEntryTimer.u1Id != 0)
    {
        Ip6TmrStop (pIp6Cxt->u4ContextId, IP6_DEF_RTR_LIFE_TIMER_ID,
                    gIp6GblInfo.Ip6TimerListId,
                    &pNd6RtEntry->RtEntryTimer.appTimer);
    }

    Ip6RelMem (pIp6Cxt->u4ContextId, gi4ND6RouteId, (UINT1 *) pNd6RtEntry);

    return IP6_SUCCESS;
}

/***************************************************************************
FUNCTION            ND6DeleteRoutes     
DESCRIPTION         This function deletes the ND routes learnt for the given
                     interface.
Input(s)            u4Index - The deleted interface index       
Output(s)           None
RETURNS             IP6_SUCCESS / IP6_FAILURE
****************************************************************************/
VOID
ND6DeleteRoutes (UINT4 u4Index)
{
    tNd6RtEntry        *pNd6RtEntry = NULL;
    tTMO_SLL_NODE      *pNode = NULL;
    UINT1               u1RtEntrtyCount = 0;
    UINT1               u1Cntr = 0;

    u1RtEntrtyCount = TMO_SLL_Count (&Nd6RouteList);

    for (u1Cntr = 0; u1Cntr < u1RtEntrtyCount; u1Cntr++)
    {
        pNode = TMO_SLL_Next (&Nd6RouteList, pNd6RtEntry);
        if (pNode == NULL)
        {
            break;
        }
        pNd6RtEntry = IP6_GET_DEF_RTR_LST_FROM_ROUTE (pNode);
        if (pNd6RtEntry->u4IfIndex == u4Index)
        {
            Nd6TimeOutDefRoute (pNd6RtEntry);
        }
        else if (pNd6RtEntry->u4IfIndex > u4Index)
        {
            break;
        }

    }

    return;

}

/***************************************************************************
FUNCTION            Nd6GetDefRtrLifetime 
DESCRIPTION         This function provides the available lifetime for the   
                     given default route.
Input(s)            i4Index - The interface index       
                    pIp6Addr - The next hop address
Output(s)           pu4LifeTime - The available def. route life time.
RETURNS             IP6_SUCCESS / IP6_FAILURE
****************************************************************************/
INT4
Nd6GetDefRtrLifetime (tIp6Addr * pIp6Addr, INT4 i4Index, UINT4 *pu4LifeTime)
{
    tIp6Addr            Ip6Addr;
    tNd6RtEntry        *pNd6RtEntry = NULL;
    tTMO_SLL_NODE      *pNode = NULL;
    UINT1               u1RtEntrtyCount = 0;
    UINT1               u1Cntr = 0;

    MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));

    u1RtEntrtyCount = TMO_SLL_Count (&Nd6RouteList);

    for (u1Cntr = 0; u1Cntr < u1RtEntrtyCount; u1Cntr++)
    {
        pNode = TMO_SLL_Next (&Nd6RouteList, pNd6RtEntry);
        if (pNode == NULL)
        {
            pNd6RtEntry = NULL;
            break;
        }
        pNd6RtEntry = IP6_GET_DEF_RTR_LST_FROM_ROUTE (pNode);
        if ((MEMCMP (&(pNd6RtEntry->NextHop), pIp6Addr,
                     sizeof (tIp6Addr)) == 0) &&
            ((INT4) pNd6RtEntry->u4IfIndex == i4Index))
        {
            break;
        }
    }

    if (pNd6RtEntry == NULL)
    {
        return IP6_FAILURE;
    }

    if (TmrGetRemainingTime (gIp6GblInfo.Ip6TimerListId,
                             (tTmrAppTimer *)
                             & pNd6RtEntry->RtEntryTimer.appTimer,
                             pu4LifeTime) == TMR_SUCCESS)
    {
        *pu4LifeTime /= (SYS_NUM_OF_TIME_UNITS_IN_A_SEC);
        return IP6_SUCCESS;
    }

    return IP6_FAILURE;
}

/***************************************************************************
FUNCTION            Nd6GetNextDefRtr     
DESCRIPTION         This function provides the available lifetime for the   
                     given default route.
Input(s)            i4Index - The interface index       
                    pIp6Addr - The next hop address
Output(s)           pu4LifeTime - The available def. route life time.
RETURNS             IP6_SUCCESS / IP6_FAILURE
****************************************************************************/
INT4
Nd6GetNextDefRtr (tIp6Addr * pIp6Addr, tIp6Addr * pNextIp6Addr,
                  INT4 i4RtrIfIndex, INT4 *pi4NextRtrIfIndex)
{
    tNd6RtEntry        *pNd6RtEntry = NULL;
    tTMO_SLL_NODE      *pNode = NULL;
    INT1                i1RetVal = 0;
    UINT1               u1RtEntrtyCount = 0;
    UINT1               u1Cntr = 0;

    MEMSET (pNextIp6Addr, 0, sizeof (tIp6Addr));
    *pi4NextRtrIfIndex = 0;
    u1RtEntrtyCount = TMO_SLL_Count (&Nd6RouteList);

    for (u1Cntr = 0; u1Cntr < u1RtEntrtyCount; u1Cntr++)
    {
        pNode = TMO_SLL_Next (&Nd6RouteList, pNd6RtEntry);
        if (pNode == NULL)
        {
            return IP6_FAILURE;
        }
        pNd6RtEntry = IP6_GET_DEF_RTR_LST_FROM_ROUTE (pNode);
        i1RetVal = MEMCMP (&(pNd6RtEntry->NextHop), pIp6Addr,
                           sizeof (tIp6Addr));
        if (((i1RetVal == 0) && ((INT4) pNd6RtEntry->u4IfIndex > i4RtrIfIndex))
            || (i1RetVal > 0))
        {
            break;
        }
    }
    if (u1Cntr < u1RtEntrtyCount)
    {
        Ip6AddrCopy (pNextIp6Addr, &(pNd6RtEntry->NextHop));
        *pi4NextRtrIfIndex = pNd6RtEntry->u4IfIndex;
        return IP6_SUCCESS;
    }
    return IP6_FAILURE;
}

/*****************************************************************************
* DESCRIPTION : This routine is called when an link-local address is deleted.
*               This routine stops the timer for sending the Router 
*               solicitation message.
*                    
* INPUTS      : pAddr6      -   Pointer to the deleted address
*               pIf6        -   Pointer to the associated interface
* 
* OUTPUTS     : None
* 
* RETURNS     : None
*****************************************************************************/
VOID
Nd6CeaseRAOnAddr (tIp6Addr * pAddr6, tIp6If * pIf6)
{
    UNUSED_PARAM (pAddr6);

    if ((IP6_IF_RS_TIMER (pIf6))->u1Id != 0)
    {
        Ip6TmrStop (pIf6->pIp6Cxt->u4ContextId,
                    ND6_ROUT_ADV_SOL_TIMER_ID,
                    gIp6GblInfo.Ip6TimerListId,
                    &(IP6_IF_RS_TIMER (pIf6))->appTimer);
    }
    return;
}

/**************************** END OF FILE *************************************/
