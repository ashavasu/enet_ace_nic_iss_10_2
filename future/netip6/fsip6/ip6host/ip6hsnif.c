/*
 *----------------------------------------------------------------------------- 
 *    FILE NAME                    :    ip6hsnif.c
 *
 *    PRINCIPAL AUTHOR             :    Jayabharathi R.
 *
 *    SUBSYSTEM NAME               :    IPv6
 *
 *    MODULE NAME                  :    IP6 Interface Submodule
 *
 *    LANGUAGE                     :    C
 *
 *    TARGET ENVIRONMENT           :    UNIX
 *
 *    DATE OF FIRST RELEASE        :    DDD-MM-1996
 *
 *    DESCRIPTION                  :    This file contains routines called from  *                                      low-level routines. This routines will 
 *                                      appropriate action when the mib-object 
 *                                      set for host.*
 *----------------------------------------------------------------------------- 
 */

#include "ip6inc.h"

/******************************************************************************
 * DESCRIPTION : This routine is called from low-level routine for setting 
 *               the IPv6 Forwarding Status.
 *
 * INPUTS      : IPv6 Forwarding Status (u4ForwStatus)
 *
 * OUTPUTS     : None 
 *
 * RETURNS     : IP6_SUCCESS or IP6_FAILURE
 *               
 *
 * NOTES       : 
 ******************************************************************************/
INT1
Ip6SetIpv6ForwardingInCxt (tIp6Cxt * pIp6Cxt, UINT4 u4ForwStatus)
{
    UNUSED_PARAM (pIp6Cxt);
    if (u4ForwStatus == IP6_FORW_DISABLE)
    {
        return IP6_SUCCESS;
    }

    /* Forwarding cannot be enabled for HOST */
    return IP6_FAILURE;
}

/******************************************************************************
 * DESCRIPTION : This routine is called from low-level routine for getting the 
 *               interface Router Advertisement Status. Since This module Corresponds
 *               to HOST, this function always returns IP6_FAILURE.
 *
 * INPUTS      : i4Ipv6IfIndex - IP6 Interface Index.
 *
 * OUTPUTS     : pi4RetValIpv6IfRouterAdvStatus - Pointer to the Router 
 *                Advertisement Status of the interface.
 *
 * RETURNS     : IP6_FAILURE
 *               
 *
 * NOTES       : 
 ******************************************************************************/

INT1
Ip6GetFsipv6IfRouterAdvStatus (INT4 i4Ipv6IfIndex,
                               INT4 *pi4RetValIpv6IfRouterAdvStatus)
{
    UNUSED_PARAM (i4Ipv6IfIndex);
    UNUSED_PARAM (pi4RetValIpv6IfRouterAdvStatus);
    return IP6_FAILURE;
}

/******************************************************************************
 * DESCRIPTION : This routine is called from low-level routine for getting the 
 *               Interface Router Advertisement Flag. Since This module
 *               Corresponds to HOST, this function always returns IP6_FAILURE.
 *
 * INPUTS      : i4Ipv6IfIndex - IP6 Interface Index.
 *
 * OUTPUTS     : pi4RetValIpv6IfRouterAdvFlags - Pointer to the Router 
 *               Advertisement Flag of the interface.
 *
 * RETURNS     : IP6_FAILURE
 *               
 *
 * NOTES       : 
 ******************************************************************************/
INT1
Ip6GetFsipv6IfRouterAdvFlags (INT4 i4Ipv6IfIndex,
                              INT4 *pi4RetValIpv6IfRouterAdvFlags)
{
    UNUSED_PARAM (i4Ipv6IfIndex);
    UNUSED_PARAM (pi4RetValIpv6IfRouterAdvFlags);
    return IP6_FAILURE;

}

/******************************************************************************
 * DESCRIPTION : This routine is called from low-level routine for setting the 
 *               Interface Router Advertisement Status. Since This module
 *               Corresponds to HOST, this function always returns IP6_FAILURE.
 *
 * INPUTS      : i4Ipv6IfIndex - IP6 Interface Index.
 *               i4SetValIpv6IfRouterAdvStatus - Router Advertisement Status.
 * OUTPUTS     : None.
 *
 * RETURNS     : IP6_FAILURE
 *               
 *
 * NOTES       : 
 ******************************************************************************/

INT1
Ip6SetFsipv6IfRouterAdvStatus (INT4 i4Ipv6IfIndex,
                               INT4 i4SetValIpv6IfRouterAdvStatus)
{
    UNUSED_PARAM (i4Ipv6IfIndex);
    UNUSED_PARAM (i4SetValIpv6IfRouterAdvStatus);
    return IP6_FAILURE;
}

/******************************************************************************
 * DESCRIPTION : This routine is called from low-level routine for setting the 
 *               Interface Router Advertisement Status. Since This module
 *               Corresponds to HOST, this function always returns IP6_FAILURE.
 *
 * INPUTS      : i4Ipv6IfIndex - IP6 Interface Index.
 *               i4SetValIpv6IfRouterAdvFlags - Router Advertisement Flag
 *
 * OUTPUTS     : None.
 *
 * RETURNS     : IP6_FAILURE
 *
 * NOTES       : 
 ******************************************************************************/
INT1
Ip6SetFsipv6IfRouterAdvFlags (INT4 i4Ipv6IfIndex,
                              INT4 i4SetValIpv6IfRouterAdvFlags)
{
    UNUSED_PARAM (i4Ipv6IfIndex);
    UNUSED_PARAM (i4SetValIpv6IfRouterAdvFlags);
    return IP6_FAILURE;
}

/******************************************************************************
 * DESCRIPTION : This routine is called from low-level Test routine of the 
 *               Interface Router Advertisement Status. Since This module
 *               Corresponds to HOST, this function always returns IP6_FAILURE.
 *
 * INPUTS      : i4Ipv6IfIndex - IP6 Interface Index.
 *               i4TestValIpv6IfRouterAdvStatus - Value of Router Advertisement
 *                                              status to be tested.
 *
 * OUTPUTS     : pu4ErrorCode - Pointer to the  Error Code
 *
 * RETURNS     : IP6_FAILURE
 *               
 * NOTES       : 
 ******************************************************************************/

INT1
Ip6Testv2Fsipv6IfRouterAdvStatus (UINT4 *pu4ErrorCode, INT4 i4Ipv6IfIndex,
                                  INT4 i4TestValIpv6IfRouterAdvStatus)
{
    UNUSED_PARAM (i4Ipv6IfIndex);
    UNUSED_PARAM (i4TestValIpv6IfRouterAdvStatus);
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    return (IP6_FAILURE);

}

/******************************************************************************
 * DESCRIPTION : This routine is called from low-level Test routine of the 
 *               Interface Router Advertisement Flags. Since This module
 *               Corresponds to HOST, this function always returns IP6_FAILURE.
 *
 * INPUTS      : i4Ipv6IfIndex - IP6 Interface Index.
 *               i4TestValIpv6IfRouterAdvFlags- Value of Router Advertisement
 *                                              Flag to be tested.
 *
 * OUTPUTS     : pu4ErrorCode - Pointer to the  Error Code
 *
 * RETURNS     : IP6_FAILURE
 *               
 * NOTES       : 
 ******************************************************************************/
INT1
Ip6Testv2Fsipv6IfRouterAdvFlags (UINT4 *pu4ErrorCode, INT4 i4Ipv6IfIndex,
                                 INT4 i4TestValIpv6IfRouterAdvFlags)
{
    UNUSED_PARAM (i4Ipv6IfIndex);
    UNUSED_PARAM (i4TestValIpv6IfRouterAdvFlags);
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    return (IP6_FAILURE);
}

/******************************************************************************
 * DESCRIPTION : This routine is called from low-level routine for getting the 
 *               Admin status of the address. For host operstatus depends both on
 *               result of DAD and preffered and valid life time of the address.
 *
 * INPUTS      : i4Ipv6IfIndex - If index over which the address is assigned.
 *               pIpv6AddrAddress - Pointer to the address( SNMP octet string)
 *
 * OUTPUTS     : pi4RetValIpv6AddrStatus - Pointer to the status of the 
 *                                             address.
 *
 * RETURNS     : IP6_SUCCESS/Ip6_FAILURE
 *               
 *
 * NOTES       : 
 ******************************************************************************/

INT1
Ip6GetIpv6AddrStatus (INT4 i4Ipv6IfIndex,
                      tSNMP_OCTET_STRING_TYPE * pIpv6AddrAddress,
                      INT4 *pi4RetValIpv6AddrStatus)
{
    tIp6AddrInfo       *pAddrInfo = NULL;
    tIp6LlocalInfo     *pLlocalInfo = NULL;
    tIp6If             *pIf6 = gIp6GblInfo.apIp6If[i4Ipv6IfIndex];
    tIp6Addr            Ipv6Address;
    Ip6AddrCopy (&Ipv6Address, (tIp6Addr *) pIpv6AddrAddress->pu1_OctetList);
    if (pIf6 == NULL)
        return IP6_FAILURE;
    if (IS_ADDR_LLOCAL (Ipv6Address))
    {
        pLlocalInfo
            = Ip6AddrTblGetLlocalEntry ((UINT4) i4Ipv6IfIndex,
                                        (tIp6Addr *) & Ipv6Address);
        if (pLlocalInfo != NULL)
        {

            if ((pLlocalInfo->u1Status & ADDR6_TENTATIVE) &&
                (pLlocalInfo->u1Status & ADDR6_UP))
            {
                /* Interface is UP, address is UP and DAD is tentative */
                *pi4RetValIpv6AddrStatus = IP6_ADDR_STAT_INACCESSIBLE;
                return IP6_SUCCESS;
            }
            else if ((pLlocalInfo->u1Status & (UINT1) ADDR6_COMPLETE) &&
                     (pLlocalInfo->u1Status & ADDR6_UP))
            {
                /* Interface is UP, address is UP and DAD is complete */
                if (pLlocalInfo->u1Status & ADDR6_PREFERRED)
                    *pi4RetValIpv6AddrStatus = IP6_ADDR_STAT_PREFERRED;
                else
                    *pi4RetValIpv6AddrStatus = IP6_ADDR_STAT_DEPRECATE;
                return IP6_SUCCESS;
            }
            else if ((pLlocalInfo->u1Status & ADDR6_FAILED) &&
                     (pLlocalInfo->u1Status & ADDR6_UP))
            {
                /* Interface is UP, address is UP and DAD has failed */
                *pi4RetValIpv6AddrStatus = IP6_ADDR_STAT_INACCESSIBLE;
                return IP6_SUCCESS;
            }
            else if ((pLlocalInfo->u1Status == ADDR6_DOWN) ||
                     (pLlocalInfo->u1Status & ADDR6_UP))
            {
                /* Interface DOWN or address is DOWN */
                *pi4RetValIpv6AddrStatus = IP6_ADDR_STAT_INACCESSIBLE;
                return IP6_SUCCESS;
            }

            return IP6_FAILURE;
        }
        else                    /* Ip6AddrTblGetLlocalEntry returned NULL */
        {
            *pi4RetValIpv6AddrStatus = IP6_ADDR_STAT_INVALID;
            return IP6_FAILURE;
        }
    }

    pAddrInfo =
        Ip6AddrTblGetEntry ((UINT4) i4Ipv6IfIndex,
                            (tIp6Addr *) & Ipv6Address, 0);

    if (pAddrInfo == NULL)
    {
        *pi4RetValIpv6AddrStatus = IP6_ADDR_STAT_INVALID;
        return IP6_FAILURE;
    }
    if ((pAddrInfo->u1Status & ADDR6_TENTATIVE) &&
        (pAddrInfo->u1Status & ADDR6_UP))
    {
        /* Interface is UP, address is UP and DAD is tentative */
        *pi4RetValIpv6AddrStatus = IP6_ADDR_STAT_INACCESSIBLE;
        return IP6_SUCCESS;
    }
    else if ((pAddrInfo->u1Status & (UINT1) ADDR6_COMPLETE) &&
             (pAddrInfo->u1Status & ADDR6_UP))
    {
        /* Interface is UP, address is UP and DAD is complete */
        if (pAddrInfo->u1Status & ADDR6_PREFERRED)
            *pi4RetValIpv6AddrStatus = IP6_ADDR_STAT_PREFERRED;
        else
            *pi4RetValIpv6AddrStatus = IP6_ADDR_STAT_DEPRECATE;

        return IP6_SUCCESS;
    }
    else if ((pAddrInfo->u1Status & ADDR6_FAILED) &&
             (pAddrInfo->u1Status & ADDR6_UP))
    {
        /* Interface is UP, address is UP and DAD has failed */
        *pi4RetValIpv6AddrStatus = IP6_ADDR_STAT_INACCESSIBLE;
        return IP6_SUCCESS;
    }
    else if ((pAddrInfo->u1Status == ADDR6_DOWN) ||
             (pAddrInfo->u1Status & ADDR6_UP))
    {
        /* Interface DOWN or address is DOWN */
        *pi4RetValIpv6AddrStatus = IP6_ADDR_STAT_INACCESSIBLE;
        return IP6_SUCCESS;
    }

    return IP6_FAILURE;

}

/******************************************************************************
 * DESCRIPTION : This routine is called from low-level routine for getting the 
 *               operstatus of the address. For host operstatus depends both on
 *               result of DAD and preffered and valid life time of the address.
 *
 * INPUTS      : i4Ipv6AddrIndex - If index over which the address is assigned.
 *               pIpv6AddrAddress - Pointer to the address( SNMP octet string)
 *               i4Ipv6AddrPrefixLen - Address prefix length.
 *
 * OUTPUTS     : pi4RetValIpv6AddrOperStatus - Pointer to the operstatus of the 
 *                                             address.
 *
 * RETURNS     : SNMP_SUCCESS/SNMP_FAILURE
 *               
 *
 * NOTES       : 
 ******************************************************************************/
INT1
Ip6GetFsipv6AddrOperStatus (INT4 i4Ipv6AddrIndex,
                            tSNMP_OCTET_STRING_TYPE * pIpv6AddrAddress,
                            INT4 i4Ipv6AddrPrefixLen,
                            INT4 *pi4RetValIpv6AddrOperStatus)
{
    tIp6AddrInfo       *pAddrInfo = NULL;
    tIp6LlocalInfo     *pLlocalInfo = NULL;
    tIp6Addr            Ipv6Address;
    Ip6AddrCopy (&Ipv6Address, (tIp6Addr *) pIpv6AddrAddress->pu1_OctetList);
    if (Ip6AddrType (&Ipv6Address) == ADDR6_LLOCAL)
    {
        if ((pLlocalInfo =
             Ip6AddrTblGetLlocalEntry ((UINT4) i4Ipv6AddrIndex,
                                       &Ipv6Address)) != NULL)
        {

            if ((pLlocalInfo->u1Status & ADDR6_TENTATIVE) &&
                (pLlocalInfo->u1Status & ADDR6_UP))
            {
                /* Interface is UP, address is UP and DAD is tentative */
                *pi4RetValIpv6AddrOperStatus = IP6_ADDR_OPER_TENTATIVE;
                return SNMP_SUCCESS;
            }
            else if ((pLlocalInfo->u1Status & (UINT1) ADDR6_COMPLETE) &&
                     (pLlocalInfo->u1Status & ADDR6_UP))
            {
                /* Interface is UP, address is UP and DAD is complete */
                *pi4RetValIpv6AddrOperStatus = IP6_ADDR_OPER_COMPLETE;
                return SNMP_SUCCESS;
            }
            else if ((pLlocalInfo->u1Status & ADDR6_FAILED) &&
                     (pLlocalInfo->u1Status & ADDR6_UP))
            {
                /* Interface is UP, address is UP and DAD has failed */
                *pi4RetValIpv6AddrOperStatus = IP6_ADDR_OPER_FAILED;
                return SNMP_SUCCESS;
            }
            else if ((pLlocalInfo->u1Status == ADDR6_DOWN) ||
                     (pLlocalInfo->u1Status & ADDR6_UP))
            {
                /* Interface DOWN or address is DOWN */
                *pi4RetValIpv6AddrOperStatus = IP6_ADDR_OPER_DOWN;
                return SNMP_SUCCESS;
            }

            return SNMP_FAILURE;
        }
        else                    /* Ip6AddrTblGetLlocalEntry returned NULL */
            return SNMP_FAILURE;

    }

    pAddrInfo =
        Ip6AddrTblGetEntry ((UINT4) i4Ipv6AddrIndex,
                            (tIp6Addr *) & Ipv6Address,
                            (UINT1) i4Ipv6AddrPrefixLen);

    if (pAddrInfo == NULL)
        return SNMP_FAILURE;

    if ((pAddrInfo->u1Status & ADDR6_TENTATIVE) &&
        (pAddrInfo->u1Status & ADDR6_UP))
    {
        /* Interface is UP, address is UP and DAD is tentative */
        *pi4RetValIpv6AddrOperStatus = IP6_ADDR_OPER_TENTATIVE;
        return SNMP_SUCCESS;
    }
    else if ((pAddrInfo->u1Status & (UINT1) ADDR6_COMPLETE) &&
             (pAddrInfo->u1Status & ADDR6_UP))
    {
        /* Interface is UP, address is UP and DAD is complete */
        *pi4RetValIpv6AddrOperStatus = IP6_ADDR_OPER_COMPLETE;
        return SNMP_SUCCESS;
    }
    else if ((pAddrInfo->u1Status & ADDR6_FAILED) &&
             (pAddrInfo->u1Status & ADDR6_UP))
    {
        /* Interface is UP, address is UP and DAD has failed */
        *pi4RetValIpv6AddrOperStatus = IP6_ADDR_OPER_FAILED;
        return SNMP_SUCCESS;
    }
    else if ((pAddrInfo->u1Status == ADDR6_DOWN) ||
             (pAddrInfo->u1Status & ADDR6_UP))
    {
        /* Interface DOWN or address is DOWN */
        *pi4RetValIpv6AddrOperStatus = IP6_ADDR_OPER_DOWN;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/******************************************************************************
 * DESCRIPTION : This routine is called from low-level routine for testing whether 
 *               the value set for enabling/disabling IP Forwarding is correct/not. 
 *               Since This module Corresponds to HOST, this function always returns 
 *               IP6_FAILURE.
 *
 * INPUTS      : i4TestValIpv6Forwarding - Value set for IP Forwarding Object 
 *
 * OUTPUTS     : pu4ErrorCode - pointer to the Error Code.
 *
 * RETURNS     : IP6_FAILURE
 *               
 *
 * NOTES       : 
 ******************************************************************************/
#
INT1
Ip6Testv2Ipv6Forwarding (UINT4 *pu4ErrorCode, INT4 i4TestValIpv6Forwarding)
{
    UNUSED_PARAM (i4TestValIpv6Forwarding);
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/******************************************************************************
 * DESCRIPTION : This routine is called from low-level Test routine of the 
 *               Interface Router Advertisement Status. Since This module
 *               Corresponds to HOST, this function always returns IP6_FAILURE.
 *
 * INPUTS      : i4Ipv6IfIndex - IP6 Interface Index.
 *               i4TestValIpv6IfPrefixAdvStatus- Value of Router Advertisement
 *                                              Flag to be tested.
 *
 * OUTPUTS     : pu4ErrorCode - Pointer to the  Error Code
 *
 * RETURNS     : IP6_FAILURE
 *               
 * NOTES       : 
 ******************************************************************************/
INT1
Ip6Testv2Fsipv6IfPrefixAdvStatus (UINT4 *pu4ErrorCode, INT4 i4Ipv6IfIndex,
                                  INT4 i4TestValIpv6IfPrefixAdvStatus)
{
    UNUSED_PARAM (i4Ipv6IfIndex);
    UNUSED_PARAM (i4TestValIpv6IfPrefixAdvStatus);
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return (IP6_FAILURE);

}

/******************************************************************************
 * DESCRIPTION : This routine is called from low-level Test routine of the 
 *               Interface Min Router Advertisement Time. Since This module
 *               Corresponds to HOST, this function always returns IP6_FAILURE.
 *
 * INPUTS      : i4Ipv6IfIndex - IP6 If Index.
 *               i4TestValIpv6IfMinRouterAdvTime- Value of Router Advertisement
 *                                              Flag to be tested.
 *
 * OUTPUTS     : pu4ErrorCode - Pointer to the  Error Code
 *
 * RETURNS     : IP6_FAILURE
 *               
 * NOTES       : 
 ******************************************************************************/
INT1
Ip6Testv2Fsipv6IfMinRouterAdvTime (UINT4 *pu4ErrorCode,
                                   INT4 i4Ipv6IfIndex,
                                   INT4 i4TestValIpv6IfMinRouterAdvTime)
{
    UNUSED_PARAM (i4Ipv6IfIndex);
    UNUSED_PARAM (i4TestValIpv6IfMinRouterAdvTime);
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return (IP6_FAILURE);

}

/******************************************************************************
 * DESCRIPTION : This routine is called from low-level Test routine of the 
 *               Interface Max Router Advertisement Time. Since This module
 *               Corresponds to HOST, this function always returns IP6_FAILURE.
 *
 * INPUTS      : i4Ipv6IfIndex - IP6 If Index.
 *               i4TestValIpv6IfMaxRouterAdvTime- Value of Router Advertisement
 *                                              Flag to be tested.
 *
 * OUTPUTS     : pu4ErrorCode - Pointer to the  Error Code
 *
 * RETURNS     : IP6_FAILURE
 *               
 * NOTES       : 
 ******************************************************************************/
INT1
Ip6Testv2Fsipv6IfMaxRouterAdvTime (UINT4 *pu4ErrorCode,
                                   INT4 i4Ipv6IfIndex,
                                   INT4 i4TestValIpv6IfMaxRouterAdvTime)
{
    UNUSED_PARAM (i4Ipv6IfIndex);
    UNUSED_PARAM (i4TestValIpv6IfMaxRouterAdvTime);
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return (IP6_FAILURE);

}

/******************************************************************************
 * DESCRIPTION : This routine is called from low-level Test routine of the 
 *               Interface Default Router Life Time  Since This module
 *               Corresponds to HOST, this function always returns IP6_FAILURE.
 *
 * INPUTS      : i4Ipv6IfIndex - IP6 If Index.
 *               i4TestValIpv6IfMaxRouterAdvTime- Value of Router Advertisement
 *                                              Flag to be tested.
 *
 * OUTPUTS     : pu4ErrorCode - Pointer to the  Error Code
 *
 * RETURNS     : IP6_FAILURE
 *               
 * NOTES       : 
 ******************************************************************************/
INT1
Ip6Testv2Fsipv6IfDefRouterTime (UINT4 *pu4ErrorCode, INT4 i4Ipv6IfIndex,
                                INT4 i4TestValIpv6IfDefRouterTime)
{
    UNUSED_PARAM (i4Ipv6IfIndex);
    UNUSED_PARAM (i4TestValIpv6IfDefRouterTime);
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return (IP6_FAILURE);

}

/******************************************************************************
 * DESCRIPTION : This routine is called from low-level Get Routine for
 *               fetching the Profile Index of Prefix to be advertised in
 *               Router Advertisement message.
 *               Since this module is corresponding to the router, this
 *               functionr returns IP6_FAILURE
 *
 * INPUTS      : i4Fsipv6PrefixIndex - Interface Index
 *               pFsipv6PrefixAddress - Prefix to be advertised
 *               i4Fsipv6PrefixAddrLen - Prefix length of the advertised prefix
 *
 * OUTPUTS     : pi4RetValFsipv6PrefixProfileIndex - Profile index of the
 *                                                   prefix.
 *
 * RETURNS     : IP6_FAILURE / IP6_SUCCESS
 *               
 * NOTES       : 
 ******************************************************************************/
INT1
Ip6GetFsipv6PrefixProfileIndex (INT4 i4Fsipv6PrefixIndex,
                                tSNMP_OCTET_STRING_TYPE * pFsipv6PrefixAddress,
                                INT4 i4Fsipv6PrefixAddrLen,
                                INT4 *pi4RetValFsipv6PrefixProfileIndex)
{
    UNUSED_PARAM (i4Fsipv6PrefixIndex);
    UNUSED_PARAM (pFsipv6PrefixAddress);
    UNUSED_PARAM (i4Fsipv6PrefixAddrLen);
    UNUSED_PARAM (pi4RetValFsipv6PrefixProfileIndex);
    return IP6_FAILURE;
}

/******************************************************************************
 * DESCRIPTION : This routine is called from low-level Get Routine for
 *               fetching the Admin Status of Prefix to be advertised in
 *               Router Advertisement message.
 *               Since this module is corresponding to the router, this
 *               functionr returns IP6_FAILURE
 *
 * INPUTS      : i4Fsipv6PrefixIndex - Interface Index
 *               pFsipv6PrefixAddress - Prefix to be advertised
 *               i4Fsipv6PrefixAddrLen - Prefix length of the advertised prefix
 *
 * OUTPUTS     : pi4RetValFsipv6PrefixAdminStatus - Admin Status of the prefix.
 *
 * RETURNS     : IP6_FAILURE / IP6_SUCCESS
 *               
 * NOTES       : 
 ******************************************************************************/
INT1
Ip6GetFsipv6PrefixAdminStatus (INT4 i4Fsipv6PrefixIndex,
                               tSNMP_OCTET_STRING_TYPE * pFsipv6PrefixAddress,
                               INT4 i4Fsipv6PrefixAddrLen,
                               INT4 *pi4RetValFsipv6PrefixAdminStatus)
{
    UNUSED_PARAM (i4Fsipv6PrefixIndex);
    UNUSED_PARAM (pFsipv6PrefixAddress);
    UNUSED_PARAM (i4Fsipv6PrefixAddrLen);
    UNUSED_PARAM (pi4RetValFsipv6PrefixAdminStatus);
    return IP6_FAILURE;
}

/******************************************************************************
 * DESCRIPTION : This routine is called from low-level Set Routine for
 *               setting the Profile Index of Prefix to be advertised in
 *               Router Advertisement message.
 *               Since this module is corresponding to the router, this
 *               functionr returns IP6_FAILURE
 *
 * INPUTS      : i4Fsipv6PrefixIndex - Interface Index
 *               pFsipv6PrefixAddress - Prefix to be advertised
 *               i4Fsipv6PrefixAddrLen - Prefix length of the advertised prefix
 *
 * OUTPUTS     : i4SetValFsipv6PrefixProfileIndex - Profile Index of the prefix
 *
 * RETURNS     : IP6_FAILURE / IP6_SUCCESS
 *               
 * NOTES       : 
 ******************************************************************************/
INT1
Ip6SetFsipv6PrefixProfileIndex (INT4 i4Fsipv6PrefixIndex,
                                tSNMP_OCTET_STRING_TYPE * pFsipv6PrefixAddress,
                                INT4 i4Fsipv6PrefixAddrLen,
                                INT4 i4SetValFsipv6PrefixProfileIndex)
{
    UNUSED_PARAM (i4Fsipv6PrefixIndex);
    UNUSED_PARAM (pFsipv6PrefixAddress);
    UNUSED_PARAM (i4Fsipv6PrefixAddrLen);
    UNUSED_PARAM (i4SetValFsipv6PrefixProfileIndex);
    return IP6_FAILURE;
}

/******************************************************************************
 * DESCRIPTION : This routine is called from low-level Set Routine for
 *               setting the Admin Status of Prefix to be advertised in
 *               Router Advertisement message.
 *               Since this module is corresponding to the router, this
 *               functionr returns IP6_FAILURE
 *
 * INPUTS      : i4Fsipv6PrefixIndex - Interface Index
 *               pFsipv6PrefixAddress - Prefix to be advertised
 *               i4Fsipv6PrefixAddrLen - Prefix length of the advertised prefix
 *
 * OUTPUTS     : i4SetValFsipv6PrefixAdminStatus - Admin Status of the prefix
 *
 * RETURNS     : IP6_FAILURE / IP6_SUCCESS
 *               
 * NOTES       : 
 ******************************************************************************/
INT1
Ip6SetFsipv6PrefixAdminStatus (INT4 i4Fsipv6PrefixIndex,
                               tSNMP_OCTET_STRING_TYPE * pFsipv6PrefixAddress,
                               INT4 i4Fsipv6PrefixAddrLen,
                               INT4 i4SetValFsipv6PrefixAdminStatus)
{
    UNUSED_PARAM (i4Fsipv6PrefixIndex);
    UNUSED_PARAM (pFsipv6PrefixAddress);
    UNUSED_PARAM (i4Fsipv6PrefixAddrLen);
    UNUSED_PARAM (i4SetValFsipv6PrefixAdminStatus);
    return IP6_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/******************************************************************************
 * DESCRIPTION : This routine is called from low-level test Routine for
 *               testing the Profile Index of Prefix to be advertised in
 *               Router Advertisement message.
 *               Since this module is corresponding to the router, this
 *               functionr returns IP6_FAILURE
 *
 * INPUTS      : i4Fsipv6PrefixIndex - Interface Index
 *               pFsipv6PrefixAddress - Prefix to be advertised
 *               i4Fsipv6PrefixAddrLen - Prefix length of the advertised prefix
 *               i4TestValFsipv6PrefixProfileIndex -Profile Index of the prefix
 * OUTPUTS     : pu4ErrorCode - Error Code
 *
 * RETURNS     : IP6_FAILURE / IP6_SUCCESS
 *               
 * NOTES       : 
 ******************************************************************************/
INT1
Ip6Testv2Fsipv6PrefixProfileIndex (UINT4 *pu4ErrorCode,
                                   INT4 i4Fsipv6PrefixIndex,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsipv6PrefixAddress,
                                   INT4 i4Fsipv6PrefixAddrLen,
                                   INT4 i4TestValFsipv6PrefixProfileIndex)
{
    UNUSED_PARAM (*pu4ErrorCode);
    UNUSED_PARAM (i4Fsipv6PrefixIndex);
    UNUSED_PARAM (pFsipv6PrefixAddress);
    UNUSED_PARAM (i4Fsipv6PrefixAddrLen);
    UNUSED_PARAM (i4TestValFsipv6PrefixProfileIndex);
    return IP6_FAILURE;
}

/******************************************************************************
 * DESCRIPTION : This routine is called from low-level Test Routine for
 *               testing the Admin Status of Prefix to be advertised in
 *               Router Advertisement message.
 *               Since this module is corresponding to the router, this
 *               functionr returns IP6_FAILURE
 *
 * INPUTS      : i4Fsipv6PrefixIndex - Interface Index
 *               pFsipv6PrefixAddress - Prefix to be advertised
 *               i4Fsipv6PrefixAddrLen - Prefix length of the advertised prefix
 *               i4TestValFsipv6PrefixAdminStatus - Admin Status of the prefix
 *
 * OUTPUTS     : pu4ErrorCode - Error Code
 *
 * RETURNS     : IP6_FAILURE / IP6_SUCCESS
 *               
 * NOTES       : 
 ******************************************************************************/
INT1
Ip6Testv2Fsipv6PrefixAdminStatus (UINT4 *pu4ErrorCode,
                                  INT4 i4Fsipv6PrefixIndex,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pFsipv6PrefixAddress,
                                  INT4 i4Fsipv6PrefixAddrLen,
                                  INT4 i4TestValFsipv6PrefixAdminStatus)
{
    UNUSED_PARAM (*pu4ErrorCode);
    UNUSED_PARAM (i4Fsipv6PrefixIndex);
    UNUSED_PARAM (pFsipv6PrefixAddress);
    UNUSED_PARAM (i4Fsipv6PrefixAddrLen);
    UNUSED_PARAM (i4TestValFsipv6PrefixAdminStatus);
    return IP6_FAILURE;
}

/* This following function are Sepcific to  Ip6Router so this will be
 * stub for the host implementaion and return SNMP_FAILURE */
/****************************************************************************
 Function    :  Ip6ValidateIndexInstanceRtrAdvtTable
 Input       :  i4RtrAdvtIfIndex  -   RA Table Entry Index
 Output      :  None
 Returns     :  SNMP_FAILURE
****************************************************************************/
INT1
Ip6ValidateIndexInstanceRtrAdvtTable (INT4 i4RtrAdvtIfIndex)
{
    UNUSED_PARAM (i4RtrAdvtIfIndex);
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  Ip6GetFirstIndexRtrAdvtTable
 Input       :  pi4RtrAdvtIfIndex -  First RA Table Entry Index
 Output      :  None
 Returns     :  SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
Ip6GetFirstIndexRtrAdvtTable (INT4 *pi4RtrAdvtIfIndex)
{
    UNUSED_PARAM (pi4RtrAdvtIfIndex);
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  Ip6GetNextIndexRtrAdvtTable
 Input       :  i4RtrAdvtIfIndex     - Ip6IfIndex
                pi4NextRtrAdvtIfIndex -  Next RA Table Entry Index     
 Output      :  None
 Returns     :  SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
Ip6GetNextIndexRtrAdvtTable (INT4 i4RtrAdvtIfIndex, INT4 *pi4NextRtrAdvtIfIndex)
{
    UNUSED_PARAM (i4RtrAdvtIfIndex);
    UNUSED_PARAM (pi4NextRtrAdvtIfIndex);
    return (SNMP_FAILURE);
}

/* GET Routine for All Objects  */
/****************************************************************************
 Function    :  Ip6GetRtrAdvtSendAdverts
 Input       :  i4RtrAdvtIfIndex     - Ip6IfIndex
                pi4RtrAdvtSendAdverts - RA Table RA Send Status     
 Output      :  None
 Returns     :  SNMP_FAILURE
****************************************************************************/
INT1
Ip6GetRtrAdvtSendAdverts (INT4 i4RtrAdvtIfIndex, INT4 *pi4RtrAdvtSendAdverts)
{
    UNUSED_PARAM (i4RtrAdvtIfIndex);
    UNUSED_PARAM (pi4RtrAdvtSendAdverts);
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  Ip6GetRtrAdvtMaxInterval
 Input       :  i4RtrAdvtIfIndex     - Ip6IfIndex
                pu4RtrAdvtMaxInterval - RA Table Max RA Tx Interval 
 Output      :  None
 Returns     :  SNMP_FAILURE
****************************************************************************/
INT1
Ip6GetRtrAdvtMaxInterval (INT4 i4RtrAdvtIfIndex, UINT4 *pu4RtrAdvtMaxInterval)
{
    UNUSED_PARAM (i4RtrAdvtIfIndex);
    UNUSED_PARAM (pu4RtrAdvtMaxInterval);
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  Ip6GetRtrAdvtMinInterval
 Input       :  i4RtrAdvtIfIndex     - Ip6IfIndex
                pu4RtrAdvtMinInterval - RA Table Min RA Tx Interval 
 Output      :  None
 Returns     :  SNMP_FAILURE
****************************************************************************/
INT1
Ip6GetRtrAdvtMinInterval (INT4 i4RtrAdvtIfIndex, UINT4 *pu4RtrAdvtMinInterval)
{
    UNUSED_PARAM (i4RtrAdvtIfIndex);
    UNUSED_PARAM (pu4RtrAdvtMinInterval);
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  Ip6GetRtrAdvtMFlag
 Input       :  i4RtrAdvtIfIndex     - Ip6IfIndex
                pi4RtrAdvtMFlag - RA Table M Flag Value 
 Output      :  None
 Returns     :  SNMP_FAILURE
****************************************************************************/
INT1
Ip6GetRtrAdvtMFlag (INT4 i4RtrAdvtIfIndex, INT4 *pi4RtrAdvtMFlag)
{
    UNUSED_PARAM (i4RtrAdvtIfIndex);
    UNUSED_PARAM (pi4RtrAdvtMFlag);
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  Ip6GetRtrAdvtOFlag
 Input       :  i4RtrAdvtIfIndex     - Ip6IfIndex
                pi4RtrAdvtOFlag  - RA Table O Flag Value  
 Output      :  None
 Returns     :  SNMP_FAILURE
****************************************************************************/
INT1
Ip6GetRtrAdvtOFlag (INT4 i4RtrAdvtIfIndex, INT4 *pi4RtrAdvtOFlag)
{
    UNUSED_PARAM (i4RtrAdvtIfIndex);
    UNUSED_PARAM (pi4RtrAdvtOFlag);
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  Ip6GetRtrAdvtLinkMTU
 Input       :  i4RtrAdvtIfIndex     - Ip6IfIndex
                pu4RtrAdvtLinkMTU     - RA Table Link MTU 
 Output      :  None
 Returns     :  SNMP_FAILURE
****************************************************************************/
INT1
Ip6GetRtrAdvtLinkMTU (INT4 i4RtrAdvtIfIndex, UINT4 *pu4RtrAdvtLinkMTU)
{
    UNUSED_PARAM (i4RtrAdvtIfIndex);
    UNUSED_PARAM (pu4RtrAdvtLinkMTU);
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  Ip6GetRtrAdvtReachTime
 Input       :  i4RtrAdvtIfIndex    - Ip6IfIndex
                pu4RtrAdvtReachTime - RA Table Reachable Time 
 Output      :  None
 Returns     :  SNMP_FAILURE
****************************************************************************/
INT1
Ip6GetRtrAdvtReachTime (INT4 i4RtrAdvtIfIndex, UINT4 *pu4RtrAdvtReachTime)
{
    UNUSED_PARAM (i4RtrAdvtIfIndex);
    UNUSED_PARAM (pu4RtrAdvtReachTime);
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  Ip6GetRtrAdvtTxTime
 Input       :  i4RtrAdvtIfIndex    - Ip6IfIndex
                u4RtrAdvtTxTime - RA Table RetTransmit Time 
 Output      :  None
 Returns     :  SNMP_FAILURE
****************************************************************************/
INT1
Ip6GetRtrAdvtTxTime (INT4 i4RtrAdvtIfIndex, UINT4 *pu4RtrAdvtTxTime)
{
    UNUSED_PARAM (i4RtrAdvtIfIndex);
    UNUSED_PARAM (pu4RtrAdvtTxTime);
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  Ip6GetRtrAdvtCurHopLimit
 Input       :  i4RtrAdvtIfIndex    - Ip6IfIndex
                pu4RtrAdvtCurHopLimit - RA Table HopLimit 
 Output      :  None
 Returns     :  SNMP_FAILURE
****************************************************************************/
INT1
Ip6GetRtrAdvtCurHopLimit (INT4 i4RtrAdvtIfIndex, UINT4 *pu4RtrAdvtCurHopLimit)
{
    UNUSED_PARAM (i4RtrAdvtIfIndex);
    UNUSED_PARAM (pu4RtrAdvtCurHopLimit);
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  Ip6GetRtrAdvtDefLifeTime
 Input       :  i4RtrAdvtIfIndex    - Ip6IfIndex
                pu4RtrAdvtDefLifeTime - RA Table Default Life Time 
 Output      :  None
 Returns     :  SNMP_FAILURE
****************************************************************************/
INT1
Ip6GetRtrAdvtDefLifeTime (INT4 i4RtrAdvtIfIndex, UINT4 *pu4RtrAdvtDefLifeTime)
{
    UNUSED_PARAM (i4RtrAdvtIfIndex);
    UNUSED_PARAM (pu4RtrAdvtDefLifeTime);
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  Ip6GetRtrAdvtRowStatus
 Input       :  i4RtrAdvtIfIndex    - Ip6IfIndex
                pi4RtrAdvtRowStatus  - RA Table RowStatus Value
 Output      :  None
 Returns     :  SNMP_FAILURE
****************************************************************************/
INT1
Ip6GetRtrAdvtRowStatus (INT4 i4RtrAdvtIfIndex, INT4 *pi4RtrAdvtRowStatus)
{
    UNUSED_PARAM (i4RtrAdvtIfIndex);
    UNUSED_PARAM (pi4RtrAdvtRowStatus);
    return (SNMP_FAILURE);
}

/* SET Routine for All Objects  */
/****************************************************************************
 Function    :  Ip6SetRtrAdvtSendAdverts
 Input       :  i4RtrAdvtIfIndex     - Ip6IfIndex
                i4RtrAdvtSendAdverts - RA Table RA Send Status     
 Output      :  None
 Returns     :  SNMP_FAILURE
****************************************************************************/
INT1
Ip6SetRtrAdvtSendAdverts (INT4 i4RtrAdvtIfIndex, INT4 i4RtrAdvtSendAdverts)
{
    UNUSED_PARAM (i4RtrAdvtIfIndex);
    UNUSED_PARAM (i4RtrAdvtSendAdverts);
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  Ip6SetRtrAdvtMaxInterval
 Input       :  i4RtrAdvtIfIndex     - Ip6IfIndex
                u4RtrAdvtMaxInterval - RA Table Max RA Tx Interval 
 Output      :  None
 Returns     :  SNMP_FAILURE
****************************************************************************/
INT1
Ip6SetRtrAdvtMaxInterval (INT4 i4RtrAdvtIfIndex, UINT4 u4RtrAdvtMaxInterval)
{

    UNUSED_PARAM (i4RtrAdvtIfIndex);
    UNUSED_PARAM (u4RtrAdvtMaxInterval);
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  Ip6SetRtrAdvtMinInterval
 Input       :  i4RtrAdvtIfIndex     - Ip6IfIndex
                u4RtrAdvtMinInterval - RA Table Min RA Tx Interval 
 Output      :  None
 Returns     :  SNMP_FAILURE
****************************************************************************/
INT1
Ip6SetRtrAdvtMinInterval (INT4 i4RtrAdvtIfIndex, UINT4 u4RtrAdvtMinInterval)
{
    UNUSED_PARAM (i4RtrAdvtIfIndex);
    UNUSED_PARAM (u4RtrAdvtMinInterval);
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  Ip6SetRtrAdvtMFlag
 Input       :  i4RtrAdvtIfIndex     - Ip6IfIndex
                i4RtrAdvtMFlag - RA Table M Flag Value 
 Output      :  None
 Returns     :  SNMP_FAILURE
****************************************************************************/
INT1
Ip6SetRtrAdvtMFlag (INT4 i4RtrAdvtIfIndex, INT4 i4RtrAdvtMFlag)
{
    UNUSED_PARAM (i4RtrAdvtIfIndex);
    UNUSED_PARAM (i4RtrAdvtMFlag);
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  Ip6SetRtrAdvtOFlag
 Input       :  i4RtrAdvtIfIndex     - Ip6IfIndex
                i4RtrAdvtOFlag  - RA Table O Flag Value  
 Output      :  None
 Returns     :  SNMP_FAILURE
****************************************************************************/
INT1
Ip6SetRtrAdvtOFlag (INT4 i4RtrAdvtIfIndex, INT4 i4RtrAdvtOFlag)
{
    UNUSED_PARAM (i4RtrAdvtIfIndex);
    UNUSED_PARAM (i4RtrAdvtOFlag);
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  Ip6SetRtrAdvtLinkMTU
 Input       :  i4RtrAdvtIfIndex     - Ip6IfIndex
                u4RtrAdvtLinkMTU     - RA Table Link MTU 
 Output      :  None
 Returns     :  SNMP_FAILURE
****************************************************************************/
INT1
Ip6SetRtrAdvtLinkMTU (INT4 i4RtrAdvtIfIndex, UINT4 u4RtrAdvtLinkMTU)
{
    UNUSED_PARAM (i4RtrAdvtIfIndex);
    UNUSED_PARAM (u4RtrAdvtLinkMTU);
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  Ip6SetRtrAdvtReachTime
 Input       :  i4RtrAdvtIfIndex    - Ip6IfIndex
                u4RtrAdvtReachTime - RA Table Reachable Time 
 Output      :  None
 Returns     :  SNMP_FAILURE
****************************************************************************/
INT1
Ip6SetRtrAdvtReachTime (INT4 i4RtrAdvtIfIndex, UINT4 u4RtrAdvtReachTime)
{
    UNUSED_PARAM (i4RtrAdvtIfIndex);
    UNUSED_PARAM (u4RtrAdvtReachTime);
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  Ip6SetRtrAdvtTxTime
 Input       :  i4RtrAdvtIfIndex    - Ip6IfIndex
                u4RtrAdvtTxTime - RA Table RetTransmit Time 
 Output      :  None
 Returns     :  SNMP_FAILURE
****************************************************************************/
INT1
Ip6SetRtrAdvtTxTime (INT4 i4RtrAdvtIfIndex, UINT4 u4RtrAdvtTxTime)
{
    UNUSED_PARAM (i4RtrAdvtIfIndex);
    UNUSED_PARAM (u4RtrAdvtTxTime);
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  Ip6SetRtrAdvtCurHopLimit
 Input       :  i4RtrAdvtIfIndex    - Ip6IfIndex
                u4RtrAdvtCurHopLimit - RA Table HopLimit 
 Output      :  None
 Returns     :  SNMP_FAILURE
****************************************************************************/
INT1
Ip6SetRtrAdvtCurHopLimit (INT4 i4RtrAdvtIfIndex, UINT4 u4RtrAdvtCurHopLimit)
{
    UNUSED_PARAM (i4RtrAdvtIfIndex);
    UNUSED_PARAM (u4RtrAdvtCurHopLimit);
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  Ip6SetRtrAdvtDefLifeTime
 Input       :  i4RtrAdvtIfIndex    - Ip6IfIndex
                u4RtrAdvtDefLifeTime - RA Table Default Life Time 
 Output      :  None
 Returns     :  SNMP_FAILURE
****************************************************************************/
INT1
Ip6SetRtrAdvtDefLifeTime (INT4 i4RtrAdvtIfIndex, UINT4 u4RtrAdvtDefLifeTime)
{
    UNUSED_PARAM (i4RtrAdvtIfIndex);
    UNUSED_PARAM (u4RtrAdvtDefLifeTime);
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  Ip6SetRtrAdvtRowStatus
 Input       :  i4RtrAdvtIfIndex    - Ip6IfIndex
                i4RtrAdvtRowStatus  - RA Table RowStatus Value
 Output      :  None
 Returns     :  SNMP_FAILURE
****************************************************************************/
INT1
Ip6SetRtrAdvtRowStatus (INT4 i4RtrAdvtIfIndex, INT4 i4RtrAdvtRowStatus)
{
    UNUSED_PARAM (i4RtrAdvtIfIndex);
    UNUSED_PARAM (i4RtrAdvtRowStatus);
    return (SNMP_FAILURE);
}

/* TEST Routines for All Objects  */
/****************************************************************************
 Function    :  Ip6TestRtrAdvtSendAdverts
 Input       :  pu4ErrorCode         - Error Code 
                i4RtrAdvtIfIndex     - Ip6IfIndex
                i4RtrAdvtSendAdverts - RA Table RA Send Status     
 Output      :  None
 Returns     :  SNMP_FAILURE
****************************************************************************/
INT1
Ip6TestRtrAdvtSendAdverts (UINT4 *pu4ErrorCode, INT4 i4RtrAdvtIfIndex,
                           INT4 i4RtrAdvtSendAdverts)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4RtrAdvtIfIndex);
    UNUSED_PARAM (i4RtrAdvtSendAdverts);
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  Ip6TestRtrAdvtMaxInterval
 Input       :  pu4ErrorCode         - Error Code 
                i4RtrAdvtIfIndex     - Ip6IfIndex
                u4RtrAdvtMaxInterval - RA Table Max RA Tx Interval 
 Output      :  None
 Returns     :  SNMP_FAILURE
****************************************************************************/
INT1
Ip6TestRtrAdvtMaxInterval (UINT4 *pu4ErrorCode, INT4 i4RtrAdvtIfIndex,
                           UINT4 u4RtrAdvtMaxInterval)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4RtrAdvtIfIndex);
    UNUSED_PARAM (u4RtrAdvtMaxInterval);
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  Ip6TestRtrAdvtMinInterval
 Input       :  pu4ErrorCode         - Error Code 
                i4RtrAdvtIfIndex     - Ip6IfIndex
                u4RtrAdvtMinInterval - RA Table Min RA Tx Interval 
 Output      :  None
 Returns     :  SNMP_FAILURE
****************************************************************************/
INT1
Ip6TestRtrAdvtMinInterval (UINT4 *pu4ErrorCode, INT4 i4RtrAdvtIfIndex,
                           UINT4 u4RtrAdvtMinInterval)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4RtrAdvtIfIndex);
    UNUSED_PARAM (u4RtrAdvtMinInterval);
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  Ip6TestRtrAdvtMFlag
 Input       :  pu4ErrorCode         - Error Code 
                i4RtrAdvtIfIndex     - Ip6IfIndex
                i4RtrAdvtMFlag - RA Table M Flag Value 
 Output      :  None
 Returns     :  SNMP_FAILURE
****************************************************************************/
INT1
Ip6TestRtrAdvtMFlag (UINT4 *pu4ErrorCode, INT4 i4RtrAdvtIfIndex,
                     INT4 i4RtrAdvtMFlag)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4RtrAdvtIfIndex);
    UNUSED_PARAM (i4RtrAdvtMFlag);
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  Ip6TestRtrAdvtOFlag
 Input       :  pu4ErrorCode         - Error Code 
                i4RtrAdvtIfIndex     - Ip6IfIndex
                i4RtrAdvtOFlag  - RA Table O Flag Value  
 Output      :  None
 Returns     :  SNMP_FAILURE
****************************************************************************/
INT1
Ip6TestRtrAdvtOFlag (UINT4 *pu4ErrorCode, INT4 i4RtrAdvtIfIndex,
                     INT4 i4RtrAdvtOFlag)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4RtrAdvtIfIndex);
    UNUSED_PARAM (i4RtrAdvtOFlag);
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  Ip6TestRtrAdvtLinkMTU
 Input       :  pu4ErrorCode         - Error Code 
                i4RtrAdvtIfIndex     - Ip6IfIndex
                u4RtrAdvtLinkMTU     - RA Table Link MTU 
 Output      :  None
 Returns     :  SNMP_FAILURE
****************************************************************************/
INT1
Ip6TestRtrAdvtLinkMTU (UINT4 *pu4ErrorCode, INT4 i4RtrAdvtIfIndex,
                       UINT4 u4RtrAdvtLinkMTU)
{

    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4RtrAdvtIfIndex);
    UNUSED_PARAM (u4RtrAdvtLinkMTU);
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  Ip6TestRtrAdvtReachTime
 Input       :  pu4ErrorCode        - Error Code 
                i4RtrAdvtIfIndex    - Ip6IfIndex
                u4RtrAdvtReachTime - RA Table Reachable Time 
 Output      :  None
 Returns     :  SNMP_FAILURE
****************************************************************************/
INT1
Ip6TestRtrAdvtReachTime (UINT4 *pu4ErrorCode, INT4 i4RtrAdvtIfIndex,
                         UINT4 u4RtrAdvtReachTime)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4RtrAdvtIfIndex);
    UNUSED_PARAM (u4RtrAdvtReachTime);
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  Ip6TestRtrAdvtTxTime
 Input       :  pu4ErrorCode        - Error Code 
                i4RtrAdvtIfIndex    - Ip6IfIndex
                u4RtrAdvtTxTime - RA Table RetTransmit Time 
 Output      :  None
 Returns     :  SNMP_FAILURE
****************************************************************************/
INT1
Ip6TestRtrAdvtTxTime (UINT4 *pu4ErrorCode, INT4 i4RtrAdvtIfIndex,
                      UINT4 u4RtrAdvtTxTime)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4RtrAdvtIfIndex);
    UNUSED_PARAM (u4RtrAdvtTxTime);
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  Ip6TestRtrAdvtCurHopLimit
 Input       :  pu4ErrorCode        - Error Code 
                i4RtrAdvtIfIndex    - Ip6IfIndex
                u4RtrAdvtCurHopLimit - RA Table HopLimit 
 Output      :  None
 Returns     :  SNMP_FAILURE
****************************************************************************/
INT1
Ip6TestRtrAdvtCurHopLimit (UINT4 *pu4ErrorCode, INT4 i4RtrAdvtIfIndex,
                           UINT4 u4RtrAdvtCurHopLimit)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4RtrAdvtIfIndex);
    UNUSED_PARAM (u4RtrAdvtCurHopLimit);
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    : Ip6TestRtrAdvtDefLifeTime 
 Input       :  pu4ErrorCode        - Error Code 
                i4RtrAdvtIfIndex    - Ip6IfIndex
                u4RtrAdvtDefLifeTime - RA Table Default Life Time 
 Output      :  None
 Returns     :  SNMP_FAILURE
****************************************************************************/
INT1
Ip6TestRtrAdvtDefLifeTime (UINT4 *pu4ErrorCode, INT4 i4RtrAdvtIfIndex,
                           UINT4 u4RtrAdvtDefLifeTime)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4RtrAdvtIfIndex);
    UNUSED_PARAM (u4RtrAdvtDefLifeTime);
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  Ip6TestRtrAdvtRowStatus
 Input       :  pu4ErrorCode        - Error Code 
                i4RtrAdvtIfIndex    - Ip6IfIndex
                i4RtrAdvtRowStatus  - RA Table RowStatus Value
 Output      :  None
 Returns     :  SNMP_FAILURE
****************************************************************************/
INT1
Ip6TestRtrAdvtRowStatus (UINT4 *pu4ErrorCode, INT4 i4RtrAdvtIfIndex,
                         INT4 i4RtrAdvtRowStatus)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4RtrAdvtIfIndex);
    UNUSED_PARAM (i4RtrAdvtRowStatus);
    return (SNMP_FAILURE);
}
