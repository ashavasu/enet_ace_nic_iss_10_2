/*
 *----------------------------------------------------------------------------- 
 *    FILE NAME                    :    ip6hfwd.c
 *
 *    PRINCIPAL AUTHOR             :    Aricent Inc.
 *
 *    SUBSYSTEM NAME               :    IPv6
 *
 *    MODULE NAME                  :    IP6 Forwarding Submodule for HOST.
 *
 *    LANGUAGE                     :    C
 *
 *    TARGET ENVIRONMENT           :    UNIX
 *
 *    DATE OF FIRST RELEASE        :    DDD-MM-2002
 *
 *    DESCRIPTION                  :    This file contains dummy routine for
 *                                      Ip6Forward() function call. When IP6
 *                                      host is enabled, this function would
 *                                      get called, and the packets gets dropped.
 *
 *----------------------------------------------------------------------------- 
 */

#include "ip6inc.h"

/******************************************************************************
 * DESCRIPTION : This routine is called to forward a received packet which is 
 *               not destined to this node. As the host does do forwarding 
 *               it just drops the packet.
 *
 * INPUTS      : Destination address (pDst)
 *
 * OUTPUTS     : NDCache pointer (pPNd6c) and Routing entry pointer (pPRt6)
 *
 * RETURNS     : The destination IPv6 Interface or NULL
 *
 * NOTES       :
 ******************************************************************************/
VOID
Ip6Forward (tIp6Hdr * pIp6, tIp6If * pIf6, UINT4 u4Len, UINT1 u1RtFlag,
            tCRU_BUF_CHAIN_HEADER * pBuf)
{
    UNUSED_PARAM (pIp6);
    UNUSED_PARAM (pIf6);
    UNUSED_PARAM (u4Len);
    UNUSED_PARAM (u1RtFlag);

    /* HOST - Doesn't fwd any datagrams , so release buffer */

    Ip6BufRelease (pIf6->pIp6Cxt->u4ContextId, pBuf, FALSE, IP6_FWD_SUBMODULE);

    return;
}

/******************************************************************************
 * DESCRIPTION : This routine returns the default forward status of the router
 *
 * INPUTS      : None     
 *
 * OUTPUTS     : None
 *
 * RETURNS     : Ipv6 default forward status
 ******************************************************************************/
INT4
Ip6GetDefForwardStatus ()
{
    return IP6_FORW_DISABLE;
}

/*************************** END OF FILE **************************************/
