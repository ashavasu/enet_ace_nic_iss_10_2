enum {
    MAX_IP6HOST_ND6_ROUTES_SIZING_ID,
    IP6HOST_MAX_SIZING_ID
};


#ifdef  _IP6HOSTSZ_C
tMemPoolId IP6HOSTMemPoolIds[ IP6HOST_MAX_SIZING_ID];
INT4  Ip6hostSizingMemCreateMemPools(VOID);
VOID  Ip6hostSizingMemDeleteMemPools(VOID);
INT4  Ip6hostSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#else  /*  _IP6HOSTSZ_C  */
extern tMemPoolId IP6HOSTMemPoolIds[ ];
extern INT4  Ip6hostSizingMemCreateMemPools(VOID);
extern VOID  Ip6hostSizingMemDeleteMemPools(VOID);
extern INT4  Ip6hostSzRegisterModuleSizingParams( CHR1 *pu1ModName); 
#endif /*  _IP6HOSTSZ_C  */


#ifdef  _IP6HOSTSZ_C
tFsModSizingParams FsIP6HOSTSizingParams [] = {
{ "tNd6RtEntry", "MAX_IP6HOST_ND6_ROUTES", sizeof(tNd6RtEntry),MAX_IP6HOST_ND6_ROUTES, MAX_IP6HOST_ND6_ROUTES,0 },
{"\0","\0",0,0,0,0}
};
#else  /*  _IP6HOSTSZ_C  */
extern tFsModSizingParams FsIP6HOSTSizingParams [];
#endif /*  _IP6HOSTSZ_C  */


