/*******************************************************************
 *     Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *  
 *     $Id: ip6fwd.c,v 1.29 2017/12/19 13:41:55 siva Exp $
 *    
 *****************************************************************/

/*
 *----------------------------------------------------------------------------- 
 *    FILE NAME                    :    ip6fwd.c
 *
 *    PRINCIPAL AUTHOR             :    Vivek Venkatraman
 *
 *    SUBSYSTEM NAME               :    IPv6
 *
 *    MODULE NAME                  :    IP6 Forwarding Submodule
 *
 *    LANGUAGE                     :    C
 *
 *    TARGET ENVIRONMENT           :    UNIX
 *
 *    DATE OF FIRST RELEASE        :    DDD-MM-1996
 *
 *    DESCRIPTION                  :    This file contains routines for
 *                                      forwarding IPv6 datagrams which are
 *                                      not destined to the router.
 *
 *----------------------------------------------------------------------------- 
 */

#include "ip6inc.h"
#include "secv6.h"

/******************************************************************************
 * DESCRIPTION : This routine is called to forward a IPv6 datagram which
 *               is not addressed to the router. It is also called to forward
 *               source routed IPv6 datagrams which have not reached their
 *               final destination. The routine checks on the addresses and
 *               if valid, calls the route find routine to determine the
 *               destination interface and the neighbor cache entry if any.
 *               If no route is found, a destination unreachable ICMP message
 *               is generated and the routine returns. If the outgoing 
 *               interface is same as the incoming interface, a redirect 
 *               message is generated if needed. If the packet size is bigger
 *               than the MTU of the outgoing inyterface, a packet too big
 *               ICMP message is generated and the routine returns. Otherwise,
 *               the datagram is sent out on the outgoing interface by
 *               calling the interface specific send routine.
 *
 * INPUTS      : The interface pointer (pIf6), the IPv6 header pointer (pIp6),
 *               the length of the datagram (u4Len), route flag used for
 *               strict/loose source routing (u1RtFlag) and the buffer (pBuf)
 *
 * OUTPUTS     : None
 *
 * RETURNS     : None
 *
 * NOTES       :
 ******************************************************************************/
VOID
Ip6Forward (tIp6Hdr * pIp6, tIp6If * pIf6, UINT4 u4Len, UINT1 u1RtFlag,
            tCRU_BUF_CHAIN_HEADER * pBuf)
{

    tIp6Cxt            *pIp6Cxt = NULL;
    UINT1               u1HopLim, u1Code = 0, u1RtType;
    UINT1               u1Nhdr;
#if defined (IPSECv6_WANTED) && !defined (VPN_WANTED)
    UINT4               u4Pmtu;
    tIp6Addr           *pErrPktDstAddr = NULL, ErrPktDstAddr;
    tIp6Addr           *pErrPktSrcAddr = NULL, ErrPktSrcAddr;
    tIp6Hdr             ip6Hdr;
#endif
    UINT4               u4DstMtu;
    tNd6CacheEntry     *pNd6c = NULL, *pNd6cSrc = NULL;
    tNd6CacheEntry     *pNd6cDst = NULL;
    tNetIpv6RtInfo      NetIp6RtInfo;
    tIp6If             *pDstIf6 = NULL;
    tIp6Addr           *pNextHop = NULL;
    UINT4               u4ProcessedLen;
    UINT4               u4TotLen;
    UINT1               u1NextHdr;
    INT4                i4Status = IP6_SUCCESS;
#ifdef MLD_WANTED
    INT1                i1McastPktSentFlag = 0;
    UINT4               u4IfIndex = 0;
    tIp6If             *pIf6Ptr = NULL;
    tCRU_BUF_CHAIN_HEADER *pCloneBuf = NULL;
#endif
    tCRU_BUF_CHAIN_HEADER *pRedirectBuf = NULL;
    UINT4               u4TmpIndex = 0;
    UINT4               u4DestAddrType = 0;
    UINT1               u1SrcAddrScope = 0;
    UINT1               u1DstAddrScope = 0;
    UINT4               u4Offset = 0;
    tIp6Addr           *pSrcAddr = NULL, SrcAddr;
#ifdef NPAPI_WANTED
    tIp6Addr            Ip6Addr;
#endif
    UINT1               u1Type = 0;
    struct Rtm6Cxt     *pRtm6Cxt = NULL;
    tIp6RtEntry        *pBestRt = NULL;
    pIp6Cxt = pIf6->pIp6Cxt;

    IP6IF_INC_IN_FORW_DGRAMS (pIf6);
    IP6SYS_INC_IN_FORW_DGRAMS (pIp6Cxt->Ip6SysStats);
    MEMSET (&NetIp6RtInfo, 0, sizeof (tNetIpv6RtInfo));
#ifdef NPAPI_WANTED
    MEMSET (&Ip6Addr, 0, sizeof (tIpAddr));
    MEMCPY (&Ip6Addr, &pIp6->dstAddr, sizeof (tIp6Addr));
#endif

    if (pIf6->u1Ipv6IfFwdOperStatus == IP6_IF_FORW_DISABLE)
    {
        UNUSED_PARAM (pIp6);
        UNUSED_PARAM (pIf6);
        UNUSED_PARAM (u4Len);
        UNUSED_PARAM (u1RtFlag);

        /* HOST - Doesn't fwd any datagrams , so release buffer */

        Ip6BufRelease (pIf6->pIp6Cxt->u4ContextId, pBuf, FALSE,
                       IP6_FWD_SUBMODULE);

#if defined (NPAPI_WANTED) && defined (CFA_WANTED)
        /* Remove the Entry created in the DLF Hash Table */
        Ipv6FsCfaHwRemoveIp6NetRcvdDlfInHash (pIp6Cxt->u4ContextId, Ip6Addr,
                                              IP6_MAX_PREFIX_LEN);
#endif
        return;

    }

    if (pIp6Cxt->u4ForwFlag == IP6_FORW_DISABLE)
    {
        IP6IF_INC_OUT_DISCARDS (pIf6);
        IP6SYS_INC_OUT_DISCARDS (pIp6Cxt->Ip6SysStats);
        Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE, IP6_FWD_SUBMODULE);
#if defined (NPAPI_WANTED) && defined (CFA_WANTED)
        /* Remove the Entry created in the DLF Hash Table */
        Ipv6FsCfaHwRemoveIp6NetRcvdDlfInHash (pIp6Cxt->u4ContextId, Ip6Addr,
                                              IP6_MAX_PREFIX_LEN);
#endif
        return;
    }

    /*
     * Hop-by-Hop options header must be examined and processed by
     * every node along a packet's delivery path, including source
     * and destination nodes (RFC 2460). If this occurs in packet
     * destinated to this node, it is taken care in Ip6Rcv(), if the
     * destination is not us, then this option is processed here.
     * Incase of Jumbogram, u4TotLen contains the total length of
     * the packet, otherwise the lenth of the packet can be obtained
     * from IPv6 header (pIp6).
     */

    if (pIp6->u1Nh == NH_H_BY_H)
    {
        u4ProcessedLen = IP6_HDR_LEN;
        u4TotLen = IP6_HDR_LEN + NTOHS (pIp6->u2Len);
        u1NextHdr = pIp6->u1Nh;

        i4Status = Ip6PrcsHbyhOptHdr (&u1NextHdr, &u4ProcessedLen, &u4TotLen,
                                      pIf6, pIp6, pBuf, OSIX_FALSE);
        if (i4Status == IP6_FAILURE)
        {
            /* Failure in Handling Hop By Hop Option. */
            IP6IF_INC_OUT_DISCARDS (pIf6);
            IP6SYS_INC_OUT_DISCARDS (pIp6Cxt->Ip6SysStats);
            Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf,
                           FALSE, IP6_FWD_SUBMODULE);
            return;
        }

        if (i4Status == IP6_ACCEPTED)
        {
            /* Packet forwarded to higher layer. No need to
             * proceed further.
             * CAUTION : Dont Release the Buffer. */
            return;
        }
    }

    u1DstAddrScope = Ip6GetAddrScope (&pIp6->dstAddr);
    u1SrcAddrScope = Ip6GetAddrScope (&pIp6->srcAddr);

    if ((u1SrcAddrScope < u1DstAddrScope) && (!IS_ADDR_MULTI (pIp6->dstAddr)))
    {
        IP6IF_INC_OUT_DISCARDS (pIf6);
        IP6SYS_INC_OUT_DISCARDS (pIp6Cxt->Ip6SysStats);
        Icmp6SendErrMsg (pIf6, pIp6, ICMP6_DEST_UNREACHABLE,
                         ICMP6_NOT_NEIGHBOUR, 0, pBuf);

#if defined (NPAPI_WANTED) && defined (CFA_WANTED)
        /* Remove the Entry created in the DLF Hash Table */
        Ipv6FsCfaHwRemoveIp6NetRcvdDlfInHash (pIp6Cxt->u4ContextId, Ip6Addr,
                                              IP6_MAX_PREFIX_LEN);
#endif
        return;
    }

    IP6_TRC_ARG4 (pIp6Cxt->u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                  "IP6FWD:Ip6Forward: Trying to forward Src IP6IF =%d Len =%d "
                  "RtFlag =%d Bufptr=%p", pIf6->u4Index, u4Len, u1RtFlag, pBuf);

    IP6_TRC_ARG2 (pIp6Cxt->u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                  "IP6FWD:Ip6Forward: Src Addr = %s Dst Addr = %s\n",
                  Ip6PrintAddr (&pIp6->srcAddr), Ip6PrintAddr (&pIp6->dstAddr));

    if (IS_ADDR_LINK_SCOPE_MULTI (pIp6->dstAddr))
    {
        /* Link-Local Scope MultiCast address. Need not forward it */
        IP6_TRC_ARG3 (pIp6Cxt->u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC,
                      IP6_NAME,
                      "IP6FWD:Ip6Forward: Pkt not fwd to Link Scope multicast "
                      "addr - IP6IF= %d Src Addr= %s Dst Addr=%s\n",
                      pIf6->u4Index, Ip6ErrPrintAddr (ERROR_MINOR,
                                                      &pIp6->srcAddr),
                      Ip6ErrPrintAddr (ERROR_MINOR, &pIp6->dstAddr));

        IP6IF_INC_OUT_DISCARDS (pIf6);
        IP6SYS_INC_OUT_DISCARDS (pIp6Cxt->Ip6SysStats);
        Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE, IP6_FWD_SUBMODULE);
#if defined (NPAPI_WANTED) && defined (CFA_WANTED)
        /* Remove the Entry created in the DLF Hash Table */
        Ipv6FsCfaHwRemoveIp6NetRcvdDlfInHash (pIp6Cxt->u4ContextId, Ip6Addr,
                                              IP6_MAX_PREFIX_LEN);
#endif
        return;
    }

    /* Hop limit check */

    u1HopLim = pIp6->u1Hlim;

    if (!u1HopLim || --u1HopLim == 0)
    {
        IP6_TRC_ARG2 (pIp6Cxt->u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC,
                      IP6_NAME,
                      "IP6FWD:Ip6Forward: Pkt dropped as Hop Limit is ZERO, "
                      "IP6IF = %d Src Addr = %s\n", pIf6->u4Index,
                      Ip6ErrPrintAddr (ERROR_MINOR, &pIp6->srcAddr));

        Icmp6SendErrMsg (pIf6, pIp6, ICMP6_TIME_EXCEEDED,
                         ICMP6_HOP_LIMIT_EXCEEDED, 0, pBuf);
#if defined (NPAPI_WANTED) && defined (CFA_WANTED)
        /* Remove the Entry created in the DLF Hash Table */
        Ipv6FsCfaHwRemoveIp6NetRcvdDlfInHash (pIp6Cxt->u4ContextId, Ip6Addr,
                                              IP6_MAX_PREFIX_LEN);
#endif
        return;
    }

    if (Ip6BufWrite (pBuf, &u1HopLim, IP6_OFFSET_FOR_HOPLIM_FIELD,
                     sizeof (UINT1), TRUE) != IP6_SUCCESS)
    {
        IP6IF_INC_OUT_DISCARDS (pIf6);
        IP6SYS_INC_OUT_DISCARDS (pIp6Cxt->Ip6SysStats);
        IP6_TRC_ARG (pIp6Cxt->u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                     "IP6FWD:Ip6Forward: Writing Hop Limit failed!\n");
        IP6_TRC_ARG3 (pIp6Cxt->u4ContextId, IP6_MOD_TRC, BUFFER_TRC, IP6_NAME,
                      "%s buf err: Type = %d Bufptr = %p\n", ERROR_FATAL_STR,
                      ERR_BUF_WRITE, pBuf);
        Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE, IP6_FWD_SUBMODULE);
#if defined (NPAPI_WANTED) && defined (CFA_WANTED)
        /* Remove the Entry created in the DLF Hash Table */
        Ipv6FsCfaHwRemoveIp6NetRcvdDlfInHash (pIp6Cxt->u4ContextId, Ip6Addr,
                                              IP6_MAX_PREFIX_LEN);
#endif
        return;
    }

#ifdef MLD_WANTED
    if (IS_ADDR_MULTI (pIp6->dstAddr))
    {
        u4IfIndex = 1;

        IP6_IF_SCAN (u4IfIndex, (UINT4) IP6_MAX_LOGICAL_IF_INDEX)
        {
            pIf6Ptr = gIp6GblInfo.apIp6If[u4IfIndex];
            if (pIf6Ptr == NULL)
                continue;

            /* Packet should not be forwarded on the interface on which
             * it has been received
             */
            if (pIf6Ptr == pIf6)
                continue;

            if ((Ip6SendIfCheck (pIf6Ptr) == IP6_SUCCESS) &&
                (MLDIsCacheEntryPresent (&pIp6->dstAddr, u4IfIndex) == TRUE))
            {
                pCloneBuf = CRU_BUF_Duplicate_BufChain (pBuf);
                if (pCloneBuf == NULL)
                {
                    IP6_TRC_ARG (pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                                 ALL_FAILURE_TRC, IP6_NAME,
                                 "IP6FWD: Ip6Fwd : Dupilcate Buffer Alloc "
                                 "Failed\n");
                    Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE,
                                   IP6_FWD_SUBMODULE);
#if defined (NPAPI_WANTED) && defined (CFA_WANTED)
                    /* Remove the Entry created in the DLF Hash Table */
                    Ipv6FsCfaHwRemoveIp6NetRcvdDlfInHash (pIp6Cxt->u4ContextId,
                                                          Ip6Addr,
                                                          IP6_MAX_PREFIX_LEN);
#endif
                    return;
                }
                Ip6ifSend (pIf6Ptr, &pIp6->dstAddr, pNd6c, u4Len, pCloneBuf,
                           ADDR6_MULTI);
                i1McastPktSentFlag = 1;
                pCloneBuf = NULL;
                IP6SYS_INC_OUT_FWD_DGRAMS (pIp6Cxt->Ip6SysStats);
                IP6IF_INC_OUT_FWD_DGRAMS (pIf6Ptr);
            }
        }

#if defined (NPAPI_WANTED) && defined (CFA_WANTED)
        /* Remove the Entry created in the DLF Hash Table */
        Ipv6FsCfaHwRemoveIp6NetRcvdDlfInHash (pIp6Cxt->u4ContextId, Ip6Addr,
                                              IP6_MAX_PREFIX_LEN);
#endif
        if (i1McastPktSentFlag == 0)
        {
            Icmp6SendErrMsg (pIf6, pIp6, ICMP6_DEST_UNREACHABLE, u1Code, 0,
                             pBuf);
        }
        else
        {
            Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf,
                           FALSE, IP6_FWD_SUBMODULE);
        }
        return;
    }
#endif

    /*
     * Find a route to this destination and get the destination interface
     * and neighbor cache pointer (if exists)
     * Locking of tasks is done here as we access the routing table which
     * is in RIP6 task and could get updated simultaneously.
     */
    u4DestAddrType = Ip6AddrType (&pIp6->dstAddr);
    if ((u4DestAddrType == ADDR6_UNICAST) ||
        (u4DestAddrType == ADDR6_V4_COMPAT))
    {
        pDstIf6 = Ip6GetRouteInCxt (pIp6Cxt->u4ContextId, &pIp6->dstAddr,
                                    &pNd6c, &NetIp6RtInfo);

        if (pDstIf6 == NULL)
        {
            u1Code = ICMP6_NO_ROUTE_TO_DEST;

            if (Ip6IsOurAddrInCxt (pIp6Cxt->u4ContextId, &(pIp6->srcAddr),
                                   &u4TmpIndex) == IP6_SUCCESS)
            {
                IP6IF_INC_OUT_NO_ROUTE (pIf6);
                IP6SYS_INC_OUT_NO_ROUTE (pIp6Cxt->Ip6SysStats);
            }
            else
            {
                IP6IF_INC_IN_NO_ROUTE (pIf6);
                IP6SYS_INC_IN_NO_ROUTE (pIp6Cxt->Ip6SysStats);
            }
            Icmp6SendErrMsg (pIf6, pIp6, ICMP6_DEST_UNREACHABLE, u1Code, 0,
                             pBuf);
#if defined (NPAPI_WANTED) && defined (CFA_WANTED)
            /* Remove the Entry created in the DLF Hash Table */
            Ipv6FsCfaHwRemoveIp6NetRcvdDlfInHash (pIp6Cxt->u4ContextId, Ip6Addr,
                                                  IP6_MAX_PREFIX_LEN);
#endif
            return;
        }
        else                    /* route is present for this network */
        {
            /* ND Throttling: if next hop is not resolved yet, add drop route */
            if ((NULL == pNd6c) && (NP_PRESENT == NetIp6RtInfo.u1HwStatus)
                && (IP6_ROUTE_TYPE_DISCARD != NetIp6RtInfo.i1Type)
                && (IP6_LOCAL_PROTOID != NetIp6RtInfo.i1Proto))
            {
                /* In Rtm6AddRtToPRTInCxt, Ip6GetFwdRouteEntryInCxt eventually
                 * acquires IP6 Lock at Nd6IsCacheReachable. Since IP6 task
                 * already has this lock, a hang is observed when trying to
                 * re-acquire this. Hence IP6 Lock is relinquished here */

                IP6_TASK_UNLOCK ();

                /* Add route to pend list with discard flag */
                /* oper status for the Interface will be down around ~5 second due to event 
                 * processing when we shutdown/ No shutdown and we will add drop route only 
                 * when Oper status is UP */
                if (pBuf != NULL)
                {
                    u4Offset = IP6_BUF_READ_OFFSET (pBuf);
                    pSrcAddr = Ip6BufRead (pBuf, (UINT1 *) &SrcAddr,
                                           IP6_OFFSET_FOR_SRCADDR_FIELD,
                                           sizeof (tIp6Addr), 1);
                    IP6_BUF_READ_OFFSET (pBuf) = u4Offset;

                    u4Offset = IP6_BUF_READ_OFFSET (pBuf);

                    if (Ip6IsPktToMeInCxt
                        (pDstIf6->pIp6Cxt, &u1Type, ADDR6_LLOCAL, pSrcAddr,
                         pDstIf6) == IP6_FAILURE)
                    {
                        u4Offset = IP6_BUF_READ_OFFSET (pBuf);
                        pSrcAddr = Ip6GetLlocalAddr (pDstIf6->u4Index);

                        pRtm6Cxt = UtilRtm6GetCxt (pIp6Cxt->u4ContextId);
                        if (pSrcAddr != NULL)
                        {
                            if (pRtm6Cxt == NULL)
                            {
                                return;
                            }

                            Ip6GetBestRouteEntryInCxt (pRtm6Cxt, &pIp6->dstAddr,
                                                       NetIp6RtInfo.u1Prefixlen,
                                                       &pBestRt);
                            Rtm6AddRtToPRTInCxt (pRtm6Cxt, pBestRt);
                            Rtm6ApiAddDropRouteInNP (pIp6Cxt->u4ContextId,
                                                     &NetIp6RtInfo);
                        }
                    }
                }

                IP6_TASK_LOCK ();

                IP6_TRC_ARG2 (pIp6Cxt->u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC,
                              IP6_NAME,
                              "IP6FWD:Ip6Forward: Adding Drop route for route "
                              "DstAddr=%s NxtHop=%s\n",
                              Ip6PrintAddr (&(NetIp6RtInfo.Ip6Dst)),
                              Ip6PrintAddr (&(NetIp6RtInfo.NextHop)));

            }
        }

        u1RtType = NetIp6RtInfo.i1Type;
        pNextHop = &NetIp6RtInfo.NextHop;

        IP6_TRC_ARG4 (pIp6Cxt->u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC,
                      IP6_NAME,
                      "IP6FWD:Ip6Forward:After finding route,DstIP6 IF=%d NDC=%p "
                      "DstAddr=%s NxtHop=%s\n", pDstIf6->u4Index, pNd6c,
                      Ip6PrintAddr (&pIp6->dstAddr), Ip6PrintAddr (pNextHop));

    }
    /* This check is added for RFC4007 - Sec-8 
       If a router receives a packet with a link-local destination address that
       is not one of the router's own link-local addresses on the arrival
       link, the router is expected to try to forward the packet to the
       destination on that link (subject to successful determination of the
       destination's link-layer address via the Neighbor Discovery protocol */

    else
    {
        pNd6cDst = Nd6IsCacheForAddr (pIf6, &pIp6->dstAddr);

        if ((pNd6cDst == NULL) || (pNd6cDst->pIf6 != pIf6))
        {
            IP6_TRC_ARG3 (pIp6Cxt->u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC,
                          IP6_NAME, "IP6FWD:Ip6Forward:As the src or"
                          "dst address is llocal scope both the incoming"
                          "and outgoing interface are expected to be the"
                          "same,this is violated IP6IF= %d "
                          "Src Addr= %s Dst Addr=%s\n", pIf6->u4Index,
                          Ip6ErrPrintAddr (ERROR_MINOR, &pIp6->srcAddr),
                          Ip6ErrPrintAddr (ERROR_MINOR, &pIp6->dstAddr));

            IP6IF_INC_OUT_DISCARDS (pIf6);
            IP6SYS_INC_OUT_DISCARDS (pIp6Cxt->Ip6SysStats);
            Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE,
                           IP6_FWD_SUBMODULE);
#if defined (NPAPI_WANTED) && defined (CFA_WANTED)
            /* Remove the Entry created in the DLF Hash Table */
            Ipv6FsCfaHwRemoveIp6NetRcvdDlfInHash (pIp6Cxt->u4ContextId, Ip6Addr,
                                                  IP6_MAX_PREFIX_LEN);
#endif
            return;
        }
        pDstIf6 = pIf6;
        u1RtType = DIRECT;
    }

    /* Added for RFC4007 Code forwarding code changes- When a packet
       is to be forwarded it checks the scope of the next address, if the
       scope of the next address is smaller than the scope of the
       original destination address, the node MUST discard the packet.
       Also if the source and the next hop zone index is not the same
       the packet has to be discarded */
    if (u1RtType != DIRECT)
    {
        if (IP6_FAILURE ==
            Ip6fwdCheckZoneForOutGoingIf (pIp6, pNextHop, pDstIf6, pIf6))
        {
            IP6_TRC_ARG3 (pIp6Cxt->u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC,
                          IP6_NAME, "IP6FWD:Ip6Forward:As the next hop"
                          "address is smaller than the destination scope"
                          "zone rule is violated hence returning failure"
                          "If Index = %d dest Addr= %s Next Hop=%s\n",
                          pIf6->u4Index, Ip6ErrPrintAddr (ERROR_MINOR,
                                                          &pIp6->dstAddr),
                          Ip6ErrPrintAddr (ERROR_MINOR, pNextHop));

            IP6IF_INC_OUT_DISCARDS (pIf6);
            IP6SYS_INC_OUT_DISCARDS (pIp6Cxt->Ip6SysStats);
            Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE,
                           IP6_FWD_SUBMODULE);
#if defined (NPAPI_WANTED) && defined (CFA_WANTED)
            /* Remove the Entry created in the DLF Hash Table */
            Ipv6FsCfaHwRemoveIp6NetRcvdDlfInHash (pIp6Cxt->u4ContextId, Ip6Addr,
                                                  IP6_MAX_PREFIX_LEN);
#endif
            return;
        }
    }
    if (pDstIf6 == pIf6 &&
        u1RtFlag == IP6_FWD_NO_SRCRT &&
        ((pIf6->u1IfType == IP6_ENET_INTERFACE_TYPE) ||
         (pIf6->u1IfType == IP6_PSEUDO_WIRE_INTERFACE_TYPE) ||
         (pIf6->u1IfType == IP6_L3VLAN_INTERFACE_TYPE) ||
         (pIf6->u1IfType == IP6_LAGG_INTERFACE_TYPE) ||
         (pIf6->u1IfType == IP6_L3SUB_INTF_TYPE)))
    {
        /* 
         * outgoing and incoming interface are same and the interface is
         * a multicast interface, so send a redirect if the source is our
         * neighbor
         */
        pNd6cSrc = Nd6IsCacheForAddr (pIf6, &pIp6->srcAddr);
        pRedirectBuf = CRU_BUF_Duplicate_BufChain (pBuf);
    }

    u4DstMtu = Ip6ifGetMtu (pDstIf6);

    IP6_BUF_READ_OFFSET (pBuf) = 0;
    Ip6GetHlProtocolInCxt (pIp6Cxt->u4ContextId, pBuf, &u1Nhdr, NULL);
#ifdef IPSECv6_WANTED
    u4DstMtu -= Secv6GetSecAssocOverHead (pIp6->srcAddr, pIp6->dstAddr,
                                          (UINT2) pDstIf6->u4Index, u1Nhdr);
#endif
#ifdef TUNNEL_WANTED
    if (pDstIf6->u1IfType == IP6_TUNNEL_INTERFACE_TYPE)
    {
        if (u4DstMtu <= IP6_MIN_MTU)
        {
            if (u4Len > IP6_MIN_MTU)
            {
                IP6_TRC_ARG3 (pIp6Cxt->u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC,
                              IP6_NAME,
                              "IP6FWD: Ip6Forward:Sending PktTooBig ICMP err, "
                              "Pkt len = %d Dst IP6 IF = %d Out MTU = %d\n",
                              u4Len, pDstIf6->u4Index, IP6_MIN_MTU);

                pDstIf6->stats.u4TooBigerrs++;

                /* Pkt length is more than Pmtu, but Pmtu is less than MIN MTU, 
                 * So Sending error message with 1280 bytes so that oroginators 
                 * Pmtu will be updated with 1280 bytes.
                 */
                Icmp6SendErrMsg (pIf6, pIp6, ICMP6_PKT_TOO_BIG, 0, IP6_MIN_MTU,
                                 pBuf);
                Ip6BufRelease (pIp6Cxt->u4ContextId, pRedirectBuf, FALSE,
                               IP6_IF_SUBMODULE);
#if defined (NPAPI_WANTED) && defined (CFA_WANTED)
                /* Remove the Entry created in the DLF Hash Table */
                Ipv6FsCfaHwRemoveIp6NetRcvdDlfInHash (pIp6Cxt->u4ContextId,
                                                      Ip6Addr,
                                                      IP6_MAX_PREFIX_LEN);
#endif
                return;
            }
        }
        else if (u4Len > u4DstMtu)    /* 20 bytes already subtracted from u4DstMtu */
        {
            IP6_TRC_ARG3 (pIp6Cxt->u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC,
                          IP6_NAME,
                          "IP6FWD: Ip6Forward:Sending PktTooBig ICMP err, Pkt "
                          "len = %d Dst IP6 IF = %d Out MTU = %d \n ", u4Len,
                          pDstIf6->u4Index, u4DstMtu);

            pDstIf6->stats.u4TooBigerrs++;

            /* Pkt length is more than Pmtu,  So Sending error message with 
             * Pmtu so that oroginators Pmtu will be updated with Pmtu.
             */
            Icmp6SendErrMsg (pIf6, pIp6, ICMP6_PKT_TOO_BIG, 0, u4DstMtu, pBuf);
            Ip6BufRelease (pIp6Cxt->u4ContextId, pRedirectBuf, FALSE,
                           IP6_IF_SUBMODULE);
#if defined (NPAPI_WANTED) && defined (CFA_WANTED)
            /* Remove the Entry created in the DLF Hash Table */
            Ipv6FsCfaHwRemoveIp6NetRcvdDlfInHash (pIp6Cxt->u4ContextId, Ip6Addr,
                                                  IP6_MAX_PREFIX_LEN);
#endif
            return;
        }
    }
    else
    {
#endif
        if (u4Len > u4DstMtu)
        {

            IP6_TRC_ARG3 (pIp6Cxt->u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC,
                          IP6_NAME,
                          "IP6FWD:Ip6Forward: Sending PktTooBig ICMPerr, "
                          "Pktlen= %d DstIP6 IF=%d OutMTU=%d\n", u4Len,
                          pDstIf6->u4Index, u4DstMtu);

            IP6SYS_INC_OUT_FRAG_FAILS (pIp6Cxt->Ip6SysStats);
            IP6IF_INC_OUT_FRAG_FAILS (pDstIf6);

            pDstIf6->stats.u4TooBigerrs++;
            Icmp6SendErrMsg (pIf6, pIp6, ICMP6_PKT_TOO_BIG, 0, u4DstMtu, pBuf);
            Ip6BufRelease (pIp6Cxt->u4ContextId, pRedirectBuf, FALSE,
                           IP6_IF_SUBMODULE);
#if defined (NPAPI_WANTED) && defined (CFA_WANTED)
            /* Remove the Entry created in the DLF Hash Table */
            Ipv6FsCfaHwRemoveIp6NetRcvdDlfInHash (pIp6Cxt->u4ContextId, Ip6Addr,
                                                  IP6_MAX_PREFIX_LEN);
#endif
            return;
        }
#ifdef TUNNEL_WANTED
    }
#endif

#if defined (IPSECv6_WANTED) && !defined (VPN_WANTED)
    if (pIp6->u1Nh == NH_ICMP6)
    {
        /* Forwarding an Icmp packet , Updating the Pmtu if the Packet
         * is an error packet of type PKT_TOO_BIG.
         */
        if (Ip6GetProtoFromIcmp6ErrPktInCxt (pIp6Cxt, pBuf, &u1Nhdr, &u4Pmtu)
            == IP6_SUCCESS)
        {
            /* Calculating the effetive PMTU if a Security Association entry
             * is present.
             */
            pErrPktDstAddr =
                (tIp6Addr *) Ip6BufRead (pBuf,
                                         (UINT1 *) &ErrPktDstAddr,
                                         IP6_OFFSET_FOR_ERR_PKT_DESTADDR_FIELD
                                         /* 72 */ ,
                                         sizeof (tIp6Addr), TRUE);
            pErrPktSrcAddr =
                (tIp6Addr *) Ip6BufRead (pBuf,
                                         (UINT1 *) &ErrPktSrcAddr,
                                         IP6_OFFSET_FOR_ERR_PKT_SRCADDR_FIELD
                                         /* 56 */ ,
                                         sizeof (tIp6Addr), TRUE);
            if ((pErrPktDstAddr == NULL) || (pErrPktSrcAddr == NULL))
            {
                /*add trace */
                IP6IF_INC_OUT_DISCARDS (pIf6);
                IP6SYS_INC_OUT_DISCARDS (pIp6Cxt->Ip6SysStats);
                Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf,
                               FALSE, IP6_FWD_SUBMODULE);
                Ip6BufRelease (pIp6Cxt->u4ContextId, pRedirectBuf, FALSE,
                               IP6_IF_SUBMODULE);
#if defined (NPAPI_WANTED) && defined (CFA_WANTED)
                /* Remove the Entry created in the DLF Hash Table */
                Ipv6FsCfaHwRemoveIp6NetRcvdDlfInHash (pIp6Cxt->u4ContextId,
                                                      Ip6Addr,
                                                      IP6_MAX_PREFIX_LEN);
#endif
                return;
            }
            u4Pmtu -= Secv6GetSecAssocOverHead (*pErrPktSrcAddr,
                                                *pErrPktDstAddr,
                                                (UINT2) pIf6->u4Index, u1Nhdr);
            u4Pmtu = OSIX_HTONL ((u4Pmtu));
            if (Ip6BufWrite
                (pBuf, (UINT1 *) &u4Pmtu, (IP6_HDR_LEN + sizeof (tIcmp6PktHdr)),
                 sizeof (UINT4), TRUE) != IP6_SUCCESS)
            {
                IP6IF_INC_OUT_DISCARDS (pIf6);
                IP6SYS_INC_OUT_DISCARDS (pIp6Cxt->Ip6SysStats);
                IP6_TRC_ARG (pIp6Cxt->u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC,
                             IP6_NAME,
                             "IP6FWD:IP6FWD: Writing Hop Limit failed!\n");
                IP6_TRC_ARG3 (pIp6Cxt->u4ContextId, IP6_MOD_TRC, BUFFER_TRC,
                              IP6_NAME, "%s buf err: Type = %d Bufptr = %p\n",
                              ERROR_FATAL_STR, ERR_BUF_WRITE, pBuf);
                Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE,
                               IP6_FWD_SUBMODULE);
                Ip6BufRelease (pIp6Cxt->u4ContextId, pRedirectBuf, FALSE,
                               IP6_IF_SUBMODULE);
#if defined (NPAPI_WANTED) && defined (CFA_WANTED)
                /* Remove the Entry created in the DLF Hash Table */
                Ipv6FsCfaHwRemoveIp6NetRcvdDlfInHash (pIp6Cxt->u4ContextId,
                                                      Ip6Addr,
                                                      IP6_MAX_PREFIX_LEN);
#endif
                return;
            }
        }
    }
    if (Secv6OutProcess (pBuf, pDstIf6, &u4Len) == SEC_FAILURE)
    {
        IP6_TRC_ARG (pIp6Cxt->u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                     "IP6FWD: Ip6forward: Secv6OutProcess Fails\n");
        Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE, IP6_FWD_SUBMODULE);
        Ip6BufRelease (pIp6Cxt->u4ContextId, pRedirectBuf, FALSE,
                       IP6_IF_SUBMODULE);
        return;
    }
    else
    {
        pIp6 = Ip6BufRead (pBuf, (UINT1 *) &ip6Hdr, 0, IPV6_HEADER_LEN, 0);
        IP6_BUF_READ_OFFSET (pBuf) -= IPV6_HEADER_LEN;
    }
#endif

    IP6_TRC_ARG3 (pIp6Cxt->u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                  "IP6FWD:IP6FWD:Fwding on IF= %d Len= %d BufPtr= %p",
                  pDstIf6->u4Index, u4Len, pBuf);
    IP6_TRC_ARG3 (pIp6Cxt->u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                  "IP6FWD:IP6FWD:Fwding on Src= %s Dst= %s Next Hop= %s\n",
                  Ip6PrintAddr (&pIp6->srcAddr),
                  Ip6PrintAddr (&pIp6->dstAddr), Ip6PrintAddr (pNextHop));

    /*
     * Incase of Tunneling, route should be added as DIRECT,
     * in which case, the ipv6 destination address should NOT be taken
     * from the pNextHop
     */

    Ip6ifSend (pDstIf6, (u1RtType == DIRECT) ?
               &pIp6->dstAddr : pNextHop, pNd6c, u4Len, pBuf,
               NetIp6RtInfo.u1AddrType);

    if (pRedirectBuf != NULL)
    {
        Nd6SendRedirect (pNd6cSrc, pIf6, pIp6,
                         (u1RtType == DIRECT) ? &pIp6->dstAddr : pNextHop,
                         pRedirectBuf);
        Ip6BufRelease (pIp6Cxt->u4ContextId,
                       pRedirectBuf, FALSE, IP6_IF_SUBMODULE);
    }
    IP6SYS_INC_OUT_FWD_DGRAMS (pIp6Cxt->Ip6SysStats);
    IP6IF_INC_OUT_FWD_DGRAMS (pDstIf6);
    return;
}

/******************************************************************************
 * DESCRIPTION : This function, when called returns 0x02 - meaning address state
 *                is complete.        
 *
 * INPUTS      : None.
 * 
 * OUTPUTS     : None
 *
 * RETURNS     : returns Address State as Complete
 *
 * NOTES       :
 ******************************************************************************/
UINT1
Ip6RetAddrStatComplete (tIp6If * pIf6)
{
    if (pIf6->u1Ipv6IfFwdOperStatus == IP6_IF_FORW_ENABLE)
    {
        return (0x0a);
    }
    else
    {
        return (0x02);
    }
}

/******************************************************************************
 * DESCRIPTION : This routine returns the default forward status of the router
 *
 * INPUTS      : None     
 *
 * OUTPUTS     : None
 *
 * RETURNS     : Ipv6 default forward status
 ******************************************************************************/
INT4
Ip6GetDefForwardStatus ()
{
    return IP6_FORW_ENABLE;
}

/******************************************************************************
 * DESCRIPTION : This routine performs validation regarding the scope-zone
 *               for outgoing interface
 *
 * INPUTS      : pointer to Next hop address
 *               pointer to the Ip6 header
 *
 * OUTPUTS     : None
 *
 * RETURNS     : IP6_SUCCESS - If validation is passed
 *               IP6_FAILURE - If Validation failed
 ******************************************************************************/
INT4
Ip6fwdCheckZoneForOutGoingIf (tIp6Hdr * pIp6, tIp6Addr * pNextHop,
                              tIp6If * pDstIf6, tIp6If * pIf6)
{
    INT4                i4NextHopZoneIndex = IP6_ZONE_INVALID;
    INT4                i4SrcZoneIndex = IP6_ZONE_INVALID;
    UINT1               u1NextHopScope = 0;    /* Added for RFC4007  */
    UINT1               u1SrcAddrScope = 0;    /* Added for RFC4007  */

    u1SrcAddrScope = Ip6GetAddrScope (&pIp6->srcAddr);
    u1NextHopScope = Ip6GetAddrScope (pNextHop);

    IP6_TASK_UNLOCK ();
    i4NextHopZoneIndex =
        NetIpv6GetIfScopeZoneIndex (u1NextHopScope, pDstIf6->u4Index);
    i4SrcZoneIndex = NetIpv6GetIfScopeZoneIndex (u1SrcAddrScope, pIf6->u4Index);

    if ((IP6_ZONE_INVALID == i4NextHopZoneIndex) ||
        (IP6_ZONE_INVALID == i4SrcZoneIndex))
    {
        IP6_TRC_ARG3 (pIf6->pIp6Cxt->u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC,
                      IP6_NAME, "IP6FWD:Ip6Forward: The zone-index"
                      "is invalid for the src or next hop"
                      "zone rule is violated hence returning failure"
                      "If Index = %d src addr= %s Next Hop=%s\n", pIf6->u4Index,
                      Ip6ErrPrintAddr (ERROR_MINOR, &pIp6->srcAddr),
                      Ip6ErrPrintAddr (ERROR_MINOR, pNextHop));
        IP6_TASK_LOCK ();
        return IP6_FAILURE;
    }
    IP6_TASK_LOCK ();
    return IP6_SUCCESS;
}

/***************************** END OF FILE **********************************/
