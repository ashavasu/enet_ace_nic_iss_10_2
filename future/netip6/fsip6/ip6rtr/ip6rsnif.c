
/*******************************************************************
 *   Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *   
 *   $Id: ip6rsnif.c,v 1.29 2017/12/19 13:41:55 siva Exp $
 * 
 * ****************************************************************/

/*
 *----------------------------------------------------------------------------- 
 *    FILE NAME                    :    ip6rsnif.c
 *
 *    PRINCIPAL AUTHOR             :    Jayabharathi R.
 *
 *    SUBSYSTEM NAME               :    IPv6
 *
 *    MODULE NAME                  :    IP6 Interface Submodule
 *
 *    LANGUAGE                     :    C
 *
 *    TARGET ENVIRONMENT           :    UNIX
 *
 *    DATE OF FIRST RELEASE        :    DDD-MM-1996
 *
 *    DESCRIPTION                  :    This file contains routines called from  *                                      low-level routines. This routines will 
 *                                      appropriate action when the mib-object 
 *                                      set for IPV6-ROUTER.
 *----------------------------------------------------------------------------- 
 */
#include "ip6inc.h"
#include "ip6cli.h"

/******************************************************************************
 * DESCRIPTION : This routine is called from low-level routine for setting 
 *               the IPv6 Forwarding Status.
 *
 * INPUTS      : IPv6 Forwarding Status (u4ForwStatus)
 *
 * OUTPUTS     : None 
 *
 * RETURNS     : IP6_SUCCESS or IP6_FAILURE
 *               
 *
 * NOTES       : 
 ******************************************************************************/
INT1
Ip6SetIpv6ForwardingInCxt (tIp6Cxt * pIp6Cxt, UINT4 u4ForwStatus)
{
    tIp6Addr            tmpAddr;
    tIp6If             *pIf6 = NULL;
    UINT4               u4Index;

    pIp6Cxt->u4ForwFlag = u4ForwStatus;
    if (pIp6Cxt->u4ForwFlag == IP6_FORW_DISABLE)
    {
        u4Index = 1;

        IP6_IF_SCAN (u4Index, (UINT4) IP6_MAX_LOGICAL_IF_INDEX)
        {
            pIf6 = gIp6GblInfo.apIp6If[u4Index];
            if ((pIf6 == NULL) || (pIf6->pIp6Cxt != pIp6Cxt) ||
                (pIf6->u1Ipv6IfFwdOperStatus == IP6_IF_FORW_DISABLE))
            {
                continue;
            }
            if (pIf6->u1AdminStatus == ADMIN_UP)
            {
                /* Ensure that the ALL ROUTER Multicast address is deleted. */
                SET_ALL_ROUTERS_MULTI (tmpAddr);
                Ip6AddrDeleteMcast (u4Index, &tmpAddr);
            }
            /* If RA is enabled, then ensure that Final RA message are sent
             * out. */
            if ((Ip6CheckRAStatus ((INT4) (pIf6->u4Index))) == TRUE)
            {
                Nd6ActOnRaCnf (pIf6, IP6_IF_NO_RA_ADV);
            }
            pIf6->u1Ipv6IfFwdOperStatus = IP6_IF_FORW_DISABLE;
        }
    }
    else
    {
        u4Index = 1;

        IP6_IF_SCAN (u4Index, (UINT4) IP6_MAX_LOGICAL_IF_INDEX)
        {
            pIf6 = gIp6GblInfo.apIp6If[u4Index];
            if ((pIf6 == NULL) || (pIf6->pIp6Cxt != pIp6Cxt))
            {
                continue;
            }
            pIf6->u1Ipv6IfFwdOperStatus = pIf6->u1Ipv6IfFwdStatusConfigured;

            if ((pIf6->u1AdminStatus == ADMIN_UP) &&
                (pIf6->u1Ipv6IfFwdOperStatus == IP6_IF_FORW_ENABLE))
            {
                /* Ensure that the ALL ROUTER Multicast address is added. */
                SET_ALL_ROUTERS_MULTI (tmpAddr);
                Ip6AddrCreateMcast (u4Index, &tmpAddr);

                /* If RA is enabled, then ensure that RA message are sent. */
                if ((Ip6CheckRAStatus ((INT4) (pIf6->u4Index))) == TRUE)
                {
                    Nd6ActOnRaCnf (pIf6, IP6_IF_RA_ADV);
                }
            }
        }
    }

    return SNMP_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : This routine is called from low-level routine for getting the 
 *               Interface Router Advertisement Status. Since This module
 *               Corresponds to HOST, this function always returns IP6_FAILURE.
 *
 * INPUTS      : i4Ipv6IfIndex - IP6 Interface Index.
 *
 * OUTPUTS     : pi4RetValIpv6IfRouterAdvStatus - Pointer to the Router 
 *                Advertisement Status of the interface.
 *
 * RETURNS     : IP6_FAILURE
 *               
 *
 * NOTES       : 
 ******************************************************************************/
INT1
Ip6GetFsipv6IfRouterAdvStatus (INT4 i4Ipv6IfIndex,
                               INT4 *pi4RetValIpv6IfRouterAdvStatus)
{

    tIp6If             *pIf6 = NULL;

    if (Ip6IsIfFwdEnabled (i4Ipv6IfIndex) == FALSE)
    {
        UNUSED_PARAM (pi4RetValIpv6IfRouterAdvStatus);
        return IP6_FAILURE;
    }

    pIf6 = Ip6ifGetEntry ((UINT4) i4Ipv6IfIndex);
    if (pIf6 == NULL)
    {
        UNUSED_PARAM (pi4RetValIpv6IfRouterAdvStatus);
        return IP6_FAILURE;
    }
    if (pIf6->u1RaCnf & IP6_IF_RA_ADV)
        *pi4RetValIpv6IfRouterAdvStatus = IP6_IF_ROUT_ADV_ENABLED;
    else
        *pi4RetValIpv6IfRouterAdvStatus = IP6_IF_ROUT_ADV_DISABLED;

    return IP6_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : This routine is called from low-level routine for getting the 
 *               interface Router Advertisement Flag. Since This module
 *               Corresponds to HOST, this function always returns IP6_FAILURE.
 *
 * INPUTS      : i4Ipv6IfIndex - IP6 Interface Index.
 *
 * OUTPUTS     : pi4RetValIpv6IfRouterAdvFlags - Pointer to the Router 
 *                Advertisement Flag of the interface.
 *
 * RETURNS     : IP6_FAILURE
 *               
 *
 * NOTES       : 
 ******************************************************************************/
INT1
Ip6GetFsipv6IfRouterAdvFlags (INT4 i4Ipv6IfIndex,
                              INT4 *pi4RetValIpv6IfRouterAdvFlags)
{
    UINT1               u1Mbit, u1_obit;
    tIp6If             *pIf6 = NULL;

    if (Ip6IsIfFwdEnabled (i4Ipv6IfIndex) == FALSE)
    {
        UNUSED_PARAM (pi4RetValIpv6IfRouterAdvFlags);
        return IP6_FAILURE;
    }

    pIf6 = Ip6ifGetEntry ((UINT4) i4Ipv6IfIndex);
    if (pIf6 == NULL)
    {
        UNUSED_PARAM (pi4RetValIpv6IfRouterAdvFlags);
        return IP6_FAILURE;
    }

    u1Mbit = pIf6->u1RaCnf & IP6_IF_M_BIT_ADV;
    u1_obit = pIf6->u1RaCnf & IP6_IF_O_BIT_ADV;

    if (u1Mbit && u1_obit)
    {
        *pi4RetValIpv6IfRouterAdvFlags = IP6_IF_ROUT_ADV_BOTH_BIT;
    }
    else if (u1Mbit)
    {
        *pi4RetValIpv6IfRouterAdvFlags = IP6_IF_ROUT_ADV_M_BIT;
    }
    else if (u1_obit)
    {
        *pi4RetValIpv6IfRouterAdvFlags = IP6_IF_ROUT_ADV_O_BIT;
    }
    else
    {
        *pi4RetValIpv6IfRouterAdvFlags = IP6_IF_ROUT_ADV_NO_BIT;
    }
    return IP6_SUCCESS;

}

/******************************************************************************
 * DESCRIPTION : This routine is called from low-level routine for setting the 
 *               interface Router Advertisement Status. Since This module
 *               Corresponds to HOST, this function always returns IP6_FAILURE.
 *
 * INPUTS      : i4Ipv6IfIndex - IP6 interface Index.
 *               i4SetValIpv6IfRouterAdvStatus - Router Advertisement Status.
 * OUTPUTS     : None.
 *
 * RETURNS     : IP6_FAILURE
 *               
 *
 * NOTES       : 
 ******************************************************************************/

INT1
Ip6SetFsipv6IfRouterAdvStatus (INT4 i4Ipv6IfIndex,
                               INT4 i4SetValIpv6IfRouterAdvStatus)
{
    tIp6If             *pIf6 = NULL;

    if (Ip6IsIfFwdEnabled (i4Ipv6IfIndex) == FALSE)
    {
        UNUSED_PARAM (i4SetValIpv6IfRouterAdvStatus);
        return IP6_FAILURE;
    }

    pIf6 = Ip6ifGetEntry ((UINT4) i4Ipv6IfIndex);
    if (pIf6 == NULL)
    {
        UNUSED_PARAM (i4SetValIpv6IfRouterAdvStatus);
        return IP6_FAILURE;
    }

    if (i4SetValIpv6IfRouterAdvStatus == IP6_IF_ROUT_ADV_ENABLED)
    {
        if (pIf6->u1RaCnf & IP6_IF_RA_ADV)
            return IP6_SUCCESS;

        pIf6->u1RaCnf |= IP6_IF_RA_ADV;
        pIf6->u1RaCnf &= ~IP6_IF_NO_RA_ADV;

        if (gIp6GblInfo.apIp6If[i4Ipv6IfIndex]->u1OperStatus == OPER_UP)
        {
            /* Indicate to ND of the RA Status */
            Nd6ActOnRaCnf (pIf6, IP6_IF_RA_ADV);
        }
    }
    else if (i4SetValIpv6IfRouterAdvStatus == IP6_IF_ROUT_ADV_DISABLED)
    {
        if (pIf6->u1RaCnf & IP6_IF_NO_RA_ADV)
            return IP6_SUCCESS;

        if (gIp6GblInfo.apIp6If[i4Ipv6IfIndex]->u1OperStatus == OPER_UP)
        {
            /* Indicate to ND of the RA Status */
            Nd6ActOnRaCnf (pIf6, IP6_IF_NO_RA_ADV);
        }

        pIf6->u1RaCnf |= IP6_IF_NO_RA_ADV;
        pIf6->u1RaCnf &= ~IP6_IF_RA_ADV;

    }

    return (IP6_SUCCESS);

}

/******************************************************************************
 * DESCRIPTION : This routine is called from low-level routine for setting the 
 *               interface Router Advertisement Status. Since This module
 *               Corresponds to HOST, this function always returns IP6_FAILURE.
 *
 * INPUTS      : i4Ipv6IfIndex - IP6 interface Index.
 *               i4SetValIpv6IfRouterAdvFlags - Router Advertisement Flag
 *
 * OUTPUTS     : None.
 *
 * RETURNS     : IP6_FAILURE
 *
 * NOTES       : 
 ******************************************************************************/
INT1
Ip6SetFsipv6IfRouterAdvFlags (INT4 i4Ipv6IfIndex,
                              INT4 i4SetValIpv6IfRouterAdvFlags)
{
    tIp6If             *pIf6 = NULL;

    if (Ip6IsIfFwdEnabled (i4Ipv6IfIndex) == FALSE)
    {
        UNUSED_PARAM (i4SetValIpv6IfRouterAdvFlags);
        return IP6_FAILURE;
    }

    pIf6 = Ip6ifGetEntry ((UINT4) i4Ipv6IfIndex);
    if (pIf6 == NULL)
    {
        UNUSED_PARAM (i4SetValIpv6IfRouterAdvFlags);
        return IP6_FAILURE;
    }

    switch (i4SetValIpv6IfRouterAdvFlags)
    {

        case IP6_IF_ROUT_ADV_M_BIT:
            pIf6->u1RaCnf |= IP6_IF_M_BIT_ADV;
            break;

        case IP6_IF_ROUT_ADV_O_BIT:
            pIf6->u1RaCnf |= IP6_IF_O_BIT_ADV;
            break;

        case IP6_IF_ROUT_ADV_NO_M_BIT:
            pIf6->u1RaCnf &= ~IP6_IF_M_BIT_ADV;
            break;

        case IP6_IF_ROUT_ADV_NO_O_BIT:
            pIf6->u1RaCnf &= ~IP6_IF_O_BIT_ADV;
            break;

        case IP6_IF_ROUT_ADV_BOTH_BIT:
            pIf6->u1RaCnf |= IP6_IF_M_BIT_ADV;
            pIf6->u1RaCnf |= IP6_IF_O_BIT_ADV;
            break;

        case IP6_IF_ROUT_ADV_NO_BIT:
            pIf6->u1RaCnf &= ~IP6_IF_M_BIT_ADV;
            pIf6->u1RaCnf &= ~IP6_IF_O_BIT_ADV;
            break;

    }

    return (IP6_SUCCESS);

}

/******************************************************************************
 * DESCRIPTION : This routine is called from low-level Test routine of the 
 *               interface Router Advertisement Status. Since This module
 *               Corresponds to HOST, this function always returns IP6_FAILURE.
 *
 * INPUTS      : i4Ipv6IfIndex - IP6 Interface Index.
 *               i4TestValIpv6IfRouterAdvStatus - Value of Router Advertisement
 *                                              status to be tested.
 *
 * OUTPUTS     : pu4ErrorCode - Pointer to the  Error Code
 *
 * RETURNS     : IP6_FAILURE
 *               
 * NOTES       : 
 ******************************************************************************/
INT1
Ip6Testv2Fsipv6IfRouterAdvStatus (UINT4 *pu4ErrorCode, INT4 i4Ipv6IfIndex,
                                  INT4 i4TestValIpv6IfRouterAdvStatus)
{
    tIp6If             *pIf6 = NULL;

    if (Ip6IsIfFwdEnabled (i4Ipv6IfIndex) == FALSE)
    {
        UNUSED_PARAM (i4TestValIpv6IfRouterAdvStatus);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return IP6_FAILURE;
    }

    pIf6 = Ip6ifGetEntry ((UINT4) i4Ipv6IfIndex);
    if (pIf6 == NULL)
    {
        UNUSED_PARAM (i4TestValIpv6IfRouterAdvStatus);
        return IP6_FAILURE;
    }

    if ((pIf6->u1IfType != IP6_ENET_INTERFACE_TYPE) &&
#ifdef TUNNEL_WANTED
        (pIf6->u1IfType != IP6_TUNNEL_INTERFACE_TYPE) &&
#endif /* TUNNEL_WANTED */
        (pIf6->u1IfType != IP6_PSEUDO_WIRE_INTERFACE_TYPE) &&
        (pIf6->u1IfType != IP6_L3VLAN_INTERFACE_TYPE) &&
        (pIf6->u1IfType != IP6_LAGG_INTERFACE_TYPE) &&
        (pIf6->u1IfType != IP6_L3SUB_INTF_TYPE))
    {
        /* RA is currently not supported for these interfaces. */
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
        return (IP6_FAILURE);
    }

    if (i4TestValIpv6IfRouterAdvStatus != IP6_IF_ROUT_ADV_ENABLED &&
        i4TestValIpv6IfRouterAdvStatus != IP6_IF_ROUT_ADV_DISABLED)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_INVALID_RT_ADV_STATUS);
        return (IP6_FAILURE);
    }

    return (IP6_SUCCESS);

}

/******************************************************************************
 * DESCRIPTION : This routine is called from low-level Test routine of the 
 *               Interface Router Advertisement Flags. Since This module
 *               Corresponds to HOST, this function always returns IP6_FAILURE.
 *
 * INPUTS      : i4Ipv6IfIndex - IP6 Interface Index.
 *               i4TestValIpv6IfRouterAdvFlags- Value of Router Advertisement
 *                                              Flag to be tested.
 *
 * OUTPUTS     : pu4ErrorCode - Pointer to the  Error Code
 *
 * RETURNS     : IP6_FAILURE
 *               
 * NOTES       : 
 ******************************************************************************/
INT1
Ip6Testv2Fsipv6IfRouterAdvFlags (UINT4 *pu4ErrorCode, INT4 i4Ipv6IfIndex,
                                 INT4 i4TestValIpv6IfRouterAdvFlags)
{
    if (Ip6IsIfFwdEnabled (i4Ipv6IfIndex) == FALSE)
    {
        UNUSED_PARAM (i4TestValIpv6IfRouterAdvFlags);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return IP6_FAILURE;
    }
    if (i4TestValIpv6IfRouterAdvFlags != IP6_IF_ROUT_ADV_M_BIT &&
        i4TestValIpv6IfRouterAdvFlags != IP6_IF_ROUT_ADV_O_BIT &&
        i4TestValIpv6IfRouterAdvFlags != IP6_IF_ROUT_ADV_NO_M_BIT &&
        i4TestValIpv6IfRouterAdvFlags != IP6_IF_ROUT_ADV_NO_O_BIT &&
        i4TestValIpv6IfRouterAdvFlags != IP6_IF_ROUT_ADV_BOTH_BIT &&
        i4TestValIpv6IfRouterAdvFlags != IP6_IF_ROUT_ADV_NO_BIT)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_INVALID_RT_ADV_FLAGS);
        return (IP6_FAILURE);
    }

    return (IP6_SUCCESS);

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/******************************************************************************
 * DESCRIPTION : This routine is called from low-level routine for getting the 
 *               Admin status of the address. For host operstatus depends both on
 *               result of DAD and preffered and valid life time of the address.
 *
 * INPUTS      : i4Ipv6IfIndex - If index over which the address is assigned.
 *               pIpv6AddrAddress - Pointer to the address( SNMP octet string)
 *
 * OUTPUTS     : pi4RetValIpv6AddrStatus - Pointer to the status of the 
 *                                             address.
 *
 * RETURNS     : SNMP_SUCCESS/SNMP_FAILURE
 *               
 *
 * NOTES       : 
 ******************************************************************************/
INT1
Ip6GetIpv6AddrStatus (INT4 i4Ipv6IfIndex,
                      tSNMP_OCTET_STRING_TYPE * pIpv6AddrAddress,
                      INT4 *pi4RetValIpv6AddrStatus)
{
#define IP6_ADDR_PREFERRED 0x01
    tIp6If             *pIf6 = gIp6GblInfo.apIp6If[i4Ipv6IfIndex];
    tTMO_SLL_NODE      *pSllAddr = NULL;
    UINT1               u1PfxLen = 0;
    UINT1               u1AddrType = 0;

    tIp6AddrInfo       *pAddrInfo = NULL;
    tIp6LlocalInfo     *pLlocalInfo = NULL;
    tIp6Addr            Ipv6Address;

    if (pIf6 == NULL)
    {
        return IP6_FAILURE;
    }
    if (Ip6IsIfFwdEnabled (i4Ipv6IfIndex) == TRUE)
    {
        u1AddrType =
            Ip6AddrType ((tIp6Addr *) (VOID *) pIpv6AddrAddress->pu1_OctetList);
        if (u1AddrType == ADDR6_LLOCAL)
        {
            TMO_SLL_Scan (&pIf6->lla6Ilist, pSllAddr, tTMO_SLL_NODE *)
            {
                pLlocalInfo = IP6_LLADDR_PTR_FROM_SLL (pSllAddr);
                if (MEMCMP
                    (&pLlocalInfo->ip6Addr, pIpv6AddrAddress->pu1_OctetList,
                     sizeof (tIp6Addr)) == 0)
                {
                    u1PfxLen = 128;
                    break;
                }
            }
        }
        else
        {
            TMO_SLL_Scan (&pIf6->addr6Ilist, pSllAddr, tTMO_SLL_NODE *)
            {
                pAddrInfo = IP6_ADDR_PTR_FROM_SLL (pSllAddr);
                if (MEMCMP
                    (&pAddrInfo->ip6Addr, pIpv6AddrAddress->pu1_OctetList,
                     sizeof (tIp6Addr)) == 0)
                {
                    u1PfxLen = pAddrInfo->u1PrefLen;
                    break;
                }
            }
        }
        if (pSllAddr == NULL)
            return IP6_FAILURE;
        nmhGetFsipv6AddrOperStatus (i4Ipv6IfIndex, pIpv6AddrAddress, u1PfxLen,
                                    pi4RetValIpv6AddrStatus);

        if (*pi4RetValIpv6AddrStatus == (UINT1) ADDR6_COMPLETE (pIf6))
        {
            *pi4RetValIpv6AddrStatus = IP6_ADDR_STAT_PREFERRED;
        }
        else if (*pi4RetValIpv6AddrStatus == IP6_ADDR_OPER_COMPLETE)
        {
            *pi4RetValIpv6AddrStatus = IP6_ADDR_STAT_PREFERRED;
        }
        else if (*pi4RetValIpv6AddrStatus == ADDR6_TENTATIVE)
        {
            *pi4RetValIpv6AddrStatus = IP6_ADDR_STAT_UNKNOWN;
        }
        else if (*pi4RetValIpv6AddrStatus == ADDR6_DOWN)
        {
            *pi4RetValIpv6AddrStatus = IP6_ADDR_STAT_INACCESSIBLE;
        }
        else
        {
            *pi4RetValIpv6AddrStatus = IP6_ADDR_STAT_INVALID;
        }
        return IP6_SUCCESS;
    }
    else
    {
        Ip6AddrCopy (&Ipv6Address,
                     (tIp6Addr *) (VOID *) pIpv6AddrAddress->pu1_OctetList);
        if (IS_ADDR_LLOCAL (Ipv6Address))
        {
            pLlocalInfo
                = Ip6AddrTblGetLlocalEntry ((UINT4) i4Ipv6IfIndex,
                                            (tIp6Addr *) & Ipv6Address);
            if (pLlocalInfo != NULL)
            {

                if ((pLlocalInfo->u1Status & ADDR6_TENTATIVE) &&
                    (pLlocalInfo->u1Status & ADDR6_UP))
                {
                    /* Interface is UP, address is UP and DAD is tentative */
                    *pi4RetValIpv6AddrStatus = IP6_ADDR_STAT_INACCESSIBLE;
                    return IP6_SUCCESS;
                }
                else if ((pLlocalInfo->u1Status & (UINT1) ADDR6_COMPLETE (pIf6))
                         && (pLlocalInfo->u1Status & ADDR6_UP))
                {
                    /* Interface is UP, address is UP and DAD is complete */
                    if (pLlocalInfo->u1Status & ADDR6_PREFERRED)
                        *pi4RetValIpv6AddrStatus = IP6_ADDR_STAT_PREFERRED;
                    else
                        *pi4RetValIpv6AddrStatus = IP6_ADDR_STAT_DEPRECATE;
                    return IP6_SUCCESS;
                }
                else if ((pLlocalInfo->u1Status & ADDR6_FAILED) &&
                         (pLlocalInfo->u1Status & ADDR6_UP))
                {
                    /* Interface is UP, address is UP and DAD has failed */
                    *pi4RetValIpv6AddrStatus = IP6_ADDR_STAT_INACCESSIBLE;
                    return IP6_SUCCESS;
                }
                else if ((pLlocalInfo->u1Status == ADDR6_DOWN) ||
                         (pLlocalInfo->u1Status & ADDR6_UP))
                {
                    /* Interface DOWN or address is DOWN */
                    *pi4RetValIpv6AddrStatus = IP6_ADDR_STAT_INACCESSIBLE;
                    return IP6_SUCCESS;
                }

                return IP6_FAILURE;
            }
            else                /* Ip6AddrTblGetLlocalEntry returned NULL */
            {
                *pi4RetValIpv6AddrStatus = IP6_ADDR_STAT_INVALID;
                return IP6_FAILURE;
            }
        }

        pAddrInfo =
            Ip6AddrTblGetEntry ((UINT4) i4Ipv6IfIndex,
                                (tIp6Addr *) & Ipv6Address, 0);

        if (pAddrInfo == NULL)
        {
            *pi4RetValIpv6AddrStatus = IP6_ADDR_STAT_INVALID;
            return IP6_FAILURE;
        }
        if ((pAddrInfo->u1Status & ADDR6_TENTATIVE) &&
            (pAddrInfo->u1Status & ADDR6_UP))
        {
            /* Interface is UP, address is UP and DAD is tentative */
            *pi4RetValIpv6AddrStatus = IP6_ADDR_STAT_INACCESSIBLE;
            return IP6_SUCCESS;
        }
        else if ((pAddrInfo->u1Status & (UINT1) ADDR6_COMPLETE (pIf6)) &&
                 (pAddrInfo->u1Status & ADDR6_UP))
        {
            /* Interface is UP, address is UP and DAD is complete */
            if (pAddrInfo->u1Status & ADDR6_PREFERRED)
                *pi4RetValIpv6AddrStatus = IP6_ADDR_STAT_PREFERRED;
            else
                *pi4RetValIpv6AddrStatus = IP6_ADDR_STAT_DEPRECATE;

            return IP6_SUCCESS;
        }
        else if ((pAddrInfo->u1Status & ADDR6_FAILED) &&
                 (pAddrInfo->u1Status & ADDR6_UP))
        {
            /* Interface is UP, address is UP and DAD has failed */
            *pi4RetValIpv6AddrStatus = IP6_ADDR_STAT_INACCESSIBLE;
            return IP6_SUCCESS;
        }
        else if ((pAddrInfo->u1Status == ADDR6_DOWN) ||
                 (pAddrInfo->u1Status & ADDR6_UP))
        {
            /* Interface DOWN or address is DOWN */
            *pi4RetValIpv6AddrStatus = IP6_ADDR_STAT_INACCESSIBLE;
            return IP6_SUCCESS;
        }

        return IP6_FAILURE;
    }
    return IP6_FAILURE;
}

/******************************************************************************
 * DESCRIPTION : This routine is called from low-level routine for getting the 
 *               operstatus of the address. For host operstatus depends both on
 *               result of DAD and preffered and valid life time of the address.
 *
 * INPUTS      : i4Ipv6AddrIndex - If index over which the address is assigned.
 *               pIpv6AddrAddress - Pointer to the address( SNMP octet string)
 *               i4Ipv6AddrPrefixLen - Address prefix length.
 *
 * OUTPUTS     : pi4RetValIpv6AddrOperStatus - Pointer to the operstatus of the 
 *                                             address.
 *
 * RETURNS     : SNMP_SUCCESS/SNMP_FAILURE
 *               
 *
 * NOTES       : 
 ******************************************************************************/
INT1
Ip6GetFsipv6AddrOperStatus (INT4 i4Ipv6AddrIndex,
                            tSNMP_OCTET_STRING_TYPE * pIpv6AddrAddress,
                            INT4 i4Ipv6AddrPrefixLen,
                            INT4 *pi4RetValIpv6AddrOperStatus)
{
    tIp6AddrInfo       *pAddrInfo = NULL;
    tIp6LlocalInfo     *pLlocalInfo = NULL;
    tIp6Addr            Ipv6Address;

    tIp6If             *pIf6 = NULL;
    Ip6AddrCopy (&Ipv6Address, (tIp6Addr *) (VOID *) pIpv6AddrAddress->
                 pu1_OctetList);

    pIf6 = gIp6GblInfo.apIp6If[i4Ipv6AddrIndex];

    if (pIf6 == NULL)
    {
        return IP6_FAILURE;
    }
    /*same behavior for both router or host interface */
    if (Ip6AddrType (&Ipv6Address) == ADDR6_LLOCAL)
    {
        if ((pLlocalInfo =
             Ip6AddrTblGetLlocalEntry ((UINT4) i4Ipv6AddrIndex,
                                       &Ipv6Address)) != NULL)
        {

            if ((pLlocalInfo->u1Status & ADDR6_TENTATIVE) &&
                (pLlocalInfo->u1Status & ADDR6_UP))
            {
                /* Interface is UP, address is UP and DAD is tentative */
                *pi4RetValIpv6AddrOperStatus = IP6_ADDR_OPER_TENTATIVE;
                return SNMP_SUCCESS;
            }
            else if ((pLlocalInfo->u1Status & (UINT1) ADDR6_COMPLETE (pIf6)) &&
                     (pLlocalInfo->u1Status & ADDR6_UP))
            {
                /* Interface is UP, address is UP and DAD is complete */
                *pi4RetValIpv6AddrOperStatus = IP6_ADDR_OPER_COMPLETE;
                return SNMP_SUCCESS;
            }
            else if ((pLlocalInfo->u1Status & ADDR6_FAILED) &&
                     (pLlocalInfo->u1Status & ADDR6_UP))
            {
                /* Interface is UP, address is UP and DAD has failed */
                *pi4RetValIpv6AddrOperStatus = IP6_ADDR_OPER_FAILED;
                return SNMP_SUCCESS;
            }
            else if ((pLlocalInfo->u1Status == ADDR6_DOWN) ||
                     (pLlocalInfo->u1Status & ADDR6_UP))
            {
                /* Interface DOWN or address is DOWN */
                *pi4RetValIpv6AddrOperStatus = IP6_ADDR_OPER_DOWN;
                return SNMP_SUCCESS;
            }

            return SNMP_FAILURE;
        }
        else                    /* Ip6AddrTblGetLlocalEntry returned NULL */
            return SNMP_FAILURE;

    }

    pAddrInfo =
        Ip6AddrTblGetEntry ((UINT4) i4Ipv6AddrIndex,
                            (tIp6Addr *) & Ipv6Address,
                            (UINT1) i4Ipv6AddrPrefixLen);

    if (pAddrInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    if ((pAddrInfo->u1Status & ADDR6_TENTATIVE) &&
        (pAddrInfo->u1Status & ADDR6_UP))
    {
        /* Interface is UP, address is UP and DAD is tentative */
        *pi4RetValIpv6AddrOperStatus = IP6_ADDR_OPER_TENTATIVE;
        return SNMP_SUCCESS;
    }
    else if ((pAddrInfo->u1Status & (UINT1) ADDR6_COMPLETE (pIf6)) &&
             (pAddrInfo->u1Status & ADDR6_UP))
    {
        /* Interface is UP, address is UP and DAD is complete */
        *pi4RetValIpv6AddrOperStatus = IP6_ADDR_OPER_COMPLETE;
        return SNMP_SUCCESS;
    }
    else if ((pAddrInfo->u1Status & ADDR6_FAILED) &&
             (pAddrInfo->u1Status & ADDR6_UP))
    {
        /* Interface is UP, address is UP and DAD has failed */
        *pi4RetValIpv6AddrOperStatus = IP6_ADDR_OPER_FAILED;
        return SNMP_SUCCESS;
    }
    else if ((pAddrInfo->u1Status == ADDR6_DOWN) ||
             (pAddrInfo->u1Status & ADDR6_UP))
    {
        /* Interface DOWN or address is DOWN */
        *pi4RetValIpv6AddrOperStatus = IP6_ADDR_OPER_DOWN;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/******************************************************************************
 * DESCRIPTION : This routine is called from low-level routine for testing whether 
 *               the value set for enabling/disabling IP Forwarding is correct/not. 
 *               Since This module Corresponds to HOST, this function always returns 
 *               IP6_FAILURE.
 *
 * INPUTS      : i4TestValIpv6Forwarding - Value set for IP Forwarding Object 
 *
 * OUTPUTS     : pu4ErrorCode - pointer to the Error Code.
 *
 * RETURNS     : IP6_FAILURE
 *               
 *
 * NOTES       : 
 ******************************************************************************/
INT1
Ip6Testv2Ipv6Forwarding (UINT4 *pu4ErrorCode, INT4 i4TestValIpv6Forwarding)
{
    if ((i4TestValIpv6Forwarding != IP6_FORW_ENABLE) &&
        i4TestValIpv6Forwarding != IP6_FORW_DISABLE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : This routine is called from low-level Test routine of the 
 *               Interface Router Advertisement Status. Since This module
 *               Corresponds to HOST, this function always returns IP6_FAILURE.
 *
 * INPUTS      : i4Ipv6IfIndex - IP6 If Index.
 *               i4TestValIpv6IfPrefixAdvStatus- Value of Router Advertisement
 *                                              Flag to be tested.
 *
 * OUTPUTS     : pu4ErrorCode - Pointer to the  Error Code
 *
 * RETURNS     : IP6_FAILURE
 *               
 * NOTES       : 
 ******************************************************************************/
INT1
Ip6Testv2Fsipv6IfPrefixAdvStatus (UINT4 *pu4ErrorCode, INT4 i4Ipv6IfIndex,
                                  INT4 i4TestValIpv6IfPrefixAdvStatus)
{

    if (Ip6IsIfFwdEnabled (i4Ipv6IfIndex) == FALSE)
    {
        UNUSED_PARAM (i4TestValIpv6IfPrefixAdvStatus);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return IP6_FAILURE;
    }

    if (i4TestValIpv6IfPrefixAdvStatus != IP6_IF_PREFIX_ADV_ENABLED &&
        i4TestValIpv6IfPrefixAdvStatus != IP6_IF_PREFIX_ADV_DISABLED)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (IP6_FAILURE);
    }

    return (IP6_SUCCESS);

}

/******************************************************************************
 * DESCRIPTION : This routine is called from low-level Test routine of the 
 *               Interface Min Router Advertisement Time. Since This module
 *               Corresponds to HOST, this function always returns IP6_FAILURE.
 *
 * INPUTS      : i4Ipv6IfIndex - IP6 If Index.
 *               i4TestValIpv6IfMinRouterAdvTime- Value of Router Advertisement
 *                                              Flag to be tested.
 *
 * OUTPUTS     : pu4ErrorCode - Pointer to the  Error Code
 *
 * RETURNS     : IP6_FAILURE
 *               
 * NOTES       : 
 ******************************************************************************/
INT1
Ip6Testv2Fsipv6IfMinRouterAdvTime (UINT4 *pu4ErrorCode,
                                   INT4 i4Ipv6IfIndex,
                                   INT4 i4TestValIpv6IfMinRouterAdvTime)
{
    if (Ip6IsIfFwdEnabled (i4Ipv6IfIndex) == FALSE)
    {
        UNUSED_PARAM (i4TestValIpv6IfMinRouterAdvTime);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return IP6_FAILURE;
    }
    if (i4TestValIpv6IfMinRouterAdvTime < IP6_IF_MIN_RA_MIN_TIME)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (IP6_FAILURE);
    }

    /* MinRouterAdvTime should not be > 3/4 of MaxRouterAdvTime */
    if (i4TestValIpv6IfMinRouterAdvTime >
        (INT4) ((3 * IP6_IF_MAX_RA_TIME (gIp6GblInfo.apIp6If[i4Ipv6IfIndex]))
                / 4))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (IP6_FAILURE);
    }

    return (IP6_SUCCESS);

}

/******************************************************************************
 * DESCRIPTION : This routine is called from low-level Test routine of the 
 *               Interface Max Router Advertisement Time. Since This module
 *               Corresponds to HOST, this function always returns IP6_FAILURE.
 *
 * INPUTS      : i4Ipv6IfIndex - IP6 If Index.
 *               i4TestValIpv6IfMaxRouterAdvTime- Value of Router Advertisement
 *                                              Flag to be tested.
 *
 * OUTPUTS     : pu4ErrorCode - Pointer to the  Error Code
 *
 * RETURNS     : IP6_FAILURE
 *               
 * NOTES       : 
 ******************************************************************************/
INT1
Ip6Testv2Fsipv6IfMaxRouterAdvTime (UINT4 *pu4ErrorCode,
                                   INT4 i4Ipv6IfIndex,
                                   INT4 i4TestValIpv6IfMaxRouterAdvTime)
{
    if (Ip6IsIfFwdEnabled (i4Ipv6IfIndex) == FALSE)
    {
        UNUSED_PARAM (i4TestValIpv6IfMaxRouterAdvTime);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return IP6_FAILURE;
    }
    if (i4TestValIpv6IfMaxRouterAdvTime < IP6_IF_MAX_RA_MIN_TIME ||
        i4TestValIpv6IfMaxRouterAdvTime > IP6_IF_MAX_RA_MAX_TIME)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_INVALID_RA_INTERVAL);
        return (IP6_FAILURE);
    }

    /* If Default Route LifeTime is set, then this value should be
     * <= LifeTime value. */
    if (((IP6_IF_DEFTIME (gIp6GblInfo.apIp6If[i4Ipv6IfIndex]) != 0) &&
         (i4TestValIpv6IfMaxRouterAdvTime >
          IP6_IF_DEFTIME (gIp6GblInfo.apIp6If[i4Ipv6IfIndex]))) ||
        (i4TestValIpv6IfMaxRouterAdvTime <=
         (INT4) IP6_IF_MIN_RA_TIME (gIp6GblInfo.apIp6If[i4Ipv6IfIndex])))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_INVALID_RA_INTERVAL);
        return (IP6_FAILURE);
    }

    return (IP6_SUCCESS);

}

/******************************************************************************
 * DESCRIPTION : This routine is called from low-level Test routine of the 
 *               Interface Default Router Life Time  Since This module
 *               Corresponds to HOST, this function always returns IP6_FAILURE.
 *
 * INPUTS      : i4Ipv6IfIndex - IP6 If Index.
 *               i4TestValIpv6IfMaxRouterAdvTime- Value of Router Advertisement
 *                                              Flag to be tested.
 *
 * OUTPUTS     : pu4ErrorCode - Pointer to the  Error Code
 *
 * RETURNS     : IP6_FAILURE
 *               
 * NOTES       : 
 ******************************************************************************/
INT1
Ip6Testv2Fsipv6IfDefRouterTime (UINT4 *pu4ErrorCode, INT4 i4Ipv6IfIndex,
                                INT4 i4TestValIpv6IfDefRouterTime)
{
    if (Ip6IsIfFwdEnabled (i4Ipv6IfIndex) == FALSE)
    {
        UNUSED_PARAM (i4TestValIpv6IfDefRouterTime);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return IP6_FAILURE;
    }
    /* Should be 0, or (>= MAX_RA_TIME && <= MAX_DEFTIME) */
    if ((i4TestValIpv6IfDefRouterTime == 0) ||
        ((i4TestValIpv6IfDefRouterTime >=
          (INT4) IP6_IF_MAX_RA_TIME (gIp6GblInfo.apIp6If[i4Ipv6IfIndex])) &&
         (i4TestValIpv6IfDefRouterTime <= IP6_IF_MAX_DEFTIME)))
    {
        return IP6_SUCCESS;
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    CLI_SET_ERR (CLI_IP6_INVALID_RA_LIFETIME);
    return (IP6_FAILURE);
}

/******************************************************************************
 * DESCRIPTION : This routine is called from low-level Get Routine for
 *               fetching the Profile Index of Prefix to be advertised in
 *               Router Advertisement message.
 *
 * INPUTS      : i4Fsipv6PrefixIndex - Interface Index
 *               pFsipv6PrefixAddress - Prefix to be advertised
 *               i4Fsipv6PrefixAddrLen - Prefix length of the advertised prefix
 *
 * OUTPUTS     : pi4RetValFsipv6PrefixProfileIndex - Profile index of the
 *                                                   prefix.
 *
 * RETURNS     : IP6_FAILURE / IP6_SUCCESS
 *               
 * NOTES       : 
 ******************************************************************************/
INT1
Ip6GetFsipv6PrefixProfileIndex (INT4 i4Fsipv6PrefixIndex,
                                tSNMP_OCTET_STRING_TYPE * pFsipv6PrefixAddress,
                                INT4 i4Fsipv6PrefixAddrLen,
                                INT4 *pi4RetValFsipv6PrefixProfileIndex)
{
    tIp6PrefixNode     *pPrefix = NULL;

    if (Ip6IsIfFwdEnabled (i4Fsipv6PrefixIndex) == FALSE)
    {
        UNUSED_PARAM (pFsipv6PrefixAddress);
        UNUSED_PARAM (i4Fsipv6PrefixAddrLen);
        UNUSED_PARAM (pi4RetValFsipv6PrefixProfileIndex);
        return IP6_FAILURE;
    }

    pPrefix = Ip6GetRAPrefix ((UINT4) i4Fsipv6PrefixIndex,
                              (tIp6Addr *) (VOID *) pFsipv6PrefixAddress->
                              pu1_OctetList, (UINT1) i4Fsipv6PrefixAddrLen);
    if (pPrefix != NULL)
    {
        *pi4RetValFsipv6PrefixProfileIndex = pPrefix->u2ProfileIndex;
        return IP6_SUCCESS;
    }

    return IP6_FAILURE;
}

/******************************************************************************
 * DESCRIPTION : This routine is called from low-level Get Routine for
 *               fetching the RpPrefix Flag of Prefix to be advertised in
 *               Router Advertisement message.
 *
 * INPUTS      : i4Fsipv6PrefixIndex - Interface Index
 *               pFsipv6PrefixAddress - Prefix to be advertised
 *               i4Fsipv6PrefixAddrLen - Prefix length of the advertised prefix
 *
 * OUTPUTS     : pi4RetValFsipv6PrefixProfileIndex - Profile index of the
 *                                                   prefix.
 *
 * RETURNS     : IP6_FAILURE / IP6_SUCCESS
 *               
 * NOTES       : 
 ******************************************************************************/
INT1
Ip6GetFsipv6SupportEmbeddedRp (INT4 i4Fsipv6PrefixIndex,
                               tSNMP_OCTET_STRING_TYPE * pFsipv6PrefixAddress,
                               INT4 i4Fsipv6PrefixAddrLen,
                               INT4 *pi4RetValFsipv6SupportEmbeddedRp)
{
    tIp6PrefixNode     *pPrefix = NULL;

    pPrefix = Ip6GetRAPrefix ((UINT4) i4Fsipv6PrefixIndex,
                              (tIp6Addr *) (VOID *) pFsipv6PrefixAddress->
                              pu1_OctetList, (UINT1) i4Fsipv6PrefixAddrLen);
    if (pPrefix != NULL)
    {
        *pi4RetValFsipv6SupportEmbeddedRp = pPrefix->u1EmbdRpValid;
        return IP6_SUCCESS;
    }

    return IP6_FAILURE;
}

/******************************************************************************
 * DESCRIPTION : This routine is called from low-level Get Routine for
 *               fetching the Admin Status of Prefix to be advertised in
 *               Router Advertisement message.
 *
 * INPUTS      : i4Fsipv6PrefixIndex - Interface Index
 *               pFsipv6PrefixAddress - Prefix to be advertised
 *               i4Fsipv6PrefixAddrLen - Prefix length of the advertised prefix
 *
 * OUTPUTS     : pi4RetValFsipv6PrefixAdminStatus - Admin Status of the prefix.
 *
 * RETURNS     : IP6_FAILURE / IP6_SUCCESS
 *               
 * NOTES       : 
 ******************************************************************************/
INT1
Ip6GetFsipv6PrefixAdminStatus (INT4 i4Fsipv6PrefixIndex,
                               tSNMP_OCTET_STRING_TYPE * pFsipv6PrefixAddress,
                               INT4 i4Fsipv6PrefixAddrLen,
                               INT4 *pi4RetValFsipv6PrefixAdminStatus)
{
    tIp6PrefixNode     *pPrefix = NULL;

    if (Ip6IsIfFwdEnabled (i4Fsipv6PrefixIndex) == FALSE)
    {
        UNUSED_PARAM (pFsipv6PrefixAddress);
        UNUSED_PARAM (i4Fsipv6PrefixAddrLen);
        UNUSED_PARAM (pi4RetValFsipv6PrefixAdminStatus);
        return IP6_FAILURE;
    }

    pPrefix = Ip6GetRAPrefix ((UINT4) i4Fsipv6PrefixIndex,
                              (tIp6Addr *) (VOID *) pFsipv6PrefixAddress->
                              pu1_OctetList, (UINT1) i4Fsipv6PrefixAddrLen);
    if (pPrefix != NULL)
    {
        *pi4RetValFsipv6PrefixAdminStatus = pPrefix->u1AdminStatus;
        return IP6_SUCCESS;
    }

    return IP6_FAILURE;
}

/******************************************************************************
 * DESCRIPTION : This routine is called from low-level Set Routine for
 *               setting the Profile Index of Prefix to be advertised in
 *               Router Advertisement message.
 *
 * INPUTS      : i4Fsipv6PrefixIndex - Interface Index
 *               pFsipv6PrefixAddress - Prefix to be advertised
 *               i4Fsipv6PrefixAddrLen - Prefix length of the advertised prefix
 *
 * OUTPUTS     : i4SetValFsipv6PrefixProfileIndex - Profile Index of the prefix
 *
 * RETURNS     : IP6_FAILURE / IP6_SUCCESS
 *               
 * NOTES       : 
 ******************************************************************************/
INT1
Ip6SetFsipv6PrefixProfileIndex (INT4 i4Fsipv6PrefixIndex,
                                tSNMP_OCTET_STRING_TYPE * pFsipv6PrefixAddress,
                                INT4 i4Fsipv6PrefixAddrLen,
                                INT4 i4SetValFsipv6PrefixProfileIndex)
{
    tIp6PrefixNode     *pPrefix = NULL;
    UINT4               u4ContextId = 0;

    if (Ip6IsIfFwdEnabled (i4Fsipv6PrefixIndex) == FALSE)
    {
        UNUSED_PARAM (pFsipv6PrefixAddress);
        UNUSED_PARAM (i4Fsipv6PrefixAddrLen);
        UNUSED_PARAM (i4SetValFsipv6PrefixProfileIndex);
        return IP6_FAILURE;
    }

    Ip6GetCxtId (i4Fsipv6PrefixIndex, &u4ContextId);

    pPrefix = Ip6GetRAPrefix ((UINT4) i4Fsipv6PrefixIndex,
                              (tIp6Addr *) (VOID *) pFsipv6PrefixAddress->
                              pu1_OctetList, (UINT1) i4Fsipv6PrefixAddrLen);
    if (pPrefix != NULL)
    {
        if (pPrefix->u2ProfileIndex != (UINT2) i4SetValFsipv6PrefixProfileIndex)
        {
            /* Stop the Prefer and Valid Timer if running */
            if (pPrefix->ValidTimer.u1Id != 0)
            {
                Ip6TmrStop (u4ContextId, IP6_PREFIX_VALID_TIMER_ID,
                            gIp6GblInfo.Ip6TimerListId,
                            &(pPrefix->ValidTimer.appTimer));
            }

            if (pPrefix->PreferTimer.u1Id != 0)
            {
                Ip6TmrStop (u4ContextId, IP6_PREFIX_PREF_TIMER_ID,
                            gIp6GblInfo.Ip6TimerListId,
                            &(pPrefix->PreferTimer.appTimer));
            }

            /* Decrease the Ref Count for Current Associated Profile Entry */
            if (pPrefix->u2ProfileIndex <
                FsIP6SizingParams[MAX_IP6_ADDR_PROFILES_SIZING_ID].
                u4PreAllocatedUnits)
            {
                IP6_ADDR_PROF_REF_COUNT (pPrefix->u2ProfileIndex)--;
                if (IP6_ADDR_PROF_REF_COUNT (pPrefix->u2ProfileIndex) == 0)
                {
                    IP6_ADDR_PROF_ADMIN (pPrefix->u2ProfileIndex)
                        = IP6_ADDR_PROF_INVALID;
                    if (pPrefix->u2ProfileIndex <
                        gIp6GblInfo.u4NextProfileIndex)
                    {
                        gIp6GblInfo.u4NextProfileIndex =
                            pPrefix->u2ProfileIndex;
                        gIp6GblInfo.u4MaxAssignedProfileIndex =
                            gIp6GblInfo.u4NextProfileIndex;
                    }
                }
            }

            /* Assign the New Profile Index */
            pPrefix->u2ProfileIndex = (UINT2) i4SetValFsipv6PrefixProfileIndex;

            /* Increase the Ref Count for Current Associated Profile Entry */
            IP6_ADDR_PROF_REF_COUNT (i4SetValFsipv6PrefixProfileIndex)++;

            /* If the Valid and Prefer Time Associated with this prefix is
             * variable then start the corresponding timers. */
            if (IP6_ADDR_VALID_TIME_FIXED (pPrefix->u2ProfileIndex) == 0)
            {
                /* Starting the Variable Valid Timer */
                Ip6TmrStart (u4ContextId, IP6_PREFIX_VALID_TIMER_ID,
                             gIp6GblInfo.Ip6TimerListId,
                             &pPrefix->ValidTimer.appTimer,
                             IP6_ADDR_VALID_TIME (pPrefix->u2ProfileIndex));
            }

            if (IP6_ADDR_PREF_TIME_FIXED (pPrefix->u2ProfileIndex) == 0)
            {
                /* Starting the Variable Prefer Timer */
                Ip6TmrStart (u4ContextId, IP6_PREFIX_PREF_TIMER_ID,
                             gIp6GblInfo.Ip6TimerListId,
                             &pPrefix->PreferTimer.appTimer,
                             IP6_ADDR_PREF_TIME (pPrefix->u2ProfileIndex));
            }
        }
        return IP6_SUCCESS;
    }

    return IP6_FAILURE;
}

/******************************************************************************
 * DESCRIPTION : This routine is called from low-level Set Routine for
 *               setting the Profile Index of Prefix to be advertised in
 *               Router Advertisement message.
 *
 * INPUTS      : i4Fsipv6PrefixIndex - Interface Index
 *               pFsipv6PrefixAddress - Prefix to be advertised
 *               i4Fsipv6PrefixAddrLen - Prefix length of the advertised prefix
 *
 * OUTPUTS     : i4SetValFsipv6PrefixProfileIndex - Profile Index of the prefix
 *
 * RETURNS     : IP6_FAILURE / IP6_SUCCESS
 *               
 * NOTES       : 
 ******************************************************************************/
INT1
Ip6SetFsipv6SupportEmbeddedRp (INT4 i4Fsipv6PrefixIndex,
                               tSNMP_OCTET_STRING_TYPE * pFsipv6PrefixAddress,
                               INT4 i4Fsipv6PrefixAddrLen,
                               INT4 i4SetValFsipv6SupportEmbeddedRp)
{
    tIp6PrefixNode     *pPrefix = NULL;
    UINT4               u4ContextId = 0;

    Ip6GetCxtId (i4Fsipv6PrefixIndex, &u4ContextId);

    pPrefix = Ip6GetRAPrefix ((UINT4) i4Fsipv6PrefixIndex,
                              (tIp6Addr *) (VOID *) pFsipv6PrefixAddress->
                              pu1_OctetList, (UINT1) i4Fsipv6PrefixAddrLen);
    if (pPrefix != NULL)
    {
        if (pPrefix->u1EmbdRpValid != (UINT1) i4SetValFsipv6SupportEmbeddedRp)
        {
            pPrefix->u1EmbdRpValid = (UINT1) i4SetValFsipv6SupportEmbeddedRp;
        }
        return IP6_SUCCESS;
    }

    return IP6_FAILURE;
}

/******************************************************************************
 * DESCRIPTION : This routine is called from low-level Set Routine for
 *               setting the Admin Status of Prefix to be advertised in
 *               Router Advertisement message.
 *
 * INPUTS      : i4Fsipv6PrefixIndex - Interface Index
 *               pFsipv6PrefixAddress - Prefix to be advertised
 *               i4Fsipv6PrefixAddrLen - Prefix length of the advertised prefix
 *
 * OUTPUTS     : i4SetValFsipv6PrefixAdminStatus - Admin Status of the prefix
 *
 * RETURNS     : IP6_FAILURE / IP6_SUCCESS
 *               
 * NOTES       : 
 ******************************************************************************/
INT1
Ip6SetFsipv6PrefixAdminStatus (INT4 i4Fsipv6PrefixIndex,
                               tSNMP_OCTET_STRING_TYPE * pFsipv6PrefixAddress,
                               INT4 i4Fsipv6PrefixAddrLen,
                               INT4 i4SetValFsipv6PrefixAdminStatus)
{
    tIp6PrefixNode     *pPrefix = NULL;
    UINT4               u4ContextId = 0;

    if (Ip6IsIfFwdEnabled (i4Fsipv6PrefixIndex) == FALSE)
    {
        UNUSED_PARAM (pFsipv6PrefixAddress);
        UNUSED_PARAM (i4Fsipv6PrefixAddrLen);
        UNUSED_PARAM (i4SetValFsipv6PrefixAdminStatus);
        return IP6_FAILURE;
    }

    switch (i4SetValFsipv6PrefixAdminStatus)
    {

        case IP6FWD_CREATE_AND_WAIT:
            pPrefix = Ip6RAPrefixCreate ((UINT4) i4Fsipv6PrefixIndex, (tIp6Addr *) (VOID *) pFsipv6PrefixAddress->pu1_OctetList, (UINT1) i4Fsipv6PrefixAddrLen, 0,    /* By Default Prefix is associated with
                                                                                                                                                                     * the profile index 0 */
                                         IP6FWD_NOT_READY);
            if (pPrefix == NULL)
            {
                return IP6_FAILURE;
            }
            break;

        case IP6FWD_ACTIVE:
            pPrefix = Ip6GetRAPrefix ((UINT4) i4Fsipv6PrefixIndex,
                                      (tIp6Addr *) (VOID *)
                                      pFsipv6PrefixAddress->pu1_OctetList,
                                      (UINT1) i4Fsipv6PrefixAddrLen);
            if (pPrefix == NULL)
            {
                return IP6_FAILURE;
            }

            if (pPrefix->u1AdminStatus == IP6FWD_ACTIVE)
            {
                /* Already Active */
                break;
            }

            pPrefix->u1AdminStatus = IP6FWD_ACTIVE;
            Ip6GetCxtId (i4Fsipv6PrefixIndex, &u4ContextId);

            /* If the Valid and Prefer Time Associated with this prefix is
             * variable then start the corresponding timers. */
            if (pPrefix->u2ProfileIndex <
                FsIP6SizingParams[MAX_IP6_ADDR_PROFILES_SIZING_ID].
                u4PreAllocatedUnits)
            {
                if (IP6_ADDR_VALID_TIME_FIXED (pPrefix->u2ProfileIndex) == 0)
                {
                    /* Need to start the Variable Valid Timer */
                    Ip6TmrStart (u4ContextId, IP6_PREFIX_VALID_TIMER_ID,
                                 gIp6GblInfo.Ip6TimerListId,
                                 &pPrefix->ValidTimer.appTimer,
                                 IP6_ADDR_VALID_TIME (pPrefix->u2ProfileIndex));
                }

                if (IP6_ADDR_PREF_TIME_FIXED (pPrefix->u2ProfileIndex) == 0)
                {
                    /* Need to start the Variable Prefer Timer */
                    Ip6TmrStart (u4ContextId, IP6_PREFIX_PREF_TIMER_ID,
                                 gIp6GblInfo.Ip6TimerListId,
                                 &pPrefix->PreferTimer.appTimer,
                                 IP6_ADDR_PREF_TIME (pPrefix->u2ProfileIndex));
                }
            }
            break;

        case IP6FWD_DESTROY:
            pPrefix = Ip6GetRAPrefix ((UINT4) i4Fsipv6PrefixIndex,
                                      (tIp6Addr *) (VOID *)
                                      pFsipv6PrefixAddress->
                                      pu1_OctetList,
                                      (UINT1) i4Fsipv6PrefixAddrLen);
            if (pPrefix == NULL)
            {
                return IP6_FAILURE;
            }

            /* Delete the Prefix from interface */
            Ip6RAPrefixDelete ((UINT4) i4Fsipv6PrefixIndex,
                               (tIp6Addr *) (VOID *) pFsipv6PrefixAddress->
                               pu1_OctetList, (UINT1) i4Fsipv6PrefixAddrLen);
            break;

        default:
            return IP6_FAILURE;
    }

    return IP6_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/******************************************************************************
 * DESCRIPTION : This routine is called from low-level test Routine for
 *               testing the Profile Index of Prefix to be advertised in
 *               Router Advertisement message.
 *
 * INPUTS      : i4Fsipv6PrefixIndex - Interface Index
 *               pFsipv6PrefixAddress - Prefix to be advertised
 *               i4Fsipv6PrefixAddrLen - Prefix length of the advertised prefix
 *               i4TestValFsipv6PrefixProfileIndex -Profile Index of the prefix
 * OUTPUTS     : pu4ErrorCode - Error Code
 *
 * RETURNS     : IP6_FAILURE / IP6_SUCCESS
 *               
 * NOTES       : 
 ******************************************************************************/
INT1
Ip6Testv2Fsipv6PrefixProfileIndex (UINT4 *pu4ErrorCode,
                                   INT4 i4Fsipv6PrefixIndex,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsipv6PrefixAddress,
                                   INT4 i4Fsipv6PrefixAddrLen,
                                   INT4 i4TestValFsipv6PrefixProfileIndex)
{
    INT1                i1AddrType;

    if (Ip6IsIfFwdEnabled (i4Fsipv6PrefixIndex) == FALSE)
    {
        UNUSED_PARAM (*pu4ErrorCode);
        UNUSED_PARAM (pFsipv6PrefixAddress);
        UNUSED_PARAM (i4Fsipv6PrefixAddrLen);
        UNUSED_PARAM (i4TestValFsipv6PrefixProfileIndex);
        return IP6_FAILURE;
    }

    /* Index Validation */
    if (Ip6ifEntryExists ((UINT4) i4Fsipv6PrefixIndex) == IP6_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
        return IP6_FAILURE;
    }

    /* Prefix Validation */
    i1AddrType = Ip6AddrType ((tIp6Addr *) (VOID *) pFsipv6PrefixAddress->
                              pu1_OctetList);
    if (i1AddrType != ADDR6_UNICAST)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_ADDR_TYPE);
        return IP6_FAILURE;
    }

    /* Prefix Len Validation */
    if ((i4Fsipv6PrefixAddrLen <= IP6_ADDR_MIN_PREFIX) ||
        (i4Fsipv6PrefixAddrLen > IP6_ADDR_MAX_PREFIX))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_PREFIXLEN);
        return IP6_FAILURE;
    }

    if ((i4TestValFsipv6PrefixProfileIndex < IP6_MIN_PROFILE_INDEX) ||
        (i4TestValFsipv6PrefixProfileIndex >=
         (INT4) FsIP6SizingParams[MAX_IP6_ADDR_PROFILES_SIZING_ID].
         u4PreAllocatedUnits))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_INVALID_PROF_INDEX);
        return IP6_FAILURE;
    }

    /* Ensure that the Profile is already existing */
    if (Ip6addrProfEntryExists ((UINT4) i4TestValFsipv6PrefixProfileIndex) ==
        IP6_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_IP6_PROFILE_NOT_FOUND);
        return IP6_FAILURE;
    }

    return IP6_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : This routine is called from low-level test Routine for
 *               testing the RpPrefix Flag of Prefix to be advertised in
 *               Router Advertisement message.
 *
 * INPUTS      : i4Fsipv6PrefixIndex - Interface Index
 *               pFsipv6PrefixAddress - Prefix to be advertised
 *               i4Fsipv6PrefixAddrLen - Prefix length of the advertised prefix
 *               i4TestValFsipv6PrefixProfileIndex -Profile Index of the prefix
 * OUTPUTS     : pu4ErrorCode - Error Code
 *
 * RETURNS     : IP6_FAILURE / IP6_SUCCESS
 *               
 * NOTES       : 
 ******************************************************************************/
INT1
Ip6Testv2Fsipv6SupportEmbeddedRp (UINT4 *pu4ErrorCode,
                                  INT4 i4Fsipv6PrefixIndex,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pFsipv6PrefixAddress,
                                  INT4 i4Fsipv6PrefixAddrLen,
                                  INT4 i4TestValFsipv6SupportEmbeddedRp)
{
    INT1                i1AddrType;

    /* Index Validation */
    if (Ip6ifEntryExists ((UINT4) i4Fsipv6PrefixIndex) == IP6_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
        return IP6_FAILURE;
    }

    /* Prefix Validation */
    i1AddrType = Ip6AddrType ((tIp6Addr *) (VOID *) pFsipv6PrefixAddress->
                              pu1_OctetList);
    if (i1AddrType != ADDR6_UNICAST)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_ADDR_TYPE);
        return IP6_FAILURE;
    }

    /* Prefix Len Validation */
    if ((i4Fsipv6PrefixAddrLen <= IP6_ADDR_MIN_PREFIX) ||
        (i4Fsipv6PrefixAddrLen > IP6_ADDR_MAX_PREFIX))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_PREFIXLEN);
        return IP6_FAILURE;
    }
    if ((i4TestValFsipv6SupportEmbeddedRp < IP6_SUPPORT_EMBD_RP_ENABLE) ||
        (i4TestValFsipv6SupportEmbeddedRp > IP6_SUPPORT_EMBD_RP_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_INVALID_EMBEDDED_RP);
        return IP6_FAILURE;
    }

    return IP6_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : This routine is called from low-level Test Routine for
 *               testing the Admin Status of Prefix to be advertised in
 *               Router Advertisement message.
 *
 * INPUTS      : i4Fsipv6PrefixIndex - Interface Index
 *               pFsipv6PrefixAddress - Prefix to be advertised
 *               i4Fsipv6PrefixAddrLen - Prefix length of the advertised prefix
 *               i4TestValFsipv6PrefixAdminStatus - Admin Status of the prefix
 *
 * OUTPUTS     : pu4ErrorCode - Error Code
 *
 * RETURNS     : IP6_FAILURE / IP6_SUCCESS
 *               
 * NOTES       : 
 ******************************************************************************/
INT1
Ip6Testv2Fsipv6PrefixAdminStatus (UINT4 *pu4ErrorCode,
                                  INT4 i4Fsipv6PrefixIndex,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pFsipv6PrefixAddress,
                                  INT4 i4Fsipv6PrefixAddrLen,
                                  INT4 i4TestValFsipv6PrefixAdminStatus)
{
    tIp6PrefixNode     *pPrefix = NULL;
    tIp6RARouteInfoNode *pIp6GetRARoutInfo = NULL;
    tIp6Addr            RARoutePrefix;
    INT1                i1AddrType;

    MEMSET (&RARoutePrefix, 0, sizeof (tIp6Addr));
    Ip6AddrCopy (&RARoutePrefix,
                 (tIp6Addr *) (VOID *) pFsipv6PrefixAddress->pu1_OctetList);

    if (Ip6IsIfFwdEnabled (i4Fsipv6PrefixIndex) == FALSE)
    {
        UNUSED_PARAM (*pu4ErrorCode);
        UNUSED_PARAM (pFsipv6PrefixAddress);
        UNUSED_PARAM (i4Fsipv6PrefixAddrLen);
        UNUSED_PARAM (i4TestValFsipv6PrefixAdminStatus);
        return IP6_FAILURE;
    }

    /* Index Validation */
    if (Ip6ifEntryExists ((UINT4) i4Fsipv6PrefixIndex) == IP6_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
        return IP6_FAILURE;
    }

    /* Prefix Validation */
    i1AddrType = Ip6AddrType ((tIp6Addr *) (VOID *)
                              pFsipv6PrefixAddress->pu1_OctetList);
    if (i1AddrType != ADDR6_UNICAST)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_ADDR_TYPE);
        return IP6_FAILURE;
    }

    /* Prefix Len Validation */
    if ((i4Fsipv6PrefixAddrLen <= 0) ||
        (i4Fsipv6PrefixAddrLen > IP6_ADDR_MAX_PREFIX))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_PREFIXLEN);
        return IP6_FAILURE;
    }

    if ((i4TestValFsipv6PrefixAdminStatus != IP6FWD_CREATE_AND_WAIT) &&
        (i4TestValFsipv6PrefixAdminStatus != IP6FWD_ACTIVE) &&
        (i4TestValFsipv6PrefixAdminStatus != IP6FWD_DESTROY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_INVALID_PREF_ADMIN_STATUS);
        return IP6_FAILURE;
    }

    pPrefix = Ip6GetRAPrefix ((UINT4) i4Fsipv6PrefixIndex,
                              (tIp6Addr *) (VOID *)
                              pFsipv6PrefixAddress->pu1_OctetList,
                              (UINT1) i4Fsipv6PrefixAddrLen);

    pIp6GetRARoutInfo = Ip6GetRARoutInfo ((UINT4) i4Fsipv6PrefixIndex,
                                          &RARoutePrefix,
                                          (UINT1) i4Fsipv6PrefixAddrLen);

    if (i4TestValFsipv6PrefixAdminStatus == IP6FWD_CREATE_AND_WAIT)
    {
        if (pPrefix != NULL)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            CLI_SET_ERR (CLI_IP6_PREFIX_ALREADY_EXISTS);
            return IP6_FAILURE;
        }
        if (pIp6GetRARoutInfo != NULL)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            CLI_SET_ERR (CLI_IP6_RA_ROUTE_INFO_AVAIL_FOR_PREFIX);
            return IP6_FAILURE;
        }
    }
    else
    {
        if (pPrefix == NULL)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            CLI_SET_ERR (CLI_IP6_PREFIX_NOT_FOUND);
            return IP6_FAILURE;
        }
    }

    return IP6_SUCCESS;
}

/****************************************************************************
 Function    :  Ip6ValidateIndexInstanceRtrAdvtTable
 Input       :  i4RtrAdvtIfIndex  -   RA Table Entry Index
 Output      :  None
 Returns     :  SNMP_FAILURE
****************************************************************************/
INT1
Ip6ValidateIndexInstanceRtrAdvtTable (INT4 i4RtrAdvtIfIndex)
{
    tIp6RAEntry        *pRAEntry = NULL;

    if (Ip6IsIfFwdEnabled (i4RtrAdvtIfIndex) == FALSE)
    {
        return SNMP_FAILURE;
    }
    pRAEntry = Ip6RaTblGetEntry (i4RtrAdvtIfIndex);
    if (pRAEntry == NULL)
    {
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  Ip6GetFirstIndexRtrAdvtTable
 Input       :  pi4RtrAdvtIfIndex -  First RA Table Entry Index
 Output      :  None
 Returns     :  SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
Ip6GetFirstIndexRtrAdvtTable (INT4 *pi4RtrAdvtIfIndex)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = Ip6GetNextRaTblEntry (IP6_ZERO, pi4RtrAdvtIfIndex);
    return (i1RetVal);
}

/****************************************************************************
 Function    :  Ip6GetNextIndexRtrAdvtTable
 Input       :  i4RtrAdvtIfIndex     - Ip6IfIndex
                pi4NextRtrAdvtIfIndex -  Next RA Table Entry Index     
 Output      :  None
 Returns     :  SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
Ip6GetNextIndexRtrAdvtTable (INT4 i4RtrAdvtIfIndex, INT4 *pi4NextRtrAdvtIfIndex)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = Ip6GetNextRaTblEntry (i4RtrAdvtIfIndex, pi4NextRtrAdvtIfIndex);
    return (i1RetVal);
}

/* GET Routine for All Objects  */
/****************************************************************************
 Function    :  Ip6GetRtrAdvtSendAdverts
 Input       :  i4RtrAdvtIfIndex     - Ip6IfIndex
                pi4RtrAdvtSendAdverts - RA Table RA Send Status     
 Output      :  None
 Returns     :  SNMP_FAILURE
****************************************************************************/
INT1
Ip6GetRtrAdvtSendAdverts (INT4 i4RtrAdvtIfIndex, INT4 *pi4RtrAdvtSendAdverts)
{
    tIp6RAEntry        *pRAEntry = NULL;

    if (Ip6IsIfFwdEnabled (i4RtrAdvtIfIndex) == FALSE)
    {
        UNUSED_PARAM (pi4RtrAdvtSendAdverts);
        return SNMP_FAILURE;
    }
    pRAEntry = Ip6RaTblGetEntry (i4RtrAdvtIfIndex);
    if (pRAEntry != NULL)
    {
        *pi4RtrAdvtSendAdverts = (INT4) pRAEntry->u1SendAdverts;
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  Ip6GetRtrAdvtMaxInterval
 Input       :  i4RtrAdvtIfIndex     - Ip6IfIndex
                pu4RtrAdvtMaxInterval - RA Table Max RA Tx Interval 
 Output      :  None
 Returns     :  SNMP_FAILURE
****************************************************************************/
INT1
Ip6GetRtrAdvtMaxInterval (INT4 i4RtrAdvtIfIndex, UINT4 *pu4RtrAdvtMaxInterval)
{
    tIp6RAEntry        *pRAEntry = NULL;

    if (Ip6IsIfFwdEnabled (i4RtrAdvtIfIndex) == FALSE)
    {
        UNUSED_PARAM (pu4RtrAdvtMaxInterval);
        return SNMP_FAILURE;
    }
    pRAEntry = Ip6RaTblGetEntry (i4RtrAdvtIfIndex);
    if (pRAEntry != NULL)
    {
        *pu4RtrAdvtMaxInterval = pRAEntry->u4MaxInterval;
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  Ip6GetRtrAdvtMinInterval
 Input       :  i4RtrAdvtIfIndex     - Ip6IfIndex
                pu4RtrAdvtMinInterval - RA Table Min RA Tx Interval 
 Output      :  None
 Returns     :  SNMP_FAILURE
****************************************************************************/
INT1
Ip6GetRtrAdvtMinInterval (INT4 i4RtrAdvtIfIndex, UINT4 *pu4RtrAdvtMinInterval)
{
    tIp6RAEntry        *pRAEntry = NULL;

    if (Ip6IsIfFwdEnabled (i4RtrAdvtIfIndex) == FALSE)
    {
        UNUSED_PARAM (pu4RtrAdvtMinInterval);
        return SNMP_FAILURE;
    }
    pRAEntry = Ip6RaTblGetEntry (i4RtrAdvtIfIndex);
    if (pRAEntry != NULL)
    {
        *pu4RtrAdvtMinInterval = pRAEntry->u4MinInterval;
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  Ip6GetRtrAdvtMFlag
 Input       :  i4RtrAdvtIfIndex     - Ip6IfIndex
                pi4RtrAdvtMFlag - RA Table M Flag Value 
 Output      :  None
 Returns     :  SNMP_FAILURE
****************************************************************************/
INT1
Ip6GetRtrAdvtMFlag (INT4 i4RtrAdvtIfIndex, INT4 *pi4RtrAdvtMFlag)
{
    tIp6RAEntry        *pRAEntry = NULL;

    if (Ip6IsIfFwdEnabled (i4RtrAdvtIfIndex) == FALSE)
    {
        UNUSED_PARAM (pi4RtrAdvtMFlag);
        return SNMP_FAILURE;
    }
    pRAEntry = Ip6RaTblGetEntry (i4RtrAdvtIfIndex);
    if (pRAEntry != NULL)
    {
        *pi4RtrAdvtMFlag = (INT4) pRAEntry->u1MFlag;
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  Ip6GetRtrAdvtOFlag
 Input       :  i4RtrAdvtIfIndex     - Ip6IfIndex
                pi4RtrAdvtOFlag  - RA Table O Flag Value  
 Output      :  None
 Returns     :  SNMP_FAILURE
****************************************************************************/
INT1
Ip6GetRtrAdvtOFlag (INT4 i4RtrAdvtIfIndex, INT4 *pi4RtrAdvtOFlag)
{
    tIp6RAEntry        *pRAEntry = NULL;

    if (Ip6IsIfFwdEnabled (i4RtrAdvtIfIndex) == FALSE)
    {
        UNUSED_PARAM (pi4RtrAdvtOFlag);
        return SNMP_FAILURE;
    }
    pRAEntry = Ip6RaTblGetEntry (i4RtrAdvtIfIndex);
    if (pRAEntry != NULL)
    {
        *pi4RtrAdvtOFlag = (INT4) pRAEntry->u1OFlag;
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  Ip6GetRtrAdvtLinkMTU
 Input       :  i4RtrAdvtIfIndex     - Ip6IfIndex
                pu4RtrAdvtLinkMTU     - RA Table Link MTU 
 Output      :  None
 Returns     :  SNMP_FAILURE
****************************************************************************/
INT1
Ip6GetRtrAdvtLinkMTU (INT4 i4RtrAdvtIfIndex, UINT4 *pu4RtrAdvtLinkMTU)
{
    tIp6RAEntry        *pRAEntry = NULL;

    if (Ip6IsIfFwdEnabled (i4RtrAdvtIfIndex) == FALSE)
    {
        UNUSED_PARAM (pu4RtrAdvtLinkMTU);
        return SNMP_FAILURE;
    }
    pRAEntry = Ip6RaTblGetEntry (i4RtrAdvtIfIndex);
    if (pRAEntry != NULL)
    {
        *pu4RtrAdvtLinkMTU = pRAEntry->u4LinkMTU;
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  Ip6GetRtrAdvtReachTime
 Input       :  i4RtrAdvtIfIndex    - Ip6IfIndex
                pu4RtrAdvtReachTime - RA Table Reachable Time 
 Output      :  None
 Returns     :  SNMP_FAILURE
****************************************************************************/
INT1
Ip6GetRtrAdvtReachTime (INT4 i4RtrAdvtIfIndex, UINT4 *pu4RtrAdvtReachTime)
{
    tIp6RAEntry        *pRAEntry = NULL;

    if (Ip6IsIfFwdEnabled (i4RtrAdvtIfIndex) == FALSE)
    {
        UNUSED_PARAM (pu4RtrAdvtReachTime);
        return SNMP_FAILURE;
    }
    pRAEntry = Ip6RaTblGetEntry (i4RtrAdvtIfIndex);
    if (pRAEntry != NULL)
    {
        *pu4RtrAdvtReachTime = pRAEntry->u4ReachableTime;
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  Ip6GetRtrAdvtTxTime
 Input       :  i4RtrAdvtIfIndex    - Ip6IfIndex
                u4RtrAdvtTxTime - RA Table RetTransmit Time 
 Output      :  None
 Returns     :  SNMP_FAILURE
****************************************************************************/
INT1
Ip6GetRtrAdvtTxTime (INT4 i4RtrAdvtIfIndex, UINT4 *pu4RtrAdvtTxTime)
{
    tIp6RAEntry        *pRAEntry = NULL;

    if (Ip6IsIfFwdEnabled (i4RtrAdvtIfIndex) == FALSE)
    {
        UNUSED_PARAM (pu4RtrAdvtTxTime);
        return SNMP_FAILURE;
    }
    pRAEntry = Ip6RaTblGetEntry (i4RtrAdvtIfIndex);
    if (pRAEntry != NULL)
    {
        *pu4RtrAdvtTxTime = pRAEntry->u4RetransmitTime;
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  Ip6GetRtrAdvtCurHopLimit
 Input       :  i4RtrAdvtIfIndex    - Ip6IfIndex
                pu4RtrAdvtCurHopLimit - RA Table HopLimit 
 Output      :  None
 Returns     :  SNMP_FAILURE
****************************************************************************/
INT1
Ip6GetRtrAdvtCurHopLimit (INT4 i4RtrAdvtIfIndex, UINT4 *pu4RtrAdvtCurHopLimit)
{
    tIp6RAEntry        *pRAEntry = NULL;

    if (Ip6IsIfFwdEnabled (i4RtrAdvtIfIndex) == FALSE)
    {
        UNUSED_PARAM (pu4RtrAdvtCurHopLimit);
        return SNMP_FAILURE;
    }
    pRAEntry = Ip6RaTblGetEntry (i4RtrAdvtIfIndex);
    if (pRAEntry != NULL)
    {
        *pu4RtrAdvtCurHopLimit = (UINT4) pRAEntry->u1CurHopLimit;
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/****************************************************************************
Function    :  Ip6GetRtrAdvtDefLifeTime
Input       :  i4RtrAdvtIfIndex    - Ip6IfIndex
                pu4RtrAdvtDefLifeTime - RA Table Default Life Time 
 Output      :  None
 Returns     :  SNMP_FAILURE
****************************************************************************/
INT1
Ip6GetRtrAdvtDefLifeTime (INT4 i4RtrAdvtIfIndex, UINT4 *pu4RtrAdvtDefLifeTime)
{
    tIp6RAEntry        *pRAEntry = NULL;

    if (Ip6IsIfFwdEnabled (i4RtrAdvtIfIndex) == FALSE)
    {
        UNUSED_PARAM (pu4RtrAdvtDefLifeTime);
        return SNMP_FAILURE;
    }
    pRAEntry = Ip6RaTblGetEntry (i4RtrAdvtIfIndex);
    if (pRAEntry != NULL)
    {
        *pu4RtrAdvtDefLifeTime = (UINT4) pRAEntry->u2DefLifetime;
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  Ip6GetRtrAdvtRowStatus
 Input       :  i4RtrAdvtIfIndex    - Ip6IfIndex
                pi4RtrAdvtRowStatus  - RA Table RowStatus Value
 Output      :  None
 Returns     :  SNMP_FAILURE
****************************************************************************/
INT1
Ip6GetRtrAdvtRowStatus (INT4 i4RtrAdvtIfIndex, INT4 *pi4RtrAdvtRowStatus)
{
    tIp6RAEntry        *pRAEntry = NULL;

    if (Ip6IsIfFwdEnabled (i4RtrAdvtIfIndex) == FALSE)
    {
        UNUSED_PARAM (pi4RtrAdvtRowStatus);
        return SNMP_FAILURE;
    }
    pRAEntry = Ip6RaTblGetEntry (i4RtrAdvtIfIndex);
    if (pRAEntry != NULL)
    {
        *pi4RtrAdvtRowStatus = (INT4) pRAEntry->u1RowStatus;
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/* SET Routine for All Objects  */
/****************************************************************************
 Function    :  Ip6SetRtrAdvtSendAdverts
 Input       :  i4RtrAdvtIfIndex     - Ip6IfIndex
                i4RtrAdvtSendAdverts - RA Table RA Send Status     
 Output      :  None
 Returns     :  SNMP_FAILURE
****************************************************************************/
INT1
Ip6SetRtrAdvtSendAdverts (INT4 i4RtrAdvtIfIndex, INT4 i4RtrAdvtSendAdverts)
{
    tIp6RAEntry        *pRAEntry = NULL;
    tIp6If             *pIf6 = NULL;

    if (Ip6IsIfFwdEnabled (i4RtrAdvtIfIndex) == FALSE)
    {
        UNUSED_PARAM (i4RtrAdvtSendAdverts);
        return SNMP_FAILURE;
    }
    pRAEntry = Ip6RaTblGetEntry (i4RtrAdvtIfIndex);
    if (pRAEntry != NULL)
    {
        if (pRAEntry->u1SendAdverts == (UINT1) i4RtrAdvtSendAdverts)
        {
            return (SNMP_SUCCESS);
        }

        /* Call Send/ Stop RA */
        pIf6 = Ip6ifGetEntry ((UINT4) i4RtrAdvtIfIndex);
        if (pIf6 == NULL)
        {
            return (SNMP_FAILURE);
        }

        if (i4RtrAdvtSendAdverts == IPVX_TRUTH_VALUE_TRUE)
        {
            pRAEntry->u1SendAdverts = IPVX_TRUTH_VALUE_TRUE;

            if ((gIp6GblInfo.apIp6If[i4RtrAdvtIfIndex]->u1OperStatus
                 == OPER_UP) && (pRAEntry->u1RowStatus == ACTIVE))
            {
                /* Indicate to ND of the RA Status */
                Nd6ActOnRaCnf (pIf6, IP6_IF_RA_ADV);
            }
        }
        else if (i4RtrAdvtSendAdverts == IPVX_TRUTH_VALUE_FALSE)
        {
            if ((gIp6GblInfo.apIp6If[i4RtrAdvtIfIndex]->u1OperStatus
                 == OPER_UP) && (pRAEntry->u1RowStatus == ACTIVE))
            {
                /* Indicate to ND of the RA Status */
                Nd6ActOnRaCnf (pIf6, IP6_IF_NO_RA_ADV);
            }
            pRAEntry->u1SendAdverts = IPVX_TRUTH_VALUE_FALSE;
        }

        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  Ip6SetRtrAdvtMaxInterval
 Input       :  i4RtrAdvtIfIndex     - Ip6IfIndex
                u4RtrAdvtMaxInterval - RA Table Max RA Tx Interval 
 Output      :  None
 Returns     :  SNMP_FAILURE
****************************************************************************/
INT1
Ip6SetRtrAdvtMaxInterval (INT4 i4RtrAdvtIfIndex, UINT4 u4RtrAdvtMaxInterval)
{
    tIp6RAEntry        *pRAEntry = NULL;
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4RtrAdvtIfIndex);

    if (Ip6IsIfFwdEnabled (i4RtrAdvtIfIndex) == FALSE)
    {
        UNUSED_PARAM (u4RtrAdvtMaxInterval);
        return SNMP_FAILURE;
    }
    pRAEntry = Ip6RaTblGetEntry (i4RtrAdvtIfIndex);
    if (pRAEntry != NULL)
    {
        pRAEntry->u4MaxInterval = u4RtrAdvtMaxInterval;
        if ((pIf6 != NULL)
            && (Ip6CheckRAStatus ((INT4) (i4RtrAdvtIfIndex))) == TRUE)
        {
            /* Indicate to ND RA */
            Nd6ActOnRaCnf (pIf6, IP6_IF_RA_ADV);
        }

        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  Ip6SetRtrAdvtMinInterval
 Input       :  i4RtrAdvtIfIndex     - Ip6IfIndex
                u4RtrAdvtMinInterval - RA Table Min RA Tx Interval 
 Output      :  None
 Returns     :  SNMP_FAILURE
****************************************************************************/
INT1
Ip6SetRtrAdvtMinInterval (INT4 i4RtrAdvtIfIndex, UINT4 u4RtrAdvtMinInterval)
{
    tIp6RAEntry        *pRAEntry = NULL;

    if (Ip6IsIfFwdEnabled (i4RtrAdvtIfIndex) == FALSE)
    {
        UNUSED_PARAM (u4RtrAdvtMinInterval);
        return SNMP_FAILURE;
    }
    pRAEntry = Ip6RaTblGetEntry (i4RtrAdvtIfIndex);
    if (pRAEntry != NULL)
    {
        pRAEntry->u4MinInterval = u4RtrAdvtMinInterval;
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  Ip6SetRtrAdvtMFlag
 Input       :  i4RtrAdvtIfIndex     - Ip6IfIndex
                i4RtrAdvtMFlag - RA Table M Flag Value 
 Output      :  None
 Returns     :  SNMP_FAILURE
****************************************************************************/
INT1
Ip6SetRtrAdvtMFlag (INT4 i4RtrAdvtIfIndex, INT4 i4RtrAdvtMFlag)
{
    tIp6RAEntry        *pRAEntry = NULL;

    if (Ip6IsIfFwdEnabled (i4RtrAdvtIfIndex) == FALSE)
    {
        UNUSED_PARAM (i4RtrAdvtMFlag);
        return SNMP_FAILURE;
    }
    pRAEntry = Ip6RaTblGetEntry (i4RtrAdvtIfIndex);
    if (pRAEntry != NULL)
    {
        pRAEntry->u1MFlag = (UINT1) i4RtrAdvtMFlag;
        return (SNMP_SUCCESS);
    }

    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  Ip6SetRtrAdvtOFlag
 Input       :  i4RtrAdvtIfIndex     - Ip6IfIndex
                i4RtrAdvtOFlag  - RA Table O Flag Value  
 Output      :  None
 Returns     :  SNMP_FAILURE
****************************************************************************/
INT1
Ip6SetRtrAdvtOFlag (INT4 i4RtrAdvtIfIndex, INT4 i4RtrAdvtOFlag)
{
    tIp6RAEntry        *pRAEntry = NULL;

    if (Ip6IsIfFwdEnabled (i4RtrAdvtIfIndex) == FALSE)
    {
        UNUSED_PARAM (i4RtrAdvtOFlag);
        return SNMP_FAILURE;
    }
    pRAEntry = Ip6RaTblGetEntry (i4RtrAdvtIfIndex);
    if (pRAEntry != NULL)
    {
        pRAEntry->u1OFlag = (UINT1) i4RtrAdvtOFlag;
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  Ip6SetRtrAdvtLinkMTU
 Input       :  i4RtrAdvtIfIndex     - Ip6IfIndex
                u4RtrAdvtLinkMTU     - RA Table Link MTU 
 Output      :  None
 Returns     :  SNMP_FAILURE
****************************************************************************/
INT1
Ip6SetRtrAdvtLinkMTU (INT4 i4RtrAdvtIfIndex, UINT4 u4RtrAdvtLinkMTU)
{
    tIp6RAEntry        *pRAEntry = NULL;

    if (Ip6IsIfFwdEnabled (i4RtrAdvtIfIndex) == FALSE)
    {
        UNUSED_PARAM (u4RtrAdvtLinkMTU);
        return SNMP_FAILURE;
    }
    pRAEntry = Ip6RaTblGetEntry (i4RtrAdvtIfIndex);
    if (pRAEntry != NULL)
    {
        pRAEntry->u4LinkMTU = u4RtrAdvtLinkMTU;
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  Ip6SetRtrAdvtReachTime
 Input       :  i4RtrAdvtIfIndex    - Ip6IfIndex
                u4RtrAdvtReachTime - RA Table Reachable Time 
 Output      :  None
 Returns     :  SNMP_FAILURE
****************************************************************************/
INT1
Ip6SetRtrAdvtReachTime (INT4 i4RtrAdvtIfIndex, UINT4 u4RtrAdvtReachTime)
{
    tIp6RAEntry        *pRAEntry = NULL;

    if (Ip6IsIfFwdEnabled (i4RtrAdvtIfIndex) == FALSE)
    {
        UNUSED_PARAM (u4RtrAdvtReachTime);
        return SNMP_FAILURE;
    }
    pRAEntry = Ip6RaTblGetEntry (i4RtrAdvtIfIndex);
    if (pRAEntry != NULL)
    {
        pRAEntry->u4ReachableTime = u4RtrAdvtReachTime;
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  Ip6SetRtrAdvtTxTime
 Input       :  i4RtrAdvtIfIndex    - Ip6IfIndex
                u4RtrAdvtTxTime - RA Table RetTransmit Time 
 Output      :  None
 Returns     :  SNMP_FAILURE
****************************************************************************/
INT1
Ip6SetRtrAdvtTxTime (INT4 i4RtrAdvtIfIndex, UINT4 u4RtrAdvtTxTime)
{
    tIp6RAEntry        *pRAEntry = NULL;

    if (Ip6IsIfFwdEnabled (i4RtrAdvtIfIndex) == FALSE)
    {
        UNUSED_PARAM (u4RtrAdvtTxTime);
        return SNMP_FAILURE;
    }
    pRAEntry = Ip6RaTblGetEntry (i4RtrAdvtIfIndex);
    if (pRAEntry != NULL)
    {
        pRAEntry->u4RetransmitTime = u4RtrAdvtTxTime;
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  Ip6SetRtrAdvtCurHopLimit
 Input       :  i4RtrAdvtIfIndex    - Ip6IfIndex
                u4RtrAdvtCurHopLimit - RA Table HopLimit 
 Output      :  None
 Returns     :  SNMP_FAILURE
****************************************************************************/
INT1
Ip6SetRtrAdvtCurHopLimit (INT4 i4RtrAdvtIfIndex, UINT4 u4RtrAdvtCurHopLimit)
{
    tIp6RAEntry        *pRAEntry = NULL;

    if (Ip6IsIfFwdEnabled (i4RtrAdvtIfIndex) == FALSE)
    {
        UNUSED_PARAM (u4RtrAdvtCurHopLimit);
        return SNMP_FAILURE;
    }
    pRAEntry = Ip6RaTblGetEntry (i4RtrAdvtIfIndex);
    if (pRAEntry != NULL)
    {
        pRAEntry->u1CurHopLimit = (UINT1) u4RtrAdvtCurHopLimit;
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  Ip6SetRtrAdvtDefLifeTime
 Input       :  i4RtrAdvtIfIndex    - Ip6IfIndex
                u4RtrAdvtDefLifeTime - RA Table Default Life Time 
 Output      :  None
 Returns     :  SNMP_FAILURE
****************************************************************************/
INT1
Ip6SetRtrAdvtDefLifeTime (INT4 i4RtrAdvtIfIndex, UINT4 u4RtrAdvtDefLifeTime)
{
    tIp6RAEntry        *pRAEntry = NULL;

    if (Ip6IsIfFwdEnabled (i4RtrAdvtIfIndex) == FALSE)
    {
        UNUSED_PARAM (u4RtrAdvtDefLifeTime);
        return SNMP_FAILURE;
    }
    pRAEntry = Ip6RaTblGetEntry (i4RtrAdvtIfIndex);
    if (pRAEntry != NULL)
    {
        pRAEntry->u2DefLifetime = (UINT2) u4RtrAdvtDefLifeTime;
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  Ip6SetRtrAdvtRowStatus
 Input       :  i4RtrAdvtIfIndex    - Ip6IfIndex
                i4RtrAdvtRowStatus  - RA Table RowStatus Value
 Output      :  None
 Returns     :  SNMP_FAILURE
****************************************************************************/
INT1
Ip6SetRtrAdvtRowStatus (INT4 i4RtrAdvtIfIndex, INT4 i4RtrAdvtRowStatus)
{
    tIp6RAEntry        *pRAEntry = NULL;
    tIp6If             *pIf6 = NULL;

    if (Ip6IsIfFwdEnabled (i4RtrAdvtIfIndex) == FALSE)
    {
        UNUSED_PARAM (i4RtrAdvtRowStatus);
        return SNMP_FAILURE;
    }
    pRAEntry = Ip6RaTblGetEntry (i4RtrAdvtIfIndex);
    if (pRAEntry != NULL)
    {
        /* Entry found in the Table */
        if (pRAEntry->u1RowStatus == i4RtrAdvtRowStatus)
        {
            return (SNMP_SUCCESS);
        }
    }
    else if (i4RtrAdvtRowStatus != CREATE_AND_WAIT)
    {
        return (SNMP_FAILURE);
    }

    if ((i4RtrAdvtIfIndex <= IP6_ZERO) ||
        (i4RtrAdvtIfIndex > IP6_MAX_LOGICAL_IF_INDEX))
    {
        return (SNMP_FAILURE);
    }

    switch (i4RtrAdvtRowStatus)
    {
            /* Modify an Entry */
        case ACTIVE:
            pRAEntry->u1RowStatus = ACTIVE;
            /* Call the RA Send if SendFlag also TRUE */
            pIf6 = Ip6ifGetEntry ((UINT4) i4RtrAdvtIfIndex);
            if (pIf6 == NULL)
            {
                return (SNMP_FAILURE);
            }
            if ((pIf6->u1OperStatus == OPER_UP) &&
                (pRAEntry->u1SendAdverts == IPVX_TRUTH_VALUE_TRUE))
            {
                /* Indicate to ND of the RA Status */
                Nd6ActOnRaCnf (pIf6, IP6_IF_RA_ADV);
            }
            break;

        case NOT_IN_SERVICE:

            /* Call the RA Stop */
            pIf6 = Ip6ifGetEntry ((UINT4) i4RtrAdvtIfIndex);
            if (pIf6 == NULL)
            {
                return (SNMP_FAILURE);
            }
            if ((pIf6->u1OperStatus == OPER_UP) &&
                (pRAEntry->u1RowStatus == ACTIVE) &&
                (pRAEntry->u1SendAdverts == IPVX_TRUTH_VALUE_TRUE))
            {
                /* Indicate to ND of the RA Status */
                Nd6ActOnRaCnf (pIf6, IP6_IF_NO_RA_ADV);
            }
            pRAEntry->u1RowStatus = NOT_IN_SERVICE;
            break;

        case DESTROY:
            /* Call the RA Stop */
            pIf6 = Ip6ifGetEntry ((UINT4) i4RtrAdvtIfIndex);
            if (pIf6 == NULL)
            {
                return (SNMP_FAILURE);
            }
            if ((pIf6->u1OperStatus == OPER_UP) &&
                (pRAEntry->u1RowStatus == ACTIVE) &&
                (pRAEntry->u1SendAdverts == IPVX_TRUTH_VALUE_TRUE))
            {
                /* Indicate to ND of the RA Status */
                Nd6ActOnRaCnf (pIf6, IP6_IF_NO_RA_ADV);
            }
            /* Set the RA Entry DESTROY */
            pRAEntry->u1RowStatus = DESTROY;
            Ip6DeleteRaTblEntry (i4RtrAdvtIfIndex);
            break;

        case CREATE_AND_WAIT:
            /* Create a Entry and Init with Default 
             * Set RA Entry as NOT_IN_SERVICE */
            pRAEntry = Ip6CreateRaTblEntry (i4RtrAdvtIfIndex);
            if (pRAEntry == NULL)
            {
                return (SNMP_FAILURE);
            }
            pRAEntry->u1RowStatus = NOT_IN_SERVICE;
            break;

        case CREATE_AND_GO:
            return (SNMP_FAILURE);

        default:
            return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/* TEST Routines for All Objects  */
/****************************************************************************
 Function    :  Ip6TestRtrAdvtSendAdverts
 Input       :  pu4ErrorCode         - Error Code 
                i4RtrAdvtIfIndex     - Ip6IfIndex
                i4RtrAdvtSendAdverts - RA Table RA Send Status     
 Output      :  None
 Returns     :  SNMP_FAILURE
****************************************************************************/
INT1
Ip6TestRtrAdvtSendAdverts (UINT4 *pu4ErrorCode, INT4 i4RtrAdvtIfIndex,
                           INT4 i4RtrAdvtSendAdverts)
{
    tIp6RAEntry        *pRAEntry = NULL;

    if (Ip6IsIfFwdEnabled (i4RtrAdvtIfIndex) == FALSE)
    {
        UNUSED_PARAM (pu4ErrorCode);
        UNUSED_PARAM (i4RtrAdvtSendAdverts);
        return SNMP_FAILURE;
    }
    /* Get RA Table Info */
    pRAEntry = Ip6RaTblGetEntry (i4RtrAdvtIfIndex);
    if (pRAEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    /* Should be TRUE/FAILS */
    if ((i4RtrAdvtSendAdverts != IPVX_TRUTH_VALUE_TRUE) &&
        (i4RtrAdvtSendAdverts != IPVX_TRUTH_VALUE_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_INVALID_RT_ADV_STATUS);
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  Ip6TestRtrAdvtMaxInterval
 Input       :  pu4ErrorCode         - Error Code 
                i4RtrAdvtIfIndex     - Ip6IfIndex
                u4RtrAdvtMaxInterval - RA Table Max RA Tx Interval 
 Output      :  None
 Returns     :  SNMP_FAILURE
****************************************************************************/
INT1
Ip6TestRtrAdvtMaxInterval (UINT4 *pu4ErrorCode, INT4 i4RtrAdvtIfIndex,
                           UINT4 u4RtrAdvtMaxInterval)
{
    tIp6RAEntry        *pRAEntry = NULL;

    if (Ip6IsIfFwdEnabled (i4RtrAdvtIfIndex) == FALSE)
    {
        UNUSED_PARAM (pu4ErrorCode);
        UNUSED_PARAM (u4RtrAdvtMaxInterval);
        return SNMP_FAILURE;
    }
    /* Get RA Table Info */
    pRAEntry = Ip6RaTblGetEntry (i4RtrAdvtIfIndex);
    if (pRAEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }
    /* Should be 4..1800 Sec. */
    if ((u4RtrAdvtMaxInterval < IP6_IF_MAX_RA_MIN_TIME) ||
        (u4RtrAdvtMaxInterval > IP6_IF_MAX_RA_MAX_TIME))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_INVALID_RA_INTERVAL);
        return (SNMP_FAILURE);
    }

    /* If Default Route LifeTime is set, then this value should be
     * <= LifeTime value. */
    if ((pRAEntry->u2DefLifetime != IP6_ZERO) &&
        ((u4RtrAdvtMaxInterval > (pRAEntry->u2DefLifetime)) ||
         (u4RtrAdvtMaxInterval <
          ((IP6_FOUR * pRAEntry->u4MinInterval) / IP6_THREE))))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        if (u4RtrAdvtMaxInterval <
            ((IP6_FOUR * pRAEntry->u4MinInterval) / IP6_THREE))
        {
            CLI_SET_ERR (CLI_IP6_INVALID_MIN_RA_INTERVAL);
        }
        else
        {
            CLI_SET_ERR (CLI_IP6_INVALID_RA_INTERVAL);
        }
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  Ip6TestRtrAdvtMinInterval
 Input       :  pu4ErrorCode         - Error Code 
                i4RtrAdvtIfIndex     - Ip6IfIndex
                u4RtrAdvtMinInterval - RA Table Min RA Tx Interval 
 Output      :  None
 Returns     :  SNMP_FAILURE
****************************************************************************/
INT1
Ip6TestRtrAdvtMinInterval (UINT4 *pu4ErrorCode, INT4 i4RtrAdvtIfIndex,
                           UINT4 u4RtrAdvtMinInterval)
{
    tIp6RAEntry        *pRAEntry = NULL;

    if (Ip6IsIfFwdEnabled (i4RtrAdvtIfIndex) == FALSE)
    {
        UNUSED_PARAM (pu4ErrorCode);
        UNUSED_PARAM (u4RtrAdvtMinInterval);
        return SNMP_FAILURE;
    }
    /* Get RA Table Info */
    pRAEntry = Ip6RaTblGetEntry (i4RtrAdvtIfIndex);
    if (pRAEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }
    /* Should be =< 3, or  (3/4 of MaxRouterAdvTime)) Sec. */
    if ((u4RtrAdvtMinInterval < IP6_IF_MIN_RA_MIN_TIME) ||
        (u4RtrAdvtMinInterval >
         ((IP6_THREE * (pRAEntry->u4MaxInterval)) / IP6_FOUR)))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_INVALID_RA_INTERVAL);
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  Ip6TestRtrAdvtMFlag
 Input       :  pu4ErrorCode         - Error Code 
                i4RtrAdvtIfIndex     - Ip6IfIndex
                i4RtrAdvtMFlag - RA Table M Flag Value 
 Output      :  None
 Returns     :  SNMP_FAILURE
****************************************************************************/
INT1
Ip6TestRtrAdvtMFlag (UINT4 *pu4ErrorCode, INT4 i4RtrAdvtIfIndex,
                     INT4 i4RtrAdvtMFlag)
{
    tIp6RAEntry        *pRAEntry = NULL;

    if (Ip6IsIfFwdEnabled (i4RtrAdvtIfIndex) == FALSE)
    {
        UNUSED_PARAM (pu4ErrorCode);
        UNUSED_PARAM (i4RtrAdvtMFlag);
        return SNMP_FAILURE;
    }
    /* Get RA Table Info */
    pRAEntry = Ip6RaTblGetEntry (i4RtrAdvtIfIndex);
    if (pRAEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    /* Range Specified  TRUE/FAILS */
    if ((i4RtrAdvtMFlag != IPVX_TRUTH_VALUE_TRUE) &&
        (i4RtrAdvtMFlag != IPVX_TRUTH_VALUE_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_INVALID_RT_ADV_FLAGS);
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  Ip6TestRtrAdvtOFlag
 Input       :  pu4ErrorCode         - Error Code 
                i4RtrAdvtIfIndex     - Ip6IfIndex
                i4RtrAdvtOFlag  - RA Table O Flag Value  
 Output      :  None
 Returns     :  SNMP_FAILURE
****************************************************************************/
INT1
Ip6TestRtrAdvtOFlag (UINT4 *pu4ErrorCode, INT4 i4RtrAdvtIfIndex,
                     INT4 i4RtrAdvtOFlag)
{
    tIp6RAEntry        *pRAEntry = NULL;

    if (Ip6IsIfFwdEnabled (i4RtrAdvtIfIndex) == FALSE)
    {
        UNUSED_PARAM (pu4ErrorCode);
        UNUSED_PARAM (i4RtrAdvtOFlag);
        return SNMP_FAILURE;
    }
    /* Get RA Table Info */
    pRAEntry = Ip6RaTblGetEntry (i4RtrAdvtIfIndex);
    if (pRAEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    /* Range Specified  TRUE/FAILS */
    if ((i4RtrAdvtOFlag != IPVX_TRUTH_VALUE_TRUE) &&
        (i4RtrAdvtOFlag != IPVX_TRUTH_VALUE_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_INVALID_RT_ADV_FLAGS);
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  Ip6TestRtrAdvtLinkMTU
 Input       :  pu4ErrorCode         - Error Code 
                i4RtrAdvtIfIndex     - Ip6IfIndex
                u4RtrAdvtLinkMTU     - RA Table Link MTU 
 Output      :  None
 Returns     :  SNMP_FAILURE
****************************************************************************/
INT1
Ip6TestRtrAdvtLinkMTU (UINT4 *pu4ErrorCode, INT4 i4RtrAdvtIfIndex,
                       UINT4 u4RtrAdvtLinkMTU)
{
    tIp6RAEntry        *pRAEntry = NULL;

    if (Ip6IsIfFwdEnabled (i4RtrAdvtIfIndex) == FALSE)
    {
        UNUSED_PARAM (pu4ErrorCode);
        UNUSED_PARAM (u4RtrAdvtLinkMTU);
        return SNMP_FAILURE;
    }
    /* Get RA Table Info */
    pRAEntry = Ip6RaTblGetEntry (i4RtrAdvtIfIndex);
    if (pRAEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    /* No Range Specified */
    if ((u4RtrAdvtLinkMTU != IP6_ZERO) &&
        ((u4RtrAdvtLinkMTU < IP6_RA_MIN_LINK_MTU) ||
         (u4RtrAdvtLinkMTU > IP6_RA_MAX_LINK_MTU)))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_INVALID_RA_LINK_MTU);
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  Ip6TestRtrAdvtReachTime
 Input       :  pu4ErrorCode        - Error Code 
                i4RtrAdvtIfIndex    - Ip6IfIndex
                u4RtrAdvtReachTime - RA Table Reachable Time 
 Output      :  None
 Returns     :  SNMP_FAILURE
****************************************************************************/
INT1
Ip6TestRtrAdvtReachTime (UINT4 *pu4ErrorCode, INT4 i4RtrAdvtIfIndex,
                         UINT4 u4RtrAdvtReachTime)
{
    tIp6RAEntry        *pRAEntry = NULL;

    if (Ip6IsIfFwdEnabled (i4RtrAdvtIfIndex) == FALSE)
    {
        UNUSED_PARAM (pu4ErrorCode);
        UNUSED_PARAM (u4RtrAdvtReachTime);
        return SNMP_FAILURE;
    }
    /* Get RA Table Info */
    pRAEntry = Ip6RaTblGetEntry (i4RtrAdvtIfIndex);
    if (pRAEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    /* Range Specified 0..3600000 MSec. */
    if (u4RtrAdvtReachTime > IP6_IF_MAX_REACHTIME)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_INVALID_RA_REACHTIME);
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  Ip6TestRtrAdvtTxTime
 Input       :  pu4ErrorCode        - Error Code 
                i4RtrAdvtIfIndex    - Ip6IfIndex
                u4RtrAdvtTxTime - RA Table RetTransmit Time 
 Output      :  None
 Returns     :  SNMP_FAILURE
****************************************************************************/
INT1
Ip6TestRtrAdvtTxTime (UINT4 *pu4ErrorCode, INT4 i4RtrAdvtIfIndex,
                      UINT4 u4RtrAdvtTxTime)
{
    tIp6RAEntry        *pRAEntry = NULL;

    if (Ip6IsIfFwdEnabled (i4RtrAdvtIfIndex) == FALSE)
    {
        UNUSED_PARAM (pu4ErrorCode);
        UNUSED_PARAM (u4RtrAdvtTxTime);
        return SNMP_FAILURE;
    }
    /* Get RA Table Info */
    pRAEntry = Ip6RaTblGetEntry (i4RtrAdvtIfIndex);
    if (pRAEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    /* No Range Specified */
    if ((u4RtrAdvtTxTime != IP6_RA_DEF_RETTIME) &&
        ((u4RtrAdvtTxTime < IP6_IF_MIN_RETTIME) ||
         (u4RtrAdvtTxTime > IP6_IF_MAX_RETTIME)))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_INVALID_RA_RETTIME);
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  Ip6TestRtrAdvtCurHopLimit
 Input       :  pu4ErrorCode        - Error Code 
                i4RtrAdvtIfIndex    - Ip6IfIndex
                u4RtrAdvtCurHopLimit - RA Table HopLimit 
 Output      :  None
 Returns     :  SNMP_FAILURE
****************************************************************************/
INT1
Ip6TestRtrAdvtCurHopLimit (UINT4 *pu4ErrorCode, INT4 i4RtrAdvtIfIndex,
                           UINT4 u4RtrAdvtCurHopLimit)
{
    tIp6RAEntry        *pRAEntry = NULL;

    if (Ip6IsIfFwdEnabled (i4RtrAdvtIfIndex) == FALSE)
    {
        UNUSED_PARAM (pu4ErrorCode);
        UNUSED_PARAM (u4RtrAdvtCurHopLimit);
        return SNMP_FAILURE;
    }
    /* Get RA Table Info */
    pRAEntry = Ip6RaTblGetEntry (i4RtrAdvtIfIndex);
    if (pRAEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    /* Should be 0..255  */
    if (u4RtrAdvtCurHopLimit > IP6_RA_MAX_HOPLMT)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_INVALID_HOPLIMIT);
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    : Ip6TestRtrAdvtDefLifeTime 
 Input       :  pu4ErrorCode        - Error Code 
                i4RtrAdvtIfIndex    - Ip6IfIndex
                u4RtrAdvtDefLifeTime - RA Table Default Life Time 
 Output      :  None
 Returns     :  SNMP_FAILURE
****************************************************************************/
INT1
Ip6TestRtrAdvtDefLifeTime (UINT4 *pu4ErrorCode, INT4 i4RtrAdvtIfIndex,
                           UINT4 u4RtrAdvtDefLifeTime)
{
    tIp6RAEntry        *pRAEntry = NULL;

    if (Ip6IsIfFwdEnabled (i4RtrAdvtIfIndex) == FALSE)
    {
        UNUSED_PARAM (pu4ErrorCode);
        UNUSED_PARAM (u4RtrAdvtDefLifeTime);
        return SNMP_FAILURE;
    }
    /* Get RA Table Info */
    pRAEntry = Ip6RaTblGetEntry (i4RtrAdvtIfIndex);
    if (pRAEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }
    /* Should be 0, or (>= MAX_RA_TIME && <= MAX_DEFTIME (9000)) Sec. */
    if (!((u4RtrAdvtDefLifeTime == IP6_ZERO) ||
          ((u4RtrAdvtDefLifeTime >= pRAEntry->u4MaxInterval) &&
           (u4RtrAdvtDefLifeTime <= IP6_IF_MAX_DEFTIME))))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_INVALID_RA_LIFETIME);
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  Ip6TestRtrAdvtRowStatus
 Input       :  pu4ErrorCode        - Error Code 
                i4RtrAdvtIfIndex    - Ip6IfIndex
                i4RtrAdvtRowStatus  - RA Table RowStatus Value
 Output      :  None
 Returns     :  SNMP_FAILURE
****************************************************************************/
INT1
Ip6TestRtrAdvtRowStatus (UINT4 *pu4ErrorCode, INT4 i4RtrAdvtIfIndex,
                         INT4 i4RtrAdvtRowStatus)
{
    tIp6RAEntry        *pRAEntry = NULL;
    tIp6If             *pIf6 = NULL;

    if (Ip6IsIfFwdEnabled (i4RtrAdvtIfIndex) == FALSE)
    {
        UNUSED_PARAM (pu4ErrorCode);
        UNUSED_PARAM (i4RtrAdvtRowStatus);
        return SNMP_FAILURE;
    }

    pIf6 = Ip6ifGetEntry (i4RtrAdvtIfIndex);

    if ((pIf6 == NULL) || ((pIf6->u1IfType != IP6_ENET_INTERFACE_TYPE) &&
#ifdef TUNNEL_WANTED
                           (pIf6->u1IfType != IP6_TUNNEL_INTERFACE_TYPE) &&
#endif /* TUNNEL_WANTED */
                           (pIf6->u1IfType != IP6_PSEUDO_WIRE_INTERFACE_TYPE) &&
                           (pIf6->u1IfType != IP6_L3VLAN_INTERFACE_TYPE) &&
                           (pIf6->u1IfType != IP6_LAGG_INTERFACE_TYPE) &&
                           (pIf6->u1IfType != IP6_LOOPBACK_INTERFACE_TYPE) &&
                           (pIf6->u1IfType != IP6_L3SUB_INTF_TYPE)))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (SNMP_FAILURE);
    }

    /* Get RA Table Info */
    pRAEntry = Ip6RaTblGetEntry (i4RtrAdvtIfIndex);
    if (pRAEntry == NULL)
    {
        /* Entry Not Found In The Table */
        switch (i4RtrAdvtRowStatus)
        {
            case ACTIVE:
            case NOT_IN_SERVICE:
            case CREATE_AND_GO:
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return (SNMP_FAILURE);

            case DESTROY:
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return (SNMP_FAILURE);

            case CREATE_AND_WAIT:
                break;

            default:
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return (SNMP_FAILURE);
        }
    }
    else
    {
        /* Entry Found In The Table */
        switch (i4RtrAdvtRowStatus)
        {
            case ACTIVE:
            case NOT_IN_SERVICE:
            case DESTROY:
                break;

            case CREATE_AND_WAIT:
            case CREATE_AND_GO:
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_IP6_INCONST_RA_ROWSTATUS);
                return (SNMP_FAILURE);

            default:
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return (SNMP_FAILURE);
        }                        /* End of switch */
    }                            /* End of if */

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  Ip6RaTblGetEntry       
 Description :  This function return the RA Entry form the RA Table for a 
                given RAIfIndex if Entry Created otherwise NULL.
 Input       :  i4Index             - Ip6RAIfIndex 
 Output      :  None
 Returns     :  Ptr to tIp6RAEntry or NULL.
****************************************************************************/
tIp6RAEntry        *
Ip6RaTblGetEntry (INT4 i4Index)
{
    tIp6RAEntry        *pRAEntry = NULL;

    if ((i4Index < IP6_ZERO) || (i4Index > IP6_MAX_LOGICAL_IF_INDEX))
    {
        return (NULL);
    }

    pRAEntry = gIp6GblInfo.apIp6RaTbl[i4Index];
    if (pRAEntry == NULL)
    {
        return (NULL);
    }
    if (pRAEntry->u1RowStatus != IP6_RA_INVALID)
    {
        return (pRAEntry);
    }

    return (NULL);
}

/****************************************************************************
 Function    :  Ip6GetNextRaTblEntry   
 Description :  This function return the Next RA Entry form the RA Table for a 
                given RAIfIndex.
 Input       :  i4RaIfIndex         - Ip6RAIfIndex 
             :  pi4NextRaIfIndex    - NextIp6RAIfIndex 
 Output      :  None
 Returns     :  SNMP_SUCCESS/SNMP_FAILURE.
****************************************************************************/
INT1
Ip6GetNextRaTblEntry (INT4 i4RaIfIndex, INT4 *pi4NextRaIfIndex)
{
    INT4                i4Index = i4RaIfIndex + IP6_ONE;

    if ((i4RaIfIndex < IP6_ZERO) || (i4Index < IP6_ZERO))
    {
        return (SNMP_FAILURE);
    }

    IP6_IF_SCAN (i4Index, (INT4) IP6_MAX_LOGICAL_IF_INDEX)
    {
        if ((Ip6RaTblGetEntry (i4Index) != NULL) &&
            (Ip6IsIfFwdEnabled (i4Index) == TRUE))
        {
            *pi4NextRaIfIndex = i4Index;
            return (SNMP_SUCCESS);
        }
    }

    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  Ip6GetNextRaTblEntry   
 Description :  This function Create RA Entry in the RA Table for a 
                given RAIfIndex otherwise NULL.
 Input       :  i4RtrAdvtIfIndex    - Ip6RAIfIndex 
 Output      :  None
 Returns     :  Ptr to tIp6RAEntry or NULL.
****************************************************************************/
tIp6RAEntry        *
Ip6CreateRaTblEntry (INT4 i4RtrAdvtIfIndex)
{
    tIp6RAEntry        *pRAEntry = NULL;

    if ((i4RtrAdvtIfIndex < IP6_ZERO) ||
        (i4RtrAdvtIfIndex > IP6_MAX_LOGICAL_IF_INDEX))
    {
        return (NULL);
    }

    pRAEntry = gIp6GblInfo.apIp6RaTbl[i4RtrAdvtIfIndex];
    if (pRAEntry == NULL)
    {
        return (NULL);
    }
    if (pRAEntry->u1RowStatus == IP6_RA_INVALID)
    {
        /* Init With Default Values... */
        pRAEntry->u1CurHopLimit = IP6_RA_DEF_HOPLMT;
        pRAEntry->u4ReachableTime = IP6_RA_DEF_REACHTIME;
        pRAEntry->u4RetransmitTime = IP6_RA_DEF_RETTIME;
        pRAEntry->u1SendAdverts = IP6_RA_DEF_SEND_STATUS;
        pRAEntry->u1OFlag = IP6_RA_DEF_O_FLAG;
        pRAEntry->u1MFlag = IP6_RA_DEF_M_FLAG;
        pRAEntry->u4MaxInterval = IP6_RA_DEF_MAX_RA_TIME;
        pRAEntry->u4MinInterval = IP6_RA_DEF_MIN_RA_TIME;
        pRAEntry->u2DefLifetime = IP6_RA_DEF_DEFTIME;
        pRAEntry->u4LinkMTU = IP6_RA_DEF_LINK_MTU;
        pRAEntry->u1RowStatus = NOT_IN_SERVICE;

        return (pRAEntry);
    }

    return (NULL);
}

/****************************************************************************
 Function    :  Ip6DeleteRaTblEntry    
 Description :  This function Delete the RA Entry form the RA Table for a 
                given RAIfIndex.
 Input       :  i4RaIfIndex         - Ip6RAIfIndex 
 Output      :  None
 Returns     :  SNMP_SUCCESS/SNMP_FAILURE.
****************************************************************************/
INT1
Ip6DeleteRaTblEntry (INT4 i4RtrAdvtIfIndex)
{
    tIp6RAEntry        *pRAEntry = NULL;

    if ((i4RtrAdvtIfIndex < IP6_ZERO) ||
        (i4RtrAdvtIfIndex > IP6_MAX_LOGICAL_IF_INDEX))
    {
        return (SNMP_FAILURE);
    }

    pRAEntry = gIp6GblInfo.apIp6RaTbl[i4RtrAdvtIfIndex];
    if (pRAEntry == NULL)
    {
        return (SNMP_FAILURE);
    }

    /* Invalidate the RA Entry */
    pRAEntry->u1RowStatus = IP6_RA_INVALID;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 *  Function    :  Ip6IsIfFwdEnabled       
 *  Description :  This function returns TRUE/FALSE depending on
 * whether forwarding is enabled or disabled on a particular interface
 * Input       :  i4IfIndex            
 * Output      :  None
 * Returns     :  TRUE/FALSE 
 * **************************************************************************/
UINT1
Ip6IsIfFwdEnabled (INT4 i4IfIndex)
{
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4IfIndex);

    if (pIf6 == NULL)
    {
        return FALSE;
    }

    if (pIf6->u1Ipv6IfFwdOperStatus == IP6_IF_FORW_DISABLE)
    {
        return FALSE;
    }
    return TRUE;
}

/******************************************************************************
*  DESCRIPTION : This routine is used to get the address type of the prefix
* 
* 
*  INPUTS      : i4Fsipv6AddrSelPolicyIfIndex - Interface Index
*                pFsipv6AddrSelPolicyPrefix - Policy Prefix
*                i4Fsipv6AddrSelPolicyPrefixLen - Prefix Length
*
*
*  OUTPUTS     : pi4RetValFsipv6AddrSelPolicyAddrType - Address type to
*                                                       be returned
*
*  RETURNS     : IP6_FAILURE / IP6_SUCCESS
*  NOTES       :
* **************************************************************************/

INT1
Ip6GetFsipv6AddrSelPolicyAddrType (INT4 i4Fsipv6AddrSelPolicyIfIndex,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsipv6AddrSelPolicyPrefix,
                                   INT4 i4Fsipv6AddrSelPolicyPrefixLen,
                                   INT4 *pi4RetValFsipv6AddrSelPolicyAddrType)
{

    tIp6AddrSelPolicy  *pPolicyPrefix = NULL;

    pPolicyPrefix = Ip6GetPolicyPrefix ((UINT4) i4Fsipv6AddrSelPolicyIfIndex,
                                        (tIp6Addr *) (VOID *)
                                        pFsipv6AddrSelPolicyPrefix->
                                        pu1_OctetList,
                                        (UINT1) i4Fsipv6AddrSelPolicyPrefixLen);
    if (pPolicyPrefix != NULL)
    {
        *pi4RetValFsipv6AddrSelPolicyAddrType = pPolicyPrefix->u1AddrType;
        return IP6_SUCCESS;
    }

    return IP6_FAILURE;
}
