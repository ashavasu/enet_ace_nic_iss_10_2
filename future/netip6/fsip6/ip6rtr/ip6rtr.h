#ifndef __IP6RTR_H__
#define __IP6RTR_H__
/*
 *----------------------------------------------------------------------------- 
 *    FILE NAME                    :    ip6rtr.h
 *
 *    PRINCIPAL AUTHOR             :    Jayabharathi R.
 *
 *    SUBSYSTEM NAME               :    IPv6
 *
 *    MODULE NAME                  :    IP6 ND6 Submodule
 *
 *    LANGUAGE                     :    C
 *
 *    TARGET ENVIRONMENT           :    UNIX
 *
 *    DATE OF FIRST RELEASE        :    DDD-MM-1996
 *
 *    DESCRIPTION                  :    This file contains the router related 
 *                                      constants, macros.
 *
 *----------------------------------------------------------------------------- 
 */
#define IP6_IF_RA_TIMER(if)  &if->Timer
#endif
