/*******************************************************************
 *     Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * 
 *     $Id: nd6rtr.c,v 1.61 2017/12/19 13:41:55 siva Exp $
 * 
 * ****************************************************************/

/*
 *----------------------------------------------------------------------------- 
 *    FILE NAME                    :    nd6rtr.c
 *
 *    PRINCIPAL AUTHOR             :    Jayabharathi R.
 *
 *    SUBSYSTEM NAME               :    IPv6
 *
 *    MODULE NAME                  :    IP6 ND Submodule for ROUTER
 *
 *    LANGUAGE                     :    C
 *
 *    TARGET ENVIRONMENT           :    UNIX
 *
 *    DATE OF FIRST RELEASE        :    DDD-MM-2002
 *
 *    DESCRIPTION                  :    This file contains routines for 
 *                                      processing Neighbor discovery messages 
 *                                      of an IPV6-router.
 *----------------------------------------------------------------------------- 
 */
#include "ip6inc.h"
#include "ip6rtr.h"
#ifdef NPAPI_WANTED
#include "nd6red.h"
#endif
#ifdef HA_WANTED
#include "mipv6.h"
extern INT4         i4GlbPrefPoolId;
extern INT2         gi2HAPreference;
#endif

#ifdef MN_WANTED
#include "mnmvdet.h"
extern tMNMvDetInfo **MoveDetInfo;
#endif
PRIVATE INT4        Nd6FillRouterSol
PROTO ((UINT1 u1Flag, tIp6If * pIf6, tCRU_BUF_CHAIN_HEADER * pBuf));

tTMO_SLL            Nd6RouteList;

PRIVATE INT4        Nd6DelRtEntryInCxt (tIp6Cxt *, tIp6Addr *,
                                        UINT1, tIp6Addr *);
PRIVATE tNd6RtEntry *ND6GetRouteEntry (tIp6Addr *, UINT1, tIp6Addr *, UINT4);

PRIVATE INT4        Nd6AddRtEntry (tIp6Addr * pDstAddr, UINT1 u1Prefixlen,
                                   tIp6Addr * pNextHop, UINT4 u4IfIndex,
                                   UINT2 u2LifeTime, UINT1 u1RouteType, UINT1);
PRIVATE INT4        Nd6FillRoutAdv
PROTO ((tIp6If * pIf6, UINT1 u1Flag, tCRU_BUF_CHAIN_HEADER * pBuf));

PRIVATE INT4        Nd6FillRedirectInCxt
PROTO ((tIp6Cxt * pIp6Cxt, tIp6Addr * pDstAddr6,
        tIp6Addr * pTargAddr6, tCRU_BUF_CHAIN_HEADER * pBuf));

PRIVATE INT4        Nd6FillRedirExtInCxt
PROTO ((tIp6Cxt * pIp6Cxt, UINT4 u4RedirDataLen,
        tCRU_BUF_CHAIN_HEADER * pRcvdBuf, tCRU_BUF_CHAIN_HEADER * pBuf));

PRIVATE INT4        Nd6GetPrefixPerRa
PROTO ((tIp6If * pIf6, UINT4 u4RaSize, INT2 *pPrefPerRa));

PRIVATE INT4 Nd6FillMtu PROTO ((tIp6If * pIf6, tCRU_BUF_CHAIN_HEADER * pBuf));

INT4 Nd6FillPrefixesInCxt PROTO ((tIp6Cxt * pIp6Cxt, tTMO_SLL * pAddr6List,
                                  tTMO_SLL_NODE ** ppCur, INT2 i2Prefixes,
                                  INT2 *pPrefSeen, UINT2 *pu2Offset,
                                  tCRU_BUF_CHAIN_HEADER * pBuf));

PRIVATE VOID Nd6DeleteReDirectRoute PROTO ((VOID));
PRIVATE INT4 Nd6FillRARouteInfoOption PROTO
    ((tIp6If * pIf6, UINT1 u1LifetimeFlag,
      tIp6RARouteInfoNode * pRARouteInfo,
      tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 *u4PktLen));

#ifdef HA_WANTED
INT4 Nd6FillHaInfoOptionInCxt PROTO ((tIp6Cxt * pIp6Cxt,
                                      tCRU_BUF_CHAIN_HEADER * pBuf));
INT4 Nd6FillAdvIntervalOption PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf,
                                      tIp6If * pIf6));
#endif

/*****************************************************************************
DESCRIPTION         Processes the received Router Solicitation
                    Messages, updates the corresponding 
                    NDCache entry and schedules the response
                    Router Advertisements

Input(s)            pIf6        -   Pointer to the interface 
                    pIp6        -   Pointer to the IPv6 header of
                                     the received packet
                    u2Len       -   Length of the ND payload in the
                                     received IPv6 packet
                    pBuf        -   Pointer to the received
                                     IPv6 packet

Output(s)           None

RETURNS             IP6_SUCCESS     - Processing of RS succeeds
                    IP6_FAILURE - Processing of RS fails
****************************************************************************/
INT4
Nd6RcvRoutSol (tIp6If * pIf6, tIp6Hdr * pIp6, UINT2 u2Len,
               tCRU_BUF_CHAIN_HEADER * pBuf)
{
    UINT1               u1Copy = 0;
    UINT4               u4ExtnsLen = 0;
    UINT4               u4Nd6Offset = 0;
    tNd6RoutSol         nd6Rsol, *pNd6Rsol;
    tIp6Addr           *pIp6SrcAddr = NULL;

    /* Extract RS Message from the buffer */
    u4Nd6Offset = IP6_BUF_READ_OFFSET (pBuf) - sizeof (tIcmp6PktHdr);
    IP6_BUF_READ_OFFSET (pBuf) -= sizeof (tIcmp6PktHdr);

    if ((pNd6Rsol = (tNd6RoutSol *) Ip6BufRead
         (pBuf, (UINT1 *) &nd6Rsol, u4Nd6Offset,
          sizeof (tNd6RoutSol), u1Copy)) == NULL)
    {
        IP6_TRC_ARG2 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                      BUFFER_TRC | ALL_FAILURE_TRC, ND6_NAME,
                      "ND6:Nd6RcvRoutSol: Receive RS - BufRead Failed! \
                       BufPtr %p Offset %d\n", pBuf, u4Nd6Offset);
        IP6_TRC_ARG3 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC, BUFFER_TRC,
                      IP6_NAME,
                      "%s buffer error occurred, Type = %d Module = 0x%X Bufptr = %p ",
                      ERROR_FATAL_STR, ERR_BUF_READ, pBuf);
        return (IP6_FAILURE);
    }
    u4ExtnsLen = u2Len - sizeof (tNd6RoutSol);

    /* Validate the NS message */
    if (Nd6ValidateRoutSolInCxt (pIf6->pIp6Cxt, pIp6, pNd6Rsol, u2Len)
        == IP6_FAILURE)
    {
        /* Increment the count on ICMP bad packets received */
        ICMP6_INC_IN_ERRS (pIf6->pIp6Cxt->Icmp6Stats);
        return (IP6_FAILURE);
    }
    /* Process the Extension Options */
    if (u4ExtnsLen > 0)
    {
        if (Nd6ProcessRoutSolExtns (pIf6, pIp6, u4ExtnsLen, pBuf) ==
            IP6_FAILURE)
        {
            return IP6_FAILURE;
        }
    }

    /* Check and Send Solicited RA message as response */
    if ((Ip6CheckRAStatus ((INT4) (pIf6->u4Index))) == TRUE)
    {
        pIp6SrcAddr = Ip6GetLlocalAddr (pIf6->u4Index);
        if (pIp6SrcAddr != NULL)
        {
            Nd6SendSolRoutAdv (pIf6, pIp6SrcAddr, &pIp6->srcAddr, 0);
        }
    }

    if (Ip6BufRelease (pIf6->pIp6Cxt->u4ContextId, pBuf,
                       FALSE, ND6_MODULE) != IP6_SUCCESS)
    {
        IP6_TRC_ARG (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                     BUFFER_TRC | ALL_FAILURE_TRC, ND6_NAME,
                     "ND6:Nd6RcvRoutSol: Rcv RS - BufRelease Failed\n");
        IP6_TRC_ARG3 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC, BUFFER_TRC,
                      IP6_NAME,
                      "%s buffer error occurred, Type = %d Module = 0x%X Bufptr = %p ",
                      ERROR_FATAL_STR, ERR_BUF_RELEASE, pBuf);
    }
    return (IP6_SUCCESS);

}

/*****************************************************************************
DESCRIPTION         This routine processes the RS Extension Options and acts
                    accordingly

Input(s)            pIf6        -   Pointer to the interface 
                    pIp6        -   Pointer to the IPv6 header of
                                     the received packet
                    u4ExtnsLen  -   Length of the ND extentions in the
                                     received IPv6 packet
                    pBuf        -   Pointer to the received
                                     IPv6 packet

Output(s)           None

RETURNS             IP6_SUCCESS     - Processing of RS extns succeeds
                    IP6_FAILURE - Processing of RS extns fails
****************************************************************************/
INT4
Nd6ProcessRoutSolExtns (tIp6If * pIf6, tIp6Hdr * pIp6, UINT4 u4ExtnsLen,
                        tCRU_BUF_CHAIN_HEADER * pBuf)
{
    tNd6ExtHdr          nd6Ext;
    tNd6ExtHdr         *pNd6Ext = NULL;
    tNd6AddrExt         Nd6Lla;
    tNd6AddrExt        *pNd6Lla = NULL;
    tCgaOptions         CgaOptions;
    tNd6CacheEntry     *pNd6cEntry = NULL;
    tUtlSysPreciseTime  SysPreciseTime;
    UINT4               u4ExtOffset = 0;
    UINT4               u4TSNew = 0;
    UINT4               u4TempOffset = 0;
    UINT4               u4SeNDOptCount = 0;
    UINT1               u1SecureFlag = IP_ZERO;
    UINT1               au1PubKey[IP6_SEND_CGA_BYTES];
    UINT1               au1Nonce[ND6_NONCE_LENGTH];
    UINT1               u1SeNDFlag = 0;

    MEMSET (&nd6Ext, 0, sizeof (tNd6ExtHdr));
    MEMSET (&Nd6Lla, 0, sizeof (tNd6AddrExt));
    MEMSET (&CgaOptions, 0, sizeof (tCgaOptions));
    MEMSET (au1PubKey, 0, IP6_SEND_CGA_BYTES);
    MEMSET (au1Nonce, 0, ND6_NONCE_LENGTH);
    MEMSET (&SysPreciseTime, 0, sizeof (tUtlSysPreciseTime));

    u4TempOffset = IP6_BUF_READ_OFFSET (pBuf) - sizeof (tNd6RoutSol);
    CgaOptions.pu1DerPubKey = au1PubKey;

    /* Extract the Extension Options */
    while (u4ExtnsLen)
    {
        if ((pNd6Ext = Nd6ExtractExtnHdrInCxt (pIf6->pIp6Cxt, pBuf)) == NULL)
        {
            return (IP6_FAILURE);
        }
        u4ExtOffset = IP6_BUF_READ_OFFSET (pBuf) - IP6_TYPE_LEN_BYTES;
        IP6_BUF_READ_OFFSET (pBuf) = u4ExtOffset;

        switch (pNd6Ext->u1Type)
        {
            case ND6_SRC_LLA_EXT:
                if (Nd6ProcessSLLAOption
                    (u4ExtnsLen, pNd6Ext, pIf6, &Nd6Lla, &pNd6Lla,
                     pBuf) == IP6_FAILURE)
                {
                    IP6_TRC_ARG (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                                 DATA_PATH_TRC, ND6_NAME,
                                 "ND6: Nd6ProcessRoutSolExtns:Validation of NS "
                                 "Failed - Invalid SLLA \n");
                    (IP6_SEND_IF_DROP_STATS
                     (pIf6, COUNT_RS)).u4SrcLinkAddrPkts++;
                    (IP6_IF_STATS (pIf6))->u4NdSecureDroppedPkts++;
                    return IP6_FAILURE;
                }
                (IP6_SEND_IF_IN_STATS (pIf6, COUNT_RS)).u4SrcLinkAddrPkts++;
                u4ExtnsLen -= sizeof (tNd6AddrExt);
                break;

            case ND6_SEND_CGA_OPT:

                if (IP6_TRUE == IF_SEND_ENABLED (pIf6))
                {
                    if (Nd6SeNDProcessCgaOption (pIf6, pIp6, pBuf,
                                                 pNd6Ext->u1Len,
                                                 &CgaOptions) == IP6_FAILURE)
                    {
                        IP6_TRC_ARG (pIf6->pIp6Cxt->u4ContextId,
                                     ND6_MOD_TRC, BUFFER_TRC,
                                     ND6_NAME, "Nd6ProcessRoutSolExtns:"
                                     " Cga processing failed\n");
                        (IP6_SEND_IF_DROP_STATS
                         (pIf6, COUNT_RS)).u4CgaOptPkts++;
                        (IP6_IF_STATS (pIf6))->u4NdSecureDroppedPkts++;
                        return IP6_FAILURE;
                    }
                    (IP6_SEND_IF_IN_STATS (pIf6, COUNT_RS)).u4CgaOptPkts++;
                    u4SeNDOptCount++;
                }
                else
                {
                    IP6_BUF_READ_OFFSET (pBuf) +=
                        (UINT4) (pNd6Ext->u1Len * BYTES_IN_OCTECT);
                }
                /* extension length is in unit of 8 octets */
                u4ExtnsLen -= (UINT4) (pNd6Ext->u1Len * BYTES_IN_OCTECT);
                break;

            case ND6_SEND_TIMESTAMP_OPT:

                if (IP6_TRUE == IF_SEND_ENABLED (pIf6))
                {
                    if (Nd6SeNDProcessTSOption (pIf6, pIp6, pBuf, &u4TSNew)
                        == IP6_FAILURE)
                    {
                        IP6_TRC_ARG (pIf6->pIp6Cxt->u4ContextId,
                                     ND6_MOD_TRC, BUFFER_TRC,
                                     ND6_NAME, "Nd6ProcessRoutSolExtns:"
                                     " Timestamp processing failed\n");
                        (IP6_SEND_IF_DROP_STATS
                         (pIf6, COUNT_RS)).u4TimeStampOptPkts++;
                        (IP6_IF_STATS (pIf6))->u4NdSecureDroppedPkts++;
                        return IP6_FAILURE;
                    }
                    u4SeNDOptCount++;
                    (IP6_SEND_IF_IN_STATS (pIf6, COUNT_RS)).
                        u4TimeStampOptPkts++;
                }
                else
                {
                    IP6_BUF_READ_OFFSET (pBuf) +=
                        (UINT4) (pNd6Ext->u1Len * BYTES_IN_OCTECT);
                }
                /* extension length is in unit of 8 octets */
                u4ExtnsLen -= (UINT4) (pNd6Ext->u1Len * BYTES_IN_OCTECT);
                break;

            case ND6_SEND_NONCE_OPT:

                if (IP6_TRUE == IF_SEND_ENABLED (pIf6))
                {
                    if (Nd6SeNDProcessNonceOption (pIf6, pIp6, pBuf,
                                                   au1Nonce,
                                                   u1SeNDFlag) == IP6_FAILURE)
                    {

                        IP6_TRC_ARG (pIf6->pIp6Cxt->u4ContextId,
                                     ND6_MOD_TRC, BUFFER_TRC,
                                     ND6_NAME, "Nd6ProcessRoutSolExtns:"
                                     " Nonce processing failed\n");
                        (IP6_SEND_IF_DROP_STATS
                         (pIf6, COUNT_RS)).u4NonceOptPkts++;
                        (IP6_IF_STATS (pIf6))->u4NdSecureDroppedPkts++;
                        return IP6_FAILURE;
                    }
                    (IP6_SEND_IF_IN_STATS (pIf6, COUNT_RS)).u4NonceOptPkts++;
                    u4SeNDOptCount++;
                    MEMCPY (pIf6->au1RcvdNonce, au1Nonce, ND6_NONCE_LENGTH);
                }
                else
                {
                    IP6_BUF_READ_OFFSET (pBuf) +=
                        (UINT4) (pNd6Ext->u1Len * BYTES_IN_OCTECT);
                }
                /* extension length is in unit of 8 octets */
                u4ExtnsLen -= (UINT4) (pNd6Ext->u1Len * BYTES_IN_OCTECT);
                break;

            case ND6_SEND_RSA_SIGN_OPT:
                if (IP6_TRUE == IF_SEND_ENABLED (pIf6))
                {
                    if (Nd6SeNDProcessRsaOption (pIf6, pIp6, pBuf,
                                                 pNd6Ext->u1Len, u4TempOffset,
                                                 &CgaOptions) == IP6_FAILURE)
                    {
                        IP6_TRC_ARG (pIf6->pIp6Cxt->u4ContextId,
                                     ND6_MOD_TRC, BUFFER_TRC,
                                     ND6_NAME, "Nd6ProcessRoutSolExtns:"
                                     " RSA processing failed\n");
                        (IP6_SEND_IF_DROP_STATS
                         (pIf6, COUNT_RS)).u4RsaOptPkts++;
                        (IP6_IF_STATS (pIf6))->u4NdSecureDroppedPkts++;
                        return IP6_FAILURE;
                    }
                    (IP6_SEND_IF_IN_STATS (pIf6, COUNT_RS)).u4RsaOptPkts++;
                    u4SeNDOptCount++;
                }
                else
                {
                    IP6_BUF_READ_OFFSET (pBuf) +=
                        (UINT4) (pNd6Ext->u1Len * BYTES_IN_OCTECT);
                }
                /* extension length is in unit of 8 octets */
                u4ExtnsLen -= (UINT4) (pNd6Ext->u1Len * BYTES_IN_OCTECT);
                break;

            default:
                IP6_BUF_READ_OFFSET (pBuf) +=
                    (UINT4) (pNd6Ext->u1Len * BYTES_IN_OCTECT);
                u4ExtnsLen -= (UINT4) (pNd6Ext->u1Len * BYTES_IN_OCTECT);
                break;
        }
    }

    /* If SeND is enabled in full secure mode then all the four options
     * should present, and in mixed mode either all options should present
     * or no SeND options should present in the packet.
     * if it is not then discard the packet
     */

    if (((ND6_SECURE_ENABLE == pIf6->u1SeNDStatus)
         && u4SeNDOptCount != ND6_SEND_SOL_OPT_COUNT)
        || ((ND6_SECURE_MIXED == pIf6->u1SeNDStatus)
            && (!((u4SeNDOptCount == IP6_ZERO)
                  || (u4SeNDOptCount == ND6_SEND_SOL_OPT_COUNT)))))
    {
        (IP6_IF_STATS (pIf6))->u4NdSecureInvalidPkts++;
        return (IP6_FAILURE);
    }

    u1SecureFlag = ND6_UNSECURE_ENTRY;
    if ((ND6_SECURE_MIXED != pIf6->u1SeNDStatus)
        && (ND6_SEND_SOL_OPT_COUNT == u4SeNDOptCount))
    {
        u1SecureFlag = ND6_SECURE_ENTRY;
    }
    if (!(IS_ADDR_UNSPECIFIED (pIp6->srcAddr)))
    {
        /* 
         * Update the NDCache entry if the source is not
         * an Unspecified address 
         */
        if (pNd6Lla != NULL)
        {
            if (Nd6UpdateCache (pIf6, &pIp6->srcAddr,
                                pNd6Lla->lladdr, IP6_ENET_ADDR_LEN,
                                ND6_NEIGHBOR_SOLICITATION, NULL,
                                u1SecureFlag) == IP6_FAILURE)
            {
                IP6_TRC_ARG (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                             ALL_FAILURE_TRC, ND6_NAME,
                             "Nd6ProcessRoutSolExtns: SRC LLA Extn - Update "
                             "Cache Failed \n");
                return (IP6_FAILURE);
            }

            if (IP6_TRUE == IF_SEND_ENABLED (pIf6))
            {
                pNd6cEntry = Nd6IsCacheForAddr (pIf6, &pIp6->srcAddr);
                if (NULL != pNd6cEntry)
                {
                    /* first message received from the neighbor */
                    if (u4TSNew != IP6_ZERO)
                    {
                        TmrGetPreciseSysTime (&SysPreciseTime);
                        MEMCPY (&pNd6cEntry->u4SendRDLast,
                                &(SysPreciseTime.u4Sec), IP6_FOUR);
                        pNd6cEntry->u4SendTSLast = u4TSNew;
                    }

                    /* nonce from the received secured solicitation */
                    if (MEMCMP (pNd6cEntry->au1RcvdNonce, au1Nonce,
                                ND6_NONCE_LENGTH) != IP6_ZERO)
                    {
                        MEMCPY (pNd6cEntry->au1RcvdNonce, au1Nonce,
                                ND6_NONCE_LENGTH);
                    }
                    else
                    {
                        if (ND6_SECURE_MIXED != pIf6->u1SeNDStatus)
                        {
                            /* stale nonce */
                            IP6_TRC_ARG (pIf6->pIp6Cxt->u4ContextId,
                                         ND6_MOD_TRC, ALL_FAILURE_TRC,
                                         ND6_NAME, "Nd6ProcessRoutSolExtns:"
                                         " received stale nonce \n");
                            return (IP6_FAILURE);
                        }
                    }
                }
            }
        }
    }
    return (IP6_SUCCESS);
}

/*****************************************************************************
DESCRIPTION         This routine validates the received RS message

Input(s)            pIp6        -   Pointer to the IPv6 header of
                                     the received packet
                    pNd6Rsol    -   Pointer to the router solicitation.
                    u2Len       -   Length of the ND payload in the
                                     received IPv6 packet

Output(s)           None

RETURNS             IP6_SUCCESS     - if Validation if RS succeeds
                    IP6_FAILURE - if Validation if RS fails
****************************************************************************/
INT4
Nd6ValidateRoutSolInCxt (tIp6Cxt * pIp6Cxt, tIp6Hdr * pIp6,
                         tNd6RoutSol * pNd6Rsol, UINT2 u2Len)
{
    tIcmp6PktHdr        icmp6Hdr;

    icmp6Hdr = pNd6Rsol->icmp6Hdr;
    if (Nd6ValidateMsgHdrInCxt (pIp6Cxt, pIp6, &icmp6Hdr) == IP6_FAILURE)
    {
        return (IP6_FAILURE);
    }

    /* ICMP Length MUST be 8 or more octets */
    if (u2Len < sizeof (tNd6RoutSol))
    {
        IP6_TRC_ARG1 (pIp6Cxt->u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC,
                      ND6_NAME,
                      "ND6: Nd6ValidateRoutSol:Validation of RS Failed - "
                      "Incorrect Len %d\n", u2Len);
        return (IP6_FAILURE);
    }

    return (IP6_SUCCESS);

}

/*****************************************************************************
DESCRIPTION         Processes the received Router Advertisement 
                    Messages. 
                    
                    pIp6        -   Pointer to the IPv6 header of
                                     the received packet
                    u2Len       -   Length of the ND payload in the
                                     received IPv6 packet
                    pBuf        -   Pointer to the received
                                     IPv6 packet

OUTPUTS             None

RETURNS             IP6_SUCCESS      - Sending of RA succeeds
                    IP6_FAILURE  - Processing of RA fails 
****************************************************************************/

INT4
Nd6RcvRoutAdv (tIp6Hdr * pIp6, tIp6If * pIf6, UINT2 u2Len,
               tCRU_BUF_CHAIN_HEADER * pBuf)
{
    UINT1               u1SecureFlag = IP6_ZERO;

    /* proxy enabled on interface */
    if (Nd6IsProxyEnabled (pIf6) == IP6_TRUE)
    {
        Nd6ProxyRtrAdvt (pIp6, pIf6, u2Len, pBuf);
    }

    if (pIf6->u1Ipv6IfFwdOperStatus == IP6_IF_FORW_ENABLE)
    {
        UINT1               u1Copy = 0;
        UINT1               u1HAInfoOpt;
        UINT4               u4Nd6Offset = 0;
        UINT4               u4ExtnsLen = 0;
        tNd6RoutAdv         nd6RAdv, *pNd6RAdv;
#ifdef HA_WANTED
        tHaListEntry       *pHaEntry;
        u1HAInfoOpt = HOME_AGT_INFO_NO_OPT;
#endif

        /* Extract RA Message from the buffer */
        u4Nd6Offset = IP6_BUF_READ_OFFSET (pBuf) - sizeof (tIcmp6PktHdr);
        IP6_BUF_READ_OFFSET (pBuf) -= sizeof (tIcmp6PktHdr);

        if ((pNd6RAdv = (tNd6RoutAdv *) Ip6BufRead
             (pBuf, (UINT1 *) &nd6RAdv, u4Nd6Offset,
              sizeof (tNd6RoutAdv), u1Copy)) == NULL)
        {
            IP6_TRC_ARG2 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC, BUFFER_TRC,
                          ND6_NAME,
                          "ND6:Nd6ProcessRoutSolExtns: BufRead "
                          "Fail! BufPtr %p Offset%d\n", pBuf,
                          IP6_BUF_READ_OFFSET (pBuf));
            IP6_TRC_ARG3 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC, BUFFER_TRC,
                          IP6_NAME,
                          "ND6:Nd6ProcessRoutSolExtns: %s buffer error occurred, Type "
                          "= %d Module=0x%X Bufptr=%p", ERROR_FATAL_STR,
                          ERR_BUF_READ, pBuf);
            return (IP6_FAILURE);
        }

        /* Validate the RA message */
        if (Nd6ValidateRoutAdvInCxt (pIf6, pIp6,
                                     pNd6RAdv, u2Len) == IP6_FAILURE)
        {
            /* Increment the count on ICMP bad packets received */
            ICMP6_INC_IN_ERRS (pIf6->pIp6Cxt->Icmp6Stats);
            return (IP6_FAILURE);
        }

#ifdef HA_WANTED
        if (!
            (pNd6RAdv->
             u1StfulFlag & ND6_H_BIT_STFUL_FLAG /* if  H Bit is set */ ))
        {
            if ((pHaEntry =
                 Mip6GetHaEntry (&pIp6->srcAddr, pIf6->u4Index)) != NULL)
            {
                /* Delete This Entry From the HA List */
                Mip6DeleteHaEntry (&pIp6->srcAddr, pIf6->u4Index);
            }
        }
        else                    /* the entry is not present already */
        {
            /*
             * Create a New HA List Entry passing this pIp6->pSrcAddr,
             * interface index, set preference value to zero here
             * initially- but when  HA info Option is present, the
             * value for preference would be updated from that.
             */

            /*
             * hence, an entry would be created here in the HA List.
             * The initial contents of the entry are u4IfIndex &
             * ip6HaLlAddr.  u1Pref would be updated if  HA information
             * option is present in the function Nd6ProcessRoutAdvExtns( )
             */

            /* Create this entry */
            if (pNd6RAdv->u2Lifetime != 0)
            {
                Mip6UpdateHaEntry (&pIp6->srcAddr, pIf6->u4Index, 0,
                                   (UINT2) OSIX_NTOHS (pNd6RAdv->u2Lifetime));
            }
        }
#endif

        /* Process the Extension Options */
        u4ExtnsLen = u2Len - sizeof (tNd6RoutAdv);
        if (Nd6ProcessRoutAdvExtns (pIp6, pIf6, u4ExtnsLen, pBuf, 0,
                                    (UINT2) OSIX_NTOHS (pNd6RAdv->u2Lifetime),
                                    &u1HAInfoOpt, &u1SecureFlag) == IP6_FAILURE)
        {
            return (IP6_FAILURE);
        }
#ifdef HA_WANTED
        else if (pNd6RAdv->
                 u1StfulFlag & ND6_H_BIT_STFUL_FLAG /* if  H Bit is set */ )
        {
            if ((pHaEntry =
                 Mip6GetHaEntry (&pIp6->srcAddr, pIf6->u4Index)) != NULL)
            {
                if ((pNd6RAdv->u2Lifetime == 0)
                    && (u1HAInfoOpt == HOME_AGT_INFO_NO_OPT))
                {
                    /* Delete this entry from HA LIST   */
                    Mip6DeleteHaEntry (&pIp6->srcAddr, pIf6->u4Index);
                }
            }
        }
#endif

#ifdef IP6_TEST_WANTED
        /* Send Certificate Path Solicitation */
        if (IP6_FAILURE == Nd6SeNDCertPathSol (pIf6, pIp6))
        {
            return IP6_FAILURE;
        }
#endif /* IP6_TEST_WANTED */

        if (Ip6BufRelease (pIf6->pIp6Cxt->u4ContextId, pBuf,
                           FALSE, ND6_MODULE) != IP6_SUCCESS)
        {
            IP6_TRC_ARG (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                         BUFFER_TRC | ALL_FAILURE_TRC, ND6_NAME,
                         "ND6: Nd6RcvRoutAdv:Rcv RA - BufRelease Failed\n");
            IP6_TRC_ARG3 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC, BUFFER_TRC,
                          IP6_NAME,
                          "%s buffer error occurred, Type = %d Module = 0x%X Bufptr = %p ",
                          ERROR_FATAL_STR, ERR_BUF_RELEASE, pBuf);
        }
        return (IP6_SUCCESS);
    }
    else
    {
        INT4                i4RetVal = IP6_FAILURE;
        UINT1               u1Copy = 1;
        UINT4               u4Nd6Offset = 0;
        UINT4               u4ExtnsLen = 0;
        tNd6RoutAdv         nd6RAdv, *pNd6RAdv;
        tIp6Cxt            *pIp6Cxt = pIf6->pIp6Cxt;
#ifdef MN_WANTED
        tHaListEntry       *pHaEntry;
        UINT1               u1HAInfoOpt = 0;
#endif

        /* Extract RA Message from the buffer */
        u4Nd6Offset = IP6_BUF_READ_OFFSET (pBuf) - sizeof (tIcmp6PktHdr);
        IP6_BUF_READ_OFFSET (pBuf) -= sizeof (tIcmp6PktHdr);

        if ((pNd6RAdv = (tNd6RoutAdv *) Ip6BufRead
             (pBuf, (UINT1 *) &nd6RAdv, u4Nd6Offset,
              sizeof (tNd6RoutAdv), u1Copy)) == NULL)
        {
            IP6_TRC_ARG2 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC, BUFFER_TRC,
                          ND6_NAME,
                          "ND6:Nd6ProcessRoutSolExtns: BufRead "
                          "Fail! BufPtr %p Offset%d\n", pBuf,
                          IP6_BUF_READ_OFFSET (pBuf));
            IP6_TRC_ARG3 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC, BUFFER_TRC,
                          IP6_NAME,
                          "ND6:Nd6ProcessRoutSolExtns: %s buffer error occurred, Type "
                          "= %d Module=0x%X Bufptr=%p", ERROR_FATAL_STR,
                          ERR_BUF_READ, pBuf);
            return (IP6_FAILURE);
        }

        /* Validate the RA message */
        if (Nd6ValidateRoutAdvInCxt (pIf6, pIp6, pNd6RAdv, u2Len) ==
            IP6_FAILURE)
        {
            /* Increment the count on ICMP bad packets received */
            ICMP6_INC_IN_ERRS (pIp6Cxt->Icmp6Stats);
            return (IP6_FAILURE);
        }

#ifdef MN_WANTED
        if (!
            (pNd6RAdv->
             u1StfulFlag & ND6_H_BIT_STFUL_FLAG /* if  H Bit is not set */ ))
        {
            if ((pHaEntry =
                 Mip6GetHaEntry (&pIp6->srcAddr, pIf6->u4Index)) != NULL)
            {
                /* Delete This Entry From the HA List */
                Mip6DeleteHaEntry (&pIp6->srcAddr, pIf6->u4Index);
            }
        }
        else                    /* the entry is not present already */
        {
            /*
             * Create a New HA List Entry passing this pIp6->pSrcAddr,
             * interface index, set preference value to zero here
             * initially- but when  HA info Option is present, the
             * value for preference would be updated from that.
             */

            /*
             * hence, an entry would be created here in the HA List.
             * The initial contents of the entry are u4IfIndex &
             * ip6HaLlAddr.  u1Pref would be updated if  HA information
             * option is present in the function Nd6ProcessRoutAdvExtns( )
             */

            /* Create this entry */
            if (pNd6RAdv->u2Lifetime != 0)
            {
                Mip6UpdateHaEntry (&pIp6->srcAddr, pIf6->u4Index, 0,
                                   (UINT2) OSIX_NTOHS (pNd6RAdv->u2Lifetime));
            }
        }

#endif

        /* Process the Extension Options */
        u4ExtnsLen = u2Len - sizeof (tNd6RoutAdv);
#ifndef MIP6_WANTED
        if (Nd6ProcessRoutAdvExtns (pIp6, pIf6, u4ExtnsLen,
                                    pBuf, 0, 0, NULL,
                                    &u1SecureFlag) == IP6_FAILURE)
#else
        if (Nd6ProcessRoutAdvExtns
            (pIp6, pIf6, u4ExtnsLen, pBuf, 0, pNd6RAdv->u2Lifetime,
             &u1HAInfoOpt, &u1SecureFlag) == IP6_FAILURE)
#endif
        {
            return (IP6_FAILURE);
        }

#ifdef MN_WANTED
        if ((i4RetVal = MNProcessRoutAdv (&pIp6->srcAddr, pIf6)) == TERMINATE)
        {
            IP6_TRC_ARG (pIp6Cxt->u4ContextId, ND6_MOD_TRC, ALL_FAILURE_TRC,
                         ND6_NAME, "Nd6RcvRoutAdv:RA process terminated ...\n");
            return IP6_FAILURE;
        }
        else if (i4RetVal == IP6_FAILURE)
        {
            IP6_TRC_ARG (pIp6Cxt->u4ContextId, ND6_MOD_TRC, ALL_FAILURE_TRC,
                         ND6_NAME,
                         "RA process terminated - Could not get RA type...\n");
            return IP6_FAILURE;
        }
#endif
        i4RetVal = Nd6ProcessRouterAdv (pIf6, pIp6, pNd6RAdv, u1SecureFlag);
        UNUSED_PARAM (i4RetVal);

#ifdef MN_WANTED
        else
    if (pNd6RAdv->u1StfulFlag & ND6_H_BIT_STFUL_FLAG /* if  H Bit is set */ )
    {
        if ((pHaEntry = Mip6GetHaEntry (&pIp6->srcAddr, pIf6->u4Index)) != NULL)
        {
            if ((pNd6RAdv->u2Lifetime == 0)
                && (u1HAInfoOpt == HOME_AGT_INFO_NO_OPT))
            {
                /* Delete this entry from HA LIST   */
                Mip6DeleteHaEntry (&pIp6->srcAddr, pIf6->u4Index);
            }
        }
    }
#endif
        /* RA has been received, So need not send any more RSs if RS sent 
         * is more or equal to 1.
         */
        pIf6->u1RsFlag = IP6_IF_NO_RS_SEND;

#ifdef IP6_TEST_WANTED
        /* Send Certificate Path Solicitation */
        if (IP6_FAILURE == Nd6SeNDCertPathSol (pIf6, pIp6))
        {
            return IP6_FAILURE;
        }
#endif /* IP6_TEST_WANTED */

        if (Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE, ND6_MODULE) !=
            IP6_SUCCESS)
        {
            IP6_TRC_ARG (pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                         BUFFER_TRC | ALL_FAILURE_TRC, ND6_NAME,
                         "ND6: Nd6RcvRoutAdv:Rcv RA - BufRelease Failed\n");
            IP6_TRC_ARG3 (pIp6Cxt->u4ContextId, ND6_MOD_TRC, BUFFER_TRC,
                          IP6_NAME,
                          "%s buffer error occurred, Type = %d Module = 0x%X Bufptr = %p ",
                          ERROR_FATAL_STR, ERR_BUF_RELEASE, pBuf);
        }
        return (IP6_SUCCESS);

    }
}                                /*end of Nd6RcvRoutAdv */

/*****************************************************************************
DESCRIPTION         This routine validates the received RA message
                    Messages. 
                    
                    pIp6        -   Pointer to the IPv6 header of
                                    the received packet
                    pNd6RAdv    -   Pointer to the Router Adv.
                    u2Len       -   Length of the ND payload in the
                                     received IPv6 packet

OUTPUTS             None

RETURNS             IP6_SUCCESS      - if Validation of RA succeeds
                    IP6_FAILURE  - if Validation of RA fails 
****************************************************************************/
INT4
Nd6ValidateRoutAdvInCxt (tIp6If * pIf6, tIp6Hdr * pIp6,
                         tNd6RoutAdv * pNd6RAdv, UINT2 u2Len)
{
    tIp6Cxt            *pIp6Cxt = pIf6->pIp6Cxt;
    tIcmp6PktHdr        icmp6Hdr;

    if (pIf6->u1Ipv6IfFwdOperStatus == IP6_IF_FORW_ENABLE)
    {
        icmp6Hdr = pNd6RAdv->icmp6Hdr;
        if (Nd6ValidateMsgHdrInCxt (pIp6Cxt, pIp6, &icmp6Hdr) == IP6_FAILURE)
        {
            return (IP6_FAILURE);
        }

        /* ICMP Length MUST be 16 or more octets */
        if (u2Len < sizeof (tNd6RoutAdv))
        {
            IP6_TRC_ARG1 (pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                          DATA_PATH_TRC | ALL_FAILURE_TRC, ND6_NAME,
                          "ND6: Nd6ValidateRoutAdv:Validation of RA Failed - "
                          "Incorrect Len %d\n", u2Len);
            return (IP6_FAILURE);
        }

        /* validate the cur hop  limit value */
        /* if IP6_RA_CUR_MAX_HOP_LIMIT is changed to other than 255 add check */
        if ((NTOHL (pNd6RAdv->u4ReachTime) > (IP6_RA_MAX_REACH_TIME)) ||
            (NTOHL (pNd6RAdv->u4RetransTime) > (IP6_RA_MAX_RETRANS_TIMER)))
        {
            IP6_TRC_ARG2 (pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                          DATA_PATH_TRC | ALL_FAILURE_TRC, ND6_NAME,
                          "ND6: Nd6ValidateRoutAdv :Validation of RA Failed - "
                          "wrong Value - Reach Time %d, Retransmit time %d\n",
                          pNd6RAdv->u4ReachTime, pNd6RAdv->u4RetransTime);
            return (IP6_FAILURE);
        }

#ifdef HA_WANTED
        /* validate the values set for M or O or H flags */
        if ((pNd6RAdv->u1StfulFlag | ND6_MOH_FLAG_CHECK) != ND6_MOH_FLAG_CHECK)
        {
            IP6_TRC_ARG1 (pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                          DATA_PATH_TRC | ALL_FAILURE_TRC, ND6_NAME,
                          "ND6: Validation of RA Failed - wrong flag state %d\n",
                          pNd6RAdv->u1StfulFlag);
            return (IP6_FAILURE);
        }
#else
        /* validate the values set for M or O flags */
        /* validate MO bits only when P bit not set */
        if (ND6_P_BIT_NDPROXY_FLAG !=
            (pNd6RAdv->u1StfulFlag & ND6_P_BIT_NDPROXY_FLAG))
        {
            if ((pNd6RAdv->u1StfulFlag | ND6_MO_FLAG_CHECK) !=
                ND6_MO_FLAG_CHECK)
            {
                IP6_TRC_ARG1 (pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                              DATA_PATH_TRC | ALL_FAILURE_TRC, ND6_NAME,
                              "ND6: Nd6ValidateRoutAdv:Validation of RA Failed - "
                              "wrong flag state %d\n", pNd6RAdv->u1StfulFlag);
                return (IP6_FAILURE);
            }
        }
#endif

        /* validate retransmit timer value */
        if (((NTOHL (pNd6RAdv->u4RetransTime) < (IP6_RA_MIN_RETRANS_TIMER))
             || (NTOHL (pNd6RAdv->u4RetransTime) >
                 (IP6_RA_MAX_RETRANS_TIMER)))
            && (NTOHL (pNd6RAdv->u4RetransTime) != 0))
        {
            IP6_TRC_ARG1 (pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                          DATA_PATH_TRC | ALL_FAILURE_TRC, ND6_NAME,
                          "ND6: Nd6ValidateRoutAdv:Validation of RA Failed - "
                          "wrong Retans Time %d\n", pNd6RAdv->u4RetransTime);
            return (IP6_FAILURE);
        }

        return (IP6_SUCCESS);
    }
    else
    {

        icmp6Hdr = pNd6RAdv->icmp6Hdr;

        /* Source should be a link local address for hosts to 
         * identify a router uniquely
         */

        if (!(IS_ADDR_LLOCAL (pIp6->srcAddr)))
        {
            IP6_TRC_ARG1 (pIp6Cxt->u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC, ND6_NAME, "ND6:Nd6ValidateRoutAdv: Validation of ND MsgHdr Failed -\
                      Incorrect source address %s\n",
                          Ip6PrintAddr (&(pIp6->srcAddr)));
            return IP6_FAILURE;
        }

        if (Nd6ValidateMsgHdrInCxt (pIp6Cxt, pIp6, &icmp6Hdr) == IP6_FAILURE)
        {
            return (IP6_FAILURE);
        }

        /* ICMP Length MUST be 16 or more octets */
        if (u2Len < sizeof (tNd6RoutAdv))
        {
            IP6_TRC_ARG1 (pIp6Cxt->u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC | ALL_FAILURE_TRC, ND6_NAME, "ND6: Nd6ValidateRoutAdv:Validation of RA Failed - \
Incorrect Len %d\n", u2Len);
            return (IP6_FAILURE);
        }

        return (IP6_SUCCESS);
    }
}                                /*end of Nd6ValidateRoutAdv */

/*****************************************************************************
DESCRIPTION         This routine processes the RA Extension Options and acts
                    accordingly
                    
                    pIp6        -   Pointer to the IPv6 header of
                                     the received packet
                    u4ExtnsLen  -   Length of the ND extentions in the
                                     received IPv6 packet
                    pBuf        -   Pointer to the received
                                     IPv6 packet

OUTPUTS             None

RETURNS             IP6_SUCCESS      - if Validation of RA succeeds
                    IP6_FAILURE  - if Validation of RA fails 
****************************************************************************/
INT4
Nd6ProcessRoutAdvExtns (tIp6Hdr * pIp6, tIp6If * pIf6, UINT4 u4ExtnsLen,
                        tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 u1PrefAdvFlag,
                        UINT2 u2RtrLife, UINT1 *pu1HaInfoPresent,
                        UINT1 *pu1SecureFlag)
/* u1PrefAdvFlag - when set called for processing extentions when a prefix
adv is recd.*/
{
    tCgaOptions         CgaOptions;
    tUtlSysPreciseTime  SysPreciseTime;
    tNd6CacheEntry     *pNd6cEntry = NULL;
    UINT4               u4TSNew = 0;
    UINT4               u4TempOffset = 0;
    UINT4               u4SeNDOptCount = 0;
    UINT4               u4SeNDCountCheck = 0;
    UINT1               au1PubKey[IP6_SEND_CGA_BYTES];
    UINT1               u1SeNDFlag = 0;

    MEMSET (&CgaOptions, 0, sizeof (tCgaOptions));
    MEMSET (au1PubKey, 0, IP6_SEND_CGA_BYTES);
    MEMSET (&SysPreciseTime, 0, sizeof (tUtlSysPreciseTime));

    u4TempOffset = IP6_BUF_READ_OFFSET (pBuf) - sizeof (tNd6RoutAdv);
    CgaOptions.pu1DerPubKey = au1PubKey;

    if ((IS_ALL_NODES_MULTI (pIp6->dstAddr)))
    {
        u4SeNDCountCheck = ND6_SEND_UNSOL_OPT_COUNT;
    }

    if (pIf6->u1Ipv6IfFwdOperStatus == IP6_IF_FORW_ENABLE)
    {
        UINT1               u1Copy = 0;
        UINT4               u4ExtOffset = 0;
        tNd6ExtHdr         *pNd6Ext;
        tNd6MtuExt          nd6Mtu, *p_nd6_mtu;
        tNd6PrefixExt       nd6Prefix, *pNd6Prefix;

#ifdef HA_WANTED
        tNd6AdvInterval     nd6AdvInt, *p_nd6_adv_int;
        tNd6HaInfo          nd6HAInfo, *pNd6HaInfo;
#else
        UNUSED_PARAM (pu1HaInfoPresent);
#endif
        UNUSED_PARAM (u2RtrLife);
        UNUSED_PARAM (pIp6);
        UNUSED_PARAM (pIf6);
        UNUSED_PARAM (u1PrefAdvFlag);

        /* Extract the Extension Options */
        while (u4ExtnsLen)
        {
            if ((pNd6Ext =
                 Nd6ExtractExtnHdrInCxt (pIf6->pIp6Cxt, pBuf)) == NULL)
            {
                return (IP6_FAILURE);
            }
            u4ExtOffset = IP6_BUF_READ_OFFSET (pBuf) - IP6_TYPE_LEN_BYTES;

            switch (pNd6Ext->u1Type)
            {
                case ND6_SRC_LLA_EXT:
                    /* ND cache entries should not be updated using
                       SLLA option  */

                    IP6_BUF_READ_OFFSET (pBuf) -= IP6_TYPE_LEN_BYTES;

                    IP6_BUF_READ_OFFSET (pBuf) = IP6_BUF_READ_OFFSET (pBuf) +
                        sizeof (tNd6AddrExt);
                    u4ExtnsLen -= sizeof (tNd6AddrExt);
                    (IP6_SEND_IF_IN_STATS (pIf6, COUNT_RA)).u4SrcLinkAddrPkts++;
                    break;
                case ND6_MTU_EXT:
                    if ((u4ExtnsLen < (UINT4) (pNd6Ext->u1Len * 8)) ||
                        (pNd6Ext->u1Len != ND6_MTU_EXT_LEN))

                    {
                        IP6_TRC_ARG1 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                                      DATA_PATH_TRC, ND6_NAME,
                                      "ND6: Nd6ProcessRoutAdvExtns:NS SRC LLA "
                                      "Extn - Invalid Len %d\n",
                                      pNd6Ext->u1Len);
                        (IP6_SEND_IF_DROP_STATS
                         (pIf6, COUNT_RA)).u4MtuOptPkts++;
                        (IP6_IF_STATS (pIf6))->u4NdSecureDroppedPkts++;
                        return (IP6_FAILURE);
                    }

                    if ((p_nd6_mtu = (tNd6MtuExt *) Ip6BufRead
                         (pBuf, (UINT1 *) &nd6Mtu, u4ExtOffset,
                          sizeof (tNd6MtuExt), u1Copy)) == NULL)
                    {
                        IP6_TRC_ARG2 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                                      BUFFER_TRC, ND6_NAME,
                                      "ND6: Nd6ProcessRoutAdvExtns:Process NS Extn"
                                      " - BufRead Failed! BufPtr %p Offset %d\n",
                                      pBuf, u4ExtOffset);
                        IP6_TRC_ARG3 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                                      BUFFER_TRC, IP6_NAME,
                                      "%s buffer error occurred, Type = %d Module "
                                      "= 0x%X Bufptr = %p ", ERROR_FATAL_STR,
                                      ERR_BUF_READ, pBuf);
                        (IP6_SEND_IF_DROP_STATS
                         (pIf6, COUNT_RA)).u4MtuOptPkts++;
                        (IP6_IF_STATS (pIf6))->u4NdSecureDroppedPkts++;
                        return (IP6_FAILURE);
                    }

                    if (HTONL (p_nd6_mtu->u4Mtu) < IP6_MIN_MTU)
                    {

                        IP6_TRC_ARG1 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                                      DATA_PATH_TRC, ND6_NAME,
                                      "ND6: Nd6ProcessRoutAdvExtns:RA MTU Extn - "
                                      "Invalid MTU %d\n", p_nd6_mtu->u4Mtu);
                        (IP6_SEND_IF_DROP_STATS
                         (pIf6, COUNT_RA)).u4MtuOptPkts++;
                        (IP6_IF_STATS (pIf6))->u4NdSecureDroppedPkts++;
                        return (IP6_FAILURE);

                    }
                    IP6_BUF_READ_OFFSET (pBuf) -= IP6_TYPE_LEN_BYTES;
                    u4ExtnsLen -= sizeof (tNd6AddrExt);
                    (IP6_SEND_IF_IN_STATS (pIf6, COUNT_RA)).u4MtuOptPkts++;
                    break;

                case ND6_SEND_CGA_OPT:
                    IP6_BUF_READ_OFFSET (pBuf) -= IP6_TYPE_LEN_BYTES;
                    if (IP6_TRUE == IF_SEND_ENABLED (pIf6))
                    {
                        if (Nd6SeNDProcessCgaOption (pIf6, pIp6, pBuf,
                                                     pNd6Ext->u1Len,
                                                     &CgaOptions) ==
                            IP6_FAILURE)
                        {
                            IP6_TRC_ARG (pIf6->pIp6Cxt->u4ContextId,
                                         ND6_MOD_TRC, BUFFER_TRC,
                                         ND6_NAME, "Nd6ProcessRoutAdvExtns:"
                                         " Cga processing failed\n");
                            (IP6_SEND_IF_DROP_STATS
                             (pIf6, COUNT_RA)).u4CgaOptPkts++;
                            (IP6_IF_STATS (pIf6))->u4NdSecureDroppedPkts++;
                            return IP6_FAILURE;
                        }
                        (IP6_SEND_IF_IN_STATS (pIf6, COUNT_RA)).u4CgaOptPkts++;
                        u4SeNDOptCount++;
                    }
                    else
                    {
                        IP6_BUF_READ_OFFSET (pBuf) +=
                            (UINT4) (pNd6Ext->u1Len * BYTES_IN_OCTECT);
                    }
                    /* extension length is in unit of 8 octets */
                    u4ExtnsLen -= (UINT4) (pNd6Ext->u1Len * BYTES_IN_OCTECT);
                    break;

                case ND6_SEND_TIMESTAMP_OPT:

                    IP6_BUF_READ_OFFSET (pBuf) -= IP6_TYPE_LEN_BYTES;
                    if (IP6_TRUE == IF_SEND_ENABLED (pIf6))
                    {
                        if (Nd6SeNDProcessTSOption (pIf6, pIp6, pBuf,
                                                    &u4TSNew) == IP6_FAILURE)
                        {
                            IP6_TRC_ARG (pIf6->pIp6Cxt->u4ContextId,
                                         ND6_MOD_TRC, BUFFER_TRC,
                                         ND6_NAME, "Nd6ProcessRoutAdvExtns:"
                                         " Timestamp processing failed\n");
                            (IP6_SEND_IF_DROP_STATS
                             (pIf6, COUNT_RA)).u4TimeStampOptPkts++;
                            (IP6_IF_STATS (pIf6))->u4NdSecureDroppedPkts++;
                            return IP6_FAILURE;
                        }
                        u4SeNDOptCount++;
                        (IP6_SEND_IF_IN_STATS (pIf6, COUNT_RA)).
                            u4TimeStampOptPkts++;
                    }
                    else
                    {
                        IP6_BUF_READ_OFFSET (pBuf) +=
                            (UINT4) (pNd6Ext->u1Len * BYTES_IN_OCTECT);
                    }
                    /* extension length is in unit of 8 octets */
                    u4ExtnsLen -= (UINT4) (pNd6Ext->u1Len * BYTES_IN_OCTECT);
                    break;

                case ND6_SEND_NONCE_OPT:
                    u1SeNDFlag = ND6_SEND_RA_MSG;
                    IP6_BUF_READ_OFFSET (pBuf) -= IP6_TYPE_LEN_BYTES;
                    u4SeNDCountCheck = ND6_SEND_SOL_OPT_COUNT;
                    if (IP6_TRUE == IF_SEND_ENABLED (pIf6))
                    {
                        if (Nd6SeNDProcessNonceOption (pIf6, pIp6, pBuf,
                                                       NULL,
                                                       u1SeNDFlag) ==
                            IP6_FAILURE)
                        {
                            IP6_TRC_ARG (pIf6->pIp6Cxt->u4ContextId,
                                         ND6_MOD_TRC, BUFFER_TRC,
                                         ND6_NAME, "Nd6ProcessRoutAdvExtns:"
                                         " Nonce processing failed\n");
                            (IP6_SEND_IF_DROP_STATS
                             (pIf6, COUNT_RA)).u4NonceOptPkts++;
                            (IP6_IF_STATS (pIf6))->u4NdSecureDroppedPkts++;
                            return IP6_FAILURE;
                        }
                        u4SeNDOptCount++;
                        (IP6_SEND_IF_IN_STATS (pIf6, COUNT_RA)).
                            u4NonceOptPkts++;
                    }
                    else
                    {
                        IP6_BUF_READ_OFFSET (pBuf) +=
                            (UINT4) (pNd6Ext->u1Len * BYTES_IN_OCTECT);
                    }
                    /* extension length is in unit of 8 octets */
                    u4ExtnsLen -= (UINT4) (pNd6Ext->u1Len * BYTES_IN_OCTECT);
                    break;

                case ND6_SEND_RSA_SIGN_OPT:
                    IP6_BUF_READ_OFFSET (pBuf) -= IP6_TYPE_LEN_BYTES;
                    if (IP6_TRUE == IF_SEND_ENABLED (pIf6))
                    {
                        if (Nd6SeNDProcessRsaOption (pIf6, pIp6, pBuf,
                                                     pNd6Ext->u1Len,
                                                     u4TempOffset,
                                                     &CgaOptions) ==
                            IP6_FAILURE)
                        {
                            IP6_TRC_ARG (pIf6->pIp6Cxt->u4ContextId,
                                         ND6_MOD_TRC, BUFFER_TRC,
                                         ND6_NAME, "Nd6ProcessRoutAdvExtns:"
                                         " RSA processing failed\n");
                            (IP6_SEND_IF_DROP_STATS
                             (pIf6, COUNT_RA)).u4RsaOptPkts++;
                            (IP6_IF_STATS (pIf6))->u4NdSecureDroppedPkts++;
                            return IP6_FAILURE;
                        }
                        u4SeNDOptCount++;
                        (IP6_SEND_IF_IN_STATS (pIf6, COUNT_RA)).u4RsaOptPkts++;
                    }
                    else
                    {
                        IP6_BUF_READ_OFFSET (pBuf) +=
                            (UINT4) (pNd6Ext->u1Len * BYTES_IN_OCTECT);
                    }
                    /* extension length is in unit of 8 octets */
                    u4ExtnsLen -= (UINT4) (pNd6Ext->u1Len * BYTES_IN_OCTECT);
                    break;

                case ND6_PREFIX_EXT:
                    if ((u4ExtnsLen < (UINT4) (pNd6Ext->u1Len * 8)) ||
                        (pNd6Ext->u1Len != ND6_PREFIX_EXT_LEN))

                    {
                        IP6_TRC_ARG1 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                                      DATA_PATH_TRC, ND6_NAME,
                                      "ND6: Nd6ProcessRoutAdvExtns:NS SRC LLA Extn"
                                      " - Invalid Len %d\n", pNd6Ext->u1Len);
                        (IP6_SEND_IF_DROP_STATS
                         (pIf6, COUNT_RA)).u4PrefixOptPkts++;
                        (IP6_IF_STATS (pIf6))->u4NdSecureDroppedPkts++;
                        return (IP6_FAILURE);
                    }

                    if ((pNd6Prefix = (tNd6PrefixExt *) Ip6BufRead
                         (pBuf, (UINT1 *) &nd6Prefix, u4ExtOffset,
                          sizeof (tNd6PrefixExt), u1Copy)) == NULL)
                    {
                        IP6_TRC_ARG2 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                                      BUFFER_TRC, ND6_NAME,
                                      "ND6: Nd6ProcessRoutAdvExtns:Process NS Extn"
                                      " - BufRead Failed! BufPtr %p Offset%d\n",
                                      pBuf, u4ExtOffset);
                        IP6_TRC_ARG3 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                                      BUFFER_TRC, IP6_NAME,
                                      "%s buffer error occurred, Type = %d Module "
                                      "= 0x%X Bufptr = %p ", ERROR_FATAL_STR,
                                      ERR_BUF_READ, pBuf);
                        (IP6_SEND_IF_DROP_STATS
                         (pIf6, COUNT_RA)).u4PrefixOptPkts++;
                        (IP6_IF_STATS (pIf6))->u4NdSecureDroppedPkts++;
                        return (IP6_FAILURE);
                    }

#ifdef HA_WANTED

                    Mip6AddGlobAddrHAList (pNd6Prefix, pIf6, pIp6);
#endif

                    IP6_BUF_READ_OFFSET (pBuf) -= IP6_TYPE_LEN_BYTES;
                    u4ExtnsLen -= sizeof (tNd6PrefixExt);
                    (IP6_SEND_IF_IN_STATS (pIf6, COUNT_RA)).u4PrefixOptPkts++;
                    UNUSED_PARAM (pNd6Prefix);

                    break;

#ifdef HA_WANTED
                case ADV_INTERVAL_OPTION:

                    if ((u4ExtnsLen < (UINT4) (pNd6Ext->u1Len * 8))
                        || (pNd6Ext->u1Len != ND6_ADV_INTERVAL_OPTION_LEN))

                    {
                        return (IP6_FAILURE);
                    }

                    if ((p_nd6_adv_int = (tNd6AdvInterval *) Ip6BufRead
                         (pBuf, (UINT1 *) &nd6AdvInt, u4ExtOffset,
                          sizeof (tNd6AdvInterval), u1Copy)) == NULL)
                    {
                        IP6_TRC_ARG2 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                                      BUFFER_TRC, ND6_NAME,
                                      "ND6: Nd6ProcessRoutAdvExtns:Process NS Extn"
                                      " - BufRead Failed! BufPtr %p Offset%d\n",
                                      pBuf, u4ExtOffset);
                        IP6_TRC_ARG3 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                                      BUFFER_TRC, IP6_NAME,
                                      "%s buffer error occurred, Type = %d Module "
                                      "= 0x%X Bufptr = %p ", ERROR_FATAL_STR,
                                      ERR_BUF_READ, pBuf);
                        return (IP6_FAILURE);
                    }
                    IP6_BUF_READ_OFFSET (pBuf) -= IP6_TYPE_LEN_BYTES;
                    u4ExtnsLen -= sizeof (tNd6AdvInterval);
                    break;

                case HA_INFO_OPTION:

                    if ((u4ExtnsLen < (UINT4) (pNd6Ext->u1Len * 8))
                        || (pNd6Ext->u1Len != ND6_HA_INFO_OPTION_LEN))
                    {
                        IP6_TRC_ARG2 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                                      BUFFER_TRC, ND6_NAME,
                                      "ND6: Nd6ProcessRoutAdvExtns:Process NS Extn"
                                      " - BufRead Failed! BufPtr %p Offset%d\n",
                                      pBuf, u4ExtOffset);
                        IP6_TRC_ARG3 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                                      BUFFER_TRC, IP6_NAME,
                                      "%s buffer error occurred, Type = %d Module "
                                      "= 0x%X Bufptr = %p ", ERROR_FATAL_STR,
                                      ERR_BUF_READ, pBuf);
                        return (IP6_FAILURE);
                    }

                    if ((pNd6HaInfo =
                         (tNd6HaInfo *) Ip6BufRead (pBuf, (UINT1 *) &nd6HAInfo,
                                                    u4ExtOffset,
                                                    sizeof (tNd6HaInfo),
                                                    u1Copy)) == NULL)
                    {
                        IP6_TRC_ARG2 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                                      BUFFER_TRC, ND6_NAME,
                                      "ND6: Nd6ProcessRoutAdvExtns:Process NS Extn"
                                      " - BufRead Failed! BufPtr %p Offset%d\n",
                                      pBuf, u4ExtOffset);
                        IP6_TRC_ARG3 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                                      BUFFER_TRC, IP6_NAME,
                                      "%s buffer error occurred, Type = %d Module "
                                      "= 0x%X Bufptr = %p ", ERROR_FATAL_STR,
                                      ERR_BUF_READ, pBuf);
                        return (IP6_FAILURE);
                    }

                    /* If the RA Contains a home agent Information option,
                     * then the lifetime is taken from the HA lifetime
                     * field in the option
                     */
                    if (!IS_ADDR_UNSPECIFIED (pIp6->srcAddr))
                    {

                        /*
                         * Source address of this RA is not unspecified,
                         * So it can be added in the default router list.
                         */

                    }

                    /*
                     * if the link-local addr of the home agent
                     * sending this RA is already present in the
                     * receiving home agent's HA List, Reset its
                     * lifetime & preference to the values determined above
                     */

                    Mip6ProcessHAInfoOpt (pIf6, pIp6, pNd6HaInfo);
                    *pu1HaInfoPresent = HOME_AGT_INFO_OPT;
                    IP6_BUF_READ_OFFSET (pBuf) -= IP6_TYPE_LEN_BYTES;
                    u4ExtnsLen -= sizeof (tNd6HaInfo);
                    break;
#endif

                default:
                    return (IP6_SUCCESS);

            }
        }
        /* If SeND is enabled in full secure mode then all the four options
         * should present, and in mixed mode either all options should present
         * or no SeND options should present in the packet.
         * if it is not then discard the packet
         */

        if (((ND6_SECURE_ENABLE == pIf6->u1SeNDStatus)
             && u4SeNDOptCount != u4SeNDCountCheck)
            || ((ND6_SECURE_MIXED == pIf6->u1SeNDStatus)
                && (!((u4SeNDOptCount == IP6_ZERO)
                      || (u4SeNDOptCount == u4SeNDCountCheck)))))
        {
            (IP6_IF_STATS (pIf6))->u4NdSecureInvalidPkts++;
            return (IP6_FAILURE);
        }

        *pu1SecureFlag = ND6_UNSECURE_ENTRY;
        if ((ND6_SECURE_MIXED != pIf6->u1SeNDStatus)
            && (u4SeNDOptCount == u4SeNDCountCheck))
        {
            *pu1SecureFlag = ND6_SECURE_ENTRY;
        }
        return (IP6_SUCCESS);
    }
    else
    {
        UINT1               u1Copy = 0, u1pNd6PrefixIndex = 0;
        UINT1               u1Cntr = 0;
        UINT4               u4ExtOffset = 0;
        tNd6ExtHdr         *pNd6Ext = NULL;
        tNd6MtuExt          Nd6Mtu, *pNd6Mtu = NULL;
        tNd6PrefixExt       nd6Prefix, *pNd6Prefix[ND6_PREFIX_CNT];
        tNd6AddrExt         Nd6Lla, *pNd6Lla = NULL;
#ifdef MN_WANTED
        tNd6HaInfo         *pNd6HaInfo = NULL;
        tNd6HaInfo          nd6HAInfo;
#endif
        UNUSED_PARAM (u1PrefAdvFlag);
        UNUSED_PARAM (u2RtrLife);
        UNUSED_PARAM (pu1HaInfoPresent);

        /* Extract the Extension Options */
        while (u4ExtnsLen)
        {
            if ((pNd6Ext =
                 Nd6ExtractExtnHdrInCxt (pIf6->pIp6Cxt, pBuf)) == NULL)
            {
                return (IP6_FAILURE);
            }
            u4ExtOffset = IP6_BUF_READ_OFFSET (pBuf) - IP6_TYPE_LEN_BYTES;
            IP6_BUF_READ_OFFSET (pBuf) = u4ExtOffset;

            switch (pNd6Ext->u1Type)
            {

                case ND6_MTU_EXT:
                    if ((u4ExtnsLen < (UINT4) pNd6Ext->u1Len * BYTES_IN_OCTECT)
                        || (pNd6Ext->u1Len != ND6_MTU_EXT_LEN))

                    {
                        IP6_TRC_ARG1 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                                      DATA_PATH_TRC, ND6_NAME,
                                      "ND6: Nd6ProcessRoutAdvExtns:NS SRC LLA "
                                      "Extn - Invalid Len %d\n",
                                      pNd6Ext->u1Len);
                        (IP6_SEND_IF_DROP_STATS
                         (pIf6, COUNT_RA)).u4MtuOptPkts++;
                        (IP6_IF_STATS (pIf6))->u4NdSecureDroppedPkts++;
                        return (IP6_FAILURE);
                    }

                    if ((pNd6Mtu = (tNd6MtuExt *) Ip6BufRead
                         (pBuf, (UINT1 *) &Nd6Mtu, u4ExtOffset,
                          sizeof (tNd6MtuExt), u1Copy)) == NULL)
                    {
                        IP6_TRC_ARG2 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                                      BUFFER_TRC, ND6_NAME,
                                      "ND6: Nd6ProcessRoutAdvExtns:Process NS Extn"
                                      " - BufRead Failed! BufPtr %p Offset %d\n",
                                      pBuf, u4ExtOffset);
                        IP6_TRC_ARG3 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                                      BUFFER_TRC, IP6_NAME,
                                      "%s buffer error occurred, Type = %d Module ="
                                      " 0x%X Bufptr = %p ", ERROR_FATAL_STR,
                                      ERR_BUF_READ, pBuf);
                        (IP6_SEND_IF_DROP_STATS
                         (pIf6, COUNT_RA)).u4MtuOptPkts++;
                        (IP6_IF_STATS (pIf6))->u4NdSecureDroppedPkts++;
                        return (IP6_FAILURE);
                    }
                    if (HTONL (pNd6Mtu->u4Mtu) < IP6_MIN_MTU)
                    {

                        IP6_TRC_ARG1 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                                      DATA_PATH_TRC, ND6_NAME,
                                      "ND6: Nd6ProcessRoutAdvExtns:RA MTU Extn - "
                                      "Invalid MTU %d\n", pNd6Mtu->u4Mtu);
                        (IP6_SEND_IF_DROP_STATS
                         (pIf6, COUNT_RA)).u4MtuOptPkts++;
                        (IP6_IF_STATS (pIf6))->u4NdSecureDroppedPkts++;
                        return (IP6_FAILURE);

                    }
                    (IP6_SEND_IF_IN_STATS (pIf6, COUNT_RA)).u4MtuOptPkts++;
                    u4ExtnsLen -= sizeof (tNd6MtuExt);
                    break;
                case ND6_PREFIX_EXT:

                    if ((u4ExtnsLen < (UINT4) pNd6Ext->u1Len * BYTES_IN_OCTECT)
                        || (pNd6Ext->u1Len != ND6_PREFIX_EXT_LEN))

                    {
                        IP6_TRC_ARG1 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                                      DATA_PATH_TRC, ND6_NAME,
                                      "ND6: Nd6ProcessRoutAdvExtns:RS PREFIX Extn "
                                      "- Invalid Len %d\n", pNd6Ext->u1Len);
                        (IP6_SEND_IF_DROP_STATS
                         (pIf6, COUNT_RA)).u4PrefixOptPkts++;
                        (IP6_IF_STATS (pIf6))->u4NdSecureDroppedPkts++;
                        return (IP6_FAILURE);
                    }

                    if ((pNd6Prefix[u1pNd6PrefixIndex] =
                         (tNd6PrefixExt *) Ip6BufRead (pBuf,
                                                       (UINT1 *) &nd6Prefix,
                                                       u4ExtOffset,
                                                       sizeof (tNd6PrefixExt),
                                                       u1Copy)) == NULL)
                    {
                        IP6_TRC_ARG2 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                                      BUFFER_TRC, ND6_NAME,
                                      "ND6: Nd6ProcessRoutAdvExtns:Process RS Extn "
                                      "- BufRead Failed!BufPtr %p Offset%d\n",
                                      pBuf, u4ExtOffset);
                        IP6_TRC_ARG3 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                                      BUFFER_TRC, IP6_NAME,
                                      "%s buffer error occurred, Type = %d Module ="
                                      " 0x%X Bufptr = %p ", ERROR_FATAL_STR,
                                      ERR_BUF_READ, pBuf);
                        (IP6_SEND_IF_DROP_STATS
                         (pIf6, COUNT_RA)).u4PrefixOptPkts++;
                        (IP6_IF_STATS (pIf6))->u4NdSecureDroppedPkts++;
                        return (IP6_FAILURE);
                    }
                    (IP6_SEND_IF_IN_STATS (pIf6, COUNT_RA)).u4PrefixOptPkts++;
                    u1pNd6PrefixIndex++;
                    u4ExtnsLen -= sizeof (tNd6PrefixExt);
                    break;
                case ND6_SRC_LLA_EXT:
                    if (Nd6ProcessSLLAOption
                        (u4ExtnsLen, pNd6Ext, pIf6, &Nd6Lla, &pNd6Lla,
                         pBuf) == IP6_FAILURE)
                    {
                        IP6_TRC_ARG (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                                     DATA_PATH_TRC, ND6_NAME,
                                     "ND6: Nd6ProcessRoutSolExtns:Validation of NS "
                                     "Failed - Invalid SLLA \n");
                        (IP6_SEND_IF_DROP_STATS
                         (pIf6, COUNT_RA)).u4SrcLinkAddrPkts++;
                        (IP6_IF_STATS (pIf6))->u4NdSecureDroppedPkts++;
                        return IP6_FAILURE;
                    }
                    (IP6_SEND_IF_IN_STATS (pIf6, COUNT_RA)).u4SrcLinkAddrPkts++;
                    u4ExtnsLen -= sizeof (tNd6AddrExt);
                    break;
                case ND6_ADV_INTERVAL_OPT:
#ifdef MN_WANTED

                    if (Nd6ProcessAdvIntervalOpt (pBuf,
                                                  IP6_BUF_READ_OFFSET (pBuf)) ==
                        IP6_FAILURE)
                    {
                        IP6_TRC_ARG (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                                     DATA_PATH_TRC, ND6_NAME,
                                     "ND6: Nd6ProcessRoutAdvExtns:Invalid Advertisement Interval Option \n");
                        return IP6_FAILURE;
                    }
#endif
                    u4ExtnsLen -= sizeof (tNd6AdvInterval);

                    break;
                case HA_INFO_OPTION:
#ifdef MN_WANTED
                    if ((u4ExtnsLen <
                         (UINT4) (pNd6Ext->u1Len * BYTES_IN_OCTECT))
                        || (pNd6Ext->u1Len != (UINT1) ND6_HA_INFO_OPTION_LEN))
                    {
                        return (IP6_FAILURE);
                    }

                    if ((pNd6HaInfo =
                         (tNd6HaInfo *) Ip6BufRead (pBuf, (UINT1 *) &nd6HAInfo,
                                                    u4ExtOffset,
                                                    sizeof (tNd6HaInfo),
                                                    u1Copy)) == NULL)
                    {
                        return (IP6_FAILURE);
                    }
#endif
                    u4ExtnsLen -= sizeof (tNd6HaInfo);
                    break;

                case ND6_SEND_CGA_OPT:

                    if (IP6_TRUE == IF_SEND_ENABLED (pIf6))
                    {
                        if (Nd6SeNDProcessCgaOption (pIf6, pIp6, pBuf,
                                                     pNd6Ext->u1Len,
                                                     &CgaOptions) ==
                            IP6_FAILURE)
                        {
                            IP6_TRC_ARG (pIf6->pIp6Cxt->u4ContextId,
                                         ND6_MOD_TRC, BUFFER_TRC,
                                         ND6_NAME, "Nd6ProcessRoutAdvExtns:"
                                         " Cga processing failed\n");
                            (IP6_SEND_IF_DROP_STATS
                             (pIf6, COUNT_RA)).u4CgaOptPkts++;
                            (IP6_IF_STATS (pIf6))->u4NdSecureDroppedPkts++;
                            return IP6_FAILURE;
                        }
                        u4SeNDOptCount++;
                        (IP6_SEND_IF_IN_STATS (pIf6, COUNT_RA)).u4CgaOptPkts++;
                    }
                    else
                    {
                        IP6_BUF_READ_OFFSET (pBuf) +=
                            (UINT4) (pNd6Ext->u1Len * BYTES_IN_OCTECT);
                    }
                    /* extension length is in unit of 8 octets */
                    u4ExtnsLen -= (UINT4) (pNd6Ext->u1Len * BYTES_IN_OCTECT);
                    break;

                case ND6_SEND_TIMESTAMP_OPT:

                    if (IP6_TRUE == IF_SEND_ENABLED (pIf6))
                    {
                        if (Nd6SeNDProcessTSOption (pIf6, pIp6, pBuf,
                                                    &u4TSNew) == IP6_FAILURE)
                        {
                            IP6_TRC_ARG (pIf6->pIp6Cxt->u4ContextId,
                                         ND6_MOD_TRC, BUFFER_TRC,
                                         ND6_NAME, "Nd6ProcessRoutAdvExtns:"
                                         " Timestamp processing failed\n");
                            (IP6_SEND_IF_DROP_STATS
                             (pIf6, COUNT_RA)).u4TimeStampOptPkts++;
                            (IP6_IF_STATS (pIf6))->u4NdSecureDroppedPkts++;
                            return IP6_FAILURE;
                        }
                        u4SeNDOptCount++;
                        (IP6_SEND_IF_IN_STATS (pIf6, COUNT_RA)).
                            u4TimeStampOptPkts++;
                    }
                    else
                    {
                        IP6_BUF_READ_OFFSET (pBuf) +=
                            (UINT4) (pNd6Ext->u1Len * BYTES_IN_OCTECT);
                    }
                    /* extension length is in unit of 8 octets */
                    u4ExtnsLen -= (UINT4) (pNd6Ext->u1Len * BYTES_IN_OCTECT);
                    break;

                case ND6_SEND_NONCE_OPT:
                    u1SeNDFlag = ND6_SEND_RA_MSG;
                    u4SeNDCountCheck = ND6_SEND_SOL_OPT_COUNT;
                    if (IP6_TRUE == IF_SEND_ENABLED (pIf6))
                    {
                        if (Nd6SeNDProcessNonceOption (pIf6, pIp6, pBuf,
                                                       NULL,
                                                       u1SeNDFlag) ==
                            IP6_FAILURE)
                        {

                            IP6_TRC_ARG (pIf6->pIp6Cxt->u4ContextId,
                                         ND6_MOD_TRC, BUFFER_TRC,
                                         ND6_NAME, "Nd6ProcessRoutAdvExtns:"
                                         " Nonce processing failed\n");
                            (IP6_SEND_IF_DROP_STATS
                             (pIf6, COUNT_RA)).u4NonceOptPkts++;
                            (IP6_IF_STATS (pIf6))->u4NdSecureDroppedPkts++;
                            return IP6_FAILURE;
                        }
                        (IP6_SEND_IF_IN_STATS (pIf6, COUNT_RA)).
                            u4NonceOptPkts++;
                        u4SeNDOptCount++;
                    }
                    else
                    {
                        IP6_BUF_READ_OFFSET (pBuf) +=
                            (UINT4) (pNd6Ext->u1Len * BYTES_IN_OCTECT);
                    }
                    /* extension length is in unit of 8 octets */
                    u4ExtnsLen -= (UINT4) (pNd6Ext->u1Len * BYTES_IN_OCTECT);
                    break;

                case ND6_SEND_RSA_SIGN_OPT:

                    if (IP6_TRUE == IF_SEND_ENABLED (pIf6))
                    {
                        if (Nd6SeNDProcessRsaOption (pIf6, pIp6, pBuf,
                                                     pNd6Ext->u1Len,
                                                     u4TempOffset,
                                                     &CgaOptions) ==
                            IP6_FAILURE)
                        {
                            IP6_TRC_ARG (pIf6->pIp6Cxt->u4ContextId,
                                         ND6_MOD_TRC, BUFFER_TRC,
                                         ND6_NAME, "Nd6ProcessRoutAdvExtns:"
                                         " RSA processing failed\n");
                            (IP6_SEND_IF_DROP_STATS
                             (pIf6, COUNT_RA)).u4RsaOptPkts++;
                            (IP6_IF_STATS (pIf6))->u4NdSecureDroppedPkts++;
                            return IP6_FAILURE;
                        }
                        (IP6_SEND_IF_IN_STATS (pIf6, COUNT_RA)).u4RsaOptPkts++;
                        u4SeNDOptCount++;
                    }
                    else
                    {
                        IP6_BUF_READ_OFFSET (pBuf) +=
                            (UINT4) (pNd6Ext->u1Len * BYTES_IN_OCTECT);
                    }
                    /* extension length is in unit of 8 octets */
                    u4ExtnsLen -= (UINT4) (pNd6Ext->u1Len * BYTES_IN_OCTECT);
                    break;

                default:
                    /* Any other options should just be ignored  */
                    IP6_BUF_READ_OFFSET (pBuf) +=
                        (UINT4) (pNd6Ext->u1Len * BYTES_IN_OCTECT);
                    u4ExtnsLen -= (UINT4) (pNd6Ext->u1Len * BYTES_IN_OCTECT);
                    break;
            }

        }

        /* If SeND is enabled in full secure mode then all the four options
         * should present, and in mixed mode either all options should present
         * or no SeND options should present in the packet.
         * if it is not then discard the packet
         */

        if (((ND6_SECURE_ENABLE == pIf6->u1SeNDStatus)
             && u4SeNDOptCount != u4SeNDCountCheck)
            || ((ND6_SECURE_MIXED == pIf6->u1SeNDStatus)
                && (!((u4SeNDOptCount == IP6_ZERO)
                      || (u4SeNDOptCount == u4SeNDCountCheck)))))
        {
            (IP6_IF_STATS (pIf6))->u4NdSecureInvalidPkts++;
            return (IP6_FAILURE);
        }

        *pu1SecureFlag = ND6_UNSECURE_ENTRY;
        if ((ND6_SECURE_MIXED != pIf6->u1SeNDStatus)
            && (u4SeNDOptCount == u4SeNDCountCheck))
        {
            *pu1SecureFlag = ND6_SECURE_ENTRY;
        }
        if (!(IS_ADDR_UNSPECIFIED (pIp6->srcAddr)))
        {
            /* 
             * Update the NDCache entry if the source is not 
             * an Unspecified address 
             */
            if (pNd6Lla != NULL)
            {
                if (Nd6UpdateCache (pIf6, &pIp6->srcAddr,
                                    pNd6Lla->lladdr, IP6_ENET_ADDR_LEN,
                                    ND6_NEIGHBOR_SOLICITATION,
                                    NULL, *pu1SecureFlag) == IP6_FAILURE)
                {
                    IP6_TRC_ARG (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                                 ALL_FAILURE_TRC, ND6_NAME,
                                 "ND6:Nd6ProcessSLLAOption: SRC LLA Extn - Update "
                                 "Cache Failed \n");
                    return (IP6_FAILURE);
                }
                if (IP6_TRUE == IF_SEND_ENABLED (pIf6))
                {
                    if (u4TSNew != 0)
                    {
                        pNd6cEntry = Nd6IsCacheForAddr (pIf6, &pIp6->srcAddr);
                        if (NULL != pNd6cEntry)
                        {
                            TmrGetPreciseSysTime (&SysPreciseTime);
                            MEMCPY (&pNd6cEntry->u4SendRDLast,
                                    &(SysPreciseTime.u4Sec), sizeof (UINT4));
                            pNd6cEntry->u4SendTSLast = u4TSNew;
                        }
                    }
                }
            }
        }
#ifdef MN_WANTED
        if (!(u1PrefAdvFlag))
        {
            /* 
             * MTU option included with prefix adv does not
             * have any meaning. So other validity checks can
             * be done. MTU need not be copied
             */
#endif
            if (pNd6Mtu != NULL)
            {
                if (pIf6->u4Mtu > NTOHL (pNd6Mtu->u4Mtu))
                {
                    pIf6->u4Mtu = NTOHL (pNd6Mtu->u4Mtu);
                }
            }
#ifdef MN_WANTED
        }
#endif
        for (u1Cntr = 0; u1Cntr < u1pNd6PrefixIndex; u1Cntr++)
        {
            if (pNd6Prefix[u1Cntr] != NULL)
            {
#ifdef MIP6_WANTED
                if (u1PrefAdvFlag)
                {
                    if (MNProcessPrefixOption (pNd6Prefix[u1Cntr], pIp6, pIf6)
                        == IP6_FAILURE)
                        return IP6_FAILURE;
                }
                else
#endif
                    /* As part of SeND, prefix option processing and updation
                     * should not be performed if the existing entry is secured
                     * incoming update is an unsecured one, but this is taken care 
                     * updatecache entry itself. If the check fails function will 
                     * return, so no need to check here.
                     */
                if (Nd6ProcessPrefixOption (pNd6Prefix[u1Cntr], pIf6, NULL)
                        == IP6_FAILURE)
                {
                    return IP6_FAILURE;
                }
#ifdef MIP6_WANTED
                if (Mip6AddGlobAddrHAList (pNd6Prefix[u1Cntr], pIf6, pIp6)
                    == IP6_FAILURE)
                {
                    return IP6_FAILURE;
                }
#endif
            }
        }

#ifdef MN_WANTED
        /* 
         * If the RA Contains a home agent Information option,
         * then the lifetime is taken from the HA lifetime
         * field in the option
         */

        /*
         * if the link-local addr of the home agent
         * sending this RA is already present in the
         * receiving home agent's HA List, Reset its
         * lifetime & preference to the values determined above
         */

        if (pNd6HaInfo != NULL)
        {
            Mip6ProcessHAInfoOpt (pIf6, pIp6, pNd6HaInfo);
            *pu1HaInfoPresent = HOME_AGT_INFO_OPT;
        }
#endif
        return (IP6_SUCCESS);
    }
}                                /*end of Nd6ProcessRoutAdvExtns */

/*****************************************************************************
* DESCRIPTION : Forms the ND Router Advertisement message and 
*               sends it to the lower layer for transmission
* 
* INPUTS      : pIf6        -  Pointer to the interface 
*               pSrcAddr6  -  Pointer to IPv6 source address
*               pDstAddr6  -  Pointer to IPv6 destination address
*               u1Flag      -  Flag indicating as whether router
*                               is enabled/ceased to advertise
*
* OUTPUTS     : None
*
* RETURNS     : IP6_SUCCESS       -  Sending of RA succeeds
*               IP6_FAILURE   -  Sending of RA fails
****************************************************************************/
INT4
Nd6SendRoutAdv (tNd6CacheEntry * pNd6cEntry, tIp6If * pIf6,
                tIp6Addr * pSrcAddr6, tIp6Addr * pDstAddr6, UINT1 u1Flag)
{
    UINT1               u1LlaLen = 0, lladdr[IP6_MAX_LLA_LEN];
    INT2                i2NumPrefixes = 0, i2_pref_per_ra = 0,
        i2_pref_seen = 0, i2PrefSent = 0;
    UINT4               u4Woffset, u4Nd6Size, u4_ip6hdr_size,
        u4_tot_ra_size = 0, u4_num_ra = 1, u4PktLen = 0;
    tTMO_SLL           *p_prefix_list = &pIf6->prefixlist;
    tTMO_SLL_NODE      *pCur = NULL;
    tCRU_BUF_CHAIN_HEADER *pBuf;
    UINT2               u2Offset = 0;
    tIp6RAEntry        *pRAEntry = NULL;
    UINT1               u1SeNDFlag = ND6_UNSOLICITED_MSG;
    UINT1               au1Zero[ND6_NONCE_LENGTH];
#ifdef VRRP_WANTED
    tIPvXAddr           IpAddr;
    INT4                i4VrId = 0;
#endif
    UINT4               u4RARoutInfoCount = 0;
    tIp6RARouteInfoNode *pRARouteInfo = NULL;
    tIp6RARouteInfoNode *pIp6NextRARouteInfo = NULL;
    MEMSET (au1Zero, 0, ND6_NONCE_LENGTH);

    if ((Ip6CheckRAStatus ((INT4) pIf6->u4Index)) != TRUE)
    {
        /* RA Disabled. Cant be an Advertisement Interface */
        return (IP6_FAILURE);
    }

#ifdef VRRP_WANTED
    IPVX_ADDR_CLEAR (&IpAddr);

    IPVX_ADDR_INIT_FROMV6 (IpAddr, pSrcAddr6);

    if (VrrpGetVridFromAssocIpAddress (pIf6->u4Index, IpAddr, &i4VrId)
        == VRRP_NOT_OK)
    {
        return IP6_FAILURE;
    }
#endif

    pRAEntry = Ip6RaTblGetEntry ((INT4) pIf6->u4Index);
    if (pRAEntry == NULL)
    {
        return (IP6_FAILURE);
    }

    u4Woffset = Ip6BufWoffset (ND6_MODULE);

    /*
     * Calculate the size of RA without including any extension options
     */
    u4Nd6Size = sizeof (tNd6RoutAdv);

    /* Assume the MAX size of IPv6 header with Extension Options */
    u4_ip6hdr_size = sizeof (tIp6Hdr);

    if (u1Flag & ND6_RA_ADV)
    {
        if (IP6_TRUE == IF_SEND_ENABLED (pIf6))
        {
            /*
             * If SeND is enabled, increase the pBuf size to accommodate
             * all the SeND options
             */
            u4_tot_ra_size += ND6_SEND_OPTIONS_LENGTH;
            if (MEMCMP (pIf6->au1RcvdNonce, au1Zero, ND6_NONCE_LENGTH) != 0)
            {
                u1SeNDFlag = ND6_SEND_SOL_RA_MSG;
            }
        }

        u4Nd6Size += sizeof (tNd6AddrExt);

        /* if RALinkMTU is not 0 add MTU Options also. */
        if (pRAEntry->u4LinkMTU != IP6_ZERO)
        {
            u4Nd6Size += sizeof (tNd6MtuExt);
        }

#ifdef HA_WANTED
        if (gu4mip6Entities == HOME_AGENT)
        {
            u4Nd6Size += sizeof (tNd6AdvInterval) + sizeof (tNd6HaInfo);
        }
#endif
        if (IP6_IF_SEND_PA (pIf6))
        {
            if ((i2NumPrefixes = (INT2) IP6_NUM_ADDR_PREFIX (pIf6)))
            {
                i2_pref_per_ra = i2NumPrefixes;
                u4_num_ra = (UINT4) Nd6GetPrefixPerRa
                    (pIf6, u4_ip6hdr_size + u4Nd6Size, &i2_pref_per_ra);

                /*
                 * The size of RA message is increased by the total size of
                 * prefixes allowed to be advertised in each RA
                 */
                u4Nd6Size += (UINT4) ((UINT4) sizeof (tNd6PrefixExt) *
                                      (UINT4) i2_pref_per_ra);
            }
        }

        /* 
         * The size of RA message is increased to acommodate the 
         * RA Route Information option
         */

        Ip6ifGetEthLladdr (pIf6, lladdr, &u1LlaLen);
    }

    RBTreeCount (pIf6->Ip6RARouteInfoTree, &u4RARoutInfoCount);
    if (u4RARoutInfoCount > 0)
    {
        u4_tot_ra_size += (UINT4) (ND6_MAX_RA_ROUTE_LENGTH * u4RARoutInfoCount);
    }
    u4_tot_ra_size += (u4Woffset + u4Nd6Size);

    while (u4_num_ra)
    {

        /* Allocate a Buffer for the IPv6 packet size of RA message */
        if ((pBuf = Ip6BufAlloc (pIf6->pIp6Cxt->u4ContextId,
                                 u4_tot_ra_size, u4Woffset,
                                 ND6_MODULE)) == NULL)
        {
            IP6_TRC_ARG (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                         BUFFER_TRC | ALL_FAILURE_TRC, ND6_NAME,
                         "ND6: Nd6SendRoutAdv:Send RA - BufAlloc Failed\n");
            IP6_TRC_ARG3 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC, BUFFER_TRC,
                          IP6_NAME, "%s buf err: Type = %d  Bufptr = %p",
                          ERROR_FATAL_STR, ERR_BUF_ALLOC, NULL);
            return (IP6_FAILURE);
        }

        /* Fill the RA Message fields */
        if (Nd6FillRoutAdv (pIf6, u1Flag, pBuf) == IP6_FAILURE)
            return (IP6_FAILURE);

        u4PktLen = u4Nd6Size;
#ifdef HA_WANTED
        u2Offset = sizeof (tNd6RoutAdv);
#endif

        if (u1Flag & ND6_RA_ADV)
        {
            if (Nd6FillLladdrInCxt (pIf6->pIp6Cxt, lladdr, ND6_SRC_LLA_EXT,
                                    pBuf) == IP6_FAILURE)
                return (IP6_FAILURE);
            (IP6_SEND_IF_OUT_STATS (pIf6, COUNT_RA)).u4SrcLinkAddrPkts++;

            /* if RALinkMTU is not 0 add MTU Options also. */
            if (pRAEntry->u4LinkMTU != IP6_ZERO)
            {
                if (Nd6FillMtu (pIf6, pBuf) == IP6_FAILURE)
                {
                    return (IP6_FAILURE);
                }
                (IP6_SEND_IF_OUT_STATS (pIf6, COUNT_RA)).u4MtuOptPkts++;
            }
#ifdef HA_WANTED
            u2Offset += sizeof (tNd6AddrExt) + sizeof (tNd6MtuExt);
#endif

            if (i2NumPrefixes)
            {
                if ((INT1) (i2PrefSent = (INT2) Nd6FillPrefixesInCxt
                            (pIf6->pIp6Cxt, p_prefix_list, &pCur,
                             i2_pref_per_ra, (INT2 *) &i2_pref_seen,
                             &u2Offset, pBuf)) == IP6_FAILURE)
                {
                    return (IP6_FAILURE);
                }

                i2NumPrefixes = (INT2) (i2NumPrefixes - i2_pref_seen);
                i2_pref_seen = 0;

                /*
                 * The valid length of RA is calculated based on the
                 * actual number of prefixes sent in the message
                 */
                u4PktLen = u4Nd6Size - (UINT4) ((UINT4) sizeof (tNd6PrefixExt) *
                                                (UINT4) (i2_pref_per_ra -
                                                         i2PrefSent));
            }
#ifdef HA_WANTED
            /* if, H Bit is set  */
            if (gu4mip6Entities == HOME_AGENT)
            {
                if (Nd6FillAdvIntervalOption (pBuf, pIf6) == IP6_FAILURE)
                    return IP6_FAILURE;

                if (Nd6FillHaInfoOptionInCxt (pIf6->pIp6Cxt, pBuf) ==
                    IP6_FAILURE)
                    return IP6_FAILURE;
            }
#endif
        }

        /* RFC 4191 Default router preference */
        /* Adding Route information options to RA message as an extension */
        if (u4RARoutInfoCount != 0)
        {
            IP6_TRC_ARG1 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                          BUFFER_TRC | ALL_FAILURE_TRC, ND6_NAME,
                          "ND6: Nd6SendRoutAdv:Sending RA - Available Route Information: %u\n",
                          u4RARoutInfoCount);
            pRARouteInfo = (tIp6RARouteInfoNode *) RBTreeGetFirst
                (pIf6->Ip6RARouteInfoTree);
            do
            {
                /* Check the rowstatus and if the row status is active, add the route
                 * route information to RA message, otherwise skip the RARoute
                 * information
                 */
                if (pRARouteInfo->i4RARouteRowStatus ==
                    IP6_RA_ROUTE_ROW_STATUS_ACTIVE)
                {
                    if (Nd6FillRARouteInfoOption
                        (pIf6, u1Flag, pRARouteInfo, pBuf,
                         &u4PktLen) == IP6_FAILURE)
                    {
                        return IP6_FAILURE;
                    }
                }
                if ((pIp6NextRARouteInfo =
                     (RBTreeGetNext
                      (pIf6->Ip6RARouteInfoTree, (tRBElem *) pRARouteInfo,
                       Ip6RARouteInfoCmp))) == NULL)
                {
                    IP6_TRC_ARG1 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                                  DATA_PATH_TRC, ND6_NAME,
                                  "ND6: Nd6SendSolRoutAdv: %u Route Information added to RA\n",
                                  u4RARoutInfoCount);
                    break;
                }
                pRARouteInfo = pIp6NextRARouteInfo;
            }
            while (pRARouteInfo != NULL);
        }

        /* Set the RA sent time to current time when periodic RA is sent only if
         * the RA is sent to ALL-Node address. This helps us to rate limit the
         * advertisement of RA sent to ALL-Node address.
         */
        if (IS_ALL_NODES_MULTI (*pDstAddr6))
        {
            IP6_IF_RA_SENT_TIME (pIf6) = OsixGetSysUpTime ();
        }
        ICMP6_INC_OUT_RADVS (pIf6->pIp6Cxt->Icmp6Stats);
        (IP6_IF_STATS (pIf6))->u4OutRoutadvs++;

        /* Send the RA message over the interface */

        IP6_TRC_ARG6 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC,
                      ND6_NAME,
                      "ND6:Nd6SendRoutAdv:SendingpktIP6IFPtr %p Len %d Type %d"
                      "Bufptr %p SrcAdr %s DstAdr %s\n", pIf6, u4PktLen,
                      ND6_ROUTER_ADVERTISEMENT, pBuf, Ip6PrintAddr (pSrcAddr6),
                      Ip6PrintAddr (pDstAddr6));

#ifdef TUNNEL_WANTED
        /* In case of tunnel interface pkt need to be enqueued to V4 task */
        if (pIf6->u1IfType == IP6_TUNNEL_INTERFACE_TYPE)
        {
            if (Ip6SendNdMsgOverTunl (pIf6, pNd6cEntry, pSrcAddr6,
                                      pDstAddr6, u4PktLen, pBuf) == IP6_FAILURE)
            {
                return (IP6_FAILURE);
            }
        }
        else
#endif
        {
            if (Ip6SendNdMsg (pIf6, pNd6cEntry, pSrcAddr6, pDstAddr6,
                              u4PktLen, pBuf, u1SeNDFlag, NULL) == IP6_FAILURE)
            {
                return (IP6_FAILURE);
            }

            if (IP6_TRUE == IF_SEND_ENABLED (pIf6))
            {
                (IP6_SEND_IF_OUT_STATS (pIf6, COUNT_RA)).u4CgaOptPkts++;
                (IP6_SEND_IF_OUT_STATS (pIf6, COUNT_RA)).u4RsaOptPkts++;
                (IP6_SEND_IF_OUT_STATS (pIf6, COUNT_RA)).u4TimeStampOptPkts++;
                (IP6_SEND_IF_OUT_STATS (pIf6, COUNT_RA)).u4NonceOptPkts++;
            }
        }
        u4_num_ra--;
    }
    IP6_IF_RA_INITIAL_CNT (pIf6)++;
    return (IP6_SUCCESS);

}

/*****************************************************************************
* DESCRIPTION : Check and send solicited Router Advertisement based on the
*               scheduling of Periodic RA

*
* INPUTS      : pIf6        -  Pointer to the interface.
*               pSrcAddr6   -  Pointer to the Source IPv6 address.
*               pDstAddr6   -  Pointer to the Destination address
*
* OUTPUTS     : None.
* 
* RETURNS     : IP6_SUCCESS or IP6_FAILURE.
*****************************************************************************/
VOID
Nd6SendSolRoutAdv (tIp6If * pIf6, tIp6Addr * pSrcAddr6, tIp6Addr * pDstAddr6,
                   UINT1 u1SecureFlag)
{
    UINT4               u4RaTime, u4_cur_time, u4RaSchedTime, u4RaSentTime;
    tNd6CacheEntry     *pNd6cEntry;
    tIp6Timer          *pRaTimer;
    INT1                i1AddrType = ADDR6_UNICAST;

    u4_cur_time = OsixGetSysUpTime ();

    /* Compute a Random time between 0 - MAX_RA_DELAY_TIME */
    u4RaTime = Ip6Random (0, MAX_RA_DELAY_TIME);

    IP6_TRC_ARG1 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC,
                  ND6_NAME,
                  "ND6 :Nd6SendSolRoutAdv: Sending Solicited RA - Random time %d\n",
                  u4RaTime);

    u4RaSchedTime = IP6_IF_RA_SCHED_TIME (pIf6);
    u4RaSentTime = IP6_IF_RA_SENT_TIME (pIf6);

    if (IS_ADDR_UNSPECIFIED (*pDstAddr6))
    {
        i1AddrType = ADDR6_UNSPECIFIED;
        SET_ALL_NODES_MULTI (*pDstAddr6);
    }

    /* This is to account for the Wrap around timer counter */
    if (u4_cur_time < u4RaSentTime)
    {
        if (u4RaSchedTime > u4RaSentTime)
        {
            u4RaSchedTime = IP6_MAX_SYS_TIME - IP6_IF_RA_SCHED_TIME (pIf6);
        }
        u4RaSentTime = IP6_MAX_SYS_TIME - IP6_IF_RA_SENT_TIME (pIf6);
    }

    /* If SeND is enabled, Sender will expect a Solicited RA, so skipping
     * the below check which is always true
     */

    if (IP6_FALSE == IF_SEND_ENABLED (pIf6))
    {
        if (((u4_cur_time * SYS_NUM_OF_TIME_UNITS_IN_A_SEC) + u4RaTime) >
            (u4RaSchedTime * SYS_NUM_OF_TIME_UNITS_IN_A_SEC))
        {
            IP6_TRC_ARG (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC, MGMT_TRC,
                         ND6_NAME,
                         "ND6 :Nd6SendSolRoutAdv: No Solicited RA sent - Multicast RA scheduled");
            return;
        }
    }
    pRaTimer = IP6_IF_RA_TIMER (pIf6);
    /*  If the router sent a multicast Router Advertisement with in the
     *  last MIN_DELAY_BETWEEN_RAS seconds, schedule the advertisement
     *  to be sent at a time corresponding to MIN_DELAY_BETWEEN_RAS
     *  plus the random value after the previous advertisement was sent
     *  Thus if an RA has been within 3 seconds , sehedule the next
     *  else send now.
     */
    if ((u4RaTime != 0) && (i1AddrType == ADDR6_UNSPECIFIED))
    {
        if (u4RaSentTime >= (u4_cur_time - MIN_DELAY_BETWEEN_RAS))
        {
            IP6_TRC_ARG (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC, MGMT_TRC,
                         ND6_NAME,
                         "ND6: Nd6SendSolRoutAdv:No Solicited RA sent - "
                         "Multicast RA re-scheduled");
            u4RaTime +=
                (MIN_DELAY_BETWEEN_RAS * SYS_NUM_OF_TIME_UNITS_IN_A_SEC);
        }
        Nd6TmrRestart (pIf6->pIp6Cxt->u4ContextId,
                       ND6_ROUT_ADV_SOL_TIMER_ID,
                       gIp6GblInfo.Ip6TimerListId,
                       &(pRaTimer->appTimer), u4RaTime);
        return;
    }

    if ((pNd6cEntry = Nd6IsCacheForAddr (pIf6, pDstAddr6)) == NULL)
    {
        /*
         * Check whether a NDCache entry exists for the destination address
         * and if not found, RA response is not sent
         */
        if ((pNd6cEntry = Nd6CreateCache (pIf6, pDstAddr6,
                                          NULL, 0, ND6C_STALE,
                                          u1SecureFlag)) == NULL)
        {
            IP6_TRC_ARG (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC, MGMT_TRC,
                         ND6_NAME,
                         "ND6: Sending Solicited RA Failed - No NDCache\n");
            UNUSED_PARAM (pNd6cEntry);
            return;
        }
        else
        {
            pNd6cEntry = Nd6LookupCache (pIf6, pDstAddr6);
        }
    }
    UNUSED_PARAM (pNd6cEntry);
    /* Send Solicited RA to unicast address immediately */
    /* According to rfc 2461(6.2.6) Solicited RA should be sent
       to All node multicast group */
    SET_ALL_NODES_MULTI (*pDstAddr6);
    if (Nd6SendRoutAdv (NULL, pIf6, pSrcAddr6,
                        pDstAddr6, ND6_RA_ADV) == IP6_FAILURE)
    {
        IP6_TRC_ARG (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC, MGMT_TRC,
                     ND6_NAME,
                     "ND6: Nd6SendSolRoutAdv:Sending Solicited RA Failed\n");
        return;
    }

    IP6_TRC_ARG (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC, MGMT_TRC, ND6_NAME,
                 "ND6: Nd6SendSolRoutAdv:Sending Solicited RA immediately\n");
}

/*****************************************************************************
* DESCRIPTION : Calculate the number of Prefixes allowed to be advertised in
*               a single RA and also returns the number of RAs to be sent so 
*               as to advertise all of the prefixes
*
* INPUTS      : pIf6        -  Pointer to the interface.
*               u4RaSize    -  Router adv size. 
*               pPrefPerRa  -  R-S-O flag(router, solicited, and override)
*
* OUTPUTS     : None.
* 
* RETURNS     : IP6_SUCCESS or IP6_FAILURE.
*****************************************************************************/
PRIVATE INT4
Nd6GetPrefixPerRa (tIp6If * pIf6, UINT4 u4RaSize, INT2 *pPrefPerRa)
{
    INT2                i2Prefixes = *pPrefPerRa;    /* Total number of prefixes */
    UINT4               u4Mtu, u4_tot_pref_size, u4_num_ra = 1;

    /*
     * MTU of the link is verified when a RA is to be sent with
     * Prefix information in the extension option
     */
    u4Mtu = Ip6ifGetMtu (pIf6);
    u4_tot_pref_size =
        (UINT4) ((UINT4) sizeof (tNd6PrefixExt) * (UINT4) i2Prefixes);

    /*
     * Calculate the number of prefixes that is to be sent in 
     * each RA and also the number of RAs to be sent
     */
    if (u4Mtu < (u4RaSize + u4_tot_pref_size))
    {
        *pPrefPerRa = (INT2) ((u4Mtu - u4RaSize) / sizeof (tNd6PrefixExt));
        if (*pPrefPerRa)
        {
            u4_num_ra = (UINT4) (i2Prefixes / *pPrefPerRa);
            u4_num_ra = ((i2Prefixes % *pPrefPerRa) != 0) ?
                u4_num_ra + 1 : u4_num_ra;
        }
    }
    else
        *pPrefPerRa = i2Prefixes;

    return ((INT4) u4_num_ra);
}

/*****************************************************************************
* DESCRIPTION : Fill the RA message fields 
*
* INPUTS      : pIf6        -  Pointer to the interface.
*               u1Flag      -  Flag indicating as whether router
*                               is enabled/ceased to advertise
*               pBuf        -  Pointer to the message buffer.
*
* OUTPUTS     : None.
* 
* RETURNS     : IP6_SUCCESS or IP6_FAILURE.
*****************************************************************************/
PRIVATE INT4
Nd6FillRoutAdv (tIp6If * pIf6, UINT1 u1Flag, tCRU_BUF_CHAIN_HEADER * pBuf)
{
    UINT1               u1Copy = 1;
    tNd6RoutAdv         nd6Radv, *p_nd6_radv;
    tIp6RAEntry        *pRAEntry = NULL;

    if (pIf6 == NULL)
    {
        return (IP6_FAILURE);
    }

    pRAEntry = Ip6RaTblGetEntry ((INT4) pIf6->u4Index);
    if (pRAEntry == NULL)
    {
        return (IP6_FAILURE);
    }

    MEMSET (&nd6Radv, 0, sizeof (tNd6RoutAdv));

    p_nd6_radv = (tNd6RoutAdv *) & nd6Radv;
    p_nd6_radv->icmp6Hdr.u1Type = ND6_ROUTER_ADVERTISEMENT;
    p_nd6_radv->icmp6Hdr.u1Code = ND6_RSVD_CODE;
    p_nd6_radv->icmp6Hdr.u2Chksum = 0;
    p_nd6_radv->u1HopLmt = pRAEntry->u1CurHopLimit;

    if (pRAEntry->u1MFlag == IPVX_TRUTH_VALUE_TRUE)
    {
        p_nd6_radv->u1StfulFlag = ND6_M_BIT_STFUL_FLAG;
    }
    if (pRAEntry->u1OFlag == IPVX_TRUTH_VALUE_TRUE)
    {
        p_nd6_radv->u1StfulFlag |= ND6_O_BIT_STFUL_FLAG;
    }

#ifdef HA_WANTED
    /*  check if, i'm a Home Agent. This variable to be
     *  set if HA Feature is enabled through the SNMP
     */

    if (gu4mip6Entities == HOME_AGENT)
        p_nd6_radv->u1StfulFlag |= ND6_H_BIT_STFUL_FLAG;
#endif

    /*
     * Upon the u1Flag indicating that the interface is ceasing to
     * advertise, send the RA with router lifetime as zero
     */
    if ((u1Flag & ND6_RA_ADV)
        && (pIf6->u1Ipv6IfFwdOperStatus == IP6_IF_FORW_ENABLE))
    {
        p_nd6_radv->u2Lifetime = HTONS ((pRAEntry->u2DefLifetime));
        if (pIf6->u2Preference == IP6_RA_ROUTE_PREF_HIGH)
        {
            p_nd6_radv->u1StfulFlag |= ND6_RA_PREFERENCE_HIGH;
        }
        else if (pIf6->u2Preference == IP6_RA_ROUTE_PREF_MED)
        {
            p_nd6_radv->u1StfulFlag |= ND6_RA_PREFERENCE_MEDIUM;
        }
        else if (pIf6->u2Preference == IP6_RA_ROUTE_PREF_LOW)
        {
            p_nd6_radv->u1StfulFlag |= ND6_RA_PREFERENCE_LOW;
        }
        else
        {
            /*Default Router Preference can't be other than low, medium or High */
            IP6_TRC_ARG (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC, MGMT_TRC,
                         ND6_NAME,
                         "ND6: Nd6FillRoutAdv:Invalid Default Router Preference\n");

            return (IP6_FAILURE);
        }
    }
    else
    {
        p_nd6_radv->u2Lifetime = 0;

        /* As per RFC 4191 section 2.2, if the Router LifeTime is zero 
         * preference must also be set to zero (default)*/
        p_nd6_radv->u1StfulFlag |= ND6_RA_PREFERENCE_MEDIUM;
    }

    p_nd6_radv->u4ReachTime = HTONL ((pRAEntry->u4ReachableTime));
    p_nd6_radv->u4RetransTime = HTONL ((pRAEntry->u4RetransmitTime));

    if (Ip6BufWrite (pBuf,
                     (UINT1 *) p_nd6_radv, IP6_BUF_WRITE_OFFSET (pBuf),
                     sizeof (tNd6RoutAdv), u1Copy) == IP6_FAILURE)
    {
        IP6_TRC_ARG2 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC, BUFFER_TRC | ALL_FAILURE_TRC, ND6_NAME, "ND6: Nd6FillRoutAdv: Fill RA - BufWrite Failed! \
                   BufPtr %p Offset %d\n", pBuf,
                      IP6_BUF_WRITE_OFFSET (pBuf));
        IP6_TRC_ARG3 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC, BUFFER_TRC,
                      IP6_NAME, "%s buf err: Type = %d  Bufptr = %p",
                      ERROR_FATAL_STR, ERR_BUF_WRITE, pBuf);
        Ip6BufRelease (pIf6->pIp6Cxt->u4ContextId, pBuf, FALSE, ND6_MODULE);
        return (IP6_FAILURE);
    }

    return (IP6_SUCCESS);

}

/*****************************************************************************
* DESCRIPTION : Forms the ND Redirect message and sends 
*               it to the lower layer for transmission
*
* INPUTS      : pIf6        -  Pointer to the interface 
*               pIp6        -  Pointer to the IPv6 header of
*                               the received packet
*               pTargAddr6 -  Pointer to ICMP target address
*               pRcvdBuf   -  Pointer to the received
*                               IPv6 packet
*
* OUTPUTS     : None
* 
* RETURNS     : IP6_SUCCESS       -  Sending of Redirect succeeds
*               IP6_FAILURE   -  Sending of Redirect fails
****************************************************************************/

INT4
Nd6SendRedirect (tNd6CacheEntry * pNd6cSrc, tIp6If * pIf6, tIp6Hdr * pIp6,
                 tIp6Addr * pTargAddr6, tCRU_BUF_CHAIN_HEADER * pRcvdBuf)
{
    tIp6Cxt            *pIp6Cxt = NULL;
    UINT4               u4Woffset, u4Nd6Size, u4_ip6hdr_size,
        u4_redir_data_size, u4_rcvd_pkt_size, u4_tot_size;
    tIp6Addr            srcAddr6, dstAddr6;
    tIp6Addr           *pIp6SrcAddr = NULL;
    tCRU_BUF_CHAIN_HEADER *pBuf;
    tNd6CacheLanInfo   *pNd6cLinfo;
    tNd6CacheEntry     *pNd6cEntry;

    pIp6Cxt = pIf6->pIp6Cxt;

    u4Woffset = Ip6BufWoffset (ND6_MODULE);

    /* 
     * Subtract 4 bytes from Redirect Extn header size as it also
     * comprises storage for redirect IP data pointer
     */
    u4Nd6Size = sizeof (tNd6Redirect) +
        sizeof (tNd6AddrExt) + (sizeof (tNd6RedirExt) - sizeof (UINT1 *));

    /* Assume the MAX size of IPv6 header with Extension Options */
    u4_ip6hdr_size = sizeof (tIp6Hdr);

    /* Calculate the redirect data size to be included in the message */
    u4_redir_data_size = IP6_MIN_MTU - (u4Nd6Size + u4_ip6hdr_size);

    u4_rcvd_pkt_size = IP6_BUF_DATA_LEN (pRcvdBuf);
    if (u4_rcvd_pkt_size < u4_redir_data_size)
    {
        u4_redir_data_size = u4_rcvd_pkt_size;
    }

    IP6_TRC_ARG2 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC,
                  ND6_NAME,
                  "ND6: Nd6SendRedirect:Redirect - Rcvd pkt len %d Redirect data len %d\n",
                  u4_rcvd_pkt_size, u4_redir_data_size);

    /* Allocate a buffer for the IPv6 packet size of Redirect message */
    u4Nd6Size += u4_redir_data_size;

    /* pad to 8 octets */
    if ((u4Nd6Size % IP6_EIGHT) != IP6_ZERO)
    {
        u4Nd6Size = (((u4Nd6Size / IP6_EIGHT) + IP6_ONE) * IP6_EIGHT);
    }
    u4_tot_size = u4Woffset + u4Nd6Size;

    /*
     * If SeND is enabled, increase the pBuf size to accommodate
     * all the SeND options
     */
    if (IP6_TRUE == IF_SEND_ENABLED (pIf6))
    {
        u4_tot_size += ND6_SEND_OPTIONS_LENGTH;
    }

    if ((pBuf = Ip6BufAlloc (pIf6->pIp6Cxt->u4ContextId, u4_tot_size,
                             u4Woffset, ND6_MODULE)) == NULL)
    {
        IP6_TRC_ARG (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                     BUFFER_TRC | ALL_FAILURE_TRC, ND6_NAME,
                     "ND6: Nd6SendRedirect:Send Redirect - BufAlloc Failed\n");
        IP6_TRC_ARG3 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC, BUFFER_TRC,
                      IP6_NAME, "%s buf err: Type = %d  Bufptr = %p",
                      ERROR_FATAL_STR, ERR_BUF_ALLOC, NULL);
        return (IP6_FAILURE);
    }

    /* Source address of Redirect is set to Link local address of interface */
    pIp6SrcAddr = Ip6GetLlocalAddr (pIf6->u4Index);
    if (pIp6SrcAddr == NULL)
    {
        /* Valid Link-Local Address not present. */
        Ip6BufRelease (pIf6->pIp6Cxt->u4ContextId, pBuf, FALSE, ND6_MODULE);
        return (IP6_FAILURE);
    }

    Ip6AddrCopy (&srcAddr6, pIp6SrcAddr);

    /* Dest address of Redirect is set to source addr of recd packet */
    Ip6AddrCopy (&dstAddr6, &pIp6->srcAddr);

    if (Nd6FillRedirectInCxt (pIf6->pIp6Cxt, &pIp6->dstAddr, pTargAddr6,
                              pBuf) == IP6_FAILURE)
        return (IP6_FAILURE);

    /*
     * Search in the NDCache table for link layer address of the
     * target address. If not present, the link layer extension option
     * is not included in the Redirect message and the size of the
     * redirect packet is reduced by address extension length
     */
    if ((pNd6cEntry = Nd6IsCacheForAddr (pIf6, pTargAddr6)) != NULL)
    {
        pNd6cLinfo = (tNd6CacheLanInfo *) (VOID *) pNd6cEntry->pNd6cInfo;
        if (Nd6FillLladdrInCxt (pIf6->pIp6Cxt, pNd6cLinfo->lladdr,
                                ND6_TARG_LLA_EXT, pBuf) == IP6_FAILURE)
            return (IP6_FAILURE);
    }
    else
    {
        u4Nd6Size -= sizeof (tNd6AddrExt);
    }

    if (Nd6FillRedirExtInCxt (pIf6->pIp6Cxt, u4_redir_data_size,
                              pRcvdBuf, pBuf) == IP6_FAILURE)
        return (IP6_FAILURE);

    ICMP6_INC_OUT_REDIRS (pIf6->pIp6Cxt->Icmp6Stats);
    (IP6_IF_STATS (pIf6))->u4OutRedirs++;

    /* Send the Redirect message over the interface */

    IP6_TRC_ARG6 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC,
                  ND6_NAME,
                  "ND6:Nd6SendRedirect:SendingPktIFPtr %p  Len%d  Type%d "
                  "Bufptr%p SrcAdr%s DstAdr%s ", pIf6, u4Nd6Size, ND6_REDIRECT,
                  pBuf, Ip6PrintAddr (&srcAddr6), Ip6PrintAddr (&dstAddr6));

    IP6_TRC_ARG1 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC,
                  ND6_NAME, "TarAdr %s", Ip6PrintAddr (pTargAddr6));
#ifdef TUNNEL_WANTED
    /* In case of tunnel interface pkt need to be enqueued to V4 task */
    if (pIf6->u1IfType == IP6_TUNNEL_INTERFACE_TYPE)
    {
        return (Ip6SendNdMsgOverTunl (pIf6, pNd6cSrc, &srcAddr6,
                                      &dstAddr6, u4Nd6Size, pBuf));
    }
#endif
    if ((pNd6cSrc != NULL) && (Nd6GetReachState (pNd6cSrc) != ND6C_INCOMPLETE))
    {
        return (Ip6SendNdMsg (pIf6, pNd6cSrc,
                              &srcAddr6, &dstAddr6, u4Nd6Size, pBuf, 0, NULL));
    }
    else
    {
        if (Ip6FormPkt (pIf6, &srcAddr6, &dstAddr6,
                        &u4Nd6Size, NH_ICMP6, pBuf,
                        IP6_MAX_HOP_LIMIT) == IP6_FAILURE)
        {
            IP6_TRC_ARG3 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                          DATA_PATH_TRC, IP6_NAME,
                          "IP6OUT:Ip6SendNdMsg:ConstrnND PktFailed, DstIF=%d "
                          "Len=%d BufPtr=%p", pIf6->u4Index, u4Nd6Size, pBuf);
            IP6IF_INC_OUT_DISCARDS (pIf6);
            IP6SYS_INC_OUT_DISCARDS (pIp6Cxt->Ip6SysStats);
            Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE,
                           IP6_CORE_SUBMODULE);
            return (IP6_FAILURE);
        }

        Nd6Resolve (pNd6cSrc, pIf6, &dstAddr6, pBuf, 0);
        return (IP6_SUCCESS);
    }

}

#ifdef HA_WANTED
INT4
Nd6FillAdvIntervalOption (tCRU_BUF_CHAIN_HEADER * pBuf, tIp6If * pIf6)
{
    UINT1               u1Copy = 1;
    tNd6AdvInterval     nd6NewAdvInt, *p_nd6_new_adv_int;

    p_nd6_new_adv_int = (tNd6AdvInterval *) & nd6NewAdvInt;

    p_nd6_new_adv_int->u1Type = ADV_INTERVAL_OPTION /* 7 */ ;
    p_nd6_new_adv_int->u1Len = ND6_ADV_INTERVAL_OPTION_LEN /* 1 */ ;
    p_nd6_new_adv_int->u2Rsvd = 0;
    p_nd6_new_adv_int->u4AdvInterval = OSIX_HTONL (IP6_IF_MAX_RA_TIME (pIf6));    /* global */

    if (Ip6BufWrite (pBuf,
                     (UINT1 *) p_nd6_new_adv_int, IP6_BUF_WRITE_OFFSET (pBuf),
                     sizeof (tNd6AdvInterval), u1Copy) == IP6_FAILURE)
    {
        Ip6BufRelease (pIf6->pIp6Cxt->u4ContextId, pBuf, FALSE, ND6_MODULE);
        return (IP6_FAILURE);
    }
    return (IP6_SUCCESS);
}

INT4
Nd6FillHaInfoOptionInCxt (tIp6Cxt * pIp6Cxt, tCRU_BUF_CHAIN_HEADER * pBuf)
{
    UINT1               u1Copy = 1;
    tNd6HaInfo          nd6HAInfo, *p_nd6_HA_info;

    p_nd6_HA_info = (tNd6HaInfo *) & nd6HAInfo;

    p_nd6_HA_info->u1Type = HA_INFO_OPTION /* 8 */ ;
    p_nd6_HA_info->u1Length = ND6_HA_INFO_OPTION_LEN /* 1 */ ;
    p_nd6_HA_info->u2Rsvd = 0;
    p_nd6_HA_info->i2HaPref = OSIX_HTONS (gi2HAPreference);
    p_nd6_HA_info->u2HaLifetime = OSIX_NTOHS (MIP6_HA_DEF_LIFE_TIME);    /* TODO mib configured value             */

    if (Ip6BufWrite (pBuf,
                     (UINT1 *) p_nd6_HA_info, IP6_BUF_WRITE_OFFSET (pBuf),
                     sizeof (tNd6HaInfo), u1Copy) == IP6_FAILURE)
    {
        Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE, ND6_MODULE);
        return (IP6_FAILURE);
    }
    return (IP6_SUCCESS);
}
#endif

/*****************************************************************************
* DESCRIPTION : Fill the Redirect message fields 
*
* INPUTS      : pDstAddr6   -  Pointer to the Destination address.
*               pTargAddr6  -  Pointer to the target address.
*               pBuf        -  Pointer to the message buffer to be sent.
*
* OUTPUTS     : None.
* 
* RETURNS     : IP6_SUCCESS or IP6_FAILURE.
*****************************************************************************/
PRIVATE INT4
Nd6FillRedirectInCxt (tIp6Cxt * pIp6Cxt, tIp6Addr * pDstAddr6,
                      tIp6Addr * pTargAddr6, tCRU_BUF_CHAIN_HEADER * pBuf)
{
    UINT1               u1Copy = 1;
    tNd6Redirect        nd6Redir, *p_nd6_redir;

    p_nd6_redir = (tNd6Redirect *) & nd6Redir;
    p_nd6_redir->icmp6Hdr.u1Type = ND6_REDIRECT;
    p_nd6_redir->icmp6Hdr.u1Code = ND6_RSVD_CODE;
    p_nd6_redir->icmp6Hdr.u2Chksum = 0;
    p_nd6_redir->u4Rsvd = 0;
    Ip6AddrCopy (&p_nd6_redir->targAddr6, pTargAddr6);
    Ip6AddrCopy (&p_nd6_redir->destAddr6, pDstAddr6);

    if (Ip6BufWrite (pBuf,
                     (UINT1 *) p_nd6_redir, IP6_BUF_WRITE_OFFSET (pBuf),
                     sizeof (tNd6Redirect) - 1, u1Copy) == IP6_FAILURE)
    {
        IP6_TRC_ARG2 (pIp6Cxt->u4ContextId, ND6_MOD_TRC, BUFFER_TRC | ALL_FAILURE_TRC, ND6_NAME, "ND6: Nd6FillRedirect:Fill Redirect - BufWrite Failed! \
                   BufPtr %p Offset %d\n", pBuf,
                      IP6_BUF_WRITE_OFFSET (pBuf));
        IP6_TRC_ARG3 (pIp6Cxt->u4ContextId, ND6_MOD_TRC, BUFFER_TRC, IP6_NAME,
                      "%s buf err: Type = %d  Bufptr = %p", ERROR_FATAL_STR,
                      ERR_BUF_WRITE, pBuf);
        Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE, ND6_MODULE);
        return (IP6_FAILURE);
    }

    return (IP6_SUCCESS);

}

/*****************************************************************************
* DESCRIPTION : Fill the MTU option fields 
*
* INPUTS      : pIf6        -  Pointer to the interface.
*               pBuf        -  Pointer to the Message buffer.
*
* OUTPUTS     : None.
* 
* RETURNS     : IP6_SUCCESS or IP6_FAILURE.
*****************************************************************************/
PRIVATE INT4
Nd6FillMtu (tIp6If * pIf6, tCRU_BUF_CHAIN_HEADER * pBuf)
{
    UINT1               u1Copy = 1;
    tNd6MtuExt          nd6Mtu, *p_nd6_mtu;
    tIp6RAEntry        *pRAEntry = NULL;

    if (pIf6 == NULL)
    {
        Ip6BufRelease (VCM_INVALID_VC, pBuf, FALSE, ND6_MODULE);
        return (IP6_FAILURE);
    }

    pRAEntry = Ip6RaTblGetEntry ((INT4) pIf6->u4Index);
    if (pRAEntry == NULL)
    {
        IP6_TRC_ARG1 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                      BUFFER_TRC | ALL_FAILURE_TRC, ND6_NAME,
                      "ND6: Ip6RaTblGetEntry Failed! for IfIndex %d \r\n",
                      pIf6->u4Index);
        Ip6BufRelease (pIf6->pIp6Cxt->u4ContextId, pBuf, FALSE, ND6_MODULE);
        return (IP6_FAILURE);
    }

    p_nd6_mtu = (tNd6MtuExt *) & nd6Mtu;
    p_nd6_mtu->u1Type = ND6_MTU_EXT;
    p_nd6_mtu->u1Len = ND6_MTU_EXT_LEN;
    p_nd6_mtu->u2Rsvd = 0;
    p_nd6_mtu->u4Mtu = HTONL (pRAEntry->u4LinkMTU);

    if (Ip6BufWrite (pBuf,
                     (UINT1 *) p_nd6_mtu, IP6_BUF_WRITE_OFFSET (pBuf),
                     sizeof (tNd6MtuExt), u1Copy) == IP6_FAILURE)
    {
        IP6_TRC_ARG2 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC, BUFFER_TRC | ALL_FAILURE_TRC, ND6_NAME, "ND6: Nd6FillMtu:  BufWrite Failed! \
                   BufPtr %p Offset %d\n", pBuf,
                      IP6_BUF_WRITE_OFFSET (pBuf));
        IP6_TRC_ARG3 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC, BUFFER_TRC,
                      IP6_NAME, "%s buf err: Type = %d  Bufptr = %p",
                      ERROR_FATAL_STR, ERR_BUF_WRITE, pBuf);
        Ip6BufRelease (pIf6->pIp6Cxt->u4ContextId, pBuf, FALSE, ND6_MODULE);
        return (IP6_FAILURE);
    }

    return (IP6_SUCCESS);

}

/*****************************************************************************
* DESCRIPTION : Fill the Prefix option fields 
*
* INPUTS      : pAddr6List  -  Pointer to the unicast address sll.
*               pCur        -  Pointer to the Current address in the list.
*               i2Prefixes  -  Total Number of RA Prefix.
*               pPrefSeen   -  Number of filled RA Prefix.
*               pBuf        -  Pointer to the message buffer.
*
* OUTPUTS     : None.
* 
* RETURNS     : IP6_SUCCESS or IP6_FAILURE.
*****************************************************************************/
INT4
Nd6FillPrefixesInCxt (tIp6Cxt * pIp6Cxt, tTMO_SLL * pAddr6List,
                      tTMO_SLL_NODE ** ppCur, INT2 i2Prefixes,
                      INT2 *pPrefSeen, UINT2 *pu2Offset,
                      tCRU_BUF_CHAIN_HEADER * pBuf)
{
    UINT1               u1Copy = 1;
    UINT2               u2Addr6Prof;
    INT2                i2PrefSent = 0;
    tNd6PrefixExt       nd6Prefix;
    tNd6PrefixExt      *p_nd6_prefix = NULL;
    tIp6PrefixNode     *pPrefixInfo = NULL;
    UINT4               u4RemainingTime;
    UINT1               u1PrefLen;
#ifndef HA_WANTED
    *pu2Offset = (UINT2) IP6_BUF_WRITE_OFFSET (pBuf);
#endif
    while (i2PrefSent < i2Prefixes)
    {
        MEMSET (&nd6Prefix, 0, sizeof (tNd6PrefixExt));
        if (((*ppCur) = TMO_SLL_Next (pAddr6List, (*ppCur))) == NULL)
            break;

        (*pPrefSeen)++;
        pPrefixInfo = (tIp6PrefixNode *) (*ppCur);
        u2Addr6Prof = pPrefixInfo->u2ProfileIndex;

        if (u2Addr6Prof >=
            FsIP6SizingParams[MAX_IP6_ADDR_PROFILES_SIZING_ID].
            u4PreAllocatedUnits)
        {
            break;
        }
        if (IP6_ADDR_SEND_PREFIX (u2Addr6Prof))
        {
            i2PrefSent++;
            p_nd6_prefix = (tNd6PrefixExt *) & nd6Prefix;
            p_nd6_prefix->u1Type = ND6_PREFIX_EXT;
            p_nd6_prefix->u1Len = ND6_PREFIX_EXT_LEN;
            p_nd6_prefix->u1PrefLen = pPrefixInfo->u1PrefixLen;

            if (IP6_ADDR_ON_LINK (u2Addr6Prof))
                p_nd6_prefix->u1PrefFlag = ND6_ON_LINK_FLAG;
            if (IP6_ADDR_AUTO_CONFIG (u2Addr6Prof))
                p_nd6_prefix->u1PrefFlag |= ND6_AUTO_CONFIG_FLAG;

#ifdef HA_WANTED
            if (gu4mip6Entities == HOME_AGENT)
                p_nd6_prefix->u1PrefFlag |= ND6_ROUTER_ADDRESS_FLAG;
#endif
            /*
             * Currently, Life Time's are fixed. Another option,
             * i.e., a time that decrements in real time must be
             * inserted here. But, when another option is introduced
             * -  when should be the fixed value used and when should
             * be the decremented value used?  
             */

            if (IP6_ADDR_VALID_TIME_FIXED (u2Addr6Prof))
            {
                p_nd6_prefix->u4ValidTime =
                    HTONL (IP6_ADDR_VALID_TIME (u2Addr6Prof));
            }
            else
            {
                u4RemainingTime = 0;
                if (TmrGetRemainingTime (gIp6GblInfo.Ip6TimerListId,
                                         &pPrefixInfo->ValidTimer.appTimer,
                                         &u4RemainingTime) == TMR_FAILURE)
                {
                    p_nd6_prefix->u4ValidTime = 0;
                }
                else
                {

                    p_nd6_prefix->u4ValidTime =
                        HTONL (u4RemainingTime /
                               SYS_NUM_OF_TIME_UNITS_IN_A_SEC);
                }
            }

            if (IP6_ADDR_PREF_TIME_FIXED (u2Addr6Prof))
            {
                p_nd6_prefix->u4PrefTime =
                    HTONL (IP6_ADDR_PREF_TIME (u2Addr6Prof));
            }
            else
            {
                u4RemainingTime = 0;
                if (TmrGetRemainingTime (gIp6GblInfo.Ip6TimerListId,
                                         &pPrefixInfo->PreferTimer.appTimer,
                                         &u4RemainingTime) == TMR_FAILURE)
                {
                    p_nd6_prefix->u4PrefTime = 0;
                }
                else
                {

                    p_nd6_prefix->u4PrefTime =
                        HTONL (u4RemainingTime /
                               SYS_NUM_OF_TIME_UNITS_IN_A_SEC);
                }
            }

            p_nd6_prefix->u4Rsvd = 0;

            /*
             * Fill the Prefix field of RA with the prefix of the address
             * followed by zeroes for sizeof IPv6 address
             */
#ifdef HA_WANTED
            if (gu4mip6Entities == HOME_AGENT)
            {
                u1PrefLen = 128;
            }
            else
#endif
                u1PrefLen = pPrefixInfo->u1PrefixLen;

            Ip6CopyAddrBits (&p_nd6_prefix->prefAddr6,
                             &pPrefixInfo->Ip6Prefix, (INT4) u1PrefLen);

            if (Ip6BufWrite (pBuf,
                             (UINT1 *) p_nd6_prefix,
                             *pu2Offset, sizeof (tNd6PrefixExt),
                             u1Copy) == IP6_FAILURE)
            {
                IP6_TRC_ARG2 (pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                              BUFFER_TRC | ALL_FAILURE_TRC, ND6_NAME,
                              "ND6: Nd6FillPrefixes: Fill PREFIX - BufWrite "
                              "Failed!  BufPtr %p Offset %d\n", pBuf,
                              IP6_BUF_WRITE_OFFSET (pBuf));
                IP6_TRC_ARG3 (pIp6Cxt->u4ContextId, ND6_MOD_TRC, BUFFER_TRC,
                              IP6_NAME, "%s buf err: Type = %d  Bufptr = %p",
                              ERROR_FATAL_STR, ERR_BUF_WRITE, pBuf);
                Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE, ND6_MODULE);
                return (IP6_FAILURE);
            }

            *pu2Offset = (UINT2) (*pu2Offset + sizeof (tNd6PrefixExt));

        }
    }
    return (i2PrefSent);
}

/*****************************************************************************
* DESCRIPTION : Fill the Redirect Extension option fields 
*
* INPUTS      : u4RedirDataLen - length of redirect message.
*               pRcvdBuf       - Pointer to the rcvd message buffer.
*
* OUTPUTS     : pBuf           - Pointer to the message buffer to be sent.
* 
* RETURNS     : IP6_SUCCESS or IP6_FAILURE.
*****************************************************************************/
PRIVATE INT4
Nd6FillRedirExtInCxt (tIp6Cxt * pIp6Cxt, UINT4 u4RedirDataLen,
                      tCRU_BUF_CHAIN_HEADER * pRcvdBuf,
                      tCRU_BUF_CHAIN_HEADER * pBuf)
{
    tNd6RedirExt       *p_nd6_redir = NULL;
    UINT1              *pu1NdRedirectData = NULL;
    UINT4               u4RedirExtLen = IP6_ZERO;

    /* allocate memory for the redirect extension + data */
    pu1NdRedirectData =
        (UINT1 *) MemAllocMemBlk (gNd6GblInfo.u4Ip6NdRedirectDataPoolId);
    if (NULL == pu1NdRedirectData)
    {
        return IP6_FAILURE;
    }

    u4RedirExtLen = sizeof (tNd6RedirExt) - sizeof (UINT1 *) + u4RedirDataLen;

    /* extension length in terms of 8 octets */
    if ((u4RedirExtLen % IP6_EIGHT) != IP6_ZERO)
    {
        u4RedirExtLen = (u4RedirExtLen / IP6_EIGHT) + IP6_ONE;
    }

    p_nd6_redir = (tNd6RedirExt *) (VOID *) pu1NdRedirectData;
    p_nd6_redir->u1Type = ND6_REDIRECT_EXT;
    p_nd6_redir->u1Len = (UINT1) u4RedirExtLen;
    p_nd6_redir->u2Rsvd1 = 0;
    p_nd6_redir->u4Rsvd2 = 0;

    /* read the data after the extension header */
    if (Ip6BufRead (pRcvdBuf, pu1NdRedirectData + IP6_EIGHT, 0,
                    u4RedirDataLen, TRUE) == NULL)
    {
        IP6_TRC_ARG2 (pIp6Cxt->u4ContextId, ND6_MOD_TRC, BUFFER_TRC,
                      ND6_NAME,
                      "ND6:Nd6FillRedirExtInCxt: RD - BufRead Failed! "
                      "BufPtr %p Offset %d\n", pBuf,
                      IP6_BUF_READ_OFFSET (pBuf));
        IP6_TRC_ARG3 (pIp6Cxt->u4ContextId, ND6_MOD_TRC, BUFFER_TRC, IP6_NAME,
                      "%s buffer error occurred, Type = %d Module = 0x%X "
                      "Bufptr = %p ", ERROR_FATAL_STR, ERR_BUF_READ, pBuf);
        Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE, ND6_MODULE);
        MemReleaseMemBlock (gNd6GblInfo.u4Ip6NdRedirectDataPoolId,
                            (UINT1 *) pu1NdRedirectData);
        return (IP6_FAILURE);
    }

    /* write into the new buffer, data includes the pad */
    if (Ip6BufWrite (pBuf, pu1NdRedirectData, IP6_BUF_WRITE_OFFSET (pBuf),
                     (u4RedirExtLen * IP6_EIGHT), TRUE) == IP6_FAILURE)
    {
        IP6_TRC_ARG2 (pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                      BUFFER_TRC | ALL_FAILURE_TRC, ND6_NAME,
                      "ND6: Nd6FillRedirExt:Fill NS - BufWrite Failed! \
                       BufPtr %p Offset %d\n", pBuf, IP6_BUF_WRITE_OFFSET (pBuf));
        IP6_TRC_ARG3 (pIp6Cxt->u4ContextId, ND6_MOD_TRC, BUFFER_TRC, IP6_NAME,
                      "%s buf err: Type = %d  Bufptr = %p", ERROR_FATAL_STR,
                      ERR_BUF_WRITE, pBuf);
        Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE, ND6_MODULE);
        MemReleaseMemBlock (gNd6GblInfo.u4Ip6NdRedirectDataPoolId,
                            (UINT1 *) pu1NdRedirectData);
        return (IP6_FAILURE);
    }
    MemReleaseMemBlock (gNd6GblInfo.u4Ip6NdRedirectDataPoolId,
                        (UINT1 *) pu1NdRedirectData);

    return (IP6_SUCCESS);

}

/*****************************************************************************
* DESCRIPTION : Decides on sending/stopping of Router Advertisements
*               based on RA Advertisement status on the interface
*                    
* INPUTS      : pIf6        -   Pointer to the interface 
*               u1Status    -   Status of the interface
* 
* OUTPUTS     : None
* 
* RETURNS     : None
*****************************************************************************/
VOID
Nd6ActOnRaCnf (tIp6If * pIf6, UINT1 u1Status)
{
    tIp6Addr            dstAddr6;
    tIp6Addr           *pIp6SrcAddr = NULL;

    IP6_TRC_ARG2 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC,
                  ND6_NAME,
                  "ND6: Nd6ActOnRaCnf:Indication on RA Adv Status -  IF %d "
                  "Status %d\n", pIf6->u4Index, u1Status);

    switch (u1Status)
    {
        case IP6_IF_RA_ADV:
            SET_ALL_NODES_MULTI (dstAddr6);
            /*
             * RA is sent immediately and the next few RAs are scheduled
             * to be initial advertisements sent in small interval of time
             */
            pIp6SrcAddr = Ip6GetLlocalAddr (pIf6->u4Index);
            if (pIp6SrcAddr == NULL)
            {
                IP6_TRC_ARG1 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                              DATA_PATH_TRC, ND6_NAME,
                              "ND6: Nd6ActOnRaCnf:Valid Link-Local Address "
                              "not present on interface %d\n", pIf6->u4Index);
                return;
            }
            if (Nd6SendRoutAdv (NULL, pIf6, (tIp6Addr *) pIp6SrcAddr,
                                &dstAddr6, ND6_RA_ADV) == IP6_FAILURE)
            {
                IP6_TRC_ARG1 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                              DATA_PATH_TRC, ND6_NAME,
                              "ND6: Nd6ActOnRaCnf:Indication on RA Adv Status "
                              "- Send RA failed on IF %d\n", pIf6->u4Index);
                return;
            }
            Nd6SchedNextRoutAdv (pIf6);
            break;

        case IP6_IF_NO_RA_ADV:
            Nd6CeaseToRoutAdv (pIf6);
            break;

        default:
            break;
    }

}

/*****************************************************************************
* DESCRIPTION : This routine is called when the interface is stopped to 
*               advertise due to the interface getting down/deleted or 
*               when the ra_cnf flag is set to stop sending RAs
*                    
* INPUTS      : pIf6        -   Pointer to the interface 
* 
* OUTPUTS     : None
* 
* RETURNS     : None
*****************************************************************************/
VOID
Nd6CeaseToRoutAdv (tIp6If * pIf6)
{
    UINT4               u4RaCnt = 2;
    tIp6Addr            dstAddr6;
    tIp6Addr           *pIp6SrcAddr = NULL;
    tIp6Timer          *pRaTimer;

    /* Stop the RA timer running */
    pRaTimer = IP6_IF_RA_TIMER (pIf6);

    if (pRaTimer->u1Id != 0)
        Ip6TmrStop (pIf6->pIp6Cxt->u4ContextId,
                    ND6_ROUT_ADV_SOL_TIMER_ID,
                    gIp6GblInfo.Ip6TimerListId, &(pRaTimer->appTimer));
    SET_ALL_NODES_MULTI (dstAddr6);

    /* Send RA twice on the interface immediately */
    for (u4RaCnt = 0; u4RaCnt < MAX_FINAL_RTR_ADVERTISEMENTS; u4RaCnt++)
    {
        pIp6SrcAddr = Ip6GetLlocalAddr (pIf6->u4Index);
        if (pIp6SrcAddr != NULL)
        {
            if (Nd6SendRoutAdv (NULL, pIf6, (tIp6Addr *) pIp6SrcAddr,
                                &dstAddr6, ND6_CEASE_RA_ADV) == IP6_FAILURE)
            {
                IP6_TRC_ARG1 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                              DATA_PATH_TRC, ND6_NAME,
                              "ND6: Nd6CeaseToRoutAdv:Cease to Advertise RA - "
                              "Send RA failed on IF %d\n", pIf6->u4Index);
            }
        }
    }

    /* Re-initialise the RA initial count */
    IP6_IF_RA_INITIAL_CNT (pIf6) = 0;

    /* Reset the stats on the next RA scheduled time */
    IP6_IF_RA_SCHED_TIME (pIf6) = 0;

}

/*****************************************************************************
* DESCRIPTION : Performs time-out actions specific
*               to Router Advertisement timer expiry
* 
* INPUTS      : pIf6        -  Pointer to the interface 
*
* OUTPUTS     : None
* 
* RETURNS     : None
*****************************************************************************/

VOID
Nd6RaTimeout (tIp6If * pIf6)
{
    tIp6Addr            dstAddr6;
    tIp6Addr           *pIp6SrcAddr = NULL;

    /*
     * Sends RA and schedules the next RA by starting the RA timer
     * for the computed random time
     */
    SET_ALL_NODES_MULTI (dstAddr6);
    pIp6SrcAddr = Ip6GetLlocalAddr (pIf6->u4Index);
    if ((pIp6SrcAddr == NULL) ||
        (Nd6SendRoutAdv (NULL, pIf6, pIp6SrcAddr, &dstAddr6, ND6_RA_ADV)
         == IP6_FAILURE))
    {
        IP6_TRC_ARG1 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC,
                      ND6_NAME,
                      "ND6: Nd6RaTimeout:RA Timeout - Send RA failed on IF %d\n",
                      pIf6->u4Index);
        return;
    }

    Nd6SchedNextRoutAdv (pIf6);

}

/*****************************************************************************
* DESCRIPTION : This routine computes a random time between the RA AdvInterval
*               and schedules the sending of periodic RA by starting the
*               timer for the computed RA time interval.

* 
* INPUTS      : pIf6        -  Pointer to the interface 
*
* OUTPUTS     : None
* 
* RETURNS     : None
*****************************************************************************/
VOID
Nd6SchedNextRoutAdv (tIp6If * pIf6)
{
    UINT4               u4RaTime;
    tIp6Timer          *pRaTimer;
    UINT4               u4CurrentTime;
    tIp6RAEntry        *pRAEntry = NULL;

    pRAEntry = Ip6RaTblGetEntry ((INT4) pIf6->u4Index);
    if (pRAEntry == NULL)
    {
        IP6_TRC_ARG1 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC, BUFFER_TRC | ALL_FAILURE_TRC, ND6_NAME, "ND6:Nd6SchedNextRoutAdv :Ip6RaTblGetEntry Failed! \
                      for IfIndex %d \n",
                      (pIf6->u4Index));
        return;
    }

    u4RaTime = Ip6Random ((pRAEntry->u4MinInterval), (pRAEntry->u4MaxInterval));

    /*
     * Checks whether the number of RAs sent is less than MAX INITIAL RAS
     * and increments the count, initialises the RA timer to
     * Max Interval if the computed random time exceeds the Max value
     */
    if (IP6_IF_RA_INITIAL_CNT (pIf6) < MAX_INITIAL_RTR_ADVERTISEMENTS)
    {
        if (u4RaTime > MAX_INITIAL_RTR_ADVERT_INTERVAL)
            u4RaTime = MAX_INITIAL_RTR_ADVERT_INTERVAL;
    }

    IP6_TRC_ARG1 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC,
                  ND6_NAME,
                  "ND6: Nd6SchedNextRoutAdv:Schedule Next RA - RA time %d\n",
                  u4RaTime);

    /* Start the RA timer for the computed RA time period */
    pRaTimer = IP6_IF_RA_TIMER (pIf6);
    Ip6TmrStop (pIf6->pIp6Cxt->u4ContextId, ND6_ROUT_ADV_SOL_TIMER_ID,
                gIp6GblInfo.Ip6TimerListId, &(pRaTimer->appTimer));
    Ip6TmrStart (pIf6->pIp6Cxt->u4ContextId, ND6_ROUT_ADV_SOL_TIMER_ID,
                 gIp6GblInfo.Ip6TimerListId, &(pRaTimer->appTimer), u4RaTime);
    u4CurrentTime = OsixGetSysUpTime ();
    IP6_IF_RA_SCHED_TIME (pIf6) = u4CurrentTime + u4RaTime;
}

/*****************************************************************************
* DESCRIPTION : ND timer handler routine
*                       
* INPUTS      : u1TimerId  -   Timer Id
*               pNd6Timer  -   Pointer to ND timer node
* 
* OUTPUTS     : None
* 
* RETURNS     : None
*****************************************************************************/
VOID
Nd6Timeout (UINT1 u1TimerId, tIp6Timer * pNd6Timer)
{
    tIp6If             *pIf6;
    tNd6CacheLanInfo   *pNd6cLinfo;

    switch (u1TimerId)
    {
        case ND6_RETRANS_TIMER_ID:
        case ND6_REACH_TIMER_ID:
        case ND6_DELAY_PROBE_TIMER_ID:
            pNd6cLinfo = ND6C_LAN_INFO_FROM_TIMER (pNd6Timer);
            Nd6CacheTimeout (u1TimerId, pNd6cLinfo->pNd6cEntry);
            break;
        case ND6_ROUT_ADV_SOL_TIMER_ID:
            pIf6 = IP6_IF_PTR_FROM_TIMER (pNd6Timer);
            if (pIf6->u1Ipv6IfFwdOperStatus == IP6_IF_FORW_ENABLE)
            {
                Nd6RaTimeout (pIf6);
            }
            else
            {
                Nd6RsTimeout (pIf6);
            }
            break;
        case ND6_PROXY_LOOP_TIMER_ID:
            pIf6 = IP6_IF_PTR_FROM_PROXY_TIMER (pNd6Timer);
            Nd6ProxyTimeout (pIf6);
            break;
        default:
            break;
    }

}

/*****************************************************************************
* DESCRIPTION : Decides on sending of ND messages based on the
*               interface status, handles the Router Advertisement
*               timer and purges the NDCache entries of the interface
*               upon interface down/delete indication
*                    
* INPUTS      : pIf6        -   Pointer to the interface 
*               u1Status    -   Status of the interface
* 
* OUTPUTS     : None
* 
* RETURNS     : None
*****************************************************************************/
VOID
Nd6ActOnIfstatus (tIp6If * pIf6, UINT1 u1Status)
{

    tIp6PrefixNode     *pTempPrefixInfo = NULL;
    tIp6PrefixNode     *pPrefixInfo = NULL;
    IP6_TRC_ARG3 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC,
                  ND6_NAME,
                  "ND6: Nd6ActOnIfstatus:Indication for IF %d Type %d Status %d\n",
                  pIf6->u4Index, pIf6->u1IfType, u1Status);

    if (pIf6->u1IfType != IP6_ENET_INTERFACE_TYPE &&
        pIf6->u1IfType != IP6_L3VLAN_INTERFACE_TYPE &&
        pIf6->u1IfType != IP6_LAGG_INTERFACE_TYPE &&
        pIf6->u1IfType != IP6_PSEUDO_WIRE_INTERFACE_TYPE &&
        pIf6->u1IfType != IP6_X25_INTERFACE_TYPE &&
        (pIf6->u1IfType != IP6_L3SUB_INTF_TYPE) &&
#ifdef WGS_WANTED
        (pIf6->u1IfType != IP6_L2VLAN_INTERFACE_TYPE) &&
#endif
#ifdef TUNNEL_WANTED
        pIf6->u1IfType != IP6_TUNNEL_INTERFACE_TYPE &&
#endif /* TUNNEL_WANTED */
        pIf6->u1IfType != IP6_FR_INTERFACE_TYPE)
    {

        return;
    }

    switch (u1Status)
    {
        case IP6_IF_UP:
            /*
             * Oper Up indication is received for LAN interfaces
             * only upon link-local address being a VALID address
             * assigned (DAD completed) on the interface
             */
            if (pIf6->u1Ipv6IfFwdOperStatus == IP6_IF_FORW_ENABLE)    /*router */
            {
                if ((pIf6->u1IfType == IP6_ENET_INTERFACE_TYPE) ||
#ifdef TUNNEL_WANTED
                    (pIf6->u1IfType == IP6_TUNNEL_INTERFACE_TYPE) ||
#endif /* TUNNEL_WANTED */
                    (pIf6->u1IfType == IP6_PSEUDO_WIRE_INTERFACE_TYPE) ||
                    (pIf6->u1IfType == IP6_L3VLAN_INTERFACE_TYPE) ||
                    (pIf6->u1IfType == IP6_LAGG_INTERFACE_TYPE) ||
                    (pIf6->u1IfType == IP6_L3SUB_INTF_TYPE))
                {
                    /* Schedule the sending of periodic RA */
                    if ((Ip6CheckRAStatus ((INT4) (pIf6->u4Index))) == TRUE)
                    {
                        Nd6SchedNextRoutAdv (pIf6);
                    }
                }
            }
            else if (pIf6->u1Ipv6IfFwdOperStatus == IP6_IF_FORW_DISABLE)
            {

                if ((pIf6->u1IfType == IP6_ENET_INTERFACE_TYPE) ||
#ifdef TUNNEL_WANTED
                    (pIf6->u1IfType == IP6_TUNNEL_INTERFACE_TYPE) ||
#endif /* TUNNEL_WANTED */
#ifdef WGS_WANTED
                    (pIf6->u1IfType == IP6_L2VLAN_INTERFACE_TYPE) ||
#endif
                    (pIf6->u1IfType == IP6_PSEUDO_WIRE_INTERFACE_TYPE) ||
                    (pIf6->u1IfType == IP6_L3VLAN_INTERFACE_TYPE) ||
                    (pIf6->u1IfType == IP6_LAGG_INTERFACE_TYPE) ||
                    (pIf6->u1IfType == IP6_L3SUB_INTF_TYPE))
                {
                    /*  Send the router solicitation and schedule
                     * the sending of next router solicitation  */
                    Nd6SendRouterSol (pIf6, NULL);
                    (IP6_SEND_IF_OUT_STATS (pIf6, COUNT_NA)).u4CgaOptPkts++;
                    (IP6_SEND_IF_OUT_STATS (pIf6, COUNT_NA)).u4RsaOptPkts++;
                    (IP6_SEND_IF_OUT_STATS (pIf6, COUNT_NA)).
                        u4TimeStampOptPkts++;
                    (IP6_SEND_IF_OUT_STATS (pIf6, COUNT_NA)).u4NonceOptPkts++;

                    Nd6SchedNextRoutSol (pIf6);

                }
            }
            break;

        case IP6_IF_DOWN:
            /*
             * Upon Admin/Oper down indication, the dynamic NDCache entries 
             * on that interface are purged out. For LAN interfaces, RA
             * periodic timer is stopped and RA is sent with lifetime Zero
             */
            if (pIf6->u1Ipv6IfFwdOperStatus == IP6_IF_FORW_ENABLE)    /*router */
            {
                if ((pIf6->u1IfType == IP6_ENET_INTERFACE_TYPE) ||
#ifdef TUNNEL_WANTED
                    (pIf6->u1IfType == IP6_TUNNEL_INTERFACE_TYPE) ||
#endif /* TUNNEL_WANTED */
                    (pIf6->u1IfType == IP6_PSEUDO_WIRE_INTERFACE_TYPE) ||
                    (pIf6->u1IfType == IP6_L3VLAN_INTERFACE_TYPE) ||
                    (pIf6->u1IfType == IP6_LAGG_INTERFACE_TYPE) ||
                    (pIf6->u1IfType == IP6_L3SUB_INTF_TYPE))
                {
                    if ((Ip6CheckRAStatus ((INT4) (pIf6->u4Index))) == TRUE)
                    {
                        Nd6CeaseToRoutAdv (pIf6);
                    }
                }
            }
            else
            {
                if ((IP6_IF_RS_TIMER (pIf6))->u1Id != 0)
                {
                    Ip6TmrStop (pIf6->pIp6Cxt->u4ContextId,
                                ND6_ROUT_ADV_SOL_TIMER_ID,
                                gIp6GblInfo.Ip6TimerListId,
                                &(IP6_IF_RS_TIMER (pIf6))->appTimer);
                }
            }

            Nd6PurgeCacheOnIface (pIf6, ND6C_PURGE_DYNAMIC);
            break;

        case IP6_IF_DELETE:
            /*
             * Upon Interface Delete indication, all the NDCache 
             * static/dynamic entries on that interface are purged out
             */
            if (pIf6->u1Ipv6IfFwdOperStatus == IP6_IF_FORW_ENABLE)    /*router */
            {
                if ((pIf6->u1IfType == IP6_ENET_INTERFACE_TYPE) ||
#ifdef TUNNEL_WANTED
                    (pIf6->u1IfType == IP6_TUNNEL_INTERFACE_TYPE) ||
#endif /* TUNNEL_WANTED */
                    (pIf6->u1IfType == IP6_PSEUDO_WIRE_INTERFACE_TYPE) ||
                    (pIf6->u1IfType == IP6_L3VLAN_INTERFACE_TYPE) ||
                    (pIf6->u1IfType == IP6_LAGG_INTERFACE_TYPE) ||
                    (pIf6->u1IfType == IP6_L3SUB_INTF_TYPE))
                {
                    if ((Ip6CheckRAStatus ((INT4) (pIf6->u4Index))) == TRUE)
                    {
                        Nd6CeaseToRoutAdv (pIf6);
                    }
                }
                TMO_DYN_SLL_Scan (&pIf6->prefixlist, pTempPrefixInfo,
                                  pPrefixInfo, tIp6PrefixNode *)
                {
                    if (pTempPrefixInfo != NULL
                        && ((UINT4) (pTempPrefixInfo->u2ProfileIndex) != 0))
                    {
                        TMO_SLL_Delete (&pIf6->prefixlist,
                                        &(pTempPrefixInfo->NextPrefix));
                        Ip6RaPrefixNotify (pTempPrefixInfo, PREFIX_DELETE);
                    }
                }
            }
            else
            {
                if ((IP6_IF_RS_TIMER (pIf6))->u1Id != 0)
                {
                    Ip6TmrStop (pIf6->pIp6Cxt->u4ContextId,
                                ND6_ROUT_ADV_SOL_TIMER_ID,
                                gIp6GblInfo.Ip6TimerListId,
                                &(IP6_IF_RS_TIMER (pIf6))->appTimer);
                }
            }
            Nd6PurgeCacheOnIface (pIf6, ND6C_PURGE_ALL);
            break;

        case IP6_IF_UNMAP:
            /*
             * When an interface is unmapped, the ND cache entries
             * learnt on that interface in the previous virtual router have
             * to be purged. The RA configurations done on the interface
             * are not dependent on the virtual router, hence they are not
             * cleared.
             */
            Nd6PurgeCacheOnIface (pIf6, ND6C_PURGE_ALL);
            break;

        default:
            break;
    }
}

/*****************************************************************************
* DESCRIPTION : Deletes the first NDCache entry from the lst of
*               anycast destinations
*              
* INPUTS      : pNd6cEntry   -  Pointer to the NDCache Entry
* 
* OUTPUTS     : None
* 
* RETURNS     : IP6_SUCCESS         -  ND entry deletion succeeds
*             : IP6_FAILURE     -  Deletion fails
*****************************************************************************/

INT4
Nd6PurgeAnycastCache (tNd6CacheEntry * pNd6cEntry)
{
    tIp6If             *pIf6 = NULL;
    tNd6CacheLanInfo   *pNd6cInfo = NULL;
    tAnycastNDCache    *pNextAnycastNDCache = NULL;
    tAnycastNDCache    *pFirstAnycastNDCache = NULL;
    if (pNd6cEntry == NULL)
        return (IP6_FAILURE);

    pIf6 = pNd6cEntry->pIf6;

    pFirstAnycastNDCache =
        (tAnycastNDCache *) TMO_SLL_First (&(pNd6cEntry->anycastDestAddrList));

    /* remove that node alone
       get the next node in the list and copy to tha main info */
    if (pFirstAnycastNDCache != NULL)
    {
        TMO_SLL_Delete (&(pNd6cEntry->anycastDestAddrList),
                        (tTMO_SLL_NODE *) (VOID *) pFirstAnycastNDCache);
        Ip6RelMem (pIf6->pIp6Cxt->u4ContextId,
                   (UINT2) gIp6GblInfo.i4AnycastDstId,
                   (UINT1 *) pFirstAnycastNDCache);
        if (TMO_SLL_Count (&(pNd6cEntry->anycastDestAddrList)) == 0)
        {
            Nd6PurgeCache (pNd6cEntry);
        }
        else
        {
            pNextAnycastNDCache =
                (tAnycastNDCache *)
                TMO_SLL_First (&(pNd6cEntry->anycastDestAddrList));
            pNd6cInfo = (tNd6CacheLanInfo *) (VOID *) pNd6cEntry->pNd6cInfo;

            MEMCPY (pNd6cInfo->lladdr, pNextAnycastNDCache->lladdr,
                    IP6_ENET_ADDR_LEN);
            Ip6AddrCopy (&(pNd6cEntry->anycastSrcAddr),
                         &(pNextAnycastNDCache->sourceAddr6));
            pNd6cInfo->u1LlaLen = IP6_ENET_ADDR_LEN;
        }
    }
    return (IP6_SUCCESS);
}

/*****************************************************************************
* DESCRIPTION : Deletes the NDCache entry and releases
*               the queued IPv6 packets if any
* 
* INPUTS      : pNd6cEntry   -  Pointer to the NDCache Entry
* 
* OUTPUTS     : None
* 
* RETURNS     : IP6_SUCCESS         -  ND entry deletion succeeds
*             : IP6_FAILURE     -  Deletion fails
*****************************************************************************/
INT4
Nd6PurgeCache (tNd6CacheEntry * pNd6cEntry)
{

    tIp6If             *pIf6 = NULL;
    tNd6CacheLanInfo   *pNd6cLinfo = NULL;
    UINT4               u4ContextId = IP6_DEFAULT_CONTEXT;
    tNd6ProxySrcEntry  *pNd6SrcNode = NULL;
    tNd6ProxySrcEntry  *pNd6SrcNextNode = NULL;

    if (pNd6cEntry == NULL)
        return (IP6_FAILURE);

    pIf6 = pNd6cEntry->pIf6;
    if ((pIf6 == NULL) || (pIf6->pIp6Cxt == NULL))
    {
        return (IP6_FAILURE);
    }
    if (pIf6->pIp6Cxt != NULL)
    {
        u4ContextId = pIf6->pIp6Cxt->u4ContextId;
    }
    IP6_TRC_ARG1 (u4ContextId, ND6_MOD_TRC, MGMT_TRC, ND6_NAME,
                  "ND6: Nd6PurgeCache:Purging Cache entry %p\n", pNd6cEntry);

    /*
     * Stop the ND Timer running and release the queued packets if any
     * on dynamic LAN Cache entry
     */

    /* ND cache entries can exist for either tunnel or ethernet interface */

    if ((pIf6->u1IfType == IP6_ENET_INTERFACE_TYPE)
        || (pIf6->u1IfType == IP6_PSEUDO_WIRE_INTERFACE_TYPE)
        || (pIf6->u1IfType == IP6_L3VLAN_INTERFACE_TYPE)
        || (pIf6->u1IfType == IP6_LAGG_INTERFACE_TYPE)
        || (pIf6->u1IfType == IP6_L3SUB_INTF_TYPE)
#ifdef TUNNEL_WANTED
        || (pIf6->u1IfType == IP6_TUNNEL_INTERFACE_TYPE)
#endif
#ifdef WGS_WANTED
        || (pIf6->u1IfType == IP6_L2VLAN_INTERFACE_TYPE)
#endif
        )
    {
        pNd6cLinfo = (tNd6CacheLanInfo *) (VOID *) pNd6cEntry->pNd6cInfo;
        if ((pNd6cEntry->u1ReachState != ND6C_STATIC) &&
            (pNd6cEntry->u1ReachState != ND6C_STATIC_NOT_IN_SERVICE))
        {
            Nd6CancelCacheTimer (pNd6cEntry);
            Nd6PurgePendQue (pNd6cEntry);
        }
        if (pNd6cEntry->u1ReachState == ND6C_STATIC)
        {
            Rtm6ApiDelAllRtWithNxtHopInHWInLnx (u4ContextId,
                                                &pNd6cEntry->addr6);

        }

    }
    /*host behavior */
    if ((pIf6->u1Ipv6IfFwdOperStatus == IP6_IF_FORW_DISABLE)
        && (pNd6cEntry->u1ReachState == ND6C_PROBE))
    {
        Ip6UpdateRoutingDataBase (pNd6cEntry);
    }

#ifdef NPAPI_WANTED
    pNd6cEntry->u1HwStatus = NP_NOT_PRESENT;
    if ((ND6_GET_NODE_STATUS () == RM_ACTIVE)
        && (pNd6cEntry->u1ReachState != ND6C_STALE))
    {
        Nd6RedAddDynamicInfo (pNd6cEntry,
                              ((tNd6CacheLanInfo *) (VOID *)
                               (pNd6cEntry->pNd6cInfo))->lladdr,
                              ND6_RED_DEL_CACHE);
        Nd6RedSendDynamicCacheInfo ();
    }
    if (ND6_IS_NP_PROGRAMMING_ALLOWED () == OSIX_TRUE)
    {
        Ipv6FsNpIpv6NeighCacheDel (u4ContextId, (UINT1 *) &pNd6cEntry->addr6,
                                   pIf6->u4Index);
    }

    pNd6cEntry->u1HwStatus = NP_PRESENT;
    if ((ND6_GET_NODE_STATUS () == RM_ACTIVE)
        && (pNd6cEntry->u1ReachState != ND6C_STALE))
    {
        Nd6RedAddDynamicInfo (pNd6cEntry,
                              ((tNd6CacheLanInfo *) (VOID *)
                               (pNd6cEntry->pNd6cInfo))->lladdr,
                              ND6_RED_DEL_CACHE);
        Nd6RedSendDynamicCacheInfo ();
    }
#else
    pNd6cEntry->u1HwStatus = NP_PRESENT;
#endif /* NPAPI_WANTED */
    if ((pNd6cEntry->u1ReachState != ND6C_STALE) &&
        ((pNd6cEntry->u1StaleToProbe == FALSE) ||
         (pNd6cEntry->u1ReachState != ND6C_PROBE)))
    {
        DecrementNDCounters (pNd6cEntry->u1ReachState);
        gu4Nd6CacheEntries = IP6_ND_ENTRIES_IN_HW;
    }

    switch (pNd6cEntry->u1ReachState)
    {
        case ND6C_INCOMPLETE:
            if (gNd6GblInfo.u4IncompNd6CacheEntries > 0)
            {
                gNd6GblInfo.u4IncompNd6CacheEntries--;
            }
            break;

        case ND6C_STALE:
            if (gNd6GblInfo.u4StaleNd6CacheEntries > 0)
            {
                gNd6GblInfo.u4StaleNd6CacheEntries--;
            }
            break;

        case ND6C_DELAY:
            if (gNd6GblInfo.u4DelayNd6CacheEntries > 0)
            {
                gNd6GblInfo.u4DelayNd6CacheEntries--;
            }
            break;

        case ND6C_PROBE:
            if (gNd6GblInfo.u4ProbeNd6CacheEntries > 0)
            {
                gNd6GblInfo.u4ProbeNd6CacheEntries--;
            }
            break;

        case ND6C_REACHABLE:
            if (gNd6GblInfo.u4ReachNd6CacheEntries > 0)
            {
                gNd6GblInfo.u4ReachNd6CacheEntries--;
            }
            break;
        default:
            break;
    }

    if (gNd6GblInfo.u4Nd6CacheEntries > 0)
    {
        gNd6GblInfo.u4Nd6CacheEntries--;
    }

    RBTreeRem (gNd6GblInfo.Nd6CacheTable, pNd6cEntry);

    /* ND Proxy: Release the NdProxyNsSrcList first */
    UTL_DLL_OFFSET_SCAN (&(pNd6cEntry->NdProxyNsSrcList), pNd6SrcNode,
                         pNd6SrcNextNode, tNd6ProxySrcEntry *)
    {
        TMO_DLL_Delete (&(pNd6cEntry->NdProxyNsSrcList),
                        &(pNd6SrcNode->DllNode));
        if (Ip6RelMem
            (pIf6->pIp6Cxt->u4ContextId, (UINT2) ND6_NS_SRC_MEM_POOL,
             (UINT1 *) pNd6SrcNode) == IP6_FAILURE)
        {
            return (IP6_FAILURE);
        }
    }

    /* ND cache entries can exist for either tunnel or ethernet interface */
    if ((pIf6->u1IfType == IP6_ENET_INTERFACE_TYPE) ||
        (pIf6->u1IfType == IP6_PSEUDO_WIRE_INTERFACE_TYPE) ||
        (pIf6->u1IfType == IP6_L3VLAN_INTERFACE_TYPE) ||
        (pIf6->u1IfType == IP6_LAGG_INTERFACE_TYPE) ||
        (pIf6->u1IfType == IP6_L3SUB_INTF_TYPE)
#ifdef TUNNEL_WANTED
        || (pIf6->u1IfType == IP6_TUNNEL_INTERFACE_TYPE)
#endif
#ifdef WGS_WANTED
        || (pIf6->u1IfType == IP6_L2VLAN_INTERFACE_TYPE)
#endif
        )
    {
        if (pNd6cLinfo != NULL)
        {
            Ip6TmrStop (u4ContextId, ND6_CACHE_ENTRY_STATIC,
                        gIp6GblInfo.Ip6TimerListId,
                        &(pNd6cLinfo->timer.appTimer));
            pNd6cEntry->pNd6cInfo = NULL;
            if (Ip6RelMem (pIf6->pIp6Cxt->u4ContextId,
                           (UINT2) gNd6GblInfo.i4Nd6cLanId,
                           (UINT1 *) pNd6cLinfo) == IP6_FAILURE)
                return (IP6_FAILURE);
        }
    }
    if (Ip6RelMem
        (u4ContextId, (UINT2) gNd6GblInfo.i4Nd6CacheId,
         (UINT1 *) pNd6cEntry) == IP6_FAILURE)
    {
        return (IP6_FAILURE);
    }
    gNd6GblInfo.u4Nd6CacheEntries--;
    if (pIf6->u1Ipv6IfFwdOperStatus == IP6_IF_FORW_ENABLE)    /*router behavior */
    {
        pIf6->pIp6Cxt->u4Nd6CacheEntries--;
    }
    return (IP6_SUCCESS);

}

/******************************************************************************
DESCRIPTION         This is a 'dummy' routine here. It's used as handler for
                    Nd6AddrPrefferedLifeTime.

INPUTS              None.

OUTPUTS             None. 

RETURNS             IP6_SUCCESS 
*******************************************************************************/
VOID
Nd6AddrPrefLifeTimeHandler (tIp6Timer * pTimerNode)
{
    tIp6AddrInfo       *pAddrInfo;

    /* Get address from the timer */
    pAddrInfo =
        (tIp6AddrInfo *) (VOID *) ((UINT1 *) (pTimerNode) -
                                   IP6_OFFSET (tIp6AddrInfo, AddrPrefTimer));

    Ip6TmrStop (pAddrInfo->pIf6->pIp6Cxt->u4ContextId,
                IP6_PREF_LIFETIME_TIMER_ID, gIp6GblInfo.Ip6TimerListId,
                &(pAddrInfo->AddrPrefTimer.appTimer));

    /* Set the address status to deprecated */
    pAddrInfo->u1Status &= (UINT1) ADDR6_DEPRECATED;

    return;
}

/******************************************************************************
DESCRIPTION         This is a 'dummy' routine here. It's used as handler for
                    Nd6AddrValidLifeTime.

INPUTS              None.

OUTPUTS             None. 

RETURNS             IP6_SUCCESS 
*******************************************************************************/
VOID
Nd6AddrValidLifeTimeHandler (tIp6Timer * pTimerNode)
{
    tIp6AddrInfo       *pAddrInfo;

    /* Get address from the timer */
    pAddrInfo =
        (tIp6AddrInfo *) (VOID *) ((UINT1 *) (pTimerNode) -
                                   IP6_OFFSET (tIp6AddrInfo, AddrValidTimer));

    /* Address is no more valid so delete the address */
    Ip6AddrDelete (pAddrInfo->pIf6->u4Index, &pAddrInfo->ip6Addr,
                   pAddrInfo->u1PrefLen, 1);

    return;
}

/******************************************************************************
DESCRIPTION         This is a 'dummy' routine here. It's used to calculate new
                    Reachable Time in case of Router.

INPUTS              None.

OUTPUTS             None. 

RETURNS             IP6_SUCCESS 
*******************************************************************************/
VOID
Nd6CalNewReachTime (tIp6If * pIf6, UINT4 u4ReachTime)
{
    UINT4               u4MaxVal;
    UINT4               u4MinVal;
    UINT4               u4If;

    if ((pIf6 != NULL) && (Ip6IsIfFwdEnabled ((INT4) pIf6->u4Index) == TRUE))
    {
        UNUSED_PARAM (u4ReachTime);
        return;
    }
    /* Compute uniform random reachable time value from the given 
     * base timer value*/

    else
    {
        u4MaxVal = (u4ReachTime * 3) / 2;
        u4MinVal = u4ReachTime / 2;

        if (pIf6 == NULL)
        {
            u4If = 1;

            IP6_IF_SCAN (u4If, (UINT4) IP6_MAX_LOGICAL_IF_INDEX)
            {
                if (Ip6ifEntryExists (u4If) == IP6_SUCCESS)
                {
                    if ((u4ReachTime = Ip6Random (u4MinVal, u4MaxVal)) != 0)
                    {
                        if (gIp6GblInfo.apIp6If[u4If] != NULL)
                        {
                            gIp6GblInfo.apIp6If[u4If]->u4Reachtime =
                                u4ReachTime;
                        }
                    }
                }
            }
        }
        else if ((u4ReachTime = Ip6Random (u4MinVal, u4MaxVal)) != 0)
        {
            pIf6->u4Reachtime = u4ReachTime;
        }
    }
    return;
}

/******************************************************************************
DESCRIPTION         This routine deletes the default router from the list.
                    
INPUTS              *pDefRtEntry - The default route entry to be deleted.
 
OUTPUTS             DefRtList

RETURNS             Void. 
*******************************************************************************/
VOID
Nd6TimeOutDefRoute (tNd6RtEntry * pNd6RtEntry)
{
    tNetIpv6RtInfo      NetIpv6RtInfo;
    UINT4               u4ContextId = 0;

    if (Ip6IsIfFwdEnabled ((INT4) pNd6RtEntry->u4IfIndex) == TRUE)
    {
        return;
    }

    Ip6GetCxtId (pNd6RtEntry->u4IfIndex, &u4ContextId);
    if (pNd6RtEntry->RtEntryTimer.u1Id != 0)
    {
        Ip6TmrStop (u4ContextId, IP6_DEF_RTR_LIFE_TIMER_ID,
                    gIp6GblInfo.Ip6TimerListId,
                    &pNd6RtEntry->RtEntryTimer.appTimer);
    }

    /* Before releasing Rt Entry to pool save the details 
     * in local variable for further processing .
     */

    MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));
    MEMCPY (&NetIpv6RtInfo.NextHop, &pNd6RtEntry->NextHop, sizeof (tIp6Addr));
    MEMCPY (&NetIpv6RtInfo.Ip6Dst, &pNd6RtEntry->Dst, sizeof (tIp6Addr));
    NetIpv6RtInfo.u1Prefixlen = pNd6RtEntry->u1PrefLen;
    NetIpv6RtInfo.u4Index = pNd6RtEntry->u4IfIndex;
    NetIpv6RtInfo.i1Proto = IP6_LOCAL_PROTOID;
    NetIpv6RtInfo.i1DefRtrFlag = IP6_DEF_RTR_FLAG;
    NetIpv6RtInfo.u4RowStatus = IP6FWD_DESTROY;
    NetIpv6RtInfo.u2ChgBit = IP6_BIT_STATUS;
    NetIpv6RtInfo.u4ContextId = u4ContextId;
    TMO_SLL_Delete (&Nd6RouteList, (tTMO_SLL_NODE *) (VOID *) pNd6RtEntry);
    Ip6RelMem (NetIpv6RtInfo.u4ContextId, (UINT2) gIp6GblInfo.i4ND6RouteId,
               (UINT1 *) pNd6RtEntry);

    IP6_TASK_UNLOCK ();
    Rtm6ApiIpv6LeakRoute (NETIPV6_DELETE_ROUTE, &NetIpv6RtInfo);
    IP6_TASK_LOCK ();

    return;
}

/***************************************************************************
FUNCTION            ND6DeleteRoutes     
DESCRIPTION         This function deletes the ND routes learnt for the given
                     interface.
Input(s)            u4Index - The deleted interface index       
Output(s)           None
RETURNS             IP6_SUCCESS / IP6_FAILURE
****************************************************************************/
VOID
ND6DeleteRoutes (UINT4 u4Index)
{
    tNd6RtEntry        *pNd6RtEntry = NULL;
    tTMO_SLL_NODE      *pNode = NULL;
    UINT1               u1RtEntrtyCount = 0;
    UINT1               u1Cntr = 0;

    if (Ip6IsIfFwdEnabled ((INT4) u4Index) == TRUE)
    {
        return;
    }

    u1RtEntrtyCount = (UINT1) TMO_SLL_Count (&Nd6RouteList);

    for (u1Cntr = 0; u1Cntr < u1RtEntrtyCount; u1Cntr++)
    {
        pNode = TMO_SLL_Next (&Nd6RouteList, pNd6RtEntry);
        if (pNode == NULL)
        {
            break;
        }
        pNd6RtEntry =
            (tNd6RtEntry *) (VOID *) ((UINT1 *) (pNode) -
                                      IP6_OFFSET (tNd6RtEntry, pNext));
        if (pNd6RtEntry->u4IfIndex == u4Index)
        {
            Nd6TimeOutDefRoute (pNd6RtEntry);
        }
        else if (pNd6RtEntry->u4IfIndex > u4Index)
        {
            break;
        }

    }
    return;

}

/***************************************************************************
FUNCTION            Nd6GetDefRtrLifetime 
DESCRIPTION         This function provides the available lifetime for the   
                     given default route.
Input(s)            i4Index - The interface index       
                    pIp6Addr - The next hop address
Output(s)           pu4LifeTime - The available def. route life time.
RETURNS             IP6_SUCCESS / IP6_FAILURE
****************************************************************************/
INT4
Nd6GetDefRtrLifetime (tIp6Addr * pIp6Addr, INT4 i4Index, UINT4 *pu4LifeTime)
{

    tIp6Addr            Ip6Addr;
    tNd6RtEntry        *pNd6RtEntry = NULL;
    tTMO_SLL_NODE      *pNode = NULL;
    UINT1               u1RtEntrtyCount = 0;
    UINT1               u1Cntr = 0;
    tIp6If             *pIf6 = NULL;

    pIf6 = Ip6ifGetEntry ((UINT4) i4Index);
    if (pIf6 == NULL)
    {
        return IP6_FAILURE;
    }
    if (Ip6IsIfFwdEnabled (i4Index) == TRUE)
    {
        UNUSED_PARAM (pIp6Addr);
        UNUSED_PARAM (pu4LifeTime);
        return IP6_SUCCESS;
    }
    else
    {

        MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));

        u1RtEntrtyCount = (UINT1) TMO_SLL_Count (&Nd6RouteList);

        for (u1Cntr = 0; u1Cntr < u1RtEntrtyCount; u1Cntr++)
        {
            pNode = TMO_SLL_Next (&Nd6RouteList, pNd6RtEntry);
            if (pNode == NULL)
            {
                pNd6RtEntry = NULL;
                break;
            }
            pNd6RtEntry =
                (tNd6RtEntry *) (VOID *) ((UINT1 *) (pNode) -
                                          IP6_OFFSET (tNd6RtEntry, pNext));
            if ((MEMCMP (&(pNd6RtEntry->NextHop), pIp6Addr, sizeof (tIp6Addr))
                 == 0) && ((INT4) pNd6RtEntry->u4IfIndex == i4Index))
            {
                break;
            }
        }

        if (pNd6RtEntry == NULL)
        {
            return IP6_FAILURE;
        }

        if (TmrGetRemainingTime (gIp6GblInfo.Ip6TimerListId,
                                 (tTmrAppTimer *)
                                 & pNd6RtEntry->RtEntryTimer.appTimer,
                                 pu4LifeTime) == TMR_SUCCESS)
        {
            *pu4LifeTime /= (SYS_NUM_OF_TIME_UNITS_IN_A_SEC);
            return IP6_SUCCESS;
        }

        return IP6_FAILURE;
    }
    return IP6_FAILURE;

}

/***************************************************************************
FUNCTION            Nd6GetNextDefRtr 
DESCRIPTION         This function provides the available lifetime for the   
                     given default route.
Input(s)            i4Index - The interface index       
                    pIp6Addr - The next hop address
Output(s)           pu4LifeTime - The available def. route life time.
RETURNS             IP6_SUCCESS / IP6_FAILURE
****************************************************************************/
INT4
Nd6GetNextDefRtr (tIp6Addr * pIp6Addr, tIp6Addr * pNextIp6Addr,
                  INT4 i4RtrIfIndex, INT4 *pi4NextRtrIfIndex)
{
    tNd6RtEntry        *pNd6RtEntry = NULL;
    tTMO_SLL_NODE      *pNode = NULL;
    INT4                i4RetVal = 0;
    UINT1               u1RtEntrtyCount = 0;
    UINT1               u1Cntr = 0;

    MEMSET (pNextIp6Addr, 0, sizeof (tIp6Addr));
    *pi4NextRtrIfIndex = 0;
    u1RtEntrtyCount = (UINT1) TMO_SLL_Count (&Nd6RouteList);

    for (u1Cntr = 0; u1Cntr < u1RtEntrtyCount; u1Cntr++)
    {
        pNode = TMO_SLL_Next (&Nd6RouteList, pNd6RtEntry);
        if (pNode == NULL)
        {
            return IP6_FAILURE;
        }
        pNd6RtEntry =
            (tNd6RtEntry *) (VOID *) ((UINT1 *) (pNode) -
                                      IP6_OFFSET (tNd6RtEntry, pNext));
        i4RetVal =
            MEMCMP (&(pNd6RtEntry->NextHop), pIp6Addr, sizeof (tIp6Addr));
        if (((i4RetVal == 0) && ((INT4) pNd6RtEntry->u4IfIndex > i4RtrIfIndex))
            || (i4RetVal > 0))
        {
            break;
        }
    }
    if (u1Cntr < u1RtEntrtyCount)
    {
        Ip6AddrCopy (pNextIp6Addr, &(pNd6RtEntry->NextHop));
        *pi4NextRtrIfIndex = (INT4) pNd6RtEntry->u4IfIndex;
        return IP6_SUCCESS;
    }
    return IP6_FAILURE;
}

/******************************************************************************
DESCRIPTION         This routine is called from ND6 main module when ever router 
                    receives a router redirect message. As a router doesn't 
                    process a redirect message it just releases the buffer 
                    which has been passed to it.
                    
INPUTS              pIf6 - pointer to the interface structure.
                    pIp6 - pointer to the message header.
                    u2Len - length of the message.
                    pBuf  - pointer to the message buffer.
 
OUTPUTS             None. 

RETURNS             IP6_SUCCESS/ IP6_FAILURE 
*******************************************************************************/

INT4
Nd6RcvRouterRedirect (tIp6If * pIf6, tIp6Hdr * pIp6, UINT2 u2Len,
                      tCRU_BUF_CHAIN_HEADER * pBuf)
{
    UINT1               u1Copy = 0;
    UINT1               u1SecureFlag = IP6_ZERO;
    UINT4               u4ExtnsLen;
    UINT4               u4Nd6Offset = 0;
    tNd6Redirect        nd6Rdirect, *pNd6Rdirect;
    tNd6RtEntry        *pNd6RtEntry = NULL;
    tIp6Addr            IpAddr;

    if (!(IS_ADDR_LLOCAL (pIp6->srcAddr)))
    {

        IP6_TRC_ARG1 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC,
                      ND6_NAME,
                      "ND6: Nd6RcvRouterRedirect:Invalid src address for "
                      "redirect message from %s\n",
                      Ip6PrintAddr (&pIp6->srcAddr));
        return IP6_FAILURE;
    }

    u4Nd6Offset = IP6_BUF_READ_OFFSET (pBuf) - sizeof (tIcmp6PktHdr);
    IP6_BUF_READ_OFFSET (pBuf) -= sizeof (tIcmp6PktHdr);

    if ((pNd6Rdirect = (tNd6Redirect *) Ip6BufRead
         (pBuf, (UINT1 *) &nd6Rdirect, u4Nd6Offset,
          sizeof (tNd6Redirect), u1Copy)) == NULL)
    {
        IP6_TRC_ARG2 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC, BUFFER_TRC,
                      ND6_NAME,
                      "ND6:Nd6RcvRouterRedirect:Receive RD - BufRead Failed! "
                      "BufPtr %p Offset %d\n", pBuf, u4Nd6Offset);
        IP6_TRC_ARG3 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC, BUFFER_TRC,
                      IP6_NAME,
                      "%s buffer error occurred, Type = %d Module = 0x%X "
                      "Bufptr = %p ", ERROR_FATAL_STR, ERR_BUF_READ, pBuf);
        return (IP6_FAILURE);
    }

    /* Validating the received Redirect message */
    if ((Nd6ValidateRedirectMsgInCxt (pIf6->pIp6Cxt, pIp6, pNd6Rdirect, u2Len))
        == IP6_FAILURE)
    {
        /* A host receving invalid redirect messages should silently 
         * discard the message.
         */
        return IP6_FAILURE;
    }

    /* A to-be proxied Redirect msg shall contain a neighbor's IP address and 
     * Proxy's * TLLA in it. This type of ND entry must not be learned by the Proxy.
     * So should skip Nd6ProcessRedirectExtns in proxy case */
    if (IP6_TRUE == Nd6IsProxyEnabled (pIf6))
    {
        Nd6ProxyNeighRedir (pIf6, pIp6, pNd6Rdirect, pBuf, u2Len);
    }
    else                        /* Only update info if proxying is not enabled */
    {
        /* All included options length must be more than 0 */
        /* Check if any options are included in the message */
        /* If target link layer option is there update the Nd cache entry. */

        /* Process the Extension Options */
        u4ExtnsLen = u2Len - sizeof (tNd6Redirect);
        if (Nd6ProcessRedirectExtns (pIp6, pIf6, pNd6Rdirect, pBuf, u4ExtnsLen)
            == IP6_FAILURE)
        {
            return (IP6_FAILURE);
        }

        /* If SeND Mixed mode is enabled */
        if (ND6_SECURE_MIXED == pIf6->u1SeNDStatus)
        {
            pNd6RtEntry = ND6GetRouteEntry (&IpAddr, 0, &pIp6->srcAddr, 0);
            if (NULL != pNd6RtEntry)
            {
                /* If  Entry already exists then check whether it is a secured
                 * entry, and check whether the incoming entry is secured.  If
                 * both are true, then no need to update the cache entry...
                 */
                if ((ND6_SECURE_ENTRY == pNd6RtEntry->u1SecureFlag)
                    && (ND6_UNSECURE_ENTRY != u1SecureFlag))
                {
                    return IP6_FAILURE;
                }
            }
        }
        Nd6UpdateDestCacheEntry (pIf6, pIp6, pNd6Rdirect);
    }

    if (Ip6BufRelease (pIf6->pIp6Cxt->u4ContextId, pBuf, FALSE, ND6_MODULE) !=
        IP6_SUCCESS)
    {
        IP6_TRC_ARG (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                     BUFFER_TRC | ALL_FAILURE_TRC, ND6_NAME,
                     "ND6: Nd6RcvRouterRedirect:Rcv Redirect - BufRelease "
                     "Failed\n");
        IP6_TRC_ARG3 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC, BUFFER_TRC,
                      IP6_NAME,
                      "%s buffer error occurred, Type = %d Module = 0x%X "
                      "Bufptr = %p ", ERROR_FATAL_STR, ERR_BUF_RELEASE, pBuf);
    }

    return (IP6_SUCCESS);

}

/*****************************************************************************
FUNCTION            Ip6GetIfReTransTime 
DESCRIPTION         This function give the If Retransmit time if  the RA Entry 
                    is created and the value is not ZERO otherwise 
                    Default RetransmitTimer for the NODE.

Input(s)            u4IfIndex   -   If Index 
Output(s)           None

RETURNS             RetransmitTime in MilliSeconds
****************************************************************************/
UINT4
Ip6GetIfReTransTime (UINT4 u4IfIndex)
{
    tIp6RAEntry        *pRAEntry = NULL;
    /* Get the ReTrans Time from the If Table */
    tIp6If             *pIf6 = NULL;

    if ((pIf6 = Ip6ifGetEntry (u4IfIndex)) == NULL)
    {
        return (RETRANS_TIMER / IP6_THOUSAND);
    }

    if (Ip6IsIfFwdEnabled ((INT4) u4IfIndex) == TRUE)
    {
        /* Get the ReTrans Time for the RA Table  if it is zero return
         * DEFAULT RETRANS TIME*/
        pRAEntry = Ip6RaTblGetEntry ((INT4) u4IfIndex);
        if ((pRAEntry == NULL) || (pRAEntry->u4RetransmitTime == IP6_ZERO))
        {
            /* No Ra Entry Created so use RFC Node Constant : 1000 MSec */
            return (RETRANS_TIMER / IP6_THOUSAND);
        }
        return (pRAEntry->u4RetransmitTime);
    }
    else
    {
        return ((pIf6->u4Rettime / 1000));
    }
}

/*****************************************************************************
FUNCTION            Ip6GetIfReachTime   
DESCRIPTION         This function give the If Reachable time if the RA Entry 
                    is created and the value is not ZERO otherwise 
                    Default Reachable for the NODE.

Input(s)            u4IfIndex   -   If Index 
Output(s)           None

RETURNS             Reachable Time in MilliSeconds
****************************************************************************/
UINT4
Ip6GetIfReachTime (UINT4 u4IfIndex)
{
    tIp6RAEntry        *pRAEntry = NULL;
    /* Get the Reachable Time from the If Table for host */
    tIp6If             *pIf6 = NULL;

    if ((pIf6 = Ip6ifGetEntry (u4IfIndex)) == NULL)
    {
        return (REACHABLE_TIME);
    }

    if (Ip6IsIfFwdEnabled ((INT4) u4IfIndex) == TRUE)
    {
        /* Get the Reachable Time for the RA Table  if it is zero return
         * DEFAULT REACHABLE TIME*/
        pRAEntry = Ip6RaTblGetEntry ((INT4) u4IfIndex);
        if ((pRAEntry == NULL) || (pRAEntry->u4ReachableTime == IP6_ZERO))
        {
            /* No Ra Entry Created so use RFC Node Constant : 30000 MSec */
            return (REACHABLE_TIME);
        }
        return (pRAEntry->u4ReachableTime);
    }
    else
    {
        return (pIf6->u4Reachtime);
    }
}

/*****************************************************************************
FUNCTION            Ip6CheckRAStatus    
DESCRIPTION         This function check the the give the If is Configured to 
                    send RA or Not. 

Input(s)            u4IfIndex   -   If Index 
Output(s)           None

RETURNS             TRUE / FALSE 
****************************************************************************/
INT1
Ip6CheckRAStatus (INT4 i4IfIndex)
{
    tIp6RAEntry        *pRAEntry = NULL;

    pRAEntry = Ip6RaTblGetEntry (i4IfIndex);
    /* Check the 
     * 1. RARowStatus  == ACTIVE
     * 2. RASendStatus == TRUE  */
    if (pRAEntry != NULL)
    {
        if ((pRAEntry->u1SendAdverts == IPVX_TRUTH_VALUE_TRUE) &&
            (pRAEntry->u1RowStatus == ACTIVE))
        {
            return (TRUE);
        }
    }
    return (FALSE);
}

/*****************************************************************************
* DESCRIPTION : This routine is called when an link-local address is deleted.
*               This routine sends a RA message on the old address with 
*               the router life time set to zero.
*                    
* INPUTS      : pAddr6      -   Pointer to the deleted address
*               pIf6        -   Pointer to the associated interface
* 
* OUTPUTS     : None
* 
* RETURNS     : None
*****************************************************************************/
VOID
Nd6CeaseRAOnAddr (tIp6Addr * pAddr6, tIp6If * pIf6)
{
    UINT4               u4RaCnt = 2;
    tIp6Addr            dstAddr6;
    tIp6Timer          *pRaTimer;

    if (pIf6->u1Ipv6IfFwdOperStatus == IP6_IF_FORW_DISABLE)
    {
        UNUSED_PARAM (pAddr6);

        if ((IP6_IF_RS_TIMER (pIf6))->u1Id != 0)
        {
            Ip6TmrStop (pIf6->pIp6Cxt->u4ContextId,
                        ND6_ROUT_ADV_SOL_TIMER_ID,
                        gIp6GblInfo.Ip6TimerListId,
                        &(IP6_IF_RS_TIMER (pIf6))->appTimer);
        }
        return;
    }
    /* Stop the RA timer running */
    pRaTimer = IP6_IF_RA_TIMER (pIf6);

    if (pRaTimer->u1Id != 0)
    {
        Ip6TmrStop (pIf6->pIp6Cxt->u4ContextId,
                    ND6_ROUT_ADV_SOL_TIMER_ID,
                    gIp6GblInfo.Ip6TimerListId, &(pRaTimer->appTimer));
    }

    SET_ALL_NODES_MULTI (dstAddr6);

    /* Send RA twice on the interface immediately */
    for (u4RaCnt = 0; u4RaCnt < MAX_FINAL_RTR_ADVERTISEMENTS; u4RaCnt++)
    {
        if (Nd6SendRoutAdv (NULL, pIf6, pAddr6, &dstAddr6, ND6_CEASE_RA_ADV)
            == IP6_FAILURE)
        {
            IP6_TRC_ARG1 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                          DATA_PATH_TRC, ND6_NAME,
                          "ND6: Nd6CeaseRAOnAddr:Cease to Advertise RA - "
                          "Send RA failed on IF %d\n", pIf6->u4Index);
        }
    }

    /* Re-initialise the RA initial count */
    IP6_IF_RA_INITIAL_CNT (pIf6) = 0;

    /* Reset the stats on the next RA scheduled time */
    IP6_IF_RA_SCHED_TIME (pIf6) = 0;
    return;
}

/******************************************************************************
DESCRIPTION         This routine processes the RA and acts accordingly.
                    
INPUTS              pIf6        -   Ip6 interface over which this RA is received

                    pIp6        -   Pointer to the IPv6 header of
                                     the received packet

                    pBuf        -   Pointer to the received
                                     IPv6 packet

OUTPUTS             None

RETURNS             IP6_SUCCESS      - if Validation of RA succeeds
                    IP6_FAILURE  - if Validation of RA fails 

*******************************************************************************/
INT4
Nd6ProcessRouterAdv (tIp6If * pIf6, tIp6Hdr * pIp6, tNd6RoutAdv * pNd6RAdv,
                     UINT1 u1SecureFlag)
{
    UINT4               u4ReachTimeRcd = NTOHL (pNd6RAdv->u4ReachTime);
    UINT2               u2LifeTime = NTOHS (pNd6RAdv->u2Lifetime);

    if (!IS_ADDR_UNSPECIFIED (pIp6->srcAddr))
    {
        /* Source address of this RA is not an unspecified address ,
         * So it can be added in the default router list.
         */

        Nd6AddDefRouterList (pIf6, &pIp6->srcAddr, u2LifeTime, u1SecureFlag);

    }
    if (pNd6RAdv->u1HopLmt != 0)
    {
        /* Update the current hop limit to the received value . */

        pIf6->u1Hoplmt = pNd6RAdv->u1HopLmt;
    }

    /*Recd value is in msec */
    u4ReachTimeRcd = NTOHL (pNd6RAdv->u4ReachTime);

    if ((u4ReachTimeRcd != 0) && (pIf6->u4Reachtime != u4ReachTimeRcd))
    {
        /* Update base reachable time and recompute the 
         * new random reachable timer value */
        Nd6CalNewReachTime (pIf6, u4ReachTimeRcd);
    }

    if ((NTOHL (pNd6RAdv->u4RetransTime)) != 0)
    {
        pIf6->u4Rettime = NTOHL (pNd6RAdv->u4RetransTime);
    }

    return IP6_SUCCESS;

}

/******************************************************************************
DESCRIPTION         This routine adds the source address of RA to default 
                    router list based on its life time.
                    
INPUTS              pIf6        -   Ip6 interface over which this RA is received

                    pSrcAddr        Pointer to the source address of the router
                                    from which Advertisement has been received.

                    u2LifeTime  -   Life time advertised by the router.

OUTPUTS             DefRtList

RETURNS             Void. 
*******************************************************************************/
VOID
Nd6AddDefRouterList (tIp6If * pIf6, tIp6Addr * pSrcAddr, UINT2 u2Lifetime,
                     UINT1 u1SecureFlag)
{
    tNd6RtEntry        *pNd6RtEntry = NULL;
    tNetIpv6RtInfo      NetIpv6RtInfo;
    tIp6Addr            IpAddr;
    INT4                i4Status = 0;
    UINT1               u1DefRtCnt = 0;

    MEMSET (&IpAddr, 0, sizeof (tIp6Addr));

    /* Default route is learnt via Neighbor Discovery protocol. */
    pNd6RtEntry = ND6GetRouteEntry (&IpAddr, 0, pSrcAddr, 0);
    /* If SeND Mixed mode is enabled */
    if (ND6_SECURE_MIXED == pIf6->u1SeNDStatus)
    {
        if (NULL != pNd6RtEntry)
        {
            /* Entry already exists */

            /* entry is secure and incoming message is unsecure, discard */
            if ((ND6_SECURE_ENTRY == pNd6RtEntry->u1SecureFlag)
                && (ND6_UNSECURE_ENTRY == u1SecureFlag))
            {
                return;
            }

            /* entry is unsecure and incoming messge is secure, update */
            if ((ND6_UNSECURE_ENTRY == pNd6RtEntry->u1SecureFlag)
                && (ND6_SECURE_ENTRY == u1SecureFlag))
            {
                pNd6RtEntry->u1SecureFlag = ND6_SECURE_ENTRY;
            }
        }
    }
    if (u2Lifetime == 0)
    {
        if (pNd6RtEntry != NULL)
        {
            Nd6TimeOutDefRoute (pNd6RtEntry);
        }
        Nd6DeleteReDirectRoute ();
        return;
    }

    /* ND route for the given next hop doesnot exist */
    if (pNd6RtEntry == NULL)
    {
        /* If max. no of ND routes are learnt, purge the stalest route */
        u1DefRtCnt = (UINT1) TMO_SLL_Count (&Nd6RouteList);
        if (u1DefRtCnt ==
            FsIP6SizingParams[MAX_IP6HOST_ND6_ROUTES_SIZING_ID].
            u4PreAllocatedUnits)
        {
            Nd6PurgeStalestDefRt ();
        }
        if (Nd6AddRtEntry (&IpAddr, 0, pSrcAddr,
                           pIf6->u4Index, u2Lifetime, 0, 0) == IP6_FAILURE)
        {
            IP6_TRC_ARG (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                         ALL_FAILURE_TRC, ND6_NAME,
                         "Nd6AddDefRouterList: Adding a new ND Route Entry"
                         " FAILED\n");
            return;
        }

        /* Add the Default route */
        MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));
        MEMCPY (&NetIpv6RtInfo.NextHop, pSrcAddr, sizeof (tIp6Addr));
        NetIpv6RtInfo.u4Index = pIf6->u4Index;
        /* For Default route metric is always 0 */
        NetIpv6RtInfo.u4Metric = 0;
        NetIpv6RtInfo.i1Proto = IP6_LOCAL_PROTOID;
        NetIpv6RtInfo.i1DefRtrFlag = IP6_DEF_RTR_FLAG;
        NetIpv6RtInfo.i1Type = INDIRECT;
        NetIpv6RtInfo.u4RouteTag = 0;
        NetIpv6RtInfo.u4RowStatus = IP6FWD_ACTIVE;
        NetIpv6RtInfo.u2ChgBit = IP6_BIT_ALL;
        NetIpv6RtInfo.u4ContextId = pIf6->pIp6Cxt->u4ContextId;

        IP6_TASK_UNLOCK ();
        i4Status = Rtm6ApiIpv6LeakRoute (NETIPV6_ADD_ROUTE, &NetIpv6RtInfo);
        IP6_TASK_LOCK ();

        if (i4Status == RTM6_FAILURE)
        {
            Nd6DelRtEntryInCxt (pIf6->pIp6Cxt, &IpAddr, 0, pSrcAddr);
            return;
        }

        /* Initiate address resolution to determine link layer address
         * of this default router.
         */
#ifdef TUNNEL_WANTED
        if (pIf6->u1IfType != IP6_TUNNEL_INTERFACE_TYPE)
        {
            Nd6Resolve (NULL, pIf6, pSrcAddr, NULL, 0);
        }
#else
        Nd6Resolve (NULL, pIf6, pSrcAddr, NULL, 0);
#endif

        return;
    }

    /* ND6 route already available */
    /* Update  the life time of existing entry */
    Ip6TmrRestart (pIf6->pIp6Cxt->u4ContextId,
                   IP6_DEF_RTR_LIFE_TIMER_ID, gIp6GblInfo.Ip6TimerListId,
                   (tTmrAppTimer *) & pNd6RtEntry->RtEntryTimer.
                   appTimer, u2Lifetime);

    return;
}

/*****************************************************************************
DESCRIPTION         This routine validates the redirect message.

INPUTS              pIp6        -   Pointer to the Ip6 header.
 
                    pNd6Rdirect -   Pointer to the redirect message.

                    u2Len       -   Length of the ND payload in the
                                     received IPv6 packet

OUTPUTS             None.

RETURNS             SUCCESS or FAILURE.
****************************************************************************/
INT4
Nd6ValidateRedirectMsgInCxt (tIp6Cxt * pIp6Cxt, tIp6Hdr * pIp6,
                             tNd6Redirect * pNd6Rdirect, UINT2 u2Len)
{
    tIcmp6PktHdr        icmp6Hdr;

    /* Extract Icmp hdr */
    icmp6Hdr = pNd6Rdirect->icmp6Hdr;

    if (Nd6ValidateMsgHdrInCxt (pIp6Cxt, pIp6, &icmp6Hdr) == IP6_FAILURE)
    {
        return (IP6_FAILURE);
    }

    /* ICMP Length MUST be 40 or more octets */
    if (u2Len < sizeof (tNd6Redirect))
    {
        IP6_TRC_ARG1 (pIp6Cxt->u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC,
                      ND6_NAME,
                      "ND6:Nd6ValidateRedirectMsg: Validation of Redirect "
                      "failed - Incorrect Len %d\n", u2Len);
        return (IP6_FAILURE);
    }

    /* Destination address field in icmp redirect MUST not be Multicast 
     * address.
     */
    if (IS_ADDR_MULTI (pNd6Rdirect->destAddr6))
    {
        IP6_TRC_ARG1 (pIp6Cxt->u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC,
                      ND6_NAME,
                      "ND6: Nd6ValidateRedirectMsg:Validation of Redirect "
                      "Failed -  Dest Address %s MUST not be MULTICAST\n",
                      Ip6ErrPrintAddr (ERROR_MINOR, &pNd6Rdirect->destAddr6));
        return (IP6_FAILURE);
    }

    /* Target should either be linklocal or same as icmp destination address */
    if (!
        ((IS_ADDR_LLOCAL (pNd6Rdirect->targAddr6))
         ||
         (MEMCMP
          (&pNd6Rdirect->targAddr6, &pNd6Rdirect->destAddr6,
           sizeof (tIp6Addr)) == 0))
        || (MEMCMP (&pIp6->srcAddr, &pNd6Rdirect->targAddr6, sizeof (tIp6Addr))
            == 0))
    {
        IP6_TRC_ARG2 (pIp6Cxt->u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC,
                      IP6_NAME,
                      "ND6:Nd6ValidateRedirectMsg: Invalid REDIRECT Msg ,"
                      "target = %s != Icmp dest = %s\n",
                      Ip6PrintAddr (&pNd6Rdirect->targAddr6),
                      Ip6PrintAddr (&pNd6Rdirect->destAddr6));
        return (IP6_FAILURE);
    }

    return IP6_SUCCESS;
}

/*****************************************************************************
DESCRIPTION         This routine updates the destination entry if exists other
                    wise creates one.

INPUTS              pIf6        -   Pointer to the Ip6 interface.
 
                    pNd6Rdirect -   Pointer to the redirect message.

OUTPUTS             Routing  table.

RETURNS             None
****************************************************************************/
VOID
Nd6UpdateDestCacheEntry (tIp6If * pIf6, tIp6Hdr * pIp6,
                         tNd6Redirect * pNd6Rdirect)
{
    tIp6Addr           *pDstAddr6 = NULL;
    tIp6Addr            DstAddr;
    tIp6Addr            NextHop;
    tNetIpv6RtInfo      NetIpv6RtInfo;
    UINT4               u4ContextId = 0;
    INT4                i4Status = 0;
    UINT1               u1Prefixlen = 128;
    INT1                i1Type;
    tNd6CacheEntry     *pNd6c = NULL;

    u4ContextId = pIf6->pIp6Cxt->u4ContextId;
    pIf6 = Ip6GetRouteInCxt (u4ContextId, &pNd6Rdirect->destAddr6,
                             &pNd6c, &NetIpv6RtInfo);

    if (pIf6 == NULL)
    {
        IP6_TRC_ARG1 (u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                      "ND6: Nd6UpdateDestCacheEntry:Route not found for "
                      "the given destination address %s\n",
                      Ip6PrintAddr (&pNd6Rdirect->destAddr6));
        return;
    }

    /* Source address of the IP packet should be same as current 
     * first-hop router for the specified destination address.
     */
    i1Type = NetIpv6RtInfo.i1Type;

    if (i1Type == DIRECT)
    {
        pDstAddr6 = &pNd6Rdirect->destAddr6;
    }
    else
    {
        Ip6AddrCopy (&DstAddr, &NetIpv6RtInfo.NextHop);
        pDstAddr6 = &DstAddr;
    }

    if (MEMCMP (&pIp6->srcAddr, pDstAddr6, sizeof (tIp6Addr)) != 0)
    {
        IP6_TRC_ARG2 (u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                      "ND6: Nd6ValidateRedirectMsg:Invalid REDIRECT Msg, "
                      "Source = %s != Icmp dest = %s\n",
                      Ip6PrintAddr (&pIp6->srcAddr), Ip6PrintAddr (pDstAddr6));
        return;
    }

    /* If the earlier route exactly matches the new redirectly route and if
     * is learnt via NDISC, then delete the old route and add the new route.
     */
    if ((MEMCMP (&NetIpv6RtInfo.Ip6Dst, &pNd6Rdirect->destAddr6,
                 sizeof (tIp6Addr)) == 0)
        && (NetIpv6RtInfo.i1Proto == IP6_LOCAL_PROTOID))
    {
        /* Delete old route and re-add the new route. Else we might still
         * be using the old route for forwarding. */
        Nd6DelRtEntryInCxt (pIf6->pIp6Cxt, &NetIpv6RtInfo.Ip6Dst,
                            NetIpv6RtInfo.u1Prefixlen, &NetIpv6RtInfo.NextHop);

        IP6_TASK_UNLOCK ();
        i4Status = Rtm6ApiIpv6LeakRoute (NETIPV6_DELETE_ROUTE, &NetIpv6RtInfo);
        IP6_TASK_LOCK ();

        if (i4Status == RTM6_FAILURE)
        {
            return;
        }
    }

    if (MEMCMP (&pNd6Rdirect->targAddr6, &pNd6Rdirect->destAddr6,
                sizeof (tIp6Addr)) == 0)
    {
        /* Destination is a neighbor , so add a direct route */
        i1Type = DIRECT;
        SET_ADDR_UNSPECIFIED (NextHop);
    }
    else
    {
        /* target address is a better first hop router, next hop  */
        i1Type = INDIRECT;
        MEMCPY (&NextHop, &pNd6Rdirect->targAddr6, sizeof (tIp6Addr));
    }

    if (Nd6AddRtEntry (&pNd6Rdirect->destAddr6, u1Prefixlen,
                       &NextHop, pIf6->u4Index, -1,
                       (UINT1) IP6_ROUTE_TYPE_REDIRECT, 0) == IP6_FAILURE)
    {
        IP6_TRC_ARG3 (u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                      "ND6: Nd6UpdateDestCacheEntry:Dest cache update Failed "
                      "Dst Addr = %s Preflen=0x%X, NextHop = %s\n",
                      Ip6PrintAddr (&pNd6Rdirect->destAddr6), u1Prefixlen,
                      Ip6PrintAddr (&NextHop));
        return;
    }

    MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));
    MEMCPY (&NetIpv6RtInfo.NextHop, &NextHop, sizeof (tIp6Addr));
    MEMCPY (&NetIpv6RtInfo.Ip6Dst, &pNd6Rdirect->destAddr6, sizeof (tIp6Addr));
    NetIpv6RtInfo.u1Prefixlen = u1Prefixlen;
    NetIpv6RtInfo.u4Index = pIf6->u4Index;
    NetIpv6RtInfo.u4Metric = 0;
    NetIpv6RtInfo.i1Proto = IP6_LOCAL_PROTOID;
    NetIpv6RtInfo.i1Type = i1Type;
    NetIpv6RtInfo.u4RouteTag = 0;
    NetIpv6RtInfo.i1DefRtrFlag = 0;
    NetIpv6RtInfo.u4RowStatus = IP6FWD_ACTIVE;
    NetIpv6RtInfo.u2ChgBit = IP6_BIT_ALL;
    NetIpv6RtInfo.u4ContextId = u4ContextId;

    IP6_TASK_UNLOCK ();
    i4Status = Rtm6ApiIpv6LeakRoute (NETIPV6_ADD_ROUTE, &NetIpv6RtInfo);
    IP6_TASK_LOCK ();

    if (i4Status == RTM6_FAILURE)
    {
        Nd6DelRtEntryInCxt (pIf6->pIp6Cxt, &pNd6Rdirect->destAddr6,
                            u1Prefixlen, &NextHop);
        IP6_TRC_ARG3 (u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                      "ND6: Nd6UpdateDestCacheEntry:Dest cache update Failed "
                      "Dst Addr = %s Preflen=0x%X, NextHop = %s\n",
                      Ip6PrintAddr (&pNd6Rdirect->destAddr6), u1Prefixlen,
                      Ip6PrintAddr (&NextHop));
    }
#ifdef TUNNEL_WANTED
    if (pIf6->u1IfType != IP6_TUNNEL_INTERFACE_TYPE)
    {
        Nd6Resolve (NULL, pIf6, &NextHop, NULL, 0);
    }
#else
    Nd6Resolve (NULL, pIf6, &NextHop, NULL, 0);
#endif

    return;
}

/*****************************************************************************
DESCRIPTION         This routine processes the redirect message extentions

INPUTS              pIp6        -   Pointer to the Ip6 header.
 
                    pNd6Rdirect -   Pointer to the redirect message.
                
                    pBuf        -   Pointer to the message buffer.

                    u2ExtnsLen  -   Extention message length.     

OUTPUTS             ND cache table.

RETURNS             IP6_SUCCESS or IP6_FAILURE
****************************************************************************/
INT4
Nd6ProcessRedirectExtns (tIp6Hdr * pIp6, tIp6If * pIf6,
                         tNd6Redirect * pNd6Rdirect,
                         tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4ExtnsLen)
{
    tNd6AddrExt         Nd6Lla;
    tNd6AddrExt        *pNd6Lla = NULL;
    tNd6ExtHdr         *pNd6Ext = NULL;
    tCgaOptions         CgaOptions;
    tUtlSysPreciseTime  SysPreciseTime;
    tNd6CacheEntry     *pNd6cEntry = NULL;
    UINT4               u4TSNew = 0;
    UINT4               u4ExtOffset = 0;
    UINT4               u4AdvFlag = 0;
    UINT4               u4TempOffset = 0;
    UINT4               u4SeNDOptCount = 0;
    UINT1               u1SecureFlag = IP6_ZERO;
    UINT1               au1PubKey[IP6_SEND_CGA_BYTES];
    UINT1               au1Nonce[ND6_NONCE_LENGTH];

    MEMSET (&Nd6Lla, 0, sizeof (tNd6AddrExt));
    MEMSET (&CgaOptions, 0, sizeof (tCgaOptions));
    MEMSET (au1PubKey, 0, IP6_SEND_CGA_BYTES);
    MEMSET (au1Nonce, 0, ND6_NONCE_LENGTH);
    MEMSET (&SysPreciseTime, 0, sizeof (tUtlSysPreciseTime));

    u4TempOffset = IP6_BUF_READ_OFFSET (pBuf) - sizeof (tNd6Redirect);
    CgaOptions.pu1DerPubKey = au1PubKey;

    /* Extract the Extension Options */
    while (u4ExtnsLen)
    {
        if ((pNd6Ext = Nd6ExtractExtnHdrInCxt (pIf6->pIp6Cxt, pBuf)) == NULL)
        {
            return (IP6_FAILURE);
        }
        u4ExtOffset = IP6_BUF_READ_OFFSET (pBuf) - IP6_TYPE_LEN_BYTES;
        IP6_BUF_READ_OFFSET (pBuf) = u4ExtOffset;

        switch (pNd6Ext->u1Type)
        {

            case ND6_TARG_LLA_EXT:

                /* Setting the override flag */

                u4AdvFlag |= ND6_OVERRIDE_FLAG;
                if ((MEMCMP (&pNd6Rdirect->targAddr6, &pNd6Rdirect->destAddr6,
                             sizeof (tIp6Addr)) != 0))
                {
                    /* Target address and destination address are not equal , 
                     * so target is a better first hop router , 
                     * so set router flag.
                     */

                    u4AdvFlag |= ND6_DEFAULT_ROUTER;
                }

                u4AdvFlag |= ND6_REDIRECT;
                if (Nd6ProcessTLLAOption (u4ExtnsLen, pNd6Ext, pIf6,
                                          &Nd6Lla, &pNd6Lla,
                                          pBuf) != IP6_SUCCESS)
                {
                    IP6_TRC_ARG (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                                 DATA_PATH_TRC, ND6_NAME,
                                 "ND6:Nd6ProcessRedirectExtns: Validation of NA"
                                 " Failed - Invalid TLLA \n");
                    (IP6_SEND_IF_DROP_STATS
                     (pIf6, COUNT_RD)).u4TgtLinkAddrPkts++;
                    (IP6_IF_STATS (pIf6))->u4NdSecureDroppedPkts++;
                    return IP6_FAILURE;
                }
                (IP6_SEND_IF_IN_STATS (pIf6, COUNT_RD)).u4TgtLinkAddrPkts++;
                u4ExtnsLen -= sizeof (tNd6AddrExt);

                break;

            case ND6_REDIRECT_EXT:

                /* Redirected header option is the last one in the 
                 * redirected message so return success.
                 */
                IP6_BUF_READ_OFFSET (pBuf) +=
                    (UINT4) (pNd6Ext->u1Len * BYTES_IN_OCTECT);
                u4ExtnsLen -= (UINT4) (pNd6Ext->u1Len * BYTES_IN_OCTECT);
                (IP6_SEND_IF_IN_STATS (pIf6, COUNT_RD)).u4RedirHrPkts++;
                break;

            case ND6_SEND_CGA_OPT:

                if (IP6_TRUE == IF_SEND_ENABLED (pIf6))
                {
                    if (Nd6SeNDProcessCgaOption (pIf6, pIp6, pBuf,
                                                 pNd6Ext->u1Len,
                                                 &CgaOptions) == IP6_FAILURE)
                    {
                        IP6_TRC_ARG (pIf6->pIp6Cxt->u4ContextId,
                                     ND6_MOD_TRC, BUFFER_TRC,
                                     ND6_NAME, "Nd6ProcessRoutSolExtns:"
                                     " Cga processing failed\n");
                        (IP6_SEND_IF_DROP_STATS
                         (pIf6, COUNT_RD)).u4CgaOptPkts++;
                        (IP6_IF_STATS (pIf6))->u4NdSecureDroppedPkts++;
                        return IP6_FAILURE;
                    }
                    (IP6_SEND_IF_IN_STATS (pIf6, COUNT_RD)).u4CgaOptPkts++;
                    u4SeNDOptCount++;
                }
                else
                {
                    IP6_BUF_READ_OFFSET (pBuf) +=
                        (UINT4) (pNd6Ext->u1Len * BYTES_IN_OCTECT);
                }
                /* extension length is in unit of 8 octets */
                u4ExtnsLen -= (UINT4) (pNd6Ext->u1Len * BYTES_IN_OCTECT);
                break;

            case ND6_SEND_TIMESTAMP_OPT:

                if (IP6_TRUE == IF_SEND_ENABLED (pIf6))
                {
                    if (Nd6SeNDProcessTSOption (pIf6, pIp6, pBuf, &u4TSNew)
                        == IP6_FAILURE)
                    {
                        IP6_TRC_ARG (pIf6->pIp6Cxt->u4ContextId,
                                     ND6_MOD_TRC, BUFFER_TRC,
                                     ND6_NAME, "Nd6ProcessRoutSolExtns:"
                                     " Timestamp processing failed\n");
                        (IP6_SEND_IF_DROP_STATS
                         (pIf6, COUNT_RD)).u4TimeStampOptPkts++;
                        (IP6_IF_STATS (pIf6))->u4NdSecureDroppedPkts++;
                        return IP6_FAILURE;
                    }
                    (IP6_SEND_IF_IN_STATS (pIf6, COUNT_RD)).
                        u4TimeStampOptPkts++;
                    u4SeNDOptCount++;
                }
                else
                {
                    IP6_BUF_READ_OFFSET (pBuf) +=
                        (UINT4) (pNd6Ext->u1Len * BYTES_IN_OCTECT);
                }
                /* extension length is in unit of 8 octets */
                u4ExtnsLen -= (UINT4) (pNd6Ext->u1Len * BYTES_IN_OCTECT);
                break;

            case ND6_SEND_RSA_SIGN_OPT:

                if (IP6_TRUE == IF_SEND_ENABLED (pIf6))
                {
                    if (Nd6SeNDProcessRsaOption (pIf6, pIp6, pBuf,
                                                 pNd6Ext->u1Len, u4TempOffset,
                                                 &CgaOptions) == IP6_FAILURE)
                    {
                        IP6_TRC_ARG (pIf6->pIp6Cxt->u4ContextId,
                                     ND6_MOD_TRC, BUFFER_TRC,
                                     ND6_NAME, "Nd6ProcessRoutSolExtns:"
                                     " RSA processing failed\n");
                        (IP6_SEND_IF_DROP_STATS
                         (pIf6, COUNT_RD)).u4RsaOptPkts++;
                        (IP6_IF_STATS (pIf6))->u4NdSecureDroppedPkts++;
                        return IP6_FAILURE;
                    }
                    (IP6_SEND_IF_IN_STATS (pIf6, COUNT_RD)).u4RsaOptPkts++;
                    u4SeNDOptCount++;
                }
                else
                {
                    IP6_BUF_READ_OFFSET (pBuf) +=
                        (UINT4) (pNd6Ext->u1Len * BYTES_IN_OCTECT);
                }
                /* extension length is in unit of 8 octets */
                u4ExtnsLen -= (UINT4) (pNd6Ext->u1Len * BYTES_IN_OCTECT);
                break;

            default:
                /* Any other options should silently be ignored */
                IP6_BUF_READ_OFFSET (pBuf) +=
                    (UINT4) (pNd6Ext->u1Len * BYTES_IN_OCTECT);
                u4ExtnsLen -= (UINT4) (pNd6Ext->u1Len * BYTES_IN_OCTECT);
                break;
        }
    }

    /* If SeND is enabled in full secure mode then all the four options
     * should present, and in mixed mode either all options should present
     * or no SeND options should present in the packet.
     * if it is not then discard the packet
     */
    if (((ND6_SECURE_ENABLE == pIf6->u1SeNDStatus)
         && u4SeNDOptCount != ND6_SEND_UNSOL_OPT_COUNT)
        || ((ND6_SECURE_MIXED == pIf6->u1SeNDStatus)
            && (!((u4SeNDOptCount == 0)
                  || (u4SeNDOptCount == ND6_SEND_UNSOL_OPT_COUNT)))))
    {
        (IP6_IF_STATS (pIf6))->u4NdSecureInvalidPkts++;
        return (IP6_FAILURE);
    }

    u1SecureFlag = ND6_UNSECURE_ENTRY;
    if ((ND6_SECURE_MIXED != pIf6->u1SeNDStatus)
        && (ND6_SEND_SOL_OPT_COUNT == u4SeNDOptCount))
    {
        u1SecureFlag = ND6_SECURE_ENTRY;
    }

    if (!(IS_ADDR_UNSPECIFIED (pIp6->srcAddr)))
    {
        /* Update the NDCache entry if the source is not an Unspecified address */
        if (pNd6Lla != NULL)
        {
            if (Nd6UpdateCache (pIf6, &pNd6Rdirect->targAddr6,
                                pNd6Lla->lladdr, IP6_ENET_ADDR_LEN,
                                u4AdvFlag, pIp6, u1SecureFlag) == IP6_FAILURE)
            {
                IP6_TRC_ARG (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                             MGMT_TRC | ALL_FAILURE_TRC, ND6_NAME,
                             "ND6:Nd6ProcessTLLAOption: Target LLA Extn, - Update "
                             "Cache Failed \n");
                return (IP6_FAILURE);
            }

            if (IP6_TRUE == IF_SEND_ENABLED (pIf6))
            {
                if (u4TSNew != 0)
                {
                    pNd6cEntry = Nd6IsCacheForAddr (pIf6, &pIp6->srcAddr);
                    if (NULL != pNd6cEntry)
                    {
                        TmrGetPreciseSysTime (&SysPreciseTime);
                        MEMCPY (&pNd6cEntry->u4SendRDLast,
                                &(SysPreciseTime.u4Sec), sizeof (UINT4));
                        pNd6cEntry->u4SendTSLast = u4TSNew;
                    }
                }
            }
        }
    }
    return (IP6_SUCCESS);
}

/*****************************************************************************
DESCRIPTION         This routine performs the processing of prefix option.

INPUTS              pNd6Prefix  -   Pointer to the prefix information.
      
                    pIf6        -   Pointer to the Ip6 interface

OUTPUTS             address list.

RETURNS             SUCCESS or FAILURE.
****************************************************************************/
INT1
Nd6ProcessPrefixOption (tNd6PrefixExt * pNd6Prefix, tIp6If * pIf6,
                        tIp6Hdr * pIp6Hdr)
{
    tIp6Addr            Ip6Addr;
    tIp6Addr            NextHop;
    tNd6RtEntry        *pNd6RtEntry = NULL;
    tNetIpv6RtInfo      NetIpv6RtInfo;
    tIp6AddrInfo       *pAddrInfo = NULL;
    tTMO_SLL_NODE      *pAddrSll = NULL;
    UINT2               u2ProfIndx = 0;
    UINT4               u4RcvdLifeTime = 0;
    UINT4               u4RemainLifeTime = 0;
    INT4                i4RetVal = IP6_FAILURE;
    INT4                i4Status = 0;
#ifdef TUNNEL_WANTED
    UINT4               u4IpAddr = 0;
    UINT4               u4IsaTapId = 0;
#endif
    INT1                i1Type = 0;

    UNUSED_PARAM (pIp6Hdr);
    SET_ADDR_UNSPECIFIED (Ip6Addr);

    /* if the prefix received is a link local prefix a host 
     * should ignore this option and proceed with processing
     * remaining message. This is common for both On-Link and Autonous Flag.
     */
    if ((IS_ADDR_LLOCAL (pNd6Prefix->prefAddr6)))
    {
        return IP6_SUCCESS;
    }

    /* If the ON_LINK Flag is set, then update the Prefix List
     * (Routing Table) as per RFC 2462 */
    if (IS_ONLINK_FLAG_SET (pNd6Prefix->u1PrefFlag))
    {
        /* Check whether the prefix is already present in the prefix list
         * or not. */
        Ip6CopyAddrBits (&Ip6Addr, &pNd6Prefix->prefAddr6,
                         pNd6Prefix->u1PrefLen);

        /* On-Link Flag is set. So next-hop is in the same link. */
        SET_ADDR_UNSPECIFIED (NextHop);
        i1Type = DIRECT;

        u4RcvdLifeTime = NTOHL (pNd6Prefix->u4ValidTime);

        if (u4RcvdLifeTime == 0)
        {
            if (Nd6DelRtEntryInCxt (pIf6->pIp6Cxt, &Ip6Addr,
                                    pNd6Prefix->u1PrefLen, &NextHop)
                == IP6_SUCCESS)
            {
                MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));
                MEMCPY (&NetIpv6RtInfo.NextHop, &NextHop, sizeof (tIp6Addr));
                MEMCPY (&NetIpv6RtInfo.Ip6Dst, &Ip6Addr, sizeof (tIp6Addr));
                NetIpv6RtInfo.u1Prefixlen = pNd6Prefix->u1PrefLen;
                NetIpv6RtInfo.u4Index = pIf6->u4Index;
                NetIpv6RtInfo.i1Proto = IP6_LOCAL_PROTOID;
                NetIpv6RtInfo.u4RowStatus = IP6FWD_DESTROY;
                NetIpv6RtInfo.u2ChgBit = IP6_BIT_STATUS;
                Ip6GetCxtId (pIf6->u4Index, &NetIpv6RtInfo.u4ContextId);

                IP6_TASK_UNLOCK ();
                Rtm6ApiIpv6LeakRoute (NETIPV6_DELETE_ROUTE, &NetIpv6RtInfo);
                IP6_TASK_LOCK ();
            }
        }
        else
        {
            pNd6RtEntry = ND6GetRouteEntry (&Ip6Addr, pNd6Prefix->u1PrefLen,
                                            &NextHop, 0);
            if (pNd6RtEntry == NULL)
            {
                if (Nd6AddRtEntry (&Ip6Addr, pNd6Prefix->u1PrefLen, &NextHop,
                                   pIf6->u4Index, (UINT2) u4RcvdLifeTime, 0, 0)
                    == IP6_FAILURE)
                {
                    IP6_TRC_ARG (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                                 ALL_FAILURE_TRC, ND6_NAME,
                                 "Adding a new ND Route Entry FAILED\n");
                    return IP6_FAILURE;
                }

                MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));
                MEMCPY (&NetIpv6RtInfo.NextHop, &NextHop, sizeof (tIp6Addr));
                MEMCPY (&NetIpv6RtInfo.Ip6Dst, &Ip6Addr, sizeof (tIp6Addr));
                NetIpv6RtInfo.u1Prefixlen = pNd6Prefix->u1PrefLen;
                NetIpv6RtInfo.u4Index = pIf6->u4Index;
                NetIpv6RtInfo.u4Metric = 1;
                NetIpv6RtInfo.i1Proto = IP6_LOCAL_PROTOID;
                NetIpv6RtInfo.i1Type = i1Type;
                NetIpv6RtInfo.u4RouteTag = 0;
                NetIpv6RtInfo.i1DefRtrFlag = 0;
                NetIpv6RtInfo.u4RowStatus = IP6FWD_ACTIVE;
                NetIpv6RtInfo.u2ChgBit = IP6_BIT_ALL;
                Ip6GetCxtId (pIf6->u4Index, &NetIpv6RtInfo.u4ContextId);

                IP6_TASK_UNLOCK ();
                i4Status = Rtm6ApiIpv6LeakRoute (NETIPV6_ADD_ROUTE,
                                                 &NetIpv6RtInfo);
                IP6_TASK_LOCK ();

                if (i4Status == RTM6_FAILURE)
                {
                    Nd6DelRtEntryInCxt (pIf6->pIp6Cxt, &Ip6Addr,
                                        pNd6Prefix->u1PrefLen, &NextHop);
                    return IP6_FAILURE;
                }
            }
            else
            {
                Ip6TmrRestart (pIf6->pIp6Cxt->u4ContextId,
                               IP6_DEF_RTR_LIFE_TIMER_ID,
                               gIp6GblInfo.Ip6TimerListId,
                               (tTmrAppTimer *) & pNd6RtEntry->RtEntryTimer.
                               appTimer, (UINT2) u4RcvdLifeTime);
            }
        }
    }

    /* If autonomous flag is set then this prefix option can be used for
     * deriving the automatic stateless address for this interface as per
     * the RFC 2462. If autonomous flag is not set then this prefix option
     * can be used to form the prefix list as per RFC 2461.
     */
    if (IS_AUTONOMOUS_FLAG_SET (pNd6Prefix->u1PrefFlag))
    {
        /* If the preferred lifetime is greater than the valid lifetime silently
         * ignore the option. */
        if (NTOHL (pNd6Prefix->u4PrefTime) > NTOHL (pNd6Prefix->u4ValidTime))
        {
            IP6_TRC_ARG (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                         DATA_PATH_TRC | ALL_FAILURE_TRC, ND6_NAME,
                         "ND6: Nd6ProcessPrefixOption: Preference Time > Valid Time"
                         ". Ignoring Option.\n");
            return IP6_SUCCESS;
        }

        /* Prefixes should not be same across different interfaces. */
        if ((Ip6AddrScanAllIfInCxt
             (pIf6->pIp6Cxt->u4ContextId, &pNd6Prefix->prefAddr6,
              (UINT1) pNd6Prefix->u1PrefLen)) != NULL)
        {
            IP6_TRC_ARG (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                         ALL_FAILURE_TRC, ND6_NAME,
                         "ND6: Nd6ProcessPrefixOption: Prefix already configured on some interface"
                         "Ignoring this Prefix.\n");
            return IP6_SUCCESS;
        }

        /* Check whether the Received Prefix is already present or not. */
        TMO_SLL_Scan (&pIf6->addr6Ilist, pAddrSll, tTMO_SLL_NODE *)
        {
            pAddrInfo = IP6_ADDR_PTR_FROM_SLL (pAddrSll);

            if ((Ip6AddrMatch (&pNd6Prefix->prefAddr6, &pAddrInfo->ip6Addr,
                               pNd6Prefix->u1PrefLen) == TRUE) &&
                (pAddrInfo->u1PrefLen == pNd6Prefix->u1PrefLen))
            {
                /* Matching Entry Found. */
                break;
            }
        }

        if (pAddrSll == NULL)
        {
            /* Received Prefix Does not match the existing Address Prefix. */
            if (pNd6Prefix->u4ValidTime == 0)
            {
                return IP6_SUCCESS;
            }

#ifdef TUNNEL_WANTED
            /* If the interface is isatap tunnel interface form the isatap
             * address using the prefix */
            if ((pIf6->u1IfType == IP6_TUNNEL_INTERFACE_TYPE) &&
                (pIf6->pTunlIf->u1TunlType == IPV6_ISATAP_TUNNEL))
            {
                if (pNd6Prefix->u1PrefLen != IP6_ISATAP_PREFIX_LEN)
                {
                    IP6_TRC_ARG (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                                 ALL_FAILURE_TRC, ND6_NAME,
                                 "ND6: Nd6ProcessPrefixOption: Sum of Prefix Len and "
                                 "interface id len != 128. Ignoring Option.\n");
                    return IP6_SUCCESS;
                }
                /* Form the isatap address using prefix
                 * isatap address = prefix(64) isatapid(32) v4addr(32) */
                MEMCPY ((UINT1 *) &u4IpAddr,
                        (UINT1 *) &pIf6->pTunlIf->tunlSrc.u1_addr[12], 4);

                MEMCPY (&Ip6Addr, &pNd6Prefix->prefAddr6, IP6_EUI_ADDRESS_LEN);

                u4IsaTapId =
                    ((IS_IP_ADDR_PRIVATE (u4IpAddr)) == OSIX_TRUE) ?
                    IP6_ISATAP_PRIVATE_ID : IP6_ISATAP_GLOBAL_ID;
                u4IsaTapId = OSIX_HTONL (u4IsaTapId);
                MEMCPY (&Ip6Addr.u4_addr[2], &u4IsaTapId, sizeof (UINT4));

                MEMCPY (&Ip6Addr.u4_addr[3], &u4IpAddr, sizeof (UINT4));
            }
            else
#endif
            {
                if ((pNd6Prefix->u1PrefLen + (IP6_EUI_ADDRESS_LEN *
                                              IP6_EUI_ADDRESS_LEN)) !=
                    IP6_ADDR_MAX_PREFIX)
                {
                    /* If the sum of prefix len of received prefix and
                     * interface identifier length is not equal to 128,
                     * ignore the option. */
                    IP6_TRC_ARG (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                                 ALL_FAILURE_TRC, ND6_NAME,
                                 "ND6: Nd6ProcessPrefixOption: Sum of Prefix "
                                 "Len and interface id len != 128. Ignoring Option.\n");
                    return IP6_FAILURE;
                }

                /* Form the address from the received prefix and 
                 * interface id. */
                MEMCPY (&Ip6Addr, &pNd6Prefix->prefAddr6, sizeof (tIp6Addr));

                MEMCPY (&Ip6Addr.u4_addr[2], IP6_IF_TOKEN (pIf6->u4Index),
                        IP6_EUI_ADDRESS_LEN);
            }

            /* Add the address to the interface address list. */
            i4RetVal = Ip6CreateNewProfileIndex (pNd6Prefix, pIf6);
            if (i4RetVal == IP6_FAILURE)
            {
                IP6_TRC_ARG (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                             DATA_PATH_TRC, ND6_NAME,
                             "ND6: Nd6ProcessPrefixOption:"
                             "No free Address profile\n");
                return IP6_FAILURE;
            }
            u2ProfIndx = (UINT2) i4RetVal;

            if ((pAddrInfo = Ip6AddrCreate (pIf6->u4Index, &(Ip6Addr),
                                            pNd6Prefix->u1PrefLen,
                                            ADMIN_DOWN, IP6_ADDR_TYPE_UNICAST,
                                            u2ProfIndx, IP6_ADDR_AUTO_SL))
                == NULL)
            {
                IP6_TRC_ARG (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                             DATA_PATH_TRC, ND6_NAME,
                             "ND6: Nd6ProcessPrefixOption:"
                             "Creating Address profile FAILED\n");
                return IP6_FAILURE;
            }

            if (Ip6AddrUp (pIf6->u4Index, &(Ip6Addr), pNd6Prefix->u1PrefLen,
                           pAddrInfo) == IP6_FAILURE)
            {
                IP6_TRC_ARG (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                             DATA_PATH_TRC, ND6_NAME,
                             "ND6: Nd6ProcessPrefixOption:"
                             "Activating Address profile FAILED\n");
                Ip6AddrDelete (pIf6->u4Index, &Ip6Addr, pNd6Prefix->u1PrefLen,
                               TRUE);
                return IP6_FAILURE;
            }

            pAddrInfo->u1AdminStatus = IP6FWD_ACTIVE;

            /* Start the valid life timer for this prefix. */
            pAddrInfo->AddrValidTimer.u1Id = IP6_VALID_LIFETIME_TIMER_ID;
            if (NTOHL (pNd6Prefix->u4ValidTime) != IP6_ADDR_PROF_MAX_VALID_LIFE)
            {
                Ip6TmrStart (pIf6->pIp6Cxt->u4ContextId,
                             pAddrInfo->AddrValidTimer.u1Id,
                             gIp6GblInfo.Ip6TimerListId,
                             &pAddrInfo->AddrValidTimer.appTimer,
                             NTOHL (pNd6Prefix->u4ValidTime));
            }

            /* Start the preffered life timer for this prefix. */
            if (NTOHL (pNd6Prefix->u4PrefTime) == 0)
            {
                /* Set the address status to deprecated */
                pAddrInfo->u1Status &= (UINT1) ADDR6_DEPRECATED;
            }
            else
            {
                if (NTOHL (pNd6Prefix->u4PrefTime) !=
                    IP6_ADDR_PROF_MAX_PREF_LIFE)
                {
                    Ip6TmrStart (pIf6->pIp6Cxt->u4ContextId,
                                 IP6_PREF_LIFETIME_TIMER_ID,
                                 gIp6GblInfo.Ip6TimerListId,
                                 &pAddrInfo->AddrPrefTimer.appTimer,
                                 NTOHL (pNd6Prefix->u4PrefTime));
                }
                /*Set the address status to prefered */
                pAddrInfo->u1Status |= ADDR6_PREFERRED;
            }
        }
        else
        {
#ifdef MIP6_WANTED
            if (pAddrInfo->u1Status & ADDR6_UNKNOWN)
            {
                return IP6_SUCCESS;
            }
#endif
            if (pAddrInfo->u1ConfigMethod == IP6_ADDR_STATIC)
            {
                /* Matching Address is STATICALLY configured Address. */
                return IP6_SUCCESS;
            }

            /* This condition exists when the address corresponding 
             * to the prefix already exists. So update the preferred and
             * valid life time based on the values received. */
            u2ProfIndx = pAddrInfo->u2Addr6Profile;
            if (u2ProfIndx < gIp6GblInfo.u4MaxAddrProfileLimit)
            {
                u4RcvdLifeTime = NTOHL (pNd6Prefix->u4ValidTime);
                u4RemainLifeTime = 0;
                if (TmrGetRemainingTime (gIp6GblInfo.Ip6TimerListId,
                                         &pAddrInfo->AddrValidTimer.appTimer,
                                         &u4RemainLifeTime) == TMR_FAILURE)
                {
                    u4RemainLifeTime = 0;
                }
                if ((u4RcvdLifeTime >
                     (u4RemainLifeTime / SYS_NUM_OF_TIME_UNITS_IN_A_SEC)) ||
                    (u4RcvdLifeTime >
                     (IP6_RA_DEF_VALID_LIFE_TIME / SYS_TIME_TICKS_IN_A_SEC)))
                {
                    /* Update the Stored Life time of the address */
                    IP6_ADDR_VALID_TIME (u2ProfIndx) = u4RcvdLifeTime;
                    IP6_ADDR_PREF_TIME (u2ProfIndx) =
                        NTOHL (pNd6Prefix->u4PrefTime);
                }

                else if (((u4RemainLifeTime / SYS_NUM_OF_TIME_UNITS_IN_A_SEC) <=
                          (IP6_RA_DEF_VALID_LIFE_TIME /
                           SYS_TIME_TICKS_IN_A_SEC))
                         && (u4RcvdLifeTime >=
                             (u4RemainLifeTime /
                              SYS_NUM_OF_TIME_UNITS_IN_A_SEC))
                         && (u4RcvdLifeTime >=
                             (IP6_RA_DEF_VALID_LIFE_TIME /
                              SYS_TIME_TICKS_IN_A_SEC)))
                {
                    /* If the sender is authenticated, then update the stored Valid
                     * Life time. Else ignore the packet. */
                    /* NOTE : Currently just ignoring the packet. When
                     * Authentication validation is performed this part of the
                     * code needs to be modified.
                     */

                    IP6_ADDR_VALID_TIME (u2ProfIndx) = u4RcvdLifeTime;
                    IP6_ADDR_PREF_TIME (u2ProfIndx) =
                        NTOHL (pNd6Prefix->u4PrefTime);
                }
                else
                {
                    /* Reseting the valid life time to 2 hrs */
                    IP6_ADDR_VALID_TIME (u2ProfIndx) =
                        (IP6_RA_DEF_VALID_LIFE_TIME / SYS_TIME_TICKS_IN_A_SEC);
                    IP6_ADDR_PREF_TIME (u2ProfIndx) =
                        NTOHL (pNd6Prefix->u4PrefTime);
                }

                /* Start the invalidation timer for this prefix */
                if (pAddrInfo->AddrValidTimer.u1Id != 0)
                {
                    Ip6TmrStop (pIf6->pIp6Cxt->u4ContextId,
                                IP6_VALID_LIFETIME_TIMER_ID,
                                gIp6GblInfo.Ip6TimerListId,
                                &(pAddrInfo->AddrValidTimer.appTimer));
                }

                pAddrInfo->AddrValidTimer.u1Id = IP6_VALID_LIFETIME_TIMER_ID;
                Ip6TmrStart (pIf6->pIp6Cxt->u4ContextId,
                             IP6_VALID_LIFETIME_TIMER_ID,
                             gIp6GblInfo.Ip6TimerListId,
                             &pAddrInfo->AddrValidTimer.appTimer,
                             IP6_ADDR_VALID_TIME (u2ProfIndx));

                /* Start the preffered timer for this prefix. */
                if (pAddrInfo->AddrPrefTimer.u1Id != 0)
                {
                    Ip6TmrStop (pIf6->pIp6Cxt->u4ContextId,
                                IP6_PREF_LIFETIME_TIMER_ID,
                                gIp6GblInfo.Ip6TimerListId,
                                &(pAddrInfo->AddrPrefTimer.appTimer));
                }

                if (NTOHL (pNd6Prefix->u4PrefTime) == 0)
                {
                    /* Set the address status to deprecated */
                    pAddrInfo->u1Status &= (UINT1) ADDR6_DEPRECATED;
                }
                else
                {
                    pAddrInfo->AddrPrefTimer.u1Id = IP6_PREF_LIFETIME_TIMER_ID;
                    Ip6TmrStart (pIf6->pIp6Cxt->u4ContextId,
                                 IP6_PREF_LIFETIME_TIMER_ID,
                                 gIp6GblInfo.Ip6TimerListId,
                                 &pAddrInfo->AddrPrefTimer.appTimer,
                                 IP6_ADDR_PREF_TIME (u2ProfIndx));
                    /* Set the address status to preferred */
                    pAddrInfo->u1Status |= ADDR6_PREFERRED;
                }
            }
        }
    }

    return IP6_SUCCESS;
}

/*****************************************************************************
DESCRIPTION         This routine deletes the stalest default route for 
                    creating room for new entries.

INPUTS              None.
      
OUTPUTS             None. 

RETURNS             IP6_SUCCESS or IP6_FAILURE.
****************************************************************************/
INT4
Nd6PurgeStalestDefRt (VOID)
{
    tNd6RtEntry        *pNd6RtEntry = NULL;
    tNd6RtEntry        *pNd6DelRtEntry = NULL;
    tIp6Addr            Ip6Addr;
    tTMO_SLL_NODE      *pNode = NULL;
    INT4                i4RemainingTime = 0;
    INT4                i4TempRemainingTime = 0;
    UINT1               u1RtEntrtyCount = 0;
    UINT1               u1Cntr = 0;

    MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));
    u1RtEntrtyCount = (UINT1) TMO_SLL_Count (&Nd6RouteList);

    for (u1Cntr = 0; u1Cntr < u1RtEntrtyCount; u1Cntr++)
    {
        pNode = TMO_SLL_Next (&Nd6RouteList, pNd6RtEntry);
        pNd6RtEntry =
            (tNd6RtEntry *) (VOID *) ((UINT1 *) (pNode) -
                                      IP6_OFFSET (tNd6RtEntry, pNext));
        /* As the destination address is 0::0 the prefix len is ignored */
        if (MEMCMP (&(pNd6RtEntry->Dst), &Ip6Addr, sizeof (tIp6Addr)) == 0)
        {
            if (TmrGetRemainingTime (gIp6GblInfo.Ip6TimerListId,
                                     (tTmrAppTimer *)
                                     & pNd6RtEntry->RtEntryTimer.appTimer,
                                     (UINT4 *) &i4TempRemainingTime)
                == TMR_FAILURE)
            {
                continue;
            }

            if (i4TempRemainingTime > i4RemainingTime)
            {
                i4RemainingTime = i4TempRemainingTime;
                pNd6DelRtEntry = pNd6RtEntry;
            }
        }
    }

    if (pNd6DelRtEntry != NULL)
    {
        Nd6TimeOutDefRoute (pNd6DelRtEntry);
    }

    return IP6_SUCCESS;
}

/*****************************************************************************
DESCRIPTION         This routine is called when ever an Nd cache entry is going
                    to be purged. This function deletes the destination cache 
                    entries which are are pointing to this ND cache entries, 
                    so that the unreachable next-hop will not be probed 
                    unnecessarily there after. If it becomes reachable that 
                    will be learnt by succeding RAs or Redirects. 

INPUTS              pNd6cEntry - pointer to the ND cache entry which is going 
                                 to be purged.

OUTPUTS             None.

RETURNS             None.
****************************************************************************/
VOID
Ip6UpdateRoutingDataBase (tNd6CacheEntry * pNd6cEntry)
{
    tNd6RtEntry        *pNd6RtEntry = NULL;
    tIp6If             *pIf6 = NULL;
    tIp6Addr            Ip6Addr;
    UINT4               u4Index = 0;

    /* When ever a NDcache entry is purged the destination cache should also 
     * be purged, other wise the unreachable nexthop will always be probed
     * resulting unnecessary NSs and at the same time the packets destined 
     * to that destination are always last even there are other paths to 
     * reach the destination (say a default router).
     */
    pIf6 = pNd6cEntry->pIf6;
    u4Index = pIf6->u4Index;
    MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));

    Nd6DeleteReDirectRoute ();
    pNd6RtEntry = ND6GetRouteEntry (&Ip6Addr, 0, &(pNd6cEntry->addr6), u4Index);

    if (pNd6RtEntry == NULL)
    {
        IP6_TRC_ARG1 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC,
                      ND6_NAME,
                      "Ip6UpdateRoutingDataBase: No default routes exist"
                      " for the address %s\n",
                      Ip6PrintAddr (&(pNd6cEntry->addr6)));
        return;
    }
    Nd6TimeOutDefRoute (pNd6RtEntry);

    return;
}

/*****************************************************************************
* DESCRIPTION : This function forms and sends router solicitation
*
* INPUTS      : pIf6    - Pointer to interface structure over which RS needs 
*                         to be sent
* OUTPUTS     : None. 
* RETURNS     : IP6_SUCCESS or IP6_FAILURE.
*****************************************************************************/
INT4
Nd6SendRouterSol (tIp6If * pIf6, tIp6Addr * pDest)
    /* pDest - Used when RS needs to be sent to unicast destination
     * address of a router. It is useful in router discovery*/
{
    /* Flag indicating to include SLLA option */
    UINT1               u1Flag = 0;
    UINT4               u4Woffset;
    UINT4               u4Nd6Size;
    UINT4               u4TotalSize;
    tNd6CacheEntry     *pNd6cEntry = NULL;
    tIp6Addr           *pSrcAddr6;
    tIp6Addr            SrcAddr6;
    tIp6Addr            DstAddr6;
    tIp6Cxt            *pIp6Cxt = NULL;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    UINT1               u1SeNDFlag = 0;

#ifndef MN_WANTED
    UNUSED_PARAM (pDest);
#endif
    if (pIf6 == NULL)
    {
        IP6_GBL_TRC_ARG (ND6_MOD_TRC, ALL_FAILURE_TRC | BUFFER_TRC, ND6_NAME,
                         "ND6: Nd6SendRouterSol:Send RS - Wrong If is specified \n");
        return IP6_FAILURE;
    }

    u4Woffset = Ip6BufWoffset (ND6_MODULE);
#ifdef MN_WANTED
    if (pDest != NULL)
    {
        if ((pNd6cEntry = Nd6IsCacheForAddr (pIf6, pDest)) == NULL)
        {
            return IP6_FAILURE;
        }
        MEMCPY (&DstAddr6, pDest, sizeof (tIp6Addr));
    }
    else
#endif
        SET_ALL_ROUTERS_MULTI (DstAddr6);

    /* Allocate a Buffer for the IPv6 packet size of RS message */
    u4Nd6Size = sizeof (tNd6RoutSol) + sizeof (tNd6AddrExt);
    u4TotalSize = u4Woffset + u4Nd6Size;

    /*
     * If SeND is enabled, increase the pBuf size to accommodate
     * all the SeND options
     */
    if (IP6_TRUE == IF_SEND_ENABLED (pIf6))
    {
        u4TotalSize += ND6_SEND_OPTIONS_LENGTH;
    }

    if ((pBuf = Ip6BufAlloc (pIf6->pIp6Cxt->u4ContextId, u4TotalSize,
                             u4Woffset, ND6_MODULE)) == NULL)
    {
        IP6_TRC_ARG (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                     BUFFER_TRC | ALL_FAILURE_TRC, ND6_NAME,
                     "ND6: Nd6SendRouterSol:Send RS - BufAlloc Failed\n");
        IP6_TRC_ARG3 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC, BUFFER_TRC,
                      IP6_NAME, "%s buf err: Type = %d  Bufptr = %p",
                      ERROR_FATAL_STR, ERR_BUF_ALLOC, NULL);
        return (IP6_FAILURE);
    }

    /* Source Address of RS should be either the Global unicast address or
     * unspecified Address. */
    pSrcAddr6 = Ip6GetGlobalAddr (pIf6->u4Index, &DstAddr6);

    if (pSrcAddr6 == NULL)
    {
        pSrcAddr6 = Ip6GetLlocalAddr (pIf6->u4Index);
    }

    /* In case no address is available source address should 
     * be set to unspecified.
     */
    if ((pSrcAddr6 != NULL) && (!IS_ADDR_UNSPECIFIED (*pSrcAddr6)))
    {
        /* Address is not unspecified , so we can include SLLA */
        u1Flag = 1;
    }
    else
    {
        u4Nd6Size -= sizeof (tNd6AddrExt);
        pSrcAddr6 = &SrcAddr6;
        SET_ADDR_UNSPECIFIED (*pSrcAddr6);
    }
    Nd6FillRouterSol (u1Flag, pIf6, pBuf);

    IP6_TRC_ARG1 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC,
                  IP6_NAME, "SEND RS to %s\n", Ip6PrintAddr (&DstAddr6));

    pIp6Cxt = pIf6->pIp6Cxt;
    (IP6_IF_STATS (pIf6))->u4OutRoutSols++;
    ICMP6_INC_OUT_RSOLS (pIp6Cxt->Icmp6Stats);

#ifdef TUNNEL_WANTED
    /* In case of tunnel interface pkt need to be enqueued to V4 task */
    if (pIf6->u1IfType == IP6_TUNNEL_INTERFACE_TYPE)
    {
        return (Ip6SendNdMsgOverTunl (pIf6, pNd6cEntry, pSrcAddr6,
                                      &DstAddr6, u4Nd6Size, pBuf));
    }
    else
#endif
    {
        if (IP6_TRUE == IF_SEND_ENABLED (pIf6))
        {
            u1SeNDFlag = ND6_SEND_RS_MSG;
        }

        return (Ip6SendNdMsg
                (pIf6, pNd6cEntry, pSrcAddr6, &DstAddr6, u4Nd6Size, pBuf,
                 u1SeNDFlag, NULL));
    }
}

/*****************************************************************************
* DESCRIPTION : Fill the RS message fields 
*
* INPUTS      : u1Flag      -  Flag indicating whether to include SLLA or not.
*               pIf6        -  Pointer to the interface over whcih RS needs 
*                              to be sent.
*               pBuf        - Pointer to the message buffer. 
*
* OUTPUTS     : None.
* 
* RETURNS     : IP6_SUCCESS or IP6_FAILURE.
*****************************************************************************/
PRIVATE INT4
Nd6FillRouterSol (UINT1 u1Flag, tIp6If * pIf6, tCRU_BUF_CHAIN_HEADER * pBuf)
{
    UINT1               u1Copy = 1;
    UINT1               u1LlaLen = 0, lladdr[IP6_MAX_LLA_LEN];
    tNd6RoutSol         nd6Rsol, *pNd6Rsol;

    pNd6Rsol = (tNd6RoutSol *) & nd6Rsol;
    pNd6Rsol->icmp6Hdr.u1Type = ND6_ROUTER_SOLICITATION;
    pNd6Rsol->icmp6Hdr.u1Code = ND6_RSVD_CODE;    /* Should be 0 */
    pNd6Rsol->icmp6Hdr.u2Chksum = 0;
    pNd6Rsol->u4Rsvd = 0;

    if (Ip6BufWrite (pBuf,
                     (UINT1 *) pNd6Rsol, IP6_BUF_WRITE_OFFSET (pBuf),
                     sizeof (tNd6RoutSol), u1Copy) == IP6_FAILURE)
    {
        IP6_TRC_ARG2 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC,
                      ND6_NAME,
                      "ND6:Nd6FillRouterSol: Fill RS - BufWrite Failed! "
                      "BufPtr %p Offset %d\n", pBuf,
                      IP6_BUF_WRITE_OFFSET (pBuf));
        IP6_TRC_ARG3 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC, BUFFER_TRC,
                      IP6_NAME, "%s buf err: Type = %d  Bufptr = %p",
                      ERROR_FATAL_STR, ERR_BUF_WRITE, pBuf);
        Ip6BufRelease (pIf6->pIp6Cxt->u4ContextId, pBuf, FALSE, ND6_MODULE);
        return (IP6_FAILURE);
    }
    /*
     * Here a check is to be made, such that - if the v6 Source Address
     * is unspecified, do not use the link-layer address option.
     * So, added additional check so that, The LL Address option would
     * be filled only if the Source address is not Unspecified.
     */
    if (u1Flag)
    {
        Ip6ifGetEthLladdr (pIf6, lladdr, &u1LlaLen);
        if (Nd6FillLladdrInCxt (pIf6->pIp6Cxt, lladdr,
                                ND6_SRC_LLA_EXT, pBuf) == IP6_FAILURE)
            return (IP6_FAILURE);
    }

    return (IP6_SUCCESS);

}

/*****************************************************************************
* DESCRIPTION : Performs time-out actions specific
*               to Router Solicitation timer expiry
* 
* INPUTS      : pIf6        -  Pointer to the interface 
*
* OUTPUTS     : None
* 
* RETURNS     : None
*****************************************************************************/

VOID
Nd6RsTimeout (tIp6If * pIf6)
{

    if (pIf6 == NULL)
        return;

    if (pIf6->u1AdminStatus != ADMIN_UP)
        return;

    if (pIf6->u1RsFlag == IP6_IF_NO_RS_SEND)
    {
        /* RA has been received already, so must desist to send further RSs */
        return;
    }

    /*
     * Sends RS and schedules the next RS by starting the RA&RS timer
     * for the computed random time
     */
    if (Nd6SendRouterSol (pIf6, NULL) == IP6_FAILURE)
    {
        IP6_TRC_ARG1 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC,
                      ND6_NAME,
                      "ND6:Nd6RsTimeout: RS Timeout - Send RS failed on IF %d\n",
                      pIf6->u4Index);
        return;
    }
    (IP6_SEND_IF_OUT_STATS (pIf6, COUNT_NA)).u4CgaOptPkts++;
    (IP6_SEND_IF_OUT_STATS (pIf6, COUNT_NA)).u4RsaOptPkts++;
    (IP6_SEND_IF_OUT_STATS (pIf6, COUNT_NA)).u4TimeStampOptPkts++;
    (IP6_SEND_IF_OUT_STATS (pIf6, COUNT_NA)).u4NonceOptPkts++;

    Nd6SchedNextRoutSol (pIf6);

}

/*****************************************************************************
* DESCRIPTION : 
* 
* INPUTS      : pIf6        -  Pointer to the interface 
*
* OUTPUTS     : None
* 
* RETURNS     : None
*****************************************************************************/
VOID
Nd6SchedNextRoutSol (tIp6If * pIf6)
{
    tIp6Timer          *pRsTimer;
    UINT4               u4CurrentTime;

    /* Check whether any RA has been received , if so no further RS 
     * needs to be sent.
     */
    if (pIf6->u1RsFlag == IP6_IF_NO_RS_SEND)
    {
        /* RA has been received already, so must desist to send further RSs */
        return;
    }

    /*
     * Checks whether the number of RSs sent is less than MAX INITIAL RSS
     * and increments the count, initialises the RS timer to RS interval.
     */
    if (IP6_IF_RS_INITIAL_CNT (pIf6) < MAX_RTR_SOLICITATIONS)
    {
        IP6_IF_RS_INITIAL_CNT (pIf6)++;
    }

    if (IP6_IF_RS_INITIAL_CNT (pIf6) != MAX_RTR_SOLICITATIONS)
    {
#ifdef MIP6_WANTED
        IP6_TRC_ARG1 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC,
                      ND6_NAME,
                      "ND6:Nd6SchedNextRoutSol: Schedule Next RS - RS time %d\n",
                      (RTR_SOLICITATION_INTERVAL (pIf6)));
#else
        IP6_TRC_ARG1 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC,
                      ND6_NAME,
                      "ND6:Nd6SchedNextRoutSol: Schedule Next RS - RS time %d\n",
                      RTR_SOLICITATION_INTERVAL);
#endif

        /* Start the RA timer for the computed RA time period */
        pRsTimer = IP6_IF_RS_TIMER (pIf6);
        Ip6TmrStart (pIf6->pIp6Cxt->u4ContextId, ND6_ROUT_ADV_SOL_TIMER_ID,
                     gIp6GblInfo.Ip6TimerListId, &(pRsTimer->appTimer),
#ifdef MIP6_WANTED
                     RTR_SOLICITATION_INTERVAL (pIf6));
#else
                     RTR_SOLICITATION_INTERVAL);
#endif
        OsixGetSysTime (&u4CurrentTime);
        IP6_IF_RS_SCHED_TIME (pIf6) = u4CurrentTime +
#ifdef MIP6_WANTED
            RTR_SOLICITATION_INTERVAL (pIf6);
#else
            RTR_SOLICITATION_INTERVAL;
#endif
    }
}

/***************************************************************************
FUNCTION            ND6GetRouteEntry    
DESCRIPTION         This function retrives the ND route entry added for the    
                     given destination.
Input(s)            pDstAddr  - Destination address of the route
                    u1Prefixlen - Prefix Length
                    pNextHop - NextHop of the ND route.
                    u4Index - Interface Index
Output(s)           None
RETURNS             ND RouteEntry / NULL            
****************************************************************************/
PRIVATE tNd6RtEntry *
ND6GetRouteEntry (tIp6Addr * pDstAddr, UINT1 u1Prefixlen,
                  tIp6Addr * pNextHop, UINT4 u4Index)
{
    tNd6RtEntry        *pNd6RtEntry = NULL;
    tTMO_SLL_NODE      *pNode = NULL;
    UINT1               u1RtEntrtyCount = 0;
    UINT1               u1Cntr = 0;

    u1RtEntrtyCount = (UINT1) TMO_SLL_Count (&Nd6RouteList);

    for (u1Cntr = 0; u1Cntr < u1RtEntrtyCount; u1Cntr++)
    {
        pNode = TMO_SLL_Next (&Nd6RouteList, pNd6RtEntry);
        if (pNode == NULL)
        {
            break;
        }
        pNd6RtEntry =
            (tNd6RtEntry *) (VOID *) ((UINT1 *) (pNode) -
                                      IP6_OFFSET (tNd6RtEntry, pNext));
        if ((MEMCMP (&(pNd6RtEntry->NextHop), pNextHop, sizeof (tIp6Addr)) == 0)
            && (MEMCMP (&(pNd6RtEntry->Dst), pDstAddr, sizeof (tIp6Addr)) == 0)
            && (pNd6RtEntry->u1PrefLen == u1Prefixlen))
        {
            if ((u4Index == 0) || (pNd6RtEntry->u4IfIndex == u4Index))
            {
                return pNd6RtEntry;
            }
        }
    }

    return NULL;
}

/***************************************************************************
FUNCTION            Nd6AddRtEntry       
DESCRIPTION         This function adds a new ND route to the route list and 
                     starts the timer for the route for the given time.
Input(s)            pDstAddr  - Destination address of the route
                    u1Prefixlen - Prefix Length
                    pNextHop - NextHop of the ND route.
                    u4IfIndex - Interface on which the route is learnt
                    u2LifeTime - Valid lifetime for the route
             u1RouteType - route type example redirect/default route    
Output(s)           None
RETURNS             IP6_SUCCESS / IP6_FAILURE
****************************************************************************/
PRIVATE INT4
Nd6AddRtEntry (tIp6Addr * pDstAddr, UINT1 u1Prefixlen, tIp6Addr * pNextHop,
               UINT4 u4IfIndex, UINT2 u2LifeTime, UINT1 u1RouteType,
               UINT1 u1SecureFlag)
{
    tNd6RtEntry        *pNd6RtEntry = NULL;
    tNd6RtEntry        *pNd6PrevRtEntry = NULL;
    tNd6RtEntry        *pNd6CurrRtEntry = NULL;
    tTMO_SLL_NODE      *pNode = NULL;
    UINT4               u4ContextId;
    INT4                i4RetVal = 0;
    UINT1               u1RtEntrtyCount = 0;
    UINT1               u1Cntr = 0;

    Ip6GetCxtId (u4IfIndex, &u4ContextId);
    pNd6RtEntry =
        (tNd6RtEntry *) (VOID *) Ip6GetMem (u4ContextId,
                                            gIp6GblInfo.i4ND6RouteId);
    if (pNd6RtEntry == NULL)
    {
        IP6_TRC_ARG (u4ContextId, ND6_MOD_TRC, ALL_FAILURE_TRC, ND6_NAME,
                     "Nd6AddRtEntry: Creating ND RtEntry Failed "
                     " - Memory Allocation FAILED\n");
        return IP6_FAILURE;
    }

    MEMSET (pNd6RtEntry, 0, sizeof (tNd6RtEntry));
    Ip6AddrCopy (&(pNd6RtEntry->Dst), pDstAddr);
    pNd6RtEntry->u1PrefLen = u1Prefixlen;
    Ip6AddrCopy (&(pNd6RtEntry->NextHop), pNextHop);
    pNd6RtEntry->u4IfIndex = u4IfIndex;
    pNd6RtEntry->u1RouteType = u1RouteType;
    pNd6RtEntry->u1SecureFlag = u1SecureFlag;

    u1RtEntrtyCount = (UINT1) TMO_SLL_Count (&Nd6RouteList);
    for (u1Cntr = 0; u1Cntr < u1RtEntrtyCount; u1Cntr++)
    {
        pNode = TMO_SLL_Next (&Nd6RouteList, pNd6PrevRtEntry);
        if (pNode == NULL)
        {
            break;
        }
        pNd6CurrRtEntry =
            (tNd6RtEntry *) (VOID *) ((UINT1 *) (pNode) -
                                      IP6_OFFSET (tNd6RtEntry, pNext));
        i4RetVal =
            MEMCMP (&(pNd6CurrRtEntry->NextHop), pNextHop, sizeof (tIp6Addr));
        if (((i4RetVal == 0) && (pNd6CurrRtEntry->u4IfIndex > u4IfIndex))
            || (i4RetVal > 0))
        {
            break;
        }
        pNd6PrevRtEntry = pNd6CurrRtEntry;
    }

    TMO_SLL_Insert (&Nd6RouteList, (tTMO_SLL_NODE *) (VOID *) pNd6PrevRtEntry,
                    (tTMO_SLL_NODE *) (VOID *) pNd6RtEntry);

    /* start the invalidation timer */
    if (u2LifeTime != (UINT2) IP6_DEF_ROUTERS_LFE_TIME_INFINITY)
    {
        Ip6TmrStart (u4ContextId, IP6_DEF_RTR_LIFE_TIMER_ID,
                     gIp6GblInfo.Ip6TimerListId,
                     (tTmrAppTimer *) & pNd6RtEntry->RtEntryTimer.appTimer,
                     u2LifeTime);
    }
    return IP6_SUCCESS;
}

/***************************************************************************
FUNCTION            Nd6DelRtEntry       
DESCRIPTION         This function deletes the ND route present in the route
                     list.                      
Input(s)            pDstAddr  - Destination address of the route
                    u1Prefixlen - Prefix Length
                    pNextHop - NextHop of the ND route.
Output(s)           None
RETURNS             IP6_SUCCESS / IP6_FAILURE
****************************************************************************/
PRIVATE INT4
Nd6DelRtEntryInCxt (tIp6Cxt * pIp6Cxt, tIp6Addr * pDstAddr,
                    UINT1 u1Prefixlen, tIp6Addr * pNextHop)
{
    tNd6RtEntry        *pNd6RtEntry = NULL;

    pNd6RtEntry = ND6GetRouteEntry (pDstAddr, u1Prefixlen, pNextHop, 0);
    if (pNd6RtEntry == NULL)
    {
        IP6_TRC_ARG1 (pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                      MGMT_TRC | ALL_FAILURE_TRC, ND6_NAME,
                      "Nd6DelRtEntry: No ND route entry exists for the "
                      "address %s\n", Ip6PrintAddr (pDstAddr));
        return IP6_FAILURE;
    }
    TMO_SLL_Delete (&Nd6RouteList, (tTMO_SLL_NODE *) (VOID *) pNd6RtEntry);
    if (pNd6RtEntry->RtEntryTimer.u1Id != 0)
    {
        Ip6TmrStop (pIp6Cxt->u4ContextId, IP6_DEF_RTR_LIFE_TIMER_ID,
                    gIp6GblInfo.Ip6TimerListId,
                    &pNd6RtEntry->RtEntryTimer.appTimer);
    }

    Ip6RelMem (pIp6Cxt->u4ContextId, (UINT2) gIp6GblInfo.i4ND6RouteId,
               (UINT1 *) pNd6RtEntry);

    return IP6_SUCCESS;
}

/****************************************************************************
 FUNCTION            Nd6DeleteReDirectRoute
 DESCRIPTION         This function deletes the ND redirect route present in
                     the route list.
 Input(s)            None
 Output(s)           None
 RETURNS             IP6_SUCCESS / IP6_FAILURE
*****************************************************************************/
PRIVATE VOID
Nd6DeleteReDirectRoute (VOID)
{
    tNd6RtEntry        *pNd6RtEntry = NULL;
    tTMO_SLL_NODE      *pNode = NULL;
    tNetIpv6RtInfo      NetIpv6RtInfo;
    UINT4               u4ContextId = 0;
    UINT1               u1RtEntrtyCount = 0;
    UINT1               u1Cntr = 0;

    u1RtEntrtyCount = (UINT1) TMO_SLL_Count (&Nd6RouteList);
    for (u1Cntr = 0; u1Cntr < u1RtEntrtyCount; u1Cntr++)
    {
        pNode = TMO_SLL_Next (&Nd6RouteList, pNd6RtEntry);
        if (pNode == NULL)
        {
            break;
        }
        pNd6RtEntry =
            (tNd6RtEntry *) (VOID *) ((UINT1 *) (pNode) -
                                      IP6_OFFSET (tNd6RtEntry, pNext));
        if (pNd6RtEntry->u1RouteType == IP6_ROUTE_TYPE_REDIRECT)
        {
            Ip6GetCxtId (pNd6RtEntry->u4IfIndex, &u4ContextId);
            if (pNd6RtEntry->RtEntryTimer.u1Id != 0)
            {
                Ip6TmrStop (u4ContextId, IP6_DEF_RTR_LIFE_TIMER_ID,
                            gIp6GblInfo.Ip6TimerListId,
                            &pNd6RtEntry->RtEntryTimer.appTimer);
            }
            MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));
            MEMCPY (&NetIpv6RtInfo.NextHop, &pNd6RtEntry->NextHop,
                    sizeof (tIp6Addr));
            MEMCPY (&NetIpv6RtInfo.Ip6Dst, &pNd6RtEntry->Dst,
                    sizeof (tIp6Addr));
            NetIpv6RtInfo.u1Prefixlen = pNd6RtEntry->u1PrefLen;
            NetIpv6RtInfo.u4Index = pNd6RtEntry->u4IfIndex;
            NetIpv6RtInfo.u4Metric = 0;
            NetIpv6RtInfo.i1Proto = IP6_LOCAL_PROTOID;
            NetIpv6RtInfo.i1Type = DIRECT;
            NetIpv6RtInfo.u4RouteTag = 0;
            NetIpv6RtInfo.i1DefRtrFlag = 0;
            NetIpv6RtInfo.u4RowStatus = IP6FWD_DESTROY;
            NetIpv6RtInfo.u2ChgBit = IP6_BIT_STATUS;
            NetIpv6RtInfo.u4ContextId = u4ContextId;

            IP6_TASK_UNLOCK ();
            Rtm6ApiIpv6LeakRoute (NETIPV6_DELETE_ROUTE, &NetIpv6RtInfo);
            IP6_TASK_LOCK ();
            TMO_SLL_Delete (&Nd6RouteList,
                            (tTMO_SLL_NODE *) (VOID *) pNd6RtEntry);
            Ip6RelMem (NetIpv6RtInfo.u4ContextId,
                       (UINT2) gIp6GblInfo.i4ND6RouteId, (UINT1 *) pNd6RtEntry);
            return;
        }

    }
    return;
}

/*****************************************************************************
* DESCRIPTION : Fill the Route Information option field
*
* INPUTS      : pIf6           - Pointer to the interface structure.
*               pRARouteInfo   - Pointer to the Route Information.
*               pBuf           - Pointer to the message buffer.
*               u1LifetimeFlag - Lifetime flag   
*
* OUTPUTS     : None.
*
* RETURNS     : IP6_SUCCESS or IP6_FAILURE.
*****************************************************************************/
INT4
Nd6FillRARouteInfoOption (tIp6If * pIf6, UINT1 u1LifetimeFlag,
                          tIp6RARouteInfoNode * pRARouteInfo,
                          tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 *u4PktLen)
{

    tNd6RARouteInfoExt  Nd6RARouteInfo;
    INT4                i4PrefLen = IP6_ZERO;
    UINT4               u4Len = IP6_ZERO;

    MEMSET (&Nd6RARouteInfo, 0, sizeof (tNd6RARouteInfoExt));

    i4PrefLen = pRARouteInfo->i4RARoutePrefixLen;

    /*
     * Filling the length field.
     * Based on the prefix length, total Length of the option
     * is calculated.
     * Prefix Length > 64, then Prefix is of 16 Octets.
     * Prefix Length < 64, then prefix is of 8 Octets.
     * Prefix Length = 0, then Prefix is not added.
     */

    if (i4PrefLen > IP6_EIGHT_OCTETS)
    {
        Nd6RARouteInfo.u1Length = IP6_THREE;
    }
    if ((i4PrefLen > IP6_ZERO) && (i4PrefLen <= IP6_EIGHT_OCTETS))
    {
        Nd6RARouteInfo.u1Length = IP6_TWO;
    }
    if ((i4PrefLen == IP6_ZERO))
    {
        Nd6RARouteInfo.u1Length = IP6_ONE;
    }

    /* 
     * Filling Type, Prefix Length of the route information option
     */

    Nd6RARouteInfo.u1Type = ND6_RA_ROUTE_INFO_EXT;
    Nd6RARouteInfo.u1PrefixLen = (UINT1) pRARouteInfo->i4RARoutePrefixLen;

    /* 
     *  When ceasing to be an advertising interface, RA should sent with 
     *  Router Lifetime of zero and should set the lifetime of all route
     *  information as zero
     */

    if (pRARouteInfo->i4RARoutePref == IP6_RA_ROUTE_PREF_LOW)
    {
        Nd6RARouteInfo.u1Preference = ND6_RA_PREFERENCE_LOW;
        IP6_TRC_ARG3 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC,
                      ND6_NAME,
                      "ND6: Nd6FillRARouteInfoOption: RouteInfoPrefix: %s Prefix Length: %d Preference: low Lifetime: %u\n",
                      Ip6PrintNtop (&pRARouteInfo->Ip6RARoutePrefix),
                      pRARouteInfo->i4RARoutePrefixLen,
                      pRARouteInfo->u4RARouteLifetime);
    }
    if (pRARouteInfo->i4RARoutePref == IP6_RA_ROUTE_PREF_HIGH)
    {
        Nd6RARouteInfo.u1Preference = ND6_RA_PREFERENCE_HIGH;
        IP6_TRC_ARG3 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC,
                      ND6_NAME,
                      "ND6: Nd6FillRARouteInfoOption: RouteInfoPrefix: %s Prefix Length: %d Preference: high Lifetime: %u\n",
                      Ip6PrintNtop (&pRARouteInfo->Ip6RARoutePrefix),
                      pRARouteInfo->i4RARoutePrefixLen,
                      pRARouteInfo->u4RARouteLifetime);
    }
    if (pRARouteInfo->i4RARoutePref == IP6_RA_ROUTE_PREF_MED)
    {
        Nd6RARouteInfo.u1Preference = ND6_RA_PREFERENCE_MEDIUM;
        IP6_TRC_ARG3 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC,
                      ND6_NAME,
                      "ND6: Nd6FillRARouteInfoOption: RouteInfoPrefix: %s Prefix Length: %d Preference: medium Lifetime: %u\n",
                      Ip6PrintNtop (&pRARouteInfo->Ip6RARoutePrefix),
                      pRARouteInfo->i4RARoutePrefixLen,
                      pRARouteInfo->u4RARouteLifetime);
    }

    if ((u1LifetimeFlag & ND6_RA_ADV)
        && (pIf6->u1Ipv6IfFwdOperStatus == IP6_IF_FORW_ENABLE))
    {
        Nd6RARouteInfo.u4RARouteLifetime =
            HTONL (pRARouteInfo->u4RARouteLifetime);
    }
    else
    {
        Nd6RARouteInfo.u4RARouteLifetime = IP6_ZERO;
        Nd6RARouteInfo.u1Preference = ND6_RA_PREFERENCE_MEDIUM;
    }

    Ip6CopyAddrBits (&Nd6RARouteInfo.Ip6RaRoutePrefix,
                     &pRARouteInfo->Ip6RARoutePrefix, i4PrefLen);

    u4Len = (UINT4) (Nd6RARouteInfo.u1Length * IP6_EIGHT);

    /*
     * Updating the Packet Length based on the correct length of
     * corrent length of the RA Route Information option
     */

    *u4PktLen += u4Len;
    if (Ip6BufWrite (pBuf,
                     (UINT1 *) &Nd6RARouteInfo, IP6_BUF_WRITE_OFFSET (pBuf),
                     u4Len, TRUE) == IP6_FAILURE)
    {
        IP6_TRC_ARG2 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC,
                      ND6_NAME,
                      "ND6:Nd6FillRARouteInfoOption: Fill RA RouteInfo - BufWrite Failed! "
                      "BufPtr %p Offset %d\n", pBuf,
                      IP6_BUF_WRITE_OFFSET (pBuf));
        IP6_TRC_ARG3 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC, BUFFER_TRC,
                      IP6_NAME, "%s buf err: Type = %d  Bufptr = %p",
                      ERROR_FATAL_STR, ERR_BUF_WRITE, pBuf);
        Ip6BufRelease (pIf6->pIp6Cxt->u4ContextId, pBuf, FALSE, ND6_MODULE);
        return (IP6_FAILURE);
    }
    return (IP6_SUCCESS);
}

/**************************** END OF FILE *************************************/
