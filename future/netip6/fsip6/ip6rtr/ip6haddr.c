/*******************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ip6haddr.c,v 1.2 2011/12/21 10:20:00 siva Exp $
 *
 *******************************************************************/

/*
 *-----------------------------------------------------------------------------
 *    FILE NAME                    :    ip6addr.c   
 *
 *    PRINCIPAL AUTHOR             :    Kaushik Biswas 
 *
 *    SUBSYSTEM NAME               :    IPv6
 *
 *    MODULE NAME                  :    IP6  Address Submodule
 *
 *    LANGUAGE                     :    C
 *
 *    TARGET ENVIRONMENT           :    UNIX
 *
 *    DATE OF FIRST RELEASE        :    DDD-MM-1996 
 *
 *    DESCRIPTION                  :    Handles the address related 
 *                                      routines and also routines for 
 *                                      doing Duplicate Address Detection
 *
 *-----------------------------------------------------------------------------
 */

#include "ip6inc.h"

/******************************************************************************
 * DESCRIPTION : This function will be called when ever a host learns an       
 *               on-link prefix. This function will try to get a address       
 *               profile to store the valid and prefferes life time of the     
 *               prefix being learnt.                                          
 * INPUTS      : pIf6 - Interface over which host has learnt the prefix.
 * 
 * OUTPUTS     : None
 *
 * RETURNS     : returns profile index if success otherwise
 *               IP6_MAX_PROFILE_INDEX
 *
 * NOTES       :
 ******************************************************************************/

UINT4
Ip6GetNextFreeProfileIndex (tIp6If * pIf6)
{
    UINT2               u2ProfIndx;
    UINT2               u2EmptyIndex = 0;

    UNUSED_PARAM (pIf6);
    for (u2ProfIndx = 1; ((u2ProfIndx < gIp6GblInfo.u4MaxAddrProfileLimit)
                          && (IP6_ADDR_PROF_ADMIN (u2ProfIndx) ==
                              IP6_ADDR_PROF_VALID)); u2ProfIndx++)
    {
        if ((u2EmptyIndex == 0) && (IP6_ADDR_PROF_REF_COUNT (u2ProfIndx) == 0))
        {
            u2EmptyIndex = u2ProfIndx;
        }
    }

    if (u2ProfIndx == gIp6GblInfo.u4MaxAddrProfileLimit)
    {
        /* So no free profile is existing , scan for an entry
         * which has no prefix Associated with it and use it. Else
         * return FAILURE
         */
        if (u2EmptyIndex != 0)
        {
            u2ProfIndx = u2EmptyIndex;
        }
        else
        {
            /* No Free Profile Exists */
            return IP6_MAX_PROFILE_INDEX;
        }
    }

    return u2ProfIndx;
}

/******************************************************************************
 * DESCRIPTION : This function will check whether any matching profile index
 *               is already existing for the received parameter. If so return
 *               it.
 * INPUTS      : pNd6Prefix - pointer Prefix option in RA.
 *               pIf6       - Interface over which host has learnt the prefix.
 * 
 * OUTPUTS     : None
 *
 * RETURNS     : returns profile index if success otherwise
 *               IP6_MAX_PROFILE_INDEX
 *
 * NOTES       :
******************************************************************************/

UINT4
Ip6GetMatchProfileIndex (tNd6PrefixExt * pNd6Prefix, tIp6If * pIf6)
{
    UINT2               u2ProfIndex;
    UINT1               u1RaPrefCnf = 0;

    UNUSED_PARAM (pIf6);

    if (IS_ONLINK_FLAG_SET (pNd6Prefix->u1PrefFlag))
    {
        u1RaPrefCnf |= IP6_ADDR_ONLINK_ADV;
    }

    if (IS_AUTONOMOUS_FLAG_SET (pNd6Prefix->u1PrefFlag))
    {
        u1RaPrefCnf |= IP6_ADDR_AUTO_ADV;
    }

    for (u2ProfIndex = 0; u2ProfIndex < gIp6GblInfo.u4MaxAddrProfileLimit;
         u2ProfIndex++)
    {
        if ((IP6_ADDR_PROF_ADMIN (u2ProfIndex) == IP6_ADDR_PROF_VALID) &&
            (IP6_ADDR_VALID_TIME (u2ProfIndex) ==
             OSIX_NTOHL (pNd6Prefix->u4ValidTime)) &&
            (IP6_ADDR_PREF_TIME (u2ProfIndex) ==
             OSIX_NTOHL (pNd6Prefix->u4PrefTime)) &&
            ((IP6_ADDR_PROF_CONF (u2ProfIndex) & u1RaPrefCnf) == u1RaPrefCnf))
        {
            /* Matching Entry found */
            return u2ProfIndex;
        }
    }

    return gIp6GblInfo.u4MaxAddrProfileLimit;
}

/******************************************************************************
 * DESCRIPTION : This function will be called when ever a host learns an       
 *               on-link prefix. This function will check whether any 
 *               matching profile index is already existing for the received
 *               parameter. If so return it. Else will create a new address 
 *               profile to store the valid and prefferes life time of the     
 *               prefix being learnt.                                          
 *
 * INPUTS      : pNd6Prefix - pointer Prefix option in RA.
 *               pIf6       - Interface over which host has learnt the prefix.
 * 
 * OUTPUTS     : None
 *
 * RETURNS     : returns profile index if success otherwise IP6_FAILURE
 *
 * NOTES       :
******************************************************************************/

INT4
Ip6CreateNewProfileIndex (tNd6PrefixExt * pNd6Prefix, tIp6If * pIf6)
{
    UINT2               u2ProfIndex;
    UINT4               u4RetVal;

    u4RetVal = Ip6GetMatchProfileIndex (pNd6Prefix, pIf6);
    if (u4RetVal != gIp6GblInfo.u4MaxAddrProfileLimit)
    {
        /* Matching Prefix Already Exists */
        return (UINT2) u4RetVal;
    }

    u4RetVal = Ip6GetNextFreeProfileIndex (pIf6);
    u2ProfIndex = (UINT2) u4RetVal;
    if ((u2ProfIndex >= gIp6GblInfo.u4MaxAddrProfileLimit) ||
        (u2ProfIndex >= MAX_IP6_ADDR_PROFILES_LIMIT))
    {
        return IP6_FAILURE;
    }

    IP6_ADDR_PROF_ADMIN (u2ProfIndex) = IP6_ADDR_PROF_VALID;
    IP6_ADDR_VALID_TIME (u2ProfIndex) = OSIX_NTOHL (pNd6Prefix->u4ValidTime);
    IP6_ADDR_PREF_TIME (u2ProfIndex) = OSIX_NTOHL (pNd6Prefix->u4PrefTime);

    if (IS_ONLINK_FLAG_SET (pNd6Prefix->u1PrefFlag))
    {
        IP6_ADDR_PROF_CONF (u2ProfIndex) |= IP6_ADDR_ONLINK_ADV;
    }
    else
    {
        IP6_ADDR_PROF_CONF (u2ProfIndex) &= ~IP6_ADDR_ONLINK_ADV;
    }

    if (IS_AUTONOMOUS_FLAG_SET (pNd6Prefix->u1PrefFlag))
    {
        IP6_ADDR_PROF_CONF (u2ProfIndex) |= IP6_ADDR_AUTO_ADV;
    }
    else
    {
        IP6_ADDR_PROF_CONF (u2ProfIndex) &= ~IP6_ADDR_AUTO_ADV;
    }

    return u2ProfIndex;
}

/***************************** END OF FILE **********************************/
