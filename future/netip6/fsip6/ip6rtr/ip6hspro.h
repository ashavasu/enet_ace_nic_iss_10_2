#ifndef _IP6HSPRO_H
#define _IP6HSPRO_H
/*---------------------------------------------------------------------------- 
 *    PRINCIPAL AUTHOR             :    Jayabharathi R.
 *
 *    SUBSYSTEM NAME               :    IPv6
 *
 *    MODULE NAME                  :    All Modules
 *
 *    LANGUAGE                     :    C
 *
 *    TARGET ENVIRONMENT           :    UNIX
 *
 *    DATE OF FIRST RELEASE        :    DDD-MM-1996
 *
 *    DESCRIPTION                  :    This file contains extern declarations
 *                                      of the variables and functions which
 *                                      are used by multiple modules of the
 *                                      IPv6 subsystem.
 *
 *----------------------------------------------------------------------------- 
 */
#ifndef IPV6_ROUTER

#include "ip6inc.h"
INT4    Nd6SendRouterSol                PROTO ((tIp6If *pIf6));

INT4    Nd6RcvRouterRedirect            PROTO ((tIp6If *pIf6 ,tIp6Hdr *pIp6,
                                        UINT2 u2Len,
                                        tCRU_BUF_CHAIN_HEADER *pBuf));

INT4    Nd6ProcessPrefixOption          PROTO ((tNd6PrefixExt *p_nd6_prefix,
                                        tIp6If *pIf6));


INT4    Nd6ProcessSLLAOption            PROTO (( UINT2 u2ExtnsLen, 
                                        tNd6ExtHdr *pNd6Ext, tIp6Hdr *pIp6,
                                        tIp6If *pIf6, 
                                        tCRU_BUF_CHAIN_HEADER *pBuf));

INT4    Nd6ProcessTLLAOption            PROTO (( UINT2 u2ExtnsLen, 
                                        UINT4 u4AdvFlag, tIp6Addr *pIp6Addr,
                                        tNd6ExtHdr *pNd6Ext, tIp6Hdr *pIp6,
                                        tIp6If *pIf6, 
                                        tCRU_BUF_CHAIN_HEADER *pBuf));
INT4    Nd6ProcessRouterAdv             PROTO ((tIp6If *pIf6, tIp6Hdr *pIp6,
                                        tNd6RoutAdv *pNd6RAdv));

VOID    Nd6AddDefRouterList            PROTO (( tIp6If *pIf6, 
                                       tIp6Addr *pSrcAddr, UINT2 u2Lifetime));

VOID    Nd6CalNewReachTime             PROTO (( tIp6If *pIf6, 
                                       UINT4 u4ReachTime));
VOID    Nd6TimeOutDefRoute             PROTO ((tIp6RtEntry *pDefRtEntry));


INT4    Nd6PurgeStalestDefRt           PROTO ((UINT1 *pu1Index));


INT4    Nd6ValidateRedirectMsg         PROTO (( tIp6Hdr *pIp6, 
                                       tNd6Redirect *pNd6Rdirect, 
                                       UINT2 u2Len));

VOID    Nd6UpdateDestCacheEntry        PROTO (( tIp6If *pIf6, 
                                       tNd6Redirect *pNd6Rdirect));

INT4    Nd6ProcessRedirectExtns        PROTO (( tIp6Hdr *pIp6, tIp6If *pIf6, 
                                       tNd6Redirect *pNd6Rdirect, 
                                       tCRU_BUF_CHAIN_HEADER *pBuf, 
                                       UINT2 u2ExtnsLen));

INT4    Nd6ValidateRedirectMsg         PROTO (( tIp6Hdr *pIp6, 
                                       tNd6Redirect *pNd6Rdirect, UINT2 u2Len));

INT4    Nd6ProcessRedirectExtns        PROTO (( tIp6Hdr *pIp6, tIp6If *pIf6, 
                                       tNd6Redirect *pNd6Rdirect, 
                                       tCRU_BUF_CHAIN_HEADER *pBuf, 
                                       UINT2 u2ExtnsLen));

VOID   Nd6AddrPrefLifeTimeHandler      PROTO (( tIp6Timer * pTimerNode));

VOID   Nd6AddrValidLifeTimeHandler     PROTO (( tIp6Timer * pTimerNode));

VOID   Nd6DefRouterTimerHandler        PROTO (( tIp6RtEntry *pDefRt));

INT4   Ip6DefRtrListInit               PROTO (( VOID));

tIp6RtEntry *    Ip6DefRtLookup       PROTO ((tIp6RtEntry *pRtEntryHead));

tIp6RtEntry * 
         Ip6FillDefRt                  PROTO ((tIp6Addr *pNextHop, 
                                       UINT1 u1Metric, INT1 i1RtType,
                                       INT1 i1Proto, UINT2 u2Index, 
                                       UINT2 u2RtTag));

VOID    Nd6SchedNextRoutSol            PROTO ((tIp6If *pIf6));

VOID    Nd6RsTimeout                   PROTO ((tIp6If  *pIf6));

VOID    Ip6UpdateRoutingDataBase       PROTO ((tNd6CacheEntry *pNd6cEntry));

#endif
#endif
