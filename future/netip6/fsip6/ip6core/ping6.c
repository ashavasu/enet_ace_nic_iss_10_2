/*******************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ping6.c,v 1.17 2016/03/18 13:19:27 siva Exp $
 *************************************************************/
/*
 *----------------------------------------------------------------------------- 
 *    FILE NAME                    :    ping6.c                                 
 *                                                                              
 *    PRINCIPAL AUTHOR             :    Anshul Garg                             
 *                                                                              
 *    SUBSYSTEM NAME               :    IPv6                                    
 *                                                                              
 *    MODULE NAME                  :    PING6 Module                            
 *                                                                              
 *    LANGUAGE                     :    C                                       
 *                                                                              
 *    TARGET ENVIRONMENT           :    UNIX                                    
 *                                                                              
 *    DATE OF FIRST RELEASE        :                                 
 *                                                                              
 *    DESCRIPTION                  :    This file contains C routines used  
 *                                      during ping process
 *                                                                              
 *----------------------------------------------------------------------------- 
 */

#include "ip6inc.h"
#include "secv6.h"
#ifdef PING6_LNXIP_WANTED
#include "lnxoob.h"
#endif

 /* PING6 Data structure */
INT4                gi4Ping6Socket = -1;

/*
 * Local function prototypes
 */

PRIVATE tPing6     *Ping6GetEntry (tIp6Addr ping6Dst, tIp6Addr ping6Src,
                                   UINT2 u2Id, UINT4 u4Index);
PRIVATE UINT1       gau1SendEchoBuf[PING6_MAX_DATA_SIZE];

/*
 * Global variables 
 */
INT4                i4Ping6TablePoolId;
UINT4               gu4MaxPing6Dst;
tPing6             *pPing[MAX_PING6_MAX_DST_LIMIT];
tTimerListId        gPing6TimerListId;
INT4                gIp6AllowUnSecuredPing = IP6_SUCCESS;
UINT4               gu4Ping6Dbg = 0;
UINT2               u2Ping6Id;

/******************************************************************************
 * DESCRIPTION : This function initializes the statistic and status variables
 *               for ping block.
 * INPUTS      : pointer to ping (tPing6 *pPing6)
 * OUTPUTS     : None
 * RETURNS     : None
 * NOTES       :
 ******************************************************************************/

void
Ping6TaskMain (INT1 *pDummy)
{
    tOsixQId            QId;
    UINT4               u4Event = 0;
    UINT4               u4Result = 0;
    tIcmp6Params       *pParams = NULL;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;

    UNUSED_PARAM (pDummy);

    if (OsixCreateQ
        ((const UINT1 *) PING6_TASK_PING_INPUT_QUEUE, IP6_Q_DEPTH_DEF,
         0, &QId) != OSIX_SUCCESS)
    {
        IP6_GBL_TRC_ARG (PING6_MOD_TRC, ALL_FAILURE_TRC, IP6_NAME,
                         "PING6MAIN:Ping6TaskMain:PING6_TASK_PING_INPUT_QUEUE "
                         "Creation Failed \n");
#ifdef ISS_WANTED
        PING6_INIT_COMPLETE (OSIX_FAILURE);
#endif
        return;
    }

    /*
     * Initialize the Ping data structures and the timer list for PING6 task
     */

    if (Ping6Init () == FAILURE)
    {
        OsixDeleteQ (IP6_SELF_NODE,
                     (const UINT1 *) PING6_TASK_PING_INPUT_QUEUE);
#ifdef ISS_WANTED
        PING6_INIT_COMPLETE (OSIX_FAILURE);
#endif
        return;
    }

    u4Result =
        TmrCreateTimerList ((const UINT1 *) PING6_TASK_NAME, PING6_TIMER0_EVENT,
                            NULL, &gPing6TimerListId);

#ifdef ISS_WANTED
    PING6_INIT_COMPLETE (OSIX_SUCCESS);
#endif
    /*
     * wait on events and process them - the events are TIMER event and
     * PING_MESG_RECVD event
     */

    while (1)
    {
        OsixReceiveEvent ((PING6_TIMER0_EVENT | PING6_PING_MESG_RECD_EVENT),
                          OSIX_WAIT, 0, &u4Event);
        if (u4Event & PING6_TIMER0_EVENT)
        {
            Ping6TimerHandler ();
        }

        if (u4Event & PING6_PING_MESG_RECD_EVENT)
        {
            while (OsixReceiveFromQ
                   (SELF, (const UINT1 *) PING6_TASK_PING_INPUT_QUEUE,
                    OSIX_NO_WAIT, 0, &pBuf) == OSIX_SUCCESS)
            {
                pParams = (tIcmp6Params *) Ip6GetParams (pBuf);

                if (pParams == NULL)
                {
                    IP6_GBL_TRC_ARG (PING6_MOD_TRC, BUFFER_TRC, PING6_NAME,
                                     "PING6:Ping6TaskMain: Param buff is NULL\n");
                    Ip6BufRelease (VCM_INVALID_VC, pBuf, FALSE, PING6_MODULE);
                    return;
                }

                Ping6Rcv (pParams, pBuf);
            }
        }
    }
    UNUSED_PARAM (u4Result);
}

/******************************************************************************
 * DESCRIPTION : This routine allocates memory for each ping block and if
 *               it is allocated , assign the value of configurable variables to
 *               ping block and initiallizes all other statistical and status
 *               variables .
 * INPUTS      : None
 * OUTPUTS     : None
 * RETURNS     : None
 * NOTES       : This function is called only at the time of initialization of
 *               PING module.
 ******************************************************************************/

INT4
Ping6Init (VOID)
{
    UINT1               u1Count = 0;

    if (Ping6SizingMemCreateMemPools () == OSIX_FAILURE)
    {
        IP6_GBL_TRC_ARG (PING6_MOD_TRC, ALL_FAILURE_TRC, IP6_NAME,
                         "PING6:Ping6Init:MemPool Creation of PingTable Failed.\n");
        IP6_GBL_TRC_ARG4 (PING6_MOD_TRC, MGMT_TRC, IP6_NAME,
                          "PING6:Ping6Init: %s memory error occurred, Type = %d  "
                          "PoolId = %d Memptr = %p\n",
                          ERROR_FATAL_STR, ERR_MEM_CREATE, NULL, 0);
        return (IP6_FAILURE);
    }

    i4Ping6TablePoolId = PING6MemPoolIds[MAX_PING6_MAX_DST_SIZING_ID];
    gu4MaxPing6Dst =
        (MAX_PING6_MAX_DST_LIMIT <
         FsIP6SizingParams[MAX_PING6_MAX_DST_SIZING_ID].
         u4PreAllocatedUnits ? MAX_PING6_MAX_DST_LIMIT :
         FsPING6SizingParams[MAX_PING6_MAX_DST_SIZING_ID].u4PreAllocatedUnits);

    for (u1Count = 0; u1Count < gu4MaxPing6Dst; u1Count++)
    {
        pPing[u1Count] =
            (tPing6 *) (VOID *) Ip6GetMem (VCM_INVALID_VC, i4Ping6TablePoolId);

        if (pPing[u1Count] != NULL)
        {
            MEMSET (pPing[u1Count], 0, sizeof (tPing6));
            pPing[u1Count]->u1Admin = IP6_PING_INVALID;
        }
        else
        {

            IP6_GBL_TRC_ARG (PING6_MOD_TRC, MGMT_TRC, PING6_NAME,
                             "PING6:Ping6Init:Allocation of Ping records failed\n");
            IP6_GBL_TRC_ARG4 (PING6_MOD_TRC, MGMT_TRC, PING6_NAME,
                              "PING6:Ping6Init: %s mem err:, Type = %d  PoolId = %d Memptr = %p\n",
                              ERROR_FATAL_STR, ERR_MEM_CREATE, NULL, 0);
            return IP6_FAILURE;
        }
    }

    return IP6_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : This routine is called to send a ping packet to IP6 task. In
 *               the function source and destination module id's are assigned
 *               to the packet and is enqueued to the IP6 task.
 * INPUTS      : interface parameter structure (tIcmp6Params *pParam)&
 *               ping packet                   (tCRU_BUF_CHAIN_HEADER *pBuf)
 * OUTPUTS     : None
 * RETURNS     : None
 * NOTES       :
 ******************************************************************************/

VOID
Ping6Send (tIcmp6Params * pParam, tCRU_BUF_CHAIN_HEADER * pBuf)
{

    IP6_GBL_TRC_ARG6 (PING6_MOD_TRC, DATA_PATH_TRC, PING6_NAME,
                      "PING6:Ping6Send:Sending Pkt Type= 0x%X Len=%d Id=%d Seq=%d "
                      "Bufptr= %p Dst= %s\n",
                      pParam->u1Type, pParam->u2Len, pParam->u2Id,
                      pParam->u2Seq, pBuf, Ip6PrintAddr (&pParam->ip6Dst));

    IP6_GBL_TRC_ARG (PING6_MOD_TRC, BUFFER_TRC, PING6_NAME,
                     "PING6:Ping6Send: Output packet is : \n");
    IP6_PKT_DUMP (VCM_INVALID_VC, PING6_MOD_TRC, DUMP_TRC, PING6_NAME, pBuf, 0);

    Ip6SetParams ((tIp6Params *) (VOID *) pParam, pBuf);
    IP6_BUF_READ_OFFSET (pBuf) = 0;

    if (OsixSendToQ (SELF, (const UINT1 *) IP6_TASK_PING_INPUT_QUEUE, pBuf,
                     OSIX_MSG_URGENT) != OSIX_SUCCESS)
    {
        IP6_GBL_TRC_ARG1 (PING6_MOD_TRC, BUFFER_TRC, PING6_NAME,
                          "PING6:Ping6Send: Packet could not be enqueued to the "
                          "IP6 task Bufptr = %p\n", pBuf);
        IP6_GBL_TRC_ARG3 (PING6_MOD_TRC, BUFFER_TRC, PING6_NAME,
                          "PING6:Ping6Send:%s buf err: Type = %d Bufptr = %p",
                          ERROR_FATAL_STR, ERR_GEN_TQUEUE_FAIL, pBuf);
        Ip6RelMem (VCM_INVALID_VC, (UINT2) gIp6GblInfo.i4ParamId,
                   (UINT1 *) pParam);
        Ip6BufRelease (VCM_INVALID_VC, pBuf, FALSE, PING6_MODULE);
        return;
    }

    if (OsixSendEvent
        (SELF, (const UINT1 *) IP6_TASK_NAME,
         IP6_PING_MESG_RECD_EVENT) != OSIX_SUCCESS)
    {
        IP6_GBL_TRC_ARG1 (PING6_MOD_TRC, BUFFER_TRC, PING6_NAME,
                          "PING6:Ping6Send:Event could not be sent to the IP6 task "
                          "Bufptr = %p\n", pBuf);
        IP6_GBL_TRC_ARG3 (PING6_MOD_TRC, BUFFER_TRC, PING6_NAME,
                          "PING6:Ping6Send: %s buf err: Type = %d Bufptr = %p",
                          ERROR_FATAL_STR, ERR_GEN_TQUEUE_FAIL, pBuf);
        return;
    }
}

/******************************************************************************
 * DESCRIPTION : This function is used for storing the start time of the echo
 *               request packet initiated from our side.
 * INPUTS      : ping block (tPing6 *pPing6)
 * OUTPUTS     : None
 * RETURNS     : None
 * NOTES       : Start time is stored only for last MAX_PING6_WIDTH no. of 
 *               packets, so it searches whole array assigned for 
 *               MAX_PING6_WIDTH no. of packets. If already this no. of packets
 *               have been sent, then start time of the earliest of these 
 *               packets is replaced by the new one.
 ******************************************************************************/

VOID
Ping6PutSeqStartTime (tPing6 * pPing6)
{
    UINT1               u1Count = 0;
    UINT4               u4PresentTime = 0;

    u4PresentTime = OsixGetSysUpTime ();

    for (u1Count = 0; u1Count < MAX_PING6_WIDTH; u1Count++)
    {
        if ((pPing6->pktStat[u1Count].i2Seq == -1) ||
            ((pPing6->pktStat[u1Count].i2Seq != -1) &&
             ((PING6_GET_DIFF_TIME
               (u4PresentTime,
                pPing6->pktStat[u1Count].u4StartTime)) >
              pPing6->u2Ping6RcvTimeout)))
        {

            pPing6->pktStat[u1Count].i2Seq = pPing6->u2Seq;
            pPing6->pktStat[u1Count].u4StartTime = u4PresentTime;
            return;
        }
    }

}

/******************************************************************************
 * DESCRIPTION : This routine is called to send echo requests packets to 
 *               remote host. Buffer for the packet is assigned in this
 *               function and after assigning various fields to interface
 *               parameter structure, function Ping6Send () is called to 
 *               enqueue packet to IP6 task
 * INPUTS      : ping block (tPing6 *pPing6)
 * OUTPUTS     : None
 * RETURNS     : None
 * NOTES       :
 ******************************************************************************/

VOID
Ping6SendEchoReq (tPing6 * pPing6)
{
#ifdef PING6_LNXIP_WANTED
    tNetIpv6RtInfo          NetIp6RtInfo;
    tNetIpv6RtInfoQueryMsg  RtQuery;
#endif
    tIcmp6Params           *pParam = NULL;
    tCRU_BUF_CHAIN_HEADER  *pBuf = NULL;
    UINT4                   u4Ping6Offset = 0;
    UINT2                   u2Size = pPing6->u2Ping6Size;
    UINT1                  *pLocalBuf = NULL;
    UINT1                  *pTempLocalBuf = NULL;
    UINT4                   u4Count = 0;

#ifdef PING6_LNXIP_WANTED
    /* If Linux Ip ping flag is defined then check whether the Echo request
     * packet needs to be sent via OOB and call appropriate function to 
     * send via Linux socket */
    MEMSET (&NetIp6RtInfo, 0, sizeof (tNetIpv6RtInfo));
    MEMSET (&RtQuery, 0, sizeof (tNetIpv6RtInfoQueryMsg));

    RtQuery.u4ContextId = pPing6->u4ContextId;
    RtQuery.u1QueryFlag = RTM6_QUERIED_FOR_NEXT_HOP;
    Ip6AddrCopy (&NetIp6RtInfo.Ip6Dst, &pPing6->ping6Dst);
    NetIp6RtInfo.u1Prefixlen = IP6_ADDR_MAX_PREFIX;
    if (NetIpv6GetRoute (&RtQuery, &NetIp6RtInfo) == NETIPV6_SUCCESS)
    {
        if (CfaIsMgmtPort (NetIp6RtInfo.u4Index) == TRUE)
        {
            if (Ping6LnxOobSend (pPing6) == OSIX_FAILURE)
            {
                IP6_GBL_TRC_ARG (PING6_MOD_TRC, ALL_FAILURE_TRC, PING6_NAME,
                         "PING6:Ping6SendEchoReq: Failed to send Echo request via "
                         "Linux OOB Interface \n");
            }
            return;
        }
    }
#endif

    /*Malloc removal */
    pLocalBuf = gau1SendEchoBuf;
    MEMSET (pLocalBuf, 0, PING6_MAX_DATA_SIZE);
    if (pLocalBuf == NULL)
    {
        IP6_GBL_TRC_ARG (PING6_MOD_TRC, BUFFER_TRC, PING6_NAME,
                         "PING6:Ping6SendEchoReq: Memory could not be allocated for"
                         " local buffer\n");
        return;
    }
    u4Ping6Offset = Ip6BufWoffset (PING6_MODULE);
    pBuf = Ip6BufAlloc (VCM_INVALID_VC,
                        pPing6->u2Ping6Size + u4Ping6Offset,
                        u4Ping6Offset, PING6_MODULE);

    if (pBuf == NULL)
    {
        IP6_GBL_TRC_ARG (PING6_MOD_TRC, BUFFER_TRC, PING6_NAME,
                         "PING6:Ping6SendEchoReq: Buffer for ping packet could not "
                         "be allocated\n");
        IP6_GBL_TRC_ARG3 (PING6_MOD_TRC, BUFFER_TRC, PING6_NAME,
                          "PING6:Ping6SendEchoReq:%s buf err: Type =%d Bufptr = %p",
                          ERROR_FATAL_STR, ERR_BUF_ALLOC, NULL);
        MEMSET (pLocalBuf, 0, PING6_MAX_DATA_SIZE);
        return;
    }

    /*
     * fill the Ping data
     */

    pTempLocalBuf = pLocalBuf;
    if (STRCMP (pPing6->PingData, "") != 0)
    {
        for (u4Count = 1; u4Count <= u2Size / ((STRLEN (pPing6->PingData)) -
                                               IP6_TWO); u4Count++)
        {
            MEMCPY (pLocalBuf, (pPing6->PingData) + IP6_TWO,
                    (STRLEN (pPing6->PingData)) - IP6_TWO);
            pLocalBuf = pLocalBuf + ((STRLEN (pPing6->PingData)) - 2);
        }
        pLocalBuf = pTempLocalBuf;
    }
    else
    {
        MEMSET (pLocalBuf, 0xA5, u2Size);
    }

    if (Ip6BufWrite (pBuf, pLocalBuf, 0, u2Size, 1) == IP6_FAILURE)
    {
        IP6_GBL_TRC_ARG (PING6_MOD_TRC, BUFFER_TRC | ALL_FAILURE_TRC,
                         PING6_NAME,
                         "PING6:Ping6SendEchoReq:ping pkt couldn't be alloc Er in "
                         "CRU_BUF_CopyOverBufChain\n");
        IP6_GBL_TRC_ARG3 (PING6_MOD_TRC, BUFFER_TRC, PING6_NAME,
                          "PING6:Ping6SendEchoReq: %s buf err: Type = %d "
                          "Bufptr = %p", ERROR_FATAL_STR, ERR_BUF_ALLOC, NULL);
        MEMSET (pLocalBuf, 0, PING6_MAX_DATA_SIZE);
        Ip6BufRelease (VCM_INVALID_VC, pBuf, FALSE, PING6_MODULE);
        return;
    }
    Ping6PutSeqStartTime (pPing6);

    MEMSET (pLocalBuf, 0, PING6_MAX_DATA_SIZE);
    pPing6->u2SentCount++;
    Ip6TmrStart (VCM_INVALID_VC, PING6_DEST_TIMER_ID, gPing6TimerListId,
                &(pPing6->timer.appTimer), pPing6->u2Ping6RcvTimeout);
    if ((pParam =
         (tIcmp6Params *) (VOID *) Ip6GetMem (VCM_INVALID_VC,
                                              (UINT2) gIp6GblInfo.i4ParamId)) ==
        NULL)
    {
        IP6_GBL_TRC_ARG (PING6_MOD_TRC, BUFFER_TRC, PING6_NAME,
                         "PING6: Ping6SendEchoReq: Buffer for passing params could "
                         "not be allocated\n");
        IP6_GBL_TRC_ARG4 (PING6_MOD_TRC, MGMT_TRC, PING6_NAME,
                          "PING6: Ping6SendEchoReq: %s mem err: Type = %d  PoolId "
                          "= %d Memptr = %p\n",
                          ERROR_FATAL_STR, ERR_BUF_ALLOC, NULL, 0);
        Ip6BufRelease (VCM_INVALID_VC, pBuf, FALSE, PING6_MODULE);
        return;
    }

    if (!IS_ADDR_UNSPECIFIED (pPing6->PingSrcAddr))
    {
        Ip6AddrCopy (&pParam->ip6Src, &pPing6->PingSrcAddr);
    }
    else
    {
        SET_ADDR_UNSPECIFIED (pParam->ip6Src);
    }
    Ip6AddrCopy (&pParam->ip6Dst, &pPing6->ping6Dst);

    pParam->u1Type = ICMP6_ECHO_REQUEST;
    pParam->u1Code = 0;
    pParam->u2Len = pPing6->u2Ping6Size;
    pParam->u2Id = pPing6->u2Id;
    pParam->u2Seq = (UINT2) pPing6->u2Seq++;
    pParam->u4Index = pPing6->u2IfIndex;
    pParam->u4ContextId = pPing6->u4ContextId;

    Ping6Send (pParam, pBuf);
}

/******************************************************************************
 * DESCRIPTION : This routine is called to update the statistics of ping block
 * INPUTS      : ping block (tPing6 *pPing6)
 * OUTPUTS     : None
 * RETURNS     : None
 * NOTES       :
 ******************************************************************************/

VOID
Ping6Update (tPing6 * pPing6, UINT2 u2Seq)
{

    tUtlSysPreciseTime  CurrentPreciseTime;
    UINT4               u4RemainingTimeInSec = 0;
    UINT4               u4RemainingTimeInNSec = 0;

    UNUSED_PARAM (u2Seq);
    MEMSET (&CurrentPreciseTime, 0, sizeof (tUtlSysPreciseTime));

    /*
     * check that the corr. request packet is still awaiting a response
     */

    /* Calculating u4Timetaken in msec using UtlGetPreciseSysTime  */
    UtlGetPreciseSysTime (&CurrentPreciseTime);
    u4RemainingTimeInSec = CurrentPreciseTime.u4Sec;
    u4RemainingTimeInNSec = CurrentPreciseTime.u4NanoSec;

    if (u4RemainingTimeInSec >= pPing6->u4Seconds)
    {
        if (u4RemainingTimeInNSec < pPing6->u4NanoSeconds)
        {
            u4RemainingTimeInSec = CurrentPreciseTime.u4Sec;
            u4RemainingTimeInNSec = CurrentPreciseTime.u4NanoSec;
            u4RemainingTimeInSec--;
            u4RemainingTimeInNSec += (IP_THOUSAND * 1000000);
        }

        u4RemainingTimeInSec = u4RemainingTimeInSec - pPing6->u4Seconds;
        u4RemainingTimeInNSec = u4RemainingTimeInNSec - pPing6->u4NanoSeconds;
    }

    pPing6->u4TimeTaken =
        ((u4RemainingTimeInSec * IP_THOUSAND) +
         (u4RemainingTimeInNSec / 1000000));
    if (pPing6->i2Count == 0)
    {
        pPing6->u2MaxTime = (UINT2) pPing6->u4TimeTaken;
        pPing6->u2MinTime = (UINT2) pPing6->u4TimeTaken;
    }

    else
    {
        if (pPing6->u4TimeTaken > pPing6->u2MaxTime)
        {
            pPing6->u2MaxTime = (UINT2) pPing6->u4TimeTaken;
        }
        if (pPing6->u4TimeTaken < pPing6->u2MinTime)
        {
            pPing6->u2MinTime = (UINT2) pPing6->u4TimeTaken;
        }
    }

    pPing6->i2Count++;
    pPing6->u2Successes++;
    /*
     *                (N1 * M1) + (N2 * M2)
     *   Mean Time =  ----------------------   Where N2 = 1
     *                       N1 + N2           N1 = pPing->u2Success - 1
     */
    pPing6->u2AverageTime =
        (UINT2) ((pPing6->u2MaxTime + pPing6->u2MinTime) / IP_TWO);

}

/******************************************************************************
 * DESCRIPTION : This routine processes the incoming echo packets. If it is a
 *               request packet, after some changes this packet is sent back to
 *               IP6 task. Otherwise it is a reply packet, so after packet 
 *               validation, function Ping6Update () is called to update the
 *               statistics for the packet.
 * INPUTS      : interface parameter structure (tIcmp6Params  *pParam)&
 *               ping packet                    tCRU_BUF_CHAIN_HEADER *pBuf)
 * OUTPUTS     : None
 * RETURNS     : None
 * NOTES       : In echo reply packets ping data is not validated, as it is
 *               assumed that checksum for the packet included data is already
 *               verified in lower layers.
 ******************************************************************************/

VOID
Ping6Rcv (tIcmp6Params * pParams, tCRU_BUF_CHAIN_HEADER * pBuf)
{
    tIp6Addr            ip6Addr;
    tPing6             *pPing6 = NULL;

    IP6_GBL_TRC_ARG6 (PING6_MOD_TRC, DATA_PATH_TRC, PING6_NAME,
                      "PING6:Ping6Rcv: Rcvd PktType= 0x%X Len= %d Id= %d Seq= %d "
                      "BufPtr= %p Src= %s\n",
                      pParams->u1Type, pParams->u2Len, pParams->u2Id,
                      pParams->u2Seq, pBuf, Ip6PrintAddr (&pParams->ip6Src));

    IP6_GBL_TRC_ARG (PING6_MOD_TRC, BUFFER_TRC, PING6_NAME,
                     "PING6:Ping6Rcv: Input packet is : \n");
    IP6_PKT_DUMP (VCM_INVALID_VC, PING6_MOD_TRC, DUMP_TRC, PING6_NAME, pBuf, 0);

    if (pParams->u4ContextId == VCM_INVALID_VC)
    {
        IP6_GBL_TRC_ARG (PING6_MOD_TRC, DATA_PATH_TRC | ALL_FAILURE_TRC,
                         PING6_NAME,
                         "PING6:Ping6Rcv: Invalid context information"
                         " is present in the received ping packet."
                         "Hence, packet is dropped\n");
        Ip6RelMem (VCM_INVALID_VC, (UINT2) gIp6GblInfo.i4ParamId,
                   (UINT1 *) pParams);
        Ip6BufRelease (VCM_INVALID_VC, pBuf, FALSE, PING6_MODULE);
        return;
    }

    if (Ipv6AllowUnSecuredPing () != IP6_SUCCESS)
    {
#ifdef IPSECv6_WANTED
        if (Secv6AccListExist (pParams->ip6Src, pParams->ip6Dst) == SEC_FAILURE)
#endif
        {
            Ip6RelMem (VCM_INVALID_VC, (UINT2) gIp6GblInfo.i4ParamId,
                       (UINT1 *) pParams);
            Ip6BufRelease (VCM_INVALID_VC, pBuf, FALSE, PING6_MODULE);
            return;
        }
    }

    if (pParams->u1Type == ICMP6_ECHO_REQUEST)
    {
        pParams->u1Type = ICMP6_ECHO_REPLY;

        /* If the destination address is loop back don't interchange 
         * source and destination addresses, because loopback can not be 
         * used as source address of ipv6 message.
         */
        if (!IS_ADDR_LOOPBACK (pParams->ip6Dst))
        {
            Ip6AddrCopy (&ip6Addr, &pParams->ip6Src);
            Ip6AddrCopy (&pParams->ip6Src, &pParams->ip6Dst);
            Ip6AddrCopy (&pParams->ip6Dst, &ip6Addr);
        }

        Ping6Send (pParams, pBuf);
        return;
    }

    if (pParams->u1Type == ICMP6_ECHO_REPLY)
    {
        /* If the destination address is loopback while replying source 
         * and destination addresses are not interchanged, so ping entry 
         * should be obtained from destination address of ping message 
         * rather than from source address.
         */
        if (IS_ADDR_LOOPBACK (pParams->ip6Dst))
        {
            pPing6 = Ping6GetEntry (pParams->ip6Dst, pParams->ip6Src,
                                    pParams->u2Id, pParams->u4Index);
        }
        else
        {
            pPing6 = Ping6GetEntry (pParams->ip6Src, pParams->ip6Dst,
                                    pParams->u2Id, pParams->u4Index);
        }

        if (pPing6 != NULL)
        {
            Ping6Update (pPing6, pParams->u2Seq);
        }
        else
        {
            IP6_GBL_TRC_ARG2 (PING6_MOD_TRC, DATA_PATH_TRC, PING6_NAME,
                              "PING6:Ping6Rcv: Received Echo Reply but no Request "
                              "sent, Src = %s Id = %d\n",
                              Ip6ErrPrintAddr (ERROR_MINOR, &pParams->ip6Src),
                              pParams->u2Id);
        }
    }

    Ip6RelMem (VCM_INVALID_VC, (UINT2) gIp6GblInfo.i4ParamId,
               (UINT1 *) pParams);
    Ip6BufRelease (VCM_INVALID_VC, pBuf, FALSE, PING6_MODULE);

}

/******************************************************************************
 * DESCRIPTION : This routine is called on the expiry of the ping timeout timer
 *               It checks whether already maximum no. of echo request packets
 *               have been sent. If no, it calls fun. Ping6SendEchoReq ()
 *               to send one more echo request packet to destination 
 * INPUTS      : ping block ( tPing6 *pPing6)
 * OUTPUTS     : None
 * RETURNS     : None
 * NOTES       :
 ******************************************************************************/

VOID
Ping6TimeoutForDst (tPing6 * pPing6)
{
    if (pPing6->u2SentCount >= pPing6->u2Ping6MaxTries)
    {
        pPing6->u1Oper = IP6_PING_NOT_IN_PROGRESS;
        return;
    }

    Ping6SendEchoReq (pPing6);
}

/******************************************************************************
 * DESCRIPTION : Checks whether an entry exists in ping table for the given
 *               destination address.
 * INPUTS      : Ping destination Address (ping6Dst),
 *               Ping source Address (ping6Src),
 *               Id of Ping Packet Rcvd(u2Id)
 *               Index from which the packet is received.
 * OUTPUTS     : None.
 * RETURNS     : Pointer Ping Entry (pPing) if entry exists / NULL Otherwise.
 * NOTES       :
 ******************************************************************************/

PRIVATE tPing6     *
Ping6GetEntry (tIp6Addr ping6Dst, tIp6Addr ping6Src, UINT2 u2Id, UINT4 u4Index)
{
    tIp6Addr           *pAddr = NULL;
    tIp6If             *pIf6 = NULL;
    UINT1               u1Count = 0;

    pIf6 = Ip6ifGetEntry (u4Index);
    for (u1Count = 0; u1Count < gu4MaxPing6Dst; u1Count++)
    {

        if (pPing[u1Count]->u1Admin != IP6_PING_INVALID)
        {
            if ((Ip6AddrMatch (&pPing[u1Count]->ping6Dst, &ping6Dst,
                               IP6_ADDR_SIZE_IN_BITS) == TRUE) &&
                (pPing[u1Count]->u2Id == u2Id))
            {
                return pPing[u1Count];
            }
            else if ((IS_ADDR_MULTI (pPing[u1Count]->ping6Dst)) &&
                     (pPing[u1Count]->u2Id == u2Id))
            {
                return pPing[u1Count];
            }
            else if ((pPing[u1Count]->u1AddrType == ADDR6_ANYCAST) &&
                     (pPing[u1Count]->u2Id == u2Id))
            {
                return pPing[u1Count];
            }
            else if ((!(IS_ADDR_LLOCAL (pPing[u1Count]->ping6Dst))) &&
                     (IS_ADDR_LLOCAL (ping6Dst)) &&
                     (IS_ADDR_LLOCAL (ping6Src)) &&
                     (((pAddr = Ip6AddrGetSrc (pIf6, &ping6Dst)) != NULL) &&
                      (Ip6AddrMatch (&ping6Src, pAddr,
                                     IP6_ADDR_SIZE_IN_BITS) == TRUE)) &&
                     (pPing[u1Count]->u2Id == u2Id))
            {
                /* If Ping6 is initiated for unicast-address and Ping6 request
                 * is sent out with Source address as interface's Link-Local,
                 * then reply is expected to have the link-local address as
                 * the Reply's source Address.
                 * Above condition is met here. Return this entry. */
                return pPing[u1Count];
            }
        }
    }

    return NULL;
}

/******************************************************************************
 * DESCRIPTION : Timeout handling routine for the PING6 task
 * INPUTS      : None. 
 * OUTPUTS     : None.
 * RETURNS     : None.
 * NOTES       :
 ******************************************************************************/

VOID
Ping6TimerHandler (VOID)
{
    tTmrAppTimer       *pTimerHdr = NULL, *pTimerNode = NULL;
    tPing6             *pPing6 = NULL;

    TmrGetExpiredTimers (gPing6TimerListId, &pTimerHdr);
    if (pTimerHdr == NULL)
    {
        IP6_GBL_TRC_ARG (PING6_MOD_TRC, DATA_PATH_TRC, PING6_NAME,
                         "PING6:Ping6TimerHandler: Expired timer list is NULL\n");
        return;
    }

    while (pTimerHdr)
    {
        pTimerNode = pTimerHdr;
        pTimerHdr = TmrGetNextExpiredTimer (gPing6TimerListId);
        pPing6 = (tPing6 *) (VOID *) GET_PING6_PTR_FROM_PING_TAB (pTimerNode);
        Ping6TimeoutForDst (pPing6);
    }

}
INT4
Ipv6AllowUnSecuredPing (VOID)
{
    return (gIp6AllowUnSecuredPing);
}

/*****************************************************************************/
/* Function     : Ping6EnableDebug                                           */
/*                                                                           */
/* Description  : This function configures the ping6 debug flag              */
/*                                                                           */
/* Input        : u4Value  - The ICMP debug value configured                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
VOID
Ping6EnableDebug (UINT4 u4DbgVal)
{
    gu4Ping6Dbg = u4DbgVal;
    return;
}

#ifdef PING6_LNXIP_WANTED
/*****************************************************************************/
/* Function     : Ping6LnxOobSend                                            */
/*                                                                           */
/* Description  : This function Send Echo Request through Linux OOB Interface*/
/*                                                                           */
/* Input        : Ping6 Entry                                                */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
INT4
Ping6LnxOobSend (tPing6 * pPing6)
{
    struct sockaddr_in6 ToDest;
    struct in6_pktinfo *pIpPktInfo = NULL;
    struct msghdr       Ip6MsgHdr;
    struct icmp6_hdr   *pPing6IcmpHdr;
    struct pollfd       pset;
    tNetIpv6IfInfo      NetIpIfInfo;
    tNetIpv6RtInfo      NetIp6RtInfo;
    tNetIpv6RtInfoQueryMsg RtQuery;
    tIp6Addr            SrcAddress;
    tIp6Addr            IfSrcAddr;
    INT1                ai1SendBuff[PING6_BUFFER_SIZE];
    INT1               *pi1SendBuff = NULL;
    UINT4               u4IfIndex = 0;
    UINT1               u1Ping6Data = 0;
    UINT1               au1CmsgInfo[112];
    struct iovec        Iov;
    struct cmsghdr     *pCmsgInfo;
    INT4                i4RetVal = 0;
    UINT4               u4Count = 0;

    MEMSET (ai1SendBuff, 0, sizeof (ai1SendBuff));
    MEMSET (&SrcAddress, 0, sizeof (tIp6Addr));
    MEMSET (&IfSrcAddr, 0, sizeof (tIp6Addr));

    IP6_GBL_TRC_ARG (PING6_MOD_TRC, MGMT_TRC, PING6_NAME, 
                            "Ping6LnxOobSend: Sending a ping packet\n");
    /* For Self Ping6, when the interface is down, return failure.
       This is applicable for both FSIP and LNXIP */
    if ((Ip6IsOurAddrInCxt (pPing6->u4ContextId, &(pPing6->ping6Dst),
                                   &u4IfIndex) == NETIPV6_SUCCESS))
    {
        MEMSET (&NetIpIfInfo, 0, sizeof (tNetIpv6IfInfo));

        if (Ipv6GetIfInfo (u4IfIndex, &NetIpIfInfo) != NETIPV6_SUCCESS)
        {
            pPing6->u1Oper = PING6_OPER_NOT_INITIATED;
            IP6_GBL_TRC_ARG (PING6_MOD_TRC, ALL_FAILURE_TRC, PING6_NAME, 
                           "Ping6LnxOobSend: Cannot obtain the "
                           "interface info\n");
            return OSIX_FAILURE;
        }
        if ((NetIpIfInfo.u4Admin == NETIPV6_ADMIN_DOWN) ||
            (NetIpIfInfo.u4Oper == NETIPV6_OPER_DOWN))
        {
            pPing6->u1Oper = PING6_OPER_NOT_INITIATED;
            IP6_GBL_TRC_ARG (PING6_MOD_TRC, ALL_FAILURE_TRC, PING6_NAME, 
                           "Ping6LnxOobSend: Interface status is"
                           "disabled\n");
            return OSIX_FAILURE;
        }
    }

    if ((pPing6->PingSrcAddr.u4_addr[0] |
         pPing6->PingSrcAddr.u4_addr[1] |
         pPing6->PingSrcAddr.u4_addr[2] | pPing6->PingSrcAddr.u4_addr[3]) == 0)
    {
        if (NetIpv6GetSrcAddrForDestAddr
            (pPing6->u4ContextId, &(pPing6->ping6Dst),
             &(pPing6->PingSrcAddr)) == NETIPV6_FAILURE)
        {
            return OSIX_FAILURE;
        }
    }
    /* If the Ping6 Instance created is the first one, Open the Socket
       for sending Ping6 Request and receving Ping6 replies */
    if (Ping6GetValidInstanceCount () == 0)
    {
        if (Ping6OpenSocket (pPing6->u4ContextId) == OSIX_FAILURE)
        {
            pPing6->u1Oper = PING6_OPER_NOT_INITIATED;
            return OSIX_FAILURE;
        }
    }

    /* Update the Status of the Ping6 - inprogress/completed */
    if (pPing6->u2SentCount < pPing6->u2Ping6MaxTries)
    {
        if (pPing6->u2SentCount == 0)
        {
            pPing6->u1Oper = PING6_OPER_IN_PROGRESS;
        }

        /* Incase of Linux IP, timer is used only to calculate the time
         * taken for Ping response */
        Ip6TmrRestart (VCM_INVALID_VC, PING6_DEST_TIMER_ID, gPing6TimerListId, 
                             &(pPing6->timer.appTimer),
                             pPing6->u2Ping6RcvTimeout);
    }
    else
    {
        Ip6TmrStop (VCM_INVALID_VC, PING6_DEST_TIMER_ID, gPing6TimerListId,
                        &(pPing6->timer.appTimer));
        pPing6->u1Oper = PING6_OPER_COMPLETED;
        return OSIX_SUCCESS;
    }

    /* Fill Destination Information needed for Socket to send Packet */
    MEMSET (&ToDest, 0, sizeof (ToDest));
    ToDest.sin6_family = AF_INET6;
    ToDest.sin6_port = 0;
    MEMCPY (&(ToDest.sin6_addr), &(pPing6->ping6Dst),
            sizeof (ToDest.sin6_addr));

    pi1SendBuff = ai1SendBuff;

    /*Fill the ICMP Header Information */
    pPing6IcmpHdr = (struct icmp6_hdr *) (VOID *) pi1SendBuff;
    pPing6IcmpHdr->icmp6_type = ICMP6_ECHO_REQUEST;
    pPing6IcmpHdr->icmp6_code = 0;
    pPing6IcmpHdr->icmp6_id = pPing6->u2Id;
    pPing6IcmpHdr->icmp6_cksum = 0;
    pPing6->u2Seq++;
    pPing6IcmpHdr->icmp6_seq = OSIX_HTONS (pPing6->u2Seq);

    /* Fill the Ping6 Data in the Buffer */
    u1Ping6Data = 0xA5;
    MEMSET (pi1SendBuff + sizeof (struct icmp6_hdr), u1Ping6Data,
            pPing6->u2Ping6Size);

    MEMSET (&(au1CmsgInfo), 0, sizeof (au1CmsgInfo));
    MEMSET (&(ToDest), 0, sizeof (struct sockaddr_in6));
    ToDest.sin6_family = AF_INET6;
    MEMCPY (&(ToDest.sin6_addr), &(pPing6->ping6Dst), sizeof (tIp6Addr));

    Iov.iov_base = (VOID *) pi1SendBuff;
    Iov.iov_len = sizeof (struct icmp6_hdr) + pPing6->u2Ping6Size;

    Ip6MsgHdr.msg_name = ((void *) &ToDest);
    Ip6MsgHdr.msg_namelen = sizeof (struct sockaddr_in6);
    Ip6MsgHdr.msg_iov = (void *) &Iov;
    Ip6MsgHdr.msg_iovlen = 1;
    Ip6MsgHdr.msg_control = (void *) &au1CmsgInfo;
    Ip6MsgHdr.msg_controllen = CMSG_SPACE (sizeof (struct in6_pktinfo));

    pCmsgInfo = CMSG_FIRSTHDR (&Ip6MsgHdr);
    pCmsgInfo->cmsg_level = IPPROTO_IPV6;
    pCmsgInfo->cmsg_type = IPV6_PKTINFO;
    pCmsgInfo->cmsg_len = CMSG_LEN (sizeof (struct in6_pktinfo));

    pIpPktInfo = (struct in6_pktinfo *) (VOID *) CMSG_DATA (pCmsgInfo);
    MEMCPY (&(pIpPktInfo->ipi6_addr), &SrcAddress, sizeof (tIp6Addr));

    if (pPing6->u2IfIndex == 0)
    {
        MEMSET (&NetIp6RtInfo, 0, sizeof (tNetIpv6RtInfo));
        MEMSET (&RtQuery, 0, sizeof (tNetIpv6RtInfoQueryMsg));

#if defined(VRF_WANTED) && defined(LINUX_310_WANTED)
        RtQuery.u4ContextId = pPing6->u4ContextId;
#else
        RtQuery.u4ContextId = VCM_DEFAULT_CONTEXT;
#endif

        RtQuery.u1QueryFlag = RTM6_QUERIED_FOR_NEXT_HOP;
        Ip6AddrCopy (&NetIp6RtInfo.Ip6Dst, &(pPing6->ping6Dst));
        NetIp6RtInfo.u1Prefixlen = IP6_ADDR_MAX_PREFIX;
        if (NetIpv6GetRoute (&RtQuery, &NetIp6RtInfo) == NETIPV6_FAILURE)
        {
            Ip6TmrStop (VCM_INVALID_VC, PING6_DEST_TIMER_ID, gPing6TimerListId,
                             &(pPing6->timer.appTimer));
            pPing6->u1Oper = PING6_OPER_COMPLETED;
            return OSIX_FAILURE;
        }
        pPing6->u2IfIndex = NetIp6RtInfo.u4Index;
    }
    pIpPktInfo->ipi6_ifindex = CfaGetIfIpPort ((UINT2) pPing6->u2IfIndex);

    i4RetVal = sendmsg (gi4Ping6Socket, &Ip6MsgHdr, 0);

    if (i4RetVal < 0)
    {
        Ip6TmrStop (VCM_INVALID_VC, PING6_DEST_TIMER_ID, gPing6TimerListId,
                             &(pPing6->timer.appTimer));

        pPing6->u1Oper = PING6_OPER_COMPLETED;
        perror("send failed\n");
        IP6_GBL_TRC_ARG (PING6_MOD_TRC, ALL_FAILURE_TRC, PING6_NAME, 
                       "Ping6LnxOobSend: sendmsg failed\n");

        return OSIX_FAILURE;
    }

    pPing6->u2SentCount++;
    IP6_GBL_TRC_ARG (PING6_MOD_TRC, MGMT_TRC, PING6_NAME, 
                      "Ping6LnxOobSend: Sent ICMPv6 Echo request\n");

    /* Poll till reply is received or timeout */
    MEMSET (&pset, 0, sizeof (struct pollfd));
    pset.fd = gi4Ping6Socket;
    pset.events = POLLIN | POLLERR;
    pset.revents = 0;
    for (;;)
    {
        if (poll (&pset, 1, 1) < 1 ||    /* 1ms */
            !(pset.revents & (POLLIN | POLLERR)))
        {
            u4Count++;
            if (u4Count >= (pPing6->u2Ping6RcvTimeout * 
                                SYS_NUM_OF_TIME_UNITS_IN_A_SEC))
            {
                Ip6TmrStop (VCM_INVALID_VC, PING6_DEST_TIMER_ID, gPing6TimerListId,
                             &(pPing6->timer.appTimer));
                if (pPing6->u2SentCount == pPing6->u2Ping6MaxTries)
                {
                    pPing6->u1Oper = PING6_OPER_COMPLETED;
                }
                return OSIX_FAILURE;
            }
            continue;
        }
        else
        {
            break;
        }
    }

    if (Ping6LnxOobReplyHandler () == OSIX_FAILURE)
    {
        Ip6TmrStop (VCM_INVALID_VC, PING6_DEST_TIMER_ID, gPing6TimerListId,
                        &(pPing6->timer.appTimer));
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function     : Ping6LnxOobReplyHandler                                 
 *                                                                        
 * Description  : Reads the Packet from Socket and then                   
 *               it searches the ping table for the match. If we have an  
 *               entry for the host and if we are waiting for the response
 *               then the statistics is updated                           
 *                                                                        
 * Input        : Ping6 Entry                                             
 *                                                                        
 * Output       : None                                                    
 *                                                                        
 * Returns      : OSIX_SUCCESS/OSIX_FAILURE                               
 *                                                                        
 *****************************************************************************/
INT4
Ping6LnxOobReplyHandler (VOID)
{
    tPing6             *pPing6 = NULL;
    tIp6Addr            Ip6SrcAddr;
    tIp6Addr            Ip6DestAddr;
    struct icmp6_hdr   *pIcmp6Hdr = NULL;
    struct sockaddr_in6 PeerAddr;
    INT1                ai1RecvBuff[PING6_BUFFER_SIZE];
    INT1               *pi1RecvBuff = NULL;
    INT4                i4DataLen = 0;
    UINT4               u4RemainingTime = 0;
    struct cmsghdr     *pCmsgInfo;
    struct iovec        Iov;
    struct msghdr       Ip6MsgHdr;
    UINT1               au1Cmsg[IP6_DEFAULT_MTU];    /* Max PDU size */
    tNetIpv6RtInfo      NetIp6RtInfo;
    tNetIpv6RtInfo      TmpNetIp6RtInfo;
    tNetIpv6RtInfoQueryMsg RtQuery;
    struct nd_redirect *pNdRedirectHdr = NULL;
    tIp6If             *pIf6 = NULL;

    MEMSET (&NetIp6RtInfo, 0, sizeof (tNetIpv6RtInfo));
    MEMSET (&TmpNetIp6RtInfo, 0, sizeof (tNetIpv6RtInfo));
    MEMSET (&RtQuery, 0, sizeof (tNetIpv6RtInfoQueryMsg));

    MEMSET (&PeerAddr, 0, sizeof (PeerAddr));
    PeerAddr.sin6_family = AF_INET6;
    PeerAddr.sin6_port = 0;

    MEMSET (ai1RecvBuff, 0, sizeof (PING6_BUFFER_SIZE));
    MEMSET (&Ip6SrcAddr, 0, sizeof (tIp6Addr));
    MEMSET (&Ip6DestAddr, 0, sizeof (tIp6Addr));
    MEMSET (au1Cmsg, 0, sizeof (au1Cmsg));

    pi1RecvBuff = ai1RecvBuff;
    Ip6MsgHdr.msg_name = (void *) &PeerAddr;
    Ip6MsgHdr.msg_namelen = sizeof (PeerAddr);
    Iov.iov_base = pi1RecvBuff;
    Iov.iov_len = PING6_BUFFER_SIZE;
    Ip6MsgHdr.msg_iov = &Iov;
    Ip6MsgHdr.msg_iovlen = 1;
    Ip6MsgHdr.msg_control = (VOID *) &au1Cmsg;
    Ip6MsgHdr.msg_controllen = IP6_DEFAULT_MTU;

    pCmsgInfo = CMSG_FIRSTHDR (&Ip6MsgHdr);
    pCmsgInfo->cmsg_level = SOL_IP;
    pCmsgInfo->cmsg_type = IPV6_PKTINFO;
    pCmsgInfo->cmsg_len = sizeof (au1Cmsg);

    i4DataLen = recvmsg (gi4Ping6Socket, &Ip6MsgHdr, 0);

    if (i4DataLen < 0)
    {
        IP6_GBL_TRC_ARG (PING6_MOD_TRC, ALL_FAILURE_TRC, PING6_NAME, 
                          "Ping6ReplyHandler :recvmsg error\r\n");
        return OSIX_FAILURE;
    }

    IP6_GBL_TRC_ARG (PING6_MOD_TRC, MGMT_TRC, PING6_NAME, 
                          "Ping6ReplyHandler: Received a ping packet \n");

    pIcmp6Hdr = (struct icmp6_hdr *) (VOID *) (pi1RecvBuff);

    pNdRedirectHdr = (struct nd_redirect *) (VOID *) (pi1RecvBuff);

    Ip6AddrCopy (&Ip6SrcAddr, (tIp6Addr *) (VOID *) PeerAddr.sin6_addr.s6_addr);
    pPing6 = Ping6GetEntry (Ip6SrcAddr, Ip6DestAddr, pIcmp6Hdr->icmp6_id, 0);

    if (pPing6 == NULL)
    {
        return OSIX_FAILURE;
    }

    /* ICMP6 Redirect Enable/Disable */
    pIf6 = Ip6ifGetEntry (pPing6->u2IfIndex);

    if (pIf6 == NULL)
    {
        return OSIX_FAILURE;
    }

    if (pIf6->Icmp6ErrRLInfo.i4Icmp6RedirectMsg == ICMP6_REDIRECT_ENABLE)
    {

        /*Code to handle ICMPv6 Redirect messages */
        if (pIcmp6Hdr->icmp6_type == ND_REDIRECT)
        {
#if defined(VRF_WANTED) && defined(LINUX_310_WANTED)
        RtQuery.u4ContextId = pPing6->u4ContextId;
#else
        RtQuery.u4ContextId = VCM_DEFAULT_CONTEXT;
#endif

            RtQuery.u1QueryFlag = RTM6_QUERIED_FOR_NEXT_HOP;
            MEMCPY (&(NetIp6RtInfo.Ip6Dst), &(pNdRedirectHdr->nd_rd_dst),
                    sizeof (tIp6Addr));
            NetIp6RtInfo.u1Prefixlen = IP6_ADDR_MAX_PREFIX;
            if (NetIpv6GetRoute (&RtQuery, &NetIp6RtInfo) == NETIPV6_SUCCESS)
            {
                /*Copy the entry to the temporary buffer */
                MEMCPY (&TmpNetIp6RtInfo, &NetIp6RtInfo,
                        sizeof (tNetIpv6RtInfo));
                MEMCPY (&(TmpNetIp6RtInfo.NextHop),
                        &(pNdRedirectHdr->nd_rd_target), sizeof (tIp6Addr));
                if (!(IS_ADDR_LLOCAL (TmpNetIp6RtInfo.NextHop)))
                {
                    IP6_GBL_TRC_ARG (PING6_MOD_TRC, MGMT_TRC,
                                   PING6_NAME, 
                          "Ping6ReplyHandler: Target MUST be Link-Local Address \n");
                    return OSIX_FAILURE;
                }
                if (Ip6AddrCompare
                    (NetIp6RtInfo.NextHop, TmpNetIp6RtInfo.NextHop) != IP6_ZERO)
                {
                    /*Delete the entry from the route table */
                    NetIpv6LeakRoute (NETIPV6_DELETE_ROUTE, &NetIp6RtInfo);

                    /* Add the updated entry */
                    NetIpv6LeakRoute (NETIPV6_ADD_ROUTE, &TmpNetIp6RtInfo);
                }
            }
            else
            {
                return OSIX_FAILURE;
            }

        }
    }

    /* Only Echo Replies should be considered */
    if (pIcmp6Hdr->icmp6_type == ICMP6_ECHO_REPLY)
    {
        /* If Valid Id,then update the Statistics */
        if (pIcmp6Hdr->icmp6_id == pPing6->u2Id)
        {
            TmrGetRemainingTime (gPing6TimerListId,
                                 &(pPing6->timer.appTimer), &u4RemainingTime);

            /* Calculate time taken  for Ping6Reply */
            pPing6->u4TimeTaken = (pPing6->u2Ping6RcvTimeout *
                                   SYS_NUM_OF_TIME_UNITS_IN_A_SEC) -
                u4RemainingTime;

            /* Time taken for Reply in msec */
            pPing6->u4TimeTaken =
                (pPing6->u4TimeTaken * 1000) / SYS_NUM_OF_TIME_UNITS_IN_A_SEC;

            if (pPing6->u2Successes == 0)
            {
                pPing6->u2MaxTime = (UINT2) pPing6->u4TimeTaken;
                pPing6->u2MinTime = (UINT2) pPing6->u4TimeTaken;
            }
            else
            {
                if (pPing6->u4TimeTaken > pPing6->u2MaxTime)
                {
                    pPing6->u2MaxTime = (UINT2) pPing6->u4TimeTaken;
                }
                if (pPing6->u4TimeTaken < pPing6->u2MinTime)
                {
                    pPing6->u2MinTime = (UINT2) pPing6->u4TimeTaken;
                }
            }

            pPing6->u2Successes++;
            /*
             *                (N1 * M1) + (N2 * M2)
             *   Mean Time =  ----------------------   Where N2 = 1
             *                       N1 + N2           N1 = pPing6->i2Success - 1
             */

            pPing6->u2AverageTime = (pPing6->u2MaxTime + pPing6->u2MinTime) / IP_TWO;

            /* TODO - multiple pings are going when initiated via SNMP */
            /* Stop timer alone, the application will be trigger the
             * ping request again */
            TmrStopTimer (gPing6TimerListId, &(pPing6->timer.appTimer));
            if (pPing6->u2SentCount == pPing6->u2Ping6MaxTries)
            {
                pPing6->u1Oper = PING6_OPER_COMPLETED;
            }
        }
    }
    else
    {
        /*if the received packet is other than icmp reply packet ,return failure */
        return OSIX_FAILURE;

    }

    /* If no valid Ping6 Instance exists, Close the Socket Opened
       for sending Ping6 Request and receving Ping6 replies */
    if (Ping6GetValidInstanceCount () == 0)
    {
        IP6_GBL_TRC_ARG (PING6_MOD_TRC, MGMT_TRC,
                       PING6_NAME, 
                       "Ping6ReplyHandler: Closing the ping socket \n");
        Ping6CloseSocket ();
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function     : Ping6GetValidInstanceCount                               
 *                                                                         
 * Description  : Provides the count of active Ping6 Instances.(i.e OnGoing
 *                Ping6)                                                   
 *                                                                         
 * Input        : None                                                     
 *                                                                         
 * Output       : None                                                     
 *                                                                         
 * Returns      : Returns Ping6Instance Count                              
 *                                                                         
 *****************************************************************************/
PUBLIC INT4
Ping6GetValidInstanceCount (VOID)
{
    INT4                i4Ping6InstCount = 0;
    UINT1               u1Count = 0;

    for (u1Count = 0; u1Count < gu4MaxPing6Dst; u1Count++)
    {
        if (pPing[u1Count]->u1Oper == PING6_OPER_IN_PROGRESS)
        {
            i4Ping6InstCount++;
        }
    }
    return i4Ping6InstCount;
}

/*****************************************************************************
 * Function     : Ping6OpenSocket                                     
 *                                                                    
 * Description  : Open and Initializes the Socket used by Ping6 Module
 *                                                                    
 * Input        : None                                                
 *                                                                    
 * Output       : None                                                
 *                                                                    
 * Returns      : OSIX_SUCCESS/OSIX_FAILURE                           
 *                                                                    
 *****************************************************************************/
INT4
Ping6OpenSocket (UINT4 u4ContextId)
{
    struct icmp6_filter Filter;
    INT4                i4RetVal = 0;
    INT4                i4Value = 0;

#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    tLnxVrfEventInfo  LnxVrfEventInfo;
#else
    UNUSED_PARAM(u4ContextId);
#endif

    MEMSET (&Filter, 0, sizeof (struct icmp6_filter));

    IP6_GBL_TRC_ARG (PING6_MOD_TRC, MGMT_TRC, PING6_NAME, 
                   "Ping6OpenSocket: Opening a new socket for "
                   "ping session\n");
    /* Open the RawSocket for sending EchoRequests and 
       Receving EchoReplies */
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)

    LnxVrfEventInfo.u4ContextId = u4ContextId;
    LnxVrfEventInfo.i4Sockdomain = AF_INET6;
    LnxVrfEventInfo.i4SockType=SOCK_RAW;
    LnxVrfEventInfo.i4SockProto = IPPROTO_ICMPV6;
    LnxVrfEventInfo.u1MsgType = LNX_VRF_OPEN_SOCKET;
    LnxVrfSockLock();
    LnxVrfEventHandling(&LnxVrfEventInfo,&gi4Ping6Socket);
    LnxVrfSockUnLock();
#else
    gi4Ping6Socket = socket (AF_INET6, SOCK_RAW, IPPROTO_ICMPV6);
#endif

    if (gi4Ping6Socket <= 0)
    {
        return OSIX_FAILURE;
    }

    i4Value = 2;

    i4RetVal = setsockopt (gi4Ping6Socket, SOL_RAW, IPV6_CHECKSUM, &i4Value,
                           sizeof (INT4));
    if (i4RetVal < 0)
    {
        Ping6CloseSocket ();
        IP6_GBL_TRC_ARG (PING6_MOD_TRC, ALL_FAILURE_TRC,
                       PING6_NAME, "Ping6OpenSocket: IPV6_CHECKSUM failed\n");
        return OSIX_FAILURE;
    }

    i4Value = 1;
    setsockopt (gi4Ping6Socket, SOL_IPV6, IPV6_RECVERR, (char *) &i4Value,
                sizeof (i4Value));

    if (i4RetVal < 0)
    {
        Ping6CloseSocket ();
        IP6_GBL_TRC_ARG (PING6_MOD_TRC, ALL_FAILURE_TRC,
                       PING6_NAME, "Ping6OpenSocket: IPV6_RECVERR failed\n");
        return OSIX_FAILURE;
    }

    i4Value = 1;
    setsockopt (gi4Ping6Socket, IPPROTO_IPV6, IPV6_RECVHOPLIMIT,
                &i4Value, sizeof (i4Value));

    if (i4RetVal < 0)
    {
        Ping6CloseSocket ();
        IP6_GBL_TRC_ARG (PING6_MOD_TRC, ALL_FAILURE_TRC, PING6_NAME, 
                       "Ping6OpenSocket: "
                       "IPV6_RECVHOPLIMIT failed\n");
        return OSIX_FAILURE;
    }

    /* Select icmp echo reply as icmp type to receive */

    ICMP6_FILTER_SETBLOCKALL (&Filter);
    ICMP6_FILTER_SETPASS (ICMP6_ECHO_REPLY, &Filter);
    ICMP6_FILTER_SETPASS (ICMP6_DST_UNREACH, &Filter);
    ICMP6_FILTER_SETPASS (ICMP6_PACKET_TOO_BIG, &Filter);
    ICMP6_FILTER_SETPASS (ICMP6_TIME_EXCEEDED, &Filter);
    ICMP6_FILTER_SETPASS (ICMP6_PARAM_PROB, &Filter);
    ICMP6_FILTER_SETPASS (ICMP6_ECHO_REPLY, &Filter);
    ICMP6_FILTER_SETPASS (ND_REDIRECT, &Filter);

    i4RetVal = setsockopt (gi4Ping6Socket, IPPROTO_ICMPV6, ICMP6_FILTER,
                           &Filter, sizeof (struct icmp6_filter));

    if (i4RetVal < 0)
    {
        Ping6CloseSocket ();
        IP6_GBL_TRC_ARG (PING6_MOD_TRC, ALL_FAILURE_TRC,
                       PING6_NAME, "Ping6OpenSocket: ICMP6_FILTER failed\n");
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function     : Ping6CloseSocket                                       
 *                                                                       
 * Description  : Closes the Raw Socket used by Ping6 Module.When no ping
 *                instance exists,the socket is closed                   
 *                                                                       
 * Input        : None                                                   
 *                                                                       
 * Output       : None                                                   
 *                                                                       
 * Returns      : OSIX_SUCCESS/OSIX_FAILURE                              
 *                                                                       
 *****************************************************************************/
INT4
Ping6CloseSocket (VOID)
{
    INT4                i4RetVal = 0;

    /* Close the RAW Socket Opened */
    if (gi4Ping6Socket != -1)
    {
        i4RetVal = close (gi4Ping6Socket);

        if (i4RetVal < 0)
        {
            IP6_GBL_TRC_ARG (PING6_MOD_TRC, ALL_FAILURE_TRC,
                           PING6_NAME, "Ping6CloseSocket: Socket close failed\n");
            return (OSIX_FAILURE);
        }
    }

    gi4Ping6Socket = -1;
    IP6_GBL_TRC_ARG (PING6_MOD_TRC, MGMT_TRC, PING6_NAME, 
            "Ping6CloseSocket: Closed the ping socket\n");
    return (OSIX_SUCCESS);
}
#endif

/***************************** END OF FILE **********************************/
