/*******************************************************************
 *   Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *   
 *   $Id: ip6exhdr.c,v 1.12 2013/12/20 03:27:30 siva Exp $
 *
 ******************************************************************/

/*
 *-----------------------------------------------------------------------------
 *    FILE NAME                    :    ip6exhdr.c   
 *
 *    PRINCIPAL AUTHOR             :    Kaushik Biswas 
 *
 *    SUBSYSTEM NAME               :    IPv6
 *
 *    MODULE NAME                  :    IP6 Core Submodule
 *
 *    LANGUAGE                     :    C
 *
 *    TARGET ENVIRONMENT           :    UNIX
 *
 *    DATE OF FIRST RELEASE        :    DDD-MM-1996 
 *
 *    DESCRIPTION                  :   This routine handles the processing
 *                                     of the extention headers other than 
 *                                     the Fragment header.Currently only  
 *                                     the routing header is processed
 *                                     and only on the input side
 *-----------------------------------------------------------------------------
 */

#include "ip6inc.h"

PRIVATE INT4        Ip6JmbHdrValid (tCRU_BUF_CHAIN_HEADER * pBuf,
                                    tIp6If * pIf6,
                                    tIp6Hdr * pIp6,
                                    tJmbHdr * pJmbHdr, UINT4 u4TotLen);

PRIVATE INT4        Ip6PrcsJmbOpt (tCRU_BUF_CHAIN_HEADER * pBuf,
                                   UINT1 *pu1ByteToRead, UINT4 *pu4BalanceBytes,
                                   UINT4 *pu4TotLen, tIp6If * pIf6,
                                   tIp6Hdr * pIp6);

PRIVATE INT4        Ip6PrcsRtAlertOpt (UINT4 u4Len, tIp6If * pIf6,
                                       tIp6Hdr * pIp6,
                                       tCRU_BUF_CHAIN_HEADER * pBuf,
                                       UINT1 u1IsFinalDest,
                                       UINT1 *pu1ByteToRead,
                                       UINT4 *pu4BalanceBytes,
                                       UINT2 u2HopbyHopPadLen);

/******************************************************************************
 * DESCRIPTION : Handles the processing of Destination options header
 *
 * INPUTS      : Processed length of the packet including the IPv6 header
 *               (pu4TotLen) for updation, the interface pointer (pIf6),
 *               IPv6 hdr pointer (pIp6) and the pointer to the buffer
 *               containing the datagram (pBuf)
 * OUTPUTS     : Next header after this header (pu1NextHdr), updated processed
 *               length (pu4Len) of packet including this header
 *
 * RETURNS     : IP6_SUCCESS - If processing okay
 *               IP6_FAILURE - If processing tells hdr is not okay
 *
 * NOTES       :
 ******************************************************************************/
#ifdef MIP6_WANTED
INT4
Ip6PrcsDestOptHdr (UINT1 *pu1NextHdr, UINT4 *pu4Len, tIp6If * pIf6,
                   tIp6Hdr * pIp6, tCRU_BUF_CHAIN_HEADER * pBuf,
                   tIp6Addr * pHomeAddrOptAddr)
#else
INT4
Ip6PrcsDestOptHdr (UINT1 *pu1NextHdr, UINT4 *pu4Len, tIp6If * pIf6,
                   tIp6Hdr * pIp6, tCRU_BUF_CHAIN_HEADER * pBuf)
#endif
{
    tExtHdr            *pDestHdr = NULL, DestHdr;
    UINT1               u1ByteToRead = 0;
    UINT4               u4BalanceBytes = 0;
    UINT4               u4ProcessedLen = 0;
#ifdef MIP6_WANTED
    INT1                i1HomeAddrOptPresent = 0;
#endif

    if ((pDestHdr = (tExtHdr *) Ip6BufRead (pBuf, (UINT1 *) &DestHdr,
                                            IP6_BUF_READ_OFFSET (pBuf),
                                            IP6_TYPE_LEN_BYTES, FALSE)) == NULL)
    {
        return IP6_FAILURE;
    }

    /* To get the extension header length and the Next Header */
    *pu4Len += ((pDestHdr->u1HdrLen + 1) * BYTES_IN_OCTECT);
    *pu1NextHdr = pDestHdr->u1NextHdr;

    /* To find the option type - with out moving READ_OFFSET */
    EXTRACT_1_BYTE (pBuf, u1ByteToRead);

    /* Number of bytes to be processed in the extension header */
    u4BalanceBytes = ((pDestHdr->u1HdrLen + 1) * BYTES_IN_OCTECT) - IP6_TWO;

    while (u4BalanceBytes != 0)
    {
        switch (u1ByteToRead)
        {

            case PAD1_OPTION:
                u4ProcessedLen = IP6_BUF_READ_OFFSET (pBuf);
                Ip6PrcsPad1Opt (pBuf, &u1ByteToRead, &u4BalanceBytes,
                                &u4ProcessedLen);
                IP6_BUF_READ_OFFSET (pBuf) = u4ProcessedLen;
                break;

            case PADN_OPTION:
                if (Ip6PrcsPadNOpt
                    (pBuf, &u1ByteToRead, &u4BalanceBytes) == IP6_FAILURE)
                {
                    return IP6_FAILURE;
                }
                break;

#ifdef MIP6_WANTED
            case HOME_ADDR_OPTION:
                u4ProcessedLen = IP6_BUF_READ_OFFSET (pBuf);
                if (Ip6RcvHomeAddrOption (pBuf, &u1ByteToRead,
                                          u4ProcessedLen,
                                          &u4BalanceBytes, pHomeAddrOptAddr,
                                          &i1HomeAddrOptPresent, pIp6)
                    == IP6_FAILURE)
                {
                    return IP6_FAILURE;
                }
                u4ProcessedLen += (IP6_TWO + sizeof (tIp6Addr));
                IP6_BUF_READ_OFFSET (pBuf) = u4ProcessedLen;
                break;
#endif

            default:
                u4ProcessedLen = IP6_BUF_READ_OFFSET (pBuf);
                if (Ip6PrcsUnRecOpt (pBuf, &u1ByteToRead, &u4BalanceBytes, pIf6,
                                     pIp6) == IP6_FAILURE)
                {
                    return IP6_FAILURE;
                }
                break;
        }
    }                            /* end of while */
    return IP6_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : Handles the processing of the HBYH header on the input side
 *
 * INPUTS      : Total length of the packet including the IPv6 header(u4TotLen),
 *               the interface pointer (pIf6),IPv6 hdr pointer (pIp6),
 *               the pointer to the buffer containing the datagram (pBuf)
 *               and flag indication whether the packet has reached final
 *               destination or not.
 * OUTPUTS     : Next header after this header (pu1NextHdr) and the
 *               offset(pu4Len) of packet including this header
 *
 * RETURNS     : IP6_SUCCESS/IP6_ACCEPTED - If processing okay
 *               IP6_FAILURE - If processing tells hdr is not okay
 *
 * NOTES       :
 ******************************************************************************/
INT4
Ip6PrcsHbyhOptHdr (UINT1 *pu1NextHdr,
                   UINT4 *pu4Len,
                   UINT4 *pu4TotLen,
                   tIp6If * pIf6, tIp6Hdr * pIp6, tCRU_BUF_CHAIN_HEADER * pBuf,
                   UINT1 u1IsFinalDest)
{

    tExtHdr            *pHbyhHdr, HbyhHdr;    /* structure for DestOptHeader and
                                               Hob-by-Hop are same */
    UINT1               u1ByteToRead = 0;
    UINT4               u4BalanceBytes = 0;
    UINT4               u4ProcessedLen = 0;
    INT4                i4Status = IP6_SUCCESS;
    UINT2               u2HopbyHopPadLen = 0;
    if ((pHbyhHdr = (tExtHdr *) Ip6BufRead (pBuf, (UINT1 *) &HbyhHdr,
                                            IP6_BUF_READ_OFFSET (pBuf),
                                            IP6_TYPE_LEN_BYTES, FALSE)) == 0)
    {
        return IP6_FAILURE;
    }

    *pu4Len += ((pHbyhHdr->u1HdrLen + 1) * BYTES_IN_OCTECT);
    *pu1NextHdr = pHbyhHdr->u1NextHdr;

    EXTRACT_1_BYTE (pBuf, u1ByteToRead);
    u4BalanceBytes = ((pHbyhHdr->u1HdrLen + 1) * BYTES_IN_OCTECT) - IP6_TWO;

    while (u4BalanceBytes != 0)
    {

        switch (u1ByteToRead)
        {

            case PAD1_OPTION:
                u4ProcessedLen = IP6_BUF_READ_OFFSET (pBuf);
                Ip6PrcsPad1Opt (pBuf, &u1ByteToRead, &u4BalanceBytes,
                                &u4ProcessedLen);
                IP6_BUF_READ_OFFSET (pBuf) = u4ProcessedLen;
                break;

            case PADN_OPTION:
                if (Ip6PrcsPadNOpt
                    (pBuf, &u1ByteToRead, &u4BalanceBytes) == IP6_FAILURE)
                {
                    return IP6_FAILURE;
                }
                break;

            case JMB_OPT_TYPE:
                if (Ip6PrcsJmbOpt
                    (pBuf, &u1ByteToRead, &u4BalanceBytes, pu4TotLen, pIf6,
                     pIp6) == IP6_FAILURE)
                {
                    return IP6_FAILURE;
                }
                break;

            case IP6_ROUTE_ALERT_OPT:
                if (u4BalanceBytes < ROUTE_ALERT_LEN)
                {
                    return IP6_FAILURE;
                }
                u2HopbyHopPadLen = (u4BalanceBytes - ROUTE_ALERT_LEN);
                if ((*pu1NextHdr == NH_ICMP6) && (u2HopbyHopPadLen > 0))
                {
                    u4BalanceBytes -= u2HopbyHopPadLen;
                }
                else
                {
                    u2HopbyHopPadLen = 0;
                }
                i4Status = Ip6PrcsRtAlertOpt ((*pu4TotLen - *pu4Len), pIf6,
                                              pIp6, pBuf, u1IsFinalDest,
                                              &u1ByteToRead, &u4BalanceBytes,
                                              u2HopbyHopPadLen);
                if (i4Status == IP6_FAILURE)
                {
                    /* Error in handling option. */
                    return IP6_FAILURE;
                }
                break;

            default:
                if (Ip6PrcsUnRecOpt (pBuf, &u1ByteToRead, &u4BalanceBytes, pIf6,
                                     pIp6) == IP6_FAILURE)
                {
                    return IP6_FAILURE;
                }
                break;
        }
    }
    return i4Status;
}

/******************************************************************************
 * DESCRIPTION : Validates the jumbo header in the received ipv6 pkt.
 *
 * INPUTS      : The interface pointer (pIf6),IPv6 hdr pointer (pIp6) and
 *               the pointer to the buffer containing the datagram (pBuf)
 *               the pointer to the jumbo header, Total length of the packet
 *               including the IPv6 header(u4TotLen).
 * OUTPUTS     : None.
 *
 * RETURNS     : IP6_SUCCESS - If processing okay
 *               IP6_FAILURE - If processing tells hdr is not okay
 *
 * NOTES       :
 ******************************************************************************/ PRIVATE INT4
Ip6JmbHdrValid (tCRU_BUF_CHAIN_HEADER * pBuf,
                tIp6If * pIf6,
                tIp6Hdr * pIp6, tJmbHdr * pJmbHdr, UINT4 u4TotLen)
{
    UINT1               u1HbyhFlag = 0;
    UINT1               u1ErrFlag = 0;
    tCRU_BUF_CHAIN_HEADER *pErrBuf = NULL;
    UINT1               u1PayloadLenFlag = FALSE;
    UINT1               u1JmbFlag = 0;
    UINT1               u1Code = 0;
    UINT1               u1Type = 0;
    UINT1               u1Parm = 0;

    gIp6GblInfo.i1JmbPktFlag = TRUE;
    u1HbyhFlag = TRUE;
    u1ErrFlag = FALSE;
    u1JmbFlag = FALSE;

    if (u4TotLen - IP6_HDR_LEN == 0)
    {
        u1PayloadLenFlag = TRUE;
    }
    if (pJmbHdr->u1OptType == JMB_OPT_TYPE)
    {
        u1JmbFlag = TRUE;
    }
    if ((u1PayloadLenFlag) && (u1HbyhFlag) && (!u1JmbFlag))
    {
        u1Parm = IP6_OFFSET_FOR_PAYLOAD_LEN;

        u1ErrFlag = TRUE;
    }
    if ((!u1PayloadLenFlag) && (u1JmbFlag))
    {
        u1Parm = JMB_OPT_TYPE_OFFSET;
        u1ErrFlag = TRUE;
    }
    if ((u1JmbFlag) && ((NTOHL (pJmbHdr->u4JmbLen) < JMB_MIN_PAYLOAD_LEN)))
    {
        u1Parm = JMB_OPT_LEN_OFFSET;
        u1ErrFlag = TRUE;
    }
    if (u1ErrFlag == TRUE)
    {
        pErrBuf = CRU_BUF_Duplicate_BufChain (pBuf);
        u1Code = ICMP6_HDR_PROB;
        u1Type = ICMP6_PKT_PARAM_PROBLEM;
        pIf6->pIp6Cxt->u2JmbErrPkts++;
        if (pErrBuf != NULL)
        {
            Icmp6SendErrMsg (pIf6, pIp6, u1Type, u1Code, u1Parm, pErrBuf);
        }
        return IP6_FAILURE;
    }
    return IP6_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : Handles the processing of the routing header on the input side
 *
 * INPUTS      : Flag telling that address is tentative(u1Type),total length
 *               of the packet including the IPv6 header(u4TotLen),
 *               the interface pointer (pIf6),IPv6 hdr pointer (pIp6) and
 *               the pointer to the buffer containing the datagram (pBuf)
 *               and the offset of packet till the starting of this header
 *
 * OUTPUTS     : Next header after this header (pu1NextHdr) and the 
 *               offset of packet including this header
 *
 * RETURNS     : IP6_SUCCESS - If processing okay
 *               IP6_EXT_HDR_DISCARD - If processing tells hdr is not okay
 *
 * NOTES       :
 ******************************************************************************/

INT4
Ip6PrcsRtHdr (UINT1 *pu1Type, UINT1 *pu1NextHdr, UINT4 *pu4Len, UINT4 u4TotLen,
              tIp6If * pIf6, tIp6Hdr * pIp6, tCRU_BUF_CHAIN_HEADER * pBuf)
{

    INT4                i4RetVal = IP6_SUCCESS;
    UINT1               u1Addrs = 0, u1AddrType = 0, u1Count = 0;
    UINT2               u2ExtLen = 0;

    tIp6Cxt            *pIp6Cxt = NULL;
    tIp6Addr            addri, tmp_addri, tmp_dest, *p_addri = NULL;
    tRtHdr             *pRtEhdr = NULL, rt_ehdr;
    tRt0Hdr            *pRt0Hdr = NULL, rt0_hdr;
    UINT1               u1Type = *pu1Type;
    UINT4               u4Len = *pu4Len;
#ifdef MN_WANTED
    UINT4               u4Index = 0;
#endif

    pIp6Cxt = pIf6->pIp6Cxt;
    IP6_TRC_ARG4 (pIp6Cxt->u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                  "IP6EXH:Ip6PrcsRtHdr:Got Routing ExtHdr AddrType=%d Len "
                  "Proces=%d TotLen=%d IF=%d",
                  *pu1Type, *pu4Len, u4TotLen, pIf6->u4Index);

    IP6_TRC_ARG3 (pIp6Cxt->u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                  "Buf ptr=%p SrcAddr=%s DestAddr=%s \n", pBuf,
                  Ip6PrintAddr (&pIp6->srcAddr), Ip6PrintAddr (&pIp6->dstAddr));

    /* Get the tRtHdr from the buffer */
    pRtEhdr = Ip6BufRead (pBuf, (UINT1 *) &rt_ehdr,
                          (UINT4) IP6_BUF_READ_OFFSET (pBuf),
                          (UINT4) sizeof (tRtHdr), 0);
    if (pRtEhdr == NULL)
    {
        IP6_TRC_ARG (pIp6Cxt->u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                     "IP6EXH : Ip6PrcsRtHdr: Got a NULL pRtEhdr\n");
        IP6_TRC_ARG3 (pIp6Cxt->u4ContextId, IP6_MOD_TRC, BUFFER_TRC, IP6_NAME,
                      "%s buf err: Type=%d  Bufptr=%p", ERROR_FATAL_STR,
                      ERR_BUF_READ, pBuf);

        if (Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE, IP6_MODULE) ==
            IP6_FAILURE)
        {
            IP6_TRC_ARG1 (pIp6Cxt->u4ContextId, IP6_MOD_TRC, BUFFER_TRC,
                          IP6_NAME, "IP6EXH : Ip6PrcsRtHdr:Failure in "
                          "rel buf=%p\n", pBuf);

            IP6_TRC_ARG3 (pIp6Cxt->u4ContextId, IP6_MOD_TRC, BUFFER_TRC,
                          IP6_NAME, "%s buf err: Type=%d  Bufptr=%p",
                          ERROR_FATAL_STR, ERR_BUF_RELEASE, pBuf);
        }

        IP6IF_INC_IN_DISCARDS (pIf6);
        IP6SYS_INC_IN_DISCARDS (pIp6Cxt->Ip6SysStats);

        return IP6_EXT_HDR_DISCARD;
    }

    IP6_TRC_ARG4 (pIp6Cxt->u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                  "IP6EXH:Ip6PrcsRtHdr: NH = %d ExtHdrLen=%d Routing Type=%d "
                  "Seg Left=%d\n",
                  pRtEhdr->u1Nexthdr, pRtEhdr->u1Hdrlen,
                  pRtEhdr->u1Type, pRtEhdr->u1SegmentsLeft);

#ifdef MIP6_WANTED

    if (pRtEhdr->u1Type == IP6_RT2_HDR)
    {
        if ((pRtEhdr->u1Hdrlen != IP6_RT2_HDR_LEN) ||
            (pRtEhdr->u1SegmentsLeft != IP6_RT2_SEGMENTS_LEFT))
        {
            Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE, IP6_MODULE);
            return IP6_EXT_HDR_DISCARD;
        }
        IP6_BUF_READ_OFFSET (pBuf) += sizeof (UINT4);    /* u4Reserved in Rt2Hdr */
    }
#endif

    if (pRtEhdr->u1Type == IP6_RT0_HDR)
    {
        /* Get the tRt0Hdr from the buffer */
        pRt0Hdr = Ip6BufRead (pBuf, (UINT1 *) &rt0_hdr,
                              (UINT4) IP6_BUF_READ_OFFSET (pBuf),
                              (UINT4) sizeof (tRt0Hdr), 0);

        if (pRt0Hdr == NULL)
        {

            IP6_TRC_ARG (pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                         DATA_PATH_TRC, IP6_NAME,
                         "IP6EXH : Ip6PrcsRtHdr: Got a NULL pRt0Hdr\n");
            IP6_TRC_ARG3 (pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                          BUFFER_TRC, IP6_NAME,
                          "%s buf err: Type=%d  Bufptr=%p", ERROR_FATAL_STR,
                          ERR_BUF_READ, pBuf);

            if (Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE, IP6_MODULE) ==
                IP6_FAILURE)
            {
                IP6_TRC_ARG1 (pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                              BUFFER_TRC, IP6_NAME,
                              "IP6EXH: Ip6PrcsRtHdr:Fails in rel buf=%p\n",
                              pBuf);

                IP6_TRC_ARG3 (pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                              BUFFER_TRC, IP6_NAME,
                              "%s buf err: Type = %d  Bufptr = %p",
                              ERROR_FATAL_STR, ERR_BUF_RELEASE, pBuf);
            }

            IP6IF_INC_IN_DISCARDS (pIf6);
            IP6SYS_INC_IN_DISCARDS (pIp6Cxt->Ip6SysStats);

            return IP6_EXT_HDR_DISCARD;
        }
    }

    /* Get the extention header length */
    u2ExtLen = (UINT2) (pRtEhdr->u1Hdrlen * BYTE_LENGTH + BYTE_LENGTH);

    *pu1NextHdr = pRtEhdr->u1Nexthdr;

    /* Check if the routing extention header's type is not 0 and only if the
     * the segments left is non-zero then the packet must be discarded
     * and an ICMPv6 packet is sent to the packet's source.If the segments
     * left is zero then the the routing header is to be ignored and proceed
     * to process the next header in the packet
     */
#ifdef MIP6_WANTED
    if ((pRtEhdr->u1Type != IP6_RT0_HDR) && (pRtEhdr->u1Type != IP6_RT2_HDR))
#else
    if ((pRtEhdr->u1Type != IP6_RT0_HDR) || ((pRtEhdr->u1Type == IP6_RT0_HDR)
                                             && (gIp6GblInfo.
                                                 u1RFC5095Compatibility ==
                                                 IP6_RFC5095_COMPATIBLE)))
#endif
    {
        if (pRtEhdr->u1SegmentsLeft == 0)
        {
            *pu4Len += (UINT4) u2ExtLen;

            /* Set the read offset of the CRU buffer to the starting of
             * next header beyond the routing header
             */
            IP6_BUF_READ_OFFSET (pBuf) = *pu4Len;

            return IP6_SUCCESS;
        }
        else
        {
            IP6_TRC_ARG (pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                         DATA_PATH_TRC, IP6_NAME,
                         "IP6EXH:Ip6PrcsRtHdr:Send ICMPErrMsg <ParamProb.code0>"
                         " offset ptg unrecog rout type");
            Icmp6SendErrMsg (pIf6, pIp6, ICMP6_PKT_PARAM_PROBLEM,
                             ICMP6_HDR_PROB,
                             *pu4Len + RT0_HDR_OFFSET_FOR_TYPE, pBuf);

            *pu4Len += (UINT4) u2ExtLen;

            IP6IF_INC_IN_DISCARDS (pIf6);
            IP6SYS_INC_IN_DISCARDS (pIp6Cxt->Ip6SysStats);

            return IP6_EXT_HDR_DISCARD;
        }
    }

    if (pRtEhdr->u1SegmentsLeft == 0)
    {

        IP6_TRC_ARG (pIp6Cxt->u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                     "IP6EXH:Ip6PrcsRtHdr: final dest reached \n");

        *pu4Len += (UINT4) u2ExtLen;

        /* Set the read offset of the CRU buffer to the starting of
         * next header beyond the routing header
         */
        IP6_BUF_READ_OFFSET (pBuf) = *pu4Len;

        return IP6_SUCCESS;
    }

    /* If the extention header length is odd or greater than 46 than
     * generate an ICMPv6 error message pointing to the extention header
     * length
     */
    else if (pRtEhdr->u1Hdrlen & 0x01)
    {
        IP6_TRC_ARG (pIp6Cxt->u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                     "IP6EXH:Ip6PrcsRtHdr:xHdrLen odd or >46 Send "
                     "ICMP6ErMsg[PProb,code0]offset pt Len\n");

        Icmp6SendErrMsg (pIf6, pIp6, ICMP6_PKT_PARAM_PROBLEM,
                         ICMP6_HDR_PROB, *pu4Len +
                         RT0_HDR_OFFSET_FOR_HDR_LEN, pBuf);

        *pu4Len += (UINT4) u2ExtLen;

        return IP6_EXT_HDR_DISCARD;
    }
    else
    {

        /* no of addresses in the routing header */
        u1Addrs = (UINT1) (pRtEhdr->u1Hdrlen >> 1);

        /* If the segments left field is greater than the addresses in the
         * the routing header then generate ICMPv6 error message
         */
        if (pRtEhdr->u1SegmentsLeft > u1Addrs)
        {
            IP6_TRC_ARG (pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                         DATA_PATH_TRC, IP6_NAME,
                         "IP6EXH:Ip6PrcsRtHdr: SegLeftFld>addr send "
                         "ICMP6[PProb,code 0]offset->SegLeftFld\n");

            Icmp6SendErrMsg (pIf6, pIp6, ICMP6_PKT_PARAM_PROBLEM,
                             ICMP6_HDR_PROB, *pu4Len +
                             RT0_HDR_OFFSET_FOR_SEGMENTS_LEFT, pBuf);

            *pu4Len += (UINT4) u2ExtLen;

            return IP6_EXT_HDR_DISCARD;

        }
        else
        {

            --pRtEhdr->u1SegmentsLeft;

            /* Update the segments-left field in the buffer */
            IP6_BUF_SET_1_BYTE (pBuf, *pu4Len + IP6_OFFSET_FOR_SEGMENT_FIELD,
                                pRtEhdr->u1SegmentsLeft);
            /* 
             * Compute the index of the next address to be visited
             * in the address vector
             */
            u1Count = (INT1) (u1Addrs - pRtEhdr->u1SegmentsLeft);

            --u1Count;

            /* Move the offset by u1Count*(sizeof(addr6)) into the pBuf
             * and get the u1Count th addr
             */
            p_addri = Ip6BufRead (pBuf, (UINT1 *) &tmp_addri, (UINT4)
                                  (IP6_BUF_READ_OFFSET (pBuf) +
                                   u1Count * sizeof (tIp6Addr)),
                                  (UINT4) sizeof (tIp6Addr), 0);
            if (p_addri == NULL)
            {

                IP6_TRC_ARG (pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                             DATA_PATH_TRC, IP6_NAME,
                             "IP6EXH : Ip6PrcsRtHdr: Got a NULL p_addri \n");
                IP6_TRC_ARG3 (pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                              BUFFER_TRC, IP6_NAME,
                              "%s buf err: Type = %d  Bufptr = %p",
                              ERROR_FATAL_STR, ERR_BUF_READ, pBuf);

                if (Ip6BufRelease
                    (pIp6Cxt->u4ContextId, pBuf, FALSE,
                     IP6_MODULE) == IP6_FAILURE)
                {
                    IP6_TRC_ARG1 (pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                                  BUFFER_TRC, IP6_NAME,
                                  "IP6EXH : Ip6PrcsRtHdr: Failure in releasing "
                                  "buffer ptr = %p \n", pBuf);

                    IP6_TRC_ARG3 (pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                                  BUFFER_TRC, IP6_NAME,
                                  "%s buf err: Type = %d  Bufptr = %p",
                                  ERROR_FATAL_STR, ERR_BUF_RELEASE, pBuf);
                }

                IP6IF_INC_IN_DISCARDS (pIf6);
                IP6SYS_INC_IN_DISCARDS (pIp6Cxt->Ip6SysStats);

                return IP6_EXT_HDR_DISCARD;
            }

            Ip6AddrCopy (&addri, p_addri);
            u1AddrType = Ip6AddrType (&addri);

            if (u1AddrType == ADDR6_MULTI)
            {

                IP6_TRC_ARG1 (pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                              BUFFER_TRC, IP6_NAME,
                              "IP6EXH : Ip6PrcsRtHdr: Addr in routing hdr was "
                              "mcast .Rel buf ptr = %p \n", pBuf);

                /* Release buf */
                if (Ip6BufRelease
                    (pIp6Cxt->u4ContextId, pBuf, FALSE,
                     IP6_MODULE) == IP6_FAILURE)
                {
                    IP6_TRC_ARG1 (pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                                  BUFFER_TRC, IP6_NAME,
                                  "IP6EXH : Ip6PrcsRtHdr:Failure in rel buf "
                                  "ptr = %p \n", pBuf);

                    IP6_TRC_ARG3 (pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                                  BUFFER_TRC, IP6_NAME,
                                  "%s buf err: Type = %d  Bufptr = %p",
                                  ERROR_FATAL_STR, ERR_BUF_RELEASE, pBuf);
                }

                IP6IF_INC_IN_DISCARDS (pIf6);
                IP6SYS_INC_IN_DISCARDS (pIp6Cxt->Ip6SysStats);

                return IP6_EXT_HDR_DISCARD;
            }
            else if (u1AddrType == ADDR6_UNSPECIFIED)
            {
                IP6_TRC_ARG1 (pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                              DATA_PATH_TRC, IP6_NAME,
                              "IP6EXH:Ip6PrcsRtHdr:Addr in the routing hdr "
                              "was unspecified.Rel buf ptr = %p\n", pBuf);

                /* Release buf */
                if (Ip6BufRelease
                    (pIp6Cxt->u4ContextId, pBuf, FALSE,
                     IP6_MODULE) == IP6_FAILURE)
                {
                    IP6_TRC_ARG1 (pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                                  BUFFER_TRC, IP6_NAME,
                                  "IP6EXH : Ip6PrcsRtHdr:Failure in rel buf "
                                  "ptr = %p \n", pBuf);

                    IP6_TRC_ARG3 (pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                                  BUFFER_TRC, IP6_NAME,
                                  "%s buf err: Type = %d  Bufptr = %p",
                                  ERROR_FATAL_STR, ERR_BUF_RELEASE, pBuf);
                }

                IP6IF_INC_IN_DISCARDS (pIf6);
                IP6SYS_INC_IN_DISCARDS (pIp6Cxt->Ip6SysStats);

                return IP6_EXT_HDR_DISCARD;
            }
            else
            {
                /* Swap the IPv6 destination address and addri
                 * in the CRU buffer and also in the pIp6 hdr
                 */

                IP6_TRC_ARG3 (pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                              DATA_PATH_TRC, IP6_NAME,
                              "IP6EXH:Ip6PrcsRtHdr:New Dest Addr=%s is %dth "
                              "addr in list of addr.SegLeft= %d\n",
                              Ip6PrintAddr (&addri), u1Count,
                              pRtEhdr->u1SegmentsLeft);

                /* Write the older destination address into the
                 * place where u1Count th address was there.
                 */

                Ip6AddrCopy (&tmp_dest, &(pIp6->dstAddr));

#ifdef MIP6_WANTED
                if (pRtEhdr->u1Type == IP6_RT2_HDR)
                {
                    i4RetVal = Ip6BufWrite (pBuf, (UINT1 *) &tmp_dest,
                                            (UINT4) (*pu4Len + sizeof (tRtHdr) +
                                                     sizeof (UINT4) +
                                                     u1Count *
                                                     sizeof (tIp6Addr)),
                                            sizeof (tIp6Addr), 1);
                }
#endif

                if (pRtEhdr->u1Type == IP6_RT0_HDR)
                {
                    i4RetVal = Ip6BufWrite (pBuf, (UINT1 *) &tmp_dest,
                                            (UINT4) (*pu4Len + sizeof (tRtHdr) +
                                                     sizeof (tRt0Hdr) +
                                                     u1Count *
                                                     sizeof (tIp6Addr)),
                                            sizeof (tIp6Addr), 1);
                }

                if (i4RetVal == IP6_FAILURE)
                {

                    IP6_TRC_ARG1 (pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                                  DATA_PATH_TRC, IP6_NAME,
                                  "IP6EXH:IP6 Ip6PrcsRtHdr: unable to do write "
                                  "onto buf = %p\n", pBuf);

                    IP6_TRC_ARG3 (pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                                  BUFFER_TRC, IP6_NAME,
                                  "%s buf err: Type = %d  Bufptr = %p",
                                  ERROR_FATAL_STR, ERR_BUF_WRITE, pBuf);

                    if (Ip6BufRelease
                        (pIp6Cxt->u4ContextId, pBuf, FALSE,
                         IP6_MODULE) == IP6_FAILURE)
                    {
                        IP6_TRC_ARG1 (pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                                      DATA_PATH_TRC, IP6_NAME,
                                      "IP6EXH : Ip6PrcsRtHdr:Failure in rel "
                                      "buf = %p \n", pBuf);

                        IP6_TRC_ARG3 (pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                                      BUFFER_TRC, IP6_NAME,
                                      "%s buf err: Type = %d  Bufptr = %p",
                                      ERROR_FATAL_STR, ERR_BUF_RELEASE, pBuf);
                    }

                    IP6IF_INC_IN_DISCARDS (pIf6);
                    IP6SYS_INC_IN_DISCARDS (pIp6Cxt->Ip6SysStats);

                    return IP6_EXT_HDR_DISCARD;
                }

                /* Write the u1Count th address into the place where the older
                 * destination address was there.
                 */
                if (Ip6BufWrite (pBuf, (UINT1 *) &addri,
                                 (UINT4) (sizeof (tIp6Hdr) -
                                          sizeof (tIp6Addr)),
                                 sizeof (tIp6Addr), 1) == IP6_FAILURE)
                {

                    IP6_TRC_ARG1 (pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                                  BUFFER_TRC, IP6_NAME,
                                  "IP6EXH : Ip6PrcsRtHdr: unable to do write "
                                  "onto buf = %p\n", pBuf);

                    IP6_TRC_ARG3 (pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                                  BUFFER_TRC, IP6_NAME,
                                  "%s buf err: Type = %d  Bufptr = %p",
                                  ERROR_FATAL_STR, ERR_BUF_WRITE, pBuf);

                    if (Ip6BufRelease
                        (pIp6Cxt->u4ContextId, pBuf, FALSE,
                         IP6_MODULE) == IP6_FAILURE)
                    {
                        IP6_TRC_ARG1 (pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                                      BUFFER_TRC, IP6_NAME,
                                      "IP6EXH : Ip6PrcsRtHdr:Failure in "
                                      "releasing buffer ptr = %p \n", pBuf);

                        IP6_TRC_ARG3 (pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                                      BUFFER_TRC, IP6_NAME,
                                      "%s buf err: Type = %d  Bufptr = %p",
                                      ERROR_FATAL_STR, ERR_BUF_RELEASE, pBuf);
                    }

                    IP6IF_INC_IN_DISCARDS (pIf6);
                    IP6SYS_INC_IN_DISCARDS (pIp6Cxt->Ip6SysStats);

                    return IP6_EXT_HDR_DISCARD;
                }

                Ip6AddrCopy (&pIp6->dstAddr, &addri);

                /* 
                 * Check if Strict Source Route by checking the corresponding
                 * bit in the bitmap
                 */
                if (Ip6IsPktToMeInCxt (pIp6Cxt, pu1Type, ADDR6_LLOCAL,
                                       &(pIp6->dstAddr), pIf6) == IP6_SUCCESS)
                {
                    if (pRtEhdr->u1SegmentsLeft == 0)
                    {
                        *pu4Len += (UINT4) u2ExtLen;

                        /* Set the read offset of the CRU buffer to the
                         * starting of next header beyond the routing header
                         */
                        IP6_BUF_READ_OFFSET (pBuf) = *pu4Len;
                        return IP6_SUCCESS;
                    }
                    if (pRtEhdr->u1SegmentsLeft >= 1)
                    {
                        /* Segements left more than 1, so loop back 
                         * with the same node.*/
                        IP6_BUF_READ_OFFSET (pBuf) = u4Len;
                        Ip6Rcv (u1Type, u4Len, pIp6, pIf6, pBuf);

                    }
                    return IP6_EXT_HDR_DISCARD;

                }
#ifdef MN_WANTED
                else if (Mip6IsHomeAddr (&(pIp6->dstAddr), &u4Index)
                         == IP6_SUCCESS)
                {
                    if ((pRtEhdr->u1SegmentsLeft == 0)
                        && (pRtEhdr->u1Type == IP6_RT2_HDR))
                    {
                        *pu4Len += (UINT4) u2ExtLen;

                        /* Set the read offset of the CRU buffer to the
                         * starting of next header beyond the routing header
                         */
                        IP6_BUF_READ_OFFSET (pBuf) = *pu4Len;
                        return IP6_SUCCESS;
                    }
                    return IP6_EXT_HDR_DISCARD;
                }
#endif
                else
                {
                    if (*pu1Type & (UINT1) (ADDR6_COMPLETE (pIf6)))
                    {
                        Ip6Forward (pIp6, pIf6, u4TotLen, IP6_FWD_SRCRT, pBuf);
                    }
                    else
                    {
                        Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE,
                                       IP6_MODULE);
                    }
                    return IP6_EXT_HDR_DISCARD;
                }
            }
        }
    }
}

/******************************************************************************
 * DESCRIPTION : Handles the processing of Pad1 Option
 *
 * INPUTS      : Ponter to the buffer (pBuf) that contains the option
 *
 * OUTPUTS     : Next option value (*pu1ByteToRead) and the remaining
 *               number of bytes for furthur processing
 *
 * RETURNS     : None
 *
 * NOTES       : Pad1 option can occur in either Hop-by-Hop or Destination
 *               options
 ******************************************************************************/
INT4
Ip6PrcsPad1Opt (tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 *pu1OptType,
                UINT4 *pu4BalanceBytes, UINT4 *pu4ProcessedLen)
{
    UINT1               u1TmpOptType = 0;
    UINT1              *pu1TmpOptType = NULL;

    *pu4ProcessedLen += 1;

    /* u4BalanceByte is decremented for the Bytes Extracted */
    *pu4BalanceBytes -= 1;

    /*Next u1ByteToRead is the Next Byte */
    if ((pu1TmpOptType = Ip6BufRead (pBuf, &u1TmpOptType, *pu4ProcessedLen,
                                     sizeof (UINT1), TRUE)) == NULL)
    {
        return IP6_FAILURE;
    }
    *pu1OptType = *pu1TmpOptType;
    return IP6_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : Handles the processing of Unrecognized Options
 *
 * INPUTS      : Ponter to the buffer (pBuf) that contains the option
 *
 * OUTPUTS     : Next option value (*pu1ByteToRead) and the remaining
 *               number of bytes for furthur processing
 *
 * RETURNS     : None
 *
 * NOTES       : Processing of unrecognized options are done accroding to 
 *               RFC2460
 ******************************************************************************/
INT4
Ip6PrcsUnRecOpt (tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 *pu1ByteToRead,
                 UINT4 *pu4BalanceBytes, tIp6If * pIf6, tIp6Hdr * pIp6)
{
    UINT2               u2OptDataLen = 0;
    tIp6Cxt            *pIp6Cxt = NULL;
    tCRU_BUF_CHAIN_HEADER *pErrBuf = NULL;
    UINT4               u4OptDataLen = 0;

    pIp6Cxt = pIf6->pIp6Cxt;

    if ((*pu1ByteToRead & DEST_HDR_OPTION_00) == *pu1ByteToRead)
    {
        /* skip and continue processing the header */
        IP6_BUF_READ_OFFSET (pBuf) += 1;
        EXTRACT_1_BYTE (pBuf, u2OptDataLen);
        IP6_BUF_READ_OFFSET (pBuf) += (u2OptDataLen + 1);
        EXTRACT_1_BYTE (pBuf, *pu1ByteToRead);

        if (*pu4BalanceBytes < (u4OptDataLen = u2OptDataLen + IP6_TWO))
        {
            return IP6_FAILURE;
        }

        *pu4BalanceBytes -= (u2OptDataLen + IP6_TWO);
        return IP6_SUCCESS;
    }

    if ((*pu1ByteToRead & DEST_HDR_OPTION_01) == *pu1ByteToRead)
    {
        /* discard the packet */
        IP6IF_INC_IN_DISCARDS (pIf6);
        IP6SYS_INC_IN_DISCARDS (pIp6Cxt->Ip6SysStats);

        return IP6_FAILURE;
    }

    if ((*pu1ByteToRead & DEST_HDR_OPTION_10) == *pu1ByteToRead)
    {
        /* discard the packet */
        IP6IF_INC_IN_DISCARDS (pIf6);
        IP6SYS_INC_IN_DISCARDS (pIp6Cxt->Ip6SysStats);

        pErrBuf = CRU_BUF_Duplicate_BufChain (pBuf);
        if (pErrBuf != NULL)
        {
            IP6_TRC_ARG (pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                         DATA_PATH_TRC, IP6_NAME,
                         "IP6EXH:Ip6PrcsUnRecOpt:Send ICMPErrMsg<ParamProb. code 2>");
            Icmp6SendErrMsg (pIf6, pIp6, ICMP6_PKT_PARAM_PROBLEM,
                             ICMP6_UNKNOWN_OPTTYPE, IP6_BUF_READ_OFFSET (pBuf),
                             pErrBuf);
        }
        return IP6_FAILURE;
    }

    if ((*pu1ByteToRead & DEST_HDR_OPTION_11) == *pu1ByteToRead)
    {
        if (!(IS_ADDR_MULTI (pIp6->dstAddr)))
        {
            pErrBuf = CRU_BUF_Duplicate_BufChain (pBuf);
            if (pErrBuf != NULL)
            {
                Icmp6SendErrMsg (pIf6, pIp6, ICMP6_PKT_PARAM_PROBLEM,
                                 ICMP6_UNKNOWN_OPTTYPE,
                                 IP6_BUF_READ_OFFSET (pBuf), pErrBuf);
            }
        }
        /* discard the packet */
        IP6IF_INC_IN_DISCARDS (pIf6);
        IP6SYS_INC_IN_DISCARDS (pIp6Cxt->Ip6SysStats);

        IP6_TRC_ARG (pIp6Cxt->u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                     "IP6EXH: Ip6PrcsUnRecOpt:Pkt Discarded. Destination "
                     "Option - Last 2  bits set");
        return IP6_FAILURE;
    }
    return IP6_SUCCESS;
}

 /****************************************************************************** * DESCRIPTION : Handles the processing of Jumbogram option
 *
 * INPUTS      : Ponter to the buffer (pBuf) that contains the option
 *
 * OUTPUTS     : Next option value (*pu1ByteToRead) and the remaining
 *               number of bytes for furthur processing
 *
 * RETURNS     : None
 *
 * NOTES       : None
 ******************************************************************************/
PRIVATE INT4
Ip6PrcsJmbOpt (pBuf, pu1ByteToRead, pu4BalanceBytes, pu4TotLen, pIf6, pIp6)
     tCRU_BUF_CHAIN_HEADER *pBuf;
     UINT1              *pu1ByteToRead;

     UINT4              *pu4BalanceBytes;
     UINT4              *pu4TotLen;
     tIp6If             *pIf6;
     tIp6Hdr            *pIp6;
{

    tJmbHdr            *pJmbHdr, JmbHdr;

    if (*pu4BalanceBytes < JMB_HDR_LEN)
    {
        return IP6_FAILURE;
    }
    pIf6->pIp6Cxt->u2JmbRecdPkts++;

    if ((pJmbHdr =
         (tJmbHdr *) Ip6BufRead (pBuf, (UINT1 *) &JmbHdr,
                                 IP6_BUF_READ_OFFSET (pBuf),
                                 sizeof (tJmbHdr) - IP6_TWO, FALSE)) == 0)
    {
        return IP6_FAILURE;
    }

    if (Ip6JmbHdrValid (pBuf, pIf6, pIp6, pJmbHdr, *pu4TotLen) == IP6_FAILURE)
    {
        return IP6_FAILURE;
    }

    /* Payload length of the IPv6 packet is taken from Jumbogram header */
    *pu4TotLen = pJmbHdr->u4JmbLen;

    /*Next u1ByteToRead is the Next Byte */

    EXTRACT_1_BYTE (pBuf, *pu1ByteToRead);

    /* u4BalanceByte is decremented for the Bytes Extracted */
    *pu4BalanceBytes -= JMB_HDR_LEN;

    return IP6_SUCCESS;

}

 /****************************************************************************** * DESCRIPTION : Handles the processing of Router Alert Option
 *
 * INPUTS      : Ponter to the buffer (pBuf) that contains the option
 *
 * OUTPUTS     : Next option value (*pu1ByteToRead) and the remaining
 *               number of bytes for furthur processing
 *
 * RETURNS     : IP6_ACCEPTED - If packet forwarded to the higher layer.
 *               IP6_SUCCESS - if packet processed successfully.
 *               IP6_FAILURE - On Error.
 *
 * NOTES       : Router Alert option occurs in Hop-by-Hop options
 ******************************************************************************/
PRIVATE INT4
Ip6PrcsRtAlertOpt (UINT4 u4Len, tIp6If * pIf6, tIp6Hdr * pIp6,
                   tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 u1IsFinalDest,
                   UINT1 *pu1ByteToRead, UINT4 *pu4BalanceBytes,
                   UINT2 u2HopbyHopPadLen)
{
    tIp6RtAlertHdr      RtAlertHdr;

    if (*pu4BalanceBytes < (sizeof (tIp6RtAlertHdr)))
    {
        return IP6_FAILURE;
    }
    if (Ip6BufRead (pBuf, (UINT1 *) &RtAlertHdr, IP6_BUF_READ_OFFSET (pBuf),
                    sizeof (tIp6RtAlertHdr), TRUE) == NULL)
    {
        return IP6_FAILURE;
    }
    if (u2HopbyHopPadLen > 0)
    {
        IP6_BUF_READ_OFFSET (pBuf) += u2HopbyHopPadLen;    /* u4Reserved in Rt2Hdr */
    }

    *pu4BalanceBytes -= sizeof (tIp6RtAlertHdr);
    *pu1ByteToRead += sizeof (tIp6RtAlertHdr);

    if (RtAlertHdr.u2OptValue == ICMP6_MLD_PKT)
    {
        if (pIp6->u1Hlim > 1)
        {
            /* RFC 3810 -  Section 5
             * All MLDv2 messages MUST be sent with a link-local IPv6 Source
             * Address, an IPv6 Hop Limit of 1, and an IPv6 Router Alert option
             * [RFC2711] in a Hop-by-Hop Options header.
             * So dropping the packet since Hop limit is greater than 1.*/
            return IP6_FAILURE;
        }

        if (u1IsFinalDest == OSIX_FALSE)
        {
            /* Packet is not destined to this router and is to be forwarded.
             * Post it to the MLD layer for forwarding */
            Icmp6Rcv (pIf6, pIp6, (UINT2) u4Len, ADDR6_MULTI, pBuf);
            /*
             * MLD packet will be furthur processed by MLD Task via ICMP6
             * and need not be furthur processed at IP6 task.
             * So returning IP6_ACCEPTED
             */
            *pu4BalanceBytes = 0;
            *pu1ByteToRead = 0;
            return IP6_ACCEPTED;
        }
    }
    return IP6_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : Handles the processing of PadN Option
 *
 * INPUTS      : Ponter to the buffer (pBuf) that contains the option
 *
 * OUTPUTS     : Next option value (*pu1ByteToRead) and the remaining
 *               number of bytes for furthur processing
 *
 * RETURNS     : None
 *
 * NOTES       : PadN option can occur in either Hop-by-Hop or Destination
 *               options
 ******************************************************************************/ INT4
Ip6PrcsPadNOpt (tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 *pu1ByteToRead,
                UINT4 *pu4BalanceBytes)
{
    UINT1               u1OptDataLen = 0;
    IP6_BUF_READ_OFFSET (pBuf) += 1;

    EXTRACT_1_BYTE (pBuf, u1OptDataLen);

    if (*pu4BalanceBytes < (UINT4) (u1OptDataLen + IP6_TWO))
    {
        return IP6_FAILURE;
    }
    IP6_BUF_READ_OFFSET (pBuf) += (u1OptDataLen + 1);

    /* PADN option data len cannot be greater than 8 */
    if (u1OptDataLen > BYTE_LENGTH)
    {
        return IP6_FAILURE;
    }

    /*Next u1ByteToRead is the Next Byte */
    EXTRACT_1_BYTE (pBuf, *pu1ByteToRead);

    /* u4BalanceByte is decremented for the Bytes Extracted */
    *pu4BalanceBytes -= (u1OptDataLen + IP6_TWO);

    return IP6_SUCCESS;
}

/***************************** END OF FILE **********************************/
