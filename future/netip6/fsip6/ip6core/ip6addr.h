/*******************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ip6addr.h,v 1.11 2017/12/26 13:34:21 siva Exp $
 *
 * Description: This file contains the typedefs for the IPv6 Address structures
 *   as well as related constants, macros and function prototypes.
 *
 *******************************************************************/
#ifndef _IP6ADDR_H
#define _IP6ADDR_H

/*
 *----------------------------------------------------------------------------- 
 *    FILE NAME                    :    ip6addr.h
 *
 *    PRINCIPAL AUTHOR             :    Vivek Venkatraman / Kaushik Biswas
 *
 *    SUBSYSTEM NAME               :    IPv6
 *
 *    MODULE NAME                  :    IP6 ADDR Submodule
 *
 *    LANGUAGE                     :    C
 *
 *    TARGET ENVIRONMENT           :    UNIX
 *
 *    DATE OF FIRST RELEASE        :    DDD-MM-1996
 *
 *    DESCRIPTION                  :    This file contains the typedefs
 *                                      for the IPv6 Address structures
 *                                      as well as related constants, macros 
 *                                      and function prototypes.
 *
 *----------------------------------------------------------------------------- 
 */

/*
 * The following strcuture contains the parameters for the link-local addresses
 * on a IPv6 interface [RFC 1884]. These addresses are automatically generated 
 * (NOT manually configured) by concatenating the prefix 0xFE80 and the 
 * lower-layer interface token. The interface token is got from the MAC address 
 * for LAN interfaces and the IPv4 address for configured tunnel interfaces. 
 * For other interfaces, it can be got from the interface or taken from the 
 * configured value ('ifaceTok' in IP6_IF) or learnt from interface layer 
 * after negotiation (e.g. PPP).
 *
 * The structure contains the interface pointer, status of the address and
 * parameters for DAD [RFC 1971] for this address.
 *
 * NOTE (Apr 8, 1997):
 *   Though the link-local addresses are maintained as a linked-list on the
 * interface, only one address is effectively used as the interface layer
 * (Ethernet, PPP or FR) can indicate only one token to IPv6. One possible way
 * to have multiple addresses is to make the other addresses configurable.
 */

#define  IP6_ADDR_AUTO_SL   2   /* STATE-LESS Auto Configuration. */
#define  IP6_ADDR_AUTO_SF   3   /* STATE-FUL Auto Configuration */
#define  IP6_ADDR_DYNAMIC   4   /* DYNAMIC Configuration - other than above
                                 * configuration method. Eg - dynamic
                                 * creation of address in compat tunnel
                                 * interface. */ 

/*
 * The following strcuture contains the parameters for the unicast/anycast
 * addresses on a IPv6 interface [RFC 1884]. These addresses are manually
 * configured with a 'type' field specifying whether it is unicast or anycast.
 * Other than address and type, the prefix length of the address should also
 * be configured.
 *
 * When an address is configured, it is added into two hash tables - one for
 * the address and the other for the subnet-router anycast address that is
 * formed from the address.
 *
 * Other fields are the interface pointer, status of the address, index into
 * address profile table which contains parameters relevant for Prefix Adv.
 * and parameters for DAD [RFC 1971] for this address.
 */


typedef struct _IP6_MULTICAST_INFORMATION
{

    tTMO_SLL_NODE  nextAif;     /* Pointer to next multicast
                                 * address on this interface */
    /* configurable parameters  */
    tIp6Addr  ip6Addr;          /* IPv6 MCAST address */
    UINT2     u2RefCnt;         /* Reference count */
    UINT1     u1AddrScope;     /*Scope of the multicast Address- Added for RFC4007*/
    UINT1     u1Pad;
}tIp6McastInfo;

/*
 * The following strcuture contains parameters used for Prefix Adv. by the
 * ND protocol [RFC 1970]. The table is indexed using the 'u1Addr6Profile'
 * in the address structure. For addresses on non-LAN interfaces, the value of
 * this field is 0 as ND adv. are not done on these interfaces.
 */

typedef struct _IP6_ADDR_PROFILE_INFORMATION
{
    UINT4       u4ValidLife;    /* Valid lifetime of the prefix */
    UINT4       u4PrefLife;     /* Preferred lifetime of the prefix */
    UINT2       u2RefCnt;       /* Number of Prefixes using this Profile */
    UINT1       u1AdminStatus;  /* Admin Status of the entry */
    UINT1       u1RaPrefCnf;    /* Whether to advertise this prefix
                                 * and its usage for on-link and
                                 * address autoconfiguration */
}
tIp6AddrProfile;

#define IP6_MAX_FILTER (IP6_MAX_LOGICAL_IFACES + 1)

typedef struct _IP6_MAC_FILTER_INFORMATION
{
    UINT4    u4Index;     /*  if index */
    tTMO_SLL MacAddrList; /* List of mac address configured on this lan
                             index */
}tIp6MacFilter;

typedef struct _IP6_MAC_SLL_NODE_INFORMATION
{
    tTMO_SLL_NODE NextNode;
    UINT1 u1MacAddr[IP6_MAX_ENET_ADDR_LEN];
    UINT2 u2Padding;
}tIp6MacFilterNode;


/*The following structure is used for destination selection algorithm (RFC 3484)*/
typedef struct _IP6_DST_ADDR
{
    tTMO_SLL_NODE DstNode;
    tIp6Addr  dstaddr;                        /*Ipv6 destination address */
    UINT4     u4IfIndex;                      /*Interface Index */
    UINT1     u1CumulativeScore;              /*Cumulative Score*/
    UINT1     u1Precedence;                   /*Precedence value */
    UINT1     u1Scope;                        /*Scope of the address */
    UINT1     u1MatchLen;                     /*Matching prefixlen */
    UINT1     u1PrefixLen;                    /*Prefix Length of dst address*/
    UINT1     au1Padding[3];
}
tIp6DstAddr;

typedef struct _IP6_MC_REG
{
    tIp6Addr  MCastaddr;                        /*Ipv6 destination address */
    UINT4     u4IfIndex;                      /*Interface Index */
    UINT1     u1MsgType;                      /*Prefix Length of dst address*/
    UINT1     au1Padding[3];
}
tIp6McRegNode;

/*
 * Constants and macros for the address table
 */

#define  IP6_NAME         "IP6"

#define ADDR_PTR_FROM_U_HASH(h)  \
        (tIp6AddrInfo *)((UINT1 *)(h) - IP6_OFFSET(tIp6AddrInfo,uHash))

#define ADDR_PTR_FROM_A_HASH(h)  \
        (tIp6AddrInfo *)((UINT1 *)(h) - IP6_OFFSET(tIp6AddrInfo,aHash))

#define ADDR_PTR_FROM_M_HASH(h)  \
        (tIp6McastInfo *)((UINT1 *)(h) - IP6_OFFSET(tIp6McastInfo,mHash))

#define IP6_GET_ADDR_INFO_FROM_SLL(x,y) \
      (y *)((UINT1 *)(x) - IP6_OFFSET(y,nextAif))

#define IP6_GET_ADDR_INFO_FROM_TIMER(x,y) \
      (y *)(VOID *)((UINT1 *)(x) - IP6_OFFSET(y,dadTimer))

#define IP6_NUM_ADDR_PREFIX(pIf6) \
        TMO_SLL_Count(&(pIf6->prefixlist))

#define IP6_GET_PREFIX_INFO_FROM_VALID_TIMER(x,y) \
      (y *)(VOID *)((UINT1 *)(x) - IP6_OFFSET(y,ValidTimer))

#define IP6_GET_PREFIX_INFO_FROM_PREFER_TIMER(x,y) \
      (y *)(VOID *)((UINT1 *)(x) - IP6_OFFSET(y,PreferTimer))

/* Values for status of the address */

#define  ADDR6_COMPLETE(pIf6)         ((UINT1)Ip6RetAddrStatComplete(pIf6))
#define  ADDR6_TENTATIVE        0x01 
#define  ADDR6_FAILED           0x04 
#define  ADDR6_UP               0x10 
#define  ADDR6_DOWN             0x20 

#define ADDR6_PREFERRED         0x08
#define ADDR6_DEPRECATED        ~(ADDR6_PREFERRED)
#define ADDR6_INACCESSIBLE      0x40
#define ADDR6_UNKNOWN           0x80


#define  ADDR6_LLOCAL_PREFIX_1  0xFE 
#define  ADDR6_LLOCAL_PREFIX_2  0x80 

#define IP6_ADDR_TYPE_STATELESS 0x01
#define IP6_ADDR_TYPE_STATEFUL  0x02

#define  PREFIX_CREATE          0x1
#define  PREFIX_DELETE          0x2


/*Policy table entries */

#define IP6_POLICY_STATUS_AUTO          1  /*Default entry in the table */
#define IP6_POLICY_STATUS_MANUAL        2  /*Configured entry */

# define IP6_MIN_PRECEDENCE             1   /*Minimum precdence value*/
# define IP6_MAX_PRECEDENCE             128 /*Maximum precdence value
                                              in the policy table*/

# define IP6_MIN_LABEL                  0   /*Minimum Label value*/
# define IP6_MAX_LABEL                  255 /*Maximum label value
                                              in the policy table*/

#define IP6_SELF_ADDR             1   /*Address configured on the given interface*/
#define IP6_NOT_SELF_ADDR         2   /*Address not configured on the given interface*/
#define IP6_DEFAULT_POLICY_ENTRY  3   /*Default address entry */

/*
 * DAD related constants
 */

#define  DAD_FAILURE  0 
#define  DAD_SUCCESS  1 

#define UDP6_ZERO             0
#define UDP6_ONE              1
#define UDP_IPV6_ADDR_TYPE    2

/*
 * Address Profile table related constants and macros
 */

/* Values for prefix advt configuration */


#define IP6_ADDR_RP_STATIC       1
#define IP6_ADDR_RP_DYNAMIC      2

#define  IP6_ADDR_ACCESS_IN_SHORT_INT  8  
#define  IP6_ADDR_ACCESS_IN_WORDS      4  

#ifndef IPV6_ROUTER


#define IP6_ADDR_FROM_PREF_TIMER(x)\
      (tIp6AddrInfo *)((UINT1 *)(x) - IP6_OFFSET(tIp6AddrInfo,AddrPrefTimer))

#define IP6_ADDR_FROM_VALID_TIMER(x)\
      (tIp6AddrInfo *)((UINT1 *)(x) - IP6_OFFSET(tIp6AddrInfo,AddrValidTimer))

#endif  /* IPV6_ROUTER */

#endif /* !_IP6ADDR_H */
