/********************************************************************
* Copyright (C) 2011 Aricent Inc . All Rights Reserved
*
* $Id: ip6usrke.c,v 1.1 2011/07/19 11:12:30 siva Exp $
*
* Description : This file contains wrapper function provided to the secv6
*               module.This file is compiled for user and kernel space.
*
*******************************************************************/
#include "lr.h"
#include "cfa.h"
#include "cust.h"
#include "ipv6.h"
#include "ip6.h"
#include "ip.h"
#include "ip6sys.h"
#include "ip6frag.h"
#include "ip6trace.h"
#include "secmod.h"

/******************************************************************************
* DESCRIPTION : This function is wrapper for the Ip6GetHlProtocolInCxt in case
                of complete userspace. Called by the secv6 module only.
*
* INPUTS      : pBuf : pointer to the message buffer containing
*
* OUTPUTS     : pu1Nhdr, pu2Offset
*
* RETURNS     : IP6_SUCCESS or IP6_FAILURE
*
* NOTES       : None
******************************************************************************/
INT4
Ip6Secv6GetHlProtocol (tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 *pu1Nhdr,
                       UINT2 *pu2Offset)
{

#if !defined (SECURITY_KERNEL_WANTED)
    return (Ip6GetHlProtocol (pBuf, pu1Nhdr, pu2Offset));
#else

    UINT1               u1Nhdr = 0;
    UINT1              *pu1TmpNhdr = NULL;
    UINT2               u2OldLen = 0, u2Len = IP6_HDR_LEN;
    /* Updating processed length
     * to IPv6 Header length.
     */
    UINT1              *pu1Len = NULL, u1Len = 0;

    *pu1Nhdr = 0;

    if (CRU_BUF_Copy_FromBufChain (pBuf, &u1Nhdr, (IP6_BUF_READ_OFFSET (pBuf) +
                                                   IP6_OFFSET_FOR_NEXTHDR_FIELD),
                                   sizeof (UINT1)) == 0)
    {
        pu1TmpNhdr = NULL;
    }
    else
    {
        pu1TmpNhdr = &u1Nhdr;
        IP6_BUF_READ_OFFSET (pBuf) =
            IP6_BUF_READ_OFFSET (pBuf) + sizeof (UINT1);
    }

    /* Moving the valid offset to the start of next header */
    u2Len = (UINT2) (u2Len + IP6_BUF_READ_OFFSET (pBuf) - 1);
    while ((pu1TmpNhdr != NULL) && (*pu1TmpNhdr != NH_NO_NEXT_HDR))
    {
        switch (*pu1TmpNhdr)
        {
            case NH_AUTH_HDR:
            case NH_ESP_HDR:
            case NH_UDP6:
            case NH_TCP6:
            case NH_ICMP6:
            case NH_MOBILITY_HDR:

                *pu1Nhdr = *pu1TmpNhdr;
                if (pu2Offset != NULL)
                {
                    *pu2Offset = u2Len;
                }
                return IP6_SUCCESS;

            case NH_FRAGMENT_HDR:
                u2OldLen = u2Len;
                u2Len += sizeof (tIp6FragHdr);
                break;

            case NH_H_BY_H:
            case NH_DEST_HDR:
            case NH_ROUTING_HDR:
                u2OldLen = u2Len;
                if (CRU_BUF_Copy_FromBufChain (pBuf, &u1Len, u2Len + 1,
                                               sizeof (UINT1)) == 0)
                {
                    IP6_TRC_ARG1 (u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC,
                                  IP6_NAME,
                                  "IP6FWD:Ip6GetHlProtocol: BufRead Failed"
                                  " for the forwarded pkt%x \n", pBuf);
                    return IP6_FAILURE;
                }
                else
                {
                    pu1Len = &u1Len;
                    IP6_BUF_READ_OFFSET (pBuf) = IP6_BUF_READ_OFFSET (pBuf) +
                        sizeof (UINT1);
                }
                u2Len += (*pu1Len + 1) * BYTE_LENGTH;
                break;

            default:

                IP6_TRC_ARG1 (u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                              "IP6FWD: Ip6GetHlProtocol:Unknown protocol = %d"
                              " found for the forwarded pkt \n", *pu1TmpNhdr);

                return IP6_FAILURE;
        }
        IP6_BUF_READ_OFFSET (pBuf) = u2OldLen;
        if (CRU_BUF_Copy_FromBufChain (pBuf, &u1Nhdr, u2OldLen, sizeof (UINT1))
            == 0)
        {
            pu1TmpNhdr = NULL;
            IP6_TRC_ARG1 (u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                          "IP6FWD:Ip6GetHlProtocol: BufRead Failed for the"
                          "forwarded pkt%x \n", pBuf);
            return IP6_FAILURE;
        }
        else
        {
            IP6_BUF_READ_OFFSET (pBuf) = IP6_BUF_READ_OFFSET (pBuf) +
                sizeof (UINT1);
            pu1TmpNhdr = &u1Nhdr;
        }

    }
    return IP6_FAILURE;

#endif
}

/******************************************************************************
* DESCRIPTION : This function is wrapper for the Ip6GetGlobalAddr function
*               incase of complete userspce. Called by the secv6 module only.
*
* INPUTS      : The interface table index (i4Index) and the destination
*               IPv6 address (pAddr6)
*
* OUTPUTS     : None
*
* RETURNS     : The global address(tIp6Addr) or NULL
*
******************************************************************************/
tIp6Addr           *
Ip6Secv6GetGlobalAddr (UINT4 u4Index, tIp6Addr * pAddr6)
{
#if !defined (SECURITY_KERNEL_WANTED)
    return (Ip6GetGlobalAddr (u4Index, pAddr6));
#else
    return (Sec6UtilGetGlobalAddr (u4Index, pAddr6));
#endif
}

/**************************************************************************
* DESCRIPTION : This function is wrapper for the Ip6IsOurAddr function
*               incase of complete userspace. Called by the secv6 module only.
*
* INPUTS      : The IPv6 destination addr ptr  (pDstAddr) and
*               IPv6 source addr ptr (pSrcAddr),
*
* OUTPUTS     : None
*
* RETURNS     : IP6_SUCCESS / IP6_FAILURE
*
* NOTES       :
******************************************************************************/
INT4
Ip6Secv6IsOurAddr (tIp6Addr * pIp6Addr, UINT4 *pu4Index)
{
#if !defined (SECURITY_KERNEL_WANTED)
    return (Ip6IsOurAddr (pIp6Addr, pu4Index));
#else
    /* Call the securiry module API */
    if (Sec6UtilIpIfIsOurAddress (pIp6Addr, pu4Index) == OSIX_SUCCESS)
    {
        return IP6_SUCCESS;
    }
    return IP6_FAILURE;
#endif
}
