/*******************************************************************
 *   Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *   
 *   $Id: ip6snif.c,v 1.17 2015/06/17 04:58:56 siva Exp $
 **************************************************************/

#include "ip6inc.h"

tIp6Addr     gZeroIpv6Addr;

/******************************************************************************
 * DESCRIPTION : Gets the index of the next entry in the ipv6IfTable for a 
 *               given a current entry.The current entry need not be a 
 *               valid entry however.
 *
 * INPUTS      : pu4Index
 *
 * OUTPUTS     : Next entry(pu4Index)
 *
 * RETURNS     : IP6_SUCCESS/IP6_FAILURE 
 *
 * NOTES       :
 ******************************************************************************/

/*
 * Get the index of the next entry in the ipv6IfTable given a current entry.
 * The current entry need not be a valid entry however.
 */

INT4
Ip6ifGetNextIndex (UINT4 *pu4Index)
{
    UINT4               u4Index = (*pu4Index) + 1;

    IP6_IF_SCAN (u4Index, (UINT4) IP6_MAX_LOGICAL_IF_INDEX)
    {
        if (Ip6ifEntryExists (u4Index) == IP6_SUCCESS)
        {
            *pu4Index = u4Index;
            return IP6_SUCCESS;
        }
    }

    return IP6_FAILURE;
}

/******************************************************************************
 * DESCRIPTION :  For a given the index, check whether the valid ip6If 
 *                entry exists or not
 *
 * INPUTS      : u4Index
 *
 * OUTPUTS     : NONE. 
 *
 * RETURNS     : IP6_SUCCESS/IP6_FAILURE 
 *               
 *
 * NOTES       :
 ******************************************************************************/
/*
 * Given the index, check whether the ip6If entry exists or not
 */

INT1
Ip6ifEntryExists (UINT4 u4Index)
{
    if (Ip6ifGetEntry (u4Index) == NULL)
    {
        return IP6_FAILURE;
    }
    return IP6_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : Get the ip6If entry if it exists for the given index
 *
 * INPUTS      : u4Index  
 *
 * OUTPUTS     : NONE. 
 *
 * RETURNS     : Pointer to the if_entry on Success. NULL on Failure. 
 *               
 *
 * NOTES       :
 ******************************************************************************/
/*
 * Get the ip6If entry if it exists from the index
 */

tIp6If             *
Ip6ifGetEntry (UINT4 u4Index)
{
    tIp6If             *pIf6 = NULL;

    if ((u4Index > (UINT4) IP6_MAX_LOGICAL_IF_INDEX) || (u4Index <= 0))
    {
        return (NULL);
    }

    pIf6 = gIp6GblInfo.apIp6If[u4Index];
    if ((pIf6 != NULL) && (pIf6->u1AdminStatus != ADMIN_INVALID))
    {
        return (pIf6);
    }

    return (NULL);
}

/******************************************************************************
 * DESCRIPTION : Internal routine to get the next available address 
 *               This is used by both the get-first and get-next routines
 *
 * INPUTS      : Pointer to Interface Index (pu4Index), Pointer to Ip6Addr (pAddr6) 
 *               , Pointer to Prefix Length (pu1Prefixlen), Search Flag (u1SrchFlag)
 *
 * OUTPUTS     : NONE. 
 *
 * RETURNS     : IP6_SUCCESS/IP6_FAILURE 
 *               
 * NOTES       :
 *****************************************************************************/
INT4
Ip6AddrTblNextAddr (UINT4 *pu4Index, tIp6Addr * pAddr6, UINT1 *pu1Prefixlen,
                    UINT1 u1SrchFlag)
{
    UINT4               u4Index = 0;
    tIp6AddrInfo       *pNewAddrInfo = NULL;
    tIp6LlocalInfo     *pNewLlocalInfo = NULL;
    tTMO_SLL_NODE      *pSnode = NULL;

    u4Index = *pu4Index;

    IP6_IF_SCAN (u4Index, (UINT4) IP6_MAX_LOGICAL_IF_INDEX)
    {
        if (Ip6ifEntryExists (u4Index) != IP6_SUCCESS)
        {
            continue;
        }
        if ((u1SrchFlag & IP6_ADDR_NEXT_SRCH_ALIST))
        {
            pSnode = TMO_SLL_First (&gIp6GblInfo.apIp6If[u4Index]->addr6Ilist);
            if (pSnode != NULL)
            {
                pNewAddrInfo = IP6_ADDR_PTR_FROM_SLL (pSnode);

                *pu4Index = u4Index;
                Ip6AddrCopy (pAddr6, &pNewAddrInfo->ip6Addr);
                *pu1Prefixlen = pNewAddrInfo->u1PrefLen;

                return IP6_SUCCESS;
            }
        }

        pSnode = TMO_SLL_First (&gIp6GblInfo.apIp6If[u4Index]->lla6Ilist);
        if (pSnode != NULL)
        {
            pNewLlocalInfo = IP6_LLADDR_PTR_FROM_SLL (pSnode);
            *pu4Index = u4Index;
            Ip6AddrCopy (pAddr6, &pNewLlocalInfo->ip6Addr);
            *pu1Prefixlen = IP6_ADDR_SIZE_IN_BITS;

            return IP6_SUCCESS;
        }

        u1SrchFlag = IP6_ADDR_NEXT_SRCH_BOTH;
    }

    return IP6_FAILURE;
}

/******************************************************************************
 *DESCRIPTION : Returns entry ptr or NULL in the IPv6 Address Table(link-local)
 *
 * INPUTS      : Interface Index (pu4Index), Ip6Addr (pAddr6) 
 *
 * OUTPUTS     : NONE. 
 *
 * RETURNS     : Link Local Address Information if present, NULL Otherwise.
 *               
 * NOTES       :
 *****************************************************************************/
tIp6LlocalInfo     *
Ip6AddrTblGetLlocalEntry (UINT4 u4Index, tIp6Addr * pAddr6)
{
    tTMO_SLL_NODE      *pAddrSll = NULL;
    tIp6LlocalInfo     *pAddrInfo = NULL;

    if (Ip6ifEntryExists (u4Index) != IP6_SUCCESS)
    {
        return (NULL);
    }

    TMO_SLL_Scan (&gIp6GblInfo.apIp6If[u4Index]->lla6Ilist,
                  pAddrSll, tTMO_SLL_NODE *)
    {
        pAddrInfo = IP6_LLADDR_PTR_FROM_SLL (pAddrSll);

        if (MEMCMP (pAddr6, &pAddrInfo->ip6Addr, sizeof (tIp6Addr)) == 0)
        {
            return (pAddrInfo);
        }
    }

    return (NULL);
}

/******************************************************************************
 * DESCRIPTION : Returns entry ptr or NULL in the IPv6 Address Table(unicast)
 *
 * INPUTS      : Interface Index (u4Index), Ip6Addr (pAddr6) 
 *               Prefix Length (pu1Prefixlen)
 *
 * OUTPUTS     : NONE. 
 *
 * RETURNS     : Address Information if present, NULL Otherwise.
 *               
 * NOTES       :
 *****************************************************************************/

tIp6AddrInfo       *
Ip6AddrTblGetEntry (UINT4 u4Index, tIp6Addr * pAddr6, UINT1 u1Prefixlen)
{
    tTMO_SLL_NODE      *pAddrSll = NULL;
    tIp6AddrInfo       *pAddrInfo = NULL;

    if (Ip6ifEntryExists (u4Index) != IP6_SUCCESS)
    {
        return (NULL);
    }

    TMO_SLL_Scan (&gIp6GblInfo.apIp6If[u4Index]->addr6Ilist,
                  pAddrSll, tTMO_SLL_NODE *)
    {
        pAddrInfo = IP6_ADDR_PTR_FROM_SLL (pAddrSll);

        if ((MEMCMP (pAddr6, &pAddrInfo->ip6Addr, sizeof (tIp6Addr)) == 0)
            && (pAddrInfo->u1PrefLen == u1Prefixlen))
        {
            return (pAddrInfo);
        }
    }

    return (NULL);
}

/******************************************************************************
* DESCRIPTION : Returns entry ptr or NULL in the IPv6 Address Table(unicast)
*
 * INPUTS      : Interface Index (u4Index) 
 *
 * OUTPUTS     : NONE. 
 *
 * RETURNS     : Address Information if present, NULL Otherwise.
 *               
 * NOTES       :
 *****************************************************************************/

tIp6AddrInfo       *
Ip6AddrTblGetAddrInfo (UINT4 u4Index)
{
    tTMO_SLL_NODE      *pAddrSll = NULL;
    tIp6AddrInfo       *pAddrInfo = NULL;

    if (Ip6ifEntryExists (u4Index) != IP6_SUCCESS)
    {
        return (NULL);
    }

    TMO_SLL_Scan (&gIp6GblInfo.apIp6If[u4Index]->addr6Ilist, pAddrSll,
                  tTMO_SLL_NODE *)
    {
        pAddrInfo = IP6_ADDR_PTR_FROM_SLL (pAddrSll);

        if (pAddrInfo != NULL)
        {
            return (pAddrInfo);
        }
    }

    return (NULL);
}

/******************************************************************************
* DESCRIPTION : Returns entry ptr or NULL in the IPv6 Address Table(unicast)
*
* INPUTS      : Ip6Addr (pAddr6)
*
* OUTPUTS     : NONE.
*
* RETURNS     : Address if present, NULL Otherwise.
*
* NOTES       :
******************************************************************************/

tIp6AddrInfo       *
Ip6AddrAllIfScanInCxt (UINT4 u4ContextId, tIp6Addr * pAddr6, UINT1 u1Prefixlen)
{
    UINT2               u2Count = 1;
    tTMO_SLL_NODE      *pSllInfo = NULL;
    tIp6AddrInfo       *pInfo = NULL;
    tIp6If             *pIf6 = NULL;
    UINT1               u1MinPrefix = 0;

    IP6_IF_SCAN (u2Count, IP6_MAX_LOGICAL_IF_INDEX)
    {
        pIf6 = Ip6ifGetEntry (u2Count);
        if ((pIf6 == NULL) || (pIf6->pIp6Cxt->u4ContextId != u4ContextId))
        {
            continue;
        }
        TMO_SLL_Scan (&pIf6->addr6Ilist, pSllInfo, tTMO_SLL_NODE *)
        {
            pInfo = IP6_ADDR_PTR_FROM_SLL (pSllInfo);
            u1MinPrefix = ((pInfo->u1PrefLen) <
                           u1Prefixlen) ? (pInfo->u1PrefLen) : u1Prefixlen;
            if (Ip6AddrMatch (&(pInfo->ip6Addr), pAddr6, (INT4) u1MinPrefix))
            {
                return (pInfo);
            }
        }
    }
    return NULL;
}

/******************************************************************************
* DESCRIPTION : Returns entry ptr or NULL in the IPv6 Address Table(unicast)
*                for the given context.
*
* INPUTS      : Ip6Addr (pAddr6)
*               Prefix Length (pu1Prefixlen)
*
* OUTPUTS     : NONE.
*
* RETURNS     : Address if present, NULL Otherwise.
*
* NOTES       :
******************************************************************************/

tIp6AddrInfo       *
Ip6AddrScanAllIfInCxt (UINT4 u4ContextId, tIp6Addr * pAddr6, UINT1 u1Prefixlen)
{
    tIp6AddrInfo       *pInfo = NULL;

    pInfo = Ip6AddrAllIfScanInCxt (u4ContextId, pAddr6, u1Prefixlen);

    if (pInfo == NULL)
    {
        return NULL;
    }
    else
    {
        return pInfo;
    }
}

/******************************************************************************
 * DESCRIPTION : Checks if the Prefix is Greater
 *
 * INPUTS      : Pointer to IP6 Addr (pIp6Addr), Prefix Len (u1Prefix Len), 
 *               Protocol (i1Proto), Pointer to the Route Entry (*pRtEntry)
 *
 * OUTPUTS     : NONE. 
 *
 * RETURNS     : IP6_SUCCESS/IP6_FAILURE
 *               
 * NOTES       :
 *****************************************************************************/
INT1
Ip6IsPrefixGreater (tIp6Addr * pIp6Addr, UINT1 u1PrefixLen, INT1 i1Proto,
                    UINT4 u4Index, tIp6RtEntry * pRtEntry)
{
    if (Ip6IsAddrGreater (pIp6Addr, &pRtEntry->dst) == IP6_SUCCESS)
    {
        return IP6_SUCCESS;
    }
    if (Ip6AddrMatch (pIp6Addr, &pRtEntry->dst, IP6_ADDR_SIZE_IN_BITS) == TRUE)
    {
        if (pRtEntry->u1Prefixlen > u1PrefixLen)
        {
            return IP6_SUCCESS;
        }

        if (pRtEntry->u1Prefixlen == u1PrefixLen)
        {
            if (pRtEntry->i1Proto > i1Proto)
            {
                return IP6_SUCCESS;
            }
            if (pRtEntry->i1Proto == i1Proto)
            {
                if (pRtEntry->u4Index > u4Index)
                {
                    return IP6_SUCCESS;
                }
            }
        }
    }
    return IP6_FAILURE;
}

/******************************************************************************
 * DESCRIPTION : Checks if the Prefix is Inbetween
 *
 * INPUTS      : Pointer to IP6 Addr (pIp6Addr), Prefix Len (u1Prefix Len), 
 *               Protocol (i1Proto), Pointer to the Route Entry (*pRtEntry)
 *               Pointer to Route Entry (pRtTmp)
 *
 * OUTPUTS     : NONE. 
 *
 * RETURNS     : IP6_SUCCESS/IP6_FAILURE
 *               
 * NOTES       :
 *****************************************************************************/
INT1
Ip6IsPrefixInBetween (tIp6Addr * pIp6Addr, tIp6RtEntry * pRtTmp,
                      tIp6RtEntry * pRtEntry)
{
    if (Ip6IsAddrInBetween (pIp6Addr, &pRtTmp->dst,
                            &pRtEntry->dst) == IP6_SUCCESS)
    {
        return IP6_SUCCESS;
    }
    if (Ip6AddrMatch (&pRtTmp->dst, &pRtEntry->dst, IP6_ADDR_SIZE_IN_BITS)
        == TRUE)
    {
        if (pRtEntry->u1Prefixlen < pRtTmp->u1Prefixlen)
        {
            return IP6_SUCCESS;
        }

        if (pRtEntry->u1Prefixlen == pRtTmp->u1Prefixlen)
        {
            if (pRtEntry->i1Proto < pRtTmp->i1Proto)
            {
                return IP6_SUCCESS;
            }
            if (pRtEntry->i1Proto == pRtTmp->i1Proto)
            {
                if (pRtEntry->u4Index < pRtTmp->u4Index)
                {
                    return IP6_SUCCESS;
                }
            }
        }
    }
    return IP6_FAILURE;
}

/******************************************************************************
 * DESCRIPTION : Create a ping entry
 *
 * INPUTS      : Index to Ping Table (u2Index), Interfac Index (i2IfIndex), 
 *               Ping Destination (pPing6Dst), admin status (u1Admin)
 *
 * OUTPUTS     : NONE. 
 *
 * RETURNS     :
 *               
 * NOTES       :
 *****************************************************************************/
INT1
Ping6CreateEntry (UINT2 u2Index, UINT2 u2IfIndex, tIp6Addr * pPing6Dst,
                  UINT1 u1Admin)
{
    UINT1               u1Count = 0;

    pPing[u2Index]->u2Ping6MaxTries = IP6_PING_DEF_TRIES;
    pPing[u2Index]->u2Ping6Interval = IP6_PING_DEF_INTERVAL;
    pPing[u2Index]->u2Ping6Size = IP6_PING_DEF_SIZE;
    pPing[u2Index]->u2Ping6RcvTimeout = IP6_PING_DEF_RCV_TIMEOUT;
    pPing[u2Index]->u1Admin = u1Admin;

    pPing[u2Index]->u2IfIndex = u2IfIndex;
    if (pPing6Dst)
    {
        Ip6AddrCopy (&pPing[u2Index]->ping6Dst, pPing6Dst);
    }

    pPing[u2Index]->u2SentCount = 0;
    pPing[u2Index]->u2AverageTime = 0;
    pPing[u2Index]->u2MaxTime = 0;
    pPing[u2Index]->u2MinTime = 0;
    pPing[u2Index]->u2Successes = 0;
    pPing[u2Index]->u2Id = u2Ping6Id++;
    pPing[u2Index]->u2StartSeq = 0;
    pPing[u2Index]->u2Seq = 0;
    pPing[u2Index]->i2Count = 0;
    pPing[u2Index]->u4ContextId = IP6_DEFAULT_CONTEXT;

    MEMSET (pPing[u2Index]->au1ZoneId, 0, CFA_CLI_MAX_IF_NAME_LEN);

    for (u1Count = 0; u1Count < MAX_PING6_WIDTH; u1Count++)
    {
        pPing[u2Index]->pktStat[u1Count].i2Seq = -1;
    }

    if (u1Admin == IP6_PING_ENABLE)
    {
        pPing[u2Index]->u1Oper = IP6_PING_IN_PROGRESS;

        Ping6SendEchoReq (pPing[u2Index]);
    }
    else
    {
        pPing[u2Index]->u1Oper = IP6_PING_NOT_IN_PROGRESS;
    }

    return IP6_SUCCESS;

}

/******************************************************************************
 * DESCRIPTION : Set the status of the ping for ping block of given address
 *
 * INPUTS      : Index to Ping Table (u2Index), Ping Value (i4Value)
 *
 * OUTPUTS     : NONE. 
 *
 * RETURNS     : IP6_SUCCESS, if Status is set / IP6_FAILURE otherwise.
 *               
 * NOTES       :
 *****************************************************************************/
INT1                            /* ashis INT4 to INT1 */
Ping6AdminSet (UINT2 u2Index, INT4 i4Value)
{
    UINT1               u1Count = 0;
    tDNSResolvedIpInfo  ResolvedIpInfo;
    INT4                i4RetVal = 0;

    MEMSET (&ResolvedIpInfo, 0, sizeof (tDNSResolvedIpInfo));

    if ((pPing[u2Index]->u1Admin != IP6_PING_INVALID) &&
        (i4Value == IP6_PING_INVALID))
    {
        pPing[u2Index]->u1Admin = (UINT1) i4Value;
        if (pPing[u2Index]->timer.u1Id != 0)
        {
            Ip6TmrStop (VCM_INVALID_VC, PING6_DEST_TIMER_ID,
                        gPing6TimerListId, &(pPing[u2Index]->timer.appTimer));
        }
        return IP6_SUCCESS;
    }

    if (i4Value == IP6_PING_DISABLE)
    {
        if (pPing[u2Index]->u1Admin != IP6_PING_DISABLE)
        {
            pPing[u2Index]->u1Admin = (UINT1) i4Value;
            if (pPing[u2Index]->u1Oper == IP6_PING_IN_PROGRESS)
            {
                Ip6TmrStop (VCM_INVALID_VC, PING6_DEST_TIMER_ID,
                            gPing6TimerListId,
                            &(pPing[u2Index]->timer.appTimer));
                pPing[u2Index]->u1Oper = IP6_PING_NOT_IN_PROGRESS;
            }
        }
        return IP6_SUCCESS;
    }

    if (i4Value == IP6_PING_ENABLE)
    {
        pPing[u2Index]->u1Oper = IP6_PING_IN_PROGRESS;
        pPing[u2Index]->u2SentCount = 0;
        pPing[u2Index]->u2AverageTime = 0;
        pPing[u2Index]->u2MaxTime = 0;
        pPing[u2Index]->u2MinTime = 0;
        pPing[u2Index]->u2Successes = 0;
        pPing[u2Index]->u2Id = 0;
        pPing[u2Index]->u2StartSeq = 0;
        pPing[u2Index]->u2Seq = 0;

        MEMSET (&gZeroIpv6Addr, 0, sizeof (tIp6Addr));
        if (MEMCMP (&pPing[u2Index]->ping6Dst, &gZeroIpv6Addr, 
                    sizeof (tIp6Addr)) == 0)
        {
            /* here 0 denotes (2nd arguement) its a blocking call*/
            IP6_TASK_UNLOCK ();
            i4RetVal = FsUtlIPvXResolveHostName (pPing[u2Index]->au1HostName, 
                                                 DNS_BLOCK,
                                                 &ResolvedIpInfo);
            IP6_TASK_LOCK ();

            if ((i4RetVal == DNS_NOT_RESOLVED) ||
                (ResolvedIpInfo.Resolv6Addr.u1AddrLen == 0))

            {
                return IP6_FAILURE;
            }
            else if (i4RetVal == DNS_IN_PROGRESS)
            {
                return IP6_FAILURE;
            }
            else if (i4RetVal == DNS_CACHE_FULL)
            {
                return IP6_FAILURE;
            }

            MEMCPY (&pPing[u2Index]->ping6Dst, ResolvedIpInfo.Resolv6Addr.au1Addr,
                    ResolvedIpInfo.Resolv6Addr.u1AddrLen);

        }
        for (u1Count = 0; u1Count < MAX_PING6_WIDTH; u1Count++)
        {
            pPing[u2Index]->pktStat[u1Count].i2Seq = -1;
        }
        pPing[u2Index]->u2Id = u2Ping6Id++;
        if (pPing[u2Index]->u1Admin == IP6_PING_DISABLE)
        {
            pPing[u2Index]->u1Admin = (UINT1) i4Value;
        }
        else if (pPing[u2Index]->u1Oper == IP6_PING_IN_PROGRESS)
        {
            Ip6TmrStop (VCM_INVALID_VC, PING6_DEST_TIMER_ID, gPing6TimerListId,
                        &(pPing[u2Index]->timer.appTimer));
        }
        Ping6SendEchoReq (pPing[u2Index]);
        return IP6_SUCCESS;
    }
    return IP6_FAILURE;
}

/******************************************************************************
 * DESCRIPTION : Calculate and return the percentage loss of packets sent
 *                for pinging the given address.
 *
 * INPUTS      :  Ping Index
 *
 * OUTPUTS     : NONE. 
 *
 * RETURNS     : Percentage Loss if entry exists in Ping Table, NULL Otherwise.
 *               
 * NOTES       :
 *****************************************************************************/
INT4
Ping6PercentageLossGet (UINT2 u2Index)
{
    if (pPing[u2Index]->u2SentCount != 0)
    {
        return ((INT4) ((pPing[u2Index]->u2SentCount -
                         pPing[u2Index]->u2Successes)
                        * IP6_HUNDRED / pPing[u2Index]->u2SentCount));
    }
    return 0;
}

/******************************************************************************
 * DESCRIPTION : Get the Address Profile Index  of the given address and 
 *               prefixlength
 *
 * INPUTS      : Index to Address Profile Table 
 *  
 * OUTPUTS     : NONE. 
 *
 * RETURNS     : IP6_SUCCESS, IP6_FAILURE otherwise.
 *               
 * NOTES       :
 *****************************************************************************/

INT4
Ip6GetProfileIndex (INT4 i4Ipv6IfIndex, tIp6Addr pIpv6AddrAddress,
                    INT4 i4Ipv6AddrPrefixLength)
{
    tIp6AddrInfo       *pAddrInfo = NULL;

    if (Ip6AddrType (&pIpv6AddrAddress) == ADDR6_LLOCAL)
    {
        return IP6_SUCCESS;
    }

    pAddrInfo = Ip6AddrTblGetEntry ((UINT2) i4Ipv6IfIndex,
                                    &pIpv6AddrAddress,
                                    (UINT1) i4Ipv6AddrPrefixLength);
    if (pAddrInfo == NULL)
    {
        return IP6_FAILURE;
    }
    return (pAddrInfo->u2Addr6Profile);

}

/******************************************************************************
 * DESCRIPTION : Get the PrefixAddresslen of the given Ip6 address and 
 *               Profile Index. 
 *
 * INPUTS      : Index to  Profile Table 
 *
 * OUTPUTS     : NONE. 
 *
 * RETURNS     : IP6_SUCCESS,IP6_FAILURE otherwise.
 *               
 * NOTES       :
 *****************************************************************************/

INT4
Ip6GetPrefixAddrLen (UINT2 u2AddrProfileIndex, INT4 i4Ipv6IfIndex,
                     tIp6Addr * pIpv6AddrAddress, INT4 *pi4Ipv6AddrPrefixLength)
{
    tIp6AddrInfo       *pAddrInfo = NULL;
    tTMO_SLL_NODE      *pAddrSll = NULL;

    TMO_SLL_Scan (&gIp6GblInfo.apIp6If[i4Ipv6IfIndex]->addr6Ilist,
                  pAddrSll, tTMO_SLL_NODE *)
    {
        pAddrInfo = IP6_ADDR_PTR_FROM_SLL (pAddrSll);
        if (pAddrInfo->u2Addr6Profile == u2AddrProfileIndex)
        {
            break;
        }
    }
    if (pAddrSll == NULL)
    {
        return IP6_FAILURE;
    }

    MEMCPY (pIpv6AddrAddress, &pAddrInfo->ip6Addr, sizeof (tIp6Addr));
    *pi4Ipv6AddrPrefixLength = pAddrInfo->u1PrefLen;
    return IP6_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : Get the AddrPrefixlen of the given Ip6 address  
 *
 * INPUTS      : Ip6Address,Ip6IfIndex 
 *
 * OUTPUTS     : NONE. 
 *
 * RETURNS     : IP6_SUCCESS,IP6_FAILURE otherwise.
 *               
 * NOTES       :
 *****************************************************************************/

INT4
Ip6GetAddrPrefixLen (INT4 i4Ipv6IfIndex, tIp6Addr * pIpv6AddrAddress,
                     INT4 *pi4Ipv6AddrPrefixLength)
{
    tIp6If             *pIf6 = gIp6GblInfo.apIp6If[i4Ipv6IfIndex];
    tIp6AddrInfo       *pCurrAddr = NULL;
    tIp6LlocalInfo     *pCurrLLAddr = NULL;
    tTMO_SLL_NODE      *pSllAddr = NULL;
    INT4                i4PfxLen = 0;

    if (Ip6AddrType (pIpv6AddrAddress) == ADDR6_LLOCAL)
    {
        TMO_SLL_Scan (&pIf6->lla6Ilist, pSllAddr, tTMO_SLL_NODE *)
        {
            pCurrLLAddr = IP6_LLADDR_PTR_FROM_SLL (pSllAddr);
            if (MEMCMP (&pCurrLLAddr->ip6Addr, pIpv6AddrAddress,
                        sizeof (tIp6Addr)) == 0)
            {
                i4PfxLen = IP6_ADDR_MAX_PREFIX;
                break;
            }
        }
    }
    else
    {
        TMO_SLL_Scan (&pIf6->addr6Ilist, pSllAddr, tTMO_SLL_NODE *)
        {
            pCurrAddr = IP6_ADDR_PTR_FROM_SLL (pSllAddr);
            if (MEMCMP (&pCurrAddr->ip6Addr, pIpv6AddrAddress,
                        sizeof (tIp6Addr)) == 0)
            {
                i4PfxLen = pCurrAddr->u1PrefLen;
                break;
            }
        }
    }
    if (pSllAddr == NULL)
    {
        return IP6_FAILURE;
    }

    *pi4Ipv6AddrPrefixLength = i4PfxLen;
    return IP6_SUCCESS;
}

/****************************************************************************
 Function    :  Ip6GetNextNdLanCacheTableEntry
 Input       :  The Indices
                i4IfIndex
                ipv6Addr
                pi4IfIndex
                pipv6Addr

 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
Ip6GetNextNdLanCacheTableEntry (INT4 i4IfIndex, INT4 *pi4IfIndex,
                                tIp6Addr ipv6Addr, tIp6Addr * pipv6Addr)
{
    UINT4               u4TempIndex = 0;
    tIp6If             *pIf6 = NULL;
    tIp6Addr            TempAddr;
    tNd6CacheEntry     *pNd6cEntry = NULL;
    tNd6CacheEntry      Nd6cEntry;

    MEMSET (&TempAddr, 0, IP6_ADDR_SIZE);

    if (i4IfIndex > IP6_MAX_LOGICAL_IF_INDEX)
    {
        return SNMP_FAILURE;
    }

    pIf6 = gIp6GblInfo.apIp6If[i4IfIndex];
    if (pIf6 == NULL)
    {
        u4TempIndex = i4IfIndex + 1;
        IP6_IF_SCAN (u4TempIndex, IP6_MAX_LOGICAL_IF_INDEX)
        {
            if (gIp6GblInfo.apIp6If[u4TempIndex] != NULL)
            {
                pIf6 = gIp6GblInfo.apIp6If[u4TempIndex];
                break;
            }
        }
        if (u4TempIndex > IP6_MAX_LOGICAL_IF_INDEX)
        {
            return SNMP_FAILURE;
        }
        MEMSET (&ipv6Addr, 0, sizeof (tIp6Addr));
    }
    MEMSET (&Nd6cEntry, 0, sizeof (tNd6CacheEntry));
    Nd6cEntry.pIf6 = pIf6;
    Ip6AddrCopy (&Nd6cEntry.addr6, &ipv6Addr);

    pNd6cEntry = RBTreeGetNext (gNd6GblInfo.Nd6CacheTable,
                                (tRBElem *) & Nd6cEntry, NULL);

    if (pNd6cEntry != NULL)
    {
        *pi4IfIndex = pNd6cEntry->pIf6->u4Index;
        Ip6AddrCopy (pipv6Addr, &pNd6cEntry->addr6);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/******************************************************************************
 * DESCRIPTION : Get the unnumnumbered interface entry if it exists for the given index
 *
 * INPUTS      : u4IfIndex
 *
 * OUTPUTS     : NONE.
 *
 * RETURNS     : Pointer to the if_entry on Success. NULL on Failure.
 ******************************************************************************/
PUBLIC INT1
Ip6UtlGetUnnumIfEntry (UINT4 u4IfIndex)
{
    tIp6If             *pIf6Entry = NULL;
    UINT4               u4ArrayIndex = 0;

    for (u4ArrayIndex = 0; u4ArrayIndex < IP6_MAX_LOGICAL_IF_INDEX;
         u4ArrayIndex++)
    {
        pIf6Entry = gIp6GblInfo.apIp6If[u4ArrayIndex];

        if (pIf6Entry == NULL)
        {
            continue;
        }

        if (pIf6Entry->u4UnnumAssocIPv6If == u4IfIndex)
        {
            return OSIX_FAILURE;
        }
    }

    return OSIX_SUCCESS;
}

/***************************** END OF FILE **********************************/
