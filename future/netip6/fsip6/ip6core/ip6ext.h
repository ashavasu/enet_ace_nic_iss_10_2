/*******************************************************************
 *    Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *   
 *    $Id: ip6ext.h,v 1.9 2012/10/10 07:07:26 siva Exp $
 *
 *
 ******************************************************************/
#ifndef _IP6EXT_H
#define _IP6EXT_H
/*---------------------------------------------------------------------------- 
 *    PRINCIPAL AUTHOR             :    Senthil Vadivu
 *
 *    SUBSYSTEM NAME               :    IPv6
 *
 *    MODULE NAME                  :    All Modules
 *
 *    LANGUAGE                     :    C
 *
 *    TARGET ENVIRONMENT           :    UNIX
 *
 *    DATE OF FIRST RELEASE        :    DDD-MM-1996
 *
 *    DESCRIPTION                  :    This file contains extern declarations
 *                                      of IPv6 subsystem.
 *----------------------------------------------------------------------------- 
 */

/*
 * Extern Variable Declarations
 */

extern tTimerListId gPing6TimerListId;

extern tNetIpv6RegTbl gaNetIpv6RegTable[];
extern tNdGblInfo       gNd6GblInfo;
extern tIp6GblInfo      gIp6GblInfo;

extern tNd6RedGlobalInfo gNd6RedGlobalInfo;
extern INT4              gi4Ip6SysLogId;

extern tPing6           *pPing[];
extern UINT4            gu4MaxPing6Dst;
extern UINT2            u2Ping6Id;

extern INT4                i4Ping6TablePoolId;


#endif /* !_IP6EXT_H */
