#ifndef IP6_TUNL_H
#define IP6_TUNL_H

extern INT4 gi4Sockid; 
VOID IptaskIp6Input (VOID); 
/*  Macros for socket calls */
#define TUNL_SOCKET      socket
#define TUNL_SOCK_SEND   sendto
#define TUNL_SOCK_SELECT select
#define TUNL_SOCK_RECV   recvfrom
#define TUNL_SOCK_FCNTL  fcntl
#define TUNL_SOCK_CLOSE  close

#define IP6_TUNL_TASK_NAME  "TUNL"
#define IP6_TUNL_TASK_PRIORITY  50 
#define INVALID_SOCKID  -1

INT1 RegisterWithSocketForV4   PROTO ((VOID));

#define IP6_FOREVER()   for(;;)
#endif /* IP6_TUNL_H */
