/*******************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ip6addr.c,v 1.67 2017/12/19 13:41:54 siva Exp $
 *
 * Description: Cli related functions are handled here.
 *
 *******************************************************************/

#include "ip6inc.h"
/*
 * Private function Prototypes
 */

PRIVATE INT4        Ip6AddrAnycastAddrTableInit (VOID);
PRIVATE INT4 Ip6AddrMACFilterInit PROTO ((VOID));
PRIVATE INT4 Ip6AddrUnicastAddrTableInit PROTO ((VOID));
PRIVATE INT4 Ip6AddrSelPolicyInit PROTO ((VOID));

PRIVATE INT4 Ip6AddrProfileInit PROTO ((VOID));

PRIVATE INT4 Ip6AddrInfoCmp PROTO ((tRBElem *, tRBElem *));
PRIVATE INT4 Ip6AddrInfoMibCmp PROTO ((tRBElem *, tRBElem *));
/*
 * Other function Prototypes
 */

VOID Ip6AddrRandomTimerHandler PROTO ((tTmrAppTimer * pTimerNode, UINT1 u1Id));

VOID Ip6AddrDadTimerHandler PROTO ((tTmrAppTimer * pTimerNode, UINT1 u1Id));

/******************************************************************************
 * DESCRIPTION : Initializes the various address related data structures
 *
 * INPUTS      : None
 *
 * OUTPUTS     : None
 *
 * RETURNS     : None
 *
 * NOTES       :
 ******************************************************************************/

INT4
Ip6AddrInit ()
{
    if ((Ip6AddrProfileInit () == IP6_SUCCESS) &&
        (Ip6AddrMACFilterInit () == IP6_SUCCESS) &&
        (Ip6AddrUnicastAddrTableInit () == IP6_SUCCESS) &&
        (Ip6AddrAnycastAddrTableInit () == IP6_SUCCESS) &&
        (Ip6AddrMACFilterInit () == IP6_SUCCESS) &&
        (Ip6AddrSelPolicyInit () == IP6_SUCCESS))
    {
        return IP6_SUCCESS;
    }

    IP6_GBL_TRC_ARG (IP6_MOD_TRC, CONTROL_PLANE_TRC, IP6_NAME,
                     "IP6ADDR: Ip6AddrInit: Failed!!\n");

    return IP6_FAILURE;
}

/******************************************************************************
 * DESCRIPTION :  Initializes the IPv6 address profile table
 *
 * INPUTS      :  None 
 *                    
 * OUTPUTS     :  None
 *                    
 * RETURNS     :  None
 *
 * NOTES       :
 ******************************************************************************/

PRIVATE INT4
Ip6AddrProfileInit ()
{
    UINT2               u2Count = 0;

    for (u2Count = 0; u2Count < gIp6GblInfo.u4MaxAddrProfileLimit; u2Count++)
    {
        gIp6GblInfo.apIp6AddrProfile[u2Count] =
            (tIp6AddrProfile *) (VOID *) Ip6GetMem (VCM_INVALID_VC,
                                                    gIp6GblInfo.
                                                    i4Ip6AddrProfPoolId);

        if (gIp6GblInfo.apIp6AddrProfile[u2Count] != NULL)
        {
            MEMSET (gIp6GblInfo.apIp6AddrProfile[u2Count], 0,
                    sizeof (tIp6AddrProfile));
            gIp6GblInfo.apIp6AddrProfile[u2Count]->u1RaPrefCnf =
                IP6_ADDR_DEF_RA_PREF_CNF;
            gIp6GblInfo.apIp6AddrProfile[u2Count]->u4ValidLife =
                IP6_ADDR_PROF_DEF_VALID_LIFE;
            gIp6GblInfo.apIp6AddrProfile[u2Count]->u4PrefLife =
                IP6_ADDR_PROF_DEF_PREF_LIFE;
            gIp6GblInfo.apIp6AddrProfile[u2Count]->u2RefCnt = 0;
            gIp6GblInfo.apIp6AddrProfile[u2Count]->u1AdminStatus =
                IP6_ADDR_PROF_INVALID;
        }
        else
        {
            IP6_GBL_TRC_ARG4 (IP6_MOD_TRC,
                              CONTROL_PLANE_TRC, IP6_NAME,
                              "IP6ADDR :Ip6AddrProfileInit: Fail %s mem err: "
                              "Type= %d PoolId= %d Memptr= %p\n",
                              ERROR_FATAL_STR, ERR_MEM_CREATE, NULL, 0);

            return IP6_FAILURE;
        }
    }

    /* The first profile is initialized to default values */

    gIp6GblInfo.apIp6AddrProfile[0]->u1AdminStatus = IP6_ADDR_PROF_VALID;
    gIp6GblInfo.apIp6AddrProfile[0]->u1RaPrefCnf = IP6_ADDR_DEF_RA_PREF_CNF;
    gIp6GblInfo.apIp6AddrProfile[0]->u4ValidLife = IP6_ADDR_PROF_DEF_VALID_LIFE;
    gIp6GblInfo.apIp6AddrProfile[0]->u4PrefLife = IP6_ADDR_PROF_DEF_PREF_LIFE;
    gIp6GblInfo.apIp6AddrProfile[0]->u2RefCnt = 0;

    return IP6_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION :   Initializes the RBtree for unicast addresses
 *
 * INPUTS      :   None
 *                     
 * OUTPUTS     :   None
 *                     
 * RETURNS     :   None
 *
 * NOTES       :
 ******************************************************************************/

PRIVATE INT4
Ip6AddrUnicastAddrTableInit ()
{
    gIp6GblInfo.UnicastAddrTree =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tIp6AddrInfo, unicastRbNode),
                              Ip6AddrInfoCmp);
    if (gIp6GblInfo.UnicastAddrTree == NULL)
    {

        IP6_GBL_TRC_ARG4 (IP6_MOD_TRC, CONTROL_PLANE_TRC, IP6_NAME,
                          "IP6ADDR: Ip6AddrUnicastAddrTableInit : fail %s mem error"
                          ": Type=%d PoolId=%d Memptr=%p\n",
                          ERROR_FATAL_STR, ERR_MEM_CREATE, 0, 0);
        return IP6_FAILURE;
    }

    return IP6_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION :  Initialized global MAC Filter database link list. Also adds
 *                multicast MAC address corresponding to All-nodes multicast
 *                and All-routers multicast address.
 *
 * INPUTS      :  None
 *
 * OUTPUTS     :  None
 *
 * RETURNS     :  None
 ******************************************************************************/
PRIVATE INT4
Ip6AddrMACFilterInit ()
{
    UINT4               u4Index = 0;

    for (u4Index = 0; u4Index < (UINT4) IP6_MAX_FILTER; u4Index++)
    {
        gIp6GblInfo.aIp6MACFilter[u4Index].u4Index = IP6_INVALID_IF_INDEX;
        TMO_SLL_Init (&gIp6GblInfo.aIp6MACFilter[u4Index].MacAddrList);
    }
    return IP6_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION :   Initializes the hash table for anycast addresses
 *
 * INPUTS      :   None
 *                     
 * OUTPUTS     :   None
 *                     
 * RETURNS     :   None
 *
 * NOTES       :
 ******************************************************************************/

PRIVATE INT4
Ip6AddrAnycastAddrTableInit ()
{
    gIp6GblInfo.AnycastAddrTree =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tIp6AddrInfo, anycastRbNode),
                              Ip6AddrInfoCmp);
    if (gIp6GblInfo.AnycastAddrTree == NULL)
    {

        IP6_GBL_TRC_ARG4 (IP6_MOD_TRC, MGMT_TRC, IP6_NAME,
                          "IP6ADDR: Ip6AddrAnycastAddrTableInit: Fail %s mem error"
                          ":Type=%d PoolId=%d Memptr=%p\n",
                          ERROR_FATAL_STR, ERR_MEM_CREATE, 0, 0);
        return IP6_FAILURE;
    }

    return IP6_SUCCESS;
}

/******************************************************************************
 *  DESCRIPTION :   Initializes the policy prefix tree
 *  
 *   
 *  INPUTS      :   None
 *    
 *  OUTPUTS     :   None
 *       
 *  RETURNS     :   None
 *   
 *  NOTES       :
 *******************************************************************************/

PRIVATE INT4
Ip6AddrSelPolicyInit ()
{

    /*Policy table contains five default entries. These entries should be configured initially */
    UINT1               u1Index = 0;

    tIp6Addr            Ip6Addr;

    tIp6AddrSelPolicy  *pNode = NULL;

    UINT4               u4Result = 0;
    /*Five default prefix entries */
    UINT1
         
         
         
         
         
         
         
        aDefEntries[IP6_MAX_DEFAULT_POLICY_ENTRIES][IP6_ADDR_MAX_PREFIX] =
        { {"::1"}, {"::"}, {"2002::"}, {"::"}, {"::ffff:0:0"} };

    /*Precedence value of the default entries */
    UINT4               au4Precedence[IP6_MAX_DEFAULT_POLICY_ENTRIES] =
        { 50, 40, 30, 20, 10 };

    /*Label value of the default entries */
    UINT4               au4Label[IP6_MAX_DEFAULT_POLICY_ENTRIES] =
        { 0, 1, 2, 3, 4 };

    /*Prefix Len of the default entries */
    UINT4               au4PrefixLen[IP6_MAX_DEFAULT_POLICY_ENTRIES] =
        { 128, 0, 16, 96, 96 };

    /*Scope value of the default entries */
    UINT4               au4Scope[IP6_MAX_DEFAULT_POLICY_ENTRIES] =
        { ADDR6_SCOPE_INTLOCAL, ADDR6_SCOPE_LLOCAL,
        ADDR6_SCOPE_GLOBAL, ADDR6_SCOPE_LLOCAL,
        ADDR6_SCOPE_GLOBAL
    };

    /*Default interface index(value of 0) of the default entries */
    UINT4               au4IfIndex[IP6_MAX_DEFAULT_POLICY_ENTRIES] =
        { 0, 0, 0, 0, 0 };

    /*Address  value of the default entries */
    UINT4               au4AddrType[IP6_MAX_DEFAULT_POLICY_ENTRIES] =
        { ADDR6_UNICAST, ADDR6_UNICAST,
        ADDR6_UNICAST, ADDR6_UNICAST, ADDR6_UNICAST
    };

    /*Is the address is an self address or not. The default entries does not come
       this category */
    UINT4               au4IsSelfAddr[IP6_MAX_DEFAULT_POLICY_ENTRIES] =
        { IP6_DEFAULT_POLICY_ENTRY,
        IP6_DEFAULT_POLICY_ENTRY,
        IP6_DEFAULT_POLICY_ENTRY,
        IP6_DEFAULT_POLICY_ENTRY,
        IP6_DEFAULT_POLICY_ENTRY
    };

    /*Is the address is public or not */
    UINT4               au4IsPublicAddr[IP6_MAX_DEFAULT_POLICY_ENTRIES] =
        { OSIX_FALSE, OSIX_FALSE, OSIX_TRUE,
        OSIX_FALSE, OSIX_FALSE
    };

    /*Reachabiltiy value of the default entries */
    UINT4               au4ReachabilityStatus[IP6_MAX_DEFAULT_POLICY_ENTRIES] =
        { IP6_POLICY_ENTRY_REACHABLE,
        IP6_POLICY_ENTRY_REACHABLE, IP6_POLICY_ENTRY_REACHABLE,
        IP6_POLICY_ENTRY_REACHABLE, IP6_POLICY_ENTRY_REACHABLE
    };

    /*Entry status of the default entries (default or configurable */
    UINT4               au4EntryStatus[IP6_MAX_DEFAULT_POLICY_ENTRIES] =
        { IP6_POLICY_STATUS_AUTO,
        IP6_POLICY_STATUS_AUTO,
        IP6_POLICY_STATUS_AUTO,
        IP6_POLICY_STATUS_AUTO,
        IP6_POLICY_STATUS_AUTO
    };

    /*Add the entry in the RB Tree */
    gIp6GblInfo.PolicyPrefixTree =
        RBTreeCreateEmbedded (FSAP_OFFSETOF
                              (tIp6AddrSelPolicy, PolicyPrefixRbNode),
                              Ip6PolicyPrefixInfoCmp);

    /* Check is done for any memory failures */
    if (gIp6GblInfo.PolicyPrefixTree == NULL)
    {
        return IP6_FAILURE;
    }

    /*IP6_MAX_DEFAULT_POLICY_ENTRIES refers to 5 default entries */
    for (u1Index = 0; u1Index < IP6_MAX_DEFAULT_POLICY_ENTRIES; u1Index++)
    {
        MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));
        if (INET_ATON6 (aDefEntries[u1Index], &Ip6Addr) != 0)
        {

            pNode =
                (tIp6AddrSelPolicy *) (VOID *) Ip6GetMem (IP6_DEFAULT_CONTEXT,
                                                          (UINT2) gIp6GblInfo.
                                                          i4Ip6PolicyPrefixListId);
            if (pNode == NULL)
            {
                return IP6_FAILURE;
            }
            /*Assign the parameters of the default entries */
            pNode->u4IfIndex = au4IfIndex[u1Index];;
            Ip6CopyAddrBits (&(pNode->Ip6PolicyPrefix), &Ip6Addr,
                             IP6_ADDR_SIZE_IN_BITS);
            pNode->u1Precedence = (UINT1) au4Precedence[u1Index];
            pNode->u1Label = (UINT1) au4Label[u1Index];
            pNode->u1PrefixLen = (UINT1) au4PrefixLen[u1Index];
            pNode->u1Scope = (UINT1) au4Scope[u1Index];
            pNode->u1AddrType = (UINT1) au4AddrType[u1Index];
            pNode->u1IsSelfAddr = (UINT1) au4IsSelfAddr[u1Index];
            pNode->u1IsPublicAddr = (UINT1) au4IsPublicAddr[u1Index];
            pNode->u1ReachabilityStatus =
                (UINT1) au4ReachabilityStatus[u1Index];
            pNode->u1CreateStatus = (UINT1) au4EntryStatus[u1Index];
            u4Result =
                RBTreeAdd (gIp6GblInfo.PolicyPrefixTree, (tRBElem *) pNode);

        }
    }
    UNUSED_PARAM (u4Result);
    return IP6_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : Determines whether the packet is for us
 *
 * INPUTS      : The type of address(Link-local,multicast,etc),flag telling
 *               whether to check in the link-local address list,the IPv6
 *               address(pDstaddr) and pointer to the interface structure
 *               (pIf6)
 * OUTPUTS     : The state of the address(Tentative,complete) in *pu1Type
 *
 * RETURNS     : IP6_SUCCESS if the packet is for us 
 *               IP6_FAILURE if the packet is not for us
 *
 * NOTES       :
 ******************************************************************************/
INT4
Ip6IsPktToMeInCxt (tIp6Cxt * pIp6Cxt, UINT1 *pu1Type, UINT1 u1LlocalChk,
                   tIp6Addr * pDstaddr, tIp6If * pIf6)
{

    tTMO_SLL_NODE      *p_sll_llocal = NULL;
    tIp6McastInfo      *pMcastInfo = NULL;
    tIp6AddrInfo       *pAddr6Info = NULL;
    tIp6LlocalInfo     *pCurrLlocal = NULL;
    tTMO_SLL_NODE      *pSllInfo = NULL;
    tIp6AddrInfo        Ip6AddrInfo;
    tIp6If              TempIf6;

    if (!(pDstaddr))
    {
        IP6_TRC_ARG2 (pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                      CONTROL_PLANE_TRC, IP6_NAME,
                      "IP6ADDR: Ip6IsPktToMeInCxt: Rcvd address is NULL %s "
                      "gen err: Type = %d\n",
                      ERROR_FATAL_STR, ERR_GEN_NULL_PTR);

        return IP6_FAILURE;
    }

    /* If the address Loop back address packet is destined to this router
     * So return success */
    if (IS_ADDR_LOOPBACK (*pDstaddr))
    {
        return IP6_SUCCESS;
    }

    if ((*pu1Type) == ADDR6_MULTI)
    {

        if (!(pIf6))
        {
            IP6_TRC_ARG2 (pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                          CONTROL_PLANE_TRC, IP6_NAME,
                          "IP6ADDR: Ip6IsPktToMeInCxt, rcvd IF pointer is "
                          " NULL %s gen err: Type = %d n",
                          ERROR_FATAL_STR, ERR_GEN_NULL_PTR);

            return IP6_FAILURE;
        }

        if (IS_SUBNET_ADDR_SOLICITED (*pDstaddr))
        {
            *pu1Type = (UINT1) ADDR6_COMPLETE (pIf6);
            return IP6_SUCCESS;
        }

        TMO_SLL_Scan (&pIf6->mcastIlist, pMcastInfo, tIp6McastInfo *)
        {
            if (Ip6AddrMatch (&(pMcastInfo->ip6Addr), pDstaddr,
                              IP6_ADDR_SIZE_IN_BITS) == TRUE)

            {
                /* Node is listening for the Multi-Cast address over
                 * this interface. */
                *pu1Type = (UINT1) ADDR6_COMPLETE (pIf6);
                return IP6_SUCCESS;
            }
        }
        return IP6_FAILURE;
    }

    else if (*pu1Type == ADDR6_LLOCAL)
    {
        if (!(pIf6))
        {
            IP6_TRC_ARG2 (pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                          CONTROL_PLANE_TRC, IP6_NAME,
                          "IP6ADDR: Ip6IsPktToMeInCxt, rcvd IF pointer is NULL "
                          "%s gen err: Type = %d n",
                          ERROR_FATAL_STR, ERR_GEN_NULL_PTR);

            return IP6_FAILURE;
        }
        if (u1LlocalChk)
        {
            /*
             * search the list of link-local addresses on the interface
             */

            TMO_SLL_Scan (&pIf6->lla6Ilist, p_sll_llocal, tTMO_SLL_NODE *)
            {
                pCurrLlocal = IP6_LLADDR_PTR_FROM_SLL (p_sll_llocal);

                if ((pCurrLlocal->u1Status & ADDR6_UP) &&
                    (!(pCurrLlocal->u1Status & ADDR6_FAILED)))
                {
                    if (MEMCMP (pDstaddr, &pCurrLlocal->ip6Addr,
                                sizeof (tIp6Addr)) == 0)
                    {
                        *pu1Type = pCurrLlocal->u1Status;
                        return IP6_SUCCESS;
                    }
                }
            }
            return IP6_FAILURE;
        }
    }

    else if (*pu1Type == ADDR6_V4_COMPAT)
    {

        /* If the packet is not destined to our router, forward.
         * This check will be done by comparing all our v6 address  
         * with destination address.  check all the address in the 
         * automatic tunnel interface */

        if (!(pIf6))
        {
            IP6_TRC_ARG2 (pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                          CONTROL_PLANE_TRC, IP6_NAME,
                          "IP6ADDR: Ip6IsPktToMeInCxt, rcvd IF pointer is "
                          " NULL %s gen err: Type = %d n",
                          ERROR_FATAL_STR, ERR_GEN_NULL_PTR);

            return IP6_FAILURE;
        }

        TMO_SLL_Scan (&pIf6->addr6Ilist, pSllInfo, tTMO_SLL_NODE *)
        {
            pAddr6Info = IP6_ADDR_PTR_FROM_SLL (pSllInfo);

            if (IS_ADDR_V4_COMPAT (pAddr6Info->ip6Addr))
            {
                if (MEMCMP (&pDstaddr->u4_addr[IP6_THREE],
                            &pAddr6Info->ip6Addr.u4_addr[IP6_THREE],
                            IP6_FOUR) == 0)
                {
                    *pu1Type = pAddr6Info->u1Status;
                    return IP6_SUCCESS;
                }
            }
        }
    }
    else
    {
        /*
         * the address is not multicast and not link-local, check the
         * unicast and anycast (for subnet router anycast) RBTrees for
         * a match
         */

        /* Scan the unicast address RBTree */
        MEMSET (&Ip6AddrInfo, 0, sizeof (tIp6AddrInfo));
        MEMSET (&TempIf6, 0, sizeof (tIp6If));
        Ip6AddrCopy (&Ip6AddrInfo.ip6Addr, pDstaddr);
        Ip6AddrInfo.pIf6 = &TempIf6;
        TempIf6.pIp6Cxt = pIp6Cxt;

        /* As the prefix len of the input dest addr. is not available
         * get the available next entry in the given context with the 
         * given prefix */
        pAddr6Info = RBTreeGet (gIp6GblInfo.UnicastAddrTree,
                                (tRBElem *) & Ip6AddrInfo);

        if (pAddr6Info == NULL)
        {

            pAddr6Info = RBTreeGetNext (gIp6GblInfo.UnicastAddrTree,
                                        (tRBElem *) & Ip6AddrInfo, NULL);
        }
        while (pAddr6Info != NULL)
        {
            /* If the retrieved address info does not match 
             * with the incoming dest address. The address
             * is not configured for the given context */
            if ((pIp6Cxt != pAddr6Info->pIf6->pIp6Cxt) ||
                (Ip6AddrMatch (&pAddr6Info->ip6Addr, pDstaddr,
                               pAddr6Info->u1PrefLen) == FALSE))
            {
                break;
            }

            if (Ip6AddrMatch (&pAddr6Info->ip6Addr, pDstaddr,
                              IP6_ADDR_SIZE_IN_BITS) == TRUE)
            {
                if ((pAddr6Info->u1Status & ADDR6_UP) &&
                    (!(pAddr6Info->u1Status & ADDR6_FAILED)))
                {
                    *pu1Type = pAddr6Info->u1Status;
                    return IP6_SUCCESS;
                }
            }

            if ((pAddr6Info->u1AddrType == ADDR6_ANYCAST) &&
                (Ip6AddrMatch (&pAddr6Info->ip6Addr, pDstaddr,
                               IP6_ADDR_SIZE_IN_BITS) == TRUE))
            {
                *pu1Type = pAddr6Info->u1Status;
                return IP6_SUCCESS;
            }
            /* Retrieve the next address info with the given prefix */
            pAddr6Info = RBTreeGetNext (gIp6GblInfo.UnicastAddrTree,
                                        pAddr6Info, NULL);
        }
    }
    return IP6_FAILURE;
}

/******************************************************************************
 * DESCRIPTION : Get a link-local address that is not tentative
 *
 * INPUTS      : The interface table index (i4Index)
 *
 * OUTPUTS     : None
 *
 * RETURNS     : The link-local address(tIp6Addr) or NULL
 *
 * NOTES       :
 ******************************************************************************/

tIp6Addr           *
Ip6GetLlocalAddr (UINT4 u4Index)
{

    tTMO_SLL_NODE      *pSllInfo = NULL;
    tIp6LlocalInfo     *pInfo = NULL;
    tIp6If             *pIf6 = NULL;

    pIf6 = Ip6ifGetEntry (u4Index);
    if (pIf6 == NULL)
    {
        return NULL;
    }
    TMO_SLL_Scan (&pIf6->lla6Ilist, pSllInfo, tTMO_SLL_NODE *)
    {
        pInfo = IP6_LLADDR_PTR_FROM_SLL (pSllInfo);

        if ((pInfo->u1Status & (UINT1) ADDR6_COMPLETE (pIf6))
            && (pInfo->u1Status & ADDR6_PREFERRED))
        {
            break;
        }
    }

    if (pSllInfo == NULL)
    {
        return NULL;
    }
    return (&pInfo->ip6Addr);
}

/******************************************************************************
 * DESCRIPTION : Get an global unicast address which is not tentative
 *
 * INPUTS      : The interface table index (i4Index) and the destination
 *               IPv6 address (pAddr6)
 *                                                          
 * OUTPUTS     : None                                      
 *                                                          
 * RETURNS     : The global address(tIp6Addr) or NULL
 *
 * NOTES       :
 ******************************************************************************/

tIp6Addr           *
Ip6GetGlobalAddr (UINT4 u4Index, tIp6Addr * pAddr6)
{

    tTMO_SLL_NODE      *pSllInfo = NULL;
    tIp6AddrInfo       *pInfo = NULL;
    tIp6Addr           *pStoredAddr = NULL;
    tIp6If             *pIf6 = NULL;
    tIp6If             *pIfIntf = NULL;
    UINT2               u2Count = 1;

    pIf6 = Ip6ifGetEntry (u4Index);
    if (pIf6 == NULL)
    {
        return NULL;
    }

    TMO_SLL_Scan (&pIf6->addr6Ilist, pSllInfo, tTMO_SLL_NODE *)
    {
        pInfo = IP6_ADDR_PTR_FROM_SLL (pSllInfo);

        if ((pInfo->u1Status & (UINT1) ADDR6_COMPLETE (pIf6)) &&
            (pInfo->u1Status & ADDR6_PREFERRED) &&
            (pInfo->u1AddrType == ADDR6_UNICAST) &&
            (Ip6AddrMatch (&(pInfo->ip6Addr), pAddr6, (INT4) pInfo->u1PrefLen)))
        {
            return &(pInfo->ip6Addr);
        }
        else if ((pInfo->u1Status & (UINT1) ADDR6_COMPLETE (pIf6)) &&
                 (pInfo->u1Status & ADDR6_PREFERRED) &&
                 (pInfo->u1AddrType == ADDR6_UNICAST))
        {
            pStoredAddr = &(pInfo->ip6Addr);
        }

    }

    if (pStoredAddr != NULL)
    {
        return pStoredAddr;
    }

    IP6_IF_SCAN (u2Count, IP6_MAX_LOGICAL_IF_INDEX)
    {
        pIfIntf = Ip6ifGetEntry (u2Count);
        if (pIfIntf == NULL)
        {
            continue;
        }
        TMO_SLL_Scan (&pIfIntf->addr6Ilist, pSllInfo, tTMO_SLL_NODE *)
        {
            pInfo = IP6_ADDR_PTR_FROM_SLL (pSllInfo);

            if ((pInfo->u1Status & (UINT1) ADDR6_COMPLETE (pIfIntf)) &&
                (pInfo->u1Status & ADDR6_PREFERRED) &&
                (pInfo->u1AddrType == ADDR6_UNICAST) &&
                (Ip6AddrMatch (&(pInfo->ip6Addr), pAddr6,
                               (INT4) pInfo->u1PrefLen)) &&
                (pIf6->pIp6Cxt->u4ContextId == pIfIntf->pIp6Cxt->u4ContextId))
            {
                return &(pInfo->ip6Addr);
            }
            else if ((pInfo->u1Status & (UINT1) ADDR6_COMPLETE (pIfIntf)) &&
                     (pInfo->u1Status & ADDR6_PREFERRED) &&
                     (pInfo->u1AddrType == ADDR6_UNICAST) &&
                     (pIf6->pIp6Cxt->u4ContextId ==
                      pIfIntf->pIp6Cxt->u4ContextId))
            {
                pStoredAddr = &(pInfo->ip6Addr);
            }

        }

        if (pStoredAddr != NULL)
        {
            return pStoredAddr;
        }
    }

    return NULL;
}

/******************************************************************************
 * DESCRIPTION : Returns an appropriate source address given the interface on 
 *               which the pkt is to go out and the dest address.
 *
 * INPUTS      : The interface table pointer (pIf6) and the destination
 *               addres(pDst)
 *
 * OUTPUTS     : None
 *
 * RETURNS     / The address (tIp6Addr *) or NULL
 *
 * NOTES       :
 ******************************************************************************/

tIp6Addr           *
Ip6AddrGetSrc (tIp6If * pIf6, tIp6Addr * pDst)
{

    UINT1               u1Type = 0;
    tIp6Addr           *pSrcAddr = NULL;

    if (!(pIf6))
    {
        IP6_GBL_TRC_ARG2 (IP6_MOD_TRC, CONTROL_PLANE_TRC, IP6_NAME,
                          "IP6ADDR: Ip6AddrGetSrc: the IF ptr is NULL %s gen err: "
                          "Type = %d ", ERROR_FATAL_STR, ERR_GEN_NULL_PTR);

        return NULL;
    }

    u1Type = (UINT1) Ip6AddrType (pDst);

    /* Source address for the loopback address is again loopback address */
    if (u1Type == ADDR6_LOOPBACK)
    {
        /* since Src and Dest Addr are same here */
        return pDst;
    }

    else
    {
        IP6_TASK_UNLOCK ();
        pSrcAddr = NetIpv6SelectSrcAddrForDstAddr (pDst, pIf6);
        IP6_TASK_LOCK ();
        if (pSrcAddr == NULL)
        {
            pSrcAddr = Ip6GetGlobalAddr (pIf6->u4Index, pDst);
            if (pSrcAddr == NULL)
            {
                /* No Active Uni-Cast Address is present. Use the Link-Local
                 *              * Address. */
                pSrcAddr = Ip6GetLlocalAddr (pIf6->u4Index);
            }
        }
    }
    return (pSrcAddr);

}

/******************************************************************************
 * DESCRIPTION : Checks whether the address is a neighbour address 
 *
 * INPUTS      : The interface ptr (pIf6) and the IPv6 address (pAddr6)
 *
 * OUTPUTS     : None
 *
 * RETURNS     : IP6_SUCCESS - if the address is a neighbour
 *               IP6_FAILURE - otherwise
 * NOTES       :
 ******************************************************************************/

tIp6AddrInfo       *
Ip6AddrIsNeighbor (tIp6If * pIf6, tIp6Addr * pAddr6)
{

    tTMO_SLL_NODE      *pCurrSll = NULL;
    tIp6AddrInfo       *pCurrAddr = NULL;

    if (!(pIf6))
    {
        IP6_GBL_TRC_ARG2 (IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                          "IP6ADDR: Ip6AddrIsNeighbor: the IF ptr is NULL %s gen err: Type = %d\n",
                          ERROR_FATAL_STR, ERR_GEN_NULL_PTR);
        return NULL;
    }

    if (!(pAddr6))
    {
        IP6_TRC_ARG2 (pIf6->pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                      CONTROL_PLANE_TRC, IP6_NAME,
                      "IP6ADDR: Ip6AddrIsNeighbor: the address is NULL %s gen err: Type = %d\n",
                      ERROR_FATAL_STR, ERR_GEN_NULL_PTR);
        return NULL;
    }

    TMO_SLL_Scan (&pIf6->addr6Ilist, pCurrSll, tTMO_SLL_NODE *)
    {
        pCurrAddr = IP6_ADDR_PTR_FROM_SLL (pCurrSll);

        if ((pCurrAddr->u1Status & (UINT1) ADDR6_COMPLETE (pIf6)) &&
            (Ip6AddrMatch (&(pCurrAddr->ip6Addr), pAddr6,
                           (INT4) pCurrAddr->u1PrefLen)))
        {
            return pCurrAddr;
        }
    }

    return NULL;
}

/******************************************************************************
 * DESCRIPTION : Called to indicate the interface's status to the address
 *               Based on that DAD may be started or stopped for the link
 *               local address. Also if the interface is deleted then
 *               the link-local info is deleted. Assumption is that the status
 *               of link-local address won't be allowed to made DOWN 
 * INPUTS      : IPv6 addr (pAddr) and interface status (u1Status)
 *
 * OUTPUTS     : None
 *
 * RETURNS     : None
 *
 * NOTES       :
 ******************************************************************************/

VOID
Ip6AddrNotifyLla (tIp6LlocalInfo * pAddr, UINT1 u1Status)
{
    if (!(pAddr))
    {
        IP6_GBL_TRC_ARG2 (IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                          "IP6ADDR: Ip6AddrNotifyLla: got NULL addr info ptr %s "
                          "gen err: Type = %d\n",
                          ERROR_FATAL_STR, ERR_GEN_NULL_PTR);
        return;
    }

    IP6_TRC_ARG2 (pAddr->pIf6->pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                  DATA_PATH_TRC, IP6_NAME,
                  "IP6ADDR: Ip6AddrNotifyLla: Status = 0x%X for address = %s\n",
                  u1Status, Ip6PrintAddr (&pAddr->ip6Addr));

    if ((u1Status == IP6_IF_UP) && (pAddr->u1AdminStatus == IP6FWD_ACTIVE))
    {
        pAddr->u1Status = ADDR6_UP;

        /* Add the Solicated multicast address derived out of the
         * Link-Local Address. */
        Ip6AddrCreateSolicitMcast (pAddr->pIf6->u4Index, &pAddr->ip6Addr);

        /* The interface is UP now so start the DAD */
        pAddr->u1Status |= ADDR6_TENTATIVE;

        Ip6AddrDadStart ((UINT1 *) pAddr, ADDR6_LLOCAL);
    }
    else if (u1Status == IP6_IF_DOWN)
    {
        if (pAddr->u1Status & ADDR6_DOWN)
        {
            pAddr->u1Status = ADDR6_DOWN;
            return;
        }

        /* The interface is made DOWN so stop the DAD if running */

        if (pAddr->u1Status & ADDR6_TENTATIVE)
        {
            Ip6AddrDadStatus ((UINT1 *) pAddr, DAD_FAILURE, ADDR6_LLOCAL);
        }
    }
    else if (u1Status == IP6_IF_DELETE)
    {
        /*
         * The interface is DELETED so stop DAD if running
         * and also delete the address structure
         */

        if (pAddr->u1Status & ADDR6_TENTATIVE)
        {
            Ip6AddrDadStatus ((UINT1 *) pAddr, DAD_FAILURE, ADDR6_LLOCAL);
        }
        Ip6AddrDeleteSolicitMcast (pAddr->pIf6->u4Index, &pAddr->ip6Addr);
        Ip6AddrDeleteInfo ((UINT1 *) pAddr, ADDR6_LLOCAL);
    }

    return;
}

/******************************************************************************
 * DESCRIPTION : This routine is called to notify the state of the interface
 *               Depending on the state of the interface either DAD is started
 *               or if it were running it is stopped.Also if the interface
 *               is deleted then the info structure is deleted.This is for
 *               UNICAST ADDRESS
 *
 * INPUTS      : IPv6 addr (pAddr) and the interface status (u1Status)
 *
 * OUTPUTS     : None
 *
 * RETURNS     : None
 *
 * NOTES       :
 ******************************************************************************/

VOID
Ip6AddrNotify (tIp6AddrInfo * pAddr, UINT1 u1Status)
{

    tIp6Addr            tmpAddr;

    MEMSET (&tmpAddr, 0, sizeof (tIp6Addr));

    if (!(pAddr))
    {
        IP6_GBL_TRC_ARG2 (IP6_MOD_TRC, CONTROL_PLANE_TRC, IP6_NAME,
                          "IP6ADDR: Ip6AddrNotify: got NULL addr info ptr %s gen "
                          "err: Type = %d\n", ERROR_FATAL_STR,
                          ERR_GEN_NULL_PTR);
        return;
    }

    IP6_TRC_ARG2 (pAddr->pIf6->pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                  DATA_PATH_TRC, IP6_NAME,
                  "IP6ADDR: Ip6AddrNotify: Status = 0x%X for address = %s\n",
                  u1Status, Ip6PrintAddr (&pAddr->ip6Addr));

    if (u1Status == IP6_IF_UP)
    {
        if (pAddr->u1AdminStatus != IP6FWD_ACTIVE)
        {
            /* Address is not administratively enabled. */
            return;
        }

        pAddr->u1Status = ADDR6_UP;
        pAddr->u1Status |= ADDR6_TENTATIVE;

        /* Add the Solicate Mcast address and corresponding MAC Filter entry */
        Ip6AddrCreateSolicitMcast (pAddr->pIf6->u4Index, &pAddr->ip6Addr);

        Ip6AddrDadStart ((UINT1 *) pAddr, ADDR6_UNICAST);

        return;
    }
    else if (u1Status == IP6_IF_DOWN)
    {                            /* This should be a unreachable code. Pl. confirm */
        /*
         * Interface DOWN indication
         */

        if (pAddr->u1Status & ADDR6_DOWN)
        {
            pAddr->u1Status = ADDR6_DOWN;
            return;
        }

        /* Check whether the DAD already over */
        if (pAddr->u1Status & ADDR6_TENTATIVE)
        {
            Ip6AddrDadStatus ((UINT1 *) pAddr, DAD_FAILURE, ADDR6_UNICAST);
        }

        /* Also delete the Solicate Mcast address */
        Ip6AddrDeleteSolicitMcast (pAddr->pIf6->u4Index, &pAddr->ip6Addr);

        /* Interface gone down. Set the Address Oper Status as down. */
        pAddr->u1Status = ADDR6_DOWN;
    }
    else if (u1Status == IP6_IF_DELETE)
    {
        /*
         * Interface DELETE indication 
         */

        if ((pAddr->u1Status & ADDR6_DOWN) != ADDR6_DOWN)
        {
            /* Check whether the DAD already over */
            if (pAddr->u1Status & ADDR6_TENTATIVE)
            {
                Ip6AddrDadStatus ((UINT1 *) pAddr, DAD_FAILURE, ADDR6_UNICAST);
            }

            /* Delete Address from the Prefix List */
            Ip6RAPrefixDelete (pAddr->pIf6->u4Index, &pAddr->ip6Addr,
                               pAddr->u1PrefLen);

            /* Also delete the Solicate Mcast address */
            Ip6AddrDeleteSolicitMcast (pAddr->pIf6->u4Index, &pAddr->ip6Addr);

        }

        pAddr->u1Status = ADDR6_DOWN;

        /* Remove the Addr Info from Unicast and Anycast RBtrees */
        RBTreeRem (gIp6GblInfo.UnicastAddrTree, pAddr);

        RBTreeRem (gIp6GblInfo.AnycastAddrTree, pAddr);

        Ip6AddrDeleteInfo ((UINT1 *) pAddr, ADDR6_UNICAST);
    }

    return;
}

/******************************************************************************
 * DESCRIPTION :  Memory allocated and ptr returned to the link-local
 *                info which is put in the interface list of 
 *                link-local addresses
 *
 * INPUTS      : Token (pToken),length of the token(u1TokLen),interface
 *               ptr(pIf6)
 *
 * OUTPUTS     : None
 *
 * RETURNS     : The link_local info ptr 
 *
 * NOTES       :
 ******************************************************************************/

tIp6LlocalInfo     *
Ip6AddrCreateLla (UINT1 *pToken, UINT1 u1TokLen, tIp6If * pIf6)
{

    tIp6LlocalInfo     *pAddrInfo = NULL;

    if (!(pToken))
    {
        IP6_GBL_TRC_ARG2 (IP6_MOD_TRC, CONTROL_PLANE_TRC, IP6_NAME,
                          "IP6ADDR:Ip6AddrCreateLla: got a NULL token %s gen err: "
                          "Type = %d\n", ERROR_FATAL_STR, ERR_GEN_NULL_PTR);
        return NULL;
    }

    if (!(pIf6))
    {
        IP6_GBL_TRC_ARG2 (IP6_MOD_TRC, CONTROL_PLANE_TRC, IP6_NAME,
                          "IP6ADDR: Ip6AddrCreateLla: got a NULL IF ptr %s gen "
                          "err: Type = %d\n", ERROR_FATAL_STR,
                          ERR_GEN_NULL_PTR);
        return NULL;
    }

    IP6_TRC_ARG2 (pIf6->pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                  DATA_PATH_TRC, IP6_NAME, "IP6ADDR: Ip6AddrCreateLla: "
                  "Creating LLAddr on IF = %d with token = %s\n",
                  pIf6->u4Index, Ip6PrintIftok (pToken, u1TokLen));

    if (gIp6GblInfo.i4Ip6llocalId == IP6_FAILURE)
    {
        IP6_TRC_ARG (pIf6->pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                     CONTROL_PLANE_TRC, IP6_NAME,
                     "IP6ADDR: i4Ip6llocalId: Link Local Address Init Failed\n");
        return NULL;
    }
    pAddrInfo = (tIp6LlocalInfo *) (VOID *)
        Ip6GetMem (pIf6->pIp6Cxt->u4ContextId,
                   (UINT2) gIp6GblInfo.i4Ip6llocalId);

    if (pAddrInfo == NULL)
    {
        IP6_TRC_ARG1 (pIf6->pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                      CONTROL_PLANE_TRC, IP6_NAME,
                      "IP6ADDR: Ip6AddrCreateLla: unable to get memory from "
                      "pool = %d\n", gIp6GblInfo.i4Ip6llocalId);
        IP6_TRC_ARG4 (pIf6->pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                      MGMT_TRC, IP6_NAME,
                      "%s mem err: Type = %d  PoolId = %d Memptr = %p\n",
                      ERROR_FATAL_STR, ERR_MEM_GET, 0,
                      gIp6GblInfo.i4Ip6llocalId);
        return NULL;
    }

    MEMSET (pAddrInfo, 0, sizeof (tIp6LlocalInfo));

    pAddrInfo->ip6Addr.u1_addr[0] = ADDR6_LLOCAL_PREFIX_1;
    pAddrInfo->ip6Addr.u1_addr[1] = ADDR6_LLOCAL_PREFIX_2;

    u1TokLen = (UINT1) MEM_MAX_BYTES (u1TokLen, IP6_EUI_ADDRESS_LEN);
    MEMCPY (pAddrInfo->ip6Addr.u1_addr + (sizeof (tIp6Addr) - u1TokLen),
            pToken, u1TokLen);

    pAddrInfo->u1Status = ADDR6_DOWN;

    pAddrInfo->pIf6 = pIf6;

    return (pAddrInfo);
}

/******************************************************************************
 * DESCRIPTION : This routine deletes the addr_info structure.Called only
 *               after the DAD is stopped.Also before calling this the
 *               info structure would have been removed from the 
 *               interface list and both the unicast and anycast hash tables
 *
 * INPUTS      : Addr info ptr (pAddrInfo) and the type of addr (u1Type)-
 *               whether LLOCAL or UNICAST
 *
 * OUTPUTS     : None
 *
 * RETURNS     : None
 *
 * NOTES       :
 ******************************************************************************/

VOID
Ip6AddrDeleteInfo (UINT1 *pAddrInfo, UINT1 u1Type)
{
    tIp6Cxt            *pIp6Cxt = NULL;
    UINT2               u2ProfIndx = 0;

    if (!(pAddrInfo))
    {

        IP6_GBL_TRC_ARG2 (IP6_MOD_TRC, CONTROL_PLANE_TRC, IP6_NAME,
                          "IP6ADDR: Ip6AddrDeleteInfo: pAddrInfo pointer is NULL "
                          "%s gen err: Type = %d\n",
                          ERROR_FATAL_STR, ERR_GEN_NULL_PTR);
        return;
    }

    if (u1Type == ADDR6_LLOCAL)
    {
        pIp6Cxt = ((tIp6LlocalInfo *) (VOID *) pAddrInfo)->pIf6->pIp6Cxt;
        if (Ip6RelMem
            (pIp6Cxt->u4ContextId, (UINT2) gIp6GblInfo.i4Ip6llocalId,
             pAddrInfo) == IP6_FAILURE)
        {
            IP6_TRC_ARG1 (pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                          CONTROL_PLANE_TRC, IP6_NAME,
                          "IP6ADDR: Ip6AddrDeleteInfo: failure in releasing to "
                          "pool %d\n", gIp6GblInfo.i4Ip6llocalId);
            IP6_TRC_ARG4 (pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                          MGMT_TRC, IP6_NAME,
                          " %s mem err: Type = %d  PoolId = %d Memptr = %p\n",
                          ERROR_FATAL_STR, ERR_MEM_RELEASE, 0,
                          gIp6GblInfo.i4Ip6llocalId);
        }
    }
    else
    {
        pIp6Cxt = ((tIp6AddrInfo *) (VOID *) pAddrInfo)->pIf6->pIp6Cxt;
        if (((tIp6AddrInfo *) (VOID *) pAddrInfo)->AddrPrefTimer.u1Id != 0)
        {
            Ip6TmrStop (pIp6Cxt->u4ContextId, IP6_PREF_LIFETIME_TIMER_ID,
                        gIp6GblInfo.Ip6TimerListId,
                        &(((tIp6AddrInfo *) (VOID *) pAddrInfo)->AddrPrefTimer.
                          appTimer));
        }

        if (((tIp6AddrInfo *) (VOID *) pAddrInfo)->AddrValidTimer.u1Id != 0)
        {
            Ip6TmrStop (pIp6Cxt->u4ContextId, IP6_VALID_LIFETIME_TIMER_ID,
                        gIp6GblInfo.Ip6TimerListId,
                        &(((tIp6AddrInfo *) (VOID *) pAddrInfo)->AddrValidTimer.
                          appTimer));
        }
        u2ProfIndx = ((tIp6AddrInfo *) (VOID *) pAddrInfo)->u2Addr6Profile;
        if (Ip6RelMem
            (pIp6Cxt->u4ContextId, (UINT2) gIp6GblInfo.i4Ip6unicastId,
             pAddrInfo) == IP6_FAILURE)
        {
            IP6_TRC_ARG1 (pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                          CONTROL_PLANE_TRC, IP6_NAME,
                          "IP6ADDR: Ip6AddrDeleteInfo: failure in releasing to "
                          "pool %d\n", gIp6GblInfo.i4Ip6unicastId);
            IP6_TRC_ARG4 (pIp6Cxt->u4ContextId, IP6_MOD_TRC, MGMT_TRC, IP6_NAME,
                          "%s mem err: Type = %d  PoolId = %d Memptr = %p\n",
                          ERROR_FATAL_STR, ERR_MEM_RELEASE, 0,
                          gIp6GblInfo.i4Ip6unicastId);
        }

        /* Address is Deleted. Decrement the reference count for the
         * PROFILE Index */
        if (u2ProfIndx <
            FsIP6SizingParams[MAX_IP6_ADDR_PROFILES_SIZING_ID].
            u4PreAllocatedUnits)
        {
            IP6_ADDR_PROF_REF_COUNT (u2ProfIndx)--;
        }
    }
}

/******************************************************************************
 * DESCRIPTION : Gets the addr info given the address and the interface pointer
 *               It can return the tIp6LlocalInfo or the tIp6AddrInfo
 * INPUTS      : Interface ptr (pIf6) and the IPv6 addr ptr (pAddr)
 *
 * OUTPUTS     : None
 *
 * RETURNS     : The addr info ptr
 *
 * NOTES       :
 ******************************************************************************/

UINT1              *
Ip6GetAddrInfo (tIp6If * pIf6, tIp6Addr * pAddr)
{
    tTMO_SLL_NODE      *pCurrSll = NULL;
#ifdef HA_WANTED
    tIp6AddrInfo       *pHAAddr = NULL;
#endif

    if (!(pIf6))
    {
        IP6_GBL_TRC_ARG2 (IP6_MOD_TRC, CONTROL_PLANE_TRC, IP6_NAME,
                          "IP6ADDR: Ip6GetAddrInfo: IF pointer is NULL %s gen err: Type = %d\n",
                          ERROR_FATAL_STR, ERR_GEN_NULL_PTR);
        return NULL;
    }

    if (!(pAddr))
    {
        IP6_TRC_ARG2 (pIf6->pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                      CONTROL_PLANE_TRC, IP6_NAME, "IP6ADDR: Ip6GetAddrInfo: "
                      "Addr pointer is NULL %s gen err: Type = %d\n",
                      ERROR_FATAL_STR, ERR_GEN_NULL_PTR);
        return NULL;
    }

    if (IS_ADDR_LLOCAL (*pAddr))
    {
        tIp6LlocalInfo     *pCurrAddr = NULL;

        TMO_SLL_Scan (&pIf6->lla6Ilist, pCurrSll, tTMO_SLL_NODE *)
        {
            pCurrAddr = IP6_LLADDR_PTR_FROM_SLL (pCurrSll);

            if (MEMCMP (pAddr, &pCurrAddr->ip6Addr, sizeof (tIp6Addr)) == 0)
            {
                return ((UINT1 *) pCurrAddr);
            }
        }
    }
    else
    {
        tIp6AddrInfo       *pCurrAddr = NULL;

        TMO_SLL_Scan (&pIf6->addr6Ilist, pCurrSll, tTMO_SLL_NODE *)
        {
            pCurrAddr = IP6_ADDR_PTR_FROM_SLL (pCurrSll);
            if (Ip6AddrMatch (&(pCurrAddr->ip6Addr), pAddr,
                              IP6_ADDR_SIZE_IN_BITS) == TRUE)
            {
                return ((UINT1 *) pCurrAddr);
            }
            if ((pCurrAddr->u1AddrType == ADDR6_ANYCAST) &&
                (Ip6AddrMatch (&(pCurrAddr->ip6Addr), pAddr,
                               pCurrAddr->u1PrefLen) == TRUE))
            {
                return ((UINT1 *) pCurrAddr);
            }
        }

#ifdef HA_WANTED
        TMO_SLL_Scan (&MNHAIlist[pIf6->u4Index], pCurrSll, tTMO_SLL_NODE *)
        {
            pHAAddr = IP6_MN_HOMEADDR_PTR_FROM_SLL (pCurrSll);
            if ((Ip6AddrMatch
                 (&(pHAAddr->ip6Addr), pAddr, IP6_ADDR_SIZE_IN_BITS))
                && (pHAAddr->u2AddrFlag == 1))
            {
                return ((UINT1 *) pHAAddr);
            }
        }
#endif

    }

    return NULL;
}

/******************************************************************************
 * DESCRIPTION : Returns the status of the address 
 * 
 * INPUTS      : Interface ptr (pIf6) and the IPv6 addr ptr (pAddr)
 *
 * OUTPUTS     : None
 *
 * RETURNS     : The addr status VALID, INVALID, UP or DOWN.
 *
 * NOTES       :
 ******************************************************************************/

UINT1
Ip6AddrGetStatus (tIp6If * pIf6, tIp6Addr * pAddr)
{

    if (!(pIf6))
    {
        IP6_GBL_TRC_ARG2 (IP6_MOD_TRC, CONTROL_PLANE_TRC, IP6_NAME,
                          "IP6ADDR: Ip6AddrGetStatus: IF pointer is NULL %s gen err: Type = %d\n",
                          ERROR_FATAL_STR, ERR_GEN_NULL_PTR);
        return ADDR6_INVALID;
    }

    if (!(pAddr))
    {
        IP6_TRC_ARG2 (pIf6->pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                      CONTROL_PLANE_TRC, IP6_NAME,
                      "IP6ADDR: Ip6AddrGetStatus: Addr pointer is NULL %s gen err: Type = %d\n",
                      ERROR_FATAL_STR, ERR_GEN_NULL_PTR);
        return ADDR6_INVALID;
    }

    if (IS_ADDR_LLOCAL (*pAddr))
    {
        tIp6LlocalInfo     *pAddrInfo = NULL;

        pAddrInfo = (tIp6LlocalInfo *) (VOID *) Ip6GetAddrInfo (pIf6, pAddr);

        if (!(pAddrInfo))
        {
            return ADDR6_INVALID;
        }

        return (pAddrInfo->u1Status);
    }
    else
    {
        tIp6AddrInfo       *pAddrInfo = NULL;

        pAddrInfo = (tIp6AddrInfo *) (VOID *) Ip6GetAddrInfo (pIf6, pAddr);

        if (!(pAddrInfo))
        {
            return ADDR6_INVALID;
        }

        return (pAddrInfo->u1Status);
    }
}

/******************************************************************************
 * DESCRIPTION : Returns address type  
 * 
 * INPUTS      : Interface ptr (pIf6) and the IPv6 addr ptr (pAddr)
 *
 * OUTPUTS     : None
 *
 * RETURNS     : as UNICAST or ANYCAST or IP6_FAILURE
 *
 * NOTES       :
 ******************************************************************************/

INT4
Ip6GetUniAddrType (tIp6If * pIf6, tIp6Addr * pAddr)
{

    UINT1               u1Type = 0;
    tIp6AddrInfo       *pAddrInfo = NULL;

    if (!(pIf6))
    {
        IP6_GBL_TRC_ARG2 (IP6_MOD_TRC, CONTROL_PLANE_TRC, IP6_NAME,
                          "IP6ADDR: Ip6GetUniAddrType: IF pointer is NULL %s "
                          "gen err: Type = %d\n",
                          ERROR_FATAL_STR, ERR_GEN_NULL_PTR);
        return IP6_FAILURE;
    }

    if (!(pAddr))
    {
        IP6_TRC_ARG2 (pIf6->pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                      CONTROL_PLANE_TRC, IP6_NAME,
                      "IP6ADDR: Ip6GetUniAddrType: Addr pointer is NULL %s "
                      "gen err: Type = %d\n",
                      ERROR_FATAL_STR, ERR_GEN_NULL_PTR);
        return IP6_FAILURE;
    }

    u1Type = (UINT1) Ip6AddrType (pAddr);

    if (u1Type == ADDR6_LLOCAL)
    {
        return ADDR6_UNICAST;
    }

    if ((u1Type == ADDR6_UNICAST) || (u1Type == ADDR6_V4_COMPAT))
    {
        pAddrInfo = (tIp6AddrInfo *) (VOID *) Ip6GetAddrInfo (pIf6, pAddr);
        if (!pAddrInfo)
        {
            return IP6_FAILURE;
        }

        if (pAddrInfo->u1AddrType == ADDR6_UNICAST)
        {
            return ADDR6_UNICAST;
        }
        else if (pAddrInfo->u1AddrType == ADDR6_ANYCAST)
        {
            return ADDR6_ANYCAST;
        }
        else
        {
            return IP6_FAILURE;
        }
    }
    else
    {
        return IP6_FAILURE;
    }

}

/*
 * Duplicate Address Dectection routines
 */

/******************************************************************************
 * DESCRIPTION : This routine is called when the DAD is to start
 *
 * INPUTS      : The address info structure (pInfo) and the 
 *               type of the address info(u1Type)
 *
 * OUTPUTS     : None
 *
 * RETURNS     : None
 *
 * NOTES       :
 ******************************************************************************/

VOID
Ip6AddrDadStart (UINT1 *pInfo, UINT1 u1Type)
{

    tIp6If             *pIf6 = NULL;
    tIp6Addr           *pAddr = NULL;
    tIp6AddrInfo       *pAddrInfo = NULL;
    tIp6LlocalInfo     *pLlocalInfo = NULL;
    tIp6Addr            Prefix;
#ifdef TUNNEL_WANTED
    tIp6TunlIf          TnlIfEntry;
    tIp6TunlIf         *pTnlIfEntry = NULL;
#endif

    if (!(pInfo))
    {
        IP6_GBL_TRC_ARG2 (IP6_MOD_TRC, CONTROL_PLANE_TRC, IP6_NAME,
                          "IP6ADDR: Ip6AddrDadStart: Addr info pointer is NULL %s "
                          "gen err: Type = %d\n",
                          ERROR_FATAL_STR, ERR_GEN_NULL_PTR);
        return;
    }

    if (u1Type == ADDR6_LLOCAL)
    {
        pLlocalInfo = (tIp6LlocalInfo *) (VOID *) pInfo;

        /* For Virtual addresses if DAD runs, it will fail,
         * So, as per RFC 5798 Section 8.2.2, there is no need to run DAD. */
        if (pLlocalInfo->u1ConfigMethod == IP6_ADDR_VIRTUAL)
        {
            Ip6AddrDadStatus ((UINT1 *) pLlocalInfo, DAD_SUCCESS, u1Type);
            return;
        }

        pIf6 = pLlocalInfo->pIf6;
        pAddr = &pLlocalInfo->ip6Addr;

#ifdef TUNNEL_WANTED
        if ((IP6_IF_TYPE (pIf6) != IP6_ENET_INTERFACE_TYPE) &&
            (IP6_IF_TYPE (pIf6) != IP6_PSEUDO_WIRE_INTERFACE_TYPE) &&
            (IP6_IF_TYPE (pIf6) != IP6_L3VLAN_INTERFACE_TYPE) &&
            (IP6_IF_TYPE (pIf6) != IP6_LAGG_INTERFACE_TYPE) &&
            (!((IP6_IF_TYPE (pIf6) == IP6_TUNNEL_INTERFACE_TYPE) &&
               ((pIf6->pTunlIf->u1TunlType == IPV6_OVER_IPV4_TUNNEL) ||
                (pIf6->pTunlIf->u1TunlType == IPV6_GRE_TUNNEL)))))
#else
        if ((IP6_IF_TYPE (pIf6) != IP6_ENET_INTERFACE_TYPE) &&
            (IP6_IF_TYPE (pIf6) != IP6_PSEUDO_WIRE_INTERFACE_TYPE) &&
            (IP6_IF_TYPE (pIf6) != IP6_L3VLAN_INTERFACE_TYPE) &&
            (IP6_IF_TYPE (pIf6) != IP6_LAGG_INTERFACE_TYPE) &&
            (IP6_IF_TYPE (pIf6) != IP6_L3SUB_INTF_TYPE))
#endif
        {

            pLlocalInfo->u1Status &= (UINT1) ~ADDR6_TENTATIVE;
            pLlocalInfo->u1Status &= (UINT1) ~ADDR6_FAILED;
            pLlocalInfo->u1Status |= (UINT1) ADDR6_COMPLETE (pIf6);
            pLlocalInfo->u1Status |= ADDR6_PREFERRED;

            IP6_TRC_ARG2 (pIf6->pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                          DATA_PATH_TRC, IP6_NAME,
                          "IP6ADDR: Ip6AddrDadStart: Address = %s on IF = %d "
                          "made COMPLETE\n",
                          Ip6PrintAddr (pAddr), pIf6->u4Index);
            Ip6LlAddrDadStatus (pLlocalInfo);
            return;
        }

        if (IP6_IF_DAD_SEND (pLlocalInfo->pIf6) <= 0)
        {
            pLlocalInfo->u1Status &= (UINT1) ~ADDR6_TENTATIVE;
            pLlocalInfo->u1Status &= (UINT1) ~ADDR6_FAILED;
            pLlocalInfo->u1Status |= (UINT1) ADDR6_COMPLETE (pIf6);
            pLlocalInfo->u1Status |= ADDR6_PREFERRED;

            IP6_TRC_ARG2 (pIf6->pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                          DATA_PATH_TRC, IP6_NAME,
                          "IP6ADDR:Ip6AddrDadStart: Address = %s on IF = %d "
                          "made COMPLETE\n",
                          Ip6PrintAddr (pAddr), pIf6->u4Index);
            Ip6LlAddrDadStatus (pLlocalInfo);
            return;
        }

        if (pLlocalInfo->u1Status & (UINT1) ADDR6_COMPLETE (pIf6))
        {
            pLlocalInfo->u1Status &= (UINT1) ~ADDR6_TENTATIVE;
            pLlocalInfo->u1Status &= (UINT1) ~ADDR6_FAILED;
            pLlocalInfo->u1Status |= (UINT1) ADDR6_COMPLETE (pIf6);
            pLlocalInfo->u1Status |= ADDR6_PREFERRED;

            IP6_TRC_ARG2 (pIf6->pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                          DATA_PATH_TRC, IP6_NAME,
                          "IP6ADDR:Ip6AddrDadStart: Address = %s on IF = %d "
                          "made COMPLETE\n",
                          Ip6PrintAddr (pAddr), pIf6->u4Index);
            Ip6LlAddrDadStatus (pLlocalInfo);
            return;
        }
/*  In case of Uni-directional tunnels, we need not perform DAD. If the 
 *  tunnel is a send-only, then we'll not be able to receive a NA. If its
 *  a receive-only, we'll not be able to send NS over it */

#ifdef TUNNEL_WANTED
        if ((IP6_IF_TYPE (pIf6) == IP6_TUNNEL_INTERFACE_TYPE) &&
            ((pIf6->pTunlIf->u1TunlType == IPV6_OVER_IPV4_TUNNEL) ||
             (pIf6->pTunlIf->u1TunlType == IPV6_GRE_TUNNEL)))
        {
            pTnlIfEntry = &TnlIfEntry;
            Ip6GetTunnelParams (pIf6->u4Index, pTnlIfEntry);
            if (pTnlIfEntry->u1TunlFlag == TNL_UNIDIRECTIONAL)
            {
                pLlocalInfo->u1Status &= (UINT1) ~ADDR6_TENTATIVE;
                pLlocalInfo->u1Status &= (UINT1) ~ADDR6_FAILED;
                pLlocalInfo->u1Status |= (UINT1) ADDR6_COMPLETE (pIf6);
                pLlocalInfo->u1Status |= ADDR6_PREFERRED;

                IP6_TRC_ARG2 (pIf6->pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                              DATA_PATH_TRC, IP6_NAME, "IP6ADDR:Ip6AddrDadStart"
                              ": Address = %s on IF = %d made COMPLETE\n",
                              Ip6PrintAddr (pAddr), pIf6->u4Index);
                Ip6LlAddrDadStatus (pLlocalInfo);
                return;
            }
        }
#endif

        /* Initialize the addr_info's dad_sent to MAX DADS that can be sent */
        pLlocalInfo->u2DadSent = IP6_IF_DAD_SEND (pIf6);

        IP6_TRC_ARG3 (pIf6->pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                      DATA_PATH_TRC, IP6_NAME,
                      "IP6ADDR: Ip6AddrDadStart: Starting DAD process for "
                      "Addr=%s IF=%d Attempts=%d\n",
                      Ip6PrintAddr (pAddr), pIf6->u4Index,
                      pLlocalInfo->u2DadSent);

        if ((pIf6->u1SeNDStatus == ND6_SECURE_ENABLE) ||
            (pIf6->u1SeNDStatus == ND6_SECURE_MIXED))
        {
            /*
             * when send is enabled, create a dummy cache entry
             * to store secure parameters. This will be deleted
             * once the duplicate is detected
             */
            if (Nd6CreateCache (pIf6, pAddr, NULL, 0,
                                ND6C_INCOMPLETE, 0) == NULL)
            {
                /* unable to create cache */
                return;
            }
        }

        /* 
         * Send the first DAD Neighbour Solicitation after this random timer
         * expiry
         */

        pLlocalInfo->dadTimer.u1Id = IP6_LLOCAL_RAND_TIMER_ID;
        Ip6TmrStart (pIf6->pIp6Cxt->u4ContextId,
                     IP6_LLOCAL_RAND_TIMER_ID, gIp6GblInfo.Ip6TimerListId,
                     &(pLlocalInfo->dadTimer.appTimer),
                     (Ip6Random (0, MAX_RTR_SOLICITATION_DELAY) + 1));
    }
    else
    {
        pAddrInfo = (tIp6AddrInfo *) (VOID *) pInfo;

        /* For Virtual addresses if DAD runs, it will fail,
         * So, as per RFC 5798 Section 8.2.2, there is no need to run DAD. */
        if (pAddrInfo->u1ConfigMethod == IP6_ADDR_VIRTUAL)
        {
            Ip6AddrDadStatus ((UINT1 *) pAddrInfo, DAD_SUCCESS, u1Type);
            return;
        }

        pIf6 = pAddrInfo->pIf6;
        pAddr = &pAddrInfo->ip6Addr;
        MEMSET (&Prefix, 0, sizeof (tIp6Addr));
        Ip6CopyAddrBits (&Prefix, pAddr, pAddrInfo->u1PrefLen);

#ifdef TUNNEL_WANTED
        if ((IP6_IF_TYPE (pIf6) != IP6_ENET_INTERFACE_TYPE) &&
            (IP6_IF_TYPE (pIf6) != IP6_PSEUDO_WIRE_INTERFACE_TYPE) &&
            (IP6_IF_TYPE (pIf6) != IP6_L3VLAN_INTERFACE_TYPE) &&
            (IP6_IF_TYPE (pIf6) != IP6_LAGG_INTERFACE_TYPE) &&
            (!((IP6_IF_TYPE (pIf6) == IP6_TUNNEL_INTERFACE_TYPE) &&
               ((pIf6->pTunlIf->u1TunlType == IPV6_OVER_IPV4_TUNNEL) ||
                (pIf6->pTunlIf->u1TunlType == IPV6_GRE_TUNNEL)))))
#else
        if ((IP6_IF_TYPE (pIf6) != IP6_ENET_INTERFACE_TYPE) &&
            (IP6_IF_TYPE (pIf6) != IP6_PSEUDO_WIRE_INTERFACE_TYPE) &&
            (IP6_IF_TYPE (pIf6) != IP6_L3VLAN_INTERFACE_TYPE) &&
            (IP6_IF_TYPE (pIf6) != IP6_LAGG_INTERFACE_TYPE) &&
            (IP6_IF_TYPE (pIf6) != IP6_L3SUB_INTF_TYPE))
#endif
        {
            pAddrInfo->u1Status &= (UINT1) ~ADDR6_TENTATIVE;
            pAddrInfo->u1Status &= (UINT1) ~ADDR6_FAILED;
            pAddrInfo->u1Status |= (UINT1) ADDR6_COMPLETE (pIf6);
            pAddrInfo->u1Status |= ADDR6_PREFERRED;

            if (pAddrInfo->u1ConfigMethod != IP6_ADDR_AUTO_SL)
            {
                Ip6LocalRouteAdd (pIf6->u4Index, pAddr, pAddrInfo->u1PrefLen);
            }

            if (Ip6GetRAPrefix (pIf6->u4Index, &Prefix, pAddrInfo->u1PrefLen)
                == NULL)
            {
                Ip6RAPrefixCreate (pIf6->u4Index, pAddr, pAddrInfo->u1PrefLen,
                                   pAddrInfo->u2Addr6Profile, IP6FWD_ACTIVE);
            }

            NetIpv6InvokeAddressChange ((pAddr), pAddrInfo->u1PrefLen,
                                        pAddrInfo->u1AddrType, pIf6->u4Index,
                                        NETIPV6_ADDRESS_ADD);

            IP6_TRC_ARG2 (pIf6->pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                          DATA_PATH_TRC, IP6_NAME,
                          "IP6ADDR: Ip6AddrDadStart: Addr=%s IF=%d made "
                          "COMPLETE\n", Ip6PrintAddr (pAddr), pIf6->u4Index);
            return;
        }

        if (pAddrInfo->u1AddrType == ADDR6_ANYCAST)
        {
            pAddrInfo->u1Status &= (UINT1) ~ADDR6_TENTATIVE;
            pAddrInfo->u1Status &= (UINT1) ~ADDR6_FAILED;
            pAddrInfo->u1Status |= (UINT1) ADDR6_COMPLETE (pIf6);
            pAddrInfo->u1Status |= ADDR6_PREFERRED;

            if (pAddrInfo->u1ConfigMethod != IP6_ADDR_AUTO_SL)
            {
                Ip6LocalRouteAdd (pIf6->u4Index, pAddr, pAddrInfo->u1PrefLen);
            }

            if (Ip6GetRAPrefix (pIf6->u4Index, &Prefix, pAddrInfo->u1PrefLen)
                == NULL)
            {
                Ip6RAPrefixCreate (pIf6->u4Index, pAddr, pAddrInfo->u1PrefLen,
                                   pAddrInfo->u2Addr6Profile, IP6FWD_ACTIVE);
            }

            NetIpv6InvokeAddressChange ((pAddr), pAddrInfo->u1PrefLen,
                                        pAddrInfo->u1AddrType, pIf6->u4Index,
                                        NETIPV6_ADDRESS_ADD);

            IP6_TRC_ARG2 (pIf6->pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                          DATA_PATH_TRC, IP6_NAME,
                          "IP6ADDR: Ip6AddrDadStart: Address = %s IF = %d "
                          "made COMPLETE\n",
                          Ip6PrintAddr (pAddr), pIf6->u4Index);
            return;
        }

/*  In case of Uni-directional tunnels, we need not perform DAD. If the 
 *  tunnel is a send-only, then we'll not be able to receive a NA. If its
 *  a receive-only, we'll not be able to send NS over it */
#ifdef TUNNEL_WANTED
        if ((IP6_IF_TYPE (pIf6) == IP6_TUNNEL_INTERFACE_TYPE) &&
            ((pIf6->pTunlIf->u1TunlType == IPV6_OVER_IPV4_TUNNEL) ||
             (pIf6->pTunlIf->u1TunlType == IPV6_GRE_TUNNEL)))
        {
            pTnlIfEntry = &TnlIfEntry;
            Ip6GetTunnelParams (pIf6->u4Index, pTnlIfEntry);
            if (pTnlIfEntry->u1TunlFlag == TNL_UNIDIRECTIONAL)
            {
                pAddrInfo->u1Status &= (UINT1) ~ADDR6_TENTATIVE;
                pAddrInfo->u1Status &= (UINT1) ~ADDR6_FAILED;
                pAddrInfo->u1Status |= (UINT1) ADDR6_COMPLETE (pIf6);
                pAddrInfo->u1Status |= ADDR6_PREFERRED;

                if (pAddrInfo->u1ConfigMethod != IP6_ADDR_AUTO_SL)
                {
                    Ip6LocalRouteAdd (pIf6->u4Index, pAddr,
                                      pAddrInfo->u1PrefLen);
                }

                if (Ip6GetRAPrefix (pIf6->u4Index, &Prefix,
                                    pAddrInfo->u1PrefLen) == NULL)
                {
                    Ip6RAPrefixCreate (pIf6->u4Index, pAddr,
                                       pAddrInfo->u1PrefLen,
                                       pAddrInfo->u2Addr6Profile,
                                       IP6FWD_ACTIVE);
                }

                NetIpv6InvokeAddressChange ((pAddr), pAddrInfo->u1PrefLen,
                                            pAddrInfo->u1AddrType,
                                            pIf6->u4Index, NETIPV6_ADDRESS_ADD);

                IP6_TRC_ARG2 (pIf6->pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                              DATA_PATH_TRC, IP6_NAME,
                              "IP6ADDR: Ip6AddrDadStart: Addr=%s IF=%d made "
                              "COMPLETE\n", Ip6PrintAddr (pAddr),
                              pIf6->u4Index);
                return;
            }
        }
#endif
        if (IP6_IF_DAD_SEND (pAddrInfo->pIf6) <= 0)
        {
            pAddrInfo->u1Status &= (UINT1) ~ADDR6_TENTATIVE;
            pAddrInfo->u1Status &= (UINT1) ~ADDR6_FAILED;
            pAddrInfo->u1Status |= (UINT1) ADDR6_COMPLETE (pIf6);
            pAddrInfo->u1Status |= ADDR6_PREFERRED;

#ifdef MN_WANTED
            /* new address is configured. Check
             * if the node is in home link. */
            MNPrcsIfAddrExistsOnPrevLink (pAddrInfo, pAddrInfo->pIf6);
#endif

            if (pAddrInfo->u1ConfigMethod != IP6_ADDR_AUTO_SL)
            {
                Ip6LocalRouteAdd (pIf6->u4Index, pAddr, pAddrInfo->u1PrefLen);
            }

            if (Ip6GetRAPrefix (pIf6->u4Index, &Prefix, pAddrInfo->u1PrefLen)
                == NULL)
            {
                Ip6RAPrefixCreate (pIf6->u4Index, pAddr, pAddrInfo->u1PrefLen,
                                   pAddrInfo->u2Addr6Profile, IP6FWD_ACTIVE);
            }

            NetIpv6InvokeAddressChange ((pAddr), pAddrInfo->u1PrefLen,
                                        pAddrInfo->u1AddrType, pIf6->u4Index,
                                        NETIPV6_ADDRESS_ADD);

            IP6_TRC_ARG2 (pIf6->pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                          DATA_PATH_TRC, IP6_NAME,
                          "IP6ADDR: Ip6AddrDadStart:Address = %s on IF = %d "
                          "made COMPLETE\n",
                          Ip6PrintAddr (pAddr), pIf6->u4Index);
            return;
        }

        if (pAddrInfo->u1Status & (UINT1) ADDR6_COMPLETE (pIf6))
        {
            pAddrInfo->u1Status &= (UINT1) ~ADDR6_TENTATIVE;
            pAddrInfo->u1Status &= (UINT1) ~ADDR6_FAILED;
            pAddrInfo->u1Status |= (UINT1) ADDR6_COMPLETE (pIf6);
            pAddrInfo->u1Status |= ADDR6_PREFERRED;

#ifdef MN_WANTED
            /* new address is configured. Check
             * if the node is in home link. */
            MNPrcsIfAddrExistsOnPrevLink (pAddrInfo, pAddrInfo->pIf6);
#endif

            if (pAddrInfo->u1ConfigMethod != IP6_ADDR_AUTO_SL)
            {
                Ip6LocalRouteAdd (pIf6->u4Index, pAddr, pAddrInfo->u1PrefLen);
            }

            if (Ip6GetRAPrefix (pIf6->u4Index, &Prefix, pAddrInfo->u1PrefLen)
                == NULL)
            {
                Ip6RAPrefixCreate (pIf6->u4Index, pAddr, pAddrInfo->u1PrefLen,
                                   pAddrInfo->u2Addr6Profile, IP6FWD_ACTIVE);
            }

            NetIpv6InvokeAddressChange ((pAddr), pAddrInfo->u1PrefLen,
                                        pAddrInfo->u1AddrType, pIf6->u4Index,
                                        NETIPV6_ADDRESS_ADD);

            IP6_TRC_ARG2 (pIf6->pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                          DATA_PATH_TRC, IP6_NAME,
                          "IP6ADDR: Ip6AddrDadStart:Address = %s IF = %d "
                          "made COMPLETE\n",
                          Ip6PrintAddr (pAddr), pIf6->u4Index);
            return;
        }

        /* Initialize the addr_info's dad_sent to MAX DADS that can be sent */
        pAddrInfo->u2DadSent = IP6_IF_DAD_SEND (pIf6);

        IP6_TRC_ARG3 (pIf6->pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                      DATA_PATH_TRC, IP6_NAME,
                      "IP6ADDR: Ip6AddrDadStart: Starting DAD Addr=%s IF=%d "
                      "Total Attempts=%d\n", Ip6PrintAddr (pAddr),
                      pIf6->u4Index, pAddrInfo->u2DadSent);

        if ((pIf6->u1SeNDStatus == ND6_SECURE_ENABLE) ||
            (pIf6->u1SeNDStatus == ND6_SECURE_MIXED))
        {
            /*
             * when send is enabled, create a dummy cache entry
             * to store secure parameters. This will be deleted
             * once the duplicate is detected
             */
            if (Nd6CreateCache (pIf6, pAddr, NULL, 0,
                                ND6C_INCOMPLETE, 0) == NULL)
            {
                /* unable to create cache */
                return;
            }
        }

        /* 
         * Send the first DAD Neighbour Solicitation and after this random timer
         * expiry
         */
        pAddrInfo->dadTimer.u1Id = IP6_UNI_RAND_TIMER_ID;
        Ip6TmrStart (pIf6->pIp6Cxt->u4ContextId,
                     IP6_UNI_RAND_TIMER_ID, gIp6GblInfo.Ip6TimerListId,
                     &(pAddrInfo->dadTimer.appTimer),
                     Ip6Random (0, MAX_RTR_SOLICITATION_DELAY) + 1);
    }
}

/******************************************************************************
 * DESCRIPTION : Handles the address timeouts
 *
 * INPUTS      : The timer id (u1Id) and the timer structure (pTimer)
 *
 * OUTPUTS     : None
 *
 * RETURNS     : None
 *
 * NOTES       :
 ******************************************************************************/
VOID
Ip6AddrTimeout (UINT1 u1Id, tIp6Timer * pTimer)
{

    switch (u1Id)
    {
        case IP6_LLOCAL_RAND_TIMER_ID:
        case IP6_UNI_RAND_TIMER_ID:
            Ip6AddrRandomTimerHandler (&(pTimer->appTimer), u1Id);
            break;

        case IP6_LLOCAL_DAD_TIMER_ID:
        case IP6_UNI_DAD_TIMER_ID:
            Ip6AddrDadTimerHandler (&(pTimer->appTimer), u1Id);
            break;

        default:
            break;
    }

    return;
}

/******************************************************************************
 * DESCRIPTION : Handles the timeout of the random address timer
 *
 * INPUTS      : The timer node (pTimerNode) and the timer id (u1Id)
 *
 * OUTPUTS     : None
 *
 * RETURNS     : None
 *
 * NOTES       :
 ******************************************************************************/

VOID
Ip6AddrRandomTimerHandler (tTmrAppTimer * pTimerNode, UINT1 u1Id)
{

    UINT1               u1Type = 0;

    tIp6If             *pIf6 = NULL;
    tIp6Addr           *pAddr = NULL, src, dst, target;
    tIp6LlocalInfo     *pLlocalInfo = NULL;
    tIp6LlocalInfo     *pTempLlocalInfo = NULL;
    tTMO_SLL_NODE      *pTmpSll = NULL;
    tTMO_SLL_NODE      *pNextTmpSll = NULL;
    UINT1               u1IsAltLLPresent = OSIX_FALSE;
    tIp6AddrInfo       *pAddrInfo = NULL;
    UINT4               u4IfReTransTime = RETRANS_TIMER;

    if (u1Id == IP6_LLOCAL_RAND_TIMER_ID)
    {
        pLlocalInfo = IP6_GET_ADDR_INFO_FROM_TIMER (pTimerNode, tIp6LlocalInfo);
        u1Type = ADDR6_LLOCAL;

        pIf6 = pLlocalInfo->pIf6;
        pAddr = &pLlocalInfo->ip6Addr;

    }
    else
    {
        pAddrInfo = IP6_GET_ADDR_INFO_FROM_TIMER (pTimerNode, tIp6AddrInfo);
        u1Type = ADDR6_UNICAST;

        pIf6 = pAddrInfo->pIf6;
        pAddr = &pAddrInfo->ip6Addr;
    }

    /* Initiate the NS for DAD */

    MEMSET (&src, 0, sizeof (tIp6Addr));
    Ip6AddrCopy (&target, pAddr);
    GET_ADDR_SOLICITED (*pAddr, dst);

    if (Nd6SendNeighSol (NULL, pIf6, &src, &dst, &target) == IP6_FAILURE)
    {
        IP6_TRC_ARG2 (pIf6->pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                      DATA_PATH_TRC, IP6_NAME,
                      "IP6ADDR:Ip6AddrRandomTimerHandler:Could not send NS."
                      "DAD failed addr=%s IF=%d\n",
                      Ip6ErrPrintAddr (ERROR_MINOR, pAddr), pIf6->u4Index);

        if (u1Type == ADDR6_LLOCAL)
        {
            Ip6AddrDadStatus ((UINT1 *) pLlocalInfo, DAD_FAILURE, u1Type);
            /* DAD Fails for the link-local address. Remove all other invalid
             * Link-Local Address. In case if an alternate valid Link-Local
             * Address is present, then continue to use that address.
             * Else bring down all the uni-cast address down and make the
             * interface status as STALLED */
            pTmpSll = TMO_SLL_First (&pIf6->lla6Ilist);
            while (pTmpSll)
            {
                pTempLlocalInfo = IP6_LLADDR_PTR_FROM_SLL (pTmpSll);
                pNextTmpSll = TMO_SLL_Next (&pIf6->lla6Ilist, pTmpSll);

                if (MEMCMP (pLlocalInfo->ip6Addr.u1_addr,
                            pTempLlocalInfo->ip6Addr.u1_addr,
                            IP6_ADDR_SIZE) != 0)
                {
                    if (pTempLlocalInfo->u1Status & ADDR6_FAILED)
                    {
                        /* Invalid Link-Local Address. Delete it */
                        TMO_SLL_Delete (&pIf6->lla6Ilist, pTmpSll);
                        Ip6AddrNotifyLla (pTempLlocalInfo, IP6_IF_DELETE);
                    }
                    else
                    {
                        /* Valid Alternate Address is present. */
                        u1IsAltLLPresent = OSIX_TRUE;
                    }
                }

                pTmpSll = pNextTmpSll;
                pNextTmpSll = NULL;
                pTempLlocalInfo = NULL;
            }

            if (u1IsAltLLPresent == OSIX_FALSE)
            {
                /* No valid Link-Local Address present for this interface. */
                Ip6IfStall (pIf6);
            }
        }
        else
        {
            Ip6AddrDadStatus ((UINT1 *) pAddrInfo, DAD_FAILURE, u1Type);
        }

        return;
    }

    if (u1Type == ADDR6_LLOCAL)
    {
        --pLlocalInfo->u2DadSent;

        IP6_TRC_ARG3 (pIf6->pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                      DATA_PATH_TRC, IP6_NAME,
                      "IP6ADDR:Ip6AddrRandomTimerHandler: DAD retry addr=%s "
                      "IF=%d Attempts left=%d\n",
                      Ip6PrintAddr (pAddr), pIf6->u4Index,
                      pLlocalInfo->u2DadSent);

        pLlocalInfo->dadTimer.u1Id = IP6_LLOCAL_DAD_TIMER_ID;

        u4IfReTransTime = Ip6GetIfReTransTime (pIf6->u4Index);

        Ip6MSecTmrStart (pIf6->pIp6Cxt->u4ContextId,
                         IP6_LLOCAL_DAD_TIMER_ID, gIp6GblInfo.Ip6TimerListId,
                         &(pLlocalInfo->dadTimer.appTimer), u4IfReTransTime);
    }
    else
    {
        --pAddrInfo->u2DadSent;

        IP6_TRC_ARG3 (pIf6->pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                      DATA_PATH_TRC, IP6_NAME,
                      "IP6ADDR:Ip6AddrRandomTimerHandler: DAD retry addr=%s "
                      "IF=%d Attempts left=%d\n",
                      Ip6PrintAddr (pAddr), pIf6->u4Index,
                      pAddrInfo->u2DadSent);

        pAddrInfo->dadTimer.u1Id = IP6_UNI_DAD_TIMER_ID;

        u4IfReTransTime = Ip6GetIfReTransTime (pIf6->u4Index);

        Ip6MSecTmrStart (pIf6->pIp6Cxt->u4ContextId, IP6_UNI_DAD_TIMER_ID,
                         gIp6GblInfo.Ip6TimerListId,
                         &(pAddrInfo->dadTimer.appTimer), u4IfReTransTime);
    }

    return;
}

/******************************************************************************
 * DESCRIPTION : This routine handles the DAD timeouts 
 *
 * INPUTS      : The timer node (pTimer) and the timer id (u1Id)
 *
 * OUTPUTS     : None
 *
 * RETURNS     : None
 *
 * NOTES       :
 ******************************************************************************/

VOID
Ip6AddrDadTimerHandler (tTmrAppTimer * pTimerNode, UINT1 u1Id)
{

    UINT1               u1Type = 0;

    tIp6If             *pIf6 = NULL;
    tIp6Addr            src, dst, target, *pAddr = NULL;
    tIp6LlocalInfo     *pLlocalInfo = NULL;
    tIp6AddrInfo       *pAddrInfo = NULL;
    tIp6LlocalInfo     *pTempLlocalInfo = NULL;
    tTMO_SLL_NODE      *pTmpSll = NULL;
    tTMO_SLL_NODE      *pNextTmpSll = NULL;
    UINT1               u1IsAltLLPresent = OSIX_FALSE;
    UINT4               u4IfReTransTime = RETRANS_TIMER;

    if (u1Id == IP6_LLOCAL_DAD_TIMER_ID)
    {
        pLlocalInfo = IP6_GET_ADDR_INFO_FROM_TIMER (pTimerNode, tIp6LlocalInfo);
        u1Type = ADDR6_LLOCAL;
        pIf6 = pLlocalInfo->pIf6;
        pAddr = &pLlocalInfo->ip6Addr;

    }
    else
    {
        pAddrInfo = IP6_GET_ADDR_INFO_FROM_TIMER (pTimerNode, tIp6AddrInfo);
        u1Type = ADDR6_UNICAST;
        pIf6 = pAddrInfo->pIf6;
        pAddr = &pAddrInfo->ip6Addr;
    }

    /* Initiate the NS for DAD if retries are pending */

    MEMSET (&src, 0, sizeof (tIp6Addr));
    Ip6AddrCopy (&target, pAddr);
    GET_ADDR_SOLICITED (*pAddr, dst);

    if (u1Type == ADDR6_LLOCAL)
    {
        if (pLlocalInfo->u2DadSent <= 0)
        {
            Ip6AddrDadStatus ((UINT1 *) pLlocalInfo, DAD_SUCCESS, u1Type);
            return;
        }

        if (Nd6SendNeighSol (NULL, pIf6, &src, &dst, &target) == IP6_FAILURE)
        {
            IP6_TRC_ARG2 (pIf6->pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                          DATA_PATH_TRC, IP6_NAME,
                          "IP6ADDR:Ip6AddrDadTimerHandler: Could not send NS. "
                          "DAD failed addr = %s IF = %d\n",
                          Ip6ErrPrintAddr (ERROR_MINOR, pAddr), pIf6->u4Index);

            Ip6AddrDadStatus ((UINT1 *) pLlocalInfo, DAD_FAILURE, u1Type);
            /* DAD Fails for the link-local address. Remove all other invalid
             * Link-Local Address. In case if an alternate valid Link-Local
             * Address is present, then continue to use that address.
             * Else bring down all the uni-cast address down and make the
             * interface status as STALLED */
            pTmpSll = TMO_SLL_First (&pIf6->lla6Ilist);
            while (pTmpSll)
            {
                pTempLlocalInfo = IP6_LLADDR_PTR_FROM_SLL (pTmpSll);
                pNextTmpSll = TMO_SLL_Next (&pIf6->lla6Ilist, pTmpSll);

                if (MEMCMP (pLlocalInfo->ip6Addr.u1_addr,
                            pTempLlocalInfo->ip6Addr.u1_addr,
                            IP6_ADDR_SIZE) != 0)
                {
                    if (pTempLlocalInfo->u1Status & ADDR6_FAILED)
                    {
                        /* Invalid Link-Local Address. Delete it */
                        TMO_SLL_Delete (&pIf6->lla6Ilist, pTmpSll);
                        Ip6AddrNotifyLla (pTempLlocalInfo, IP6_IF_DELETE);
                    }
                    else
                    {
                        /* Valid Alternate Address is present. */
                        u1IsAltLLPresent = OSIX_TRUE;
                    }
                }

                pTmpSll = pNextTmpSll;
                pNextTmpSll = NULL;
                pTempLlocalInfo = NULL;
            }

            if (u1IsAltLLPresent == OSIX_FALSE)
            {
                /* No valid Link-Local Address present for this interface. */
                Ip6IfStall (pIf6);
            }
            return;
        }

        --pLlocalInfo->u2DadSent;

        IP6_TRC_ARG3 (pIf6->pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                      DATA_PATH_TRC, IP6_NAME,
                      "IP6ADDR: Ip6AddrDadTimerHandler: DAD retry addr=%s "
                      "IF=%d Attempts left=%d\n",
                      Ip6PrintAddr (pAddr), pIf6->u4Index,
                      pLlocalInfo->u2DadSent);

        pLlocalInfo->dadTimer.u1Id = IP6_LLOCAL_DAD_TIMER_ID;

        u4IfReTransTime = Ip6GetIfReTransTime (pIf6->u4Index);

        Ip6MSecTmrStart (pIf6->pIp6Cxt->u4ContextId, IP6_LLOCAL_DAD_TIMER_ID,
                         gIp6GblInfo.Ip6TimerListId,
                         &(pLlocalInfo->dadTimer.appTimer), u4IfReTransTime);
    }
    else
    {
        if (pAddrInfo->u2DadSent <= 0)
        {
            Ip6AddrDadStatus ((UINT1 *) pAddrInfo, DAD_SUCCESS, u1Type);
            return;
        }

        if (Nd6SendNeighSol (NULL, pIf6, &src, &dst, &target) == IP6_FAILURE)
        {
            IP6_TRC_ARG2 (pIf6->pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                          DATA_PATH_TRC, IP6_NAME,
                          "IP6ADDR: Ip6AddrDadTimerHandler: Could not send NS. "
                          "DAD failed addr=%s IF=%d\n",
                          Ip6ErrPrintAddr (ERROR_MINOR, pAddr), pIf6->u4Index);

            Ip6AddrDadStatus ((UINT1 *) pAddrInfo, DAD_FAILURE, u1Type);
            return;
        }

        --pAddrInfo->u2DadSent;

        IP6_TRC_ARG3 (pIf6->pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                      DATA_PATH_TRC, IP6_NAME,
                      "IP6ADDR: Ip6AddrDadTimerHandler: DAD retry addr=%s "
                      "IF=%d Attempts left=%d\n",
                      Ip6PrintAddr (pAddr), pIf6->u4Index,
                      pAddrInfo->u2DadSent);

        pAddrInfo->dadTimer.u1Id = IP6_UNI_DAD_TIMER_ID;

        u4IfReTransTime = Ip6GetIfReTransTime (pIf6->u4Index);

        Ip6MSecTmrStart (pIf6->pIp6Cxt->u4ContextId, IP6_UNI_DAD_TIMER_ID,
                         gIp6GblInfo.Ip6TimerListId,
                         &(pAddrInfo->dadTimer.appTimer), u4IfReTransTime);
    }

    return;

}

/******************************************************************************
 * DESCRIPTION : This routine is called to indicate the status of DAD.And
 *               accordingly address is made complete or failed
 *
 * INPUTS      : The addr info pointer (pInfo),the status of the DAD(u1Status)
 *               DAD_SUCCESS/DAD_FAILURE and the type of the addr_info (u1Type)
 *
 * OUTPUTS     : None
 *
 * RETURNS     : None
 *
 * NOTES       :
 ******************************************************************************/

VOID
Ip6AddrDadStatus (UINT1 *pInfo, UINT1 u1Status, UINT1 u1Type)
{

    tTMO_SLL_NODE      *pCurrSll = NULL;
    tIp6AddrInfo       *pCurrInfo = NULL;
    tIp6LlocalInfo     *pLlocalInfo = NULL;
    tIp6AddrInfo       *pAddrInfo = NULL;
    tIp6Cxt            *pIp6Cxt = NULL;
    tNd6CacheEntry     *pNd6cEntry = NULL;
    INT4                i4RetVal = 0;
#ifdef HA_WANTED
    tBindEntry         *pBinding;
    INT4                i4RemainingTime = 0;
#endif
    tIp6Addr            Prefix;

    if (!(pInfo))
    {
        IP6_GBL_TRC_ARG (IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                         "IP6ADDR: Ip6AddrDadStatus: the addr info ptr is NULL\n");
        IP6_GBL_TRC_ARG2 (IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                          "%s gen err: Type = %d ", ERROR_FATAL_STR,
                          ERR_GEN_NULL_PTR);
        return;
    }

    if (u1Type == ADDR6_LLOCAL)
    {
        pLlocalInfo = (tIp6LlocalInfo *) (VOID *) pInfo;
        pIp6Cxt = pLlocalInfo->pIf6->pIp6Cxt;

        if (u1Status == DAD_SUCCESS)
        {

            IP6_TRC_ARG2 (pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                          DATA_PATH_TRC, IP6_NAME,
                          "IP6ADDR: Ip6AddrDadStatus: DAD succeeded for "
                          "address = %s on IF = %d\n",
                          Ip6PrintAddr (&pLlocalInfo->ip6Addr),
                          pLlocalInfo->pIf6->u4Index);

            pLlocalInfo->u1Status &= (UINT1) ~ADDR6_TENTATIVE;
            pLlocalInfo->u1Status &= (UINT1) ~ADDR6_FAILED;
            pLlocalInfo->u1Status |= (UINT1) ADDR6_COMPLETE (pLlocalInfo->pIf6);
            pLlocalInfo->u1Status |= ADDR6_PREFERRED;

            Ip6LlAddrDadStatus (pLlocalInfo);

            pLlocalInfo->u2DadSent = 0;

            TMO_SLL_Scan (&pLlocalInfo->pIf6->addr6Ilist, pCurrSll,
                          tTMO_SLL_NODE *)
            {
                pCurrInfo = IP6_ADDR_PTR_FROM_SLL (pCurrSll);
                /* DAD is successful for the Link-Local Address. Check for
                 * uni-cast address status and if necessary initiate
                 * then initiated DAD for those uni-cast address */
                pCurrInfo = IP6_ADDR_PTR_FROM_SLL (pCurrSll);
                if ((pCurrInfo->u1AdminStatus == IP6FWD_ACTIVE) &&
                    ((pCurrInfo->u1Status & ADDR6_UP) != ADDR6_UP))
                {
                    i4RetVal = Ip6AddrUp (pLlocalInfo->pIf6->u4Index,
                                          &pCurrInfo->ip6Addr,
                                          pCurrInfo->u1PrefLen, pCurrInfo);
                }
            }

            if (IP6_TRUE == IF_SEND_ENABLED (pLlocalInfo->pIf6))
            {
                if ((pNd6cEntry = Nd6IsCacheForAddr (pLlocalInfo->pIf6,
                                                     &(pLlocalInfo->ip6Addr)))
                    != NULL)
                {
                    if (Nd6PurgeCache (pNd6cEntry) == IP6_FAILURE)
                    {
                        IP6_TRC_ARG1 (pLlocalInfo->pIf6->pIp6Cxt->u4ContextId,
                                      ND6_MOD_TRC, MGMT_TRC | ALL_FAILURE_TRC,
                                      ND6_NAME,
                                      "ND6:Nd6PurgeCacheOnIface: Purging Cache "
                                      "on IF %d Failed\n",
                                      pLlocalInfo->pIf6->u4Index);
                    }
                }
            }
            /* Also update the Routing Table */
            IP6_TASK_UNLOCK ();
            Rtm6ApiActOnIpv6IfChange (RTM6_SCAN_INTF_UP,
                                      pLlocalInfo->pIf6->u4Index);
            IP6_TASK_LOCK ();
        }
        else if (u1Status == DAD_FAILURE)
        {

            IP6_TRC_ARG2 (pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                          DATA_PATH_TRC, IP6_NAME,
                          "IP6ADDR: Ip6AddrDadStatus: DAD failed for address "
                          "= %s on IF = %d\n",
                          Ip6PrintAddr (&pLlocalInfo->ip6Addr),
                          pLlocalInfo->pIf6->u4Index);

            pLlocalInfo->u1Status &= (UINT1) ~ADDR6_PREFERRED;
            pLlocalInfo->u1Status &=
                (UINT1) ~ADDR6_COMPLETE (pLlocalInfo->pIf6);
            pLlocalInfo->u1Status &= (UINT1) ~ADDR6_TENTATIVE;
            pLlocalInfo->u1Status |= ADDR6_FAILED;

            pLlocalInfo->u2DadSent = 0;

            /* Unlink DAD timer from the timer list */
            if (pLlocalInfo->dadTimer.u1Id != 0)
            {
                Ip6TmrStop (pIp6Cxt->u4ContextId, pLlocalInfo->dadTimer.u1Id,
                            gIp6GblInfo.Ip6TimerListId,
                            &(pLlocalInfo->dadTimer.appTimer));
            }

            if (IP6_TRUE == IF_SEND_ENABLED (pLlocalInfo->pIf6))
            {
                if ((pNd6cEntry = Nd6IsCacheForAddr (pLlocalInfo->pIf6,
                                                     &(pLlocalInfo->ip6Addr)))
                    != NULL)
                {
                    if (Nd6PurgeCache (pNd6cEntry) == IP6_FAILURE)
                    {
                        IP6_TRC_ARG1 (pLlocalInfo->pIf6->pIp6Cxt->u4ContextId,
                                      ND6_MOD_TRC, MGMT_TRC | ALL_FAILURE_TRC,
                                      ND6_NAME,
                                      "ND6:Nd6PurgeCacheOnIface: Purging Cache "
                                      "on IF %d Failed\n",
                                      pLlocalInfo->pIf6->u4Index);
                    }
                }
            }

        }
    }
    else                        /* UNICAST address */
    {
        pAddrInfo = (tIp6AddrInfo *) (VOID *) pInfo;
        MEMSET (&Prefix, 0, sizeof (tIp6Addr));
        Ip6CopyAddrBits (&Prefix, &pAddrInfo->ip6Addr, pAddrInfo->u1PrefLen);

        if (pAddrInfo->pIf6 == NULL)
        {
            return;
        }
        pIp6Cxt = pAddrInfo->pIf6->pIp6Cxt;

        if (u1Status == DAD_SUCCESS)
        {
            if ((pAddrInfo->u1ConfigMethod != IP6_ADDR_AUTO_SL)
#ifdef MIP6_WANTED
                && (pAddrInfo->u2AddrFlag != 1)
#endif
                )
            {
                Ip6LocalRouteAdd (pAddrInfo->pIf6->u4Index,
                                  &pAddrInfo->ip6Addr, pAddrInfo->u1PrefLen);
            }

            if (Ip6GetRAPrefix (pAddrInfo->pIf6->u4Index, &Prefix,
                                pAddrInfo->u1PrefLen) == NULL)
            {
                Ip6RAPrefixCreate (pAddrInfo->pIf6->u4Index,
                                   &pAddrInfo->ip6Addr, pAddrInfo->u1PrefLen,
                                   pAddrInfo->u2Addr6Profile, IP6FWD_ACTIVE);
            }

#ifdef HA_WANTED
            if (pAddrInfo->u2AddrFlag != 1)
            {
#endif

                IP6_TRC_ARG2 (pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                              DATA_PATH_TRC, IP6_NAME,
                              "IP6ADDR: Ip6AddrDadStatus: DAD succeeded for "
                              "address = %s on IF = %d\n",
                              Ip6PrintAddr (&pAddrInfo->ip6Addr),
                              pAddrInfo->pIf6->u4Index);

                pAddrInfo->u1Status &= (UINT1) ~ADDR6_TENTATIVE;
                pAddrInfo->u1Status &= (UINT1) ~ADDR6_FAILED;
                pAddrInfo->u1Status |= (UINT1) ADDR6_COMPLETE (pAddrInfo->pIf6);
                pAddrInfo->u1Status |= ADDR6_PREFERRED;

                pAddrInfo->u2DadSent = 0;
                NetIpv6InvokeAddressChange (&(pAddrInfo->ip6Addr),
                                            (UINT4) pAddrInfo->u1PrefLen,
                                            (UINT4) u1Type,
                                            (pAddrInfo->pIf6->u4Index),
                                            (NETIPV6_ADDRESS_ADD));
                if (IP6_TRUE == IF_SEND_ENABLED (pAddrInfo->pIf6))
                {
                    if ((pNd6cEntry = Nd6IsCacheForAddr (pAddrInfo->pIf6,
                                                         &(pAddrInfo->ip6Addr)))
                        != NULL)
                    {
                        if (Nd6PurgeCache (pNd6cEntry) == IP6_FAILURE)
                        {
                            IP6_TRC_ARG1 (pAddrInfo->pIf6->pIp6Cxt->u4ContextId,
                                          ND6_MOD_TRC,
                                          MGMT_TRC | ALL_FAILURE_TRC, ND6_NAME,
                                          "ND6:Nd6PurgeCacheOnIface: Purging Cache "
                                          "on IF %d Failed\n",
                                          pAddrInfo->pIf6->u4Index);
                        }
                    }
                }
#ifdef HA_WANTED
                HAPrcsAddrUpdate (pAddrInfo->pIf6, (UINT1 *) pAddrInfo,
                                  ADMIN_UP, 0);
            }
            if (pAddrInfo->u2AddrFlag == 1 /*MN_HOMEADDR_FLAG */ )
            {
                if ((Mip6LookupCache (&pAddrInfo->ip6Addr, &pBinding) !=
                     MIP6_FAILURE) &&
                    (TmrGetRemainingTime (gIp6GblInfo.Ip6TimerListId,
                                          (tTmrAppTimer *) & pBinding->
                                          bindTimer.appTimer,
                                          (UINT4 *) &i4RemainingTime) ==
                     TMR_SUCCESS))
                {
                    Mip6SendBindAck (pAddrInfo->pIf6, pAddrInfo->ip6SrcAddr,
                                     pAddrInfo->ip6Addr, i4RemainingTime,
                                     pBinding->u2MaxSeqNum, 0, pBinding);
                }
            }
#endif
        }
        else if (u1Status == DAD_FAILURE)
        {

#ifdef HA_WANTED
            if (pAddrInfo->u2AddrFlag != 1)
            {
#endif
                IP6_TRC_ARG2 (pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                              DATA_PATH_TRC, IP6_NAME,
                              "IP6ADDR: Ip6AddrDadStatus: DAD failed for "
                              "address = %s on IF = %d\n",
                              Ip6PrintAddr (&pAddrInfo->ip6Addr),
                              pAddrInfo->pIf6->u4Index);

                pAddrInfo->u1Status &=
                    (UINT1) ~ADDR6_COMPLETE (pAddrInfo->pIf6);
                pAddrInfo->u1Status &= (UINT1) ~ADDR6_PREFERRED;
                pAddrInfo->u1Status &= (UINT1) ~ADDR6_TENTATIVE;
                pAddrInfo->u1Status |= ADDR6_FAILED;

                pAddrInfo->u2DadSent = 0;

                /* Stop the DAD timer for this address if running */
                if (pAddrInfo->dadTimer.u1Id != 0)
                {
                    Ip6TmrStop (pIp6Cxt->u4ContextId, pAddrInfo->dadTimer.u1Id,
                                gIp6GblInfo.Ip6TimerListId,
                                &(pAddrInfo->dadTimer.appTimer));
                }
#ifdef HA_WANTED
            }

            if (pAddrInfo->u2AddrFlag == 1 /*MN_HOMEADDR_FLAG */ )
            {
                if ((Mip6LookupCache (&pAddrInfo->ip6Addr, &pBinding) !=
                     MIP6_FAILURE) &&
                    (TmrGetRemainingTime (gIp6GblInfo.Ip6TimerListId,
                                          (tTmrAppTimer *) & pBinding->
                                          bindTimer.appTimer,
                                          (UINT4 *) &i4RemainingTime) ==
                     TMR_SUCCESS))
                {
                    Mip6SendBindAck (pAddrInfo->pIf6, pAddrInfo->ip6SrcAddr,
                                     pAddrInfo->ip6Addr,
                                     i4RemainingTime, pBinding->u2MaxSeqNum,
                                     /* BIND_ACK_DAD_FAILED */ 138, pBinding);
                    Ip6TmrStop (pIp6Cxt->u4ContextId, pAddrInfo->dadTimer.u1Id,
                                gIp6GblInfo.Ip6TimerListId,
                                &(pAddrInfo->dadTimer.appTimer));
                }
            }
#endif
            if (IP6_TRUE == IF_SEND_ENABLED (pAddrInfo->pIf6))
            {
                if ((pNd6cEntry = Nd6IsCacheForAddr (pAddrInfo->pIf6,
                                                     &(pAddrInfo->ip6Addr)))
                    != NULL)
                {
                    if (Nd6PurgeCache (pNd6cEntry) == IP6_FAILURE)
                    {
                        IP6_TRC_ARG1 (pAddrInfo->pIf6->pIp6Cxt->u4ContextId,
                                      ND6_MOD_TRC, MGMT_TRC | ALL_FAILURE_TRC,
                                      ND6_NAME,
                                      "ND6:Nd6PurgeCacheOnIface: Purging Cache "
                                      "on IF %d Failed\n",
                                      pAddrInfo->pIf6->u4Index);
                    }
                }
            }
        }
    }
    UNUSED_PARAM (i4RetVal);
    return;
}

/******************************************************************************
 * DESCRIPTION : Called when a Static Link Local Address is configured. 
 *
 * INPUTS      : Status of the addr whether UP/DOWN (u1Status), the logical
 *               interface (i2Index), IPv6 addr ptr (pAddr), Config Method
 *
 * OUTPUTS     : None
 *
 * RETURNS     : Created Address Info or NULL 
 *
 * NOTES       :
 ******************************************************************************/

tIp6LlocalInfo     *
Ip6LlAddrCreate (UINT4 u4Index, tIp6Addr * pAddr, UINT1 u1Status,
                 UINT1 u1ConfigMethod)
{
    tIp6LlocalInfo     *pAddrInfo = NULL;
    tIp6LlocalInfo     *pPrevAddrInfo = NULL;
    tIp6LlocalInfo     *pTempAddrInfo = NULL;
    tTMO_SLL_NODE      *pSllInfo = NULL;
    tIp6If             *pIf6 = (tIp6If *) IP6_INTERFACE (u4Index);
    UINT1               u1Scope = ADDR6_SCOPE_LLOCAL;

    IP6_TRC_ARG3 (pIf6->pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                  DATA_PATH_TRC, IP6_NAME,
                  "IP6ADDR:Ip6LlAddrCreate: addr %s Admin %d IF %d\n",
                  Ip6PrintAddr (pAddr), u1Status, u4Index);

    pAddrInfo = (tIp6LlocalInfo *) (VOID *)
        Ip6GetMem (pIf6->pIp6Cxt->u4ContextId,
                   (UINT2) gIp6GblInfo.i4Ip6llocalId);

    if (pAddrInfo == NULL)
    {
        IP6_TRC_ARG1 (pIf6->pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                      DATA_PATH_TRC, IP6_NAME,
                      "IP6ADDR:Ip6LlAddrCreate: failure in getting mem from "
                      "pool %d\n", gIp6GblInfo.i4Ip6llocalId);
        IP6_TRC_ARG4 (pIf6->pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                      MGMT_TRC, IP6_NAME,
                      "%s mem err: Type = %d  PoolId = %d Memptr = %p\n",
                      ERROR_FATAL, ERR_MEM_GET, 0, gIp6GblInfo.i4Ip6llocalId);
        return NULL;
    }

    MEMSET (pAddrInfo, 0, sizeof (tIp6LlocalInfo));

    MEMCPY (&(pAddrInfo->ip6Addr), pAddr, sizeof (tIp6Addr));

    pAddrInfo->u1Status = ADDR6_DOWN;

    pAddrInfo->pIf6 = pIf6;
    pAddrInfo->u1ConfigMethod = u1ConfigMethod;

    /* Add the address in the address list in ascending order */
    TMO_SLL_Scan (&gIp6GblInfo.apIp6If[u4Index]->lla6Ilist,
                  pSllInfo, tTMO_SLL_NODE *)
    {
        pTempAddrInfo = IP6_LLADDR_PTR_FROM_SLL (pSllInfo);

        if (Ip6IsAddrGreater (pAddr, &pTempAddrInfo->ip6Addr) == IP6_SUCCESS)
        {
            break;
        }
        pPrevAddrInfo = pTempAddrInfo;
    }

    if (pPrevAddrInfo == NULL)
    {
        /* Node to be inserted in first position */
        TMO_SLL_Insert (&gIp6GblInfo.apIp6If[u4Index]->lla6Ilist,
                        NULL, &pAddrInfo->nextLif);
    }
    else
    {
        TMO_SLL_Insert (&gIp6GblInfo.apIp6If[u4Index]->lla6Ilist,
                        &pPrevAddrInfo->nextLif, &pAddrInfo->nextLif);
    }

    if (ND6_GET_NODE_STATUS () != RM_STANDBY)
    {
        /* Create the linklocal zone for this address */
        Ip6ZoneCreateAutoScopeZone (u4Index, u1Scope);
    }

    /* Check whether to do DAD or not */
    if ((u1Status == ADMIN_UP) &&
        (Ip6ifGetIfOper (pAddrInfo->pIf6->u1IfType,
                         pAddrInfo->pIf6->u4Index,
                         pAddrInfo->pIf6->u2CktIndex) == OPER_UP))
    {
        pAddrInfo->u1Status = ADDR6_UP;
        pAddrInfo->u1Status |= ADDR6_TENTATIVE;

        Ip6AddrDadStart ((UINT1 *) pAddrInfo, ADDR6_LLOCAL);

        /* Add the Solicated Multicast Address */
        Ip6AddrCreateSolicitMcast (u4Index, pAddr);
    }

    return pAddrInfo;
}

/******************************************************************************
 * DESCRIPTION : Called when a configured Link Local Address is deleted. 
 *
 * INPUTS      : the logical interface (u4Index), IPv6 addr ptr (pAddr)
 *
 * OUTPUTS     : None
 *4
 * RETURNS     : IP6_SUCCESS/IP6_FAILURE
 *
 * NOTES       :
 ******************************************************************************/

INT1
Ip6LlAddrDelete (UINT4 u4Index, tIp6Addr * pAddr)
{
    tIp6If             *pIf6 = NULL;
    tTMO_SLL_NODE      *pTmpSll = NULL;
    tIp6LlocalInfo     *pLlocalInfo = NULL;
    tTMO_SLL_NODE      *pNextTmpSll = NULL;
    tTMO_SLL_NODE      *pDelTmpSll = NULL;
    UINT1               u1IsAltLLPresent = OSIX_FALSE;

    pIf6 = gIp6GblInfo.apIp6If[u4Index];
    pTmpSll = TMO_SLL_First (&pIf6->lla6Ilist);
    while (pTmpSll)
    {
        pLlocalInfo = IP6_LLADDR_PTR_FROM_SLL (pTmpSll);
        pNextTmpSll = TMO_SLL_Next (&pIf6->lla6Ilist, pTmpSll);

        if (MEMCMP (pLlocalInfo->ip6Addr.u1_addr, pAddr->u1_addr,
                    IP6_ADDR_SIZE) == 0)
        {
            /* Address to be deleted. */
            pDelTmpSll = pTmpSll;
        }
        else if (pLlocalInfo->u1ConfigMethod != IP6_ADDR_VIRTUAL)
        {
            /* This occurs either when the configured Link-Local Address
             * is deleted before made ACTIVE or when there are more than
             * one statically configured link-local address for which
             * DAD has failed. */
            u1IsAltLLPresent = OSIX_TRUE;
        }

        pTmpSll = pNextTmpSll;
        pNextTmpSll = NULL;
        pLlocalInfo = NULL;
    }

    if (pDelTmpSll == NULL)
    {
        /* Matching Address not present. */
        return IP6_FAILURE;
    }

    pLlocalInfo = IP6_LLADDR_PTR_FROM_SLL (pDelTmpSll);
    if (pLlocalInfo->u1ConfigMethod == IP6_ADDR_AUTO_SL)
    {
        /* Trying to delete Link-Local Address created via
         * Stateless Auto-configuration */
        return IP6_FAILURE;
    }

    /* Delete the configured Link-Local Address. Before that send
     * a Final RA/NS in case of router/host for this address */
    if (pLlocalInfo->u1Status & ADDR6_PREFERRED)
    {
        /* Preferred Link-Local Address */
        Nd6ActOnIfstatus (pLlocalInfo->pIf6, IP6_IF_DOWN);

        NetIpv6InvokeAddressChange (&(pLlocalInfo->ip6Addr),
                                    (IP6_ADDR_MAX_PREFIX),
                                    ADDR6_LLOCAL, u4Index,
                                    (NETIPV6_ADDRESS_DELETE));
    }

    if ((pLlocalInfo->u1Status & ADDR6_DOWN) != ADDR6_DOWN)
    {
        Ip6AddrDeleteSolicitMcast (u4Index, &pLlocalInfo->ip6Addr);
    }

    TMO_SLL_Delete (&pIf6->lla6Ilist, pDelTmpSll);

    Ip6AddrNotifyLla (pLlocalInfo, IP6_IF_DELETE);

    if (u1IsAltLLPresent == OSIX_FALSE)
    {
        if (pIf6->u1OperStatus == OPER_DOWN)
        {
            /* Interface Status is Down. No need for any further processing. */
            return IP6_SUCCESS;
        }

        if (pIf6->u1OperStatus == OPER_NOIFIDENTIFIER)
        {
            /* Interface Status is inactive because the deleted Address should
             * have been duplicate address. Now DAD should be reinitiated for
             * the Stateless Auto-Configured Link-Local Address. */
            pIf6->u1OperStatus = OPER_UP;
        }

        /* Re-Initialise the Address Auto-configuration process */
        if (Ip6ifInitialize (pIf6, pIf6->ifaceTok, pIf6->u1TokLen) !=
            IP6_SUCCESS)
        {
            /* Unable to initialise the Link-Local Address for this interface. */
            return IP6_FAILURE;
        }

        if (Ip6ifGetIfOper (pIf6->u1IfType,
                            pIf6->u4Index, pIf6->u2CktIndex) == OPER_UP)
        {
            Ip6AddrNotifyLla (pLlocalInfo, IP6_IF_UP);
        }
    }
    else
    {
        /* Alternate Link-Local Address is present. DAD should have been performed
         * for this address. So update the interface status as per the new Link-Local
         * Address status. */
        pTmpSll = TMO_SLL_First (&pIf6->lla6Ilist);
        if (pTmpSll == NULL)
        {
            return IP6_FAILURE;
        }
        pLlocalInfo = IP6_LLADDR_PTR_FROM_SLL (pTmpSll);

        if (pLlocalInfo->u1Status & ADDR6_FAILED)
        {
            /* DAD FAILED for Alternative address. Make the interface status as
             * STALLED. */
            Ip6IfStall (pIf6);
        }
    }

    return IP6_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : Called when DAD is successful for Link Local Address
 *
 * INPUTS      : IPv6 Llocal Address Info (pLlAddrInfo)
 *
 * OUTPUTS     : None
 *
 * RETURNS     : IP6_SUCCESS/IP6_FAILURE
 *
 * NOTES       :
 ******************************************************************************/

INT1
Ip6LlAddrDadStatus (tIp6LlocalInfo * pLlAddrInfo)
{
    tIp6If             *pIf6 = NULL;
    tIp6LlocalInfo     *pOldLlAddr = NULL;
    tTMO_SLL_NODE      *pTmpSll = NULL;
    tTMO_SLL_NODE      *pNextTmpSll = NULL;

    if (pLlAddrInfo->u1ConfigMethod == IP6_ADDR_STATIC)
    {
        pIf6 = pLlAddrInfo->pIf6;

        /* DAD is successful for Configured new Link-local Address. So
         * in case of router send a Final RA message for the old
         * link-local Address and delete it. In case of Host send
         * a Final NS message for the old link-local address and
         * delete it. */

        pTmpSll = TMO_SLL_First (&pIf6->lla6Ilist);

        while (pTmpSll)
        {
            pOldLlAddr = IP6_LLADDR_PTR_FROM_SLL (pTmpSll);
            pNextTmpSll = TMO_SLL_Next (&pIf6->lla6Ilist, pTmpSll);

            if (MEMCMP (pLlAddrInfo->ip6Addr.u1_addr,
                        pOldLlAddr->ip6Addr.u1_addr, IP6_ADDR_SIZE) != 0)
            {
                if (pOldLlAddr->u1Status & ADDR6_PREFERRED
                    || IP6_ADDR_AUTO_SL == pOldLlAddr->u1ConfigMethod)
                {
                    /* Preferred Link-Local Address */
                    Nd6CeaseRAOnAddr (&(pOldLlAddr->ip6Addr), pOldLlAddr->pIf6);

                    NetIpv6InvokeAddressChange (&(pOldLlAddr->ip6Addr),
                                                (IP6_ADDR_MAX_PREFIX),
                                                ADDR6_LLOCAL,
                                                (pIf6->u4Index),
                                                (NETIPV6_ADDRESS_DELETE));
                }

                Ip6AddrDeleteSolicitMcast (pIf6->u4Index, &pOldLlAddr->ip6Addr);
                TMO_SLL_Delete (&pIf6->lla6Ilist, pTmpSll);

                Ip6AddrNotifyLla (pOldLlAddr, IP6_IF_DELETE);
            }

            /* Process Next Address */
            pTmpSll = pNextTmpSll;
            pNextTmpSll = NULL;
            pOldLlAddr = NULL;
        }
    }

    /* Now advertise RA/NS for the new link-local address in case of
     * router/host respectively. */
    Nd6ActOnIfstatus (pLlAddrInfo->pIf6, IP6_IF_UP);

    NetIpv6InvokeAddressChange (&(pLlAddrInfo->ip6Addr),
                                (IP6_ADDR_MAX_PREFIX),
                                ADDR6_LLOCAL,
                                (pLlAddrInfo->pIf6->u4Index),
                                (NETIPV6_ADDRESS_ADD));
    return IP6_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : Called when an unicast/anycast address is to be created 
 *
 * INPUTS      : Status of the addr whether UP/DOWN (u1Status),type of
 *               address whether UNICAST/ANYCAST (u1Type),prefixlength
 *               (u1Prefixlen),the logical interface (i2Index),IPv6 
 *               addr ptr (pAddr),the index to profile table(u2ProfIndex),
 *               the configuration method (u1ConfigMethod)
 *
 * OUTPUTS     : None
 *
 * RETURNS     : Created Address Info or NULL 
 *
 * NOTES       :
 ******************************************************************************/

tIp6AddrInfo       *
Ip6AddrCreate (UINT4 u4Index, tIp6Addr * pAddr, UINT1 u1Prefixlen,
               UINT1 u1Status, UINT1 u1Type, UINT2 u2ProfIndex,
               UINT1 u1ConfigMethod)
{
    tIp6AddrInfo       *pAddrInfo = NULL;
    tIp6AddrInfo       *pPrevAddrInfo = NULL;
    tIp6AddrInfo       *pTempAddrInfo = NULL;
    tTMO_SLL_NODE      *pSllInfo = NULL;
    tIp6If             *pIf6 = (tIp6If *) IP6_INTERFACE (u4Index);
    UINT4               u4Result = 0;
    UINT1               u1Scope = ADDR6_SCOPE_GLOBAL;    /*RFC 4007 scope to be created */

    IP6_TRC_ARG6 (pIf6->pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                  DATA_PATH_TRC, IP6_NAME,
                  "IP6ADDR:Ip6AddrCreate: addr %s Pfxlen %d Admin %d Type %d "
                  "Prof %d IF %d\n",
                  Ip6PrintAddr (pAddr), u1Prefixlen, u1Status, u1Type,
                  u2ProfIndex, u4Index);

    pAddrInfo =
        (tIp6AddrInfo *) (VOID *) Ip6GetMem (pIf6->pIp6Cxt->u4ContextId,
                                             (UINT2) gIp6GblInfo.
                                             i4Ip6unicastId);

    if (pAddrInfo == NULL)
    {
        IP6_TRC_ARG1 (pIf6->pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                      DATA_PATH_TRC, IP6_NAME,
                      "IP6ADDR:Ip6AddrCreate: failure in getting mem from "
                      "pool %d\n", gIp6GblInfo.i4Ip6unicastId);
        IP6_TRC_ARG4 (pIf6->pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                      MGMT_TRC, IP6_NAME,
                      "%s mem err: Type = %d  PoolId = %d Memptr = %p\n",
                      ERROR_FATAL, ERR_MEM_GET, 0, gIp6GblInfo.i4Ip6unicastId);
        return NULL;
    }

    MEMSET (pAddrInfo, 0, sizeof (tIp6AddrInfo));

    MEMCPY (&(pAddrInfo->ip6Addr), pAddr, sizeof (tIp6Addr));

    pAddrInfo->u1Status = ADDR6_DOWN;

    pAddrInfo->pIf6 = pIf6;

    if (u1Type == IP6_ADDR_TYPE_ANYCAST)
    {
        pAddrInfo->u1AddrType = ADDR6_ANYCAST;
    }
    else if (u1Type == ADDR6_V4_COMPAT)
    {
        pAddrInfo->u1AddrType = ADDR6_UNICAST;
    }
    else
    {
        pAddrInfo->u1AddrType = ADDR6_UNICAST;
    }

    pAddrInfo->u2Addr6Profile = u2ProfIndex;
    pAddrInfo->u1ProfileIndex = (UINT1) u2ProfIndex;
    pAddrInfo->u1PrefLen = u1Prefixlen;
    pAddrInfo->u1ConfigMethod = u1ConfigMethod;
    pAddrInfo->u1AddrScope = Ip6GetAddrScope (pAddr);

    /* Increment the Address Profile's Ref Count */
    IP6_ADDR_PROF_REF_COUNT (u2ProfIndex)++;
    /*
     * Add the address to the Unicast and Anycast RBTrees as well as the
     * list of addresses on the interface
     */

    u4Result = RBTreeAdd (gIp6GblInfo.UnicastAddrTree, (tRBElem *) pAddrInfo);

    u4Result = RBTreeAdd (gIp6GblInfo.AnycastAddrTree, (tRBElem *) pAddrInfo);

    /* Add the address in the address list in ascending order */
    TMO_SLL_Scan (&gIp6GblInfo.apIp6If[u4Index]->addr6Ilist,
                  pSllInfo, tTMO_SLL_NODE *)
    {
        pTempAddrInfo = IP6_ADDR_PTR_FROM_SLL (pSllInfo);

        if (Ip6IsAddrGreater (pAddr, &pTempAddrInfo->ip6Addr) == SUCCESS)
        {
            break;
        }
        /* If the address matches, address having smaller
         * prefix is smaller */
        if ((Ip6AddrMatch
             (pAddr, &pTempAddrInfo->ip6Addr, IP6_ADDR_SIZE_IN_BITS) == TRUE)
            && (u1Prefixlen < pTempAddrInfo->u1PrefLen))
        {
            break;
        }
        pPrevAddrInfo = pTempAddrInfo;
    }

    if (pPrevAddrInfo == NULL)
    {
        /* Node to be inserted in first position */
        TMO_SLL_Insert (&gIp6GblInfo.apIp6If[u4Index]->addr6Ilist,
                        NULL, &pAddrInfo->nextAif);
    }
    else
    {
        TMO_SLL_Insert (&gIp6GblInfo.apIp6If[u4Index]->addr6Ilist,
                        &pPrevAddrInfo->nextAif, &pAddrInfo->nextAif);
    }

    /* RFC Create global scope-zone for this address if the global zone
       doesnot exist already create it and map this interface to that zone */
    Ip6ZoneCreateAutoScopeZone (u4Index, u1Scope);

    if (u1Type == ADDR6_V4_COMPAT)
    {
        /* No need to perform DAD for V4_COMPAT address .Add the Solicated
         * Multicast Address for this V4 COMPAT Address and Update the
         * MAC Filter Table. */
        if (u1Status == ADMIN_UP)
        {
            pAddrInfo->u1Status = ADDR6_UP;

            Ip6AddrCreateSolicitMcast (u4Index, pAddr);
        }
        return pAddrInfo;
    }

    /* Check whether to do DAD or not */
    if ((u1Status == ADMIN_UP) &&
        (pAddrInfo->pIf6->u1OperStatus == OPER_UP) &&
        (Ip6ifGetIfOper (pAddrInfo->pIf6->u1IfType,
                         pAddrInfo->pIf6->u4Index,
                         pAddrInfo->pIf6->u2CktIndex) == OPER_UP))
    {
        pAddrInfo->u1Status = ADDR6_UP;
        pAddrInfo->u1Status &= (UINT1) ~ADDR6_COMPLETE (pIf6);
        pAddrInfo->u1Status &= (UINT1) ~ADDR6_FAILED;
        pAddrInfo->u1Status |= ADDR6_TENTATIVE;

        Ip6AddrDadStart ((UINT1 *) pAddrInfo, ADDR6_UNICAST);

        /* Add the Solicated Multicast Address */
        Ip6AddrCreateSolicitMcast (u4Index, pAddr);
    }
    UNUSED_PARAM (u4Result);
    return pAddrInfo;
}

/******************************************************************************
 * DESCRIPTION : Called when an address is to be deleted.
 *
 * INPUTS      : The logical interface (i2Index),IPv6 addr ptr (pAddr),
 *               Prefixlength (u1Prefixlen),the addr info ptr (pCurrInfo)
 *
 * OUTPUTS     : None
 *
 * RETURNS     : None
 *
 * NOTES       : 
 *****************************************************************************/

INT1
Ip6AddrDelete (UINT4 u4Index, tIp6Addr * pAddr, UINT1 u1Prefixlen,
               UINT1 u1RtDelStatus)
{
    tIp6AddrInfo       *pCurrInfo = NULL;
    tIp6AddrInfo       *pOldInfo = NULL;
    tIp6IfZoneMapInfo  *pIp6IfZoneMapInfo = NULL;
    UINT4               u4ContextId = 0;
    UINT1               u1Type = 0;
    INT4                i4ZoneId = 0;
    UINT1               u1Scope = ADDR6_SCOPE_GLOBAL;    /*RFC 4007 scope to be deleted */

    if (Ip6ifEntryExists (u4Index) == IP6_FAILURE)
    {
        u4ContextId = VCM_INVALID_VC;
    }
    else
    {
        Ip6GetCxtId (u4Index, &u4ContextId);
    }
    IP6_TRC_ARG3 (u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                  "IP6ADDR: Ip6AddrDelete: Deleting address = %s Pfxlen = %d "
                  "IF = %d\n", Ip6PrintAddr (pAddr), u1Prefixlen, u4Index);

    u1Type = (UINT1) Ip6AddrType (pAddr);

    if (u1Type == ADDR6_MULTI)
    {
        return (Ip6AddrDeleteMcast (u4Index, pAddr));
    }

    if (u1Type == ADDR6_UNSPECIFIED)
    {
        IP6_TRC_ARG (u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                     "IP6ADDR:Ip6AddrDelete: Cannot delete unspecified "
                     "address\n");
        return IP6_FAILURE;
    }
    else if ((u1Type == ADDR6_LLOCAL) && (u4Index != 0))
    {
        IP6_TRC_ARG (u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                     "IP6ADDR:Ip6AddrDelete: Cannot delete link-local "
                     "address\n");
        return IP6_FAILURE;
    }
    else if (u4Index == 0)
    {
        IP6_TRC_ARG (u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                     "IP6ADDR:Ip6AddrDelete: Invalid Interface Index\n");
        return IP6_FAILURE;
    }

    if ((pCurrInfo = Ip6AddrTblGetEntry (u4Index, pAddr, u1Prefixlen)) == NULL)
    {
        return IP6_FAILURE;
    }

    /* 
     * Indicate to RTM6 module to delete the local route corresponding to
     * this address from the routing table
     */
    if ((u1RtDelStatus) && (pCurrInfo->u1ConfigMethod != IP6_ADDR_AUTO_SL)
        && (pCurrInfo->u1Status & ADDR6_COMPLETE (pCurrInfo->pIf6)))
    {
        Ip6DelLocalRoute (&pCurrInfo->ip6Addr, u1Prefixlen, u4Index);
    }

    /*
     * if DAD is running for the address, stop it by indicating failure
     */
    if (pCurrInfo->u1Status & ADDR6_TENTATIVE)
    {
        Ip6AddrDadStatus ((UINT1 *) pCurrInfo, DAD_FAILURE, ADDR6_UNICAST);
    }

    /* Delele Address to the Prefix List */
    Ip6RAPrefixDelete (u4Index, pAddr, u1Prefixlen);

    /* Delete the neighbor cache for corresponding IP address */
    Nd6PurgeCacheForIP (u4Index, pAddr, u1Prefixlen);

    Ip6AddrDeleteSolicitMcast (u4Index, pAddr);

    if (pCurrInfo->u1Status & ADDR6_PREFERRED)
    {
        NetIpv6InvokeAddressChange (pAddr,
                                    (UINT4) u1Prefixlen,
                                    (UINT4) u1Type,
                                    (gIp6GblInfo.apIp6If[u4Index]->u4Index),
                                    (NETIPV6_ADDRESS_DELETE));
    }

#if defined(CLI_LNXIP_WANTED) || defined(SNMP_LINXIP_WANTED)
    /* Any change in ipv6 address for OOB interface is indicated to cfa
     * when node state is active. This is because when ipv6 addr change
     * happens for OOB, the linux programming will be done in cfa. */
    if ((CfaIsMgmtPort (u4Index) == TRUE) && (u1Type == ADDR6_UNICAST))
    {
        CfaSetIfIp6Addr (u4Index, pAddr, (INT4) u1Prefixlen, OSIX_FALSE);
    }
#endif

    /*
     * Delete the address from the list of addresses on the interface as
     * well as the Hash tables
     */

    pOldInfo = pCurrInfo;

    RBTreeRem (gIp6GblInfo.UnicastAddrTree, pCurrInfo);

    RBTreeRem (gIp6GblInfo.AnycastAddrTree, pCurrInfo);

    TMO_SLL_Delete (&pOldInfo->pIf6->addr6Ilist, &pOldInfo->nextAif);

    /* RFC-4007 If this is the last global address to be deleted on this interface
       delete the corresponding scope-zone also */
    if (TMO_SLL_Count (&pOldInfo->pIf6->addr6Ilist) == 0)
    {
        pIp6IfZoneMapInfo = Ip6ZoneGetIfScopeZoneEntry (u4Index, u1Scope);

        if (NULL != pIp6IfZoneMapInfo)
        {
            i4ZoneId = pIp6IfZoneMapInfo->i4ZoneId;
            Ip6ScopeZoneDelete (u4Index, u1Scope, i4ZoneId);
        }
    }
    /* RFC 4007 */
    /* release the address info structure to the free pool */

    Ip6AddrDeleteInfo ((UINT1 *) pOldInfo, ADDR6_UNICAST);

    return IP6_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : Called when an address is made up
 *
 * INPUTS      : The logical interface (i2Index),The IPv6 addr ptr (pAddr),
 *               the prefixlength (u1Prefixlen),the addr info ptr (pAddrInfo) *
 * OUTPUTS     : None
 *
 * RETURNS     : None
 *
 * NOTES       : 
 *****************************************************************************/

INT1
Ip6AddrUp (UINT4 u4Index, tIp6Addr * pAddr, UINT1 u1Prefixlen,
           tIp6AddrInfo * pAddrInfo)
{
    tTMO_SLL_NODE      *pAddrSll = NULL;
    tIp6If             *pIf6 = NULL;
    UINT4               u4ContextId = VCM_INVALID_VC;
    UINT1               u1Type = 0;

    /* To handle warnings in case of Trace is not defined */
    UNUSED_PARAM (u1Prefixlen);

    if (u4Index != 0)
    {
        Ip6GetCxtId (u4Index, &u4ContextId);
    }

    IP6_TRC_ARG3 (u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                  "IP6ADDR :Ip6AddrUp: Make status UP for Addr = %s "
                  "Prefixlen = %d If = %d \n",
                  Ip6PrintAddr (pAddr), u1Prefixlen, u4Index);

    /* Check the type of address */
    u1Type = (UINT1) Ip6AddrType (pAddr);

    if ((u1Type == ADDR6_UNSPECIFIED) || (u1Type == ADDR6_MULTI))
    {
        IP6_TRC_ARG (u4ContextId, IP6_MOD_TRC, ALL_FAILURE_TRC, IP6_NAME,
                     "IP6ADDR :Ip6AddrUp: Enabling unspecified or "
                     "multicast address FAILED\n");

        return IP6_FAILURE;
    }

    else if (u1Type == ADDR6_LLOCAL)
    {
        tIp6LlocalInfo     *pLlocalInfo = NULL;

        pIf6 = Ip6ifGetEntry (u4Index);
        if (pIf6 == NULL)
        {
            return IP6_FAILURE;
        }
        TMO_SLL_Scan (&pIf6->lla6Ilist, pAddrSll, tTMO_SLL_NODE *)
        {
            pLlocalInfo = IP6_LLADDR_PTR_FROM_SLL (pAddrSll);

            if (MEMCMP (pAddr, &pLlocalInfo->ip6Addr, sizeof (tIp6Addr)) == 0)
            {
                break;
            }
        }

        if (pLlocalInfo == NULL)
        {
            return IP6_FAILURE;
        }

        if ((Ip6ifGetIfOper (pLlocalInfo->pIf6->u1IfType,
                             pLlocalInfo->pIf6->u4Index,
                             pLlocalInfo->pIf6->u2CktIndex) == OPER_UP))
        {
            if (pIf6->u1OperStatus == OPER_NOIFIDENTIFIER)
            {
                /* Interface is down, because of Link-Local DAD failure.
                 * Now DAD is to be intiated again. So set the status
                 * as UP and initiate DAD. */
                pIf6->u1OperStatus = OPER_UP;
            }

            pLlocalInfo->u1Status = ADDR6_UP;
            pLlocalInfo->u1Status |= ADDR6_TENTATIVE;

            Ip6AddrDadStart ((UINT1 *) pLlocalInfo, ADDR6_LLOCAL);

            /* Add the Solicated Multicast Address */
            Ip6AddrCreateSolicitMcast (u4Index, pAddr);
        }
    }
    else
    {
        if (pAddrInfo == NULL)
        {
            return IP6_FAILURE;
        }
        if ((pAddrInfo->pIf6->u1OperStatus == OPER_UP) &&
            (Ip6ifGetIfOper (pAddrInfo->pIf6->u1IfType,
                             pAddrInfo->pIf6->u4Index,
                             pAddrInfo->pIf6->u2CktIndex) == OPER_UP))
        {
#if defined(CLI_LNXIP_WANTED) || defined(SNMP_LINXIP_WANTED)
            /* Any change in ipv6 address for OOB interface is indicated to cfa
             * when node state is active. This is because when ipv6 addr change
             * happens for OOB, the linux programming will be done in cfa. */
            if ((CfaIsMgmtPort (u4Index) == TRUE) && (u1Type == ADDR6_UNICAST))
            {
                CfaSetIfIp6Addr (u4Index, pAddr, (INT4) u1Prefixlen, OSIX_TRUE);
            }
#endif

            /* Set the Address Oper Status as UP. Also initiate the DAD for this
             * address */
            pAddrInfo->u1Status = ADDR6_UP;
            pAddrInfo->u1Status |= ADDR6_TENTATIVE;
            NetIpv6InvokeAddressChange (pAddr, u1Prefixlen, ADDR6_UNICAST,
                                        u4Index, NETIPV6_ADDRESS_ADD);

            Ip6AddrDadStart ((UINT1 *) pAddrInfo, ADDR6_UNICAST);

            /* Add the Solicated Multicast Address */
            Ip6AddrCreateSolicitMcast (u4Index, pAddr);
        }
    }

    return IP6_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : Called when an address is made down
 *
 * INPUTS      : The If  (i2Index), IPv6 addr ptr (pAddr),
 *               prefixlength (u1Prefixlen) , addr info ptr (pInfo)
 *
 * OUTPUTS     : None
 *
 * RETURNS     : None
 *
 * NOTES       : 
 ******************************************************************************/

INT1
Ip6AddrDown (UINT4 u4Index, tIp6Addr * pAddr, UINT1 u1Prefixlen,
             tIp6AddrInfo * pInfo)
{
    UINT4               u4ContextId = 0;
    UINT1               u1Type = 0;

    if (u4Index == 0)
    {
        u4ContextId = VCM_INVALID_VC;
    }
    else
    {
        u4ContextId =
            ((tIp6If *) IP6_INTERFACE (u4Index))->pIp6Cxt->u4ContextId;
    }

    IP6_TRC_ARG3 (u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                  "IP6ADDR:Ip6AddrDown: Disabling address = %s Pfxlen = %d "
                  "on IF = %d\n", Ip6PrintAddr (pAddr), u1Prefixlen, u4Index);

    u1Type = (UINT1) Ip6AddrType (pAddr);

    if ((u1Type == ADDR6_UNSPECIFIED) || (u1Type == ADDR6_MULTI))
    {
        IP6_TRC_ARG (u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                     "IP6ADDR: Ip6AddrDown:Cannot disable unspecified or "
                     "multicast address\n");
        return IP6_FAILURE;
    }
    else if (u1Type == ADDR6_LLOCAL)
    {
        IP6_TRC_ARG (u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                     "IP6ADDR:Ip6AddrDown: Cannot disable link-local address\n");
        return IP6_FAILURE;
    }
    else
    {

        if ((pInfo->u1Status & ADDR6_DOWN) != ADDR6_DOWN)
        {
            /* 
             * Indicate to RIP6 module to delete the local route corresponding
             * to this address from the routing table
             */
            if ((pInfo->u1ConfigMethod != IP6_ADDR_AUTO_SL) &&
                (pInfo->u1Status & ADDR6_COMPLETE (pInfo->pIf6)))
            {
                Ip6DelLocalRoute (&pInfo->ip6Addr, u1Prefixlen, u4Index);
            }

            /* Stop DAD for this address if running */

            if (pInfo->u1Status & ADDR6_TENTATIVE)
            {
                Ip6AddrDadStatus ((UINT1 *) pInfo, DAD_FAILURE, ADDR6_UNICAST);
            }

            pInfo->u1Status = ADDR6_DOWN;

            /* Remove the Solicated Multicast Address */
            Ip6AddrDeleteSolicitMcast (pInfo->pIf6->u4Index, &pInfo->ip6Addr);
        }
    }

    return IP6_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : This function can be used by external modules to get the
 *               source address corresponding to the destination address
 *               in the default context and it will be used while
 *               sending the packet.
 *
 * INPUTS      : The IPv6 destination addr ptr  (pDstAddr) and
 *               IPv6 source addr ptr (pSrcAddr),
 *
 * OUTPUTS     : None
 *
 * RETURNS     : IP6_SUCCESS / IP6_FAILURE
 *
 * NOTES       :
 ******************************************************************************/
INT4
Ip6SrcAddrForDestAddr (tIp6Addr * pDstAddr, tIp6Addr * pSrcAddr)
{
    return (Ip6SrcAddrForDestAddrInCxt (IP6_DEFAULT_CONTEXT,
                                        pDstAddr, pSrcAddr));

}

/******************************************************************************
 * DESCRIPTION : This function can be used by external modules to get the
 *               source address corresponding to the destination address
 *               in the given context and it will be used while
 *               sending the packet.
 *
 * INPUTS      : The IPv6 destination addr ptr  (pDstAddr) and
 *               IPv6 source addr ptr (pSrcAddr),
 *
 * OUTPUTS     : None
 *
 * RETURNS     : IP6_SUCCESS / IP6_FAILURE
 *
 * NOTES       :
 ******************************************************************************/

INT4
Ip6SrcAddrForDestAddrInCxt (UINT4 u4ContextId, tIp6Addr * pDstAddr,
                            tIp6Addr * pSrcAddr)
{
    tIp6If             *pIf6 = NULL;
    tIp6Addr           *pSrcTempAddr = NULL;

    if (IS_ADDR_LOOPBACK (*pDstAddr))
    {
        MEMCPY (pSrcAddr, pDstAddr, sizeof (tIp6Addr));
        return IP6_SUCCESS;
    }
    else
    {
        pIf6 = Ip6FindRtIfInCxt (u4ContextId, pDstAddr);
    }

    if ((pSrcTempAddr = Ip6AddrGetSrc (pIf6, pDstAddr)) == NULL)
    {
        IP6_TRC_ARG (u4ContextId, IP6_MOD_TRC, MGMT_TRC, IP6_NAME,
                     "ADDR6: Ip6SrcAddrForDestAddr:SrcAddr is NULL\n");
        IP6_TRC_ARG2 (u4ContextId, IP6_MOD_TRC, ALL_FAILURE_TRC, IP6_NAME,
                      "%s gen err: Type = %d \n", ERROR_MINOR_STR,
                      ERR_GEN_NULL_PTR);
        /* this can be used by calling function for check */
        return (IP6_FAILURE);
    }

    /* moving contents from TempAddr to SrcAddr */
    *pSrcAddr = *pSrcTempAddr;

    return IP6_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : This function can be used by external modules to get the
 *               source address corresponding to the destination address
 *               that will be used while sending the packet.
 *
 * INPUTS      : The IPv6 destination addr ptr  (pDstAddr) and
 *               IPv6 source addr ptr (pSrcAddr),
 *
 * OUTPUTS     : None
 *
 * RETURNS     : IP6_SUCCESS / IP6_FAILURE
 *
 * NOTES       :
 ******************************************************************************/
INT4
Ip6IsOurAddrInCxt (UINT4 u4ContextId, tIp6Addr * pIp6Addr, UINT4 *pu4Index)
{
    tIp6Cxt            *pIp6Cxt = NULL;
    UINT4               u4Index = 1;

    pIp6Cxt = Ipv6UtilGetCxtEntryFromCxtId (u4ContextId);
    if (pIp6Cxt == NULL)
    {
        return IP6_FAILURE;
    }

    IP6_IF_SCAN (u4Index, (UINT4) IP6_MAX_LOGICAL_IF_INDEX)
    {
        if ((gIp6GblInfo.apIp6If[u4Index] == NULL)
            || (gIp6GblInfo.apIp6If[u4Index]->pIp6Cxt != pIp6Cxt))
        {
            continue;
        }
        if ((gIp6GblInfo.apIp6If[u4Index]->u1AdminStatus == ADMIN_VALID) ||
            (gIp6GblInfo.apIp6If[u4Index]->u1AdminStatus == ADMIN_UP))
        {
            if (Ip6IsMyAddr (pIp6Addr, gIp6GblInfo.apIp6If[u4Index])
                == IP6_SUCCESS)
            {
                /* update index for correct value */
                if (pu4Index != NULL)
                {
                    *pu4Index = u4Index;
                }
                return IP6_SUCCESS;
            }
        }
    }

    return IP6_FAILURE;

}

/******************************************************************************
 * DESCRIPTION : Called when an solicated multicast address is to be created 
 *
 * INPUTS      : Interface index, pointer to unicast address.
 *
 * OUTPUTS     : None
 *
 * RETURNS     : IP6_SUCCESS/IP6_FAILURE 
 *
 * NOTES       : For every unicast address, there should exist a corresponding
 *               solicated multicast address. So derive the solicated multicast
 *               address for the given unicast address and add it to the mcast
 *               list supported for this interface.
 ******************************************************************************/
INT1
Ip6AddrCreateSolicitMcast (UINT4 u4Index, tIp6Addr * pAddr)
{
    INT1                i1RetVal = 0;
    tIp6Addr            tmpAddr;

    MEMSET (&tmpAddr, 0, sizeof (tIp6Addr));
    tmpAddr.u1_addr[0] = IP6_UINT1_ALL_ONE;
    tmpAddr.u1_addr[1] = 0x02;
    tmpAddr.u1_addr[11] = 0x01;
    tmpAddr.u1_addr[12] = IP6_UINT1_ALL_ONE;
    tmpAddr.u1_addr[13] = pAddr->u1_addr[13];
    tmpAddr.u1_addr[14] = pAddr->u1_addr[14];
    tmpAddr.u1_addr[15] = pAddr->u1_addr[15];

    i1RetVal = Ip6AddrCreateMcast (u4Index, &tmpAddr);
    return i1RetVal;
}

/******************************************************************************
 * DESCRIPTION : Called when an solicated multicast address is to be deleted
 *
 * INPUTS      : Interface index, pointer to unicast address.
 *
 * OUTPUTS     : None
 *
 * RETURNS     : IP6_SUCCESS/IP6_FAILURE 
 *
 * NOTES       : For every unicast address, there should exist a corresponding
 *               solicated multicast address. So derive the solicated multicast
 *               address for the given unicast address and delete it from the
 *               mcast list supported for this interface.
 ******************************************************************************/
INT1
Ip6AddrDeleteSolicitMcast (UINT4 u4Index, tIp6Addr * pAddr)
{
    INT1                i1RetVal = 0;
    tIp6Addr            tmpAddr;

    MEMSET (&tmpAddr, 0, sizeof (tIp6Addr));
    tmpAddr.u1_addr[0] = IP6_UINT1_ALL_ONE;
    tmpAddr.u1_addr[1] = 0x02;
    tmpAddr.u1_addr[11] = 0x01;
    tmpAddr.u1_addr[12] = IP6_UINT1_ALL_ONE;
    tmpAddr.u1_addr[13] = pAddr->u1_addr[13];
    tmpAddr.u1_addr[14] = pAddr->u1_addr[14];
    tmpAddr.u1_addr[15] = pAddr->u1_addr[15];

    i1RetVal = Ip6AddrDeleteMcast (u4Index, &tmpAddr);
    return i1RetVal;
}

/******************************************************************************
 * DESCRIPTION : Called when an multicast address is to be created 
 *
 * INPUTS      : Interface index, pointer to multicast address.
 *
 * OUTPUTS     : None
 *
 * RETURNS     : IP6_SUCCESS/IP6_FAILURE 
 *
 * NOTES       :
 ******************************************************************************/
INT1
Ip6AddrCreateMcast (UINT4 u4Index, tIp6Addr * pAddr)
{
    tIp6McastInfo      *pAddrInfo = NULL;
    tIp6McastInfo      *pPrevAddrInfo = NULL;
    tIp6If             *pIf6 = NULL;

    /* Verify whether the same MCAST Address is already present or not.
     * If present, then increment the RefCount. Else create a new
     * address and add to the list. */
    pIf6 = Ip6ifGetEntry (u4Index);
    if (pIf6 == NULL)
    {
        return IP6_FAILURE;
    }
    TMO_SLL_Scan (&pIf6->mcastIlist, pAddrInfo, tIp6McastInfo *)
    {
        if (Ip6AddrMatch (&(pAddrInfo->ip6Addr), pAddr,
                          IP6_ADDR_SIZE_IN_BITS) == TRUE)
        {
            /* Matching Address Present. Increment the Ref Count */
            pAddrInfo->u2RefCnt++;
            return IP6_SUCCESS;
        }

        if (Ip6IsAddrGreater (pAddr, &pAddrInfo->ip6Addr) == SUCCESS)
        {
            /* Address are stored in the Ascending order. New Address
             * is smaller than existing address. So add the new
             * address before this address */
            break;
        }
        pPrevAddrInfo = pAddrInfo;

    }

    /* Allocate memory and add the new MCAST Address */
    pAddrInfo =
        (tIp6McastInfo *) (VOID *) Ip6GetMem (pIf6->pIp6Cxt->u4ContextId,
                                              (UINT2) gIp6GblInfo.i4Ip6mcastId);

    if (pAddrInfo == NULL)
    {
        return IP6_FAILURE;
    }

    MEMSET (pAddrInfo, 0, sizeof (tIp6McastInfo));

    MEMCPY (&(pAddrInfo->ip6Addr), pAddr, sizeof (tIp6Addr));

    /* Add the address to the list of addresses on the interface */
    TMO_SLL_Insert (&pIf6->mcastIlist, &(pPrevAddrInfo->nextAif),
                    &(pAddrInfo->nextAif));

    /* Set the Ref Count */
    pAddrInfo->u2RefCnt = 1;
    pAddrInfo->u1AddrScope = Ip6GetAddrScope (pAddr);
    Ip6ZoneCreateAutoScopeZone (u4Index, pAddrInfo->u1AddrScope);

    NetIpv6InvokeAddressChange (pAddr, IP6_ADDR_MAX_PREFIX, ADDR6_MULTI,
                                pIf6->u4Index, NETIPV6_ADDRESS_ADD);

    /* Update the MAC-Filter List */
    Ip6AddMcastAddrInMACFilter (u4Index, pAddr);

    return IP6_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : Called when an multicast address is to be deleted 
 *
 * INPUTS      : pointer to multicast address.
 *               Interface index from which this multicast address needs
 *               to be removed.
 *
 * OUTPUTS     : None
 *
 * RETURNS     : None
 *
 * NOTES       :
 ******************************************************************************/
INT1
Ip6AddrDeleteMcast (UINT4 u4IfIndex, tIp6Addr * ip6Addr)
{

    tIp6McastInfo      *pAddr6Info = NULL;
    tIp6If             *pIf6 = NULL;
    tIp6IfZoneMapInfo  *pIp6IfZoneMapInfo = NULL;
    INT4                i4ZoneId = 0;
    UINT1               u1Scope = ADDR6_SCOPE_INVALID;
    UINT4               u4ContextId = IP6_DEFAULT_CONTEXT;

    /* Scan through the Mcast List for the given interface and delete
     * the matching entry. */
    pIf6 = Ip6ifGetEntry (u4IfIndex);
    if (pIf6 == NULL)
    {
        return IP6_FAILURE;
    }

    TMO_SLL_Scan (&pIf6->mcastIlist, pAddr6Info, tIp6McastInfo *)
    {
        if (Ip6AddrMatch (&(pAddr6Info->ip6Addr), ip6Addr,
                          IP6_ADDR_SIZE_IN_BITS) == TRUE)

        {
            /* Node is listening for the Multi-Cast address over
             * this interface. Decrement the Reference Count. And if RefCnt
             * is 0, release the MCAST Address */
            pAddr6Info->u2RefCnt--;
            if (pAddr6Info->u2RefCnt > 0)
            {
                return IP6_SUCCESS;
            }
            break;
        }
    }

    if (pAddr6Info == NULL)
    {
        return IP6_FAILURE;
    }

    NetIpv6InvokeAddressChange (ip6Addr, IP6_ADDR_MAX_PREFIX, ADDR6_MULTI,
                                u4IfIndex, NETIPV6_ADDRESS_DELETE);

    /* Update the MAC-Filter List */
    Ip6DeleteMcastAddrInMACFilter (u4IfIndex, ip6Addr);

    TMO_SLL_Delete (&pIf6->mcastIlist, &(pAddr6Info->nextAif));

    /* Free the memory. */
    if (pIf6->pIp6Cxt != NULL)
    {
        u4ContextId = pIf6->pIp6Cxt->u4ContextId;
    }
    Ip6RelMem (u4ContextId, (UINT2) gIp6GblInfo.i4Ip6mcastId,
               (UINT1 *) pAddr6Info);
    u1Scope = Ip6GetAddrScope (ip6Addr);

    if (((u1Scope == ADDR6_SCOPE_LLOCAL) &&
         (TMO_SLL_Count (&pIf6->lla6Ilist) == 0)))
    {
        pIp6IfZoneMapInfo = Ip6ZoneGetIfScopeZoneEntry (u4IfIndex, u1Scope);
        if (NULL != pIp6IfZoneMapInfo)
        {
            i4ZoneId = pIp6IfZoneMapInfo->i4ZoneId;
            Ip6ScopeZoneDelete (u4IfIndex, u1Scope, i4ZoneId);
        }
    }

    return IP6_SUCCESS;

}

/******************************************************************************
 * DESCRIPTION : Determines whether the packet is addressed to the Node 
 *               for whom we act as Proxy.
 *
 * INPUTS      : The type of address(Link-local,multicast,etc),flag telling
 *               whether to check in the link-local address list,the IPv6
 *               address(pIp6Addr)
 *
 * OUTPUTS     : The state of the address(Tentative,complete) in *pu1Type
 *
 * RETURNS     : IP6_SUCCESS if, we need to act as Proxy.
 *               IP6_FAILURE if, we need not act as Proxy.
 *
 * NOTES       :
 ******************************************************************************/
INT4
Ip6IsPktToProxyInCxt (tIp6Cxt * pIp6Cxt, UINT1 *pu1Type, tIp6Addr * pIp6Addr,
                      tCRU_BUF_CHAIN_HEADER * pBuf)
{
    tTMO_SLL_NODE      *pSll = NULL;
    tNd6ProxyListNode  *pCurrNode = NULL;
#ifdef HA_WANTED
    UINT1               u1Type;
    UINT1               u1Nhdr = 0;
    UINT2               u2Offset = 0;
    UINT2               u2Temp = 0;
#else
    UNUSED_PARAM (pBuf);
#endif

    if (!(pIp6Addr))
    {
        IP6_TRC_ARG (pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                     CONTROL_PLANE_TRC, IP6_NAME,
                     "IP6ADDR: Ip6IsPktToProxyInCxt: rcvd address is NULL\n");
        IP6_TRC_ARG2 (pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                      CONTROL_PLANE_TRC, IP6_NAME,
                      "%s gen err: Type = %d ", ERROR_FATAL_STR,
                      ERR_GEN_NULL_PTR);

        return IP6_FAILURE;
    }

    if ((((*pu1Type) == ADDR6_MULTI) && (IS_ADDR_SOLICITED (*pIp6Addr)))
        || ((*pu1Type) == ADDR6_UNICAST))
    {
        /*
         * Loop through the list of addresses in the Proxy List
         * and compare the lower 24 bits of each address against
         * the passed Destination address in the Packet.
         */

        TMO_SLL_Scan (&(pIp6Cxt->Nd6ProxyList), pSll, tTMO_SLL_NODE *)
        {
            pCurrNode = ND6_PROXY_LST_PTR_FROM_SLL (pSll);

            if ((OSIX_HTONL (pCurrNode->Nd6ProxyAddr.u4_addr[IP6_THREE]) &
                 0x00ffffff) ==
                (OSIX_HTONL (pIp6Addr->u4_addr[IP6_THREE]) & 0x00ffffff))
            {
                *pu1Type = (UINT1) 0x02;
#ifdef HA_WANTED
                u2Temp = (UINT2) IP6_BUF_READ_OFFSET (pBuf);
                IP6_BUF_READ_OFFSET (pBuf) = 0;

                /* Now , check if the packet is ND? */
                if ((pBuf != NULL)
                    && (Ip6GetHlProtocolInCxt (pIp6Cxt->u4ContextId, pBuf,
                                               &u1Nhdr, &u2Offset)
                        == IP6_SUCCESS))
                {
                    if (u1Nhdr == NH_ICMP6)
                    {
                        if (Ip6BufRead
                            (pBuf, &u1Type, u2Offset, sizeof (UINT1),
                             1) != NULL)
                        {
                            IP6_BUF_READ_OFFSET (pBuf) = u2Temp;
                            if ((u1Type == ND6_NEIGHBOR_SOLICITATION)
                                || (u1Type == ND6_NEIGHBOR_ADVERTISEMENT))
                            {
                                return IP6_SUCCESS;
                            }
                        }
                        IP6_BUF_READ_OFFSET (pBuf) = u2Temp;
                    }
                }
                return IP6_FAILURE;
#else
                return IP6_SUCCESS;
#endif
            }
        }

    }

    IP6_TRC_ARG (pIp6Cxt->u4ContextId, IP6_MOD_TRC, CONTROL_PLANE_TRC, IP6_NAME,
                 "IP6ADDR: Ip6IsPktToProxyInCxt: Address Not Solicited "
                 "MCAST\n");
    return IP6_FAILURE;
}

#ifdef HA_WANTED
/******************************************************************************
 * DESCRIPTION : This function verifies whether this address belongs to the 
 *               mobile node for which this node is serving as home agent.
 * INPUTS      : pIf6 - Pointer to the interface structure on which the address
 *                      need to be searched.
 *               pAddr - pointer to the address which needs to be verified.
 *               pu1AddrType - Pointer to the address type variable.
 * OUTPUTS     : None.
 * RETURNS     : IP6_SUCCESS if, we need to act as Proxy.
 *               IP6_FAILURE if, we need not act as Proxy.
 ******************************************************************************/

INT4
Ip6IsAddrMNHomeAddr (tIp6If * pIf6, tIp6Addr * pAddr, UINT1 *pu1AddrType)
{
    tIp6AddrInfo       *pCurrAddr = NULL;
    tTMO_SLL_NODE      *p_sll_llocal = NULL;

    TMO_SLL_Scan (&MNHAIlist[pIf6->u4Index], p_sll_llocal, tTMO_SLL_NODE *)
    {
        pCurrAddr = IP6_MN_HOMEADDR_PTR_FROM_SLL (p_sll_llocal);
        if ((Ip6AddrMatch (pAddr, &pCurrAddr->ip6Addr, IP6_ADDR_MAX_PREFIX)) &&
            (pCurrAddr->u2AddrFlag == 1))
        {
            *pu1AddrType |= ADDR6_TENTATIVE;
            return IP6_SUCCESS;
        }
    }
    return IP6_FAILURE;
}
#endif

tIp6AddrProfile    *
Ip6GetAddrProfileFromProfIndex (UINT2 u2ProfIndex)
{
    if (IP6_ADDR_PROF_ADMIN (u2ProfIndex) != IP6_ADDR_PROF_INVALID)
    {
        return gIp6GblInfo.apIp6AddrProfile[u2ProfIndex];
    }
    return NULL;
}

/******************************************************************************
 * DESCRIPTION : This function creates an entry for the given index in the
 *               global MAC Filter database.
 *
 * INPUTS      : Interface index, MAC address.
 *
 * OUTPUTS     : None.
 *
 * RETURNS     : IP6_SUCCESS/IP6_FAILURE
 ******************************************************************************/
INT4
Ip6AddMACAddrInMACFilter (UINT4 u4Index, UINT1 *pMacAddr)
{
    UINT4               u4Ind = 0;
    tIp6MacFilterNode  *pNode = NULL;
    UINT1               au1MacAddr[IP6_MAX_ENET_ADDR_LEN];
    UINT4               u4ContextId = 0;

    for (u4Ind = 0; u4Ind < (UINT4) IP6_MAX_FILTER; u4Ind++)
    {
        if (TMO_SLL_Count (&gIp6GblInfo.aIp6MACFilter[u4Ind].MacAddrList) == 0)
        {
            break;
        }
    }

    if (u4Ind == (UINT4) IP6_MAX_FILTER)
    {
        return IP6_FAILURE;
    }

    Ip6GetCxtId (u4Index, &u4ContextId);
    MEMCPY (au1MacAddr, pMacAddr, IP6_MAX_ENET_ADDR_LEN);
    pNode =
        (tIp6MacFilterNode *) (VOID *) Ip6GetMem (u4ContextId,
                                                  (UINT2) gIp6GblInfo.
                                                  i4MacFilterId);
    if (pNode == NULL)
    {
        return IP6_FAILURE;
    }

    gIp6GblInfo.aIp6MACFilter[u4Ind].u4Index = u4Index;
    MEMCPY (pNode->u1MacAddr, au1MacAddr, IP6_MAX_ENET_ADDR_LEN);
    TMO_SLL_Add (&gIp6GblInfo.aIp6MACFilter[u4Ind].MacAddrList,
                 &(pNode->NextNode));

    return IP6_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : This function adds multicast MAC address for the given
 *               multicast address in the global MAC Filter database.
 *
 * INPUTS      : Interface index, unicast address.
 *
 * OUTPUTS     : None.
 *
 * RETURNS     : IP6_SUCCESS/IP6_FAILURE
 ******************************************************************************/
INT4
Ip6AddMcastAddrInMACFilter (UINT4 u4Index, tIp6Addr * pMcastAddr)
{
    tIp6MacFilterNode  *pNode = NULL;
    UINT4               u4Ind = 0;
    UINT4               u4ContextId = 0;

    for (u4Ind = 0; u4Ind < (UINT4) IP6_MAX_FILTER; u4Ind++)
    {
        if (gIp6GblInfo.aIp6MACFilter[u4Ind].u4Index == u4Index)
        {
            break;
        }
    }

    if (u4Ind == (UINT4) IP6_MAX_FILTER)
    {
        return IP6_FAILURE;
    }

    Ip6GetCxtId (u4Index, &u4ContextId);
    pNode =
        (tIp6MacFilterNode *) (VOID *) Ip6GetMem (u4ContextId,
                                                  (UINT2) gIp6GblInfo.
                                                  i4MacFilterId);
    if (pNode == NULL)
    {
        return IP6_FAILURE;
    }

    pNode->u1MacAddr[0] = MCAST_MAC_ADDR_OCTET_1;
    pNode->u1MacAddr[1] = MCAST_MAC_ADDR_OCTET_2;
    pNode->u1MacAddr[2] = pMcastAddr->u1_addr[12];
    pNode->u1MacAddr[3] = pMcastAddr->u1_addr[13];
    pNode->u1MacAddr[4] = pMcastAddr->u1_addr[14];
    pNode->u1MacAddr[5] = pMcastAddr->u1_addr[15];

    TMO_SLL_Add (&gIp6GblInfo.aIp6MACFilter[u4Ind].MacAddrList,
                 &(pNode->NextNode));

#ifdef NPAPI_WANTED
    Ipv6FsNpIpv6AddMcastMAC (u4ContextId, u4Index,
                             pNode->u1MacAddr, IP6_MAX_ENET_ADDR_LEN);
#endif /* NPAPI_WANTED */

    return IP6_SUCCESS;

}

/******************************************************************************
 * DESCRIPTION : This function deletes multicast MAC address for the given
 *               multicast address from the global MAC Filter database.
 *
 * INPUTS      : Interface index, unicast address.
 *
 * OUTPUTS     : None.
 *
 * RETURNS     : IP6_SUCCESS/IP6_FAILURE
 ******************************************************************************/
INT4
Ip6DeleteMcastAddrInMACFilter (UINT4 u4Index, tIp6Addr * pMcastAddr)
{
    tIp6MacFilterNode  *pNode = NULL;
    UINT1               au1MacAddr[IP6_MAX_ENET_ADDR_LEN];
    UINT4               u4Ind = 0;
    UINT4               u4ContextId = 0;

    /* Get the solicated address */
    au1MacAddr[0] = MCAST_MAC_ADDR_OCTET_1;
    au1MacAddr[1] = MCAST_MAC_ADDR_OCTET_2;
    au1MacAddr[2] = pMcastAddr->u1_addr[12];
    au1MacAddr[3] = pMcastAddr->u1_addr[13];
    au1MacAddr[4] = pMcastAddr->u1_addr[14];
    au1MacAddr[5] = pMcastAddr->u1_addr[15];

    for (u4Ind = 0; u4Ind < (UINT4) IP6_MAX_FILTER; u4Ind++)
    {
        if (gIp6GblInfo.aIp6MACFilter[u4Ind].u4Index == u4Index)
        {
            break;
        }
    }

    if (u4Ind == (UINT4) IP6_MAX_FILTER)
    {
        return IP6_FAILURE;
    }

    /* Find the matching node */
    TMO_SLL_Scan (&gIp6GblInfo.aIp6MACFilter[u4Ind].MacAddrList, pNode,
                  tIp6MacFilterNode *)
    {
        if (MEMCMP (au1MacAddr, pNode->u1MacAddr, MAC_ADDR_LEN) == 0)
        {
            /* Match found. */
            break;
        }
    }

    if (pNode == NULL)
    {
        /* No matching entry */
        return IP6_FAILURE;
    }

    /* Delete the node from the list. */
    TMO_SLL_Delete (&gIp6GblInfo.aIp6MACFilter[u4Ind].MacAddrList,
                    &(pNode->NextNode));

    Ip6GetCxtId (u4Index, &u4ContextId);
#ifdef NPAPI_WANTED
    Ipv6FsNpIpv6DelMcastMAC (u4ContextId, u4Index,
                             pNode->u1MacAddr, IP6_MAX_ENET_ADDR_LEN);
#endif /* NPAPI_WANTED */

    /* Free the Node */
    Ip6RelMem (u4ContextId, (UINT2) gIp6GblInfo.i4MacFilterId, (UINT1 *) pNode);

    if (TMO_SLL_Count (&gIp6GblInfo.aIp6MACFilter[u4Ind].MacAddrList) == 0)
    {
        /* MAC Filter list is empty. Initialize the list. */
        gIp6GblInfo.aIp6MACFilter[u4Index].u4Index = IP6_INVALID_IF_INDEX;
        TMO_SLL_Init (&gIp6GblInfo.aIp6MACFilter[u4Index].MacAddrList);
    }
    return IP6_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : This function deletes all the MAC address from MAC Filter
 *               table for the given interface.
 *
 * INPUTS      : Interface index
 *
 * OUTPUTS     : None.
 *
 * RETURNS     : IP6_SUCCESS/IP6_FAILURE
 ******************************************************************************/
INT4
Ip6DeleteMACAddrFromMACFilter (UINT4 u4Index)
{
    tTMO_SLL           *pList = NULL;
    tTMO_SLL_NODE      *pTempNode = NULL;
    tTMO_SLL_NODE      *pNode = NULL;
    tTMO_SLL_NODE      *pHead = NULL;
    UINT4               u4Ind = 0;
    UINT4               u4ContextId = 0;

    for (u4Ind = 0; u4Ind < (UINT4) IP6_MAX_FILTER; u4Ind++)
    {
        if (gIp6GblInfo.aIp6MACFilter[u4Ind].u4Index == u4Index)
        {
            break;
        }
    }

    if (u4Ind == (UINT4) IP6_MAX_FILTER)
    {
        return IP6_FAILURE;
    }

    pList = &gIp6GblInfo.aIp6MACFilter[u4Ind].MacAddrList;
    for (pHead = &pList->Head, pNode = pList->Head.pNext; pNode != pHead;
         pNode = pTempNode)
    {
        pTempNode = pNode->pNext;

        Ip6GetCxtId (u4Index, &u4ContextId);
#ifdef NPAPI_WANTED
        Ipv6FsNpIpv6DelMcastMAC (u4ContextId, u4Index,
                                 ((tIp6MacFilterNode *) pNode)->u1MacAddr,
                                 IP6_MAX_ENET_ADDR_LEN);
#endif /* NPAPI_WANTED */

        Ip6RelMem (u4ContextId, (UINT2) gIp6GblInfo.i4MacFilterId,
                   (UINT1 *) pNode);
    }

    TMO_SLL_Init (pList);
    return IP6_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : Called when an RA Prefix is either created or deleted to
 *               perform the corresponding operations.
 *
 * INPUTS      : Pointer to the PerfixNode created or deleted. 
 *
 * OUTPUTS     : None
 *
 * RETURNS     : IP6_SUCCESS / IP6_FAILURE
 *
 * NOTES       :
 ******************************************************************************/
INT4
Ip6RaPrefixNotify (tIp6PrefixNode * pPrefixInfo, UINT4 u4PrefixStatus)
{
    UINT4               u4ContextId = 0;

    Ip6GetCxtId (pPrefixInfo->u4IfIndex, &u4ContextId);
    if (pPrefixInfo->u2ProfileIndex >=
        FsIP6SizingParams[MAX_IP6_ADDR_PROFILES_SIZING_ID].u4PreAllocatedUnits)
    {
        return IP6_FAILURE;
    }
    switch (u4PrefixStatus)
    {
        case PREFIX_CREATE:
            if (IP6_ADDR_VALID_TIME_FIXED (pPrefixInfo->u2ProfileIndex) == 0)
            {
                /* Need to start the Variable Valid Timer */
                Ip6TmrStart (u4ContextId, IP6_PREFIX_VALID_TIMER_ID,
                             gIp6GblInfo.Ip6TimerListId,
                             &pPrefixInfo->ValidTimer.appTimer,
                             IP6_ADDR_VALID_TIME (pPrefixInfo->u2ProfileIndex));
            }

            if (IP6_ADDR_PREF_TIME_FIXED (pPrefixInfo->u2ProfileIndex) == 0)
            {
                /* Need to start the Variable Prefer Timer */
                Ip6TmrStart (u4ContextId, IP6_PREFIX_PREF_TIMER_ID,
                             gIp6GblInfo.Ip6TimerListId,
                             &pPrefixInfo->PreferTimer.appTimer,
                             IP6_ADDR_PREF_TIME (pPrefixInfo->u2ProfileIndex));
            }
            break;

        case PREFIX_DELETE:
            /* Stop the Prefer and Valid Timer if running */
            if (pPrefixInfo->ValidTimer.u1Id != 0)
            {
                Ip6TmrStop (u4ContextId, IP6_PREFIX_VALID_TIMER_ID,
                            gIp6GblInfo.Ip6TimerListId,
                            &(pPrefixInfo->ValidTimer.appTimer));
            }

            if (pPrefixInfo->PreferTimer.u1Id != 0)
            {
                Ip6TmrStop (u4ContextId, IP6_PREFIX_PREF_TIMER_ID,
                            gIp6GblInfo.Ip6TimerListId,
                            &(pPrefixInfo->PreferTimer.appTimer));
            }

            /* Decrement the Profile's Ref Count */
            IP6_ADDR_PROF_REF_COUNT (pPrefixInfo->u2ProfileIndex)--;
            if (IP6_ADDR_PROF_REF_COUNT (pPrefixInfo->u2ProfileIndex) == 0)
            {
                if (pPrefixInfo->u2ProfileIndex <
                    gIp6GblInfo.u4NextProfileIndex)
                {
                    gIp6GblInfo.u4NextProfileIndex =
                        pPrefixInfo->u2ProfileIndex;
                }
            }

            /* Free the memory. */
            Ip6RelMem (u4ContextId, (UINT2) gIp6GblInfo.i4Ip6PrefListId,
                       (UINT1 *) pPrefixInfo);
            break;

        default:
            return IP6_FAILURE;
    }

    return IP6_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : Called when an prefix advertised in the RA message is to be
 *               created 
 *
 * INPUTS      : Interface index, pointer to Prefix address, Prefix Length
 *
 * OUTPUTS     : None
 *
 * RETURNS     : Create Prefix Info or NULL
 *
 * NOTES       :
 ******************************************************************************/
tIp6PrefixNode     *
Ip6RAPrefixCreate (UINT4 u4IfIndex, tIp6Addr * pPrefix, UINT1 u1PrefixLen,
                   UINT2 u2Addr6Profile, UINT1 u1AdminStatus)
{
    tIp6PrefixNode     *pTempPrefixInfo = NULL;
    tIp6PrefixNode     *pPrevPrefixInfo = NULL;
    tIp6PrefixNode     *pPrefixInfo = NULL;
    tIp6If             *pIf6 = NULL;

    pIf6 = Ip6ifGetEntry (u4IfIndex);
    if (pIf6 == NULL)
    {
        return NULL;
    }
    if (u2Addr6Profile >=
        FsIP6SizingParams[MAX_IP6_ADDR_PROFILES_SIZING_ID].u4PreAllocatedUnits)
    {
        return NULL;
    }

    pPrefixInfo = (tIp6PrefixNode *) (VOID *)
        Ip6GetMem (pIf6->pIp6Cxt->u4ContextId,
                   (UINT2) gIp6GblInfo.i4Ip6PrefListId);

    if (pPrefixInfo == NULL)
    {
        return NULL;
    }

    MEMSET (pPrefixInfo, 0, sizeof (tIp6PrefixNode));

    Ip6CopyAddrBits (&(pPrefixInfo->Ip6Prefix), pPrefix, u1PrefixLen);
    pPrefixInfo->u1PrefixLen = u1PrefixLen;

    pPrefixInfo->u2ProfileIndex = u2Addr6Profile;
    pPrefixInfo->u1AdminStatus = u1AdminStatus;
    pPrefixInfo->u4IfIndex = u4IfIndex;

    /* Increment the profile-index count */
    IP6_ADDR_PROF_REF_COUNT (pPrefixInfo->u2ProfileIndex)++;

    /* Add the address to the list of addresses on the interface in the
     * ascending order of the prefix */
    TMO_SLL_Scan (&pIf6->prefixlist, pTempPrefixInfo, tIp6PrefixNode *)
    {
        if (Ip6IsAddrGreaterUptoPrefixLen (pPrefix, &pTempPrefixInfo->Ip6Prefix,
                                           u1PrefixLen) == SUCCESS)
        {
            break;
        }
        pPrevPrefixInfo = pTempPrefixInfo;
    }

    TMO_SLL_Insert (&pIf6->prefixlist, &(pPrevPrefixInfo->NextPrefix),
                    &(pPrefixInfo->NextPrefix));

    /* If the Valid and Prefer Time Associated with this prefix is variable
     * then start the corresponding timers. */
    if (u1AdminStatus == IP6FWD_ACTIVE)
    {
        Ip6RaPrefixNotify (pPrefixInfo, PREFIX_CREATE);
    }

    return pPrefixInfo;
}

/******************************************************************************
 * DESCRIPTION : Called when an Prefix advertised over an interface is to be
 *               deleted 
 *
 * INPUTS      : Pointer to Prefix address, Prefix Length.
 *               Interface index from which this prefix needs to be removed.
 *
 * OUTPUTS     : None
 *
 * RETURNS     : IP6_SUCCESS / IP6_FAILURE
 *
 * NOTES       :
 ******************************************************************************/
INT1
Ip6RAPrefixDelete (UINT4 u4IfIndex, tIp6Addr * pPrefix, UINT1 u1PrefixLen)
{

    tIp6PrefixNode     *pPrefixInfo = NULL;
    tIp6If             *pIf6 = NULL;

    /* Scan through the Prefix List for the given interface and delete
     * the matching entry. */
    pIf6 = Ip6ifGetEntry (u4IfIndex);
    if (pIf6 == NULL)
    {
        return IP6_FAILURE;
    }

    TMO_SLL_Scan (&pIf6->prefixlist, pPrefixInfo, tIp6PrefixNode *)
    {
        if ((Ip6AddrMatch (&(pPrefixInfo->Ip6Prefix), pPrefix,
                           u1PrefixLen) == TRUE) &&
            (u1PrefixLen == pPrefixInfo->u1PrefixLen))

        {
            /* Matching Entry found */
            TMO_SLL_Delete (&pIf6->prefixlist, &(pPrefixInfo->NextPrefix));
            break;
        }
    }

    if (pPrefixInfo == NULL)
    {
        return IP6_FAILURE;
    }

    Ip6RaPrefixNotify (pPrefixInfo, PREFIX_DELETE);

    return IP6_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : Called when an prefix advertised in the RA message is to be
 *               retrieved.
 *
 * INPUTS      : Interface index, pointer to Prefix address, Prefix Length
 *
 * OUTPUTS     : None
 *
 * RETURNS     : Prefix Info if present or NULL
 *
 * NOTES       :
 ******************************************************************************/
tIp6PrefixNode     *
Ip6GetRAPrefix (UINT4 u4IfIndex, tIp6Addr * pPrefix, UINT1 u1PrefixLen)
{
    tIp6PrefixNode     *pPrefixInfo = NULL;
    tIp6If             *pIf6 = NULL;

    /* Scan through the Prefix List for the given interface and delete
     * the matching entry. */
    pIf6 = Ip6ifGetEntry (u4IfIndex);
    if (pIf6 == NULL)
    {
        return NULL;
    }

    TMO_SLL_Scan (&pIf6->prefixlist, pPrefixInfo, tIp6PrefixNode *)
    {
        if ((Ip6AddrMatch (&(pPrefixInfo->Ip6Prefix), pPrefix,
                           IP6_ADDR_SIZE_IN_BITS) == TRUE) &&
            (u1PrefixLen == pPrefixInfo->u1PrefixLen))

        {
            /* Matching Entry found */
            break;
        }
    }

    if (pPrefixInfo == NULL)
    {
        return NULL;
    }

    return pPrefixInfo;
}

/******************************************************************************
 * DESCRIPTION : Called to fetch the first prefix from the Prefix List 
 *               advertised over the given interface.
 *
 * INPUTS      : Interface index
 *
 * OUTPUTS     : None
 *
 * RETURNS     : Prefix Info if present or NULL
 *
 * NOTES       :
 ******************************************************************************/
tIp6PrefixNode     *
Ip6GetFirstPrefix (UINT4 u4IfIndex)
{
    tIp6PrefixNode     *pPrefix = NULL;

    if (Ip6ifEntryExists (u4IfIndex) == IP6_FAILURE)
    {
        return NULL;
    }

    pPrefix = (tIp6PrefixNode *)
        TMO_SLL_First (&gIp6GblInfo.apIp6If[u4IfIndex]->prefixlist);
    return pPrefix;
}

/******************************************************************************
 * DESCRIPTION : Called to fetch the next prefix from the Prefix List 
 *               advertised over the given interface.
 *
 * INPUTS      : Interface index, pointer to Prefix address, Prefix Length
 *
 * OUTPUTS     : None
 *
 * RETURNS     : Prefix Info if present or NULL
 *
 * NOTES       :
 ******************************************************************************/
tIp6PrefixNode     *
Ip6GetNextPrefix (UINT4 u4IfIndex, tIp6Addr * pIp6Addr, UINT1 u1PrefixLen)
{
    tIp6PrefixNode     *pPrefix = NULL;
    tIp6PrefixNode     *pNextPrefix = NULL;

    if (Ip6ifEntryExists (u4IfIndex) == IP6_FAILURE)
    {
        return NULL;
    }

    pPrefix = Ip6GetRAPrefix (u4IfIndex, pIp6Addr, u1PrefixLen);
    if (pPrefix == NULL)
    {
        return NULL;
    }
    /* If current prefix entry is null, retrieve the first valid entry
     * for that interface */
    pNextPrefix = (tIp6PrefixNode *)
        TMO_SLL_Next ((&gIp6GblInfo.apIp6If[u4IfIndex]->prefixlist),
                      &(pPrefix->NextPrefix));
    if (pNextPrefix != NULL)
    {
        return pNextPrefix;
    }

    return NULL;
}

/*****************************************************************************
* DESCRIPTION : Verifies whether the given address is anycast or not. 
* 
* INPUTS      : pIp6Addr - Address to be verified
* 
* OUTPUTS     : None
* 
* RETURNS     : IP6_SUCCESS or IP6_FAILURE 
*****************************************************************************/
INT4
Ip6IsAddrAnycastInCxt (tIp6Cxt * pIp6Cxt, tIp6Addr * pIp6Addr)
{
    UINT4               u4Index = 0;

    if (Ip6IsOurAddrInCxt (pIp6Cxt->u4ContextId, pIp6Addr, &u4Index)
        == IP6_SUCCESS)
    {
        if (Ip6GetUniAddrType (gIp6GblInfo.apIp6If[u4Index],
                               pIp6Addr) == ADDR6_ANYCAST)
        {
            return IP6_SUCCESS;
        }
    }
    return IP6_FAILURE;
}

/******************************************************************************
 * DESCRIPTION : Checks if the given addresses matches upto the prefix len
 *
 * INPUTS      : The two address (pA1,pA2)
 *
 * OUTPUTS     : SUCCESS, if the address passed as arg1 is greater
                 FAILURE, if the address passed as arg1 is not greater
 *
 * RETURNS     : The difference bit  between the 2 addresses
 *
 * NOTES       :
 ******************************************************************************/
#ifdef __STDC__
INT4
Ip6IsAddrGreaterUptoPrefixLen (tIp6Addr * pIp6Addr1, tIp6Addr * p_ip6_addr2,
                               UINT1 u1PrefixLen)
#else
INT4
Ip6IsAddrMatch (pIp6Addr1, p_ip6_addr2, u1PrefixLen)
     tIp6Addr           *pIp6Addr1, *p_ip6_addr2;
     UINT1               u1PrefixLen;
#endif
{
    UINT1               u1I = 0;

    if ((u1PrefixLen == 0) || (u1PrefixLen > IP6_ADDR_SIZE))
    {
        u1PrefixLen = IP6_ADDR_SIZE;
    }

    while ((u1I < u1PrefixLen) &&
           (p_ip6_addr2->ip6_addr_u.u1ByteAddr[u1I] ==
            pIp6Addr1->ip6_addr_u.u1ByteAddr[u1I]))
    {
        u1I++;
    }
    if (u1I >= u1PrefixLen)
    {
        return IP6_FAILURE;
    }

    if (p_ip6_addr2->ip6_addr_u.u1ByteAddr[u1I] >
        pIp6Addr1->ip6_addr_u.u1ByteAddr[u1I])
    {
        return IP6_SUCCESS;
    }

    return IP6_FAILURE;
}

/******************************************************************************
 * DESCRIPTION : This function compares the indices of the AddressInfo node
 *                and return whether the received node is smaller or greater
 *
 * INPUTS      : The two AddressInfo nodes (pAddrInfo,pAddrInfoIn)
 *
 * OUTPUTS     : SUCCESS, if the address passed as arg1 is greater
                 FAILURE, if the address passed as arg1 is not greater
 *
 * RETURNS     : The difference bit  between the 2 addresses
 *
 * NOTES       :
 ******************************************************************************/
INT4
Ip6AddrInfoCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tIp6AddrInfo       *pAddrInfo = pRBElem1;
    tIp6AddrInfo       *pAddrInfoIn = pRBElem2;
    UINT4               u4ContextId = 0, u4ContextIdIn = 0;
    UINT1               u1PrefixLen = 0, u1PrefixLenIn = 0;
    INT4                i4RetVal = 0;

    u4ContextId = pAddrInfo->pIf6->pIp6Cxt->u4ContextId;
    u4ContextIdIn = pAddrInfoIn->pIf6->pIp6Cxt->u4ContextId;

    if (u4ContextId < u4ContextIdIn)
    {
        return IP6_RB_LESSER;
    }
    else if (u4ContextId > u4ContextIdIn)
    {
        return IP6_RB_GREATER;
    }

    i4RetVal = Ip6AddrCompare (pAddrInfo->ip6Addr, pAddrInfoIn->ip6Addr);

    if (i4RetVal == IP6_ONE)
    {
        return IP6_RB_GREATER;
    }
    else if (i4RetVal == IP6_MINUS_ONE)
    {
        return IP6_RB_LESSER;
    }

    u1PrefixLen = pAddrInfo->u1PrefLen;
    u1PrefixLenIn = pAddrInfoIn->u1PrefLen;

    if (u1PrefixLen < u1PrefixLenIn)
    {
        return IP6_RB_LESSER;
    }
    else if (u1PrefixLen > u1PrefixLenIn)
    {
        return IP6_RB_GREATER;
    }

    return IP6_RB_EQUAL;
}

/******************************************************************************
 * DESCRIPTION : This function retrives the address information added for the 
 *                given address in the given context
 *
 * INPUTS      : u4ContextId - Context Identifier
 *               Ip6Addr     - Ip6 address to be retrived
 *
 * OUTPUTS     : None
 *
 * RETURNS     : Pointer to the Address Info strucuture / NULL
 ******************************************************************************/
tIp6AddrInfo       *
Ip6GetAddressInfo (UINT4 u4ContextId, tIp6Addr Ip6Addr)
{
    tIp6AddrInfo       *pAddr6Info = NULL;
    tIp6AddrInfo        Ip6AddrInfo;
    tIp6If              TempIf6;
    tIp6Cxt            *pIp6Cxt = NULL;

    pIp6Cxt = Ipv6UtilGetCxtEntryFromCxtId (u4ContextId);
    if (pIp6Cxt == NULL)
    {
        return NULL;
    }

    MEMSET (&Ip6AddrInfo, 0, sizeof (tIp6AddrInfo));
    MEMSET (&TempIf6, 0, sizeof (tIp6If));
    Ip6AddrCopy (&Ip6AddrInfo.ip6Addr, &Ip6Addr);
    Ip6AddrInfo.pIf6 = &TempIf6;
    TempIf6.pIp6Cxt = pIp6Cxt;

    pAddr6Info = RBTreeGet (gIp6GblInfo.UnicastAddrTree,
                            (tRBElem *) & Ip6AddrInfo);

    if (pAddr6Info == NULL)
    {
        pAddr6Info = RBTreeGetNext (gIp6GblInfo.UnicastAddrTree,
                                    (tRBElem *) & Ip6AddrInfo, NULL);
    }

    while (pAddr6Info != NULL)
    {
        if ((pIp6Cxt != pAddr6Info->pIf6->pIp6Cxt) ||
            (Ip6AddrMatch (&pAddr6Info->ip6Addr, &Ip6Addr,
                           pAddr6Info->u1PrefLen) == FALSE))
        {
            break;
        }

        if (Ip6AddrMatch (&pAddr6Info->ip6Addr, &Ip6Addr,
                          IP6_ADDR_SIZE_IN_BITS) == TRUE)
        {
            return pAddr6Info;
        }
        pAddr6Info = RBTreeGetNext (gIp6GblInfo.UnicastAddrTree,
                                    pAddr6Info, NULL);
    }
    return NULL;
}

/******************************************************************************
 * DESCRIPTION : This function retrives the next address information for the 
 *                given address in the given context
 *
 * INPUTS      : u4ContextId - Context Identifier
 *               Ip6Addr     - Ip6 address to be retrived
 *
 * OUTPUTS     : None
 *
 * RETURNS     : Pointer to the Address Info strucuture / NULL
 ******************************************************************************/
tIp6AddrInfo       *
Ip6GetNextAddressInfo (UINT4 u4ContextId, tIp6Addr Ip6Addr)
{
    tIp6AddrInfo       *pAddr6Info = NULL;
    tIp6AddrInfo        Ip6AddrInfo;
    tIp6If              TempIf6;
    tIp6Cxt            *pIp6Cxt = NULL;

    pIp6Cxt = Ipv6UtilGetCxtEntryFromCxtId (u4ContextId);
    if (pIp6Cxt == NULL)
    {
        return NULL;
    }

    MEMSET (&Ip6AddrInfo, 0, sizeof (tIp6AddrInfo));
    MEMSET (&TempIf6, 0, sizeof (tIp6If));
    Ip6AddrCopy (&Ip6AddrInfo.ip6Addr, &Ip6Addr);
    Ip6AddrInfo.pIf6 = &TempIf6;
    TempIf6.pIp6Cxt = pIp6Cxt;

    pAddr6Info = RBTreeGetNext (gIp6GblInfo.UnicastAddrTree,
                                (tRBElem *) & Ip6AddrInfo, Ip6AddrInfoMibCmp);

    return pAddr6Info;
}

/***********************************************************************
 *  Function Name    :   Ipv6FormSrcAddrList
 *
 *  Description      :   This function forms the source address list
 *    
 *  Inputs           :   pSrcAddrlist - Source address list pointer
 *      
 *  Outputs          :   pSrcAddrList - Source address list after 
 *                       gathering the source addresses
 *        
 *  Return Value     :    NETIPV6_SUCCESS
 *
 ************************************************************************/

INT4
Ipv6FormSrcAddrList (tTMO_SLL * pSrcAddrlist, tIp6If * pIf6)
{
    tIp6If             *pIf6OnLink = NULL;
    tIp6If             *pIf6Out = NULL;
    tIp6If             *pIf6ActiveIf = NULL;
    tIp6AddrInfo       *pAddr = NULL;
    tIp6LlocalInfo     *pLladdr = NULL;
    tTMO_SLL_NODE      *pNode = NULL;
    tIp6SrcAddr        *pSrcAddrInfo = NULL;
    UINT4               u4Index = 0;
    UINT4               u4OnLinkIfIndex = 0;
    UINT1               u1Counter = 0;
    UINT4               u4Array[IPV6_MAX_IF_OVER_LINK];
    /*Initializing the source address list */
    TMO_SLL_Init (pSrcAddrlist);

    /*Scan the lla6Ilist to form the candidate set
       of source addresses and copy the
       addresses into a new list source address list */
    u4Array[0] = pIf6->u4Index;

    if (pIf6->u2OnlinkIfCount > 0)
    {
        pIf6ActiveIf = IP6_INTERFACE (pIf6->u4OnLinkActiveIfId);

        if (pIf6ActiveIf != NULL)
        {
            for (u4Index = 0; ((u4Index < pIf6->u2OnlinkIfCount) &&
                               (u1Counter < IPV6_MAX_IF_OVER_LINK)); u4Index++)
            {
                u4OnLinkIfIndex = pIf6ActiveIf->au1OnLinkIfaces[u4Index];
                pIf6OnLink = IP6_INTERFACE (u4OnLinkIfIndex);
                if (pIf6OnLink != NULL)
                {
                    u4Array[u1Counter] = pIf6OnLink->u4Index;
                    u1Counter++;
                }
            }
        }
    }
    u4Index = 0;
    do
    {
        pIf6Out = IP6_INTERFACE (u4Array[u4Index]);
        TMO_SLL_Scan (&pIf6Out->lla6Ilist, pNode, tTMO_SLL_NODE *)
        {
            pLladdr = IP6_LLADDR_PTR_FROM_SLL (pNode);
            pSrcAddrInfo =
                (tIp6SrcAddr *) (VOID *) Ip6GetMem (IP6_DEFAULT_CONTEXT,
                                                    (UINT2) gIp6GblInfo.
                                                    i4Ip6SrcAddrListId);
            if (pSrcAddrInfo != NULL)
            {
                MEMCPY (&(pSrcAddrInfo->srcaddr),
                        &(pLladdr->ip6Addr), sizeof (tIp6Addr));

                pSrcAddrInfo->u4IfIndex = pIf6Out->u4Index;
                pSrcAddrInfo->u1CumulativeScore = 0;
                TMO_SLL_Add (pSrcAddrlist, &(pSrcAddrInfo->SrcNode));
            }
        }
        TMO_SLL_Scan (&pIf6Out->addr6Ilist, pNode, tTMO_SLL_NODE *)
        {
            pAddr = IP6_ADDR_PTR_FROM_SLL (pNode);

            if ((pAddr->u1AddrType == ADDR6_ANYCAST)
                || (pAddr->u1AddrType == ADDR6_MULTI)
                || (pAddr->u1AddrType == ADDR6_LOOPBACK)
                || (pAddr->u1AddrType == ADDR6_UNSPECIFIED))
            {
                continue;
            }
            pSrcAddrInfo =
                (tIp6SrcAddr *) (VOID *) Ip6GetMem (IP6_DEFAULT_CONTEXT,
                                                    (UINT2) gIp6GblInfo.
                                                    i4Ip6SrcAddrListId);

            if (pSrcAddrInfo != NULL)
            {
                MEMCPY (&(pSrcAddrInfo->srcaddr),
                        &(pAddr->ip6Addr), sizeof (tIp6Addr));

                pSrcAddrInfo->u4IfIndex = pIf6Out->u4Index;
                pSrcAddrInfo->u1CumulativeScore = 0;
                TMO_SLL_Add (pSrcAddrlist, &(pSrcAddrInfo->SrcNode));
            }

        }
        u4Index++;
    }
    while (u4Index < pIf6->u2OnlinkIfCount);

    return IP6_SUCCESS;

}

/***********************************************************************
 *    Function Name    :   Ipv6SortSrcAddrList
 *    Description      :   This function sorts the source address list
 *    
 *    Inputs           :   psrcaddrlist - Source address list pointer
 *      
 *    Outputs          :   psortedlist  - Sorted list pointer
 *      
 *    Return Value     :    NETIPV6_SUCCESS
 * ***********************************************************************/

VOID
Ipv6SortSrcAddrList (tTMO_SLL * pSrcaddrlist)
{
    UINT1               u1Count = 0;
    UINT1               u1Index;
    tIp6SrcAddr        *pCurNode = NULL;
    tIp6SrcAddr        *pNextNode = NULL;
    tIp6SrcAddr        *pPrevNode = NULL;

    u1Count = (UINT1) TMO_SLL_Count (pSrcaddrlist);
    if (u1Count == 1)
    {
        return;
    }

    for (u1Index = 0; u1Index < u1Count; u1Index++)
    {
        TMO_DYN_SLL_Scan (pSrcaddrlist, pCurNode, pNextNode, tIp6SrcAddr *)
        {

            if (pNextNode == NULL)
            {
                break;
            }
            if (pCurNode->u1CumulativeScore < pNextNode->u1CumulativeScore)
            {
                /*To handle the case of first two nodes-first iteration */
                if ((pSrcaddrlist->Head.pNext == &(pCurNode->SrcNode)) ||
                    (pPrevNode == NULL))
                {
                    pSrcaddrlist->Head.pNext = &(pNextNode->SrcNode);
                }
                else
                {

                    pPrevNode->SrcNode.pNext = &(pNextNode->SrcNode);
                }
                pCurNode->SrcNode.pNext = pNextNode->SrcNode.pNext;
                pNextNode->SrcNode.pNext = &(pCurNode->SrcNode);

                if (pCurNode->SrcNode.pNext == &(pSrcaddrlist->Head))
                {
                    pSrcaddrlist->Tail = &(pNextNode->SrcNode);
                }

                pPrevNode = pNextNode;
            }
            else
            {
                pPrevNode = pCurNode;
                continue;
            }
        }
    }
    return;
}

/**************************************************************************
 *    Function Name    :   Ip6IsAddrEqual
 *    Description      :   This function is used to find whether the given
 *                         addresses are equal
 *     
 *    Inputs           :    Addr1            - IPv6 address
 *                          Addr2            - IPv6 address
 *    Outputs          :    None
 *    Return Value     :    NETIPV6_SUCCESS  - If both the addresses are equal
 *                                             
 *                          NETIPV6_FAILURE  - If both the addresses are not
 *                                             equal
 *                                             
 * *************************************************************************/

INT1
Ip6AddrEqual (tIp6Addr Addr1, tIp6Addr Addr2)
{

    if (((Addr1.ip6_addr_u.u4WordAddr[0] ^ Addr2.ip6_addr_u.u4WordAddr[0]) |
         (Addr1.ip6_addr_u.u4WordAddr[1] ^ Addr2.ip6_addr_u.u4WordAddr[1]) |
         (Addr1.ip6_addr_u.u4WordAddr[2] ^ Addr2.ip6_addr_u.u4WordAddr[2]) |
         (Addr1.ip6_addr_u.u4WordAddr[3] ^ Addr2.ip6_addr_u.u4WordAddr[3]))
        == 0)
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

/***********************************************************************
 *   Function Name    :   Ipv6SortDstAddrList
 *    
 *   Description      :   This function sorts the source address list
 *      
 *   Inputs           :   psrcaddrlist - Source address list pointer
 *        
 *   Outputs          :   psortedlist  - Sorted list pointer
 *          
 *   Return Value     :
 * ********************************************************************/

VOID
Ipv6SortDstAddrList (tTMO_SLL * pDstaddrlist)
{

    UINT1               u1Count = 0;
    UINT1               u1Index;
    tIp6DstAddr        *pCurNode = NULL;
    tIp6DstAddr        *pNextNode = NULL;
    tIp6DstAddr        *pPrevNode = NULL;

    u1Count = (UINT1) TMO_SLL_Count (pDstaddrlist);
    if (u1Count == 1)
    {
        return;
    }

    for (u1Index = 0; u1Index < u1Count; u1Index++)
    {

        TMO_DYN_SLL_Scan (pDstaddrlist, pCurNode, pNextNode, tIp6DstAddr *)
        {
            if (pNextNode == NULL)
            {
                break;
            }
            if (pCurNode->u1CumulativeScore < pNextNode->u1CumulativeScore)
            {
                /*To handle the case of first two nodes - first iteration */
                if ((pDstaddrlist->Head.pNext == &(pCurNode->DstNode)) ||
                    (pPrevNode == NULL))
                {
                    pDstaddrlist->Head.pNext = &(pNextNode->DstNode);
                }
                else
                {
                    pPrevNode->DstNode.pNext = &(pNextNode->DstNode);
                }
                pCurNode->DstNode.pNext = pNextNode->DstNode.pNext;
                pNextNode->DstNode.pNext = &(pCurNode->DstNode);
                if (pCurNode->DstNode.pNext == &(pDstaddrlist->Head))
                {
                    pDstaddrlist->Tail = &(pNextNode->DstNode);
                }
                pPrevNode = pNextNode;
            }
            else
            {
                pPrevNode = pCurNode;
                continue;
            }
        }
    }
    return;
}

/**************************************************************************
 * DESCRIPTION : This function can be used by external modules to get the
 *               source address corresponding to the destination address
 *               that will be used while sending the packet.
 *
 * INPUTS      : The IPv6 destination addr ptr  (pDstAddr) and
 *               IPv6 source addr ptr (pSrcAddr),
 *
 * OUTPUTS     : None
 *
 * RETURNS     : IP6_SUCCESS / IP6_FAILURE
 *
 * NOTES       :
 ******************************************************************************/
INT4
Ip6IsOurAddr (tIp6Addr * pIp6Addr, UINT4 *pu4Index)
{
    return (Ip6IsOurAddrInCxt (IP6_DEFAULT_CONTEXT, pIp6Addr, pu4Index));
}

INT4
Ip6AddrInfoMibCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tIp6AddrInfo       *pAddrInfo = pRBElem1;
    tIp6AddrInfo       *pAddrInfoIn = pRBElem2;
    UINT4               u4ContextId = 0, u4ContextIdIn = 0;
    INT4                i4RetVal = 0;

    u4ContextId = pAddrInfo->pIf6->pIp6Cxt->u4ContextId;
    u4ContextIdIn = pAddrInfoIn->pIf6->pIp6Cxt->u4ContextId;

    if (u4ContextId < u4ContextIdIn)
    {
        return IP6_RB_LESSER;
    }
    else if (u4ContextId > u4ContextIdIn)
    {
        return IP6_RB_GREATER;
    }

    i4RetVal = Ip6AddrCompare (pAddrInfo->ip6Addr, pAddrInfoIn->ip6Addr);

    if (i4RetVal == IP6_ONE)
    {
        return IP6_RB_GREATER;
    }
    else if (i4RetVal == IP6_MINUS_ONE)
    {
        return IP6_RB_LESSER;
    }

    return IP6_RB_EQUAL;
}

/******************************************************************************
 * DESCRIPTION : This function deletes the specified MAC address from MAC Filter
 *               table for the given interface.
 *
 * INPUTS      : Interface index , MAC address.
 *
 * OUTPUTS     : None.
 * 
 * RETURNS     : IP6_SUCCESS/IP6_FAILURE
 *******************************************************************************/
INT4
Ip6DeleteMACAddrInMACFilter (UINT4 u4Index, UINT1 *pMacAddr)
{
    tIp6MacFilterNode  *pNode = NULL;
    UINT1               au1MacAddr[IP6_MAX_ENET_ADDR_LEN];
    UINT4               u4Ind = 0;
    UINT4               u4ContextId = 0;

    for (u4Ind = 0; u4Ind < (UINT4) IP6_MAX_FILTER; u4Ind++)
    {
        if (gIp6GblInfo.aIp6MACFilter[u4Ind].u4Index == u4Index)
        {
            break;
        }
    }

    if (u4Ind == (UINT4) IP6_MAX_FILTER)
    {
        return IP6_FAILURE;
    }

    MEMCPY (au1MacAddr, pMacAddr, IP6_MAX_ENET_ADDR_LEN);
    /* Find the matching node */
    TMO_SLL_Scan (&gIp6GblInfo.aIp6MACFilter[u4Ind].MacAddrList, pNode,
                  tIp6MacFilterNode *)
    {
        if (MEMCMP (au1MacAddr, pNode->u1MacAddr, MAC_ADDR_LEN) == 0)
        {
            /* Match found. */
            break;
        }
    }

    if (pNode == NULL)
    {
        /* No matching entry */
        return IP6_FAILURE;
    }

    /* Delete the node from the list. */
    TMO_SLL_Delete (&gIp6GblInfo.aIp6MACFilter[u4Ind].MacAddrList,
                    &(pNode->NextNode));

    Ip6GetCxtId (u4Index, &u4ContextId);
#ifdef NPAPI_WANTED
    Ipv6FsNpIpv6DelMcastMAC (u4ContextId, u4Index,
                             pNode->u1MacAddr, IP6_MAX_ENET_ADDR_LEN);
#endif /* NPAPI_WANTED */

    /* Free the Node */
    Ip6RelMem (u4ContextId, (UINT2) gIp6GblInfo.i4MacFilterId, (UINT1 *) pNode);
    if (TMO_SLL_Count (&gIp6GblInfo.aIp6MACFilter[u4Ind].MacAddrList) == 0)
    {
        /* MAC Filter list is empty. Initialize the list. */
        gIp6GblInfo.aIp6MACFilter[u4Index].u4Index = IP6_INVALID_IF_INDEX;
        TMO_SLL_Init (&gIp6GblInfo.aIp6MACFilter[u4Index].MacAddrList);
    }
    return IP6_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : Called when an more specific route information in the RA message 
 *               is to be created 
 *
 * INPUTS      : Interface index, 
 *               pointer to Prefix address, 
 *               Prefix Length,
 *               Route Preference,
 *               Route Lifetime
 *
 * OUTPUTS     : None
 *
 * RETURNS     : Create RARoute Info or NULL
 *
 * NOTES       : None
 ******************************************************************************/
tIp6RARouteInfoNode *
Ip6RARouteInfoCreate (UINT4 u4IfIndex, tIp6Addr * pRARouteInfoPrefix,
                      INT4 i4PrefixLen, INT4 i4RowStatus)
{
    tIp6RARouteInfoNode *pRARouteInfo = NULL;
    tIp6If             *pIf6 = NULL;
    UINT4               u4RoutInfoCount = 0;
    pIf6 = Ip6ifGetEntry (u4IfIndex);
    if (pIf6 == NULL)
    {
        IP6_GBL_TRC_ARG2 (IP6_MOD_TRC, CONTROL_PLANE_TRC, IP6_NAME,
                          "IP6RAROUTEINFO: Ip6RARouteInfoCreate: IF pointer is NULL %s gen err: Type = %d\n",
                          ERROR_FATAL_STR, ERR_GEN_NULL_PTR);
        return NULL;
    }

    if (RB_SUCCESS !=
        (RBTreeCount (pIf6->Ip6RARouteInfoTree, &u4RoutInfoCount)))
    {
        IP6_TRC_ARG (pIf6->pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                     CONTROL_PLANE_TRC, IP6_NAME,
                     "IP6RAROUTEINFO: Ip6RARouteInfoCreate: Get RARouteInfo count Failed\n");
        return NULL;
    }

    if (u4RoutInfoCount >= IP6_RA_MAX_RIO_PER_INT)
    {
        IP6_TRC_ARG (pIf6->pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                     CONTROL_PLANE_TRC, IP6_NAME,
                     "IP6RAROUTEINFO: Ip6RARouteInfoCreate: Maximum RARouteInfo count reached\n");
        return NULL;
    }

    pRARouteInfo = (tIp6RARouteInfoNode *) (VOID *)
        Ip6GetMem (pIf6->pIp6Cxt->u4ContextId,
                   (UINT2) gIp6GblInfo.i4Ip6RARouteInfoPoolId);

    if (pRARouteInfo == NULL)
    {
        IP6_TRC_ARG2 (pIf6->pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                      CONTROL_PLANE_TRC, IP6_NAME,
                      "IP6RAROUTEINFO: Ip6RARouteInfoCreate: "
                      "RARouteInfo pointer is NULL %s gen err: Type = %d\n",
                      ERROR_FATAL_STR, ERR_GEN_NULL_PTR);
        return NULL;
    }

    MEMSET (pRARouteInfo, 0, sizeof (tIp6RARouteInfoNode));

    Ip6CopyAddrBits (&(pRARouteInfo->Ip6RARoutePrefix), pRARouteInfoPrefix,
                     i4PrefixLen);
    pRARouteInfo->i4RARoutePrefixLen = i4PrefixLen;
    pRARouteInfo->i4RARouteRowStatus = i4RowStatus;
    pRARouteInfo->i4RARouteIfIndex = (INT4) u4IfIndex;
    pRARouteInfo->i4RARoutePref = IP6_RA_ROUTE_PREF_MED;
    pRARouteInfo->u4RARouteLifetime = IP6_RA_ROUTE_MAX_LIFETIME;

    if (RB_FAILURE ==
        RBTreeAdd (pIf6->Ip6RARouteInfoTree, (tRBElem *) pRARouteInfo))
    {
        return NULL;
    }
    return pRARouteInfo;
}

/******************************************************************************
 * DESCRIPTION : Called when an more specific route information in the RA message
 *               is to be deleted 
 *
 * INPUTS      : Interface index,
 *               pointer to Prefix address,
 *               Prefix Length,
 *               Route Preference,
 *               Route Lifetime
 *
 * OUTPUTS     : None
 *
 * RETURNS     : IP6_SUCCESS or IP6_FAILURE
 *
 * NOTES       : None
 ******************************************************************************/
INT4
IP6DelRARouteInfo (UINT4 u4IfIndex, tIp6Addr * pRARouteInfoPrefix,
                   INT4 i4PrefixLen)
{
    tIp6RARouteInfoNode Ip6RARouteInfo;
    tIp6RARouteInfoNode *pIp6RARouteInfo = NULL;
    tIp6If             *pIf6 = NULL;

    MEMSET (&Ip6RARouteInfo, 0, sizeof (tIp6RARouteInfoNode));

    pIf6 = Ip6ifGetEntry (u4IfIndex);
    if (pIf6 == NULL)
    {
        IP6_GBL_TRC_ARG2 (IP6_MOD_TRC, CONTROL_PLANE_TRC, IP6_NAME,
                          "IP6RAROUTEINFO: IP6DelRARouteInfo: IF pointer is NULL %s gen err: Type = %d\n",
                          ERROR_FATAL_STR, ERR_GEN_NULL_PTR);
        return IP6_FAILURE;
    }
    Ip6CopyAddrBits (&(Ip6RARouteInfo.Ip6RARoutePrefix), pRARouteInfoPrefix,
                     i4PrefixLen);
    Ip6RARouteInfo.i4RARoutePrefixLen = i4PrefixLen;
    Ip6RARouteInfo.i4RARouteIfIndex = (INT4) u4IfIndex;

    pIp6RARouteInfo =
        (tIp6RARouteInfoNode *) RBTreeRem (pIf6->Ip6RARouteInfoTree,
                                           (tRBElem *) & Ip6RARouteInfo);
    if (pIp6RARouteInfo != NULL)
    {
        Ip6RelMem (pIf6->pIp6Cxt->u4ContextId,
                   (UINT2) gIp6GblInfo.i4Ip6RARouteInfoPoolId,
                   (UINT1 *) pIp6RARouteInfo);
        return IP6_SUCCESS;
    }
    IP6_TRC_ARG2 (pIf6->pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                  CONTROL_PLANE_TRC, IP6_NAME,
                  "IP6RAROUTEINFO: IP6DelRARouteInfo: "
                  "RARouteInfo pointer is NULL %s gen err: Type = %d\n",
                  ERROR_FATAL_STR, ERR_GEN_NULL_PTR);
    return IP6_FAILURE;
}

/******************************************************************************
 * DESCRIPTION : This function compares the indices of the AddressInfo node
 *                and return whether the received node is smaller or greater
 *
 * INPUTS      : The two AddressInfo nodes (pAddrInfo,pAddrInfoIn)
 *
 * OUTPUTS     : SUCCESS, if the address passed as arg1 is greater
                 FAILURE, if the address passed as arg1 is not greater
 *
 * RETURNS     : The difference bit  between the 2 addresses
 *
 * NOTES       :
 ******************************************************************************/
INT4
Ip6RARouteInfoCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tIp6RARouteInfoNode *pRARouteInfo = pRBElem1;
    tIp6RARouteInfoNode *pRARouteInfoIn = pRBElem2;
    INT4                i4PrefixLen = 0, i4PrefixLenIn = 0;
    INT4                i4RetVal = 0;

    i4RetVal = Ip6AddrCompare (pRARouteInfo->Ip6RARoutePrefix,
                               pRARouteInfoIn->Ip6RARoutePrefix);

    if (i4RetVal == IP6_ONE)
    {
        return IP6_RB_GREATER;
    }
    else if (i4RetVal == IP6_MINUS_ONE)
    {
        return IP6_RB_LESSER;
    }

    i4PrefixLen = pRARouteInfo->i4RARoutePrefixLen;
    i4PrefixLenIn = pRARouteInfoIn->i4RARoutePrefixLen;

    if (i4PrefixLen < i4PrefixLenIn)
    {
        return IP6_RB_LESSER;
    }
    else if (i4PrefixLen > i4PrefixLenIn)
    {
        return IP6_RB_GREATER;
    }

    return IP6_RB_EQUAL;
}

/**************************************************************************
 ** DESCRIPTION : Called to get the RA Route Information entry from
 **               the Route Information tree
 **
 ** INPUTS      : Interface index, pointer to Prefix address, Prefix Length
 **
 ** OUTPUTS     : None
 **
 ** RETURNS     : RA Route Info if present or NULL
 **
 ** NOTES       :
 ************************************************************************/

tIp6RARouteInfoNode *
Ip6GetRARoutInfo (UINT4 u4IfIndex, tIp6Addr * pRARoutePrefix, INT4 i4PrefixLen)
{
    tIp6If             *pIf6 = NULL;
    tIp6RARouteInfoNode *pIp6RARouteInfo = NULL;
    tIp6RARouteInfoNode Ip6RARouteInfo;

    pIf6 = Ip6ifGetEntry (u4IfIndex);
    if (pIf6 == NULL)
    {
        IP6_GBL_TRC_ARG2 (IP6_MOD_TRC, CONTROL_PLANE_TRC, IP6_NAME,
                          "IP6RAROUTEINFO: Ip6GetRARoutInfo: IF pointer is NULL %s gen err: Type = %d\n",
                          ERROR_FATAL_STR, ERR_GEN_NULL_PTR);
        return NULL;
    }

    MEMSET (&Ip6RARouteInfo, 0, sizeof (tIp6RARouteInfoNode));
    Ip6CopyAddrBits (&(Ip6RARouteInfo.Ip6RARoutePrefix), pRARoutePrefix,
                     i4PrefixLen);
    Ip6RARouteInfo.i4RARoutePrefixLen = i4PrefixLen;
    Ip6RARouteInfo.i4RARouteIfIndex = (INT4) u4IfIndex;

    pIp6RARouteInfo = RBTreeGet (pIf6->Ip6RARouteInfoTree,
                                 (tRBElem *) & Ip6RARouteInfo);
    if (pIp6RARouteInfo != NULL)
    {
        return pIp6RARouteInfo;
    }
    IP6_TRC_ARG2 (pIf6->pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                  CONTROL_PLANE_TRC, IP6_NAME,
                  "IP6RAROUTEINFO: Ip6GetRARoutInfo: "
                  "RARouteInfo pointer is NULL %s gen err: Type = %d\n",
                  ERROR_FATAL_STR, ERR_GEN_NULL_PTR);
    return NULL;
}

/**********************************************************************

 * DESCRIPTION : Called to fetch the first prefix
 **               from the RA Route information tree 
 **
 ** INPUTS      : Interface index
 **
 ** OUTPUTS     : None
 **
 ** RETURNS     : RA Route Info if present or NULL
 **
 ** NOTES        :
 ** *********************************************************************/

tIp6RARouteInfoNode *
Ip6GetFirstRARouteInfo (UINT4 u4IfIndex)
{
    tIp6RARouteInfoNode *pRARouteInfo = NULL;
    tIp6If             *pIf6 = NULL;

    pIf6 = Ip6ifGetEntry (u4IfIndex);
    if (pIf6 == NULL)
    {
        IP6_GBL_TRC_ARG2 (IP6_MOD_TRC, CONTROL_PLANE_TRC, IP6_NAME,
                          "IP6RAROUTEINFO: Ip6GetFirstRARouteInfo: IF pointer is NULL %s gen err: Type = %d\n",
                          ERROR_FATAL_STR, ERR_GEN_NULL_PTR);
        return NULL;
    }

    pRARouteInfo = (tIp6RARouteInfoNode *)
        RBTreeGetFirst (pIf6->Ip6RARouteInfoTree);
    if (pRARouteInfo == NULL)
    {
        IP6_TRC_ARG2 (pIf6->pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                      CONTROL_PLANE_TRC, IP6_NAME,
                      "IP6RAROUTEINFO: Ip6GetFirstRARouteInfo: "
                      "RARouteInfo pointer is NULL %s gen err: Type = %d\n",
                      ERROR_FATAL_STR, ERR_GEN_NULL_PTR);
        return NULL;
    }
    return pRARouteInfo;
}

/************************************************************************
 *
 ** DESCRIPTION : Called to fetch the next RA Route Information entry from
 **               the policy prefix tree
 **
 ** INPUTS      : Interface index, pointer to Prefix address, Prefix Length
 **
 ** OUTPUTS     : None
 **
 ** RETURNS     : RA Route Info if present or NULL
 **
 ** NOTES       :
 ** **********************************************************************/

tIp6RARouteInfoNode *
Ip6GetNextRARouteInfo (UINT4 u4IfIndex, tIp6Addr * pRARouteInfoPrefix,
                       INT4 i4PrefixLen)
{

    tIp6RARouteInfoNode *pIp6NextRARouteInfo = NULL;
    tIp6RARouteInfoNode Ip6RARouteInfo;
    tIp6If             *pIf6 = NULL;

    MEMSET (&Ip6RARouteInfo, 0, sizeof (Ip6RARouteInfo));
    pIf6 = Ip6ifGetEntry (u4IfIndex);
    if (pIf6 == NULL)
    {
        IP6_GBL_TRC_ARG2 (IP6_MOD_TRC, CONTROL_PLANE_TRC, IP6_NAME,
                          "IP6RAROUTEINFO: Ip6GetNextRARouteInfo: IF pointer is NULL %s gen err: Type = %d\n",
                          ERROR_FATAL_STR, ERR_GEN_NULL_PTR);
        return NULL;
    }

    Ip6CopyAddrBits (&(Ip6RARouteInfo.Ip6RARoutePrefix), pRARouteInfoPrefix,
                     i4PrefixLen);
    Ip6RARouteInfo.i4RARoutePrefixLen = i4PrefixLen;
    Ip6RARouteInfo.i4RARouteIfIndex = (INT4) u4IfIndex;

    pIp6NextRARouteInfo = RBTreeGetNext (pIf6->Ip6RARouteInfoTree,
                                         (tRBElem *) & Ip6RARouteInfo,
                                         Ip6RARouteInfoCmp);
    if (pIp6NextRARouteInfo != NULL)
    {
        return pIp6NextRARouteInfo;
    }
    IP6_TRC_ARG2 (pIf6->pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                  CONTROL_PLANE_TRC, IP6_NAME,
                  "IP6RAROUTEINFO: Ip6GetNextRARouteInfo: "
                  "RARouteInfo pointer is NULL %s gen err: Type = %d\n",
                  ERROR_FATAL_STR, ERR_GEN_NULL_PTR);
    return NULL;
}

/****************************************************************************
 *    FUNCTION NAME    : Ip6UtlRBFreeAgentToRARouteTable 
 *    DESCRIPTION      : Frees the memory pool for application RB Tree
 *                       Ip6RARouteInfoTree 
 *    INPUT            : pRBElem - RB Tree Pointer
 *    OUTPUT           : None
 *    RETURNS          :
 ****************************************************************************/
INT4
Ip6UtlRBFreeAgentToRARouteTable (tRBElem * pRBElem, UINT4 u4Arg)
{
    UNUSED_PARAM (u4Arg);
    if (pRBElem != NULL)
    {
        MemReleaseMemBlock (gIp6GblInfo.i4Ip6RARouteInfoPoolId,
                            (UINT1 *) pRBElem);
        pRBElem = NULL;

    }
    return OSIX_SUCCESS;
}

/**************************************************************************
 * DESCRIPTION : This function can be used to check the whether the target
 *               address is available on the given interface.
 *
 * INPUTS      : The IPv6 target addr ptr (pTargetAddr) and
 *               IPv6 interface ptr (pIf6), flag telling
 *               whether to check in the link-local address list
 *
 * OUTPUTS     : None
 *
 * RETURNS     : IP6_SUCCESS / IP6_FAILURE
 *
 * NOTES       :
 ******************************************************************************/
INT4
Ip6IsAddrOnInterface (UINT1 *pu1Type, UINT1 u1LlocalChk, tIp6Addr * pTargetaddr,
                      tIp6If * pIf6)
{
    tIp6LlocalInfo     *pCurrLlocal = NULL;
    tIp6AddrInfo       *pCurrAddrInfo = NULL;
    tTMO_SLL_NODE      *p_sll_llocal = NULL;
    tTMO_SLL_NODE      *p_sll_unicast = NULL;

    if ((*pu1Type == ADDR6_UNICAST) || (*pu1Type == ADDR6_ANYCAST))
    {
        TMO_SLL_Scan (&pIf6->addr6Ilist, p_sll_unicast, tTMO_SLL_NODE *)
        {
            pCurrAddrInfo = IP6_ADDR_PTR_FROM_SLL (p_sll_unicast);

            if ((pCurrAddrInfo->u1Status & ADDR6_UP) &&
                (!(pCurrAddrInfo->u1Status & ADDR6_FAILED)))
            {
                if (MEMCMP (pTargetaddr, &pCurrAddrInfo->ip6Addr,
                            sizeof (tIp6Addr)) == 0)
                {
                    return IP6_SUCCESS;
                }
            }
        }
        return IP6_FAILURE;
    }

    else if (*pu1Type == ADDR6_LLOCAL)
    {
        if (u1LlocalChk)
        {
            /*
             * search the list of link-local addresses on the interface
             */

            TMO_SLL_Scan (&pIf6->lla6Ilist, p_sll_llocal, tTMO_SLL_NODE *)
            {
                pCurrLlocal = IP6_LLADDR_PTR_FROM_SLL (p_sll_llocal);

                if ((pCurrLlocal->u1Status & ADDR6_UP) &&
                    (!(pCurrLlocal->u1Status & ADDR6_FAILED)))
                {
                    if (MEMCMP (pTargetaddr, &pCurrLlocal->ip6Addr,
                                sizeof (tIp6Addr)) == 0)
                    {
                        return IP6_SUCCESS;
                    }
                }
            }
            return IP6_FAILURE;
        }
    }
    return IP6_FAILURE;
}

/***************************** END OF FILE **********************************/
