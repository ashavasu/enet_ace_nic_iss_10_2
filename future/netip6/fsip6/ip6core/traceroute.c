/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: traceroute.c,v 1.7 2014/07/04 10:17:47 siva Exp $
 *
 * Description: TraceRoute Application for v4 and v6.
 *
 *******************************************************************/

#include "traceroute.h"

UINT1               gfstraceaddr[128];
INT1                gIpVersion = 0;

VOID
TraceRoute (INT1 i1ipver, UINT1 *ip6addr)
{
    tOsixTaskId         TaskId;

    MEMCPY (gfstraceaddr, ip6addr, 16);

    if ((i1ipver == TR_V4) || (i1ipver == TR_V6))
        gIpVersion = i1ipver;
    else
    {
        TR_LOG ("Invalid ip version\n");
        return;
    }
    if (gIpVersion == TR_V6)
    {
        OsixCreateTask (TR_TASK_NAME,
                        TR_TASK_PRIORITY,
                        TR_TASK_STACK_SIZE,
                        TraceRoute6, NULL, OSIX_DEFAULT_TASK_MODE, &TaskId);
    }
    else
    {
        OsixCreateTask (TR_TASK_NAME,
                        TR_TASK_PRIORITY,
                        TR_TASK_STACK_SIZE,
                        TraceRoute4, NULL, OSIX_DEFAULT_TASK_MODE, &TaskId);

    }
    return;
}

VOID
TraceRouteDelete ()
{
    OsixDeleteTask (SELF, TR_TASK_NAME);
    return;
}

/****************************************************************************
 Function    :  FsTraceRoute6
 Input       :  Destination address.
 Output      :  None.
 Description :  This function intiates a trace route functionality to the 
                v6 destination specified in the input argument.
 Returns     :  None.
****************************************************************************/
VOID
TraceRoute6 (INT1 *pDummy)
{
    INT4                i4SockSend = -1, i4SockRecv = -1;
    struct sockaddr_in6 bindaddr;
    tpktinfo            packetopt;

    UNUSED_PARAM (pDummy);

    /* UDP socket created for sending packets */
    i4SockSend = socket (AF_INET6, SOCK_DGRAM, IPPROTO_UDP);
    if (i4SockSend < 0)
    {
        TR_LOG ("Error in creating UDP socket\n");
        TraceRouteDelete ();
        return;
    }

    MEMSET (&bindaddr, 0, sizeof (struct sockaddr_in6));
    bindaddr.sin6_family = AF_INET6;
    bindaddr.sin6_port = OSIX_HTONS (8000);
    inet_pton (AF_INET6, (const CHR1 *) TR_INADDR_ANY,
               &bindaddr.sin6_addr.s6_addr);

    if (bind (i4SockSend, (struct sockaddr *) &bindaddr,
              sizeof (struct sockaddr_in6)) < 0)
    {
        TR_LOG ("Binding of socket failed\n");
        close (i4SockSend);
        TraceRouteDelete ();
        return;
    }

    /* RAW Socket for receving packets */
    i4SockRecv = socket (AF_INET6, SOCK_RAW, IPPROTO_ICMPV6);
    if (i4SockRecv < 0)
    {
        TR_LOG ("Error in creating RAW socket\n");
        close (i4SockSend);
        TraceRouteDelete ();
        return;
    }
    /* Setting the socket to non-blocking mode */
    if (fcntl (i4SockRecv, F_SETFL, O_NONBLOCK) < 0)
    {
        TR_LOG ("Error in setting non-blocking mode!!!\n");
        close (i4SockSend);
        close (i4SockRecv);
        TraceRouteDelete ();
        return;
    }
    packetopt.u1MaxHopLmts = TR_DEF_MAX_HOP_LMT;
    packetopt.i4CurrHops = TR_DEF_INITIAL_HOP_LMT;
    packetopt.u2Seq = TR_INITIAL_PORT;
    packetopt.tv.tv_sec = TR_DEF_TIME_TO_WAIT;
    packetopt.tv.tv_usec = 0;

    MEMCPY (&packetopt.ip6Addr, gfstraceaddr, IP6_ADDR_SIZE);

    TraceRouteProcess (i4SockSend, i4SockRecv, &packetopt);
    TraceRouteDelete ();
    return;

}

/****************************************************************************
 Function    :  FsTraceRoute4
 Input       :  Destination address.
 Output      :  None.
 Description :  This function intiates a trace route functionality to the 
                v4 destination specified in the input argument.
 Returns     :  None.
****************************************************************************/
VOID
TraceRoute4 (INT1 *pDummy)
{
    INT4                i4SockSend = -1, i4SockRecv = -1;
    struct sockaddr_in  bindaddr;
    tpktinfo            packetopt;
    UINT4               v4addr;

    UNUSED_PARAM (pDummy);

    /* UDP socket created for sending packets */
    i4SockSend = socket (AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if (i4SockSend < 0)
    {
        TR_LOG ("Error in creating UDP socket\n");
        TraceRouteDelete ();
        return;
    }

    MEMSET (&bindaddr, 0, sizeof (struct sockaddr_in));
    bindaddr.sin_family = AF_INET;
    bindaddr.sin_port = OSIX_HTONS (8000);

    if (bind (i4SockSend, (struct sockaddr *) &bindaddr,
              sizeof (struct sockaddr_in)) < 0)
    {
        TR_LOG ("Binding of socket failed\n");
        close (i4SockSend);
        TraceRouteDelete ();
        return;
    }

    /* RAW Socket for receving packets */
    i4SockRecv = socket (AF_INET, SOCK_RAW, IPPROTO_ICMP);
    if (i4SockRecv < 0)
    {
        TR_LOG ("Error in creating RAW socket\n");
        close (i4SockSend);
        TraceRouteDelete ();
        return;
    }
    /* Setting the socket to non-blocking mode */
    if (fcntl (i4SockRecv, F_SETFL, O_NONBLOCK) < 0)
    {
        TR_LOG ("Error in setting non-blocking mode!!!\n");
        close (i4SockSend);
        close (i4SockRecv);
        TraceRouteDelete ();
        return;
    }
    packetopt.u1MaxHopLmts = TR_DEF_MAX_HOP_LMT;
    packetopt.i4CurrHops = TR_DEF_INITIAL_HOP_LMT;
    packetopt.u2Seq = TR_INITIAL_PORT;
    packetopt.tv.tv_sec = TR_DEF_TIME_TO_WAIT;
    packetopt.tv.tv_usec = 0;

    inet_pton (AF_INET, (const CHR1 *) gfstraceaddr, &v4addr);
    TRCRT_V4_MAPPED_ADDR_COPY (&packetopt.ip6Addr, v4addr);
    TraceRouteProcess (i4SockSend, i4SockRecv, &packetopt);
    TraceRouteDelete ();
    return;

}

/****************************************************************************
 Function    :  TraceRouteProcess
 Input       :  Sending socket id.
                Receiving socket id.
                packetopt 
 Output      :  None.
 Description :  This function sends and receives the traceroute packets for 
                v4 as well as v6 functionality. 
 Returns     :  None. 
****************************************************************************/
VOID
TraceRouteProcess (INT4 i4SockSend, INT4 i4SockRecv, tpktinfo * packetopt)
{
    INT2                i2Count = 0, i2C = 0;
    INT4                i4RetVal;
    tOsixSysTime        Time1 = 0;
    tOsixSysTime        Time2 = 0;
    tIp6Addr            ip6RecvAddr;
    UINT4               v4addr;
    UINT1               u1temp[64];

    MEMSET (u1temp, 0, 64);
    for (i2Count = 0; i2Count < packetopt->u1MaxHopLmts; i2Count++)
    {
        TR_LOG1 ("\n%d", i2Count + 1);
        for (i2C = 0; i2C < 3; i2C++)
        {
            if (TraceSend (i4SockSend, packetopt) == TR_FAILURE)
            {
                TR_LOG ("Error in sending packet\n");
                close (i4SockSend);
                close (i4SockRecv);
                TR_LOG ("\n");
                return;
            }

            OsixGetSysTime (&Time1);
            i4RetVal = TraceRecv (i4SockRecv, &ip6RecvAddr, &Time2, packetopt);
            if (i4RetVal == TR_FAILURE)
            {
                TR_LOG ("Error in receiving packet\n");
                close (i4SockSend);
                close (i4SockRecv);
                TR_LOG ("\n");
                return;
            }
            if (i4RetVal == TR_TIMEDOUT)
            {
                TR_LOG ("\t*");
            }
            else
            {
                if (i2C == 0)
                {
                    /* Display the address only once */
                    if (gIpVersion == TR_V6)
                        TR_LOG1 ("\t%s",
                                 inet_ntop (AF_INET6, (void *) &ip6RecvAddr,
                                            (CHR1 *) u1temp, INET6_ADDRSTRLEN));
                    else
                    {
                        v4addr = TRCRT_V4_FROM_V6_ADDR (ip6RecvAddr);
                        TR_LOG1 ("\t%s", inet_ntop (AF_INET, (void *) &v4addr,
                                                    (CHR1 *) u1temp,
                                                    INET_ADDRSTRLEN));
                    }
                }
                TR_LOG1 ("   %g ms", TR_TIME_DIFF (Time2, Time1));
            }
            packetopt->u2Seq += 1;
        }
        packetopt->i4CurrHops += 1;

        if (MEMCMP (&ip6RecvAddr, &packetopt->ip6Addr, sizeof (tIp6Addr)) == 0)
        {
            /* Reached the destination */
            close (i4SockSend);
            close (i4SockRecv);
            TR_LOG ("\n");
            return;
        }
    }
    close (i4SockSend);
    close (i4SockRecv);
    return;
}

/****************************************************************************
 Function    :  TraceSend
 Input       :  Sending socket id.
                packetopt 
 Output      :  None.
 Description :  This function sends the traceroute packets for 
                v4 as well as v6 functionality. 
 Returns     :  TR_SUCCESS/TR_FAILURE. 
****************************************************************************/
INT1
TraceSend (INT4 i4SockId, tpktinfo * packetopt)
{
    struct sockaddr_in6 v6sendaddr;
    struct sockaddr_in  v4sendaddr;
    struct sockaddr    *sendaddr;

    UINT1               u1msg[TR_MAX_DATA_LEN];
    INT4                i4OptVal = (INT4) packetopt->i4CurrHops;
    INT4                sendaddrlen;

    MEMSET (u1msg, 0, TR_MAX_DATA_LEN);

    if (gIpVersion == TR_V6)
    {
        if (setsockopt (i4SockId, IPPROTO_IPV6, IPV6_UNICAST_HOPS,
                        &i4OptVal, sizeof (INT4)) < 0)
        {
            return TR_FAILURE;
        }

        MEMSET (&v6sendaddr, 0, sizeof (struct sockaddr_in6));
        v6sendaddr.sin6_family = AF_INET6;
        v6sendaddr.sin6_port = OSIX_HTONS (packetopt->u2Seq);
        MEMCPY (&v6sendaddr.sin6_addr.s6_addr, &packetopt->ip6Addr,
                sizeof (tIp6Addr));
        sendaddr = (struct sockaddr *) &v6sendaddr;
        sendaddrlen = sizeof (struct sockaddr_in6);
    }
    else
    {
        if (setsockopt (i4SockId, IPPROTO_IP, IP_TTL,
                        &i4OptVal, sizeof (INT4)) < 0)
        {
            return TR_FAILURE;
        }

        MEMSET (&v4sendaddr, 0, sizeof (struct sockaddr_in));
        v4sendaddr.sin_family = AF_INET;
        v4sendaddr.sin_port = OSIX_HTONS (packetopt->u2Seq);
        v4sendaddr.sin_addr.s_addr = TRCRT_V4_FROM_V6_ADDR (packetopt->ip6Addr);
        sendaddr = (struct sockaddr *) &v4sendaddr;
        sendaddrlen = sizeof (struct sockaddr_in);
    }

    if (sendto (i4SockId, u1msg, TR_MAX_DATA_LEN, 0,
                sendaddr, sendaddrlen) != TR_MAX_DATA_LEN)
    {
        return TR_FAILURE;
    }
    return TR_SUCCESS;
}

/****************************************************************************
 Function    :  TraceRecv
 Input       :  Receiving socket id.
                packetopt 
 Output      :  Ip address from where the packet has been received. 
                Time when the packet was received.
 Description :  This function receives the traceroute packets for 
                v4 as well as v6 functionality. 
 Returns     :  TR_SUCCESS/TR_FAILURE. 
****************************************************************************/
INT1
TraceRecv (INT4 i4SockId, tIp6Addr * pIp6Addr, tOsixSysTime * pTime,
           tpktinfo * packetopt)
{

    INT4                i4ms, i4Count, i4RetVal = 0;
    UINT1               u1End = FALSE;
    struct sockaddr_in6 v6recvaddr;
    struct sockaddr_in  v4recvaddr;
    struct sockaddr    *recvaddr;
    INT4                i4recvaddrlen;
    UINT1               u1recvmsg[TR_MIN_RECV_LEN];
    tIcmp6PktHdr       *pIcmpHdr = NULL;
    tUdp6Hdr           *pUdpHdr = NULL;

    i4ms = (packetopt->tv.tv_sec) + (packetopt->tv.tv_usec / 1000000);
    if (gIpVersion == TR_V6)
    {
        MEMSET (&v6recvaddr, 0, sizeof (struct sockaddr_in6));
        v6recvaddr.sin6_family = AF_INET6;
        recvaddr = (struct sockaddr *) &v6recvaddr;
        i4recvaddrlen = sizeof (v6recvaddr);
    }
    else
    {
        MEMSET (&v4recvaddr, 0, sizeof (struct sockaddr_in));
        v4recvaddr.sin_family = AF_INET;
        recvaddr = (struct sockaddr *) &v4recvaddr;
        i4recvaddrlen = sizeof (v4recvaddr);
    }
    for (i4Count = 0; i4Count < i4ms; i4Count++)
    {
        i4RetVal = recvfrom (i4SockId, u1recvmsg, TR_MIN_RECV_LEN, 0,
                             recvaddr, &i4recvaddrlen);

        if (i4RetVal <= 0)
        {
            /* recvfrom might have returned error as no packet was available */
            if (errno == SLI_EWOULDBLOCK)
            {
                OsixDelayTask ((SYS_NUM_OF_TIME_UNITS_IN_A_SEC * 100) /
                               TR_THOUSAND);
                continue;
            }
            return TR_FAILURE;
        }

        OsixGetSysTime (pTime);
        if (gIpVersion == TR_V6)
        {
            pIcmpHdr = (tIcmp6PktHdr *) (VOID *) u1recvmsg;
            switch (pIcmpHdr->u1Type)
            {
                case ICMP6_DEST_UNREACHABLE:
                {
                    switch (pIcmpHdr->u1Code)
                    {
                        case ICMP6_NO_ROUTE_TO_DEST:
                            TR_LOG ("No Route to destination\n");
                            return TR_FAILURE;
                        case ICMP6_ADDRESS_UNREACHABLE:
                            TR_LOG ("Destination unreachable\n");
                            return TR_FAILURE;
                        case ICMP6_PORT_UNREACHABLE:
                            pUdpHdr = (tUdp6Hdr *) (VOID *) (u1recvmsg +
                                                             sizeof
                                                             (tIcmp6PktHdr) +
                                                             44);
                            if (OSIX_NTOHS (pUdpHdr->u2DstPort) ==
                                packetopt->u2Seq)
                                u1End = TRUE;
                            break;
                        default:
                            break;
                    }
                }
                    break;
                case ICMP6_PKT_TOO_BIG:
                    TR_LOG ("\nMsg too long");
                    break;
                case ICMP6_TIME_EXCEEDED:
                    if (pIcmpHdr->u1Code == 0)
                        u1End = TRUE;
                    break;
                default:
                    break;
            }
        }
        else
        {
            pIcmpHdr = (tIcmp6PktHdr *) (VOID *) (u1recvmsg + 20);
            switch (pIcmpHdr->u1Type)
            {
                case ICMP_DEST_UNREACHABLE:
                    switch (pIcmpHdr->u1Code)
                    {
                        case ICMP_NET_UNREACHABLE:
                            TR_LOG ("Network unreachable\n");
                            return TR_FAILURE;
                        case ICMP_HOST_UNREACHABLE:
                            TR_LOG ("Host unreachable\n");
                            return TR_FAILURE;
                        case ICMP_PORT_UNREACHABLE:
                            pUdpHdr = (tUdp6Hdr *) (VOID *) (u1recvmsg + 48);
                            if (OSIX_NTOHS (pUdpHdr->u2DstPort) ==
                                packetopt->u2Seq)
                                u1End = TRUE;
                            break;
                        default:
                            break;

                    }
                    break;
                case ICMP_TIME_EXCEEDED:
                    if (pIcmpHdr->u1Code == 0)
                        u1End = 0;
                    break;
                default:
                    break;
            }                    /* end of switch case */
        }

        if (u1End == TRUE)
            break;
        OsixDelayTask ((SYS_NUM_OF_TIME_UNITS_IN_A_SEC * 100) / TR_THOUSAND);
    }
    if (i4Count == i4ms)
        return TR_TIMEDOUT;
    else
    {
        if (gIpVersion == TR_V6)
        {
            v6recvaddr = *((struct sockaddr_in6 *) (VOID *) recvaddr);
            MEMCPY (pIp6Addr, &v6recvaddr.sin6_addr.s6_addr, sizeof (tIp6Addr));
        }
        else
        {
            v4recvaddr = *((struct sockaddr_in *) (VOID *) recvaddr);
            TRCRT_V4_MAPPED_ADDR_COPY (pIp6Addr,
                                       OSIX_NTOHL (v4recvaddr.sin_addr.s_addr));
        }
        return TR_SUCCESS;
    }

}

/****************************************************************************
 Function    :  TR_TIME_DIFF
 Input       :  timevals
 Output      :  None. 
 Description :  This function computes the difference between the two
                time values given.
 Returns     :  Difference of the two times in ms.
****************************************************************************/
double
TR_TIME_DIFF (tOsixSysTime t1, tOsixSysTime t2)
{
    double              i4ms, i4seconds;
    i4seconds = (double) (t1 - t2) / SYS_NUM_OF_TIME_UNITS_IN_A_SEC;
    i4ms = i4seconds * TR_THOUSAND;
    return i4ms;
}
