#ifndef _IP6DBG_H
#define _IP6DBG_H

/* *     $Id: ip6dbg.h,v 1.3 2014/03/03 12:14:08 siva Exp $ */
/* *----------------------------------------------------------------------------- 
 *    FILE NAME                    :    ip6dbg.h
 *
 *    PRINCIPAL AUTHOR             :    
 *
 *    SUBSYSTEM NAME               :    IPv6
 *
 *    MODULE NAME                  :    IP6 Module
 *
 *    LANGUAGE                     :    C
 *
 *    TARGET ENVIRONMENT           :    UNIX
 *
 *    DATE OF FIRST RELEASE        :    DDD-MM-1996
 *
 *    DESCRIPTION                  :    This file contains constants and
 *                                      extern declarations used
 *                                      for debugging purposes
 *
 *----------------------------------------------------------------------------- 
 */
/*
 * Trace Flag constants
 */

/* For IP6 */

#define  IP6_NO_TRACE                0x00000000     /* No Trace */
#define  IP6_TRACE_ALL               0xffffffff     /* All IPv6 information */

#define  IP6_TRACE_INPUT             0x00000001     /* incoming IPv6 packets */
#define  IP6_TRACE_OUTPUT            0x00000002     /* outgoing IPv6 packets */
#define  IP6_TRACE_FORWARD           0x00000004     /* forwarded IPv6 packets */
#define  IP6_TRACE_IF                0x00000008     /* important IP6 IF procs */
#define  IP6_TRACE_ADDR              0x00000010     /* important IP6 ADDR 
                                                     * procs */
#define  IP6_TRACE_LIB               0x00000020     /* Buffers */
#define  IP6_TRACE_TASK              0x00000040     /* Task Ids, Events, 
                                                     * Queues */
#define  IP6_TRACE_TIMER             0x00000080     /* Timers */

#define  IP6_TRACE_INPUT_DTLD        0x00000100     /* incoming IPv6 packets
                                                     * in detail */
#define  IP6_TRACE_OUTPUT_DTLD       0x00000200     /* outgoing IPv6 packets
                                                     * in detail */
#define  IP6_TRACE_FORWARD_DTLD      0x00000400     /* forwarded IPv6 packet
                                                     * in detail */
#define  IP6_TRACE_IF_DTLD           0x00000800     /* most IP6 IF procs */
#define  IP6_TRACE_ADDR_DTLD         0x00001000     /* most IP6 ADDR procs */

#define  IP6_TRACE_FRAG_INPUT        0x00002000     /* reassembly */
#define  IP6_TRACE_FRAG_OUTPUT       0x00004000     /* fragmentation */
#define  IP6_TRACE_EXH               0x00010000     /* IPv6 Extension Header
                                                     * processing */
#define  IP6_TRACE_CFA_IF            0x00020000     /* IPv6 CFA interface */

#define  IP6_TRACE_FRAG_INPUT_DTLD   0x00040000     /* reassembly in detail */
#define  IP6_TRACE_FRAG_OUTPUT_DTLD  0x00080000     /* fragmentation in 
                                                     * detail */

/* For ICMP6 */

#define  ICMP6_NO_TRACE      0x00000000     /* No Trace */
#define  ICMP6_TRACE_ALL     0xffffffff     /* All ICMP6 information */

#define  ICMP6_TRACE_INPUT   0x00000001     /* incoming ICMP6 pkts */
#define  ICMP6_TRACE_OUTPUT  0x00000002     /* outgoing ICMP6 pkts */

/* For UDP6 */

#define  UDP6_NO_TRACE           0x00000000     /* No Trace */
#define  UDP6_TRACE_ALL          0xffffffff     /* All UDP6 information */

#define  UDP6_TRACE_INPUT        0x00000001     /* incoming UDP6 pkts */
#define  UDP6_TRACE_INPUT_DTLD   0x00000002     /* incoming UDP6 packets
                                                 * in detail */
#define  UDP6_TRACE_OUTPUT       0x00000004     /* outgoing UDP6 pkts */
#define  UDP6_TRACE_OUTPUT_DTLD  0x00000008     /* outgoing UDP6 packets
                                                 * in detail */
#define  UDP6_TRACE_TABLE        0x00000010     /* operations on UDP6 Port
                                                 * table */

/* For ND6 */

#define  ND6_NO_TRACE        0x00000000     /* No Trace */
#define  ND6_TRACE_ALL       0xffffffff     /* All ND6 information */

#define  ND6_TRACE_IN_PKTS   0x00000001     /* Incoming frames */
#define  ND6_TRACE_OUT_PKTS  0x00000002     /* Outgoing frames */
#define  ND6_TRACE_IND       0x00000004     /* Status Indications */
#define  ND6_TRACE_CACHE     0x00000008     /* Cache */
#define  ND6_TRACE_TIMER     0x00000010     /* Timers */
#define  ND6_TRACE_BUF       0x00000020     /* Buffers */
#define  ND6_TRACE_IN_DTLD   0x00000040     /* Incoming frames in detail */
#define  ND6_TRACE_OUT_DTLD  0x00000080     /* Outgoing frames in detail */

/* For PING6 */

#define  PING6_NO_TRACE           0x00000000     /* No Trace */
#define  PING6_TRACE_ALL          0xffffffff     /* All PING6 information */

#define  PING6_TRACE_INPUT        0x00000001     /* incoming Echo pkts */
#define  PING6_TRACE_INPUT_DTLD   0x00000002     /* incoming Echo packets
                                                  * in detail */
#define  PING6_TRACE_OUTPUT       0x00000004     /* outgoing Echo pkts */
#define  PING6_TRACE_OUTPUT_DTLD  0x00000008     /* outgoing Echo packets
                                                  * in detail */
/*
 * Function prototypes
 */

/* General */

VOID Ip6dbgBufStats PROTO ((VOID));


UINT1              *Ip6PrintIftok PROTO ((UINT1 *pToken, UINT1 u1TokLen));

/* IP6 IF */

VOID Ip6ifPrint    PROTO ((UINT4 u4Index));
VOID Ip6ifPrintFromIf PROTO ((tIp6If * pIf6));
VOID Ip6ifPrintStats PROTO ((UINT4 u4Index));
VOID Ip6ifPrintStatsFromIf PROTO ((tIp6If * pIf6));
VOID Ip6ifPrintHash PROTO ((VOID));
VOID Ip6ifPrintLlocs PROTO ((UINT4 u4Index));
VOID Ip6ifPrintAddrs PROTO ((UINT4 u4Index));

/* IP6 ADDR */

VOID Ip6AddrStats PROTO ((UINT4 u4Index, tIp6Addr * pAddr,
                            UINT1 u1Prefixlen));

/* IP6 FRAG */

VOID Ip6fragPrintTab PROTO ((tIp6Cxt *));

/* ICMP6 */

VOID Icmp6PrintStats PROTO ((tIp6Cxt *));

/* ND6 */

VOID Nd6PrintCacheTab PROTO ((VOID));
VOID Nd6PrintCacheEntry PROTO ((tNd6CacheEntry * pNd6cEntry));
VOID Nd6PrintTimers PROTO ((VOID));

/* PING6 */

VOID Ping6PrintTab PROTO ((VOID));
VOID Ping6PrintRecord PROTO ((UINT2 u2Index));

#endif /* _IP6DBG_H */
