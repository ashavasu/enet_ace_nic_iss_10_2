/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: traceroute.h,v 1.4 2012/12/13 14:33:27 siva Exp $
 *
 * Description:
 *
 *******************************************************************/
#ifndef _FS_TRACEROUTE_H_
#define _FS_TRACEROUTE_H_


#include "lr.h"
#include "fssocket.h"
#include "ipv6.h"
#include "ip6util.h"
#include "icmp6.h"
#include "udp6.h"

typedef struct 
{
    tIp6Addr  ip6Addr;       /* ip addr to be traceroute */
    INT4      i4CurrHops;    /* Incremental hop limit */      
    UINT2     u2Seq;         /* port no of packet to be sent */
    UINT1     u1Reserved;    /* Padding */
    UINT1     u1MaxHopLmts;  /* Max hops to be traversed */    
    struct timeval tv;     /* Time to wait for response */ 
}tpktinfo;


#define  ICMP_DEST_UNREACHABLE                        3
#define  ICMP_TIME_EXCEEDED                          11
#define  ICMP_NET_UNREACHABLE                         0
#define  ICMP_HOST_UNREACHABLE                        1
#define  ICMP_PORT_UNREACHABLE                        3

#define  TR_TASK_NAME             (const UINT1 *)"TRCR"
#define  TR_TASK_PRIORITY                            40
#define  TR_TASK_STACK_SIZE                       20000

#define  TR_INITIAL_PORT                          33434
#define  TR_DEF_TIME_TO_WAIT                          5
#define  TR_DEF_MAX_HOP_LMT                          30
#define  TR_DEF_INITIAL_HOP_LMT                       1
#define  TR_MAX_DATA_LEN                             40
#define  TR_MIN_RECV_LEN                            100

#define  TR_SUCCESS                                   0
#define  TR_FAILURE                                  -1
#define  TR_TIMEDOUT                                 -2

#define  TR_THOUSAND                               1000
#define  TR_V4                                        4
#define  TR_V6                                        6
#define  TR_INADDR_ANY                           "0::0"
#define  TR_LOG         UtlTrcPrint
#define  TR_LOG1(x,y)   MOD_TRC_ARG1(0xffffffff,0xffffffff," ",x,y)

#define   TRCRT_V4_FROM_V6_ADDR(a)         (a).u4_addr[3]

#define   TRCRT_V4_MAPPED_ADDR_COPY(a,b) { (*a).u4_addr[3] = b; \
                                           (*a).u4_addr[2] = 0xffff; \
                                           (*a).u4_addr[1] = 0; \
                                           (*a).u4_addr[0] = 0; }
#ifdef BSDCOMP_SLI_WANTED
#define SLI_EWOULDBLOCK               EWOULDBLOCK
#endif
/* Prototype declaration */
    
VOID TraceRoute         PROTO((INT1 i1ipver,UINT1* ip6addr ));
    
VOID TraceRouteDelete   PROTO((VOID));

VOID TraceRoute6        PROTO((INT1 *pDummy));

VOID TraceRoute4        PROTO((INT1 *pDummy));

VOID TraceRouteProcess  PROTO((INT4 i4SockSend,
                               INT4 i4SockRecv,tpktinfo *packetopt));

INT1 TraceSend          PROTO((INT4 i4SockId,tpktinfo *packetopt));

INT1 TraceRecv          PROTO((INT4 i4SockId, tIp6Addr *ipAddr,
                               tOsixSysTime *pTime,tpktinfo *packetopt));

double TR_TIME_DIFF     PROTO((tOsixSysTime t1,tOsixSysTime t2));

#endif /* _FS_TRACEROUTE_H_ */
