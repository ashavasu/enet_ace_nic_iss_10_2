/*******************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ip6main.c,v 1.79 2017/12/26 13:34:21 siva Exp $
 *
 * Description: Cli related functions are handled here.
 *
 *******************************************************************/

#include "ip6inc.h"
#include "secv6.h"
#include "nd6red.h"

extern VOID RegisterFSIPV6 PROTO ((void));
extern VOID RegisterFSMPIPV6 PROTO ((void));
extern tNetIpv6MCastProtoRegTbl gaNetIpv6MCastRegTable[IP6_MAX_MCAST_PROTOCOLS];

/*
 * Global Declarations
 */

tIp6GblInfo         gIp6GblInfo;
tOsixTaskId         i4Ip6TaskId, i4_ping6_task_id;
tOsixSemId          gIp6SemTaskLockId;
tNd6RedGlobalInfo   gNd6RedGlobalInfo;
INT4                gi4Ip6SysLogId = 0;
INT4                gi4HaEnabled = 0;
UINT4               gu4Nd6CacheEntries = 0;

/******************************************************************************
DESCRIPTION : IPv6 Protocol Initialisation
 
INPUTS      : None

OUTPUTS     : None

RETURNS     : IP6_SUCCESS      -  Initialisation succeeds
              IP6_FAILURE  -  Initialisation fails
******************************************************************************/

INT4
Ip6ProtoInit ()
{
    /*
     * SPAWN ALL TASKS RELATED WITH IPv6. THERE ARE 3 TASK AVAILABLE.
     * SPAWN THE TASK IN THE FOLLOWING ORDER.
     * 1) IPV6 MAIN TASK - PROCESS ALL IPv6 RELATED PACKETS AND
     *                     FUNCTIONALITY. 
     * 2) PING6 TASK - HANDLES THE PING6 REQUEST AND GENERATE RESPONSE.
     *
     */

    /* Spawning of IPv6 and Ping6 Tasks */
    if (Ip6SpawnTasks () == IP6_FAILURE)
    {
        Ip6Shutdown ();
        return IP6_FAILURE;
    }

    return IP6_SUCCESS;
}

/******************************************************************************
DESCRIPTION : IPv6 Protocol Shutdown
 
INPUTS      : None

OUTPUTS     : None

RETURNS     :None
******************************************************************************/
VOID
Ip6Shutdown ()
{
    UINT2               u2Count = 0;

    /* During task initialization failures, the gIp6GblInfo.i4Ip6Status will
     * not be set. Still the memory allocated has to be freed */

    Ip6Disable ();
    VcmDeRegisterHLProtocol (IP6_FWD_PROTOCOL_ID);
    /* Deletion of hash tables */
    if (gIp6GblInfo.UnicastAddrTree != NULL)
    {
        RBTreeDelete (gIp6GblInfo.UnicastAddrTree);
        gIp6GblInfo.UnicastAddrTree = NULL;
    }

    if (gIp6GblInfo.AnycastAddrTree != NULL)
    {
        RBTreeDelete (gIp6GblInfo.AnycastAddrTree);
        gIp6GblInfo.AnycastAddrTree = NULL;
    }

    /* Deleting Interfaces */
    u2Count = 1;

    IP6_IF_SCAN (u2Count, IP6_MAX_LOGICAL_IF_INDEX)
    {
        if (Ip6ifEntryExists ((UINT4) u2Count) == IP6_SUCCESS)
        {
            if (Ip6DeleteIf ((UINT4) u2Count) != IP6_SUCCESS)
            {
                IP6_GBL_TRC_ARG (IP6_MOD_TRC, ALL_FAILURE_TRC,
                                 IP6_NAME, "IP6MAIN: Ip6Shutdown: Unable To "
                                 "Delete Default Interface!!! \n");
            }
        }
    }
    if (gIp6GblInfo.pIf6Htab)
    {
        TMO_HASH_Delete_Table (gIp6GblInfo.pIf6Htab, NULL);
        gIp6GblInfo.pIf6Htab = NULL;
    }
    if (gNd6GblInfo.Nd6CacheTable != NULL)
    {
        RBTreeDelete (gNd6GblInfo.Nd6CacheTable);
        gNd6GblInfo.Nd6CacheTable = NULL;
    }

    /* Delete the Scopezone table */
    if (gIp6GblInfo.ScopeZoneTree != NULL)
    {
        RBTreeDelete (gIp6GblInfo.ScopeZoneTree);
        gIp6GblInfo.ScopeZoneTree = NULL;
    }
    Ip6HandleDeleteContext (IP6_DEFAULT_CONTEXT);
    /* Deletion of Mempools */
    Ip6SizingMemDeleteMemPools ();
    Ping6SizingMemDeleteMemPools ();

    ip6PmtuShut ();

    /* Deleting the timer list */
    if (gIp6GblInfo.Ip6TimerListId)
    {
        TmrDeleteTimerList (gIp6GblInfo.Ip6TimerListId);
        gIp6GblInfo.Ip6TimerListId = 0;
    }

    /* Deleting the Semaphore */
    if (gIp6SemTaskLockId)
    {
        OsixDeleteSem (SELF, (CONST UINT1 *) IP6_TASK_SEM_NAME);
        gIp6SemTaskLockId = 0;
    }

    /* Deleting Qs */
    if (OsixDeleteQ (SELF, (const UINT1 *) IP6_TASK_CONTROL_QUEUE) !=
        OSIX_SUCCESS)
    {
        IP6_GBL_TRC_ARG (IP6_MOD_TRC, ALL_FAILURE_TRC, IP6_NAME,
                         "IP6MAIN: Ip6Shutdown: Deletion of IP6 task control "
                         "queue fails !!! \n");
    }
    if (OsixDeleteQ (SELF, (const UINT1 *) IP6_TASK_IP_INPUT_QUEUE) !=
        OSIX_SUCCESS)
    {
        IP6_GBL_TRC_ARG (IP6_MOD_TRC, ALL_FAILURE_TRC, IP6_NAME,
                         "IP6MAIN: Ip6Shutdown: Deletion of IP6 Input queue "
                         "fails !!! \n");
    }
    if (OsixDeleteQ (SELF, (const UINT1 *) IP6_TASK_NS_NA_QUEUE) !=
        OSIX_SUCCESS)
    {
        IP6_GBL_TRC_ARG (IP6_MOD_TRC, ALL_FAILURE_TRC, IP6_NAME,
                         "IP6MAIN: Ip6Shutdown: Deletion of IP6_TASK_NS_NA_QUEUE "
                         "fails !!! \n");
    }
    if (OsixDeleteQ (SELF, (const UINT1 *) PING6_TASK_PING_INPUT_QUEUE) !=
        OSIX_SUCCESS)
    {
        IP6_GBL_TRC_ARG (IP6_MOD_TRC, ALL_FAILURE_TRC, IP6_NAME,
                         "IP6MAIN:Ip6Shutdown:PING6_TASK_PING_INPUT_QUEUE "
                         "Deleteion Failed \n");
    }
    if (OsixDeleteQ (SELF, (const UINT1 *) IP6_TASK_PING_INPUT_QUEUE) !=
        OSIX_SUCCESS)
    {
        IP6_GBL_TRC_ARG (IP6_MOD_TRC, ALL_FAILURE_TRC, IP6_NAME,
                         "IP6MAIN: Ip6Shutdown: Deletion of IP6 ping input queue "
                         "fails !!! \n");
    }

    /* delete the IP6_TASK_ONLINK_NOTIFY_QUEUE on shutdown - RFC 4007 */
    if (OsixDeleteQ (SELF, (const UINT1 *) IP6_TASK_ONLINK_NOTIFY_QUEUE) !=
        OSIX_SUCCESS)
    {
        IP6_GBL_TRC_ARG (IP6_MOD_TRC, ALL_FAILURE_TRC, IP6_NAME,
                         "IP6MAIN: Ip6Shutdown: Deletion of IP6_TASK_ONLINK_NOTIFY_QUEUE queue "
                         "fails !!! \n");
    }

    if (OsixDeleteQ (SELF, (const UINT1 *) IP6_BFD_STATE_CHANGE_QUEUE) !=
        OSIX_SUCCESS)
    {
        IP6_GBL_TRC_ARG (IP6_MOD_TRC, ALL_FAILURE_TRC, IP6_NAME,
                         "IP6MAIN: Ip6Shutdown: Deletion of IP6_BFD_STATE_CHANGE_QUEUE queue "
                         "fails !!! \n");
    }

    if (gNd6RedGlobalInfo.u1RedStatus == IP6_TRUE)
    {
        if (OsixDeleteQ (SELF, (const UINT1 *) IP6_RM_PKT_QUEUE)
            != OSIX_SUCCESS)
        {
            IP6_GBL_TRC_ARG (IP6_MOD_TRC, ALL_FAILURE_TRC, IP6_NAME,
                             "IP6MAIN: Ip6Shutdown: Deletion of IP6_RM_PKT_QUEUE queue "
                             "fails !!! \n");
        }
        Nd6RedDeInitGlobalInfo ();
    }
    /* Re-Initialise the Higher Layer Protocols */
    Ip6InitRegTable ();

#ifndef RM_WANTED
    /* Delete IP6 Task */
    OsixDeleteTask (IP6_SELF_NODE, (const UINT1 *) IP6_TASK_NAME);
    /* Delete Ping Task */
    OsixDeleteTask (IP6_SELF_NODE, (const UINT1 *) PING6_TASK_NAME);
#endif

    /* Set the global Flag to indicate that IP6 is Disabled. */
    gIp6GblInfo.i4Ip6Status = IP6_FAILURE;
}

/******************************************************************************
DESCRIPTION : Spawns the IPv6 Protocol based tasks in the system
 
INPUTS      : None

OUTPUTS     : None

RETURNS     : IP6_SUCCESS      - Spawning of tasks succeeds
              IP6_FAILURE  - Spawning of tasks fails
******************************************************************************/

INT4
Ip6SpawnTasks ()
{

    if (OsixCreateTask ((const UINT1 *) IP6_TASK_NAME,
                        IP6_TASK_PRIORITY,
                        OSIX_DEFAULT_STACK_SIZE,
                        Ip6TaskMain, NULL, 0, &i4Ip6TaskId) != OSIX_SUCCESS)
    {
        IP6_GBL_TRC_ARG (IP6_MOD_TRC, ALL_FAILURE_TRC, IP6_NAME,
                         "IP6MAIN:Ip6SpawnTasks:Failed \n");
        return (IP6_FAILURE);
    }

    if (OsixCreateTask ((const UINT1 *) PING6_TASK_NAME,
                        PING6_TASK_PRIORITY, OSIX_DEFAULT_STACK_SIZE,
                        Ping6TaskMain, NULL, 0,
                        &i4_ping6_task_id) != OSIX_SUCCESS)
    {
        IP6_GBL_TRC_ARG (IP6_MOD_TRC, ALL_FAILURE_TRC, IP6_NAME,
                         "IP6MAIN:Ip6SpawnTasks:PING6 Task Spawn Failed \n");
        return (IP6_FAILURE);
    }

    IP6_GBL_TRC_ARG2 (IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                      "IP6MAIN:Ip6SpawnTasks:Tasks spawned, IP6 ID = %d  PING6 ID = %d\n",
                      i4Ip6TaskId, i4_ping6_task_id);

    return (IP6_SUCCESS);

}

/******************************************************************************
DESCRIPTION : Performs the IPv6 Task Initialisation and
              processing of Queue Events
 
INPUTS      : None

OUTPUTS     : None

RETURNS     : None 
******************************************************************************/

VOID
Ip6TaskMain (INT1 *pDummy)
{
    UINT4               u4Event = 0;
    UINT4               u4Index = 0;
    UINT4               u4EntryCount = 0;
    tIp6If             *pIf6 = NULL;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    tOsixQId            QId;
    UINT1               u1Cmd = 0;
    tHlToIp6Params     *pHlParams = NULL;
    tHlToIp6McastParams *pIp6McastHlParams = NULL;
    tIcmp6Params       *pIcmp6Params = NULL;
    tIp6Hdr            *pIp6 = NULL, ip6Hdr;
    tIp6VcmParams      *pParams = NULL;
    tIp6Cxt            *pIp6Cxt = NULL;
    tIp6TunlAccessListNode *pIp6TunlAccessListNodePtr = NULL;
    tIp6Addr            Ip6Addr;
    UINT4               u4NumOfMsgs = 0;
    INT1                i1AddrType = 0;

    UNUSED_PARAM (pDummy);
    UNUSED_PARAM (pIp6TunlAccessListNodePtr);

    MEMSET (&gIp6GblInfo, 0, sizeof (tIp6GblInfo));
    gIp6GblInfo.i4Ip6Status = IP6_FAILURE;

    gNd6RedGlobalInfo.u1RedStatus = IP6_FALSE;
#ifdef L2RED_WANTED
    gNd6RedGlobalInfo.u1RedStatus = IP6_TRUE;
#endif

    if (OsixCreateQ
        ((const UINT1 *) IP6_TASK_CONTROL_QUEUE, IP6_Q_DEPTH_DEF,
         0, &QId) != OSIX_SUCCESS)
    {
        IP6_GBL_TRC_ARG (IP6_MOD_TRC, ALL_FAILURE_TRC, IP6_NAME,
                         "IP6MAIN:Ip6SpawnTasks:IP6_TASK_CONTROL_QUEUE Creation "
                         "Failed \n");
#ifdef ISS_WANTED
        IP6_INIT_COMPLETE (OSIX_FAILURE);
#endif
        return;
    }

    if (OsixCreateQ
        ((const UINT1 *) IP6_TASK_IP_INPUT_QUEUE, IP6_Q_DEPTH_DEF,
         0, &QId) != OSIX_SUCCESS)
    {
        IP6_GBL_TRC_ARG (IP6_MOD_TRC, ALL_FAILURE_TRC, IP6_NAME,
                         "IP6MAIN:Ip6SpawnTasks:IP6_TASK_IP_INPUT_QUEUE Creation "
                         "Failed \n");
        /* Queue will be deleted in Ip6Shutdown function */
#ifdef ISS_WANTED
        IP6_INIT_COMPLETE (OSIX_FAILURE);
#endif
        return;
    }
    if (OsixCreateQ
        ((const UINT1 *) IP6_TASK_NS_NA_QUEUE, IP6_Q_DEPTH_DEF,
         0, &QId) != OSIX_SUCCESS)
    {
        IP6_GBL_TRC_ARG (IP6_MOD_TRC, ALL_FAILURE_TRC, IP6_NAME,
                         "IP6MAIN:Ip6SpawnTasks:IP6_TASK_NS_NA_QUEUE Creation "
                         "Failed \n");
        /* Queue will be deleted in Ip6Shutdown function */
#ifdef ISS_WANTED
        IP6_INIT_COMPLETE (OSIX_FAILURE);
#endif
        return;
    }
    if (OsixCreateQ
        ((const UINT1 *) IP6_TASK_PING_INPUT_QUEUE, IP6_Q_DEPTH_DEF,
         0, &QId) != OSIX_SUCCESS)
    {
        IP6_GBL_TRC_ARG (IP6_MOD_TRC, ALL_FAILURE_TRC, IP6_NAME,
                         "IP6MAIN:Ip6SpawnTasks:IP6_TASK_PING_INPUT_QUEUE Creation "
                         "Failed \n");
        /* Queue will be deleted in Ip6Shutdown function */
#ifdef ISS_WANTED
        IP6_INIT_COMPLETE (OSIX_FAILURE);
#endif
        return;
    }
    /* New queue added to receive the onlink message from OSPF - RFC4007 */
    if (OsixCreateQ
        ((const UINT1 *) IP6_TASK_ONLINK_NOTIFY_QUEUE, IP6_Q_DEPTH_DEF,
         0, &QId) != OSIX_SUCCESS)
    {
        IP6_GBL_TRC_ARG (IP6_MOD_TRC, ALL_FAILURE_TRC, IP6_NAME,
                         "IP6MAIN:Ip6SpawnTasks:IP6_TASK_ONLINK_NOTIFY_QUEUE Creation "
                         "Failed \n");
        /* Queue will be deleted in Ip6Shutdown function */
#ifdef ISS_WANTED
        IP6_INIT_COMPLETE (OSIX_FAILURE);
#endif
        return;
    }
    if (OsixCreateQ
        ((const UINT1 *) IP6_BFD_STATE_CHANGE_QUEUE, IP6_Q_DEPTH_DEF,
         0, &QId) != OSIX_SUCCESS)
    {
        IP6_GBL_TRC_ARG (IP6_MOD_TRC, ALL_FAILURE_TRC, IP6_NAME,
                         "IP6MAIN:Ip6SpawnTasks:IP6_BFD_STATE_CHANGE_QUEUE Creation "
                         "Failed \n");
        /* Queue will be deleted in Ip6Shutdown function */
#ifdef ISS_WANTED
        IP6_INIT_COMPLETE (OSIX_FAILURE);
#endif
        return;
    }
    /*Creating a new Queue for receiving routes from ECMPv6 PRT which
     * need ND resolution*/
    if (OsixCreateQ ((const UINT1 *) IP6_ECMP6_PRT_RESOLVE_QUEUE,
                     IP6_Q_DEPTH_DEF, 0, &QId) != OSIX_SUCCESS)
    {
        IP6_GBL_TRC_ARG (IP6_MOD_TRC, ALL_FAILURE_TRC, IP6_NAME,
                         "IP6MAIN:Ip6SpawnTasks:IP6_TASK_CONTROL_QUEUE Creation "
                         "Failed \n");
#ifdef ISS_WANTED
        IP6_INIT_COMPLETE (OSIX_FAILURE);
#endif
        return;
    }

    if (OsixCreateQ ((const UINT1 *) IP6_MULTICAST_REG_QUEUE,
                     IP6_MC6Q_DEPTH_DEF, 0, &QId) != OSIX_SUCCESS)
    {
        IP6_GBL_TRC_ARG (IP6_MOD_TRC, ALL_FAILURE_TRC, IP6_NAME,
                         "IP6MAIN:Ip6SpawnTasks:IP6_MULTICAST_REG_QUEUE Creation "
                         "Failed \n");
#ifdef ISS_WANTED
        IP6_INIT_COMPLETE (OSIX_FAILURE);
#endif
        return;
    }

    /*
     * Do the IP6 task initialisations
     */

    if (Ip6TaskInit () == IP6_FAILURE)
    {
        IP6_GBL_TRC_ARG (IP6_MOD_TRC, INIT_SHUT_TRC | ALL_FAILURE_TRC, IP6_NAME,
                         "IP6MAIN:Ip6TaskMain: IP6 TASK INIT IP6_FAILURE\n");
        /* Queue will be deleted in Ip6Shutdown function */
#ifdef ISS_WANTED
        IP6_INIT_COMPLETE (OSIX_FAILURE);
#endif
        return;
    }

    /* Notify to the lower layer that IP6 task is READY. */
    Ip6NotifyLL ();

#ifdef SNMP_2_WANTED
    RegisterFSIPV6 ();
    RegisterSTDIP6 ();
    RegisterFSMPIPV6 ();
#endif

    Ip6InitMCastRegTable ();
    /*
     * Wait on events and process them
     */
#ifdef ISS_WANTED
    IP6_INIT_COMPLETE (OSIX_SUCCESS);
#endif

    while (1)
    {
        /*
         *   IP6 task waits on the following events and acts accordingly 
         *   upon its occurence
         */
        u4Event = 0;
        OsixReceiveEvent ((IP6_CONTROL_EVENT |
                           IP6_TIMER0_EVENT |
                           IP6_DGRAM_RECD_EVENT |
                           IP6_PING_MESG_RECD_EVENT |
                           IP6_ON_LINK_MSG_RECD_EVENT |
                           IP6_RM_PKT_EVENT |
                           IP6_HA_PEND_BLKUPD_EVENT |
                           IP6_RED_TIMER_EVENT |
                           IP6_RED_START_TIMER_EVENT |
                           IP6_ND_RESOLVE_EVENT |
                           IP6_MC_REG_EVENT |
                           IP6_BFD_STATE_CHANGE_EVENT), OSIX_WAIT, 0, &u4Event);

        IP6_GBL_TRC_ARG1 (IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                          "IP6MAIN:Ip6TaskMain: Events Mask 0x%X \n", u4Event);
        Ip6GLock ();
        if (IP6_TASK_LOCK () == SNMP_FAILURE)
        {
            /* This is FATAL. Dont process the IP6 Events */
            continue;
        }

        if (u4Event & IP6_RM_PKT_EVENT)
        {
            Nd6RedHandleRmEvents ();
        }

        /* Interface Status Notification */
        if (u4Event & IP6_CONTROL_EVENT)
        {

            while (OsixReceiveFromQ
                   (SELF, (const UINT1 *) IP6_TASK_CONTROL_QUEUE, OSIX_NO_WAIT,
                    0, &pBuf) == OSIX_SUCCESS)
            {
                IP6_GBL_TRC_ARG1 (IP6_MOD_TRC,
                                  CONTROL_PLANE_TRC, IP6_NAME,
                                  "IP6MAIN:Ip6TaskMain: Control Event - buffer = "
                                  "%p\n", pBuf);
                switch (IP6_GET_COMMAND (pBuf))
                {
#ifdef MBSM_WANTED                /* IP6_CHASSIS_MERGE */
                    case MBSM_MSG_CARD_INSERT:
                    case MBSM_MSG_CARD_REMOVE:
                        Ip6MbsmUpdateLCStatus (pBuf, IP6_GET_COMMAND (pBuf));
                        break;
#endif
                    case IP6_DELETE_CONTEXT:
                        pParams = (tIp6VcmParams *) Ip6GetParams (pBuf);
                        Ip6HandleDeleteContext (pParams->u4ContextId);
                        Ip6RelMem (pParams->u4ContextId,
                                   (UINT2) gIp6GblInfo.i4ParamId,
                                   (UINT1 *) pParams);
                        Ip6BufRelease (pParams->u4ContextId, pBuf, FALSE,
                                       IP6_MODULE);
                        break;
                    case IP6_MAP_INTERFACE:
                        pParams = (tIp6VcmParams *) Ip6GetParams (pBuf);
                        Ip6HandleIntfMapEvent (pParams->u4IfIndex,
                                               IP6_DEFAULT_CONTEXT,
                                               pParams->u4ContextId);
                        Ip6RelMem (pParams->u4ContextId,
                                   (UINT2) gIp6GblInfo.i4ParamId,
                                   (UINT1 *) pParams);
                        Ip6BufRelease (pParams->u4ContextId, pBuf, FALSE,
                                       IP6_MODULE);
                        break;
                    case IP6_UNMAP_INTERFACE:
                        pParams = (tIp6VcmParams *) Ip6GetParams (pBuf);
                        Ip6HandleIntfMapEvent (pParams->u4IfIndex,
                                               pParams->u4ContextId,
                                               IP6_DEFAULT_CONTEXT);
                        Ip6RelMem (pParams->u4ContextId,
                                   (UINT2) gIp6GblInfo.i4ParamId,
                                   (UINT1 *) pParams);
                        Ip6BufRelease (pParams->u4ContextId, pBuf, FALSE,
                                       IP6_MODULE);
                        break;
                    default:
                        Ip6ControlMsg (pBuf);
                        Ip6BufRelease (VCM_INVALID_VC, pBuf,
                                       FALSE, IP6_MAIN_SUBMODULE);
                }
            }
        }
        if (u4Event & IP6_ON_LINK_MSG_RECD_EVENT)
        {
            while (OsixReceiveFromQ
                   (SELF, (const UINT1 *) IP6_TASK_ONLINK_NOTIFY_QUEUE,
                    OSIX_NO_WAIT, 0, &pBuf) == OSIX_SUCCESS)
            {

                Ip6HandleSelfIfOnLinkEvent (pBuf);
            }
        }

        if (u4Event & IP6_BFD_STATE_CHANGE_EVENT)
        {
            while (OsixReceiveFromQ
                   (SELF, (const UINT1 *) IP6_BFD_STATE_CHANGE_QUEUE,
                    OSIX_NO_WAIT, 0, &pBuf) == OSIX_SUCCESS)
            {
                Ip6ProcessPathStatusNotification (pBuf);
            }
        }
        /*
         *   Only one frame is dequeued from each of the following
         *   queues upon generation of event; after that, if messages are
         *   pending in the queue, the event is posted again. This is done
         *   to ensure that no single event hogs the processing delaying
         *   the handling of other events
         */

        /* IPv6 Packet Reception */
        if (u4Event & IP6_DGRAM_RECD_EVENT)
        {
            while ((OsixReceiveFromQ
                    (SELF, (const UINT1 *) IP6_TASK_NS_NA_QUEUE, OSIX_NO_WAIT,
                     0, &pBuf) == OSIX_SUCCESS))
            {
                /*
                 *  Packet from lower layer to IPv6, Module Data is
                 *  used for params structure
                 */
                if ((pIf6 = Ip6LocateIf (pBuf)) != NULL)
                {
                    IP6_TRC_ARG2 (pIf6->pIp6Cxt->u4ContextId,
                                  IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                                  "IP6MAIN: Ip6TaskMain:IP6 DGram "
                                  "Received Event - buffer= %p "
                                  "Interface = %d\n", pBuf, pIf6->u4Index);
                    Ip6Input (pIf6, pBuf, NULL);
                }
                else
                {
                    IP6_GBL_TRC_ARG (IP6_MOD_TRC,
                                     DATA_PATH_TRC, IP6_NAME,
                                     "IP6MAIN:Ip6TaskMain: IP6 DGram "
                                     "Received Event - IPv6 pkt dropped : "
                                     "No Valid IF\n");
                    if (Ip6BufRelease (VCM_INVALID_VC, pBuf, FALSE,
                                       IP6_MAIN_SUBMODULE) != IP6_SUCCESS)
                    {
                        IP6_GBL_TRC_ARG (IP6_MOD_TRC,
                                         BUFFER_TRC | ALL_FAILURE_TRC,
                                         IP6_NAME,
                                         "IP6MAIN:Ip6TaskMain:Deque IPv6 "
                                         "Pkt  BufRelease Failed\n");
                        IP6_GBL_TRC_ARG3 (IP6_MOD_TRC,
                                          BUFFER_TRC, IP6_NAME,
                                          "IP6MAIN:Ip6TaskMain: %s buf err "
                                          ": Type = %d Bufptr = %p ",
                                          ERROR_FATAL_STR,
                                          ERR_BUF_RELEASE, pBuf);
                    }
                }
            }
            u4NumOfMsgs = 0;
            while ((OsixReceiveFromQ
                    (SELF, (const UINT1 *) IP6_TASK_IP_INPUT_QUEUE,
                     OSIX_NO_WAIT, 0, &pBuf) == OSIX_SUCCESS))
            {

                u1Cmd = IP6_GET_COMMAND (pBuf);

                switch (u1Cmd)
                {
                    case IP6_LAYER2_DATA:

                        /* 
                         *  Packet from lower layer to IPv6, Module Data is 
                         *  used for params structure 
                         */
                        if ((pIf6 = Ip6LocateIf (pBuf)) != NULL)
                        {
                            IP6_TRC_ARG2 (pIf6->pIp6Cxt->u4ContextId,
                                          IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                                          "IP6MAIN: Ip6TaskMain:IP6 DGram "
                                          "Received Event - buffer= %p "
                                          "Interface = %d\n",
                                          pBuf, pIf6->u4Index);
                            Ip6Input (pIf6, pBuf, &Ip6Addr);
                            if (!(IS_ADDR_UNSPECIFIED (Ip6Addr)))
                            {
                                /* Remove the Entry created in the DLF Hash Table */
#if defined (NPAPI_WANTED) && defined (CFA_WANTED)
                                Ipv6FsCfaHwRemoveIp6NetRcvdDlfInHash (pIf6->
                                                                      pIp6Cxt->
                                                                      u4ContextId,
                                                                      Ip6Addr,
                                                                      IP6_MAX_PREFIX_LEN);
#endif
                            }
                        }
                        else
                        {
                            IP6_GBL_TRC_ARG (IP6_MOD_TRC,
                                             DATA_PATH_TRC, IP6_NAME,
                                             "IP6MAIN:Ip6TaskMain: IP6 DGram "
                                             "Received Event - IPv6 pkt dropped : "
                                             "No Valid IF\n");

                            if (Ip6BufRelease (VCM_INVALID_VC, pBuf, FALSE,
                                               IP6_MAIN_SUBMODULE) !=
                                IP6_SUCCESS)
                            {
                                IP6_GBL_TRC_ARG (IP6_MOD_TRC,
                                                 BUFFER_TRC | ALL_FAILURE_TRC,
                                                 IP6_NAME,
                                                 "IP6MAIN:Ip6TaskMain:Deque IPv6 "
                                                 "Pkt  BufRelease Failed\n");
                                IP6_GBL_TRC_ARG3 (IP6_MOD_TRC,
                                                  BUFFER_TRC, IP6_NAME,
                                                  "IP6MAIN:Ip6TaskMain: %s buf err "
                                                  ": Type = %d Bufptr = %p ",
                                                  ERROR_FATAL_STR,
                                                  ERR_BUF_RELEASE, pBuf);
                            }
                        }
                        u4NumOfMsgs++;
                        break;

                    case IP6_LAYER4_DATA:

                        /* 
                         *  Packet from  TCPv6/IPv4 layer to IPv6, seperate mem
                         *  block is used for params structure 
                         */

                        pHlParams = (tHlToIp6Params *) Ip6GetParams (pBuf);
                        /* To handle IPv6 link local telnet request, the interface index should be
                         * properly updated. Since TCP always gives interface index as invalid one, change
                         * the interface index to correct value 
                         */
                        if (((pHlParams->u1Proto == TCP_PROTOCOL)
                             && (Ip6AddrType (&pHlParams->Ip6SrcAddr) ==
                                 ADDR6_LLOCAL)
                             && (pHlParams->u4Index == IPV6_MAX_AS_NUM)
                             && (Ip6IsOurAddr (&pHlParams->Ip6SrcAddr, &u4Index)
                                 == IP6_SUCCESS)))
                        {
                            pHlParams->u4Index = u4Index;
                        }

                        /* get pointer to interface, from interface index */
                        if (Ip6ifEntryExists (pHlParams->u4Index) ==
                            IP6_SUCCESS)
                        {
                            pIf6 = IP6_INTERFACE (pHlParams->u4Index);
                            pIp6Cxt = pIf6->pIp6Cxt;
                        }
                        else
                        {
                            pIf6 = NULL;
                            pIp6Cxt = Ipv6UtilGetCxtEntryFromCxtId
                                (pHlParams->u4ContextId);
                            if (pIp6Cxt == NULL)
                            {
                                Ip6BufRelease (VCM_INVALID_VC, pBuf,
                                               FALSE, IP6_CORE_SUBMODULE);
                                Ip6RelMem (VCM_INVALID_VC,
                                           (UINT2) gIp6GblInfo.i4ParamId,
                                           (UINT1 *) pHlParams);
                                break;
                            }
                        }
                        i1AddrType = Ip6AddrType (&pHlParams->Ip6DstAddr);
                        if ((i1AddrType == ADDR6_MULTI)
                            || (i1AddrType == ADDR6_LLOCAL))
                        {
                            Ip6SendInCxt (pIp6Cxt, pIf6,
                                          &pHlParams->Ip6SrcAddr,
                                          &pHlParams->Ip6DstAddr,
                                          pHlParams->u4Len,
                                          pHlParams->u1Proto, pBuf,
                                          pHlParams->u1Hlim);
                        }
                        else
                        {
                            Ip6SendInCxt (pIp6Cxt, NULL,
                                          &pHlParams->Ip6SrcAddr,
                                          &pHlParams->Ip6DstAddr,
                                          pHlParams->u4Len,
                                          pHlParams->u1Proto, pBuf,
                                          pHlParams->u1Hlim);
                        }

                        Ip6RelMem (pIp6Cxt->u4ContextId,
                                   (UINT2) gIp6GblInfo.i4ParamId,
                                   (UINT1 *) pHlParams);
                        break;
                    case IP6_MCAST_DATA_PACKET_FROM_MRM:

                        pIp6McastHlParams =
                            (tHlToIp6McastParams *) Ip6GetParams (pBuf);

                        Ip6SendMcastPkt (pBuf, pIp6McastHlParams);

                        Ip6RelMem (VCM_INVALID_VC,
                                   (UINT2) gIp6GblInfo.i4ParamId,
                                   (UINT1 *) pIp6McastHlParams);

                        break;

                    case IP6_ICMPERR_DATA:

                        pIcmp6Params = (tIcmp6Params *) Ip6GetParams (pBuf);

                        /* get pointer to interface, from interface index */
                        if (Ip6ifEntryExists (pIcmp6Params->u4Index) ==
                            IP6_SUCCESS)
                        {
                            pIf6 = IP6_INTERFACE (pIcmp6Params->u4Index);
                        }
                        else
                        {
                            pIp6Cxt =
                                gIp6GblInfo.apIp6Cxt[pIcmp6Params->u4ContextId];
                            pIf6 = Ip6FindRtIfInCxt (pIcmp6Params->u4ContextId,
                                                     &pIcmp6Params->ip6Dst);
                            if (pIf6 == NULL)
                            {
                                Ip6RelMem (pIp6Cxt->u4ContextId,
                                           (UINT2) gIp6GblInfo.i4ParamId,
                                           (UINT1 *) pIcmp6Params);
                                Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf,
                                               FALSE, IP6_CORE_SUBMODULE);
                                break;
                            }
                        }

                        /* 
                         * Get IPv6 header from the packet received from
                         * higher layers ( TCPv6 / IPv4 )
                         */

                        pIp6 = (tIp6Hdr *) Ip6BufRead (pBuf,
                                                       (UINT1 *) &ip6Hdr, 0,
                                                       (UINT4) (sizeof
                                                                (tIp6Hdr)), 0);

                        if (pIp6 == NULL)
                        {
                            Ip6RelMem (pIf6->pIp6Cxt->u4ContextId,
                                       (UINT2) gIp6GblInfo.i4ParamId,
                                       (UINT1 *) pIcmp6Params);
                            Ip6BufRelease (pIf6->pIp6Cxt->u4ContextId,
                                           pBuf, FALSE, IP6_CORE_SUBMODULE);
                            break;
                        }
                        Icmp6SendErrMsg (pIf6, pIp6,
                                         pIcmp6Params->u1Type,
                                         pIcmp6Params->u1Code, 0, pBuf);
                        Ip6RelMem (pIf6->pIp6Cxt->u4ContextId,
                                   (UINT2) gIp6GblInfo.i4ParamId,
                                   (UINT1 *) pIcmp6Params);

                        break;
#ifdef SLI_WANTED
                    case IP6_RAW_DATA:
                        /*
                         * Based on the Header include flag, valid
                         * offset is moved.
                         */

                        pHlParams = (tHlToIp6Params *) Ip6GetParams (pBuf);

                        /*
                         * If IPv6-header-include flag is not set,
                         * IPv6 will form the header. If it is set,
                         * application should have formed the IPv6 header
                         * and the receiving buffer contains it.
                         * u1Reserved should be correctly named.
                         */

                        if (pHlParams->u1Reserved == IP6_HDR_INC)
                        {

                            /*
                             * Get IPv6 header contents to send to Ip6Send,
                             * so that IPv6 will reconstruct the V6 header.
                             */
                            pIp6 = (tIp6Hdr *) Ip6BufRead (pBuf,
                                                           (UINT1 *) &ip6Hdr, 0,
                                                           (UINT4) (sizeof
                                                                    (tIp6Hdr)),
                                                           0);

                            if (pIp6 == NULL)
                            {
                                Ip6BufRelease (pHlParams->u4ContextId,
                                               pBuf, FALSE, IP6_CORE_SUBMODULE);
                                Ip6RelMem (pHlParams->u4ContextId,
                                           (UINT2) gIp6GblInfo.i4ParamId,
                                           (UINT1 *) pHlParams);
                                break;
                            }
                            Ip6AddrCopy (&pHlParams->Ip6SrcAddr,
                                         &pIp6->srcAddr);
                            Ip6AddrCopy (&pHlParams->Ip6DstAddr,
                                         &pIp6->dstAddr);
                            pHlParams->u4Len = pIp6->u2Len;
                            pHlParams->u1Proto = pIp6->u1Nh;
                            pHlParams->u1Hlim = pIp6->u1Hlim;

                            /*
                             * Move the valid offset back to V6 Header length
                             * so that IPv6 Header can be constructed and
                             * prepeneded to the data packet
                             */
                            CRU_BUF_Move_ValidOffset (pBuf, sizeof (tIp6Hdr));
                            IP6_BUF_READ_OFFSET (pBuf) = 0;
                        }

                        pIp6Cxt = gIp6GblInfo.apIp6Cxt[pHlParams->u4ContextId];
                        pIf6 = IP6_INTERFACE (pHlParams->u4Index);

                        if (pIp6Cxt == NULL)
                        {
                            Ip6BufRelease (pHlParams->u4ContextId, pBuf,
                                           FALSE, IP6_CORE_SUBMODULE);
                            Ip6RelMem (pHlParams->u4ContextId,
                                       (UINT2) gIp6GblInfo.i4ParamId,
                                       (UINT1 *) pHlParams);
                            break;
                        }

                        i1AddrType = Ip6AddrType (&pHlParams->Ip6DstAddr);
                        if ((i1AddrType == ADDR6_MULTI)
                            || (i1AddrType == ADDR6_LLOCAL))
                        {
                            Ip6SendInCxt (pIp6Cxt, pIf6,
                                          &pHlParams->Ip6SrcAddr,
                                          &pHlParams->Ip6DstAddr,
                                          pHlParams->u4Len,
                                          pHlParams->u1Proto, pBuf,
                                          pHlParams->u1Hlim);
                        }
                        else
                        {
                            Ip6SendInCxt (pIp6Cxt, NULL,
                                          &pHlParams->Ip6SrcAddr,
                                          &pHlParams->Ip6DstAddr,
                                          pHlParams->u4Len,
                                          pHlParams->u1Proto, pBuf,
                                          pHlParams->u1Hlim);
                        }

                        Ip6RelMem (pIp6Cxt->u4ContextId,
                                   (UINT2) gIp6GblInfo.i4ParamId,
                                   (UINT1 *) pHlParams);
                        break;
#endif
                    default:
                        break;
                }
                if (u4NumOfMsgs == MAX_IP6_PACKET_PROCESS)
                {
                    OsixSendEvent (SELF, (const UINT1 *) IP6_TASK_NAME,
                                   IP6_DGRAM_RECD_EVENT);
                    break;
                }
            }

        }                        /* IP6_DGRAM_RECD_EVENT */

        /* Timer Expiry Notification */
        if (u4Event & IP6_TIMER0_EVENT)
        {
            Ip6TimerHandler ();
        }

        /* PING Packet Reception */
        if (u4Event & IP6_PING_MESG_RECD_EVENT)
        {
            while (OsixReceiveFromQ
                   (SELF, (const UINT1 *) IP6_TASK_PING_INPUT_QUEUE,
                    OSIX_NO_WAIT, 0, &pBuf) == OSIX_SUCCESS)
            {
                IP6_GBL_TRC_ARG1 (IP6_MOD_TRC, BUFFER_TRC, IP6_NAME,
                                  "IP6MAIN:Ip6TaskMain: PING Message Received "
                                  "Event - buffer = %p\n", pBuf);
                Icmp6RcvPingMsg (pBuf);

            }
        }
        /*ECMP6 Timer Expiry handling */
        if (u4Event & IP6_ND_RESOLVE_EVENT)
        {
            while (OsixReceiveFromQ
                   (SELF, (const UINT1 *) IP6_ECMP6_PRT_RESOLVE_QUEUE,
                    OSIX_NO_WAIT, 0, &pBuf) == OSIX_SUCCESS)
            {
                Ip6SendNS (pBuf);
            }

        }

        if (u4Event & IP6_MC_REG_EVENT)
        {
            NetIpv6McastMsgHandle ();
        }

        if (gNd6RedGlobalInfo.u1RedStatus == IP6_TRUE)
        {
            /* A one second timer is used for invoking an event to send dynamic
             * updates from a single point of the main loop. The function
             * Nd6RedSendDynamicCacheInfo is called whenever an event is 
             * received, or when the one second timer expires.
             * In the active node, when the bulk update is not started, the 
             * dynamic update will not be sent. The update will be taken care 
             * in the bulk update message. And the timer is restarted after 
             * sending the dynamic update. */

            if (u4Event & IP6_HA_PEND_BLKUPD_EVENT)
            {
                Nd6RedSendBulkUpdMsg ();
            }

            if (u4Event & IP6_RED_START_TIMER_EVENT)
            {
                RBTreeWalk (gNd6GblInfo.Nd6CacheTable,
                            (tRBWalkFn) Nd6StartTimers, NULL, NULL);
            }

            if ((gNd6RedGlobalInfo.u1NodeStatus == RM_ACTIVE) &&
                (ND6_IS_STANDBY_UP () == OSIX_TRUE))
            {
                u4EntryCount = 0;
                RBTreeCount (gIp6GblInfo.Nd6RedTable, &u4EntryCount);
                if (u4EntryCount > 0)
                {
                    Nd6RedSendDynamicCacheInfo ();
                }
            }
        }
        IP6_TASK_UNLOCK ();
        Ip6GUnLock ();
    }                            /* End of main waiting loop */

}

/******************************************************************************
DESCRIPTION : Identifies the expired timers and invokes 
              the corresponding timer routines
 
INPUTS      : None

OUTPUTS     : None

RETURNS     : None 
******************************************************************************/

VOID
Ip6TimerHandler ()
{

    UINT1               u1TimerId = 0;
    tTmrAppTimer       *pTmrHead = NULL;
    tTmrAppTimer       *pTimerNode = NULL;
    tNd6RtEntry        *pRtEntry = NULL;
    tIp6PrefixNode     *pPrefixInfo = NULL;
    UINT4               u4ContextId = 0;
    UINT4               u4NoOfRetransTimesPerLoop = 0;
    UINT4               u4NoOfReachTimesPerLoop = 0;
    UINT4               u4MaxTimes = 100;

    /*
     * Get all the Expired timer nodes from the IP6 timer list
     */
    TmrGetExpiredTimers (gIp6GblInfo.Ip6TimerListId, &pTmrHead);

    IP6_GBL_TRC_ARG1 (IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                      "IP6MAIN:Ip6TimerHandler: Expired Timer List = %p \n",
                      pTmrHead);

    while (pTmrHead)
    {

        pTimerNode = pTmrHead;
        pTmrHead = TmrGetNextExpiredTimer (gIp6GblInfo.Ip6TimerListId);

        /* Extract the timer-id */
        u1TimerId = ((tIp6Timer *) pTimerNode)->u1Id;

        switch (u1TimerId)
        {

                /* Address - DAD Random Delay, DAD Retransmit */
            case IP6_LLOCAL_RAND_TIMER_ID:
            case IP6_UNI_RAND_TIMER_ID:
            case IP6_LLOCAL_DAD_TIMER_ID:
            case IP6_UNI_DAD_TIMER_ID:
                Ip6AddrTimeout (u1TimerId, (tIp6Timer *) pTimerNode);
                break;

                /* Frag-Reassembly */
            case FRAG_REASM_TIMER_ID:
                Ip6ReasmTimeout ((tIp6Timer *) pTimerNode);
                break;

                /*
                 *  ND - Router Adv, Garbage collect,
                 *       Retransmit, Reachable, DelayProbe
                 */

            case ND6_ROUT_ADV_SOL_TIMER_ID:
            case ND6_RETRANS_TIMER_ID:
                u4NoOfRetransTimesPerLoop++;
            case ND6_REACH_TIMER_ID:
                u4NoOfReachTimesPerLoop++;
            case ND6_DELAY_PROBE_TIMER_ID:
            case ND6_PROXY_LOOP_TIMER_ID:
                Nd6Timeout (u1TimerId, (tIp6Timer *) pTimerNode);
                break;
            case IP6_PMTU_TIMER_ID:
                ip6PmtuTimerExpiryHandler ((tIp6Timer *) pTimerNode);
                break;
            case IP6_BASE_REACH_TIMER_ID:

                Nd6CalNewReachTime (NULL, (UINT4) REACHABLE_TIME);

                Ip6TmrStop (VCM_INVALID_VC, IP6_BASE_REACH_TIMER_ID,
                            gIp6GblInfo.Ip6TimerListId,
                            (tTmrAppTimer *) pTimerNode);

                Ip6TmrStart (VCM_INVALID_VC, IP6_BASE_REACH_TIMER_ID,
                             gIp6GblInfo.Ip6TimerListId,
                             (tTmrAppTimer *) pTimerNode,
                             IP6_RA_DEF_PREF_LIFE_TIME);
                break;

            case IP6_PREF_LIFETIME_TIMER_ID:

                Nd6AddrPrefLifeTimeHandler ((tIp6Timer *) pTimerNode);

                break;

            case IP6_VALID_LIFETIME_TIMER_ID:

                Nd6AddrValidLifeTimeHandler ((tIp6Timer *) pTimerNode);

                break;

            case IP6_DEF_RTR_LIFE_TIMER_ID:

                pRtEntry = IP6_GET_RT_FROM_TIMER (pTimerNode);
                /* Route possible added via NDISC protocol has Timed out. 
                 * Just remove this route form the Routing Table. */
                Nd6TimeOutDefRoute (pRtEntry);
                break;
#ifdef HA_WANTED
            case IP6_PREFIX_TIMER_ID:
            case IP6_PROXY_ND_TIMER_ID:
                Ip6HandleHATimers (u1TimerId, pTimerNode);
                break;
#endif
#ifdef MIP6_WANTED
            case IP6_BIND_CACHE_TIMER_ID:
            case IP6_HA_LIFE_TIMER_ID:
                Ip6HandleHATimers (u1TimerId, pTimerNode);
                break;
#endif

#ifdef MN_WANTED
            case MIP6_BIND_UPDATE_TIMER_ID:
            case MN_DHAD_TIMER_ID:
            case MN_PRFX_SOL_TIMER_ID:
            case MN_MVMNT_DET_RS_TMR_ID:
            case MIP6_RETRANS_TIMER_ID:
            case GUARD_TIMER_ID:
            case MN_RAND_DEF_RTR_TIMER_ID:
                Ip6HandleMNTimers (u1TimerId, pTimerNode);
                break;
#endif

            case IP6_PREFIX_VALID_TIMER_ID:
                /* Prefix has time out. Remove it from the Prefix List */
                pPrefixInfo =
                    IP6_GET_PREFIX_INFO_FROM_VALID_TIMER (pTimerNode,
                                                          tIp6PrefixNode);
                Ip6GetCxtId (pPrefixInfo->u4IfIndex, &u4ContextId);
                Ip6TmrStop (u4ContextId, IP6_PREFIX_VALID_TIMER_ID,
                            gIp6GblInfo.Ip6TimerListId,
                            (tTmrAppTimer *) pTimerNode);

                /* Delete this Prefix Info Entry */
                Ip6RAPrefixDelete (pPrefixInfo->u4IfIndex,
                                   &pPrefixInfo->Ip6Prefix,
                                   pPrefixInfo->u1PrefixLen);
                break;

            case IP6_PREFIX_PREF_TIMER_ID:
                pPrefixInfo =
                    IP6_GET_PREFIX_INFO_FROM_PREFER_TIMER (pTimerNode,
                                                           tIp6PrefixNode);
                Ip6GetCxtId (pPrefixInfo->u4IfIndex, &u4ContextId);

                Ip6TmrStop (u4ContextId, IP6_PREFIX_PREF_TIMER_ID,
                            gIp6GblInfo.Ip6TimerListId,
                            (tTmrAppTimer *) pTimerNode);
                break;

            case IP6_ICMP6_TIMER_ID:
                Icmp6RateLimitTimeout (u1TimerId, (tIp6Timer *) pTimerNode);
                break;

            default:
                IP6_GBL_TRC_ARG1 (IP6_MOD_TRC, DATA_PATH_TRC,
                                  IP6_NAME,
                                  "IP6MAIN:Ip6TimerHandler: Invalid TimerId %d\n",
                                  u1TimerId);
                IP6_GBL_TRC_ARG3 (IP6_MOD_TRC, DATA_PATH_TRC,
                                  IP6_NAME,
                                  "IP6MAIN:Ip6TimerHandler: %s timer error "
                                  "occurred, Type = %d Module = 0x%X ",
                                  ERROR_FATAL_STR, ERR_TIMER_TIMEOUT,
                                  gIp6GblInfo.Ip6TimerListId);
                IP6_GBL_TRC_ARG2 (IP6_MOD_TRC, DATA_PATH_TRC,
                                  IP6_NAME,
                                  "TimerList = 0x%d TimerNode = %p \n",
                                  pTimerNode, IP6_MAIN_SUBMODULE);

                break;

        }                        /* End of switch */
        /* Stagger the timer handling - go and pick up some other event
         * and dont get stuck here
         */
        if (u4NoOfReachTimesPerLoop > u4MaxTimes)
        {
            OsixSendEvent (SELF, (const UINT1 *) IP6_TASK_NAME,
                           IP6_TIMER0_EVENT);
            break;
        }
    }                            /* End of while */

}                                /* End of function */

/******************************************************************************
DESCRIPTION : Acts on the control message from TEST which gives indication of
              interface status. This will be the place to handle ICMPv4
              error messages later. XXXX
 
INPUTS      : None

OUTPUTS     : None

RETURNS     : None 
******************************************************************************/

VOID
Ip6ControlMsg (tCRU_BUF_CHAIN_HEADER * pBuf)
{
    tIp6CfaParams      *pIp6CfaParams = NULL;
    tIp6If             *pIf6 = NULL;
    UINT4               u4Index = 0;
    UINT4               u4DefVlanIndex = CFA_DEFAULT_ROUTER_VLAN_IFINDEX;
    UINT4               u4ContextId = 0;
    UINT1               u1Status = 0;
    UINT1               aifTok[IP6_EUI_ADDRESS_LEN];
    INT1                i1RetVal = 0;

    MEMSET (aifTok, 0, IP6_EUI_ADDRESS_LEN);
    pIp6CfaParams = (tIp6CfaParams *) (VOID *) IP6_BUF_DATA_PTR (pBuf);
    if (pIp6CfaParams == NULL)
    {
        IP6_GBL_TRC_ARG (IP6_MOD_TRC, BUFFER_TRC, IP6_NAME,
                         "IP6MAIN:Ip6ControlMsg: Buffer Non Linear for "
                         "ip6_cfa_params ");
        return;
    }

    VcmGetContextIdFromCfaIfIndex (pIp6CfaParams->u4IfIndex, &u4ContextId);
    IP6_TRC_ARG2 (u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                  "IP6MAIN:Ip6ControlMsg: Rcvd msg from CFA, Type= 0x%X "
                  "IfType= 0x%X ",
                  pIp6CfaParams->u1MsgType, pIp6CfaParams->u1IfType);

    IP6_TRC_ARG2 (u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                  "Ifnum= %d Ckt= %d\n",
                  pIp6CfaParams->u4IfIndex, pIp6CfaParams->u2CktIndex);

    u4Index = pIp6CfaParams->u4IfIndex;
    /*validating the index */
    if (u4Index > IP6_MAX_LOGICAL_IF_INDEX)
    {
        IP6_GBL_TRC_ARG1 (IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                          "IP6MAIN:Ip6ControlMsg: Invalid u4Index,"
                          "index received is %d\n", u4Index);
        return;
    }

    if (gIp6GblInfo.apIp6If[u4Index] != NULL)
    {
        pIf6 = gIp6GblInfo.apIp6If[u4Index];
    }
    else
    {
        pIf6 = NULL;
    }

    /* When a the msg type is IP6_LANIF_ENTRY_VALID, the mentioned interface 
     * has to be newly created. In that case the pIf6 entry should not exist.
     * If it exists return failure. When the msg type IP6_LANIF_ENTRY_UP,
     * the already created interface is enabled in ipv6. In this case, the 
     * pIf6 should have been already created */
    if (pIp6CfaParams->u1MsgType == IP6_LANIF_ENTRY_VALID)
    {
        if (pIf6 != NULL)
        {
            /*Interface is already created */
            IP6_TRC_ARG1 (u4ContextId, IP6_MOD_TRC, BUFFER_TRC, IP6_NAME,
                          "IP6MAIN:Ip6ControlMsg: Interface already exists"
                          " for index %d\n", u4Index);
            return;
        }
    }
    else if (pIf6 == NULL)
    {
        /*Interface does not exist */
        IP6_TRC_ARG1 (u4ContextId, IP6_MOD_TRC, BUFFER_TRC, IP6_NAME,
                      "IP6MAIN:Ip6ControlMsg: Interface already exists"
                      " for index %d\n", u4Index);
        return;
    }

    switch (pIp6CfaParams->u1MsgType)
    {
        case IP6_LANIF_ENTRY_UP:
        case IP6_LANIF_ENTRY_VALID:
            /* Lower layer Interface is administratively enabled. But
             * interface can be operationally UP or DOWN. */
            if (pIp6CfaParams->u4Status == IP6_LANIF_OPER_UP)
            {
                u1Status = OPER_UP;
            }
            else
            {
                u1Status = OPER_DOWN;
            }

            if (pIf6 == NULL)
            {
                /* Lower layer indicates a new interface is created and there
                 * is no matching interface in IP6. So create Ip6 logical
                 * interface. Since there is a one-to-one mapping between
                 * ip6 interface index and IfIndex, ip6if is created for
                 * the IfIndex received from lower layer.
                 */
                i1RetVal = (INT1) Ip6CreateIf (u4Index,
                                               pIp6CfaParams->u4Speed,
                                               pIp6CfaParams->u4HighSpeed,
                                               pIp6CfaParams->u1IfType,
                                               pIp6CfaParams->u2CktIndex,
                                               u1Status, aifTok, IP6_ZERO);

                if (i1RetVal != IP6_SUCCESS)
                {
                    IP6_TRC_ARG1 (u4ContextId, IP6_MOD_TRC,
                                  CONTROL_PLANE_TRC, IP6_NAME,
                                  "IP6MAIN:Ip6ControlMsg: Interface [%d] "
                                  "creation FAILED\n", u4Index);
                    return;
                }
                /* For Default Vlan Interface, the admin/oper status
                 * should be up when the switch starts up */
                if (u4Index == u4DefVlanIndex)
                {
                    if (Ip6EnableIf (u4Index) != IP6_SUCCESS)
                    {
                        IP6_TRC_ARG1 (u4ContextId, IP6_MOD_TRC,
                                      CONTROL_PLANE_TRC, IP6_NAME,
                                      "IP6MAIN:Ip6ControlMsg: Interface [%d] "
                                      "creation FAILED\n", u4Index);
                        return;
                    }
                }
                /*For Default Vlan interface ,it should have ipv6 address */
                if ((u4Index == u4DefVlanIndex)
                    && (pIp6CfaParams->u1PrefixLenth != 0))
                {
                    if (SetIpv6IfAddress
                        ((INT4) u4Index, pIp6CfaParams->Ip6Addr,
                         (INT4) pIp6CfaParams->u1PrefixLenth) != SNMP_SUCCESS)
                    {
                        return;
                    }
                }
            }
            else
            {
                /* Valid Interface already exists. May be event describing
                 * some change in the status of the interface. */
                /* Update the lower layer status in IP6 interface structure. */
                pIf6->u1LLOperStatus = u1Status;

                /* Get the New Operational Status */
                u1Status = Ip6ifGetIfOper (pIf6->u1IfType, pIf6->u4Index,
                                           pIf6->u2CktIndex);

                if ((pIf6->u1AdminStatus == ADMIN_DOWN) ||
                    (u1Status == pIf6->u1OperStatus))
                {
                    /* No change in operation status or the IPv6 is not enabled
                     * over this interface. No need for any action. */
                    return;
                }

                /* IPv6 is enabled over this interface and Lower Layer indicates
                 * change in Operational Status. */
                if (u1Status == OPER_UP)
                {
                    /* Interface Oper status is UP. */
                    Ip6IfUp (pIf6);
                    NetIpv6InvokeInterfaceStatusChange (u4Index,
                                                        NETIPV6_ALL_PROTO,
                                                        NETIPV6_IF_UP,
                                                        NETIPV6_INTERFACE_STATUS_CHANGE);
                }
                else
                {
                    /* Interface Oper status is DOWN. */
                    if ((pIf6->u1OperStatus != OPER_DOWN) &&
                        (pIf6->u4CurOperStatus != OPER_DOWN_INPROGRESS))
                    {
                        Ip6IfDown (pIf6);
                        NetIpv6InvokeInterfaceStatusChange (u4Index,
                                                            NETIPV6_ALL_PROTO,
                                                            NETIPV6_IF_DOWN,
                                                            NETIPV6_INTERFACE_STATUS_CHANGE);
                    }
                }
            }
            break;

        case IP6_LANIF_ENTRY_DOWN:
            /* Lower layer Interface is administratively disabled. */
            if (Ip6ifEntryExists (u4Index) == IP6_FAILURE)
            {
                /* Lower layer indicates, interface goes down. But no matching
                 * valid interface available in IP6. */
                return;
            }

            /* Update the Lower Layer Operational Status. */
            pIf6->u1LLOperStatus = OPER_DOWN;

            /*  IP6 Interface need not be disabled when it is already down */
            if ((pIf6->u1OperStatus != OPER_DOWN) &&
                (pIf6->u4CurOperStatus != OPER_DOWN_INPROGRESS))
            {
                Ip6IfDown (pIf6);
                NetIpv6InvokeInterfaceStatusChange (u4Index,
                                                    NETIPV6_ALL_PROTO,
                                                    NETIPV6_IF_DOWN,
                                                    NETIPV6_INTERFACE_STATUS_CHANGE);
            }
            else
            {
                /* Interface is already Down. No need for any action. */
                return;
            }
            break;

        case IP6_LANIF_ENTRY_INVALID:
            /* Lower layer Interface is administratively deleted. */
            if (Ip6ifEntryExists (u4Index) == IP6_FAILURE)
            {
                return;
            }

            Ip6DeleteIf (pIf6->u4Index);
            break;

        case IP6_LANIF_MTU_CHANGE:
            /* Interface MTU Change has triggered a change in Interface
             * Operational Status */
            if (Ip6ifEntryExists (u4Index) == IP6_FAILURE)
            {
                /* No matching valid interface available in IP6. */
                return;
            }

            u1Status = Ip6ifGetIfOper (pIf6->u1IfType, pIf6->u4Index,
                                       pIf6->u2CktIndex);

            /*  IP6 Interface need not be disabled when it is already down */
            if ((u1Status == OPER_DOWN) &&
                ((pIf6->u1OperStatus != OPER_DOWN) &&
                 (pIf6->u4CurOperStatus != OPER_DOWN_INPROGRESS)))
            {
                /* Interface should be made down. */
                Ip6IfDown (pIf6);
                NetIpv6InvokeInterfaceStatusChange (u4Index,
                                                    NETIPV6_ALL_PROTO,
                                                    NETIPV6_IF_DOWN,
                                                    NETIPV6_INTERFACE_STATUS_CHANGE);
            }
            else if ((u1Status == OPER_UP) &&
                     ((pIf6->u1OperStatus == OPER_DOWN) ||
                      (pIf6->u4CurOperStatus == OPER_DOWN_INPROGRESS)))
            {
                /* Interface should be made up. */
                Ip6IfUp (pIf6);
                NetIpv6InvokeInterfaceStatusChange (u4Index,
                                                    NETIPV6_ALL_PROTO,
                                                    NETIPV6_IF_UP,
                                                    NETIPV6_INTERFACE_STATUS_CHANGE);
            }
            break;

        default:
            IP6_TRC_ARG1 (pIf6->pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                          CONTROL_PLANE_TRC, IP6_NAME,
                          "IP6MAIN:Ip6ControlMsg: Invalid message 0x%X from "
                          "CFA\n", pIp6CfaParams->u1MsgType);
            break;
    }

    return;
}

/******************************************************************************
DESCRIPTION : Initialises all the modules and the data structures of IP6 task
 
INPUTS      : None

OUTPUTS     : None

RETURNS     : IP6_SUCCESS      -  Initialisation succeeds
              IP6_FAILURE  -  Initialisation fails
******************************************************************************/

INT4
Ip6TaskInit ()
{
    tOsixQId            QId;
    tVcmRegInfo         VcmRegInfo;
#ifdef NPAPI_WANTED
    UINT4               u4ForwStatus = 0;
#endif

    MEMSET (&gNd6GblInfo, 0, sizeof (tNdGblInfo));
    if (Ip6TaskSemInit () == IP6_FAILURE)
    {
        return (IP6_FAILURE);
    }

    /* memcreate mempool for IP6 */
    if (Ip6SizingMemCreateMemPools () == OSIX_FAILURE)
    {
        return (IP6_FAILURE);
    }
#ifdef TUNNEL_WANTED
    gIp6GblInfo.i4Ip6TunlAccessListPoolId =
        (INT4) IP6MemPoolIds[MAX_IP6_TUNNEL_INTERFACES_SIZING_ID];
    gIp6GblInfo.i4Ip6TunlIfPoolId =
        (INT4) IP6MemPoolIds[MAX_IP6_TUNNEL_INTERFACES_SIZING_ID];
#endif
    gIp6GblInfo.MemPoolId = IP6MemPoolIds[MAX_IP6_PMTU_ENTRIES_SIZING_ID];
    gIp6GblInfo.i4Ip6RaTblPoolId =
        (INT4) IP6MemPoolIds[MAX_IP6_RA_ENTRY_SIZING_ID];
    gIp6GblInfo.i4ParamId = (INT4) IP6MemPoolIds[MAX_IP6_PARAMS_SIZING_ID];
    gIp6GblInfo.i4Ip6CxtPoolId =
        (INT4) IP6MemPoolIds[MAX_IP6_CONTEXT_SIZING_ID];
    gIp6GblInfo.i4Udp6CBPoolId =
        (INT4) IP6MemPoolIds[MAX_IP6_UDP6_PORT_SIZING_ID];
    gIp6GblInfo.i4Ip6FragId =
        (INT4) IP6MemPoolIds[MAX_IP6_FRAG_QUEUES_SIZING_ID];
    gIp6GblInfo.i4Ip6ReasmPoolId =
        (INT4) IP6MemPoolIds[MAX_IP6_REASM_ENTRIES_SIZING_ID];
    gIp6GblInfo.i4Ip6unicastId =
        (INT4) IP6MemPoolIds[MAX_IP6_UNICAST_ADDRESS_SIZING_ID];
    gIp6GblInfo.i4Ip6llocalId =
        (INT4) IP6MemPoolIds[MAX_IP6_LINK_LOCAL_ADDRESS_SIZING_ID];
    gIp6GblInfo.i4MacFilterId =
        (INT4) IP6MemPoolIds[MAX_IP6_MAC_FILTER_NODE_SIZING_ID];
    gIp6GblInfo.i4Ip6AddrProfPoolId =
        (INT4) IP6MemPoolIds[MAX_IP6_ADDR_PROFILES_SIZING_ID];
    gIp6GblInfo.i4Ip6mcastId =
        (INT4) IP6MemPoolIds[MAX_IP6_MCAST_ADDRESS_SIZING_ID];
    gIp6GblInfo.i4Ip6PrefListId =
        (INT4) IP6MemPoolIds[MAX_IP6_PREFIX_ADDRESS_SIZING_ID];
    gIp6GblInfo.i4Ip6IfPoolId =
        (INT4) IP6MemPoolIds[MAX_IP6_INTERFACES_SIZING_ID];
    gIp6GblInfo.i4Icmp6IfPoolId =
        (INT4) IP6MemPoolIds[MAX_IP6_ICMP6_STATS_NODE_SIZING_ID];
    gIp6GblInfo.i4AnycastDstId =
        (INT4) IP6MemPoolIds[MAX_IP6_ANYCAST_ND_CACHE_ENTRIES_SIZING_ID];
    gIp6GblInfo.i4Ip6PolicyPrefixListId =
        (INT4) IP6MemPoolIds[MAX_IP6_POLICY_TABLE_ENTRIES_SIZING_ID];
    gIp6GblInfo.i4Ip6SrcAddrListId =
        (INT4) IP6MemPoolIds[MAX_IP6_SRC_ADDR_ENTRIES_SIZING_ID];
    gIp6GblInfo.i4Ip6DstAddrListId =
        (INT4) IP6MemPoolIds[MAX_IP6_DST_ADDR_ENTRIES_SIZING_ID];
    gNd6GblInfo.i4Nd6CacheId =
        (INT4) IP6MemPoolIds[MAX_IP6_ND6_CACHE_ENTRIES_SIZING_ID];
    gNd6GblInfo.i4Nd6cLanId =
        (INT4) IP6MemPoolIds[MAX_IP6_ND6_CACHE_LAN_ENTRIES_SIZING_ID];
    gNd6GblInfo.i4Nd6ProxyListId =
        (INT4) IP6MemPoolIds[MAX_IP6_ND6_PROXYLIST_ENTRIES_SIZING_ID];
    gNd6GblInfo.u4Ip6NdRedirectDataPoolId =
        IP6MemPoolIds[MAX_IP6_ND_REDIRECT_DATA_SIZING_ID];

    /* Update the Current Value of Maximum allowed IP6 context 
     * This is minimum of MAX_IP6_CONTEXT_LIMIT and 
     * FsIP6SizingParams[MAX_IP6_CONTEXT_SIZING_ID].u4PreAllocatedUnits */
    gIp6GblInfo.u4MaxContextLimit =
        (MAX_IP6_CONTEXT_LIMIT <
         FsIP6SizingParams[MAX_IP6_CONTEXT_SIZING_ID].
         u4PreAllocatedUnits ? MAX_IP6_CONTEXT_LIMIT :
         FsIP6SizingParams[MAX_IP6_CONTEXT_SIZING_ID].u4PreAllocatedUnits);
    /* Update the Current Value of Maximum allowed IP6 interfaces (includes tunnel interfaces)
     * This is minimum of MAX_IP6_INTERFACES_LIMIT and 
     * FsIP6SizingParams[MAX_IP6_INTERFACES_SIZING_ID].u4PreAllocatedUnits */
    gIp6GblInfo.u4MaxIp6IfLimit = MAX_IP6_INTERFACES_LIMIT;
    /* Update the Current Value of Maximum allowed address profile entries
     * This is minimum of MAX_IP6_ADDR_PROFILES_LIMIT and
     * FsIP6SizingParams[MAX_IP6_ADDR_PROFILES_SIZING_ID].u4PreAllocatedUnits */
    gIp6GblInfo.u4MaxAddrProfileLimit =
        (MAX_IP6_ADDR_PROFILES_LIMIT <
         FsIP6SizingParams[MAX_IP6_ADDR_PROFILES_SIZING_ID].
         u4PreAllocatedUnits ? MAX_IP6_ADDR_PROFILES_LIMIT :
         FsIP6SizingParams[MAX_IP6_ADDR_PROFILES_SIZING_ID].
         u4PreAllocatedUnits);

    gIp6GblInfo.i4ND6RouteId =
        (INT4) IP6MemPoolIds[MAX_IP6HOST_ND6_ROUTES_SIZING_ID];
    /*Create MEM Pools for Scoped Addres Architecture - RFC 4007 Start */
    gIp6GblInfo.i4Ip6IfzonePoolId =
        (INT4) IP6MemPoolIds[MAX_IP6_INTERFACE_SCOPE_ZONE_SIZING_ID];
    gIp6GblInfo.i4Ip6ScopeZonePoolId =
        (INT4) IP6MemPoolIds[MAX_IP6_SCOPE_ZONES_SIZING_ID];
    gIp6GblInfo.i4Ip6BufPoolId =
        (INT4) IP6MemPoolIds[MAX_IP6_BUFFER_ENTRIES_SIZING_ID];
    gIp6GblInfo.i4Ip6Nd6PoolId =
        (INT4) IP6MemPoolIds[MAX_ND6_QUEUE_MSG_SIZING_ID];
    gIp6GblInfo.i4Ip6Nd6DynMsgPoolId =
        (INT4) IP6MemPoolIds[MAX_ND6_DYN_MSG_SIZING_ID];
    gIp6GblInfo.i4Ip6IsZoneIndexValidPoolId =
        (INT4) IP6MemPoolIds[MAX_IP6_SCOPE_ZONES_VALID_SIZING_ID];
    /* Mem Pool for Src List of Proxied NS */
    gIp6GblInfo.i4Ip6Nd6NsSrcPoolId =
        (INT4) IP6MemPoolIds[MAX_IP6_ND6_NS_SRC_SIZING_ID];
    gIp6GblInfo.i4Ip6Nd6SecurePoolId =
        (INT4) IP6MemPoolIds[MAX_IP6_SEND_SIZING_ID];
    gIp6GblInfo.i4Ip6RARouteInfoPoolId =
        (INT4) IP6MemPoolIds[MAX_IP6_RA_ROUTEINFO_TABLE_SIZING_ID];
    gIp6GblInfo.u4Ip6MCastPoolId = IP6MemPoolIds[MAX_IP6_MC_REG_MSG_SIZING_ID];

    /*Create MEM Pools for Scoped Addres Architecture - RFC 4007 End */

    if (Ip6Init () == IP6_FAILURE)
    {
        return (IP6_FAILURE);
    }

    /* Add IP6 timer list to TMO timer block */
    if (Ip6TimerInit () == IP6_FAILURE)
    {
        return (IP6_FAILURE);
    }
    if (gNd6RedGlobalInfo.u1RedStatus == IP6_TRUE)
    {
        if (OsixCreateQ
            ((const UINT1 *) IP6_RM_PKT_QUEUE, IP6_Q_DEPTH_DEF,
             0, &QId) != OSIX_SUCCESS)

        {
            IP6_GBL_TRC_ARG (IP6_MOD_TRC, ALL_FAILURE_TRC, IP6_NAME,
                             "IP6MAIN:Ip6SpawnTasks:IP6_RM_PKT_QUEUE Creation "
                             "Failed \n");
            /* Queue will be deleted in Ip6Shutdown function */
            return IP6_FAILURE;
        }

        if (Nd6RedInitGlobalInfo () == OSIX_FAILURE)
        {
            IP6_GBL_TRC_ARG (IP6_MOD_TRC, ALL_FAILURE_TRC, IP6_NAME,
                             "IP6MAIN:Ip6SpawnTasks:Nd6RedInitGlobalInfo "
                             "Failed \n");
            /* Queue will be deleted in Ip6Shutdown function */
            return IP6_FAILURE;
        }

    }

    /* Initialise Logical interface table */
    if (Ip6IfInit () == IP6_FAILURE)
    {
        return (IP6_FAILURE);
    }

    /* Initialise Address information block */
    if (Ip6AddrInit () == IP6_FAILURE)
    {
        return (IP6_FAILURE);
    }

    /* Initialise NDCache table */
    if (Nd6Init () == IP6_FAILURE)
    {
        return (IP6_FAILURE);
    }

    /* Initialise UDP Port table */
    if (Udp6Init () == IP6_FAILURE)
    {
        return (IP6_FAILURE);
    }

    /* Initialise PMTU  */
    if (ip6PmtuInit () == IP6_FAILURE)
    {
        return (IP6_FAILURE);
    }

#ifdef MIP6_WANTED
    Mip6Init ();
#endif

    /* Initialise Scope-Zone  */
    if (Ip6ZoneInit () == IP6_FAILURE)
    {
        return (IP6_FAILURE);
    }

    if (Ip6HandleCreateContext (IP6_DEFAULT_CONTEXT) == IP6_FAILURE)
    {
        return IP6_FAILURE;
    }

    /*Register with VCM to receive context status notifications */
    MEMSET (&VcmRegInfo, 0, sizeof (tVcmRegInfo));
    VcmRegInfo.u1ProtoId = IP6_FWD_PROTOCOL_ID;
    VcmRegInfo.pIfMapChngAndCxtChng = Ip6NotifyContextStatus;
    VcmRegInfo.u1InfoMask = VCM_CONTEXT_CREATE | VCM_CONTEXT_DELETE;
    if (VcmRegisterHLProtocol (&VcmRegInfo) == VCM_FAILURE)
    {
        return IP6_FAILURE;
    }
    Ip6Enable ();

#ifdef NPAPI_WANTED
    /* Configure the initial value of the Forward status in the 
     * default virtual router in the np level */
    u4ForwStatus = Ip6GetDefForwardStatus ();
    if (Ipv6FsNpIpv6UcRouting (IP6_DEFAULT_CONTEXT, u4ForwStatus) ==
        FNP_FAILURE)
    {
        return (IP6_FAILURE);
    }
#endif

    /* Register with lower layer */
    Ip6RegisterLL ();
    gIp6GblInfo.i4NdCacheTimeout = ND_DEF_CACHE_TIMEOUT;
    /*
     * IPv6 TASK has been created successfully. Enable the IPv6 global
     * status to process packets/request from other layers.
     */
    gIp6GblInfo.i4Ip6Status = IP6_SUCCESS;

    return (IP6_SUCCESS);
}

/*****************************************************************************/
/* Function     : Ip6Disable                                                 */
/*                                                                           */
/*  Description : Deletes the dynamic entries and stops teh timers           */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*****************************************************************************/
VOID
Ip6Disable (VOID)
{
    tNd6CacheEntry     *pNd6Cache = NULL;
    tNd6CacheEntry      Nd6CacheNode;
    UINT4               u4Index = 0;
    tIp6If             *pIf6 = NULL;

    pNd6Cache = RBTreeGetFirst (gNd6GblInfo.Nd6CacheTable);

    while (pNd6Cache != NULL)
    {
        MEMSET (&Nd6CacheNode, 0, sizeof (tNd6CacheEntry));
        Nd6CacheNode.addr6 = pNd6Cache->addr6;
        u4Index = pNd6Cache->pIf6->u4Index;
        Nd6CacheNode.pIf6 = Ip6ifGetEntry (u4Index);

        if ((pNd6Cache->u1ReachState != ND6C_STATIC_NOT_IN_SERVICE) &&
            (pNd6Cache->u1ReachState != ND6C_STATIC))
        {
            Nd6CancelCacheTimer (pNd6Cache);
            Nd6PurgeCache (pNd6Cache);
        }
        pNd6Cache = RBTreeGetNext (gNd6GblInfo.Nd6CacheTable,
                                   (tRBElem *) & Nd6CacheNode, NULL);
    }

    ResetNDCounters ();
    IP6_IF_SCAN (u4Index, (UINT4) IP6_MAX_LOGICAL_IF_INDEX)
    {
        pIf6 = gIp6GblInfo.apIp6If[u4Index];
        if (Ip6ifEntryExists (u4Index) != IP6_SUCCESS)
        {
            continue;
        }
        Ip6TmrStop (pIf6->pIp6Cxt->u4ContextId,
                    ND6_PROXY_LOOP_TIMER_ID, gIp6GblInfo.Ip6TimerListId,
                    &(pIf6->proxyLoopTimer.appTimer));
        pIf6->u1NDProxyOperStatus = ND6_PROXY_OPER_DOWN;
    }

#ifdef NPAPI_WANTED
    Ipv6FsNpIpv6Deinit ();
#endif /* NPAPI_WANTED */

    SYS_LOG_DEREGISTER ((UINT4) gi4Ip6SysLogId);
}

/*****************************************************************************/
/* Function     : Ip6Enable                                                  */
/*                                                                           */
/*  Description : Enables IP6, Registers with Higher layers                  */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Success / Failure                                          */
/*****************************************************************************/
INT4
Ip6Enable (VOID)
{
    UINT4               u4Index = 0;
    tIp6If             *pIf6 = NULL;

#ifdef NPAPI_WANTED
    /* Initialise the NPAPI Related data structures. */
    if (Ipv6FsNpIpv6Init () == FNP_FAILURE)
    {
        return (IP6_FAILURE);
    }
#endif

    gi4Ip6SysLogId = SYS_LOG_REGISTER ((UINT1 *) IP6_TASK_NAME,
                                       SYSLOG_CRITICAL_LEVEL);

    IP6_IF_SCAN (u4Index, (UINT4) IP6_MAX_LOGICAL_IF_INDEX)
    {
        pIf6 = gIp6GblInfo.apIp6If[u4Index];
        if (Ip6ifEntryExists (u4Index) != IP6_SUCCESS)
        {
            continue;
        }
        if (pIf6->u1NDProxyAdminStatus == ND6_IF_PROXY_ADMIN_UP)
        {
            pIf6->u1NDProxyOperStatus = ND6_PROXY_OPER_UP;
        }
    }
    return IP6_SUCCESS;

}

/******************************************************************************
DESCRIPTION : Initialise Global Parameters
 
INPUTS      : None

OUTPUTS     : None

RETURNS     : IP6_SUCCESS      -  Initialisation succeeds
              IP6_FAILURE  -  Initialisation fails
******************************************************************************/

INT4
Ip6Init ()
{
    gIp6GblInfo.u4NextProfileIndex = 1;
    gIp6GblInfo.u4MaxAssignedProfileIndex = 1;
    gIp6GblInfo.u1RFC5095Compatibility = IP6_RFC5095_COMPATIBLE;

    return (IP6_SUCCESS);

}

/******************************************************************************
 * DESCRIPTION : IP6 Timer Initialisation
 *
 * INPUTS      : None
 *
 * OUTPUTS     : None
 *
 * RETURNS     : IP6_SUCCESS      
 ******************************************************************************/
INT4
Ip6TimerInit ()
{
    if (TmrCreateTimerList ((const UINT1 *) IP6_TASK_NAME, IP6_TIMER0_EVENT,
                            NULL, &gIp6GblInfo.Ip6TimerListId) == TMR_FAILURE)
    {
        return IP6_FAILURE;
    }
    return (IP6_SUCCESS);
}

/******************************************************************************
 * DESCRIPTION : IP6 Semaphore Initialisation
 *
 * INPUTS      : None
 *
 * OUTPUTS     : None
 *
 * RETURNS     : IP6_SUCCESS      
 ******************************************************************************/
INT4
Ip6TaskSemInit ()
{
    tOsixSemId          SemId = 0;

    if (OsixCreateSem ((const UINT1 *) IP6_TASK_SEM_NAME,
                       IP6_SEM_CREATE_INIT_CNT, 0, &SemId) != OSIX_SUCCESS)
    {
        IP6_GBL_TRC_ARG (IP6_MOD_TRC, ALL_FAILURE_TRC, IP6_NAME,
                         "IP6MAIN: Ip6Init :Semaphore Creation Failed \n");
        return IP6_FAILURE;
    }

    gIp6SemTaskLockId = SemId;
    if (OsixCreateSem ((const UINT1 *) IP6_TASK_GLOBAL_SEM_NAME,
                       IP6_SEM_CREATE_INIT_CNT, 0, &SemId) != OSIX_SUCCESS)
    {
        IP6_GBL_TRC_ARG (IP6_MOD_TRC, ALL_FAILURE_TRC, IP6_NAME,
                         "IP6MAIN: Ip6Init :Semaphore Creation Failed \n");
        return IP6_FAILURE;
    }

    return (IP6_SUCCESS);
}

 /***************************************************************************/
 /*                                                                         */
 /*  Function Name: Ip6TaskEnqueuePkt                                       */
 /*                                                                         */
 /*  Description  : Enqueues the packet to IP6 task.                        */
 /*                 should be called by lower layer protcols.               */
 /*                                                                         */
 /*  Input(s)     : pBuf - Pointer to the buffer.                           */
 /*                                                                         */
 /*  Output(s)    : None.                                                   */
 /*                                                                         */
 /*  Returns      :  IP6_SUCCESS - if enquing is successful.                    */
 /*               :  IP6_FAILURE - otherwise.                                   */
 /*                                                                         */
 /***************************************************************************/
 /**********************************************************************/
 /****   $$TRACE_PROCEDURE_NAME  =  Ip6TaskEnqueuePkt               ****/
 /****   $$TRACE_PROCEDURE_LEVEL =  INTMD                           ****/
 /**********************************************************************/

INT4
Ip6TaskEnqueuePkt (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex,
                   UINT1 *pu1MacAddr)
{
    tLlToIp6Params     *pParams = NULL;
#ifdef IP6_WANTED
    tIp6Addr            Ip6Addr;
#endif
    UINT4               u4DestIP = 0;
    UINT4               u4ContextId = IP6_ZERO;
    UINT2               u2Offset = 0;

    UINT1               u1NHdr = 0;
    UINT1               au1McastHwAddr[IP6_THREE];
    UINT1               au1BcastHwaddr[MAC_ADDR_LEN] =
        { IP6_UINT1_ALL_ONE, IP6_UINT1_ALL_ONE, IP6_UINT1_ALL_ONE,
        IP6_UINT1_ALL_ONE, IP6_UINT1_ALL_ONE, IP6_UINT1_ALL_ONE
    };
    UINT1               u1PktType = 0;
    UINT1               u1PktFormat = 0;
#ifdef TUNNEL_WANTED
    UNUSED_PARAM (u4ContextId);
#endif
    if (gIp6GblInfo.i4Ip6Status == IP6_FAILURE)
    {
        /* IP6 Task Not enabled */
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return IP6_FAILURE;
    }

    if (Ip6ifEntryExists (u4IfIndex) == IP6_FAILURE)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return IP6_FAILURE;
    }

    if (gIp6GblInfo.apIp6If[u4IfIndex]->u1OperStatus != OPER_UP)
    {
        /* Interface is operationally Down. Drop all packets. */
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return IP6_FAILURE;
    }

#ifdef TUNNEL_WANTED
    if ((gIp6GblInfo.apIp6If[u4IfIndex]->u1IfType
         != IP6_TUNNEL_INTERFACE_TYPE) && (pu1MacAddr != NULL))
#else

    u4ContextId = gIp6GblInfo.apIp6If[u4IfIndex]->pIp6CurrCxt.u4ContextId;
    if (pu1MacAddr != NULL)
#endif
    {
        /* Check for MAC filter only if pu1MacAddr
         * is not NULL. If filtering of packets is done
         * at lower layer, pu1MacAddr can be passed as NULL.
         * For CFA as lower layer, filtering is done
         * at IP6 level.
         */

        /* Get the physical interface from the logical interface */
        if (Ip6BufRead (pBuf, &u1NHdr, IP6_OFFSET_FOR_NEXTHDR_FIELD,
                        sizeof (u1NHdr), TRUE) != NULL)
        {
            if (u1NHdr != NH_H_BY_H)
            {
                if (gIp6GblInfo.apIp6If[u4IfIndex]->u1AdminStatus
                    == ADMIN_INVALID)
                {
                    CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                    return IP6_FAILURE;
                }
            }
        }
    }

    pParams = (tLlToIp6Params *) IP6_GET_MODULE_DATA_PTR (pBuf);

    /* The same effect of using IP6_SET_COMMAND */
    pParams->u1Cmd = IP6_LAYER2_DATA;
    pParams->u4Port = u4IfIndex;
    pParams->u1LinkType = gIp6GblInfo.apIp6If[u4IfIndex]->u1IfType;
    pParams->u1Dos = FALSE;

    au1McastHwAddr[0] = MCAST_MAC_ADDR_OCTET_1;
    au1McastHwAddr[1] = MCAST_MAC_ADDR_OCTET_2;

    if ((pu1MacAddr != NULL) &&
        ((!(MEMCMP (pu1MacAddr, au1BcastHwaddr, MAC_ADDR_LEN))) ||
         (!(MEMCMP (pu1MacAddr, au1McastHwAddr, IP6_TWO)))))
    {
        pParams->u1Dos = TRUE;
    }

#ifdef TUNNEL_WANTED
    /* In case of tunnel interface, the source address and destination
     * address are stored in CRU_BUF_MODULE_DATA, Need to rearrange this
     * because IP6, always expects packet source address to be at
     * ModuleData.u4Reserved3 */
    CRU_BUF_Set_U4Reserved3 (pBuf, (CRU_BUF_Get_U4Reserved1 (pBuf)));
#endif

    IP6_BUF_READ_OFFSET (pBuf) = 0;
    IP6_BUF_WRITE_OFFSET (pBuf) = 0;

    /* The Ethernet address is already stripped in the buffer,
     *     Assuming the buffer is of the below format
     *     |----------|-----------|
     *     |   IPv6   |   ICMPv6  |
     *     |----------|-----------|
     *      The offset to check the format of the packet (ICMPv6 in this case),
     *      and the type of the packet (Neighbor Advertisement), is directly
     *      given */

    CRU_BUF_Copy_FromBufChain (pBuf, &u1PktFormat, ICMP6_OFFSET, 1);
    CRU_BUF_Copy_FromBufChain (pBuf, &u1PktType, ICMP6_NS_NA_OFFSET, 1);
    if (((u1PktType == ICMP6_NEIGHBOUR_ADVERTISEMENT)
         || (u1PktType == ICMP6_NEIGHBOUR_SOLICITATION))
        && (u1PktFormat == ICMP6_FORMAT))
    {
        /* Enqueing NA Packet from lower layer */
        if (OsixSendToQ (SELF,
                         (const UINT1 *) IP6_TASK_NS_NA_QUEUE,
                         pBuf, OSIX_MSG_URGENT) != OSIX_SUCCESS)
        {
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            return (IP6_FAILURE);
        }
    }
    else
    {
        /* Enquing to IP6 from lower layer */
        if (OsixSendToQ (SELF,
                         (const UINT1 *) IP6_TASK_IP_INPUT_QUEUE,
                         pBuf, OSIX_MSG_URGENT) != OSIX_SUCCESS)
        {
            MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));
            u2Offset = (UINT2) (u2Offset + IP6_OFFSET_FOR_DESTADDR_FIELD);
            CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u4DestIP,
                                       u2Offset, sizeof (UINT4));
            u4DestIP = OSIX_NTOHL (u4DestIP);
            Ip6Addr.u4_addr[0] = u4DestIP;

            u2Offset = (UINT2) (u2Offset + sizeof (UINT4));
            CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u4DestIP,
                                       u2Offset, sizeof (UINT4));
            u4DestIP = OSIX_NTOHL (u4DestIP);
            Ip6Addr.u4_addr[1] = u4DestIP;

            u2Offset = (UINT2) (u2Offset + sizeof (UINT4));
            CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u4DestIP,
                                       u2Offset, sizeof (UINT4));
            u4DestIP = OSIX_NTOHL (u4DestIP);
            Ip6Addr.u4_addr[2] = u4DestIP;

            u2Offset = (UINT2) (u2Offset + sizeof (UINT4));
            CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u4DestIP,
                                       u2Offset, sizeof (UINT4));
            u4DestIP = OSIX_NTOHL (u4DestIP);
            Ip6Addr.u4_addr[3] = u4DestIP;

            /* Remove the Entry created in the DLF Hash Table */
#if defined (NPAPI_WANTED) && defined (CFA_WANTED)
            Ipv6FsCfaHwRemoveIp6NetRcvdDlfInHash (u4ContextId, Ip6Addr,
                                                  IP6_MAX_PREFIX_LEN);
#else
            UNUSED_PARAM (u4ContextId);
#endif

            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            return (IP6_FAILURE);
        }
    }

    if (OsixSendEvent (SELF, (const UINT1 *) IP6_TASK_NAME,
                       IP6_DGRAM_RECD_EVENT) != OSIX_SUCCESS)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return (IP6_FAILURE);
    }
    return (IP6_SUCCESS);
}

/******************************************************************************
 * DESCRIPTION : This routine Registers Proprietary IPv6 MIB with Future SNMP
 * INPUTS      : None
 * OUTPUTS     : None
 * RETURNS     : IP6_SUCCESS/IP6_FAILURE
 * NOTES       :
 ******************************************************************************/

/******************************************************************************
 * DESCRIPTION :  This function is called when ever an icmp packet is being
 *                forwarded by the router. If the icmp packet is of an error 
 *                message of type equal to PKT_TOO_BIG this function returns
 *                higher layer protocol of error packet and pointer to Pmtu.
 *
 * INPUTS      :  pBuf : pointer to the message buffer containing 
 *                ipv6|icmp6|ipv6|.....
 *
 * OUTPUTS     : pu1Nhdr, pu4Pmtu
 *
 * RETURNS     : IP6_SUCCESS or IP6_FAILURE
 *
 * NOTES       : None
 ******************************************************************************/

INT4
Ip6GetProtoFromIcmp6ErrPktInCxt (tIp6Cxt * pIp6Cxt,
                                 tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 *pu1Nhdr,
                                 UINT4 *pu4Pmtu)
{

    if (Ip6GetPmtuFromIcmpErrPktInCxt (pIp6Cxt, pBuf, pu4Pmtu) == IP6_FAILURE)
    {
        return IP6_FAILURE;
    }

    IP6_BUF_READ_OFFSET (pBuf) = IP6_HDR_LEN + sizeof (tIcmp6ErrMsg);
    return (Ip6GetHlProtocolInCxt (pIp6Cxt->u4ContextId, pBuf, pu1Nhdr, NULL));
}

/******************************************************************************
 * DESCRIPTION :  This function is called when ever an ip6 packet is being      
 *                forwarded by the router. This function scans through the
 *                received packet and returns pointer to the next header
 *                field.
 *
 * INPUTS      :  pBuf : pointer to the message buffer containing 
 *                ipv6|icmp6|ipv6|.....
 *
 * OUTPUTS     : pu1Nhdr, pu2Offset
 *
 * RETURNS     : IP6_SUCCESS or IP6_FAILURE
 *
 * NOTES       : None
 ******************************************************************************/

INT4
Ip6GetHlProtocol (tCRU_BUF_CHAIN_HEADER * pBuf,
                  UINT1 *pu1Nhdr, UINT2 *pu2Offset)
{
    return (Ip6GetHlProtocolInCxt (IP6_DEFAULT_CONTEXT,
                                   pBuf, pu1Nhdr, pu2Offset));
}

/******************************************************************************
 * DESCRIPTION :  This function is called when ever an ip6 packet is being      
 *                forwarded by the router. This function scans through the
 *                received packet and returns pointer to the next header
 *                field.
 *
 * INPUTS      :  pBuf : pointer to the message buffer containing 
 *                ipv6|icmp6|ipv6|.....
 *
 * OUTPUTS     : pu1Nhdr, pu2Offset
 *
 * RETURNS     : IP6_SUCCESS or IP6_FAILURE
 *
 * NOTES       : None
 ******************************************************************************/

INT4
Ip6GetHlProtocolInCxt (UINT4 u4ContextId, tCRU_BUF_CHAIN_HEADER * pBuf,
                       UINT1 *pu1Nhdr, UINT2 *pu2Offset)
{
    UINT1               u1Nhdr = 0;
    UINT1              *pu1TmpNhdr = NULL;
    UINT2               u2OldLen = 0, u2Len = IP6_HDR_LEN;
    /* Updating processed length 
     * to IPv6 Header length.
     */
    UINT1              *pu1Len = NULL, u1Len = 0;
    UINT1               u1Copy = TRUE;

    /* To handle warnings in case of Trace is not defined */
    UNUSED_PARAM (u4ContextId);

    *pu1Nhdr = 0;
    pu1TmpNhdr = (UINT1 *) Ip6BufRead (pBuf, &u1Nhdr,
                                       (IP6_BUF_READ_OFFSET (pBuf) +
                                        IP6_OFFSET_FOR_NEXTHDR_FIELD) /* 6 */ ,
                                       sizeof (UINT1), u1Copy);

    /* Moving the valid offset to the start of next header */
    u2Len = (UINT2) (u2Len + IP6_BUF_READ_OFFSET (pBuf) - 1);

    while ((pu1TmpNhdr != NULL) && (*pu1TmpNhdr != NH_NO_NEXT_HDR))
    {
        switch (*pu1TmpNhdr)
        {
            case NH_AUTH_HDR:
            case NH_ESP_HDR:
            case NH_UDP6:
            case NH_TCP6:
            case NH_ICMP6:
            case NH_MOBILITY_HDR:

                *pu1Nhdr = *pu1TmpNhdr;
                if (pu2Offset != NULL)
                {
                    *pu2Offset = u2Len;
                }
                return IP6_SUCCESS;

            case NH_FRAGMENT_HDR:
                u2OldLen = u2Len;
                u2Len += (UINT2) (sizeof (tIp6FragHdr));
                break;

            case NH_H_BY_H:
            case NH_DEST_HDR:
            case NH_ROUTING_HDR:
                u2OldLen = u2Len;
                pu1Len =
                    (UINT1 *) Ip6BufRead (pBuf, &u1Len, (UINT4) (u2Len + 1),
                                          sizeof (UINT1), u1Copy);
                if (pu1Len == NULL)
                {
                    IP6_TRC_ARG1 (u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC,
                                  IP6_NAME,
                                  "IP6FWD:Ip6GetHlProtocol: BufRead Failed for the forwarded pkt%x \n",
                                  pBuf);
                    return IP6_FAILURE;
                }
                u2Len += (UINT2) ((*pu1Len + 1) * BYTE_LENGTH);
                break;
            default:

                IP6_TRC_ARG1 (u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                              "IP6FWD: Ip6GetHlProtocol:Unknown protocol = %d found for the forwarded pkt \n",
                              *pu1TmpNhdr);

                return IP6_FAILURE;
        }
        IP6_BUF_READ_OFFSET (pBuf) = u2OldLen;
        pu1TmpNhdr = (UINT1 *) Ip6BufRead (pBuf, &u1Nhdr, u2OldLen,
                                           sizeof (UINT1), u1Copy);

        if (pu1TmpNhdr == NULL)
        {
            IP6_TRC_ARG1 (u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                          "IP6FWD:Ip6GetHlProtocol: BufRead Failed for the forwarded pkt%x \n",
                          pBuf);
            return IP6_FAILURE;
        }

    }
    return IP6_FAILURE;
}

/******************************************************************************
 * DESCRIPTION : This routine gets the PMTU value for the give pBuf.
 * INPUTS      : Pointer to the Buffer(pBuf)
 * OUTPUTS     : PMTU value (pu4Pmtu)
 * RETURNS     : IP6_SUCCESS / IP6_FAILURE 
 * NOTES       :
 ******************************************************************************/
INT4
Ip6GetPmtuFromIcmpErrPktInCxt (tIp6Cxt * pIp6Cxt,
                               tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 *pu4Pmtu)
{
    tIcmp6ErrMsg       *pIcmp6 = NULL, icmp6Hdr;
    UINT2               u2Len = IP6_HDR_LEN;

    /* To handle warnings in case of Trace is not defined */
    UNUSED_PARAM (pIp6Cxt);

    IP6_BUF_READ_OFFSET (pBuf) = u2Len;
    pIcmp6 = (tIcmp6ErrMsg *) Ip6BufRead (pBuf, (UINT1 *) &icmp6Hdr,
                                          u2Len, sizeof (tIcmp6ErrMsg), TRUE);
    if (pIcmp6 == NULL)
    {
        IP6_TRC_ARG (pIp6Cxt->u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                     "IP6FWD: Ip6GetPmtuFromIcmpErrPkt:BufRead Fail");

        IP6_TRC_ARG1 (pIp6Cxt->u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC,
                      IP6_NAME,
                      "while extracting icmp6Hdr from the forwarded pkt%x \n",
                      pBuf);
        return IP6_FAILURE;
    }
    if (pIcmp6->icmp6Hdr.u1Type == ICMP6_PKT_TOO_BIG)
    {
        *pu4Pmtu = OSIX_NTOHL (pIcmp6->u4ErrInfo);
        return IP6_SUCCESS;
    }
    return IP6_FAILURE;
}

/******************************************************************************
 * DESCRIPTION : Function to get Layer 4 header in the buffer and offset to 
 *               particular header.
 * INPUTS      : Pointer to the Buffer(pBuf)
 * OUTPUTS     : Pointer to the nexthdr, Pointer to the offset 
 *               to the next header
 * RETURNS     : IP6_SUCCESS / IP6_FAILURE 
 * NOTES       : Function expects buffer to start with first extention header,
 *               it doesn't expect IP6 header to be there in the buffer.
 ******************************************************************************/
INT4
Ip6GetHlProtocolFromHLPktInCxt (tIp6Cxt * pIp6Cxt,
                                tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 *pu1Nhdr,
                                UINT2 *pu2Offset)
{
    UINT1               u1Nhdr = 0;
    UINT1              *pu1TmpNhdr = NULL;
    UINT2               u2OldLen = 0, u2Len = 0;
    UINT1              *pu1Len = NULL, u1Len = 0;
    UINT1               u1Copy = TRUE;

    /* To handle warnings in case of Trace is not defined */
    UNUSED_PARAM (pIp6Cxt);

    u1Nhdr = *pu1Nhdr;
    pu1TmpNhdr = &u1Nhdr;

    while ((pu1TmpNhdr != NULL) && (*pu1TmpNhdr != NH_NO_NEXT_HDR))
    {
        switch (*pu1TmpNhdr)
        {
            case NH_AUTH_HDR:
            case NH_ESP_HDR:
            case NH_UDP6:
            case NH_TCP6:
            case NH_ICMP6:
            case NH_MOBILITY_HDR:

                *pu1Nhdr = *pu1TmpNhdr;
                if (pu2Offset != NULL)
                {
                    *pu2Offset = u2Len;
                }
                return IP6_SUCCESS;

            case NH_FRAGMENT_HDR:
                u2OldLen = u2Len;
                u2Len += (UINT2) (sizeof (tIp6FragHdr));
                break;

            case NH_H_BY_H:
            case NH_DEST_HDR:

            case NH_ROUTING_HDR:
                u2OldLen = u2Len;
                pu1Len =
                    (UINT1 *) Ip6BufRead (pBuf, &u1Len, (UINT4) (u2Len + 1),
                                          sizeof (UINT1), u1Copy);
                if (pu1Len == NULL)
                {
                    IP6_TRC_ARG1 (pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                                  DATA_PATH_TRC, IP6_NAME,
                                  "Ip6GetHlProtocolFromHLPkt: BufRead Failed\n",
                                  pBuf);
                    return IP6_FAILURE;
                }
                u2Len += (UINT2) ((*pu1Len + 1) * BYTE_LENGTH);
                break;
            default:

                IP6_TRC_ARG1 (pIp6Cxt->u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC,
                              IP6_NAME,
                              "Ip6GetHlProtocolFromHLPkt:Unknown protocol = %d"
                              " found for the forwarded pkt \n", *pu1TmpNhdr);

                return IP6_FAILURE;
        }
        IP6_BUF_READ_OFFSET (pBuf) = u2OldLen;
        pu1TmpNhdr = (UINT1 *) Ip6BufRead (pBuf, &u1Nhdr, u2OldLen,
                                           sizeof (UINT1), u1Copy);

        if (pu1TmpNhdr == NULL)
        {
            IP6_TRC_ARG1 (pIp6Cxt->u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC,
                          IP6_NAME,
                          "Ip6GetHlProtocolFromHLPkt: BufRead Failed for the "
                          "forwarded pkt%x \n", pBuf);
            return IP6_FAILURE;
        }

    }
    return IP6_FAILURE;
}

INT4
Ip6DuplicateBuffer (tCRU_BUF_CHAIN_HEADER * pBuf,
                    tCRU_BUF_CHAIN_HEADER ** ppDupBuf, UINT4 u4PktSize)
{

    /* Allocate the Buffer chain for the Duplicate buffer */
    if (((*ppDupBuf) = CRU_BUF_Allocate_MsgBufChain ((u4PktSize), 0)) == NULL)
    {
        return IP6_FAILURE;
    }
    else
    {
        /* Allocate memory for the linear buffer */

        if (CRU_FAILURE ==
            CRU_BUF_Copy_BufChains (*ppDupBuf, pBuf, 0, 0, u4PktSize))
        {
            CRU_BUF_Release_MsgBufChain ((*ppDupBuf), FALSE);
            return IP6_FAILURE;
        }
    }

    return IP6_SUCCESS;
}

/* -------------------------------------------------------------------+
 * Function           : Ip6SendMcastPkt
 *
 * Input(s)           : pBuf - Pointer to the buffer - ipv6 header 
 *                      is already filled in the pBuf.
 *                      tHlToIp6McastParams - Pointer to the multicast params. 
 *
 * Output(s)          : None.
 *
 * Returns            : None. 
 *
 * Action :
 *
 *
 * This function is called when PIM Protocol wants to send an IPv6 packet on one
 * or more interfaces. If the packet is to be sent on many interfaces then the  
 * buffer is duplicated and sent out.
+-------------------------------------------------------------------*/
INT4
Ip6SendMcastPkt (tCRU_BUF_CHAIN_HEADER * pBuf,
                 tHlToIp6McastParams * pIp6McastHlParams)
{
    tCRU_BUF_CHAIN_HEADER *pDupBuf = NULL;    /* duplicate buffer */
    tIp6If             *pIf6 = NULL;
    tIp6Hdr            *pIp6 = NULL, ip6Hdr;
    tPmtuEntry         *pPmtuEntry = NULL;
    tIp6Cxt            *pIp6Cxt = NULL;
    UINT4               u4Index = 0;
    UINT4               u4Mtu = 0;
    UINT4               u4Len = 0;
    UINT4               u4BufLen = 0;
    UINT4               u4IfIndex = 0;
    UINT4               u4TotNumIface = 0;    /* No of interfaces on which this 
                                               packet is to be forwarded. */

    u4TotNumIface = pIp6McastHlParams->pu4OIfList[0];

    /*
     *  Get IPv6 header from the packet 
     * */

    pIp6 = (tIp6Hdr *) Ip6BufRead (pBuf, (UINT1 *) &ip6Hdr, 0,
                                   (UINT4) (sizeof (tIp6Hdr)), 0);
    if (pIp6 == NULL)
    {
        return IP6_FAILURE;
    }

    u4Len = pIp6McastHlParams->u4Len;

    for (u4Index = 0; u4Index < u4TotNumIface; u4Index++)
    {

        u4IfIndex = pIp6McastHlParams->pu4OIfList[u4Index + IP6_TWO];
        /* Adding 2 becos array index 0 - has total no of outgoing interfaces 
           array index 1 -has the incoming interface index */

        /* get pointer to interface, from interface index */

        if (Ip6ifEntryExists (u4IfIndex) == IP6_SUCCESS)
        {
            pIf6 = IP6_INTERFACE (u4IfIndex);
            pIp6Cxt = pIf6->pIp6Cxt;
        }
        else
        {
            /* The mcast protocols sends only the outgoing interface list.
             * When the interface doesnot exist in IP6, the packets are
             * not sent on those interfaces */
            continue;
        }
        u4BufLen = CRU_BUF_Get_ChainValidByteCount (pBuf);
        if (Ip6DuplicateBuffer (pBuf, &pDupBuf, u4BufLen) != IP6_SUCCESS)
        {
            break;
        }

        if (pDupBuf == NULL)
        {
            continue;
        }

        if (pIp6Cxt->u1PmtuEnable == IP6_PMTU_ENABLE)
        {
            ip6PmtuLookupEntryInCxt (pIp6Cxt->u4ContextId, &pIp6->dstAddr,
                                     &pPmtuEntry);
            if (pPmtuEntry != NULL)
            {
                u4Mtu = pPmtuEntry->u4Pmtu;
            }
            else
            {
                pIf6->u1TooBigMsgReceived = OSIX_FALSE;
                u4Mtu = Ip6ifGetMtu (pIf6);
            }
        }
        else
        {
            pIf6->u1TooBigMsgReceived = OSIX_FALSE;
            u4Mtu = Ip6ifGetMtu (pIf6);
        }
        if ((u4Len > u4Mtu) || (pIf6->u1TooBigMsgReceived == OSIX_TRUE))
        {
            IP6_TRC_ARG3 (pIp6Cxt->u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC,
                          IP6_NAME,
                          "Ip6SendMcastPkt: Pkt len %d greater than MTU %d "
                          "Dst IP6 IF = %d\n", u4Len, u4Mtu, pIf6->u4Index);
            if (Ip6FragSend
                (pIf6, &pIp6->dstAddr, NULL, u4Mtu, u4Len, pDupBuf,
                 ADDR6_MULTI) == FAILURE)
            {
                IP6_RELEASE_BUF (pDupBuf, FALSE);
            }
        }
        else
        {
            Ip6ifSend (pIf6, &pIp6->dstAddr, NULL, u4Len, pDupBuf, ADDR6_MULTI);
        }
    }

    /* Free the malloc'ed OifList and the incoming Buffer */
    UtlShMemFreeOifList (pIp6McastHlParams->pu4OIfList);
    IP6_RELEASE_BUF (pBuf, FALSE);

    return (IP6_SUCCESS);

}

/******************************************************************************
 * DESCRIPTION : This function initializes the IP6 context information for the
 *                given context.
 * INPUTS      : u4ContextId - Context to be created
 * OUTPUTS     : None 
 * RETURNS     : IP6_SUCCESS / IP6_FAILURE
 *****************************************************************************/
INT4
Ip6HandleCreateContext (UINT4 u4ContextId)
{
    tIp6Cxt            *pIp6Cxt = NULL;
    tRtm6RegnId         Rtm6RegnId;
    UINT4               u4Cntr = 0;
    UINT1               u1Cntr = 0;
    UINT1               u1ReCntr = 0;

    MEMSET (&Rtm6RegnId, 0, sizeof (tRtm6RegnId));

    if (u4ContextId >= IP6_MAX_INSTANCES)
    {
        IP6_TRC_ARG1 (u4ContextId, IP6_MOD_TRC,
                      CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                      IP6_NAME, "Ip6HandleCreateContext: Invalid context Id"
                      "%d\n", u4ContextId);
        return IP6_FAILURE;
    }
    if (gIp6GblInfo.apIp6Cxt[u4ContextId] != NULL)
    {
        IP6_TRC_ARG1 (u4ContextId, IP6_MOD_TRC,
                      CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                      IP6_NAME, "Ip6HandleCreateContext: Context Information"
                      "already exists for the context %d\n", u4ContextId);
        return IP6_FAILURE;
    }

    gIp6GblInfo.apIp6Cxt[u4ContextId] =
        MemAllocMemBlk ((UINT4) gIp6GblInfo.i4Ip6CxtPoolId);

    if (gIp6GblInfo.apIp6Cxt[u4ContextId] == NULL)
    {
        IP6_TRC_ARG (u4ContextId, IP6_MOD_TRC,
                     CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                     IP6_NAME, "Ip6HandleCreateContext: Memory allocation "
                     "failed for creation of context entry \n");
        return IP6_FAILURE;
    }

    pIp6Cxt = gIp6GblInfo.apIp6Cxt[u4ContextId];

    MEMSET (pIp6Cxt, 0, sizeof (tIp6Cxt));

    /* Initialize the Ip6 Context information */
    pIp6Cxt->u4ContextId = u4ContextId;
    TMO_SLL_Init (&pIp6Cxt->Nd6ProxyList);
    TMO_SLL_Init (&pIp6Cxt->Udp6CxtCbEntry);
    pIp6Cxt->u4Ipv6HopLimit = IP6_DEF_HOP_LIMIT;
    OsixGetSysTime (&(pIp6Cxt->u4DgramFragId));
    pIp6Cxt->i4Nd6CacheMaxRetries = IP6_THREE;
    pIp6Cxt->u1PmtuEnable = IP6_PMTU_ENABLE;
    pIp6Cxt->u1JmbCfgStatus = IP6_JUMBO_ENABLE;
    pIp6Cxt->u4CfgTimeOut = IP6_PMTU_DEF_TIME_OUT_INTERVAL;
    pIp6Cxt->u4ForwFlag = (UINT4) Ip6GetDefForwardStatus ();

    /* Initialise the count of non-global zone created -4007 */
    pIp6Cxt->u4MaxNonGblZoneCount = IP6_ZERO;

    for (u1Cntr = 0; u1Cntr < MAX_IP6_REASM_ENTRIES_LIMIT; u1Cntr++)
    {
        if ((pIp6Cxt->apIp6Ream[u1Cntr] =
             (tIp6ReasmEntry *) (VOID *) Ip6GetMem (u4ContextId,
                                                    gIp6GblInfo.
                                                    i4Ip6ReasmPoolId)) == NULL)
        {
            IP6_TRC_ARG (u4ContextId, IP6_MOD_TRC, ALL_FAILURE_TRC, IP6_NAME,
                         "Ip6HandleCreateContext: Memory allocation failed for"
                         " Reassembly Queue entries\n");
            IP6_TRC_ARG4 (u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                          "%s mem err:, Type = %d  PoolId = %d Memptr = %p",
                          ERROR_FATAL_STR, ERR_MEM_CREATE, NULL, 0);

            for (u1ReCntr = 0; u1ReCntr < u1Cntr; u1ReCntr++)
            {
                Ip6RelMem (u4ContextId, (UINT2) gIp6GblInfo.i4Ip6ReasmPoolId,
                           (UINT1 *) pIp6Cxt->apIp6Ream[u1ReCntr]);
            }
            MemReleaseMemBlock ((UINT4) gIp6GblInfo.i4Ip6CxtPoolId,
                                (UINT1 *) pIp6Cxt);
            return IP6_FAILURE;
        }

        MEMSET (pIp6Cxt->apIp6Ream[u1Cntr], 0, sizeof (tIp6ReasmEntry));
        pIp6Cxt->apIp6Ream[u1Cntr]->u1State = ENTRY_UNUSED;
    }
    /* Initialise the ZoneId entry in the zontext table */
    for (u4Cntr = 0; u4Cntr < IP6_MAX_LOGICAL_IFACES; u4Cntr++)
    {
        pIp6Cxt->ai4Ip6LlocalZoneId[u4Cntr] = IP6_ZONE_INVALID;
        pIp6Cxt->ai4Ip6IntLocalZoneId[u4Cntr] = IP6_ZONE_INVALID;
    }

    /* Register with RTM6 for Local Routes. */
    Rtm6RegnId.u2ProtoId = IP6_LOCAL_PROTOID;
    Rtm6RegnId.u4ContextId = u4ContextId;
    Rtm6Register (&Rtm6RegnId, 0, NULL);

    /* Register with RTM6 for Static Routes. */
    Rtm6RegnId.u2ProtoId = IP6_NETMGMT_PROTOID;
    Rtm6RegnId.u4ContextId = u4ContextId;
    Rtm6Register (&Rtm6RegnId, 0, NULL);

    return IP6_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : This function de-initializes the IP6 context information
 *                for the given context.
 * INPUTS      : u4ContextId - Context to be deleted
 * OUTPUTS     : None 
 * RETURNS     : IP6_SUCCESS / IP6_FAILURE;
 *****************************************************************************/
INT4
Ip6HandleDeleteContext (UINT4 u4ContextId)
{
    tIp6Cxt            *pIp6Cxt = NULL;

    if (u4ContextId >= IP6_MAX_INSTANCES)
    {
        IP6_TRC_ARG1 (u4ContextId, IP6_MOD_TRC,
                      CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                      IP6_NAME, "Ip6HandleCreateContext: Invalid context Id"
                      "%d\n", u4ContextId);
        return IP6_FAILURE;
    }

    pIp6Cxt = gIp6GblInfo.apIp6Cxt[u4ContextId];

    if (pIp6Cxt == NULL)
    {
        IP6_TRC_ARG1 (u4ContextId, IP6_MOD_TRC,
                      CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                      IP6_NAME, "Ip6HandleDeleteContext: Context Information"
                      "already deleted for the context %d\n", u4ContextId);
        return IP6_FAILURE;
    }

    Nd6DeleteContext (pIp6Cxt);

    Ip6ClearReasmEntries (pIp6Cxt);

    Ip6ClearPmtuEntries (pIp6Cxt);

    MEMSET (pIp6Cxt, 0, sizeof (tIp6Cxt));

    MemReleaseMemBlock ((UINT4) gIp6GblInfo.i4Ip6CxtPoolId, (UINT1 *) pIp6Cxt);
    pIp6Cxt = NULL;
    gIp6GblInfo.apIp6Cxt[u4ContextId] = NULL;
    return IP6_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : This function disassociates the given interface from the 
 *                current context to which it is mapped and maps it to the 
 *                new context. The interface deletion notification will be 
 *                given to the higher layer protocols and the IP addresses 
 *                associated with the interface will also be removed.
 * INPUTS      : u4IfIndex     - Interface index.
 *               u4CurrContext - The new context to be mapped to the interface
 *               u4PrevContext - The prev context mapped to the interface
 * OUTPUTS     : None 
 * RETURNS     : None
 *****************************************************************************/
VOID
Ip6HandleIntfMapEvent (UINT4 u4IfIndex, UINT4 u4PrevContext,
                       UINT4 u4CurrContext)
{
    tIp6If             *pIf6 = NULL;
    tIp6Cxt            *pIp6Cxt = NULL;

    if ((u4PrevContext >= IP6_MAX_INSTANCES) ||
        (u4CurrContext >= IP6_MAX_INSTANCES))
    {
        IP6_GBL_TRC_ARG2 (IP6_MOD_TRC,
                          CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          IP6_NAME,
                          "Ip6IfaceMapping:Invalid context Ids %d %d\n",
                          u4PrevContext, u4CurrContext);
        return;
    }

    if (u4IfIndex > IP6_MAX_LOGICAL_IF_INDEX)
    {
        IP6_TRC_ARG1 (u4PrevContext, IP6_MOD_TRC,
                      CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                      IP6_NAME, "Ip6IfaceMapping:Invalid interface %d\n",
                      u4IfIndex);
        return;
    }
    pIf6 = gIp6GblInfo.apIp6If[u4IfIndex];
    pIp6Cxt = gIp6GblInfo.apIp6Cxt[u4CurrContext];

    if (pIf6 == NULL)
    {
        IP6_TRC_ARG1 (u4PrevContext, IP6_MOD_TRC,
                      CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                      IP6_NAME, "Ip6IfaceMapping:Invalid IF index %d\n",
                      u4IfIndex);
        return;
    }

    if (pIp6Cxt == NULL)
    {
        IP6_TRC_ARG1 (u4PrevContext, IP6_MOD_TRC,
                      CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                      IP6_NAME, "Ip6IfaceMapping:Invalid context Id %d\n",
                      u4CurrContext);
        return;
    }

    if (gIp6GblInfo.apIp6Cxt[u4PrevContext] != pIf6->pIp6Cxt)
    {
        IP6_TRC_ARG1 (u4PrevContext, IP6_MOD_TRC,
                      CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                      IP6_NAME, "Ip6IfaceMapping:Invalid previous context %d\n",
                      u4PrevContext);
        return;
    }

    Ip6UnmapIf (pIf6);

    Ip6MapIf (pIf6, pIp6Cxt);

}

/******************************************************************************
 *  * DESCRIPTION :  This function creates/deletes zones for on link interfaces
 *   *                based on the operation received from OSPF.
 *    * INPUTS      : *pBuf - Pointer to the CRU BUF
 *     * OUTPUTS     : None
 *      * RETURNS     : None
 *       *****************************************************************************/
VOID
Ip6HandleSelfIfOnLinkEvent (tCRU_BUF_CHAIN_HEADER * pBuf)
{
    tIp6MultIfToIp6Link IfOverLinkInfo;
    tIp6If             *pIf6 = NULL;
    tIp6If             *pIf6Active = NULL;
    UINT2               u2OnLinkIfCount = 0;

    MEMSET (&IfOverLinkInfo, IP6_ZERO, sizeof (tIp6MultIfToIp6Link));

    CRU_BUF_Copy_FromBufChain (pBuf,
                               (UINT1 *) &(IfOverLinkInfo),
                               0, sizeof (tIp6MultIfToIp6Link));

    CRU_BUF_Release_MsgBufChain (pBuf, FALSE);

    pIf6Active = IP6_INTERFACE (IfOverLinkInfo.u4OnLinkActiveIfId);

    if (pIf6Active == NULL)
        return;

    if (IfOverLinkInfo.u2LinkOperation == IPV6_STANDBY_MODIFY)
    {
        pIf6 = IP6_INTERFACE (IfOverLinkInfo.apIp6If[0]);
        pIf6->u1IfStatusOnLink = IPV6_IF_ACTIVE;
        pIf6->u4OnLinkActiveIfId = pIf6->u4Index;
        pIf6->u2OnlinkIfCount = 0;
    }
    else
    {
        for (u2OnLinkIfCount = 0;
             u2OnLinkIfCount < IfOverLinkInfo.u2OnLinkIfCount;
             u2OnLinkIfCount++)
        {
            pIf6 = IP6_INTERFACE (IfOverLinkInfo.apIp6If[u2OnLinkIfCount]);

            if (pIf6 == NULL)
                return;

            if (IfOverLinkInfo.u2LinkOperation == IPV6_ACTIVE_ADD)
            {
                if (IfOverLinkInfo.apIp6If[u2OnLinkIfCount] ==
                    IfOverLinkInfo.u4OnLinkActiveIfId)
                {
                    pIf6->u1IfStatusOnLink = IPV6_IF_ACTIVE;
                }
                else
                {
                    pIf6->u1IfStatusOnLink = IPV6_IF_STANDBY;
                }
                pIf6->u4OnLinkActiveIfId = IfOverLinkInfo.u4OnLinkActiveIfId;
                pIf6Active->au1OnLinkIfaces[u2OnLinkIfCount] =
                    (UINT1) IfOverLinkInfo.apIp6If[u2OnLinkIfCount];
                pIf6->u2OnlinkIfCount = IfOverLinkInfo.u2OnLinkIfCount;

            }
            else if (IfOverLinkInfo.u2LinkOperation == IPV6_ACTIVE_DELETE)
            {
                pIf6->u1IfStatusOnLink = IPV6_IF_ACTIVE;
                pIf6->u4OnLinkActiveIfId = pIf6->u4Index;
                pIf6->u2OnlinkIfCount = 0;
            }

        }
    }
    Ip6ZoneOnLinkNotification (&IfOverLinkInfo);
    return;
}

/***************************** END OF FILE **********************************/
