/*******************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ip6sys.h,v 1.46 2017/12/26 13:34:22 siva Exp $
 *
 * Description: Cli related functions are handled here.
 *
 *******************************************************************/
#ifndef _IP6SYS_H
#define _IP6SYS_H

/*
 *----------------------------------------------------------------------------- 
 *    FILE NAME                    :    ip6sys.h
 *
 *    PRINCIPAL AUTHOR             :    Vivek Venkatraman/Senthil Vadivu
 *
 *    SUBSYSTEM NAME               :    IPv6
 *
 *    MODULE NAME                  :    All Modules
 *
 *    LANGUAGE                     :    C
 *
 *    TARGET ENVIRONMENT           :    UNIX
 *
 *    DATE OF FIRST RELEASE        :    DDD-MM-1996
 *
 *    DESCRIPTION                  :    This file contains constants, macros,
 *                                      prototypes and typedefs which are
 *                                      common to the whole IPv6 subsystem.
 *                                      These are likely to change during
 *                                      porting to a target platform.
 *
 *----------------------------------------------------------------------------- 
 */

/*
 * General constants
 */

#ifndef TMO_NOT_OK
#define  TMO_NOT_OK  (-1) 
#endif

#ifndef TMO_ERROR
#define  TMO_ERROR         (-1)       
#endif

#define  IP6_MAX_SYS_TIME  0xFFFFFFFF 
#define  MAX_IP6_PACKET_PROCESS 40

#define  ENTRY_UNUSED      0          
#define  ENTRY_USED        1          
#define  IP6_VRID          0

/*
 * Task-related constants: Task name, priority, queues, events
 */

#define  IP6_TASK_NAME    "IP6"  
#define  PING6_TASK_NAME  "PNG6" 

#define  IP6_TASK_PRIORITY    30 
#define  RIP6_TASK_PRIORITY   40 
#define  PING6_TASK_PRIORITY  45 

#define  IP6_TASK_CONTROL_QUEUE     "IPQ0" 
#define  IP6_TASK_IP_INPUT_QUEUE    "IPQ1" 
#define  IP6_TASK_PING_INPUT_QUEUE  "IPQ3" 

#define  PING6_TASK_PING_INPUT_QUEUE  "PGQ0" 
#define IP6_TASK_ONLINK_NOTIFY_QUEUE  "IPQ4"
#define IP6_RM_PKT_QUEUE              "IPQ5"
#define  IP6_TASK_NS_NA_QUEUE         "IPQ6"
#define IP6_BFD_STATE_CHANGE_QUEUE    "IPQ7"

#define  IP6_CONTROL_EVENT         (0x00000001) 
#define  IP6_DGRAM_RECD_EVENT      (0x00000002) 
#define  IP6_MESG_RECD_EVENT       (0x00000004) 
#define  IP6_PING_MESG_RECD_EVENT  (0x00000008) 
#define  IP6_ON_LINK_MSG_RECD_EVENT (0x00000010)
#define  IP6_NS_NA_RECD_EVENT      (0x00002000)
#define  IP6_BFD_STATE_CHANGE_EVENT (0x00004000)

/* RED Support */
#define   IP6_RM_PKT_EVENT          (0x00000020)
#define   IP6_HA_PEND_BLKUPD_EVENT  (0x00000040)
#define   IP6_RED_TIMER_EVENT       (0x00000080)

#define  IP6_TIMER0_EVENT            (0x00008000) 
#define  IP6_MC_REG_EVENT            (0x00010000)

#define  PING6_PING_MESG_RECD_EVENT  (0x00000100) 
#define  PING6_TIMER0_EVENT          (0x00000200) 
#define  IP6_SHUT_DOWN_EVENT         (0x00000400)
#define  IP6_RED_START_TIMER_EVENT   (0x00000800) 
#define  IP6_ND_RESOLVE_EVENT      (0x00001000)


#define  NETIPV6_REG_ENABLE           0x1f
#define  NETIPV6_REG_DISABLE          0

/*
 * Module, Submodule constants
 */

#define  IP6_MODULE          0x60 
#define  ICMP6_MODULE        0x61 
#define  UDP6_MODULE         0x62 
#define  ND6_MODULE          0x63 
#define  RIP6_MODULE         0x64 
#define  PING6_MODULE        0x65 
#define  IP6_LIB_MODULE      0x66 
#define  IPSEC6_MODULE       0x67 

#define  IP6_MAIN_SUBMODULE  0x70 
#define  IP6_IF_SUBMODULE    0x71 
#define  IP6_ADDR_SUBMODULE  0x72 
#define  IP6_CORE_SUBMODULE  0x73 
#define  IP6_FWD_SUBMODULE   0x74 
#define  IP6_FRAG_SUBMODULE  0x75 



#define IP6_LAYER2_DATA  01
#define IP6_ICMPERR_DATA 02
#define IP6_DEREGISTER 0
#define IP6_REGISTER 1

#define IP6_Q_DEPTH_DEF         1000 
#define IP6_MC6Q_DEPTH_DEF      50
/* Maximum IP6 instances supported by router */
#define IP6_MAX_INSTANCES       (FsIP6SizingParams[MAX_IP6_CONTEXT_SIZING_ID].u4PreAllocatedUnits)

/* Ip6 VCM related Events */

#define IP6_CREATE_CONTEXT     0x06
#define IP6_DELETE_CONTEXT     0x07
#define IP6_MAP_INTERFACE      0x08
#define IP6_UNMAP_INTERFACE    0x09

/*
 * This structre stores the various parameters registered by applications
 * while registering their protocol identifiers with IP6.
 */

typedef struct _Ip6RegTblEntry
{
    VOID (*pAppRcv) (tCRU_BUF_CHAIN_HEADER *pBuf,
                     UINT4 u4Len, UINT4 u4Index,
                     UINT1 u1Type);
    VOID (*pIfStChg) (UINT4 u4Index , UINT1 u1Prefixlen, 
                               INT1 i1RtFlag, tIp6Addr *pAddr);
    VOID (*pRtChg)   (tNetIpv6RtInfo *pIp6RtInfo, INT4 i4RtFlag);
    VOID (*pIcmp6ErrSend) (tCRU_BUF_CHAIN_HEADER *pBuf,
                           tIp6Addr Addr,
                           UINT1 u1Type, UINT1 u1Code,
                           UINT4 u4Len);
    VOID (*pPmtuNotify)   (tIp6Addr Addr, UINT4 u4Pmtu);
    UINT1     u1RegFlag;
    UINT1     u1AlignmentByte;    /* 3 bytes to ensure a 4-byte boundary */
    UINT2     u2AlignmentByte;
} tIp6RegTblEntry;

#define MN_REVERSE_TUNNEL  1
#define MN_DIRECT_DELIVERY 2
/*
 * System sizing constants
 */


#ifdef TUNNEL_WANTED

#define  IP6_MAX_LOGICAL_IF_INDEX  (SYS_DEF_MAX_PHYSICAL_INTERFACES \
                                    + LA_MAX_AGG_INTF + \
                                    IP_DEV_MAX_L3VLAN_INTF + \
                                    SYS_DEF_MAX_TUNL_IFACES + \
                                    SYS_DEF_MAX_MPLS_IFACES + \
                                    SYS_DEF_MAX_MPLS_TNL_IFACES + \
                                    SYS_MAX_LOOPBACK_INTERFACES +\
                                    SYS_MAX_PPP_INTERFACES + \
                                    SYS_DEF_MAX_ILAN_IFACES + \
                                    SYS_DEF_MAX_INTERNAL_IFACES + \
                                    SYS_DEF_MAX_NUM_OF_VPNC + \
                                    SYS_DEF_MAX_TELINK_INTERFACES + \
                                    SYS_DEF_MAX_L2_PSW_IFACES + \
                                    SYS_DEF_MAX_L2_AC_IFACES +\
                                    SYS_DEF_MAX_NVE_IFACES +\
                                    CFA_MAX_NUM_OF_HDLC +\
                                    SYS_DEF_MAX_SBP_IFACES +\
                                    MAX_TAP_INTERFACES +\
                                    SYS_DEF_MAX_L3SUB_IFACES)

#define  IP6_IF_NEXT_VISIBLE_IF(u4StartIfIndex)\
                               (u4StartIfIndex == CFA_MAX_TNL_IF_INDEX) ?\
                               (CFA_MIN_LOOPBACK_IF_INDEX): \
                               ((u4StartIfIndex == CFA_MAX_LOOPBACK_IF_INDEX) ?\
                                (CFA_MIN_PSW_IF_INDEX):(u4StartIfIndex + 1))
#else
#define  IP6_MAX_LOGICAL_IF_INDEX  (SYS_DEF_MAX_PHYSICAL_INTERFACES \
                                    + LA_MAX_AGG_INTF + \
                                    IP_DEV_MAX_L3VLAN_INTF + \
                                    SYS_DEF_MAX_MPLS_IFACES + \
                                    SYS_DEF_MAX_MPLS_TNL_IFACES + \
                                    SYS_MAX_LOOPBACK_INTERFACES +\
                                    SYS_MAX_PPP_INTERFACES + \
                                    SYS_DEF_MAX_ILAN_IFACES + \
                                    SYS_DEF_MAX_INTERNAL_IFACES + \
                                    SYS_DEF_MAX_NUM_OF_VPNC + \
                                    SYS_DEF_MAX_TELINK_INTERFACES + \
                                    SYS_DEF_MAX_L2_PSW_IFACES +\
                                    SYS_DEF_MAX_L2_AC_IFACES +\
                                    SYS_DEF_MAX_NVE_IFACES +\
                                    CFA_MAX_NUM_OF_HDLC +\
                                    SYS_DEF_MAX_SBP_IFACES +\
                                    MAX_TAP_INTERFACES +\
                                    SYS_DEF_MAX_L3SUB_IFACES)

#define  IP6_IF_NEXT_VISIBLE_IF(u4StartIfIndex)\
                               (u4StartIfIndex == CFA_MAX_IVR_IF_INDEX) ?\
                               (CFA_MIN_LOOPBACK_IF_INDEX): \
                               ((u4StartIfIndex == CFA_MAX_LOOPBACK_IF_INDEX) ?\
                                (CFA_MIN_PSW_IF_INDEX):(u4StartIfIndex + 1))
#endif



#define  IP6_MAX_ENET_INTERFACES    3                         
#define  IP6_MAX_LAN_INTERFACES     CFA_MAX_INTERFACES_IN_SYS 
#define  IP6_MAX_WAN_INTERFACES     2
#define  IP6_MAX_PHYS_INTERFACES    (IP6_MAX_LAN_INTERFACES   + \
                                    IP6_MAX_WAN_INTERFACES)
#ifdef TUNNEL_WANTED

#define  IP6_MAX_TUNNEL_INTERFACES  IP6_DEF_MAX_TUNNEL_INTERFACES

#define  IP6_DEF_TOS                0 
#define  IP6_DEF_TTL                0 
#endif

#ifdef HA_WANTED
#define  IP6_MAX_ADDRESSES           250
#else
#define  IP6_MAX_ADDRESSES           IP6_DEF_MAX_ADDRESSES
#endif

#define IP6_MAX_ROUTE_ENTRIES        IP6_DEF_MAX_ROUTE_ENTRIES
#define IP6_ND6_MAX_CACHE_ENTRIES    IP6_DEF_ND6_MAX_CACHE_ENTRIES
#define IP6_DEF_ND6_MAX_CACHE_ENTRIES (IP_DEV_MAX_IP_INTF * 4)

#define  IP6_MIN_ROUTE_ENTRIES       5

#define  IP6_MAX_UNICAST_ADDRESS     (IP6_MAX_LOGICAL_IFACES * 2)                     
#define  IP6_MAX_PREFIX_ADDRESS      (IP6_MAX_LOGICAL_IFACES * 3)

#define  IP6_MAX_MCAST_ADDRESS       (IP6_MAX_LOGICAL_IFACES * 10)
#define  IP6_MAX_LINK_LOCAL_ADDRESS  (IP6_MAX_LOGICAL_IFACES * 3)

#define  IP6_MAX_MAC_FILTER_NODE    \
              (IP6_MAX_UNICAST_ADDRESS + IP6_MAX_MCAST_ADDRESS + IP6_MAX_LINK_LOCAL_ADDRESS)
#define  IP6_MAX_ADDR_PROFILES       IP6_MAX_ADDRESSES         
#define  IP6_MIN_ADDR_PROFILES       0

#define  IP6_MAX_FRAG_QUEUES         IP6_MAX_FRAG_REASM_ENTRY    /*  30  */                      
#define  ND6_MAX_CACHE_LAN_ENTRIES   IP6_ND6_MAX_CACHE_ENTRIES     

#define IP6_PURGE_ALL_ROUTES         10

#define  UDP6_MAX_PORT               10                         

#define  PING6_MAX_DST               5                         

#define  MAX_PING6_DATA_SIZE         2080       

/* HASH constants */

#define  IP6IF_HASH_TBL_SIZE         25     /* LOGICAL_IFACES */

/*
 * The following Hash Sizes are based on the hash function return values
 */
#define  IP6_ADDR_HASH_SIZE   16     /* IPv6 ADDRESSES */
#define  ND6_CACHE_HASH_SIZE  16     /* ND CACHE */

#define IP6_MAX_DEF_ROUTERS          5 /* Maximum default routers can a
                                        * host maintain in its list */
#define IP6_ADDR_NEXT_SRCH_LLIST       1
#define IP6_ADDR_NEXT_SRCH_ALIST       2
#define IP6_ADDR_NEXT_SRCH_BOTH        3
#define IP6_ADDR_NEXT_SRCH_MLIST       4

/*
 *  TIMER constants
 */


#define IP6_ROUTE_TAG_INTERNAL       1
#define IP6_ROUTE_TAG_EXTERNAL       2



#define  IP6_LLOCAL_RAND_TIMER_ID    1       
#define  IP6_UNI_RAND_TIMER_ID       2       
#define  IP6_LLOCAL_DAD_TIMER_ID     3       
#define  IP6_UNI_DAD_TIMER_ID        4       
#define  ND6_ROUT_ADV_SOL_TIMER_ID   5
#define  ND6_RETRANS_TIMER_ID        6       
#define  ND6_REACH_TIMER_ID          7       
#define  ND6_DELAY_PROBE_TIMER_ID    8       

#define  FRAG_REASM_TIMER_ID         9       

#define  SEC_LIFETIME_TIMER_ID       10      

#define  IP6_PMTU_TIMER_ID           11      

#define IP6_VALID_LIFETIME_TIMER_ID  12 
#define IP6_PREF_LIFETIME_TIMER_ID   13 

#define IP6_BASE_REACH_TIMER_ID      14
#define IP6_DEF_RTR_LIFE_TIMER_ID    15
#define IP6_PREFIX_VALID_TIMER_ID    16
#define IP6_PREFIX_PREF_TIMER_ID     17
#define ND6_PROXY_LOOP_TIMER_ID      18       
#define IP6_PROXY_ND_TIMER_ID        33 
#define IP6_ICMP6_TIMER_ID           40

#define  RIP6_AGE_OUT_TIMER_ID       1       
#define  RIP6_GARB_COLLECT_TIMER_ID  2       
#define  RIP6_UPDATE_TIMER_ID        3       
#define  RIP6_TRIG_TIMER_ID          4      


#define  PING6_DEST_TIMER_ID         1       

#define  IP6_MIN_DBG_VAL             0       
#define  IP6_SELF_NODE             0       


#define IP6_TUNL_ENABLE       1
#define IP6_TUNL_DISABLE      2

#define IP6_FIXED_TIME        1
#define IP6_VARIABLE_TIME     2

#define ND6_PROXY_LOOP_TMR_INTERVAL (60*60)

/*
 * Error Handling related constants
 */

#define  ERROR_FATAL          1       
#define  ERROR_MINOR          2       

#define  ERROR_FATAL_STR      "FATAL" 
#define  ERROR_MINOR_STR      "MINOR" 

#define  ERR_BUF_ALLOC        0       
#define  ERR_BUF_RELEASE      1       
#define  ERR_BUF_READ         2       
#define  ERR_BUF_WRITE        3       
#define  ERR_BUF_PPTR         4       
#define  ERR_BUF_WPTR         5       
#define  ERR_BUF_WOFFSET      6       

#define  ERR_MEM_CREATE       0       
#define  ERR_MEM_GET          1       
#define  ERR_MEM_RELEASE      2       
#define  ERR_MEM_DELETE       3       
#define  ERR_MEM_SET_PARAM    4       
#define  ERR_MEM_GET_PARAM    5       

#define  ERR_TIMER_INIT       0       
#define  ERR_TIMER_START      1       
#define  ERR_TIMER_STOP       2       
#define  ERR_TIMER_RESTART    3       
#define  ERR_TIMER_TIMEOUT    4       

#define  ERR_GEN_NULL_PTR     0       
#define  ERR_GEN_INVALID_VAL  1       
#define  ERR_GEN_TQUEUE_FAIL  2       

#define IP6_MAX_VALUE            0xFFFFFFFF 


/*
 * Structure for exchanging information between Lower Layer protocols
 * and IP6 tasks
 */

typedef struct _LL_IP6_PARAMETERS
{
    UINT1 u1Cmd;
    UINT1 u1LinkType;
    UINT1 u1Dos;      /* Flag for Denial of Service */
    UINT1 u1Reserved; /* For Alignment */
    UINT4 u4Port;
}
tLlToIp6Params;

/*
 * Structure for exchanging RIP message parameters
 * between IP6 and RIP6 tasks
 */

typedef struct _RIP6_UDP6_PARAMETERS
{

    UINT1       u1Hlim;          /* Hop Limit of the packet */
    UINT1       au1Reserved[3];  /* Padding */ 
    UINT4       u4Index;         /* Index of the interface over which
                                  * it is to be sent */
    UINT2       u2SrcPort;       /* Source port number over which packet
                                  * came 
                                  */
    UINT2       u2DstPort;       /* Destination port number over which
                                  * packet is to be sent */
    UINT4       u4Len;           /* Length of application data */
    tIp6Addr  srcAddr;           /* Source address in the packet */
    tIp6Addr  dstAddr;           /* Destination address in the packet */

}
tRip6Udp6Params;

/* Parameter structure for exchanging information between vcm and IP6 */
typedef struct
{
    UINT4  u4ContextId;
    UINT4  u4IfIndex;
}
tIp6VcmParams;


/*
 * The following structure is the union of the above parmeter structures
 * used in exchanging information between tasks
 */

typedef struct _IP6_PARAMETERS
{
    union
    {
        tIcmp6Params  ping6Icmp6Params;
        tRip6Udp6Params    rip6Udp6Params;
        tHlToIp6Params     hlToIp6Params;
        tHlToIp6McastParams  hlToIp6McastParams;
        tLlToIp6Params     llToIp6Params;
        tIp6VcmParams      Ip6VcmParams;
    }
    ip6_parm_u;

}
tIp6Params;

/*
 * Parameter structure for exchanging information between
 * TEST and IP6 tasks
 */

typedef struct
{
    UINT1  u1MsgType;
#define  CFA_IF_TOKEN_AVAIL  3 
    /* Type can also be CFA_IF_UP & CFA_IF_DOWN which is 
     * declared in CFA */
    UINT1  u1IfType;
    UINT1  u1PrefixLenth;/*prefix lenth for default ipv6 address configured*/
    UINT1  u1pad;
    UINT2  u2CktIndex;
    UINT2  u2padd;
    UINT4  u4IfIndex;
    UINT4  u4Status;
    UINT4  u4Speed;
    UINT4  u4HighSpeed;
    tIp6Addr Ip6Addr;  /*default ipv6 address at startup*/
}
tIp6CfaParams;

/* Structure added for creating mempools for linear and unfragmented buffer */
typedef struct 
{
    UINT1  u1UnfragBuf[IP6_MAX_BUF_SIZE];
}    
tIp6SzBuf;

/*
 * MACROs
 */

#define  IP6_MEM_MALLOC(Size,Type)  MEM_MALLOC(Size,Type)                            
#define  IP6_MEM_FREE(Ptr)          MEM_FREE(Ptr)                      

#define  HTONL(u4_val)          OSIX_HTONL(u4_val)                   
#define  HTONS(u2_val)          OSIX_HTONS(u2_val)                   
#define  NTOHL(u4_val)          OSIX_NTOHL(u4_val)                   
#define  NTOHS(u2_val)          OSIX_NTOHS(u2_val)                   

#define  IP6_OFFSET(x,y)        FSAP_OFFSETOF(x,y)

/* Definition Related with Semaphore locks. */
#define  IP6_TASK_SEM_NAME              "IP6L"
#define  IP6_TASK_GLOBAL_SEM_NAME       "IP6G"
#define  IP6_SEM_CREATE_INIT_CNT        1

#define  IP6_BUF_DATA_LEN(buf)  CRU_BUF_Get_ChainValidByteCount(buf) 

#define  IP6_BUF_DATA_PTR(pbuf)     ((pbuf)->pFirstValidDataDesc->pu1_FirstValidByte) 
#define  IP6_BUF_READ_OFFSET(buf)   CB_READ_OFFSET(buf)                               
#define  IP6_BUF_WRITE_OFFSET(buf)  CB_WRITE_OFFSET(buf)                              

#define Ip6ifFormEthHdr(x,y) Ip6SetParams ((tIp6Params*)(VOID *)x,y)

#define IP6_BUF_GET_1_BYTE(buf, offset, value) \
        GET_1_BYTE(buf, offset, value)
#define IP6_BUF_GET_2_BYTE(buf, offset, value) \
        GET_2_BYTE(buf, offset, value)
#define IP6_BUF_GET_4_BYTE(buf, offset, value) \
        GET_4_BYTE(buf, offset, value)

#define IP6_BUF_SET_1_BYTE(buf, offset, value) \
        ASSIGN_1_BYTE(buf, offset, value)
#define IP6_BUF_SET_2_BYTE(buf, offset, value) \
        ASSIGN_2_BYTE(buf, offset, value)
#define IP6_BUF_SET_4_BYTE(buf, offset, value) \
        ASSIGN_4_BYTE(buf, offset, value)


/* Comparison constants */

#define  IP6_RB_GREATER               1
#define  IP6_RB_LESSER               -1
#define  IP6_RB_EQUAL                 0

#define IP6_FSIP6_TBL_OID             1
#define IP6_ROUTE_TBL_OID             6

/*This is solicited multicast address with interface identifier as zero
 * used fot subnet-router anycast */
#define IS_SUBNET_ADDR_SOLICITED(a)   (((a).u4_addr[0] == CRU_HTONL(0xff020000)) && \
                                      ((a).u4_addr[1] == 0)          && \
                                      ((a).u4_addr[2] == CRU_HTONL(1)) && \
                                      ((a).u1_addr[12] == 0xff))

/*
 * Prototypes of library functions
 */

INT4                CRUBUFCopyBufChains (tCRU_BUF_CHAIN_HEADER *
                                            pDstChainDesc,
                                            tCRU_BUF_CHAIN_HEADER *
                                            pSrcChainDesc, UINT4 u4DstOffset,
                                            UINT4 u4SrcOffset, UINT4 u4Size);

tCRU_BUF_CHAIN_HEADER *CRUBUFDeleteBufChainAtStart (tCRU_BUF_CHAIN_HEADER *
                                                       pChainDesc,
                                                       UINT4 u4Size);

tCRU_BUF_CHAIN_HEADER *Ip6BufAlloc PROTO ((UINT4 u4ContextId, UINT4 u4Size,
                                                UINT4 u4Offset,
                                                UINT1 u1Module));

INT4 Ip6BufRelease PROTO ((UINT4 u4ContextId, tCRU_BUF_CHAIN_HEADER * pBuf,
                                UINT1 u1Rflag, UINT1 u1Module));

VOID               *Ip6BufRead PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf,
                                         UINT1 *pData,
                                         UINT4 u4Offset,
                                         UINT4 u4Size, UINT1 u1Copy));

UINT4 Ip6BufWoffset PROTO ((UINT1 u1Module));

INT4 Ip6BufWrite  PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf,
                            UINT1 *pData,
                            UINT4 u4Offset, UINT4 u4Size, UINT1 u1Copy));

VOID               *Ip6BufPptr PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf,
                                         UINT1 *pData,
                                         UINT4 u4Size, UINT1 *pu1Copy));

UINT1 *Ip6GetMem PROTO ((UINT4 u4ContextId, INT4 i4Id));

INT1 Ip6RelMem PROTO ((UINT4 u4ContextId, UINT2 u2Id, UINT1 *pBuf)); 

INT1 Ip6DeleteMemPool PROTO ((UINT4 u4Id)); 
                                            

VOID Ip6SetParams PROTO ((tIp6Params * pParams,
                            tCRU_BUF_CHAIN_HEADER * pBuf));

FS_ULONG              *Ip6GetParams PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf));

#define IP6_GET_MODULE_DATA_PTR(pBuf) \
&(pBuf->ModuleData)
#define IP6_SET_COMMAND(pBuf,u1Cmd) CRU_BUF_Set_SourceModuleId(pBuf,u1Cmd)
#define IP6_GET_COMMAND(pBuf) CRU_BUF_Get_SourceModuleId(pBuf)

VOID Ip6TmrStart PROTO ((UINT4 u4ContextId, UINT1 u1TimerId,
                               tTimerListId timerListId,
                               tTmrAppTimer * pAppTimer, UINT4 u4Duration));
VOID Ip6MSecTmrStart PROTO ((UINT4 u4ContextId, UINT1 u1TimerId,
                                  tTimerListId timerListId,
                                  tTmrAppTimer * pAppTimer, UINT4 u4Duration));

VOID Ip6TmrStop PROTO ((UINT4 u4ContextId, UINT1 u1TimerId,
                              tTimerListId timerListId,
                              tTmrAppTimer * pAppTimer));

VOID Ip6TmrRestart PROTO ((UINT4 u4ContextId, UINT1 u1TimerId,
                                 tTimerListId timerListId,
                                 tTmrAppTimer * pAppTimer, UINT4 u4Duration));
VOID Nd6TmrRestart PROTO ((UINT4 u4ContextId, UINT1 u1TimerId,
                                tTimerListId timerListId,
                                tTmrAppTimer * pAppTimer, UINT4 u4Duration));
UINT2 Ip6Checksum  PROTO ((tIp6Addr * pSrc,
                            tIp6Addr * pDst,
                            UINT4 u4Len,
                            UINT1 u1Proto, tCRU_BUF_CHAIN_HEADER * pBuf));

VOID Ip6FillCksum PROTO ((UINT2 u2Cksum, UINT1 u1Proto,
                            tCRU_BUF_CHAIN_HEADER * pBuf));

#endif /* !_IP6SYS_H */
