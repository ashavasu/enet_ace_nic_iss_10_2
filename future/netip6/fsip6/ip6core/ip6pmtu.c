/*  $Id: ip6pmtu.c,v 1.9 2014/01/31 13:11:45 siva Exp $ */
/*
 *----------------------------------------------------------------------------- 
 *    FILE NAME                    :    ip6pmtu.c
 *
 *    PRINCIPAL AUTHOR             :    Vivek Venkatraman
 *
 *    SUBSYSTEM NAME               :    IPv6
 *
 *    MODULE NAME                  :    IP6 Core Submodule
 *
 *    LANGUAGE                     :    C
 *
 *    TARGET ENVIRONMENT           :    UNIX
 *
 *    DATE OF FIRST RELEASE        :    DDD-MM-1996
 *
 *    DESCRIPTION                  :    This file contains routines for
 *                                      IPv6 Pmtu discovery
 *
 *----------------------------------------------------------------------------- 
 */
#include "ip6inc.h"

INT4                (*gIp6PmtuHandler) (UINT4, tIp6Addr, UINT4) = NULL;

PRIVATE INT4        PmtuEntryCmp (tRBElem *, tRBElem *);

/******************************************************************************
 * DESCRIPTION : Initializes Pmtu related global variables.
 *
 * INPUTS      : None. 
 *
 * OUTPUTS     : None
 *
 * RETURNS     : IP6_SUCCESS if initialization is success.
 *               IP6_FAILURE on initialization failure.
 *
 * NOTES       :
 ******************************************************************************/
INT4
ip6PmtuInit (VOID)
{

    gIp6GblInfo.u4MaxPmtuEntries = MAX_IP6_PMTU_ENTRIES;
    /* enable PMTU by default */
    gIp6GblInfo.u2PmtuGrbgInterval = IP6_PMTU_DEF_TIME_OUT_INTERVAL;

    gIp6GblInfo.PmtuRBTable =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tPmtuEntry, RBNode), PmtuEntryCmp);
    gIp6GblInfo.PmtuTimer.u1Id = IP6_PMTU_TIMER_ID;
    Ip6TmrStart (VCM_INVALID_VC, IP6_PMTU_TIMER_ID,
                 gIp6GblInfo.Ip6TimerListId,
                 &(gIp6GblInfo.PmtuTimer.appTimer),
                 gIp6GblInfo.u2PmtuGrbgInterval);
    return IP6_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : This function is called when ever the router is going down.
 *               Releases all the memory allocated for the pmtu entries.
 *
 * INPUTS      : None
 *
 * OUTPUTS     : None
 *
 * RETURNS     : IP6_SUCCESS if initialization is success.
 *               IP6_FAILURE on initialization failure.
 *
 * NOTES       :
 ******************************************************************************/
INT4
ip6PmtuShut (VOID)
{
    if (gIp6GblInfo.PmtuRBTable != NULL)
    {
        RBTreeDestroy (gIp6GblInfo.PmtuRBTable, NULL, 0);
    }

    Ip6TmrStop (VCM_INVALID_VC, IP6_PMTU_TIMER_ID, gIp6GblInfo.Ip6TimerListId,
                &(gIp6GblInfo.PmtuTimer.appTimer));
    return IP6_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : This function is called when ever a omtu entry need to be 
                 deleted. Releases the memory allocated for the entry.
 *
 * INPUTS      : pPmtuEntry : Entry to be deleted.
 *
 * OUTPUTS     : None
 *
 * RETURNS     : None
 *
 * NOTES       :
 ******************************************************************************/
VOID
ip6PmtuReleaseEntry (tPmtuEntry * pPmtuEntry)
{
    Ip6RelMem (VCM_INVALID_VC, (UINT2) gIp6GblInfo.MemPoolId,
               (UINT1 *) pPmtuEntry);
}

/******************************************************************************
 * DESCRIPTION : Does pmtu look up in the pmtu table for a particular 
 *               destination.
 *
 * INPUTS      : pIp6Addr     : Destination IPv6 address 
 *               
 * OUTPUTS     : pSearchEntry : Entry will be returned in this pointer.
 *
 * RETURNS     : IP6_SUCCESS or IP6_FAILURE.
 *
 * NOTES       :
 ******************************************************************************/
INT4
ip6PmtuLookupEntryInCxt (UINT4 u4ContextId, tIp6Addr * pIp6Addr,
                         tPmtuEntry ** pSearchEntry)
{
    tPmtuEntry          PmtuEntry;
    tIp6Cxt            *pIp6Cxt = NULL;
    pIp6Cxt = MemAllocMemBlk (gIp6GblInfo.i4Ip6CxtPoolId);

    if (NULL == pIp6Cxt)
    {
        return IP6_FAILURE;
    }

    PmtuEntry.pIp6Cxt = pIp6Cxt;
    PmtuEntry.pIp6Cxt->u4ContextId = u4ContextId;

    MEMCPY (&PmtuEntry.Ip6Addr, pIp6Addr, sizeof (tIp6Addr));

    /* Get PMTU entry from PMTU RBtree table */
    *pSearchEntry =
        (tPmtuEntry *) RBTreeGet (gIp6GblInfo.PmtuRBTable, &PmtuEntry);
    if (*pSearchEntry == NULL)
    {
        MemReleaseMemBlock (gIp6GblInfo.i4Ip6CxtPoolId, (UINT1 *) pIp6Cxt);
        return IP6_FAILURE;
    }

    MemReleaseMemBlock (gIp6GblInfo.i4Ip6CxtPoolId, (UINT1 *) pIp6Cxt);
    return IP6_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : Handles timer expiry of pmtu entries.
 *
 * INPUTS      : pTimer : pointer to the timer expired.
 *
 * OUTPUTS     : None
 *
 * RETURNS     : None
 *
 * NOTES       :
 ******************************************************************************/
VOID
ip6PmtuTimerExpiryHandler (tIp6Timer * pTimer)
{
    tOsixSysTime        CurTime = 0;
    tPmtuEntry         *pScanEntry = NULL;
    tPmtuEntry         *pNextEntry = NULL;

    CurTime = 0;

    switch (pTimer->u1Id)
    {

        case IP6_PMTU_TIMER_ID:

            OsixGetSysTime (&CurTime);
            pScanEntry =
                (tPmtuEntry *) RBTreeGetFirst (gIp6GblInfo.PmtuRBTable);

            while (pScanEntry != NULL)
            {
                pNextEntry =
                    (tPmtuEntry *) RBTreeGetNext (gIp6GblInfo.PmtuRBTable,
                                                  (tRBElem *) pScanEntry, NULL);
                /* No need to delete static pmtu entries */
                if (pScanEntry->PmtuTimeStamp != IP6_MAX_SYS_TIME)
                {
                    if ((CurTime -
                         ((tPmtuEntry *) pScanEntry)->PmtuTimeStamp) >
                        (((tPmtuEntry *) pScanEntry)->pIp6Cxt->u4CfgTimeOut *
                         10 * SYS_NUM_OF_TIME_UNITS_IN_A_SEC))
                    {
                        RBTreeRem (gIp6GblInfo.PmtuRBTable, pScanEntry);
                        ip6PmtuReleaseEntry ((tPmtuEntry *) pScanEntry);
                    }
                }
                pScanEntry = pNextEntry;
            }
        default:
        {
            break;
        }
    }                            /* end of switch */

    gIp6GblInfo.PmtuTimer.u1Id = IP6_PMTU_TIMER_ID;

    Ip6TmrStart (VCM_INVALID_VC, gIp6GblInfo.PmtuTimer.u1Id,
                 gIp6GblInfo.Ip6TimerListId, &(gIp6GblInfo.PmtuTimer.appTimer),
                 gIp6GblInfo.u2PmtuGrbgInterval);
}

/****************************************************************************
 * DESCRIPTION : This routine is called by the IP layer's PathMTU module,
 *                  whenever PMTU value changes or if any new entry is created
 *                 in PMTU table, this routine is called.
 * INPUTS      :
 *
 * OUTPUTS     : None
 *
 * RETURNS     : None
 *
 * NOTES       :
 *****************************************************************************/

INT4
Ip6PmtuNotifyInCxt (UINT4 u4ContextId, tIp6Addr Ip6Addr, UINT4 u4Pmtu)
{
    if (gIp6PmtuHandler != NULL)
    {
        gIp6PmtuHandler (u4ContextId, Ip6Addr, u4Pmtu);
        return IP6_SUCCESS;
    }
    return IP6_FAILURE;
}

/****************************************************************************
 * DESCRIPTION : This routine is used by SLI to register the Path MTU handler
 *               with IP6
 * INPUTS      : A function Pointer with Destination Ip6 Address and
 *               new PathMTU value as its input parameters
 *
 * OUTPUTS     : Updates the Global variable gIp6PmtuHandler, if not null
 *
 * RETURNS     : IP6_SUCCESS/IP6_FAILURE
 *
 * NOTES       : The registered function is called whenever there is a change in *               the Path MTU
 *****************************************************************************/
INT4
Ip6PmtuRegHl (PmtuHandler)
     INT4                (*PmtuHandler) (UINT4, tIp6Addr, UINT4);
{
    if ((gIp6PmtuHandler == NULL) && (PmtuHandler != NULL))
    {
        gIp6PmtuHandler = PmtuHandler;
        return IP6_SUCCESS;
    }
    else
        return IP6_FAILURE;
}

INT4
PmtuEntryCmp (tRBElem * e1, tRBElem * e2)
{
    tPmtuEntry         *pFirstEntry = e1;
    tPmtuEntry         *pSecondEntry = e2;
    INT4                i4RetVal = 0;

    if (pFirstEntry->pIp6Cxt->u4ContextId > pSecondEntry->pIp6Cxt->u4ContextId)
    {
        return IP6_RB_GREATER;
    }
    else if (pFirstEntry->pIp6Cxt->u4ContextId <
             pSecondEntry->pIp6Cxt->u4ContextId)
    {
        return IP6_RB_LESSER;
    }

    i4RetVal = Ip6AddrCompare (pFirstEntry->Ip6Addr, pSecondEntry->Ip6Addr);

    if (i4RetVal == IP6_ZERO)
    {
        return IP6_RB_EQUAL;
    }
    else if (i4RetVal == IP6_ONE)
    {
        return IP6_RB_GREATER;
    }
    else
    {
        return IP6_RB_LESSER;
    }
}

/******************************************************************************
 * DESCRIPTION : This routine clears all the pmtu entries added for the
 *               given context. 
 * INPUTS      : pIp6Cxt - The context structure.                  
 * OUTPUTS     : None
 * RETURNS     : None
 ******************************************************************************/
VOID
Ip6ClearPmtuEntries (tIp6Cxt * pIp6Cxt)
{
    tPmtuEntry          PmtuEntry;
    tPmtuEntry         *pPmtuEntry = NULL;
    tPmtuEntry         *pNextPmtuEntry = NULL;

    if (pIp6Cxt->u1PmtuEnable != IP6_PMTU_ENABLE)
    {
        return;
    }

    MEMSET (&PmtuEntry, IP6_ZERO, sizeof (tPmtuEntry));

    PmtuEntry.pIp6Cxt = pIp6Cxt;

    pPmtuEntry = RBTreeGetNext (gIp6GblInfo.PmtuRBTable,
                                (tRBElem *) & PmtuEntry, NULL);
    while (pPmtuEntry != NULL)
    {
        if (pPmtuEntry->pIp6Cxt->u4ContextId != pIp6Cxt->u4ContextId)
        {
            break;
        }

        pNextPmtuEntry = RBTreeGetNext (gIp6GblInfo.PmtuRBTable,
                                        pPmtuEntry, NULL);

        RBTreeRem (gIp6GblInfo.PmtuRBTable, pPmtuEntry);
        ip6PmtuReleaseEntry (pPmtuEntry);
        pPmtuEntry = pNextPmtuEntry;

    }

    return;
}
