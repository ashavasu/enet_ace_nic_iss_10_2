/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * 
 * $Id: ip6out.c,v 1.32 2017/12/19 13:41:54 siva Exp $
 *
 ********************************************************************/
/*
 *----------------------------------------------------------------------------- 
 *    FILE NAME                    :    ip6out.c
 *
 *    PRINCIPAL AUTHOR             :    Vivek Venkatraman
 *
 *    SUBSYSTEM NAME               :    IPv6
 *
 *    MODULE NAME                  :    IP6 Core Submodule
 *
 *    LANGUAGE                     :    C
 *
 *    TARGET ENVIRONMENT           :    UNIX
 *
 *    DATE OF FIRST RELEASE        :    DDD-MM-1996
 *
 *    DESCRIPTION                  :    This file contains routines for
 *                                      transmitting IPv6 datagrams. These
 *                                      are originated by modules such as
 *                                      ICMP6, UDP6 and ND6.
 *
 *----------------------------------------------------------------------------- 
 */

#include "ip6inc.h"
#include "secv6.h"

/*
 * Private functions
 */

PRIVATE VOID Ip6PutModuleHdr PROTO ((tIp6If * pIf6,
                                     tCRU_BUF_CHAIN_HEADER * pBuf));

/******************************************************************************
 * DESCRIPTION : This routine is called to send out a IPv6 datagram.
 *               The higher layer data and its length and the destination
 *               address are passed. For multicast and link-local destinations,
 *               the outgoing interface also has to be specified. For other
 *               destinations, the route find routine is called to get the
 *               route and the outgoing interface. The source address is
 *               determined if not given by higher layer. The datagram is then
 *               formed and sent out on the outgoing interface, doing
 *               fragmentation if required.
 *
 * INPUTS      : The outgoing interface (pIf6), source and destination IPv6 
 *               address (pSrc & pDst), length of the datagram (u4Len),
 *               the protocol (u1Proto - ICMP/UDP) and the buffer (pBuf)
 *
 * OUTPUTS     : None
 *
 * RETURNS     : None
 *
 * NOTES       :
 ******************************************************************************/

INT4
Ip6SendInCxt (tIp6Cxt * pIp6Cxt, tIp6If * pIf6, tIp6Addr * pSrc,
              tIp6Addr * pDst, UINT4 u4Len, UINT1 u1Proto,
              tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 u1Hlim)
{

    UINT1               u1AddrType = 0, u1RtType = 0;
    UINT1               u1Type = 0;
    tIp6If             *pNdIp6If = NULL;
    tNd6CacheEntry     *pNd6c = NULL;
    tNetIpv6RtInfo      NetIp6RtInfo;
    UINT4               u4Mtu = 0;
    tIp6Addr           *pDstAddr6 = NULL;
    tPmtuEntry         *pPmtuEntry = NULL;
    UINT4               u4Index = 0;
    UINT4               u4TmpIndex = 0;
    UINT1               u1LlocalChk = 1;
    UINT1               u1DstAddrScope = ADDR6_SCOPE_INVALID;    /*Added for RFC4007 */
#ifdef MN_WANTED
    INT1                i1RevTunlFlag = 0;
    INT4                i4RetVal = 0;
    UINT4               u4TunlIndex = 0;
#endif
    tNetIpv6RtInfo      NetIpv6Rt;
    MEMSET (&NetIpv6Rt, 0, sizeof (tNetIpv6RtInfo));

    INT4                i4RetVal = 0;
    tIp6If             *pDstIf6 = NULL;
    tIp6Addr            tempAddr6;
    MEMSET (&tempAddr6, 0, sizeof (tIp6Addr));
    UINT1               u1Found;

    IP6_TRC_ARG4 (pIp6Cxt->u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                  "IP6OUT:Ip6Send:Trying to send pkt IP6 IfPtr =%p Len =%d "
                  "Proto = 0x%X BufPtr= %p", pIf6, u4Len, u1Proto, pBuf);

    IP6_TRC_ARG2 (pIp6Cxt->u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                  "IP6OUT:Ip6Send:Src Addr = %s Dst Addr = %s\n",
                  Ip6PrintAddr (pSrc), Ip6PrintAddr (pDst));

    if (!pDst)
    {
        IP6_TRC_ARG1 (pIp6Cxt->u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC,
                      IP6_NAME,
                      "IP6OUT:Ip6Send: Dest Address is NULL, Proto = 0x%X\n",
                      u1Proto);
        IP6_TRC_ARG2 (pIp6Cxt->u4ContextId, IP6_MOD_TRC, ALL_FAILURE_TRC,
                      IP6_NAME,
                      "IP6OUT:Ip6Send: %s general error occurred, Type = %d ",
                      ERROR_FATAL_STR, ERR_GEN_NULL_PTR);
        Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE, IP6_CORE_SUBMODULE);
        return (IP6_FAILURE);
    }
    MEMSET (&NetIp6RtInfo, 0, sizeof (tNetIpv6RtInfo));

#ifdef MN_WANTED
    if ((i4RetVal = MNProcessOutPkt (&pBuf, pSrc, pDst, &u4Len, &pIf6,
                                     &u1Proto)) == MN_REVERSE_TUNNEL)
    {
        i1RevTunlFlag = 1;
    }
    else if (i4RetVal == MIP6_FAILURE)
    {
        Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE, IP6_CORE_SUBMODULE);
        return IP6_FAILURE;
    }
#endif

#ifdef MIP6_WANTED
    Mip6ProcessOutPkt (pSrc, pDst, &u1Proto, pBuf, &u4Len);
#endif

    if ((u1AddrType = (UINT1) Ip6AddrType (pDst)) != ADDR6_MULTI)
    {

        /*
         * based on type of the destination address, try to send out the
         * packet
         */
        if (IS_ADDR_LOOPBACK (*pDst) ||
            (Ip6IsPktToMeInCxt (pIp6Cxt, &u1AddrType, u1LlocalChk, pDst,
                                pIf6) == IP6_SUCCESS))
        {
            if ((pIf6 == NULL) || (pIf6->u1AdminStatus == ADMIN_INVALID))
            {
                IP6_FIRST_VALID_IF_IN_CXT (&u4Index, pIp6Cxt);
                if ((u4Index > (UINT4) IP6_MAX_LOGICAL_IF_INDEX)
                    || (u4Index <= 0))
                {
                    Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE,
                                   IP6_CORE_SUBMODULE);
                    return IP6_FAILURE;
                }
                pIf6 = gIp6GblInfo.apIp6If[u4Index];
            }
            /*
             * obtain source address if not given to us
             */
            if ((!pSrc) || (Ip6IsAddrAnycastInCxt (pIp6Cxt, pSrc)
                            == IP6_SUCCESS))
            {
                pSrc = Ip6AddrGetSrc (pIf6, pDst);
                if (!pSrc)
                {
                    IP6IF_INC_OUT_DISCARDS (pIf6);
                    IP6SYS_INC_OUT_DISCARDS (pIp6Cxt->Ip6SysStats);
                    IP6_TRC_ARG3 (pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                                  DATA_PATH_TRC, IP6_NAME,
                                  "IP6OUT:Ip6Send:Src Addr couldn't be found "
                                  "Dst IP6 IF=%d Proto=0x%X DstAddr=%s\n",
                                  pIf6->u4Index, u1Proto,
                                  Ip6ErrPrintAddr (ERROR_FATAL, pDst));

                    Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE,
                                   IP6_CORE_SUBMODULE);
                    return (IP6_FAILURE);
                }
            }
            /*
             * form the IPv6 and Extension headers as well as do checksum
             */

            if (Ip6FormPkt (pIf6, pSrc, pDst, &u4Len,
                            u1Proto, pBuf, u1Hlim) != IP6_SUCCESS)
            {
                IP6IF_INC_OUT_DISCARDS (pIf6);
                IP6SYS_INC_OUT_DISCARDS (pIp6Cxt->Ip6SysStats);

                IP6_TRC_ARG4 (pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                              DATA_PATH_TRC, IP6_NAME,
                              "IP6OUT:Ip6Send:pktconstrn fail,Dst IP6 IF= %d "
                              "Len= %d Proto= 0x%X BufPtr= %p ",
                              pIf6->u4Index, u4Len, u1Proto, pBuf);

                IP6_TRC_ARG2 (pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                              DATA_PATH_TRC, IP6_NAME,
                              "IP6OUT:Ip6Send:SrcAddr= %s DstAddr= %s\n",
                              Ip6ErrPrintAddr (ERROR_FATAL, pSrc),
                              Ip6ErrPrintAddr (ERROR_FATAL, pDst));

                Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE,
                               IP6_CORE_SUBMODULE);
                return (IP6_FAILURE);
            }

            /* Pcaket is destined to this router, So post back the message 
             * to IP6 task only so that it reaches the destined 
             * application.
             * If the destination address is assigned to the tunnel 
             * interface , give the pkt directly to ip6*/

            Ip6Input (pIf6, pBuf, NULL);
            return (IP6_SUCCESS);
        }
    }
    switch (u1AddrType)
    {

        case ADDR6_MULTI:

            /* check that outgoing interface is UP */
            if (Ip6SendIfCheck (pIf6) != IP6_SUCCESS)
            {

                IP6_TRC_ARG1 (pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                              DATA_PATH_TRC, IP6_NAME,
                              "IP6OUT:Ip6Send:Send check on IP6 IFptr = %p "
                              "failed - Interface Down\n", pIf6);
                Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE,
                               IP6_CORE_SUBMODULE);
                return (IP6_FAILURE);
            }
            pDstAddr6 = pDst;
            IP6IF_INC_OUT_MCAST_PKT (pIf6);
            IP6SYS_INC_OUT_MCAST_PKT (pIp6Cxt->Ip6SysStats);
            IP6IF_INC_OUT_MCAST_OCTETS (pIf6, u4Len);
            IP6SYS_INC_OUT_MCAST_OCTETS (pIp6Cxt->Ip6SysStats, u4Len);
            break;

        case ADDR6_LLOCAL:

            /* check that outgoing interface is UP */
            if (Ip6SendIfCheck (pIf6) != IP6_SUCCESS)
            {

                IP6_TRC_ARG1 (pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                              DATA_PATH_TRC, IP6_NAME,
                              "IP6OUT:Ip6Send:Send check on IP6 IFptr = %p "
                              "failed - Interface Down\n", pIf6);
                Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE,
                               IP6_CORE_SUBMODULE);
                return (IP6_FAILURE);
            }
            /*
             * get Neighbor Cache entry if sending out on an Ethernet or tunl
             * interface; for other interfaces, just knowing the correct
             * outgoing interface will do
             */

            if ((pIf6->u1IfType == IP6_ENET_INTERFACE_TYPE) ||
                (pIf6->u1IfType == IP6_PSEUDO_WIRE_INTERFACE_TYPE) ||
                (pIf6->u1IfType == IP6_L3VLAN_INTERFACE_TYPE) ||
                (pIf6->u1IfType == IP6_LAGG_INTERFACE_TYPE) ||
                (pIf6->u1IfType == IP6_L3SUB_INTF_TYPE)
#ifdef TUNNEL_WANTED
                || (pIf6->u1IfType == IP6_TUNNEL_INTERFACE_TYPE)
#endif
#ifdef WGS_WANTED
                || (pIf6->u1IfType == IP6_L2VLAN_INTERFACE_TYPE)
#endif
                )
            {
                pNd6c = Nd6LookupCache (pIf6, pDst);
            }

            pDstAddr6 = pDst;

            IP6_TRC_ARG2 (pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                          DATA_PATH_TRC, IP6_NAME,
                          "IP6OUT:Ip6Send: ND Lookup returned NDC = %p for "
                          "Link-local Dst addr = %s",
                          pNd6c, Ip6PrintAddr (pDst));
            break;

        default:

#ifdef MN_WANTED
            if (i1RevTunlFlag != 1)
            {
#endif
                /* 
                 * Find a route to this destination and get the dest interface
                 * and neighbor cache pointer (if exists)
                 * Locking of tasks is done here as we access the routing table 
                 * which is in RIP6 task and could get updated simultaneously.
                 */

                pNdIp6If = Ip6GetRouteInCxt (pIp6Cxt->u4ContextId, pDst,
                                             &pNd6c, &NetIp6RtInfo);

                if ((pNdIp6If == NULL)
                    || ((pNdIp6If != pIf6) && (pIf6 != NULL)))
                {
                    if (Ip6IsOurAddrInCxt (pIp6Cxt->u4ContextId, pSrc,
                                           &u4TmpIndex) == IP6_SUCCESS)
                    {
                        /* pNdIp6If is null. Stats will any how not be updated */
                        IP6SYS_INC_OUT_NO_ROUTE (pIp6Cxt->Ip6SysStats);
                    }
                    else
                    {
                        /* pNdIp6If is null. Stats will any how not be updated */
                        IP6SYS_INC_IN_NO_ROUTE (pIp6Cxt->Ip6SysStats);
                    }

                    Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE,
                                   IP6_CORE_SUBMODULE);
                    return (IP6_FAILURE);
                }
                pIf6 = pNdIp6If;

                /* check that outgoing interface is UP */
                if (Ip6SendIfCheck (pIf6) != IP6_SUCCESS)
                {
                    IP6_TRC_ARG1 (pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                                  DATA_PATH_TRC, IP6_NAME,
                                  "IP6OUT:Ip6Send:Send check on IP6 IFptr = %p"
                                  " failed - Interface Down\n", pIf6);
                    Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE,
                                   IP6_CORE_SUBMODULE);
                    return (IP6_FAILURE);
                }

                /* Nd6 Lookup is not required here since If route or ND 
                   failure pIf6 will be NULL, that is handled in previous 
                   step */
                u1RtType = (UINT1) NetIp6RtInfo.i1Type;

                if (u1RtType == DIRECT)
                {
                    pDstAddr6 = pDst;
                }
                else
                {
                    pDstAddr6 = &NetIp6RtInfo.NextHop;
                }
                IP6_TRC_ARG5 (pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                              DATA_PATH_TRC, IP6_NAME,
                              "IP6OUT:Ip6Send:Dst IP6 IF=%d NDCPtr=%p "
                              "Proto=0x%X DstAddr=%s NextHop=%s",
                              pIf6->u4Index, pNd6c, u1Proto,
                              Ip6PrintAddr (pDst), Ip6PrintAddr (pDstAddr6));
                break;
#ifdef MN_WANTED
            }
            pDstAddr6 = pDst;
#endif
    }                            /* switch */

    IP6IF_INC_OUT_REQS (pIf6);
    IP6SYS_INC_OUT_REQS (pIp6Cxt->Ip6SysStats);

    /* Skip Scope-Zone validation for IPv6 PIM SSM address */
    if (!(IS_ALL_PREFIX_SSM_MULTI (pDst)))
    {
        /* Added as a part of RFC4007 code changes- If the destination 
           address zone is not configured on the interface the packet should
           not be forwarded */

        u1DstAddrScope = Ip6GetAddrScope (pDst);

        if (ADDR6_SCOPE_INVALID == u1DstAddrScope)
        {
            IP6_TRC_ARG1 (pIp6Cxt->u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC,
                          IP6_NAME,
                          "IP6OUT:Ip6Send: Dest Address scope is invalid"
                          " Dst Addr = %s\n", Ip6PrintAddr (pDst));

            Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE,
                           IP6_CORE_SUBMODULE);
            return (IP6_FAILURE);

        }
        IP6_TASK_UNLOCK ();
        if (IP6_ZONE_INVALID == NetIpv6GetIfScopeZoneIndex
            (u1DstAddrScope, pIf6->u4Index))
        {

            i4RetVal = Rtm6ApiRoute6LookupInCxt (pIp6Cxt->u4ContextId, pDst,
                                                 IP6_ADDR_MAX_PREFIX,
                                                 IP6_ROUTE_PREFERENCE_BEST_METRIC,
                                                 &u1Found, &NetIpv6Rt);
            if (i4RetVal != RTM6_FAILURE)
            {
                pDstIf6 = gIp6GblInfo.apIp6If[NetIpv6Rt.u4Index];
                if ((Ip6SendIfCheck (pDstIf6) != IP6_SUCCESS)
                    ||
                    ((MEMCMP (&NetIpv6Rt.NextHop, &tempAddr6, sizeof (tIp6Addr))
                      != 0) && (!(IS_ADDR_LLOCAL (NetIpv6Rt.NextHop)))))
                {
                    IP6_TRC_ARG1 (pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                                  CONTROL_PLANE_TRC, IP6_NAME,
                                  "IP6OUT/FWD:Rtm6ApiRoute6LookupInCxt: Send check on IP6 IfPtr = %p "
                                  "failed\n", pDstIf6);
                    Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE,
                                   IP6_CORE_SUBMODULE);
                    IP6_TASK_LOCK ();
                    return (IP6_FAILURE);
                }
            }
            else
            {
                IP6_TRC_ARG1 (pIp6Cxt->u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC,
                              IP6_NAME,
                              "IP6OUT:Ip6Send: Dest Address zone is not configured"
                              " in the outgoing interface Dst Addr = %s\n",
                              Ip6PrintAddr (pDst));
                Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE,
                               IP6_CORE_SUBMODULE);
                IP6_TASK_LOCK ();
                return (IP6_FAILURE);
            }
        }
        IP6_TASK_LOCK ();
    }
    /*
     * obtain source address if not given to us
     */
    if ((!pSrc) || IS_ADDR_UNSPECIFIED (*pSrc) ||
        (IS_ADDR_MULTI (*pSrc)) || (Ip6IsAddrAnycastInCxt (pIp6Cxt, pSrc)
                                    == IP6_SUCCESS))
    {
        pSrc = Ip6AddrGetSrc (pIf6, pDst);
        if (!pSrc)
        {
            IP6IF_INC_OUT_DISCARDS (pIf6);
            IP6SYS_INC_OUT_DISCARDS (pIp6Cxt->Ip6SysStats);

            IP6_TRC_ARG3 (pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                          DATA_PATH_TRC, IP6_NAME,
                          "IP6OUT:Ip6Send:Src Addr couldn't be found Dst IP6 "
                          "IF= %d Proto=0x%X DstAddr=%s\n",
                          pIf6->u4Index, u1Proto,
                          Ip6ErrPrintAddr (ERROR_FATAL, pDst));

            Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE,
                           IP6_CORE_SUBMODULE);
            return (IP6_FAILURE);
        }
    }
    else
    {
        /* In the case of host address might have become invalid (for 
         * communications which have started earlier, such as tcp ), 
         * so check the same before transmitting the packet with the 
         * address which is not existing.
         */
        u1Type = (UINT1) Ip6AddrType (pSrc);
        if (((Ip6IsPktToMeInCxt (pIp6Cxt, &u1Type, 1, pSrc, pIf6))
             != IP6_SUCCESS)
#ifdef MN_WANTED
            /* should be MIP6_SUCCESS */
            && (Mip6IsHomeAddr (pSrc, &u4TunlIndex) != IP6_SUCCESS)
#endif
            )
        {
            IP6_TRC_ARG2 (pIp6Cxt->u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC,
                          IP6_NAME,
                          "IP6OUT: Ip6Send: Src Addr has become invalid "
                          "for Dst IP6 IF= %d SrcAddr = %s",
                          pIf6->u4Index, Ip6ErrPrintAddr (ERROR_FATAL, pSrc));
            IP6_TRC_ARG1 (pIp6Cxt->u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC,
                          IP6_NAME,
                          "DstAddr= %s\n", Ip6ErrPrintAddr (ERROR_FATAL, pDst));

            Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE,
                           IP6_CORE_SUBMODULE);
            return (IP6_FAILURE);
        }
    }

    /*
     * form the IPv6 and Extension headers as well as do checksum
     */

    if (Ip6FormPkt (pIf6, pSrc, pDst, &u4Len,
                    u1Proto, pBuf, u1Hlim) != IP6_SUCCESS)
    {
        IP6IF_INC_OUT_DISCARDS (pIf6);
        IP6SYS_INC_OUT_DISCARDS (pIp6Cxt->Ip6SysStats);

        IP6_TRC_ARG4 (pIp6Cxt->u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC,
                      IP6_NAME,
                      "IP6OUT:Ip6Send:pktconstrn fail,Dst IP6 IF= %d Len= %d "
                      "Proto= 0x%X BufPtr= %p ", pIf6->u4Index, u4Len, u1Proto,
                      pBuf);

        IP6_TRC_ARG2 (pIp6Cxt->u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC,
                      IP6_NAME, "IP6OUT:Ip6Send: SrcAddr= %s DstAddr= %s\n",
                      Ip6ErrPrintAddr (ERROR_FATAL, pSrc),
                      Ip6ErrPrintAddr (ERROR_FATAL, pDst));

        Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE, IP6_CORE_SUBMODULE);
        return (IP6_FAILURE);
    }

    IP6_TRC_ARG6 (pIp6Cxt->u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                  "IP6OUT:Ip6Send: SendingOn IF=%d Len=%d Proto=0x%X "
                  "BufPtr=%p Src=%s Dst=%s \n",
                  pIf6->u4Index, u4Len, u1Proto, pBuf,
                  Ip6PrintAddr (pSrc), Ip6PrintAddr (pDst));

    IP6_TRC_ARG1 (pIp6Cxt->u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                  "IP6OUT:Ip6Send:NextHop=%s\n", Ip6PrintAddr (pDstAddr6));

    if (pIp6Cxt->u1PmtuEnable == IP6_PMTU_ENABLE)
    {
        ip6PmtuLookupEntryInCxt (pIp6Cxt->u4ContextId, pDst, &pPmtuEntry);
        if (pPmtuEntry != NULL)
        {
            u4Mtu = pPmtuEntry->u4Pmtu;
        }
        else
        {
            pIf6->u1TooBigMsgReceived = OSIX_FALSE;
            u4Mtu = Ip6ifGetMtu (pIf6);
        }
    }
    else
    {
        pIf6->u1TooBigMsgReceived = OSIX_FALSE;
        u4Mtu = Ip6ifGetMtu (pIf6);
    }
    if ((u4Len > u4Mtu) || (pIf6->u1TooBigMsgReceived == OSIX_TRUE))
    {
        IP6SYS_INC_OUT_FRAG_REQS (pIp6Cxt->Ip6SysStats);
        IP6IF_INC_OUT_FRAG_REQS (pIf6);

        IP6_TRC_ARG3 (pIp6Cxt->u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC,
                      IP6_NAME,
                      "IP6OUT:Ip6Send: Pkt len %d greater than MTU %d "
                      "Dst IP6 IF = %d\n", u4Len, u4Mtu, pIf6->u4Index);
        if (Ip6FragSend
            (pIf6, pDstAddr6, pNd6c, u4Mtu, u4Len, pBuf,
             NetIp6RtInfo.u1AddrType) == FAILURE)
        {
            Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE,
                           IP6_CORE_SUBMODULE);
            return (IP6_FAILURE);
        }
    }
    else
    {
        Ip6ifSend (pIf6, pDstAddr6, pNd6c, u4Len, pBuf,
                   NetIp6RtInfo.u1AddrType);
        return IP6_SUCCESS;
    }

    return (IP6_SUCCESS);

}

/*****************************************************************************
* DESCRIPTION : This routine is used by ND6 module alone for sending ND
*               messages, since these don't need address resolution and other
*               stuff. The routine Prepends the IPv6 header to the ND packet and
*               enqueues it to TEST for transmission over the interface
* 
* INPUTS      : pIf6        -  Pointer to Logical interface
*               pNd6cEntry -  Pointer to NDCache entry
*               pSrcAddr6  -  Pointer to IPv6 source address
*               pDstAddr6  -  Pointer to IPv6 destination address
*               u4Nd6Size  -  Size of ND Message
*               pBuf        -  Pointer to the IPv6 packet
*                               to be sent
* 
* OUTPUTS     : None
* 
* RETURNS     : IP6_SUCCESS      -   Sending of datagram successful
*               IP6_FAILURE  -   Sending fails
****************************************************************************/

INT4
Ip6SendNdMsg (tIp6If * pIf6, tNd6CacheEntry * pNd6c, tIp6Addr * pSrc,
              tIp6Addr * pDst, UINT4 u4Nd6Size,
              tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 u1SeNDFlag,
              tIp6Addr * pTargetAddr)
{
    UINT1               u1LlaLen = 0, au1dstmac[IP6_ENET_ADDR_LEN];
    UINT4               u4PktLen = u4Nd6Size;
    tNd6CacheLanInfo   *pNd6cLinfo = NULL;
    tIp6Cxt            *pIp6Cxt = NULL;

    pIp6Cxt = pIf6->pIp6Cxt;
    IP6_TRC_ARG2 (pIp6Cxt->u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                  "IP6OUT:Ip6SendNdMsg:Trying to Send ND pkt from src %s to Dst %s\n",
                  Ip6PrintAddr (pSrc), Ip6PrintAddr (pDst));

    IP6_TRC_ARG4 (pIp6Cxt->u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                  "IP6OUT:Ip6SendNdMsg:IfPtr=%p Len=%d NDC=%p BufPtr=%p\n",
                  pIf6, u4Nd6Size, pNd6c, pBuf);

    /* check that outgoing interface is UP */

    if (Ip6SendIfCheck (pIf6) != IP6_SUCCESS)
    {

        IP6_TRC_ARG1 (pIp6Cxt->u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC,
                      IP6_NAME,
                      "IP6OUT:Ip6SendNdMsg: Send check on IP6 IFptr = %p "
                      "failed Protocol is ND\n", pIf6);

        Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE, IP6_CORE_SUBMODULE);
        return (IP6_FAILURE);
    }

    IP6IF_INC_OUT_REQS (pIf6);
    IP6SYS_INC_OUT_REQS (pIp6Cxt->Ip6SysStats);

    /*
     * get the destination MAC addresses from the ND cache or from the
     * address itself if it is a multicast
     */

    if (Ip6AddrType (pDst) == ADDR6_MULTI)
    {
        Ip6ifFormEthMaddr (pDst, (UINT1 *) &au1dstmac, &u1LlaLen);
        IP6IF_INC_OUT_MCAST_PKT (pIf6);
        IP6SYS_INC_OUT_MCAST_PKT (pIp6Cxt->Ip6SysStats);
        IP6IF_INC_OUT_MCAST_OCTETS (pIf6, u4Nd6Size);
        IP6SYS_INC_OUT_MCAST_OCTETS (pIp6Cxt->Ip6SysStats, u4Nd6Size);
    }
    else
    {
        if (pNd6c != NULL)
        {
            pNd6cLinfo = (tNd6CacheLanInfo *) (VOID *) pNd6c->pNd6cInfo;
            u1LlaLen =
                (UINT1) MEM_MAX_BYTES (pNd6cLinfo->u1LlaLen, IP6_ENET_ADDR_LEN);
            MEMCPY (&au1dstmac, pNd6cLinfo->lladdr, u1LlaLen);
        }
        else
        {
            IP6IF_INC_OUT_DISCARDS (pIf6);
            IP6SYS_INC_OUT_DISCARDS (pIp6Cxt->Ip6SysStats);
            Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE,
                           IP6_CORE_SUBMODULE);
            return (IP6_FAILURE);
        }
    }

    /*
     * add secure options, before adding the
     * ipv6 header and ICMPv6 checksum calculation
     */
    if (IP6_TRUE == IF_SEND_ENABLED (pIf6))
    {
        if ((ND6_SEND_ADD != u1SeNDFlag) && (ND6_PROXY_MSG != u1SeNDFlag))
        {
            if (IP6_FAILURE ==
                Nd6SeNDAddOpts (pIf6, pSrc, pDst, pBuf,
                                u1SeNDFlag, pTargetAddr, &u4PktLen))
            {
                /* CRU Buf is release in the called function itself */
                return IP6_FAILURE;
            }
        }
    }

    /*
     * form the IPv6 and Extension headers as well as do checksum
     */

    if (Ip6FormPkt (pIf6, pSrc, pDst,
                    &u4PktLen, NH_ICMP6, pBuf,
                    IP6_MAX_HOP_LIMIT) == IP6_FAILURE)
    {
        IP6_TRC_ARG3 (pIp6Cxt->u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC,
                      IP6_NAME,
                      "IP6OUT:Ip6SendNdMsg:ConstrnND PktFailed, DstIF=%d "
                      "Len=%d BufPtr=%p", pIf6->u4Index, u4PktLen, pBuf);
        IP6_TRC_ARG2 (pIp6Cxt->u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC,
                      IP6_NAME, "SrcAddr=%s DstAddr=%s\n",
                      Ip6ErrPrintAddr (ERROR_FATAL, pSrc),
                      Ip6ErrPrintAddr (ERROR_FATAL, pDst));

        IP6IF_INC_OUT_DISCARDS (pIf6);
        IP6SYS_INC_OUT_DISCARDS (pIp6Cxt->Ip6SysStats);
        Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE, IP6_CORE_SUBMODULE);
        return (IP6_FAILURE);
    }

    IP6_TRC_ARG (pIp6Cxt->u4ContextId, ND6_MOD_TRC, BUFFER_TRC, ND6_NAME,
                 "Ip6SendNdMsg: The send buffer is :\n");
    IP6_PKT_DUMP (pIp6Cxt->u4ContextId, ND6_MOD_TRC, DUMP_TRC, ND6_NAME, pBuf,
                  0);

    /*
     * set MAC addresses in buffer and send out the datagram
     */

    Ip6ifFormEthHdr ((UINT1 *) &au1dstmac, pBuf);
    Ip6Output (pIf6, u4PktLen, pBuf);

    return (IP6_SUCCESS);

}

#ifdef TUNNEL_WANTED
/*****************************************************************************
* DESCRIPTION : This routine is used by ND6 module alone for sending ND
*               messages over tunnels for NUD. After forming ND message the 
*               v4 source address will obtained and packet will be enqued 
*               to V4 task. 
* INPUTS      : pIf6        -  Pointer to Logical interface
*               pNd6cEntry -  Pointer to NDCache entry
*               pSrcAddr6  -  Pointer to IPv6 source address
*               pDstAddr6  -  Pointer to IPv6 destination address
*               u4Nd6Size  -  Size of ND Message
*               pBuf        -  Pointer to the IPv6 packet
*                               to be sent
* 
* OUTPUTS     : None
* 
* RETURNS     : IP6_SUCCESS      -   Sending of datagram successful
*               IP6_FAILURE  -   Sending fails
****************************************************************************/

INT4
Ip6SendNdMsgOverTunl (tIp6If * pIf6, tNd6CacheEntry * pNd6c, tIp6Addr * pSrc,
                      tIp6Addr * pDst, UINT4 u4Nd6Size,
                      tCRU_BUF_CHAIN_HEADER * pBuf)
{
    tIp6Cxt            *pIp6Cxt = NULL;
    UINT4               u4Len = u4Nd6Size;
    UINT4               u4DstIp;
    UINT4               u4Mtu;
    UINT2               u2DontFrag = 0;
    UINT2               u2Proto = IP6_PTCL;

    /* To handle warnings in case of Trace is not defined */
    UNUSED_PARAM (pNd6c);

    pIp6Cxt = pIf6->pIp6Cxt;

    IP6_TRC_ARG2 (pIp6Cxt->u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                  "IP6OUT:Ip6SendNdMsgOverTunl:Trying to Send ND pkt from "
                  "src %s to Dst %s\n",
                  Ip6PrintAddr (pSrc), Ip6PrintAddr (pDst));

    IP6_TRC_ARG4 (pIp6Cxt->u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                  "IP6OUT:Ip6SendNdMsgOverTunl:IfPtr=%p Len=%d NDC=%p "
                  "BufPtr=%p\n", pIf6, u4Nd6Size, pNd6c, pBuf);

    /* check that outgoing interface is UP */

    if (Ip6SendIfCheck (pIf6) != IP6_SUCCESS)
    {

        IP6_TRC_ARG1 (pIp6Cxt->u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC,
                      IP6_NAME,
                      "IP6OUT:Ip6SendNdMsgOverTunl:Send check on IP6 "
                      "IFptr =%p failed Protocol is ND\n", pIf6);

        Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE, IP6_CORE_SUBMODULE);
        return (IP6_FAILURE);
    }

    IP6IF_INC_OUT_REQS (pIf6);
    IP6SYS_INC_OUT_REQS (pIp6Cxt->Ip6SysStats);

    /*
     * form the IPv6 and Extension headers as well as do checksum
     */

    if (Ip6FormPkt (pIf6, pSrc, pDst,
                    &u4Len, NH_ICMP6, pBuf, IP6_MAX_HOP_LIMIT) == IP6_FAILURE)
    {
        IP6_TRC_ARG3 (pIp6Cxt->u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC,
                      IP6_NAME,
                      "P6OUT:Ip6SendNdMsgOverTunl:ConstrnND PktFailed, "
                      "DstIF=%d Len=%d BufPtr=%p", pIf6->u4Index, u4Len, pBuf);
        IP6_TRC_ARG2 (pIp6Cxt->u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC,
                      IP6_NAME, "SrcAddr=%s DstAddr=%s\n",
                      Ip6ErrPrintAddr (ERROR_FATAL, pSrc),
                      Ip6ErrPrintAddr (ERROR_FATAL, pDst));

        IP6IF_INC_OUT_DISCARDS (pIf6);
        IP6SYS_INC_OUT_DISCARDS (pIp6Cxt->Ip6SysStats);
        Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE, IP6_CORE_SUBMODULE);
        return (IP6_FAILURE);
    }

    /* Configured tunneling can be for ipv4 compatible ipv6 addresses
     * also. Based on the lower layer if it is auto tunnel interface v4
     * destination is extracted from v6 address other wise it is taken
     * from configured tunnel table.
     */
    if (pIf6->pTunlIf->u1TunlType == IPV6_AUTO_COMPAT)
    {
        /* Automatic Compatible Tunnel. Get the v4 Destination address
         * from the Compatible V6 Destination Address */
        if (IS_ADDR_MULTI (*pDst))
        {
            /* If the Destination happens to be multicast address and
             * if v4 Destination address is configured, then the packets
             * are sent to that address. */
            u4DstIp = IP6_TUNLIF_DST (pIf6);
            u4DstIp = HTONL (u4DstIp);
        }
        else
        {
            if (IS_ADDR_V4_COMPAT (*pDst) == 0)
            {
                /* Destination is not V4 Compatible. Reject it. */
                IP6IF_INC_OUT_DISCARDS (pIf6);
                IP6SYS_INC_OUT_DISCARDS (pIp6Cxt->Ip6SysStats);
                Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE,
                               IP6_CORE_SUBMODULE);
                return IP6_FAILURE;
            }

            u4DstIp = HTONL (pDst->u4_addr[IP6_THREE]);
        }
        if (Ip6ValidateIpv4Addr (u4DstIp, pIf6->u4Index) == IP6_FAILURE)
        {
            IP6_TRC_ARG1 (pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                          DATA_PATH_TRC | BUFFER_TRC, IP6_NAME,
                          "IP6OUT:Ip6SendNdMsgOverTunl:Destination address is "
                          "not a valid address %x\n", u4DstIp);
            IP6IF_INC_OUT_DISCARDS (pIf6);
            IP6SYS_INC_OUT_DISCARDS (pIp6Cxt->Ip6SysStats);
            Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE, IP6_IF_SUBMODULE);
            return IP6_FAILURE;
        }

        u4Mtu = IP6_MIN_MTU;
    }
    else if (pIf6->pTunlIf->u1TunlType == IPV6_SIX_TO_FOUR)
    {
        /* Automatic SixToFour Tunnel. Get the v4 Destination address
         * from the 6to4 V6 Destination Address */
        if (IS_ADDR_MULTI (*pDst))
        {
            /* If the Destination happens to be multicast address and
             * if v4 Destination address is configured, then the packets
             * are sent to that address. */
            u4DstIp = IP6_TUNLIF_DST (pIf6);
            u4DstIp = HTONL (u4DstIp);
        }
        else
        {
            if (IS_ADDR_6to4 (*pDst) == 0)
            {
                /* Destination is not V4 Compatible. Reject it. */
                IP6IF_INC_OUT_DISCARDS (pIf6);
                IP6SYS_INC_OUT_DISCARDS (pIp6Cxt->Ip6SysStats);
                Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE,
                               IP6_CORE_SUBMODULE);
                return IP6_FAILURE;
            }

            MEMCPY ((UINT1 *) &u4DstIp, &pDst->u1_addr[IP6_TWO], IP6_FOUR);
            u4DstIp = HTONL (u4DstIp);
        }
        if (Ip6ValidateIpv4Addr (u4DstIp, pIf6->u4Index) == IP6_FAILURE)
        {
            IP6_TRC_ARG1 (pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                          DATA_PATH_TRC | BUFFER_TRC, IP6_NAME,
                          "IP6OUT:Ip6SendNdMsgOverTunl:Destination address is "
                          "not a valid address %x\n", u4DstIp);
            IP6IF_INC_OUT_DISCARDS (pIf6);
            IP6SYS_INC_OUT_DISCARDS (pIp6Cxt->Ip6SysStats);
            Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE, IP6_IF_SUBMODULE);
            return IP6_FAILURE;
        }

        u4Mtu = IP6_MIN_MTU;
    }
    else if (pIf6->pTunlIf->u1TunlType == IPV6_ISATAP_TUNNEL)
    {
        /* Automatic isatap Tunnel. Get the v4 Destination address
         * from the isatap Destination Address */
        if (IS_ADDR_MULTI (*pDst))
        {
            /* If the Destination happens to be multicast address and
             * if v4 Destination address is configured, then the packets
             * are sent to that address. */
            u4DstIp = IP6_TUNLIF_DST (pIf6);
            u4DstIp = HTONL (u4DstIp);
        }
        else
        {
            if (IS_ADDR_ISATAP (*pDst) == 0)
            {
                /* Destination is not isatap. Reject it. */
                IP6IF_INC_OUT_DISCARDS (pIf6);
                IP6SYS_INC_OUT_DISCARDS (pIp6Cxt->Ip6SysStats);
                Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE, ND6_MODULE);
                return IP6_FAILURE;
            }

            MEMCPY ((UINT1 *) &u4DstIp, &pDst->u1_addr[0], IP6_FOUR);
            u4DstIp = HTONL (u4DstIp);
        }

        if (Ip6ValidateIpv4Addr (u4DstIp, pIf6->u4Index) == IP6_FAILURE)
        {
            IP6_TRC_ARG1 (pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                          DATA_PATH_TRC | BUFFER_TRC, IP6_NAME,
                          "IP6OUT:Ip6SendNdMsgOverTunl:Destination address is "
                          "not a valid address %x\n", u4DstIp);
            IP6IF_INC_OUT_DISCARDS (pIf6);
            IP6SYS_INC_OUT_DISCARDS (pIp6Cxt->Ip6SysStats);
            Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE, IP6_IF_SUBMODULE);
            return IP6_FAILURE;
        }

        u4Mtu = IP6_MIN_MTU;
    }
    else
    {
        /* Configured Tunnel. */
        if (pIf6->pTunlIf->u1TunlType == IPV6_GRE_TUNNEL)
        {
            u2Proto = IP6_GRE_PTCL;
        }
        u4DstIp = IP6_TUNLIF_DST (pIf6);
        u4DstIp = HTONL (u4DstIp);
        u4Mtu = IP6_TUNLIF_MTU (pIf6);
        if (u4Mtu < IP6_MIN_MTU)
        {
            u4Mtu = IP6_MIN_MTU;
        }
    }

    if (u4Mtu < (u4Len + IP6_TUNL_HDR_LEN))
    {
        u2DontFrag = HTONS (0x4000);
    }

    if (Ip6TunnelOutput (u4DstIp, IP6_DEF_TOS /* 0 */ , u2Proto,
                         IP6_DEF_TTL /* 0 */ , pBuf, (UINT2) u4Len,
                         0 /* i2Id */ , (UINT1) u2DontFrag, 0,
                         pIf6->u4Index) == IP6_FAILURE)
    {
        IP6_TRC_ARG (pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                     DATA_PATH_TRC | BUFFER_TRC, IP6_NAME,
                     "IP6OUT:Ip6SendNdMsgOverTunl:Failure in sending the "
                     "packet to v4 stack \n");
        Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE, IP6_IF_SUBMODULE);
        IP6_TRC_ARG2 (pIp6Cxt->u4ContextId, IP6_MOD_TRC, MGMT_TRC | BUFFER_TRC,
                      IP6_NAME,
                      "IP6OUT:Ip6SendNdMsgOverTunl:%s gen err:, Type = %d  ",
                      ERROR_FATAL_STR, ERR_GEN_INVALID_VAL);
        return IP6_FAILURE;
    }

    return IP6_SUCCESS;
}
#endif
/******************************************************************************
 * DESCRIPTION : This routine forms the IPv6 header and if required the
 *               Authentication header for the packet. The checksum for the
 *               higher layer is also calculated and filled into the packet.
 *
 * INPUTS      : The IPv6 interface pointer (pIf6), the source and dest
 *               addresses (pSrc & pDst), length of the higher layer
 *               data (pLen), the protocol (u1Proto) and the buffer 
 *               containing the datagram (pBuf)
 *
 * OUTPUTS     : The length of the complete IPv6 packet (pLen)
 *
 * RETURNS     : OK, if the header formation succeeds
 *               NOT_OK, if the header formation fails
 *
 * NOTES       : Assumption is made that the only extension header possible
 *               for a packet generated by the router is Authentication
 *               header (packet could get fragmented later).
 ******************************************************************************/

INT4
Ip6FormPkt (tIp6If * pIf6, tIp6Addr * pSrc, tIp6Addr * pDst, UINT4 *pLen,
            UINT1 u1Proto, tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 u1Hlim)
{

    tIp6Cxt            *pIp6Cxt = NULL;
    UINT1               u1Copy = 0;
    UINT2               u2Cksum = 0;
    UINT4               u4HdrSize = 0;
    tIp6Hdr             ip6Hdr, *pIp6 = NULL;
    tJmbHdr             JmbHdr, *pJmbHdr = NULL;
    UINT1               u1NxtProto = 0;
    tIp6RtAlertHdr      RtAlertHdr;
    tExtHdr             HbyhHdr, *pHbyhHdr = NULL;
    UINT1               u1Icmp6Type = 0;
    UINT2               u2Align = 0;    /* Used for alignment while
                                           constructing the Hop-by-Hop
                                           header with Router Alert option */

    UINT1               u1HdrExtLen = 0;    /* used for calculating
                                               Header extension length
                                               (for hop-by-hop/dest opt)
                                               and to fill in the extension
                                               header - 0 for both Router Alert
                                               and Jumbogram */
    UINT4               u4Offset = 0;
    UINT2               u2HLOffset = 0;
    UINT1               u1Nhdr = 0;
    UINT4               u4Len = 0;
    tIp6Addr            RtDst;

    pIp6Cxt = pIf6->pIp6Cxt;

    /* 
     * determine checksum and fill into buffer; this is done here instead
     * of by the higher layers as the addresses may not be known by the
     * higher layer
     */

    if (u1Proto == NH_ROUTING_HDR)
    {
        u4Offset = IP6_BUF_READ_OFFSET (pBuf);
        if (Ip6BufRead (pBuf, (UINT1 *) &RtDst, BYTE_LENGTH, sizeof (tIp6Addr),
                        1) == NULL)
        {
            return IP6_FAILURE;
        }
        IP6_BUF_READ_OFFSET (pBuf) = 0;
        u1Nhdr = u1Proto;
        if (Ip6GetHlProtocolFromHLPktInCxt (pIp6Cxt, pBuf, &u1Nhdr,
                                            &u2HLOffset) == IP6_SUCCESS)
        {
            if ((u1Nhdr == NH_ICMP6) || (u1Nhdr == NH_UDP6))
            {
                IP6_BUF_READ_OFFSET (pBuf) = u2HLOffset;
                u4Len = CRU_BUF_Get_ChainValidByteCount (pBuf);
                u4Len -= u2HLOffset;
                u2Cksum = Ip6Checksum (pSrc, &RtDst, u4Len, u1Nhdr, pBuf);
                if (u1Nhdr == NH_ICMP6)
                {
                    if (Ip6BufWrite (pBuf, (UINT1 *) &u2Cksum, u2HLOffset +
                                     IP6_OFFSET (tIcmp6PktHdr, u2Chksum),
                                     sizeof (UINT2), TRUE) == IP6_FAILURE)
                    {
                        return IP6_FAILURE;
                    }
                }
                if (u1Nhdr == NH_UDP6)
                {
                    if (Ip6BufWrite (pBuf, (UINT1 *) &u2Cksum, u2HLOffset +
                                     IP6_OFFSET (tUdp6Hdr, u2Chksum),
                                     sizeof (UINT2), TRUE) == IP6_FAILURE)
                    {
                        return IP6_FAILURE;
                    }
                }
            }
        }
        IP6_BUF_READ_OFFSET (pBuf) = u4Offset;
    }

    if ((u1Proto == NH_UDP6) || (u1Proto == NH_ICMP6))
    {
        u2Cksum = Ip6Checksum (pSrc, pDst, *pLen, u1Proto, pBuf);

        /* RFC 2460 - Section 8.1 */
        if ((u1Proto == NH_UDP6) && (u2Cksum == 0x0))
        {
            u2Cksum = IP6_BIT_ALL;
        }

        Ip6FillCksum (u2Cksum, u1Proto, pBuf);
    }

    if (u1Proto == NH_ICMP6)
    {
        Ip6BufRead (pBuf, &u1Icmp6Type, IP6_OFFSET (tIcmp6PktHdr, u1Type),
                    sizeof (UINT1), TRUE);
        IP6_BUF_READ_OFFSET (pBuf) -= sizeof (UINT1);
        /* Only for MLD packets, RouterAlert option is constructed */
        if ((u1Icmp6Type == ICMP6_MLD_QUERY) ||
            (u1Icmp6Type == ICMP6_MLD_REPORT) ||
            (u1Icmp6Type == ICMP6_MLD_DONE))
        {
            /* Update the icmp6 stats */
            switch (u1Icmp6Type)
            {
                case ICMP6_MLD_QUERY:
                    ICMP6_INC_OUT_MLD_QUERY (pIp6Cxt->Icmp6Stats);
                    ICMP6_INTF_INC_OUT_MLD_QUERY (pIf6);
                    break;
                case ICMP6_MLD_REPORT:
                    ICMP6_INC_OUT_MLD_REPORT (pIp6Cxt->Icmp6Stats);
                    ICMP6_INTF_INC_OUT_MLD_REPORT (pIf6);
                    break;
                case ICMP6_MLD_DONE:
                    ICMP6_INC_OUT_MLD_DONE (pIp6Cxt->Icmp6Stats);
                    ICMP6_INTF_INC_OUT_MLD_DONE (pIf6);
                    break;
            }

            /*
             * For alignement purpose, 2 bytes are to be prepended 
             * (alignment requirement for Router Alert:2n+0 -  PadN 
             * option of lenth 0); This is done after prepending
             * Router Alert option first. 
             * When the router receives Router Alert option, 
             * after seeing its option value, the packet will be 
             * directly given to the corresponding module. Hence
             * the PadN option should be before Router Alert Option.
             */

            RtAlertHdr.u1OptType = IP6_ROUTE_ALERT_OPT;
            RtAlertHdr.u1OptLen = IP6_RT_ALERT_OPT_VALLEN;
            RtAlertHdr.u2OptValue = ICMP6_MLD_PKT;

            if (Ip6BufPptr
                (pBuf, (UINT1 *) &RtAlertHdr, sizeof (tIp6RtAlertHdr),
                 &u1Copy) == NULL)
            {
                return IP6_FAILURE;
            }

            u2Align = IP6_RT_ALERT_ALIGN;

            if (Ip6BufPptr (pBuf, (UINT1 *) &u2Align, sizeof (UINT2),
                            &u1Copy) == NULL)
            {
                return IP6_FAILURE;
            }

            u1NxtProto = u1Proto;
            u1Proto = NH_H_BY_H;

            /*
             * Incase of Router Alert the Header Extension
             * length is 0
             */
            *pLen += sizeof (tIp6RtAlertHdr) + sizeof (UINT2);
        }
    }

    if (*pLen == 0)
    {
        pJmbHdr = (tJmbHdr *) Ip6BufPptr (pBuf, (UINT1 *) &JmbHdr,
                                          JMB_HDR_LEN, &u1Copy);
        if (!pJmbHdr)
        {
            IP6_TRC_ARG (pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                         BUFFER_TRC | ALL_FAILURE_TRC, IP6_NAME,
                         "IP6OUT/FWD:Ip6FormPkt: Ip6BufPptr failed!\n");
            IP6_TRC_ARG3 (pIp6Cxt->u4ContextId, IP6_MOD_TRC, BUFFER_TRC,
                          IP6_NAME,
                          "IP6OUT/FWD:Ip6FormPkt: %s buffer error occurred, Type = %d Bufptr = %p",
                          ERROR_FATAL_STR, ERR_BUF_WRITE, pBuf);
            return (IP6_FAILURE);
        }

        pJmbHdr->u1OptType = JMB_OPT_TYPE;
        pJmbHdr->u1OptLen = JMB_OPT_LEN;

        *pLen = pJmbHdr->u4JmbLen = CRU_BUF_Get_ChainValidByteCount (pBuf);

        u1NxtProto = u1Proto;
        u1Proto = NH_H_BY_H;
        pIp6Cxt->u2JmbSendPkts++;
    }

    if (u1Proto == NH_H_BY_H)
    {
        HbyhHdr.u1NextHdr = u1NxtProto;
        HbyhHdr.u1HdrLen = u1HdrExtLen;
        pHbyhHdr = (tExtHdr *) Ip6BufPptr (pBuf, (UINT1 *) &HbyhHdr,
                                           IP6_TYPE_LEN_BYTES, &u1Copy);
        if (!pHbyhHdr)
        {
            IP6_TRC_ARG (pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                         BUFFER_TRC | ALL_FAILURE_TRC, IP6_NAME,
                         "IP6OUT/FWD: Ip6FormPkt: Ip6BufPptr failed!\n");
            IP6_TRC_ARG3 (pIp6Cxt->u4ContextId, IP6_MOD_TRC, BUFFER_TRC,
                          IP6_NAME,
                          "%s buffer error occurred, Type = %d Bufptr = %p",
                          ERROR_FATAL_STR, ERR_BUF_WRITE, pBuf);
            return (IP6_FAILURE);
        }

        /* Length of the packet should include Ext headers also */
        *pLen += IP6_TYPE_LEN_BYTES;
    }
    /* determine size of IPv6 and extension headers (currently only AH) */

    u4HdrSize = sizeof (tIp6Hdr);

    *pLen += u4HdrSize;

    /*
     * prepend the IPv6 and extension headers (AH only at present) to buffer
     */

    pIp6 = (tIp6Hdr *) Ip6BufPptr (pBuf, (UINT1 *) &ip6Hdr, u4HdrSize, &u1Copy);
    if (!pIp6)
    {
        IP6_TRC_ARG (pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                     BUFFER_TRC | ALL_FAILURE_TRC, IP6_NAME,
                     "IP6OUT/FWD:Ip6FormPkt: Ip6BufPptr failed!\n");
        IP6_TRC_ARG3 (pIp6Cxt->u4ContextId, IP6_MOD_TRC, BUFFER_TRC, IP6_NAME,
                      "IP6OUT/FWD:Ip6FormPkt: %s buffer error occurred, Type = %d Bufptr = %p",
                      ERROR_FATAL_STR, ERR_BUF_WRITE, pBuf);
        return (IP6_FAILURE);
    }

    Ip6FillHdr (pIp6Cxt, pIp6, pSrc, pDst,
                *pLen - sizeof (tIp6Hdr), u1Proto, u1Hlim);

    if (Ip6BufWrite (pBuf, (UINT1 *) pIp6, 0,
                     sizeof (tIp6Hdr), u1Copy) != IP6_SUCCESS)
    {
        IP6_TRC_ARG (pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                     BUFFER_TRC | ALL_FAILURE_TRC, IP6_NAME,
                     "IP6OUT/FWD: Ip6FormPkt: Ip6BufWrite failed!\n");
        IP6_TRC_ARG3 (pIp6Cxt->u4ContextId, IP6_MOD_TRC, BUFFER_TRC, IP6_NAME,
                      "IP6OUT/FWD:Ip6FormPkt:%s buffer error occurred, Type = %d Bufptr = %p",
                      ERROR_FATAL_STR, ERR_BUF_WRITE, pBuf);
        return (IP6_FAILURE);
    }
#if defined (IPSECv6_WANTED) && !defined (VPN_WANTED)
    if (Secv6OutProcess (pBuf, pIf6, pLen) == SEC_FAILURE)
    {
        IP6_TRC_ARG (pIp6Cxt->u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                     "ip6forward : Secv6OutProcess Fails\n");
        return (IP6_FAILURE);
    }
#else
    UNUSED_PARAM (pIf6);
#endif
    return (IP6_SUCCESS);

}

/******************************************************************************
 * DESCRIPTION : This routine forms the fields of the IPv6 header into the
 *               given IPv6 Header pointer.
 *
 * INPUTS      : The IPv6 Header pointer (pIp6), the source and dest
 *               addresses (pSrc & pDst), length of the higher layer
 *               data (u4Len) and the protocol (u1Proto)
 *
 * OUTPUTS     : The IPv6 Header filled in (pIp6)
 *
 * RETURNS     : None
 *
 * NOTES       :
 ******************************************************************************/

VOID
Ip6FillHdr (tIp6Cxt * pIp6Cxt, tIp6Hdr * pIp6, tIp6Addr * pSrc,
            tIp6Addr * pDst, UINT4 u4Len, UINT1 u1Proto, UINT1 u1Hlim)
{

    pIp6->u4Head = IP6_DEF_HDR_VAL;
    pIp6->u2Len = (UINT2) (CRU_HTONS ((UINT2) (u4Len)));
    pIp6->u1Nh = u1Proto;

    /* 
     * Set the hop limit to the value specified in the parameter
     * if it is non-zero, otherwise set to default hop limit value
     */
    if (u1Hlim != 0)
    {
        pIp6->u1Hlim = u1Hlim;
    }
    else
    {
        pIp6->u1Hlim = (UINT1) pIp6Cxt->u4Ipv6HopLimit;
    }

    Ip6AddrCopy (&pIp6->srcAddr, pSrc);
    Ip6AddrCopy (&pIp6->dstAddr, pDst);

}

/******************************************************************************
 * DESCRIPTION : This routine is called to send out an IPv6 datagram after
 *               the interface-specific parameters have all been filled. The
 *               routine updates parameters of the CRU Buffer header and then
 *               enqueues the buffer to the TEST task.
 *
 * INPUTS      : The IPv6 interface pointer (pIf6), length of the IPv6
 *               datagram (u4Len) and the buffer containing the datagram 
 *               (pBuf)
 *
 * OUTPUTS     : None
 *
 * RETURNS     : None
 *
 * NOTES       :
 ******************************************************************************/

VOID
Ip6Output (tIp6If * pIf6, UINT4 u4Len, tCRU_BUF_CHAIN_HEADER * pBuf)
{
    tIp6Cxt            *pIp6Cxt = NULL;
    UINT1              *pu1DestinLanAddr = NULL;

    Ip6PutModuleHdr (pIf6, pBuf);

    pIp6Cxt = pIf6->pIp6Cxt;
    IP6_TRC_ARG3 (pIp6Cxt->u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                  "IP6OUT/FWD:Ip6Output: Sending frame on IP6 IF=%d Buflen=%d "
                  "Bufptr=%p\n", pIf6->u4Index, u4Len, pBuf);

    IP6_TRC_ARG (pIp6Cxt->u4ContextId, IP6_MOD_TRC, BUFFER_TRC, IP6_NAME,
                 "IP6OUT/FWD:Ip6Output: The send buffer is :\n");
    IP6_PKT_DUMP (pIp6Cxt->u4ContextId, IP6_MOD_TRC, DUMP_TRC, IP6_NAME, pBuf,
                  0);
    pu1DestinLanAddr = (UINT1 *) Ip6GetParams (pBuf);

    IP6IF_INC_OUT_TRANSMITS (pIf6);
    IP6SYS_INC_OUT_TRANSMITS (pIp6Cxt->Ip6SysStats);

    IP6IF_INC_OUT_OCTETS (pIf6, u4Len);
    IP6SYS_INC_OUT_OCTETS (pIp6Cxt->Ip6SysStats, u4Len);

    /* passing Protocol Type and Encaps Type directly */
    if (Ip6SendToLL (pBuf, pIf6->u4Index, pu1DestinLanAddr, IP6_UCAST, 0) ==
        IP6_FAILURE)
    {
        IP6_TRC_ARG (pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                     DATA_PATH_TRC | BUFFER_TRC, IP6_NAME,
                     "IP6OUT:Ip6Output: Failure in Posting to CFA\n");
        IP6_TRC_ARG2 (pIp6Cxt->u4ContextId, IP6_MOD_TRC, ALL_FAILURE_TRC,
                      IP6_NAME,
                      "IP6OUT:Ip6Output:%s general error occurred, Type = %d ",
                      ERROR_FATAL_STR, ERR_GEN_TQUEUE_FAIL);
        Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE, IP6_CORE_SUBMODULE);

    }
}

tIp6If             *
Ip6FindRtIfInCxt (UINT4 u4ContextId, tIp6Addr * pDst)
{

    tIp6If             *pDstIf6 = NULL;
    tNd6CacheEntry     *pNd6c = NULL;
    tNetIpv6RtInfo      NetIpv6Rt;
    INT4                i4Status = 0;

    MEMSET (&NetIpv6Rt, 0, sizeof (tNetIpv6RtInfo));

    i4Status = Ip6RtLookupInCxt (u4ContextId, pDst, &NetIpv6Rt);

    if (i4Status == IP6_FAILURE)
    {
        return (NULL);
    }

    pDstIf6 = gIp6GblInfo.apIp6If[NetIpv6Rt.u4Index];

    /* Ip6ifResolveDst should not be called.
     * instead Nd6LookupCache to be called 
     */
    pNd6c = Ip6ifResolveDst (&NetIpv6Rt, pDst, &pDstIf6);

    if (Ip6SendIfCheck (pDstIf6) != IP6_SUCCESS)
    {
        IP6_TRC_ARG1 (u4ContextId, IP6_MOD_TRC, CONTROL_PLANE_TRC, IP6_NAME,
                      "IP6OUT/FWD:Ip6FindRoute: Send check on IP6 IfPtr = %p "
                      "failed\n", pDstIf6);
        return (NULL);
    }
    UNUSED_PARAM (pNd6c);
    return (pDstIf6);

}

/******************************************************************************
 * DESCRIPTION : This routine checks if the outgoing IPv6 interface is
 *               allocated and operational.
 *
 * INPUTS      : IP6 Interface pointer (pIf6)
 *
 * OUTPUTS     : None
 *
 * RETURNS     : OK or NOT_OK
 *
 * NOTES       :
 ******************************************************************************/

INT4
Ip6SendIfCheck (tIp6If * pIf6)
{

    if (!pIf6)
    {
        return (IP6_FAILURE);
    }

    if (pIf6->u1AdminStatus != ADMIN_UP)
    {
        return (IP6_FAILURE);
    }

    if (pIf6->u1OperStatus != OPER_UP)
    {
        return (IP6_FAILURE);
    }

    return (IP6_SUCCESS);

}

/******************************************************************************
 * DESCRIPTION : This routine fills the fields of the CRU buffer header before
 *               the datagram is transmitted to TEST.
 *
 * INPUTS      : The IPv6 interface pointer (pIf6), length of the IPv6
 *               datagram (u4Len) and the buffer containing the datagram 
 *               (pBuf)
 *
 * OUTPUTS     : None
 *
 * RETURNS     : None
 *
 * NOTES       :
 ******************************************************************************/

PRIVATE VOID
Ip6PutModuleHdr (tIp6If * pIf6, tCRU_BUF_CHAIN_HEADER * pBuf)
{

    CRU_BUF_Set_Interface_Type (CRU_BUF_Get_InterfaceId (pBuf), pIf6->u1IfType);
    CRU_BUF_Set_Interface_Num (CRU_BUF_Get_InterfaceId (pBuf),
                               (UINT1) pIf6->u4Index);
    CRU_BUF_Set_Interface_SubRef (CRU_BUF_Get_InterfaceId (pBuf),
                                  pIf6->u2CktIndex);
    CRU_BUF_Set_SourceModuleId (pBuf, IP6_MODULE);

    CRU_BUF_Set_DestinModuleId (pBuf, CRU_COMMON_FORWARD_MODULE);
}

/******************************************************************************
 * DESCRIPTION : This routines finds the route for a specific destination.
 *               If route is not found,it looks up in the default route.
 *
 * INPUTS      : Received interface,destination to send
 *
 * OUTPUTS     : ND Entry, Route Entry.
 *
 * RETURNS     : Pointer to Interface
 *
 * NOTES       :
 ******************************************************************************/
tIp6If             *
Ip6GetRouteInCxt (UINT4 u4ContextId, tIp6Addr * pDst, tNd6CacheEntry ** ppNd6c,
                  tNetIpv6RtInfo * pNetIp6RtInfo)
{
    tIp6If             *pDstIf6 = NULL;
    UINT1               u1Found = 0;
    INT4                i4RetValue = RTM6_FAILURE;

    IP6_TASK_UNLOCK ();
    /* Assumption: RTM6 lock will be taken inside RTM6 API */
    i4RetValue = Rtm6ApiRoute6LookupInCxt (u4ContextId,
                                           pDst, IP6_ADDR_MAX_PREFIX,
                                           IP6_ROUTE_PREFERENCE_BEST_METRIC,
                                           &u1Found, pNetIp6RtInfo);

    IP6_TASK_LOCK ();
    if (i4RetValue == RTM6_SUCCESS)
    {
        pDstIf6 = gIp6GblInfo.apIp6If[pNetIp6RtInfo->u4Index];

        if (Ip6SendIfCheck (pDstIf6) != IP6_SUCCESS)
        {
            IP6_TRC_ARG1 (u4ContextId, IP6_MOD_TRC, CONTROL_PLANE_TRC, IP6_NAME,
                          "IP6OUT/FWD:Ip6FindDefRoute: Send check on IP6 "
                          "IfPtr = %p failed\n", pDstIf6);
            i4RetValue = RTM6_FAILURE;
        }
    }

    if (i4RetValue == RTM6_FAILURE)
    {
        IP6_TASK_UNLOCK ();
        /* Assumption: RTM6 lock will be taken inside RTM6 API */
        i4RetValue = Rtm6ApiDefRtLookupInCxt (u4ContextId, pNetIp6RtInfo);
        IP6_TASK_LOCK ();

        if (i4RetValue == RTM6_FAILURE)
        {
            return NULL;
        }
        pDstIf6 = gIp6GblInfo.apIp6If[pNetIp6RtInfo->u4Index];

        if (Ip6SendIfCheck (pDstIf6) != IP6_SUCCESS)
        {
            IP6_TRC_ARG1 (u4ContextId, IP6_MOD_TRC, CONTROL_PLANE_TRC, IP6_NAME,
                          "IP6OUT/FWD:Ip6FindDefRoute: Send check on IP6 "
                          "IfPtr = %p failed\n", pDstIf6);
            return NULL;
        }
    }

    *ppNd6c = Ip6ifResolveDst (pNetIp6RtInfo, pDst, &pDstIf6);
    return pDstIf6;
}

/***************************** END OF FILE **********************************/
