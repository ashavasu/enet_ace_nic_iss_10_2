/********************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: nd6hastb.c,v 1.3 2014/07/10 12:35:51 siva Exp $
 *
 * Description: This file contains ND Redundancy related routines
 *           
 *******************************************************************/
#ifndef __ND6HASTB_C
#define __ND6HASTB_C

#include "ip6inc.h"
#include "nd6red.h"

/************************************************************************/
/*  Function Name   : Nd6RedInitGlobalInfo                              */
/*                                                                      */
/*  Description     : This function is invoked by the IPV6 module to    */
/*                    register itself with the RM module. This          */
/*                    registration is required to send/receive peer     */
/*                    synchronization messages and control events       */
/*                                                                      */
/*  Input(s)        : None.                                             */
/*                                                                      */
/*  Output(s)       : None.                                             */
/*                                                                      */
/*  Returns         : OSIX_SUCCESS / OSIX_FAILURE                       */
/************************************************************************/

PUBLIC INT4
Nd6RedInitGlobalInfo (VOID)
{
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : Nd6RedRmRegisterProtocols                         */
/*                                                                      */
/*  Description     : This function is invoked by the IPV6 module to    */
/*                    register itself with the RM module. This          */
/*                    registration is required to send/receive peer     */
/*                    synchronization messages and control events       */
/*                                                                      */
/*  Input(s)        : pRmReg -- Pointer to structure tRmRegParams       */
/*                                                                      */
/*  Output(s)       : None.                                             */
/*                                                                      */
/*  Returns         : OSIX_SUCCESS / OSIX_FAILURE                       */
/************************************************************************/

PUBLIC INT4
Nd6RedRmRegisterProtocols (tRmRegParams * pRmReg)
{
    UNUSED_PARAM (pRmReg);
    return OSIX_SUCCESS;
}

/************************************************************************/
/* Function Name      : Nd6RedDeInitGlobalInfo                          */
/*                                                                      */
/* Description        : This function is invoked by the IPV6 module     */
/*                      during module shutdown and this function        */
/*                      deinitializes the redundancy global variables   */
/*                      and de-register IPV6 with RM.                   */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/************************************************************************/

INT4
Nd6RedDeInitGlobalInfo (VOID)
{
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : Nd6RmDeRegisterProtocols                          */
/*                                                                      */
/*  Description     : This function is invoked by the IPV6 module to    */
/*                    de-register itself with the RM module.            */
/*                                                                      */
/*  Input(s)        : None.                                             */
/*                                                                      */
/*  Output(s)       : None.                                             */
/*                                                                      */
/*  Returns         : OSIX_SUCCESS / OSIX_FAILURE                       */
/************************************************************************/

PUBLIC INT4
Nd6RedRmDeRegisterProtocols (VOID)
{
    return OSIX_SUCCESS;
}

/************************************************************************/
/* Function Name      : Nd6RedRmCallBack                                */
/*                                                                      */
/* Description        : This function will be invoked by the IPV6 module*/
/*                      to enque events and post messages to ND         */
/*                                                                      */
/* Input(s)           : u1Event   - Event type given by RM module       */
/*                      pData     - RM Message to enqueue               */
/*                      u2DataLen - Message size                        */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/************************************************************************/

PUBLIC INT4
Nd6RedRmCallBack (UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen)
{
    UNUSED_PARAM (u1Event);
    UNUSED_PARAM (pData);
    UNUSED_PARAM (u2DataLen);
    return OSIX_SUCCESS;
}

/************************************************************************/
/* Function Name      : Nd6RedHandleRmEvents                            */
/*                                                                      */
/* Description        : This function is invoked by the IPV6 module to  */
/*                      process all the events and messages posted by   */
/*                      the RM module                                   */
/*                                                                      */
/* Input(s)           : pMsg -- Pointer to the ND Q Msg                 */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
Nd6RedHandleRmEvents ()
{
    return;
}

/************************************************************************/
/* Function Name      : Nd6RedHandleGoActive                            */
/*                                                                      */
/* Description        : This function is invoked by the IPV6 module upon*/
/*                      receiving the GO_ACTIVE indication from RM      */
/*                      module. And this function responds to RM with an*/
/*                      acknowledgement.                                */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
Nd6RedHandleGoActive (VOID)
{
    return;
}

/************************************************************************/
/* Function Name      : Nd6RedHandleGoStandby                           */
/*                                                                      */
/* Description        : This function is invoked by the IPV6 module upon*/
/*                      receiving the GO_STANDBY indication from RM     */
/*                      module. And this function responds to RM module */
/*                      with an acknowledgement.                        */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
Nd6RedHandleGoStandby (VOID)
{
    return;
}

/************************************************************************/
/* Function Name      : Nd6RedHandleIdleToActive                        */
/*                                                                      */
/* Description        : This function updates the node status to ACTIVE */
/*                      on transition from Idle to Active and also      */
/*                      updates the number of Standby node count.       */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
Nd6RedHandleIdleToActive (VOID)
{
    return;
}

/************************************************************************/
/* Function Name      : Nd6RedHandleIdleToStandby                       */
/*                                                                      */
/* Description        : This function updates the node status to STANDBY*/
/*                      on transition from Idle to Standby and also     */
/*                      updates the number of Standby node count to 0   */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
Nd6RedHandleIdleToStandby (VOID)
{
    return;
}

/************************************************************************/
/* Function Name      : Nd6StartTimers                                  */
/*                                                                      */
/* Description        : This is a recursive function which is registered*/
/*                      to RBTreeWalk. The RBTree is walked and the     */
/*                      elements in the RBtree is checked for either    */
/*                      static or dynamic entry. If static entry, then  */
/*                      the timer is not started. If the entry is       */
/*                      dynamic, then the cache timer is started.       */
/*                                                                      */
/* Input(s)           : pRBElem - RBTree to walk.                       */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : RB_WALK_CONT/RB_WALK_BREAK                      */
/************************************************************************/

INT4
Nd6StartTimers (tRBElem * pRBElem, eRBVisit visit, UINT4 u4Level,
                void *pArg, void *pOut)
{
    UNUSED_PARAM (pRBElem);
    UNUSED_PARAM (visit);
    UNUSED_PARAM (u4Level);
    UNUSED_PARAM (pArg);
    UNUSED_PARAM (pOut);
    return RB_WALK_CONT;
}

/************************************************************************/
/* Function Name      : Nd6RedHandleStandbyToActive                     */
/*                                                                      */
/* Description        : This function handles the Standby node to Active*/
/*                      node transistion. The following action are      */
/*                      performed during this transistion.              */
/*                      1.Update the Node Status and Standby node count.*/
/*                      2.Start the timers and enable the module.       */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
Nd6RedHandleStandbyToActive (VOID)
{
    return;
}

/************************************************************************/
/* Function Name      : Nd6RedHandleActiveToStandby                     */
/*                                                                      */
/* Description        : This function handles the Active node to Standby*/
/*                      node transistion. The following action are      */
/*                      performed during this transistion               */
/*                      1.Update the Node Status and Standby node count */
/*                      2.Disable and Enable the module                 */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
Nd6RedHandleActiveToStandby (VOID)
{
    return;
}

/************************************************************************/
/* Function Name      : Nd6RedProcessPeerMsgAtActive                    */
/*                                                                      */
/* Description        : This routine handles messages from the other    */
/*                      node (Standby) at Active. The messages that are */
/*                      handled in Active node are                      */
/*                      1. RM_BULK_UPDT_REQ_MSG                         */
/*                                                                      */
/* Input(s)           : pMsg - RM Data buffer holding message           */
/*                      u2DataLen - Length of data in buffer.           */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
Nd6RedProcessPeerMsgAtActive (tRmMsg * pMsg, UINT2 u2DataLen)
{
    UNUSED_PARAM (pMsg);
    UNUSED_PARAM (u2DataLen);
    return;
}

/************************************************************************/
/* Function Name      : Nd6RedProcessPeerMsgAtStandby                   */
/*                                                                      */
/* Description        : This routine handles messages from the other    */
/*                      node (Active) at standby. The messages that are */
/*                      handled in Standby node are                     */
/*                      1. RM_BULK_UPDT_TAIL_MSG                        */
/*                      2. Dynamic sync-up messages                     */
/*                      3. Dynamic bulk messages                        */
/*                                                                      */
/* Input(s)           : pMsg - RM Data buffer holding message           */
/*                      u2DataLen - Length of data in buffer.           */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
Nd6RedProcessPeerMsgAtStandby (tRmMsg * pMsg, UINT2 u2DataLen)
{
    UNUSED_PARAM (pMsg);
    UNUSED_PARAM (u2DataLen);
    return;
}

/************************************************************************/
/* Function Name      : Nd6RedRmReleaseMemoryForMsg                     */
/*                                                                      */
/* Description        : This function is invoked by the IPV6 module to  */
/*                      release the allocated memory by calling the RM  */
/*                      module.                                         */
/*                                                                      */
/* Input(s)           : pu1Block -- Memory Block                        */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/************************************************************************/

PUBLIC INT4
Nd6RedRmReleaseMemoryForMsg (UINT1 *pu1Block)
{
    UNUSED_PARAM (pu1Block);
    return OSIX_SUCCESS;
}

/************************************************************************/
/* Function Name      : Nd6RedRmReleaseMemoryForMsg                     */
/*                                                                      */
/* Description        : This routine enqueues the Message to RM. If the */
/*                      sending fails, it frees the memory.             */
/*                                                                      */
/* Input(s)           : pMsg - RM Message Data Buffer                   */
/*                      u2Length - Length of the message                */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/************************************************************************/

PUBLIC INT4
Nd6RedSendMsgToRm (tRmMsg * pMsg, UINT2 u2Length)
{
    UNUSED_PARAM (pMsg);
    UNUSED_PARAM (u2Length);
    return OSIX_SUCCESS;
}

/************************************************************************/
/* Function Name      : Nd6RedSendBulkReqMsg                          */
/*                                                                      */
/* Description        : This function sends the bulk request message    */
/*                      from standby node to Active node to initiate the*/
/*                      bulk update process.                            */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
Nd6RedSendBulkReqMsg (VOID)
{
    return;
}

/************************************************************************/
/* Function Name      : Nd6RedSendBulkUpdMsg                        */
/*                                                                      */
/* Description        : This function sends the binding table dynamic   */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
Nd6RedSendBulkUpdMsg (VOID)
{
    return;

}

/************************************************************************/
/* Function Name      : Nd6RedSendBulkCacheInfo                         */
/*                                                                      */
/* Description        : This function sends the Bulk update messages to */
/*                      the peer standby ND.                            */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : u1BulkUpdPendFlg                                */
/*                                                                      */
/* Returns            : IP6_SUCCESS/IP6_FAILURE                         */
/************************************************************************/

PUBLIC INT4
Nd6RedSendBulkCacheInfo (UINT1 *pu1BulkUpdPendFlg)
{
    UNUSED_PARAM (pu1BulkUpdPendFlg);
    return IP6_SUCCESS;
}

/************************************************************************/
/* Function Name      : Nd6RedSendBulkUpdTailMsg                        */
/*                                                                      */
/* Description        : This function sends the binding table dynamic   */
/*                      sync up message to Standby node from the Active */
/*                      node, and dynamically updates the               */
/*                      binding table entries.                          */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/************************************************************************/

VOID
Nd6RedSendBulkUpdTailMsg (VOID)
{
    return;
}

/************************************************************************/
/* Function Name      : Nd6RedProcessBulkTailMsg                        */
/*                                                                      */
/* Description        : This function process the bulk update tail      */
/*                      message and send bulk updates                   */
/*                                                                      */
/* Input(s)           : pMsg - RM Data buffer holding messges           */
/*                      u2DataLen - Length of data in buffer            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
Nd6RedProcessBulkTailMsg (tRmMsg * pMsg, UINT2 *pu2OffSet)
{
    UNUSED_PARAM (pMsg);
    UNUSED_PARAM (pu2OffSet);
    return;
}

/************************************************************************
 * Function Name      : Nd6RedSendDynamicCacheInfo 
 *                                                
 * Description        : This function sends the binding table dynamic  
 *                      sync up message to Standby node from the Active 
 *                      node, and dynamically update the   
 *                      binding table entries.                        
 *                                                                   
 * Input(s)           : None
 *                                                                    
 * Output(s)          : None                                         
 *                                                                  
 * Returns            : OSIX_SUCCESS / OSIX_FAILURE                
 ************************************************************************/

PUBLIC VOID
Nd6RedSendDynamicCacheInfo ()
{
    return;
}

/************************************************************************/
/* Function Name      : Nd6RedProcessBulkInfo                           */
/*                                                                      */
/* Description        : This function decodes the sync-up message from  */
/*                      Active node and updates the binding information */
/*                      at the Standby node.                            */
/*                                                                      */
/* Input(s)           : pMsg - RM Sync-up message                       */
/*                      u2RemMsgLen - Length of value of Message        */
/*                                                                      */
/* Output(s)          : u2OffSet - OffSet value                         */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

VOID
Nd6RedProcessBulkInfo (tRmMsg * pMsg, UINT2 *pu2OffSet)
{

    UNUSED_PARAM (pMsg);
    UNUSED_PARAM (pu2OffSet);
    return;
}

/************************************************************************/
/* Function Name      : Nd6RedProcessDynamicInfo                        */
/*                                                                      */
/* Description        : This function decodes the sync-up message from  */
/*                      Active node and updates the binding information */
/*                      at the Standby node.                            */
/*                                                                      */
/* Input(s)           : pMsg - RM Sync-up message                       */
/*                      u2RemMsgLen - Length of value of Message        */
/*                                                                      */
/* Output(s)          : u2OffSet - OffSet value                         */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

VOID
Nd6RedProcessDynamicInfo (tRmMsg * pMsg, UINT2 *pu2OffSet)
{
    UNUSED_PARAM (pMsg);
    UNUSED_PARAM (pu2OffSet);
    return;
}

/************************************************************************/
/* Function Name      : Nd6RedAddDynamicInfo                            */
/*                                                                      */
/* Description        : This function adds the dynamic entries to the   */
/*                      global tree and this tree is scanned everytime  */
/*                      for sending the dynamic entry.                  */
/*                                                                      */
/* Input(s)           : pNd6Cache - ND Cache message                    */
/*                      u1Operation - add or delete entry               */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/
VOID
Nd6RedAddDynamicInfo (tNd6CacheEntry * pNd6Cache, UINT1 *llAddr, UINT1 u1Action)
{
    UNUSED_PARAM (llAddr);
    UNUSED_PARAM (pNd6Cache);
    UNUSED_PARAM (u1Action);
    return;
}

/************************************************************************/
/* Function Name      : Nd6RedHwAudit                                   */
/*                                                                      */
/* Description        : This function does the hardware audit in two    */
/*                      approches.                                      */
/*                                                                      */
/*                      First:                                          */
/*                      When there is a transaction between standby and */
/*                      active node, the Nd6RedTable is walked, if      */
/*                      there  are any entries in the table, they are   */
/*                      verified with the hardware, if the entry is     */
/*                      present in the hardware, then the entry is      */
/*                      added to the sofware. If not, the entry is      */
/*                      deleted.                                        */
/*                                                                      */
/*                      Second:                                         */
/*                      When there is a transaction between standby and */
/*                      active node, the entries in the hardware are    */
/*                      retrived and checked with the software. If      */
/*                      is a mismatch in the entry, the entry in the    */
/*                      hardware is updated to the software.            */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/
VOID
Nd6RedHwAudit ()
{
    return;
}

/*-------------------------------------------------------------------+
 * Function           : Nd6RBTreeRedEntryCmp 
 *
 * Input(s)           : pRBElem, pRBElemIn
 *
 * Output(s)          : None.
 *
 * Returns            : IP6_RB_LESSER  if pRBElem <  pRBElemIn
 *                      IP6_RB_GREATER if pRBElem >  pRBElemIn
 *                      IP6_RB_EQUAL   if pRBElem == pRBElemIn
 * Action :
 * This procedure compares the two Redundancy cache entries in lexicographic
 * order of the indices of the Redundancy cache entry.
+-------------------------------------------------------------------*/

INT4
Nd6RBTreeRedEntryCmp (tRBElem * pRBElem, tRBElem * pRBElemIn)
{
    UNUSED_PARAM (pRBElem);
    UNUSED_PARAM (pRBElemIn);

    return IP6_RB_EQUAL;
}
#endif
