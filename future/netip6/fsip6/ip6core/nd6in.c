/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * 
 * $Id: nd6in.c,v 1.26 2015/07/17 09:53:06 siva Exp $
 *
 ********************************************************************/
/*
 * +--------------------------------------------------------------------------+
 * |                                                                          |
 * |   FILE  NAME             :  nd6in.c                                      |
 * |                                                                          |
 * |   PRINCIPAL AUTHOR       :  N. Senthil Vadivu                            |
 * |                                                                          |
 * |   SUBSYSTEM NAME         :  IPv6                                         |
 * |                                                                          |
 * |   MODULE NAME            :  ND6 Module                                   |
 * |                                                                          |
 * |   LANGUAGE               :  C                                            |
 * |                                                                          |
 * |   TARGET ENVIRONMENT     :  UNIX                                         |
 * |                                                                          |
 * |   DATE OF FIRST RELEASE  :                                               |
 * |                                                                          |
 * |   DESCRIPTION            :  This file contains the ND routines handling  |
 * |                             Reception and Processing of received         |
 * |                             ND messages                                  |
 * |                                                                          |
 * +--------------------------------------------------------------------------+
 *
 *
 *     CHANGE RECORD :
 *
 * +--------------------------------------------------------------------------+
 * |   VERSION               AUTHOR/                      DESCRIPTION OF      |
 * |                         DATE                            CHANGE           |
 * |                                                                          |
 * |    1.0                  Senthil/                     Created file        |
 * |                                                                          |
 * +--------------------------------------------------------------------------+
 */

#include "ip6inc.h"

/*****************************************************************************
DESCRIPTION         Decodes the type of ND messages received 
                    on a port and hands it over to the 
                    respective functions for further processing

INPUTS              pIf6        -   Pointer to the interface 
                    pIp6        -   Pointer to the IPv6 header of
                                     the received packet
                    u1Nd6Type  -   Type of ND message
                    u2Len       -   Length of the ND payload in the
                                     received IPv6 packet
                    u1AddrFlag -   Flag indicating the address status
                    pBuf        -   Pointer to the received
                                     IPv6 packet

OUTPUTS             None

RETURNS             None
****************************************************************************/

VOID
Nd6Rcv (tIp6If * pIf6, tIp6Hdr * pIp6, UINT1 u1Nd6Type, UINT2 u2Len,
        UINT1 u1AddrFlag, tCRU_BUF_CHAIN_HEADER * pBuf)
{
    INT4                i4RelFlag = IP6_FAILURE;
    UINT4               u4ContextId = pIf6->pIp6Cxt->u4ContextId;

    IP6_TRC_ARG6 (u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC, ND6_NAME,
                  "ND6:Nd6Rcv: Rcvd pkt IP6IF=%d Len=%d Type=%d Bufptr=%p "
                  "SrcAddr=%s DstAddr=%s\n",
                  pIf6->u4Index, u2Len, u1Nd6Type, pBuf,
                  Ip6PrintAddr (&pIp6->srcAddr), Ip6PrintAddr (&pIp6->dstAddr));

    IP6_TRC_ARG (u4ContextId, ND6_MOD_TRC, BUFFER_TRC, ND6_NAME,
                 "ND6:Nd6Rcv:The Receive buffer is :\n");
    IP6_PKT_DUMP (u4ContextId, ND6_MOD_TRC, DUMP_TRC, ND6_NAME, pBuf, 0);

    switch (u1Nd6Type)
    {
        case ND6_NEIGHBOR_SOLICITATION:
            ICMP6_INC_IN_NSOLS (pIf6->pIp6Cxt->Icmp6Stats);
            ICMP6_INTF_INC_IN_NSOLS (pIf6);
            (IP6_IF_STATS (pIf6))->u4InNsols++;

            if (Nd6RcvNeighSol (pIf6, pIp6, u2Len, pBuf) == IP6_FAILURE)
            {
                i4RelFlag = IP6_SUCCESS;
            }
            break;

        case ND6_NEIGHBOR_ADVERTISEMENT:
            ICMP6_INC_IN_NADVS (pIf6->pIp6Cxt->Icmp6Stats);
            ICMP6_INTF_INC_IN_NADVS (pIf6);
            (IP6_IF_STATS (pIf6))->u4InNadvs++;
            if (Nd6RcvNeighAdv (pIf6, pIp6, u2Len, pBuf) == IP6_FAILURE)
            {
                i4RelFlag = IP6_SUCCESS;
            }
            break;

        case ND6_ROUTER_SOLICITATION:
            /*
             * RA messages received on tentative addresses
             * MUST not be processed
             */
            ICMP6_INC_IN_RSOLS (pIf6->pIp6Cxt->Icmp6Stats);
            ICMP6_INTF_INC_IN_RSOLS (pIf6);
            (IP6_IF_STATS (pIf6))->u4InRoutsols++;
            if ((pIf6->u1Ipv6IfFwdOperStatus == IP6_IF_FORW_ENABLE)
                && (!(u1AddrFlag & ADDR6_TENTATIVE)))
            {
                if (Nd6RcvRoutSol (pIf6, pIp6, u2Len, pBuf) == IP6_FAILURE)
                {
                    i4RelFlag = IP6_SUCCESS;
                }
            }
            else
                i4RelFlag = IP6_SUCCESS;
            break;

        case ND6_ROUTER_ADVERTISEMENT:
            ICMP6_INC_IN_RADVS (pIf6->pIp6Cxt->Icmp6Stats);
            ICMP6_INTF_INC_IN_RADVS (pIf6);
            (IP6_IF_STATS (pIf6))->u4InRoutadvs++;
            IP6_TRC_ARG (u4ContextId, ND6_MOD_TRC, MGMT_TRC, ND6_NAME,
                         "ND6:Nd6Rcv: Reception of RA - No processing\n");
            if (Nd6RcvRoutAdv (pIp6, pIf6, u2Len, pBuf) == IP6_FAILURE)
            {
                i4RelFlag = IP6_SUCCESS;
            }
            break;

        case ROUTE_REDIRECT:
            ICMP6_INC_IN_REDIRS (pIf6->pIp6Cxt->Icmp6Stats);
            ICMP6_INTF_INC_IN_REDIRS (pIf6);
            (IP6_IF_STATS (pIf6))->u4InRoutRedirs++;
            IP6_TRC_ARG (u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC, ND6_NAME,
                         "ND6:Nd6Rcv: Reception of Router redirect - "
                         "processing\n");
            if (pIf6->u1Ipv6IfFwdOperStatus == IP6_IF_FORW_ENABLE)
            {
                i4RelFlag = IP6_SUCCESS;
            }
            else
            {
                if (pIf6->Icmp6ErrRLInfo.i4Icmp6RedirectMsg ==
                    ICMP6_REDIRECT_ENABLE)
                {
                    if (Nd6RcvRouterRedirect (pIf6, pIp6, u2Len, pBuf) ==
                        IP6_FAILURE)
                    {
                        i4RelFlag = IP6_SUCCESS;
                    }
                }
                else
                {
                    i4RelFlag = IP6_SUCCESS;
                }
            }
            break;

        case ND6_CERT_PATH_SOLICITATION:
            if (IP6_TRUE == IF_SEND_ENABLED (pIf6))
            {

                if (Nd6RcvCertPathSol (pIf6, pIp6, pBuf, u2Len) == IP6_FAILURE)
                {
                    i4RelFlag = IP6_SUCCESS;
                }
            }
            break;

        case ND6_CERT_PATH_ADVERTISEMENT:
			if (IP6_TRUE == IF_SEND_ENABLED (pIf6))
			{
                if (Nd6RcvCertPathAdv (pIf6, pIp6, pBuf, u2Len) == IP6_FAILURE)
                {
                    i4RelFlag = IP6_SUCCESS;
                }
			}
            break;

        default:
            i4RelFlag = IP6_SUCCESS;
            break;
    }

    /* Release the received buffer upon the flag being set */
    if (i4RelFlag == IP6_SUCCESS)
    {
        if (Ip6BufRelease (u4ContextId, pBuf, FALSE, ND6_MODULE) != IP6_SUCCESS)
        {
            IP6_TRC_ARG (u4ContextId, ND6_MOD_TRC, BUFFER_TRC, ND6_NAME,
                         "ND6:Nd6Rcv: Rcv Msg - BufRelease Failed\n");
            IP6_TRC_ARG3 (u4ContextId, ND6_MOD_TRC, BUFFER_TRC, IP6_NAME,
                          "ND6:Nd6Rcv: %s buffer error occurred, Type = %d "
                          "Module = 0x%X Bufptr = %p ",
                          ERROR_FATAL_STR, ERR_BUF_RELEASE, pBuf);

        }
        ICMP6_INC_IN_ERRS (pIf6->pIp6Cxt->Icmp6Stats);
        ICMP6_INTF_INC_IN_ERRS (pIf6);
    }

}

/*****************************************************************************
DESCRIPTION         Processes the received Neighbor Solicitation
                    Messages, updates the corresponding NDCache
                    entry and sends the response Solicited
                    Neighbor Advertisements
                    
INPUTS              pIf6        -   Pointer to the interface 
                    pIp6        -   Pointer to the IPv6 header of
                                     the received packet
                    u2Len       -   Length of the ND payload in the
                                     received IPv6 packet
                    pBuf        -   Pointer to the received
                                     IPv6 packet

OUTPUTS             None

RETURNS             IP6_SUCCESS      - Sending of NA succeeds
                    IP6_FAILURE  - Processing of NS fails 
****************************************************************************/

INT4
Nd6RcvNeighSol (tIp6If * pIf6, tIp6Hdr * pIp6, UINT2 u2Len,
                tCRU_BUF_CHAIN_HEADER * pBuf)
{
    UINT1               u1Copy = 0, u1_addr_status = 0;
    UINT1               u1TgtAddrType = 0;
    UINT2               u2ExtnsLen = 0;
    UINT4               u4Nd6Offset = 0;
    tNd6NeighSol        nd6Nsol, *pNd6Nsol = NULL;
    tNd6ExtHdr         *pNd6Ext = NULL;
    UINT4               u4ContextId = pIf6->pIp6Cxt->u4ContextId;
    INT4                i4VrId = 0, i4RetVal = 0;
    tNd6CacheEntry  *pNd6CacheEntry = NULL;
#ifdef VRRP_WANTED
    tIPvXAddr           IpAddr;
#endif
#ifdef EVPN_VXLAN_WANTED
    tNd6CacheEntry      Nd6cEntry;
    tIp6Addr            Ip6TgtAddr;
    UINT4               u4Offset = 0;

    MEMSET (&Nd6cEntry, 0, sizeof (tNd6CacheEntry));
    MEMSET (&Ip6TgtAddr, 0, sizeof (tIp6Addr));
#endif

#ifdef VRRP_WANTED
    IPVX_ADDR_CLEAR (&IpAddr);
#endif

    /* Extract NS Message from the buffer */
    u4Nd6Offset = IP6_BUF_READ_OFFSET (pBuf) - sizeof (tIcmp6PktHdr);
    IP6_BUF_READ_OFFSET (pBuf) = u4Nd6Offset;

    u2ExtnsLen = (UINT2) (u2Len - sizeof (tNd6NeighSol));

    /*
     * If the source IP6 address is unspecified address &
     * the destination address is solicited-node mcast
     * address, discard the pkt.
     */
    if (IS_ADDR_UNSPECIFIED (pIp6->srcAddr))
    {
        if (!(IS_ADDR_SOLICITED (pIp6->dstAddr)))
        {
            ICMP6_INC_IN_ERRS (pIf6->pIp6Cxt->Icmp6Stats);
            ICMP6_INTF_INC_IN_ERRS (pIf6);
            return (IP6_FAILURE);
        }
        IP6_BUF_READ_OFFSET (pBuf) += sizeof (tNd6NeighSol);
        if ((pNd6Ext = Nd6ExtractExtnHdrInCxt (pIf6->pIp6Cxt, pBuf)) != NULL)
        {
            if ((IS_ADDR_UNSPECIFIED (pIp6->srcAddr)) &&
                (pNd6Ext->u1Type == ND6_SRC_LLA_EXT))
            {
                ICMP6_INC_IN_ERRS (pIf6->pIp6Cxt->Icmp6Stats);
                ICMP6_INTF_INC_IN_ERRS (pIf6);
                return (IP6_FAILURE);
            }
            IP6_BUF_READ_OFFSET (pBuf) = u4Nd6Offset;
        }
    }

    if ((pNd6Nsol = (tNd6NeighSol *) Ip6BufRead
         (pBuf, (UINT1 *) &nd6Nsol, u4Nd6Offset,
          sizeof (tNd6NeighSol), u1Copy)) == NULL)
    {
        IP6_TRC_ARG2 (u4ContextId, ND6_MOD_TRC, BUFFER_TRC, ND6_NAME,
                      "ND6:Nd6RcvNeighSol: Receive NS - BufRead Failed! "
                      "BufPtr %p Offset %d\n", pBuf, u4Nd6Offset);
        IP6_TRC_ARG3 (u4ContextId, ND6_MOD_TRC, BUFFER_TRC, IP6_NAME,
                      "ND6:Nd6RcvNeighSol: %s buffer error occurred, "
                      "Type = %d Module=0x%X Bufptr= %p",
                      ERROR_FATAL_STR, ERR_BUF_READ, pBuf);
        return (IP6_FAILURE);
    }

    IP6_TRC_ARG1 (u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC, ND6_NAME,
                  "ND6:Nd6RcvNeighSol: Reception of NS - Target Address = %s\n",
                  Ip6PrintAddr (&pNd6Nsol->targAddr6));

    /* Validate the NS message */
    if (Nd6ValidateNeighSolInCxt (pIf6->pIp6Cxt, pIp6, pNd6Nsol, u2Len)
        == IP6_FAILURE)
    {
        /* Increment the count on ICMP bad packets received */
        ICMP6_INC_IN_ERRS (pIf6->pIp6Cxt->Icmp6Stats);
        ICMP6_INTF_INC_IN_ERRS (pIf6);
        return (IP6_FAILURE);
    }

    /* Process the Extension Options */
    if (u2ExtnsLen > 0)
    {
        if (Nd6ProcessNeighSolExtns (pIf6, pIp6, u2ExtnsLen, pBuf) ==
            IP6_FAILURE)
        {
            return (IP6_FAILURE);
        }
    }
    /* Check whether the target address is an address on the interface */
    u1_addr_status = (UINT1)Ip6AddrType (&pNd6Nsol->targAddr6);

#ifdef VRRP_WANTED
    IPVX_ADDR_INIT_FROMV6 (IpAddr, &pNd6Nsol->targAddr6);

    if (VrrpGetVridFromAssocIpAddress (pIf6->u4Index, IpAddr, &i4VrId)
        == VRRP_NOT_OK)
    {
        /* Ignore failure */
    }
#endif

#ifdef EVPN_VXLAN_WANTED
    u4Offset = IP6_BUF_READ_OFFSET (pBuf); 
    IP6_BUF_READ_OFFSET (pBuf) = IPV6_HEADER_LEN;
    Nd6GetNSTargetAddr (pIf6, pIp6, pBuf, &Ip6TgtAddr);
    IP6_BUF_READ_OFFSET (pBuf) = u4Offset; 
#endif

    if (((i4VrId == 0) &&
        (Ip6IsPktToMeInCxt (pIf6->pIp6Cxt, &u1_addr_status, 1,
                            &pNd6Nsol->targAddr6, pIf6) == IP6_FAILURE))
#ifdef EVPN_VXLAN_WANTED
        && (Nd6IsCacheForAddrLearntFromEvpn (&Ip6TgtAddr, &Nd6cEntry)
           == IP6_FAILURE)
#endif
        )
    {
        if (Nd6ProxyCheckAddrInCxt (pIf6->pIp6Cxt, pNd6Nsol->targAddr6) != NULL)
        {
            u1_addr_status = ADDR6_UP;
        }
        else if (Nd6IsNSProxyAllowed (pIf6, pIp6) == IP6_SUCCESS)
        {
            /* Proxy the NS message */
            if ((i4RetVal =
                 Nd6ProxyNeighSol (pIf6, pIp6, pNd6Nsol,
                                   pNd6Ext)) == IP6_FAILURE)
            {
                IP6_TRC_ARG (u4ContextId, ND6_MOD_TRC,
                             MGMT_TRC | ALL_FAILURE_TRC, ND6_NAME,
                             "ND6:Nd6RcvNeighSol: Proxying NS Failed\n");
            }
            if (Ip6BufRelease (u4ContextId, pBuf, FALSE, ND6_MODULE) !=
                IP6_SUCCESS)
            {
                IP6_TRC_ARG (u4ContextId, ND6_MOD_TRC, BUFFER_TRC, ND6_NAME,
                             "ND6:Nd6RcvNeighSol: Rcv NS - BufRelease Failed\n");
                IP6_TRC_ARG3 (u4ContextId, ND6_MOD_TRC, BUFFER_TRC, IP6_NAME,
                              "ND6:Nd6RcvNeighSol: %s buffer error occurred,Type =%d Module =0x%X Bufptr =%p",
                              ERROR_FATAL_STR, ERR_BUF_RELEASE, pBuf);
            }
            return i4RetVal;

            /* Donot send NA Response */
        }
        else
        {
            return (IP6_FAILURE);
        }
    }

    /* 
     * RM#21946 - ISS should not respond to a NS message with a target address 
     * which is not assigned to the receiving interface.
     */

    if (Ip6IsPktToMeInCxt (pIf6->pIp6Cxt, &u1_addr_status, IP6_ONE,
                            &pNd6Nsol->targAddr6, pIf6) != IP6_FAILURE)
    {
        u1TgtAddrType = (UINT1) Ip6AddrType (&pNd6Nsol->targAddr6);
        if (Ip6IsAddrOnInterface(&u1TgtAddrType, IP6_ONE, &pNd6Nsol->targAddr6, pIf6) 
                                 == IP6_FAILURE)
        {
            IP6_TRC_ARG1 (u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC, ND6_NAME,
                          "ND6:Nd6RcvNeighSol: Reception of NS - Target Address = %s which is not assigned on the interface\n",
                          Ip6PrintAddr (&pNd6Nsol->targAddr6));
            return (IP6_FAILURE);
        }
    }  

#ifdef EVPN_VXLAN_WANTED
    if (Nd6IsCacheForAddrLearntFromEvpn (&Ip6TgtAddr, &Nd6cEntry)
           == IP6_SUCCESS)
    {
        u1_addr_status = ADDR6_UP;
    }
#endif
    if (i4VrId != 0)
    {
        /* If VRID is present for the IP address for which MAC is required,
         * neighbor advertisement should not be sent since DAD will fail and 
         * VRRP settlement will not happen. */
        if (!(IS_ADDR_UNSPECIFIED (pIp6->srcAddr)))
        {
            u1_addr_status = ADDR6_UP;
        }
        else
        {
            return (IP6_FAILURE);
        }
    }

    /* Duplicate Address Detection */
    if (u1_addr_status & ADDR6_TENTATIVE)
    {
        if (IS_ADDR_UNSPECIFIED (pIp6->srcAddr))
        {
            /* Upon the source being an unspecified, indicate DAD status */
            Nd6IndicateDadStatus (pIf6, &pNd6Nsol->targAddr6, DAD_FAILURE);
            return IP6_FAILURE;
        }
        else
        {
            /* NS message is silently dropped */
            IP6_TRC_ARG2 (u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC, ND6_NAME,
                          "ND6:Nd6RcvNeighSol: SrcAddr=%s Not UNSPECIFIED "
                          "for Tentative Target Add=%s\n",
                          Ip6PrintAddr (&pIp6->srcAddr),
                          Ip6PrintAddr (&pNd6Nsol->targAddr6));

            return (IP6_FAILURE);
        }
    }

    if (!(IS_ADDR_UNSPECIFIED (pIp6->srcAddr)))
    {
        if (*(IP6_BUF_DATA_PTR(pBuf) + IPV6_PAYLOAD_OFFSET) < IPV6_NS_PKT_LENGTH)
        {
            Nd6Resolve (NULL, pIf6, &pIp6->srcAddr, pBuf, ADDR6_MULTI);
            pNd6CacheEntry = Nd6IsCacheForAddr (pIf6, &pIp6->srcAddr);
            if (pNd6CacheEntry != NULL)
            {
                pNd6CacheEntry->b1LinkAddrNAFlag = IPV6_SEND_NA_FLAG_SET;
            }
            return (IP6_SUCCESS);
        }
    }

    /* Send Solicited NA message as response */
    if (Nd6SendSolNeighAdv (pIf6, pIp6, pNd6Nsol) == IP6_FAILURE)
    {
        IP6_TRC_ARG (u4ContextId, ND6_MOD_TRC, MGMT_TRC | ALL_FAILURE_TRC,
                     ND6_NAME,
                     "ND6:Nd6RcvNeighSol: Sending Solicited NA Failed\n");
        return (IP6_FAILURE);
    }

    if (Ip6BufRelease (u4ContextId, pBuf, FALSE, ND6_MODULE) != IP6_SUCCESS)
    {
        IP6_TRC_ARG (u4ContextId, ND6_MOD_TRC, BUFFER_TRC, ND6_NAME,
                     "ND6:Nd6RcvNeighSol: Rcv NS - BufRelease Failed\n");
        IP6_TRC_ARG3 (u4ContextId, ND6_MOD_TRC, BUFFER_TRC, IP6_NAME,
                      "ND6:Nd6RcvNeighSol: %s buffer error occurred,Type =%d Module =0x%X Bufptr =%p",
                      ERROR_FATAL_STR, ERR_BUF_RELEASE, pBuf);
    }
    return (IP6_SUCCESS);

}

/*****************************************************************************
DESCRIPTION         This routine processes the NS Extension Options and acts
                    accordingly

                    
INPUTS              pIf6        -   Pointer to the interface 
                    pIp6        -   Pointer to the IPv6 header of
                                     the received packet
                    u2ExtnsLen  -   Length of the ND extensions in the
                                     received IPv6 packet
                    pBuf        -   Pointer to the received
                                     IPv6 packet

OUTPUTS             None

RETURNS             IP6_SUCCESS      - Sending of NA succeeds
                    IP6_FAILURE  - Processing of NS fails 
****************************************************************************/
INT4
Nd6ProcessNeighSolExtns (tIp6If * pIf6, tIp6Hdr * pIp6, UINT2 u2ExtnsLen,
                         tCRU_BUF_CHAIN_HEADER * pBuf)
{
    tNd6ExtHdr          nd6Ext;
    tNd6ExtHdr         *pNd6Ext = NULL;
    tNd6AddrExt         Nd6Lla;
    tNd6AddrExt        *pNd6Lla = NULL;
    tUtlSysPreciseTime  SysPreciseTime;
    tCgaOptions         CgaOptions;
    tNd6CacheEntry     *pNd6cEntry = NULL;
    UINT4               u4ExtOffset = 0;
    UINT1               u1SecureFlag = IP6_ZERO;
    UINT4               u4TempOffset = 0;
    UINT4               u4SeNDOptCount = 0;
    UINT4               u4TSNew =0 ;
    UINT1               au1PubKey[IP6_SEND_CGA_BYTES];
    UINT1               au1Nonce[ND6_NONCE_LENGTH];
    UINT1		u1SeNDFlag = 0;
 
    MEMSET (&nd6Ext, 0, sizeof (tNd6ExtHdr));
    MEMSET (&Nd6Lla, 0, sizeof (tNd6AddrExt));
    MEMSET (&CgaOptions, 0, sizeof (tCgaOptions));
    MEMSET (au1PubKey, 0, IP6_SEND_CGA_BYTES);
    MEMSET (au1Nonce, 0, ND6_NONCE_LENGTH);
    MEMSET (&SysPreciseTime, 0, sizeof(tUtlSysPreciseTime));

    u4TempOffset = IP6_BUF_READ_OFFSET (pBuf) - sizeof (tNd6NeighSol);
    CgaOptions.pu1DerPubKey = au1PubKey;

    /* Extract the Extension Options */
    while (u2ExtnsLen)
    {
        if ((pNd6Ext = Nd6ExtractExtnHdrInCxt (pIf6->pIp6Cxt, pBuf)) == NULL)
        {
            return (IP6_FAILURE);
        }

        /* Already read 2 (IP6_TYEP_LEN_BYTES) bytes of Data.
         * Reset the IP6_BUFFER_READ_OFFSET to the previous value for enabling
         * quick processing of the remaining options. */
        u4ExtOffset = IP6_BUF_READ_OFFSET (pBuf) - IP6_TYPE_LEN_BYTES;
        IP6_BUF_READ_OFFSET (pBuf) = u4ExtOffset;

        switch (pNd6Ext->u1Type)
        {
            case ND6_SRC_LLA_EXT:
                if (Nd6ProcessSLLAOption (u2ExtnsLen, pNd6Ext, pIf6,
                                          &Nd6Lla, &pNd6Lla,
                                          pBuf) == IP6_FAILURE)
                {
                    IP6_TRC_ARG (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                                 DATA_PATH_TRC, ND6_NAME,
                                 "ND6: Nd6ProcessNeighSolExtns:Validation of NS "
                                 "Failed - Invalid SLLA \n");
                    (IP6_SEND_IF_DROP_STATS
                     (pIf6, COUNT_NS)).u4SrcLinkAddrPkts++;
                    return IP6_FAILURE;
                }
                u2ExtnsLen = (UINT2) (u2ExtnsLen - sizeof (tNd6AddrExt));
                (IP6_SEND_IF_IN_STATS (pIf6, COUNT_NS)).u4SrcLinkAddrPkts++;
                break;

            case ND6_SEND_CGA_OPT:
                if (IP6_TRUE == IF_SEND_ENABLED (pIf6))
                {
                    if (Nd6SeNDProcessCgaOption (pIf6, pIp6, pBuf,
                            pNd6Ext->u1Len, &CgaOptions) == IP6_FAILURE)
                    {
                        IP6_TRC_ARG (pIf6->pIp6Cxt->u4ContextId,
                                     ND6_MOD_TRC, BUFFER_TRC, 
                                     ND6_NAME, "Nd6ProcessNeighSolExtns:"
                                     " Cga processing failed\n");
                        (IP6_SEND_IF_DROP_STATS
                        (pIf6, COUNT_NS)).u4CgaOptPkts++;
                        (IP6_IF_STATS (pIf6))->u4NdSecureDroppedPkts++;
                        return IP6_FAILURE;
                    }
                    (IP6_SEND_IF_IN_STATS 
                     (pIf6, COUNT_NS)).u4CgaOptPkts++;
                    u4SeNDOptCount++;
                }
		else
		{
                     IP6_BUF_READ_OFFSET (pBuf) +=
                      (UINT4)(pNd6Ext->u1Len * BYTES_IN_OCTECT);
		}
                /* extension length is in unit of 8 octets */
                u2ExtnsLen = (UINT2)(u2ExtnsLen - 
                                    (pNd6Ext->u1Len * BYTES_IN_OCTECT));
                break;

            case ND6_SEND_TIMESTAMP_OPT:

                if (IP6_TRUE == IF_SEND_ENABLED (pIf6))
                {
                    if (Nd6SeNDProcessTSOption(pIf6, pIp6, pBuf, &u4TSNew)
                           == IP6_FAILURE)
                    {
                        IP6_TRC_ARG (pIf6->pIp6Cxt->u4ContextId,
                                     ND6_MOD_TRC, BUFFER_TRC, 
                                     ND6_NAME, "Nd6ProcessNeighSolExtns:"
                                     " Timestamp processing failed\n");
                        (IP6_SEND_IF_DROP_STATS
                        (pIf6, COUNT_NS)).u4TimeStampOptPkts++;
                        (IP6_IF_STATS (pIf6))->u4NdSecureDroppedPkts++;
                        return IP6_FAILURE;
                    }
                    u4SeNDOptCount++;
                    (IP6_SEND_IF_IN_STATS 
		    (pIf6, COUNT_NS)).u4TimeStampOptPkts++;
                }
		else
		{
                     IP6_BUF_READ_OFFSET (pBuf) +=
                      (UINT4)(pNd6Ext->u1Len * BYTES_IN_OCTECT);
		}
                /* extension length is in unit of 8 octets */
                u2ExtnsLen = (UINT2) (u2ExtnsLen -
                                     (pNd6Ext->u1Len * BYTES_IN_OCTECT));
                break;

            case ND6_SEND_NONCE_OPT:

                if (IP6_TRUE == IF_SEND_ENABLED (pIf6))
                {
                    if (Nd6SeNDProcessNonceOption(pIf6, pIp6, pBuf,
                            au1Nonce, u1SeNDFlag) == IP6_FAILURE)
                    {
                        IP6_TRC_ARG (pIf6->pIp6Cxt->u4ContextId,
                                     ND6_MOD_TRC, BUFFER_TRC, 
                                     ND6_NAME, "Nd6ProcessNeighSolExtns:"
                                     " Nonce processing failed\n");
                        (IP6_SEND_IF_DROP_STATS
                        (pIf6, COUNT_NS)).u4NonceOptPkts++;
                        (IP6_IF_STATS (pIf6))->u4NdSecureDroppedPkts++;
                        return IP6_FAILURE;
                    }
                    u4SeNDOptCount++;
                    (IP6_SEND_IF_IN_STATS 
		    (pIf6, COUNT_NS)).u4NonceOptPkts++;
                }
		else
		{
                     IP6_BUF_READ_OFFSET (pBuf) +=
                      (UINT4)(pNd6Ext->u1Len * BYTES_IN_OCTECT);
		}
                /* extension length is in unit of 8 octets */
                u2ExtnsLen = (UINT2) (u2ExtnsLen -
                                     (pNd6Ext->u1Len * BYTES_IN_OCTECT));
                break;

            case ND6_SEND_RSA_SIGN_OPT:
                if (IP6_TRUE == IF_SEND_ENABLED (pIf6))
                {
                    if (Nd6SeNDProcessRsaOption (pIf6, pIp6, pBuf,
                            pNd6Ext->u1Len, u4TempOffset,
                            &CgaOptions) == IP6_FAILURE)
                    {
                        IP6_TRC_ARG (pIf6->pIp6Cxt->u4ContextId,
                                     ND6_MOD_TRC, BUFFER_TRC, 
                                     ND6_NAME, "Nd6ProcessNeighSolExtns:"
                                     " RSA processing failed\n");
                        (IP6_SEND_IF_DROP_STATS
                        (pIf6, COUNT_NS)).u4RsaOptPkts++;
                        (IP6_IF_STATS (pIf6))->u4NdSecureDroppedPkts++;
                        return IP6_FAILURE;
                    }
                    u4SeNDOptCount++;
                    (IP6_SEND_IF_IN_STATS 
		    (pIf6, COUNT_NS)).u4RsaOptPkts++;
                }
		else
		{
                     IP6_BUF_READ_OFFSET (pBuf) +=
                      (UINT4)(pNd6Ext->u1Len * BYTES_IN_OCTECT);
		}
                /* extension length is in unit of 8 octets */
                u2ExtnsLen = (UINT2) (u2ExtnsLen -
                                     (pNd6Ext->u1Len * BYTES_IN_OCTECT));
                break;

            default:
                if (u2ExtnsLen < (pNd6Ext->u1Len * BYTES_IN_OCTECT))
                {
                    /* Invalid NS Packet Option. */
                    IP6_TRC_ARG (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                                 DATA_PATH_TRC, ND6_NAME,
                                 "ND6:Nd6ProcessNeighSolExtns: Validation of "
                                 "NS Failed - Invalid Unknown Option Length \n");
                    return IP6_FAILURE;
                }

                /* Unknown Option. No need for any processing. Go to next
                 * available Option. */
                u2ExtnsLen = (UINT2)(u2ExtnsLen - 
                                    (pNd6Ext->u1Len * BYTES_IN_OCTECT));
                IP6_BUF_READ_OFFSET (pBuf) +=
                    (UINT4)(pNd6Ext->u1Len * BYTES_IN_OCTECT);
                break;
        }
    }

    /* 
     * If SeND is enabled in full secure mode, all the four options 
     * should be present.
     * In mixed mode, either all options should be present 
     * or no SeND options should be present in the packet.
     */
    if (((ND6_SECURE_ENABLE == pIf6->u1SeNDStatus) &&
          u4SeNDOptCount != ND6_SEND_SOL_OPT_COUNT) ||
       ((ND6_SECURE_MIXED == pIf6->u1SeNDStatus) && 
        (!((u4SeNDOptCount == IP6_ZERO) ||
         (u4SeNDOptCount == ND6_SEND_SOL_OPT_COUNT)))))
    {
        (IP6_IF_STATS (pIf6))->u4NdSecureInvalidPkts++;
        return (IP6_FAILURE);
    }

    /* update IsSecure flag */
    u1SecureFlag = ND6_UNSECURE_ENTRY;
    if ((ND6_SECURE_DISABLE != pIf6->u1SeNDStatus)
        && (ND6_SEND_SOL_OPT_COUNT == u4SeNDOptCount))
    {
        u1SecureFlag = ND6_SECURE_ENTRY;
    }

    if (!(IS_ADDR_UNSPECIFIED (pIp6->srcAddr)))
    {
        /* 
         * Update the NDCache entry if the source is not
         * an Unspecified address 
         */
        if (pNd6Lla != NULL)
        {
            if (Nd6UpdateCache (pIf6, &pIp6->srcAddr,
                                pNd6Lla->lladdr, IP6_ENET_ADDR_LEN,
                                ND6_NEIGHBOR_SOLICITATION, NULL,
                                u1SecureFlag) == IP6_FAILURE)
            {
                IP6_TRC_ARG (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                             ALL_FAILURE_TRC, ND6_NAME,
                             "ND6:Nd6ProcessNeighSolExtns: SRC LLA Extn - Update "
                             "Cache Failed \n");
                return (IP6_FAILURE);
            }

            /* Update TSLast and RDLast values */
            if (IP6_TRUE == IF_SEND_ENABLED (pIf6))
            {
                pNd6cEntry = Nd6IsCacheForAddr (pIf6, &pIp6->srcAddr);
                if (NULL != pNd6cEntry)
                {   
                    if (u4TSNew != IP6_ZERO)
                    {
                        /* first recevied message from the neighbor */
                        TmrGetPreciseSysTime (&SysPreciseTime);
                        MEMCPY (&pNd6cEntry->u4SendRDLast, 
                                &(SysPreciseTime.u4Sec), IP6_FOUR);
                        pNd6cEntry->u4SendTSLast = u4TSNew; 
                    } 

                    /* update the nonce incase it exists */
                    if (MEMCMP (pNd6cEntry->au1RcvdNonce, au1Nonce,
                            ND6_NONCE_LENGTH) != IP6_ZERO)
                    {
                        MEMCPY (pNd6cEntry->au1RcvdNonce, au1Nonce,
                                ND6_NONCE_LENGTH);
                    }  
                    else
                    {
			if (ND6_SECURE_MIXED != pIf6->u1SeNDStatus)
			{
                        /* stale nonce */
                           IP6_TRC_ARG (pIf6->pIp6Cxt->u4ContextId,
                                        ND6_MOD_TRC, ALL_FAILURE_TRC,
                                        ND6_NAME, "Nd6ProcessRoutSolExtns:"
                                        " received stale nonce \n");
                           return (IP6_FAILURE);	
			}
                    }

                }
            }
        }
    }

    return (IP6_SUCCESS);

}

/*****************************************************************************
DESCRIPTION         This routine validates the received NS message

                    
INPUTS              pIp6        -   Pointer to the IPv6 header of
                                     the received packet
                    pNd6NSol    -   Pointer to the neighbor solicitation
                    u2Len       -   Length of the neighbor solicitation.

OUTPUTS             None

RETURNS             IP6_SUCCESS      - Sending of NA succeeds
                    IP6_FAILURE  - Processing of NS fails 
****************************************************************************/
INT4
Nd6ValidateNeighSolInCxt (tIp6Cxt * pIp6Cxt, tIp6Hdr * pIp6,
                          tNd6NeighSol * pNd6Nsol, UINT2 u2Len)
{
    tIcmp6PktHdr        icmp6Hdr;

    icmp6Hdr = pNd6Nsol->icmp6Hdr;
    if (Nd6ValidateMsgHdrInCxt (pIp6Cxt, pIp6, &icmp6Hdr) == IP6_FAILURE)
    {
        return (IP6_FAILURE);
    }

    /* ICMP Length MUST be 24 or more octets */
    if (u2Len < sizeof (tNd6NeighSol))
    {
        IP6_TRC_ARG1 (pIp6Cxt->u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC,
                      ND6_NAME,
                      "ND6:Nd6ValidateNeighSol: Validation of NS Failed - Incorrect Len %d\n",
                      u2Len);
        return (IP6_FAILURE);
    }

    /* Target MUST not be Multicast address */
    if (IS_ADDR_MULTI (pNd6Nsol->targAddr6))
    {
        IP6_TRC_ARG1 (pIp6Cxt->u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC,
                      ND6_NAME,
                      "ND6:Nd6ValidateNeighSol: NS Fail- Target Addr%s MUST not be MULTICAST:\n",
                      Ip6ErrPrintAddr (ERROR_MINOR, &pNd6Nsol->targAddr6));
        return (IP6_FAILURE);
    }

    return (IP6_SUCCESS);

}

/*****************************************************************************
DESCRIPTION         Processes the received Neighbor Advertisement
                    Messages and updates the corresponding 
                    NDCache entry

INPUTS              pIf6        -   Pointer to the interface 
                    pIp6        -   Pointer to the IPv6 header of
                                     the received packet
                    u2Len       -   Length of the ND payload in the
                                     received IPv6 packet
                    pBuf        -   Pointer to the received
                                     IPv6 packet

OUTPUTS             None

RETURNS             IP6_SUCCESS     -  Processing of NA succeeds
                    IP6_FAILURE -  Processing of NA fails
****************************************************************************/

INT4
Nd6RcvNeighAdv (tIp6If * pIf6, tIp6Hdr * pIp6, UINT2 u2Len,
                tCRU_BUF_CHAIN_HEADER * pBuf)
{
    tNd6NeighAdv  nd6Nadv;
    tNd6NeighAdv *pNd6Nadv = NULL;
    tIp6LlocalInfo     *pInfo = NULL;
    tTMO_SLL_NODE      *pSllInfo = NULL;
    UINT1              u1Flag = OSIX_FALSE;
    UINT4               u4Index = 0;
    tIp6Addr     *pIp6TempAddr = NULL;
    tCgaOptions  *pCgaParams = NULL;
    UINT1        *pu1AddrInfo = NULL;
    UINT1         u1Copy = 0, u1AddrType = 0;
    UINT2         u2ExtnsLen = 0;
    UINT4         u4Nd6Offset = 0, u4AdvFlag = 0;
    UINT4         u4ContextId = pIf6->pIp6Cxt->u4ContextId;
    UINT1         u1NbrSecure = IP6_ZERO;
    UINT1         u1DadFailure = IP6_ZERO;
    UINT1         u1PrefixLen = IP6_ZERO;
    tNd6CacheEntry  *pNd6CacheEntry = NULL;

    MEMSET(&nd6Nadv, IP6_ZERO, sizeof (tNd6NeighAdv));

    /* Extract NA Message from the buffer */
    u4Nd6Offset = IP6_BUF_READ_OFFSET (pBuf) - sizeof (tIcmp6PktHdr);
    IP6_BUF_READ_OFFSET (pBuf) -= sizeof (tIcmp6PktHdr);

    if ((pNd6Nadv = (tNd6NeighAdv *) Ip6BufRead
         (pBuf, (UINT1 *) &nd6Nadv, u4Nd6Offset,
          sizeof (tNd6NeighAdv), u1Copy)) == NULL)
    {
        IP6_TRC_ARG2 (u4ContextId, ND6_MOD_TRC, BUFFER_TRC, ND6_NAME,
                      "ND6:Nd6RcvNeighAdv: Receive NA - BufRead Failed! "
                      "BufPtr %p Offset %d\n", pBuf, u4Nd6Offset);
        IP6_TRC_ARG3 (u4ContextId, ND6_MOD_TRC, BUFFER_TRC, IP6_NAME,
                      "ND6:Nd6RcvNeighAdv: %s buffer error occurred, Type = %d "
                      "Module = 0x%X Bufptr = %p",
                      ERROR_FATAL_STR, ERR_BUF_READ, pBuf);
        return (IP6_FAILURE);
    }

    u4AdvFlag = NTOHL (pNd6Nadv->u4AdvFlag);

    IP6_TRC_ARG2 (u4ContextId, ND6_MOD_TRC, CONTROL_PLANE_TRC, ND6_NAME,
                  "ND6:Nd6RcvNeighAdv: Reception of NA -Target Address = %s "
                  "Advertise Flag =0x%X\n",
                  Ip6PrintAddr (&pNd6Nadv->targAddr6), u4AdvFlag);

    /* Validate the NS message */
    if (Nd6ValidateNeighAdvInCxt (pIf6->pIp6Cxt, pIp6, pNd6Nadv,
                                  u4AdvFlag, u2Len) == IP6_FAILURE)
    {
        /* Increment the count on ICMP bad packets received */
        ICMP6_INC_IN_ERRS (pIf6->pIp6Cxt->Icmp6Stats);
        ICMP6_INTF_INC_IN_ERRS (pIf6);
        return (IP6_FAILURE);
    }

    /* Duplicate Address Detection */
    u1AddrType = (UINT1) Ip6AddrType (&pNd6Nadv->targAddr6);
    if ((Ip6IsPktToMeInCxt (pIf6->pIp6Cxt, &u1AddrType, 1, &pNd6Nadv->targAddr6,
                            pIf6) == IP6_SUCCESS)
#ifdef HA_WANTED
        || (Ip6IsAddrMNHomeAddr (pIf6, &pNd6Nadv->targAddr6, &u1AddrType) ==
            IP6_SUCCESS)
#endif
        )
    {
        /* 
         * Upon the target address being TENTATIVE on the interface,
         * indicate DAD status to ADDR submodule
         */
        if (u1AddrType & ADDR6_TENTATIVE)
        {
            if (!IS_ALL_NODES_MULTI (pIp6->dstAddr))
            {
                return IP6_FAILURE;
            }

            /* set the Dad flag */
            u1DadFailure = IP6_ONE;
        }
    }

    else
    {
	    if (Ip6IsOurAddrInCxt (pIf6->pIp6Cxt->u4ContextId,
				    &pNd6Nadv->targAddr6, &u4Index) == IP6_FAILURE)
	    {
		    if(Nd6IsCacheForAddr (pIf6, &pNd6Nadv->targAddr6) == NULL)
		    {
			    /* Increment the count on ICMP bad packets received */
			    ICMP6_INC_IN_ERRS (pIf6->pIp6Cxt->Icmp6Stats);
			    ICMP6_INTF_INC_IN_ERRS (pIf6);
			    return (IP6_FAILURE);
		    }
	    }

    }

    u2ExtnsLen = (UINT2) (u2Len - sizeof (tNd6NeighAdv));
    if ((u2ExtnsLen > 0) &&
        (u1DadFailure == IP6_ONE))
    {
        /*
         * calling neighbor extensions to know the neighbor is 
         * secure or not
         */ 
        if (Nd6ProcessNeighAdvExtns (pIf6, pIp6, pNd6Nadv,
                                     u4AdvFlag, u2ExtnsLen, pBuf,
                                     TRUE, &u1NbrSecure) == IP6_FAILURE)
        {
            return (IP6_FAILURE);
        }
    }

    if (u1DadFailure)
    {
        if (pIf6->u1SeNDStatus != ND6_SECURE_DISABLE)
        {
			if (gNd6GblInfo.u4Nd6SeNDAcceptUnsecure == 
			   			ND6_ACCEPT_UNSEC_ADV_ENABLE)
            {
               pu1AddrInfo = Ip6GetAddrInfo (pIf6, &pNd6Nadv->targAddr6);
               if (pu1AddrInfo == NULL)
               {
                   return IP6_FAILURE;
               }

               if (Ip6AddrType (&pNd6Nadv->targAddr6) == ADDR6_LLOCAL)
               {
                   pCgaParams = &((tIp6LlocalInfo *) (VOID *) pu1AddrInfo)->cgaParams;
                   pIp6TempAddr = &((tIp6LlocalInfo *) (VOID *) pu1AddrInfo)->ip6Addr; 
                   u1PrefixLen = (UINT1)IP6_UINT1_ALL_ONE;
               }
               else
               {
                   pCgaParams = &((tIp6AddrInfo *)(VOID *) pu1AddrInfo)->cgaParams;
                   pIp6TempAddr = &((tIp6AddrInfo *)(VOID *) pu1AddrInfo)->ip6Addr;
                   u1PrefixLen = ((tIp6AddrInfo *)(VOID *) pu1AddrInfo)->u1PrefLen;
               }

               /*
                *
                */
               if (((u1NbrSecure == ND6_UNSECURE_ENTRY) &&
                    (pIf6->u1SeNDStatus == ND6_SECURE_MIXED) &&
                    (pCgaParams->u1Collisions == 0)) ||
                   ((u1NbrSecure == ND6_SECURE_ENTRY) &&
                    (pIf6->u1SeNDStatus != ND6_SECURE_DISABLE) &&
                    (pCgaParams->u1Collisions < IP6_TWO)))
               {
                   pCgaParams->u1Collisions++;
                   if (Nd6SeNDCgaGenerate (pIf6, pIp6TempAddr, pCgaParams)
                                             == IP6_FAILURE)
                   {
                       return IP6_FAILURE;
                   }

                   /*
                    * explicit sync of modifier is required to replace
                    * the old cga address to the newly generated
                    */
                   Ip6UtilCgaAddrSyncToStandby(pIf6->u4Index, pIp6TempAddr,
                                               u1PrefixLen,
                                               pCgaParams);
	               Ip6AddrDadStart (pu1AddrInfo, u1AddrType);
                   return IP6_SUCCESS;
               }
		    }
        }

        Nd6IndicateDadStatus (pIf6, &pNd6Nadv->targAddr6, DAD_FAILURE);
        if (Ip6BufRelease (u4ContextId, pBuf, FALSE, ND6_MODULE) !=
               IP6_SUCCESS)
        {
            IP6_TRC_ARG (u4ContextId, ND6_MOD_TRC, BUFFER_TRC, ND6_NAME,
                         "ND6:Nd6RcvNeighAdv: Rcv NA - BufRelease Failed\n");
            IP6_TRC_ARG3 (u4ContextId, ND6_MOD_TRC, BUFFER_TRC, IP6_NAME,
                          "ND6:Nd6RcvNeighAdv: %s buffer error occurred, "
                          "Type = %d Module = 0x%X Bufptr= %p",
                          ERROR_FATAL_STR, ERR_BUF_RELEASE, pBuf);
        }
        return (IP6_SUCCESS);
    }

    /* IPv6-TahiKame Test suite Failure Fix
     * RFC 4861 - Section 7.2.5
     * When a valid Neighbor Advertisement is received (either
     * solicited or unsolicited), the Neighbor Cache is searched 
     * for the target's entry. If no entry exists, the
     * advertisement SHOULD be silently discarded. */
    if (IS_ADDR_LLOCAL (pNd6Nadv->targAddr6))
    {
        TMO_SLL_Scan (&pIf6->lla6Ilist, pSllInfo, tTMO_SLL_NODE *)
        {
             pInfo = IP6_LLADDR_PTR_FROM_SLL (pSllInfo);
             if (MEMCMP (&pInfo->ip6Addr, &pNd6Nadv->targAddr6, sizeof (tIp6Addr)) == 0)
             { 
                 u1Flag = OSIX_FALSE;
                 break;
             }   
             else
             {
                 u1Flag = OSIX_TRUE;
             }   
        }  

     if ((u1Flag == OSIX_TRUE) && Nd6IsCacheForAddr (pIf6, &pNd6Nadv->targAddr6) == NULL)

        {
            /* Increment the count on ICMP bad packets received */
            ICMP6_INC_IN_ERRS (pIf6->pIp6Cxt->Icmp6Stats);
            ICMP6_INTF_INC_IN_ERRS (pIf6);
            return (IP6_FAILURE);
        }
    }

    if (u2ExtnsLen > 0)
    {
        if (Nd6ProcessNeighAdvExtns (pIf6, pIp6, pNd6Nadv,
                                     u4AdvFlag, u2ExtnsLen,
                                     pBuf, FALSE, &u1NbrSecure) == IP6_FAILURE)
        {
            return (IP6_FAILURE);
        }
    }
    else
    {
        /*
         * Update the NDCache entry if the received NA is solicited
         * and does not contain the target link layer extension
         */
        u4AdvFlag |= ND6_NEIGHBOR_ADVERTISEMENT;
        Nd6UpdateCache (pIf6, &pIp6->srcAddr, NULL, 0, u4AdvFlag, NULL, 0);
    }

    if (Nd6IsProxyEnabled (pIf6) == IP6_TRUE)
    {
        /* pkt is not for me but proxy enabled on interface */
        if (Nd6ProxyNeighAdv (pIf6, pIp6, pNd6Nadv) == IP6_FAILURE)
        {
            IP6_TRC_ARG (u4ContextId, ND6_MOD_TRC, MGMT_TRC | ALL_FAILURE_TRC,
                         ND6_NAME, "ND6:Nd6RcvNeighSol: Proxying NS Failed\n");
            return (IP6_FAILURE);
        }
        return IP6_SUCCESS;
    }
    pNd6CacheEntry = Nd6IsCacheForAddr (pIf6, &pIp6->srcAddr);
    if ((pNd6CacheEntry != NULL) && (pNd6CacheEntry->b1LinkAddrNAFlag == IPV6_SEND_NA_FLAG_SET))
    {
           Nd6SendNeighAdv (pIf6, pNd6CacheEntry, &(pIp6->dstAddr), &(pIp6->srcAddr),
                           &(pIp6->dstAddr), ND6_OVERRIDE_FLAG);
           pNd6CacheEntry->b1LinkAddrNAFlag = IPV6_SEND_NA_FLAG_RESET;
    }
    if (Ip6BufRelease (u4ContextId, pBuf, FALSE, ND6_MODULE) != IP6_SUCCESS)
    {
        IP6_TRC_ARG (u4ContextId, ND6_MOD_TRC, BUFFER_TRC | ALL_FAILURE_TRC,
                     ND6_NAME,
                     "ND6:Nd6RcvNeighAdv: Rcv NA - BufRelease Failed\n");
        IP6_TRC_ARG3 (u4ContextId, ND6_MOD_TRC, BUFFER_TRC, IP6_NAME,
                      "ND6:Nd6RcvNeighAdv: %s buffer error occurred, Type =%d "
                      "Module = 0x%X Bufptr = %p", ERROR_FATAL_STR,
                      ERR_BUF_RELEASE, pBuf);
    }
    return (IP6_SUCCESS);
}

/*****************************************************************************
DESCRIPTION         This routine processes the NA Extension Options and acts
                    accordingly

INPUTS              pIf6        -   Pointer to the interface 
                    pIp6        -   Pointer to the IPv6 header of
                                     the received packet
                    pNd6Nadv    -   Pointer to the Neigh adv
                    u4AdvFlag   -   R-S-O flag(router, solicited, and override) 
                    u2ExtnsLen  -   Length of neigh adv extentions.
                    pBuf        -   Pointer to the received
                                     IPv6 packet

OUTPUTS             None

RETURNS             IP6_SUCCESS     -  Processing of NA succeeds
                    IP6_FAILURE -  Processing of NA fails
****************************************************************************/
INT4
Nd6ProcessNeighAdvExtns (tIp6If *pIf6, tIp6Hdr *pIp6, 
                         tNd6NeighAdv *pNd6Nadv, UINT4 u4AdvFlag,
                         UINT2 u2ExtnsLen, tCRU_BUF_CHAIN_HEADER *pBuf,
                         UINT1 u1ForDad, UINT1 *pu1NbrSecure)
{
    tNd6AddrExt         Nd6Lla;
    tNd6AddrExt         *pNd6Lla = NULL;
    tNd6ExtHdr          *pNd6Ext = NULL;
    tCgaOptions         CgaOptions;
    tUtlSysPreciseTime  SysPreciseTime;
    tNd6CacheEntry     *pNd6cEntry = NULL;
    UINT4               u4ExtOffset = 0;
    UINT1               u1SecureFlag = IP6_ZERO;
    UINT4               u4TempOffset = 0;
    UINT4               u4SeNDOptCount = 0;
    UINT4               u4SeNDCountCheck = 0;
    UINT4               u4TSNew = 0;
    UINT1               au1PubKey[IP6_SEND_CGA_BYTES];
    UINT1		u1SeNDFlag = 0;    

    MEMSET (&Nd6Lla, 0, sizeof (tNd6AddrExt));
    MEMSET (&CgaOptions, 0, sizeof (tCgaOptions));
    MEMSET (au1PubKey, 0, IP6_SEND_CGA_BYTES);
    MEMSET (&SysPreciseTime, 0, sizeof(tUtlSysPreciseTime));

    u4TempOffset = IP6_BUF_READ_OFFSET (pBuf) - sizeof (tNd6NeighSol);
    CgaOptions.pu1DerPubKey = au1PubKey;

    if (u4AdvFlag & ND6_SOLICITED_FLAG)
    {
        /*
         * If it is a solicited message,
         * all 4 Options should be present
         */
        u4SeNDCountCheck = ND6_SEND_SOL_OPT_COUNT;
    }
    else
    {
        /* 
         * If it is a unsolicited message, 
         * then except nonce all other
         * options should be present 
         */
        u4SeNDCountCheck = ND6_SEND_UNSOL_OPT_COUNT;
    }

    /* Extract the Extension Options */
    while (u2ExtnsLen)
    {
        if ((pNd6Ext = Nd6ExtractExtnHdrInCxt (pIf6->pIp6Cxt, pBuf)) == NULL)
        {
            return (IP6_FAILURE);
        }
        u4ExtOffset = IP6_BUF_READ_OFFSET (pBuf) - IP6_TYPE_LEN_BYTES;
        IP6_BUF_READ_OFFSET (pBuf) = u4ExtOffset;

        switch (pNd6Ext->u1Type)
        {

            case ND6_TARG_LLA_EXT:
                u4AdvFlag |= ND6_NEIGHBOR_ADVERTISEMENT;

                if (Nd6ProcessTLLAOption
                    (u2ExtnsLen, pNd6Ext,
                     pIf6, &Nd6Lla, &pNd6Lla, pBuf) != IP6_SUCCESS)

                {
                    IP6_TRC_ARG (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                                 DATA_PATH_TRC, ND6_NAME,
                                 "ND6:Nd6ProcessNeighAdvExtns: Validation of NA Failed - Invalid TLLA \n");
                    (IP6_SEND_IF_DROP_STATS
                     (pIf6, COUNT_NA)).u4TgtLinkAddrPkts++;
                    (IP6_IF_STATS (pIf6))->u4NdSecureDroppedPkts++;
                    return IP6_FAILURE;
                }

                u2ExtnsLen = (UINT2)(u2ExtnsLen - sizeof (tNd6AddrExt));
                (IP6_SEND_IF_IN_STATS (pIf6, COUNT_NA)).u4TgtLinkAddrPkts++;
                break;

            case ND6_SEND_CGA_OPT:
                if (IP6_TRUE == IF_SEND_ENABLED (pIf6))
                {
                    if (Nd6SeNDProcessCgaOption (pIf6, pIp6, pBuf,
                            pNd6Ext->u1Len, &CgaOptions) == IP6_FAILURE)
                    {
                        IP6_TRC_ARG (pIf6->pIp6Cxt->u4ContextId,
                                     ND6_MOD_TRC, BUFFER_TRC, 
                                     ND6_NAME, "Nd6ProcessNeighSolExtns:"
                                     " Cga processing failed\n");
                        (IP6_SEND_IF_DROP_STATS
                        (pIf6, COUNT_NA)).u4CgaOptPkts++;
                        (IP6_IF_STATS (pIf6))->u4NdSecureDroppedPkts++;
                        return IP6_FAILURE;
                    }
                    u4SeNDOptCount++;
                }
		else
		{
                     IP6_BUF_READ_OFFSET (pBuf) +=
                      (UINT4)(pNd6Ext->u1Len * BYTES_IN_OCTECT);
		}
                /* extension length is in unit of 8 octets */
                u2ExtnsLen = (UINT2) (u2ExtnsLen -
                                     (pNd6Ext->u1Len * BYTES_IN_OCTECT));
                (IP6_SEND_IF_IN_STATS (pIf6, COUNT_NA)).u4CgaOptPkts++;
                break;

            case ND6_SEND_TIMESTAMP_OPT:

                if (IP6_TRUE == IF_SEND_ENABLED (pIf6))
                {
                    if (Nd6SeNDProcessTSOption(pIf6, pIp6, pBuf, &u4TSNew)
                           == IP6_FAILURE)
                    {
                        IP6_TRC_ARG (pIf6->pIp6Cxt->u4ContextId,
                                     ND6_MOD_TRC, BUFFER_TRC, 
                                     ND6_NAME, "Nd6ProcessNeighSolExtns:"
                                     " Timestamp processing failed\n");
                        (IP6_SEND_IF_DROP_STATS
                        (pIf6, COUNT_NA)).u4TimeStampOptPkts++;
                        (IP6_IF_STATS (pIf6))->u4NdSecureDroppedPkts++;
                        return IP6_FAILURE;
                    }
                    u4SeNDOptCount++;
                }
		else
		{
                     IP6_BUF_READ_OFFSET (pBuf) +=
                      (UINT4)(pNd6Ext->u1Len * BYTES_IN_OCTECT);
		}
                /* extension length is in unit of 8 octets */
                u2ExtnsLen = (UINT2) (u2ExtnsLen -
                                     (pNd6Ext->u1Len * BYTES_IN_OCTECT));
                (IP6_SEND_IF_IN_STATS (pIf6, COUNT_NA)).u4TimeStampOptPkts++;
                break;

            case ND6_SEND_NONCE_OPT:
				u1SeNDFlag = ND6_SEND_NA_MSG;
                if (IP6_TRUE == IF_SEND_ENABLED (pIf6))
                {
                    if (Nd6SeNDProcessNonceOption(pIf6, pIp6, pBuf,
                            NULL, u1SeNDFlag) == IP6_FAILURE)
                    {
                        IP6_TRC_ARG (pIf6->pIp6Cxt->u4ContextId,
                                     ND6_MOD_TRC, BUFFER_TRC, 
                                     ND6_NAME, "Nd6ProcessNeighSolExtns:"
                                     " Nonce processing failed\n");
                        (IP6_SEND_IF_DROP_STATS
                        (pIf6, COUNT_NA)).u4NonceOptPkts++;
                        (IP6_IF_STATS (pIf6))->u4NdSecureDroppedPkts++;
                        return IP6_FAILURE;
                    }
                    u4SeNDOptCount++;
                }
		else
		{
                     IP6_BUF_READ_OFFSET (pBuf) +=
                      (UINT4)(pNd6Ext->u1Len * BYTES_IN_OCTECT);
		}
                /* extension length is in unit of 8 octets */
                u2ExtnsLen = (UINT2) (u2ExtnsLen -
                                     (pNd6Ext->u1Len * BYTES_IN_OCTECT));
                (IP6_SEND_IF_IN_STATS (pIf6, COUNT_NA)).u4NonceOptPkts++;
                break;

            case ND6_SEND_RSA_SIGN_OPT:
                if (IP6_TRUE == IF_SEND_ENABLED (pIf6))
                {
                    if (Nd6SeNDProcessRsaOption (pIf6, pIp6, pBuf,
                            pNd6Ext->u1Len, u4TempOffset,
                            &CgaOptions) == IP6_FAILURE)
                    {
                        IP6_TRC_ARG (pIf6->pIp6Cxt->u4ContextId,
                                     ND6_MOD_TRC, BUFFER_TRC, 
                                     ND6_NAME, "Nd6ProcessNeighSolExtns:"
                                     " RSA processing failed\n");
                        (IP6_SEND_IF_DROP_STATS
                        (pIf6, COUNT_NA)).u4RsaOptPkts++;
                        (IP6_IF_STATS (pIf6))->u4NdSecureDroppedPkts++;
                        return IP6_FAILURE;
                    }
                    u4SeNDOptCount++;
                }
		else
		{
                     IP6_BUF_READ_OFFSET (pBuf) +=
                      (UINT4)(pNd6Ext->u1Len * BYTES_IN_OCTECT);
		}
                /* extension length is in unit of 8 octets */
                u2ExtnsLen = (UINT2) (u2ExtnsLen -
                                     (pNd6Ext->u1Len * BYTES_IN_OCTECT));
                (IP6_SEND_IF_IN_STATS (pIf6, COUNT_NA)).u4RsaOptPkts++;
                break;

            default:
                return IP6_SUCCESS;
        }
    }

    /* If SeND is enabled in full secure mode then all the four options
     * should present, and in mixed mode either all options should present
     * or no SeND options should present in the packet.
     * if it is not then discard the packet
     */

    if (((ND6_SECURE_ENABLE == pIf6->u1SeNDStatus)&& 
         (u4SeNDOptCount != u4SeNDCountCheck)) ||
       ((ND6_SECURE_MIXED == pIf6->u1SeNDStatus) && 
        (!((u4SeNDOptCount == IP6_ZERO) ||
        (u4SeNDOptCount == u4SeNDCountCheck)))))
    {
        (IP6_IF_STATS (pIf6))->u4NdSecureInvalidPkts++;
        return (IP6_FAILURE);
    }

    u1SecureFlag = ND6_UNSECURE_ENTRY;
    if ((ND6_SECURE_DISABLE != pIf6->u1SeNDStatus)
        && (u4SeNDCountCheck == u4SeNDOptCount))
    {
        u1SecureFlag = ND6_SECURE_ENTRY;
    }
    *pu1NbrSecure = u1SecureFlag;

    if (u1ForDad == IP6_ONE)
    {
        return IP6_SUCCESS;
    }

    /* If all the options length are greater than zero update the cache entry */
    if (!(IS_ADDR_UNSPECIFIED (pIp6->srcAddr)))
    {
        /* Update the NDCache entry if the source is not an Unspecified address */
        if (pNd6Lla != NULL)
        {
            if (Nd6UpdateCache (pIf6, &pNd6Nadv->targAddr6,
                                pNd6Lla->lladdr, IP6_ENET_ADDR_LEN, u4AdvFlag,
                                pIp6, u1SecureFlag) == IP6_FAILURE)
            {
                IP6_TRC_ARG (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                             MGMT_TRC | ALL_FAILURE_TRC, ND6_NAME,
                             "ND6:Nd6ProcessNeighAdvExtns: Target LLA Extn, - Update "
                             "Cache Failed \n");
                return (IP6_FAILURE);
            }
			
            if (IP6_TRUE == IF_SEND_ENABLED (pIf6))
            {
               if (u4TSNew != 0)
               {
                   pNd6cEntry = Nd6IsCacheForAddr (pIf6, &pIp6->srcAddr);
                   if (NULL != pNd6cEntry)
                   {
                       TmrGetPreciseSysTime (&SysPreciseTime);
                       MEMCPY (&pNd6cEntry->u4SendRDLast,
                               &(SysPreciseTime.u4Sec), sizeof (UINT4));
                       pNd6cEntry->u4SendTSLast = u4TSNew;
                   }
               }
            }
        }
    }
    return (IP6_SUCCESS);
}

/*****************************************************************************
DESCRIPTION         This routine validates the received NA message

INPUTS              pIp6        -   Pointer to the IPv6 header of
                                     the received packet
                    pNd6Nadv    -   Pointer to the Neigh adv
                    u4AdvFlag   -   R-S-O flag(router, solicited, and override)
                    u2Len  -   Length of neigh adv.

OUTPUTS             None

RETURNS             IP6_SUCCESS     -  Processing of NA succeeds
                    IP6_FAILURE -  Processing of NA fails
****************************************************************************/
INT4
Nd6ValidateNeighAdvInCxt (tIp6Cxt * pIp6Cxt, tIp6Hdr * pIp6,
                          tNd6NeighAdv * pNd6Nadv, UINT4 u4AdvFlag, UINT2 u2Len)
{
    tIcmp6PktHdr        icmp6Hdr;

    icmp6Hdr = pNd6Nadv->icmp6Hdr;
    if (Nd6ValidateMsgHdrInCxt (pIp6Cxt, pIp6, &icmp6Hdr) == IP6_FAILURE)
    {
        return (IP6_FAILURE);
    }

    /* ICMP Length MUST be 24 or more octets */
    if (u2Len < sizeof (tNd6NeighAdv))
    {
        IP6_TRC_ARG1 (pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                      DATA_PATH_TRC | ALL_FAILURE_TRC, ND6_NAME,
                      "Nd6ValidateNeighAdv: Validation of NA Failed - Incorrect Len %d\n",
                      u2Len);
        return (IP6_FAILURE);
    }

    /* Target MUST not be Multicast address */
    if (IS_ADDR_MULTI (pNd6Nadv->targAddr6))
    {
        IP6_TRC_ARG1 (pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                      DATA_PATH_TRC | ALL_FAILURE_TRC, ND6_NAME,
                      "ND6:Nd6ValidateNeighAdv: NA Fail - Target Address %s Must not be MULTICAST : \n",
                      Ip6ErrPrintAddr (ERROR_MINOR, &pNd6Nadv->targAddr6));
        return (IP6_FAILURE);
    }

    /* 
     * Solicited bit MUST not be set for IPv6 Destination 
     * being a MULTICAST address
     */
    if ((IS_ADDR_MULTI (pIp6->dstAddr)) && (u4AdvFlag & ND6_SOLICITED_FLAG))
    {
        IP6_TRC_ARG (pIp6Cxt->u4ContextId, ND6_MOD_TRC, ALL_FAILURE_TRC,
                     ND6_NAME, "ND6:Nd6ValidateNeighAdv: Valdn of NAFailed");
        IP6_TRC_ARG1 (pIp6Cxt->u4ContextId, ND6_MOD_TRC, ALL_FAILURE_TRC,
                      ND6_NAME,
                      "Solicited Flag Must notbe set for MULTICASR DestAddr:%s\n",
                      Ip6ErrPrintAddr (ERROR_MINOR, &pIp6->dstAddr));
        return (IP6_FAILURE);
    }
    if (u4AdvFlag & ND6_SOLICITED_FLAG)
    {
        ICMP6_INC_IN_NA_SFLAG (pIp6Cxt->Icmp6Stats);
    }
    if (u4AdvFlag & ND6_OVERRIDE_FLAG)
    {
        ICMP6_INC_IN_NA_OFLAG (pIp6Cxt->Icmp6Stats);
    }
    if (u4AdvFlag & ND6_DEFAULT_ROUTER)
    {
        ICMP6_INC_IN_NA_RFLAG (pIp6Cxt->Icmp6Stats);
    }
    return (IP6_SUCCESS);

}

/*****************************************************************************
DESCRIPTION         Validates the ND messages by checking
                    on the expected values of the fields like 
                    hop-limit, ICMP header fields

INPUTS              pBuf    -   Pointer to the ND packet
                                 for validation

OUTPUTS             None

RETURNS             OK      -  Validation successful
                    NOT_OK  -  Validation fails
****************************************************************************/

INT4
Nd6ValidateMsgHdrInCxt (tIp6Cxt * pIp6Cxt, tIp6Hdr * pIp6,
                        tIcmp6PktHdr * pIcmp6)
{

    /* To handle warnings in case of Trace is not defined */
    UNUSED_PARAM (pIp6Cxt);
    /* Hop Limit MUST have value 255 */
    if (pIp6->u1Hlim != IP6_MAX_HLIM)
    {
        IP6_TRC_ARG1 (pIp6Cxt->u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC,
                      ND6_NAME,
                      "ND6:Nd6ValidateMsgHdr: Validation of ND MsgHdr Fail - "
                      "Incorrect IP HopLimit %d\n", pIp6->u1Hlim);
        return (IP6_FAILURE);
    }

    /* ICMP Code MUST be ZERO */
    if (pIcmp6->u1Code != 0)
    {
        IP6_TRC_ARG1 (pIp6Cxt->u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC,
                      ND6_NAME,
                      "ND6:Nd6ValidateMsgHdr: Validation of ND MsgHdr Failed "
                      "- ICMP Bad Code %d\n", pIcmp6->u1Code);
        return (IP6_FAILURE);
    }

    return (IP6_SUCCESS);

}

/*****************************************************************************
DESCRIPTION         Extract the Extension Header of Options in ND messages

INPUTS              pBuf    -   Pointer to the ND packet

OUTPUTS             None

RETURNS             Pointer to the Nd6 extentions header if success.
                    NULL if extract fails 
****************************************************************************/

tNd6ExtHdr         *
Nd6ExtractExtnHdrInCxt (tIp6Cxt * pIp6Cxt, tCRU_BUF_CHAIN_HEADER * pBuf)
{
    UINT1               u1Copy = 0;
    tNd6ExtHdr          nd6Ext, *pNd6Ext = NULL;

    /* To handle warnings in case of Trace is not defined */
    UNUSED_PARAM (pIp6Cxt);

    if ((pNd6Ext = (tNd6ExtHdr *) Ip6BufRead
         (pBuf, (UINT1 *) &nd6Ext, IP6_BUF_READ_OFFSET (pBuf),
          IP6_TYPE_LEN_BYTES, u1Copy)) == NULL)
    {
        IP6_TRC_ARG2 (pIp6Cxt->u4ContextId, ND6_MOD_TRC, BUFFER_TRC, ND6_NAME,
                      "ND6:Nd6ExtractExtnHdr: Extract Extension Hdr-BufRead "
                      "Fail! BufPtr %p Offset%d\n",
                      pBuf, IP6_BUF_READ_OFFSET (pBuf));
        IP6_TRC_ARG3 (pIp6Cxt->u4ContextId, ND6_MOD_TRC, BUFFER_TRC, IP6_NAME,
                      "ND6:Nd6ExtractExtnHdr: %s buffer error occurred, Type "
                      "= %d Module=0x%X Bufptr=%p",
                      ERROR_FATAL_STR, ERR_BUF_READ, pBuf);
        return (NULL);
    }

    IP6_TRC_ARG1 (pIp6Cxt->u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC, ND6_NAME,
                  "ND6:Nd6ExtractExtnHdr: Extracting Extn Hdr : Extn Type %d\n",
                  pNd6Ext->u1Type);

    if (pNd6Ext->u1Len == 0)
    {
        IP6_TRC_ARG1 (pIp6Cxt->u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC,
                      ND6_NAME,
                      "ND6:Nd6ExtractExtnHdr: Extracting the Extn Header : "
                      "Invalid Len %d\n", pNd6Ext->u1Len);
        return (NULL);
    }

    return (pNd6Ext);

}

/*****************************************************************************
DESCRIPTION         This routine indicates to ADDR submodule of duplicate 
                    address upon it being TENTATIVE

INPUTS              pIf6        -   Pointer to the Interface structure.
                    pTargAddr6  -   Pointer to the target address. 
                    u1Status    -   Status of the address.

OUTPUTS             None.

RETURNS             None.
****************************************************************************/
VOID
Nd6IndicateDadStatus (tIp6If * pIf6, tIp6Addr * pTargAddr6, UINT1 u1Status)
{
    tIp6LlocalInfo     *pLlocalInfo = NULL;
    tIp6LlocalInfo     *pTempLlocalInfo = NULL;
    tIp6AddrInfo       *pUniAddrInfo = NULL;
    tTMO_SLL_NODE      *pTmpSll = NULL;
    tTMO_SLL_NODE      *pNextTmpSll = NULL;
    UINT1               u1IsAltLLPresent = OSIX_FALSE;
    UINT1               u1AddrType = 0;
    UINT1              *pAddrInfo = NULL;

    IP6_TRC_ARG3 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC,
                  ND6_NAME,
                  "ND6:Nd6IndicateDadStatus:IP6IF %d Status %d Target Address=%s\n",
                  pIf6->u4Index, u1Status, Ip6PrintAddr (pTargAddr6));

    pAddrInfo = Ip6GetAddrInfo (pIf6, pTargAddr6);

    if (pAddrInfo == NULL)
    {
        return;
    }
    u1AddrType = (UINT1)Ip6AddrType (pTargAddr6);
    if (u1AddrType != ADDR6_LLOCAL)
    {
        u1AddrType = ADDR6_UNICAST;
    }

    Ip6AddrDadStatus (pAddrInfo, u1Status, u1AddrType);
    if (u1Status == DAD_FAILURE)
    {
        if (u1AddrType == ADDR6_LLOCAL)
        {
            /* DAD Fails for the link-local address. Remove all other invalid
             * Link-Local Address. In case if an alternate valid Link-Local
             * Address is present, then continue to use that address.
             * Else bring down all the uni-cast address down and make the
             * interface status as STALLED */
            pLlocalInfo = (tIp6LlocalInfo *) (VOID *) pAddrInfo;

            pTmpSll = TMO_SLL_First (&pIf6->lla6Ilist);
            while (pTmpSll)
            {
                pTempLlocalInfo = IP6_LLADDR_PTR_FROM_SLL (pTmpSll);
                pNextTmpSll = TMO_SLL_Next (&pIf6->lla6Ilist, pTmpSll);

                if (MEMCMP (pLlocalInfo->ip6Addr.u1_addr,
                            pTempLlocalInfo->ip6Addr.u1_addr,
                            IP6_ADDR_SIZE) != 0)
                {
                    if ((pTempLlocalInfo->u1Status & ADDR6_FAILED) &&
                        (pTempLlocalInfo->u1ConfigMethod != IP6_ADDR_STATIC))
                    {
                        /* Invalid Link-Local Address. Delete it */
                        TMO_SLL_Delete (&pIf6->lla6Ilist, pTmpSll);
                        Ip6AddrNotifyLla (pTempLlocalInfo, IP6_IF_DELETE);
                    }
                    else
                    {
                        /* Valid Alternate Address is present. */
                        u1IsAltLLPresent = (UINT1)OSIX_TRUE;
                    }
                }

                pTmpSll = pNextTmpSll;
                pNextTmpSll = NULL;
                pTempLlocalInfo = NULL;
            }

            if (u1IsAltLLPresent == OSIX_FALSE)
            {
                /* No valid Link-Local Address present for this interface. */
                Ip6IfStall (pIf6);
            }
            else
            {
                Ip6AddrDeleteSolicitMcast (pIf6->u4Index,
                                           &pLlocalInfo->ip6Addr);
                pLlocalInfo->u1Status &= (UINT1)~ADDR6_UP;
                pLlocalInfo->u1Status |= ADDR6_DOWN;
            }
        }
        else
        {
            /* Remove the Solicated Link-local Address and set the 
             * address as operationally down. */
            pUniAddrInfo = (tIp6AddrInfo *) (VOID *) pAddrInfo;
            Ip6AddrDeleteSolicitMcast (pIf6->u4Index, &pUniAddrInfo->ip6Addr);
            pUniAddrInfo->u1Status &= (UINT1)~ADDR6_UP;
            pUniAddrInfo->u1Status |= ADDR6_DOWN;
        }
    }
    return;
}

/*****************************************************************************
DESCRIPTION         This routine performs the processing of source link layer 
                    option.

INPUTS              u2ExtnsLen  -   Length of extention header.
                    pNd6Ext     -   Pointer to the extention header.
                    pIp6        -   Pointer to Ip6 header.
                    pIf6        -   Pointer to the ip6 interface structure.
                    pBuf        -   Pointer to the message buffer.

OUTPUTS             Nd cache table.

RETURNS             SUCCESS or FAILURE.
****************************************************************************/
INT4
Nd6ProcessSLLAOption (UINT4 u4ExtnsLen, tNd6ExtHdr * pNd6Ext, tIp6If * pIf6,
                      tNd6AddrExt * pNd6Lla, tNd6AddrExt ** ppNd6Lla,
                      tCRU_BUF_CHAIN_HEADER * pBuf)
{

    UINT1               u1Copy = 1;
    UINT4               u4ExtOffset = 0;

    /* used only for trace */
    UNUSED_PARAM (pIf6);

    if ((u4ExtnsLen < (UINT4) pNd6Ext->u1Len * BYTE_LENGTH)
        || (pNd6Ext->u1Len != ND6_LLA_EXT_LEN))
    {
        IP6_TRC_ARG1 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC,
                      ND6_NAME,
                      "ND6:Nd6ProcessSLLAOption: SRC LLA Extn - Invalid Len %d\n",
                      pNd6Ext->u1Len);
        return (IP6_FAILURE);
    }

    u4ExtOffset = IP6_BUF_READ_OFFSET (pBuf);

    if ((*ppNd6Lla = (tNd6AddrExt *) Ip6BufRead (pBuf, (UINT1 *) pNd6Lla,
                                                 u4ExtOffset,
                                                 sizeof (tNd6AddrExt),
                                                 u1Copy)) == NULL)
    {
        IP6_TRC_ARG2 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC, BUFFER_TRC,
                      ND6_NAME,
                      "ND6:Nd6ProcessSLLAOption: Process SLLA Extn - BufRead "
                      "Fail! BufPtr %p Offset %d\n", pBuf, u4ExtOffset);
        IP6_TRC_ARG3 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC, BUFFER_TRC,
                      IP6_NAME,
                      "ND6:Nd6ProcessSLLAOption: %s buf error occurred, "
                      "Type = %d Module = 0x%X Bufptr = %p", ERROR_FATAL_STR,
                      ERR_BUF_READ, pBuf);
        return (IP6_FAILURE);
    }
    return IP6_SUCCESS;
}

/*****************************************************************************
DESCRIPTION         This routine performs the processing of target link layer 
                    option.

INPUTS              u2ExtnsLen  -   Length of extention header.
                    u4AdvFlag   -   R-S-O flag.
                    pIp6Addr    -   Pointer to the target address.
                    pNd6Ext     -   Pointer to the extention header.
                    pIp6        -   Pointer to Ip6 header.
                    pIf6        -   Pointer to the ip6 interface structure.
                    pBuf        -   Pointer to the message buffer.

OUTPUTS             Nd cache table.

RETURNS             SUCCESS or FAILURE.
****************************************************************************/
INT4
Nd6ProcessTLLAOption (UINT4 u4ExtnsLen, tNd6ExtHdr * pNd6Ext, tIp6If * pIf6,
                      tNd6AddrExt * Nd6Lla, tNd6AddrExt ** ppNd6Lla,
                      tCRU_BUF_CHAIN_HEADER * pBuf)
{
    UINT1               u1Copy = 1;
    UINT4               u4ExtOffset = 0;

    /* used only for trace */
    UNUSED_PARAM (pIf6);

    if ((u4ExtnsLen < (UINT4) pNd6Ext->u1Len * BYTE_LENGTH)
        || (pNd6Ext->u1Len != ND6_LLA_EXT_LEN))
    {
        IP6_TRC_ARG1 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC,
                      ND6_NAME,
                      "ND6:Nd6ProcessTLLAOption: Targ LLA Extn - Invalid "
                      "Len %d\n", pNd6Ext->u1Len);
        return (IP6_FAILURE);
    }
    u4ExtOffset = IP6_BUF_READ_OFFSET (pBuf);

    if ((*ppNd6Lla = (tNd6AddrExt *) Ip6BufRead (pBuf, (UINT1 *) Nd6Lla,
                                                 u4ExtOffset,
                                                 sizeof (tNd6AddrExt),
                                                 u1Copy)) == NULL)
    {
        IP6_TRC_ARG2 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                      BUFFER_TRC | ALL_FAILURE_TRC, ND6_NAME,
                      "ND6:Nd6ProcessTLLAOption: Process ND Extn-BufRead "
                      "Failed! BufPtr %p Offset %d\n", pBuf, u4ExtOffset);
        IP6_TRC_ARG3 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC, BUFFER_TRC,
                      IP6_NAME,
                      "ND6:Nd6ProcessTLLAOption: %s buf error occurred, "
                      "Type =%d Module =0x%X Bufptr=%p", ERROR_FATAL_STR,
                      ERR_BUF_READ, pBuf);
        return (IP6_FAILURE);
    }
    return IP6_SUCCESS;
}

/***************************** END OF FILE **********************************/
