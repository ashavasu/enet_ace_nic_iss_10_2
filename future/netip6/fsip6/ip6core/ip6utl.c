/********************************************************************
 * Copyright (C) 2008 Aricent Inc . All Rights Reserved
 *
 * $Id: ip6utl.c,v 1.29 2017/12/19 13:41:54 siva Exp $
 *
 * Description: IPv6 utility functions are present in this file.
 * *********************************************************************/

# include "ip6inc.h"
# include "fsmpipv6lw.h"
# include "rmgr.h"

PRIVATE VOID        Ip6ClearPmtuTable (UINT4 u4ContextId);
PRIVATE VOID        Ip6ClearPreferenceTable (UINT4 u4ContextId);
PRIVATE VOID        Ip6ClearNdProxyTable (UINT4 u4ContextId);
PRIVATE VOID        Ip6ClearAddressTable (UINT4 u4IfIndex);
PRIVATE VOID        Ip6ClearPrefixTable (UINT4 u4IfIndex);
PRIVATE VOID        Ip6ClearPolicyPrefixTable (UINT4 u4IfIndex);
PRIVATE VOID        Ip6ClearInterfaceTable (UINT4 u4IfIndex);

extern UINT4        FsMIIpv6AddrSENDCgaModifier[13];
extern UINT4        FsMIIpv6AddrSENDCollisionCount[13];

/****************************************************************************
 *  Function    :  IncMsrForIpv6NetToPhyTable
 *
 *   Input      :   i4IfIndex   - The interface index where the entry is added
 *                  u4IpAddress - IP address of the cache entry
 *                  cDataType   - Datatype of the configured value.
 *                  pSetVal     - Configured value that has to be notified.
 *                  pu4ObjectId - Object ID.
 *                  u4OIDLen    - Object Id length.
 *                  IsRowStatus - Rowstatus (TRUE/FALSE)
 *
 * Description :  This function sends notifications to MSR and RM
 *                 for the NetToMedia Table.
 *
 * Output      :  None.
 *
 * Returns     :  None                            
 *
 * ****************************************************************************/

VOID
IncMsrForIpv6NetToPhyTable (INT4 i4IfIndex,
                            tSNMP_OCTET_STRING_TYPE * pIpAddress,
                            CHR1 cDatatype, VOID *pSetVal, UINT4 *pu4ObjectId,
                            UINT4 u4OIdLen, UINT1 IsRowStatus)
{
    UINT4               u4SeqNum = IPVX_ZERO;
    INT4                i4SetValue = IPVX_ZERO;
    tSNMP_OCTET_STRING_TYPE *pSetString;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = pu4ObjectId;
    SnmpNotifyInfo.u4OidLen = u4OIdLen;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = IsRowStatus;
    SnmpNotifyInfo.pLockPointer = Ip6Lock;
    SnmpNotifyInfo.pUnLockPointer = Ip6UnLock;
    SnmpNotifyInfo.u4Indices = IPVX_THREE;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

    switch (cDatatype)
    {
        case 'i':
            i4SetValue = *(INT4 *) pSetVal;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %s %i",
                              i4IfIndex, INET_ADDR_TYPE_IPV6,
                              pIpAddress, i4SetValue));
            break;
        case 's':
            pSetString = (tSNMP_OCTET_STRING_TYPE *) pSetVal;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %s %s",
                              i4IfIndex, INET_ADDR_TYPE_IPV6,
                              pIpAddress, pSetString));
            break;
    }
#ifndef ISS_WANTED
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (pIpAddress);
#endif
    return;
}

/****************************************************************************
 *  Function    :  IncMsrForIpv6ContextTable
 *
 *   Input      :   u4ContextId - The context Id 
 *                  cDataType   - Datatype of the configured value.
 *                  pSetVal     - Configured value that has to be notified.
 *                  pu4ObjectId - Object ID.
 *                  u4OIDLen    - Object Id length.
 *
 * Description :  This function sends notifications to MSR and RM
 *                 for the IPV6 Context Table.
 *
 * Output      :  None.
 *
 * Returns     :  IPVX_SUCCESS or IPVX_FAILURE
 *
 * ****************************************************************************/

VOID
IncMsrForIpv6ContextTable (UINT4 u4ContextId, CHR1 cDatatype, VOID *pSetVal,
                           UINT4 *pu4ObjectId, UINT4 u4OIdLen)
{
    UINT4               u4SeqNum = IPVX_ZERO;
    INT4                i4SetValue = IPVX_ZERO;
    UINT4               u4SetValue = IPVX_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = pu4ObjectId;
    SnmpNotifyInfo.u4OidLen = u4OIdLen;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = Ip6Lock;
    SnmpNotifyInfo.pUnLockPointer = Ip6UnLock;
    SnmpNotifyInfo.u4Indices = IP6_ONE;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

    switch (cDatatype)
    {
        case 'i':
            i4SetValue = *(INT4 *) pSetVal;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                              u4ContextId, i4SetValue));
            break;
        case 'u':
            u4SetValue = *(UINT4 *) pSetVal;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u",
                              u4ContextId, u4SetValue));
            break;
    }
#ifndef ISS_WANTED
    UNUSED_PARAM (u4ContextId);
#endif
    return;
}

/****************************************************************************
 *  Function    :  IncMsrForIpv6AddrTable
 *
 *   Input      :   i4IfIndex   - The interface index where the entry is added
 *                  Ipv6Address - IP address
 *                  i4PrefixLen - Length of IPV6 address 
 *                  cDataType   - Datatype of the configured value.
 *                  pSetVal     - Configured value that has to be notified.
 *                  pu4ObjectId - Object ID.
 *                  u4OIDLen    - Object Id length.
 *                  IsRowStatus - Rowstatus (TRUE/FALSE)
 *
 * Description :  This function sends notifications to MSR and RM
 *                 for the Ipv6Addr Table.
 *
 * Output      :  None.
 *
 * Returns     :  IPVX_SUCCESS or IPVX_FAILURE
 *
 * ****************************************************************************/

VOID
IncMsrForIpv6AddrTable (INT4 i4Index, tSNMP_OCTET_STRING_TYPE * Ipv6Address,
                        INT4 i4PrefixLen, CHR1 cDatatype, VOID *pSetVal,
                        UINT4 *pu4ObjectId, UINT4 u4OIdLen, UINT1 IsRowStatus)
{
    tSNMP_OCTET_STRING_TYPE *pSetString;
    UINT4               u4SeqNum = IPVX_ZERO;
    INT4                i4SetValue = IPVX_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = pu4ObjectId;
    SnmpNotifyInfo.u4OidLen = u4OIdLen;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = IsRowStatus;
    SnmpNotifyInfo.pLockPointer = Ip6Lock;
    SnmpNotifyInfo.pUnLockPointer = Ip6UnLock;
    SnmpNotifyInfo.u4Indices = IPVX_THREE;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

    switch (cDatatype)
    {
        case 'i':
            i4SetValue = *(INT4 *) pSetVal;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %s %i %i",
                              i4Index, Ipv6Address, i4PrefixLen, i4SetValue));
            break;

        case 's':
            pSetString = (tSNMP_OCTET_STRING_TYPE *) pSetVal;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %s %i %s", i4Index,
                              Ipv6Address, i4PrefixLen, pSetString));
            break;
    }
#ifndef ISS_WANTED
    UNUSED_PARAM (i4Index);
    UNUSED_PARAM (Ipv6Address);
    UNUSED_PARAM (i4PrefixLen);
#endif
    return;
}

/****************************************************************************
 *  Function    :  IncMsrForIp6PolicyPrefixTable
 *
 *   Input      :   i4IfIndex   - The interface index where the entry is added
 *                  Ipv6Address - IP address
 *                  i4PrefixLen - Length of IPV6 address 
 *                  cDataType   - Datatype of the configured value.
 *                  pSetVal     - Configured value that has to be notified.
 *                  pu4ObjectId - Object ID.
 *                  u4OIDLen    - Object Id length.
 *                  IsRowStatus - Rowstatus (TRUE/FALSE)
 *
 * Description :  This function sends notifications to MSR and RM
 *                 for the Policy Prefix Table.
 *
 * Output      :  None.
 *
 * Returns     :  IPVX_SUCCESS or IPVX_FAILURE
 *
 * ****************************************************************************/

/****************************************************************************
 *  Function    :  IncMsrForIpv6AddrProfileTable 
 *
 *   Input      :   u4ProfileIndex - Index of AddrProfileTable
 *                  cDataType   - Datatype of the configured value.
 *                  pSetVal     - Configured value that has to be notified.
 *                  pu4ObjectId - Object ID.
 *                  u4OIDLen    - Object Id length.
 *                  IsRowStatus - Rowstatus (TRUE/FALSE)
 *
 * Description :  This function sends notifications to MSR and RM
 *                 for the Ipv6AddrProfile Table.
 *
 * Output      :  None.
 *
 * Returns     :  IPVX_SUCCESS or IPVX_FAILURE
 *
 * ****************************************************************************/

VOID
IncMsrForIpv6AddrProfileTable (UINT4 u4ProfileIndex, CHR1 cDatatype,
                               VOID *pSetVal, UINT4 *pu4ObjectId,
                               UINT4 u4OIdLen)
{
    UINT4               u4SeqNum = IP6_ZERO;
    INT4                i4SetValue = IP6_ZERO;
    UINT4               u4SetValue = IP6_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = pu4ObjectId;
    SnmpNotifyInfo.u4OidLen = u4OIdLen;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = Ip6Lock;
    SnmpNotifyInfo.pUnLockPointer = Ip6UnLock;
    SnmpNotifyInfo.u4Indices = IP6_ONE;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

    switch (cDatatype)
    {
        case 'i':
            i4SetValue = *(INT4 *) pSetVal;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %i",
                              u4ProfileIndex, i4SetValue));
            break;
        case 'u':
            u4SetValue = *(INT4 *) pSetVal;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u",
                              u4ProfileIndex, u4SetValue));
            break;
    }
#ifndef ISS_WANTED
    UNUSED_PARAM (u4ProfileIndex);
#endif
    return;
}

/****************************************************************************
 *  Function    :  IncMsrForIpv6PmtuTable     
 *
 *   Input      :   u4ContextId - The context in which the pmtu entry is added
 *                  pPmtuDest   - Pmtu Entry Destination address
 *                  i4SetVal    - Configured value that has to be notified.
 *                  pu4ObjectId - Object ID.
 *                  u4OIDLen    - Object Id length.
 *
 * Description :  This function sends notifications to MSR and RM
 *                 for the FsMIIpv6PmtuTable.
 *
 * Output      :  None.
 *
 * Returns     :  None                            
 *
 * ****************************************************************************/
VOID
IncMsrForIpv6PmtuTable (UINT4 u4ContextId, tSNMP_OCTET_STRING_TYPE * pIpAddress,
                        INT4 i4SetVal, UINT4 *pu4ObjectId, UINT4 u4OIdLen)
{
    UINT4               u4SeqNum = IP6_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = pu4ObjectId;
    SnmpNotifyInfo.u4OidLen = u4OIdLen;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = Ip6Lock;
    SnmpNotifyInfo.pUnLockPointer = Ip6UnLock;
    SnmpNotifyInfo.u4Indices = IP6_TWO;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %s %i",
                      (INT4) u4ContextId, pIpAddress, i4SetVal));
#ifndef ISS_WANTED
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (pIpAddress);
    UNUSED_PARAM (i4SetVal);
#endif
    return;
}

/****************************************************************************
 *  Function    :  IncMsrForIpv6IfTable
 *
 *   Input      :   i4IfIndex   - The interface index where the entry is added
 *                  cDataType   - Datatype of the configured value.
 *                  pSetVal     - Configured value that has to be notified.
 *                  pu4ObjectId - Object ID.
 *                  u4OIDLen    - Object Id length.
 *
 * Description :  This function sends notifications to MSR and RM
 *                 for the Ipv6 IfTable.
 *
 * Output      :  None.
 *
 * Returns     :  None                            
 *
 * ****************************************************************************/

VOID
IncMsrForIpv6IfTable (INT4 i4IfIndex, CHR1 cDatatype, VOID *pSetVal,
                      UINT4 *pu4ObjectId, UINT4 u4OIdLen)
{
    tSNMP_OCTET_STRING_TYPE *pSetString;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = IP6_ZERO;
    INT4                i4SetValue;
    UINT4               u4SetValue;

    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = pu4ObjectId;
    SnmpNotifyInfo.u4OidLen = u4OIdLen;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = Ip6Lock;
    SnmpNotifyInfo.pUnLockPointer = Ip6UnLock;
    SnmpNotifyInfo.u4Indices = IP6_ONE;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

    switch (cDatatype)
    {
        case 'i':
            i4SetValue = *(INT4 *) pSetVal;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", i4IfIndex, i4SetValue));
            break;

        case 'u':
            u4SetValue = *(UINT4 *) pSetVal;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u", i4IfIndex, u4SetValue));
            break;

        case 's':
            pSetString = (tSNMP_OCTET_STRING_TYPE *) pSetVal;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %s", i4IfIndex, pSetString));
            break;
    }
#ifndef ISS_WANTED
    UNUSED_PARAM (i4IfIndex);
#endif
    return;
}

/****************************************************************************
 *  Function    :  IncMsrForIp6PrefixTable
 *
 *   Input      :   i4IfIndex   - The interface index where the entry is added
 *                  i4PrefixLen - Prefix address length
 *                  pIpAddress  - IP address of the prefix entry
 *                  i4SetVal    - Configured value that has to be notified.
 *                  pu4ObjectId - Object ID.
 *                  u4OIDLen    - Object Id length.
 *                  IsRowStatus - Rowstatus (TRUE/FALSE)
 *
 * Description :  This function sends notifications to MSR and RM
 *                 for the fsipv6PrefixTable
 *
 * Output      :  None.
 *
 * Returns     :  None                            
 *
 * ****************************************************************************/

VOID
IncMsrForIp6PrefixTable (INT4 i4IfIndex, INT4 i4PrefixLen,
                         tSNMP_OCTET_STRING_TYPE * pIpAddress,
                         INT4 i4SetVal, UINT4 *pu4ObjectId,
                         UINT4 u4OIdLen, UINT1 IsRowStatus)
{
    UINT4               u4SeqNum = IP6_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = pu4ObjectId;
    SnmpNotifyInfo.u4OidLen = u4OIdLen;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = IsRowStatus;
    SnmpNotifyInfo.pLockPointer = Ip6Lock;
    SnmpNotifyInfo.pUnLockPointer = Ip6UnLock;
    SnmpNotifyInfo.u4Indices = IPVX_THREE;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %s %i %i",
                      i4IfIndex, pIpAddress, i4PrefixLen, i4SetVal));
#ifndef ISS_WANTED
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (pIpAddress);
    UNUSED_PARAM (i4PrefixLen);
    UNUSED_PARAM (i4SetVal);
#endif
    return;
}

/****************************************************************************
 *  Function    :  IncMsrForIpv6IfZoneMapTable
 *
 *   Input      :   i4IfIndex   - The interface index where the entry is added
 *                  cDataType   - Datatype of the configured value
 *                  pSetVal     - Configured value that has to be notified.
 *                  pu4ObjectId - Object ID.
 *                  u4OIDLen    - Object Id length.
 *                  IsRowStatus - Rowstatus (TRUE/FALSE)
 *
 * Description :  This function sends notifications to MSR and RM
 *                 for the fsipv6PrefixTable
 *
 * Output      :  None.
 *
 * Returns     :  None
 *
 ****************************************************************************/
VOID
IncMsrForIpv6IfZoneMapTable (INT4 i4IfIndex, CHR1 cDatatype, VOID *pSetVal,
                             UINT4 *pu4ObjectId, UINT4 u4OIdLen,
                             UINT1 IsRowStatus)
{
    UINT4               u4SeqNum = IPVX_ZERO;
    INT4                i4SetValue = IPVX_ZERO;
    tSNMP_OCTET_STRING_TYPE *pSetString;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = pu4ObjectId;
    SnmpNotifyInfo.u4OidLen = u4OIdLen;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = IsRowStatus;
    SnmpNotifyInfo.pLockPointer = Ip6Lock;
    SnmpNotifyInfo.pUnLockPointer = Ip6UnLock;
    SnmpNotifyInfo.u4Indices = IP6_ONE;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

    switch (cDatatype)
    {
        case 'i':
            i4SetValue = *(INT4 *) pSetVal;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", i4IfIndex, i4SetValue));
            break;
        case 's':
            pSetString = (tSNMP_OCTET_STRING_TYPE *) pSetVal;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %s", i4IfIndex, pSetString));
            break;
    }
#ifndef ISS_WANTED
    UNUSED_PARAM (i4IfIndex);
#endif
    return;
}

/****************************************************************************
 *  Function    :  IncMsrForIpv6ScopeZoneTable
 *
 *   Input      :   i4IfIndex   - The interface index where the entry is added
 *                  cDataType   - Datatype of the configured value
 *                  pSetVal     - Configured value that has to be notified.
 *                  pu4ObjectId - Object ID.
 *                  u4OIDLen    - Object Id length.
 *                  IsRowStatus - Rowstatus (TRUE/FALSE)
 *
 * Description :  This function sends notifications to MSR and RM
 *                 for the fsipv6PrefixTable
 *
 * Output      :  None.
 *
 * Returns     :  None
 *
 ****************************************************************************/
VOID
IncMsrForIpv6ScopeZoneTable (UINT4 u4ContextId,
                             tSNMP_OCTET_STRING_TYPE * pFsipv6ScopeZoneName,
                             INT4 i4SetVal, UINT4 *pu4ObjectId, UINT4 u4OIdLen,
                             UINT1 IsRowStatus)
{
    UINT4               u4SeqNum = IP6_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = pu4ObjectId;
    SnmpNotifyInfo.u4OidLen = u4OIdLen;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = IsRowStatus;
    SnmpNotifyInfo.pLockPointer = Ip6Lock;
    SnmpNotifyInfo.pUnLockPointer = Ip6UnLock;
    SnmpNotifyInfo.u4Indices = IP6_TWO;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %s %i",
                      (INT4) u4ContextId, pFsipv6ScopeZoneName, i4SetVal));

#ifndef ISS_WANTED
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (pFsipv6ScopeZoneName);
    UNUSED_PARAM (i4SetVal);
#endif

    return;
}

/****************************************************************************
 *  Function    :  Ipv6UtilGetCxtEntryFromCxtId
 *
 *   Input      :   u4ContextId - The context Id 
 *
 * Description :  This function to get IP6 context entry from context id
 *
 * Output      :  None.
 *
 * Returns     :  pIp6CxtEntry - Pointer to the IP6 context entry
 *
 * ****************************************************************************/

tIp6Cxt            *
Ipv6UtilGetCxtEntryFromCxtId (UINT4 u4ContextId)
{
    tIp6Cxt            *pIp6CxtEntry = NULL;

    if (u4ContextId >= IP6_MAX_INSTANCES)
    {
        return NULL;
    }
    pIp6CxtEntry = gIp6GblInfo.apIp6Cxt[u4ContextId];
    return pIp6CxtEntry;
}

/****************************************************************************
 *  Function    :  Ipv6UtilGetCurrentCxtEntry
 *
 *   Input      :   u4ContextId - The context Id 
 *
 * Description :  This function to get IP6 context entry from context id
 *
 * Output      :  None.
 *
 * Returns     :  pIp6CxtEntry - Pointer to the IP6 context entry
 *
 * ****************************************************************************/

tIp6Cxt            *
Ipv6UtilGetCurrentCxtEntry (VOID)
{
    tIp6Cxt            *pIp6CxtEntry = NULL;

    pIp6CxtEntry = gIp6GblInfo.pIp6CurrCxt;
    return pIp6CxtEntry;
}

/****************************************************************************
 *  Function    :  Ipv6UtilGetCxtEntryFromIfIndex
 *
 *   Input      :  u4IfIndex - Interface Index
 *
 * Description :  This function to get IP6 context entry from Interface Index
 *
 * Output      :  None.
 *
 * Returns     :  pIp6CxtEntry - Pointer to the IP6 context entry
 *
 * ****************************************************************************/

tIp6Cxt            *
Ipv6UtilGetCxtEntryFromIfIndex (UINT4 u4IfIndex)
{
    tIp6If             *pIf6 = NULL;

    pIf6 = Ip6ifGetEntry (u4IfIndex);
    if (pIf6 == NULL)
    {
        return NULL;
    }

    return (pIf6->pIp6Cxt);
}

/****************************************************************************
 *  Function    :  Ip6VRExists                  
 *
 *  Input       :  u4ContextId  - Context Identifier 
 *
 *  Description :  This function verifies whether the given context is 
 *                  existing in the IP6 module.
 *
 * Output      :  None.
 *
 * Returns     :  IPV6_SUCCESS or IPV6_FAILURE
 *
 * ****************************************************************************/
INT4
Ip6VRExists (UINT4 u4ContextId)
{
    if (VcmIsL3VcExist (u4ContextId) == VCM_FALSE)
    {
        return IP6_FAILURE;
    }
    if (gIp6GblInfo.apIp6Cxt[u4ContextId] == NULL)
    {
        return IP6_FAILURE;
    }
    return IP6_SUCCESS;
}

/******************************************************************************
 *Description         : This functions gets the context id associated with the 
 *                      the given IPv6 Interface Index
 *
 * Input(s)           : u4IfIndex - Ipv6 Interface Index
 *
 * Output(s)          : *pu4ContextId - The VR to which the interface is mapped
 *
 * Returns            : IP6_SUCCESS or IP6_FAILURE
 ******************************************************************************/
INT4
Ip6GetCxtId (UINT4 u4IfIndex, UINT4 *pu4ContextId)
{
    tIp6If             *pIf6 = NULL;

    pIf6 = Ip6ifGetEntry (u4IfIndex);

    if ((pIf6 == NULL) || (pIf6->pIp6Cxt == NULL))
    {
        return IP6_FAILURE;
    }
    *pu4ContextId = pIf6->pIp6Cxt->u4ContextId;
    return IP6_SUCCESS;
}

/****************************************************************************
 *  Function    :  Ip6UtilGetFirstCxtId
 *
 *   Input      :   None
 *
 * Description :  This function gives first valid context identifier
 *
 * Output      :  pu4Ip6CxtId - The context Id
 *
 * Returns     :  IPVX_SUCCESS or IPVX_FAILURE
 *
 * ****************************************************************************/

INT4
Ip6UtilGetFirstCxtId (UINT4 *pu4Ip6CxtId)
{
    UINT4               u4Ip6CxtId;

    for (u4Ip6CxtId = 0; u4Ip6CxtId < IP6_MAX_INSTANCES; u4Ip6CxtId++)
    {
        if ((gIp6GblInfo.apIp6Cxt[u4Ip6CxtId]) != NULL)
        {
            *pu4Ip6CxtId = u4Ip6CxtId;
            return IP6_SUCCESS;

        }
    }
    return IP6_FAILURE;
}

/****************************************************************************
 *  Function    :  Ip6UtilGetNextCxtId
 *
 *   Input      :   u4Ip6CxtId - The context Id 
 *
 * Description :  This function gives the next valid context id
 *
 * Output      :  pu4NextIp6CxtId - The Next Context id
 *
 * Returns     :  IPVX_SUCCESS or IPVX_FAILURE
 *
 * ****************************************************************************/

INT4
Ip6UtilGetNextCxtId (UINT4 u4Ip6CxtId, UINT4 *pu4NextIp6CxtId)
{
    UINT4               u4CurIp6CxtId;

    u4Ip6CxtId++;
    for (u4CurIp6CxtId = u4Ip6CxtId; u4CurIp6CxtId < IP6_MAX_INSTANCES;
         u4CurIp6CxtId++)
    {
        if ((gIp6GblInfo.apIp6Cxt[u4CurIp6CxtId]) != NULL)
        {
            *pu4NextIp6CxtId = u4CurIp6CxtId;
            return IP6_SUCCESS;

        }
    }
    return IP6_FAILURE;
}

/***************************************************************/
/*  Function Name   : Ip6SelectContext                         */
/*  Description     : Set the current IP6 context pointer      */
/*  Input(s)        : u4UdpContextId - UDP Context Identifier  */
/*  Output(s)       : None                                     */
/*  Returns         : SUCCESS/FAILURE                          */
/***************************************************************/
INT4
Ip6SelectContext (UINT4 u4ContextId)
{
    if (VcmIsL3VcExist (u4ContextId) != VCM_FALSE)
    {
        gIp6GblInfo.pIp6CurrCxt = Ipv6UtilGetCxtEntryFromCxtId (u4ContextId);
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/***************************************************************/
/*  Function Name   : Ip6ReleaseContext                        */
/*  Description     : Reset the current IP6 context pointer    */
/*  Input(s)        : None                                     */
/*  Output(s)       : None                                     */
/*  Returns         : None                                     */
/***************************************************************/
VOID
Ip6ReleaseContext (VOID)
{
    gIp6GblInfo.pIp6CurrCxt = NULL;
    return;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : Ip6DeleteContextInfo                               */
/*                                                                           */
/*     DESCRIPTION      : This function clears the context based Ip6         */
/*                        configurations done for the given context.         */
/*                                                                           */
/*     INPUT            : u4ContextId    - Context-Id.                       */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : IP6_SUCCESS / IP6_FAILURE;                         */
/*                                                                           */
/*****************************************************************************/

INT4
Ip6DeleteContextInfo (UINT4 u4ContextId)
{
    INT4                i4RetVal = SNMP_FAILURE;

    i4RetVal = nmhValidateIndexInstanceFsMIIpv6ContextTable (u4ContextId);
    if (i4RetVal == SNMP_FAILURE)
    {
        return IP6_FAILURE;
    }

    /* Clear the global config table */
    Ip6UtilClearContextInfo ((INT4) u4ContextId);

    /* Clear the PMTU table entries */
    Ip6ClearPmtuTable (u4ContextId);
    Ip6ClearNdProxyTable (u4ContextId);
    IP6_TASK_UNLOCK ();
    Ip6ClearPreferenceTable (u4ContextId);
    IpvxClearIp6ContextEntries (u4ContextId);
    IP6_TASK_LOCK ();

    return IP6_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : Ip6ClearPmtuTable                                  */
/*                                                                           */
/*     DESCRIPTION      : This function clears the Pmtu information          */
/*                        configurations done for the given context.         */
/*                                                                           */
/*     INPUT            : u4ContextId    - Context-Id.                       */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/

VOID
Ip6ClearPmtuTable (UINT4 u4ContextId)
{
    UINT4               u4NxtCxtId = 0;
    tSNMP_OCTET_STRING_TYPE Dest;
    tSNMP_OCTET_STRING_TYPE NxtDest;
    UINT1               au1Dest[IPVX_IPV6_ADDR_LEN];
    UINT1               au1NxtDest[IPVX_IPV6_ADDR_LEN];

    MEMSET (au1Dest, IP6_ZERO, IPVX_IPV6_ADDR_LEN);
    MEMSET (au1NxtDest, IP6_ZERO, IPVX_IPV6_ADDR_LEN);
    Dest.pu1_OctetList = au1Dest;
    NxtDest.pu1_OctetList = au1NxtDest;
    Dest.i4_Length = IP6_ZERO;
    NxtDest.i4_Length = IP6_ZERO;

    while (nmhGetNextIndexFsMIIpv6PmtuTable ((INT4) u4ContextId,
                                             (INT4 *) &u4NxtCxtId,
                                             &Dest, &NxtDest) != SNMP_FAILURE)
    {
        if (u4ContextId != u4NxtCxtId)
        {
            break;
        }
        Ip6UtilClearPmtuTable (u4ContextId, &NxtDest);
        MEMCPY (Dest.pu1_OctetList, NxtDest.pu1_OctetList, IPVX_IPV6_ADDR_LEN);
        Dest.i4_Length = NxtDest.i4_Length;
    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : Ip6ClearPreferenceTable                            */
/*                                                                           */
/*     DESCRIPTION      : This function clears the route preference info     */
/*                        configurations done for the given context.         */
/*                                                                           */
/*     INPUT            : u4ContextId    - Context-Id.                       */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/

VOID
Ip6ClearPreferenceTable (UINT4 u4ContextId)
{
    INT4                i4NxtCxtId = 0;
    INT4                i4Protocol = 0;
    INT4                i4NxtProtocol = 0;
    INT4                i4PrefVal = 0;

    RTM6_TASK_LOCK ();
    while (nmhGetNextIndexFsMIIpv6PrefTable ((INT4) u4ContextId, &i4NxtCxtId,
                                             i4Protocol, &i4NxtProtocol)
           != SNMP_FAILURE)
    {
        if ((INT4) u4ContextId < i4NxtCxtId)
        {
            break;
        }
        i4Protocol = i4NxtProtocol;

        switch (i4NxtProtocol)
        {
            case IP6_OTHER_PROTOID:
                i4PrefVal = IP6_PREFERENCE_OTHERS;
                break;
            case IP6_LOCAL_PROTOID:
                i4PrefVal = IP6_PREFERENCE_LOCAL;
                break;
            case IP6_NETMGMT_PROTOID:
                i4PrefVal = IP6_PREFERENCE_NETMGMT;
                break;
            case IP6_NDISC_PROTOID:
                i4PrefVal = IP6_PREFERENCE_NDISC;
                break;
            case IP6_RIP_PROTOID:
                i4PrefVal = IP6_PREFERENCE_RIP;
                break;
            case IP6_OSPF_PROTOID:
                i4PrefVal = IP6_PREFERENCE_OSPF;
                break;
            case IP6_BGP_PROTOID:
                i4PrefVal = IP6_PREFERENCE_BGP;
                break;
            case IP6_IDRP_PROTOID:
                i4PrefVal = IP6_PREFERENCE_IDRP;
                break;
            case IP6_IGRP_PROTOID:
                i4PrefVal = IP6_PREFERENCE_IGRP;
                break;
        }
        Ip6UtilClearPreferenceTable (i4NxtCxtId, i4NxtProtocol, i4PrefVal);
    }
    RTM6_TASK_UNLOCK ();
    return;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : Ip6ClearNdProxyTable                               */
/*                                                                           */
/*     DESCRIPTION      : This function clears the Neighbor proxy information*/
/*                        configurations done for the given context.         */
/*                                                                           */
/*     INPUT            : u4ContextId    - Context-Id.                       */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/

VOID
Ip6ClearNdProxyTable (UINT4 u4ContextId)
{
    UINT4               u4NxtCxtId = 0;
    tSNMP_OCTET_STRING_TYPE Addr;
    tSNMP_OCTET_STRING_TYPE NxtAddr;
    UINT1               au1Addr[IPVX_IPV6_ADDR_LEN];
    UINT1               au1NxtAddr[IPVX_IPV6_ADDR_LEN];

    MEMSET (au1Addr, IP6_ZERO, IPVX_IPV6_ADDR_LEN);
    MEMSET (au1NxtAddr, IP6_ZERO, IPVX_IPV6_ADDR_LEN);
    Addr.pu1_OctetList = au1Addr;
    NxtAddr.pu1_OctetList = au1NxtAddr;
    Addr.i4_Length = IP6_ZERO;
    NxtAddr.i4_Length = IP6_ZERO;

    while (nmhGetNextIndexFsMIIpv6NDProxyListTable ((INT4) u4ContextId,
                                                    (INT4 *) &u4NxtCxtId,
                                                    &Addr, &NxtAddr)
           != SNMP_FAILURE)
    {
        if (u4ContextId != u4NxtCxtId)
        {
            break;
        }
        Ip6UtilClearNdProxyTable (u4ContextId, &NxtAddr);
        MEMCPY (Addr.pu1_OctetList, NxtAddr.pu1_OctetList, IPVX_IPV6_ADDR_LEN);
        Addr.i4_Length = NxtAddr.i4_Length;
    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : Ip6DeleteInterfaceInfo                             */
/*                                                                           */
/*     DESCRIPTION      : This function clears the interface based Ip6       */
/*                        configurations done for the given interface.       */
/*                                                                           */
/*     INPUT            : u4ContextId    - Context-Id.                       */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : IP6_SUCCESS / IP6_FAILURE;                         */
/*                                                                           */
/*****************************************************************************/

INT4
Ip6DeleteInterfaceInfo (UINT4 u4IfIndex)
{
    /* Clear the IP6 address table */
    Ip6ClearAddressTable (u4IfIndex);
    Ip6ClearPrefixTable (u4IfIndex);
    Ip6ClearPolicyPrefixTable (u4IfIndex);
    Ip6ClearInterfaceTable (u4IfIndex);
    IpvxClearIp6InterfaceEntries (u4IfIndex);
    return IP6_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : Ip6ClearAddressTable                               */
/*                                                                           */
/*     DESCRIPTION      : This function clears the interface address table   */
/*                        configurations done for the given interface.       */
/*                                                                           */
/*     INPUT            : u4ContextId    - Context-Id.                       */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/

VOID
Ip6ClearAddressTable (UINT4 u4IfIndex)
{
    INT4                i4NxtIndex = 0;
    INT4                i4PrefixLen = 0;
    INT4                i4NxtPrefixLen = 0;
    tSNMP_OCTET_STRING_TYPE Addr;
    tSNMP_OCTET_STRING_TYPE NxtAddr;
    UINT1               au1Addr[IPVX_IPV6_ADDR_LEN];
    UINT1               au1NxtAddr[IPVX_IPV6_ADDR_LEN];

    MEMSET (au1Addr, IP6_ZERO, IPVX_IPV6_ADDR_LEN);
    MEMSET (au1NxtAddr, IP6_ZERO, IPVX_IPV6_ADDR_LEN);
    Addr.pu1_OctetList = au1Addr;
    NxtAddr.pu1_OctetList = au1NxtAddr;
    Addr.i4_Length = IP6_ZERO;
    NxtAddr.i4_Length = IP6_ZERO;

    while (nmhGetNextIndexFsMIIpv6AddrTable ((INT4) u4IfIndex, &i4NxtIndex,
                                             &Addr, &NxtAddr, i4PrefixLen,
                                             &i4NxtPrefixLen) != SNMP_FAILURE)
    {
        if (i4NxtIndex > (INT4) u4IfIndex)
        {
            break;
        }

        MEMCPY (Addr.pu1_OctetList, NxtAddr.pu1_OctetList, IPVX_IPV6_ADDR_LEN);
        Addr.i4_Length = NxtAddr.i4_Length;
        i4PrefixLen = i4NxtPrefixLen;
        Ip6UtilClearAddressTable (i4NxtIndex, &NxtAddr, i4NxtPrefixLen);
    }
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : Ip6ClearPrefixTable                                */
/*                                                                           */
/*     DESCRIPTION      : This function clears the interface prefix table    */
/*                        configurations done for the given interface.       */
/*                                                                           */
/*     INPUT            : u4ContextId    - Context-Id.                       */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/

VOID
Ip6ClearPrefixTable (UINT4 u4IfIndex)
{
    INT4                i4NxtIndex = 0;
    INT4                i4PrefixLen = 0;
    INT4                i4NxtPrefixLen = 0;
    tSNMP_OCTET_STRING_TYPE Addr;
    tSNMP_OCTET_STRING_TYPE NxtAddr;
    UINT1               au1Addr[IPVX_IPV6_ADDR_LEN];
    UINT1               au1NxtAddr[IPVX_IPV6_ADDR_LEN];

    MEMSET (au1Addr, IP6_ZERO, IPVX_IPV6_ADDR_LEN);
    MEMSET (au1NxtAddr, IP6_ZERO, IPVX_IPV6_ADDR_LEN);
    Addr.pu1_OctetList = au1Addr;
    NxtAddr.pu1_OctetList = au1NxtAddr;
    Addr.i4_Length = IP6_ZERO;
    NxtAddr.i4_Length = IP6_ZERO;

    while (nmhGetNextIndexFsipv6PrefixTable ((INT4) u4IfIndex, &i4NxtIndex,
                                             &Addr, &NxtAddr, i4PrefixLen,
                                             &i4NxtPrefixLen) != SNMP_FAILURE)
    {
        if (i4NxtIndex > (INT4) u4IfIndex)
        {
            break;
        }

        MEMCPY (Addr.pu1_OctetList, NxtAddr.pu1_OctetList, IPVX_IPV6_ADDR_LEN);
        Addr.i4_Length = NxtAddr.i4_Length;
        i4PrefixLen = i4NxtPrefixLen;
        Ip6UtilClearPrefixTable (i4NxtIndex, &NxtAddr, i4NxtPrefixLen);
    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : Ip6ClearInterfaceTable                             */
/*                                                                           */
/*     DESCRIPTION      : This function clears the Ipv6 interface table      */
/*                        configurations done for the given interface.       */
/*                                                                           */
/*     INPUT            : u4IfIndex - Interface index.                       */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/

VOID
Ip6ClearInterfaceTable (UINT4 u4IfIndex)
{
    if (nmhValidateIndexInstanceFsMIIpv6IfTable ((INT4) u4IfIndex)
        == SNMP_FAILURE)
    {
        return;
    }

    Ip6ClearIfTable ((INT4) u4IfIndex);

    return;
}

/****************************************************************************
* Function     : Ip6GetDebugFlag
*
* Description  : This function returns the Ip6 Debug flag for the given
*                 context or returns the global Ip6 debug flag if the
*                 given context id is invalid context
*
* Input        : u4ContextId - context Id.
*
* Output       : None.
*
* Returns      : Debug Value.
*
***************************************************************************/
INT4
Ip6GetDebugFlag (UINT4 u4ContextId)
{
    if (u4ContextId == VCM_DEFAULT_CONTEXT)
    {
        return gIp6GblInfo.u4GlbIp6Dbg;
    }

    if ((u4ContextId < IP6_MAX_INSTANCES) &&
        (gIp6GblInfo.apIp6Cxt[u4ContextId] != NULL))
    {
        return gIp6GblInfo.apIp6Cxt[u4ContextId]->u4Ip6Dbg;

    }
    return 0;
}

/****************************************************************************
* Function     : Ipv6UtilGetConfigMethod
*
* Description  : This function gives the configuration method for the 
*                 given link local address.
*
* Input        : u4IfIndex   - The interface on which the address is configured
*                pFsipv6AddrAddress - The link local address  
*                u1PrefixLen - Prefix Length
*                u1AddressType - Address Type
*
* Output       : pu1CfgMethod - The configuration method.
*
* Returns      : Debug Value.
*
***************************************************************************/

VOID
Ipv6UtilGetConfigMethod (UINT4 u4IfIndex,
                         tSNMP_OCTET_STRING_TYPE * pFsipv6AddrAddress,
                         UINT1 u1PrefixLen, UINT1 u1AddressType,
                         UINT1 *pu1CfgMethod)
{
    tIp6LlocalInfo     *pLlocalInfo = NULL;
    tIp6AddrInfo       *pAddrInfo = NULL;
    tIp6Addr            Ip6Addr;

    *pu1CfgMethod = 0;

    MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));

    MEMCPY (&Ip6Addr, pFsipv6AddrAddress->pu1_OctetList, sizeof (tIp6Addr));

    if (u1AddressType == ADDR6_LLOCAL)
    {
        pLlocalInfo = Ip6AddrTblGetLlocalEntry (u4IfIndex, &Ip6Addr);

        if (pLlocalInfo != NULL)
        {
            *pu1CfgMethod = pLlocalInfo->u1ConfigMethod;
        }
    }
    else if (u1AddressType == ADDR6_UNICAST)
    {
        pAddrInfo = Ip6AddrTblGetEntry (u4IfIndex, &Ip6Addr, u1PrefixLen);

        if (pAddrInfo != NULL)
        {
            *pu1CfgMethod = pAddrInfo->u1ConfigMethod;
        }
    }

    return;

}

/******************************************************************************
 * Function Name    :   Ip6UtlConstructRpAddr
 * Description      :   This function can be used by netip6  
 *                      to validate and construct the RP address
 *                      embedded in the given multicast ipv6 address.
 * Inputs           :   pu1InterfaceId - Interface identifier
 *                      pMcastAddr  - IPv6 multicast address 
 *                      u1PrefixLen - Prefix Length
 *                      u1Status - RP_STATIC/RP_DYNAMIC
 * Outputs          :   Prefix - Ipv6 RP Address related information
 * Return Value     :   NETIPV6_SUCCESS or NETIPV6_FAILURE
 *******************************************************************************/
INT4
Ip6UtlConstructRpAddr (tIp6Addr * pMcastAddr, tIp6Addr * pPrefix,
                       UINT1 *pu1InterfaceId, UINT1 u1PrefixLen, UINT1 u1Status)
{
    UINT1               au1Ip6IfId[IP6_EUI_ADDRESS_LEN];
    UINT1               u1RpId = 0;

    if (!IS_ALL_RP_PREFIX_MULTI (pMcastAddr))
    {
        return NETIPV6_FAILURE;
    }

    u1RpId = IP6_GET_RIID_FROM_MCAST_ADDR (pMcastAddr);
    if (IS_IP6_RIID_IN_RANGE (u1RpId))
    {
        return NETIPV6_FAILURE;
    }
    MEMSET (&au1Ip6IfId, 0, IP6_EUI_ADDRESS_LEN);
    MEMCPY (&au1Ip6IfId[7], &u1RpId, sizeof (u1RpId));

    if ((u1Status == IP6_ADDR_RP_STATIC) && (pu1InterfaceId != NULL))
    {
        if (MEMCMP (au1Ip6IfId, pu1InterfaceId, IP6_EUI_ADDRESS_LEN) != 0)
        {
            return NETIPV6_FAILURE;
        }
    }

    MEMSET (pPrefix, 0, sizeof (tIp6Addr));
    MEMCPY (pPrefix, ((UINT1 *) (pMcastAddr) + 4),
            (u1PrefixLen / IP6_EUI_ADDRESS_LEN));
    MEMCPY (((UINT1 *) (pPrefix) + (IP6_EUI_ADDRESS_LEN)), au1Ip6IfId,
            IP6_EUI_ADDRESS_LEN);

    return NETIPV6_SUCCESS;
}

/******************************************************************************
 * Function Name    :   Ip6UtlCheckIfForEmbdRp
 * Description      :   This function can be used by netip6  
 *                      to validate whether embedded flag is enabled on the
 *                      interface. This function can also be used by netip6
 *                      to check whether a particular interface exists or not.
 * Inputs           :   pIf6 - Interface node
 *                      pPrefix - constructed RP address
 *                      u1PrefixLen - Prefix Length of Rp address
 * Outputs          :   pu1EmbdRpFlag - sets to IP6_TRUE if RP is enabled in
 *                         that interface, otherwise IP6_FALSE
 * Return Value     :   NETIPV6_SUCCESS/NETIPV6_FAILURE
 *******************************************************************************/
INT4
Ip6UtlCheckIfForEmbdRp (tIp6If * pIf6, tIp6Addr * pPrefix, UINT1 u1PrefixLen,
                        UINT1 *pu1EmbdRpFlag)
{
    tIp6PrefixNode     *pPrefixInfo = NULL;
    TMO_SLL_Scan (&pIf6->prefixlist, pPrefixInfo, tIp6PrefixNode *)
    {
        if ((Ip6AddrMatch (&(pPrefixInfo->Ip6Prefix),
                           pPrefix, u1PrefixLen) == TRUE) &&
            (u1PrefixLen == pPrefixInfo->u1PrefixLen))
        {
            if (pPrefixInfo->u1EmbdRpValid == IP6_TRUE)
            {
                /*Given prefix (Embedded RP unicast prefix) belongs to the self node,
                 *since it is in the prefix list of the given interface index */
                *pu1EmbdRpFlag = IP6_TRUE;
            }
            else
            {
                *pu1EmbdRpFlag = IP6_FALSE;
            }
            return NETIPV6_SUCCESS;
        }
    }
    return NETIPV6_FAILURE;
}

/******************************************************************************
 * Function Name    :   Ip6UtlFillRpInfo
 * Description      :   This function can be used by netip6  
 *                      to copy RP details from the given inputs.
 * Inputs           :   pIf6 - Interface node
 *                      pPrefix - constructed RP address
 *                      u1PrefixLen - Prefix Length of Rp address
 * Outputs          :   pIp6RpAddrInfo - RP Address related information
 * Return Value     :   NETIPV6_SUCCESS or NETIPV6_FAILURE
 *******************************************************************************/
INT4
Ip6UtlFillRpInfo (tIp6Addr * pPrefix, UINT4 u4OutIfIndex,
                  UINT1 u1IsSelfNode, tIp6RpAddrInfo * pIp6RpAddrInfo)
{
    if (pIp6RpAddrInfo != NULL)
    {
        pIp6RpAddrInfo->u4OutIfIndex = u4OutIfIndex;
        MEMCPY (&(pIp6RpAddrInfo->RpAddr), pPrefix, IP6_ADDR_SIZE);
        pIp6RpAddrInfo->u1IsSelfNode = u1IsSelfNode;
        return NETIPV6_SUCCESS;
    }
    return NETIPV6_FAILURE;
}

/******************************************************************************
 * Function Name    :   Ip6UtlCheckRouteForEmbdRpInCxt
 * Description      :   This function can be used by netip6  
 *                      to copy validate the presence of route info 
 *                      for the given RP address.
 * Inputs           :   u4ContextId  - Context Identifier
 *                      pPrefix - constructed RP address
 * Outputs          :   pIp6RpAddrInfo - RP Address related information
 * Return Value     :   NETIPV6_SUCCESS or NETIPV6_FAILURE
 *******************************************************************************/
INT4
Ip6UtlCheckRouteForEmbdRpInCxt (UINT4 u4ContextId, tIp6Addr * pPrefix,
                                UINT4 *pu4OutIfIndex)
{
    tNetIpv6RtInfo      NetIp6RtInfo;
    tNetIpv6RtInfoQueryMsg RtQuery;

    MEMSET (&NetIp6RtInfo, 0, sizeof (tNetIpv6RtInfo));
    MEMSET (&RtQuery, 0, sizeof (tNetIpv6RtInfoQueryMsg));

    RtQuery.u4ContextId = u4ContextId;
    RtQuery.u1QueryFlag = RTM6_QUERIED_FOR_NEXT_HOP;
    Ip6AddrCopy (&(NetIp6RtInfo.Ip6Dst), pPrefix);
    NetIp6RtInfo.u1Prefixlen = IP6_ADDR_MAX_PREFIX;

    if (NetIpv6GetRoute (&RtQuery, &NetIp6RtInfo) == NETIPV6_SUCCESS)
    {
        *pu4OutIfIndex = NetIp6RtInfo.u4Index;
        return NETIPV6_SUCCESS;
    }
    return NETIPV6_FAILURE;
}

/******************************************************************************
 * Function Name    :   IPv6CheckForCacheForAddr
 * Description      :   This function checks if the ND cache exists for the next hop.
 *
 * Inputs           :   u4IfIndex  - Interface Index
 *                      pDest  - Dest IPv6 ADDR
 * Return Value     :   IP6_SUCCESS or IP6_FAILURE
 *******************************************************************************/

INT4
Ip6CheckForCacheForAddr (UINT4 u4IfIndex, tIp6Addr * pDest)
{
    tIp6If             *pIf6 = NULL;
    tNd6CacheEntry     *pNd6cEntry = NULL;

    pIf6 = IP6_INTERFACE (u4IfIndex);

    if (pIf6 == NULL)
    {
        return IP6_FAILURE;
    }

    pNd6cEntry = Nd6IsCacheForAddr (pIf6, pDest);
    if (pNd6cEntry != NULL)
    {
        return IP6_SUCCESS;
    }
    return IP6_FAILURE;
}

/******************************************************************************
 * Function Name    :   Ip6SendNS
 * Description      :   This function checks if the ND cache exists or It will 
 *                      Send New NS packet for Next hop.
 *
 * Inputs           :   pBuf - Buffer for the DS
 *******************************************************************************/
VOID
Ip6SendNS (tCRU_BUF_CHAIN_HEADER * pBuf)
{
    tIp6Addr           *pNextHop = NULL;
    tNdData            *pNdData = NULL;
    tIp6If             *pIf6 = NULL;
    tNd6CacheEntry     *pNd6cEntry = NULL;
    tNd6CacheEntry     *pNd6c = NULL;

    pNdData = (tNdData *) IP6_GET_MODULE_DATA_PTR (pBuf);

    if (Ip6ifEntryExists (pNdData->u4IfIndex) == IP6_SUCCESS)
    {
        pIf6 = IP6_INTERFACE (pNdData->u4IfIndex);
        pNextHop = &(pNdData->NextHop);

        pNd6cEntry = Nd6IsCacheForAddr (pIf6, pNextHop);

        if ((pNd6cEntry != NULL)
            && ((pNd6cEntry->u1ReachState == ND6C_REACHABLE)
                || (pNd6cEntry->u1ReachState == ND6C_STATIC)))
        {

            /*Nothing to be done */
        }
        else
        {

            if (pNd6cEntry == NULL)
            {
                Nd6Resolve (NULL, pIf6, pNextHop, NULL, 0);
            }
            else
            {

                pNd6c = Nd6LookupCache (pIf6, pNextHop);

                if (pNd6c)
                {

                    Nd6UpdateLastUseTime (pNd6c);

                }
            }
        }
    }
    if (pBuf != NULL)
    {
        Ip6BufRelease (VCM_INVALID_VC, pBuf, FALSE, ND6_MODULE);
    }

    return;

}

/******************************************************************************
 * Function Name    :   Ip6UtilClearContextInfo
 * Description      :   This function deletes the context related info.
 *
 * Inputs           :   i4ContextId - Context Id
 * Return Value     :   IP6_SUCCESS or IP6_FAILURE
 *******************************************************************************/
INT1
Ip6UtilClearContextInfo (INT4 i4ContextId)
{
    if (Ip6SelectContext ((UINT4) i4ContextId) == IP6_FAILURE)
    {
        return IP6_FAILURE;
    }

    if (gIp6GblInfo.pIp6CurrCxt == NULL)
    {
        return IP6_FAILURE;
    }
    gIp6GblInfo.pIp6CurrCxt->i4Nd6CacheMaxRetries =
        ND6_DEF_CACHE_SOLICIT_RETRIES;

    gIp6GblInfo.pIp6CurrCxt->u1PmtuEnable = (UINT1) IP6_PMTU_ENABLE;

    gIp6GblInfo.pIp6CurrCxt->u4CfgTimeOut =
        (IP6_PMTU_DEF_TIME_OUT_INTERVAL / 60);

    gIp6GblInfo.pIp6CurrCxt->u1JmbCfgStatus = (UINT1) IP6_JUMBO_ENABLE;

    gIp6GblInfo.pIp6CurrCxt->u4Ip6Dbg = IP6_ZERO;

    Ip6ReleaseContext ();
    return IP6_SUCCESS;

}

/******************************************************************************
 * Function Name    :   Ip6UtilClearPmtuTable
 * Description      :   This function clears the Pmtu information            
 *                      configurations done for the given context.         
 *
 * Inputs           :   i4ContextId - Context Id
 *                      pu4NxtDest
 * Return Value     :   IP6_SUCCESS or IP6_FAILURE
 *******************************************************************************/
INT1
Ip6UtilClearPmtuTable (UINT4 u4ContextId, tSNMP_OCTET_STRING_TYPE * pu4NxtDest)
{
    tPmtuEntry         *pPmtuEntry = NULL;

    if (Ip6SelectContext (u4ContextId) == IP6_FAILURE)
    {
        return IP6_FAILURE;
    }

    if (gIp6GblInfo.pIp6CurrCxt == NULL)
    {
        Ip6ReleaseContext ();
        return IP6_FAILURE;
    }

    ip6PmtuLookupEntryInCxt (gIp6GblInfo.pIp6CurrCxt->u4ContextId,
                             (tIp6Addr *) (VOID *) pu4NxtDest->
                             pu1_OctetList, &pPmtuEntry);

    if (pPmtuEntry != NULL)
    {                            /* Entry present */
        RBTreeRem (gIp6GblInfo.PmtuRBTable, pPmtuEntry);
        ip6PmtuReleaseEntry ((tPmtuEntry *) pPmtuEntry);
        Ip6ReleaseContext ();
        return IP6_SUCCESS;
    }
    else
    {
        Ip6ReleaseContext ();
        return IP6_SUCCESS;
    }
}

/******************************************************************************
 * Function Name    :   Ip6UtilClearPreferenceTable
 * Description      :   This function clears the route preference info
 *                      configurations done for the given context.
 *
 * Inputs           :   i4ContextId - Context Id
 *                      i4NxtProtocol
 *                      i4PrefVal
 * Return Value     :   IP6_SUCCESS or IP6_FAILURE
 *******************************************************************************/
INT1
Ip6UtilClearPreferenceTable (INT4 i4NxtCxtId, INT4 i4NxtProtocol,
                             INT4 i4PrefVal)
{
    UINT4               u4ContextId;

    if (UtilRtm6SetContext ((UINT4) i4NxtCxtId) == IP6_FAILURE)
    {
        return IP6_FAILURE;
    }
    u4ContextId = (UINT4) UtilRtm6GetCurrCxtId ();

    if (Rtm6ApiSetProtocolPrefInCxt (u4ContextId, (UINT4) i4NxtProtocol,
                                     (UINT4) i4PrefVal) == IP6_FAILURE)
    {
        return IP6_FAILURE;
    }
    UtilRtm6ResetContext ();
    return IP6_SUCCESS;
}

/******************************************************************************
 * Function Name    :   Ip6UtilClearNdProxyTable
 * Description      :   This function clears the route preference info
 *                      configurations done for the given context.
 *
 * Inputs           :   i4ContextId - Context Id
 *                      NxtAddr
 * Return Value     :   IP6_SUCCESS or IP6_FAILURE
 *******************************************************************************/
INT1
Ip6UtilClearNdProxyTable (UINT4 u4ContextId, tSNMP_OCTET_STRING_TYPE * NxtAddr)
{
    tIp6Addr            Ip6Addr;
    tNd6ProxyListNode  *pNd6ProxyListNode = NULL;
    tIp6Cxt            *pIp6Cxt;
    INT1                i1Found = 0;

    if (Ip6SelectContext (u4ContextId) == IP6_FAILURE)
    {
        return IP6_FAILURE;
    }
    pIp6Cxt = gIp6GblInfo.pIp6CurrCxt;
    Ip6AddrCopy (&Ip6Addr, (tIp6Addr *) (VOID *) NxtAddr->pu1_OctetList);

    /* Check for the Presence of the address in Proxy List */
    if ((pNd6ProxyListNode = Nd6ProxyCheckAddrInCxt (pIp6Cxt, Ip6Addr)) != NULL)
    {
        /* Entry exists! */
        i1Found = 1;
    }
    if (i1Found == 0)
    {
        Ip6ReleaseContext ();
        return IP6_FAILURE;
    }
    else
    {
        TMO_SLL_Delete (&(pIp6Cxt->Nd6ProxyList), &(pNd6ProxyListNode->Next));
        Ip6TmrStop (pIp6Cxt->u4ContextId, IP6_PROXY_ND_TIMER_ID,
                    gIp6GblInfo.Ip6TimerListId,
                    &pNd6ProxyListNode->ProxyTimer.appTimer);
        Ip6RelMem (pIp6Cxt->u4ContextId, (UINT2) gNd6GblInfo.i4Nd6ProxyListId,
                   (UINT1 *) pNd6ProxyListNode);

        Ip6ReleaseContext ();
    }
    return IP6_SUCCESS;
}

/******************************************************************************
 * Function Name    :   Ip6UtilClearAddressTable
 * Description      :   This function clears the interface address table
 *                      configurations done for the given context.
 *
 * Inputs           :   i4ContextId - Context Id
 *                      NxtAddr
 *                      i4NxtPrefixLen
 * Return Value     :   IP6_SUCCESS or IP6_FAILURE
 *******************************************************************************/
INT1
Ip6UtilClearAddressTable (INT4 i4NxtIndex, tSNMP_OCTET_STRING_TYPE * NxtAddr,
                          INT4 i4NxtPrefixLen)
{
    tIp6AddrInfo       *pAddrInfo = NULL;
    tIp6Addr            Fsipv6Address;

    MEMSET (&Fsipv6Address, 0, sizeof (tIp6Addr));
    MEMCPY (&Fsipv6Address, NxtAddr->pu1_OctetList,
            MEM_MAX_BYTES ((INT4) sizeof (tIp6Addr), NxtAddr->i4_Length));

    if (Ip6AddrType (&Fsipv6Address) == ADDR6_LLOCAL)
    {
        if (Ip6LlAddrDelete ((UINT4) i4NxtIndex, &Fsipv6Address) == IP6_FAILURE)
        {
            return IP6_FAILURE;
        }
    }
    else
    {
        pAddrInfo = Ip6AddrTblGetEntry ((UINT4) i4NxtIndex,
                                        &Fsipv6Address, (UINT1) i4NxtPrefixLen);
        /* Delete this Address */
#ifdef HA_WANTED
        HAPrcsAddrUpdate (pAddrInfo->pIf6, (UINT1 *) pAddrInfo,
                          ADMIN_INVALID, 0);
#endif
        UNUSED_PARAM (pAddrInfo);
        if (Ip6AddrDelete ((UINT4) i4NxtIndex, &Fsipv6Address,
                           (UINT1) i4NxtPrefixLen, 1) == IP6_FAILURE)
        {
            return IP6_FAILURE;
        }
    }
    return IP6_SUCCESS;
}

/******************************************************************************
 * Function Name    :   Ip6UtilClearPrefixTable
 * Description      :   This function clears the interface prefix table
 *                      configurations done for the given context.
 *
 * Inputs           :   i4ContextId - Context Id
 *                      NxtAddr
 *                      i4NxtPrefixLen
 * Return Value     :   IP6_SUCCESS or IP6_FAILURE
 *******************************************************************************/
INT1
Ip6UtilClearPrefixTable (INT4 i4NxtIndex, tSNMP_OCTET_STRING_TYPE * NxtAddr,
                         INT4 i4NxtPrefixLen)
{
    tIp6If             *pIf6 = NULL;
    pIf6 = Ip6ifGetEntry ((UINT4) i4NxtIndex);

    if (pIf6 == NULL)
    {
        return IP6_FAILURE;
    }

    if (Ip6SetFsipv6PrefixAdminStatus (i4NxtIndex,
                                       NxtAddr,
                                       i4NxtPrefixLen,
                                       IP6FWD_DESTROY) == IP6_FAILURE)
    {
        return IP6_FAILURE;
    }

    return IP6_SUCCESS;

}

/******************************************************************************
 * Function Name    :   Ip6ClearIfTable
 * Description      :   This function clears the Ipv6 interface table
 *                      configurations done for the given interface.
 *
 * Inputs           :   i4IfIndex - Interface Id
 * Return Value     :   IP6_FAILURE/IP6_SUCCESS
 *******************************************************************************/
INT1
Ip6ClearIfTable (INT4 i4IfIndex)
{
    tSNMP_OCTET_STRING_TYPE IfToken;
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4IfIndex);
    UINT1               au1Token[IP6_EUI_ADDRESS_LEN];

    MEMSET (au1Token, IP6_ZERO, IP6_EUI_ADDRESS_LEN);
    IfToken.pu1_OctetList = au1Token;
    IfToken.i4_Length = 0;
    if (pIf6 == NULL)
    {
        return IP6_FAILURE;
    }

    /*Clearing IfToken */
    if ((pIf6->u1IfType == IP6_FR_INTERFACE_TYPE) ||
        (pIf6->u1IfType == IP6_L3VLAN_INTERFACE_TYPE) ||
        (pIf6->u1IfType == IP6_PSEUDO_WIRE_INTERFACE_TYPE) ||
        (pIf6->u1IfType == IP6_ENET_INTERFACE_TYPE) ||
        (pIf6->u1IfType == IP6_LAGG_INTERFACE_TYPE) ||
        (pIf6->u1IfType == IP6_L3SUB_INTF_TYPE))
    {
        pIf6->u1TokLen = (UINT1) IfToken.i4_Length;
        MEMCPY (pIf6->ifaceTok, IfToken.pu1_OctetList, IfToken.i4_Length);
    }

    /*Clearing IfHopLimit */
    pIf6->u1Hoplmt = (UINT1) IP6_IF_DEF_HOPLMT;

    /*Clearing IfDefRouterTime */
    pIf6->u2Deftime = (UINT2) IP6_IF_DEF_DEFTIME;

    /*Clearing IfReachableTime */
    pIf6->u4Reachtime = (UINT4) (IP6_IF_DEF_REACHTIME);

    /*Clearing IfRetransmitTime */
    pIf6->u4Rettime = (UINT4) (IP6_IF_DEF_RETTIME * 1000);

    OsixGetSysTime (&gIp6GblInfo.u4Ip6IfTableLastChange);

    /*Clearing IfDelayProbeTime */
    pIf6->u4Pdelaytime = (UINT4) IP6_IF_DEF_PDTIME;

    /*Clearing IfPrefixAdvStatus */
    pIf6->u1PrefAdv = IP6_IF_PREFIX_ADV;

    /*Clearing IfMinRouterAdvTime */
    pIf6->u4MinRaTime = (UINT4) IP6_IF_DEF_MIN_RA_TIME;

    /*Clearing IfMaxRouterAdvTime */
    pIf6->u4MaxRaTime = (UINT4) IP6_IF_DEF_MAX_RA_TIME;

    /*Clearing IfDADRetries */
    pIf6->u2DadSend = (UINT2) IP6_IF_DEF_DAD_SEND;

    /*Clearing IfRouterAdvStatus */
    if (Ip6IsIfFwdEnabled (i4IfIndex) == FALSE)
    {
        return IP6_FAILURE;
    }

    if ((pIf6->u1RaCnf & IP6_IF_NO_RA_ADV) == 0)
    {
        if (gIp6GblInfo.apIp6If[i4IfIndex]->u1OperStatus == OPER_UP)
        {
            /* Indicate to ND of the RA Status */
            Nd6ActOnRaCnf (pIf6, IP6_IF_NO_RA_ADV);
        }
        pIf6->u1RaCnf |= IP6_IF_NO_RA_ADV;
        pIf6->u1RaCnf &= (UINT1) ~IP6_IF_RA_ADV;
    }

    /*Clearing IfRouterAdvFlags */
    pIf6->u1RaCnf &= (UINT1) ~IP6_IF_M_BIT_ADV;
    pIf6->u1RaCnf &= (UINT1) ~IP6_IF_O_BIT_ADV;

    return IP6_SUCCESS;
}

/******************************************************************************
 * Function Name    :   Ip6UtilClearIpv6InterfaceTable
 * Description      :   This function clears the Ipv6Interface entry
 *                      configurations done for the given interface.
 *
 * Inputs           :   i4IfIndex - Interface Id
 * Return Value     :   IP6_SUCCESS or IP6_FAILURE
 *******************************************************************************/
INT1
Ip6UtilClearIpv6InterfaceTable (INT4 i4IfIndex)
{
#ifdef IP6_WANTED
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4IfIndex);

    if (pIf6 == NULL)
    {
        return IP6_FAILURE;
    }

    if (pIf6->u1AdminStatus == ADMIN_DOWN)
    {
        /* No change in the Status. */
        pIf6->u1AdminConfigFlag = TRUE;
        return IP6_SUCCESS;
    }
    if (((pIf6->u1OperStatus != NETIPV6_OPER_DOWN) &&
         (pIf6->u4CurOperStatus != NETIPV6_OPER_DOWN_INPROGRESS)) ||
        (pIf6->u1AdminStatus == ADMIN_UP))
    {
        /* IPv6 needs to disabled over this interface. */
        Ip6DisableIf ((UINT4) i4IfIndex);
    }

#else
    UNUSED_PARAM (i4IfIndex);
#endif
    return IP6_SUCCESS;
}

/********************************************************************
 * Function Name : Ip6UtlCgaAddrInfoGet  
 * Description   : This function fetches the Address Info given
 *                 the modifier 
 *
 * Inputs        : u4IfIndex - Interface Id
 *                 u1Type    - Linklocal or other 
 *                 pu1Modifier - Pointer to modifier
 * Return Value  : Address Info or NULL 
 ********************************************************************/
UINT1              *
Ip6UtlCgaAddrInfoGet (UINT4 u4IfIndex, UINT1 u1Type, UINT1 *pu1Modifier)
{
    tTMO_SLL_NODE      *pAddrSll = NULL;
    tIp6LlocalInfo     *pLlocalInfo = NULL;
    tIp6AddrInfo       *pAddrInfo = NULL;

    if (pu1Modifier == NULL)
    {
        return NULL;
    }

    if (u1Type == ADDR6_LLOCAL)
    {
        /* scan the link local SLL to find the matching modifier */
        TMO_SLL_Scan (&gIp6GblInfo.apIp6If[u4IfIndex]->lla6Ilist,
                      pAddrSll, tTMO_SLL_NODE *)
        {
            pLlocalInfo = IP6_LLADDR_PTR_FROM_SLL (pAddrSll);

            if (MEMCMP (pLlocalInfo->cgaParams.au1Modifier,
                        pu1Modifier, ND6_SEND_CGA_MOD_LEN) == 0)
            {
                return (UINT1 *) pLlocalInfo;
            }
        }
        return NULL;
    }

    /* scan the address info SLL to find the matching modifier */
    TMO_SLL_Scan (&gIp6GblInfo.apIp6If[u4IfIndex]->addr6Ilist,
                  pAddrSll, tTMO_SLL_NODE *)
    {
        pAddrInfo = IP6_ADDR_PTR_FROM_SLL (pAddrSll);

        if (MEMCMP (pAddrInfo->cgaParams.au1Modifier,
                    pu1Modifier, ND6_SEND_CGA_MOD_LEN) == 0)
        {
            return (UINT1 *) pAddrInfo;
        }
    }
    return NULL;
}

/********************************************************************
 * Function Name : Ip6UtilCgaAddrSyncToStandby  
 * Description   : This function synchs the address configuration to
 *                  standby 
 *
 * Inputs        : u4IfIndex - Interface Id
 *                 pIp6Addr  - IPv6 Address 
 *                 u1PrefixLen - Prefix Length
 *                 pCgaOptions - Pointer to CGA Paramters
 *
 * Return Value  : None 
 ********************************************************************/
VOID
Ip6UtilCgaAddrSyncToStandby (UINT4 u4IfIndex, tIp6Addr * pIp6Addr,
                             UINT1 u1PrefixLen, tCgaOptions * pCgaOptions)
{
    tSNMP_OCTET_STRING_TYPE OctetIp6Addr;
    tSNMP_OCTET_STRING_TYPE OctetCgaModifier;

    OctetIp6Addr.pu1_OctetList = pIp6Addr->u1_addr;
    OctetIp6Addr.i4_Length = IP6_ADDR_SIZE;

    OctetCgaModifier.pu1_OctetList = pCgaOptions->au1Modifier;
    OctetCgaModifier.i4_Length = ND6_SEND_CGA_MOD_LEN;

    /* 
     * The newly generated address is synced to standby using 
     * modifier
     */
    IncMsrForIpv6AddrTable ((INT4) u4IfIndex, &OctetIp6Addr,
                            u1PrefixLen, 's',
                            &OctetCgaModifier,
                            FsMIIpv6AddrSENDCgaModifier,
                            sizeof (FsMIIpv6AddrSENDCgaModifier) /
                            sizeof (UINT4), FALSE);

    /* syncing collision count is also necessary */
    IncMsrForIpv6AddrTable ((INT4) u4IfIndex, &OctetIp6Addr,
                            u1PrefixLen, 'i',
                            &pCgaOptions->u1Collisions,
                            FsMIIpv6AddrSENDCollisionCount,
                            sizeof (FsMIIpv6AddrSENDCollisionCount) /
                            sizeof (UINT4), FALSE);
}

/****************************************************************************
 *  Function    :  IncMsrForIpv6RARouteTable 
 *
 *   Input      :   i4IfIndex   - The interface index where the entry is added
 *                  Ipv6Address - IP address
 *                  i4PrefixLen - Length of IPV6 address 
 *                  cDataType   - Datatype of the configured value.
 *                  pSetVal     - Configured value that has to be notified.
 *                  pu4ObjectId - Object ID.
 *                  u4OIDLen    - Object Id length.
 *                  IsRowStatus - Rowstatus (TRUE/FALSE)
 *
 * Description :  This function sends notifications to MSR and RM
 *                 for the RA Route Information Table.
 *
 * Output      :  None.
 *
 * Returns     :  None.
 *
 * ****************************************************************************/

VOID
IncMsrForIpv6RARouteTable (INT4 i4Index, tSNMP_OCTET_STRING_TYPE * Ipv6Address,
                           INT4 i4PrefixLen, CHR1 cDatatype, VOID *pSetVal,
                           UINT4 *pu4ObjectId, UINT4 u4OIdLen,
                           UINT1 IsRowStatus)
{
    UINT4               u4SeqNum = IPVX_ZERO;
    INT4                i4SetValue = IPVX_ZERO;
    UINT4               u4SetValue = IPVX_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = pu4ObjectId;
    SnmpNotifyInfo.u4OidLen = u4OIdLen;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = IsRowStatus;
    SnmpNotifyInfo.pLockPointer = Ip6Lock;
    SnmpNotifyInfo.pUnLockPointer = Ip6UnLock;
    SnmpNotifyInfo.u4Indices = IPVX_THREE;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

    switch (cDatatype)
    {
        case 'i':
            i4SetValue = *(INT4 *) pSetVal;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %s %i %i",
                              i4Index, Ipv6Address, i4PrefixLen, i4SetValue));
            break;

        case 'u':
            u4SetValue = *(UINT4 *) pSetVal;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %s %i %u",
                              i4Index, Ipv6Address, i4PrefixLen, u4SetValue));
            break;
    }
#ifndef ISS_WANTED
    UNUSED_PARAM (i4Index);
    UNUSED_PARAM (Ipv6Address);
    UNUSED_PARAM (i4PrefixLen);
#endif
    return;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : Ip6ClearPolicyPrefixTable                          */
/*                                                                           */
/*     DESCRIPTION      : This function clears the interface policy prefix   */
/*                        table configurations done for the given interface  */
/*                                                                           */
/*     INPUT            : u4IfIndex                                          */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/

VOID
Ip6ClearPolicyPrefixTable (UINT4 u4IfIndex)
{

    INT1                i1Result = 0;
    INT4                i4Index = 0;
    INT4                i4PrefixLen = 0;
    INT4                i4PrevPrefixLen = 0;
    INT4                i4PrevIndex = 0;
    tSNMP_OCTET_STRING_TYPE StartPolicyPrefix;
    tSNMP_OCTET_STRING_TYPE PrevPolicyPrefix;
    tIp6AddrSelPolicy  *pPolicyPrefix = NULL;
    tIp6AddrSelPolicy  *pNextPrefix = NULL;

    UINT1               au1Addr[IPVX_IPV6_ADDR_LEN];
    UINT1               au1Address[IPVX_IPV6_ADDR_LEN];

    MEMSET (au1Addr, IP6_ZERO, IPVX_IPV6_ADDR_LEN);
    StartPolicyPrefix.pu1_OctetList = au1Addr;

    PrevPolicyPrefix.pu1_OctetList = au1Address;
    MEMSET (au1Address, 0, IP6_ADDR_SIZE);
    pPolicyPrefix = Ip6GetFirstPolicyPrefix ((UINT4) i4Index);

    if (pPolicyPrefix != NULL)
    {
        MEMCPY (StartPolicyPrefix.pu1_OctetList,
                &pPolicyPrefix->Ip6PolicyPrefix, IP6_ADDR_SIZE);
        StartPolicyPrefix.i4_Length = IP6_ADDR_SIZE;
        i4PrefixLen = pPolicyPrefix->u1PrefixLen;
        i1Result = IP6_SUCCESS;
    }
    else
    {
        /*if policy prefix does not exist */
        return;
    }

    while (i1Result != IP6_FAILURE)
    {
        i4PrevIndex = i4Index;

        MEMCPY (PrevPolicyPrefix.pu1_OctetList,
                StartPolicyPrefix.pu1_OctetList, IP6_ADDR_SIZE);
        PrevPolicyPrefix.i4_Length = IP6_ADDR_SIZE;
        i4PrevPrefixLen = i4PrefixLen;

        pNextPrefix = Ip6GetNextPolicyPrefix ((UINT4) i4PrevIndex,
                                              (tIp6Addr *) (VOID *)
                                              PrevPolicyPrefix.
                                              pu1_OctetList,
                                              (UINT1) i4PrevPrefixLen);
        if (pNextPrefix != NULL)
        {
            i4Index = (INT4) pNextPrefix->u4IfIndex;
            MEMCPY (StartPolicyPrefix.pu1_OctetList,
                    &pNextPrefix->Ip6PolicyPrefix, IP6_ADDR_SIZE);
            StartPolicyPrefix.i4_Length = IP6_ADDR_SIZE;
            i4PrefixLen = pNextPrefix->u1PrefixLen;
            i1Result = IP6_SUCCESS;
        }
        else
        {
            i1Result = IP6_FAILURE;
        }
        if (i4PrevIndex == (INT4) u4IfIndex)
        {
            i1Result = Ip6PolicyPrefixDelete ((UINT4) i4PrevIndex,
                                              (tIp6Addr *) (VOID *)
                                              PrevPolicyPrefix.pu1_OctetList,
                                              (UINT1) i4PrevPrefixLen);
        }
        if (i4Index > (INT4) u4IfIndex)
        {
            break;
        }
    }
    return;
}

#ifdef EVPN_VXLAN_WANTED
/******************************************************************************
 * Function Name    :   Ip6UpdateCacheForAddrFromEvpn
 * Description      :   This function checks if the ND cache exists for the next hop.
 *
 * Inputs           :   u4IfIndex  - Interface Index
 *                      pDest  - Dest IPv6 ADDR
 * Return Value     :   IP6_SUCCESS or IP6_FAILURE
 *******************************************************************************/
INT4
Ip6UpdateCacheForAddrFromEvpn (UINT4 u4IfIndex, tIp6Addr * pDest,
                               UINT1 *pau1Lladdr)
{
    tIp6If             *pIf6 = NULL;
    tNd6CacheEntry     *pNd6cEntry = NULL;

    pIf6 = IP6_INTERFACE (u4IfIndex);

    if (pIf6 == NULL)
    {
        return IP6_FAILURE;
    }

    IP6_TASK_LOCK ();
    pNd6cEntry = Nd6IsCacheForAddr (pIf6, pDest);
    if ((pNd6cEntry != NULL) && (pNd6cEntry->u1ReachState
                                 == ND6_CACHE_ENTRY_EVPN))
    {
        IP6_TASK_UNLOCK ();
        return IP6_SUCCESS;
    }
    else
    {
        pNd6cEntry = Nd6CreateCache (pIf6, pDest, pau1Lladdr, ISS_MAC_LEN,
                                     ND6_CACHE_ENTRY_EVPN, 0);
        if (pNd6cEntry != NULL)
        {
            IP6_TASK_UNLOCK ();
            return IP6_SUCCESS;
        }
    }
    IP6_TASK_UNLOCK ();
    return IP6_FAILURE;
}
#endif
