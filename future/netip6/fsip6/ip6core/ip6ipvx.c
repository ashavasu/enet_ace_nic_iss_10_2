/*******************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ip6ipvx.c,v 1.13 2011/03/24 07:03:34 siva Exp $
 *
 * Description: Cli related functions are handled here.
 *
 *******************************************************************/
#ifndef __IP6_IPVX_C__
#define __IP6_IPVX_C__

#include "ip6inc.h"
#include "ipvx.h"

/*  Functions for : InetCidrRouteTable       */

/****************************************************************************
 *
 *    FUNCTION NAME    : IPvxGetIpv6Stats             
 *
 *    DESCRIPTION      : This function Get the given Type of statistic value 
 *                       for a ipv6 interface or IPv6 Module level         
 *
 *    INPUT            : i4Ipv6IfIndex  - IPv6 ifindex (ifIndex level) or 
 *                                       IPVX_INVALID_IFIDX (ipv6 VR level) 
 *                     : i4ContextId    - VR id or IPVX_INVALID_IFIDX 
 *                     : u4IPvxStatsType -  statistic  Type 
 *                     : pu1Ipv6StatsObj - ptr to statistic value.
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : SNMP_SUCCESS / SNMP_FAILURE
 *
 ****************************************************************************/
INT1
IPvxGetIpv6Stats (INT4 i4Ipv6IfIndex, INT4 i4ContextId,
                  UINT4 u4IPvxStatsType, UINT1 *pu1Ipv6StatsObj)
{
    tIp6If             *pIf6 = NULL;
    tIp6Cxt            *pIp6Cxt = NULL;
    INT1                i1RetVal = SNMP_FAILURE;
    UINT4               u4IpSysStats = IP6_ZERO;
    UINT4              *pu4Stats = (UINT4 *) (VOID *) pu1Ipv6StatsObj;
    tSNMP_COUNTER64_TYPE *pu8Stats =
        (tSNMP_COUNTER64_TYPE *) (VOID *) pu1Ipv6StatsObj;

    if (i4Ipv6IfIndex != IPVX_INVALID_IFIDX)
    {
        pIf6 = Ip6ifGetEntry ((UINT4) i4Ipv6IfIndex);
        if (pIf6 == NULL)
        {
            return (SNMP_FAILURE);
        }
    }
    else
    {
        if (i4ContextId != IPVX_INVALID_IFIDX)
        {
            pIp6Cxt = gIp6GblInfo.apIp6Cxt[i4ContextId];
        }
        if (pIp6Cxt == NULL)
        {
            return (SNMP_FAILURE);
        }
    }

    /* Ptr to ObjStats = (pIf6 == NULL)?  System Stats : If Stats; */
    switch (u4IPvxStatsType)
    {
        case IPVX_STATS_IN_RECEIVES:

            if (pIf6 == NULL)
            {
                *pu4Stats = pIp6Cxt->Ip6SysStats.u4InRcvs;
            }
            else
            {
                i1RetVal = nmhGetFsipv6IfStatsInReceives (i4Ipv6IfIndex,
                                                          &u4IpSysStats);
                *pu4Stats = u4IpSysStats;
            }

            break;

        case IPVX_STATS_HC_IN_RECEIVES:

            pu8Stats->msn = (pIf6 == NULL) ?
                pIp6Cxt->Ip6SysStats.u4HCInReceives :
                pIf6->stats.u4HCInReceives;

            if (pIf6 == NULL)
            {
                pu8Stats->lsn = pIp6Cxt->Ip6SysStats.u4InRcvs;
            }
            else
            {
                i1RetVal = nmhGetFsipv6IfStatsInReceives (i4Ipv6IfIndex,
                                                          &u4IpSysStats);
                pu8Stats->lsn = u4IpSysStats;
                if (u4IpSysStats < pIf6->stats.u4InRcvs)
                {
                    pu8Stats->msn += IP6_ONE;
                }
            }
            break;

        case IPVX_STATS_IN_OCTETS:
            *pu4Stats = (pIf6 == NULL) ? pIp6Cxt->Ip6SysStats.u4InOctets :
                pIf6->stats.u4InOctets;
            break;

        case IPVX_STATS_HC_IN_OCTETS:
            pu8Stats->lsn = (pIf6 == NULL) ? pIp6Cxt->Ip6SysStats.u4InOctets :
                pIf6->stats.u4InOctets;
            pu8Stats->msn = (pIf6 == NULL) ? pIp6Cxt->Ip6SysStats.u4HCInOctets :
                pIf6->stats.u4HCInOctets;
            break;

        case IPVX_STATS_IN_HDR_ERRS:

            if (pIf6 == NULL)
            {
                *pu4Stats = pIp6Cxt->Ip6SysStats.u4InHdrerrs;
            }
            else
            {
                i1RetVal = nmhGetFsipv6IfStatsInHdrErrors (i4Ipv6IfIndex,
                                                           &u4IpSysStats);
                *pu4Stats = u4IpSysStats;
            }

            break;

        case IPVX_STATS_IN_NO_ROUTE:

            if (pIf6 == NULL)
            {
                *pu4Stats = pIp6Cxt->Ip6SysStats.u4InNorts;
            }
            else
            {
                i1RetVal = nmhGetIpv6IfStatsInNoRoutes (i4Ipv6IfIndex,
                                                        &u4IpSysStats);
                *pu4Stats = u4IpSysStats;
            }

            break;

        case IPVX_STATS_IN_ADDR_ERRS:

            if (pIf6 == NULL)
            {
                *pu4Stats = pIp6Cxt->Ip6SysStats.u4InAddrerrs;
            }
            else
            {
                i1RetVal = nmhGetFsipv6IfStatsInAddrErrors (i4Ipv6IfIndex,
                                                            &u4IpSysStats);
                *pu4Stats = u4IpSysStats;
            }

            break;

        case IPVX_STATS_IN_UNKNOWN_PROTOS:

            if (pIf6 == NULL)
            {
                *pu4Stats = pIp6Cxt->Ip6SysStats.u4InUnkprots;
            }
            else
            {
                i1RetVal = nmhGetFsipv6IfStatsInUnknownProtos (i4Ipv6IfIndex,
                                                               &u4IpSysStats);
                *pu4Stats = u4IpSysStats;
            }

            break;

        case IPVX_STATS_IN_TRUN_PKTS:

            if (pIf6 == NULL)
            {
                *pu4Stats = pIp6Cxt->Ip6SysStats.u4InTruncs;
            }
            else
            {
                i1RetVal = nmhGetFsipv6IfStatsInTruncatedPkts (i4Ipv6IfIndex,
                                                               &u4IpSysStats);
                *pu4Stats = u4IpSysStats;
            }

            break;

        case IPVX_STATS_IN_FWD_DGRAMS:
            if (pIf6 == NULL)
            {
                *pu4Stats = pIp6Cxt->Ip6SysStats.u4ForwDgrams;
            }
            else
            {
                i1RetVal = nmhGetFsipv6IfStatsForwDatagrams (i4Ipv6IfIndex,
                                                             &u4IpSysStats);
                *pu4Stats = u4IpSysStats;
            }
            break;

        case IPVX_STATS_HC_IN_FWD_DGRAMS:

            pu8Stats->msn = (pIf6 == NULL) ?
                pIp6Cxt->Ip6SysStats.u4HCInForwDgrams :
                pIf6->stats.u4HCInForwDgrams;

            if (pIf6 == NULL)
            {
                pu8Stats->lsn = pIp6Cxt->Ip6SysStats.u4ForwDgrams;
            }
            else
            {
                i1RetVal = nmhGetFsipv6IfStatsForwDatagrams (i4Ipv6IfIndex,
                                                             &u4IpSysStats);
                pu8Stats->lsn = u4IpSysStats;
                if (u4IpSysStats < pIf6->stats.u4ForwDgrams)
                {
                    pu8Stats->msn += IP6_ONE;
                }
            }
            break;

        case IPVX_STATS_REASM_REQDS:

            if (pIf6 == NULL)
            {
                *pu4Stats = pIp6Cxt->Ip6SysStats.u4Reasmreqs;
            }
            else
            {
                i1RetVal = nmhGetFsipv6IfStatsReasmReqds (i4Ipv6IfIndex,
                                                          &u4IpSysStats);
                *pu4Stats = u4IpSysStats;
            }
            break;

        case IPVX_STATS_REASM_OK:

            if (pIf6 == NULL)
            {
                *pu4Stats = pIp6Cxt->Ip6SysStats.u4Reasmoks;
            }
            else
            {
                i1RetVal = nmhGetFsipv6IfStatsReasmOKs (i4Ipv6IfIndex,
                                                        &u4IpSysStats);
                *pu4Stats = u4IpSysStats;
            }
            break;

        case IPVX_STATS_REASM_FAILS:

            if (pIf6 == NULL)
            {
                *pu4Stats = pIp6Cxt->Ip6SysStats.u4Reasmfails;
            }
            else
            {
                i1RetVal = nmhGetFsipv6IfStatsReasmFails (i4Ipv6IfIndex,
                                                          &u4IpSysStats);
                *pu4Stats = u4IpSysStats;
            }

            break;

        case IPVX_STATS_IN_DISCARDS:

            if (pIf6 == NULL)
            {
                *pu4Stats = pIp6Cxt->Ip6SysStats.u4InDiscards;
            }
            else
            {
                i1RetVal = nmhGetFsipv6IfStatsInDiscards (i4Ipv6IfIndex,
                                                          &u4IpSysStats);
                *pu4Stats = u4IpSysStats;
            }

            break;

        case IPVX_STATS_IN_DELIVERS:

            if (pIf6 == NULL)
            {
                *pu4Stats = pIp6Cxt->Ip6SysStats.u4InDelivers;
            }
            else
            {
                i1RetVal = nmhGetFsipv6IfStatsInDelivers (i4Ipv6IfIndex,
                                                          &u4IpSysStats);
                *pu4Stats = u4IpSysStats;
            }

            break;

        case IPVX_STATS_HC_IN_DELIVERS:

            pu8Stats->msn = (pIf6 == NULL) ?
                pIp6Cxt->Ip6SysStats.u4HCInDelivers :
                pIf6->stats.u4HCInDelivers;

            if (pIf6 == NULL)
            {
                pu8Stats->lsn = pIp6Cxt->Ip6SysStats.u4InDelivers;
            }
            else
            {
                i1RetVal = nmhGetFsipv6IfStatsInDelivers (i4Ipv6IfIndex,
                                                          &u4IpSysStats);
                pu8Stats->lsn = u4IpSysStats;
                if (u4IpSysStats < pIf6->stats.u4OutReqs)
                {
                    pu8Stats->msn += IP6_ONE;
                }
            }
            break;

        case IPVX_STATS_OUT_REQUESTS:

            if (pIf6 == NULL)
            {
                *pu4Stats = pIp6Cxt->Ip6SysStats.u4OutReqs;
            }
            else
            {
                i1RetVal = nmhGetFsipv6IfStatsOutRequests (i4Ipv6IfIndex,
                                                           &u4IpSysStats);
                *pu4Stats = u4IpSysStats;
            }

            break;

        case IPVX_STATS_HC_OUT_REQUESTS:

            pu8Stats->msn = (pIf6 == NULL) ?
                pIp6Cxt->Ip6SysStats.u4HCOutRequests :
                pIf6->stats.u4HCOutRequests;

            if (pIf6 == NULL)
            {
                pu8Stats->lsn = pIp6Cxt->Ip6SysStats.u4OutReqs;
            }
            else
            {
                i1RetVal = nmhGetFsipv6IfStatsOutRequests (i4Ipv6IfIndex,
                                                           &u4IpSysStats);
                pu8Stats->lsn = u4IpSysStats;
                if (u4IpSysStats < pIf6->stats.u4OutReqs)
                {
                    pu8Stats->msn += IP6_ONE;
                }
            }
            break;

        case IPVX_STATS_OUT_NO_ROUTES:

            if (pIf6 == NULL)
            {
                *pu4Stats = pIp6Cxt->Ip6SysStats.u4OutNorts;
            }
            else
            {
                i1RetVal = nmhGetFsipv6IfStatsOutNoRoutes (i4Ipv6IfIndex,
                                                           &u4IpSysStats);
                *pu4Stats = u4IpSysStats;
            }
            break;

        case IPVX_STATS_OUT_FWD_DGRAMS:
            *pu4Stats = (pIf6 == NULL) ? pIp6Cxt->Ip6SysStats.u4OutForwds :
                pIf6->stats.u4OutForwds;
            break;

        case IPVX_STATS_HC_OUT_FWD_DGRAMS:
            pu8Stats->lsn = (pIf6 == NULL) ? pIp6Cxt->Ip6SysStats.u4OutForwds :
                pIf6->stats.u4OutForwds;
            pu8Stats->msn = (pIf6 == NULL) ?
                pIp6Cxt->Ip6SysStats.u4HCOutForwds : pIf6->stats.u4HCOutForwds;
            break;

        case IPVX_STATS_OUT_DISCARDS:

            if (pIf6 == NULL)
            {
                *pu4Stats = pIp6Cxt->Ip6SysStats.u4OutDiscards;
            }
            else
            {
                i1RetVal = nmhGetFsipv6IfStatsOutDiscards (i4Ipv6IfIndex,
                                                           &u4IpSysStats);
                *pu4Stats = u4IpSysStats;
            }

            break;

        case IPVX_STATS_OUT_FRAG_REQDS:
            *pu4Stats = (pIf6 == NULL) ? pIp6Cxt->Ip6SysStats.u4OutFragReqds :
                pIf6->stats.u4OutFragReqds;
            break;

        case IPVX_STATS_OUT_FRAG_OK:

            if (pIf6 == NULL)
            {
                *pu4Stats = pIp6Cxt->Ip6SysStats.u4Fragoks;
            }
            else
            {
                i1RetVal = nmhGetFsipv6IfStatsFragOKs (i4Ipv6IfIndex,
                                                       &u4IpSysStats);
                *pu4Stats = u4IpSysStats;
            }

            break;

        case IPVX_STATS_OUT_FRAG_FAILS:

            if (pIf6 == NULL)
            {
                *pu4Stats = pIp6Cxt->Ip6SysStats.u4Fragfails;
            }
            else
            {
                i1RetVal = nmhGetFsipv6IfStatsFragFails (i4Ipv6IfIndex,
                                                         &u4IpSysStats);
                *pu4Stats = u4IpSysStats;
            }

            break;

        case IPVX_STATS_OUT_FRAG_CRET:

            if (pIf6 == NULL)
            {
                *pu4Stats = pIp6Cxt->Ip6SysStats.u4Fragcreates;
            }
            else
            {
                i1RetVal = nmhGetFsipv6IfStatsFragCreates (i4Ipv6IfIndex,
                                                           &u4IpSysStats);
                *pu4Stats = u4IpSysStats;
            }

            break;

        case IPVX_STATS_OUT_TRANS:
            *pu4Stats = (pIf6 == NULL) ? pIp6Cxt->Ip6SysStats.u4OutTrans :
                pIf6->stats.u4OutTrans;
            break;

        case IPVX_STATS_HC_OUT_TRANS:
            pu8Stats->lsn = (pIf6 == NULL) ? pIp6Cxt->Ip6SysStats.u4OutTrans :
                pIf6->stats.u4OutTrans;
            pu8Stats->msn = (pIf6 == NULL) ? pIp6Cxt->Ip6SysStats.u4HCOutTrans :
                pIf6->stats.u4HCOutTrans;
            break;

        case IPVX_STATS_OUT_OCTETS:
            *pu4Stats = (pIf6 == NULL) ? pIp6Cxt->Ip6SysStats.u4OutOctets :
                pIf6->stats.u4OutOctets;
            break;

        case IPVX_STATS_HC_OUT_OCTETS:
            pu8Stats->lsn = (pIf6 == NULL) ? pIp6Cxt->Ip6SysStats.u4OutOctets :
                pIf6->stats.u4OutOctets;
            pu8Stats->msn = (pIf6 == NULL) ?
                pIp6Cxt->Ip6SysStats.u4HCOutOctets : pIf6->stats.u4HCOutOctets;
            break;

        case IPVX_STATS_IN_MCAST_PKTS:
            if (pIf6 == NULL)
            {
                *pu4Stats = pIp6Cxt->Ip6SysStats.u4InMcasts;
            }
            else
            {
                i1RetVal = nmhGetFsipv6IfStatsInMcastPkts (i4Ipv6IfIndex,
                                                           &u4IpSysStats);
                *pu4Stats = u4IpSysStats;
            }
            break;

        case IPVX_STATS_HC_IN_MCAST_PKTS:

            pu8Stats->msn = (pIf6 == NULL) ? pIp6Cxt->Ip6SysStats.u4HCInMcasts :
                pIf6->stats.u4HCInMcasts;

            if (pIf6 == NULL)
            {
                pu8Stats->lsn = pIp6Cxt->Ip6SysStats.u4InMcasts;
            }
            else
            {
                i1RetVal = nmhGetFsipv6IfStatsInMcastPkts (i4Ipv6IfIndex,
                                                           &u4IpSysStats);
                pu8Stats->lsn = u4IpSysStats;
                if (u4IpSysStats < pIf6->stats.u4InMcasts)
                {
                    pu8Stats->msn += IP6_ONE;
                }
            }

            break;

        case IPVX_STATS_IN_MCAST_OCTETS:
            *pu4Stats = (pIf6 == NULL) ? pIp6Cxt->Ip6SysStats.u4InMcastOctets :
                pIf6->stats.u4InMcastOctets;
            break;

        case IPVX_STATS_HC_IN_MCAST_OCTETS:
            pu8Stats->lsn = (pIf6 == NULL) ?
                pIp6Cxt->Ip6SysStats.u4InMcastOctets :
                pIf6->stats.u4InMcastOctets;
            pu8Stats->msn = (pIf6 == NULL) ?
                pIp6Cxt->Ip6SysStats.u4HCInMcastOctets :
                pIf6->stats.u4HCInMcastOctets;
            break;

        case IPVX_STATS_OUT_MCAST_PKTS:
            if (pIf6 == NULL)
            {
                *pu4Stats = pIp6Cxt->Ip6SysStats.u4OutMcasts;
            }
            else
            {
                i1RetVal = nmhGetFsipv6IfStatsOutMcastPkts (i4Ipv6IfIndex,
                                                            &u4IpSysStats);
                *pu4Stats = u4IpSysStats;
            }
            break;

        case IPVX_STATS_HC_OUT_MCAST_PKTS:

            pu8Stats->msn = (pIf6 == NULL) ?
                pIp6Cxt->Ip6SysStats.u4HCOutMcastPkts :
                pIf6->stats.u4HCOutMcastPkts;

            if (pIf6 == NULL)
            {
                pu8Stats->lsn = pIp6Cxt->Ip6SysStats.u4OutMcasts;
            }
            else
            {
                i1RetVal = nmhGetFsipv6IfStatsOutMcastPkts (i4Ipv6IfIndex,
                                                            &u4IpSysStats);
                pu8Stats->lsn = u4IpSysStats;
                if (u4IpSysStats < pIf6->stats.u4OutMcasts)
                {
                    pu8Stats->msn += IP6_ONE;
                }
            }

            break;

        case IPVX_STATS_OUT_MCAST_OCTETS:
            *pu4Stats = (pIf6 == NULL) ? pIp6Cxt->Ip6SysStats.u4OutMcastOctets :
                pIf6->stats.u4OutMcastOctets;
            break;

        case IPVX_STATS_HC_OUT_MCAST_OCTETS:
            pu8Stats->lsn = (pIf6 == NULL) ?
                pIp6Cxt->Ip6SysStats.u4OutMcastOctets :
                pIf6->stats.u4OutMcastOctets;
            pu8Stats->msn = (pIf6 == NULL) ?
                pIp6Cxt->Ip6SysStats.u4HCOutMcastOctets :
                pIf6->stats.u4HCOutMcastOctets;
            break;

        case IPVX_STATS_IN_BCAST_PKTS:
        case IPVX_STATS_OUT_BCAST_PKTS:
            *pu4Stats = IP6_ZERO;
            break;

        case IPVX_STATS_HC_IN_BCAST_PKTS:
        case IPVX_STATS_HC_OUT_BCAST_PKTS:
            pu8Stats->msn = IP6_ZERO;
            pu8Stats->lsn = IP6_ZERO;
            break;

        default:
            return (SNMP_FAILURE);

    }

    return (SNMP_SUCCESS);
}

/*  Functions for : IpAddressPrefixTable     */

/*  Functions for : IpAddressTable           */

/*  Functions for : IpNetToPhysicalTable     */

/****************************************************************************
 *
 *    FUNCTION NAME    : IPvxGetIPv6NetToPhyLastUpdated 
 *
 *    DESCRIPTION      : This function get ND Cache entry last updated time. 
 *
 *    INPUT            : i4Ipv6IfIndex              - IPv6 ifindex  
 *                     : pIpv6Addr                  - Ipv6 address         
 *                     : pu4IPv6NDCTblLastUpdated   - ptr to last updated time
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : SNMP_SUCCESS / SNMP_FAILURE
 *
 ****************************************************************************/
INT1
IPvxGetIPv6NetToPhyLastUpdated (INT4 i4Ipv6IfIndex,
                                tSNMP_OCTET_STRING_TYPE * pIpv6Addr,
                                UINT4 *pu4IPv6NDCTblLastUpdated)
{
    tIp6If             *pIf6 = NULL;
    tNd6CacheEntry     *pNd6cEntry = NULL;

    pIf6 = Ip6ifGetEntry ((UINT2) i4Ipv6IfIndex);
    if (pIf6 == NULL)
    {
        return (SNMP_FAILURE);
    }
    pNd6cEntry =
        Nd6IsCacheForAddr (pIf6,
                           (tIp6Addr *) (VOID *) pIpv6Addr->pu1_OctetList);
    if (pNd6cEntry == NULL)
    {
        return (SNMP_FAILURE);
    }

    *pu4IPv6NDCTblLastUpdated = pNd6cEntry->u4LastUpdatedTime;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 *
 *    FUNCTION NAME    : IPvxGetIPv6NetToPhyType 
 *
 *    DESCRIPTION      : This function get ND Cache entry Type. 
 *
 *    INPUT            : i4Ipv6IfIndex              - IPv6 ifindex  
 *                     : pIpv6Addr                  - Ipv6 address         
 *                     : pi4IpNetToPhysicalType     - ptr to Entry Type 
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : SNMP_SUCCESS / SNMP_FAILURE
 *
 ****************************************************************************/
INT1
IPvxGetIPv6NetToPhyType (INT4 i4Ipv6IfIndex,
                         tSNMP_OCTET_STRING_TYPE * pIpv6Addr,
                         INT4 *pi4IpNetToPhysicalType)
{
    tIp6If             *pIf6 = NULL;
    tNd6CacheEntry     *pNd6cEntry = NULL;

    pIf6 = Ip6ifGetEntry ((UINT2) i4Ipv6IfIndex);
    if (pIf6 == NULL)
    {
        return (SNMP_FAILURE);
    }
    pNd6cEntry =
        Nd6IsCacheForAddr (pIf6,
                           (tIp6Addr *) (VOID *) pIpv6Addr->pu1_OctetList);
    if (pNd6cEntry == NULL)
    {
        return (SNMP_FAILURE);
    }

    switch (pNd6cEntry->u1ReachState)
    {
        case ND6C_STATIC:
        case ND6C_STATIC_NOT_IN_SERVICE:
            *pi4IpNetToPhysicalType = IPVX_NET_TO_PHY_TYPE_STATIC;
            break;

        case ND6C_REACHABLE:
        case ND6C_INCOMPLETE:
        case ND6C_STALE:
        case ND6C_DELAY:
        case ND6C_PROBE:
            *pi4IpNetToPhysicalType = IPVX_NET_TO_PHY_TYPE_DYNAMIC;
            break;

        default:
            *pi4IpNetToPhysicalType = IPVX_NET_TO_PHY_TYPE_OTHER;
            break;
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 *
 *    FUNCTION NAME    : IPvxGetIpv6NetToPhyState       
 *
 *    DESCRIPTION      : This function get ND Cache entry State. 
 *
 *    INPUT            : i4Ipv6IfIndex              - IPv6 ifindex  
 *                     : pIpv6Addr                  - Ipv6 address         
 *                     : pi4IPv6NDCTblState         - ptr to State of Entry.  
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : SNMP_SUCCESS / SNMP_FAILURE
 *
 ****************************************************************************/
INT1
IPvxGetIpv6NetToPhyState (INT4 i4Ipv6IfIndex,
                          tSNMP_OCTET_STRING_TYPE * pIpv6Addr,
                          INT4 *pi4IPv6NDCTblState)
{
    tIp6If             *pIf6 = NULL;
    tNd6CacheEntry     *pNd6cEntry = NULL;

    pIf6 = Ip6ifGetEntry ((UINT2) i4Ipv6IfIndex);

    if (pIf6 == NULL)
    {
        return (SNMP_FAILURE);
    }
    pNd6cEntry =
        Nd6IsCacheForAddr (pIf6,
                           (tIp6Addr *) (VOID *) pIpv6Addr->pu1_OctetList);
    if (pNd6cEntry == NULL)
    {
        return (SNMP_FAILURE);
    }

    switch (pNd6cEntry->u1ReachState)
    {
        case ND6C_STATIC:
        case ND6C_REACHABLE:
            *pi4IPv6NDCTblState = IPVX_NET_TO_PHY_STATE_REACHABLE;
            break;

        case ND6C_INCOMPLETE:
            *pi4IPv6NDCTblState = IPVX_NET_TO_PHY_STATE_INCOMPLETE;
            break;

        case ND6C_STALE:
            *pi4IPv6NDCTblState = IPVX_NET_TO_PHY_STATE_STALE;
            break;

        case ND6C_DELAY:
            *pi4IPv6NDCTblState = IPVX_NET_TO_PHY_STATE_DELAY;
            break;

        case ND6C_PROBE:
            *pi4IPv6NDCTblState = IPVX_NET_TO_PHY_STATE_PROBE;
            break;

        case ND6C_STATIC_NOT_IN_SERVICE:
            *pi4IPv6NDCTblState = IPVX_NET_TO_PHY_STATE_UNKNOWN;
            break;

        default:
            *pi4IPv6NDCTblState = IPVX_NET_TO_PHY_STATE_UNKNOWN;
            break;
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 *
 *    FUNCTION NAME    : IPvxGetIpv6NetToPhyRowStatus 
 *
 *    DESCRIPTION      : This function get ND Cache entry Rowstatus. 
 *
 *    INPUT            : i4Ipv6IfIndex          - IPv6 ifindex  
 *                     : pIpv6Addr              - Ipv6 address         
 *                     : pi4Ipv6GetRowStatus    - ptr to entry rowstatus
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : SNMP_SUCCESS / SNMP_FAILURE
 *
 ****************************************************************************/
INT1
IPvxGetIpv6NetToPhyRowStatus (INT4 i4Ipv6IfIndex,
                              tSNMP_OCTET_STRING_TYPE * pIpv6Addr,
                              INT4 *pi4Ipv6GetRowStatus)
{
    tIp6If             *pIf6 = NULL;
    tNd6CacheEntry     *pNd6cEntry = NULL;

    pIf6 = Ip6ifGetEntry ((UINT2) i4Ipv6IfIndex);
    if (pIf6 == NULL)
    {
        return (SNMP_FAILURE);
    }
    pNd6cEntry =
        Nd6IsCacheForAddr (pIf6,
                           (tIp6Addr *) (VOID *) pIpv6Addr->pu1_OctetList);
    if (pNd6cEntry == NULL)
    {
        return (SNMP_FAILURE);
    }

    *pi4Ipv6GetRowStatus = pNd6cEntry->u1RowStatus;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 *
 *    FUNCTION NAME    : IPvxSetIPv6NetToPhyPhyAddr        
 *
 *    DESCRIPTION      : This function set ND Cache entry type . 
 *
 *    INPUT            : i4Ipv6IfIndex                - IPv6 ifindex  
 *                     : pIpv6Addr                    - Ipv6 address         
 *                     : i4TestValIpNetToPhysicalType - Entry type 
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : SNMP_SUCCESS / SNMP_FAILURE
 *
 ****************************************************************************/
INT1
IPvxSetIPv6NetToPhyPhyAddr (INT4 i4Ipv6IfIndex,
                            tSNMP_OCTET_STRING_TYPE * pIpv6Addr,
                            tSNMP_OCTET_STRING_TYPE * pPhysAddr)
{
    tIp6If             *pIf6 = NULL;
    tNd6CacheEntry     *pNd6cEntry = NULL;
    tNd6CacheLanInfo   *pNd6cLinfo = NULL;

    pIf6 = Ip6ifGetEntry ((UINT2) i4Ipv6IfIndex);
    if (pIf6 == NULL)
    {
        return (SNMP_FAILURE);
    }

    pNd6cEntry =
        Nd6IsCacheForAddr (pIf6,
                           (tIp6Addr *) (VOID *) pIpv6Addr->pu1_OctetList);
    if (pNd6cEntry == NULL)
    {
        /* For Read-create Support */
        pNd6cEntry = Nd6CreateCache (pIf6, (tIp6Addr *) (VOID *)
                                     pIpv6Addr->pu1_OctetList,
                                     pPhysAddr->pu1_OctetList,
                                     (UINT1) pPhysAddr->i4_Length,
                                     ND6C_STATIC_NOT_IN_SERVICE);
        if (pNd6cEntry == NULL)
        {
            return (SNMP_FAILURE);
        }
        return (SNMP_SUCCESS);
    }

    /* If the RowStatus is ACTIVE set active otherwise set Not in Service */
    pNd6cLinfo = (tNd6CacheLanInfo *) (VOID *) pNd6cEntry->pNd6cInfo;
    if (pNd6cLinfo == NULL)
    {
        return (SNMP_FAILURE);
    }

    if (pNd6cEntry->u1RowStatus != ACTIVE)
    {
        Nd6UpdateStaticCache (pNd6cEntry, pPhysAddr->pu1_OctetList,
                              (UINT1) pPhysAddr->i4_Length,
                              ND6C_STATIC_NOT_IN_SERVICE);
    }
    else
    {
        Nd6UpdateStaticCache (pNd6cEntry, pPhysAddr->pu1_OctetList,
                              (UINT1) pPhysAddr->i4_Length, ND6C_STATIC);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 *
 *    FUNCTION NAME    : IPvxSetIPv6NetToPhyType        
 *
 *    DESCRIPTION      : This function set ND Cache entry type . 
 *
 *    INPUT            : i4Ipv6IfIndex                - IPv6 ifindex  
 *                     : pIpv6Addr                    - Ipv6 address         
 *                     : i4TestValIpNetToPhysicalType - Entry type 
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : SNMP_SUCCESS / SNMP_FAILURE
 *
 ****************************************************************************/
INT1
IPvxSetIPv6NetToPhyType (INT4 i4Ipv6IfIndex,
                         tSNMP_OCTET_STRING_TYPE * pIpv6Addr,
                         INT4 i4TestValIpNetToPhysicalType)
{
    tIp6If             *pIf6 = NULL;
    tNd6CacheEntry     *pNd6cEntry = NULL;
    tNd6CacheLanInfo   *pNd6cLinfo = NULL;
    INT4                i4RetVal = IP6_FAILURE;

    pIf6 = Ip6ifGetEntry ((UINT2) i4Ipv6IfIndex);
    if (pIf6 == NULL)
    {
        return (SNMP_FAILURE);
    }

    pNd6cEntry =
        Nd6IsCacheForAddr (pIf6,
                           (tIp6Addr *) (VOID *) pIpv6Addr->pu1_OctetList);
    if (pNd6cEntry == NULL)
    {
        /* For Read-create Support */
        if (i4TestValIpNetToPhysicalType == IPVX_NET_TO_PHY_TYPE_STATIC)
        {
            pNd6cEntry = Nd6CreateCache (pIf6, (tIp6Addr *) (VOID *)
                                         pIpv6Addr->pu1_OctetList,
                                         NULL, IP6_ZERO,
                                         ND6C_STATIC_NOT_IN_SERVICE);
            if (pNd6cEntry == NULL)
            {
                return (SNMP_FAILURE);
            }
        }

        return (SNMP_SUCCESS);
    }

    /* Delete an Entry at any State */
    if (i4TestValIpNetToPhysicalType == IPVX_NET_TO_PHY_TYPE_INVALID)
    {
        i4RetVal = Nd6PurgeCache (pNd6cEntry);
        if (i4RetVal != IP6_SUCCESS)
        {
            return (SNMP_FAILURE);
        }

        return (SNMP_SUCCESS);
    }

    /* if Static Entry Return */
    if ((pNd6cEntry->u1ReachState == ND6C_STATIC) ||
        (pNd6cEntry->u1ReachState == ND6C_STATIC_NOT_IN_SERVICE))
    {
        return (SNMP_SUCCESS);
    }

    /* Update the Dynamic Entry as a Static */
    pNd6cLinfo = (tNd6CacheLanInfo *) (VOID *) pNd6cEntry->pNd6cInfo;
    if (pNd6cLinfo == NULL)
    {
        return (SNMP_FAILURE);
    }
    if (pNd6cLinfo->u1LlaLen != CFA_ENET_ADDR_LEN)
    {
        Nd6UpdateStaticCache (pNd6cEntry, NULL, IP6_ZERO,
                              ND6C_STATIC_NOT_IN_SERVICE);
    }
    else
    {
        Nd6UpdateStaticCache (pNd6cEntry, pNd6cLinfo->lladdr,
                              pNd6cLinfo->u1LlaLen, ND6C_STATIC);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 *
 *    FUNCTION NAME    : IPvxSetIpv6NetToPhyRowStatus 
 *
 *    DESCRIPTION      : This function set ND Cache entry Rowstatus. 
 *
 *    INPUT            : i4Ipv6IfIndex          - IPv6 ifindex  
 *                     : pIpv6Addr              - Ipv6 address         
 *                     : i4Ipv6SetRowStatus     - entry rowstatus
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : SNMP_SUCCESS / SNMP_FAILURE
 *
 ****************************************************************************/
INT1
IPvxSetIpv6NetToPhyRowStatus (INT4 i4Ipv6IfIndex,
                              tSNMP_OCTET_STRING_TYPE * pIpv6Addr,
                              INT4 i4Ipv6SetRowStatus)
{
    tIp6If             *pIf6 = NULL;
    tNd6CacheEntry     *pNd6cEntry = NULL;
    tNd6CacheLanInfo   *pNd6cLinfo = NULL;
    INT4                i4RetVal = IP6_FAILURE;

    pIf6 = Ip6ifGetEntry ((UINT2) i4Ipv6IfIndex);
    if (pIf6 == NULL)
    {
        return (SNMP_FAILURE);
    }

    pNd6cEntry =
        Nd6IsCacheForAddr (pIf6,
                           (tIp6Addr *) (VOID *) pIpv6Addr->pu1_OctetList);
    if (pNd6cEntry != NULL)
    {
        /* Entry found in the Table */
        if (pNd6cEntry->u1RowStatus == i4Ipv6SetRowStatus)
        {
            return (SNMP_SUCCESS);
        }
    }
    else if (i4Ipv6SetRowStatus != CREATE_AND_WAIT)
    {
        return (SNMP_FAILURE);
    }

    switch (i4Ipv6SetRowStatus)
    {
            /* Modify an Entry */
        case ACTIVE:
            /* 1. Set the Entry as ND6C_STATIC */
            pNd6cLinfo = (tNd6CacheLanInfo *) (VOID *) pNd6cEntry->pNd6cInfo;
            if (pNd6cLinfo == NULL)
            {
                return (SNMP_FAILURE);
            }
            Nd6UpdateStaticCache (pNd6cEntry, pNd6cLinfo->lladdr,
                                  pNd6cLinfo->u1LlaLen, ND6C_STATIC);
            break;

        case NOT_IN_SERVICE:
            /* 1. Set the Entry as ND6C_STATIC_NOT_IN_SERVICE  */
            pNd6cLinfo = (tNd6CacheLanInfo *) (VOID *) pNd6cEntry->pNd6cInfo;
            if (pNd6cLinfo == NULL)
            {
                return (SNMP_FAILURE);
            }
            Nd6UpdateStaticCache (pNd6cEntry, pNd6cLinfo->lladdr,
                                  pNd6cLinfo->u1LlaLen,
                                  ND6C_STATIC_NOT_IN_SERVICE);
            break;

        case CREATE_AND_WAIT:
            /* 1. Create a Entry with Reachable State as 
             * ND6C_STATIC_NOT_IN_SERVICE
             * This state is not in NDCache State machine.
             *  -- staticaly configured entry which is not used = 
             *  ND6C_STATIC_NOT_IN_SERVICE 
             */
            pNd6cEntry = Nd6CreateCache (pIf6, (tIp6Addr *) (VOID *)
                                         pIpv6Addr->pu1_OctetList,
                                         NULL, IP6_ZERO,
                                         ND6C_STATIC_NOT_IN_SERVICE);
            if (pNd6cEntry == NULL)
            {
                return (SNMP_FAILURE);
            }
            break;

        case DESTROY:
            i4RetVal = Nd6PurgeCache (pNd6cEntry);
            if (i4RetVal != IP6_SUCCESS)
            {
                return (SNMP_FAILURE);
            }
            break;

        case CREATE_AND_GO:
            return (SNMP_FAILURE);

        default:
            return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 *
 *    FUNCTION NAME    : IPvxTestIPv6NetToPhyType       
 *
 *    DESCRIPTION      : This function test ND Cache entry type . 
 *
 *    INPUT            : pu4ErrorCode                 - ptr to error code
 *                     : i4Ipv6IfIndex                - IPv6 ifindex  
 *                     : pIpv6Addr                    - Ipv6 address         
 *                     : i4TestValIpNetToPhysicalType - Entry type 
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : SNMP_SUCCESS / SNMP_FAILURE
 *
 ****************************************************************************/
INT1
IPvxTestIPv6NetToPhyType (UINT4 *pu4ErrorCode, INT4 i4Ipv6IfIndex,
                          tSNMP_OCTET_STRING_TYPE * pIpv6Addr,
                          INT4 i4TestValIpNetToPhysicalType)
{
    tIp6If             *pIf6 = NULL;
    tNd6CacheEntry     *pNd6cEntry = NULL;

    if ((i4TestValIpNetToPhysicalType != IPVX_NET_TO_PHY_TYPE_INVALID) &&
        (i4TestValIpNetToPhysicalType != IPVX_NET_TO_PHY_TYPE_STATIC))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    /* Entry not present cann't set INVALID */
    pIf6 = Ip6ifGetEntry ((UINT2) i4Ipv6IfIndex);
    if (pIf6 == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    pNd6cEntry =
        Nd6IsCacheForAddr (pIf6,
                           (tIp6Addr *) (VOID *) pIpv6Addr->pu1_OctetList);
    if (pNd6cEntry == NULL)
    {
        if (i4TestValIpNetToPhysicalType == IPVX_NET_TO_PHY_TYPE_INVALID)
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            return (SNMP_FAILURE);
        }
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 *
 *    FUNCTION NAME    : IPvxTestIpv6NetToPhyRowStatus
 *
 *    DESCRIPTION      : This function test ND Cache entry Rowstatus. 
 *
 *    INPUT            : i4Ipv6IfIndex          - IPv6 ifindex  
 *                     : pIpv6Addr              - Ipv6 address         
 *                     : i4Ipv6TestRowStatus    - entry rowstatus
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : SNMP_SUCCESS / SNMP_FAILURE
 *
 ****************************************************************************/
INT1
IPvxTestIpv6NetToPhyRowStatus (UINT4 *pu4ErrorCode, INT4 i4Ipv6IfIndex,
                               tSNMP_OCTET_STRING_TYPE * pIpv6Addr,
                               INT4 i4Ipv6TestRowStatus)
{
    tIp6If             *pIf6 = NULL;
    tNd6CacheEntry     *pNd6cEntry = NULL;
    tNd6CacheLanInfo   *pNd6cLinfo = NULL;

    pIf6 = Ip6ifGetEntry ((UINT2) i4Ipv6IfIndex);
    if (pIf6 == NULL)
    {
        return (SNMP_FAILURE);
    }
    pNd6cEntry =
        Nd6IsCacheForAddr (pIf6,
                           (tIp6Addr *) (VOID *) pIpv6Addr->pu1_OctetList);
    if (pNd6cEntry == NULL)
    {
        /* Entry Not Found In The Table */
        switch (i4Ipv6TestRowStatus)
        {
            case ACTIVE:
            case NOT_IN_SERVICE:
            case CREATE_AND_GO:
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return (SNMP_FAILURE);

            case DESTROY:
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return (SNMP_FAILURE);

            case CREATE_AND_WAIT:
                break;

            default:
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return (SNMP_FAILURE);
        }
    }
    else
    {
        /* Entry Found In The Table */
        switch (i4Ipv6TestRowStatus)
        {
            case ACTIVE:
                /* Check the MAC Address it should not be NULL */
                pNd6cLinfo =
                    (tNd6CacheLanInfo *) (VOID *) pNd6cEntry->pNd6cInfo;
                if (pNd6cLinfo == NULL)
                {
                    return (SNMP_FAILURE);
                }
                if ((pNd6cEntry->u1RowStatus == NOT_READY) ||
                    (pNd6cLinfo->u1LlaLen == IPVX_ZERO))
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return (SNMP_FAILURE);
                }
                break;

            case NOT_IN_SERVICE:
                if ((pNd6cEntry->u1RowStatus == NOT_READY) ||
                    ((pNd6cEntry->u1ReachState != ND6C_STATIC_NOT_IN_SERVICE) &&
                     (pNd6cEntry->u1ReachState != ND6C_STATIC)))
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return (SNMP_FAILURE);
                }
                break;

            case DESTROY:
                break;

            case CREATE_AND_WAIT:
            case CREATE_AND_GO:
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return (SNMP_FAILURE);

            default:
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return (SNMP_FAILURE);
        }                        /* End of switch */
    }                            /* End of if */

    return (SNMP_SUCCESS);
}

/*  Functions for : Ipv6ScopeZoneIndexTable  -- No Functions */

/*  Functions for : IpDefaultRouterTable     */
/****************************************************************************
 *
 *    FUNCTION NAME    : IPvxValIdxInstIpv6DefRtrTbl  
 *
 *    DESCRIPTION      : This function validate the given  Router address 
 *                       is learned as default router through ND on the
 *                       given interface or not. 
 *
 *    INPUT            : pIpv6DefRtrAddr        - Ipv6 default router address 
 *                     : i4Ipv6DefRtrIfIndex    - IPv6 ifindex         
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : SNMP_SUCCESS / SNMP_FAILURE
 *
 ****************************************************************************/
INT1
IPvxValIdxInstIpv6DefRtrTbl (tSNMP_OCTET_STRING_TYPE * pIpv6DefRtrAddr,
                             INT4 i4Ipv6DefRtrIfIndex)
{
    tIp6Addr            Ip6Addr;
    UINT4               u4RtrLifetime = 0;
    INT4                i1RetVal = SNMP_FAILURE;

    MEMCPY (&Ip6Addr, pIpv6DefRtrAddr->pu1_OctetList, sizeof (tIp6Addr));
    i1RetVal = Nd6GetDefRtrLifetime (&Ip6Addr, i4Ipv6DefRtrIfIndex,
                                     &u4RtrLifetime);

    if (i1RetVal == IP6_SUCCESS)
    {
        i1RetVal = SNMP_SUCCESS;
    }
    else
    {
        i1RetVal = SNMP_FAILURE;
    }

    return i1RetVal;

}

/****************************************************************************
 *
 *    FUNCTION NAME    : IPvxGetNxtIdxIpv6DefRtrTbl  
 *
 *    DESCRIPTION      : This function give the next default Router address 
 *                       for a given values.
 *
 *    INPUT            : pIpv6DefRtrAddr        - Ipv6 default router address 
 *                     : pNextIpv6DefRtrAddr    - next Ipv6 default router
 *                                                address 
 *                     : i4Ipv6DefRtrIfIndex    - IPv6 ifindex         
 *                     : pi4NextIpv6DefRtrIfIndex - Ipv6 ifindex  of
 *                           ' pNextIpv6DefRtrAddr '        
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : SNMP_SUCCESS / SNMP_FAILURE
 *
 ****************************************************************************/
INT1
IPvxGetNxtIdxIpv6DefRtrTbl (tSNMP_OCTET_STRING_TYPE * pIpv6DefRtrAddr,
                            tSNMP_OCTET_STRING_TYPE * pNextIpv6DefRtrAddr,
                            INT4 i4Ipv6DefRtrIfIndex,
                            INT4 *pi4NextIpv6DefRtrIfIndex)
{
    tIp6Addr            Ip6Addr, NextIp6Addr;
    INT1                i1RetVal;

    if (pNextIpv6DefRtrAddr == NULL)
    {
        return SNMP_FAILURE;
    }
    if (pIpv6DefRtrAddr != NULL)
    {
        MEMCPY (&Ip6Addr, pIpv6DefRtrAddr->pu1_OctetList, sizeof (tIp6Addr));
    }
    else
    {
        MEMSET (&Ip6Addr, IP6_ZERO, sizeof (tIp6Addr));
    }
    i1RetVal = (INT1) Nd6GetNextDefRtr (&Ip6Addr, &NextIp6Addr,
                                        i4Ipv6DefRtrIfIndex,
                                        pi4NextIpv6DefRtrIfIndex);
    MEMCPY (pNextIpv6DefRtrAddr->pu1_OctetList, &NextIp6Addr,
            sizeof (tIp6Addr));
    pNextIpv6DefRtrAddr->i4_Length = IPVX_IPV6_ADDR_LEN;

    if (i1RetVal == IP6_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 *
 *    FUNCTION NAME    : IPvxGetIpv6DefRtrLifetime   
 *
 *    DESCRIPTION      : This function give the Life time for a given 
 *                       default Router address and ifindex
 *
 *    INPUT            : pIpv6DefRtrAddr        - Ipv6 default router address 
 *                     : i4Ipv6DefRtrIfIndex    - IPv6 ifindex         
 *                     : pu4Ipv6DefRtrLifetime  - Life time of the default 
 *                           router        
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : SNMP_SUCCESS / SNMP_FAILURE
 *
 ****************************************************************************/
INT1
IPvxGetIpv6DefRtrLifetime (tSNMP_OCTET_STRING_TYPE * pIpv6DefRtrAddr,
                           INT4 i4Ipv6DefRtrIfIndex,
                           UINT4 *pu4Ipv6DefRtrLifetime)
{
    tIp6Addr            Ip6Addr;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMCPY (&Ip6Addr, pIpv6DefRtrAddr->pu1_OctetList, sizeof (tIp6Addr));
    i1RetVal = (INT1) Nd6GetDefRtrLifetime (&Ip6Addr, i4Ipv6DefRtrIfIndex,
                                            pu4Ipv6DefRtrLifetime);
    if (i1RetVal == IP6_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 *
 *    FUNCTION NAME    : IPvxGetIpv6DefRtrPreference 
 *
 *    DESCRIPTION      : This function give the prererence value for a given 
 *                       default Router address and ifindex
 *
 *    INPUT            : pIpv6DefRtrAddr        - Ipv6 default router address 
 *                     : i4Ipv6DefRtrIfIndex    - IPv6 ifindex         
 *                     : pu4Ipv6DefRtrPref      - Preferences value of the
 *                        default router        
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : SNMP_SUCCESS / SNMP_FAILURE
 *
 ****************************************************************************/
INT1
IPvxGetIpv6DefRtrPreference (tSNMP_OCTET_STRING_TYPE * pIpv6DefRtrAddr,
                             INT4 i4Ipv6DefRtrIfIndex, UINT4 *pu4Ipv6DefRtrPref)
{
    /* RFC 4191 - NOt Implemented so always medium (0) 
     * for Both IPV4, IPV6 */
    UNUSED_PARAM (pIpv6DefRtrAddr);
    UNUSED_PARAM (i4Ipv6DefRtrIfIndex);

    *pu4Ipv6DefRtrPref = IPVX_DEF_ROUTER_PREF_MEDIUM;

    return (SNMP_SUCCESS);
}

/*  Functions for : Ipv6RouterAdvertTable    */

/*  Functions for : IcmpStatsTable  -- No Functions */

/*  Functions for : IcmpMsgStatsTable        */
/****************************************************************************
 *
 *    FUNCTION NAME    : IPvxValidateIpv6IcmpMsgType  
 *
 *    DESCRIPTION      : This function validate the Given Icmp6 message type 
 *
 *    INPUT            : i4IcmpMsgStatsType - Type of Icmp6 Message 
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : SNMP_SUCCESS / SNMP_FAILURE
 *
 ****************************************************************************/
INT1
IPvxValidateIpv6IcmpMsgType (INT4 i4IcmpMsgStatsType)
{
    switch (i4IcmpMsgStatsType)
    {
        case ICMP6_DEST_UNREACHABLE:    /*   1 */
        case ICMP6_PKT_TOO_BIG:    /*   2 */
        case ICMP6_TIME_EXCEEDED:    /*   3 */
        case ICMP6_PKT_PARAM_PROBLEM:    /*   4 */
        case ICMP6_ECHO_REQUEST:    /* 128 */
        case ICMP6_ECHO_REPLY:    /* 129 */
        case ICMP6_MLD_QUERY:    /* 130 */
        case ICMP6_MLD_REPORT:    /* 131 */
        case ICMP6_MLD_DONE:    /* 132 */
        case ICMP6_ROUTER_SOLICITATION:    /* 133 */
        case ICMP6_ROUTER_ADVERTISEMENT:    /* 134 */
        case ICMP6_NEIGHBOUR_SOLICITATION:    /* 135 */
        case ICMP6_NEIGHBOUR_ADVERTISEMENT:    /* 136 */
        case ICMP6_ROUTE_REDIRECT:    /* 137 */
        case ICMP6_RATE_LIMIT:    /* 138 */
            break;

        default:
            return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 *
 *    FUNCTION NAME    : IPvxGetNxtIpv6IcmpMsgType  
 *
 *    DESCRIPTION      : This function give the next valid Icmp6 message type 
 *
 *    INPUT            : i4IcmpMsgType      - Type of Icmp6 Message 
 *                     : pi4NextIcmpMsgType - Type of next Icmp6 Message 
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : SNMP_SUCCESS / SNMP_FAILURE
 *
 ****************************************************************************/
INT1
IPvxGetNxtIpv6IcmpMsgType (INT4 i4IcmpMsgType, INT4 *pi4NextIcmpMsgType)
{

    /* This is to fill the gap between 4-128 */
    if ((i4IcmpMsgType > ICMP6_PKT_PARAM_PROBLEM) &&
        (i4IcmpMsgType < ICMP6_ECHO_REQUEST))
    {
        i4IcmpMsgType = ICMP6_PKT_PARAM_PROBLEM;
    }

    switch (i4IcmpMsgType)
    {
        case ICMP6_DEST_UNREACHABLE:    /*   1 */
            *pi4NextIcmpMsgType = ICMP6_PKT_TOO_BIG;
            break;

        case ICMP6_PKT_TOO_BIG:    /*   2 */
            *pi4NextIcmpMsgType = ICMP6_TIME_EXCEEDED;
            break;

        case ICMP6_TIME_EXCEEDED:    /*   3 */
            *pi4NextIcmpMsgType = ICMP6_PKT_PARAM_PROBLEM;
            break;

        case ICMP6_PKT_PARAM_PROBLEM:    /*   4 */
            *pi4NextIcmpMsgType = ICMP6_ECHO_REQUEST;
            break;

        case ICMP6_ECHO_REQUEST:    /* 128 */
            *pi4NextIcmpMsgType = ICMP6_ECHO_REPLY;
            break;

        case ICMP6_ECHO_REPLY:    /* 129 */
            *pi4NextIcmpMsgType = ICMP6_MLD_QUERY;
            break;

        case ICMP6_MLD_QUERY:    /* 130 */
            *pi4NextIcmpMsgType = ICMP6_MLD_REPORT;
            break;

        case ICMP6_MLD_REPORT:    /* 131 */
            *pi4NextIcmpMsgType = ICMP6_MLD_DONE;
            break;

        case ICMP6_MLD_DONE:    /* 132 */
            *pi4NextIcmpMsgType = ICMP6_ROUTER_SOLICITATION;
            break;

        case ICMP6_ROUTER_SOLICITATION:    /* 133 */
            *pi4NextIcmpMsgType = ICMP6_ROUTER_ADVERTISEMENT;
            break;

        case ICMP6_ROUTER_ADVERTISEMENT:    /* 134 */
            *pi4NextIcmpMsgType = ICMP6_NEIGHBOUR_SOLICITATION;
            break;

        case ICMP6_NEIGHBOUR_SOLICITATION:    /* 135 */
            *pi4NextIcmpMsgType = ICMP6_NEIGHBOUR_ADVERTISEMENT;
            break;

        case ICMP6_NEIGHBOUR_ADVERTISEMENT:    /* 136 */
            *pi4NextIcmpMsgType = ICMP6_ROUTE_REDIRECT;
            break;

        case ICMP6_ROUTE_REDIRECT:    /* 137 */
            *pi4NextIcmpMsgType = ICMP6_RATE_LIMIT;
            break;

        case ICMP6_RATE_LIMIT:    /* 138 */
            return (SNMP_FAILURE);

        default:
            return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 *
 *    FUNCTION NAME    : IPvxIpv6GetIcmpMsgStatsPkts 
 *
 *    DESCRIPTION      : This function used to get the Icmp6 stats for the   
 *                       given counter type
 *
 *    INPUT            : i4IcmpMsgStatsType - Type of Icmp6 Message 
 *                     : u1Dir              - Direction of the Counter (IN/OUT)
 *                     : pu4RetValIcmpMsgStatsInPkts - ptr to Counter      
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : SNMP_SUCCESS / SNMP_FAILURE
 *
 ****************************************************************************/
INT1
IPvxIpv6GetIcmpMsgStatsPkts (UINT4 u4ContextId, INT4 i4IcmpMsgStatsType,
                             UINT1 u1Dir, UINT4 *pu4RetValIcmpMsgStatsInPkts)
{
    tIp6Cxt            *pIp6Cxt = NULL;
    UINT4               u4Pkts = IP6_ZERO;

    pIp6Cxt = gIp6GblInfo.apIp6Cxt[u4ContextId];

    if (pIp6Cxt == NULL)
    {
        return SNMP_FAILURE;
    }

    switch (i4IcmpMsgStatsType)
    {
        case ICMP6_DEST_UNREACHABLE:    /*   1 */
            u4Pkts =
                (u1Dir ==
                 IPVX_STATS_DIR_IN) ? (pIp6Cxt->Icmp6Stats.u4InDstUnreach) :
                (pIp6Cxt->Icmp6Stats.u4OutDstUnreach);
            break;

        case ICMP6_PKT_TOO_BIG:    /*   2 */
            u4Pkts = (u1Dir == IPVX_STATS_DIR_IN) ?
                pIp6Cxt->Icmp6Stats.u4InToobig :
                pIp6Cxt->Icmp6Stats.u4OutToobig;
            break;

        case ICMP6_TIME_EXCEEDED:    /*   3 */
            u4Pkts = (u1Dir == IPVX_STATS_DIR_IN) ?
                pIp6Cxt->Icmp6Stats.u4InTmexceeded :
                pIp6Cxt->Icmp6Stats.u4OutTmexceeded;
            break;

        case ICMP6_PKT_PARAM_PROBLEM:    /*   4 */
            u4Pkts = (u1Dir == IPVX_STATS_DIR_IN) ?
                pIp6Cxt->Icmp6Stats.u4InParamprob :
                pIp6Cxt->Icmp6Stats.u4OutParamprob;
            break;

        case ICMP6_ECHO_REQUEST:    /* 128 */
            u4Pkts = (u1Dir == IPVX_STATS_DIR_IN) ?
                pIp6Cxt->Icmp6Stats.u4InEchoReq :
                pIp6Cxt->Icmp6Stats.u4OutEchoReq;
            break;

        case ICMP6_ECHO_REPLY:    /* 129 */
            u4Pkts = (u1Dir == IPVX_STATS_DIR_IN) ?
                pIp6Cxt->Icmp6Stats.u4InEchoResp :
                pIp6Cxt->Icmp6Stats.u4OutEchoResp;
            break;

        case ICMP6_MLD_QUERY:    /* 130 */
            u4Pkts = (u1Dir == IPVX_STATS_DIR_IN) ?
                pIp6Cxt->Icmp6Stats.u4InMLDQuery :
                pIp6Cxt->Icmp6Stats.u4OutMLDQuery;
            break;

        case ICMP6_MLD_REPORT:    /* 131 */
            u4Pkts = (u1Dir == IPVX_STATS_DIR_IN) ?
                pIp6Cxt->Icmp6Stats.u4InMLDReport :
                pIp6Cxt->Icmp6Stats.u4OutMLDReport;
            break;

        case ICMP6_MLD_DONE:    /* 132 */
            u4Pkts = (u1Dir == IPVX_STATS_DIR_IN) ?
                pIp6Cxt->Icmp6Stats.u4InMLDDone :
                pIp6Cxt->Icmp6Stats.u4OutMLDDone;
            break;

        case ICMP6_ROUTER_SOLICITATION:    /* 133 */
            u4Pkts = (u1Dir == IPVX_STATS_DIR_IN) ?
                pIp6Cxt->Icmp6Stats.u4InRouterSol :
                pIp6Cxt->Icmp6Stats.u4OutRouterSol;
            break;

        case ICMP6_ROUTER_ADVERTISEMENT:    /* 134 */
            u4Pkts = (u1Dir == IPVX_STATS_DIR_IN) ?
                pIp6Cxt->Icmp6Stats.u4InRouterAdv :
                pIp6Cxt->Icmp6Stats.u4OutRouterAdv;
            break;

        case ICMP6_NEIGHBOUR_SOLICITATION:    /* 135 */
            u4Pkts = (u1Dir == IPVX_STATS_DIR_IN) ?
                pIp6Cxt->Icmp6Stats.u4InNeighSol :
                pIp6Cxt->Icmp6Stats.u4OutNeighSol;
            break;

        case ICMP6_NEIGHBOUR_ADVERTISEMENT:    /* 136 */
            u4Pkts = (u1Dir == IPVX_STATS_DIR_IN) ?
                pIp6Cxt->Icmp6Stats.u4InNeighAdv :
                pIp6Cxt->Icmp6Stats.u4OutNeighAdv;
            break;

        case ICMP6_ROUTE_REDIRECT:    /* 137 */
            u4Pkts = (u1Dir == IPVX_STATS_DIR_IN) ?
                pIp6Cxt->Icmp6Stats.u4InRedir : pIp6Cxt->Icmp6Stats.u4OutRedir;
            break;

        case ICMP6_RATE_LIMIT:    /* 138 */
            UNUSED_PARAM (u1Dir);
            u4Pkts = pIp6Cxt->Icmp6Stats.u4IcmpOutRateLimit;
            break;

        default:
            u4Pkts = IP6_ZERO;
            break;
    }

    *pu4RetValIcmpMsgStatsInPkts = u4Pkts;
    return (SNMP_SUCCESS);
}

/*********************************************************************
*  Function Name : IPvxSetIPv6Addr 
*  Description   : Configuring Ipv6 Address
*  Input(s)      : u4Ip6IfIndex - Interface Index
*                  Ip6Addr - Ip6 Address to be set
*                  i4PrefixLen - Prefix Length
*                  i4AddrType - Adress type (Unicast,link-local,
*                               anycast,eui-based)
*  Output(s)     :  
*  Return Values : None.
*********************************************************************/

VOID
IPvxSetIPv6Addr (UINT4 u4Ip6IfIndex, tIp6Addr Ip6Addr,
                 INT4 i4PrefixLen, INT4 i4AddrType)
{
    tSNMP_OCTET_STRING_TYPE Addr;
    tSNMP_OCTET_STRING_TYPE IfToken;
    UINT4               u4ErrorCode;
    INT4                i4Status;
    UINT1               au1AddrOctetList[IP6_ADDR_SIZE];
    UINT1               au1IfTokenOctetList[IP6_EUI_ADDRESS_LEN];

    MEMSET (au1AddrOctetList, IP6_ZERO, IP6_ADDR_SIZE);
    MEMSET (au1IfTokenOctetList, IP6_ZERO, IP6_EUI_ADDRESS_LEN);

    Addr.pu1_OctetList = au1AddrOctetList;
    MEMCPY (Addr.pu1_OctetList, &Ip6Addr, IP6_ADDR_SIZE);
    Addr.i4_Length = IP6_ADDR_SIZE;

    if (IS_ADDR_MULTI (Ip6Addr) || IS_ADDR_UNSPECIFIED (Ip6Addr))
    {
        return;
    }

    /*
     * EUI-64 address configuration is supported only for CLI. When the
     * address type is set as EUI-64 form the fetch the interface
     * identifier value and form the complete IP6 address to be configured.
     */
    if (i4AddrType == ADDR6_UNSPECIFIED)
    {                            /* EUI-64 */
        /* Length should not be greater than 64. */
        if (i4PrefixLen != IP6_EUI_ADDRESS_PREFIX_LEN)
        {
            return;
        }

        /* EUI-64 can be configured only if IPv6 is already enabled over
         * that interface */
        if ((nmhGetFsipv6IfAdminStatus
             ((INT4) u4Ip6IfIndex, &i4Status)
             == SNMP_FAILURE) || (i4Status != ADMIN_UP))
        {
            return;
        }

        /* Form the Actual IP6 Address */
        IfToken.pu1_OctetList = au1IfTokenOctetList;
        IfToken.i4_Length = 0;

        if (nmhGetFsipv6IfToken (u4Ip6IfIndex, &IfToken) == SNMP_FAILURE)
        {
            return;
        }

        MEMCPY (&Addr.pu1_OctetList[IP6_EUI_ADDRESS_LEN],
                IfToken.pu1_OctetList, IP6_EUI_ADDRESS_LEN);

        /* Also update the address type as UNICAST */
        i4AddrType = ADDR6_UNICAST;
    }

    /* ADMIN STATUS */
    if (nmhTestv2Fsipv6AddrAdminStatus (&u4ErrorCode,
                                        u4Ip6IfIndex, &Addr,
                                        i4PrefixLen, CREATE_AND_WAIT)
        == SNMP_FAILURE)
    {
        return;
    }

    /* First the Admin Status Should be set to CREATE_AND_WAIT */
    if (nmhSetFsipv6AddrAdminStatus (u4Ip6IfIndex,
                                     &Addr,
                                     i4PrefixLen,
                                     CREATE_AND_WAIT) == SNMP_FAILURE)
    {
        return;
    }

    /* ADDR TYPE - set to specified Address Type */
    if (nmhTestv2Fsipv6AddrType (&u4ErrorCode,
                                 u4Ip6IfIndex,
                                 &Addr,
                                 i4PrefixLen, i4AddrType) == SNMP_FAILURE)
    {
        nmhSetFsipv6AddrAdminStatus (u4Ip6IfIndex, &Addr, i4PrefixLen, DESTROY);
        return;
    }

    if (nmhSetFsipv6AddrType (u4Ip6IfIndex,
                              &Addr, i4PrefixLen, i4AddrType) == SNMP_FAILURE)
    {
        nmhSetFsipv6AddrAdminStatus (u4Ip6IfIndex, &Addr, i4PrefixLen, DESTROY);
        return;
    }

    /* ADMIN STATUS */
    if (nmhTestv2Fsipv6AddrAdminStatus (&u4ErrorCode,
                                        u4Ip6IfIndex, &Addr,
                                        i4PrefixLen, ACTIVE) == SNMP_FAILURE)
    {
        nmhSetFsipv6AddrAdminStatus (u4Ip6IfIndex, &Addr, i4PrefixLen, DESTROY);
        return;
    }

    /* First the Admin Status Should be set to 1 (up) */
    if (nmhSetFsipv6AddrAdminStatus (u4Ip6IfIndex,
                                     &Addr,
                                     i4PrefixLen, ACTIVE) == SNMP_FAILURE)

    {
        nmhSetFsipv6AddrAdminStatus (u4Ip6IfIndex, &Addr, i4PrefixLen, DESTROY);
        return;
    }

    return;
}

/*********************************************************************
*  Function Name : IPvxDeleteIPv6Add 
*  Description   : Deleting Ipv6 Address
*  Input(s)      : u4Ip6IfIndex - Interface Index
*                  Ip6Addr - Ipv6 Addressto be deleted
*                  i4PrefixLen - Prefix Length
*                  i4AddrType - Address type
*  Output(s)     :  
*  Return Values : None.
*********************************************************************/

VOID
IPvxDeleteIPv6Add (UINT4 u4Ip6IfIndex, tIp6Addr Ip6Addr,
                   UINT4 i4PrefixLen, INT4 i4AddrType)
{
    UINT4               u4ErrorCode = IPVX_ZERO;
    tSNMP_OCTET_STRING_TYPE Addr;
    tSNMP_OCTET_STRING_TYPE IfToken;
    UINT1               au1AddrOctetList[IP6_ADDR_SIZE];
    UINT1               au1IfTokenOctetList[IP6_EUI_ADDRESS_LEN];

    MEMSET (au1AddrOctetList, IP6_ZERO, IP6_ADDR_SIZE);
    MEMSET (au1IfTokenOctetList, IP6_ZERO, IP6_EUI_ADDRESS_LEN);

    Addr.pu1_OctetList = au1AddrOctetList;
    MEMCPY (Addr.pu1_OctetList, &Ip6Addr, IP6_ADDR_SIZE);
    Addr.i4_Length = IP6_ADDR_SIZE;

    /*
     * EUI-64 address configuration is supported only for CLI. When the
     * address type is set as EUI-64 form the fetch the interface
     * identifier value and form the complete IP6 address to be configured.
     */
    if (i4AddrType == ADDR6_UNSPECIFIED)
    {                            /* EUI-64 */
        /* Length should not be greater than 64. */
        if (i4PrefixLen != IP6_EUI_ADDRESS_PREFIX_LEN)
        {
            return;
        }

        /* Form the Actual IP6 Address */
        IfToken.pu1_OctetList = au1IfTokenOctetList;
        IfToken.i4_Length = IP6_ZERO;

        if (nmhGetFsipv6IfToken (u4Ip6IfIndex, &IfToken) == SNMP_FAILURE)
        {
            return;
        }

        MEMCPY (&Addr.pu1_OctetList[IP6_EUI_ADDRESS_LEN],
                IfToken.pu1_OctetList, IP6_EUI_ADDRESS_LEN);

        /* Also update the address type as UNICAST */
        i4AddrType = ADDR6_UNICAST;
    }

    if (i4AddrType != IP6_ZERO)
    {
        if ((nmhGetFsipv6AddrType ((INT4) u4Ip6IfIndex, &Addr, i4PrefixLen,
                                   &i4AddrType) == SNMP_FAILURE) ||
            (i4AddrType != i4AddrType))
        {
            return;
        }
    }

    /* ADMIN STATUS */
    if (nmhTestv2Fsipv6AddrAdminStatus (&u4ErrorCode,
                                        u4Ip6IfIndex, &Addr,
                                        i4PrefixLen,
                                        IP6FWD_DESTROY) == SNMP_FAILURE)
    {
        return;
    }

    /* First the Admin Status Should be set to 3 (invalid) */
    if (nmhSetFsipv6AddrAdminStatus (u4Ip6IfIndex,
                                     &Addr,
                                     i4PrefixLen,
                                     IP6FWD_DESTROY) == SNMP_FAILURE)

    {
        return;
    }

    return;
}

#endif /* __IP6_IPVX_C__ */

/****************************   End of the File ***************************/
