/*******************************************************************
 *   Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *   
 *    $Id: ip6inc.h,v 1.17 2014/04/10 13:27:04 siva Exp $
 ******************************************************************/


#ifndef _IP6INC_H
#define _IP6INC_H

/*
 *----------------------------------------------------------------------------- 
 *    FILE NAME                    :    ip6inc.h
 *
 *    PRINCIPAL AUTHOR             :    Vivek Venkatraman/Senthil Vadivu
 *
 *    SUBSYSTEM NAME               :    IPv6
 *
 *    MODULE NAME                  :    All Modules
 *
 *    LANGUAGE                     :    C
 *
 *    TARGET ENVIRONMENT           :    UNIX
 *
 *    DATE OF FIRST RELEASE        :    DDD-MM-1996
 *
 *    DESCRIPTION                  :    This file includes all the 
 *                                      IPv6 based header files.
 *
 *----------------------------------------------------------------------------- 
 */

/* IPv6 Include Files */
#include "lr.h"
#include "cfa.h"
#include "cust.h"
#include "ipv6.h"
#include "ip6.h"                /* IPv6 Protocol Specific Info */
#include "ip.h"
#include "ip6sys.h"                /* IPv6 System-related Info */
#include "ip6util.h"
#include "ripv6.h"

#include "trieinc.h"               /* Trie2 Related info */
#include "rtm6.h"                  /* RTM6 Info */

#include "stdip6wr.h"
#include "ip6snmp.h"            /* SNMP Info */
#include "ip6if.h"                /* IPv6 Interface Info */
#include "ip6addr.h"            /* IPv6 Address Info */
#include "ip6zone.h"            /* scope zone info -RFC4007*/
#include "ip6frag.h"            /* IPv6 Fragmentation Info */
#include "icmp6.h"                /* ICMP Info */
#include "iss.h"
#include "rmgr.h"
#include "nd6.h"                /* ND Info */
#include "udp6.h"                /* UDP Info */
#include "ip6rt.h"
#include "ping6.h"                /* PING Info */
#include "ip6pmtu.h"            /* Pmtu Info */
#include "ip6trace.h"            /* TRACE FILE */
#include "ip6dbg.h"                /* IPv6 Debug Info */
#include "traceroute.h"            /* IPv6 Trace Route */

#include "ip6glob.h"
#include "ip6ext.h"                /* IPv6 External Declarations Info */

#include "fssocket.h"
#include "snmccons.h"
#include "snmcdefn.h"
#include "snmctdfs.h"
#include "ip6proto.h"            /* IPv6 External Declarations Info */
#include "fsip6low.h"
#include "stdip6lw.h"
#include "fsip6sz.h"
#include "ping6sz.h"
#include "utlshinc.h"
#include "fssyslog.h"

#ifdef NPAPI_WANTED
#include "npapi.h"
#include "ip6np.h"
#include "ipnpwr.h"
#include "ipv6npwr.h"
#endif

#ifdef MLD_WANTED
#include "mld.h"
#endif

#ifdef TUNNEL_WANTED

#ifdef SLI_WANTED
# include "tcp.h"
# include "sli.h"
#endif

#endif

#ifdef MIP6_WANTED
#include "mipv6.h"
#endif

#ifdef IP6_TEST
#include "ip6tst.h"
#endif

#include "ip6port.h"               /* IPv6 porting related Info */

#include "ipvx.h"
#include "ip6ipvx.h"

#include "vcm.h"

#include "vrrp.h"

#ifdef HA_WANTED
#undef HA_WANTED
#endif

#endif /* !_IP6INC_H */
