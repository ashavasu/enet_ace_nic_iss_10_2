/*******************************************************************
 *  Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *   
 *  $Id: nd6out.c,v 1.20 2015/07/17 09:53:06 siva Exp $
 *   
 * ****************************************************************/

/*
 * +--------------------------------------------------------------------------+
 * |                                                                          |
 * |   FILE  NAME             :  nd6out.c                                     |
 * |                                                                          |
 * |   PRINCIPAL AUTHOR       :  N. Senthil Vadivu                            |
 * |                                                                          |
 * |   SUBSYSTEM NAME         :  IPv6                                         |
 * |                                                                          |
 * |   MODULE NAME            :  ND6 Module                                   |
 * |                                                                          |
 * |   LANGUAGE               :  C                                            |
 * |                                                                          |
 * |   TARGET ENVIRONMENT     :  UNIX                                         |
 * |                                                                          |
 * |   DATE OF FIRST RELEASE  :                                               |
 * |                                                                          |
 * |   DESCRIPTION            :  This file contains the ND routines handling  |
 * |                             Forming and Sending of ND messages           | 
 * |                                                                          |
 * +--------------------------------------------------------------------------+
 */

#include "ip6inc.h"

/*
 * Prototypes for Private Functions
 */

PRIVATE INT4        Nd6FillNeighSolInCxt
PROTO ((tIp6Cxt * pIp6Cxt, tIp6Addr * pTargAddr6,
        tCRU_BUF_CHAIN_HEADER * pBuf));

PRIVATE INT4        Nd6FillNeighAdvInCxt
PROTO ((tIp6Cxt * pIp6Cxt, tIp6Addr * pTargAddr6, UINT4 u4AdvFlag,
        tCRU_BUF_CHAIN_HEADER * pBuf));

/*****************************************************************************
* DESCRIPTION : Checks the Reachability state and the Max Solicit 
*               status for a NDCache entry and decides on sending 
*               the Neighbor Solicitation message
* 
* INPUTS      : pNd6cEntry    -  Pointer to the NDCache Entry
* 
* OUTPUTS     : None
* 
* RETURNS     : IP6_SUCCESS          -  NS is sent
*             : IP6_FAILURE      -  NS is not sent
****************************************************************************/

UINT4 gNoOfNSRetrial_1 = 0;
UINT4 gNoOfNSRetrial_2 = 0;
UINT4 gNoOfNSRetrial_3 = 0;


INT4
Nd6CheckAndSendNeighSol (tNd6CacheEntry * pNd6cEntry, tIp6Addr * pSrcAddr6)
{
    tIp6If             *pIf6 = NULL;
    tIp6Cxt            *pIp6Cxt = NULL;
    tNd6CacheLanInfo   *pNd6cLinfo = NULL;
    tIp6Addr            dstAddr6, targAddr6;
    UINT4               u4IfReTransTime = RETRANS_TIMER;

    if (pNd6cEntry == NULL)
    {
        return (IP6_FAILURE);
    }

    if ((pNd6cEntry->u1ReachState != ND6C_INCOMPLETE) &&
        (pNd6cEntry->u1ReachState != ND6C_PROBE))
    {
        return (IP6_FAILURE);
    }

    pIf6 = pNd6cEntry->pIf6;
    pIp6Cxt = pIf6->pIp6Cxt;
    pNd6cLinfo = (tNd6CacheLanInfo *) (VOID *) pNd6cEntry->pNd6cInfo;

    /*
     * Get the Source, Destination and Target addresses to be filled
     * in the NS message
     */
    if (pNd6cEntry->u1ReachState == ND6C_INCOMPLETE)
    {
        /* Check whether MAX_MULTICAST_SOLICIT attempts are made */
        if (pNd6cLinfo->u4Retries > (UINT4) pIp6Cxt->i4Nd6CacheMaxRetries)
        {
            if (pNd6cEntry->u1AddrType == ADDR6_ANYCAST)
            {
                Nd6GetAnycastcache (pNd6cEntry);
                return (IP6_SUCCESS);
            }
            if (pNd6cLinfo->u4Retries == (((UINT4) pIp6Cxt->i4Nd6CacheMaxRetries)+IP6_TWO))
            {
                /* ND Throttling: * Reinstall the drop route 
                 * programmed in hardware as local route after
                 * max-retries of NS */

                 Rtm6ApiAddLocalRouteInNP (pIp6Cxt->u4ContextId,
                                      &(pNd6cEntry->addr6));
                 IP6_TRC_ARG (pIp6Cxt->u4ContextId, ND6_MOD_TRC, ALL_FAILURE_TRC,
                         ND6_NAME,
                         "ND6:Nd6CheckAndSendNeighSol: Send NS Failed - Max "
                         "MULTICAST Retries Attempted\n");
                 return (IP6_FAILURE);
            }
            u4IfReTransTime = Ip6GetIfReTransTime (pIf6->u4Index);
            u4IfReTransTime = (UINT4)((IP6_TWO << (u4IfReTransTime - IP6_ONE)) + IP6_ONE) * IP6_THOUSAND;
            pNd6cLinfo->u4Retries++;
            Nd6SetMSecCacheTimer (pNd6cEntry, ND6_RETRANS_TIMER_ID, u4IfReTransTime);
            return (IP6_SUCCESS);

       }
#ifdef TUNNEL_WANTED
        if (pIf6->u1IfType == IP6_TUNNEL_INTERFACE_TYPE)
        {
            /* On  tunnel interfaces only NUD are done. For NUD the destination
             * address should be global unicast address */
            Ip6AddrCopy (&dstAddr6, &pNd6cEntry->addr6);
        }
        else
#endif
            GET_ADDR_SOLICITED (pNd6cEntry->addr6, dstAddr6);
    }
    else if (pNd6cEntry->u1ReachState == ND6C_PROBE)
    {
        /* Check whether MAX_UNICAST_SOLICIT attempts are made */
         if (pNd6cLinfo->u4Retries > (UINT4) pIp6Cxt->i4Nd6CacheMaxRetries)
        { 
           if (pNd6cLinfo->u4Retries >= (((UINT4) pIp6Cxt->i4Nd6CacheMaxRetries)+IP6_TWO))                                                                              
           {
            IP6_TRC_ARG (pIp6Cxt->u4ContextId, ND6_MOD_TRC, ALL_FAILURE_TRC,
                         ND6_NAME,
                         "ND6:Nd6CheckAndSendNeighSol: Send NS Failed - "
                         "Max UNICAST Retries Attempted\n");
            return (IP6_FAILURE);
        }
           u4IfReTransTime = Ip6GetIfReTransTime (pIf6->u4Index);
           u4IfReTransTime =  (UINT4) ((IP6_TWO << (u4IfReTransTime - IP6_ONE)) + IP6_ONE) * IP6_THOUSAND;
           pNd6cLinfo->u4Retries++;
           Nd6SetMSecCacheTimer (pNd6cEntry, ND6_RETRANS_TIMER_ID, u4IfReTransTime);
           return (IP6_SUCCESS);

       }
        Ip6AddrCopy (&dstAddr6, &pNd6cEntry->addr6);
    }

    Ip6AddrCopy (&targAddr6, &pNd6cEntry->addr6);
    if (Nd6SendNeighSol (pNd6cEntry, pIf6, pSrcAddr6,
                         &dstAddr6, &targAddr6) == IP6_FAILURE)
    {
        return (IP6_FAILURE);
    }
    (IP6_SEND_IF_OUT_STATS (pIf6, COUNT_NS)).u4CgaOptPkts++;
    (IP6_SEND_IF_OUT_STATS (pIf6, COUNT_NS)).u4TimeStampOptPkts++;
    (IP6_SEND_IF_OUT_STATS (pIf6, COUNT_NS)).u4NonceOptPkts++;
    (IP6_SEND_IF_OUT_STATS (pIf6, COUNT_NS)).u4RsaOptPkts++;
    pNd6cLinfo->u4Retries++;

    u4IfReTransTime = Ip6GetIfReTransTime (pIf6->u4Index);


    /* Check Range for Exponential Back-off Retransmission Timer */
    /*     Range       1st Retry   2nd Retry  3rd Retry */
    /*       1              3        5            9     */
    /*       2              5        9            17    */
    /*       3              9        17           33    */
    /*       4             17        33           65    */
    /*       5             33        65           129   */
    /*       6             65        129          257   */
    /*       7            129        257          513   */
    /*       8            257        513          1025  */
    /*       9            513        1025         2049  */
   if (pNd6cLinfo->u4Retries == 1) 
   {
       gNoOfNSRetrial_1++;
       u4IfReTransTime = (UINT4) ((IP6_TWO << (u4IfReTransTime - IP6_ONE)) + IP6_ONE) * IP6_THOUSAND;
   }
   if (pNd6cLinfo->u4Retries == 2)
   {
       gNoOfNSRetrial_2++;
       u4IfReTransTime = (UINT4) ((IP6_TWO << (u4IfReTransTime + IP6_ZERO)) + IP6_ONE) * IP6_THOUSAND;
   }
   if (pNd6cLinfo->u4Retries == 3)
   {
       gNoOfNSRetrial_3++;
            if (u4IfReTransTime != IP6_MAX_RETR_RANGE)
            {
              u4IfReTransTime = (UINT4) ((IP6_TWO << (u4IfReTransTime + IP6_ONE)) + IP6_ONE) * IP6_THOUSAND;
            }
            else
            {
               u4IfReTransTime = IP6_IF_MAX_RETTIME;
            }
   }
    Nd6SetMSecCacheTimer (pNd6cEntry, ND6_RETRANS_TIMER_ID, u4IfReTransTime);

    return (IP6_SUCCESS);

}

/*****************************************************************************
* DESCRIPTION : Forms the ND Neighbor Solicitation message and 
*               sends it to the lower layer for transmission
* 
* INPUTS      : pNd6cEntry   -  Pointer to the NDCache Entry
*               pIf6          -  Pointer to the interface 
*               pSrcAddr6    -  Pointer to IPv6 source address
*               pDstAddr6    -  Pointer to IPv6 destination address
*               pTargAddr6   -  Pointer to IPv6 target address
*
* OUTPUTS     : None
*
* RETURNS     : IP6_SUCCESS         -  Sending of NS succeeds
*               IP6_FAILURE     -  Sending of NS fails
****************************************************************************/
INT4
Nd6SendNeighSol (tNd6CacheEntry * pNd6cEntry, tIp6If * pIf6,
                 tIp6Addr * pSrcAddr6, tIp6Addr * pDstAddr6,
                 tIp6Addr * pTargAddr6)
{
    UINT1               u1LlaLen = 0, au1lladdr[IP6_MAX_LLA_LEN];
    UINT4               u4Nd6Size = 0, u4_tot_size = 0, u4Woffset = 0;
    UINT4               u4ContextId = pIf6->pIp6Cxt->u4ContextId;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    INT4                i4VrId = 0;
    UINT1               u1SeNDFlag = 0;
#ifdef VRRP_WANTED
    tIPvXAddr           IpAddr;
#endif

#ifdef VRRP_WANTED
    IPVX_ADDR_CLEAR (&IpAddr);

    /* When sending neighbor soliciation message, link layer address should be
     * filled with virtual MAC address if VRRP is enabled on that. */
    IPVX_ADDR_INIT_FROMV6 (IpAddr, pSrcAddr6);

    if (VrrpGetVridFromAssocIpAddress (pIf6->u4Index, IpAddr, &i4VrId)
        == VRRP_NOT_OK)
    {
        return IP6_FAILURE;
    }
#endif

    u4Woffset = Ip6BufWoffset (ND6_MODULE);

    /* Allocate a Buffer for the IPv6 packet size of NS message */
    u4Nd6Size = sizeof (tNd6NeighSol) + sizeof (tNd6AddrExt);
    u4_tot_size = u4Woffset + u4Nd6Size;

    /*
     * If SeND is enabled, increase the pBuf size to accommodate
     * all the SeND options
     */
    if (IP6_TRUE == IF_SEND_ENABLED (pIf6))
    {
        u4_tot_size += ND6_SEND_OPTIONS_LENGTH;
    }

    if ((pBuf = Ip6BufAlloc (u4ContextId, u4_tot_size,
                             u4Woffset, ND6_MODULE)) == NULL)
    {
        IP6_TRC_ARG (u4ContextId, ND6_MOD_TRC, BUFFER_TRC | ALL_FAILURE_TRC,
                     ND6_NAME,
                     "ND6:Nd6SendNeighSol: Send NS - BufAlloc Failed\n");
        IP6_TRC_ARG3 (u4ContextId, ND6_MOD_TRC, BUFFER_TRC, IP6_NAME,
                      "ND6:Nd6SendNeighSol: %s buf err: Type = %d  Bufptr = %p\n",
                      ERROR_FATAL_STR, ERR_BUF_ALLOC, NULL);
        return (IP6_FAILURE);
    }

    /* Fill the NS Message fields */
    /* In the case of failure buffer is released inside the function */
    if (Nd6FillNeighSolInCxt (pIf6->pIp6Cxt, pTargAddr6, pBuf) == IP6_FAILURE)
    {
        return (IP6_FAILURE);
    }

    if (i4VrId != 0)
    {
#ifdef VRRP_WANTED
        VrrpGetMacAddress (pIf6->u4Index, i4VrId, IPVX_ADDR_FMLY_IPV6,
                           au1lladdr);
#endif
        u1LlaLen = MAC_ADDR_LEN;
    }
    else
    {
        Ip6ifGetEthLladdr (pIf6, au1lladdr, &u1LlaLen);
    }

    /* Get the source address of the packet if it is passed as NULL */
    if (pSrcAddr6 == NULL)
    {
        if ((pSrcAddr6 = Ip6AddrGetSrc (pIf6, pTargAddr6)) == NULL)
        {
            IP6_TRC_ARG (u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC, ND6_NAME,
                         "ND6:Nd6SendNeighSol: Send NS - SrcAddr is NULL\n");
            IP6_TRC_ARG2 (u4ContextId, ND6_MOD_TRC, ALL_FAILURE_TRC, IP6_NAME,
                          "ND6:Nd6SendNeighSol: %s gen err: Type = %d \n",
                          ERROR_MINOR_STR, ERR_GEN_NULL_PTR);
            Ip6BufRelease (u4ContextId, pBuf, FALSE, ND6_MODULE);
            return (IP6_FAILURE);
        }
    }
    /*
     * Here a check is to be made, such that - if the v6 Source Address
     * is unspecified, do not use the link-layer address option.
     * So, added additional check so that, The LL Address option would
     * be filled only if the Source address is not Unspecified.
     */
    if (pSrcAddr6 != NULL)
    {
        if (!(IS_ADDR_UNSPECIFIED (*pSrcAddr6)))
        {
            if (Nd6FillLladdrInCxt (pIf6->pIp6Cxt, au1lladdr,
                                    ND6_SRC_LLA_EXT, pBuf) == IP6_FAILURE)
            {
                return (IP6_FAILURE);
            }

            if (IP6_TRUE == IF_SEND_ENABLED (pIf6))
    	    {
            	(IP6_SEND_IF_OUT_STATS 
		        (pIf6, COUNT_NS)).u4SrcLinkAddrPkts++;
	        }
        }
        else
        {
            u4Nd6Size = u4Nd6Size - sizeof (tNd6AddrExt);
            u4_tot_size = u4_tot_size - sizeof (tNd6AddrExt);
        }
    }

    ICMP6_INC_OUT_NSOLS (pIf6->pIp6Cxt->Icmp6Stats);
    ICMP6_INTF_INC_OUT_NSOLS (pIf6);
    (IP6_IF_STATS (pIf6))->u4OutNsols++;

    /* Send the NS message over the interface */

    IP6_TRC_ARG6 (u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC, ND6_NAME,
                  "ND6:Nd6SendNeighSol: SendingPkt IFPtr%p Len%d Type%d "
                  "BufPtr %p SrcAdr%s DstAdr%s \n",
                  pIf6, u4Nd6Size, ND6_NEIGHBOR_SOLICITATION, pBuf,
                  Ip6PrintAddr (pSrcAddr6), Ip6PrintAddr (pDstAddr6));

    IP6_TRC_ARG1 (u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC, ND6_NAME,
                  "ND6:Nd6SendNeighSol: TarAdr %s \n", Ip6PrintAddr (pTargAddr6));

#ifdef TUNNEL_WANTED
    if (pIf6->u1IfType == IP6_TUNNEL_INTERFACE_TYPE)
        /*Buffer is released in side the function */
        return (Ip6SendNdMsgOverTunl (pIf6, pNd6cEntry, pSrcAddr6,
                                      pDstAddr6, u4Nd6Size, pBuf));
    else
#endif
        /*Buffer is released in side the function */
    if (IP6_TRUE == IF_SEND_ENABLED (pIf6))
    {
        u1SeNDFlag = ND6_SEND_MSG;
    }
    return (Ip6SendNdMsg (pIf6, pNd6cEntry, pSrcAddr6,
                          pDstAddr6, u4Nd6Size, pBuf,
                          u1SeNDFlag, pTargAddr6));

}

/*****************************************************************************
* DESCRIPTION : Fill the NS message fields 
*
* INPUTS      : pTargAddr6  -  Pointer to the Target address.
*               pBuf        - Pointer to the message buffer. 
*
* OUTPUTS     : None.
* 
* RETURNS     : IP6_SUCCESS or IP6_FAILURE.
*****************************************************************************/
PRIVATE INT4
Nd6FillNeighSolInCxt (tIp6Cxt * pIp6Cxt, tIp6Addr * pTargAddr6,
                      tCRU_BUF_CHAIN_HEADER * pBuf)
{
    UINT1               u1Copy = 1;
    tNd6NeighSol        nd6Nsol, *pNd6Nsol = NULL;
    pNd6Nsol = (tNd6NeighSol *) & nd6Nsol;
    pNd6Nsol->icmp6Hdr.u1Type = ND6_NEIGHBOR_SOLICITATION;
    pNd6Nsol->icmp6Hdr.u1Code = ND6_RSVD_CODE;
    pNd6Nsol->icmp6Hdr.u2Chksum = 0;
    pNd6Nsol->u4Rsvd = 0;
    Ip6AddrCopy (&pNd6Nsol->targAddr6, pTargAddr6);

    if (Ip6BufWrite (pBuf,
                     (UINT1 *) pNd6Nsol, IP6_BUF_WRITE_OFFSET (pBuf),
                     sizeof (tNd6NeighSol), u1Copy) == IP6_FAILURE)
    {
        IP6_TRC_ARG2 (pIp6Cxt->u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC,
                      ND6_NAME,
                      "ND6:Nd6FillNeighSol: Fill NS - BufWrite Failed!  BufPtr %p Offset %d\n",
                      pBuf, IP6_BUF_WRITE_OFFSET (pBuf));
        IP6_TRC_ARG3 (pIp6Cxt->u4ContextId, ND6_MOD_TRC, BUFFER_TRC, IP6_NAME,
                      "ND6:Nd6FillNeighSol: %s buf err: Type = %d  Bufptr = %p",
                      ERROR_FATAL_STR, ERR_BUF_WRITE, pBuf);
        Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE, ND6_MODULE);
        return (IP6_FAILURE);
    }

    return (IP6_SUCCESS);

}

/*****************************************************************************
* DESCRIPTION : This routine sends the Solicited NA as response to the 
*               received NS message

*
* INPUTS      : pIf6        -  Pointer to the interface.
*               pIp6        -  Pointer to the Ipv6 header. 
*               pNd6Nsol    -  Pointer to the Neigh Solicitation.
*
* OUTPUTS     : None.
* 
* RETURNS     : IP6_SUCCESS or IP6_FAILURE.
*****************************************************************************/
INT4
Nd6SendSolNeighAdv (tIp6If * pIf6, tIp6Hdr * pIp6, tNd6NeighSol * pNd6Nsol)
{
    tIp6Addr            dstAddr6;
    tNd6CacheEntry     *pNd6cEntry = NULL;
    UINT4               u4AdvFlag = 0;
    INT4                i4AddrType = 0;
    INT4                i4RetVal = 0;

    MEMSET(&dstAddr6, IP6_ZERO, sizeof (tIp6Addr));

    /* 
     * Destination address in NA is set to all-nodes Multicast if
     * the source address of NS being unspecified, otherwise it is
     * set to the source address of NS
     */
    if (IS_ADDR_UNSPECIFIED (pIp6->srcAddr))
    {
        SET_ALL_NODES_MULTI (dstAddr6)
    }
    else
    {
        Ip6AddrCopy (&dstAddr6, &pIp6->srcAddr);
        pNd6cEntry = Nd6IsCacheForAddr (pIf6, &dstAddr6);
        u4AdvFlag = ND6_SOLICITED_FLAG;
    }

    /*
     * Default Router flag is set based on the configured status in the
     * router. Solicited flag is set in the NA message and Override flag is
     * set for target being an unicast address and is not set for anycast
     * address
     */
    if (pIf6->u1Ipv6IfFwdOperStatus == IP6_IF_FORW_ENABLE)
    {
        u4AdvFlag = u4AdvFlag | ND6_DEFAULT_ROUTER;
    }

    /*
     * All solicited proxy NA or ANYCAST address NA MUST have the Override
     * Flag set to zero. So, check if this is Proxy NA or ANYCAST address NA,
     *  before setting the Override Flag.
     */
    i4AddrType = Ip6GetUniAddrType (pIf6, &pNd6Nsol->targAddr6);
    if (((i4AddrType == ADDR6_UNICAST)
         && (Nd6ProxyCheckAddrInCxt (pIf6->pIp6Cxt, pNd6Nsol->targAddr6)
             != NULL)) || (i4AddrType == ADDR6_ANYCAST))
    {
        u4AdvFlag &= (UINT4)~ND6_OVERRIDE_FLAG;
    }
    else
    {
        u4AdvFlag |= ND6_OVERRIDE_FLAG;
    }

    /* Send the NA message */
    if (IS_ADDR_MULTI (pIp6->dstAddr))
    {
        i4RetVal = Nd6SendNeighAdv
            (pIf6, pNd6cEntry, NULL,
             &dstAddr6, &pNd6Nsol->targAddr6, u4AdvFlag);
        return i4RetVal;
    }
    i4RetVal = Nd6SendNeighAdv (pIf6, pNd6cEntry, &pIp6->dstAddr,
                                &dstAddr6, &pNd6Nsol->targAddr6, u4AdvFlag);
    return i4RetVal;
}

/*****************************************************************************
* DESCRIPTION : Forms the ND Neighbor Advertisement message and 
*               sends it to the lower layer for transmission
* 
* INPUTS      : pIf6         -  Pointer to the interface 
*               pNd6cEntry  -  Pointer to the NDCache Entry
*               pSrcAddr6   -  Pointer to IPv6 source address
*               pDstAddr6   -  Pointer to IPv6 destination address
*               pTargAddr6  -  Pointer to IPv6 target address
*               u4AdvFlag   -  Flag indicating solicited/over-ride
*                                status
*
* OUTPUTS     : None
*
* RETURNS     : IP6_SUCCESS        -  Sending of NA succeeds
*               IP6_FAILURE    -  Sending of NA fails
****************************************************************************/
INT4
Nd6SendNeighAdv (tIp6If * pIf6, tNd6CacheEntry * pNd6cEntry,
                 tIp6Addr * pSrcAddr6, tIp6Addr * pDstAddr6,
                 tIp6Addr * pTargAddr6, UINT4 u4AdvFlag)
{
    UINT1               u1LlaLen = 0, au1lladdr[IP6_MAX_LLA_LEN];
    UINT4               u4Nd6Size = 0, u4_tot_size = 0, u4Woffset = 0;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    INT4                i4VrId = 0;
    UINT1               u1SeNDFlag = 0;
#ifdef VRRP_WANTED
    tIPvXAddr           IpAddr;
#endif
#ifdef EVPN_VXLAN_WANTED
    tNd6CacheEntry      Nd6cEntry;

    MEMSET (&Nd6cEntry, 0, sizeof (tNd6CacheEntry));
#endif

#ifdef VRRP_WANTED
    IPVX_ADDR_CLEAR (&IpAddr);

    IPVX_ADDR_INIT_FROMV6 (IpAddr, pTargAddr6);

    if (VrrpGetVridFromAssocIpAddress (pIf6->u4Index, IpAddr, &i4VrId)
        == VRRP_NOT_OK)
    {
        return IP6_FAILURE;
    }
#endif

    u4Woffset = Ip6BufWoffset (ND6_MODULE);

    /* Allocate a buffer for the IPv6 packet size of NA message */
    u4Nd6Size = sizeof (tNd6NeighAdv) + sizeof (tNd6AddrExt);

    u4_tot_size = u4Woffset + u4Nd6Size;

    /*  
     * If SeND is enabled, increase the pBuf size to accommodate
     * all the SeND options
     */
    if (IP6_TRUE == IF_SEND_ENABLED (pIf6))
    {
        u4_tot_size += ND6_SEND_OPTIONS_LENGTH;
        if (u4AdvFlag & ND6_SOLICITED_FLAG)
        {
            u1SeNDFlag = ND6_SOLICITED_MSG;
        }
        else
        {
            u1SeNDFlag = ND6_UNSOLICITED_MSG;
        }
    }

    if ((pBuf = Ip6BufAlloc (pIf6->pIp6Cxt->u4ContextId, u4_tot_size,
                             u4Woffset, ND6_MODULE)) == NULL)
    {
        IP6_TRC_ARG (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                     BUFFER_TRC | ALL_FAILURE_TRC, ND6_NAME,
                     "ND6:Nd6SendNeighAdv: Send NA - BufAlloc Failed\n");
        IP6_TRC_ARG3 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC, BUFFER_TRC,
                      IP6_NAME,
                      "ND6:Nd6SendNeighAdv: %s buf err: Type = %d  Bufptr = %p",
                      ERROR_FATAL_STR, ERR_BUF_ALLOC, NULL);
        return (IP6_FAILURE);
    }

    /* Fill the NA Message fields */
    if (Nd6FillNeighAdvInCxt (pIf6->pIp6Cxt, pTargAddr6, u4AdvFlag, pBuf)
        == IP6_FAILURE)
    {
        return (IP6_FAILURE);
    }

    if (i4VrId != 0)
    {
#ifdef VRRP_WANTED
        VrrpGetMacAddress (pIf6->u4Index, i4VrId, IPVX_ADDR_FMLY_IPV6,
                           au1lladdr);
#endif
        u1LlaLen = MAC_ADDR_LEN;
    }
    else
    {
        Ip6ifGetEthLladdr (pIf6, au1lladdr, &u1LlaLen);
    }
#ifdef EVPN_VXLAN_WANTED
    if (Nd6IsCacheForAddrLearntFromEvpn (pTargAddr6, &Nd6cEntry)
           == IP6_SUCCESS)
    {
        MEMCPY (au1lladdr, Nd6cEntry.pNd6cInfo, IP6_MAX_LLA_LEN);
        u1LlaLen = IP6_MAX_LLA_LEN;
    }
#endif

/*The neighbor sends NS to anycast address during probe state, instead of solicited nulticast node address , then NA is sent with source address as anycast address, to avoid that we try to fetch some valid unicast address */

    /* Get the source address of the packet if it is passed as NULL */
    if ((pSrcAddr6 == NULL)
        || (Ip6IsAddrAnycastInCxt (pIf6->pIp6Cxt, pSrcAddr6) == IP6_SUCCESS))
    {
        if ((pSrcAddr6 = Ip6AddrGetSrc (pIf6, pTargAddr6)) == NULL)
        {
            IP6_TRC_ARG (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC, MGMT_TRC,
                         ND6_NAME,
                         "ND6:Nd6SendNeighAdv: Send NA - SrcAddr is NULL\n");
            IP6_TRC_ARG2 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                          ALL_FAILURE_TRC, IP6_NAME,
                          "ND6:Nd6SendNeighAdv: %s gen err: Type = %d \n",
                          ERROR_MINOR_STR, ERR_GEN_NULL_PTR);
            Ip6BufRelease (pIf6->pIp6Cxt->u4ContextId, pBuf, FALSE, ND6_MODULE);
            return (IP6_FAILURE);
        }
    }

    if (!(IS_ADDR_UNSPECIFIED (*pSrcAddr6)))
    {
        if (Nd6FillLladdrInCxt (pIf6->pIp6Cxt, au1lladdr,
                                ND6_TARG_LLA_EXT, pBuf) == IP6_FAILURE)
        {
            return (IP6_FAILURE);
        }
	
	    if (IP6_TRUE == IF_SEND_ENABLED (pIf6))
    	{
	        (IP6_SEND_IF_OUT_STATS
                (pIf6, COUNT_NS)).u4SrcLinkAddrPkts++;
    	}
    }

    ICMP6_INC_OUT_NADVS (pIf6->pIp6Cxt->Icmp6Stats);
    ICMP6_INTF_INC_OUT_NADVS (pIf6);
    (IP6_IF_STATS (pIf6))->u4OutNadvs++;

    if (u4AdvFlag & ND6_OVERRIDE_FLAG)
    {
        ICMP6_INC_OUT_NA_OFLAG (pIf6->pIp6Cxt->Icmp6Stats);
    }
    if (u4AdvFlag & ND6_DEFAULT_ROUTER)
    {
        ICMP6_INC_OUT_NA_RFLAG (pIf6->pIp6Cxt->Icmp6Stats);
    }
    if (u4AdvFlag & ND6_SOLICITED_FLAG)
    {
        ICMP6_INC_OUT_NA_SFLAG (pIf6->pIp6Cxt->Icmp6Stats);
    }
    /* Send the NA message over the interface */

    IP6_TRC_ARG6 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC,
                  ND6_NAME,
                  "ND6:Nd6SendNeighAdv: Send Pkt IFPtr%p Len%d Type%d Bufptr%p SrcAdr%s DstAdr%s\n",
                  pIf6, u4Nd6Size, ND6_NEIGHBOR_ADVERTISEMENT, pBuf,
                  Ip6PrintAddr (pSrcAddr6), Ip6PrintAddr (pDstAddr6));
    IP6_TRC_ARG1 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC,
                  ND6_NAME, "ND6:Nd6SendNeighAdv: TarAdr %s \n",
                  Ip6PrintAddr (pTargAddr6));
#ifdef TUNNEL_WANTED
    /* In case of tunnel interface pkt need to be enqueued to V4 task */
    if (pIf6->u1IfType == IP6_TUNNEL_INTERFACE_TYPE)
    {
        return (Ip6SendNdMsgOverTunl (pIf6, pNd6cEntry, pSrcAddr6,
                                      pDstAddr6, u4Nd6Size, pBuf));
    }
    else
#endif
    {
        return (Ip6SendNdMsg
                (pIf6, pNd6cEntry, pSrcAddr6, pDstAddr6, u4Nd6Size, pBuf,
                 u1SeNDFlag, pTargAddr6));
    }

}

/*****************************************************************************
* DESCRIPTION : Fill the NA message fields 
*
* INPUTS      : pTargAddr6  -  Pointer to the target address.
*               u4AdvFlag   -  R-S-O flag(router, solicited, and override)
*               pBuf        -  Pointer to the message buffer.
*
* OUTPUTS     : None.
* 
* RETURNS     : IP6_SUCCESS or IP6_FAILURE.
*****************************************************************************/
PRIVATE INT4
Nd6FillNeighAdvInCxt (tIp6Cxt * pIp6Cxt, tIp6Addr * pTargAddr6, UINT4 u4AdvFlag,
                      tCRU_BUF_CHAIN_HEADER * pBuf)
{
    UINT1               u1Copy = 1;
    tNd6NeighAdv        nd6Nadv, *pNd6Nadv = NULL;
    pNd6Nadv = (tNd6NeighAdv *) & nd6Nadv;
    pNd6Nadv->icmp6Hdr.u1Type = ND6_NEIGHBOR_ADVERTISEMENT;
    pNd6Nadv->icmp6Hdr.u1Code = ND6_RSVD_CODE;
    pNd6Nadv->icmp6Hdr.u2Chksum = 0;
    pNd6Nadv->u4AdvFlag = HTONL (u4AdvFlag);
    Ip6AddrCopy (&pNd6Nadv->targAddr6, pTargAddr6);

    if (Ip6BufWrite (pBuf,
                     (UINT1 *) pNd6Nadv, IP6_BUF_WRITE_OFFSET (pBuf),
                     sizeof (tNd6NeighAdv), u1Copy) == IP6_FAILURE)
    {
        IP6_TRC_ARG2 (pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                      BUFFER_TRC | ALL_FAILURE_TRC, ND6_NAME,
                      "ND6:Nd6FillNeighAdv: Fill NA - BufWrite Failed! BufPtr "
                      "%p Offset %d\n", pBuf, IP6_BUF_WRITE_OFFSET (pBuf));
        IP6_TRC_ARG3 (pIp6Cxt->u4ContextId, ND6_MOD_TRC, BUFFER_TRC, IP6_NAME,
                      "ND6:Nd6FillNeighAdv: %s buf err: Type = %d  Bufptr = %p",
                      ERROR_FATAL_STR, ERR_BUF_WRITE, pBuf);
        Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE, ND6_MODULE);
        return (IP6_FAILURE);
    }

    return (IP6_SUCCESS);

}

/*****************************************************************************
* DESCRIPTION : Fill the Link layer address option fields 
*
* INPUTS      : pLladdr     -  Pointer to the Link local address.
*               u1LlaType   -  ND extention header options type.  
*               pBuf        -  Pointer to the message buffer.
*
* OUTPUTS     : None.
* 
* RETURNS     : IP6_SUCCESS or IP6_FAILURE.
*****************************************************************************/
INT4
Nd6FillLladdrInCxt (tIp6Cxt * pIp6Cxt, UINT1 *pLladdr, UINT1 u1LlaType,
                    tCRU_BUF_CHAIN_HEADER * pBuf)
{
    UINT1               u1Copy = 1;
    tNd6AddrExt         nd6Lladdr, *p_nd6_lladdr = NULL;

    p_nd6_lladdr = (tNd6AddrExt *) & nd6Lladdr;
    p_nd6_lladdr->u1Type = u1LlaType;
    p_nd6_lladdr->u1Len = ND6_LLA_EXT_LEN;    /* multiples of 8 octets */
    MEMCPY (p_nd6_lladdr->lladdr, pLladdr, IP6_ENET_ADDR_LEN);

    if (Ip6BufWrite (pBuf,
                     (UINT1 *) p_nd6_lladdr, IP6_BUF_WRITE_OFFSET (pBuf),
                     sizeof (tNd6AddrExt), u1Copy) == IP6_FAILURE)
    {
        IP6_TRC_ARG2 (pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                      BUFFER_TRC | ALL_FAILURE_TRC, ND6_NAME,
                      "ND6:Nd6FillLladdr: Fill LLAddr - BufWrite Failed! "
                      "BufPtr %p Offset %d\n", pBuf,
                      IP6_BUF_WRITE_OFFSET (pBuf));
        IP6_TRC_ARG3 (pIp6Cxt->u4ContextId, ND6_MOD_TRC, BUFFER_TRC, IP6_NAME,
                      "ND6:Nd6FillLladdr: %s buf err: Type = %d  Bufptr = %p",
                      ERROR_FATAL_STR, ERR_BUF_WRITE, pBuf);
        Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE, ND6_MODULE);
        return (IP6_FAILURE);
    }

    return (IP6_SUCCESS);

}

/***************************** END OF FILE **********************************/
