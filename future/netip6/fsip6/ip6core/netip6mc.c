/********************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: netip6mc.c,v 1.1 2010/09/23 12:11:47 prabuc Exp $
 *
 * Description:This file holds the APIs for IPv6 multicast routing 
 *             support
 *
 *******************************************************************/
#ifndef __NETIP6MC_C__
#define __NETIP6MC_C__
#include "ip6inc.h"

/****************************************************************************
* FUNCTION NAME    : NetIpv6McastSetMcStatusOnIface 
* DESCRIPTION      : Enables/Disables multicast routing on an interface
*                    according to u4McastStatus. 
* INPUT            : pNetIp6McastInfo: contains the ipport and the 
*                    corresponding multicast routing information 
*                    u4McastStatus : NETIPV6_ENABLED/NETIPV6_DISABLED
* OUTPUT           : None
* RETURNS          : NETIPV6_SUCCESS/NETIPV6_FAILURE
****************************************************************************/
INT4
NetIpv6McastSetMcStatusOnIface (tNetIp6McastInfo * pNetIp6McastInfo,
                                UINT4 u4McastStatus)
{
    UNUSED_PARAM (pNetIp6McastInfo);
    UNUSED_PARAM (u4McastStatus);

    return NETIPV6_SUCCESS;
}

/****************************************************************************
* FUNCTION NAME    : NetIpv6McastRouteUpdate
* DESCRIPTION      : Adds/deletes multicast route as per u4RouteFlag.
* INPUT            : pIp6McastRoute: Route information
*                    u4RouteFlag : NETIPV6_ADD_ROUTE/NETIPV6_DELETE_ROUTE
* OUTPUT           : None.
* RETURNS          : NETIPV6_SUCCESS/NETIPV6_FAILURE
****************************************************************************/
INT4
NetIpv6McastRouteUpdate (tNetIp6McRouteInfo * pIp6McastRoute, UINT4 u4RouteFlag)
{
    UNUSED_PARAM (pIp6McastRoute);
    UNUSED_PARAM (u4RouteFlag);

    return NETIPV6_SUCCESS;
}

/****************************************************************************
* FUNCTION NAME    : NetIpv6McastUpdateCpuPortStatus
* DESCRIPTION      : Creates/Deletes CPU Port into/from LinuxIp6 when PIM
*                    is enabled/disabled, as per the value of u4CpuPortStatus.
* INPUT            : u4CpuPortStatus :
*                    ENABLED - Creates MIF in LnxIp6 for CpuPort.
*                    DISABLED -Deletes MIF for CPUPort from LnxIp6.
* OUTPUT           : None
* RETURNS          : NETIPV6_SUCCESS/NETIPV6_FAILURE
****************************************************************************/
INT4
NetIpv6McastUpdateCpuPortStatus (UINT4 u4CpuPortStatus)
{
    UNUSED_PARAM (u4CpuPortStatus);
    return NETIPV6_SUCCESS;
}

/****************************************************************************
* FUNCTION NAME    : NetIpv6McastUpdateRouteCpuPort
* DESCRIPTION      : Adds/Deletes CPU Port into/from LinuxIp6 route entry
*                    Oif list as per the value of u4CpuPortStatus.
* INPUT            : tNetIp6McRouteInfo * : Multicast route info
*                    UINT4 : CPU port status. 
* OUTPUT           : None
* RETURNS          : NETIPV6_SUCCESS/NETIPV6_FAILURE
****************************************************************************/
INT4
NetIpv6McastUpdateRouteCpuPort (tNetIp6McRouteInfo * pIp6McastRoute,
                                UINT4 u4CpuPortStatus)
{
    UNUSED_PARAM (pIp6McastRoute);
    UNUSED_PARAM (u4CpuPortStatus);

    return NETIPV6_SUCCESS;
}
#endif /* __NETIP6MC_C__ */
/*-----------------------------------------------------------------------*/
/*                       End of the file  netip6mc.c                     */
/*-----------------------------------------------------------------------*/
