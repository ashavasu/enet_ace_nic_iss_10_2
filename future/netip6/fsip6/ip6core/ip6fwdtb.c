/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * 
 * $Id: ip6fwdtb.c,v 1.9
 * 
 *
 ********************************************************************/
/*
 *-----------------------------------------------------------------------------
 *    FILE NAME                    :    ip6fwdtb.c   
 *
 *    PRINCIPAL AUTHOR             :    Kaushik Biswas 
 *
 *    SUBSYSTEM NAME               :    IPv6
 *
 *    MODULE NAME                  :    IP6 RIP6 Module
 *
 *    LANGUAGE                     :    C
 *
 *    TARGET ENVIRONMENT           :    UNIX
 *
 *    DATE OF FIRST RELEASE        :    DDD-MM-1996 
 *
 *    DESCRIPTION                  :    This file contains the routines
 *                                      for handling Route Table processing 
 *                                      
 *-----------------------------------------------------------------------------
 * Revision History 
 * ---------------------------------------------------------------------------
 * Name/ Date           | Reason for Change   |    Change Description
 *----------------------------------------------------------------------------
 * SIVARKA/  10-01-2002 | IP6-RIP6 decoupling |  File name changed from rip6rt.
 *                      |                     |  The Function names starting
 *                      |                     |  with Rip6 are modified to
 *                      |                     |  start with Ip6.
 *----------------------------------------------------------------------------*/
#include "ip6inc.h"
/*
 * Globals
 */

 /* Flag to indicate  whether to prop  static rts/not */
UINT4               u4Ip6StaticPropogate = IP6_PROPOGATE_DYNAMIC_ONLY;

/* Flag telling how to lookup */
UINT1               u1Ip6LookupPreference = IP6_ROUTE_PREFERENCE_BEST_METRIC;

 /* ****************************************************************************
  * DESCRIPTION : Does the route lookup in the RIP6 routing table
  *
  * INPUTS      : The ptr to IPv6 address for which to lookup (pAddr)
  *
  * OUTPUTS     : None
  *
  * RETURNS     : The routing table entry if found, else NULL
  *
  * NOTES       : Returns the route based upon the preference of static
  *               dynamic or the best metric route.
  *
  ***************************************************************************/

INT4
Ip6RtLookupInCxt (UINT4 u4ContextId, tIp6Addr * pAddr,
                  tNetIpv6RtInfo * pNetIpv6RtInfo)
{
    UINT1               u1Found = 0;
    INT4                i4RetVal = 0;

    IP6_TASK_UNLOCK ();
    i4RetVal = Rtm6ApiRoute6LookupInCxt (u4ContextId, pAddr,
                                         IP6_ADDR_MAX_PREFIX,
                                         u1Ip6LookupPreference, &u1Found,
                                         pNetIpv6RtInfo);
    IP6_TASK_LOCK ();
    return i4RetVal;
}

/******************************************************************************
 * DESCRIPTION : Called whenever a address is configured on an interface
 *               to add a LOCAL route 
 *
 * INPUTS      : The logical interface (u4Index),the ptr to IPv6 address 
 *               (pAddr), prefix length (u1Prefixlen)
 *
 * OUTPUTS     : None
 *
 * RETURNS     : None
 *
 * NOTES       :
 ******************************************************************************/

VOID
Ip6LocalRouteAdd (UINT4 u4Index, tIp6Addr * pAddr, UINT1 u1Prefixlen)
{
    tNetIpv6RtInfo      NetIpv6RtInfo;
    tIp6Addr            nexthop;
    tIp6If             *pIf6 = NULL;
    UINT4               u4ContextId = 0;
    INT4                i4AddrType = 0;

    if ((pIf6 = Ip6ifGetEntry (u4Index)) == NULL)
    {
        return;
    }

    i4AddrType = Ip6GetUniAddrType (pIf6, pAddr);

    Ip6GetCxtId (u4Index, &u4ContextId);
    IP6_TRC_ARG3 (u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                  "IP6: Adding Local Route,If = %d Prefixlen = %d Address = %s\n",
                  u4Index, u1Prefixlen, Ip6PrintAddr (pAddr));
    MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));
    MEMSET (&nexthop, 0, sizeof (tIp6Addr));
    Ip6CopyAddrBits (&NetIpv6RtInfo.Ip6Dst, pAddr, u1Prefixlen);
    NetIpv6RtInfo.u1Prefixlen = u1Prefixlen;
    NetIpv6RtInfo.u4Index = u4Index;
    NetIpv6RtInfo.u4Metric = 1;    /* For local route metric is always 1 */
    NetIpv6RtInfo.i1Proto = IP6_LOCAL_PROTOID;
    NetIpv6RtInfo.i1Type = IP6_ROUTE_TYPE_DIRECT;
    NetIpv6RtInfo.u4RowStatus = IP6FWD_ACTIVE;
    NetIpv6RtInfo.u2ChgBit = IP6_BIT_ALL;
    NetIpv6RtInfo.u4ContextId = u4ContextId;
    NetIpv6RtInfo.u1AddrType = (UINT1) i4AddrType;

    if (Rtm6ApiIpv6LeakRoute (NETIPV6_ADD_ROUTE, &NetIpv6RtInfo) ==
        RTM6_FAILURE)
    {
        /* Adding Local Route Fails. */
        return;
    }

    return;
}

/******************************************************************************
 * DESCRIPTION :  Called to add a  static route. 
 *
 * INPUTS      : The ptr to Dest IPv6 addr (pDest), the prefix length
 *               (u1Prefixlen),ptr to Next Hop IPv6 address (pNexthop),
 *               metric (u1Metric),type of route (i1Type),protocol of
 *               the route (i1Proto),the logical interface (i2Index)
 *
 * OUTPUTS     : None
 *
 * RETURNS     : IP6_SUCCESS or IP6_FAILURE
 *
 * NOTES       :
 ******************************************************************************/
INT4
Ip6AddRouteInCxt (UINT4 u4ContextId, tIp6Addr * pDest, UINT1 u1Prefixlen,
                  tIp6Addr * pNexthop, UINT4 u4Metric, INT4 i4Preference,
                  INT1 i1Type, INT1 i1Proto, UINT4 u4Index, UINT1 u1AddrType,
                  UINT4 u4RtTag, UINT4 u4RowStatus)
{
    tNetIpv6RtInfo      NetIpv6RtInfo;
    tNd6CacheEntry     *pNd6c = NULL;
    tIp6If             *pTempRouteIndex = NULL;

    IP6_TRC_ARG6 (u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                  "IP6: Static Route Add: DesAdr=%s PrfLen=%d NxtHpAdr=%s "
                  "Metrc=%d Type=%d Prot=%d",
                  Ip6PrintAddr (pDest), u1Prefixlen,
                  Ip6PrintAddr (pNexthop), u4Metric, i1Type, i1Proto);

    IP6_TRC_ARG1 (u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                  "Indx=%d", u4Index);

    /* This routine is accessed within the RTM6 lock. Hence for accessing
     * IP6 data, IP6 lock is taken here */
    IP6_TASK_LOCK ();
    if (u4RowStatus == ACTIVE)
    {
        if (gIp6GblInfo.apIp6If[u4Index] == NULL)
        {
            IP6_TASK_UNLOCK ();
            return IP6_FAILURE;
        }
    }

    MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));
    Ip6CopyAddrBits (&NetIpv6RtInfo.Ip6Dst, pDest, u1Prefixlen);
    NetIpv6RtInfo.u1Prefixlen = u1Prefixlen;
    MEMCPY (&NetIpv6RtInfo.NextHop, pNexthop, sizeof (tIp6Addr));
    NetIpv6RtInfo.u4Index = u4Index;
    NetIpv6RtInfo.u4Metric = u4Metric;
    NetIpv6RtInfo.u1Preference = (UINT1) i4Preference;
    NetIpv6RtInfo.i1Proto = i1Proto;
    NetIpv6RtInfo.u4RouteTag = u4RtTag;
    NetIpv6RtInfo.i1Type = i1Type;
    NetIpv6RtInfo.u4ContextId = u4ContextId;
    NetIpv6RtInfo.u1AddrType = u1AddrType;

    if ((u1Prefixlen == 0) && (IS_ADDR_UNSPECIFIED (NetIpv6RtInfo.Ip6Dst)))
    {
        NetIpv6RtInfo.i1DefRtrFlag = IP6_DEF_RTR_FLAG;
    }
    else
    {
        NetIpv6RtInfo.i1DefRtrFlag = 0;
    }

    if (u4RowStatus == ACTIVE)
    {
        if (gIp6GblInfo.apIp6If[u4Index]->u1OperStatus == OPER_UP)
        {
            NetIpv6RtInfo.u4RowStatus = ACTIVE;
        }
        else
        {
            NetIpv6RtInfo.u4RowStatus = IP6FWD_NOT_IN_SERVICE;
        }
    }
    else
    {
        NetIpv6RtInfo.u4RowStatus = u4RowStatus;
    }

    NetIpv6RtInfo.u2ChgBit = IP6_BIT_ALL;

    IP6_TASK_UNLOCK ();

    if (Rtm6UtilIpv6LeakRoute (NETIPV6_ADD_ROUTE, &NetIpv6RtInfo) ==
        RTM6_FAILURE)
    {
        /* Adding Static Route Fails. */
        IP6_TRC_ARG6 (u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                      "IP6: Static Route Add - DesAdr=%s PrfLen=%d NxtHop=%s "
                      "Metrc=%d Type=%d Prot=%d - FAILED!!!",
                      Ip6PrintAddr (pDest), u1Prefixlen,
                      Ip6PrintAddr (pNexthop), u4Metric, i1Type, i1Proto);

        return IP6_FAILURE;
    }

    if ((NetIpv6RtInfo.i1DefRtrFlag == IP6_DEF_RTR_FLAG) &&
        (NetIpv6RtInfo.u4RowStatus == ACTIVE))
    {
        /* Adding Default Route Successful. */
        IP6_TRC_ARG (u4ContextId, IP6_MOD_TRC, MGMT_TRC, IP6_NAME,
                     "IP6: Default Route Entry Added Successfully\n");

        /* Resolve the MAC Address for the Next Hope (Neighbor) */
        /* only when the next hop is specified */
        if (!IS_ADDR_UNSPECIFIED (NetIpv6RtInfo.NextHop)
            && !IS_ADDR_LLOCAL (NetIpv6RtInfo.NextHop))
        {
            RTM6_TASK_UNLOCK ();
            IP6_TASK_LOCK ();
            pTempRouteIndex =
                Ip6GetRouteInCxt (u4ContextId, pNexthop, &pNd6c,
                                  &NetIpv6RtInfo);
            IP6_TASK_UNLOCK ();
            RTM6_TASK_LOCK ();
            if (pTempRouteIndex == NULL)
            {
                IP6_TRC_ARG (u4ContextId, IP6_MOD_TRC, ALL_FAILURE_TRC,
                             IP6_NAME,
                             "IP6: Next Hop not reachable. Hence neighbor discovery is not resloved!!!\n");
                return IP6_SUCCESS;
            }
        }

        if (!(IS_ADDR_UNSPECIFIED (NetIpv6RtInfo.NextHop)))
        {

            RTM6_TASK_UNLOCK ();
            IP6_TASK_LOCK ();
#ifdef TUNNEL_WANTED
            if (gIp6GblInfo.apIp6If[u4Index]->u1IfType !=
                IP6_TUNNEL_INTERFACE_TYPE)
            {
                Nd6Resolve (NULL, gIp6GblInfo.apIp6If[u4Index], pNexthop, NULL,
                            u1AddrType);
            }
#else
            Nd6Resolve (NULL, gIp6GblInfo.apIp6If[u4Index], pNexthop, NULL,
                        u1AddrType);
#endif
            IP6_TASK_UNLOCK ();
            RTM6_TASK_LOCK ();
        }
        return IP6_SUCCESS;
    }

    return IP6_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : Called when the route is deleted from the routing Table
 *
 * INPUTS      : Ptr to IPv6 address (pAddr)
 *
 * OUTPUTS     : None
 *
 * RETURNS     : None 
 *
 * NOTES       : IP6_SUCCESS / IP6_FAILURE
 ******************************************************************************/

INT4
Ip6DelRouteInCxt (UINT4 u4ContextId, tIp6Addr * pAddr, UINT1 u1Prefixlen,
                  INT1 i1Proto, tIp6Addr * pNextHop)
{
    tNetIpv6RtInfo      NetIpv6RtInfo;

    MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));
    Ip6CopyAddrBits (&NetIpv6RtInfo.Ip6Dst, pAddr, u1Prefixlen);
    NetIpv6RtInfo.u1Prefixlen = u1Prefixlen;
    MEMCPY (&NetIpv6RtInfo.NextHop, pNextHop, sizeof (tIp6Addr));
    NetIpv6RtInfo.i1Proto = i1Proto;
    NetIpv6RtInfo.u4RowStatus = IP6FWD_DESTROY;
    NetIpv6RtInfo.u2ChgBit = IP6_BIT_STATUS;
    NetIpv6RtInfo.u4ContextId = u4ContextId;

    if ((u1Prefixlen == 0) && (IS_ADDR_UNSPECIFIED (NetIpv6RtInfo.Ip6Dst)))
    {
        NetIpv6RtInfo.i1DefRtrFlag = IP6_DEF_RTR_FLAG;
    }
    else
    {
        NetIpv6RtInfo.i1DefRtrFlag = 0;
    }

    if (Rtm6UtilIpv6LeakRoute (NETIPV6_DELETE_ROUTE, &NetIpv6RtInfo) ==
        RTM6_FAILURE)
    {
        /* Deleting Static Route Fails. */
        IP6_TRC_ARG (u4ContextId, IP6_MOD_TRC, ALL_FAILURE_TRC, IP6_NAME,
                     "IP6: Deleting Static route FAILED!!!\n");
        return IP6_FAILURE;
    }

    return IP6_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : Called when the interface address is deleted.             
 *
 * INPUTS      : Ptr to IPv6 address (pAddr)
 *
 * OUTPUTS     : None
 *
 * RETURNS     : None 
 *
 * NOTES       :
 ******************************************************************************/

INT4
Ip6DelLocalRoute (tIp6Addr * pAddr, UINT1 u1Prefixlen, UINT4 u4IfIndex)
{
    tNetIpv6RtInfo      NetIpv6RtInfo;

    MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));
    Ip6CopyAddrBits (&NetIpv6RtInfo.Ip6Dst, pAddr, u1Prefixlen);
    NetIpv6RtInfo.u1Prefixlen = u1Prefixlen;
    NetIpv6RtInfo.u4Index = u4IfIndex;
    NetIpv6RtInfo.i1Proto = IP6_LOCAL_PROTOID;
    NetIpv6RtInfo.u4RowStatus = IP6FWD_DESTROY;
    NetIpv6RtInfo.u2ChgBit = IP6_BIT_STATUS;
    Ip6GetCxtId (u4IfIndex, &NetIpv6RtInfo.u4ContextId);

    if (Rtm6ApiIpv6LeakRoute (NETIPV6_DELETE_ROUTE, &NetIpv6RtInfo) ==
        RTM6_FAILURE)
    {
        /* Deleting Static Route Fails. */
        IP6_TRC_ARG (NetIpv6RtInfo.u4ContextId, IP6_MOD_TRC,
                     ALL_FAILURE_TRC, IP6_NAME,
                     "IP6: Deleting Local route FAILED!!!\n");
        return IP6_FAILURE;
    }

    return IP6_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : Purges out a route from the routing table
 *
 * INPUTS      : The routing table entry ptr to be deleted (pRt)
 *
 * OUTPUTS     : None
 *
 * RETURNS     : IP6_SUCCESS - if successful in purging out the route
 *               IP6_FAILURE - otherwise
 *
 * NOTES       : Tries to optimize the delete by recursively deleting the 
 *               parent of the deleted node 
 ******************************************************************************/

INT4
Ip6RoutePurge (tIp6RtEntry * pRt)
{
    tNetIpv6RtInfo      NetIpv6RtInfo;
    MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));
    MEMCPY (&NetIpv6RtInfo.Ip6Dst, &pRt->dst, sizeof (tIp6Addr));
    NetIpv6RtInfo.u1Prefixlen = pRt->u1Prefixlen;
    MEMCPY (&NetIpv6RtInfo.NextHop, &pRt->nexthop, sizeof (tIp6Addr));
    NetIpv6RtInfo.u4Index = pRt->u4Index;
    NetIpv6RtInfo.i1Proto = pRt->i1Proto;
    NetIpv6RtInfo.i1DefRtrFlag = pRt->i1DefRtrFlag;
    NetIpv6RtInfo.u4RowStatus = IP6FWD_DESTROY;
    NetIpv6RtInfo.u2ChgBit = IP6_BIT_STATUS;
    Ip6GetCxtId (pRt->u4Index, &NetIpv6RtInfo.u4ContextId);

    if (Rtm6ApiIpv6LeakRoute (NETIPV6_DELETE_ROUTE, &NetIpv6RtInfo) ==
        RTM6_FAILURE)
    {
        return IP6_FAILURE;
    }

    return IP6_SUCCESS;
}

/**************************** END OF FILE *************************************/
