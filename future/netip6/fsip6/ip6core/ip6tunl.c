/*******************************************************************
 *   Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *   
 *    $Id: ip6tunl.c,v 1.3 2013/01/07 12:23:21 siva Exp $
 *
 * ****************************************************************/

#include "ip6inc.h"
#ifdef TUNNEL_WANTED
/* #include "ip6tunl.h" */
#ifndef IP_WANTED

tOsixTaskId         gi4Ip6TunlTaskId;
/* Global socket used for tunneling */
INT4                gi4Sockid = INVALID_SOCKID;

/******************************************************************************
DESCRIPTION : IPv4 Protocol Initialisation
 
INPUTS      : None

OUTPUTS     : None

RETURNS     : IP6_SUCCESS  -  Socket/Task Creation succeeds
              IP6_FAILURE  -  Socket/Task Creation fails
******************************************************************************/
INT1
RegisterWithSocketForV4 ()
{
    gi4Sockid = TUNL_SOCKET (AF_INET, SOCK_RAW, IPPROTO_IPV6);
    if (gi4Sockid <= INVALID_SOCKID)
    {
        perror ("Tunl Socket creation ... ");
        IP6_TRC_ARG (IP6_MOD_TRC, ALL_FAILURE_TRC, IP6_NAME,
                     "IP6TUN:RegisterWithSocketForV4:IP6 TUNNEL Socket "
                     "creation Failed!!! \n");
        return IP6_FAILURE;
    }
    if (TUNL_SOCK_FCNTL (gi4Sockid, F_SETFL, O_NONBLOCK) <= -1)
    {
        IP6_TRC_ARG (IP6_MOD_TRC, ALL_FAILURE_TRC, IP6_NAME,
                     "IP6TUN:RegisterWithSocketForV4:IP6 TUNNEL failure "
                     "in setting non-blocking option!!!\n");
        return IP6_FAILURE;
    }

    /* Ipv6 has been registered. So create task which handles packets 
       that are received for this tunnel interface. */
    if (OsixCreateTask (IP6_TUNL_TASK_NAME,
                        IP6_TUNL_TASK_PRIORITY,
                        OSIX_DEFAULT_STACK_SIZE,
                        (VOID *) IptaskIp6Input, NULL, 0,
                        &gi4Ip6TunlTaskId) != OSIX_SUCCESS)
    {
        IP6_TRC_ARG (IP6_MOD_TRC, ALL_FAILURE_TRC, IP6_NAME,
                     "IP6TUN:RegisterWithSocketForV4:IIP6 TUNNEL TASK "
                     "Creation Failed \n");
        return (IP6_FAILURE);
    }
    return (IP6_SUCCESS);
}

/******************************************************************************
DESCRIPTION : Entry point function which handles packets that are received
              from the socket. It process the packets received and enqueues 
              as a CRU buffer and sends the corresponding event to IP6.
INPUTS      : None

OUTPUTS     : None

RETURNS     : None. 

NOTES       : Similar function appears in ip6main.c file which
              would be called when IP_WANTED is defined.   
******************************************************************************/
VOID
IptaskIp6Input (VOID)
{
    struct sockaddr_in  DstAddr;
    UINT1               au1Ipv6Pkt[IP6_MIN_MTU];
    INT2                i2Retval = 0;
    tCRU_BUF_CHAIN_HEADER *pBuf;
    INT4                DstAddrLen = sizeof (struct sockaddr);
    tLlToIp6Params     *pParams = NULL;

    DstAddr.sin_family = AF_INET;
    DstAddr.sin_port = (0);

    IP6_FOREVER ()
    {
        i2Retval = 0;
        while ((i2Retval = TUNL_SOCK_RECV (gi4Sockid, (char *) au1Ipv6Pkt,
                                           IP6_MIN_MTU, 0,
                                           (struct sockaddr *) &DstAddr,
                                           &DstAddrLen)) >= 0)
        {

            pBuf = Ip6BufAlloc (i2Retval, 0, IP6_MODULE);
            if (pBuf == NULL)
                break;

            if (CRU_BUF_Copy_OverBufChain
                (pBuf, au1Ipv6Pkt, 0, i2Retval) == CRU_FAILURE)
            {
                IP6_TRC_ARG (IP6_MOD_TRC, BUFFER_TRC | ALL_FAILURE_TRC,
                             IP6_NAME,
                             "IP6:IptaskIp6Input:Buf for tunel pkt couldn't "
                             "be allo CRU_BUF_CopyOverBufChain\n");
                IP6_TRC_ARG3 (IP6_MOD_TRC, BUFFER_TRC, IP6_NAME,
                              "IP6:IptaskIp6Input:%s buf err: Type = %d "
                              "Bufptr = %p",
                              ERROR_FATAL_STR, ERR_BUF_ALLOC, NULL);
                Ip6BufRelease (pBuf, FALSE, IP6_MAIN_SUBMODULE);
                break;
            }
            CRU_BUF_Move_ValidOffset (pBuf, 20);

            /* IP6 uses the type of interface to find out the if entry.
             * This can be achieved by using InterfaceId also
             */
            CRU_BUF_Set_Interface_Num (CRU_BUF_Get_InterfaceId (pBuf), 0);
            CRU_BUF_Set_Interface_SubRef (CRU_BUF_Get_InterfaceId (pBuf), 0);

            /*
               locates interface using src addr of ipv4 pkt ,
               during adding to the tunnel hashtable, dest addr
               of tunnel entry is used
             */

            CRU_BUF_Set_U4Reserved3 (pBuf, NTOHL (DstAddr.sin_addr.s_addr));

            pParams = (tLlToIp6Params *) IP6_GET_MODULE_DATA_PTR (pBuf);

            /* The same effect of using IP6_SET_COMMAND */
            IP6_SET_COMMAND (pBuf, IP6_LAYER2_DATA);
            pParams->u1LinkType = IP6_TUNNEL_INTERFACE_TYPE;
            tunlStats.u4TunlInPkts++;

            /* Enquing to IP6 from IPv4 */
            if (OsixSendToQ (SELF,
                             IP6_TASK_IP_INPUT_QUEUE,
                             pBuf, OSIX_MSG_URGENT) != OSIX_SUCCESS)
            {
                IP6_TRC_ARG (IP6_MOD_TRC, BUFFER_TRC | ALL_FAILURE_TRC,
                             IP6_NAME,
                             "IP6:IptaskIp6Input: Send to Ip6 QUE is failed \n");
                IP6_TRC_ARG3 (IP6_MOD_TRC, BUFFER_TRC, IP6_NAME,
                              "IP6:IptaskIp6Input:%s buf err: Type = %d Bufptr = %p",
                              ERROR_FATAL_STR, ERR_BUF_ALLOC, NULL);
                Ip6BufRelease (pBuf, FALSE, IP6_MAIN_SUBMODULE);
                tunlStats.u4TunlInErrPkts++;
                break;
            }

            if (OsixSendEvent (SELF, IP6_TASK_NAME,
                               IP6_DGRAM_RECD_EVENT) != OSIX_SUCCESS)
            {
                IP6_TRC_ARG (IP6_MOD_TRC, BUFFER_TRC | ALL_FAILURE_TRC,
                             IP6_NAME,
                             "IP6:IptaskIp6Input: Send EVENT to IP6QUE is failed \n");
                IP6_TRC_ARG3 (IP6_MOD_TRC, BUFFER_TRC, IP6_NAME,
                              "IP6:IptaskIp6Input:%s buf err: Type = %d Bufptr = %p",
                              ERROR_FATAL_STR, ERR_BUF_ALLOC, NULL);
                tunlStats.u4TunlInErrPkts++;
                break;
            }
        }                        /* end of while */
        OsixDelayTask (1);

    }                            /* end of FOREVER */

}

/******************************************************************************
DESCRIPTION : Function which sends a tunnel packet to the socket. 
 
INPUTS      : u4DstIp - Destination Address of the packet.
              pBuf    - CRU Buffer to be sent to the destination.

OUTPUTS     : None

RETURNS     : IP6_SUCCESS/ IP6_FAILURE
******************************************************************************/
INT4
Ip6SendToV4Socket (UINT4 u4DstIp, tCRU_BUF_CHAIN_HEADER * pBuf)
{
    UINT1              *pIpv6Pkt = NULL;
    INT2                i2Ipv6PktLen;
    struct sockaddr_in  DstAddr;
    if (gi4Sockid == INVALID_SOCKID)
        return IP6_FAILURE;

    /*Change the CRU buffer to ipv6 packet and send */
/* TODO : change the below to macro */
    pIpv6Pkt = pBuf->pFirstValidDataDesc->pu1_FirstValidByte;
    i2Ipv6PktLen = pBuf->pFirstValidDataDesc->u4_ValidByteCount;

    /* Set the destination address */
    memset ((char *) (&DstAddr), 0, sizeof (DstAddr));
    DstAddr.sin_family = AF_INET;
    DstAddr.sin_addr.s_addr = HTONL (u4DstIp);
    DstAddr.sin_port = (0);

    if (TUNL_SOCK_SEND (gi4Sockid, (VOID *) pIpv6Pkt, i2Ipv6PktLen,
                        0, (struct sockaddr *) &DstAddr,
                        sizeof (struct sockaddr)) <= -1)
    {
        /* add trace */
        /* Release the pBuf */
        Ip6BufRelease (pBuf, FALSE, IP6_MAIN_SUBMODULE);
        return IP6_FAILURE;
    }

    /* Release the pBuf */
    Ip6BufRelease (pBuf, FALSE, IP6_MAIN_SUBMODULE);
    return IP6_SUCCESS;

}

/******************************************************************************
DESCRIPTION : Function to get ipv6 address of particular interface.  
 
INPUTS      : u2IfIndex - interface for which the ip address needs 
                          to be returned.

OUTPUTS     : None

RETURNS     : IP ADDRESS/ IP6_FAILURE
******************************************************************************/
UINT4
IpifGetIfAddr (UINT2 u2IfIndex)
{
    struct ifreq        ifrs[2];
    struct ifconf       ifcnf;
    struct sockaddr_in *sockaddr;

    MEMSET (&ifcnf, 0x0, sizeof (ifcnf));
    MEMSET (&ifrs, 0x0, sizeof (ifrs));
    ifcnf.ifc_len = sizeof (ifrs);
    ifcnf.ifc_req = ifrs;

    if (ioctl (gi4Sockid, SIOCGIFCONF, &ifcnf) < 0)
    {
        return IP6_FAILURE;
    }

    sockaddr = (struct sockaddr_in *) &(ifrs[u2IfIndex].ifr_addr);
    return (OSIX_NTOHL ((unsigned int) sockaddr->sin_addr.s_addr));
}

#endif

/******************************************************************************
 * DESCRIPTION : This function is called to configure the compatible address
 *               for automatic tunneling interface. It takes the ipv4 address
 *               from the interface over which the automatic tunnel is enabled.
 *
 * INPUTS      : pIf6 - Automatic tunnel interface
 *
 * OUTPUTS     : None
 *
 * RETURNS     : IP6_SUCCESS/IP6_FAILURE
 *               
 *
 * NOTES       : 
 ******************************************************************************/
INT4
Ip6ifCreateV4compatv6Addr (tIp6If * pIf6)
{
    tIp6AddrInfo       *pAddrInfo = NULL;

    if (Ip6AddrCreate (pIf6->u4Index, &pIf6->pTunlIf->tunlSrc, 96, ADMIN_UP,
                       ADDR6_V4_COMPAT, 1, IP6_ADDR_STATIC) == IP6_SUCCESS)
    {
        if ((pAddrInfo = Ip6AddrTblGetEntry
             (pIf6->u4Index, &pIf6->pTunlIf->tunlSrc, 96)) != NULL)
        {
            /* No need DAD for Automatic Tunnel. */
            pAddrInfo->u1Status &= ~(UINT1) ADDR6_TENTATIVE;
            pAddrInfo->u1Status &= ~ADDR6_FAILED;
            pAddrInfo->u1Status |= ADDR6_COMPLETE (pIf6);
            pAddrInfo->u1Status |= ADDR6_PREFERRED;

            /* Create the Auto Compatible Route. */
            Ip6LocalRouteAdd (pIf6->u4Index, &pIf6->pTunlIf->tunlSrc, 96);
            return IP6_SUCCESS;
        }
    }

    return IP6_FAILURE;
}

/******************************************************************************
 * DESCRIPTION : This function is called to Delete the compatible address
 *               from automatic tunneling interface.
 *
 * INPUTS      : pIf6 - Automatic tunnel interface
 *
 * OUTPUTS     : None
 *
 * RETURNS     : IP6_SUCCESS/IP6_FAILURE
 *               
 *
 * NOTES       : 
 ******************************************************************************/
INT4
Ip6ifDeleteV4compatv6Addr (tIp6If * pIf6)
{
    /* Delete the Auto Compatible Route. */
    Ip6DelLocalRoute (&pIf6->pTunlIf->tunlSrc, 96, pIf6->u4Index);

    /* Deleting the V4compatible v6 address */
    if (Ip6AddrDelete (pIf6->u4Index, &pIf6->pTunlIf->tunlSrc, 96, 1) ==
        IP6_SUCCESS)
    {
        return IP6_SUCCESS;
    }

    return IP6_FAILURE;

}

#endif
