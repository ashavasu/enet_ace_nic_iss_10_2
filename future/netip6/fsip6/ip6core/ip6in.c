/*******************************************************************
 *   Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *   
 *     $Id: ip6in.c,v 1.30 2017/11/14 07:31:13 siva Exp $
 * *************************************************************/
/*
 *-----------------------------------------------------------------------------
 *    FILE NAME                    :    ip6in.c   
 *
 *    PRINCIPAL AUTHOR             :    Kaushik Biswas 
 *
 *    SUBSYSTEM NAME               :    IPv6
 *
 *    MODULE NAME                  :    IP6 Core Submodule 
 *
 *    LANGUAGE                     :    C
 *
 *    TARGET ENVIRONMENT           :    UNIX
 *
 *    DATE OF FIRST RELEASE        :    DDD-MM-1996 
 *
 *    DESCRIPTION                  :    This file handles the processing
 *                                      of the IPv6 packet on the input side
 *                                      
 *
 *-----------------------------------------------------------------------------
 */

#include "ip6inc.h"
#include "secv6.h"

/******************************************************************************
 * DESCRIPTION : This routine receives incoming IPv6 packets and does basic
 *               validation. Then based on whether the packet is addressed to
 *               us or not, it is handed to the receive routine or the forward
 *               routine.
 *
 * INPUTS      : The IPv6 interface pointer(pIf6) and the buffer containing 
 *               the datagram(pBuf).
 *
 * OUTPUTS     : None
 *
 * RETURNS     : None
 *
 * NOTES       :
 ******************************************************************************/
VOID
Ip6Input (tIp6If * pIf6, tCRU_BUF_CHAIN_HEADER * pBuf, tIp6Addr * pIp6Addr)
{
#ifdef NPAPI_WANTED
    tIp6Addr            Ip6Addr;
#endif
    UINT1               u1Type = 0;
    tIp6Hdr            *pIp6 = NULL, ip6Hdr;
    tIp6Cxt            *pIp6Cxt = NULL;
    UINT4               u4PayLoadLength = 0;
    UINT4               u4ExtraBytes = 0;
    UINT4               u4TotalBytes = 0;
    UINT4               u4Octs = 0;
    UINT1               u1DstAddrScope = 0;    /* Added for RFC4007  */
    INT4                i4IsPktToMe = IP6_FAILURE;
    INT4                i4IsProxyReq = IP6_FALSE;
    UINT4               u4Index = 0;
#ifdef EVPN_VXLAN_WANTED
    tNd6CacheEntry      Nd6cEntry;
    tIp6Addr            Ip6TgtAddr;

    MEMSET (&Nd6cEntry, 0, sizeof (tNd6CacheEntry));
    MEMSET (&Ip6TgtAddr, 0, sizeof (tIp6Addr));
#endif

    pIp6Cxt = pIf6->pIp6Cxt;

    IP6IF_INC_IN_RECEVES (pIf6);
    IP6SYS_INC_IN_RECEVES (pIp6Cxt->Ip6SysStats);

    u4Octs = IP6_BUF_DATA_LEN (pBuf);

    IP6IF_INC_IN_OCTETS (pIf6, u4Octs);
    IP6SYS_INC_IN_OCTETS (pIp6Cxt->Ip6SysStats, u4Octs);

    if (IP6_BUF_DATA_LEN (pBuf) < IP6_HDR_LEN)
    {
        IP6IF_INC_IN_TRUNCATED (pIf6);
        IP6SYS_INC_IN_TRUNCATED (pIp6Cxt->Ip6SysStats);

        IP6_TRC_ARG3 (pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                      DATA_PATH_TRC, IP6_NAME,
                      "IP6IN: Ip6Input:Got truncated IPv6 packet Len = %d "
                      "Bufptr = %p IF = %d\n",
                      IP6_BUF_DATA_LEN (pBuf), pBuf, pIf6->u4Index);

        if (Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE, IP6_MODULE) ==
            IP6_FAILURE)
        {
            IP6_TRC_ARG3 (pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                          BUFFER_TRC, IP6_NAME,
                          "IP6IN: Ip6Input: %s buf err:, Type = %d "
                          "Module = 0x%X Bufptr = %p",
                          ERROR_FATAL_STR, ERR_BUF_RELEASE, pBuf);
        }
        /* Unable to clear the Hash entry in h/w because IP is not 
         * yet extracted 
         */
        return;
    }

    /*
     * Get IPv6 header from the packet and validate the packet
     */

    pIp6 = (tIp6Hdr *) Ip6BufRead (pBuf, (UINT1 *) &ip6Hdr, 0,
                                   (UINT4) (sizeof (tIp6Hdr)), 1);

    if (pIp6 == NULL)
    {
        Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE, IP6_MODULE);
        /* Unable to clear the Hash entry in h/w because IP is not 
         * yet extracted 
         */
        return;
    }
#ifdef NPAPI_WANTED
    MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));
    MEMCPY (&Ip6Addr, &pIp6->dstAddr, sizeof (tIp6Addr));
#endif

    if (pIp6Addr != NULL)
    {
        MEMSET (pIp6Addr, 0, sizeof (tIp6Addr));
        u1Type = (UINT1) Ip6AddrType (&(pIp6->dstAddr));

        if ((!(Ip6IsOurAddrInCxt (pIp6Cxt->u4ContextId,
                                  &pIp6->dstAddr, &u4Index)))
            && (u1Type == ADDR6_UNICAST))
        {

            MEMCPY (pIp6Addr, &pIp6->dstAddr, sizeof (tIp6Addr));
        }
    }
    IP6_TRC_ARG6 (pIp6Cxt->u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                  "IP6IN:Ip6Input:Received on IF= %d PLen= %d NH= 0x%x "
                  "Bufptr= %p Src= %s Dst=%s\n",
                  pIf6->u4Index, NTOHS (pIp6->u2Len), pIp6->u1Nh, pBuf,
                  Ip6PrintAddr (&pIp6->srcAddr), Ip6PrintAddr (&pIp6->dstAddr));

    IP6_TRC_ARG (pIp6Cxt->u4ContextId, IP6_MOD_TRC, BUFFER_TRC, IP6_NAME,
                 "IP6IN: Ip6Input: The received buffer is :\n");
    IP6_PKT_DUMP (pIp6Cxt->u4ContextId, IP6_MOD_TRC, DUMP_TRC,
                  IP6_NAME, pBuf, 0);

    if ((NTOHS (pIp6->u2Len) + IP6_HDR_LEN) > IP6_BUF_DATA_LEN (pBuf))
    {
        IP6IF_INC_IN_TRUNCATED (pIf6);
        IP6SYS_INC_IN_TRUNCATED (pIp6Cxt->Ip6SysStats);

        IP6_TRC_ARG4 (pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                      DATA_PATH_TRC | BUFFER_TRC, IP6_NAME,
                      "IP6IN:Ip6Input:Insufficient data in IPv6 pkt, "
                      "Len = %d PLen=%d Bufptr=%p IF=%d\n",
                      IP6_BUF_DATA_LEN (pBuf), NTOHS (pIp6->u2Len), pBuf,
                      pIf6->u4Index);

        if (Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE, IP6_MODULE) ==
            IP6_FAILURE)
        {
            IP6_TRC_ARG3 (pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                          BUFFER_TRC, IP6_NAME,
                          "IP6IN: Ip6Input: %s buf err:, Type = %d "
                          "Module = 0x%X Bufptr = %p ",
                          ERROR_FATAL_STR, ERR_BUF_RELEASE, pBuf);
        }

#if defined (NPAPI_WANTED) && defined (CFA_WANTED)
        /* Remove the Entry created in the DLF Hash Table */
        Ipv6FsCfaHwRemoveIp6NetRcvdDlfInHash (pIp6Cxt->u4ContextId, Ip6Addr,
                                              IP6_MAX_PREFIX_LEN);
#endif
        return;
    }

    u4PayLoadLength = OSIX_NTOHS (pIp6->u2Len);
    u4TotalBytes = CRU_BUF_Get_ChainValidByteCount (pBuf);
    u4ExtraBytes = u4TotalBytes - (u4PayLoadLength + IPV6_HEADER_LEN);
    if (u4ExtraBytes != 0)
    {
        CRU_BUF_Delete_BufChainAtEnd (pBuf, u4ExtraBytes);
    }

    if (((pIp6->u4Head) & IP6_VER_BITMASK)
        != HTONL (IP6_VER << IP6_CLASS_AND_FLOW_LBL_LEN))
    {
        IP6IF_INC_IN_HDR_ERR (pIf6);
        IP6SYS_INC_IN_HDR_ERR (pIp6Cxt->Ip6SysStats);

        IP6_TRC_ARG3 (pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                      BUFFER_TRC, IP6_NAME,
                      "IP6IN: Ip6Input:Incorrect version = 0x%08X "
                      "Bufptr = %p IF = %d\n",
                      NTOHL (pIp6->u4Head), pBuf, pIf6->u4Index);

        if (Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE, IP6_MODULE) ==
            IP6_FAILURE)
        {
            IP6_TRC_ARG3 (pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                          BUFFER_TRC, IP6_NAME,
                          "IP6IN: Ip6Input: %s buf err:, Type = %d "
                          "Module = 0x%X Bufptr = %p ",
                          ERROR_FATAL_STR, ERR_BUF_RELEASE, pBuf);
        }

#if defined (NPAPI_WANTED) && defined (CFA_WANTED)
        /* Remove the Entry created in the DLF Hash Table */
        Ipv6FsCfaHwRemoveIp6NetRcvdDlfInHash (pIp6Cxt->u4ContextId, Ip6Addr,
                                              IP6_MAX_PREFIX_LEN);
#endif
        return;
    }

    /*
     * Validate the source and destination addresses
     */

    if (IS_ADDR_MULTI (pIp6->srcAddr))
    {
        IP6IF_INC_IN_HDR_ERR (pIf6);
        IP6SYS_INC_IN_HDR_ERR (pIp6Cxt->Ip6SysStats);

        IP6_TRC_ARG3 (pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                      DATA_PATH_TRC, IP6_NAME,
                      "IP6IN: Ip6Input: Source address = %s is Multicast "
                      "Bufptr = %p IF = %d\n",
                      Ip6ErrPrintAddr (ERROR_MINOR, &pIp6->srcAddr), pBuf,
                      pIf6->u4Index);

        if (Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE, IP6_MODULE) ==
            IP6_FAILURE)
        {
            IP6_TRC_ARG3 (pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                          BUFFER_TRC, IP6_NAME,
                          "IP6IN: Ip6Input:%s buf err:, Type = %d "
                          "Module = 0x%X Bufptr = %p ",
                          ERROR_FATAL_STR, ERR_BUF_RELEASE, pBuf);
        }

#if defined (NPAPI_WANTED) && defined (CFA_WANTED)
        /* Remove the Entry created in the DLF Hash Table */
        Ipv6FsCfaHwRemoveIp6NetRcvdDlfInHash (pIp6Cxt->u4ContextId, Ip6Addr,
                                              IP6_MAX_PREFIX_LEN);
#endif
        return;
    }

#ifdef TUNNEL_WANTED
    if (pIf6->u1IfType == IP6_TUNNEL_INTERFACE_TYPE)
    {
        /* If it is a Ipv4 compatible v6 address v4 part of 
         * the address should not be broadcast, multicat, unspecified, 
         * and loopback address.
         */
        if (IS_ADDR_V4_COMPAT (pIp6->srcAddr))
        {
            if (Ip6ValidateIpv4Addr ((pIp6->srcAddr).u4_addr[IP6_THREE],
                                     pIf6->u4Index) == IP6_FAILURE)
            {
                IP6IF_INC_IN_HDR_ERR (pIf6);
                IP6SYS_INC_IN_HDR_ERR (pIp6Cxt->Ip6SysStats);

                IP6_TRC_ARG3 (pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                              DATA_PATH_TRC, IP6_NAME,
                              "IP6IN: Ip6Input: Source address = %s is Invalid "
                              "Bufptr = %p IF = %d\n",
                              Ip6ErrPrintAddr (ERROR_MINOR, &pIp6->srcAddr),
                              pBuf, pIf6->u4Index);

                if (Ip6BufRelease
                    (pIp6Cxt->u4ContextId, pBuf, FALSE,
                     IP6_MODULE) == IP6_FAILURE)
                {
                    IP6_TRC_ARG3 (pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                                  BUFFER_TRC, IP6_NAME,
                                  "IP6IN: Ip6Input: %s buf err:, Type = %d "
                                  "Module = 0x%X Bufptr = %p ",
                                  ERROR_FATAL_STR, ERR_BUF_RELEASE, pBuf);
                }

#if defined (NPAPI_WANTED) && defined (CFA_WANTED)
                /* Remove the Entry created in the DLF Hash Table */
                Ipv6FsCfaHwRemoveIp6NetRcvdDlfInHash (pIp6Cxt->u4ContextId,
                                                      Ip6Addr,
                                                      IP6_MAX_PREFIX_LEN);
#endif
                return;
            }
        }
    }
#endif
    /* IPv6_Thaikame - Fix
       Test case Id :Dst_Unreach_2.seq
       RFC 4443 description:
       Router should send valid ICMPv6 Destination
       Unreachable (code 2) in response to a packet when the scope of
       the source address is smaller than the scope of the destination
       address and the packet cannot be delivered to the destination
       without leaving the scope of the source address. */

    u1Type = (UINT1) Ip6AddrType (&(pIp6->dstAddr));
    i4IsPktToMe = Ip6IsPktToMeInCxt (pIp6Cxt, &u1Type, ADDR6_LLOCAL,
                                     &(pIp6->dstAddr), pIf6);

    /* Skip Scope-Zone validation for IPv6 PIM SSM address */
    if (!(IS_ALL_PREFIX_SSM_MULTI (&(pIp6->dstAddr))))
    {
        /* Get the Scope of the destination address and check if the scopezone
           is configured on this interface if not reject the packet and release
           the buffer -RFC 4007 */

        u1DstAddrScope = Ip6GetAddrScope (&pIp6->dstAddr);

        if (ADDR6_SCOPE_INVALID == u1DstAddrScope)
        {
            IP6_TRC_ARG3 (pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                          DATA_PATH_TRC, IP6_NAME,
                          "IP6IN: Ip6Input: Invalid Scope for destincation address"
                          "src address = %s Bufptr = %p IF = %d\n",
                          Ip6ErrPrintAddr (ERROR_MINOR, &pIp6->dstAddr),
                          pBuf, pIf6->u4Index);

            if (Ip6BufRelease
                (pIp6Cxt->u4ContextId, pBuf, FALSE, IP6_MODULE) == IP6_FAILURE)
            {
                IP6_TRC_ARG3 (pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                              BUFFER_TRC, IP6_NAME,
                              "IP6IN: Ip6Input: %s buf err:, Type = %d "
                              "Module = 0x%X Bufptr = %p ",
                              ERROR_FATAL_STR, ERR_BUF_RELEASE, pBuf);
            }

#if defined (NPAPI_WANTED) && defined (CFA_WANTED)
            /* Remove the Entry created in the DLF Hash Table */
            Ipv6FsCfaHwRemoveIp6NetRcvdDlfInHash (pIp6Cxt->u4ContextId, Ip6Addr,
                                                  IP6_MAX_PREFIX_LEN);
#endif
            return;
        }

        /* Check if the interface has the src and the destination
           scope configured on it, if not return failure - RFC4007 */
        IP6_TASK_UNLOCK ();
        if (IP6_ZONE_INVALID == NetIpv6GetIfScopeZoneIndex
            (u1DstAddrScope, pIf6->u4Index))
        {
            IP6_TRC_ARG3 (pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                          DATA_PATH_TRC, IP6_NAME,
                          "IP6IN: Ip6Input: Out of Scope Packet Received"
                          "Dest address = %s Bufptr = %p IF = %d\n",
                          Ip6ErrPrintAddr (ERROR_MINOR, &pIp6->dstAddr),
                          pBuf, pIf6->u4Index);

            if (IP6_SUCCESS != i4IsPktToMe)
            {
                if (Ip6BufRelease
                    (pIp6Cxt->u4ContextId, pBuf, FALSE,
                     IP6_MODULE) == IP6_FAILURE)
                {
                    IP6_TRC_ARG3 (pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                                  BUFFER_TRC, IP6_NAME,
                                  "IP6IN: Ip6Input: %s buf err:, Type = %d "
                                  "Module = 0x%X Bufptr = %p ",
                                  ERROR_FATAL_STR, ERR_BUF_RELEASE, pBuf);
                }
                IP6_TASK_LOCK ();
#if defined (NPAPI_WANTED) && defined (CFA_WANTED)
                /* Remove the Entry created in the DLF Hash Table */
                Ipv6FsCfaHwRemoveIp6NetRcvdDlfInHash (pIp6Cxt->u4ContextId,
                                                      Ip6Addr,
                                                      IP6_MAX_PREFIX_LEN);
#endif
                return;
            }
        }
        IP6_TASK_LOCK ();
        /* RFC4007- Scoped Address Code changes endss */
    }
    else
    {
        IP6_TRC_ARG3 (pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                      DATA_PATH_TRC, IP6_NAME,
                      "IP6IN: Ip6Input: Skipping Scope-Zone validation for "
                      "IPv6 PIM SSM Dest address = %s Bufptr = %p IF = %d\n",
                      Ip6ErrPrintAddr (ERROR_MINOR, &pIp6->dstAddr),
                      pBuf, pIf6->u4Index);
    }

    if (u1Type == ADDR6_MULTI)
    {
        IP6IF_INC_IN_MCAST_PKT (pIf6);
        IP6SYS_INC_IN_MCAST_PKT (pIp6Cxt->Ip6SysStats);
        IP6IF_INC_IN_MCAST_OCTETS (pIf6, u4Octs);
        IP6SYS_INC_IN_MCAST_OCTETS (pIp6Cxt->Ip6SysStats, u4Octs);
    }
    else if (u1Type == ADDR6_UNSPECIFIED)
    {
        IP6IF_INC_IN_ADDR_ERR (pIf6);
        IP6SYS_INC_IN_ADDR_ERR (pIp6Cxt->Ip6SysStats);

        IP6_TRC_ARG3 (pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                      DATA_PATH_TRC | BUFFER_TRC, IP6_NAME,
                      "IP6IN: Ip6Input: Dest address = %s is Unspecified "
                      "Bufptr = %p IF = %d\n",
                      Ip6ErrPrintAddr (ERROR_MINOR, &pIp6->dstAddr), pBuf,
                      pIf6->u4Index);

        if (Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE, IP6_MODULE) ==
            IP6_FAILURE)
        {
            IP6_TRC_ARG3 (pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                          BUFFER_TRC, IP6_NAME,
                          "IP6IN: Ip6Input:%s buf err:, Type = %d "
                          "Module = 0x%X Bufptr = %p ",
                          ERROR_FATAL_STR, ERR_BUF_RELEASE, pBuf);
        }

#if defined (NPAPI_WANTED) && defined (CFA_WANTED)
        /* Remove the Entry created in the DLF Hash Table */
        Ipv6FsCfaHwRemoveIp6NetRcvdDlfInHash (pIp6Cxt->u4ContextId, Ip6Addr,
                                              IP6_MAX_PREFIX_LEN);
#endif
        return;
    }

    /* 
     * Since we are not doing resolution of the link-level address
     * (ARP Functionality) for FR or X25, whenever we get a packet on them
     * and the sender is our neighbour, then we extract the link-level address 
     * from this sender's packet and store it in our ND cache 
     */

    if ((IP6_IF_TYPE (pIf6) == IP6_X25_INTERFACE_TYPE) ||
        (IP6_IF_TYPE (pIf6) == IP6_FR_INTERFACE_TYPE))
    {
        if (Ip6AddrIsNeighbor (pIf6, &pIp6->srcAddr) != NULL)
        {
            Nd6UpdateCache (pIf6, &(pIp6->srcAddr), NULL, 0, 0, NULL, 0);
        }
    }

#ifdef EVPN_VXLAN_WANTED
    Nd6GetNSTargetAddr (pIf6, pIp6, pBuf, &Ip6TgtAddr);
#endif
    /* 
     * Check if the packet is addressed to the router or not and act
     * accordingly
     */
    i4IsProxyReq = Nd6IsNDProxyRequired (pIf6, pIp6, pBuf);

    if ((IP6_SUCCESS == i4IsPktToMe) || (IP6_TRUE == i4IsProxyReq) ||
        (Ip6IsPktToProxyInCxt (pIp6Cxt, &u1Type, &(pIp6->dstAddr), pBuf)
         == IP6_SUCCESS)
#ifdef HA_WANTED
        || (Ip6AddrIsAnyCast (pIf6, &(pIp6->dstAddr)) == IP6_SUCCESS)
#endif
#ifdef MN_WANTED
        || (Ip6IsPktFromHAToMN (&u1Type, pIf6, pIp6) == IP6_SUCCESS)
#endif
#ifdef EVPN_VXLAN_WANTED
        || (Nd6IsCacheForAddrLearntFromEvpn (&Ip6TgtAddr, &Nd6cEntry)
            == IP6_SUCCESS)
#endif
        )
    {
        /* If pkt is not to me and Proxy required */
        if ((IP6_FAILURE == i4IsPktToMe) && (IP6_TRUE == i4IsProxyReq) &&
            (Nd6IsProxyReqIcmp6Msg (pIf6, pIp6, pBuf) != IP6_TRUE))
        {
            /* If Not NS, NA, RA, NR then forward - RFC 4389 */
            /* else If any of these types, continue to Icmp6Rcv */
            Nd6ProxyForward (pIp6, pIf6,
                             (UINT4) (NTOHS (pIp6->u2Len) + IP6_HDR_LEN), pBuf);
            return;
        }
        else
        {
            Ip6Rcv (u1Type, IP6_HDR_LEN, pIp6, pIf6, pBuf);
        }
    }
#ifdef HA_WANTED
    else if (Ip6IsPktToMobile (pIf6, pIp6, pBuf) == SUCCESS)
    {
        return;
    }
#endif
    else if (u1Type == ADDR6_MULTI)
    {
#ifdef MLD_WANTED
        /* Received pkt may be a Mld packet */

        /* This check is based on the assumption that  ipv6 mld  
         * multicast data pkt will always have an ipv6          
         * hop-by-hop header                                    
         */

        /* Forwarding of mld data packets is taken care in Ip6Forward 
         */
        if (pIp6->u1Nh == NH_H_BY_H)
        {

            Ip6Forward (pIp6, pIf6,
                        (UINT4) (NTOHS (pIp6->u2Len) + IP6_HDR_LEN),
                        IP6_FWD_NO_SRCRT, pBuf);
            return;
        }
#endif
        /* check for ipv6 multicast data packet */
        if ((IS_RESERVED_LINK_SCOPE_MULTI (pIp6->dstAddr)) ||
            (IS_RESERVED_NODE_SCOPE_MULTI (pIp6->dstAddr)) ||
            (IS_RESERVED_MULTI (pIp6->dstAddr)) ||
            (IS_ALL_INVALID_MULTI (pIp6->dstAddr)))
            /*Invalid prefix based multicast address are also validated RFC3306 */
        {
            IP6_RELEASE_BUF (pBuf, FALSE);

            IP6IF_INC_IN_ADDR_ERR (pIf6);
            IP6SYS_INC_IN_ADDR_ERR (pIp6Cxt->Ip6SysStats);

#if defined (NPAPI_WANTED) && defined (CFA_WANTED)
            /* Remove the Entry created in the DLF Hash Table */
            Ipv6FsCfaHwRemoveIp6NetRcvdDlfInHash (pIp6Cxt->u4ContextId, Ip6Addr,
                                                  IP6_MAX_PREFIX_LEN);
#endif
            return;

        }
        else
        {
            if (IS_ALL_PREFIX_MULTI ((&pIp6->dstAddr)))
            {
                IP6_TASK_UNLOCK ();
                /* For SSM multicast address RP validation not needed */
                if (!(IS_ALL_PREFIX_SSM_MULTI (&pIp6->dstAddr)))
                {
                    if (NetIpv6ValidateUnicastPrefix
                        (pIp6Cxt->u4ContextId, pIf6->u4Index,
                         &(pIp6->dstAddr)) == IP6_FAILURE)
                    {
                        IP6_RELEASE_BUF (pBuf, FALSE);

                        IP6IF_INC_IN_ADDR_ERR (pIf6);
                        IP6SYS_INC_IN_ADDR_ERR (pIp6Cxt->Ip6SysStats);
#if defined (NPAPI_WANTED) && defined (CFA_WANTED)
                        /* Remove the Entry created in the DLF Hash Table */
                        Ipv6FsCfaHwRemoveIp6NetRcvdDlfInHash (pIp6Cxt->
                                                              u4ContextId,
                                                              Ip6Addr,
                                                              IP6_MAX_PREFIX_LEN);

#endif
                        IP6_TASK_LOCK ();
                        return;
                    }
                }
                IP6_TASK_LOCK ();
            }
            if (NetIpv6InvokeMCastApplReceive (pIp6Cxt, pBuf) ==
                NETIPV6_SUCCESS)
            {
                IP6IF_INC_IN_DELIVERS (pIf6);
                IP6SYS_INC_IN_DELIVERS (pIp6Cxt->Ip6SysStats);
                return;
            }
        }
    }
    else
    {
        Ip6Forward (pIp6, pIf6,
                    (UINT4) (NTOHS (pIp6->u2Len) + IP6_HDR_LEN),
                    IP6_FWD_NO_SRCRT, pBuf);
    }

}

/******************************************************************************
 * DESCRIPTION : If the packet is addressed to us then this routine will
 *               will be called to process the next-headers. Based on the
 *               next header value it will call the respective routines and 
 *               also it will be passed to the respective higher layer
 *
 * INPUTS      : The type of address (u1Type),the length of the processed
 *               IPv6 packet (u4Len),the pointer to the IPv6 header (pIp6),
 *               the interface on which the packet is received (pIf6) and
 *               the packet itself (pBuf).
 *
 * OUTPUTS     : None
 * 
 * RETURNS     : None
 *
 * NOTES       : During starting it is assumed that there are externsion
 *               headers in the packet (u4NoExt = 0 meaning setting
 *               u4NoExt to FALSE).
 ******************************************************************************/

VOID
Ip6Rcv (UINT1 u1Type, UINT4 u4Len, tIp6Hdr * pIp6, tIp6If * pIf6,
        tCRU_BUF_CHAIN_HEADER * pBuf)
{
    UINT1               u1NextHdr = 0;
    UINT4               u4Status = 0, u4NoExt = 1, u4TotLen = 0;
    tCRU_BUF_CHAIN_HEADER *pNewbuf = NULL;
    tIp6Hdr             ip6Hdr;
    tIp6Cxt            *pIp6Cxt = NULL;
    tIp6Hdr            *pIp6Hdr = NULL;
#if defined (IPSECv6_WANTED) && !defined (VPN_WANTED)
    UINT1               u1AddrType = 0;
#endif

    UINT4               u4ReadOffAfterReasm = 0;
    UINT4               u4ParmProbPtr = 0;    /* To fill offset in
                                               ICMP parameter problem
                                               error packet */
#ifdef MIP6_WANTED
    tIp6Addr            HomeAddrOptAddr;
    SET_ADDR_UNSPECIFIED (HomeAddrOptAddr);
#endif

    u1NextHdr = pIp6->u1Nh;
    u4TotLen = u4Len + NTOHS (pIp6->u2Len);
    u4Status = IP6_EXH_HDR_CONTINUE;

    pIp6Cxt = pIf6->pIp6Cxt;

    /*
     * traverse the extension headers until we come across a problem, the
     * packet is queued for reassembly or we come to an unknown extension
     * header (hopefully a higher layer protocol). Upon successful processing
     * of an extension header, the following next header field and the length
     * of datagram processed will be returned.
     */

    while (u4Status == IP6_EXH_HDR_CONTINUE)
    {
        switch (u1NextHdr)
        {
            case NH_H_BY_H:
                u4ParmProbPtr = IP6_BUF_READ_OFFSET (pBuf);
                /* if the HByH option does not come after IP6 Hdr, 
                 * send Param Problem Msg
                 */
                if (u4Len != IP6_HDR_LEN)
                {
                    Icmp6SendErrMsg (pIf6, pIp6, ICMP6_PKT_PARAM_PROBLEM,
                                     ICMP6_UNKNOWN_OPTION, IP6_HDR_LEN, pBuf);

                    IP6IF_INC_IN_HDR_ERR (pIf6);
                    IP6SYS_INC_IN_HDR_ERR (pIp6Cxt->Ip6SysStats);

                    u4Status = IP6_EXT_HDR_DISCARD;
                    IP6_TRC_ARG (pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                                 DATA_PATH_TRC, IP6_NAME,
                                 "IP6IN:Ip6Rcv: HByH Hdr after Non IP6 Hdr");

                    break;
                }
                if (Ip6PrcsHbyhOptHdr (&u1NextHdr, &u4Len, &u4TotLen,
                                       pIf6, pIp6, pBuf, OSIX_TRUE) ==
                    IP6_FAILURE)
                {
                    IP6IF_INC_IN_HDR_ERR (pIf6);
                    IP6SYS_INC_IN_HDR_ERR (pIp6Cxt->Ip6SysStats);

                    Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE,
                                   IP6_MODULE);
                    u4Status = IP6_EXT_HDR_DISCARD;
                    break;
                }
                break;

            case NH_DEST_HDR:
                u4NoExt = 0;
                u4ParmProbPtr = IP6_BUF_READ_OFFSET (pBuf);
#ifdef MIP6_WANTED
                if (Ip6PrcsDestOptHdr (&u1NextHdr, &u4Len,
                                       pIf6, pIp6, pBuf,
                                       &HomeAddrOptAddr) != IP6_SUCCESS)
#else
                if (Ip6PrcsDestOptHdr (&u1NextHdr, &u4Len,
                                       pIf6, pIp6, pBuf) != IP6_SUCCESS)
#endif
                {
                    IP6IF_INC_IN_HDR_ERR (pIf6);
                    IP6SYS_INC_IN_HDR_ERR (pIp6Cxt->Ip6SysStats);

                    Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE,
                                   IP6_MODULE);
                    u4Status = IP6_EXT_HDR_DISCARD;
                    break;
                }
                break;

            case NH_ROUTING_HDR:
                u4ParmProbPtr = IP6_BUF_READ_OFFSET (pBuf);
                u4NoExt = 0;
                if (Ip6PrcsRtHdr (&u1Type, &u1NextHdr, &u4Len,
                                  u4TotLen, pIf6, pIp6,
                                  pBuf) == IP6_EXT_HDR_DISCARD)
                {
                    IP6IF_INC_IN_HDR_ERR (pIf6);
                    IP6SYS_INC_IN_HDR_ERR (pIp6Cxt->Ip6SysStats);

                    u4Status = IP6_EXT_HDR_DISCARD;
                    break;
                }

                break;

            case NH_FRAGMENT_HDR:
                u4ParmProbPtr = IP6_BUF_READ_OFFSET (pBuf);
                if (gIp6GblInfo.i1JmbPktFlag)
                {
                    Icmp6SendErrMsg (pIf6, pIp6, ICMP6_PKT_PARAM_PROBLEM,
                                     ICMP6_HDR_PROB, u4Len, pBuf);

                    IP6IF_INC_IN_HDR_ERR (pIf6);
                    IP6SYS_INC_IN_HDR_ERR (pIp6Cxt->Ip6SysStats);

                    u4Status = IP6_EXT_HDR_DISCARD;
                    break;
                }

                if (!(u1Type & ADDR6_TENTATIVE))
                {
                    if ((pNewbuf = Ip6HandleReasm (&u1NextHdr, &u4Len,
                                                   pIf6, pIp6, pBuf)) == NULL)
                    {
                        u4Status = IP6_EXT_HDR_DISCARD;
                        break;
                    }

                    pBuf = pNewbuf;

                    u4ReadOffAfterReasm = IP6_BUF_READ_OFFSET (pBuf);
                    IP6_BUF_READ_OFFSET (pBuf) = 0;

                    /* 
                     * Get IPv6 header from the reassembled packet 
                     */

                    pIp6Hdr = (tIp6Hdr *)
                        Ip6BufRead (pBuf, (UINT1 *) &ip6Hdr, 0,
                                    (UINT4) (sizeof (tIp6Hdr)), 0);

                    if (pIp6Hdr == NULL)
                    {
                        u4Status = IP6_EXT_HDR_DISCARD;
                        Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE,
                                       IP6_MODULE);
                        break;
                    }
                    IP6_BUF_READ_OFFSET (pBuf) = u4ReadOffAfterReasm;

                    u4TotLen = IP6_HDR_LEN + NTOHS (pIp6Hdr->u2Len);

                }
                else
                {
                    /*
                     * The address was tentative So no reassembly eventhough
                     * packet is addressed to us
                     */
                    IP6IF_INC_IN_DISCARDS (pIf6);
                    IP6SYS_INC_IN_DISCARDS (pIp6Cxt->Ip6SysStats);

                    /* Release buf */
                    if (Ip6BufRelease
                        (pIp6Cxt->u4ContextId, pBuf, FALSE,
                         IP6_MODULE) == IP6_FAILURE)
                    {
                        IP6_TRC_ARG3 (pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                                      BUFFER_TRC, IP6_NAME,
                                      "IP6IN:Ip6Rcv: %s buf err:, Type = %d "
                                      "Module = 0x%X Bufptr = %p",
                                      ERROR_FATAL_STR, ERR_BUF_RELEASE, pBuf);
                    }

                    return;
                }
                break;

            default:
                /* Flow comes here when higher layer
                 * type is encountered or the header
                 * type is not defined. If the header
                 * type is not defined, ICMP error msg
                 * will be sent with parameter problem
                 * as the next header field's pointer
                 */
                u4Status = IP6_HIGHER_LAYER;
                break;
        }

    }

    if (u4Status == IP6_EXT_HDR_DISCARD)
    {
        return;
    }

#if defined (IPSECv6_WANTED) && !defined (VPN_WANTED)
    u4ParmProbPtr = IP6_BUF_READ_OFFSET (pBuf);
    if (Secv6InProcess (pBuf, pIf6, &u1NextHdr, &u4Len) == SEC_FAILURE)
    {
        Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE, IP6_MODULE);
        return;
    }
    else
    {
        pIp6Hdr = Ip6BufRead (pBuf, (UINT1 *) &ip6Hdr, 0, IPV6_HEADER_LEN, 0);
        if (pIp6Hdr == NULL)
        {
            Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE, IP6_MODULE);
            return;
        }
        pIp6 = pIp6Hdr;
        IP6_BUF_READ_OFFSET (pBuf) -= IPV6_HEADER_LEN;
        u4TotLen = CRU_BUF_Get_ChainValidByteCount (pBuf);
        {
            u1AddrType = (UINT1) Ip6AddrType (&pIp6->dstAddr);

            if ((Ip6IsPktToMeInCxt (pIp6Cxt, &u1AddrType, ADDR6_LLOCAL,
                                    &pIp6->dstAddr, pIf6) == IP6_FAILURE))
            {

                if ((Ip6IsPktToProxyInCxt (pIp6Cxt, &u1AddrType,
                                           &(pIp6->dstAddr),
                                           pBuf) == IP6_FAILURE))
                {
                    if (Nd6IsNDProxyRequired (pIf6, pIp6, pBuf) == IP6_TRUE)
                    {
                        if (Nd6IsProxyReqIcmp6Msg (pIf6, pIp6,
                                                   pBuf) != IP6_TRUE)
                        {
                            /* If Not NS, NA, RA, NR then forward - RFC 4389 */
                            /* else If any of these types, continue to Icmp6Rcv */
                            Nd6ProxyForward (pIp6, pIf6, u4TotLen, pBuf);
                            return;
                        }
                    }
                    else        /* Normal forwarding */
                    {
                        Ip6Forward (pIp6, pIf6, u4TotLen, IP6_FWD_NO_SRCRT,
                                    pBuf);
                        return;
                    }
                }
            }
        }
    }
#endif

#ifdef SLI_WANTED

    /*
     * When Application sends data to SLI, Protect Semaphore of the socket is
     * taken and pkt is deliverd to IP after  acquiring IP6LOCK. 
     *
     * Meanwhile when lower layer delivers a pkt to SLI via IP, IP6 Lock is
     * acquired and a wait on the same Socket semaphore happens and a dead 
     * lock occurs.
     * To avoid this deadlock, IP6 Lock is relinquished here and re-acquired
     * after enqueing to SLI
     */

    IP6_TASK_UNLOCK ();

    SliEnqueueIpv6RawPacketInCxt (pIp6Cxt->u4ContextId, u1NextHdr,
                                  (u4TotLen - u4Len),
                                  u4Len, (UINT1 *) pIp6, pBuf);
    IP6_TASK_LOCK ();
#endif

    switch (u1NextHdr)
    {
        case NH_ICMP6:
            IP6IF_INC_IN_DELIVERS (pIf6);
            IP6SYS_INC_IN_DELIVERS (pIp6Cxt->Ip6SysStats);
            Icmp6Rcv (pIf6, pIp6, (UINT2) (u4TotLen - u4Len), u1Type, pBuf);
            break;

        case NH_UDP6:
            if (!(u1Type & ADDR6_TENTATIVE))
            {
                IP6IF_INC_IN_DELIVERS (pIf6);
                IP6SYS_INC_IN_DELIVERS (pIp6Cxt->Ip6SysStats);
                Udp6Rcv (pIf6, pIp6, u4TotLen - u4Len, pBuf);
            }
            else
            {
                IP6IF_INC_IN_DISCARDS (pIf6);
                IP6SYS_INC_IN_DISCARDS (pIp6Cxt->Ip6SysStats);

                /* Release buf */
                if (Ip6BufRelease
                    (pIp6Cxt->u4ContextId, pBuf, FALSE,
                     IP6_MODULE) == IP6_FAILURE)
                {
                    IP6_TRC_ARG3 (pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                                  BUFFER_TRC, IP6_NAME,
                                  "IP6IN:Ip6Rcv: %s buf err: Type = %d Module = 0x%X Bufptr = %p",
                                  ERROR_FATAL_STR, ERR_BUF_RELEASE, pBuf);
                }

            }
            break;

#ifdef MIP6_WANTED
        case NH_MOBILITY_HDR:
            Mip6Rcv (pIp6, pIf6, pBuf, u4TotLen, u4Len, HomeAddrOptAddr);

            if (Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE, IP6_MODULE) ==
                IP6_FAILURE)
            {
                IP6_TRC_ARG3 (pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                              BUFFER_TRC, IP6_NAME,
                              "IP6IN:Ip6Rcv: Error",
                              ERROR_FATAL_STR, ERR_BUF_RELEASE, pBuf);

            }
            return;
#endif

#ifdef TUNNEL_WANTED
        case NH_IPV6:
            Ip6Tunl6Recv (pIf6, pBuf);
            break;
#endif

        case NH_NO_NEXT_HDR:
            /* Drop this packet. */
            Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE, IP6_MODULE);
            break;

        default:
            /* Possibly the Data Should have been received for the higher layer
             * protocol. Check whether if the respective protocol has been
             * registered with IP6. If yes then post the message to the
             * protocol. */
            if (NetIpv6InvokeApplicationReceive ((pBuf),
                                                 (u4TotLen - u4Len),
                                                 (pIf6->u4Index),
                                                 (UINT4) u1Type,
                                                 (UINT4) u1NextHdr) ==
                NETIPV6_SUCCESS)
            {
                /* Successfully posted the message to the Application. */
                IP6IF_INC_IN_DELIVERS (pIf6);
                IP6SYS_INC_IN_DELIVERS (pIp6Cxt->Ip6SysStats);
                break;
            }

            /*
             * No higher layer has registered for this protocol message.
             * Send ICMP error message with type as parameter problem, code as 2
             * and offset pointing to the erroneous next header value in the pkt
             * The offset has to calculated separately for the case when there
             * is no extention header after the IPv6 header and the case when
             * there is one.
             */

            IP6IF_INC_IN_UNKNOWN_PROTOS (pIf6);
            IP6SYS_INC_IN_UNKNOWN_PROTOS (pIp6Cxt->Ip6SysStats);

            if (u4NoExt == 1)
            {
                Icmp6SendErrMsg (pIf6, pIp6, ICMP6_PKT_PARAM_PROBLEM,
                                 ICMP6_UNKNOWN_OPTION,
                                 IP6_OFFSET_FOR_NEXTHDR_FIELD, pBuf);
            }
            else
            {
                Icmp6SendErrMsg (pIf6, pIp6, ICMP6_PKT_PARAM_PROBLEM,
                                 ICMP6_UNKNOWN_OPTION, u4ParmProbPtr, pBuf);
            }
            break;
    }
    return;
}

/***************************** END OF FILE **********************************/
