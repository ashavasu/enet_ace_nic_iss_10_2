/* *     $Id: ip6port.h,v 1.5 2017/12/19 13:41:54 siva Exp $ */
#ifndef _IP6PORT_H
#define _IP6PORT_H

/*
 *----------------------------------------------------------------------------- 
 *    FILE NAME                    :    ip6port.h
 *
 *    PRINCIPAL AUTHOR             :    Murali Krishna / Jaya Bharathi
 *
 *    SUBSYSTEM NAME               :    IPv6
 *
 *    MODULE NAME                  :    All Modules
 *
 *    LANGUAGE                     :    C
 *
 *    TARGET ENVIRONMENT           :    UNIX
 *
 *    DATE OF FIRST RELEASE        :    DDD-MM-2001
 *
 *    DESCRIPTION                  :    This file contains constants, macros,
 *                                      prototypes and typedefs which are
 *                                      used by IPv6 from other external 
 *                                      modules. 
 *                                      These are likely to change during
 *                                      porting to a target platform.
 *
 *----------------------------------------------------------------------------- 
 */

#define IP6_SNMP_AGT_REGISTERMIB       SNMP_AGT_RegisterMib
#define IP6_SOCK_OVER_V4               socket 
#define IP6_SET_SOCK_OPT               setsockopt 
#define IP6_REG_V4_PMTU                SliRegisterPmtuHandler 
#define IP6_CONNECT                    connect 
#define IP6_CLOSE                      close
#define IP6_INIT_COMPLETE(u4Status)     lrInitComplete(u4Status)
#define PING6_INIT_COMPLETE(u4Status)   lrInitComplete(u4Status)

/* Redundancy support */
#ifdef RM_WANTED
#define   ND6_IS_NP_PROGRAMMING_ALLOWED() \
           L2RED_IS_NP_PROGRAMMING_ALLOWED ()
#else
#define   ND6_IS_NP_PROGRAMMING_ALLOWED() OSIX_TRUE
#endif

/* Functions that needs to be ported with Lower Layer */
INT4    Ip6RegisterLL           PROTO ((VOID));

VOID    Ip6NotifyLL             PROTO ((VOID));

INT4    Ip6SendToLL             PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf,
                                        UINT4 u4IfIndex, UINT1 *au1MediaAddr,
                                        UINT1 u1PktType, UINT1 u1EncapType));

INT4    Ip6GetHwAddr            PROTO ((UINT4 u4IfIndex, UINT1 *au1HwAddr));
    
INT4    Ip6GetDot1qVlanEncapStatus PROTO ((UINT4 u4IfIndex));
    
INT1    Ip6ifUpdateEthMaddr     PROTO ((tIp6If * pIf6, UINT1 *u1MacAddr,
                                        UINT1 u1Oper));

INT4    Ip6GetIfName            PROTO ((UINT4 u4IfIndex, UINT1 *au1IfName));

INT4    Ip6GetIfIndexFromName   PROTO ((UINT1 *au1IfName, UINT4 *pu4IfIndex));
    
/* IP6 - IP Portable Functions. */
#ifdef TUNNEL_WANTED
INT4    Ip6GetTunnelInterfaceInCxt PROTO ((UINT4, UINT4, UINT4, UINT4 *));

INT4    Ip6GetPhyIfForTunlIndex PROTO ((UINT4 u4TunlIndex, UINT4 *pu4IfIndex));

INT4    Ip6ValidateIpv4Addr     PROTO ((UINT4 u4DstIp, UINT4 u4TunlIndex));

INT4    Ip6TunnelOutput         PROTO ((UINT4 u4Dest, INT1 i1Tos, UINT2 u2Proto,
                                        INT1 i1Ttl,
                                        tCRU_BUF_CHAIN_HEADER * pBuf,
                                        UINT2 u2Len, INT2 i2Id, INT1 i1Df,
                                        UINT1 u1Olen, UINT4 u4Port));

#endif
#endif
