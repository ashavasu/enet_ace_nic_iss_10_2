/*******************************************************************
 *  Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *  
 *  $Id: icmp6.h,v 1.10 2015/06/30 06:06:15 siva Exp $
 *    
 *  
 ********************************************************************/



#ifndef _ICMP6_H
#define _ICMP6_H
/*
 *----------------------------------------------------------------------------- 
 *    FILE NAME                    :    icmp6.h                                 
 *                                                                              
 *    PRINCIPAL AUTHOR             :    Anshul Garg                             
 *                                                                              
 *    SUBSYSTEM NAME               :    IPv6                                    
 *                                                                              
 *    MODULE NAME                  :    ICMP6 Module                            
 *                                                                              
 *    LANGUAGE                     :    C                                       
 *                                                                              
 *    TARGET ENVIRONMENT           :    UNIX                                    
 *                                                                              
 *    DATE OF FIRST RELEASE        :    Dec-02-1996                             
 *                                                                              
 *    DESCRIPTION                  :    This file contains typedef and macro
 *                                      definitions for handling various     
 *                                      Control messages       
 *                                                                              
 *----------------------------------------------------------------------------- 
 */

/*
 * ICMP6 main header
 */

/*
 * ICMP6 Information Message Header
 */

typedef struct
{
    tIcmp6PktHdr  icmp6Hdr;  /* common header */
    INT2             u2Id;      /* Id of the ECHO packet */
    INT2             u2Seqno;   /* Sequence no. of the ECHO packet */
}
tIcmp6InfoMsg;

/*
 * ICMP6 Error Message Header
 */

typedef struct
{
    tIcmp6PktHdr  icmp6Hdr;    /* common header */
    UINT4            u4ErrInfo;  /* error information */
}
tIcmp6ErrMsg;


#define ICMP6_GET_ERR_CXT_INFO_FROM_TIMER(x,y) \
        (y *)(VOID *)((UINT1 *)(x) - IP6_OFFSET(y,ErrIntervalTimer))

#define ICMP_MAX_ERR_MSG_COUNT(errInterval, bucketSize) ((1000 * bucketSize) / errInterval)


/*
 * Constant definitions for the ICMPv6 messages
 */

/* ICMPv6 Message Types */

#define  ICMP6_PKT_TOO_BIG            2   
#define  ICMP6_TIME_EXCEEDED          3   
#define  ICMP6_PKT_PARAM_PROBLEM      4   
#define  ICMP6_ECHO_REQUEST           128 
#define  ICMP6_ECHO_REPLY             129 
#define  ICMP6_INFO_MSG_TYPE_MIN_VAL  128 
#define  ICMP6_FORMAT                 58
#define  ICMP6_OFFSET                 6

/* Neighbour Discovery Message Types */

#define  ROUTER_SOLICITATION      133 
#define  ROUTER_ADVERTISEMENT     134 
#define  NEIGHBOUR_SOLICITATION   135 
#define  NEIGHBOUR_ADVERTISEMENT  136 
#define  ROUTE_REDIRECT           137 
#define  ND6_CERT_PATH_SOLICITATION 148 
#define  ND6_CERT_PATH_ADVERTISEMENT 149
#define  ICMP6_NS_NA_OFFSET       40

/* ICMPv6 Time Exceeded Message Codes */

#define  ICMP6_HOP_LIMIT_EXCEEDED     0 
#define  ICMP6_FRAG_REASM_TMEXCEEDED  1 

/* ICMPv6 Packet Parameter Problem Message Codes */

#define  ICMP6_HDR_PROB         0 
#define  ICMP6_UNKNOWN_OPTION   1
#define  ICMP6_UNKNOWN_OPTTYPE  2

/* Other ICMP6 related constants and macros */

#define  ICMP6_MAX_ERR_DATA_SIZE  576              
#define  ICMP6_MIN(x,y)           (x <= y ? x : y) 

/*Global ICMP6 counters*/
#define ICMP6_INC_IN_MSGS(icmp6Stats)           \
         icmp6Stats.u4InMsgs++
#define ICMP6_INC_IN_ERRS(icmp6Stats)           \
         icmp6Stats.u4InErrs++
#define ICMP6_INC_IN_BADCODE(icmp6Stats)        \
         icmp6Stats.u4InBadcode++
#define ICMP6_INC_IN_TOOBIG(icmp6Stats)         \
         icmp6Stats.u4InToobig++
#define ICMP6_INC_IN_DST_UNREACH(icmp6Stats)    \
         icmp6Stats.u4InDstUnreach++
#define ICMP6_INC_IN_TMEXCEEDED(icmp6Stats)     \
         icmp6Stats.u4InTmexceeded++
#define ICMP6_INC_IN_PARAMPROB(icmp6Stats)      \
         icmp6Stats.u4InParamprob++
#define ICMP6_INC_IN_ECHO_REQ(icmp6Stats)       \
         icmp6Stats.u4InEchoReq++
#define ICMP6_INC_IN_ECHO_RESP(icmp6Stats)      \
         icmp6Stats.u4InEchoResp++
#define ICMP6_INC_IN_MLD_QUERY(icmp6Stats)      \
         icmp6Stats.u4InMLDQuery++
#define ICMP6_INC_IN_MLD_REPORT(icmp6Stats)      \
         icmp6Stats.u4InMLDReport++
#define ICMP6_INC_IN_MLD_DONE(icmp6Stats)      \
         icmp6Stats.u4InMLDDone++

#define ICMP6_INC_OUT_MSGS(icmp6Stats)          \
         icmp6Stats.u4OutMsgs++
#define ICMP6_INC_OUT_ERRS(icmp6Stats)          \
         icmp6Stats.u4OutErrs++
#define ICMP6_INC_OUT_DST_UNREACH(icmp6Stats)   \
         icmp6Stats.u4OutDstUnreach++
#define ICMP6_INC_OUT_TOOBIG(icmp6Stats)        \
         icmp6Stats.u4OutToobig++
#define ICMP6_INC_OUT_TMEXCEEDED(icmp6Stats)    \
         icmp6Stats.u4OutTmexceeded++
#define ICMP6_INC_OUT_PARAMPROB(icmp6Stats)     \
         icmp6Stats.u4OutParamprob++
#define ICMP6_INC_OUT_ECHO_REQ(icmp6Stats)      \
         icmp6Stats.u4OutEchoReq++
#define ICMP6_INC_OUT_ECHO_RESP(icmp6Stats)     \
         icmp6Stats.u4OutEchoResp++
#define ICMP6_INC_OUT_MLD_QUERY(icmp6Stats)     \
         icmp6Stats.u4OutMLDQuery++
#define ICMP6_INC_OUT_MLD_REPORT(icmp6Stats)    \
         icmp6Stats.u4OutMLDReport++
#define ICMP6_INC_OUT_MLD_DONE(icmp6Stats)      \
         icmp6Stats.u4OutMLDDone++
#define ICMP6_INC_OUT_RATE_LIMITED(icmp6Stats)  \
         icmp6Stats.u4IcmpOutRateLimit++

#define ICMP6_INC_IN_RSOLS(icmp6Stats)          \
         icmp6Stats.u4InRouterSol++
#define ICMP6_INC_IN_RADVS(icmp6Stats)          \
         icmp6Stats.u4InRouterAdv++
#define ICMP6_INC_IN_NSOLS(icmp6Stats)          \
         icmp6Stats.u4InNeighSol++
#define ICMP6_INC_IN_NADVS(icmp6Stats)          \
         icmp6Stats.u4InNeighAdv++
#define ICMP6_INC_IN_REDIRS(icmp6Stats)         \
         icmp6Stats.u4InRedir++

#define ICMP6_INC_OUT_RSOLS(icmp6Stats)         \
         icmp6Stats.u4OutRouterSol++
#define ICMP6_INC_OUT_RADVS(icmp6Stats)         \
         icmp6Stats.u4OutRouterAdv++
#define ICMP6_INC_OUT_NSOLS(icmp6Stats)         \
         icmp6Stats.u4OutNeighSol++
#define ICMP6_INC_OUT_NADVS(icmp6Stats)         \
         icmp6Stats.u4OutNeighAdv++
#define ICMP6_INC_OUT_REDIRS(icmp6Stats)        \
         icmp6Stats.u4OutRedir++
#define ICMP6_INC_IN_NA_RFLAG(icmp6Stats)        \
         icmp6Stats.u4InNARouterFlag++
#define ICMP6_INC_IN_NA_SFLAG(icmp6Stats)        \
         icmp6Stats.u4InNASolicitedFlag++
#define ICMP6_INC_IN_NA_OFLAG(icmp6Stats)        \
         icmp6Stats.u4InNAOverrideFlag++
#define ICMP6_INC_OUT_NA_RFLAG(icmp6Stats)        \
         icmp6Stats.u4OutNARouterFlag++
#define ICMP6_INC_OUT_NA_SFLAG(icmp6Stats)        \
         icmp6Stats.u4OutNASolicitedFlag++
#define ICMP6_INC_OUT_NA_OFLAG(icmp6Stats)        \
         icmp6Stats.u4OutNAOverrideFlag++
#ifdef TUNNEL_WANTED
#define        IP6_GET_ICMP6_CODE(i1Code)              \
        if((i1Code == 0) || ( i1Code == 1 ) ||  \
           (i1Code == 11) || (i1Code == 12))    \
        i1Code = 0;                             \
        else if ((i1Code == 5) || (i1Code == 6) \
             || (i1Code == 7) || (i1Code == 8)) \
        i1Code = 2;                             \
        if ((i1Code == 9) || (i1Code == 10))    \
        i1Code = 1;
#endif 

/*Per interface ICMP6 counters*/
#define ICMP6_INTF_INC_IN_MSGS(pIf6)           \
         pIf6->pIfIcmp6Stats->u4InMsgs++
#define ICMP6_INTF_INC_IN_ERRS(pIf6)           \
         pIf6->pIfIcmp6Stats->u4InErrs++
#define ICMP6_INTF_INC_IN_BADCODE(pIf6)        \
         pIf6->pIfIcmp6Stats->u4InBadcode++
#define ICMP6_INTF_INC_IN_TOOBIG(pIf6)         \
         pIf6->pIfIcmp6Stats->u4InToobig++
#define ICMP6_INTF_INC_IN_DST_UNREACH(pIf6)    \
         pIf6->pIfIcmp6Stats->u4InDstUnreach++
#define ICMP6_INTF_INC_IN_TMEXCEEDED(pIf6)     \
         pIf6->pIfIcmp6Stats->u4InTmexceeded++
#define ICMP6_INTF_INC_IN_PARAMPROB(pIf6)      \
         pIf6->pIfIcmp6Stats->u4InParamprob++
#define ICMP6_INTF_INC_IN_ECHO_REQ(pIf6)       \
         pIf6->pIfIcmp6Stats->u4InEchoReq++
#define ICMP6_INTF_INC_IN_ECHO_RESP(pIf6)      \
         pIf6->pIfIcmp6Stats->u4InEchoResp++
#define ICMP6_INTF_INC_IN_MLD_QUERY(pIf6)      \
         pIf6->pIfIcmp6Stats->u4InMLDQuery++
#define ICMP6_INTF_INC_IN_MLD_REPORT(pIf6)      \
         pIf6->pIfIcmp6Stats->u4InMLDReport++
#define ICMP6_INTF_INC_IN_MLD_DONE(pIf6)      \
         pIf6->pIfIcmp6Stats->u4InMLDDone++

#define ICMP6_INTF_INC_OUT_MSGS(pIf6)          \
         pIf6->pIfIcmp6Stats->u4OutMsgs++
#define ICMP6_INTF_INC_OUT_ERRS(pIf6)          \
         pIf6->pIfIcmp6Stats->u4OutErrs++
#define ICMP6_INTF_INC_OUT_DST_UNREACH(pIf6)   \
         pIf6->pIfIcmp6Stats->u4OutDstUnreach++
#define ICMP6_INTF_INC_OUT_TOOBIG(pIf6)        \
         pIf6->pIfIcmp6Stats->u4OutToobig++
#define ICMP6_INTF_INC_OUT_TMEXCEEDED(pIf6)    \
         pIf6->pIfIcmp6Stats->u4OutTmexceeded++
#define ICMP6_INTF_INC_OUT_PARAMPROB(pIf6)     \
         pIf6->pIfIcmp6Stats->u4OutParamprob++
#define ICMP6_INTF_INC_OUT_ECHO_REQ(pIf6)      \
         pIf6->pIfIcmp6Stats->u4OutEchoReq++
#define ICMP6_INTF_INC_OUT_ECHO_RESP(pIf6)     \
         pIf6->pIfIcmp6Stats->u4OutEchoResp++
#define ICMP6_INTF_INC_OUT_MLD_QUERY(pIf6)     \
         pIf6->pIfIcmp6Stats->u4OutMLDQuery++
#define ICMP6_INTF_INC_OUT_MLD_REPORT(pIf6)    \
         pIf6->pIfIcmp6Stats->u4OutMLDReport++
#define ICMP6_INTF_INC_OUT_MLD_DONE(pIf6)      \
         pIf6->pIfIcmp6Stats->u4OutMLDDone++
#define ICMP6_INTF_INC_OUT_RATE_LIMITED(pIf6)  \
         pIf6->pIfIcmp6Stats->u4IcmpOutRateLimit++

#define ICMP6_INTF_INC_IN_RSOLS(pIf6)          \
         pIf6->pIfIcmp6Stats->u4InRouterSol++
#define ICMP6_INTF_INC_IN_RADVS(pIf6)          \
         pIf6->pIfIcmp6Stats->u4InRouterAdv++
#define ICMP6_INTF_INC_IN_NSOLS(pIf6)          \
         pIf6->pIfIcmp6Stats->u4InNeighSol++
#define ICMP6_INTF_INC_IN_NADVS(pIf6)          \
         pIf6->pIfIcmp6Stats->u4InNeighAdv++
#define ICMP6_INTF_INC_IN_REDIRS(pIf6)         \
         pIf6->pIfIcmp6Stats->u4InRedir++

#define ICMP6_INTF_INC_OUT_RSOLS(pIf6)         \
         pIf6->pIfIcmp6Stats->u4OutRouterSol++
#define ICMP6_INTF_INC_OUT_RADVS(pIf6)         \
         pIf6->pIfIcmp6Stats->u4OutRouterAdv++
#define ICMP6_INTF_INC_OUT_NSOLS(pIf6)         \
         pIf6->pIfIcmp6Stats->u4OutNeighSol++
#define ICMP6_INTF_INC_OUT_NADVS(pIf6)         \
         pIf6->pIfIcmp6Stats->u4OutNeighAdv++
#define ICMP6_INTF_INC_OUT_REDIRS(pIf6)        \
         pIf6->pIfIcmp6Stats->u4OutRedir++
#endif /* _ICMP6_H */
