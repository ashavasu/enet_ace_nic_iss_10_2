/*******************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: nd6main.c,v 1.68.2.1 2018/03/09 13:40:47 siva Exp $
 *
 * Description: This file contains the ND routines handling 
 *        data-structures initialisation, NDCache
 *        entries creation/deletion/updation, timer
 *        based services and status indications
 *
 *******************************************************************/

#include "ip6inc.h"
#include "nd6red.h"

/*
 *  Global Declarations
 */

tNdGblInfo          gNd6GblInfo;
tOsixSemId          gNd6SemId = 0;
extern tTMO_SLL     Nd6RouteList;
PRIVATE INT4        ND6CreateSemaphore (void);
UINT4               gu4CumulativeNdHwFailCount = 0;
/*****************************************************************************
* DESCRIPTION : Allocates Memory for HASH, NDCache and
*               NDCache LAN INFO tables
*
* INPUTS      : None
*
* OUTPUTS     : None
*
* RETURNS     : IP6_SUCCESS     - Initialisation successful
*               IP6_FAILURE - Initialisation fails
*****************************************************************************/

INT4
Nd6Init (VOID)
{

    gNd6GblInfo.Nd6CacheTable = RBTreeCreateEmbedded (FSAP_OFFSETOF
                                                      (tNd6CacheEntry,
                                                       RbNode),
                                                      Nd6RBTreeCacheEntryCmp);
    if (gNd6GblInfo.Nd6CacheTable == NULL)
    {
        IP6_GBL_TRC_ARG (ND6_MOD_TRC, MGMT_TRC | ALL_FAILURE_TRC, ND6_NAME,
                         "ND6:Nd6Init: NDCache RBTree Create Failed!!\n");
        return (IP6_FAILURE);
    }

    if (gNd6RedGlobalInfo.u1RedStatus == IP6_TRUE)
    {
        ND6CreateSemaphore ();
        gIp6GblInfo.Nd6RedTable =
            RBTreeCreateEmbedded (FSAP_OFFSETOF (tNd6RedTable, RbNode),
                                  Nd6RBTreeRedEntryCmp);
        if (gIp6GblInfo.Nd6RedTable == NULL)
        {
            IP6_GBL_TRC_ARG (ND6_MOD_TRC, MGMT_TRC | ALL_FAILURE_TRC, ND6_NAME,
                             "ND6:Nd6Init: ND Redundancy Cache RBTree Create Failed!\n");
            return IP6_FAILURE;
        }
    }
    gNd6GblInfo.u4Nd6SeNDPrefixCheck = ND6_PREFIX_CHECK_DISABLE;
    gNd6GblInfo.u4Nd6SeNDMinKeyLength = 1024;
    gNd6GblInfo.u4Nd6SeNDAcceptUnsecure = ND6_ACCEPT_UNSEC_ADV_ENABLE;
    TMO_SLL_Init (&Nd6RouteList);
    return (IP6_SUCCESS);

}

/*****************************************************************************
* DESCRIPTION : Creates an entry in the NDCache table
* 
* INPUTS      : pIf6        -   Pointer to the interface 
*               pAddr6      -   Pointer to IPv6 address
*               pLladdr     -   Pointer to lower layer address
*               u1LlaLen   -   Length of lower layer address
*               u1State     -   Reachability state of entry
* 
* OUTPUTS     : None
* 
* RETURNS     : Pointer to NDCache entry, creation succeeds
*               NULL -  NDCache entry creation fails
*****************************************************************************/

tNd6CacheEntry     *
Nd6CreateCache (tIp6If * pIf6, tIp6Addr * pAddr6, UINT1 *pLladdr,
                UINT1 u1LlaLen, UINT1 u1State, UINT1 u1SecureFlag)
{

    tNd6CacheEntry     *pNd6cEntry = NULL;
    tNd6CacheLanInfo   *pNd6cLinfo = NULL;
    UINT4               u4ContextId = pIf6->pIp6Cxt->u4ContextId;

    IP6_TRC_ARG5 (u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC, ND6_NAME,
                  "ND6:Nd6CreateCache: IF %d, IPv6 addr:%s LLayerAddrLen %d  "
                  "LLA=%s ReachSte %d",
                  pIf6->u4Index, Ip6PrintAddr (pAddr6), u1LlaLen,
                  Ip6PrintIftok (pLladdr, u1LlaLen), u1State);

    /*
     * Upon the NDCache table being already full, delete the stalest
     * dynamic entry if present and create space for this entry
     */

    if ((gu4Nd6CacheEntries == MAX_IP6_ND6_HW_CACHE_ENTRIES) &&
        (u1State != ND6_CACHE_ENTRY_STALE))
    {
        return NULL;
    }
    if (gNd6GblInfo.u4Nd6CacheEntries ==
        FsIP6SizingParams[MAX_IP6_ND6_CACHE_ENTRIES_SIZING_ID].
        u4PreAllocatedUnits)
    {
        if (Nd6PurgeStalestCache () == IP6_FAILURE)
        {
            IP6_TRC_ARG (u4ContextId, ND6_MOD_TRC, ALL_FAILURE_TRC, ND6_NAME,
                         "ND6 :Nd6CreateCache: Creating Cache Failed - No "
                         "Free Entry\n");
            return (NULL);
        }
    }

    /* Allocate space for the entry */
    if ((pNd6cEntry =
         (tNd6CacheEntry *) (VOID *) Ip6GetMem (u4ContextId,
                                                gNd6GblInfo.i4Nd6CacheId)) ==
        NULL)
    {
        IP6_TRC_ARG (u4ContextId, ND6_MOD_TRC, ALL_FAILURE_TRC, ND6_NAME,
                     "ND6 :Nd6CreateCache: Creating Cache Failed - "
                     "Memory Allocation FAILED\n");
        return (NULL);
    }

    /* Fill the fields of the entry */

    MEMSET (pNd6cEntry, 0, sizeof (tNd6CacheEntry));
    pNd6cEntry->u1ProxyCache = IP6_FALSE;
    pNd6cEntry->u1LlaFlag = FALSE;
    /* update the secure flag for this first message */
    if (ND6_SECURE_DISABLE != pIf6->u1SeNDStatus)
    {
        pNd6cEntry->u1SecureFlag = u1SecureFlag;
    }

    TMO_DLL_Init (&(pNd6cEntry->NdProxyNsSrcList));
    Ip6AddrCopy (&pNd6cEntry->addr6, pAddr6);
    pNd6cEntry->pIf6 = pIf6;

    /* Lan info on tunnel interface is for getting the reachability information
     * before sending the packet out over ipv4 cloud. It doesn't actually serve
     * the purpose of getting physical interface address */

    if ((pIf6->u1IfType == IP6_ENET_INTERFACE_TYPE) ||
        (pIf6->u1IfType == IP6_PSEUDO_WIRE_INTERFACE_TYPE) ||
        (pIf6->u1IfType == IP6_L3VLAN_INTERFACE_TYPE) ||
        (pIf6->u1IfType == IP6_LAGG_INTERFACE_TYPE) ||
        (pIf6->u1IfType == IP6_L3SUB_INTF_TYPE)
#ifdef TUNNEL_WANTED
        || (pIf6->u1IfType == IP6_TUNNEL_INTERFACE_TYPE)
#endif
#ifdef WGS_WANTED
        || (pIf6->u1IfType == IP6_L2VLAN_INTERFACE_TYPE)
#endif
        )
    {
        /* Allocate space for LAN information for the entry */
        if ((pNd6cLinfo =
             (tNd6CacheLanInfo *) (VOID *) Ip6GetMem (u4ContextId,
                                                      gNd6GblInfo.
                                                      i4Nd6cLanId)) == NULL)
        {
            Ip6RelMem (u4ContextId, (UINT2) gNd6GblInfo.i4Nd6CacheId,
                       (UINT1 *) pNd6cEntry);
            return (NULL);
        }
    }

    if (pNd6cLinfo)
    {
        MEMSET (pNd6cLinfo, 0, sizeof (tNd6CacheLanInfo));
        pNd6cEntry->pNd6cInfo = (UINT1 *) pNd6cLinfo;
        u1LlaLen = (UINT1) MEM_MAX_BYTES (u1LlaLen, IP6_MAX_LLA_LEN);
        if (pLladdr)
        {
            MEMCPY (pNd6cLinfo->lladdr, pLladdr, u1LlaLen);
        }
        pNd6cLinfo->u1LlaLen = u1LlaLen;
        pNd6cLinfo->pNd6cEntry = pNd6cEntry;
    }

    /* Set the RowStatus */
    if (u1State != ND6C_STATIC_NOT_IN_SERVICE)
    {
        /* This is for Dynamic Entries and Static Active */
        pNd6cEntry->u1RowStatus = ACTIVE;
    }
    else
    {
        if (pLladdr != NULL)
        {
            pNd6cEntry->u1RowStatus = NOT_IN_SERVICE;
        }
        else
        {
            pNd6cEntry->u1RowStatus = NOT_READY;
        }
    }

    /* Update the NDCache last updated time for this entry */
    OsixGetSysTime ((tOsixSysTime *) & (pNd6cEntry->u4LastUpdatedTime));

    if (RBTreeAdd (gNd6GblInfo.Nd6CacheTable, (tRBElem *) pNd6cEntry)
        == RB_FAILURE)
    {
        IP6_TRC_ARG1 (u4ContextId, ND6_MOD_TRC, ALL_FAILURE_TRC, ND6_NAME,
                      "Addition of the neighbor cache entry to the "
                      "RBTree FAILED for the address %s \n",
                      Ip6PrintNtop (pAddr6));
        if (pNd6cLinfo)
        {
            Ip6TmrStop (u4ContextId, ND6_RETRANS_TIMER_ID,
                        gIp6GblInfo.Ip6TimerListId,
                        &(pNd6cLinfo->timer.appTimer));
            Ip6RelMem (u4ContextId, (UINT2) gNd6GblInfo.i4Nd6cLanId,
                       (UINT1 *) pNd6cLinfo);
        }
        Ip6RelMem (u4ContextId, (UINT2) gNd6GblInfo.i4Nd6CacheId,
                   (UINT1 *) pNd6cEntry);
        return NULL;
    }
    /* Update Nd6 Reach State */
    Nd6SetReachState (pNd6cEntry, u1State);

    pIf6->pIp6Cxt->u4Nd6CacheEntries++;
    gNd6GblInfo.u4Nd6CacheEntries++;
    return (pNd6cEntry);

}

/*****************************************************************************
* DESCRIPTION : Purges the stalest entries in the NDCache table
*               irrespective of the interface type based on
*               its reach (last use) time
* 
* INPUTS      : None
* 
* OUTPUTS     : None
* 
* RETURNS     : IP6_SUCCESS             -  Stalest ND entry deleted
*             : IP6_FAILURE         -  Deletion fails
*****************************************************************************/

INT4
Nd6PurgeStalestCache (VOID)
{
    tNd6CacheEntry     *pNd6cEntry = NULL;
    tNd6CacheEntry     *pNd6cEntryTmp = NULL;
    UINT4               u4CurrentTime = 0, u4InactiveTime = 0;
    INT4                i4PurgeStatus = IP6_FAILURE;

    /*
     * The garbabe collection on NDCache entries takes place based
     * on the last use time and those entries get deleted
     */
    u4CurrentTime = OsixGetSysUpTime ();

    pNd6cEntry = RBTreeGetFirst (gNd6GblInfo.Nd6CacheTable);

    while (pNd6cEntry != NULL)
    {
        pNd6cEntryTmp = RBTreeGetNext (gNd6GblInfo.Nd6CacheTable, pNd6cEntry,
                                       NULL);

        if ((pNd6cEntry->u1ReachState == ND6C_STATIC) ||
            (pNd6cEntry->u1ReachState == ND6C_INCOMPLETE) ||
            (pNd6cEntry->u1ReachState == ND6C_STATIC_NOT_IN_SERVICE))
        {
            pNd6cEntry = pNd6cEntryTmp;
            continue;
        }

        u4InactiveTime =
            ND6_GET_DIFF_TIME (u4CurrentTime, pNd6cEntry->u4LastUseTime);

        if (u4InactiveTime >= ND6_GARB_COLLECT_INTERVAL)
        {
            if (Nd6PurgeCache (pNd6cEntry) == IP6_FAILURE)
            {
                IP6_TRC_ARG (pNd6cEntry->pIf6->pIp6Cxt->u4ContextId,
                             ND6_MOD_TRC, ALL_FAILURE_TRC, ND6_NAME,
                             "ND6:Nd6PurgeStalestCache: Purging Stale Cache Failed\n");
            }
            else
            {
                i4PurgeStatus = IP6_SUCCESS;
            }
        }
        pNd6cEntry = pNd6cEntryTmp;
    }
    return (i4PurgeStatus);

}

/*****************************************************************************
* DESCRIPTION : Purges either all or dynamic NDCache entries 
*               of an interface
* 
* INPUTS      : pIf6          -   Pointer to the interface 
*               u1_nd6c_flag   -   Flag indicating whether all
*                                  or only the dynamic entries
*                                  are to be deleted
* 
* OUTPUTS     : None
* 
* RETURNS     : None
*****************************************************************************/

VOID
Nd6PurgeCacheOnIface (tIp6If * pIf6, UINT2 u2Nd6cFlag)
{
    tNd6CacheEntry     *pNd6cEntry = NULL;
    tNd6CacheEntry     *pNd6cEntryTmp = NULL;
    tNd6CacheEntry      Nd6cEntry;

    if (!pIf6)
    {

        return;
    }

    MEMSET (&Nd6cEntry, 0, sizeof (tNd6CacheEntry));
    Nd6cEntry.pIf6 = pIf6;
    pNd6cEntry = RBTreeGetNext (gNd6GblInfo.Nd6CacheTable,
                                (tRBElem *) & Nd6cEntry, NULL);

    while (pNd6cEntry != NULL)
    {
        pNd6cEntryTmp = RBTreeGetNext (gNd6GblInfo.Nd6CacheTable,
                                       pNd6cEntry, NULL);

        if (pNd6cEntry->pIf6 == pIf6)
        {
            /*
             * Purge all or dynamic NDCache entries on an interface
             * based on the u1_nd6c_flag indication
             */
            if ((u2Nd6cFlag == ND6C_PURGE_ALL) ||
                ((u2Nd6cFlag == ND6C_PURGE_DYNAMIC) &&
                 (pNd6cEntry->u1ReachState != ND6C_STATIC) &&
                 (pNd6cEntry->u1ReachState != ND6C_STATIC_NOT_IN_SERVICE)))
            {
                if (Nd6PurgeCache (pNd6cEntry) == IP6_FAILURE)
                {
                    IP6_TRC_ARG1 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                                  MGMT_TRC | ALL_FAILURE_TRC, ND6_NAME,
                                  "ND6:Nd6PurgeCacheOnIface: Purging Cache "
                                  "on IF %d Failed\n", pIf6->u4Index);
                }

            }

        }
        else
        {
            /* All the cache entries learnt on this interface have been verified
             * moving to the cache entries learnt on the next interface
             * Hence break the loop */
            break;
        }
        pNd6cEntry = pNd6cEntryTmp;
    }
}

/*****************************************************************************
* DESCRIPTION : Modifies a dynamic entry in the NDCache table.
* 
* INPUTS      : pIf6        -   Pointer to the interface 
*               pAddr6      -   Pointer to IPv6 address
*               pLladdr     -   Pointer to link-layer address
*               u1LlaLen   -   Length of the link-layer address
*               u4AdvFlag  -   Flag - Lower byte - indicating ND msg type
*                                       Higher  - Sol/Override status for NA
*               pIp6       - pointer to the IPv6 Header
*
* OUTPUTS     : None
* 
* RETURNS     : IP6_SUCCESS      -  NDCache Updation successful
*               IP6_FAILURE  -  NDCache Updation fails
*****************************************************************************/

INT4
Nd6UpdateCache (tIp6If * pIf6, tIp6Addr * pAddr6, UINT1 *pLladdr,
                UINT1 u1LlaLen, UINT4 u4AdvFlag, tIp6Hdr * pIp6,
                UINT1 u1SecureFlag)
{
    tNd6CacheEntry     *pNd6cEntry = NULL;
    tNd6RtEntry        *pNd6RtEntry = NULL;
    tTMO_SLL_NODE      *pNode = NULL;
    UINT1               u1Nd6Type = (UINT1) u4AdvFlag;
    UINT1               u1RtEntryCount = 0;
    UINT1               u1Cntr = 0;

    /* ND cache entries can exist for either tunnel or ethernet interface */

    if ((pIf6->u1IfType != IP6_ENET_INTERFACE_TYPE) &&
        (pIf6->u1IfType != IP6_PSEUDO_WIRE_INTERFACE_TYPE) &&
        (pIf6->u1IfType != IP6_L3VLAN_INTERFACE_TYPE) &&
        (pIf6->u1IfType != IP6_LAGG_INTERFACE_TYPE) &&
        (pIf6->u1IfType != IP6_X25_INTERFACE_TYPE) &&
        (pIf6->u1IfType != IP6_L3SUB_INTF_TYPE) &&
#ifdef TUNNEL_WANTED
        (pIf6->u1IfType != IP6_TUNNEL_INTERFACE_TYPE) &&
#endif
#ifdef WGS_WANTED
        (pIf6->u1IfType != IP6_L2VLAN_INTERFACE_TYPE) &&
#endif
        (pIf6->u1IfType != IP6_FR_INTERFACE_TYPE))
    {
        return (IP6_FAILURE);
    }

    /* Check whether an entry already exists */
    pNd6cEntry = Nd6IsCacheForAddr (pIf6, pAddr6);

    /* For SeND */
    if (IP6_TRUE == IF_SEND_ENABLED (pIf6))
    {
        if (NULL != pNd6cEntry)
        {
            /* Cache Entry Exists */

            /* If cache is secure and incoming message is unsecure */
            if ((ND6_SECURE_ENTRY == pNd6cEntry->u1SecureFlag)
                && (ND6_UNSECURE_ENTRY == u1SecureFlag))
            {
                return IP6_FAILURE;
            }
            else if ((pNd6cEntry->u1SecureFlag == ND6_UNSECURE_ENTRY) &&
                     (u1SecureFlag == ND6_SECURE_ENTRY))
            {
                pNd6cEntry->u1SecureFlag = ND6_SECURE_ENTRY;
            }
        }
    }

    switch (pIf6->u1IfType)
    {
            /*
             * Incase of NBMA interfaces, create a NDCache entry if it 
             * doesn't exist already
             */
        case IP6_X25_INTERFACE_TYPE:
        case IP6_FR_INTERFACE_TYPE:

            /*
             * Create an entry with state as REACHABLE if it doesn't 
             * exist already
             */
            if (pNd6cEntry == NULL)
            {
                if ((pNd6cEntry = Nd6CreateCache (pIf6, pAddr6, pLladdr,
                                                  u1LlaLen, ND6C_REACHABLE,
                                                  0)) == NULL)
                {
                    return (IP6_FAILURE);
                }
            }

            break;

            /*
             * Incase of LAN interfaces, create a NDCache entry if it 
             * doesn't exist already, Otherwise update the entry based on 
             * the type of ND message
             */

            /* ND cache entries can exist for either tunnel or ethernet interface */

        case IP6_ENET_INTERFACE_TYPE:
        case IP6_L3VLAN_INTERFACE_TYPE:
        case IP6_LAGG_INTERFACE_TYPE:
        case IP6_PSEUDO_WIRE_INTERFACE_TYPE:
        case IP6_L3SUB_INTF_TYPE:
#ifdef WGS_WANTED
        case IP6_L2VLAN_INTERFACE_TYPE:
#endif
#ifdef TUNNEL_WANTED
        case IP6_TUNNEL_INTERFACE_TYPE:
#endif

            if ((pNd6cEntry != NULL) &&
                ((pNd6cEntry->u1ReachState == ND6C_STATIC) ||
                 (pNd6cEntry->u1ReachState == ND6C_STATIC_NOT_IN_SERVICE)))
            {
                IP6_TRC_ARG1 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                              ALL_FAILURE_TRC, ND6_NAME,
                              "ND6:Nd6UpdateCache: Updation of Cache Failed - "
                              "%p is STATIC\n", pNd6cEntry);
                return (IP6_SUCCESS);
            }

            /*
             * Create an entry with state as STALE if it doesn't exist 
             * already, Otherwise update the existing entry
             */
            if (pNd6cEntry == NULL)
            {
                /*
                 * silently discards a Neighbor Advertisement
                 * if the entry does not have a Neighbor Cache
                 */

                if ((pNd6cEntry = Nd6CreateCache (pIf6, pAddr6,
                                                  pLladdr, u1LlaLen,
                                                  ND6C_STALE, 0)) == NULL)
                {
                    return (IP6_FAILURE);
                }
            }
            else if ((pNd6cEntry->u1AddrType == ADDR6_ANYCAST) &&
                     (pNd6cEntry->u1ReachState == ND6C_INCOMPLETE))
            {
                Nd6UpdateAnycastCacheForEnet (pNd6cEntry, pLladdr, u1LlaLen,
                                              u4AdvFlag, pIp6);
            }
            else
            {
                u1RtEntryCount = (UINT1) TMO_SLL_Count (&Nd6RouteList);
                for (u1Cntr = 0; u1Cntr < u1RtEntryCount; u1Cntr++)
                {
                    pNode = TMO_SLL_Next (&Nd6RouteList, pNd6RtEntry);
                    if (pNode == NULL)
                    {
                        break;
                    }
                    pNd6RtEntry = (tNd6RtEntry *) (VOID *) ((UINT1 *) (pNode) -
                                                            IP6_OFFSET
                                                            (tNd6RtEntry,
                                                             pNext));
                    if (pNd6RtEntry != NULL)
                    {
                        if ((MEMCMP
                             (pAddr6, &pNd6RtEntry->NextHop,
                              sizeof (tIp6Addr)) == 0)
                            && ((u4AdvFlag & ND6_DEFAULT_ROUTER) == 0)
                            && (u1Nd6Type == ND6_NEIGHBOR_ADVERTISEMENT))
                        {
                            Ip6UpdateRoutingDataBase (pNd6cEntry);
                            return (IP6_SUCCESS);
                        }
                    }
                }
                Nd6UpdateCacheForEnet (pNd6cEntry, pLladdr, u1LlaLen,
                                       u4AdvFlag);
            }
            break;

        default:
            return (IP6_FAILURE);

    }
    UNUSED_PARAM (pNd6cEntry);
    return (IP6_SUCCESS);

}

/* 
 *  
 */
/*****************************************************************************
* DESCRIPTION : Update the NDCache entry on Enet interface based on the
*               received ND message
* INPUTS      : pNd6cEntry  -   Pointer to the Nd cache entry.
*               pLladdr     -   Pointer to link-layer address
*               u1LlaLen   -   Length of the link-layer address
*               u4AdvFlag  -   Flag - Lower byte - indicating ND msg type
*                                       Higher  - Sol/Override status for NA
*
* OUTPUTS     : None
* 
* RETURNS     : IP6_SUCCESS      -  NDCache Updation successful
*               IP6_FAILURE  -  NDCache Updation fails
*****************************************************************************/

VOID
Nd6UpdateCacheForEnet (tNd6CacheEntry * pNd6cEntry, UINT1 *pLladdr,
                       UINT1 u1LlaLen, UINT4 u4AdvFlag)
{
    tIp6Cxt            *pIp6Cxt = NULL;
    UINT4               u4PdelayTime = 0;
    UINT1               u1EntryFlag = ND6C_NO_ENTRY_UPDATE;
    UINT1               u1LlaFlag = ND6C_NO_LLA_UPDATE;
    UINT1               u1OldState = pNd6cEntry->u1ReachState, u1Nd6Type = 0;
    UINT1               u1NewState = ND6C_INCOMPLETE;
    UINT1               u1SolNAFlag = ND6C_NO_ENTRY_UPDATE;
    UINT4               u4IfReachTime = REACHABLE_TIME;
    BOOL1               b1UpdateFlag = FALSE;

    tNd6CacheLanInfo   *pNd6cLinfo = NULL;
    pNd6cLinfo = (tNd6CacheLanInfo *) (VOID *) pNd6cEntry->pNd6cInfo;
    u1Nd6Type = (UINT1) u4AdvFlag;
    pNd6cEntry->u1LlaFlag = FALSE;
    pIp6Cxt = pNd6cEntry->pIf6->pIp6Cxt;
    IP6_TRC_ARG4 (pIp6Cxt->u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC, ND6_NAME,
                  "ND6:Nd6UpdateCacheForEnet: Cache %p on ENET - ND Message %d "
                  "LLA Len%d LLA= %s\n", pNd6cEntry, u1Nd6Type, u1LlaLen,
                  Ip6PrintIftok (pLladdr, u1LlaLen));

    u1LlaLen = (UINT1) MEM_MAX_BYTES (u1LlaLen, IP6_MAX_LLA_LEN);

    switch (u1Nd6Type)
    {
        case ND6_ROUTER_SOLICITATION:
        case ND6_NEIGHBOR_SOLICITATION:
        case ND6_REDIRECT:
            if (pLladdr == NULL)
            {
                if (pNd6cEntry->u1ReachState != ND6C_INCOMPLETE)
                {
                    u1NewState = ND6C_INCOMPLETE;
                }
                u1EntryFlag = ND6C_ENTRY_UPDATE;
                break;
            }
            else if ((u1OldState != ND6C_INCOMPLETE) &&
                     (pNd6cLinfo->u1LlaLen == u1LlaLen) &&
                     ((MEMCMP (pNd6cLinfo->lladdr, pLladdr, u1LlaLen)) == 0))
            {
                break;
            }
            u1NewState = ND6C_STALE;
            u1EntryFlag = ND6C_ENTRY_UPDATE;
            u1LlaFlag = ND6C_LLA_UPDATE;
            break;

        case ND6_NEIGHBOR_ADVERTISEMENT:

            IP6_TRC_ARG1 (pIp6Cxt->u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC,
                          ND6_NAME,
                          "ND6:Nd6UpdateCacheForEnet: NA Adv Status %X\n",
                          u4AdvFlag);
            if (pNd6cEntry->u1ReachState != ND6C_INCOMPLETE)
            {
                if ((!(u4AdvFlag & ND6_SOLICITED_FLAG))
                    && (!(u4AdvFlag & ND6_OVERRIDE_FLAG)))
                {
                    break;
                }
                if (!(u4AdvFlag & ND6_OVERRIDE_FLAG) &&
                    ((pLladdr != NULL) &&
                     (MEMCMP (pNd6cLinfo->lladdr, pLladdr, u1LlaLen) != 0)))
                {
                    if (pNd6cEntry->u1ReachState == ND6C_REACHABLE)
                    {
                        u1NewState = ND6C_STALE;
                        u1EntryFlag = ND6C_ENTRY_UPDATE;
                        break;
                    }
                    else
                        break;
                }
                else if ((u4AdvFlag & ND6_OVERRIDE_FLAG) ||
                         (!(u4AdvFlag & ND6_OVERRIDE_FLAG) &&
                          ((pLladdr != NULL) &&
                           (MEMCMP (pNd6cLinfo->lladdr, pLladdr, u1LlaLen) ==
                            0))) || (pLladdr == NULL))
                {
                    /* if linklayer address is different, update the cache */
                    if ((pLladdr != NULL) &&
                        (MEMCMP (pNd6cLinfo->lladdr, pLladdr, u1LlaLen) != 0))
                    {
                        u1LlaFlag = ND6C_LLA_UPDATE;
                    }
                    if (u4AdvFlag & ND6_SOLICITED_FLAG)
                    {
                        /* for the case of rSo NA w/o TLL */
                        u1EntryFlag = ND6C_ENTRY_UPDATE;
                        u1NewState = ND6C_REACHABLE;
                        u1SolNAFlag = ND6C_ENTRY_UPDATE;
                        break;
                    }
                    else if ((u1LlaFlag == ND6C_LLA_UPDATE) &&
                             !(u4AdvFlag & ND6_SOLICITED_FLAG))
                    {
                        u1EntryFlag = ND6C_ENTRY_UPDATE;
                        u1NewState = ND6C_STALE;
                    }
                    break;
                }
            }
            else                /* if state is INCOMPLETE */
            {
                if (pLladdr == NULL)
                {
                    if (pNd6cEntry->pIf6->u1IfType == IP6_TUNNEL_INTERFACE_TYPE)
                    {
                        u1EntryFlag = ND6C_ENTRY_UPDATE;
                        if (u4AdvFlag & ND6_SOLICITED_FLAG)
                        {
                            u1NewState = ND6C_REACHABLE;
                            u1SolNAFlag = ND6C_ENTRY_UPDATE;
                        }
                        else
                        {
                            u1NewState = ND6C_STALE;
                        }
                    }
                    break;
                }
                else
                {
                    u1EntryFlag = ND6C_ENTRY_UPDATE;
                    u1LlaFlag = ND6C_LLA_UPDATE;
                    if (u4AdvFlag & ND6_SOLICITED_FLAG)
                    {
                        u1NewState = ND6C_REACHABLE;
                        u1SolNAFlag = ND6C_ENTRY_UPDATE;
                    }
                    else
                    {
                        u1NewState = ND6C_STALE;
                    }
                }
            }
            break;
        default:
            break;

    }

    /*
     * Upon the u1EntryFlag being set to Update the Cache, record
     * the link layer address and send queued packets if any on the
     * entry that was previously in INCOMPLETE state
     */
    if (u1EntryFlag == ND6C_ENTRY_UPDATE)
    {
        if ((u1SolNAFlag == ND6C_ENTRY_UPDATE) ||
            (pNd6cEntry->u1ReachState != ND6C_REACHABLE))
        {
            /* Cancel already running timer if any on the entry */
            Nd6CancelCacheTimer (pNd6cEntry);
        }

        if (u1LlaFlag == ND6C_LLA_UPDATE)
        {
            MEMCPY (pNd6cLinfo->lladdr, pLladdr, u1LlaLen);
            pNd6cLinfo->u1LlaLen = u1LlaLen;
            pNd6cEntry->u1LlaFlag = TRUE;
        }

        /* Update the ND6 Entry Reachability State. */
        Nd6SetReachState (pNd6cEntry, u1NewState);

        /* Send the pending IPv6 pkts queued on the entry that was
         * previously in INCOMPLETE state */
        if ((u1OldState == ND6C_INCOMPLETE) ||
            ((u1OldState != ND6C_INCOMPLETE) &&
             (pNd6cEntry->u1ReachState == ND6C_INCOMPLETE)))
        {
            Nd6SendPendQue (pNd6cEntry);
            Nd6UpdateLastUseTime (pNd6cEntry);
        }

        /* Start the Reachability timer for the entry in Reachable state */
        if ((u1SolNAFlag == ND6C_ENTRY_UPDATE)
            && (pNd6cEntry->u1ReachState == ND6C_REACHABLE))
        {
            u4IfReachTime = Ip6GetIfReachTime (pNd6cEntry->pIf6->u4Index);
            u4IfReachTime =
                Ip6Random ((UINT4) (u4IfReachTime * 0.5),
                           (UINT4) (u4IfReachTime * 1.5));
            Nd6SetMSecCacheTimer (pNd6cEntry, ND6_REACH_TIMER_ID,
                                  u4IfReachTime);
        }
        /*According to RFC 4861 page 89 if the state of the neighbor entry is not INCOMPLETE and an
         * NA is received with Solicited = 0   Override = 1  New MAC address
         * then we should change the mac address of the neighbor entry from the NA pkt and
         * change the state of the entry to STALE*/
        if (((u1Nd6Type == ND6_NEIGHBOR_ADVERTISEMENT) &&
             (u4AdvFlag & ND6_OVERRIDE_FLAG) &&
             (!(u4AdvFlag & ND6_SOLICITED_FLAG)) &&
             (u1LlaFlag == ND6C_LLA_UPDATE)))
        {
            b1UpdateFlag = TRUE;
        }
        if ((u1Nd6Type == ND6_NEIGHBOR_ADVERTISEMENT) &&
            (u4AdvFlag & ND6_SOLICITED_FLAG) &&
            (!(u4AdvFlag & ND6_OVERRIDE_FLAG)) &&
            (u1OldState == ND6C_REACHABLE))
        {
            b1UpdateFlag = TRUE;
        }
        if ((u1Nd6Type != ND6_NEIGHBOR_ADVERTISEMENT) &&
            (u1LlaFlag == ND6C_LLA_UPDATE) && (u1OldState != ND6C_INCOMPLETE))
        {
            b1UpdateFlag = TRUE;
        }

        if (b1UpdateFlag == FALSE)
        {
            if (pNd6cEntry->u1ReachState == ND6C_STALE)
            {
                if ((u4PdelayTime = IP6_IF_PDELAYTIME (pNd6cEntry->pIf6)))
                {
                    Nd6SetReachState (pNd6cEntry, ND6C_DELAY);
                    Nd6SetCacheTimer (pNd6cEntry, ND6_DELAY_PROBE_TIMER_ID,
                                      u4PdelayTime);
                }
                else
                {
                    /* Probe immediately as delay timer is zero */
                    Nd6SetReachState (pNd6cEntry, ND6C_PROBE);
                    Nd6ProbeOnCache (pNd6cEntry);
                }
            }
        }
        /* Update the NDCache last updated time for this entry */
        OsixGetSysTime ((tOsixSysTime *) & (pNd6cEntry->u4LastUpdatedTime));
    }
    return;
}

/*****************************************************************************
* DESCRIPTION : Update the NDCache entry on Enet interface based on the
*               received ND message for anycast address
* INPUTS      : pNd6cEntry  -   Pointer to the Nd cache entry.
*               pLladdr     -   Pointer to link-layer address
*               u1LlaLen   -   Length of the link-layer address
*               u4AdvFlag  -   Flag - Lower byte - indicating ND msg type
*                                       Higher  - Sol/Override status for NA
*
* OUTPUTS     : None
* 
* RETURNS     : None
*               
*****************************************************************************/
VOID
Nd6UpdateAnycastCacheForEnet (tNd6CacheEntry * pNd6cEntry, UINT1 *pLladdr,
                              UINT1 u1LlaLen, UINT4 u4AdvFlag, tIp6Hdr * pIp6)
{
    tIp6Cxt            *pIp6Cxt = NULL;
    tAnycastNDCache    *pAnycastDest = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    UINT1               u1Nd6Type = 0;

    u1Nd6Type = (UINT1) u4AdvFlag;

    pIp6Cxt = pNd6cEntry->pIf6->pIp6Cxt;
    u1LlaLen = (UINT1) MEM_MAX_BYTES (u1LlaLen, IP6_MAX_LLA_LEN);

    if ((u1Nd6Type != ND6_NEIGHBOR_ADVERTISEMENT) || (pLladdr == NULL))
    {
        return;
    }

    if (pNd6cEntry->u1ReachState == ND6C_INCOMPLETE)
    {
        TMO_SLL_Scan (&(pNd6cEntry->anycastDestAddrList),
                      pLstNode, tTMO_SLL_NODE *)
        {
            pAnycastDest = (tAnycastNDCache *) pLstNode;

            if (MEMCMP (pAnycastDest->lladdr, pLladdr, u1LlaLen) == 0)
            {
                return;
            }
        }
        pAnycastDest =
            (tAnycastNDCache *) (VOID *) Ip6GetMem (pIp6Cxt->u4ContextId,
                                                    gIp6GblInfo.i4AnycastDstId);
        if (pAnycastDest == NULL)
        {
            IP6_TRC_ARG (pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                         BUFFER_TRC | ALL_FAILURE_TRC, ND6_NAME,
                         "Nd6UpdateAnycastCacheForEnet: Memory Allocation for "
                         "Anycast list FAILED\n");
            return;
        }
        Ip6AddrCopy (&(pAnycastDest->sourceAddr6), &(pIp6->srcAddr));
        MEMCPY (pAnycastDest->lladdr, pLladdr, u1LlaLen);
        pAnycastDest->u4IfIndex = pNd6cEntry->pIf6->u4Index;
        TMO_SLL_Add (&(pNd6cEntry->anycastDestAddrList),
                     &(pAnycastDest->nextAnycastNDCache));
    }
    return;
}

/*****************************************************************************
* DESCRIPTION : Searches for a NDCache entry matching
*               the IPv6 destination address during packet
*               forwarding and if found updates the entry
*  
* INPUTS      : pIf6       -   Pointer to the interface 
*               pAddr6     -   Pointer to IPv6 address for which 
*                               the NDCache table is searched
* 
* OUTPUTS     : None
* 
* RETURNS     : NULL        -   No NDCache entry exists
*               Otherwise,      Pointer to the matching 
*                               NDCache Entry
*****************************************************************************/

tNd6CacheEntry     *
Nd6LookupCache (tIp6If * pIf6, tIp6Addr * pAddr6)
{
    UINT4               u4PdelayTime = 0;
    tNd6CacheEntry     *pNd6cEntry = NULL;

    if (Ip6AddrType (pAddr6) == ADDR6_MULTI)
    {
        IP6_TRC_ARG1 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC, ALL_FAILURE_TRC,
                      ND6_NAME,
                      "ND6:Nd6LookupCache: Cache Lookup Failed - Addr is "
                      "Multicast : %s\n", Ip6PrintAddr (pAddr6));
        return (NULL);
    }

    IP6_TRC_ARG2 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC,
                  ND6_NAME,
                  "ND6:Nd6LookupCache: Cache Lookup on IF %d for Addr : %s\n",
                  pIf6->u4Index, Ip6PrintAddr (pAddr6));

    if ((pNd6cEntry = Nd6IsCacheForAddr (pIf6, pAddr6)) == NULL)
    {
        return (NULL);
    }

    IP6_TRC_ARG1 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC,
                  ND6_NAME,
                  "ND6:Nd6LookupCache: Cache Lookup - Matching entry %p\n",
                  pNd6cEntry);

    /* Update the NDCache entry */
    switch (pNd6cEntry->u1ReachState)
    {
        case ND6C_STALE:
            if ((u4PdelayTime = IP6_IF_PDELAYTIME (pIf6)))
            {
                Nd6SetReachState (pNd6cEntry, ND6C_DELAY);
                Nd6SetCacheTimer (pNd6cEntry,
                                  ND6_DELAY_PROBE_TIMER_ID, u4PdelayTime);
            }
            else
            {
                /* Probe immediately as delay timer is zero */
                Nd6SetReachState (pNd6cEntry, ND6C_PROBE);
                Nd6ProbeOnCache (pNd6cEntry);
            }
            /* Fall thru' */

        case ND6C_REACHABLE:
        case ND6C_DELAY:
        case ND6C_PROBE:
        case ND6C_STATIC:
            Nd6UpdateLastUseTime (pNd6cEntry);
            break;

        case ND6C_INCOMPLETE:
            break;
        case ND6C_STATIC_NOT_IN_SERVICE:
            break;

        default:
            break;

    }

    return (pNd6cEntry);

}

/*****************************************************************************
DESCRIPTION           Performs link-layer Address Resolution and
                      creates an entry if it is not already
                      present, Queues the IPv6 packet into
                      the entry

INPUTS                pNd6cEntry   -   Pointer to the NDCache Entry
                      pIf6          -   Pointer to the interface 
                      pAddr6        -   Pointer to Destination IPv6 address
                      pBuf          -   Pointer to the IPv6 packet
                                         to be sent
                      u1RouteAddrType - Indicates whether the destination address
                        in the route entry for anycast

OUTPUTS               None

RETURNS               IP6_SUCCESS      -  Packet queued on NDCache entry
                      IP6_FAILURE  -  NDCache create fails
*****************************************************************************/

VOID
Nd6Resolve (tNd6CacheEntry * pNd6cEntry, tIp6If * pIf6, tIp6Addr * pAddr6,
            tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 u1RouteAddrType)
{
    tIp6Addr           *pSrcAddr = NULL, SrcAddr;
    tIp6Addr           *pIp6TempAddr = NULL, Ip6TempAddr;
    UINT4               u4Offset = 0;
    UINT4               u4ContextId = pIf6->pIp6Cxt->u4ContextId;
    UINT1               u1Type = 0;
    UINT1               u1LoopBackAddrFlag = FALSE;
    tIp6AddrInfo       *pAddr6Info = NULL;
    tIp6AddrInfo        Ip6AddrInfo;
    tIp6If              TempIf6;

    MEMSET (&Ip6AddrInfo, 0, sizeof (tIp6AddrInfo));
    MEMSET (&Ip6TempAddr, 0, sizeof (tIp6Addr));
    MEMSET (&TempIf6, 0, sizeof (tIp6If));
    Ip6AddrInfo.pIf6 = &TempIf6;
    TempIf6.pIp6Cxt = pIf6->pIp6Cxt;
    pIp6TempAddr = &Ip6TempAddr;

    if (!(pNd6cEntry))
    {
        /* Check whether Neighbor Cache Exists for the given destination. 
         * If present, then use it. Else, Create a new entry. 
         * NOTE: Currently this routine is called whenever a Default
         * router is first installed or when pkt needs to be send out.
         * So it is mandatory to check for presence of Cache even if input
         * value is NULL. If later, ND Resolve is removed while adding the
         * default route, then remove this check also and always create a
         * entry. */
        pNd6cEntry = Nd6IsCacheForAddr (pIf6, pAddr6);

        if (pNd6cEntry == NULL)
        {
            /* Entry does not exist. Create New Entry and send NS */
            if ((pNd6cEntry = Nd6CreateCache (pIf6, pAddr6, NULL, 0,
                                              ND6C_INCOMPLETE, 0)) == NULL)
            {
                IP6_TRC_ARG (u4ContextId, ND6_MOD_TRC,
                             MGMT_TRC | ALL_FAILURE_TRC, ND6_NAME,
                             "ND6:Nd6Resolve: Resolve - Creation Failed\n");
                if (pBuf != NULL)
                {
                    Ip6BufRelease (u4ContextId, pBuf, FALSE, ND6_MODULE);
                }
                return;
            }
            pNd6cEntry->u1AddrType = u1RouteAddrType;

            TMO_SLL_Init (&(pNd6cEntry->anycastDestAddrList));

            /*
             * For the combination of globla --> linklocal, outgoing NS should
             * be linklocal --> global. Pass the linklocal address
             * here, so that source addr of the NS will be linklocal
             */
            /*
             * get the source address of the packet.
             * Based on this, the outgoing address
             * will be decided for the NS.
             */
            if (pBuf != NULL)
            {
                u4Offset = IP6_BUF_READ_OFFSET (pBuf);
                pSrcAddr = Ip6BufRead (pBuf, (UINT1 *) &SrcAddr,
                                       IP6_OFFSET_FOR_SRCADDR_FIELD,
                                       sizeof (tIp6Addr), 1);
                IP6_BUF_READ_OFFSET (pBuf) = u4Offset;

                /* 
                 * In case the pBuf is a packet which we should 
                 * forward(Ip6IsPktToMeInCxt fails), then we should 
                 * not put the source address from that pointer, 
                 * instead we should fetch our own address and 
                 * put it in the source address field of the NS 
                 * message.
                 */
                if (Ip6IsPktToMeInCxt (pIf6->pIp6Cxt, &u1Type, ADDR6_LLOCAL,
                                       pSrcAddr, pIf6) == IP6_FAILURE)
                {
                    pSrcAddr = Ip6GetLlocalAddr (pIf6->u4Index);
                }
            }
            else
            {
                /* Neighbor Resolve request is originated for this node.
                 * Get the Link-Local Address. */
                pSrcAddr = Ip6GetLlocalAddr (pIf6->u4Index);
            }

            if (pSrcAddr == NULL)
            {
                IP6_TRC_ARG (u4ContextId, ND6_MOD_TRC,
                             MGMT_TRC | ALL_FAILURE_TRC, ND6_NAME,
                             "ND6: Nd6Resolve: Resolve - NS Send Failed\n");
                Nd6PurgeCache (pNd6cEntry);

                if (pBuf != NULL)
                {
                    Ip6BufRelease (u4ContextId, pBuf, FALSE, ND6_MODULE);
                }
                return;
            }
            /* RM#23119 NS uses loopback address as source address */

            /* 
             * Get the address structure and check whether the interface type is a 
             * loopback interface type, if it is a loopback interface address, then
             * get a suitable unicast address for NS, don't use loopback address.
             */
            Ip6AddrCopy (&Ip6AddrInfo.ip6Addr, pSrcAddr);
            pAddr6Info = RBTreeGet (gIp6GblInfo.UnicastAddrTree,
                                    (tRBElem *) & Ip6AddrInfo);

            if (pAddr6Info == NULL)
            {
                pAddr6Info = RBTreeGetNext (gIp6GblInfo.UnicastAddrTree,
                                            (tRBElem *) & Ip6AddrInfo, NULL);
            }

            while (pAddr6Info != NULL)
            {
                if (Ip6AddrMatch (&pAddr6Info->ip6Addr, pSrcAddr,
                                  IP6_ADDR_SIZE_IN_BITS) == TRUE)
                {
                    if (pAddr6Info->pIf6->u1IfType ==
                        IP6_LOOPBACK_INTERFACE_TYPE)
                    {
                        u1LoopBackAddrFlag = TRUE;
                        break;
                    }
                }
                pAddr6Info = RBTreeGetNext (gIp6GblInfo.UnicastAddrTree,
                                            pAddr6Info, NULL);
            }

            if (u1LoopBackAddrFlag != TRUE)
            {
                /*
                 * Copy the recent source address in the cache entry
                 * for further usage
                 */
                Ip6AddrCopy (&pNd6cEntry->recentSrcAddr, pSrcAddr);
            }
            else
            {
                /*
                 * Get a valid unicast address and use it is a
                 * source address for NS, if the actual source address 
                 * is a loopback address.
                 */
                Ip6AddrCopy (pIp6TempAddr, pSrcAddr);
                pSrcAddr = NULL;
                pSrcAddr = Ip6AddrGetSrc (pIf6, pIp6TempAddr);
                if (pSrcAddr == NULL)
                {
                    IP6_TRC_ARG (u4ContextId, ND6_MOD_TRC,
                                 MGMT_TRC | ALL_FAILURE_TRC, ND6_NAME,
                                 "ND6: Nd6Resolve: Resolve - NS Send Failed\n");
                    Nd6PurgeCache (pNd6cEntry);

                    if (pBuf != NULL)
                    {
                        Ip6BufRelease (u4ContextId, pBuf, FALSE, ND6_MODULE);
                    }
                    return;
                }
                Ip6AddrCopy (&pNd6cEntry->recentSrcAddr, pSrcAddr);
            }

            if (Nd6CheckAndSendNeighSol (pNd6cEntry, pSrcAddr) == IP6_FAILURE)
            {
                IP6_TRC_ARG (u4ContextId, ND6_MOD_TRC,
                             MGMT_TRC | ALL_FAILURE_TRC, ND6_NAME,
                             "ND6: Nd6Resolve: Resolve - NS Send Failed\n");
                Nd6PurgeCache (pNd6cEntry);

                if (pBuf != NULL)
                {
                    Ip6BufRelease (u4ContextId, pBuf, FALSE, ND6_MODULE);
                }
                return;
            }
        }
    }

    if ((pBuf != NULL) && ((Nd6QueueToCache (pNd6cEntry, pBuf) == IP6_FAILURE)))
    {
        IP6_TRC_ARG (u4ContextId, ND6_MOD_TRC, MGMT_TRC | ALL_FAILURE_TRC,
                     ND6_NAME, "ND6:Nd6Resolve:  Resolve - Queueing Failed\n");
        if (Ip6BufRelease (u4ContextId, pBuf, FALSE, ND6_MODULE) != IP6_SUCCESS)
        {
            IP6_TRC_ARG (u4ContextId, ND6_MOD_TRC, BUFFER_TRC | ALL_FAILURE_TRC,
                         ND6_NAME,
                         "ND6:Nd6Resolve:  Resolve - BufRelease Failed\n");
            IP6_TRC_ARG3 (u4ContextId, ND6_MOD_TRC, BUFFER_TRC, IP6_NAME,
                          "ND6:Nd6Resolve: %s buffer error occurred, Type = %d "
                          "Bufptr = %p ", ERROR_FATAL_STR, ERR_BUF_RELEASE,
                          pBuf);
        }
    }

}

/*****************************************************************************
DESCRIPTION           Checks for whether a NDCache entry exists for an IPv6 
                      address and if present returns the entry, Otherwise NULL
 

INPUTS                pIf6          -   Pointer to the interface 
                      pAddr6        -   Pointer to Destination IPv6 address

OUTPUTS               None

RETURNS               IP6_SUCCESS      -  Packet queued on NDCache entry
                      IP6_FAILURE  -  NDCache create fails
*****************************************************************************/
tNd6CacheEntry     *
Nd6IsCacheForAddr (tIp6If * pIf6, tIp6Addr * pAddr6)
{
    tNd6CacheEntry     *pNd6cEntry = NULL;
    tNd6CacheEntry      Nd6cEntry;

    MEMSET (&Nd6cEntry, 0, sizeof (tNd6CacheEntry));
    Nd6cEntry.pIf6 = pIf6;
    Ip6AddrCopy (&(Nd6cEntry.addr6), pAddr6);

    IP6_TRC_ARG2 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC,
                  ND6_NAME,
                  "Nd6IsCacheForAddr: Obtaining the cache entry for the "
                  "interface %d and address %s\n", pIf6->u4Index,
                  Ip6PrintNtop (pAddr6));
    pNd6cEntry = RBTreeGet (gNd6GblInfo.Nd6CacheTable, (tRBElem *) & Nd6cEntry);

    return (pNd6cEntry);
}

/*****************************************************************************
DESCRIPTION           Checks for whether a NDCache entry is REACHABLE or not

INPUTS                pIf6          -   Pointer to the interface
                      pAddr6        -   Pointer to Destination IPv6 address

OUTPUTS               None

RETURNS               TRUE-  Addr is reachable
                      FALSE- Addr is not reachable
*****************************************************************************/

INT4
Nd6IsIpv6AddrCacheReachable (tIp6If * pIf6, tIp6Addr * pAddr6)
{

    tNd6CacheEntry     *pNd6cEntry = NULL;
    tNd6CacheEntry      Nd6cEntry;

    MEMSET (&Nd6cEntry, 0, sizeof (tNd6CacheEntry));
    Nd6cEntry.pIf6 = pIf6;
    Ip6AddrCopy (&(Nd6cEntry.addr6), pAddr6);

    IP6_TRC_ARG2 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC,
                  ND6_NAME,
                  "Nd6IsIpv6AddrCacheReachable: Obtaining the cache entry for the "
                  "interface %d and address %s\n", pIf6->u4Index,
                  Ip6PrintNtop (pAddr6));
    pNd6cEntry = RBTreeGet (gNd6GblInfo.Nd6CacheTable, (tRBElem *) & Nd6cEntry);

    if ((pNd6cEntry != NULL)
        && ((pNd6cEntry->u1ReachState == ND6_CACHE_ENTRY_REACHABLE)
            || (pNd6cEntry->u1ReachState == ND6_CACHE_ENTRY_STATIC)))
    {

        return TRUE;
    }

    return FALSE;

}

/*****************************************************************************
DESCRIPTION           Among the several responses received from Anycast 
                      destinations which are on-link, the best destination 
                      is selected.
 

INPUTS                pNd6cEntry    -   Pointer to the Nd6 cache entry

OUTPUTS               None

RETURNS               IP6_SUCCESS  
                      IP6_FAILURE 
*****************************************************************************/

INT4
Nd6SelectBestAnycastDestination (tNd6CacheEntry * pNd6cEntry)
{
    tAnycastNDCache    *pBestAnycastNDCache = NULL;
    tNd6CacheLanInfo   *pNd6cInfo = NULL;
    tIp6DstInfo         Ip6DstInfo[MAX_IP6_ANYCAST_DST];
    tIp6DstInfo        *pIp6DstInfo[MAX_IP6_ANYCAST_DST] = { NULL };
    tTMO_SLL_NODE      *pLstNode = NULL;
    tAnycastNDCache    *pAnycastDest = NULL;
    UINT4               u4Index = 0;
    UINT4               u4Count = 0;

    u4Count = TMO_SLL_Count (&(pNd6cEntry->anycastDestAddrList));
    if (u4Count == 0)
    {
        return IP6_FAILURE;
    }

    /*apply the address selection policy to select the best destination */
    TMO_SLL_Scan (&(pNd6cEntry->anycastDestAddrList), pLstNode, tTMO_SLL_NODE *)
    {
        pAnycastDest = (tAnycastNDCache *) pLstNode;
        MEMSET (&(Ip6DstInfo[u4Index]), 0, sizeof (tIp6DstInfo));
        Ip6AddrCopy (&(Ip6DstInfo[u4Index].dstaddr),
                     &(pAnycastDest->sourceAddr6));
        Ip6DstInfo[u4Index].u4IfIndex = pAnycastDest->u4IfIndex;
        pIp6DstInfo[u4Index] = &(Ip6DstInfo[u4Index]);
        u4Index++;
    }
    IP6_TASK_UNLOCK ();
    NetIpv6SelectDestAddr (&(pIp6DstInfo[0]), (UINT1) u4Count);
    IP6_TASK_LOCK ();
    TMO_SLL_Scan (&(pNd6cEntry->anycastDestAddrList), pLstNode, tTMO_SLL_NODE *)
    {
        for (u4Index = 0; u4Index < u4Count; u4Index++)
        {
            pAnycastDest = (tAnycastNDCache *) pLstNode;
            if (MEMCMP
                (&(pAnycastDest->sourceAddr6), &(Ip6DstInfo[u4Index].dstaddr),
                 IP6_ADDR_SIZE) == 0)
            {

                pAnycastDest->u1NextHopPrefernce =
                    Ip6DstInfo[u4Index].u1CumulativeScore;
                break;
            }
        }
    }
    Ipv6SortDstAddrList (&(pNd6cEntry->anycastDestAddrList));

    pBestAnycastNDCache =
        (tAnycastNDCache *) TMO_SLL_First (&(pNd6cEntry->anycastDestAddrList));
    pNd6cInfo = (tNd6CacheLanInfo *) (VOID *) pNd6cEntry->pNd6cInfo;
    MEMCPY (pNd6cInfo->lladdr, pBestAnycastNDCache->lladdr, IP6_ENET_ADDR_LEN);
    Ip6AddrCopy (&(pNd6cEntry->anycastSrcAddr),
                 &(pBestAnycastNDCache->sourceAddr6));
    pNd6cInfo->u1LlaLen = IP6_ENET_ADDR_LEN;

    return IP6_SUCCESS;
}

/*****************************************************************************
* DESCRIPTION : Performs time-out actions specific
*               to NDCache entries.
* 
* INPUTS      : pNd6Cache    -  Pointer to the NDCache Entry
* 
* OUTPUTS     : None
* 
* RETURNS     : None
*****************************************************************************/

VOID
Nd6CacheTimeout (UINT1 u1TimerId, tNd6CacheEntry * pNd6cEntry)
{
    tIp6Addr           *pSrcAddr = NULL, srcAddr;
#ifdef NPAPI_WANTED
    UINT4               u4IfReachTime = 0;
#endif
    INT4                i4ReachNextHop = FALSE;

    if (!IS_ADDR_UNSPECIFIED (pNd6cEntry->recentSrcAddr))
    {
        pSrcAddr = &srcAddr;
        Ip6AddrCopy (pSrcAddr, &pNd6cEntry->recentSrcAddr);
    }

    switch (u1TimerId)
    {
        case ND6_RETRANS_TIMER_ID:
            /* Retransmit Timer Expiry */
            /* In case of Proxy cache, just purge it.
             * The source will only retransmit NS, on
             * which the cache shall be created again*/

            if (IP6_TRUE == pNd6cEntry->u1ProxyCache)
            {
                Nd6PurgeCache (pNd6cEntry);
                break;
            }
            else if (Nd6CheckAndSendNeighSol (pNd6cEntry, pSrcAddr) ==
                     IP6_FAILURE)
            {
                IP6_TRC_ARG1 (pNd6cEntry->pIf6->pIp6Cxt->u4ContextId,
                              ND6_MOD_TRC, DATA_PATH_TRC, ND6_NAME,
                              "ND6:Nd6CacheTimeout:  Retrans Timeout - NS Send "
                              "Failed on Cache %p \n", pNd6cEntry);
                Nd6PurgeCache (pNd6cEntry);
            }
            break;

        case ND6_REACH_TIMER_ID:
            /* Reachability Timer Expiry. Set the State as STALE if HitBit 
             * is not set, else inititate ND*/

            i4ReachNextHop = Rtm6GetReachableNextHopInCxt
                (pNd6cEntry->pIf6->pIp6Cxt->u4ContextId, &(pNd6cEntry->addr6));
#ifdef NPAPI_WANTED
            /* In case of Proxy cache,
             * don't probe the cache on reach tmr expiry. It shall
             * be done by the sending host and the NS/NA should 
             * be proxied */
            if ((Ipv6FsNpIpv6CheckHitOnNDCacheEntry
                 (pNd6cEntry->pIf6->pIp6Cxt->u4ContextId,
                  (UINT1 *) &(pNd6cEntry->addr6),
                  pNd6cEntry->pIf6->u4Index) == FNP_TRUE)
                || (i4ReachNextHop == TRUE))
            {

                if (pNd6cEntry->u1ProxyCache == IP6_TRUE)
                {
                    u4IfReachTime =
                        Ip6GetIfReachTime (pNd6cEntry->pIf6->u4Index);
                    u4IfReachTime =
                        Ip6Random ((u4IfReachTime * 0.5),
                                   (u4IfReachTime * 1.5));
                    Nd6SetMSecCacheTimer (pNd6cEntry, ND6_REACH_TIMER_ID,
                                          u4IfReachTime);
                }
                else
                {
                    Nd6SetReachState (pNd6cEntry, ND6C_PROBE);
                    Nd6ProbeOnCache (pNd6cEntry);
                }
                OsixGetSysTime ((tOsixSysTime *) &
                                (pNd6cEntry->u4LastUpdatedTime));

                IP6_TRC_ARG1 (pNd6cEntry->pIf6->pIp6Cxt->u4ContextId,
                              ND6_MOD_TRC, DATA_PATH_TRC, ND6_NAME,
                              "ND6:Nd6CacheTimeout:  Reach Timeout "
                              "on Cache - Neighbor IP %s. Probing Neighbor as Hit Bit is set.\n",
                              Ip6PrintAddr (&pNd6cEntry->addr6));
            }
            else
#endif
            {
                if (pNd6cEntry->u1ProxyCache == IP6_TRUE)
                {
                    Nd6PurgeCache (pNd6cEntry);
                }
                else
                {
                    if (i4ReachNextHop == FALSE)
                    {
                        Nd6SetReachState (pNd6cEntry, ND6C_STALE);
                    }
                    else
                    {
                        Nd6SetReachState (pNd6cEntry, ND6C_PROBE);
                        Nd6ProbeOnCache (pNd6cEntry);
                        OsixGetSysTime ((tOsixSysTime *) &
                                        (pNd6cEntry->u4LastUpdatedTime));
                    }
                }

                IP6_TRC_ARG1 (pNd6cEntry->pIf6->pIp6Cxt->u4ContextId,
                              ND6_MOD_TRC, DATA_PATH_TRC, ND6_NAME,
                              "ND6:Nd6CacheTimeout:  Reach Timeout  on Cache - Neighbor "
                              "IP %s. Entry is stale as HitBit is not set\n",
                              Ip6PrintAddr (&pNd6cEntry->addr6));
            }
            break;

        case ND6_DELAY_PROBE_TIMER_ID:
            /* Delay First Probe Timer Expiry */
            pNd6cEntry->u1StaleToProbe = TRUE;
            Nd6SetReachState (pNd6cEntry, ND6C_PROBE);
            Nd6ProbeOnCache (pNd6cEntry);
            break;

        default:
            break;

    }

}

/*****************************************************************************
* DESCRIPTION : This routine initialises the fields on NDCache entry before
*               Probing and sends NS message to verify the reachability
*               of the address on the entry
* 
* INPUTS      : pNd6Entry    -  Pointer to the NDCache Entry
* 
* OUTPUTS     : None
* 
* RETURNS     : None
*****************************************************************************/

VOID
Nd6ProbeOnCache (tNd6CacheEntry * pNd6cEntry)
{
    tNd6CacheLanInfo   *pNd6cLinfo = NULL;
    tIp6Addr           *pSrcAddr = NULL, srcAddr;
    if (!IS_ADDR_UNSPECIFIED (pNd6cEntry->recentSrcAddr))
    {
        pSrcAddr = &srcAddr;
        Ip6AddrCopy (pSrcAddr, &pNd6cEntry->recentSrcAddr);
    }

    pNd6cLinfo = (tNd6CacheLanInfo *) (VOID *) pNd6cEntry->pNd6cInfo;
    pNd6cLinfo->u4Retries = 0;

    if (Nd6CheckAndSendNeighSol (pNd6cEntry, pSrcAddr) == IP6_FAILURE)
    {
        IP6_TRC_ARG (pNd6cEntry->pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                     BUFFER_TRC, ND6_NAME,
                     "ND6:Nd6ProbeOnCache: Probe On Cache - NS Send Failed\n");
        Nd6PurgeCache (pNd6cEntry);
    }
}

/*****************************************************************************
DESCRIPTION           Enqueues the datagram to the pending
                      queue of the specific ND entry

INPUTS                pNd6cEntry  -  Pointer to NDCache Entry
                      pBuf         -  Pointer to the IP packet
                                       to be sent

OUTPUTS               None

RETURNS               OK      - Enqueuing is successful
                      NOT_OK  - Enqueuing fails
*****************************************************************************/

INT4
Nd6QueueToCache (tNd6CacheEntry * pNd6cEntry, tCRU_BUF_CHAIN_HEADER * pBuf)
{
    tIp6If             *pIf6 = pNd6cEntry->pIf6;
    tNd6CacheLanInfo   *pNd6cLinfo = NULL;
    tCRU_BUF_CHAIN_HEADER *pTempBuf = NULL;
    /* ND cache entries can exist for either tunnel or ethernet interface */

    if ((pIf6->u1IfType == IP6_ENET_INTERFACE_TYPE) ||
        (pIf6->u1IfType == IP6_PSEUDO_WIRE_INTERFACE_TYPE) ||
        (pIf6->u1IfType == IP6_L3VLAN_INTERFACE_TYPE) ||
        (pIf6->u1IfType == IP6_LAGG_INTERFACE_TYPE) ||
        (pIf6->u1IfType == IP6_L3SUB_INTF_TYPE)
#ifdef TUNNEL_WANTED
        || (pIf6->u1IfType == IP6_TUNNEL_INTERFACE_TYPE)
#endif
#ifdef WGS_WANTED
        || (pIf6->u1IfType == IP6_L2VLAN_INTERFACE_TYPE)
#endif
        )
    {
        pNd6cLinfo = (tNd6CacheLanInfo *) (VOID *) pNd6cEntry->pNd6cInfo;
        /*
         * Upon the queued IPv6 pkts exceeding the MAX limit, release
         * any new buffer attempted to be queued on the entry
         */
        if (ND6_PEND_QUE_CNT_FROM_LINFO (pNd6cLinfo) == ND6C_QUEUE_LEN)
        {

            pTempBuf = ND6_PEND_QUE_LST_FROM_LINFO (pNd6cLinfo);

            /*Unlink the header from the list and make the 2nd node as header */

            if (pTempBuf != NULL)
            {
                ND6_PEND_QUE_LST_FROM_LINFO (pNd6cLinfo) =
                    CRU_BUF_UnLink_BufChain
                    (ND6_PEND_QUE_LST_FROM_LINFO (pNd6cLinfo));

                Ip6BufRelease (pIf6->pIp6Cxt->u4ContextId, pTempBuf, FALSE,
                               ND6_MODULE);
                ND6_PEND_QUE_CNT_FROM_LINFO (pNd6cLinfo)--;
            }
        }

        if (ND6_PEND_QUE_LST_FROM_LINFO (pNd6cLinfo) == NULL)
        {
            ND6_PEND_QUE_LST_FROM_LINFO (pNd6cLinfo) = pBuf;
        }
        else
        {
            CRU_BUF_Link_BufChains
                (ND6_PEND_QUE_END_FROM_LINFO (pNd6cLinfo), pBuf);
        }

        ND6_PEND_QUE_END_FROM_LINFO (pNd6cLinfo) = pBuf;
        ND6_PEND_QUE_CNT_FROM_LINFO (pNd6cLinfo)++;

    }

    return (IP6_SUCCESS);

}

/*****************************************************************************
* DESCRIPTION : Purge the IPv6 packets queued on NDCache entry of Enet interface
* 
* INPUTS      : pNd6cEntry  -  Pointer to the Nd cache entry.
*
* OUTPUTS     : None
* 
* RETURNS     : None
*****************************************************************************/
VOID
Nd6PurgePendQue (tNd6CacheEntry * pNd6cEntry)
{
    tIp6Hdr             ip6Hdr, *pIp6Hdr = NULL;
    tIp6If             *pIf6 = NULL;
    tNd6CacheLanInfo   *pNd6cLinfo = NULL;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    tIp6Cxt            *pIp6Cxt = NULL;
    /* FIX */
    UINT4               u4Count = 0;
    UINT4               u4TmpIndex = 0;
    UINT1               u1Copy = 0;

    pIf6 = pNd6cEntry->pIf6;
    pIp6Cxt = pIf6->pIp6Cxt;

    if ((pIf6->u1IfType == IP6_ENET_INTERFACE_TYPE) ||
        (pIf6->u1IfType == IP6_PSEUDO_WIRE_INTERFACE_TYPE) ||
        (pIf6->u1IfType == IP6_LAGG_INTERFACE_TYPE) ||
        (pIf6->u1IfType == IP6_L3VLAN_INTERFACE_TYPE) ||
        (pIf6->u1IfType == IP6_L3SUB_INTF_TYPE)
#ifdef TUNNEL_WANTED
        || (pIf6->u1IfType == IP6_TUNNEL_INTERFACE_TYPE)
#endif
#ifdef WGS_WANTED
        || (pIf6->u1IfType == IP6_L2VLAN_INTERFACE_TYPE)
#endif
        )
    {
        pNd6cLinfo = (tNd6CacheLanInfo *) (VOID *) pNd6cEntry->pNd6cInfo;

        /* Release the IPv6 packets queued on the NDCache entry */
        for (u4Count = ND6_PEND_QUE_CNT_FROM_LINFO (pNd6cLinfo); u4Count != 0;
             u4Count--)
        {
            if (ND6_PEND_QUE_LST_FROM_LINFO (pNd6cLinfo) == NULL)
            {
                continue;
            }
            pBuf = ND6_PEND_QUE_LST_FROM_LINFO (pNd6cLinfo);    /* FIX */

            ND6_PEND_QUE_LST_FROM_LINFO (pNd6cLinfo) =
                CRU_BUF_UnLink_BufChain
                (ND6_PEND_QUE_LST_FROM_LINFO (pNd6cLinfo));

            ND6_PEND_QUE_CNT_FROM_LINFO (pNd6cLinfo)--;

            /*
             * Release the buffer if the state of the NDCache entry is not
             * INCOMPLETE, Otherwise, send ICMP error message using the
             * same buffer
             */
            if ((pNd6cEntry->u1ReachState == ND6C_INCOMPLETE) ||
                (pNd6cEntry->u1ReachState == ND6C_STATIC_NOT_IN_SERVICE))
            {
                if ((pIp6Hdr = (tIp6Hdr *) Ip6BufRead
                     (pBuf, (UINT1 *) &ip6Hdr, 0,
                      (sizeof (tIp6Hdr)), u1Copy)) == NULL)
                {
                    /* Incase of FATAL errors, the following buf calls need 
                     * not be checked for return values
                     */
                    IP6_TRC_ARG2 (pIp6Cxt->u4ContextId, ND6_MOD_TRC, BUFFER_TRC,
                                  ND6_NAME,
                                  "ND6:Nd6PurgePendQue: Purge Queue - BufRead "
                                  "Failed! BufPtr %p Offset %d\n", pBuf, 0);
                    IP6_TRC_ARG3 (pIp6Cxt->u4ContextId, ND6_MOD_TRC, BUFFER_TRC,
                                  IP6_NAME,
                                  "ND6:Nd6PurgePendQue: %s buffer error "
                                  "occurred, Type = %d Bufptr = %p",
                                  ERROR_FATAL_STR, ERR_BUF_READ, pBuf);
                    Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE,
                                   ND6_MODULE);
                    continue;
                }

                Icmp6SendErrMsg (pIf6, pIp6Hdr, ICMP6_DEST_UNREACHABLE,
                                 ICMP6_ADDRESS_UNREACHABLE, 0, pBuf);

                if (Ip6IsOurAddrInCxt (pIp6Cxt->u4ContextId,
                                       &(pIp6Hdr->srcAddr), &u4TmpIndex) ==
                    IP6_SUCCESS)
                {
                    IP6IF_INC_OUT_NO_ROUTE (pIf6);
                    IP6SYS_INC_OUT_NO_ROUTE (pIp6Cxt->Ip6SysStats);
                }
                else
                {
                    IP6IF_INC_IN_NO_ROUTE (pIf6);
                    IP6SYS_INC_IN_NO_ROUTE (pIp6Cxt->Ip6SysStats);
                }
            }
            else
            {
                if (Ip6BufRelease
                    (pIp6Cxt->u4ContextId, pBuf, FALSE,
                     ND6_MODULE) != IP6_SUCCESS)
                {
                    IP6_TRC_ARG (pIp6Cxt->u4ContextId, ND6_MOD_TRC, BUFFER_TRC,
                                 ND6_NAME,
                                 "ND6:Nd6PurgePendQue: Purge Queue - "
                                 "BufRelease Failed\n");
                    IP6_TRC_ARG3 (pIp6Cxt->u4ContextId, ND6_MOD_TRC, BUFFER_TRC,
                                  IP6_NAME,
                                  "ND6:Nd6PurgePendQue: %s buffer error "
                                  "occurred, Type = %d Bufptr = %p",
                                  ERROR_FATAL_STR, ERR_BUF_RELEASE, pBuf);
                }
            }
        }
        ND6_PEND_QUE_END_FROM_LINFO (pNd6cLinfo) = NULL;
        ND6_PEND_QUE_CNT_FROM_LINFO (pNd6cLinfo) = 0;
    }
}

/*****************************************************************************
* DESCRIPTION : Send the IPv6 packets from the pending queue of the NDCache 
*               entry on Enet interface 
* 
* INPUTS      : pNd6cEntry  -  Pointer to the Nd cache entry.
*
* OUTPUTS     : None
* 
* RETURNS     : None
*****************************************************************************/
VOID
Nd6SendPendQue (tNd6CacheEntry * pNd6cEntry)
{
    tIp6If             *pIf6 = NULL;
    tNd6CacheLanInfo   *pNd6cLinfo = NULL;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    UINT4               u4Count = 0;

    pIf6 = pNd6cEntry->pIf6;

    /* ND cache entries can exist for either tunnel or ethernet interface */

    if ((pIf6->u1IfType == IP6_ENET_INTERFACE_TYPE) ||
        (pIf6->u1IfType == IP6_PSEUDO_WIRE_INTERFACE_TYPE) ||
        (pIf6->u1IfType == IP6_LAGG_INTERFACE_TYPE) ||
        (pIf6->u1IfType == IP6_L3VLAN_INTERFACE_TYPE) ||
        (pIf6->u1IfType == IP6_L3SUB_INTF_TYPE)
#ifdef TUNNEL_WANTED
        || (pIf6->u1IfType == IP6_TUNNEL_INTERFACE_TYPE)
#endif
#ifdef WGS_WANTED
        || (pIf6->u1IfType == IP6_L2VLAN_INTERFACE_TYPE)
#endif
        )
    {
        pNd6cLinfo = (tNd6CacheLanInfo *) (VOID *) pNd6cEntry->pNd6cInfo;

        /* Send the IPv6 packets queued on the NDCache entry */
        for (u4Count = ND6_PEND_QUE_CNT_FROM_LINFO (pNd6cLinfo); u4Count != 0;
             u4Count--)
        {
            pBuf = ND6_PEND_QUE_LST_FROM_LINFO (pNd6cLinfo);

            ND6_PEND_QUE_LST_FROM_LINFO (pNd6cLinfo) =
                CRU_BUF_UnLink_BufChain (ND6_PEND_QUE_LST_FROM_LINFO
                                         (pNd6cLinfo));

            ND6_PEND_QUE_CNT_FROM_LINFO (pNd6cLinfo)--;
#ifdef TUNNEL_WANTED

            /* If the queued pkts are to be sent over tunnel, the pkt needs 
             * to be enqued to IPv4 task after finding source v4 address*/

            if (pIf6->u1IfType == IP6_TUNNEL_INTERFACE_TYPE)
            {
                Ip6ifTunlSend (pIf6, &pNd6cEntry->addr6, pNd6cEntry,
                               IP6_BUF_DATA_LEN (pBuf), pBuf);
            }
            else
#endif
            {
                Ip6ifFormEthHdr (pNd6cLinfo->lladdr, pBuf);
                Ip6Output (pIf6, IP6_BUF_DATA_LEN (pBuf), pBuf);
            }
        }
        ND6_PEND_QUE_END_FROM_LINFO (pNd6cLinfo) = NULL;
        ND6_PEND_QUE_CNT_FROM_LINFO (pNd6cLinfo) = 0;
    } /*** if end ***/
} /*** fn. end ***/

/*****************************************************************************
* DESCRIPTION : This routine assigns the reachability state for a NDCache entry
*
* INPUTS      : pNd6cEntry  -  Pointer to the Nd cache entry.
*               u1State     -  State of the entry.
*
* OUTPUTS     : None
* 
* RETURNS     : None
*****************************************************************************/
VOID
Nd6SetReachState (tNd6CacheEntry * pNd6cEntry, UINT1 u1State)
{
    UINT4               u4ContextId = 0;
#ifdef NPAPI_WANTED
    UINT2               u2VlanId;
    UINT4               u4PhyIfIndex = 0;
#ifdef TUNNEL_WANTED
    tTnlIfEntry         TnlIfEntry;
#endif
#endif /* NPAPI_WANTED */

    u4ContextId = pNd6cEntry->pIf6->pIp6Cxt->u4ContextId;
    UINT1               u1PrevState = pNd6cEntry->u1ReachState;
    IP6_TRC_ARG2 (u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC, ND6_NAME,
                  "ND6:Nd6SetReachState: Set Reach State - ND Cache %p State %d\n",
                  pNd6cEntry, u1State);

    if ((gu4Nd6CacheEntries >= MAX_IP6_ND6_HW_CACHE_ENTRIES) &&
        (((pNd6cEntry->u1ReachState == ND6C_PROBE) &&
          (u1State == ND6C_REACHABLE) &&
          (pNd6cEntry->u1StaleToProbe == TRUE)) ||
         (u1State == ND6C_INCOMPLETE)))
    {
         /** If HW L3 host table reached the maximum supported
             ND6_cache entries, then no entries can be programmed in HW.
             Even new state to be set is reachable or Incomplete,
             the state is changed as STALE explicitly.
              So HW programming wil not happen when table reached MAX ***/
        pNd6cEntry->u1ReachState = ND6C_STALE;
        if (gNd6GblInfo.u4ProbeNd6CacheEntries > 0)
        {
            gNd6GblInfo.u4ProbeNd6CacheEntries--;
        }
        gNd6GblInfo.u4StaleNd6CacheEntries++;
        /* Update the NDCache last updated time for this entry */
        OsixGetSysTime ((tOsixSysTime *) & (pNd6cEntry->u4LastUpdatedTime));

        return;

    }

    if ((gu4Nd6CacheEntries >= MAX_IP6_ND6_HW_CACHE_ENTRIES)
        && ((u1State == ND6C_DELAY) && (u1PrevState == ND6C_STALE)))
    {
        u1State = ND6C_STALE;
    }

    UpdateNDCounters (u1PrevState, u1State);
    gu4Nd6CacheEntries = IP6_ND_ENTRIES_IN_HW;

    if (((pNd6cEntry->u1ReachState == ND6C_INCOMPLETE) ||
         (pNd6cEntry->u1ReachState == ND6C_REACHABLE)) &&
        (u1State == ND6C_STALE))
    {
        pNd6cEntry->u1StaleToProbe = FALSE;
    }

    pNd6cEntry->u1ReachState = u1State;

    /* By default we will not sync the entry to standby. On specific cases, sync will be set to true */

    pNd6cEntry->u1Sync = OSIX_FALSE;

    /* Update the NDCache last updated time for this entry */
    OsixGetSysTime ((tOsixSysTime *) & (pNd6cEntry->u4LastUpdatedTime));

#ifdef NPAPI_WANTED
    if ((pNd6cEntry->pIf6->u1IfType == IP6_L3VLAN_INTERFACE_TYPE)
#ifdef WGS_WANTED
        || (pNd6cEntry->pIf6->u1IfType == IP6_L2VLAN_INTERFACE_TYPE)
#endif
        || (pNd6cEntry->pIf6->u1IfType == IP6_LAGG_INTERFACE_TYPE)
        || (pNd6cEntry->pIf6->u1IfType == IP6_PSEUDO_WIRE_INTERFACE_TYPE)
        || (pNd6cEntry->pIf6->u1IfType == IP6_L3SUB_INTF_TYPE))
    {
        u4PhyIfIndex = pNd6cEntry->pIf6->u4Index;
    }
#ifdef TUNNEL_WANTED
    else if (pNd6cEntry->pIf6->u1IfType == IP6_TUNNEL_INTERFACE_TYPE)
    {
        /* fetch the Tunnel interface's Physical Interface index */
        if (CfaGetTnlEntryFromIfIndex (pNd6cEntry->pIf6->u4Index, &TnlIfEntry)
            == CFA_FAILURE)
        {
            return;
        }
        u4PhyIfIndex = TnlIfEntry.u4PhyIfIndex;
    }
#endif /* TUNNEL_WANTED */

    /* For Router port , VLAN Id will be zero. Only Interface Index
     * will be provided to NPAPI
     */
    if ((pNd6cEntry->pIf6->u1IfType != IP6_ENET_INTERFACE_TYPE) &&
        (pNd6cEntry->pIf6->u1IfType != IP6_PSEUDO_WIRE_INTERFACE_TYPE))
    {

        if (CfaGetVlanId (u4PhyIfIndex, &u2VlanId) == CFA_FAILURE)
        {
            return;
        }
    }
    else
    {

        u2VlanId = 0;
    }
    if (((pNd6cEntry->u1ReachState == ND6C_INCOMPLETE) ||
         (pNd6cEntry->u1ReachState == ND6C_REACHABLE)) &&
        (u1State == ND6C_STALE))
    {
        pNd6cEntry->u1StaleToProbe = FALSE;
    }

    pNd6cEntry->u1HwStatus = NP_NOT_PRESENT;
    IP6_TASK_UNLOCK ();

    switch (u1State)
    {
        case ND6C_DELAY:

            if (u1PrevState != ND6C_STALE)
            {
                break;

            }

            /* when moving from Stale --> Delay 
             * configure a drop ND iin the hardware
             */
            if (ND6_IS_NP_PROGRAMMING_ALLOWED () == OSIX_TRUE)
            {
                pNd6cEntry->u1Sync = OSIX_TRUE;
                if (ND6_GET_NODE_STATUS () == RM_ACTIVE)
                {
                    Nd6RedAddDynamicInfo (pNd6cEntry,
                                          ((tNd6CacheLanInfo *) (VOID
                                                                 *)
                                           (pNd6cEntry->pNd6cInfo))->lladdr,
                                          ND6_RED_ADD_CACHE);
                    Nd6RedSendDynamicCacheInfo ();
                }
                if (Ipv6FsNpIpv6NeighCacheAdd (u4ContextId,
                                               (UINT1 *) &pNd6cEntry->addr6,
                                               pNd6cEntry->pIf6->u4Index, NULL,
                                               0, NP_IPV6_NH_INCOMPLETE,
                                               u2VlanId) == FNP_FAILURE)
                {
                    gu4CumulativeNdHwFailCount++;
                    break;
                }

            }

            break;

        case ND6C_STATIC_NOT_IN_SERVICE:
        case ND6C_INCOMPLETE:
            if (ND6_IS_NP_PROGRAMMING_ALLOWED () == OSIX_TRUE)
            {
                pNd6cEntry->u1Sync = OSIX_TRUE;
                if (ND6_GET_NODE_STATUS () == RM_ACTIVE)
                {
                    Nd6RedAddDynamicInfo (pNd6cEntry,
                                          ((tNd6CacheLanInfo *) (VOID
                                                                 *)
                                           (pNd6cEntry->pNd6cInfo))->lladdr,
                                          ND6_RED_ADD_CACHE);
                    Nd6RedSendDynamicCacheInfo ();
                }
                if (Ipv6FsNpIpv6NeighCacheAdd (u4ContextId,
                                               (UINT1 *) &pNd6cEntry->addr6,
                                               pNd6cEntry->pIf6->u4Index, NULL,
                                               0, NP_IPV6_NH_INCOMPLETE,
                                               u2VlanId) == FNP_FAILURE)
                {
                    gu4CumulativeNdHwFailCount++;
                    break;
                }

            }
            if (ND6_GET_NODE_STATUS () == RM_ACTIVE)
            {
                Rtm6ApiDelAllRtWithNxtHopInHWInLnx (u4ContextId,
                                                    &pNd6cEntry->addr6);
            }

            break;

        case ND6C_STALE:
            pNd6cEntry->u1Sync = OSIX_TRUE;
            if (ND6_GET_NODE_STATUS () == RM_ACTIVE)
            {
                Nd6RedAddDynamicInfo (pNd6cEntry,
                                      ((tNd6CacheLanInfo *) (VOID
                                                             *) (pNd6cEntry->
                                                                 pNd6cInfo))->
                                      lladdr, ND6_RED_DEL_CACHE);
                Nd6RedSendDynamicCacheInfo ();
            }
            if (ND6_IS_NP_PROGRAMMING_ALLOWED () == OSIX_TRUE)
            {
                if (Ipv6FsNpIpv6NeighCacheDel (u4ContextId,
                                               (UINT1 *) &pNd6cEntry->addr6,
                                               pNd6cEntry->pIf6->u4Index)
                    == FNP_FAILURE)
                {
                    gu4CumulativeNdHwFailCount++;
                    break;
                }

            }
            if (ND6_GET_NODE_STATUS () == RM_ACTIVE)
            {
                Rtm6ApiDelAllRtWithNxtHopInHWInLnx (u4ContextId,
                                                    &pNd6cEntry->addr6);
                /* take lock while accessing rtm api 22May */
                RTM6_TASK_LOCK ();
                Rtm6ApiDelNextHopInCxt (u4ContextId, &pNd6cEntry->addr6);
                RTM6_TASK_UNLOCK ();
            }
            break;

        case ND6C_STATIC:
        case ND6C_REACHABLE:

            /* 
             * In case of Reachable-->Probe-->Reachable ignore the hardware
             * programming and red sync
             * In case we are NOT moving from Stale-->Probe->Reachable , then 
             * do NOT program the hardware again
             */
            if ((u1PrevState == ND6C_PROBE) &&
                (u1State == ND6C_REACHABLE) &&
                (pNd6cEntry->u1StaleToProbe == FALSE) &&
                (pNd6cEntry->u1LlaFlag != TRUE))
            {
                break;
            }
            pNd6cEntry->u1StaleToProbe = FALSE;

            /* Redundancy related processing for ND entries reaching Active state */
            pNd6cEntry->u1Sync = OSIX_TRUE;

            if (ND6_GET_NODE_STATUS () == RM_ACTIVE)
            {
                Nd6RedAddDynamicInfo (pNd6cEntry,
                                      ((tNd6CacheLanInfo *) (VOID
                                                             *) (pNd6cEntry->
                                                                 pNd6cInfo))->
                                      lladdr, ND6_RED_ADD_CACHE);
                Nd6RedSendDynamicCacheInfo ();
            }
            if (ND6_IS_NP_PROGRAMMING_ALLOWED () == OSIX_TRUE)
            {
                if (Ipv6FsNpIpv6NeighCacheAdd (u4ContextId,
                                               (UINT1 *) &pNd6cEntry->addr6,
                                               pNd6cEntry->pIf6->u4Index,
                                               ((tNd6CacheLanInfo *) (VOID
                                                                      *)
                                                (pNd6cEntry->pNd6cInfo))->
                                               lladdr, IP6_MAX_LLA_LEN,
                                               NP_IPV6_NH_REACHABLE,
                                               u2VlanId) == FNP_FAILURE)
                {
                    gu4CumulativeNdHwFailCount++;
                    break;
                }

                Ipv6FsNpIpv6CheckHitOnNDCacheEntry (u4ContextId,
                                                    (UINT1 *) &pNd6cEntry->
                                                    addr6,
                                                    pNd6cEntry->pIf6->u4Index);

            }
            /* Scan the routing table for the routes whose next hop is
             * matches with the resolved next hop and populate the routes
             * in the H/W routing table 
             */

            if (ND6_GET_NODE_STATUS () == RM_ACTIVE)
            {
                /* take lock 22May */
                RTM6_TASK_LOCK ();
                Rtm6ApiAddRtInHWInCxt (u4ContextId, &pNd6cEntry->addr6);
                RTM6_TASK_UNLOCK ();
                Rtm6ApiAddAllRtWithNxtHopInHWInCxt (u4ContextId,
                                                    &pNd6cEntry->addr6);
                Rtm6ApiAddAllRtWithNxtHopInHWInLnx (u4ContextId,
                                                    &pNd6cEntry->addr6);
            }
            break;
    }

    IP6_TASK_LOCK ();

    /* The updated/added/deleted entry is added to the global RedRBtree.
     * This tree is scanned for sending dynamic updates.
     * In case of Async NP calls, the HwStatus is set as NP_NOT_PRESENT
     * before writing to the NP. And the change is synced with the standby
     * node. When the NP call is success, the state is changed to NP_PRESENT
     * and the new state is synced as part of the NP calls return value.
     * In case of sequential NP calls, entry is synced only when the NP
     * call returns success. So there is no need to sync the HwStatus
     * before and after writing to NP. */
    pNd6cEntry->u1HwStatus = NP_PRESENT;
    if (ND6_GET_NODE_STATUS () == RM_ACTIVE)
    {
        if (pNd6cEntry->u1Sync == OSIX_TRUE)
        {
            if (u1State == ND6C_STALE)
            {

                Nd6RedAddDynamicInfo (pNd6cEntry,
                                      ((tNd6CacheLanInfo *) (VOID
                                                             *) (pNd6cEntry->
                                                                 pNd6cInfo))->
                                      lladdr, ND6_RED_DEL_CACHE);
                Nd6RedSendDynamicCacheInfo ();
            }
            else
            {

                Nd6RedAddDynamicInfo (pNd6cEntry,
                                      ((tNd6CacheLanInfo *) (VOID
                                                             *) (pNd6cEntry->
                                                                 pNd6cInfo))->
                                      lladdr, ND6_RED_ADD_CACHE);
                Nd6RedSendDynamicCacheInfo ();
            }

        }
    }
    pNd6cEntry->u1Sync = OSIX_FALSE;
#endif /* NPAPI_WANTED */
}

/*****************************************************************************
* DESCRIPTION : This routine updates the ND counters
*
* INPUTS      : 
*               u1PrevState -  Previous state of ND Entry
*               u1State     -  New state of ND Entry
*
* OUTPUTS     : None
* 
* RETURNS     : None
*****************************************************************************/

VOID
UpdateNDCounters (UINT1 u1PrevState, UINT1 u1NewState)
{

    DecrementNDCounters (u1PrevState);

    IncrementNDCounters (u1NewState);

}

VOID
IncrementNDCounters (UINT1 u1NewState)
{

    switch (u1NewState)
    {
        case ND6C_INCOMPLETE:
            gNd6GblInfo.u4IncompNd6CacheEntries++;
            break;

        case ND6C_STALE:
            /* This state is not applicable for Standby */
            if (ND6_GET_NODE_STATUS () == RM_ACTIVE)
                gNd6GblInfo.u4StaleNd6CacheEntries++;
            break;

        case ND6C_DELAY:
            gNd6GblInfo.u4DelayNd6CacheEntries++;
            break;

        case ND6C_PROBE:
            gNd6GblInfo.u4ProbeNd6CacheEntries++;
            break;

        case ND6C_REACHABLE:
            gNd6GblInfo.u4ReachNd6CacheEntries++;
            break;
        default:
            break;
    }
}

VOID
DecrementNDCounters (UINT1 u1PrevState)
{

    switch (u1PrevState)
    {
        case ND6C_INCOMPLETE:
            if (gNd6GblInfo.u4IncompNd6CacheEntries > 0)
            {
                gNd6GblInfo.u4IncompNd6CacheEntries--;
            }
            break;

        case ND6C_STALE:
            if (gNd6GblInfo.u4StaleNd6CacheEntries > 0)
            {
                gNd6GblInfo.u4StaleNd6CacheEntries--;
            }
            break;

        case ND6C_DELAY:
            if (gNd6GblInfo.u4DelayNd6CacheEntries > 0)
            {
                gNd6GblInfo.u4DelayNd6CacheEntries--;
            }
            break;

        case ND6C_PROBE:
            if (gNd6GblInfo.u4ProbeNd6CacheEntries > 0)
            {
                gNd6GblInfo.u4ProbeNd6CacheEntries--;
            }
            break;

        case ND6C_REACHABLE:
            if (gNd6GblInfo.u4ReachNd6CacheEntries > 0)
            {
                gNd6GblInfo.u4ReachNd6CacheEntries--;
            }
            break;
        default:
            break;
    }

}

/*****************************************************************************
* DESCRIPTION : This routine Resets  the ND counters to 0
*
* INPUTS      : None
*
* OUTPUTS     : None
* 
* RETURNS     : None
*****************************************************************************/

VOID
ResetNDCounters ()
{
    gNd6GblInfo.u4IncompNd6CacheEntries = 0;
    gNd6GblInfo.u4StaleNd6CacheEntries = 0;
    gNd6GblInfo.u4DelayNd6CacheEntries = 0;
    gNd6GblInfo.u4ProbeNd6CacheEntries = 0;
    gNd6GblInfo.u4ReachNd6CacheEntries = 0;
}

/*****************************************************************************
* DESCRIPTION : This routine starts the specified timer on a NDCache entry
*
* INPUTS      : pNd6cEntry  -  Pointer to the Nd cache entry.
*               u1TimerId   -  Timer Id.  
*               u1TimerVal  -  Time value.
*
* OUTPUTS     : None
* 
* RETURNS     : None
*****************************************************************************/
VOID
Nd6SetCacheTimer (tNd6CacheEntry * pNd6cEntry, UINT1 u1TimerId,
                  UINT4 u4TimerVal)
{
    tNd6CacheLanInfo   *pNd6cLinfo = NULL;

    pNd6cLinfo = (tNd6CacheLanInfo *) (VOID *) pNd6cEntry->pNd6cInfo;
    if (u4TimerVal != 0)
    {
        Ip6TmrStart (pNd6cEntry->pIf6->pIp6Cxt->u4ContextId, u1TimerId,
                     gIp6GblInfo.Ip6TimerListId,
                     &(pNd6cLinfo->timer.appTimer), u4TimerVal);
    }
}

/*****************************************************************************
* DESCRIPTION : This routine starts the specified timer in Milli Second 
*               on a NDCache entry
*
* INPUTS      : pNd6cEntry  -  Pointer to the Nd cache entry.
*               u1TimerId   -  Timer Id.  
*               u4TimerVal  -  Time value In Milli Second.
*
* OUTPUTS     : None
* 
* RETURNS     : None
*****************************************************************************/
VOID
Nd6SetMSecCacheTimer (tNd6CacheEntry * pNd6cEntry, UINT1 u1TimerId,
                      UINT4 u4TimerVal)
{
    tNd6CacheLanInfo   *pNd6cLinfo = NULL;

    pNd6cLinfo = (tNd6CacheLanInfo *) (VOID *) pNd6cEntry->pNd6cInfo;
    if (u4TimerVal != IP6_ZERO)
    {
        /* MilliSeconds to TIME_UNITS_IN_A_SEC */
        u4TimerVal = (u4TimerVal / IP6_THOUSAND);

        Ip6TmrStart (pNd6cEntry->pIf6->pIp6Cxt->u4ContextId, u1TimerId,
                     gIp6GblInfo.Ip6TimerListId,
                     &(pNd6cLinfo->timer.appTimer), u4TimerVal);
    }
}

/*****************************************************************************
* DESCRIPTION : This routine cancels any already running timer on a NDCache 
*               entry.
*
* INPUTS      : pNd6cEntry  -  Pointer to the Nd cache entry.
*
* OUTPUTS     : None
* 
* RETURNS     : None
*****************************************************************************/
VOID
Nd6CancelCacheTimer (tNd6CacheEntry * pNd6cEntry)
{
    tNd6CacheLanInfo   *pNd6cLinfo = NULL;

    pNd6cLinfo = (tNd6CacheLanInfo *) (VOID *) pNd6cEntry->pNd6cInfo;

    /* 
     * ND Timer Node is the same for starting any of the Cache timers 
     * and hence timer-id is extracted from the same timer node, becoz
     * No two Cache timers run at the same time.
     */
    if (pNd6cLinfo->timer.u1Id != 0)
    {
        Ip6TmrStop (pNd6cEntry->pIf6->pIp6Cxt->u4ContextId,
                    pNd6cLinfo->timer.u1Id, gIp6GblInfo.Ip6TimerListId,
                    &(pNd6cLinfo->timer.appTimer));
    }
}

/*****************************************************************************
* DESCRIPTION : This routine updates the Usage time of the NDCache entry
*
* INPUTS      : pNd6cEntry  -  Pointer to the Nd cache entry.
*
* OUTPUTS     : None
*
* RETURNS     : None
*****************************************************************************/
VOID
Nd6UpdateLastUseTime (tNd6CacheEntry * pNd6cEntry)
{
    pNd6cEntry->u4LastUseTime = OsixGetSysUpTime ();
}

/*****************************************************************************
* DESCRIPTION : This routine returns the reachability state of a NDCache entry
*
* INPUTS      : pNd6cEntry  -  Pointer to the Nd cache entry.
*
* OUTPUTS     : None
* 
* RETURNS     : None
*****************************************************************************/
UINT1
Nd6GetReachState (tNd6CacheEntry * pNd6cEntry)
{

    return (pNd6cEntry->u1ReachState);

}

/*****************************************************************************
* DESCRIPTION : This routine returns the lower layer address of a ENET NDCache 
*               entry
*
* INPUTS      : pNd6cEntry  -  Pointer to the Nd cache entry.
*               
*
* OUTPUTS     : pLladdr     -  pointer to the link local address.
*               pLlaLen     -  pointer to the link local address length.
* 
* RETURNS     : None
*****************************************************************************/
VOID
Nd6GetLladdr (tNd6CacheEntry * pNd6cEntry, UINT1 *pLladdr, UINT1 *pLlaLen)
{
    tNd6CacheLanInfo   *pNd6cLinfo = NULL;

    pNd6cLinfo = (tNd6CacheLanInfo *) (VOID *) pNd6cEntry->pNd6cInfo;
    *pLlaLen = IP6_ENET_ADDR_LEN;
    MEMCPY (pLladdr, pNd6cLinfo->lladdr, *pLlaLen);

}

/* API's to access the ND Proxy List */

/*****************************************************************************
* DESCRIPTION : This routine Adds/Deletes an Address from the NDProxy List 
*                in the given context.
*
* INPUTS      : pIp6Cxt - Pointer to the Ip6Cxt in which the proxy node 
*                         has to be updated.
*               Ip6Addr - The Address to be added to, or deleted from the List.
*               i1Flag  - Flag to indicate whether the address 
*                         is to be added/deleted.
* OUTPUTS     : None.
* 
* RETURNS     : IP6_SUCCESS / IP6_FAILURE
*****************************************************************************/
INT4
Nd6ProxyUpdateInCxt (tIp6Cxt * pIp6Cxt, tIp6Addr Ip6Addr, INT4 i4Flag)
{
    tNd6ProxyListNode  *pNd6ProxyListNode = NULL;
    tNd6ProxyListNode  *pNd6TempNode = NULL;
    tNd6ProxyListNode  *pNd6PrevNode = NULL;
    tTMO_SLL_NODE      *pSllInfo = NULL;
    INT1                i1Found = 0;

    /* Check for the Presence of the address in Proxy List */
    if ((pNd6ProxyListNode = Nd6ProxyCheckAddrInCxt (pIp6Cxt, Ip6Addr)) != NULL)
    {
        /* Entry exists! */
        i1Found = 1;
    }

    /* Add Entry, if only it's not already in the List */
    if ((i4Flag == ND6_ADD_TO_PROXY_LIST) && (i1Found == 0))
    {
        if ((pNd6ProxyListNode =
             (tNd6ProxyListNode *) (VOID *) Ip6GetMem (pIp6Cxt->u4ContextId,
                                                       gNd6GblInfo.
                                                       i4Nd6ProxyListId)) ==
            NULL)
        {
            IP6_TRC_ARG (pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                         MGMT_TRC | ALL_FAILURE_TRC, ND6_NAME,
                         "Nd6ProxyUpdateInCxt: Memory Allocation for "
                         "Proxy list FAILED\n");
            return IP6_FAILURE;
        }

        Ip6AddrCopy (&pNd6ProxyListNode->Nd6ProxyAddr, &Ip6Addr);

        TMO_SLL_Init_Node (&(pNd6ProxyListNode->Next));
        /* Add the address in the address list in ascending order */
        TMO_SLL_Scan (&(pIp6Cxt->Nd6ProxyList), pSllInfo, tTMO_SLL_NODE *)
        {
            pNd6TempNode = ND6_PROXY_LST_PTR_FROM_SLL (pSllInfo);

            if (Ip6IsAddrGreater (&Ip6Addr, &pNd6TempNode->Nd6ProxyAddr) ==
                SUCCESS)
            {
                break;
            }
            pNd6PrevNode = pNd6TempNode;
        }
        TMO_SLL_Insert (&(pIp6Cxt->Nd6ProxyList),
                        &(pNd6PrevNode->Next), &(pNd6ProxyListNode->Next));

        return IP6_SUCCESS;
    }
    /* Delete Entry, if only it's present in the List */
    else if ((i4Flag == ND6_DEL_FROM_PROXY_LIST) && (i1Found == 1))
    {
        TMO_SLL_Delete (&(pIp6Cxt->Nd6ProxyList), &(pNd6ProxyListNode->Next));
        Ip6TmrStop (pIp6Cxt->u4ContextId, IP6_PROXY_ND_TIMER_ID,
                    gIp6GblInfo.Ip6TimerListId,
                    &pNd6ProxyListNode->ProxyTimer.appTimer);
        Ip6RelMem (pIp6Cxt->u4ContextId, (UINT2) gNd6GblInfo.i4Nd6ProxyListId,
                   (UINT1 *) pNd6ProxyListNode);

        return IP6_SUCCESS;
    }
    /* return FAILURE, for any other combination of i1Flag & u1Found */
    else
    {
        return IP6_FAILURE;
    }

}

/*****************************************************************************
* DESCRIPTION : This routine Checks for the presence of an Address 
*               in the NDProxy List for the given context.
*
* INPUTS      : pIp6Cxt - Pointer to the Ip6Cxt in which the proxy node
*                          has to be searched.
*               Ip6Addr - The Address to be checked in the List.
*
* OUTPUTS     : None.
* 
* RETURNS     :  NULL - if the address do not exists in Proxy List OR
*                a Pointer to tNd6ProxyListNode, if the address exists in 
*                the List.
*****************************************************************************/
tNd6ProxyListNode  *
Nd6ProxyCheckAddrInCxt (tIp6Cxt * pIp6Cxt, tIp6Addr Ip6Addr)
{
    tNd6ProxyListNode  *pProxyListFirstNode = NULL;
    tNd6ProxyListNode  *pProxyListNextNode = NULL;
    tTMO_SLL_NODE      *pNext = NULL;
    UINT4               u4ProxyCount = 0;
    UINT2               u2Index = 0;

    u4ProxyCount = TMO_SLL_Count (&(pIp6Cxt->Nd6ProxyList));

    /* First Check if an Entry Exists in the List. */
    if (u4ProxyCount <= 0)
    {
        IP6_TRC_ARG1 (pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                      MGMT_TRC | ALL_FAILURE_TRC, ND6_NAME,
                      "Nd6ProxyCheckAddrInCxt: No proxy nodes are configured "
                      "for the context %d\n", pIp6Cxt->u4ContextId);
        return NULL;
    }

    pProxyListFirstNode = NULL;

    for (u2Index = 0; u2Index < u4ProxyCount; u2Index++)
    {
        pNext =
            TMO_SLL_Next (&(pIp6Cxt->Nd6ProxyList),
                          &(pProxyListFirstNode->Next));
        pProxyListNextNode = ND6_PROXY_LST_PTR_FROM_SLL (pNext);

        if (Ip6AddrMatch (&pProxyListNextNode->Nd6ProxyAddr, &Ip6Addr,
                          IP6_ADDR_SIZE_IN_BITS) == TRUE)
        {
            return pProxyListNextNode;
        }

        pProxyListFirstNode = pProxyListNextNode;
    }

    return NULL;
}

#ifdef MIP6_WANTED
/*****************************************************************************
* DESCRIPTION : This function processes home agent information option
*               Resets the HA life time and preference.
*
* INPUTS      : pIf6 - Pointer to the interface structure.
*               pIp6 - Pointer to the IP6 header of the received message.
*               pNd6HaInfo - Pointer to the home agent information option.
*
* OUTPUTS     : None.
*
* RETURNS     : None.
*****************************************************************************/
VOID
Mip6ProcessHAInfoOpt (tIp6If * pIf6, tIp6Hdr * pIp6, tNd6HaInfo * pNd6HaInfo)
{
    tHaListEntry       *pHAEntry = NULL;
    UINT$               u4ContextId = pIf6->pIp6Cxt->u4ContextId;

    if ((pHAEntry = Mip6GetHaEntry (&pIp6->srcAddr, (UINT2) pIf6->u4Index))
        != NULL)
    {
        /* set Current Preference and Lifetime values
         * to the pointer to HAEntry received above
         */
        pHAEntry->i2Pref = OSIX_NTOHS (pNd6HaInfo->i2HaPref);
        Ip6TmrStop (u4ContextId, IP6_HA_LIFE_TIMER_ID,
                    gIp6GblInfo.Ip6TimerListId,
                    &pHAEntry->haListEntryTimer.appTimer);
        if (pNd6HaInfo->u2HaLifetime)
        {
            Ip6TmrStart (u4ContextId, IP6_HA_LIFE_TIMER_ID,
                         gIp6GblInfo.Ip6TimerListId,
                         &pHAEntry->haListEntryTimer.appTimer,
                         OSIX_NTOHS (pNd6HaInfo->u2HaLifetime));
        }
        else
        {
            Mip6DeleteHaEntry (&pIp6->srcAddr, (UINT2) pIf6->u4Index);
        }
    }
    else
    {
        Mip6UpdateHaEntry (&pIp6->srcAddr, pIf6->u4Index,
                           (INT2) OSIX_NTOHS (pNd6HaInfo->i2HaPref),
                           (UINT2) OSIX_NTOHS (pNd6HaInfo->u2HaLifetime));
    }
}

/******************************************************************************
DESCRIPTION        This routine processes prefix option as a part os updating
                   HAlist-global addresses which are used in dynamic home agent
                   discovery.
INPUTS              pNd6Prefix - Pointer to the prefix option.
                    pIf6 - pointer to the interface structure.
                    pIp6 - pointer to the message header.
OUTPUTS             None.
RETURNS             IP6_SUCCESS/ IP6_FAILURE
NOTES               Prefixes which are autoconfigurable are not taken
                    into  consideration. By default HA advertises prefixes of
                    it own interface.
*******************************************************************************/
INT4
Mip6AddGlobAddrHAList (tNd6PrefixExt * pNd6Prefix, tIp6If * pIf6,
                       tIp6Hdr * pIp6)
{
    tTMO_SLL_NODE      *pSnode = 0;
    tHaGlbPrefix       *pAddrInfo = NULL;
    tIp6Addr           *pAddr = 0;
    tHaListEntry       *pHAEntry = 0;
    tHaGlbPrefix       *pNode = NULL;
    UINT4               u4ContextId = pIf6->pIp6Cxt->u4ContextId;

    /*if address in not usable for autoconfiguration and as HA,
     * No need to add this in the list*/
    if (!((pNd6Prefix->u1PrefFlag & ND6_ROUTER_ADDRESS_FLAG)
          || (pNd6Prefix->u1PrefFlag & ND6_AUTO_CONFIG_FLAG)))
    {
        return IP6_SUCCESS;
    }
    else if ((pHAEntry = Mip6GetHaEntry (&pIp6->srcAddr, pIf6->u4Index))
             == NULL)
    {
        return IP6_SUCCESS;
    }

    TMO_SLL_Scan (&pHAEntry->GlobalAddrList, pSnode, tTMO_SLL_NODE *)
    {
        pAddrInfo = (tHaGlbPrefix *) (pSnode);
        pAddr = &pAddrInfo->GlbAddr;

        if (Ip6AddrMatch (pAddr, &pNd6Prefix->prefAddr6, pNd6Prefix->u1PrefLen))
        {
            if ((pNd6Prefix->u1PrefFlag | ND6_ON_LINK_FLAG))
            {
                pAddrInfo->u1Flags |= ND6_ON_LINK_FLAG;
            }
            /* Preserve the flags as they are advertised */
            if ((pNd6Prefix->u1PrefFlag | ND6_AUTO_CONFIG_FLAG))
            {
                /* This prefix will be used while Prefix Adv sent by this HA */
                pAddrInfo->u1Flags |= ND6_AUTO_CONFIG_FLAG;
            }

            if ((pNd6Prefix->u1PrefFlag | ND6_ROUTER_ADDRESS_FLAG))
            {
                /*This Prefix will be used while sending DHAD reply to MN */
                pAddrInfo->u1Flags |= ND6_ROUTER_ADDRESS_FLAG;
            }

            if ((pNd6Prefix->u4ValidTime != 0) && (pNd6Prefix->u4PrefTime != 0))
            {
                /* restart the timers */
                Ip6TmrStop (u4ContextId, IP6_PRFX_VLD_LIFE_TIMER_ID,
                            gIp6GblInfo.Ip6TimerListId,
                            &pAddrInfo->AddrValidTimer.appTimer);
                Ip6TmrStart (u4ContextId, IP6_PRFX_VLD_LIFE_TIMER_ID,
                             gIp6GblInfo.Ip6TimerListId,
                             &pAddrInfo->AddrValidTimer.appTimer,
                             OSIX_NTOHL (pNd6Prefix->u4ValidTime));

                Ip6TmrStop (u4ContextId, IP6_PRFX_PREF_LIFE_TIMER_ID,
                            gIp6GblInfo.Ip6TimerListId,
                            &pAddrInfo->AddrPrefTimer.appTimer);
                Ip6TmrStart (u4ContextId, IP6_PRFX_PREF_LIFE_TIMER_ID,
                             gIp6GblInfo.Ip6TimerListId,
                             &pAddrInfo->AddrPrefTimer.appTimer,
                             OSIX_NTOHL (pNd6Prefix->u4PrefTime));
            }
            else if (((pNd6Prefix->u4ValidTime == 0)
                      && (pNd6Prefix->u4PrefTime == 0)))
            {
                /* Prefix is to be deleted , First inform MN if any 
                 * exists */
                Ip6TmrStop (u4ContextId, IP6_PRFX_VLD_LIFE_TIMER_ID,
                            gIp6GblInfo.Ip6TimerListId,
                            &pAddrInfo->AddrValidTimer.appTimer);
                Ip6TmrStop (u4ContextId, IP6_PRFX_PREF_LIFE_TIMER_ID,
                            gIp6GblInfo.Ip6TimerListId,
                            &pAddrInfo->AddrPrefTimer.appTimer);
#ifdef HA_WANTED
                HAPrcsAddrUpdate (pIf6, (UINT1 *) pAddrInfo, ADMIN_INVALID, 1);
#endif

                TMO_SLL_Delete (&pHAEntry->GlobalAddrList, pSnode);
                Ip6RelMem (u4ContextId, (UINT2) i4GlbPrefPoolId,
                           (UINT1 *) pAddrInfo);

            }
            /* found match - update is over - return */
            return IP6_SUCCESS;
        }
    }
    /* This address is not there in the list - create it */
    if ((pNd6Prefix->u4ValidTime != 0) && (pNd6Prefix->u4PrefTime != 0))
    {

        if ((pNode =
             (tHaGlbPrefix *) Ip6GetMem (u4ContextId, i4GlbPrefPoolId)) == NULL)
        {
            return IP6_FAILURE;
        }

        TMO_SLL_Init_Node ((tTMO_SLL_NODE *) & pNode->nextGlbAddr);

        MEMCPY (&pNode->GlbAddr, &pNd6Prefix->prefAddr6, sizeof (tIp6Addr));

        pNode->u1Flags |= pNd6Prefix->u1PrefFlag;
        pNode->u1PrefixLen = pNd6Prefix->u1PrefLen;

        TMO_SLL_Add (&pHAEntry->GlobalAddrList, (tTMO_SLL_NODE *) pNode);

        /* Start the Timers  */
        Ip6TmrStart (u4ContextId, IP6_PRFX_VLD_LIFE_TIMER_ID,
                     gIp6GblInfo.Ip6TimerListId,
                     &pNode->AddrValidTimer.appTimer,
                     OSIX_NTOHL (pNd6Prefix->u4ValidTime));

        Ip6TmrStart (u4ContextId, IP6_PRFX_PREF_LIFE_TIMER_ID,
                     gIp6GblInfo.Ip6TimerListId, &pNode->AddrPrefTimer.appTimer,
                     OSIX_NTOHL (pNd6Prefix->u4PrefTime));
#ifdef HA_WANTED
        HAPrcsAddrUpdate (pIf6, (UINT1 *) pNode, ADMIN_UP, 1);
#endif
    }

    return IP6_SUCCESS;
}
#endif

/*****************************************************************************
* DESCRIPTION : This routine returns the next availabale Address 
*               in the NDProxy List maintained for the given context.
*
* INPUTS      : pIp6Cxt - The Current context information
*               Ip6Addr - The Address to be checked in the List.
*
* OUTPUTS     : None.
* 
* RETURNS     :  NULL - if the NDProxy List is Empty 
*****************************************************************************/
tNd6ProxyListNode  *
Nd6NextProxyAddrInCxt (tIp6Cxt * pIp6Cxt, tIp6Addr Ip6Addr)
{
    tNd6ProxyListNode  *pProxyListFirstNode = NULL;
    tNd6ProxyListNode  *pProxyListNextNode = NULL;
    tTMO_SLL_NODE      *pNext = NULL;
    UINT4               u4ProxyCount = 0;
    UINT2               u2Index = 0;

    u4ProxyCount = TMO_SLL_Count (&(pIp6Cxt->Nd6ProxyList));

    /* First Check if an Entry Exists in the List. */
    if (u4ProxyCount <= 0)
    {
        IP6_TRC_ARG1 (pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                      MGMT_TRC | ALL_FAILURE_TRC, ND6_NAME,
                      "Nd6NextProxyAddrInCxt: No proxy nodes are configured "
                      "for the context %d\n", pIp6Cxt->u4ContextId);
        return NULL;
    }

    pProxyListFirstNode = NULL;

    for (u2Index = 0; u2Index < u4ProxyCount; u2Index++)
    {
        pNext =
            TMO_SLL_Next (&(pIp6Cxt->Nd6ProxyList),
                          &(pProxyListFirstNode->Next));
        pProxyListNextNode = ND6_PROXY_LST_PTR_FROM_SLL (pNext);

        if (Ip6IsAddrGreater
            (&Ip6Addr, &pProxyListNextNode->Nd6ProxyAddr) == SUCCESS)
        {
            return pProxyListNextNode;
        }

        pProxyListFirstNode = pProxyListNextNode;
    }

    return NULL;
}

/*****************************************************************************
* DESCRIPTION : This function used to Modifies an entry in the NDCache table
*               by management.
* 
* INPUTS      : pNd6cEntry  -   Pointer to the NDCache Entry.
*               pLladdr     -   Pointer to link-layer address
*               u1LlaLen    -   Length of the link-layer address
*               u1NewState  -   the new state of the Entry assigned by 
*                               management
* OUTPUTS     : None
* 
* RETURNS     : None
*****************************************************************************/
VOID
Nd6UpdateStaticCache (tNd6CacheEntry * pNd6cEntry, UINT1 *pLladdr,
                      UINT1 u1LlaLen, UINT1 u1NewState)
{
    UINT1               u1OldState = pNd6cEntry->u1ReachState;

    tNd6CacheLanInfo   *pNd6cLinfo = NULL;
    pNd6cLinfo = (tNd6CacheLanInfo *) (VOID *) pNd6cEntry->pNd6cInfo;

    /*
     * Update the Cache, record the link layer address and send queued packets
     * if any on the entry that was previously in INCOMPLETE or 
     * ND6C_STATIC_NOT_IN_SERVICE state
     */
    if ((pNd6cEntry->u1ReachState != ND6C_STATIC) &&
        (pNd6cEntry->u1ReachState != ND6C_STATIC_NOT_IN_SERVICE))
    {
        /* Cancel already running timer if any on the entry */
        Nd6CancelCacheTimer (pNd6cEntry);
    }

    /* Update the Mac Address and RowStatus */
    if (pLladdr != NULL)
    {
        if (u1LlaLen > IP6_MAX_LLA_LEN)
        {
            return;
        }
        MEMCPY (pNd6cLinfo->lladdr, pLladdr, u1LlaLen);
        pNd6cLinfo->u1LlaLen = u1LlaLen;

        if (u1NewState == ND6C_STATIC_NOT_IN_SERVICE)
        {
            pNd6cEntry->u1RowStatus = NOT_IN_SERVICE;
        }
        else
        {
            pNd6cEntry->u1RowStatus = ACTIVE;
        }
    }
    else
    {
        /* Mac Addr is NULL; update the RowStatus */
        pNd6cEntry->u1RowStatus = NOT_READY;
    }

    /* Update the ND6 Entry Reachability State. */
    Nd6SetReachState (pNd6cEntry, u1NewState);

    /* Send the pending IPv6 pkts queued on the entry that was
     * previously in INCOMPLETE state */
    if (((u1OldState == ND6C_INCOMPLETE) ||
         (u1OldState == ND6C_STATIC_NOT_IN_SERVICE)) &&
        (pNd6cEntry->u1ReachState == ND6C_STATIC))
    {
        Nd6SendPendQue (pNd6cEntry);
        Nd6UpdateLastUseTime (pNd6cEntry);
    }

    /* Update the NDCache last updated time for this entry */
    OsixGetSysTime ((tOsixSysTime *) & (pNd6cEntry->u4LastUpdatedTime));
    return;
}

/*****************************************************************************
* FUNTION     : Nd6RBTreeCacheEntryCmp
*
* DESCRIPTION : This function is used to compare the RBTree keys of the 
*               ND6Cache RBTree.
*
* INPUTS      : pRBElem, pRBElemIn
*
* OUTPUTS     : None
*
* RETURNS     : IP6_RB_LESSER  if pRBElem <  pRBElemIn
*               IP6_RB_GREATER if pRBElem >  pRBElemIn
*               IP6_RB_EQUAL   if pRBElem == pRBElemIn
*****************************************************************************/

INT4
Nd6RBTreeCacheEntryCmp (tRBElem * pRBElem, tRBElem * pRBElemIn)
{
    tNd6CacheEntry     *pNd6cEntry = pRBElem;
    tNd6CacheEntry     *pNd6cEntryIn = pRBElemIn;
    INT4                i4RetVal = 0;

    if (pNd6cEntry->pIf6->u4Index < pNd6cEntryIn->pIf6->u4Index)
    {
        return IP6_RB_LESSER;
    }
    else if (pNd6cEntry->pIf6->u4Index > pNd6cEntryIn->pIf6->u4Index)
    {
        return IP6_RB_GREATER;
    }

    i4RetVal = Ip6AddrCompare (pNd6cEntry->addr6, pNd6cEntryIn->addr6);

    if (i4RetVal == IP6_ZERO)
    {
        return IP6_RB_EQUAL;
    }
    else if (i4RetVal == IP6_ONE)
    {
        return IP6_RB_GREATER;
    }
    else
    {
        return IP6_RB_LESSER;
    }
}

/*****************************************************************************
* FUNTION     : Nd6DeleteContext       
*
* DESCRIPTION : This function deletes the proxy nodes added
*                for the given context.
*
* INPUTS      : u4ContextId
*
* OUTPUTS     : None
*
* RETURNS     : None
*****************************************************************************/

VOID
Nd6DeleteContext (tIp6Cxt * pIp6Cxt)
{
    tNd6ProxyListNode  *pNd6ProxyListNode = NULL;
    tTMO_SLL_NODE      *pNode = NULL;

    /* Get all the proxy list nodes configured for the given context */

    if (TMO_SLL_Count (&(pIp6Cxt->Nd6ProxyList)) > 0)
    {
        pNode = TMO_SLL_First (&(pIp6Cxt->Nd6ProxyList));
        pNd6ProxyListNode = ND6_PROXY_LST_PTR_FROM_SLL (pNode);

        TMO_SLL_Delete (&(pIp6Cxt->Nd6ProxyList), pNode);

        Ip6TmrStop (pIp6Cxt->u4ContextId, IP6_PROXY_ND_TIMER_ID,
                    gIp6GblInfo.Ip6TimerListId,
                    &pNd6ProxyListNode->ProxyTimer.appTimer);
        Ip6RelMem (pIp6Cxt->u4ContextId, (UINT2) gNd6GblInfo.i4Nd6ProxyListId,
                   (UINT1 *) pNd6ProxyListNode);
    }

    TMO_SLL_Init (&(pIp6Cxt->Nd6ProxyList));

    return;
}

/*****************************************************************************
DESCRIPTION           Retrives the state of the ND cache entry learnt for the
                      given IPv6 address. 

INPUTS                u4IfIndex     -   Interface Index          
                      pAddr6        -   Pointer to Destination IPv6 address

OUTPUTS               None    

RETURNS               IP6_SUCCESS / IP6_FAILURE
*****************************************************************************/
INT4
Nd6IsCacheReachable (UINT4 u4IfIndex, tIp6Addr * pAddr6)
{
    tNd6CacheEntry     *pNd6CacheEntry = NULL;
    tIp6If             *pIf6 = NULL;

    IP6_TASK_LOCK ();

    pIf6 = Ip6GetIfFromIndex (u4IfIndex);
    if (pIf6 == NULL)
    {
        IP6_TASK_UNLOCK ();
        return IP6_FAILURE;
    }
    pNd6CacheEntry = Nd6IsCacheForAddr (pIf6, pAddr6);

    if (pNd6CacheEntry == NULL)
    {
        IP6_TASK_UNLOCK ();
        return IP6_FAILURE;
    }

    if ((pNd6CacheEntry->u1ReachState != ND6C_INCOMPLETE)
        && (pNd6CacheEntry->u1ReachState != ND6C_STATIC_NOT_IN_SERVICE))
    {
        IP6_TASK_UNLOCK ();
        return IP6_SUCCESS;
    }

    IP6_TASK_UNLOCK ();
    return IP6_FAILURE;
}

/*****************************************************************************
* DESCRIPTION :This routine adds all the NDCache entries to forwarding plane,
*              When IPv6 Forwarding is enabled
*              This routine deletes all NDCache entries from forwarding plane,
*               When IPv6 Forwarding is disabled
*
* INPUTS      : u4ContextId -  u4ContextId.
*               i4ForwardStatus - IP6 Forwarding enabled or disabled
*
* OUTPUTS     : None
* 
* RETURNS     : None
*****************************************************************************/
VOID
Nd6AddOrDeleteCacheEntriesInCxt (UINT4 u4ContextId, INT4 i4ForwardStatus)
{
#ifdef NPAPI_WANTED
    tNd6CacheEntry     *pNd6cEntry = NULL;
    tNd6CacheEntry     *pNd6cEntryTmp = NULL;
    UINT2               u2VlanId;
    UINT4               u4PhyIfIndex;
    INT4                i4OutCome = 0;
#ifdef TUNNEL_WANTED
    tTnlIfEntry         TnlIfEntry;
#endif
#endif /* NPAPI_WANTED */
#ifndef NPAPI_WANTED
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (i4ForwardStatus);
    return;
#else
    pNd6cEntry = RBTreeGetFirst (gNd6GblInfo.Nd6CacheTable);
    while (pNd6cEntry != NULL)
    {
        /* Entries in the respective context will be validated for Add/Delete in NP */
        if (pNd6cEntry->pIf6->pIp6Cxt->u4ContextId == u4ContextId)
        {
            if ((pNd6cEntry->pIf6->u1IfType == IP6_L3VLAN_INTERFACE_TYPE)
                || (pNd6cEntry->pIf6->u1IfType == IP6_LAGG_INTERFACE_TYPE)
                || (pNd6cEntry->pIf6->u1IfType == IP6_L3SUB_INTF_TYPE)
#ifdef WGS_WANTED
                || (pNd6cEntry->pIf6->u1IfType == IP6_L2VLAN_INTERFACE_TYPE)
#endif
                )
            {
                u4PhyIfIndex = pNd6cEntry->pIf6->u4Index;
            }
#ifdef TUNNEL_WANTED
            else if (pNd6cEntry->pIf6->u1IfType == IP6_TUNNEL_INTERFACE_TYPE)
            {
                /* fetch the Tunnel interface's Physical Interface index */
                if (CfaGetTnlEntryFromIfIndex
                    (pNd6cEntry->pIf6->u4Index, &TnlIfEntry) == CFA_FAILURE)
                {
                    return;
                }
                u4PhyIfIndex = TnlIfEntry.u4PhyIfIndex;
            }
#endif /* TUNNEL_WANTED */
            else
            {
                return;
            }

            if (CfaGetVlanId (u4PhyIfIndex, &u2VlanId) == CFA_FAILURE)
            {
                return;
            }
            if (i4ForwardStatus == IP6_FORW_DISABLE)
            {
                /* IP6 Forwarding is disabled, hence delete the ND Cache entries */
                i4OutCome = Ipv6FsNpIpv6NeighCacheDel (u4ContextId,
                                                       (UINT1 *) &pNd6cEntry->
                                                       addr6,
                                                       pNd6cEntry->pIf6->
                                                       u4Index);
            }
            else
            {
                /* IP6 Forwarding is enabled, hence add the ND Cache entries */
                switch (pNd6cEntry->u1ReachState)
                {
                    case ND6C_STATIC_NOT_IN_SERVICE:
                    case ND6C_INCOMPLETE:
                        if (ND6_IS_NP_PROGRAMMING_ALLOWED () == OSIX_TRUE)
                        {

                            i4OutCome = Ipv6FsNpIpv6NeighCacheAdd (u4ContextId,
                                                                   (UINT1 *)
                                                                   &pNd6cEntry->
                                                                   addr6,
                                                                   pNd6cEntry->
                                                                   pIf6->
                                                                   u4Index,
                                                                   NULL, 0,
                                                                   NP_IPV6_NH_INCOMPLETE,
                                                                   u2VlanId);
                        }
                        break;

                    case ND6C_STALE:
                        pNd6cEntry->u1HwStatus = NP_NOT_PRESENT;
                        pNd6cEntry->u1Sync = OSIX_TRUE;
                        if (ND6_GET_NODE_STATUS () == RM_ACTIVE)
                        {
                            Nd6RedAddDynamicInfo (pNd6cEntry,
                                                  ((tNd6CacheLanInfo *) (VOID
                                                                         *)
                                                   (pNd6cEntry->pNd6cInfo))->
                                                  lladdr, ND6_RED_DEL_CACHE);
                            Nd6RedSendDynamicCacheInfo ();
                        }

                        if (ND6_IS_NP_PROGRAMMING_ALLOWED () == OSIX_TRUE)
                        {

                            pNd6cEntry->u1HwStatus = NP_PRESENT;
                            pNd6cEntry->u1Sync = OSIX_TRUE;
                            i4OutCome = Ipv6FsNpIpv6NeighCacheAdd (u4ContextId,
                                                                   (UINT1 *)
                                                                   &pNd6cEntry->
                                                                   addr6,
                                                                   pNd6cEntry->
                                                                   pIf6->
                                                                   u4Index,
                                                                   ((tNd6CacheLanInfo *) (VOID *) (pNd6cEntry->pNd6cInfo))->lladdr, IP6_MAX_LLA_LEN, NP_IPV6_NH_STALE, u2VlanId);
                        }
                        break;

                    case ND6C_STATIC:
                    case ND6C_REACHABLE:
                        pNd6cEntry->u1Sync = OSIX_TRUE;
                        pNd6cEntry->u1HwStatus = NP_NOT_PRESENT;

                        if (ND6_GET_NODE_STATUS () == RM_ACTIVE)
                        {
                            Nd6RedAddDynamicInfo (pNd6cEntry,
                                                  ((tNd6CacheLanInfo *) (VOID
                                                                         *)
                                                   (pNd6cEntry->pNd6cInfo))->
                                                  lladdr, ND6_RED_ADD_CACHE);
                            Nd6RedSendDynamicCacheInfo ();
                        }
                        if (ND6_IS_NP_PROGRAMMING_ALLOWED () == OSIX_TRUE)
                        {
                            /*Get the port number with the vlan and mac-address
                             *and pass it for ARP addition so that for harware chipset
                             *the port info can be taken with this param*/

                            if (u2VlanId != 0)
                            {

                                i4OutCome =
                                    Ipv6FsNpIpv6NeighCacheAdd (u4ContextId,
                                                               (UINT1 *)
                                                               &pNd6cEntry->
                                                               addr6,
                                                               pNd6cEntry->
                                                               pIf6->u4Index,
                                                               ((tNd6CacheLanInfo *) (VOID *) (pNd6cEntry->pNd6cInfo))->lladdr, IP6_MAX_LLA_LEN, NP_IPV6_NH_REACHABLE, u2VlanId);
                            }
                        }
                        break;
                }
                if (i4OutCome == FNP_SUCCESS)
                {
                    pNd6cEntry->u1HwStatus = NP_PRESENT;

                    if (ND6_GET_NODE_STATUS () == RM_ACTIVE)
                    {
                        if (pNd6cEntry->u1Sync == OSIX_TRUE)
                        {
                            if (pNd6cEntry->u1ReachState == ND6C_STALE)
                            {
                                Nd6RedAddDynamicInfo (pNd6cEntry,
                                                      ((tNd6CacheLanInfo
                                                        *) (VOID
                                                            *) (pNd6cEntry->
                                                                pNd6cInfo))->
                                                      lladdr,
                                                      ND6_RED_DEL_CACHE);
                                Nd6RedSendDynamicCacheInfo ();
                            }
                            else
                            {
                                Nd6RedAddDynamicInfo (pNd6cEntry,
                                                      ((tNd6CacheLanInfo
                                                        *) (VOID
                                                            *) (pNd6cEntry->
                                                                pNd6cInfo))->
                                                      lladdr,
                                                      ND6_RED_ADD_CACHE);
                                Nd6RedSendDynamicCacheInfo ();
                            }
                            pNd6cEntry->u1Sync = OSIX_FALSE;
                        }
                    }
                }
            }
        }
        pNd6cEntryTmp = RBTreeGetNext (gNd6GblInfo.Nd6CacheTable, pNd6cEntry,
                                       NULL);
        pNd6cEntry = pNd6cEntryTmp;
    }                            /* End of while loop for all the neighbor cache entries */
    return;
#endif /* NPAPI_WANTED */
}

/*****************************************************************************
DESCRIPTION           Among the several responses received from Anycast 
                      destinations which are on-link, the best destination 
                      is selected else the cahce entry is purged
 

INPUTS                pNd6cEntry    -   Pointer to the Nd6 cache entry

OUTPUTS               None

RETURNS               IP6_SUCCESS  
                      IP6_FAILURE 
*****************************************************************************/

VOID
Nd6GetAnycastcache (tNd6CacheEntry * pNd6cEntry)
{
    tNd6CacheLanInfo   *pNd6cLinfo = NULL;
    UINT4               u4IfReachTime = 0;

    pNd6cLinfo = (tNd6CacheLanInfo *) (VOID *) pNd6cEntry->pNd6cInfo;

    if (pNd6cLinfo->u4Retries >=
        ((UINT4) pNd6cEntry->pIf6->pIp6Cxt->
         i4Nd6CacheMaxRetries - 1)
        && (pNd6cEntry->u1ReachState == ND6C_INCOMPLETE))
    {
        if (Nd6SelectBestAnycastDestination (pNd6cEntry) == IP6_SUCCESS)
        {
            Nd6CancelCacheTimer (pNd6cEntry);
            Nd6SetReachState (pNd6cEntry, ND6C_REACHABLE);
            Nd6SendPendQue (pNd6cEntry);
            Nd6UpdateLastUseTime (pNd6cEntry);

            u4IfReachTime = Ip6GetIfReachTime (pNd6cEntry->pIf6->u4Index);
            u4IfReachTime =
                Ip6Random ((UINT4) (u4IfReachTime * 0.5),
                           (UINT4) (u4IfReachTime * 1.5));

            Nd6SetMSecCacheTimer (pNd6cEntry, ND6_REACH_TIMER_ID,
                                  u4IfReachTime);
            OsixGetSysTime ((tOsixSysTime *) & (pNd6cEntry->u4LastUpdatedTime));
        }
        else
        {
            Nd6PurgeCache (pNd6cEntry);
        }
    }
}

/*****************************************************************************
* DESCRIPTION : Purges the cache entries which matches the interface
*               address till the mask. This util is helpful to clear 
*               the cache if any one particular Global IP is changed/removed
* 
* INPUTS      : u4Index  -Inteface index                   
*               pAddr   - Address for which corresponding cache to be cleaned 
*               u1PrefixLen   -  Prefix length for this IP Address
* 
* OUTPUTS     : None
* 
* RETURNS     : None
*****************************************************************************/
VOID
Nd6PurgeCacheForIP (UINT4 u4IfIndex, tIp6Addr * pAddr, UINT1 u1PrefixLen)
{
    tNd6CacheEntry     *pNd6cEntry = NULL;
    tNd6CacheEntry     *pNd6cEntryTmp = NULL;
    tNd6CacheEntry      Nd6cEntry;
    tIp6If             *pIf6 = NULL;

    pIf6 = Ip6ifGetEntry (u4IfIndex);

    if (!pIf6)
    {
        return;
    }

    MEMSET (&Nd6cEntry, 0, sizeof (tNd6CacheEntry));
    Nd6cEntry.pIf6 = pIf6;
    pNd6cEntry = RBTreeGetNext (gNd6GblInfo.Nd6CacheTable,
                                (tRBElem *) & Nd6cEntry, NULL);

    while (pNd6cEntry != NULL)
    {
        pNd6cEntryTmp = RBTreeGetNext (gNd6GblInfo.Nd6CacheTable,
                                       pNd6cEntry, NULL);

        if (pNd6cEntry->pIf6 == pIf6)
        {
            /*
             * Purge all or dynamic NDCache entries on an interface
             * based on the reachable cache for given IP
             */
            if ((pNd6cEntry->u1ReachState != ND6C_STATIC) &&
                (pNd6cEntry->u1ReachState != ND6C_STATIC_NOT_IN_SERVICE))
            {
                if (Ip6AddrMatch (pAddr, &pNd6cEntry->addr6, u1PrefixLen)
                    == TRUE)
                {
                    if (Nd6PurgeCache (pNd6cEntry) == IP6_FAILURE)
                    {
                        IP6_TRC_ARG1 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                                      MGMT_TRC | ALL_FAILURE_TRC, ND6_NAME,
                                      "ND6:Nd6PurgeCacheForIP: Purging Cache "
                                      "on IF %d Failed\n", pIf6->u4Index);
                    }
                }
            }
        }
        else
        {
            /* All the cache entries learnt on this interface have been verified
             * moving to the cache entries learnt on the next interface
             * Hence break the loop */
            break;
        }
        pNd6cEntry = pNd6cEntryTmp;
    }
    return;
}

/**************************************************************************/
/*   Function Name   : RipCreateSemaphore                          */
/*   Description     : This function create the semaphore                 */
/*   Input(s)        : None                                               */
/*   Output(s)       : None                                               */
/*   Return Value    : IP6_SUCCESS or IP6_FAILURE                         */
/**************************************************************************/

INT4
ND6CreateSemaphore (void)
{
    /* Create the semaphore identifier for the RIP task */
    if (OsixSemCrt (ND6_SEM_NAME, &gNd6SemId) != OSIX_SUCCESS)
    {
        return IP6_FAILURE;
    }
    OsixSemGive (gNd6SemId);
    return IP6_SUCCESS;
}

/**************************************************************************/
/*   Function Name   : ND6Lock                                          */
/*   Description     : This function takes the ND6 protocol semaphore  */
/*   Input(s)        : None                                               */
/*   Output(s)       : None                                               */
/*   Return Value    : IP6_SUCCESS or IP6_FAILURE                       */
/**************************************************************************/
INT4
ND6Lock (VOID)
{
    if (OsixSemTake (gNd6SemId) == OSIX_FAILURE)
    {
        return IP6_FAILURE;
    }
    return IP6_SUCCESS;
}

/**************************************************************************/
/*   Function Name   : ND6UnLock                                          */
/*   Description     : This function releases the ND6 protocol semaphore  */
/*   Input(s)        : None                                               */
/*   Output(s)       : None                                               */
/*   Return Value    : IP6_SUCCESS or IP6_FAILURE                       */
/**************************************************************************/
INT4
ND6UnLock (VOID)
{
    OsixSemGive (gNd6SemId);
    return IP6_SUCCESS;
}

#ifdef EVPN_VXLAN_WANTED
/*****************************************************************************
DESCRIPTION           Checks for whether a NDCache entry exists for an IPv6 
                      address and if present returns the entry, Otherwise NULL
 

INPUTS                pAddr6        -   Pointer to Destination IPv6 address

OUTPUTS               None

RETURNS               IP6_SUCCESS      -  Packet queued on NDCache entry
                      IP6_FAILURE  -  NDCache create fails
*****************************************************************************/
INT4
Nd6IsCacheForAddrLearntFromEvpn (tIp6Addr * pAddr6, tNd6CacheEntry * pNd6cEntry)
{
    tNd6CacheEntry     *pNd6cacheEntry = NULL;
    tNd6CacheEntry     *pNd6cEntryTmp = NULL;

    UNUSED_PARAM (pAddr6);
    pNd6cacheEntry = RBTreeGetFirst (gNd6GblInfo.Nd6CacheTable);

    while (pNd6cacheEntry != NULL)
    {
        if ((MEMCMP (&(pNd6cacheEntry->addr6), pAddr6, sizeof (tIp6Addr)) == 0)
            && (pNd6cacheEntry->u1ReachState == ND6C_EVPN))
        {
            break;
        }
        pNd6cEntryTmp =
            RBTreeGetNext (gNd6GblInfo.Nd6CacheTable, pNd6cacheEntry, NULL);
        pNd6cacheEntry = pNd6cEntryTmp;
    }
    if (pNd6cacheEntry != NULL)
    {
        MEMCPY (pNd6cEntry, pNd6cacheEntry, sizeof (tNd6CacheEntry));
        return IP6_SUCCESS;
    }
    return IP6_FAILURE;
}
#endif
/***************************** END OF FILE **********************************/
