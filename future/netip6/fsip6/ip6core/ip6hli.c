/*
 *----------------------------------------------------------------------------- 
 *    FILE NAME                    :    ip6hli.c
 *                                                                              
 *    PRINCIPAL AUTHOR             :    Murali Krishna
 *                                                                              
 *    SUBSYSTEM NAME               :    IPv6                                    
 *                                                                              
 *    MODULE NAME                  :    IP6 Module                            
 *                                                                              
 *    LANGUAGE                     :    C                                       
 *                                                                              
 *    TARGET ENVIRONMENT           :    UNIX                                    
 *                                                                              
 *    DATE OF FIRST RELEASE        :    Sep-09-2001                             
 *                                                                              
 *    DESCRIPTION                  :    This file contains C routines for       
 *                                      handling various higher layer interface
 *                    functionality.
 *    $Id: ip6hli.c,v 1.14 2015/10/10 11:03:19 siva Exp $
 *----------------------------------------------------------------------------- 
 * Revision History 
 * ----------------------------------------------------------------------------
 *  Date                | Reason for Change   |    Change Description
 *-----------------------------------------------------------------------------
 */

#include "ip6inc.h"

/*******************************************************************************
 * Function Name     :  Ip6RcvFromHl
 *
 * Description       :  This is a procedural interface used by higher layers
 *                      to send packets to IPv6. 
 *
 * Input(s)          :  pBuf - packet (segment) to be sent to IPv6
 *                      u4Length  - length of the packet 
 *                      Ip6
 * Output(s)         :  None.
 *
 * Returns           :  SUCCESS or FAILURE
 ******************************************************************************/

INT4
Ip6RcvFromHl (tCRU_BUF_CHAIN_HEADER * pBuf, tHlToIp6Params * pParams)
{
    tHlToIp6Params     *pHlParams = NULL;

    IP6_SET_COMMAND (pBuf, pParams->u1Cmd);

    if ((pHlParams = (tHlToIp6Params *) (VOID *)
         Ip6GetMem (pParams->u4ContextId,
                    (UINT2) gIp6GblInfo.i4ParamId)) == NULL)
    {
        IP6_TRC_ARG (pParams->u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                     "IP6:Ip6RcvFromHl: Send Dg,NoMem while Alloc for");
        IP6_TRC_ARG2 (pParams->u4ContextId, IP6_MOD_TRC,
                      DATA_PATH_TRC, IP6_NAME,
                      "HlToIp6Params fromPoolOfId=%d.So BufPtr=%p rel\n",
                      gIp6GblInfo.i4ParamId, pBuf);

        IP6_TRC_ARG4 (pParams->u4ContextId, IP6_MOD_TRC, MGMT_TRC, IP6_NAME,
                      "IP6:Ip6RcvFromHl: %s mem err: Type = %d  PoolId = %d Memptr = %p\n",
                      ERROR_FATAL_STR, ERR_MEM_GET, NULL,
                      gIp6GblInfo.i4ParamId);

        if (Ip6BufRelease (pParams->u4ContextId, pBuf, FALSE, IP6_MODULE) ==
            IP6_FAILURE)
        {
            IP6_TRC_ARG1 (pParams->u4ContextId, IP6_MOD_TRC,
                          BUFFER_TRC, IP6_NAME,
                          "IP6:Ip6RcvFromHl: send datagram, Failure in releasing buffer ptr = %p \n",
                          pBuf);

            IP6_TRC_ARG3 (pParams->u4ContextId, IP6_MOD_TRC,
                          BUFFER_TRC, IP6_NAME,
                          "IP6:Ip6RcvFromHl: %s buf err: Type = %d Bufptr = %p",
                          ERROR_FATAL_STR, ERR_BUF_RELEASE, pBuf);
        }
        return IP6_FAILURE;
    }

    MEMCPY (pHlParams, pParams, sizeof (tHlToIp6Params));

    Ip6SetParams ((tIp6Params *) pHlParams, pBuf);

    IP6_BUF_READ_OFFSET (pBuf) = 0;

    /* Enquing to IP6 from lower layer */
    if (OsixSendToQ (SELF,
                     (const UINT1 *) IP6_TASK_IP_INPUT_QUEUE,
                     pBuf, OSIX_MSG_URGENT) != OSIX_SUCCESS)
    {
        IP6_TRC_ARG1 (pParams->u4ContextId, IP6_MOD_TRC, BUFFER_TRC, IP6_NAME,
                      "IP6:Ip6RcvFromHl: send to IP6_QUE is failed, releasing buffer ptr = %p \n",
                      pBuf);

        if (Ip6RelMem
            (pParams->u4ContextId, (UINT2) gIp6GblInfo.i4ParamId,
             (UINT1 *) pHlParams) == IP6_FAILURE)
        {
            IP6_TRC_ARG (pParams->u4ContextId, IP6_MOD_TRC,
                         BUFFER_TRC, IP6_NAME,
                         "IP6:Ip6RcvFromHl: Ip6RelMem Failure\n ");
            return IP6_FAILURE;
        }

        if (Ip6BufRelease (pParams->u4ContextId, pBuf, FALSE, IP6_MODULE) ==
            IP6_FAILURE)
        {
            IP6_TRC_ARG1 (pParams->u4ContextId, IP6_MOD_TRC,
                          BUFFER_TRC, IP6_NAME,
                          "IP6:Ip6RcvFromHl: send datagram, Failure in releasing buffer ptr = %p \n",
                          pBuf);

            IP6_TRC_ARG3 (pParams->u4ContextId, IP6_MOD_TRC,
                          BUFFER_TRC, IP6_NAME,
                          "IP6:Ip6RcvFromHl: %s buf err: Type = %d Bufptr = %p",
                          ERROR_FATAL_STR, ERR_BUF_RELEASE, pBuf);
        }
        return (IP6_FAILURE);
    }

    if (OsixSendEvent (SELF, (const UINT1 *) IP6_TASK_NAME,
                       IP6_DGRAM_RECD_EVENT) != OSIX_SUCCESS)
    {
        IP6_TRC_ARG1 (pParams->u4ContextId, IP6_MOD_TRC, BUFFER_TRC, IP6_NAME,
                      "IP6:Ip6RcvFromHl: send event to IP6_QUE is failed,releasing buffer ptr = %p \n",
                      pBuf);
        return (IP6_FAILURE);
    }
    return (IP6_SUCCESS);
}

/*******************************************************************************
 * Function Name     :  Ip6RcvIcmpErrorPkt
 *
 * Description       :  This is a procedural interface used by other 
 *                      (higher /IPv4) layers to send ICMP error packets 
 *                to the source of the error packet.
 *
 * Input(s)          :  pBuf - packet (segment) to be sent to IPv6
 *                      pIcmpParams - ICMP parameters
 * Output(s)         :  None.
 *
 * Returns           :  SUCCESS or FAILURE
 ******************************************************************************/

VOID
Ip6RcvIcmpErrorPkt (tCRU_BUF_CHAIN_HEADER * pBuf, tIcmp6Params * pIcmpParams)
{
    tIcmp6Params       *pParams = NULL;

    IP6_SET_COMMAND (pBuf, IP6_ICMPERR_DATA);

    if ((pParams =
         (tIcmp6Params *) (VOID *) Ip6GetMem (pIcmpParams->u4ContextId,
                                              (UINT2) gIp6GblInfo.i4ParamId)) ==
        NULL)
    {
        IP6_TRC_ARG (pIcmpParams->u4ContextId, IP6_MOD_TRC, BUFFER_TRC,
                     IP6_NAME,
                     "IP6:Ip6RcvIcmpErrorPkt: Buffer for passing params could "
                     "not be allocated\n");
        Ip6BufRelease (pIcmpParams->u4ContextId, pBuf, FALSE, PING6_MODULE);
        return;
    }

    MEMCPY (pParams, pIcmpParams, sizeof (tIcmp6Params));

    Ip6SetParams ((tIp6Params *) (VOID *) pParams, pBuf);
    IP6_BUF_READ_OFFSET (pBuf) = 0;

    if (OsixSendToQ (SELF, (const UINT1 *) IP6_TASK_IP_INPUT_QUEUE, pBuf,
                     OSIX_MSG_URGENT) != OSIX_SUCCESS)
    {
        IP6_TRC_ARG1 (pIcmpParams->u4ContextId, IP6_MOD_TRC, BUFFER_TRC,
                      IP6_NAME,
                      "IP6:Ip6RcvIcmpErrorPkt: Pkt could not be enqueued to "
                      "the IP6 task Bufptr = %p\n", pBuf);
        IP6_TRC_ARG3 (pIcmpParams->u4ContextId, IP6_MOD_TRC, BUFFER_TRC,
                      IP6_NAME,
                      "IP6:Ip6RcvIcmpErrorPkt: %s buf err: Type = %d Bufptr = %p",
                      ERROR_FATAL_STR, ERR_GEN_TQUEUE_FAIL, pBuf);
        Ip6RelMem (pIcmpParams->u4ContextId, (UINT2) gIp6GblInfo.i4ParamId,
                   (UINT1 *) pParams);
        Ip6BufRelease (pIcmpParams->u4ContextId, pBuf, FALSE, IP6_MODULE);
        return;
    }

    if (OsixSendEvent
        (SELF, (const UINT1 *) IP6_TASK_NAME,
         IP6_DGRAM_RECD_EVENT) != OSIX_SUCCESS)
    {
        IP6_TRC_ARG1 (pIcmpParams->u4ContextId, IP6_MOD_TRC, BUFFER_TRC,
                      IP6_NAME,
                      "IP6:Ip6RcvIcmpErrorPkt: Event could not be sent to the "
                      "IP6 task Bufptr = %p\n", pBuf);
        IP6_TRC_ARG3 (pIcmpParams->u4ContextId, IP6_MOD_TRC, BUFFER_TRC,
                      IP6_NAME,
                      "IP6:Ip6RcvIcmpErrorPkt: %s buf err: Type = %d Bufptr = %p",
                      ERROR_FATAL_STR, ERR_GEN_TQUEUE_FAIL, pBuf);
        Ip6RelMem (pIcmpParams->u4ContextId, (UINT2) gIp6GblInfo.i4ParamId,
                   (UINT1 *) pParams);
        Ip6BufRelease (pIcmpParams->u4ContextId, pBuf, FALSE, IP6_MODULE);
        return;
    }
    return;
}

/******************************************************************************
 * DESCRIPTION : This function intimates the status of all the IP6 
 *             : interfaces as soon as the higher layer protocol
 *             : register or deregister with IP6
 * INPUTS      : u4RegStatus - IP6_REGISTER or IP6_DEREGISTER
 *               u4Proto     - Protocol ID
 *               u4Mask      - Registration Mask
 * OUTPUTS     : None
 * RETURNS     : None
 ******************************************************************************/
INT4
Ip6RouteChgRegHandler (tIp6RtEntry * pIp6RtEntry, VOID *pAppSpecData)
{
    tNetIpv6RtInfo      NetIpv6RtInfo;
    UINT4               u4Proto = *(UINT4 *) pAppSpecData;

    /* Fill the Route information. */
    MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));
    MEMCPY (&NetIpv6RtInfo.Ip6Dst, &pIp6RtEntry->dst, sizeof (tIp6Addr));
    NetIpv6RtInfo.u1Prefixlen = pIp6RtEntry->u1Prefixlen;
    MEMCPY (&NetIpv6RtInfo.NextHop, &pIp6RtEntry->nexthop, sizeof (tIp6Addr));
    NetIpv6RtInfo.u4Index = pIp6RtEntry->u4Index;
    NetIpv6RtInfo.u4Metric = pIp6RtEntry->u4Metric;
    NetIpv6RtInfo.i1Proto = pIp6RtEntry->i1Proto;
    NetIpv6RtInfo.i1DefRtrFlag = pIp6RtEntry->i1DefRtrFlag;
    NetIpv6RtInfo.u4RowStatus = IP6FWD_ACTIVE;
    NetIpv6RtInfo.u2ChgBit = IP6_BIT_ALL;

    NetIpv6InvokeRouteChange (&NetIpv6RtInfo, u4Proto);
    return RTM6_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : This function intimates the status of all the IP6 
 *             : interfaces as soon as the higher layer protocol
 *             : register or deregister with IP6
 * INPUTS      : u4RegStatus - IP6_REGISTER or IP6_DEREGISTER
 *               u4Proto     - Protocol ID
 *               u4Mask      - Registration Mask
 * OUTPUTS     : None
 * RETURNS     : None
 ******************************************************************************/

VOID
Ip6ActOnHLRegDereg (UINT4 u4ContextId, UINT4 u4RegStatus,
                    UINT4 u4Proto, UINT4 u4Mask)
{
    UINT4               u4Index = 1;

    if (u4RegStatus == IP6_REGISTER)
    {
        if (u4Mask & NETIPV6_INTERFACE_PARAMETER_CHANGE)
        {
            IP6_IF_SCAN (u4Index, (UINT4) (IP6_MAX_LOGICAL_IF_INDEX))
            {
                if ((gIp6GblInfo.apIp6If[u4Index] == NULL) ||
                    (gIp6GblInfo.apIp6If[u4Index]->u1AdminStatus
                     == ADMIN_INVALID) ||
                    (gIp6GblInfo.apIp6If[u4Index]->pIp6Cxt->u4ContextId
                     != u4ContextId))
                {
                    continue;
                }
                switch (gIp6GblInfo.apIp6If[u4Index]->u1OperStatus)
                {
                    case OPER_UP:
                        IP6_TRC_ARG (u4ContextId, IP6_MOD_TRC,
                                     CONTROL_PLANE_TRC, IP6_NAME,
                                     "NETIP6 : Invoke Interface Change - "
                                     "ADMIN UP\n");
                        NetIpv6InvokeInterfaceStatusChange
                            (u4Index, u4Proto, NETIPV6_IF_UP,
                             NETIPV6_INTERFACE_STATUS_CHANGE);
                        break;

                    default:
                        break;
                }
            }
        }

        if (u4Mask & NETIPV6_ROUTE_CHANGE)
        {
            tIp6Addr            SrcAddr;
            tIp6Addr            DestAddr;
            UINT1               u1SrcPrefixLen = 0;
            UINT1               u1DstPrefixLen = 0;

            IP6_TRC_ARG (u4ContextId, IP6_MOD_TRC, CONTROL_PLANE_TRC, IP6_NAME,
                         "NETIP6 : Processing Route Change Registriation\n");
            MEMSET (&SrcAddr, 0, sizeof (tIp6Addr));
            MEMSET (&DestAddr, 0, sizeof (tIp6Addr));

            IP6_TASK_UNLOCK ();
            Rtm6ApiScanRouteTableForBestRouteInCxt (u4ContextId, &SrcAddr,
                                                    u1SrcPrefixLen,
                                                    Ip6RouteChgRegHandler, 0,
                                                    &DestAddr, &u1DstPrefixLen,
                                                    (VOID *) &u4Proto);
            IP6_TASK_LOCK ();
        }
    }
    return;
}

/*******************************************************************************
 * Function Name     :  Ip6RcvMcastPktFromHl
 *
 * Description       :  This is a procedural interface used by higher layers
 *                      to send Multicast packets to IPv6. 
 *
 * Input(s)          :  pBuf - packet (segment) to be sent to IPv6
 *                      pMcastParams - ip6 Mulitcastparams
 * Output(s)         :  None.
 *
 * Returns           :  SUCCESS or FAILURE
 ******************************************************************************/

INT4
Ip6RcvMcastPktFromHl (tCRU_BUF_CHAIN_HEADER * pBuf,
                      tHlToIp6McastParams * pMcastParams)
{
    tHlToIp6McastParams *pHlMcastParams = NULL;
    UINT4               u4ContextId = 0;

    IP6_SET_COMMAND (pBuf, pMcastParams->u1Cmd);

    /* In tHlToIp6McastParams the pu4OIfList array contains
     * the mcast packet incoming interface in pMcastParams->pu4OIfList in the
     * index 1, followed by the outgoing interfaces.
     * The context associated with the incoming interface
     * is used for context based debugging */
    Ip6GetCxtId (pMcastParams->pu4OIfList[IP6_ONE], &u4ContextId);

    if ((pHlMcastParams =
         (tHlToIp6McastParams *) (VOID *) Ip6GetMem (u4ContextId,
                                                     (UINT2) gIp6GblInfo.
                                                     i4ParamId)) == NULL)
    {
        IP6_TRC_ARG (u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                     "IP6:Ip6RcvFromHl: Send Dg,NoMem while Alloc for");
        IP6_TRC_ARG2 (u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                      "HlToIp6Params fromPoolOfId=%d.So BufPtr=%p rel\n",
                      gIp6GblInfo.i4ParamId, pBuf);

        IP6_TRC_ARG4 (u4ContextId, IP6_MOD_TRC, MGMT_TRC, IP6_NAME,
                      "IP6:Ip6RcvFromHl: %s mem err: Type = %d  PoolId = %d Memptr = %p\n",
                      ERROR_FATAL_STR, ERR_MEM_GET, NULL,
                      gIp6GblInfo.i4ParamId);

        if (Ip6BufRelease (u4ContextId, pBuf, FALSE, IP6_MODULE) == IP6_FAILURE)
        {
            IP6_TRC_ARG1 (u4ContextId, IP6_MOD_TRC, BUFFER_TRC, IP6_NAME,
                          "IP6:Ip6RcvFromHl: send datagram, Failure in releasing buffer ptr = %p \n",
                          pBuf);

            IP6_TRC_ARG3 (u4ContextId, IP6_MOD_TRC, BUFFER_TRC, IP6_NAME,
                          "IP6:Ip6RcvFromHl: %s buf err: Type = %d Bufptr = %p",
                          ERROR_FATAL_STR, ERR_BUF_RELEASE, pBuf);
        }
        return IP6_FAILURE;
    }

    MEMCPY (pHlMcastParams, pMcastParams, sizeof (tHlToIp6McastParams));

    Ip6SetParams ((tIp6Params *) pHlMcastParams, pBuf);

    IP6_BUF_READ_OFFSET (pBuf) = 0;

    /* Enquing to IP6 from lower layer */
    if (OsixSendToQ (SELF,
                     (const UINT1 *) IP6_TASK_IP_INPUT_QUEUE,
                     pBuf, OSIX_MSG_URGENT) != OSIX_SUCCESS)
    {
        IP6_TRC_ARG1 (u4ContextId, IP6_MOD_TRC, BUFFER_TRC, IP6_NAME,
                      "IP6:Ip6RcvFromHl: send to IP6_QUE is failed, releasing buffer ptr = %p \n",
                      pBuf);

        if (Ip6RelMem
            (u4ContextId, (UINT2) gIp6GblInfo.i4ParamId,
             (UINT1 *) pHlMcastParams) == IP6_FAILURE)
        {
            IP6_TRC_ARG (u4ContextId, IP6_MOD_TRC, BUFFER_TRC, IP6_NAME,
                         "IP6:Ip6RcvFromHl: Ip6RelMem Failure\n ");
            return IP6_FAILURE;
        }

        if (Ip6BufRelease (u4ContextId, pBuf, FALSE, IP6_MODULE) == IP6_FAILURE)
        {
            IP6_TRC_ARG1 (u4ContextId, IP6_MOD_TRC, BUFFER_TRC, IP6_NAME,
                          "IP6:Ip6RcvFromHl: send datagram, Failure in releasing buffer ptr = %p \n",
                          pBuf);

            IP6_TRC_ARG3 (u4ContextId, IP6_MOD_TRC, BUFFER_TRC, IP6_NAME,
                          "IP6:Ip6RcvFromHl: %s buf err: Type = %d Bufptr = %p",
                          ERROR_FATAL_STR, ERR_BUF_RELEASE, pBuf);
        }
        return (IP6_FAILURE);
    }

    if (OsixSendEvent (SELF, (const UINT1 *) IP6_TASK_NAME,
                       IP6_DGRAM_RECD_EVENT) != OSIX_SUCCESS)
    {
        IP6_TRC_ARG1 (u4ContextId, IP6_MOD_TRC, BUFFER_TRC, IP6_NAME,
                      "IP6:Ip6RcvFromHl: send event to IP6_QUE is failed,releasing buffer ptr = %p \n",
                      pBuf);
        return (IP6_FAILURE);
    }
    return (IP6_SUCCESS);
}
