/*******************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: udp6.c,v 1.19 2015/06/17 04:58:56 siva Exp $
 *
 * Description: This file contains C routines for  
 *              handling user datagram packets.
 *        
 *******************************************************************/

#include "ip6inc.h"

/*
 * Local function prototypes
 */

PRIVATE UINT2       Udp6GetFreePort (VOID);

/****************************************************************************
 * DESCRIPTION : This routine initializes port control blocks used by the
 *               application.
 *
 * INPUTS      : None
 *
 * OUTPUTS     : None
 *
 * RETURNS     : IP6_SUCCESS - if memory is allocated for UDP6_MAX_PORT number of 
 *                         control blocks
 *               IP6_FAILURE - otherwise
 *
 * NOTES       : All the control blocks are assigned port number value zero
 *               which indicate that no application is attached to this port.
 *****************************************************************************/

INT4
Udp6Init ()
{
    TMO_SLL_Init (&(gIp6GblInfo.Udp6CbEntry));

    return IP6_SUCCESS;
}

/****************************************************************************
 * DESCRIPTION : This routine enrols an application to UDP port. Only after
 *               this call application can be allowed to send data through
 *               UDP port.
 *
 * INPUTS      : Port number                 (UINT2 u2UPort)  &
 *               Application task identifier (UINT2 u2_task_id) &
 *               Queue identifier            (UINT2 u2_rcv_q_id) &
                 Context Identifier          (UINT4 u4ContextId) &
                 Socket Mode                 (UINT1 u1FdMode)
 *                 
 * OUTPUTS     : None
 *
 * RETURNS     : IP6_SUCCESS - if application is attached to one of the control 
 *                         blocks
 *               IP6_FAILURE - otherwise
 * NOTES       : Presently only RIP6 can be enrolled as an application
 *****************************************************************************/
INT4
Udp6OpenInCxt (u4ContextId, u1FdMode, i4SockDesc, pu2UPort, au1TaskName,
               au1RcvQName, pIp6Addr, pu2RPort, pIp6RemAddr, fn)
     UINT1               u1FdMode;
     UINT4               u4ContextId;
     INT4                i4SockDesc;
     UINT2              *pu2UPort;
     UINT1               au1TaskName[UDP6_MAX_TASK_Q_NAME_LEN];
     UINT1               au1RcvQName[UDP6_MAX_TASK_Q_NAME_LEN];
     tIp6Addr           *pIp6Addr;
     UINT2              *pu2RPort;
     tIp6Addr           *pIp6RemAddr;
     VOID                (*fn);
{
    UINT1               u1Udp6CBMode = 0;
    tUdp6Port          *pUdp6CBEntry = NULL;

    IP6_TASK_LOCK ();
    if (Udp6GetPortCBInCxt (u4ContextId, *pu2UPort, pIp6Addr,
                            &pUdp6CBEntry, &u1Udp6CBMode) == IP6_SUCCESS)
    {
        IP6_TRC_ARG1 (u4ContextId, UDP6_MOD_TRC, DATA_PATH_TRC, UDP6_NAME,
                      "UDP6:Udp6Open: Port %d is already opened\n", *pu2UPort);
        IP6_TASK_UNLOCK ();
        return IP6_FAILURE;
    }

    pUdp6CBEntry =
        (tUdp6Port *) (VOID *) Ip6GetMem (u4ContextId,
                                          gIp6GblInfo.i4Udp6CBPoolId);

    if (pUdp6CBEntry != NULL)
    {
        MEMSET (pUdp6CBEntry, 0, sizeof (tUdp6Port));
        TMO_SLL_Init_Node (&(pUdp6CBEntry->NextCBNode));
        if (au1TaskName != NULL)
            MEMCPY (pUdp6CBEntry->au1TaskName, au1TaskName, 4);
        if (au1RcvQName != NULL)
            MEMCPY (pUdp6CBEntry->au1RcvQName, au1RcvQName, 4);
        if (*pu2UPort == 0)
            *pu2UPort = Udp6GetFreePort ();
        pUdp6CBEntry->u2UPort = *pu2UPort;
        pUdp6CBEntry->i4SockDesc = i4SockDesc;
        pUdp6CBEntry->rcv_fnc = (VOID (*)(UINT4 u4CxtId,
                                          tIp6Addr srcAddr,
                                          tIp6Addr dstAddr,
                                          UINT2 u2SrcPort,
                                          UINT2 u2DstPort,
                                          tCRU_BUF_CHAIN_HEADER * pBuf,
                                          UINT4 u4Index,
                                          UINT4 u4Len, UINT1 u1HLim)) fn;

        if (pIp6Addr != NULL)
        {
            Ip6AddrCopy (&pUdp6CBEntry->addr, pIp6Addr);
        }
        else
        {
            SET_ADDR_UNSPECIFIED (pUdp6CBEntry->addr);
        }
        pUdp6CBEntry->i4LocalAddrType = UDP_IPV6_ADDR_TYPE;
        pUdp6CBEntry->i4RemoteAddrType = UDP_IPV6_ADDR_TYPE;
        if (pIp6RemAddr != NULL)
        {
            Ip6AddrCopy (&pUdp6CBEntry->Remote_addr, pIp6RemAddr);
        }
        else
        {
            SET_ADDR_UNSPECIFIED (pUdp6CBEntry->Remote_addr);
        }
        pUdp6CBEntry->u2Remote_port = *pu2RPort;
        pUdp6CBEntry->u4Instance = UDP6_ZERO;
        UDP6_STAT_NUM_PORT_INC (u1FdMode, u4ContextId);

        if (u1FdMode == SOCK_UNIQUE_MODE)
        {
            /* Create UDP6 control block in context specific 
             * table incase of unique sock mode ie.,
             * this control block is specific to a context */
            if (Udp6AddCBNodeToCxtTable (u4ContextId, pUdp6CBEntry)
                == IP6_FAILURE)
            {
                MemReleaseMemBlock (gIp6GblInfo.i4Udp6CBPoolId,
                                    (UINT1 *) pUdp6CBEntry);
                IP6_TASK_UNLOCK ();
                return IP6_FAILURE;
            }
        }
        else
        {
            /* Create UDP6 control block in global UDP
             * table incase of global sock mode ie.,
             * this control block is common for all contexts */
            Udp6AddCBNodeToGlbTable (pUdp6CBEntry);
        }
    }
    else
    {
        IP6_TRC_ARG (u4ContextId, UDP6_MOD_TRC, ALL_FAILURE_TRC, UDP6_NAME,
                     "UDP6:Udp6Init: Memory allocation for Port records failed\n");
        IP6_TASK_UNLOCK ();
        return IP6_FAILURE;
    }
    IP6_TASK_UNLOCK ();
    KW_FALSEPOSITIVE_FIX (pUdp6CBEntry);
    return IP6_SUCCESS;
}

/****************************************************************************
 * DESCRIPTION : Cancels the enrolment of an application to UDP port. If it is
                 not enrolled returns IP6_FAILURE;
 *
 * INPUTS      : Port number               (UINT2 u2UPort)
                 Context Identifier        (UINT4 u4ContextId)
                 Pointer to Ip6Addr        (tIp6Addr *pIp6Addr)
 *
 * OUTPUTS     : None
 *
 * RETURNS     : IP6_SUCCESS - If enrolment is cancelled.
 *               IP6_FAILURE - Otherwise
 * NOTES       :
 *****************************************************************************/

INT4
Udp6CloseInCxt (UINT4 u4ContextId, UINT2 u2UPort, tIp6Addr * pIp6Addr)
{
    tUdp6Port          *pUdp6CBEntry = NULL;
    UINT1               u1Udp6CBMode = 0;

    IP6_TRC_ARG2 (u4ContextId, UDP6_MOD_TRC, DATA_PATH_TRC, UDP6_NAME,
                  "UDP6:Udp6Init: Closing UDP Port %d IPv6 Address = %s\n",
                  u2UPort, Ip6PrintAddr (pIp6Addr));
    if (u2UPort == 0)
    {
        return IP6_SUCCESS;
    }

    IP6_TASK_LOCK ();
    if (Udp6GetPortCBInCxt (u4ContextId, u2UPort, pIp6Addr,
                            &pUdp6CBEntry, &u1Udp6CBMode) == IP_SUCCESS)
    {
        if (pUdp6CBEntry == NULL)
        {
            IP6_TASK_UNLOCK ();
            return IP6_FAILURE;
        }
        if (u1Udp6CBMode == SOCK_UNIQUE_MODE)
        {
            /* Delete the control block from UDP context table */
            if (Udp6DeleteCBNodeFromCxtTable (u4ContextId, pUdp6CBEntry)
                == IP6_SUCCESS)
            {
                UDP6_STAT_NUM_PORT_DEC (u1Udp6CBMode, u4ContextId);
                IP6_TASK_UNLOCK ();
                return IP6_SUCCESS;
            }
        }
        else
        {
            /* Delete the control block from UDP global table */
            if (Udp6DeleteCBNodeFromGlbTable (pUdp6CBEntry) == IP6_SUCCESS)
            {
                UDP6_STAT_NUM_PORT_DEC (u1Udp6CBMode, u4ContextId);
                IP6_TASK_UNLOCK ();
                return IP6_SUCCESS;
            }
        }
    }
    IP6_TASK_UNLOCK ();
    return IP6_FAILURE;
}

/****************************************************************************
 * DESCRIPTION : This routine is called when a UDP6 datagram is to be sent.
 *               It constructs the UDP header from parameters passed by the
 *               application through parameter structure, attaches it to
 *               data and send it to IP6 layer. The Application over UDP6
 *               could use Socket Layer to enque packets to UDP6.              
 *
 * INPUTS      : parameters structure (t_RIP6_UDP6_PARAM     *pParam) &
 *               application data     (tCRU_BUF_CHAIN_HEADER *pBuf)
                 Context Identifier          (UINT4 u4ContextId)
 *
 * OUTPUTS     : None
 *
 * RETURNS     : None
 *
 * NOTES       : Presently only RIP6 is enqueuing its data to UDP port
 *****************************************************************************/

INT4
Udp6SendInCxt (UINT4 u4ContextId, UINT2 u2SrcPort, UINT2 u2DstPort,
               UINT4 u4Len, UINT2 u2Index,
               tIp6Addr * pSrc, tIp6Addr * pDst, tCRU_BUF_CHAIN_HEADER * pBuf,
               UINT1 u1Hlim)
{
    tIp6If             *pIf6 = NULL;
    tIp6Cxt            *pIp6Cxt = NULL;
    UINT1               u1Copy = 0;
    tUdp6Hdr            udpHdr, *p_udp = NULL;
    tIp6Addr           *pIp6SrcAddr = NULL;
    tUdp6Port          *pUdp6CBEntry = NULL;
    UINT1               u1Udp6CBMode = 0;
    INT1                i1AddrType = 0;	

    if (IP6_TASK_LOCK () == SNMP_FAILURE)
    {
        IP6_TRC_ARG (u4ContextId, UDP6_MOD_TRC, DATA_PATH_TRC, UDP6_NAME,
                     "UDP6: IP6 Task Lock Failed \n");
        IP6_TRC_ARG3 (u4ContextId, UDP6_MOD_TRC, BUFFER_TRC, IP6_NAME,
                      "%s buf err: Type = %d Bufptr = %p", ERROR_FATAL_STR,
                      ERR_BUF_WRITE, pBuf);
        Ip6BufRelease (u4ContextId, pBuf, FALSE, UDP6_MODULE);
        return IP6_FAILURE;
    }

    if ((u2DstPort == 0) || (IS_ADDR_UNSPECIFIED (*pDst)))
    {
        IP6_TRC_ARG (u4ContextId, UDP6_MOD_TRC, DATA_PATH_TRC, UDP6_NAME,
                     "UDP6: Invalid Destination \n");
        IP6_TRC_ARG3 (u4ContextId, UDP6_MOD_TRC, BUFFER_TRC, IP6_NAME,
                      "%s buf err: Type = %d Bufptr = %p", ERROR_FATAL_STR,
                      ERR_BUF_WRITE, pBuf);
        Ip6BufRelease (u4ContextId, pBuf, FALSE, UDP6_MODULE);
        IP6_TASK_UNLOCK ();
        return IP6_FAILURE;
    }

    p_udp = Ip6BufPptr (pBuf, (UINT1 *) &udpHdr, sizeof (tUdp6Hdr), &u1Copy);

    if (p_udp != NULL)
    {
        /* Transfer data without binding to a particular port */
        if (u2SrcPort == 0)
        {
            u2SrcPort = Udp6GetFreePort ();
        }
        p_udp->u2SrcPort = HTONS (u2SrcPort);
        p_udp->u2DstPort = HTONS (u2DstPort);

        if (u4Len > JMB_MIN_PAYLOAD_LEN)
        {
            p_udp->u2Len = 0;
        }
        else
        {
            p_udp->u2Len = HTONS (((UINT2) u4Len + sizeof (tUdp6Hdr)));
        }
        p_udp->u2Chksum = 0;

    }
    else
    {
        IP6_TRC_ARG (u4ContextId, UDP6_MOD_TRC, BUFFER_TRC, UDP6_NAME,
                     "UDP6:Udp6Send: Buffer for UDP header could not be allocated\n");
        IP6_TRC_ARG3 (u4ContextId, UDP6_MOD_TRC, BUFFER_TRC, IP6_NAME,
                      "UDP6:Udp6Send: %s buf err: Type = %d Bufptr = %p",
                      ERROR_FATAL_STR, ERR_BUF_PPTR, pBuf);
        Ip6BufRelease (u4ContextId, pBuf, FALSE, UDP6_MODULE);
        IP6_TASK_UNLOCK ();
        return IP6_FAILURE;
    }

    if ((IS_ADDR_LLOCAL (*pDst)) && (u2Index > 0) &&
        (u2Index <= (UINT2) IP6_MAX_LOGICAL_IF_INDEX))
    {
        if ((IS_ADDR_UNSPECIFIED (*pSrc)) || (IS_ADDR_LLOCAL (*pSrc) == 0))
        {
            /* Destination address is Link-Local. Source address either
             * not specified or not a link-local address. In this case,
             * update the source address as the link-local address.
             */
            pIp6SrcAddr = Ip6GetLlocalAddr (u2Index);
            if (pIp6SrcAddr == NULL)
            {
                IP6_TRC_ARG (u4ContextId, UDP6_MOD_TRC, DATA_PATH_TRC,
                             UDP6_NAME,
                             "UDP6: Udp6Send:Active Interface Link-Local "
                             "Address not present\n");
                IP6_TRC_ARG3 (u4ContextId, UDP6_MOD_TRC, BUFFER_TRC, IP6_NAME,
                              "%s buf err: Type = %d Bufptr = %p",
                              ERROR_FATAL_STR, ERR_BUF_WRITE, pBuf);
                Ip6BufRelease (u4ContextId, pBuf, FALSE, UDP6_MODULE);
                IP6_TASK_UNLOCK ();
                return IP6_FAILURE;
            }
            MEMCPY (pSrc, pIp6SrcAddr, sizeof (tIp6Addr));
        }
    }

    if ((IS_ADDR_UNSPECIFIED (*pSrc))
        && ((Ip6SrcAddrForDestAddrInCxt (u4ContextId, pDst, pSrc)) ==
            IP_FAILURE))
    {
        IP6_TRC_ARG (u4ContextId, UDP6_MOD_TRC, DATA_PATH_TRC, UDP6_NAME,
                     "UDP6: Udp6Send:Destination Unreachable\n");
        IP6_TRC_ARG3 (u4ContextId, UDP6_MOD_TRC, BUFFER_TRC, IP6_NAME,
                      "%s buf err: Type = %d Bufptr = %p", ERROR_FATAL_STR,
                      ERR_BUF_WRITE, pBuf);
        Ip6BufRelease (u4ContextId, pBuf, FALSE, UDP6_MODULE);
        IP6_TASK_UNLOCK ();
        return IP6_FAILURE;
    }

    if (Ip6BufWrite (pBuf, (UINT1 *) p_udp, IP6_BUF_WRITE_OFFSET (pBuf),
                     sizeof (tUdp6Hdr), u1Copy) == IP6_FAILURE)
    {
        IP6_TRC_ARG (u4ContextId, UDP6_MOD_TRC, DATA_PATH_TRC, UDP6_NAME,
                     "UDP6: Udp6Send: Hdr could not be written in pkt\n");
        IP6_TRC_ARG3 (u4ContextId, UDP6_MOD_TRC, BUFFER_TRC, IP6_NAME,
                      "UDP6: Udp6Send: %s buf err: Type = %d Bufptr = %p",
                      ERROR_FATAL_STR, ERR_BUF_WRITE, pBuf);
        Ip6BufRelease (u4ContextId, pBuf, FALSE, UDP6_MODULE);
        IP6_TASK_UNLOCK ();
        return IP6_FAILURE;
    }

    pIf6 = Ip6ifGetEntry (u2Index);
    if (pIf6 == NULL)
    {
        pIp6Cxt = Ipv6UtilGetCxtEntryFromCxtId (u4ContextId);
    }
    else
    {
        pIp6Cxt = pIf6->pIp6Cxt;
    }

    Udp6GetPortCBInCxt (u4ContextId, u2SrcPort, pSrc,
                        &pUdp6CBEntry, &u1Udp6CBMode);
    UDP6_STAT_OUT_INC (u1Udp6CBMode, u4ContextId);
    UDP6_STAT_HC_OUT_INC (u1Udp6CBMode, u4ContextId);

    IP6_TRC_ARG6 (u4ContextId, UDP6_MOD_TRC, DATA_PATH_TRC, UDP6_NAME,
                  "UDP6:Udp6Send:TX Src =%s Dst =%s Src Port =%d Dst Port =%d Len = %d Buf = %p\n",
                  Ip6PrintAddr (pSrc), Ip6PrintAddr (pDst), u2SrcPort,
                  u2DstPort, u4Len + sizeof (tUdp6Hdr), pBuf);

    IP6_TRC_ARG (u4ContextId, UDP6_MOD_TRC, DATA_PATH_TRC, UDP6_NAME,
                 "UDP6:Udp6Send: The Output packet is : \n");
    IP6_PKT_DUMP (u4ContextId, UDP6_MOD_TRC, DUMP_TRC, UDP6_NAME, pBuf, 0);
    if (NULL != pIp6Cxt)
    {
        i1AddrType = Ip6AddrType (pDst);
        if ((i1AddrType == ADDR6_MULTI) || (i1AddrType == ADDR6_LLOCAL))
        {
            Ip6SendInCxt (pIp6Cxt, pIf6, pSrc, pDst,
                          HTONS (p_udp->u2Len), NH_UDP6, pBuf, u1Hlim);
        }						  
        else
		{
            Ip6SendInCxt (pIp6Cxt, NULL, pSrc, pDst,
                          HTONS (p_udp->u2Len), NH_UDP6, pBuf, u1Hlim);
        }		
    }
    IP6_TASK_UNLOCK ();
    return IP6_SUCCESS;
}

/****************************************************************************
 * DESCRIPTION : This routine is called by the IP layer, whenever it gets one
 *               UDP datagram. It extract out UDP header from the packet and
 *               checks on which port this packet is to be sent. If this port
 *               service is available packet is enqueued to the destination 
 *               task otherwise ICMP6 destination unreachable message is sent
 *               back to the source.
 *
 * INPUTS      : interface pointer  (tIp6If              *pIf6)    & 
 *               IPv6 main header   (tIp6Hdr             *pIp6)    &
 *               Length of datagram (UINT4                 u4Len)    &
 *               UDP datagram       (tCRU_BUF_CHAIN_HEADER *pBuf) 
 * 
 * OUTPUTS     : None
 *
 * RETURNS     : None
 *
 * NOTES       : Presently only RIP6 packets are enqueued on the ports
 *               IP6_TASK_LOCK is taken in ip6main.c
 *****************************************************************************/

VOID
Udp6Rcv (tIp6If * pIf6, tIp6Hdr * pIp6, UINT4 u4Len,
         tCRU_BUF_CHAIN_HEADER * pBuf)
{
    tRip6Udp6Params    *pParam = NULL;
    tUdp6Hdr            udpHdr, *p_udp = NULL;
    UINT2               u2Chksum = 0;
    UINT2               u2ChksumRcvd = 0;
    tIp6Hdr             ip6Hdr;
    tUdp6Port          *pUdp6CBEntry = NULL;
    UINT1               u1Udp6CBMode = 0;
    INT1                i1Status = FALSE;
    INT4                i4RetVal = 0;

    if (u4Len < UDP6_HDR_LEN)
    {
        IP6_TRC_ARG1 (pIf6->pIp6Cxt->u4ContextId,
                      UDP6_MOD_TRC, DATA_PATH_TRC, UDP6_NAME,
                      "UDP6:Udp6Rcv: Truncated Pkt recvd length = %d\n", u4Len);
        IP6_TRC_ARG3 (pIf6->pIp6Cxt->u4ContextId,
                      UDP6_MOD_TRC, BUFFER_TRC, IP6_NAME,
                      "UDP6:Udp6Rcv: %s buf err: Type = %d Bufptr = %p",
                      ERROR_FATAL_STR, ERR_BUF_READ, pBuf);
        Ip6BufRelease (pIf6->pIp6Cxt->u4ContextId, pBuf, FALSE, UDP6_MODULE);
        return;
    }
    p_udp = Ip6BufRead (pBuf, (UINT1 *) &udpHdr,
                        IP6_BUF_READ_OFFSET (pBuf), sizeof (tUdp6Hdr), 0);

    if (p_udp == NULL)
    {
        IP6_TRC_ARG (pIf6->pIp6Cxt->u4ContextId,
                     UDP6_MOD_TRC, DATA_PATH_TRC, UDP6_NAME,
                     "UDP6:Udp6Rcv: Reading of UDP header failed\n");
        IP6_TRC_ARG3 (pIf6->pIp6Cxt->u4ContextId,
                      UDP6_MOD_TRC, BUFFER_TRC, IP6_NAME,
                      "UDP6:Udp6Rcv:%s buf err: Type = %d Bufptr = %p",
                      ERROR_FATAL_STR, ERR_BUF_READ, pBuf);
        Ip6BufRelease (pIf6->pIp6Cxt->u4ContextId, pBuf, FALSE, UDP6_MODULE);
        return;
    }
    if (Udp6GetPortCBInCxt (pIf6->pIp6Cxt->u4ContextId,
                            (UINT2) NTOHS (p_udp->u2DstPort), &pIp6->dstAddr,
                            &pUdp6CBEntry, &u1Udp6CBMode) == IP_SUCCESS)
    {
        i1Status = TRUE;
    }

    UDP6_STAT_IN_INC (u1Udp6CBMode, pIf6->pIp6Cxt->u4ContextId);
    UDP6_STAT_HC_IN_INC (u1Udp6CBMode, pIf6->pIp6Cxt->u4ContextId);
    IP6_BUF_READ_OFFSET (pBuf) -= sizeof (tUdp6Hdr);
    CRU_BUF_Move_ValidOffset (pBuf, IP6_BUF_READ_OFFSET (pBuf));
    IP6_BUF_READ_OFFSET (pBuf) = 0;

    IP6_BUF_GET_2_BYTE (pBuf, (IP6_BUF_READ_OFFSET (pBuf) + 6), u2ChksumRcvd);
    CRU_BUF_Move_ValidOffset (pBuf, IP6_BUF_READ_OFFSET (pBuf));

    i4RetVal = Ip6BufWrite (pBuf, (UINT1 *) &u2Chksum,
                            IP6_OFFSET (tUdp6Hdr, u2Chksum), sizeof (UINT2),
                            TRUE);
    IP6_BUF_READ_OFFSET (pBuf) = 0;
    if (Ip6Checksum (&pIp6->srcAddr, &pIp6->dstAddr, u4Len, NH_UDP6, pBuf)
        != NTOHS (u2ChksumRcvd))
    {
        /* RFC 2460 - Section 8.1 */
        if (NTOHS (u2ChksumRcvd) != IP6_BIT_ALL)
        {
            IP6_TRC_ARG (pIf6->pIp6Cxt->u4ContextId,
                         UDP6_MOD_TRC, DATA_PATH_TRC, UDP6_NAME,
                         "UDP6:Udp6Rcv: Check Sum failed for Udp packet arrived\n");
            Ip6BufRelease (pIf6->pIp6Cxt->u4ContextId, pBuf, FALSE,
                           UDP6_MODULE);
            UDP6_STAT_ERR_INC (u1Udp6CBMode, pIf6->pIp6Cxt->u4ContextId);
            return;
        }
    }
    u2ChksumRcvd = HTONS (u2ChksumRcvd);
    i4RetVal =
        Ip6BufWrite (pBuf, (UINT1 *) &u2ChksumRcvd,
                     IP6_OFFSET (tUdp6Hdr, u2Chksum), sizeof (UINT2), TRUE);

    IP6_BUF_READ_OFFSET (pBuf) -= u4Len;
    CRU_BUF_Move_ValidOffset (pBuf, IP6_BUF_READ_OFFSET (pBuf));

    IP6_TRC_ARG6 (pIf6->pIp6Cxt->u4ContextId, UDP6_MOD_TRC,
                  DATA_PATH_TRC, UDP6_NAME,
                  "UDP6:Udp6Rcv:RX IF=%d, Src=%s Dst=%s SrcPort=%d DstPort=%d "
                  " Len=%d \n", pIf6->u4Index, Ip6PrintAddr (&pIp6->srcAddr),
                  Ip6PrintAddr (&pIp6->dstAddr), NTOHS (p_udp->u2SrcPort),
                  NTOHS (p_udp->u2DstPort), u4Len);

    IP6_TRC_ARG1 (pIf6->pIp6Cxt->u4ContextId,
                  UDP6_MOD_TRC, DATA_PATH_TRC, UDP6_NAME, "Buf=%p\n", pBuf);

    IP6_TRC_ARG (pIf6->pIp6Cxt->u4ContextId,
                 UDP6_MOD_TRC, BUFFER_TRC, UDP6_NAME,
                 "UDP6:Udp6Rcv: The Input packet is : \n");
    IP6_PKT_DUMP (pIf6->pIp6Cxt->u4ContextId,
                  UDP6_MOD_TRC, DUMP_TRC, UDP6_NAME, pBuf, 0);

    if ((i1Status == TRUE) && (pUdp6CBEntry != NULL))
    {
        pParam = (tRip6Udp6Params *) (VOID *) Ip6GetMem (pIf6->pIp6Cxt->
                                                         u4ContextId,
                                                         (UINT2) gIp6GblInfo.
                                                         i4ParamId);

        if (pParam != NULL)
        {
            pParam->u1Hlim = pIp6->u1Hlim;
            pParam->u4Index = pIf6->u4Index;

            pParam->u2SrcPort = NTOHS (p_udp->u2SrcPort);
            pParam->u2DstPort = NTOHS (p_udp->u2DstPort);

            pParam->u4Len = u4Len - sizeof (tUdp6Hdr);

            Ip6AddrCopy (&pParam->srcAddr, &pIp6->srcAddr);
            Ip6AddrCopy (&pParam->dstAddr, &pIp6->dstAddr);

            Ip6SetParams ((tIp6Params *) (VOID *) pParam, pBuf);
            IP6_BUF_READ_OFFSET (pBuf) = 8;
            CRU_BUF_Move_ValidOffset (pBuf, IP6_BUF_READ_OFFSET (pBuf));
            IP6_BUF_READ_OFFSET (pBuf) = 0;

            /* Call to higher layer protocol registered with udp6 */
            if (pUdp6CBEntry->rcv_fnc)
            {
                pUdp6CBEntry->rcv_fnc (pIf6->pIp6Cxt->
                                       u4ContextId,
                                       pIp6->dstAddr,
                                       pIp6->srcAddr /* remote addr */ ,
                                       pParam->u2DstPort,
                                       pParam->u2SrcPort /* rem port */ ,
                                       pBuf, pIf6->u4Index,
                                       pParam->u4Len, pParam->u1Hlim);
                Ip6RelMem (pIf6->pIp6Cxt->u4ContextId,
                           (UINT2) gIp6GblInfo.i4ParamId, (UINT1 *) pParam);
            }

#ifdef SLI_WANTED
            SliSelectScanList (pUdp6CBEntry->i4SockDesc, SELECT_READ);
#endif
        }
    }                            /* port num < MAX_PORT */
    else
    {
        IP6_BUF_READ_OFFSET (pBuf) = 0;
        UDP6_STAT_ERR_INC (u1Udp6CBMode, pIf6->pIp6Cxt->u4ContextId);

        IP6_TRC_ARG3 (pIf6->pIp6Cxt->u4ContextId,
                      UDP6_MOD_TRC, DATA_PATH_TRC, UDP6_NAME,
                      "UDP6:Udp6Rcv: RX Pkt on unassigned Port=%d Dst=%s Src=%s, sending ICMP6 Err Msg\n",
                      NTOHS (p_udp->u2DstPort),
                      Ip6PrintAddr (&pIp6->dstAddr),
                      Ip6PrintAddr (&pIp6->srcAddr));
        MEMCPY (&ip6Hdr, pIp6, sizeof (tIp6Hdr));
        CRU_BUF_Prepend_BufChain (pBuf, (UINT1 *) &ip6Hdr, sizeof (tIp6Hdr));
        Icmp6SendErrMsg (pIf6, pIp6, ICMP6_DEST_UNREACHABLE,
                         ICMP6_PORT_UNREACHABLE, 0, pBuf);

        return;
    }
    UNUSED_PARAM (i4RetVal);
}

/****************************************************************************
 * DESCRIPTION : This routine returns the index  of control block in global 
 *               table which is assigned the given port number.
 *
 * INPUTS      : Port Number (u2UPort)
 *               Addr for which the index of CB is needed (pIp6Addr)
 *
 * OUTPUTS     : None
 *
 * RETURNS     : None
 *
 * NOTES       : Presently only RIP6 packets are enqueued on the ports
 *               IP6_TASK_LOCK is taken before calling this function
 *****************************************************************************/
INT4
Udp6GetPort (UINT2 u2UPort, tIp6Addr * pIp6Addr, tUdp6Port * pUdp6PortEntry)
{
    tUdp6Port          *pUdp6CBNode = NULL;

    if (u2UPort == 0)
    {
        return IP6_FAILURE;
    }

    TMO_SLL_Scan (&(gIp6GblInfo.Udp6CbEntry), pUdp6CBNode, tUdp6Port *)
    {
        if (pUdp6CBNode->u2UPort == u2UPort)
        {
            /* To get a free port */
            if (pIp6Addr == NULL)
            {
                return IP6_SUCCESS;
            }

            if (((pIp6Addr != NULL)
                 &&
                 (Ip6AddrMatch
                  (&pUdp6CBNode->addr, pIp6Addr,
                   IP6_ADDR_SIZE_IN_BITS) == TRUE))
                || ((IS_ADDR_UNSPECIFIED (pUdp6CBNode->addr))))
            {
                MEMCPY (pUdp6PortEntry, pUdp6CBNode, sizeof (tUdp6Port));
                return IP6_SUCCESS;
            }
        }

    }
    return IP6_FAILURE;
}

/*****************************************************************/
/*  Function Name   : Udp6GetPortCBInCxt                         */
/*  Description     : This routine returns the index  of control */
/*                    block in global table which is assigned for*/
/*                    given port number and context id           */
/*                    table.                                     */
/*  Input(s)        : u4ContextId - Context Identifier           */
/*                    u2UPort - Port Number                      */
/*                    pIp6Addr - IPv6 Address                    */
/*                    pUdp6PortEntry - UDP6 port Entry           */
/*                    pu1Udp6CBMode - UDP6 control block mode    */
/*  Output(s)       : None                                       */
/*  Returns         : IP6_SUCCESS/IP6_FAILURE                    */
/*  Notes           : IP6_TASK_LOCK is taken before calling      */
/*                    this function                              */
/*****************************************************************/
INT4
Udp6GetPortCBInCxt (UINT4 u4ContextId, UINT2 u2UPort, tIp6Addr * pIp6Addr,
                    tUdp6Port ** pUdp6PortEntry, UINT1 *pu1Udp6CBMode)
{
    tIp6Cxt            *pIp6Cxt = NULL;
    tUdp6Port          *pUdp6CBNode = NULL;

    if (u2UPort == 0)
    {
        return IP6_FAILURE;
    }

    pIp6Cxt = Ipv6UtilGetCxtEntryFromCxtId (u4ContextId);
    if (pIp6Cxt != NULL)
    {
        TMO_SLL_Scan (&(pIp6Cxt->Udp6CxtCbEntry), pUdp6CBNode, tUdp6Port *)
        {
            if (pUdp6CBNode->u2UPort == u2UPort)
            {
                /* To get a free port */
                if (pIp6Addr == NULL)
                {
                    return IP6_SUCCESS;
                }

                if (((pIp6Addr != NULL)
                     &&
                     (Ip6AddrMatch
                      (&pUdp6CBNode->addr, pIp6Addr,
                       IP6_ADDR_SIZE_IN_BITS) == TRUE))
                    || ((IS_ADDR_UNSPECIFIED (pUdp6CBNode->addr))))
                {
                    *pUdp6PortEntry = pUdp6CBNode;
                    *pu1Udp6CBMode = SOCK_UNIQUE_MODE;
                    return IP6_SUCCESS;
                }
            }
        }
    }

    /* Check UDP control block entries in UDP global table */
    TMO_SLL_Scan (&(gIp6GblInfo.Udp6CbEntry), pUdp6CBNode, tUdp6Port *)
    {
        if (pUdp6CBNode->u2UPort == u2UPort)
        {
            /* To get a free port */
            if (pIp6Addr == NULL)
            {
                return IP6_SUCCESS;
            }

            if (((pIp6Addr != NULL)
                 &&
                 (Ip6AddrMatch
                  (&pUdp6CBNode->addr, pIp6Addr,
                   IP6_ADDR_SIZE_IN_BITS) == TRUE))
                || ((IS_ADDR_UNSPECIFIED (pUdp6CBNode->addr))))
            {
                *pUdp6PortEntry = pUdp6CBNode;
                *pu1Udp6CBMode = SOCK_GLOBAL_MODE;
                return IP6_SUCCESS;
            }
        }

    }
    return IP6_FAILURE;
}

/****************************************************************************
 * DESCRIPTION : This routine returns the index  of control block in global 
 *               table which is assigned the given port number.
 *
 * INPUTS      : u2LPort, pIp6LAddr, u2RPort, pIp6RAddr
 *
 * OUTPUTS     : None
 *
 * RETURNS     : None
 *
 * NOTES       : Presently only RIP6 packets are enqueued on the ports
 *                IP6_TASK_LOCK is taken before calling this function
 *****************************************************************************/

INT4
UdpIpv6GetPort (UINT2 u2LPort, tIp6Addr * pIp6LAddr,
                UINT2 u2RPort, tIp6Addr * pIp6RAddr,
                tUdp6Port ** pUdp6PortEntry, UINT1 *pu1Udp6CBMode)
{
    tIp6Cxt            *pIp6Cxt = NULL;
    tUdp6Port          *pUdp6CBNode = NULL;
    UINT4               u4ContextId = 0;

    if (u2LPort == 0)
    {
        return IP6_FAILURE;
    }

    /* Search the UDP6 control block entry in context table first
     * if entry is not there, search in global table */
    for (u4ContextId = 0; u4ContextId <= VCM_MAX_L3_CONTEXT; u4ContextId++)
    {
        pIp6Cxt = Ipv6UtilGetCxtEntryFromCxtId (u4ContextId);
        if (pIp6Cxt != NULL)
        {
            TMO_SLL_Scan (&(pIp6Cxt->Udp6CxtCbEntry), pUdp6CBNode, tUdp6Port *)
            {
                if ((pUdp6CBNode->u2UPort == u2LPort) &&
                    (pUdp6CBNode->u2Remote_port == u2RPort))
                {
                    /* To get a free port */
                    if (pIp6LAddr == NULL)
                    {
                        return IP6_FAILURE;
                    }

                    if (((pIp6LAddr != NULL)
                         &&
                         (Ip6AddrMatch
                          (&pUdp6CBNode->addr, pIp6LAddr,
                           IP6_ADDR_SIZE_IN_BITS) == TRUE))
                        || ((IS_ADDR_UNSPECIFIED (pUdp6CBNode->addr))))
                    {
                        if (pIp6RAddr == NULL)
                        {
                            return IP6_FAILURE;
                        }

                        if (((pIp6RAddr != NULL) &&
                             (Ip6AddrMatch (&pUdp6CBNode->Remote_addr,
                                            pIp6RAddr,
                                            IP6_ADDR_SIZE_IN_BITS) == TRUE)) ||
                            ((IS_ADDR_UNSPECIFIED (pUdp6CBNode->Remote_addr))))
                        {
                            *pUdp6PortEntry = pUdp6CBNode;
                            *pu1Udp6CBMode = SOCK_UNIQUE_MODE;
                            return IP6_SUCCESS;
                        }
                    }
                }

            }
        }
    }

    TMO_SLL_Scan (&(gIp6GblInfo.Udp6CbEntry), pUdp6CBNode, tUdp6Port *)
    {
        if ((pUdp6CBNode->u2UPort == u2LPort) &&
            (pUdp6CBNode->u2Remote_port == u2RPort))
        {
            /* To get a free port */
            if (pIp6LAddr == NULL)
            {
                return IP6_FAILURE;
            }

            if (((pIp6LAddr != NULL)
                 &&
                 (Ip6AddrMatch
                  (&pUdp6CBNode->addr, pIp6LAddr,
                   IP6_ADDR_SIZE_IN_BITS) == TRUE))
                || ((IS_ADDR_UNSPECIFIED (pUdp6CBNode->addr))))
            {
                if (pIp6RAddr == NULL)
                {
                    return IP6_FAILURE;
                }

                if (((pIp6RAddr != NULL) &&
                     (Ip6AddrMatch (&pUdp6CBNode->Remote_addr,
                                    pIp6RAddr,
                                    IP6_ADDR_SIZE_IN_BITS) == TRUE)) ||
                    ((IS_ADDR_UNSPECIFIED (pUdp6CBNode->Remote_addr))))
                {
                    *pUdp6PortEntry = pUdp6CBNode;
                    *pu1Udp6CBMode = SOCK_GLOBAL_MODE;
                    return IP6_SUCCESS;
                }
            }
        }
    }
    return IP6_FAILURE;

}

/****************************************************************************/
/* DESCRIPTION : This function returns an euphemeral port                   */
/****************************************************************************/
PRIVATE UINT2
Udp6GetFreePort ()
{
    tUdp6Port           Udp6PortEntry;
    UINT2               u2UPort = 0;

    MEMSET (&Udp6PortEntry, 0, sizeof (tUdp6Port));
    for (u2UPort = PVT_UDP_PORT_START; u2UPort <= PVT_UDP_PORT_END; u2UPort++)
    {
        if (Udp6GetPort (u2UPort, NULL, &Udp6PortEntry) == IP6_FAILURE)
        {
            return u2UPort;
        }
    }
    return u2UPort;
}

/****************************************************************************
 * DESCRIPTION : This routine returns the index of next control block.
 *
 * INPUTS      : u2LPort, pIp6LAddr, u2RPort, pIp6RAddr
 *
 * OUTPUTS     : None
 *
 * RETURNS     : None
 *
 * NOTES       : Presently only RIP6 packets are enqueued on the ports
 *               IP6_TASK_LOCK is taken before calling this function
 *****************************************************************************/
UINT1
udp6GetNextIndexIpvxUdp6Table (UINT2 u2LPort,
                               UINT2 *u2NextLPort,
                               tSNMP_OCTET_STRING_TYPE * pIp6LAddr,
                               tSNMP_OCTET_STRING_TYPE * pIp6NextLAddr,
                               UINT2 u2RPort,
                               UINT2 *u2NextRPort,
                               tSNMP_OCTET_STRING_TYPE * pIp6RAddr,
                               tSNMP_OCTET_STRING_TYPE * pIp6NextRAddr)
{
    tIp6Addr            TempLAddr;
    tIp6Addr            TempRAddr;
    tUdp6Port          *pCurrUdpCb = NULL;
    tUdp6Port          *pNextUdpCb = NULL;

    MEMCPY (&TempLAddr, pIp6LAddr->pu1_OctetList, pIp6LAddr->i4_Length);
    MEMCPY (&TempRAddr, pIp6LAddr->pu1_OctetList, pIp6RAddr->i4_Length);

    if ((!IS_ADDR_UNSPECIFIED (TempLAddr))
        || (!IS_ADDR_UNSPECIFIED (TempRAddr)))
    {
        return SNMP_FAILURE;
    }

    TMO_SLL_Scan (&(gIp6GblInfo.Udp6CbEntry), pCurrUdpCb, tUdp6Port *)
    {
        if ((Ip6AddrMatch (&pCurrUdpCb->addr, &TempLAddr, IP6_ADDR_SIZE_IN_BITS)
             == TRUE) && (pCurrUdpCb->u2UPort == u2LPort) &&
            (Ip6AddrMatch (&pCurrUdpCb->Remote_addr, &TempRAddr,
                           IP6_ADDR_SIZE_IN_BITS) == TRUE)
            && (pCurrUdpCb->u2Remote_port == u2RPort))
        {
            pNextUdpCb = (tUdp6Port *)
                TMO_SLL_Next (&(gIp6GblInfo.Udp6CbEntry),
                              &(pCurrUdpCb->NextCBNode));
            if (pNextUdpCb != NULL)
            {
                *u2NextLPort = pNextUdpCb->u2UPort;
                *u2NextRPort = pNextUdpCb->u2Remote_port;
                Ip6AddrCopy ((tIp6Addr *) (VOID *) pIp6NextLAddr->pu1_OctetList,
                             &pNextUdpCb->addr);
                Ip6AddrCopy ((tIp6Addr *) (VOID *) pIp6NextRAddr->pu1_OctetList,
                             &pNextUdpCb->Remote_addr);
                return (SNMP_SUCCESS);
            }
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 * DESCRIPTION : This routine returns the first index of context table.
 *
 * INPUTS      : None
 *
 * OUTPUT      : pIp6FirstLAddr - Pointer to the First local address 
 *               pu4FirstPort - Pointer to the first udp port
 *               pIp6FirstRAddr - Pointer to the first remote address
 *               pu4FirstRemotePort - Pointer to the first remote port
 *               pu4FirstInstance - Pointer to the first instance
 *
 * RETURN      : SNMP_SUCCESS/SNMP_FAILURE 
 *               IP6_TASK_LOCK is taken before calling this function
 *****************************************************************************/
UINT1
udp6CxtGetFirstIndexIpvxUdp6Table (tSNMP_OCTET_STRING_TYPE * pIp6FirstLAddr,
                                   UINT4 *pu4FirstPort,
                                   tSNMP_OCTET_STRING_TYPE * pIp6FirstRAddr,
                                   UINT4 *pu4FirstRemotePort,
                                   UINT4 *pu4FirstInstance)
{
    tUdp6Port          *pUdp6CBNode = NULL;
    tIp6Cxt            *pIp6Cxt = NULL;

    pIp6Cxt = Ipv6UtilGetCurrentCxtEntry ();
    if (pIp6Cxt == NULL)
    {
        return (SNMP_FAILURE);
    }

    pUdp6CBNode = (tUdp6Port *) TMO_SLL_First (&(pIp6Cxt->Udp6CxtCbEntry));
    if (pUdp6CBNode != NULL)
    {
        *pu4FirstPort = (UINT4) pUdp6CBNode->u2UPort;
        *pu4FirstRemotePort = (UINT4) pUdp6CBNode->u2Remote_port;
        *pu4FirstInstance = pUdp6CBNode->u4Instance;

        Ip6AddrCopy ((tIp6Addr *) (VOID *) pIp6FirstLAddr->pu1_OctetList,
                     &pUdp6CBNode->addr);
        Ip6AddrCopy ((tIp6Addr *) (VOID *) pIp6FirstRAddr->pu1_OctetList,
                     &pUdp6CBNode->Remote_addr);
        pIp6FirstLAddr->i4_Length = sizeof (tIp6Addr);
        pIp6FirstRAddr->i4_Length = sizeof (tIp6Addr);
        return (SNMP_SUCCESS);

    }
    return (SNMP_FAILURE);
}

/****************************************************************************
 * DESCRIPTION : This routine returns the next index of context table.
 *
 * INPUTS      : pIp6LAddr - Pointer to the local address 
 *               u4LPort - udp port
 *               pIp6RAddr - Pointer to the remote address
 *               u4RPort - remote port
 *               u4Instance - udp instance
 *
 * OUTPUT      : pIp6NextLAddr - Pointer to the next local address 
 *               pu4NextLPort - Pointer to the next udp port
 *               pIp6NextRAddr - Pointer to the next remote address
 *               pu4NextRPort - Pointer to the next remote port
 *               pu4Instance - Pointer to the next instance
 *
 * RETURN      : SNMP_SUCCESS/SNMP_FAILURE 
 *               IP6_TASK_LOCK is taken before calling this function
 *****************************************************************************/
UINT1
udp6CxtGetNextIndexIpvxUdp6Table (tSNMP_OCTET_STRING_TYPE * pIp6LAddr,
                                  tSNMP_OCTET_STRING_TYPE * pIp6NextLAddr,
                                  UINT4 u4LPort,
                                  UINT4 *pu4NextLPort,
                                  tSNMP_OCTET_STRING_TYPE * pIp6RAddr,
                                  tSNMP_OCTET_STRING_TYPE * pIp6NextRAddr,
                                  UINT4 u4RPort,
                                  UINT4 *pu4NextRPort,
                                  UINT4 u4Instance, UINT4 *pu4Instance)
{
    tIp6Addr            TempLAddr;
    tIp6Addr            TempRAddr;
    tIp6Cxt            *pIp6Cxt = NULL;
    tUdp6Port          *pUdp6CBNode = NULL;
    tUdp6Port          *pNextUdp6CBNode = NULL;

    UNUSED_PARAM (u4Instance);
    MEMCPY (&TempLAddr, pIp6LAddr->pu1_OctetList, pIp6LAddr->i4_Length);
    MEMCPY (&TempRAddr, pIp6RAddr->pu1_OctetList, pIp6RAddr->i4_Length);

    pIp6Cxt = Ipv6UtilGetCurrentCxtEntry ();
    if (pIp6Cxt == NULL)
    {
        return (SNMP_FAILURE);
    }

    if (((INT4) u4LPort < 0) || (!IS_ADDR_UNSPECIFIED (TempLAddr)))
    {
        return SNMP_FAILURE;
    }

    if (pIp6Cxt != NULL)
    {
        TMO_SLL_Scan (&(pIp6Cxt->Udp6CxtCbEntry), pUdp6CBNode, tUdp6Port *)
        {
            if ((Ip6AddrMatch
                 (&pUdp6CBNode->addr, &TempLAddr,
                  IP6_ADDR_SIZE_IN_BITS) == TRUE)
                && (pUdp6CBNode->u2UPort == u4LPort)
                &&
                (Ip6AddrMatch
                 (&pUdp6CBNode->Remote_addr, &TempRAddr,
                  IP6_ADDR_SIZE_IN_BITS) == TRUE)
                && (pUdp6CBNode->u2Remote_port == u4RPort))
            {
                pNextUdp6CBNode = (tUdp6Port *)
                    TMO_SLL_Next (&(pIp6Cxt->Udp6CxtCbEntry),
                                  &(pUdp6CBNode->NextCBNode));
                if (pNextUdp6CBNode != NULL)
                {
                    *pu4NextLPort = (UINT4) pNextUdp6CBNode->u2UPort;
                    *pu4NextRPort = (UINT4) pNextUdp6CBNode->u2Remote_port;
                    *pu4Instance = pNextUdp6CBNode->u4Instance;

                    Ip6AddrCopy ((tIp6Addr *) (VOID *) pIp6NextLAddr->
                                 pu1_OctetList, &pNextUdp6CBNode->addr);
                    Ip6AddrCopy ((tIp6Addr *) (VOID *) pIp6NextRAddr->
                                 pu1_OctetList, &pNextUdp6CBNode->Remote_addr);
                    return (SNMP_SUCCESS);
                }
            }
        }
    }

    return SNMP_FAILURE;
}

/***************************************************************/
/*  Function Name   : Udp6AddCBNodeToCxtTable                  */
/*  Description     : Adds the control block node to the       */
/*                    context table                            */
/*  Input(s)        : u4ContextId - Context Identifier         */
/*                    pUdp6CbNode - UDP6 Control Block Node    */
/*  Output(s)       : None                                     */
/*  Returns         : IP6_SUCCESS/IP6_FAILURE                  */
/*  Notes           : IP6_TASK_LOCK is taken before            */
/*                    calling this function                    */
/***************************************************************/
INT4
Udp6AddCBNodeToCxtTable (UINT4 u4ContextId, tUdp6Port * pUdp6CbNode)
{
    tUdp6Port          *pPrevUdpEntry = NULL;
    tUdp6Port          *pCurrUdpEntry = NULL;
    tIp6Cxt            *pIp6Cxt = NULL;

    pIp6Cxt = Ipv6UtilGetCxtEntryFromCxtId (u4ContextId);
    if (pIp6Cxt == NULL)
    {
        return IP6_FAILURE;
    }
    /* Currently u4Instance is invalid index */
    TMO_SLL_Scan (&(pIp6Cxt->Udp6CxtCbEntry), pCurrUdpEntry, tUdp6Port *)
    {
        if ((pCurrUdpEntry->i4LocalAddrType == pUdp6CbNode->i4LocalAddrType) &&
            (pCurrUdpEntry->i4RemoteAddrType == pUdp6CbNode->i4RemoteAddrType))
        {
            if (Ip6IsAddrGreater (&pUdp6CbNode->addr, &pCurrUdpEntry->addr)
                == IP6_FAILURE)
            {
                break;
            }
            else if (Ip6AddrMatch (&pUdp6CbNode->addr, &pCurrUdpEntry->addr,
                                   IP6_ADDR_SIZE_IN_BITS) == TRUE)
            {
                if (pUdp6CbNode->u2UPort < pCurrUdpEntry->u2UPort)
                {
                    break;
                }
                else if (pUdp6CbNode->u2UPort == pCurrUdpEntry->u2UPort)
                {
                    if (Ip6IsAddrGreater (&pUdp6CbNode->Remote_addr,
                                          &pCurrUdpEntry->Remote_addr) ==
                        IP6_FAILURE)
                    {
                        break;
                    }
                    else if (Ip6AddrMatch (&pUdp6CbNode->Remote_addr,
                                           &pCurrUdpEntry->Remote_addr,
                                           IP6_ADDR_SIZE_IN_BITS) == TRUE)
                    {
                        if (pUdp6CbNode->u2Remote_port <=
                            pCurrUdpEntry->u2Remote_port)
                        {
                            break;
                        }
                    }
                }
            }
        }
        pPrevUdpEntry = pCurrUdpEntry;
    }
    TMO_SLL_Insert (&(pIp6Cxt->Udp6CxtCbEntry),
                    (&(pPrevUdpEntry->NextCBNode)),
                    (&(pUdp6CbNode->NextCBNode)));
    return IP6_SUCCESS;

}

/***************************************************************/
/*  Function Name   : Udp6DeleteCBNodeFromCxtTable             */
/*  Description     : Deletes the control block node from the  */
/*                    global context table.                    */
/*  Input(s)        : u4ContextId - Context Identifier         */
/*                    pUdp6CbNode - UDP6 Control Block Node    */
/*  Output(s)       : None                                     */
/*  Returns         : IP6_SUCCESS/IP6_FAILURE                  */
/*  Notes           : IP6_TASK_LOCK is taken before            */
/*                     calling this function                   */
/***************************************************************/
INT4
Udp6DeleteCBNodeFromCxtTable (UINT4 u4ContextId, tUdp6Port * pUdp6CbNode)
{
    tIp6Cxt            *pIp6Cxt = NULL;

    if (pUdp6CbNode != NULL)
    {
        pIp6Cxt = Ipv6UtilGetCxtEntryFromCxtId (u4ContextId);
        if (pIp6Cxt != NULL)
        {
            TMO_SLL_Delete (&(pIp6Cxt->Udp6CxtCbEntry),
                            &(pUdp6CbNode->NextCBNode));
            TMO_SLL_Init_Node (&(pUdp6CbNode->NextCBNode));
            MemReleaseMemBlock (gIp6GblInfo.i4Udp6CBPoolId,
                                (UINT1 *) pUdp6CbNode);
        }
        return IP6_SUCCESS;
    }
    return IP6_FAILURE;
}

/***************************************************************/
/*  Function Name   : Udp6AddCBNodeToGlbTable                  */
/*  Description     : Adds the control block node to the       */
/*                    global table.                            */
/*  Input(s)        : pUdp6CbNode - UDP6 Control Block Node    */
/*  Output(s)       : None                                     */
/*  Returns         : SUCCESS/FAILURE                          */
/*  Notes           : IP6_TASK_LOCK is taken before            */
/*                     calling this function                   */
/***************************************************************/
INT4
Udp6AddCBNodeToGlbTable (tUdp6Port * pUdp6CbNode)
{
    tUdp6Port          *pPrevUdpEntry = NULL;
    tUdp6Port          *pCurrUdpEntry = NULL;

    if (pUdp6CbNode == NULL)
    {
        return IP6_FAILURE;
    }
    /* Currently Instance is invalid index */
    TMO_SLL_Scan (&(gIp6GblInfo.Udp6CbEntry), pCurrUdpEntry, tUdp6Port *)
    {
        if ((pCurrUdpEntry->i4LocalAddrType == pUdp6CbNode->i4LocalAddrType) &&
            (pCurrUdpEntry->i4RemoteAddrType == pUdp6CbNode->i4RemoteAddrType))
        {
            if (Ip6IsAddrGreater (&pUdp6CbNode->addr, &pCurrUdpEntry->addr) ==
                IP6_FAILURE)
            {
                break;
            }
            else if (Ip6AddrMatch (&pUdp6CbNode->addr, &pCurrUdpEntry->addr,
                                   IP6_ADDR_SIZE_IN_BITS) == TRUE)
            {
                if (pUdp6CbNode->u2UPort < pCurrUdpEntry->u2UPort)
                {
                    break;
                }
                else if (pUdp6CbNode->u2UPort == pCurrUdpEntry->u2UPort)
                {
                    if (Ip6IsAddrGreater (&pUdp6CbNode->Remote_addr,
                                          &pCurrUdpEntry->Remote_addr) ==
                        IP6_FAILURE)
                    {
                        break;
                    }
                    else if (Ip6AddrMatch (&pUdp6CbNode->Remote_addr,
                                           &pCurrUdpEntry->Remote_addr,
                                           IP6_ADDR_SIZE_IN_BITS) == TRUE)
                    {
                        if (pUdp6CbNode->u2Remote_port <=
                            pCurrUdpEntry->u2Remote_port)
                        {
                            break;
                        }
                    }
                }
            }
        }
        pPrevUdpEntry = pCurrUdpEntry;
    }
    TMO_SLL_Insert (&(gIp6GblInfo.Udp6CbEntry),
                    (&(pPrevUdpEntry->NextCBNode)),
                    (&(pUdp6CbNode->NextCBNode)));
    return IP6_SUCCESS;
}

/***************************************************************/
/*  Function Name   : Udp6DeleteCBNodeFromGlbTable             */
/*  Description     : Deletes the control block node from the  */
/*                    global table.                            */
/*  Input(s)        : pUdp6CbNode - UDP6 Control Block Node    */
/*  Output(s)       : None                                     */
/*  Returns         : IP6_SUCCESS/IP6_FAILURE                  */
/*  Notes           : IP6_TASK_LOCK is taken before            */
/*                     calling this function                   */
/***************************************************************/
INT4
Udp6DeleteCBNodeFromGlbTable (tUdp6Port * pUdp6CbNode)
{
    if (pUdp6CbNode != NULL)
    {
        TMO_SLL_Delete (&(gIp6GblInfo.Udp6CbEntry), &(pUdp6CbNode->NextCBNode));
        TMO_SLL_Init_Node (&(pUdp6CbNode->NextCBNode));
        MemReleaseMemBlock ((gIp6GblInfo.i4Udp6CBPoolId),
                            (UINT1 *) (pUdp6CbNode));
        return IP6_SUCCESS;
    }
    return IP6_FAILURE;
}

/***************************************************************/
/*  Function Name   : Udp6GetStatEntry                         */
/*  Description     : Gets the stats Entry from the            */
/*                    global table.                            */
/*  Input(s)        : None                                     */
/*  Output(s)       : None                                     */
/*  Returns         : pUdp6StatEntry - UDP6 Control stats entry*/
/*  Notes           : IP6_TASK_LOCK is taken before            */
/*                     calling this function                   */
/***************************************************************/
tIp6Udp6Stats      *
Udp6GetStatEntry (VOID)
{
    tIp6Udp6Stats      *pUdp6StatEntry = NULL;

    pUdp6StatEntry = &gIp6GblInfo.Udp6Stats;
    return pUdp6StatEntry;
}

/******************************************************************/
/*  Function Name   : Udp6GetCurrCxtStatEntry                     */
/*  Description     : Gets the current stats entry for the context*/
/*  Input(s)        : None                                        */
/*  Output(s)       : None                                        */
/*  Returns         : pUdp6StatEntry - UDP6 stat entry            */
/*  Notes           : IP6_TASK_LOCK is taken before               */
/*                     calling this function                      */
/******************************************************************/
tIp6Udp6Stats      *
Udp6GetCurrCxtStatEntry (VOID)
{
    tIp6Udp6Stats      *pUdp6StatEntry = NULL;

    if (gIp6GblInfo.pIp6CurrCxt != NULL)
    {
        pUdp6StatEntry = &gIp6GblInfo.pIp6CurrCxt->Udp6Stats;
    }
    return pUdp6StatEntry;
}

/******************************************************************************
 * FUNCTION NAME    :  ClearUDPIpv6Datagrams
 *
 * DESCRIPTION      : This function clear the Stats UDPIpv6Datagrams counters.
 *
 * INPUT            : u4ContextId
 *
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 ******************************************************************************/
VOID
ClearUDPIpv6Datagrams (UINT4 u4ContextId)
{

    if (gIp6GblInfo.apIp6Cxt[u4ContextId] == NULL)
    {
        return;
    }
    IP6_TASK_LOCK ();
    gIp6GblInfo.apIp6Cxt[u4ContextId]->Udp6Stats.u4OutDgrams = 0;
    gIp6GblInfo.Udp6Stats.u4OutDgrams = 0;
    gIp6GblInfo.apIp6Cxt[u4ContextId]->Udp6Stats.u4InDgrams = 0;
    gIp6GblInfo.Udp6Stats.u4InDgrams = 0;
    IP6_TASK_UNLOCK ();
    return;
}

/******************************************************************************
 * FUNCTION NAME    :  UdpTstPopulateIpv6Datagrams
 *
 * DESCRIPTION      : This function clear the Stats UDPIpv6Datagrams counters.
 *
 * INPUT            : u4ContextId
 *
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 ******************************************************************************/
VOID
UdpTstPopulateIpv6Datagrams (UINT4 u4ContextId)
{
    UINT4               u4Value = 5;

    if (gIp6GblInfo.apIp6Cxt[u4ContextId] == NULL)
    {
        return;
    }
    IP6_TASK_LOCK ();
    gIp6GblInfo.apIp6Cxt[u4ContextId]->Udp6Stats.u4OutDgrams = u4Value;
    gIp6GblInfo.Udp6Stats.u4OutDgrams = u4Value;
    gIp6GblInfo.apIp6Cxt[u4ContextId]->Udp6Stats.u4InDgrams = u4Value;
    gIp6GblInfo.Udp6Stats.u4InDgrams = u4Value;
    IP6_TASK_UNLOCK ();
    return;
}

/***************************** END OF FILE **********************************/
