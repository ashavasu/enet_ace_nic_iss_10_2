/*******************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: nd6.h,v 1.28 2015/07/17 09:53:06 siva Exp $
 *
 * Description: 
 *
 *******************************************************************/
#ifndef _ND6_H
#define _ND6_H

/* CONSTANTS */
/*************/

/* ND Cache Reachability States  - SNMP Enum Values */

#define  ND6C_STATIC      ND6_CACHE_ENTRY_STATIC      /* Static NDCache entry */
#define  ND6C_REACHABLE   ND6_CACHE_ENTRY_REACHABLE   /* Reachable state */
#define  ND6C_INCOMPLETE  ND6_CACHE_ENTRY_INCOMPLETE  /* Incomplete state */
#define  ND6C_STALE       ND6_CACHE_ENTRY_STALE       /* Stale state */
#define  ND6C_DELAY       ND6_CACHE_ENTRY_DELAY       /* Delay state */
#define  ND6C_PROBE       ND6_CACHE_ENTRY_PROBE       /* Probe state */
#define  ND6C_EVPN        ND6_CACHE_ENTRY_EVPN        /* EVPN state */
#define  ND6C_STATIC_NOT_IN_SERVICE  8                /* Static Not in service
                                                         state is an ND6 Entry
                                                         created by static but 
                                                         not in use */

#define ND6C_TYPE_OTHER  1
#define ND6C_TYPE_DYNAMIC  2
#define ND6C_TYPE_STATIC  3
#define ND6C_TYPE_REMOTE  4

/* Other Constants */

#define  ND6_ON_LINK_FLAG           0x80        /* On-link flag */
#define  ND6_AUTO_CONFIG_FLAG       0x40        /* Auto-config flag */

#ifdef MIP6_WANTED
#define  ND6_ROUTER_ADDRESS_FLAG    0x20        /* Auto-config flag */
#define  ND6_H_BIT_STFUL_FLAG       0x20        /* H bit flag */
#define  ND6_MOH_FLAG_CHECK         0xe0        /* M, O & H bits set */
#endif
#define  ND6_MO_FLAG_CHECK          0x03       /* M & O bits set */

#define  ND6_M_BIT_STFUL_FLAG       0x80        /* M bit flag */
#define  ND6_O_BIT_STFUL_FLAG       0x40        /* O bit flag */
#define  ND6_P_BIT_NDPROXY_FLAG     0x04        /* P bit -Proxy Flag */

#define  ND6_RA_PREFERENCE_HIGH     0x08        /* Preference field - High */
#define  ND6_RA_PREFERENCE_MEDIUM   0x00        /* Preference field - Medium */
#define  ND6_RA_PREFERENCE_LOW      0x18        /* Preference field - Low */

#define  ND6_GARB_COLLECT_INTERVAL  300         /* Garbage collect timer
                                                 * period in seconds */

#define  ND6C_QUEUE_LEN             2         /* Maximum Queue length of
                                                 * IPv6 packets queued on a 
                                                 * NDCache entry */

#define  ND6C_PURGE_DYNAMIC         0x01        /* Flag to indicate purging
                                                 * only dynamic NDCache
                                                 * entries */
#define  ND6C_PURGE_ALL             0x02        /* Flag to indicate purging
                                                 * of all NDCache entries */

#define  ND6_CEASE_RA_ADV           0x00        /* Flag to cease RA
                                                 * advertisement */
#define  ND6_RA_ADV                 0x01        /* Flag to advertise
                                                 * RA with extensions */

#define  MAX_REDIRECT_DATASIZE      1280       
#define  ND6_NONCE_LENGTH           6

#define IP6_RA_CUR_MAX_HOP_LIMIT IP6_MAX_HOP_LIMIT
#define IP6_RA_CUR_MIN_HOP_LIMIT IP6_MIN_HOP_LIMIT  

#define IP6_RA_MAX_REACH_TIME  IP6_IF_MAX_REACHTIME
#define IP6_RA_MIN_REACH_TIME  IP6_IF_MIN_REACHTIME                             
#define IP6_RA_MAX_RETRANS_TIMER IP6_IF_MAX_RETTIME 
#define IP6_RA_MIN_RETRANS_TIMER IP6_IF_MIN_RETTIME 

#define MAX_STR_LEN                 256
#define ND_SYSLOG_INFO_MSG(msg) \
    if(gi4Ip6SysLogId > 0)\
    { \
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, (UINT4)gi4Ip6SysLogId, (const CHR1 *)(msg))); \
    }
extern UINT4 gu4Nd6CacheEntries;

#define IP6_ND_ENTRIES_IN_HW  (gNd6GblInfo.u4IncompNd6CacheEntries + gNd6GblInfo.u4ProbeNd6CacheEntries + gNd6GblInfo.u4DelayNd6CacheEntries + gNd6GblInfo.u4ReachNd6CacheEntries) 
/* TYPEDEFS */
/*************/

/*
 * The following structure is used for caching information about 
 * individual neighbors to which traffic has been sent recently. It is 
 * created and updated by various ND messages and is consulted for
 * link-layer address resolution, neighbor unreachability detection
 * during IPv6 packet forwarding
 */

typedef struct AnycastDest
{
    tTMO_SLL_NODE  nextAnycastNDCache;  /* Points to the next element in this list*/ 
    tIp6Addr       sourceAddr6;      /* UnicastAddress response received from anycast dest */
    UINT4          u4IfIndex;
    UINT1          lladdr[IP6_MAX_LLA_LEN];  /* Link layer address of sourceAddr6 */
    UINT1          u1NextHopPrefernce;
    UINT1          au1Reserved[1];
} tAnycastNDCache;

typedef struct Nd6CacheEntry
{

    tRBNodeEmbd    RbNode;        /* RbNode for the cache entry */
    tTMO_SLL       anycastDestAddrList; /* SLL to store the list of any 
                                           cast addresses */ 
    tIp6Addr       addr6;           /* IPv6 unicast/anycast address */
    tIp6If        *pIf6;           /* Pointer to Interface record */
    tIp6Addr       recentSrcAddr;
    tIp6Addr       anycastSrcAddr;
    UINT4          u4LastUseTime; /* Last request time for 
                                    * use of the NDCache entry */
    UINT4          u4LastUpdatedTime; /* This entry last updated time */
    tTMO_DLL       NdProxyNsSrcList; /* List of sources from which NS 
                                        has been proxied and NA
                                        hasn't been proxied back.
                                        Node type: tNd6ProxySrc */
    UINT1         *pNd6cInfo;      /* Pointer to NDCache information 
                                    * specific to Media (LAN/WAN) */
    UINT1          u1ReachState;  /* Reachability State of the 
                                    * NDCache entry */
    UINT1          u1RowStatus;       /* Rowstatus for IPvx MIB */
    UINT1          u1AddrType;   /* Indicates addr6 type as unicast/anycast */
    UINT1          u1HwStatus;   /* Hardware status. wherther NP_PRESENT or
                                    NP_NOT_PRESENT */
    UINT1          u1ProxyCache; /* Is this cache created for a proxied Neighbor */
    UINT1          u1SecureFlag; /* Is this Neighbor Secured */
    UINT1          u1Sync; /* Flag used for sync */
    UINT1          u1LlaFlag;
    UINT1          u1StaleToProbe;
    UINT1          au1Reserved[2];
    BOOL1         b1LinkAddrNAFlag;
    UINT1          au1SentNonce[ND6_NONCE_LENGTH];
    UINT1          au1RcvdNonce[ND6_NONCE_LENGTH];
    UINT4          u4SendTSLast; /* TS - Time Stamp in the last received packet*/
    UINT4          u4SendRDLast; /* RD - Received time of the Last received packet */
}
tNd6CacheEntry;


/*
 * The following structure is used for queueing IPv6 packets to be
 * forwarded after address resolution completion on NDCache entry
 */

typedef struct t_ND6_CACHE_PENDING_QUEUE
{

    tCRU_BUF_CHAIN_HEADER  *pLst;  /* Queue to store the
                                     * pending IPv6 packets */
    tCRU_BUF_CHAIN_HEADER  *pEnd;  /* Pointer to last packet
                                     * of pending queue */
    UINT4                  u4Cnt;  /* Number of IPv6 pkts
                                     * queued */

}
tNd6CachePendQue;

/*
 * The following structure is used for caching information specific to
 * NDCache entries on LAN interfaces
 */

typedef struct t_ND6_CACHE_LAN_INFORMATION
{

    UINT1                 lladdr[IP6_MAX_LLA_LEN];  /* Lower layer address
                                                     * of an IPv6 address
                                                     * of a NDCache entry */
    UINT1                 u1LlaLen;               /* Lower layer address 
                                                     * length */
    UINT1                 u1Reserved;
    UINT4                 u4Retries;               /* Number of retries */
    tNd6CachePendQue  pendQue;                 /* Pending Queue */
    tNd6CacheEntry     *pNd6cEntry;            /* Pointer to the NDCache
                                                     * entry for which the
                                                     * link-layer address is
                                                     * cached */
    tIp6Timer           timer;                    /* ND Timer node */

}
tNd6CacheLanInfo;

typedef struct _sND6ProxySrc{
    tTMO_DLL_NODE DllNode;
    tIp6Addr      addr6;          /* IP address of neighbor who sent the NS msg */
    UINT4         u4IfIndex;      /* Interface on which the neighbor is reachable */
} tNd6ProxySrcEntry;

typedef struct t_ND6_SEND_TIMESTAMP
{

    UINT1     u1Type;   /* Value of TimeStamp 13 */
    UINT1     u1Len;    /* Length of the option
                         * in units of 8 octets */

    UINT1     au1Rsvd[6];   /* Unused field that is reserved 48-bit */
    UINT1     au1TimeStamp[8]; /* TimeStamp value of the system 64-bit timestamp */

}
tNd6SendTimeStamp;

/* The following Structure is used for verification of advertisement sent is a fresh response to a solicitation sent earlier by the node using Nonce */

typedef struct t_ND6_SEND_NONCE
{

    UINT1     u1Type;   /* Value of Nonce 14 */
    UINT1     u1Len;    /* Length of the option
                         * in units of 8 octets */

    UINT1     au1Nonce[ND6_NONCE_LENGTH]; /* Nonce value */

}
tNd6SendNonce;
/*
 * The following structure is used by nodes to form and send the
 * Certificate Path Solicitations  */

typedef struct t_ND6_SEND_CERTIFICATE_PATH_SOLICITATION
{

    tIcmp6PktHdr  icmp6Hdr;   /* ICMP Main Packet Header */
    UINT2         u2Identifier;     /* Identifier field */
    UINT2         u2Component;  /* ComponentField */
}
tNd6SendCertPathSol;
/*
 * The following structure is used by nodes to form and send the
 * Certificate Path Advertisement  */
                                                                                                                                      typedef struct t_ND6_SEND_CERTIFICATE_PATH_ADVERTISEMENT
{

    tIcmp6PktHdr  icmp6Hdr;   /* ICMP Main Packet Header */
    UINT2         u2Identifier;     /* Identifier field */
    UINT2         u2AllComponent;  /* All Component Field */
    UINT2         u2Component;  /* ComponentField */
    UINT2         u2Rsvd;     /* Unused field that is
                                   * initialised to zero */

}
tNd6SendCertPathAdv;

typedef struct t_ND6_TRUST_ANCHOR_EXTN_HDR
{
    UINT1         u1Type;     /* Value of Trust Anchor Option 15 */
    UINT1         u1Len;      /* Length of the option  */
    UINT1         u1NameType; /* Name Type of TA is 1-DER Encoded X.501 Name  */
    UINT1         u1PadLen;   /* Pad Length of the option  */
}
tNd6TaExtnHdr;


typedef struct t_ND6_CERTIFICATE_OPTION
{
 UINT1    u1Type;
 UINT1    u1Len;
 UINT1    u1CertType;
 UINT1    u1Rsvd;
}
tNd6CertExtnHdr;
 

/*
 * The following structure is used by nodes to form and send the 
 * Neighbor Solicitations so as to request on link-layer address of a 
 * target node, to verify the reachability of a neighbor or to perform 
 * duplicate address detection. It is also used to interpret the 
 * Neighbor solicitations received from a neighbor
 */

typedef struct t_ND6_NEIGHBOR_SOLICITATION
{

    tIcmp6PktHdr  icmp6Hdr;   /* ICMP Main Packet Header */
    UINT4            u4Rsvd;     /* Unused field that is
                                   * initialised to zero */
    tIp6Addr       targAddr6;  /* IPv6 address of the 
                                   * target of the solicitation */

}
tNd6NeighSol;

/*
 * The following structure is used by the router to interpret the 
 * Router Solicitations received from the hosts
 */

typedef struct t_ND6_ROUTER_SOLICITATION
{

    tIcmp6PktHdr  icmp6Hdr;  /* ICMP Main Packet Header */
    UINT4            u4Rsvd;    /* Unused field that is
                                  * initialised to zero */

}
tNd6RoutSol;

/*
 * The following structure is used by the router to form and send out 
 * the Router Advertisements periodically, or in response to the
 * Router Solicitations so as to inform its presence along with other
 * information to the hosts
 */

typedef struct t_ND6_ROUTER_ADVERTISEMENT
{

    tIcmp6PktHdr  icmp6Hdr;         /* ICMP Main Packet Header */
    UINT1            u1HopLmt;        /* Hop Limit Value */
    UINT1            u1StfulFlag;     /* Contains the sub-fields
                                         +-+-+-+-+-+-+-+-+
                                         |M|O|H|Prf|P|Rsv|
                                         +-+-+-+-+-+-+-+-+

                                         * M   - Managed address
                                           Stateful configuration,
                                         * O   - Other Stateful
                                           address configuration 
                                         * H   - Not implemented
                                         * Prf - Not implemented
                                         * P   - Proxied
                                         * 'Reserved' (Unused) 
                                         */

    UINT2            u2Lifetime;       /* Lifetime (seconds) 
                                         * associated with the 
                                         * default router */
    UINT4            u4ReachTime;     /* Reachability time (seconds)
                                         * associated with a neighbor */
    UINT4            u4RetransTime;   /* Retransmit time (seconds)
                                         * between consecutive
                                         * Neighbor Solicitations */

}
tNd6RoutAdv;

/*
 * The following structure is used by the router to send redirect 
 * packets to inform hosts of a better first-hop node on the path to the
 * destination
 */

typedef struct tNd6Redirect
{

    tIcmp6PktHdr  icmp6Hdr;   /* ICMP Main Packet Header */
    UINT4            u4Rsvd;     /* Unused field that is
                                   * initialised to zero */
    tIp6Addr       targAddr6;  /* IPv6 address of the 
                                   * target of the solicitation */
    tIp6Addr       destAddr6;  /* IPv6 address of the 
                                   * destination redirected to
                                   * the target */

}
tNd6Redirect;

/*
 * The following structure consists of fields that are common to all
 * ND extension option formats
 */
typedef struct t_ND6_EXTENSION_HEADER
{

    UINT1  u1Type;  /* Value indicating the
                      * type of the ND Option
                      * messages */
    UINT1  u1Len;   /* Length of the option
                      * in units of 8 octets */

    UINT2  u2Reserved;
}
tNd6ExtHdr;

/*
 * The following structure consists of fields that are common to all
 * ND extension option formats
 */
typedef struct t_ND6_EXTENSION_CGA_HEADER
{

    UINT1  u1Type;  /* Value indicating the
                      * type of the ND Option
                      * messages */
    UINT1  u1Len;   /* Length of the option
                      * in units of 8 octets */

    UINT1  u1PadLen;
    UINT1  u1Reserved;
}tNd6CgaExtHdr;


/*
 * The following structure is used in specifying the prefix information
 * that can be used for on-link determination and/or address 
 * autoconfiguration by stateless means
 */

typedef struct t_ND6_PREFIX_EXTENSION_OPTION
{

    UINT1  u1Type;  /* Value indicating the 
                      * type of the ND Option
                      * messages */
    UINT1  u1Len;   /* Length of the option
                      * in units of 8 octets */

    UINT1          u1PrefLen;    /* Number of valid leading 
                                    * bits of Prefix */
    UINT1          u1PrefFlag;   /* Contains the sub-fields, 
                                    * L bit - on-link determination, 
                                    * A bit - address autoconfig
        * R bit - router address flag
                                    * 'Reserved' (Unused) */
    UINT4          u4ValidTime;  /* Valid Lifetime (seconds) of
                                    * the prefix for on-link
                                    * determination */
    UINT4          u4PrefTime;   /* Preferred Lifetime (seconds)
                                    * of the address generated
                                    * from the prefix via stateless  means */
    UINT4          u4Rsvd;        /* Unused field that is
                                    * initialised to zero */
    tIp6Addr     prefAddr6;     /* IPv6 address or Prefix
                                    * of an IPv6 address */

}
tNd6PrefixExt;

/*
 * The following structure is used in specifying the prefix information
 * that can be used for on-link determination and/or address
 * autoconfiguration by stateless means
 */
typedef struct t_ND6_RA_ROUTE_INFORMATION_OPTION
{
     UINT1     u1Type;
     UINT1     u1Length;
     UINT1     u1PrefixLen;
     UINT1     u1Preference;
     UINT4     u4RARouteLifetime;
     tIp6Addr  Ip6RaRoutePrefix;
}
tNd6RARouteInfoExt;

/*
 * The following structure is used in sending the IP packet along with
 * the Redirect packet that got triggered by it
 */

typedef struct t_ND6_REDIRECT_EXTENSION_OPTION
{

    UINT1  u1Type;  /* Value indicating the 
                      * type of the ND Option
                      * messages */
    UINT1  u1Len;   /* Length of the option
                      * in units of 8 octets */
    UINT2          u2Rsvd1;        /* Unused field that is
                                     * initialised to zero */
    UINT4          u4Rsvd2;        /* Unused field that is
                                     * initialised to zero */
    UINT1          *pu1Ip6Data;  /* IPv6 packet to be sent
                                     * with Redirect packet */

}
tNd6RedirExt;

/*
 * The following structure is used in specifying the link MTU to hosts
 */

typedef struct t_ND6_MTU_EXTENSION_OPTION
{

    UINT1  u1Type;  /* Value indicating the 
                      * type of the ND Option
                      * messages */
    UINT1  u1Len;   /* Length of the option
                      * in units of 8 octets */
    UINT2          u2Rsvd;  /* Unused field that is initialised to zero */
    UINT4          u4Mtu;   /* MTU for the link */

}
tNd6MtuExt;

typedef struct t_ND6_HA_INFO_OPTION
{
    UINT1 u1Type;
    UINT1 u1Length;
    UINT2 u2Rsvd ;
    INT2  i2HaPref;
    UINT2 u2HaLifetime;
}
tNd6HaInfo;

typedef struct t_ND6_NEW_ADV_INTERVAL_OPTION
{
    UINT1          u1Type;
    UINT1          u1Len;
    UINT2          u2Rsvd;
    UINT4          u4AdvInterval;
}tNd6AdvInterval;


/* 
 * The following structure is used to store the address 
 * of those Peers, for whom This Node acts as Proxy. This
 * data shall be maintained in a SLL, and Manipulated. 
 */
typedef struct _ND6_PROXY_LIST
{
tTMO_SLL_NODE Next;     /* Pointer to the next Node */
tIp6Addr  Nd6ProxyAddr;   /* IPv6 Address for which Proxy
                           * ND service is requested.
                           */
tIp6Timer ProxyTimer;
UINT4     u4Index;
}
tNd6ProxyListNode;

typedef struct _tIp6RAEntry {
    INT4  i4IfIndex; 
    UINT4 u4MaxInterval;
    UINT4 u4MinInterval;
    UINT4 u4ReachableTime;
    UINT4 u4RetransmitTime;
    UINT4 u4LinkMTU;
    UINT2 u2DefLifetime;
    UINT1 u1CurHopLimit;
    UINT1 u1SendAdverts;
    UINT1 u1MFlag;
    UINT1 u1OFlag;
    UINT1 u1UnUsed;
    UINT1 u1RowStatus;
} tIp6RAEntry;

typedef struct _tNd6RtEntry {
    tTMO_SLL_NODE   *pNext;
    tIp6Addr         Dst;         /* The destination address prefix */
    tIp6Addr         NextHop;     /* The next hop address */
    tIp6Timer        RtEntryTimer;
    UINT4            u4IfIndex;
    UINT1            u1PrefLen;
    UINT1            u1RouteType;
    UINT1            u1SecureFlag;
    UINT1            au1Padding[1];
} tNd6RtEntry;

/* The following structure contains the global information 
 * for the ND protocol */

typedef struct {
    tRBTree            Nd6CacheTable;     /* RBTree to store the neighbor
                                            cache information */
    INT4               i4Nd6CacheId;      /* ID to ND6Cache table */
    INT4               i4Nd6cLanId;       /* ID to ND6C LAN INFO table */
    INT4               i4Nd6ProxyListId; 
    UINT4              u4Nd6CacheEntries; /* Total no. of Cache entries
                                              saved across all virtual
                                                 routers */

    UINT4              u4Ip6NdRedirectDataPoolId;
    UINT4              u4IncompNd6CacheEntries;
    UINT4              u4StaleNd6CacheEntries;
    UINT4              u4DelayNd6CacheEntries;
    UINT4              u4ProbeNd6CacheEntries;
    UINT4              u4ReachNd6CacheEntries;
    UINT1              u1Nd6SeNDSec;     /* Sec value used in SeND CGA 
                                            generation*/
    UINT1              u1Nd6SeNDNbrSecLevel; /* Acceptable sec value from neighbors */
    UINT1              u1Nd6SeNDAuthType;
    UINT1              u4Nd6SeNDPrefixCheck;
    UINT4              u4Nd6SeNDMinKeyLength;
    UINT4              u4Nd6SeNDAcceptUnsecure;

} tNdGblInfo;

typedef struct _tNdRedirectData
{

UINT1               au1NdRedirectData[MAX_REDIRECT_DATASIZE];

}tNdRedirectData;

typedef struct _Nd6RmCtrlMsg {
    tRmMsg           *pData;     /* RM message pointer */
    UINT2             u2DataLen; /* Length of RM message */
    UINT1             u1Event;   /* RM event */
    UINT1             au1Pad[1];
}tNd6RmCtrlMsg;

typedef struct _Nd6RmMsg
{
     tNd6RmCtrlMsg RmCtrlMsg; /* Control message from RM */
}tNd6RmMsg;

typedef struct _Nd6RedTable {
    tRBNodeEmbd    RbNode;  /* RbNode for the cache entry */
    tIp6Addr       addr6;  /* IPv6 Address */
    tIp6Addr       recentSrcAddr; /* Recent src address in case of
                                    Anycast address */
    UINT4          u4Index;  /* IfIndex for the entry */
    UINT1          lladdr[IP6_MAX_LLA_LEN]; /* Link layer address for the 
                                               ipv6 address */
    UINT1          u1IfType;  /* Interface type */
    UINT1          u1AddrType; /* Address type - Anycast or Unicast */
    UINT1          u1DelFlag; /* Delete flag. Whether to delete the entry or
                                 not */
    UINT1          u1RowStatus;  /* Row status of the entry */
    UINT1          u1HwStatus;  /* Hw status. Whether present in NP or not */
    UINT1          u1ReachState; /* Reachability state - Reachable, Stale,
                                                        Incomplete, Probe */
    UINT1          u1Action;  /* Action flag which indicates Addition/ Deletion
                                if Cache entry */
    UINT1          u1SecureFlag; /* is neighbor secure */
    UINT1          au1Pad[2];    /*Reserved */
    UINT1          au1SentNonce[ND6_NONCE_LENGTH];
    UINT1          au1RcvdNonce[ND6_NONCE_LENGTH];
    UINT4          u4SendTSLast; /* TS - Time Stamp in the last received packet*/
    UINT4          u4SendRDLast; /* RD - Received time of the Last received packet */
}tNd6RedTable;


typedef struct _Nd6HACacheMarker
{
 UINT4  u4Index;    /* Pointer to Interface record */
 tIp6Addr       addr6;
}tNd6HACacheMarker;

typedef struct _sNd6RedGlobalInfo{
    tNd6HACacheMarker   Nd6HACacheMarker;/* Holds the keys for the NdTable
                                           * entry which is marked for batch
                                           * processing module.*/

    INT4                i4Nd6RedEntryTime; /* Time stamp when arp switchover
                                            * from active to standby starts */
    INT4                i4Nd6RedExitTime;  /* Time stamp when arp switchover
                                              from active to standby is over */

    UINT1               u1BulkUpdStatus;/* bulk update status
                                         * ND6_HA_UPD_NOT_STARTED 0
                                         * ND6_HA_UPD_COMPLETED 1
                                         * ND6_HA_UPD_IN_PROGRESS 2,
                                         * ND6_HA_UPD_ABORTED 3 */
    UINT1   u1NodeStatus;     /* Node status(RM_INIT/RM_ACTIVE/RM_STANDBY). */
    UINT1   u1NumPeersUp;     /* Indicates number of standby nodes
                                 that are up. */
    UINT1   bBulkReqRcvd;     /* To check whether bulk request recieved
                                 from standby before RM_STANDBY_UP event. */
    UINT1   u1RedStatus;      /* Redundancy status of the node */
    UINT1   au1Padding[3];    /* Reserved */
}tNd6RedGlobalInfo;

/* Secure ND (SEND) Datastructures */
#define SHA_DIGEST_LENGTH                20      /* 20x8 = 160 bits */
#define ND6_SEND_RSA_KEY_HASH            16
#define ND6_SEND_RSA_HASH_LEN            20
#define ND6_SEND_RSA_SIGN_LEN            256
#define ND6_EXT_HDR_LEN_UNIT              8      /* Length is in units of 8 octets */
#define ND6_SEND_CGA_IFID_LEN            ND6_SEND_CGA_PREFIX_LEN
#define ND6_SEND_CGA_SEC_MULT            16      /* sec multiplier */
#define ND6_SEND_CGA_MAX_COLLISION        2
#define ND6_SEND_CGA_MAX_SEC              7
#define ND6_SEND_CGA_PARAM_LEN           (CGA_MODLEN + 8 + 1)
#define ND6_SEND_RSA_OPT                 12
#define ND6_SEND_NONCE_RAND               6
#define ND6_SEND_CPS_IDENTIFIER           2

#define ND6_SEND_CGA_HASH1_LEN            8       /* 64 bits */ 
#define ND6_SEND_CGA_HASH2_LEN           14       /* 112 bits */
#define ND6_SEND_CGA_HASH1_MASK        0x1c      /* 0001 1100 */
#define ND6_SEND_CGA_TAG                 16      /* 16x8 = 128 bits */
#define ND6_SEND_ADDR_LENGTH             16      /* 16 Bytes (Source and Destination IP6 Address)*/

#define ND6_SEND_MAX_BUF_SIZE          1024      /* MEMPOOL */
#define UINT1_MAX_VALUE                 255       /* 0xFF */

#define ND6_SEND_CPS_EXTN_LEN  64  /* Approx. Length of TA Option (Variable Length) */

#define ND6_TA_SUPPORTED                1
#define ND6_TA_NOT_SUPPORTED            2

#define ND6_SECURE_ENTRY                  1
#define ND6_UNSECURE_ENTRY                0

/*
#define ND6_SEND_CGA_MOD_LEN             16
#define ND6_SEND_CGA_PREFIX_LEN           8       64 bits */

/*
typedef struct _sCgaOptions
{
    UINT1       au1Modifier [ND6_SEND_CGA_MOD_LEN];
    UINT1       au1Prefix [ND6_SEND_CGA_PREFIX_LEN];
    UINT1       u1Collisions;
}tCgaOptions;*/

typedef struct _sRsaOptions
{
    UINT1      au1KeyHash[ND6_SEND_RSA_KEY_HASH];
    UINT1      *pu1Signature;
    UINT4      u4SignLen;
}tRsaOptions;     

/* MACROs */
/**********/

#ifdef L2RED_WANTED
#define ND6_GET_NODE_STATUS()  gNd6RedGlobalInfo.u1NodeStatus
#else
#define ND6_GET_NODE_STATUS()  RM_ACTIVE
#endif

#ifdef L2RED_WANTED
#define ND6_GET_RMNODE_STATUS()  RmGetNodeState ()
#else
#define ND6_GET_RMNODE_STATUS()  RM_ACTIVE
#endif

#define   NP_PRESENT            1
#define   NP_NOT_PRESENT        2

#define ND6_PROXY_NODE_FROM_TIMER(TimerNode) \
        (tNd6ProxyListNode *)\
 ((UINT1 *)(TimerNode) - IP6_OFFSET(tNd6ProxyListNode, ProxyTimer))

#define ND6C_LAN_INFO_FROM_TIMER(timer_node) \
        (tNd6CacheLanInfo *) (VOID *)\
        ((UINT1 *)(timer_node) - IP6_OFFSET(tNd6CacheLanInfo, timer))

#define ND6_PEND_QUE_LST_FROM_LINFO(pNd6cLinfo) \
        pNd6cLinfo->pendQue.pLst

#define ND6_PEND_QUE_END_FROM_LINFO(pNd6cLinfo) \
        pNd6cLinfo->pendQue.pEnd

#define ND6_PEND_QUE_CNT_FROM_LINFO(pNd6cLinfo) \
        pNd6cLinfo->pendQue.u4Cnt

#define ND6_GET_DIFF_TIME(u4CurrentTime, u4_cmp_time) \
        (u4CurrentTime >= u4_cmp_time) ?\
           u4CurrentTime - u4_cmp_time :\
           u4CurrentTime + IP6_MAX_SYS_TIME + 1 - u4_cmp_time

#define ND6_IS_ACTIVE_TIMER    TMO_Get_Remaining_Time

#define IP6_IF_RS_TIMER(if)  \
        &if->Timer

#define IP6_IF_RS_SCHED_TIME(if)  \
        if->stats.u4RsSchedTime

#define IP6_IF_RS_SENT_TIME(if)  \
        if->stats.u4RsSentTime

#define IP6_IF_RS_INITIAL_CNT(if)  \
        if->stats.u4RsInitialCnt

#define IP6_RA_DEF_VALID_LIFE_TIME         7200*(SYS_TIME_TICKS_IN_A_SEC)

#define IP6_RA_DEF_PREF_LIFE_TIME         7200*(SYS_TIME_TICKS_IN_A_SEC)

#define IS_AUTONOMOUS_FLAG_SET(u1flag)  ( u1flag & ND6_AUTO_CONFIG_FLAG)

#define IS_ONLINK_FLAG_SET(u1flag)      ( u1flag & ND6_ON_LINK_FLAG)

#define IP6_GET_DEF_RTR_LST_FROM_ROUTE(x) \
      (tNd6RtEntry *)((UINT1 *)(x) - IP6_OFFSET(tNd6RtEntry ,pNext))

#define IP6_MAX_DEF_ROUTERS 5
#define IP6_DEF_ROUTERS_LFE_TIME_INFINITY  0xffffffff

/* If related macros */
#define  IP6_IF_TOKEN(index)        gIp6GblInfo.apIp6If[index]->ifaceTok

/* Macros for Proxy ND */

#define ND6_PROXY_LST_PTR_FROM_SLL(sll)  \
        (tNd6ProxyListNode *)(VOID *)\
        ((UINT1 * )(VOID *)(sll)-IP6_OFFSET(tNd6ProxyListNode, Next))

#define ND6_ADD_TO_PROXY_LIST     1
#define ND6_DEL_FROM_PROXY_LIST   2
   
#define ND6C_NO_ENTRY_UPDATE        0x00
#define ND6C_ENTRY_UPDATE           0x01

#define ND6C_NO_LLA_UPDATE          0x00
#define ND6C_LLA_UPDATE             0x01
 
#define IPV6_SEND_NA_FLAG_RESET 0
#define IPV6_SEND_NA_FLAG_SET 1
#define IPV6_PAYLOAD_OFFSET   5
#define IPV6_NS_PKT_LENGTH 32

/* IPvx MIB RA Default Values and Macros */
#define IP6_RA_INVALID         0x08

#define IP6_RA_MIN_LINK_MTU      1280
#define IP6_RA_MAX_LINK_MTU      1500
#define IP6_RA_MIN_HOPLMT        0
#define IP6_RA_MAX_HOPLMT        255

#define IP6_RA_DEF_REACHTIME     0
#define IP6_RA_DEF_RETTIME       0
#define IP6_RA_DEF_MAX_RA_TIME   600
#define IP6_RA_DEF_MIN_RA_TIME   (0.33 * IP6_RA_DEF_MAX_RA_TIME)
#define IP6_RA_DEF_DEFTIME       (3 * IP6_RA_DEF_MAX_RA_TIME)
#define IP6_RA_DEF_HOPLMT        64
#define IP6_RA_DEF_LINK_MTU      0
#define IP6_RA_DEF_SEND_STATUS   IPVX_TRUTH_VALUE_FALSE
#define IP6_RA_DEF_O_FLAG        IPVX_TRUTH_VALUE_FALSE
#define IP6_RA_DEF_M_FLAG        IPVX_TRUTH_VALUE_FALSE
#define IP6_RA_DEF_PREFERENCE    0x00

INT4
ND6Lock (VOID);

INT4
ND6UnLock (VOID);

#endif /* !_ND6_H */
