
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * 
 * $Id: ip6frag.c,v 1.12
 * 
 *
 ********************************************************************/
/*
 *----------------------------------------------------------------------------- 
 *    FILE NAME                    :    ip6frag.c                          
 *                                                                              
 *    PRINCIPAL AUTHOR             :    Anshul Garg                             
 *                                                                              
 *    SUBSYSTEM NAME               :    IPv6                                    
 *                                                                              
 *    MODULE NAME                  :    Fragmentation & Reassembly  Submodule 
 *                                                                              
 *    LANGUAGE                     :    C                                       
 *                                                                              
 *    TARGET ENVIRONMENT           :    UNIX                                    
 *                                                                              
 *    DATE OF FIRST RELEASE        :    Dec-02-1996                             
 *                                                                              
 *    DESCRIPTION                  :    This file contains C routines to       
 *                                      fragment and reassembly ipv6 packets  
 *----------------------------------------------------------------------------- 
 */

#include "ip6inc.h"

/*
 * Local function prototypes
 */

PRIVATE UINT4       Ip6FindUnfragSize (UINT1 *, tCRU_BUF_CHAIN_HEADER *);
PRIVATE INT1        Ip6PutFragHdrInBufInCxt (tIp6Cxt *, UINT1, UINT1, UINT2,
                                             tCRU_BUF_CHAIN_HEADER *);

#define IN_MULTIPLES_OF_8_OCTETS(x)    ((x) * 8)

/******************************************************************************
 * DESCRIPTION : This fucntion is for fragmenting original packet at the source
 *               and sending subsequent fragments to the destination. First
 *               unfragmentable part in the packet is determined and later on
 *               this part is sent with each fragment
 * INPUTS      : 
 * OUTPUTS     : None
 * RETURNS     : IP6_SUCCESS - if fragmentation is complete
 *               IP6_FAILURE - otherwise 
 * NOTES       :
 ******************************************************************************/

INT4
Ip6FragSend (tIp6If * pIf6, tIp6Addr * pDst6, tNd6CacheEntry * pNd6c,
             UINT4 u4Mtu, UINT4 u4Len, tCRU_BUF_CHAIN_HEADER * pBuf,
             UINT1 u1RouteAddrType)
{
    tIp6Cxt            *pIp6Cxt = NULL;
    UINT1               u1MFlag = 0, u1Hdr = 0;
    UINT2               u2Len = 0;
    UINT4               u4PktLen = 0;
    UINT2               u2FragOffset = 0, u2_frag_data_size = 0;
    UINT4               u4UnfragSize = 0, u4_frag_size = 0;
    tCRU_BUF_CHAIN_HEADER *pUnfragPart = NULL, *p_frag_part = NULL;
    UINT1              *pu1UnfragBuf = NULL;
    tIp6FragHdr        *pFragmentHdr = NULL;
    UINT4               u4FragmentSize = 0;
    UINT2               u2PayloadLen = 0;
    UINT1               u1Copy = 0;

    /* Get the value of next header in IP6 main header */
    IP6_BUF_GET_1_BYTE (pBuf, IP6_OFFSET_FOR_NEXTHDR_FIELD, u1Hdr);

    /* Get the next header value in last header of unfragmentable part */
    u4UnfragSize = Ip6FindUnfragSize (&u1Hdr, pBuf);
    u4_frag_size = u4Len - u4UnfragSize;

    /*
     * Obtain the unfragmentable part of the buffer and divide the rest into
     * fragments
     */

    pUnfragPart = pBuf;

    if (CRU_BUF_Fragment_BufChain (pUnfragPart, u4UnfragSize, &pBuf)
        == CRU_FAILURE)
    {
        return IP6_FAILURE;
    }

    pIp6Cxt = pIf6->pIp6Cxt;
    /* Append the dummy fragment to the unfragmentable part. Whose fragment
     * offset and more fragments flags could be filled while sending each 
     * of the fragments
     */
    if (Ip6PutFragHdrInBufInCxt (pIp6Cxt, u1Hdr, u1MFlag, u2FragOffset,
                                 pUnfragPart) == IP6_FAILURE)
    {
        IP6IF_INC_OUT_FRAG_FAILS (pIf6);
        IP6SYS_INC_OUT_FRAG_FAILS (pIp6Cxt->Ip6SysStats);
        return IP6_FAILURE;
    }

    pu1UnfragBuf = MemAllocMemBlk (gIp6GblInfo.i4Ip6BufPoolId);
    if (pu1UnfragBuf == NULL)
    {
        IP6IF_INC_OUT_FRAG_FAILS (pIf6);
        IP6SYS_INC_OUT_FRAG_FAILS (pIp6Cxt->Ip6SysStats);
        return IP6_FAILURE;
    }

    pFragmentHdr = (tIp6FragHdr *) (VOID *) &pu1UnfragBuf[u4UnfragSize];

    if (Ip6BufRead (pUnfragPart, pu1UnfragBuf, 0,
                    u4UnfragSize + sizeof (tIp6FragHdr), 1) == NULL)
    {
        MemReleaseMemBlock (gIp6GblInfo.i4Ip6BufPoolId, (UINT1 *) pu1UnfragBuf);
        return IP6_FAILURE;
    }
    else
    {
        /*
         * If the Unfragmentable part (pUnfragPart) is successfuly copied 
         * to the local linear buffer (pu1UnfragBuf), then releasing
         * the pUnfragPart
         */
        Ip6BufRelease (pIp6Cxt->u4ContextId, pUnfragPart,
                       FALSE, IP6_FRAG_SUBMODULE);
    }

    IP6_TRC_ARG6 (pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                  DATA_PATH_TRC, IP6_NAME,
                  "IP6FRAG:Ip6FragSend: UnfragPart=%p size=%d FragPart=%p "
                  "size=%d IF=%d MTU=%d",
                  pUnfragPart, u4UnfragSize, pBuf, u4_frag_size,
                  pIf6->u4Index, u4Mtu);

    /*
     * At this point, 'pUnfragPart' points to the unfragmentable portion
     * of the packet and 'pBuf' points to the rest of the packet which has
     * to be split into fragments
     */
    while (u4_frag_size > (u4FragmentSize))
    {
        /*
         * find the length of remaining data and see if it can fit within
         * a single fragment (last fragment)
         */

        /* If the payload length is greater than 65535, truncate 
         * the length to 65535 */

        if ((u4_frag_size >= MAX_PAYLOAD_SIZE) && (u2FragOffset == 0))
        {
            u4PktLen = MAX_PAYLOAD_SIZE;
        }
        else
        {
            u4PktLen =
                (u4_frag_size -
                 IN_MULTIPLES_OF_8_OCTETS (u2FragOffset) +
                 u4UnfragSize + sizeof (tIp6FragHdr));
        }

        p_frag_part = pBuf;

        if (u4PktLen <= u4Mtu)
        {
            u2_frag_data_size =
                (UINT2) (u4_frag_size -
                         IN_MULTIPLES_OF_8_OCTETS (u2FragOffset));
            u1MFlag = 0;
        }
        else
        {

            u2_frag_data_size =
                (UINT2) (((u4Mtu - u4UnfragSize -
                           sizeof (tIp6FragHdr)) / BYTE_LENGTH) * BYTE_LENGTH);
            u1MFlag = 1;

            if (CRU_BUF_Fragment_BufChain (p_frag_part, u2_frag_data_size,
                                           &pBuf) == CRU_FAILURE)
            {
                IP6_TRC_ARG (pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                             DATA_PATH_TRC, IP6_NAME,
                             "\nIP6FRAG:Ip6FragInit: couldn't fragment the "
                             "buffer \n");
                MemReleaseMemBlock (gIp6GblInfo.i4Ip6BufPoolId,
                                    (UINT1 *) pu1UnfragBuf);
                IP6IF_INC_OUT_FRAG_FAILS (pIf6);
                IP6SYS_INC_OUT_FRAG_FAILS (pIp6Cxt->Ip6SysStats);
                return IP6_FAILURE;
            }
        }

        /*
         * Determine the payload len (u4Len) of this fragment, form the
         * fragment header, prepend unfragmentable part (pUnfragPart) with 
         * the fragment (p_frag_part) and pass the fragment 
         * to the output routine
         */

        u2Len = (UINT2) (u4UnfragSize + sizeof (tIp6FragHdr) +
                         u2_frag_data_size - sizeof (tIp6Hdr));
        u2PayloadLen = (UINT2) (HTONS (u2Len));
        MEMCPY ((void *) (&pu1UnfragBuf[IP6_FOUR]), (void *) &u2PayloadLen,
                sizeof (UINT2));

        u4Len = u2Len + sizeof (tIp6Hdr);

        IP6_TRC_ARG3 (pIf6->pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                      DATA_PATH_TRC, IP6_NAME,
                      "IP6FRAG:Ip6FragSend: Fragment data size=%d TotPktLen=%d "
                      "MoreFlag=%d\n", u2_frag_data_size, u4Len, u1MFlag);

        pFragmentHdr->u2FragOff =
            (UINT2) (HTONS (((u2FragOffset << IP6_THREE) | u1MFlag)));

        if (Ip6BufPptr (p_frag_part, pu1UnfragBuf,
                        u4UnfragSize + sizeof (tIp6FragHdr), &u1Copy) == NULL)
        {
            MemReleaseMemBlock (gIp6GblInfo.i4Ip6BufPoolId,
                                (UINT1 *) pu1UnfragBuf);

            IP6IF_INC_OUT_FRAG_FAILS (pIf6);
            IP6SYS_INC_OUT_FRAG_FAILS (pIp6Cxt->Ip6SysStats);
            return IP6_FAILURE;
        }

        IP6_TRC_ARG (pIf6->pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                     DATA_PATH_TRC, IP6_NAME,
                     "IP6FRAG:Ip6FragSend: Fragment Pkt being sent is :\n");
        IP6_PKT_DUMP (pIf6->pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                      DUMP_TRC, IP6_NAME, p_frag_part, 0);

        IP6IF_INC_OUT_FRAG_CREATES (pIf6);
        IP6SYS_INC_OUT_FRAG_CREATES (pIp6Cxt->Ip6SysStats);
        Ip6ifSend (pIf6, pDst6, pNd6c, u4Len, p_frag_part, u1RouteAddrType);

        if (u1MFlag == 1)
        {
            u2FragOffset += u2_frag_data_size / BYTE_LENGTH;

            /*
             * On Ethernet, we need to do a ND cache lookup again if the
             * cache was originally NULL because when the first fragment is
             * sent, the cache entry would have been created
             */

            if (((pIf6->u1IfType == IP6_ENET_INTERFACE_TYPE) ||
#ifdef WGS_WANTED
                 (pIf6->u1IfType == IP6_L2VLAN_INTERFACE_TYPE) ||
#endif
                 (pIf6->u1IfType == IP6_PSEUDO_WIRE_INTERFACE_TYPE) ||
                 (pIf6->u1IfType == IP6_L3VLAN_INTERFACE_TYPE) ||
                 (pIf6->u1IfType == IP6_LAGG_INTERFACE_TYPE) ||
                 (pIf6->u1IfType == IP6_L3SUB_INTF_TYPE)) && (pNd6c == NULL))
            {
                pNd6c = Nd6LookupCache (pIf6, pDst6);
            }
        }
        else
        {
            break;
        }
        u4FragmentSize = IN_MULTIPLES_OF_8_OCTETS (u2FragOffset);
    }

    OsixGetSysTime (&pIp6Cxt->u4DgramFragId);
    ++pIp6Cxt->u4DgramFragId;
    IP6IF_INC_OUT_FRAG_OKS (pIf6);
    IP6SYS_INC_OUT_FRAG_OKS (pIp6Cxt->Ip6SysStats);
    MemReleaseMemBlock (gIp6GblInfo.i4Ip6BufPoolId, (UINT1 *) pu1UnfragBuf);
    return IP6_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : This function forms the fragment header and appends it to
 *               the unfragmentable part given by 'pTmpBuf'
 ******************************************************************************/

PRIVATE INT1
Ip6PutFragHdrInBufInCxt (tIp6Cxt * pIp6Cxt, UINT1 u1Hdr, UINT1 u1MFlag,
                         UINT2 u2FragOffset, tCRU_BUF_CHAIN_HEADER * pTmpBuf)
{
    tIp6FragHdr         fragHdr;
    tCRU_BUF_CHAIN_HEADER *pFragHdrBuf = NULL;

    fragHdr.u1NextHdr = u1Hdr;
    fragHdr.u1Rsvd = 0;

    fragHdr.u2FragOff = (UINT2) (HTONS (((u2FragOffset << IP6_THREE) |
                                         u1MFlag)));
    fragHdr.u4Id = HTONL (pIp6Cxt->u4DgramFragId);

    if ((pFragHdrBuf = Ip6BufAlloc (pIp6Cxt->u4ContextId,
                                    sizeof (tIp6FragHdr),
                                    0, IP6_FRAG_SUBMODULE)) == NULL)
    {
        IP6_TRC_ARG (pIp6Cxt->u4ContextId, IP6_MOD_TRC, BUFFER_TRC, IP6_NAME,
                     "IP6FRAG:Ip6PutFragHdrInBufInCxt: Buffer could not "
                     "be allocated for fragment header\n");
        IP6_TRC_ARG3 (pIp6Cxt->u4ContextId, IP6_MOD_TRC, BUFFER_TRC, IP6_NAME,
                      "IP6FRAG:Ip6PutFragHdrInBufInCxt: %s buf err:, "
                      "Type = %d Bufptr = %p",
                      ERROR_FATAL_STR, ERR_BUF_ALLOC, pTmpBuf);
        return IP6_FAILURE;
    }

    if (Ip6BufWrite (pFragHdrBuf, (UINT1 *) &fragHdr, 0, sizeof
                     (tIp6FragHdr), 1) == IP6_FAILURE)
    {
        IP6_TRC_ARG (pIp6Cxt->u4ContextId, IP6_MOD_TRC, BUFFER_TRC, IP6_NAME,
                     "IP6FRAG:Ip6PutFragHdrInBufInCxt: Fragment header "
                     "could not be written in buffer\n");
        IP6_TRC_ARG3 (pIp6Cxt->u4ContextId, IP6_MOD_TRC, BUFFER_TRC, IP6_NAME,
                      "%s buf err:, Type = %d Bufptr = %p", ERROR_FATAL_STR,
                      ERR_BUF_WRITE, 0);
        Ip6BufRelease (pIp6Cxt->u4ContextId, pFragHdrBuf, FALSE,
                       IP6_FRAG_SUBMODULE);
        return IP6_FAILURE;
    }

    CRU_BUF_Concat_MsgBufChains (pTmpBuf, pFragHdrBuf);

    return IP6_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : This function determines the size of the unfragmentable
 *               part by couting all those headers which cannot be fragmented
 *               as specified in RFC 1883. Additionally, it also finds the
 *               first header in the fragmentable part and changes the "next
 *               header" field of the previous header to point to fragment
 *               header
 ******************************************************************************/

UINT4
Ip6FindUnfragSize (UINT1 *pu1Hdr, tCRU_BUF_CHAIN_HEADER * pBuf)
{
    UINT1               u1HdrFlag = TRUE, u1HdrLen = 0;
    UINT4               u4PrevHdrOffset = 0, u4HdrOffset = 0;

    u4HdrOffset = IP6_HDR_LEN;
    u4PrevHdrOffset = IP6_OFFSET_FOR_NEXTHDR_FIELD;

    while (u1HdrFlag == TRUE)
    {
        switch (*pu1Hdr)
        {
                /* Hop-by-hop and Routing headers are unfragmentable */
            case NH_H_BY_H:
            case NH_ROUTING_HDR:
                break;

                /* Other headers go into fragmentable part */
            default:
                u1HdrFlag = FALSE;
                break;
        }

        if (u1HdrFlag)
        {
            IP6_BUF_GET_1_BYTE (pBuf, u4HdrOffset, *pu1Hdr);
            IP6_BUF_GET_1_BYTE (pBuf, u4HdrOffset + 1, u1HdrLen);

            u4PrevHdrOffset = u4HdrOffset;
            u4HdrOffset += IN_MULTIPLES_OF_8_OCTETS (u1HdrLen + 1);

        }

    }

    IP6_BUF_SET_1_BYTE (pBuf, u4PrevHdrOffset, NH_FRAGMENT_HDR);
    return u4HdrOffset;
}

/******************************************************************************
 * DESCRIPTION : Puts the fragment in the fragment queue according to the
 *               value of start and end offset for the fragment data in 
 *               original packet.
 * INPUTS      : Start offset     (UINT2                u2StartOffset) &
 *               End offset       (UINT2                u2EndOffset)   &
 *               Reassembly entry (tIp6ReasmEntry    *pIp6Reasm)    &
 *               Fragment packet  (tCRU_BUF_CHAIN_HEADER *pBuf)
 * OUTPUTS     : None
 * RETURNS     : None
 * NOTES       :
 ******************************************************************************/

INT1
Ip6PutFragInQueue (UINT2 u2StartOffset, UINT2 u2EndOffset,
                   tIp6ReasmEntry * pIp6Reasm, tCRU_BUF_CHAIN_HEADER * pBuf)
{

    tIp6FragQueEntry   *pFrag = NULL, *p_next_frag = NULL, *p_prev_frag = NULL;
    tIp6Cxt            *pIp6Cxt = NULL;

    /* Searching the exact position for the fragment to be inserted */

    pIp6Cxt = ((tIp6If *) IP6_INTERFACE (pIp6Reasm->u4Index))->pIp6Cxt;
    TMO_DLL_Scan (&(pIp6Reasm->fragQue), p_next_frag, tIp6FragQueEntry *)
    {
        if (p_next_frag->u2StartOff > u2StartOffset)
        {
            break;
        }
        p_prev_frag = p_next_frag;
    }

    /*
     * Check if new fragment overlaps with previous or next fragments and
     * if so return error
     */

    if (((p_prev_frag != NULL) &&
         (p_prev_frag->u2EndOff > u2StartOffset))
        || ((p_next_frag != NULL) && (p_next_frag->u2StartOff < u2EndOffset)))
    {
        IP6_TRC_ARG (pIp6Cxt->u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                     "IP6FRAG:Ip6PutFragInQueue: Overlapping fragment received\n");
        return IP6_FAILURE;
    }

    pFrag =
        (tIp6FragQueEntry *) (VOID *) Ip6GetMem (pIp6Cxt->u4ContextId,
                                                 (UINT2) gIp6GblInfo.
                                                 i4Ip6FragId);

    if (pFrag == NULL)
    {
        IP6_TRC_ARG (pIp6Cxt->u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                     "IP6FRAG:Ip6PutFragInQueue: Unable to get memory for fragment queue entry\n");
        return IP6_FAILURE;
    }

    /*
     * fill the fields of the entry and insert in the linked list
     */

    pFrag->u2StartOff = u2StartOffset;
    pFrag->u2EndOff = u2EndOffset;
    pFrag->pBuf = pBuf;

    if (p_prev_frag == NULL)
    {
        TMO_DLL_Insert (&(pIp6Reasm->fragQue), (tTMO_DLL_NODE *) NULL,
                        &pFrag->link);
    }
    else
    {
        TMO_DLL_Insert (&(pIp6Reasm->fragQue), &p_prev_frag->link,
                        &pFrag->link);
    }

    return IP6_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : Check whether any fragment with the same identifier has already
 *               come or not. If it has come , it returns corresponding 
 *               reassembly entry otherwise NULL. 
 * INPUTS      : IPv6 main header      (tIp6Hdr *pIp6)      &
 *               identifier for packet (UINT4     u4Id)
 * OUTPUTS     : None
 * RETURNS     : If reassembly entry exists - reassembly entry 
 *               NULL                       - otherwise
 * NOTES       :
 ******************************************************************************/

tIp6ReasmEntry     *
Ip6LookupReasmEntryInCxt (tIp6Cxt * pIp6Cxt, tIp6Hdr * pIp6, UINT4 u4Id)
{
    tIp6ReasmEntry     *pIp6Reasm = NULL;
    UINT1               u1Count = 0;

    for (u1Count = 0; u1Count < MAX_IP6_REASM_ENTRIES_LIMIT; u1Count++)
    {
        if (pIp6Cxt->apIp6Ream[u1Count]->u1State == ENTRY_USED)
        {
            pIp6Reasm = pIp6Cxt->apIp6Ream[u1Count];
            if ((Ip6AddrMatch (&pIp6Reasm->ip6Src, &pIp6->srcAddr,
                               IP6_ADDR_SIZE_IN_BITS) == TRUE) &&
                (Ip6AddrMatch (&pIp6Reasm->ip6Dst, &pIp6->dstAddr,
                               IP6_ADDR_SIZE_IN_BITS) == TRUE) &&
                (pIp6Reasm->u4Id == u4Id))
            {

                return (pIp6Reasm);
            }
        }
    }

    return NULL;
}

/******************************************************************************
 * DESCRIPTION : Allocate reassembly entry for the fragment having new 
 *               identifier or source and destination address.
 * INPUTS      : IPv6 main header  (tIp6Hdr *pIp6)       &
 *               Identifier        (UINT4     u4Id) 
 * OUTPUTS     : None
 * RETURNS     : Reassembly entry - if allocated
 *               NULL             - otherwise
 * NOTES       :
 ******************************************************************************/

tIp6ReasmEntry     *
Ip6CreatReasmEntryInCxt (tIp6Cxt * pIp6Cxt, tIp6Hdr * pIp6,
                         UINT4 u4Id, UINT4 u4IfIndex)
{
    tIp6ReasmEntry     *pIp6Reasm = NULL;
    UINT1               u1Count = 0;

    for (u1Count = 0; u1Count < MAX_IP6_REASM_ENTRIES_LIMIT; u1Count++)
    {
        if (pIp6Cxt->apIp6Ream[u1Count]->u1State == ENTRY_UNUSED)
        {
            pIp6Reasm = pIp6Cxt->apIp6Ream[u1Count];
            Ip6AddrCopy (&pIp6Reasm->ip6Src, &pIp6->srcAddr);
            Ip6AddrCopy (&pIp6Reasm->ip6Dst, &pIp6->dstAddr);
            pIp6Reasm->u4Id = u4Id;
            pIp6Reasm->u1State = ENTRY_USED;
            pIp6Reasm->u4PktLen = 0;
            pIp6Reasm->u4CurLen = 0;
            pIp6Reasm->u4Index = u4IfIndex;
            TMO_DLL_Init (&pIp6Reasm->fragQue);
            Ip6TmrStart (pIp6Cxt->u4ContextId, FRAG_REASM_TIMER_ID,
                         gIp6GblInfo.Ip6TimerListId,
                         &pIp6Reasm->timer.appTimer, IP6_REASM_TIME);
            return pIp6Reasm;
        }
    }

    IP6_TRC_ARG (pIp6Cxt->u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                 "IP6FRAG:Ip6CreatReasmEntry: Could not get a free "
                 "Reassembly entry\n");

    return NULL;
}

/******************************************************************************
 * DESCRIPTION : Reassembly all the fragment data from fragment queue to form
 *               original packet.
 * INPUTS      : Reassembly entry (tIp6ReasmEntry *pIp6Reasm)
 * OUTPUTS     : None
 * RETURNS     : Original IPv6 packet - if reassembly is complete
 *               NULL                 - otherwise
 * NOTES       :
 ******************************************************************************/
tCRU_BUF_CHAIN_HEADER *
Ip6ReasmFrags (UINT1 *pu1NextHdr, UINT4 *pu4Len, tIp6ReasmEntry * pIp6Reasm)
{

    tIp6FragQueEntry   *pFirstFrag = NULL;
    tCRU_BUF_CHAIN_HEADER *pReasmPkt = NULL;
    tIp6Cxt            *pIp6Cxt = NULL;
    UINT1               u1HdrLen = 0;
    UINT4               u4HdrOffset = 0;
    UINT4               u4PrevHdrOffset = 0;
    UINT4               u4UnfragLen = 0;

    pIp6Cxt = ((tIp6If *) IP6_INTERFACE (pIp6Reasm->u4Index))->pIp6Cxt;
    /*
     * Copy the unfragmentable portion into a separate buffer (pReasmPkt)
     */
    pFirstFrag = (tIp6FragQueEntry *) TMO_DLL_First (&(pIp6Reasm->fragQue));
    if (pFirstFrag == NULL)
    {
        IP6_TRC_ARG (pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                     DATA_PATH_TRC | ALL_FAILURE_TRC, IP6_NAME,
                     "IP6FRAG:Ip6ReasmFrags: No fragment entry\n");
        return NULL;
    }

    pReasmPkt = pFirstFrag->pBuf;

    /*
     * Fill the correct payload length and the next header value in the
     * unfragmentable part of the packet being reassembled
     */

    IP6_BUF_SET_2_BYTE (pReasmPkt, (UINT4) IP6_OFFSET_FOR_PAYLOAD_LEN,
                        (UINT2) (pIp6Reasm->u4PktLen - sizeof (tIp6Hdr)));

    u4PrevHdrOffset = IP6_OFFSET_FOR_NEXTHDR_FIELD;
    u4HdrOffset = sizeof (tIp6Hdr);

    IP6_BUF_GET_1_BYTE (pReasmPkt, IP6_OFFSET_FOR_NEXTHDR_FIELD, *pu1NextHdr);

    while (*pu1NextHdr != NH_FRAGMENT_HDR)
    {
        IP6_BUF_GET_1_BYTE (pReasmPkt, u4HdrOffset, *pu1NextHdr);
        IP6_BUF_GET_1_BYTE (pReasmPkt, u4HdrOffset + 1, u1HdrLen);

        u4PrevHdrOffset = u4HdrOffset;
        u4HdrOffset += IN_MULTIPLES_OF_8_OCTETS (u1HdrLen + 1);
    }

    *pu4Len = u4HdrOffset;
    u4UnfragLen = u4HdrOffset;

    IP6_BUF_GET_1_BYTE (pFirstFrag->pBuf, u4HdrOffset, (*pu1NextHdr));
    IP6_BUF_SET_1_BYTE (pReasmPkt, u4PrevHdrOffset, (*pu1NextHdr));

    /*
     * Remove the fragment header from the first fragment and concat the
     * rest of the fragment with the unfragmentable part
     */
    CRUBUFCopyBufChains (pReasmPkt, pFirstFrag->pBuf,
                         sizeof (tIp6FragHdr), 0, u4UnfragLen);

    CRU_BUF_Move_ValidOffset (pReasmPkt, sizeof (tIp6FragHdr));

    pFirstFrag->pBuf = NULL;

    TMO_DLL_Delete (&(pIp6Reasm->fragQue), &pFirstFrag->link);

    if (Ip6RelMem
        (pIp6Cxt->u4ContextId, (UINT2) gIp6GblInfo.i4Ip6FragId,
         (UINT1 *) pFirstFrag) == IP6_FAILURE)
    {
        IP6_TRC_ARG4 (pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                      DATA_PATH_TRC, IP6_NAME,
                      "IP6FRAG:Ip6ReasmFrags: %s mem err:, Type = %d  PoolId = %d Memptr = %p",
                      ERROR_FATAL_STR, ERR_MEM_RELEASE, 0,
                      gIp6GblInfo.i4Ip6FragId);

        return (NULL);
    }

    /*
     * Remove the fragment header from each of the remaining fragments and
     * concat with the reassembled packet being formed
     */
    while ((pFirstFrag = (tIp6FragQueEntry *) TMO_DLL_First
            (&(pIp6Reasm->fragQue))) != NULL)
    {
        CRUBUFDeleteBufChainAtStart (pFirstFrag->pBuf,
                                     IP6_BUF_READ_OFFSET (pFirstFrag->pBuf));
        CRU_BUF_Concat_MsgBufChains (pReasmPkt, pFirstFrag->pBuf);

        pFirstFrag->pBuf = NULL;

        TMO_DLL_Delete (&(pIp6Reasm->fragQue), &pFirstFrag->link);

        if (Ip6RelMem (pIp6Cxt->u4ContextId, (UINT2) gIp6GblInfo.i4Ip6FragId,
                       (UINT1 *) pFirstFrag) == IP6_FAILURE)
        {
            IP6_TRC_ARG4 (pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                          DATA_PATH_TRC, IP6_NAME,
                          "IP6FRAG:Ip6ReasmFrags: %s mem err:, Type = %d  PoolId = %d Memptr = %p",
                          ERROR_FATAL_STR, ERR_MEM_RELEASE, 0,
                          gIp6GblInfo.i4Ip6FragId);
            return (NULL);
        }
    }

    /* Set read offset at the end of unfrag part */
    IP6_BUF_READ_OFFSET (pReasmPkt) = u4HdrOffset;

    IP6_TRC_ARG (pIp6Cxt->u4ContextId, IP6_MOD_TRC, BUFFER_TRC, IP6_NAME,
                 "IP6FRAG: Ip6ReasmFrags: The Reassembled pkt is :\n");
    IP6_PKT_DUMP (pIp6Cxt->u4ContextId, IP6_MOD_TRC, DUMP_TRC,
                  IP6_NAME, pReasmPkt, 0);

    /*
     * Stop the Reassembly timer and mark the Reassembly entry as unused
     */
    if (pIp6Reasm->timer.u1Id != 0)
    {
        Ip6TmrStop (pIp6Cxt->u4ContextId, FRAG_REASM_TIMER_ID,
                    gIp6GblInfo.Ip6TimerListId, &(pIp6Reasm->timer.appTimer));
    }
    MEMSET (pIp6Reasm, 0, sizeof (tIp6ReasmEntry));
    pIp6Reasm->u1State = ENTRY_UNUSED;

    return (pReasmPkt);
}

/******************************************************************************
 * DESCRIPTION : This routine handles the incoming fragment packets
 * INPUTS      : Interface record (tIp6If              *pIf6)         &
 *               IPv6 main header (tIp6Hdr             *pIp6)         &
 *               Packet itself    (tCRU_BUF_CHAIN_HEADER *pBuf)
 * OUTPUTS     : None
 * RETURNS     : IPv6 original packet - if all the packets have come and
 *                                      reassembly is complete
 *               NULL                 - otherwise
 * NOTES       :
 ******************************************************************************/
tCRU_BUF_CHAIN_HEADER *
Ip6HandleReasm (UINT1 *pu1NextHdr, UINT4 *pu4Len, tIp6If * pIf6, tIp6Hdr * pIp6,
                tCRU_BUF_CHAIN_HEADER * pBuf)
{

    UINT1               u1MFlag = 0;
    UINT2               u2FragLen = 0, u2StartOffset = 0;
    UINT4               u4EndOffset = 0;
    UINT2               u2FirstFragLen = 0, u2_last_frag_len = 0;
    UINT2               u2PayloadLen = 0, u2_last_offset = 0;
    UINT4               u4Offset = 0;

    tIp6Cxt            *pIp6Cxt = NULL;
    tIp6ReasmEntry     *pIp6Reasm = NULL;
    tIp6FragHdr         fragHdr, *pFrag = NULL;
    tIp6FragQueEntry   *pFragQue = NULL;

    pIp6Cxt = pIf6->pIp6Cxt;

    IP6_TRC_ARG5 (pIp6Cxt->u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                  "IP6FRAG:Ip6HandleReasm: Recvd Frag on IF=%d PLen=%d Bufptr=%p Src=%s Dst=%s\n",
                  pIf6->u4Index, pIp6->u2Len, pBuf,
                  Ip6PrintAddr (&pIp6->srcAddr), Ip6PrintAddr (&pIp6->dstAddr));

    IP6IF_INC_REASM_REQDS (pIf6);
    IP6SYS_INC_REASM_REQDS (pIp6Cxt->Ip6SysStats);

    /* Here u4Offset is used locally to find the totall no. of bytes in pkt */
    u4Offset = IP6_BUF_DATA_LEN (pBuf);

    if (u4Offset < (*pu4Len) + sizeof (tIp6FragHdr))
    {
        IP6_TRC_ARG (pIp6Cxt->u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                     "IP6FRAG:Ip6HandleReasm: Not enough bytes in pkt for Fragment Hdr\n");
        IP6_TRC_ARG3 (pIp6Cxt->u4ContextId, IP6_MOD_TRC, BUFFER_TRC, IP6_NAME,
                      "IP6FRAG:Ip6HandleReasm: %s buf err:, Type = %d Bufptr = %p",
                      ERROR_FATAL_STR, ERR_BUF_READ, pBuf);

        IP6IF_INC_REASM_FAILS (pIf6);
        IP6SYS_INC_REASM_FAILS (pIp6Cxt->Ip6SysStats);

        Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE, IP6_FRAG_SUBMODULE);
        return NULL;
    }

    pFrag = (tIp6FragHdr *) Ip6BufRead (pBuf, (UINT1 *) &fragHdr,
                                        IP6_BUF_READ_OFFSET (pBuf),
                                        sizeof (tIp6FragHdr), 0);

    if (pFrag == NULL)
    {
        IP6_TRC_ARG (pIp6Cxt->u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                     "IP6FRAG:Ip6HandleReasm: Could not read fragment header from received packet\n");
        IP6_TRC_ARG3 (pIp6Cxt->u4ContextId, IP6_MOD_TRC, BUFFER_TRC, IP6_NAME,
                      "IP6FRAG:Ip6HandleReasm: %s buf err: Type = %d Bufptr = %p",
                      ERROR_FATAL_STR, ERR_BUF_READ, pBuf);

        IP6IF_INC_REASM_FAILS (pIf6);
        IP6SYS_INC_REASM_FAILS (pIp6Cxt->Ip6SysStats);

        Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE, IP6_FRAG_SUBMODULE);
        return NULL;
    }

    u2FragLen = (UINT2) ((HTONS (pIp6->u2Len) + IP6_HDR_LEN) -
                         (IP6_BUF_READ_OFFSET (pBuf)));

    u2StartOffset =
        (UINT2) (IN_MULTIPLES_OF_8_OCTETS
                 (((UINT2) (NTOHS (pFrag->u2FragOff) & 0xfff8) >> 3)));
    u1MFlag = (UINT1) (NTOHS (pFrag->u2FragOff) & 0x1);
    u4EndOffset = u2StartOffset + u2FragLen;

    if (u4EndOffset > MAX_PAYLOAD_SIZE)
    {
        IP6_TRC_ARG (pIp6Cxt->u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                     "IP6FRAG:Ip6HandleReasm: Frag could cause reassembled");
        IP6_TRC_ARG (pIp6Cxt->u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                     "pkt's PL length to exceed 65535 bytes");

        Icmp6SendErrMsg (pIf6, pIp6, ICMP6_PKT_PARAM_PROBLEM,
                         ICMP6_HDR_PROB,
                         IP6_OFFSET_FOR_FRAGOFF_FIELD + *pu4Len, pBuf);

        IP6IF_INC_REASM_FAILS (pIf6);
        IP6SYS_INC_REASM_FAILS (pIp6Cxt->Ip6SysStats);

        return NULL;
    }

    IP6_TRC_ARG2 (pIp6Cxt->u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                  "IP6FRAG:Ip6HandleReasm: Received fragment of size = %d Start offset = %d",
                  u2FragLen, u2StartOffset);
    IP6_TRC_ARG2 (pIp6Cxt->u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                  "End offset = %d More Flag = %d\n", u4EndOffset, u1MFlag);

    if (u1MFlag == 1)
    {
        /* Check if fragment length is multiple of 8 or not */

        if (u2FragLen % BYTE_LENGTH != 0)
        {
            IP6_TRC_ARG1 (pIp6Cxt->u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC,
                          IP6_NAME, "IP6FRAG:Ip6HandleReasm: Received fragment"
                          " which is not last but invalid len%d\n", u2FragLen);
            Icmp6SendErrMsg (pIf6, pIp6, ICMP6_PKT_PARAM_PROBLEM,
                             ICMP6_HDR_PROB, IP6_OFFSET_FOR_PAYLOAD_LEN, pBuf);

            IP6IF_INC_REASM_FAILS (pIf6);
            IP6SYS_INC_REASM_FAILS (pIp6Cxt->Ip6SysStats);

            return NULL;
        }

    }

    /* 
     * Put the fragment pkt into appropriate Reassembly queue, creating
     * one if necessary
     */

    if ((pIp6Reasm = Ip6LookupReasmEntryInCxt (pIp6Cxt, pIp6,
                                               NTOHL (pFrag->u4Id))) == NULL)
    {

        /* All the fragments of a packet will be received in the same
         * IP interface. Hence when creating the Reasm entry, fill the
         * incoming interface index in the entry so that it can be used 
         * when the reasm entry timeouts */
        if ((pIp6Reasm = Ip6CreatReasmEntryInCxt (pIp6Cxt, pIp6,
                                                  NTOHL (pFrag->u4Id),
                                                  pIf6->u4Index)) == NULL)
        {
            Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE,
                           IP6_FRAG_SUBMODULE);

            IP6IF_INC_REASM_FAILS (pIf6);
            IP6SYS_INC_REASM_FAILS (pIp6Cxt->Ip6SysStats);

            return NULL;
        }
    }

    if (Ip6PutFragInQueue (u2StartOffset, (UINT2) u4EndOffset,
                           pIp6Reasm, pBuf) == IP6_FAILURE)
    {
        Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE, IP6_FRAG_SUBMODULE);

        IP6IF_INC_REASM_FAILS (pIf6);
        IP6SYS_INC_REASM_FAILS (pIp6Cxt->Ip6SysStats);

        return NULL;
    }

    /* 
     * Increment length of fragments received and if first and last fragment
     * are received, calculate the original pkt length
     */

    if (u2StartOffset == 0)
    {
        pIp6Reasm->u4CurLen += IP6_BUF_READ_OFFSET (pBuf) +
            u2FragLen - sizeof (tIp6FragHdr);

        pFragQue = (tIp6FragQueEntry *) TMO_DLL_Last (&(pIp6Reasm->fragQue));
        if (pFragQue == NULL)
        {
            IP6IF_INC_REASM_FAILS (pIf6);
            IP6SYS_INC_REASM_FAILS (pIp6Cxt->Ip6SysStats);
            Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE,
                           IP6_FRAG_SUBMODULE);
            return NULL;
        }
        u4Offset = IP6_BUF_READ_OFFSET (pFragQue->pBuf);

        /*
         * Obtain the 'fragment offset' of the last fragment in our queue and
         * see if its 'More' bit is set or not
         */

        IP6_BUF_GET_2_BYTE (pFragQue->pBuf, (UINT4) (u4Offset - 6),
                            u2_last_offset);

        if (!(u2_last_offset & 0x0001))
        {
            u2_last_offset = (UINT2) ((u2_last_offset & 0x0fff8) >> IP6_THREE);
            u2_last_frag_len =
                (UINT2) (IP6_BUF_DATA_LEN (pFragQue->pBuf) - u4Offset);
            {
                UINT4               u4_first_frag_off =
                    *pu4Len + sizeof (tIp6FragHdr);
                IP6_BUF_GET_2_BYTE (pBuf, (UINT4) IP6_FOUR, u2PayloadLen);
                IP6_BUF_READ_OFFSET (pBuf) = u4_first_frag_off;
            }

            /*
             * calculate length of original packet as specified in RFC 1883
             */

            pIp6Reasm->u4PktLen = u2PayloadLen -
                (UINT4) u2FragLen - BYTE_LENGTH +
                IN_MULTIPLES_OF_8_OCTETS (u2_last_offset) +
                u2_last_frag_len + sizeof (tIp6Hdr);

            if ((pIp6Reasm->u4PktLen - sizeof (tIp6Hdr)) > MAX_REASM_SIZE)
            {
                IP6_TRC_ARG1 (pIp6Cxt->u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC,
                              IP6_NAME, "IP6FRAG:Ip6HandleReasm: Calculated "
                              "length of reassembled pkt = %d",
                              pIp6Reasm->u4PktLen);
                IP6_TRC_ARG1 (pIp6Cxt->u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC,
                              IP6_NAME, "is greater than Max allowed = %d\n",
                              MAX_REASM_SIZE);

                Icmp6SendErrMsg (pIf6, pIp6, ICMP6_PKT_TOO_BIG, 1, 0, pBuf);

                IP6IF_INC_REASM_FAILS (pIf6);
                IP6SYS_INC_REASM_FAILS (pIp6Cxt->Ip6SysStats);

                return NULL;
            }

        }
        IP6_BUF_READ_OFFSET (pFragQue->pBuf) = u4Offset;
    }
    else
    {
        pIp6Reasm->u4CurLen += u2FragLen;
    }

    if (u1MFlag == 0)
    {
        pFragQue = (tIp6FragQueEntry *) TMO_DLL_First (&(pIp6Reasm->fragQue));
        if (pFragQue == NULL)
        {
            IP6IF_INC_REASM_FAILS (pIf6);
            IP6SYS_INC_REASM_FAILS (pIp6Cxt->Ip6SysStats);
            Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE,
                           IP6_FRAG_SUBMODULE);
            return NULL;
        }

        /*
         * check if the first fragment that we have in the queue is the
         * first fragment of the original packet
         */

        if (!pFragQue->u2StartOff)
        {
            u4Offset = IP6_BUF_READ_OFFSET (pFragQue->pBuf);
            IP6_BUF_GET_2_BYTE (pFragQue->pBuf, (UINT4) IP6_FOUR, u2PayloadLen);
            u2FirstFragLen = (UINT2) (IP6_BUF_DATA_LEN (pFragQue->pBuf) -
                                      u4Offset);

            /*
             * calculate length of original packet as specified in RFC 1883
             */

            pIp6Reasm->u4PktLen = u2PayloadLen - u2FirstFragLen - BYTE_LENGTH +
                u2StartOffset + u2FragLen + sizeof (tIp6Hdr);
            IP6_BUF_READ_OFFSET (pFragQue->pBuf) = u4Offset;

            if ((pIp6Reasm->u4PktLen - sizeof (tIp6Hdr)) > MAX_REASM_SIZE)
            {
                IP6_TRC_ARG1 (pIp6Cxt->u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC,
                              IP6_NAME, "IP6FRAG:Ip6HandleReasm: Calculated "
                              "length of reassembled pkt = %d",
                              pIp6Reasm->u4PktLen);
                IP6_TRC_ARG1 (pIp6Cxt->u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC,
                              IP6_NAME, "is greater than Max allowed = %d\n",
                              MAX_REASM_SIZE);

                Icmp6SendErrMsg (pIf6, pIp6, ICMP6_PKT_TOO_BIG, 1, 0, pBuf);

                IP6IF_INC_REASM_FAILS (pIf6);
                IP6SYS_INC_REASM_FAILS (pIp6Cxt->Ip6SysStats);

                return NULL;
            }
        }
    }

    IP6_TRC_ARG3 (pIp6Cxt->u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                  "IP6FRAG:Ip6HandleReasm: Entry Id = %d Frag Len Recvd = %d Total Pkt Len = %d\n",
                  pIp6Reasm->u4Id, pIp6Reasm->u4CurLen, pIp6Reasm->u4PktLen);

    /*
     * if received length matches the total packet length, reassemble the
     * fragments and hand back to calling function
     */

    if (pIp6Reasm->u4CurLen == pIp6Reasm->u4PktLen)
    {
        IP6IF_INC_REASM_OKS (pIf6);
        IP6SYS_INC_REASM_OKS (pIp6Cxt->Ip6SysStats);

        return (Ip6ReasmFrags (pu1NextHdr, pu4Len, pIp6Reasm));
    }

    return NULL;
}

/******************************************************************************
 * DESCRIPTION : This routine is called after the expiry of reassembly timer.
 *               After the expiry of timer, it is checked whether first fragment
 *               has come or not. If it has, it returns ICMP6 reassembly time
 *               exceeded message back to the source.
 * INPUTS      : Interface record (tIp6If    *pIf6)             &
 *               IPv6 main header (tIp6Hdr   *pIp6)             &
 *               expired timer    (tIp6Timer *pTimer)
 * OUTPUTS     : None
 * RETURNS     : None
 * NOTES       :
 ******************************************************************************/

VOID
Ip6ReasmTimeout (tIp6Timer * pTimer)
{
    tIp6Cxt            *pIp6Cxt = NULL;
    tIp6ReasmEntry     *pIp6Reasm = NULL;
    tIp6FragQueEntry   *pFrag = NULL;
    tIp6Hdr            *pIp6 = NULL;
    tIp6Hdr             ip6Hdr;
    tIp6If             *pIf6 = NULL;
    UINT4               u4ContextId = VCM_INVALID_VC;

    pIp6Reasm =
        (tIp6ReasmEntry *) (VOID *) GET_REASM_PTR_FROM_REASM_LST (pTimer);

    if (pTimer == NULL)
    {
        IP6_GBL_TRC_ARG (IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                         "IP6FRAG:Ip6ReasmTimeout: Received timer node is NULL\n");
        IP6_GBL_TRC_ARG5 (IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                          "%s timer error occurred, Type = %d Module = 0x%X TimerList = 0x%d TimerNode = %p \n",
                          ERROR_FATAL_STR, ERR_TIMER_TIMEOUT,
                          gIp6GblInfo.Ip6TimerListId, NULL, IP6_FRAG_SUBMODULE);
        return;
    }

    /*
     * if the first fragment of the original packet is received, we
     * need to send back an ICMP error message
     */

    pFrag = (tIp6FragQueEntry *) TMO_DLL_First (&(pIp6Reasm->fragQue));

    if ((pFrag != NULL) && (pFrag->u2StartOff == 0))
    {
        if (IP6_BUF_DATA_LEN (pFrag->pBuf) >= IP6_HDR_LEN)
        {
            pIp6 = (tIp6Hdr *) Ip6BufRead (pFrag->pBuf,
                                           (UINT1 *) &ip6Hdr, 0,
                                           sizeof (tIp6Hdr), 0);
        }

        if (pIp6 != NULL)
        {
            /* A valid interface index is filled in the reasm entry. 
             * Hence pIf6 will not be null */
            pIf6 = Ip6ifGetEntry (pIp6Reasm->u4Index);
            if (pIf6 != NULL)
            {
                pIp6Cxt = pIf6->pIp6Cxt;
                u4ContextId = pIp6Cxt->u4ContextId;

                Icmp6SendErrMsg (pIf6, pIp6, ICMP6_TIME_EXCEEDED,
                                 ICMP6_FRAG_REASM_TMEXCEEDED, 0, pFrag->pBuf);

                IP6IF_INC_REASM_FAILS (pIf6);
                IP6SYS_INC_REASM_FAILS (pIp6Cxt->Ip6SysStats);
            }

            pFrag->pBuf = NULL;
        }
        else
        {
            IP6_GBL_TRC_ARG (IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                             "IP6FRAG:Ip6ReasmTimeout: First pkt in frag queue does not have IP6 main hdr\n");
            IP6_GBL_TRC_ARG3 (IP6_MOD_TRC, BUFFER_TRC, IP6_NAME,
                              "IP6FRAG:Ip6ReasmTimeout: %s buf err:, Type = %d Bufptr = %p",
                              ERROR_MINOR, ERR_BUF_READ, pFrag->pBuf);
            Ip6BufRelease (VCM_INVALID_VC, pFrag->pBuf, FALSE,
                           IP6_FRAG_SUBMODULE);
        }

        TMO_DLL_Delete (&(pIp6Reasm->fragQue), &pFrag->link);

        if (Ip6RelMem
            (u4ContextId, (UINT2) gIp6GblInfo.i4Ip6FragId,
             (UINT1 *) pFrag) == IP6_FAILURE)
        {
            IP6_TRC_ARG4 (u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                          "IP6FRAG:Ip6ReasmTimeout: %s mem err:, Type = %d  PoolId = %d Memptr = %p",
                          ERROR_FATAL_STR, ERR_MEM_RELEASE, 0,
                          gIp6GblInfo.i4Ip6FragId);
            return;
        }

    }

    /*
     * Release all the packets waiting for reassembly and free up the
     * associated memory
     */

    while ((pFrag = (tIp6FragQueEntry *) TMO_DLL_First
            (&(pIp6Reasm->fragQue))) != NULL)
    {
        if (pFrag->pBuf != NULL)
        {
            if (Ip6BufRelease
                (u4ContextId, pFrag->pBuf, FALSE,
                 IP6_FRAG_SUBMODULE) == IP6_FAILURE)
            {
                IP6_TRC_ARG (u4ContextId, IP6_MOD_TRC,
                             BUFFER_TRC | ALL_FAILURE_TRC, IP6_NAME,
                             "IP6FRAG:Ip6ReasmTimeout: Buffer release failed for Pkt awaiting reassembly\n");
                IP6_TRC_ARG3 (u4ContextId, IP6_MOD_TRC, BUFFER_TRC, IP6_NAME,
                              "%s buf err: Type = %d Bufptr = %p",
                              ERROR_FATAL_STR, ERR_BUF_RELEASE, pFrag->pBuf);
            }
        }

        TMO_DLL_Delete (&(pIp6Reasm->fragQue), &pFrag->link);

        if (Ip6RelMem
            (u4ContextId, (UINT2) gIp6GblInfo.i4Ip6FragId,
             (UINT1 *) pFrag) == IP6_FAILURE)
        {
            IP6_TRC_ARG4 (u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                          "IP6FRAG:Ip6ReasmTimeout: %s mem err:Type = %d  PoolId = %d Memptr = %p",
                          ERROR_FATAL_STR, ERR_MEM_RELEASE, 0,
                          gIp6GblInfo.i4Ip6FragId);
            return;
        }

    }

    /* Mark this Reassembly entry as unused */

    MEMSET (pIp6Reasm, 0, sizeof (tIp6ReasmEntry));
    pIp6Reasm->u1State = ENTRY_UNUSED;

}

/******************************************************************************
 * DESCRIPTION : This routine clears all the reassembly entries present for 
 *               the given context. If the reassembly entry has any fragmented
 *               packets they are also freed.
 * INPUTS      : pIp6Cxt - The context structure.                  
 * OUTPUTS     : None
 * RETURNS     : None
 ******************************************************************************/
VOID
Ip6ClearReasmEntries (tIp6Cxt * pIp6Cxt)
{
    tIp6ReasmEntry     *pIp6Reasm = NULL;
    tIp6FragQueEntry   *pFrag = NULL;
    UINT1               u1Cntr = 0;

    for (u1Cntr = 0; u1Cntr < MAX_IP6_REASM_ENTRIES_LIMIT; u1Cntr++)
    {
        pIp6Reasm = pIp6Cxt->apIp6Ream[u1Cntr];

        if (pIp6Reasm->timer.u1Id != 0)
        {
            Ip6TmrStop (pIp6Cxt->u4ContextId, FRAG_REASM_TIMER_ID,
                        gIp6GblInfo.Ip6TimerListId,
                        &(pIp6Reasm->timer.appTimer));

            /* Release the fragmented packets present in fragQue */
            do
            {
                pFrag = (tIp6FragQueEntry *)
                    TMO_DLL_First (&(pIp6Reasm->fragQue));

                if (pFrag != NULL)
                {
                    Ip6BufRelease (pIp6Cxt->u4ContextId, pFrag->pBuf, FALSE,
                                   IP6_FRAG_SUBMODULE);

                    TMO_DLL_Delete (&(pIp6Reasm->fragQue), &pFrag->link);

                    if (Ip6RelMem (pIp6Cxt->u4ContextId,
                                   (UINT2) gIp6GblInfo.i4Ip6FragId,
                                   (UINT1 *) pFrag) == IP6_FAILURE)
                    {
                        IP6_TRC_ARG4 (pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                                      DATA_PATH_TRC, IP6_NAME,
                                      "Ip6ClearReasmEntries: %s mem err:, "
                                      "Type = %d  PoolId = %d Memptr = %p",
                                      ERROR_FATAL_STR, ERR_MEM_RELEASE, 0,
                                      gIp6GblInfo.i4Ip6FragId);
                    }
                }

            }
            while (pFrag != NULL);
        }

        if (Ip6RelMem
            (pIp6Cxt->u4ContextId, (UINT2) gIp6GblInfo.i4Ip6ReasmPoolId,
             (UINT1 *) pIp6Reasm) == IP6_FAILURE)
        {
            IP6_TRC_ARG4 (pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                          DATA_PATH_TRC, IP6_NAME,
                          "Ip6ClearReasmEntries: %s mem err:, "
                          "Type = %d  PoolId = %d Memptr = %p",
                          ERROR_FATAL_STR, ERR_MEM_RELEASE, 0,
                          gIp6GblInfo.i4Ip6ReasmPoolId);
        }
    }

    return;
}

/***************************** END OF FILE **********************************/
