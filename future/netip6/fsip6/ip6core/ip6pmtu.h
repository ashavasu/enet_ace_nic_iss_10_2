/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ip6pmtu.h,v 1.7 2010/11/19 10:37:55 siva Exp $
 *
 * Description:This file contains the exported definitions and  
 *             macros of IPV6                                    
 *
 *******************************************************************/

#ifndef IP6_PMTU_H
#define IP6_PMTU_H
typedef struct
{
    tRBNodeEmbd     RBNode;
    tIp6Cxt        *pIp6Cxt;
    tOsixSysTime    PmtuTimeStamp;
    tIp6Addr        Ip6Addr;
    UINT4           u4Pmtu;
}
tPmtuEntry;

extern INT4       (*gIp6PmtuHandler)(UINT4, tIp6Addr, UINT4);
    
#define  V6_HOST_PREFIX_LENGTH     128
#define  IP6_PMTU_HASH_TABLE_SIZE  255 
#define  IP6_PMTU_DEF_TIME_OUT_INTERVAL 60

#endif    /* IP6_PMTU_H */
    
