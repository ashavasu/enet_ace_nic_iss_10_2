
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ip6tunnel.c,v 1.16 2015/06/30 06:06:15 siva Exp $
 *
 * Description:
 *
 *******************************************************************/

/*
 *----------------------------------------------------------------------------- 
 *    FILE NAME                    :    ip6tunnel.c
 *    PRINCIPAL AUTHOR             :    Aricent Inc.
 *
 *    SUBSYSTEM NAME               :    IPv6
 *
 *    MODULE NAME                  :    IP6 Core Submodule
 *
 *    LANGUAGE                     :    C
 *
 *    TARGET ENVIRONMENT           :    UNIX
 *
 *    DATE OF FIRST RELEASE        :    DDD-MM-1996
 *
 *    DESCRIPTION                  :    This file contains routines for
 *                                      transmitting IPv6 datagrams through
 *                                      tunnels.
 *
 *----------------------------------------------------------------------------- 
 */
#include "ip6inc.h"
#ifdef TUNNEL_WANTED

/******************************************************************************
 * DESCRIPTION : This function is called from Lower Layer to intimate any
 *               changes in Tunnel Parameters.
 *
 * INPUTS      : u4IfIndex, u4TnlDir, u4TnlDirFlag, u4EncapOption, u4EncapLimit,
 *               u4Mask
 *
 * OUTPUTS     : None
 *
 * RETURNS     : IP6_SUCCESS/IP6_FAILURE
 *               
 *
 * NOTES       : 
 ******************************************************************************/

INT4
Ip6TunlifUpdate (UINT4 u4IfIndex, UINT1 u1TnlDir, UINT1 u1TnlDirFlag,
                 UINT1 u1EncapOption, UINT1 u1EncapLimit, UINT1 u1HopLimit,
                 UINT4 u4Mask, UINT4 *pu4LocalAddr, UINT4 *pu4RemoteAddr,
                 UINT4 u4TnlType)
{
    tIp6If             *pIf6 = NULL;
    UINT4               u4ContextId;

    VcmGetContextIdFromCfaIfIndex (u4IfIndex, &u4ContextId);
    if (gIp6GblInfo.i4Ip6Status == IP6_FAILURE)
    {
        IP6_TRC_ARG (u4ContextId, IP6_MOD_TRC, CONTROL_PLANE_TRC, IP6_NAME,
                     "IP6IF: Ip6TunlifUpdate: IP6 Task Not Initialised !!!\n");
        return (IP6_FAILURE);
    }

    if (IP6_TASK_LOCK () == SNMP_FAILURE)
    {
        IP6_TRC_ARG (u4ContextId, IP6_MOD_TRC, CONTROL_PLANE_TRC, IP6_NAME,
                     "IP6IF: Ip6TunlifUpdate: IP6 Task Lock Failed !!!\n");
        return (IP6_FAILURE);
    }

    pIf6 = Ip6ifGetEntry (u4IfIndex);
    if (pIf6 == NULL)
    {
        /* No matching interface exists in IP6 */
        IP6_TRC_ARG1 (u4ContextId, IP6_MOD_TRC, CONTROL_PLANE_TRC, IP6_NAME,
                      "IP6IF: Ip6TunlifUpdate: Invalid IF index %d\n",
                      u4IfIndex);
        IP6_TASK_UNLOCK ();
        return IP6_FAILURE;
    }

    if (pIf6->u1IfType != IP6_TUNNEL_INTERFACE_TYPE)
    {
        IP6_TRC_ARG1 (u4ContextId, IP6_MOD_TRC, CONTROL_PLANE_TRC, IP6_NAME,
                      "IP6IF: Ip6TunlifUpdate: Not Tunnel Interface %d\n",
                      u4IfIndex);
        IP6_TASK_UNLOCK ();
        return IP6_FAILURE;
    }

    if (u4Mask & IP6_TNL_CHG_MASK_DIR)
    {
        pIf6->pTunlIf->u1TunlDir = u1TnlDir;
    }

    if (u4Mask & IP6_TNL_CHG_MASK_DIR_FLAG)
    {
        pIf6->pTunlIf->u1TunlFlag = u1TnlDirFlag;
    }

    if (u4Mask & IP6_TNL_CHG_MASK_ENCAP_OPT)
    {
        pIf6->pTunlIf->u1EncapFlag = u1EncapOption;
    }

    if (u4Mask & IP6_TNL_CHG_MASK_ENCAP_LMT)
    {
        pIf6->pTunlIf->u1TunlEncaplmt = u1EncapLimit;
    }

    if (u4Mask & IP6_TNL_CHG_MASK_END_POINT)
    {
        /*  Get all Tunnel params from CFA tunnel table */
        Ip6GetTunnelParams (pIf6->u4Index, pIf6->pTunlIf);

        /* Set IP address and type as per notification */
        SET_ADDR_UNSPECIFIED (pIf6->pTunlIf->tunlSrc);
        SET_ADDR_UNSPECIFIED (pIf6->pTunlIf->tunlDst);
        PTR_ASSIGN4 (&(pIf6->pTunlIf->tunlSrc.u1_addr[12]), *pu4LocalAddr);
        PTR_ASSIGN4 (&(pIf6->pTunlIf->tunlDst.u1_addr[12]), *pu4RemoteAddr);
        pIf6->pTunlIf->u1TunlType = u4TnlType;
    }

    if (u4Mask & IP6_TNL_CHG_MASK_HOP_LIMIT)
    {
        pIf6->pTunlIf->u2HopLimit = u1HopLimit;
    }

    IP6_TASK_UNLOCK ();
    return IP6_SUCCESS;
}

/****************************************************************************
 Function    :  Ip6Tunl6Send 
 Input       :  Pointer to the interface on which to send the pkt.
                Cru buffer to enqueue
 Output      :  None.
 Returns     :  None.
****************************************************************************/
VOID
Ip6Tunl6Send (tIp6If * pIf6, tCRU_BUF_CHAIN_HEADER * pBuf)
{
    tIp6TunlIf         *pTunlIf6 = NULL;
    tIp6Hdr             ip6Hdr, *pOrigIp6 = NULL;
    UINT1               u1DstHdrLen = 0;
    UINT1               u1Proto = NH_IPV6;
    UINT1               u1Hlim = 0, u1Tel = 0;
    tEncapHdr           EncapHdr, *pEncap = NULL;
    UINT2               u2Len = 0;

    pTunlIf6 = pIf6->pTunlIf;

    if (pTunlIf6->u1EncapFlag == IPV6_ENCAP_OPT_YES)
    {
        u1Tel = pTunlIf6->u1TunlEncaplmt;
    }

    /* Get the ipv6 header from the buffer */
    pOrigIp6 = (tIp6Hdr *) Ip6BufRead (pBuf, (UINT1 *) &ip6Hdr, 0,
                                       sizeof (tIp6Hdr), 0);
    if (pOrigIp6 == NULL)
    {
        Ip6BufRelease (pIf6->pIp6Cxt->u4ContextId, pBuf, FALSE,
                       IP6_MAIN_SUBMODULE);
        return;
    }
    if (pOrigIp6->u1Nh == NH_DEST_HDR)
    {
        /* pBuf is a tunneled packet,so get the encapsulation lmt from pBuf */
        if ((pEncap = (tEncapHdr *) Ip6BufRead (pBuf, (UINT1 *) &EncapHdr,
                                                sizeof (tIp6Hdr),
                                                sizeof (tEncapHdr),
                                                FALSE)) == 0)
        {
            Ip6BufRelease (pIf6->pIp6Cxt->u4ContextId, pBuf, FALSE,
                           IP6_MAIN_SUBMODULE);
            return;
        }
        IP6_BUF_READ_OFFSET (pBuf) =
            IP6_BUF_READ_OFFSET (pBuf) - sizeof (tIp6Hdr) - sizeof (tEncapHdr);

        if (pEncap->u1EncapOpt == DEST_HDR_OPTION_ENCAP)
        {
            u1Tel = pEncap->u1Encaplmt;
            if (u1Tel == 1)
            {
                /* tunnel option in the original pkt is one, 
                 * so send an ICMP error message & drop the packet 
                 */
                Icmp6SendErrMsg (pIf6, pOrigIp6, ICMP6_PKT_PARAM_PROBLEM, 0, 0,
                                 pBuf);
                return;
            }
            u1Tel--;
        }
    }
    if (u1Tel)
    {
        Ip6TunlOption (pBuf, u1Tel);
        u1Proto = NH_DEST_HDR;
        u1DstHdrLen = sizeof (tEncapHdr);
    }

    u2Len = (UINT2) (NTOHS (pOrigIp6->u2Len) + u1DstHdrLen + sizeof (tIp6Hdr));
    u1Hlim = Ip6GetHlim ();

    Ip6SendInCxt (pIf6->pIp6Cxt, NULL, &(pTunlIf6->tunlSrc),
                  &(pTunlIf6->tunlDst), u2Len, u1Proto, pBuf, u1Hlim);

}

/****************************************************************************
 Function    :  Ip6TunlOption
 Input       :  Cru buffer
                Encapsulation Option value
 Output      :  None.
 Returns     :  None.
****************************************************************************/
VOID
Ip6TunlOption (tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 u1Tel)
{
    tEncapHdr           EncapHdr;
    UINT1               u1Copy = 0;

    EncapHdr.u1NextHdr = NH_IPV6;
    EncapHdr.u1HdrLen = 0;
    EncapHdr.u1EncapOpt = DEST_HDR_OPTION_ENCAP;
    EncapHdr.u1EncapLen = 1;
    EncapHdr.u1Encaplmt = u1Tel;
    EncapHdr.u1OptType = 1;
    EncapHdr.u1OptLen = 1;
    EncapHdr.u1PadN = 1;

    /* Prepend the buffer to fill the extension hdr */
    Ip6BufPptr (pBuf, (UINT1 *) &EncapHdr, sizeof (tEncapHdr), &u1Copy);
    return;

}

/****************************************************************************
 Function    :  Ip6Tunl6Recv 
 Input       :  Pointer to the interface on which pkt is received.
                Cru buffer
 Output      :  None.
 Returns     :  IP6_SUCCESS/IP6_FAILURE
****************************************************************************/
VOID
Ip6Tunl6Recv (tIp6If * pIf6, tCRU_BUF_CHAIN_HEADER * pBuf)
{
    tCRU_BUF_CHAIN_HEADER *pFragBuf = NULL;
    tIp6Hdr             ip6Hdr, *pIp6 = NULL;
    UINT1               u1DstHdrLen = 0;
    INT4                i4RetVal = 0;

    /* Get the ipv6 header from the buffer */
    pIp6 = (tIp6Hdr *) Ip6BufRead (pBuf, (UINT1 *) &ip6Hdr, 0,
                                   sizeof (tIp6Hdr), 0);
    if (pIp6 == NULL)
    {
        Ip6BufRelease (pIf6->pIp6Cxt->u4ContextId, pBuf, FALSE,
                       IP6_MAIN_SUBMODULE);
        return;
    }
    if (pIp6->u1Nh == NH_DEST_HDR)
    {
        u1DstHdrLen = sizeof (tEncapHdr);
    }

    i4RetVal =
        CRU_BUF_Fragment_BufChain (pBuf, sizeof (tIp6Hdr) + u1DstHdrLen,
                                   &pFragBuf);
    Ip6BufRelease (pIf6->pIp6Cxt->u4ContextId, pBuf, FALSE, IP6_MAIN_SUBMODULE);
    if (pFragBuf == NULL)
    {
        return;
    }

    pIf6 = Ip6TunnelEntryGetInCxt (pIf6->pIp6Cxt, &pIp6->srcAddr);
    if (pIf6 == NULL)
    {
        return;
    }

    (IP6_IF_STATS (pIf6))->u4InRcvs++;
    IP6_BUF_READ_OFFSET (pFragBuf) = 0;
    IP6_BUF_WRITE_OFFSET (pFragBuf) = 0;

    /* Post the packet back to ip6 task */
    Ip6Input (pIf6, pFragBuf, NULL);
    UNUSED_PARAM (i4RetVal);
}

/******************************************************************************
 * DESCRIPTION : Checks Whether the Tunnel Entry exixts or not.  
 *              
 *
 * INPUTS      :  Tunnel Index , Tunnel Destination Address
 *
 * OUTPUTS     : NONE. 
 *
 * RETURNS     : TRUE if entry exists in Tunnel Table, FALSE Otherwise.
 *               
 * NOTES       :
 *****************************************************************************/

INT4
Ip6TunlEntryExists (i4TunlIndex, pTunlDstAddress)
     INT4                i4TunlIndex;
     tIp6Addr           *pTunlDstAddress;
{
    tIp6TunlIf         *pTunlIf = NULL;

    if (gIp6GblInfo.apIp6If[i4TunlIndex]->u1IfType != IP6_TUNNEL_INTERFACE_TYPE)
    {
        return FALSE;
    }

    pTunlIf = gIp6GblInfo.apIp6If[i4TunlIndex]->pTunlIf;

    if (pTunlIf->u1TunlType == IPV6_OVER_IPV6_TUNNEL)
    {
        return (Ip6AddrMatch (&pTunlIf->tunlDst, pTunlDstAddress,
                              V6_HOST_PREFIX_LENGTH));
    }
    else                        /* IPv4 Tunnel */
    {
        if (pTunlIf->tunlDst.u4_addr[IP6_THREE] ==
            NTOHL (pTunlDstAddress->u4_addr[IP6_THREE]))
        {
            return TRUE;
        }
    }

    return FALSE;
}

/****************************************************************************
 Function    : Ip6TunnelEntryGetInCxt
 INPUTS      : Ip6 Destination Address (pDestAddr)
 Output      : None.
 Returns     : pointer to Interface.
 NOTE        : This routine takes the Ip6 Destination Address and checks
               whether there exists a matching tunnel (IPV6_OVER_IPV6) for
               the destination.
****************************************************************************/
tIp6If             *
Ip6TunnelEntryGetInCxt (tIp6Cxt * pIp6Cxt, tIp6Addr * pDestAddr)
{
    tIp6If             *pIf6 = NULL;
    UINT4               u4Index = 1;

    IP6_IF_SCAN (u4Index, (UINT4) IP6_MAX_LOGICAL_IF_INDEX)
    {
        pIf6 = gIp6GblInfo.apIp6If[u4Index];
        if (pIf6 == NULL)
        {
            continue;
        }
        if ((pIf6->u1IfType == IP6_TUNNEL_INTERFACE_TYPE) &&
            (pIf6->pTunlIf->u1TunlType == IPV6_OVER_IPV6_TUNNEL) &&
            (pIf6->pIp6Cxt == pIp6Cxt) &&
            (Ip6AddrMatch (pDestAddr, &pIf6->pTunlIf->tunlDst,
                           IP6_ADDR_MAX_PREFIX) == TRUE))
        {
            return pIf6;
        }
    }
    return NULL;
}

/****************************************************************************
 Function    : Ip6Tunl4Send 
 INPUTS      : The interface pointer (pIf6), the destination IPv6 address
               (pDst6), the length of the datagram (u4Len) and
               the buffer (pBuf)
 Output      : None.
 Returns     : IP6_SUCCESS/IP6_FAILURE
****************************************************************************/
VOID
Ip6Tunl4Send (tIp6If * pIf6, tIp6Addr * pDst6, tNd6CacheEntry * pNd6CacheEntry,
              UINT4 u4Len, tCRU_BUF_CHAIN_HEADER * pBuf)
{
    tIp6Cxt            *pIp6Cxt = NULL;
    UINT4               u4DstIp;
    UINT4               u4Mtu;
    UINT2               u2DontFrag = 0;
    UINT2               u2Proto = IP6_PTCL;

    UNUSED_PARAM (pNd6CacheEntry);

    pIp6Cxt = pIf6->pIp6Cxt;

    /* Configured tunneling can be for ipv4 compatible ipv6 addresses
     * also. Based on the lower layer if it is auto tunnel interface v4 
     * destination is extracted from v6 address other wise it is taken 
     * from configured tunnel table.
     */
    if (pIf6->pTunlIf->u1TunlType == IPV6_AUTO_COMPAT)
    {
        /* Automatic Compatible Tunnel. Get the v4 Destination address
         * from the Compatible V6 Destination Address */
        if (IS_ADDR_MULTI (*pDst6))
        {
            /* If the Destination happens to be multicast address and
             * if v4 Destination address is configured, then the packets
             * are sent to that address. */
            u4DstIp = IP6_TUNLIF_DST (pIf6);
            u4DstIp = HTONL (u4DstIp);
        }
        else
        {
            if (IS_ADDR_V4_COMPAT (*pDst6) == 0)
            {
                /* Destination is not V4 Compatible. Reject it. */
                IP6IF_INC_OUT_DISCARDS (pIf6);
                IP6SYS_INC_OUT_DISCARDS (pIp6Cxt->Ip6SysStats);
                Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE,
                               IP6_IF_SUBMODULE);
                return;
            }
            u4DstIp = HTONL (pDst6->u4_addr[IP6_THREE]);
        }

        if (Ip6ValidateIpv4Addr (u4DstIp, pIf6->u4Index) == IP6_FAILURE)
        {
            IP6_TRC_ARG1 (pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                          DATA_PATH_TRC | BUFFER_TRC, IP6_NAME,
                          "IP6OUT:Ip6Tunl4Send:Destination address is "
                          "not a valid address %x\n", u4DstIp);
            IP6IF_INC_OUT_DISCARDS (pIf6);
            IP6SYS_INC_OUT_DISCARDS (pIp6Cxt->Ip6SysStats);
            Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE, IP6_IF_SUBMODULE);
            return;
        }

        u4Mtu = IP6_MIN_MTU;
    }
    else if (pIf6->pTunlIf->u1TunlType == IPV6_SIX_TO_FOUR)
    {
        /* Automatic SixToFour Tunnel. Get the v4 Destination address
         * from the 6to4 V6 Destination Address */
        if (IS_ADDR_MULTI (*pDst6))
        {
            /* If the Destination happens to be multicast address and
             * if v4 Destination address is configured, then the packets
             * are sent to that address. */
            u4DstIp = IP6_TUNLIF_DST (pIf6);
            u4DstIp = HTONL (u4DstIp);
        }
        else
        {
            if (IS_ADDR_6to4 (*pDst6) == 0)
            {
                /* Destination is not V4 Compatible. Reject it. */
                IP6IF_INC_OUT_DISCARDS (pIf6);
                IP6SYS_INC_OUT_DISCARDS (pIp6Cxt->Ip6SysStats);
                Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE,
                               IP6_IF_SUBMODULE);
                return;
            }

            MEMCPY ((UINT1 *) &u4DstIp, &pDst6->u1_addr[IP6_TWO], IP6_FOUR);
            u4DstIp = HTONL (u4DstIp);
        }

        if (Ip6ValidateIpv4Addr (u4DstIp, pIf6->u4Index) == IP6_FAILURE)
        {
            IP6_TRC_ARG1 (pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                          DATA_PATH_TRC | BUFFER_TRC, IP6_NAME,
                          "IP6OUT:Ip6Tunl4Send:Destination address is "
                          "not a valid address %x\n", u4DstIp);
            IP6IF_INC_OUT_DISCARDS (pIf6);
            IP6SYS_INC_OUT_DISCARDS (pIp6Cxt->Ip6SysStats);
            Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE, IP6_IF_SUBMODULE);
            return;
        }

        u4Mtu = IP6_MIN_MTU;
    }
    else if (pIf6->pTunlIf->u1TunlType == IPV6_ISATAP_TUNNEL)
    {
        /* Automatic isatap Tunnel. Get the v4 Destination address
         * from the isatap V6 Destination Address */
        if (IS_ADDR_MULTI (*pDst6))
        {
            /* If the Destination happens to be multicast address and
             * if v4 Destination address is configured, then the packets
             * are sent to that address. */
            u4DstIp = IP6_TUNLIF_DST (pIf6);
            u4DstIp = HTONL (u4DstIp);
        }
        else
        {
            if (IS_ADDR_ISATAP (*pDst6) == 0)
            {
                /* Destination is not isatap. Reject it. */
                IP6IF_INC_OUT_DISCARDS (pIf6);
                IP6SYS_INC_OUT_DISCARDS (pIp6Cxt->Ip6SysStats);
                Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE,
                               IP6_IF_SUBMODULE);
                return;
            }

            u4DstIp = HTONL (pDst6->u4_addr[IP6_THREE]);
        }

        if (Ip6ValidateIpv4Addr (u4DstIp, pIf6->u4Index) == IP6_FAILURE)
        {
            IP6_TRC_ARG1 (pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                          DATA_PATH_TRC | BUFFER_TRC, IP6_NAME,
                          "IP6OUT:Ip6Tunl4Send:Destination address is "
                          "not a valid address %x\n", u4DstIp);
            IP6IF_INC_OUT_DISCARDS (pIf6);
            IP6SYS_INC_OUT_DISCARDS (pIp6Cxt->Ip6SysStats);
            Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE, IP6_IF_SUBMODULE);
            return;
        }

        u4Mtu = IP6_MIN_MTU;
    }
    else
    {
        /* Configured Tunnel. */
        if (pIf6->pTunlIf->u1TunlType == IPV6_GRE_TUNNEL)
        {
            u2Proto = IP6_GRE_PTCL;
        }

        u4DstIp = IP6_TUNLIF_DST (pIf6);
        u4DstIp = HTONL (u4DstIp);
        u4Mtu = IP6_TUNLIF_MTU (pIf6);
        if (u4Mtu < IP6_MIN_MTU)
        {
            u4Mtu = IP6_MIN_MTU;
        }
    }

    /* if packet length greater than minimum mtu and less than PMtu
     * need to set don't fragment bit to prevent Ipv4 fragmentation.
     */
    if (((u4Mtu - IP6_TUNL_HDR_LEN) > IP6_MIN_MTU)
        && (u4Len < (u4Mtu - IP6_TUNL_HDR_LEN)))
    {
        u2DontFrag = 0x0001;
    }

    if (Ip6TunnelOutput (u4DstIp, IP6_DEF_TOS /* 0 */ , u2Proto,
                         pIf6->pTunlIf->u2HopLimit, pBuf, (UINT2) u4Len,
                         0 /* i2Id */ , (UINT1) u2DontFrag, 0,
                         pIf6->u4Index) == IP6_FAILURE)
    {
        IP6_TRC_ARG (pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                     DATA_PATH_TRC | BUFFER_TRC, IP6_NAME,
                     "IP6OUT:Ip6Tunl4Send:Failure in sending the "
                     "packet to v4 stack \n");
        Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE, IP6_IF_SUBMODULE);
        IP6_TRC_ARG2 (pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                      MGMT_TRC | BUFFER_TRC, IP6_NAME,
                      "IP6OUT:Ip6Tunl4Send:%s gen err:, Type = %d  ",
                      ERROR_FATAL_STR, ERR_GEN_INVALID_VAL);
    }

    return;
}

/******************************************************************************
 * DESCRIPTION : This routine is called to get and validate the Tunnel Interface
 *               matching the given Remote end point and Local end point address
 *               of the tunnel.
 *
 * INPUTS      : u4LocalAddr - Source Address.
 *               u4RemoteAddr - Destination Address.
 *
 * OUTPUTS     : None
 *
 * RETURNS     : Pointer to IPv6 Logical Interface, if found
 *               NULL, if not found
 *
 * NOTES       :
 ******************************************************************************/

tIp6If             *
Ip6GetIfTunlInCxt (tIp6Cxt * pIp6Cxt, UINT4 u4LocalAddr,
                   UINT4 u4RemoteAddr, UINT4 u4TunlIfIndex)
{

    if (u4TunlIfIndex == 0)
    {
        if (pIp6Cxt == NULL)
        {
            return NULL;
        }
        /* Get the Tunnel Interface Index for the given end point. */
        if (Ip6GetTunnelInterfaceInCxt (pIp6Cxt->u4ContextId, u4LocalAddr,
                                        u4RemoteAddr, &u4TunlIfIndex)
            == IP6_FAILURE)
        {
            return NULL;
        }
    }

    if (gIp6GblInfo.apIp6If[u4TunlIfIndex]->u1IfType
        != IP6_TUNNEL_INTERFACE_TYPE)
    {
        return NULL;
    }

    return gIp6GblInfo.apIp6If[u4TunlIfIndex];
}

/******************************************************************************
DESCRIPTION : IPv6 Get HopLimit 

INPUTS      : None

OUTPUTS     : None

RETURNS     :  
******************************************************************************/

UINT1
Ip6GetHlim (VOID)
{
    /* For Host implemention hop limit is ND advertised Hop limit.
     * For Router implementation hop limit is default IPv6 hop
     * limit value from the Assigned Numbers RFC 
     */
    return IP6_DEF_HOP_LIMIT;    /* ND Advertised hop Limit */
}

/******************************************************************************
DESCRIPTION : This function is called when an icmp v4 error occurs for a 
              packet sent over the tunnel. This function maps that to the 
              corresponding V6 error message and sends back it to the originator
              of the IPv6 packet. Incase if it is originated in the same router
              based on the type it takes the actions. 
 
INPUTS      : pBuf       : pointer to the message buffer.
              u4Ipv4Src  : source ipv4 address from which error message 
                           is originated
              u4Ipv4Dst  : Destination ipv4 address of the error message
              i1Type     : icmp v4 message type.
              i1Code     : icmp error message code.

OUTPUTS     : None

RETURNS     : IP6_SUCCESS      -  Initialisation succeeds
              IP6_FAILURE  -  Initialisation fails
******************************************************************************/
VOID
Ip6RcvIcmpv4ErrMsgInCxt (UINT4 u4ContextId, tCRU_BUF_CHAIN_HEADER * pBuf,
                         UINT4 u4Ipv4Src, UINT4 u4Ipv4Dst, INT1 i1Type,
                         INT1 i1Code)
{
    tIp6If             *pIf6 = NULL;
    tIcmp6Params        Icmp6Params;
    tIp6Cxt            *pIp6Cxt = NULL;

    if (gIp6GblInfo.i4Ip6Status == IP6_FAILURE)
    {
        if (Ip6BufRelease (u4ContextId, pBuf, FALSE, IP6_MAIN_SUBMODULE) !=
            IP6_SUCCESS)
        {
            IP6_TRC_ARG (u4ContextId, IP6_MOD_TRC, BUFFER_TRC | ALL_FAILURE_TRC,
                         IP6_NAME,
                         "Ip6RcvIcmpv4ErrMsgInCxt: Deque IPv6 Pkt - "
                         "BufRelease Failed\n");
            IP6_TRC_ARG3 (u4ContextId, IP6_MOD_TRC, BUFFER_TRC, IP6_NAME,
                          "Ip6RcvIcmpv4ErrMsgInCxt: %s buf err: Type = %d "
                          "Bufptr = %p ", ERROR_FATAL_STR, ERR_BUF_RELEASE,
                          pBuf);
        }
        return;
    }

    if (IP6_TASK_LOCK () == SNMP_FAILURE)
    {
        if (Ip6BufRelease (u4ContextId, pBuf, FALSE, IP6_MAIN_SUBMODULE) !=
            IP6_SUCCESS)
        {
            IP6_TRC_ARG (u4ContextId, IP6_MOD_TRC, BUFFER_TRC | ALL_FAILURE_TRC,
                         IP6_NAME,
                         "Ip6RcvIcmpv4ErrMsgInCxt: Deque IPv6 Pkt - "
                         "BufRelease Failed\n");
            IP6_TRC_ARG3 (u4ContextId, IP6_MOD_TRC, BUFFER_TRC, IP6_NAME,
                          "Ip6RcvIcmpv4ErrMsgInCxt: %s buf err: Type = %d "
                          "Bufptr = %p ", ERROR_FATAL_STR, ERR_BUF_RELEASE,
                          pBuf);
        }
        return;

    }

    pIp6Cxt = gIp6GblInfo.apIp6Cxt[u4ContextId];

    /* Now pointer in buffer is to Ipv4 icmp header.
     * Need to extract source ipv6 address, to send
     * the error packet if necessary
     */

    /* Move valid offset to IPv6 header b4 locating the interface */
    if (IP6_BUF_DATA_LEN (pBuf) < IP6_HDR_LEN)
    {
        IP6_TRC_ARG (u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                     "Ip6RcvIcmpv4ErrMsgInCxt:No use of processessing"
                     " error packet as IPv6 info cannot be obtained -"
                     " IPv6 pkt dropped\n");

        if (Ip6BufRelease (u4ContextId, pBuf, FALSE, IP6_MAIN_SUBMODULE) !=
            IP6_SUCCESS)
        {
            IP6_TRC_ARG (u4ContextId, IP6_MOD_TRC, BUFFER_TRC | ALL_FAILURE_TRC,
                         IP6_NAME,
                         "Ip6RcvIcmpv4ErrMsgInCxt: Deque IPv6 Pkt - "
                         "BufRelease Failed\n");
            IP6_TRC_ARG3 (u4ContextId, IP6_MOD_TRC, BUFFER_TRC, IP6_NAME,
                          "Ip6RcvIcmpv4ErrMsgInCxt: %s buf err: Type = %d "
                          "Bufptr = %p ", ERROR_FATAL_STR, ERR_BUF_RELEASE,
                          pBuf);
        }

        IP6_TASK_UNLOCK ();

        return;
    }

    if ((pIf6 = Ip6GetIfTunlInCxt (pIp6Cxt, u4Ipv4Src, u4Ipv4Dst, 0)) == NULL)
    {
        IP6_TRC_ARG (u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                     "Ip6RcvIcmpv4ErrMsgInCxt:IP6 IPv4 err received-IPv6 "
                     "pkt dropped:No ValidIF\n");

        if (Ip6BufRelease (u4ContextId, pBuf, FALSE, IP6_MAIN_SUBMODULE) !=
            IP6_SUCCESS)
        {
            IP6_TRC_ARG (u4ContextId, IP6_MOD_TRC, BUFFER_TRC | ALL_FAILURE_TRC,
                         IP6_NAME,
                         "Ip6RcvIcmpv4ErrMsgInCxt: Deque IPv6 Pkt - "
                         "BufRelease Failed\n");
            IP6_TRC_ARG3 (u4ContextId, IP6_MOD_TRC, BUFFER_TRC, IP6_NAME,
                          "Ip6RcvIcmpv4ErrMsgInCxt: %s buf err: Type = %d "
                          "Bufptr = %p ", ERROR_FATAL_STR, ERR_BUF_RELEASE,
                          pBuf);
        }

        IP6_TASK_UNLOCK ();

        return;
    }

    /* Need to set the buffer offset to IPv6 packet for which error
     * message is received.
     */
    /* Based on the type of message the Ip6 error message will be sent. */
    /*icmp types for ipv4 icmp dest unreach  3   */
    /*                    icmp parm prob     12  */
    /*                    icmp time excceed  11  */
    /*                    icmp quench        4   */
    if ((i1Type == ICMP_DEST_UNREACH) || (i1Type == ICMP_QUENCH))
    {
        /* Send destination unreachable message with appropriate code 
         * to the source ipv6 packet */
        /*-----------------------------------------------------------\
         |    ipv4 code            -       ipv6 code                 |   
         |    0, 1, 11, 12         -         0                       |
         |    2, 3                 -         4                       |
         |    5, 6, 7, 8           -         2                       |
         |    9, 10                -         1                       |
         |    4                    -         pkt too big message     |
         |    source quench        -         3                       |
         `-----------------------------------------------------------'
         */
        IP6_GET_ICMP6_CODE (i1Code);
        Icmp6Params.u1Code = i1Code;
        Icmp6Params.u1Type = ICMP6_DEST_UNREACHABLE;

        IP6_TASK_UNLOCK ();

        return;
    }
    if (i1Type == ICMP_PARAM_PROB)
    {
        /* This error message has come as a result of ipv4 header 
         * constructed by the ipv4 implementation */

        Ip6BufRelease (u4ContextId, pBuf, FALSE, IP6_MAIN_SUBMODULE);
        IP6_TRC_ARG (u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC | BUFFER_TRC,
                     IP6_NAME,
                     "Ip6RcvIcmpv4ErrMsgInCxt:Icmp4 parm prob error for "
                     "Ipv6 tunneled pkt\n");

        IP6_TASK_UNLOCK ();

        return;
    }
    if (i1Type == ICMP_TIME_EXCEED)
    {
        /* Send Ipv6 Icmp time exceeded message to the source ipv6 packet */
        Icmp6Params.u1Type = ICMP6_TIME_EXCEEDED;
        Icmp6Params.u1Code = ICMP6_HOP_LIMIT_EXCEEDED;

        IP6_TASK_UNLOCK ();

        return;
    }
    IP6_SET_COMMAND (pBuf, IP6_ICMPERR_DATA);
    Icmp6Params.u4Index = pIf6->u4Index;
    Icmp6Params.u4ContextId = u4ContextId;

    /* Enque pkt to IP6 task */
    Ip6RcvIcmpErrorPkt (pBuf, &Icmp6Params);

    IP6_TASK_UNLOCK ();
}

/******************************************************************************
 * DESCRIPTION : This function is called to configure the compatible address
 *               for automatic tunneling interface. It takes the ipv4 address
 *               from the interface over which the automatic tunnel is enabled.
 *
 * INPUTS      : pIf6 - Automatic tunnel interface
 *
 * OUTPUTS     : None
 *
 * RETURNS     : IP6_SUCCESS/IP6_FAILURE
 *               
 *
 * NOTES       : 
 ******************************************************************************/
INT4
Ip6ifCreateV4compatv6Addr (tIp6If * pIf6)
{
    tIp6AddrInfo       *pAddrInfo = NULL;

    if ((pAddrInfo = Ip6AddrCreate (pIf6->u4Index, &pIf6->pTunlIf->tunlSrc,
                                    AT_RTENTRY_PFXLEN, ADMIN_UP,
                                    ADDR6_V4_COMPAT, 0,
                                    IP6_ADDR_DYNAMIC)) != NULL)
    {
        pAddrInfo->u1AdminStatus = IP6FWD_ACTIVE;
        /* No need DAD for Automatic Tunnel. */
        pAddrInfo->u1Status &= ~(UINT1) ADDR6_TENTATIVE;
        pAddrInfo->u1Status &= ~ADDR6_FAILED;
        pAddrInfo->u1Status |= ADDR6_COMPLETE (pIf6);
        pAddrInfo->u1Status |= ADDR6_PREFERRED;

        NetIpv6InvokeAddressChange (&pAddrInfo->ip6Addr,
                                    (UINT4) pAddrInfo->u1PrefLen,
                                    ADDR6_V4_COMPAT,
                                    pIf6->u4Index, (NETIPV6_ADDRESS_ADD));

        /* Create the Auto Compatible Route. */
        Ip6LocalRouteAdd (pIf6->u4Index, &pIf6->pTunlIf->tunlSrc,
                          AT_RTENTRY_PFXLEN);
        return IP6_SUCCESS;
    }

    return IP6_FAILURE;
}

/******************************************************************************
 * DESCRIPTION : This function is called to Delete the compatible address
 *               from automatic tunneling interface.
 *
 * INPUTS      : pIf6 - Automatic tunnel interface
 *
 * OUTPUTS     : None
 *
 * RETURNS     : IP6_SUCCESS/IP6_FAILURE
 *               
 *
 * NOTES       : 
 ******************************************************************************/
INT4
Ip6ifDeleteV4compatv6Addr (tIp6If * pIf6)
{
    /* Delete the Auto Compatible Route. */
    Ip6DelLocalRoute (&pIf6->pTunlIf->tunlSrc, AT_RTENTRY_PFXLEN,
                      pIf6->u4Index);

    /* Deleting the V4compatible v6 address */
    if (Ip6AddrDelete (pIf6->u4Index, &pIf6->pTunlIf->tunlSrc,
                       AT_RTENTRY_PFXLEN, 1) == IP6_SUCCESS)
    {
        return IP6_SUCCESS;
    }

    return IP6_FAILURE;

}

/******************************************************************************
 * DESCRIPTION : Function returns the pointer to the tunnel Interface structure
 *
 * INPUTS      : u4TunlIfIndex - tunnel interface index
 *
 * OUTPUTS     : None
 *
 * RETURNS     : Pointer to Tunnel Interface
 *               
 * NOTES       : 
 *****************************************************************************/
struct _IP6_TUNNEL_INTERFACE *
Ip6GetTunlIf (UINT4 u4TunlIfIndex)
{
    return (gIp6GblInfo.apIp6If[u4TunlIfIndex]->pTunlIf);
}

#endif /* TUNNEL_WANTED */
