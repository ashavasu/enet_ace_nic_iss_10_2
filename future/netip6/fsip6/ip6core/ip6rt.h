#ifndef _IP6RT_H_
#define _IP6RT_H_
/*
 *-----------------------------------------------------------------------------
 *    FILE NAME                    :    ip6rt.h
 *
 *    PRINCIPAL AUTHOR             :    Kaushik Biswas 
 *
 *    SUBSYSTEM NAME               :    IPv6
 *
 *    MODULE NAME                  :    IP6 Module
 *
 *    LANGUAGE                     :    C
 *
 *    TARGET ENVIRONMENT           :    UNIX
 *
 *    DATE OF FIRST RELEASE        :    DDD-MM-1996
 *
 *    DESCRIPTION                  :    This file contains constants, macros
 *                                      and typedefs which consits of the IP6
 *                                      Forwading Table.
 *
 *-----------------------------------------------------------------------------
 */

/* 
 * Typedefs 
 */

#define RIP6_PURGE_DYNAMIC                  1

#define RIP6_INFINITY                       16

/*
 * Some constants related to NetMgmt 
 */
#define DIRECT                              IP6_ROUTE_TYPE_DIRECT
#define INDIRECT                            IP6_ROUTE_TYPE_INDIRECT

#define IP6_RT_TYPE(pRt)       (pRt)->i1Type
#define IP6_RT_NHPTR(pRt)      &((pRt)->nexthop)

/*
 * Macros related to timer processing
 */
#define IP6_GET_RT_FROM_TIMER(x) \
      (tNd6RtEntry *)(VOID *)((UINT1 *)(x) - IP6_OFFSET(tNd6RtEntry,RtEntryTimer))

#define MIN(a,b)   (a < b) ? a : b

#endif
