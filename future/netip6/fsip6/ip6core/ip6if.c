/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ip6if.c,v 1.63 2017/12/19 13:41:54 siva Exp $
 *
 ********************************************************************/
/*
 *----------------------------------------------------------------------------- 
 *    FILE NAME                    :    ip6if.c
 *
 *    PRINCIPAL AUTHOR             :    Vivek Venkatraman
 *
 *    SUBSYSTEM NAME               :    IPv6
 *
 *    MODULE NAME                  :    IP6 Interface Submodule
 *
 *    LANGUAGE                     :    C
 *
 *    TARGET ENVIRONMENT           :    UNIX
 *
 *    DATE OF FIRST RELEASE        :    DDD-MM-1996
 *
 *    DESCRIPTION                  :    This file contains routines for
 *                                      handling IPv6 interfaces. It deals
 *                                      with IPv6 Inteface creation, deletion,
 *                                      enabling, disabling, reception and
 *                                      transmission issues over interfaces
 *                                      and support for tunneling.
 *
 *----------------------------------------------------------------------------- 
 */

#include "ip6inc.h"

/* 
 * Private functions 
 */

PRIVATE INT4 Ip6ifDeInitialize PROTO ((tIp6If * pIf6));

PRIVATE INT4 Ip6ifActOnIfUp PROTO ((tIp6If * pIf6));

PRIVATE INT4 Ip6ifChkToknLla PROTO ((tIp6If * pIf6));

/*
 * Initialization Routine
 */

/******************************************************************************
 * DESCRIPTION : This routine is called during initialization of the IPv6
 *               protocol stack. It allocates memory for the IPv6 logical
 *               interfaces and the IPv6 LAN and Tunnel physical interfaces.
 *
 * INPUTS      : None
 *
 * OUTPUTS     : None
 *
 * RETURNS     : OK if the memory allocation succeeds
 *               NOT_OK if the memory allocation fails
 *
 * NOTES       :
 ******************************************************************************/

INT4
Ip6IfInit ()
{
    if ((gIp6GblInfo.pIf6Htab =
         TMO_HASH_Create_Table (IP6IF_HASH_TBL_SIZE, NULL, FALSE)) == NULL)
    {
        return (IP6_FAILURE);
    }
#ifdef TUNNEL_WANTED
    /* Initializing SLL for list of source ipv4
     * addresses whose packets can be forwarded. */
    TMO_SLL_Init (&gIp6GblInfo.AccessLlist);

#endif /* TUNNEL_WANTED */
    return (IP6_SUCCESS);

}

/*
 * Routines for creation/deletion/enabling/disabling of IPv6 Interfaces
 */

/******************************************************************************
 * DESCRIPTION : This routine is called to create an IPv6 logical interface.
 *               The routine is called from SNMP upon SET on an IPv6 interface
 *               to create it. The SNMP routines would have already checked the 
 *               validity of the various parameters. 
 *               This routine will initialize the parameters of the IPv6
 *               interface by calling other procedures. Registered tasks will be
 *               notified of the interface creation. If the interface is Admin
 *               UP and if corresponding data-link layer is operational, a
 *               routine will be called to take suitable actions to start the
 *               DAD process [RFC 1971] if required.
 *
 * INPUTS      : The index of the interface (u2Index), type of the interface
 *               (u1Type), interface number and circuit number (u1If and
 *               u2Ckt), Admin Status (u1Admin), Interface token and token
 *               length if configured (pTok and u1Tokl)
 *
 * OUTPUTS     : None
 *
 * RETURNS     : OK in normal cases (as all checks are done earlier)
 *               NOT_OK upon some software error!
 *
 * NOTES       :
 ******************************************************************************/

INT1
Ip6CreateIf (UINT4 u4Index, UINT4 u4Speed, UINT4 u4HighSpeed, UINT1 u1Type,
             UINT2 u2Ckt, UINT1 u1Admin, UINT1 *pTok, UINT1 u1Tokl)
{
    tIp6If             *pIf6 = NULL;
    tIp6Cxt            *pIp6Cxt = NULL;
    UINT4               u4ContextId = 0;
    UINT4               u4Hkey = 0;

    if (VcmGetContextIdFromCfaIfIndex (u4Index, &u4ContextId) == VCM_FAILURE)
    {
        return IP6_FAILURE;
    }

    IP6_TRC_ARG5 (u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                  "IP6IF:Ip6CreateIf:IF %d Type %d Ckt %d Admin %d "
                  "Token %s\n", u4Index, u1Type, u2Ckt, u1Admin,
                  Ip6PrintIftok (pTok, u1Tokl));

    if ((gIp6GblInfo.apIp6If[u4Index] =
         (tIp6If *) (VOID *) Ip6GetMem (u4ContextId,
                                        gIp6GblInfo.i4Ip6IfPoolId)) == NULL)
    {
        IP6_TRC_ARG (u4ContextId, IP6_MOD_TRC, ALL_FAILURE_TRC, IP6_NAME,
                     "IP6IF:Ip6CreateIf: Malloc of tIp6If Failed!\n");
        IP6_TRC_ARG4 (u4ContextId, IP6_MOD_TRC, MGMT_TRC, IP6_NAME,
                      "IP6IF:Ip6CreateIf: %s mem err:, Type = %d  "
                      "PoolId = %d Memptr = %p ",
                      ERROR_FATAL_STR, ERR_MEM_CREATE, NULL, 0);
        return (IP6_FAILURE);
    }
    pIf6 = gIp6GblInfo.apIp6If[u4Index];

    if ((gIp6GblInfo.apIp6RaTbl[u4Index] =
         (tIp6RAEntry *) (VOID *) Ip6GetMem (u4ContextId,
                                             gIp6GblInfo.i4Ip6RaTblPoolId)) ==
        NULL)
    {
        IP6_TRC_ARG (u4ContextId, IP6_MOD_TRC, ALL_FAILURE_TRC, IP6_NAME,
                     "IP6IF:Ip6IfInit: Malloc of tIp6RAEntry Failed!\n");
        IP6_TRC_ARG4 (u4ContextId, IP6_MOD_TRC, MGMT_TRC, IP6_NAME,
                      "IP6IF:Ip6IfInit: %s mem err:, Type = %d  "
                      "PoolId = %d Memptr = %p ",
                      ERROR_FATAL_STR, ERR_MEM_CREATE, NULL, IP6_ZERO);
        Ip6RelMem (u4ContextId, (UINT2) gIp6GblInfo.i4Ip6IfPoolId,
                   (UINT1 *) pIf6);
        gIp6GblInfo.apIp6If[u4Index] = NULL;
        return (IP6_FAILURE);
    }

    MEMSET (gIp6GblInfo.apIp6RaTbl[u4Index], IP6_ZERO, sizeof (tIp6RAEntry));
    gIp6GblInfo.apIp6RaTbl[u4Index]->u1RowStatus = IP6_RA_INVALID;
    gIp6GblInfo.apIp6RaTbl[u4Index]->i4IfIndex = (INT4) u4Index;

    if ((pIp6Cxt = Ipv6UtilGetCxtEntryFromCxtId (u4ContextId)) == NULL)
    {
        Ip6RelMem (u4ContextId, (UINT2) gIp6GblInfo.i4Ip6IfPoolId,
                   (UINT1 *) pIf6);
        gIp6GblInfo.apIp6If[u4Index] = NULL;
        Ip6RelMem (u4ContextId, (UINT2) gIp6GblInfo.i4Ip6RaTblPoolId,
                   (UINT1 *) gIp6GblInfo.apIp6RaTbl[u4Index]);
        gIp6GblInfo.apIp6RaTbl[u4Index] = NULL;
        return IP6_FAILURE;
    }

    MEMSET (pIf6, 0, sizeof (tIp6If));

    pIf6->u4Index = u4Index;
    pIf6->pIp6Cxt = pIp6Cxt;
    pIf6->u1IfType = u1Type;
    pIf6->u2CktIndex = u2Ckt;
    u1Tokl = (UINT1) MEM_MAX_BYTES (u1Tokl, IP6_EUI_ADDRESS_LEN);
    pIf6->u1TokLen = u1Tokl;
    MEMCPY (pIf6->ifaceTok, pTok, u1Tokl);

    pIf6->Icmp6ErrRLInfo.i4Icmp6ErrRLFlag = ICMP6_RATE_LIMIT_ENABLE;
    pIf6->Icmp6ErrRLInfo.i4Icmp6ErrInterval = ICMP6_DEF_ERR_INETRVAL;
    pIf6->Icmp6ErrRLInfo.i4Icmp6BucketSize = ICMP6_DEF_BUCKET_SIZE;
    pIf6->Icmp6ErrRLInfo.i4Icmp6DstUnReachable = ICMP6_DEST_UNREACHABLE_ENABLE;
    pIf6->Icmp6ErrRLInfo.i4Icmp6RedirectMsg = ICMP6_REDIRECT_DISABLE;
    pIf6->u1RaCnf = IP6_IF_NO_RA_ADV;
    pIf6->u1RsFlag = IP6_IF_RS_SEND;
    pIf6->u1Hoplmt = IP6_IF_DEF_HOPLMT;
    pIf6->u2Deftime = IP6_IF_DEF_DEFTIME;
    pIf6->u4Reachtime = IP6_IF_DEF_REACHTIME;
    pIf6->u4Rettime = (IP6_IF_DEF_RETTIME * 1000);
    pIf6->u4Pdelaytime = IP6_IF_DEF_PDTIME;
    pIf6->u1PrefAdv = IP6_IF_PREFIX_ADV;
    pIf6->u4MaxRaTime = IP6_IF_DEF_MAX_RA_TIME;
    pIf6->u4MinRaTime = IP6_IF_DEF_MIN_RA_TIME;
    pIf6->u4Mtu = IP6_DEFAULT_MTU;
    pIf6->u4IfSpeed = u4Speed;
    pIf6->u4IfHighSpeed = u4HighSpeed;

    pIf6->u1IfStatusOnLink = IPV6_IF_ACTIVE;
    pIf6->u4OnLinkActiveIfId = u4Index;
    pIf6->u2OnlinkIfCount = 0;
    pIf6->u1NDProxyAdminStatus = ND6_IF_PROXY_ADMIN_DOWN;
    pIf6->u1NDProxyOperStatus = ND6_PROXY_OPER_DOWN;
    pIf6->u1NDProxyMode = ND6_PROXY_MODE_GLOBAL;
    pIf6->b1NDProxyUpStream = ND6_PROXY_IF_DOWNSTREAM;
    pIf6->u2SeNDDeltaTime = ND6_SECURE_DELTA_DEFAULT;
    pIf6->u2SeNDFuzzTime = ND6_SECURE_FUZZ_DEFAULT;
    pIf6->u1SeNDDriftTime = ND6_SECURE_DRIFT_DEFAULT;
    pIf6->u1SeNDStatus = ND6_SECURE_DISABLE;
    pIf6->u2Preference = IP6_RA_ROUTE_PREF_MED;

    MEMSET (pIf6->au1OnLinkIfaces, 0, IPV6_MAX_IF_OVER_LINK);

    /* Update the Lower Layer Status */
    pIf6->u1LLOperStatus = u1Admin;

    /* In IPv6 interface will be always created with Admin Status as DOWN. */
    pIf6->u1AdminStatus = ADMIN_DOWN;
    pIf6->u1OperStatus = OPER_DOWN;
    pIf6->u1AdminConfigFlag = FALSE;
    pIf6->u1Ipv6IfFwdOperStatus = IP6_IF_FORW_ENABLE;
    pIf6->u1Ipv6IfFwdStatusConfigured = IP6_IF_FORW_ENABLE;

    pIf6->u4IpPort = (UINT4) CfaGetIfIpPort ((UINT2) u4Index);
    pIf6->u1ZoneCreationStatus = IP6_ZONE_NOT_CREATED;
    pIf6->u1ZoneRowStatus = IP6_ZONE_ROW_STATUS_NOT_READY;
    TMO_SLL_Init (&pIf6->lla6Ilist);
    TMO_SLL_Init (&pIf6->addr6Ilist);
    TMO_SLL_Init (&pIf6->zoneIlist);
    TMO_SLL_Init (&pIf6->mcastIlist);
    TMO_SLL_Init (&pIf6->prefixlist);
    TMO_SLL_Init_Node (&pIf6->ifHash);
#ifdef HA_WANTED
    TMO_SLL_Init (&MNHAIlist[pIf6->u4Index]);
#endif
    if ((gIp6GblInfo.apIp6If[u4Index]->pIfIcmp6Stats =
         (tIcmp6Stats *) (VOID *) Ip6GetMem (pIf6->pIp6Cxt->u4ContextId,
                                             gIp6GblInfo.i4Icmp6IfPoolId)) ==
        NULL)
    {
        IP6_TRC_ARG (u4ContextId, IP6_MOD_TRC, ALL_FAILURE_TRC, IP6_NAME,
                     "IP6IF:Ip6CreateIf: Memory allocation for ICMP6Stats Failed.\n");
        Ip6RelMem (u4ContextId, (UINT2) gIp6GblInfo.i4Ip6IfPoolId,
                   (UINT1 *) pIf6);
        gIp6GblInfo.apIp6If[u4Index] = NULL;
        Ip6RelMem (u4ContextId, (UINT2) gIp6GblInfo.i4Ip6RaTblPoolId,
                   (UINT1 *) gIp6GblInfo.apIp6RaTbl[u4Index]);
        gIp6GblInfo.apIp6RaTbl[u4Index] = NULL;
        return (IP6_FAILURE);
    }
    MEMSET (gIp6GblInfo.apIp6If[u4Index]->pIfIcmp6Stats, 0,
            sizeof (tIcmp6Stats));

#ifdef TUNNEL_WANTED
    pIf6->pTunlIf = NULL;

    if (u1Type == IP6_TUNNEL_INTERFACE_TYPE)
    {
        /* New Tunnel interface created. Allocate Memory for New interface */
        if ((pIf6->pTunlIf =
             (tIp6TunlIf *) (VOID *) Ip6GetMem (pIf6->pIp6Cxt->u4ContextId,
                                                gIp6GblInfo.
                                                i4Ip6TunlIfPoolId)) == NULL)
        {
            /* Memory Allocation Fails */
            IP6_TRC_ARG1 (u4ContextId, IP6_MOD_TRC, OS_RESOURCE_TRC, IP6_NAME,
                          "IP6IF:Ip6CreateIf: Tunnel Interface [%d] "
                          "Memory Allocation FAILED\n", u4Index);
            if (gIp6GblInfo.apIp6If[u4Index]->pIfIcmp6Stats != NULL)
            {
                Ip6RelMem (pIf6->pIp6Cxt->u4ContextId,
                           (UINT2) gIp6GblInfo.i4Icmp6IfPoolId,
                           (UINT1 *) gIp6GblInfo.apIp6If[u4Index]->
                           pIfIcmp6Stats);
            }
            MEMSET (pIf6, 0, sizeof (tIp6If));
            Ip6RelMem (u4ContextId, (UINT2) gIp6GblInfo.i4Ip6IfPoolId,
                       (UINT1 *) pIf6);
            gIp6GblInfo.apIp6If[u4Index] = NULL;
            Ip6RelMem (u4ContextId, (UINT2) gIp6GblInfo.i4Ip6RaTblPoolId,
                       (UINT1 *) gIp6GblInfo.apIp6RaTbl[u4Index]);
            gIp6GblInfo.apIp6RaTbl[u4Index] = NULL;
            return IP6_FAILURE;
        }

    }
#endif /* TUNNEL_WANTED */

    /* Add the interface to the hash table */
    u4Hkey = Ip6ifHash (pIf6->u4Index, pIf6->u2CktIndex);
    TMO_HASH_Add_Node (gIp6GblInfo.pIf6Htab, &pIf6->ifHash, u4Hkey, NULL);

    OsixGetSysTime (&gIp6GblInfo.u4Ip6IfTableLastChange);
    OsixGetSysTime (&(pIf6->u4IfLastUpdate));

    if ((gIp6GblInfo.apIp6If[u4Index]->Ip6RARouteInfoTree =
         RBTreeCreateEmbedded
         (FSAP_OFFSETOF (tIp6RARouteInfoNode, RoutInfoRBNode),
          Ip6RARouteInfoCmp)) == NULL)
    {
        IP6_TRC_ARG (u4ContextId, IP6_MOD_TRC, ALL_FAILURE_TRC, IP6_NAME,
                     "IP6IF:Ip6CreateIf: Memory allocation for Ip6RARouteInfoTree Failed.\n");
        Ip6RelMem (u4ContextId, (UINT2) gIp6GblInfo.i4Ip6IfPoolId,
                   (UINT1 *) pIf6);
        gIp6GblInfo.apIp6If[u4Index] = NULL;
        Ip6RelMem (u4ContextId, (UINT2) gIp6GblInfo.i4Ip6RaTblPoolId,
                   (UINT1 *) gIp6GblInfo.apIp6RaTbl[u4Index]);
        gIp6GblInfo.apIp6RaTbl[u4Index] = NULL;
        return (IP6_FAILURE);
    }
    if (u1Type == IP6_LOOPBACK_INTERFACE_TYPE)
    {
        pIf6->u2DadSend = IP6_ZERO;
    }
    else
    {
        pIf6->u2DadSend = IP6_IF_DEF_DAD_SEND;
    }

    return (IP6_SUCCESS);
}

/******************************************************************************
 * DESCRIPTION : This routine is called to delete an IPv6 logical interface.
 *               The routine is called from SNMP upon SET on an IPv6 interface
 *               to delete it. The SNMP routines would have already checked
 *               that the interface is present.
 *               The routine will inform ND6 Module about the deletion of the
 *               interface, post an event to RIP6 to notify the interface
 *               deletion and take action for all addresses on the interface.
 *               The link-local address will be deleted, the interface removed
 *               from the hash table and marked as not in use.
 *
 * INPUTS      : The index of the interface (u4Index)
 *
 * OUTPUTS     : None
 *
 * RETURNS     : IP6_SUCCESS/IP6_FAILURE
 *
 * NOTES       :
 ******************************************************************************/

INT1
Ip6DeleteIf (UINT4 u4Index)
{
    tIp6If             *pIf6 = NULL;
    tTMO_SLL_NODE      *pNext = NULL;
    tTMO_SLL_NODE      *pCur = NULL;
    UINT4               u4ContextId = VCM_INVALID_VC;
    UINT4               u4Hkey = 0;

    Ip6GetCxtId (u4Index, &u4ContextId);
    IP6_TRC_ARG1 (u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                  "IP6IF: Ip6DeleteIf:Deleting IP6 IF = %d\n", u4Index);

    pIf6 = Ip6ifGetEntry (u4Index);
    if (pIf6 == NULL)
    {
        IP6_TRC_ARG1 (u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                      "IP6IF: Ip6DeleteIf:Invalid interface index = %d\n",
                      u4Index);
        return IP6_FAILURE;
    }

    /* Invoke UnmapIf as the unicast and anycast address are cleared and
     * also the higher layer protocols are notified */
    Ip6UnmapIf (pIf6);

    u4Hkey = Ip6ifHash (pIf6->u4Index, pIf6->u2CktIndex);
    TMO_HASH_Delete_Node (gIp6GblInfo.pIf6Htab, &pIf6->ifHash, u4Hkey);

    /* Notify ND that this interface is deleted */
    Nd6ActOnIfstatus (pIf6, IP6_IF_DELETE);

    /* Delete all the interface prefix. */
    pNext = TMO_SLL_First (&pIf6->prefixlist);
    while ((pCur = pNext) != NULL)
    {
        pNext = TMO_SLL_Next (&pIf6->prefixlist, pCur);

        TMO_SLL_Delete (&pIf6->prefixlist, pCur);

        Ip6RaPrefixNotify ((tIp6PrefixNode *) pCur, PREFIX_DELETE);
    }

#ifdef HA_WANTED
    pNext = TMO_SLL_First (&MNHAIlist[u4Index]);
    while ((pCur = pNext) != NULL)
    {
        pNext = TMO_SLL_Next (&MNHAIlist[u4Index], pCur);

        TMO_SLL_Delete (&MNHAIlist[u4Index], pCur);
    }
#endif

#ifdef MN_WANTED
    MNPrcsDeleteIf (u4Index);
#endif
    if (pIf6->pIfIcmp6Stats != NULL)
    {
        /* Ip6 interface Delete. De-Allocate Memory for icmp6 */
        Ip6RelMem (u4ContextId, (UINT2) gIp6GblInfo.i4Icmp6IfPoolId,
                   (UINT1 *) pIf6->pIfIcmp6Stats);
        pIf6->pIfIcmp6Stats = NULL;
    }
#ifdef TUNNEL_WANTED
    if (pIf6->u1IfType == IP6_TUNNEL_INTERFACE_TYPE)
    {
        /* Tunnel interface Delete. De-Allocate Memory for interface */
        Ip6RelMem (u4ContextId, (UINT2) gIp6GblInfo.i4Ip6TunlIfPoolId,
                   (UINT1 *) pIf6->pTunlIf);
        pIf6->pTunlIf = NULL;
    }
#endif /* TUNNEL_WANTED */

    if (pIf6->Icmp6ErrRLInfo.ErrIntervalTimer.appTimer.u2Flags)
    {
        Ip6TmrStop (pIf6->pIp6Cxt->u4ContextId, IP6_ICMP6_TIMER_ID,
                    gIp6GblInfo.Ip6TimerListId,
                    (tTmrAppTimer *) (&pIf6->Icmp6ErrRLInfo.ErrIntervalTimer.
                                      appTimer));
    }

    RBTreeDestroy (pIf6->Ip6RARouteInfoTree,
                   Ip6UtlRBFreeAgentToRARouteTable, 0);

    /* CAUTION : Interface information is getting deleted here. Ensure
     * that all the interface related data are cleared before this point.
     */
    MEMSET (pIf6, 0, sizeof (tIp6If));
    Ip6RelMem (u4ContextId, (UINT2) gIp6GblInfo.i4Ip6IfPoolId, (UINT1 *) pIf6);
    gIp6GblInfo.apIp6If[u4Index] = NULL;

    MEMSET (gIp6GblInfo.apIp6RaTbl[u4Index], 0, sizeof (tIp6RAEntry));
    Ip6RelMem (u4ContextId, (UINT2) gIp6GblInfo.i4Ip6RaTblPoolId,
               (UINT1 *) gIp6GblInfo.apIp6RaTbl[u4Index]);
    gIp6GblInfo.apIp6RaTbl[u4Index] = NULL;

    OsixGetSysTime (&gIp6GblInfo.u4Ip6IfTableLastChange);
    return (IP6_SUCCESS);
}

/******************************************************************************
 * DESCRIPTION : This routine is called when IPv6 is enabled on an Interface.
 *               Basically called when manager explicitly enable IPv6 on the
 *               interface or when an IPv6 address is assigned to an Interface
 *               whose status is DOWN.
 *               The routine will make the Admin as UP and check and form the
 *               link-local address. Then, if corresponding data-link layer is 
 *               operational, a routine will be called to take suitable actions 
 *               to start the DAD process if required.
 *
 * INPUTS      : The index of the interface (u4Index)
 *
 * OUTPUTS     : None
 *
 * RETURNS     : IP6_SUCCESS or
 *               IP6_FAILURE if link-local address check failed
 *
 * NOTES       :
 ******************************************************************************/

INT1
Ip6EnableIf (UINT4 u4Index)
{
    tIp6If             *pIf6 = NULL;
    UINT2               u2VlanId = 0;
    pIf6 = gIp6GblInfo.apIp6If[u4Index];

    if ((pIf6 == NULL) || (pIf6->pIp6Cxt == NULL))
    {
        IP6_TRC_ARG1 (VCM_INVALID_VC, IP6_MOD_TRC, DATA_PATH_TRC,
                      IP6_NAME, "Ip6EnableIf: Interface not created for %d\n",
                      u4Index);
        return IP6_FAILURE;
    }

    IP6_TRC_ARG1 (pIf6->pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                  DATA_PATH_TRC, IP6_NAME,
                  "IP6IF: Ip6EnableIf:Enabling IP6 IF = %d\n", u4Index);

    pIf6->u1AdminStatus = ADMIN_UP;
    pIf6->u1OperStatus = OPER_DOWN;

    pIf6->u4IpPort = (UINT4) CfaGetIfIpPort ((UINT2) u4Index);
#ifdef MN_WANTED
    MNInitDetInfoOnIf (pIf6->u4Index, 0xffffffff);
#endif

    /*
     * Initialize the interface by forming the link-local
     * address etc. Under normal circumstances, the initialization will succeed
     * as the validation is already done. The return code check is done as a
     * precaution.
     */
#if defined (NPAPI_WANTED) && defined (TUNNEL_WANTED)
    if (pIf6->u1IfType == IP6_TUNNEL_INTERFACE_TYPE)
    {
        Ipv6FsNpIpv6TunlParamSet (0, u4Index, pIf6->pTunlIf->u1TunlType,
                                  HTONL (IP6_TUNLIF_SRC (pIf6)),
                                  HTONL (IP6_TUNLIF_DST (pIf6)));
    }
#endif /* NPAPI_WANTED && TUNNEL_WANTED */

    if (Ip6ifInitialize (pIf6, pIf6->ifaceTok, pIf6->u1TokLen) != IP6_SUCCESS)
    {
        IP6_TRC_ARG6 (pIf6->pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                      ALL_FAILURE_TRC, IP6_NAME,
                      "IP6IF:Ip6EnableIf: IP6IF %d Type %d Ifnum %d Ckt %d "
                      "Admin %d Token %s - FAILED!!!\n",
                      u4Index, pIf6->u1IfType, pIf6->u4Index,
                      pIf6->u2CktIndex, pIf6->u1LLOperStatus,
                      Ip6ErrPrintIftok (ERROR_FATAL, pIf6->ifaceTok,
                                        pIf6->u1TokLen));
        pIf6->u1AdminStatus = ADMIN_DOWN;
        return (IP6_FAILURE);
    }
    if (pIf6->u1IfType == IP6_L3VLAN_INTERFACE_TYPE)
    {
        if (CfaGetVlanId (u4Index, &u2VlanId) == CFA_FAILURE)
        {
            IP6_TRC_ARG1 (pIf6->pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                          DATA_PATH_TRC, IP6_NAME,
                          "IP6IF: Ip6EnableIf:CfaGetVlanId failed for = %d\n",
                          u4Index);
        }
    }
#if defined (NPAPI_WANTED)

    Ipv6FsNpIpv6Enable (u4Index, u2VlanId);

#endif
    /*
     * If link-layer is operational take appropriate actions
     */
    if (Ip6ifGetIfOper (pIf6->u1IfType,
                        pIf6->u4Index, pIf6->u2CktIndex) == OPER_UP)
    {
        pIf6->u1OperStatus = OPER_UP;
        Ip6IfUp (pIf6);

        /* Indicate to the Higher layer that an interface is UP */
        NetIpv6InvokeInterfaceStatusChange (u4Index,
                                            NETIPV6_ALL_PROTO,
                                            NETIPV6_IF_UP,
                                            NETIPV6_INTERFACE_STATUS_CHANGE);
    }
    else
    {
        /* Indicate to the Higher layer that an interface is created
         * but the status is DOWN. */
        NetIpv6InvokeInterfaceStatusChange (u4Index,
                                            NETIPV6_ALL_PROTO,
                                            NETIPV6_IF_DOWN,
                                            NETIPV6_INTERFACE_STATUS_CHANGE);
    }
    OsixGetSysTime (&(pIf6->u4IfLastUpdate));
    OsixGetSysTime (&gIp6GblInfo.u4Ip6IfTableLastChange);

    return (IP6_SUCCESS);
}

/******************************************************************************
 * DESCRIPTION : This routine is called when IPv6 has been disabled on
 *               an existing IPv6 logical interface.
 *               The routine will make the Admin Status and Operationa Status
 *               as DOWN and then perform other actions such as inform ND6
 *               Module and higher layer about the disabling of the interface,
 *               stopping the timers for the addresses and so on.
 *
 * INPUTS      : The index of the interface (u4Index)
 *
 * OUTPUTS     : None
 *
 * RETURNS     : OK
 *
 * NOTES       : Admin Status is set to DOWN in the low level routine, before
                 this routine is called 
 ******************************************************************************/

INT1
Ip6DisableIf (UINT4 u4Index)
{
    tIp6If             *pIf6 = NULL;
    tTMO_SLL_NODE      *pNext = NULL;
    tIp6LlocalInfo     *pLladdr = NULL;
    UINT2               u2VlanId = 0;

    pIf6 = gIp6GblInfo.apIp6If[u4Index];

    IP6_TRC_ARG1 (pIf6->pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                  DATA_PATH_TRC, IP6_NAME,
                  "IP6IF: Ip6DisableIf:Disabling IP6 IF = %d\n", u4Index);

    if (pIf6->u1OperStatus != OPER_DOWN)
    {
        /* Operationally Disable the interface. */
        Ip6IfDown (pIf6);
    }

    if (pIf6->u1IfType == IP6_L3VLAN_INTERFACE_TYPE)
    {
        if (CfaGetVlanId (u4Index, &u2VlanId) == CFA_FAILURE)
        {
            IP6_TRC_ARG1 (pIf6->pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                          DATA_PATH_TRC, IP6_NAME,
                          "IP6IF: Ip6DisableIf:CfaGetVlanId failed for IF = %d\n",
                          u4Index);
        }
    }

#if defined (NPAPI_WANTED)

    Ipv6FsNpIpv6Disable (u4Index, u2VlanId);

#endif
    /* Clear the interface Statistics */
    MEMSET (&pIf6->stats, 0, sizeof (tIp6IfStats));
    pNext = TMO_SLL_First (&(pIf6->lla6Ilist));
    if (pNext != NULL)
    {
        pLladdr = IP6_LLADDR_PTR_FROM_SLL (pNext);
        if (pLladdr->u1ConfigMethod != IP6_ADDR_STATIC)
        {

            Ip6ifDeInitialize (pIf6);
        }
    }
    pIf6->u1OperStatus = OPER_DOWN;
    pIf6->u1AdminStatus = ADMIN_DOWN;
    OsixGetSysTime (&(pIf6->u4IfLastUpdate));
    OsixGetSysTime (&gIp6GblInfo.u4Ip6IfTableLastChange);

    if (pIf6->Icmp6ErrRLInfo.ErrIntervalTimer.appTimer.u2Flags)
    {
        Ip6TmrStop (pIf6->pIp6Cxt->u4ContextId, IP6_ICMP6_TIMER_ID,
                    gIp6GblInfo.Ip6TimerListId,
                    (tTmrAppTimer *) (&pIf6->Icmp6ErrRLInfo.ErrIntervalTimer.
                                      appTimer));
    }

    /* Indicate the Higher layer that the interface is deleted to the
     * higher layer. */
    NetIpv6InvokeInterfaceStatusChange (u4Index,
                                        NETIPV6_ALL_PROTO,
                                        NETIPV6_IF_DOWN,
                                        NETIPV6_INTERFACE_STATUS_CHANGE);
    return (IP6_SUCCESS);
}

/******************************************************************************
 * DESCRIPTION : This routine is called when an interface on which IPv6 is
 *               enabled becomes operationally up.
 *
 * INPUTS      : The interface pointer (pIf6)
 *
 * OUTPUTS     : None
 *
 * RETURNS     : IP6_SUCCESS/IP6_FAILURE
 *
 * NOTES       :
 ******************************************************************************/

INT4
Ip6IfUp (tIp6If * pIf6)
{
    tIp6Addr            tmpAddr;

    IP6_TRC_ARG1 (pIf6->pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                  DATA_PATH_TRC, IP6_NAME,
                  "IP6IF: Ip6IfUp:Interface UP Indication for IP6 IF = %d\n",
                  pIf6->u4Index);

    pIf6->u1OperStatus = OPER_UP;

    /* Ensure that ALL NODE Multi-Cast address is enabled for this
     * interface. */
    SET_ALL_NODES_MULTI (tmpAddr);
    Ip6AddrCreateMcast (pIf6->u4Index, &tmpAddr);

    if (pIf6->u1Ipv6IfFwdOperStatus == IP6_IF_FORW_ENABLE)
    {
        /* Routing is enabled. Ensure that ALL ROUTER Multi-Cast address
         * is enabled for this interface. */
        SET_ALL_ROUTERS_MULTI (tmpAddr);
        Ip6AddrCreateMcast (pIf6->u4Index, &tmpAddr);
    }

    Ip6ifActOnIfUp (pIf6);

    /* Ensure that all static routes learnt over this interface is
     * activated. */
    IP6_TASK_UNLOCK ();
    Rtm6ApiActOnIpv6IfChange (RTM6_SCAN_INTF_UP, pIf6->u4Index);
    IP6_TASK_LOCK ();
    if (ND6_IF_PROXY_ADMIN_UP == pIf6->u1NDProxyAdminStatus)
    {
        Nd6ProxyUtlSetOperStatus (pIf6, ND6_PROXY_OPER_UP);
    }

    IP6_TRC_ARG1 (pIf6->pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                  DATA_PATH_TRC, IP6_NAME,
                  "IP6IF: Ip6IfUp:Interface %d is made UP\n", pIf6->u4Index);

    return (IP6_SUCCESS);

}

/******************************************************************************
 * DESCRIPTION : This routine is called when an interface on which IPv6 is
 *               enabled becomes operationally down. The indication of the
 *               interface going down is received through an event from
 *               the TEST task.
 *               A routine will be called to perform actions such as inform 
 *               ND6 Module and RIP6 about the disabling of the interface, 
 *               stopping the timers for the addresses and so on.
 *
 * INPUTS      : The interface pointer (pIf6)
 *
 * OUTPUTS     : None
 *
 * RETURNS     : OK
 *
 * NOTES       :
 ******************************************************************************/

INT4
Ip6IfDown (tIp6If * pIf6)
{
    tTMO_SLL_NODE      *pSnode = NULL;
    tIp6LlocalInfo     *pAddr = NULL;
    tIp6Addr            tmpAddr;

    IP6_TRC_ARG1 (pIf6->pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                  DATA_PATH_TRC, IP6_NAME,
                  "IP6IF:Ip6IfDown:Interface DOWN Indication for IP6 IF = %d\n",
                  pIf6->u4Index);

    if (pIf6->u1AdminStatus == ADMIN_DOWN)
    {
        /* Admin is already Down. So interface would have not been enabled
         * earlier. So no need for any processing. */
        pIf6->u1OperStatus = OPER_DOWN;
        return (IP6_SUCCESS);
    }
    pIf6->u4CurOperStatus = OPER_DOWN_INPROGRESS;

    /* Ensure that ALL NODE Multi-Cast address is disabled for this
     * interface. */
    SET_ALL_NODES_MULTI (tmpAddr);
    Ip6AddrDeleteMcast (pIf6->u4Index, &tmpAddr);

    if (pIf6->u1Ipv6IfFwdOperStatus == IP6_IF_FORW_ENABLE)
    {
        /* Routing is enabled. Ensure that ALL ROUTER Multi-Cast address
         * is disabled for this interface. */
        SET_ALL_ROUTERS_MULTI (tmpAddr);
        Ip6AddrDeleteMcast (pIf6->u4Index, &tmpAddr);
    }

    if (pIf6->u1OperStatus == OPER_UP)
    {
        ND6DeleteRoutes (pIf6->u4Index);
        IP6_TASK_UNLOCK ();
        Rtm6ApiActOnIpv6IfChange (RTM6_SCAN_INTF_DOWN, pIf6->u4Index);
        IP6_TASK_LOCK ();
        /* After handling routes hosts are deleted */
        Ip6ifActOnIfDown (pIf6);
        /* If any Link-Local Address is present, then set the
         * address status as DOWN. */
        TMO_SLL_Scan (&pIf6->lla6Ilist, pSnode, tTMO_SLL_NODE *)
        {
            pAddr = IP6_LLADDR_PTR_FROM_SLL (pSnode);
            pAddr->u1Status = ADDR6_DOWN;
        }

#ifdef MN_WANTED
        MNPrcsDeleteIf (pIf6->u4Index);
#endif
    }
    pIf6->u1OperStatus = OPER_DOWN;
    pIf6->u4CurOperStatus = OPER_DOWN;
    Nd6ProxyUtlSetOperStatus (pIf6, ND6_PROXY_OPER_DOWN);
    /* Stop ND6 Proxy loop timer if running */
    Ip6TmrStop (pIf6->pIp6Cxt->u4ContextId,
                ND6_PROXY_LOOP_TIMER_ID, gIp6GblInfo.Ip6TimerListId,
                &(pIf6->proxyLoopTimer.appTimer));

    return (IP6_SUCCESS);
}

/******************************************************************************
 * DESCRIPTION : This routine is called when the DAD for link-local address
 *               fails.
 *
 * INPUTS      : The interface pointer (pIf6)
 *
 * OUTPUTS     : None
 *
 * RETURNS     : IP6_SUCCESS/IP6_FAILURE
 *
 * NOTES       :
 ******************************************************************************/

INT4
Ip6IfStall (tIp6If * pIf6)
{

    IP6_TRC_ARG1 (pIf6->pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                  DATA_PATH_TRC, IP6_NAME,
                  "IP6IF:Ip6IfStall:Interface Stall Indication for IP6 IF = %d\n",
                  pIf6->u4Index);

    if (pIf6->u1AdminStatus == ADMIN_DOWN)
    {
        /* Interface is administratiely down. So oper status has to be down. */
        pIf6->u1OperStatus = OPER_DOWN;
        return (IP6_SUCCESS);
    }

    Ip6ifActOnIfDown (pIf6);

    pIf6->u1OperStatus = OPER_NOIFIDENTIFIER;
    ND6DeleteRoutes (pIf6->u4Index);
    IP6_TASK_UNLOCK ();
    Rtm6ApiActOnIpv6IfChange (RTM6_SCAN_INTF_DOWN, pIf6->u4Index);
    IP6_TASK_LOCK ();

#ifdef MN_WANTED
    MNPrcsDeleteIf (pIf6->u4Index);
#endif

    return (IP6_SUCCESS);
}

/******************************************************************************
 * DESCRIPTION : This routine initializes the IPv6 interface upon enabling.
 *               It forms the link-local address, adds to Hash table etc. based
 *               on the type of the interface.
 *
 * INPUTS      : The interface pointer (pIf6), the interface token (pTok)
 *               and the token length (u1Tokl). Note that the token can be
 *               NULL.
 *
 * OUTPUTS     : None
 *
 * RETURNS     : OK upon success
 *               NOT_OK upon failure
 *
 * NOTES       :
 ******************************************************************************/

INT4
Ip6ifInitialize (tIp6If * pIf6, UINT1 *pTok, UINT1 u1Tokl)
{

    UINT4               u4If;
#ifdef TUNNEL_WANTED
    UINT4               u4IsaTapId = 0;
#endif /* TUNNEL_WANTED */

    u4If = pIf6->u4Index;
    switch (pIf6->u1IfType)
    {

#ifdef TUNNEL_WANTED
        case IP6_TUNNEL_INTERFACE_TYPE:
            MEMSET (pIf6->ifaceTok, 0, IP6_EUI_ADDRESS_LEN);
            if (pIf6->pTunlIf->u1TunlType == IPV6_ISATAP_TUNNEL)
            {
                u4IsaTapId =
                    (IS_IP_ADDR_PRIVATE ((OSIX_HTONL
                                          (pIf6->pTunlIf->tunlSrc.
                                           u4_addr[IP6_THREE]))) ==
                     OSIX_TRUE) ? IP6_ISATAP_PRIVATE_ID : IP6_ISATAP_GLOBAL_ID;
                u4IsaTapId = OSIX_HTONL (u4IsaTapId);
                MEMCPY (&(pIf6->ifaceTok[0]), &u4IsaTapId, IP6_FOUR);
                MEMCPY (&(pIf6->ifaceTok[IP6_FOUR]),
                        &pIf6->pTunlIf->tunlSrc.u1_addr[12], IP6_FOUR);
            }
            else
            {
                /* Both Automatic and Configured Tunnels are IPv6 Interface and
                 * should have Link-Local Address for running routing protocols
                 * over it. The 32-bit IPv4 Address of the interface over which
                 * the Tunnel is created is used as the Interface Identifier to
                 * create the Link-Local Address. */
                MEMCPY (&(pIf6->ifaceTok[IP6_FOUR]),
                        &pIf6->pTunlIf->tunlSrc.u1_addr[12], IP6_FOUR);
            }
            pIf6->u1TokLen = IP6_EUI_ADDRESS_LEN;

            if (Ip6ifCreateLla (pIf6, pIf6->ifaceTok, pIf6->u1TokLen) ==
                IP6_FAILURE)
            {
                IP6_TRC_ARG2 (pIf6->pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                              ALL_FAILURE_TRC, IP6_NAME,
                              "IP6IF:Ip6ifInitialize:Create LLA for Tunnel "
                              "Interface fails - IP6IF Index = %d "
                              "Token = %s\n", pIf6->u4Index,
                              Ip6ErrPrintIftok (ERROR_FATAL, pTok, u1Tokl));
                return (IP6_FAILURE);
            }

            break;
#endif /* TUNNEL_WANTED */

        case IP6_ENET_INTERFACE_TYPE:
        case IP6_L3VLAN_INTERFACE_TYPE:
        case IP6_LAGG_INTERFACE_TYPE:
        case IP6_PSEUDO_WIRE_INTERFACE_TYPE:
        case IP6_LOOPBACK_INTERFACE_TYPE:
        case IP6_L3SUB_INTF_TYPE:
#ifdef WGS_WANTED
        case IP6_L2VLAN_INTERFACE_TYPE:
#endif
            if (Ip6ifFormLanLla (pIf6) != IP6_SUCCESS)
            {
                IP6_TRC_ARG3 (pIf6->pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                              ALL_FAILURE_TRC, IP6_NAME,
                              "IP6IF: Ip6ifInitialize: LLA failed IP6 IF = %d "
                              "Type = 0x%X Ifnum = %d\n",
                              pIf6->u4Index, pIf6->u1IfType, u4If);
                return (IP6_FAILURE);
            }
            break;

        case IP6_X25_INTERFACE_TYPE:
        case IP6_FR_INTERFACE_TYPE:
            if (u1Tokl != 0)
            {
                u1Tokl = (UINT1) MEM_MAX_BYTES (u1Tokl, IP6_EUI_ADDRESS_LEN);
                pIf6->u1TokLen = u1Tokl;
                MEMCPY (pIf6->ifaceTok, pTok, u1Tokl);
                if (Ip6ifCreateLla (pIf6, pIf6->ifaceTok,
                                    pIf6->u1TokLen) != IP6_SUCCESS)
                {
                    IP6_TRC_ARG3 (pIf6->pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                                  ALL_FAILURE_TRC, IP6_NAME,
                                  "IP6IF: Ip6ifInitialize:LLA failed "
                                  "IP6IF = %d Type = 0x%X IP6IF = %d Token= %s\n",
                                  pIf6->u4Index, pIf6->u1IfType,
                                  Ip6ErrPrintIftok (ERROR_FATAL, pTok, u1Tokl));
                    return (IP6_FAILURE);
                }
            }
            break;

        default:
            IP6_TRC_ARG1 (pIf6->pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                          DATA_PATH_TRC, IP6_NAME,
                          "IP6IF: Ip6ifInitialize Unknown Type %d\n",
                          pIf6->u1IfType);
            IP6_TRC_ARG2 (pIf6->pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                          MGMT_TRC, IP6_NAME,
                          "IP6IF: Ip6ifInitialize :%s gen err:, Type = %d  ",
                          ERROR_FATAL_STR, ERR_GEN_INVALID_VAL);
            return (IP6_FAILURE);
    }

    return (IP6_SUCCESS);

}

/******************************************************************************
 * DESCRIPTION : This routine de-initialize the IPv6 interface upon disabling.
 *               It removes the link-local address, and deletes it from Hash
 *               table etc. based on the type of the interface.
 *
 * INPUTS      : The interface pointer (pIf6)
 *
 * OUTPUTS     : None
 *
 * RETURNS     : IP6_SUCCESS upon success
 *               IP6_FAILURE upon failure
 *
 * NOTES       :
 ******************************************************************************/

PRIVATE INT4
Ip6ifDeInitialize (tIp6If * pIf6)
{

    switch (pIf6->u1IfType)
    {

        case IP6_TUNNEL_INTERFACE_TYPE:
            Ip6ifLlaStatus (pIf6, IP6_IF_DELETE);
            break;

        case IP6_ENET_INTERFACE_TYPE:
        case IP6_L3VLAN_INTERFACE_TYPE:
        case IP6_LAGG_INTERFACE_TYPE:
        case IP6_PSEUDO_WIRE_INTERFACE_TYPE:
        case IP6_L3SUB_INTF_TYPE:
#ifdef WGS_WANTED
        case IP6_L2VLAN_INTERFACE_TYPE:
#endif
            Ip6ifLlaStatus (pIf6, IP6_IF_DELETE);
            break;

        case IP6_X25_INTERFACE_TYPE:
        case IP6_FR_INTERFACE_TYPE:
            if (pIf6->u1TokLen != 0)
            {
                Ip6ifLlaStatus (pIf6, IP6_IF_DELETE);
            }
            break;

        default:
            IP6_TRC_ARG1 (pIf6->pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                          DATA_PATH_TRC, IP6_NAME,
                          "IP6IF: Ip6ifDeInitialize Unknown Type %d\n",
                          pIf6->u1IfType);
            IP6_TRC_ARG2 (pIf6->pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                          MGMT_TRC, IP6_NAME,
                          "IP6IF: Ip6ifDeInitialize :%s gen err:, Type = %d\n",
                          ERROR_FATAL_STR, ERR_GEN_INVALID_VAL);
            return (IP6_FAILURE);
    }

    return (IP6_SUCCESS);

}

/******************************************************************************
 * DESCRIPTION : This routine is called when an IPv6 interface is about to
 *               become operational (as a result of Admin UP or when lower
 *               layer comes UP). It ensures that the link-local address is
 *               either already present or obtained on the interface.
 *
 * INPUTS      : The interface pointer (pIf6)
 *
 * OUTPUTS     : None
 *
 * RETURNS     : OK upon success
 *               NOT_OK upon failure
 *
 * NOTES       :
 ******************************************************************************/

PRIVATE INT4
Ip6ifChkToknLla (tIp6If * pIf6)
{
    INT4                i4Status = IP6_SUCCESS;

    switch (pIf6->u1IfType)
    {
        case IP6_ENET_INTERFACE_TYPE:
        case IP6_L3VLAN_INTERFACE_TYPE:
        case IP6_LAGG_INTERFACE_TYPE:
        case IP6_LOOPBACK_INTERFACE_TYPE:
        case IP6_PSEUDO_WIRE_INTERFACE_TYPE:
        case IP6_L3SUB_INTF_TYPE:
#ifdef WGS_WANTED
        case IP6_L2VLAN_INTERFACE_TYPE:
#endif
            if (IP6IF_TOKENLESS (pIf6))
            {
                i4Status = IP6_FAILURE;
            }
            break;

#ifdef TUNNEL_WANTED
        case IP6_TUNNEL_INTERFACE_TYPE:
            i4Status = IP6_SUCCESS;
            break;
#endif

        case IP6_PPP_INTERFACE_TYPE:
            i4Status = Ip6ifFormPppLla (pIf6);
            break;

        case IP6_X25_INTERFACE_TYPE:
        case IP6_FR_INTERFACE_TYPE:
            if (IP6IF_TOKENLESS (pIf6))
            {
                i4Status = Ip6ifFormWanLla (pIf6);
            }
            break;

        default:
            IP6_TRC_ARG1 (pIf6->pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                          DATA_PATH_TRC, IP6_NAME,
                          "IP6IF: Ip6ifChkToknLla:Unknown Type %d\n",
                          pIf6->u1IfType);
            IP6_TRC_ARG2 (pIf6->pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                          MGMT_TRC, IP6_NAME,
                          "IP6IF: Ip6ifChkToknLla: %s gen err:, Type = %d ",
                          ERROR_FATAL_STR, ERR_GEN_INVALID_VAL);
            return (IP6_FAILURE);
    }

    return (i4Status);

}

/******************************************************************************
 * DESCRIPTION : This routine is called when an IPv6 interface is about to
 *               become operational (as a result of Admin UP or when lower
 *               layer comes UP). It checks that the link-local address is
 *               present and then takes the UP action for all addresses on
 *               the interface as well as sends a UP notification to RIP6.
 *
 * INPUTS      : The interface pointer (pIf6)
 *
 * OUTPUTS     : None
 *
 * RETURNS     : None
 *
 * NOTES       : IP6_SUCCESS/IP6_FAILURE
 ******************************************************************************/

PRIVATE INT4
Ip6ifActOnIfUp (tIp6If * pIf6)
{
    tTMO_SLL_NODE      *pSnode = NULL;
    tIp6AddrInfo       *pAddr = NULL;

    /* 
     * obtain the interface token and form the link-local address for
     * PPP interface and also for FR/X25 interface if not configured
     */

    if (Ip6ifChkToknLla (pIf6) != IP6_SUCCESS)
    {
        IP6_TRC_ARG2 (pIf6->pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                      ALL_FAILURE_TRC, IP6_NAME,
                      "IP6IF: Ip6ifChkToknLla:IP6 IF = %d Type = %d UP but "
                      "Token Check failed\n", pIf6->u4Index, pIf6->u1IfType);
        IP6_TRC_ARG2 (pIf6->pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                      MGMT_TRC, IP6_NAME,
                      "IP6IF: Ip6ifChkToknLla: %s gen err:, Type = %d  ",
                      ERROR_FATAL_STR, ERR_GEN_INVALID_VAL);
        return IP6_FAILURE;
    }

    /*
     * notify all addresses on the interface that interface is up
     */

    Ip6ifLlaStatus (pIf6, IP6_IF_UP);

    TMO_SLL_Scan (&pIf6->addr6Ilist, pSnode, tTMO_SLL_NODE *)
    {
        pAddr = IP6_ADDR_PTR_FROM_SLL (pSnode);
        Ip6AddrNotify (pAddr, IP6_IF_UP);
    }

#ifdef TUNNEL_WANTED
    /* If the interface is Automatic Tunnel Interface, then add the 
     * autocompatible address over this interface */
    if ((pIf6->u1IfType == IP6_TUNNEL_INTERFACE_TYPE) &&
        (pIf6->pTunlIf->u1TunlType == IPV6_AUTO_COMPAT))
    {
        Ip6ifCreateV4compatv6Addr (pIf6);
    }
#endif

    return IP6_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : This routine is called when an IPv6 interface goes down
 *               (as a result of Admin DOWN or when lower layer going DOWN).
 *               The DOWN notification is given to ND6 and RIP6 and then
 *               DOWN action initiated for all addresses on the interface.
 *
 * INPUTS      : The interface pointer (pIf6)
 *
 * OUTPUTS     : None
 *
 * RETURNS     : None
 *
 * NOTES       :
 ******************************************************************************/

VOID
Ip6ifActOnIfDown (tIp6If * pIf6)
{

    tIp6AddrInfo       *pAddr = NULL;
    tTMO_SLL_NODE      *pNext = NULL, *pCur = NULL;

#ifdef TUNNEL_WANTED
    /* If the interface is Automatic Tunnel Interface, then add the 
     * autocompatible address over this interface */
    if ((pIf6->u1IfType == IP6_TUNNEL_INTERFACE_TYPE) &&
        (pIf6->pTunlIf->u1TunlType == IPV6_AUTO_COMPAT))
    {
        Ip6ifDeleteV4compatv6Addr (pIf6);
    }
#endif

    /*
     * In the case of interface down unicast addresses assigned on 
     * the interface should not be deleted and but however corresponding
     * local routes need to deleted.
     */

    pNext = TMO_SLL_First (&pIf6->addr6Ilist);
    while ((pCur = pNext) != NULL)
    {
        pNext = TMO_SLL_Next (&pIf6->addr6Ilist, pCur);

        pAddr = IP6_ADDR_PTR_FROM_SLL (pCur);

        /* 
         * When the address is already down, the routes would have been
         * deleted. So deleting the routes only when address is in 
         * ADDR6_UP state. 
         */
        if ((pAddr->u1Status & ADDR6_UP) &&
            (pAddr->u1ConfigMethod != IP6_ADDR_AUTO_SL))
        {
            Ip6DelLocalRoute (&pAddr->ip6Addr, pAddr->u1PrefLen, pIf6->u4Index);
        }

        if (pAddr->u1ConfigMethod == IP6_ADDR_STATIC)
        {
            /* Inform all the higher layer about the change in Address Status */
            NetIpv6InvokeAddressChange (&(pAddr->ip6Addr),
                                        (pAddr->u1PrefLen),
                                        (UINT4) pAddr->u1AddrType,
                                        (pIf6->u4Index),
                                        (NETIPV6_ADDRESS_DELETE));
            Ip6AddrNotify (pAddr, IP6_IF_DOWN);
        }
        else
        {
            /* Dynamically learnt Address. This address needs to be removed
             * and address will be re-allocated when interface is brought
             * back to service. */
            Ip6AddrDelete (pIf6->u4Index, &pAddr->ip6Addr, pAddr->u1PrefLen, 0);
        }
    }

    if (Ip6GetLlocalAddr (pIf6->u4Index) != NULL)
    {
        /* Notify ND that this interface is down */
        Nd6ActOnIfstatus (pIf6, IP6_IF_DOWN);
    }

    Ip6ifLlaStatus (pIf6, IP6_IF_DOWN);

}

/*
 * Routines for locating the IPv6 Interface
 */

/******************************************************************************
 * DESCRIPTION : This routine is called to locate the IPv6 logical interface
 *               upon receiving a IPv6 datagram. From the buffer, the interface
 *               type, number and circuit are obtained. Based on the type,
 *               the IPv6 interface is located and returned. For non-tunnel
 *               interfaces, the hash table is looked up to get the IPv6
 *               interface.
 *
 * INPUTS      : The buffer containing the received datagram (pBuf). A field 
 *               of the buffer header specifies the interface parameters.
 *
 * OUTPUTS     : None
 *
 * RETURNS     : The IPv6 interface pointer, if found
 *               NULL, if interface not found
 *
 * NOTES       :
 ******************************************************************************/

tIp6If             *
Ip6LocateIf (tCRU_BUF_CHAIN_HEADER * pBuf)
{
    tIp6If             *pIf6 = NULL;
    tLlToIp6Params     *pIp6Params = NULL;
    tIp6Addr           *pSrcAddr = NULL;
    tIp6Addr            SrcAddr;
    UINT4               u4Offset = 0;
#ifdef TUNNEL_WANTED
    UINT4               u4LocalAddr = 0;
    UINT4               u4RemoteAddr = 0;
#endif
    UINT4               u4If = 0;
    UINT4               u4ContextId = 0;
    UINT2               u2Ckt = 0;
    UINT1               u1Type = 0;

    pIp6Params = (tLlToIp6Params *) IP6_GET_MODULE_DATA_PTR (pBuf);
    u1Type = pIp6Params->u1LinkType;
    u4If = pIp6Params->u4Port;

    switch (u1Type)
    {
        case IP6_ENET_INTERFACE_TYPE:
        case IP6_L3VLAN_INTERFACE_TYPE:
        case IP6_LAGG_INTERFACE_TYPE:
        case IP6_PSEUDO_WIRE_INTERFACE_TYPE:
        case IP6_X25_INTERFACE_TYPE:
        case IP6_FR_INTERFACE_TYPE:
        case IP6_PPP_INTERFACE_TYPE:
        case IP6_L3SUB_INTF_TYPE:
#ifdef WGS_WANTED
        case IP6_L2VLAN_INTERFACE_TYPE:
#endif
            u4Offset = IP6_BUF_READ_OFFSET (pBuf);
            pSrcAddr =
                Ip6BufRead (pBuf, (UINT1 *) &SrcAddr,
                            IP6_OFFSET_FOR_SRCADDR_FIELD, sizeof (tIp6Addr), 1);
            IP6_BUF_READ_OFFSET (pBuf) = u4Offset;
            if (pSrcAddr == NULL)
            {
                return NULL;
            }

            if (IS_ADDR_LOOPBACK (*pSrcAddr))
            {
                /* Invalid Source Address */
                return NULL;
            }
            pIf6 = Ip6GetIf (u4If, u2Ckt);
            Ip6GetCxtId (u4If, &u4ContextId);
            break;

#ifdef TUNNEL_WANTED
        case IP6_TUNNEL_INTERFACE_TYPE:
            /* Get the Tunnel Source And Destination Address. */
            u4LocalAddr = CRU_BUF_Get_U4Reserved2 (pBuf);
            u4RemoteAddr = CRU_BUF_Get_U4Reserved3 (pBuf);

            /*  find the ip6 if using the destination
             *  ipv4 addr (tunnel's dest addr)
             */

            /* u4If will always be non-zero. Hence, Ip6Cxt info
             * will never be used in Ip6GetIfTunlInCxt */
            pIf6 = (tIp6If *) Ip6GetIfTunlInCxt (NULL, u4LocalAddr,
                                                 u4RemoteAddr, u4If);
            Ip6GetCxtId (u4If, &u4ContextId);
            break;
#endif /* TUNNEL_WANTED */

        default:
            IP6_GBL_TRC_ARG1 (IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                              "IP6IF: Ip6LocateIf: Unknown Type %d\n", u1Type);
            IP6_GBL_TRC_ARG2 (IP6_MOD_TRC, MGMT_TRC, IP6_NAME,
                              "IP6IF: Ip6LocateIf: %s gen err:, Type = %d ",
                              ERROR_FATAL_STR, ERR_GEN_INVALID_VAL);
            u4ContextId = VCM_INVALID_VC;
            break;
    }

    IP6_TRC_ARG4 (u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                  "IP6IF:Ip6LocateIf IFPtr = %p for Type = %d IfNum = %d Ckt = %d\n",
                  pIf6, u1Type, u4If, u2Ckt);

    return (pIf6);

}

/******************************************************************************
 * DESCRIPTION : This routine is called to hash into the IPv6 Interface Hash
 *               table and locate the IPv6 logical interface. Hashing is
 *               done based on the port number and circuit number.
 *
 * INPUTS      : The interface number (u1If) and circuit number (u2Ckt)
 *
 * OUTPUTS     : None
 *
 * RETURNS     : Pointer to IPv6 Logical Interface, if found
 *               NULL, if not found
 *
 * NOTES       :
 ******************************************************************************/

tIp6If             *
Ip6GetIf (UINT4 u4If, UINT2 u2Ckt)
{

    tTMO_HASH_NODE     *pHnode = NULL;
    tIp6If             *pIf6 = NULL;
    UINT4               u4Hkey = 0;

    u4Hkey = Ip6ifHash (u4If, u2Ckt);

    /* hash using the key and scan that bucket for a matching IP6 interface */

    TMO_HASH_Scan_Bucket (gIp6GblInfo.pIf6Htab, u4Hkey,
                          pHnode, tTMO_HASH_NODE *)
    {
        pIf6 = IP6_IF_PTR_FROM_HASH (pHnode);

        if ((pIf6->u4Index == u4If) && (pIf6->u2CktIndex == u2Ckt))
        {
            return (pIf6);
        }
    }

    return (NULL);

}

/*
 * Routine for sending packets on various interfaces
 */

/******************************************************************************
 * DESCRIPTION : This routine is called to send out a IPv6 datagram on a
 *               IPv6 interface. It is called in the Send path [Ip6Send()]
 *               as well as the Forward path [Ip6Forward()]. Based on the
 *               type of interface, individual routines are called to take
 *               further action.
 *
 * INPUTS      : The interface pointer (pIf6), destination IPv6 address
 *               (pDstAddr6), ND Cache pointer (pNd6CacheEntry) which is
 *               used only for LAN interfaces, length of the datagram (u4Len),
 *               the buffer (pBuf) and the destination address route entry     
 *               if anycast
 *
 * OUTPUTS     : None
 *
 * RETURNS     : None
 *
 * NOTES       :
 ******************************************************************************/

VOID
Ip6ifSend (tIp6If * pIf6, tIp6Addr * pDstAddr6, tNd6CacheEntry * pNd6CacheEntry,
           UINT4 u4Len, tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 u1RouteAddrType)
{
    UINT4               u4ContextId = pIf6->pIp6Cxt->u4ContextId;

    IP6_TRC_ARG5 (u4ContextId, IP6_MOD_TRC,
                  DATA_PATH_TRC | BUFFER_TRC, IP6_NAME,
                  "IP6IF:Ip6ifSend:IP6IF = %d NDC = %p BufLen = %d BufPtr = "
                  "%p NextHop = %s\n",
                  pIf6->u4Index, pNd6CacheEntry, u4Len, pBuf,
                  Ip6PrintAddr (pDstAddr6));

    switch (pIf6->u1IfType)
    {
        case IP6_ENET_INTERFACE_TYPE:
        case IP6_L3VLAN_INTERFACE_TYPE:
        case IP6_LAGG_INTERFACE_TYPE:
        case IP6_PSEUDO_WIRE_INTERFACE_TYPE:
        case IP6_L3SUB_INTF_TYPE:
#ifdef WGS_WANTED
        case IP6_L2VLAN_INTERFACE_TYPE:
#endif
            Ip6ifEthSend (pIf6, pDstAddr6, pNd6CacheEntry, u4Len, pBuf,
                          u1RouteAddrType);
            break;

        case IP6_X25_INTERFACE_TYPE:
            Ip6ifX25Send (pIf6, u4Len, pBuf);
            break;

        case IP6_FR_INTERFACE_TYPE:
            Ip6ifFrSend (pIf6, u4Len, pBuf);
            break;

        case IP6_PPP_INTERFACE_TYPE:
            Ip6ifPppSend (pIf6, u4Len, pBuf);
            break;

#ifdef TUNNEL_WANTED
        case IP6_TUNNEL_INTERFACE_TYPE:
            Ip6ifTunlSend (pIf6, pDstAddr6, pNd6CacheEntry, u4Len, pBuf);
            break;
#endif

        default:
            IP6_TRC_ARG1 (u4ContextId, IP6_MOD_TRC,
                          DATA_PATH_TRC, IP6_NAME,
                          "IP6IF: Ip6ifSend: Unknown Interface Type %d\n",
                          pIf6->u1IfType);
            Ip6BufRelease (u4ContextId, pBuf, FALSE, IP6_IF_SUBMODULE);
            IP6_TRC_ARG2 (u4ContextId, IP6_MOD_TRC, MGMT_TRC, IP6_NAME,
                          "IP6IF: Ip6ifSend: %s gen err:, Type = %d  ",
                          ERROR_FATAL_STR, ERR_GEN_INVALID_VAL);
            break;
    }

}

/*
 * LAN (Ethernet) Interface Handling Routines
 */

/******************************************************************************
 * DESCRIPTION : This routine is called to send out a IPv6 datagram on a
 *               Ethernet interface. If the datagram is destined to a multicast
 *               IPv6 address, the destination MAC address is formed as per
 *               RFC 1972. Otherwise, if the ND6 Cache entry is not present or
 *               its state is incomplete, the packet is "queued" onto the ND6
 *               module for Address resolution and transmission; else, the MAC
 *               address is got from the Cache entry. After the MAC parameters
 *               are filled in the buffer, the output routine is called to 
 *               send out the packet.
 *
 * INPUTS      : The interface pointer (pIf6), destination IPv6 address
 *               (pDstAddr6), ND Cache pointer (pNd6CacheEntry) which is
 *               used only for LAN interfaces, length of the datagram (u4Len),
 *               the buffer (pBuf) nd the destination address route entry
 *               if anycast
 *
 *
 * OUTPUTS     : None
 *
 * RETURNS     : None
 *
 * NOTES       :
 ******************************************************************************/

VOID
Ip6ifEthSend (tIp6If * pIf6, tIp6Addr * pDstAddr6,
              tNd6CacheEntry * pNd6CacheEntry, UINT4 u4Len,
              tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 u1RouteAddrType)
{

    UINT1               au1hwaddr[IP6_ENET_ADDR_LEN];
    UINT1               u1HwaddrLen = 0;

    MEMSET (au1hwaddr, 0, IP6_ENET_ADDR_LEN);

    if (Ip6AddrType (pDstAddr6) == ADDR6_MULTI)
    {
        /* form the MAC address as specified in RFC 1972 */

        IP6_TRC_ARG1 (pIf6->pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                      DATA_PATH_TRC, IP6_NAME,
                      "IP6IF: Ip6ifEthSend: Sending multicast packet to dest = %s\n",
                      Ip6PrintAddr (pDstAddr6));

        Ip6ifFormEthMaddr (pDstAddr6, au1hwaddr, &u1HwaddrLen);
    }
    else if (pIf6->u4UnnumAssocIPv6If != 0)
    {
        /*This is a unnumbered interface use peer mac address as the destination */
        CfaGetIfUnnumPeerMac (pIf6->u4Index, au1hwaddr);
        Ip6ifFormEthHdr (au1hwaddr, pBuf);
        Ip6Output (pIf6, u4Len, pBuf);
        return;
    }
    else if ((!pNd6CacheEntry) ||
             (Nd6GetReachState (pNd6CacheEntry) == ND6C_INCOMPLETE) ||
             (Nd6GetReachState (pNd6CacheEntry) == ND6C_STATIC_NOT_IN_SERVICE))
    {

        /*
         * We don't have the Neighbor cache entry or the entry is in
         * Incomplete state (RFC 1970). So pass packet to ND module which
         * will queue it up for sending later (if possible)
         */

        IP6_TRC_ARG2 (pIf6->pIp6Cxt->u4ContextId,
                      IP6_MOD_TRC, DATA_PATH_TRC | BUFFER_TRC, IP6_NAME,
                      "IP6IF: Ip6ifEthSend: Enqueuing pkt to ND, Buflen = %d "
                      "Bufptr = %p\n", u4Len, pBuf);
        Nd6Resolve (pNd6CacheEntry, pIf6, pDstAddr6, pBuf, u1RouteAddrType);
        return;

    }
    else
    {
        /* get MAC address from the Neighbor cache entry */

        Nd6GetLladdr (pNd6CacheEntry, au1hwaddr, &u1HwaddrLen);
    }

    /* fill in the Ethernet MAC addresses and send out */

    Ip6ifFormEthHdr (au1hwaddr, pBuf);
    Ip6Output (pIf6, u4Len, pBuf);

}

/******************************************************************************
 * DESCRIPTION : Gets v4Addr from 6to4Addr.
 *
 * INPUTS      : 6to4 address
 *
 * OUTPUTS     : None.
 *
 * RETURNS     : v4Addr
 *
 ******************************************************************************/
UINT4
IP6_Get_V4_From_6to4 (tIp6Addr * pAddr)
{
    UINT4               u4IpAddr = 0;
    INT4                i4Count1 = 0, i4Count2 = 0;
    for (i4Count1 = 0, i4Count2 = IP6_TWO; i4Count1 < IP6_FOUR;
         i4Count1++, i4Count2++)
    {
        ((UINT1 *) &u4IpAddr)[i4Count1] = pAddr->u1_addr[i4Count2];
    }
    return OSIX_HTONL (u4IpAddr);
}

/******************************************************************************
 * DESCRIPTION : Form the link-local address on the LAN interface by using
 *               the MAC address
 *
 * INPUTS      : The interface pointer (pIf6)
 *
 * OUTPUTS     : None
 *
 * RETURNS     : OK upon success
 *               NOT_OK upon failure
 *
 * NOTES       :
 ******************************************************************************/

INT4
Ip6ifFormLanLla (tIp6If * pIf6)
{
    UINT4               u4Ind = 0;
    UINT1               au1ethAddr[IP6_ENET_ADDR_LEN];
    UINT1               u1EthAlen = 0;
    UINT1               au1Ip6IfId[IP6_EUI_ADDRESS_LEN];    /* Interface Id max 
                                                             * len is 64 bits acc 
                                                             * to EUI format
                                                             */
    tIp6MacFilterNode  *pNode = NULL;
    /* get the MAC address on this interface */

    Ip6ifGetEthLladdr (pIf6, au1ethAddr, &u1EthAlen);
    if (!u1EthAlen)
    {
        IP6_TRC_ARG2 (pIf6->pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                      DATA_PATH_TRC | ALL_FAILURE_TRC, IP6_NAME,
                      "IP6IF: Ip6ifFormLanLla: get MAC addr to form LLA, "
                      "IP6 IF = %d IfNum = %d\n", pIf6->u4Index, pIf6->u4Index);
        IP6_TRC_ARG2 (pIf6->pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                      MGMT_TRC, IP6_NAME,
                      "IP6IF: Ip6ifFormLanLla: %s gen err:, Type = %d  ",
                      ERROR_FATAL_STR, ERR_GEN_INVALID_VAL);
        return (IP6_FAILURE);
    }

    for (u4Ind = 0; u4Ind < (UINT4) IP6_MAX_FILTER; u4Ind++)
    {
        if (gIp6GblInfo.aIp6MACFilter[u4Ind].u4Index == pIf6->u4Index)
        {
            break;
        }
    }
    if (u4Ind == (UINT4) IP6_MAX_FILTER)
    {
        Ip6AddMACAddrInMACFilter (pIf6->u4Index, au1ethAddr);
    }
    else
    {
        /* Find the matching node */
        TMO_SLL_Scan (&gIp6GblInfo.aIp6MACFilter[u4Ind].MacAddrList, pNode,
                      tIp6MacFilterNode *)
        {
            if (MEMCMP (au1ethAddr, pNode->u1MacAddr, MAC_ADDR_LEN) == 0)
            {
                /* Match found. */
                break;
            }
        }
        if (pNode == NULL)
        {
            Ip6AddMACAddrInMACFilter (pIf6->u4Index, au1ethAddr);
        }
    }
    /*
     *  copy the token and length into our interface structure
     */
    /*  Constructing EUI-64 address to form 64 bit if identifier. */
    pIf6->u1TokLen = IP6_EUI_ADDRESS_LEN;
    GET_EUI_64_IF_ID (au1ethAddr, au1Ip6IfId);
    MEMCPY (pIf6->ifaceTok, au1Ip6IfId, IP6_EUI_ADDRESS_LEN);
    /*
     * form the link-local address using the MAC address
     */

    return (Ip6ifCreateLla (pIf6, au1Ip6IfId, IP6_EUI_ADDRESS_LEN));

}

/******************************************************************************
 * DESCRIPTION : Obtain the MAC address of the specified interface.
 *
 * INPUTS      : The interface pointer (pIf6)
 *
 * OUTPUTS     : MAC address (pMacaddr) and MAC address length (pMaclen)
 *
 * RETURNS     : None
 *
 * NOTES       :
 ******************************************************************************/

VOID
Ip6ifGetEthLladdr (tIp6If * pIf6, UINT1 *pMacaddr, UINT1 *pMaclen)
{
    if (pIf6->u1IfType == IP6_TUNNEL_INTERFACE_TYPE)
    {
        MEMCPY (pMacaddr, &pIf6->ifaceTok, MAC_ADDR_LEN);
    }
    else
    {
        Ip6GetHwAddr (pIf6->u4Index, pMacaddr);
    }
    *pMaclen = MAC_ADDR_LEN;
    return;
}

/******************************************************************************
 * DESCRIPTION : Forms the multicast MAC address corresponding to the
 *               IPv6 address as specified in RFC 1972.
 *
 * INPUTS      : The destination IPv6 address (pAddr6)
 *
 * OUTPUTS     : The multicast MAC address (pMacaddr) and the MAC address
 *               length (pMaclen)
 *
 * RETURNS     : None
 *
 * NOTES       :
 ******************************************************************************/

VOID
Ip6ifFormEthMaddr (tIp6Addr * pAddr6, UINT1 *pMacaddr, UINT1 *pMaclen)
{
    pMacaddr[0] = MCAST_MAC_ADDR_OCTET_1;
    pMacaddr[1] = MCAST_MAC_ADDR_OCTET_2;

    MEMCPY (pMacaddr + IP6_TWO, &pAddr6->u4_addr[IP6_THREE], sizeof (UINT4));
    *pMaclen = IP6_ENET_ADDR_LEN;

}

/*
 * PPP Interface Handling Routines
 */

/******************************************************************************
 * DESCRIPTION : This routine is called to send out a IPv6 datagram on a
 *               PPP interface. It merely calls the output routine to send out
 *               the packet.
 *
 * INPUTS      : The interface pointer (pIf6), length of the datagram (u4Len)
 *               and the buffer (pBuf)
 *
 * OUTPUTS     : None
 *
 * RETURNS     : None
 *
 * NOTES       :
 ******************************************************************************/

VOID
Ip6ifPppSend (tIp6If * pIf6, UINT4 u4Len, tCRU_BUF_CHAIN_HEADER * pBuf)
{
    Ip6Output (pIf6, u4Len, pBuf);
}

/******************************************************************************
 * DESCRIPTION : Form the link-local address on the PPP interface by using
 *               the interface token configured/negotiated by PPP.
 *
 * INPUTS      : The interface pointer (pIf6)
 *
 * OUTPUTS     : None
 *
 * RETURNS     : OK upon success
 *               NOT_OK upon failure
 *
 * NOTES       :
 ******************************************************************************/

INT4
Ip6ifFormPppLla (tIp6If * pIf6)
{

    UINT1               au1iftok[IP6_EUI_ADDRESS_LEN];    /* Interface Id max 
                                                         * len is 64 bits acc 
                                                         * to EUI format
                                                         */
    UINT1               u1Toklen = 0;

    /* get the interface token */

    MEMSET (au1iftok, IP6_ZERO, IP6_EUI_ADDRESS_LEN);

    Ip6ifGetPppLladdr (pIf6, au1iftok, &u1Toklen);
    if (!u1Toklen)
    {
        IP6_TRC_ARG2 (pIf6->pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                      DATA_PATH_TRC | ALL_FAILURE_TRC, IP6_NAME,
                      "IP6IF:Ip6ifFormPppLla:Failed to get PPP interface "
                      "token IP6IF = %d IfNum = %d\n",
                      pIf6->u4Index, pIf6->u4Index);
        IP6_TRC_ARG2 (pIf6->pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                      MGMT_TRC, IP6_NAME,
                      "IP6IF:Ip6ifFormPppLla:%s gen err:, Type = %d  ",
                      ERROR_FATAL_STR, ERR_GEN_INVALID_VAL);
        return (IP6_FAILURE);
    }

    /* 
     * if there is no existing link-local address or if the interface token
     * has changed, form the new link-local address
     */

    if ((IP6IF_TOKENLESS (pIf6)) ||
        (MEMCMP (pIf6->ifaceTok, au1iftok, u1Toklen) != 0))
    {

        /* delete existing token if present */

        if (!IP6IF_TOKENLESS (pIf6))
        {
            Ip6ifLlaStatus (pIf6, IP6_IF_DELETE);
        }

        /*
         * copy the token and length into our interface structure
         * and form the link-local address
         */

        pIf6->u1TokLen = u1Toklen;
        MEMCPY (pIf6->ifaceTok, au1iftok, u1Toklen);

        return (Ip6ifCreateLla (pIf6, au1iftok, u1Toklen));
    }

    return (IP6_SUCCESS);

}

/******************************************************************************
 * DESCRIPTION : Obtain the interface token (link address) on the PPP interface.
 *
 * INPUTS      : The interface pointer (pIf6)
 *
 * OUTPUTS     : Interface token (pToken) and token length (pToklen)
 *
 * RETURNS     : None
 *
 * NOTES       :
 ******************************************************************************/

VOID
Ip6ifGetPppLladdr (tIp6If * pIf6, UINT1 *pToken, UINT1 *pToklen)
{
    UNUSED_PARAM (pIf6);
    UNUSED_PARAM (pToken);
    UNUSED_PARAM (pToklen);
}

/*
 * NBMA Interface Handling Routines
 */

/******************************************************************************
 * DESCRIPTION : This routine is called to send out a IPv6 datagram on a
 *               Frame Relay interface. It merely calls the output routine to 
 *               send out the packet.
 *
 * INPUTS      : The interface pointer (pIf6), length of the datagram (u4Len)
 *               and the buffer (pBuf)
 *
 * OUTPUTS     : None
 *
 * RETURNS     : None
 *
 * NOTES       :
 ******************************************************************************/

VOID
Ip6ifFrSend (tIp6If * pIf6, UINT4 u4Len, tCRU_BUF_CHAIN_HEADER * pBuf)
{
    Ip6Output (pIf6, u4Len, pBuf);
}

/******************************************************************************
 * DESCRIPTION : This routine is called to send out a IPv6 datagram on a
 *               X25 interface. It merely calls the output routine to send out
 *               the packet.
 *
 * INPUTS      : The interface pointer (pIf6), length of the datagram (u4Len)
 *               and the buffer (pBuf)
 *
 * OUTPUTS     : None
 *
 * RETURNS     : None
 *
 * NOTES       :
 ******************************************************************************/

VOID
Ip6ifX25Send (tIp6If * pIf6, UINT4 u4Len, tCRU_BUF_CHAIN_HEADER * pBuf)
{
    Ip6Output (pIf6, u4Len, pBuf);
}

/******************************************************************************
 * DESCRIPTION : Form the link-local address on the FR/X25 interface by using
 *               the interface token obtained from it. This will typically
 *               be the DLCI or PVC number or the X121 address.
 *
 * INPUTS      : The interface pointer (pIf6)
 *
 * OUTPUTS     : None
 *
 * RETURNS     : OK upon success
 *               NOT_OK upon failure
 *
 * NOTES       :
 ******************************************************************************/

INT4
Ip6ifFormWanLla (tIp6If * pIf6)
{

    UINT1               au1iftok[IP6_EUI_ADDRESS_LEN];
    UINT1               u1Toklen = 0;

    MEMSET (au1iftok, IP6_ZERO, IP6_EUI_ADDRESS_LEN);

    /* get the interface token */
    Ip6ifGetWanLladdr (pIf6, au1iftok, &u1Toklen);

    if (!u1Toklen)
    {

        IP6_TRC_ARG3 (pIf6->pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                      DATA_PATH_TRC, IP6_NAME,
                      "IP6IF:Ip6ifFormWanLla:Fail to get token on NBMA, "
                      "IP6IF = %d IfNum = %d Ckt = %d\n",
                      pIf6->u4Index, pIf6->u4Index, pIf6->u2CktIndex);

        return (IP6_FAILURE);
    }

    /*
     *  copy the token and length into our interface structure
     */

    pIf6->u1TokLen = u1Toklen;
    MEMCPY (pIf6->ifaceTok, au1iftok, u1Toklen);

    /*
     * form the link-local address using the token
     */

    return (Ip6ifCreateLla (pIf6, au1iftok, u1Toklen));

}

/******************************************************************************
 * DESCRIPTION : Obtain the interface token on a FR/X25 interface.
 *
 * INPUTS      : The interface pointer (pIf6)
 *
 * OUTPUTS     : Interface token (pAddr) and token length (pAlen)
 *
 * RETURNS     : None
 *
 * NOTES       :
 ******************************************************************************/

VOID
Ip6ifGetWanLladdr (tIp6If * pIf6, UINT1 *pAddr, UINT1 *pAlen)
{
    UNUSED_PARAM (pIf6);
    UNUSED_PARAM (pAddr);
    UNUSED_PARAM (pAlen);
}

#ifdef TUNNEL_WANTED

/*
 * Tunnel Interface Handling Routines
 */

/******************************************************************************
 * DESCRIPTION : This routine is called to send out a IPv6 datagram on a
 *               Tunnel interface.
 *
 * INPUTS      : The interface pointer (pIf6), the destination IPv6 address
 *               (pDst6), the length of the datagram (u4Len) and
 *               the buffer (pBuf)
 *
 * OUTPUTS     : None
 *
 * RETURNS     : None
 *
 * NOTES       :
 ******************************************************************************/

VOID
Ip6ifTunlSend (tIp6If * pIf6, tIp6Addr * pDst6, tNd6CacheEntry * pNd6CacheEntry,
               UINT4 u4Len, tCRU_BUF_CHAIN_HEADER * pBuf)
{

    if (pIf6->pTunlIf->u1TunlType == IPV6_OVER_IPV6_TUNNEL)
    {
        Ip6Tunl6Send (pIf6, pBuf);
    }
    else
    {
        Ip6Tunl4Send (pIf6, pDst6, pNd6CacheEntry, u4Len, pBuf);
    }
}

#endif /* TUNNEL_WANTED */

/*
 * Misc Routines
 */

/******************************************************************************
 * DESCRIPTION : This routine is called after a route is determined for an
 *               outgoing IPv6 packet. It determines the neighbor cache
 *               entry which holds the link-layer address for the next hop
 *               and in case of NBMA networks, determines the exact outgoing
 *               interface for directly-connected destinations.
 *
 * INPUTS      : The routing entry pointer (pRt6) and the destination IPv6
 *               address (pDst)
 *
 * OUTPUTS     : The outgoing IPv6 interface (pPIf6)
 *
 * RETURNS     : The neighbor cache pointer or NULL
 *
 * NOTES       :
 ******************************************************************************/

tNd6CacheEntry     *
Ip6ifResolveDst (tNetIpv6RtInfo * pRt6, tIp6Addr * pDst, tIp6If ** pPIf6)
{

    UINT1               u1RtType = 0;
    tIp6If             *pDstIf6 = *pPIf6;
    tNd6CacheEntry     *pNd6c = NULL;
    tIp6Addr           *pNextHop = NULL;

    IP6_TRC_ARG3 (pRt6->u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                  "IP6IF:Ip6ifResolveDst:Resolving destination, RTPtr = %p "
                  "IFPtr = %p Dest = %s\n", pRt6, pDstIf6, Ip6PrintAddr (pDst));

    if (!pDstIf6)
    {
        return (NULL);
    }

#ifdef TUNNEL_WANTED
    if ((pDstIf6->u1IfType == IP6_TUNNEL_INTERFACE_TYPE) &&
        ((pDstIf6->pTunlIf->u1TunlType == IPV6_AUTO_COMPAT) ||
         (pDstIf6->pTunlIf->u1TunlType == IPV6_ISATAP_TUNNEL) ||
         (pDstIf6->pTunlIf->u1TunlType == IPV6_SIX_TO_FOUR)))
    {
        /* NUD is not done over auto tunnel interface */
        return (NULL);
    }
#endif

    u1RtType = (UINT1) pRt6->i1Type;

    switch (pDstIf6->u1IfType)
    {

        case IP6_ENET_INTERFACE_TYPE:
        case IP6_L3VLAN_INTERFACE_TYPE:
        case IP6_LAGG_INTERFACE_TYPE:
        case IP6_PSEUDO_WIRE_INTERFACE_TYPE:
        case IP6_LOOPBACK_INTERFACE_TYPE:
        case IP6_L3SUB_INTF_TYPE:
#ifdef TUNNEL_WANTED
        case IP6_TUNNEL_INTERFACE_TYPE:    /* NUD is to be done for tunnel also */
#endif
#ifdef WGS_WANTED
        case IP6_L2VLAN_INTERFACE_TYPE:
#endif

            pNextHop = &(pRt6->NextHop);

            /* 
             * For INDIRECT routes, try to obtain the Neighbor cache entry
             * from the route entry itself. If route is not INDIRECT or we
             * fail to obtain the Neighbor cache entry, do a lookup of the
             * Neighbor cache. If we find a Neighbor cache entry, update
             * the route also (provided it is INDIRECT) so we avoid the
             * lookup from now on.
             *
             * For DIRECT routes, we cannot have the Neighbor cache in the
             * route as the same route is used to reach many neighbors.
             */
            pNd6c = Nd6LookupCache (pDstIf6, (u1RtType == DIRECT) ?
                                    pDst : pNextHop);

            IP6_TRC_ARG3 (pRt6->u4ContextId, IP6_MOD_TRC,
                          DATA_PATH_TRC, IP6_NAME,
                          "IP6IF: Ip6ifResolveDst: ND lookup gave NDC = %p, "
                          "RTPtr = %p IFPtr = %p\n", pNd6c, pRt6, pDstIf6);

            if (pNd6c)
            {
                /* update Neighbor cache Usage time */
                Nd6UpdateLastUseTime (pNd6c);
            }

            break;

        case IP6_X25_INTERFACE_TYPE:
        case IP6_FR_INTERFACE_TYPE:

            /*
             * In case of NBMA interfaces, we are actually looking for the
             * correct outgoing interface and dont require any other
             * information from the Neighbor Cache. In the case of INDIRECT
             * routes, the route entry itself tells us the destination
             * interface; we dont need to look further in the Neighbor
             * Cache. In the case of DIRECT routes, the route entry tells
             * us how to reach the network; we need to lookup the Neighbor
             * cache to get the proper interface to reach our Neighbor
             */

            if (u1RtType == DIRECT)
            {
                pNd6c = Nd6LookupCache (pDstIf6, pDst);

                IP6_TRC_ARG3 (pRt6->u4ContextId, IP6_MOD_TRC,
                              DATA_PATH_TRC, IP6_NAME,
                              "IP6IF:Ip6ifResolveDst: ND lookup gave NDC = %p, "
                              "RTPtr = %p IFPtr = %p\n", pNd6c, pRt6, pDstIf6);

                if (pNd6c != NULL)
                {
                    pDstIf6 = pNd6c->pIf6;
                }
                else
                {
                    pDstIf6 = NULL;
                }
            }
            break;

        case IP6_PPP_INTERFACE_TYPE:
            /* no Neighbor cache for these interfaces */
            break;

        default:
            IP6_TRC_ARG3 (pRt6->u4ContextId, IP6_MOD_TRC,
                          DATA_PATH_TRC, IP6_NAME,
                          "IP6IF:Ip6ifResolveDst: Unknown Interface Type %d, "
                          "IFptr = %p RTptr = %p\n",
                          pDstIf6->u1IfType, pDstIf6, pRt6);
            pDstIf6 = NULL;
            IP6_TRC_ARG2 (pRt6->u4ContextId, IP6_MOD_TRC,
                          MGMT_TRC, IP6_NAME,
                          "IP6IF:Ip6ifResolveDst: %s gen err:, Type = %d  ",
                          ERROR_FATAL_STR, ERR_GEN_INVALID_VAL);
            break;
    }

    IP6_TRC_ARG2 (pRt6->u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                  "IP6IF:Ip6ifResolveDst:Resolve Dest returning NDPtr = %p "
                  "IFPtr = %p\n", pNd6c, pDstIf6);

    *pPIf6 = pDstIf6;

    return (pNd6c);

}

/******************************************************************************
 * DESCRIPTION : Obtain the MTU of the interface Index.
 *
 * INPUTS      : The interface Index (u4Index)
 *
 * OUTPUTS     : None
 *
 * RETURNS     : The MTU of the interface, if Interface exists, 
 *               IP6_FAILURE otherwise.
 *
 * NOTES       :
 ******************************************************************************/
UINT4
Ip6GetMtu (UINT4 u4Index)
{
    if (Ip6ifEntryExists (u4Index) == IP6_SUCCESS)
    {
        return (Ip6ifGetMtu (gIp6GblInfo.apIp6If[u4Index]));
    }
    else
    {
        return 0;
    }
}

/******************************************************************************
 * DESCRIPTION : Obtain the MTU of the interface.
 *
 * INPUTS      : The interface pointer (pIf6)
 *
 * OUTPUTS     : None
 *
 * RETURNS     : The MTU of the interface
 *
 * NOTES       :
 ******************************************************************************/

UINT4
Ip6ifGetMtu (tIp6If * pIf6)
{
#ifdef TUNNEL_WANTED
    UINT4               u4DefMtu;

    if (pIf6->u1IfType == IP6_TUNNEL_INTERFACE_TYPE)
    {
        /* Configured Tunnel. */
        u4DefMtu = IP6_TUNLIF_MTU (pIf6);    /* Over tunnels Pmtu is discovered 
                                             * using V4 PMtud if Pmtu is not 
                                             * there need to IPv6MIN MTU.
                                             */
        if (u4DefMtu <= IP6_MIN_MTU)
        {
            u4DefMtu = IP6_MIN_MTU;
        }
    }
    else
    {
        u4DefMtu = pIf6->u4Mtu;
    }
    return u4DefMtu;
#else
    return pIf6->u4Mtu;
#endif
}

/******************************************************************************
 * DESCRIPTION : Hash the interface number and circuit number to search
 *               the IPv6 IF Hash table.
 *
 * INPUTS      : The interface number (u1If) and the circuit number (u2Ckt)
 *
 * OUTPUTS     : None
 *
 * RETURNS     : The hash value.
 *
 * NOTES       :
 ******************************************************************************/

UINT4
Ip6ifHash (UINT4 u4If, UINT2 u2Ckt)
{

    char                key[20], *p = NULL;
    INT1                i1Keylen = 0;
    INT4                i4Count = 0;
    UINT4               u4Temp1 = 0, u4Temp2 = 0;

    u4Temp1 = 0;
    key[0] = '\0';
    SNPRINTF (key, 20, "%d%d", u4If, u2Ckt);
    i1Keylen = (INT1) STRLEN (key);

    for (i4Count = 0, p = key; i4Count < i1Keylen; i4Count++, p++)
    {
        u4Temp1 = (u4Temp1 << IP6_FOUR) + (*p);
        if ((u4Temp2 = u4Temp1 & 0xf0000000))
        {
            u4Temp1 = u4Temp1 ^ (u4Temp2 >> 24);
            u4Temp1 = u4Temp1 ^ u4Temp2;
        }
    }

    return (u4Temp1 % IP6IF_HASH_TBL_SIZE);

}

/******************************************************************************
 * DESCRIPTION : Obtain the Oper status of the interface (in case of IPv4
 *               Tunnels, the status of IPv4)
 *
 * INPUTS      : The interface type (u1Type), interface number (u1If) and
 *               circuit number (u2Ckt)
 *
 * OUTPUTS     : None
 *
 * RETURNS     : OPER_UP if the lower layer is UP
 *               OPER_DOWN if the lower layer is DOWN
 *
 * NOTES       :
 ******************************************************************************/

UINT1
Ip6ifGetIfOper (UINT1 u1Type, UINT4 u4If, UINT2 u2Ckt)
{
    tIp6If             *pIf6 = NULL;
    UINT4               u4ContextId = VCM_INVALID_VC;
    UNUSED_PARAM (u2Ckt);

    pIf6 = Ip6ifGetEntry (u4If);
    if (pIf6 == NULL)
    {
        return (OPER_DOWN);
    }
    Ip6GetCxtId (u4If, &u4ContextId);

    switch (u1Type)
    {
        case IP6_ENET_INTERFACE_TYPE:
        case IP6_L3VLAN_INTERFACE_TYPE:
        case IP6_LAGG_INTERFACE_TYPE:
        case IP6_PSEUDO_WIRE_INTERFACE_TYPE:
        case IP6_X25_INTERFACE_TYPE:
        case IP6_FR_INTERFACE_TYPE:
        case IP6_PPP_INTERFACE_TYPE:
        case IP6_L3SUB_INTF_TYPE:
#ifdef WGS_WANTED
        case IP6_L2VLAN_INTERFACE_TYPE:
#endif
            /* If the lower layer indicates that the interface is
             * active and if the interface MTU value is greater than
             * 1280, then Ip6 interface operation status can be
             * set as active.
             */
            if ((pIf6->u1LLOperStatus == ADMIN_UP) &&
                (pIf6->u4Mtu >= IP6_MIN_MTU))
            {
                return OPER_UP;
            }
            else
            {
                return OPER_DOWN;
            }

#ifdef TUNNEL_WANTED
        case IP6_TUNNEL_INTERFACE_TYPE:
            /* If the lower layer indicates that the interface is
             * active, then Ip6 interface operation status can be
             * set as active.
             */
            if (pIf6->u1LLOperStatus == ADMIN_UP)
            {
                return OPER_UP;
            }
            else
            {
                return OPER_DOWN;
            }
#endif
        case IP6_LOOPBACK_INTERFACE_TYPE:
            if (pIf6->u1LLOperStatus == ADMIN_UP)
            {
                return OPER_UP;
            }
            else
            {
                return OPER_DOWN;
            }
        default:
            IP6_TRC_ARG1 (u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                          "IP6IF: Ip6ifGetIfOper: Unknown type = %d\n", u1Type);
            IP6_TRC_ARG2 (u4ContextId, IP6_MOD_TRC, MGMT_TRC, IP6_NAME,
                          "IP6IF: Ip6ifGetIfOper:%s gen err:, Type = %d  ",
                          ERROR_FATAL_STR, ERR_GEN_INVALID_VAL);
            return (OPER_DOWN);
    }
}

/******************************************************************************
 * DESCRIPTION : This routine creates the link-local address on the interface
 *               by calling a routine of the IP6ADDR submodule.
 *
 * INPUTS      : The interface pointer (pIf6), interface token (pTok) and
 *               the token length (u1Tokl)
 *
 * OUTPUTS     : None
 *
 * RETURNS     : OK upon success
 *               NOT_OK upon failure to create the link-local address
 *
 * NOTES       :
 ******************************************************************************/

INT4
Ip6ifCreateLla (tIp6If * pIf6, UINT1 *pTok, UINT1 u1Tokl)
{
    tIp6LlocalInfo     *pLladdr = NULL;
    tIp6LlocalInfo     *pAddr = NULL;
    tTMO_SLL_NODE      *pSnode = NULL;
    INT4                i4LlAddressPresent = 0;
    UINT4               u4Index = pIf6->u4Index;
    UINT1               u1Scope = ADDR6_SCOPE_INTLOCAL;    /*RFC 4007 scope to be created */

    IP6_TRC_ARG2 (pIf6->pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                  DATA_PATH_TRC, IP6_NAME,
                  "IP6IF: Ip6ifCreateLla: IP6 IF = %d Token = %s\n",
                  pIf6->u4Index, Ip6PrintIftok (pTok, u1Tokl));

    /*
     * call routine of ADDR submodule (ip6addr.c) to form the link-local
     * address structure
     */

    if ((pLladdr = Ip6AddrCreateLla (pTok, u1Tokl, pIf6)) == NULL)
    {
        IP6_TRC_ARG2 (pIf6->pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                      DATA_PATH_TRC | ALL_FAILURE_TRC, IP6_NAME,
                      "IP6IF: Ip6ifCreateLla: LLA failed in IP6ADDR "
                      "IP6 IF = %d Token = %s\n",
                      pIf6->u4Index, Ip6ErrPrintIftok (ERROR_FATAL, pTok,
                                                       u1Tokl));
        return (IP6_FAILURE);
    }

    IP6_TRC_ARG1 (pIf6->pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                  DATA_PATH_TRC, IP6_NAME,
                  "IP6IF: Ip6ifCreateLla:LLA Info pointer given by "
                  "IP6ADDR = %p\n", pLladdr);

    pLladdr->u1ConfigMethod = IP6_ADDR_AUTO_SL;

    /* Update the Admin Status and the Config Method */
    pLladdr->u1AdminStatus = IP6FWD_ACTIVE;
    TMO_SLL_Scan (&pIf6->lla6Ilist, pSnode, tTMO_SLL_NODE *)
    {
        pAddr = IP6_LLADDR_PTR_FROM_SLL (pSnode);
        if (Ip6AddrMatch
            (&(pAddr->ip6Addr), &(pLladdr->ip6Addr),
             IP6_ADDR_MAX_PREFIX) == TRUE)
        {
            i4LlAddressPresent = 1;
        }
    }
    if (i4LlAddressPresent == 0)
    {
        /* add link-local address into our linked list (SLL) */
        TMO_SLL_Add (&pIf6->lla6Ilist, &pLladdr->nextLif);

        /* RFC4007 Create the LLocal zone for this interface only if no other
           link-local zone exist on this interface */
        for (u1Scope = ADDR6_SCOPE_INTLOCAL; u1Scope <= ADDR6_SCOPE_LLOCAL;
             u1Scope++)
        {
            Ip6ZoneCreateAutoScopeZone (u4Index, u1Scope);
        }
        if (pIf6->u1IfType == IP6_TUNNEL_INTERFACE_TYPE)
        {
            Ip6ZoneCreateAutoScopeZone (u4Index, ADDR6_SCOPE_GLOBAL);
        }

    }
    else
    {
        Ip6AddrDeleteSolicitMcast (pIf6->u4Index, &pLladdr->ip6Addr);
        Ip6AddrDeleteInfo ((UINT1 *) pLladdr, ADDR6_LLOCAL);

    }
    KW_FALSEPOSITIVE_FIX (pLladdr);
    return (IP6_SUCCESS);

}

/******************************************************************************
 * DESCRIPTION : This routine takes actions on the link-local address based
 *               on the status. Actions when the interface status becomes
 *               UP, DOWN or DELETE is handled here.
 *
 * INPUTS      : The interface pointer (pIf6) and status (u1Status)
 *
 * OUTPUTS     : None
 *
 * RETURNS     : None
 *
 * NOTES       :
 ******************************************************************************/

VOID
Ip6ifLlaStatus (tIp6If * pIf6, UINT1 u1Status)
{

    tTMO_SLL_NODE      *pNext = NULL, *pCur = NULL;
    tIp6LlocalInfo     *pLladdr = NULL;
    tIp6LlocalInfo     *pNextLladdr = NULL;
    UINT1               au1ethAddr[IP6_ENET_ADDR_LEN];
    UINT1               u1EthAlen = 0;
    IP6_TRC_ARG2 (pIf6->pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                  DATA_PATH_TRC, IP6_NAME,
                  "IP6IF: Ip6ifLlaStatus: status change, IP6 IF = %d Status = %d\n",
                  pIf6->u4Index, u1Status);

    /* 
     * take required action for each of our link-local addresses.....
     * our processing of SLL is a bit weird - we do a "First" and "Next"
     * instead of the normal "Scan" as we have to handle the "delete" case
     */

    pNext = TMO_SLL_First (&pIf6->lla6Ilist);
    while ((pCur = pNext) != NULL)
    {
        pNext = TMO_SLL_Next (&pIf6->lla6Ilist, pCur);
        pLladdr = IP6_LLADDR_PTR_FROM_SLL (pCur);

        if ((u1Status == IP6_IF_DELETE) || (u1Status == IP6_IF_DOWN))
        {
            if (pLladdr->u1Status & ADDR6_PREFERRED)
            {
                /* Indicate to the higher layer about the Link-Local
                 * Address status change. */
                NetIpv6InvokeAddressChange (&(pLladdr->ip6Addr),
                                            (IP6_ADDR_MAX_PREFIX),
                                            ADDR6_LLOCAL, pIf6->u4Index,
                                            (NETIPV6_ADDRESS_DELETE));
            }
            if ((pLladdr->u1Status & ADDR6_DOWN) != ADDR6_DOWN)
            {
                /*
                 * For every link-local address, a corresponding solicated
                 * multicast address is added. So derive the solicated
                 * multi-cast address for the link-local address and delete
                 * it from the mcast list supported for this interface.
                 */
                Ip6AddrDeleteSolicitMcast (pIf6->u4Index, &pLladdr->ip6Addr);
            }
        }

        if (u1Status == IP6_IF_DELETE)
        {
            TMO_SLL_Delete (&pIf6->lla6Ilist, pCur);
        }

        Ip6AddrNotifyLla (pLladdr, u1Status);

        if ((u1Status == IP6_IF_DOWN) && (pNext != NULL))
        {
            pNextLladdr = IP6_LLADDR_PTR_FROM_SLL (pNext);
            if ((pNextLladdr->u1Status & ADDR6_FAILED) &&
                (pNextLladdr->u1ConfigMethod == IP6_ADDR_STATIC))
            {
                /* DAD for next Link-Local Address in the list had Failed.
                 * Means that the newly configured Link-Local address had
                 * failed in DAD and we have retained the old address. 
                 * Now the old address should be deleted and when the 
                 * interface comes up, DAD should be performed for the
                 * new address. */
                TMO_SLL_Delete (&pIf6->lla6Ilist, pCur);
                Ip6AddrDeleteInfo ((UINT1 *) pLladdr, ADDR6_LLOCAL);
            }
        }
    }
    if (u1Status == IP6_IF_DELETE)
    {
        /* get the MAC address on this interface */
        Ip6ifGetEthLladdr (pIf6, au1ethAddr, &u1EthAlen);

        if (!u1EthAlen)
        {
            IP6_TRC_ARG2 (pIf6->pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                          DATA_PATH_TRC | ALL_FAILURE_TRC, IP6_NAME,
                          "IP6IF: Ip6ifLlaStatus: status change, IP6 IF = %d Status = %d\n",
                          pIf6->u4Index, u1Status);
            IP6_TRC_ARG2 (pIf6->pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                          MGMT_TRC, IP6_NAME,
                          "IP6IF: Ip6ifLlaStatus: %s gen err:, Type = %d  ",
                          ERROR_FATAL_STR, ERR_GEN_INVALID_VAL);
            return;
        }

        Ip6DeleteMACAddrInMACFilter (pIf6->u4Index, au1ethAddr);

        /* RFC 4007- This function deletes the all the scope zone on this 
           interface */
        Ip6ZoneDeleteExistingZoneOnIf (pIf6->u4Index);
    }
}

/******************************************************************************
 * DESCRIPTION : This function is called from Interface managar module. It
 *               creates the same image of all the interfaces of Interface
 *               manager at IPV6 level.
 *
 * INPUTS      : u1Port, u1AdminStatus, u1OperStatus
 *
 * OUTPUTS     : None
 *
 * RETURNS     : IP6_SUCCESS/IP6_FAILURE
 *
 * NOTES       : 
 ******************************************************************************/
INT4
Ip6Lanif (UINT4 u4Port, UINT4 u4Speed, UINT4 u4HighSpeed, UINT1 u1Type,
          UINT1 u1AdminStatus, UINT1 u1OperStatus, tIp6Addr Ip6Addr,
          UINT1 u1PrefixLenth)
{
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    tIp6CfaParams      *pParams = NULL;
    UINT4               u4ContextId = 0;

    VcmGetContextIdFromCfaIfIndex (u4Port, &u4ContextId);
    if (gIp6GblInfo.i4Ip6Status == IP6_FAILURE)
    {
        return (IP6_FAILURE);
    }

    if (IP6_TASK_LOCK () == SNMP_FAILURE)
    {
        IP6_TRC_ARG (u4ContextId, IP6_MOD_TRC, CONTROL_PLANE_TRC, IP6_NAME,
                     "IP6IF: Ip6Lanif:IP6 Task Lock Failed !!!\n");
        return (IP6_FAILURE);
    }

    if (((u4Port <= 0) || (u4Port > (UINT4) IP6_MAX_LOGICAL_IF_INDEX))
        && (CfaIsL3SubIfIndex (u4Port) == CFA_FALSE))
    {
        IP6_TRC_ARG1 (u4ContextId, IP6_MOD_TRC, CONTROL_PLANE_TRC, IP6_NAME,
                      "IP6IF: Ip6Lanif:Invalid LAN IF index %d\n", u4Port);
        IP6_TASK_UNLOCK ();
        return (IP6_FAILURE);
    }

#ifndef TUNNEL_WANTED
    if (u1Type == IP6_TUNNEL_INTERFACE_TYPE)
    {
        /* Tunnel interface created at the lower layer. But Tunnel not
         * enabled at IP6 layer. */
        IP6_TASK_UNLOCK ();
        return IP6_SUCCESS;
    }
#endif /* TUNNEL_WANTED */

    switch (u1AdminStatus)
    {
        case IP6_LANIF_ENTRY_UP:
        case IP6_LANIF_ENTRY_VALID:

            /* Lower layer indicates the interface is administratively
             * activated and Operation status can be up or down.
             * Send notification message to IP6 protocol.
             */

            pBuf = Ip6BufAlloc (u4ContextId, sizeof (tIp6CfaParams),
                                0, IP6_MODULE);

            if (!(pBuf))
            {
                IP6_TRC_ARG (u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                             "IP6IF: Ip6Lanif:alloc of buffer for "
                             "indicating to IP6 failed\n");
                IP6_TASK_UNLOCK ();
                return IP6_FAILURE;
            }

            pParams = (tIp6CfaParams *) (VOID *) IP6_BUF_DATA_PTR (pBuf);
            pParams->u1MsgType = u1AdminStatus;
            pParams->u4Status = u1OperStatus;
            pParams->u4IfIndex = u4Port;
            pParams->u1IfType = u1Type;
            pParams->u2CktIndex = 0;
            pParams->u4Speed = u4Speed;
            pParams->u4HighSpeed = u4HighSpeed;
            pParams->Ip6Addr = Ip6Addr;
            pParams->u1PrefixLenth = u1PrefixLenth;

            if (OsixSendToQ (SELF, (const UINT1 *) IP6_TASK_CONTROL_QUEUE,
                             pBuf, OSIX_MSG_NORMAL) == OSIX_FAILURE)
            {
                if (Ip6BufRelease (u4ContextId, pBuf, FALSE, IP6_MODULE) ==
                    IP6_FAILURE)
                {
                    IP6_TRC_ARG4 (u4ContextId, IP6_MOD_TRC, BUFFER_TRC,
                                  IP6_NAME,
                                  "IP6IF: Ip6Lanif: Interface UP: "
                                  "reporting %s Buferr type %s,err%d,Buf "
                                  "Ptr= %p", ERROR_FATAL_STR, ERR_BUF_RELEASE,
                                  pBuf, IP6_IF_SUBMODULE);
                }
            }
            else
            {
                OsixSendEvent (SELF, (const UINT1 *) IP6_TASK_NAME,
                               IP6_CONTROL_EVENT);
            }
            break;

        case IP6_LANIF_ENTRY_DOWN:

            /* Lower layer indicates a interface is going down.
             * Send notification message to IP6 protocol.
             */
            pBuf = Ip6BufAlloc (u4ContextId, sizeof (tIp6CfaParams),
                                0, IP6_MODULE);

            if (!(pBuf))
            {
                IP6_TRC_ARG (u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                             "IP6IF: Ip6Lanif:alloc of buffer for "
                             "indicating to IP6 failed\n");
                IP6_TASK_UNLOCK ();
                return IP6_FAILURE;
            }

            pParams = (tIp6CfaParams *) (VOID *) IP6_BUF_DATA_PTR (pBuf);
            pParams->u1MsgType = IP6_LANIF_ENTRY_DOWN;
            pParams->u4Status = u1OperStatus;
            pParams->u4IfIndex = u4Port;
            pParams->u1IfType = u1Type;
            pParams->u2CktIndex = 0;

            if (OsixSendToQ (SELF, (const UINT1 *) IP6_TASK_CONTROL_QUEUE,
                             pBuf, OSIX_MSG_NORMAL) == OSIX_FAILURE)
            {
                if (Ip6BufRelease (u4ContextId, pBuf, FALSE, IP6_MODULE) ==
                    IP6_FAILURE)
                {
                    IP6_TRC_ARG4 (u4ContextId, IP6_MOD_TRC, BUFFER_TRC,
                                  IP6_NAME,
                                  "IP6IF: Ip6Lanif: Interface Down: "
                                  "reporting %s Buferr type %s,err%d,Buf "
                                  "Ptr= %p", ERROR_FATAL_STR, ERR_BUF_RELEASE,
                                  pBuf, IP6_IF_SUBMODULE);

                }
            }
            else
            {
                OsixSendEvent (SELF, (const UINT1 *) IP6_TASK_NAME,
                               IP6_CONTROL_EVENT);
            }
            break;

        case IP6_LANIF_ENTRY_INVALID:

            /* Lower layer indicates a interface is getting deleted.
             * Send notification message to IP6 protocol.
             */
            pBuf = Ip6BufAlloc (u4ContextId, sizeof (tIp6CfaParams),
                                0, IP6_MODULE);

            if (!(pBuf))
            {
                IP6_TRC_ARG (u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                             "IP6IF: Ip6Lanif:alloc of buffer for "
                             "indicating to IP6 failed\n");
                IP6_TASK_UNLOCK ();
                return IP6_FAILURE;
            }

            pParams = (tIp6CfaParams *) (VOID *) IP6_BUF_DATA_PTR (pBuf);
            pParams->u1MsgType = IP6_LANIF_ENTRY_INVALID;
            pParams->u4Status = u1OperStatus;
            pParams->u4IfIndex = u4Port;
            pParams->u1IfType = u1Type;
            pParams->u2CktIndex = 0;

            /* Remove the static configurations done on this interface */
            Ip6DeleteInterfaceInfo (u4Port);

            if (OsixSendToQ (SELF, (const UINT1 *) IP6_TASK_CONTROL_QUEUE,
                             pBuf, OSIX_MSG_NORMAL) == OSIX_FAILURE)
            {
                if (Ip6BufRelease (u4ContextId, pBuf, FALSE, IP6_MODULE) ==
                    IP6_FAILURE)
                {
                    IP6_TRC_ARG4 (u4ContextId, IP6_MOD_TRC, BUFFER_TRC,
                                  IP6_NAME,
                                  "IP6IF: Ip6Lanif: Interface Delete: "
                                  "reporting %s Buferr type %s,err%d,Buf "
                                  "Ptr= %p", ERROR_FATAL_STR, ERR_BUF_RELEASE,
                                  pBuf, IP6_IF_SUBMODULE);
                }

            }
            else
            {
                OsixSendEvent (SELF, (const UINT1 *) IP6_TASK_NAME,
                               IP6_CONTROL_EVENT);
            }
            break;

        default:
            /* Invalid Admin Status */
            IP6_TASK_UNLOCK ();
            return IP6_FAILURE;
    }

    IP6_TASK_UNLOCK ();
    return IP6_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : This function is called from Lower Layer to intimate any
 *               changes in mtu and Speed.
 *
 * INPUTS      : u1Port, u1AdminStatus, u1OperStatus
 *
 * OUTPUTS     : None
 *
 * RETURNS     : IP6_SUCCESS/IP6_FAILURE
 *               
 *
 * NOTES       : 
 ******************************************************************************/

INT4
Ip6LanifUpdate (UINT4 u4Port, UINT4 u4Mtu, UINT4 u4Speed, UINT4 u4HighSpeed)
{
    tIp6If             *pIf6 = NULL;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    tIp6CfaParams      *pParams = NULL;
    UINT4               u4ContextId;
    UINT1               u1CurOperStatus = 0;
    UINT1               u1NewOperStatus = 0;

    VcmGetContextIdFromCfaIfIndex (u4Port, &u4ContextId);

    if (gIp6GblInfo.i4Ip6Status == IP6_FAILURE)
    {
        IP6_TRC_ARG (u4ContextId, IP6_MOD_TRC, CONTROL_PLANE_TRC, IP6_NAME,
                     "IP6IF: Ip6LanifUpdate: IP6 Task Not Initialised !!!\n");
        return (IP6_FAILURE);
    }

    if (IP6_TASK_LOCK () == SNMP_FAILURE)
    {
        IP6_TRC_ARG (u4ContextId, IP6_MOD_TRC, CONTROL_PLANE_TRC, IP6_NAME,
                     "IP6IF: Ip6LanifUpdate: IP6 Task Lock Failed !!!\n");
        return (IP6_FAILURE);
    }

    if ((u4Port <= 0) || (u4Port > (UINT4) IP6_MAX_LOGICAL_IF_INDEX))
    {
        IP6_TRC_ARG1 (u4ContextId, IP6_MOD_TRC, CONTROL_PLANE_TRC, IP6_NAME,
                      "IP6IF: Ip6LanifUpdate: Invalid IF index %d\n", u4Port);
        IP6_TASK_UNLOCK ();
        return (IP6_FAILURE);
    }

    /* update ip6 if mtu */
    pIf6 = Ip6ifGetEntry (u4Port);
    if (pIf6 == NULL)
    {
        /* No matching interface exists in IP6 */
        IP6_TASK_UNLOCK ();
        return IP6_FAILURE;
    }

    if (u4Mtu != pIf6->u4Mtu)
    {
        /* Get the Interface Current Operational Status. */
        u1CurOperStatus = Ip6ifGetIfOper (pIf6->u1IfType, pIf6->u4Index,
                                          pIf6->u2CktIndex);
#ifdef TUNNEL_WANTED
        if ((pIf6->u1IfType == IP6_TUNNEL_INTERFACE_TYPE) &&
            (u4Mtu < IP6_MIN_MTU))
        {
            pIf6->u4Mtu = IP6_MIN_MTU;
        }
        else
#endif
            pIf6->u4Mtu = u4Mtu;

        NetIpv6InvokeInterfaceStatusChange (pIf6->u4Index,
                                            NETIPV6_ALL_PROTO,
                                            pIf6->u4Mtu,
                                            NETIPV6_INTERFACE_MTU_CHANGE);
        /* Change in MTU can affect the interfaces operational Status.
         * Get the Interface New Operational Status. */
        u1NewOperStatus = Ip6ifGetIfOper (pIf6->u1IfType, pIf6->u4Index,
                                          pIf6->u2CktIndex);

        if (u1CurOperStatus != u1NewOperStatus)
        {
            /* Change in the interface operational Status. */
            pBuf = Ip6BufAlloc (u4ContextId, sizeof (tIp6CfaParams),
                                0, IP6_MODULE);

            if (!(pBuf))
            {
                IP6_TRC_ARG (u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                             "IP6IF: Ip6Lanif:alloc of buffer for "
                             "indicating to IP6 failed\n");
                IP6_TASK_UNLOCK ();
                return IP6_FAILURE;
            }

            pParams = (tIp6CfaParams *) (VOID *) IP6_BUF_DATA_PTR (pBuf);
            pParams->u1MsgType = IP6_LANIF_MTU_CHANGE;
            if (u1NewOperStatus == OPER_UP)
            {
                /* Change in MTU has made interface operationally
                 * Active. */
                pParams->u4Status = IP6_LANIF_OPER_UP;
            }
            else
            {
                /* Change in MTU has made interface operationally
                 * InActive. */
                pParams->u4Status = IP6_LANIF_OPER_DOWN;
            }
            pParams->u4IfIndex = pIf6->u4Index;
            pParams->u1IfType = pIf6->u1IfType;
            pParams->u2CktIndex = pIf6->u2CktIndex;

            if (OsixSendToQ (SELF, (const UINT1 *) IP6_TASK_CONTROL_QUEUE,
                             pBuf, OSIX_MSG_NORMAL) == OSIX_FAILURE)
            {
                if (Ip6BufRelease (u4ContextId, pBuf, FALSE, IP6_MODULE) ==
                    IP6_FAILURE)
                {
                    IP6_TRC_ARG4 (u4ContextId, IP6_MOD_TRC,
                                  BUFFER_TRC, IP6_NAME,
                                  "IP6IF: Ip6Lanif: Interface Delete: "
                                  "reporting %s Buferr type %s,err%d,Buf "
                                  "Ptr= %p",
                                  ERROR_FATAL_STR, ERR_BUF_RELEASE, pBuf,
                                  IP6_IF_SUBMODULE);
                }
            }
            else
            {
                OsixSendEvent (SELF, (const UINT1 *) IP6_TASK_NAME,
                               IP6_CONTROL_EVENT);
            }
        }
    }

    if ((u4Speed != pIf6->u4IfSpeed) && (u4HighSpeed == 0))
    {
        /* Indicates change in Speed Value. */
        pIf6->u4IfSpeed = u4Speed;
        NetIpv6InformSpeedChange (u4Port, u4Speed);
    }

    if ((u4HighSpeed != pIf6->u4IfHighSpeed) && (u4Speed == IP6_MAX_VALUE))
    {
        /* Indicates change in High Speed Value. */
        pIf6->u4IfHighSpeed = u4HighSpeed;
        NetIpv6InformHighSpeedChange (u4Port, u4HighSpeed);
    }
    IP6_TASK_UNLOCK ();
    return IP6_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : This routine returns the Pointer to the corresponding Iface 
 *               Entry if an IfIndex is passed.
 *
 * INPUTS      : The index of the interface (*pu4Index)
 *
 * OUTPUTS     : None. 
 *
 * RETURNS     : Pointer to the IP6 Interface.
 ******************************************************************************/
tIp6If             *
Ip6GetIfFromIndex (UINT4 u4Index)
{
    if (Ip6ifEntryExists (u4Index) == IP6_SUCCESS)
    {
        return gIp6GblInfo.apIp6If[u4Index];
    }

    return NULL;
}

/******************************************************************************
 * DESCRIPTION : This routine is called to delete an IPv6 interface in the 
 *               higher layer and clear the IPv6 address configurations in the 
 *               IPv6 module. The routine is called upon receiving an interface
 *               map or interface unmap event from the vcm module.
 *               The routine will inform ND6 Module about the deletion of the
 *               interface, post an event to RIP6 to notify the interface
 *               deletion and take action for all addresses on the interface.
 *
 * INPUTS      : pIf6 - Pointer to the interface that is unmapped.
 *
 * OUTPUTS     : None
 *
 * RETURNS     : IP6_SUCCESS/IP6_FAILURE
 ******************************************************************************/
INT1
Ip6UnmapIf (tIp6If * pIf6)
{
    tTMO_SLL_NODE      *pNext = NULL;
    tTMO_SLL_NODE      *pCur = NULL;
    tIp6AddrInfo       *pAddr = NULL;
    tIp6McastInfo      *pMcastInfo = NULL;
    UINT4               u4Index = 0;

    u4Index = pIf6->u4Index;

    IP6_TRC_ARG2 (pIf6->pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                  DATA_PATH_TRC, IP6_NAME,
                  "Ip6UnmapIf: Unmapping IP6 IF %d from context\n",
                  u4Index, pIf6->pIp6Cxt->u4ContextId);

    /*  delete all unicast address on the interface */
    pNext = TMO_SLL_First (&pIf6->addr6Ilist);
    while ((pCur = pNext) != NULL)
    {
        pNext = TMO_SLL_Next (&pIf6->addr6Ilist, pCur);

        pAddr = IP6_ADDR_PTR_FROM_SLL (pCur);

        TMO_SLL_Delete (&pIf6->addr6Ilist, pCur);

        /* 
         * When the address is already down, the routes would have been
         * deleted. So deleting the routes only when address is in 
         * ADDR6_UP state. 
         */
        if (pAddr->u1Status & ADDR6_UP)
        {
            /* Inform all the higher layer about the change in Address Status */
            NetIpv6InvokeAddressChange (&(pAddr->ip6Addr),
                                        (pAddr->u1PrefLen),
                                        (UINT4) pAddr->u1AddrType,
                                        (u4Index), (NETIPV6_ADDRESS_DELETE));

            if (pAddr->u1ConfigMethod != IP6_ADDR_AUTO_SL)
            {
                Ip6DelLocalRoute (&pAddr->ip6Addr, pAddr->u1PrefLen,
                                  pIf6->u4Index);
            }
        }
        Ip6AddrNotify (pAddr, IP6_IF_DELETE);
    }

    /* 
     * delete all multicast address on the interface 
     */
    pNext = TMO_SLL_First (&pIf6->mcastIlist);
    while ((pCur = pNext) != NULL)
    {
        pNext = TMO_SLL_Next (&pIf6->mcastIlist, pCur);

        pMcastInfo = (tIp6McastInfo *) pCur;
        /* Inform all the higher layer about the change in Address Status */
        NetIpv6InvokeAddressChange (&(pMcastInfo->ip6Addr), 128, ADDR6_MULTI,
                                    u4Index, NETIPV6_ADDRESS_DELETE);
        TMO_SLL_Delete (&pIf6->mcastIlist, pCur);

        Ip6RelMem (pIf6->pIp6Cxt->u4ContextId, (UINT2) gIp6GblInfo.i4Ip6mcastId,
                   (UINT1 *) pCur);
    }

    /* Delete the Link-Local Address over this interface. */
    Ip6ifDeInitialize (pIf6);

    /* Delete All the entry from the MAC Filter Table. */
    Ip6DeleteMACAddrFromMACFilter (u4Index);

    ND6DeleteRoutes (u4Index);

    IP6_TASK_UNLOCK ();
    Rtm6ApiActOnIpv6IfChange (RTM6_SCAN_INTF_DEL, pIf6->u4Index);
    IP6_TASK_LOCK ();

    /* Notify ND that this interface is deleted */
    Nd6ActOnIfstatus (pIf6, IP6_IF_UNMAP);

    NetIpv6InvokeInterfaceStatusChange (u4Index,
                                        NETIPV6_ALL_PROTO,
                                        NETIPV6_IF_DELETE,
                                        NETIPV6_INTERFACE_STATUS_CHANGE);

    Ip6ZoneDeleteExistingZoneOnIf (pIf6->u4Index);

    return (IP6_SUCCESS);
}

/*****************************************************************************
 * DESCRIPTION : This routine is called to map a context with an interface in
 *               IPv6 module. The routine is called upon receiving an interface
 *               map or interface unmap event from the vcm module.
 *
 * INPUTS      : pIf6 - Pointer to the interface that is mapped
 *               pIp6Cxt - Pointer to the newly created context to which
 *                         the interface has to be mapped
 *
 * OUTPUTS     : None
 *
 * RETURNS     : IP6_SUCCESS/IP6_FAILURE
 ***************************************************************************/

INT1
Ip6MapIf (tIp6If * pIf6, tIp6Cxt * pIp6Cxt)
{
    tIp6Addr            GrpAddr;

    MEMSET (&GrpAddr, 0, sizeof (tIp6Addr));
    /* Ensure that ALL NODE Multi-Cast address is enabled for this
       interface. */
    pIf6->pIp6Cxt = pIp6Cxt;

    SET_ALL_NODES_MULTI (GrpAddr);
    Ip6AddrCreateMcast (pIf6->u4Index, &GrpAddr);

    if (pIf6->u1Ipv6IfFwdOperStatus == IP6_IF_FORW_ENABLE)
    {
        /* Routing is enabled. Ensure that ALL ROUTER Multi-Cast address
           is enabled for this interface. */
        SET_ALL_ROUTERS_MULTI (GrpAddr);
        Ip6AddrCreateMcast (pIf6->u4Index, &GrpAddr);
    }

    /* Notify all the LL addresses as up */
    Ip6ifLlaStatus (pIf6, IP6_IF_UP);

    OsixGetSysTime (&gIp6GblInfo.u4Ip6IfTableLastChange);

    return (IP6_SUCCESS);

}

/***************************** END OF FILE **********************************/
