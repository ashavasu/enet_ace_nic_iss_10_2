/*****************************************************************************/
/*    FILE  NAME            : ip6api.c                                       */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    MODULE NAME           : IPv6                                           */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DESCRIPTION           : This file contains the exported APIs of        */
/*                            the IPv6 task                                  */
/*---------------------------------------------------------------------------*/

#include "ip6inc.h"

/****************************************************************************
 * Function     : Ip6NotifyContextStatus
 *
 * Description  : This function is invoked by the VCM module to notify the
 *                 context creation / context deletion events to the IP6 task.
 *
 * Input        : u4IfIndex   - The interface Index
 *                u4ContextId - The virtual router Id
 *                u4Event     - Context create / delete event.
 *
 * Output       : None
 *
 * Returns      : None
 *
 ***************************************************************************/
VOID
Ip6NotifyContextStatus (UINT4 u4IfIndex, UINT4 u4ContextId, UINT1 u1Event)
{
    tCRU_BUF_CHAIN_HEADER *pBuf;
    tIp6VcmParams      *pParams;

    UNUSED_PARAM (u4IfIndex);

    if (gIp6GblInfo.i4Ip6Status == IP6_FAILURE)
    {
        IP6_GBL_TRC_ARG (IP6_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                         IP6_NAME,
                         "Ip6NotifyContextStatus: IP6 Task Not Initialised !!!\n");
        return;
    }

    if (u4ContextId > IP6_MAX_INSTANCES)
    {
        IP6_GBL_TRC_ARG1 (IP6_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          IP6_NAME,
                          "Ip6NotifyContextStatus: Invalid context Id %d\n",
                          u4ContextId);
        return;
    }

    pBuf = Ip6BufAlloc (u4ContextId, sizeof (tIp6VcmParams), 0, IP6_MODULE);

    if (pBuf == NULL)
    {
        IP6_TRC_ARG (u4ContextId, IP6_MOD_TRC,
                     CONTROL_PLANE_TRC | ALL_FAILURE_TRC, IP6_NAME,
                     "Ip6NotifyContextStatus: Unable to allocate buffer\n");
        return;
    }

    if ((pParams =
         (tIp6VcmParams *) (VOID *) Ip6GetMem (u4ContextId,
                                               (UINT2) gIp6GblInfo.
                                               i4ParamId)) == NULL)
    {
        IP6_TRC_ARG (u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                     "Ip6NotifyContextStatus: Unable to allocate memory"
                     " for tIp6VcmParams");
        IP6_TRC_ARG4 (u4ContextId, IP6_MOD_TRC, MGMT_TRC, IP6_NAME,
                      "Ip6NotifyContextStatus: %s mem err: Type = %d "
                      "PoolId = %d Memptr = %p\n",
                      ERROR_FATAL_STR, ERR_MEM_GET, NULL,
                      gIp6GblInfo.i4ParamId);

        if (Ip6BufRelease (u4ContextId, pBuf, FALSE, IP6_MODULE) == IP6_FAILURE)
        {
            IP6_TRC_ARG1 (u4ContextId, IP6_MOD_TRC, BUFFER_TRC, IP6_NAME,
                          "Ip6NotifyContextStatus: Failure in releasing buffer"
                          "ptr = %p \n", pBuf);
        }
        return;

    }

    if (u1Event == VCM_CONTEXT_CREATE)
    {
        IP6_TASK_LOCK ();
        Ip6HandleCreateContext (u4ContextId);
        IP6_TASK_UNLOCK ();
        Ip6RelMem (u4ContextId, (UINT2) gIp6GblInfo.i4ParamId,
                   (UINT1 *) pParams);
        Ip6BufRelease (u4ContextId, pBuf, FALSE, IP6_MODULE);
        return;
    }
    else
    {
        IP6_SET_COMMAND (pBuf, IP6_DELETE_CONTEXT);
    }
    pParams->u4ContextId = u4ContextId;

    Ip6SetParams ((tIp6Params *) (VOID *) pParams, pBuf);

    /* Remove the static configurations */
    IP6_TASK_LOCK ();
    Ip6DeleteContextInfo (u4ContextId);
    IP6_TASK_UNLOCK ();

    if (OsixSendToQ (SELF, (const UINT1 *) IP6_TASK_CONTROL_QUEUE,
                     pBuf, OSIX_MSG_NORMAL) == OSIX_FAILURE)
    {
        Ip6RelMem (u4ContextId, (UINT2) gIp6GblInfo.i4ParamId,
                   (UINT1 *) pParams);
        if (Ip6BufRelease (u4ContextId, pBuf, FALSE, IP6_MODULE) == IP6_FAILURE)
        {
            IP6_TRC_ARG4 (u4ContextId, IP6_MOD_TRC, BUFFER_TRC, IP6_NAME,
                          "Ip6NotifyContextStatus: "
                          "reporting %s Buferr type %s,err%d,Buf "
                          "Ptr= %p",
                          ERROR_FATAL_STR, ERR_BUF_RELEASE, pBuf,
                          IP6_CORE_SUBMODULE);
            return;
        }
    }
    if (OsixSendEvent (SELF, (const UINT1 *) IP6_TASK_NAME,
                       IP6_CONTROL_EVENT) == OSIX_FAILURE)
    {
        Ip6RelMem (u4ContextId, (UINT2) gIp6GblInfo.i4ParamId,
                   (UINT1 *) pParams);
        if (Ip6BufRelease (u4ContextId, pBuf, FALSE, IP6_MODULE) == IP6_FAILURE)
        {
            IP6_TRC_ARG4 (u4ContextId, IP6_MOD_TRC, BUFFER_TRC, IP6_NAME,
                          "Ip6NotifyContextStatus: "
                          "reporting %s Buferr type %s,err%d,Buf "
                          "Ptr= %p",
                          ERROR_FATAL_STR, ERR_BUF_RELEASE, pBuf,
                          IP6_CORE_SUBMODULE);
            return;
        }

    }

    return;
}

/****************************************************************************
 * Function     : Ip6IfaceMapping       
 *
 * Description  : This function is invoked by the VCM module to notify the
 *                 mapping and unmapping of the interface from the existing   
 *                 virtual routers.
 *
 * Input        : u4ContextId - The virtual router Id
 *                u4IfIndex   - The interface index.
 *                u4Event     - Context create / delete event.
 *
 * Output       : None
 *
 * Returns      : IP6_SUCCESS/IP6_FAILURE
 *
 ***************************************************************************/
INT1
Ip6IfaceMapping (UINT4 u4ContextId, UINT4 u4IfIndex, UINT1 u1Event)
{
    tCRU_BUF_CHAIN_HEADER *pBuf;
    tIp6VcmParams      *pParams;

    if (gIp6GblInfo.i4Ip6Status == IP6_FAILURE)
    {
        IP6_GBL_TRC_ARG (IP6_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                         IP6_NAME,
                         "Ip6IfaceMapping: IP6 Task Not Initialised !!!\n");
        return (IP6_FAILURE);
    }

    if (u4ContextId > IP6_MAX_INSTANCES)
    {
        IP6_GBL_TRC_ARG1 (IP6_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          IP6_NAME,
                          "Ip6IfaceMapping: Invalid context Id %d\n",
                          u4ContextId);
        return (IP6_FAILURE);
    }

    if ((u4IfIndex <= 0) || (u4IfIndex > (UINT4) IP6_MAX_LOGICAL_IF_INDEX))
    {
        IP6_TRC_ARG1 (u4ContextId, IP6_MOD_TRC, CONTROL_PLANE_TRC, IP6_NAME,
                      "Ip6IfaceMapping:Invalid IF index %d\n", u4IfIndex);
        return (IP6_FAILURE);
    }

    pBuf = Ip6BufAlloc (u4ContextId, sizeof (tIp6VcmParams), 0, IP6_MODULE);

    if (pBuf == NULL)
    {
        IP6_TRC_ARG (u4ContextId, IP6_MOD_TRC,
                     CONTROL_PLANE_TRC | ALL_FAILURE_TRC, IP6_NAME,
                     "Ip6NotifyContextStatus: Unable to allocate buffer\n");
        return (IP6_FAILURE);
    }

    if ((pParams =
         (tIp6VcmParams *) (VOID *) Ip6GetMem (u4ContextId,
                                               (UINT2) gIp6GblInfo.
                                               i4ParamId)) == NULL)
    {
        IP6_TRC_ARG (u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                     "Ip6NotifyContextStatus: Unable to allocate memory"
                     " for tIp6VcmParams");
        IP6_TRC_ARG4 (u4ContextId, IP6_MOD_TRC, MGMT_TRC, IP6_NAME,
                      "Ip6NotifyContextStatus: %s mem err: Type = %d "
                      "PoolId = %d Memptr = %p\n",
                      ERROR_FATAL_STR, ERR_MEM_GET, NULL,
                      gIp6GblInfo.i4ParamId);

        if (Ip6BufRelease (u4ContextId, pBuf, FALSE, IP6_MODULE) == IP6_FAILURE)
        {
            IP6_TRC_ARG1 (u4ContextId, IP6_MOD_TRC, BUFFER_TRC, IP6_NAME,
                          "Ip6NotifyContextStatus: Failure in releasing buffer"
                          "ptr = %p \n", pBuf);
        }
        return IP6_FAILURE;

    }

    Ip6SetParams ((tIp6Params *) (VOID *) pParams, pBuf);

    if (u1Event == VCM_IF_MAP_CHG_REQ)
    {
        IP6_SET_COMMAND (pBuf, IP6_MAP_INTERFACE);
    }
    else
    {
        IP6_SET_COMMAND (pBuf, IP6_UNMAP_INTERFACE);
    }
    pParams->u4ContextId = u4ContextId;
    pParams->u4IfIndex = u4IfIndex;

    IP6_TASK_LOCK ();
    Ip6DeleteInterfaceInfo (u4IfIndex);
    IP6_TASK_UNLOCK ();

    if (OsixSendToQ (SELF, (const UINT1 *) IP6_TASK_CONTROL_QUEUE,
                     pBuf, OSIX_MSG_NORMAL) == OSIX_FAILURE)
    {
        if (Ip6BufRelease (u4ContextId, pBuf, FALSE, IP6_MODULE) == IP6_FAILURE)
        {
            IP6_TRC_ARG4 (u4ContextId, IP6_MOD_TRC, BUFFER_TRC, IP6_NAME,
                          "Ip6NotifyContextStatus: "
                          "reporting %s Buferr type %s,err%d,Buf "
                          "Ptr= %p",
                          ERROR_FATAL_STR, ERR_BUF_RELEASE, pBuf,
                          IP6_CORE_SUBMODULE);
        }
    }
    OsixSendEvent (SELF, (const UINT1 *) IP6_TASK_NAME, IP6_CONTROL_EVENT);

    return IP6_SUCCESS;
}
