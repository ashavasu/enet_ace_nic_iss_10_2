#ifndef _UDP6_H
#define _UDP6_H

/*
 *----------------------------------------------------------------------------- 
 *    FILE NAME                    :    udp6.h                                 
 *                                                                              
 *    PRINCIPAL AUTHOR             :    Anshul Garg                             
 *                                                                              
 *    SUBSYSTEM NAME               :    IPv6                                    
 *                                                                              
 *    MODULE NAME                  :    UDP6 Module                            
 *                                                                              
 *    LANGUAGE                     :    C                                       
 *                                                                              
 *    TARGET ENVIRONMENT           :    UNIX                                    
 *                                                                              
 *    DATE OF FIRST RELEASE        :    Dec-02-1996                             
 *                                                                              
 *    DESCRIPTION                  :    This file contains typedefs and
 *                                      macro definitions for handling 
 *                                      user datagram packets.      
 *                                                                              
 *----------------------------------------------------------------------------- 
 */

/*
 * UDP header
 */

typedef struct _UDP_HEADER
{
    UINT2  u2SrcPort;  /* Port number from which application 
                          * data is being sent */
    UINT2  u2DstPort;  /* Port number at destination on which 
                          * application data is to be sent */
    UINT2  u2Len;       /* Length of UDP header+ data */
    UINT2  u2Chksum;    /* Checksum of datagram prepended with 
                          * pseudo header */
}
tUdp6Hdr;


/* Constants */

#define  UDP6_HDR_LEN  sizeof (tUdp6Hdr) 
#define  PVT_UDP_PORT_START     49152 
#define  PVT_UDP_PORT_END       65534
#define  UDP6_MAX_TASK_Q_NAME_LEN                      4

#endif /* _UDP6_H */
