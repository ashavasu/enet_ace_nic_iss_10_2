/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: netipv6.c,v 1.48 2018/02/14 12:33:52 siva Exp $
 *
 * Description: Standardised Ipv6 Interface with HL protocols
 *
 *******************************************************************/

#include "ip6inc.h"

tNetIpv6RegTbl      gaNetIpv6RegTable[IP6_MAX_PROTOCOLS];
tNetIpv6MCastProtoRegTbl gaNetIpv6MCastRegTable[IP6_MAX_MCAST_PROTOCOLS];

PRIVATE INT4        Ipv6GetIPPort (UINT4 u4IfIndex, UINT4 *pu4IpPort);
UINT1               gau1SrcAddr[IP6_ADDR_SIZE];
/*
 ******************************************************************************
 * Function Name    :   Ipv6GetIPPort
 * Description      :   This function provides the IP port number 
 *                      for the given interface index
 * Inputs           :   u4IfIndex - Interface index
 * Outputs          :   pu4IpPort- IP port for the given
 *                      interface index
 * Return Value     :   NETIPV6_SUCCESS or NETIPV6_FAILURE
 *******************************************************************************
 */

INT4
Ipv6GetIPPort (UINT4 u4IfIndex, UINT4 *pu4IpPort)
{
    tIp6If             *pIf6 = NULL;

    pIf6 = Ip6ifGetEntry (u4IfIndex);
    if (pIf6 == NULL)
    {
        return (NETIPV6_FAILURE);
    }
    else
    {
        *pu4IpPort = pIf6->u4IpPort;
        return (NETIPV6_SUCCESS);
    }
}

/*
 ******************************************************************************
 * Function Name    :   Ipv6GetIfInfo
 * Description      :   This function provides the interface related
 *                      information for the given interface index
 * Inputs           :   u4IfIndex - Interface index
 * Outputs          :   pNetIpv6IfInfo - Interface information for the given
 *                      interface index
 * Return Value     :   NETIPV6_SUCCESS or NETIPV6_FAILURE
 *******************************************************************************
 */

INT4
Ipv6GetIfInfo (UINT4 u4IfIndex, tNetIpv6IfInfo * pNetIpv6IfInfo)
{
    tTMO_SLL_NODE      *pSllInfo = NULL;
    tIp6LlocalInfo     *pLladdr = NULL;
    tIp6If             *pIf6 = NULL;
    UINT4               u4ContextId = IP6_ZERO;

    VcmGetContextIdFromCfaIfIndex (u4IfIndex, &u4ContextId);
    if (Ip6GetIfName (u4IfIndex, pNetIpv6IfInfo->au1IfName) == IP6_FAILURE)
    {
        IP6_TRC_ARG (u4ContextId, NETIP6_MOD_TRC, DATA_PATH_TRC, NETIP6_NAME,
                     "Ipv6GetIfInfo : Invalid Interface Index !!!\n");
        return (NETIPV6_FAILURE);
    }

    pIf6 = Ip6ifGetEntry (u4IfIndex);
    if (pIf6 == NULL)
    {
        IP6_TRC_ARG (u4ContextId, NETIP6_MOD_TRC, DATA_PATH_TRC, NETIP6_NAME,
                     "Ipv6GetIfInfo : Invalid Interface Index !!!\n");
        return (NETIPV6_FAILURE);
    }

    MEMSET (&(pNetIpv6IfInfo->Ip6Addr), IP6_ZERO, sizeof (tIp6Addr));

    TMO_SLL_Scan (&pIf6->lla6Ilist, pSllInfo, tTMO_SLL_NODE *)
    {
        pLladdr = IP6_LLADDR_PTR_FROM_SLL (pSllInfo);

        if (pLladdr->u1ConfigMethod == IP6_ADDR_VIRTUAL)
        {
            continue;
        }

        MEMCPY (&(pNetIpv6IfInfo->Ip6Addr), &(pLladdr->ip6Addr),
                sizeof (tIp6Addr));
    }

    pNetIpv6IfInfo->u4IfIndex = u4IfIndex;
    pNetIpv6IfInfo->u4IfSpeed = pIf6->u4IfSpeed;
    pNetIpv6IfInfo->u4IfHighSpeed = pIf6->u4IfHighSpeed;
    pNetIpv6IfInfo->u4Admin = pIf6->u1AdminStatus;
    pNetIpv6IfInfo->u4Oper = pIf6->u1OperStatus;
    pNetIpv6IfInfo->u4InterfaceType = pIf6->u1IfType;
    pNetIpv6IfInfo->u4Mtu = pIf6->u4Mtu;
    pNetIpv6IfInfo->u4IpPort = pIf6->u4IpPort;
    pNetIpv6IfInfo->u4AddressLessIf = pIf6->u4UnnumAssocIPv6If;

    return (NETIPV6_SUCCESS);
}

/*
 ******************************************************************************
 * Function Name    :   NetIpv6GetIfInfo
 * Description      :   This function provides the interface related
 *                      information for the given interface index
 * Inputs           :   u4IfIndex - Interface index
 * Outputs          :   pNetIpv6IfInfo - Interface information for the given
 *                      interface index
 * Return Value     :   NETIPV6_SUCCESS or NETIPV6_FAILURE
 *******************************************************************************
 */

INT4
NetIpv6GetIfInfo (UINT4 u4IfIndex, tNetIpv6IfInfo * pNetIpv6IfInfo)
{
    INT4                i4Status;
    UINT4               u4ContextId = 0;

    VcmGetContextIdFromCfaIfIndex (u4IfIndex, &u4ContextId);
    if (gIp6GblInfo.i4Ip6Status == IP6_FAILURE)
    {
        IP6_TRC_ARG (u4ContextId, NETIP6_MOD_TRC, DATA_PATH_TRC, NETIP6_NAME,
                     "NetIpv6GetIfInfo : IP6 Task Not Initialised !!!\n");
        return (NETIPV6_FAILURE);
    }

    if (IP6_TASK_LOCK () == SNMP_FAILURE)
    {
        IP6_TRC_ARG (u4ContextId, NETIP6_MOD_TRC, DATA_PATH_TRC, NETIP6_NAME,
                     "NetIpv6GetIfInfo : IP6 Task Lock Failed !!!\n");
        return (NETIPV6_FAILURE);
    }

    if (Ip6ifEntryExists (u4IfIndex) != IP6_SUCCESS)
    {
        IP6_TASK_UNLOCK ();
        IP6_TRC_ARG (u4ContextId, NETIP6_MOD_TRC, DATA_PATH_TRC, NETIP6_NAME,
                     "NetIpv6GetIfInfo : Invalid Interface Index !!!\n");
        return (NETIPV6_FAILURE);
    }

    i4Status = Ipv6GetIfInfo (u4IfIndex, pNetIpv6IfInfo);
    IP6_TASK_UNLOCK ();

    return i4Status;
}

/*
 ******************************************************************************
 * Function Name    :   NetIpv6GetFirstIfInfo
 * Description      :   This function provides the first entry in the interface
 *                      table
 * Inputs           :   None
 * Outputs          :   pNetIpv6IfInfo - First interface information
 * Return Value     :   NETIPV6_SUCCESS | NETIPV6_FAILURE
 *******************************************************************************
 */

INT4
NetIpv6GetFirstIfInfo (tNetIpv6IfInfo * pNetIpv6IfInfo)
{
    UINT4               u4IfIndex = 1;    /* IfIndex Always Start with 1 */
    INT4                i4Status;

    if (gIp6GblInfo.i4Ip6Status == IP6_FAILURE)
    {
        IP6_GBL_TRC_ARG (NETIP6_MOD_TRC, DATA_PATH_TRC, NETIP6_NAME,
                         "NetIpv6GetFirstIfInfo : IP6 Task Not Initialised !!!\n");
        return (NETIPV6_FAILURE);
    }

    if (IP6_TASK_LOCK () == SNMP_FAILURE)
    {
        IP6_GBL_TRC_ARG (NETIP6_MOD_TRC, DATA_PATH_TRC, NETIP6_NAME,
                         "NetIpv6GetFirstIfInfo : IP6 Task Lock Failed !!!\n");
        return (NETIPV6_FAILURE);
    }

    if (Ip6ifEntryExists (u4IfIndex) != IP6_SUCCESS)
    {
        if (Ip6ifGetNextIndex (&u4IfIndex) != IP6_SUCCESS)
        {
            IP6_TASK_UNLOCK ();
            IP6_GBL_TRC_ARG (NETIP6_MOD_TRC, DATA_PATH_TRC, NETIP6_NAME,
                             "NetIpv6GetFirstIfInfo : No Interface Index Exist !!!\n");
            return (NETIPV6_FAILURE);
        }
    }

    i4Status = Ipv6GetIfInfo (u4IfIndex, pNetIpv6IfInfo);

    IP6_TASK_UNLOCK ();
    return i4Status;
}

/*
 ******************************************************************************
 * Function Name    :   NetIpv6GetNextIfInfo
 * Description      :   This function returns the next interface information
 *                      of the input interface index
 * Inputs           :   u4IfIndex - Interface index
 * Outputs          :   pNextNetIpv6IfInfo - Interface information of the
 *                      next interface index
 * Return Value     :   NETIPV6_SUCCESS | NETIPV6_FAILURE
 *******************************************************************************
 */

INT4
NetIpv6GetNextIfInfo (UINT4 u4IfIndex, tNetIpv6IfInfo * pNextNetIpv6IfInfo)
{
    UINT4               u4NextIndex = u4IfIndex;
    INT4                i4Status;

    if (gIp6GblInfo.i4Ip6Status == IP6_FAILURE)
    {
        IP6_GBL_TRC_ARG (NETIP6_MOD_TRC, DATA_PATH_TRC, NETIP6_NAME,
                         "NetIpv6GetNextIfInfo : IP6 Task Not Initialised !!!\n");
        return (NETIPV6_FAILURE);
    }

    if (IP6_TASK_LOCK () == SNMP_FAILURE)
    {
        IP6_GBL_TRC_ARG (NETIP6_MOD_TRC, DATA_PATH_TRC, NETIP6_NAME,
                         "NetIpv6GetNextIfInfo : IP6 Task Lock Failed !!!\n");
        return (NETIPV6_FAILURE);
    }

    if (Ip6ifGetNextIndex (&u4NextIndex) != IP6_SUCCESS)
    {
        IP6_TASK_UNLOCK ();
        IP6_GBL_TRC_ARG (NETIP6_MOD_TRC, DATA_PATH_TRC, NETIP6_NAME,
                         "NetIpv6GetNextIfInfo : No Next Interface Index Exist !!!\n");
        return (NETIPV6_FAILURE);
    }

    i4Status = Ipv6GetIfInfo (u4NextIndex, pNextNetIpv6IfInfo);

    IP6_TASK_UNLOCK ();
    return i4Status;
}

/*
 ******************************************************************************
 * Function Name    :   NetIpv6InformSpeedChange
 * Description      :   This function is used by the lower layer to inform IPv6
 *                      about the change in the speed of the link
 * Inputs           :   u4IfIndex - Interface index number
 *                  :   u4IfSpeed - Speed of the interface
 * Outputs          :   None
 * Return Value     :   None
 *******************************************************************************
 */

VOID
NetIpv6InformSpeedChange (UINT4 u4IfIndex, UINT4 u4IfSpeed)
{
    NetIpv6InvokeInterfaceStatusChange (u4IfIndex,
                                        NETIPV6_ALL_PROTO,
                                        u4IfSpeed,
                                        NETIPV6_INTERFACE_SPEED_CHANGE);
    return;
}

/*
 ******************************************************************************
 * Function Name    :   NetIpv6InformHighSpeedChange
 * Description      :   This function is used by the lower layer to inform IPv6
 *                      about the change in the speed of the link
 * Inputs           :   u4IfIndex - Interface index number
 *                  :   u4HighIfSpeed - Speed of the interface
 * Outputs          :   None
 * Return Value     :   None
 *******************************************************************************
 */

VOID
NetIpv6InformHighSpeedChange (UINT4 u4IfIndex, UINT4 u4IfHighSpeed)
{
    NetIpv6InvokeInterfaceStatusChange (u4IfIndex,
                                        NETIPV6_ALL_PROTO,
                                        u4IfHighSpeed,
                                        NETIPV6_INTERFACE_HIGH_SPEED_CHANGE);
    return;
}

/*
 ******************************************************************************
 * Function Name    :   NetIpv6GetFirstIfAddr
 * Description      :   This function provides the first address of the
 *                      interface
 * Inputs           :   u4IfIndex - Interface index
 * Outputs          :   pNetIpv6AddrInfo - First address information
 * Return Value     :   NETIPV6_SUCCESS | NETIPV6_FAILURE
 *******************************************************************************
 */

INT4
NetIpv6GetFirstIfAddr (UINT4 u4IfIndex, tNetIpv6AddrInfo * pNetIpv6AddrInfo)
{
    tTMO_SLL_NODE      *pNode = NULL;
    tIp6AddrInfo       *pAddr = NULL;
    UINT4               u4ContextId = IP6_ZERO;

    VcmGetContextIdFromCfaIfIndex (u4IfIndex, &u4ContextId);
    if (gIp6GblInfo.i4Ip6Status == IP6_FAILURE)
    {
        IP6_TRC_ARG (u4ContextId, NETIP6_MOD_TRC, DATA_PATH_TRC, NETIP6_NAME,
                     "NetIpv6GetFirstIfAddr : IP6 Task Not Initialised !!!\n");
        return (NETIPV6_FAILURE);
    }

    if (IP6_TASK_LOCK () == SNMP_FAILURE)
    {
        IP6_TRC_ARG (u4ContextId, NETIP6_MOD_TRC, DATA_PATH_TRC, NETIP6_NAME,
                     "NetIpv6GetFirstIfAddr : IP6 Task Lock Failed !!!\n");
        return (NETIPV6_FAILURE);
    }

    if (Ip6ifEntryExists (u4IfIndex) != IP6_SUCCESS)
    {
        IP6_TASK_UNLOCK ();
        IP6_TRC_ARG (u4ContextId, NETIP6_MOD_TRC, DATA_PATH_TRC, NETIP6_NAME,
                     "NetIpv6GetFirstIfAddr : Invalid Interface Index !!!\n");
        return (NETIPV6_FAILURE);
    }

    TMO_SLL_Scan (&gIp6GblInfo.apIp6If[u4IfIndex]->addr6Ilist,
                  pNode, tTMO_SLL_NODE *)
    {
        pAddr = IP6_ADDR_PTR_FROM_SLL (pNode);
        if (pAddr->u1ConfigMethod == (UINT1) IP6_ADDR_VIRTUAL)
        {
            continue;
        }

        if ((pAddr->u1Status & ADDR6_COMPLETE (gIp6GblInfo.apIp6If[u4IfIndex]))
            && (pAddr->u1Status & ADDR6_PREFERRED)
            && (pAddr->u1AddrType == ADDR6_UNICAST))
        {
            MEMCPY (&(pNetIpv6AddrInfo->Ip6Addr),
                    &(pAddr->ip6Addr), sizeof (tIp6Addr));
            pNetIpv6AddrInfo->u4PrefixLength = pAddr->u1PrefLen;
            pNetIpv6AddrInfo->u4Type = pAddr->u1AddrType;
            IP6_TASK_UNLOCK ();
            return (NETIPV6_SUCCESS);
        }
    }

    IP6_TASK_UNLOCK ();
    IP6_TRC_ARG (u4ContextId, NETIP6_MOD_TRC, DATA_PATH_TRC, NETIP6_NAME,
                 "NetIpv6GetFirstIfAddr : No Address is Configured !!!\n");
    return (NETIPV6_FAILURE);
}

/*
 ******************************************************************************
 * Function Name    :   NetIpv6GetNextIfAddr
 * Description      :   This function provides the next address of the
 *                      interface
 * Inputs           :   u4IfIndex - Interface index
 *                  :   pNetIpv6AddrInfo - Address information
 * Outputs          :   pNetIpv6NextAddrInfo - Next address information
 * Return Value     :   NETIPV6_SUCCESS | NETIPV6_FAILURE
 *******************************************************************************
 */

INT4
NetIpv6GetNextIfAddr (UINT4 u4IfIndex,
                      tNetIpv6AddrInfo * pNetIpv6AddrInfo,
                      tNetIpv6AddrInfo * pNetIpv6NextAddrInfo)
{
    tTMO_SLL_NODE      *pNode = NULL;
    tIp6AddrInfo       *pAddr = NULL;
    UINT4               u4CopyFlag = FALSE;
    UINT4               u4ContextId = IP6_ZERO;

    VcmGetContextIdFromCfaIfIndex (u4IfIndex, &u4ContextId);

    if (gIp6GblInfo.i4Ip6Status == IP6_FAILURE)
    {
        IP6_TRC_ARG (u4ContextId, NETIP6_MOD_TRC, DATA_PATH_TRC, NETIP6_NAME,
                     "NetIpv6GetNextIfAddr : IP6 Task Not Initialised !!!\n");
        return (NETIPV6_FAILURE);
    }

    if (IP6_TASK_LOCK () == SNMP_FAILURE)
    {
        IP6_TRC_ARG (u4ContextId, NETIP6_MOD_TRC, DATA_PATH_TRC, NETIP6_NAME,
                     "NetIpv6GetNextIfAddr : IP6 Task Lock Failed !!!\n");
        return (NETIPV6_FAILURE);
    }

    if (Ip6ifEntryExists (u4IfIndex) != IP6_SUCCESS)
    {
        IP6_TASK_UNLOCK ();
        IP6_TRC_ARG (u4ContextId, NETIP6_MOD_TRC, DATA_PATH_TRC, NETIP6_NAME,
                     "NetIpv6GetNextIfAddr : Invalid Interface Index !!!\n");
        return (NETIPV6_FAILURE);
    }

    TMO_SLL_Scan (&gIp6GblInfo.apIp6If[u4IfIndex]->addr6Ilist,
                  pNode, tTMO_SLL_NODE *)
    {
        pAddr = IP6_ADDR_PTR_FROM_SLL (pNode);

        if (pAddr->u1ConfigMethod == (UINT1) IP6_ADDR_VIRTUAL)
        {
            continue;
        }

        if ((pAddr->u1Status & ADDR6_COMPLETE (gIp6GblInfo.apIp6If[u4IfIndex]))
            && (pAddr->u1Status & ADDR6_PREFERRED)
            && (pAddr->u1AddrType == ADDR6_UNICAST) && (u4CopyFlag != FALSE))
        {
            MEMCPY (&(pNetIpv6NextAddrInfo->Ip6Addr),
                    &(pAddr->ip6Addr), sizeof (tIp6Addr));
            pNetIpv6NextAddrInfo->u4PrefixLength = pAddr->u1PrefLen;
            pNetIpv6NextAddrInfo->u4Type = pAddr->u1AddrType;
            IP6_TASK_UNLOCK ();
            return (NETIPV6_SUCCESS);
        }

        if ((Ip6AddrMatch (&(pAddr->ip6Addr),
                           &(pNetIpv6AddrInfo->Ip6Addr), pAddr->u1PrefLen)) &&
            (pAddr->u1PrefLen == pNetIpv6AddrInfo->u4PrefixLength))
        {
            u4CopyFlag = TRUE;
        }
    }

    IP6_TASK_UNLOCK ();
    IP6_TRC_ARG (u4ContextId, NETIP6_MOD_TRC, DATA_PATH_TRC, NETIP6_NAME,
                 "NetIpv6GetNextIfAddr : No Next Address is Configured !!!\n");
    return (NETIPV6_FAILURE);
}

/*
 ******************************************************************************
 * Function Name    :   NetIpv6GetIfIndexFromNameInCxt
 * Description      :   This function returns the Interface index for the
 *                      given Interface name
 * Inputs           :   u4L2ContextId - Layer 2 context Id for the Interface
 *                  :   au1IfName - Interface name
 * Outputs          :   u4IfIndex - Interface index of the given Ipv6 address
 * Return Value     :   NETIPV6_SUCCESS | NETIPV6_FAILURE
 *******************************************************************************
 */

INT4
NetIpv6GetIfIndexFromNameInCxt (UINT4 u4L2ContextId, UINT1 *au1IfName,
                                UINT4 *pu4Index)
{
    if (gIp6GblInfo.i4Ip6Status == IP6_FAILURE)
    {
        IP6_GBL_TRC_ARG (NETIP6_MOD_TRC, DATA_PATH_TRC, NETIP6_NAME,
                         "NetIpv6GetIfIndexFromName : IP6 Task Not Initialised !!!\n");
        return (NETIPV6_FAILURE);
    }

    if (IP6_TASK_LOCK () == SNMP_FAILURE)
    {
        IP6_GBL_TRC_ARG (NETIP6_MOD_TRC, DATA_PATH_TRC, NETIP6_NAME,
                         "NetIpv6GetIfIndexFromName : IP6 Task Lock Failed !!!\n");
        return (NETIPV6_FAILURE);
    }

    if (Ip6GetIfIndexFromNameInCxt (u4L2ContextId, au1IfName, pu4Index) ==
        IP6_SUCCESS)
    {
        IP6_TASK_UNLOCK ();
        return (NETIPV6_SUCCESS);
    }
    IP6_GBL_TRC_ARG (NETIP6_MOD_TRC, DATA_PATH_TRC, NETIP6_NAME,
                     "NetIpv6GetIfIndexFromName : Failed to Get Index From Name !!!\n");
    IP6_TASK_UNLOCK ();
    return (NETIPV6_FAILURE);
}

/*
 ******************************************************************************
 * Function Name    :   NetIpv6RegisterHigherLayerProtocol
 * Description      :   This function registers the protocol specified by
 *                      the application
 * Inputs           :   u4Proto - Protocol identifier
 *                  :   u4Mask  - Bit that identify the function pointer type
 *                  :   pFp     - Function pointer to be registered
 * Outputs          :   None
 * Return Value     :   NETIPV6_SUCCESS | NETIPV6_FAILURE
 *******************************************************************************
 */
INT4
NetIpv6RegisterHigherLayerProtocol (UINT4 u4Proto, UINT4 u4Mask, VOID *pFp)
{
    return (NetIpv6RegisterHigherLayerProtocolInCxt (IP6_DEFAULT_CONTEXT,
                                                     u4Proto, u4Mask, pFp));
}

/*
 ******************************************************************************
 * Function Name    :   NetIpv6RegisterHigherLayerProtocolInCxt
 * Description      :   This function registers the protocol specified by
 *                      the application for each context
 * Inputs           :   u4ContextId - context Identifier
 *                  :   u4Proto - Protocol identifier
 *                  :   u4Mask  - Bit that identify the function pointer type
 *                  :   pFp     - Function pointer to be registered
 * Outputs          :   None
 * Return Value     :   NETIPV6_SUCCESS | NETIPV6_FAILURE
 *******************************************************************************
 */

INT4
NetIpv6RegisterHigherLayerProtocolInCxt (UINT4 u4ContextId, UINT4 u4Proto,
                                         UINT4 u4Mask, VOID *pFp)
{
    tIp6Cxt            *pIp6Cxt = NULL;

    if (gIp6GblInfo.i4Ip6Status == IP6_FAILURE)
    {
        IP6_TRC_ARG (u4ContextId, NETIP6_MOD_TRC, DATA_PATH_TRC, NETIP6_NAME,
                     "NetIpv6RegisterHigherLayerProtocol : IP6 Task Not "
                     "Initialised !!!\n");
        return (NETIPV6_FAILURE);
    }

    if (IP6_TASK_LOCK () == SNMP_FAILURE)
    {
        IP6_TRC_ARG (u4ContextId, NETIP6_MOD_TRC, DATA_PATH_TRC, NETIP6_NAME,
                     "NetIpv6RegisterHigherLayerProtocol : IP6 Task Lock "
                     "Failed !!!\n");
        return (NETIPV6_FAILURE);
    }

    if (u4Proto >= IP6_MAX_PROTOCOLS)
    {
        IP6_TRC_ARG (u4ContextId, NETIP6_MOD_TRC, DATA_PATH_TRC, NETIP6_NAME,
                     "NetIpv6RegisterHigherLayerProtocol : Invalid Protocol "
                     "ID!!!\n");
        IP6_TASK_UNLOCK ();
        return (NETIPV6_FAILURE);
    }

    if (pFp == NULL)
    {
        IP6_TRC_ARG (u4ContextId, NETIP6_MOD_TRC, DATA_PATH_TRC, NETIP6_NAME,
                     "NetIpv6RegisterHigherLayerProtocol : Function Pointer "
                     "is NULL !!!\n");
        IP6_TASK_UNLOCK ();
        return (NETIPV6_FAILURE);
    }

    pIp6Cxt = Ipv6UtilGetCxtEntryFromCxtId (u4ContextId);
    if (pIp6Cxt != NULL)
    {
        if ((pIp6Cxt->au1HLRegTable[u4Proto] & u4Mask) == u4Mask)
        {
            /* This protocol is already registered for the given 
             * application in this context */
            IP6_TASK_UNLOCK ();
            return (NETIPV6_SUCCESS);

        }
        if (pIp6Cxt->au1HLRegTable[u4Proto] == 0)
        {
            gaNetIpv6RegTable[u4Proto].u1NumOfReg++;
        }
        pIp6Cxt->au1HLRegTable[u4Proto] |= u4Mask;
    }
    else
    {
        /* Registration in invalid context */
        IP6_TASK_UNLOCK ();
        return (NETIPV6_FAILURE);
    }

    /* If this is the first context registrating for this
     * proto means, update gaNetIpv6Reg*/
    if ((u4Mask & NETIPV6_APPLICATION_RECEIVE)
        && (gaNetIpv6RegTable[u4Proto].pAppRcv == NULL))
    {
        IP6_TRC_ARG (u4ContextId, NETIP6_MOD_TRC, DATA_PATH_TRC, NETIP6_NAME,
                     "NetIpv6RegisterHigherLayerProtocol : Application Send "
                     "and Receive Registered !!!\n");
        gaNetIpv6RegTable[u4Proto].pAppRcv =
            (VOID (*)(tNetIpv6HliParams * pNetIpv6HlParams)) pFp;
    }

    if ((u4Mask & NETIPV6_ADDRESS_CHANGE)
        && (gaNetIpv6RegTable[u4Proto].pAddrChange == NULL))
    {
        IP6_TRC_ARG (u4ContextId, NETIP6_MOD_TRC, DATA_PATH_TRC, NETIP6_NAME,
                     "NetIpv6RegisterHigherLayerProtocol : Address Change "
                     "Registered !!!\n");
        gaNetIpv6RegTable[u4Proto].pAddrChange =
            (VOID (*)(tNetIpv6HliParams * pNetIpv6HlParams)) pFp;
    }

    if ((u4Mask & NETIPV6_ROUTE_CHANGE)
        && (gaNetIpv6RegTable[u4Proto].pRouteChange == NULL))
    {
        IP6_TRC_ARG (u4ContextId, NETIP6_MOD_TRC, DATA_PATH_TRC, NETIP6_NAME,
                     "NetIpv6RegisterHigherLayerProtocol : Route Change "
                     "Registered !!!\n");
        gaNetIpv6RegTable[u4Proto].pRouteChange =
            (VOID (*)(tNetIpv6HliParams * pNetIpv6HlParams)) pFp;
        Ip6ActOnHLRegDereg (u4ContextId, IP6_REGISTER,
                            u4Proto, NETIPV6_ROUTE_CHANGE);
    }

    if ((u4Mask & NETIPV6_INTERFACE_PARAMETER_CHANGE)
        && (gaNetIpv6RegTable[u4Proto].pIfChange == NULL))
    {
        IP6_TRC_ARG (u4ContextId, NETIP6_MOD_TRC, DATA_PATH_TRC, NETIP6_NAME,
                     "NetIpv6RegisterHigherLayerProtocol : Interface Change "
                     "Registered !!!\n");
        gaNetIpv6RegTable[u4Proto].pIfChange =
            (VOID (*)(tNetIpv6HliParams * pNetIpv6HlParams)) pFp;
        Ip6ActOnHLRegDereg (u4ContextId, IP6_REGISTER, u4Proto,
                            NETIPV6_INTERFACE_PARAMETER_CHANGE);
    }

    if ((u4Mask & NETIPV6_ICMPV6_ERROR_SEND)
        && (gaNetIpv6RegTable[u4Proto].pIcmp6ErrSnd == NULL))
    {
        IP6_TRC_ARG (u4ContextId, NETIP6_MOD_TRC, DATA_PATH_TRC, NETIP6_NAME,
                     "NetIpv6RegisterHigherLayerProtocol : ICMP6 Error Send "
                     "Registered !!!\n");
        gaNetIpv6RegTable[u4Proto].pIcmp6ErrSnd =
            (VOID (*)(tNetIpv6HliParams * pNetIpv6HlParams)) pFp;
    }

    /* Registration of Zone change indication to Hlayer - RFC 4007 */
    if ((u4Mask & NETIPV6_ZONE_CHANGE) &&
        (gaNetIpv6RegTable[u4Proto].pZoneChange == NULL))
    {
        IP6_TRC_ARG (u4ContextId, NETIP6_MOD_TRC, DATA_PATH_TRC, NETIP6_NAME,
                     "NetIpv6RegisterHigherLayerProtocol : Non-Global Zone Change "
                     "Registered !!!\n");
        gaNetIpv6RegTable[u4Proto].pZoneChange =
            (VOID (*)(tNetIpv6HliParams * pNetIpv6HlParams)) pFp;
    }

    /* Registration of Zone change indication to Hlayer - RFC 4007 End */
    IP6_TASK_UNLOCK ();
    return (NETIPV6_SUCCESS);
}

/*
 ******************************************************************************
 * Function Name    :   NetIpv6DeRegisterHigherLayerProtocol
 * Description      :   This function de-registers the protocol specified by
 *                      the application
 * Inputs           :   u4Proto - Protocol identifier
 * Outputs          :   None
 * Return Value     :   NETIPV6_SUCCESS | NETIPV6_FAILURE
 *******************************************************************************
 */
INT4
NetIpv6DeRegisterHigherLayerProtocol (UINT4 u4Proto)
{
    return (NetIpv6DeRegisterHigherLayerProtocolInCxt (IP6_DEFAULT_CONTEXT,
                                                       u4Proto));
}

/*
 ******************************************************************************
 * Function Name    :   NetIpv6DeRegisterHigherLayerProtocolInCxt
 * Description      :   This function de-registers the protocol specified by
 *                      the application per context
 * Inputs           :   u4ContextId - Context Identifier
 *                  :   u4Proto - Protocol identifier
 * Outputs          :   None
 * Return Value     :   NETIPV6_SUCCESS | NETIPV6_FAILURE
 *******************************************************************************
 */

INT4
NetIpv6DeRegisterHigherLayerProtocolInCxt (UINT4 u4ContextId, UINT4 u4Proto)
{
    tIp6Cxt            *pIp6Cxt = NULL;
    if (gIp6GblInfo.i4Ip6Status == IP6_FAILURE)
    {
        return (NETIPV6_FAILURE);
    }

    if (IP6_TASK_LOCK () == SNMP_FAILURE)
    {
        IP6_TRC_ARG (u4ContextId, NETIP6_MOD_TRC, DATA_PATH_TRC, NETIP6_NAME,
                     "NetIpv6DeRegisterHigherLayerProtocol : IP6 Task Lock "
                     "Failed !!!\n");
        return (NETIPV6_FAILURE);
    }

    if (u4Proto >= IP6_MAX_PROTOCOLS)
    {
        IP6_TRC_ARG (u4ContextId, NETIP6_MOD_TRC, DATA_PATH_TRC, NETIP6_NAME,
                     "NetIpv6DeRegisterHigherLayerProtocol : Invalid Protocol "
                     "ID !!!\n");
        IP6_TASK_UNLOCK ();
        return (NETIPV6_FAILURE);
    }

    pIp6Cxt = Ipv6UtilGetCxtEntryFromCxtId (u4ContextId);
    if (pIp6Cxt != NULL)
    {
        if (pIp6Cxt->au1HLRegTable[u4Proto] == NETIPV6_REG_DISABLE)
        {
            /* This protocol has already de-registered for the given context */
            IP6_TASK_UNLOCK ();
            return (NETIPV6_SUCCESS);

        }

        pIp6Cxt->au1HLRegTable[u4Proto] = NETIPV6_REG_DISABLE;
    }
    else
    {
        IP6_TASK_UNLOCK ();
        return (NETIPV6_FAILURE);
    }

    gaNetIpv6RegTable[u4Proto].u1NumOfReg--;
    if (gaNetIpv6RegTable[u4Proto].u1NumOfReg == 0)
    {
        gaNetIpv6RegTable[u4Proto].pAppRcv = NULL;
        gaNetIpv6RegTable[u4Proto].pAddrChange = NULL;
        gaNetIpv6RegTable[u4Proto].pRouteChange = NULL;
        gaNetIpv6RegTable[u4Proto].pIfChange = NULL;
        gaNetIpv6RegTable[u4Proto].pIcmp6ErrSnd = NULL;
        /* DeRegistration of Zone change indication from Hlayer - RFC 4007 */
        gaNetIpv6RegTable[u4Proto].pZoneChange = NULL;
        IP6_TRC_ARG1 (u4ContextId, NETIP6_MOD_TRC, DATA_PATH_TRC, NETIP6_NAME,
                      "NetIpv6DeRegisterHigherLayerProtocol : Protocol [%d] "
                      "DeRegistration Success !!!\n", u4Proto);
    }
    IP6_TASK_UNLOCK ();
    return (NETIPV6_SUCCESS);
}

/*
 ******************************************************************************
 * Function Name    :   Ip6InitRegTable
 * Description      :   This routine is used to initializes the NetIpv6
 *                      Registration Table
 * Inputs           :   None
 * Outputs          :   None
 * Return Value     :   None
 *******************************************************************************
 */

VOID
Ip6InitRegTable (VOID)
{
    UINT4               u4Index;

    for (u4Index = 0; u4Index < IP6_MAX_PROTOCOLS; u4Index++)
    {
        gaNetIpv6RegTable[u4Index].pAppRcv = NULL;
        gaNetIpv6RegTable[u4Index].pAddrChange = NULL;
        gaNetIpv6RegTable[u4Index].pRouteChange = NULL;
        gaNetIpv6RegTable[u4Index].pIfChange = NULL;
        gaNetIpv6RegTable[u4Index].pIcmp6ErrSnd = NULL;
        /* Initialization of Zone change indication params to Hlayer - RFC 4007 */
        gaNetIpv6RegTable[u4Index].pZoneChange = NULL;
    }

    IP6_GBL_TRC_ARG (NETIP6_MOD_TRC, DATA_PATH_TRC, NETIP6_NAME,
                     "Ip6InitRegTable : Initialization Success !!!\n");
    return;
}

/*
 ******************************************************************************
 * Function Name    :   NetIpv6InvokeApplicationReceive
 * Description      :   This function invokes the registered application Send
 *                      and receive call back function
 * Inputs           :   pBuf     - Buffer Pointer
 *                  :   u4PktLen - Packet Length of the Buffer
 *                  :   u4Index  - Interface Index
 *                  :   u4Type   - Address Type
 *                  :   u4Proto  - Protocol identifier
 * Outputs          :   None
 * Return Value     :   NETIPV6_SUCCESS | NETIPV6_FAILURE
 *******************************************************************************
 */

INT4
NetIpv6InvokeApplicationReceive (tCRU_BUF_CHAIN_HEADER * pBuf,
                                 UINT4 u4PktLen,
                                 UINT4 u4Index, UINT4 u4Type, UINT4 u4Proto)
{
    tNetIpv6HliParams   Ip6HlParams;
    tIp6Cxt            *pIp6Cxt = NULL;
    UINT4               u4ContextId;

    Ip6GetCxtId (u4Index, &u4ContextId);
    IP6_TRC_ARG (u4ContextId, NETIP6_MOD_TRC, DATA_PATH_TRC, NETIP6_NAME,
                 "NetIpv6InvokeApplicationReceive : Invoke Trace !!!\n");

    if (pBuf == NULL)
    {
        IP6_TRC_ARG (u4ContextId, NETIP6_MOD_TRC, DATA_PATH_TRC, NETIP6_NAME,
                     "NetIpv6InvokeApplicationReceive : Buffer Pointer NULL !!!\n");
        return (NETIPV6_FAILURE);
    }

    if (u4Proto >= IP6_MAX_PROTOCOLS)
    {
        IP6_TRC_ARG (u4ContextId, NETIP6_MOD_TRC, DATA_PATH_TRC, NETIP6_NAME,
                     "NetIpv6InvokeApplicationReceive : Invalid Protocol ID !!!\n");
        return (NETIPV6_FAILURE);
    }

    pIp6Cxt = Ipv6UtilGetCxtEntryFromIfIndex (u4Index);
    if (pIp6Cxt != NULL)
    {
        if ((pIp6Cxt->au1HLRegTable[u4Proto] & NETIPV6_APPLICATION_RECEIVE)
            != NETIPV6_APPLICATION_RECEIVE)
        {
            return (NETIPV6_FAILURE);
        }
    }
    else
    {
        return (NETIPV6_FAILURE);
    }

    MEMSET (&Ip6HlParams, ZERO, sizeof (tNetIpv6HliParams));
    if (gaNetIpv6RegTable[u4Proto].pAppRcv)
    {
        Ip6HlParams.unIpv6HlCmdType.AppRcv.pBuf = pBuf;
        Ip6HlParams.unIpv6HlCmdType.AppRcv.u4PktLength = u4PktLen;
        Ip6HlParams.unIpv6HlCmdType.AppRcv.u4Index = u4Index;
        Ip6HlParams.unIpv6HlCmdType.AppRcv.u4Type = u4Type;
        Ip6HlParams.u4Command = NETIPV6_APPLICATION_RECEIVE;
        gaNetIpv6RegTable[u4Proto].pAppRcv (&Ip6HlParams);
        return (NETIPV6_SUCCESS);
    }

    return (NETIPV6_FAILURE);
}

/*
 ******************************************************************************
 * Function Name    :   NetIpv6InvokeAddressChange
 * Description      :   This function invokes the registered Address Status
 *                      Change call back function
 * Inputs           :   pIp6Addr - Pointer to the Address
 *                  :   u4PreLen - Prefix Length of the Address
 *                  :   u4Type   - Type of the Address
 *                  :   u4Index  - Interface Index
 *                  :   u4Mask   - Type of Address Status Change
 * Outputs          :   None
 * Return Value     :   NETIPV6_SUCCESS | NETIPV6_FAILURE
 *******************************************************************************
 */

INT4
NetIpv6InvokeAddressChange (tIp6Addr * pIp6Addr,
                            UINT4 u4PreLen,
                            UINT4 u4Type, UINT4 u4Index, UINT4 u4Mask)
{
    UINT4               u4Proto = 0;
#ifdef NPAPI_WANTED
    UINT4               u4NpAddrType = 0;
#endif /* NPAPI_WANTED */
    tNetIpv6HliParams   Ip6HlParams;
    tIp6Cxt            *pIp6Cxt = NULL;

    pIp6Cxt = Ipv6UtilGetCxtEntryFromIfIndex (u4Index);
    if (pIp6Cxt == NULL)
    {
        return (NETIPV6_FAILURE);
    }

    IP6_TRC_ARG (pIp6Cxt->u4ContextId, NETIP6_MOD_TRC,
                 DATA_PATH_TRC, NETIP6_NAME,
                 "NetIpv6InvokeAddressChange : Invoke Trace !!!\n");

    MEMSET (&Ip6HlParams, ZERO, sizeof (tNetIpv6HliParams));
    MEMCPY (&(Ip6HlParams.unIpv6HlCmdType.AddrChange.Ipv6AddrInfo.Ip6Addr),
            (pIp6Addr), sizeof (tIp6Addr));
    Ip6HlParams.unIpv6HlCmdType.AddrChange.Ipv6AddrInfo.u4PrefixLength =
        u4PreLen;
    Ip6HlParams.unIpv6HlCmdType.AddrChange.Ipv6AddrInfo.u4Type = u4Type;
    Ip6HlParams.unIpv6HlCmdType.AddrChange.u4Index = u4Index;
    Ip6HlParams.unIpv6HlCmdType.AddrChange.u4Mask = u4Mask;
    Ip6HlParams.u4Command = NETIPV6_ADDRESS_CHANGE;

#ifdef NPAPI_WANTED
    switch (u4Type)
    {
        case ADDR6_UNICAST:
        case ADDR6_ANYCAST:
        case ADDR6_LLOCAL:
        case ADDR6_V4_COMPAT:
            u4NpAddrType = NP_IP6_UNI_ADDR;
            break;

        case ADDR6_MULTI:
            u4NpAddrType = NP_IP6_MULTI_ADDR;
            break;

        default:
            /* Other address types not supported. */
            u4NpAddrType = 0;
    }

    if (u4NpAddrType != 0)
    {
        if (u4Mask == NETIPV6_ADDRESS_ADD)
        {
            Ipv6FsNpIpv6AddrCreate (pIp6Cxt->u4ContextId, u4Index, u4NpAddrType,
                                    (UINT1 *) pIp6Addr, (UINT1) u4PreLen);
        }
        else
        {
            Ipv6FsNpIpv6AddrDelete (pIp6Cxt->u4ContextId, u4Index, u4NpAddrType,
                                    (UINT1 *) pIp6Addr, (UINT1) u4PreLen);
        }
    }
#endif /* NPAPI_WANTED */

    for (u4Proto = ZERO; u4Proto < IP6_MAX_PROTOCOLS; u4Proto++)
    {
        if ((pIp6Cxt->au1HLRegTable[u4Proto] & NETIPV6_ADDRESS_CHANGE)
            != NETIPV6_ADDRESS_CHANGE)
        {
            continue;
        }

        if (gaNetIpv6RegTable[u4Proto].pAddrChange)
        {
            gaNetIpv6RegTable[u4Proto].pAddrChange (&Ip6HlParams);
        }
    }

    return (NETIPV6_SUCCESS);
}

/*
 ******************************************************************************
 * Function Name    :   NetIpv6InvokeRouteChangeForEcmp
 * Description      :   This function invokes the registered Route Status
 *                      Change call back function
 * Inputs           :   pNetIpv6RtInfo - Pointer to Route Change Information
 *                      u4Proto - If equal to 0xFFFFFFFF then send the route
 *                                change notification to all the protocols.
 *                                Else send the notification to the specific
 *                                protocol.
 * Outputs          :   None
 * Return Value     :   NETIPV6_SUCCESS | NETIPV6_FAILURE
 *******************************************************************************
 */

INT4
NetIpv6InvokeRouteChangeForEcmp (tNetIpv6RtInfo * pNetIpv6RtInfo, UINT4 u4Proto)
{
    tIp6Cxt            *pIp6Cxt = NULL;

    pIp6Cxt = Ipv6UtilGetCxtEntryFromIfIndex (pNetIpv6RtInfo->u4Index);
    if (pIp6Cxt == NULL)
    {
        return (NETIPV6_FAILURE);
    }
    IP6_TRC_ARG (pIp6Cxt->u4ContextId, NETIP6_MOD_TRC, DATA_PATH_TRC,
                 NETIP6_NAME, "NetIpv6InvokeRouteChange: Notifing Route change"
                 " to higher layer protocols\n");

    UNUSED_PARAM (pIp6Cxt);
    UNUSED_PARAM (u4Proto);

    return (NETIPV6_SUCCESS);
}

/*
 ******************************************************************************
 * Function Name    :   NetIpv6InvokeRouteChange
 * Description      :   This function invokes the registered Route Status
 *                      Change call back function
 * Inputs           :   pNetIpv6RtInfo - Pointer to Route Change Information
 *                      u4Proto - If equal to 0xFFFFFFFF then send the route
 *                                change notification to all the protocols.
 *                                Else send the notification to the specific
 *                                protocol.
 * Outputs          :   None
 * Return Value     :   NETIPV6_SUCCESS | NETIPV6_FAILURE
 *******************************************************************************
 */

INT4
NetIpv6InvokeRouteChange (tNetIpv6RtInfo * pNetIpv6RtInfo, UINT4 u4Proto)
{
    tNetIpv6HliParams   Ip6HlParams;
    tIp6Cxt            *pIp6Cxt = NULL;
    UINT4               u4Protocol;

    pIp6Cxt = Ipv6UtilGetCxtEntryFromIfIndex (pNetIpv6RtInfo->u4Index);
    if (pIp6Cxt == NULL)
    {
        return (NETIPV6_FAILURE);
    }
    IP6_TRC_ARG (pIp6Cxt->u4ContextId, NETIP6_MOD_TRC, DATA_PATH_TRC,
                 NETIP6_NAME, "NetIpv6InvokeRouteChange: Notifing Route change"
                 " to higher layer protocols\n");

    MEMSET (&Ip6HlParams, ZERO, sizeof (tNetIpv6HliParams));
    MEMCPY (&(Ip6HlParams.unIpv6HlCmdType.RouteChange),
            (pNetIpv6RtInfo), sizeof (tNetIpv6RtInfo));
    Ip6HlParams.u4Command = NETIPV6_ROUTE_CHANGE;

    if (u4Proto != NETIPV6_ALL_PROTO)
    {
        /* Verify if the protocol has registered with the ip6 task */
        if ((pIp6Cxt->au1HLRegTable[u4Proto] & NETIPV6_ROUTE_CHANGE)
            != NETIPV6_ROUTE_CHANGE)
        {
            return (NETIPV6_FAILURE);
        }

        /* Route Status Change to be send only the specific protocol */
        if (gaNetIpv6RegTable[u4Proto].pRouteChange)
        {
            gaNetIpv6RegTable[u4Proto].pRouteChange (&Ip6HlParams);
        }
        return (NETIPV6_SUCCESS);
    }

    for (u4Protocol = ZERO; u4Protocol < IP6_MAX_PROTOCOLS; u4Protocol++)
    {
        if ((pIp6Cxt->au1HLRegTable[u4Protocol] & NETIPV6_ROUTE_CHANGE)
            != NETIPV6_ROUTE_CHANGE)
        {
            continue;
        }
        if (gaNetIpv6RegTable[u4Protocol].pRouteChange)
        {
            gaNetIpv6RegTable[u4Protocol].pRouteChange (&Ip6HlParams);
        }
    }

    return (NETIPV6_SUCCESS);
}

/*
 ******************************************************************************
 * Function Name    :   NetIpv6InvokeInterfaceStatusChange
 * Description      :   This function invokes the registered Interface Status
 *                      Change call back function
 * Inputs           :   u4Index  - Interface Index
 *                  :   u4Proto  - Protocol ID
 *                  :   u4Value  - Value of Interface Status Change Parameter
 *                  :   u4Mask   - Type of Interface Status Change
 * Outputs          :   None
 * Return Value     :   NETIPV6_SUCCESS | NETIPV6_FAILURE
 *******************************************************************************
 */

INT4
NetIpv6InvokeInterfaceStatusChange (UINT4 u4Index,
                                    UINT4 u4Proto, UINT4 u4Value, UINT4 u4Mask)
{
    UINT4               u4ProtoCtr;
    UINT4               u4IpPort = ZERO;
    tNetIpv6HliParams   Ip6HlParams;
    tIp6Cxt            *pIp6Cxt = NULL;

    pIp6Cxt = Ipv6UtilGetCxtEntryFromIfIndex (u4Index);
    if (pIp6Cxt == NULL)
    {
        return (NETIPV6_FAILURE);
    }
    IP6_TRC_ARG (pIp6Cxt->u4ContextId, NETIP6_MOD_TRC, DATA_PATH_TRC,
                 NETIP6_NAME,
                 "NetIpv6InvokeInterfaceStatusChange : Invoke Trace !!!\n");

    MEMSET (&Ip6HlParams, ZERO, sizeof (tNetIpv6HliParams));
    Ip6HlParams.unIpv6HlCmdType.IfStatChange.u4Index = u4Index;

    switch (u4Mask)
    {
        case NETIPV6_INTERFACE_MTU_CHANGE:
            Ip6HlParams.unIpv6HlCmdType.IfStatChange.u4Mtu = u4Value;
            break;

        case NETIPV6_INTERFACE_SPEED_CHANGE:
            Ip6HlParams.unIpv6HlCmdType.IfStatChange.u4IfSpeed = u4Value;
            break;

        case NETIPV6_INTERFACE_HIGH_SPEED_CHANGE:
            Ip6HlParams.unIpv6HlCmdType.IfStatChange.u4IfSpeed = u4Value;
            break;

        case NETIPV6_INTERFACE_STATUS_CHANGE:
            Ip6HlParams.unIpv6HlCmdType.IfStatChange.u4IfStat = u4Value;
            Ipv6GetIPPort (u4Index, &u4IpPort);
            Ip6HlParams.unIpv6HlCmdType.IfStatChange.u4IpPort = u4IpPort;
            break;

        default:
            IP6_TRC_ARG (pIp6Cxt->u4ContextId, NETIP6_MOD_TRC, DATA_PATH_TRC,
                         NETIP6_NAME,
                         "NetIpv6InvokeInterfaceStatusChange : Invalid Mask !!!\n");
            return (NETIPV6_FAILURE);
    }

    Ip6HlParams.unIpv6HlCmdType.IfStatChange.u4Mask = u4Mask;
    Ip6HlParams.u4Command = NETIPV6_INTERFACE_PARAMETER_CHANGE;

    if ((u4Proto != NETIPV6_ALL_PROTO) && (u4Proto < IP6_MAX_PROTOCOLS))
    {
        /* Verify if the protocol has registered with the ip6 task */
        if ((pIp6Cxt->au1HLRegTable[u4Proto] &
             NETIPV6_INTERFACE_PARAMETER_CHANGE)
            != NETIPV6_INTERFACE_PARAMETER_CHANGE)
        {
            return (NETIPV6_FAILURE);
        }

        /* Interface Status Change to be send only the specific protocol */
        if (gaNetIpv6RegTable[u4Proto].pIfChange)
        {
            gaNetIpv6RegTable[u4Proto].pIfChange (&Ip6HlParams);
        }
        return (NETIPV6_SUCCESS);
    }

    for (u4ProtoCtr = IP6_ZERO; u4ProtoCtr < IP6_MAX_PROTOCOLS; u4ProtoCtr++)
    {
        /* Invoke the callback only if the protocol has registered with 
         * the ip6 task in the given context */
        if ((pIp6Cxt->au1HLRegTable[u4ProtoCtr] &
             NETIPV6_INTERFACE_PARAMETER_CHANGE)
            != NETIPV6_INTERFACE_PARAMETER_CHANGE)
        {
            continue;
        }
        /* Interface Status Change to be send to all registered protocol */
        if (gaNetIpv6RegTable[u4ProtoCtr].pIfChange)
        {
            gaNetIpv6RegTable[u4ProtoCtr].pIfChange (&Ip6HlParams);
        }
    }

    return (NETIPV6_SUCCESS);
}

/*
 ******************************************************************************
 * Function Name    :   NetIpv6InvokeIcmpv6ErrorSend
 * Description      :   This function invokes the registered ICMPv6 Error
 *                      Send call back function
 * Inputs           :   pBuf     - Buffer Pointer
 *                  :   Ip6Addr  - Source Address of the ICMPv6 Message
 *                  :   u4Type   - ICMPv6 Message Type
 *                  :   u4Code   - ICMPv6 Message Code
 *                  :   u4PktLen - Packet Length of the Buffer
 *                  :   u4Index  - Interface Index
 *                  :   u4Proto  - Protocol identifier
 * Outputs          :   None
 * Return Value     :   NETIPV6_SUCCESS | NETIPV6_FAILURE
 *******************************************************************************
 */

INT4
NetIpv6InvokeIcmpv6ErrorSend (tCRU_BUF_CHAIN_HEADER * pBuf,
                              tIp6Addr * Ip6Addr,
                              UINT4 u4Type,
                              UINT4 u4Code,
                              UINT4 u4PktLen, UINT4 u4Index, UINT4 u4Proto)
{
    tNetIpv6HliParams   Ip6HlParams;
    tIp6Cxt            *pIp6Cxt = NULL;

    pIp6Cxt = Ipv6UtilGetCxtEntryFromIfIndex (u4Index);
    if (pIp6Cxt == NULL)
    {
        return NETIPV6_FAILURE;
    }
    IP6_TRC_ARG (pIp6Cxt->u4ContextId, NETIP6_MOD_TRC, DATA_PATH_TRC,
                 NETIP6_NAME,
                 "NetIpv6InvokeIcmpv6ErrorSend : Invoke Trace !!!\n");

    if (u4Proto >= IP6_MAX_PROTOCOLS)
    {
        IP6_TRC_ARG (pIp6Cxt->u4ContextId, NETIP6_MOD_TRC, DATA_PATH_TRC,
                     NETIP6_NAME,
                     "NetIpv6InvokeIcmpv6ErrorSend : Invalid Protocol ID !!!\n");
        return (NETIPV6_FAILURE);
    }

    if ((pIp6Cxt->au1HLRegTable[u4Proto] & NETIPV6_ICMPV6_ERROR_SEND)
        != NETIPV6_ICMPV6_ERROR_SEND)
    {
        return (NETIPV6_FAILURE);
    }

    MEMSET (&Ip6HlParams, ZERO, sizeof (tNetIpv6HliParams));

    if (gaNetIpv6RegTable[u4Proto].pIcmp6ErrSnd)
    {
        MEMCPY (&(Ip6HlParams.unIpv6HlCmdType.Icmpv6ErrorSend.Ip6Addr),
                Ip6Addr, sizeof (tIp6Addr));
        Ip6HlParams.unIpv6HlCmdType.Icmpv6ErrorSend.pBuf = pBuf;
        Ip6HlParams.unIpv6HlCmdType.Icmpv6ErrorSend.u4Type = u4Type;
        Ip6HlParams.unIpv6HlCmdType.Icmpv6ErrorSend.u4Code = u4Code;
        Ip6HlParams.unIpv6HlCmdType.Icmpv6ErrorSend.u4PktLength = u4PktLen;
        Ip6HlParams.unIpv6HlCmdType.Icmpv6ErrorSend.u4Index = u4Index;
        Ip6HlParams.u4Command = NETIPV6_ICMPV6_ERROR_SEND;
        gaNetIpv6RegTable[u4Proto].pIcmp6ErrSnd (&Ip6HlParams);
        return (NETIPV6_SUCCESS);
    }

    return (NETIPV6_FAILURE);
}

/*
 ******************************************************************************
 * Function Name    :   NetIpv6InvokeNonGlobalZoneChange 
 *
 * Description      :   This function invokes the registered zone Status
 *                      Change call back function
 *
 * Inputs           :   u4ZoneMask - Type of Zone Status Change
 *                      i4ZoneIndex - Zone Index for which status 
 *                                    changes is sent
 *                      u4IfIndex   - Interface index on which the 
 *                                    zone status is changed
 *
 * Outputs          :   None
 *
 * Return Value     :   NETIPV6_SUCCESS | NETIPV6_FAILURE
 *******************************************************************************/

INT4
NetIpv6InvokeNonGlobalZoneChange (UINT4 u4ZoneMask, INT4 i4ZoneIndex,
                                  UINT4 u4IfIndex)
{
    UINT4               u4Proto = 0;

    tNetIpv6HliParams   Ip6HlParams;
    tIp6Cxt            *pIp6Cxt = NULL;

    pIp6Cxt = Ipv6UtilGetCxtEntryFromIfIndex (u4IfIndex);
    if (pIp6Cxt == NULL)
    {
        return (NETIPV6_FAILURE);
    }

    IP6_TRC_ARG (pIp6Cxt->u4ContextId, NETIP6_MOD_TRC,
                 DATA_PATH_TRC, NETIP6_NAME,
                 "NetIpv6InvokeFirstNonGlobalZoneCreation: Invoke Trace !!!\n");

    MEMSET (&Ip6HlParams, ZERO, sizeof (tNetIpv6HliParams));
    Ip6HlParams.unIpv6HlCmdType.ZoneChange.i4ZoneIndex = i4ZoneIndex;
    Ip6HlParams.unIpv6HlCmdType.ZoneChange.u4ZoneMask = u4ZoneMask;
    Ip6HlParams.unIpv6HlCmdType.ZoneChange.u4IfIndex = u4IfIndex;
    Ip6HlParams.u4Command = NETIPV6_ZONE_CHANGE;

    for (u4Proto = ZERO; u4Proto < IP6_MAX_PROTOCOLS; u4Proto++)
    {
        if ((pIp6Cxt->au1HLRegTable[u4Proto] & NETIPV6_ZONE_CHANGE)
            != NETIPV6_ZONE_CHANGE)
        {
            continue;
        }

        if (gaNetIpv6RegTable[u4Proto].pZoneChange)
        {
            gaNetIpv6RegTable[u4Proto].pZoneChange (&Ip6HlParams);
        }
    }
    return (NETIPV6_SUCCESS);
}

/*
 ******************************************************************************
 * Function Name    :   NetIpv6LeakRoute
 * Description      :   This function, depending on the value of u1CmdType Adds
 *                      (or) Deletes (or) modifies the given Route Information
 *                      stored in Ip6FwdTable accordingly.
 * Inputs           :   u1CmdType - Command to ADD | Delete | Modify the Route.
 *                      pNetIpv6RtInfo - Route to be Added | Deleted | Modified.
 * Outputs          :   None.
 * Return Value     :   NETIPV6_SUCCESS | NETIPV6_FAILURE.
 *******************************************************************************
 */

INT4
NetIpv6LeakRoute (UINT1 u1CmdType, tNetIpv6RtInfo * pNetIpv6RtInfo)
{

    /* ContextId can be accessible in pNetIpv6RtInfo */
    if (Rtm6ApiIpv6LeakRoute (u1CmdType, pNetIpv6RtInfo) == RTM6_FAILURE)
    {
        return NETIPV6_FAILURE;
    }

    return NETIPV6_SUCCESS;
}

/*
 ******************************************************************************
 * Function Name    :   NetIpv6GetRoute
 * Description      :   This function provides the Route (either Exact Route or
 *                      Best route) for a given destination and Mask based on
 *                      the incoming request.
 * Inputs           :   pNetIpv6RtQuery - Infomation about the route to be
 *                                 retrieved.
 * Outputs          :   pNetIpv6RtInfo - Information about the requested route.
 * Return Value     :   NETIPV6_SUCCESS - if the route is present.
 *                      NETIPV6_FAILURE - if the route is not present.
 ******************************************************************************
 */

INT4
NetIpv6GetRoute (tNetIpv6RtInfoQueryMsg * pNetIpv6RtQuery,
                 tNetIpv6RtInfo * pNetIpv6RtInfo)
{
    if (gIp6GblInfo.i4Ip6Status == IP6_FAILURE)
    {
        IP6_TRC_ARG (pNetIpv6RtQuery->u4ContextId, NETIP6_MOD_TRC,
                     DATA_PATH_TRC, NETIP6_NAME,
                     "NetIpv6GetRoute : IP6 Task Not Initialised !!!\n");
        return (NETIPV6_FAILURE);
    }
    /* ContextId can be accessible in pNetIpv6RtQuery */
    if (Rtm6ApiNetIpv6GetRoute (pNetIpv6RtQuery, pNetIpv6RtInfo) ==
        RTM6_FAILURE)
    {
        return NETIPV6_FAILURE;
    }

    return NETIPV6_SUCCESS;
}

/*
 ******************************************************************************
 * Function Name    :   NetIpv6GetFwdTableRouteEntry 
 * Description      :   This function provides the Route (Best route) for a
 *                      given destination and Mask.
 * Inputs           :   pIp6DestAddr - The Destination Address.
 *                      u1PrefixLen - Prefix len for the given Destination
 *                                    Address.
 * Outputs          :   pNetIpv6RtInfo - The Route Information for the given
 *                      Destination and Mask.
 * Return Value     :   NETIPV6_SUCCESS | NETIPV6_FAILURE.
 *******************************************************************************
 */

INT4
NetIpv6GetFwdTableRouteEntry (tIp6Addr * pIp6DestAddr,
                              UINT1 u1PrefixLength,
                              tNetIpv6RtInfo * pNetIpv6RtInfo)
{
    /* For Default context */
    return (NetIpv6GetFwdTableRouteEntryInCxt (IP6_DEFAULT_CONTEXT,
                                               pIp6DestAddr, u1PrefixLength,
                                               pNetIpv6RtInfo));
}

/*
 ******************************************************************************
 * Function Name    :   NetIpv6GetFwdTableRouteEntryInCxt 
 * Description      :   This function provides the Route (Best route) for a
 *                      given destination and Mask.
 * Inputs           :   pIp6DestAddr - The Destination Address.
 *                      u1PrefixLen - Prefix len for the given Destination
 *                                    Address.
 * Outputs          :   pNetIpv6RtInfo - The Route Information for the given
 *                      Destination and Mask.
 * Return Value     :   NETIPV6_SUCCESS | NETIPV6_FAILURE.
 *******************************************************************************
 */
INT4
NetIpv6GetFwdTableRouteEntryInCxt (UINT4 u4ContextId,
                                   tIp6Addr * pIp6DestAddr,
                                   UINT1 u1PrefixLength,
                                   tNetIpv6RtInfo * pNetIpv6RtInfo)
{
    if (Rtm6ApiGetFwdRouteEntryInCxt (u4ContextId, pIp6DestAddr,
                                      u1PrefixLength, pNetIpv6RtInfo)
        == RTM6_FAILURE)
    {
        return NETIPV6_FAILURE;
    }

    return NETIPV6_SUCCESS;
}

/*
 ******************************************************************************
 * Function Name    :   NetIpv6GetFirstFwdTableRouteEntry
 * Description      :   This function provides the First Route Entry present in
 *                      the Ip6 Forwarding Table.
 * Inputs           :   None.
 * Outputs          :   pNetIpv6RtInfo - First Route Entry of the Ip6 Forwarding
 *                      Table.
 * Return Value     :   NETIPV6_SUCCESS | NETIPV6_FAILURE.
 *******************************************************************************
 */
INT4
NetIpv6GetFirstFwdTableRouteEntry (tNetIpv6RtInfo * pNetIpv6RtInfo)
{
    /* For Default context */
    return (NetIpv6GetFirstFwdTableRouteEntryInCxt (IP6_DEFAULT_CONTEXT,
                                                    pNetIpv6RtInfo));
}

/*
 ******************************************************************************
 * Function Name    :   NetIpv6GetFirstFwdTableRouteEntryInCxt
 * Description      :   This function provides the First Route Entry present in
 *                      the Ip6 Forwarding Table in the given context.
 * Inputs           :   u4ContextId - Context identifier.
 * Outputs          :   pNetIpv6RtInfo - First Route Entry of the Ip6 Forwarding
 *                      Table.
 * Return Value     :   NETIPV6_SUCCESS | NETIPV6_FAILURE.
 *******************************************************************************
 */
INT4
NetIpv6GetFirstFwdTableRouteEntryInCxt (UINT4 u4ContextId,
                                        tNetIpv6RtInfo * pNetIpv6RtInfo)
{

    if (gIp6GblInfo.i4Ip6Status == IP6_FAILURE)
    {
        IP6_TRC_ARG (u4ContextId, NETIP6_MOD_TRC, DATA_PATH_TRC, NETIP6_NAME,
                     "NetIpv6GetFirstFwdTableRouteEntry: IP6 Task Not Initialised !!!\n");
        return (NETIPV6_FAILURE);
    }

    if (Rtm6ApiTrieGetFirstEntryInCxt (u4ContextId,
                                       pNetIpv6RtInfo) == RTM6_FAILURE)
    {
        return NETIPV6_FAILURE;
    }
    return (NETIPV6_SUCCESS);
}

/*
 ******************************************************************************
 * Function Name    :   NetIpv6GetNextFwdTableRouteEntry
 * Description      :   This function returns the Next Route Entry in the Ip6
 *                      Forwarding Table for a given Route Entry.
 * Inputs           :   pNetIpv6RtInfo - Route Entry for which the Next Entry is
 *                      required.
 * Outputs          :   pNextNetRtInfo - Next Route Entry in the Ip Forwarding
 *                      Table.
 * Return Value     :   NETIPV6_SUCCESS | NETIPV6_FAILURE.
 *******************************************************************************
 */

INT4
NetIpv6GetNextFwdTableRouteEntry (tNetIpv6RtInfo * pNetIpv6RtInfo,
                                  tNetIpv6RtInfo * pNextNetIpv6RtInfo)
{
    if (gIp6GblInfo.i4Ip6Status == IP6_FAILURE)
    {
        IP6_TRC_ARG (pNetIpv6RtInfo->u4ContextId, NETIP6_MOD_TRC, DATA_PATH_TRC,
                     NETIP6_NAME,
                     "NetIpv6GetNextFwdTableRouteEntry: IP6 Task Not Initialised !!!\n");
        return (NETIPV6_FAILURE);
    }
    /* Currently this function is not being used.
     * This will give default context entry only */

    if (Rtm6ApiTrieGetNextEntry (pNetIpv6RtInfo,
                                 pNextNetIpv6RtInfo) == RTM6_FAILURE)
    {
        return NETIPV6_FAILURE;
    }
    return (NETIPV6_SUCCESS);
}

/*
 ******************************************************************************
 * Function Name    :   NetIpv6OspfMcastJoin
 * Description      :   This function register the provided MultiCast Address
 *                      to the specified interface.
 * Inputs           :   pMcastAddr     - Multicast Address to be registered.
 *                      u4IfIndex      - Interface over which this address
 *                                       needs to be registered.
 * Outputs          :   None.
 * Return Value     :   NETIPV6_SUCCESS | NETIPV6_FAILURE.
 *******************************************************************************
 */

INT4
NetIpv6OspfMcastJoin (UINT4 u4IfIndex, tIp6Addr * pMcastAddr)
{
    tIp6McRegNode      *pIp6MulitCastRegInfo = NULL;

    pIp6MulitCastRegInfo = MemAllocMemBlk (gIp6GblInfo.u4Ip6MCastPoolId);
    if (pIp6MulitCastRegInfo == NULL)
    {
        return NETIPV6_FAILURE;
    }
    MEMSET (pIp6MulitCastRegInfo, 0, sizeof (tIp6McRegNode));
    pIp6MulitCastRegInfo->u4IfIndex = u4IfIndex;
    MEMCPY (&pIp6MulitCastRegInfo->MCastaddr, pMcastAddr, sizeof (tIp6Addr));
    pIp6MulitCastRegInfo->u1MsgType = IP6_MULTICAST_JOIN;

    if (OsixSendToQ (SELF, (const UINT1 *) IP6_MULTICAST_REG_QUEUE,
                     (tOsixMsg *) pIp6MulitCastRegInfo,
                     OSIX_MSG_NORMAL) == OSIX_FAILURE)
    {
        MemReleaseMemBlock ((tMemPoolId) gIp6GblInfo.u4Ip6MCastPoolId,
                            (UINT1 *) pIp6MulitCastRegInfo);
        return NETIPV6_FAILURE;
    }
    else
    {
        if (OsixSendEvent
            (SELF, (const UINT1 *) IP6_TASK_NAME,
             IP6_MC_REG_EVENT) == OSIX_FAILURE)
        {
            return NETIPV6_FAILURE;
        }
    }
    return NETIPV6_SUCCESS;
}

/*
 ******************************************************************************
 * Function Name    :   NetIpv6OspfMcastLeave
 * Description      :   This function deregisters the provided MultiCast Address
 *                      from the specified interface.
 * Inputs           :   pMcastAddr     - Multicast Address to be deregistered.
 *                      u4IfIndex      - Interface over which this address
 *                                       needs to be deregistered.
 * Outputs          :   None.
 * Return Value     :   NETIPV6_SUCCESS | NETIPV6_FAILURE.
 *******************************************************************************
 */

INT4
NetIpv6OspfMcastLeave (UINT4 u4IfIndex, tIp6Addr * pMcastAddr)
{
    tIp6McRegNode      *pIp6MulitCastRegInfo = NULL;

    pIp6MulitCastRegInfo = MemAllocMemBlk (gIp6GblInfo.u4Ip6MCastPoolId);

    if (pIp6MulitCastRegInfo == NULL)
    {
        return NETIPV6_FAILURE;
    }

    MEMSET (pIp6MulitCastRegInfo, 0, sizeof (tIp6McRegNode));
    pIp6MulitCastRegInfo->u4IfIndex = u4IfIndex;
    MEMCPY (&pIp6MulitCastRegInfo->MCastaddr, pMcastAddr, sizeof (tIp6Addr));
    pIp6MulitCastRegInfo->u1MsgType = IP6_MULTICAST_LEAVE;

    if (OsixSendToQ (SELF, (const UINT1 *) IP6_MULTICAST_REG_QUEUE,
                     (tOsixMsg *) pIp6MulitCastRegInfo,
                     OSIX_MSG_NORMAL) == OSIX_FAILURE)
    {
        MemReleaseMemBlock ((tMemPoolId) gIp6GblInfo.u4Ip6MCastPoolId,
                            (UINT1 *) pIp6MulitCastRegInfo);
        return NETIPV6_FAILURE;
    }
    else
    {
        if (OsixSendEvent
            (SELF, (const UINT1 *) IP6_TASK_NAME,
             IP6_MC_REG_EVENT) == OSIX_FAILURE)
        {
            return NETIPV6_FAILURE;
        }
    }
    return NETIPV6_SUCCESS;
}

/*
 ******************************************************************************
 * Function Name    :   NetIpv6McastJoin
 * Description      :   This function register the provided MultiCast Address
 *                      to the specified interface.
 * Inputs           :   pMcastAddr     - Multicast Address to be registered.
 *                      u4IfIndex      - Interface over which this address
 *                                       needs to be registered.
 * Outputs          :   None.
 * Return Value     :   NETIPV6_SUCCESS | NETIPV6_FAILURE.
 *******************************************************************************
 */

INT4
NetIpv6McastJoin (UINT4 u4IfIndex, tIp6Addr * pMcastAddr)
{
    UINT4               u4ContextId = 0;

    VcmGetContextIdFromCfaIfIndex (u4IfIndex, &u4ContextId);
    if (gIp6GblInfo.i4Ip6Status == IP6_FAILURE)
    {
        IP6_TRC_ARG (u4ContextId, NETIP6_MOD_TRC, DATA_PATH_TRC, NETIP6_NAME,
                     "NetIpv6McastJoin: IP6 Task Not Initialised !!!\n");
        return (NETIPV6_FAILURE);
    }

    IP6_TASK_LOCK ();

    /* Verify whether the interface is valid or not. */
    if (gIp6GblInfo.apIp6If[u4IfIndex] == NULL)
    {
        IP6_TASK_UNLOCK ();
        return NETIPV6_FAILURE;
    }

    if (gIp6GblInfo.apIp6If[u4IfIndex]->u1AdminStatus == ADMIN_INVALID)
    {
        IP6_TASK_UNLOCK ();
        return NETIPV6_FAILURE;
    }

    if (Ip6AddrCreateMcast (u4IfIndex, pMcastAddr) == IP6_FAILURE)
    {
        /* Failure in Registering the Mcast Address */
        IP6_TASK_UNLOCK ();
        return NETIPV6_FAILURE;
    }

    IP6_TASK_UNLOCK ();
    return NETIPV6_SUCCESS;
}

/*
 ******************************************************************************
 * Function Name    :   NetIpv6McastLeave
 * Description      :   This function deregisters the provided MultiCast Address
 *                      from the specified interface.
 * Inputs           :   pMcastAddr     - Multicast Address to be deregistered.
 *                      u4IfIndex      - Interface over which this address
 *                                       needs to be deregistered.
 * Outputs          :   None.
 * Return Value     :   NETIPV6_SUCCESS | NETIPV6_FAILURE.
 *******************************************************************************
 */

INT4
NetIpv6McastLeave (UINT4 u4IfIndex, tIp6Addr * pMcastAddr)
{
    UINT4               u4ContextId = 0;

    VcmGetContextIdFromCfaIfIndex (u4IfIndex, &u4ContextId);
    if (gIp6GblInfo.i4Ip6Status == IP6_FAILURE)
    {
        IP6_TRC_ARG (u4ContextId, NETIP6_MOD_TRC, DATA_PATH_TRC, NETIP6_NAME,
                     "NetIpv6McastLeave: IP6 Task Not Initialised !!!\n");
        return (NETIPV6_FAILURE);
    }

    if (IP6_TASK_LOCK () == SNMP_FAILURE)
    {
        IP6_TRC_ARG (u4ContextId, NETIP6_MOD_TRC, DATA_PATH_TRC, NETIP6_NAME,
                     "NetIpv6McastLeave: IP6 Task Lock Failed !!!\n");
        return (NETIPV6_FAILURE);
    }

    /* Verify whether the interface is valid or not. */
    if (gIp6GblInfo.apIp6If[u4IfIndex] == NULL)
    {
        IP6_TASK_UNLOCK ();
        return NETIPV6_FAILURE;
    }

    if (gIp6GblInfo.apIp6If[u4IfIndex]->u1AdminStatus == ADMIN_INVALID)
    {
        IP6_TASK_UNLOCK ();
        return NETIPV6_FAILURE;
    }

    if (Ip6AddrDeleteMcast (u4IfIndex, pMcastAddr) == IP6_FAILURE)
    {
        /* Failure in Deregistering the Mcast Address */
        IP6_TASK_UNLOCK ();
        return NETIPV6_FAILURE;
    }

    IP6_TASK_UNLOCK ();
    return NETIPV6_SUCCESS;
}

/*
 ******************************************************************************
 * Function Name    :   NetIpv6GetSrcForDest
 * Description      :   Get a global unicast address which is not tentative
 *                      for the given destination address
 * Inputs           :   u4IfIndex      - Interface Index
 *                      pDstAddr       - Destination Address
 * Outputs          :   pSrcAddr       - Soruce Address
 * Return Value     :   NETIPV6_SUCCESS | NETIPV6_FAILURE
 *******************************************************************************
 */

INT4
NetIpv6GetSrcForDest (UINT4 u4IfIndex, tIp6Addr * pDstAddr, tIp6Addr * pSrcAddr)
{
    tIp6Addr           *pIp6Addr = NULL;

    if (IP6_TASK_LOCK () == SNMP_FAILURE)
    {
        return NETIPV6_FAILURE;
    }

    pIp6Addr = Ip6GetGlobalAddr (u4IfIndex, pDstAddr);
    if (pIp6Addr == NULL)
    {
        IP6_TASK_UNLOCK ();
        return NETIPV6_FAILURE;
    }
    MEMCPY (pSrcAddr, pIp6Addr, sizeof (tIp6Addr));
    IP6_TASK_UNLOCK ();
    return NETIPV6_SUCCESS;
}

/*
 ******************************************************************************
 * Function Name    :   NetIpv6IsOurAddressInCxt
 * Description      :   Check wether the given address is our addess and
 *                      fetch the interface index of that address
 * Inputs           :   pAddr          - IPv6 Address
 * Outputs          :   pu4IfIndex     - Interface Index
 * Return Value     :   NETIPV6_SUCCESS | NETIPV6_FAILURE
 *******************************************************************************
 */

INT4
NetIpv6IsOurAddressInCxt (UINT4 u4ContextId, tIp6Addr * pAddr,
                          UINT4 *pu4IfIndex)
{
    UINT4               u4IfIndex;

    if (IP6_TASK_LOCK () == SNMP_FAILURE)
    {
        return NETIPV6_FAILURE;
    }

    if (Ip6IsOurAddrInCxt (u4ContextId, pAddr, &u4IfIndex) != IP6_SUCCESS)
    {
        IP6_TASK_UNLOCK ();
        return NETIPV6_FAILURE;
    }
    *pu4IfIndex = u4IfIndex;

    IP6_TASK_UNLOCK ();
    return NETIPV6_SUCCESS;
}

/*
 * ****************************************************************************
 * Function Name     :  Ip6InitMCastRegTable
 *
 * Description       :  This function intialises the Multicast protocols
 *                      Registration Table at startup.
 *
 * Global Variables  :  gaNetIpv6MCastRegTable
 *    Affected
 *
 * Input(s)          :  None
 *
 * Output(s)         :  None
 *
 * Returns           :  None
 * *****************************************************************************
 */
VOID
Ip6InitMCastRegTable (VOID)
{
    UINT1               u1Index;

    for (u1Index = 0; u1Index < IP6_MAX_MCAST_PROTOCOLS; u1Index++)
    {
        gaNetIpv6MCastRegTable[u1Index].u1RegFlag = IP6_DEREGISTER;
        gaNetIpv6MCastRegTable[u1Index].u1RegCntr = IP6_ZERO;
        gaNetIpv6MCastRegTable[u1Index].pAppRcv = NULL;
    }
    return;
}

/*
 ******************************************************************************
 * Function Name    : NetIpv6RegisterHLProtocolForMCastPkts
 * Description      :   This function registers the protocol specified by
 *                      the application
 * Inputs           :   PappRcv - Function pointer to be called when mulitcast packet
 *                                needs to be passed to higher layer.
 * Outputs          :   None
 * Return Value     :   NETIPV6_FAILURE or Registered Application Id
 *******************************************************************************
 */

INT4
NetIpv6RegisterHLProtocolForMCastPkts (VOID (*pAppRcv)
                                       (tCRU_BUF_CHAIN_HEADER *))
{
    INT4                i4RetVal = NETIPV6_FAILURE;
    UINT1               u1Index = 0;

    for (u1Index = 0; u1Index < IP6_MAX_MCAST_PROTOCOLS; u1Index++)
    {
        if (gaNetIpv6MCastRegTable[u1Index].u1RegFlag == IP6_DEREGISTER)
        {
            break;
        }
    }

    i4RetVal = NetIpv6RegisterHLProtocolForMCastPktsInCxt (IP6_DEFAULT_CONTEXT,
                                                           u1Index, pAppRcv);
    if (i4RetVal == NETIPV6_FAILURE)
    {
        return NETIPV6_FAILURE;
    }
    else
    {
        return u1Index;
    }
}

/*
 ******************************************************************************
 * Function Name    : NetIpv6RegisterHLProtocolForMCastPktsInCxt
 * Description      :   This function registers the protocol specified by
 *                      the application
 * Inputs           :   PappRcv - Function pointer to be called when mulitcast packet
 *                                needs to be passed to higher layer.
 * Outputs          :   None
 * Return Value     :   NETIPV6_FAILURE or Registered Application Id
 *******************************************************************************
 */

INT4
NetIpv6RegisterHLProtocolForMCastPktsInCxt (UINT4 u4ContextId, UINT1 u1Proto,
                                            VOID (*pAppRcv)
                                            (tCRU_BUF_CHAIN_HEADER *))
{
    tIp6Cxt            *pIp6Cxt = NULL;

    if (pAppRcv == NULL)
    {
        return (NETIPV6_FAILURE);
    }

    if (u1Proto >= IP6_MAX_MCAST_PROTOCOLS)
    {
        return NETIPV6_FAILURE;
    }

    pIp6Cxt = Ipv6UtilGetCxtEntryFromCxtId (u4ContextId);
    if (pIp6Cxt != NULL)
    {
        if (pIp6Cxt->au1HLMcastRegTable[u1Proto] == NETIPV6_REG_ENABLE)
        {
            /* This protocol is already registered */
            return (NETIPV6_SUCCESS);
        }
        pIp6Cxt->au1HLMcastRegTable[u1Proto] = NETIPV6_REG_ENABLE;
    }
    else
    {
        return (NETIPV6_FAILURE);
    }

    if (gaNetIpv6MCastRegTable[u1Proto].u1RegCntr == 0)
    {
        gaNetIpv6MCastRegTable[u1Proto].u1RegFlag = IP6_REGISTER;
        gaNetIpv6MCastRegTable[u1Proto].pAppRcv =
            (VOID (*)(tIP_BUF_CHAIN_HEADER *)) pAppRcv;
    }
    gaNetIpv6MCastRegTable[u1Proto].u1RegCntr++;

    return NETIPV6_SUCCESS;
}

/*
 ******************************************************************************
 * Function Name    :   NetIpv6DeRegisterHLProtocolForMCastPkts
 * Description      :   This function de-registers the protocol specified by
 *                      the application
 * Inputs           :   u1AppId- Application Id to be de-registered
 * Outputs          :   None
 * Return Value     :   NETIPV6_SUCCESS | NETIPV6_FAILURE
 *******************************************************************************
 */

INT4
NetIpv6DeRegisterHLProtocolForMCastPkts (UINT1 u1AppId)
{
    return (NetIpv6DeRegisterHLProtocolForMCastPktsInCxt (IP6_DEFAULT_CONTEXT,
                                                          u1AppId));
}

/*
 ******************************************************************************
 * Function Name    :   NetIpv6DeRegisterHLProtocolForMCastPktsInCxt
 * Description      :   This function de-registers the protocol specified by
 *                      the application
 * Inputs           :   u1AppId- Application Id to be de-registered
 * Outputs          :   None
 * Return Value     :   NETIPV6_SUCCESS | NETIPV6_FAILURE
 *******************************************************************************
 */

INT4
NetIpv6DeRegisterHLProtocolForMCastPktsInCxt (UINT4 u4ContextId, UINT1 u1AppId)
{
    tIp6Cxt            *pIp6Cxt = NULL;

    if (u1AppId >= IP6_MAX_MCAST_PROTOCOLS)
    {
        return NETIPV6_FAILURE;
    }

    pIp6Cxt = Ipv6UtilGetCxtEntryFromCxtId (u4ContextId);
    if (pIp6Cxt != NULL)
    {
        if (pIp6Cxt->au1HLMcastRegTable[u1AppId] == NETIPV6_REG_DISABLE)
        {
            /* This protocol is already de-registered */
            return (NETIPV6_SUCCESS);
        }
        pIp6Cxt->au1HLMcastRegTable[u1AppId] = NETIPV6_REG_DISABLE;
    }
    else
    {
        return (NETIPV6_FAILURE);
    }

    gaNetIpv6MCastRegTable[u1AppId].u1RegCntr--;
    if (gaNetIpv6MCastRegTable[u1AppId].u1RegCntr == 0)
    {
        gaNetIpv6MCastRegTable[u1AppId].u1RegFlag = IP6_DEREGISTER;
        gaNetIpv6MCastRegTable[u1AppId].pAppRcv = NULL;
    }

    return NETIPV6_SUCCESS;
}

/*
 ******************************************************************************
 * Function Name    :   NetIpv6InvokeMCastApplReceive
 * Description      :   This function invokes the registered application
 *                      receive call back function.
 * Inputs           :   pIp6Cxt  - Ip6Cxt information.
 *                      pBuf     - Buffer Pointer
 * Outputs          :   None
 * Return Value     :   NETIPV6_SUCCESS | NETIPV6_FAILURE
 *******************************************************************************
 */

INT4
NetIpv6InvokeMCastApplReceive (tIp6Cxt * pIp6Cxt, tCRU_BUF_CHAIN_HEADER * pBuf)
{
    INT1                u1Index;
    tCRU_BUF_CHAIN_HEADER *pDupBuf = NULL;

    /* what ever protocols registered for getting the ipv6 multicast packets
     * should be given the multicast packets by duplicating the buffer.
     * So the protocols receiving this buffer should take care that they
     * are releasing the buffer not forcefully.
     */

    for (u1Index = 0; u1Index < IP6_MAX_MCAST_PROTOCOLS; u1Index++)
    {
        if ((gaNetIpv6MCastRegTable[u1Index].pAppRcv != NULL) &&
            (pIp6Cxt->au1HLMcastRegTable[u1Index] == NETIPV6_REG_ENABLE))
        {
            pDupBuf = CRU_BUF_Duplicate_BufChain (pBuf);

            if (pDupBuf != NULL)
            {
                gaNetIpv6MCastRegTable[u1Index].pAppRcv (pDupBuf);
            }
        }
    }

    /* The buffer which is duplicated will be released in the received module.
     * Here, we will release the actual buffer. It is a success case.
     */
    IP6_RELEASE_BUF (pBuf, FALSE);

    return NETIPV6_SUCCESS;

}

/*
 ******************************************************************************
 * Function Name    :   NetIpv6IsOurAddressInCxt
 * Description      :   Check wether the given address is our addess and
 *                      fetch the interface index of that address
 * Inputs           :   pAddr          - IPv6 Address
 * Outputs          :   pu4IfIndex     - Interface Index
 * Return Value     :   NETIPV6_SUCCESS | NETIPV6_FAILURE
 *******************************************************************************
 */

INT4
NetIpv6IsOurAddress (tIp6Addr * pAddr, UINT4 *pu4IfIndex)
{
    return (NetIpv6IsOurAddressInCxt (IP6_DEFAULT_CONTEXT, pAddr, pu4IfIndex));
}

/******************************************************************************
 *Description         : This functions gets the context id associated with the 
 *                      the given IPv6 Interface Index
 *
 * Input(s)           : u4IfIndex - Ipv6 Interface Index
 *
 * Output(s)          : *pu4ContextId - The VR to which the interface is mapped
 *
 * Returns            : IP6_SUCCESS or IP6_FAILURE
 ******************************************************************************/
INT4
NetIpv6GetCxtId (UINT4 u4IfIndex, UINT4 *pu4ContextId)
{
    INT4                i4RetVal = NETIPV6_FAILURE;
    if (IP6_TASK_LOCK () == SNMP_FAILURE)
    {
        IP6_GBL_TRC_ARG (NETIP6_MOD_TRC, DATA_PATH_TRC, NETIP6_NAME,
                         "NetIpv6GetCxtId : IP6 Task Lock Failed !!!\n");
        return i4RetVal;
    }

    if (Ip6GetCxtId (u4IfIndex, pu4ContextId) == IP6_SUCCESS)
    {
        i4RetVal = NETIPV6_SUCCESS;
    }
    IP6_TASK_UNLOCK ();
    return i4RetVal;
}

/******************************************************************************
 * Function Name    :   NetIpv6GetSrcAddrForDestAddr
 * Description      :   This function can be used by external modules to get 
 *                      the source address to use corresponding to a destination
 *                      address while transmitting a packet out
 * Inputs           :   u4ContextId - Context id
 * Outputs          :   pDstAddr    - IPv6 destination address pointer
 *                      pSrcAddr    - IPv6 source address pointer
 * Return Value     :   NETIPV6_SUCCESS or NETIPV6_FAILURE
 *******************************************************************************/
INT4
NetIpv6GetSrcAddrForDestAddr (UINT4 u4ContextId, tIp6Addr * pDstAddr,
                              tIp6Addr * pSrcAddr)
{
    INT4                i4RetVal = NETIPV6_FAILURE;
    IP6_TASK_LOCK ();
    i4RetVal = Ip6SrcAddrForDestAddrInCxt (u4ContextId, pDstAddr, pSrcAddr);
    IP6_TASK_UNLOCK ();
    return i4RetVal;
}

/******************************************************************************
 * Function Name    :   NetIpv6NotifySelfIfOnLink
 * Description      :   This API is called by OSPF to intimate the onlink 
 *                      detection or modification of interfaces 
 * Inputs           :   u1ProtocolId - Protocol from which this API is called
 *                      pMultIfPtr  - Pointer to the message
 * Outputs          :   None
 * Return Value     :   NETIPV6_SUCCESS or NETIPV6_FAILURE
 *******************************************************************************/
VOID
NetIpv6NotifySelfIfOnLink (UINT1 u1ProtocolId, tIp6MultIfToIp6Link * pMultIfPtr)
{
    UNUSED_PARAM (u1ProtocolId);
    if (OsixSendToQ (SELF, (const UINT1 *) IP6_TASK_ONLINK_NOTIFY_QUEUE,
                     (tOsixMsg *) (VOID *) pMultIfPtr,
                     OSIX_MSG_NORMAL) == OSIX_FAILURE)
    {
        CRU_BUF_Release_MsgBufChain ((tOsixMsg *) (VOID *) pMultIfPtr, FALSE);

        return;
    }
    else
    {
        OsixSendEvent (SELF, (const UINT1 *) IP6_TASK_NAME,
                       IP6_ON_LINK_MSG_RECD_EVENT);
    }

    return;

}

/******************************************************************************
 * Function Name    :   NetIpv6GetIfIndexFromIp6Address
 * Description      :   This function can be used by external modules to get 
 *                      the Interface Index using the given IPv6 address 
 * Inputs           :   u4ContextId - Context id
 * Outputs          :   pIpAddr    - IPv6 address pointer
 * Return Value     :   NETIPV6_SUCCESS or NETIPV6_FAILURE
 *******************************************************************************/
INT4
NetIpv6GetIfIndexFromIp6Address (tSNMP_OCTET_STRING_TYPE * pIpAddressAddr,
                                 UINT4 *u4IfIndex)
{
    UNUSED_PARAM (pIpAddressAddr);
    UNUSED_PARAM (u4IfIndex);
    return NETIPV6_SUCCESS;
}

/**********************************************************************
 *   Function Name    :   NetIpv6SelectSrcAddrForDstAddr
 *   Description      :   This function can be used by external modules to
 *                        select the source address for the given destination
 *                        address by applying a set of rules
 *   Inputs           :   pIp6DestAddr    - IPv6 destination address pointer
 *                        pIf6            - IPv6 interface pointer
 *   Outputs          :   None
 *   Return Value     :   The address (tIp6Addr *) or NULL
 * ********************************************************************/

tIp6Addr           *
NetIpv6SelectSrcAddrForDstAddr (tIp6Addr * pIp6DestAddr, tIp6If * pIf6)
{

    tTMO_SLL            srcaddrlist;
    tIp6Addr           *pFinalSrcAddr = NULL;
    tIp6SrcAddr        *pSrcAddrInfo = NULL;
    tIp6SrcAddr        *pTempSrcAddrInfo = NULL;
    tIp6SrcAddr        *pSortedSrcInfo = NULL;
    tIp6AddrSelPolicy  *pPolicyPrefixInfo = NULL;
    tIp6AddrSelPolicy   PolicyPrefixInfo;
    tTMO_SLL_NODE      *pNext = NULL;
    tTMO_SLL_NODE      *pCur = NULL;
    INT4                i4Ipv6AddrStatus = 0;
    UINT1               u1SrcLabel = 0;
    UINT1               u1DstLabel = 0;
    UINT1               u1MaxMatchLen = 0;
    UINT1               u1LocalScore = 0;
    UINT1               u1SrcScope;
    UINT1               u1DestScope;
    tSNMP_OCTET_STRING_TYPE Addr;
    UINT1               au1AddrOctetList[IP6_ADDR_SIZE];
    tNetIpv6RtInfo      NetIpv6RtInfo;
    tNd6CacheEntry     *pNd6c = NULL;

    IP6_TASK_LOCK ();

    Ipv6FormSrcAddrList (&srcaddrlist, pIf6);

    /* Rule 1: Prefer same address (Prefer rule)
       Check whether both the addresses are equal,
       If equal,increment the local score by 1
       Else increment the local score by 0 */
    TMO_SLL_Scan (&srcaddrlist, pSrcAddrInfo, tIp6SrcAddr *)
    {
        u1LocalScore = Ip6AddrEqual (*pIp6DestAddr, pSrcAddrInfo->srcaddr);
        pSrcAddrInfo->u1CumulativeScore += u1LocalScore;
    }
    /*Rule 2:   Prefer same scope (Reject rule)
       Find the scope of the source address
       Find the scope of the destination address
       If both the scopes match, increment the local score by 1
       Else increment the local score by 0
       Delete the entry from the source address list */

    TMO_DYN_SLL_Scan (&srcaddrlist, pSrcAddrInfo, pTempSrcAddrInfo,
                      tIp6SrcAddr *)
    {
        u1SrcScope = Ip6GetAddrScope (&pSrcAddrInfo->srcaddr);
        u1DestScope = Ip6GetAddrScope (pIp6DestAddr);
        if (u1SrcScope >= u1DestScope)
        {
            u1LocalScore = 1;
        }
        else
        {
            u1LocalScore = 0;
        }
        if (u1LocalScore == 0)
        {

            TMO_SLL_Delete (&srcaddrlist, &(pSrcAddrInfo->SrcNode));
            Ip6RelMem (IP6_DEFAULT_CONTEXT,
                       (UINT2) gIp6GblInfo.i4Ip6SrcAddrListId,
                       (UINT1 *) pSrcAddrInfo);

            pSrcAddrInfo = pTempSrcAddrInfo;
        }
        else
        {
            pSrcAddrInfo->u1CumulativeScore += u1LocalScore;
        }
    }

    /*Rule 3: Avoid deprecated addresses 
       Get the address status of the address
       If its preferred, increment local score by 2
       If its deprecated, increment local score by 1
       Else increment the local score by 0 */

    TMO_SLL_Scan (&srcaddrlist, pSrcAddrInfo, tIp6SrcAddr *)
    {
        MEMSET (au1AddrOctetList, IP6_ZERO, IP6_ADDR_SIZE);
        MEMSET (&Addr, IP6_ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));

        Addr.pu1_OctetList = au1AddrOctetList;

        MEMCPY (Addr.pu1_OctetList, &pSrcAddrInfo->srcaddr, 16);
        Addr.i4_Length = 16;

        Ip6GetIpv6AddrStatus (pSrcAddrInfo->u4IfIndex, &Addr,
                              &i4Ipv6AddrStatus);
        if (i4Ipv6AddrStatus == IP6_ADDR_STAT_DEPRECATE)
        {
            u1LocalScore = 1;
        }
        else if (i4Ipv6AddrStatus == IP6_ADDR_STAT_PREFERRED)
        {
            u1LocalScore = 2;

        }
        else
        {
            u1LocalScore = 0;
        }

        pSrcAddrInfo->u1CumulativeScore += u1LocalScore;
    }

    /* Rule 4: Prefer the same outgoing interface*
       If both the interfaces matches, 
       then increment the local score by 1
       Else increment the local score by 0 */
    TMO_SLL_Scan (&srcaddrlist, pSrcAddrInfo, tIp6SrcAddr *)
    {
        if (pSrcAddrInfo->u4IfIndex == pIf6->u4Index)
        {
            u1LocalScore = 1;
        }
        else
        {
            u1LocalScore = 0;
        }
        pSrcAddrInfo->u1CumulativeScore += u1LocalScore;
    }

    /* Rule 5 : Prefer matching label (Prefer rule)
       Find the source address label by scanning the RBTree
       If the label is not present,
       set the label to the default value
       Find the destination address label by scanning the RBTree
       If the label is not present, 
       set the label to the default value
       If both the labels are present, increment the local score by 1
       Else increment the local score by 0 */

    /*Find the destination  address label
       If the entry is present, get the corresponding label
       using the default compare function
       Else get the label of the longest matching prefix
       using the new compare function */
    MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));
    pIf6 = Ip6GetRouteInCxt (pIf6->pIp6Cxt->u4ContextId, pIp6DestAddr,
                             &pNd6c, &NetIpv6RtInfo);

    UNUSED_PARAM (pIf6);
    MEMSET (&PolicyPrefixInfo, 0, sizeof (tIp6AddrSelPolicy));
    Ip6AddrCopy (&(PolicyPrefixInfo.Ip6PolicyPrefix), pIp6DestAddr);
    PolicyPrefixInfo.u1PrefixLen = NetIpv6RtInfo.u1Prefixlen;
    pPolicyPrefixInfo = RBTreeGetNext (gIp6GblInfo.PolicyPrefixTree,
                                       (tRBElem *) & PolicyPrefixInfo, NULL);

    if (pPolicyPrefixInfo != NULL)
    {
        u1DstLabel = pPolicyPrefixInfo->u1Label;
    }
    else
    {
        u1DstLabel = 1;            /*Default label */
    }

    /*Find the source label */
    TMO_SLL_Scan (&srcaddrlist, pSrcAddrInfo, tIp6SrcAddr *)
    {
        Ip6AddrCopy (&(PolicyPrefixInfo.Ip6PolicyPrefix),
                     &pSrcAddrInfo->srcaddr);
        pPolicyPrefixInfo = RBTreeGetNext
            (gIp6GblInfo.PolicyPrefixTree,
             (tRBElem *) & PolicyPrefixInfo, NULL);
        if (pPolicyPrefixInfo != NULL)
        {
            u1SrcLabel = pPolicyPrefixInfo->u1Label;
        }
        else
        {
            u1SrcLabel = 1;        /*Default label */

        }
        if (u1SrcLabel == u1DstLabel)
        {
            u1LocalScore = 1;
        }
        else
        {
            u1LocalScore = 0;
        }
        pSrcAddrInfo->u1CumulativeScore += u1LocalScore;
    }

    /* Rule 6: Prefer public addresses (Prefer rule)
       Find whether the address is an public address or not
       If it is a publio address, increment the local score by 1
       Else increment the local score by 0 */
    TMO_SLL_Scan (&srcaddrlist, pSrcAddrInfo, tIp6SrcAddr *)
    {
        u1LocalScore = Ip6IsAddrPublic (pSrcAddrInfo->srcaddr);
        pSrcAddrInfo->u1CumulativeScore += u1LocalScore;
    }

    /* Rule  7:Prefer longest matching prefix
       Find the longest matching prefix using the IP6AddrDiff function
       Increment the local score by 1, for the longest matching prefix
       Else increment the local score by 0 */
    TMO_SLL_Scan (&srcaddrlist, pSrcAddrInfo, tIp6SrcAddr *)
    {
        pSrcAddrInfo->u1MatchLen =
            Ip6AddrDiff (&pSrcAddrInfo->srcaddr, pIp6DestAddr);
        if (pSrcAddrInfo->u1MatchLen > u1MaxMatchLen)
        {
            u1MaxMatchLen = pSrcAddrInfo->u1MatchLen;
        }
        else
        {
            continue;
        }
    }
    TMO_SLL_Scan (&srcaddrlist, pSrcAddrInfo, tIp6SrcAddr *)
    {
        if (pSrcAddrInfo->u1MatchLen == u1MaxMatchLen)
        {
            u1LocalScore = 1;
        }
        else
        {
            u1LocalScore = 0;
        }
        pSrcAddrInfo->u1CumulativeScore += u1LocalScore;
    }

    /* Sort the source address list and store it in sorted list
       Search the sorted list for an unique cumulative score
       If there is unique cumulative score, 
       return the first element of the list
       Else continue with the next rule */

    Ipv6SortSrcAddrList (&srcaddrlist);
    MEMSET (gau1SrcAddr, IP6_ZERO, IP6_ADDR_SIZE);
    pFinalSrcAddr = (tIp6Addr *) (VOID *) gau1SrcAddr;
    pSortedSrcInfo = (tIp6SrcAddr *) TMO_SLL_First (&srcaddrlist);
    if (pSortedSrcInfo != NULL)
    {
        MEMCPY (pFinalSrcAddr, &pSortedSrcInfo->srcaddr, sizeof (tIp6Addr));

        pNext = TMO_SLL_First (&srcaddrlist);
        while ((pCur = pNext) != NULL)
        {
            pNext = TMO_SLL_Next (&srcaddrlist, pCur);

            TMO_SLL_Delete (&srcaddrlist, pCur);

            Ip6RelMem (IP6_DEFAULT_CONTEXT,
                       (UINT2) gIp6GblInfo.i4Ip6SrcAddrListId, (UINT1 *) pCur);
        }
        IP6_TASK_UNLOCK ();
        return (pFinalSrcAddr);
    }
    else
    {
        IP6_TASK_UNLOCK ();
        return NULL;
    }
}

/****************************************************************************
 *   Function Name    :   NetIpv6SelectDestAddr
 *   Description      :   This function can be used by external modules 
 *                        to select  the destinaition address by
 *                         applying a set of rules
 *   Inputs           :   pDstInfo    - IPv6 destination info pointer
 *   Outputs          :   None
 *   Return Value     :   The required destination address
 *
 * **************************************************************************/

INT4
NetIpv6SelectDestAddr (tIp6DstInfo ** pDstInfo, UINT1 u1Count)
{
    tIp6DstAddr        *pDstAddrInfo = NULL;
    tIp6DstAddr        *pMaxPrecdence = NULL;
    tTMO_SLL            dstaddrlist;
    tIp6AddrSelPolicy  *pPolicyPrefixInfo = NULL;
    tNd6CacheEntry     *pNd6c = NULL;
    tIp6Addr           *pSrcaddr = NULL;
    tIp6AddrSelPolicy   PolicyPrefixInfo;
    tNetIpv6RtInfo      NetIpv6Rt;
    tIp6If             *pIf6 = NULL;
    tIp6Addr           *pNextHop = NULL;
    tTMO_SLL_NODE      *pNext = NULL;
    tTMO_SLL_NODE      *pCur = NULL;
    tSNMP_OCTET_STRING_TYPE Addr;
    INT4                i4Ipv6AddrStatus;
    INT4                i4Status = 0;
    UINT1               u1LocalScore = 0;
    UINT1               u1MinScope = 0xff;
    UINT1               u1MaxPrecedence = 0;
    UINT1               u1MaxMatchLen = 0;
    UINT1               u1SrcScope;
    UINT1               u1DestScope;
    UINT1               u1SrcLabel = 0;
    UINT1               u1DstLabel = 0;
    UINT1               au1AddrOctetList[IP6_ADDR_SIZE];
    UINT1               u1Type = 0;
    UINT1               u1RtType = 0;
    UINT1               u1Index = 0;

    IP6_TASK_LOCK ();

    TMO_SLL_Init (&dstaddrlist);

    for (; u1Index < u1Count; u1Index++)
    {

        pDstAddrInfo =
            (tIp6DstAddr *) (VOID *) Ip6GetMem (IP6_DEFAULT_CONTEXT,
                                                (UINT2) gIp6GblInfo.
                                                i4Ip6DstAddrListId);
        if (pDstAddrInfo == NULL)
        {
            IP6_TASK_UNLOCK ();
            return NETIPV6_FAILURE;
        }

        MEMCPY (&(pDstAddrInfo->dstaddr),
                &(pDstInfo[u1Index]->dstaddr), sizeof (tIp6Addr));

        pDstAddrInfo->u4IfIndex = pDstInfo[u1Index]->u4IfIndex;
        pDstAddrInfo->u1PrefixLen = pDstInfo[u1Index]->u1PrefixLen;
        pDstAddrInfo->u1CumulativeScore = 0;
        pDstAddrInfo->u1Precedence = 0;
        pDstAddrInfo->u1Scope = 0;

        TMO_SLL_Add (&dstaddrlist, &(pDstAddrInfo->DstNode));

    }

    /* Rule 1 :Avoid unreachable destinations (Reject rule) 
       Get the state of the address from the neighbor cache table
       If it is reachable, increment the local score by 1, else
       increment the local score by 0 */

    MEMSET (&NetIpv6Rt, 0, sizeof (tNetIpv6RtInfo));
    TMO_SLL_Scan (&dstaddrlist, pDstAddrInfo, tIp6DstAddr *)
    {
        u1Type = Ip6AddrType (&pDstAddrInfo->dstaddr);
        if (u1Type != ADDR6_LLOCAL)
        {
            /*Perfom route lookup for global addresses */
            i4Status = Ip6RtLookupInCxt (IP6_DEFAULT_CONTEXT,
                                         (&pDstAddrInfo->dstaddr), &NetIpv6Rt);
            if (i4Status == IP6_FAILURE)
            {
                /* If route exists, check in neighbour cache table */
                pNextHop = &(NetIpv6Rt.NextHop);
                u1RtType = NetIpv6Rt.i1Type;
                pIf6 = IP6_INTERFACE (pDstAddrInfo->u4IfIndex);
                if (pIf6 != NULL)
                {
                    pNd6c = Nd6LookupCache (pIf6, (u1RtType == DIRECT) ?
                                            &pDstAddrInfo->dstaddr : pNextHop);
                    if (pNd6c != NULL)
                    {
                        if (Nd6GetReachState (pNd6c) == ND6C_REACHABLE)
                        {
                            /*If it is reachable, update the local score by 1 */
                            u1LocalScore = 1;
                            pDstAddrInfo->u1CumulativeScore += u1LocalScore;
                            continue;
                        }
                    }
                }
                /*Destination is not reachable. Update the local score by 0 */
                u1LocalScore = 0;
                pDstAddrInfo->u1CumulativeScore += u1LocalScore;
            }
            else
            {
                /*Route does not exists. Update the local score by 0 */
                u1LocalScore = 0;
                pDstAddrInfo->u1CumulativeScore += u1LocalScore;
            }
        }
        else
        {
            /*Perfom lookup in neighbour cache table for link local address */
            pIf6 = IP6_INTERFACE (pDstAddrInfo->u4IfIndex);
            if (pIf6 != NULL)
            {
                pNd6c = Nd6LookupCache (pIf6, &pDstAddrInfo->dstaddr);
                if (pNd6c != NULL)
                {
                    if (Nd6GetReachState (pNd6c) == ND6C_REACHABLE)
                    {
                        /*If it is reachable, update the local score by 1 */
                        u1LocalScore = 1;
                        pDstAddrInfo->u1CumulativeScore += u1LocalScore;
                        continue;
                    }
                }
                /*Destination is not reachable. Update the local score by 0 */
                u1LocalScore = 0;
                pDstAddrInfo->u1CumulativeScore += u1LocalScore;
            }
        }
    }

    /* Rule 2: Prefer matching scope ( Reject rule)
       Find the scope of the source address
       Find the scope of the destination address
       If both the scopes match, increment the local score by 1
       Else increment the local score by 0
       Delete the entry from the destination address list */

    TMO_SLL_Scan (&dstaddrlist, pDstAddrInfo, tIp6DstAddr *)
    {

        pIf6 = IP6_INTERFACE (pDstAddrInfo->u4IfIndex);
        pSrcaddr = Ip6AddrGetSrc (pIf6, &pDstAddrInfo->dstaddr);
        if (pSrcaddr == NULL)
        {
            IP6_TASK_UNLOCK ();
            return NETIPV6_FAILURE;
        }
        u1SrcScope = Ip6GetAddrScope (pSrcaddr);
        u1DestScope = Ip6GetAddrScope (&pDstAddrInfo->dstaddr);

        if (u1DestScope >= u1SrcScope)
        {
            u1LocalScore = 1;
        }
        else
        {
            u1LocalScore = 0;
        }
        pDstAddrInfo->u1CumulativeScore += u1LocalScore;
    }

    /* Rule 3: Avoid deprecated addresses (Prefer rule)
       Get the address status of the address
       If its preferred, increment local score by 2
       If its deprecated, increment local score by 1
       Else increment the local score by 0 */

    TMO_SLL_Scan (&dstaddrlist, pDstAddrInfo, tIp6DstAddr *)
    {

        MEMSET (au1AddrOctetList, IP6_ZERO, IP6_ADDR_SIZE);
        MEMSET (&Addr, IP6_ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));

        Addr.pu1_OctetList = au1AddrOctetList;

        MEMCPY (Addr.pu1_OctetList, &pDstAddrInfo->dstaddr, 16);
        Addr.i4_Length = 16;

        Ip6GetIpv6AddrStatus (pDstAddrInfo->u4IfIndex, &Addr,
                              &i4Ipv6AddrStatus);

        if (i4Ipv6AddrStatus == IP6_ADDR_STAT_DEPRECATE)
        {
            u1LocalScore = 1;
        }
        else if (i4Ipv6AddrStatus == IP6_ADDR_STAT_PREFERRED)
        {
            u1LocalScore = 2;
        }
        else
        {
            u1LocalScore = 0;
        }
        pDstAddrInfo->u1CumulativeScore += u1LocalScore;
    }

    /* Rule 4 : Prefer Matching label (Prefer rule) 
       Find the source address using IP6AddrGetSrc & get the corresponding
       label entry from the policy prefix tree
       If the label is not present, set the label to the default value
       Find the destination address label by scanning the RBTree
       If the label is not present, set the label to the default value
       If both the labels are present and they match, 
       increment the local score by 1
       Else increment the local score by 0 */

    TMO_SLL_Scan (&dstaddrlist, pDstAddrInfo, tIp6DstAddr *)
    {

        /*Find the source address label
           If the entry is present, get the corresponding label
           using the default compare function 
           Else get the label of the longest matching prefix
           using the new compare function */
        MEMSET (&PolicyPrefixInfo, 0, sizeof (PolicyPrefixInfo));
        pIf6 = IP6_INTERFACE (pDstAddrInfo->u4IfIndex);
        pSrcaddr = Ip6AddrGetSrc (pIf6, &pDstAddrInfo->dstaddr);
        if (pSrcaddr == NULL)
        {
            IP6_TASK_UNLOCK ();
            return NETIPV6_FAILURE;
        }
        Ip6AddrCopy (&(PolicyPrefixInfo.Ip6PolicyPrefix), pSrcaddr);
        PolicyPrefixInfo.u1PrefixLen = pDstAddrInfo->u1PrefixLen;
        PolicyPrefixInfo.u4IfIndex = pDstAddrInfo->u4IfIndex;

        pPolicyPrefixInfo = RBTreeGetNext
            (gIp6GblInfo.PolicyPrefixTree,
             (tRBElem *) & PolicyPrefixInfo, NULL);

        if (pPolicyPrefixInfo != NULL)
        {
            u1SrcLabel = pPolicyPrefixInfo->u1Label;
        }
        else
        {
            u1SrcLabel = 1;        /*Default label */
        }
        /*Find the destination  address label
           If the entry is present, get the corresponding label
           using the default compare function
           Else get the label of the longest matching prefix
           using the new compare function */

        Ip6AddrCopy (&(PolicyPrefixInfo.Ip6PolicyPrefix),
                     &pDstAddrInfo->dstaddr);

        pPolicyPrefixInfo = RBTreeGetNext
            (gIp6GblInfo.PolicyPrefixTree,
             (tRBElem *) & PolicyPrefixInfo, NULL);
        if (pPolicyPrefixInfo != NULL)
        {
            u1DstLabel = pPolicyPrefixInfo->u1Label;
        }
        else
        {
            u1DstLabel = 1;        /*Default label */
        }
        if (u1SrcLabel == u1DstLabel)
        {
            u1LocalScore = 1;
        }
        else
        {
            u1LocalScore = 0;
        }
        pDstAddrInfo->u1CumulativeScore += u1LocalScore;
    }

    /* Rule 5:Prefer higher precedence (Prefer rule)
       Find the destination address precedence  by scanning the RBTree
       Find the destination with highest precdence.
       increment the local score by 1
       Else increment the local score by 0 */

    TMO_SLL_Scan (&dstaddrlist, pDstAddrInfo, tIp6DstAddr *)
    {

        MEMSET (&PolicyPrefixInfo, 0, sizeof (PolicyPrefixInfo));
        Ip6AddrCopy (&(PolicyPrefixInfo.Ip6PolicyPrefix),
                     &pDstAddrInfo->dstaddr);
        PolicyPrefixInfo.u1PrefixLen = pDstAddrInfo->u1PrefixLen;
        PolicyPrefixInfo.u4IfIndex = pDstAddrInfo->u4IfIndex;
        pPolicyPrefixInfo = RBTreeGet
            (gIp6GblInfo.PolicyPrefixTree, (tRBElem *) & PolicyPrefixInfo);
        if (pPolicyPrefixInfo != NULL)
        {
            pDstAddrInfo->u1Precedence = pPolicyPrefixInfo->u1Precedence;
        }
        else
        {
            pDstAddrInfo->u1Precedence = 40;    /*Default precedence */
        }
        if (pDstAddrInfo->u1Precedence >= u1MaxPrecedence)
        {
            u1MaxPrecedence = pDstAddrInfo->u1Precedence;
            pMaxPrecdence = pDstAddrInfo;
        }
        else
        {
            continue;
        }
    }
    if (pMaxPrecdence != NULL)
    {
        pMaxPrecdence->u1CumulativeScore += 1;
    }

    /* Rule 6 : Prefer smallest scope (Prefer rule)
       Find the smallest scope in the destination address
       list and  increment the local score by 1
       For other addresses, increment the local score by 0 */

    TMO_SLL_Scan (&dstaddrlist, pDstAddrInfo, tIp6DstAddr *)
    {

        pDstAddrInfo->u1Scope = Ip6GetAddrScope (&pDstAddrInfo->dstaddr);
        if (pDstAddrInfo->u1Scope < u1MinScope)
        {
            u1MinScope = pDstAddrInfo->u1Scope;

        }
        else
        {
            continue;
        }
    }
    TMO_SLL_Scan (&dstaddrlist, pDstAddrInfo, tIp6DstAddr *)
    {
        if (pDstAddrInfo->u1Scope == u1MinScope)
        {
            u1LocalScore = 1;
        }
        else
        {
            u1LocalScore = 0;
        }
        pDstAddrInfo->u1CumulativeScore += u1LocalScore;
    }

    /*Rule 7: Prefer native transport (Prefer rule) 
       Find the interface of the destination address
       If it is a tunnel interface, increment the local score by 0
       Else if is an ethernet interface,
       increment the local score by 1 */

    TMO_SLL_Scan (&dstaddrlist, pDstAddrInfo, tIp6DstAddr *)
    {

        pIf6 = IP6_INTERFACE (pDstAddrInfo->u4IfIndex);
        if (IP6_IF_TYPE (pIf6) == IP6_TUNNEL_INTERFACE_TYPE)
        {
            u1LocalScore = 0;
        }
        else
        {

            u1LocalScore = 1;
        }
        pDstAddrInfo->u1CumulativeScore += u1LocalScore;
    }

    /* Rule 8: Prefer longest matching prefix 
       Find the longest matching prefix using the IP6AddrDiff function
       Increment the local score by 1, for the longest matching prefix
       Else increment the local score by 0 */
    TMO_SLL_Scan (&dstaddrlist, pDstAddrInfo, tIp6DstAddr *)
    {

        pDstAddrInfo->u1MatchLen =
            Ip6AddrDiff (&pDstAddrInfo->dstaddr, pSrcaddr);
        if (pDstAddrInfo->u1MatchLen > u1MaxMatchLen)
        {
            u1MaxMatchLen = pDstAddrInfo->u1MatchLen;
        }
        else
        {
            continue;
        }
    }
    TMO_SLL_Scan (&dstaddrlist, pDstAddrInfo, tIp6DstAddr *)
    {
        if (pDstAddrInfo->u1MatchLen == u1MaxMatchLen)
        {
            u1LocalScore = 1;
        }
        else
        {
            u1LocalScore = 0;
        }
        pDstAddrInfo->u1CumulativeScore += u1LocalScore;
    }
    /* Sort the destination address list and store it in sorted list
       Search the sorted list for an unique cumulative score
       If there is unique cumulative score, return the address
       Else continue with the next rule */

    Ipv6SortDstAddrList (&dstaddrlist);

    u1Index = 0;
    TMO_SLL_Scan (&dstaddrlist, pDstAddrInfo, tIp6DstAddr *)
    {
        MEMCPY (&(pDstInfo[u1Index]->dstaddr),
                &(pDstAddrInfo->dstaddr), sizeof (tIp6Addr));

        pDstInfo[u1Index]->u4IfIndex = pDstAddrInfo->u4IfIndex;
        pDstInfo[u1Index]->u1CumulativeScore = pDstAddrInfo->u1CumulativeScore;
        u1Index++;

    }

    pNext = TMO_SLL_First (&dstaddrlist);
    while ((pCur = pNext) != NULL)
    {
        pNext = TMO_SLL_Next (&dstaddrlist, pCur);

        TMO_SLL_Delete (&dstaddrlist, pCur);

        Ip6RelMem (IP6_DEFAULT_CONTEXT, (UINT2) gIp6GblInfo.i4Ip6DstAddrListId,
                   (UINT1 *) pCur);
    }

    IP6_TASK_UNLOCK ();
    return NETIPV6_SUCCESS;

}

/******************************************************************************
 * Function Name    :   NetIpv6ValidateRpPrefixInCxt
 * Description      :   This function can be used by external modules 
 *                      to validate the RP address
 *                      embedded in the given multicast ipv6 address.
 * Inputs           :   u4ContextId - Context id
 *                      u4IfIndex   - Interface index
 *                      pMcastAddr  - IPv6 multicast address 
 * Outputs          :   pIp6RpAddrInfo  - RP Address related information
 * Return Value     :   NETIPV6_SUCCESS or NETIPV6_FAILURE
 *******************************************************************************/
INT4
NetIpv6ValidateRpPrefixInCxt (UINT4 u4ContextId, UINT4 u4IfIndex,
                              tIp6Addr * pMcastAddr)
{
    tIp6Addr            Prefix;
    tIp6If             *pIf6 = NULL;
    UINT1               u1EmbdRpFlag = 0;
    UINT1               u1PrefixLen = 0;

    IP6_TASK_LOCK ();

    pIf6 = Ip6ifGetEntry (u4IfIndex);
    if (pIf6 == NULL)
    {
        IP6_TASK_UNLOCK ();
        IP6_TRC_ARG (u4ContextId, NETIP6_MOD_TRC, DATA_PATH_TRC, NETIP6_NAME,
                     "NetIpv6GetIfInfo : Invalid Interface Index !!!\n");
        return NETIPV6_FAILURE;
    }

    u1PrefixLen = (UINT1) IP6_GET_UNICAST_PREFIXLEN_FROM_MCASTADDR (pMcastAddr);
    if (u1PrefixLen > 64)
    {
        IP6_TASK_UNLOCK ();
        IP6_TRC_ARG (u4ContextId, NETIP6_MOD_TRC, DATA_PATH_TRC, NETIP6_NAME,
                     "NetIpv6GetIfInfo : Invalid Prefix Length !!!\n");
        return NETIPV6_FAILURE;
    }

    if (Ip6UtlConstructRpAddr (pMcastAddr, &Prefix, pIf6->ifaceTok, u1PrefixLen,
                               IP6_ADDR_RP_STATIC) != NETIPV6_SUCCESS)
    {
        IP6_TASK_UNLOCK ();
        IP6_TRC_ARG (u4ContextId, NETIP6_MOD_TRC, DATA_PATH_TRC, NETIP6_NAME,
                     "NetIpv6GetIfInfo : Invalid RP Address !!!\n");
        return NETIPV6_FAILURE;
    }

    if (IS_ADDR_LLOCAL (Prefix))
    {
        IP6_TASK_UNLOCK ();
        IP6_TRC_ARG (u4ContextId, NETIP6_MOD_TRC, DATA_PATH_TRC, NETIP6_NAME,
                     "NetIpv6GetIfInfo : Link local address !!!\n");
        return NETIPV6_FAILURE;
    }

    if (Ip6UtlCheckIfForEmbdRp (pIf6, &Prefix, u1PrefixLen, &u1EmbdRpFlag)
        != NETIPV6_SUCCESS)
    {
        IP6_TASK_UNLOCK ();
        IP6_TRC_ARG (u4ContextId, NETIP6_MOD_TRC, DATA_PATH_TRC, NETIP6_NAME,
                     "NetIpv6GetIfInfo : Invalid interface !!!\n");
        return NETIPV6_FAILURE;
    }

    if (u1EmbdRpFlag != IP6_TRUE)
    {
        IP6_TASK_UNLOCK ();
        IP6_TRC_ARG (u4ContextId, NETIP6_MOD_TRC, DATA_PATH_TRC, NETIP6_NAME,
                     "NetIpv6GetIfInfo : Embedded flag not enbled !!!\n");
        return NETIPV6_FAILURE;
    }

    IP6_TASK_UNLOCK ();

    UNUSED_PARAM (u4ContextId);
    return NETIPV6_SUCCESS;
}

/******************************************************************************
 * Function Name    :   NetIpv6GetUnicastRpPrefix
 * Description      :   This function can be used by external modules 
 *                      to validate and retrieve the RP address
 *                      embedded in the given multicast ipv6 address.
 * Inputs           :   u4ContextId - Context id
 *                      u4IfIndex   - Interface index
 *                      pMcastAddr  - IPv6 multicast address 
 * Outputs          :   pIp6RpAddrInfo  - RP Address related information
 * Return Value     :   NETIPV6_SUCCESS or NETIPV6_FAILURE
 *******************************************************************************/
INT4
NetIpv6GetUnicastRpPrefix (UINT4 u4ContextId, tIp6Addr * pMcastAddr,
                           tIp6RpAddrInfo * pIp6RpAddrInfo)
{
    tIp6Addr            Prefix;
    UINT4               u4OutIfIndex = 0;
    UINT1               u1PrefixLen = 0;

    IP6_TASK_LOCK ();

    u1PrefixLen = (UINT1) IP6_GET_UNICAST_PREFIXLEN_FROM_MCASTADDR (pMcastAddr);
    if (u1PrefixLen > 64)
    {
        IP6_TASK_UNLOCK ();
        IP6_TRC_ARG (u4ContextId, NETIP6_MOD_TRC, DATA_PATH_TRC, NETIP6_NAME,
                     "NetIpv6GetIfInfo : Invalid Prefix Length !!!\n");
        return NETIPV6_FAILURE;
    }

    if (Ip6UtlConstructRpAddr (pMcastAddr, &Prefix, NULL, u1PrefixLen,
                               IP6_ADDR_RP_DYNAMIC) != NETIPV6_SUCCESS)
    {
        IP6_TASK_UNLOCK ();
        IP6_TRC_ARG (u4ContextId, NETIP6_MOD_TRC, DATA_PATH_TRC, NETIP6_NAME,
                     "NetIpv6GetIfInfo : Invalid RP Address !!!\n");
        return NETIPV6_FAILURE;
    }
    /*Retrieving the route information */
    if (IS_ADDR_LLOCAL (Prefix))
    {
        IP6_TASK_UNLOCK ();
        IP6_TRC_ARG (u4ContextId, NETIP6_MOD_TRC, DATA_PATH_TRC, NETIP6_NAME,
                     "NetIpv6GetIfInfo : Address Link local !!!\n");
        return NETIPV6_FAILURE;
    }

    if (Ip6UtlCheckRouteForEmbdRpInCxt (u4ContextId, &Prefix, &u4OutIfIndex)
        != NETIPV6_SUCCESS)
    {
        IP6_TASK_UNLOCK ();
        IP6_TRC_ARG (u4ContextId, NETIP6_MOD_TRC, DATA_PATH_TRC, NETIP6_NAME,
                     "NetIpv6GetIfInfo : Route does not exist !!!\n");
        return NETIPV6_FAILURE;
    }

    if (Ip6UtlFillRpInfo (&Prefix, u4OutIfIndex, IP6_FALSE, pIp6RpAddrInfo)
        != NETIPV6_SUCCESS)
    {
        IP6_TASK_UNLOCK ();
        IP6_TRC_ARG (u4ContextId, NETIP6_MOD_TRC, DATA_PATH_TRC, NETIP6_NAME,
                     "NetIpv6GetIfInfo : Could not copy the RP details !!!\n");
        return NETIPV6_FAILURE;
    }

    IP6_TASK_UNLOCK ();
    UNUSED_PARAM (u4ContextId);
    return NETIPV6_SUCCESS;
}

/******************************************************************************
 * Function Name    :   NetIpv6ValidateUnicastPrefix
 * Description      :   This function can be used to validate 
 *                      the Unicast IPv6 prefix embedded in the 
 *                      given multicast ipv6 address.
 * Inputs           :   u4ContextId - Context id
 *                      u4IfIndex   - Interface index
 *                      pMcastAddr  - IPv6 multicast address 
 * Outputs          :   None
 * Return Value     :   NETIPV6_SUCCESS or NETIPV6_FAILURE
 *******************************************************************************/
INT4
NetIpv6ValidateUnicastPrefix (UINT4 u4ContextId, UINT4 u4IfIndex,
                              tIp6Addr * pMcastAddr)
{
    tIp6Addr            Prefix;
    tIp6If             *pIf6 = NULL;
    UINT4               u4OutIfIndex = 0;
    UINT1               u1PrefixLen = 0;
    UINT1               u1EmbdRpFlag = 0;

    IP6_TASK_LOCK ();

    pIf6 = Ip6ifGetEntry (u4IfIndex);

    if (pIf6 == NULL)
    {
        IP6_TASK_UNLOCK ();
        IP6_TRC_ARG (u4ContextId, NETIP6_MOD_TRC, DATA_PATH_TRC, NETIP6_NAME,
                     "NetIpv6GetIfInfo : Invalid Interface Index !!!\n");
        return NETIPV6_FAILURE;
    }

    if (!IS_ALL_PREFIX_MULTI (pMcastAddr))
    {
        IP6_TASK_UNLOCK ();
        IP6_TRC_ARG (u4ContextId, NETIP6_MOD_TRC, DATA_PATH_TRC, NETIP6_NAME,
                     "NetIpv6GetIfInfo : P bit is not set !!!\n");
        return NETIPV6_FAILURE;
    }

    u1PrefixLen = (UINT1) IP6_GET_UNICAST_PREFIXLEN_FROM_MCASTADDR (pMcastAddr);
    if (u1PrefixLen > 64)
    {
        IP6_TASK_UNLOCK ();
        IP6_TRC_ARG (u4ContextId, NETIP6_MOD_TRC, DATA_PATH_TRC, NETIP6_NAME,
                     "NetIpv6GetIfInfo : Invalid prefix length !!!\n");
        return NETIPV6_FAILURE;
    }

    MEMSET (&Prefix, 0, sizeof (tIp6Addr));
    MEMCPY (&Prefix, &(pMcastAddr->u4_addr[1]), IP6_EUI_ADDRESS_LEN);
    MEMCPY ((UINT1 *) (&Prefix) + IP6_EUI_ADDRESS_LEN,
            pIf6->ifaceTok, IP6_EUI_ADDRESS_LEN);

    if (IS_ADDR_LLOCAL (Prefix))
    {
        IP6_TASK_UNLOCK ();
        IP6_TRC_ARG (u4ContextId, NETIP6_MOD_TRC, DATA_PATH_TRC, NETIP6_NAME,
                     "NetIpv6GetIfInfo : Link local Address !!!\n");
        return NETIPV6_FAILURE;
    }
    if (Ip6UtlCheckIfForEmbdRp (pIf6, &Prefix, u1PrefixLen, &u1EmbdRpFlag)
        != NETIPV6_SUCCESS)
    {
        if (Ip6UtlCheckRouteForEmbdRpInCxt (u4ContextId, &Prefix, &u4OutIfIndex)
            != NETIPV6_SUCCESS)
        {
            IP6_TRC_ARG (u4ContextId, NETIP6_MOD_TRC, DATA_PATH_TRC,
                         NETIP6_NAME,
                         "NetIpv6GetIfInfo : Interface/Route not exist !!!\n");
            IP6_TASK_UNLOCK ();
            return NETIPV6_FAILURE;
        }
    }
    IP6_TASK_UNLOCK ();
    UNUSED_PARAM (u4ContextId);
    return NETIPV6_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : This API gives the zone Index corresponding to the scope 
 *               configured on an interface when the application requests 
 *               for it.
 *
 * INPUTS      : scope     - Scope Configured on an interface
 *               u4IfIndex - Interface Index
 *
 * OUTPUT      : *pi4ZoneIndex - Pointer to the zone index.
 *               Valid zoneindex value if success else updated with
 *               invalid value
 *
 * RETURNS     :  Valid ZoneIndex (If success)
 *              or IP6_ZONE_INVALID(InValid ZoneIndex)
 *
 * NOTES       : This functions is added as a part of RFC4007 Implementation
 ******************************************************************************/
INT4
NetIpv6GetIfScopeZoneIndex (UINT1 u1Scope, UINT4 u4IfIndex)
{
    tIp6IfZoneMapInfo  *pIp6IfZoneMapInfo = NULL;
    tIp6If             *pIf6 = NULL;
    UINT4               u4ContextId = 0;

    VcmGetContextIdFromCfaIfIndex (u4IfIndex, &u4ContextId);

    if (IP6_TASK_LOCK () == SNMP_FAILURE)
    {
        IP6_TRC_ARG (u4ContextId, NETIP6_MOD_TRC, DATA_PATH_TRC, NETIP6_NAME,
                     "NetIpv6GetIfScopeZoneIndex: IP6 Task Lock Failed !!!\n");
        return (IP6_ZONE_INVALID);
    }

    pIf6 = IP6_INTERFACE (u4IfIndex);
    if (pIf6 == NULL)
    {
        IP6_TASK_UNLOCK ();
        return IP6_ZONE_INVALID;
    }

    IP6_TRC_ARG3 (pIf6->pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                  DATA_PATH_TRC, IP6_NAME,
                  "IP6ScopeZone:scope %d IF %d Context_id %d\n",
                  u1Scope, u4IfIndex, pIf6->pIp6Cxt->u4ContextId);

    pIp6IfZoneMapInfo = Ip6ZoneGetIfScopeZoneEntry (u4IfIndex, u1Scope);

    if (NULL != pIp6IfZoneMapInfo)
    {
        IP6_TASK_UNLOCK ();
        return (pIp6IfZoneMapInfo->i4ZoneIndex);
    }

    IP6_TASK_UNLOCK ();
    return (IP6_ZONE_INVALID);
}

/******************************************************************************
 * DESCRIPTION : This API is to get the scope info from ScopeZone-name
 *              interface with the given scope. 
 *              The application gives Scope and ScopeZone-name
 *
 * INPUTS      : ScopeZone-name- Scope name from which scope-zone info 
 *                               to be verified 
 *              
 *
 *
 * OUTPUT      : u1Scope - Comparison to be done for the given scope 
 *
 * RETURNS     : NETIPV6_SUCCESS - If zone exists on this interface
 *               or NETIPV6_FAILURE - If zone doesn't exist on this interface
 *  
 * NOTES       : This functions is added as a part of RFC4007 implementation
 *
 ******************************************************************************/

INT4
NetIpv6GetScopeId (UINT1 *pu1ZoneName, UINT1 *pu1Scope)
{
    INT4                i4ZoneId = ZERO;

    IP6_TASK_LOCK ();
    if (IP6_SUCCESS == Ip6ZoneGetZoneIdFromZoneName
        (pu1ZoneName, pu1Scope, &i4ZoneId))
    {
        IP6_TASK_UNLOCK ();
        return NETIPV6_SUCCESS;
    }

    IP6_TASK_UNLOCK ();
    return NETIPV6_FAILURE;

}

/************************************************************************
 * DESCRIPTION : This functions tests if the name and the zone id       *
 *               valid one or not.                                      *
 *                                                                      *
 * INPUTS      :                                                        *
 *               au1ScopeZone - Pointer to the Scope-Zone name,         *
 *                                                                      *
 * OUTPUTS     : *pu1ErrVal - the output error value are as follows     *
 *                NETIPV6_INVALID_ZONE_LEN - if the input len is        *
 *                     greater than max scope-zone name len             *
 *                NETIPV6_INVALID_ZONE_NAME - if the input name is      *
 *                                            invalid                *
 *                NETIPV6_INVALID_ZONE_ID - if the zone id in the name  *
 *                                          is invalid                  *
 * RETURNS     : NETIPV6_SUCCESS (If validation is successful)          *
 *               NETIPV6_FAILURE (If validation fails)                  *
 *                                                                      *
 * NOTES       : This function is added as a part of RFC4007 code       *
 *               changes                                                *
 ************************************************************************/

INT4
NetIpv6ValidateZoneName (UINT1 *pu1ScopeZone, UINT1 *pu1ErrVal)
{
    INT4                i4ZoneId = 0;
    INT4                i4ZoneNameLen = 0;
    UINT1               u1ScopeNameLen = 0;
    UINT1               u1Scope = 0;
    UINT1              *pu1KeyStr = NULL;

    IP6_TASK_LOCK ();

    *pu1ErrVal = 0;
    /*check if the scope-zone name len is not exceeding the max len */
    i4ZoneNameLen = (INT4) STRLEN (pu1ScopeZone);

    if (i4ZoneNameLen > IP6_SCOPE_ZONE_NAME_LEN)
    {
        *pu1ErrVal = NETIPV6_INVALID_ZONE_LEN;
        IP6_TASK_UNLOCK ();
        return NETIPV6_FAILURE;
    }

    /* Checks if the name configured matches with any of the scope name */
    for (u1Scope = ADDR6_SCOPE_INTLOCAL; u1Scope <= ADDR6_SCOPE_GLOBAL;
         u1Scope++)
    {
        u1ScopeNameLen =
            (UINT1) STRLEN (gIp6GblInfo.asIp6ScopeName[u1Scope].au1ZoneName);

        if (STRNCMP (pu1ScopeZone,
                     gIp6GblInfo.asIp6ScopeName[u1Scope].au1ZoneName,
                     u1ScopeNameLen) == 0)
        {
            /* If the name matches update the scope and zone-id */
            pu1KeyStr = &pu1ScopeZone[u1ScopeNameLen];
            if (0 != *pu1KeyStr)
            {
                i4ZoneId = ATOI (pu1KeyStr);
                if ((i4ZoneId > 0) && (i4ZoneId <= IP6_MAX_VALUE_FOR_ZONE_ID))
                {
                    /* Valid value for Zone Id so return success */
                    IP6_TASK_UNLOCK ();
                    return NETIPV6_SUCCESS;
                }
                *pu1ErrVal = NETIPV6_INVALID_ZONE_ID;
                IP6_TASK_UNLOCK ();
                return NETIPV6_FAILURE;
            }
            break;
        }
    }
    *pu1ErrVal = NETIPV6_INVALID_ZONE_NAME;
    IP6_TASK_UNLOCK ();
    return NETIPV6_FAILURE;
}

/******************************************************************************
 * DESCRIPTION : This API is for checking the existence of a scope-zone on IPv6
 *               interface with the given scope.
 *               The application gives Scope,context-Id and the scope-zone name
 *               as input
 *
 * INPUTS      : u4ContextId - Context from which scope-zone info needs to be
 *                             verified
 *               u1Scope - Comparison to be done for the given scope 
 *               pu1ZoneName - Scope zone name 
 *
 * OUTPUT      : pi4ZoneIndex - Scope Zone Index 
 *
 * RETURNS     : NETIPV6_SUCCESS - If zone exists on this interface
 *               or NETIPV6_FAILURE - If zone doesn't exist on this interface
 *  
 * NOTES       : This functions is added as a part of RFC4007 implementation
 *
 ******************************************************************************/

INT4
NetIpv6FindZoneIndex (UINT4 u4ContextId, UINT1 u1Scope,
                      UINT1 *pu1ZoneName, INT4 *pi4ZoneIndex)
{
    tIp6ScopeZoneInfo   Ip6ScopeZoneInfo;
    tIp6ScopeZoneInfo  *pScopeZoneInfo = NULL;

    MEMSET (&Ip6ScopeZoneInfo, 0, sizeof (tIp6ScopeZoneInfo));

    Ip6ScopeZoneInfo.u4ContextId = u4ContextId;
    Ip6ScopeZoneInfo.u1Scope = u1Scope;

    IP6_TASK_LOCK ();

    while (OSIX_TRUE == OSIX_TRUE)
    {
        pScopeZoneInfo = RBTreeGetNext (gIp6GblInfo.ScopeZoneTree,
                                        (tRBElem *) & Ip6ScopeZoneInfo, NULL);

        if ((pScopeZoneInfo != NULL) && (pScopeZoneInfo->u1Scope == u1Scope) &&
            (!MEMCMP (pu1ZoneName, pScopeZoneInfo->au1ZoneName,
                      STRLEN (pu1ZoneName))))
        {
            *pi4ZoneIndex = pScopeZoneInfo->i4ZoneIndex;

            IP6_TASK_UNLOCK ();
            return NETIPV6_SUCCESS;
        }

        if ((pScopeZoneInfo == NULL) || (pScopeZoneInfo->u1Scope != u1Scope))
        {
            break;
        }

        MEMCPY (&Ip6ScopeZoneInfo, pScopeZoneInfo, sizeof (Ip6ScopeZoneInfo));
    }
    IP6_TASK_UNLOCK ();
    return NETIPV6_FAILURE;
}

/******************************************************************************
 * DESCRIPTION : This API checks the existence of a particular scope within a 
 *               context. If the scope exist it copies the name and its corresponding
 *               zone index.
 *              
 *              The application gives Scope and context-Id as input
 *
 * INPUTS      : u4ContextId - Context from which the existence of the scope needs 
 *                             to be verified
 *               u1Scope - scope whose existence needs to be verified.
 *
 * OUTPUT      : pau1ZoneName -Scope Zone name
 *               pi4ZoneIndex - Scope Zone Index 
 *
 * RETURNS     : NETIPV6_SUCCESS - If zone exists on this interface
 *               or NETIPV6_FAILURE - If zone doesn't exist on this interface
 *  
 * NOTES       : This functions is added as a part of RFC4007 implementation
 *
 ******************************************************************************/

INT4
NetIpv6CheckIfAnyZoneExistForGivenScope (UINT4 u4ContextId, UINT1 u1Scope,
                                         UINT1 *pau1ZoneName,
                                         INT4 *pi4ZoneIndex)
{
    tIp6ScopeZoneInfo   Ip6ScopeZoneInfo;
    tIp6ScopeZoneInfo  *pScopeZoneInfo = NULL;

    MEMSET (&Ip6ScopeZoneInfo, 0, sizeof (tIp6ScopeZoneInfo));

    Ip6ScopeZoneInfo.u4ContextId = u4ContextId;
    Ip6ScopeZoneInfo.u1Scope = u1Scope;

    IP6_TASK_LOCK ();

    pScopeZoneInfo = RBTreeGetNext (gIp6GblInfo.ScopeZoneTree,
                                    (tRBElem *) & Ip6ScopeZoneInfo, NULL);

    if ((pScopeZoneInfo != NULL) && (pScopeZoneInfo->u1Scope == u1Scope))

    {
        *pi4ZoneIndex = pScopeZoneInfo->i4ZoneIndex;

        MEMCPY (pau1ZoneName, pScopeZoneInfo->au1ZoneName,
                STRLEN (pScopeZoneInfo->au1ZoneName));

        IP6_TASK_UNLOCK ();
        return NETIPV6_SUCCESS;
    }

    IP6_TASK_UNLOCK ();
    return NETIPV6_FAILURE;
}

/******************************************************************************
 * DESCRIPTION : This API gets the scope-zone information from the interface
 *               scope-zone structure. It also passes an auction based on the 
 *               action comparison it retrieves the other parameters.
 *               The application gives zone index ,ifIndex and u1Action 
 *               as input
 *
 * INPUTS      : u4IfIndex - Interface index from which scope-zone info to be 
 *                           obtained
 *               u1Action - Comparison to be done for the field in pInScopeInfo.
 *                          The possible fields that are compared are
 *                          IP6_SCOPE_ZONE_INDEX
 *                          IP6_SCOPE_ZONE_NAME
 *                          IP6_SCOPE_ZONE_SCOPE
 *               pInScopeInfo - Structure having the scope zone info related to
 *                              the u1Action
 *
 * OUTPUT      : pOutScopeInfo -Scope Zone info updated based on the u1Action 
 *
 * RETURNS     : NETIPV6_SUCCESS - If zone exists on this interface
 *               or NETIPV6_FAILURE - If zone doesn't exist on this interface
 *  
 * NOTES       : This functions is added as a part of RFC4007 implementation
 *
 ******************************************************************************/
INT4
NetIpv6GetIfScopeZoneInfo (UINT4 u4IfIndex, UINT1 u1Action,
                           tIp6ZoneInfo * pInScopeInfo,
                           tIp6ZoneInfo * pOutScopeInfo)
{
    tTMO_SLL_NODE      *pZoneNode = NULL;
    tIp6IfZoneMapInfo  *pIp6IfZoneMapInfo = NULL;
    tIp6If             *pIf6 = NULL;
    UINT4               u4ContextId = 0;
    UINT1               u1ZoneNameLen = ZERO;
    UINT1               u1IP6ZoneNameLen = ZERO;

    VcmGetContextIdFromCfaIfIndex (u4IfIndex, &u4ContextId);

    if (IP6_TASK_LOCK () == SNMP_FAILURE)
    {
        IP6_TRC_ARG (u4ContextId, NETIP6_MOD_TRC, DATA_PATH_TRC, NETIP6_NAME,
                     "NetIpv6GetIfScopeZoneInfo: IP6 Task Lock Failed !!!\n");
        return (NETIPV6_FAILURE);
    }

    pIf6 = IP6_INTERFACE (u4IfIndex);
    if (pIf6 == NULL)
    {
        IP6_TASK_UNLOCK ();
        return NETIPV6_FAILURE;
    }

    /* Scans the scope zone configured on the given interface if 
       any scope-zone index matches with the given zone index retrieves it */

    TMO_SLL_Scan (&pIf6->zoneIlist, pZoneNode, tTMO_SLL_NODE *)
    {
        pIp6IfZoneMapInfo = IP6_SCOPE_ZONE_PTR_FROM_SLL (pZoneNode);

        switch (u1Action)
        {
            case IP6_SCOPE_ZONE_INDEX:

                if (pIp6IfZoneMapInfo->i4ZoneIndex == pInScopeInfo->i4ZoneIndex)
                {
                    Ip6ScopeZoneCopy (pOutScopeInfo->au1ZoneName,
                                      pIp6IfZoneMapInfo->au1ZoneName);
                    pOutScopeInfo->i4ZoneIndex = pIp6IfZoneMapInfo->i4ZoneIndex;
                    pOutScopeInfo->u1Scope = pIp6IfZoneMapInfo->u1Scope;
                    IP6_TASK_UNLOCK ();
                    return NETIPV6_SUCCESS;

                }
                break;

            case IP6_SCOPE_ZONE_NAME:

                u1ZoneNameLen = (UINT1) STRLEN (pInScopeInfo->au1ZoneName);
                u1IP6ZoneNameLen =
                    (UINT1) STRLEN (pIp6IfZoneMapInfo->au1ZoneName);
                if ((u1ZoneNameLen == u1IP6ZoneNameLen)
                    && (0 ==
                        MEMCMP (pInScopeInfo->au1ZoneName,
                                pIp6IfZoneMapInfo->au1ZoneName, u1ZoneNameLen)))
                {
                    Ip6ScopeZoneCopy (pOutScopeInfo->au1ZoneName,
                                      pIp6IfZoneMapInfo->au1ZoneName);
                    pOutScopeInfo->i4ZoneIndex = pIp6IfZoneMapInfo->i4ZoneIndex;
                    pOutScopeInfo->u1Scope = pIp6IfZoneMapInfo->u1Scope;
                    IP6_TASK_UNLOCK ();
                    return NETIPV6_SUCCESS;

                }
                break;

            case IP6_SCOPE_ZONE_SCOPE:
                if (pIp6IfZoneMapInfo->u1Scope == pInScopeInfo->u1Scope)
                {
                    Ip6ScopeZoneCopy (pOutScopeInfo->au1ZoneName,
                                      pIp6IfZoneMapInfo->au1ZoneName);
                    pOutScopeInfo->i4ZoneIndex = pIp6IfZoneMapInfo->i4ZoneIndex;
                    pOutScopeInfo->u1Scope = pIp6IfZoneMapInfo->u1Scope;
                    IP6_TASK_UNLOCK ();
                    return NETIPV6_SUCCESS;

                }
                break;

            default:
                IP6_TASK_UNLOCK ();
                return NETIPV6_FAILURE;

        }
    }

    IP6_TASK_UNLOCK ();
    return NETIPV6_FAILURE;
}

/*-------------------------------------------------------------------+
 * Function           :  Ip6HandlePathStatusChange
 *
 *Description         : This functions indicates that there is a
 *                      BFD session state change and the same 
 *                      has to be posted as an event for ND6
 *                      cache entry deletion.
 *
 * Input(s)           : u4ContextId - Context id of the interface
 *                      pIpNbrInfo  - Next hop details to clear
 *                                    route and arp cache.
 * Output(s)          : NONE
 * Returns            : NETIPV4_SUCCESS or NETIPV4_FAILURE
------------------------------------------------------------------- */

INT1
Ip6HandlePathStatusChange (UINT4 u4ContextId,
                           tClientNbrIp6PathInfo * pIpNbrInfo)
{
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;

    UNUSED_PARAM (u4ContextId);

    if (pIpNbrInfo == NULL)
    {
        return NETIPV6_FAILURE;
    }

    pBuf = Ip6BufAlloc (u4ContextId, sizeof (tClientNbrIp6PathInfo),
                        0, IP6_MODULE);

    if (!(pBuf))
    {
        IP6_TRC_ARG (u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                     "IP6IF: Ip6HandlePathStatusChange:buffer allocation "
                     "failed\n");
        return NETIPV6_FAILURE;
    }

    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) (VOID *) (pIpNbrInfo),
                               0, sizeof (tClientNbrIp6PathInfo));

    if (OsixSendToQ (SELF, (const UINT1 *) IP6_BFD_STATE_CHANGE_QUEUE,
                     (tOsixMsg *) (VOID *) pBuf,
                     OSIX_MSG_NORMAL) == OSIX_FAILURE)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return NETIPV6_FAILURE;
    }
    else
    {
        OsixSendEvent (SELF, (const UINT1 *) IP6_TASK_NAME,
                       IP6_BFD_STATE_CHANGE_EVENT);
    }
    return NETIPV6_SUCCESS;
}

/*-------------------------------------------------------------------+
 * Function           :  Ip6ProcessPathStatusNotification
 *
 *Description         : This functions process the session state 
 *                      change notification from BFD and moves the 
 *                      ND6 Cache entry to stale.
 *
 * Input(s)           : u4ContextId - Context id of the interface
 *                      pIpNbrInfo  - Next hop details to clear
 *                                    route and arp cache.
 * Output(s)          : NONE
 * Returns            : NETIPV4_SUCCESS or NETIPV4_FAILURE
------------------------------------------------------------------- */

VOID
Ip6ProcessPathStatusNotification (tCRU_BUF_CHAIN_HEADER * pBuf)
{
    tNd6CacheEntry     *pNd6cEntry = NULL;
    tClientNbrIp6PathInfo IpNbrInfo;
    tIp6Addr            NbrAddr6;
    tIp6If             *pIp6If = NULL;

    MEMSET (&NbrAddr6, 0, sizeof (tIp6Addr));
    MEMSET (&IpNbrInfo, 0, sizeof (tClientNbrIp6PathInfo));

    CRU_BUF_Copy_FromBufChain (pBuf,
                               (UINT1 *) &(IpNbrInfo),
                               0, sizeof (tClientNbrIp6PathInfo));
    CRU_BUF_Release_MsgBufChain (pBuf, FALSE);

    MEMCPY (&NbrAddr6, &(IpNbrInfo.NbrAddr), sizeof (tIp6Addr));
    /* Get the IPv6 Interface Entry from u4Index */
    pIp6If = Ip6ifGetEntry (IpNbrInfo.u4IfIndex);
    if (pIp6If == NULL)
    {
        return;
    }

    /* Get the Neighbor Cache Entry for the given destination and interface */
    pNd6cEntry = Nd6IsCacheForAddr (pIp6If, &NbrAddr6);

    /* Set the reach state of the ND6Cache Entry to STALE. This inturn
     * will add the Static IPv6 route being monitored by BFD to the PRT 
     * (Pending Route Table) */
    if (pNd6cEntry != NULL)
    {
        Nd6SetReachState (pNd6cEntry, ND6C_STALE);
    }

    return;
}

/*-----------------------------------------------------------------------------+
 * Function           : NetIpv6GetCfaIfIndexFromPort
 * 
 * Description        : Provides the CFA IfIndex Corresponding to IP Port No
 * 
 * Input(s)           : u4Port - IP Port Number
 * 
 * Output(s)          : CFA Interface Index
 *
 * Returns            : NETIPV6_SUCCESS or NETIPV6_FAILURE
 *----------------------------------------------------------------------------- */
INT4
NetIpv6GetCfaIfIndexFromPort (UINT4 u4Port, UINT4 *pu4IfIndex)
{
    *pu4IfIndex = u4Port;

    return NETIPV6_SUCCESS;
}

/*-----------------------------------------------------------------------------+
 * Function           : NetIpv6GetPortFromCfaIfIndex
 * 
 * Description        : Provides the IP Port Number for the CFA IfIndex
 *
 * Input(s)           : u4IfIndex - CFA Interface Index
 *
 * Output(s)          : *pu4Port  - IP Port Number
 *
 * Returns            : NETIPV6_SUCCESS or NETIPV6_FAILURE
 *----------------------------------------------------------------------------- */
INT4
NetIpv6GetPortFromCfaIfIndex (UINT4 u4IfIndex, UINT4 *pu4Port)
{
    *pu4Port = u4IfIndex;

    return NETIPV6_SUCCESS;
}

#ifdef VRRP_WANTED
/*-------------------------------------------------------------------+
 * Function           : NetIpv6CreateVrrpInterface
 *                     
 * Description        : Enables VRRP capability for the IP interface or
 *                      adds address as virtual address to the IP
 *                      interface.
 *
 * Input(s)           : pVrrpNwIntf    - Pointer to VRRP Interface
 *                                       Information.
 *
 * Output(s)          : None
 *
 * Returns            : NETIPV6_SUCCESS or NETIPV6_FAILURE
------------------------------------------------------------------- */
INT4
NetIpv6CreateVrrpInterface (tVrrpNwIntf * pVrrpNwIntf)
{
    tIp6Addr            Ip6Addr;
    tIp6LlocalInfo     *pLlAddrInfo = NULL;
    tIp6AddrInfo       *pAddrInfo = NULL;
    UINT1               u1AddressType = 0;

    MEMSET ((UINT1 *) &Ip6Addr, 0, sizeof (tIp6Addr));

    if (IP6_TASK_LOCK () == SNMP_FAILURE)
    {
        IP6_TRC_ARG (IP6_DEFAULT_CONTEXT, NETIP6_MOD_TRC, DATA_PATH_TRC,
                     NETIP6_NAME,
                     "NetIpv6CreateVrrpInterface: IP6 Task Lock Failed !!!\n");
        return (NETIPV6_FAILURE);
    }

    MEMCPY (Ip6Addr.u1_addr, pVrrpNwIntf->IpvXAddr.au1Addr, IPVX_IPV6_ADDR_LEN);

    if (pVrrpNwIntf->u1Action == VRRP_NW_INTF_MCAST_CREATE)
    {
        if (Ip6AddrCreateSolicitMcast (pVrrpNwIntf->u4IfIndex, &Ip6Addr)
            == IP6_FAILURE)
        {
            IP6_TASK_UNLOCK ();

            return NETIPV6_FAILURE;
        }
    }
    else if (pVrrpNwIntf->u1Action == VRRP_NW_INTF_SECONDARY_CREATE)
    {
        u1AddressType = (UINT1) Ip6AddrType (&Ip6Addr);

        if (u1AddressType == ADDR6_LLOCAL)
        {
            pLlAddrInfo = Ip6LlAddrCreate (pVrrpNwIntf->u4IfIndex, &Ip6Addr,
                                           ADMIN_UP, IP6_ADDR_VIRTUAL);

            if (pLlAddrInfo == NULL)
            {
                IP6_TASK_UNLOCK ();
                return NETIPV6_FAILURE;
            }
        }
        else if (u1AddressType == ADDR6_UNICAST)
        {
            pAddrInfo = Ip6AddrCreate (pVrrpNwIntf->u4IfIndex, &Ip6Addr,
                                       IP6_ADDR_SIZE_IN_BITS, ADMIN_UP,
                                       IP6_ADDR_TYPE_UNICAST, 0,
                                       IP6_ADDR_VIRTUAL);

            if (pAddrInfo == NULL)
            {
                IP6_TASK_UNLOCK ();
                return NETIPV6_FAILURE;
            }
        }

        KW_FALSEPOSITIVE_FIX (pAddrInfo);
        KW_FALSEPOSITIVE_FIX1 (pLlAddrInfo);
    }
    else if (pVrrpNwIntf->u1Action == VRRP_NW_INTF_CREATE)
    {
#ifdef NPAPI_WANTED
        if (IpFsNpVrrpHwProgram (VRRP_NP_CREATE_INTERFACE,
                                 pVrrpNwIntf) == FNP_FAILURE)
        {
            IP6_TASK_UNLOCK ();

            return NETIPV6_FAILURE;
        }
#endif
    }
    else
    {
        IP6_TASK_UNLOCK ();

        return NETIPV6_FAILURE;
    }

    IP6_TASK_UNLOCK ();

    return NETIPV6_SUCCESS;
}

/*-------------------------------------------------------------------+
 * Function           : NetIpv6DeleteVrrpInterface
 *                     
 * Description        : Disables VRRP capability for the IP interface or
 *                      deletes virtual address from the IP interface.
 *
 * Input(s)           : pVrrpNwIntf    - Pointer to VRRP Interface
 *                                       Information.
 *
 * Output(s)          : None
 *
 * Returns            : NETIPV6_SUCCESS or NETIPV6_FAILURE
------------------------------------------------------------------- */
INT4
NetIpv6DeleteVrrpInterface (tVrrpNwIntf * pVrrpNwIntf)
{
    tIp6Addr            Ip6Addr;
    UINT1               u1AddressType = 0;

    MEMSET ((UINT1 *) &Ip6Addr, 0, sizeof (tIp6Addr));

    if (IP6_TASK_LOCK () == SNMP_FAILURE)
    {
        IP6_TRC_ARG (IP6_DEFAULT_CONTEXT, NETIP6_MOD_TRC, DATA_PATH_TRC,
                     NETIP6_NAME,
                     "NetIpv6DeleteVrrpInterface: IP6 Task Lock Failed !!!\n");
        return (NETIPV6_FAILURE);
    }

    MEMCPY (Ip6Addr.u1_addr, pVrrpNwIntf->IpvXAddr.au1Addr, IPVX_IPV6_ADDR_LEN);

    if (pVrrpNwIntf->u1Action == VRRP_NW_INTF_MCAST_DELETE)
    {
        if (Ip6AddrDeleteSolicitMcast (pVrrpNwIntf->u4IfIndex,
                                       &Ip6Addr) == IP6_FAILURE)
        {
            IP6_TASK_UNLOCK ();

            return NETIPV6_FAILURE;
        }
    }
    else if (pVrrpNwIntf->u1Action == VRRP_NW_INTF_SECONDARY_DELETE)
    {
        u1AddressType = (UINT1) Ip6AddrType (&Ip6Addr);

        if ((u1AddressType == ADDR6_LLOCAL) &&
            (Ip6LlAddrDelete (pVrrpNwIntf->u4IfIndex, &Ip6Addr) == IP6_FAILURE))
        {
            IP6_TASK_UNLOCK ();

            return NETIPV6_FAILURE;
        }
        else if ((u1AddressType == (UINT1) ADDR6_UNICAST) &&
                 (Ip6AddrDelete (pVrrpNwIntf->u4IfIndex, &Ip6Addr,
                                 IP6_ADDR_SIZE_IN_BITS, TRUE) == IP6_FAILURE))
        {
            IP6_TASK_UNLOCK ();

            return NETIPV6_FAILURE;
        }
    }
    else if (pVrrpNwIntf->u1Action == VRRP_NW_INTF_DELETE)
    {
#ifdef NPAPI_WANTED
        if (IpFsNpVrrpHwProgram (VRRP_NP_DELETE_INTERFACE,
                                 pVrrpNwIntf) == FNP_FAILURE)
        {
            IP6_TASK_UNLOCK ();

            return NETIPV6_FAILURE;
        }
#endif
    }
    else
    {
        IP6_TASK_UNLOCK ();

        return NETIPV6_FAILURE;
    }

    IP6_TASK_UNLOCK ();

    return NETIPV6_SUCCESS;
}

/*-------------------------------------------------------------------+
 * Function           : NetIpv6GetVrrpInterface
 *            
 * Description        : Retrieves VRRP information corresponding to
 *                      IP interface from NP.
 *
 *                      This NetIp API also handles the below actions
 *                            1. To Change Network action.
 *                            2. To Check Primary IP against Associated
 *                               IP.
 *                      The above actions are only applicable for
 *                      Linux IP.
 *
 * Input(s)           : pVrrpNwInIntf    - Pointer to VRRP Interface
 *                                       Information.
 *
 * Output(s)          : pVrrpNwOutIntf   - Pointer to VRRP Interface
 *                                       Information.
 *
 * Returns            : NETIPV6_SUCCESS or NETIPV6_FAILURE
------------------------------------------------------------------- */
INT4
NetIpv6GetVrrpInterface (tVrrpNwIntf * pVrrpNwInIntf,
                         tVrrpNwIntf * pVrrpNwOutIntf)
{
    if ((pVrrpNwInIntf->b1IsChgNwActionReqd == TRUE) ||
        (pVrrpNwInIntf->b1IsCheckIpReqd == TRUE))
    {
        return NETIPV6_SUCCESS;
    }

#ifdef NPAPI_WANTED
    if (IpFsNpVrrpHwProgram (VRRP_NP_GET_INTERFACE,
                             pVrrpNwInIntf) == FNP_FAILURE)
    {
        return NETIPV6_FAILURE;
    }

    MEMCPY (pVrrpNwOutIntf, pVrrpNwInIntf, sizeof (tVrrpNwIntf));
#else
    UNUSED_PARAM (pVrrpNwInIntf);
    UNUSED_PARAM (pVrrpNwOutIntf);
#endif

    return NETIPV6_SUCCESS;
}

#endif
/******************************************************************************
 * Function Name    :   NetIpv6UpdateNDCache
 * Description      :   This function currently is called only from VRRP to
 *                      remove ND Cache entry for Virtual MAC when a router
 *                      becomes Master.
 * Inputs           :   u4IfIndex   - Interface index
 *                      pIp6Addr    - IP Address
 *                      pMacAddr    - Dynamically learnt MAC
 *                      u1NdState   - State of the entry
 *                      u2NlMsgType - NetLink Message Type
 * Outputs          :   None
 * Return Value     :   None
 ******************************************************************************/

PUBLIC VOID
NetIpv6UpdateNDCache (UINT4 u4IfIndex, tIp6Addr * pIp6Addr, tMacAddr * pMacAddr,
                      UINT1 u1NdState, UINT2 u2NlMsgType)
{
    tIp6If             *pIf6 = NULL;
    tNd6CacheEntry     *pNd6Cache = NULL;

    UNUSED_PARAM (pMacAddr);
    UNUSED_PARAM (u2NlMsgType);

    if (IP6_TASK_LOCK () == SNMP_FAILURE)
    {
        return;
    }

    pIf6 = IP6_INTERFACE (u4IfIndex);
    if (pIf6 == NULL)
    {
        IP6_TASK_UNLOCK ();
        return;
    }

    pNd6Cache = Nd6LookupCache (pIf6, pIp6Addr);

    if (pNd6Cache == NULL)
    {
        IP6_TASK_UNLOCK ();

        return;
    }

    if (u1NdState == ND6_CACHE_ENTRY_INCOMPLETE)
    {
        Nd6PurgeCache (pNd6Cache);
    }

    IP6_TASK_UNLOCK ();

    return;
}

/*-------------------------------------------------------------------+
 * Function           :  Ipv6NdEventSend
 *
 *Description         : This functions sends message to IPv6 task
 *                      to check and initiate ND resolution for a Next Hop.
 *                      This is called from RTM context.
 *
 * Input(s)           : pNdData- CRU Buf pointer containing ND data
 *
 * Output(s)          : None
 * Returns            : OSIX_SUCCESS/OSIX_FAILURE
------------------------------------------------------------------- */

INT4
Ipv6NdEventSend (tCRU_BUF_CHAIN_HEADER * pNdData)
{

    if (OsixSendToQ
        (SELF, (const UINT1 *) IP6_ECMP6_PRT_RESOLVE_QUEUE,
         (tCRU_BUF_CHAIN_HEADER *) pNdData, OSIX_MSG_URGENT) != OSIX_SUCCESS)
    {
        CRU_BUF_Release_MsgBufChain ((tCRU_BUF_CHAIN_HEADER *) pNdData, 100);
        return OSIX_FAILURE;
    }
    if (OsixSendEvent
        (SELF, (const UINT1 *) IP6_TASK_NAME,
         IP6_ND_RESOLVE_EVENT) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*-------------------------------------------------------------------+
 * Function           : NetIpv6ArpResolve
 *
 *Description         : This function returns next hop mac address
 *                      for next hop IP.
 *
 * Input(s)           : u4DstIntfIdx - destination interface index 
 *                      pDstAddr6 - destination IPv6 address
 *
 * Output(s)          : pu1HwAddr - next hop mac address
 * Returns            : OSIX_SUCCESS/OSIX_FAILURE
------------------------------------------------------------------- */

INT4
NetIpv6Nd6Resolve (UINT4 u4DstIntfIdx, tIp6Addr * pDstAddr6, UINT1 *pu1HwAddr)
{
    UINT1               u1HwaddrLen = 0;
    tNd6CacheEntry     *pNd6CacheEntry = NULL;
    UINT4               u4Index = 0;
    tIp6If             *pIf6 = NULL;

    if (IP6_TASK_LOCK () == SNMP_FAILURE)
    {
        return NETIPV6_FAILURE;
    }

    pIf6 = Ip6ifGetEntry (u4DstIntfIdx);
    if (NULL == pIf6)
    {
        IP6_TASK_UNLOCK ();
        return NETIPV6_FAILURE;
    }

    u4Index = pIf6->u4Index;
    if (pIf6->u4UnnumAssocIPv6If != 0)
    {
        /*This is a unnumbered interface use peer mac address as the destination */
        IP6_TASK_UNLOCK ();
        CfaGetIfUnnumPeerMac (u4Index, pu1HwAddr);
        return NETIPV6_SUCCESS;
    }

    pNd6CacheEntry = Nd6IsCacheForAddr (pIf6, pDstAddr6);
    if ((!pNd6CacheEntry) ||
        (Nd6GetReachState (pNd6CacheEntry) == ND6C_INCOMPLETE) ||
        (Nd6GetReachState (pNd6CacheEntry) == ND6C_STATIC_NOT_IN_SERVICE))
    {
        Nd6Resolve (NULL, pIf6, pDstAddr6, NULL,
                    (UINT1) Ip6AddrType (pDstAddr6));
        /*start arp times for mpls to retry */
        IP6_TASK_UNLOCK ();
        return NETIPV6_FAILURE;
    }

    if ((pNd6CacheEntry != NULL) &&
        ((Nd6GetReachState (pNd6CacheEntry) == ND6C_STATIC) ||
         (Nd6GetReachState (pNd6CacheEntry) == ND6C_REACHABLE)))
    {
        /* get MAC address from the Neighbor cache entry */
        Nd6GetLladdr (pNd6CacheEntry, pu1HwAddr, &u1HwaddrLen);
        IP6_TASK_UNLOCK ();
        return NETIPV6_SUCCESS;
    }

    IP6_TASK_UNLOCK ();
    return NETIPV6_FAILURE;
}

/******************************************************************************
 * Function Name    :   NetIpv6AddNdCache
 * Description      :   This function currently is called only from DHCP6 relay
 *                      to add a ND Cache entry 
 * Inputs           :   u4IfIndex  - Interface index
 *                      pAddr6     - IP Address
 *                      pLladdr    - Dynamically learnt MAC
 *                      u1LlaLen   - Length of Link layer address
 * Outputs          :   None
 * Return Value     :   None
 ******************************************************************************/

PUBLIC VOID
NetIpv6AddNdCache (UINT4 u4IfIndex, tIp6Addr * pAddr6, UINT1 *pLladdr,
                   UINT1 u1LlaLen, UINT1 u1State)
{
    tIp6If             *pIf6 = NULL;
    tNd6CacheEntry     *pNd6Cache = NULL;
    UINT4               u4IfReachTime = 0;

    if (IP6_TASK_LOCK () == SNMP_FAILURE)
    {
        return;
    }

    pIf6 = IP6_INTERFACE (u4IfIndex);
    if (pIf6 == NULL)
    {
        IP6_TASK_UNLOCK ();
        return;
    }

    pNd6Cache = Nd6CreateCache (pIf6, (tIp6Addr *) (VOID *)
                                pAddr6, pLladdr, u1LlaLen, u1State, 0);
    if (pNd6Cache == NULL)
    {
        IP6_TASK_UNLOCK ();

        return;
    }

    u4IfReachTime = Ip6GetIfReachTime (pNd6Cache->pIf6->u4Index);
    u4IfReachTime =
        Ip6Random ((UINT4) (u4IfReachTime * 0.5),
                   (UINT4) (u4IfReachTime * 1.5));
    Nd6SetMSecCacheTimer (pNd6Cache, ND6_REACH_TIMER_ID, u4IfReachTime);

    IP6_TASK_UNLOCK ();

    return;
}

/******************************************************************************
 * Function Name    :   NetIpv6GetNbrllAddrFromIfIndex
 * Description      :   This function is used to get the link local address
 *            of the neighbor for given index                      
 * Inputs           :   u4IfIndex   - Interface index
 * Outputs          :   pNd6cEntry - Pointer to Cache entry
 * Return Value     :   None
 ******************************************************************************/

PUBLIC INT4
NetIpv6GetNbrllAddrFromIfIndex (UINT4 u4IfIndex, tIp6Addr * pIp6LLAddr)
{
    tIp6If             *pIf6 = NULL;
    tNd6CacheEntry      Nd6cEntry;
    tNd6CacheEntry     *pNd6cEntry = NULL;

    pIf6 = IP6_INTERFACE (u4IfIndex);
    if (pIf6 == NULL)
    {
        return NETIPV6_FAILURE;
    }

    MEMSET (&Nd6cEntry, 0, sizeof (tNd6CacheEntry));
    Nd6cEntry.pIf6 = pIf6;
    pNd6cEntry = RBTreeGetNext (gNd6GblInfo.Nd6CacheTable,
                                (tRBElem *) & Nd6cEntry, NULL);
    if (pNd6cEntry != NULL)
    {
        Ip6AddrCopy (pIp6LLAddr, &pNd6cEntry->addr6);
        return NETIPV6_SUCCESS;
    }

    return NETIPV6_FAILURE;
}

 /******************************************************************************
 * Function Name    :   NetIpv6McastMsgHandle
 * Description      :   This function register/deregister the provided MultiCast Address
 *                      to the specified interface.
 *                      u4IfIndex      - Interface over which this address
 *                                       needs to be registered.
 * Outputs          :   None.
 * Return Value     :   NONE
 *******************************************************************************
 */
VOID
NetIpv6McastMsgHandle (VOID)
{
    tOsixMsg           *pBuf = NULL;
    tIp6McRegNode      *pIp6MulitCastRegInfo = NULL;
    UINT4               u4ContextId = 0;

    while (OsixReceiveFromQ
           (SELF, (const UINT1 *) IP6_MULTICAST_REG_QUEUE,
            OSIX_NO_WAIT, 0, &pBuf) == OSIX_SUCCESS)
    {
        pIp6MulitCastRegInfo = (tIp6McRegNode *) pBuf;

        if ((pIp6MulitCastRegInfo->u1MsgType == IP6_MULTICAST_JOIN) ||
            (pIp6MulitCastRegInfo->u1MsgType == IP6_MULTICAST_LEAVE))
        {
            VcmGetContextIdFromCfaIfIndex (pIp6MulitCastRegInfo->u4IfIndex,
                                           &u4ContextId);
            if ((gIp6GblInfo.i4Ip6Status == IP6_FAILURE)
                || (gIp6GblInfo.apIp6If[pIp6MulitCastRegInfo->u4IfIndex] ==
                    NULL)
                || (gIp6GblInfo.apIp6If[pIp6MulitCastRegInfo->u4IfIndex]->
                    u1AdminStatus == ADMIN_INVALID))
            {
                MemReleaseMemBlock ((tMemPoolId) gIp6GblInfo.u4Ip6MCastPoolId,
                                    (UINT1 *) pIp6MulitCastRegInfo);
                pIp6MulitCastRegInfo = NULL;
                continue;
            }

        }
        switch (pIp6MulitCastRegInfo->u1MsgType)
        {
            case IP6_MULTICAST_JOIN:
                Ip6AddrCreateMcast (pIp6MulitCastRegInfo->u4IfIndex,
                                    &pIp6MulitCastRegInfo->MCastaddr);
                break;
            case IP6_MULTICAST_LEAVE:
                Ip6AddrDeleteMcast (pIp6MulitCastRegInfo->u4IfIndex,
                                    &pIp6MulitCastRegInfo->MCastaddr);
                break;
            default:
                break;
        }
        MemReleaseMemBlock ((tMemPoolId) gIp6GblInfo.u4Ip6MCastPoolId,
                            (UINT1 *) pIp6MulitCastRegInfo);
        pIp6MulitCastRegInfo = NULL;

    }

}
