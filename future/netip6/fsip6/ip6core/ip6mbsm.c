/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ip6mbsm.c,v 1.16 2017/12/19 13:41:54 siva Exp $
 *                  
 *
 *******************************************************************/

#ifdef MBSM_WANTED                /* IP6_CHASSIS_MERGE */
#include "ip6inc.h"

INT4 Ip6NpMbsmInitProtocol PROTO ((tMbsmSlotInfo *));
#ifndef IPVX_ADDR_FMLY_IPV6
#define IPVX_ADDR_FMLY_IPV6 2
#endif
/*****************************************************************************/
/*                                                                           */
/* Function     : Ip6FwdMbsmUpdateCardStatus                                 */
/*                                                                           */
/* Description  : Allocates a CRU buffer and enqueues the line card          */
/*                change status to the IP6 forward task                      */
/*                                                                           */
/* Input        : pProtoMsg - Contains the slot and port information         */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : MBSM_SUCCESS or MBSM_FAILURE                               */
/*                                                                           */
/*****************************************************************************/

INT4
Ip6FwdMbsmUpdateCardStatus (tMbsmProtoMsg * pProtoMsg, UINT1 u1Cmd)
{
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;

    pBuf =
        Ip6BufAlloc (VCM_INVALID_VC, (sizeof (tMbsmProtoMsg)), 0, IP6_MODULE);
    if (pBuf == NULL)
    {
        MBSM_DBG (MBSM_PROTO_TRC, MBSM_IP6, "CRU Buffer Allocation Failed\n");
        return MBSM_FAILURE;
    }

    if (Ip6BufWrite (pBuf, (UINT1 *) pProtoMsg, 0,
                     sizeof (tMbsmProtoMsg), TRUE) != IP6_SUCCESS)
    {
        Ip6BufRelease (VCM_INVALID_VC, pBuf, FALSE, IP6_MODULE);
        MBSM_DBG (MBSM_PROTO_TRC, MBSM_IP6, "Copy Over CRU Buffer Failed\n");
        return MBSM_FAILURE;
    }

    IP6_SET_COMMAND (pBuf, u1Cmd);

    if (OsixSendToQ (SELF,
                     (const UINT1 *) IP6_TASK_CONTROL_QUEUE,
                     pBuf, OSIX_MSG_NORMAL) != OSIX_SUCCESS)
    {
        Ip6BufRelease (VCM_INVALID_VC, pBuf, FALSE, IP6_MODULE);
        MBSM_DBG (MBSM_PROTO_TRC, MBSM_IP6, "Send To IP6 Q Failed\n");
        return MBSM_FAILURE;
    }

    OsixSendEvent (SELF, (const UINT1 *) IP6_TASK_NAME, IP6_CONTROL_EVENT);

    return MBSM_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Ip6MbsmUpdateLCStatus                                      */
/*                                                                           */
/* Description  : Initialise the line card and update the interface table,   */
/*                neigbour table in the NP, based on the line card status    */
/*                received                                                   */
/*                                                                           */
/* Input        : pBuf     - Buffer containing the protocol message info.    */
/*                u1Cmd    - Line card status (UP/DOWN)                      */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

VOID
Ip6MbsmUpdateLCStatus (tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 u1Cmd)
{
    INT4                i4RetStatus = MBSM_SUCCESS;
    UINT4               u4Port = 0;
    UINT2               u2VlanId;
    UINT4               u4PhyIfIndex = 0;
    tIp6Cxt            *pIp6Cxt = NULL;
    tMbsmSlotInfo      *pSlotInfo = NULL;
    tNd6CacheEntry     *pNd6cEntry = NULL;
    tNd6CacheEntry     *pNd6cNextEntry = NULL;
    tMbsmProtoMsg       ProtoMsg;
    tMbsmProtoAckMsg    ProtoAckMsg;
    tIp6If             *pIf6;
    tFsNpIntInfo        IntInfo;
#ifdef TUNNEL_WANTED
    tTnlIfEntry         TnlIfEntry;
#endif
    MEMSET (&ProtoMsg, ZERO, sizeof (tMbsmProtoMsg));
    MEMSET (&ProtoAckMsg, ZERO, sizeof (tMbsmProtoAckMsg));
    MEMSET (&IntInfo, 0, sizeof (tFsNpIntInfo));

    if ((Ip6BufRead (pBuf, (UINT1 *) &ProtoMsg, 0,
                     sizeof (tMbsmProtoMsg), 1)) == NULL)
    {
        MBSM_DBG (MBSM_PROTO_TRC, MBSM_IP6, "Copy From CRU Buffer Failed\n");
        i4RetStatus = MBSM_FAILURE;
    }

    if (i4RetStatus != MBSM_FAILURE)
    {
        pSlotInfo = &(ProtoMsg.MbsmSlotInfo);

        if ((u1Cmd == MBSM_MSG_CARD_INSERT) &&
            (!MBSM_SLOT_INFO_ISPORTMSG (pSlotInfo)))
        {

            if ((Ip6NpMbsmInitProtocol (pSlotInfo)) != MBSM_SUCCESS)
            {
                i4RetStatus = MBSM_FAILURE;
            }

            if (i4RetStatus != MBSM_FAILURE)
            {
                u4Port = 1;

                IP6_IF_SCAN (u4Port, (UINT4) IP6_MAX_LOGICAL_IF_INDEX)
                {
                    pIf6 = Ip6ifGetEntry (u4Port);
                    if (pIf6 != NULL)
                    {
                        if ((pIf6->u1IfType == IP6_L3VLAN_INTERFACE_TYPE) ||
                            (pIf6->u1IfType == IP6_LAGG_INTERFACE_TYPE) ||
                            (pIf6->u1IfType == IP6_PSEUDO_WIRE_INTERFACE_TYPE))
                        {
                            IntInfo.u4PhyIfIndex = pIf6->u4Index;
                            IntInfo.u1IfType = pIf6->u1IfType;
                        }
#ifdef TUNNEL_WANTED
                        else if (pIf6->u1IfType == IP6_TUNNEL_INTERFACE_TYPE)
                        {
                            /* fetch the Tunnel interface's when tunnel configurations are present*
                             * Physical Interface index */

                            MEMSET (&TnlIfEntry, 0, sizeof (tTnlIfEntry));
                            if (CfaGetTnlEntryFromIfIndex (pIf6->u4Index,
                                                           &TnlIfEntry) ==
                                CFA_SUCCESS)
                            {
                                IntInfo.u4PhyIfIndex = TnlIfEntry.u4PhyIfIndex;
                                IntInfo.TunlInfo.u1TunlType =
                                    pIf6->pTunlIf->u1TunlType;
                                IntInfo.u1IfType = pIf6->u1IfType;
                                IntInfo.TunlInfo.tunlSrc.u4_addr[3] =
                                    pIf6->pTunlIf->tunlSrc.u4_addr[3];
                                IntInfo.TunlInfo.tunlDst.u4_addr[3] =
                                    pIf6->pTunlIf->tunlDst.u4_addr[3];
                                IntInfo.TunlInfo.u1TunlFlag =
                                    pIf6->pTunlIf->u1TunlFlag;
                                IntInfo.TunlInfo.u1TunlDir =
                                    pIf6->pTunlIf->u1TunlDir;
                            }

                        }
#endif /* TUNNEL_WANTED */
                        if ((pIf6->u1IfType == IP6_L3VLAN_INTERFACE_TYPE) ||
                            (pIf6->u1IfType == IP6_LAGG_INTERFACE_TYPE) ||
                            (pIf6->u1IfType == IP6_L3SUB_INTF_TYPE) ||
                            (pIf6->u1IfType == IP6_PSEUDO_WIRE_INTERFACE_TYPE)
                            || ((pIf6->u1IfType == IP6_TUNNEL_INTERFACE_TYPE)
                                && (0 != TnlIfEntry.u4PhyIfIndex)))
                        {
                            if (CfaGetVlanId (IntInfo.u4PhyIfIndex,
                                              &(IntInfo.u2VlanId)) ==
                                CFA_FAILURE)
                            {
                                i4RetStatus = MBSM_FAILURE;
                            }

                            if (Ip6GetHwAddr (IntInfo.u4PhyIfIndex,
                                              IntInfo.au1MacAddr) !=
                                CFA_SUCCESS)
                            {
                                i4RetStatus = MBSM_FAILURE;
                            }
                        }
                        if (Ipv6FsNpMbsmIpv6IntfStatus
                            (pIf6->pIp6Cxt->u4ContextId, pIf6->u1OperStatus,
                             &IntInfo, pSlotInfo) == FNP_FAILURE)
                        {
                            i4RetStatus = MBSM_FAILURE;
                        }
                    }
                }

                pNd6cEntry = RBTreeGetFirst (gNd6GblInfo.Nd6CacheTable);

                while (pNd6cEntry != NULL)
                {
                    if ((pNd6cEntry->pIf6->u1IfType ==
                         IP6_L3VLAN_INTERFACE_TYPE)
                        || (pNd6cEntry->pIf6->u1IfType ==
                            IP6_PSEUDO_WIRE_INTERFACE_TYPE)
                        || (pNd6cEntry->pIf6->u1IfType ==
                            IP6_ENET_INTERFACE_TYPE)
                        || (pNd6cEntry->pIf6->u1IfType ==
                            IP6_L3SUB_INTF_TYPE)
                        || (pNd6cEntry->pIf6->u1IfType ==
                            IP6_LAGG_INTERFACE_TYPE))
                    {
                        u4PhyIfIndex = pNd6cEntry->pIf6->u4Index;
                    }
#ifdef TUNNEL_WANTED
                    else if (pNd6cEntry->pIf6->u1IfType ==
                             IP6_TUNNEL_INTERFACE_TYPE)
                    {
                        /* fetch the Tunnel interface's    *
                         *        Physical Interface index */
                        if (CfaGetTnlEntryFromIfIndex
                            (pNd6cEntry->pIf6->u4Index, &TnlIfEntry)
                            == CFA_SUCCESS)
                        {
                            u4PhyIfIndex = TnlIfEntry.u4PhyIfIndex;
                        }
                    }
#endif /* TUNNEL_WANTED */
                    else
                    {
                        i4RetStatus = MBSM_FAILURE;
                    }

                    if (CfaGetVlanId (u4PhyIfIndex, &u2VlanId) == CFA_SUCCESS)
                    {
                        pIp6Cxt = pNd6cEntry->pIf6->pIp6Cxt;
                        /* Only static and reachable states needs to be programmed */
                        if ((pNd6cEntry->u1ReachState == ND6C_STATIC) ||
                            (pNd6cEntry->u1ReachState == ND6C_REACHABLE))
                        {

                            if (Ipv6FsNpMbsmIpv6NeighCacheAdd
                                (pIp6Cxt->u4ContextId,
                                 ((UINT1 *) (&(pNd6cEntry->addr6))),
                                 pNd6cEntry->pIf6->u4Index,
                                 ((tNd6CacheLanInfo *)
                                  (pNd6cEntry->pNd6cInfo))->lladdr,
                                 IP6_MAX_LLA_LEN, NP_IPV6_NH_REACHABLE,
                                 u2VlanId, pSlotInfo) == FNP_FAILURE)
                            {
                                i4RetStatus = MBSM_FAILURE;
                            }
                        }
                    }
                    pNd6cNextEntry = RBTreeGetNext (gNd6GblInfo.Nd6CacheTable,
                                                    pNd6cEntry, NULL);
                    pNd6cEntry = pNd6cNextEntry;
                }

            }

        }
        else
        {
            MBSM_DBG (MBSM_PROTO_TRC, MBSM_IP6,
                      "MBSM_CARD_REMOVE - No Support\n");
            i4RetStatus = MBSM_SUCCESS;
        }
        ProtoAckMsg.i4SlotId = pSlotInfo->i4SlotId;
        ProtoAckMsg.i4ProtoCookie = ProtoMsg.i4ProtoCookie;
    }

#ifdef VRRP_WANTED
    if (VrrpIpIfHandleCardAttach (pSlotInfo, IPVX_ADDR_FMLY_IPV6) ==
        VRRP_NOT_OK)
    {
        return;
    }
#endif

    ProtoAckMsg.i4RetStatus = i4RetStatus;

    MbsmSendAckFromProto (&ProtoAckMsg);

    Ip6BufRelease (VCM_INVALID_VC, pBuf, FALSE, IP6_MODULE);

}

/*****************************************************************************/
/*                                                                           */
/* Function     : Ip6NpMbsmInitProtocol                                      */
/*                                                                           */
/* Description  : Initialise the IPv6 protocol on the line card              */
/*                                                                           */
/* Input        : pSlotInfo - Contains the slot information                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : MBSM_SUCCESS or MBSM_FAILURE                               */
/*                                                                           */
/*****************************************************************************/

INT4
Ip6NpMbsmInitProtocol (tMbsmSlotInfo * pSlotInfo)
{
    if (0 == MBSM_SLOT_INFO_ISPORTMSG (pSlotInfo))
    {
        if (Ipv6FsNpMbsmIpv6Init (pSlotInfo) != FNP_SUCCESS)
        {
            return MBSM_FAILURE;
        }
    }
    return MBSM_SUCCESS;
}
#endif
