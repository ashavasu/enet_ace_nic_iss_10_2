/*******************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: ip6zone.h,v 1.8 2015/06/01 12:23:15 siva Exp $
 *
 * Description: This file contains the typedefs for the IPv6 Scope Zone
 *              structures as well as related constants, macros and 
 *              function prototypes.
 *
 *  Author : Vidya 
 *
 *******************************************************************/


#ifndef _IP6ZONE_H
#define _IP6ZONE_H

/* RFC4007 New Macro - Start */

#define   IP6_DEFAULT_SCOPE_ZONE_NAME_LEN       7
#define   IP6_DEFAULT_SCOPE_ZONE_NAME           "invalid"
#define   IP6_DEFAULT_SCOPE_ZONE_NAME_LEN       7
#define   IP6_GLOBAL_ZONEID                     1
#define   IP6_ZONE_NOT_CREATED                  0
#define   IP6_FIRST_NON_GLOBAL_ZONE_CREATED     1
#define   IP6_LAST_NON_GLOBAL_ZONE_DELETED      0
#define   IP6_MAX_VALUE_FOR_ZONE_ID             65535



typedef struct _IP6_SCOPE_INFO
{
      UINT1           au1ZoneName[IP6_SCOPE_ZONE_NAME_LEN];
}tIp6ScopeName;


/* Adds the interface list au1List2 to au1List1 - ie adds members of
 *  *      * au1List2 to au1List1*/
#define IP6_ADD_ZONE_INT_LIST(au1List1, au1List2) \
              {\
                 UINT2 u2ByteIndex;\
                 \
                 for (u2ByteIndex = 0;\
                      u2ByteIndex < IP6_MAX_ZONE_INT_LIST_SIZE;\
                      u2ByteIndex++) {\
                    au1List1[u2ByteIndex] |= au1List2[u2ByteIndex];\
                 }\
              }

/* RFC4007 New Macro - End */
#endif /* !_IP6ZONE_H */

