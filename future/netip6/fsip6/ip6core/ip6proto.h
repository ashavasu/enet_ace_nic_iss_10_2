/*******************************************************************
 *     Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *   
 *     $Id: ip6proto.h,v 1.49 2016/03/18 13:19:27 siva Exp $
 ******************************************************************/

#ifndef _IP6PROTO_H
#define _IP6PROTO_H
/*---------------------------------------------------------------------------- 
 *    PRINCIPAL AUTHOR             :    Senthil Vadivu
 *
 *    SUBSYSTEM NAME               :    IPv6
 *
 *    MODULE NAME                  :    All Modules
 *
 *    LANGUAGE                     :    C
 *
 *    TARGET ENVIRONMENT           :    UNIX
 *
 *    DATE OF FIRST RELEASE        :    DDD-MM-1996
 *
 *    DESCRIPTION                  :    This file contains extern declarations
 *                                      of the variables and functions which
 *                                      are used by multiple modules of the
 *                                      IPv6 subsystem.
 *
 *----------------------------------------------------------------------------- 
 */
/* IP6 Module */

VOID    Ip6ControlMsg           PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf));


VOID    Ip6TimerHandler         PROTO ((VOID));

INT4    Ip6IfInit               PROTO ((VOID));

INT1    Ip6CreateIf             PROTO ((UINT4 u4index, 
                                        UINT4 u4Speed, UINT4 u4HighSpeed,
                                        UINT1 u1type,
                                        UINT2 u2ckt, UINT1 u1admin,
                                        UINT1 *ptok, UINT1 u1tokl));

INT1    Ip6DeleteIf             PROTO ((UINT4 u4Index));

INT1    Ip6EnableIf             PROTO ((UINT4 u4Index));

INT1    Ip6DisableIf            PROTO ((UINT4 u4Index));

UINT4   Ip6ifHash               PROTO ((UINT4 u4If, UINT2 u2Ckt));

tIp6If *Ip6GetIf                PROTO ((UINT4 u4If, UINT2 u2Ckt));

tIp6If *Ip6GetIfFromIndex       PROTO ((UINT4 u4Index));

VOID    Ip6ifSend               PROTO ((tIp6If * pIf6,
                                        tIp6Addr * pDstAddr6,
                                        tNd6CacheEntry * pNd6CacheEntry,
                                        UINT4 u4Len,
                                        tCRU_BUF_CHAIN_HEADER * pBuf,
                                        UINT1 u1RouteAddrType));

VOID    Ip6ifFormEthMaddr       PROTO ((tIp6Addr * pIf6, UINT1 *hwaddr,
                                        UINT1 *u1_hwalen));
VOID    Ip6ifGetEthLladdr       PROTO ((tIp6If * pIf6, UINT1 *pMacaddr,
                                        UINT1 *pMaclen));

VOID    Ip6ifGetWanLladdr       PROTO ((tIp6If * pIf6, UINT1 *pAddr,
                                        UINT1 *pAlen));

VOID    Ip6ifGetPppLladdr       PROTO ((tIp6If * pIf6, UINT1 *pAddr,
                                        UINT1 *pAlen));

tNd6CacheEntry *Ip6ifResolveDst PROTO ((tNetIpv6RtInfo *pRt6,
                                        tIp6Addr * pDst,
                                        tIp6If ** pPIf6));

UINT1   Ip6ifGetIfOper          PROTO ((UINT1 u1Type, UINT4 u4If,
                                        UINT2 u2Ckt));

UINT4   Ip6ifGetMtu             PROTO ((tIp6If * pIf6));

INT4    Ip6SendInCxt            PROTO ((tIp6Cxt * pIp6Cxt, 
                                        tIp6If * pIf6,
                                        tIp6Addr * pSrc,
                                        tIp6Addr * pDst,
                                        UINT4 u4Len,
                                        UINT1 u1Proto,
                                        tCRU_BUF_CHAIN_HEADER * pBuf,
                                        UINT1 u1Hlim));

INT4    Ip6SendNdMsg            PROTO ((tIp6If * pIf6,
                                        tNd6CacheEntry * pNd6cEntry,
                                        tIp6Addr * pSrcAddr6,
                                        tIp6Addr * pDstAddr6,
                                        UINT4 u4Nd6Size,
                                        tCRU_BUF_CHAIN_HEADER * pBuf,
                                         UINT1 u1SeNDFlag,
                                         tIp6Addr *));

INT4    Ip6FormPkt              PROTO ((tIp6If * pIf6, tIp6Addr * pSrc,
                                        tIp6Addr * pDst, UINT4 *pLen,
                                        UINT1 u1Proto,
                                        tCRU_BUF_CHAIN_HEADER * pBuf,
                                        UINT1 u1Hlim));

VOID    Ip6Output               PROTO ((tIp6If * pIf6, UINT4 u4Len,
                                        tCRU_BUF_CHAIN_HEADER * pBuf));

UINT4   IP6_Get_V4_From_6to4    PROTO((tIp6Addr *pAddr));

tIp6If *Ip6FindRtIfInCxt        PROTO ((UINT4 u4ContextId, tIp6Addr * pDst));
tIp6If *Ip6GetRouteInCxt        PROTO ((UINT4 u4ContextId, tIp6Addr *pDst,
                                        tNd6CacheEntry **pNd6c,
                                        tNetIpv6RtInfo *));

/* ND Proxy */
INT4 Nd6ProxyProcessIcmp6Extns  PROTO ((tIp6Hdr * pIp6, tIp6If * pIf6, UINT2 u2ExtnsLen,
                                        tCRU_BUF_CHAIN_HEADER * pBuf, tCRU_BUF_CHAIN_HEADER * pOutBuf));
INT4 Nd6IsProxyEnabled          PROTO ((tIp6If *pIf6));

INT4 Nd6ProxyNeighSol           PROTO ((tIp6If * pIf6, tIp6Hdr * pIp6, tNd6NeighSol *pNd6Nsol,
                                        tNd6ExtHdr  *pNd6Ext));
INT4 Nd6ProxyNeighAdv           PROTO ((tIp6If * pIf6, tIp6Hdr * pIp6, tNd6NeighAdv *pNd6Nadv));
INT4 Nd6IsNSProxyAllowed        PROTO ((tIp6If * pIf6, tIp6Hdr * pIp6));
INT4 Nd6IsNeighborProxyPkt      PROTO ((tIp6If * pIf6, tIp6Hdr * pIp6, UINT2 u2Len,
                                        tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 u1NextHdr));
VOID Nd6ProxyPkt                PROTO ((tIp6Hdr * pIp6, tIp6If * pIf6, UINT4 u4Len,
                                        tCRU_BUF_CHAIN_HEADER * pBuf));

INT4 Nd6GetDownStrProxyIfList   PROTO ((tIp6If * pIf6, UINT4 *pu4IfIndexArray));
VOID Nd6ProxyRtrAdvt            PROTO ((tIp6Hdr * pIp6, tIp6If * pIf6, UINT2 u2Len,
                                        tCRU_BUF_CHAIN_HEADER * pBuf));

VOID Nd6ProxyTimeout            PROTO((tIp6If *pIf6));

INT4 Nd6ProxyNeighRedir         PROTO((tIp6If * pIf6, tIp6Hdr * pIp6,
                                        tNd6Redirect  *pNd6RdirHdr,
                                        tCRU_BUF_CHAIN_HEADER * pBuf, UINT2 u2Len));

INT4 Nd6IsNDProxyRequired       PROTO((tIp6If * pIf6, tIp6Hdr * pIp6, tCRU_BUF_CHAIN_HEADER *));
INT4 Nd6IsProxyReqIcmp6Msg      PROTO((tIp6If * pIf6, tIp6Hdr * pIp6,
                                        tCRU_BUF_CHAIN_HEADER * pBuf));
VOID Nd6ProxyUtlSetOperStatus   PROTO ((tIp6If *pIf6, UINT1  u1OperStatus));

INT4 Nd6ProxyProcessTLLA        PROTO ((tIp6Hdr * pIp6, tIp6If * pIf6, UINT2 u2ExtnsLen,
                                        tCRU_BUF_CHAIN_HEADER * pBuf, tCRU_BUF_CHAIN_HEADER * pOutBuf,
                                        tIp6Addr *pTargAddr6, UINT4 *u4Nd6Size));
VOID Nd6ProxyForward            PROTO ((tIp6Hdr * pIp6, tIp6If * pIf6, UINT4 u4Len,
                                        tCRU_BUF_CHAIN_HEADER * pBuf));


VOID Nd6ProxyCloneAndTxRtrAdv   PROTO( (tIp6Hdr *pIp6, tCRU_BUF_CHAIN_HEADER *pInBuf, 
                                UINT4  u4BufLen, tNd6RoutAdv *pNd6RAdv, 
                                UINT4  u4ContextId, tIp6If *pOutIf6));

INT4 Nd6GetNSTargetAddr         PROTO ((tIp6If * pIf6, tIp6Hdr * pIp6, tCRU_BUF_CHAIN_HEADER * pBuf,
                                tIp6Addr *pIp6TgtAddr));


VOID    Ip6Forward              PROTO ((tIp6Hdr * pIp6, tIp6If * pIf6,
                                        UINT4 u4Len, UINT1 u1RtFlag,
                                        tCRU_BUF_CHAIN_HEADER * pBuf));

INT4    Ip6GetPmtuFromIcmpErrPktInCxt
                                PROTO ((tIp6Cxt * pIp6Cxt,
                                        tCRU_BUF_CHAIN_HEADER * pBuf,
                                        UINT4 *pu4Pmtu));
INT4    Ip6GetProtoFromIcmp6ErrPktInCxt
                                PROTO ((tIp6Cxt * pIp6Cxt, 
                                        tCRU_BUF_CHAIN_HEADER * pBuf, 
                                        UINT1 *pu1Nhdr, UINT4 *pu4Pmtu));

INT4    Ip6IsPktToMeInCxt       PROTO ((tIp6Cxt *pIp6Cxt, UINT1 *pu1Type,
                                        UINT1 u1LlocalChk, tIp6Addr * pDstaddr,
                                        tIp6If * pIf6));

VOID    Ip6AddrDadStart         PROTO ((UINT1 *pAddrInfo, UINT1 u1Type));

VOID    Ip6FillHdr              PROTO ((tIp6Cxt *, tIp6Hdr *, tIp6Addr *,
                                        tIp6Addr *, UINT4, UINT1, UINT1));

VOID    Ip6AddrTimeout          PROTO ((UINT1 u1Id, tIp6Timer * pTimerNode));

INT4    Ip6AddrInit             PROTO ((VOID));

UINT1  *Ip6GetAddrInfo          PROTO ((tIp6If * pIf6, tIp6Addr * pAddr6));

tIp6Addr *Ip6AddrGetSrc         PROTO ((tIp6If * pIf6, tIp6Addr * pDst));

tIp6AddrInfo *Ip6AddrIsNeighbor PROTO ((tIp6If * pIf6, tIp6Addr * pAddr6));

VOID    Ip6AddrNotifyLla        PROTO ((tIp6LlocalInfo * pAddr,
                                        UINT1 u1Status));

VOID    Ip6AddrDadStatus        PROTO ((UINT1 *pAddrInfo, UINT1 u1Status,
                                        UINT1 u1Type));

VOID    Ip6AddrNotify           PROTO ((tIp6AddrInfo * pAddr, UINT1 u1Status));

tIp6LlocalInfo *Ip6LlAddrCreate PROTO ((UINT4 u4Index, tIp6Addr * pAddr,
                                        UINT1 u1Status, UINT1 u1ConfigMethod));

INT1    Ip6LlAddrDelete         PROTO ((UINT4 u4Index, tIp6Addr * pAddr));

INT1    Ip6LlAddrDadStatus      PROTO ((tIp6LlocalInfo * pLlAddrInfo));

tIp6AddrInfo   *Ip6AddrCreate   PROTO ((UINT4 u4Index, tIp6Addr * pAddr, 
                                        UINT1 u1Prefixlen, UINT1 u1Status,
                                        UINT1 u1Type, UINT2 u2ProfIndex,
                                        UINT1 u1ConfigMethod));

tIp6LlocalInfo *Ip6AddrCreateLla
                                PROTO ((UINT1 *pToken, UINT1 u1TokLen,
                                        tIp6If * pIf6));

UINT1   Ip6AddrGetStatus        PROTO ((tIp6If * pIf6, tIp6Addr * pAddr));

INT4    Ip6GetUniAddrType       PROTO ((tIp6If * pIf6, tIp6Addr * pAddr));

INT1    Ip6AddrDelete           PROTO ((UINT4 u4Index, tIp6Addr * pAddr,
                                        UINT1 u1Prefixlen,
                                        UINT1 u1RtDelStatus));

INT1    Ip6AddrUp               PROTO ((UINT4 u4Index, tIp6Addr * pAddr, 
                                        UINT1 u1Prefixlen,
                                        tIp6AddrInfo * pAddrInfo));

INT1    Ip6AddrDown             PROTO ((UINT4 u2Index, tIp6Addr * pAddr, 
                                        UINT1 u1Prefixlen,
                                        tIp6AddrInfo * pInfo));

tIp6AddrProfile *Ip6GetAddrProfileFromProfIndex
                                PROTO ((UINT2 u2ProfIndex));

INT1    Ip6AddrCreateMcast      PROTO ((UINT4 u4Index, tIp6Addr * pAddr));

INT1    Ip6AddrDeleteMcast      PROTO ((UINT4 u4Index, tIp6Addr *ip6Addr));

INT1    Ip6AddrCreateSolicitMcast PROTO ((UINT4 u4Index, tIp6Addr *pAddr));

INT1    Ip6AddrDeleteSolicitMcast PROTO ((UINT4 u4Index, tIp6Addr *pAddr));

INT4    Ip6PrcsPad1Opt          PROTO ((tCRU_BUF_CHAIN_HEADER *pBuf,
                                        UINT1 *pu1OptType,
                               UINT4* pu4BalanceBytes,
                                        UINT4 *pu4ProcLen));

INT4    Ip6PrcsPadNOpt          PROTO ((tCRU_BUF_CHAIN_HEADER *pBuf,
                                        UINT1 *pu1ByteToRead, 
                               UINT4 *pu4BalanceBytes));

INT4    Ip6PrcsRtHdr            PROTO ((UINT1 *pu1Type, UINT1 *u1_next,
                                        UINT4 *u4Len, UINT4 u4TotLen,
                                        tIp6If * pIf6, tIp6Hdr * pIp6,
                                        tCRU_BUF_CHAIN_HEADER * pBuf));
INT4    Ip6IsAddrGreaterUptoPrefixLen  PROTO ((tIp6Addr * pIp6Addr1,
                                              tIp6Addr * p_ip6_addr2,
                                               UINT1 u1PrefixLen));



INT1 Ip6AddrEqual PROTO((tIp6Addr Addr1, tIp6Addr Addr2));


INT4 Ip6PolicyInfoCmp PROTO((tRBElem *, tRBElem *));

VOID  Ipv6SortDstAddrList  PROTO((tTMO_SLL * ));

VOID  Ipv6SortSrcAddrList PROTO ((tTMO_SLL * ));

INT4  Ipv6FormSrcAddrList PROTO ((tTMO_SLL * ,tIp6If * pIf6));


VOID
Nd6UpdateStaticCache (tNd6CacheEntry * pNd6cEntry, UINT1 *pLladdr,
                      UINT1 u1LlaLen, UINT1 u1NewState);
#ifdef MIP6_WANTED
INT4    Ip6PrcsDestOptHdr       PROTO ((UINT1 *pu1NextHdr,
                                        UINT4 *pu4Len,
                                        tIp6If * pIf6,
                                        tIp6Hdr * pIp6,
                                        tCRU_BUF_CHAIN_HEADER * pBuf,
                                        tIp6Addr *pHomeAddr));

VOID    Mip6ProcessHAInfoOpt    PROTO ((tIp6If *pIf6,tIp6Hdr *pIp6,
                                        tNd6HaInfo *pNd6HaInfo));

INT4    Mip6AddGlobAddrHAList   PROTO ((tNd6PrefixExt *pNd6Prefix,
                                        tIp6If *pIf6, tIp6Hdr *pIp6));

INT4    MNProcessPrefixOption   PROTO ((tNd6PrefixExt *pNd6Prefix,
                                        tIp6Hdr *pIp6, tIp6If *pIf6));

VOID    MNSetAddrStatAfterMvmntDet PROTO ((UINT4 u4Index));

VOID    MNAddTmpHAsAfterMvmntDet   PROTO ((UINT4 u4Index));

INT4    MNAddPrefixToIfAddrList    PROTO ((UINT4 u4Index,
                                           tIp6AddrInfo *pAddr6Info));
#else
INT4    Ip6PrcsDestOptHdr       PROTO ((UINT1 *pu1NextHdr, UINT4 *pu4Len,
                                        tIp6If * pIf6, tIp6Hdr * pIp6,
                                        tCRU_BUF_CHAIN_HEADER * pBuf));
#endif

INT4    Ip6PrcsHbyhOptHdr       PROTO ((UINT1 *pu1NextHdr, UINT4 *pu4Len,
                                        UINT4 *pu4TotLen, tIp6If * pIf6,
                                        tIp6Hdr * pIp6,
                                        tCRU_BUF_CHAIN_HEADER * pBuf,
                                        UINT1 u1IsFinalDest));

INT4    Ip6FragInit             PROTO ((VOID));

INT4    Ip6FragSend             PROTO ((tIp6If * pIf6, tIp6Addr * pDst,
                                        tNd6CacheEntry * pNd6c,
                                        UINT4 u4Mtu, UINT4 u4Len,
                                        tCRU_BUF_CHAIN_HEADER * pBuf,
                                        UINT1 u1RouteAddrType));

tCRU_BUF_CHAIN_HEADER *Ip6HandleReasm
                                PROTO ((UINT1 *u1NextHdr, UINT4 *u4Len,
                                        tIp6If * pIf6, tIp6Hdr * pIp6,
                                        tCRU_BUF_CHAIN_HEADER * pBuf));

VOID    Ip6ReasmTimeout         PROTO ((tIp6Timer * pTimer));

INT4    Ip6PmtuNotifyInCxt      PROTO((UINT4, tIp6Addr, UINT4));

VOID    Ip6InitRegTable         PROTO ((VOID));

VOID    Ip6InitMCastRegTable    PROTO ((VOID));

INT4    Ip6RouteChgRegHandler   PROTO ((tIp6RtEntry *, VOID *));

VOID    Ip6ActOnHLRegDereg      PROTO((UINT4, UINT4, UINT4, UINT4));

INT4    Ipv6GetIfInfo PROTO ((UINT4 , tNetIpv6IfInfo *));


/* ICMP6 Module */

VOID    Icmp6RcvPingMsg         PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf));

VOID    Icmp6Rcv                PROTO ((tIp6If * pIf6, tIp6Hdr * pIp6,
                                        UINT2 u2Len, UINT1 u1Flag,
                                        tCRU_BUF_CHAIN_HEADER * pBuf));

VOID    Icmp6SendInCxt          PROTO ((tIp6Cxt * pIp6Cxt, tIp6If * pIf6,
                                        tIp6Addr * pSrc,
                                        tIp6Addr * pDst, UINT4 u2Len,
                                        tCRU_BUF_CHAIN_HEADER * pBuf));

VOID    Icmp6SendErrMsg         PROTO ((tIp6If * pIf6, tIp6Hdr * pIp6,
                                        UINT1 u1Type, UINT1 u1Code,
                                        UINT4 u4Parm,
                                        tCRU_BUF_CHAIN_HEADER * pBuf));

VOID    Icmp6SendInfoMsg        PROTO ((tIcmp6Params * pInfo,
                                        tCRU_BUF_CHAIN_HEADER * pBuf));

VOID    Icmp6RateLimitTimerHandler PROTO ((UINT1 u1Id, tIp6Timer * pTimer));

/* UDP6 Module */

INT4    Udp6Init                PROTO ((VOID));

INT4
Udp6GetPort (UINT2 u2UPort, tIp6Addr * pIp6Addr, tUdp6Port * pUdp6PortEntry);

INT4
UdpIpv6GetPort (UINT2 u2LPort, tIp6Addr * pIp6LAddr,
                UINT2 u2RPort, tIp6Addr * pIp6RAddr,
                tUdp6Port ** pUdp6PortEntry, UINT1 *pu1Udp6CBMode);

VOID    Udp6Rcv                 PROTO ((tIp6If * pIf6, tIp6Hdr * pIp6,
                                        UINT4 u4Len,
                                        tCRU_BUF_CHAIN_HEADER * pBuf));

/* ND6 Module */

INT4    Nd6Init                 PROTO ((VOID));

tNd6CacheEntry  *Nd6CreateCache PROTO ((tIp6If * pIf6, tIp6Addr * pAddr6,
                                        UINT1 *pLladdr, UINT1 u1LlaLen,
                                        UINT1 u1State, UINT1 u1SecureFlag));
INT4
Nd6SelectBestAnycastDestination PROTO ((tNd6CacheEntry *pNd6cEntry));

VOID
Nd6GetAnycastcache PROTO ((tNd6CacheEntry * pNd6cEntry));


tNd6CacheEntry  *Nd6IsCacheForAddr
                                PROTO ((tIp6If * pIf6, tIp6Addr * pAddr6));

tNd6CacheEntry  *Nd6LookupCache PROTO ((tIp6If * pIf6,
                                             tIp6Addr * pAddr6));

INT4    Nd6UpdateCache          PROTO ((tIp6If * pIf6, tIp6Addr * pAddr6,
                                        UINT1 *pLladdr, UINT1 u1LlaLen,
                                        UINT4 u4AdvFlag, tIp6Hdr * pIp6,
                                        UINT1 u1SecureFlag));

INT4    Nd6PurgeCache           PROTO ((tNd6CacheEntry * pNd6cEntry));

INT4    Nd6PurgeAnycastCache    PROTO ((tNd6CacheEntry * pNd6cEntry));

VOID    Nd6Rcv                  PROTO ((tIp6If * pIf6, tIp6Hdr * pIp6,
                                        UINT1 u1Nd6Type, UINT2 u2Len,
                                        UINT1 u1AddrFlag,
                                        tCRU_BUF_CHAIN_HEADER * pBuf));

VOID    Nd6Resolve              PROTO ((tNd6CacheEntry * pNd6CacheEntry,
                                        tIp6If * pIf6,
                                        tIp6Addr * pDstAddr6,
                                        tCRU_BUF_CHAIN_HEADER * pBuf,
                                        UINT1 u1RouteAddrType));

VOID    Nd6ActOnIfstatus        PROTO ((tIp6If * pIf6, UINT1 u1Status));

VOID    Nd6ActOnRaCnf           PROTO ((tIp6If * pIf6, UINT1 u1Status));

VOID    Nd6UpdateLastUseTime    PROTO ((tNd6CacheEntry * pNd6cEntry));

VOID    Nd6Timeout              PROTO ((UINT1 u1TimerId,
                                        tIp6Timer * pNd6Timer));

UINT1   Nd6GetReachState        PROTO ((tNd6CacheEntry * pNd6cEntry));

VOID    Nd6GetLladdr            PROTO ((tNd6CacheEntry * pNd6cEntry,
                                        UINT1 *pLladdr, UINT1 *pLlaLen));

INT4    Nd6SendNeighSol         PROTO ((tNd6CacheEntry * pNd6cEntry,
                                        tIp6If * pIf6,
                                        tIp6Addr * pSrcAddr6,
                                        tIp6Addr * pDstAddr6,
                                        tIp6Addr * pTargAddr6));

INT4    Nd6SendRedirect         PROTO ((tNd6CacheEntry * pNd6cEntry,
                                        tIp6If * pIf6, tIp6Hdr * pIp6,
                                        tIp6Addr * pAddr6,
                                        tCRU_BUF_CHAIN_HEADER * pBuf));
INT1 
Ip6CheckRAStatus (INT4 i4IfIndex);
tIp6RAEntry *
Ip6RaTblGetEntry (INT4 i4RtrAdvtIfIndex);
INT1    
Ip6GetNextRaTblEntry (INT4 i4RaIfIndex, INT4 *pi4NextRaIfIndex);
tIp6RAEntry *
Ip6CreateRaTblEntry (INT4 i4RtrAdvtIfIndex);
INT1
Ip6DeleteRaTblEntry (INT4 i4RtrAdvtIfIndex);

INT4    Nd6RBTreeCacheEntryCmp PROTO ((tRBElem *, tRBElem *));

VOID    Nd6DeleteContext PROTO ((tIp6Cxt *));

/* Proxy ND API's */

INT4    Ip6IsPktToProxyInCxt  PROTO ((tIp6Cxt *, UINT1 *, tIp6Addr *,
                                      tCRU_BUF_CHAIN_HEADER *));

INT4    Nd6ProxyUpdateInCxt PROTO ((tIp6Cxt *, tIp6Addr, INT4));

tNd6ProxyListNode* Nd6ProxyCheckAddrInCxt PROTO ((tIp6Cxt *, tIp6Addr));

tNd6ProxyListNode* Nd6NextProxyAddrInCxt PROTO ((tIp6Cxt *, tIp6Addr));

/* RIP6 Module */

INT4 Ip6RtLookupInCxt                PROTO ((UINT4 u4ContextId, tIp6Addr * pAddr,
                                        tNetIpv6RtInfo *pNetIpv6Rt));

 
INT4    Ip6AddRouteInCxt             PROTO ((UINT4 u4ContextId, tIp6Addr * pDest,
                                        UINT1 u1Prefixlen,
                                        tIp6Addr * pNexthop, UINT4 u4Metric,
                                        INT4 i4IP6RoutePreference,
                                        INT1 i1Type, INT1 i1Proto,
                                        UINT4 u4Index, UINT1 u1AddrType,
     UINT4 u4RtTag, UINT4 u4RowStatus));


INT4    Ip6DelRouteInCxt             PROTO ((UINT4 u4ContextId, tIp6Addr * pAddr,
                                        UINT1 u1Prefixlen,
                                        INT1 i1Proto, tIp6Addr * pNextHop));

INT4    Ip6DelLocalRoute        PROTO ((tIp6Addr * pAddr, UINT1 u1Prefixlen,
                                        UINT4 u4IfIndex));

INT4    Ip6RoutePurge           PROTO ((tIp6RtEntry * pRt));

/* PING6 Module */


INT4    Ping6Init               PROTO ((VOID));

VOID    Ping6SendEchoReq        PROTO ((tPing6 * pPing6));

VOID    Ping6Rcv                PROTO ((tIcmp6Params * pParam,
                                        tCRU_BUF_CHAIN_HEADER * pBuf));

VOID    Ping6TimerHandler       PROTO ((VOID));

VOID    Ping6TimeoutForDst      PROTO ((tPing6 * pPing6));

INT4    Ipv6AllowUnSecuredPing  PROTO ((VOID));

INT4    Ping6VcmIsVRExist       PROTO ((UINT4));

VOID    Ping6EnableDebug        PROTO ((UINT4));

#ifdef PING6_LNXIP_WANTED
INT4 Ping6LnxOobSend PROTO ((tPing6 * pPing6));
INT4 Ping6LnxOobReplyHandler PROTO ((VOID));
INT4 Ping6GetValidInstanceCount PROTO ((VOID));
INT4 Ping6OpenSocket PROTO ((UINT4));
INT4 Ping6CloseSocket PROTO ((VOID));
#endif

/*
 * Internal Function Prototypes
 */

/* IP6 Module */

INT4    Ip6SpawnTasks           PROTO ((VOID));

INT4    Ip6Init                 PROTO ((VOID));

INT4    Ip6TimerInit            PROTO ((VOID));

INT4    Ip6TaskSemInit          PROTO ((VOID));

INT4 Ip6Enable     PROTO ((VOID));

VOID Ip6Disable              PROTO ((VOID));

INT4    Ip6IfUp                 PROTO ((tIp6If * pIf6));

INT4    Ip6IfDown               PROTO ((tIp6If * pIf6));

INT4    Ip6IfStall              PROTO ((tIp6If * pIf6));

INT4    Ip6ifInitialize         PROTO ((tIp6If * pIf6, UINT1 *pTok,
                                        UINT1 u1Tokl));

VOID    Ip6ifLlaStatus          PROTO ((tIp6If * pIf6, UINT1 u1Status));

INT4    Ip6ifFormLanLla         PROTO ((tIp6If * pIf6));

INT4    Ip6ifFormWanLla         PROTO ((tIp6If * pIf6));

INT4    Ip6ifFormPppLla         PROTO ((tIp6If * pIf6));

INT4    Ip6ifCreateLla          PROTO ((tIp6If * pIf6, UINT1 *pTok,
                                        UINT1 u1Tokl));

VOID    Ip6ifEthSend            PROTO ((tIp6If * pIf6, tIp6Addr * pDstAddr6,
                                        tNd6CacheEntry * pNd6CacheEntry,
                                        UINT4 u4Len,
                                        tCRU_BUF_CHAIN_HEADER * pBuf,
                                        UINT1 u1RouteAddrType));

VOID    Ip6ifPppSend            PROTO ((tIp6If * pIf6, UINT4 u4Len,
                                        tCRU_BUF_CHAIN_HEADER * pBuf));

VOID    Ip6ifFrSend             PROTO ((tIp6If * pIf6, UINT4 u4Len,
                                        tCRU_BUF_CHAIN_HEADER * pBuf));

VOID    Ip6ifX25Send            PROTO ((tIp6If * pIf6, UINT4 u4Len,
                                        tCRU_BUF_CHAIN_HEADER * pBuf));

INT1    Ip6PutFragInQueue       PROTO ((UINT2 u2StartOffset, 
                                        UINT2 u2EndOffset,
                                        tIp6ReasmEntry * pIp6Reasm,
                                        tCRU_BUF_CHAIN_HEADER * pBuf));

tIp6ReasmEntry  *Ip6LookupReasmEntryInCxt PROTO ((tIp6Cxt *,
                                                  tIp6Hdr *, UINT4));

tIp6ReasmEntry  *Ip6CreatReasmEntryInCxt PROTO ((tIp6Cxt *, tIp6Hdr *,
                                                 UINT4, UINT4));

tCRU_BUF_CHAIN_HEADER *Ip6ReasmFrags
                                PROTO ((UINT1 *u1NextHdr, UINT4 *u4Len,
                                        tIp6ReasmEntry * pIp6Reasm));

/* ICMP6 Module */

VOID    Icmp6RcvErrMsg          PROTO ((UINT1 u1Type, UINT1 u1Code,
                                        tIp6If * pIf6,
                                        tCRU_BUF_CHAIN_HEADER * pBuf));

VOID    Icmp6RcvInfoMsg         PROTO ((tIp6If * pIf6, tIp6Hdr * pIp6,
                                        UINT1 u1Type, UINT1 u1Code,
                                        UINT2 u2Len, UINT1 u1Flag,
                                        tCRU_BUF_CHAIN_HEADER * pBuf));
VOID    Icmp6EnqueueInfoMsg     PROTO ((tIp6If * pIf6, tIp6Hdr * pIp6,
                                        UINT1 u1Type, UINT1 u1Code,
                                        UINT2 u2Len,
                                        tCRU_BUF_CHAIN_HEADER * pBuf));

/* ND6 Module */

INT4    Nd6PurgeStalestCache    PROTO ((VOID));

VOID    Nd6PurgeCacheOnIface    PROTO ((tIp6If * pIf6, UINT2 i2_nd6c_flag));

VOID    Nd6PurgePendQue         PROTO ((tNd6CacheEntry * pNd6cEntry));

VOID    Nd6SendPendQue          PROTO ((tNd6CacheEntry * pNd6cEntry));

VOID    Nd6UpdateCacheForEnet   PROTO ((tNd6CacheEntry * pNd6cEntry,
                                        UINT1 *pLladdr, UINT1 u1LlaLen,
                                        UINT4 u4AdvFlag));
VOID
Nd6UpdateAnycastCacheForEnet PROTO(( tNd6CacheEntry * pNd6cEntry, UINT1 *pLladdr, UINT1 u1LlaLen, UINT4 u4AdvFlag, tIp6Hdr *pIp6));


VOID    Nd6CeaseToRoutAdv       PROTO ((tIp6If * pIf6));

VOID    Nd6CacheTimeout         PROTO ((UINT1 u1TimerId,
                                        tNd6CacheEntry * pNd6cEntry));

VOID    Nd6ProbeOnCache         PROTO ((tNd6CacheEntry * pNd6cEntry));

VOID    Nd6RaTimeout            PROTO ((tIp6If * pIf6));

VOID    Nd6SetReachState        PROTO ((tNd6CacheEntry * pNd6cEntry,
                                        UINT1 u1State));

VOID    Nd6SchedNextRoutAdv     PROTO ((tIp6If * pIf6));

VOID    Nd6SetCacheTimer        PROTO ((tNd6CacheEntry * pNd6cEntry,
                                        UINT1 u1TimerId, UINT4 u4TimerVal));
VOID    Nd6SetMSecCacheTimer    PROTO ((tNd6CacheEntry * pNd6cEntry,
                                        UINT1 u1TimerId, UINT4 u4TimerVal));

VOID    Nd6CancelCacheTimer     PROTO ((tNd6CacheEntry * pNd6cEntry));

INT4    Nd6QueueToCache         PROTO ((tNd6CacheEntry * pNd6cEntry,
                                        tCRU_BUF_CHAIN_HEADER * pBuf));

INT4    Nd6SendRoutAdv          PROTO ((tNd6CacheEntry * pNd6cEntry,
                                        tIp6If * pIf6, tIp6Addr * srcAddr6,
                                         tIp6Addr * dstAddr6, UINT1 u1_flags));

INT4    Nd6CheckAndSendNeighSol PROTO ((tNd6CacheEntry * pNd6cEntry,
                                        tIp6Addr * pSrcAddr6));

INT4    Nd6SendNeighAdv         PROTO ((tIp6If * pIf6,
                                        tNd6CacheEntry * pNd6cEntry,
                                        tIp6Addr * pSrcAddr6,
                                        tIp6Addr * pDstAddr6,
                                        tIp6Addr * pTargAddr6,
                                        UINT4 u4AdvFlag));

INT4    Nd6SendSolNeighAdv      PROTO ((tIp6If * pIf6, tIp6Hdr * pIp6,
                                        tNd6NeighSol * pNd6Nsol));

VOID    Nd6SendSolRoutAdv       PROTO ((tIp6If * pIf6,
                                        tIp6Addr * pSrcAddr6,
                                        tIp6Addr * pDstAddr6, 
                                        UINT1 u1SecureFlag));

INT4    Nd6RcvNeighSol          PROTO ((tIp6If * pIf6, tIp6Hdr * pIp6,
                                        UINT2 u2Len,
                                        tCRU_BUF_CHAIN_HEADER * pBuf));

INT4    Nd6ProcessNeighSolExtns PROTO ((tIp6If * pIf6, tIp6Hdr * pIp6,
                                        UINT2 u2ExtnsLen,
                                        tCRU_BUF_CHAIN_HEADER * pBuf));

INT4    Nd6ValidateNeighSolInCxt PROTO ((tIp6Cxt * pIp6Cxt, tIp6Hdr * pIp6,
                                         tNd6NeighSol * pNd6Nsol,
                                         UINT2 u2Len));

INT4    Nd6RcvNeighAdv          PROTO ((tIp6If * pIf6, tIp6Hdr * pIp6,
                                        UINT2 u2Len,
                                        tCRU_BUF_CHAIN_HEADER * pBuf));

INT4 Nd6ProcessNeighAdvExtns PROTO ((tIp6If *pIf6, tIp6Hdr *pIp6,
                         tNd6NeighAdv *pNd6Nadv, UINT4 u4AdvFlag,
                         UINT2 u2ExtnsLen, tCRU_BUF_CHAIN_HEADER *pBuf,
                         UINT1 u1ForDad, UINT1 *pu1NbrSecure));

INT4    Nd6ValidateNeighAdvInCxt PROTO ((tIp6Cxt * pIp6Cxt, tIp6Hdr * pIp6,
                                         tNd6NeighAdv * pNd6Nadv,
                                         UINT4 u4AdvFlag, UINT2 u2Len));

INT4    Nd6RcvRoutSol           PROTO ((tIp6If * pIf6, tIp6Hdr * pIp6,
                                        UINT2 u2Len,
                                        tCRU_BUF_CHAIN_HEADER * pBuf));

INT4    Nd6ProcessRoutSolExtns  PROTO ((tIp6If * pIf6, tIp6Hdr * pIp6,
                                        UINT4 u4ExtnsLen,
                                        tCRU_BUF_CHAIN_HEADER * pBuf));

INT4    Nd6ValidateRoutSolInCxt PROTO ((tIp6Cxt * pIp6Cxt, tIp6Hdr * pIp6,
                                        tNd6RoutSol * pNd6Rsol, UINT2 u2Len));

INT4    Nd6ValidateMsgHdrInCxt  PROTO ((tIp6Cxt * pIp6Cxt, tIp6Hdr * pIp6,
                                        tIcmp6PktHdr *pIcmp6));

INT4    Nd6RcvRoutAdv           PROTO ((tIp6Hdr *pIp6, tIp6If *pIf6,
                                        UINT2 u2Len,
                                        tCRU_BUF_CHAIN_HEADER *pBuf));

INT4    Nd6ValidateRoutAdvInCxt PROTO ((tIp6If * pIf6, tIp6Hdr * pIp6,
                                        tNd6RoutAdv * pNd6RAdv, UINT2 u2Len));

INT4    Nd6ProcessRoutAdvExtns  PROTO ((tIp6Hdr * pIp6, tIp6If * pIf6,
                                        UINT4 u4ExtnsLen,
                                        tCRU_BUF_CHAIN_HEADER * pBuf, 
                                        UINT1 u1PrefAdvFlag, UINT2 u2Lifetime, 
                                        UINT1 *pHAInfo, UINT1 *pu1));

/* Secured Neighbor Discovery */
extern INT1 MsrIsMibRestoreInProgress PROTO ((VOID));

INT4 Nd6SeNDAddOpts PROTO ((tIp6If *pIf6, tIp6Addr *pSrc, tIp6Addr *pDst,
                            tCRU_BUF_CHAIN_HEADER *pBuf,  UINT1 u1SeNDFlag,
                            tIp6Addr *, UINT4 *pu4PktLen));

INT4 Nd6RcvCertPathSol PROTO ((tIp6If *pIf6, tIp6Hdr *pIp6,
                               tCRU_BUF_CHAIN_HEADER *pBuf, UINT2 u2Len));

INT4 Nd6FillCertPathAdvInCxt PROTO ((tIp6Cxt *pIp6Cxt,
                                     tNd6SendCertPathSol *pNd6CertPathSol , 
                                     tCRU_BUF_CHAIN_HEADER *pBuf, UINT4 *pu4BufLen));
INT4 Ip6SeNDVerifyTimeStamp PROTO ((tIp6If *pIf6, tIp6Addr *pAddr6, 
                                    tNd6SendTimeStamp *pRcvdTimestamp,
                                    UINT4 *pu4TSNew));

INT4 Ip6SeNDVerifyNonce PROTO ((tIp6If *pIf6,  tIp6Addr *pSrc,
                                tNd6SendNonce *pRcvdNonce, UINT1 u1SeNDFlag));

INT4 Ip6SeNDAddTimeStamp PROTO ((UINT4 u4ContextId,
                                 tCRU_BUF_CHAIN_HEADER *pBuf,
                                 UINT4 *pu4Len));

INT4 Ip6SeNDAddNonce PROTO ((tIp6If *pIf6,  tIp6Addr *pDst,
                             tCRU_BUF_CHAIN_HEADER *pBuf,
                             UINT1 u1SeNDFlag, UINT4 *pu4Len));

INT4 Nd6ValCertPathSolInCxt PROTO ((tIp6If *pIf6, tIp6Hdr *pIp6,
                                    tCRU_BUF_CHAIN_HEADER *pBuf,
                                    tNd6SendCertPathSol *pNd6CertPathSol,
                                    UINT1 *pu1TrustAnchorBuffer,
                                    UINT2 u2Len, UINT4 *pu4TACheck, 
         UINT4 *pu4TAOptLen));

INT4 Nd6SeNDSolCertPathAdv PROTO ((tIp6If *pIf6, tIp6Hdr *pIp6,
                                   tNd6SendCertPathSol *pNd6CertPathSol,
                                   UINT1 *pu1TrustAnchorBuffer,
                                   UINT4 u4TAOptLen, UINT4 u4TACheck));
INT4
Nd6ValidateCertOptInCxt PROTO ((tIp6If *pIf6, 
                        tCRU_BUF_CHAIN_HEADER *pBuf, UINT2 u2Len));

INT4 Nd6FillTAOptInCxt PROTO ((tIp6Cxt *pIp6Cxt, UINT1 *pu1TrustAnchorBuffer,
                               tCRU_BUF_CHAIN_HEADER *pBuf, UINT4 u4TAOptLen));

INT4 Nd6SendCertPathAdv PROTO ((tIp6If *pIf6, tNd6CacheEntry *pNd6cEntry,
                                tIp6Addr *pSrcAddr6, tIp6Addr *pDstAddr6,
                                tNd6SendCertPathSol *pNd6CertPathSol,
                                UINT1 *pu1TrustAnchorBuffer,
                                UINT4 u4TAOptLen, UINT4 u4TACheck));

INT4 Nd6FillCertOptInCxt PROTO ((tIp6Cxt *pIp6Cxt, UINT1 u1LlaType,
                                 tCRU_BUF_CHAIN_HEADER *pBuf, UINT4 *pu4BuflLen));

INT4 Nd6AddSendOpts PROTO ((tIp6If *pIf6, tIp6Addr *pSrc, tIp6Addr *pDst,
                            tCRU_BUF_CHAIN_HEADER *pBuf, UINT4 u4PktLen));

INT4 Nd6SeNDRsaGenerate PROTO ((tIp6If *pIf6, tIp6Addr *pSrc, tIp6Addr *pDst,
                                tCRU_BUF_CHAIN_HEADER *pBuf, UINT4 u4PktLen,
                                tCgaOptions *pCgaOptions,
                                tRsaOptions *pRsaOptions));

INT4 Nd6SeNDRsaVerify PROTO ((tIp6If *pIf6, tIp6Hdr *pIp6,
                              tCRU_BUF_CHAIN_HEADER *pBuf,
                              UINT4 u4PktLen, tCgaOptions *pCgaOptions,
                              tRsaOptions *pRsaOptions));

INT4 Nd6SeNDAddRsaOpt PROTO ((UINT4 u4ContextId, tCRU_BUF_CHAIN_HEADER *pBuf,
                              tRsaOptions *pRsaOpt, UINT4 *pu4PktLen));

INT4 Nd6SeNDCgaGenerate PROTO ((tIp6If *pIf6, tIp6Addr *pIp6Addr,
                                tCgaOptions *pCgaOptions));

INT4 Nd6SeNDCgaVerify PROTO ((tIp6If * pIf6, tIp6Addr *pIp6Addr,
                              tCgaOptions *pCgaOptions));

INT4 Nd6SeNDAddCgaOpt PROTO ((UINT4 u4ContextId, tCRU_BUF_CHAIN_HEADER *pBuf,
                              tCgaOptions *pCgaOpt, UINT4 *pu4PktLen));

INT4 Nd6SeNDValidateOpts PROTO ((tIp6If *pIf6, tIp6Hdr *pIp6,
                                 tCRU_BUF_CHAIN_HEADER *pBuf, 
                                 UINT2 *pu2PktLen, UINT1 u1SeNDFlag));

INT4 Nd6SeNDProcessRsaOption PROTO ((tIp6If *pIf6, tIp6Hdr *pIp6,
                              tCRU_BUF_CHAIN_HEADER *pBuf,
                              UINT1 u1Len, UINT4 u4NdOffset,
                              tCgaOptions *pCgaOptions));

INT4 Nd6SeNDProcessNonceOption PROTO ((tIp6If *pIf6, tIp6Hdr *pIp6,
                                tCRU_BUF_CHAIN_HEADER *pBuf,
                                UINT1 *pu1Nonce,UINT1 u1SeNDFlag));

INT4 Nd6SeNDProcessTSOption PROTO ((tIp6If *pIf6, tIp6Hdr *pIp6,
                             tCRU_BUF_CHAIN_HEADER *pBuf,
                             UINT4 *pu4TSNew));

INT4 Nd6SeNDProcessCgaOption PROTO ((tIp6If *pIf6, tIp6Hdr *pIp6,
                              tCRU_BUF_CHAIN_HEADER *pBuf,
                              UINT1 u1Len, tCgaOptions *pCgaOptions));

INT4
Nd6SeNDCertPathSol PROTO ((tIp6If *pIf6, tIp6Hdr *pIp6));


INT4
Nd6SendCertPathSol PROTO ((tIp6If *pIf6, tNd6CacheEntry *pNd6cEntry,
                    tIp6Addr *pSrcAddr6, tIp6Addr *pDstAddr6));

INT4
Nd6FillCertPathSolInCxt PROTO ((tIp6Cxt *pIp6Cxt,
                         tCRU_BUF_CHAIN_HEADER *pBuf, UINT4 *pu4BufLen));

INT4
Nd6FillCertSolTAOptInCxt PROTO ((tIp6Cxt *pIp6Cxt,
                          tCRU_BUF_CHAIN_HEADER *pBuf, UINT4 *pu4BufLen));
INT4
Nd6RcvCertPathAdv PROTO ((tIp6If *pIf6, tIp6Hdr *pIp6,
                   tCRU_BUF_CHAIN_HEADER *pBuf, UINT2 u2Len));

INT4
Nd6ValCertPathAdvInCxt PROTO ((tIp6If *pIf6, tIp6Hdr *pIp6,
                        tCRU_BUF_CHAIN_HEADER *pBuf,
                        tNd6SendCertPathAdv *pNd6CertPathAdv,
                        UINT1 *pu1TrustAnchorBuffer, UINT2 u2Len));

INT4
Nd6SecureCheck PROTO ((INT4 i4IfIndex));

UINT4
Ip6GetIfReTransTime (UINT4 u4IfIndex);
UINT4
Ip6GetIfReachTime (UINT4 u4IfIndex);

tNd6ExtHdr *Nd6ExtractExtnHdrInCxt PROTO ((tIp6Cxt * pIp6Cxt, 
                                           tCRU_BUF_CHAIN_HEADER * pBuf));

VOID    Nd6IndicateDadStatus    PROTO ((tIp6If * pIf6, tIp6Addr * pTargAddr6,
                                        UINT1 u1Status));


VOID UpdateNDCounters (UINT1 u1PrevState, UINT1 u1NewState);

VOID IncrementNDCounters (UINT1 u1NewState);

VOID DecrementNDCounters (UINT1 u1PrevState);

VOID ResetNDCounters(VOID);

VOID    Nd6CeaseRAOnAddr PROTO ((tIp6Addr * pAddr6, tIp6If *pIf6));

VOID    Nd6AddOrDeleteCacheEntriesInCxt PROTO ((UINT4 u4ContextId,
                                              INT4  i4ForwardStatus));
INT4  Nd6RBTreeRedEntryCmp PROTO ((tRBElem *, tRBElem *));
INT4  Nd6RedInitGlobalInfo PROTO ((VOID));
INT4  Nd6RedRmRegisterProtocols PROTO ((tRmRegParams *pRmReg));
INT4  Nd6RedRmDeRegisterProtocols PROTO ((VOID));
INT4  Nd6RedRmCallBack PROTO ((UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen));
VOID  Nd6RedHandleRmEvents PROTO ((VOID));
VOID  Nd6RedHandleGoActive PROTO ((VOID));
VOID  Nd6RedHandleGoStandby PROTO ((VOID));
VOID  Nd6RedHandleStandbyToActive PROTO ((VOID));
VOID  Nd6RedHandleActiveToStandby PROTO ((VOID));
VOID  Nd6RedHandleIdleToActive PROTO ((VOID));
VOID  Nd6RedHandleIdleToStandby PROTO ((VOID));
VOID  Nd6RedProcessPeerMsgAtActive PROTO ((tRmMsg * pMsg, UINT2 u2DataLen));
VOID  Nd6RedProcessPeerMsgAtStandby PROTO ((tRmMsg * pMsg, UINT2 u2DataLen));
UINT4 Nd6RedRmHandleProtocolEvent PROTO ((tRmProtoEvt *pEvt));
INT4  Nd6RedRmReleaseMemoryForMsg PROTO ((UINT1 *pu1Block));
INT4  Nd6RedSendMsgToRm PROTO ((tRmMsg * pMsg , UINT2 u2Length ));
VOID  Nd6RedSendBulkReqMsg PROTO ((VOID));
INT4  Nd6RedDeInitGlobalInfo PROTO ((VOID));
VOID  Nd6RedSendBulkUpdTailMsg PROTO ((VOID));
VOID  Nd6RedProcessBulkTailMsg PROTO ((tRmMsg * pMsg, UINT2 *pu2OffSet));
VOID  Nd6RedSendDynamicCacheInfo PROTO ((VOID));
VOID  Nd6RedSendBulkUpdMsg PROTO ((VOID));
VOID  Nd6RedSendDynamicBulkMsg PROTO ((VOID));
VOID  Nd6RedProcessBulkInfo PROTO ((tRmMsg * pMsg, UINT2 *pu2OffSet));
VOID  Nd6RedProcessDynamicInfo PROTO ((tRmMsg * pMsg, UINT2 *pu2OffSet));
INT4  Nd6RedSendBulkCacheInfo PROTO((UINT1 *pu1BulkUpdPendFlg));
VOID  Nd6RedAddDynamicInfo PROTO ((tNd6CacheEntry *, UINT1 *, UINT1));
VOID  Nd6RedHwAudit PROTO ((VOID));
INT4  Nd6StartTimers (tRBElem * pRBElem, eRBVisit visit, UINT4 u4Level,
                      VOID *pArg, VOID *pOut);


/* IP Fwd DB Module */

VOID    Ip6LocalRouteAdd        PROTO ((UINT4 u4Index, tIp6Addr * pAddr,
                                        UINT1 u1Prefixlen));

/* PING6 Module */

VOID    Ping6Send               PROTO ((tIcmp6Params * pParam,
                                        tCRU_BUF_CHAIN_HEADER * pBuf));

VOID    Ping6Update             PROTO ((tPing6 * pPing6, UINT2 u2Seq));

VOID    Ping6PutSeqStartTime    PROTO ((tPing6 * pPing6));

/* PMTU Module */
INT4    ip6PmtuInit             PROTO ((VOID));

INT4    ip6PmtuLookupEntryInCxt PROTO ((UINT4 u4ContextId, tIp6Addr * pIp6Addr,
                                        tPmtuEntry ** pSearchEntry));

INT4    ip6PmtuShut             PROTO ((VOID));

VOID    ip6PmtuTimerExpiryHandler PROTO ((tIp6Timer * pTimer));

VOID    Icmp6RateLimitTimeout   PROTO ((UINT1 u1TimerId, tIp6Timer * pTimer));

UINT1   Icmp6RatelimitErrMsg    PROTO ((tIp6If * pIf6));

VOID    ip6PmtuReleaseEntry     PROTO  ((tPmtuEntry *));

INT4    Ip6GetProfileIndex      PROTO ((INT4 ,tIp6Addr, INT4 ));

INT4    Ip6GetPrefixAddrLen     PROTO ((UINT2 ,INT4 ,tIp6Addr *,INT4 *));

INT4    Nd6FillLladdrInCxt      PROTO ((tIp6Cxt * pIp6Cxt, UINT1 *pLladdr,
                                        UINT1 u1LlaType,
                                        tCRU_BUF_CHAIN_HEADER * pBuf));

INT4    Ip6SendIfCheck          PROTO ((tIp6If * pIf6));

VOID    Ip6AddrDeleteInfo       PROTO ((UINT1 *pAddrInfo, UINT1 u1Type));

VOID    Ip6ifActOnIfDown        PROTO ((tIp6If * pIf6));

/* snmp related prototypes */

INT1    Ip6SetIpv6ForwardingInCxt    PROTO ((tIp6Cxt *, UINT4));

INT1    Ip6GetFsipv6IfRouterAdvStatus
                                PROTO ((INT4 i4Ipv6IfIndex,
                                        INT4 *pi4RetValIpv6IfRouterAdvStatus));

INT1    Ip6GetFsipv6IfRouterAdvFlags
                                PROTO ((INT4 i4Ipv6IfIndex,
                                        INT4 *pi4RetValIpv6IfRouterAdvFlags));

INT1    Ip6SetFsipv6IfRouterAdvStatus
                                PROTO ((INT4 i4Ipv6IfIndex,
                                        INT4 i4SetValIpv6IfRouterAdvStatus));

INT1    Ip6SetFsipv6IfRouterAdvFlags
                                PROTO ((INT4 i4Ipv6IfIndex,
                                        INT4 i4SetValIpv6IfRouterAdvFlags));

INT1    Ip6Testv2Fsipv6IfRouterAdvStatus 
                                PROTO ((UINT4 *pu4ErrorCode, 
                                        INT4 i4Ipv6IfIndex,
                                        INT4 i4TestValIpv6IfRouterAdvStatus));

INT1    Ip6Testv2Fsipv6IfRouterAdvFlags 
                                PROTO ((UINT4 *pu4ErrorCode, 
                                        INT4 i4Ipv6IfIndex,
                                        INT4 i4TestValIpv6IfRouterAdvFlags));

INT1    Ip6Testv2Fsipv6IfPrefixAdvStatus 
                                PROTO ((UINT4 *pu4ErrorCode, 
                                        INT4 i4Ipv6IfIndex,
                                        INT4 i4TestValIpv6IfPrefixAdvStatus));

INT1    Ip6Testv2Fsipv6IfMinRouterAdvTime 
                                PROTO ((UINT4 *pu4ErrorCode, 
                                        INT4 i4Ipv6IfIndex,
                                        INT4 i4TestValIpv6IfMinRouterAdvTime));

INT1    Ip6Testv2Fsipv6IfMaxRouterAdvTime 
                                PROTO ((UINT4 *pu4ErrorCode, 
                                        INT4 i4Ipv6IfIndex,
                                        INT4 i4TestValIpv6IfMaxRouterAdvTime));

INT1    Ip6Testv2Fsipv6IfDefRouterTime 
                                PROTO ((UINT4 *pu4ErrorCode, 
                                        INT4 i4Ipv6IfIndex,
                                        INT4 i4TestValIpv6IfDefRouterTime));


INT1    Ip6GetFsipv6PrefixProfileIndex
                                PROTO ((INT4 i4Fsipv6PrefixIndex,
                                        tSNMP_OCTET_STRING_TYPE *pFsipv6PrefixAddress,
                                        INT4 i4Fsipv6PrefixAddrLen,
                                        INT4 *pi4RetValFsipv6PrefixProfileIndex));

INT1    Ip6GetFsipv6SupportEmbeddedRp
                                PROTO ((INT4 i4Fsipv6PrefixIndex,
                                        tSNMP_OCTET_STRING_TYPE *pFsipv6PrefixAddress,
                                        INT4 i4Fsipv6PrefixAddrLen,
                                        INT4 *pi4RetValFsipv6SupportEmbeddedRp));

INT1    Ip6GetFsipv6PrefixAdminStatus
                                PROTO ((INT4 i4Fsipv6PrefixIndex,
                                        tSNMP_OCTET_STRING_TYPE *pFsipv6PrefixAddress,
                                        INT4 i4Fsipv6PrefixAddrLen,
                                        INT4 *pi4RetValFsipv6PrefixAdminStatus));

INT1    Ip6SetFsipv6PrefixProfileIndex
                                PROTO ((INT4 i4Fsipv6PrefixIndex,
                                        tSNMP_OCTET_STRING_TYPE *pFsipv6PrefixAddress,
                                        INT4 i4Fsipv6PrefixAddrLen,
                                        INT4 i4SetValFsipv6PrefixProfileIndex));

INT1    Ip6SetFsipv6SupportEmbeddedRp
                                PROTO ((INT4 i4Fsipv6PrefixIndex,
                                        tSNMP_OCTET_STRING_TYPE *pFsipv6PrefixAddress,
                                        INT4 i4Fsipv6PrefixAddrLen,
                                        INT4 i4SetValFsipv6SupportEmbeddedRp));

INT1    Ip6SetFsipv6PrefixAdminStatus
                                PROTO ((INT4 i4Fsipv6PrefixIndex,
                                        tSNMP_OCTET_STRING_TYPE *pFsipv6PrefixAddress,
                                        INT4 i4Fsipv6PrefixAddrLen,
                                        INT4 i4SetValFsipv6PrefixAdminStatus));

INT1    Ip6Testv2Fsipv6PrefixProfileIndex
                                PROTO ((UINT4 *pu4ErrorCode,
                                        INT4 i4Fsipv6PrefixIndex,
                                        tSNMP_OCTET_STRING_TYPE *pFsipv6PrefixAddress,
                                        INT4 i4Fsipv6PrefixAddrLen,
                                        INT4 i4TestValFsipv6PrefixProfileIndex));

INT1    Ip6Testv2Fsipv6SupportEmbeddedRp
                                PROTO ((UINT4 *pu4ErrorCode,
                                        INT4 i4Fsipv6PrefixIndex,
                                        tSNMP_OCTET_STRING_TYPE *pFsipv6PrefixAddress,
                                        INT4 i4Fsipv6PrefixAddrLen,
                                        INT4 i4TestValFsipv6SupportEmbeddedRp));

INT1    Ip6Testv2Fsipv6PrefixAdminStatus
                                PROTO ((UINT4 *pu4ErrorCode,
                                        INT4 i4Fsipv6PrefixIndex,
                                        tSNMP_OCTET_STRING_TYPE *pFsipv6PrefixAddress,
                                        INT4 i4Fsipv6PrefixAddrLen,
                                        INT4 i4TestValFsipv6PrefixAdminStatus));


INT1 Ip6GetFsipv6AddrSelPolicyAddrType
                  PROTO ((INT4 i4Fsipv6AddrSelPolicyIfIndex ,
                          tSNMP_OCTET_STRING_TYPE *pFsipv6AddrSelPolicyPrefix,
                          INT4 i4Fsipv6AddrSelPolicyPrefixLen,
                          INT4 *pi4RetValFsipv6AddrSelPolicyAddrType));

INT1 Ip6UtilClearContextInfo PROTO ((INT4 i4ContextId));
INT1 Ip6UtilClearPmtuTable PROTO ((UINT4 u4ContextId, tSNMP_OCTET_STRING_TYPE *NxtDest));
INT1 Ip6UtilClearPreferenceTable PROTO ((INT4 i4ContextId, INT4 i4NxtProtocol, INT4 i4SetPreference));
INT1 Ip6UtilClearNdProxyTable PROTO ((UINT4 u4ContextId, tSNMP_OCTET_STRING_TYPE *NxtAddr));
INT1 Ip6UtilClearAddressTable PROTO ((INT4 i4NxtIndex, tSNMP_OCTET_STRING_TYPE *NxtAddr, INT4 i4NxtPrefixLen));
INT1 Ip6UtilClearPrefixTable PROTO ((INT4 i4NxtIndex, tSNMP_OCTET_STRING_TYPE *NxtAddr, INT4 i4NxtPrefixLen));
INT1 Ip6ClearIfTable PROTO ((INT4 u4IfIndex));
INT1 Ip6UtilClearIpv6InterfaceTable PROTO ((INT4 i4IfIndex));

INT4    Nd6FillPrefixes         PROTO ((tTMO_SLL * pAddr6List,
   tTMO_SLL_NODE ** ppCur,
   INT2 i2Prefixes,
   INT2 *pPrefSeen,
   UINT2 *pu2Offset,
   tCRU_BUF_CHAIN_HEADER * pBuf));

INT4    Nd6RcvRouterRedirect    PROTO ((tIp6If *pIf6 ,tIp6Hdr *pIp6,
                                        UINT2 u2Len,
                                        tCRU_BUF_CHAIN_HEADER *pBuf));

UINT4   Ip6GetNextFreeProfileIndex PROTO ((tIp6If *pIf6));

INT4    Nd6SendRouterSol        PROTO ((tIp6If *pIf6,tIp6Addr *pDest));

VOID    Nd6RsTimeout            PROTO ((tIp6If  *pIf6));

VOID    Nd6AddDefRouterList     PROTO ((tIp6If *pIf6, 
                                        tIp6Addr *pSrcAddr, UINT2 u2Lifetime,
                                        UINT1 u1SecureFlag));

VOID    Nd6UpdateDestCacheEntry PROTO ((tIp6If *pIf6, tIp6Hdr *pIp6, 
                                        tNd6Redirect *pNd6Rdirect));

INT1    Nd6ProcessPrefixOption  PROTO ((tNd6PrefixExt *p_nd6_prefix,
                                        tIp6If *pIf6,tIp6Hdr *pIp6Hdr));

INT4    Nd6PurgeStalestDefRt    PROTO ((VOID));

INT4    Nd6ProcessRouterAdv     PROTO ((tIp6If *pIf6, tIp6Hdr *pIp6,
                                        tNd6RoutAdv *pNd6RAdv, 
                                        UINT1 u1SecureFlag));

VOID    Nd6SchedNextRoutSol     PROTO ((tIp6If *pIf6));

INT4   Ip6CreateNewProfileIndex PROTO ((tNd6PrefixExt *p_nd6_prefix,
                                        tIp6If  *pIf6));

VOID    Ip6UpdateRoutingDataBase PROTO ((tNd6CacheEntry *pNd6cEntry));

UINT4   Ip6GetMatchProfileIndex  PROTO ((tNd6PrefixExt * pNd6Prefix,
                                        tIp6If * pIf6));


INT4    Nd6ProcessSLLAOption    PROTO ((UINT4 u4ExtnsLen, 
                                        tNd6ExtHdr *pNd6Ext,tIp6If *pIf6,
                                        tNd6AddrExt *pNd6Lla,
                                        tNd6AddrExt **ppNd6Lla,
                                        tCRU_BUF_CHAIN_HEADER *pBuf));

INT4    Nd6ProcessTLLAOption    PROTO ((UINT4 u4ExtnsLen, 
                                        tNd6ExtHdr *pNd6Ext,  tIp6If * pIf6,
                                        tNd6AddrExt *nd6Lla,
                                        tNd6AddrExt **ppNd6Lla,
                                        tCRU_BUF_CHAIN_HEADER *pBuf));

VOID    Nd6CalNewReachTime      PROTO ((tIp6If *pIf6, 
                                        UINT4 u4ReachTime));
VOID    Nd6TimeOutDefRoute      PROTO ((tNd6RtEntry *pDefRtEntry));


INT4    Nd6ValidateRedirectMsgInCxt    PROTO ((tIp6Cxt * pIp6Cxt,
                                               tIp6Hdr *pIp6, 
                                               tNd6Redirect *pNd6Rdirect,
                                               UINT2 u2Len));

INT4    Nd6ProcessRedirectExtns PROTO ((tIp6Hdr *pIp6, tIp6If *pIf6, 
                                        tNd6Redirect *pNd6Rdirect, 
                                        tCRU_BUF_CHAIN_HEADER *pBuf, 
                                        UINT4 u4ExtnsLen));

VOID    Nd6AddrPrefLifeTimeHandler PROTO (( tIp6Timer * pTimerNode));

VOID    Nd6AddrValidLifeTimeHandler PROTO (( tIp6Timer * pTimerNode));

VOID    ND6DeleteRoutes PROTO ((UINT4 u4Index));

INT4    Nd6GetDefRtrLifetime PROTO ((tIp6Addr *, INT4, UINT4 *));

INT4    Nd6GetNextDefRtr PROTO ((tIp6Addr *, tIp6Addr *, INT4, INT4 *));

UINT1   Ip6RetAddrStatComplete  PROTO ((tIp6If *pIf6));

VOID    Ip6Rcv                  PROTO ((UINT1 u1Type, UINT4 u4Len,
                                        tIp6Hdr * pIp6, tIp6If * pIf6,
                                        tCRU_BUF_CHAIN_HEADER * pBuf));

/* IP6 Tunnel Module */

#ifdef TUNNEL_WANTED
INT4    Ip6TunlEntryExists      PROTO ((INT4 i4TunlIndex,
                                        tIp6Addr *pTunlDstAddress));
 
INT4    Ip6TunlAccessListInit   PROTO ((VOID));

VOID    Ip6Tunl6Send            PROTO ((tIp6If *pIf6,
                                        tCRU_BUF_CHAIN_HEADER *pBuf));

VOID    Ip6TunlOption           PROTO ((tCRU_BUF_CHAIN_HEADER *pBuf,
                                        UINT1 u1Tel));

tIp6If *Ip6TunnelEntryGetInCxt  PROTO ((tIp6Cxt *pIp6Cxt, tIp6Addr *pDst));

VOID    Ip6Tunl6Recv            PROTO ((tIp6If *pIf6,
                                        tCRU_BUF_CHAIN_HEADER *pBuf));

VOID    Ip6Tunl4Send            PROTO ((tIp6If *pIf6,tIp6Addr *pDst6,
                                        tNd6CacheEntry *pNd6CacheEntry,
                                        UINT4 u4Len,
                                        tCRU_BUF_CHAIN_HEADER *pBuf));

UINT1   Ip6GetHlim              PROTO ((VOID ));


tIp6If *Ip6GetIfTunlInCxt       PROTO ((tIp6Cxt *, UINT4, UINT4, UINT4));

INT4    Ip6SendNdMsgOverTunl    PROTO ((tIp6If * pIf6,
                                        tNd6CacheEntry * pNd6cEntry,
                                        tIp6Addr * pSrcAddr6,
                                        tIp6Addr * pDstAddr6,
                                        UINT4 u4Nd6Size,
                                        tCRU_BUF_CHAIN_HEADER * pBuf));

VOID    Ip6ifTunlSend           PROTO ((tIp6If * pIf6, tIp6Addr * pDst6,
                                        tNd6CacheEntry * pNd6cEntry,
                                        UINT4 u4Len,
                                        tCRU_BUF_CHAIN_HEADER * pBuf));

INT4    Ip6GetTunnelParams      PROTO ((UINT4 u4IfIndex,
                                        tIp6TunlIf *pIp6TunlIf));

INT4    Ip6ifCreateV4compatv6Addr PROTO ((tIp6If *pIf6));

INT4    Ip6ifDeleteV4compatv6Addr PROTO ((tIp6If *pIf6));
#endif  /* TUNNEL_WANTED */

tIp6If *Ip6LocateIf             PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf));

VOID    Ip6Input                PROTO ((tIp6If * pIf6,
                                        tCRU_BUF_CHAIN_HEADER * pBuf,tIp6Addr *));
INT4    Ip6PrcsUnRecOpt         PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf,
                                        UINT1 *pu1ByteToRead,
                                        UINT4 *pu4BalanceBytes, tIp6If * pIf6,
                                        tIp6Hdr * pIp6));

#ifdef MIP6_WANTED
VOID    Ip6CopyRestofAddrBits   PROTO ((tIp6Addr *pPref, tIp6Addr *pAddr,
                                        INT4 i4NumBits));
#endif

#ifdef HA_WANTED
INT4    Ip6IsAddrMNHomeAddr     PROTO ((tIp6If *pIf6, tIp6Addr *pAddr,
                                        UINT1 *pu1AddrType));
#endif

INT4    Ip6AddEntryToMACFilter        PROTO ((UINT4 u4Index,tIp6Addr *pAddr));

INT4    Ip6AddMACAddrInMACFilter      PROTO ((UINT4 u4Index,UINT1 *pMacAddr));

INT4    Ip6AddMcastAddrInMACFilter    PROTO ((UINT4 u4Index,
                                              tIp6Addr *pMcastAddr));

INT4    Ip6ChkEntryInMACFilter        PROTO((UINT4 u4IfIndex,UINT1 *pMacAddr));

INT4    Ip6DeleteEntryFromMACFilter   PROTO((UINT4 u4Index,tIp6Addr *pAddr));

INT4    Ip6DeleteMACAddrFromMACFilter PROTO((UINT4 u4Index));

INT4    Ip6DeleteMcastAddrInMACFilter PROTO ((UINT4 u4Index,
                                              tIp6Addr *pMcastAddr));
INT4    Ip6DeleteMACAddrInMACFilter   PROTO ((UINT4 u4Index, UINT1 *pMacAddr));
INT4    Ip6RaPrefixNotify             PROTO ((tIp6PrefixNode *pPrefixInfo,
                                              UINT4 u4PrefixStatus));
    
tIp6PrefixNode  *Ip6RAPrefixCreate    PROTO ((UINT4 u4IfIndex,
                                              tIp6Addr *pPrefix,
                                              UINT1 u1PrefixLen,
                                              UINT2 u2Addr6Profile,
                                              UINT1 u1AdminStatus));


INT1    Ip6RAPrefixDelete             PROTO ((UINT4 u4IfIndex,
                                              tIp6Addr *pPrefix,
                                              UINT1 u1PrefixLen));

tIp6PrefixNode  *Ip6GetRAPrefix       PROTO ((UINT4 u4IfIndex,
                                              tIp6Addr *pPrefix,
                                              UINT1 u1PrefixLen));

tIp6PrefixNode  *Ip6GetFirstPrefix    PROTO ((UINT4 u4IfIndex));

tIp6PrefixNode  *Ip6GetNextPrefix     PROTO ((UINT4 u4IfIndex,
                                              tIp6Addr *pIp6Addr,
                                              UINT1 u1PrefixLen));


INT4    Ip6GetHlProtocolFromHLPktInCxt PROTO ((tIp6Cxt * pIp6Cxt,
                                               tCRU_BUF_CHAIN_HEADER * ,
                                              UINT1 *, UINT2 *));

INT4    Ip6GetHlProtocolInCxt          PROTO ((UINT4 u4ContextId, 
                                               tCRU_BUF_CHAIN_HEADER *pBuf, 
                                               UINT1 *pu1Nhdr,
                                               UINT2 *pu2Offset));

/* This function is defined based on router mode or host mode in ip6rtr
 *  and ip6host respectively.
 */
INT1    Ip6GetFsipv6AddrOperStatus
                                PROTO ((INT4 i4Ipv6AddrIndex,
                                        tSNMP_OCTET_STRING_TYPE *pIpv6Address,
                                        INT4 i4Ipv6AddrPrefixLen,
                                        INT4 *pi4RetValIpv6AddrOperStatus));

INT1    Ip6GetIpv6AddrStatus    PROTO ((INT4 i4Ipv6IfIndex,
                                        tSNMP_OCTET_STRING_TYPE *pIpv6Address,
                                        INT4 *pi4RetValIpv6AddrStatus));

INT1    Ip6Testv2Ipv6Forwarding PROTO ((UINT4 *pu4ErrorCode, 
                                        INT4 i4TestValIpv6Forwarding));

INT4    Ip6GetAddrPrefixLen     PROTO ((INT4 i4Ipv6IfIndex,
                                        tIp6Addr * pIpv6AddrAddress, 
                                        INT4 *pi4Ipv6AddrPrefixLength));

INT4  Ip6CheckAutoTunlAddr    PROTO ((tIp6Addr *pAddr, INT4 i4Index));

INT4 Ip6IsAddrAnycastInCxt PROTO ((tIp6Cxt *pIp6Cxt, tIp6Addr *pIp6Addr));

INT1
Ip6GetNextNdLanCacheTableEntry  PROTO((INT4 i4IfIndex, INT4 *pi4IfIndex,
                                tIp6Addr ipv6Addr, tIp6Addr *pipv6Addr));

INT4
Ip6SendMcastPkt PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf,
                 tHlToIp6McastParams * pIp6McastHlParams));

VOID
IncMsrForIpv6NetToPhyTable PROTO ((INT4, tSNMP_OCTET_STRING_TYPE *,
                                   CHR1, VOID *, UINT4 *, UINT4, UINT1));

VOID
IncMsrForIpv6ContextTable PROTO ((UINT4, CHR1, VOID *, UINT4 *, UINT4));

VOID
IncMsrForIpv6AddrTable  (INT4 i4Index, tSNMP_OCTET_STRING_TYPE * Ipv6Address,
                         INT4 i4PrefixLen, CHR1 cDatatype, VOID *pSetVal, 
                         UINT4 *pu4ObjectId, UINT4 u4OIdLen, UINT1 IsRowStatus);
VOID
IncMsrForIpv6RARouteTable (INT4 i4Index, tSNMP_OCTET_STRING_TYPE * Ipv6Address,
                        INT4 i4PrefixLen, CHR1 cDatatype, VOID *pSetVal,
                        UINT4 *pu4ObjectId, UINT4 u4OIdLen, UINT1 IsRowStatus);

VOID
IncMsrForIpv6AddrProfileTable   (UINT4 u4ProfileIndex, CHR1 cDatatype,
                                 VOID *pSetVal, UINT4 *pu4ObjectId,
                                 UINT4 u4OIdLen);
VOID
IncMsrForIpv6PmtuTable (UINT4 u4ContextId, tSNMP_OCTET_STRING_TYPE *pIpAddress,
                        INT4 i4SetVal, UINT4 *pu4ObjectId, UINT4 u4OIdLen);

VOID
IncMsrForIpv6IfTable PROTO ((INT4 i4IfIndex, CHR1 cDatatype, VOID *pSetVal,
                             UINT4 *pu4ObjectId, UINT4 u4OIdLen));

VOID
IncMsrForIp6PrefixTable PROTO ((INT4 i4IfIndex, INT4 i4PrefixLen,
                                tSNMP_OCTET_STRING_TYPE * pIpAddress,
                                INT4 i4SetVal, UINT4 *pu4ObjectId,
                                UINT4 u4OIdLen, UINT1 IsRowStatus));

extern INT1
IncMsrForIPVXIfTable ARG_LIST ((INT4 i4Ipv6InterfaceIfIndex,
                                INT4 i4SetVal, UINT4 *pu4ObjectId,
                                UINT4 u4OIdLen, UINT1 IsRowStatus));
extern VOID
IncMsrForIpv6RouteTable ARG_LIST ((UINT4 u4ContextId,
                                   tSNMP_OCTET_STRING_TYPE * pDestIp,
                                   INT4 i4PrefixLen, 
                                   tSNMP_OCTET_STRING_TYPE * pNextHop,
                                   CHR1 cDatatype, VOID *pSetVal,
                                   UINT4 *pu4ObjectId, UINT4 u4OIdLen,
                                   UINT1 IsRowStatus));
extern INT1
IncMsrForIpvxGlbTable ARG_LIST ((UINT4 u4ContextId, INT4 i4SetVal,
                                 UINT4 *pu4ObjectId, UINT4 u4OIdLen));

VOID
Ip6HandleIntfMapEvent PROTO ((UINT4, UINT4, UINT4));

INT4
Ip6HandleDeleteContext PROTO ((UINT4));

INT4
Ip6HandleCreateContext PROTO ((UINT4));

INT1
Ip6UnmapIf PROTO ((tIp6If *));

INT1
Ip6MapIf PROTO ((tIp6If * pIf6,tIp6Cxt *pIp6Cxt));

VOID
Ip6ClearReasmEntries PROTO ((tIp6Cxt *pIp6Cxt));

INT4 
Ip6GetDefForwardStatus  PROTO ((VOID));

INT4
Ip6UtilGetNextCxtId (UINT4 u4Ip6CxtId, UINT4 *pu4NextIp6CxtId);

INT4
Ip6UtilGetFirstCxtId (UINT4 *pu4Ip6CxtId);

tIp6Cxt *
Ipv6UtilGetCxtEntryFromIfIndex (UINT4 u4IfIndex);

tIp6Cxt *
Ipv6UtilGetCxtEntryFromCxtId (UINT4 u4ContextId);

INT4
Ip6GetIfIndexFromNameInCxt (UINT4 u4L2ContextId, UINT1 *au1IfName,
                            UINT4 *pu4IfIndex);

INT4
Udp6AddCBNodeToCxtTable (UINT4 u4ContextId, tUdp6Port * pUdp6CbNode);

INT4
Udp6DeleteCBNodeFromCxtTable (UINT4 u4ContextId, tUdp6Port  * pUdp6CbNode);

INT4
Udp6AddCBNodeToGlbTable (tUdp6Port  * pUdp6CbNode);

INT4
Udp6DeleteCBNodeFromGlbTable (tUdp6Port  * pUdp6CbNode);


tIp6Cxt *
Ipv6UtilGetCurrentCxtEntry (VOID);


INT4
Ip6VRExists (UINT4 u4ContextId);

tIp6AddrInfo * 
Ip6GetAddressInfo PROTO ((UINT4 u4ContextId, tIp6Addr Ip6Addr));

tIp6AddrInfo * 
Ip6GetNextAddressInfo PROTO ((UINT4 u4ContextId, tIp6Addr Ip6Addr));

VOID
Ip6ClearPmtuEntries PROTO ((tIp6Cxt *pIp6Cxt));

INT4
Ip6GetDebugFlag PROTO ((UINT4 u4ContextId));

VOID
Ipv6UtilGetConfigMethod PROTO ((UINT4 u4IfIndex,
                                tSNMP_OCTET_STRING_TYPE * pFsipv6AddrAddress,
                                UINT1 u1PrefixLen, UINT1 u1AddressType,
                                UINT1 *pu1CfgMethod));

INT4
Ip6UtlConstructRpAddr (tIp6Addr * pMcastAddr, tIp6Addr * pPrefix,
                       UINT1 * pu1InterfaceId, UINT1 u1PrefixLen,
         UINT1 u1Status);

INT4
Ip6UtlCheckIfForEmbdRp (tIp6If *pIf6, tIp6Addr *pPrefix, UINT1 u1PrefixLen,
                           UINT1 *pu1EmbdRpFlag);
INT4
Ip6UtlFillRpInfo (tIp6Addr *pPrefix, UINT4 u4OutIfIndex,
                  UINT1 u1IsSelfNode, tIp6RpAddrInfo *pIp6RpAddrInfo);
INT4
Ip6UtlCheckRouteForEmbdRpInCxt (UINT4 u4ContextId, tIp6Addr *pPrefix,
                                UINT4 *pu4OutIfIndex);

INT4
Ip6DeleteInterfaceInfo PROTO ((UINT4 u4IfIndex));

INT4
Ip6DeleteContextInfo PROTO ((UINT4 u4ContextId));

/* Rtm6 utilities */
PUBLIC INT4
UtilRtm6SetContext PROTO ((UINT4 u4Rtm6CxtId));

PUBLIC VOID
UtilRtm6ResetContext PROTO ((VOID));

PUBLIC INT4
UtilRtm6GetFirstCxtId PROTO ((UINT4 *pu4Rtm6CxtId));

PUBLIC INT4
UtilRtm6GetNextCxtId PROTO ((UINT4 u4Rtm6CxtId, UINT4 *pu4NextRtm6CxtId));

PUBLIC INT4
UtilRtm6VcmIsVcExist PROTO ((UINT4 u4Rtm6CxtId));

PUBLIC INT4
UtilRtm6GetCurrCxtId PROTO ((VOID));

INT4
Ip6GetBestRouteEntryInCxt  (struct Rtm6Cxt *pRtm6Cxt, tIp6Addr *pDest, UINT1 u1PrefixLen,                                                                                                           tIp6RtEntry **ppIp6OutRtInfo);
PUBLIC struct Rtm6Cxt * UtilRtm6GetCxt PROTO ((UINT4 u4Rtm6CxtId));
INT4 Rtm6AddRtToPRTInCxt (struct Rtm6Cxt * pRtm6Cxt, tIp6RtEntry * pPRoute);


/* IPv6 Address */


INT4 Ip6AddrTblNextAddr PROTO ((UINT4 *pu4Index, tIp6Addr * pAddr6,
                                    UINT1 *pu1Prefixlen, UINT1 u1SrchFlag));

tIp6LlocalInfo  *Ip6AddrTblGetLlocalEntry PROTO ((UINT4 u4Index,
                                                          tIp6Addr * pAddr6));

tIp6AddrInfo       *
Ip6AddrAllIfScanInCxt PROTO ((UINT4 u4ContextId, tIp6Addr * pAddr6,
                              UINT1 u1Prefixlen));

tIp6If  *
Ip6AddrScanForInterface PROTO ((tIp6Addr * pAddr6));

tIp6AddrInfo    *Ip6AddrTblGetEntry PROTO ((UINT4 u4Index,
                                                   tIp6Addr * pAddr6,
                                                   UINT1 u1Prefixlen));

tIp6AddrInfo    *Ip6AddrScanAllIfInCxt PROTO ((UINT4 u4ContextId,
                                           tIp6Addr * pAddr6,
                                           UINT1 u1Prefixlen));

/* PING */

INT4 Ping6PercentageLossGet PROTO ((UINT2 u2Index));

INT1 Ping6CreateEntry PROTO ((UINT2 u2Index, UINT2 u2IfIndex,
                                tIp6Addr * pPing6Dst, UINT1 u1Admin));

INT1 Ping6AdminSet PROTO ((UINT2 u2Index, INT4 i4Value)); 

INT4
Ip6DuplicateBuffer (tCRU_BUF_CHAIN_HEADER * pBuf,
                   tCRU_BUF_CHAIN_HEADER ** ppDupBuf, UINT4 u4PktSize);

INT1 Ip6IsPrefixGreater 
PROTO ((tIp6Addr * pIp6Addr, UINT1 u1PrefixLen, INT1 i1Proto, UINT4 u4Index,
        tIp6RtEntry * pRtEntry));

INT1 Ip6IsPrefixInBetween PROTO ((tIp6Addr * pIp6Addr,tIp6RtEntry * pRtTmp,
            tIp6RtEntry * pRtEntry));
tIp6AddrInfo       *
Ip6AddrTblGetAddrInfo PROTO ((UINT4 u4Index));

/* RFC 4007 - Prototypes Start*/

VOID IP6ScopeNameTableInit PROTO ((VOID));

INT1
Ip6ZoneTestScopeZoneEntry PROTO ((UINT4 *pu4ErrorCode, UINT4 u4Index,UINT1 u1Scope,
                    UINT1 *au1ScopeZone, UINT1 u1ZoneCreationStaus));

INT4
Ip6ZoneVerifyIntOtherZoneMembersZoneConfig PROTO ((UINT4 u4Index,UINT1 U1Scope,INT4 i4Zoneid));


INT4
Ip6ZoneCreateScopeZoneInfo PROTO ((UINT4 u4Index,UINT1 u1Scope,UINT1* pu1ZoneName,
                              UINT1 u1CreationStatus,INT4 i4ZoneId));

tIp6ScopeZoneInfo *
Ip6ZoneGetRBTreeScopeZoneEntry PROTO ((UINT1* au1ZoneName,UINT4 u4ContextId));

VOID
Ip6ZoneResetOtherDefaultZoneEntry PROTO ((UINT1 u1Scope, INT4 i4ZoneId,UINT4 u4ContextId));

INT4
Ip6ZoneIsScopeSmaller PROTO ((UINT1 u1Scope1, UINT1 u1Scope2));

INT4
Ip6ZoneCheckAddrMatch PROTO ((UINT4 u4IfIndex1,UINT4 u4IfIndex2,UINT1 u1Scope));

INT4
Ip6ZoneGetFreeZoneId PROTO ((UINT4 u4IfIndex,UINT1 u1Scope));

VOID
IncMsrForIpv6IfZoneMapTable PROTO ((INT4 i4IfIndex, CHR1 cDatatype, VOID *pSetVal,
                      UINT4 *pu4ObjectId, UINT4 u4OIdLen,UINT1 IsRowStatus));

VOID
IncMsrForIpv6ScopeZoneTable PROTO ((UINT4 u4ContextId,tSNMP_OCTET_STRING_TYPE *pFsipv6ScopeZoneName,
                            INT4 i4SetVal,UINT4 *pu4ObjectId, UINT4 u4OIdLen,UINT1 IsRowStatus));

VOID
Ip6ZoneReleaseLlocalZoneId PROTO ((INT4 I4ZoneId, UINT4 u4IfIndex,UINT1 u1Scope));

INT4
Ip6ZoneGetFreeZoneIndex PROTO ((VOID));

INT4
Ip6ZoneCheckFreeIndexAvail PROTO ((VOID));

INT4
Ip6ZoneIntegrityCheckForHigherZone PROTO ((UINT4 u4Index,UINT1 u1Scope,
                                           INT4  i4ZoneId,tIp6IfZoneMapInfo* pTempIfZoneMapInfo));

INT4
Ip6ZoneIntegrityCheckForLowerZone PROTO ((UINT4 u4Index,UINT1 u1Scope,
                     INT4 i4Zoneid,tIp6IfZoneMapInfo* pIfZoneMapInfo));

UINT1
Ip6ZoneFindIfDefZoneCreatedForScope PROTO ((UINT1 u1Scope,UINT4 u4ContextId));


INT4
Ip6ZoneReplaceZoneOnStandbyIf PROTO ((UINT4 u4IfIndex,UINT1 u1Scope,UINT1 u1CreationStatus,
                                  INT4 i4ZoneId));

VOID
Ip6ZoneReleaseZoneIndex PROTO ((INT4 i4ZoneIndex));


VOID
Ip6ZoneSendNotifyOnNonGlobalZoneCreation PROTO ((UINT4 u4Mask,UINT1 u1Scope,
                                          INT4 i4ZoneIndex,UINT4 u4Index));
VOID
Ip6ZoneOnLinkNotification PROTO ((tIp6MultIfToIp6Link *pIp6IfOverLinkInfo));

VOID
Ip6ZoneCreateZoneForOnLinkIf PROTO ((tIp6MultIfToIp6Link *pIp6IfOverLinkInfo));

VOID
Ip6ZoneSendNotifyOnNonGlobalZoneDeletion PROTO ((UINT1 u4Mask,UINT1 u1Scope,
                                   INT4 i4ZoneIndex,UINT4 u4Index));

VOID
Ip6ZoneDelLlocalZoneForOnLinkIf PROTO ((tIp6MultIfToIp6Link *pIp6IfOverLinkInfo));

VOID
Ip6ZoneCreateAutoScopeZone PROTO ((UINT4 u4IfIndex,UINT1 u1Scope));

VOID 
Ip6HandleSelfIfOnLinkEvent PROTO((tCRU_BUF_CHAIN_HEADER * pBuf));

VOID
Ip6ZoneGetZoneName PROTO ((UINT1 *pu1ZoneName,UINT1 u1Scope,INT4 i4ZoneId)); 

INT4
Ip6ZoneGetZoneIdFromZoneName PROTO((UINT1 *pu1ZoneName,UINT1 *pu1Scope,INT4 *pi4ZoneId));

INT4
Ip6ZoneCheckScopeZoneIntegrity PROTO ((tIp6ScopeZoneInfo *pRBTreeScopeZoneinfo,
                                 UINT1 u1Scope,INT4 i4ZoneId, UINT4 u4IfIndex));

INT4
Ip6ZoneCreateZoneNodeinRbTree PROTO ((UINT4 u4IfIndex,UINT1 u1Scope,
            UINT1 *pu1ZoneName,UINT1 u1CreationStatus,INT4 i4ZoneId,INT4 i4ZoneIndex));

VOID
Ip6ZoneSetNonGlobalZoneCnt PROTO((UINT1 u1Scope, UINT4 u4ContextId, UINT1 u1IsNonGblZoneSet));

VOID
Ip6ZoneUpdateZoneNodeinRbTree PROTO ((UINT4 u4IfIndex,
                          tIp6ScopeZoneInfo  *pIp6ScopeZoneInfo));

INT4
Ip6ZoneCreateIfZoneNode PROTO ((UINT1 u1Scope,
                         UINT4 u4IfIndex, INT4 i4ZoneId, INT4 i4ZoneIndex,
                         UINT1 u1CreationStatus,UINT1* pu1ZoneName));

VOID 
Ip6ZoneDeleteZoneNodeFromRbTree PROTO ((UINT4 u4IfIndex,UINT1 u1Scope,INT4 i4ZoneId));

VOID
Ip6ZoneDeleteIfZoneNode PROTO ((UINT4 u4Index,UINT1 u1Scope));

UINT4
Ip6ZoneCheckIfLastInterface PROTO ((tIp6ScopeZoneInfo *pIp6ScopeZoneOldInfo,
                            UINT4 u4IfIndex));

VOID
Ip6ScopeZoneDelete PROTO ((UINT4 u4Index, UINT1 u1Scope,INT4 i4ZoneId));

INT4
Ip6ZoneTestZoneNameandId PROTO ((UINT4 *pu4ErrorCode,UINT1 *pu1ScopeZone));

INT4 Ip6ZoneInfoCmp PROTO ((tRBElem *, tRBElem *));

VOID
Ip6ZoneDeleteExistingZoneOnIf PROTO((UINT4 u4IfIndex));

INT4 Ip6ZoneInit PROTO((VOID));

VOID
Ip6ZoneSetZoneIdforAutoZones PROTO ((UINT4 u4IfIndex, INT4 i4ZoneId,UINT1 u1Scope));

INT4
Ip6fwdCheckZoneForOutGoingIf PROTO ((tIp6Hdr * pIp6, tIp6Addr *pNextHop,
                                     tIp6If * pDstIf6,tIp6If * pIf6));
/* RFC 4007 - Prototypes End*/
INT4
Nd6GetShowCmdOutputAndCalcChkSum (UINT2 *pu2SwAudChkSum);

VOID
Nd6PurgeCacheForIP (UINT4 u4IfIndex, tIp6Addr * pAddr, UINT1 u1PrefixLen);

VOID
Ip6UtilCgaAddrSyncToStandby (UINT4 u4IfIndex, tIp6Addr *pIp6Addr,
                             UINT1 u1PrefixLen, tCgaOptions *pCgaOptions);

UINT1 *
Ip6UtlCgaAddrInfoGet (UINT4 u4IfIndex, UINT1 u1Type, UINT1 *pu1Modifier);


/* RFC 4191 - Default Router Preference Prototypes */
tIp6RARouteInfoNode *
Ip6RARouteInfoCreate PROTO ((UINT4 u4IfIndex, tIp6Addr * pRARouteInfoPrefix,
                      INT4 i4PrefixLen, INT4 i4RowStatus));

INT4
IP6DelRARouteInfo PROTO ((UINT4 u4IfIndex, tIp6Addr * pRARouteInfoPrefix,
                      INT4 i4PrefixLen));

INT4
Ip6RARouteInfoCmp PROTO ((tRBElem * pRBElem1, tRBElem * pRBElem2));

tIp6RARouteInfoNode * PROTO
(Ip6GetRARoutInfo (UINT4 u4IfIndex, tIp6Addr * pRARoutePrefix,
                   INT4 i4PrefixLen));

tIp6RARouteInfoNode * PROTO
(Ip6GetFirstRARouteInfo (UINT4 u4IfIndex));

tIp6RARouteInfoNode * PROTO
(Ip6GetNextRARouteInfo (UINT4 u4IfIndex, tIp6Addr * pRARouteInfoPrefix,
                        INT4 i4PrefixLen));

INT4
Ip6UtlRBFreeAgentToRARouteTable PROTO ((tRBElem * pRBElem, UINT4 u4Arg));

INT4
Ip6IsAddrOnInterface PROTO ((UINT1 *pu1Type, UINT1 u1LlocalChk, tIp6Addr * pTargetaddr,
                      tIp6If * pIf6));
#ifdef EVPN_VXLAN_WANTED
INT4 Nd6IsCacheForAddrLearntFromEvpn PROTO ((tIp6Addr * pAddr6, 
                                             tNd6CacheEntry * pNd6cEntry));
#endif

/* RFC 4191 - Prototypes End */
#endif /* !_IP6PROTO_H */
