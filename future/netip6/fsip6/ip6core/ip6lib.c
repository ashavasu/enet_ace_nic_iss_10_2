/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * 
 * $Id: ip6lib.c,v 1.18 2016/05/19 10:58:28 siva Exp $
 *
 ********************************************************************/

/*
 *----------------------------------------------------------------------------- 
 *    FILE NAME                    :    ip6lib.c
 *
 *    PRINCIPAL AUTHOR             :    Vivek Venkatraman
 *
 *    SUBSYSTEM NAME               :    IPv6
 *
 *    MODULE NAME                  :    IP6 Library Module
 *
 *    LANGUAGE                     :    C
 *
 *    TARGET ENVIRONMENT           :    UNIX
 *
 *    DATE OF FIRST RELEASE        :    DDD-MM-1996
 *
 *    DESCRIPTION                  :    This file contains routines for
 *                                      using buffer, timer and memory
 *                                      services and is used by other modules
 *                                      of IPv6. The routines in turn make
 *                                      calls to CRU and TMO. Most of the
 *                                      routines here will need to be modified
 *                                      when porting to a target platform.
 *
 *----------------------------------------------------------------------------- 
 */

#include "ip6inc.h"

tOsixTaskId        gu4Ip6LockTaskId = 0;
/*
 * Buffer Manipulation routines
 */

/******************************************************************************
 * DESCRIPTION : This routine will attempt to allocate a buffer for the
 *               requested size and set its offset to the desired value.
 *               A CRU library routine is called for this purpose.
 *
 * INPUTS      : size of buffer to be allocated (u4Size),
 *               offset to be set (u4Offset) and
 *               module requesting the alloc (u1Module)
 *
 * OUTPUTS     : None
 *
 * RETURNS     : Pointer to buffer if allocation succeeds
 *               NULL, if allocation fails
 *
 * NOTES       :
 ******************************************************************************/

tCRU_BUF_CHAIN_HEADER *
Ip6BufAlloc (UINT4 u4ContextId, UINT4 u4Size, UINT4 u4Offset, UINT1 u1Module)
{
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;

    /* To handle warnings in case of Trace is not defined */
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u1Module);

    if ((pBuf = CRU_BUF_Allocate_MsgBufChain (u4Size, u4Offset)) != NULL)
    {
        IP6_BUF_READ_OFFSET (pBuf) = 0;
        IP6_BUF_WRITE_OFFSET (pBuf) = 0;

        IP6_TRC_ARG4 (u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                      "IP6LIB:Ip6BufAlloc:Allocated bufsize = %d offset=%d Module = 0x%X Bufptr=%p\n",
                      u4Size, u4Offset, u1Module, pBuf);
    }
    else
    {
        IP6_TRC_ARG3 (u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                      "IP6LIB:Ip6BufAlloc: Buffer alloc failed, size = %d offset = %d Module = 0x%X\n",
                      u4Size, u4Offset, u1Module);
    }

    return (pBuf);
}

/******************************************************************************
 * DESCRIPTION : This routine will attempt to release the passed buffer.
 *               A CRU library routine is called for this purpose.
 *
 * INPUTS      : buffer to be released (pBuf),
 *               forcible release or not (u1Rflag) and
 *               module requesting the alloc (u1Module)
 *
 * OUTPUTS     : None
 *
 * RETURNS     : IP6_SUCCESS, if the release succeeds
 *               IP6_FAILURE, if the release fails
 *
 * NOTES       :
 ******************************************************************************/

INT4
Ip6BufRelease (UINT4 u4ContextId, tCRU_BUF_CHAIN_HEADER * pBuf,
               UINT1 u1Rflag, UINT1 u1Module)
{
    INT4                i4Status = 0;

    /* To handle warnings in case of Trace is not defined */
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u1Module);

    if ((i4Status = CRU_BUF_Release_MsgBufChain (pBuf, u1Rflag)) == CRU_SUCCESS)
    {
        IP6_TRC_ARG3 (u4ContextId, IP6_MOD_TRC, BUFFER_TRC, IP6_NAME,
                      "IP6LIB:Ip6BufRelease: Released buffer = %p Flag = %d Module = 0x%X\n",
                      pBuf, u1Rflag, u1Module);
        pBuf = NULL;
    }
    else
    {
        IP6_TRC_ARG3 (u4ContextId, IP6_MOD_TRC, BUFFER_TRC | ALL_FAILURE_TRC,
                      IP6_NAME,
                      "IP6LIB:Ip6BufRelease:Buffer Release fail,buffer = %p Flag = %d Module = 0x%X\n",
                      pBuf, u1Rflag, u1Module);
    }

    return (i4Status);

}

/******************************************************************************
 * DESCRIPTION : This routine will return a pointer to data for reading at the 
 *               requested offset within the buffer from the first valid byte 
 *               in the buffer. If the data in the buffer is NOT contiguous 
 *               from the specified offset for the specified size OR the caller 
 *               wants a copy of data, the routine will copy the requested 
 *               number of bytes into the data pointer given and return a 
 *               pointer to this copied data. Otherwise, the routine will just 
 *               return a pointer into the buffer.
 *
 * INPUTS      : buffer pointer (pBuf), data pointer (pData),
 *               offset from where to read (u4Offset), 
 *               number of bytes to read (u4Size) and 
 *               whether the data should be compulsorily copied (u1Copy).
 *
 * OUTPUTS     : None (copy could have taken place into 'pData' but this
 *               is not known to the caller)
 *
 * RETURNS     : Void pointer to the data to be written - caller should
 *               cast it to desired type
 *
 * NOTES       :
 ******************************************************************************/

VOID               *
Ip6BufRead (tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 *pData, UINT4 u4Offset,
            UINT4 u4Size, UINT1 u1Copy)
{

    UINT1              *pStart = NULL;

    IP6_GBL_TRC_ARG5 (IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                      "IP6LIB:Ip6BufRead:Reading from buf= %p data= %p offset= %d "
                      "size= %d copy= %d\n", pBuf, pData, u4Offset, u4Size,
                      u1Copy);

    /*
     * check if a copy is requested
     */

    if (u1Copy)
    {
        if (CRU_BUF_Copy_FromBufChain (pBuf, pData, u4Offset, u4Size) == 0)
        {
            IP6_GBL_TRC_ARG5 (IP6_MOD_TRC, BUFFER_TRC, IP6_NAME,
                              "IP6LIB:Ip6BufRead:CRU_BUF_Copy_FromBufChain fail, "
                              "buf=%p data=%p offset=%d size=%d copy=%d",
                              pBuf, pData, u4Offset, u4Size, u1Copy);
            return (NULL);
        }
        IP6_BUF_READ_OFFSET (pBuf) = IP6_BUF_READ_OFFSET (pBuf) + u4Size;
        return ((VOID *) pData);
    }

    pStart = CRU_BUF_Get_DataPtr_IfLinear (pBuf, u4Offset, u4Size);
    if (pStart == NULL)
    {
        IP6_GBL_TRC_ARG5 (IP6_MOD_TRC, BUFFER_TRC, IP6_NAME,
                          "IP6LIB:Ip6BufRead:CRU_BUF_Read_FromBufChain fail, "
                          "buf=%p data=%p offset=%d size=%d copy=%d",
                          pBuf, pData, u4Offset, u4Size, u1Copy);

        if (CRU_BUF_Copy_FromBufChain (pBuf, pData, u4Offset, u4Size) == 0)
        {
            IP6_GBL_TRC_ARG5 (IP6_MOD_TRC, BUFFER_TRC, IP6_NAME,
                              "IP6LIB:Ip6BufRead:CRU_BUF_Copy_FromBufChain fail, "
                              "buf=%p data=%p offset=%d size=%d copy=%d",
                              pBuf, pData, u4Offset, u4Size, u1Copy);
            return (NULL);
        }
        IP6_BUF_READ_OFFSET (pBuf) = IP6_BUF_READ_OFFSET (pBuf) + u4Size;
        return ((VOID *) pData);
    }

    IP6_BUF_READ_OFFSET (pBuf) = IP6_BUF_READ_OFFSET (pBuf) + u4Size;
    IP6_GBL_TRC_ARG2 (IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                      "IP6LIB:Ip6BufRead: Returning pointer = %p local data = %p\n",
                      pStart, pData);

    return ((VOID *) pStart);
}

/******************************************************************************
 * DESCRIPTION : This routine returns the offset from which the calling module
 *               should commence writing its information. This is called by
 *               various modules which need to allocate a buffer and form a
 *               message. The current strategy provides for sufficient offset
 *               so that all the IPv6 headers which can occur before the
 *               calling module are accomodated.
 *
 * INPUTS      : calling module (u1Module)
 *
 * OUTPUTS     : None
 *
 * RETURNS     : offset to be allowed before this module starts writing its
 *               information
 *
 * NOTES       :
 ******************************************************************************/

UINT4
Ip6BufWoffset (UINT1 u1Module)
{

    UINT4               u4Offset;

    /* 
     * It is assumed here that only Authentication Extension Option
     * is supported in self-generated packets
     */

    switch (u1Module)
    {
        case ND6_MODULE:
            u4Offset = sizeof (tIp6Hdr);
            break;

        case PING6_MODULE:
            u4Offset = sizeof (tIcmp6InfoMsg) + sizeof (tIp6Hdr);
            break;

        default:
            IP6_GBL_TRC_ARG1 (IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                              "IP6LIB:Ip6BufWoffset: Unknown module 0x%X trying for write offset\n",
                              u1Module);
            u4Offset = 0;
            break;
    }

    IP6_GBL_TRC_ARG2 (IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                      "IP6LIB: Ip6BufWoffset:Module = 0x%X write offset = %d\n",
                      u1Module, u4Offset);

    return (u4Offset);

}

/******************************************************************************
 * DESCRIPTION : This routine will write the passed data into the buffer at
 *               the specified offset if copy is required.
 *
 * INPUTS      : buffer pointer (pBuf), data pointer (pData),
 *               offset from where to write (u4Offset),
 *               number of bytes to write (u4Size) and
 *               whether copy into buffer is really required or not (u1Copy)
 *
 * OUTPUTS     : None
 *
 * RETURNS     : IP6_SUCCESS, upon successful completion
 *               IP6_FAILURE, upon failure to copy
 *
 * NOTES       :
 ******************************************************************************/

INT4
Ip6BufWrite (tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 *pData, UINT4 u4Offset,
             UINT4 u4Size, UINT1 u1Copy)
{

    IP6_GBL_TRC_ARG5 (IP6_MOD_TRC, BUFFER_TRC, IP6_NAME,
                      "IP6LIB: Ip6BufWrite: Bufptr = %p data = %p offset = %d size = %d Copy = %d\n",
                      pBuf, pData, u4Offset, u4Size, u1Copy);

    if (!u1Copy)
    {
        return (IP6_SUCCESS);
    }

    /* 
     * we need to copy from pData into the buffer, so call the CRU library
     * routine
     */

    if (CRU_BUF_Copy_OverBufChain (pBuf, pData, u4Offset, u4Size) ==
        CRU_FAILURE)
    {
        IP6_GBL_TRC_ARG2 (IP6_MOD_TRC, BUFFER_TRC, IP6_NAME,
                          "IP6LIB:Ip6BufWrite: CRU_BUF_Copy_OverBufChain fail Write, buf=%p data=%p",
                          pBuf, pData);
        IP6_GBL_TRC_ARG2 (IP6_MOD_TRC, BUFFER_TRC, IP6_NAME,
                          "offset=%d size=%d", u4Offset, u4Size);

        return (IP6_FAILURE);
    }

    IP6_BUF_WRITE_OFFSET (pBuf) = IP6_BUF_WRITE_OFFSET (pBuf) + u4Size;
    return (IP6_SUCCESS);

}

/******************************************************************************
 * DESCRIPTION : This routine will return a pointer to data to be prepended
 *               to the start of the buffer. If headroom in the buffer is NOT 
 *               available or NOT contiguous, the prepend routine is called
 *               to prepend proper number of bytes and the pointer to the
 *               passed data is returned. Otherwise, the routine will just 
 *               return a pointer into the buffer after updating the number 
 *               of bytes and the offset. This routine is used in conjunction 
 *               with Ip6BufWrite() when prepending data to a buffer.
 *
 * INPUTS      : buffer pointer (pBuf), data pointer (pData) and
 *               number of bytes to write (u4Size)
 *
 * OUTPUTS     : Whether data has to be later copied into the buffer or
 *               not (pu1Copy)
 *
 * RETURNS     : Void pointer to the data to be written - caller should
 *               cast it to desired type
 *
 * NOTES       :
 ******************************************************************************/

VOID               *
Ip6BufPptr (tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 *pData, UINT4 u4Size,
            UINT1 *pu1Copy)
{
    UINT1              *pStart = NULL;

    IP6_GBL_TRC_ARG3 (IP6_MOD_TRC, IP6_TRACE_LIB, IP6_NAME,
                      "IP6LIB:Ip6BufPptr: Getting ptr for prepend, buf = %p data = %p size = %d\n",
                      pBuf, pData, u4Size);

    if (CRU_BUF_Prepend_BufChain (pBuf, pData, u4Size) == CRU_FAILURE)
    {
        IP6_GBL_TRC_ARG3 (IP6_MOD_TRC, BUFFER_TRC, IP6_NAME,
                          "IP6LIB:Ip6BufPptr:CRU_BUF_Prepend_BufChain fail,buf =%p data = %p size = %d\n",
                          pBuf, pData, u4Size);
        return (NULL);
    }
    IP6_BUF_READ_OFFSET (pBuf) = 0;
    IP6_BUF_WRITE_OFFSET (pBuf) = 0;
    pStart = (UINT1 *) pData;
    *pu1Copy = TRUE;

    IP6_GBL_TRC_ARG2 (IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                      "IP6LIB:Ip6BufPptr:Returning pointer = %p local data = %p\n",
                      pStart, pData);

    return ((VOID *) pStart);
}

/*************************************************************************************
 * DESCRIPTION : This routine will return a pointer to data to be prepended
 *
 * INPUTS      : dest buffer pointer (pDstChainBuf), src buffer pointer (pSrcChainDesc)
 *               dest offset (u4DstOffset) , source offset (u4SrcOffset), size of data
 *               to be copied (u4Size)
 *
 * OUTPUTS     : None.
 *
 * RETURNS     : IP6_SUCCESS/IP6_FAILURE
 *
 * NOTES       :
 **************************************************************************************/

INT4
CRUBUFCopyBufChains (tCRU_BUF_CHAIN_HEADER * pDstChainDesc,
                     tCRU_BUF_CHAIN_HEADER * pSrcChainDesc, UINT4 u4DstOffset,
                     UINT4 u4SrcOffset, UINT4 u4Size)
{
    UINT1              *pu1TmpBuf = NULL;
    INT4                i4Retval1 = CRU_FAILURE;
    INT4                i4Retval2 = CRU_FAILURE;

    if (u4Size == 0)
    {
        return (IP6_SUCCESS);
    }

    pu1TmpBuf = (UINT1 *) MemAllocMemBlk (gIp6GblInfo.i4Ip6BufPoolId);

    if (pu1TmpBuf == NULL)
    {
        return (IP6_FAILURE);
    }

    i4Retval1 =
        CRU_BUF_Copy_FromBufChain ((tCRU_BUF_CHAIN_HEADER *) pSrcChainDesc,
                                   pu1TmpBuf, u4SrcOffset, u4Size);
    if (i4Retval1 == CRU_FAILURE)
    {

        MemReleaseMemBlock (gIp6GblInfo.i4Ip6BufPoolId, (UINT1 *) pu1TmpBuf);
        return (IP6_FAILURE);
    }
    else
    {
        u4Size = i4Retval1;
    }

    i4Retval2 =
        CRU_BUF_Copy_OverBufChain ((tCRU_BUF_CHAIN_HEADER *) pDstChainDesc,
                                   pu1TmpBuf, u4DstOffset, u4Size);
    MemReleaseMemBlock (gIp6GblInfo.i4Ip6BufPoolId, (UINT1 *) pu1TmpBuf);
    if (i4Retval2 == CRU_FAILURE)
    {
        return (IP6_FAILURE);
    }
    else
    {
        return (IP6_SUCCESS);
    }

}

/*************************************************************************************
 * DESCRIPTION : This routine will delete given bytes of data  from the start of the 
 *               Buffer mentioned.  
 *
 * INPUTS      : buffer pointer (pChainDesc),
 *               size of data to be deleted (u4Size)
 *
 * OUTPUTS     : None.
 *
 * RETURNS     : IP6_SUCCESS/IP6_FAILURE
 *
 * NOTES       :
 **************************************************************************************/
tCRU_BUF_CHAIN_HEADER *
CRUBUFDeleteBufChainAtStart (tCRU_BUF_CHAIN_HEADER * pChainDesc, UINT4 u4Size)
{

    UINT4               u4ValidByteCount = 0;
    INT4                i4Retval = CRU_FAILURE;

    if ((u4ValidByteCount = CRU_BUF_Get_ChainValidByteCount (pChainDesc)) <=
        u4Size)
    {
        CRU_BUF_Release_MsgBufChain (pChainDesc, FALSE);
        return NULL;
    }

    i4Retval = CRU_BUF_Move_ValidOffset (pChainDesc, u4Size);
    if (i4Retval == CRU_FAILURE)
    {
        return (NULL);
    }
    else
    {
        return (pChainDesc);
    }
}

/*
 * Timer Management routines
 */

/******************************************************************************
 * DESCRIPTION : This routine will start a timer of specified duration
 *               by invoking the library routine.
 *
 * INPUTS      : Timer Id (u1TimerId), List in which to add the timer
 *               (p_timer_list), the timer node structure which is part
 *               of the data structure (pAppTimer) and the timer duration
 *               (u4Duration)
 *
 * OUTPUTS     : None
 *
 * RETURNS     : None
 *
 * NOTES       :
 ******************************************************************************/
VOID
Ip6TmrStart (UINT4 u4ContextId, UINT1 u1TimerId,
             tTimerListId timerListId,
             tTmrAppTimer * pAppTimer, UINT4 u4Duration)
{
    /* To handle warnings in case of Trace is not defined */
    UNUSED_PARAM (u4ContextId);
    IP6_TRC_ARG3 (u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                  "IP6LIB:Ip6TmrStart: Starting timer Id = %d Duration = %d Timer Node = %p\n",
                  u1TimerId, u4Duration, pAppTimer);

    ((tIp6Timer *) pAppTimer)->u1Id = u1TimerId;
    TmrStartTimer (timerListId, pAppTimer,
                   SYS_NUM_OF_TIME_UNITS_IN_A_SEC * u4Duration);
}

/******************************************************************************
 * DESCRIPTION : This routine will start a timer of specified duration 
 *               In MilliSecond  by invoking the library routine.
 *
 * INPUTS      : Timer Id (u1TimerId), List in which to add the timer
 *               (p_timer_list), the timer node structure which is part
 *               of the data structure (pAppTimer) and the timer duration
 *               (u4Duration in msec)  
 *
 * OUTPUTS     : None
 *
 * RETURNS     : None
 *
 * NOTES       :
 ******************************************************************************/
VOID
Ip6MSecTmrStart (UINT4 u4ContextId, UINT1 u1TimerId,
                 tTimerListId timerListId,
                 tTmrAppTimer * pAppTimer, UINT4 u4Duration)
{
    /* To handle warnings in case of Trace is not defined */
    UNUSED_PARAM (u4ContextId);
    IP6_TRC_ARG3 (u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                  "IP6LIB:Ip6TmrStart: Starting timer Id = %d Duration"
                  "in MSec = %d Timer Node = %p\n",
                  u1TimerId, u4Duration, pAppTimer);

    ((tIp6Timer *) pAppTimer)->u1Id = u1TimerId;

    /* MilliSecond to TIME_UNITS_IN_A_SEC */
    u4Duration =
        ((u4Duration * (SYS_NUM_OF_TIME_UNITS_IN_A_SEC)) / IP6_THOUSAND);

    TmrStartTimer (timerListId, pAppTimer, u4Duration);
}

/******************************************************************************
 * DESCRIPTION : This routine will restart a timer which was running by
 *               by calling the stop and start routines.
 *
 * INPUTS      : Timer Id (u1TimerId), List in which to add the timer
 *               (p_timer_list), the timer node structure which is part
 *               of the data structure (pAppTimer) and the timer duration
 *               (u4Duration)
 *
 * OUTPUTS     : None
 *
 * RETURNS     : None
 *
 * NOTES       :
 ******************************************************************************/
VOID
Nd6TmrRestart (UINT4 u4ContextId, UINT1 u1TimerId,
               tTimerListId timerListId,
               tTmrAppTimer * pAppTimer, UINT4 u4Duration)
{
    /* To handle warnings in case of Trace is not defined */
    UNUSED_PARAM (u4ContextId);
    IP6_TRC_ARG3 (u4ContextId, IP6_MOD_TRC, CONTROL_PLANE_TRC, IP6_NAME,
                  "IP6LIB: Nd6TmrRestart: Restarting timer Id = %d Duration = %d Timer Node = %p\n",
                  u1TimerId, u4Duration, pAppTimer);

    ((tIp6Timer *) pAppTimer)->u1Id = u1TimerId;

    IP6_TRC_ARG2 (u4ContextId, IP6_MOD_TRC, CONTROL_PLANE_TRC, IP6_NAME,
                  "IP6LIB: Nd6TmrRestart: Stopping timer Id = %d Timer Node = %p\n",
                  u1TimerId, pAppTimer);

    if (TmrStopTimer (timerListId, pAppTimer) == TMR_SUCCESS)
    {
        TmrStartTimer (timerListId, pAppTimer, u4Duration);
        return;
    }
    IP6_TRC_ARG3 (u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                  "IP6LIB: Nd6TmrRestart: Starting timer Id = %d Duration = %d Timer Node = %p\n",
                  u1TimerId, u4Duration, pAppTimer);
    /* No need to multiply the u4Duration with SYS_NUM_OF_TIME_UNITS_IN_A_SEC,
     * as it is done while generating the Random Timer value. */
    TmrStartTimer (timerListId, pAppTimer, u4Duration);
}

/******************************************************************************
 * DESCRIPTION : This routine will stop a timer by invoking the library routine.
 *
 * INPUTS      : Timer Id (u1TimerId), List in which the timer is present
 *               (p_timer_list) and the timer node structure (pAppTimer)
 *
 * OUTPUTS     : None
 *
 * RETURNS     : NONE
 *
 * NOTES       :
 ******************************************************************************/
VOID
Ip6TmrStop (UINT4 u4ContextId, UINT1 u1TimerId,
            tTimerListId timerListId, tTmrAppTimer * pAppTimer)
{
    /* To handle warnings in case of Trace is not defined */
    UNUSED_PARAM (u1TimerId);
    UNUSED_PARAM (u4ContextId);

    IP6_TRC_ARG2 (u4ContextId, IP6_MOD_TRC, CONTROL_PLANE_TRC, IP6_NAME,
                  "IP6LIB:Ip6TmrStop: Stopping timer Id = %d Timer Node = %p\n",
                  u1TimerId, pAppTimer);
    if (TmrStopTimer (timerListId, pAppTimer) != TMR_SUCCESS)
    {
        IP6_TRC_ARG3 (u4ContextId, IP6_MOD_TRC, CONTROL_PLANE_TRC, IP6_NAME,
                      "IP6LIB:Ip6TmrStop: Unlinking of timer Id = %d Node = %p "
                      "list = %p failed\n", u1TimerId, pAppTimer, timerListId);
    }

    ((tIp6Timer *) pAppTimer)->u1Id = 0;
}

/******************************************************************************
 * DESCRIPTION : This routine will restart a timer which was running by
 *               by calling the stop and start routines.
 *
 * INPUTS      : Timer Id (u1TimerId), List in which to add the timer
 *               (p_timer_list), the timer node structure which is part
 *               of the data structure (pAppTimer) and the timer duration
 *               (u4Duration)
 *
 * OUTPUTS     : None
 *
 * RETURNS     : None
 *
 * NOTES       :
 ******************************************************************************/
VOID
Ip6TmrRestart (UINT4 u4ContextId, UINT1 u1TimerId,
               tTimerListId timerListId,
               tTmrAppTimer * pAppTimer, UINT4 u4Duration)
{
    /* To handle warnings in case of Trace is not defined */
    UNUSED_PARAM (u4ContextId);
    IP6_TRC_ARG3 (u4ContextId, IP6_MOD_TRC, CONTROL_PLANE_TRC, IP6_NAME,
                  "IP6LIB: Ip6TmrRestart: Restarting timer Id = %d Duration = %d Timer Node = %p\n",
                  u1TimerId, u4Duration, pAppTimer);

    ((tIp6Timer *) pAppTimer)->u1Id = u1TimerId;

    IP6_TRC_ARG2 (u4ContextId, IP6_MOD_TRC, CONTROL_PLANE_TRC, IP6_NAME,
                  "IP6LIB: Ip6TmrRestart: Stopping timer Id = %d Timer Node = %p\n",
                  u1TimerId, pAppTimer);

    if (TmrStopTimer (timerListId, pAppTimer) != TMR_SUCCESS)
    {

        IP6_TRC_ARG3 (u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                      "IP6LIB: Ip6TmrRestart: Starting timer Id = %d Duration = %d Timer Node = %p\n",
                      u1TimerId, u4Duration, pAppTimer);
    }

    TmrStartTimer (timerListId, pAppTimer,
                   SYS_NUM_OF_TIME_UNITS_IN_A_SEC * u4Duration);
}

/******************************************************************************
 * DESCRIPTION : This routine will allocate N units from the 
 *               Buffer Pool Record as indexed by u2Id
 *
 * INPUTS      : Buffer Pool Record Index (i4Id)
 *
 * OUTPUTS     : None
 *
 * RETURNS     : Pointer to the buffer, if allocation succeeds
 *               NULL, if allocation fails
 *
 * NOTES       :
 ******************************************************************************/

UINT1              *
Ip6GetMem (UINT4 u4ContextId, INT4 i4Id)
{
    UINT1              *pBuf = NULL;

    /* To handle warnings in case of Trace is not defined */
    UNUSED_PARAM (u4ContextId);

    if (NULL == (pBuf = MemAllocMemBlk ((tMemPoolId) i4Id)))
    {
        IP6_TRC_ARG1 (u4ContextId, IP6_MOD_TRC, BUFFER_TRC, IP6_NAME,
                      "IP6LIB:Ip6GetMem: CRU MEM Allocation Failed, Pool Id = %d\n",
                      i4Id);
        return (NULL);
    }

    IP6_TRC_ARG2 (u4ContextId, IP6_MOD_TRC, CONTROL_PLANE_TRC, IP6_NAME,
                  "IP6LIB:Ip6GetMem: Allocated MEM %p from Pool Id = %d\n",
                  pBuf, i4Id);

    return (pBuf);

}

/******************************************************************************
 * DESCRIPTION : This routine will release N units from the 
 *               Buffer Pool Record as indexed by u2Id, starting
 *               from the address as specified in the pBuf
 *
 * INPUTS      : Buffer Pool Record Index (u2Id),
 *               Pointer to the buffer for release (pBuf)
 *
 * OUTPUTS     : None
 *
 * RETURNS     : IP6_SUCCESS, if the release succeeds
 *               IP6_FAILURE, if the release fails
 *
 * NOTES       :
 ******************************************************************************/

INT1
Ip6RelMem (UINT4 u4ContextId, UINT2 u2Id, UINT1 *pBuf)
{

    /* To handle warnings in case of Trace is not defined */
    UNUSED_PARAM (u4ContextId);

    IP6_TRC_ARG2 (u4ContextId, IP6_MOD_TRC, CONTROL_PLANE_TRC, IP6_NAME,
                  "IP6LIB: Ip6RelMem:Releasing MEM %p to Pool Id = %d\n", pBuf,
                  u2Id);
    if (MemReleaseMemBlock ((tMemPoolId) u2Id, pBuf) == MEM_FAILURE)
    {
        IP6_TRC_ARG2 (u4ContextId, IP6_MOD_TRC, CONTROL_PLANE_TRC, IP6_NAME,
                      "IP6LIB:Ip6RelMem: Release of MEM %p to Pool Id = %d "
                      "failed\n", pBuf, u2Id);
        return (IP6_FAILURE);
    }

    pBuf = NULL;
    return (IP6_SUCCESS);

}

/*************************************************************************************
 * DESCRIPTION : Inserts p_parms in the buffer
 *
 * INPUTS      : Pointer to Ip6Params (pParams) , buffer pointer (pBuf),
 *
 * OUTPUTS     : None.
 *
 * RETURNS     : None.
 *
 * NOTES       :
 **************************************************************************************/

VOID
Ip6SetParams (tIp6Params * pParams, tCRU_BUF_CHAIN_HEADER * pBuf)
{
    pBuf->ModuleData.u4Reserved3 = (FS_ULONG) pParams;
}

/*************************************************************************************
 * DESCRIPTION : Returns the Pointer p_parms in the buffer
 *
 * INPUTS      : buffer pointer (pBuf),
 *
 * OUTPUTS     : None.
 *
 * RETURNS     : Pointer to p_parms in the Buffer.
 *
 * NOTES       :
 **************************************************************************************/
FS_ULONG           *
Ip6GetParams (tCRU_BUF_CHAIN_HEADER * pBuf)
{
    return (FS_ULONG *) ((pBuf)->ModuleData.u4Reserved3);
}

/*************************************************************************************
 * DESCRIPTION : Routine for calculating the checksum
 *
 * INPUTS      : Pointer to Source Address (pSrc), Pointer to Dest Address (pDst), 
 *               Length of the Packet (u4Len), Protocol (u1Proto), Buffer pointer (pBuf),
 *
 * OUTPUTS     : None.
 *
 * RETURNS     : Checksum
 *
 * NOTES       :
 **************************************************************************************/

UINT2
Ip6Checksum (tIp6Addr * pSrc, tIp6Addr * pDst, UINT4 u4Len, UINT1 u1Proto,
             tCRU_BUF_CHAIN_HEADER * pBuf)
{
    UINT4               u4Sum = 0, u4DataLen = 0, u4Count = 0, u4HalfLen = 0;
    UINT4               u4Length = 0, u4Offset = IP6_BUF_READ_OFFSET (pBuf);
    UINT2              *pu2Pointer = NULL, *pu2Data = 0, u2Temp = 0, *pu2Temp =
        0;
    UINT1               au1Data[2];
    UINT2               u2Data = 0;
    tCRU_BUF_DATA_DESC *pCRU = NULL;

    for (u4Count = 0; u4Count < IP6_ADDR_ACCESS_IN_SHORT_INT; u4Count++)
    {
        u4Sum += OSIX_HTONS (pSrc->u2_addr[u4Count]);
    }

    for (u4Count = 0; u4Count < IP6_ADDR_ACCESS_IN_SHORT_INT; u4Count++)
    {
        u4Sum += OSIX_HTONS (pDst->u2_addr[u4Count]);
    }

    u4Sum += u4Len >> 16;
    u4Sum += u4Len & 0xffff;
    u4Sum += u1Proto;

    pCRU = pBuf->pFirstValidDataDesc;
    /* Finding in which chain descriptor the read offset present */
    while ((pCRU != 0) && ((u4Offset + 1) > pCRU->u4_ValidByteCount))
    {
        u4Offset -= pCRU->u4_ValidByteCount;
        pCRU = pCRU->pNext;
    }
    if (pCRU)
    {
        pu2Pointer = (UINT2 *) (VOID *) (pCRU->pu1_FirstValidByte + u4Offset);
        u4Length = pCRU->u4_ValidByteCount - u4Offset;
    }
    u4DataLen = u4Len;
    while ((pCRU != 0) && (u4DataLen != 0))
    {
        if (u4DataLen < u4Length)
        {
            u4Length = u4DataLen;
        }
        u4DataLen -= u4Length;
        u4HalfLen = u4Length / IP6_TWO;
        pu2Temp = pu2Pointer;
        while (u4HalfLen != 0)
        {
            u2Temp = *pu2Temp;
            u2Temp = OSIX_NTOHS (u2Temp);
            u4Sum += u2Temp;
            u4HalfLen -= 1;
            pu2Temp += 1;
        }
        pCRU = pCRU->pNext;
        if (pCRU)
        {
            /* Add the last byte of one chain descriptor to the next for
             * check sum calculation. This will occur if the first
             * descriptor length is odd number */
            if (u4Length % IP6_TWO != 0)
            {
                au1Data[0] = (UINT1) (*(pu2Pointer + (u4Length / IP6_TWO)));
                au1Data[1] = *pCRU->pu1_FirstValidByte;
                pu2Data = (UINT2 *) (VOID *) au1Data;
                *pu2Data = OSIX_NTOHS (*pu2Data);
                u4Sum += *pu2Data;
                u4Length = pCRU->u4_ValidByteCount - 1;
                pu2Pointer = (UINT2 *) (VOID *) (pCRU->pu1_FirstValidByte + 1);
                u4DataLen -= 1;
            }
            else
            {
                u4Length = pCRU->u4_ValidByteCount;
                pu2Pointer = (UINT2 *) (VOID *) pCRU->pu1_FirstValidByte;
            }
        }
        else
        {
            /* add the last byte to the check sum if total size 
             * is odd number */
            if (u4Length % IP6_TWO != 0)
            {
                u2Data = *(pu2Pointer + (u4Length / 2));
                u2Data = OSIX_NTOHS (u2Data);
                u2Data = u2Data & 0xff00;
                u4Sum += u2Data;
            }
        }

    }
    IP6_BUF_READ_OFFSET (pBuf) = IP6_BUF_READ_OFFSET (pBuf) + u4Len;
    /* Repeat while overflow bit exists */
    while (u4Sum >> 16)
        u4Sum = (u4Sum >> 16) + (u4Sum & IP6_BIT_ALL);

    return OSIX_NTOHS ((UINT2) ~(u4Sum & IP6_BIT_ALL));
}

/******************************************************************************
 * DESCRIPTION : Routine for filling the checksum
 *
 * INPUTS      : Checksum  (u2Cksum, Protocol (u1Proto), Buffer pointer (pBuf),
 *
 * OUTPUTS     : None.
 *
 * RETURNS     : None.
 *
 * NOTES       :
 ******************************************************************************/
VOID
Ip6FillCksum (UINT2 u2Cksum, UINT1 u1Proto, tCRU_BUF_CHAIN_HEADER * pBuf)
{
    INT4                i4RetVal = 0;

    switch (u1Proto)
    {
        case NH_ICMP6:
            i4RetVal = Ip6BufWrite (pBuf, (UINT1 *) &u2Cksum,
                                    IP6_OFFSET (tIcmp6PktHdr, u2Chksum),
                                    sizeof (UINT2), TRUE);
            break;

        case NH_UDP6:
            i4RetVal = Ip6BufWrite (pBuf, (UINT1 *) &u2Cksum,
                                    IP6_OFFSET (tUdp6Hdr, u2Chksum),
                                    sizeof (UINT2), TRUE);
            break;

        default:
            IP6_GBL_TRC_ARG1 (IP6_MOD_TRC, MGMT_TRC, IP6_NAME,
                              "IP6LIB:Ip6FillCksum: While filling checksum, Invalid protocol %d\n",
                              u1Proto);
            break;
    }
    UNUSED_PARAM (i4RetVal);
}

#ifdef MIP6_WANTED
/*****************************************************************************
* DESCRIPTION : Fills the bits from the prefix len from an
*               IPv6 address into another address
*
* INPUTS      : pPref       -  Pointer to address to get copied
*               pAddr       -  Pointer to address from which
*                               bits are copied
*               i4NumBits  -   Addr must be copied from this bit to last
* OUTPUTS     : None
*
* RETURNS     : None
****************************************************************************/

VOID
Ip6CopyRestofAddrBits (tIp6Addr * pPref, tIp6Addr * pAddr, INT4 i4NumBits)
{
    INT4                i4Dw = 0, i4_set_bit, i4Bit;
    UINT4               u4B1 = 0, u4Mask = 0;

    for (i4_set_bit = i4NumBits; i4_set_bit < IP6_ADDR_SIZE_IN_BITS;
         i4_set_bit++)
    {
        i4Dw = u4B1 = u4Mask = 0;
        i4Bit = i4_set_bit;

        i4Dw = i4Bit >> 0x05;

        u4B1 = pAddr->u4_addr[i4Dw];
        i4Bit = ~i4Bit;
        i4Bit &= 0x1f;
        u4Mask = OSIX_HTONL (1 << i4Bit);

        pPref->u4_addr[i4Dw] |= (u4B1 & u4Mask);
    }
}
#endif

/*****************************************************************************
* DESCRIPTION : Function for Locking IP6 Task
* INPUTS      : None 
* OUTPUTS     : None
* RETURNS     : SNMP_SUCCESS/SNMP_FAILURE
****************************************************************************/
INT4
Ip6Lock (VOID)
{
    if (OsixTakeSem (SELF, (const UINT1 *) IP6_TASK_SEM_NAME,
                     OSIX_WAIT, 0) != OSIX_SUCCESS)
    {
        IP6_GBL_TRC_ARG1 (IP6_MOD_TRC, MGMT_TRC, IP6_NAME,
                          "IP6LIB:Ip6Lock - Sem Take Failed for %s\n",
                          IP6_TASK_SEM_NAME);
        return SNMP_FAILURE;
    }
    gu4Ip6LockTaskId = OsixGetCurTaskId();
    return SNMP_SUCCESS;
}

/*****************************************************************************
* DESCRIPTION : Function for UnLocking IP6 Task
* INPUTS      : None 
* OUTPUTS     : None
* RETURNS     : SNMP_SUCCESS/SNMP_FAILURE
****************************************************************************/
INT4
Ip6UnLock (VOID)
{
    gu4Ip6LockTaskId = 0;
    if (OsixGiveSem (SELF, (const UINT1 *) IP6_TASK_SEM_NAME) != OSIX_SUCCESS)
    {
        IP6_GBL_TRC_ARG1 (IP6_MOD_TRC, MGMT_TRC, IP6_NAME,
                          "IP6LIB:Ip6Lock - Sem Give Failed for %s\n",
                          IP6_TASK_SEM_NAME);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/*****************************************************************************
* DESCRIPTION : Function returns the Interface Type for the given index
* INPUTS      : Interface Index
* OUTPUTS     : None 
* RETURNS     : Interface Type
****************************************************************************/
UINT1
Ip6GetIfType (UINT4 u4IfIndex)
{
    if (gIp6GblInfo.apIp6If[u4IfIndex] != NULL)
    {
        return (gIp6GblInfo.apIp6If[u4IfIndex]->u1IfType);
    }
    else
    {
        return SNMP_FAILURE; 
    }
}

/*****************************************************************************
* DESCRIPTION : Function for Locking IP6 Task
* INPUTS      : None
* OUTPUTS     : None
* RETURNS     : SNMP_SUCCESS/SNMP_FAILURE
****************************************************************************/
INT4
Ip6GLock (VOID)
{
    if (OsixTakeSem (SELF, (const UINT1 *) IP6_TASK_GLOBAL_SEM_NAME,
                     OSIX_WAIT, 0) != OSIX_SUCCESS)
    {
        IP6_GBL_TRC_ARG1 (IP6_MOD_TRC, MGMT_TRC, IP6_NAME,
                          "IP6LIB:Ip6Lock - Sem Take Failed for %s\n",
                          IP6_TASK_GLOBAL_SEM_NAME);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/*****************************************************************************
* DESCRIPTION : Function for UnLocking IP6 Task
* INPUTS      : None
* OUTPUTS     : None
* RETURNS     : SNMP_SUCCESS/SNMP_FAILURE
****************************************************************************/
INT4
Ip6GUnLock (VOID)
{
    if (OsixGiveSem (SELF, (const UINT1 *) IP6_TASK_GLOBAL_SEM_NAME) != OSIX_SUCCESS)
    {
        IP6_GBL_TRC_ARG1 (IP6_MOD_TRC, MGMT_TRC, IP6_NAME,
                          "IP6LIB:Ip6Lock - Sem Give Failed for %s\n",
                          IP6_TASK_GLOBAL_SEM_NAME);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/***************************** END OF FILE **********************************/
