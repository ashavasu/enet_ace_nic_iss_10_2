 /********************************************************************
  * Copyright (C) 2006 Aricent Inc . All Rights Reserved
  *
  * $Id: ip6fscli.c,v 1.4 2013/12/16 15:36:22 siva Exp $
  *
  * Description: This file contains the functions of cli related calls.
  *
  ********************************************************************/

/* SOURCE FILE HEADER :
 *
 *  ---------------------------------------------------------------------------
 * |  FILE NAME             : cliip6.c                                         |
 * |                                                                           |
 * |  PRINCIPAL AUTHOR      : Aricent Inc.                              |
 * |                                                                           |
 * |  SUBSYSTEM NAME        : CLI                                              |
 * |                                                                           |
 * |  MODULE NAME           : IP6 configuration                                |
 * |                                                                           |
 * |  LANGUAGE              : C                                                |
 * |                                                                           |
 * |  TARGET ENVIRONMENT    :                                                  |
 * |                                                                           |
 * |  DATE OF FIRST RELEASE :                                                  |
 * |                                                                           |
 * |  DESCRIPTION           : Action routines for set/get objects in           |
 * |                         fsipv6.mib                                        |
 *  ---------------------------------------------------------------------------
 *
 */

#include "ip6inc.h"
#include "cliip6.h"
#include "ip6cli.h"

/*********************************************************************
*  Function Name : cli_process_ip6_cmd() 
*  Description   : This function processes the IPv6 Related Commands ,
*                  Pre-validates the Inputs before sending to the
*                  IP6CLI Module , Fills up the IP6 Config Params
*                  Structure with the input values given by the User,
*                  fills up the Command in the Input Message, and
*                  calls Ip6CliCmdActionRoutine() for further Processing of
*                  the User Commands/Inputs.
*  Input(s)      : UserInputs/Command in va_alist. 
*  Output(s)     : None.
*  Return Values : None.
*********************************************************************/

#ifdef IP6_WANTED
VOID
cli_process_ip6_cmd (UINT4 u4Command, ...)
{
    va_list             ap;
    UINT1              *args[10];
    INT1                argno = 0;
    UINT1               au1InputMsg[sizeof (tIp6CliConfigParams) +
                                    (2 * sizeof (UINT4))];
    UINT1              *pRespMsg;
    UINT1              *pDisplayBuffer = 0;
    UINT4               u4RespStatus = 0;
    UINT1              *pu1Inst;
    tIp6CliConfigParams *pIp6CliConfigParams;
    INT4                i4IfaceIndex = 0;

    va_start (ap, u4Command);

    /* second arguement is always InterfaceName/Index */
    pu1Inst = va_arg (ap, UINT1 *);

    if (pu1Inst != NULL)
    {
        i4IfaceIndex = *(INT4 *) (pu1Inst);
    }

    CLI_SET_COMMAND (au1InputMsg, u4Command);
    CLI_SET_RESP_STATUS (au1InputMsg, u4RespStatus);
    pIp6CliConfigParams =
        (tIp6CliConfigParams *) CLI_GET_DATA_PTR (au1InputMsg);

    /* Walk through the rest of the arguements and store in args array. 
     * Store 12 arguements at the max. This is because ipv6 commands do not
     * take more than 12 inputs from the command line. Another reason to
     * store is in some cases first input may be optional but user may give 
     * second input. In that case first arg will be null and second arg only 
     * has value */

    while (1)
    {
        args[argno++] = va_arg (ap, UINT1 *);
        if (argno == 12)
            break;
    }
    va_end (ap);

    switch (u4Command)
    {
        case IP6_CLI_ROUTING:
            pIp6CliConfigParams->i1StatusFlag = (INT1) CLI_ATOI (args[0]);
            break;

        case IP6_CLI_INTERFACE:

            pIp6CliConfigParams->Ip6IfMibTable.u4Ip6IfIndex =
                (UINT4) i4IfaceIndex;

            pIp6CliConfigParams->Ip6IfMibTable.i4Ip6IfAdminStatus =
                (INT4) CLI_ATOI (args[0]);
            break;

        case IP6_ADDR:

            pIp6CliConfigParams->Ip6AddrMibTable.u4Ip6IfIndex =
                (UINT4) i4IfaceIndex;

            /* get the string in args[0], and convert it to IP6 address format,
               copy it to the variable below */
            MEMSET (&pIp6CliConfigParams->Ip6AddrMibTable.Ip6Addr, 0,
                    sizeof (tIp6Addr));
            if (INET_ATON6 (args[0],
                            &pIp6CliConfigParams->Ip6AddrMibTable.Ip6Addr) == 0)
            {
                mmi_printf ("Invalid IPv6 Address\n");
                return;
            }

            pIp6CliConfigParams->Ip6AddrMibTable.i4PrefixLen =
                *(INT4 *) (args[1]);
            if ((pIp6CliConfigParams->Ip6AddrMibTable.i4PrefixLen < 1) ||
                (pIp6CliConfigParams->Ip6AddrMibTable.i4PrefixLen > 128))
            {
                mmi_printf ("Invalid PrefixLen\n");
                return;
            }

            pIp6CliConfigParams->Ip6AddrMibTable.i4AddrType = (INT4) args[2];
            break;

        case IP6_CLI_PING:
            MEMSET (&pIp6CliConfigParams->Ip6PingMibTable.Ip6Addr, 0,
                    sizeof (tIp6Addr));
            if (INET_ATON6 (args[0],
                            &pIp6CliConfigParams->Ip6PingMibTable.Ip6Addr) == 0)
            {
                mmi_printf ("Invalid Input ...\n");
                return;
            }

            pIp6CliConfigParams->Ip6PingMibTable.i4Count = (args[1] == NULL) ?
                CLI_PING6_DEFAULT_COUNT : (*(INT4 *) (args[1]));

            pIp6CliConfigParams->Ip6PingMibTable.i4Wait = (args[2] == NULL) ?
                CLI_PING6_DEFAULT_WAIT : (*(INT4 *) (args[2]));

            pIp6CliConfigParams->Ip6PingMibTable.i4PacketSize =
                (args[3] == NULL) ?
                CLI_PING6_DEFAULT_SIZE : (*(INT4 *) (args[3]));

            pIp6CliConfigParams->Ip6PingMibTable.i4Timeout = (args[4] == NULL) ?
                CLI_PING6_DEFAULT_TIMEOUT : (*(INT4 *) (args[4]));

            pIp6CliConfigParams->Ip6PingMibTable.u4Ip6IfIndex =
                (UINT4) i4IfaceIndex;

            pIp6CliConfigParams->Ip6PingMibTable.i4AddrType = (INT4) args[5];

            break;

        case IP6_ADDR_SHOW:
            break;

        case IP6_RT_SHOW:
            if (args[0] == NULL)
            {
                /* Display only the Best Route. */
                pIp6CliConfigParams->i1StatusFlag = FALSE;
            }
            else
            {
                /* Display all the Best Route. */
                pIp6CliConfigParams->i1StatusFlag = TRUE;
            }
            break;

        case IP6_IF_SHOW:
            pIp6CliConfigParams->u4IfIndex = (UINT4) i4IfaceIndex;
            break;

        case IP6_CLI_ROUTE_ADD:
            MEMSET (&pIp6CliConfigParams->Ip6RouteMibTable.RouteDest, 0,
                    sizeof (tIp6Addr));
            if (INET_ATON6 (args[0],
                            &pIp6CliConfigParams->Ip6RouteMibTable.RouteDest) ==
                0)
            {
                mmi_printf ("Invalid IPv6 Address ...\n");
                CLI_FREE (pInputMsg);
                return;
            }

            MEMSET (&pIp6CliConfigParams->Ip6RouteMibTable.NextHop, 0,
                    sizeof (tIp6Addr));
            if (INET_ATON6 (args[1],
                            &pIp6CliConfigParams->Ip6RouteMibTable.NextHop) ==
                0)
            {
                mmi_printf ("Invalid Next-Hop ...\n");
                return;
            }

            pIp6CliConfigParams->Ip6RouteMibTable.i4PrefixLen =
                *(INT4 *) (args[2]);
            if ((pIp6CliConfigParams->Ip6RouteMibTable.i4PrefixLen < 0) ||
                (pIp6CliConfigParams->Ip6RouteMibTable.i4PrefixLen > 128))
            {
                mmi_printf ("Invalid PrefixLen ...\n");
                return;
            }

            pIp6CliConfigParams->Ip6RouteMibTable.u4RouteIndex = i4IfaceIndex;
            if (args[3] != NULL)
            {
                pIp6CliConfigParams->Ip6RouteMibTable.u4Metric =
                    *(UINT4 *) (args[3]);
            }
            else
            {
                /* For Static Route Default metric is 1 */
                pIp6CliConfigParams->Ip6RouteMibTable.u4Metric = 1;
            }
            break;

        case IP6_CLI_ROUTE_DEL:
            MEMSET (&pIp6CliConfigParams->Ip6RouteMibTable.RouteDest, 0,
                    sizeof (tIp6Addr));
            if (INET_ATON6 (args[0],
                            &pIp6CliConfigParams->Ip6RouteMibTable.RouteDest) ==
                0)
            {
                mmi_printf ("Invalid IP6 Address...\n");
                return;
            }

            pIp6CliConfigParams->Ip6RouteMibTable.i4PrefixLen =
                *(INT4 *) (args[1]);
            if ((pIp6CliConfigParams->Ip6RouteMibTable.i4PrefixLen < 0) ||
                (pIp6CliConfigParams->Ip6RouteMibTable.i4PrefixLen > 128))
            {
                mmi_printf ("Invalid PrefixLen\n");
                return;
            }

            MEMSET (&pIp6CliConfigParams->Ip6RouteMibTable.NextHop, 0,
                    sizeof (tIp6Addr));
            if (INET_ATON6 (args[2],
                            &pIp6CliConfigParams->Ip6RouteMibTable.NextHop) ==
                0)
            {
                mmi_printf ("Invalid NextHop Address...\n");
                return;
            }

            break;

        case IP6_DEFAULT_ROUTE_SHOW:
            break;

        case IP6_ND6_SHOW:
            break;

        case IP6_ENABLE_RT_ADV_STATUS:
            pIp6CliConfigParams->Ip6IfStatsMibTable.u4Ip6IfaceIndex =
                (UINT4) i4IfaceIndex;
            pIp6CliConfigParams->Ip6IfStatsMibTable.i4RouteAdvStatus =
                *(INT4 *) (args[0]);
            break;

        case IP6_ENABLE_RT_ADV_FLAG:
            pIp6CliConfigParams->Ip6IfStatsMibTable.u4Ip6IfaceIndex =
                (UINT4) i4IfaceIndex;
            pIp6CliConfigParams->Ip6IfStatsMibTable.i4RouteAdvFlags =
                *(INT4 *) (args[0]);
            break;

        case IP6_HOP_LIMIT:
            pIp6CliConfigParams->Ip6IfStatsMibTable.u4Ip6IfaceIndex =
                (UINT4) i4IfaceIndex;
            pIp6CliConfigParams->Ip6IfStatsMibTable.i4HopLimit =
                *(INT4 *) (args[0]);
            break;

        case IP6_DEF_ROUTER_LIFE_TIME:
            pIp6CliConfigParams->Ip6IfStatsMibTable.u4Ip6IfaceIndex =
                (UINT4) i4IfaceIndex;
            pIp6CliConfigParams->Ip6IfStatsMibTable.u4DefRouterTime =
                *(UINT4 *) (args[0]);
            break;

        case IP6_PREFIX_ADV_STATUS:
            pIp6CliConfigParams->Ip6IfStatsMibTable.u4Ip6IfaceIndex =
                (UINT4) i4IfaceIndex;
            pIp6CliConfigParams->Ip6IfStatsMibTable.i4PrefixAdvStatus =
                *(INT4 *) (args[0]);
            break;

        case IP6_DAD_RETRIES:
            pIp6CliConfigParams->Ip6IfStatsMibTable.u4Ip6IfaceIndex =
                (UINT4) i4IfaceIndex;
            pIp6CliConfigParams->Ip6IfStatsMibTable.i4DADRetries =
                *(INT4 *) (args[0]);
            break;

        case IP6_IF_STATS_SHOW:
            break;

        case IP6_TRAFFIC_SHOW:
            break;

        case IP6_REACHABLE_TIME:
            pIp6CliConfigParams->Ip6IfStatsMibTable.u4Ip6IfaceIndex =
                (UINT4) i4IfaceIndex;
            pIp6CliConfigParams->Ip6IfStatsMibTable.u4ReachableTime =
                *(UINT4 *) (args[0]);
            break;

        case IP6_ROUTEADV_INTERVAL:
            pIp6CliConfigParams->Ip6IfStatsMibTable.u4Ip6IfaceIndex =
                (UINT4) i4IfaceIndex;
            pIp6CliConfigParams->Ip6IfStatsMibTable.u4MaxRAInterval =
                *(UINT4 *) (args[0]);
            pIp6CliConfigParams->Ip6IfStatsMibTable.u4MinRAInterval =
                *(UINT4 *) (args[1]);
            break;

        case IP6_ADDR_DEL:

            pIp6CliConfigParams->Ip6AddrMibTable.u4Ip6IfIndex =
                (UINT4) i4IfaceIndex;

            /* get the string in args[0], and convert it to IP6 address format,
               copy it to the variable below */
            MEMSET (&pIp6CliConfigParams->Ip6AddrMibTable.Ip6Addr, 0,
                    sizeof (tIp6Addr));
            if (INET_ATON6 (args[0],
                            &pIp6CliConfigParams->Ip6AddrMibTable.Ip6Addr) == 0)
            {
                mmi_printf ("Invalid IPv6 Address");
                return;
            }

            pIp6CliConfigParams->Ip6AddrMibTable.i4PrefixLen =
                *(INT4 *) (args[1]);
            if ((pIp6CliConfigParams->Ip6AddrMibTable.i4PrefixLen < 1) ||
                (pIp6CliConfigParams->Ip6AddrMibTable.i4PrefixLen > 128))
            {
                mmi_printf ("Invalid PrefixLen\n");
                return;
            }

            pIp6CliConfigParams->Ip6AddrMibTable.i4AddrType = (INT4) args[2];

            break;

        case IP6_CLI_TRACE:
            pIp6CliConfigParams->u4TraceEnable = *(UINT4 *) (args[0]);
            break;

        case IP6_PROTO_PREFERENC:
            pIp6CliConfigParams->Ip6ProtoPref.u4Ip6Protocol = (UINT4) (args[0]);
            pIp6CliConfigParams->Ip6ProtoPref.u4Preference =
                *(UINT4 *) (args[1]);
            break;

        case IP6_RETRANS_TIME:
            pIp6CliConfigParams->Ip6IfStatsMibTable.u4Ip6IfaceIndex =
                (UINT4) i4IfaceIndex;
            pIp6CliConfigParams->Ip6IfStatsMibTable.u4RetransTime =
                *(UINT4 *) (args[0]);
            break;

        case IP6_PREFIX_LIST:
            pIp6CliConfigParams->Ip6PrefixList.u4ProfileIndex =
                *(UINT4 *) (args[0]);

            if ((args[1]) || (args[2]))
            {
                /* Valid Life Time is Set */
                if (args[1])
                {
                    pIp6CliConfigParams->Ip6PrefixList.u4ValidTime =
                        *(UINT4 *) (args[1]);
                }
                else
                {
                    /* Infinite Valid Life Time */
                    pIp6CliConfigParams->Ip6PrefixList.u4ValidTime = 0xFFFFFFFF;
                }
            }
            else
            {
                /* Take the default Valid Life Time value */
                pIp6CliConfigParams->Ip6PrefixList.u4ValidTime =
                    IP6_ADDR_PROF_DEF_VALID_LIFE;
            }

            if (args[3])
            {
                /* Valid Life Time is Variable */
                pIp6CliConfigParams->Ip6PrefixList.u1IsValidTimeFixed =
                    IP6_VARIABLE_TIME;
            }
            else                /* args[4] != NULL */
            {
                /* Fixed Valid Life Time */
                pIp6CliConfigParams->Ip6PrefixList.u1IsValidTimeFixed =
                    IP6_FIXED_TIME;
            }

            if ((args[5]) || (args[6]))
            {
                /* Preferred Life Time is Set */
                if (args[5])
                {
                    pIp6CliConfigParams->Ip6PrefixList.u4PrefTime =
                        *(UINT4 *) (args[5]);
                }
                else
                {
                    /* Infinite Preferred Life Time */
                    pIp6CliConfigParams->Ip6PrefixList.u4PrefTime = 0xFFFFFFFF;
                }
            }
            else
            {
                /* Take the default Preferred Life Time value */
                pIp6CliConfigParams->Ip6PrefixList.u4PrefTime =
                    IP6_ADDR_PROF_DEF_PREF_LIFE;
            }

            if (args[7])
            {
                /* Preferred Life Time is Variable */
                pIp6CliConfigParams->Ip6PrefixList.u1IsPrefTimeFixed =
                    IP6_VARIABLE_TIME;
            }
            else                /* args[8] != NULL */
            {
                /* Fixed Preferred Life Time */
                pIp6CliConfigParams->Ip6PrefixList.u1IsPrefTimeFixed =
                    IP6_FIXED_TIME;
            }

            if (args[9])
            {
                /* Prefix not to be advertised */
                pIp6CliConfigParams->Ip6PrefixList.u1AdvtStatus =
                    IP6_ADDR_PROF_PREF_ADV_OFF;
            }
            else
            {
                /* Prefix to be advertised */
                pIp6CliConfigParams->Ip6PrefixList.u1AdvtStatus =
                    IP6_ADDR_PROF_PREF_ADV_ON;
            }

            if (args[10])
            {
                /* Prefix not ON-LINK */
                pIp6CliConfigParams->Ip6PrefixList.u1OnLinkStatus =
                    IP6_ADDR_PROF_ONLINK_ADV_OFF;
            }
            else
            {
                /* Prefix ON-LINK */
                pIp6CliConfigParams->Ip6PrefixList.u1OnLinkStatus =
                    IP6_ADDR_PROF_ONLINK_ADV_ON;
            }

            if (args[11])
            {
                /* Prefix not For Auto-Configuration */
                pIp6CliConfigParams->Ip6PrefixList.u1AutoConfigStatus =
                    IP6_ADDR_PROF_AUTO_ADV_OFF;
            }
            else
            {
                /* Prefix For Auto-Configuration */
                pIp6CliConfigParams->Ip6PrefixList.u1AutoConfigStatus =
                    IP6_ADDR_PROF_AUTO_ADV_ON;
            }

            pIp6CliConfigParams->Ip6PrefixList.u1Status = IP6_ADDR_PROF_VALID;
            break;

        case IP6_PREFIX_LIST_DEL:
            pIp6CliConfigParams->Ip6PrefixList.u4ProfileIndex =
                *(UINT4 *) (args[0]);
            pIp6CliConfigParams->Ip6PrefixList.u1Status = IP6_ADDR_PROF_INVALID;
            break;

        case IP6_PREFIX_CONFIG:
            pIp6CliConfigParams->Ip6PrefixConfig.u4IfIndex =
                (UINT4) i4IfaceIndex;

            /* get the string in args[0], and convert it to IP6 address format,
               copy it to the variable below */

            MEMSET (&pIp6CliConfigParams->Ip6PrefixConfig.Ip6Prefix, 0,
                    sizeof (tIp6Addr));
            if (INET_ATON6 (args[0],
                            &pIp6CliConfigParams->Ip6PrefixConfig.Ip6Prefix) ==
                0)
            {
                mmi_printf ("Invalid IPv6 Address");
                return;
            }

            pIp6CliConfigParams->Ip6PrefixConfig.i4PrefixLen =
                *(INT4 *) (args[1]);
            if ((pIp6CliConfigParams->Ip6PrefixConfig.i4PrefixLen < 1) ||
                (pIp6CliConfigParams->Ip6PrefixConfig.i4PrefixLen > 128))
            {
                mmi_printf ("Invalid PrefixLen\n");
                return;
            }

            pIp6CliConfigParams->Ip6PrefixConfig.u4ProfileIndex =
                *(UINT4 *) (args[2]);

            pIp6CliConfigParams->Ip6PrefixConfig.i4Status = IP6FWD_ACTIVE;
            break;

        case IP6_PREFIX_CONFIG_DEL:
            pIp6CliConfigParams->Ip6PrefixConfig.u4IfIndex =
                (UINT4) i4IfaceIndex;

            /* get the string in args[0], and convert it to IP6 address format,
               copy it to the variable below */

            MEMSET (&pIp6CliConfigParams->Ip6PrefixConfig.Ip6Prefix, 0,
                    sizeof (tIp6Addr));
            if (INET_ATON6 (args[0],
                            &pIp6CliConfigParams->Ip6PrefixConfig.Ip6Prefix) ==
                0)
            {
                mmi_printf ("Invalid IPv6 Address");
                return;
            }

            pIp6CliConfigParams->Ip6PrefixConfig.i4PrefixLen =
                *(INT4 *) (args[1]);
            if ((pIp6CliConfigParams->Ip6PrefixConfig.i4PrefixLen < 1) ||
                (pIp6CliConfigParams->Ip6PrefixConfig.i4PrefixLen > 128))
            {
                mmi_printf ("Invalid PrefixLen\n");
                return;
            }

            pIp6CliConfigParams->Ip6PrefixConfig.i4Status = IP6FWD_DESTROY;
            break;

        case IP6_PREFIX_SHOW:
            pIp6CliConfigParams->Ip6PrefixShow.u4IfIndex = (UINT4) i4IfaceIndex;

            if (args[0])
            {
                /* get the string in args[0], and convert it to IP6 address
                 * format, copy it to the variable below */
                MEMSET (&pIp6CliConfigParams->Ip6PrefixShow.Ip6Prefix, 0,
                        sizeof (tIp6Addr));
                if (INET_ATON6 (args[0],
                                &pIp6CliConfigParams->Ip6PrefixShow.Ip6Prefix)
                    == 0)
                {
                    mmi_printf ("Invalid IPv6 Address");
                    return;
                }

                pIp6CliConfigParams->Ip6PrefixShow.i4PrefixLen =
                    *(INT4 *) (args[1]);
                if ((pIp6CliConfigParams->Ip6PrefixShow.i4PrefixLen < 1) ||
                    (pIp6CliConfigParams->Ip6PrefixShow.i4PrefixLen > 128))
                {
                    mmi_printf ("Invalid PrefixLen\n");
                    return;
                }
            }
            else
            {
                /* Display all prefix over the interface */
                MEMSET (&pIp6CliConfigParams->Ip6PrefixShow.Ip6Prefix, 0, 16);
                pIp6CliConfigParams->Ip6PrefixShow.i4PrefixLen = 0;
            }
            break;

        case IP6_ND6_CACHE_ADD:
            pIp6CliConfigParams->Ip6Nd6Cache.u4IfIndex = (UINT4) i4IfaceIndex;

            /* get the string in args[0], and convert it to IP6 address format,
               copy it to the variable below */

            MEMSET (&pIp6CliConfigParams->Ip6Nd6Cache.Ip6Addr, 0,
                    sizeof (tIp6Addr));
            if (INET_ATON6 (args[0],
                            &pIp6CliConfigParams->Ip6Nd6Cache.Ip6Addr) == 0)
            {
                mmi_printf ("Invalid IPv6 Address");
                return;
            }

            CliConvertDotStrToStr (args[1],
                                   pIp6CliConfigParams->Ip6Nd6Cache.
                                   au1PhyMacAddr);
            break;

        case IP6_ND6_CACHE_DEL:
            pIp6CliConfigParams->Ip6Nd6Cache.u4IfIndex = (UINT4) i4IfaceIndex;

            /* get the string in args[0], and convert it to IP6 address format,
               copy it to the variable below */

            MEMSET (&pIp6CliConfigParams->Ip6Nd6Cache.Ip6Addr, 0,
                    sizeof (tIp6Addr));
            if (INET_ATON6 (args[0],
                            &pIp6CliConfigParams->Ip6Nd6Cache.Ip6Addr) == 0)
            {
                mmi_printf ("Invalid IPv6 Address");
                return;
            }
            break;

        case IP6_TRACEROUTE:
            MEMSET (&pIp6CliConfigParams->Ip6Trct.Ip6Addr, 0,
                    sizeof (tIp6Addr));
            if (INET_ATON6 (args[0],
                            &pIp6CliConfigParams->Ip6Trct.Ip6Addr) == 0)
            {
                mmi_printf ("Invalid IPv6 Address");
                return;
            }
            break;

        case PING6_UNSECURED:
            pIp6CliConfigParams->i4PingAllowFlag = (INT4) args[0];
            break;

        case IP6_UNITUNL_SRC:
            pIp6CliConfigParams->Ip6UniTunl.u4TunlIfIndex =
                (UINT4) i4IfaceIndex;
            pIp6CliConfigParams->Ip6UniTunl.u4TunlSrcAddr =
                CLI_INET_ADDR (args[0]);
            break;

        case IP6_NO_UNITUNL_SRC:
            pIp6CliConfigParams->Ip6UniTunl.u4TunlIfIndex =
                (UINT4) i4IfaceIndex;
            pIp6CliConfigParams->Ip6UniTunl.u4TunlSrcAddr =
                CLI_INET_ADDR (args[0]);
            break;

        default:
            mmi_printf ("\nCommand Not Supported\n");
            return;
    }
    if (IP6_TASK_LOCK () == SNMP_FAILURE)
    {
        mmi_printf ("\nIP6 Locking Failed - Command not Executed\n");
        return;
    }

    Ip6CliCmdActionRoutine (au1InputMsg, &pRespMsg);

    /* Unlock the IP6 Task */
    IP6_TASK_UNLOCK ();

    if (pRespMsg == NULL)
    {
        return;
    }

    CLI_GET_RESP_STATUS (u4RespStatus, pRespMsg);

    if (u4RespStatus == TRUE)
    {
        pDisplayBuffer = CLI_GET_DATA_PTR (pRespMsg);
        mmi_more_printf ((INT1 *) pDisplayBuffer);
    }

    /* free the memory allocated for the response message
     * by the protocol module */
    CLI_FREE (pRespMsg);

}
#else
VOID
cli_process_ip6_cmd (UINT4 u4Command, ...)
{
    mmi_printf ("IP6  module not available\n");
}
#endif
