/*******************************************************************
 *   Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *   
 *   $Id: ip6.h,v 1.4 2015/01/14 12:30:45 siva Exp $
 *
 ******************************************************************/
#ifndef _IP6_H
#define _IP6_H

/*
 *----------------------------------------------------------------------------- 
 *    FILE NAME                    :    ip6.h
 *
 *    PRINCIPAL AUTHOR             :    Kaushik Biswas
 *
 *    SUBSYSTEM NAME               :    IPv6
 *
 *    MODULE NAME                  :    IP6 Module
 *
 *    LANGUAGE                     :    C
 *
 *    TARGET ENVIRONMENT           :    UNIX
 *
 *    DATE OF FIRST RELEASE        :    DDD-MM-1996
 *
 *    DESCRIPTION                  :    This file contains IPv6 protocol
 *                                      related typedefs and constants such
 *                                      as the IPv6 address, IPv6 Packet
 *                                      Headers and constants for Next Header
 *                                      and so on.
 *
 *----------------------------------------------------------------------------- 
 */

/*
 * The following structure defines IPv6 address as a union.
 */

#define IP6_UCAST  4

/* 
 * The routing extention header 
 */

typedef struct
{

    UINT1  u1Nexthdr;        /* Identifies the type of header
                               * immediately following the routing header.
                               */
    UINT1  u1Hdrlen;         /* Length of the routing header in 8-octet
                               *  units,not including the first 8-octets
                               */
    UINT1  u1Type;           /* Type of routing header */
    UINT1  u1SegmentsLeft;  /* Number of route segments remaining ie.
                               * number of explicitly listed intermediate
                               * nodes still to be visited before reaching
                               * the final destination
                               */
}
tRtHdr;

/* 
 * The type 0 routing header 
 */

typedef struct
{
    UINT4  u4Bitmap;  /* 24-bit bit map  numbered 0 to 23 left to
                        * right. Used to determine strict or loose
                        * source routing
                        */
}
tRt0Hdr;

#ifdef MIP6_WANTED
typedef struct
{
    UINT4    u4Reserved;
    tIp6Addr HomeAddr;
}
tRt2Hdr;

#define IP6_RT2_HDR                   2 
#define IP6_RT2_HDR_LEN               2 
#define IP6_RT2_SEGMENTS_LEFT         1 

#define IP6_HOMEADDR_OPTION_TYPE       0xc9
#define IP6_HOMEADDR_OPTION_LENGTH     16
#define IP6_HOMEADDR_OPTION_TYPE_LEN   0xc910

#endif

/*
 * Either Hop-By-Hop or Destination Options contains
 * this same header
 */
typedef struct
{
    UINT1  u1NextHdr;  /* Type of Header immediately following the
                        *  Dest Options header
                        */
    UINT1  u1HdrLen;   /* Length of the Destination option header
                        * in 8-octet units, not including the first
                        * 8-octets
                        */
    UINT2 u2Reserved;
}
tExtHdr;

/*
 *    IPv6 hop-by-hop option header,
 *    called the Jumbo Payload option
 */

typedef struct
{
    UINT1  u1OptType;  /* Option type, for Jumbogram - 0xc2 */

    UINT1  u1OptLen;
    UINT2  u2Reserved;
    UINT4  u4JmbLen;   /* Jumbo Payload length */
}
tJmbHdr;

#ifdef  TUNNEL_WANTED
typedef struct
{
    UINT1  u1NextHdr;  /* Type of Header immediately following the
                        *  Encapsulation hdr Options header
                        */
    UINT1  u1HdrLen;   /* Length of the Encapsulation limit header
                        * in 8-octet units, not including the first
                        * 8-octets
                        */
    UINT1  u1EncapOpt;  /* Option type, for Encapsulation - 0x04*/
    UINT1  u1EncapLen;
    UINT1  u1Encaplmt;
    UINT1  u1OptType;  /* Option type, for Padding*/
    UINT1  u1OptLen;
    UINT1  u1PadN;
}
tEncapHdr;

#define DEST_HDR_OPTION_ENCAP            0x04
#endif

typedef struct IP6_RT_ALERT_OPTION
{
    UINT1 u1OptType;     /* Option type - for router alert- 5*/
    UINT1 u1OptLen;      /* Option length */
    UINT2 u2OptValue;    /* Option value */
}tIp6RtAlertHdr;

#define IP6_ROUTE_ALERT_OPT 5
#define IP6_RT_ALERT_OPT_TOTLEN 4  /* Total length of the option: 4 bytes */
#define IP6_RT_ALERT_OPT_VALLEN 2  /* Value length: 2 bytes */
#define IP6_RT_ALERT_ALIGN  OSIX_HTONS(0x0100)/*  For alignment: PadN opt of length 0 */

#define ICMP6_MLD_PKT       0
#define ICMP6_MLD_QUERY     130
#define ICMP6_MLD_REPORT    131
#define ICMP6_MLD_DONE      132


#define  ROUTE_ALERT_LEN      4 
#define  JMB_HDR_LEN          8     
#define  JMB_OPT_TYPE         0xc2  
#define  JMB_OPT_LEN          4     
#define  JMB_MIN_PAYLOAD_LEN  65536 
#define  JMB_OPT_TYPE_OFFSET  43         
#define  JMB_OPT_LEN_OFFSET   44           
#define  IP6_JUMBO_ENABLE     1            
#define  IP6_JUMBO_DISABLE    2           

/*
 * Protocol related constants
 */

#define  IP6_VER                       6  
#define  IP6_CLASS_AND_FLOW_LBL_LEN    28 /* class : 8, flow label 20 bytes*/
#define  IP6_RT0_HDR                   0  

#define  NH_IPV6          41     /* IPv6 in IPv4 or IPv6 */

#define  NH_H_BY_H        0      /* Hop-by-hop extension header */
#define  NH_ROUTING_HDR   43     /* The routing extention header */
#define  NH_FRAGMENT_HDR  44     /* The Fragment header */

#define  NH_AUTH_HDR      51
#define  NH_ESP_HDR       50     /* ESP Header */

#define  NH_NO_NEXT_HDR   59     /* No Next Header */
#define  NH_DEST_HDR      60     /* Destination options header */

#define  NH_MOBILITY_HDR  62
#define  NH_ICMP6         58     /* ICMP6 header */
#define  NH_UDP6          17     /* UDP6 header */
#define  NH_TCP6          06     /* TCP6 header */

/*
 * Internal constants
 */
#define  IP6_MIN_MTU                       1280 
#define  IP6_MIN_HLIM                      1                     
#define  IP6_DEF_HLIM                      255                  
#define  MAX_PAYLOAD_SIZE                  65535                 

#define  RT0_HDR_OFFSET_FOR_NH             0                     
#define  RT0_HDR_OFFSET_FOR_HDR_LEN        1                     
#define  RT0_HDR_OFFSET_FOR_TYPE           2                     
#define  RT0_HDR_OFFSET_FOR_SEGMENTS_LEFT  3                    

#define BYTES_IN_OCTECT                  8
#define IP6_DEST_OPT_HDR_SIZE            4

#define DEST_HDR_OPTION_00               0x3f
#define DEST_HDR_OPTION_01               0x7f
#define DEST_HDR_OPTION_10               0xbf
#define DEST_HDR_OPTION_11               0xff

#define PAD1_OPTION               0
#define PADN_OPTION               1
#define HOME_ADDR_OPTION          0xc9 /*201*/

#define PAD1_OPTION_HDR_LEN             3
 

#define  IP6_VER_BITMASK                   CRU_HTONL(0xf0000000) 
#define  IP6_DEF_HDR_VAL                   CRU_HTONL(0x60000000) 
#define  IP6_PRIO_BITMASK                  CRU_HTONL(0x0ff00000) 
#define  IP6_FL_BITMASK                    CRU_HTONL(0x000fffff) 

#define  IP6_HDR_LEN                       sizeof(tIp6Hdr)     
#define  IP6_OFFSET_FOR_SEGMENT_FIELD      3                     
#define  IP6_OFFSET_FOR_NEXTHDR_FIELD      6                     
#define  IP6_OFFSET_FOR_HOPLIM_FIELD       7                     

 /*    ipv6hdr + icmp6 err msg 
  *    + (offset to destination address in side the error pkt )*/
#define  IP6_OFFSET_FOR_ERR_PKT_DESTADDR_FIELD  72  
#define  IP6_OFFSET_FOR_ERR_PKT_SRCADDR_FIELD   56  

#define  IP6_OFFSET_FOR_PAYLOAD_LEN        4                     
#define  IP6_OFFSET_FOR_FRAGOFF_FIELD      2                     

#define  IP6_FWD_NO_SRCRT                  0                     
#define  IP6_FWD_SRCRT                     1               

#define  IP6_EXH_HDR_CONTINUE              1                     
#define  IP6_HIGHER_LAYER                  2                     
#define  IP6_EXT_HDR_DISCARD               3                     
#define  CFA_ENET_IPV6                     0x86DD                

#define  IP6_TYPE_LEN_BYTES                 2
#define  IP6_TYPE_SEND_LEN_BYTES            4

#define IP6_SEND_CGA_BYTES              256
#endif /* !_IP6_H */
