
/*******************************************************************
 *     Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * 
 *     $Id: ping6.h,v 1.6 2015/06/17 04:58:56 siva Exp $
 * 
 *****************************************************************/


#ifndef _PING6_H
#define _PING6_H

#include "cfa.h"
#include "vcm.h"
#include "dns.h"

/*
 *----------------------------------------------------------------------------- 
 *    FILE NAME                    :    ping6.h                                 
 *                                                                              
 *    PRINCIPAL AUTHOR             :    Anshul Garg                             
 *                                                                              
 *    SUBSYSTEM NAME               :    IPv6                                    
 *                                                                              
 *    MODULE NAME                  :    PING6 Module                            
 *                                                                              
 *    LANGUAGE                     :    C                                       
 *                                                                              
 *    TARGET ENVIRONMENT           :    UNIX                                    
 *                                                                              
 *    DATE OF FIRST RELEASE        :                                
 *                                                                              
 *    DESCRIPTION                  :    This file contains typedef and macro
 *                                      definitions for handling ping packets
 *                                                                              
 *    CHANGE RECORD                :                                            
 *----------------------------------------------------------------------------- 
 *     VERSION            AUTHOR/                        DESCRIPTION OF CHANGE  
 *                        DATE                                                  
 *     1.0                Anshul/                        Created file           
 *                        Nov 20, 1996
 *----------------------------------------------------------------------------- 
 */

#define  MAX_PING6_WIDTH  10     /* Max. number of sent Echo Requests for
                                 * which we can await response. The Seq
                                 * number and start time of so many Echo
                                 * Requests can be maintained and so
                                 * checked against responses.
                                 */

typedef struct _PING6_STAT
{
    UINT4  u4StartTime;   /* Starting time of the packet   */
    INT2   i2Seq;         /* Sequence number of the packet */
    INT2   i2Reserved;    /* Padding                       */
}
tPing6Stat;

/*
 * Ping structure maintained for each Ping initiated from this entity
 */

typedef struct _PING6_MAIN
{
    tIp6Addr      ping6Dst;                /* Address of remote host 
                                            * to be pinged */
    tPing6Stat    pktStat[MAX_PING6_WIDTH];
    tIp6Timer     timer;                   /* Timer to send echo request
                                            * packets after each timeout
                                            * interval */
    tIp6Addr      PingSrcAddr;
    UINT4         u4ContextId;             /* The virtual router Id */  
    UINT2         u2Ping6Interval;         /* Interval after which echo
                                            * request packets are to be 
                                            * sent repeatedly */
    UINT2         u2Ping6RcvTimeout;       /* Maximum permitted time for
                                            * the echo reply packet to
                                            * come back */
    UINT2         u2Ping6MaxTries;         /* Maximum number of tries 
                                            * for which a remote host 
                                            * can be pinged */
    UINT2         u2Ping6Size;             /* Size of the data to be 
                                            * sent in ping packet */
    UINT2         u2SentCount;             /* Number of ping packets 
                                            * sent till now */
    UINT2         u2AverageTime;           /* Average response time 
                                            * for the pings */
    UINT2         u2MaxTime;               /* Maximum response time 
                                            * for the ping */
    UINT2         u2MinTime;               /* Minimum response time
                                            * for the ping */
    UINT2         u2Successes;             /* No. of ping responses 
                                            * till now got successfully */
    UINT2         u2Id;                    /* Identification number for
                                            * the ping packet */
    UINT2         u2Seq;                   /* Sequence number of the
                                            * ping packet */
    UINT2         u2IfIndex;               /* Interface index over which
                                            * packet is to be passed */
    INT2          u2StartSeq;               /* Starting sequence number 
                                             * for the pings */
    UINT1         au1ZoneId[CFA_CLI_MAX_IF_NAME_LEN];/*ZoneId added for RFC4007*/
    UINT1         PingData[6];
    UINT1         u1AddrType;
    UINT1         u1Admin;                 /* Status of the ping block */
    UINT1         u1Oper;                  /* Operational status of 
                                            * the ping block */
    INT2          i2Count;
    UINT4         u4Seconds;
    UINT4         u4NanoSeconds;
    UINT4         u4TimeTaken;
    UINT1         au1HostName[DNS_MAX_QUERY_LEN];
    UINT1         au1Pad[1];
}
tPing6;

/* Macros */

#define  GET_PING6_PTR_FROM_PING_TAB(x)  ((UINT1 *)(x)- IP6_OFFSET(tPing6,timer)) 

#define  PING6_GET_DIFF_TIME(u4PresentTime,u4StartTime)   \
              (u4PresentTime > u4StartTime) ? \
              (u4PresentTime - u4StartTime):  \
              (IP6_MAX_SYS_TIME - u4StartTime + 1) + u4PresentTime 

#define  IS_PING_TIMER_RUNNING  TMO_Get_Remaining_Time 

#endif /* _PING6_H */
