/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ping6sz.c,v 1.6 2013/12/18 12:48:31 siva Exp $
 *
 * Description: This files for RADIUS.
 *******************************************************************/

#define _PING6SZ_C
#include "ip6inc.h"
extern INT4         IssSzRegisterModuleSizingParams (CHR1 * pu1ModName,
                                                     tFsModSizingParams *
                                                     pModSizingParams);
extern INT4         IssSzRegisterModulePoolId (CHR1 * pu1ModName,
                                               tMemPoolId * pModPoolId);

INT4
Ping6SizingMemCreateMemPools ()
{
    INT4                i4RetVal;
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < PING6_MAX_SIZING_ID; i4SizingId++)
    {
        i4RetVal =
            MemCreateMemPool (FsPING6SizingParams[i4SizingId].u4StructSize,
                              FsPING6SizingParams[i4SizingId].
                              u4PreAllocatedUnits, MEM_DEFAULT_MEMORY_TYPE,
                              &(PING6MemPoolIds[i4SizingId]));
        if (i4RetVal == (INT4) MEM_FAILURE)
        {
            Ping6SizingMemDeleteMemPools ();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

INT4
Ping6SzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, FsPING6SizingParams);
    IssSzRegisterModulePoolId (pu1ModName, PING6MemPoolIds);
    return OSIX_SUCCESS;
}

VOID
Ping6SizingMemDeleteMemPools ()
{
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < PING6_MAX_SIZING_ID; i4SizingId++)
    {
        if (PING6MemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (PING6MemPoolIds[i4SizingId]);
            PING6MemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
