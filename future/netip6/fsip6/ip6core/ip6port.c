/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: ip6port.c,v 1.22 2017/12/19 13:41:54 siva Exp $
*
* Description: Functions that access the external module (link layer,
*              IPv4 layer, etc) data structure.
*              When FSIP6 is ported for different target all the
*              functions in this file should be ported.
*********************************************************************/

#include "ip6inc.h"

INT4                Ip6IfCreate (tCfaRegInfo * pCfaIfInfo);
INT4                Ip6IfDelete (tCfaRegInfo * pCfaIfInfo);
INT4                Ip6IfUpdate (tCfaRegInfo * pCfaIfInfo);
INT4                Ip6TnlIfUpdate (tCfaRegInfo * pCfaIfInfo);
INT4                Ip6IfOperStChg (tCfaRegInfo * pCfaIfInfo);
INT4                Ip6IfRcvPkt (tCfaRegInfo * pCfaIfInfo);

/* table for mapping CIDR numbers to IP subnet masks - from RFC 1878*/
UINT4               u4Ip6CidrSubnetMask[] = {
    0x00000000, 0x80000000, 0xC0000000, 0xE0000000, 0xF0000000, 0xF8000000,
    0xFC000000, 0xFE000000, 0xFF000000, 0xFF800000, 0xFFC00000, 0xFFE00000,
    0xFFF00000, 0xFFF80000, 0xFFFC0000, 0xFFFE0000, 0xFFFF0000, 0xFFFF8000,
    0xFFFFC000, 0xFFFFE000, 0xFFFFF000, 0xFFFFF800, 0xFFFFFC00, 0xFFFFFE00,
    0xFFFFFF00, 0xFFFFFF80, 0xFFFFFFC0, 0xFFFFFFE0, 0xFFFFFFF0, 0xFFFFFFF8,
    0xFFFFFFFC, 0xFFFFFFFE, 0xFFFFFFFF
};

#ifdef TUNNEL_WANTED
/******************************************************************************
 * Function           : Ip6TunnelOutput
 * Input(s)           : u4Src, u4Dest, u2Proto, i1Tos, i1Ttl, pBuf,
 *                      i2Len, i2Id, i1Df, i2Olen, u2Port
 * Output(s)          : None.
 * Returns            : IP6_SUCCESS/FAILURE
 * Action :
 * Send Routine to IP. Constructs the IP header and then
 * enqueues it to the IP.
 * Calls ip_construct_header directly to Fill up the header it -
 * may have to be avoided if the target OS does not support the
 * direct procedure calls.
 ******************************************************************************/
INT4
Ip6TunnelOutput (UINT4 u4Dest, INT1 i1Tos, UINT2 u2Proto, INT1 i1Ttl,
                 tCRU_BUF_CHAIN_HEADER * pBuf, UINT2 u2Len, INT2 i2Id,
                 INT1 i1Df, UINT1 u1Olen, UINT4 u4Port)
{
    tTnlPktTxInfo       TnlPktInfo;
    UINT4               u4CxtId = 0;

    MEMSET (&TnlPktInfo, 0, sizeof (tTnlPktTxInfo));
    TnlPktInfo.pBuf = pBuf;
    TnlPktInfo.TnlDestAddr.Ip4TnlAddr = u4Dest;
    TnlPktInfo.u4AddrType = ADDR_TYPE_IPV4;
    TnlPktInfo.u4IfIndex = u4Port;
    TnlPktInfo.u2Len = u2Len;
    TnlPktInfo.u2Proto = u2Proto;

    VcmGetContextIdFromCfaIfIndex (u4Port, &u4CxtId);
    TnlPktInfo.u4ContextId = u4CxtId;

    TnlPktInfo.IpHdr.u2Len = u2Len;    /* In case of GRE Tunnel this length
                                     * value has to be recalculated. */
    TnlPktInfo.IpHdr.u2Id = (UINT2) i2Id;
    TnlPktInfo.IpHdr.u1Df = (UINT1) i1Df;
    TnlPktInfo.IpHdr.u1Ttl = (UINT1) i1Ttl;
    TnlPktInfo.IpHdr.u1Tos = (UINT1) i1Tos;
    TnlPktInfo.IpHdr.u1Proto = (UINT1) u2Proto;    /* In case of GRE Tunnel this
                                                   value needs to be updated. */
    TnlPktInfo.IpHdr.u1Olen = u1Olen;

    if (CfaTnlMgrTxFrame (&TnlPktInfo) == CFA_FAILURE)
    {
        return IP6_FAILURE;
    }
    return IP6_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : This function gets the Physical Interface over which the
 *               tunnel interface is created.
 *
 * INPUTS      : u4LocalAddr - Source Address.
 *               u4RemoteAddr - Destination Address.
 *
 * OUTPUTS     : pu4IfIndex - Physical Interface Index.
 *
 * RETURNS     : IP6_SUCCESS/IP6_FAILURE
 *
 ******************************************************************************/
INT4
Ip6GetTunnelInterfaceInCxt (UINT4 u4ContextId, UINT4 u4LocalAddr,
                            UINT4 u4RemoteAddr, UINT4 *pu4IfIndex)
{
    tTnlIfEntry         TnlIfEntry;
    tTnlIpAddr          TnlSrcAddr;
    tTnlIpAddr          TnlDstAddr;

    TnlSrcAddr.Ip4TnlAddr = u4LocalAddr;
    TnlDstAddr.Ip4TnlAddr = u4RemoteAddr;

    if (CfaGetTnlEntryInCxt (u4ContextId, &TnlIfEntry, ADDR_TYPE_IPV4,
                             &TnlSrcAddr, &TnlDstAddr) == CFA_FAILURE)
    {
        return IP6_FAILURE;
    }

    *pu4IfIndex = TnlIfEntry.u4IfIndex;
    return IP6_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : This function gets the Physical Interface over which the
 *               tunnel interface is created.
 *
 * INPUTS      : u4TunlIndex - Tunnel Interface Index.
 *
 * OUTPUTS     : pu4IfIndex - Physical Interface Index.
 *
 * RETURNS     : IP6_SUCCESS/IP6_FAILURE
 *
 ******************************************************************************/
INT4
Ip6GetPhyIfForTunlIndex (UINT4 u4TunlIndex, UINT4 *pu4IfIndex)
{
    tTnlIfEntry         TnlIfEntry;
    tTnlIpAddr          TnlSrcAddr;
    tTnlIpAddr          TnlDstAddr;
    tIp6If             *pIf6 = NULL;

    pIf6 = Ip6ifGetEntry (u4TunlIndex);
    if (pIf6 == NULL)
    {
        return IP6_FAILURE;
    }
    PTR_FETCH4 (TnlSrcAddr.Ip4TnlAddr, &(pIf6->pTunlIf->tunlSrc.u1_addr[12]));
    PTR_FETCH4 (TnlDstAddr.Ip4TnlAddr, &(pIf6->pTunlIf->tunlDst.u1_addr[12]));
    if (CfaGetTnlEntryInCxt (pIf6->pIp6Cxt->u4ContextId, &TnlIfEntry,
                             ADDR_TYPE_IPV4, &TnlSrcAddr,
                             &TnlDstAddr) == CFA_FAILURE)
    {
        return IP6_FAILURE;
    }
    *pu4IfIndex = TnlIfEntry.u4PhyIfIndex;
    return IP6_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : This function gets the parameters associated with the given
 *               tunnel interface and updates the given Tunnel interface
 *               structure.
 *
 * INPUTS      : u4IfIndex - Index pointing to the Tunnel Interface.
 *
 * OUTPUTS     : pIp6TunlIf - Updated Tunnel Interface Structure. 
 *
 * RETURNS     : IP6_SUCCESS/IP6_FAILURE
 *
 ******************************************************************************/
INT4
Ip6GetTunnelParams (UINT4 u4IfIndex, tIp6TunlIf * pIp6TunlIf)
{
    tTnlIfEntry         TnlIfEntry;

    if (CfaGetTnlEntryFromIfIndex (u4IfIndex, &TnlIfEntry) == CFA_FAILURE)
    {
        return IP6_FAILURE;
    }
    pIp6TunlIf->u1TunlType = (UINT1) TnlIfEntry.u4EncapsMethod;
    pIp6TunlIf->u1TunlFlag = TnlIfEntry.u1DirFlag;
    pIp6TunlIf->u1TunlDir = TnlIfEntry.u1Direction;
    SET_ADDR_UNSPECIFIED (pIp6TunlIf->tunlSrc);
    SET_ADDR_UNSPECIFIED (pIp6TunlIf->tunlDst);
    PTR_ASSIGN4 (&(pIp6TunlIf->tunlSrc.u1_addr[12]),
                 TnlIfEntry.LocalAddr.Ip4TnlAddr);
    PTR_ASSIGN4 (&(pIp6TunlIf->tunlDst.u1_addr[12]),
                 TnlIfEntry.RemoteAddr.Ip4TnlAddr);
    pIp6TunlIf->u1TunlEncaplmt = (UINT1) TnlIfEntry.u4EncapLmt;
    pIp6TunlIf->u1EncapFlag = TnlIfEntry.u1EncapOption;
    pIp6TunlIf->u2HopLimit = TnlIfEntry.u2HopLimit;
    return IP6_SUCCESS;
}
#endif /* TUNNEL_WANTED */

#ifdef TUNNEL_WANTED
/*****************************************************************************
* DESCRIPTION : Checks the tunnel interface address compatibility with
*               the underlying layers v4 address
* 
* INPUTS      : Tunnel interface v6 address, tunnel interface index
* 
* OUTPUTS     : None
* 
* RETURNS     : IP6_SUCCESS or IP6_FAILURE 
*****************************************************************************/
INT4
Ip6CheckAutoTunlAddr (tIp6Addr * pAddr, INT4 i4Index)
{
    tIp6If             *pIf;
    tTnlIfEntry         TnlIfEntry;

    pIf = gIp6GblInfo.apIp6If[i4Index];

    /* Check the interface is Tunnel */
    if (pIf->u1IfType == IP6_TUNNEL_INTERFACE_TYPE)
    {

        if (CfaGetTnlEntryFromIfIndex ((UINT4) i4Index,
                                       &TnlIfEntry) == CFA_FAILURE)
        {
            return IP6_FAILURE;
        }
        TnlIfEntry.LocalAddr.Ip4TnlAddr =
            OSIX_HTONL (TnlIfEntry.LocalAddr.Ip4TnlAddr);

        if (TnlIfEntry.u4EncapsMethod == IPV6_SIX_TO_FOUR)
        {
            if (IS_ADDR_6to4 (*pAddr))
            {
                if (MEMCMP (&(TnlIfEntry.LocalAddr.Ip4TnlAddr),
                            &(pAddr->u1_addr[2]), 4) != 0)
                    return IP6_FAILURE;
            }
        }
        if (TnlIfEntry.u4EncapsMethod == IPV6_ISATAP_TUNNEL)
        {
            if (IS_ADDR_ISATAP (*pAddr))
            {
                if (MEMCMP (&(TnlIfEntry.LocalAddr.Ip4TnlAddr),
                            &(pAddr->u1_addr[12]), 4) != 0)
                {
                    return IP6_FAILURE;
                }
            }
        }
    }
    return IP6_SUCCESS;
}
#endif

/******************************************************************************
 * DESCRIPTION : The below routines are the call backs routines registered with
 *               CFA to get the notification for interface creation, deletion
 *               status changes, IPv6 Packet reception.
 *
 * INPUTS      : pCfaIfInfo - Cfa Information 
 *
 * OUTPUTS     : None. 
 *
 * RETURNS     : CFA_SUCCESS / CFA_FAILURE
 *
 * NOTES       : 
 ******************************************************************************/
INT4
Ip6IfCreate (tCfaRegInfo * pCfaInfo)
{
    UINT4               u4ContextId = 0;

    /* create the new (default) ip6LanifTable entry */
    if (Ip6Lanif (pCfaInfo->u4IfIndex,
                  pCfaInfo->CfaIntfInfo.u4IfSpeed,
                  pCfaInfo->CfaIntfInfo.u4IfHighSpeed,
                  pCfaInfo->CfaIntfInfo.u1IfType,
                  IP6_LANIF_ENTRY_VALID,
                  (UINT1) pCfaInfo->CfaIntfInfo.u1IfOperStatus,
                  pCfaInfo->CfaIntfInfo.IfIp6Addr,
                  pCfaInfo->CfaIntfInfo.u1PrefixLen) == IP6_SUCCESS)
    {
        return CFA_SUCCESS;
    }
    Ip6GetCxtId (pCfaInfo->u4IfIndex, &u4ContextId);
    IP6_TRC_ARG1 (u4ContextId, IP6_MOD_TRC,
                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC, IP6_NAME,
                  "ERROR !!!! Couldnot create the Ip6 Interface ; Index : %d\n",
                  pCfaInfo->u4IfIndex);
    return CFA_FAILURE;
}

INT4
Ip6IfDelete (tCfaRegInfo * pCfaInfo)
{
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    tIp6CfaParams      *pParams = NULL;
    UINT4               u4ContextId = 0;

    /* Lower layer indicates a interface is getting deleted.
     * Send notification message to IP6 protocol.
     */
    pBuf = Ip6BufAlloc (u4ContextId, sizeof (tIp6CfaParams), 0, IP6_MODULE);
    if (!(pBuf))
    {
        return IP6_FAILURE;
    }

    pParams = (tIp6CfaParams *) (VOID *) IP6_BUF_DATA_PTR (pBuf);
    pParams->u1MsgType = IP6_LANIF_ENTRY_INVALID;
    pParams->u4Status = IP6_LANIF_OPER_DOWN;
    pParams->u4IfIndex = pCfaInfo->u4IfIndex;
    pParams->u1IfType = pCfaInfo->CfaIntfInfo.u1IfType;
    pParams->u2CktIndex = 0;

    if (OsixSendToQ (SELF, (const UINT1 *) IP6_TASK_CONTROL_QUEUE,
                     pBuf, OSIX_MSG_NORMAL) == OSIX_FAILURE)
    {
        if (Ip6BufRelease (u4ContextId, pBuf, FALSE, IP6_MODULE) == IP6_FAILURE)
        {
            IP6_TRC_ARG4 (u4ContextId, IP6_MOD_TRC, BUFFER_TRC,
                          IP6_NAME,
                          "IP6IF: Ip6Lanif: Interface Delete: "
                          "reporting %s Buferr type %s,err%d,Buf "
                          "Ptr= %p", ERROR_FATAL_STR, ERR_BUF_RELEASE,
                          pBuf, IP6_IF_SUBMODULE);
        }

    }
    else
    {
        OsixSendEvent (SELF, (const UINT1 *) IP6_TASK_NAME, IP6_CONTROL_EVENT);
    }

    Ip6GetCxtId (pCfaInfo->u4IfIndex, &u4ContextId);
    IP6_TRC_ARG1 (u4ContextId, IP6_MOD_TRC,
                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC, IP6_NAME,
                  "ERROR !!!! Couldnot delete  the Ip6 Interface : Index -%d\n",
                  pCfaInfo->u4IfIndex);
    return CFA_FAILURE;
}

INT4
Ip6IfUpdate (tCfaRegInfo * pCfaInfo)
{
    UINT4               u4ContextId = 0;

    if (Ip6LanifUpdate (pCfaInfo->u4IfIndex,
                        pCfaInfo->CfaIntfInfo.u4IfMtu,
                        pCfaInfo->CfaIntfInfo.u4IfSpeed,
                        pCfaInfo->CfaIntfInfo.u4IfHighSpeed) == IP6_SUCCESS)
    {
        return CFA_SUCCESS;
    }
    Ip6GetCxtId (pCfaInfo->u4IfIndex, &u4ContextId);
    IP6_TRC_ARG1 (u4ContextId, IP6_MOD_TRC,
                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC, IP6_NAME,
                  "ERROR !!!! Couldnot Update the Ip6 Interface : Index -%d\n",
                  pCfaInfo->u4IfIndex);
    return CFA_FAILURE;
}

INT4
Ip6TnlIfUpdate (tCfaRegInfo * pCfaInfo)
{
#ifdef TUNNEL_WANTED
    UINT4               u4Mask = 0;

    if (pCfaInfo->CfaTnlIfInfo.u4TnlMask & TNL_DIRECTION_CHG)
        u4Mask |= IP6_TNL_CHG_MASK_DIR;

    if (pCfaInfo->CfaTnlIfInfo.u4TnlMask & TNL_DIR_FLAG_CHG)
        u4Mask |= IP6_TNL_CHG_MASK_DIR_FLAG;

    if (pCfaInfo->CfaTnlIfInfo.u4TnlMask & TNL_ENCAP_OPT_CHG)
        u4Mask |= IP6_TNL_CHG_MASK_ENCAP_OPT;

    if (pCfaInfo->CfaTnlIfInfo.u4TnlMask & TNL_ENCAP_LIMIT_CHG)
        u4Mask |= IP6_TNL_CHG_MASK_ENCAP_LMT;

    if (pCfaInfo->CfaTnlIfInfo.u4TnlMask & TNL_ENCAP_ADDR_CHG)
        u4Mask |= IP6_TNL_CHG_MASK_END_POINT;

    if (pCfaInfo->CfaTnlIfInfo.u4TnlMask & TNL_HOP_LIMIT_CHG)
        u4Mask |= IP6_TNL_CHG_MASK_HOP_LIMIT;

    if (Ip6TunlifUpdate (pCfaInfo->u4IfIndex,
                         (UINT1) pCfaInfo->CfaTnlIfInfo.u4TnlDir,
                         (UINT1) pCfaInfo->CfaTnlIfInfo.u4TnlDirFlag,
                         (UINT1) pCfaInfo->CfaTnlIfInfo.u4TnlEncapOpt,
                         (UINT1) pCfaInfo->CfaTnlIfInfo.u4TnlEncapLmt,
                         (UINT1) pCfaInfo->CfaTnlIfInfo.u4TnlHopLimit,
                         u4Mask, &(pCfaInfo->CfaTnlIfInfo.u4LocalAddr),
                         &(pCfaInfo->CfaTnlIfInfo.u4RemoteAddr),
                         (UINT4) pCfaInfo->CfaTnlIfInfo.u4TnlType) ==
        IP6_FAILURE)
    {
        return CFA_FAILURE;
    }
    return CFA_SUCCESS;
#else
    UNUSED_PARAM (pCfaInfo);
    return CFA_FAILURE;
#endif
}

INT4
Ip6IfOperStChg (tCfaRegInfo * pCfaInfo)
{
    UINT4               u4ContextId = 0;

    if (Ip6Lanif (pCfaInfo->u4IfIndex,
                  pCfaInfo->CfaIntfInfo.u4IfSpeed,
                  pCfaInfo->CfaIntfInfo.u4IfHighSpeed,
                  pCfaInfo->CfaIntfInfo.u1IfType,
                  IP6_LANIF_ENTRY_UP,
                  (UINT1) pCfaInfo->CfaIntfInfo.u1IfOperStatus,
                  pCfaInfo->CfaIntfInfo.IfIp6Addr,
                  pCfaInfo->CfaIntfInfo.u1PrefixLen) == IP6_SUCCESS)
    {
        return CFA_SUCCESS;
    }
    Ip6GetCxtId (pCfaInfo->u4IfIndex, &u4ContextId);
    IP6_TRC_ARG1 (u4ContextId, IP6_MOD_TRC,
                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC, IP6_NAME,
                  "ERROR !!!! Couldnot Change the Operstatus of the Ip6 Interface : Index -%d\n",
                  pCfaInfo->u4IfIndex);
    return CFA_FAILURE;
}

INT4
Ip6IfRcvPkt (tCfaRegInfo * pCfaInfo)
{
    UINT4               u4ContextId = 0;

    Ip6GetCxtId (pCfaInfo->u4IfIndex, &u4ContextId);
    if (Ip6TaskEnqueuePkt (pCfaInfo->CfaPktInfo.pBuf, pCfaInfo->u4IfIndex,
                           pCfaInfo->CfaPktInfo.au1DstMac) == IP6_FAILURE)
    {
        IP6_TRC_ARG1 (u4ContextId, IP6_MOD_TRC,
                      CONTROL_PLANE_TRC | ALL_FAILURE_TRC, IP6_NAME,
                      "ERROR !!!! Couldnot enque the packet to the Ip6 Interface : Index -%d\n",
                      pCfaInfo->u4IfIndex);
        return CFA_FAILURE;
    }
    return CFA_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : This routine will register IPv6 with lower layer
 * INPUTS      : None 
 *
 * OUTPUTS     : None
 *
 * RETURNS     : IP6_SUCCESS/IP6_FAILURE;
 *
 * NOTES       : 
 ******************************************************************************/
INT4
Ip6RegisterLL (VOID)
{
    tCfaRegParams       CfaRegParams;
    INT4                i4RetValue = 0;

    UNUSED_PARAM (i4RetValue);

    CfaRegParams.u2LenOrType = CFA_ENET_IPV6;
    CfaRegParams.u2RegMask = CFA_IF_CREATE | CFA_IF_DELETE | CFA_IF_UPDATE |
        CFA_IF_OPER_ST_CHG | CFA_IF_RCV_PKT | CFA_TNL_IF_UPDATE;
    CfaRegParams.pIfCreate = Ip6IfCreate;
    CfaRegParams.pIfDelete = Ip6IfDelete;
    CfaRegParams.pIfUpdate = Ip6IfUpdate;
#ifdef TUNNEL_WANTED
    CfaRegParams.pTnlIfUpdate = Ip6TnlIfUpdate;
#else
    CfaRegParams.pTnlIfUpdate = NULL;
#endif
    CfaRegParams.pIfOperStChg = Ip6IfOperStChg;
    CfaRegParams.pIfRcvPkt = Ip6IfRcvPkt;
    i4RetValue = CfaRegisterHL (&CfaRegParams);

    return IP6_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : This routine notifies the lower layer that the IP6 Task is
 *               UP and READY
 *
 * INPUTS      : None 
 *
 * OUTPUTS     : None
 *
 * RETURNS     : NONE
 *
 * NOTES       : In case of FS-Stack, this routine will inform CFA task, that
 *               the IP layer is UP if IPv4 is not enabled. If IPv4 is enabled
 *               IPv4 is expected to notify CFA.
 ******************************************************************************/
VOID
Ip6NotifyLL (VOID)
{
#ifndef IP_WANTED
#ifdef CFA_WANTED
    CfaNotifyIpUp ();
#endif
#endif
    return;
}

/******************************************************************************
 * DESCRIPTION : This routine is called to send out an IPv6 datagram after
 *               the interface-specific parameters have all been filled. The
 *               routine updates parameters of the CRU Buffer header and then
 *               enqueues the buffer to the TEST task.
 *
 * INPUTS      : The IPv6 interface pointer (pIf6), length of the IPv6
 *               datagram (u4Len) and the buffer containing the datagram 
 *               (pBuf)
 *
 * OUTPUTS     : None
 *
 * RETURNS     : None
 *
 * NOTES       : In case of FS-Stack, this routine will inform CFA task, to
 *               send the IPv6 packet out.
 ******************************************************************************/
INT4
Ip6SendToLL (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex,
             UINT1 *au1MediaAddr, UINT1 u1PktType, UINT1 u1EncapType)
{
    /* passing Protocol Type and Encaps Type directly */
    if (CfaHandlePktFromIp (pBuf, u4IfIndex, 0, au1MediaAddr,
                            CFA_ENET_IPV6, u1PktType, u1EncapType)
        == CFA_FAILURE)
    {
        return IP6_FAILURE;
    }
    return IP6_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : This routine enables IPv6 to receive packets that are addressed
 *               to the specified multicast MAC address.
 *
 * INPUTS      : Pointer to the interface (pIf6),
 *               Pointer to the  Multicast MacAddress (u1MacAddr),
 *               Operation to be performed 
 *                  if ADDR6_UP, then Add MAC to Enet
 *                  if ADDR6_DOWN, then Delete MAC from Enet
 *
 * OUTPUTS     : None
 *
 * RETURNS     : IP6_SUCCESS / IP6_FAILURE
 *
 * NOTES       : This routine uses linux calls in CFA , needs to be ported for
 *               other OS.
 ******************************************************************************/
INT1
Ip6ifUpdateEthMaddr (tIp6If * pIf6, UINT1 *u1MacAddr, UINT1 u1Oper)
{

    UINT1               u1EnetOper;

    if (u1Oper == ADDR6_UP)
    {
        u1EnetOper = CFA_ADD_ENET;
    }
    else
    {
        u1EnetOper = CFA_DEL_ENET;
    }

#ifdef CFA_WANTED
    if (CfaIfmEnetConfigMcastAddr (u1EnetOper, (UINT2) pIf6->u4Index, 0,
                                   u1MacAddr) == CFA_SUCCESS)
    {
        IP6_TRC_ARG2 (pIf6->pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                      CONTROL_PLANE_TRC, IP6_NAME,
                      "IP6IF : IP6IfUpdateEthMaddr SUCCESS for IP6 IF = %d "
                      "Oper = %d\n", pIf6->u4Index, u1EnetOper);
        return IP6_SUCCESS;
    }
#else
    UNUSED_PARAM (u1MacAddr);
#endif
    IP6_TRC_ARG2 (pIf6->pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                  CONTROL_PLANE_TRC, IP6_NAME,
                  "IP6IF : IP6IfUpdateEthMaddr FAILURE for IP6 IF = %d "
                  "Oper = %d\n", pIf6->u4Index, u1EnetOper);
    return IP6_FAILURE;
}

/******************************************************************************
 * DESCRIPTION : This routine retrives the MAC Address for given Interface
 *
 * INPUTS      : u4IfIndex - Interface whose MAC Address is required. 
 *
 * OUTPUTS     : au1HwAddr - MAC Address of the interface
 *
 * RETURNS     : IP6_SUCCESS / IP6_FAILURE
 *
 * NOTES       : This routine uses linux calls in CFA , needs to be ported for
 *               other OS.
 ******************************************************************************/
INT4
Ip6GetHwAddr (UINT4 u4IfIndex, UINT1 *au1HwAddr)
{
    tCfaIfInfo          IfInfo;
    if (CfaGetIfInfo ((UINT2) u4IfIndex, &IfInfo) == CFA_FAILURE)
    {
        return IP6_FAILURE;
    }
    MEMCPY (au1HwAddr, IfInfo.au1MacAddr, CFA_ENET_ADDR_LEN);

    return IP6_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : This routine checks whether the DOT1Q VLAN ID is configured for
 *               an L3 Sub-interface or not.
 *
 * INPUTS      : u4IfIndex - Interface whose DOT1Q configuration status needs to
 *               be verified 
 *
 * RETURNS     : IP6_TRUE/IP6_FALSE
 *
 ******************************************************************************/
INT4
Ip6GetDot1qVlanEncapStatus (UINT4 u4IfIndex)
{
    tCfaIfInfo          IfInfo;
    if ((CfaGetIfInfo ((UINT2) u4IfIndex, &IfInfo) == CFA_FAILURE)
        || (IfInfo.u1IsEncapSet == CFA_FALSE))
    {
        return IP6_FALSE;
    }
    return IP6_TRUE;
}

/******************************************************************************
 * DESCRIPTION : This routine retrives the name for given Interface
 *
 * INPUTS      : u4IfIndex - Interface whose name is required. 
 *
 * OUTPUTS     : au1IfName - Interface Name
 *
 * RETURNS     : IP6_SUCCESS / IP6_FAILURE
 *
 * NOTES       : This routine uses linux calls in CFA , needs to be ported for
 *               other OS.
 ******************************************************************************/
INT4
Ip6GetIfName (UINT4 u4IfIndex, UINT1 *au1IfName)
{
    if (CfaGetInterfaceNameFromIndex (u4IfIndex, au1IfName) == OSIX_FAILURE)
    {
        return IP6_FAILURE;
    }

    return IP6_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : This routine retrives the Index for given Interface name
 *
 * INPUTS      : u4L2ContextId - Layer 2 context Id for the Interface
 *             : au1IfName - Interface Name whose index is required.
 *
 * OUTPUTS     : pu4IfIndex - Interface Index
 *
 * RETURNS     : IP6_SUCCESS / IP6_FAILURE
 *
 * NOTES       : This routine uses linux calls in CFA , needs to be ported for
 *               other OS.
 ******************************************************************************/
INT4
Ip6GetIfIndexFromNameInCxt (UINT4 u4L2ContextId, UINT1 *au1IfName,
                            UINT4 *pu4IfIndex)
{
    if (CfaGetInterfaceIndexFromNameInCxt (u4L2ContextId,
                                           au1IfName,
                                           pu4IfIndex) == OSIX_FAILURE)
    {
        return IP6_FAILURE;
    }

    return IP6_SUCCESS;
}

/************************************************************************/
/*      ALL Functions below are porting functions to IPv4 module.       */
/************************************************************************/

#ifdef TUNNEL_WANTED

/******************************************************************************
 * DESCRIPTION : This function is called when ever packets recved over a tunnel
 *               interface or before passing the ipv6 packets over tunnel
 *               interface. This function validates destination V4 against
 *               broadcast, multicast, and loopback address. 
 *              
 *
 * INPUTS      : u4Ipv4Addr - Ipv4 Address of the Tunnel's Remote Endpoint 
 *               u4TunlIndex - Tunnel Interface Index 
 *
 * OUTPUTS     : None
 *
 * RETURNS     : IP6_SUCCESS/IP6_FAILURE
 *               
 *
 * NOTES       : 
 ******************************************************************************/
INT4
Ip6ValidateIpv4Addr (UINT4 u4DstAddr, UINT4 u4TunlIndex)
{
    tCfaIfInfo          IfInfo;
    UINT4               u4BcastAddr;
    UINT4               u4IfIndex = 0;

    /*  Check for loopback address  and unspecified address */
    if (((u4DstAddr & LOOPBACK_ADDRESS) == LOOPBACK_ADDRESS) ||
        (u4DstAddr == 0))
    {
        return IP6_FAILURE;
    }

    /* Check for multicast address */
    if (IP_IS_ADDR_CLASS_D (u4DstAddr) == TRUE)
    {
        return IP6_FAILURE;
    }

    /* Check for BroadCast Address - Get the Subnet Mask for the interface
     * via this destination is reachable and check for BroadCast Address.
     */
    if (Ip6GetPhyIfForTunlIndex (u4TunlIndex, &u4IfIndex) == IP6_FAILURE)
    {
        return IP6_FAILURE;
    }

    MEMSET (&IfInfo, 0, sizeof (tCfaIfInfo));
    if (CfaGetIfInfo ((UINT2) u4IfIndex, &IfInfo) == CFA_FAILURE)
    {
        return IP6_FAILURE;
    }
    /*u4BcastAddr = (0xffffffff | u4DstAddr); */
    u4BcastAddr = 0xffffffff;
    if (u4BcastAddr == u4DstAddr)
    {
        return IP6_FAILURE;
    }
    return IP6_SUCCESS;
}
#endif /* TUNNEL_WANTED */

/*****************************************************************************
 *                                                                           *
 * Function           : Ip6UpdateIfaceInDiscards                             *
 *                                                                           *
 * Description        : Increment the error count for input interface        *
 *                      error in the specified interface.                    *
 *                                                                           *
 * Input(s)           : u4IfIndex                                            *
 *                                                                           *
 * Output(s)          : NONE.                                                *
 *                                                                           *
 * Returns            : NONE                                                 *
 *                                                                           *
 *****************************************************************************/
VOID
Ip6UpdateIfaceInDiscards (UINT4 u4IfIndex)
{
    tIp6If             *pIf6 = NULL;

    IP6_TASK_LOCK ();
    pIf6 = Ip6ifGetEntry (u4IfIndex);
    if (pIf6 != NULL)
    {
        IP6IF_INC_IN_DISCARDS (pIf6);
    }
    IP6_TASK_UNLOCK ();
}

/*****************************************************************************
 *                                                                           *
 * Function           : Ip6UpdateIfaceOutDiscards                            *
 *                                                                           *
 * Description        : Increment the error count for outgoing interface     *
 *                      error in the specified interface.                    *
 *                                                                           *
 * Input(s)           : u4IfIndex                                            *
 *                                                                           *
 * Output(s)          : NONE.                                                *
 *                                                                           *
 * Returns            : NONE                                                 *
 *                                                                           *
 *****************************************************************************/
VOID
Ip6UpdateIfaceOutDiscards (UINT4 u4IfIndex)
{
    tIp6If             *pIf6 = NULL;

    IP6_TASK_LOCK ();
    pIf6 = Ip6ifGetEntry (u4IfIndex);
    if (pIf6 != NULL)
    {
        IP6IF_INC_OUT_DISCARDS (pIf6);
    }
    IP6_TASK_UNLOCK ();
}
