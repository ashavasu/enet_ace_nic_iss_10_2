/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: icmp6.c,v 1.28 2015/07/03 09:45:34 siva Exp $
 *
 * Declaration of private functions used in this file */
/*******************************************************************/
/*
 *----------------------------------------------------------------------------- 
 *    FILE NAME                    :    icmp6.c                                 
 *                                                                              
 *    PRINCIPAL AUTHOR             :    Anshul Garg                             
 *                                                                              
 *    SUBSYSTEM NAME               :    IPv6                                    
 *                                                                              
 *    MODULE NAME                  :    ICMP6 Module                            
 *                                                                              
 *    LANGUAGE                     :    C                                       
 *                                                                              
 *    TARGET ENVIRONMENT           :    UNIX                                    
 *                                                                              
 *    DATE OF FIRST RELEASE        :    Dec-02-1996                             
 *                                                                              
 *    DESCRIPTION                  :    This file contains C routines for       
 *                                      handling various Control messages       
 *                                                                              
 *----------------------------------------------------------------------------- 
 */
#include "ip6inc.h"
/******************************************************************************
 * DESCRIPTION : This routine process all received ICMP6 messages
 * INPUTS      : interface structure pointer (tIp6If *pIf6)               &
 *               main ip6 header             (tIp6Hdr *pIp6)              &
 *               packet data                 (tCRU_BUF_CHAIN_HEADER *pBuf) &
 *               packet data length          (UINT2 u2Len)                  
 * OUTPUTS     : None
 * RETURNS     : None
 * NOTES       :
 ******************************************************************************/
VOID
Icmp6Rcv (tIp6If * pIf6, tIp6Hdr * pIp6, UINT2 u2Len, UINT1 u1Flag,
          tCRU_BUF_CHAIN_HEADER * pBuf)
{
    tIp6Cxt            *pIp6Cxt = NULL;
    tIcmp6PktHdr        icmp6Hdr, *pIcmp6 = NULL;
    UINT4               u4Offset = 0;
    UINT2               u2CksumRecd = 0;
    UINT2               u2Cksum = 0;
    UINT2              *pu2CheckSum = NULL;
    INT4                i4RetVal = 0;

    UNUSED_PARAM (pu2CheckSum);

    pIp6Cxt = pIf6->pIp6Cxt;
    ICMP6_INC_IN_MSGS (pIp6Cxt->Icmp6Stats);
    ICMP6_INTF_INC_IN_MSGS (pIf6);

    if (u2Len < sizeof (tIcmp6PktHdr))
    {
        IP6_TRC_ARG2 (pIp6Cxt->u4ContextId, ICMP6_MOD_TRC,
                      DATA_PATH_TRC, ICMP6_NAME,
                      "ICMP6: Icmp6Rcv: Received Truncated packet, Len = %d "
                      "IfIndex = %d\n", u2Len, pIf6->u4Index);
        Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE, ICMP6_MODULE);
        return;
    }

    /* Preserving the buffer read offset */
    u4Offset = IP6_BUF_READ_OFFSET (pBuf);

    /* Preserving the received check sum for verification */
    pu2CheckSum = (UINT2 *) Ip6BufRead (pBuf, (UINT1 *) &u2CksumRecd,
                                        (u4Offset +
                                         IP6_OFFSET (tIcmp6PktHdr, u2Chksum)),
                                        sizeof (UINT2), TRUE);

    /* Filling the checksum field with zero for recomputing check sum */
    i4RetVal = Ip6BufWrite (pBuf, (UINT1 *) &u2Cksum,
                            IP6_BUF_READ_OFFSET (pBuf), sizeof (UINT2), TRUE);

    /* Buffer read offset should be at the start of the icmp6 header */
    IP6_BUF_READ_OFFSET (pBuf) = u4Offset;

    u2Cksum = Ip6Checksum (&(pIp6->srcAddr), &(pIp6->dstAddr),
                           (UINT4) u2Len, NH_ICMP6, pBuf);

    /* Now compare the check sum calculated with received check sum */
    if (u2Cksum != u2CksumRecd)
    {
        IP6_TRC_ARG1 (pIp6Cxt->u4ContextId, ICMP6_MOD_TRC,
                      BUFFER_TRC, ICMP6_NAME,
                      "ICMP6: Icmp6Rcv: Check sum Error. Checksum = %d \n",
                      u2Cksum);
        ICMP6_INC_IN_ERRS (pIp6Cxt->Icmp6Stats);
        ICMP6_INTF_INC_IN_ERRS (pIf6);
        Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE, ICMP6_MODULE);
        return;
    }

    /* Buffer read offset should be at the start of the icmp6 header */
    IP6_BUF_READ_OFFSET (pBuf) = u4Offset;

    pIcmp6 = (tIcmp6PktHdr *) Ip6BufRead (pBuf, (UINT1 *) &icmp6Hdr,
                                          IP6_BUF_READ_OFFSET (pBuf),
                                          sizeof (tIcmp6PktHdr), 0);

    if (pIcmp6 == NULL)
    {
        IP6_TRC_ARG3 (pIp6Cxt->u4ContextId, ICMP6_MOD_TRC,
                      BUFFER_TRC, ICMP6_NAME,
                      "ICMP6: Icmp6Rcv: buffer read failed: %s buf err: "
                      "Type = %d Bufptr = %p",
                      ERROR_FATAL_STR, ERR_BUF_READ, pBuf);
        Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE, ICMP6_MODULE);
        return;
    }

    IP6_TRC_ARG6 (pIp6Cxt->u4ContextId, ICMP6_MOD_TRC,
                  DATA_PATH_TRC, ICMP6_NAME,
                  "ICMP6: Icmp6Rcv: RxIF = %d:Type = 0x%X:Code = 0x%X:Len = %d:"
                  "Buf = %p:Src = %s",
                  pIf6->u4Index, pIcmp6->u1Type, pIcmp6->u1Code, u2Len,
                  pBuf, Ip6PrintAddr (&pIp6->srcAddr));

    IP6_TRC_ARG1 (pIp6Cxt->u4ContextId, ICMP6_MOD_TRC,
                  DATA_PATH_TRC, ICMP6_NAME, " Dst = %s\n",
                  Ip6PrintAddr (&pIp6->dstAddr));

    if (pIcmp6->u1Type < ICMP6_INFO_MSG_TYPE_MIN_VAL)
    {
        if ((u1Flag & ADDR6_TENTATIVE) != ADDR6_TENTATIVE)
        {
            Icmp6RcvErrMsg (pIcmp6->u1Type, pIcmp6->u1Code, pIf6, pBuf);
        }
        else
        {
            IP6_TRC_ARG1 (pIp6Cxt->u4ContextId, ICMP6_MOD_TRC,
                          DATA_PATH_TRC, ICMP6_NAME,
                          "ICMP6: Icmp6Rcv: Packet received is addressed to "
                          "tentative address %s\n",
                          Ip6ErrPrintAddr (ERROR_MINOR, &pIp6->dstAddr));

            ICMP6_INC_IN_ERRS (pIp6Cxt->Icmp6Stats);
            ICMP6_INTF_INC_IN_ERRS (pIf6);
            Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE, ICMP6_MODULE);
        }
    }
    else
    {
        Icmp6RcvInfoMsg (pIf6, pIp6, pIcmp6->u1Type, pIcmp6->u1Code,
                         u2Len, u1Flag, pBuf);
    }
    UNUSED_PARAM (i4RetVal);
    UNUSED_PARAM (pu2CheckSum);
}

/******************************************************************************
 * DESCRIPTION : Process received error messages
 * INPUTS      : Type of message      (UINT1 u1Type)                 &
 *               Code of message      (UINT1 u1Code)                 &
 *               Error packet         (tCRU_BUF_CHAIN_HEADER *pBuf) 
 * OUTPUTS     : None
 * RETURNS     : None
 * NOTES       : In the present implementation only statistics is maintained, 
 *               packet is not being passed to upper-layer.
 ******************************************************************************/
VOID
Icmp6RcvErrMsg (UINT1 u1Type, UINT1 u1Code, tIp6If * pIf6,
                tCRU_BUF_CHAIN_HEADER * pBuf)
{
    tIp6Cxt            *pIp6Cxt = NULL;
    tIp6Hdr             Ip6Hdr;
    tIcmp6ErrMsg        Icmp6Hdr;
    tIp6Hdr            *pDroppedIp6Hdr = NULL;
    tIcmp6ErrMsg       *pIcmp6Hdr = NULL;
    tPmtuEntry         *pPmtuEntry = NULL;
    UINT4               u4Pmtu = 0;
    tIp6Hdr            *pIp6 = NULL, ip6Hdr;
    UINT4               u4ReadOffset = IP6_BUF_READ_OFFSET (pBuf);

    pIp6Cxt = pIf6->pIp6Cxt;

    switch (u1Type)
    {
        case ICMP6_DEST_UNREACHABLE:
            ICMP6_INC_IN_DST_UNREACH (pIp6Cxt->Icmp6Stats);
            ICMP6_INTF_INC_IN_DST_UNREACH (pIf6);
#ifdef MIP6_WANTED
            Mip6PrcsDestUnreachMsg (pBuf);
#endif
            IP6_BUF_READ_OFFSET (pBuf) = u4ReadOffset;
            break;

        case ICMP6_PKT_TOO_BIG:
            ICMP6_INC_IN_TOOBIG (pIp6Cxt->Icmp6Stats);
            ICMP6_INTF_INC_IN_TOOBIG (pIf6);

            IP6_BUF_READ_OFFSET (pBuf) = IP6_BUF_READ_OFFSET (pBuf) -
                sizeof (tIcmp6PktHdr);
            pIcmp6Hdr = (tIcmp6ErrMsg *) Ip6BufRead (pBuf,
                                                     (UINT1 *) &Icmp6Hdr,
                                                     sizeof (tIp6Hdr),
                                                     (UINT4) (sizeof
                                                              (tIcmp6ErrMsg)),
                                                     0);
            pDroppedIp6Hdr =
                (tIp6Hdr *) Ip6BufRead (pBuf, (UINT1 *) &Ip6Hdr,
                                        sizeof (tIp6Hdr) +
                                        sizeof (tIcmp6ErrMsg),
                                        (UINT4) (sizeof (tIp6Hdr)), 0);

            if (pIcmp6Hdr == NULL)
            {
                break;
            }
            if (pDroppedIp6Hdr == NULL)
            {
                break;
            }
            u4Pmtu = NTOHL (pIcmp6Hdr->u4ErrInfo);

            if (u4Pmtu > (Ip6ifGetMtu (pIf6)))
            {
                IP6_TRC_ARG1 (pIp6Cxt->u4ContextId, ICMP6_MOD_TRC,
                              DATA_PATH_TRC, ICMP6_NAME,
                              "ICMP6: Icmp6RcvErrMsg: Received invalid "
                              "MTU = 0x%X\n, discarding pkt", u4Pmtu);
                break;
            }
            if (u4Pmtu < IP6_MIN_MTU)
            {
                pIf6->u1TooBigMsgReceived = OSIX_TRUE;
                u4Pmtu = IP6_MIN_MTU;
            }

            if (pIp6Cxt->u1PmtuEnable != IP6_PMTU_ENABLE)
            {
                /* when dynamically PMTU change indication comes also,
                 * Add Pmtu entry */
                pIp6Cxt->u1PmtuEnable = IP6_PMTU_ENABLE;

            }
            ip6PmtuLookupEntryInCxt (pIp6Cxt->u4ContextId,
                                     &(pDroppedIp6Hdr->dstAddr), &pPmtuEntry);
            if (pPmtuEntry != NULL)
            {
                if (pPmtuEntry->u4Pmtu > u4Pmtu)
                {
                    pPmtuEntry->u4Pmtu = u4Pmtu;
                }
            }
            else
            {
                /* Create Pmtu entry with new PMTU value and inform to pkt 
                 * layer 
                 */

                /* check if, memory allocated is not Null */
                if ((pPmtuEntry =
                     (tPmtuEntry *) (VOID *) Ip6GetMem (pIp6Cxt->u4ContextId,
                                                        (UINT2) gIp6GblInfo.
                                                        MemPoolId)) == NULL)
                {
                    IP6_TRC_ARG1 (pIp6Cxt->u4ContextId, ICMP6_MOD_TRC,
                                  CONTROL_PLANE_TRC, IP6_NAME,
                                  "ICMP6: Icmp6RcvErrMsg: Unable to allocte "
                                  "mem pool for Record Id %d\n",
                                  gIp6GblInfo.MemPoolId);

                    break;
                }
                Ip6AddrCopy ((tIp6Addr *) & pPmtuEntry->Ip6Addr,
                             (tIp6Addr *) & pDroppedIp6Hdr->dstAddr);

                pPmtuEntry->u4Pmtu = u4Pmtu;
                pPmtuEntry->pIp6Cxt = pIp6Cxt;
                OsixGetSysTime (&(pPmtuEntry->PmtuTimeStamp));
                if (RBTreeAdd ((tRBTree) gIp6GblInfo.PmtuRBTable,
                               pPmtuEntry) == RB_FAILURE)
                {
                    ip6PmtuReleaseEntry (pPmtuEntry);
                    break;
                }
            }

            break;

        case ICMP6_TIME_EXCEEDED:
            ICMP6_INC_IN_TMEXCEEDED (pIp6Cxt->Icmp6Stats);
            ICMP6_INTF_INC_IN_TMEXCEEDED (pIf6);
            break;

        case ICMP6_PKT_PARAM_PROBLEM:
            ICMP6_INC_IN_PARAMPROB (pIp6Cxt->Icmp6Stats);
            ICMP6_INTF_INC_IN_PARAMPROB (pIf6);
            break;

        default:
            IP6_TRC_ARG1 (pIp6Cxt->u4ContextId, ICMP6_MOD_TRC,
                          DATA_PATH_TRC, ICMP6_NAME,
                          "ICMP6: Icmp6RcvErrMsg: Rx unknown pkt Type = 0x%X\n",
                          u1Type);
            ICMP6_INC_IN_BADCODE (pIp6Cxt->Icmp6Stats);
            ICMP6_INTF_INC_IN_BADCODE (pIf6);
            break;

    }

    if (IP6_BUF_DATA_LEN (pBuf) < IP6_HDR_LEN)
    {
        IP6IF_INC_IN_TRUNCATED (pIf6);
        IP6SYS_INC_IN_TRUNCATED (pIp6Cxt->Ip6SysStats);

        IP6_TRC_ARG1 (pIp6Cxt->u4ContextId, ICMP6_MOD_TRC,
                      DATA_PATH_TRC, ICMP6_NAME,
                      "ICMP6: Icmp6RcvErrMsg: Rx unknown pkt Type = 0x%X\n",
                      u1Type);

        IP6_TRC_ARG1 (pIp6Cxt->u4ContextId, ICMP6_MOD_TRC,
                      DATA_PATH_TRC, ICMP6_NAME,
                      "ICMP6: Icmp6RcvErrMsg: Got error packet, truncated in "
                      "IPv6 header Len = %d", IP6_BUF_DATA_LEN (pBuf));

        if (Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE, ICMP6_MODULE) ==
            IP6_FAILURE)
        {
            IP6_TRC_ARG3 (pIp6Cxt->u4ContextId, ICMP6_MOD_TRC,
                          BUFFER_TRC, ICMP6_NAME,
                          "ICMP6: Icmp6RcvErrMsg: %s buf err: Type = %d Bufptr = %p",
                          ERROR_FATAL_STR, ERR_BUF_RELEASE, pBuf);
        }

        return;
    }

    CRU_BUF_Move_ValidOffset (pBuf, IP6_HDR_LEN + sizeof (tIcmp6ErrMsg));
    IP6_BUF_READ_OFFSET (pBuf) = 0;
    if (CRU_BUF_Get_ChainValidByteCount (pBuf) < IP6_HDR_LEN)
    {
        if (Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE, ICMP6_MODULE) ==
            IP6_FAILURE)
        {
            IP6_TRC_ARG (pIp6Cxt->u4ContextId, ICMP6_MOD_TRC,
                         BUFFER_TRC, ICMP6_NAME, "ICMP6: Icmp6RcvErrMsg err");
        }
        return;
    }

    /*
     * Get IPv6 header from the icmp error packet
     */

    pIp6 = (tIp6Hdr *) Ip6BufRead (pBuf, (UINT1 *) &ip6Hdr, 0,
                                   (UINT4) (sizeof (tIp6Hdr)), 0);

    if (pIp6 == NULL)
    {
        Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE, ICMP6_MODULE);
        return;
    }
    if (NetIpv6InvokeIcmpv6ErrorSend (pBuf, &(pIp6->srcAddr),
                                      u1Type, u1Code,
                                      IP6_BUF_DATA_LEN (pBuf) - IP6_HDR_LEN,
                                      pIf6->u4Index, pIp6->u1Nh) ==
        NETIPV6_SUCCESS)
    {
        return;
    }

    if (Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE, ICMP6_MODULE) ==
        IP6_FAILURE)
    {
        IP6_TRC_ARG3 (pIp6Cxt->u4ContextId, ICMP6_MOD_TRC,
                      BUFFER_TRC, ICMP6_NAME,
                      "ICMP6: Icmp6RcvErrMsg: %s buf err: Type = %d Bufptr = %p",
                      ERROR_FATAL_STR, ERR_BUF_RELEASE, pBuf);
    }
}

/******************************************************************************
 * DESCRIPTION : Process received informational messages
 * INPUTS      : interface structure (tIp6If  *pIf6)       &
 *               ip6 main header     (tIp6Hdr *pIp6)       &
 *               type of the message (UINT1     u1Type)      &
 *               code of the message (UINT1     u1Code)      &
 *               length of icmp6 packet(UINT2   u2Len)       &
 *               control message packet(tCRU_BUF_CHAIN_HEADER *pBuf) 
 * OUTPUTS     : None
 * RETURNS     : None
 * NOTES       :
 ******************************************************************************/
VOID
Icmp6RcvInfoMsg (tIp6If * pIf6, tIp6Hdr * pIp6, UINT1 u1Type, UINT1 u1Code,
                 UINT2 u2Len, UINT1 u1Flag, tCRU_BUF_CHAIN_HEADER * pBuf)
{
    tIp6Cxt            *pIp6Cxt = NULL;
#ifdef MLD_WANTED
    tNetIpv6HliParams   Ip6HliParams;
#endif

    pIp6Cxt = pIf6->pIp6Cxt;

    switch (u1Type)
    {
        case ROUTER_SOLICITATION:
        case ROUTER_ADVERTISEMENT:
        case NEIGHBOUR_SOLICITATION:
        case NEIGHBOUR_ADVERTISEMENT:
        case ROUTE_REDIRECT:
            Nd6Rcv (pIf6, pIp6, u1Type, u2Len, u1Flag, pBuf);
            break;

#ifdef MIP6_WANTED
        case IP6_DHAD_REQUEST_MSG:
        case IP6_DHAD_REPLY_MSG:
        case IP6_DHAD_PREFIX_SOL:
        case IP6_DHAD_PREFIX_ADV:
            HARcv (pIf6, pIp6, u1Type,
                   (UINT2) (IP6_BUF_READ_OFFSET (pBuf) - sizeof (tIcmp6PktHdr)),
                   pBuf);
            break;

#endif
        case ICMP6_ECHO_REQUEST:
            if ((u1Flag & ADDR6_TENTATIVE) != ADDR6_TENTATIVE)
            {
                ICMP6_INC_IN_ECHO_REQ (pIp6Cxt->Icmp6Stats);
                ICMP6_INTF_INC_IN_ECHO_REQ (pIf6);
                Icmp6EnqueueInfoMsg (pIf6, pIp6, u1Type, u1Code, u2Len, pBuf);
            }
            else
            {
                IP6_TRC_ARG1 (pIp6Cxt->u4ContextId, ICMP6_MOD_TRC,
                              DATA_PATH_TRC, ICMP6_NAME,
                              "ICMP6: Icmp6RcvInfoMsg: Ec ReqRx addrsd to "
                              "tentative addr %s\n",
                              Ip6ErrPrintAddr (ERROR_MINOR, &pIp6->dstAddr));
                ICMP6_INC_IN_ERRS (pIp6Cxt->Icmp6Stats);
                ICMP6_INTF_INC_IN_ERRS (pIf6);
                Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE, ICMP6_MODULE);
            }
            break;

        case ICMP6_ECHO_REPLY:
            if ((u1Flag & ADDR6_TENTATIVE) != ADDR6_TENTATIVE)
            {
                ICMP6_INC_IN_ECHO_RESP (pIp6Cxt->Icmp6Stats);
                ICMP6_INTF_INC_IN_ECHO_RESP (pIf6);
                Icmp6EnqueueInfoMsg (pIf6, pIp6, u1Type, u1Code, u2Len, pBuf);
            }
            else
            {
                IP6_TRC_ARG1 (pIp6Cxt->u4ContextId, ICMP6_MOD_TRC,
                              DATA_PATH_TRC, ICMP6_NAME,
                              "ICMP6: Icmp6RcvInfoMsg: Ec Rep Rx addrsd to "
                              "tentative addr%s\n",
                              Ip6ErrPrintAddr (ERROR_MINOR, &pIp6->dstAddr));
                ICMP6_INC_IN_ERRS (pIp6Cxt->Icmp6Stats);
                ICMP6_INTF_INC_IN_ERRS (pIf6);
                Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE, ICMP6_MODULE);
            }
            break;
        case ICMP6_MLD_QUERY:
        case ICMP6_MLD_REPORT:
        case ICMP6_MLDV2_REPORT:
        case ICMP6_MLD_DONE:

            /* RFC 3810 -  Section 5
             * All MLDv2 messages MUST be sent with a link-local IPv6 Source
             * Address, an IPv6 Hop Limit of 1, and an IPv6 Router Alert option
             * [RFC2711] in a Hop-by-Hop Options header.

             * If Nexthdr of IP6 packet is not a Hop-By-Hop header
             * Packets will not be fed into MLD module*/

            if (pIp6->u1Nh != NH_H_BY_H)
            {
                ICMP6_INC_IN_ERRS (pIp6Cxt->Icmp6Stats);
                ICMP6_INTF_INC_IN_ERRS (pIf6);
                /* Release buf */
                if (Ip6BufRelease
                    (pIp6Cxt->u4ContextId, pBuf, FALSE,
                     ICMP6_MODULE) == IP6_FAILURE)
                {
                    IP6_TRC_ARG3 (pIp6Cxt->u4ContextId, ICMP6_MOD_TRC,
                                  BUFFER_TRC, ICMP6_NAME,
                                  "ICMP6: Icmp6RcvInfoMsg: Buf rel failed: %s "
                                  "buf err: Type = %d Bufptr = %p",
                                  ERROR_FATAL_STR, ERR_BUF_RELEASE, pBuf);
                }

                IP6_TRC_ARG (pIp6Cxt->u4ContextId, ICMP6_MOD_TRC,
                             CONTROL_PLANE_TRC, ICMP6_NAME,
                             "ICMP6: Icmp6RcvInfoMsg: MLD packet received "
                             "without hop-by-hop option header "
                             "Packet dropped\r\n");
                break;
            }
            switch (u1Type)
            {
                case ICMP6_MLD_QUERY:
                    ICMP6_INC_IN_MLD_QUERY (pIp6Cxt->Icmp6Stats);
                    ICMP6_INTF_INC_IN_MLD_QUERY (pIf6);
                    break;
                case ICMP6_MLDV2_REPORT:
                case ICMP6_MLD_REPORT:
                    ICMP6_INC_IN_MLD_REPORT (pIp6Cxt->Icmp6Stats);
                    ICMP6_INTF_INC_IN_MLD_REPORT (pIf6);
                    break;
                case ICMP6_MLD_DONE:
                    ICMP6_INC_IN_MLD_DONE (pIp6Cxt->Icmp6Stats);
                    ICMP6_INTF_INC_IN_MLD_DONE (pIf6);
                    break;
            }
#ifdef MLD_WANTED
            CRU_BUF_Move_ValidOffset (pBuf,
                                      IP6_BUF_READ_OFFSET (pBuf) -
                                      sizeof (tIcmp6PktHdr));
            Ip6HliParams.unIpv6HlCmdType.AppRcv.u4Index = pIf6->u4Index;
            MEMCPY (&Ip6HliParams.unIpv6HlCmdType.AppRcv.Ip6SrcAddr,
                    &pIp6->srcAddr, sizeof (tIp6Addr));
            Ip6HliParams.unIpv6HlCmdType.AppRcv.u4PktLength = u2Len;
            Ip6HliParams.unIpv6HlCmdType.AppRcv.pBuf = pBuf;

            MLDInput (&Ip6HliParams);
            break;
#endif
        default:
            IP6_TRC_ARG1 (pIp6Cxt->u4ContextId, ICMP6_MOD_TRC,
                          DATA_PATH_TRC, ICMP6_NAME,
                          "ICMP6: Icmp6RcvInfoMsg: Rx unknown pkt Type = 0x%X\n",
                          u1Type);
            if ((u1Flag & ADDR6_TENTATIVE) != ADDR6_TENTATIVE)
            {
                ICMP6_INC_IN_BADCODE (pIp6Cxt->Icmp6Stats);
                ICMP6_INTF_INC_IN_BADCODE (pIf6);
            }

            if (Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE, ICMP6_MODULE)
                == IP6_FAILURE)
            {
                IP6_TRC_ARG3 (pIp6Cxt->u4ContextId, ICMP6_MOD_TRC,
                              BUFFER_TRC, ICMP6_NAME,
                              "ICMP6: Icmp6RcvInfoMsg: Buf rel failed: %s "
                              "buf err: Type = %d Bufptr = %p",
                              ERROR_FATAL_STR, ERR_BUF_RELEASE, pBuf);
            }
            break;
    }

}

/******************************************************************************
 * DESCRIPTION : This routines handles ICMPv6 error messages rate-limit timeout
 * INPUTS      : The timer node (pTimer) and the timer id (u1Id) 
 *               
 * OUTPUTS     : None
 * RETURNS     : None
 * NOTES       :
 ******************************************************************************/

VOID
Icmp6RateLimitTimeout (UINT1 u1TimerId, tIp6Timer * pTimer)
{
    tIcmp6ErrRLInfo    *pIcmp6ErrInfo;

    /*
       pIcmp6ErrInfo = ICMP6_GET_ERR_CXT_INFO_FROM_TIMER(pTimer, tIcmp6ErrRLInfo);
     */

    pIcmp6ErrInfo = (tIcmp6ErrRLInfo *) (VOID *) pTimer;

    switch (pIcmp6ErrInfo->i4Icmp6ErrRLFlag)
    {
        case ICMP6_RATE_LIMIT_ENABLE:
            pIcmp6ErrInfo->i4Icmp6ErrMsgCnt = 0;

            Ip6TmrStart (u1TimerId, IP6_ICMP6_TIMER_ID,
                         gIp6GblInfo.Ip6TimerListId, &(pTimer->appTimer),
                         ICMP6_RATE_LIMIT_TIME);
            pIcmp6ErrInfo->ErrIntervalTimer.appTimer.u2Flags =
                ICMP6_RATE_LIMIT_ENABLE;

            break;

        case ICMP6_RATE_LIMIT_DISABLE:
            pIcmp6ErrInfo->i4Icmp6ErrMsgCnt = 0;
            pIcmp6ErrInfo->ErrIntervalTimer.appTimer.u2Flags =
                ICMP6_RATE_LIMIT_DISABLE;

            break;

        default:
            break;
    }
}

/******************************************************************************
 * DESCRIPTION : This routines rate limits icmpv6 error messages.
 *               Whenever  sending ICMPv6 error messages through Icmp6SendErrMsg() 
 *               function,  if the rate limit is configured it will monitor the 
 *               no of out going error messages (icmp6Stats.u4OutErrs) for that 
 *               interface 
 * INPUTS      : interface structure (tIp6If  *pIf6)                & 
 *            
 * OUTPUTS     : None
 * RETURNS     : If i4Icmp6ErrMsgCnt is less than i4Icmp6ErrMsgPerSec returns 
 *               ICMP6_ERR_MSG_ALLOW_TO_SEND otherwise ICMP6_ERR_MSG_NOT_ALLOW_TO_SEND
 * NOTES       :
******************************************************************************/

UINT1
Icmp6RatelimitErrMsg (tIp6If * pIf6)
{
    INT4                i4Icmp6ErrMsgPerSec = 0;
    INT4                i4ErrInterval = pIf6->Icmp6ErrRLInfo.i4Icmp6ErrInterval;
    INT4                i4BucketSize = pIf6->Icmp6ErrRLInfo.i4Icmp6BucketSize;

    i4Icmp6ErrMsgPerSec = ICMP_MAX_ERR_MSG_COUNT (i4ErrInterval, i4BucketSize);

    if (pIf6->Icmp6ErrRLInfo.i4Icmp6ErrMsgCnt < i4Icmp6ErrMsgPerSec)
    {
        pIf6->Icmp6ErrRLInfo.i4Icmp6ErrMsgCnt += 1;
        return ICMP6_ERR_MSG_ALLOW_TO_SEND;
    }

    return ICMP6_ERR_MSG_NOT_ALLOW_TO_SEND;
}

/******************************************************************************
 * DESCRIPTION : Sends error messages.
 * INPUTS      : interface structure (tIp6If  *pIf6)                &
 *               ip6 main header     (tIp6Hdr *pIp6)                &
 *               type of the message (UINT1     u1Type)             &
 *               code of the message (UINT1     u1Code)             &
 *               error information   (UINT4     u4Parm)             &
 *               control message packet (tCRU_BUF_CHAIN_HEADER *pBuf) 
 * OUTPUTS     : None
 * RETURNS     : None 
 * NOTES       :
 ******************************************************************************/

VOID
Icmp6SendErrMsg (tIp6If * pIf6, tIp6Hdr * pIp6,
                 UINT1 u1Type, UINT1 u1Code,
                 UINT4 u4Parm, tCRU_BUF_CHAIN_HEADER * pBuf)
{
    tIp6Cxt            *pIp6Cxt = NULL;
    UINT1               u1Copy = 0;
    UINT2               u2ErrpktLen = 0, u2_total_len = 0;
    UINT4               u4Len = 0;
    tIp6Addr           *pSrc = NULL;
    tIp6Addr           *pDst = NULL, ip6DstAddr;
    INT1                i1AddrType = 0;
    INT1                i1RateLmtErrMsg = 0;
    tIcmp6ErrMsg        icmp6ErrMsg, *p_icmp6_err_msg = NULL;
    tIcmp6PktHdr        icmp6Hdr, *pIcmp6 = NULL;
    tLlToIp6Params     *pParams = NULL;
    u2_total_len = (UINT2) IP6_BUF_DATA_LEN (pBuf);

    pIp6Cxt = pIf6->pIp6Cxt;

    /* Start the ICMPv6 rate-limit timer if the rate-limit is configured 
     * for that interface 
     * and verify the no of error messages that interface can allow to send 
     */

    if (pIf6->Icmp6ErrRLInfo.i4Icmp6DstUnReachable
        == ICMP6_DEST_UNREACHABLE_DISABLE)
    {
        IP6_TRC_ARG1 (pIp6Cxt->u4ContextId, ICMP6_MOD_TRC, DATA_PATH_TRC,
                      ICMP6_NAME, "ICMP6: Icmp6SendErrMsg: "
                      "Unable to send ICMP err Type = 0x%X\n", u1Type);
        Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE, ICMP6_MODULE);
        return;
    }

    if (pIf6->Icmp6ErrRLInfo.i4Icmp6ErrRLFlag)
    {
        if (!pIf6->Icmp6ErrRLInfo.ErrIntervalTimer.appTimer.u2Flags)
        {
            Ip6TmrStart (pIf6->pIp6Cxt->u4ContextId, IP6_ICMP6_TIMER_ID,
                         gIp6GblInfo.Ip6TimerListId,
                         (tTmrAppTimer *) (&pIf6->Icmp6ErrRLInfo.
                                           ErrIntervalTimer.appTimer),
                         ICMP6_RATE_LIMIT_TIME);

            pIf6->Icmp6ErrRLInfo.i4Icmp6ErrMsgCnt = 0;
        }

        i1RateLmtErrMsg = Icmp6RatelimitErrMsg (pIf6);
        if (i1RateLmtErrMsg == ICMP6_ERR_MSG_NOT_ALLOW_TO_SEND)
        {
            ICMP6_INC_OUT_RATE_LIMITED (pIp6Cxt->Icmp6Stats);
            ICMP6_INTF_INC_OUT_RATE_LIMITED (pIf6);

            IP6_TRC_ARG1 (pIp6Cxt->u4ContextId, ICMP6_MOD_TRC, DATA_PATH_TRC,
                          ICMP6_NAME, "ICMP6: Unable to send ICMPv6 msg,"
                          " its rate-limited : err Type = 0x%X  \n", u1Type);

            Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE, ICMP6_MODULE);

            return;
        }
    }

    /*
     * Check if this packet is addressed to Mcast/Bcast MAC Addr.
     * If yes, the service should be denied for this packet. So
     * do not send error Mesasge
     */
    if (((u1Type == ICMP6_PKT_PARAM_PROBLEM)
         && (u1Code == ICMP6_UNKNOWN_OPTTYPE)) || (u1Type == ICMP6_PKT_TOO_BIG))
    {
        /* skip */
    }
    else
    {
        pParams = (tLlToIp6Params *) IP6_GET_MODULE_DATA_PTR (pBuf);
        if (pParams->u1Dos == TRUE)
        {
            Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE, ICMP6_MODULE);
            return;
        }
    }

    /*
     * Determine the length of the error message which can be sent and
     * truncate the original packet if needed.
     *  Every ICMPv6 message must fit within min IPv6 MTU 
     */

    if (pIp6->u1Nh == NH_ICMP6)
    {
        pIcmp6 = (tIcmp6PktHdr *) Ip6BufRead (pBuf, (UINT1 *) &icmp6Hdr,
                                              IP6_HDR_LEN,
                                              sizeof (tIcmp6PktHdr), 0);
        if ((pIcmp6 == NULL) || (pIcmp6->u1Type < ICMP6_INFO_MSG_TYPE_MIN_VAL))
        {
            Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE, ICMP6_MODULE);
            return;
        }
    }

    if ((u2_total_len + sizeof (tIcmp6ErrMsg) + sizeof (tIp6Hdr)) > IP6_MIN_MTU)
    {
        u2ErrpktLen = IP6_MIN_MTU - (sizeof (tIcmp6ErrMsg) + sizeof (tIp6Hdr));
        CRU_BUF_Delete_BufChainAtEnd (pBuf, u2_total_len - u2ErrpktLen);
    }

    else
    {
        u2ErrpktLen = u2_total_len;
    }

    /*
     * validate the destination address of the erroroneous packet
     */

    i1AddrType = Ip6AddrType (&pIp6->dstAddr);

    if (i1AddrType == ADDR6_MULTI)
    {
        if ((u1Type != ICMP6_PKT_TOO_BIG) &&
            (!((u1Type == ICMP6_PKT_PARAM_PROBLEM) &&
               (u1Code == ICMP6_UNKNOWN_OPTTYPE))))
        {
            ICMP6_INC_OUT_ERRS (pIp6Cxt->Icmp6Stats);
            ICMP6_INTF_INC_OUT_ERRS (pIf6);

            IP6_TRC_ARG1 (pIp6Cxt->u4ContextId, ICMP6_MOD_TRC,
                          DATA_PATH_TRC, ICMP6_NAME,
                          "ICMP6: Icmp6SendErrMsg: Unable to send ICMP err Type"
                          " = 0x%X to pkt got for mcast addr\n", u1Type);
            Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE, ICMP6_MODULE);
            return;
        }
        else
        {
            pSrc = NULL;
        }
    }

    /*
     * if packet is for me, assign my address as source address,
     * otherwise leave it as NULL
     */
    else if (Ip6IsPktToMeInCxt (pIp6Cxt, (UINT1 *) &i1AddrType, 1,
                                &pIp6->dstAddr, pIf6) == IP6_SUCCESS)
    {
        pSrc = &pIp6->dstAddr;
        Ip6AddrCopy (pSrc, &pIp6->dstAddr);
    }

    /*
     * validate the source address of the erroroneous packet
     */

    i1AddrType = Ip6AddrType (&pIp6->srcAddr);

    if ((i1AddrType == ADDR6_MULTI) || (i1AddrType == ADDR6_UNSPECIFIED))
    {
        ICMP6_INC_OUT_ERRS (pIp6Cxt->Icmp6Stats);
        ICMP6_INTF_INC_OUT_ERRS (pIf6);

        IP6_TRC_ARG1 (pIp6Cxt->u4ContextId, ICMP6_MOD_TRC, DATA_PATH_TRC,
                      ICMP6_NAME, "ICMP6: Icmp6SendErrMsg: Unable to send ICMP "
                      "err as Src Addr= %s is mcast or unspecd\n",
                      Ip6ErrPrintAddr (ERROR_MINOR, &pIp6->srcAddr));
        Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE, ICMP6_MODULE);
        return;
    }

    pDst = &ip6DstAddr;
    Ip6AddrCopy (pDst, &pIp6->srcAddr);

    switch (u1Type)
    {
        case ICMP6_DEST_UNREACHABLE:
            ICMP6_INC_OUT_DST_UNREACH (pIp6Cxt->Icmp6Stats);
            ICMP6_INTF_INC_OUT_DST_UNREACH (pIf6);
            break;

        case ICMP6_PKT_TOO_BIG:
            ICMP6_INC_OUT_TOOBIG (pIp6Cxt->Icmp6Stats);
            ICMP6_INTF_INC_OUT_TOOBIG (pIf6);
            break;

        case ICMP6_TIME_EXCEEDED:
            ICMP6_INC_OUT_TMEXCEEDED (pIp6Cxt->Icmp6Stats);
            ICMP6_INTF_INC_OUT_TMEXCEEDED (pIf6);
            break;

        case ICMP6_PKT_PARAM_PROBLEM:
            ICMP6_INC_OUT_PARAMPROB (pIp6Cxt->Icmp6Stats);
            ICMP6_INTF_INC_OUT_PARAMPROB (pIf6);
            break;

        default:
            break;
    }

    u4Len = (sizeof (tIcmp6ErrMsg)) + u2ErrpktLen;

    /*
     * Form the ICMPv6 Error Message Header in the sending packet
     */

    p_icmp6_err_msg = (tIcmp6ErrMsg *)
        Ip6BufPptr (pBuf, (UINT1 *) &icmp6ErrMsg,
                    sizeof (tIcmp6ErrMsg), &u1Copy);

    if (p_icmp6_err_msg == NULL)
    {
        IP6_TRC_ARG3 (pIp6Cxt->u4ContextId, ICMP6_MOD_TRC,
                      BUFFER_TRC, ICMP6_NAME,
                      "ICMP6: Icmp6SendErrMsg: Buffer Allocation Failure, %s "
                      "buf err: Type = %d Bufptr = %p",
                      ERROR_FATAL_STR, ERR_BUF_PPTR, pBuf);
        Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE, ICMP6_MODULE);
        return;
    }

    p_icmp6_err_msg->icmp6Hdr.u1Type = u1Type;
    p_icmp6_err_msg->icmp6Hdr.u1Code = u1Code;
    p_icmp6_err_msg->icmp6Hdr.u2Chksum = 0;
    p_icmp6_err_msg->u4ErrInfo = HTONL (u4Parm);

    if (Ip6BufWrite (pBuf, (UINT1 *) p_icmp6_err_msg,
                     IP6_BUF_WRITE_OFFSET (pBuf),
                     sizeof (tIcmp6ErrMsg), u1Copy) == IP6_FAILURE)
    {
        ICMP6_INC_OUT_ERRS (pIp6Cxt->Icmp6Stats);
        ICMP6_INTF_INC_OUT_ERRS (pIf6);
        IP6_TRC_ARG3 (pIp6Cxt->u4ContextId, ICMP6_MOD_TRC, BUFFER_TRC,
                      ICMP6_NAME, "ICMP6: Icmp6SendErrMsg: Buf Write Failure "
                      "pkt %s buf err: Type = %d Bufptr = %p",
                      ERROR_FATAL_STR, ERR_BUF_WRITE, pBuf);
        Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE, ICMP6_MODULE);
        return;
    }

    IP6_TRC_ARG5 (pIp6Cxt->u4ContextId, ICMP6_MOD_TRC, DATA_PATH_TRC,
                  ICMP6_NAME, "ICMP6: Icmp6SendErrMsg: TX IfPtr= %p "
                  "Type= 0x%X Code= 0x%X Parm= 0x%X Buf= %p ",
                  pIf6, u1Type, u1Code, u4Parm, pBuf);

    IP6_TRC_ARG2 (pIp6Cxt->u4ContextId, ICMP6_MOD_TRC, DATA_PATH_TRC,
                  ICMP6_NAME, "Src= %s Dst= %s\n",
                  Ip6PrintAddr (pSrc), Ip6PrintAddr (pDst));

    /* Increment out messages is called wherever Icmp6SendInCxt is
     * called */
    ICMP6_INTF_INC_OUT_MSGS (pIf6);

    /*
     * Interface pointer is needed only when sending to a link-local address
     */

    if (i1AddrType != ADDR6_LLOCAL)
    {
        pIf6 = NULL;
    }

    Icmp6SendInCxt (pIp6Cxt, pIf6, pSrc, pDst, u4Len, pBuf);

}

/******************************************************************************
 * DESCRIPTION : Sends information messages to IP6 module
 * INPUTS      : ping parameter structure (tIcmp6Params *pInfo)  &
                 icmp6 packet             (tCRU_BUF_CHAIN_HEADER *pBuf)  
 * OUTPUTS     : None
 * RETURNS     : None
 * NOTES       :
 ******************************************************************************/

VOID
Icmp6SendInfoMsg (pInfo, pBuf)
     tIcmp6Params       *pInfo;
     tCRU_BUF_CHAIN_HEADER *pBuf;
{
    tIp6If             *pIf6 = NULL;
    tIp6Addr           *pIp6Src = NULL;
    tIcmp6InfoMsg       icmp6InfoMsg;
    tIcmp6InfoMsg      *p_icmp6_info_msg = NULL;
    tIp6Cxt            *pIp6Cxt = NULL;
    UINT4               u4Len = 0;
    UINT1               u1Copy = 0;

    /* When the ping request is initiated from cli, a valid interface index
     * is obtained. Else, the u4Index contains invalid index. Hence, obtain
     * the context information from the icmp6Params info */

    pIf6 = Ip6ifGetEntry (pInfo->u4Index);
    if (pIf6 != NULL)
    {
        pIp6Cxt = pIf6->pIp6Cxt;
    }
    else
    {
        pIp6Cxt = gIp6GblInfo.apIp6Cxt[pInfo->u4ContextId];
    }

    /*
     * Form the ICMPv6 Info Message Header in the sending packet
     */

    p_icmp6_info_msg = Ip6BufPptr (pBuf, (UINT1 *) &icmp6InfoMsg,
                                   sizeof (tIcmp6InfoMsg), &u1Copy);
    if (p_icmp6_info_msg == NULL)
    {
        ICMP6_INC_OUT_ERRS (pIp6Cxt->Icmp6Stats);
        if (pIf6 != NULL)
        {
            ICMP6_INTF_INC_OUT_ERRS (pIf6);
        }

        IP6_TRC_ARG3 (pIp6Cxt->u4ContextId, ICMP6_MOD_TRC,
                      BUFFER_TRC, ICMP6_NAME,
                      "ICMP6: Icmp6SendInfoMsg: Buf allocation Failure %s buf "
                      "err: Type = %d Bufptr = %p",
                      ERROR_FATAL_STR, ERR_BUF_PPTR, pBuf);
        Ip6RelMem (pIp6Cxt->u4ContextId, (UINT2) gIp6GblInfo.i4ParamId,
                   (UINT1 *) pInfo);
        Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE, ICMP6_MODULE);
        return;
    }

    p_icmp6_info_msg->icmp6Hdr.u1Type = pInfo->u1Type;
    p_icmp6_info_msg->icmp6Hdr.u1Code = pInfo->u1Code;
    p_icmp6_info_msg->icmp6Hdr.u2Chksum = 0;
    p_icmp6_info_msg->u2Id = HTONS (pInfo->u2Id);
    p_icmp6_info_msg->u2Seqno = HTONS (pInfo->u2Seq);

    if (Ip6BufWrite (pBuf, (UINT1 *) p_icmp6_info_msg,
                     IP6_BUF_WRITE_OFFSET (pBuf),
                     sizeof (tIcmp6InfoMsg), u1Copy) == IP6_FAILURE)
    {
        IP6_TRC_ARG3 (pIp6Cxt->u4ContextId, ICMP6_MOD_TRC,
                      BUFFER_TRC, ICMP6_NAME,
                      "ICMP6: Icmp6SendInfoMsg: Buf Write Failure pkt %s buf "
                      "err: Type = %d Bufptr = %p",
                      ERROR_FATAL_STR, ERR_BUF_WRITE, pBuf);
        Ip6RelMem (pIp6Cxt->u4ContextId, (UINT2) gIp6GblInfo.i4ParamId,
                   (UINT1 *) pInfo);
        Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE, ICMP6_MODULE);
        return;
    }

    u4Len = pInfo->u2Len + sizeof (tIcmp6InfoMsg);

    switch (pInfo->u1Type)
    {
        case ICMP6_ECHO_REQUEST:
            ICMP6_INC_OUT_ECHO_REQ (pIp6Cxt->Icmp6Stats);
            if (pIf6 != NULL)
            {
                ICMP6_INTF_INC_OUT_ECHO_REQ (pIf6);
            }
            break;

        case ICMP6_ECHO_REPLY:
            ICMP6_INC_OUT_ECHO_RESP (pIp6Cxt->Icmp6Stats);
            if (pIf6 != NULL)
            {
                ICMP6_INTF_INC_OUT_ECHO_RESP (pIf6);
            }
            break;

        default:
            break;
    }

    if (IS_ADDR_UNSPECIFIED (pInfo->ip6Src))
    {
        pIp6Src = NULL;
    }
    else
    {
        pIp6Src = &pInfo->ip6Src;
    }

    IP6_TRC_ARG6 (pIp6Cxt->u4ContextId, ICMP6_MOD_TRC, DATA_PATH_TRC,
                  ICMP6_NAME,
                  "ICMP6: Icmp6SendInfoMsg: TX IfPtr=%p Type=0x%X Code=0x%X "
                  "Len=%d Buf=%p Src=%s ",
                  pIf6, pInfo->u1Type, pInfo->u1Code, u4Len, pBuf,
                  Ip6PrintAddr (pIp6Src));

    IP6_TRC_ARG1 (pIp6Cxt->u4ContextId, ICMP6_MOD_TRC, DATA_PATH_TRC,
                  ICMP6_NAME, "Dst=%s\n", Ip6PrintAddr (&pInfo->ip6Dst));

    if (pIf6 != NULL)
    {
        ICMP6_INTF_INC_OUT_MSGS (pIf6);
    }

    /* Increment out messages is called wherever Icmp6SendInCxt is
     * called */
    Icmp6SendInCxt (pIp6Cxt, pIf6, pIp6Src, &pInfo->ip6Dst, u4Len, pBuf);

    Ip6RelMem (pIp6Cxt->u4ContextId, (UINT2) gIp6GblInfo.i4ParamId,
               (UINT1 *) pInfo);
}

/******************************************************************************
 * DESCRIPTION : Sends ICMP6 message packet to IP6 module
 * INPUTS      : interface structure (tIp6If  *pIf6)                &
 *               ip6 main header     (tIp6Hdr *pIp6)                &
 *               length of packet    (UINT2 u4Len)                    &
 *               control message packet (tCRU_BUF_CHAIN_HEADER *pBuf) 
 * OUTPUTS     : None
 * RETURNS     : None
 * NOTES       :
 ******************************************************************************/

VOID
Icmp6SendInCxt (pIp6Cxt, pIf6, pSrc, pDst, u4Len, pBuf)
     tIp6Cxt            *pIp6Cxt;
     tIp6If             *pIf6;
     tIp6Addr           *pSrc;
     tIp6Addr           *pDst;
     UINT4               u4Len;
     tCRU_BUF_CHAIN_HEADER *pBuf;
{
    INT1                i1AddrType = 0;
    UINT1               u1HopLmt = 0;

    ICMP6_INC_OUT_MSGS (pIp6Cxt->Icmp6Stats);
    /* 
     * The hop limit value in the echo reply should be previous
     * router Advertisement Hop Limit value
     */
    if (pIf6 != NULL)
    {
        u1HopLmt = pIp6Cxt->u4Ipv6HopLimit;
    }

    i1AddrType = Ip6AddrType (pDst);

    if ((i1AddrType == ADDR6_MULTI) || (i1AddrType == ADDR6_LLOCAL))
    {
        Ip6SendInCxt (pIp6Cxt, pIf6, pSrc, pDst, u4Len, NH_ICMP6, pBuf, u1HopLmt);
    }		
    else
    {
        Ip6SendInCxt (pIp6Cxt, NULL, pSrc, pDst, u4Len, NH_ICMP6, pBuf, u1HopLmt);
    }
}

/******************************************************************************
 * DESCRIPTION : Enqueues the echo packets on task interface.
 * INPUTS      : interface structure (tIp6If  *pIf6)       &
 *               ip6 main header     (tIp6Hdr *pIp6)       &
 *               type of the message (UINT1     u1Type)      &
 *               code of the message (UINT1     u1Code)      &
 *               control message packet (tCRU_BUF_CHAIN_HEADER *pBuf) &
 *               length of icmp6 packet (UINT2 u2Len)
 * OUTPUTS     : None
 * RETURNS     : None 
 * NOTES       :
 ******************************************************************************/

VOID
Icmp6EnqueueInfoMsg (tIp6If * pIf6, tIp6Hdr * pIp6, UINT1 u1Type, UINT1 u1Code,
                     UINT2 u2Len, tCRU_BUF_CHAIN_HEADER * pBuf)
{
    tIcmp6Params       *pParam = NULL;
    UINT4               u4ContextId = 0;
    UINT2               u2Id = 0, u2Seq = 0;
    UINT2               au2Data[IP6_TWO];    /* to read icmp info hdr */

    /*
     * Form the parameter structure and enqueue the message to PING6 task
     */

    if ((pParam = (tIcmp6Params *) (VOID *)
         Ip6GetMem (VCM_INVALID_VC, (UINT2) gIp6GblInfo.i4ParamId)) == NULL)
    {
        IP6_GBL_TRC_ARG (ICMP6_MOD_TRC, BUFFER_TRC, ICMP6_NAME,
                         "ICMP6: Icmp6EnqueueInfoMsg: Buffer for param struct. "
                         "could not be allocated");
        IP6_GBL_TRC_ARG4 (ICMP6_MOD_TRC, MGMT_TRC, ICMP6_NAME,
                          " %s mem err: Type = %d  PoolId = %d Memptr = %p\n",
                          ERROR_FATAL_STR, ERR_MEM_GET_PARAM,
                          gIp6GblInfo.i4ParamId, (UINT1 *) pParam);
        Ip6BufRelease (VCM_INVALID_VC, pBuf, FALSE, ICMP6_MODULE);
        return;
    }

    if (pIf6 != NULL)
    {
        pParam->u4Index = pIf6->u4Index;
        pParam->u4ContextId = pIf6->pIp6Cxt->u4ContextId;
        u4ContextId = pIf6->pIp6Cxt->u4ContextId;
    }
    else
    {
        pParam->u4Index = IP6_MAX_VALUE;
        pParam->u4ContextId = VCM_INVALID_VC;
        u4ContextId = VCM_INVALID_VC;
    }

    if (u2Len <= sizeof (tIcmp6InfoMsg))
    {
	    IP6_TRC_ARG (u4ContextId, ICMP6_MOD_TRC,
			    ALL_FAILURE_TRC, ICMP6_NAME,
			    "ICMP6: Icmp6EnqueueInfoMsg: Buffer length and packet length mismatch ");
	    Ip6BufRelease (u4ContextId, pBuf, FALSE, ICMP6_MODULE);
	    Ip6RelMem (u4ContextId, (UINT2) gIp6GblInfo.i4ParamId,
			    (UINT1 *) pParam);
	    return;
    }
    pParam->u2Len = (UINT2) (u2Len - sizeof (tIcmp6InfoMsg));
    pParam->u1Type = u1Type;
    pParam->u1Code = u1Code;

    Ip6AddrCopy (&pParam->ip6Src, &pIp6->srcAddr);
    Ip6AddrCopy (&pParam->ip6Dst, &pIp6->dstAddr);

    /* check for linearity of the icmp hdr */
    if (CRU_BUF_Get_DataPtr_IfLinear (pBuf,
                                      IP6_BUF_READ_OFFSET (pBuf) - IP6_FIVE,
                                      IP6_FOUR) != NULL)
    {
        /* if the buffer is linear */
        IP6_BUF_GET_2_BYTE (pBuf, IP6_BUF_READ_OFFSET (pBuf), u2Id);
        IP6_BUF_READ_OFFSET (pBuf) += IP6_TWO;
        IP6_BUF_GET_2_BYTE (pBuf, IP6_BUF_READ_OFFSET (pBuf), u2Seq);
        IP6_BUF_READ_OFFSET (pBuf) += IP6_TWO;
    }
    else
    {
        /* if the buffer is non-linear */
        if (Ip6BufRead (pBuf, (UINT1 *) &au2Data,
                        IP6_BUF_READ_OFFSET (pBuf), IP6_FOUR, 1) == NULL)
        {
            IP6_TRC_ARG (u4ContextId, ICMP6_MOD_TRC,
                         ALL_FAILURE_TRC, ICMP6_NAME,
                         "ICMP6: Icmp6EnqueueInfoMsg: copy from bufchain failed ");
            IP6_TRC_ARG2 (u4ContextId, ICMP6_MOD_TRC,
                          ALL_FAILURE_TRC, ICMP6_NAME,
                          "%s gen err: Type = %d \n", ERROR_FATAL_STR,
                          ERR_BUF_READ);
            Ip6BufRelease (u4ContextId, pBuf, FALSE, ICMP6_MODULE);
            Ip6RelMem (u4ContextId, (UINT2) gIp6GblInfo.i4ParamId,
                       (UINT1 *) pParam);
            return;
        }
        else
        {
            u2Id = NTOHS (au2Data[0]);
            u2Seq = NTOHS (au2Data[1]);
            IP6_BUF_READ_OFFSET (pBuf) += IP6_FOUR;
        }
    }

    pParam->u2Id = u2Id;
    pParam->u2Seq = u2Seq;
    CRU_BUF_Move_ValidOffset (pBuf, IP6_BUF_READ_OFFSET (pBuf));
    Ip6SetParams ((tIp6Params *) (VOID *) pParam, pBuf);

    if (!
        ((OsixSendToQ
          (SELF, (const UINT1 *) PING6_TASK_PING_INPUT_QUEUE, pBuf,
           OSIX_MSG_URGENT) == OSIX_SUCCESS)
         &&
         (OsixSendEvent
          (SELF, (const UINT1 *) PING6_TASK_NAME,
           PING6_PING_MESG_RECD_EVENT) == OSIX_SUCCESS)))
    {
        IP6_TRC_ARG (u4ContextId, ICMP6_MOD_TRC, ALL_FAILURE_TRC, ICMP6_NAME,
                     "ICMP6: Icmp6EnqueueInfoMsg: Enqueue to PING6 failed ");
        IP6_TRC_ARG2 (u4ContextId, ICMP6_MOD_TRC, ALL_FAILURE_TRC,
                      ICMP6_NAME, "%s gen err: Type = %d \n", ERROR_FATAL_STR,
                      ERR_GEN_TQUEUE_FAIL);
        Ip6BufRelease (u4ContextId, pBuf, FALSE, ICMP6_MODULE);
        Ip6RelMem (u4ContextId, (UINT2) gIp6GblInfo.i4ParamId,
                   (UINT1 *) pParam);
    }

}

/******************************************************************************
 * DESCRIPTION : This function receives the enqueued packet by PING6 task for
                 ICMP6 submodule in IP6 task.
 * INPUTS      : ping data (tCRU_BUF_CHAIN_HEADER *pBuf)
 * OUTPUTS     : None
 * RETURNS     : None
 * NOTES       :
 ******************************************************************************/

VOID
Icmp6RcvPingMsg (pBuf)
     tCRU_BUF_CHAIN_HEADER *pBuf;
{
    tIcmp6Params       *pInfo = NULL;

    pInfo = (tIcmp6Params *) Ip6GetParams (pBuf);

    if (pInfo == NULL)
    {
        IP6_GBL_TRC_ARG (ICMP6_MOD_TRC, DATA_PATH_TRC, ICMP6_NAME,
                         "ICMP6: Icmp6RcvPingMsg: Msg Rx from Ping wout params\n");
        Ip6BufRelease (VCM_INVALID_VC, pBuf, FALSE, ICMP6_MODULE);
        return;
    }

    Icmp6SendInfoMsg (pInfo, pBuf);
}

/***************************** END OF FILE **********************************/
