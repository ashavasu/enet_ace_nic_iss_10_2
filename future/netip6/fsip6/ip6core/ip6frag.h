#ifndef _IP6FRAG_H
#define _IP6FRAG_H

/*
 *----------------------------------------------------------------------------- 
 *    FILE NAME                    :    ip6frag.h                     
 *                                                                              
 *    PRINCIPAL AUTHOR             :    Anshul Garg                             
 *                                                                              
 *    SUBSYSTEM NAME               :    IPv6                                    
 *                                                                              
 *    MODULE NAME                  :    Fragment and reassembly submodule 
 *                                                                              
 *    LANGUAGE                     :    C                                       
 *                                                                              
 *    TARGET ENVIRONMENT           :    UNIX                                    
 *                                                                              
 *    DATE OF FIRST RELEASE        :    Dec-02-1996                             
 *                                                                              
 *    DESCRIPTION                  :    This file contains typedef and macro
 *                                      definitions used during fragmentation
 *                                      and reassembly of packets.
 *    CHANGE RECORD                :                                            
 *----------------------------------------------------------------------------- 
 *     VERSION            AUTHOR/                        DESCRIPTION OF CHANGE  
 *                        DATE                                                  
 *     1.0                Anshul/                        Created file           
 *                        Nov 30, 1996                              
 *----------------------------------------------------------------------------- 
 */


/*
 * An entry corresponding to each fragment which is queued against a
 * Reassembly Entry. The details include the starting and ending offsets
 * of this fragment, link to previous and next fragments besides the buffer
 * itself
 */

typedef struct _IP6_FRAG_QUE
{
    tTMO_DLL_NODE          link;
    UINT2                  u2StartOff;
    UINT2                  u2EndOff;
    tCRU_BUF_CHAIN_HEADER  *pBuf;
}
tIp6FragQueEntry;


/*
 * IPv6 Fragment Header
 */

typedef struct _IP6_FRAG
{
    UINT1  u1NextHdr;
    UINT1  u1Rsvd;
    UINT2  u2FragOff;
    UINT4  u4Id;
}
tIp6FragHdr;


/* Constants and macros related to Reassembly */

#define  MAX_REASM_SIZE  65535 
#define  IP6_REASM_TIME  60    

#define GET_REASM_PTR_FROM_REASM_LST(x) \
        ((UINT1 *) (x) - IP6_OFFSET(tIp6ReasmEntry,timer))

#endif /* _IP6FRAG_H */
