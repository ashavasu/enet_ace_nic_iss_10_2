enum {
    MAX_PING6_MAX_DST_SIZING_ID,
    PING6_MAX_SIZING_ID
};


#ifdef  _PING6SZ_C
tMemPoolId PING6MemPoolIds[ PING6_MAX_SIZING_ID];
INT4  Ping6SizingMemCreateMemPools(VOID);
VOID  Ping6SizingMemDeleteMemPools(VOID);
INT4  Ping6SzRegisterModuleSizingParams( CHR1 *pu1ModName);
#else  /*  _PING6SZ_C  */
extern tMemPoolId PING6MemPoolIds[ ];
extern INT4  Ping6SizingMemCreateMemPools(VOID);
extern VOID  Ping6SizingMemDeleteMemPools(VOID);
extern INT4  Ping6SzRegisterModuleSizingParams( CHR1 *pu1ModName); 
#endif /*  _PING6SZ_C  */


#ifdef  _PING6SZ_C
tFsModSizingParams FsPING6SizingParams [] = {
{ "tPing6", "MAX_PING6_MAX_DST", sizeof(tPing6),MAX_PING6_MAX_DST, MAX_PING6_MAX_DST,0 },
{"\0","\0",0,0,0,0}
};
#else  /*  _PING6SZ_C  */
extern tFsModSizingParams FsPING6SizingParams [];
#endif /*  _PING6SZ_C  */


