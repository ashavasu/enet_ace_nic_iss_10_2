/*******************************************************************
 * $Id: ip6trace.h,v 1.12 2015/04/30 13:02:31 siva Exp $
 *********************************************************************/

#ifndef _IP6_TRACE_H
#define _IP6_TRACE_H

#ifdef TRACE_WANTED

#define  IP6_DBG_FLAG(Cxt)  Ip6GetDebugFlag(Cxt)
#define   CONTEXT_INFO(Fmt, Cxt)  " Context - %d: " Fmt, Cxt

#define  ICMP6_NAME       "ICMP6"    
#define  UDP6_NAME        "UDP6"     
#define  ND6_NAME         "ND6"      
#define  PING6_NAME       "PING6"    
#define  MIP6_NAME        "MIPv6"
#define  NETIP6_NAME      "NETIP6"
#define  MBSM_IP6         "MbsmIp6"   


/*a note on what should be passed to the arguments given below
* ------------------------------------------------------------
*
* cmod - contains module dependent masks given above for IP6,ND6,UDP6 AND RIP6
* mask - contains masks given in file BASE_DIR/inc/Trace.h 
* mod  - contains the sub-module name  
* fmt  - format of the message
* arg1, arg2, arg3 , arg4, ...... - arguments to be passed
*/

#define IP6_PKT_DUMP(Cxt, cmod, mask,  mod, pBuf, Length) \
        {  \
             if (((IP6_DBG_FLAG (Cxt) & cmod) == cmod) && \
                 ((IP6_DBG_FLAG (Cxt) & mask) == DUMP_TRC)) { \
                 DumpPkt (pBuf, IP6_MAX_VALUE); } \
        }

#define IP6_TRC_ARG(Cxt, cmod,mask,mod,fmt) if ((IP6_DBG_FLAG (Cxt) & cmod) == cmod ) { \
                     UtlTrcLog ((UINT4) IP6_DBG_FLAG (Cxt),mask,mod, \
                                CONTEXT_INFO (fmt, Cxt)); }

#define IP6_TRC_ARG1(Cxt, cmod,mask,mod,fmt,arg1)\
               if ((IP6_DBG_FLAG (Cxt) & cmod) == cmod ){\
           UtlTrcLog ((UINT4) IP6_DBG_FLAG (Cxt),\
                                  mask, \
                                  mod,  \
                                  CONTEXT_INFO (fmt, Cxt), \
                                  arg1);\
                }

#define IP6_TRC_ARG2(Cxt, cmod,mask,mod,fmt,arg1,arg2)\
               if ((IP6_DBG_FLAG (Cxt) & cmod) == cmod ){\
           UtlTrcLog ((UINT4) IP6_DBG_FLAG (Cxt),\
                                  mask, \
                                  mod,  \
                                  CONTEXT_INFO (fmt, Cxt), \
                                  arg1, \
                  arg2);\
                }

#define IP6_TRC_ARG3(Cxt, cmod,mask,mod,fmt,arg1,arg2,arg3)\
             if ((IP6_DBG_FLAG (Cxt) & cmod) == cmod) {\
            UtlTrcLog ((UINT4) IP6_DBG_FLAG (Cxt),\
                                  mask, \
                                  mod,  \
                                  CONTEXT_INFO (fmt, Cxt), \
                                  arg1,\
                  arg2,\
                  arg3);\
               }

#define IP6_TRC_ARG4(Cxt, cmod,mask,mod,fmt,arg1,arg2,arg3,arg4)\
               if ((IP6_DBG_FLAG (Cxt) & cmod) == cmod ){\
            UtlTrcLog ((UINT4) IP6_DBG_FLAG (Cxt),\
                                  mask, \
                                  mod,  \
                                  CONTEXT_INFO (fmt, Cxt), \
                                  arg1,\
                                  arg2,\
                                  arg3,\
                                  arg4);\
                }

#define IP6_TRC_ARG5(Cxt, cmod,mask,mod,fmt,arg1,arg2,arg3,arg4,arg5)\
              if ((IP6_DBG_FLAG (Cxt) & cmod) == cmod ){\
            UtlTrcLog ((UINT4) IP6_DBG_FLAG (Cxt),\
                                  mask, \
                                  mod,  \
                                  CONTEXT_INFO (fmt, Cxt), \
                                  arg1,\
                                  arg2,\
                                  arg3,\
                                  arg4,\
                  arg5);\
                }

#define IP6_TRC_ARG6(Cxt, cmod,mask,mod,fmt,arg1,arg2,arg3,arg4,arg5,arg6)\
              if ((IP6_DBG_FLAG (Cxt) & cmod) == cmod ){\
            UtlTrcLog ((UINT4) IP6_DBG_FLAG (Cxt),\
                                  mask, \
                                  mod,  \
                                  CONTEXT_INFO (fmt, Cxt), \
                                  arg1,\
                                  arg2,\
                                  arg3,\
                                  arg4,\
                  arg5, \
                  arg6);\
                }

#define IP6_TRC_ARG7(Cxt, cmod,mask,mod,fmt,arg1,arg2,arg3,arg4,arg5,arg6,arg7)\
              if ((IP6_DBG_FLAG (Cxt) & cmod) == cmod ){\
            UtlTrcLog ((UINT4) IP6_DBG_FLAG (Cxt),\
                                  mask, \
                                  mod,  \
                                  CONTEXT_INFO (fmt, Cxt), \
                                  arg1,\
                                  arg2,\
                                  arg3,\
                                  arg4,\
                                  arg5,\
                                  arg6,\
                  arg7);\
                }

#define IP6_TRC_ARG8(Cxt, cmod,mask,mod,fmt,arg1,arg2,arg3,arg4,arg5,arg6,arg7,arg8)\
              if ((IP6_DBG_FLAG (Cxt) & cmod) == cmod ){\
                   UtlTrcLog ((UINT4) IP6_DBG_FLAG (Cxt) ,\
                                  mask, \
                                  mod,  \
                                  CONTEXT_INFO (fmt, Cxt), \
                                  arg1,\
                                  arg2,\
                                  arg3,\
                                  arg4,\
                                  arg5,\
                                  arg6,\
                                  arg7, \
                                  arg8);\
                }

#define IP6_TRC_ARG9(Cxt, cmod,mask,mod,fmt,arg1,arg2,arg3,arg4,arg5,arg6,arg7, \
  arg8,arg9)     if ((IP6_DBG_FLAG (Cxt) & cmod) == cmod ){ \
                   UtlTrcLog ((UINT4)IP6_DBG_FLAG (Cxt),\
                                  mask, \
                                  mod,  \
                                  CONTEXT_INFO (fmt, Cxt), \
                                  arg1,\
                                  arg2,\
                                  arg3,\
                                  arg4,\
                                  arg5,\
                                  arg6,\
                                  arg7, \
                                  arg8, \
                                  arg9); \
                }

#define IP6_TRC_ARG10(Cxt, cmod,mask,mod,fmt,arg1,arg2,arg3,arg4,arg5,arg6,arg7, \
arg8,arg9,arg10) if ((IP6_DBG_FLAG (Cxt) & cmod) == cmod ){\
                   UtlTrcLog ((UINT4)IP6_DBG_FLAG (Cxt) ,\
                                  mask, \
                                  mod,  \
                                  CONTEXT_INFO (fmt, Cxt), \
                                  arg1, \
                                  arg2, \
                                  arg3, \
                                  arg4, \
                                  arg5, \
                                  arg6, \
                                  arg7, \
                                  arg8, \
                                  arg9, \
                                  arg10 ); \
                }

#define IP6_GBL_TRC_ARG(cmod,mask,mod,fmt) if ((IP6_DBG_FLAG (VCM_DEFAULT_CONTEXT) & cmod) == cmod ) { \
            UtlTrcLog ((UINT4)IP6_DBG_FLAG (VCM_DEFAULT_CONTEXT),mask,mod, fmt); }

#define IP6_GBL_TRC_ARG1(cmod,mask,mod,fmt,arg1)\
               if ((IP6_DBG_FLAG (VCM_DEFAULT_CONTEXT) & cmod) == cmod ){\
           UtlTrcLog ((UINT4)IP6_DBG_FLAG (VCM_DEFAULT_CONTEXT),\
                                  mask, \
                                  mod,  \
                                  fmt,  \
                                  arg1);\
                }

#define IP6_GBL_TRC_ARG2(cmod,mask,mod,fmt,arg1,arg2)\
               if ((IP6_DBG_FLAG (VCM_DEFAULT_CONTEXT) & cmod) == cmod ){\
           UtlTrcLog ((UINT4)IP6_DBG_FLAG (VCM_DEFAULT_CONTEXT),\
                                  mask, \
                                  mod,  \
                                  fmt,  \
                                  arg1, \
                  arg2);\
                }

#define IP6_GBL_TRC_ARG3(cmod,mask,mod,fmt,arg1,arg2,arg3)\
             if ((IP6_DBG_FLAG (VCM_DEFAULT_CONTEXT) & cmod) == cmod) {\
            UtlTrcLog ((UINT4)IP6_DBG_FLAG (VCM_DEFAULT_CONTEXT),\
                                  mask, \
                                  mod,  \
                                  fmt,  \
                                  arg1,\
                  arg2,\
                  arg3);\
               }

#define IP6_GBL_TRC_ARG4(cmod,mask,mod,fmt,arg1,arg2,arg3,arg4)\
               if ((IP6_DBG_FLAG (VCM_DEFAULT_CONTEXT) & cmod) == cmod ){\
            UtlTrcLog (IP6_DBG_FLAG (VCM_DEFAULT_CONTEXT),\
                                  mask, \
                                  mod,  \
                                  fmt,  \
                                  arg1,\
                                  arg2,\
                                  arg3,\
                                  arg4);\
                }

#define IP6_GBL_TRC_ARG5(cmod,mask,mod,fmt,arg1,arg2,arg3,arg4,arg5)\
              if ((IP6_DBG_FLAG (VCM_DEFAULT_CONTEXT) & cmod) == cmod ){\
            UtlTrcLog (IP6_DBG_FLAG (VCM_DEFAULT_CONTEXT),\
                                  mask, \
                                  mod,  \
                                  fmt,  \
                                  arg1,\
                                  arg2,\
                                  arg3,\
                                  arg4,\
                  arg5);\
                }

#define IP6_GBL_TRC_ARG6(cmod,mask,mod,fmt,arg1,arg2,arg3,arg4,arg5,arg6)\
              if ((IP6_DBG_FLAG (VCM_DEFAULT_CONTEXT) & cmod) == cmod ){\
            UtlTrcLog (IP6_DBG_FLAG (VCM_DEFAULT_CONTEXT),\
                                  mask, \
                                  mod,  \
                                  fmt,  \
                                  arg1,\
                                  arg2,\
                                  arg3,\
                                  arg4,\
                  arg5, \
                  arg6);\
                }



#else
#define IP6_PKT_DUMP(Cxt, cmod, mask,  mod, pBuf, Length)
#define IP6_TRC_ARG(Cxt, cmod,mask,mod,fmt)
#define IP6_TRC_ARG1(Cxt, cmod,mask,mod,fmt,arg1)
#define IP6_TRC_ARG2(Cxt, cmod,mask,mod,fmt,arg1,arg2)
#define IP6_TRC_ARG3(Cxt, cmod,mask,mod,fmt,arg1,arg2,arg3)
#define IP6_TRC_ARG4(Cxt, cmod,mask,mod,fmt,arg1,arg2,arg3,arg4)
#define IP6_TRC_ARG5(Cxt, cmod,mask,mod,fmt,arg1,arg2,arg3,arg4,arg5)
#define IP6_TRC_ARG6(Cxt, cmod,mask,mod,fmt,arg1,arg2,arg3,arg4,arg5,arg6)
#define IP6_TRC_ARG7(Cxt, cmod,mask,mod,fmt,arg1,arg2,arg3,arg4,arg5,arg6,arg7)
#define IP6_TRC_ARG8(Cxt, cmod,mask,mod,fmt,arg1,arg2,arg3,arg4,arg5,arg6,arg7,arg8)
#define IP6_TRC_ARG9(Cxt, cmod,mask,mod,fmt,arg1,arg2,arg3,arg4,arg5,arg6,arg7, \
  arg8,arg9)
#define IP6_TRC_ARG10(Cxt, cmod,mask,mod,fmt,arg1,arg2,arg3,arg4,arg5,arg6,arg7, \
arg8,arg9,arg10)
#define IP6_GBL_TRC_ARG(cmod,mask,mod,fmt)
#define IP6_GBL_TRC_ARG1(cmod,mask,mod,fmt,arg1)
#define IP6_GBL_TRC_ARG2(cmod,mask,mod,fmt,arg1,arg2)
#define IP6_GBL_TRC_ARG3(cmod,mask,mod,fmt,arg1,arg2,arg3)
#define IP6_GBL_TRC_ARG4(cmod,mask,mod,fmt,arg1,arg2,arg3,arg4)
#define IP6_GBL_TRC_ARG5(cmod,mask,mod,fmt,arg1,arg2,arg3,arg4,arg5)
#define IP6_GBL_TRC_ARG6(cmod,mask,mod,fmt,arg1,arg2,arg3,arg4,arg5,arg6)
#endif
#endif
