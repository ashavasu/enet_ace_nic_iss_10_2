/********************************************************************
 * Copyright (C) 2014 Aricent Inc . All Rights Reserved
 *
 * $Id: nd6proxy.c,v 1.11 2015/09/15 06:43:34 siva Exp $ 
 *
 * Description: This file contains ND Proxy related routines
 *           
 *******************************************************************/
#ifndef __ND6_PROXY_C
#define __ND6_PROXY_C

#include "ip6inc.h"

/******************************************************************************
 * DESCRIPTION : Determines if Proxying of this NS msg is allowed
 *
 * INPUTS      : Incoming interface, IP6 Header
 * OUTPUTS     :
 *
 * RETURNS     : IP6_SUCCESS if allowed
 *               IP6_FAILURE if not allowed
 *
 ******************************************************************************/
INT4
Nd6IsNSProxyAllowed (tIp6If * pIf6, tIp6Hdr * pIp6)
{
    if (Nd6IsProxyEnabled (pIf6) == IP6_FALSE)
    {
        return IP6_FAILURE;
    }
    UNUSED_PARAM(pIp6);
    return IP6_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : Determines if Proxying is operational the interface
 *
 * INPUTS      : IP6 Interface
 * OUTPUTS     :
 *
 * RETURNS     : IP6_TRUE if Yes
 *               IP6_FALSE If No
 *
 ******************************************************************************/
INT4
Nd6IsProxyEnabled (tIp6If * pIf6)
{
    UINT4               u4Index = 0, u4ContextId = 0;

    u4ContextId = pIf6->pIp6Cxt->u4ContextId;
    u4Index = pIf6->u4Index;

    if ((Ip6ifEntryExists (u4Index) != IP6_FAILURE))
    {
        if ((NETIPV6_OPER_UP == pIf6->u1OperStatus) &&
            (ND6_PROXY_OPER_DOWN != pIf6->u1NDProxyOperStatus))
        {
            IP6_TRC_ARG1 (u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC, ND6_NAME,
                          "ND6:Nd6IsProxyEnabled: Proxy enabled on"
                          "IfIndex %d\n", u4Index);
            return IP6_TRUE;
        }
    }

    IP6_TRC_ARG1 (u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC, ND6_NAME,
                  "ND6:Nd6IsProxyEnabled: Proxy disabled on"
                  "IfIndex %d\n", u4Index);
    return IP6_FALSE;
}

/******************************************************************************
 * DESCRIPTION : Proxies the given NS on the interface on which the
 *               desination is reachable, Replaces the Original Source MAC with 
 *               the base mac
 *
 * INPUTS      : Incoming interface, IP6 Header, NS header, NS
 *               extended header
 * OUTPUTS     :
 *
 * RETURNS     : IP6_SUCCESS if allowed
 *               IP6_FAILURE if not allowed
 *
 ******************************************************************************/
INT4
Nd6ProxyNeighSol (tIp6If * pIf6, tIp6Hdr * pIp6, tNd6NeighSol * pNd6Nsol,
                  tNd6ExtHdr * pNd6Ext)
{
    UINT1               u1LlaLen = 0, au1lladdr[IP6_MAX_LLA_LEN];
    UINT4               u4Nd6Size = 0, u4_tot_size = 0, u4Woffset = 0;
    UINT4               u4ContextId = 0, u4IfIndex = 0;
    UINT4               u4IfReTransTime = 0;
    INT4                i4RetVal = IP6_FAILURE;
    tIp6Addr           *pSrcAddr = NULL, SrcAddr;
    tIp6Addr           *pDstAddr = NULL, DstAddr;
    tNd6CacheEntry     *pNd6cEntry = NULL;
    tNetIpv6RtInfo      NetIp6RtInfo;
    tNd6ProxySrcEntry  *pSrcEntry = NULL;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;

    UNUSED_PARAM (pNd6Ext);

    MEMSET (&SrcAddr, 0, sizeof (tIp6Addr));
    MEMSET (&DstAddr, 0, sizeof (tIp6Addr));
    MEMSET (&NetIp6RtInfo, 0, sizeof (tNetIpv6RtInfo));
    MEMSET (au1lladdr, 0, IP6_MAX_LLA_LEN);

    u4ContextId = pIf6->pIp6Cxt->u4ContextId;
    u4IfIndex = pIf6->u4Index;

    pSrcAddr = &SrcAddr;
    pDstAddr = &DstAddr;

    Ip6AddrCopy (pSrcAddr, &pIp6->srcAddr);
    Ip6AddrCopy (pDstAddr, &(pNd6Nsol->targAddr6));

    if (ND6_PROXY_OPER_UP_IN_PROG == pIf6->u1NDProxyOperStatus)
    {
        /* When in-progress, only RA should be proxied.
         * The input buffer is freed in caller fn */
        return IP6_SUCCESS;
    }
    /* If not local proxy, send on the intf with connectivity
     * to neighbor, else if local proxy, send on same intf */
    if (ND6_PROXY_MODE_GLOBAL == pIf6->u1NDProxyMode)
    {
        if (!IS_ADDR_LLOCAL (pNd6Nsol->targAddr6))
        {
            pIf6 = Ip6GetRouteInCxt (u4ContextId, pDstAddr, &pNd6cEntry,
                                     &NetIp6RtInfo);
            /* get the connected route alone */
            if ((NULL == pIf6) || (IP6_LOCAL_PROTOID != NetIp6RtInfo.i1Proto))
            {
                IP6_TRC_ARG1 (u4ContextId, ND6_MOD_TRC, ALL_FAILURE_TRC,
                              ND6_NAME,
                              "ND6:Nd6ProxyNeighSol: No Connected Route for this"
                              " network is available in context %d\n",
                              u4ContextId);
                return IP6_FAILURE;
            }
            if (Nd6IsProxyEnabled (pIf6) == IP6_FALSE)
            {
                return IP6_FAILURE;
            }
        }
        else
        {
            Ip6BufRelease (u4ContextId, pBuf, FALSE, ND6_MODULE);
            return IP6_SUCCESS;
        }
    }

    u4Woffset = Ip6BufWoffset (ND6_MODULE);
    u4Nd6Size = sizeof (tNd6NeighSol);

    /* No need to add LLA option while for DAD NS packet */
    if (!IS_ADDR_UNSPECIFIED (pIp6->srcAddr))
    {
          u4Nd6Size += sizeof (tNd6AddrExt);
    }
    /* Allocate a Buffer for the IPv6 packet, size of NS message */
    u4_tot_size = u4Woffset + u4Nd6Size;

    pBuf = Ip6BufAlloc (u4ContextId, u4_tot_size, u4Woffset, ND6_MODULE);
    if (pBuf == NULL)
    {
        IP6_TRC_ARG (u4ContextId, ND6_MOD_TRC, BUFFER_TRC | ALL_FAILURE_TRC,
                     ND6_NAME, "ND6:Nd6ProxyNeighSol: BufAlloc Failed\n");
        IP6_TRC_ARG3 (u4ContextId, ND6_MOD_TRC, BUFFER_TRC, ND6_NAME,
                      "ND6:Nd6ProxyNeighSol: %s buf err: Type = %d  Bufptr = %p",
                      ERROR_FATAL_STR, ERR_BUF_ALLOC, NULL);
        return (IP6_FAILURE);
    }

    /* Copy NS Header */
    i4RetVal = Ip6BufWrite (pBuf, (UINT1 *) pNd6Nsol,
                            IP6_BUF_WRITE_OFFSET (pBuf), sizeof (tNd6NeighSol),
                            TRUE);

    if (IP6_FAILURE == i4RetVal)
    {
        IP6_TRC_ARG2 (u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC,
                      ND6_NAME,
                      "ND6:Nd6ProxyNeighSol: BufWrite Failed!  BufPtr %p Offset %d\n",
                      pBuf, IP6_BUF_WRITE_OFFSET (pBuf));
        IP6_TRC_ARG3 (u4ContextId, ND6_MOD_TRC, BUFFER_TRC | ALL_FAILURE_TRC,
                      ND6_NAME,
                      "ND6:Nd6ProxyNeighSol: %s buf err: Type = %d  Bufptr = %p",
                      ERROR_FATAL_STR, ERR_BUF_WRITE, pBuf);
        Ip6BufRelease (u4ContextId, pBuf, FALSE, ND6_MODULE);
        return (IP6_FAILURE);
    }
    
    if (!IS_ADDR_UNSPECIFIED (pIp6->srcAddr)) 
    {
        u1LlaLen = MAC_ADDR_LEN;
        Ip6ifGetEthLladdr (pIf6, au1lladdr, &u1LlaLen);
        i4RetVal = Nd6FillLladdrInCxt (pIf6->pIp6Cxt, au1lladdr,
                                       ND6_SRC_LLA_EXT, pBuf);
        if (IP6_FAILURE == i4RetVal)
        {
            IP6_TRC_ARG (u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC,
                         ND6_NAME,
                         "ND6:Nd6ProxyNeighSol: Nd6FillLladdrInCxt failed\n");
            IP6_TRC_ARG3 (u4ContextId, ND6_MOD_TRC, BUFFER_TRC, ND6_NAME,
                          "ND6:Nd6ProxyNeighSol: %s buf err: Type = %d  Bufptr = %p",
                          ERROR_FATAL_STR, ERR_BUF_WRITE, pBuf);
            Ip6BufRelease (u4ContextId, pBuf, FALSE, ND6_MODULE);
            return (IP6_FAILURE);
        }
    }

    pNd6cEntry = Nd6IsCacheForAddr (pIf6, pDstAddr);

        if (NULL == pNd6cEntry)
        {
            /* Entry does not exist. Create New Entry and send NS */
            pNd6cEntry = Nd6CreateCache (pIf6, pDstAddr, NULL, 0,
                                         ND6C_INCOMPLETE, 0);
            if (NULL == pNd6cEntry)
            {
                IP6_TRC_ARG (u4ContextId, ND6_MOD_TRC,
                             MGMT_TRC | ALL_FAILURE_TRC, ND6_NAME,
                             "ND6:Nd6ProxyNeighSol: ND cache Creation Failed\n");
                Ip6BufRelease (u4ContextId, pBuf, FALSE, ND6_MODULE);
                return IP6_FAILURE;
            }
            u4IfReTransTime = Ip6GetIfReTransTime (pIf6->u4Index);
            Nd6SetMSecCacheTimer (pNd6cEntry, ND6_RETRANS_TIMER_ID,
                                  u4IfReTransTime);

        }
        pNd6cEntry->u1ProxyCache = IP6_TRUE;
        pNd6cEntry->u1AddrType = ADDR6_UNICAST;
        Ip6AddrCopy (&pNd6cEntry->recentSrcAddr, pSrcAddr);

        pSrcEntry = (tNd6ProxySrcEntry *) (VOID *)
            Ip6GetMem (u4ContextId, ND6_NS_SRC_MEM_POOL);
        if (NULL == pSrcEntry)
        {
            IP6_TRC_ARG (u4ContextId, ND6_MOD_TRC,
                         MGMT_TRC | ALL_FAILURE_TRC, ND6_NAME,
                         "ND6:Nd6ProxyNeighSol: NS Src List Node Alloc Failed\n");
            Ip6BufRelease (u4ContextId, pBuf, FALSE, ND6_MODULE);
            return IP6_FAILURE;
        }

        /* add the source to source list */
        Ip6AddrCopy (&(pSrcEntry->addr6), pSrcAddr);
        pSrcEntry->u4IfIndex = u4IfIndex;
        TMO_DLL_Add (&(pNd6cEntry->NdProxyNsSrcList), &(pSrcEntry->DllNode));
        IP6_TRC_ARG3 (u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC, ND6_NAME,
                      "ND6:Nd6ProxyNeighSol: Adding %s on Ifindex %d to SrcList. "
                      " Count %d\n", Ip6PrintAddr (&(pSrcEntry->addr6)),
                      u4IfIndex,
                      TMO_DLL_Count (&(pNd6cEntry->NdProxyNsSrcList)));

        ICMP6_INC_OUT_NSOLS (pIf6->pIp6Cxt->Icmp6Stats);
        ICMP6_INTF_INC_OUT_NSOLS (pIf6);
        (IP6_IF_STATS (pIf6))->u4OutNsols++;

        GET_ADDR_SOLICITED (pNd6cEntry->addr6, DstAddr);
        return (Ip6SendNdMsg (pIf6, pNd6cEntry, pSrcAddr,
                              pDstAddr, u4Nd6Size, pBuf,
                              ND6_PROXY_MSG, NULL));
}

/******************************************************************************
 * DESCRIPTION : Proxies the given NA on the interface on which the
 *               desination is reachable, Replaces the Original Source MAC with 
 *               the base mac
 *
 * INPUTS      : Incoming interface, IP6 Header, NA header
 * OUTPUTS     :
 *
 * RETURNS     : IP6_SUCCESS if allowed
 *               IP6_FAILURE if not allowed
 *
 ******************************************************************************/
INT4
Nd6ProxyNeighAdv (tIp6If * pIf6, tIp6Hdr * pIp6, tNd6NeighAdv * pNd6Nadv)
{
    UINT1               u1LlaLen = 0, au1lladdr[IP6_MAX_LLA_LEN];
    UINT4               u4Nd6Size = 0, u4_tot_size = 0, u4Woffset = 0;
    UINT4               u4ContextId = 0;
    INT4                i4RetVal = IP6_FAILURE;
    tNd6CacheEntry     *pNd6cEntry = NULL;
    tNd6CacheEntry     *pNd6OutcEntry = NULL;
    tNd6ProxySrcEntry  *pNd6SrcNode = NULL;
    tNd6ProxySrcEntry  *pNd6SrcNextNode = NULL;
    tIp6Addr           *pSrcAddr = NULL, SrcAddr;
    tIp6Addr           *pDstAddr = NULL, DstAddr;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;

    pSrcAddr = &SrcAddr;
    pDstAddr = &DstAddr;
    u4ContextId = pIf6->pIp6Cxt->u4ContextId;

    MEMSET (&SrcAddr, 0, sizeof (tIp6Addr));
    MEMSET (&DstAddr, 0, sizeof (tIp6Addr));
    MEMSET (au1lladdr, 0, IP6_MAX_LLA_LEN);

    Ip6AddrCopy (pSrcAddr, &pIp6->srcAddr);
    Ip6AddrCopy (pDstAddr, &(pIp6->dstAddr));

    if (ND6_PROXY_OPER_UP_IN_PROG == pIf6->u1NDProxyOperStatus)
    {
        /* When in-progress, only RA should be proxied.
         * The input buffer is freed in caller fn */
        return IP6_SUCCESS;
    }

    /*Get the cache for the NA sender. It contains DLL of NS 
     * senders*/
    pNd6cEntry = Nd6IsCacheForAddr (pIf6, pSrcAddr);
    if (NULL == pNd6cEntry)
    {
        IP6_TRC_ARG2 (u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC, ND6_NAME,
                      "ND6:Nd6ProxyNeighAdv: No Cache for %s on Ifindex %d"
                      "Remaining Count %d\n", Ip6PrintAddr (pSrcAddr),
                      pIf6->u4Index);
        return IP6_FAILURE;
    }

    /* for all nodes in DLL, get the cache and proxy NA */
    UTL_DLL_OFFSET_SCAN (&(pNd6cEntry->NdProxyNsSrcList), pNd6SrcNode,
                         pNd6SrcNextNode, tNd6ProxySrcEntry *)
    {
        pIf6 = IP6_INTERFACE (pNd6SrcNode->u4IfIndex);
        Ip6AddrCopy (pDstAddr, &(pNd6SrcNode->addr6));
        TMO_DLL_Delete (&(pNd6cEntry->NdProxyNsSrcList),
                        &(pNd6SrcNode->DllNode));

        IP6_TRC_ARG3 (u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC, ND6_NAME,
                      "ND6:Nd6ProxyNeighAdv: SrcList has %s on Ifindex %d"
                      "Remaining Count %d\n", Ip6PrintAddr (pDstAddr),
                      pNd6SrcNode->u4IfIndex,
                      TMO_DLL_Count (&(pNd6cEntry->NdProxyNsSrcList)));

        i4RetVal = Ip6RelMem (pIf6->pIp6Cxt->u4ContextId,
                              (UINT2) ND6_NS_SRC_MEM_POOL,
                              (UINT1 *) pNd6SrcNode);

        if (IP6_FAILURE == i4RetVal)
        {
            IP6_TRC_ARG (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                         BUFFER_TRC | ALL_FAILURE_TRC, ND6_NAME,
                         "ND6:Nd6ProxyNeighAdv: Src List Node Ip6RelMem Failed\n");
            continue;
        }

        /* Find cache for the NS Sender */
        pNd6OutcEntry = Nd6IsCacheForAddr (pIf6, pDstAddr);
        u4Woffset = (UINT4) Ip6BufWoffset (ND6_MODULE);

        /* Allocate a buffer for the IPv6 packet size of NA message */
        u4Nd6Size = sizeof (tNd6NeighAdv) + sizeof (tNd6AddrExt);
        u4_tot_size = (UINT4) (u4Woffset + u4Nd6Size);
        pBuf = Ip6BufAlloc (pIf6->pIp6Cxt->u4ContextId, u4_tot_size,
                            u4Woffset, ND6_MODULE);
        if (NULL == pBuf)
        {
            IP6_TRC_ARG (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                         BUFFER_TRC | ALL_FAILURE_TRC, ND6_NAME,
                         "ND6:Nd6ProxyNeighAdv: BufAlloc Failed\n");
            IP6_TRC_ARG3 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC, BUFFER_TRC,
                          ND6_NAME,
                          "ND6:Nd6ProxyNeighAdv: %s buf err: Type = %d  Bufptr = %p",
                          ERROR_FATAL_STR, ERR_BUF_ALLOC, NULL);
            continue;
        }

        /* Copy NA Header */
        i4RetVal = Ip6BufWrite (pBuf, (UINT1 *) pNd6Nadv,
                                IP6_BUF_WRITE_OFFSET (pBuf),
                                sizeof (tNd6NeighAdv), TRUE);
        if (IP6_FAILURE == i4RetVal)
        {
            IP6_TRC_ARG2 (u4ContextId, ND6_MOD_TRC,
                          BUFFER_TRC | ALL_FAILURE_TRC, ND6_NAME,
                          "ND6:Nd6ProxyNeighAdv: BufWrite Failed! BufPtr "
                          "%p Offset %d\n", pBuf, IP6_BUF_WRITE_OFFSET (pBuf));
            IP6_TRC_ARG3 (u4ContextId, ND6_MOD_TRC, BUFFER_TRC, ND6_NAME,
                          "ND6:Nd6ProxyNeighAdv: %s buf err: Type = %d  Bufptr = %p",
                          ERROR_FATAL_STR, ERR_BUF_WRITE, pBuf);
            Ip6BufRelease (u4ContextId, pBuf, FALSE, ND6_MODULE);
            continue;
        }

        u1LlaLen = MAC_ADDR_LEN;
        Ip6ifGetEthLladdr (pIf6, au1lladdr, &u1LlaLen);
        i4RetVal = Nd6FillLladdrInCxt (pIf6->pIp6Cxt, au1lladdr,
                                       ND6_TARG_LLA_EXT, pBuf);
        if (IP6_FAILURE == i4RetVal)
        {
            IP6_TRC_ARG (u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC,
                         ND6_NAME,
                         "ND6:Nd6ProxyNeighAdv: Nd6FillLladdrInCxt failed\n");
            IP6_TRC_ARG3 (u4ContextId, ND6_MOD_TRC, BUFFER_TRC, ND6_NAME,
                          "ND6:Nd6ProxyNeighAdv: %s buf err: Type = %d  Bufptr = %p",
                          ERROR_FATAL_STR, ERR_BUF_WRITE, pBuf);
            Ip6BufRelease (u4ContextId, pBuf, FALSE, ND6_MODULE);
            continue;
        }

        ICMP6_INC_OUT_NADVS (pIf6->pIp6Cxt->Icmp6Stats);
        ICMP6_INTF_INC_OUT_NADVS (pIf6);
        (IP6_IF_STATS (pIf6))->u4OutNadvs++;

        if (Ip6SendNdMsg (pIf6, pNd6OutcEntry, pSrcAddr,
                          pDstAddr, u4Nd6Size, pBuf,
                          ND6_PROXY_MSG, NULL) == IP6_FAILURE)
        {
            IP6_TRC_ARG2 (u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC, ND6_NAME,
                          "ND6:Nd6ProxyNeighAdv: Ip6SendNdMsg failed for %s -> %s \n",
                          Ip6PrintAddr (pSrcAddr), Ip6PrintAddr (pDstAddr));
            continue;
        }
    }                            /*DLL Scan End */
    SET_ADDR_UNSPECIFIED(pNd6cEntry->recentSrcAddr);
    return IP6_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : Determines if Proxying of this IPv6 pkt is required
 *
 * INPUTS      : Incoming interface, IP6 Header
 * OUTPUTS     :
 *
 * RETURNS     : IP6_TRUE if Yes
 *               IP6_FALSE if NO
 *
 ******************************************************************************/
INT4
Nd6IsNDProxyRequired (tIp6If * pIf6, tIp6Hdr * pIp6,
                      tCRU_BUF_CHAIN_HEADER * pBuf)
{
    UINT4               u4ContextId = 0;
    UINT1               u1PrefLen = 0;
    INT4                i4RetVal = IP6_FAILURE;
    tIp6Addr           *pSrcIp6Addr = NULL;
    tIp6Addr           *pDstIp6Addr = NULL;
    tIp6Addr            Ip6Addr;
    tNetIpv6RtInfo      NetIp6RtInfo;
    tNd6CacheEntry     *pNd6cEntry = NULL;
    tIp6If             *pDstIf6 = NULL;

    MEMSET (&NetIp6RtInfo, 0, sizeof (tNetIpv6RtInfo));
    MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));

    u4ContextId = pIf6->pIp6Cxt->u4ContextId;
    pSrcIp6Addr = &(pIp6->srcAddr);
    pDstIp6Addr = &(pIp6->dstAddr);

    /* incoming interface */
    if (Nd6IsProxyEnabled (pIf6) == IP6_FALSE)
    {
        return IP6_FALSE;
    }
    if (IS_ADDR_MULTI (pIp6->dstAddr))
    {
        if (IS_ADDR_UNSPECIFIED (pIp6->srcAddr))
        {
            return IP6_FALSE;
        }
        if (IS_ADDR_SOLICITED (pIp6->dstAddr) && (NH_ICMP6 == pIp6->u1Nh))
        {
            /* NS with Soliticited MC address is received.
             * In this case, check if the target address is reachable */
            i4RetVal = Nd6GetNSTargetAddr (pIf6, pIp6, pBuf, &Ip6Addr);
            if (IP6_SUCCESS == i4RetVal)
            {
                pDstIp6Addr = &Ip6Addr;
            }
            else
            {
                return IP_TRUE;
            }
        }
        else                    /* other multicast packets */
        {
            return IP_TRUE;
        }
    }

    pDstIf6 = Ip6GetRouteInCxt (u4ContextId, pDstIp6Addr, &pNd6cEntry,
                                &NetIp6RtInfo);
    /* get the connected route alone */
    if ((NULL == pDstIf6) || (IP6_LOCAL_PROTOID != NetIp6RtInfo.i1Proto))
    {
        IP6_TRC_ARG (u4ContextId, ND6_MOD_TRC, ALL_FAILURE_TRC, ND6_NAME,
                     "ND6:Nd6IsNDProxyRequired: No Connected Route for this "
                     " network is available\n");
        return IP6_FALSE;
    }
    /* Donot proxy on the default route */
    if ((IS_ADDR_UNSPECIFIED (NetIp6RtInfo.Ip6Dst))
        && (NetIp6RtInfo.u1Prefixlen == 0))
    {
        IP6_TRC_ARG (u4ContextId, ND6_MOD_TRC, ALL_FAILURE_TRC, ND6_NAME,
                     "ND6:Nd6IsNDProxyRequired: Only default route exists for "
                     "Reaching Neighbor\n");
        return IP6_FALSE;
    }

    /* check for local proxy */
    if (pIf6->u1NDProxyMode == ND6_PROXY_MODE_GLOBAL)
    {
        u1PrefLen = NetIp6RtInfo.u1Prefixlen;
        if (Ip6AddrMatch (pSrcIp6Addr, pDstIp6Addr, u1PrefLen))
        {
            IP6_TRC_ARG (u4ContextId, ND6_MOD_TRC, ALL_FAILURE_TRC,
                         ND6_NAME,
                         "ND6:Nd6IsNDProxyRequired: Src and Dst"
                         " are in same subnet. Local Proxy is not enabled\n");
            return IP6_FALSE;
        }
    }

    pDstIf6 = IP6_INTERFACE (NetIp6RtInfo.u4Index);
    /* outgoing interface */
    if (Nd6IsProxyEnabled (pDstIf6) == IP6_FALSE)
    {
        return IP6_FALSE;
    }

    return IP6_TRUE;
}

/******************************************************************************
 * DESCRIPTION : Returns the target address in a Neighbor Solicitation msg
 *
 * INPUTS      : Incoming interface, IP6 Header, Pkt buffer
 * OUTPUTS     : Target IP Address
 *
 * RETURNS     : IP6_FAILURE if any other type of pkt or error
 *               IP6_SUCCESS if success
 *
 ******************************************************************************/
INT4
Nd6GetNSTargetAddr (tIp6If * pIf6, tIp6Hdr * pIp6, tCRU_BUF_CHAIN_HEADER * pBuf,
                    tIp6Addr * pIp6TgtAddr)
{
    UINT4               u4Offset = 0;
    tNd6NeighSol        nd6Nsol, *pNd6Nsol = NULL;
    tIcmp6PktHdr        icmp6Hdr, *pIcmp6 = NULL;

    MEMSET (&nd6Nsol, 0, sizeof (tNd6NeighSol));
    MEMSET (&icmp6Hdr, 0, sizeof (tIcmp6PktHdr));

    /* To avoid warning when Traces are disabled */
    UNUSED_PARAM (pIf6);

    u4Offset = IP6_BUF_READ_OFFSET (pBuf);
    if (NH_ICMP6 == pIp6->u1Nh)
    {
        /* Preserving the buffer read offset */
        u4Offset = IP6_BUF_READ_OFFSET (pBuf);

        pIcmp6 = (tIcmp6PktHdr *) Ip6BufRead (pBuf, (UINT1 *) &icmp6Hdr,
                                              IP6_BUF_READ_OFFSET (pBuf),
                                              sizeof (tIcmp6PktHdr), 0);

        /* restore the buffer read offset */
        IP6_BUF_READ_OFFSET (pBuf) = u4Offset;

        if (NULL == pIcmp6)
        {
            IP6_TRC_ARG3 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                          BUFFER_TRC, ICMP6_NAME,
                          "ND6: Nd6GetNSTargetAddr: buffer read failed: %s buf err: "
                          "Type = %d Bufptr = %p",
                          ERROR_FATAL_STR, ERR_BUF_READ, pBuf);
            return IP6_FAILURE;
        }

        IP6_TRC_ARG5 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                      DATA_PATH_TRC, ICMP6_NAME,
                      "ND6: Nd6GetNSTargetAddr: IF = %d:Type = 0x%X:Code = 0x%X:"
                      "Buf = %p:Src = %s",
                      pIf6->u4Index, pIcmp6->u1Type, pIcmp6->u1Code,
                      pBuf, Ip6PrintAddr (&pIp6->srcAddr));

        IP6_TRC_ARG1 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                      DATA_PATH_TRC, ICMP6_NAME, " Dst = %s\n",
                      Ip6PrintAddr (&pIp6->dstAddr));

        if (NEIGHBOUR_SOLICITATION != pIcmp6->u1Type)
        {
            return IP6_FAILURE;
        }
        /* If NS,  return the target Addr */
        pNd6Nsol =
            (tNd6NeighSol *) Ip6BufRead (pBuf, (UINT1 *) &nd6Nsol, u4Offset,
                                         sizeof (tNd6NeighSol), FALSE);
        IP6_BUF_READ_OFFSET (pBuf) = u4Offset;

        if (NULL == pNd6Nsol)
        {
            IP6_TRC_ARG2 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC, BUFFER_TRC,
                          ND6_NAME,
                          "ND6:Nd6GetNSTargetAddr: Receive NS - BufRead Failed! "
                          "BufPtr %p Offset %d\n", pBuf, u4Offset);
            IP6_TRC_ARG3 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC, BUFFER_TRC,
                          IP6_NAME,
                          "ND6:Nd6GetNSTargetAddr: %s buffer error occurred, "
                          "Type = %d Module=0x%X Bufptr= %p", ERROR_FATAL_STR,
                          ERR_BUF_READ, pBuf);
            return (IP6_FAILURE);
        }
        MEMCPY (pIp6TgtAddr, &(pNd6Nsol->targAddr6), sizeof (tIp6Addr));
        IP6_TRC_ARG1 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC, BUFFER_TRC,
                      IP6_NAME, "ND6:Nd6GetNSTargetAddr: Target Addr: %s",
                      Ip6PrintAddr (pIp6TgtAddr));

        return IP6_SUCCESS;

    }
    return IP6_FAILURE;
}

/******************************************************************************
 * DESCRIPTION : Determines if Proxying is  required for the ICMP msg
 *               by RFC 4389 - NS, NA, RA, And NR msgs are only
 *               Proxied
 *
 * INPUTS      : Ingress Interface, IP6 Header, Payload len, Pkt, Next
 *               Header field of IPv6 Header
 * OUTPUTS     :
 *
 * RETURNS     : IP6_TRUE if Yes
 *               IP6_FALSE If No
 *
 ******************************************************************************/
INT4
Nd6IsProxyReqIcmp6Msg (tIp6If * pIf6, tIp6Hdr * pIp6,
                       tCRU_BUF_CHAIN_HEADER * pBuf)
{
    UINT4               u4Offset = 0;
    tIcmp6PktHdr        icmp6Hdr, *pIcmp6 = NULL;
    UINT4               u4ContextId = pIf6->pIp6Cxt->u4ContextId;

    /*Below Three Variables are declared as Unused to avoid Compilation warnings
       when Trace is disabled */
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (pIf6);

    if (NH_ICMP6 == pIp6->u1Nh)
    {
        /* Preserving the buffer read offset */
        u4Offset = IP6_BUF_READ_OFFSET (pBuf);

        pIcmp6 = (tIcmp6PktHdr *) Ip6BufRead (pBuf, (UINT1 *) &icmp6Hdr,
                                              IP6_BUF_READ_OFFSET (pBuf),
                                              sizeof (tIcmp6PktHdr), 0);

        /* restore the buffer read offset */
        IP6_BUF_READ_OFFSET (pBuf) = u4Offset;

        if (NULL == pIcmp6)
        {
            IP6_TRC_ARG3 (u4ContextId, ND6_MOD_TRC,
                          BUFFER_TRC, ICMP6_NAME,
                          "ND6: Nd6IsProxyReqIcmp6Msg: buffer read failed: %s buf err: "
                          "Type = %d Bufptr = %p",
                          ERROR_FATAL_STR, ERR_BUF_READ, pBuf);
            return IP6_FALSE;
        }

        IP6_TRC_ARG5 (u4ContextId, ND6_MOD_TRC,
                      DATA_PATH_TRC, ICMP6_NAME,
                      "ND6: Nd6IsProxyReqIcmp6Msg: IF = %d:Type = 0x%X:Code = 0x%X:"
                      "Buf = %p:Src = %s",
                      pIf6->u4Index, pIcmp6->u1Type, pIcmp6->u1Code,
                      pBuf, Ip6PrintAddr (&pIp6->srcAddr));

        IP6_TRC_ARG1 (u4ContextId, ND6_MOD_TRC,
                      DATA_PATH_TRC, ICMP6_NAME, " Dst = %s\n",
                      Ip6PrintAddr (&pIp6->dstAddr));

        switch (pIcmp6->u1Type)
        {
            case ROUTER_ADVERTISEMENT:
            case NEIGHBOUR_SOLICITATION:
            case NEIGHBOUR_ADVERTISEMENT:
            case ROUTE_REDIRECT:
                /* Intentional fall through. Only these packets should
                 * be proxied*/
                return IP6_TRUE;
            default:
                return IP6_FALSE;
        }

    }
    return IP6_FALSE;
}

/******************************************************************************
 * DESCRIPTION : Proxies the Router Advt on all downstream interfaces
 *
 * INPUTS      : IP6 Interface, IP6 header, Payload len, Buffer
 * OUTPUTS     :
 *
 * RETURNS     : NONE
 *
 ******************************************************************************/
VOID
Nd6ProxyRtrAdvt (tIp6Hdr * pIp6, tIp6If * pIf6, UINT2 u2Len,
                 tCRU_BUF_CHAIN_HEADER * pInBuf)
{

    UINT4               u4Nd6Offset = 0, u4BufLen = 0;
    UINT4               u4ContextId = pIf6->pIp6Cxt->u4ContextId;
    UINT4               u4Index = 0;
    CHR1                ac1Str[MAX_STR_LEN];
    UINT4               u4Offset = 0;

    tNd6RoutAdv         nd6RAdv, *pNd6RAdv = NULL;
    tIp6If             *pOutIf6 = NULL;

    MEMSET (ac1Str, '\0', MAX_STR_LEN);
    /* if dest is not multicast, return. This is because the RS is never proxied 
     * and hence Solicited RA should not too*/
    if (!IS_ALL_NODES_MULTI (pIp6->dstAddr))
    {
        return;
    }

    u4Nd6Offset = IP6_BUF_READ_OFFSET (pInBuf) - sizeof (tIcmp6PktHdr);
    IP6_BUF_READ_OFFSET (pInBuf) -= sizeof (tIcmp6PktHdr);

    if ((pNd6RAdv = (tNd6RoutAdv *) Ip6BufRead
         (pInBuf, (UINT1 *) &nd6RAdv, u4Nd6Offset,
          sizeof (tNd6RoutAdv), FALSE)) == NULL)
    {
        IP6_TRC_ARG2 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC, BUFFER_TRC, ND6_NAME, "ND6: Nd6ProxyRtrAdvt:Receive RA - BufRead Failed! \
                BufPtr %p Offset %d\n", pInBuf,
                      u4Nd6Offset);
        IP6_TRC_ARG3 (pIf6->pIp6Cxt->u4ContextId, ND6_MOD_TRC, BUFFER_TRC,
                      ND6_NAME,
                      "%s buffer error occurred, Type = %d Module = 0x%X Bufptr = %p ",
                      ERROR_FATAL_STR, ERR_BUF_READ, pInBuf);
        return;
    }

    /* Validate the RA message */
    if (Nd6ValidateRoutAdvInCxt (pIf6, pIp6, pNd6RAdv, u2Len) == IP6_FAILURE)
    {
        return;
    }

    if ((pIf6->b1NDProxyUpStream == ND6_PROXY_IF_DOWNSTREAM)    /*if downstream */
        || (pNd6RAdv->u1StfulFlag & ND6_P_BIT_NDPROXY_FLAG))    /* upstream with P bit */
    {

        Ip6TmrStop (u4ContextId,
                    ND6_PROXY_LOOP_TIMER_ID, gIp6GblInfo.Ip6TimerListId,
                    &(pIf6->proxyLoopTimer.appTimer));

        Ip6TmrStart (u4ContextId, ND6_PROXY_LOOP_TIMER_ID,
                     gIp6GblInfo.Ip6TimerListId,
                     &(pIf6->proxyLoopTimer.appTimer),
                     (ND6_PROXY_LOOP_TMR_INTERVAL));

		Nd6ProxyUtlSetOperStatus (pIf6, ND6_PROXY_OPER_DOWN);
		pIf6->u1PbitRtrAdvTxCount = 0;
		SPRINTF (ac1Str, "Nd6Proxy Loop Prevention - "
				"Disabled ND Proxy on Interface %d", pIf6->u4Index);
		ND_SYSLOG_INFO_MSG (ac1Str);
		return;

    }
    u4BufLen = CRU_BUF_Get_ChainValidByteCount (pInBuf);

    if (pIf6->u1NDProxyMode == ND6_PROXY_MODE_GLOBAL)
    {
        /* Scan through all downstream proxy enabled IP interfaces */
        IP6_IF_SCAN (u4Index, (UINT4) IP6_MAX_LOGICAL_IFACES)
        {

            if (Ip6ifEntryExists (u4Index) != IP6_SUCCESS)
            {
                continue;
            }
            /* Get the outgoing interface entry */
            pOutIf6 = IP6_INTERFACE (u4Index);
            /* send only on operational downstream proxy interfaces 
             * in context */
            if ((pOutIf6->pIp6Cxt->u4ContextId != u4ContextId) ||
                (pOutIf6->u1OperStatus != NETIPV6_OPER_UP) ||
                (pOutIf6->u1NDProxyOperStatus == ND6_PROXY_OPER_DOWN) ||
                (pOutIf6->b1NDProxyUpStream != ND6_PROXY_IF_DOWNSTREAM))
            {
                /* if not an operational downstream interface 
                 * fetch the next interface */
                continue;
            }
            u4Offset = IP6_BUF_READ_OFFSET (pInBuf);
            Nd6ProxyCloneAndTxRtrAdv (pIp6, pInBuf, u4BufLen, pNd6RAdv,
                                      u4ContextId, pOutIf6);
            /* Restore offset for next cloning */
            IP6_BUF_READ_OFFSET (pInBuf) = u4Offset;
        }
    }
    else                        /* Local Proxy */
    {
        Nd6ProxyCloneAndTxRtrAdv (pIp6, pInBuf, u4BufLen, pNd6RAdv, u4ContextId,
                                  pIf6);
    }
}

/******************************************************************************
 * DESCRIPTION : Clones and transmits the given Rtr Advt
 *
 * INPUTS      : IP6 Header, Input Buf, Buffer len, RA Header, Context ID
 *               Output interface
 * OUTPUTS     :
 *
 * RETURNS     : NONE
 *
 ******************************************************************************/
VOID
Nd6ProxyCloneAndTxRtrAdv (tIp6Hdr * pIp6, tCRU_BUF_CHAIN_HEADER * pInBuf,
                          UINT4 u4BufLen, tNd6RoutAdv * pNd6RAdv,
                          UINT4 u4ContextId, tIp6If * pOutIf6)
{
    INT4                i4RetVal = IP6_FAILURE;
    UINT4               u4PktLen = 0, u4ExtnsLen = 0;
    tIp6Addr           *pSrcAddr = NULL, SrcAddr;
    tIp6Addr           *pDstAddr = NULL, DstAddr;
    tCRU_BUF_CHAIN_HEADER *pOutBuf = NULL;

    MEMSET (&SrcAddr, 0, sizeof (tIp6Addr));
    MEMSET (&DstAddr, 0, sizeof (tIp6Addr));

    pSrcAddr = &SrcAddr;
    pDstAddr = &DstAddr;
    Ip6AddrCopy (pSrcAddr, &(pIp6->srcAddr));
    Ip6AddrCopy (pDstAddr, &(pIp6->dstAddr));
    if ((pOutBuf = Ip6BufAlloc (u4ContextId, u4BufLen,
                                sizeof (tIp6Hdr), ND6_MODULE)) == NULL)
    {
        IP6_TRC_ARG (u4ContextId, ND6_MOD_TRC, BUFFER_TRC | ALL_FAILURE_TRC,
                     ND6_NAME, "ND6:Nd6ProxyRtrAdvt:  - BufAlloc Failed\n");
        IP6_TRC_ARG3 (u4ContextId, ND6_MOD_TRC, BUFFER_TRC, ND6_NAME,
                      "ND6:Nd6ProxyRtrAdvt: %s buf err: Type = %d  Bufptr = %p",
                      ERROR_FATAL_STR, ERR_BUF_ALLOC, NULL);
        return;
    }

    /*  Set P bit in Header n write to buf */
    pNd6RAdv->u1StfulFlag |= ND6_P_BIT_NDPROXY_FLAG;

    i4RetVal = Ip6BufWrite (pOutBuf, (UINT1 *) pNd6RAdv,
                            IP6_BUF_WRITE_OFFSET (pOutBuf),
                            sizeof (tNd6RoutAdv), TRUE);
    if (i4RetVal == IP6_FAILURE)
    {
        Ip6BufRelease (u4ContextId, pOutBuf, FALSE, ND6_MODULE);
        return;
    }

    u4ExtnsLen = u4BufLen - (IPV6_HEADER_LEN + sizeof (tNd6RoutAdv));
    Nd6ProxyProcessIcmp6Extns (pIp6, pOutIf6, (UINT2) u4ExtnsLen, pInBuf,
                               pOutBuf);

    u4PktLen = u4BufLen - IPV6_HEADER_LEN;
    pOutIf6->u1PbitRtrAdvTxCount++;

    ICMP6_INTF_INC_OUT_RADVS (pOutIf6);
    ICMP6_INC_OUT_RADVS (pOutIf6->pIp6Cxt->Icmp6Stats);
    (IP6_IF_STATS (pOutIf6))->u4OutRoutadvs++;

    /* When two RA's are proxied, enable proxy oper-status on the interface */
    if (pOutIf6->u1PbitRtrAdvTxCount >= IP6_TWO)
    {
        Nd6ProxyUtlSetOperStatus (pOutIf6, ND6_PROXY_OPER_UP);
    }

    if (Ip6SendNdMsg (pOutIf6, NULL, pSrcAddr, pDstAddr,
                      u4PktLen, pOutBuf, 
                      ND6_PROXY_MSG, NULL) == IP6_FAILURE)
    {
        Ip6BufRelease (u4ContextId, pOutBuf, FALSE, ND6_MODULE);
        return;
    }
}

/******************************************************************************
 * DESCRIPTION : For RA, extended header replacecs the SLLA with
 *               base-mac
 *
 * INPUTS      : Incoming interface, IP6 Header, NS header, Extended
 *               header len, Input buffer, Output buffer
 * OUTPUTS     :
 *
 * RETURNS     : IP6_SUCCESS if allowed
 *               IP6_FAILURE if not allowed
 *
 ******************************************************************************/
INT4
Nd6ProxyProcessIcmp6Extns (tIp6Hdr * pIp6, tIp6If * pIf6, UINT2 u2ExtnsLen,
                           tCRU_BUF_CHAIN_HEADER * pBuf,
                           tCRU_BUF_CHAIN_HEADER * pOutBuf)
{
    tNd6ExtHdr         *pNd6Ext = NULL;
    UINT4               u4CurExtLen = 0;
    UINT1               u1LlaLen = 0, au1lladdr[IP6_MAX_LLA_LEN];

    UNUSED_PARAM (pIp6);
    MEMSET (au1lladdr, 0, IP6_MAX_LLA_LEN);
    while (u2ExtnsLen)
    {
        if ((pNd6Ext = Nd6ExtractExtnHdrInCxt (pIf6->pIp6Cxt, pBuf)) == NULL)
        {
            return (IP6_FAILURE);
        }
        IP6_BUF_READ_OFFSET (pBuf) -= IP6_TYPE_LEN_BYTES;
        u4CurExtLen = (UINT4) (pNd6Ext->u1Len * 8);
        switch (pNd6Ext->u1Type)
        {
            case ND6_SRC_LLA_EXT:

                u1LlaLen = MAC_ADDR_LEN;
                Ip6ifGetEthLladdr (pIf6, au1lladdr, &u1LlaLen);
                if (Nd6FillLladdrInCxt (pIf6->pIp6Cxt, au1lladdr,
                                        ND6_SRC_LLA_EXT,
                                        pOutBuf) == IP6_FAILURE)
                {
                    return (IP6_FAILURE);
                }

                IP6_BUF_READ_OFFSET (pBuf) += u4CurExtLen;

                break;

            default:
                /* copy to dest as such */
                if (CRU_FAILURE == CRU_BUF_Copy_BufChains (pOutBuf, pBuf,
                                                           IP6_BUF_WRITE_OFFSET
                                                           (pOutBuf),
                                                           IP6_BUF_READ_OFFSET
                                                           (pBuf), u4CurExtLen))
                {
                    return IP6_FAILURE;
                }

                IP6_BUF_READ_OFFSET (pBuf) += u4CurExtLen;
                IP6_BUF_WRITE_OFFSET (pOutBuf) += u4CurExtLen;

        }

        u2ExtnsLen = (UINT2) (u2ExtnsLen - u4CurExtLen);
    }
    return IP6_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : For Redirect Msg, extended header replacecs the TLLA with
 *               the target's Mac address if it's known
 *
 * INPUTS      : Incoming interface, IP6 Header, NS header, Extended
 *               header len, Input buffer, Output buffer, Target Addr,
 *               ND size
 * OUTPUTS     : Final ND size. 
 *
 * RETURNS     : IP6_SUCCESS if allowed
 *               IP6_FAILURE if not allowed
 *
 ******************************************************************************/
INT4
Nd6ProxyProcessTLLA (tIp6Hdr * pIp6, tIp6If * pIf6, UINT2 u2ExtnsLen,
                     tCRU_BUF_CHAIN_HEADER * pBuf,
                     tCRU_BUF_CHAIN_HEADER * pOutBuf, tIp6Addr * pTargAddr6,
                     UINT4 *u4Nd6Size)
{
    UINT4               u4CurExtLen = 0;
    UINT1               au1lladdr[IP6_MAX_LLA_LEN];

    tNd6CacheEntry     *pNd6cEntry = NULL;
    tNd6ExtHdr         *pNd6Ext = NULL;
    tNd6CacheLanInfo   *pNd6cLinfo = NULL;
    UNUSED_PARAM (pIp6);
    MEMSET (au1lladdr, 0, IP6_MAX_LLA_LEN);

    while (u2ExtnsLen)
    {
        if ((pNd6Ext = Nd6ExtractExtnHdrInCxt (pIf6->pIp6Cxt, pBuf)) == NULL)
        {
            return (IP6_FAILURE);
        }
        /* Move back to start of Extn header */
        IP6_BUF_READ_OFFSET (pBuf) -= IP6_TYPE_LEN_BYTES;
        u4CurExtLen = (UINT4) (pNd6Ext->u1Len * 8);

        switch (pNd6Ext->u1Type)
        {
            case ND6_TARG_LLA_EXT:

                if ((pNd6cEntry = Nd6IsCacheForAddr (pIf6, pTargAddr6)) != NULL)
                {
                    pNd6cLinfo =
                        (tNd6CacheLanInfo *) (VOID *) pNd6cEntry->pNd6cInfo;
                    if (Nd6FillLladdrInCxt
                        (pIf6->pIp6Cxt, pNd6cLinfo->lladdr, ND6_TARG_LLA_EXT,
                         pOutBuf) == IP6_FAILURE)
                    {
                        return (IP6_FAILURE);
                    }

                }
                else            /* TLLA not known. So skip and reduce Redir len */
                {
                    *u4Nd6Size -= u4CurExtLen;
                }

                IP6_BUF_READ_OFFSET (pBuf) += u4CurExtLen;
                break;

            default:            /* copy to dest as such */

                if (CRU_FAILURE == CRU_BUF_Copy_BufChains (pOutBuf, pBuf,
                                                           IP6_BUF_WRITE_OFFSET
                                                           (pOutBuf),
                                                           IP6_BUF_READ_OFFSET
                                                           (pBuf), u4CurExtLen))
                {
                    return IP6_FAILURE;
                }
        }

        u2ExtnsLen = (UINT2) (u2ExtnsLen - u4CurExtLen);
    }
    return IP6_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : Enabled the Proxy oper status on the interface when
 *               the loop timer expires
 *
 * INPUTS      : IP6 Interface
 * OUTPUTS     :
 *
 * RETURNS     : VOID
 *               
 *
 ******************************************************************************/
VOID
Nd6ProxyTimeout (tIp6If * pIf6)
{
    CHR1                ac1Str[MAX_STR_LEN];
    MEMSET (ac1Str, '\0', MAX_STR_LEN);

    if (ND6_PROXY_IF_DOWNSTREAM == pIf6->b1NDProxyUpStream)
    {
        pIf6->u1NDProxyOperStatus = ND6_PROXY_OPER_UP_IN_PROG;
        SPRINTF (ac1Str, "ND6Proxy Loop timer expired - "
                 "ND Proxy Oper-up in progress on Interface %d", pIf6->u4Index);
    }
    else
    {
        Nd6ProxyUtlSetOperStatus (pIf6, ND6_PROXY_OPER_UP);
        SPRINTF (ac1Str, "ND6Proxy Loop timer expired - "
                 "ND Proxy Oper-up on Interface %d", pIf6->u4Index);
    }
    pIf6->u1PbitRtrAdvTxCount = 0;
    ND_SYSLOG_INFO_MSG (ac1Str);
}

/******************************************************************************
 * DESCRIPTION : Proxies the given Neighbor redirect on the interface on which 
 *               the desination is reachable
 *
 * INPUTS      : Incoming interface, IP6 Header, NR header, Buffer, 
 *               ND msg length
 * OUTPUTS     :
 *
 * RETURNS     : IP6_SUCCESS if allowed
 *               IP6_FAILURE if not allowed
 *
 ******************************************************************************/
INT4
Nd6ProxyNeighRedir (tIp6If * pIf6,
                    tIp6Hdr * pIp6,
                    tNd6Redirect * pNd6RdirHdr,
                    tCRU_BUF_CHAIN_HEADER * pBuf, UINT2 u2Len)
{
    tCRU_BUF_CHAIN_HEADER *pOutBuf = NULL;

    UINT4               u4BufLen = 0, u4ExtnsLen = 0;
    UINT4               u4Woffset = 0, u4Nd6Len = 0;
    UINT4               u4Offset = 0;
    UINT4               u4ContextId = 0;
    INT4                i4RetVal = 0;
    tIp6Addr           *pSrcAddr = NULL, SrcAddr;
    tIp6Addr           *pDstAddr = NULL, DstAddr;
    tNd6CacheEntry     *pNd6cDst = NULL;
    tIp6If             *pDstIf6 = NULL;
    tNetIpv6RtInfo      NetIp6RtInfo;

    MEMSET (&NetIp6RtInfo, 0, sizeof (tNetIpv6RtInfo));
    MEMSET (&SrcAddr, 0, sizeof (tIp6Addr));
    MEMSET (&DstAddr, 0, sizeof (tIp6Addr));

    pSrcAddr = &SrcAddr;
    pDstAddr = &DstAddr;
    u4ContextId = pIf6->pIp6Cxt->u4ContextId;

    Ip6AddrCopy (pSrcAddr, &(pIp6->srcAddr));
    Ip6AddrCopy (pDstAddr, &(pIp6->dstAddr));

    if (ND6_PROXY_OPER_UP_IN_PROG == pIf6->u1NDProxyOperStatus)
    {
        /* When in-progress, only RA should be proxied.
         * The input buffer is freed in caller fn */
        return IP6_SUCCESS;
    }

    u4Woffset = Ip6BufWoffset (ND6_MODULE);
    IP6_BUF_READ_OFFSET (pBuf) = u4Woffset + sizeof (tNd6Redirect);

    u4ExtnsLen = u2Len - sizeof (tNd6Redirect);

    u4BufLen = CRU_BUF_Get_ChainValidByteCount (pBuf);
    if ((pOutBuf = Ip6BufAlloc (u4ContextId, u4BufLen,
                                sizeof (tIp6Hdr), ND6_MODULE)) == NULL)
    {

        return IP6_FAILURE;
    }

    if (Ip6BufWrite (pOutBuf,
                     (UINT1 *) pNd6RdirHdr, IP6_BUF_WRITE_OFFSET (pOutBuf),
                     sizeof (tNd6Redirect), TRUE) == IP6_FAILURE)
    {
        IP6_TRC_ARG2 (u4ContextId, ND6_MOD_TRC, BUFFER_TRC | ALL_FAILURE_TRC,
                      ND6_NAME, "ND6: Nd6FillRedirect:Fill Redirect - BufWrite Failed! \
                       BufPtr %p Offset %d\n", pBuf, IP6_BUF_WRITE_OFFSET (pBuf));
        IP6_TRC_ARG3 (u4ContextId, ND6_MOD_TRC, BUFFER_TRC, ND6_NAME,
                      "%s buf err: Type = %d  Bufptr = %p", ERROR_FATAL_STR,
                      ERR_BUF_WRITE, pBuf);
        Ip6BufRelease (pIf6->pIp6Cxt->u4ContextId, pOutBuf, FALSE, ND6_MODULE);
        return (IP6_FAILURE);
    }

    u4Nd6Len = u4BufLen - IPV6_HEADER_LEN;
    u4ExtnsLen = u2Len - sizeof (tNd6Redirect);

    u4Offset = IP6_BUF_READ_OFFSET (pBuf);
    i4RetVal =
        Nd6ProxyProcessTLLA (pIp6, pIf6, (UINT2) u4ExtnsLen, pBuf, pOutBuf,
                             &(pNd6RdirHdr->targAddr6), &u4Nd6Len);
    if (IP6_FAILURE == i4RetVal)
    {
        IP6_TRC_ARG2 (u4ContextId, ND6_MOD_TRC, BUFFER_TRC | ALL_FAILURE_TRC,
                      ND6_NAME, "ND6: Nd6ProxyProcessTLLA failed! \
                       BufPtr %p Offset %d\n", pBuf, IP6_BUF_WRITE_OFFSET (pBuf));
        IP6_TRC_ARG3 (u4ContextId, ND6_MOD_TRC, BUFFER_TRC, ND6_NAME,
                      "%s buf err: Type = %d  Bufptr = %p", ERROR_FATAL_STR,
                      ERR_BUF_WRITE, pBuf);
        Ip6BufRelease (pIf6->pIp6Cxt->u4ContextId, pOutBuf, FALSE, ND6_MODULE);
        return (IP6_FAILURE);
    }
    /*Restore original Offset */
    IP6_BUF_READ_OFFSET (pBuf) = u4Offset;

    /* For global proxy, get route to find out IF and ND Cache if present */
    if (ND6_PROXY_MODE_GLOBAL == pIf6->u1NDProxyMode)
    {
        pDstIf6 = Ip6GetRouteInCxt (u4ContextId, pDstAddr,
                                    &pNd6cDst, &NetIp6RtInfo);
    }
    else
    {
        /* For local, fetch cache if present */
        pDstIf6 = pIf6;
        pNd6cDst = Nd6IsCacheForAddr (pDstIf6, pDstAddr);
    }
    return (Ip6SendNdMsg (pDstIf6, pNd6cDst, pSrcAddr, pDstAddr,
                          u4Nd6Len, pOutBuf, ND6_PROXY_MSG, NULL));
}

/******************************************************************************
 * DESCRIPTION : Proxies other types of Unicast/Multicast packets. 
 *               No error except Pkt too-big is generated. No Hop
 *               limit change is done
 *               
 *
 * INPUTS      : Incoming interface, IP6 Header, Payload len, Buffer
 * OUTPUTS     :
 *
 * RETURNS     : IP6_SUCCESS if allowed
 *               IP6_FAILURE if not allowed
 *
 ******************************************************************************/
VOID
Nd6ProxyForward (tIp6Hdr * pIp6, tIp6If * pIf6, UINT4 u4Len,
                 tCRU_BUF_CHAIN_HEADER * pBuf)
{
    tIp6Cxt            *pIp6Cxt = NULL;
    UINT4               u4DstMtu = 0;
    tNd6CacheEntry     *pNd6c = NULL;
    tNd6CacheEntry     *pNd6cDst = NULL;
    tIp6If             *pDstIf6 = NULL;
    tIp6Addr           *pNextHop = NULL;

    tNetIpv6RtInfo      NetIp6RtInfo;
#ifdef MLD_WANTED
    UINT4               u4IfIndex = 0;
    tIp6If             *pOutIf6 = NULL;
    tCRU_BUF_CHAIN_HEADER *pCloneBuf = NULL;
    BOOL1               b1McastPktSentFlag = IP6_FALSE;
    BOOL1               b1McastPktTooBig = IP6_FALSE;
#endif
    UINT4               u4DestAddrType = 0;
    UINT4               u4TotLen = 0;

    pIp6Cxt = pIf6->pIp6Cxt;

    IP6IF_INC_IN_FORW_DGRAMS (pIf6);
    IP6SYS_INC_IN_FORW_DGRAMS (pIp6Cxt->Ip6SysStats);
    MEMSET (&NetIp6RtInfo, 0, sizeof (tNetIpv6RtInfo));
    u4TotLen = u4Len + NTOHS (pIp6->u2Len);

#ifdef SLI_WANTED
    SliEnqueueIpv6RawPacketInCxt (pIp6Cxt->u4ContextId, pIp6->u1Nh,
                                  (u4TotLen - u4Len),
                                  u4Len, (UINT1 *) pIp6, pBuf);
#endif

    if (pIf6->u1Ipv6IfFwdOperStatus == IP6_IF_FORW_DISABLE)
    {
        UNUSED_PARAM (pIp6);
        UNUSED_PARAM (pIf6);
        UNUSED_PARAM (u4Len);

        /* HOST - Doesn't fwd any datagrams , so release buffer */

        Ip6BufRelease (pIf6->pIp6Cxt->u4ContextId, pBuf, FALSE,
                       IP6_FWD_SUBMODULE);
        return;

    }

    if (pIp6Cxt->u4ForwFlag == IP6_FORW_DISABLE)
    {
        IP6IF_INC_OUT_DISCARDS (pIf6);
        IP6SYS_INC_OUT_DISCARDS (pIp6Cxt->Ip6SysStats);
        Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE, IP6_FWD_SUBMODULE);
        return;
    }

    IP6_TRC_ARG2 (pIp6Cxt->u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC, ND6_NAME,
                  "ND6:Nd6ProxyForward: Src Addr = %s Dst Addr = %s\n",
                  Ip6PrintAddr (&pIp6->srcAddr), Ip6PrintAddr (&pIp6->dstAddr));

#ifdef MLD_WANTED
    if (IS_ADDR_MULTI (pIp6->dstAddr))
    {
        u4IfIndex = 1;

        IP6_IF_SCAN (u4IfIndex, (UINT4) IP6_MAX_LOGICAL_IF_INDEX)
        {
            pOutIf6 = gIp6GblInfo.apIp6If[u4IfIndex];
            if (pOutIf6 == NULL)
                continue;

            /* Packet should not be proxied on the interface on which
             * it has been disabled
             */
            if (ND6_PROXY_OPER_UP != pOutIf6->u1NDProxyOperStatus)
                continue;

            /* Packet should not be forwarded on the interface on which
             * it has been received
             */
            if (pOutIf6 == pIf6)
                continue;

            u4DstMtu = Ip6ifGetMtu (pOutIf6);
            if (u4Len > u4DstMtu)
            {
                b1McastPktTooBig = IP6_TRUE;
                continue;
            }

            if ((Ip6SendIfCheck (pOutIf6) == IP6_SUCCESS) &&
                (MLDIsCacheEntryPresent (&pIp6->dstAddr, u4IfIndex) == TRUE))
            {
                pCloneBuf = CRU_BUF_Duplicate_BufChain (pBuf);
                if (pCloneBuf == NULL)
                {
                    IP6_TRC_ARG (pIp6Cxt->u4ContextId, ND6_MOD_TRC,
                                 ALL_FAILURE_TRC, ND6_NAME,
                                 "ND6: ND6 : Dupilcate Buffer Alloc "
                                 "Failed\n");
                    Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE,
                                   IP6_FWD_SUBMODULE);
                    return;
                }
                b1McastPktSentFlag = IP6_TRUE;
                Ip6ifSend (pOutIf6, &pIp6->dstAddr, pNd6c, u4Len, pCloneBuf,
                           ADDR6_MULTI);
                pCloneBuf = NULL;
                IP6SYS_INC_OUT_FWD_DGRAMS (pIp6Cxt->Ip6SysStats);
                IP6IF_INC_OUT_FWD_DGRAMS (pOutIf6);
            }
        }

        /* if no pkts have been successfully sent and Pkt was too big,
         * then only one attempt was made to send the pkt. This failure
         * must be notified */
        if ((IP6_FALSE == b1McastPktSentFlag) && (IP6_TRUE == b1McastPktTooBig))
        {

            IP6_TRC_ARG3 (pIp6Cxt->u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC,
                          ND6_NAME,
                          "ND6:Nd6ProxyForward: Sending PktTooBig ICMPerr, "
                          "Pktlen= %d DstIP6 IF=%d OutMTU=%d\n", u4Len,
                          pOutIf6->u4Index, u4DstMtu);

            IP6SYS_INC_OUT_FRAG_FAILS (pOutIf6->pIp6Cxt->Ip6SysStats);
            IP6IF_INC_OUT_FRAG_FAILS (pOutIf6);

            pOutIf6->stats.u4TooBigerrs++;
            Icmp6SendErrMsg (pIf6, pIp6, ICMP6_PKT_TOO_BIG, 0, u4DstMtu, pBuf);
            return;
        }

        Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE, IP6_FWD_SUBMODULE);
        return;
    }
#endif

    /*
     * Find a route to this destination and get the destination interface
     * and neighbor cache pointer (if exists)
     */
    u4DestAddrType = (UINT4) Ip6AddrType (&pIp6->dstAddr);
    if ((u4DestAddrType == ADDR6_UNICAST)
        || (u4DestAddrType == ADDR6_V4_COMPAT))
    {
        pDstIf6 = Ip6GetRouteInCxt (pIp6Cxt->u4ContextId, &pIp6->dstAddr,
                                    &pNd6c, &NetIp6RtInfo);

        if (pDstIf6 == NULL || NetIp6RtInfo.i1Proto != IP6_LOCAL_PROTOID)
        {
            Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf,
                           FALSE, IP6_FWD_SUBMODULE);
            return;
        }

        pNextHop = &NetIp6RtInfo.NextHop;

        IP6_TRC_ARG4 (pIp6Cxt->u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC,
                      ND6_NAME,
                      "ND6:Nd6ProxyForward:After finding route,DstIP6 IF=%d NDC=%p "
                      "DstAddr=%s NxtHop=%s\n", pDstIf6->u4Index, pNd6c,
                      Ip6PrintAddr (&pIp6->dstAddr), Ip6PrintAddr (pNextHop));

    }
    /* 
       If a router receives a packet with a link-local destination address that
       is not one of the router's own link-local addresses on the arrival
       link, the router is expected to try to forward the packet to the
       destination on that link (subject to successful determination of the
       destination's link-layer address via the Neighbor Discovery protocol */

    else
    {
        pNd6cDst = Nd6IsCacheForAddr (pIf6, &pIp6->dstAddr);

        if ((pNd6cDst == NULL) || (pNd6cDst->pIf6 != pIf6))
        {
            IP6_TRC_ARG3 (pIp6Cxt->u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC,
                          ND6_NAME, "ND6:Nd6ProxyForward:As the src or"
                          "dst address is llocal scope both the incoming"
                          "and outgoing interface are expected to be the"
                          "same,this is violated IP6IF= %d "
                          "Src Addr= %s Dst Addr=%s\n", pIf6->u4Index,
                          Ip6ErrPrintAddr (ERROR_MINOR, &pIp6->srcAddr),
                          Ip6ErrPrintAddr (ERROR_MINOR, &pIp6->dstAddr));

            IP6IF_INC_OUT_DISCARDS (pIf6);
            IP6SYS_INC_OUT_DISCARDS (pIp6Cxt->Ip6SysStats);
            Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE,
                           IP6_FWD_SUBMODULE);
            return;
        }
        pDstIf6 = pIf6;
    }

    u4DstMtu = Ip6ifGetMtu (pDstIf6);

    if (u4Len > u4DstMtu)
    {

        IP6_TRC_ARG3 (pIp6Cxt->u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC,
                      ND6_NAME,
                      "ND6:Nd6ProxyForward: Sending PktTooBig ICMPerr, "
                      "Pktlen= %d DstIP6 IF=%d OutMTU=%d\n", u4Len,
                      pDstIf6->u4Index, u4DstMtu);

        IP6SYS_INC_OUT_FRAG_FAILS (pIp6Cxt->Ip6SysStats);
        IP6IF_INC_OUT_FRAG_FAILS (pDstIf6);

        pDstIf6->stats.u4TooBigerrs++;
        Icmp6SendErrMsg (pIf6, pIp6, ICMP6_PKT_TOO_BIG, 0, u4DstMtu, pBuf);
        return;
    }

    IP6_TRC_ARG3 (pIp6Cxt->u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC, ND6_NAME,
                  "ND6:Fwding on IF= %d Len= %d BufPtr= %p",
                  pDstIf6->u4Index, u4Len, pBuf);
    IP6_TRC_ARG3 (pIp6Cxt->u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC, ND6_NAME,
                  "ND6:Fwding on Src= %s Dst= %s Next Hop= %s\n",
                  Ip6PrintAddr (&pIp6->srcAddr),
                  Ip6PrintAddr (&pIp6->dstAddr), Ip6PrintAddr (pNextHop));

    /* i/p and o/p on same interface and mode is not local proxy, drop
     * it */
    if ((pDstIf6 == pIf6) && (pIf6->u1NDProxyMode != ND6_PROXY_MODE_LOCAL))
    {

        IP6_TRC_ARG1 (pIp6Cxt->u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC,
                      ND6_NAME,
                      "ND6:Local Proxy is not enabled on Interface %d. "
                      "Hence Dropping the packet", pIf6->u4Index);
        Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE, IP6_FWD_SUBMODULE);
        return;
    }
    if (ND6_PROXY_OPER_UP != pDstIf6->u1NDProxyOperStatus)
    {
        IP6_TRC_ARG1 (pIp6Cxt->u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC,
                      ND6_NAME,
                      "ND6:Proxy is not enabled on Outgoing Interface %d. "
                      "Hence Dropping the packet", pDstIf6->u4Index);
        Ip6BufRelease (pIp6Cxt->u4ContextId, pBuf, FALSE, IP6_FWD_SUBMODULE);
        return;
    }

    Ip6ifSend (pDstIf6, &(pIp6->dstAddr), pNd6c, u4Len, pBuf,
               NetIp6RtInfo.u1AddrType);

    IP6SYS_INC_OUT_FWD_DGRAMS (pIp6Cxt->Ip6SysStats);
    IP6IF_INC_OUT_FWD_DGRAMS (pDstIf6);
    return;
}

/******************************************************************************
 * DESCRIPTION : Set the oper status of Proxy and calls NP wrapper to set the 
 *               oper status in H/w
 *
 * INPUTS      : Interface, oper status
 * OUTPUTS     : NONE
 *
 * RETURNS     : VOID
 *
 ******************************************************************************/
VOID
Nd6ProxyUtlSetOperStatus (tIp6If * pIf6, UINT1 u1OperStatus)
{
#ifdef NPAPI_WANTED
    tFsNpIntInfo        IntInfo;
    MEMSET (&IntInfo, 0, sizeof (tFsNpIntInfo));
#endif

    pIf6->u1NDProxyOperStatus = u1OperStatus;

#ifdef NPAPI_WANTED
    IntInfo.u1IfType = Ip6GetIfType (pIf6->u4Index);

    if (IntInfo.u1IfType == IP6_L3VLAN_INTERFACE_TYPE)
    {
        IntInfo.u4PhyIfIndex = pIf6->u4Index;
    }
    if (IntInfo.u1IfType != IP6_ENET_INTERFACE_TYPE)
    {
        if (CfaGetVlanId
            (IntInfo.u4PhyIfIndex, &(IntInfo.u2VlanId)) == CFA_FAILURE)
        {
            return;
        }
    }
    else
    {
        IntInfo.u4PhyIfIndex = pIf6->u4Index;
        IntInfo.u2VlanId = 0;
    }
    if (pIf6->u1NDProxyOperStatus == ND6_PROXY_OPER_UP)
    {
        IntInfo.u1NDProxyOperStatus = FNP_TRUE;
    }
    else
    {
        IntInfo.u1NDProxyOperStatus = FNP_FALSE;
    }

    if (ND6_IS_NP_PROGRAMMING_ALLOWED () == OSIX_TRUE)
    {
        Ipv6FsNpIpv6IntfStatus (pIf6->pIp6Cxt->u4ContextId,
                                NP_IP6_IF_UPDATE, &IntInfo);
    }
#endif
}
#endif /* __ND6_PROXY_C */
