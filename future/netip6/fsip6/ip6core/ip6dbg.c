/*******************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ip6dbg.c,v 1.12 2017/12/19 13:41:54 siva Exp $
 *
 * Description: This file contains routines for  debugging
 *
 *******************************************************************/

#include "ip6inc.h"

/*
 * Private function prototypes
 */
PRIVATE VOID Nd6PrintRaRsTimer PROTO ((VOID));

/*
 * Buffers and variables used in the printing of addresses and tokens
 */

UINT1               ip6TokPrintBuf[IP6_THREE][IP6_FIFTY];
UINT2               u2TokBufNum = 0;

#define MAX_INCR(x,y)    ((x == (y-1)) ? x = 0 : ++x)
/*
 * Routine to print buffer statistics
 */

/******************************************************************************
 * DESCRIPTION : Prints the Buffer Statistics
 *
 * INPUTS      : None
 *
 * OUTPUTS     : None
 *
 * RETURNS     : VOID
 *
 * NOTES       :
 ******************************************************************************/
VOID
Ip6dbgBufStats (VOID)
{
}

/*
 * General routines
 */
/******************************************************************************
 * DESCRIPTION : Prints Interface Token
 *
 * INPUTS      : Pointer to the Interface Token (pToken), Token Length (u1TokLen)
 *
 * OUTPUTS     : None
 *
 * RETURNS     : Interface Token
 *
 * NOTES       :
 ******************************************************************************/
UINT1              *
Ip6PrintIftok (UINT1 *pToken, UINT1 u1TokLen)
{
    INT4                i4Count = 0;
    char               *p = NULL, *pc = NULL;

    if (u2TokBufNum < IP6_THREE)
    {
        p = (char *) ip6TokPrintBuf[u2TokBufNum];
    }
    else
    {
        return (NULL);
    }
    MAX_INCR (u2TokBufNum, IP6_THREE);

    pc = p;

    if ((pToken == NULL) || (!u1TokLen))
    {
        return (NULL);
    }

    memset (p, 0, IP6_FIFTY);

    for (i4Count = 0; i4Count < u1TokLen; i4Count++)
    {
        SPRINTF (p, "%02X ", pToken[i4Count]);
        p += IP6_THREE;
    }
    *p = '\0';

    return ((UINT1 *) pc);

}

/*
 * IP6 IF submodule
 */

/******************************************************************************
 * DESCRIPTION : Prints Interface Parameters
 *
 * INPUTS      : Interface Index
 *
 * OUTPUTS     : None
 *
 * RETURNS     : VOID
 *
 * NOTES       :
 ******************************************************************************/
VOID
Ip6ifPrint (UINT4 u4Index)
{

    if (Ip6ifEntryExists (u4Index) == IP6_FAILURE)
    {
        IP6_GBL_TRC_ARG1 (IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                          "Index %d is not valid for IP6 IF!!\n", u4Index);
        return;
    }

    Ip6ifPrintFromIf (gIp6GblInfo.apIp6If[u4Index]);

}

/******************************************************************************
 * DESCRIPTION : Prints Interface parameters 
 *
 * INPUTS      : Interface Pointer
 *
 * OUTPUTS     : None
 *
 * RETURNS     : VOID
 *
 * NOTES       :
 ******************************************************************************/
VOID
Ip6ifPrintFromIf (tIp6If * pIf6)
{
    INT4                i4Count = 0;
    UINT4               u4ContextId;
    UINT1               u1TokLen = 0;

    u4ContextId = pIf6->pIp6Cxt->u4ContextId;

    IP6_TRC_ARG1 (u4ContextId, IP6_MOD_TRC,
                  DATA_PATH_TRC, IP6_NAME,
                  "\nIPv6 Interface Parameters IP6 IF = %d\n", pIf6->u4Index);

    if (pIf6->u1AdminStatus == ADMIN_INVALID)
    {
        IP6_TRC_ARG (u4ContextId, IP6_MOD_TRC,
                     DATA_PATH_TRC, IP6_NAME,
                     "IThe IPv6 Interface is NOT YET allocated!!\n");
        return;
    }

    IP6_TRC_ARG5 (u4ContextId, IP6_MOD_TRC,
                  DATA_PATH_TRC, IP6_NAME,
                  "If type=%d \nIf No.=%d \nCkt No.=%d  \nAdmin Status=%d "
                  "\nOper Status=%d",
                  pIf6->u1IfType, pIf6->u4Index, pIf6->u2CktIndex,
                  pIf6->u1AdminStatus, pIf6->u1OperStatus);

    if (pIf6->u1TokLen)
    {
        IP6_TRC_ARG1 (u4ContextId, IP6_MOD_TRC,
                      DATA_PATH_TRC, IP6_NAME,
                      "Interface Token Length = %d\n Interface Token : ",
                      pIf6->u1TokLen);

        u1TokLen = MEM_MAX_BYTES (pIf6->u1TokLen, IP6_EUI_ADDRESS_LEN);
        for (i4Count = 0; i4Count < u1TokLen; i4Count++)
        {
            IP6_TRC_ARG1 (u4ContextId, IP6_MOD_TRC,
                          DATA_PATH_TRC, IP6_NAME, "%02X ",
                          pIf6->ifaceTok[i4Count]);
        }
    }
    else
    {
        /* To handle warnings in case of Trace is not defined */
        UNUSED_PARAM (u1TokLen);
        IP6_TRC_ARG (u4ContextId, IP6_MOD_TRC,
                     DATA_PATH_TRC, IP6_NAME,
                     "Interface token is not present!\n\n");
    }

    IP6_TRC_ARG2 (u4ContextId, IP6_MOD_TRC,
                  DATA_PATH_TRC, IP6_NAME,
                  "No of Lloc addron this If=%d\n No of Config addr on this If=%d\n",
                  TMO_SLL_Count (&pIf6->lla6Ilist),
                  TMO_SLL_Count (&pIf6->addr6Ilist));

}

/******************************************************************************
 * DESCRIPTION : Prints Interface Statistics
 *
 * INPUTS      : Interface Index
 *
 * OUTPUTS     : None
 *
 * RETURNS     : VOID
 *
 * NOTES       :
 ******************************************************************************/
VOID
Ip6ifPrintStats (UINT4 u4Index)
{

    if (Ip6ifEntryExists (u4Index) == IP6_FAILURE)
    {
        IP6_GBL_TRC_ARG1 (IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                          "Index %d is not valid for IP6 IF!!\n", u4Index);
        return;
    }

    Ip6ifPrintStatsFromIf (gIp6GblInfo.apIp6If[u4Index]);

}

/******************************************************************************
 * DESCRIPTION : Prints Interface Statistics
 *
 * INPUTS      : Pointer to the Interface
 *
 * OUTPUTS     : None
 *
 * RETURNS     : VOID
 *
 * NOTES       :
 ******************************************************************************/
VOID
Ip6ifPrintStatsFromIf (tIp6If * pIf6)
{
    tIp6IfStats        *pIf6Stats = NULL;
    UINT4               u4ContextId;

    u4ContextId = pIf6->pIp6Cxt->u4ContextId;
    IP6_TRC_ARG1 (u4ContextId, IP6_MOD_TRC,
                  DATA_PATH_TRC, IP6_NAME,
                  "\nIP6 If Statistics\n ===========================\n\n IP6 IF = %d\n\n",
                  pIf6->u4Index);

    if (pIf6->u1AdminStatus == ADMIN_INVALID)
    {
        IP6_TRC_ARG (u4ContextId, IP6_MOD_TRC,
                     DATA_PATH_TRC, IP6_NAME,
                     "The IPv6 Interface is NOT YET allocated!!\n\n");
        return;
    }

    pIf6Stats = &pIf6->stats;

    IP6_TRC_ARG5 (u4ContextId, IP6_MOD_TRC,
                  DATA_PATH_TRC, IP6_NAME,
                  "If type=%d IfNo.=%d\n Ckt No.= %d\n\n Pkts Rxd=%d\n "
                  "Mcast pkts Rxd=%d\n",
                  pIf6->u1IfType, pIf6->u4Index, pIf6->u2CktIndex,
                  pIf6Stats->u4InRcvs, pIf6Stats->u4InMcasts);

    IP6_TRC_ARG3 (u4ContextId, IP6_MOD_TRC,
                  DATA_PATH_TRC, IP6_NAME,
                  "Pkts to Hlayer=%d\nPkts discard despit no err=%d\n "
                  "Pkts recvd th hdr err=%d\n ",
                  pIf6Stats->u4InDelivers, pIf6Stats->u4InDiscards,
                  pIf6Stats->u4InHdrerrs);

    IP6_TRC_ARG2 (u4ContextId, IP6_MOD_TRC,
                  DATA_PATH_TRC, IP6_NAME,
                  "Pkts recvd with err in dest addr=%d\n Pkts with unknown "
                  "proto Rxd=%d\n",
                  pIf6Stats->u4InAddrerrs, pIf6Stats->u4InUnkprots);

    IP6_TRC_ARG1 (u4ContextId, IP6_MOD_TRC,
                  DATA_PATH_TRC, IP6_NAME,
                  "\nPkts recvd with insufficient data  = %d\n ",
                  pIf6Stats->u4InTruncs);

    IP6_TRC_ARG3 (u4ContextId, IP6_MOD_TRC,
                  DATA_PATH_TRC, IP6_NAME,
                  "Pkts attemptd sent=%d Mcast pkts attemptd sent=%d Pkts "
                  "attempted to be fwd=%d",
                  pIf6Stats->u4OutReqs, pIf6Stats->u4OutMcasts,
                  pIf6Stats->u4ForwDgrams);

    IP6_TRC_ARG3 (u4ContextId, IP6_MOD_TRC,
                  DATA_PATH_TRC, IP6_NAME,
                  "Pkts too big tofwdd=%d Pktsdiscarded while sent=%d Pkts "
                  "discardd as no rt's= %d",
                  pIf6Stats->u4TooBigerrs, pIf6Stats->u4OutDiscards,
                  pIf6Stats->u4OutNorts);

    IP6_TRC_ARG3 (u4ContextId, IP6_MOD_TRC,
                  DATA_PATH_TRC, IP6_NAME,
                  "Pkts which needed reass=%d\n Pkts scessfuly reasmbld=%d\n "
                  "Reasmbly fails=%d ",
                  pIf6Stats->u4Reasmreqs, pIf6Stats->u4Reasmoks,
                  pIf6Stats->u4Reasmfails);

    IP6_TRC_ARG3 (u4ContextId, IP6_MOD_TRC,
                  DATA_PATH_TRC, IP6_NAME,
                  "Pkts scssflly fragmented=%d\n Fragmntion fails=%d Fragmnts "
                  "created=%d\n",
                  pIf6Stats->u4Fragoks, pIf6Stats->u4Fragfails,
                  pIf6Stats->u4Fragcreates);

}

/******************************************************************************
 * DESCRIPTION : Prints the Entire Interface Hash Table
 *
 * INPUTS      : None
 *
 * OUTPUTS     : None
 *
 * RETURNS     : VOID
 *
 * NOTES       :
 ******************************************************************************/
VOID
Ip6ifPrintHash (VOID)
{
    tTMO_HASH_NODE     *pHnode = NULL;
    tIp6If             *pIf6 = NULL;
    UINT4               u4Hindex = 0, u4Count = 0, u4Total = 0;

    IP6_GBL_TRC_ARG (IP6_MOD_TRC, MGMT_TRC, IP6_NAME,
                     "\nIPv6 Interface Hash Table\n");

    TMO_HASH_Scan_Table (gIp6GblInfo.pIf6Htab, u4Hindex)
    {
        u4Count = TMO_HASH_Bucket_Count (gIp6GblInfo.pIf6Htab, u4Hindex);
        u4Total += u4Count;

        if (u4Count)
        {
            IP6_GBL_TRC_ARG2 (IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                              "\nBucket = %d Count = %d\n", u4Hindex, u4Count);
        }

        TMO_HASH_Scan_Bucket (gIp6GblInfo.pIf6Htab,
                              u4Hindex, pHnode, tTMO_HASH_NODE *)
        {
            pIf6 = IP6_IF_PTR_FROM_HASH (pHnode);
            IP6_GBL_TRC_ARG5 (IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                              "IfPtr = %p Index = %d Type = %d Ifnum = %d Ckt = %d\n",
                              pIf6, pIf6->u4Index, pIf6->u1IfType,
                              pIf6->u4Index, pIf6->u2CktIndex);
        }
    }

    if (!u4Total)
    {
        IP6_GBL_TRC_ARG (IP6_MOD_TRC, MGMT_TRC, IP6_NAME,
                         "\n No entries found in table\n");
    }
    else
    {
        IP6_GBL_TRC_ARG1 (IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                          "\n Total entries found in table = %d\n", u4Total);
    }

}

/******************************************************************************
 * DESCRIPTION : Prints Interface Information
 *
 * INPUTS      : Interface Index (u1Index)
 *
 * OUTPUTS     : None
 *
 * RETURNS     : VOID
 *
 * NOTES       :
 ******************************************************************************/
VOID
Ip6ifPrintLlocs (UINT4 u4Index)
{
    tIp6If             *pIf6 = NULL;
    tTMO_SLL_NODE      *pSnode = NULL;
    tIp6LlocalInfo     *pLladdr = NULL;
    UINT4               u4Total = 0;
    INT4                i4DadTime = 0;
    UINT4               u4ContextId;

    pIf6 = Ip6ifGetEntry (u4Index);
    if (pIf6 == NULL)
    {
        IP6_GBL_TRC_ARG1 (IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                          "Index %d is not valid for IP6 IF!!\n", u4Index);
        return;
    }

    u4ContextId = pIf6->pIp6Cxt->u4ContextId;

    IP6_TRC_ARG (u4ContextId, IP6_MOD_TRC, MGMT_TRC, IP6_NAME,
                 "\nIPv6 Interface Link Local Addresses\n");

    IP6_TRC_ARG1 (u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC,
                  IP6_NAME, "IP6 IF = %d\n\n", pIf6->u4Index);

    if (pIf6->u1AdminStatus == ADMIN_INVALID)
    {
        IP6_TRC_ARG (u4ContextId, IP6_MOD_TRC, MGMT_TRC, IP6_NAME,
                     "The IPv6 Interface is NOT YET allocated!!\n\n");
        return;
    }

    u4Total = TMO_SLL_Count (&pIf6->lla6Ilist);
    if (!u4Total)
    {
        IP6_TRC_ARG (u4ContextId, IP6_MOD_TRC, MGMT_TRC, IP6_NAME,
                     "No Link-local address on this interface\n\n");
        return;
    }

    IP6_TRC_ARG1 (u4ContextId, IP6_MOD_TRC, MGMT_TRC, IP6_NAME,
                  "Number of Link-loc address on this interface = %d\n\n",
                  u4Total);

    TMO_SLL_Scan (&pIf6->lla6Ilist, pSnode, tTMO_SLL_NODE *)
    {
        pLladdr = IP6_LLADDR_PTR_FROM_SLL (pSnode);
        IP6_TRC_ARG3 (u4ContextId, IP6_MOD_TRC, MGMT_TRC, IP6_NAME,
                      "Address =  %s\n Status = %d DADs sent = %d ",
                      Ip6PrintAddr (&pLladdr->ip6Addr), pLladdr->u1Status,
                      pLladdr->u2DadSent);

        TmrGetRemainingTime (gIp6GblInfo.Ip6TimerListId,
                             &pLladdr->dadTimer.appTimer, (UINT4 *) &i4DadTime);
        if (i4DadTime > 0)
        {
            IP6_TRC_ARG1 (u4ContextId, IP6_MOD_TRC,
                          DATA_PATH_TRC, IP6_NAME,
                          "DAD Timer running, value = %d\n", i4DadTime);
        }
        else
        {
            IP6_TRC_ARG (u4ContextId, IP6_MOD_TRC,
                         MGMT_TRC, IP6_NAME, "DAD Timer not running\n\n");
        }
    }

}

/******************************************************************************
 * DESCRIPTION : Prints the Address of the Given Interface
 *
 * INPUTS      : Interface Index (u4Index) 
 *
 * OUTPUTS     : None
 *
 * RETURNS     : VOID
 *
 * NOTES       :
 ******************************************************************************/
VOID
Ip6ifPrintAddrs (UINT4 u4Index)
{
    tIp6If             *pIf6 = NULL;
    tTMO_SLL_NODE      *pSnode = NULL;
    tIp6AddrInfo       *pAddr = NULL;
    UINT4               u4Total = 0;
    UINT4               u4ContextId;
    INT4                i4DadTime = 0;

    pIf6 = Ip6ifGetEntry (u4Index);
    if (pIf6 == NULL)
    {
        IP6_GBL_TRC_ARG1 (IP6_MOD_TRC, MGMT_TRC, IP6_NAME,
                          "Index %d is not valid for IP6 IF!!\n", u4Index);
        return;
    }

    u4ContextId = pIf6->pIp6Cxt->u4ContextId;

    IP6_TRC_ARG1 (u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                  "\nIPv6 Interface Addresses\n IP6 IF = %d\n\n",
                  pIf6->u4Index);

    if (pIf6->u1AdminStatus == ADMIN_INVALID)
    {
        IP6_TRC_ARG (u4ContextId, IP6_MOD_TRC, MGMT_TRC, IP6_NAME,
                     "The IPv6 Interface is NOT YET allocated!!\n\n");
        return;
    }

    u4Total = TMO_SLL_Count (&pIf6->addr6Ilist);
    if (!u4Total)
    {
        IP6_TRC_ARG (u4ContextId, IP6_MOD_TRC, MGMT_TRC, IP6_NAME,
                     "No address on this interface\n\n");
        return;
    }

    IP6_TRC_ARG1 (u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                  "Number of address on this interface = %d\n\n", u4Total);

    TMO_SLL_Scan (&pIf6->addr6Ilist, pSnode, tTMO_SLL_NODE *)
    {
        pAddr = IP6_ADDR_PTR_FROM_SLL (pSnode);
        IP6_TRC_ARG5 (u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                      "Address =  %s\n Prefix len = %d Type = %d Status = %d Profile = %d\n",
                      Ip6PrintAddr (&pAddr->ip6Addr), pAddr->u1PrefLen,
                      pAddr->u1AddrType, pAddr->u1Status,
                      pAddr->u2Addr6Profile);

        IP6_TRC_ARG1 (u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                      "Number of DADs sent = %d ", pAddr->u2DadSent);
        TmrGetRemainingTime (gIp6GblInfo.Ip6TimerListId,
                             &pAddr->dadTimer.appTimer, (UINT4 *) &i4DadTime);
        if (i4DadTime > 0)
        {
            IP6_TRC_ARG1 (u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                          "DAD Timer running, value = %d\n", i4DadTime);
        }
        else
        {
            IP6_TRC_ARG (u4ContextId, IP6_MOD_TRC, MGMT_TRC, IP6_NAME,
                         "DAD Timer not running\n\n");
        }
    }

}

/*
 * IP6 ADDR submodule
 */

/*
 * Prints the IPv6 address in the standard format
 */

/******************************************************************************
 * DESCRIPTION : Prints Address Statistics.
 *
 * INPUTS      : Interface Index (u4Index), Pointer to the IP6 Addr (pAddr)
 *               Prefix Length (u1Prefixlen)
 *
 * OUTPUTS     : None
 *
 * RETURNS     : VOID
 *
 * NOTES       :
 ******************************************************************************/
VOID
Ip6AddrStats (UINT4 u4Index, tIp6Addr * pAddr, UINT1 u1Prefixlen)
{
    tTMO_SLL_NODE      *pSll = NULL;
    tIp6If             *pIf6 = NULL;
    UINT4               u4ContextId;

    pIf6 = Ip6ifGetEntry (u4Index);
    if (pIf6 == NULL)
    {
        IP6_GBL_TRC_ARG1 (IP6_MOD_TRC, MGMT_TRC, IP6_NAME,
                          "Index %d is not valid for IP6 IF!!\n", u4Index);
        return;
    }

    u4ContextId = pIf6->pIp6Cxt->u4ContextId;

    if (!(pAddr))
    {
        IP6_TRC_ARG (u4ContextId, IP6_MOD_TRC, MGMT_TRC,
                     IP6_NAME, "Address is NULL\n");
        return;
    }

    if (pIf6->u1AdminStatus == ADMIN_INVALID)
    {
        IP6_TRC_ARG (u4ContextId, IP6_MOD_TRC, MGMT_TRC, IP6_NAME,
                     "The IPv6 Interface is NOT YET allocated!!\n\n");
        return;
    }

    if (IS_ADDR_LLOCAL (*pAddr))
    {
        tIp6LlocalInfo     *pCurrAddr = NULL;

        TMO_SLL_Scan (&pIf6->lla6Ilist, pSll, tTMO_SLL_NODE *)
        {
            pCurrAddr = IP6_LLADDR_PTR_FROM_SLL (pSll);

            if (MEMCMP (pAddr, &pCurrAddr->ip6Addr, sizeof (tIp6Addr)) == 0)
            {
                break;
            }
        }

        if (!(pCurrAddr))
        {
            IP6_TRC_ARG (u4ContextId, IP6_MOD_TRC, MGMT_TRC, IP6_NAME,
                         "No matching address found \n");
            return;
        }

        IP6_TRC_ARG4 (u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                      "\nIPv6 Addrstatstcs Addr= %s StatOfAddr %d IFwhereLearnt= %d noOfDADSent %d",
                      Ip6PrintAddr (&pCurrAddr->ip6Addr),
                      &pCurrAddr->u1Status, pCurrAddr->pIf6->u4Index,
                      pCurrAddr->u2DadSent);
    }
    else
    {
        tIp6AddrInfo       *pCurrAddr = NULL;

        TMO_SLL_Scan (&pIf6->addr6Ilist, pSll, tTMO_SLL_NODE *)
        {
            pCurrAddr = IP6_ADDR_PTR_FROM_SLL (pSll);

            if (Ip6AddrMatch (&(pCurrAddr->ip6Addr), pAddr, u1Prefixlen))
            {
                break;
            }
        }

        if (!(pCurrAddr))
        {
            IP6_TRC_ARG (u4ContextId, IP6_MOD_TRC, MGMT_TRC, IP6_NAME,
                         "No matching address found \n");
            return;
        }

        IP6_TRC_ARG4 (u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                      "\nIPv6 Addrstics Addr=%s\n Status of addr %d type of addr %d addr profile %d",
                      Ip6PrintAddr (&pCurrAddr->ip6Addr),
                      pCurrAddr->u1Status, pCurrAddr->u1AddrType,
                      pCurrAddr->u2Addr6Profile);

        IP6_TRC_ARG2 (u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                      "If on which learnt%d no ofDADsent:%d \n",
                      pCurrAddr->pIf6->u4Index, pCurrAddr->u2DadSent);

    }

    return;
}

/*
 * IP6 FRAG Submodule
 */

/******************************************************************************
 * DESCRIPTION : Prints Information from the Fragmentation Table.
 *
 * INPUTS      :  None
 *
 * OUTPUTS     : None
 *
 * RETURNS     : VOID
 *
 * NOTES       :
 ******************************************************************************/
VOID
Ip6fragPrintTab (tIp6Cxt * pIp6Cxt)
{
    tIp6FragQueEntry   *pFrag = NULL;
    UINT4               u4Count = 0;
    UINT4               u4Total = 0;

    IP6_TRC_ARG (pIp6Cxt->u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                 "\nIPv6 Reassembly Queue Information\n");

    for (u4Count = 0; u4Count < MAX_IP6_REASM_ENTRIES_LIMIT; u4Count++)
    {
        if (pIp6Cxt->apIp6Ream[u4Count] != NULL)
        {
            if (pIp6Cxt->apIp6Ream[u4Count]->u1State != ENTRY_UNUSED)
            {
                u4Total++;

                IP6_TRC_ARG6 (pIp6Cxt->u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC,
                              IP6_NAME,
                              "Entry #%d\nSrcAddr=%s DstAddr=%s Id=%d Curlength=%d Totlength=%d\n",
                              u4Count,
                              Ip6PrintAddr (&pIp6Cxt->apIp6Ream[u4Count]->
                                            ip6Src),
                              Ip6PrintAddr (&pIp6Cxt->apIp6Ream[u4Count]->
                                            ip6Dst),
                              pIp6Cxt->apIp6Ream[u4Count]->u4Id,
                              pIp6Cxt->apIp6Ream[u4Count]->u4CurLen,
                              pIp6Cxt->apIp6Ream[u4Count]->u4PktLen);

                IP6_TRC_ARG (pIp6Cxt->u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC,
                             IP6_NAME, "Nodes in fragment queue ::\n");

                TMO_DLL_Scan (&(pIp6Cxt->apIp6Ream[u4Count]->fragQue),
                              pFrag, tIp6FragQueEntry *)
                {
                    IP6_TRC_ARG2 (pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                                  DATA_PATH_TRC, IP6_NAME,
                                  "Start offset = %d  End offset = %d\n",
                                  pFrag->u2StartOff, pFrag->u2EndOff);
                }
            }
        }
    }

    if (u4Total == 0)
    {
        IP6_TRC_ARG (pIp6Cxt->u4ContextId, IP6_MOD_TRC, MGMT_TRC, IP6_NAME,
                     " No entries found in reassembly queue\n");
    }

}

/*
 * ICMP6 Module
 */

/******************************************************************************
 * DESCRIPTION : Prints Icmp Statistics
 *
 * INPUTS      : None
 *
 * OUTPUTS     : None
 *
 * RETURNS     : VOID
 *
 * NOTES       :
 ******************************************************************************/
VOID
Icmp6PrintStats (tIp6Cxt * pIp6Cxt)
{

    /* To handle warnings in case of Trace is not defined */
    UNUSED_PARAM (pIp6Cxt);
    IP6_TRC_ARG2 (pIp6Cxt->u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                  "\nIPv6 ICMP statistics\n \nIncoming msgs%d Incoming err pkts%d",
                  pIp6Cxt->Icmp6Stats.u4InMsgs, pIp6Cxt->Icmp6Stats.u4InErrs);

    IP6_TRC_ARG3 (pIp6Cxt->u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                  "Incomng pktswithbadtype %d Incomngtoobigmsgs %d Incomingdest unreachablemsg %d",
                  pIp6Cxt->Icmp6Stats.u4InBadcode,
                  pIp6Cxt->Icmp6Stats.u4InToobig,
                  pIp6Cxt->Icmp6Stats.u4InDstUnreach);

    IP6_TRC_ARG2 (pIp6Cxt->u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                  "Incoming packet time exceeded msgs %d \nIncoming pkt parameter prob msgs %d",
                  pIp6Cxt->Icmp6Stats.u4InTmexceeded,
                  pIp6Cxt->Icmp6Stats.u4InParamprob);
}

/* 
 * ND6 Module
 */

/*
 *  This routine prints the information on NDCache Table
 */

/******************************************************************************
 * DESCRIPTION : Prints ND6 Cache Information
 *
 * INPUTS      : None
 *
 * OUTPUTS     : None
 *
 * RETURNS     : VOID
 *
 * NOTES       :
 ******************************************************************************/
VOID
Nd6PrintCacheTab (VOID)
{
    tNd6CacheEntry     *pNd6cEntry = NULL;
    tNd6CacheEntry     *pNd6cNextEntry = NULL;
    UINT4               u4Total = 0;

    IP6_GBL_TRC_ARG (IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME, "\nNDCache Table\n");

    pNd6cEntry = RBTreeGetFirst (gNd6GblInfo.Nd6CacheTable);

    while (pNd6cEntry != NULL)
    {
        pNd6cNextEntry = RBTreeGetNext (gNd6GblInfo.Nd6CacheTable,
                                        pNd6cEntry, NULL);
        u4Total++;
        Nd6PrintCacheEntry (pNd6cEntry);
        pNd6cEntry = pNd6cNextEntry;
    }

    if (u4Total == 0)
    {
        IP6_GBL_TRC_ARG (IP6_MOD_TRC, MGMT_TRC, IP6_NAME,
                         "No entries found in NDCache Table\n");
    }

}

/*
 *  This routine prints the information of a NDCache entry
 */

/******************************************************************************
 * DESCRIPTION : Prints Information for a Particular ND cache Entry
 *
 * INPUTS      : ND Cache Entry (pNd6cEntry)
 *
 * OUTPUTS     : None
 *
 * RETURNS     : VOID
 *
 * NOTES       :
 ******************************************************************************/
VOID
Nd6PrintCacheEntry (tNd6CacheEntry * pNd6cEntry)
{
    tNd6CacheLanInfo   *pNd6cLinfo = NULL;
    tIp6Cxt            *pIp6Cxt = NULL;

    if (pNd6cEntry == NULL)
    {
        return;
    }
    pIp6Cxt = pNd6cEntry->pIf6->pIp6Cxt;

    IP6_TRC_ARG1 (pIp6Cxt->u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC,
                  IP6_NAME, "NDCache Entry %p\n", pNd6cEntry);

    IP6_TRC_ARG1 (pIp6Cxt->u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                  "IPv6 Address = %s\n", Ip6PrintAddr (&pNd6cEntry->addr6));
    IP6_TRC_ARG3 (pIp6Cxt->u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                  "IF = %d Reach State = %d Last Use Time = %d\n",
                  pNd6cEntry->pIf6->u4Index, pNd6cEntry->u1ReachState,
                  pNd6cEntry->u4LastUseTime);

    if ((pNd6cEntry->pIf6->u1IfType == IP6_ENET_INTERFACE_TYPE) ||
#ifdef WGS_WANTED
        (pNd6cEntry->pIf6->u1IfType == IP6_L2VLAN_INTERFACE_TYPE) ||
#endif
        (pNd6cEntry->pIf6->u1IfType == IP6_PSEUDO_WIRE_INTERFACE_TYPE) ||
        (pNd6cEntry->pIf6->u1IfType == IP6_L3VLAN_INTERFACE_TYPE) ||
        (pNd6cEntry->pIf6->u1IfType == IP6_LAGG_INTERFACE_TYPE) ||
        (pNd6cEntry->pIf6->u1IfType == IP6_L3SUB_INTF_TYPE))
    {
        pNd6cLinfo = (tNd6CacheLanInfo *) (VOID *) pNd6cEntry->pNd6cInfo;
        IP6_TRC_ARG1 (pIp6Cxt->u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC,
                      IP6_NAME, "LLA Address = %s\n",
                      Ip6PrintIftok (pNd6cLinfo->lladdr, pNd6cLinfo->u1LlaLen));
        IP6_TRC_ARG2 (pIp6Cxt->u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC,
                      IP6_NAME, "Max Retries = %d No of Queued Pkts = %d\n",
                      pNd6cLinfo->u4Retries,
                      ND6_PEND_QUE_CNT_FROM_LINFO (pNd6cLinfo));
    }

}

/*
 * This routine prints the information on timers running
 * on NDCache entries and RA timers on interface
 */

/******************************************************************************
 * DESCRIPTION : Prints Timer Information
 *
 * INPUTS      : None
 *
 * OUTPUTS     : None
 *
 * RETURNS     : VOID
 *
 * NOTES       :
 ******************************************************************************/
VOID
Nd6PrintTimers (VOID)
{
    INT4                i4TimerVal = 0;
    tNd6CacheEntry     *pNd6cEntry = NULL;
    tNd6CacheEntry     *pNd6cNextEntry = NULL;
    tNd6CacheLanInfo   *pNd6cLinfo = NULL;

    IP6_GBL_TRC_ARG (IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                     "\nND CACHE TIMERS\n");

    pNd6cEntry = RBTreeGetFirst (gNd6GblInfo.Nd6CacheTable);

    while (pNd6cEntry != NULL)
    {
        pNd6cNextEntry = RBTreeGetNext (gNd6GblInfo.Nd6CacheTable,
                                        pNd6cEntry, NULL);

        if (pNd6cEntry->pNd6cInfo)
        {
            pNd6cLinfo = (tNd6CacheLanInfo *) (VOID *) pNd6cEntry->pNd6cInfo;
            if (TmrGetRemainingTime (gIp6GblInfo.Ip6TimerListId,
                                     &(pNd6cLinfo->timer.appTimer),
                                     (UINT4 *) &i4TimerVal) != TMR_FAILURE)
            {
                IP6_GBL_TRC_ARG1 (IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                                  "NDCache entry %p ", pNd6cEntry);
                IP6_GBL_TRC_ARG2 (IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                                  "Timer Id %d Val %d\n",
                                  pNd6cLinfo->timer.u1Id, i4TimerVal);
            }
        }

        pNd6cEntry = pNd6cNextEntry;
    }

    Nd6PrintRaRsTimer ();

}

/*
 * This routine prints the active ND RA timers
 */

/******************************************************************************
 * DESCRIPTION : Prints RA Timer Information
 *
 * INPUTS      : None
 *
 * OUTPUTS     : None
 *
 * RETURNS     : VOID
 *
 * NOTES       :
 ******************************************************************************/
PRIVATE VOID
Nd6PrintRaRsTimer (VOID)
{
    UINT4               u4Index = 0;
    INT4                i4TimerVal = 0;
    tIp6If             *pIf6 = NULL;

    IP6_GBL_TRC_ARG (IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                     "\nND INTERFACE TIMERS\n");
    u4Index = 1;

    IP6_IF_SCAN (u4Index, (UINT4) IP6_MAX_LOGICAL_IF_INDEX)
    {
        if (((pIf6 = gIp6GblInfo.apIp6If[u4Index]) == NULL) ||
            (pIf6->u1AdminStatus == ADMIN_INVALID))
        {
            continue;
        }

        IP6_GBL_TRC_ARG1 (IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                          "RA Timer on IF %d ", pIf6->u4Index);
        if (TmrGetRemainingTime
            (gIp6GblInfo.Ip6TimerListId,
             (IP6_IF_RARS_TIMER (pIf6).appTimer),
             (UINT4 *) &i4TimerVal) != TMR_FAILURE)
        {
            IP6_GBL_TRC_ARG1 (IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                              "Running : Timer Val %d\n", i4TimerVal);
        }
        else
        {
            /* To handle warnings in case of Trace is not defined */
            UNUSED_PARAM (i4TimerVal);
            IP6_GBL_TRC_ARG (IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                             "Not Running\n");
        }
    }
}

/*
 * PING6 Module
 */

/******************************************************************************
 * DESCRIPTION : Prints Information from Entire PING Table
 *
 * INPUTS      : None
 *
 * OUTPUTS     : None
 *
 * RETURNS     : VOID
 *
 * NOTES       :
 ******************************************************************************/
VOID
Ping6PrintTab (VOID)
{
    UINT2               u2Index = 0;
    UINT4               u4Total = 0;

    IP6_GBL_TRC_ARG (IP6_MOD_TRC, MGMT_TRC, IP6_NAME, "\nIPv6 Ping Table\n");

    for (u2Index = 0; u2Index < gu4MaxPing6Dst; u2Index++)
    {
        if (pPing[u2Index] != NULL)
        {
            u4Total++;
            Ping6PrintRecord (u2Index);
        }
    }

    if (u4Total == 0)
    {
        IP6_GBL_TRC_ARG (IP6_MOD_TRC, MGMT_TRC, IP6_NAME,
                         "No entries found in Ping Table\n");
    }

}

/******************************************************************************
 * DESCRIPTION : Prints Ping record for a Particular Ping Index.
 *
 * INPUTS      : Index to Ping Table (u2Index)
 *
 * OUTPUTS     : None
 *
 * RETURNS     : VOID
 *
 * NOTES       :
 ******************************************************************************/
VOID
Ping6PrintRecord (UINT2 u2Index)
{
    IP6_GBL_TRC_ARG1 (IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                      "\nPing Entry #%d\n\n", u2Index);
    IP6_GBL_TRC_ARG1 (IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                      "Ping destination = %s\n",
                      Ip6PrintAddr (&pPing[u2Index]->ping6Dst));
    IP6_GBL_TRC_ARG3 (IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                      "Admin = %d Ping Interval = %d Rcv Timeout = %d\n",
                      pPing[u2Index]->u1Admin, pPing[u2Index]->u2Ping6Interval,
                      pPing[u2Index]->u2Ping6RcvTimeout);

    IP6_GBL_TRC_ARG3 (IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                      "Max tries = %d  Ping Size = %d Oper status = %d\n\n",
                      pPing[u2Index]->u2Ping6MaxTries,
                      pPing[u2Index]->u2Ping6Size, pPing[u2Index]->u1Oper);

    IP6_GBL_TRC_ARG2 (IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                      "Sent count = %d Successes = %d\n",
                      pPing[u2Index]->u2SentCount, pPing[u2Index]->u2Successes);

    if (pPing[u2Index]->u2SentCount != 0)
    {
        IP6_GBL_TRC_ARG1 (IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                          "Percentage loss = %d\n",
                          Ping6PercentageLossGet (u2Index));
    }

    IP6_GBL_TRC_ARG3 (IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                      "Round trip min/avg/max = %d/%d/%d\n",
                      pPing[u2Index]->u2MinTime,
                      pPing[u2Index]->u2AverageTime, pPing[u2Index]->u2MaxTime);

}

/*
 * Private routines
 */

/***************************** END OF FILE **********************************/
