/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: ip6if.h,v 1.13 2017/12/26 13:34:21 siva Exp $
*
* Description: <File description>
*
*******************************************************************/
#ifndef _IP6IF_H
#define _IP6IF_H

/*
 *----------------------------------------------------------------------------- 
 *    FILE NAME                    :    ip6if.h
 *
 *    PRINCIPAL AUTHOR             :    Vivek Venkatraman
 *
 *    SUBSYSTEM NAME               :    IPv6
 *
 *    MODULE NAME                  :    IP6 IF Submodule
 *
 *    LANGUAGE                     :    C
 *
 *    TARGET ENVIRONMENT           :    UNIX
 *
 *    DATE OF FIRST RELEASE        :    DDD-MM-1996
 *
 *    DESCRIPTION                  :    This file contains the typedefs
 *                                      for the IPv6 Physical and Logical
 *                                      interfaces and related constants, 
 *                                      macros and function prototypes.
 *
 *----------------------------------------------------------------------------- 
 */

/*
 * The following strcuture defines IP6 interface statistics. The IP6
 * interface definition follows after this.
 */

typedef struct _IP6_TUNL_ACCESS_LIST
{
   tTMO_SLL_NODE *pNext; /* Pointer to the next node.*/
   UINT4  u4Ipv4SrcAddr; /* Source ipv4 from which tunl packets can be
                          * accepted.
                          */
   UINT4  u4Index;       /* Index to the Tunl table */
}
tIp6TunlAccessListNode;

#ifdef  TUNNEL_WANTED
typedef struct _IP6_TUNNEL_INTERFACE_STATISTICS
{
    /* ND Recv stats */

    UINT4  u4TunlInPkts;      /* total no. of tunl pkts rcvd via tunnels */
    UINT4  u4TunlOutPkts;     /* total no. of sent out pkts via tunnels */
    UINT4  u4TunlInErrPkts;  /* total no. of error pkts rcvd via tunnels */

}
tIp6TunlStats;

/*
 * Constants and Macros
 */

#define IP6_UNIDIRECTIONAL_TUNL     1
#define IP6_BIDIRECTIONAL_TUNL      2
#define IP6_CT_INCOMING             1
#define IP6_CT_OUTGOING             2
#define IP6_CT_NO_DIR               0
#define LOOPBACK_ADDRESS            0x7f000000 /* 127.0.0.0 */
#define IP6_TUNL_FWD_ENABLE         1
#define IP6_TUNL_FWD_DISABLE        2
#define IP6_MAX_TUNL_ACCESS_LIST    15

#define IP6_ACCESS_LST_PTR_FROM_SLL(sll)  \
        (tIp6TunlAccessListNode *)sll

#define  IP6_TUNL_HDR_LEN      20        /* Ip hdr len */
#define  AUTO_TUNL_ENABLE      1                      
#define  AUTO_TUNL_DISABLE     2                      
#define  IP6_TUNNEL_TYPE       i4AutomaticTunlFlag 

#define  IP6_TUNLIF_SRC(pIf6)  (pIf6->pTunlIf->tunlSrc).u4_addr[3]
#define  IP6_TUNLIF_DST(pIf6)  (pIf6->pTunlIf->tunlDst).u4_addr[3]
#define  IP6_TUNLIF_MTU(pIf6)  pIf6->u4Mtu 
#define  AT_RTENTRY_PFXLEN      96 
#define  IP6_ISATAP_PREFIX_LEN  64
#define  IP6_ISATAP_ID_LEN      32
#define  IP6_ISATAP_GLOBAL_ID   0x02005efe
#define  IP6_ISATAP_PRIVATE_ID  0x00005efe

#endif    /* TUNNEL_WANTED */

/*
 * Constants and Macros
 */

#define  IP6_INVALID_IF_INDEX  0xffff                 

#define  IP6_IF_CREATE              0x0001                
#define  IP6_IF_DELETE              0x0002                
#define  IP6_IF_UP                  0x0004                
#define  IP6_IF_DOWN                0x0008                
#define  IP6_IF_UNMAP               0x0010                

#define  MAC_ADDR_LEN               6                     


/* 
 * Macros for SeND Statistics 
 * Index for the Stats array
 */
#define  COUNT_NS  1
#define  COUNT_NA 2
#define  COUNT_RS 3
#define  COUNT_RA 4 
#define  COUNT_RD 5
#define  COUNT_CPS 6 
#define  COUNT_CPA 7

#define  IP6_INTERFACE(index)       gIp6GblInfo.apIp6If[index]         

#define  IP6_TUNL_INTERFACE(index)  ip6TunlIf[index]    

#define  IP6IF_TOKENLESS(if)        (if->u1TokLen == 0) 

#define  IP6_IF_TYPE(if)            if->u1IfType        

#define IP6_LLADDR_PTR_FROM_SLL(sll)  \
        (tIp6LlocalInfo *) (VOID *)\
        ((UINT1 *)(sll) - IP6_OFFSET(tIp6LlocalInfo, nextLif))

#define IP6_ADDR_PTR_FROM_SLL(sll)  \
        (tIp6AddrInfo *) (VOID *)\
        ((UINT1 *)(sll) - IP6_OFFSET(tIp6AddrInfo, nextAif))
#ifdef HA_WANTED
#define IP6_MN_HOMEADDR_PTR_FROM_SLL(sll)  \
        (tIp6AddrInfo *)\
        ((UINT1 *)(sll) - IP6_OFFSET(tIp6AddrInfo, nextMnHaif))

#endif

#define IP6_IF_PTR_FROM_HASH(h)  \
        (tIp6If *)(VOID *)((UINT1 *)(h) - IP6_OFFSET(tIp6If, ifHash))

#define IP6_IF_PTR_FROM_TIMER(tmr)  \
        (tIp6If *)(VOID *)((UINT1 *)(tmr) - IP6_OFFSET(tIp6If, Timer))

#define IP6_IF_PTR_FROM_PROXY_TIMER(tmr)  \
        (tIp6If *)(VOID *)((UINT1 *)(tmr) - IP6_OFFSET(tIp6If, proxyLoopTimer))

#ifdef MIP6_WANTED
#define IP6_IF_SEND_HBIT(if) (if->u1RaCnf & IP6_IF_H_BIT_ADV)
#endif
 
#define IP6_IF_SEND_PA(if)  (if->u1PrefAdv == IP6_IF_PREFIX_ADV)

#define IP6_IF_ADMINSTATUS(if)  if->u1AdminStatus

#define IP6_IF_HOPLMT(if)  if->u1Hoplmt

#define IP6_IF_DEFTIME(if) if->u2Deftime

#define IP6_IF_PDELAYTIME(if)  if->u4Pdelaytime

#define IP6_IF_MAX_RA_TIME(if)  if->u4MaxRaTime

#define IP6_IF_MIN_RA_TIME(if)  if->u4MinRaTime

#define IP6_IF_DAD_SEND(if)  if->u2DadSend

#define IP6_IF_RARS_TIMER(if)  &if->Timer

#define IP6_IF_STATS(if)  &if->stats

#define IP6_IF_RA_SCHED_TIME(if)  if->stats.u4RaSchedTime

#define IP6_IF_RA_SENT_TIME(if)  if->stats.u4RaSentTime

#define IP6_IF_RA_INITIAL_CNT(if)  if->stats.u4RaInitialCnt

#define IP6_SEND_IF_IN_STATS(if, index) if->pIfIcmp6Stats->Nd6SecInStats[index] 

#define IP6_SEND_IF_OUT_STATS(if, index) if->pIfIcmp6Stats->Nd6SecOutStats[index]

#define IP6_SEND_IF_DROP_STATS(if, index) if->pIfIcmp6Stats->Nd6SecDropStats[index]

#define IP6_FIRST_VALID_IF_IN_CXT(ind, pCxt) \
{\
    UINT4 i = 1;\
    IP6_IF_SCAN (i, (UINT4)IP6_MAX_LOGICAL_IF_INDEX)\
    {\
        if ((gIp6GblInfo.apIp6If[i] != NULL) && \
            (gIp6GblInfo.apIp6If[i]->pIp6Cxt == pCxt) &&\
            (gIp6GblInfo.apIp6If[i]->u1AdminStatus == ADMIN_UP) &&\
            (gIp6GblInfo.apIp6If[i]->u1OperStatus == OPER_UP))\
        {\
            *ind = i;\
            break;\
        }\
    }\
}

#define IP6_IF_SCAN(u4StartIfIndex, u4EndIfIndex) \
        for (; u4StartIfIndex <= u4EndIfIndex; \
               u4StartIfIndex = \
               IP6_IF_NEXT_VISIBLE_IF (u4StartIfIndex))

#endif /* !_IP6IF_H */
