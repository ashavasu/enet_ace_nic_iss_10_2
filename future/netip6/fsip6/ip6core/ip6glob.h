/*******************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ip6glob.h,v 1.21 2017/12/26 13:34:21 siva Exp $
 *
 * Description: Cli related functions are handled here.
 *
 *******************************************************************/
#ifndef _IP6GLOB_H
#define _IP6GLOB_H

/* This structure contains the global information required for 
 * IP6 module */
typedef struct 
{
    tTimerListId        Ip6TimerListId;
    tIp6Cxt            *apIp6Cxt[MAX_IP6_CONTEXT_LIMIT];
    tIp6Cxt            *pIp6CurrCxt;
    tTMO_HASH_TABLE    *pIf6Htab;           /* Hash table for the
                                              IPv6 logical interfaces */
#ifdef TUNNEL_WANTED
    tTMO_SLL            AccessLlist;       /* Incase Tunl interface is 
                                              unidirectional and incoming,
                                              this list defines the access list.
                                              In all other cases it takes the 
                                              value NULL */
    INT4                i4Ip6TunlAccessListPoolId;
#endif /* TUNNEL_WANTED */
    tIp6RAEntry        *apIp6RaTbl[IP6_MAX_LOGICAL_IF_INDEX + IP6_ONE]; 
                                            /* RA Table */
    tIp6AddrProfile    *apIp6AddrProfile[MAX_IP6_ADDR_PROFILES_LIMIT]; 
    tIp6If             *apIp6If[IP6_MAX_LOGICAL_IF_INDEX + IP6_ONE]; 
                                          /* Array of pointer for the IPv6
                                           * logical interfaces. It ranges from
                                           * 1 to IP6_MAX_LOGICAL_IF_INDEX. 0 is
                                           * considered as invalid index */
    tIp6MacFilter       aIp6MACFilter[IP6_MAX_FILTER];
    tIp6ScopeName       asIp6ScopeName[IP6_MAX_SCOPE_ZONE_TYPES]; /* The Scope name added for RFC4007*/ 
    tTMO_SLL            Udp6CbEntry;      /* Global UDP6 CB entries */
    tIp6Udp6Stats       Udp6Stats;
    tOsixTaskId         Ip6TaskId;/* Redundancy support provided */
    tOsixQId            Ip6RmQId; /* Redundancy support provided */
    tTMO_SLL            ScopeZoneList;
    tRBTree             Nd6RedTable;

    /* PMTU */
    tRBTree             PmtuRBTable; 
    tMemPoolId          MemPoolId;
    tIp6Timer           PmtuTimer;
    tRBTree             UnicastAddrTree;
    tRBTree             AnycastAddrTree;
    tRBTree             ScopeZoneTree; /*RB Tree node added for scope-zones RFC4007 */
    tRBTree             PolicyPrefixTree; 
    UINT4               u4MaxContextLimit; 
                                /* Current Value of Maximum allowed IP6 context 
                                 * This is minimum of MAX_IP6_CONTEXT_LIMIT and 
                                 * FsIP6SizingParams[MAX_IP6_CONTEXT_SIZING_ID].u4PreAllocatedUnits */
    UINT4               u4MaxIp6IfLimit; 
                                /* Current Value of Maximum allowed IP6 interfaces (includes tunnel interfaces)
                                 * This is minimum of MAX_IP6_INTERFACES_LIMIT and 
                                 * FsIP6SizingParams[MAX_IP6_INTERFACES_SIZING_ID].u4PreAllocatedUnits */
    UINT4               u4MaxAddrProfileLimit;
                                /* Current Value of Maximum allowed address profile entries
                                 * This is minimum of MAX_IP6_ADDR_PROFILES_LIMIT and
                                 * FsIP6SizingParams[MAX_IP6_ADDR_PROFILES_SIZING_ID].u4PreAllocatedUnits */
    UINT4               u4GlbIp6Dbg;
    UINT4               u4NextProfileIndex;
    UINT4               u4MaxAssignedProfileIndex;
    UINT4               u4Ip6IfTableLastChange;
    UINT4               u4MaxPmtuEntries;
    INT4                i4Ip6Status;
    INT4                i4Ip6RaTblPoolId;
    INT4                i4ParamId;
    INT4                i4Ip6CxtPoolId;
    INT4                i4Udp6CBPoolId;
    INT4                i4Ip6FragId;
    INT4                i4Ip6ReasmPoolId;
    INT4                i4Ip6unicastId;
    INT4                i4Ip6llocalId;
    INT4                i4MacFilterId;
    INT4                i4Ip6AddrProfPoolId;
    INT4                i4Ip6mcastId;
    INT4                i4Ip6PrefListId;
    INT4                i4Ip6PolicyPrefixListId;
    INT4                i4Ip6SrcAddrListId;
    INT4                i4Ip6DstAddrListId;
    INT4                i4Ip6IfPoolId;
    INT4                i4Ip6TunlIfPoolId;
    INT4                i4Icmp6IfPoolId;
    INT4                i4ND6RouteId;    /* Pool Id for ND routes */
    INT4                i4Ip6IfzonePoolId;  /* RFC 4007 Mem pool id added for interface
                                           Scope Zone */
    INT4                i4Ip6ScopeZonePoolId; /*RFC 4007 Mem pool id added for RBTree */
    INT4                i4Ip6BufPoolId;  /* Pool added for unfragmented buffer and liner buffer */
    INT4                i4AnycastDstId;    /* Mempool for anycast destinations*/
    INT4                i4Ip6Nd6PoolId; /* Mempool for Redundancy support */
    INT4                i4Ip6Nd6DynMsgPoolId; /* Mempool for Redundancy support */
    INT4                i4Ip6IsZoneIndexValidPoolId;
    INT4                i4NdCacheTimeout;
    INT4                i4Ip6Nd6NsSrcPoolId;
    INT4                i4Ip6Nd6SecurePoolId; /* Mempool for SeND Linear buffer for message processing*/
    INT4                i4Ip6RARouteInfoPoolId; /* Mempool for RA Route Info - RFC 4191 implementation */
    INT4                i4ECMPPRTTInterval;
    UINT4               u4Ip6MCastPoolId;
    UINT2               u2PmtuGrbgInterval;
    INT1                i1JmbPktFlag;
    UINT1               u1RFC5095Compatibility;
    UINT1               u1RFC5942Compatibility;
    UINT1               au1pad[3];
}tIp6GblInfo;

#define  UDP6_STAT_OUT_INC(u1Mode, u4ContextId)  \
             ((u1Mode == SOCK_UNIQUE_MODE) ?  \
              gIp6GblInfo.apIp6Cxt[u4ContextId]->Udp6Stats.u4OutDgrams++ : \
              gIp6GblInfo.Udp6Stats.u4OutDgrams++)

#define  UDP6_STAT_IN_INC(u1Mode, u4ContextId)  \
             ((u1Mode == SOCK_UNIQUE_MODE) ?  \
              gIp6GblInfo.apIp6Cxt[u4ContextId]->Udp6Stats.u4InDgrams++ : \
              gIp6GblInfo.Udp6Stats.u4InDgrams++)

#define  UDP6_STAT_ERR_INC(u1Mode, u4ContextId)  \
             ((u1Mode == SOCK_UNIQUE_MODE) ?  \
              gIp6GblInfo.apIp6Cxt[u4ContextId]->Udp6Stats.u4InErrs++ : \
              gIp6GblInfo.Udp6Stats.u4InErrs++)

#define  UDP6_STAT_NUM_PORT_INC(u1Mode, u4ContextId)  \
             ((u1Mode == SOCK_UNIQUE_MODE) ?  \
              gIp6GblInfo.apIp6Cxt[u4ContextId]->Udp6Stats.u4NumPorts++ : \
              gIp6GblInfo.Udp6Stats.u4NumPorts++)

#define  UDP6_STAT_NUM_PORT_DEC(u1Mode, u4ContextId)  \
             ((u1Mode == SOCK_UNIQUE_MODE) ?  \
              gIp6GblInfo.apIp6Cxt[u4ContextId]->Udp6Stats.u4NumPorts-- : \
              gIp6GblInfo.Udp6Stats.u4NumPorts--)

#define  UDP6_STAT_HC_IN_INC(u1Mode, u4ContextId)  \
     if (u1Mode == SOCK_UNIQUE_MODE) {\
          FSAP_U8_INC (&( gIp6GblInfo.apIp6Cxt[u4ContextId]->Udp6Stats.u8HcInDgrams)); }\
          else {\
            FSAP_U8_INC (&( gIp6GblInfo.Udp6Stats.u8HcInDgrams));} 

#define  UDP6_STAT_HC_OUT_INC(u1Mode, u4ContextId)  \
     if (u1Mode == SOCK_UNIQUE_MODE) {\
          FSAP_U8_INC (&( gIp6GblInfo.apIp6Cxt[u4ContextId]->Udp6Stats.u8HcOutDgrams)); }\
          else {\
            FSAP_U8_INC (&( gIp6GblInfo.Udp6Stats.u8HcOutDgrams));} 

#endif
