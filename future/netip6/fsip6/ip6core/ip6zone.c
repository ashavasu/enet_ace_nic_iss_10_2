/********************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: ip6zone.c,v 1.16 2015/06/01 12:23:15 siva Exp $
 *
 * Description: This file has all the APIs related to scope zones.
 *              This file is added as a part of RFC4007 code changes
 *              
 *******************************************************************/

#include "ip6inc.h"
#include "ip6cli.h"

PRIVATE INT4 Ip6ZoneTableInit PROTO ((VOID));
PRIVATE VOID Ip6ZoneIndexTableInit PROTO ((VOID));

/*RFC 4007 Scope Address Architecture Code Changes Start */

/*******************************************************************
 * DESCRIPTION :   Initializes the  scope-zones    related tables  *
 *                                                                 *
 * INPUTS      :   None                                            *
 *                                                                 *
 e OUTPUTS     :   None                                            *
 *                                                                 *
 * RETURNS     :  IP6_SUCCESS - On successful initialisation       *
 *                IP6_FAILURE - Non-successful initialisation      *
 *                                                                 *
 * NOTES       :  This function is added as a part of RFC4007      *
 *                implementation                                   *
 *******************************************************************/
INT4
Ip6ZoneInit ()
{
    /* Initialises the zone-index array to an
       invalid value */

    Ip6ZoneIndexTableInit ();

    /* Initialises the scope-name table */
    IP6ScopeNameTableInit ();

    /* Initialises the scope-Zone RBTree */
    if (Ip6ZoneTableInit () == IP6_SUCCESS)
    {
        return IP6_SUCCESS;
    }

    IP6_GBL_TRC_ARG (IP6_MOD_TRC, CONTROL_PLANE_TRC, IP6_NAME,
                     "IP6ZONE: Ip6ZoneInit: Failed!!\n");

    return IP6_FAILURE;
}

/*******************************************************************
 * DESCRIPTION :   Initializes the RBtree for scope-zones          *
 *                                                                 *
 * INPUTS      :   None                                            *
 *                                                                 *
 e OUTPUTS     :   None                                            *
 *                                                                 *
 * RETURNS     :  IP6_SUCCESS - On successful initialisation       *
 *                IP6_FAILURE - Non-successful initialisation      *
 *                                                                 *
 * NOTES       :  This function is added as a part of RFC4007      *
 *                implementation                                   *
 *                                                                 *
 *******************************************************************/
PRIVATE INT4
Ip6ZoneTableInit ()
{
    gIp6GblInfo.ScopeZoneTree =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tIp6ScopeZoneInfo,
                                             zoneRbNode), Ip6ZoneInfoCmp);

    if (gIp6GblInfo.ScopeZoneTree == NULL)
    {
        IP6_GBL_TRC_ARG4 (IP6_MOD_TRC, CONTROL_PLANE_TRC, IP6_NAME,
                          "IP6ZONE: Ip6ZoneTableInit: fail %s mem error"
                          ": Type=%d PoolId=%d Memptr=%p\n",
                          ERROR_FATAL_STR, ERR_MEM_CREATE, 0, 0);

        return IP6_FAILURE;

    }

    return IP6_SUCCESS;
}

/******************************************************************
 * DESCRIPTION :   Initializes the scope-zone index table         *
 *                                                                *
 * INPUTS      :   None                                           *
 *                                                                *
 * OUTPUTS     :   None                                           *
 *                                                                *
 * RETURNS     :   None                                           * 
 *                                                                *
 * NOTES       :  This function is added as a part of RFC4007     *
 *                implementation                                  *
 *                                                                *
 ******************************************************************/

PRIVATE VOID
Ip6ZoneIndexTableInit ()
{
    tIp6IsScopeZoneValid *pIp6IsScopeZoneValid = NULL;
    UINT4               u4Count = 0;

    TMO_SLL_Init (&gIp6GblInfo.ScopeZoneList);

    for (u4Count = 1; u4Count <= IP6_SCOPE_ZONE_COUNT; u4Count++)
    {
        pIp6IsScopeZoneValid = (tIp6IsScopeZoneValid *)
            MemAllocMemBlk (gIp6GblInfo.i4Ip6IsZoneIndexValidPoolId);
        if (pIp6IsScopeZoneValid == NULL)
        {
            return;
        }
        MEMSET (pIp6IsScopeZoneValid, 0, sizeof (tIp6IsScopeZoneValid));
        pIp6IsScopeZoneValid->i4ZoneIndex = u4Count;
        pIp6IsScopeZoneValid->u1Ip6IsZoneIndexValid = IP6_ZONE_INVALID;
        TMO_SLL_Add (&gIp6GblInfo.ScopeZoneList,
                     &pIp6IsScopeZoneValid->NextNode);
    }
    return;

}

/******************************************************************
 * DESCRIPTION :   Initializes the scope-name table               *
 *                                                                *
 * INPUTS      :   None                                           *
 *                                                                *
 * OUTPUTS     :   None                                           *
 *                                                                *
 * RETURNS     :   None                                           *
 *                                                                *
 * NOTES       :  This function is added as a part of RFC4007     *
 *                implementation                                  *
 ******************************************************************/
VOID
IP6ScopeNameTableInit ()
{
    STRCPY (gIp6GblInfo.asIp6ScopeName[0].au1ZoneName, "reserved");
    STRCPY (gIp6GblInfo.asIp6ScopeName[1].au1ZoneName, "interfacelocal");
    STRCPY (gIp6GblInfo.asIp6ScopeName[2].au1ZoneName, "linklocal");
    STRCPY (gIp6GblInfo.asIp6ScopeName[3].au1ZoneName, "subnetlocal");
    STRCPY (gIp6GblInfo.asIp6ScopeName[4].au1ZoneName, "adminlocal");
    STRCPY (gIp6GblInfo.asIp6ScopeName[5].au1ZoneName, "sitelocal");
    STRCPY (gIp6GblInfo.asIp6ScopeName[6].au1ZoneName, "scope6");
    STRCPY (gIp6GblInfo.asIp6ScopeName[7].au1ZoneName, "scope7");
    STRCPY (gIp6GblInfo.asIp6ScopeName[8].au1ZoneName, "orglocal");
    STRCPY (gIp6GblInfo.asIp6ScopeName[9].au1ZoneName, "scope9");
    STRCPY (gIp6GblInfo.asIp6ScopeName[10].au1ZoneName, "scopeA");
    STRCPY (gIp6GblInfo.asIp6ScopeName[11].au1ZoneName, "scopeB");
    STRCPY (gIp6GblInfo.asIp6ScopeName[12].au1ZoneName, "scopeC");
    STRCPY (gIp6GblInfo.asIp6ScopeName[13].au1ZoneName, "scopeD");
    STRCPY (gIp6GblInfo.asIp6ScopeName[14].au1ZoneName, "global");

    return;
}

/************************************************************************
 * DESCRIPTION : This function compares the keys of the ScopeZoneinfo   *
 *               node and return whether the received node is smaller   *
 *               or greater                                             *
 *                                                                      *
 * INPUTS      : The two ScopeZoneinfo nodes                            *
 *               Ip6ScopeZoneinfo                                       *
 *               Ip6ScopeZoneinfoIn                                     *
 *                                                                      *
 * OUTPUTS     : SUCCESS, if the scope passed as arg1 is greater        *
 *               FAILURE, if the scope passed as arg1 is not greater    *
 *                                                                      *
 * RETURNS     : NONE                                                   *
 *                                                                      *
 * NOTES       : This function is added as a part of RFC4007            *
 *               implementation                                         *
 ***********************************************************************/
INT4
Ip6ZoneInfoCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tIp6ScopeZoneInfo  *pIp6ScopeZoneinfo = pRBElem1;
    tIp6ScopeZoneInfo  *pIp6ScopeZoneinfoIn = pRBElem2;
    UINT4               u4ContextId = 0, u4ContextIdIn = 0;
    UINT1               u1Scope = 0, u1ScopeIn = 0;
    INT4                i4ZoneId = 0, i4ZoneIdIn = 0;

    /* Compares the context id of tree elements */
    u4ContextId = pIp6ScopeZoneinfo->u4ContextId;
    u4ContextIdIn = pIp6ScopeZoneinfoIn->u4ContextId;

    if (u4ContextId < u4ContextIdIn)
    {
        return IP6_RB_LESSER;
    }
    else if (u4ContextId > u4ContextIdIn)
    {
        return IP6_RB_GREATER;
    }

    /* Compares the scope of tree elements */
    u1Scope = pIp6ScopeZoneinfo->u1Scope;
    u1ScopeIn = pIp6ScopeZoneinfoIn->u1Scope;

    if (u1Scope < u1ScopeIn)
    {
        return IP6_RB_LESSER;
    }
    else if (u1Scope > u1ScopeIn)
    {
        return IP6_RB_GREATER;
    }

    /* Compares the zone-id of the tree elements */
    i4ZoneId = pIp6ScopeZoneinfo->i4ZoneId;
    i4ZoneIdIn = pIp6ScopeZoneinfoIn->i4ZoneId;

    if (i4ZoneId < i4ZoneIdIn)
    {
        return IP6_RB_LESSER;
    }
    else if (i4ZoneId > i4ZoneIdIn)
    {
        return IP6_RB_GREATER;
    }

    return IP6_RB_EQUAL;
}

/*******************************************************************
 * DESCRIPTION : Checks if the given scope is smaller              *
 *                                                                 *
 * INPUTS      : The two scopes (s1,s2)                            *
 *                                                                 *
 * OUTPUT      : None                                              *
 *                                                                 *
 * RETURNS     : IP6_SUCCESS (if s1 is lesser than s2)             *
 *               IP6_FAILURE (if s1 is greater or equal to s2)     *
 *                                                                 *
 * NOTES       : This functions is added as a part of RFC4007      *
 *               implementation                                    *
 *******************************************************************/
INT4
Ip6ZoneIsScopeSmaller (UINT1 u1Scope1, UINT1 u1Scope2)
{
    if (u1Scope1 < u1Scope2)
    {
        return IP6_SUCCESS;
    }
    else
    {
        return IP6_FAILURE;
    }
}

/*****************************************************************
 * DESCRIPTION : When scope and zone id are given this function  *
 *               gets the scope-zone name                        *
 *                                                               *
 * INPUTS      : scope                                           *
 *               Zone id                                         *
 *                                                               *
 * OUTPUT      : pu1ZoneName - Pointer to the zone name          *
 *                                                               *
 * RETURNS     : NONE                                            *
 *                                                               *
 * NOTES       : This functions is added as a part of RFC4007    *
 *               implementation                                  *
 *****************************************************************/

VOID
Ip6ZoneGetZoneName (UINT1 *pu1ZoneName, UINT1 u1Scope, INT4 i4ZoneId)
{
    if ((u1Scope >= ADDR6_SCOPE_INTLOCAL) && (u1Scope <= ADDR6_SCOPE_GLOBAL))
    {
        SPRINTF ((CHR1 *) pu1ZoneName, "%s%d",
                 gIp6GblInfo.asIp6ScopeName[u1Scope].au1ZoneName, i4ZoneId);
    }
    return;
}

/******************************************************************
 * DESCRIPTION : Returns entry ptr in the scope-zone map table    *
 *               for a particular scope if the scope-zone entry   *
 *               exist else returns NULL.                         *
 *                                                                *    
 * INPUTS      : u4Index - Interface Index,                       *
 *               Scope   - Scope configured on an interface       *
 *                                                                *
 * OUTPUTS     : NONE                                             *
 *                                                                *
 * RETURNS     : Scope Zone Information if present,NULL otherwise *
 *                                                                *
 * NOTES       : This function is added as a part of RFC4007 code *
 *               implementation                                   *
 *                                                                *
 ******************************************************************/

tIp6IfZoneMapInfo  *
Ip6ZoneGetIfScopeZoneEntry (UINT4 u4IfIndex, UINT1 u1Scope)
{
    tTMO_SLL_NODE      *pZoneNode = NULL;
    tIp6IfZoneMapInfo  *pIp6IfZoneMapInfo = NULL;
    tIp6If             *pIf6 = (tIp6If *) IP6_INTERFACE (u4IfIndex);

    if (pIf6 == NULL)
    {
        return NULL;
    }

    IP6_TRC_ARG3 (pIf6->pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                  DATA_PATH_TRC, IP6_NAME,
                  "IP6ScopeZone:scope %d IF %d Context_id %d\n",
                  u1Scope, u4IfIndex, pIf6->pIp6Cxt->u4ContextId);

    TMO_SLL_Scan (&pIf6->zoneIlist, pZoneNode, tTMO_SLL_NODE *)
    {
        pIp6IfZoneMapInfo = IP6_SCOPE_ZONE_PTR_FROM_SLL (pZoneNode);

        if (pIp6IfZoneMapInfo->u1Scope == u1Scope)
        {
            return (pIp6IfZoneMapInfo);
        }
    }

    return (NULL);
}

/**************************************************************************
 *  DESCRIPTION : This function retrieves the scope and zone-id from the  *
 *                zone name                                               *
 *                                                                        *
 *  INPUTS      : pu1ZoneName - Pointer to the zone-name                  *
 *                                                                        *
 *  OUTPUTS     : pu1Scope - Pointer to the scope                         *
 *                pi4ZoneId - Pointer to the zone-id                      *
 *                                                                        *
 *  RETURNS     : IP6_SUCCESS - If the zone name is valid                 *
 *                IP6_FAILURE - If the zone name is invalid               *
 *                                                                        *
 *  NOTES       : This function is added as a part of RFC4007             *
 *                implementation                                          *
 **************************************************************************/

INT4
Ip6ZoneGetZoneIdFromZoneName (UINT1 *pu1ZoneName, UINT1 *pu1Scope,
                              INT4 *pi4ZoneId)
{
    UINT1               u1ScopeNameLen = 0;
    UINT1               u1Scope = 0;
    UINT1              *pu1KeyStr = NULL;

    /* Checks if the name configured matches with any of the scope name */
    for (u1Scope = ADDR6_SCOPE_INTLOCAL; u1Scope <= ADDR6_SCOPE_GLOBAL;
         u1Scope++)
    {
        u1ScopeNameLen =
            STRLEN (gIp6GblInfo.asIp6ScopeName[u1Scope].au1ZoneName);

        if (STRNCMP (pu1ZoneName,
                     gIp6GblInfo.asIp6ScopeName[u1Scope].au1ZoneName,
                     u1ScopeNameLen) == 0)
        {
            /* If the name matches update the scope and zone-id */
            *pu1Scope = u1Scope;
            pu1KeyStr = &pu1ZoneName[u1ScopeNameLen];
            if (0 != *pu1KeyStr)
            {
                *pi4ZoneId = ATOI (pu1KeyStr);
                if (*pi4ZoneId != 0)
                    return IP6_SUCCESS;
            }
            break;
        }
    }
    /* If no match is found for the configured scope-name return
       failure */
    return IP6_FAILURE;
}

/************************************************************************
 * DESCRIPTION : This functions tests if the name and the zone id       *
 *               valid one or not.                                      *    
 *                                                                      *
 * INPUTS      : pu4ErrorCode - pointer to error code,                  *
 *               au1ScopeZone - Pointer to the Scope-Zone name,         *
 *               u4IfIndex- Interface Index,                            *
 *                                                                      *
 * OUTPUTS     : None                                                   *
 *                                                                      *
 * RETURNS     : IP6_SUCCESS (If validation is successful)              *
 *               IP6_FAILURE (If validation fails)                      *
 *                                                                      *
 * NOTES       : This function is added as a part of RFC4007 code       *
 *               changes                                                *
 ************************************************************************/
INT4
Ip6ZoneTestZoneNameandId (UINT4 *pu4ErrorCode, UINT1 *pu1ScopeZone)
{
    INT4                i4ZoneId = 0;
    INT4                i4ZoneNameLen = 0;
    UINT1               u1ZoneScope = 0;

    /*check if the scope-zone name len is not exceeding the max len */
    i4ZoneNameLen = STRLEN (pu1ScopeZone);

    if (i4ZoneNameLen > IP6_SCOPE_ZONE_NAME_LEN)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        CLI_SET_ERR (CLI_IP6_INVALID_ZONE_LEN);
        return IP6_FAILURE;
    }

    if (IP6_FAILURE == Ip6ZoneGetZoneIdFromZoneName (pu1ScopeZone,
                                                     &u1ZoneScope, &i4ZoneId))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_INVALID_ZONE_NAME);
        return IP6_FAILURE;
    }
/*    if (!ISDIGIT(i4ZoneId))
    {
         CLI_SET_ERR (CLI_IP6_INVALID_ZONE_ID);
         return IP6_FAILURE;
    } */
    /*check if the zone-id value is not exceeded beyond the max
       possible zone-id value */

    if ((i4ZoneId <= 0) || (i4ZoneId > IP6_MAX_VALUE_FOR_ZONE_ID))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_INVALID_ZONE_ID);
        return IP6_FAILURE;
    }

    return IP6_SUCCESS;
}

/************************************************************************
 * DESCRIPTION : This functions tests if the scope-zone being set is    *
 *               valid one or not.                                      *    
 *                                                                      *
 * INPUTS      : pu4ErrorCode - pointer to error code,                  *
 *               u4IfIndex- Interface Index,                            *
 *               u1Scope - Scope for which zone is configured,          *    
 *               au1ScopeZone - Pointer to the Scope-Zone name,         *
 *               u1ZoneCreationStaus - Creation status of the zone      *
 *                                                                      *
 * OUTPUTS     : None                                                   *
 *                                                                      *
 * RETURNS     : SNMP_FAILURE (If validation is successful)             *
 *               SNMP_SUCCESS (If validation fails)                     *
 *                                                                      *
 * NOTES       : This function is added as a part of RFC4007 code       *
 *               changes                                                *
************************************************************************/
INT1
Ip6ZoneTestScopeZoneEntry (UINT4 *pu4ErrorCode, UINT4 u4IfIndex, UINT1 u1Scope,
                           UINT1 *pu1ScopeZone, UINT1 u1ZoneCreationStaus)
{
    tIp6IfZoneMapInfo  *pIp6IfZoneMapInfo = NULL;
    tIp6IfZoneMapInfo  *pActiveIp6IfZoneMapInfo = NULL;
    tIp6ScopeZoneInfo  *pIp6ScopeZoneInfo = NULL;
    tIp6ScopeZoneInfo   Ip6ScopeZoneInfo;
    tIp6If             *pIf6 = (tIp6If *) IP6_INTERFACE (u4IfIndex);
    INT4                i4ZoneId = 0;
    INT4                i4ZoneLen = 0;
    UINT4               u4Interface = 0;
    UINT4               u4ActiveInt = 0;
    UINT1               u1Result = 0;
    UINT1               u1ZoneScope = ADDR6_SCOPE_INVALID;

    /*check the existence of the interface */
    if (pIf6 == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP6_INVALID_INTERFACE);
        return SNMP_FAILURE;
    }

    IP6_TRC_ARG3 (pIf6->pIp6Cxt->u4ContextId, IP6_MOD_TRC,
                  DATA_PATH_TRC, IP6_NAME,
                  "IP6ScopeZone:Ip6ZoneTestScopeZoneEntry:"
                  "Zone %s IF %d Zone-Creation Staus %d\n", pu1ScopeZone,
                  u4IfIndex, u1ZoneCreationStaus);
    MEMSET (&Ip6ScopeZoneInfo, IP6_ZERO, sizeof (tIp6ScopeZoneInfo));

    if (IP6_FAILURE == Ip6ZoneTestZoneNameandId (pu4ErrorCode, pu1ScopeZone))
    {
        return SNMP_FAILURE;
    }

    Ip6ZoneGetZoneIdFromZoneName (pu1ScopeZone, &u1ZoneScope, &i4ZoneId);

    if (u1ZoneScope != u1Scope)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_INVALID_ZONE_NAME);
        return SNMP_FAILURE;
    }

    /* Check if max zones are configured by getting the zone index 
       value if the zone index received is INVALID then the max zones 
       are configured return failure */

    if (IP6_ZONE_INVALID == Ip6ZoneCheckFreeIndexAvail ())
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IP6_MAX_ZONES_CONFIGURED);
        return SNMP_FAILURE;
    }

    pIp6IfZoneMapInfo = Ip6ZoneGetIfScopeZoneEntry (u4IfIndex, u1Scope);

    /*Verify if the zone for the same scope exist on this interface. If so
       return failure only for scopes other than linklocal ,in case of
       linklocal the user can over write the already existing default 
       zone on that interface if that interface is an on link interface */
    if ((NULL != pIp6IfZoneMapInfo) &&
        (u1Scope == ADDR6_SCOPE_LLOCAL) &&
        (u1ZoneCreationStaus == IP6_ZONE_TYPE_AUTO))
    {
        /* Get the interface index of the active interface and check
           if the zone name on link is configured for this interface
           if not return failure */
        u4ActiveInt = pIf6->u4OnLinkActiveIfId;
        pActiveIp6IfZoneMapInfo =
            Ip6ZoneGetIfScopeZoneEntry (u4ActiveInt, u1Scope);
        if (pActiveIp6IfZoneMapInfo != NULL)
        {
            i4ZoneLen = STRLEN (pu1ScopeZone);
            if (MEMCMP (pActiveIp6IfZoneMapInfo->au1ZoneName,
                        pu1ScopeZone, i4ZoneLen) != 0)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                CLI_SET_ERR (CLI_IP6_INVALID_ZONE_NAME);
                return SNMP_FAILURE;
            }
        }
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_IP6_INVALID_ZONE_NAME);
        return SNMP_FAILURE;
    }
    else if (NULL != pIp6IfZoneMapInfo)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        CLI_SET_ERR (CLI_IP6_ZONE_ALREADY_EXIST);
        return SNMP_FAILURE;
    }
    /*Verify if the interfaces in the lower scope-zone is having same scope 
       but different zone configuration if so return failure.This rule is not 
       applicable for global and interface local as global comprises of all 
       the other zones under it so this validation is not needed. */
    if ((u1Scope != ADDR6_SCOPE_GLOBAL) && (u1Scope != ADDR6_SCOPE_INTLOCAL))
    {
        if (IP6_FAILURE ==
            Ip6ZoneVerifyIntOtherZoneMembersZoneConfig (u4IfIndex, u1Scope,
                                                        i4ZoneId))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            CLI_SET_ERR (CLI_IP6_VIOLATION_OF_ZONE_INTEGRITY);
            return SNMP_FAILURE;
        }
    }
    /* Check if all the addresses within the linklocal and the global zone 
       are unique or not if they are same return failure */
    if ((u1Scope == ADDR6_SCOPE_GLOBAL) || (u1Scope == ADDR6_SCOPE_LLOCAL))
    {
        pIp6ScopeZoneInfo =
            Ip6ZoneGetRBTreeScopeZoneEntry (pu1ScopeZone,
                                            pIf6->pIp6Cxt->u4ContextId);
        if (NULL != pIp6ScopeZoneInfo)
        {
            for (u4Interface = 1; u4Interface <= IP6_MAX_ZONE_INTERFACE;
                 u4Interface++)
            {
                OSIX_BITLIST_IS_BIT_SET (pIp6ScopeZoneInfo->InterfaceList,
                                         u4Interface,
                                         IP6_MAX_ZONE_INT_LIST_SIZE, u1Result);
                if (u1Result == OSIX_TRUE)
                {
                    if (IP6_FAILURE ==
                        Ip6ZoneCheckAddrMatch (u4IfIndex, u4Interface, u1Scope))
                    {
                        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                        return SNMP_FAILURE;
                    }
                }
            }
        }
    }
    /* Also check if the zone configuration is for global then the zone id 
       is the same for all the zones within a context */
    if (u1Scope == ADDR6_SCOPE_GLOBAL)
    {
        Ip6ScopeZoneInfo.u4ContextId = pIf6->pIp6Cxt->u4ContextId;
        Ip6ScopeZoneInfo.u1Scope = ADDR6_SCOPE_GLOBAL;
        Ip6ScopeZoneInfo.i4ZoneId = 0;

        pIp6ScopeZoneInfo = RBTreeGetNext (gIp6GblInfo.ScopeZoneTree,
                                           (tRBElem *) & Ip6ScopeZoneInfo,
                                           NULL);
        if ((NULL != pIp6ScopeZoneInfo) &&
            (pIp6ScopeZoneInfo->u4ContextId == Ip6ScopeZoneInfo.u4ContextId)
            && (pIp6ScopeZoneInfo->u1Scope == Ip6ScopeZoneInfo.u1Scope))
        {
            if (pIp6ScopeZoneInfo->i4ZoneId != i4ZoneId)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                CLI_SET_ERR (CLI_IP6_INVALID_ZONE_ID);
                return SNMP_FAILURE;
            }
        }
    }
    /*Also check if the zone configuration is for interface local then 
       that zone cant be extended across interface */
    if (u1Scope == ADDR6_SCOPE_INTLOCAL)
    {
        pIp6ScopeZoneInfo =
            Ip6ZoneGetRBTreeScopeZoneEntry (pu1ScopeZone,
                                            pIf6->pIp6Cxt->u4ContextId);
        if (NULL != pIp6ScopeZoneInfo)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_IP6_INVALID_ZONE_ID);
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 * DESCRIPTION : This function checks if the interface list of the other    *
 *               scope-zones that are configured on this interface maintains*
 *               the zone integrity                                         *
 *               Refer:RFC4007                                              *
 *                                                                          *
 * INPUTS      : u4IfIndex - Interface Index,                               *
 *               u1Scope - Scope,                                           *
 *               i4ActiveIfZoneId i4Zoneid - Zone id                        *
 *                                                                          * 
 * OUTPUTS     : Updates the Error Code                                     *
 *                                                                          *
 * RETURNS     : SNMP_SUCCESS/SNMP_FAILURE                                  *  
 *                                                                          *
 * NOTES       : This function is added as a part of RFC4007 implementaion  *
 ****************************************************************************/

INT4
Ip6ZoneVerifyIntOtherZoneMembersZoneConfig (UINT4 u4IfIndex, UINT1 u1Scope,
                                            INT4 i4Zoneid)
{
    tIp6IfZoneMapInfo  *pPrevIfZoneMapInfo = NULL;
    tIp6IfZoneMapInfo  *pTempIfZoneMapInfo = NULL;
    tTMO_SLL_NODE      *pZoneNode = NULL;
    tIp6If             *pIf6 = (tIp6If *) IP6_INTERFACE (u4IfIndex);

    if (pIf6 == NULL)
    {
        return IP6_FAILURE;
    }

    /*Find the node which is the next smaller scope than the scope
       getting configured */
    TMO_SLL_Scan (&pIf6->zoneIlist, pZoneNode, tTMO_SLL_NODE *)
    {
        pTempIfZoneMapInfo = IP6_SCOPE_ZONE_PTR_FROM_SLL (pZoneNode);

        if (Ip6ZoneIsScopeSmaller (u1Scope,
                                   pTempIfZoneMapInfo->u1Scope) == IP6_SUCCESS)
        {
            if (IP6_FAILURE ==
                Ip6ZoneIntegrityCheckForHigherZone (u4IfIndex,
                                                    u1Scope,
                                                    i4Zoneid,
                                                    pTempIfZoneMapInfo))
            {
                return IP6_FAILURE;
            }
            break;
        }
        pPrevIfZoneMapInfo = pTempIfZoneMapInfo;
    }
    if ((pPrevIfZoneMapInfo != NULL) &&
        (pPrevIfZoneMapInfo->u1Scope != ADDR6_SCOPE_INTLOCAL))
    {
        if (IP6_FAILURE ==
            Ip6ZoneIntegrityCheckForLowerZone (u4IfIndex,
                                               u1Scope,
                                               i4Zoneid, pPrevIfZoneMapInfo))
        {
            return IP6_FAILURE;
        }
    }
    /* This is the first scope-zone being configured on the interface 
       so return success */
    return IP6_SUCCESS;
}

 /***************************************************************************
 * DESCRIPTION : This function checks if the lower scope-zone interface     *
 *               list has the same scopezone configuration or not.          *
 *                                                                          *
 * INPUTS      : u4IfIndex - Interface Index,                               *
 *               u1Scope - Scope of the zone getting configured,            *
 *               i4zoneid - Zone id of a scopezone,                         *
 *               pIfZoneMapInfo - pointer to the lower scope zone           *
 *                                configured on the interface               *
 *                                                                          * 
 * OUTPUTS     : NONE                                                       *
 *                                                                          *
 * RETURNS     : IP6_SUCCESS (If the lower scope doesn't have this scope    *
 *                            configuration or if it has same configuration)*
 *               IP6_FAILURE (If the lower scope zone has different         *
 *                            zone configuration)                           *
 *                                                                          *
 * NOTES       : This function is added as a part of RFC4007 implementation *
 ***************************************************************************/
INT4
Ip6ZoneIntegrityCheckForLowerZone (UINT4 u4IfIndex, UINT1 u1Scope,
                                   INT4 i4ZoneId,
                                   tIp6IfZoneMapInfo * pIfZoneMapInfo)
{
    tIp6ScopeZoneInfo   Ip6ScopeZoneinfo;
    tIp6ScopeZoneInfo  *pRBTreeScopeZoneinfo = NULL;
    tIp6If             *pIf6 = (tIp6If *) IP6_INTERFACE (u4IfIndex);

    MEMSET (&Ip6ScopeZoneinfo, 0, sizeof (tIp6ScopeZoneInfo));
    Ip6ScopeZoneinfo.u4ContextId = pIf6->pIp6Cxt->u4ContextId;
    Ip6ScopeZoneinfo.i4ZoneId = pIfZoneMapInfo->i4ZoneId;
    Ip6ScopeZoneinfo.u1Scope = pIfZoneMapInfo->u1Scope;

    /* retrieve the node with the scope lesser than the configured scope
       on this interface */
    pRBTreeScopeZoneinfo = RBTreeGet (gIp6GblInfo.ScopeZoneTree,
                                      (tRBElem *) & Ip6ScopeZoneinfo);
    if (pRBTreeScopeZoneinfo == NULL)
    {
        return IP6_FAILURE;
    }
    /* Find if the interface of the lower scope-zone belongs to the 
       same  scope zone id as being configured if not do not allow 
       this configuration */
    if (IP6_FAILURE == Ip6ZoneCheckScopeZoneIntegrity (pRBTreeScopeZoneinfo,
                                                       u1Scope, i4ZoneId,
                                                       u4IfIndex))
    {
        return IP6_FAILURE;
    }
    return IP6_SUCCESS;

}

/********************************************************************************
 * DESCRIPTION : This function checks if the upper scope-zone interface list    *
 *               has the same scope configuration or not.                       *
 *                                                                              *
 * INPUTS      : u4IfIndex - Interface Index,                                   *
 *               u1Scope - Scope,                                               *
 *               i4ZoneId - Zone id,                                            *
 *               pIfHigherZoneMapInfo - pointer to the higher scope zone node   *
 *                  configured on the interface                                 *
 *                                                                              *
 * OUTPUTS     : NONe                                                           *
 *                                                                              *
 * RETURNS     : IP6_SUCCESS/IP6_FAILURE                                        *
 *                                                                              *
 * NOTES       : This function is added as a part of RFC4007 code changes       *
*********************************************************************************/
INT4
Ip6ZoneIntegrityCheckForHigherZone (UINT4 u4IfIndex, UINT1 u1Scope,
                                    INT4 i4ZoneId,
                                    tIp6IfZoneMapInfo * pIfHigherZoneMapInfo)
{
    tIp6ScopeZoneInfo   Ip6ScopeZoneinfo;
    tIp6ScopeZoneInfo  *pRBTreeScopeZoneinfo = NULL;
    tIp6If             *pIf6 = (tIp6If *) IP6_INTERFACE (u4IfIndex);

    MEMSET (&Ip6ScopeZoneinfo, 0, sizeof (tIp6ScopeZoneInfo));

    /* If the higher node is linklocal or global this check is
       not needed */
    if ((pIfHigherZoneMapInfo->u1Scope == ADDR6_SCOPE_LLOCAL) ||
        (pIfHigherZoneMapInfo->u1Scope == ADDR6_SCOPE_GLOBAL))
    {
        return IP6_SUCCESS;
    }
    /* update the node field and get the node */
    Ip6ScopeZoneinfo.u4ContextId = pIf6->pIp6Cxt->u4ContextId;
    Ip6ScopeZoneinfo.i4ZoneId = i4ZoneId;
    Ip6ScopeZoneinfo.u1Scope = u1Scope;

    pRBTreeScopeZoneinfo = RBTreeGet (gIp6GblInfo.ScopeZoneTree,
                                      (tRBElem *) & Ip6ScopeZoneinfo);

    /* If this the first node getting configured return
       success */
    if (pRBTreeScopeZoneinfo == NULL)
    {
        return IP6_SUCCESS;
    }

    if (IP6_SUCCESS ==
        Ip6ZoneCheckScopeZoneIntegrity (pRBTreeScopeZoneinfo,
                                        pIfHigherZoneMapInfo->u1Scope,
                                        pIfHigherZoneMapInfo->i4ZoneId,
                                        u4IfIndex))
    {
        return IP6_SUCCESS;
    }
    return IP6_FAILURE;
}

/****************************************************************************
 * DESCRIPTION : This function checks retrieves the interface list of the node
 *               passed and checks if the interfaces has different zone-id for
 *               configured scope if so it returns failure
 *
 * INPUTS      : u1Scope - Scope,
 *               i4ZoneId - Zone id,
 *               pRBTreeScopeZoneinfo - pointer to the higher/lower scope zone 
 *                     node for which the zone integrity check has to be done
 *
 * OUTPUTS     : NONE
 *
 * RETURNS     : IP6_SUCCESS/IP6_FAILURE
 *
 * NOTES       : This function is added as a part of RFC4007 code changes
 *******************************************************************************/

INT4
Ip6ZoneCheckScopeZoneIntegrity (tIp6ScopeZoneInfo * pRBTreeScopeZoneinfo,
                                UINT1 u1Scope, INT4 i4ZoneId, UINT4 u4IfIndex)
{
    tTMO_SLL_NODE      *pZoneNode = NULL;
    tIp6IfZoneMapInfo  *pTempIfZoneMapInfo = NULL;
    tIp6If             *pIf6 = NULL;
    UINT4               u4Intf = 0;
    UINT1               u1Result = OSIX_FALSE;

    for (u4Intf = 1; u4Intf <= IP6_MAX_ZONE_INTERFACE; u4Intf++)
    {
        /* Get the bit list of the scope-zone */
        OSIX_BITLIST_IS_BIT_SET (pRBTreeScopeZoneinfo->InterfaceList,
                                 u4Intf, IP6_MAX_ZONE_INT_LIST_SIZE, u1Result);

        if ((u1Result == OSIX_TRUE) && (u4Intf != u4IfIndex))
        {
            /* Check if the interface exists */
            pIf6 = (tIp6If *) IP6_INTERFACE (u4Intf);

            if (pIf6 == NULL)
                return IP6_FAILURE;

            /* Scan the interface for the scope-zone and check if the 
               node has same scope but different zone-id configuration
               if yes return faillure */
            TMO_SLL_Scan (&pIf6->zoneIlist, pZoneNode, tTMO_SLL_NODE *)
            {
                pTempIfZoneMapInfo = IP6_SCOPE_ZONE_PTR_FROM_SLL (pZoneNode);

                if (pTempIfZoneMapInfo->u1Scope < u1Scope)
                {
                    continue;
                }
                else if ((pTempIfZoneMapInfo->u1Scope == u1Scope) &&
                         (pTempIfZoneMapInfo->i4ZoneId != i4ZoneId))
                {
                    return IP6_FAILURE;
                }
                else if (pTempIfZoneMapInfo->u1Scope > u1Scope)
                {
                    break;
                }
            }
        }
    }
    return IP6_SUCCESS;
}

/************************************************************************
 * DESCRIPTION : Ip6ZoneCheckAddrMatch - This function checks if    *
 *               the (linklocal & global addresses of the interface     *
 *               within the zone are unique)                *
 *                                     *
 * INPUTS      : u4IfIndex1 - Interface index of the 1st Intf        *
 *               u4IfIndex2 - Interface index of the 2nd Intf        *
 *               u1Scope    - Scope configured on this interface     *
 *                                    *
 * OUTPUTS     : None                            *
 *                                    *
 * RETURNS     : SNMP_SUCCESS or SNMP_FAILURE                *
 *                                    *
 * NOTES       : This function is added as a part of RFC4007         *
 *               implementation                        *
 ************************************************************************/
INT4
Ip6ZoneCheckAddrMatch (UINT4 u4IfIndex1, UINT4 u4IfIndex2, UINT1 u1Scope)
{
    tTMO_SLL_NODE      *pSllInfo1 = NULL;
    tTMO_SLL_NODE      *pSllInfo2 = NULL;
    tIp6AddrInfo       *pAddrInfo1 = NULL;
    tIp6AddrInfo       *pAddrInfo2 = NULL;
    tIp6LlocalInfo     *pLlocalInfo1 = NULL;
    tIp6LlocalInfo     *pLlocalInfo2 = NULL;
    tIp6If             *pIp6If1 = (tIp6If *) IP6_INTERFACE (u4IfIndex1);
    tIp6If             *pIp6If2 = (tIp6If *) IP6_INTERFACE (u4IfIndex2);

    if ((pIp6If1 == NULL) || (pIp6If2 == NULL))
    {
        return IP6_FAILURE;
    }

    if (u1Scope == ADDR6_SCOPE_GLOBAL)
    {

        TMO_SLL_Scan (&pIp6If1->addr6Ilist, pSllInfo1, tTMO_SLL_NODE *)
        {
            pAddrInfo1 = IP6_ADDR_PTR_FROM_SLL (pSllInfo1);
            TMO_SLL_Scan (&pIp6If2->addr6Ilist, pSllInfo2, tTMO_SLL_NODE *)
            {
                pAddrInfo2 = IP6_ADDR_PTR_FROM_SLL (pSllInfo2);
                if (TRUE ==
                    Ip6AddrMatch ((&pAddrInfo1->ip6Addr),
                                  (&pAddrInfo2->ip6Addr),
                                  (INT4) pAddrInfo1->u1PrefLen))
                {
                    if (&pAddrInfo1->u1PrefLen == &pAddrInfo2->u1PrefLen)
                        return IP6_FAILURE;
                }
            }
        }
    }
    else if (u1Scope == ADDR6_SCOPE_LLOCAL)
    {
        TMO_SLL_Scan (&pIp6If1->lla6Ilist, pSllInfo1, tTMO_SLL_NODE *)
        {
            pLlocalInfo1 = IP6_LLADDR_PTR_FROM_SLL (pSllInfo1);
            TMO_SLL_Scan (&pIp6If2->lla6Ilist, pSllInfo2, tTMO_SLL_NODE *)
            {
                pLlocalInfo2 = IP6_LLADDR_PTR_FROM_SLL (pSllInfo2);
                if (TRUE ==
                    Ip6AddrMatch ((&pLlocalInfo1->ip6Addr),
                                  (&pLlocalInfo2->ip6Addr),
                                  IP6_ADDR_SIZE_IN_BITS))
                {
                    return IP6_FAILURE;
                }
            }
        }
    }
    return IP6_SUCCESS;
}

/************************************************************************
 * DESCRIPTION : Creates a scope-zone node in RBTree as well as        * 
 *               on the interface and updates its info            *
 *                                    *
 * INPUTS      : u4IfIndex - Interface Index,                *
 *               u1Scope  - Scope for which the node is configured,    *
 *               pu1ZoneName - Pointer to the zone name            *
 *               u1CreationStatus - Creation status of the zone,    *
 *               i4ZoneId - Zoneid of the node                *
 *                                    *
 * OUTPUTS     : NONE                            *        
 *                                    *    
 * RETURNS     : IP6_SUCCESS                         *
 *               IP6_FAILURE                        *
 *                                    *
 * NOTES       : This function is added as a part of RFC4007         *
 *                 implementation                    *
 ************************************************************************/

INT4
Ip6ZoneCreateScopeZoneInfo (UINT4 u4IfIndex, UINT1 u1Scope, UINT1 *pu1ZoneName,
                            UINT1 u1CreationStatus, INT4 i4ZoneId)
{
    tIp6ScopeZoneInfo  *pIp6ScopeZoneInfo = NULL;
    tIp6ScopeZoneInfo   Ip6ScopeZoneInfo;
    UINT4               u4ContextId = 0;
    INT4                i4ZoneIndex = 0;
    UINT4               u4Mask = 0;

    VcmGetContextIdFromCfaIfIndex (u4IfIndex, &u4ContextId);

    IP6_TRC_ARG3 (u4ContextId, IP6_MOD_TRC,
                  DATA_PATH_TRC, IP6_NAME,
                  "IP6ScopeZone:Ip6ZoneCreateScopeZoneInfo:Zone %s "
                  "scope %d IF %d\n", pu1ZoneName, u1Scope, u4IfIndex);

    MEMSET (&Ip6ScopeZoneInfo, IP6_ZERO, sizeof (Ip6ScopeZoneInfo));

    /* Check if the scope zone already exists in the RB Tree 
       if it exists just update this interface to the interface 
       list else create a new node */

    Ip6ScopeZoneInfo.u1Scope = u1Scope;
    Ip6ScopeZoneInfo.u4ContextId = u4ContextId;
    Ip6ScopeZoneInfo.i4ZoneId = i4ZoneId;
    pIp6ScopeZoneInfo = RBTreeGet (gIp6GblInfo.ScopeZoneTree,
                                   (tRBElem *) & Ip6ScopeZoneInfo);

    /* Node does not exist at RBTree Tree so create the node */
    if (NULL == pIp6ScopeZoneInfo)
    {
        i4ZoneIndex = Ip6ZoneGetFreeZoneIndex ();
        if (i4ZoneIndex == IP6_ZONE_INVALID)
        {
            /* No Free Index max zone is already configured
               return failure */
            return IP6_FAILURE;
        }

        if (IP6_FAILURE ==
            Ip6ZoneCreateZoneNodeinRbTree (u4IfIndex, u1Scope,
                                           pu1ZoneName, u1CreationStatus,
                                           i4ZoneId, i4ZoneIndex))
        {
            Ip6ZoneReleaseZoneIndex (i4ZoneIndex);
            return IP6_FAILURE;
        }
    }
    /* Node is already available so update the interface */
    else
    {
        Ip6ZoneUpdateZoneNodeinRbTree (u4IfIndex, pIp6ScopeZoneInfo);
        i4ZoneIndex = pIp6ScopeZoneInfo->i4ZoneIndex;
    }

    /* Create a scope-zone in the interface and update its elements */
    if (IP6_FAILURE == Ip6ZoneCreateIfZoneNode (u1Scope, u4IfIndex,
                                                i4ZoneId, i4ZoneIndex,
                                                u1CreationStatus, pu1ZoneName))
    {
        /*failed to create a node in interface and hence delete 
           the node in RBTree also */
        Ip6ZoneDeleteZoneNodeFromRbTree (u4IfIndex, u1Scope, i4ZoneId);
        return IP6_FAILURE;
    }

    if ((u1CreationStatus == IP6_ZONE_TYPE_MANUAL) &&
        ((u1Scope == ADDR6_SCOPE_LLOCAL) || (u1Scope == ADDR6_SCOPE_INTLOCAL)))
    {
        Ip6ZoneSetZoneIdforAutoZones (u4IfIndex, i4ZoneId, u1Scope);
    }

    u4Mask = NETIPV6_INTERFACE_MAPPED_TO_ZONE;

    Ip6ZoneSendNotifyOnNonGlobalZoneCreation (u4Mask, u1Scope,
                                              i4ZoneIndex, u4IfIndex);

    return IP6_SUCCESS;
}

/************************************************************************
 * DESCRIPTION : Creates a scope-zone node in RBTree and updates its    *
 *               information                        *
 *                                    *
 * INPUTS      : u4IfIndex - Interface Index,                 *
 *               u1Scope  - Scope for which the node is configured,    *
 *               pu1ZoneName - Pointer to the zone name         *
 *               u1CreationStatus - Creation status of the zone,     *
 *               i4ZoneId - Zoneid of the node                *
 *                                    *
 * OUTPUTS     : NONE                            *
 *                                    *
 * RETURNS     : IP6_SUCCESS - When a node is successfully created    *
 *               IP6_FAILURE - When failed to allocate memory for the    *
 *                             node                    *
 *                                    *
 * NOTES       : This function is added as a part of RFC4007 implement    *
 ************************************************************************/
INT4
Ip6ZoneCreateZoneNodeinRbTree (UINT4 u4IfIndex, UINT1 u1Scope,
                               UINT1 *pu1ZoneName, UINT1 u1CreationStatus,
                               INT4 i4ZoneId, INT4 i4ZoneIndex)
{

    tIp6ScopeZoneInfo  *pRBTreeScopeZoneInfo = NULL;
    tIp6If             *pIf6 = (tIp6If *) IP6_INTERFACE (u4IfIndex);
    UINT4               u4ContextId = 0;
    UINT4               u4Result = 0;

    if (pIf6 == NULL)
    {
        return IP6_FAILURE;
    }
    /* Allocate memory from the mempool for the scope-zone
       node */
    VcmGetContextIdFromCfaIfIndex (u4IfIndex, &u4ContextId);
    pRBTreeScopeZoneInfo = (tIp6ScopeZoneInfo *) (VOID *)
        Ip6GetMem (u4ContextId, (UINT2) gIp6GblInfo.i4Ip6ScopeZonePoolId);

    /* If failed to allocate memory return failure */
    if (pRBTreeScopeZoneInfo == NULL)
    {
        IP6_TRC_ARG1 (u4ContextId, IP6_MOD_TRC,
                      DATA_PATH_TRC, IP6_NAME,
                      "IP6ScopeZone:Ip6ScopeZoneCreate: failure "
                      "in getting mem from pool %d\n",
                      gIp6GblInfo.i4Ip6ScopeZonePoolId);

        IP6_TRC_ARG4 (u4ContextId, IP6_MOD_TRC,
                      MGMT_TRC, IP6_NAME, "%s mem err: Type = %d "
                      "PoolId = %d Memptr = %p\n", ERROR_FATAL,
                      ERR_MEM_GET, 0, gIp6GblInfo.i4Ip6ScopeZonePoolId);

        return IP6_FAILURE;

    }
    MEMSET (pRBTreeScopeZoneInfo, IP6_ZERO, sizeof (tIp6ScopeZoneInfo));

    /* Fill the tIp6ScopeZoneInfo RB Tree Strucutre */
    pRBTreeScopeZoneInfo->u1Scope = u1Scope;
    pRBTreeScopeZoneInfo->i4ZoneId = i4ZoneId;
    pRBTreeScopeZoneInfo->u4ContextId = u4ContextId;
    pRBTreeScopeZoneInfo->i4ZoneIndex = i4ZoneIndex;
    pRBTreeScopeZoneInfo->u1CreateStatus = u1CreationStatus;
    pRBTreeScopeZoneInfo->u4ZoneCount++;
    MEMSET (pRBTreeScopeZoneInfo->au1ZoneName, 0,
            sizeof (pRBTreeScopeZoneInfo->au1ZoneName));
    MEMCPY (pRBTreeScopeZoneInfo->au1ZoneName, pu1ZoneName,
            STRLEN (pu1ZoneName));

    OSIX_BITLIST_SET_BIT (pRBTreeScopeZoneInfo->InterfaceList,
                          u4IfIndex, sizeof (tInterfaceList));

    pRBTreeScopeZoneInfo->u1IsDefaultZone =
        Ip6ZoneFindIfDefZoneCreatedForScope (u1Scope, u4ContextId);

    Ip6ZoneSetNonGlobalZoneCnt (u1Scope, u4ContextId, IP6_TRUE);
    u4Result =
        RBTreeAdd (gIp6GblInfo.ScopeZoneTree, (tRBElem *) pRBTreeScopeZoneInfo);

    UNUSED_PARAM (u4Result);
    return IP6_SUCCESS;
}

/****************************************************************
 * DESCRIPTION : This functions keeps count of no of non-global    *
 *               zone created in a particular context         *
 *                                *
 * INPUTS      : u1Scope - Scope                *
 *               u4IfIndex - Interface Index            *
 *                                *
 * OUTPUTS     : NONE                        *
 *                                *
 * RETURNS     : NONE                        *
 *                                *
 * NOTES       : This function is added as a part of RFC4007     *
 *               implement                    *
 ****************************************************************/
VOID
Ip6ZoneSetNonGlobalZoneCnt (UINT1 u1Scope, UINT4 u4ContextId,
                            UINT1 u1IsNonGblZoneSet)
{
    tIp6Cxt            *pIp6Cxt = NULL;

    pIp6Cxt = gIp6GblInfo.apIp6Cxt[u4ContextId];

    if (pIp6Cxt == NULL)
        return;

    if ((u1Scope != ADDR6_SCOPE_LLOCAL) &&
        (u1Scope != ADDR6_SCOPE_INTLOCAL) && (u1Scope != ADDR6_SCOPE_GLOBAL))
    {
        if (IP6_TRUE == u1IsNonGblZoneSet)
            pIp6Cxt->u4MaxNonGblZoneCount++;

        else
            pIp6Cxt->u4MaxNonGblZoneCount--;
    }

}

/****************************************************************
 * DESCRIPTION : Updates a scope-zone node in RBTree        * 
 *                                *
 * INPUTS      : u4IfIndex - Interface Index,            *
                 pIp6ScopeZoneInfo - Pointer to the node in     *
                                       RBTree            *
 *                                *
 * OUTPUTS     : NONE                        *
 *                                *
 * RETURNS     : NONE                        *
 *                                *
 * NOTES       : This function is added as a part of RFC4007     *
 *               implement                    *
 ****************************************************************/
VOID
Ip6ZoneUpdateZoneNodeinRbTree (UINT4 u4IfIndex,
                               tIp6ScopeZoneInfo * pIp6ScopeZoneInfo)
{

    OSIX_BITLIST_SET_BIT (pIp6ScopeZoneInfo->InterfaceList, u4IfIndex,
                          sizeof (tInterfaceList));
    pIp6ScopeZoneInfo->u4ZoneCount++;

    return;

}

/*********************************************************************
 * DESCRIPTION : This function inserts a scope-zone node in ZoneIlist*
 *                                                 *
 * INPUTS      : pIfZoneMapInfo - Pointer allocated to create a      *
 *                               zone node in the interface          *
 *               u1Scope - Scope for which zone node is created         *
 *               u4IfIndex - Interface Index                 *
 *               i4ZoneId - ZoneId of the scope-zone             *
 *               i4ZoneIndex - ZoneIndex of the scope-zone           *
 *               u1CreationStatus - CreationStatus of the zone       *
 *               pu1zoneName - Pointer to the zone name              *
 *                                     *
 * OUTPUTS     : NONE                                 *
 *                                     *
 * RETURNS     : NONE                                                *
 *                                     *
 * NOTES       : This function is added as a part of RFC4007         *
 *                        implementation                             *
 *********************************************************************/
INT4
Ip6ZoneCreateIfZoneNode (UINT1 u1Scope,
                         UINT4 u4IfIndex, INT4 i4ZoneId, INT4 i4ZoneIndex,
                         UINT1 u1CreationStatus, UINT1 *pu1ZoneName)
{
    tIp6IfZoneMapInfo  *pPrevIfZoneMapInfo = NULL;
    tIp6IfZoneMapInfo  *pTempIfZoneMapInfo = NULL;
    tIp6IfZoneMapInfo  *pIfZoneMapInfo;
    tTMO_SLL_NODE      *pZoneNode = NULL;
    tIp6If             *pIf6 = (tIp6If *) IP6_INTERFACE (u4IfIndex);
    UINT4               u4ContextId = 0;

    /* Allocate memory for the new zone node to be inserted 
       in the interface */
    VcmGetContextIdFromCfaIfIndex (u4IfIndex, &u4ContextId);

    pIfZoneMapInfo = (tIp6IfZoneMapInfo *) (VOID *)
        Ip6GetMem (u4ContextId, (UINT2) gIp6GblInfo.i4Ip6IfzonePoolId);

    /* If Memory could not be allocate return Failure */
    if (pIfZoneMapInfo == NULL)
    {
        IP6_TRC_ARG1 (u4ContextId, IP6_MOD_TRC,
                      DATA_PATH_TRC, IP6_NAME,
                      "IP6ScopeZone:Ip6ScopeZoneCreate: failure in "
                      "getting mem from pool %d\n",
                      gIp6GblInfo.i4Ip6IfzonePoolId);

        IP6_TRC_ARG4 (u4ContextId, IP6_MOD_TRC,
                      MGMT_TRC, IP6_NAME,
                      "%s mem err: Type = %d  PoolId = %d Memptr = %p\n",
                      ERROR_FATAL, ERR_MEM_GET, 0,
                      gIp6GblInfo.i4Ip6IfzonePoolId);

        return IP6_FAILURE;
    }

    pIfZoneMapInfo->u1Scope = u1Scope;
    pIfZoneMapInfo->i4ZoneId = i4ZoneId;
    pIfZoneMapInfo->u1ConfigStatus = u1CreationStatus;
    pIfZoneMapInfo->i4ZoneIndex = i4ZoneIndex;
    MEMSET (pIfZoneMapInfo->au1ZoneName,
            0, STRLEN (pIfZoneMapInfo->au1ZoneName));
    MEMCPY (pIfZoneMapInfo->au1ZoneName, pu1ZoneName, STRLEN (pu1ZoneName));

    if (TMO_SLL_Count (&pIf6->zoneIlist) == IP6_ZERO)
    {
        pIf6->u1ZoneCreationStatus = u1CreationStatus;
    }

    TMO_SLL_Scan (&gIp6GblInfo.apIp6If[u4IfIndex]->zoneIlist,
                  pZoneNode, tTMO_SLL_NODE *)
    {
        pTempIfZoneMapInfo = IP6_SCOPE_ZONE_PTR_FROM_SLL (pZoneNode);

        if (IP6_SUCCESS ==
            Ip6ZoneIsScopeSmaller (u1Scope, pTempIfZoneMapInfo->u1Scope))
        {
            break;
        }
        pPrevIfZoneMapInfo = pTempIfZoneMapInfo;
    }

    if (pPrevIfZoneMapInfo == NULL)
    {
        TMO_SLL_Insert (&gIp6GblInfo.apIp6If[u4IfIndex]->zoneIlist,
                        NULL, &pIfZoneMapInfo->nextZone);
    }
    else
    {
        TMO_SLL_Insert (&gIp6GblInfo.apIp6If[u4IfIndex]->zoneIlist,
                        &pPrevIfZoneMapInfo->nextZone,
                        &pIfZoneMapInfo->nextZone);
    }

    return IP6_SUCCESS;
}

/*****************************************************************************
 * DESCRIPTION : This function autocreates a linklocal scope-zone or global
 *               scope-zone  whenever an address is assigned on an interface or 
 *               when ipv6 is enabled on that interface
 *
 * INPUTS      : u4IfIndex - Interface Index,
 *               u1Scope   - Linklocal scope
 *
 * OUTPUTS     : NONE
 *
 * RETURNS     : IP6_SUCCESS (On successful creation of linklocal scopezone)
 *                  or
 *               IP6_FAILURE (When scope-zone cannot be created)
 *
 * NOTES       : This function is added as a part of RFC4007 implementation
 *****************************************************************************/
VOID
Ip6ZoneCreateAutoScopeZone (UINT4 u4IfIndex, UINT1 u1Scope)
{
    tIp6IfZoneMapInfo  *pIp6IfZoneMapInfo = NULL;
    tIp6If             *pIf6 = NULL;
    tIp6ScopeZoneInfo  *pIp6ScopeZoneInfo = NULL;
    tIp6ScopeZoneInfo   Ip6ScopeZoneInfo;
    INT4                i4ZoneId = IP6_ZONE_INVALID;
    UINT4               u4ContextId = 0;
    UINT1               u1CreationStatus = IP6_ZONE_TYPE_AUTO;
    UINT1               au1ZoneName[IP6_SCOPE_ZONE_NAME_LEN];

    MEMSET (au1ZoneName, IP6_ZERO, IP6_SCOPE_ZONE_NAME_LEN);
    MEMSET (&Ip6ScopeZoneInfo, IP6_ZERO, sizeof (tIp6ScopeZoneInfo));

    pIf6 = IP6_INTERFACE (u4IfIndex);
    if (NULL == pIf6)
        return;
    /* Create a llocal or global Zone on this interface only no 
       other llocal or global zone exist on this interface */
    VcmGetContextIdFromCfaIfIndex (u4IfIndex, &u4ContextId);
    pIp6IfZoneMapInfo = Ip6ZoneGetIfScopeZoneEntry (u4IfIndex, u1Scope);

    if (NULL == pIp6IfZoneMapInfo)
    {
        if ((u1Scope == ADDR6_SCOPE_LLOCAL) ||
            (u1Scope == ADDR6_SCOPE_INTLOCAL))
        {
            /*  Get any free available zone id */
            i4ZoneId = Ip6ZoneGetFreeZoneId (u4ContextId, u1Scope);

            /* If no free id is available return failure */
            if (i4ZoneId == IP6_ZONE_INVALID)
            {
                IP6_TRC_ARG3 (u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                              "IP6ScopeZone:Ip6ZoneCreateAutoScopeZone : Failure in "
                              "getting zone-id for linklocal scope scope = %d "
                              "IF = %d zone-id = %d\n", u1Scope, u4IfIndex,
                              i4ZoneId);

                return;
            }
        }
        else
        {
            Ip6ScopeZoneInfo.u4ContextId = pIf6->pIp6Cxt->u4ContextId;
            Ip6ScopeZoneInfo.u1Scope = ADDR6_SCOPE_GLOBAL;
            Ip6ScopeZoneInfo.i4ZoneId = 0;
            pIp6ScopeZoneInfo = RBTreeGetNext (gIp6GblInfo.ScopeZoneTree,
                                               (tRBElem *) & Ip6ScopeZoneInfo,
                                               NULL);
            if ((NULL != pIp6ScopeZoneInfo) &&
                (pIp6ScopeZoneInfo->u4ContextId == Ip6ScopeZoneInfo.u4ContextId)
                && (pIp6ScopeZoneInfo->u1Scope == Ip6ScopeZoneInfo.u1Scope))
            {
                i4ZoneId = pIp6ScopeZoneInfo->i4ZoneId;
            }
            else
            {
                i4ZoneId = IP6_GLOBAL_ZONEID;
            }
        }
        Ip6ZoneGetZoneName (au1ZoneName, u1Scope, i4ZoneId);
        /* Create the auto linklocal zone for this 
           interface and also create a node in the 
           RBtree for this scope zone */
        if (IP6_FAILURE ==
            Ip6ZoneCreateScopeZoneInfo (u4IfIndex, u1Scope,
                                        au1ZoneName, u1CreationStatus,
                                        i4ZoneId))
        {
            IP6_TRC_ARG3 (u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                          "IP6ScopeZone:Ip6ZoneCreateAutoScopeZone : Failure in "
                          "creation of scope-zone for scope = %d "
                          "IF = %d zone-id = %d\n", u1Scope, u4IfIndex,
                          i4ZoneId);
            if ((u1Scope == ADDR6_SCOPE_LLOCAL)
                || (u1Scope == ADDR6_SCOPE_INTLOCAL))
            {
                Ip6ZoneReleaseLlocalZoneId (i4ZoneId, u4IfIndex, u1Scope);
            }
            return;
        }
        pIf6->u1ZoneRowStatus = IP6_ZONE_ROW_STATUS_ACTIVE;
    }

    /* Already scope-zone created on this interfaace so 
       return success */
    return;
}

/*****************************************************************************
 * DESCRIPTION : This function on receiving link-notification event from
 *               OSPF creates the zone that are present in active in
 *               all the standby interfaces
 *
 * INPUTS      : pIfZoneMapInfo,pu1KeyStr,, Scope, Interface Index,
 *               i4Key (zone id),zoneIndex
 *
 * OUTPUTS     : NONE
 *
 * RETURNS     : NONE
 *
 * NOTES       : This function is added as a part of RFC4007 code changes
 ****************************************************************************/
INT4
Ip6ZoneReplaceZoneOnStandbyIf (UINT4 u4StandbyIfIndex, UINT1 u1ActiveScope,
                               UINT1 u1CreationStatus, INT4 i4ActiveZoneId)
{
    tIp6IfZoneMapInfo  *pIfZoneMapOldInfo = NULL;
    tIp6ScopeZoneInfo   Ip6ScopeZoneInfo;
    tIp6If             *pIf6 = IP6_INTERFACE (u4StandbyIfIndex);
    INT4                i4ZoneIndex = 0;
    UINT1               au1ActiveScopeZone[IP6_SCOPE_ZONE_NAME_LEN];

    if (pIf6 == NULL)
        return IP6_FAILURE;

    i4ZoneIndex = Ip6ZoneCheckFreeIndexAvail ();
    if (i4ZoneIndex == IP6_ZONE_INVALID)
    {
        /* No Free Index max zone is already configured
           return failure */
        IP6_TRC_ARG3 (pIf6->pIp6Cxt->u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC,
                      IP6_NAME,
                      "Ip6Zone: Ip6ZoneReplaceZoneOnStandbyIf: Creation of auto zones Onlink"
                      "failed in onlink interface for the standby interface as max zone is already"
                      "configured standby interface = %d active scope = %d activezone-id = %d\n",
                      u4StandbyIfIndex, u1ActiveScope, i4ActiveZoneId);

        return IP6_FAILURE;
    }

    MEMSET (&Ip6ScopeZoneInfo, 0, sizeof (tIp6ScopeZoneInfo));
    MEMSET (au1ActiveScopeZone, 0, sizeof (IP6_SCOPE_ZONE_NAME_LEN));

    /* Get the scope-zone name that has to be configured on 
       this standby interface */
    Ip6ZoneGetZoneName (au1ActiveScopeZone, u1ActiveScope, i4ActiveZoneId);

    /* Check if the standby interface has the same scope configuration */

    pIfZoneMapOldInfo = Ip6ZoneGetIfScopeZoneEntry (u4StandbyIfIndex,
                                                    u1ActiveScope);

    /* Linklocal scope-zone is already created on this interface
       check if the zone-id is same on active & standby if so
       return success else update the node in RBTree and If struc */
    if (NULL != pIfZoneMapOldInfo)
    {
        if (pIfZoneMapOldInfo->i4ZoneId == i4ActiveZoneId)
        {
            return IP6_SUCCESS;
        }
        /* as different zone id is configured on the standby
           interface reset this interface from that zone and update
           it in the active zone-node */
        Ip6ZoneDeleteZoneNodeFromRbTree (u4StandbyIfIndex, u1ActiveScope,
                                         pIfZoneMapOldInfo->i4ZoneId);

        /* delete the zone node from the interface */
        Ip6ZoneDeleteIfZoneNode (u4StandbyIfIndex, u1ActiveScope);
    }

    if (IP6_FAILURE == Ip6ZoneCreateScopeZoneInfo (u4StandbyIfIndex,
                                                   u1ActiveScope,
                                                   au1ActiveScopeZone,
                                                   u1CreationStatus,
                                                   i4ActiveZoneId))
    {
        return IP6_FAILURE;
    }

    return IP6_SUCCESS;
}

/*************************************************************************
 * DESCRIPTION : This function checks if the last interface is removed
 *               from the RBTree scopezone node, if yes it releases the
 *               zone-index and if the scope is llocal which is autocreated
 *               then it releases the zone-id too
 *
 * INPUTS      : pIp6ScopeZoneOldInfo - Pointer to the scope-zone node in 
 *               RBTree
 *
 * OUTPUTS     : None
 *
 * RETURNS     : The count of interface in a particular node
 *
 * NOTES       : This function is added as a part of RFC4007 code changes
 ***************************************************************************/
UINT4
Ip6ZoneCheckIfLastInterface (tIp6ScopeZoneInfo * pIp6ScopeZoneOldInfo,
                             UINT4 u4IfIndex)
{
    UINT4               u4Intf = 0;
    UINT4               u4Count = IP6_ZERO;
    UINT1               u1Result = OSIX_FALSE;

    for (u4Intf = 1; u4Intf <= IP6_MAX_ZONE_INTERFACE; u4Intf++)
    {

        OSIX_BITLIST_IS_BIT_SET (pIp6ScopeZoneOldInfo->InterfaceList,
                                 u4Intf, IP6_MAX_ZONE_INT_LIST_SIZE, u1Result);
        if (u1Result == OSIX_TRUE)
        {
            u4Count++;
        }
    }
    if (u4Count == IP6_ZERO)
    {
        if (((pIp6ScopeZoneOldInfo->u1Scope == ADDR6_SCOPE_INTLOCAL) ||
             (pIp6ScopeZoneOldInfo->u1Scope == ADDR6_SCOPE_LLOCAL)) &&
            (pIp6ScopeZoneOldInfo->u1CreateStatus == IP6_ZONE_TYPE_AUTO))
        {
            Ip6ZoneReleaseLlocalZoneId (pIp6ScopeZoneOldInfo->i4ZoneId,
                                        u4IfIndex,
                                        pIp6ScopeZoneOldInfo->u1Scope);
        }
        Ip6ZoneReleaseZoneIndex (pIp6ScopeZoneOldInfo->i4ZoneIndex);
    }
    return u4Count;
}

/********************************************************************************
 * DESCRIPTION : Returns entry ptr in the RBTree for a particular scope-zone
 *               if the scope-zone entry exist else returns NULL.
 *
 * INPUTS      : Character pointer with the Zone name
 *
 * OUTPUTS     : NONE
 *
 * RETURNS     : Scope Zone Information if present,NULL otherwise
 *
 * NOTES       : This function is added as a part of RFC4007 code changes
 **********************************************************************************/
tIp6ScopeZoneInfo  *
Ip6ZoneGetRBTreeScopeZoneEntry (UINT1 *pu1ZoneName, UINT4 u4ContextId)
{
    tIp6ScopeZoneInfo  *pScopeZoneInfo = NULL;
    tIp6ScopeZoneInfo   Ip6ScopeZoneInfo;
    INT4                i4ZoneId = 0;
    UINT1               u1Scope = 0;

    MEMSET (&Ip6ScopeZoneInfo, IP6_ZERO, sizeof (tIp6ScopeZoneInfo));

    /* Get the scope and the zoneid from the zone name */
    if (IP6_FAILURE == Ip6ZoneGetZoneIdFromZoneName (pu1ZoneName,
                                                     &u1Scope, &i4ZoneId))
    {
        return NULL;
    }
    Ip6ScopeZoneInfo.i4ZoneId = i4ZoneId;
    Ip6ScopeZoneInfo.u4ContextId = u4ContextId;
    Ip6ScopeZoneInfo.u1Scope = u1Scope;

    pScopeZoneInfo = RBTreeGet (gIp6GblInfo.ScopeZoneTree,
                                (tRBElem *) & Ip6ScopeZoneInfo);

    if (pScopeZoneInfo != NULL)
    {
        return pScopeZoneInfo;
    }

    return NULL;
}

/********************************************************************************
 * DESCRIPTION : when a scope-zone of a particular scope is made as default 
 *               it tries to reset the other scope-zone of the same scope which was the 
 *               default scope-zone earlier.
 *
 * INPUTS      : Character pointer with the Zone name
 *
 * OUTPUTS     : NONE
 *
 * RETURNS     : VOID
 
 * NOTES       : This function is added as a part of RFC4007 code changes
 **********************************************************************************/
VOID
Ip6ZoneResetOtherDefaultZoneEntry (UINT1 u1Scope, INT4 i4ZoneId,
                                   UINT4 u4ContextId)
{
    tIp6ScopeZoneInfo  *pScopeZoneInfo = NULL;
    tIp6ScopeZoneInfo   Ip6ScopeZoneInfo;
    tIp6ScopeZoneInfo  *pNextScopeZoneInfo = NULL;
    INT4                i4LocalZoneId = 0;

    MEMSET (&Ip6ScopeZoneInfo, 0, sizeof (tIp6ScopeZoneInfo));

    Ip6ScopeZoneInfo.u4ContextId = u4ContextId;
    Ip6ScopeZoneInfo.u1Scope = u1Scope;
    Ip6ScopeZoneInfo.i4ZoneId = i4LocalZoneId;

    /* get the first scope-zone of this scope */
    pScopeZoneInfo = RBTreeGetNext (gIp6GblInfo.ScopeZoneTree,
                                    (tRBElem *) & Ip6ScopeZoneInfo, NULL);

    while (pScopeZoneInfo != NULL)
    {
        if (pScopeZoneInfo->u1Scope > u1Scope)
        {
            break;
        }

        if ((pScopeZoneInfo->i4ZoneId != i4ZoneId) &&
            (pScopeZoneInfo->u4ContextId == u4ContextId) &&
            (pScopeZoneInfo->u1IsDefaultZone == IP6_ZONE_TRUE))
        {
            pScopeZoneInfo->u1IsDefaultZone = IP6_ZONE_FALSE;
        }

        pNextScopeZoneInfo =
            (tIp6ScopeZoneInfo *) RBTreeGetNext (gIp6GblInfo.ScopeZoneTree,
                                                 (tRBElem *) pScopeZoneInfo,
                                                 NULL);
        pScopeZoneInfo = pNextScopeZoneInfo;
    }
    return;
}

/******************************************************************************
 * DESCRIPTION : This function deletes a scope-zone
 *
 * INPUTS      : Interface Index, Scope, Zone-Id
 *
 * OUTPUTS     : None
 *
 * RETURNS     : SUCCESS or FAILURE
 *
 * NOTES       : This function is added as a part of RFC4007 code changes
 ******************************************************************************/
VOID
Ip6ScopeZoneDelete (UINT4 u4IfIndex, UINT1 u1Scope, INT4 i4ZoneId)
{
    tIp6ScopeZoneInfo   Ip6ScopeZoneInfo;
    tIp6If             *pIf6 = NULL;
    UINT4               u4ContextId = 0;
    INT4                i4ZoneIndex = 0;
    UINT4               u4Mask = 0;

    MEMSET (&Ip6ScopeZoneInfo, 0, sizeof (tIp6ScopeZoneInfo));
    if (Ip6ifEntryExists (u4IfIndex) == IP6_FAILURE)
    {
        u4ContextId = VCM_INVALID_VC;
        return;
    }

    pIf6 = IP6_INTERFACE (u4IfIndex);
    if (pIf6 == NULL)
        return;

    u4ContextId = pIf6->pIp6Cxt->u4ContextId;

    IP6_TRC_ARG3 (u4ContextId, IP6_MOD_TRC, DATA_PATH_TRC, IP6_NAME,
                  "IP6ScopeZone: Ip6ScopeZoneDelete: Deleting Scope = %d "
                  "IF = %d zone-id = %d\n", u1Scope, u4IfIndex, i4ZoneId);

    /* Delete the scope-zone node from the interface */
    Ip6ZoneDeleteIfZoneNode (u4IfIndex, u1Scope);

    /* Also delete the node from the RBTree if its the last node */
    Ip6ZoneDeleteZoneNodeFromRbTree (u4IfIndex, u1Scope, i4ZoneId);

    if (((u1Scope == ADDR6_SCOPE_GLOBAL) &&
         ((TMO_SLL_Count (&pIf6->addr6Ilist) != 0) ||
          (pIf6->u4UnnumAssocIPv6If != 0))) ||
        ((u1Scope == ADDR6_SCOPE_LLOCAL) &&
         (TMO_SLL_Count (&pIf6->lla6Ilist) != 0)))
    {
        Ip6ZoneCreateAutoScopeZone (u4IfIndex, u1Scope);

    }
    Ip6ZoneSendNotifyOnNonGlobalZoneDeletion (u4Mask, u1Scope,
                                              i4ZoneIndex, u4IfIndex);
    u4Mask = NETIPV6_INTERFACE_UNMAPPED_FROM_ZONE;
    /*IP6ZoneCheckIfLastNonGlobalIsDeleted(u4IfIndex,u1Scope,i4ZoneIndex); */

    return;
}

 /************************************************************************
 * DESCRIPTION : This function deletes a scope-zone node from the interface
 *
 * INPUTS      : u4IfIndex - Interface Index from which the scope-zone
 *                           must be removed
 *               u1Scope - Scope for which the zone needs to be removed, 
 *               Zone-Id - Zone-Id of the scope-zone
 *
 * OUTPUTS     : None
 *
 * RETURNS     : None
 *
 * NOTES       : This function is added as a part of RFC4007 implementation
 ****************************************************************************/
VOID
Ip6ZoneDeleteIfZoneNode (UINT4 u4IfIndex, UINT1 u1Scope)
{
    tIp6IfZoneMapInfo  *pIp6IfZoneMapInfo = NULL;
    tIp6If             *pIf6 = NULL;
    UINT4               u4ContextId;

    if (Ip6ifEntryExists (u4IfIndex) == IP6_FAILURE)
    {
        u4ContextId = VCM_INVALID_VC;
        return;
    }

    pIf6 = IP6_INTERFACE (u4IfIndex);

    if (pIf6 == NULL)
        return;

    u4ContextId = pIf6->pIp6Cxt->u4ContextId;

    /* Retrieve the scope-zone node from the interface */
    pIp6IfZoneMapInfo = Ip6ZoneGetIfScopeZoneEntry (u4IfIndex, u1Scope);

    if (pIp6IfZoneMapInfo == NULL)
    {
        return;
    }

    /* Delete the node */
    TMO_SLL_Delete (&gIp6GblInfo.apIp6If[u4IfIndex]->zoneIlist,
                    &(pIp6IfZoneMapInfo->nextZone));

    /* Free the memory. */
    Ip6RelMem (u4ContextId, (UINT2) gIp6GblInfo.i4Ip6IfzonePoolId,
               (UINT1 *) pIp6IfZoneMapInfo);

    return;
}

/***********************************************************************
 * DESCRIPTION : This function deletes a scope-zone node from the RB Tre
 *               if no interface is associated with the scope-zone
 *
 * INPUTS      : Interface Index,
 *               Scope, 
 *               Zone-Id
 *
 * OUTPUTS     : None
 *
 * RETURNS     : None
 *
 * NOTES       : This function is added as a part of RFC4007 code changes
 **************************************************************************/
VOID
Ip6ZoneDeleteZoneNodeFromRbTree (UINT4 u4IfIndex, UINT1 u1Scope, INT4 i4ZoneId)
{
    tIp6ScopeZoneInfo   Ip6ScopeZoneInfo;
    tIp6ScopeZoneInfo  *pIp6ScopeZoneInfo = NULL;
    tIp6If             *pIf6 = NULL;
    UINT4               u4ContextId = 0;
    UINT4               u4IntfCount = 0;

    MEMSET (&Ip6ScopeZoneInfo, 0, sizeof (tIp6ScopeZoneInfo));

    if (Ip6ifEntryExists (u4IfIndex) == IP6_FAILURE)
    {
        u4ContextId = VCM_INVALID_VC;
        return;
    }

    pIf6 = IP6_INTERFACE (u4IfIndex);

    if (pIf6 == NULL)
        return;

    u4ContextId = pIf6->pIp6Cxt->u4ContextId;

    /* update the RBTree Scope-zone structure and retrieve the node */
    Ip6ScopeZoneInfo.i4ZoneId = i4ZoneId;
    Ip6ScopeZoneInfo.u4ContextId = u4ContextId;
    Ip6ScopeZoneInfo.u1Scope = u1Scope;

    pIp6ScopeZoneInfo = RBTreeGet (gIp6GblInfo.ScopeZoneTree,
                                   (tRBElem *) & Ip6ScopeZoneInfo);
    if (pIp6ScopeZoneInfo == NULL)
    {
        return;
    }
    OSIX_BITLIST_RESET_BIT (pIp6ScopeZoneInfo->InterfaceList,
                            u4IfIndex, sizeof (tInterfaceList));

    u4IntfCount = Ip6ZoneCheckIfLastInterface (pIp6ScopeZoneInfo, u4IfIndex);

    if (u4IntfCount == IP6_ZERO)
    {
        RBTreeRem (gIp6GblInfo.ScopeZoneTree, pIp6ScopeZoneInfo);
        Ip6RelMem (u4ContextId, (UINT2) gIp6GblInfo.i4Ip6ScopeZonePoolId,
                   (UINT1 *) pIp6ScopeZoneInfo);
        Ip6ZoneSetNonGlobalZoneCnt (u1Scope, u4ContextId, IP6_FALSE);
    }
    return;
}

/***********************************************************************
 * DESCRIPTION : This function deletes all the existing scope-zones
 *               on a particular interface, this is called when interface
 *               goes down or when the rowstatus is set to destroy
 *
 * INPUTS      : Interface Index
 *
 * OUTPUTS     : None
 *
 * RETURNS     : 
 *
 * NOTES       : This function is added as a part of RFC4007 code changes
 **************************************************************************/
VOID
Ip6ZoneDeleteExistingZoneOnIf (UINT4 u4IfIndex)
{
    tIp6IfZoneMapInfo  *pIp6ZoneMapInfo = NULL;
    UINT1               u1Scope = 0;

    for (u1Scope = ADDR6_SCOPE_INTLOCAL; u1Scope <= ADDR6_SCOPE_GLOBAL;
         u1Scope++)
    {
        pIp6ZoneMapInfo = Ip6ZoneGetIfScopeZoneEntry (u4IfIndex, u1Scope);
        if (pIp6ZoneMapInfo != NULL)
        {
            Ip6ScopeZoneDelete ((UINT4) u4IfIndex,
                                u1Scope, pIp6ZoneMapInfo->i4ZoneId);
        }

    }
}

/******************************************************************************
 * DESCRIPTION : This function gets a free scope-zone id
 *
 * INPUTS      : None
 *
 * OUTPUTS     : None
 *
 * RETURNS     :  Valid / Invalid Zone Id
 *
 * NOTES       : This function is added as a part of RFC4007 code changes
 ************************************************************************/
INT4
Ip6ZoneGetFreeZoneId (UINT4 u4ContextId, UINT1 u1Scope)
{
    tIp6Cxt            *pIp6Cxt = NULL;
    INT4                i4ZoneId = IP6_ZONE_INVALID;

    pIp6Cxt = gIp6GblInfo.apIp6Cxt[u4ContextId];

    if (pIp6Cxt == NULL)
        return IP6_ZONE_INVALID;

    if (u1Scope == ADDR6_SCOPE_LLOCAL)
    {
        for (i4ZoneId = 1; i4ZoneId <= IP6_MAX_ZONE_INTERFACE; i4ZoneId++)
        {
            if (pIp6Cxt->ai4Ip6LlocalZoneId[i4ZoneId - 1] == IP6_ZONE_INVALID)
            {
                pIp6Cxt->ai4Ip6LlocalZoneId[i4ZoneId - 1] = IP6_ZONE_VALID;
                break;
            }
        }
    }
    else if (u1Scope == ADDR6_SCOPE_INTLOCAL)
    {
        for (i4ZoneId = 1; i4ZoneId <= IP6_MAX_ZONE_INTERFACE; i4ZoneId++)
        {
            if (pIp6Cxt->ai4Ip6IntLocalZoneId[i4ZoneId - 1] == IP6_ZONE_INVALID)
            {
                pIp6Cxt->ai4Ip6IntLocalZoneId[i4ZoneId - 1] = IP6_ZONE_VALID;
                break;
            }
        }
    }
    if (i4ZoneId > (INT4) IP6_SCOPE_ZONE_COUNT)
    {
        return IP6_ZONE_INVALID;
    }
    return i4ZoneId;
}

/************************************************************************
 * DESCRIPTION : This function gets a free scope-zone id        *
 *                                    *
 * INPUTS      : None                            *
 *                                    *
 * OUTPUTS     : None                            *
 *                                    *
 * RETURNS     :  Valid / Invalid Zone Id                *
 *                                    *
 * NOTES       : This function is added as a part of RFC4007         *
 *               code changes                        *
 ************************************************************************/
INT4
Ip6ZoneGetFreeZoneIndex (VOID)
{

    tIp6IsScopeZoneValid *pIp6IsScopeZoneValid = NULL;

    TMO_SLL_Scan (&gIp6GblInfo.ScopeZoneList, pIp6IsScopeZoneValid,
                  tIp6IsScopeZoneValid *)
    {
        if (pIp6IsScopeZoneValid->u1Ip6IsZoneIndexValid == IP6_ZONE_INVALID)
        {
            pIp6IsScopeZoneValid->u1Ip6IsZoneIndexValid = IP6_ZONE_VALID;
            return pIp6IsScopeZoneValid->i4ZoneIndex;
        }
    }
    return IP6_ZONE_INVALID;
}

/************************************************************************
 * DESCRIPTION : This function checks if  free scope-zone index is      *
 *               available or not                                       *
 *                                                                      *
 * INPUTS      : None                                                   *
 *                                                                      *
 * OUTPUTS     : None                                                   *
 *                                                                      *
 * RETURNS     :  Valid / Invalid Zone Id                               *
 *                                                                      *
 * NOTES       : This function is added as a part of RFC4007            *
 *               code changes                                           *
 ************************************************************************/
INT4
Ip6ZoneCheckFreeIndexAvail (VOID)
{

    tIp6IsScopeZoneValid *pIp6IsScopeZoneValid = NULL;

    TMO_SLL_Scan (&gIp6GblInfo.ScopeZoneList, pIp6IsScopeZoneValid,
                  tIp6IsScopeZoneValid *)
    {
        if (pIp6IsScopeZoneValid->u1Ip6IsZoneIndexValid == IP6_ZONE_INVALID)
        {
            return IP6_ZONE_VALID;
        }
    }
    return IP6_ZONE_INVALID;
}

/************************************************************************
 * DESCRIPTION : This function resets the zone id once a link-local    *
 *               address which has been created by default is deleted    *
 *                                    *
 * INPUTS      : ZoneId                            *
 *               u4IfIndex - Interface Index                *
 *
 * OUTPUTS     : None                            *
 *                                    *
 * RETURNS     : None                            *
 *                                    *
 * NOTES       : This function is added as a part of RFC4007         *
 *               code changes                        *
 ************************************************************************/

VOID
Ip6ZoneReleaseLlocalZoneId (INT4 i4ZoneId, UINT4 u4IfIndex, UINT1 u1Scope)
{

    tIp6If             *pIf6 = NULL;
    tIp6Cxt            *pIp6Cxt = NULL;

    pIf6 = IP6_INTERFACE (u4IfIndex);

    if (pIf6 == NULL)
        return;

    pIp6Cxt = pIf6->pIp6Cxt;

    if (pIp6Cxt == NULL)
        return;

    if (i4ZoneId > IP6_MAX_ZONE_INTERFACE)
        return;

    if (u1Scope == ADDR6_SCOPE_LLOCAL)
    {
        pIp6Cxt->ai4Ip6LlocalZoneId[i4ZoneId - 1] = IP6_ZONE_INVALID;
    }
    else
    {
        pIp6Cxt->ai4Ip6IntLocalZoneId[i4ZoneId - 1] = IP6_ZONE_INVALID;
    }
    return;
}

/********************************************************************************
 * DESCRIPTION : This function resets the zone index                 *
 *                                        *
 * INPUTS      : ZoneIndex                            *
 *                                        *    
 * OUTPUTS     : None                                *
 *                                        *
 * RETURNS     : None                                *
 *                                        *
 * NOTES       : This function is added as a part of RFC4007 implementation     *
 ********************************************************************************/

VOID
Ip6ZoneReleaseZoneIndex (INT4 i4ZoneIndex)
{

    tIp6IsScopeZoneValid *pIp6IsScopeZoneValid = NULL;

    TMO_SLL_Scan (&gIp6GblInfo.ScopeZoneList, pIp6IsScopeZoneValid,
                  tIp6IsScopeZoneValid *)
    {
        if (pIp6IsScopeZoneValid->i4ZoneIndex == i4ZoneIndex)
        {
            pIp6IsScopeZoneValid->u1Ip6IsZoneIndexValid = IP6_ZONE_INVALID;
            return;
        }
    }
    return;
}

/********************************************************************************
 * DESCRIPTION : This function sets the zone id for interface local and llocal  *
 *             when they are created manually                    *
 *                                              *
 * INPUTS      : ZoneIndex                            *
 *                                        *    
 * OUTPUTS     : None                                *
 *                                        *
 * RETURNS     : None                                *
 *                                        *
 * NOTES       : This function is added as a part of RFC4007 implementation     *
 ********************************************************************************/
VOID
Ip6ZoneSetZoneIdforAutoZones (UINT4 u4IfIndex, INT4 i4ZoneId, UINT1 u1Scope)
{
    tIp6If             *pIf6 = NULL;
    tIp6Cxt            *pIp6Cxt = NULL;

    pIf6 = IP6_INTERFACE (u4IfIndex);

    if (pIf6 == NULL)
        return;

    pIp6Cxt = pIf6->pIp6Cxt;

    if (pIp6Cxt == NULL)
        return;
    if (u1Scope == ADDR6_SCOPE_LLOCAL)
    {
        pIp6Cxt->ai4Ip6LlocalZoneId[i4ZoneId - 1] = IP6_ZONE_VALID;
    }
    else
    {
        pIp6Cxt->ai4Ip6IntLocalZoneId[i4ZoneId - 1] = IP6_ZONE_VALID;
    }
    return;
}

/******************************************************************************
 * DESCRIPTION : This function finds which is the default scope-zone
 *               for a particular scope
 *
 * INPUTS      : u1scope,Contextid,ZoneId
 *
 * OUTPUTS     : None
 *
 * RETURNS     : IP6_ZONE_TRUE - if the zone is the default zone
 *               IP6_ZONE_FALSE - if the zone is a non-default zone
 *
 * NOTES       : This function is added as a part of RFC4007 code changes
 ************************************************************************/
UINT1
Ip6ZoneFindIfDefZoneCreatedForScope (UINT1 u1Scope, UINT4 u4ContextId)
{
    tIp6ScopeZoneInfo  *pIp6ScopeZoneInfo = NULL;
    tIp6ScopeZoneInfo   Ip6ScopeZoneInfo;
    INT4                i4ZoneId = 0;

    MEMSET (&Ip6ScopeZoneInfo, 0, sizeof (tIp6ScopeZoneInfo));
    Ip6ScopeZoneInfo.u1Scope = u1Scope;
    Ip6ScopeZoneInfo.u4ContextId = u4ContextId;
    Ip6ScopeZoneInfo.i4ZoneId = i4ZoneId;

    pIp6ScopeZoneInfo = RBTreeGetNext (gIp6GblInfo.ScopeZoneTree,
                                       (tRBElem *) & Ip6ScopeZoneInfo, NULL);
    if ((pIp6ScopeZoneInfo == NULL) || (pIp6ScopeZoneInfo->u1Scope > u1Scope))

    {
        return IP6_ZONE_TRUE;
    }
    return IP6_ZONE_FALSE;
}

/**************************************************************************
 * DESCRIPTION : This function Checks if the scope thats getting created 
 *               is the first non-global scope-zone in netipv6, if so it 
 *               sends initmation to Higher layer (i.e PIM)
 *
 * INPUTS      : u1scope
 *
 * OUTPUTS     : None
 *
 * RETURNS     : None 
 *
 * NOTES       : This function is added as a part of RFC4007 code changes
 *****************************************************************************/
VOID
Ip6ZoneSendNotifyOnNonGlobalZoneCreation (UINT4 u4Mask, UINT1 u1Scope,
                                          INT4 i4ZoneIndex, UINT4 u4IfIndex)
{
    tIp6Cxt            *pIp6Cxt = NULL;
    UINT4               u4ContextId = VCM_INVALID_VC;

    if (VCM_FAILURE == VcmGetContextIdFromCfaIfIndex (u4IfIndex, &u4ContextId))
        return;

    pIp6Cxt = gIp6GblInfo.apIp6Cxt[u4ContextId];

    if ((u1Scope > ADDR6_SCOPE_LLOCAL) && (u1Scope < ADDR6_SCOPE_GLOBAL))
    {
        if (pIp6Cxt->u4MaxNonGblZoneCount == IP6_FIRST_NON_GLOBAL_ZONE_CREATED)
        {
            NetIpv6InvokeNonGlobalZoneChange (NETIPV6_FIRST_NON_GLOBAL_ZONE_ADD,
                                              i4ZoneIndex, u4IfIndex);
            return;
        }

        NetIpv6InvokeNonGlobalZoneChange (u4Mask, i4ZoneIndex, u4IfIndex);
    }
    return;
}

/*******************************************************************************
 * DESCRIPTION : This function Checks if the last non-global scope zone is
 *               deleted or not, if it is deleted then the zone deletion 
 *               indication is initmated to Highler layer (i.e PIM) and also 
 *               the state of i4IsFirstNonGlobalZone is reset to 
 *               IP6_FIRST_NON_GLOBAL_NOT_CREATED.
 *
 * INPUTS      : u1scope
 *
 * OUTPUTS     : None
 *
 * RETURNS     : None
 *
 * NOTES       : This function is added as a part of RFC4007 code changes
 *************************************************************************************/
VOID
Ip6ZoneSendNotifyOnNonGlobalZoneDeletion (UINT1 u4Mask, UINT1 u1Scope,
                                          INT4 i4ZoneIndex, UINT4 u4IfIndex)
{
    tIp6If             *pIf6 = NULL;
    tIp6Cxt            *pIp6Cxt = NULL;

    pIf6 = IP6_INTERFACE (u4IfIndex);

    if (pIf6 == NULL)
        return;

    pIp6Cxt = pIf6->pIp6Cxt;

    if ((u1Scope == ADDR6_SCOPE_GLOBAL) ||
        (u1Scope == ADDR6_SCOPE_INTLOCAL) || (u1Scope == ADDR6_SCOPE_LLOCAL))
    {
        return;
    }

    if (pIp6Cxt->u4MaxNonGblZoneCount == IP6_LAST_NON_GLOBAL_ZONE_DELETED)
    {
        NetIpv6InvokeNonGlobalZoneChange (NETIPV6_LAST_NON_GLOBAL_ZONE_DEL,
                                          i4ZoneIndex, u4IfIndex);

    }
    NetIpv6InvokeNonGlobalZoneChange (u4Mask, i4ZoneIndex, u4IfIndex);

    return;
}

/***********************************************************************************
 * DESCRIPTION : This function takes corresponding action on receving 
 *               onlink notification from OSPF
 *
 * INPUTS      : u4LinkId, aStandByIfList, u1Count, u1Operation
 *
 * OUTPUTS     : None
 *
 * RETURNS     : None
 *
 * NOTES       : This function is added as a part of RFC4007 code changes
 *************************************************************************************/

VOID
Ip6ZoneOnLinkNotification (tIp6MultIfToIp6Link * pIp6IfOverLinkInfo)
{

    switch (pIp6IfOverLinkInfo->u2LinkOperation)
    {
        case IPV6_ACTIVE_ADD:
            Ip6ZoneCreateZoneForOnLinkIf (pIp6IfOverLinkInfo);
            break;
        case IPV6_ACTIVE_DELETE:
            Ip6ZoneDelLlocalZoneForOnLinkIf (pIp6IfOverLinkInfo);
            break;
        case IPV6_STANDBY_MODIFY:
            Ip6ZoneDelLlocalZoneForOnLinkIf (pIp6IfOverLinkInfo);
            break;
        default:
            return;
    }
    return;

}

/***************************************************************************
 *DESCRIPTION  : This function on receiving link-notification event from
 *               OSPF creates the zone that are present in active in
 *               all the standby interfaces also it deletes the other
 *               scope-zones that are present in standby interfaces.
 *
 * INPUTS      : u4LinkId - Active interface index, 
 *               aStandByIfList - List of standby interfaces,
 *               u1Count - Count of standby interfaces
 *
 * OUTPUTS     : None
 *
 * RETURNS     : None
 *
 * NOTES       : This function is added as a part of RFC4007 code changes
 *****************************************************************************/
VOID
Ip6ZoneCreateZoneForOnLinkIf (tIp6MultIfToIp6Link * pIp6IfOverLinkInfo)
{
    tIp6IfZoneMapInfo  *pActiveIfZoneMapInfo = NULL;
    tIp6IfZoneMapInfo  *pStandbyIfZoneMapInfo = NULL;
    tIp6If             *pIf6 = NULL;
    tTMO_SLL_NODE      *pZoneNode = NULL;
    INT4                i4ActiveIfZoneId = 0;
    UINT4               u4StandbyIf = 0;
    UINT4               u4StandbyIfZoneId = 0;
    UINT2               u2Count = 0;
    UINT1               u1ActiveIfScope = 0;
    UINT1               u1StandbyIfScope = 0;
    UINT1               u1CreationStatus = IP6_ZONE_TYPE_OVERRIDDEN;
    UINT1               au1ScopeOnActive[IP6_MAX_SCOPE_ZONE_TYPES];

    MEMSET (au1ScopeOnActive, 0, IP6_MAX_SCOPE_ZONE_TYPES);

    if (IP6_FAILURE ==
        Ip6ifEntryExists (pIp6IfOverLinkInfo->u4OnLinkActiveIfId))
    {
        return;
    }
    pIf6 = (tIp6If *) IP6_INTERFACE (pIp6IfOverLinkInfo->u4OnLinkActiveIfId);

    /* Scan the active interface zone list and find the
       scope zones that are created on this interface and
       create the same scope zone for the standby interface
       also */
    TMO_SLL_Scan (&pIf6->zoneIlist, pZoneNode, tTMO_SLL_NODE *)
    {
        pActiveIfZoneMapInfo = IP6_SCOPE_ZONE_PTR_FROM_SLL (pZoneNode);

        u1ActiveIfScope = pActiveIfZoneMapInfo->u1Scope;
        i4ActiveIfZoneId = pActiveIfZoneMapInfo->i4ZoneId;
        /* Take a note of the scopes that are created in the
           active so that the scopes that are created other than
           the marked ones in the stanby are deleted */
        au1ScopeOnActive[u1ActiveIfScope] = IP6_TRUE;
        if (u1ActiveIfScope != ADDR6_SCOPE_INTLOCAL)
        {
            for (u2Count = 0; u2Count < pIp6IfOverLinkInfo->u2OnLinkIfCount;
                 u2Count++)
            {
                u4StandbyIf = pIp6IfOverLinkInfo->apIp6If[u2Count];

                if (u4StandbyIf != pIp6IfOverLinkInfo->u4OnLinkActiveIfId)
                {
                    if (Ip6ifEntryExists (u4StandbyIf) == IP6_FAILURE)
                    {
                        continue;
                    }
                    /* create the scopezone on active interface
                       in each of the standby interface */
                    Ip6ZoneReplaceZoneOnStandbyIf (u4StandbyIf,
                                                   u1ActiveIfScope,
                                                   u1CreationStatus,
                                                   i4ActiveIfZoneId);
                }
            }
        }
    }
    /* Delete the other scope-zones configured on standby &
       which is not there in active */
    for (u2Count = 0; u2Count < pIp6IfOverLinkInfo->u2OnLinkIfCount; u2Count++)
    {
        u4StandbyIf = pIp6IfOverLinkInfo->apIp6If[u2Count];

        pIf6 = (tIp6If *) IP6_INTERFACE (u4StandbyIf);

        if (pIf6 == NULL)
            continue;

        if (u4StandbyIf == pIp6IfOverLinkInfo->u4OnLinkActiveIfId)
            continue;

        TMO_SLL_Scan (&pIf6->zoneIlist, pZoneNode, tTMO_SLL_NODE *)
        {
            pStandbyIfZoneMapInfo = IP6_SCOPE_ZONE_PTR_FROM_SLL (pZoneNode);

            u1StandbyIfScope = pStandbyIfZoneMapInfo->u1Scope;
            u4StandbyIfZoneId = pStandbyIfZoneMapInfo->i4ZoneId;
            /* Check if this scope is available on active interface */
            if (au1ScopeOnActive[u1StandbyIfScope] != IP6_TRUE)
            {
                Ip6ScopeZoneDelete (pIf6->u4Index, u1StandbyIfScope,
                                    u4StandbyIfZoneId);
            }
        }
    }
    return;
}

/*******************************************************************************
 * DESCRIPTION : This function on receiving On link modification notification 
 *               from OSPF puts all the interfaces received in the interface list 
 *               into same linklocal scopezone
 *
 * INPUTS      : u4LinkId - Active interface index
 *               aStandByIfList - List of standby interfaces, 
 *               u1Count - No of standby interfaces
 *
 * OUTPUTS     : None
 *
 * RETURNS     : None
 *
 * NOTES       : This function is added as a part of RFC4007 code changes
 *****************************************************************************/
VOID
Ip6ZoneDelLlocalZoneForOnLinkIf (tIp6MultIfToIp6Link * pIp6IfOverLinkInfo)
{
    tIp6IfZoneMapInfo  *pStandbyIfZoneMapInfo = NULL;
    tIp6If             *pIf6 = NULL;
    INT4                i4ZoneId = 0;
    UINT4               u4StandbyIf = 0;
    UINT2               u2Count = 0;
    UINT1               u1Scope = ADDR6_SCOPE_LLOCAL;

    for (u2Count = 0; u2Count < pIp6IfOverLinkInfo->u2OnLinkIfCount; u2Count++)
    {
        u4StandbyIf = pIp6IfOverLinkInfo->apIp6If[u2Count];
        pIf6 = (tIp6If *) IP6_INTERFACE (u4StandbyIf);
        if (pIf6 == NULL)
        {
            continue;
        }
        if (u4StandbyIf == pIp6IfOverLinkInfo->u4OnLinkActiveIfId)
            continue;

        pStandbyIfZoneMapInfo =
            Ip6ZoneGetIfScopeZoneEntry (pIf6->u4Index, u1Scope);

        if (pStandbyIfZoneMapInfo == NULL)
            continue;

        i4ZoneId = pStandbyIfZoneMapInfo->i4ZoneId;
        Ip6ScopeZoneDelete (u4StandbyIf, u1Scope, i4ZoneId);
    }
    return;
}

/* RFC 4007 Scoped Address changes End */
/***************************** END OF FILE **********************************/
