/********************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: nd6red.c,v 1.18 2015/07/15 12:13:15 siva Exp $ 
 *
 * Description: This file contains ND Redundancy related routines
 *           
 *******************************************************************/
#ifndef __ND6_RED_C
#define __ND6_RED_C

#include "ip6inc.h"
#include "nd6red.h"
#include "ip6cli.h"

extern tOsixSemId gNd6SemId;
/***********   GLOBAL VARIABLES  ************/

/************************************************************************/
/*  Function Name   : Nd6RedInitGlobalInfo                              */
/*                                                                      */
/*  Description     : This function is invoked by the IPV6 module to    */
/*                    register itself with the RM module. This          */
/*                    registration is required to send/receive peer     */
/*                    synchronization messages and control events       */
/*                                                                      */
/*  Input(s)        : None.                                             */
/*                                                                      */
/*  Output(s)       : None.                                             */
/*                                                                      */
/*  Returns         : OSIX_SUCCESS / OSIX_FAILURE                       */
/************************************************************************/

PUBLIC INT4
Nd6RedInitGlobalInfo (VOID)
{
    tRmRegParams        RmRegParams;

    IP6_GBL_TRC_ARG (ND6_MOD_TRC, BUFFER_TRC, ND6_NAME,
                     "ND6: Entering Nd6RedInitGlobalInfo \n");

    MEMSET (&RmRegParams, 0, sizeof (tRmRegParams));
    RmRegParams.u4EntId = RM_ND6_APP_ID;
    RmRegParams.pFnRcvPkt = Nd6RedRmCallBack;

    if (Nd6RedRmRegisterProtocols (&RmRegParams) == OSIX_FAILURE)
    {
        IP6_GBL_TRC_ARG (ND6_MOD_TRC, ALL_FAILURE_TRC | BUFFER_TRC, ND6_NAME,
                         "Nd6RedInitGlobalInfo: Registration with RM failed \n");
        return OSIX_FAILURE;
    }
    ND6_GET_NODE_STATUS () = RM_INIT;
    ND6_NUM_STANDBY_NODES () = 0;
    gNd6RedGlobalInfo.bBulkReqRcvd = OSIX_FALSE;

    IP6_GBL_TRC_ARG (ND6_MOD_TRC, BUFFER_TRC, ND6_NAME,
                     "ND6: Exiting Nd6RedInitGlobalInfo \n");
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : Nd6RedRmRegisterProtocols                         */
/*                                                                      */
/*  Description     : This function is invoked by the IPV6 module to    */
/*                    register itself with the RM module. This          */
/*                    registration is required to send/receive peer     */
/*                    synchronization messages and control events       */
/*                                                                      */
/*  Input(s)        : pRmReg -- Pointer to structure tRmRegParams       */
/*                                                                      */
/*  Output(s)       : None.                                             */
/*                                                                      */
/*  Returns         : OSIX_SUCCESS / OSIX_FAILURE                       */
/************************************************************************/

PUBLIC INT4
Nd6RedRmRegisterProtocols (tRmRegParams * pRmReg)
{
    IP6_GBL_TRC_ARG (ND6_MOD_TRC, BUFFER_TRC, ND6_NAME,
                     "ND6: Entering Nd6RedRmRegisterProtocols \n");
    if (RmRegisterProtocols (pRmReg) == RM_FAILURE)
    {
        IP6_GBL_TRC_ARG (ND6_MOD_TRC, ALL_FAILURE_TRC | BUFFER_TRC, ND6_NAME,
                         "Nd6RedRmRegisterProtocols: Registration with RM failed \n");
        return OSIX_FAILURE;
    }
    IP6_GBL_TRC_ARG (ND6_MOD_TRC, BUFFER_TRC, ND6_NAME,
                     "ND6: Exiting Nd6RedRmRegisterProtocols \n");
    return OSIX_SUCCESS;
}

/************************************************************************/
/* Function Name      : Nd6RedDeInitGlobalInfo                          */
/*                                                                      */
/* Description        : This function is invoked by the IPV6 module     */
/*                      during module shutdown and this function        */
/*                      deinitializes the redundancy global variables   */
/*                      and de-register IPV6 with RM.                   */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/************************************************************************/

INT4
Nd6RedDeInitGlobalInfo (VOID)
{
    IP6_GBL_TRC_ARG (ND6_MOD_TRC, BUFFER_TRC, ND6_NAME,
                     "Entering Nd6RedDeInitGlobalInfo \r\n");
    if (Nd6RedRmDeRegisterProtocols () == OSIX_FAILURE)
    {
        IP6_GBL_TRC_ARG (ND6_MOD_TRC, ALL_FAILURE_TRC | BUFFER_TRC, ND6_NAME,
                         "Nd6RedDeInitGlobalInfo: De-Registration with RM failed \n");
        return OSIX_FAILURE;
    }
    IP6_GBL_TRC_ARG (ND6_MOD_TRC, BUFFER_TRC, ND6_NAME,
                     "Exiting Nd6RedDeInitGlobalInfo \r\n");
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : Nd6RmDeRegisterProtocols                          */
/*                                                                      */
/*  Description     : This function is invoked by the IPV6 module to    */
/*                    de-register itself with the RM module.            */
/*                                                                      */
/*  Input(s)        : None.                                             */
/*                                                                      */
/*  Output(s)       : None.                                             */
/*                                                                      */
/*  Returns         : OSIX_SUCCESS / OSIX_FAILURE                       */
/************************************************************************/

PUBLIC INT4
Nd6RedRmDeRegisterProtocols (VOID)
{
    IP6_GBL_TRC_ARG (ND6_MOD_TRC, BUFFER_TRC, ND6_NAME,
                     "ND6: Entering Nd6RedRmDeRegisterProtocols \n");
    if (RmDeRegisterProtocols (RM_ND6_APP_ID) == RM_FAILURE)
    {
        IP6_GBL_TRC_ARG (ND6_MOD_TRC, ALL_FAILURE_TRC | BUFFER_TRC, ND6_NAME,
                         "Nd6RedRmDeRegisterProtocols: De-Registration with RM failed \n");
        return OSIX_FAILURE;
    }
    IP6_GBL_TRC_ARG (ND6_MOD_TRC, BUFFER_TRC, ND6_NAME,
                     "ND6: Exiting Nd6RedRmDeRegisterProtocols \n");
    return OSIX_SUCCESS;
}

/************************************************************************/
/* Function Name      : Nd6RedRmCallBack                                */
/*                                                                      */
/* Description        : This function will be invoked by the IPV6 module*/
/*                      to enque events and post messages to ND         */
/*                                                                      */
/* Input(s)           : u1Event   - Event type given by RM module       */
/*                      pData     - RM Message to enqueue               */
/*                      u2DataLen - Message size                        */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/************************************************************************/

PUBLIC INT4
Nd6RedRmCallBack (UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen)
{
    tNd6RmMsg          *pMsg = NULL;

    IP6_GBL_TRC_ARG (ND6_MOD_TRC, BUFFER_TRC, ND6_NAME,
                     "Entering Nd6RedRmCallBack \n");

    if ((u1Event != RM_MESSAGE) &&
        (u1Event != GO_ACTIVE) &&
        (u1Event != GO_STANDBY) &&
        (u1Event != RM_STANDBY_UP) &&
        (u1Event != RM_STANDBY_DOWN) &&
        (u1Event != L2_INITIATE_BULK_UPDATES) &&
        (u1Event != RM_CONFIG_RESTORE_COMPLETE) &&
        (u1Event != RM_DYNAMIC_SYNCH_AUDIT))
    {
        IP6_GBL_TRC_ARG (ND6_MOD_TRC, ALL_FAILURE_TRC | BUFFER_TRC, ND6_NAME,
                         "Nd6RedRmCallBack: This event is not associated with RM \r \n");
        return OSIX_FAILURE;
    }

    if (((u1Event == RM_MESSAGE) || (u1Event == RM_STANDBY_UP) ||
         (u1Event == RM_STANDBY_DOWN)) && (pData == NULL))
    {
        IP6_GBL_TRC_ARG (ND6_MOD_TRC, ALL_FAILURE_TRC | BUFFER_TRC, ND6_NAME,
                         "Nd6RedRmCallBack: Queue Message associated with the event"
                         "is not sent by RM \r \n");
        return OSIX_FAILURE;
    }

    if ((pMsg =
         (tNd6RmMsg *) MemAllocMemBlk (gIp6GblInfo.i4Ip6Nd6PoolId)) == NULL)
    {
        IP6_GBL_TRC_ARG (ND6_MOD_TRC, ALL_FAILURE_TRC | BUFFER_TRC, ND6_NAME,
                         "Nd6RedRmCallBack: Queue message allocation failure \r \n");
        if (u1Event == RM_MESSAGE)
        {
            /* RM CRU Buffer Memory Release */
            RM_FREE (pData);
        }
        else if ((u1Event == RM_STANDBY_UP) || (u1Event == RM_STANDBY_DOWN))
        {
            /* Call RM API to release memory of data of these events */
            Nd6RedRmReleaseMemoryForMsg ((UINT1 *) pData);
        }
        return OSIX_FAILURE;
    }
    MEMSET (pMsg, 0, sizeof (tNd6RmMsg));

    pMsg->RmCtrlMsg.pData = pData;
    pMsg->RmCtrlMsg.u1Event = u1Event;
    pMsg->RmCtrlMsg.u2DataLen = u2DataLen;

    if (OsixSendToQ (SELF, (const UINT1 *) IP6_RM_PKT_QUEUE,
                     (tOsixMsg *) pMsg, OSIX_MSG_NORMAL) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (gIp6GblInfo.i4Ip6Nd6PoolId, (UINT1 *) pMsg);

        if (u1Event == RM_MESSAGE)
        {
            /* RM CRU Buffer Memory Release */
            RM_FREE (pData);
        }
        else if ((u1Event == RM_STANDBY_UP) || (u1Event == RM_STANDBY_DOWN))
        {
            /* Call RM API to release memory of data of these events */
            RmReleaseMemoryForMsg ((UINT1 *) pData);
        }
        IP6_GBL_TRC_ARG (ND6_MOD_TRC, ALL_FAILURE_TRC | BUFFER_TRC, ND6_NAME,
                         "Nd6RedRmCallBack: Queue send failure \r \n");
        return OSIX_FAILURE;
    }

    OsixSendEvent (SELF, (const UINT1 *) IP6_TASK_NAME, IP6_RM_PKT_EVENT);

    IP6_GBL_TRC_ARG (ND6_MOD_TRC, BUFFER_TRC, ND6_NAME,
                     "Exiting Nd6RedRmCallBack \n");
    return OSIX_SUCCESS;
}

/************************************************************************/
/* Function Name      : Nd6RedHandleRmEvents                            */
/*                                                                      */
/* Description        : This function is invoked by the IPV6 module to  */
/*                      process all the events and messages posted by   */
/*                      the RM module                                   */
/*                                                                      */
/* Input(s)           : pMsg -- Pointer to the ND Q Msg                 */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
Nd6RedHandleRmEvents ()
{
    tNd6RmMsg          *pMsg = NULL;
    tRmNodeInfo        *pData = NULL;
    tRmProtoAck         ProtoAck;
    tRmProtoEvt         ProtoEvt;
    UINT4               u4SeqNum = 0;

    IP6_GBL_TRC_ARG (ND6_MOD_TRC, BUFFER_TRC, ND6_NAME,
                     "Entering Nd6RedHandleRmEvents \n");
    MEMSET (&ProtoAck, 0, sizeof (tRmProtoAck));
    MEMSET (&ProtoEvt, 0, sizeof (tRmProtoEvt));

    while (OsixReceiveFromQ (SELF, (const UINT1 *) IP6_RM_PKT_QUEUE,
                             OSIX_NO_WAIT, 0,
                             (tOsixMsg **) (VOID *) &pMsg) == OSIX_SUCCESS)
    {
        switch (pMsg->RmCtrlMsg.u1Event)
        {
            case GO_ACTIVE:
                IP6_GBL_TRC_ARG (ND6_MOD_TRC, BUFFER_TRC, ND6_NAME,
                                 "Nd6RedHandleRmEvents: Received GO_ACTIVE event \r\n");
                Nd6RedHandleGoActive ();
                break;
            case GO_STANDBY:
                IP6_GBL_TRC_ARG (ND6_MOD_TRC, BUFFER_TRC, ND6_NAME,
                                 "Nd6RedHandleRmEvents: Received GO_STANDBY event \r\n");
                Nd6RedHandleGoStandby ();
                break;
            case RM_STANDBY_UP:
                IP6_GBL_TRC_ARG (ND6_MOD_TRC, BUFFER_TRC, ND6_NAME,
                                 "Nd6RedHandleRmEvents: Received "
                                 "RM_STANDBY_UP event \r\n");
                pData = (tRmNodeInfo *) pMsg->RmCtrlMsg.pData;
                ND6_NUM_STANDBY_NODES () = pData->u1NumStandby;
                Nd6RedRmReleaseMemoryForMsg ((UINT1 *) pData);
                if (ND6_RM_BULK_REQ_RCVD () == OSIX_TRUE)
                {
                    ND6_RM_BULK_REQ_RCVD () = OSIX_FALSE;
                    gNd6RedGlobalInfo.u1BulkUpdStatus = ND6_HA_UPD_NOT_STARTED;
                    Nd6RedSendBulkUpdMsg ();
                }
                break;
            case RM_STANDBY_DOWN:
                IP6_GBL_TRC_ARG (ND6_MOD_TRC, BUFFER_TRC, ND6_NAME,
                                 "Nd6RedHandleRmEvents: Received RM_STANDBY_DOWN event \r\n");
                pData = (tRmNodeInfo *) pMsg->RmCtrlMsg.pData;
                ND6_NUM_STANDBY_NODES () = pData->u1NumStandby;
                Nd6RedRmReleaseMemoryForMsg ((UINT1 *) pData);
                break;
            case RM_MESSAGE:
                /* Read the sequence number from the RM Header */
                RM_PKT_GET_SEQNUM (pMsg->RmCtrlMsg.pData, &u4SeqNum);
                /* Remove the RM Header */
                RM_STRIP_OFF_RM_HDR (pMsg->RmCtrlMsg.pData,
                                     pMsg->RmCtrlMsg.u2DataLen);
                IP6_GBL_TRC_ARG (ND6_MOD_TRC, BUFFER_TRC, ND6_NAME,
                                 "Nd6RedHandleRmEvents: Received RM_MESSAGE event \r\n");
                ProtoAck.u4AppId = RM_ND6_APP_ID;
                ProtoAck.u4SeqNumber = u4SeqNum;

                if (gNd6RedGlobalInfo.u1NodeStatus == RM_ACTIVE)
                {
                    Nd6RedProcessPeerMsgAtActive (pMsg->RmCtrlMsg.pData,
                                                  pMsg->RmCtrlMsg.u2DataLen);
                }
                else if (gNd6RedGlobalInfo.u1NodeStatus == RM_STANDBY)
                {
                    Nd6RedProcessPeerMsgAtStandby (pMsg->RmCtrlMsg.pData,
                                                   pMsg->RmCtrlMsg.u2DataLen);
                }
                else
                {
                    IP6_GBL_TRC_ARG (ND6_MOD_TRC, ALL_FAILURE_TRC | BUFFER_TRC,
                                     ND6_NAME, "Nd6RedHandleRmEvents: Sync-up"
                                     "message received at Idle Node!!\r \n");
                }

                RM_FREE (pMsg->RmCtrlMsg.pData);
                RmApiSendProtoAckToRM (&ProtoAck);
                break;
            case RM_CONFIG_RESTORE_COMPLETE:
                if (gNd6RedGlobalInfo.u1NodeStatus == RM_INIT)
                {
                    if (ND6_GET_RMNODE_STATUS () == RM_STANDBY)
                    {
                        Nd6RedHandleIdleToStandby ();

                        ProtoEvt.u4Event = RM_STANDBY_EVT_PROCESSED;

                        if (RmApiHandleProtocolEvent (&ProtoEvt) == RM_FAILURE)
                        {
                            IP6_GBL_TRC_ARG (ND6_MOD_TRC, ALL_FAILURE_TRC |
                                             BUFFER_TRC, ND6_NAME,
                                             "Nd6RedHandleRmEvents:"
                                             " Acknowledgement to RM for GO_STANDBY event"
                                             " failed!!\r \n");
                        }
                    }
                }
                break;
            case L2_INITIATE_BULK_UPDATES:
                IP6_GBL_TRC_ARG (ND6_MOD_TRC, BUFFER_TRC, ND6_NAME,
                                 "Nd6RedHandleRmEvents: Received L2_INITIATE_BULK_UPDATES"
                                 "event \r\n");
                Nd6RedSendBulkReqMsg ();
                break;
            case RM_DYNAMIC_SYNCH_AUDIT:
                nd6RedHandleDynSyncAudit ();
                break;

            default:
                IP6_GBL_TRC_ARG (ND6_MOD_TRC, ALL_FAILURE_TRC |
                                 BUFFER_TRC, ND6_NAME, "Nd6RedHandleRmEvents:"
                                 " Invalid RM event received failed!!\r \n");
                break;

        }
        MemReleaseMemBlock (gIp6GblInfo.i4Ip6Nd6PoolId, (UINT1 *) pMsg);
    }
    IP6_GBL_TRC_ARG (ND6_MOD_TRC, BUFFER_TRC, ND6_NAME,
                     "Exiting Nd6RedHandleRmEvents \r\n");
    return;
}

/************************************************************************/
/* Function Name      : Nd6RedHandleGoActive                            */
/*                                                                      */
/* Description        : This function is invoked by the IPV6 module upon*/
/*                      receiving the GO_ACTIVE indication from RM      */
/*                      module. And this function responds to RM with an*/
/*                      acknowledgement.                                */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
Nd6RedHandleGoActive (VOID)
{
    tRmProtoEvt         ProtoEvt;

    IP6_GBL_TRC_ARG (ND6_MOD_TRC, BUFFER_TRC, ND6_NAME,
                     "Entering Nd6RedHandleGoActive \r\n");
    ProtoEvt.u4AppId = RM_ND6_APP_ID;
    ProtoEvt.u4Error = RM_NONE;

    gNd6RedGlobalInfo.u1BulkUpdStatus = ND6_HA_UPD_NOT_STARTED;

    if (ND6_GET_NODE_STATUS () == RM_ACTIVE)
    {
        IP6_GBL_TRC_ARG (ND6_MOD_TRC, BUFFER_TRC, ND6_NAME,
                         "Nd6RedHandleGoActive: GO_ACTIVE event reached"
                         "when node is already active!\r\n");
        return;
    }
    if (ND6_GET_NODE_STATUS () == RM_INIT)
    {
        IP6_GBL_TRC_ARG (ND6_MOD_TRC, BUFFER_TRC, ND6_NAME,
                         "Nd6RedHandleGoActive: Idle to Active transition..."
                         "\r\n");
        Nd6RedHandleIdleToActive ();
        ProtoEvt.u4Event = RM_IDLE_TO_ACTIVE_EVT_PROCESSED;
    }
    if (ND6_GET_NODE_STATUS () == RM_STANDBY)
    {
        IP6_GBL_TRC_ARG (ND6_MOD_TRC, BUFFER_TRC, ND6_NAME,
                         "Nd6RedHandleGoActive: Standby to Active transition..."
                         "\r\n");
        Nd6RedHandleStandbyToActive ();
        ProtoEvt.u4Event = RM_STANDBY_TO_ACTIVE_EVT_PROCESSED;
    }
    if (ND6_RM_BULK_REQ_RCVD () == OSIX_TRUE)
    {
        ND6_RM_BULK_REQ_RCVD () = OSIX_FALSE;
        gNd6RedGlobalInfo.u1BulkUpdStatus = ND6_HA_UPD_NOT_STARTED;
        Nd6RedSendBulkUpdMsg ();
    }
    if (RmApiHandleProtocolEvent (&ProtoEvt) == RM_FAILURE)
    {
        IP6_GBL_TRC_ARG (ND6_MOD_TRC, ALL_FAILURE_TRC |
                         BUFFER_TRC, ND6_NAME,
                         "Nd6RedHandleGoActive: Acknowledgement"
                         "to RM for GO_ACTIVE event failed!\r \n");
    }
    IP6_GBL_TRC_ARG (ND6_MOD_TRC, BUFFER_TRC, ND6_NAME,
                     "Exiting Nd6RedHandleGoActive \r\n");
    return;
}

/************************************************************************/
/* Function Name      : Nd6RedHandleGoStandby                           */
/*                                                                      */
/* Description        : This function is invoked by the IPV6 module upon*/
/*                      receiving the GO_STANDBY indication from RM     */
/*                      module. And this function responds to RM module */
/*                      with an acknowledgement.                        */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
Nd6RedHandleGoStandby (VOID)
{
    tRmProtoEvt         ProtoEvt;
    IP6_GBL_TRC_ARG (ND6_MOD_TRC, BUFFER_TRC, ND6_NAME,
                     "Entering Nd6RedHandleGoStandby \r\n");

    ProtoEvt.u4AppId = RM_ND6_APP_ID;
    ProtoEvt.u4Error = RM_NONE;

    gNd6RedGlobalInfo.u1BulkUpdStatus = ND6_HA_UPD_NOT_STARTED;

    if (ND6_GET_NODE_STATUS () == RM_STANDBY)
    {
        IP6_GBL_TRC_ARG (ND6_MOD_TRC, BUFFER_TRC, ND6_NAME,
                         "Nd6RedHandleGoStandby: GO_STANDBY event reached"
                         "when node is already in standby \r\n");
        return;
    }
    if (ND6_GET_NODE_STATUS () == RM_INIT)
    {
        IP6_GBL_TRC_ARG (ND6_MOD_TRC, BUFFER_TRC, ND6_NAME,
                         "Nd6RedHandleGoStandby: GO_STANDBY event reached"
                         "when node is already idle \r\n");

        /* GO_STANDBY event is not processed here. It is done when
         * CONFIG_RESTORE_COMPLETE event is received. Since static bulk
         * update will be completed only by then, the acknowledgement can be
         * sent during CONFIG_RESTORE_COMPLETE handling, which will trigger RM
         * to send dynamic bulk update event to modules.
         */

        return;
    }
    else if (ND6_GET_NODE_STATUS () == ACTIVE)
    {
        IP6_GBL_TRC_ARG (ND6_MOD_TRC, BUFFER_TRC, ND6_NAME,
                         "Nd6RedHandleGoStandby: Active to Standby transition.."
                         "\r\n");
        Nd6RedHandleActiveToStandby ();
        ProtoEvt.u4Event = RM_STANDBY_EVT_PROCESSED;

        if (RmApiHandleProtocolEvent (&ProtoEvt) == RM_FAILURE)
        {
            IP6_GBL_TRC_ARG (ND6_MOD_TRC, ALL_FAILURE_TRC |
                             BUFFER_TRC, ND6_NAME,
                             "Nd6RedHandleGoStandby: Acknowledgement"
                             "to RM for GO_STANDBY event failed!\r \n");
        }
    }

    IP6_GBL_TRC_ARG (ND6_MOD_TRC, BUFFER_TRC, ND6_NAME,
                     "Exiting Nd6RedHandleGoStandby \r\n");
    return;
}

/************************************************************************/
/* Function Name      : Nd6RedHandleIdleToActive                        */
/*                                                                      */
/* Description        : This function updates the node status to ACTIVE */
/*                      on transition from Idle to Active and also      */
/*                      updates the number of Standby node count.       */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
Nd6RedHandleIdleToActive (VOID)
{
    IP6_GBL_TRC_ARG (ND6_MOD_TRC, BUFFER_TRC, ND6_NAME,
                     "Entering Nd6RedHandleIdleToActive \r\n");
    ND6_GET_NODE_STATUS () = RM_ACTIVE;
    ND6_RM_GET_NUM_STANDBY_NODES_UP ();
    IP6_GBL_TRC_ARG (ND6_MOD_TRC, BUFFER_TRC, ND6_NAME,
                     "Nd6RedHandleIdleToActive: Node Status Idle to Active \r\n");
    IP6_GBL_TRC_ARG (ND6_MOD_TRC, BUFFER_TRC, ND6_NAME,
                     "Exiting Nd6RedHandleIdleToActive \r\n");
    return;
}

/************************************************************************/
/* Function Name      : Nd6RedHandleIdleToStandby                       */
/*                                                                      */
/* Description        : This function updates the node status to STANDBY*/
/*                      on transition from Idle to Standby and also     */
/*                      updates the number of Standby node count to 0   */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
Nd6RedHandleIdleToStandby (VOID)
{
    IP6_GBL_TRC_ARG (ND6_MOD_TRC, BUFFER_TRC, ND6_NAME,
                     "Entering Nd6RedHandleIdleToStandby \r\n");

    ND6_GET_NODE_STATUS () = RM_STANDBY;
    ND6_NUM_STANDBY_NODES () = 0;
    IP6_GBL_TRC_ARG (ND6_MOD_TRC, BUFFER_TRC, ND6_NAME,
                     "Nd6RedHandleIdleToStandby: Node Status Idle to Standby \r\n");
    IP6_GBL_TRC_ARG (ND6_MOD_TRC, BUFFER_TRC, ND6_NAME,
                     "Exiting Nd6RedHandleIdleToStandby \r\n");
    return;
}

/************************************************************************/
/* Function Name      : Nd6StartTimers                                  */
/*                                                                      */
/* Description        : This is a recursive function which is registered*/
/*                      to RBTreeWalk. The RBTree is walked and the     */
/*                      elements in the RBtree is checked for either    */
/*                      static or dynamic entry. If static entry, then  */
/*                      the timer is not started. If the entry is       */
/*                      dynamic, then the cache timer is started.       */
/*                                                                      */
/* Input(s)           : pRBElem - RBTree to walk.                       */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : RB_WALK_CONT/RB_WALK_BREAK                      */
/************************************************************************/

INT4
Nd6StartTimers (tRBElem * pRBElem, eRBVisit visit, UINT4 u4Level,
                void *pArg, void *pOut)
{

    tNd6CacheEntry     *pNd6Cache = NULL;
    tNd6CacheEntry      Nd6Cache;
    UINT4               u4Duration = 0;

    UNUSED_PARAM (u4Level);
    UNUSED_PARAM (pArg);
    UNUSED_PARAM (pOut);

    if (visit == postorder || visit == leaf)
    {

        if (pRBElem != NULL)
        {
            pNd6Cache = (tNd6CacheEntry *) pRBElem;
            MEMSET (&Nd6Cache, IP6_ZERO, sizeof (tNd6CacheEntry));
            if ((pNd6Cache->u1ReachState == ND6C_STATIC) ||
                (pNd6Cache->u1ReachState == ND6C_STATIC_NOT_IN_SERVICE))
            {
                Nd6Cache.addr6 = pNd6Cache->addr6;
                Nd6Cache.pIf6 = Ip6ifGetEntry (pNd6Cache->pIf6->u4Index);
                UNUSED_PARAM (Nd6Cache.pIf6);
                return RB_WALK_CONT;
            }

            if (pNd6Cache->u1ReachState == ND6C_INCOMPLETE)
            {
                Nd6CheckAndSendNeighSol (pNd6Cache, &pNd6Cache->recentSrcAddr);
            }

            if (pNd6Cache->u1ReachState == ND6C_STALE)
            {
                if ((u4Duration = IP6_IF_PDELAYTIME (pNd6Cache->pIf6)))
                {
                    Nd6SetReachState (pNd6Cache, ND6C_DELAY);
                    Nd6SetCacheTimer (pNd6Cache, ND6_DELAY_PROBE_TIMER_ID,
                                      u4Duration);
                }
                else
                {
                    /* Probe immediately as delay timer is zero */
                    Nd6SetReachState (pNd6Cache, ND6C_PROBE);
                    Nd6ProbeOnCache (pNd6Cache);
                }

            }

            if (pNd6Cache->u1ReachState == ND6C_DELAY)
            {
                if ((u4Duration = IP6_IF_PDELAYTIME (pNd6Cache->pIf6)))
                {
                    Nd6SetCacheTimer (pNd6Cache, ND6_DELAY_PROBE_TIMER_ID,
                                      u4Duration);
                }
                else
                {
                    /* Probe immediately as delay timer is zero */
                    Nd6SetReachState (pNd6Cache, ND6C_PROBE);
                    Nd6ProbeOnCache (pNd6Cache);
                }

            }

            else if ((pNd6Cache->u1ReachState == ND6C_REACHABLE) || (pNd6Cache->u1ReachState == ND6C_PROBE))
            {
                if (pNd6Cache->u1ReachState == ND6C_PROBE)
                 {
                   pNd6Cache->u1ReachState = ND6C_REACHABLE;
                 }
                Nd6UpdateLastUseTime (pNd6Cache);

                u4Duration = 10 * REACHABLE_TIME;

                u4Duration = Ip6Random ((UINT4)(u4Duration * 0.5),(UINT4)(u4Duration * 1.5));
                Nd6SetMSecCacheTimer (pNd6Cache, ND6_REACH_TIMER_ID,
                                      u4Duration);
            }

            Nd6Cache.addr6 = pNd6Cache->addr6;
            Nd6Cache.pIf6 = Ip6ifGetEntry (pNd6Cache->pIf6->u4Index);
            UNUSED_PARAM (Nd6Cache.pIf6);
        }
        else
        {
            return RB_WALK_BREAK;
        }
    }
    return RB_WALK_CONT;
}

/************************************************************************/
/* Function Name      : Nd6RedHandleStandbyToActive                     */
/*                                                                      */
/* Description        : This function handles the Standby node to Active*/
/*                      node transistion. The following action are      */
/*                      performed during this transistion.              */
/*                      1.Update the Node Status and Standby node count.*/
/*                      2.Start the timers and enable the module.       */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
Nd6RedHandleStandbyToActive (VOID)
{
    UINT4               u4CurrentTime = 0;

    IP6_GBL_TRC_ARG (ND6_MOD_TRC, BUFFER_TRC, ND6_NAME,
                     "Entering Nd6RedHandleStandbyToActive \r\n");

    OsixGetSysTime ((tOsixSysTime *) & u4CurrentTime);
    gNd6RedGlobalInfo.i4Nd6RedEntryTime = u4CurrentTime;
    Nd6RedHwAudit ();

    ND6_GET_NODE_STATUS () = RM_ACTIVE;
    ND6_RM_GET_NUM_STANDBY_NODES_UP ();

    /* Start the necessary timers here.
     * Timers to be started, check the dynamic nd entry.
     * Depending on the state of the entry, start the correspinding
     * timer and send the NS. */

    OsixSendEvent (SELF, (const UINT1 *) IP6_TASK_NAME,
                   IP6_RED_START_TIMER_EVENT);

    OsixGetSysTime ((tOsixSysTime *) & u4CurrentTime);
    gNd6RedGlobalInfo.i4Nd6RedExitTime = u4CurrentTime;

    IP6_GBL_TRC_ARG (ND6_MOD_TRC, BUFFER_TRC, ND6_NAME,
                     "Nd6RedHandleStandbyToActive: Node Status Standby to Active \r\n");
    IP6_GBL_TRC_ARG (ND6_MOD_TRC, BUFFER_TRC, ND6_NAME,
                     "Exiting Nd6RedHandleStandbyToActive \r\n");
    return;
}

/************************************************************************/
/* Function Name      : Nd6RedHandleActiveToStandby                     */
/*                                                                      */
/* Description        : This function handles the Active node to Standby*/
/*                      node transistion. The following action are      */
/*                      performed during this transistion               */
/*                      1.Update the Node Status and Standby node count */
/*                      2.Disable and Enable the module                 */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
Nd6RedHandleActiveToStandby (VOID)
{
    IP6_GBL_TRC_ARG (ND6_MOD_TRC, BUFFER_TRC, ND6_NAME,
                     "Entering Nd6RedHandleActiveToStandby \r\n");

    /* Stop the Timer lists used by ND */

    /*
     * Stop the running timers in the active node. Check whether any timers are 
     * running, if running, stop those timers. If there are any messages in the 
     * queue, discard those messages (Need to confirm) */

    Ip6Disable ();
    Ip6Enable ();

    /*update the statistics */
    ND6_GET_NODE_STATUS () = RM_STANDBY;
    ND6_RM_GET_NUM_STANDBY_NODES_UP ();


    IP6_GBL_TRC_ARG (ND6_MOD_TRC, BUFFER_TRC, ND6_NAME,
                     "Nd6RedHandleActiveToStandby: Node Status Active to Standby \r\n");
    IP6_GBL_TRC_ARG (ND6_MOD_TRC, BUFFER_TRC, ND6_NAME,
                     "Exiting Nd6RedHandleActiveToStandby \r\n");
    return;
}

/************************************************************************/
/* Function Name      : Nd6RedProcessPeerMsgAtActive                    */
/*                                                                      */
/* Description        : This routine handles messages from the other    */
/*                      node (Standby) at Active. The messages that are */
/*                      handled in Active node are                      */
/*                      1. RM_BULK_UPDT_REQ_MSG                         */
/*                                                                      */
/* Input(s)           : pMsg - RM Data buffer holding message           */
/*                      u2DataLen - Length of data in buffer.           */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
Nd6RedProcessPeerMsgAtActive (tRmMsg * pMsg, UINT2 u2DataLen)
{
    tRmProtoEvt         ProtoEvt;
    UINT2               u2OffSet = 0;
    UINT2               u2Length = 0;
    UINT1               u1MsgType = 0;

    IP6_GBL_TRC_ARG (ND6_MOD_TRC, BUFFER_TRC, ND6_NAME,
                     "Entering Nd6RedProcessPeerMsgAtActive \r\n");

    ProtoEvt.u4AppId = RM_ND6_APP_ID;

    ND6_RM_GET_1_BYTE (pMsg, &u2OffSet, u1MsgType);
    ND6_RM_GET_2_BYTE (pMsg, &u2OffSet, u2Length);

    if (u2OffSet != u2DataLen)
    {
        /* Currently, the only RM packet expected to be processed at
         * active node is Bulk Request message which has only Type and
         * length. Hence this validation is done.
         */
        ProtoEvt.u4Error = RM_PROCESS_FAIL;
        if (RmApiHandleProtocolEvent (&ProtoEvt) == RM_FAILURE)
        {
            IP6_GBL_TRC_ARG (ND6_MOD_TRC, ALL_FAILURE_TRC |
                             BUFFER_TRC, ND6_NAME,
                             "Nd6RedProcessPeerMsgAtActive: Indication"
                             "of error to RM to process Bulk requeset failed!\r \n");
        }
        return;
    }

    if (u2Length != ND6_RED_BULK_REQ_MSG_SIZE)
    {
        ProtoEvt.u4Error = RM_PROCESS_FAIL;

        if (RmApiHandleProtocolEvent (&ProtoEvt) == RM_FAILURE)
        {
            IP6_GBL_TRC_ARG (ND6_MOD_TRC, ALL_FAILURE_TRC |
                             BUFFER_TRC, ND6_NAME,
                             "Nd6RedProcessPeerMsgAtActive: Indication"
                             "of error to RM to process Bulk requeset failed!\r \n");
        }
        return;
    }

    if (u1MsgType == ND6_RED_BULK_REQ_MESSAGE)
    {
        gNd6RedGlobalInfo.u1BulkUpdStatus = ND6_HA_UPD_NOT_STARTED;
        if (!ND6_IS_STANDBY_UP ())
        {
            /* This is a special case, where bulk request msg from
             * standby is coming before RM_STANDBY_UP. So no need to
             * process the bulk request now. Bulk updates will be send
             * on RM_STANDBY_UP event.
             */
            gNd6RedGlobalInfo.bBulkReqRcvd = OSIX_TRUE;
            return;
        }
        Nd6RedSendBulkUpdMsg ();
    }

    IP6_GBL_TRC_ARG (ND6_MOD_TRC, BUFFER_TRC, ND6_NAME,
                     "Exiting Nd6RedProcessPeerMsgAtActive \r\n");
    return;
}

/************************************************************************/
/* Function Name      : Nd6RedProcessPeerMsgAtStandby                   */
/*                                                                      */
/* Description        : This routine handles messages from the other    */
/*                      node (Active) at standby. The messages that are */
/*                      handled in Standby node are                     */
/*                      1. RM_BULK_UPDT_TAIL_MSG                        */
/*                      2. Dynamic sync-up messages                     */
/*                      3. Dynamic bulk messages                        */
/*                                                                      */
/* Input(s)           : pMsg - RM Data buffer holding message           */
/*                      u2DataLen - Length of data in buffer.           */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
Nd6RedProcessPeerMsgAtStandby (tRmMsg * pMsg, UINT2 u2DataLen)
{
    tRmProtoEvt         ProtoEvt;
    UINT2               u2OffSet = 0;
    UINT2               u2Length = 0;
    UINT2               u2RemMsgLen = 0;
    UINT2               u2MinLen = 0;
    UINT1               u1MsgType = 0;

    IP6_GBL_TRC_ARG (ND6_MOD_TRC, BUFFER_TRC, ND6_NAME,
                     "Entering Nd6RedProcessPeerMsgAtStandby \r\n");

    ProtoEvt.u4AppId = RM_ND6_APP_ID;
    u2MinLen = ND6_RED_TYPE_FIELD_SIZE + ND6_RED_LEN_FIELD_SIZE;

    ND6_RM_GET_1_BYTE (pMsg, &u2OffSet, u1MsgType);

    ND6_RM_GET_2_BYTE (pMsg, &u2OffSet, u2Length);

    if (u2Length < u2MinLen)
    {
        ProtoEvt.u4Error = RM_PROCESS_FAIL;

        if (RmApiHandleProtocolEvent (&ProtoEvt) == RM_FAILURE)
        {
            IP6_GBL_TRC_ARG (ND6_MOD_TRC, ALL_FAILURE_TRC |
                             BUFFER_TRC, ND6_NAME,
                             "Nd6RedProcessPeerMsgAtStandby: Indication"
                             "of error to RM to process Bulk update tail failed!\r \n");
            return;
        }
    }

    u2RemMsgLen = (UINT2) (u2Length - u2MinLen);

    if ((u2OffSet + u2RemMsgLen) != u2DataLen)
    {
        ProtoEvt.u4Error = RM_PROCESS_FAIL;

        if (RmApiHandleProtocolEvent (&ProtoEvt) == RM_FAILURE)
        {
            IP6_GBL_TRC_ARG (ND6_MOD_TRC, ALL_FAILURE_TRC |
                             BUFFER_TRC, ND6_NAME,
                             "Nd6RedProcessPeerMsgAtStandby: Indication"
                             "of error to RM to process Bulk update tail failed!\r \n");
            return;
        }
    }
    switch (u1MsgType)
    {
        case ND6_RED_BULK_UPD_TAIL_MESSAGE:
            if (u2Length != ND6_RED_BULK_UPD_TAIL_MSG_SIZE)
            {
                ProtoEvt.u4Error = RM_PROCESS_FAIL;

                if (RmApiHandleProtocolEvent (&ProtoEvt) == RM_FAILURE)
                {
                    IP6_GBL_TRC_ARG (ND6_MOD_TRC, ALL_FAILURE_TRC |
                                     BUFFER_TRC, ND6_NAME,
                                     "Nd6RedProcessPeerMsgAtStandby:"
                                     "Indication of error to RM to process Bulk update tail"
                                     "failed!\r \n");
                    return;
                }
            }
            Nd6RedProcessBulkTailMsg (pMsg, &u2OffSet);
            break;
        case ND6_RED_BULK_CACHE_INFO:
            Nd6RedProcessBulkInfo (pMsg, &u2OffSet);
            break;
        case ND6_RED_DYN_CACHE_INFO:
            Nd6RedProcessDynamicInfo (pMsg, &u2OffSet);
            break;
    }

    IP6_GBL_TRC_ARG (ND6_MOD_TRC, BUFFER_TRC, ND6_NAME,
                     "Exiting Nd6RedProcessPeerMsgAtStandby \r\n");
    return;
}

/************************************************************************/
/* Function Name      : Nd6RedRmReleaseMemoryForMsg                     */
/*                                                                      */
/* Description        : This function is invoked by the IPV6 module to  */
/*                      release the allocated memory by calling the RM  */
/*                      module.                                         */
/*                                                                      */
/* Input(s)           : pu1Block -- Memory Block                        */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/************************************************************************/

PUBLIC INT4
Nd6RedRmReleaseMemoryForMsg (UINT1 *pu1Block)
{
    IP6_GBL_TRC_ARG (ND6_MOD_TRC, BUFFER_TRC, ND6_NAME,
                     "Entering Nd6RedRmReleaseMemoryForMsg \r\n");
    if (RmReleaseMemoryForMsg (pu1Block) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    IP6_GBL_TRC_ARG (ND6_MOD_TRC, BUFFER_TRC, ND6_NAME,
                     "Exiting Nd6RedRmReleaseMemoryForMsg \r\n");

    return OSIX_SUCCESS;
}

/************************************************************************/
/* Function Name      : Nd6RedRmReleaseMemoryForMsg                     */
/*                                                                      */
/* Description        : This routine enqueues the Message to RM. If the */
/*                      sending fails, it frees the memory.             */
/*                                                                      */
/* Input(s)           : pMsg - RM Message Data Buffer                   */
/*                      u2Length - Length of the message                */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/************************************************************************/

PUBLIC INT4
Nd6RedSendMsgToRm (tRmMsg * pMsg, UINT2 u2Length)
{
    UINT4               u4RetVal = 0;
    IP6_GBL_TRC_ARG (ND6_MOD_TRC, BUFFER_TRC, ND6_NAME,
                     "Entering Nd6RedSendMsgToRm \r\n");

    u4RetVal = RmEnqMsgToRmFromAppl (pMsg, u2Length,
                                     RM_ND6_APP_ID, RM_ND6_APP_ID);

    if (u4RetVal != RM_SUCCESS)
    {
        RM_FREE (pMsg);
        return OSIX_FAILURE;
    }

    IP6_GBL_TRC_ARG (ND6_MOD_TRC, BUFFER_TRC, ND6_NAME,
                     "Exiting Nd6RedSendMsgToRm \r\n");
    return OSIX_SUCCESS;
}

/************************************************************************/
/* Function Name      : Nd6RedSendBulkReqMsg                          */
/*                                                                      */
/* Description        : This function sends the bulk request message    */
/*                      from standby node to Active node to initiate the*/
/*                      bulk update process.                            */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
Nd6RedSendBulkReqMsg (VOID)
{
    tRmProtoEvt         ProtoEvt;
    tRmMsg             *pMsg = NULL;
    UINT2               u2OffSet = 0;

    IP6_GBL_TRC_ARG (ND6_MOD_TRC, BUFFER_TRC, ND6_NAME,
                     "Entering Nd6RedSendBulkReqMsg \r\n");

    ProtoEvt.u4AppId = RM_ND6_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    /*
     *    Bulk Request message
     *
     *    <- 1 byte ->|<- 2 bytes->|
     *    --------------------------
     *    | Msg. Type |  Length    |
     *    |           |            |
     *    |-------------------------
     */

    if ((pMsg = RM_ALLOC_TX_BUF (ND6_RED_BULK_REQ_MSG_SIZE)) == NULL)
    {
        IP6_GBL_TRC_ARG (ND6_MOD_TRC, ALL_FAILURE_TRC |
                         BUFFER_TRC, ND6_NAME, "Nd6RedSendBulkReqMsg:RM Memory"
                         " allocation failed!\r \n");
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
        return;
    }

    ND6_RM_PUT_1_BYTE (pMsg, &u2OffSet, ND6_RED_BULK_REQ_MESSAGE);
    ND6_RM_PUT_2_BYTE (pMsg, &u2OffSet, ND6_RED_BULK_REQ_MSG_SIZE);

    if (Nd6RedSendMsgToRm (pMsg, u2OffSet) != OSIX_SUCCESS)
    {
        ProtoEvt.u4Error = RM_SENDTO_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
        return;
    }

    IP6_GBL_TRC_ARG (ND6_MOD_TRC, BUFFER_TRC, ND6_NAME,
                     "Exiting Nd6RedSendBulkReqMsg \r\n");
    return;
}

/************************************************************************/
/* Function Name      : Nd6RedSendBulkUpdMsg                        */
/*                                                                      */
/* Description        : This function sends the binding table dynamic   */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
Nd6RedSendBulkUpdMsg (VOID)
{
    UINT1               u1BulkUpdPendFlg = OSIX_FALSE;

    IP6_GBL_TRC_ARG (ND6_MOD_TRC, BUFFER_TRC, ND6_NAME,
                     "Entering Nd6RedSendBulkUpdMsg \r\n");

    if (!ND6_IS_STANDBY_UP ())
    {
        IP6_GBL_TRC_ARG (ND6_MOD_TRC, ALL_FAILURE_TRC |
                         BUFFER_TRC, ND6_NAME, "Nd6RedSendBulkUpdMsg: "
                         "No Standby node available!\r \n");

        return;
    }

    if (gNd6RedGlobalInfo.u1BulkUpdStatus == ND6_HA_UPD_NOT_STARTED)
    {
        gNd6RedGlobalInfo.u1BulkUpdStatus = ND6_HA_UPD_IN_PROGRESS;
        MEMSET (&gNd6RedGlobalInfo.Nd6HACacheMarker, 0,
                sizeof (tNd6HACacheMarker));
    }

    if (gNd6RedGlobalInfo.u1BulkUpdStatus == ND6_HA_UPD_IN_PROGRESS)
    {
        if (Nd6RedSendBulkCacheInfo (&u1BulkUpdPendFlg) != IP6_SUCCESS)
        {
            gNd6RedGlobalInfo.u1BulkUpdStatus = ND6_HA_UPD_ABORTED;
            IP6_GBL_TRC_ARG (ND6_MOD_TRC, ALL_FAILURE_TRC |
                             BUFFER_TRC, ND6_NAME, "Nd6RedSendBulkUpdMsg: "
                             "failed!\r \n");
        }

        else
        {
            RmSetBulkUpdatesStatus (RM_ND6_APP_ID);
            if (u1BulkUpdPendFlg == OSIX_TRUE)
            {
                /* Send an event to the IPV6 main task indicating that there
                 * are pending entries in the ND database to be synced */

                OsixSendEvent (SELF, (const UINT1 *) IP6_TASK_NAME,
                               IP6_HA_PEND_BLKUPD_EVENT);
            }
            else
            {
                /* Send the tail msg to indicate the completion of Bulk
                 * update process.
                 */
                gNd6RedGlobalInfo.u1BulkUpdStatus = ND6_HA_UPD_COMPLETED;
                Nd6RedSendBulkUpdTailMsg ();
            }
        }
    }

    IP6_GBL_TRC_ARG (ND6_MOD_TRC, BUFFER_TRC, ND6_NAME,
                     "Exiting Nd6RedSendBulkUpdMsg \r\n");
    return;

}

/************************************************************************/
/* Function Name      : Nd6RedSendBulkCacheInfo                         */
/*                                                                      */
/* Description        : This function sends the Bulk update messages to */
/*                      the peer standby ND.                            */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : u1BulkUpdPendFlg                                */
/*                                                                      */
/* Returns            : IP6_SUCCESS/IP6_FAILURE                         */
/************************************************************************/

PUBLIC INT4
Nd6RedSendBulkCacheInfo (UINT1 *pu1BulkUpdPendFlg)
{
    tRmMsg             *pMsg = NULL;
    tNd6HACacheMarker  *pNd6CacheMarker = NULL;
    tNd6HACacheMarker   LocalCacheMarker;
    tNd6CacheEntry     *pNd6Cache = NULL;
    tNd6CacheEntry     *pPrevNd6Cache = NULL;
    tNd6CacheEntry      Nd6Cache;
    tNd6CacheLanInfo   *pNd6cLinfo = NULL;
    tRmProtoEvt         ProtoEvt;
    UINT2               u2MsgLen = 0;
    UINT2               u2OffSet = 0;
    UINT2               u2LenOffSet = 0;
    UINT2               u2MsgPacked = 0;
    UINT2               u2NoCacheOffset = 0;

    IP6_GBL_TRC_ARG (ND6_MOD_TRC, BUFFER_TRC, ND6_NAME,
                     "Entering Nd6RedSendBulkCacheInfo \r\n");
    ProtoEvt.u4AppId = RM_ND6_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    /*
     *    Neighbor Cache Bulk Update message
     *
     *    <- 1 byte ->|<- 2 B->|<- 2 B ->|<- 16B ->|<- 6 B ->|<-1B->|<- 16B ->|
     *    ---------------------------------------------------------------------
     *    | Msg. Type | Length | No of   | IP      | HW      | Addr | Recent  |
     *    |           |        | Entries | Address | Address | Type | SrcAddr |
     *    ---------------------------------------------------------------------
     *
     *    <- 1B ->|<-1B->|<- 1B ->|<- 1B->|<- 1B ->|<- 1B ->|
     *    ---------------------------------------------------
     *    | If    | If   | Row    | Reach | Hw     | Secure |
     *    | Index | Type | Status | State | Status | Flag   |
     *    ---------------------------------------------------
     */

    if ((pMsg = RM_ALLOC_TX_BUF (ND6_RED_MAX_MSG_SIZE)) == NULL)
    {
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
        return IP6_FAILURE;
    }

    MEMSET (&Nd6Cache, 0, sizeof (tNd6CacheEntry));
    MEMSET (&LocalCacheMarker, 0, sizeof (tNd6HACacheMarker));

    pNd6CacheMarker = &(gNd6RedGlobalInfo.Nd6HACacheMarker);
    if (MEMCMP (pNd6CacheMarker, &LocalCacheMarker,
                sizeof (tNd6HACacheMarker)) == 0)
    {
        pNd6Cache = RBTreeGetFirst (gNd6GblInfo.Nd6CacheTable);
    }

    else
    {
        Nd6Cache.addr6 = pNd6CacheMarker->addr6;
        Nd6Cache.pIf6 = Ip6ifGetEntry (pNd6CacheMarker->u4Index);
        pNd6Cache = RBTreeGetNext (gNd6GblInfo.Nd6CacheTable,
                                   (tRBElem *) & Nd6Cache, NULL);
    }

    ND6_RM_PUT_1_BYTE (pMsg, &u2OffSet, ND6_RED_BULK_CACHE_INFO);

    /* INITIALLY SET THE LENGTH TO MAXIMUM LENGTH */
    u2LenOffSet = u2OffSet;
    ND6_RM_PUT_2_BYTE (pMsg, &u2OffSet, u2MsgLen);
    u2NoCacheOffset = u2OffSet;
    ND6_RM_PUT_2_BYTE (pMsg, &u2OffSet, u2MsgPacked);

    while (pNd6Cache != NULL)
    {
        if ((pNd6Cache->u1ReachState != ND6C_REACHABLE) &&
            (pNd6Cache->u1ReachState != ND6C_STATIC) &&
            (pNd6Cache->u1ReachState != ND6C_INCOMPLETE))
        {
            Nd6Cache.addr6 = pNd6Cache->addr6;
            Nd6Cache.pIf6 = Ip6ifGetEntry (pNd6Cache->pIf6->u4Index);
            pPrevNd6Cache = pNd6Cache;
            pNd6Cache = RBTreeGetNext (gNd6GblInfo.Nd6CacheTable,
                                       (tRBElem *) & Nd6Cache, NULL);
            continue;
        }

        pNd6cLinfo = (tNd6CacheLanInfo *) (VOID *) pNd6Cache->pNd6cInfo;

        if ((u2OffSet + ND6_RED_DYN_INFO_SIZE) < ND6_RED_MAX_MSG_SIZE)
        {
            ND6_RM_PUT_N_BYTE (pMsg, &u2OffSet, (pNd6Cache->addr6.u1_addr),
                               IPVX_IPV6_ADDR_LEN);
            ND6_RM_PUT_4_BYTE (pMsg, &u2OffSet, pNd6Cache->pIf6->u4Index);
            ND6_RM_PUT_1_BYTE (pMsg, &u2OffSet, pNd6Cache->u1ReachState);
            if (pNd6Cache->u1ReachState == ND6C_STATIC)
            {
                ND6_RM_PUT_1_BYTE (pMsg, &u2OffSet, pNd6Cache->u1HwStatus);
            }
            else
            {
                ND6_RM_PUT_N_BYTE (pMsg, &u2OffSet, pNd6cLinfo->lladdr,
                                   IP6_ENET_ADDR_LEN);
                ND6_RM_PUT_1_BYTE (pMsg, &u2OffSet, pNd6Cache->u1AddrType);

                /* If AddrType is Anycast, sync the recent unicast source addr */
                if (pNd6Cache->u1AddrType == ADDR6_ANYCAST)
                {
                    ND6_RM_PUT_N_BYTE (pMsg, &u2OffSet,
                                       pNd6Cache->recentSrcAddr.u1_addr,
                                       IPVX_IPV6_ADDR_LEN);
                }

                ND6_RM_PUT_1_BYTE (pMsg, &u2OffSet, pNd6Cache->pIf6->u1IfType);
                ND6_RM_PUT_1_BYTE (pMsg, &u2OffSet, pNd6Cache->u1RowStatus);
                ND6_RM_PUT_1_BYTE (pMsg, &u2OffSet, pNd6Cache->u1HwStatus);
                ND6_RM_PUT_1_BYTE (pMsg, &u2OffSet, pNd6Cache->u1SecureFlag);
				ND6_RM_PUT_N_BYTE (pMsg, &u2OffSet, pNd6Cache->au1SentNonce, 
								   ND6_NONCE_LENGTH);
				ND6_RM_PUT_N_BYTE (pMsg, &u2OffSet, pNd6Cache->au1RcvdNonce,
				                   ND6_NONCE_LENGTH);
				ND6_RM_PUT_4_BYTE (pMsg, &u2OffSet, pNd6Cache->u4SendTSLast);
				ND6_RM_PUT_4_BYTE (pMsg, &u2OffSet, pNd6Cache->u4SendRDLast);
            }
            u2MsgPacked += 1;
        }
        else
        {
            /* If message size is greater than MAX_SIZE, set the pending flag
             * as true and break */
            *pu1BulkUpdPendFlg = OSIX_TRUE;
            break;
        }
        Nd6Cache.addr6 = pNd6Cache->addr6;
        Nd6Cache.pIf6 = Ip6ifGetEntry (pNd6Cache->pIf6->u4Index);
        pPrevNd6Cache = pNd6Cache;
        pNd6Cache = RBTreeGetNext (gNd6GblInfo.Nd6CacheTable,
                                   (tRBElem *) & Nd6Cache, NULL);
    }

    ND6_RM_PUT_2_BYTE (pMsg, &u2LenOffSet, u2OffSet);
    ND6_RM_PUT_2_BYTE (pMsg, &u2NoCacheOffset, u2MsgPacked);

    if (u2MsgPacked == 0)
    {
        RM_FREE (pMsg);
        return IP6_SUCCESS;
    }

    if (Nd6RedSendMsgToRm (pMsg, u2OffSet) != OSIX_SUCCESS)
    {
        *pu1BulkUpdPendFlg = OSIX_FALSE;
        ProtoEvt.u4Error = RM_SENDTO_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
        return IP6_FAILURE;
    }

    IP6_GBL_TRC_ARG (ND6_MOD_TRC, BUFFER_TRC, ND6_NAME,
                     "Exiting Nd6RedSendBulkCacheInfo \r\n");
    /* Nd6CacheMarket is set to the last updated entry */
    pNd6CacheMarker->addr6 = pPrevNd6Cache->addr6;
    pNd6CacheMarker->u4Index = pPrevNd6Cache->pIf6->u4Index;
    return IP6_SUCCESS;
}

/************************************************************************/
/* Function Name      : Nd6RedSendBulkUpdTailMsg                        */
/*                                                                      */
/* Description        : This function sends the binding table dynamic   */
/*                      sync up message to Standby node from the Active */
/*                      node, and dynamically updates the               */
/*                      binding table entries.                          */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/************************************************************************/

VOID
Nd6RedSendBulkUpdTailMsg (VOID)
{
    tRmMsg             *pMsg = NULL;
    tRmProtoEvt         ProtoEvt;
    UINT2               u2OffSet = 0;

    IP6_GBL_TRC_ARG (ND6_MOD_TRC, BUFFER_TRC, ND6_NAME,
                     "Entering Nd6RedSendBulkUpdTailMsg \r\n");

    ProtoEvt.u4AppId = RM_ND6_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    /* Form a bulk update tail message.

     *        <------------1 Byte----------><---2 Byte--->
     *****************************************************
     *        *                             *            *
     * RM Hdr *  ND6_RED_BULK_UPD_TAIL_MSG  * Msg Length *
     *        *                             *            *
     *****************************************************

     * The RM Hdr shall be included by RM.
     */

    if ((pMsg = RM_ALLOC_TX_BUF (ND6_RED_BULK_UPD_TAIL_MSG_SIZE)) == NULL)
    {
        IP6_GBL_TRC_ARG (ND6_MOD_TRC, ALL_FAILURE_TRC |
                         BUFFER_TRC, ND6_NAME,
                         "Nd6RedSendBulkUpdTailMsg: RM Memory"
                         "allocation failed!\r \n");
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
        return;
    }

    u2OffSet = 0;

    ND6_RM_PUT_1_BYTE (pMsg, &u2OffSet, ND6_RED_BULK_UPD_TAIL_MESSAGE);
    ND6_RM_PUT_2_BYTE (pMsg, &u2OffSet, ND6_RED_BULK_UPD_TAIL_MSG_SIZE);

    /* This routine sends the message to RM and in case of failure
     * releases the RM buffer memory
     */
    if (Nd6RedSendMsgToRm (pMsg, u2OffSet) != OSIX_SUCCESS)
    {
        ProtoEvt.u4Error = RM_SENDTO_FAIL;
        if (RmApiHandleProtocolEvent (&ProtoEvt) == RM_FAILURE)
        {
            IP6_GBL_TRC_ARG (ND6_MOD_TRC, ALL_FAILURE_TRC |
                             BUFFER_TRC, ND6_NAME,
                             "Nd6RedSendBulkUpdTailMsg: Ack to RM -"
                             "Send to RM failed!\r \n");
        }
        return;
    }

    IP6_GBL_TRC_ARG (ND6_MOD_TRC, BUFFER_TRC, ND6_NAME,
                     "Exiting Nd6RedSendBulkUpdTailMsg \r\n");
    return;
}

/************************************************************************/
/* Function Name      : Nd6RedProcessBulkTailMsg                        */
/*                                                                      */
/* Description        : This function process the bulk update tail      */
/*                      message and send bulk updates                   */
/*                                                                      */
/* Input(s)           : pMsg - RM Data buffer holding messges           */
/*                      u2DataLen - Length of data in buffer            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
Nd6RedProcessBulkTailMsg (tRmMsg * pMsg, UINT2 *pu2OffSet)
{
    tRmProtoEvt         ProtoEvt;

    ProtoEvt.u4AppId = RM_ND6_APP_ID;

    UNUSED_PARAM (pMsg);
    UNUSED_PARAM (pu2OffSet);
    IP6_GBL_TRC_ARG (ND6_MOD_TRC, BUFFER_TRC, ND6_NAME,
                     "Entering Nd6RedProcessBulkTailMsg \r\n");

    ProtoEvt.u4Error = RM_NONE;
    ProtoEvt.u4Event = RM_PROTOCOL_BULK_UPDT_COMPLETION;

    if (RmApiHandleProtocolEvent (&ProtoEvt) == RM_FAILURE)
    {
        IP6_GBL_TRC_ARG (ND6_MOD_TRC, ALL_FAILURE_TRC |
                         BUFFER_TRC, ND6_NAME, "Nd6RedProcessBulkTailMsg: "
                         "Indication of error to RM to process Bulk "
                         "update completion failed!\r \n");
    }

    IP6_GBL_TRC_ARG (ND6_MOD_TRC, BUFFER_TRC, ND6_NAME,
                     "Exiting Nd6RedProcessBulkTailMsg \r\n");

    return;
}

/************************************************************************
 * Function Name      : Nd6RedSendDynamicCacheInfo 
 *                                                
 * Description        : This function sends the binding table dynamic  
 *                      sync up message to Standby node from the Active 
 *                      node, and dynamically update the   
 *                      binding table entries.                        
 *                                                                   
 * Input(s)           : None
 *                                                                    
 * Output(s)          : None                                         
 *                                                                  
 * Returns            : OSIX_SUCCESS / OSIX_FAILURE                
 ************************************************************************/

PUBLIC VOID
Nd6RedSendDynamicCacheInfo ()
{
    tRmMsg             *pMsg = NULL;
    tNd6RedTable       *pNd6RedInfo = NULL;
    tNd6RedTable       *pBkpNd6RedInfo = NULL;
    tNd6RedTable        Nd6RedCacheInfo;
    tRmProtoEvt         ProtoEvt;
    UINT4               u4MsgAllocSize = 0;
    UINT4               u4Count = 0;
    UINT2               u2MsgLen = 0;
    UINT2               u2OffSet = 0;
    UINT2               u2LenOffSet = 0;
    UINT2               u2MsgPacked = 0;
    UINT2               u2NoCacheOffset = 0;

    IP6_GBL_TRC_ARG (ND6_MOD_TRC, BUFFER_TRC, ND6_NAME,
                     "Entering Nd6RedSendDynamicCacheInfo \r\n");
    ProtoEvt.u4AppId = RM_ND6_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    MEMSET(&Nd6RedCacheInfo, 0, sizeof(tNd6RedTable));

    ND6Lock();
    RBTreeCount (gIp6GblInfo.Nd6RedTable, &u4Count);
    u4MsgAllocSize = ND6_RED_MIM_MSG_SIZE;
    u4MsgAllocSize += (u4Count * (ND6_RED_DYN_INFO_SIZE + ND6_ONE_BYTE));
    u4MsgAllocSize = (u4MsgAllocSize < ND6_RED_MAX_MSG_SIZE) ?
        u4MsgAllocSize : ND6_RED_MAX_MSG_SIZE;

    /* Message is allcoated upto required size. The number of entries in the 
     * RBTree is got and then it is multiplied with the size of a single 
     * dynamic update to get the total count. If that length is greater than
     * MAX size, then the message is allocated upto max size */

    /*
     *    Neighbor Cache Dynamic Update message
     *
     *    <- 1 byte ->|<- 2 B->|<- 2 B ->|<- 16B ->|<- 6 B ->|<-1B->|<- 16B ->|
     *    ---------------------------------------------------------------------
     *    | Msg. Type | Length | No of   | IP      | HW      | Addr | Recent  |
     *    |           |        | Entries | Address | Address | Type | SrcAddr |
     *    ---------------------------------------------------------------------
     *
     *    <- 1B ->|<-1B->|<- 1B ->|<- 1B->|<- 1B ->|<- 1B  ->|<- 1B  ->|
     *    --------------------------------------------------------------
     *    | If    | If   | Row    | Reach | Hw     | Action  | Secure  |
     *    | Index | Type | Status | State | Status | Flag    | Flag    |
     *    --------------------------------------------------------------
     */
    if ((pMsg = RM_ALLOC_TX_BUF (u4MsgAllocSize)) == NULL)
    {
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
        ND6UnLock();
        return;
    }

    pNd6RedInfo = RBTreeGetFirst (gIp6GblInfo.Nd6RedTable);

    ND6_RM_PUT_1_BYTE (pMsg, &u2OffSet, ND6_RED_DYN_CACHE_INFO);

    /* INITIALLY SET THE LENGTH TO MAXIMUM LENGTH */
    u2LenOffSet = u2OffSet;
    ND6_RM_PUT_2_BYTE (pMsg, &u2OffSet, u2MsgLen);
    u2NoCacheOffset = u2OffSet;
    ND6_RM_PUT_2_BYTE (pMsg, &u2OffSet, u2MsgPacked);

    while (pNd6RedInfo != NULL)
    {
        if ((UINT4) (u2OffSet + ND6_ONE_BYTE + ND6_RED_DYN_INFO_SIZE)
            > u4MsgAllocSize)
        {
            break;
        }

        ND6_RM_PUT_N_BYTE (pMsg, &u2OffSet, pNd6RedInfo->addr6.u1_addr,
                           IPVX_IPV6_ADDR_LEN);
        ND6_RM_PUT_N_BYTE (pMsg, &u2OffSet, pNd6RedInfo->lladdr,
                           IP6_ENET_ADDR_LEN);
        ND6_RM_PUT_1_BYTE (pMsg, &u2OffSet, pNd6RedInfo->u1AddrType);

        /* If AddrType is Anycast, sync the recent unicast source addr */
        if (pNd6RedInfo->u1AddrType == ADDR6_ANYCAST)
        {
            ND6_RM_PUT_N_BYTE (pMsg, &u2OffSet,
                               pNd6RedInfo->recentSrcAddr.u1_addr,
                               IPVX_IPV6_ADDR_LEN);
        }

        ND6_RM_PUT_4_BYTE (pMsg, &u2OffSet, pNd6RedInfo->u4Index);
        ND6_RM_PUT_1_BYTE (pMsg, &u2OffSet, pNd6RedInfo->u1IfType);
        ND6_RM_PUT_1_BYTE (pMsg, &u2OffSet, pNd6RedInfo->u1RowStatus);
        ND6_RM_PUT_1_BYTE (pMsg, &u2OffSet, pNd6RedInfo->u1ReachState);
        ND6_RM_PUT_1_BYTE (pMsg, &u2OffSet, pNd6RedInfo->u1HwStatus);
        ND6_RM_PUT_1_BYTE (pMsg, &u2OffSet, pNd6RedInfo->u1Action);
        ND6_RM_PUT_1_BYTE (pMsg, &u2OffSet, pNd6RedInfo->u1SecureFlag);
        ND6_RM_PUT_N_BYTE (pMsg, &u2OffSet, pNd6RedInfo->au1SentNonce,
                           ND6_NONCE_LENGTH);
        ND6_RM_PUT_N_BYTE (pMsg, &u2OffSet, pNd6RedInfo->au1RcvdNonce,
                           ND6_NONCE_LENGTH);
        ND6_RM_PUT_4_BYTE (pMsg, &u2OffSet, pNd6RedInfo->u4SendTSLast);
        ND6_RM_PUT_4_BYTE (pMsg, &u2OffSet, pNd6RedInfo->u4SendRDLast);

        u2MsgPacked += 1;

        Nd6RedCacheInfo.addr6 = pNd6RedInfo->addr6;
        Nd6RedCacheInfo.u4Index = pNd6RedInfo->u4Index;
        pNd6RedInfo = RBTreeGetNext (gIp6GblInfo.Nd6RedTable,
                                     (tRBElem *) & Nd6RedCacheInfo, NULL);
    }

    ND6_RM_PUT_2_BYTE (pMsg, &u2LenOffSet, u2OffSet);
    ND6_RM_PUT_2_BYTE (pMsg, &u2NoCacheOffset, u2MsgPacked);
    if (u2MsgPacked == 0)
    {
        /* If there are no new items to sent, then it the message is not sent */
        RM_FREE (pMsg);
        ND6UnLock();
        return;
    }
    if (Nd6RedSendMsgToRm (pMsg, u2OffSet) != OSIX_SUCCESS)
    {
        ProtoEvt.u4Error = RM_SENDTO_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
        ND6UnLock();
        return;
    }
    pNd6RedInfo =
        RBTreeGet (gIp6GblInfo.Nd6RedTable, (tRBElem *) & Nd6RedCacheInfo);
    if (pNd6RedInfo != NULL)
    {
        pNd6RedInfo->u1DelFlag = ND6_RED_CACHE_DEL;
    }
    /* The last updated message's delete flag is set */
    pNd6RedInfo = RBTreeGetFirst (gIp6GblInfo.Nd6RedTable);
    while (pNd6RedInfo != NULL)
    {
        Nd6RedCacheInfo.addr6 = pNd6RedInfo->addr6;
        Nd6RedCacheInfo.u4Index = pNd6RedInfo->u4Index;
        pBkpNd6RedInfo = RBTreeGetNext (gIp6GblInfo.Nd6RedTable,
                (tRBElem *) & Nd6RedCacheInfo, NULL);

        /* The temporary RBTree is scanned till the entry with the delete flag 
         * is reached. If the hardware status is NP_PRESENT, the corresponding
         * entry is deleted, else the entry is not deleted, and the
         * delete flag is reset. */
        if (pNd6RedInfo->u1HwStatus == NP_PRESENT)
        {
            RBTreeRem (gIp6GblInfo.Nd6RedTable, pNd6RedInfo);
            if (pNd6RedInfo->u1DelFlag == ND6_RED_CACHE_DEL)
            {
                MemReleaseMemBlock ((tMemPoolId) gIp6GblInfo.
                                    i4Ip6Nd6DynMsgPoolId,
                                    (UINT1 *) pNd6RedInfo);
                pNd6RedInfo = NULL;									
                break;
            }
            MemReleaseMemBlock (gIp6GblInfo.i4Ip6Nd6DynMsgPoolId,
                                (UINT1 *) pNd6RedInfo);
            pNd6RedInfo = NULL;								
        }
        else
        {
            if (pNd6RedInfo->u1DelFlag == ND6_RED_CACHE_DEL)
            {
                pNd6RedInfo->u1DelFlag = ND6_RED_CACHE_DONT_DEL;
                break;
            }
        }
		pNd6RedInfo = pBkpNd6RedInfo;
        Nd6RedCacheInfo.addr6 = pNd6RedInfo->addr6;
        Nd6RedCacheInfo.u4Index = pNd6RedInfo->u4Index;
        pNd6RedInfo = RBTreeGetNext (gIp6GblInfo.Nd6RedTable,
                                     (tRBElem *) & Nd6RedCacheInfo, NULL);
    }

    IP6_GBL_TRC_ARG (ND6_MOD_TRC, BUFFER_TRC, ND6_NAME,
                     "Exiting Nd6RedSendDynamicCacheInfo \r\n");

    ND6UnLock();
    return;
}

/************************************************************************/
/* Function Name      : Nd6RedProcessBulkInfo                           */
/*                                                                      */
/* Description        : This function decodes the sync-up message from  */
/*                      Active node and updates the binding information */
/*                      at the Standby node.                            */
/*                                                                      */
/* Input(s)           : pMsg - RM Sync-up message                       */
/*                      u2RemMsgLen - Length of value of Message        */
/*                                                                      */
/* Output(s)          : u2OffSet - OffSet value                         */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

VOID
Nd6RedProcessBulkInfo (tRmMsg * pMsg, UINT2 *pu2OffSet)
{

    tNd6CacheEntry      Nd6Cache;
    tNd6RedTable        Nd6RedNode;
    tNd6CacheEntry     *pNd6CacheInfo = NULL;
    tNd6RedTable       *pNd6RedNode = NULL;
    UINT2               u2NoOfEntries = 0;
    UINT4               u4Index = 0;
    UINT1               au1Addr[IP6_ENET_ADDR_LEN];
    IP6_GBL_TRC_ARG (ND6_MOD_TRC, BUFFER_TRC, ND6_NAME,
                     "Entering Nd6RedProcessBulkInfo \r\n");

    MEMSET (&Nd6Cache, 0, sizeof (tNd6CacheEntry));

    ND6_RM_GET_2_BYTE (pMsg, pu2OffSet, u2NoOfEntries);

    while (u2NoOfEntries > 0)
    {
        u2NoOfEntries--;
        ND6_RM_GET_N_BYTE (pMsg, pu2OffSet, Nd6Cache.addr6.u1_addr,
                           IPVX_IPV6_ADDR_LEN);
        ND6_RM_GET_4_BYTE (pMsg, pu2OffSet, u4Index);
        Nd6Cache.pIf6 = Ip6ifGetEntry (u4Index);
        if (Nd6Cache.pIf6 == NULL)
        {
            return;
        }

        ND6_RM_GET_1_BYTE (pMsg, pu2OffSet, Nd6Cache.u1ReachState);

        if (Nd6Cache.u1ReachState == ND6C_STATIC)
        {
            ND6_RM_GET_1_BYTE (pMsg, pu2OffSet, Nd6Cache.u1HwStatus);
        }
        else
        {
            ND6_RM_GET_N_BYTE (pMsg, pu2OffSet, au1Addr, IP6_ENET_ADDR_LEN);
            ND6_RM_GET_1_BYTE (pMsg, pu2OffSet, Nd6Cache.u1AddrType);

            /* If AddrType is Anycast, get the recent unicast source addr */
            if (Nd6Cache.u1AddrType == ADDR6_ANYCAST)
            {
                ND6_RM_GET_N_BYTE (pMsg, pu2OffSet,
                                   Nd6Cache.recentSrcAddr.u1_addr,
                                   IPVX_IPV6_ADDR_LEN);
            }

            ND6_RM_GET_1_BYTE (pMsg, pu2OffSet, Nd6Cache.pIf6->u1IfType);
            ND6_RM_GET_1_BYTE (pMsg, pu2OffSet, Nd6Cache.u1RowStatus);
            ND6_RM_GET_1_BYTE (pMsg, pu2OffSet, Nd6Cache.u1HwStatus);
            ND6_RM_GET_1_BYTE (pMsg, pu2OffSet, Nd6Cache.u1SecureFlag);
            ND6_RM_GET_N_BYTE (pMsg, pu2OffSet, Nd6Cache.au1SentNonce,
                               ND6_NONCE_LENGTH);
            ND6_RM_GET_N_BYTE (pMsg, pu2OffSet, Nd6Cache.au1RcvdNonce,
                               ND6_NONCE_LENGTH);
            ND6_RM_GET_4_BYTE (pMsg, pu2OffSet, Nd6Cache.u4SendTSLast);
            ND6_RM_GET_4_BYTE (pMsg, pu2OffSet, Nd6Cache.u4SendRDLast);

        }

        MEMSET (&Nd6RedNode, 0, sizeof (tNd6RedTable));
        Nd6RedNode.addr6 = Nd6Cache.addr6;
        Nd6RedNode.u4Index = u4Index;

        if (Nd6Cache.u1HwStatus == NP_PRESENT)
        {
            pNd6RedNode = RBTreeGet (gIp6GblInfo.Nd6RedTable,
                                     (tRBElem *) & Nd6RedNode);
            /* If the hardware status is NP_PRESENT, then the corresponding
             * entry is deleted from the temporary RBTree and added to the
             * software. If the hardware status is NP_NOT_PRESENT, then the
             * corresponding entry is added to the temporary data structure */

            if (pNd6RedNode != NULL)
            {
                RBTreeRem (gIp6GblInfo.Nd6RedTable, pNd6RedNode);
                MemReleaseMemBlock (gIp6GblInfo.i4Ip6Nd6DynMsgPoolId,
                                    (UINT1 *) pNd6RedNode);
            }
            if (Nd6Cache.u1ReachState == ND6C_STATIC)
            {
                pNd6CacheInfo = RBTreeGet (gNd6GblInfo.Nd6CacheTable,
                                           (tRBElem *) & Nd6Cache);
                if (pNd6CacheInfo != NULL)
                {
                    pNd6CacheInfo->u1HwStatus = Nd6Cache.u1ReachState;
                }
            }
            else
            {
                Nd6CreateCache (Nd6Cache.pIf6, &Nd6Cache.addr6, au1Addr,
                                IP6_ENET_ADDR_LEN, Nd6Cache.u1ReachState,
                                Nd6Cache.u1SecureFlag);
            }
        }
        else
        {
            /* If the entry is not present in NP (this is applicable only in
             * async NP case), then the entry is added to the temporary
             * RBtree and not updated in the software */

            if (Nd6Cache.u1ReachState == ND6C_STATIC)
            {
                pNd6CacheInfo = RBTreeGet (gNd6GblInfo.Nd6CacheTable,
                                           (tRBElem *) & Nd6Cache);
                Nd6RedAddDynamicInfo (pNd6CacheInfo, au1Addr,
                                      ND6_RED_ADD_CACHE);
            }
            else
            {
                Nd6RedAddDynamicInfo (&Nd6Cache, au1Addr, ND6_RED_ADD_CACHE);
            }
        }
    }

    IP6_GBL_TRC_ARG (ND6_MOD_TRC, BUFFER_TRC, ND6_NAME,
                     "Exiting Nd6RedProcessBulkInfo \r\n");
    return;
}

/************************************************************************/
/* Function Name      : Nd6RedProcessDynamicInfo                        */
/*                                                                      */
/* Description        : This function decodes the sync-up message from  */
/*                      Active node and updates the binding information */
/*                      at the Standby node.                            */
/*                                                                      */
/* Input(s)           : pMsg - RM Sync-up message                       */
/*                      u2RemMsgLen - Length of value of Message        */
/*                                                                      */
/* Output(s)          : u2OffSet - OffSet value                         */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

VOID
Nd6RedProcessDynamicInfo (tRmMsg * pMsg, UINT2 *pu2OffSet)
{
    tNd6CacheEntry      Nd6Cache;
    tNd6RedTable        Nd6RedNode;
    tNd6CacheEntry     *pNd6CacheInfo = NULL;
    tNd6RedTable       *pNd6RedNode = NULL;
    tNd6CacheLanInfo   *pNd6cLinfo = NULL;
    UINT2               u2NoOfEntries = 0;
    UINT4               u4Index = 0;
    UINT1               au1Addr[IP6_ENET_ADDR_LEN];
    UINT1               u1Action = 0;

    IP6_GBL_TRC_ARG (ND6_MOD_TRC, BUFFER_TRC, ND6_NAME,
                     "Enterting Nd6RedProcessDynamicInfo \r\n");

    ND6_RM_GET_2_BYTE (pMsg, pu2OffSet, u2NoOfEntries);

    while (u2NoOfEntries > 0)
    {
        u2NoOfEntries--;
        MEMSET (&Nd6Cache, 0, sizeof (tNd6CacheEntry));

        ND6_RM_GET_N_BYTE (pMsg, pu2OffSet, Nd6Cache.addr6.u1_addr,
                           IPVX_IPV6_ADDR_LEN);
        ND6_RM_GET_N_BYTE (pMsg, pu2OffSet, au1Addr, IP6_ENET_ADDR_LEN);
        ND6_RM_GET_1_BYTE (pMsg, pu2OffSet, Nd6Cache.u1AddrType);

        /* If AddrType is Anycast, get the recent unicast source addr */
        if (Nd6Cache.u1AddrType == ADDR6_ANYCAST)
        {
            ND6_RM_GET_N_BYTE (pMsg, pu2OffSet,
                               Nd6Cache.recentSrcAddr.u1_addr,
                               IPVX_IPV6_ADDR_LEN);
        }

        ND6_RM_GET_4_BYTE (pMsg, pu2OffSet, u4Index);
        Nd6Cache.pIf6 = Ip6ifGetEntry (u4Index);
        if (Nd6Cache.pIf6 == NULL)
        {
            return;
        }

        ND6_RM_GET_1_BYTE (pMsg, pu2OffSet, Nd6Cache.pIf6->u1IfType);
        ND6_RM_GET_1_BYTE (pMsg, pu2OffSet, Nd6Cache.u1RowStatus);
        ND6_RM_GET_1_BYTE (pMsg, pu2OffSet, Nd6Cache.u1ReachState);
        ND6_RM_GET_1_BYTE (pMsg, pu2OffSet, Nd6Cache.u1HwStatus);
        ND6_RM_GET_1_BYTE (pMsg, pu2OffSet, u1Action);
        ND6_RM_GET_1_BYTE (pMsg, pu2OffSet, Nd6Cache.u1SecureFlag);
        ND6_RM_GET_N_BYTE (pMsg, pu2OffSet, Nd6Cache.au1SentNonce,
                           ND6_NONCE_LENGTH);
        ND6_RM_GET_N_BYTE (pMsg, pu2OffSet, Nd6Cache.au1RcvdNonce,
                           ND6_NONCE_LENGTH);
        ND6_RM_GET_4_BYTE (pMsg, pu2OffSet, Nd6Cache.u4SendTSLast);
        ND6_RM_GET_4_BYTE (pMsg, pu2OffSet, Nd6Cache.u4SendRDLast);


        MEMSET (&Nd6RedNode, 0, sizeof (tNd6RedTable));
        Nd6RedNode.addr6 = Nd6Cache.addr6;
        Nd6RedNode.u4Index = u4Index;

        if (u1Action == ND6_RED_ADD_CACHE)
        {
            if (Nd6Cache.u1HwStatus == NP_PRESENT)
            {
                pNd6RedNode = RBTreeGet (gIp6GblInfo.Nd6RedTable,
                                         (tRBElem *) & Nd6RedNode);
                /* If the hardware status is NP_PRESENT, then the corresponding
                 * entry is deleted from the temporary RBTree and added to the
                 * software. If the hardware status is NP_NOT_PRESENT, then the
                 * corresponding entry is added to the temporary data structure */

                if (pNd6RedNode != NULL)
                {
                    RBTreeRem (gIp6GblInfo.Nd6RedTable, pNd6RedNode);
                    MemReleaseMemBlock ((tMemPoolId) gIp6GblInfo.
                                        i4Ip6Nd6DynMsgPoolId,
                                        (UINT1 *) pNd6RedNode);
                }
                pNd6CacheInfo = RBTreeGet (gNd6GblInfo.Nd6CacheTable,
                                           (tRBElem *) & Nd6Cache);
                if (pNd6CacheInfo != NULL)
                {
                    if (pNd6CacheInfo->u1ReachState == ND6C_STATIC)
                    {
                        Nd6UpdateStaticCache (pNd6CacheInfo, au1Addr,
                                              IP6_ENET_ADDR_LEN,
                                              Nd6Cache.u1ReachState);
                        pNd6CacheInfo->u1HwStatus = Nd6Cache.u1HwStatus;
                    }
                    else
                    {
                        pNd6cLinfo =
                            (tNd6CacheLanInfo *) (VOID *) pNd6CacheInfo->
                            pNd6cInfo;
                        MEMCPY (pNd6cLinfo->lladdr, au1Addr, IP6_ENET_ADDR_LEN);
						if (pNd6cLinfo->pNd6cEntry != NULL)
						{	
						    pNd6cLinfo->pNd6cEntry->u1SecureFlag 
										= Nd6Cache.u1SecureFlag;
							MEMCPY (pNd6cLinfo->pNd6cEntry->au1SentNonce,
									Nd6Cache.au1SentNonce, ND6_NONCE_LENGTH);
							MEMCPY (pNd6cLinfo->pNd6cEntry->au1RcvdNonce,
										Nd6Cache.au1RcvdNonce,ND6_NONCE_LENGTH);
							pNd6cLinfo->pNd6cEntry->u4SendTSLast = Nd6Cache.u4SendTSLast;
							pNd6cLinfo->pNd6cEntry->u4SendRDLast = Nd6Cache.u4SendRDLast;
						}
                        Nd6SetReachState (pNd6CacheInfo, Nd6Cache.u1ReachState);
                    }
                }
                else
                {
                    Nd6CreateCache (Nd6Cache.pIf6, &Nd6Cache.addr6, au1Addr,
                                    IP6_ENET_ADDR_LEN, Nd6Cache.u1ReachState, 
                                    Nd6Cache.u1SecureFlag);
                }
            }
            else
            {
                /* If the entry is not present in NP (this is applicable only in
                 * async NP case), then the entry is added to the temporary
                 * RBtree and not updated in the software */

                Nd6RedAddDynamicInfo (&Nd6Cache, au1Addr, ND6_RED_ADD_CACHE);
            }
        }
        else
        {
            /* If the hardware status is NP_PRESENT, then the corresponding
             * entry is deleted from the Hardware.If the hardware status is NP_NOT_PRESENT, 
             * then the corresponding entry is added to the temporary data structure */
            if (Nd6Cache.u1HwStatus == NP_PRESENT)
            {
                pNd6RedNode = RBTreeGet (gIp6GblInfo.Nd6RedTable,
                                         (tRBElem *) & Nd6RedNode);

                if (pNd6RedNode != NULL)
                {
                    RBTreeRem (gIp6GblInfo.Nd6RedTable, pNd6RedNode);
                    MemReleaseMemBlock ((tMemPoolId) gIp6GblInfo.
                                        i4Ip6Nd6DynMsgPoolId,
                                        (UINT1 *) pNd6RedNode);
                }
                pNd6CacheInfo = RBTreeGet (gNd6GblInfo.Nd6CacheTable,
                                           (tRBElem *) & Nd6Cache);
                if (pNd6CacheInfo != NULL)
                {
                    if ((pNd6CacheInfo->u1ReachState == ND6C_STATIC) ||
                        (pNd6CacheInfo->u1ReachState ==
                         ND6C_STATIC_NOT_IN_SERVICE))
                    {
                        continue;
                    }

                    DecrementNDCounters(pNd6CacheInfo->u1ReachState);

                    RBTreeRem (gNd6GblInfo.Nd6CacheTable, pNd6CacheInfo);
                    pNd6CacheInfo->pIf6->pIp6Cxt->u4Nd6CacheEntries--;
                    gNd6GblInfo.u4Nd6CacheEntries--;
                    gu4Nd6CacheEntries = IP6_ND_ENTRIES_IN_HW;

                   pNd6cLinfo = (tNd6CacheLanInfo *) (VOID *) pNd6CacheInfo->pNd6cInfo;

                   if(pNd6cLinfo)
                   {
                       if(&(pNd6cLinfo->timer.appTimer) != 0)
                       {
                           Ip6TmrStop (pNd6CacheInfo->pIf6->pIp6Cxt->u4ContextId, pNd6cLinfo->timer.u1Id,
                                   gIp6GblInfo.Ip6TimerListId,
                                   &(pNd6cLinfo->timer.appTimer));
                       }


                       Ip6RelMem (pNd6CacheInfo->pIf6->pIp6Cxt->u4ContextId,
                               (UINT2) gNd6GblInfo.i4Nd6cLanId,
                               (UINT1 *) pNd6cLinfo);
                   }

                    if (Ip6RelMem
                        (pNd6CacheInfo->pIf6->pIp6Cxt->u4ContextId,
                         (UINT2) gNd6GblInfo.i4Nd6CacheId,
                         (UINT1 *) pNd6CacheInfo) == IP6_FAILURE)
                    {
                        return;
                    }
                }
            }
            else
            {
                Nd6RedAddDynamicInfo (&Nd6Cache, au1Addr, ND6_RED_ADD_CACHE);
            }

        }
    }

    IP6_GBL_TRC_ARG (ND6_MOD_TRC, BUFFER_TRC, ND6_NAME,
                     "Exiting Nd6RedProcessDynamicInfo \r\n");
    return;
}

/************************************************************************/
/* Function Name      : Nd6RedAddDynamicInfo                            */
/*                                                                      */
/* Description        : This function adds the dynamic entries to the   */
/*                      global tree and this tree is scanned everytime  */
/*                      for sending the dynamic entry.                  */
/*                                                                      */
/* Input(s)           : pNd6Cache - ND Cache message                    */
/*                      u1Operation - add or delete entry               */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/
VOID
Nd6RedAddDynamicInfo (tNd6CacheEntry * pNd6Cache, UINT1 *llAddr, UINT1 u1Action)
{
    tNd6RedTable       *pNd6RedNode = NULL;
    tNd6RedTable       *pTmpNd6RedNode = NULL;
    tNd6HACacheMarker  *pNd6CacheMarker = NULL;
    tNd6RedTable        Nd6RedNode;
    INT4                i4RetVal = 0;
    UINT4               u4Result = 0;

    if (pNd6Cache == NULL)
    {
        return;
    }
    if (pNd6Cache->u1ReachState == ND6C_STATIC_NOT_IN_SERVICE)
    {
        return;
    }

    if ((ND6_GET_NODE_STATUS () != RM_STANDBY) &&
        ((gNd6RedGlobalInfo.u1BulkUpdStatus == ND6_HA_UPD_NOT_STARTED) ||
         (ND6_IS_STANDBY_UP () == OSIX_FALSE)))
    {
        return;
    }

    pNd6CacheMarker = &(gNd6RedGlobalInfo.Nd6HACacheMarker);
    i4RetVal = Ip6AddrCompare (pNd6CacheMarker->addr6, pNd6Cache->addr6);

    if ((gNd6RedGlobalInfo.u1BulkUpdStatus == ND6_HA_UPD_IN_PROGRESS) &&
        ((pNd6CacheMarker->u4Index < pNd6Cache->pIf6->u4Index) ||
         ((pNd6CacheMarker->u4Index == pNd6Cache->pIf6->u4Index) &&
          ((i4RetVal != IP6_ZERO) && (i4RetVal != IP6_ONE)))))
    {
        return;
    }
    ND6Lock();
    MEMSET (&Nd6RedNode, 0, sizeof (tNd6RedTable));
    Nd6RedNode.addr6 = pNd6Cache->addr6;
    Nd6RedNode.u4Index = pNd6Cache->pIf6->u4Index;
    pTmpNd6RedNode = RBTreeGet (gIp6GblInfo.Nd6RedTable,
                                (tRBElem *) & Nd6RedNode);
    if (pTmpNd6RedNode == NULL)
    {
        pNd6RedNode =
            (tNd6RedTable *) MemAllocMemBlk (gIp6GblInfo.i4Ip6Nd6DynMsgPoolId);
        if (pNd6RedNode == NULL)
        {
            ND6UnLock();
            return;
        }
        MEMSET (pNd6RedNode, 0, sizeof (tNd6RedTable));
        pNd6RedNode->addr6 = pNd6Cache->addr6;
        pNd6RedNode->u4Index = pNd6Cache->pIf6->u4Index;
        u4Result = RBTreeAdd (gIp6GblInfo.Nd6RedTable, pNd6RedNode);
    }
    else
    {
        pNd6RedNode = pTmpNd6RedNode;
    }

    pNd6RedNode->u1IfType = pNd6Cache->pIf6->u1IfType;
    pNd6RedNode->u1AddrType = pNd6Cache->u1AddrType;
    if (pNd6RedNode->u1AddrType == ADDR6_ANYCAST)
    {
        MEMCPY (&pNd6RedNode->recentSrcAddr, &pNd6Cache->recentSrcAddr,
                sizeof (tIp6Addr));
    }
    else
    {
        MEMSET (&pNd6RedNode->recentSrcAddr, 0, sizeof (tIp6Addr));
    }
    if (llAddr)
    {
        MEMCPY (pNd6RedNode->lladdr, llAddr, IP6_MAX_LLA_LEN);
    }

    pNd6RedNode->u1RowStatus = pNd6Cache->u1RowStatus;
    pNd6RedNode->u1HwStatus = pNd6Cache->u1HwStatus;
    pNd6RedNode->u1ReachState = pNd6Cache->u1ReachState;
    pNd6RedNode->u1Action = u1Action;
    pNd6RedNode->u1SecureFlag = pNd6Cache->u1SecureFlag;
	MEMCPY (pNd6RedNode->au1SentNonce, pNd6Cache->au1SentNonce, 
			ND6_NONCE_LENGTH); 
    MEMCPY (pNd6RedNode->au1RcvdNonce, pNd6Cache->au1RcvdNonce, 
			ND6_NONCE_LENGTH); 
    pNd6RedNode->u4SendTSLast = pNd6Cache->u4SendTSLast;
    pNd6RedNode->u4SendRDLast = pNd6Cache->u4SendRDLast;

    UNUSED_PARAM (u4Result);
    ND6UnLock();
    return;
}

/************************************************************************/
/* Function Name      : Nd6RedHwAudit                                   */
/*                                                                      */
/* Description        : This function does the hardware audit in two    */
/*                      approches.                                      */
/*                                                                      */
/*                      First:                                          */
/*                      When there is a transaction between standby and */
/*                      active node, the Nd6RedTable is walked, if      */
/*                      there  are any entries in the table, they are   */
/*                      verified with the hardware, if the entry is     */
/*                      present in the hardware, then the entry is      */
/*                      added to the sofware. If not, the entry is      */
/*                      deleted.                                        */
/*                                                                      */
/*                      Second:                                         */
/*                      When there is a transaction between standby and */
/*                      active node, the entries in the hardware are    */
/*                      retrived and checked with the software. If      */
/*                      is a mismatch in the entry, the entry in the    */
/*                      hardware is updated to the software.            */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/
VOID
Nd6RedHwAudit ()
{
#ifdef NPAPI_WANTED
    tNd6RedTable       *pNd6RedInfo = NULL;
    tNd6CacheEntry     *pNd6Cache = NULL;
    tNd6CacheEntry     *pPrevNd6Cache = NULL;
    tIp6If             *pIf6 = NULL;
    tNpNDCacheInput     Nd6Input;
    tNpNDCacheOutput    Nd6Output;
    UINT2               u2VlanId = 0;
    INT4                i4RetVal = 0;

    IP6_GBL_TRC_ARG (ND6_MOD_TRC, BUFFER_TRC, ND6_NAME,
                     "Entering Nd6RedHwAudit\r \n");
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gi4Ip6SysLogId, "Entering Nd6RedHwAudit"));
    /* If the audit level is optimized (applicable only in async NP calls) the
     * temporary RBtree is scanned and the entries in the RBtree are checked 
     * in the hardware. If present, then it is added in the software. If not
     * then the entry is ignored. In sync NP calls, there wont be any need to 
     * do the hardware audit in this manner as the entries are added to the
     * standby database only after hardware call is success */

    MEMSET (&Nd6Input, 0, sizeof (tNpNDCacheInput));
    MEMSET (&Nd6Output, 0, sizeof (tNpNDCacheOutput));

#ifdef OPTIMIZED_HW_AUDIT_WANTED
    pNd6RedInfo = RBTreeGetFirst (gIp6GblInfo.Nd6RedTable);
    while (pNd6RedInfo != NULL)
    {
        pIf6 = Ip6ifGetEntry (pNd6RedInfo->u4Index);
        if (pIf6 == NULL)
        {
            return;
        }
        Nd6Input.u4ContextId = pIf6->pIp6Cxt->u4ContextId;
        Nd6Input.Ip6Addr = pNd6RedInfo->addr6;
        if (Ipv6FsNpIpv6NeighCacheGet (Nd6Input, &Nd6Output) == FNP_SUCCESS)
        {
            pNd6Cache = Nd6IsCacheForAddr (pIf6, &pNd6RedInfo->addr6);
            if (pNd6Cache == NULL)
            {
                if (Ipv6FsNpIpv6NeighCacheDel (Nd6Input.u4ContextId,
                                               (UINT1 *) &pNd6RedInfo->addr6,
                                               pNd6RedInfo->u4Index) ==
                    FNP_FAILURE)
                {
                    IP6_GBL_TRC_ARG (ND6_MOD_TRC, ALL_FAILURE_TRC | BUFFER_TRC,
                                     ND6_NAME,
                                     "Nd6RedHwAudit: NP Neighbour cache deletion Failed \r \n");
                }
            DecrementNDCounters(pNd6Cache->u1ReachState);
            gu4Nd6CacheEntries = IP6_ND_ENTRIES_IN_HW;

            }
        }
        else
        {
            pNd6Cache = Nd6IsCacheForAddr (pIf6, &pNd6RedInfo->addr6);
            if (pNd6Cache != NULL)
            {
                if (CfaGetVlanId (pNd6Cache->pIf6->u4Index, &u2VlanId) ==
                    CFA_SUCCESS)
                {
                    if (Ipv6FsNpIpv6NeighCacheAdd
                        (pNd6Cache->pIf6->pIp6Cxt->u4ContextId,
                         (UINT1 *) &pNd6Cache->addr6, pNd6Cache->pIf6->u4Index,
                         NULL, 0, pNd6Cache->u1ReachState,
                         u2VlanId) == FNP_FAILURE)
                    {
                        IP6_GBL_TRC_ARG (ND6_MOD_TRC,
                                         ALL_FAILURE_TRC | BUFFER_TRC, ND6_NAME,
                                         "Nd6RedHwAudit: NP Neighbour cache Addition failed \r \n");
                    }
                    IncrementNDCounters(pNd6Cache->u1ReachState);
                    gu4Nd6CacheEntries = IP6_ND_ENTRIES_IN_HW;

                }

            }

        }
        RBTreeRem (gIp6GblInfo.Nd6RedTable, pNd6RedInfo);
        MemReleaseMemBlock (gIp6GblInfo.i4Ip6Nd6DynMsgPoolId,
                            (UINT1 *) pNd6RedInfo);
        pNd6RedInfo = RBTreeGetFirst (gIp6GblInfo.Nd6RedTable);
    }
    UNUSED_PARAM (pNd6Cache);
    UNUSED_PARAM (pPrevNd6Cache);
    UNUSED_PARAM (i4RetVal);
#else
    /* In normal audit, the software and the hardware entry is scanned */

    pNd6Cache = RBTreeGetFirst (gNd6GblInfo.Nd6CacheTable);
    i4RetVal = Ipv6FsNpIpv6NeighCacheGetNext (Nd6Input, &Nd6Output);
    while ((pNd6Cache != NULL) || (i4RetVal == FNP_SUCCESS))
    {
        if ((pNd6Cache == NULL) || ((i4RetVal == FNP_SUCCESS) &&
                                    (Ip6AddrCompare
                                     (pNd6Cache->addr6,
                                      Nd6Output.Ip6Addr) > IP6_ZERO)))
        {
            /* The entry is not present in software, and present in 
             * hardware, add the entry to the software and get next entry
             * in the hardware */
            pIf6 = Ip6ifGetEntry (Nd6Output.u4CfaIndex);
            if (pIf6 == NULL)
            {
                return;
            }
            Nd6CreateCache (pIf6, &Nd6Output.Ip6Addr,
                            (UINT1 *) Nd6Output.HwAddress,
                            IP6_ENET_ADDR_LEN, Nd6Output.u1ReachState);

            Nd6Input.u4ContextId = Nd6Output.u4ContextId;
            Nd6Input.Ip6Addr = Nd6Output.Ip6Addr;
            i4RetVal = Ipv6FsNpIpv6NeighCacheGetNext (Nd6Input, &Nd6Output);
        }
        else if ((i4RetVal != FNP_SUCCESS) || ((pNd6Cache != NULL) &&
                                               (Ip6AddrCompare
                                                (pNd6Cache->addr6,
                                                 Nd6Output.Ip6Addr) <
                                                IP6_ZERO)))
        {
            /* The entry is present in the software and not present in the
             * hardware, so delete the entry from the software and get
             * next entry in the software */
            pIf6 = pNd6Cache->pIf6;
            pIf6->pIp6Cxt->u4Nd6CacheEntries--;
            gNd6GblInfo.u4Nd6CacheEntries--;
            DecrementNDCounters(pNd6Cache->u1ReachState);
            gu4Nd6CacheEntries = IP6_ND_ENTRIES_IN_HW;

            pPrevNd6Cache = pNd6Cache;
            pNd6Cache = RBTreeGetNext (gNd6GblInfo.Nd6CacheTable,
                                       (tRBElem *) pPrevNd6Cache, NULL);
            if (pPrevNd6Cache->u1ReachState == ND6C_REACHABLE)
            {
                RBTreeRem (gNd6GblInfo.Nd6CacheTable, pPrevNd6Cache);

                if (Ip6RelMem
                    (pIf6->pIp6Cxt->u4ContextId,
                     (UINT2) gNd6GblInfo.i4Nd6CacheId,
                     (UINT1 *) pNd6Cache) == IP6_FAILURE)
                {
                    return;
                }
            }

        }
        else
        {
            /* If the entry is present in both the hardware and software,
             * scan for next entry in both */
            pPrevNd6Cache = pNd6Cache;
            pNd6Cache = RBTreeGetNext (gNd6GblInfo.Nd6CacheTable,
                                       (tRBElem *) pPrevNd6Cache, NULL);
            Nd6Input.u4ContextId = Nd6Output.u4ContextId;
            Nd6Input.Ip6Addr = Nd6Output.Ip6Addr;
            i4RetVal = Ipv6FsNpIpv6NeighCacheGetNext (Nd6Input, &Nd6Output);
            pPrevNd6Cache->u1HwStatus = NP_PRESENT;
        }
    }
    UNUSED_PARAM (pNd6RedInfo);
#endif
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gi4Ip6SysLogId, "Exiting Nd6RedHwAudit"));
    IP6_GBL_TRC_ARG (ND6_MOD_TRC, BUFFER_TRC, ND6_NAME,
                     "Exiting Nd6RedHwAudit\r \n");
#endif
    return;
}

/*-------------------------------------------------------------------+
 * Function           : Nd6RBTreeRedEntryCmp 
 *
 * Input(s)           : pRBElem, pRBElemIn
 *
 * Output(s)          : None.
 *
 * Returns            : IP6_RB_LESSER  if pRBElem <  pRBElemIn
 *                      IP6_RB_GREATER if pRBElem >  pRBElemIn
 *                      IP6_RB_EQUAL   if pRBElem == pRBElemIn
 * Action :
 * This procedure compares the two Redundancy cache entries in lexicographic
 * order of the indices of the Redundancy cache entry.
+-------------------------------------------------------------------*/

INT4
Nd6RBTreeRedEntryCmp (tRBElem * pRBElem, tRBElem * pRBElemIn)
{
    tNd6RedTable       *pNd6Red = pRBElem;
    tNd6RedTable       *pNd6RedIn = pRBElemIn;
    INT4                i4RetVal = 0;

    if (pNd6Red->u4Index < pNd6RedIn->u4Index)
    {
        return IP6_RB_LESSER;
    }
    else if (pNd6Red->u4Index > pNd6RedIn->u4Index)
    {
        return IP6_RB_GREATER;
    }

    i4RetVal = Ip6AddrCompare (pNd6Red->addr6, pNd6RedIn->addr6);

    if (i4RetVal == IP6_ZERO)
    {
        return IP6_RB_EQUAL;
    }
    else if (i4RetVal == IP6_ONE)
    {
        return IP6_RB_GREATER;
    }
    else
    {
        return IP6_RB_LESSER;
    }

}

/*****************************************************************************/
/* Function Name      : nd6RedHandleDynSyncAudit                             */
/*                                                                           */
/* Description        : This function Handles the Dynamic Sync-up Audit      */
/*                      event. This event is used to start the audit         */
/*                      between the active and standby units for the         */
/*                      dynamic sync-up happened                             */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
nd6RedHandleDynSyncAudit (VOID)
{
/*On receiving this event, RTM should execute show cmd and calculate checksum*/
    nd6ExecuteCmdAndCalculateChkSum ();
}

/*****************************************************************************/
/* Function Name      : nd6ExecuteCmdAndCalculateChkSum                     */
/*                                                                           */
/* Description        : This function Handles the execution of show commands */
/*                      and calculation of checksum with the output.         */
/*                      The calculated value is then being sent to RM.       */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
nd6ExecuteCmdAndCalculateChkSum (VOID)
{
    /*Execute CLI commands and calculate checksum */
    UINT2               u2AppId = RM_ND6_APP_ID;
    UINT2               u2ChkSum = 0;

    IP6_TASK_UNLOCK ();
#ifdef CLI_WANTED
    if (Nd6GetShowCmdOutputAndCalcChkSum (&u2ChkSum) == OSIX_FAILURE)
    {
        IP6_GBL_TRC_ARG (ND6_MOD_TRC, ALL_FAILURE_TRC, ND6_NAME,
                         "Checksum of calculation failed for ND6\n");
        IP6_TASK_LOCK ();
        return;
    }
#endif /* CLI_WANTED */
#ifdef L2RED_WANTED
    if ((RmEnqChkSumMsgToRmFromApp (u2AppId, u2ChkSum)) == RM_FAILURE)
    {
        IP6_GBL_TRC_ARG (ND6_MOD_TRC, ALL_FAILURE_TRC, ND6_NAME,
                         "Sending checkum to RM failed\n");
        IP6_TASK_LOCK ();
        return;
    }
#else
    UNUSED_PARAM (u2AppId);
    UNUSED_PARAM (u2ChkSum);
#endif
    IP6_TASK_LOCK ();
    return;
}

/*****************************************************************************/
/* Function Name      : nd6GetShowCmdOutputAndCalcChkSum                     */
/*                                                                           */
/* Description        : This funcion handles the execution of show commands  */
/*                      and calculation of checksum with the output.         */
/*                      The calculated value is then being sent to RM.       */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : pu2SwAudChkSum                                       */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : IP6_SUCESS/IP6_FAILURE                             */
/*****************************************************************************/
INT4
Nd6GetShowCmdOutputAndCalcChkSum (UINT2 *pu2SwAudChkSum)
{
#if (defined CLI_WANTED) && (defined RM_WANTED)

    if (ND6_GET_NODE_STATUS () == RM_ACTIVE)
    {
        if (Nd6CliGetShowCmdOutputToFile ((UINT1 *) ND6_AUDIT_FILE_ACTIVE) !=
            IP6_SUCCESS)
        {
            IP6_GBL_TRC_ARG (ND6_MOD_TRC, ALL_FAILURE_TRC, ND6_NAME,
                             "GetShRunFile Failed\n");
            return IP6_FAILURE;
        }
        if (Nd6CliCalcSwAudCheckSum
            ((UINT1 *) ND6_AUDIT_FILE_ACTIVE, pu2SwAudChkSum) != IP6_SUCCESS)
        {
            IP6_GBL_TRC_ARG (ND6_MOD_TRC, ALL_FAILURE_TRC, ND6_NAME,
                             "Nd6CliCalcSwAudCheckSum Failed\n");
            return IP6_FAILURE;
        }
    }
    else if (ND6_GET_NODE_STATUS () == RM_STANDBY)
    {
        if (Nd6CliGetShowCmdOutputToFile ((UINT1 *) ND6_AUDIT_FILE_STDBY) !=
            IP6_SUCCESS)
        {
            IP6_GBL_TRC_ARG (ND6_MOD_TRC, ALL_FAILURE_TRC, ND6_NAME,
                             "GetShRunFile Failed\n");
            return IP6_FAILURE;
        }
        if (Nd6CliCalcSwAudCheckSum
            ((UINT1 *) ND6_AUDIT_FILE_STDBY, pu2SwAudChkSum) != IP6_SUCCESS)
        {
            IP6_GBL_TRC_ARG (ND6_MOD_TRC, ALL_FAILURE_TRC, ND6_NAME,
                             "Nd6CliCalcSwAudCheckSum Failed\n");
            return IP6_FAILURE;
        }
    }
    else
    {
        return IP6_FAILURE;
    }
#else
    UNUSED_PARAM (pu2SwAudChkSum);
#endif
    return IP6_SUCCESS;
}

#endif
