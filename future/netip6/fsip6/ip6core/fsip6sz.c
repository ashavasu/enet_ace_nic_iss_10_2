/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fsip6sz.c,v 1.7 2014/06/23 11:37:50 siva Exp $
 *
 * Description: This files for RADIUS.
 *******************************************************************/

#define _IP6SZ_C
#include "ip6inc.h"
extern INT4         IssSzRegisterModuleSizingParams (CHR1 * pu1ModName,
                                                     tFsModSizingParams *
                                                     pModSizingParams);
extern INT4         IssSzRegisterModulePoolId (CHR1 * pu1ModName,
                                               tMemPoolId * pModPoolId);
INT4
Ip6SizingMemCreateMemPools ()
{
    INT4                i4RetVal;
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < IP6_MAX_SIZING_ID; i4SizingId++)
    {
        i4RetVal = MemCreateMemPool (FsIP6SizingParams[i4SizingId].u4StructSize,
                                     FsIP6SizingParams[i4SizingId].
                                     u4PreAllocatedUnits,
                                     MEM_DEFAULT_MEMORY_TYPE,
                                     &(IP6MemPoolIds[i4SizingId]));
        if (i4RetVal == (INT4) MEM_FAILURE)
        {
            Ip6SizingMemDeleteMemPools ();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

INT4
Ip6SzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, FsIP6SizingParams);
    IssSzRegisterModulePoolId (pu1ModName, IP6MemPoolIds);
    return OSIX_SUCCESS;
}

VOID
Ip6SizingMemDeleteMemPools ()
{
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < IP6_MAX_SIZING_ID; i4SizingId++)
    {
        if (IP6MemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (IP6MemPoolIds[i4SizingId]);
            IP6MemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
