/********************************************************************
 * Copyright (C) 2014 Aricent Inc . All Rights Reserved
 *
 * $Id: nd6secure.c,v 1.3 2015/09/17 10:17:54 siva Exp $
 *
 * Description: This file contains Secure ND related routines
 *           
 *******************************************************************/

#include "ip6inc.h"
#include "utilrand.h"
#include "fsssl.h"
#include "shaarprt.h"

/*
 * RFC 3971, Section-5.2
 * 128-bit CGA message type tag value for SEND
 */
UINT1 gau1CgaTag[ND6_SEND_CGA_TAG] = {0x08, 0x6F, 0xCA, 0x5E,
                                      0x10, 0xB2, 0x00, 0xC9,
                                      0x9C, 0x8C, 0xE0, 0x01,
                                      0x64, 0x27, 0x7C, 0x08
                                     };

/*
 * All zeros array to check the 16*Sec leftmost bits
 * of the second hash value - Hash2 are zero,
 * in CGA generation and verification
 */
UINT1 CGA_ZEROS[ND6_SEND_CGA_MAX_SEC * ND6_SEND_CGA_SEC_MULT];

/********************************************************************
 * DESCRIPTION : This function generates time stamp to be filled in
 *               TimeStamp option in Secure ND messages.
 *
 * INPUTS      : u4ContextId - Context Id
 *               pBuf        - Pointer to IPv6 Packet
 *
 * OUTPUTS     : pu4Len      - Pointer to Packet Length
 *
 * RETURNS     : IP6_SUCCESS/IP6_FAILURE
 *******************************************************************/
INT4
Ip6SeNDAddTimeStamp (UINT4 u4ContextId, tCRU_BUF_CHAIN_HEADER *pBuf,
                     UINT4 *pu4Len)
{
    tUtlSysPreciseTime  SysPreciseTime;
    tNd6SendTimeStamp   TimeStamp;

    MEMSET (&SysPreciseTime, IP6_ZERO, sizeof (tUtlSysPreciseTime));
    MEMSET (&TimeStamp, IP6_ZERO, sizeof (tNd6SendTimeStamp));

    /* get the time in seconds after epoch */
    TmrGetPreciseSysTime (&SysPreciseTime);

    /* fill the timestamp option header */
    TimeStamp.u1Type = ND6_SEND_TIMESTAMP;
    TimeStamp.u1Len = sizeof (tNd6SendTimeStamp) / ND6_EXT_HDR_LEN_UNIT;

    /* copy epoch seconds into TimeStamp */
    MEMCPY (&TimeStamp.au1TimeStamp[IP6_TWO],
            &(SysPreciseTime.u4Sec), IP6_FOUR);

    /* write the timestamp option into ND6 message buffer */
    if (Ip6BufWrite (pBuf, (UINT1 *) &(TimeStamp),
                     IP6_BUF_WRITE_OFFSET (pBuf),
                     sizeof (tNd6SendTimeStamp), TRUE) == IP6_FAILURE)
    {
        IP6_TRC_ARG2 (u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC,
                      ND6_NAME, "Ip6SeNDAddTimeStamp "
                      "BufWrite Failed, BufPtr %p Offset %d\n",
                      pBuf, IP6_BUF_WRITE_OFFSET (pBuf));

        Ip6BufRelease (u4ContextId, pBuf, FALSE, ND6_MODULE);
        return (IP6_FAILURE);
    }
    *pu4Len += sizeof (tNd6SendTimeStamp);

    return IP6_SUCCESS;
}

/********************************************************************
 * DESCRIPTION : This function generates random nonce to be filled 
 *               in Nonce option in Secure ND messages.
 *
 * INPUTS      : pIf6       - Pointer to Logical Interface
 *               pDst       - Pointer to IPv6 destination address
 *               pBuf       - Pointer to the IPv6 Packet
 *               u1SeNDFlag - Secure ND Message Flag
 *
 * OUTPUTS     : pu4Len     - Pointer to Packet Length
 *
 * RETURNS     : IP6_SUCCESS/IP6_FAILURE
 *******************************************************************/
INT4
Ip6SeNDAddNonce (tIp6If *pIf6, tIp6Addr *pDst,
                 tCRU_BUF_CHAIN_HEADER *pBuf,
                 UINT1 u1SeNDFlag, UINT4 *pu4Len)
{
    tNd6SendNonce   Nonce;
    tNd6CacheEntry *pCacheEntry = NULL;
    UINT4           u4ContextId = IP6_ZERO;
    INT4            i4RetVal = IP6_ZERO;
    UINT1	    au1Zero[ND6_NONCE_LENGTH];
    
    MEMSET (&Nonce, IP6_ZERO, sizeof (tNd6SendNonce));
    MEMSET (au1Zero, IP6_ZERO, ND6_NONCE_LENGTH);


    if (ND6_SEND_RS_MSG != u1SeNDFlag && ND6_SEND_SOL_RA_MSG != u1SeNDFlag) 
    {
    	/* Check whether an cache entry already exists */
    	pCacheEntry = Nd6IsCacheForAddr (pIf6, pDst);
    	if (pCacheEntry == NULL)
    	{
            IP6_TRC_ARG (u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC,
                         ND6_NAME, "Ip6SeNDAddNonce No Cache Entry\n");

            Ip6BufRelease (u4ContextId, pBuf, FALSE, ND6_MODULE);
            return IP6_FAILURE;
    	}
    }
    /* get the context from interface */
    u4ContextId = pIf6->pIp6Cxt->u4ContextId; 

    /* fill the nonce option header */
    Nonce.u1Type = ND6_SEND_NONCE;
    Nonce.u1Len = sizeof (tNd6SendNonce) / ND6_EXT_HDR_LEN_UNIT;

    
    /*
     * for solicitations generate and include a new nonce
     */ 
    if (u1SeNDFlag == ND6_SEND_MSG || u1SeNDFlag == ND6_SEND_RS_MSG) 
    {
        /* Generate a Random Number */
        i4RetVal = UtilRandNumGen (Nonce.au1Nonce, ND6_SEND_NONCE_RAND);
        if (OSIX_SUCCESS == i4RetVal)
        {
            i4RetVal = Ip6BufWrite (pBuf, (UINT1 *) &(Nonce),
                                    IP6_BUF_WRITE_OFFSET (pBuf),
                                    sizeof (tNd6SendNonce), TRUE);
            if (IP6_FAILURE == i4RetVal)
            {
                IP6_TRC_ARG2 (u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC,
                              ND6_NAME, "Ip6SeNDAddNonce "
                              "BufWrite Failed! BufPtr %p Offset %d\n",
                              pBuf, IP6_BUF_WRITE_OFFSET (pBuf));

                Ip6BufRelease (u4ContextId, pBuf, FALSE, ND6_MODULE);
                return (IP6_FAILURE);
            }
	    if (u1SeNDFlag == ND6_SEND_RS_MSG)
            {
		 MEMCPY (pIf6->au1SentNonce, Nonce.au1Nonce,
                         ND6_NONCE_LENGTH);
            }
	    else
	    {
            	MEMCPY (pCacheEntry->au1SentNonce, Nonce.au1Nonce,
                        ND6_NONCE_LENGTH);
	    }
        }
        else
        {
            IP6_TRC_ARG (u4ContextId, ND6_MOD_TRC,
                         DATA_PATH_TRC, ND6_NAME, "Ip6SeNDAddNonce "
                         "Generation of Nonce Failed \n");

            Ip6BufRelease (u4ContextId, pBuf, FALSE, ND6_MODULE);
            return IP6_FAILURE;
        }
    }
    else
    {
        /*
         * this is a response, include the nonce incase
         * we received in secure solicitation
         */
	if (u1SeNDFlag == ND6_SEND_SOL_RA_MSG)
	{
		if (MEMCMP (pIf6->au1RcvdNonce, au1Zero, ND6_NONCE_LENGTH) == IP6_ZERO)
		{
		    return IP6_SUCCESS;
		}
		MEMCPY (Nonce.au1Nonce, pIf6->au1RcvdNonce,
                	ND6_NONCE_LENGTH);
		MEMSET (pIf6->au1RcvdNonce, 0, ND6_NONCE_LENGTH);
	}
	else
	{
                if (MEMCMP (pCacheEntry->au1RcvdNonce, au1Zero, ND6_NONCE_LENGTH) == IP6_ZERO)
                {
                    return IP6_SUCCESS;
                }
        	MEMCPY (Nonce.au1Nonce, pCacheEntry->au1RcvdNonce,
                	ND6_NONCE_LENGTH);
	}
        i4RetVal = Ip6BufWrite (pBuf, (UINT1 *) &(Nonce),
                                IP6_BUF_WRITE_OFFSET (pBuf),
                                sizeof (tNd6SendNonce), TRUE);
        if (IP6_FAILURE == i4RetVal)
        {
            IP6_TRC_ARG2 (u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC,
                          ND6_NAME, "Ip6SeNDAddNonce "
                          "BufWrite Failed, BufPtr %p Offset %d\n",
                          pBuf, IP6_BUF_WRITE_OFFSET (pBuf));

            Ip6BufRelease (u4ContextId, pBuf, FALSE, ND6_MODULE);
            return (IP6_FAILURE);
        }
    }
    *pu4Len += sizeof (tNd6SendNonce);

    return IP6_SUCCESS;
}

/********************************************************************
 * DESCRIPTION : This function verifies received Nonce value with
 *               the one stored for the neighbor
 *
 * INPUTS      : pIf6        - Pointer to the Logical Interface
 *               pSrc        - Pointer to IPv6 source address
 *               pRcvdNonce  - Pointer to Nonce Option recevied
 *
 * OUTPUTS     : None
 *
 * RETURNS     : IP6_SUCCESS
 *               IP6_FAILURE
 *******************************************************************/
INT4
Ip6SeNDVerifyNonce (tIp6If *pIf6, tIp6Addr *pSrc,
                   tNd6SendNonce *pRcvdNonce, UINT1 u1SeNDFlag)
{
    tNd6CacheEntry *pCacheEntry = NULL;
    UINT4 u4ContextId = IP6_ZERO;

    /* get the context from interface */
    u4ContextId = pIf6->pIp6Cxt->u4ContextId;
  
    if (u1SeNDFlag == ND6_SEND_RA_MSG)
    {
 	 if ((MEMCMP (pIf6->au1SentNonce, pRcvdNonce->au1Nonce,
              ND6_NONCE_LENGTH) != IP6_ZERO))
	 {
             IP6_TRC_ARG (u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC,
             	     ND6_NAME, "Ip6SeNDVerifyNonce Failed\n");
	     return IP6_FAILURE;
    	 }
    }

    else
    {
    	/* Check whether an cache entry already exists */
    	pCacheEntry = Nd6IsCacheForAddr (pIf6, pSrc);
    	if (pCacheEntry == NULL)
    	{
        	IP6_TRC_ARG (u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC,
                     ND6_NAME, "Ip6SeNDVerifyNonce No Cache Entry\n");
        	return IP6_FAILURE;
    	}	

    	if ((MEMCMP (pCacheEntry->au1SentNonce, pRcvdNonce->au1Nonce,
                     ND6_NONCE_LENGTH) != IP6_ZERO))
    	{
        	IP6_TRC_ARG (u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC,
                	     ND6_NAME, "Ip6SeNDVerifyNonce Failed\n");

	        return IP6_FAILURE;
    	}
    }
    return IP6_SUCCESS;
}

/********************************************************************
 * DESCRIPTION : This function verifies the TimeStamp value in 
 *               Secure ND messages.
 *
 * INPUTS      : pIf6           - Pointer to the Logical Interface
 *               pDst           - Pointer to IPv6 destination address
 *               pRcvdTimestamp - Pointer to the TimeStamp option
 *                                 received 
 *
 * OUTPUTS     : pu4TSNew       - Pointer to TimeStamp received 
 *
 * RETURNS     : IP6_SUCCESS/IP6_FAILURE
 *******************************************************************/
INT4
Ip6SeNDVerifyTimeStamp (tIp6If *pIf6, tIp6Addr *pDst,
                        tNd6SendTimeStamp *pRcvdTimeStamp,
                        UINT4 *pu4TSNew)
{
    tUtlSysPreciseTime  SysPreciseTime;
    tNd6CacheEntry     *pCacheEntry = NULL;
    UINT4               u4ContextId = IP6_ZERO;
    UINT4               u4TSNew = IP6_ZERO;
    UINT4               u4RDNew = IP6_ZERO;

    MEMSET (&SysPreciseTime, IP6_ZERO, sizeof (tUtlSysPreciseTime));

    /* get the context from interface */
    u4ContextId = pIf6->pIp6Cxt->u4ContextId;

    /* get the time in seconds after epoch */
    TmrGetPreciseSysTime (&SysPreciseTime);

    MEMCPY (&u4RDNew, &(SysPreciseTime.u4Sec), IP6_FOUR);
    MEMCPY (&u4TSNew, pRcvdTimeStamp->au1TimeStamp, IP6_FOUR);

    /* Check whether an cache entry already exists */
    pCacheEntry = Nd6IsCacheForAddr (pIf6, pDst);
    if (pCacheEntry != NULL)
    {
        /* cache entry exists */
        if ((pCacheEntry->u4SendTSLast == IP6_ZERO) || 
            (pCacheEntry->u4SendRDLast == IP6_ZERO))
        {
            /* first packet recevied from neighbor */
            pCacheEntry->u4SendTSLast = u4TSNew;
            pCacheEntry->u4SendRDLast = u4RDNew;
        }
        else if (((u4TSNew + pIf6->u2SeNDFuzzTime) >
                 (pCacheEntry->u4SendTSLast +
                 (u4RDNew - pCacheEntry->u4SendRDLast) *
                 (IP6_ONE - (UINT4)(pIf6->u1SeNDDriftTime/IP6_HUNDRED)) -
                 pIf6->u2SeNDFuzzTime))
            && (u4TSNew > pCacheEntry->u4SendTSLast))
        {
            /* verification success, updated the TS and RD Last */
            pCacheEntry->u4SendTSLast = u4TSNew;
            pCacheEntry->u4SendRDLast = u4RDNew;
        }
        else
        {
            /* verification fails, */
            IP6_TRC_ARG (u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC,
                         ND6_NAME, "Ip6SeNDVerifyTimeStamp Failed\n");
            return IP6_FAILURE;
        }
    }
    else
    {
        /* 
         * Timestamp is required to be returned in order to 
         * be filled in the  IP6 cache entry structure,
         * once cache is created
         */
        *pu4TSNew = u4TSNew;
    }
    return IP6_SUCCESS;
}

/********************************************************************
 * DESCRIPTION : This function process CPS message received 
 *
 * INPUTS      : pIf6  - Pointer to the Logical Interface
 *               pIp6  - Pointer to the IPv6 Header
 *               pBuf  - Pointer to the IPv6 Packet
 *               u2Len - Packet Length
 *
 * OUTPUTS     : None
 *
 * RETURNS     : IP6_SUCCESS/IP6_FAILURE
 *******************************************************************/
INT4
Nd6RcvCertPathSol (tIp6If *pIf6, tIp6Hdr *pIp6,
                   tCRU_BUF_CHAIN_HEADER *pBuf, UINT2 u2Len)
{
    tNd6SendCertPathSol  Nd6CertPathSol;
    tNd6SendCertPathSol *pNd6CertPathSol = NULL;
    UINT1               *pu1TrustAnchorBuffer = NULL;
    UINT4                u4Nd6SendOffset = IP6_ZERO;
    UINT4                u4ContextId = IP6_ZERO;
    UINT4                u4TAOptLen = IP6_ZERO;
    UINT4                u4TACheck = IP6_ZERO;
    UINT1                u1Copy = IP6_ZERO;

    MEMSET (&Nd6CertPathSol, IP6_ZERO, sizeof (tNd6SendCertPathSol));

    /* get the context from the interface */
    u4ContextId = pIf6->pIp6Cxt->u4ContextId;

    /* memory allocation for linear buffer */
    pu1TrustAnchorBuffer = Ip6GetMem (u4ContextId,
                                      gIp6GblInfo.i4Ip6Nd6SecurePoolId);
    if (NULL == pu1TrustAnchorBuffer)
    {
        IP6_TRC_ARG2 (u4ContextId, ND6_MOD_TRC, BUFFER_TRC,
                      ND6_NAME, "Nd6RcvCertPathSol "
                      "Buffer Failed, BufPtr %p Offset %d\n",
                      pBuf, u4Nd6SendOffset);

        return (IP6_FAILURE);
    }
    MEMSET (pu1TrustAnchorBuffer, IP6_ZERO, ND6_SEND_MAX_BUF_SIZE);

    /* Extract CPS Message from the buffer */
    u4Nd6SendOffset = IP6_BUF_READ_OFFSET (pBuf) - sizeof (tIcmp6PktHdr);
    IP6_BUF_READ_OFFSET (pBuf) -= sizeof (tIcmp6PktHdr);

    if ((pNd6CertPathSol = (tNd6SendCertPathSol *) Ip6BufRead
         (pBuf, (UINT1 *) &Nd6CertPathSol, u4Nd6SendOffset,
          sizeof (tNd6SendCertPathSol), u1Copy)) == NULL)
    {
        IP6_TRC_ARG2 (u4ContextId, ND6_MOD_TRC, BUFFER_TRC,
                      ND6_NAME, "Nd6RcvCertPathSol "
                      " BufRead Failedi, BufPtr %p Offset %d\n",
                      pBuf, u4Nd6SendOffset);

        Ip6RelMem(u4ContextId, (UINT2)gIp6GblInfo.i4Ip6Nd6SecurePoolId,
                  pu1TrustAnchorBuffer);
        return (IP6_FAILURE);
    }

    u4TACheck = ND6_TA_SUPPORTED;
    /* Validate the CPS message */
    if (Nd6ValCertPathSolInCxt (pIf6, pIp6, pBuf, pNd6CertPathSol,
                                pu1TrustAnchorBuffer, u2Len, &u4TACheck,
								&u4TAOptLen)
        == IP6_FAILURE)
    {
        IP6_TRC_ARG (u4ContextId, ND6_MOD_TRC, BUFFER_TRC,
                      ND6_NAME, "Nd6RcvCertPathSol Validation Fails\n");

        Ip6RelMem(u4ContextId, (UINT2)gIp6GblInfo.i4Ip6Nd6SecurePoolId,
                  pu1TrustAnchorBuffer);

        /* Increment the count on ICMP bad packets received */
        ICMP6_INC_IN_ERRS (pIf6->pIp6Cxt->Icmp6Stats);
        ICMP6_INTF_INC_IN_ERRS (pIf6);
        return (IP6_FAILURE);
    }

    /* Send Solicited CPA message as response */
    if (Nd6SeNDSolCertPathAdv (pIf6, pIp6, pNd6CertPathSol,
                               pu1TrustAnchorBuffer, u4TAOptLen,
                               u4TACheck) == IP6_FAILURE)
    {
        IP6_TRC_ARG (u4ContextId, ND6_MOD_TRC, BUFFER_TRC,
                     ND6_NAME, "Nd6RcvCertPathSol "
                     " Sending Solicited CPA Failed\n");

        Ip6RelMem (u4ContextId, (UINT2)gIp6GblInfo.i4Ip6Nd6SecurePoolId,
                   pu1TrustAnchorBuffer);
        return (IP6_FAILURE);
    }
    (IP6_SEND_IF_OUT_STATS (pIf6, COUNT_CPA)).u4TaOptPkts++;
    (IP6_SEND_IF_OUT_STATS (pIf6, COUNT_CPA)).u4CertOptPkts++;

    Ip6RelMem(u4ContextId, (UINT2)gIp6GblInfo.i4Ip6Nd6SecurePoolId,
              pu1TrustAnchorBuffer);
    return (IP6_SUCCESS);
}

/********************************************************************
 * DESCRIPTION : This function validates the CPS message received
 *
 * INPUTS      : pIp6                 -  Pointer to the IPv6 header
 *               pNd6CertPathSol      -  Pointer to the CPS
 *               u2Len                -  Length of the Packet.
 *               pBuf                 -  Pointer to the IPv6 Packet
 *               pu1TrustAnchorBuffer -  Pointer to Trust Anchor
 *               u4TAOptLen           -  Trust Anchor Option Length
 *
 * OUTPUTS     : pu4TACheck           -  Pointer to Trust Anchor Check
 *
 * RETURNS     : IP6_SUCCESS/IP6_FAILURE
 *******************************************************************/
INT4
Nd6ValCertPathSolInCxt (tIp6If *pIf6, tIp6Hdr *pIp6,
                        tCRU_BUF_CHAIN_HEADER *pBuf,
                        tNd6SendCertPathSol *pNd6CertPathSol,
                        UINT1 *pu1TrustAnchorBuffer, UINT2 u2Len,
                        UINT4 *pu4TACheck, UINT4 *pu4TAOptLen)
{
    tNd6TaExtnHdr   Nd6TaExtnHdr;
    tNd6TaExtnHdr  *pNd6TaExtnHdr = NULL;
    UINT1          *pu1Buffer = NULL;
    UINT4           u4ContextId = IP6_ZERO;
    UINT4           u4ExtOffset = IP6_ZERO;
    UINT4           u4SeNdTAOptLen = IP6_ZERO;

    MEMSET (&Nd6TaExtnHdr, IP6_ZERO, sizeof (Nd6TaExtnHdr));

    /* get the context from interface */
    u4ContextId = pIf6->pIp6Cxt->u4ContextId; 

    if (Nd6ValidateMsgHdrInCxt (pIf6->pIp6Cxt, pIp6,
           &pNd6CertPathSol->icmp6Hdr) == IP6_FAILURE)
    {
        IP6_TRC_ARG (u4ContextId, ND6_MOD_TRC,
                     MGMT_TRC | ALL_FAILURE_TRC, ND6_NAME,
                     "Validation of Msg Header Failed\n");
        return (IP6_FAILURE);
    }

    if (u2Len < sizeof (tNd6SendCertPathSol))
    {
        IP6_TRC_ARG1 (u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC,
                      ND6_NAME, "Nd6ValCertPathSolInCxt "
                      "Incorrect Len %d\n", u2Len);
        return (IP6_FAILURE);
    }

    if ((pNd6CertPathSol->u2Identifier) == IPV6_MIN_AS_NUM)
    {
        IP6_TRC_ARG1 (u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC,
                      ND6_NAME, "Nd6ValidateCertPathSol "
                      "CPS Fail- Identifier Field%d :\n",
                      pNd6CertPathSol->u2Identifier);
        return (IP6_FAILURE);
    }

    if ((pNd6CertPathSol->u2Component) != IPV6_MAX_AS_NUM)
    {
        IP6_TRC_ARG1 (u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC,
                      ND6_NAME, "Nd6ValidateCertPathSol "
                      "CPS Fail- Component Field%d :\n",
                      pNd6CertPathSol->u2Component);
        return (IP6_FAILURE);
    }

    u4ExtOffset = IP6_BUF_READ_OFFSET (pBuf);

    /* Get the Trust Anchor option Header */
    if ((pNd6TaExtnHdr = (tNd6TaExtnHdr *) Ip6BufRead (pBuf,
                                                       (UINT1 *) &Nd6TaExtnHdr,
                                                       IP6_BUF_READ_OFFSET
                                                       (pBuf),
                                                       sizeof (tNd6TaExtnHdr),
                                                       TRUE)) == NULL)
    {
        IP6_TRC_ARG2 (u4ContextId, ND6_MOD_TRC, BUFFER_TRC,
                      ND6_NAME, "Nd6ValCertPathSolInCxt "
                      "Process TA Extn - BufRead Fail "
                      "BufPtr %p Offset %d\n", pBuf, u4ExtOffset);
        return (IP6_FAILURE);
    }

    if ((Nd6TaExtnHdr.u1Type) != ND6_SEND_TRUST_ANCHOR)
    {
        IP6_TRC_ARG1 (u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC,
                      ND6_NAME, "Nd6ValCertPathSolInCxt "
                      "Invalid Len %d\n", Nd6TaExtnHdr.u1Type);
        return (IP6_FAILURE);
    }

    if ((Nd6TaExtnHdr.u1NameType) != ND6_SEND_TA_NAMETYPE)
    {
        IP6_TRC_ARG1 (u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC,
                      ND6_NAME, "Nd6ValCertPathSolInCxt "
                      "Name Type of TA -%d MUST be 1\n",
                      Nd6TaExtnHdr.u1NameType);
        return (IP6_FAILURE);
    }

    u4SeNdTAOptLen = (UINT4) ((Nd6TaExtnHdr.u1Len * IP6_EIGHT) 
								- IP6_TYPE_SEND_LEN_BYTES 
								- Nd6TaExtnHdr.u1PadLen);

    if ((pu1Buffer =
         (UINT1 *) Ip6BufRead (pBuf,
                               (UINT1 *) pu1TrustAnchorBuffer +
                               IP6_TYPE_SEND_LEN_BYTES,
                               IP6_BUF_READ_OFFSET (pBuf), u4SeNdTAOptLen,
                               TRUE)) == NULL)
    {
        IP6_TRC_ARG2 (u4ContextId, ND6_MOD_TRC, BUFFER_TRC,
                      ND6_NAME, "Nd6ValCertPathSolInCxt " 
                      "Process TA Extn - BufRead Fail "
                      "BufPtr %p Offset %d\n", pBuf, u4ExtOffset);
        return (IP6_FAILURE);
    }

    MEMCPY (pu1TrustAnchorBuffer, &Nd6TaExtnHdr, sizeof (tNd6TaExtnHdr));
#ifdef SSL_WANTED
    if (SSLArTACertNameVerify (&pu1Buffer, (INT4)u4SeNdTAOptLen) == SSL_FAILURE)
    {
        IP6_TRC_ARG (u4ContextId, ND6_MOD_TRC, BUFFER_TRC,
                     ND6_NAME, "Nd6ValCertPathSolInCxt "
                     "Verification of TA Name Failed\n");
        *pu4TACheck = ND6_TA_NOT_SUPPORTED;
        return IP6_FAILURE;
    }

	u4SeNdTAOptLen += (Nd6TaExtnHdr.u1PadLen + 
                           (UINT4) IP6_TYPE_SEND_LEN_BYTES);
	*pu4TAOptLen = u4SeNdTAOptLen;
#else
	UNUSED_PARAM(pu1Buffer);
	UNUSED_PARAM(u4SeNdTAOptLen);
    UNUSED_PARAM(pu4TACheck);
	UNUSED_PARAM(pu4TAOptLen);
	return IP6_FAILURE;
#endif
	UNUSED_PARAM(pNd6TaExtnHdr);
    return (IP6_SUCCESS);
}

/********************************************************************
 * DESCRIPTION : This function sends the Solicited CPA as response
 *               to the received CPS message
 *
 * INPUTS      : pIf6                -  Pointer to the interface.
 *               pIp6                -  Pointer to the Ipv6 header.
 *               pNd6CertPathSol     -  Pointer to the CPS.
 *               pu1TrustAnchorBuffer-  Pointer to Trust Anchor.
 *               u4TAOptLen          -  Trust Anchor Option Length
 *
 * OUTPUTS     : None.
 *
 * RETURNS     : IP6_SUCCESS/IP6_FAILURE.
 *******************************************************************/
INT4
Nd6SeNDSolCertPathAdv (tIp6If *pIf6, tIp6Hdr *pIp6,
                       tNd6SendCertPathSol *pNd6CertPathSol,
                       UINT1 *pu1TrustAnchorBuffer, UINT4 u4TAOptLen,
                       UINT4 u4TACheck)
{
    tIp6Addr         DstAddr6;
    tIp6Addr        *pIp6SrcAddr = NULL;
    tNd6CacheEntry  *pNd6cEntry = NULL;
    INT4             i4RetVal = IP6_FAILURE;

    MEMSET (&DstAddr6, IP6_ZERO, sizeof (tIp6Addr));

    /*
     * Destination address in CPA is set to all-nodes Multicast if
     * the source address of CPS being unspecified, otherwise it is
     * set to the Solicited-Node multicast address corresponding to the
     * source address RFC:3971-Section 6.4.5
     */
    if (IS_ADDR_UNSPECIFIED (pIp6->srcAddr))
    {
        SET_ALL_NODES_MULTI (DstAddr6);
    }
    else
    {
        GET_ADDR_SOLICITED (pIp6->srcAddr, DstAddr6);
    }

    pIp6SrcAddr = Ip6GetLlocalAddr (pIf6->u4Index);
    if (pIp6SrcAddr != NULL)
    {
        /* Send the CPA message */
        i4RetVal = Nd6SendCertPathAdv (pIf6, pNd6cEntry, pIp6SrcAddr,
                                       &DstAddr6, pNd6CertPathSol,
                                       pu1TrustAnchorBuffer, u4TAOptLen,
                                       u4TACheck);
    }
    return (i4RetVal);

}

/********************************************************************
 * DESCRIPTION : This function sends the CPA messages by checking
 *               on the expected values of the fields
 *
* INPUTS       : pIf6                 - Pointer to the interface.
 *               pNd6cEntry           - Pointer to Cache Entry
 *               pSrcAddr6            - Pointer to IPv6 Src Address
 *               pDstAddr6            - Pointer to IPv6 Dst Address
 *               pNd6CertPathSol      - Pointer to CPS
 *               pu1TrustAnchorBuffer - Pointer to TA
 *               u4TAOptLen           - TA Opt Length
 *
 * OUTPUTS     : None
 *
 * RETURNS     : IP6_SUCCESS/IP6_FAILURE
 *******************************************************************/
INT4
Nd6SendCertPathAdv (tIp6If *pIf6, tNd6CacheEntry *pNd6cEntry,
                    tIp6Addr *pSrcAddr6, tIp6Addr *pDstAddr6,
                    tNd6SendCertPathSol *pNd6CertPathSol,
                    UINT1 *pu1TrustAnchorBuffer, UINT4 u4TAOptLen,
                    UINT4 u4TACheck)
{
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    UINT4  u4Nd6Size = IP6_ZERO;
    UINT4  u4TotalSize = IP6_ZERO;
    UINT4  u4Woffset = IP6_ZERO;
    UINT4  u4ContextId = IP6_ZERO;
    UINT1  u1SeNDFlag = ND6_SEND_ADD;
	UINT4  u4BufLen = IP6_ZERO;
    u4Woffset = Ip6BufWoffset (ND6_MODULE);
    u4ContextId = pIf6->pIp6Cxt->u4ContextId;

    /* allocate a buffer for the IPv6 packet + size of CPA message */
    u4Nd6Size = sizeof (tNd6SendCertPathAdv) + ND6_SEND_CPA_LENGTH;
    u4TotalSize = u4Woffset + u4Nd6Size;
    if ((pBuf = Ip6BufAlloc (u4ContextId, u4TotalSize,
                             u4Woffset, ND6_MODULE)) == NULL)
    {
        IP6_TRC_ARG (u4ContextId, ND6_MOD_TRC, BUFFER_TRC ,
                     ND6_NAME, "Nd6SendCertPathAdv "
                     "BufAlloc Failed\n");
        return (IP6_FAILURE);
    }

    /* Fill the CPA Message fields */
    if (Nd6FillCertPathAdvInCxt (pIf6->pIp6Cxt, pNd6CertPathSol, pBuf, &u4BufLen)
        == IP6_FAILURE)
    {
        IP6_TRC_ARG (u4ContextId, ND6_MOD_TRC, MGMT_TRC,
                     ND6_NAME, " Nd6SendCertPathAdv "
                     "Filling Cert Path Adv Failed\n");
        Ip6BufRelease (u4ContextId, pBuf, FALSE, ND6_MODULE);
        return (IP6_FAILURE);
    }

    if (Nd6FillTAOptInCxt (pIf6->pIp6Cxt, pu1TrustAnchorBuffer,
                           pBuf, u4TAOptLen) == IP6_FAILURE)
    {
        IP6_TRC_ARG (u4ContextId, ND6_MOD_TRC, MGMT_TRC,
                     ND6_NAME, "Nd6SendCertPathAdv "
                     "Filling Trust Anchor Failed\n");
        Ip6BufRelease (u4ContextId, pBuf, FALSE, ND6_MODULE);
        return (IP6_FAILURE);
    }
	u4BufLen += u4TAOptLen;

    if (ND6_TA_SUPPORTED == u4TACheck)
    {
        if (Nd6FillCertOptInCxt (pIf6->pIp6Cxt, ND6_SEND_CERTIFICATE, pBuf,
								 &u4BufLen) == IP6_FAILURE)
        {
            IP6_TRC_ARG (u4ContextId, ND6_MOD_TRC, MGMT_TRC,
                         ND6_NAME, "Nd6SendCertPathAdv "
                         "Filling Certificate Opt Failed\n");
            Ip6BufRelease (u4ContextId, pBuf, FALSE, ND6_MODULE);
            return (IP6_FAILURE);
        }
    }
    return (Ip6SendNdMsg (pIf6, pNd6cEntry, pSrcAddr6, pDstAddr6,
                          u4BufLen, pBuf, u1SeNDFlag, NULL));
}

/********************************************************************
 * DESCRIPTION : This function fills CPA messages
 *
 * INPUTS      : pBuf            - Pointer to the ND packet
 *                                 for validation
 *               pIp6Cxt         - Pointer to IPv6 Header
 *               pNd6CertPathSol - Pointer to CPS Options
 *
 * OUTPUTS     : None
 *
 * RETURNS     : IP6_SUCCESS/IP6_FAILURE
 *******************************************************************/
INT4
Nd6FillCertPathAdvInCxt (tIp6Cxt *pIp6Cxt,
                         tNd6SendCertPathSol *pNd6CertPathSol,
                         tCRU_BUF_CHAIN_HEADER *pBuf, UINT4 *pu4BufLen)
{
    tNd6SendCertPathAdv  Nd6SeNdCertPathAdv;

    MEMSET(&Nd6SeNdCertPathAdv, IP6_ZERO, sizeof (tNd6SendCertPathAdv));

    Nd6SeNdCertPathAdv.icmp6Hdr.u1Type = ND6_SEND_CPA;
    Nd6SeNdCertPathAdv.icmp6Hdr.u1Code = ND6_RSVD_CODE;
    Nd6SeNdCertPathAdv.u2AllComponent = OSIX_HTONS(ND6_SEND_ALL_COMPONENT);
    Nd6SeNdCertPathAdv.u2Identifier = pNd6CertPathSol->u2Identifier;

    if (Ip6BufWrite (pBuf, (UINT1 *) &Nd6SeNdCertPathAdv,
                     IP6_BUF_WRITE_OFFSET (pBuf),
                     sizeof (tNd6SendCertPathAdv), TRUE) == IP6_FAILURE)
    {
        IP6_TRC_ARG2 (pIp6Cxt->u4ContextId, ND6_MOD_TRC, BUFFER_TRC,
                      ND6_NAME, "Nd6FillCertPathAdvInCxt "
                      "BufWrite Failed, BufPtr %p Offset %d\n",
                      pBuf, IP6_BUF_WRITE_OFFSET (pBuf));

        /* CRU Buf will be released in caller */
        return (IP6_FAILURE);
    }
	*pu4BufLen += sizeof (tNd6SendCertPathAdv);
    UNUSED_PARAM(pIp6Cxt);
    return (IP6_SUCCESS);
}

/********************************************************************
 * DESCRIPTION : This function fills the TA option in CPA message
 *
 * INPUTS      : pBuf                 - Pointer to the ND packet
 *                                      for validation
 *               pIp6Cxt              - Pointer to IPV6 Header Cxt
 *               pu1TrustAnchorBuffer - Pointer To TA Buffer
 *               u4TAOptLen           - TA Opt Length
 *
 * OUTPUTS     : None
 *
 * RETURNS     : IP6_SUCCESS/IP6_FAILURE
 *******************************************************************/
INT4
Nd6FillTAOptInCxt (tIp6Cxt *pIp6Cxt, UINT1 *pu1TrustAnchorBuffer, 
                   tCRU_BUF_CHAIN_HEADER *pBuf, UINT4 u4TAOptLen)
{
    if (Ip6BufWrite (pBuf, (UINT1 *) pu1TrustAnchorBuffer,
                     IP6_BUF_WRITE_OFFSET (pBuf), u4TAOptLen,
                     TRUE) == IP6_FAILURE)
    {
        IP6_TRC_ARG2 (pIp6Cxt->u4ContextId, ND6_MOD_TRC, BUFFER_TRC,
                      ND6_NAME, "Nd6FillTAOptInCxt "
                      "BufWrite Failed, BufPtr %p Offset %d\n",
                      pBuf, IP6_BUF_WRITE_OFFSET (pBuf));
        /* CRU Buf will be released in caller */
        return (IP6_FAILURE);
    }
    UNUSED_PARAM(pIp6Cxt);
    return (IP6_SUCCESS);
}

/********************************************************************
 * DESCRIPTION : This function fills the certificate option in the
 *               CPA message
 *
 * INPUTS      : pBuf    - Pointer to the ND packet for validation
 *               pIp6Cxt - Pointer to IPv6 Header Cxt
 *               u1Type  - Type of the Message
 *
 * OUTPUTS     : None
 *
 * RETURNS     : IP6_SUCCESS/IP6_FAILURE
 *******************************************************************/
INT4
Nd6FillCertOptInCxt (tIp6Cxt *pIp6Cxt, UINT1 u1Type,
                     tCRU_BUF_CHAIN_HEADER *pBuf, UINT4 *pu4BufLen)
{
    UINT1  *pu1Cert = NULL;  /* Linear Buffer for Cert Name */
    UINT1  *pu1Buffer = NULL;    /* Linear Buffer */
	UINT1  *pu1TempCert = NULL;
    UINT4   u4BufLen = IP6_ZERO;
    INT4   i4CertLen = IP6_ZERO;
    UINT1   u1OptLen = IP6_ZERO;
    UINT1   u1CertType = IP6_ZERO;

    u1CertType = ND6_SEND_CERT_NAMETYPE;

    /* Memory Allocation for Linear Buffer */
    pu1Buffer = Ip6GetMem (pIp6Cxt->u4ContextId,
                           gIp6GblInfo.i4Ip6Nd6SecurePoolId);
    if (NULL == pu1Buffer)
    {
        IP6_TRC_ARG (pIp6Cxt->u4ContextId, ND6_MOD_TRC, MGMT_TRC,
                     ND6_NAME, "Nd6FillCertOptInCxt Get Mem Failed\n");

        /* CRU Buf will be released in caller */
        return IP6_FAILURE;
    }
    MEMSET (pu1Buffer, IP6_ZERO, ND6_SEND_MAX_BUF_SIZE);

    pu1Cert = Ip6GetMem (pIp6Cxt->u4ContextId,
                           gIp6GblInfo.i4Ip6Nd6SecurePoolId);
    if (NULL == pu1Cert)
    {
        IP6_TRC_ARG (pIp6Cxt->u4ContextId, ND6_MOD_TRC, MGMT_TRC,
                     ND6_NAME, "Nd6FillCertOptInCxt Get Mem Failed\n");

        /* CRU Buf will be released in caller */
        Ip6RelMem (pIp6Cxt->u4ContextId,
                  (UINT2)gIp6GblInfo.i4Ip6Nd6SecurePoolId,
                   pu1Buffer);
        return IP6_FAILURE;
    }
    MEMSET (pu1Cert, IP6_ZERO, ND6_SEND_MAX_BUF_SIZE);
	pu1TempCert = pu1Cert;


#ifdef SSL_WANTED
    if (SslArGetDerServerCert (&pu1Cert, &i4CertLen)
        == SSL_FAILURE)
    {
        Ip6RelMem (pIp6Cxt->u4ContextId, 
                   (UINT2)gIp6GblInfo.i4Ip6Nd6SecurePoolId,
                   pu1Buffer);
        Ip6RelMem (pIp6Cxt->u4ContextId,
                   (UINT2)gIp6GblInfo.i4Ip6Nd6SecurePoolId,
                   pu1Cert);
        return (IP6_FAILURE);
    }
	pu1Cert = pu1TempCert;
#else
    UNUSED_PARAM(pu1Cert);
    UNUSED_PARAM(i4CertLen);
    Ip6RelMem (pIp6Cxt->u4ContextId, gIp6GblInfo.i4Ip6Nd6SecurePoolId,
                      pu1Cert);
    Ip6RelMem (pIp6Cxt->u4ContextId,
              (UINT2)gIp6GblInfo.i4Ip6Nd6SecurePoolId,
               pu1Buffer);

    return (IP6_FAILURE);
#endif

    MEMCPY (pu1Buffer, &u1Type, IP6_ONE);
    u4BufLen += sizeof (UINT1);
    u4BufLen += sizeof (UINT1); /* one more for length field */

    MEMCPY (pu1Buffer + u4BufLen, &u1CertType, IP6_ONE);
    u4BufLen += sizeof (UINT1);
    u4BufLen += sizeof (UINT1); /* one more for reserved field */

    MEMCPY (pu1Buffer + u4BufLen, pu1Cert, i4CertLen);
    u4BufLen += (UINT4) i4CertLen;

    u1OptLen = (UINT1)(u4BufLen / ND6_EXT_HDR_LEN_UNIT);
    if (u4BufLen % ND6_EXT_HDR_LEN_UNIT)
    {
        u1OptLen = (UINT1)(u1OptLen + IP6_ONE);
        u4BufLen = (UINT4)(u1OptLen * ND6_EXT_HDR_LEN_UNIT);
    }
    MEMCPY (pu1Buffer + sizeof (UINT1), &u1OptLen, IP6_ONE);

    if (Ip6BufWrite (pBuf, (UINT1 *) pu1Buffer,
                     IP6_BUF_WRITE_OFFSET (pBuf),
                     u4BufLen, TRUE) == IP6_FAILURE)
    {
        IP6_TRC_ARG2 (pIp6Cxt->u4ContextId, ND6_MOD_TRC, BUFFER_TRC,
                      ND6_NAME, "Nd6FillCertOptInCxt "
                      "BufWrite Failed, BufPtr %p Offset %d\n",
                      pBuf, IP6_BUF_WRITE_OFFSET (pBuf));

        Ip6RelMem (pIp6Cxt->u4ContextId,
                   (UINT2)gIp6GblInfo.i4Ip6Nd6SecurePoolId,
                   pu1Cert);
        Ip6RelMem (pIp6Cxt->u4ContextId, 
                   (UINT2)gIp6GblInfo.i4Ip6Nd6SecurePoolId,
                   pu1Buffer);

        return (IP6_FAILURE);
    }

    Ip6RelMem (pIp6Cxt->u4ContextId,
               (UINT2)gIp6GblInfo.i4Ip6Nd6SecurePoolId,
               pu1Buffer);
    Ip6RelMem (pIp6Cxt->u4ContextId,
               (UINT2)gIp6GblInfo.i4Ip6Nd6SecurePoolId,
                   pu1Cert);
	*pu4BufLen += u4BufLen;
    return (IP6_SUCCESS);
}

/********************************************************************
 * DESCRIPTION : This function will generate RSA signature Option.
 *
 * INPUTS      : pIf6        - Pointer to the interface.
 *               pSrc        - Pointer to IPv6 source address
 *               pDst        - Pointer to IPv6 destination address
 *               u4PktLen    - Length of the packet
 *               pCgaOptions - Pointer to CGA Option
 *               pRsaOptions - Pointer to RSA Option
 *               pBuf        - Pointer to the IPv6 packet
 *                             to be sent
 *
 * OUTPUTS     : None
 *
 * RETURNS     : IP6_SUCCESS/IP6_FAILURE 
 *******************************************************************/
INT4
Nd6SeNDRsaGenerate (tIp6If *pIf6, tIp6Addr *pSrc, tIp6Addr *pDst,
                    tCRU_BUF_CHAIN_HEADER *pBuf, UINT4 u4PktLen,
                    tCgaOptions *pCgaOptions, tRsaOptions *pRsaOptions)
{
    UINT1 *pu1Buffer = NULL;    /* Linear Buffer for Signature Calc */
    UINT1 *pu1KeyBuffer = NULL; /* Linear Buffer for Public Key */
    UINT4  u4ContextId = IP6_ZERO;
    UINT4  u4BufLen = IP6_ZERO;
    UINT4  u4SignLen = IP6_ZERO;
    UINT1  au1Digest[SHA_DIGEST_LENGTH];
    UINT1  au1Hash[ND6_SEND_RSA_KEY_HASH];

    MEMSET(au1Digest, IP6_ZERO, SHA_DIGEST_LENGTH);
    MEMSET(au1Hash, IP6_ZERO, ND6_SEND_RSA_KEY_HASH);

    /* get the context from the interface */
    u4ContextId = pIf6->pIp6Cxt->u4ContextId;

    IP6_BUF_READ_OFFSET (pBuf) = IP6_ZERO;

    /* Memory Allocation for Linear Buffer */
    pu1Buffer = Ip6GetMem (u4ContextId, gIp6GblInfo.i4Ip6Nd6SecurePoolId);
    if (NULL == pu1Buffer)
    {
        IP6_TRC_ARG (u4ContextId, ND6_MOD_TRC, MGMT_TRC,
                     ND6_NAME, "Nd6SeNDRsaGenerate Get Mem Failed\n");

        Ip6BufRelease (u4ContextId, pBuf, FALSE, ND6_MODULE);
        return IP6_FAILURE;
    }
    MEMSET (pu1Buffer, IP6_ZERO, ND6_SEND_MAX_BUF_SIZE);

    /* Memory Allocation for Linear  Buffer for Public key */
    pu1KeyBuffer = Ip6GetMem (u4ContextId, gIp6GblInfo.i4Ip6Nd6SecurePoolId);
    if (NULL == pu1KeyBuffer)
    {
        IP6_TRC_ARG (u4ContextId, ND6_MOD_TRC, MGMT_TRC,
                     ND6_NAME, "Nd6SeNDRsaGenerate Get Mem Failed\n");

        Ip6RelMem (u4ContextId, (UINT2)gIp6GblInfo.i4Ip6Nd6SecurePoolId,
                   pu1Buffer); 
        Ip6BufRelease (u4ContextId, pBuf, FALSE, ND6_MODULE);
        return IP6_FAILURE;
    }
    MEMSET (pu1KeyBuffer, IP6_ZERO, ND6_SEND_MAX_BUF_SIZE);

    /* Tag Value copied to Linear Buffer */
    MEMCPY (pu1Buffer, gau1CgaTag, ND6_SEND_CGA_TAG);
    u4BufLen += ND6_SEND_CGA_TAG;

    /* Copying Source Ip6 Address to Linear Buffer */
    MEMCPY (pu1Buffer + u4BufLen, pSrc, sizeof (tIp6Addr));
    u4BufLen += sizeof (tIp6Addr);

    /* Copying Dest Ip6 Address to Linear Buffer */
    MEMCPY (pu1Buffer + u4BufLen, pDst, sizeof (tIp6Addr));
    u4BufLen += sizeof (tIp6Addr);

    /* NDP options preceding RSA signature option */
    if (CRU_BUF_Copy_FromBufChain (pBuf, pu1Buffer + u4BufLen, IP6_ZERO,
                                   u4PktLen) == CRU_FAILURE)
    {
        Ip6RelMem (u4ContextId, 
                   (UINT2)gIp6GblInfo.i4Ip6Nd6SecurePoolId,
                   pu1Buffer); 
        Ip6RelMem (u4ContextId,
                   (UINT2)gIp6GblInfo.i4Ip6Nd6SecurePoolId,
                   pu1KeyBuffer);

        Ip6BufRelease (u4ContextId, pBuf, FALSE, ND6_MODULE);
        return (IP6_FAILURE);
    }

    u4BufLen += u4PktLen;

    /* SHA-1 Hash Generation for Sequence of data */
    /*
     * - 128 Bit CGA Message Type Tag
     * - 128 bit Source Address field from IP header 
     * - 128 bit Destination Address field from IP header
     * - From ICMP Header 8 Bit Type, 8 bit Code & 16 bit Checksum 
     * - NDP Message header, starting from the octer after the ICMP 
     *   and continuting up to  but not including NDP options 
     * - All NDP options preceding the RSA Signature Option    */
    Sha1ArAlgo (pu1Buffer, (INT4)u4BufLen, au1Digest);

#ifdef SSL_WANTED
    /* Signature Generation - Input SHA-1 Digest and Length */
    if (SSL_SUCCESS != SslArRsaSignReq (SHA_DIGEST_LENGTH, au1Digest,
                                        pRsaOptions->pu1Signature,
                                        &u4SignLen))
    {
        IP6_TRC_ARG (u4ContextId, ND6_MOD_TRC, MGMT_TRC,
                     ND6_NAME, "Nd6SeNDRsaGenerate "
                     "Rsa Sign Req Failed\n");
        Ip6RelMem (u4ContextId, 
                   (UINT2)gIp6GblInfo.i4Ip6Nd6SecurePoolId,
                   pu1Buffer); 
        Ip6RelMem (u4ContextId, 
                   (UINT2)gIp6GblInfo.i4Ip6Nd6SecurePoolId,
                   pu1KeyBuffer);
        Ip6BufRelease (u4ContextId, pBuf, FALSE, ND6_MODULE);
        return IP6_FAILURE;
    }
#else
    UNUSED_PARAM(au1Digest);
    UNUSED_PARAM(pRsaOptions->pu1Signature);
    UNUSED_PARAM(u4SignLen);
    Ip6RelMem (u4ContextId, gIp6GblInfo.i4Ip6Nd6SecurePoolId,
               pu1Buffer);
    Ip6RelMem (u4ContextId, gIp6GblInfo.i4Ip6Nd6SecurePoolId,
               pu1KeyBuffer);
    Ip6BufRelease (u4ContextId, pBuf, FALSE, ND6_MODULE);
    return IP6_FAILURE;
#endif

    /* Copying the signature to RSA Option */
    pRsaOptions->u4SignLen = u4SignLen;

    u4BufLen = IP6_ZERO;
    MEMSET (&au1Digest, IP6_ZERO, SHA_DIGEST_LENGTH);

    /* Copying DER Encoded Public Key to Linear Buffer */
    MEMCPY (pu1KeyBuffer, pCgaOptions->pu1DerPubKey,
            pCgaOptions->u2PubKeyLen);
    u4BufLen = pCgaOptions->u2PubKeyLen;
    Sha1ArAlgo (pu1KeyBuffer, (INT4)u4BufLen, au1Digest);

    /* Copying left most 128 bit of Public Key Hash */
    MEMSET (au1Hash, IP6_ZERO, ND6_SEND_RSA_KEY_HASH);
    MEMCPY (au1Hash, au1Digest, ND6_SEND_RSA_KEY_HASH);

    /* Populating Key Hash field in RSA Option */
    MEMCPY (pRsaOptions->au1KeyHash, au1Hash, ND6_SEND_RSA_KEY_HASH);

    /* assign the read offset for proper checksum calculation */
    IP6_BUF_READ_OFFSET (pBuf) = IP6_ZERO;

    Ip6RelMem (u4ContextId, (UINT2)gIp6GblInfo.i4Ip6Nd6SecurePoolId,
               pu1Buffer); 
    Ip6RelMem (u4ContextId, (UINT2)gIp6GblInfo.i4Ip6Nd6SecurePoolId,
               pu1KeyBuffer);

    return IP6_SUCCESS;
}

/********************************************************************
 * DESCRIPTION : This function validates RSA signature Option.
 *
 * INPUTS      : pIf6        - Pointer to the interface.
 *               pIp6        - Pointer to IPv6 Header
 *               u4PktLen    - Length of the packet
 *               pCgaOptions - Pointer to CGA Option
 *               pRsaOptions - Pointer to RSA Option
 *               pBuf        - Pointer to the IPv6 packet
 *                             to be sent
 *
 * OUTPUTS     : None
 *
 * RETURNS     : IP6_SUCCESS/IP6_FAILURE
 *******************************************************************/
PUBLIC INT4
Nd6SeNDRsaVerify (tIp6If *pIf6, tIp6Hdr *pIp6,
                  tCRU_BUF_CHAIN_HEADER *pBuf,
                  UINT4 u4PktLen, tCgaOptions *pCgaOptions,
                  tRsaOptions *pRsaOptions)
{
    UINT1  *pu1Buffer = NULL;
    UINT1  *pu1DerPubKey = (pCgaOptions->pu1DerPubKey); 
    VOID   *pRsa = NULL;
    UINT4   u4ContextId = IP6_ZERO;
    UINT4   u4BufLen = IP6_ZERO;
    UINT4   u4DigestLen = IP6_ZERO;
    INT4    i4SignLen = IP6_ZERO;
    UINT1   au1Digest[SHA_DIGEST_LENGTH];
    UINT1   au1DigestOut[SHA_DIGEST_LENGTH];
    UINT1   au4Sign[ND6_SEND_RSA_SIGN_LEN];

    MEMSET (au1Digest, IP6_ZERO, SHA_DIGEST_LENGTH);
    MEMSET (au1DigestOut, IP6_ZERO, SHA_DIGEST_LENGTH);
    MEMSET (au4Sign, IP6_ZERO, ND6_SEND_RSA_SIGN_LEN);

    /* get the context from the interface */
    u4ContextId = pIf6->pIp6Cxt->u4ContextId;

    pu1Buffer = Ip6GetMem (u4ContextId,
                           gIp6GblInfo.i4Ip6Nd6SecurePoolId);
    if (NULL == pu1Buffer)
    {
        IP6_TRC_ARG (u4ContextId, ND6_MOD_TRC, MGMT_TRC,
                     ND6_NAME, "Nd6SeNDRsaVerify Get MemFailed\n");
        return IP6_FAILURE;
    }
    MEMSET (pu1Buffer, IP6_ZERO, ND6_SEND_MAX_BUF_SIZE);

    /* Tag Value copied to Linear Buffer */
    MEMCPY (pu1Buffer, gau1CgaTag, ND6_SEND_CGA_TAG);
    u4BufLen = u4BufLen + ND6_SEND_CGA_TAG;

    /* Copying Source Ip6 Address to Linear Buffer */
    MEMCPY (pu1Buffer + u4BufLen, &(pIp6->srcAddr), ND6_SEND_ADDR_LENGTH);
    u4BufLen = u4BufLen + ND6_SEND_ADDR_LENGTH;

    /* Copying Dest Ip6 Address to Linear Buffer */
    MEMCPY (pu1Buffer + u4BufLen, &(pIp6->dstAddr), ND6_SEND_ADDR_LENGTH);
    u4BufLen = u4BufLen + ND6_SEND_ADDR_LENGTH;

    if (CRU_BUF_Copy_FromBufChain (pBuf, pu1Buffer + u4BufLen,
                                   IP6_BUF_READ_OFFSET (pBuf),
                                   u4PktLen) == IP6_ZERO)
    {
        /*Free the Allocated Linear Buffer */
        Ip6RelMem (u4ContextId, 
                   (UINT2)gIp6GblInfo.i4Ip6Nd6SecurePoolId,
                   pu1Buffer);
        return (IP6_FAILURE);
    }

    u4BufLen = u4BufLen + u4PktLen;

    /* checksum field is set to zero */
    MEMSET(pu1Buffer + IP6_FIFTY, IP6_ZERO, IP6_TWO);

    /* SHA-1 Hash Generation for Sequence of data */
    /*
     * - 128 Bit CGA Message Type Tag
     * - 128 bit Source Address field from IP header
     * - 128 bit Destination Address field from IP header
     * - From ICMP Header 8 Bit Type, 8 bit Code & 16 bit Checksum
     * - NDP Message header, starting from the octer after the ICMP and 
     *   continuting up to but not including NDP options
     * - All NDP options preceding the RSA Signature Option    */
    Sha1ArAlgo (pu1Buffer, (INT4)u4BufLen, au1Digest);

#ifdef SSL_WANTED
    /* Extracting the Public Key from CGA option (DER Encoded) */
    if (SslArGetRsaPubKey ((UINT1 **) &(pCgaOptions->pu1DerPubKey),
            pCgaOptions->u2PubKeyLen, &pRsa) != SSL_SUCCESS)
    {
        Ip6RelMem (u4ContextId, 
                   (UINT2)gIp6GblInfo.i4Ip6Nd6SecurePoolId,
                   pu1Buffer);
        IP6_TRC_ARG (u4ContextId, ND6_MOD_TRC, MGMT_TRC,
                     ND6_NAME, "Nd6SeNDRsaVerify "
                     "Ssl Get Rsa Public key Failed\n");
        return IP6_FAILURE;
    }

    /* Identify the Maximum Length of the Signature from the Public Key */
    if (SSL_SUCCESS != SslArGetMaxRsaSignSize ((UINT1 **) &pu1DerPubKey,
                           pCgaOptions->u2PubKeyLen, &i4SignLen))
    {
        Ip6RelMem(u4ContextId,
                  (UINT2) gIp6GblInfo.i4Ip6Nd6SecurePoolId,
                  pu1Buffer);

        IP6_TRC_ARG (u4ContextId, ND6_MOD_TRC, MGMT_TRC,
                     ND6_NAME, "Nd6SeNDRsaVerify "
                     "Ssl Get Maximum RSA Sign Length Failed\n");
        return IP6_FAILURE;
    }

    /* 
     * If maximum length of the signature obtained from public key
     * is less than signature + padding, then take maximum length
     * as signature length. Remaining bytes are considered as padding
     */
    if (i4SignLen < (INT4) pRsaOptions->u4SignLen)
    {
        pRsaOptions->u4SignLen = (UINT4)i4SignLen;
    }

    /* Decrypt the Signature with Public Key of the Sender */
    if (SSL_SUCCESS != SslArRsaSignVer (pRsa, pRsaOptions->u4SignLen,
                                        pRsaOptions->pu1Signature,
                                        au1DigestOut, &u4DigestLen))
    {
        Ip6RelMem (u4ContextId, 
                   (UINT2)gIp6GblInfo.i4Ip6Nd6SecurePoolId,
                   pu1Buffer);

        IP6_TRC_ARG (u4ContextId, ND6_MOD_TRC, MGMT_TRC,
                     ND6_NAME, "Nd6SeNDRsaVerify "
                     "Ssl Rsa Sign Verification Failed\n");
        return IP6_FAILURE;
    }
#else
    UNUSED_PARAM(pCgaOptions);
    UNUSED_PARAM(pRsaOptions->pu1Signature);
	UNUSED_PARAM(pRsaOptions->u4SignLen);
    UNUSED_PARAM(pu1DerPubKey);
    UNUSED_PARAM(i4SignLen);
    UNUSED_PARAM(au1DigestOut);
    UNUSED_PARAM(u4DigestLen);
	UNUSED_PARAM(pCgaOptions->u2PubKeyLen);
	UNUSED_PARAM(pCgaOptions->pu1DerPubKey);
	UNUSED_PARAM(pRsa);
    Ip6RelMem (u4ContextId, 
               (UINT2)gIp6GblInfo.i4Ip6Nd6SecurePoolId,
               pu1Buffer);
    return IP6_FAILURE;
#endif

    /* Compare the two digests */
    if (IP6_ZERO != MEMCMP (au1Digest, au1DigestOut, ND6_SEND_RSA_HASH_LEN))
    {
        Ip6RelMem (u4ContextId, 
                   (UINT2)gIp6GblInfo.i4Ip6Nd6SecurePoolId,
                   pu1Buffer);
        return IP6_FAILURE;
    }

    Ip6RelMem (u4ContextId, 
               (UINT2)gIp6GblInfo.i4Ip6Nd6SecurePoolId,
               pu1Buffer);
    return IP6_SUCCESS;
}

/********************************************************************
 * DESCRIPTION : This function adds RSA signature option to ND6
 *               message 
 *
 * INPUTS      : u4ContextId -  Context Id
 *               pBuf        -  Pointer to the IPv6 packet
 *               pRsaOptions -  Pointer to RSA Option to be sent
 *
 * OUTPUTS     : pu4PktLen   - Pointer to packet length 
 *
 * RETURNS     : IP6_SUCCESS/IP6_FAILURE
 *******************************************************************/
INT4
Nd6SeNDAddRsaOpt (UINT4 u4ContextId, tCRU_BUF_CHAIN_HEADER *pBuf,
                  tRsaOptions *pRsaOpt, UINT4 *pu4PktLen)
{
    tNd6ExtHdr  Nd6ExtHdr;
    UINT1      *pu1Buf = NULL;
    INT4        i4RetVal = IP6_FAILURE;
    UINT2       u2BufLen = IP6_ZERO;

    MEMSET (&Nd6ExtHdr, IP6_ZERO, sizeof (tNd6ExtHdr));

    /* linear buffer from mem pool  */
    pu1Buf = (UINT1 *) MemAllocMemBlk ((tMemPoolId)
                                        gIp6GblInfo.i4Ip6Nd6SecurePoolId);
    if (NULL == pu1Buf)
    {
        IP6_TRC_ARG (u4ContextId, ND6_MOD_TRC, MGMT_TRC,
                     ND6_NAME, "Nd6SeNDAddRsaOpt Mem Alloc Failed\n");

        Ip6BufRelease (u4ContextId, pBuf, FALSE, ND6_MODULE);
        return IP6_FAILURE;
    }
    MEMSET (pu1Buf, IP6_ZERO, ND6_SEND_MAX_BUF_SIZE);

    /* copy the RSA params to linear buffer after the Extn Header */
    u2BufLen = (UINT2)(u2BufLen + ND6_EXT_HDR_LEN);

    /* + two bytes for reserved */
    u2BufLen = (UINT2)(u2BufLen + IP6_TWO);

    MEMCPY (pu1Buf + u2BufLen, pRsaOpt->au1KeyHash, ND6_SEND_RSA_KEY_HASH);
    u2BufLen = (UINT2)(u2BufLen + ND6_SEND_RSA_KEY_HASH);

    MEMCPY (pu1Buf + u2BufLen, pRsaOpt->pu1Signature, pRsaOpt->u4SignLen);
    u2BufLen = (UINT2)(u2BufLen + pRsaOpt->u4SignLen);

    Nd6ExtHdr.u1Type = ND6_SEND_RSA_OPT;
    /* Length is in units of 8 octets */
    Nd6ExtHdr.u1Len = (UINT1)(u2BufLen / ND6_EXT_HDR_LEN_UNIT);

    /* For Padding if Length not in units of 8 octets */
    if (IP6_ZERO != (u2BufLen % ND6_EXT_HDR_LEN_UNIT))
    {
        Nd6ExtHdr.u1Len = (UINT1) (Nd6ExtHdr.u1Len + IP6_ONE);
        u2BufLen = (UINT2) (Nd6ExtHdr.u1Len * ND6_EXT_HDR_LEN_UNIT);
    }

    /* Now copy the extn header to the beginning of the buffer */
    MEMCPY (pu1Buf, &Nd6ExtHdr, sizeof (tNd6ExtHdr));
    /* Length of buffer already includes the extn header. So don't
     * increment it now */

    /* write RSA options into packet */
    i4RetVal = Ip6BufWrite (pBuf, (UINT1 *) pu1Buf,
                            IP6_BUF_WRITE_OFFSET (pBuf), u2BufLen, TRUE);

    if (IP6_FAILURE == i4RetVal)
    {
        IP6_TRC_ARG2 (u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC,
                      ND6_NAME, "Nd6SeNDAddRsaOpt "
                      "BufWrite Failed!  BufPtr %p Offset %d\n",
                      pBuf, IP6_BUF_WRITE_OFFSET (pBuf));

        Ip6BufRelease (u4ContextId, pBuf, FALSE, ND6_MODULE);
        Ip6RelMem (u4ContextId, 
                   (UINT2)gIp6GblInfo.i4Ip6Nd6SecurePoolId,
                   pu1Buf);

        return (IP6_FAILURE);
    }
    *pu4PktLen += u2BufLen;

    Ip6RelMem (u4ContextId,
               (UINT2) gIp6GblInfo.i4Ip6Nd6SecurePoolId,
               pu1Buf);
    return IP6_SUCCESS;
}

/********************************************************************
 * DESCRIPTION : This routine will generate CGA Option.
 *
 * INPUTS      : pIf6        - Pointer to the interface.
 *               pCgaOptions - Pointer to CGA Option
 *               pIp6Addr    - Pointer to IPv6 Address
 *
 * OUTPUTS     : None
 *
 * RETURNS     : IP6_SUCCESS/IP6_FAILURE
 *******************************************************************/
INT4
Nd6SeNDCgaGenerate (tIp6If *pIf6, tIp6Addr *pIp6Addr,
                    tCgaOptions *pCgaOptions)
{
    UINT1 *pu1Buf = NULL;
    UINT4  u4ContextId = IP6_ZERO;
    UINT4  u4BufLen = IP6_ZERO;
    INT4   i4RetVal = IP6_FAILURE;
    INT4   i4Inc = IP6_ZERO;
    UINT1  u1SecValCmpLen = IP6_ZERO;
    BOOL1  b1IsHash2OK = IP6_FALSE;
    UINT1  au1Modifier[ND6_SEND_CGA_MOD_LEN];
    UINT1  au1Digest[SHA_DIGEST_LENGTH];

    MEMSET (au1Modifier, IP6_ZERO, ND6_SEND_CGA_MOD_LEN);
    MEMSET (au1Digest, IP6_ZERO, SHA_DIGEST_LENGTH);

    u4ContextId = pIf6->pIp6Cxt->u4ContextId; 
    u1SecValCmpLen = (UINT1) (gNd6GblInfo.u1Nd6SeNDSec *
                     (ND6_SEND_CGA_SEC_MULT / BYTES_IN_OCTECT));

    pu1Buf = (UINT1 *) MemAllocMemBlk ((tMemPoolId)
                                        gIp6GblInfo.i4Ip6Nd6SecurePoolId);
    if (NULL == pu1Buf)
    {
        IP6_TRC_ARG (u4ContextId, ND6_MOD_TRC, MGMT_TRC,
                     ND6_NAME, "Nd6SeNDCgaGenerate Mem Alloc Failed\n");
        return IP6_FAILURE;
    }
    MEMSET (pu1Buf, IP6_ZERO, ND6_SEND_MAX_BUF_SIZE);

    /* 
     * Perform modifier generation only when collision count is zero.
     * If DAD fails, repeat from Step 5 with new collision count
     */
    if (pCgaOptions->u1Collisions == IP6_ZERO)
    {
        i4RetVal = UtilRandNumGen (au1Modifier, ND6_SEND_CGA_MOD_LEN);
        if (OSIX_FAILURE == i4RetVal)
        {
            Ip6RelMem (u4ContextId, 
                       (UINT2)gIp6GblInfo.i4Ip6Nd6SecurePoolId,
                       pu1Buf);
            return IP6_FAILURE;
        }

        if (IP6_ZERO != gNd6GblInfo.u1Nd6SeNDSec)
        {
            MEMCPY (pu1Buf, au1Modifier, ND6_SEND_CGA_MOD_LEN);
            u4BufLen = u4BufLen + ND6_SEND_CGA_MOD_LEN;

            /* 9 octets of 0; RFC3972, Sec. 4 */
            MEMSET (pu1Buf + u4BufLen, IP6_ZERO, IP6_NINE);
            u4BufLen += IP6_NINE;

            MEMCPY (pu1Buf + u4BufLen, pCgaOptions->pu1DerPubKey,
                    pCgaOptions->u2PubKeyLen);
            u4BufLen += pCgaOptions->u2PubKeyLen;

            do
            {
                /* Compute Hash 2 */
                Sha1ArAlgo (pu1Buf, (INT4) u4BufLen, au1Digest);
                b1IsHash2OK = (MEMCMP (au1Digest, CGA_ZEROS,
                                       u1SecValCmpLen) == IP6_ZERO);
                if (IP6_ONE == b1IsHash2OK)
                {
                    break;
                }

                for (i4Inc = IP6_ZERO; i4Inc < IP6_SIXTEEN; ++i4Inc)
                {
                    if (++au1Modifier[i4Inc] != IP6_ZERO)
                    {
                        break;
                    }
                }
                MEMCPY (pu1Buf, au1Modifier, ND6_SEND_CGA_MOD_LEN);
            } while (IP6_TRUE);
        }
        MEMCPY (pCgaOptions->au1Modifier, au1Modifier, ND6_SEND_CGA_MOD_LEN);
    }
    else
    {
        /* collision count is non-zero, use the existing modifier */
        MEMCPY (au1Modifier, pCgaOptions->au1Modifier,
                ND6_SEND_CGA_MOD_LEN);
    }

    /* Step 5 of CGA Algorithm */
    MEMSET (&au1Digest, IP6_ZERO, SHA_DIGEST_LENGTH);
    MEMSET (pu1Buf, IP6_ZERO, ND6_SEND_MAX_BUF_SIZE);
    u4BufLen = IP6_ZERO;

    /* For Hash 1 computation  */
    MEMCPY (pu1Buf, au1Modifier, ND6_SEND_CGA_MOD_LEN);
    u4BufLen += ND6_SEND_CGA_MOD_LEN;

    MEMCPY (pu1Buf + u4BufLen, pCgaOptions->au1Prefix,
            ND6_SEND_CGA_PREFIX_LEN);
    u4BufLen += ND6_SEND_CGA_PREFIX_LEN;

    MEMCPY (pu1Buf + u4BufLen, &(pCgaOptions->u1Collisions),
            IP6_ONE);
    u4BufLen += sizeof (UINT1);

    MEMCPY (pu1Buf + u4BufLen, pCgaOptions->pu1DerPubKey,
            pCgaOptions->u2PubKeyLen);
    u4BufLen += pCgaOptions->u2PubKeyLen;

    /* Compute Hash 1 */
    Sha1ArAlgo (pu1Buf, (INT4)u4BufLen, au1Digest);
    /*
     * The first byte of the Hash 1 is denote below
     *     
     *  Bits  ->       0 1 2 3 4 5 6 7  
     *  Value ->       s e c       u g
     *
     * Set u and g bits to 0. 
     * In RFC3972, 1st bit is taken as bit 0. Not bit 1.
     * Hence the bits 6, 7 set to zero. (7th and 8th bit).
     *
     * Clear the left most 3 bits for sec value and
     * set them to sec val.
     *     
     *                    0 1 2 3 4 5 6 7 
     * BitMask Pattern->  0 0 0 1 1 1 0 0 ->  is 0x1c (ND6_SEND_CGA_HASH1_MASK)
     */   

    au1Digest[IP6_ZERO] = au1Digest[IP6_ZERO] & ND6_SEND_CGA_HASH1_MASK;
    /* left shift 5 bits and move the 3 bit sec value to bits 0, 1, 2 */
    au1Digest[IP6_ZERO] = (UINT1)(au1Digest[IP6_ZERO] |
                          (gNd6GblInfo.u1Nd6SeNDSec << IP6_FIVE));

    /* The left most 64 bits of this result form the interface identifier */

    MEMSET (pIp6Addr, IP6_ZERO, sizeof (tIp6Addr));
    MEMCPY (pIp6Addr, &(pCgaOptions->au1Prefix), ND6_SEND_CGA_PREFIX_LEN);
    MEMCPY (&(pIp6Addr->u1_addr[ND6_SEND_CGA_PREFIX_LEN]), au1Digest,
            ND6_SEND_CGA_HASH1_LEN);

    Ip6RelMem (u4ContextId, 
               (UINT2)gIp6GblInfo.i4Ip6Nd6SecurePoolId,
               pu1Buf);

    return IP6_SUCCESS;
}

/********************************************************************
 * DESCRIPTION : This routine will be used verify the CGA Option.
 *
 * INPUTS      : pIf6        - Pointer to the interface.
 *               pIp6Addr    - Pointer to IPv6 Address
 *               pCgaOptions - Pointer to CGA Option
 *
 * OUTPUTS     : None
 *
 * RETURNS     : IP6_SUCCESS/IP6_FAILURE
 *******************************************************************/
INT4
Nd6SeNDCgaVerify (tIp6If *pIf6, tIp6Addr *pIp6Addr,
                  tCgaOptions *pCgaOptions)
{
    tIp6Addr  Ip6Addr;
    UINT1     *pu1Buf = NULL;
    UINT4      u4ContextId = IP6_ZERO;
    UINT4      u4BufLen = IP6_ZERO;
    UINT1      u1SecValCmpLen = IP6_ZERO;
    UINT1      u1SecVal = IP6_ZERO;
    UINT1      au1Digest[SHA_DIGEST_LENGTH];

    MEMSET (&Ip6Addr, IP6_ZERO, sizeof (tIp6Addr));
    MEMSET (au1Digest, IP6_ZERO, SHA_DIGEST_LENGTH);

    u4ContextId = pIf6->pIp6Cxt->u4ContextId;

    pu1Buf = (UINT1 *) Ip6GetMem (u4ContextId,
                                  gIp6GblInfo.i4Ip6Nd6SecurePoolId);
    if (NULL == pu1Buf)
    {
        IP6_TRC_ARG (u4ContextId, ND6_MOD_TRC, MGMT_TRC,
                     ND6_NAME, "Nd6SeNDCgaVerify Get Mem Failed\n");
        return IP6_FAILURE;
    }
    MEMSET (pu1Buf, IP6_ZERO, ND6_SEND_MAX_BUF_SIZE);

    Ip6AddrCopy (&Ip6Addr, pIp6Addr);

    if (pCgaOptions->u1Collisions > ND6_SEND_CGA_MAX_COLLISION)
    {
        IP6_TRC_ARG1 (u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC,
                      ND6_NAME, "Nd6SeNDCgaVerify "
                      "Incorrect Collisions %d\n",
                      pCgaOptions->u1Collisions);

        Ip6RelMem (u4ContextId, 
                   (UINT2)gIp6GblInfo.i4Ip6Nd6SecurePoolId,
                   pu1Buf);
        return IP6_FAILURE;
    }

    if (IP6_ZERO != MEMCMP (&Ip6Addr, &(pCgaOptions->au1Prefix),
                            ND6_SEND_CGA_PREFIX_LEN))
    {
        IP6_TRC_ARG (u4ContextId, ND6_MOD_TRC, MGMT_TRC,
                     ND6_NAME, "Nd6SeNDCgaVerify "
                     "CgaParam Prefix not match with Addr Prefix\n");

        Ip6RelMem (u4ContextId, 
                   (UINT2)gIp6GblInfo.i4Ip6Nd6SecurePoolId,
                   pu1Buf);
        return IP6_FAILURE;
    }

    MEMCPY (pu1Buf, pCgaOptions->au1Modifier, ND6_SEND_CGA_MOD_LEN);
    u4BufLen += ND6_SEND_CGA_MOD_LEN;

    MEMCPY (pu1Buf + u4BufLen, pCgaOptions->au1Prefix,
            ND6_SEND_CGA_PREFIX_LEN);
    u4BufLen += ND6_SEND_CGA_PREFIX_LEN;

    MEMCPY (pu1Buf + u4BufLen, &(pCgaOptions->u1Collisions),
            IP6_ONE);
    u4BufLen += sizeof (UINT1);

    MEMCPY (pu1Buf + u4BufLen, pCgaOptions->pu1DerPubKey,
            pCgaOptions->u2PubKeyLen);
    u4BufLen += pCgaOptions->u2PubKeyLen;
    
    Sha1ArAlgo (pu1Buf, (INT4)u4BufLen, au1Digest);

    /* Rightmost 64 bits are interface ID. 
     * The leftmost 3 bits of Interface ID are Sec Value */

    /* sec value will be in the first byte of the Interface ID */
    u1SecVal = Ip6Addr.u1_addr[IP6_EIGHT];
    u1SecVal = u1SecVal >> IP6_FIVE;

    /* Clear sec, u, g bits  */
    au1Digest[IP6_ZERO] &= ND6_SEND_CGA_HASH1_MASK;
    Ip6Addr.u1_addr[IP6_EIGHT] &= ND6_SEND_CGA_HASH1_MASK;

    if (IP6_ZERO != MEMCMP (au1Digest, &Ip6Addr.u1_addr[IP6_EIGHT],
                            ND6_SEND_CGA_IFID_LEN))
    {
        Ip6RelMem (u4ContextId, 
                   (UINT2)gIp6GblInfo.i4Ip6Nd6SecurePoolId,
                   pu1Buf);
        return IP6_FAILURE;
    }

    MEMSET (pu1Buf, IP6_ZERO, ND6_SEND_MAX_BUF_SIZE);
    u4BufLen = IP6_ZERO;

    if (IP6_ZERO != u1SecVal)
    {
        MEMCPY (pu1Buf, pCgaOptions->au1Modifier, ND6_SEND_CGA_MOD_LEN);
        u4BufLen += ND6_SEND_CGA_MOD_LEN;

        /* 9 octets of 0; RFC3972, Sec. 5 */
        MEMSET (pu1Buf + u4BufLen, IP6_ZERO, IP6_NINE); 
        u4BufLen += IP6_NINE;

        MEMCPY (pu1Buf + u4BufLen, pCgaOptions->pu1DerPubKey,
                pCgaOptions->u2PubKeyLen);
        u4BufLen += pCgaOptions->u2PubKeyLen;

        /* Compute Hash 2 */
        Sha1ArAlgo (pu1Buf, (INT4) u4BufLen, au1Digest);
        u1SecValCmpLen = (UINT1) (u1SecVal * 
                                 (ND6_SEND_CGA_SEC_MULT / IP6_EIGHT));

        if (IP6_ZERO != (MEMCMP (au1Digest, CGA_ZEROS, u1SecValCmpLen)))
        {
            Ip6RelMem (u4ContextId,
                       (UINT2)gIp6GblInfo.i4Ip6Nd6SecurePoolId,
                       pu1Buf);
            return IP6_FAILURE;
        }
    }

    Ip6RelMem (u4ContextId, 
               (UINT2)gIp6GblInfo.i4Ip6Nd6SecurePoolId,
               pu1Buf);
    return IP6_SUCCESS;
}

/********************************************************************
 * DESCRIPTION : This routine will add CGA Option to IPv6
 *               packet.
 *
 * INPUTS      : u4ContextId - Context Id
 *               pBuf        - Pointer to the IPv6 packet
 *               pCgaOpt     - Pointer to RSA Option to be sent
 *
 * OUTPUTS     : pu4PktLen   - Pointer to packet length
 *
 * RETURNS     : IP6_SUCCESS/IP6_FAILURE
 *******************************************************************/
INT4
Nd6SeNDAddCgaOpt (UINT4 u4ContextId, tCRU_BUF_CHAIN_HEADER *pBuf,
                  tCgaOptions *pCgaOpt,  UINT4 *pu4PktLen)
{
    tNd6CgaExtHdr  Nd6CgaExtHdr;
    UINT1         *pu1Buf = NULL;
    INT4           i4RetVal = IP6_FAILURE;
    UINT2          u2BufLen = IP6_ZERO;

    MEMSET (&Nd6CgaExtHdr, IP6_ZERO, sizeof (tNd6CgaExtHdr));

    pu1Buf = (UINT1 *) MemAllocMemBlk ((tMemPoolId)
                                        gIp6GblInfo.i4Ip6Nd6SecurePoolId);
    if (NULL == pu1Buf)
    {
        IP6_TRC_ARG (u4ContextId, ND6_MOD_TRC, MGMT_TRC,
                     ND6_NAME, "Nd6SeNDAddCgaOpt Mem Alloc Failed\n");
        Ip6BufRelease (u4ContextId, pBuf, FALSE, ND6_MODULE);
        return IP6_FAILURE;
    }
    MEMSET (pu1Buf, IP6_ZERO, ND6_SEND_MAX_BUF_SIZE);

    /* copy the cga params to linear buffer after the Extn Header */
    u2BufLen = (UINT2) (u2BufLen + ND6_EXT_HDR_LEN);

    /* + two bytes for pad length and reserved */
    u2BufLen = (UINT2)(u2BufLen + IP6_TWO);

    MEMCPY (pu1Buf + u2BufLen, pCgaOpt->au1Modifier, ND6_SEND_CGA_MOD_LEN);
    u2BufLen = (UINT2) (u2BufLen + ND6_SEND_CGA_MOD_LEN);

    MEMCPY (pu1Buf + u2BufLen, pCgaOpt->au1Prefix, ND6_SEND_CGA_PREFIX_LEN);
    u2BufLen = (UINT2) (u2BufLen + ND6_SEND_CGA_PREFIX_LEN);

    MEMCPY (pu1Buf + u2BufLen, &(pCgaOpt->u1Collisions), sizeof (UINT1));
    u2BufLen = (UINT2) (u2BufLen + IP6_ONE);

    MEMCPY (pu1Buf + u2BufLen, pCgaOpt->pu1DerPubKey, pCgaOpt->u2PubKeyLen);
    u2BufLen = (UINT2) (u2BufLen + pCgaOpt->u2PubKeyLen);

    Nd6CgaExtHdr.u1Type = ND6_SEND_CGA_OPT;
    /* Length is in units of 8 octets */
    Nd6CgaExtHdr.u1Len = (UINT1) (u2BufLen / ND6_EXT_HDR_LEN_UNIT);

    Nd6CgaExtHdr.u1PadLen = IP6_ZERO;
    /* For Padding if Length not in units of 8 octets */
    if (IP6_ZERO != u2BufLen % ND6_EXT_HDR_LEN_UNIT)
    {
        Nd6CgaExtHdr.u1Len = (UINT1) (Nd6CgaExtHdr.u1Len + IP6_ONE);
        Nd6CgaExtHdr.u1PadLen = (UINT1) ((Nd6CgaExtHdr.u1Len *
                                          ND6_EXT_HDR_LEN_UNIT) - 
                                          u2BufLen);
        u2BufLen = (UINT2)(Nd6CgaExtHdr.u1Len * ND6_EXT_HDR_LEN_UNIT);
    }

    /* Now copy the extn header to the beginning of the buffer */
    MEMCPY (pu1Buf, &Nd6CgaExtHdr, sizeof (tNd6CgaExtHdr));
    /* Length of buffer already includes the extn header. So don't
     * increment it now */

    /* write cga options into packet */
    i4RetVal = Ip6BufWrite (pBuf, (UINT1 *) pu1Buf,
                            IP6_BUF_WRITE_OFFSET (pBuf), u2BufLen, TRUE);

    if (IP6_FAILURE == i4RetVal)
    {
        IP6_TRC_ARG2 (u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC,
                      ND6_NAME, "Nd6SeNDAddCgaOpt "
                      " BufWrite Failed!  BufPtr %p Offset %d\n",
                      pBuf, IP6_BUF_WRITE_OFFSET (pBuf));

        Ip6BufRelease (u4ContextId, pBuf, FALSE, ND6_MODULE);
        Ip6RelMem (u4ContextId, 
                   (UINT2)gIp6GblInfo.i4Ip6Nd6SecurePoolId, pu1Buf);
        return (IP6_FAILURE);
    }
    /* add the actual packet length */
    *pu4PktLen = (*pu4PktLen) + u2BufLen;

    Ip6RelMem (u4ContextId,
               (UINT2)gIp6GblInfo.i4Ip6Nd6SecurePoolId, pu1Buf);
    return IP6_SUCCESS;
}

/********************************************************************
 * DESCRIPTION : This function inturn calls functions to add all
 *               Secure Neighbor Discovery options to the IPv6 Packet
 *
 * INPUTS      : pIf6       - Pointer to Logical interface
 *               pSrc       - Pointer to IPv6 source address
 *               pDst       - Pointer to IPv6 destination address
 *               pBuf       - Pointer to the IPv6 packet
 *                            to be sent
 *               u1SeNDFlag - SeND message flag
 *
 * OUTPUTS     : pu4PktLen  - Pointer to packet length
 *
 * RETURNS     : IP6_SUCCESS/IP6_FAILURE
 *******************************************************************/
INT4
Nd6SeNDAddOpts (tIp6If *pIf6, tIp6Addr *pSrc, tIp6Addr *pDst,
                tCRU_BUF_CHAIN_HEADER *pBuf, UINT1 u1SeNDFlag,
                tIp6Addr *pTargetAddr, UINT4 *pu4PktLen)
{
    tRsaOptions     RsaOptions;
    tIp6AddrInfo   *pAddrInfo = NULL;
    tIp6Addr       *pCgaAddr = NULL;
    tIp6LlocalInfo *pLlocalInfo = NULL;
    tCgaOptions    *pCgaParams = NULL;
    UINT4  u4Offset = IP6_ZERO;
    UINT4  u4ContextId = IP6_ZERO;
    INT4   i4RetVal = IP6_FAILURE;
    UINT1  au1RsaSign[ND6_SEND_RSA_SIGN_LEN];

    MEMSET (&RsaOptions, IP6_ZERO, sizeof (RsaOptions));
    MEMSET (au1RsaSign, IP6_ZERO, ND6_SEND_RSA_SIGN_LEN);

    u4ContextId = pIf6->pIp6Cxt->u4ContextId;
    RsaOptions.pu1Signature = au1RsaSign;

    u4Offset = IP6_BUF_WRITE_OFFSET (pBuf);

    if (IS_ADDR_UNSPECIFIED(*pSrc))
    {
        /* NS for DAD scenario */
        pCgaAddr = pTargetAddr;
    }
    else
    {
        pCgaAddr = pSrc;
    }

    if (Ip6AddrType (pCgaAddr) == ADDR6_LLOCAL)
    {
        /*
         * Get the Link local Address Info Structure for associated
         * CGA Parameters Structure
         */
        pLlocalInfo = (tIp6LlocalInfo *) (VOID *) Ip6GetAddrInfo (pIf6, pCgaAddr);
        if (pLlocalInfo == NULL)
        {
           IP6_TRC_ARG (u4ContextId, ND6_MOD_TRC, MGMT_TRC,
                        ND6_NAME, "Nd6SeNDAddOpts Addr Info Failed\n");

           Ip6BufRelease (u4ContextId, pBuf, FALSE, ND6_MODULE);
           return IP6_FAILURE;
        }
        pCgaParams = &(pLlocalInfo->cgaParams);
    }
    else
    {
        /* 
         * Get the Address Info Structure for associated 
         * CGA Parameters Structure 
         */
        pAddrInfo = (tIp6AddrInfo *) (VOID *) Ip6GetAddrInfo (pIf6, pCgaAddr);
        if (NULL == pAddrInfo)
        {
            IP6_TRC_ARG (u4ContextId, ND6_MOD_TRC, MGMT_TRC,
                         ND6_NAME, "Nd6SeNDAddOpts Addr Info Failed\n");

            Ip6BufRelease (u4ContextId, pBuf, FALSE, ND6_MODULE);
            return IP6_FAILURE;
        }
        pCgaParams = &(pAddrInfo->cgaParams);
    }

    /* Adding CGA Option to the Packet */
    i4RetVal = Nd6SeNDAddCgaOpt (u4ContextId, pBuf, pCgaParams,
                                 pu4PktLen);
    if (IP6_FAILURE == i4RetVal)
    {
        IP6_TRC_ARG (u4ContextId, ND6_MOD_TRC, MGMT_TRC, ND6_NAME,
                     "Nd6SeNDAddOpts Adding CGA Opt Failed\n");
        return IP6_FAILURE;
    }

    /* Adding TimeStamp Option to the Packet */
    i4RetVal = Ip6SeNDAddTimeStamp (u4ContextId, pBuf, pu4PktLen);
    if (IP6_FAILURE == i4RetVal)
    {
        IP6_TRC_ARG (u4ContextId, ND6_MOD_TRC, MGMT_TRC, ND6_NAME,
                     "Nd6SeNDAddOpts Adding TimeStamp Opt Failed\n");
        return IP6_FAILURE;
    }

    if (ND6_SOLICITED_MSG == u1SeNDFlag || ND6_SEND_MSG == u1SeNDFlag ||
	ND6_SEND_SOL_RA_MSG == u1SeNDFlag || ND6_SEND_RS_MSG == u1SeNDFlag)
    {
        if (IS_ADDR_SOLICITED(*pDst))
        {
            /* NS for DAD scenario */
            pCgaAddr = pTargetAddr;
        }
        else
        {
            pCgaAddr = pDst;
        }

        /* Adding Nonce Option to the Packet */
        i4RetVal = Ip6SeNDAddNonce (pIf6, pCgaAddr, pBuf,
                                    u1SeNDFlag, pu4PktLen);
        if (IP6_FAILURE == i4RetVal)
        {
            IP6_TRC_ARG (u4ContextId, ND6_MOD_TRC, MGMT_TRC,ND6_NAME,
                         "Nd6SeNDAddOpts Adding Nonce Opt Failed\n");
            return IP6_FAILURE;
        }
    }

    /* Generate RSA Option */
    i4RetVal = Nd6SeNDRsaGenerate (pIf6, pSrc, pDst, pBuf, *pu4PktLen,
                                   pCgaParams, &RsaOptions);
    if (IP6_FAILURE == i4RetVal)
    {
        IP6_TRC_ARG (u4ContextId, ND6_MOD_TRC, MGMT_TRC, ND6_NAME,
                     "Nd6SeNDAddOpts Adding RSA Opt Failed\n");
        return IP6_FAILURE;
    }

    /* Adding RSA Option should be at the last */
    /* Add RSA Option to the Packet */
    i4RetVal = Nd6SeNDAddRsaOpt (u4ContextId, pBuf,
                                 &RsaOptions, pu4PktLen);
    if (IP6_FAILURE == i4RetVal)
    {
        IP6_TRC_ARG (u4ContextId, ND6_MOD_TRC, MGMT_TRC, ND6_NAME,
                     "Nd6SeNDAddOpts Adding SEND Opts Failed\n");
        return IP6_FAILURE;
    }
    IP6_BUF_WRITE_OFFSET (pBuf) = u4Offset;
    return IP6_SUCCESS;
}

/********************************************************************
 * DESCRIPTION : This function processes the RSA option in Secure ND
 *               message. 
 *
 * INPUTS      : pIf6        - Pointer to Logical interface
 *               pIp6        - Pointer to IPv6 header
 *               pBuf        - Pointer to the IPv6 packet
 *               u1Len       - Length of RSA option in 8 octects
 *               u4NdOffset  - Offset of the ND message in pBuf
 *               pCgaOptions - Pointer to CGA options
 *
 * OUTPUTS     : None
 *
 * RETURNS     : IP6_SUCCESS/IP6_FAILURE
 *******************************************************************/
INT4
Nd6SeNDProcessRsaOption (tIp6If *pIf6, tIp6Hdr *pIp6,
                         tCRU_BUF_CHAIN_HEADER *pBuf,
                         UINT1 u1Len, UINT4 u4NdOffset,
                         tCgaOptions *pCgaOptions)
{
    tRsaOptions  RsaOptions;
    UINT4  u4ContextId = IP6_ZERO;
    UINT4  u4Length = IP6_ZERO;
    UINT4  u4CopyLen = IP6_ZERO;
    UINT4  u4Offset = IP6_ZERO;
    UINT1 *pu1RsaParams = NULL;
    UINT1  au1RsaParams[IP6_SEND_CGA_BYTES];

    MEMSET (&RsaOptions, IP6_ZERO, sizeof (tRsaOptions));
    MEMSET (au1RsaParams, IP6_ZERO, IP6_SEND_CGA_BYTES);

    /* get the context from interface */
    u4ContextId = pIf6->pIp6Cxt->u4ContextId;

    /* u1Len is always in 8 octets */
    u4Length = (UINT4) (u1Len * BYTES_IN_OCTECT);

    /* read the rsa parameters */
    if ((pu1RsaParams = (UINT1 *) Ip6BufRead (pBuf, (UINT1 *)au1RsaParams,
                                             IP6_BUF_READ_OFFSET(pBuf),
                                             u4Length,
                                             TRUE)) == NULL)
    {
        IP6_TRC_ARG1 (u4ContextId, ND6_MOD_TRC, BUFFER_TRC, ND6_NAME,
                     "Nd6SeNDProcessRsaOption: BufRead Failed "
                      "BufPtr %p\n", pBuf);
        return IP6_FAILURE;
    }

    /* move the offset to point to key hash */
    pu1RsaParams += IP6_FOUR;

    /* copy key hash */
    MEMCPY (&RsaOptions.au1KeyHash, pu1RsaParams, ND6_SEND_RSA_KEY_HASH);
    pu1RsaParams += ND6_SEND_RSA_KEY_HASH;

    /* copy signature */
    RsaOptions.pu1Signature = pu1RsaParams;

    /* signature length */
    RsaOptions.u4SignLen = (u4Length - (IP6_FOUR + ND6_SEND_RSA_KEY_HASH));

    u4Offset = IP6_BUF_READ_OFFSET (pBuf);
    IP6_BUF_READ_OFFSET (pBuf) = u4NdOffset;

     /* Length of the Data  needed for RSA Verification */
     u4CopyLen = (u4Offset - (u4Length + u4NdOffset)); 
  
     if (IP6_FAILURE == Nd6SeNDRsaVerify (pIf6, pIp6, pBuf, u4CopyLen,
                                          pCgaOptions, &RsaOptions))
     {
        IP6_TRC_ARG (u4ContextId, ND6_MOD_TRC, BUFFER_TRC, ND6_NAME,
                     "Nd6SeNDProcessRsaOption : Timestamp verification "
                     "failed \n");
        return IP6_FAILURE;
     }
     IP6_BUF_READ_OFFSET (pBuf) = u4Offset;

    return IP6_SUCCESS;
}

/********************************************************************
 * DESCRIPTION : This function processes the Nonce option in Secure ND
 *               message. 
 *
 * INPUTS      : pIf6     - Pointer to Logical interface
 *               pIp6     - Pointer to IPv6 header
 *               pBuf     - Pointer to the IPv6 packet
 *
 * OUTPUTS     : pu1Nonce - Pointer to Nonce 
 *
 * RETURNS     : IP6_SUCCESS/IP6_FAILURE
 *******************************************************************/
INT4
Nd6SeNDProcessNonceOption (tIp6If *pIf6, tIp6Hdr *pIp6,
                           tCRU_BUF_CHAIN_HEADER *pBuf,
                           UINT1 *pu1Nonce, UINT1 u1SeNDFlag)
{
    tNd6SendNonce  Nonce;
    tNd6SendNonce *pNonce = NULL;
    UINT4  u4ContextId = IP6_ZERO;

    MEMSET(&Nonce, IP6_ZERO, sizeof (tNd6SendNonce));

    /* get the context from interface */
    u4ContextId = pIf6->pIp6Cxt->u4ContextId;

    if ((pNonce =
            (tNd6SendNonce *) Ip6BufRead (pBuf, (UINT1 *)&Nonce,
                                         IP6_BUF_READ_OFFSET (pBuf),
                                         sizeof (tNd6SendNonce),
                                         TRUE)) == NULL)
    {
        IP6_TRC_ARG1 (u4ContextId, ND6_MOD_TRC, BUFFER_TRC, ND6_NAME,
                      "Nd6SeNDProcessNonceOption: BufRead Failed"
                      "BufPtr %p\n", pBuf);
         return IP6_FAILURE;
    }

    if (pu1Nonce == NULL)
    {
        /* 
         * nonce in advertisements need to be verified
         */
        if (Ip6SeNDVerifyNonce (pIf6, &(pIp6->srcAddr), pNonce, u1SeNDFlag)
                == IP6_FAILURE)
        {
            IP6_TRC_ARG (u4ContextId, ND6_MOD_TRC, BUFFER_TRC, ND6_NAME,
                      "Nd6SeNDProcessNonceOption: Nonce verify failed\n");
            return IP6_FAILURE;
        }
    }
    else
    {
        /* nonce in solicitations need to be saved */
        MEMCPY (pu1Nonce, pNonce->au1Nonce, ND6_NONCE_LENGTH);
    }

    return IP6_SUCCESS;
}

/********************************************************************
 * DESCRIPTION : This function processes the Timestamp option in
 *               Secure ND message. 
 *
 * INPUTS      : pIf6     - Pointer to Logical interface
 *               pIp6     - Pointer to IPv6 header
 *               pBuf     - Pointer to the IPv6 packet
 *
 * OUTPUTS     : pu4TSNew - Pointer to Timestamp value
 *
 * RETURNS     : IP6_SUCCESS/IP6_FAILURE
 *******************************************************************/
INT4
Nd6SeNDProcessTSOption (tIp6If *pIf6, tIp6Hdr *pIp6,
                        tCRU_BUF_CHAIN_HEADER *pBuf, 
                        UINT4 *pu4TSNew)
{
    tNd6SendTimeStamp  Timestamp;
    tNd6SendTimeStamp *pTimestamp = NULL;
    UINT4 u4ContextId = IP6_ZERO;

    MEMSET (&Timestamp, IP6_ZERO, sizeof (tNd6SendTimeStamp));

    /* get the context from interface */
    u4ContextId = pIf6->pIp6Cxt->u4ContextId;

    if ((pTimestamp = 
            (tNd6SendTimeStamp *) Ip6BufRead (pBuf, (UINT1*)&Timestamp,
                                              IP6_BUF_READ_OFFSET (pBuf),
                                              sizeof (tNd6SendTimeStamp),
                                              TRUE)) == NULL)
    {
        IP6_TRC_ARG1 (u4ContextId, ND6_MOD_TRC, BUFFER_TRC, ND6_NAME,
                      "Nd6SeNDProcessTSOption: BufRead Failed"
                      "BufPtr %p\n", pBuf);
        return IP6_FAILURE;
    }

    if (IP6_FAILURE == Ip6SeNDVerifyTimeStamp (pIf6, &(pIp6->srcAddr),
                                               pTimestamp, pu4TSNew))
    {
        IP6_TRC_ARG (u4ContextId, ND6_MOD_TRC, BUFFER_TRC, ND6_NAME,
                     "Nd6SeNDProcessTSOption: Timestamp verification "
                     "failed \n");
        return IP6_FAILURE;
    }

    return IP6_SUCCESS;
}

/********************************************************************
 * DESCRIPTION : This function processes the CGA option in
 *               Secure ND message. 
 *
 * INPUTS      : pIf6        - Pointer to Logical interface
 *               pIp6        - Pointer to IPv6 header
 *               pBuf        - Pointer to the IPv6 packet
 *               u1Len       - Length in 8 octects
 *
 * OUTPUTS     : pCgaOptions - Pointer to CGA options 
 *
 * RETURNS     : IP6_SUCCESS/IP6_FAILURE
 *******************************************************************/
INT4
Nd6SeNDProcessCgaOption (tIp6If *pIf6, tIp6Hdr *pIp6, 
                         tCRU_BUF_CHAIN_HEADER *pBuf, 
                         UINT1 u1Len, tCgaOptions *pCgaOptions)
{
    UINT1 *pu1CgaParams = NULL;
    UINT4  u4ContextId = IP6_ZERO;
    UINT4  u4Length = IP6_ZERO; 
    UINT1  u1PadLength = IP6_ZERO;
    UINT1  au1CgaParams[IP6_SEND_CGA_BYTES];

    MEMSET (au1CgaParams, IP6_ZERO, IP6_SEND_CGA_BYTES);

    /* get the context from interface */
    u4ContextId = pIf6->pIp6Cxt->u4ContextId;

    /* u1Len is always in 8 octets */
    u4Length = (UINT4) (u1Len * BYTES_IN_OCTECT);

    /* read the cga parameters */
    if ((pu1CgaParams = (UINT1 *) Ip6BufRead (pBuf, (UINT1 *) au1CgaParams,
                                             IP6_BUF_READ_OFFSET (pBuf),
                                             u4Length, TRUE)) == NULL)
    {
        IP6_TRC_ARG1 (u4ContextId, ND6_MOD_TRC, BUFFER_TRC, ND6_NAME,
                     "Nd6SeNDProcessCgaOption: BufRead Failed "
                      "BufPtr %p\n", pBuf);
        return IP6_FAILURE;
    }

    /* get the pad length */
    u1PadLength = au1CgaParams[IP6_TWO];

    /* move the offset to point to Modifier */
    pu1CgaParams += IP6_FOUR;

    /* copy modifier */
    MEMCPY (pCgaOptions->au1Modifier, pu1CgaParams, ND6_SEND_CGA_MOD_LEN);
    pu1CgaParams += ND6_SEND_CGA_MOD_LEN;

    /* copy prefix */
    MEMCPY (pCgaOptions->au1Prefix, pu1CgaParams, ND6_SEND_CGA_PREFIX_LEN);
    pu1CgaParams += ND6_SEND_CGA_PREFIX_LEN;

    /* copy collision count */
    pCgaOptions->u1Collisions = *pu1CgaParams;
    pu1CgaParams++;

    /* get the public key length */
    u4Length = (UINT4) (u4Length - 
                        (UINT4)(IP6_FOUR + u1PadLength +
                         ND6_SEND_CGA_MOD_LEN + 
                         ND6_SEND_CGA_PREFIX_LEN +
                         IP6_ONE));

    /* fill cga options */
    MEMCPY(pCgaOptions->pu1DerPubKey, pu1CgaParams, u4Length);
    pCgaOptions->u2PubKeyLen =  (UINT2)u4Length;

    if (IP6_FAILURE == Nd6SeNDCgaVerify (pIf6, &(pIp6->srcAddr),
                                         pCgaOptions))
    {
        IP6_TRC_ARG (u4ContextId, ND6_MOD_TRC, BUFFER_TRC, ND6_NAME,
                     "Nd6SeNDProcessCgaOption: CGA verification failed\n");
        return IP6_FAILURE;
    }
    
    return IP6_SUCCESS;
}

/********************************************************************
 * DESCRIPTION : This function sends the Solicited CPS as response
 *               When received a solicited Router Adverisement
 *
 * INPUTS      : pIf6                -  Pointer to the interface.
 *               pIp6                -  Pointer to the Ipv6 header.
 *
 * OUTPUTS     : None.
 *
 * RETURNS     : IP6_SUCCESS/IP6_FAILURE.
 *******************************************************************/

INT4
Nd6SeNDCertPathSol (tIp6If *pIf6, tIp6Hdr *pIp6)
{
    tIp6Addr         DstAddr6;
    tIp6Addr        *pIp6SrcAddr = NULL;
    tNd6CacheEntry  *pNd6cEntry = NULL;
    INT4             i4RetVal = IP6_FAILURE;

    MEMSET (&DstAddr6, IP6_ZERO, sizeof (tIp6Addr));

    /*
     * Destination address in CPA is set to all-nodes Multicast if
     * the source address of CPS being unspecified, otherwise it is
     * set to the Solicited-Node multicast address corresponding to the
     * source address RFC:3971-Section 6.4.5
     */
    if (IS_ADDR_UNSPECIFIED (pIp6->srcAddr))
    {
        SET_ALL_ROUTERS_MULTI (DstAddr6);
    }
    else
    {
        GET_ADDR_SOLICITED (pIp6->srcAddr, DstAddr6);
    }

    pIp6SrcAddr = Ip6GetLlocalAddr (pIf6->u4Index);
    if (pIp6SrcAddr != NULL)
    {
        /* Send the CPA message */
        i4RetVal = Nd6SendCertPathSol (pIf6, pNd6cEntry, pIp6SrcAddr,
                                       &DstAddr6);
    }
    return (i4RetVal);

}

/********************************************************************
 * DESCRIPTION : This function sends the CPS messages by adding
 *               expected fields
 *
 * INPUTS       :pIf6                 - Pointer to the interface.
 *               pNd6cEntry           - Pointer to Cache Entry
 *               pSrcAddr6            - Pointer to IPv6 Src Address
 *               pDstAddr6            - Pointer to IPv6 Dst Address
 *
 * OUTPUTS     : None
 *
 * RETURNS     : IP6_SUCCESS/IP6_FAILURE
 *******************************************************************/
INT4
Nd6SendCertPathSol (tIp6If *pIf6, tNd6CacheEntry *pNd6cEntry,
                    tIp6Addr *pSrcAddr6, tIp6Addr *pDstAddr6)
{
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    UINT4  u4Nd6Size = IP6_ZERO;
    UINT4  u4TotalSize = IP6_ZERO;
    UINT4  u4Woffset = IP6_ZERO;
    UINT4  u4ContextId = IP6_ZERO;
    UINT1  u1SeNDFlag = ND6_SEND_ADD;
    UINT4  u4BufLen = IP6_ZERO;

    u4Woffset = Ip6BufWoffset (ND6_MODULE);
    u4ContextId = pIf6->pIp6Cxt->u4ContextId;

    /* allocate a buffer for the IPv6 packet + size of CPA message */
    u4Nd6Size = sizeof (tNd6SendCertPathSol) + ND6_SEND_CPS_LENGTH 
				+ sizeof (tNd6TaExtnHdr);
    u4TotalSize = u4Woffset + u4Nd6Size;
    if ((pBuf = Ip6BufAlloc (u4ContextId, u4TotalSize,
                             u4Woffset, ND6_MODULE)) == NULL)
    {
        IP6_TRC_ARG (u4ContextId, ND6_MOD_TRC, BUFFER_TRC ,
                     ND6_NAME, "Nd6SendCertPathSol"
                     "BufAlloc Failed\n");
        return (IP6_FAILURE);
    }

    /* Fill the CPA Message fields */
    if (Nd6FillCertPathSolInCxt (pIf6->pIp6Cxt, pBuf, &u4BufLen)
        == IP6_FAILURE)
    {
        IP6_TRC_ARG (u4ContextId, ND6_MOD_TRC, MGMT_TRC,
                     ND6_NAME, " Nd6SendCertPathSol "
                     "Filling Cert Path Sol Failed\n");
        Ip6BufRelease (u4ContextId, pBuf, FALSE, ND6_MODULE);
        return (IP6_FAILURE);
    }

    if (Nd6FillCertSolTAOptInCxt (pIf6->pIp6Cxt, pBuf, &u4BufLen)
            == IP6_FAILURE)
    {
        IP6_TRC_ARG (u4ContextId, ND6_MOD_TRC, MGMT_TRC,
                     ND6_NAME, "Nd6SendCertPathSol "
                     "Filling Trust Anchor Failed\n");
        Ip6BufRelease (u4ContextId, pBuf, FALSE, ND6_MODULE);
        return (IP6_FAILURE);
    }

    return (Ip6SendNdMsg (pIf6, pNd6cEntry, pSrcAddr6, pDstAddr6,
                          u4BufLen, pBuf, u1SeNDFlag, NULL));
}


/********************************************************************
 * DESCRIPTION : This function fills CPS message
 *
 * INPUTS      : pBuf            - Pointer to the ND packet
 *                                 for adding CPS message
 *               pIp6Cxt         - Pointer to IP6Cxt
 * OUTPUTS     : None
 *
 * RETURNS     : IP6_SUCCESS/IP6_FAILURE
 *******************************************************************/
INT4
Nd6FillCertPathSolInCxt (tIp6Cxt *pIp6Cxt,
                         tCRU_BUF_CHAIN_HEADER *pBuf, UINT4 *pu4BufLen)
{
    tNd6SendCertPathSol Nd6SeNdCertPathSol;
    UINT2 u2Identifier = IP6_ZERO;
    INT4 i4RetVal = IP6_ZERO;
	
    MEMSET (&Nd6SeNdCertPathSol, IP6_ZERO, sizeof (tNd6SendCertPathSol));

    Nd6SeNdCertPathSol.icmp6Hdr.u1Type = ND6_SEND_CPS;
    Nd6SeNdCertPathSol.icmp6Hdr.u1Code = ND6_RSVD_CODE;
    Nd6SeNdCertPathSol.u2Component = IPV6_MAX_AS_NUM;

    i4RetVal = UtilRandNumGen ((UINT1 *)&u2Identifier, IP6_TWO);
    if (OSIX_FAILURE == i4RetVal)
    {
      return IP6_FAILURE;
    }
    Nd6SeNdCertPathSol.u2Identifier = OSIX_HTONS(u2Identifier);

    if (Ip6BufWrite (pBuf, (UINT1 *) &Nd6SeNdCertPathSol,
                     IP6_BUF_WRITE_OFFSET (pBuf),
                     sizeof (tNd6SendCertPathSol), TRUE) == IP6_FAILURE)
    {
        IP6_TRC_ARG2 (pIp6Cxt->u4ContextId, ND6_MOD_TRC, BUFFER_TRC,
                      ND6_NAME, "Nd6FillCertPathSolInCxt"
                      "BufWrite Failed, BufPtr %p Offset %d\n",
                      pBuf, IP6_BUF_WRITE_OFFSET (pBuf));

        /* CRU Buf will be released in caller */
        return (IP6_FAILURE);
    }
    *pu4BufLen += sizeof (tNd6SendCertPathSol);
    UNUSED_PARAM(pIp6Cxt);
    return (IP6_SUCCESS);
}


/********************************************************************
 * DESCRIPTION : This function fills TA option in the CPS message
 *
 * INPUTS      : pBuf            - Pointer to the ND packet
 *                                 for adding CPS message
 *               pIp6Cxt         - Pointer to IP6Cxt
 * OUTPUTS     : None
 *
 * RETURNS     : IP6_SUCCESS/IP6_FAILURE
 *******************************************************************/
INT4
Nd6FillCertSolTAOptInCxt (tIp6Cxt *pIp6Cxt,
                          tCRU_BUF_CHAIN_HEADER *pBuf, UINT4 *pu4BufLen)
{
    tNd6TaExtnHdr Nd6TaExtnHdr;
    UINT1 *pTAName = NULL;
    UINT1  u1Len = IP6_ZERO;
    INT4   i4DerLen = IP6_ZERO;
    UINT1  au1TAName[ND6_TA_MAX_NAME_LEN];

    MEMSET (&Nd6TaExtnHdr, IP6_ZERO, sizeof(Nd6TaExtnHdr));
    MEMSET (au1TAName, IP6_ZERO, ND6_TA_MAX_NAME_LEN);

    pTAName = au1TAName;
    Nd6TaExtnHdr.u1Type = ND6_SEND_TRUST_ANCHOR;
    Nd6TaExtnHdr.u1NameType = ND6_SEND_TA_NAMETYPE;

#ifdef SSL_WANTED
    if (SSL_FAILURE == SSLArGetDerTAName (&pTAName, &i4DerLen))
    {
        IP6_TRC_ARG (pIp6Cxt->u4ContextId, ND6_MOD_TRC, BUFFER_TRC,
                     ND6_NAME, "Nd6FillCertSolTAOptInCxt"
                     "Getting TA Name Failed\n");
        return IP6_FAILURE;
    }
#else
    UNUSED_PARAM(i4DerLen);
    UNUSED_PARAM(pTAName);
    return IP6_FAILURE;
#endif
	
    u1Len = (UINT1)(sizeof(Nd6TaExtnHdr) + i4DerLen);

    Nd6TaExtnHdr.u1Len = (UINT1)(u1Len / IP6_EIGHT);
    if (u1Len % IP6_EIGHT != IP6_ZERO)
    {
        Nd6TaExtnHdr.u1Len = (UINT1)((u1Len/IP6_EIGHT) + IP6_ONE);
    }
    Nd6TaExtnHdr.u1PadLen = (UINT1)((Nd6TaExtnHdr.u1Len * IP6_EIGHT) - u1Len);
    u1Len = (UINT1)(Nd6TaExtnHdr.u1Len * IP6_EIGHT);
	
    if (Ip6BufWrite (pBuf, (UINT1 *) &Nd6TaExtnHdr,
                     IP6_BUF_WRITE_OFFSET (pBuf),
                     sizeof (tNd6TaExtnHdr), TRUE) == IP6_FAILURE)
    {
        IP6_TRC_ARG2 (pIp6Cxt->u4ContextId, ND6_MOD_TRC, BUFFER_TRC,
                      ND6_NAME, "Nd6FillCertPathSolInCxt"
                      "BufWrite Failed, BufPtr %p Offset %d\n",
                      pBuf, IP6_BUF_WRITE_OFFSET (pBuf));

        /* CRU Buf will be released in caller */
        return (IP6_FAILURE);
    }
	*pu4BufLen +=  sizeof (tNd6TaExtnHdr); 
	
    if (Ip6BufWrite (pBuf, (UINT1 *)au1TAName, 
                     IP6_BUF_WRITE_OFFSET (pBuf),
                     (u1Len - sizeof (tNd6TaExtnHdr)), TRUE) == IP6_FAILURE)
    {
        IP6_TRC_ARG2 (pIp6Cxt->u4ContextId, ND6_MOD_TRC, BUFFER_TRC,
                      ND6_NAME, "Nd6FillCertPathSolInCxt"
                      "BufWrite Failed, BufPtr %p Offset %d\n",
                      pBuf, IP6_BUF_WRITE_OFFSET (pBuf));
        /* CRU Buf will be released in caller */
        return (IP6_FAILURE);
    }
	*pu4BufLen += (u1Len - sizeof (tNd6TaExtnHdr));
        UNUSED_PARAM(pIp6Cxt); 
	return (IP6_SUCCESS);
}


/********************************************************************
 * DESCRIPTION : This function process CPS message received
 *
 * INPUTS      : pIf6  - Pointer to the Logical Interface
 *               pIp6  - Pointer to the IPv6 Header
 *               pBuf  - Pointer to the IPv6 Packet
 *               u2Len - Packet Length
 *
 * OUTPUTS     : None
 *
 * RETURNS     : IP6_SUCCESS/IP6_FAILURE
 *******************************************************************/

INT4
Nd6RcvCertPathAdv (tIp6If *pIf6, tIp6Hdr *pIp6,
                   tCRU_BUF_CHAIN_HEADER *pBuf, UINT2 u2Len)
{
    tNd6SendCertPathAdv  Nd6CertPathAdv;
    tNd6SendCertPathAdv *pNd6CertPathAdv = NULL;
    UINT1               *pu1TrustAnchorBuffer = NULL;
    UINT4                u4Nd6SendOffset = IP6_ZERO;
    UINT4                u4ContextId = IP6_ZERO;
    UINT1                u1Copy = IP6_ZERO;

    MEMSET (&Nd6CertPathAdv, IP6_ZERO, sizeof (tNd6SendCertPathAdv));

    /* get the context from the interface */
    u4ContextId = pIf6->pIp6Cxt->u4ContextId;

    /* memory allocation for linear buffer */
    pu1TrustAnchorBuffer = Ip6GetMem (u4ContextId,
                                      gIp6GblInfo.i4Ip6Nd6SecurePoolId);
    if (NULL == pu1TrustAnchorBuffer)
    {
        IP6_TRC_ARG2 (u4ContextId, ND6_MOD_TRC, BUFFER_TRC,
                      ND6_NAME, "Nd6RcvCertPathAdv "
                      "Buffer Failed, BufPtr %p Offset %d\n",
                      pBuf, u4Nd6SendOffset);

        return (IP6_FAILURE);
    }
    MEMSET (pu1TrustAnchorBuffer, IP6_ZERO, ND6_SEND_MAX_BUF_SIZE);

    /* Extract CPS Message from the buffer */
    u4Nd6SendOffset = IP6_BUF_READ_OFFSET (pBuf) - sizeof (tIcmp6PktHdr);
    IP6_BUF_READ_OFFSET (pBuf) -= sizeof (tIcmp6PktHdr);

    if ((pNd6CertPathAdv = (tNd6SendCertPathAdv *) Ip6BufRead
         (pBuf, (UINT1 *) &Nd6CertPathAdv, u4Nd6SendOffset,
          sizeof (tNd6SendCertPathAdv), u1Copy)) == NULL)
    {
        IP6_TRC_ARG2 (u4ContextId, ND6_MOD_TRC, BUFFER_TRC,
                      ND6_NAME, "Nd6RcvCertPathAdv "
                      " BufRead Failed, BufPtr %p Offset %d\n",
                      pBuf, u4Nd6SendOffset);

        Ip6RelMem(u4ContextId, (UINT2)gIp6GblInfo.i4Ip6Nd6SecurePoolId,
                  pu1TrustAnchorBuffer);
        return (IP6_FAILURE);
    }

    /* Validate the CPS message */
    if (Nd6ValCertPathAdvInCxt (pIf6, pIp6, pBuf, pNd6CertPathAdv,
                                pu1TrustAnchorBuffer, u2Len)
        == IP6_FAILURE)
    {
        IP6_TRC_ARG (u4ContextId, ND6_MOD_TRC, BUFFER_TRC,
                      ND6_NAME, "Nd6RcvCertPathAdv Validation Fails\n");

        Ip6RelMem(u4ContextId, (UINT2)gIp6GblInfo.i4Ip6Nd6SecurePoolId,
                  pu1TrustAnchorBuffer);

        /* Increment the count on ICMP bad packets received */
        ICMP6_INC_IN_ERRS (pIf6->pIp6Cxt->Icmp6Stats);
        ICMP6_INTF_INC_IN_ERRS (pIf6);
        return (IP6_FAILURE);
    }
	return IP6_SUCCESS;
}

/********************************************************************
 * DESCRIPTION : This function validates the CPS message received
 *
 * INPUTS      : pIp6                 -  Pointer to the IPv6 header
 *               pNd6CertPathSol      -  Pointer to the CPS
 *               u2Len                -  Length of the Packet.
 *               pBuf                 -  Pointer to the IPv6 Packet
 *               pu1TrustAnchorBuffer -  Pointer to Trust Anchor
 *               u4TAOptLen           -  Trust Anchor Option Length
 *
 * OUTPUTS     : pu4TACheck           -  Pointer to Trust Anchor Check
 *
 * RETURNS     : IP6_SUCCESS/IP6_FAILURE
 *******************************************************************/
INT4
Nd6ValCertPathAdvInCxt (tIp6If *pIf6, tIp6Hdr *pIp6,
                        tCRU_BUF_CHAIN_HEADER *pBuf,
                        tNd6SendCertPathAdv *pNd6CertPathAdv,
                        UINT1 *pu1TrustAnchorBuffer, UINT2 u2Len)
{
    tNd6TaExtnHdr   Nd6TaExtnHdr;
    tNd6TaExtnHdr  *pNd6TaExtnHdr = NULL;
    UINT1          *pu1Buffer = NULL;
    UINT4           u4ContextId = IP6_ZERO;
    UINT4           u4ExtOffset = IP6_ZERO;
    UINT4           u4SeNdTAOptLen = IP6_ZERO;

    MEMSET (&Nd6TaExtnHdr, IP6_ZERO, sizeof (Nd6TaExtnHdr));

    /* get the context from interface */
    u4ContextId = pIf6->pIp6Cxt->u4ContextId; 

    if (Nd6ValidateMsgHdrInCxt (pIf6->pIp6Cxt, pIp6,
           &pNd6CertPathAdv->icmp6Hdr) == IP6_FAILURE)
    {
        IP6_TRC_ARG (u4ContextId, ND6_MOD_TRC,
                     MGMT_TRC | ALL_FAILURE_TRC, ND6_NAME,
                     "Validation of Msg Header Failed\n");
        return (IP6_FAILURE);
    }

    if (u2Len < sizeof (tNd6SendCertPathAdv))
    {
        IP6_TRC_ARG1 (u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC,
                      ND6_NAME, "Nd6ValCertPathAdvInCxt "
                      "Incorrect Len %d\n", u2Len);
        return (IP6_FAILURE);
    }

    if ((pNd6CertPathAdv->u2Identifier) == IPV6_MIN_AS_NUM)
    {
        IP6_TRC_ARG1 (u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC,
                      ND6_NAME, "Nd6ValidateCertPathSol "
                      "CPS Fail- Identifier Field%d :\n",
                      pNd6CertPathAdv->u2Identifier);
        return (IP6_FAILURE);
    }

    if ((OSIX_NTOHS(pNd6CertPathAdv->u2AllComponent)) != 
             ND6_SEND_ALL_COMPONENT)
    {
        IP6_TRC_ARG1 (u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC,
                      ND6_NAME, "Nd6ValidateCertPathSol "
                      "CPS Fail- Component Field%d :\n",
                      pNd6CertPathAdv->u2AllComponent);
        return (IP6_FAILURE);
    }

    u4ExtOffset = IP6_BUF_READ_OFFSET (pBuf);

    /* Get the Trust Anchor option Header */
    if ((pNd6TaExtnHdr = 
            (tNd6TaExtnHdr *) Ip6BufRead (pBuf, (UINT1 *) &Nd6TaExtnHdr, 
                                          IP6_BUF_READ_OFFSET(pBuf),
                                          sizeof (tNd6TaExtnHdr), 
                                          TRUE)) == NULL)
    {
        IP6_TRC_ARG2 (u4ContextId, ND6_MOD_TRC, BUFFER_TRC,
                      ND6_NAME, "Nd6ValCertPathAdvInCxt "
                      "Process TA Extn - BufRead Fail "
                      "BufPtr %p Offset %d\n", pBuf, u4ExtOffset);
        return (IP6_FAILURE);
    }

    if ((Nd6TaExtnHdr.u1Type) != ND6_SEND_TRUST_ANCHOR)
    {
        IP6_TRC_ARG1 (u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC,
                      ND6_NAME, "Nd6ValCertPathSolInCxt "
                      "Invalid Len %d\n", Nd6TaExtnHdr.u1Type);
        return (IP6_FAILURE);
    }

    if ((Nd6TaExtnHdr.u1NameType) != ND6_SEND_TA_NAMETYPE)
    {
        IP6_TRC_ARG1 (u4ContextId, ND6_MOD_TRC, DATA_PATH_TRC,
                      ND6_NAME, "Nd6ValCertPathSolInCxt "
                      "Name Type of TA -%d MUST be 1\n",
                      Nd6TaExtnHdr.u1NameType);
        return (IP6_FAILURE);
    }

    u4SeNdTAOptLen = (UINT4) ((Nd6TaExtnHdr.u1Len * IP6_EIGHT) -
                               IP6_TYPE_SEND_LEN_BYTES);
    u4ExtOffset = IP6_BUF_READ_OFFSET (pBuf);

    if ((pu1Buffer =
         (UINT1 *) Ip6BufRead (pBuf,
                               (UINT1 *) pu1TrustAnchorBuffer +
                               IP6_TYPE_SEND_LEN_BYTES,
                               IP6_BUF_READ_OFFSET (pBuf), u4SeNdTAOptLen,
                               TRUE)) == NULL)
    {
        IP6_TRC_ARG2 (u4ContextId, ND6_MOD_TRC, BUFFER_TRC,
                      ND6_NAME, "Nd6ValCertPathAdvInCxt " 
                      "Process TA Extn - BufRead Fail "
                      "BufPtr %p Offset %d\n", pBuf, u4ExtOffset);
        return (IP6_FAILURE);
    }

    MEMCPY (pu1TrustAnchorBuffer, &Nd6TaExtnHdr, sizeof (tNd6TaExtnHdr));
#ifdef SSL_WANTED
    if (SSLArTACertNameVerify (&pu1Buffer, (INT4) u4SeNdTAOptLen)	
            == IP6_FAILURE)
    {
        IP6_TRC_ARG (u4ContextId, ND6_MOD_TRC, BUFFER_TRC,
                     ND6_NAME, "Nd6ValCertPathAdvInCxt "
                     "Verification of TA Name Failed\n");
        return IP6_FAILURE;
    }
#else
	UNUSED_PARAM(pu1Buffer);
	UNUSED_PARAM(u4SeNdTAOptLen);
	return IP6_FAILURE;
#endif
	
	if (Nd6ValidateCertOptInCxt(pIf6, pBuf, u2Len) ==
		IP6_FAILURE)
	{
        IP6_TRC_ARG2 (u4ContextId, ND6_MOD_TRC, BUFFER_TRC,
                      ND6_NAME, "Nd6ValCertPathAdvInCxt "
                      "Process TA Extn - BufRead Fail "
                      "BufPtr %p Offset %d\n", pBuf, u4ExtOffset);
        return (IP6_FAILURE);
	
	}
	UNUSED_PARAM(pNd6TaExtnHdr);
    return (IP6_SUCCESS);
}

/********************************************************************
 * DESCRIPTION : This function validates the CPS message received
 *
 * INPUTS      : pIp6                 -  Pointer to the IPv6 header
 *               pNd6CertPathSol      -  Pointer to the CPS
 *               u2Len                -  Length of the Packet.
 *               pBuf                 -  Pointer to the IPv6 Packet
 *               pu1TrustAnchorBuffer -  Pointer to Trust Anchor
 *               u4TAOptLen           -  Trust Anchor Option Length
 *
 * OUTPUTS     : pu4TACheck           -  Pointer to Trust Anchor Check
 *
 * RETURNS     : IP6_SUCCESS/IP6_FAILURE
 *******************************************************************/
INT4 
Nd6ValidateCertOptInCxt(tIp6If *pIf6, 
                        tCRU_BUF_CHAIN_HEADER *pBuf, UINT2 u2Len)
{
    tNd6CertExtnHdr Nd6CertExtHdr;
    tNd6CertExtnHdr *pNd6CertExtHdr = NULL;
    UINT1  *pu1Buffer = NULL; 
    UINT1  *pu1TempBuffer = NULL;
    UINT4  u4ContextId = IP6_ZERO;

    u4ContextId = pIf6->pIp6Cxt->u4ContextId;
    MEMSET (&Nd6CertExtHdr, IP6_ZERO, sizeof(tNd6CertExtnHdr));
    
    pu1Buffer = Ip6GetMem (u4ContextId,
                           gIp6GblInfo.i4Ip6Nd6SecurePoolId);
    if (NULL == pu1Buffer)
    {
        IP6_TRC_ARG (u4ContextId, ND6_MOD_TRC, MGMT_TRC,
                     ND6_NAME, "Nd6ValidateCertOptInCxt Get Mem Failed\n");

        return IP6_FAILURE;
    }
	
    MEMSET (pu1Buffer, IP6_ZERO, ND6_SEND_MAX_BUF_SIZE);

    if ((pNd6CertExtHdr = 
            (tNd6CertExtnHdr *) Ip6BufRead (pBuf, (UINT1 *) &Nd6CertExtHdr, 
                                            IP6_BUF_READ_OFFSET (pBuf), 
                                            sizeof(tNd6CertExtnHdr),
                                            TRUE)) == NULL)
   {
        IP6_TRC_ARG2 (u4ContextId, ND6_MOD_TRC, BUFFER_TRC,
                      ND6_NAME, "Nd6ValidateCertOptInCxt "
                      "Process Certificate Extn - BufRead Fail "
                      "BufPtr %p Offset %d\n", pBuf, 
                       IP6_BUF_READ_OFFSET (pBuf));
        Ip6RelMem(u4ContextId, (UINT2)gIp6GblInfo.i4Ip6Nd6SecurePoolId,
                  pu1Buffer);
        return (IP6_FAILURE);
    }

    if (u2Len < (UINT2) Nd6CertExtHdr.u1Len)
    {
        IP6_TRC_ARG (u4ContextId, ND6_MOD_TRC, BUFFER_TRC,
                     ND6_NAME, "Nd6ValidateCertOptInCxt "
                     "Verification of Option Length Failed\n");
        Ip6RelMem(u4ContextId, (UINT2)gIp6GblInfo.i4Ip6Nd6SecurePoolId,
                  pu1Buffer);
        return (IP6_FAILURE);
    }

    if (Nd6CertExtHdr.u1Type != ND6_SEND_CERTIFICATE)
    {
        IP6_TRC_ARG (u4ContextId, ND6_MOD_TRC, BUFFER_TRC,
                     ND6_NAME, "Nd6ValidateCertOptInCxt "
                     "Certificate Opt type Verification Failed\n");
        Ip6RelMem(u4ContextId, (UINT2)gIp6GblInfo.i4Ip6Nd6SecurePoolId,
                  pu1Buffer);
        return (IP6_FAILURE);
    }

    if (Nd6CertExtHdr.u1CertType != ND6_SEND_CERT_NAMETYPE)
    {
        IP6_TRC_ARG (u4ContextId, ND6_MOD_TRC, BUFFER_TRC,
                     ND6_NAME, "Nd6ValidateCertOptInCxt "
                     "Certificate Type Verification Failed\n");
        Ip6RelMem(u4ContextId, (UINT2)gIp6GblInfo.i4Ip6Nd6SecurePoolId,
                  pu1Buffer);
        return (IP6_FAILURE);
    }

    if ((pu1TempBuffer =
           (UINT1 *) Ip6BufRead (pBuf, (UINT1 *)pu1Buffer, 
           IP6_BUF_READ_OFFSET (pBuf), 
           ((UINT4) ((Nd6CertExtHdr.u1Len * IP6_EIGHT) - 
           sizeof (tNd6CertExtnHdr))), TRUE)) == NULL)
    {
        IP6_TRC_ARG2 (u4ContextId, ND6_MOD_TRC, BUFFER_TRC,
                      ND6_NAME, "Nd6ValidateCertOptInCxt "
                      "Process TA Extn - BufRead Fail "
                      "BufPtr %p Offset %d\n", pBuf, 
                      Nd6CertExtHdr.u1Len - sizeof (tNd6CertExtnHdr));
        Ip6RelMem(u4ContextId, (UINT2)gIp6GblInfo.i4Ip6Nd6SecurePoolId,
                  pu1Buffer);
        return (IP6_FAILURE);
    }

#ifdef SSL_WANTED
    if (SSLArVerifyCert(&pu1Buffer, 
            ((INT4)((Nd6CertExtHdr.u1Len * IP6_EIGHT) - 
            sizeof (tNd6CertExtnHdr)))) == SSL_FAILURE)
    { 
        IP6_TRC_ARG (u4ContextId, ND6_MOD_TRC, BUFFER_TRC,
                     ND6_NAME, "Nd6ValidateCertOptInCxt "
                     "Verification of TA Name Failed\n");
        Ip6RelMem(u4ContextId, (UINT2)gIp6GblInfo.i4Ip6Nd6SecurePoolId,
                  pu1Buffer);
        return IP6_FAILURE;
    }
    Ip6RelMem(u4ContextId, (UINT2)gIp6GblInfo.i4Ip6Nd6SecurePoolId,
              pu1Buffer);
#else
    UNUSED_PARAM(pu1Buffer);
	UNUSED_PARAM(Nd6CertExtHdr.u1Len);
    return IP6_FAILURE;
#endif
	UNUSED_PARAM(pNd6CertExtHdr);
	UNUSED_PARAM(pu1TempBuffer);
    return IP6_SUCCESS;
}

/********************************************************************
 * DESCRIPTION : This function checks for the unsecured addresss in
 *				 the given interface. 
 *
 * INPUTS      : i4IfIndex - Interface Index 
 *
 * OUTPUTS     : None 
 *
 * RETURNS     : IP6_SUCCESS/IP6_FAILURE
 *******************************************************************/
INT4 
Nd6SecureCheck (INT4 i4IfIndex)
{
	tTMO_SLL_NODE      *pLlAddrSll = NULL;
	tTMO_SLL_NODE      *pAddrSll = NULL;
	tIp6LlocalInfo     *pAddrLlInfo = NULL;
	tIp6AddrInfo	   *pAddrInfo = NULL;
 	if (Ip6ifEntryExists ((UINT4) i4IfIndex) != IP6_SUCCESS)
	{
	   return IP6_FAILURE;
	}
    TMO_SLL_Scan (&gIp6GblInfo.apIp6If[i4IfIndex]->lla6Ilist,
                  pLlAddrSll, tTMO_SLL_NODE *)
    {
        pAddrLlInfo = IP6_LLADDR_PTR_FROM_SLL (pLlAddrSll);
		if (pAddrLlInfo->b1SeNDCgaStatus == IP6_ZERO)
		{
			if (pAddrLlInfo->u1ConfigMethod == IP6_ADDR_AUTO_SL)
			{
				continue;
			}
	        return IP6_FAILURE;
		}
	}	
	
    TMO_SLL_Scan (&gIp6GblInfo.apIp6If[i4IfIndex]->addr6Ilist,
                  pAddrSll, tTMO_SLL_NODE *)
    {
        pAddrInfo = IP6_ADDR_PTR_FROM_SLL (pAddrSll);
        if (pAddrInfo->b1SeNDCgaStatus == IP6_ZERO)
        {
	        return IP6_FAILURE;
        }
    }
	return IP6_SUCCESS;
}

