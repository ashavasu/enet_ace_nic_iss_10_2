/********************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: nd6red.h,v 1.6 2015/06/15 06:54:54 siva Exp $
 *
 * Description: This file contains all macro definitions and 
 *              function prototypes related to L3 Redundancy
 *              for ND module.
 *              
 *******************************************************************/
#ifndef __ND6_RED_H
#define __ND6_RED_H

#include "ip6inc.h"


/* Represents the message types encoded in the update messages */
typedef enum {
    ND6_RED_BULK_REQ_MESSAGE      = RM_BULK_UPDT_REQ_MSG,
    ND6_RED_BULK_UPD_TAIL_MESSAGE = RM_BULK_UPDT_TAIL_MSG,
    ND6_RED_BULK_CACHE_INFO,
    ND6_RED_DYN_CACHE_INFO,
    ND6_RED_CACHE_INFO
}eNd6RedRmMsgType;

typedef enum{
    ND6_HA_UPD_NOT_STARTED = 1,/* 1 */
    ND6_HA_UPD_IN_PROGRESS,    /* 2 */
    ND6_HA_UPD_COMPLETED,      /* 3 */
    ND6_HA_UPD_ABORTED,        /* 4 */
    ND6_HA_MAX_BLK_UPD_STATUS
} eNd6HaBulkUpdStatus;

VOID nd6RedHandleDynSyncAudit (VOID);
VOID nd6ExecuteCmdAndCalculateChkSum  (VOID);

/* Macro Definitions for ND Redundancy */

#define ND6_RM_GET_NUM_STANDBY_NODES_UP() \
          gNd6RedGlobalInfo.u1NumPeersUp = RmGetStandbyNodeCount ()

#define ND6_NUM_STANDBY_NODES() gNd6RedGlobalInfo.u1NumPeersUp

#define ND6_RM_BULK_REQ_RCVD() gNd6RedGlobalInfo.bBulkReqRcvd

#define ND6_RED_MSG_SIZE(pInfo) \
        ((pInfo->u1Action == ND6_RED_ADD_CACHE) ?  \
        ND6_RED_DYN_INFO_SIZE : 4 + 4)

#define ND6_IS_STANDBY_UP() \
          ((gNd6RedGlobalInfo.u1NumPeersUp > 0) ? OSIX_TRUE : OSIX_FALSE)

/* RM wanted */

#define ND6_RM_PUT_1_BYTE(pMsg, pu4Offset, u1MesgType) \
do { \
    RM_DATA_ASSIGN_1_BYTE (pMsg, *(pu4Offset), u1MesgType); \
        *(pu4Offset) += 1;\
}while (0)

#define ND6_RM_PUT_2_BYTE(pMsg, pu4Offset, u2MesgType) \
do { \
    RM_DATA_ASSIGN_2_BYTE (pMsg, *(pu4Offset), u2MesgType); \
        *(pu4Offset) += 2;\
}while (0)

#define ND6_RM_PUT_4_BYTE(pMsg, pu4Offset, u4MesgType) \
do { \
    RM_DATA_ASSIGN_4_BYTE (pMsg, *(pu4Offset), u4MesgType); \
        *(pu4Offset) += 4;\
}while (0)

#define ND6_RM_PUT_N_BYTE(pMsg, pu4Offset, psrc, u4Size) \
do { \
    RM_COPY_TO_OFFSET (pMsg, psrc, *(pu4Offset), u4Size); \
        *(pu4Offset) += u4Size;\
}while (0)

#define ND6_RM_GET_1_BYTE(pMsg, pu4Offset, u1MesgType) \
do { \
        RM_GET_DATA_1_BYTE (pMsg, *(pu4Offset), u1MesgType); \
                *(pu4Offset) += 1;\
}while (0)

#define ND6_RM_GET_2_BYTE(pMsg, pu4Offset, u2MesgType) \
do { \
        RM_GET_DATA_2_BYTE (pMsg, *(pu4Offset), u2MesgType); \
                *(pu4Offset) += 2;\
}while (0)

#define ND6_RM_GET_4_BYTE(pMsg, pu4Offset, u4MesgType) \
do { \
        RM_GET_DATA_4_BYTE (pMsg, *(pu4Offset), u4MesgType); \
                *(pu4Offset) += 4;\
}while (0)

#define ND6_RM_GET_N_BYTE(pMsg, pu4Offset, psrc, u4Size) \
do { \
        RM_GET_DATA_N_BYTE (pMsg, psrc, *(pu4Offset), u4Size); \
                *(pu4Offset) += u4Size;\
}while (0)

#define ND6_RED_MAX_MSG_SIZE        1500
#define ND6_RED_TYPE_FIELD_SIZE     1
#define ND6_RED_LEN_FIELD_SIZE      2
#define ND6_RED_MIM_MSG_SIZE       (1 + 2 + 2)
#define ND6_RED_DYN_INFO_SIZE      (16 + 16 + 6 + 4 + 1 + 1 + 1 + 1 + 1 + 1 + 20)

#define ND6_ONE_BYTE                1
#define ND6_TWO_BYTES               2
#define ND6_FOUR_BYTES              4

#define ND6_RED_MESSAGE_MAX_SIZE   1500

#define ND6_RED_BULK_UPD_TAIL_MSG_SIZE       3
#define ND6_RED_BULK_REQ_MSG_SIZE            3

#define ND6_RED_BULQ_REQ_SIZE       3

#define ND6_RED_ADD_CACHE            1
#define ND6_RED_DEL_CACHE            2

#define ND6_RED_CACHE_DEL         1
#define ND6_RED_CACHE_DONT_DEL    0

#define   ND6_SEM_NAME      ((UINT1 *) "ND6S" )

/* Function prototypes for ND6 Redundancy */

#endif /* __ND6_RED_H */
