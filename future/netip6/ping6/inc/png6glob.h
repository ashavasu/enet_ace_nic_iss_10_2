/*******************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: png6glob.h,v 1.3 2010/08/02 06:57:01 prabuc Exp $
 *
 * Description: Ping6 global variable declarations
 *
 *******************************************************************/

#ifdef _PING6MAIN_C_
t_PING6             gaPing6Table[PING6_MAX_INSTANCES]; /* PING6 Data structure */
tTimerListId        gPing6TimerListId;
INT4                gi4Ping6Socket = -1;
tOsixSemId          gPing6SemId;
tOsixTaskId         gPing6TaskId;
UINT4               gu4Ping6Dbg = 0;
#else
extern t_PING6     gaPing6Table[];
#endif

/*********************** END OF FILE *************************************/
