/*******************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: png6tdfs.h,v 1.3 2010/08/02 06:57:01 prabuc Exp $
 *
 * Description: Ping6 utility header file
 *
 *******************************************************************/
#ifndef   __PING6_TDF_H__
#define   __PING6_TDF_H__

/* Timer Structure used for Ping6 */
typedef struct
{
    tTmrAppTimer  TimerNode;
    UINT1          u1Id;             
    UINT1          u1Align;
    UINT2          u2Align;
} t_PING6_TIMER;

typedef struct
{
    t_PING6_TIMER Timer;
    tIp6Addr     DestAddr;    /* Destination address to which ping is sent */
    tIp6Addr     SrcAddr;     /* Source address to which ping is sent */
    UINT4        u4ContextId; /* The virtual router in which ping is 
                                 initiated */
    UINT4        u4IfIndex;   /* IfIndex on which ping is initiated */
    UINT4        u4Interval;  /* Time interval in seconds between successive 
                                 pings */
    UINT4        u4TimeTaken; /* Time taken for Last Reply */
    INT4         i4Ping6Index; /* Unique Identifier for Ping6Entry */
    INT4         i4RowStatus; /* Row Status of the Ping6 Entry */
    UINT2        u2AvgTime;   /* Average time taken for successful replies */
    UINT2        u2MaxTime;   /* Max delayed pkt so far */
    UINT2        u2MinTime;   /* Time taken by the fastest reply received */
    UINT2        u2TimeOut;   /* Timeout to be used for pinging */
    INT2         i2MaxTries;  /* Number of echo requests to send */
    INT2         i2DataSize;  /* Size of the data to be sent with ping */
    INT2         i2Id;        /* Used internally to match response to request */
    INT2         i2Seq;       /* Sequence to match with thre response */
    UINT2        u2SentCount;  /* No of requests sent already */
    UINT2        u2Successes;  /* Total number of replies received */
    INT2         i2AppId;      /* if the ping is init by SNMP */
    INT1         i1Oper;       /* Indicates if the ping is in progress */
    UINT1        u1Align;
    UINT1        PingData[6];
    UINT1        au1Pad[2];
} t_PING6;

#endif            /*__IP_PING6_H__*/
/* END OF FILE */
