/********************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: ping6.h,v 1.3 2010/08/02 06:57:01 prabuc Exp $
 *
 * Description: Contains common includes used by PING6 submodule.
 *
 *******************************************************************/
#ifndef _PING6_H
#define _PING6_H

PUBLIC INT4
Ping6ApiGetAdminStatus (UINT4 u4PingInstance, INT4 *pi4Status);

PUBLIC INT4   Ping6Lock PROTO ((VOID));
PUBLIC INT4   Ping6UnLock PROTO ((VOID));
#endif /* _PING6_H */
