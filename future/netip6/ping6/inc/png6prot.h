/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: png6prot.h,v 1.4 2015/03/09 13:09:21 siva Exp $
 *
 * Description: Contains common includes used by PING6 submodule.
 *
 *******************************************************************/
#ifndef _PING6_PROTO_H
#define _PING6_PROTO_H

PUBLIC VOID Ping6InitRec PROTO ((INT2 i2Id));

PUBLIC INT4     Ping6VcmIsVRExist PROTO ((UINT4));
PUBLIC INT4     Ping6OpenSocket PROTO ((UINT4 u4ContextId));
PUBLIC INT4     Ping6CloseSocket PROTO ((VOID));
PUBLIC INT4     Ping6GetValidInstanceCount PROTO ((VOID));
PUBLIC INT4     Ping6Send PROTO ((t_PING6 * pPing6));

PUBLIC VOID Ping6EnableDebug PROTO ((UINT4));

#endif /* _PING6_PROTO_H */
