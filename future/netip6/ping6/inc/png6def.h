/*******************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: png6def.h,v 1.4 2011/04/28 14:06:51 siva Exp $
 *
 * Description: Ping utility header file
 *
 *******************************************************************/
#ifndef   __PING6_DEF_H__
#define   __PING6_DEF_H__

#define PING6_RESTART_TIMER(TimerListId, pTmrRef, u4Duration)   \
   {\
      TmrStopTimer((TimerListId), (pTmrRef));   \
      TmrStartTimer((TimerListId), (pTmrRef), \
          SYS_NUM_OF_TIME_UNITS_IN_A_SEC * (u4Duration));   \
   }

#define  PING6_MAX_INSTANCES                10 
#define  PING6_MAX_DST                      PING6_MAX_INSTANCES

#define PING6_SCAN_INSTANCE_TABLE(pPtr, id) \
   for(pPtr=&gaPing6Table[0],id=0;id <PING6_MAX_INSTANCES;pPtr=&gaPing6Table[++id])\

#define   PING6_PKT_ARRIVAL_EVENT      0x00000002
#define   PING6_TASK_NAME              ((UINT1 *)"PNG6")
#define   PING6_TASK_SEM_NAME          ((UINT1 *)"PNG6")

#define   PING6_TMR_EXPIRY_EVENT       0x00000001 
#define   PING6_REPLY_TIMER_ID         1
                                       
#define   PING6_DEF_TIMEOUT            20
#define   PING6_MIN_TIMEOUT            1
#define   PING6_MAX_TIMEOUT            100

#define   PING6_MIN_DATA_SIZE          0
#define   PING6_MAX_DATA_SIZE          2080
#define   PING6_DEF_DATA_SIZE          64

#define   PING6_CONFIG_DATA_SIZE       6
#define   PING6_ICMP6_HDR_SIZE         8

#define   PING6_BUFFER_SIZE            PING6_MAX_DATA_SIZE + PING6_ICMP6_HDR_SIZE

#define   PING6_DEF_TRIES              3
#define   PING6_MIN_TRIES              1
#define   PING6_MAX_TRIES              1000

#define   PING6_OPER_NOT_INITIATED     1
#define   PING6_OPER_IN_PROGRESS       2
#define   PING6_OPER_COMPLETED         3

#define   PING6_MAX_INTERVAL           100
#define   PING6_DEF_INTERVAL           1
#define   PING6_MIN_INTERVAL           1

#define   PING6_INVALID_ID             (-1)
#define   PING6_STATUS_ENABLE          1
#define   PING6_STATUS_DISABLE         2
#define   PING6_STATUS_INVALID         3
#define   PING6_STATUS_CREATE          4

#define   PING6_DATA                   0xA5
#define   PING6_MAX_DATA_VALUE         65535

#define   SNMP_PING6_APP_ID            1
#define   CLI_PING6_APP_ID             2

#define   PING6_MAX_SEM_NAME_LEN       4

#ifdef TRACE_WANTED
# define PING6_NAME                    "PING6"
# define PING6_DBG_FLAG                gu4Ping6Dbg

#define PING6_TRC_ARG(Val, Fmt)  \
   if((PING6_DBG_FLAG & PING6_MOD_TRC) == PING6_MOD_TRC) {\
    UtlTrcLog (PING6_DBG_FLAG, Val, PING6_NAME, Fmt); }
#else
#define PING6_TRC_ARG(Val, Fmt) 
#endif

#endif            /* __PING6_DEF_H__*/
