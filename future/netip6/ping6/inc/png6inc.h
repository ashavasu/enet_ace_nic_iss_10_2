/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: png6inc.h,v 1.6 2015/03/09 13:09:21 siva Exp $
 *
 * Description: Contains common includes used by PING6 submodule.
 *
 *******************************************************************/
#ifndef __PING6_INC_H__
#define __PING6_INC_H__
#include "lr.h"
#include "cfa.h"
#include "ipv6.h"
#if !(__GLIBC__ >= 2 && __GLIBC_MINOR__ >= 9)
#include <netinet/icmp6.h>
#endif
#include "fssocket.h"
#include "vcm.h"
#include "ip6util.h"
#include "trieinc.h"
#include "rtm6.h"

#include "ping6.h"
#include "png6tdfs.h"
#include "png6def.h"
#include "png6prot.h"
#include "png6glob.h"
#if defined(VRF_WANTED) && defined(LINUX_310_WANTED)
#include "lnxip.h"
#endif
#endif /*__PING6_INC_H__ */
