/********************************************************************
 * Description:
 *          The Ping6 software is built over ICMP ECHO REQUEST and            
 *          ECHO RESPONSE messages. The network administrator can 'Ping6 '    
 *          a remote host by invoking ping routines through network          
 *          management protocols.  Also the software allows for arbitory     
 *          message lengths and the time intervals.                          
 *           
 *          Implementation here is specific to fsping.mib. 
 *          In this implementation,Multiple pings (even to same destination)
 *          can be triggered simultaneously.This is achevied by having Ping6 
 *          Instance Id as a unique Identifier.
 *                                                                           
 *          The ping is triggered setting the RowStatus status active
 *          with appropriate destination IP Address in Ping6 Instance record.
 *          For each timeout the ping timeout handler sends the     
 *          echo request.  The current statistics about ping can be          
 *          obtained using Network Management variables.                    
 *    
 *   $Id: png6main.c,v 1.11 2015/03/09 13:09:21 siva Exp $
 * 
 ********************************************************************/

#ifndef _PING6MAIN_C_
#define _PING6MAIN_C_
#include "png6inc.h"

PRIVATE INT4 Ping6Init PROTO ((VOID));
//PRIVATE INT4 Ping6GetSrcAddr PROTO ((tIp6Addr * pDstAddr, tIp6Addr * pSrcAddr));
PRIVATE VOID Ping6HandleTimerExpiry PROTO ((VOID));
PRIVATE INT4 Ping6ReplyHandler PROTO ((VOID));

/*****************************************************************************
 * Function     : Ping6TaskMain
 *
 * Description  : This is main task Ping6 Module.It continually waits for
 *                the events.When event arrives, it calls different
 *                functions to handle the events
 *
 * Input        : Task Name
 * 
 * Output       : None
 *
 * Returns      : None
 *****************************************************************************/
PUBLIC VOID
Ping6TaskMain (INT1 *pi1TaskParam)
{
    UINT4               u4EventMask = 0;

    UNUSED_PARAM (pi1TaskParam);

    if (OsixTskIdSelf (&gPing6TaskId) == OSIX_FAILURE)
    {
        PING6_TRC_ARG (ALL_FAILURE_TRC, "Ping6 Task initialization failed\n");
        lrInitComplete (OSIX_FAILURE);
        return;
    }
    /* Initializes Ping6 DataStructure */
    if (Ping6Init () == OSIX_FAILURE)
    {
        PING6_TRC_ARG (ALL_FAILURE_TRC, "Ping6 Init failed\n");
        lrInitComplete (OSIX_FAILURE);
        return;
    }

    /* Indicate Task Initialization Success */
    lrInitComplete (OSIX_SUCCESS);

    while (1)
    {
        OsixEvtRecv (gPing6TaskId,
                     PING6_PKT_ARRIVAL_EVENT | PING6_TMR_EXPIRY_EVENT,
                     OSIX_WAIT | OSIX_EV_ANY, &u4EventMask);

        /* Process the Packet recevied on ICMP Socket */
        if (u4EventMask & PING6_PKT_ARRIVAL_EVENT)
        {
            Ping6Lock ();
            Ping6ReplyHandler ();
            Ping6UnLock ();
        }

        /* Handle Retry Timer Expiry */
        if (u4EventMask & PING6_TMR_EXPIRY_EVENT)
        {
            Ping6Lock ();
            Ping6HandleTimerExpiry ();
            Ping6UnLock ();
        }
    }
}

/*****************************************************************************
 * Function     : Ping6InitRec                      
 *                                                  
 * Description  : Initializes a Ping6 Entry         
 *                                                  
 * Input        : pPing6 - Pointer to a Ping6 Entry 
 *                                                  
 * Output       : None                              
 *                                                  
 * Returns      : None                              
 *                                                  
 *****************************************************************************/
PUBLIC VOID
Ping6InitRec (INT2 i2Id)
{
    t_PING6            *pPing6 = NULL;

    pPing6 = &gaPing6Table[i2Id];
    /* Initialize all the variables to default state */
    MEMSET (&(pPing6->Timer), 0, sizeof (t_PING6_TIMER));
    pPing6->Timer.u1Id = PING6_REPLY_TIMER_ID;

    MEMSET (&(pPing6->DestAddr), 0, sizeof (tIp6Addr));
    pPing6->i2Id = i2Id;
    pPing6->u4ContextId = VCM_DEFAULT_CONTEXT;
    pPing6->u4IfIndex = 0;
    pPing6->u2TimeOut = PING6_DEF_TIMEOUT;
    pPing6->i2MaxTries = PING6_DEF_TRIES;
    pPing6->i2DataSize = PING6_DEF_DATA_SIZE;
    pPing6->i4RowStatus = PING6_STATUS_INVALID;
    pPing6->i1Oper = PING6_OPER_NOT_INITIATED;
    pPing6->u2SentCount = 0;
    pPing6->u2AvgTime = 0;
    pPing6->u2MaxTime = 0;
    pPing6->u2MinTime = 0;
    pPing6->u2Successes = 0;
    pPing6->i4Ping6Index = PING6_INVALID_ID;
    pPing6->i2Seq = 0;
    pPing6->i2AppId = 0;
}

/*****************************************************************************
 * Function     : Ping6OpenSocket                                     
 *                                                                    
 * Description  : Open and Initializes the Socket used by Ping6 Module
 *                                                                    
 * Input        : None                                                
 *                                                                    
 * Output       : None                                                
 *                                                                    
 * Returns      : OSIX_SUCCESS/OSIX_FAILURE                           
 *                                                                    
 *****************************************************************************/
INT4
Ping6OpenSocket (UINT4 u4ContextId)
{
    struct icmp6_filter Filter;
    INT4                i4RetVal = 0;
    INT4                i4Value = 0;

#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    tLnxVrfEventInfo  LnxVrfEventInfo;
#else
    UNUSED_PARAM(u4ContextId);
#endif

    MEMSET (&Filter, 0, sizeof (struct icmp6_filter));

    PING6_TRC_ARG (MGMT_TRC, "Ping6OpenSocket: Opening a new socket for "
                   "ping session\n");
    /* Open the RawSocket for sending EchoRequests and 
       Receving EchoReplies */
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)

    LnxVrfEventInfo.u4ContextId = u4ContextId;
    LnxVrfEventInfo.i4Sockdomain = AF_INET6;
    LnxVrfEventInfo.i4SockType=SOCK_RAW;
    LnxVrfEventInfo.i4SockProto = IPPROTO_ICMPV6;
    LnxVrfEventInfo.u1MsgType = LNX_VRF_OPEN_SOCKET;
    LnxVrfSockLock();
    LnxVrfEventHandling(&LnxVrfEventInfo,&gi4Ping6Socket);
    LnxVrfSockUnLock();
#else
    gi4Ping6Socket = socket (AF_INET6, SOCK_RAW, IPPROTO_ICMPV6);
#endif

    if (gi4Ping6Socket <= 0)
    {
        return OSIX_FAILURE;
    }

    i4Value = 2;

    i4RetVal = setsockopt (gi4Ping6Socket, SOL_RAW, IPV6_CHECKSUM, &i4Value,
                           sizeof (INT4));
    if (i4RetVal < 0)
    {
        Ping6CloseSocket ();
        PING6_TRC_ARG (ALL_FAILURE_TRC,
                       "Ping6OpenSocket: IPV6_CHECKSUM failed\n");
        return OSIX_FAILURE;
    }

    i4Value = 1;
    setsockopt (gi4Ping6Socket, SOL_IPV6, IPV6_RECVERR, (char *) &i4Value,
                sizeof (i4Value));

    if (i4RetVal < 0)
    {
        Ping6CloseSocket ();
        PING6_TRC_ARG (ALL_FAILURE_TRC,
                       "Ping6OpenSocket: IPV6_RECVERR failed\n");
        return OSIX_FAILURE;
    }

    i4Value = 1;
    setsockopt (gi4Ping6Socket, IPPROTO_IPV6, IPV6_RECVHOPLIMIT,
                &i4Value, sizeof (i4Value));

    if (i4RetVal < 0)
    {
        Ping6CloseSocket ();
        PING6_TRC_ARG (ALL_FAILURE_TRC, "Ping6OpenSocket: "
                       "IPV6_RECVHOPLIMIT failed\n");
        return OSIX_FAILURE;
    }

    /* Select icmp echo reply as icmp type to receive */

    ICMP6_FILTER_SETBLOCKALL (&Filter);
    ICMP6_FILTER_SETPASS (ICMP6_ECHO_REPLY, &Filter);
    ICMP6_FILTER_SETPASS (ICMP6_DST_UNREACH, &Filter);
    ICMP6_FILTER_SETPASS (ICMP6_PACKET_TOO_BIG, &Filter);
    ICMP6_FILTER_SETPASS (ICMP6_TIME_EXCEEDED, &Filter);
    ICMP6_FILTER_SETPASS (ICMP6_PARAM_PROB, &Filter);
    ICMP6_FILTER_SETPASS (ICMP6_ECHO_REPLY, &Filter);
    ICMP6_FILTER_SETPASS (ND_REDIRECT, &Filter);

    i4RetVal = setsockopt (gi4Ping6Socket, IPPROTO_ICMPV6, ICMP6_FILTER,
                           &Filter, sizeof (struct icmp6_filter));

    if (i4RetVal < 0)
    {
        Ping6CloseSocket ();
        PING6_TRC_ARG (ALL_FAILURE_TRC,
                       "Ping6OpenSocket: ICMP6_FILTER failed\n");
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function     : Ping6CloseSocket                                       
 *                                                                       
 * Description  : Closes the Raw Socket used by Ping6 Module.When no ping
 *                instance exists,the socket is closed                   
 *                                                                       
 * Input        : None                                                   
 *                                                                       
 * Output       : None                                                   
 *                                                                       
 * Returns      : OSIX_SUCCESS/OSIX_FAILURE                              
 *                                                                       
 *****************************************************************************/
INT4
Ping6CloseSocket (VOID)
{
    INT4                i4RetVal = 0;

    /* Close the RAW Socket Opened */
    if (gi4Ping6Socket != -1)
    {
        i4RetVal = close (gi4Ping6Socket);

        if (i4RetVal < 0)
        {
            PING6_TRC_ARG (ALL_FAILURE_TRC,
                           "Ping6CloseSocket: Socket close failed\n");
            return (OSIX_FAILURE);
        }
    }

    gi4Ping6Socket = -1;
    PING6_TRC_ARG (MGMT_TRC, "Ping6CloseSocket: Closed the ping socket\n");
    return (OSIX_SUCCESS);
}

/*****************************************************************************
 * Function     : Ping6Send                                        
 *                                                                 
 * Description  : Sends Ping6 data based on Ping6 Entry Information
 *                                                                 
 * Input        : Ping6 Entry                                      
 *                                                                 
 * Output       : None                                             
 *                                                                 
 * Returns      : OSIX_SUCCESS/OSIX_FAILURE                        
 *                                                                 
 *****************************************************************************/
INT4
Ping6Send (t_PING6 * pPing6)
{
    struct sockaddr_in6 ToDest;
    struct in6_pktinfo *pIpPktInfo = NULL;
    struct msghdr       Ip6MsgHdr;
    struct icmp6_hdr   *pPing6IcmpHdr;
    struct pollfd       pset;
    tNetIpv6IfInfo      NetIpIfInfo;
    tNetIpv6RtInfo      NetIp6RtInfo;
    tNetIpv6RtInfoQueryMsg RtQuery;
    tIp6Addr            SrcAddress;
    tIp6Addr            IfSrcAddr;
    INT1                ai1SendBuff[PING6_BUFFER_SIZE];
    INT1               *pi1SendBuff = NULL;
    UINT4               u4IfIndex = 0;
    UINT1               u1Ping6Data = 0;
    UINT1               au1CmsgInfo[112];
    struct iovec        Iov;
    struct cmsghdr     *pCmsgInfo;
    INT4                i4RetVal = 0;
    UINT4               u4Count = 0;

    MEMSET (ai1SendBuff, 0, sizeof (ai1SendBuff));
    MEMSET (&SrcAddress, 0, sizeof (tIp6Addr));
    MEMSET (&IfSrcAddr, 0, sizeof (tIp6Addr));

    PING6_TRC_ARG (MGMT_TRC, "Ping6Send: Sending a ping packet\n");
    /* For Self Ping6, when the interface is down, return failure.
       This is applicable for both FSIP and LNXIP */
    if ((NetIpv6IsOurAddressInCxt (pPing6->u4ContextId, &(pPing6->DestAddr),
                                   &u4IfIndex) == NETIPV6_SUCCESS))
    {
        MEMSET (&NetIpIfInfo, 0, sizeof (tNetIpv6IfInfo));

        if (NetIpv6GetIfInfo (u4IfIndex, &NetIpIfInfo) != NETIPV6_SUCCESS)
        {
            pPing6->i1Oper = PING6_OPER_NOT_INITIATED;
            PING6_TRC_ARG (ALL_FAILURE_TRC, "Ping6Send: Cannot obtain the "
                           "interface info\n");
            return OSIX_FAILURE;
        }
        if ((NetIpIfInfo.u4Admin == NETIPV6_ADMIN_DOWN) ||
            (NetIpIfInfo.u4Oper == NETIPV6_OPER_DOWN))
        {
            pPing6->i1Oper = PING6_OPER_NOT_INITIATED;
            PING6_TRC_ARG (ALL_FAILURE_TRC, "Ping6Send: Interface status is"
                           "disabled\n");
            return OSIX_FAILURE;
        }
    }

    if ((pPing6->SrcAddr.u4_addr[0] |
         pPing6->SrcAddr.u4_addr[1] |
         pPing6->SrcAddr.u4_addr[2] | pPing6->SrcAddr.u4_addr[3]) == 0)
    {
        if (NetIpv6GetSrcAddrForDestAddr
            (pPing6->u4ContextId, &pPing6->DestAddr,
             &pPing6->SrcAddr) == NETIPV6_FAILURE)
        {
            return OSIX_FAILURE;
        }
    }
    /* If the Ping6 Instance created is the first one, Open the Socket
       for sending Ping6 Request and receving Ping6 replies */
    if (Ping6GetValidInstanceCount () == 0)
    {
        if (Ping6OpenSocket (pPing6->u4ContextId) == OSIX_FAILURE)
        {
            pPing6->i1Oper = PING6_OPER_NOT_INITIATED;
            return OSIX_FAILURE;
        }
    }

    /* Update the Status of the Ping6 - inprogress/completed */
    if (pPing6->u2SentCount < pPing6->i2MaxTries)
    {
        if (pPing6->u2SentCount == 0)
        {
            pPing6->i1Oper = PING6_OPER_IN_PROGRESS;
        }

        /* Incase of Linux IP, timer is used only to calculate the time
         * taken for Ping response */
        PING6_RESTART_TIMER (gPing6TimerListId, &(pPing6->Timer.TimerNode),
                             pPing6->u2TimeOut);
    }
    else
    {
        TmrStopTimer (gPing6TimerListId, &(pPing6->Timer.TimerNode));
        pPing6->i1Oper = PING6_OPER_COMPLETED;
        return OSIX_SUCCESS;
    }

    /* Fill Destination Information needed for Socket to send Packet */
    MEMSET (&ToDest, 0, sizeof (ToDest));
    ToDest.sin6_family = AF_INET6;
    ToDest.sin6_port = 0;
    MEMCPY (&(ToDest.sin6_addr), &(pPing6->DestAddr),
            sizeof (ToDest.sin6_addr));

    pi1SendBuff = ai1SendBuff;

    /*Fill the ICMP Header Information */
    pPing6IcmpHdr = (struct icmp6_hdr *) (VOID *) pi1SendBuff;
    pPing6IcmpHdr->icmp6_type = ICMP6_ECHO_REQUEST;
    pPing6IcmpHdr->icmp6_code = 0;
    pPing6IcmpHdr->icmp6_id = pPing6->i2Id;
    pPing6IcmpHdr->icmp6_cksum = 0;
    pPing6->i2Seq++;
    pPing6IcmpHdr->icmp6_seq = OSIX_HTONS (pPing6->i2Seq);

    /* Fill the Ping6 Data in the Buffer */
    u1Ping6Data = PING6_DATA;
    MEMSET (pi1SendBuff + sizeof (struct icmp6_hdr), u1Ping6Data,
            pPing6->i2DataSize);

    MEMSET (&(au1CmsgInfo), 0, sizeof (au1CmsgInfo));
    MEMSET (&(ToDest), 0, sizeof (struct sockaddr_in6));
    ToDest.sin6_family = AF_INET6;
    MEMCPY (&(ToDest.sin6_addr), &(pPing6->DestAddr), sizeof (tIp6Addr));

    Iov.iov_base = (VOID *) pi1SendBuff;
    Iov.iov_len = sizeof (struct icmp6_hdr) + pPing6->i2DataSize;

    Ip6MsgHdr.msg_name = ((void *) &ToDest);
    Ip6MsgHdr.msg_namelen = sizeof (struct sockaddr_in6);
    Ip6MsgHdr.msg_iov = (void *) &Iov;
    Ip6MsgHdr.msg_iovlen = 1;
    Ip6MsgHdr.msg_control = (void *) &au1CmsgInfo;
    Ip6MsgHdr.msg_controllen = CMSG_SPACE (sizeof (struct in6_pktinfo));

    pCmsgInfo = CMSG_FIRSTHDR (&Ip6MsgHdr);
    pCmsgInfo->cmsg_level = IPPROTO_IPV6;
    pCmsgInfo->cmsg_type = IPV6_PKTINFO;
    pCmsgInfo->cmsg_len = CMSG_LEN (sizeof (struct in6_pktinfo));

    pIpPktInfo = (struct in6_pktinfo *) (VOID *) CMSG_DATA (pCmsgInfo);
    MEMCPY (&(pIpPktInfo->ipi6_addr), &SrcAddress, sizeof (tIp6Addr));

    if (pPing6->u4IfIndex == 0)
    {
        MEMSET (&NetIp6RtInfo, 0, sizeof (tNetIpv6RtInfo));
        MEMSET (&RtQuery, 0, sizeof (tNetIpv6RtInfoQueryMsg));

#if defined(VRF_WANTED) && defined(LINUX_310_WANTED)
        RtQuery.u4ContextId = pPing6->u4ContextId;
#else
        RtQuery.u4ContextId = VCM_DEFAULT_CONTEXT;
#endif

        RtQuery.u1QueryFlag = RTM6_QUERIED_FOR_NEXT_HOP;
        Ip6AddrCopy (&NetIp6RtInfo.Ip6Dst, &pPing6->DestAddr);
        NetIp6RtInfo.u1Prefixlen = IP6_ADDR_MAX_PREFIX;
        if (NetIpv6GetRoute (&RtQuery, &NetIp6RtInfo) == NETIPV6_FAILURE)
        {
            TmrStopTimer (gPing6TimerListId, &(pPing6->Timer.TimerNode));
            pPing6->i1Oper = PING6_OPER_COMPLETED;
            return OSIX_FAILURE;
        }
        pPing6->u4IfIndex = NetIp6RtInfo.u4Index;
    }
    pIpPktInfo->ipi6_ifindex = CfaGetIfIpPort ((UINT2) pPing6->u4IfIndex);

    i4RetVal = sendmsg (gi4Ping6Socket, &Ip6MsgHdr, 0);

    if (i4RetVal < 0)
    {
        TmrStopTimer (gPing6TimerListId, &(pPing6->Timer.TimerNode));

        pPing6->i1Oper = PING6_OPER_COMPLETED;
        perror("send failed\n");
        PING6_TRC_ARG (ALL_FAILURE_TRC, "Ping6Send: sendmsg failed\n");

        return OSIX_FAILURE;
    }

    pPing6->u2SentCount++;
    PING6_TRC_ARG (MGMT_TRC, "Ping6Send: Sent ICMPv6 Echo request\n");

    /* Poll till reply is received or timeout */
    MEMSET (&pset, 0, sizeof (struct pollfd));
    pset.fd = gi4Ping6Socket;
    pset.events = POLLIN | POLLERR;
    pset.revents = 0;
    for (;;)
    {
        if (poll (&pset, 1, 1) < 1 ||    /* 1ms */
            !(pset.revents & (POLLIN | POLLERR)))
        {
            u4Count++;
            if (u4Count >= (pPing6->u2TimeOut * SYS_NUM_OF_TIME_UNITS_IN_A_SEC))
            {
                TmrStopTimer (gPing6TimerListId, &(pPing6->Timer.TimerNode));
                if (pPing6->u2SentCount == pPing6->i2MaxTries)
                {
                    pPing6->i1Oper = PING6_OPER_COMPLETED;
                }
                return OSIX_FAILURE;
            }
            continue;
        }
        else
        {
            break;
        }
    }

    if (Ping6ReplyHandler () == OSIX_FAILURE)
    {
        TmrStopTimer (gPing6TimerListId, &(pPing6->Timer.TimerNode));
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function     : Ping6GetValidInstanceCount                               
 *                                                                         
 * Description  : Provides the count of active Ping6 Instances.(i.e OnGoing
 *                Ping6)                                                   
 *                                                                         
 * Input        : None                                                     
 *                                                                         
 * Output       : None                                                     
 *                                                                         
 * Returns      : Returns Ping6Instance Count                              
 *                                                                         
 *****************************************************************************/
PUBLIC INT4
Ping6GetValidInstanceCount (VOID)
{
    t_PING6            *pPing6 = NULL;
    INT4                i4Ping6InstCount = 0;
    INT2                i2Id = 0;

    PING6_SCAN_INSTANCE_TABLE (pPing6, i2Id)
    {
        if (pPing6->i1Oper == PING6_OPER_IN_PROGRESS)
        {
            i4Ping6InstCount++;
        }
    }
    return i4Ping6InstCount;
}

/*****************************************************************************
 * Function     : Ping6Lock                      
 *                                               
 * Description  : Takes the Ping6 Task Semaphore 
 *                                               
 * Input        : None                           
 *                                               
 * Output       : None                           
 *                                               
 * Returns      : SNMP_SUCCESS/SNMP_FAILURE      
 *                                               
 *****************************************************************************/
PUBLIC INT4
Ping6Lock (VOID)
{
    if (OsixSemTake (gPing6SemId) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/*****************************************************************************
 * Function     : Ping6UnLock                            
 *                                                       
 * Description  : Releases the Ping6Task Semaphore taken 
 *                                                       
 * Input        : None                                   
 *                                                       
 * Output       : None                                   
 *                                                       
 * Returns      : SNMP_SUCCESS/ SNMP_FAILURE             
 *                                                       
 *****************************************************************************/
PUBLIC INT4
Ping6UnLock (VOID)
{
    OsixSemGive (gPing6SemId);
    return SNMP_SUCCESS;
}

/*****************************************************************************
 * Function     : Ping6VcmIsVRExist                                       
 *                                                                        
 * Description  : This function determines if the given context is a valid
 *                 context created in the router.                         
 *                                                                        
 * Input        : u4ContextId - The context Id                            
 *                                                                        
 * Output       : None                                                    
 *                                                                        
 * Returns      : OSIX_SUCCESS/ OSIX_FAILURE                              
 *                                                                        
 *****************************************************************************/

INT4
Ping6VcmIsVRExist (UINT4 u4ContextId)
{
    if (VcmIsVcExist (u4ContextId) == VCM_FALSE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function     : Ping6EnableDebug     
*                                            
* Description  : This function configures the Ping6 trace flag 
*
* Input        : u4TrcValue  - The configured trace value.
*
* Output       : None                         
*
* Returns      : None                      
*
***************************************************************************/
PUBLIC VOID
Ping6EnableDebug (UINT4 u4TrcValue)
{
    Ping6Lock ();
    gu4Ping6Dbg = u4TrcValue;
    Ping6UnLock ();
    return;
}

/*****************************************************************************
 * Function     : Ping6Init
 *
 * Description  : Initializes the Data Structures by Ping6 Module
 *                     
 * Input        : None 
 *                     
 * Output       : None 
 *                     
 * Returns      : None 
 *****************************************************************************/
PRIVATE INT4
Ping6Init (VOID)
{
    t_PING6            *pPing6 = NULL;
    INT2                i2Id = 0;

    /* Create a TimerList */
    if (TmrCreateTimerList (PING6_TASK_NAME,
                            PING6_TMR_EXPIRY_EVENT,
                            NULL, &gPing6TimerListId) != TMR_SUCCESS)
    {
        PING6_TRC_ARG (ALL_FAILURE_TRC,
                       "Ping6Init: Timer List creation failed\n");
        return OSIX_FAILURE;
    }

    /* Create the Semaphore for Ping6Task */
    if ((OsixSemCrt (PING6_TASK_SEM_NAME, &gPing6SemId)) != OSIX_SUCCESS)
    {
        TmrDeleteTimerList (gPing6TimerListId);
        PING6_TRC_ARG (ALL_FAILURE_TRC,
                       "Ping6Init: Semaphore creation failed\n");
        return OSIX_FAILURE;
    }

    OsixSemGive (gPing6SemId);

    /* Initialize Ping6 Instances */
    PING6_SCAN_INSTANCE_TABLE (pPing6, i2Id)
    {
        Ping6InitRec (i2Id);
    }
    UNUSED_PARAM (pPing6);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                       
 * Function     : Ping6GetSrcAddr                                        
 *                                                                       
 * Description  : The function return the Source Ip Addres of interface  
 *                                                                       
 * Input        : u4IfIndex  -   Interface Id                            
 *                pDstAddr - Destination IP address                      
 *                pSrcAddr - Source IP Address                           
 *                                                                       
 * Output       : IP Address                                             
 *                                                                       
 * Returns      : None                                                   
 *                                                                       
 *****************************************************************************/
/* PRIVATE INT4
Ping6GetSrcAddr (tIp6Addr * pDstAddr, tIp6Addr * pSrcAddr)
{
    tNetIpv6AddrInfo    FirstAddrInfo;
    tNetIpv6AddrInfo    NextAddrInfo;
    tIp6Addr           *pAddr = NULL;
    tIp6Addr           *pStoredAddr = NULL;
    INT4                i4Status = 0;
    INT4                i4Count = 0;

    for (i4Count = BRG_MAX_PHY_PLUS_LOG_PORTS + 1;
         i4Count <= BRG_MAX_PHY_PLUS_LOG_PORTS + IPIF_MAX_LOGICAL_IFACES;
         i4Count++)
    {
        i4Status = NetIpv6GetFirstIfAddr (i4Count, &FirstAddrInfo);

        if (i4Status == NETIPV6_FAILURE)
        {
            continue;
        }

        do
        {
            if (Ip6AddrMatch (&FirstAddrInfo.Ip6Addr, pDstAddr,
                              FirstAddrInfo.u4PrefixLength) == TRUE)
            {
                pStoredAddr = &FirstAddrInfo.Ip6Addr;
                MEMCPY (pSrcAddr, pStoredAddr, sizeof (tIp6Addr));
                return OSIX_SUCCESS;
            }
            else
            {
                pAddr = &FirstAddrInfo.Ip6Addr;
            }

            i4Status = NetIpv6GetNextIfAddr (i4Count, &FirstAddrInfo,
                                             &NextAddrInfo);

            if (i4Status == NETIPV6_FAILURE)
            {
                break;
            }

            MEMCPY (&FirstAddrInfo, &NextAddrInfo, sizeof (tNetIpv6AddrInfo));
        }
        while ((&NextAddrInfo != NULL) || (i4Status != NETIPV6_FAILURE));
    }

    return OSIX_FAILURE;
}

*/

/*****************************************************************************
 * Function     : Ping6HandleTimerExpiry                                   
 *                                                                         
 * Description  : Routine that Handle Ping6 Timer Expiry.If the trigger is 
 *                through SNMP,Call Sending Ping6.Or Release sem and let   
 *                application send further EchoRequests                    
 *                                                                         
 * Input        : pPing6 - Pointer to a Ping6 Entry                        
 *                                                                         
 * Output       : None                                                     
 *                                                                         
 * Returns      : None                                                     
 *****************************************************************************/
PRIVATE VOID
Ping6HandleTimerExpiry (VOID)
{
    tTmrAppTimer       *pTimer = NULL;
    t_PING6            *pPing6 = NULL;

    TmrGetExpiredTimers (gPing6TimerListId, &pTimer);

    PING6_TRC_ARG (MGMT_TRC, "Ping6HandleTimerExpiry: Ping6 Timer expired\n");
    while (pTimer != NULL)
    {
        pPing6 = (t_PING6 *) pTimer;
        if (pPing6->Timer.u1Id == PING6_REPLY_TIMER_ID)
        {
            if (pPing6->i2AppId == SNMP_PING6_APP_ID)
            {
                /* Ping6 is init from SNMP, continue with Retry */
                Ping6Send (pPing6);
            }
            else
            {
                if (pPing6->u2SentCount == pPing6->i2MaxTries)
                {
                    pPing6->i1Oper = PING6_OPER_COMPLETED;
                }
            }
        }
        TmrGetExpiredTimers (gPing6TimerListId, &pTimer);
    }

    /* If no valid Ping6 Instancee exists, Close the Socket Opened
       for sending Ping6 Request and receving Ping6 replies */
    if (Ping6GetValidInstanceCount () == 0)
    {
        Ping6CloseSocket ();
    }
}

/*****************************************************************************
 * Function     : Ping6ReplyHandler                                       
 *                                                                        
 * Description  : Reads the Packet from Socket and then                   
 *               it searches the ping table for the match. If we have an  
 *               entry for the host and if we are waiting for the response
 *               then the statistics is updated                           
 *                                                                        
 * Input        : Ping6 Entry                                             
 *                                                                        
 * Output       : None                                                    
 *                                                                        
 * Returns      : OSIX_SUCCESS/OSIX_FAILURE                               
 *                                                                        
 *****************************************************************************/
PRIVATE INT4
Ping6ReplyHandler (VOID)
{
    t_PING6            *pPing6 = NULL;
    struct icmp6_hdr   *pIcmp6Hdr = NULL;
    struct sockaddr_in6 PeerAddr;
    INT1                ai1RecvBuff[PING6_BUFFER_SIZE];
    INT1               *pi1RecvBuff = NULL;
    INT4                i4DataLen = 0;
    UINT4               u4RemainingTime = 0;
    struct cmsghdr     *pCmsgInfo;
    struct iovec        Iov;
    struct msghdr       Ip6MsgHdr;
    UINT1               au1Cmsg[IP6_DEFAULT_MTU];    /* Max PDU size */
    tNetIpv6RtInfo      NetIp6RtInfo;
    tNetIpv6RtInfo      TmpNetIp6RtInfo;
    tNetIpv6RtInfoQueryMsg RtQuery;
    struct nd_redirect *pNdRedirectHdr = NULL;
    tLip6If            *pIf6 = NULL;

    MEMSET (&NetIp6RtInfo, 0, sizeof (tNetIpv6RtInfo));
    MEMSET (&TmpNetIp6RtInfo, 0, sizeof (tNetIpv6RtInfo));
    MEMSET (&RtQuery, 0, sizeof (tNetIpv6RtInfoQueryMsg));

    MEMSET (&PeerAddr, 0, sizeof (PeerAddr));
    PeerAddr.sin6_family = AF_INET6;
    PeerAddr.sin6_port = 0;

    MEMSET (ai1RecvBuff, 0, sizeof (PING6_BUFFER_SIZE));
    MEMSET (au1Cmsg, 0, sizeof (au1Cmsg));

    pi1RecvBuff = ai1RecvBuff;
    Ip6MsgHdr.msg_name = (void *) &PeerAddr;
    Ip6MsgHdr.msg_namelen = sizeof (PeerAddr);
    Iov.iov_base = pi1RecvBuff;
    Iov.iov_len = PING6_BUFFER_SIZE;
    Ip6MsgHdr.msg_iov = &Iov;
    Ip6MsgHdr.msg_iovlen = 1;
    Ip6MsgHdr.msg_control = (VOID *) &au1Cmsg;
    Ip6MsgHdr.msg_controllen = IP6_DEFAULT_MTU;

    pCmsgInfo = CMSG_FIRSTHDR (&Ip6MsgHdr);
    pCmsgInfo->cmsg_level = SOL_IP;
    pCmsgInfo->cmsg_type = IPV6_PKTINFO;
    pCmsgInfo->cmsg_len = sizeof (au1Cmsg);

    i4DataLen = recvmsg (gi4Ping6Socket, &Ip6MsgHdr, 0);

    if (i4DataLen < 0)
    {
        PING6_TRC_ARG (ALL_FAILURE_TRC, "Ping6ReplyHandler :recvmsg error\r\n");
        return OSIX_FAILURE;
    }

    PING6_TRC_ARG (MGMT_TRC, "Ping6ReplyHandler: Received a ping packet \n");

    pIcmp6Hdr = (struct icmp6_hdr *) (VOID *) (pi1RecvBuff);

    pNdRedirectHdr = (struct nd_redirect *) (VOID *) (pi1RecvBuff);
    if (pIcmp6Hdr->icmp6_id < PING6_MAX_INSTANCES)
    {
        pPing6 = &gaPing6Table[pIcmp6Hdr->icmp6_id];
    }

    if (pPing6 == NULL)
    {
        return OSIX_FAILURE;
    }

    /* ICMP6 Redirect Enable/Disable */
    pIf6 = Lip6UtlGetIfEntry (pPing6->u4IfIndex);

    if (pIf6 == NULL)
    {
        return OSIX_FAILURE;
    }

    if (pIf6->Icmp6ErrRLInfo.i4Icmp6RedirectMsg == ICMP6_REDIRECT_ENABLE)
    {

        /*Code to handle ICMPv6 Redirect messages */
        if (pIcmp6Hdr->icmp6_type == ND_REDIRECT)
        {
#if defined(VRF_WANTED) && defined(LINUX_310_WANTED)
        RtQuery.u4ContextId = pPing6->u4ContextId;
#else
        RtQuery.u4ContextId = VCM_DEFAULT_CONTEXT;
#endif

            RtQuery.u1QueryFlag = RTM6_QUERIED_FOR_NEXT_HOP;
            MEMCPY (&(NetIp6RtInfo.Ip6Dst), &pNdRedirectHdr->nd_rd_dst,
                    sizeof (tIp6Addr));
            NetIp6RtInfo.u1Prefixlen = IP6_ADDR_MAX_PREFIX;
            if (NetIpv6GetRoute (&RtQuery, &NetIp6RtInfo) == NETIPV6_SUCCESS)
            {
                /*Copy the entry to the temporary buffer */
                MEMCPY (&TmpNetIp6RtInfo, &NetIp6RtInfo,
                        sizeof (tNetIpv6RtInfo));
                MEMCPY (&(TmpNetIp6RtInfo.NextHop),
                        &pNdRedirectHdr->nd_rd_target, sizeof (tIp6Addr));
                if (!(IS_ADDR_LLOCAL (TmpNetIp6RtInfo.NextHop)))
                {
                    PING6_TRC_ARG (MGMT_TRC,
                                   "Ping6ReplyHandler: Target MUST be Link-Local Address \n");
                    return OSIX_FAILURE;
                }
                if (Ip6AddrCompare
                    (NetIp6RtInfo.NextHop, TmpNetIp6RtInfo.NextHop) != IP6_ZERO)
                {
                    /*Delete the entry from the route table */
                    NetIpv6LeakRoute (NETIPV6_DELETE_ROUTE, &NetIp6RtInfo);

                    /* Add the updated entry */
                    NetIpv6LeakRoute (NETIPV6_ADD_ROUTE, &TmpNetIp6RtInfo);
                }
            }
            else
            {
                return OSIX_FAILURE;
            }

        }
    }

    /* Only Echo Replies should be considered */
    if (pIcmp6Hdr->icmp6_type == ICMP6_ECHO_REPLY)
    {
        /* Do a LookUp whether the reply is for the Ping6 sent by Us */
        if (pIcmp6Hdr->icmp6_id < PING6_MAX_INSTANCES)
        {
            pPing6 = &gaPing6Table[pIcmp6Hdr->icmp6_id];
        }

        if (pPing6 == NULL)
        {
            return OSIX_FAILURE;
        }
        /* If Valid Id,then update the Statistics */
        if (pIcmp6Hdr->icmp6_id == pPing6->i2Id)
        {
            TmrGetRemainingTime (gPing6TimerListId,
                                 &(pPing6->Timer.TimerNode), &u4RemainingTime);

            /* Calculate time taken  for Ping6Reply */
            pPing6->u4TimeTaken = (pPing6->u2TimeOut *
                                   SYS_NUM_OF_TIME_UNITS_IN_A_SEC) -
                u4RemainingTime;

            /* Time taken for Reply in msec */
            pPing6->u4TimeTaken =
                (pPing6->u4TimeTaken * 1000) / SYS_NUM_OF_TIME_UNITS_IN_A_SEC;

            if (pPing6->u2Successes == 0)
            {
                pPing6->u2MaxTime = (UINT2) pPing6->u4TimeTaken;
                pPing6->u2MinTime = (UINT2) pPing6->u4TimeTaken;
            }
            else
            {
                if (pPing6->u4TimeTaken > pPing6->u2MaxTime)
                {
                    pPing6->u2MaxTime = (UINT2) pPing6->u4TimeTaken;
                }
                if (pPing6->u4TimeTaken < pPing6->u2MinTime)
                {
                    pPing6->u2MinTime = (UINT2) pPing6->u4TimeTaken;
                }
            }

            pPing6->u2Successes++;
            /*
             *                (N1 * M1) + (N2 * M2)
             *   Mean Time =  ----------------------   Where N2 = 1
             *                       N1 + N2           N1 = pPing6->i2Success - 1
             */

            pPing6->u2AvgTime = (pPing6->u2MaxTime + pPing6->u2MinTime) / 2;

            if (pPing6->i2AppId == SNMP_PING6_APP_ID)
            {
                /* Ping6 is init from SNMP, continue with next req */
                Ping6Send (pPing6);
            }
            else
            {
                /* Stop timer alone, the application will be trigger the
                 * ping request again */
                TmrStopTimer (gPing6TimerListId, &(pPing6->Timer.TimerNode));
                if (pPing6->u2SentCount == pPing6->i2MaxTries)
                {
                    pPing6->i1Oper = PING6_OPER_COMPLETED;
                }
            }
        }
    }
    else
    {
        /*if the received packet is other than icmp reply packet ,return failure */
        return OSIX_FAILURE;

    }

    /* If no valid Ping6 Instance exists, Close the Socket Opened
       for sending Ping6 Request and receving Ping6 replies */
    if (Ping6GetValidInstanceCount () == 0)
    {
        PING6_TRC_ARG (MGMT_TRC,
                       "Ping6ReplyHandler: Closing the ping socket \n");
        Ping6CloseSocket ();
    }
    return OSIX_SUCCESS;
}

#endif /* _PING6MAIN_C_ */
/**************************** END OF FILE ***********************************/
