/********************************************************************
 * Description:
 *          Ping6 API functions
 ********************************************************************/

#ifndef _PING6API_C
#define _PING6API_C
#include "png6inc.h"
#include "ping6.h"

/*****************************************************************************
 * Function     : Ping6ApiGetAdminStatus                                        
 *                                                                       
 * Description  : API to return Admin status of the ping6 instance  
 *                                                                       
 * Input        : u4PingInstance - Ping instance                         
 *                                                                       
 * Output       : Ping instance Status                                   
 *                                                                       
 * Returns      : OSIX_SUCCESS/OSIX_FAILURE
 *                                                                       
 *****************************************************************************/
PUBLIC INT4
Ping6ApiGetAdminStatus (UINT4 u4PingInstance, INT4 *pi4Status)
{

    if (u4PingInstance >= PING6_MAX_INSTANCES)
    {
        return OSIX_FAILURE;
    }

    Ping6Lock ();
    *pi4Status = gaPing6Table[u4PingInstance].i4RowStatus;
    Ping6UnLock ();

    return OSIX_SUCCESS;
}

#endif /* _PING6API_C */
/**************************** END OF FILE ***********************************/
