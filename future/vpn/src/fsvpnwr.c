/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fsvpnwr.c,v 1.8 2014/07/04 05:18:12 siva Exp $
 *
 * Description: File containing wrapper functions
 * *******************************************************************/

# include  "lr.h"
# include  "fssnmp.h"
# include  "fsvpnlw.h"
# include  "fsvpnwr.h"
# include  "fsvpndb.h"

VOID
RegisterFSVPN ()
{
    SNMPRegisterMibWithLock (&fsvpnpOID, &fsvpnpEntry, VpnDsLock, VpnDsUnLock,
                             SNMP_MSR_TGR_FALSE);
    SNMPAddSysorEntry (&fsvpnpOID, (const UINT1 *) "fsvpnpolicy");
}

VOID
UnRegisterFSVPN ()
{
    SNMPUnRegisterMib (&fsvpnpOID, &fsvpnpEntry);
    SNMPDelSysorEntry (&fsvpnpOID, (const UINT1 *) "fsvpnpolicy");
}

INT4
FsVpnGlobalStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsVpnGlobalStatus (&(pMultiData->i4_SLongValue)));
}

INT4
FsVpnMaxTunnelsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsVpnMaxTunnels (&(pMultiData->i4_SLongValue)));
}

INT4
FsVpnIpPktsInGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsVpnIpPktsIn (&(pMultiData->u4_ULongValue)));
}

INT4
FsVpnIpPktsOutGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsVpnIpPktsOut (&(pMultiData->u4_ULongValue)));
}

INT4
FsVpnPktsSecuredGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsVpnPktsSecured (&(pMultiData->u4_ULongValue)));
}

INT4
FsVpnPktsDroppedGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsVpnPktsDropped (&(pMultiData->u4_ULongValue)));
}

INT4
FsVpnIkeSAsActiveGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsVpnIkeSAsActive (&(pMultiData->u4_ULongValue)));
}

INT4
FsVpnIkeNegotiationsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsVpnIkeNegotiations (&(pMultiData->u4_ULongValue)));
}

INT4
FsVpnIkeRekeysGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsVpnIkeRekeys (&(pMultiData->u4_ULongValue)));
}

INT4
FsVpnIkeNegoFailedGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsVpnIkeNegoFailed (&(pMultiData->u4_ULongValue)));
}

INT4
FsVpnIPSecSAsActiveGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsVpnIPSecSAsActive (&(pMultiData->u4_ULongValue)));
}

INT4
FsVpnIPSecNegotiationsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsVpnIPSecNegotiations (&(pMultiData->u4_ULongValue)));
}

INT4
FsVpnIPSecNegoFailedGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsVpnIPSecNegoFailed (&(pMultiData->u4_ULongValue)));
}

INT4
FsVpnTotalRekeysGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsVpnTotalRekeys (&(pMultiData->u4_ULongValue)));
}

INT4
FsVpnRaServerGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsVpnRaServer (&(pMultiData->i4_SLongValue)));
}

INT4
FsVpnDummyPktGenGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsVpnDummyPktGen (&(pMultiData->i4_SLongValue)));
}

INT4
FsVpnDummyPktParamGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsVpnDummyPktParam (&(pMultiData->i4_SLongValue)));
}

INT4
FsVpnGlobalStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsVpnGlobalStatus (pMultiData->i4_SLongValue));
}

INT4
FsVpnRaServerSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsVpnRaServer (pMultiData->i4_SLongValue));
}

INT4
FsVpnDummyPktGenSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsVpnDummyPktGen (pMultiData->i4_SLongValue));
}

INT4
FsVpnDummyPktParamSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsVpnDummyPktParam (pMultiData->i4_SLongValue));
}

INT4
FsVpnGlobalStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsVpnGlobalStatus (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsVpnRaServerTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                   tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsVpnRaServer (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsVpnDummyPktGenTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsVpnDummyPktGen (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsVpnDummyPktParamTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsVpnDummyPktParam (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsVpnGlobalStatusDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsVpnGlobalStatus
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsVpnRaServerDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                  tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsVpnRaServer (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsVpnDummyPktGenDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsVpnDummyPktGen (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsVpnDummyPktParamDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsVpnDummyPktParam
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsVpnTable (tSnmpIndex * pFirstMultiIndex,
                        tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsVpnTable
            (pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsVpnTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsVpnPolicyTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsVpnTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsVpnPolicyType (pMultiIndex->pIndex[0].pOctetStrValue,
                                   &(pMultiData->i4_SLongValue)));

}

INT4
FsVpnCertAlgoTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsVpnTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsVpnCertAlgoType (pMultiIndex->pIndex[0].pOctetStrValue,
                                     &(pMultiData->i4_SLongValue)));

}

INT4
FsVpnPolicyPriorityGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsVpnTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsVpnPolicyPriority (pMultiIndex->pIndex[0].pOctetStrValue,
                                       &(pMultiData->i4_SLongValue)));

}

INT4
FsVpnTunTermAddrTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsVpnTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsVpnTunTermAddrType (pMultiIndex->pIndex[0].pOctetStrValue,
                                        &(pMultiData->i4_SLongValue)));

}

INT4
FsVpnLocalTunTermAddrGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsVpnTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsVpnLocalTunTermAddr (pMultiIndex->pIndex[0].pOctetStrValue,
                                         pMultiData->pOctetStrValue));

}

INT4
FsVpnRemoteTunTermAddrGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsVpnTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsVpnRemoteTunTermAddr (pMultiIndex->pIndex[0].pOctetStrValue,
                                          pMultiData->pOctetStrValue));

}

INT4
FsVpnProtectNetworkTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsVpnTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsVpnProtectNetworkType
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsVpnLocalProtectNetworkGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsVpnTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsVpnLocalProtectNetwork
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
FsVpnLocalProtectNetworkPrefixLenGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsVpnTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsVpnLocalProtectNetworkPrefixLen
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsVpnRemoteProtectNetworkGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsVpnTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsVpnRemoteProtectNetwork
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
FsVpnRemoteProtectNetworkPrefixLenGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsVpnTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsVpnRemoteProtectNetworkPrefixLen
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsVpnIkeSrcPortRangeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsVpnTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsVpnIkeSrcPortRange (pMultiIndex->pIndex[0].pOctetStrValue,
                                        pMultiData->pOctetStrValue));

}

INT4
FsVpnIkeDstPortRangeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsVpnTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsVpnIkeDstPortRange (pMultiIndex->pIndex[0].pOctetStrValue,
                                        pMultiData->pOctetStrValue));

}

INT4
FsVpnSecurityProtocolGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsVpnTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsVpnSecurityProtocol (pMultiIndex->pIndex[0].pOctetStrValue,
                                         &(pMultiData->i4_SLongValue)));

}

INT4
FsVpnInboundSpiGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsVpnTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsVpnInboundSpi (pMultiIndex->pIndex[0].pOctetStrValue,
                                   &(pMultiData->i4_SLongValue)));

}

INT4
FsVpnOutboundSpiGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsVpnTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsVpnOutboundSpi (pMultiIndex->pIndex[0].pOctetStrValue,
                                    &(pMultiData->i4_SLongValue)));

}

INT4
FsVpnModeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsVpnTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsVpnMode (pMultiIndex->pIndex[0].pOctetStrValue,
                             &(pMultiData->i4_SLongValue)));

}

INT4
FsVpnAuthAlgoGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsVpnTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsVpnAuthAlgo (pMultiIndex->pIndex[0].pOctetStrValue,
                                 &(pMultiData->i4_SLongValue)));

}

INT4
FsVpnAhKeyGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsVpnTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsVpnAhKey (pMultiIndex->pIndex[0].pOctetStrValue,
                              pMultiData->pOctetStrValue));

}

INT4
FsVpnEncrAlgoGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsVpnTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsVpnEncrAlgo (pMultiIndex->pIndex[0].pOctetStrValue,
                                 &(pMultiData->i4_SLongValue)));

}

INT4
FsVpnEspKeyGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsVpnTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsVpnEspKey (pMultiIndex->pIndex[0].pOctetStrValue,
                               pMultiData->pOctetStrValue));

}

INT4
FsVpnAntiReplayGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsVpnTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsVpnAntiReplay (pMultiIndex->pIndex[0].pOctetStrValue,
                                   &(pMultiData->i4_SLongValue)));

}

INT4
FsVpnPolicyFlagGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsVpnTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsVpnPolicyFlag (pMultiIndex->pIndex[0].pOctetStrValue,
                                   &(pMultiData->i4_SLongValue)));

}

INT4
FsVpnProtocolGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsVpnTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsVpnProtocol (pMultiIndex->pIndex[0].pOctetStrValue,
                                 &(pMultiData->i4_SLongValue)));

}

INT4
FsVpnPolicyIntfIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsVpnTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsVpnPolicyIntfIndex (pMultiIndex->pIndex[0].pOctetStrValue,
                                        &(pMultiData->i4_SLongValue)));

}

INT4
FsVpnIkePhase1HashAlgoGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsVpnTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsVpnIkePhase1HashAlgo (pMultiIndex->pIndex[0].pOctetStrValue,
                                          &(pMultiData->i4_SLongValue)));

}

INT4
FsVpnIkePhase1EncryptionAlgoGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsVpnTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsVpnIkePhase1EncryptionAlgo
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsVpnIkePhase1DHGroupGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsVpnTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsVpnIkePhase1DHGroup (pMultiIndex->pIndex[0].pOctetStrValue,
                                         &(pMultiData->i4_SLongValue)));

}

INT4
FsVpnIkePhase1LocalIdTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsVpnTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsVpnIkePhase1LocalIdType
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsVpnIkePhase1LocalIdValueGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsVpnTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsVpnIkePhase1LocalIdValue
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
FsVpnIkePhase1PeerIdTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsVpnTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsVpnIkePhase1PeerIdType
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsVpnIkePhase1PeerIdValueGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsVpnTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsVpnIkePhase1PeerIdValue
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
FsVpnIkePhase1LifeTimeTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsVpnTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsVpnIkePhase1LifeTimeType
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsVpnIkePhase1LifeTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsVpnTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsVpnIkePhase1LifeTime (pMultiIndex->pIndex[0].pOctetStrValue,
                                          &(pMultiData->i4_SLongValue)));

}

INT4
FsVpnIkePhase1ModeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsVpnTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsVpnIkePhase1Mode (pMultiIndex->pIndex[0].pOctetStrValue,
                                      &(pMultiData->i4_SLongValue)));

}

INT4
FsVpnIkePhase2AuthAlgoGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsVpnTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsVpnIkePhase2AuthAlgo (pMultiIndex->pIndex[0].pOctetStrValue,
                                          &(pMultiData->i4_SLongValue)));

}

INT4
FsVpnIkePhase2EspEncryptionAlgoGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsVpnTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsVpnIkePhase2EspEncryptionAlgo
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsVpnIkePhase2LifeTimeTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsVpnTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsVpnIkePhase2LifeTimeType
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsVpnIkePhase2LifeTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsVpnTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsVpnIkePhase2LifeTime (pMultiIndex->pIndex[0].pOctetStrValue,
                                          &(pMultiData->i4_SLongValue)));

}

INT4
FsVpnIkePhase2DHGroupGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsVpnTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsVpnIkePhase2DHGroup (pMultiIndex->pIndex[0].pOctetStrValue,
                                         &(pMultiData->i4_SLongValue)));

}

INT4
FsVpnIkeVersionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsVpnTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsVpnIkeVersion (pMultiIndex->pIndex[0].pOctetStrValue,
                                   &(pMultiData->i4_SLongValue)));

}

INT4
FsVpnPolicyRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsVpnTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsVpnPolicyRowStatus (pMultiIndex->pIndex[0].pOctetStrValue,
                                        &(pMultiData->i4_SLongValue)));

}

INT4
FsVpnPolicyTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsVpnPolicyType (pMultiIndex->pIndex[0].pOctetStrValue,
                                   pMultiData->i4_SLongValue));

}

INT4
FsVpnCertAlgoTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsVpnCertAlgoType (pMultiIndex->pIndex[0].pOctetStrValue,
                                     pMultiData->i4_SLongValue));

}

INT4
FsVpnPolicyPrioritySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsVpnPolicyPriority (pMultiIndex->pIndex[0].pOctetStrValue,
                                       pMultiData->i4_SLongValue));

}

INT4
FsVpnTunTermAddrTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsVpnTunTermAddrType (pMultiIndex->pIndex[0].pOctetStrValue,
                                        pMultiData->i4_SLongValue));

}

INT4
FsVpnLocalTunTermAddrSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsVpnLocalTunTermAddr (pMultiIndex->pIndex[0].pOctetStrValue,
                                         pMultiData->pOctetStrValue));

}

INT4
FsVpnRemoteTunTermAddrSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsVpnRemoteTunTermAddr (pMultiIndex->pIndex[0].pOctetStrValue,
                                          pMultiData->pOctetStrValue));

}

INT4
FsVpnProtectNetworkTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsVpnProtectNetworkType
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsVpnLocalProtectNetworkSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsVpnLocalProtectNetwork
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
FsVpnLocalProtectNetworkPrefixLenSet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhSetFsVpnLocalProtectNetworkPrefixLen
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->u4_ULongValue));

}

INT4
FsVpnRemoteProtectNetworkSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsVpnRemoteProtectNetwork
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
FsVpnRemoteProtectNetworkPrefixLenSet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhSetFsVpnRemoteProtectNetworkPrefixLen
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->u4_ULongValue));

}

INT4
FsVpnIkeSrcPortRangeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsVpnIkeSrcPortRange (pMultiIndex->pIndex[0].pOctetStrValue,
                                        pMultiData->pOctetStrValue));

}

INT4
FsVpnIkeDstPortRangeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsVpnIkeDstPortRange (pMultiIndex->pIndex[0].pOctetStrValue,
                                        pMultiData->pOctetStrValue));

}

INT4
FsVpnSecurityProtocolSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsVpnSecurityProtocol (pMultiIndex->pIndex[0].pOctetStrValue,
                                         pMultiData->i4_SLongValue));

}

INT4
FsVpnInboundSpiSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsVpnInboundSpi (pMultiIndex->pIndex[0].pOctetStrValue,
                                   pMultiData->i4_SLongValue));

}

INT4
FsVpnOutboundSpiSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsVpnOutboundSpi (pMultiIndex->pIndex[0].pOctetStrValue,
                                    pMultiData->i4_SLongValue));

}

INT4
FsVpnModeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsVpnMode (pMultiIndex->pIndex[0].pOctetStrValue,
                             pMultiData->i4_SLongValue));

}

INT4
FsVpnAuthAlgoSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsVpnAuthAlgo (pMultiIndex->pIndex[0].pOctetStrValue,
                                 pMultiData->i4_SLongValue));

}

INT4
FsVpnAhKeySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsVpnAhKey (pMultiIndex->pIndex[0].pOctetStrValue,
                              pMultiData->pOctetStrValue));

}

INT4
FsVpnEncrAlgoSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsVpnEncrAlgo (pMultiIndex->pIndex[0].pOctetStrValue,
                                 pMultiData->i4_SLongValue));

}

INT4
FsVpnEspKeySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsVpnEspKey (pMultiIndex->pIndex[0].pOctetStrValue,
                               pMultiData->pOctetStrValue));

}

INT4
FsVpnAntiReplaySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsVpnAntiReplay (pMultiIndex->pIndex[0].pOctetStrValue,
                                   pMultiData->i4_SLongValue));

}

INT4
FsVpnPolicyFlagSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsVpnPolicyFlag (pMultiIndex->pIndex[0].pOctetStrValue,
                                   pMultiData->i4_SLongValue));

}

INT4
FsVpnProtocolSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsVpnProtocol (pMultiIndex->pIndex[0].pOctetStrValue,
                                 pMultiData->i4_SLongValue));

}

INT4
FsVpnPolicyIntfIndexSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsVpnPolicyIntfIndex (pMultiIndex->pIndex[0].pOctetStrValue,
                                        pMultiData->i4_SLongValue));

}

INT4
FsVpnIkePhase1HashAlgoSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsVpnIkePhase1HashAlgo (pMultiIndex->pIndex[0].pOctetStrValue,
                                          pMultiData->i4_SLongValue));

}

INT4
FsVpnIkePhase1EncryptionAlgoSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsVpnIkePhase1EncryptionAlgo
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsVpnIkePhase1DHGroupSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsVpnIkePhase1DHGroup (pMultiIndex->pIndex[0].pOctetStrValue,
                                         pMultiData->i4_SLongValue));

}

INT4
FsVpnIkePhase1LocalIdTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsVpnIkePhase1LocalIdType
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsVpnIkePhase1LocalIdValueSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsVpnIkePhase1LocalIdValue
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
FsVpnIkePhase1PeerIdTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsVpnIkePhase1PeerIdType
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsVpnIkePhase1PeerIdValueSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsVpnIkePhase1PeerIdValue
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
FsVpnIkePhase1LifeTimeTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsVpnIkePhase1LifeTimeType
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsVpnIkePhase1LifeTimeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsVpnIkePhase1LifeTime (pMultiIndex->pIndex[0].pOctetStrValue,
                                          pMultiData->i4_SLongValue));

}

INT4
FsVpnIkePhase1ModeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsVpnIkePhase1Mode (pMultiIndex->pIndex[0].pOctetStrValue,
                                      pMultiData->i4_SLongValue));

}

INT4
FsVpnIkePhase2AuthAlgoSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsVpnIkePhase2AuthAlgo (pMultiIndex->pIndex[0].pOctetStrValue,
                                          pMultiData->i4_SLongValue));

}

INT4
FsVpnIkePhase2EspEncryptionAlgoSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhSetFsVpnIkePhase2EspEncryptionAlgo
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsVpnIkePhase2LifeTimeTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsVpnIkePhase2LifeTimeType
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsVpnIkePhase2LifeTimeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsVpnIkePhase2LifeTime (pMultiIndex->pIndex[0].pOctetStrValue,
                                          pMultiData->i4_SLongValue));

}

INT4
FsVpnIkePhase2DHGroupSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsVpnIkePhase2DHGroup (pMultiIndex->pIndex[0].pOctetStrValue,
                                         pMultiData->i4_SLongValue));

}

INT4
FsVpnIkeVersionSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsVpnIkeVersion (pMultiIndex->pIndex[0].pOctetStrValue,
                                   pMultiData->i4_SLongValue));

}

INT4
FsVpnPolicyRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsVpnPolicyRowStatus (pMultiIndex->pIndex[0].pOctetStrValue,
                                        pMultiData->i4_SLongValue));

}

INT4
FsVpnPolicyTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    return (nmhTestv2FsVpnPolicyType (pu4Error,
                                      pMultiIndex->pIndex[0].pOctetStrValue,
                                      pMultiData->i4_SLongValue));

}

INT4
FsVpnCertAlgoTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    return (nmhTestv2FsVpnCertAlgoType (pu4Error,
                                        pMultiIndex->pIndex[0].pOctetStrValue,
                                        pMultiData->i4_SLongValue));

}

INT4
FsVpnPolicyPriorityTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2FsVpnPolicyPriority (pu4Error,
                                          pMultiIndex->pIndex[0].pOctetStrValue,
                                          pMultiData->i4_SLongValue));

}

INT4
FsVpnTunTermAddrTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2FsVpnTunTermAddrType (pu4Error,
                                           pMultiIndex->pIndex[0].
                                           pOctetStrValue,
                                           pMultiData->i4_SLongValue));

}

INT4
FsVpnLocalTunTermAddrTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2FsVpnLocalTunTermAddr (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            pOctetStrValue,
                                            pMultiData->pOctetStrValue));

}

INT4
FsVpnRemoteTunTermAddrTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2FsVpnRemoteTunTermAddr (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             pOctetStrValue,
                                             pMultiData->pOctetStrValue));

}

INT4
FsVpnProtectNetworkTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2FsVpnProtectNetworkType (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              pOctetStrValue,
                                              pMultiData->i4_SLongValue));

}

INT4
FsVpnLocalProtectNetworkTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2FsVpnLocalProtectNetwork (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               pOctetStrValue,
                                               pMultiData->pOctetStrValue));

}

INT4
FsVpnLocalProtectNetworkPrefixLenTest (UINT4 *pu4Error,
                                       tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhTestv2FsVpnLocalProtectNetworkPrefixLen (pu4Error,
                                                        pMultiIndex->pIndex[0].
                                                        pOctetStrValue,
                                                        pMultiData->
                                                        u4_ULongValue));

}

INT4
FsVpnRemoteProtectNetworkTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2FsVpnRemoteProtectNetwork (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                pOctetStrValue,
                                                pMultiData->pOctetStrValue));

}

INT4
FsVpnRemoteProtectNetworkPrefixLenTest (UINT4 *pu4Error,
                                        tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    return (nmhTestv2FsVpnRemoteProtectNetworkPrefixLen (pu4Error,
                                                         pMultiIndex->pIndex[0].
                                                         pOctetStrValue,
                                                         pMultiData->
                                                         u4_ULongValue));

}

INT4
FsVpnIkeSrcPortRangeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2FsVpnIkeSrcPortRange (pu4Error,
                                           pMultiIndex->pIndex[0].
                                           pOctetStrValue,
                                           pMultiData->pOctetStrValue));

}

INT4
FsVpnIkeDstPortRangeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2FsVpnIkeDstPortRange (pu4Error,
                                           pMultiIndex->pIndex[0].
                                           pOctetStrValue,
                                           pMultiData->pOctetStrValue));

}

INT4
FsVpnSecurityProtocolTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2FsVpnSecurityProtocol (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            pOctetStrValue,
                                            pMultiData->i4_SLongValue));

}

INT4
FsVpnInboundSpiTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    return (nmhTestv2FsVpnInboundSpi (pu4Error,
                                      pMultiIndex->pIndex[0].pOctetStrValue,
                                      pMultiData->i4_SLongValue));

}

INT4
FsVpnOutboundSpiTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    return (nmhTestv2FsVpnOutboundSpi (pu4Error,
                                       pMultiIndex->pIndex[0].pOctetStrValue,
                                       pMultiData->i4_SLongValue));

}

INT4
FsVpnModeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsVpnMode (pu4Error,
                                pMultiIndex->pIndex[0].pOctetStrValue,
                                pMultiData->i4_SLongValue));

}

INT4
FsVpnAuthAlgoTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                   tRetVal * pMultiData)
{
    return (nmhTestv2FsVpnAuthAlgo (pu4Error,
                                    pMultiIndex->pIndex[0].pOctetStrValue,
                                    pMultiData->i4_SLongValue));

}

INT4
FsVpnAhKeyTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsVpnAhKey (pu4Error,
                                 pMultiIndex->pIndex[0].pOctetStrValue,
                                 pMultiData->pOctetStrValue));

}

INT4
FsVpnEncrAlgoTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                   tRetVal * pMultiData)
{
    return (nmhTestv2FsVpnEncrAlgo (pu4Error,
                                    pMultiIndex->pIndex[0].pOctetStrValue,
                                    pMultiData->i4_SLongValue));

}

INT4
FsVpnEspKeyTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                 tRetVal * pMultiData)
{
    return (nmhTestv2FsVpnEspKey (pu4Error,
                                  pMultiIndex->pIndex[0].pOctetStrValue,
                                  pMultiData->pOctetStrValue));

}

INT4
FsVpnAntiReplayTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    return (nmhTestv2FsVpnAntiReplay (pu4Error,
                                      pMultiIndex->pIndex[0].pOctetStrValue,
                                      pMultiData->i4_SLongValue));

}

INT4
FsVpnPolicyFlagTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    return (nmhTestv2FsVpnPolicyFlag (pu4Error,
                                      pMultiIndex->pIndex[0].pOctetStrValue,
                                      pMultiData->i4_SLongValue));

}

INT4
FsVpnProtocolTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                   tRetVal * pMultiData)
{
    return (nmhTestv2FsVpnProtocol (pu4Error,
                                    pMultiIndex->pIndex[0].pOctetStrValue,
                                    pMultiData->i4_SLongValue));

}

INT4
FsVpnPolicyIntfIndexTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2FsVpnPolicyIntfIndex (pu4Error,
                                           pMultiIndex->pIndex[0].
                                           pOctetStrValue,
                                           pMultiData->i4_SLongValue));

}

INT4
FsVpnIkePhase1HashAlgoTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2FsVpnIkePhase1HashAlgo (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             pOctetStrValue,
                                             pMultiData->i4_SLongValue));

}

INT4
FsVpnIkePhase1EncryptionAlgoTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhTestv2FsVpnIkePhase1EncryptionAlgo (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   pOctetStrValue,
                                                   pMultiData->i4_SLongValue));

}

INT4
FsVpnIkePhase1DHGroupTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2FsVpnIkePhase1DHGroup (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            pOctetStrValue,
                                            pMultiData->i4_SLongValue));

}

INT4
FsVpnIkePhase1LocalIdTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2FsVpnIkePhase1LocalIdType (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                pOctetStrValue,
                                                pMultiData->i4_SLongValue));

}

INT4
FsVpnIkePhase1LocalIdValueTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    return (nmhTestv2FsVpnIkePhase1LocalIdValue (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 pOctetStrValue,
                                                 pMultiData->pOctetStrValue));

}

INT4
FsVpnIkePhase1PeerIdTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2FsVpnIkePhase1PeerIdType (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               pOctetStrValue,
                                               pMultiData->i4_SLongValue));

}

INT4
FsVpnIkePhase1PeerIdValueTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2FsVpnIkePhase1PeerIdValue (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                pOctetStrValue,
                                                pMultiData->pOctetStrValue));

}

INT4
FsVpnIkePhase1LifeTimeTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    return (nmhTestv2FsVpnIkePhase1LifeTimeType (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 pOctetStrValue,
                                                 pMultiData->i4_SLongValue));

}

INT4
FsVpnIkePhase1LifeTimeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2FsVpnIkePhase1LifeTime (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             pOctetStrValue,
                                             pMultiData->i4_SLongValue));

}

INT4
FsVpnIkePhase1ModeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    return (nmhTestv2FsVpnIkePhase1Mode (pu4Error,
                                         pMultiIndex->pIndex[0].pOctetStrValue,
                                         pMultiData->i4_SLongValue));

}

INT4
FsVpnIkePhase2AuthAlgoTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2FsVpnIkePhase2AuthAlgo (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             pOctetStrValue,
                                             pMultiData->i4_SLongValue));

}

INT4
FsVpnIkePhase2EspEncryptionAlgoTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhTestv2FsVpnIkePhase2EspEncryptionAlgo (pu4Error,
                                                      pMultiIndex->pIndex[0].
                                                      pOctetStrValue,
                                                      pMultiData->
                                                      i4_SLongValue));

}

INT4
FsVpnIkePhase2LifeTimeTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    return (nmhTestv2FsVpnIkePhase2LifeTimeType (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 pOctetStrValue,
                                                 pMultiData->i4_SLongValue));

}

INT4
FsVpnIkePhase2LifeTimeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2FsVpnIkePhase2LifeTime (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             pOctetStrValue,
                                             pMultiData->i4_SLongValue));

}

INT4
FsVpnIkePhase2DHGroupTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2FsVpnIkePhase2DHGroup (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            pOctetStrValue,
                                            pMultiData->i4_SLongValue));

}

INT4
FsVpnIkeVersionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    return (nmhTestv2FsVpnIkeVersion (pu4Error,
                                      pMultiIndex->pIndex[0].pOctetStrValue,
                                      pMultiData->i4_SLongValue));

}

INT4
FsVpnPolicyRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2FsVpnPolicyRowStatus (pu4Error,
                                           pMultiIndex->pIndex[0].
                                           pOctetStrValue,
                                           pMultiData->i4_SLongValue));

}

INT4
FsVpnTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
               tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsVpnTable (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsVpnRaUsersTable (tSnmpIndex * pFirstMultiIndex,
                               tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsVpnRaUsersTable
            (pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsVpnRaUsersTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsVpnRaUserSecretGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsVpnRaUsersTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsVpnRaUserSecret (pMultiIndex->pIndex[0].pOctetStrValue,
                                     pMultiData->pOctetStrValue));

}

INT4
FsVpnRaUserRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsVpnRaUsersTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsVpnRaUserRowStatus (pMultiIndex->pIndex[0].pOctetStrValue,
                                        &(pMultiData->i4_SLongValue)));

}

INT4
FsVpnRaUserSecretSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsVpnRaUserSecret (pMultiIndex->pIndex[0].pOctetStrValue,
                                     pMultiData->pOctetStrValue));

}

INT4
FsVpnRaUserRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsVpnRaUserRowStatus (pMultiIndex->pIndex[0].pOctetStrValue,
                                        pMultiData->i4_SLongValue));

}

INT4
FsVpnRaUserSecretTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    return (nmhTestv2FsVpnRaUserSecret (pu4Error,
                                        pMultiIndex->pIndex[0].pOctetStrValue,
                                        pMultiData->pOctetStrValue));

}

INT4
FsVpnRaUserRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2FsVpnRaUserRowStatus (pu4Error,
                                           pMultiIndex->pIndex[0].
                                           pOctetStrValue,
                                           pMultiData->i4_SLongValue));

}

INT4
FsVpnRaUsersTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsVpnRaUsersTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsVpnRaAddressPoolTable (tSnmpIndex * pFirstMultiIndex,
                                     tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsVpnRaAddressPoolTable
            (pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsVpnRaAddressPoolTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsVpnRaAddressPoolAddrTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsVpnRaAddressPoolTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsVpnRaAddressPoolAddrType
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsVpnRaAddressPoolStartGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsVpnRaAddressPoolTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsVpnRaAddressPoolStart
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
FsVpnRaAddressPoolEndGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsVpnRaAddressPoolTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsVpnRaAddressPoolEnd (pMultiIndex->pIndex[0].pOctetStrValue,
                                         pMultiData->pOctetStrValue));

}

INT4
FsVpnRaAddressPoolPrefixLenGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsVpnRaAddressPoolTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsVpnRaAddressPoolPrefixLen
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsVpnRaAddressPoolRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsVpnRaAddressPoolTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsVpnRaAddressPoolRowStatus
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsVpnRaAddressPoolAddrTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsVpnRaAddressPoolAddrType
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsVpnRaAddressPoolStartSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsVpnRaAddressPoolStart
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
FsVpnRaAddressPoolEndSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsVpnRaAddressPoolEnd (pMultiIndex->pIndex[0].pOctetStrValue,
                                         pMultiData->pOctetStrValue));

}

INT4
FsVpnRaAddressPoolPrefixLenSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsVpnRaAddressPoolPrefixLen
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->u4_ULongValue));

}

INT4
FsVpnRaAddressPoolRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsVpnRaAddressPoolRowStatus
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsVpnRaAddressPoolAddrTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    return (nmhTestv2FsVpnRaAddressPoolAddrType (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 pOctetStrValue,
                                                 pMultiData->i4_SLongValue));

}

INT4
FsVpnRaAddressPoolStartTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2FsVpnRaAddressPoolStart (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              pOctetStrValue,
                                              pMultiData->pOctetStrValue));

}

INT4
FsVpnRaAddressPoolEndTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2FsVpnRaAddressPoolEnd (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            pOctetStrValue,
                                            pMultiData->pOctetStrValue));

}

INT4
FsVpnRaAddressPoolPrefixLenTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    return (nmhTestv2FsVpnRaAddressPoolPrefixLen (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  pOctetStrValue,
                                                  pMultiData->u4_ULongValue));

}

INT4
FsVpnRaAddressPoolRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    return (nmhTestv2FsVpnRaAddressPoolRowStatus (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  pOctetStrValue,
                                                  pMultiData->i4_SLongValue));

}

INT4
FsVpnRaAddressPoolTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsVpnRaAddressPoolTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsVpnRemoteIdTable (tSnmpIndex * pFirstMultiIndex,
                                tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsVpnRemoteIdTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pNextMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsVpnRemoteIdTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].pOctetStrValue,
             pNextMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsVpnRemoteIdKeyGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsVpnRemoteIdTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsVpnRemoteIdKey (pMultiIndex->pIndex[0].i4_SLongValue,
                                    pMultiIndex->pIndex[1].pOctetStrValue,
                                    pMultiData->pOctetStrValue));

}

INT4
FsVpnRemoteIdAuthTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsVpnRemoteIdTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsVpnRemoteIdAuthType (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiIndex->pIndex[1].pOctetStrValue,
                                         &(pMultiData->i4_SLongValue)));

}

INT4
FsVpnRemoteIdStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsVpnRemoteIdTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsVpnRemoteIdStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].pOctetStrValue,
                                       &(pMultiData->i4_SLongValue)));

}

INT4
FsVpnRemoteIdKeySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsVpnRemoteIdKey (pMultiIndex->pIndex[0].i4_SLongValue,
                                    pMultiIndex->pIndex[1].pOctetStrValue,
                                    pMultiData->pOctetStrValue));

}

INT4
FsVpnRemoteIdAuthTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsVpnRemoteIdAuthType (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiIndex->pIndex[1].pOctetStrValue,
                                         pMultiData->i4_SLongValue));

}

INT4
FsVpnRemoteIdStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsVpnRemoteIdStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].pOctetStrValue,
                                       pMultiData->i4_SLongValue));

}

INT4
FsVpnRemoteIdKeyTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    return (nmhTestv2FsVpnRemoteIdKey (pu4Error,
                                       pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].pOctetStrValue,
                                       pMultiData->pOctetStrValue));

}

INT4
FsVpnRemoteIdAuthTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2FsVpnRemoteIdAuthType (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            pMultiIndex->pIndex[1].
                                            pOctetStrValue,
                                            pMultiData->i4_SLongValue));

}

INT4
FsVpnRemoteIdStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2FsVpnRemoteIdStatus (pu4Error,
                                          pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiIndex->pIndex[1].pOctetStrValue,
                                          pMultiData->i4_SLongValue));

}

INT4
FsVpnRemoteIdTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsVpnRemoteIdTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsVpnCertInfoTable (tSnmpIndex * pFirstMultiIndex,
                                tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsVpnCertInfoTable
            (pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsVpnCertInfoTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsVpnCertKeyTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsVpnCertInfoTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsVpnCertKeyType (pMultiIndex->pIndex[0].pOctetStrValue,
                                    &(pMultiData->i4_SLongValue)));

}

INT4
FsVpnCertKeyFileNameGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsVpnCertInfoTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsVpnCertKeyFileName (pMultiIndex->pIndex[0].pOctetStrValue,
                                        pMultiData->pOctetStrValue));

}

INT4
FsVpnCertFileNameGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsVpnCertInfoTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsVpnCertFileName (pMultiIndex->pIndex[0].pOctetStrValue,
                                     pMultiData->pOctetStrValue));

}

INT4
FsVpnCertEncodeTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsVpnCertInfoTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsVpnCertEncodeType (pMultiIndex->pIndex[0].pOctetStrValue,
                                       &(pMultiData->i4_SLongValue)));

}

INT4
FsVpnCertStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsVpnCertInfoTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsVpnCertStatus (pMultiIndex->pIndex[0].pOctetStrValue,
                                   &(pMultiData->i4_SLongValue)));

}

INT4
FsVpnCertKeyTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsVpnCertKeyType (pMultiIndex->pIndex[0].pOctetStrValue,
                                    pMultiData->i4_SLongValue));

}

INT4
FsVpnCertKeyFileNameSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsVpnCertKeyFileName (pMultiIndex->pIndex[0].pOctetStrValue,
                                        pMultiData->pOctetStrValue));

}

INT4
FsVpnCertFileNameSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsVpnCertFileName (pMultiIndex->pIndex[0].pOctetStrValue,
                                     pMultiData->pOctetStrValue));

}

INT4
FsVpnCertEncodeTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsVpnCertEncodeType (pMultiIndex->pIndex[0].pOctetStrValue,
                                       pMultiData->i4_SLongValue));

}

INT4
FsVpnCertStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsVpnCertStatus (pMultiIndex->pIndex[0].pOctetStrValue,
                                   pMultiData->i4_SLongValue));

}

INT4
FsVpnCertKeyTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    return (nmhTestv2FsVpnCertKeyType (pu4Error,
                                       pMultiIndex->pIndex[0].pOctetStrValue,
                                       pMultiData->i4_SLongValue));

}

INT4
FsVpnCertKeyFileNameTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2FsVpnCertKeyFileName (pu4Error,
                                           pMultiIndex->pIndex[0].
                                           pOctetStrValue,
                                           pMultiData->pOctetStrValue));

}

INT4
FsVpnCertFileNameTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    return (nmhTestv2FsVpnCertFileName (pu4Error,
                                        pMultiIndex->pIndex[0].pOctetStrValue,
                                        pMultiData->pOctetStrValue));

}

INT4
FsVpnCertEncodeTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2FsVpnCertEncodeType (pu4Error,
                                          pMultiIndex->pIndex[0].pOctetStrValue,
                                          pMultiData->i4_SLongValue));

}

INT4
FsVpnCertStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    return (nmhTestv2FsVpnCertStatus (pu4Error,
                                      pMultiIndex->pIndex[0].pOctetStrValue,
                                      pMultiData->i4_SLongValue));

}

INT4
FsVpnCertInfoTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsVpnCertInfoTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsVpnCaCertInfoTable (tSnmpIndex * pFirstMultiIndex,
                                  tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsVpnCaCertInfoTable
            (pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsVpnCaCertInfoTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsVpnCaCertFileNameGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsVpnCaCertInfoTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsVpnCaCertFileName (pMultiIndex->pIndex[0].pOctetStrValue,
                                       pMultiData->pOctetStrValue));

}

INT4
FsVpnCaCertEncodeTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsVpnCaCertInfoTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsVpnCaCertEncodeType (pMultiIndex->pIndex[0].pOctetStrValue,
                                         &(pMultiData->i4_SLongValue)));

}

INT4
FsVpnCaCertStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsVpnCaCertInfoTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsVpnCaCertStatus (pMultiIndex->pIndex[0].pOctetStrValue,
                                     &(pMultiData->i4_SLongValue)));

}

INT4
FsVpnCaCertFileNameSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsVpnCaCertFileName (pMultiIndex->pIndex[0].pOctetStrValue,
                                       pMultiData->pOctetStrValue));

}

INT4
FsVpnCaCertEncodeTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsVpnCaCertEncodeType (pMultiIndex->pIndex[0].pOctetStrValue,
                                         pMultiData->i4_SLongValue));

}

INT4
FsVpnCaCertStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsVpnCaCertStatus (pMultiIndex->pIndex[0].pOctetStrValue,
                                     pMultiData->i4_SLongValue));

}

INT4
FsVpnCaCertFileNameTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2FsVpnCaCertFileName (pu4Error,
                                          pMultiIndex->pIndex[0].pOctetStrValue,
                                          pMultiData->pOctetStrValue));

}

INT4
FsVpnCaCertEncodeTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2FsVpnCaCertEncodeType (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            pOctetStrValue,
                                            pMultiData->i4_SLongValue));

}

INT4
FsVpnCaCertStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    return (nmhTestv2FsVpnCaCertStatus (pu4Error,
                                        pMultiIndex->pIndex[0].pOctetStrValue,
                                        pMultiData->i4_SLongValue));

}

INT4
FsVpnCaCertInfoTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsVpnCaCertInfoTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4 FsIkeTraceOptionGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetFsIkeTraceOption(&(pMultiData->i4_SLongValue)));
}
INT4 FsIpsecTraceOptionGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetFsIpsecTraceOption(&(pMultiData->i4_SLongValue)));
}

INT4 FsIkeTraceOptionSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhSetFsIkeTraceOption(pMultiData->i4_SLongValue));
}


INT4 FsIpsecTraceOptionSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhSetFsIpsecTraceOption(pMultiData->i4_SLongValue));
}


INT4 FsIkeTraceOptionTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhTestv2FsIkeTraceOption(pu4Error, pMultiData->i4_SLongValue));
}


INT4 FsIpsecTraceOptionTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhTestv2FsIpsecTraceOption(pu4Error, pMultiData->i4_SLongValue));
}
INT4 FsIkeTraceOptionDep(UINT4 *pu4Error,tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2FsIkeTraceOption(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4 FsIpsecTraceOptionDep(UINT4 *pu4Error,tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2FsIpsecTraceOption(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}
