/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: vpnconf.c,v 1.15 2014/03/16 11:28:59 siva Exp $
 *
 * Description: File containing utility functions
 * *******************************************************************/
# include "lr.h"
# include "cfa.h"
# include "fssnmp.h"
# include "vpninc.h"
#include "cli.h"
#include "seccli.h"
#include "vpncli.h"
#include "secv4.h"
#include "fsvpnwr.h"
#include "ip6util.h"

#ifdef IKE_WANTED
#include "fsike.h"
#include "fssocket.h"
#endif

INT4                VpnIkeCheckAndDeleteEngine (UINT4 u4IfIndex);

/* This function will be invoked whenever there is a state change or IP address
 * change on the WAN interface.
 */
INT4
VpnIntfStateChngTrigger (UINT2 u2State, UINT4 u4IfIndex, UINT4 u4IpAddress,
                         UINT1 u1NwType, UINT1 u1WanType)
{
    UINT2               u2IsValidIntf = TRUE;
    tVpnPolicy         *pVpnPolicy = NULL;
    tSNMP_OCTET_STRING_TYPE PolicyName;
    tSNMP_OCTET_STRING_TYPE ProtectNetwork;
    UINT1               au1IpAddr[IPVX_IPV4_ADDR_LEN] = { VPN_ZERO };
    tCfaIfInfo          IfInfo;

    MEMSET (&IfInfo, VPN_ZERO, sizeof (tCfaIfInfo));
    /* Check for the validity of the state */
    if ((u2State != VPN_INTF_DOWN) &&
        (u2State != VPN_INTF_UP) && (u2State != VPN_INTF_IP_CHNG)
        && (u2State != VPN_INTF_DELETE))
    {
        return (CFA_FAILURE);
    }

    if ((u1NwType != CFA_NETWORK_TYPE_WAN) ||
        (u1WanType != CFA_WAN_TYPE_PUBLIC))
    {
        /* Intf type might have changed from WAN to something else */
        u2IsValidIntf = FALSE;
    }

    if (((u2State == VPN_INTF_UP) ||
         (u2State == VPN_INTF_IP_CHNG)) && (u4IpAddress == VPN_ZERO))
    {
        /* No ip addr is configured for the WAN interface */
        u2IsValidIntf = FALSE;
    }

    /* Search for all the policies which are applied on 
     * this inteface index. 
     */

    if (CfaGetIfInfo (u4IfIndex, &IfInfo) != CFA_SUCCESS)
    {
        return (CFA_FAILURE);
    }
    if ((u2State == VPN_INTF_DELETE) && (IfInfo.u1IfType != CFA_ENET))
    {
        /* The interface type check is for avoiding disabling the 
           policy if ip addr allocation mode is been changed from manual
           to DHCP which cause destroying the row */

        u2IsValidIntf = FALSE;
    }
    VpnDsLock ();

    TMO_SLL_Scan (&VpnPolicyList, pVpnPolicy, tVpnPolicy *)
    {
        if ((u4IfIndex != pVpnPolicy->u4IfIndex) ||
            (pVpnPolicy->u4IfIndex == VPN_ZERO))
        {
            /* Don't care the inactive tunnels (i.e not attached to any intf)
             * and not belongs to this intf.
             */
            continue;
        }

        if (u2State == VPN_INTF_DOWN)
        {
            continue;
        }

        PolicyName.pu1_OctetList = pVpnPolicy->au1VpnPolicyName;
        PolicyName.i4_Length = (INT4) STRLEN (pVpnPolicy->au1VpnPolicyName);

        if (u2IsValidIntf == FALSE)
        {
            /* Disable all the policies since the interface is no more valid */
            nmhSetFsVpnPolicyRowStatus (&PolicyName, NOT_IN_SERVICE);
            continue;
        }

        if ((u2State != VPN_INTF_DOWN) &&
            (u4IpAddress != pVpnPolicy->LocalTunnTermAddr.uIpAddr.Ip4Addr))
        {
            /* WAN ineterface ip got changed; So propagate the change to NP */
            if (nmhSetFsVpnPolicyRowStatus (&PolicyName, NOT_IN_SERVICE) ==
                SNMP_SUCCESS)
            {
                if (nmhSetFsVpnPolicyIntfIndex (&PolicyName, (INT4) u4IfIndex)
                    != SNMP_SUCCESS)
                {
                    VpnDsUnLock ();
                    return (CFA_FAILURE);
                }
                if (pVpnPolicy->u1VpnMode == VPN_TRANSPORT)
                {
                    MEMSET (au1IpAddr, VPN_ZERO, sizeof (UINT4));
                    MEMCPY (au1IpAddr, &u4IpAddress, sizeof (UINT4));

                    ProtectNetwork.pu1_OctetList = au1IpAddr;
                    ProtectNetwork.i4_Length = sizeof (UINT4);
                    /* Update the local protected n/w to WAN ip */
                    if (nmhSetFsVpnLocalProtectNetwork
                        (&PolicyName, &ProtectNetwork) != SNMP_SUCCESS)
                    {
                        VpnDsUnLock ();
                        return (CFA_FAILURE);
                    }
                }
                nmhSetFsVpnPolicyRowStatus (&PolicyName, ACTIVE);
            }
        }
    }

    if ((u1NwType != CFA_NETWORK_TYPE_WAN) ||
        (u1WanType != CFA_WAN_TYPE_PUBLIC))
    {
        /* Intf type might have changed from WAN to something else */

        /* Ike Engine associated to a WAN interface must be deleted when
         * a WAN port is turned to some other type */
        VpnIkeCheckAndDeleteEngine (u4IfIndex);
    }
    VpnDsUnLock ();

    return (CFA_SUCCESS);
}

/****************************************************************************
*  Function Name   : VpnIpv6IntfStateChngTrigger                            *
*                                                                           *
*  Description     : This function will be called when there is status      *
*                    change in the interface related parameters. Based on   *
*                    status it will trigger the address change indiacton    *
*                    to the IKE and make VPN policies active/inactive.      *
*                                                                           *
*  Input (s)       : u2Event - event type                                   *
*                    u4IfIndex - Interface index whose status is changed    *
*                    u1NwType - network type WAN/LAN                        *
*                    u1WanType - Wan type PUBLIC/PRIVATE                    *
*                                                                           *
*  Output(s)       : None.                                                  *
*                                                                           *
*  Returns         : VPN_FAILURE/VPN_SUCCESS.                               *
****************************************************************************/
INT4
VpnIpv6IntfStateChngTrigger (UINT2 u2Event, UINT4 u4IfIndex, UINT1 u1NwType,
                             UINT1 u1WanType)
{
    tNetIpv6AddrInfo    NetIpv6AddrInfo;
    tIp6Addr            NullIp6Addr;
#ifdef IKE_WANTED
    tUtlIn6Addr         In6Addr;
#endif
    UINT2               u2IsValidIntf = TRUE;
    tVpnPolicy         *pVpnPolicy = NULL;
    tSNMP_OCTET_STRING_TYPE PolicyName;
    tSNMP_OCTET_STRING_TYPE ProtectNetwork;
    tCfaIfInfo          IfInfo;
#ifdef IKE_WANTED
    UINT4               u4SnmpErrorStatus = VPN_ZERO;
    UINT1               au1EngineName[MAX_NAME_LENGTH] = { VPN_ZERO };
    UINT1               au1LocTunTerAddr[IKE_MAX_ADDR_STR_LEN] = { VPN_ZERO };
    tSNMP_OCTET_STRING_TYPE EngineNameOctetStr;
    tSNMP_OCTET_STRING_TYPE LocTunTerAddrOctStr;
    UINT1              *pu1EngineName = NULL;
#endif

    MEMSET (&IfInfo, VPN_ZERO, sizeof (tCfaIfInfo));
    MEMSET (&NetIpv6AddrInfo, VPN_ZERO, sizeof (tNetIpv6AddrInfo));
    MEMSET (&NullIp6Addr, VPN_ZERO, sizeof (tIp6Addr));

    if ((u1NwType != CFA_NETWORK_TYPE_WAN) ||
        (u1WanType != CFA_WAN_TYPE_PUBLIC))
    {
        /* Interface type might have changed from WAN to something else */
        u2IsValidIntf = FALSE;
    }

#ifdef IP6_WANTED
    if ((u2Event == VPN_ADDR_ADD) &&
        (NetIpv6GetFirstIfAddr (u4IfIndex, &NetIpv6AddrInfo)
         != NETIPV6_SUCCESS))
    {
        return (VPN_FAILURE);
    }
#endif

    if ((u2Event == VPN_ADDR_ADD) &&
        (Ip6AddrCompare (NullIp6Addr, NetIpv6AddrInfo.Ip6Addr) == IP6_ZERO))
    {
        /* No ip addr is configured for the WAN interface */
        u2IsValidIntf = FALSE;
    }

    if (CfaGetIfInfo (u4IfIndex, &IfInfo) != CFA_SUCCESS)
    {
        return (VPN_FAILURE);
    }

    if ((u2Event == VPN_INTF_DELETE) && (IfInfo.u1IfType != CFA_ENET))
    {
        /* The interface type check is for avoiding disabling the 
           policy if ip addr allocation mode is been changed from manual
           to DHCP which cause destroying the row */
        u2IsValidIntf = FALSE;
    }

    VpnDsLock ();

#ifdef IKE_WANTED
    if ((u2Event != VPN_ADDR_DEL) && (u1NwType == CFA_NETWORK_TYPE_WAN) &&
        (u1WanType == CFA_WAN_TYPE_PUBLIC) && (IfInfo.u1IfType != CFA_VPNC))
    {
        /* Set the  Tunnel termination address for the default 
         * IKE Engine. The Engine Socket will be bound to this 
         * IP address */
        MEMSET (au1EngineName, VPN_ZERO, MAX_NAME_LENGTH);
        MEMSET (au1LocTunTerAddr, VPN_ZERO, IKE_MAX_ADDR_STR_LEN);

        pu1EngineName = IkeGetEngNameFromIfIndex (u4IfIndex);

        if (NULL == pu1EngineName)
        {
            VpnDsUnLock ();
            return (CFA_FAILURE);
        }

        STRNCPY (au1EngineName, pu1EngineName, MAX_NAME_LENGTH - VPN_ONE);
        EngineNameOctetStr.pu1_OctetList = au1EngineName;
        EngineNameOctetStr.i4_Length = (INT4) STRLEN (au1EngineName);

        /* Set the Tunnel Termination Address */
        MEMCPY (&In6Addr, &(NetIpv6AddrInfo.Ip6Addr), sizeof (tIp6Addr));
        MEMCPY (au1LocTunTerAddr, INET_NTOA6 (In6Addr), VPN_IPV6_ADDR_LEN);

        LocTunTerAddrOctStr.pu1_OctetList = au1LocTunTerAddr;
        LocTunTerAddrOctStr.i4_Length = (INT4) STRLEN (au1LocTunTerAddr);

        /* The Default Engine is created by default, so set the wan 
         * ip address as the tunnel termination address */
        if (nmhTestv2FsIkeEngineTunnelTermAddr (&u4SnmpErrorStatus,
                                                &EngineNameOctetStr,
                                                &LocTunTerAddrOctStr) ==
            SNMP_SUCCESS)
        {
            nmhSetFsIkeEngineTunnelTermAddr (&EngineNameOctetStr,
                                             &LocTunTerAddrOctStr);
        }
    }
#endif

    TMO_SLL_Scan (&VpnPolicyList, pVpnPolicy, tVpnPolicy *)
    {
        if ((u4IfIndex != pVpnPolicy->u4IfIndex) ||
            (pVpnPolicy->u4IfIndex == VPN_ZERO))
        {
            /* Don't care the inactive tunnels (i.e not attached to any intf)
             * and not belongs to this intf.
             */
            continue;
        }

        if (u2Event == VPN_ADDR_DEL)
        {
            continue;
        }

        PolicyName.pu1_OctetList = pVpnPolicy->au1VpnPolicyName;
        PolicyName.i4_Length = (INT4) STRLEN (pVpnPolicy->au1VpnPolicyName);

        if (u2IsValidIntf == FALSE)
        {
            /* Disable all the policies since the interface is no more valid */
            nmhSetFsVpnPolicyRowStatus (&PolicyName, NOT_IN_SERVICE);
            continue;
        }

        if (Ip6AddrCompare (pVpnPolicy->LocalTunnTermAddr.uIpAddr.Ip6Addr,
                            NetIpv6AddrInfo.Ip6Addr) != IP6_ZERO)
        {
            /* WAN ineterface ip got changed; So propagate the change to NP */
            if (nmhSetFsVpnPolicyRowStatus (&PolicyName, NOT_IN_SERVICE) ==
                SNMP_SUCCESS)
            {
                if (nmhSetFsVpnPolicyIntfIndex (&PolicyName, (INT4) u4IfIndex)
                    != SNMP_SUCCESS)
                {
                    VpnDsUnLock ();
                    return (CFA_FAILURE);
                }

                if (pVpnPolicy->u1VpnMode == VPN_TRANSPORT)
                {
                    ProtectNetwork.pu1_OctetList =
                        NetIpv6AddrInfo.Ip6Addr.u1_addr;
                    ProtectNetwork.i4_Length = sizeof (tIp6Addr);
                    /* Update the local protected n/w to WAN ip */
                    if (nmhSetFsVpnLocalProtectNetwork (&PolicyName,
                                                        &ProtectNetwork) !=
                        SNMP_SUCCESS)
                    {
                        VpnDsUnLock ();
                        return (CFA_FAILURE);
                    }
                }
                nmhSetFsVpnPolicyRowStatus (&PolicyName, ACTIVE);
            }
        }
    }
    VpnDsUnLock ();

    return (CFA_SUCCESS);
}

/**************************************************************************/
/*  Function Name   : VpnUtilMatchAclDestIP                           */
/*  Description     : This function finds a match for an IP with the ra   */
/*                     address pool                                       */
/*  Input (s)       : u4QueryIpAddr - Ip Address.                         */
/*  Output(s)       : None.                                               */
/*  Returns         : OSIX_FAILURE/OSIX_SUCCESS.                          */
/**************************************************************************/
PUBLIC INT4
VpnUtilMatchAclDestIP (UINT4 u4QueryIpAddr, INT4 *pi4IfIndex)
{

    tVpnPolicy         *pVpnPolicy = NULL;
    UINT4               u4DestIpAddr = VPN_ZERO;
    UINT4               u4DestIpMask = VPN_ZERO;
    INT4                i4TunFlag = VPN_ZERO;

    /* If VPN is disabled, failure should be returned */
    nmhGetFsVpnGlobalStatus (&i4TunFlag);

    if (i4TunFlag == VPN_DISABLE)
    {
        return OSIX_FAILURE;
    }

    /* 
     * Scanning the policy list to get a match for the Query Ip Address
     */
    TMO_SLL_Scan (&VpnPolicyList, pVpnPolicy, tVpnPolicy *)
    {
        if (pVpnPolicy->u1VpnPolicyRowStatus != ACTIVE)

        {
            continue;
        }

        u4DestIpAddr = pVpnPolicy->RemoteProtectNetwork.IpAddr.uIpAddr.Ip4Addr;
        IPV4_MASKLEN_TO_MASK (u4DestIpMask,
                              pVpnPolicy->RemoteProtectNetwork.u4AddrPrefixLen);

        /* 
         * If the destination Ip network of access list matches with the 
         * Query Ip network and the mode is tunnel then for that query ip
         * sip should get as VPN type.
         */
        if ((u4QueryIpAddr & u4DestIpMask) == (u4DestIpAddr & u4DestIpMask))
        {
            if (pVpnPolicy->u4VpnPolicyFlag == VPN_APPLY)
            {
                if (pVpnPolicy->u1VpnMode == VPN_TUNNEL)
                {
                    *pi4IfIndex = (INT4) pVpnPolicy->u4IfIndex;
                    return (OSIX_SUCCESS);
                }
            }
        }
    }

    return OSIX_FAILURE;
}

/**************************************************************************/
/*  Function Name   : VpnIntfChangeCallBack                               */
/*  Description     : Call back function when the interface change happens*/
/*                                                                        */
/*  Input (s)       : pCfaRegInfo - Interface Information                 */
/*  Output(s)       : None.                                               */
/*  Returns         : OSIX_FAILURE/OSIX_SUCCESS.                          */
/**************************************************************************/
PUBLIC INT4
VpnIntfChangeCallBack (tCfaRegInfo * pCfaRegInfo)
{
    tVpnIntfInfoChgEvtMsg *pVpnIntfInfoChgEvtMsg = NULL;
    tCfaIfInfo          IfInfo;
    UINT1               u1Status = VPN_ZERO;

    MEMSET (&IfInfo, VPN_ZERO, sizeof (tCfaIfInfo));
    if (CfaGetIfInfo (pCfaRegInfo->u4IfIndex, &IfInfo) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    u1Status = pCfaRegInfo->uCfaRegParams.CfaInterfaceInfo.u1IfOperStatus;
    if ((u1Status == NOT_IN_SERVICE) ||
        (u1Status == ACTIVE) || (u1Status == DESTROY))
    {
        pVpnIntfInfoChgEvtMsg = (tVpnIntfInfoChgEvtMsg *)
            MemAllocMemBlk (VPN_Q_PID);
        if (pVpnIntfInfoChgEvtMsg != NULL)
        {
            pVpnIntfInfoChgEvtMsg->u4IfAddrType = IPVX_ADDR_FMLY_IPV4;
            pVpnIntfInfoChgEvtMsg->u4IfIndex = pCfaRegInfo->u4IfIndex;
            if (CfaGetIfIpAddr ((INT4) pCfaRegInfo->u4IfIndex,
                                &(pVpnIntfInfoChgEvtMsg->u4IpAddr)) ==
                OSIX_FAILURE)
            {
                MemReleaseMemBlock (VPN_Q_PID, (UINT1 *) pVpnIntfInfoChgEvtMsg);
                return OSIX_FAILURE;
            }
            pVpnIntfInfoChgEvtMsg->u1NwType = IfInfo.u1NwType;
            pVpnIntfInfoChgEvtMsg->u1WanType = IfInfo.u1WanType;

            if (u1Status == NOT_IN_SERVICE)
            {
                pVpnIntfInfoChgEvtMsg->u1Status = VPN_INTF_DOWN;
            }
            else if (u1Status == ACTIVE)
            {
                pVpnIntfInfoChgEvtMsg->u1Status = VPN_INTF_UP;
            }
            else if (u1Status == DESTROY)
            {
                pVpnIntfInfoChgEvtMsg->u1Status = VPN_INTF_DELETE;
            }

            if (VpnPostMsgToQAndNotify ((tOsixMsg *) pVpnIntfInfoChgEvtMsg,
                                        VPN_INTF_INFO_CHANGE_EVENT) ==
                VPN_FAILURE)
            {
                /* Release the memory allocated at the caller context */
                MemReleaseMemBlock (VPN_Q_PID, (UINT1 *) pVpnIntfInfoChgEvtMsg);
                return OSIX_FAILURE;
            }
        }
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/* EOF */
