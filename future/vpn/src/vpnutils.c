/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: vpnutils.c,v 1.36 2015/09/21 06:21:20 siva Exp $
 *
 * Description: File containing utilty functions
 * *******************************************************************/
#ifndef _VPN_UTILS_C__
#define _VPN_UTILS_C__

#ifdef SECMOD_WANTED
#include "vpnwincs.h"
#endif
# include "vpninc.h"
# include "cfa.h"
# include "fssnmp.h"
#include "cli.h"
#include "msr.h"
#include "seccli.h"
#include "vpncli.h"
#include "secv4.h"
#include "ip6util.h"
#include "sec.h"
#include "fips.h"

static UINT4 VpnGetIpHeaderLength PROTO ((tCRU_BUF_CHAIN_HEADER *));
static UINT2 VpnGetUdpLen PROTO ((tCRU_BUF_CHAIN_HEADER *));
static VOID VpnSetUdpLenChkSum PROTO ((tCRU_BUF_CHAIN_HEADER *, UINT2));
static UINT4 VpnIkeNattStripUdpHeader PROTO ((tCRU_BUF_CHAIN_HEADER *));
extern UINT2        Secv4IPCalcChkSum (UINT1 *pBuf, UINT4 u4Size);
UINT2               VpnGetIpTotalLen (tCRU_BUF_CHAIN_HEADER * pBuf);
UINT4               VpnGetEspSpi (tCRU_BUF_CHAIN_HEADER * pBuf);

static UINT4        gu4sVpnPolicyIndex = VPN_ONE;
unVpnCallBackEntry  gaVpnCallBack[VPN_MAX_CALLBACK_EVENTS];

/****VPN Utility functions *****/
tVpnPolicy         *
VpnSnmpGetPolicy (CONST UINT1 *pPolicyName, INT4 i4NameLen)
{
    tVpnPolicy         *pPolicy = NULL;

    if (pPolicyName == NULL)
    {
        return (tVpnPolicy *) NULL;
    }

    if ((UINT4) i4NameLen > STRLEN (pPolicyName))
    {
        return (tVpnPolicy *) NULL;
    }
    SLL_SCAN (&VpnPolicyList, pPolicy, tVpnPolicy *)
    {
        if (pPolicy != NULL)
        {
            if (STRNCASECMP (pPolicyName, pPolicy->au1VpnPolicyName, i4NameLen)
                == VPN_ZERO)
            {
                if ((UINT4) i4NameLen == STRLEN (pPolicy->au1VpnPolicyName))
                {
                    return (pPolicy);
                }
            }
        }
    }

    return (NULL);
}

/*
 * Function    :  VpnGetRaAddrPool
 * Description :  To fetch the ravpn address pool for the given name
 * Input       :  Address pool name and its length.
 * Returns     :  Address pool pointer on success
 */
tVpnRaAddressPool  *
VpnGetRaAddrPool (CONST UINT1 *pRaAddrPoolName, INT4 i4NameLen)
{
    tVpnRaAddressPool  *pVpnRaAddressPoolNode = NULL;

    if ((pRaAddrPoolName == NULL) || (i4NameLen <= VPN_ZERO))
    {
        return (NULL);
    }

    TMO_SLL_Scan (&gVpnRaAddressPoolList, pVpnRaAddressPoolNode,
                  tVpnRaAddressPool *)
    {
        if (STRNCASECMP (pVpnRaAddressPoolNode->au1VpnRaAddressPoolName,
                         pRaAddrPoolName, i4NameLen) == VPN_ZERO)
        {
            if ((UINT4) i4NameLen ==
                STRLEN (pVpnRaAddressPoolNode->au1VpnRaAddressPoolName))
            {
                return (pVpnRaAddressPoolNode);
            }
        }
    }

    return (NULL);
}

/*****************************************************************************/

INT4
VpnCheckMandatoryParams (tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName)
{
    tIp6Addr            NullIp6Addr;
    tVpnPolicy         *pVpnPolicy = NULL;
    tSNMP_OCTET_STRING_TYPE *pVpnRaUserName = NULL;
    UINT4               u4IpAddr = VPN_ZERO;
    UINT4               u4LocalMask = VPN_ZERO;
    UINT4               u4RemoteMask = VPN_ZERO;
    INT4                i4RAVPNServer = VPN_INVALID;
    INT1                i1RmtCheck = OSIX_FALSE;

    pVpnPolicy = VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                                   pFsVpnPolicyName->i4_Length);
    if (pVpnPolicy == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pVpnPolicy->u4VpnPolicyType == INVALID_VPN_PARAMETER)
    {
        CLI_SET_ERR (CLI_VPN_ERR_VPN_MODE);
        return SNMP_FAILURE;
    }

    if (pVpnPolicy->u1VpnMode == INVALID_VPN_PARAMETER)
    {
        CLI_SET_ERR (CLI_VPN_ERR_IPSEC_MODE);
        return SNMP_FAILURE;
    }

    if (((pVpnPolicy->LocalTunnTermAddr.u4AddrType == IPVX_ADDR_FMLY_IPV4) &&
         (MEMCMP (&(pVpnPolicy->LocalTunnTermAddr.uIpAddr.Ip4Addr),
                  &u4IpAddr, sizeof (tIp4Addr)) == VPN_ZERO)))
    {
        CLI_SET_ERR (CLI_VPN_INVLAID_INTF_PARAMS);
        return (SNMP_FAILURE);
    }

    if ((pVpnPolicy->LocalTunnTermAddr.u4AddrType == IPVX_ADDR_FMLY_IPV6) &&
        (MEMCMP (&(pVpnPolicy->LocalTunnTermAddr.uIpAddr.Ip6Addr),
                 &NullIp6Addr, sizeof (tIp6Addr)) == VPN_ZERO))
    {
        CLI_SET_ERR (CLI_VPN_INVLAID_INTF_PARAMS);
        return (SNMP_FAILURE);
    }

    if ((pVpnPolicy->u4VpnPolicyType != VPN_XAUTH) &&
        (pVpnPolicy->u4VpnPolicyType != VPN_IKE_RA_PRESHAREDKEY) &&
        (pVpnPolicy->u4VpnPolicyType != VPN_IKE_RA_CERT) &&
        (pVpnPolicy->u4VpnPolicyType != VPN_IKE_XAUTH_CERT))
    {
        i1RmtCheck = OSIX_TRUE;
    }

    nmhGetFsVpnRaServer (&i4RAVPNServer);
    if (((pVpnPolicy->u4VpnPolicyType == VPN_XAUTH) ||
         (pVpnPolicy->u4VpnPolicyType == VPN_IKE_RA_PRESHAREDKEY) ||
         (pVpnPolicy->u4VpnPolicyType == VPN_IKE_RA_CERT) ||
         (pVpnPolicy->u4VpnPolicyType == VPN_IKE_XAUTH_CERT)) &&
        (i4RAVPNServer == RAVPN_CLIENT))
    {
        i1RmtCheck = OSIX_TRUE;

        /* Check username.password is configured or not */
        if ((pVpnPolicy->u4VpnPolicyType == VPN_XAUTH) ||
            (pVpnPolicy->u4VpnPolicyType == VPN_IKE_XAUTH_CERT))
        {
            if ((pVpnRaUserName =
                 allocmem_octetstring (RA_USER_NAME_LENGTH + VPN_ONE)) == NULL)
            {
                return SNMP_FAILURE;
            }
            if (nmhGetFirstIndexFsVpnRaUsersTable (pVpnRaUserName) ==
                SNMP_FAILURE)
            {
                CLI_SET_ERR (CLI_VPN_ERR_RAVPN_USER_NOT_EXIST);
                free_octetstring (pVpnRaUserName);
                return SNMP_FAILURE;
            }
            free_octetstring (pVpnRaUserName);
        }
    }

    if (i1RmtCheck == OSIX_TRUE)
    {
        if ((pVpnPolicy->RemoteTunnTermAddr.u4AddrType == IPVX_ADDR_FMLY_IPV4)
            && (MEMCMP (&(pVpnPolicy->RemoteTunnTermAddr.uIpAddr.Ip4Addr),
                        &u4IpAddr, sizeof (tIp4Addr)) == VPN_ZERO))
        {
            CLI_SET_ERR (CLI_VPN_ERR_PEER_INFO);
            return (SNMP_FAILURE);
        }

        if ((pVpnPolicy->RemoteTunnTermAddr.u4AddrType == IPVX_ADDR_FMLY_IPV6)
            && (MEMCMP (&(pVpnPolicy->RemoteTunnTermAddr.uIpAddr.Ip6Addr),
                        &NullIp6Addr, sizeof (tIp6Addr)) == VPN_ZERO))
        {
            CLI_SET_ERR (CLI_VPN_ERR_PEER_INFO);
            return (SNMP_FAILURE);
        }
    }

    if (pVpnPolicy->u1VpnMode == VPN_TRANSPORT)
    {
        IPV4_MASKLEN_TO_MASK (u4LocalMask,
                              pVpnPolicy->LocalProtectNetwork.u4AddrPrefixLen);
        IPV4_MASKLEN_TO_MASK (u4RemoteMask,
                              pVpnPolicy->RemoteProtectNetwork.u4AddrPrefixLen);

        if ((pVpnPolicy->LocalTunnTermAddr.u4AddrType == IPVX_ADDR_FMLY_IPV4) &&
            (((pVpnPolicy->LocalProtectNetwork.IpAddr.uIpAddr.Ip4Addr &
               u4LocalMask) !=
              pVpnPolicy->LocalTunnTermAddr.uIpAddr.Ip4Addr) ||
             ((pVpnPolicy->RemoteProtectNetwork.IpAddr.uIpAddr.Ip4Addr &
               u4RemoteMask) !=
              pVpnPolicy->RemoteTunnTermAddr.uIpAddr.Ip4Addr)))
        {
            CLI_SET_ERR (CLI_VPN_ERR_ACL_PARAM);
            return SNMP_FAILURE;
        }
        else if ((pVpnPolicy->LocalTunnTermAddr.u4AddrType
                  == IPVX_ADDR_FMLY_IPV6) &&
                 ((Ip6AddrMatch (&pVpnPolicy->LocalTunnTermAddr.
                                 uIpAddr.Ip6Addr,
                                 &pVpnPolicy->LocalProtectNetwork.IpAddr.
                                 uIpAddr.Ip6Addr,
                                 (INT4) pVpnPolicy->LocalProtectNetwork.
                                 u4AddrPrefixLen) != TRUE)
                  ||
                  (Ip6AddrMatch
                   (&pVpnPolicy->RemoteTunnTermAddr.uIpAddr.Ip6Addr,
                    &pVpnPolicy->RemoteProtectNetwork.IpAddr.uIpAddr.Ip6Addr,
                    (INT4) pVpnPolicy->RemoteProtectNetwork.u4AddrPrefixLen) !=
                   TRUE)))
        {
            CLI_SET_ERR (CLI_VPN_ERR_ACL_PARAM);
            return SNMP_FAILURE;
        }
    }

    if (pVpnPolicy->u4VpnProtocol == INVALID_VPN_PARAMETER)
    {
        CLI_SET_ERR (CLI_VPN_ERR_ACL_PARAM);
        return SNMP_FAILURE;
    }

    if (pVpnPolicy->u4VpnPolicyType == VPN_IPSEC_MANUAL)
    {
        if (VpnCheckIpsecManualMandatoryParams (pVpnPolicy) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (VpnCheckIkeMandatoryParams (pVpnPolicy) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/*****************************************************************************/
INT4
VpnCheckIpsecManualMandatoryParams (tVpnPolicy * pVpnPolicy)
{
    if ((pVpnPolicy->uVpnKeyMode.IpsecManualKey.u4VpnAhOutboundSpi ==
         INVALID_VPN_PARAMETER) ||
        (pVpnPolicy->uVpnKeyMode.IpsecManualKey.u4VpnAhInboundSpi ==
         INVALID_VPN_PARAMETER))
    {
        CLI_SET_ERR (CLI_VPN_ERR_MAN_SPI_PARAM);
        return SNMP_FAILURE;
    }
 
    if ( VpnAccessListCheckIpMask (pVpnPolicy->LocalProtectNetwork.u4AddrPrefixLen,
                             pVpnPolicy->LocalTunnTermAddr.uIpAddr.Ip4Addr,
                             pVpnPolicy->RemoteProtectNetwork.u4AddrPrefixLen,
                             pVpnPolicy->RemoteTunnTermAddr.uIpAddr.Ip4Addr) == SNMP_FAILURE )
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/*****************************************************************************/
INT4
VpnCheckIkeMandatoryParams (tVpnPolicy * pVpnPolicy)
{
    tUtlIn6Addr         In6Addr;
    tSNMP_OCTET_STRING_TYPE PolicyName;
    tSNMP_OCTET_STRING_TYPE VpnId;
    UINT1               au1PeerIdValue[VPN_MAX_NAME_LENGTH + VPN_ONE]
        = { VPN_ZERO };
    INT4                i4IdType = VPN_ZERO;
    INT4                i4IdStatus = NOT_READY;
    CHR1               *pu1Str = NULL;
    INT4                i4LifeTime = VPN_ZERO;
    tSNMP_OCTET_STRING_TYPE *pVpnRaAddressPoolName = NULL;
    tVpnRaAddressPool  *pVpnRaAddressPoolNode = NULL;
    INT4                i4RAVPNServer = VPN_INVALID;
    INT4                i4AuthType = VPN_INVALID;
    UINT4               u4Mask = VPN_ZERO;
    tIp6Addr            NullIp6Addr = { {{VPN_ZERO}} };

    MEMSET (au1PeerIdValue, VPN_ZERO, (VPN_MAX_NAME_LENGTH + VPN_ONE));
    if ((pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase1.u4HashAlgo != VPN_HMACMD5)
        && (pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase1.u4HashAlgo !=
            VPN_HMACSHA1)
        && (pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase1.u4HashAlgo !=
            HMAC_SHA_256)
        && (pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase1.u4HashAlgo !=
            HMAC_SHA_384)
        && (pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase1.u4HashAlgo !=
            HMAC_SHA_512))
    {
        CLI_SET_ERR (CLI_VPN_ERR_PHASEI_INFO);
        return SNMP_FAILURE;
    }
    if ((pVpnPolicy->u1VpnIkeVer == CLI_VPN_IKE_V2) &&
        (pVpnPolicy->u4VpnPolicyType == VPN_XAUTH))
    {
        CLI_SET_ERR (CLI_VPN_ERR_V2_XAUTH_FAILED);
        return SNMP_FAILURE;
    }

   if ( VpnAccessListCheckIpMask (pVpnPolicy->LocalProtectNetwork.u4AddrPrefixLen, 
                             pVpnPolicy->LocalTunnTermAddr.uIpAddr.Ip4Addr,
                             pVpnPolicy->RemoteProtectNetwork.u4AddrPrefixLen, 
                             pVpnPolicy->RemoteTunnTermAddr.uIpAddr.Ip4Addr) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

#ifndef SECURITY_KERNEL_WANTED
    /* In FIPS mode, unsecure IKE Aggressive mode should not be used */
    if (FIPS_MODE == FipsGetFipsCurrOperMode ())
    {
        if ((pVpnPolicy->u1VpnIkeVer == CLI_VPN_IKE_V1) &&
            (pVpnPolicy->u4VpnPolicyType == VPN_IKE_RA_PRESHAREDKEY) &&
            (VPN_POLICY_PHASE1_EXCHG_MODE (pVpnPolicy) ==
             VPN_IKE_MODE_AGGRESSIVE))
        {
            CLI_SET_ERR (CLI_VPN_ERR_FIPS_AGGRESSIVE_MODE);
            return SNMP_FAILURE;
        }
    }
#endif

    nmhGetFsVpnRaServer (&i4RAVPNServer);
    if ((pVpnPolicy->u4VpnPolicyType == VPN_XAUTH) ||
        (pVpnPolicy->u4VpnPolicyType == VPN_IKE_RA_PRESHAREDKEY) ||
        (pVpnPolicy->u4VpnPolicyType == VPN_IKE_RA_CERT) ||
        (pVpnPolicy->u4VpnPolicyType == VPN_IKE_XAUTH_CERT))
    {
        if (i4RAVPNServer == RAVPN_SERVER)
        {
            pVpnRaAddressPoolName =
                allocmem_octetstring (RA_ADDRESS_POOL_NAME_LENGTH + VPN_ONE);
            if (pVpnRaAddressPoolName == NULL)
            {
                return SNMP_FAILURE;
            }

            if (nmhGetFirstIndexFsVpnRaAddressPoolTable (pVpnRaAddressPoolName)
                == SNMP_FAILURE)
            {
                CLI_SET_ERR (CLI_VPN_ERR_NO_RAVPN_POOL);
                free_octetstring (pVpnRaAddressPoolName);
                return SNMP_FAILURE;
            }

            pVpnRaAddressPoolNode =
                VpnGetRaAddrPool (pVpnRaAddressPoolName->pu1_OctetList,
                                  pVpnRaAddressPoolName->i4_Length);

            if (pVpnRaAddressPoolNode == NULL)
            {
                free_octetstring (pVpnRaAddressPoolName);
                return OSIX_FAILURE;
            }

            if (pVpnPolicy->RemoteProtectNetwork.IpAddr.u4AddrType ==
                IPVX_ADDR_FMLY_IPV4)
            {
                IPV4_MASKLEN_TO_MASK (u4Mask,
                                      pVpnRaAddressPoolNode->
                                      u4VpnRaAddressPoolPrefixLen);
                if ((pVpnPolicy->RemoteProtectNetwork.IpAddr.uIpAddr.Ip4Addr
                     != VPN_ZERO) &&
                    ((pVpnRaAddressPoolNode->VpnRaAddressPoolStart.uIpAddr.
                      Ip4Addr & u4Mask) !=
                     pVpnPolicy->RemoteProtectNetwork.IpAddr.uIpAddr.Ip4Addr))
                {
                    CLI_SET_ERR (CLI_VPN_ERR_INVALID_RAVPN_ACL);
                    free_octetstring (pVpnRaAddressPoolName);
                    return SNMP_FAILURE;
                }
                /* Source is the local network and
                 * and Destination is the remote client network */
                if ((pVpnPolicy->LocalProtectNetwork.IpAddr.uIpAddr.Ip4Addr ==
                     VPN_ZERO)
                    && (pVpnPolicy->RemoteProtectNetwork.IpAddr.uIpAddr.
                        Ip4Addr == VPN_ZERO))
                {
                    CLI_SET_ERR (CLI_VPN_ERR_ACL_PARAM);
                    free_octetstring (pVpnRaAddressPoolName);
                    return SNMP_FAILURE;
                }

            }
            else if (pVpnPolicy->RemoteProtectNetwork.IpAddr.u4AddrType ==
                     IPVX_ADDR_FMLY_IPV6)
            {
                if ((MEMCMP (&pVpnPolicy->RemoteProtectNetwork.IpAddr.
                             uIpAddr.Ip6Addr, &NullIp6Addr,
                             sizeof (tIp6Addr)) != IP6_ZERO)
                    &&
                    (Ip6AddrMatch
                     (&
                      (pVpnRaAddressPoolNode->VpnRaAddressPoolStart.uIpAddr.
                       Ip6Addr),
                      &(pVpnPolicy->RemoteProtectNetwork.IpAddr.uIpAddr.
                        Ip6Addr),
                      (INT4) pVpnRaAddressPoolNode->
                      u4VpnRaAddressPoolPrefixLen) != VPN_SUCCESS))
                {
                    CLI_SET_ERR (CLI_VPN_ERR_INVALID_RAVPN_ACL);
                    free_octetstring (pVpnRaAddressPoolName);
                    return SNMP_FAILURE;
                }
            }
            free_octetstring (pVpnRaAddressPoolName);
        }
    }

    if ((pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase2.u1EncryptionAlgo
         == VPN_ZERO) &&
        (pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase2.u1AuthAlgo == VPN_ZERO))
    {
        CLI_SET_ERR (CLI_VPN_ERR_PHASEII_INFO);
        return SNMP_FAILURE;
    }

    if ((pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase1.PolicyPeerID.i2IdType ==
         INVALID_VPN_PARAMETER) ||
        (pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase1.PolicyLocalID.i2IdType ==
         INVALID_VPN_PARAMETER) ||
        (pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase1.PolicyPeerID.u2Length
         == VPN_ZERO)
        || (pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase1.PolicyLocalID.
            u2Length == VPN_ZERO))

    {
        if (pVpnPolicy->u4VpnPolicyType == VPN_IKE_PRESHAREDKEY)
        {
            /* Default id type is IPv4; So do it if not exists for site2site */
            PolicyName.pu1_OctetList = pVpnPolicy->au1VpnPolicyName;
            PolicyName.i4_Length = (INT4) STRLEN (pVpnPolicy->au1VpnPolicyName);

            if (pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase1.PolicyLocalID.
                u2Length == VPN_ZERO)
            {
                if (pVpnPolicy->LocalTunnTermAddr.u4AddrType ==
                    IPVX_ADDR_FMLY_IPV4)
                {
                    pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase1.PolicyLocalID.
                        i2IdType = VPN_ID_TYPE_IPV4;
                    CLI_CONVERT_IPADDR_TO_STR (pu1Str,
                                               pVpnPolicy->LocalTunnTermAddr.
                                               uIpAddr.Ip4Addr);
                    STRNCPY (au1PeerIdValue, pu1Str, VPN_MAX_NAME_LENGTH);
                    VpnId.pu1_OctetList = au1PeerIdValue;
                    VpnId.i4_Length = (INT4) STRLEN (au1PeerIdValue);
                }
                else if (pVpnPolicy->LocalTunnTermAddr.u4AddrType
                         == IPVX_ADDR_FMLY_IPV6)
                {
                    pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase1.PolicyLocalID.
                        i2IdType = VPN_ID_TYPE_IPV6;

                    MEMCPY (&In6Addr,
                            &pVpnPolicy->LocalTunnTermAddr.uIpAddr.Ip6Addr,
                            sizeof (tIp6Addr));

                    VpnId.pu1_OctetList = (UINT1 *) INET_NTOA6 (In6Addr);
                    VpnId.i4_Length = (INT4) STRLEN (VpnId.pu1_OctetList);;
                }

                nmhSetFsVpnIkePhase1LocalIdType (&PolicyName,
                                                 pVpnPolicy->uVpnKeyMode.
                                                 VpnIkeDb.IkePhase1.
                                                 PolicyLocalID.i2IdType);
                nmhSetFsVpnIkePhase1LocalIdValue (&PolicyName, &VpnId);
            }
            if (pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase1.PolicyPeerID.
                u2Length == VPN_ZERO)
            {
                if (pVpnPolicy->RemoteTunnTermAddr.u4AddrType ==
                    IPVX_ADDR_FMLY_IPV4)
                {
                    pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase1.PolicyPeerID.
                        i2IdType = VPN_ID_TYPE_IPV4;
                    CLI_CONVERT_IPADDR_TO_STR (pu1Str,
                                               pVpnPolicy->RemoteTunnTermAddr.
                                               uIpAddr.Ip4Addr);
                    STRNCPY (au1PeerIdValue, pu1Str, VPN_MAX_NAME_LENGTH);
                    VpnId.pu1_OctetList = au1PeerIdValue;
                    VpnId.i4_Length = (INT4) STRLEN (au1PeerIdValue);
                }
                else if (pVpnPolicy->RemoteTunnTermAddr.u4AddrType
                         == IPVX_ADDR_FMLY_IPV6)
                {
                    pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase1.PolicyPeerID.
                        i2IdType = VPN_ID_TYPE_IPV6;

                    MEMCPY (&In6Addr, &pVpnPolicy->RemoteTunnTermAddr.
                            uIpAddr.Ip6Addr, sizeof (tIp6Addr));

                    VpnId.pu1_OctetList = (UINT1 *) INET_NTOA6 (In6Addr);
                    VpnId.i4_Length = (INT4) STRLEN (VpnId.pu1_OctetList);
                }

                nmhSetFsVpnIkePhase1PeerIdType (&PolicyName,
                                                pVpnPolicy->uVpnKeyMode.
                                                VpnIkeDb.IkePhase1.PolicyPeerID.
                                                i2IdType);
                nmhSetFsVpnIkePhase1PeerIdValue (&PolicyName, &VpnId);
            }
        }
        else
        {
            CLI_SET_ERR (CLI_VPN_ERR_ID_INFO);
            return SNMP_FAILURE;
        }
    }

    /* Only existing peer ids can be used in the policy */
    i4IdType = pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase1.PolicyPeerID.i2IdType;
    if (i4IdType != VPN_ZERO)
    {
        VpnId.pu1_OctetList = au1PeerIdValue;
        VpnId.i4_Length = (INT4) STRLEN (au1PeerIdValue);
        PolicyName.pu1_OctetList = pVpnPolicy->au1VpnPolicyName;
        PolicyName.i4_Length = (INT4) STRLEN (pVpnPolicy->au1VpnPolicyName);
        nmhGetFsVpnIkePhase1PeerIdValue (&PolicyName, &VpnId);

        if (nmhValidateIndexInstanceFsVpnRemoteIdTable (i4IdType, &VpnId) ==
            SNMP_SUCCESS)
        {
            if (SNMP_FAILURE ==
                nmhGetFsVpnRemoteIdStatus (i4IdType, &VpnId, &i4IdStatus))
            {
                CLI_SET_ERR (CLI_VPN_ERR_REMOTE_ID_NOT_EXIST);
                return (SNMP_FAILURE);
            }
            nmhGetFsVpnRemoteIdAuthType (i4IdType, &VpnId, &i4AuthType);
        }
        switch (pVpnPolicy->u4VpnPolicyType)
        {
            case VPN_XAUTH:
            case VPN_IKE_RA_PRESHAREDKEY:
            case VPN_IKE_PRESHAREDKEY:
            {
                if (i4AuthType != CLI_VPN_PRESHARED)
                {
                    CLI_SET_ERR (CLI_VPN_ERR_REMOTE_ID_DIFF_AUTH);
                    return (SNMP_FAILURE);
                }
                break;
            }
            case VPN_IKE_RA_CERT:
            case VPN_IKE_XAUTH_CERT:
            case VPN_IKE_CERTIFICATE:
            {
                if (i4AuthType != CLI_VPN_CERT)
                {
                    CLI_SET_ERR (CLI_VPN_ERR_REMOTE_ID_DIFF_AUTH);
                    return (SNMP_FAILURE);
                }
                break;
            }
            default:
                break;
        }
    }

    /* Verify the life time as type and value can be set individually */
    i4LifeTime = (INT4) VPN_POLICY_PHASE1_LTIME_VALUE (pVpnPolicy);
    VPN_CONVERT_LIFETIME_TO_SECS (VPN_POLICY_PHASE1_LTIME_TYPE (pVpnPolicy),
                                  i4LifeTime);
    if ((i4LifeTime < VPN_MIN_LIFETIME_SECS)
        || (i4LifeTime > VPN_PHASE1_MAX_LIFETIME_SECS))
    {
        CLI_SET_ERR (CLI_VPN_ERR_IKE_LTIME_INFO);
        return (SNMP_FAILURE);
    }

    i4LifeTime = (INT4) VPN_POLICY_PHASE2_LTIME_VALUE (pVpnPolicy);

    if (VPN_POLICY_PHASE2_LTIME_TYPE (pVpnPolicy) == VPN_LIFE_TYPE_KB)
    {
        if ((i4LifeTime < VPN_MIN_LIFETIME_KB)
            || (i4LifeTime > VPN_MAX_LIFETIME_KB))
        {
            CLI_SET_ERR (CLI_VPN_ERR_IPSEC_LTIME_INFO);
            return (SNMP_FAILURE);
        }
    }
    else
    {
        VPN_CONVERT_LIFETIME_TO_SECS (VPN_POLICY_PHASE2_LTIME_TYPE (pVpnPolicy),
                                      i4LifeTime);
        if ((i4LifeTime < VPN_MIN_LIFETIME_SECS)
            || (i4LifeTime > VPN_PHASE2_MAX_LIFETIME_SECS))
        {
            CLI_SET_ERR (CLI_VPN_ERR_IPSEC_LTIME_INFO);
            return (SNMP_FAILURE);
        }
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  VpnRaDBAddUser
 Input       :  pVpnRaUserInfoNode
 Description :  Adds a new user to Remote access Databse 
 Returns     :  NONE
****************************************************************************/
VOID
VpnRaDBAddUser (tVpnRaUserInfo * pVpnRaUserInfoNode,
                tSNMP_OCTET_STRING_TYPE * pFsVpnRaUserName)
{
    tVpnRaUserInfo     *pTmpVpnRaUserName = NULL;
    tVpnRaUserInfo     *pPrevVpnRaUserName = NULL;

    TMO_SLL_Scan (&gVpnRaUserList, pTmpVpnRaUserName, tVpnRaUserInfo *)
    {
        if (STRCMP (pTmpVpnRaUserName->au1VpnRaUserName,
                    pFsVpnRaUserName->pu1_OctetList) > VPN_ZERO)
        {
            break;
        }
        pPrevVpnRaUserName = pTmpVpnRaUserName;
    }

    TMO_SLL_Init_Node (&pVpnRaUserInfoNode->VpnRaUserInfo);
    MEMSET (pVpnRaUserInfoNode->au1VpnRaUserName, VPN_ZERO,
            sizeof (pVpnRaUserInfoNode->au1VpnRaUserName));
    MEMSET (pVpnRaUserInfoNode->au1VpnRaUserSecret, VPN_ZERO,
            sizeof (pVpnRaUserInfoNode->au1VpnRaUserSecret));
    MEMCPY (pVpnRaUserInfoNode->au1VpnRaUserName,
            pFsVpnRaUserName->pu1_OctetList, pFsVpnRaUserName->i4_Length);
    pVpnRaUserInfoNode->i4RowStatus = NOT_READY;
    TMO_SLL_Insert (&gVpnRaUserList,
                    (tTMO_SLL_NODE *) pPrevVpnRaUserName,
                    (tTMO_SLL_NODE *) pVpnRaUserInfoNode);

}

/****************************************************************************
 Function    :  VpnRaDBAddAddressPool
 Input       :  pVpnRaUserInfoNode
 Description :  Adds a new Address pool to Remote access Databse 
 Returns     :  NONE
****************************************************************************/
VOID
VpnRaDBAddAddressPool (tVpnRaAddressPool * pVpnRaAddressPoolNode,
                       tSNMP_OCTET_STRING_TYPE * pFsVpnRaAddressPoolName)
{
    tVpnRaAddressPool  *pTmpVpnRaAddressPoolNode = NULL;
    tVpnRaAddressPool  *pPrevVpnRaAddressPoolNode = NULL;

    TMO_SLL_Scan (&gVpnRaAddressPoolList, pTmpVpnRaAddressPoolNode,
                  tVpnRaAddressPool *)
    {
        if (STRNCASECMP (pTmpVpnRaAddressPoolNode->au1VpnRaAddressPoolName,
                         pFsVpnRaAddressPoolName->pu1_OctetList,
                         pFsVpnRaAddressPoolName->i4_Length) > VPN_ZERO)
        {
            break;
        }

        pPrevVpnRaAddressPoolNode = pTmpVpnRaAddressPoolNode;
    }

    TMO_SLL_Init_Node (&pVpnRaAddressPoolNode->VpnRaAddressPool);
    MEMSET (pVpnRaAddressPoolNode->au1VpnRaAddressPoolName, VPN_ZERO,
            sizeof (pVpnRaAddressPoolNode->au1VpnRaAddressPoolName));
    MEMCPY (pVpnRaAddressPoolNode->au1VpnRaAddressPoolName,
            pFsVpnRaAddressPoolName->pu1_OctetList,
            pFsVpnRaAddressPoolName->i4_Length);
    MEMSET (&(pVpnRaAddressPoolNode->VpnRaAddressPoolStart), VPN_ZERO,
            sizeof (tVpnIpAddr));
    MEMSET (&(pVpnRaAddressPoolNode->VpnRaAddressPoolEnd), VPN_ZERO,
            sizeof (tVpnIpAddr));
    pVpnRaAddressPoolNode->u4VpnRaAddressPoolPrefixLen = VPN_ZERO;
    pVpnRaAddressPoolNode->i4RowStatus = VPN_STATUS_NOT_IN_SERVICE;
    TMO_SLL_Insert (&gVpnRaAddressPoolList,
                    (tTMO_SLL_NODE *) pPrevVpnRaAddressPoolNode,
                    (tTMO_SLL_NODE *) pVpnRaAddressPoolNode);

    /*Initialize the Allocated Address Pool List */
    TMO_SLL_Init ((tTMO_SLL *) & pVpnRaAddressPoolNode->VpnAllocAddrList);
}

/****************************************************************************
 Function    :  VpnUtilValidateEmailAddr

 Input       :  pEmailId - Email Id as a "OCTET_STRING" With proper Length.

 Description :  Function echeck is used to verify if the given value is a 
                possible valid email address. 
                1. Makes sure the email address has one (@), atleast one (.).
                2. It also makes sure that there are no spaces, extra '@'s.
                3. It also makes sure that there is atleast one (.) after the @.

 Returns     :  SNMP_FAILURE/SNMP_SUCCESS
 ***************************************************************************/
INT4
VpnUtilValidateEmailAddr (tSNMP_OCTET_STRING_TYPE * pEmailId)
{
    INT1                i1Dot = '.';
    INT1                i1At = '@';
    INT1                i1Space = ' ';
    UINT2               u2Count = VPN_ZERO;

    UINT2               u2AtCnt = VPN_ZERO;
    UINT2               u2DotCnt = VPN_ZERO;

    UINT2               u2AtPos = VPN_ZERO;
    UINT2               u2DotPos = VPN_ZERO;

    INT4                i4Len = VPN_ZERO;
    INT1               *pu1Email = NULL;

    if ((pEmailId == NULL) || (pEmailId->pu1_OctetList == NULL))
    {
        return (SNMP_FAILURE);
    }

    i4Len = pEmailId->i4_Length;
    pu1Email = (INT1 *) pEmailId->pu1_OctetList;

    while (i4Len > u2Count)
    {
        /*
         * For Basic Check 
         * 1. The char should Not be a Space. 
         * 2. The char may be a "_,-". 
         * 3. The char may be a "@". 
         * 4. The char may be a ".". 
         * 5. The char may be a "DIGIT". 
         * 6. The char may be a "ALPHABETIC". 
         */
        if ((*(pu1Email + u2Count) == i1Space) ||
            (!((ISALPHA ((*(pu1Email + u2Count))) != VPN_ZERO) ||
               (ISDIGIT ((*(pu1Email + u2Count))) != VPN_ZERO) ||
               ((*(pu1Email + u2Count) == i1At)) ||
               ((*(pu1Email + u2Count) == i1Dot)) ||
               ((*(pu1Email + u2Count) == '-')) ||
               ((*(pu1Email + u2Count) == '_')))))
        {
            return (SNMP_FAILURE);
        }

        if (*(pu1Email + u2Count) == i1Dot)
        {
            u2DotCnt++;
            u2DotPos = u2Count;

            if ((u2Count == VPN_ZERO) || (u2Count == i4Len - VPN_ONE) ||
                (*(pu1Email + u2Count + VPN_ONE) == i1Dot) ||
                (*(pu1Email + u2Count + VPN_ONE) == i1At))
            {
                return (SNMP_FAILURE);
            }
        }
        else if (*(pu1Email + u2Count) == i1At)
        {
            u2AtCnt++;
            u2AtPos = u2Count;

            if ((u2Count == VPN_ZERO) ||
                (u2Count == i4Len - VPN_ONE) ||
                (u2AtCnt > VPN_ONE) ||
                (*(pu1Email + u2Count + VPN_ONE) == i1Dot))
            {
                return (SNMP_FAILURE);
            }
        }
        u2Count++;
    }

    if ((u2AtCnt == VPN_ZERO) || (u2DotCnt == VPN_ZERO) || (u2DotPos < u2AtPos))
    {
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************/
/* Function    :  VpnUtilGetConfigTunnels                                   */
/* Input       :  None.                                                     */
/*                                                                          */
/* Output      :  Number of Vpn Tunnels Configured.                         */
/* Returns     :  SNMP_SUCCESS                                              */
/****************************************************************************/
INT1
VpnUtilGetConfigTunnels (UINT4 *pu4RetValFsVpnConfigTunnels)
{
    *pu4RetValFsVpnConfigTunnels = TMO_SLL_Count (&VpnPolicyList);
    return (SNMP_SUCCESS);
}

/**************************************************************************/
/*  Function Name   : VpnUtilInitPolicyConfig                             */
/*  Description     : This function initialises the tVpnPolicy Structure  */
/*                    new Node with Ike params.                           */
/*                    Will be called whenver a policy gets created        */
/*  Input (s)       : pVpnPolicy   - New Policy Node.                     */
/*  Output(s)       : None.                                               */
/*  Returns         : None.                                               */
/**************************************************************************/
VOID
VpnUtilInitPolicyConfig (tVpnPolicy * pVpnPolicy)
{
    tIkePhase1         *pIkePhase1 = NULL;

    MEMSET (pVpnPolicy, VPN_ZERO, sizeof (tVpnPolicy));

    /* manual, ike, xauth */
    pVpnPolicy->u4VpnPolicyType = VPN_IKE_PRESHAREDKEY;
    pVpnPolicy->u1VpnMode = VPN_TUNNEL;    /* tunnel or transport */
    pVpnPolicy->u4VpnSecurityProtocol = SEC_ESP;    /* AH or ESP */
    /* tcp, udp, icmp, any, etc. */
    pVpnPolicy->u4VpnProtocol = VPN_ANY_PROTOCOL;
    /* applied or bypassed or filtered */
    pVpnPolicy->u4VpnPolicyFlag = VPN_APPLY;
    pVpnPolicy->u4VpnPolicyPriority = VPN_MIN_PRIORITY;

    /*******  IKE Phase I Proposal ******/
    pIkePhase1 = &(pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase1);
    pIkePhase1->u4HashAlgo = VPN_HMACSHA1;    /* md5|sha */
    pIkePhase1->u4EncryptionAlgo = VPN_DES_CBC;    /* des|3des|aes */
    pIkePhase1->u4DHGroup = VPN_DH_GROUP2;    /* Diffie-Helman Group 1|2|5 */
    pIkePhase1->u4Mode = VPN_IKE_MODE_MAIN;    /* Main | Aggressive */
    VPN_POLICY_PHASE1_LTIME_TYPE (pVpnPolicy) = VPN_DEFAULT_IKE_LTIME_TYPE;
    VPN_POLICY_PHASE1_LTIME_VALUE (pVpnPolicy) = VPN_DEFAULT_IKE_LTIME_VALUE;

    /* Default IPSec parameters */
    VPN_POLICY_PHASE2_LTIME_TYPE (pVpnPolicy) = VPN_DEFAULT_IPSEC_LTIME_TYPE;
    VPN_POLICY_PHASE2_LTIME_VALUE (pVpnPolicy) = VPN_DEFAULT_IPSEC_LTIME_VALUE;
    VPN_POLICY_ANTI_REPLAY_STATUS (pVpnPolicy) = (UINT1) VPN_ANTI_REPLAY_ENABLE;

    pVpnPolicy->u1VpnIkeVer = CLI_VPN_IKE_V1;
    pVpnPolicy->u1VpnPolicyRowStatus = NOT_READY;
    pVpnPolicy->u4VpnPolicyIndex = gu4sVpnPolicyIndex;
    pVpnPolicy->LocalProtectNetwork.u2StartPort = VPN_ZERO;
    pVpnPolicy->LocalProtectNetwork.u2EndPort = VPN_MAX_SRC_DST_PORT_VALUE;
    pVpnPolicy->RemoteProtectNetwork.u2StartPort = VPN_ZERO;
    pVpnPolicy->RemoteProtectNetwork.u2EndPort = VPN_MAX_SRC_DST_PORT_VALUE;

    gu4sVpnPolicyIndex = gu4sVpnPolicyIndex + VPN_TWO;
}

/****************************************************************************/
/*  Function    :  VpnGetRemoteIdInfo                                       */
/*  Description :  Gives the remote identity information matching the given */
/*                 parameters.                                              */
/*  Input       :  Identity type, value and it's length                     */
/*  Returns     :  Id node pointer on success                               */
/* **************************************************************************/
tVpnIdInfo         *
VpnGetRemoteIdInfo (INT4 i4IdType, CONST UINT1 *pu1IdValue, INT4 i4IdLen)
{
    tVpnIdInfo         *pVpnIdInfoNode = NULL;

    if ((pu1IdValue == NULL) || (i4IdLen <= VPN_ZERO))
    {
        return (NULL);
    }

    TMO_SLL_Scan (&gVpnRemoteIdList, pVpnIdInfoNode, tVpnIdInfo *)
    {
        if (i4IdType == (INT4) VPN_ID_TYPE (pVpnIdInfoNode))
        {
            if (STRNCASECMP
                (VPN_ID_VALUE (pVpnIdInfoNode),
                 pu1IdValue, i4IdLen) == VPN_ZERO)
            {
                if ((UINT4) i4IdLen == STRLEN (VPN_ID_VALUE (pVpnIdInfoNode)))
                {
                    return (pVpnIdInfoNode);
                }
            }
        }
    }

    return (NULL);
}

/****************************************************************************/
/*  Function    :  VpnGetCertInfoFromKey                                    */
/*  Description :  Gives the certificate information matching the given key */
/*                 string.                                                  */
/*  Input       :  Identity value and it's length                           */
/*  Returns     :  Id node pointer on success                               */
/* **************************************************************************/
tVpnCertInfo       *
VpnGetCertInfoFromKey (CONST UINT1 *pu1IdValue, INT4 i4IdLen)
{
    tVpnCertInfo       *pVpnCertInfoNode = NULL;

    if ((pu1IdValue == NULL) || (i4IdLen <= VPN_ZERO))
    {
        return (NULL);
    }

    TMO_SLL_Scan (&gVpnCertInfoList, pVpnCertInfoNode, tVpnCertInfo *)
    {
        if (STRNCASECMP
            (VPN_CERT_KEY_VALUE (pVpnCertInfoNode),
             pu1IdValue, i4IdLen) == VPN_ZERO)
        {
            if ((UINT4) i4IdLen ==
                STRLEN (VPN_CERT_KEY_VALUE (pVpnCertInfoNode)))
            {
                return (pVpnCertInfoNode);
            }
        }
    }

    return (NULL);
}

/****************************************************************************/
/*  Function    :  VpnGetCaCertInfoFromKey                                  */
/*  Description :  Gives the CA certificate information matching the given  */
/*                 key string.                                              */
/*  Input       :  Identity value and it's length                           */
/*  Returns     :  Id node pointer on success                               */
/* **************************************************************************/
tVpnCaCertInfo     *
VpnGetCaCertInfoFromKey (CONST UINT1 *pu1IdValue, INT4 i4IdLen)
{
    tVpnCaCertInfo     *pVpnCaCertInfoNode = NULL;

    if ((pu1IdValue == NULL) || (i4IdLen <= VPN_ZERO))
    {
        return (NULL);
    }

    TMO_SLL_Scan (&gVpnCaCertInfoList, pVpnCaCertInfoNode, tVpnCaCertInfo *)
    {
        if (STRNCASECMP
            (VPN_CA_CERT_KEY_VALUE (pVpnCaCertInfoNode),
             pu1IdValue, i4IdLen) == VPN_ZERO)
        {
            if ((UINT4) i4IdLen ==
                STRLEN (VPN_CA_CERT_KEY_VALUE (pVpnCaCertInfoNode)))
            {
                return (pVpnCaCertInfoNode);
            }
        }
    }

    return (NULL);
}

/*
 * Function    : VpnUtilsIsFqdn
 * Description : A fully qualified domain name (or FQDN) is an unambiguous
 *               domain name that specifies the node's position in the DNS
 *               tree hierarchy absolutely. To distinguish an FQDN from a
 *               regular domain name, a trailing period is added.
 *               ex: ranga.cas.com.
 *               This function verifies the given string and says it is a
 *               FQDN or not.
 * Input       : String to be validated against FQDN rules and its length.
 * Output      : None
 * Returns     : TRUE if the string is FQDN else FALSE
 */

INT4
VpnUtilsIsFqdn (CONST CHR1 * pc1Fqdn, INT4 i4Len)
{
    CHR1                i1Dot = '.';
    CHR1                i1Hiphen = '-';
    INT4                i4DotCount = VPN_ZERO;
    INT4                i4Count = VPN_ZERO;

    if (i4Len <= VPN_ZERO)
    {
        /* empty string */
        return (FALSE);
    }

    if (pc1Fqdn[i4Len - VPN_ONE] != i1Dot)
    {
        /* should end with dot(.) */
        return (FALSE);
    }

    if (TRUE == !ISALPHA (pc1Fqdn[VPN_INDEX_0]))
    {
        /* should start with alphabet */
        return (FALSE);
    }

    while (i4Count < i4Len)
    {
        if ((TRUE == !(isalnum (pc1Fqdn[i4Count])))
            && (pc1Fqdn[i4Count] != i1Hiphen) &&
            (pc1Fqdn[i4Count] != i1Dot ? TRUE : !(++i4DotCount)))
        {
            /* should contain only [alpha] [digit] [-.] */
            return (FALSE);
        }

        i4Count++;
    }

    if (i4DotCount < VPN_TWO)
    {
        /* Atleast two dots should present to represent host and domain */
        return (FALSE);
    }

    return (TRUE);
}

/*
 * Function    :  VpnGetRaVpnUserInfo
 * Description :  Provides the information present in the remote access
 *                vpn user database corresponding to the given user.
 * Input       :  User name and its length to search in db.
 * Returns     :  User node pointer on success
 */
tVpnRaUserInfo     *
VpnGetRaVpnUserInfo (CONST UINT1 *pu1UserName, INT4 i4NameLen)
{
    tVpnRaUserInfo     *pVpnRaUserNode = NULL;

    if ((pu1UserName == NULL) || (i4NameLen <= VPN_ZERO))
    {
        return (NULL);
    }

    TMO_SLL_Scan (&gVpnRaUserList, pVpnRaUserNode, tVpnRaUserInfo *)
    {
        if (STRNCASECMP
            (VPN_RA_USER_NAME (pVpnRaUserNode), pu1UserName, i4NameLen)
            == VPN_ZERO)
        {
            if ((UINT4) i4NameLen == STRLEN (VPN_RA_USER_NAME (pVpnRaUserNode)))
            {
                return (pVpnRaUserNode);
            }
        }
    }

    return (NULL);
}

/**************************************************************************
 * Function    :  VpnSetCertInfoRefCnt
 * Description :  Gives the remote identity information matching the given
 *                parameters.
 * Input       :  Identity type, value and it's length
 * Returns     :  VPN_FAILURE/VPN_SUCCESS
 **************************************************************************/
INT4
VpnSetCertInfoRefCnt (tVpnPolicy * pVpnPolicy, INT2 i2Flags)
{
    tUtlIn6Addr         In6Addr;
    tVpnCertInfo       *pVpnCertInfo = NULL;
    tVpnIdInfo         *pVpnIdInfoNode = NULL;
    UINT1              *pu1IdVal = NULL;
    UINT1               au1TempIp[VPN_IPV6_ADDR_LEN + VPN_ONE] = { VPN_ZERO };
    UINT4               u4TempIpAddr = VPN_ZERO;
    INT4                i4IdType = VPN_ZERO;
    INT4                i4Length = VPN_ZERO;

    if (pVpnPolicy == NULL)
    {
        return (VPN_FAILURE);
    }

    i4IdType = pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase1.PolicyPeerID.i2IdType;
    switch (i4IdType)
    {
        case VPN_ID_TYPE_IPV4:
            MEMSET (au1TempIp, VPN_ZERO, sizeof (au1TempIp));
            u4TempIpAddr =
                pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase1.PolicyPeerID.
                uID.Ip4Addr;
            VPN_INET_NTOA (au1TempIp, u4TempIpAddr);
            pu1IdVal = au1TempIp;
            break;

        case VPN_ID_TYPE_IPV6:
            MEMSET (au1TempIp, VPN_ZERO, sizeof (au1TempIp));
            MEMCPY (&In6Addr, &(pVpnPolicy->uVpnKeyMode.VpnIkeDb.
                                IkePhase1.PolicyPeerID.uID.Ip6Addr),
                    sizeof (tUtlIn6Addr));
            MEMCPY (au1TempIp, INET_NTOA6 (In6Addr), VPN_IPV6_ADDR_LEN);
            pu1IdVal = au1TempIp;
            break;

        case VPN_ID_TYPE_EMAIL:
            pu1IdVal =
                pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase1.PolicyPeerID.
                uID.au1Email;
            break;

        case VPN_ID_TYPE_FQDN:
            pu1IdVal =
                pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase1.PolicyPeerID.
                uID.au1Fqdn;
            break;

        case VPN_ID_TYPE_KEYID:
            pu1IdVal =
                pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase1.PolicyPeerID.
                uID.au1KeyId;
            break;

        default:
            /* Do nothing as no id is applicable for this policty */
            return (VPN_SUCCESS);
    }

    i4Length = (INT4) STRLEN (pu1IdVal);

    pVpnIdInfoNode = VpnGetRemoteIdInfo (i4IdType, pu1IdVal, i4Length);

    if (pVpnIdInfoNode == NULL)
    {
        return VPN_FAILURE;
    }

    i4Length = (INT4) STRLEN (pVpnIdInfoNode->ac1IdKey);

    pVpnCertInfo =
        VpnGetCertInfoFromKey ((CONST UINT1 *) pVpnIdInfoNode->ac1IdKey,
                               i4Length);
    if (pVpnCertInfo == NULL)
    {
        return VPN_FAILURE;
    }

    if (i2Flags == VPN_REF_INCR)
    {
        pVpnCertInfo->i4RefCnt++;
    }
    else
    {
        if (pVpnCertInfo->i4RefCnt > VPN_ZERO)
        {
            pVpnCertInfo->i4RefCnt--;
        }
    }

    return (VPN_SUCCESS);
}

/**************************************************************************
 * Function    :  VpnSetRemoteIdRefCnt
 * Description :  Gives the remote identity information matching the given
 *                parameters.
 * Input       :  Identity type, value and it's length
 * Returns     :  VPN_FAILURE/VPN_SUCCESS
 **************************************************************************/
INT4
VpnSetRemoteIdRefCnt (tVpnPolicy * pVpnPolicy, INT2 i2Flags)
{
    tUtlIn6Addr         In6Addr;
    tVpnIdInfo         *pVpnIdInfoNode = NULL;
    UINT1              *pu1IdVal = NULL;
    UINT1               au1TempIp[VPN_IPV6_ADDR_LEN + VPN_ONE] = { VPN_ZERO };
    UINT4               u4TempIpAddr = VPN_ZERO;
    INT4                i4IdType = VPN_ZERO;
    INT4                i4Length = VPN_ZERO;

    if (pVpnPolicy == NULL)
    {
        return (VPN_FAILURE);
    }

    i4IdType = pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase1.PolicyPeerID.i2IdType;
    switch (i4IdType)
    {
        case VPN_ID_TYPE_IPV4:
            MEMSET (au1TempIp, VPN_ZERO, sizeof (au1TempIp));
            u4TempIpAddr =
                pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase1.PolicyPeerID.
                uID.Ip4Addr;
            VPN_INET_NTOA (au1TempIp, u4TempIpAddr);
            pu1IdVal = au1TempIp;
            break;

        case VPN_ID_TYPE_IPV6:
            MEMSET (au1TempIp, VPN_ZERO, sizeof (au1TempIp));
            MEMCPY (&In6Addr, &(pVpnPolicy->uVpnKeyMode.VpnIkeDb.
                                IkePhase1.PolicyPeerID.uID.Ip6Addr),
                    sizeof (tUtlIn6Addr));
            MEMCPY (au1TempIp, INET_NTOA6 (In6Addr), VPN_IPV6_ADDR_LEN);
            pu1IdVal = au1TempIp;
            break;

        case VPN_ID_TYPE_EMAIL:
            pu1IdVal =
                pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase1.PolicyPeerID.
                uID.au1Email;
            break;

        case VPN_ID_TYPE_FQDN:
            pu1IdVal =
                pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase1.PolicyPeerID.
                uID.au1Fqdn;
            break;

        case VPN_ID_TYPE_DN:
            pu1IdVal =
                pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase1.PolicyPeerID.
                uID.au1Dn;
            break;

        case VPN_ID_TYPE_KEYID:
            pu1IdVal =
                pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase1.PolicyPeerID.
                uID.au1KeyId;
            break;

        default:
            /* Do nothing as no id is applicable for this policty */
            return (VPN_SUCCESS);
    }

    i4Length = (INT4) STRLEN (pu1IdVal);

    TMO_SLL_Scan (&gVpnRemoteIdList, pVpnIdInfoNode, tVpnIdInfo *)
    {
        if (i4IdType == (INT4) VPN_ID_TYPE (pVpnIdInfoNode))
        {
            if (STRNCASECMP
                (VPN_ID_VALUE (pVpnIdInfoNode), pu1IdVal, i4Length) == VPN_ZERO)
            {
                if (i2Flags == VPN_REF_INCR)
                {
                    pVpnIdInfoNode->i4RefCnt++;
                }
                else
                {
                    if (pVpnIdInfoNode->i4RefCnt > VPN_ZERO)
                    {
                        pVpnIdInfoNode->i4RefCnt--;
                    }
                }
                return (VPN_SUCCESS);
            }
        }
    }

    return (VPN_FAILURE);
}

/**************************************************************************
 * Function    :  VpnSetRavpnPoolRefCnt
 * Description :  Gives the remote identity information matching the given
 *                parameters.
 * Input       :  increment/Decrement flag 
 * Returns     :  NONE
 **************************************************************************/
INT4
VpnSetRavpnPoolRefCnt (INT2 i2Flags)
{
    tVpnRaAddressPool  *pVpnRaAddressPoolNode = NULL;
    tSNMP_OCTET_STRING_TYPE *pVpnRaAddressPoolName = NULL;

    pVpnRaAddressPoolName =
        allocmem_octetstring (RA_ADDRESS_POOL_NAME_LENGTH + VPN_ONE);
    if (pVpnRaAddressPoolName == NULL)
    {
        return VPN_FAILURE;
    }

    if (nmhGetFirstIndexFsVpnRaAddressPoolTable (pVpnRaAddressPoolName) ==
        SNMP_FAILURE)
    {
        free_octetstring (pVpnRaAddressPoolName);
        return VPN_FAILURE;
    }

    pVpnRaAddressPoolNode =
        VpnGetRaAddrPool (pVpnRaAddressPoolName->pu1_OctetList,
                          pVpnRaAddressPoolName->i4_Length);
    if (pVpnRaAddressPoolNode != NULL)
    {
        if (i2Flags == VPN_REF_INCR)
        {
            pVpnRaAddressPoolNode->i4RefCnt++;
        }
        else
        {
            if (pVpnRaAddressPoolNode->i4RefCnt > VPN_ZERO)
            {
                pVpnRaAddressPoolNode->i4RefCnt--;
            }
        }
    }
    free_octetstring (pVpnRaAddressPoolName);
    return VPN_SUCCESS;
}

#ifdef IKE_WANTED
/**************************************************************************
 * Function    :  VpnUtilCheckIfRaAddrFree
 * Description :  Checks if the specified address is free or assigned       
 * Input       :  pAllocRaAddrList = Pointer to the Allocated IP address list
 * Returns     :  VPN_SUCCESS/VPN_FAILURE   
 **************************************************************************/
INT4
VpnUtilCheckIfRaAddrFree (tTMO_SLL * pAllocRaAddrList, tVpnIpAddr * pVpnIpAddr)
{
    tVpnAllocRaAddrNode *pNode = NULL;

    SLL_SCAN (pAllocRaAddrList, pNode, tVpnAllocRaAddrNode *)
    {
        if (MEMCMP (&(pNode->IpAddr),
                    pVpnIpAddr, sizeof (tVpnIpAddr)) == VPN_ZERO)
        {
            return VPN_FAILURE;
        }
    }
    return VPN_SUCCESS;            /* Yes, the addr is free */

}

/**************************************************************************
 * Function    :  VpnUtilCheckRaAddrExistsforPeer
 * Description :  Checks if the specified address is free or assigned       
 * Input       :  pAllocRaAddrList = Pointer to the Allocated IP address list
 * Returns     :  VPN_SUCCESS/VPN_FAILURE   
 **************************************************************************/
INT4
VpnUtilCheckRaAddrExistsforPeer (tVpnIpAddr * pVpnPeerAddr,
                                 tVpnIpAddr * pVpnIpAddr, UINT4 *pu4PrefixLen)
{
    tVpnRaAddressPool  *pVpnRaAddressPoolNode = NULL;
    tVpnAllocRaAddrNode *pNode = NULL;

    TMO_SLL_Scan (&gVpnRaAddressPoolList, pVpnRaAddressPoolNode,
                  tVpnRaAddressPool *)
    {
        SLL_SCAN (&pVpnRaAddressPoolNode->VpnAllocAddrList, pNode,
                  tVpnAllocRaAddrNode *)
        {
            if (MEMCMP (&(pNode->PeerIpAddr), pVpnPeerAddr,
                        sizeof (tVpnIpAddr)) == VPN_ZERO)
            {
                MEMCPY (pVpnIpAddr, &(pNode->IpAddr), sizeof (tVpnIpAddr));
                *pu4PrefixLen =
                    pVpnRaAddressPoolNode->u4VpnRaAddressPoolPrefixLen;
                return VPN_SUCCESS;
            }
        }
    }
    return VPN_FAILURE;            /* Address is not aasigned to the particular peer */
}

/**************************************************************************
 * Function    :  VpnUtilAddAddrToAllocList
 * Description :  Adds the Specified IP address to the Allocated IP 
 *             :  address list. 
 * Input       :  pAllocRaAddrList : Pointer to the Allocated IP address list
 *                u4Addr : IP address.  
 * Returns     :  VPN_SUCCESS/VPN_FAILURE   
 **************************************************************************/
INT4
VpnUtilAddAddrToAllocList (tTMO_SLL * pAllocRaAddrList,
                           tVpnIpAddr * pVpnIpAddr, tVpnIpAddr * pVpnPeerAddr)
{
    tVpnAllocRaAddrNode *pNode = NULL;

    if (MemAllocateMemBlock
        (VPN_RA_ADDRESS_NODE_PID, (UINT1 **) (VOID *) &pNode) == MEM_FAILURE)
    {
        return (VPN_FAILURE);
    }

    if (pNode != NULL)
    {
        TMO_SLL_Init_Node ((tTMO_SLL_NODE *) pNode);
        MEMCPY (&(pNode->IpAddr), pVpnIpAddr, sizeof (tVpnIpAddr));
        MEMCPY (&(pNode->PeerIpAddr), pVpnPeerAddr, sizeof (tVpnIpAddr));
        pNode->pNextAllocAddrNode = NULL;

        TMO_SLL_Add ((tTMO_SLL *) pAllocRaAddrList, (tTMO_SLL_NODE *) pNode);
        return VPN_SUCCESS;
    }

    return VPN_FAILURE;
}

/**************************************************************************
 * Function    :  VpnUtilDelAddrFromAllocList 
 * Description :  Deletes the Specified IP address from the Allocated IP 
 *             :  address list. 
 * Input       :  pAllocRaAddrList : Pointer to the Allocated IP address list
 *                u4Addr : IP address.  
 * Returns     :  VPN_SUCCESS/VPN_FAILURE   
 **************************************************************************/
INT4
VpnUtilDelAddrFromAllocList (tVpnIpAddr * pVpnIpAddr)
{
    tVpnRaAddressPool  *pVpnRaAddressPoolNode = NULL;
    tVpnAllocRaAddrNode *pNode = NULL;

    /* Get a Free Address */
    TMO_SLL_Scan (&gVpnRaAddressPoolList, pVpnRaAddressPoolNode,
                  tVpnRaAddressPool *)
    {

        if (pVpnIpAddr->u4AddrType !=
            pVpnRaAddressPoolNode->VpnRaAddressPoolStart.u4AddrType)
        {
            continue;
        }

        if ((((MEMCMP (&(pVpnIpAddr),
                       &(pVpnRaAddressPoolNode->VpnRaAddressPoolStart),
                       sizeof (tVpnIpAddr))) > VPN_ZERO) &&
             ((MEMCMP (&(pVpnIpAddr),
                       &(pVpnRaAddressPoolNode->VpnRaAddressPoolEnd),
                       sizeof (tVpnIpAddr))) < VPN_ZERO)) ||
            (((MEMCMP (&(pVpnIpAddr),
                       &(pVpnRaAddressPoolNode->VpnRaAddressPoolStart),
                       sizeof (tVpnIpAddr))) == VPN_ZERO) ||
             ((MEMCMP (&(pVpnIpAddr),
                       &(pVpnRaAddressPoolNode->VpnRaAddressPoolEnd),
                       sizeof (tVpnIpAddr))) == VPN_ZERO)))
        {
            /* Delete the entry from the Allocated IP address list
             * in the pool. */
            TMO_SLL_Scan (&pVpnRaAddressPoolNode->VpnAllocAddrList, pNode,
                          tVpnAllocRaAddrNode *)
            {
                if (MEMCMP (&(pNode->IpAddr), pVpnIpAddr,
                            sizeof (tVpnIpAddr)) == VPN_ZERO)
                {
                    TMO_SLL_Delete (&pVpnRaAddressPoolNode->VpnAllocAddrList,
                                    (tTMO_SLL_NODE *) pNode);
                    MemReleaseMemBlock (VPN_RA_ADDRESS_NODE_PID,
                                        (UINT1 *) pNode);
                    return VPN_SUCCESS;
                }
            }
        }                        /* Address Pool Found */
    }                            /* Scan Ends */
    return VPN_FAILURE;
}

/**************************************************************************
 * Function    :  VpnUtilGetFreeAddrFromPool 
 * Description :  Gets a Free IP address from the address pool                  
 * Input       :  None                                                       
 * Returns     :  VPN_SUCCESS/VPN_FAILURE   
 **************************************************************************/
INT4
VpnUtilGetFreeAddrFromPool (tVpnIpAddr * pVpnPeerAddr, tVpnIpAddr * pFreeIPAddr,
                            UINT4 *pu4PrefixLen)
{
    tVpnRaAddressPool  *pVpnRaAddressPoolNode = NULL;
    tVpnIpAddr          VpnIpAddr;
    UINT4               u4PrefixLen = VPN_ZERO;

    MEMSET (&VpnIpAddr, VPN_ZERO, sizeof (tVpnIpAddr));
    VpnDsLock ();

    /* Check if any address is assigned to the particular peer. 
       If already assigned give the same IP , else get free IP from the 
       address pool  */
    if (VpnUtilCheckRaAddrExistsforPeer (pVpnPeerAddr, &VpnIpAddr,
                                         &u4PrefixLen) == VPN_SUCCESS)
    {
        MEMCPY (pFreeIPAddr, &VpnIpAddr, sizeof (tVpnIpAddr));
        *pu4PrefixLen = u4PrefixLen;
        VpnDsUnLock ();
        return VPN_SUCCESS;
    }

    VpnIpAddr.u4AddrType = pVpnPeerAddr->u4AddrType;
    /* Get a Free Address */
    TMO_SLL_Scan (&gVpnRaAddressPoolList, pVpnRaAddressPoolNode,
                  tVpnRaAddressPool *)
    {
        if (pVpnRaAddressPoolNode->i4RowStatus == ACTIVE)
        {
            /* Copy the start IP address. Loop untill you get the unassigned 
             * IP address. Try to optimise this below code (loop) */

            while (VPN_TRUE)
            {
                if (VpnUtilGetNextAddr (&VpnIpAddr,
                                        &(pVpnRaAddressPoolNode->
                                          VpnRaAddressPoolStart),
                                        &(pVpnRaAddressPoolNode->
                                          VpnRaAddressPoolEnd)) == VPN_FAILURE)
                {
                    break;
                }

                if (VpnUtilCheckIfRaAddrFree (&pVpnRaAddressPoolNode->
                                              VpnAllocAddrList,
                                              &VpnIpAddr) == VPN_FAILURE)
                {
                    continue;
                }

                if (VpnUtilAddAddrToAllocList (&pVpnRaAddressPoolNode->
                                               VpnAllocAddrList,
                                               &VpnIpAddr,
                                               pVpnPeerAddr) == VPN_SUCCESS)
                {
                    MEMCPY (pFreeIPAddr, &VpnIpAddr, sizeof (tVpnIpAddr));
                    *pu4PrefixLen =
                        pVpnRaAddressPoolNode->u4VpnRaAddressPoolPrefixLen;
                    VpnDsUnLock ();
                    return VPN_SUCCESS;
                }                /* Addr marked as allocated */
            }                    /* while loop */
        }                        /* Pool status is up and there are free addresses */
    }
    VpnDsUnLock ();
    return VPN_FAILURE;
}
#endif /* IKE_WANTED ENDS */

/**************************************************************************/
/*  Function Name   : VPNCheckIpInRAAddrPool                              */
/*  Description     : This function checks whether the given IP is in RA  */
/*                    Address pool or not                                 */
/*  Input (s)       : pu4IpAddr- IpAddress to be checked                  */
/*  Output(s)       : None.                                               */
/*  Returns         : OSIX_FAILURE/OSIX_SUCCESS.                          */
/**************************************************************************/
INT4
VPNCheckIpInRAAddrPool (UINT4 pu4IpAddr)
{
    tSNMP_OCTET_STRING_TYPE *pVpnRaAddressPoolName = NULL;
    tVpnRaAddressPool  *pVpnRaAddressPoolNode = NULL;

    VpnDsLock ();

    pVpnRaAddressPoolName =
        allocmem_octetstring (RA_ADDRESS_POOL_NAME_LENGTH + VPN_ONE);
    if (pVpnRaAddressPoolName == NULL)
    {
        VpnDsUnLock ();
        return OSIX_FAILURE;
    }

    if (nmhGetFirstIndexFsVpnRaAddressPoolTable (pVpnRaAddressPoolName) ==
        SNMP_FAILURE)
    {
        free_octetstring (pVpnRaAddressPoolName);
        VpnDsUnLock ();
        return OSIX_FAILURE;
    }
    pVpnRaAddressPoolNode =
        VpnGetRaAddrPool (pVpnRaAddressPoolName->pu1_OctetList,
                          pVpnRaAddressPoolName->i4_Length);

    if (pVpnRaAddressPoolNode == NULL)
    {
        free_octetstring (pVpnRaAddressPoolName);
        VpnDsUnLock ();
        return OSIX_FAILURE;
    }
    if ((pu4IpAddr != VPN_ZERO) &&
        ((pu4IpAddr >= pVpnRaAddressPoolNode->VpnRaAddressPoolStart.
          uIpAddr.Ip4Addr) &&
         (pu4IpAddr <= pVpnRaAddressPoolNode->VpnRaAddressPoolEnd.
          uIpAddr.Ip4Addr)))
    {
        free_octetstring (pVpnRaAddressPoolName);
        VpnDsUnLock ();
        return OSIX_SUCCESS;
    }
    free_octetstring (pVpnRaAddressPoolName);
    VpnDsUnLock ();
    return OSIX_FAILURE;
}

/* This function get the Tunnel endpoint using the from the configured SA. */
UINT4
VpnGetTunnelEndpoint (UINT2 u2PolicyIndex, UINT4 u4SrcAddr, UINT4 u4DestAddr,
                      UINT4 u4Protocol, UINT4 *u4TunFlag, UINT4 *u4RuleFlag,
                      UINT4 *pu4Src, UINT4 *pu4Dest)
{
    tVpnPolicy         *pVpnPolicy = NULL;
    UINT4               u4LocIpAddr = VPN_ZERO, u4DestIpAddr = VPN_ZERO;
    UINT4               u4LocIpMask = VPN_ZERO, u4DestIpMask = VPN_ZERO;
    UINT4               u4VpnSecProto = VPN_ZERO, u4VpnPolFlag = VPN_ZERO;
    INT4                i4RAVPNServer = VPN_INVALID;

    *u4TunFlag = VPN_ZERO;        /* Initialize the flag */

    VpnDsLock ();

    nmhGetFsVpnGlobalStatus ((INT4 *) u4TunFlag);
    if (*u4TunFlag == VPN_DISABLE)
    {
        *u4TunFlag = VPN_ZERO;
        VpnDsUnLock ();
        return SEC_FAILURE;
    }

    if (!u2PolicyIndex)            /* No policy index !! */
    {
        /* Scan through the database and find appropriate policy
         * matching the given parameters.
         */
        TMO_SLL_Scan (&VpnPolicyList, pVpnPolicy, tVpnPolicy *)
        {
            if (pVpnPolicy->u1VpnPolicyRowStatus != ACTIVE)
            {
                continue;
            }

            u4LocIpAddr = pVpnPolicy->LocalProtectNetwork.IpAddr.
                uIpAddr.Ip4Addr;
            IPV4_MASKLEN_TO_MASK (u4LocIpMask, pVpnPolicy->LocalProtectNetwork.
                                  u4AddrPrefixLen);
            u4DestIpAddr = pVpnPolicy->RemoteProtectNetwork.IpAddr.
                uIpAddr.Ip4Addr;

            IPV4_MASKLEN_TO_MASK (u4DestIpMask, pVpnPolicy->
                                  RemoteProtectNetwork.u4AddrPrefixLen);

            /* Match all the traffic coming from SafeNet because it needs
             * to be further processed. (u4Protocol == VPN_ANY_PROTOCOL)
             */
            if (((u4SrcAddr & u4LocIpMask) == (u4LocIpAddr & u4LocIpMask)) &&
                ((u4DestAddr & u4DestIpMask) ==
                 (u4DestIpAddr & u4DestIpMask)) &&
                ((u4Protocol == pVpnPolicy->u4VpnProtocol) ||
                 (pVpnPolicy->u4VpnProtocol == VPN_ANY_PROTOCOL) ||
                 (u4Protocol == VPN_ANY_PROTOCOL)))
            {
                *u4RuleFlag = u4VpnPolFlag = pVpnPolicy->u4VpnPolicyFlag;
                *pu4Src = (UINT4) pVpnPolicy->LocalTunnTermAddr.uIpAddr.Ip4Addr;
                *pu4Dest = (UINT4) pVpnPolicy->
                    RemoteTunnTermAddr.uIpAddr.Ip4Addr;
                if (u4VpnPolFlag == VPN_APPLY)
                {
                    nmhGetFsVpnRaServer (&i4RAVPNServer);
                    if (i4RAVPNServer == RAVPN_SERVER)
                    {
                        *u4TunFlag = ((pVpnPolicy->u1VpnMode == VPN_TUNNEL) ?
                                      ((VPN_SKIP_NAT | VPN_SKIP_FWL))
                                      : (VPN_SKIP_FWL));
                    }
                    else
                    {
                        *u4TunFlag = (((pVpnPolicy->u1VpnMode == VPN_TUNNEL) &&
                                       (pVpnPolicy->u4VpnPolicyType !=
                                        VPN_IKE_RA_PRESHAREDKEY)
                                       && (pVpnPolicy->u4VpnPolicyType !=
                                           VPN_XAUTH)
                                       && (pVpnPolicy->u4VpnPolicyType !=
                                           VPN_IKE_XAUTH_CERT)
                                       && (pVpnPolicy->u4VpnPolicyType !=
                                           VPN_IKE_RA_CERT)) ? ((VPN_SKIP_NAT |
                                                                 VPN_SKIP_FWL))
                                      : (VPN_SKIP_FWL));
                    }
                }
                VpnDsUnLock ();
                return (SEC_SUCCESS);
            }
            /* For transport mode traffic, the traffic selectors will not
               match, if virtual servers are configured. This is because, the
               src address will have the ip of vlan host, since NAT is yet to
               be done. So, for this case, check for Dest ip & mask alone
               along with tunnel mode. If this returns a match and tunnel
               mode is transport, then return success.
             */
            else
            {
                if (((u4DestAddr & u4DestIpMask) ==
                     (u4DestIpAddr & u4DestIpMask)) &&
                    ((u4Protocol == pVpnPolicy->u4VpnProtocol) ||
                     (pVpnPolicy->u4VpnProtocol == VPN_ANY_PROTOCOL) ||
                     (u4Protocol == VPN_ANY_PROTOCOL)) &&
                    (pVpnPolicy->u1VpnMode == VPN_TRANSPORT))
                {
                    *u4TunFlag = VPN_SKIP_FWL;
                    VpnDsUnLock ();
                    return (SEC_SUCCESS);
                }
            }
        }
    }
    else
    {
        TMO_SLL_Scan (&VpnPolicyList, pVpnPolicy, tVpnPolicy *)
        {
            u4LocIpAddr = pVpnPolicy->LocalProtectNetwork.IpAddr.
                uIpAddr.Ip4Addr;
            IPV4_MASKLEN_TO_MASK (u4LocIpMask, pVpnPolicy->LocalProtectNetwork.
                                  u4AddrPrefixLen);
            u4DestIpAddr = pVpnPolicy->RemoteProtectNetwork.IpAddr.
                uIpAddr.Ip4Addr;
            IPV4_MASKLEN_TO_MASK (u4DestIpMask,
                                  pVpnPolicy->RemoteProtectNetwork.
                                  u4AddrPrefixLen);
            u4VpnSecProto = pVpnPolicy->u4VpnSecurityProtocol;
            u4VpnPolFlag = pVpnPolicy->u4VpnPolicyFlag;

            if (u2PolicyIndex == pVpnPolicy->u4VpnPolicyIndex)
            {
                if (pVpnPolicy->u1VpnPolicyRowStatus != ACTIVE)
                {
                    *u4TunFlag = VPN_ZERO;
                    VpnDsUnLock ();
                    return SEC_FAILURE;
                }

                if (u4VpnPolFlag == VPN_APPLY)
                {

                    nmhGetFsVpnRaServer (&i4RAVPNServer);
                    if (i4RAVPNServer == RAVPN_SERVER)
                    {
                        *u4TunFlag = ((pVpnPolicy->u1VpnMode == VPN_TUNNEL) ?
                                      ((VPN_SKIP_NAT | VPN_SKIP_FWL))
                                      : (VPN_SKIP_FWL));
                    }
                    else
                    {
                        *u4TunFlag = (((pVpnPolicy->u1VpnMode == VPN_TUNNEL) &&
                                       (pVpnPolicy->u4VpnPolicyType !=
                                        VPN_IKE_RA_PRESHAREDKEY)
                                       && (pVpnPolicy->u4VpnPolicyType !=
                                           VPN_XAUTH)
                                       && (pVpnPolicy->u4VpnPolicyType !=
                                           VPN_IKE_RA_CERT)
                                       && (pVpnPolicy->u4VpnPolicyType !=
                                           VPN_IKE_XAUTH_CERT)) ? ((VPN_SKIP_NAT
                                                                    |
                                                                    VPN_SKIP_FWL))
                                      : (VPN_SKIP_FWL));
                    }
                }

                *u4RuleFlag = u4VpnPolFlag;

                VpnDsUnLock ();
                return (SEC_SUCCESS);
            }
        }
    }

    /* Policy is disabled or not a tunnel policy, so do NAT */
    *u4TunFlag = VPN_ZERO;
    UNUSED_PARAM (u4VpnSecProto);
    VpnDsUnLock ();
    return SEC_FAILURE;
}

/* This function get the Tunnel endpoint using the from the configured SA. */
UINT4
VpnGetTunnelEndpointForIp6 (tIp6Addr * pIp6SrcAddr, tIp6Addr * pIp6DstAddr,
                            UINT4 u4Protocol, UINT4 *u4TunFlag,
                            UINT4 *u4RuleFlag)
{
    tVpnPolicy         *pVpnPolicy = NULL;
    tIp6Addr            LocIp6Addr, DestIp6Addr;
    UINT4               u4LocIp6Mask = VPN_ZERO, u4DestIp6Mask = VPN_ZERO;
    UINT4               u4VpnPolFlag = VPN_ZERO;
    INT4                i4VpnStatus = VPN_ZERO;

    VpnDsLock ();

    /* Always skip NAT processing for IPv6 packets */
    *u4TunFlag = VPN_SKIP_NAT;

    nmhGetFsVpnGlobalStatus (&i4VpnStatus);
    if (i4VpnStatus == VPN_DISABLE)
    {
        VpnDsUnLock ();
        return SEC_FAILURE;
    }

    /* Scan through the database and find appropriate policy
     * matching the given parameters.
     */
    TMO_SLL_Scan (&VpnPolicyList, pVpnPolicy, tVpnPolicy *)
    {
        if (pVpnPolicy->u1VpnPolicyRowStatus != ACTIVE)
        {
            continue;
        }

        MEMCPY (&LocIp6Addr, &pVpnPolicy->LocalProtectNetwork.IpAddr.
                uIpAddr.Ip6Addr, sizeof (tIp6Addr));

        u4LocIp6Mask = pVpnPolicy->LocalProtectNetwork.u4AddrPrefixLen;

        MEMCPY (&DestIp6Addr, &pVpnPolicy->RemoteProtectNetwork.IpAddr.
                uIpAddr.Ip6Addr, sizeof (tIp6Addr));

        u4DestIp6Mask = pVpnPolicy->RemoteProtectNetwork.u4AddrPrefixLen;

        if ((Ip6AddrMatch (pIp6SrcAddr, &LocIp6Addr, (INT4) u4LocIp6Mask) ==
             TRUE)
            && (Ip6AddrMatch (pIp6DstAddr, &DestIp6Addr, (INT4) u4DestIp6Mask)
                == TRUE) && ((u4Protocol == pVpnPolicy->u4VpnProtocol)
                             || (pVpnPolicy->u4VpnProtocol ==
                                 VPN_ANY_PROTOCOL)))
        {
            *u4RuleFlag = u4VpnPolFlag = pVpnPolicy->u4VpnPolicyFlag;
            if (u4VpnPolFlag == VPN_APPLY)
            {
                *u4TunFlag = (*u4TunFlag | VPN_SKIP_FWL);
            }
            VpnDsUnLock ();
            return (SEC_SUCCESS);
        }
    }

    VpnDsUnLock ();
    return SEC_FAILURE;
}

/* **************************************************************************
* Function Name    :  VpnGetIpHeaderLength                                 *
* Description      :  This function gets the IP header length from the     *
*                     IP header in the packet.                             *
*                                                                          *
* Input (s)        :  pBuf - This stores the full IP packet along          *
*                     with TCP/UDP header and payload.                     *
*                                                                          *
* Output (s)       :  None                                                 *
* Returns          :  Length of the IP header.                             *
*                                                                          *
****************************************************************************/
UINT4
VpnGetIpHeaderLength (tCRU_BUF_CHAIN_HEADER * pBuf)
{
    UINT1               u1IpHeaderLen = VPN_ZERO;

    if (CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u1IpHeaderLen,
                                   IP_PKT_OFF_HLEN, SEC_IP_HEADER_LEN)
        == CRU_FAILURE)
    {
        return (OSIX_FAILURE);
    }
    u1IpHeaderLen = u1IpHeaderLen & SEC_IP_HEADER_LEN_MASK;

    return ((UINT4) (u1IpHeaderLen * VPN_FOUR));
}

/**************************************************************************
 * Function    :  VpnGetIpTotalLen 
 * Description :  Gets the total length present in an IP Packet
 * Input       :  pBuf : Cru Buf Pointer to IP Packet
 * Returns     :  Total Length of IP Packet
 **************************************************************************/
UINT2
VpnGetIpTotalLen (tCRU_BUF_CHAIN_HEADER * pBuf)
{
    UINT2               u2IpTotLen = ZERO;

    if (CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2IpTotLen,
                                   VPN_IP_TOT_LEN_OFFSET, VPN_IP_TOL_LEN_FIELD)
        == CRU_FAILURE)
    {
        return (OSIX_FAILURE);
    }
    return (OSIX_NTOHS (u2IpTotLen));
}

/**************************************************************************
 * Function    :  VpnGetEspSpi 
 * Description :  Gets the SPI value present in ESP Packet.
 * Input       :  pBuf : Cru buf Pointer to IP Packet
 * Returns     :  SPI Value Present in ESP Packet
 **************************************************************************/
UINT4
VpnGetEspSpi (tCRU_BUF_CHAIN_HEADER * pBuf)
{
    UINT2               u2EspSpi = ZERO;
    UINT4               u4IpHdrLen = ZERO;

    u4IpHdrLen = VpnGetIpHeaderLength (pBuf);
    if (CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2EspSpi,
                                   (u4IpHdrLen + VPN_UDP_HEADER_LENGTH),
                                   NON_ESP_HDR_LEN) == CRU_FAILURE)
    {
        return (OSIX_FAILURE);
    }
    return (OSIX_NTOHS (u2EspSpi));
}

/**************************************************************************
 * Function    :  VpnGetUdpLen 
 * Description :  Gets the Len value present in UDP Packet.
 * Input       :  pBuf : Cru buf Pointer to IP Packet
 * Returns     :  Length value Present in UDP Packet
 **************************************************************************/
UINT2
VpnGetUdpLen (tCRU_BUF_CHAIN_HEADER * pBuf)
{
    UINT2               u2UdpLen = ZERO;
    UINT4               u4IpHdrLen = ZERO;

    u4IpHdrLen = VpnGetIpHeaderLength (pBuf);
    if (CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2UdpLen,
                                   (u4IpHdrLen + VPN_UDP_HEADER_LENGTH -
                                    VPN_UDP_LEN_OFFSET), VPN_WORD_LEN)
        == CRU_FAILURE)
    {
        return (OSIX_FAILURE);
    }
    return (OSIX_NTOHS (u2UdpLen));
}

/**************************************************************************
 * Function    :  VpnSetUdpLenChkSum
 * Description :  Sets the Len and CheckSum value in UDP Packet.
 *             :  CheckSum is not calculated , it is set to ZERO.
 * Input       :  pBuf : Cru buf Pointer to IP Packet
 *             :  u2UdpLen : Length of UDP Packet
 * Returns     :  None
 **************************************************************************/
VOID
VpnSetUdpLenChkSum (tCRU_BUF_CHAIN_HEADER * pBuf, UINT2 u2UdpLen)
{
    UINT2               u2UdpChkSum = ZERO;
    UINT4               u4IpHdrLen = ZERO;

    u4IpHdrLen = VpnGetIpHeaderLength (pBuf);
    u2UdpLen = OSIX_NTOHS (u2UdpLen);
    if (CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u2UdpLen,
                                   (u4IpHdrLen + VPN_UDP_HEADER_LENGTH -
                                    VPN_UDP_LEN_OFFSET),
                                   VPN_IP_TOL_LEN_FIELD) == CRU_FAILURE)
    {
        return;
    }
    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u2UdpChkSum,
                               (u4IpHdrLen + VPN_UDP_HEADER_LENGTH -
                                VPN_UDP_CHKSUM_OFFSET), VPN_IP_TOL_LEN_FIELD);
    return;
}

/* **************************************************************************
 * Function Name : VpnIkeNattStripUdpHeader
 * Description   : Checks if the traffic of interest is IPSec traffic originated
 *                 or destined to us.
 * Input(s)      : IPHdr
 * Output(s)     : Cru Buf
 * Return value  : Returns the calculated prefix length
 * **************************************************************************/
UINT4
VpnIkeNattStripUdpHeader (tCRU_BUF_CHAIN_HEADER * pBuf)
{
    UINT4               u4IpHdrLen = ZERO;
    UINT4               u4StripPos = ZERO;
    UINT2               u2EspSpi = ZERO;
    UINT2               u2StripLen = ZERO;
    UINT2               u2IpTotLen = ZERO;
    UINT2               u2UdpTotLen = ZERO;
    UINT2               u2IpChkSum = ZERO;
    UINT1               u1Prot = SEC_ESP;
    tCRU_BUF_CHAIN_HEADER *pRestBuf = NULL;
    t_IP_HEADER         IpHdr;
#ifdef NAT_WANTED
    if (NatUtilIsIkeNattPassthroughTraffic (pBuf) == OSIX_SUCCESS)
    {
        return VPN_OTHER_PACKET;
    }
#endif
    u2UdpTotLen = VpnGetUdpLen (pBuf);
    if ((u2UdpTotLen - VPN_UDP_HEADER_LENGTH) == VPN_ONE)
    {
        /* IKE Natt Keep Alive Packet */
        return VPN_DROP_PACKET;
    }

    u4IpHdrLen = VpnGetIpHeaderLength (pBuf);
    u2EspSpi = VpnGetEspSpi (pBuf);

    u2StripLen = VPN_UDP_HEADER_LENGTH;
    u4StripPos = u4IpHdrLen;
    if (u2EspSpi == ZERO)
    {
        u2StripLen = NON_ESP_HDR_LEN;
        u4StripPos = u4IpHdrLen + VPN_UDP_HEADER_LENGTH;
        u2UdpTotLen = VpnGetUdpLen (pBuf);
        VpnSetUdpLenChkSum (pBuf, (UINT2) (u2UdpTotLen - NON_ESP_HDR_LEN));
        u1Prot = SEC_UDP;
    }

    if (CRU_BUF_Fragment_BufChain (pBuf, u4StripPos, &pRestBuf) == CRU_FAILURE)
    {
        return VPN_DROP_PACKET;
    }

    if (CRU_BUF_Move_ValidOffset (pRestBuf, u2StripLen) == CRU_FAILURE)
    {
        return VPN_DROP_PACKET;
    }
    CRU_BUF_Concat_MsgBufChains (pBuf, pRestBuf);
    u2IpTotLen = VpnGetIpTotalLen (pBuf);
    u2IpTotLen = (UINT2) (u2IpTotLen - u2StripLen);
    u2IpTotLen = OSIX_NTOHS (u2IpTotLen);
    /*
     * Update Total Len Field in IP Header
     */
    if (CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u2IpTotLen,
                                   VPN_IP_TOT_LEN_OFFSET,
                                   VPN_IP_TOL_LEN_FIELD) == CRU_FAILURE)
    {
        return VPN_DROP_PACKET;
    }
    /*
     * Update Protocol Field to ESP
     */
    if (CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u1Prot,
                                   VPN_PROT_OFFSET_IN_IPHEADER,
                                   VPN_IP_PROT_FIELD_LEN) == CRU_FAILURE)
    {
        return VPN_DROP_PACKET;
    }

    if (CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u2IpChkSum,
                                   VPN_IP_CHKSUM_OFFSET,
                                   VPN_WORD_LEN) == CRU_FAILURE)
    {
        return VPN_DROP_PACKET;
    }

    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &IpHdr, ZERO, u4IpHdrLen);
    u2IpChkSum = Secv4IPCalcChkSum ((UINT1 *) &IpHdr, u4IpHdrLen);
    u2IpChkSum = OSIX_NTOHS (u2IpChkSum);

    if (CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u2IpChkSum,
                                   VPN_IP_CHKSUM_OFFSET,
                                   VPN_WORD_LEN) == CRU_FAILURE)
    {
        return VPN_DROP_PACKET;
    }
    if (u2StripLen == VPN_UDP_HEADER_LENGTH)
    {
        /* IPSec Packet */
        return VPN_IPSEC_PACKET;
    }
    else
    {
        return VPN_IKE_PACKET;
    }
}

/* **************************************************************************/
/* Function Name : VpnIsIpsecTraffic                                        */
/* Description   : Checks if the traffic of interest is IPSec traffic       */
/*                 originated or destined to us.                            */
/* Input(s)      : pBuf        - Cru Buffer                                 */
/*                 u1Direction - Direction (OUTBOUND /INBOUND)              */
/* Output(s)     : None                                                     */
/* Return value  : Returns the calculated prefix length                     */
/* **************************************************************************/
UINT4
VpnIsIpsecTraffic (tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 u1Direction)
{
    tVpnUdpHdr          UdpHdr;
    INT4                i4VpnStatus = VPN_DISABLE;
    INT4                i4RetVal = OSIX_FAILURE;
    VOID               *pUdpHdr = NULL;
    UINT2               u2UdpSrcPort = ZERO;
    UINT2               u2UdpDstPort = ZERO;
    t_IP_HEADER         IpHdr;
    t_IP_HEADER        *pIpHdr = NULL;


    nmhGetFsVpnGlobalStatus (&i4VpnStatus);
    if (i4VpnStatus == VPN_DISABLE)
    {
        return (VPN_OTHER_PACKET);
    }

    pIpHdr = (t_IP_HEADER *) (void *) CRU_BUF_Get_DataPtr_IfLinear (pBuf,
                                                                    VPN_ZERO,
                                                                    IP_HDR_LEN);
    if (pIpHdr == NULL)
    {
        IPSEC_MEMSET (&IpHdr, VPN_ZERO, sizeof (t_IP_HEADER));
        IPSEC_COPY_FROM_BUF (pBuf, (UINT1 *) &IpHdr, VPN_ZERO, IP_HDR_LEN);
        pIpHdr = &IpHdr;
    }

    if (((pIpHdr->u1Proto == SEC_AH) || (pIpHdr->u1Proto == SEC_ESP)) &&
        ((SecUtilIpIfIsOurAddress (IPSEC_NTOHL (pIpHdr->u4Src)) == TRUE) ||
         (SecUtilIpIfIsOurAddress (IPSEC_NTOHL (pIpHdr->u4Dest)) == TRUE)))
    {
        return (VPN_IPSEC_PACKET);
    }
    if ((pIpHdr->u1Proto != SEC_UDP))
    {
        return (VPN_OTHER_PACKET);
    }

    pUdpHdr = CRU_BUF_Get_DataPtr_IfLinear ((pBuf), IP_HDR_LEN,
                                            VPN_UDP_HEADER_LENGTH);
    if (pUdpHdr != NULL)
    {
        u2UdpSrcPort = *(UINT2 *) pUdpHdr;
        u2UdpDstPort = *(UINT2 *) (pUdpHdr + sizeof (UINT2));

        u2UdpSrcPort = OSIX_NTOHS (u2UdpSrcPort);
        u2UdpDstPort = OSIX_NTOHS (u2UdpDstPort);
    }
    else
    {
        MEMSET (&UdpHdr, VPN_ZERO, sizeof (tVpnUdpHdr));
        if (CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &UdpHdr, IP_HDR_LEN,
                                       VPN_UDP_HEADER_LENGTH) == CRU_FAILURE)
        {
            return (VPN_DROP_PACKET);
        }

        u2UdpSrcPort = OSIX_NTOHS (UdpHdr.u2LocalPort);
        u2UdpDstPort = OSIX_NTOHS (UdpHdr.u2RemotePort);
    }

    if (u1Direction == VPN_INBOUND)
    {
        if (((SecUtilIpIfIsOurAddress (IPSEC_NTOHL (pIpHdr->u4Src)) == TRUE) ||
             (SecUtilIpIfIsOurAddress (IPSEC_NTOHL (pIpHdr->u4Dest)) == TRUE))
            && ((u2UdpSrcPort == VPN_IKE_NATT_PORT)
                || (u2UdpDstPort == VPN_IKE_NATT_PORT)))
        {
            i4RetVal = (INT4) VpnIkeNattStripUdpHeader (pBuf);
            return ((UINT4) i4RetVal);
        }
    }
    return (VPN_OTHER_PACKET);
}

/*****************************************************************************/
/* Function Name : VpnIsIkeTraffic                                           */
/* Description   : Checks if the traffic of interest is Ike/NAT-T traffic and*/
/*                 originated or destined to us.                             */
/* Input(s)      : IPHdr                                                     */
/* Output(s)     : None                                                      */
/* Return value  : Returns the calculated prefix length                      */
/*****************************************************************************/
UINT4
VpnIsIkeTraffic (t_IP_HEADER * pIPHdr, tCRU_BUF_CHAIN_HEADER * pBuf)
{
    VOID               *pUdpHdr = NULL;
    tVpnUdpHdr          UdpHdr;
    UINT2               u2UdpSrcPort = VPN_ZERO;
    UINT2               u2UdpDstPort = VPN_ZERO;
    UINT1               au1InitCookie[VPN_IKE_INIT_COOKIE_LEN] = { VPN_ZERO };
    INT4                i4VpnStatus = VPN_DISABLE;

    MEMSET (au1InitCookie, VPN_ZERO, VPN_IKE_INIT_COOKIE_LEN);

    nmhGetFsVpnGlobalStatus (&i4VpnStatus);
    if (i4VpnStatus == VPN_DISABLE)
    {
        return (OSIX_FAILURE);
    }

    if ((pIPHdr->u1Proto != SEC_UDP))
    {
        return (OSIX_FAILURE);
    }
    pUdpHdr = CRU_BUF_Get_DataPtr_IfLinear ((pBuf), VPN_BUFFER_SIZE, VPN_FOUR);
    if (pUdpHdr != NULL)
    {
        u2UdpSrcPort = *(UINT2 *) pUdpHdr;
        u2UdpDstPort = *(UINT2 *) (pUdpHdr + sizeof (UINT2));
        u2UdpSrcPort = OSIX_NTOHS (u2UdpSrcPort);
        u2UdpDstPort = OSIX_NTOHS (u2UdpDstPort);
    }
    else
    {
        MEMSET (&UdpHdr, VPN_ZERO, sizeof (tVpnUdpHdr));
        if (CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &UdpHdr, IP_HDR_LEN,
                                       VPN_UDP_HEADER_LENGTH) == CRU_FAILURE)
        {
            return (OSIX_FAILURE);
        }

        u2UdpSrcPort = OSIX_NTOHS (UdpHdr.u2LocalPort);
        u2UdpDstPort = OSIX_NTOHS (UdpHdr.u2RemotePort);
    }

#ifdef NAT_WANTED
    if (((SecUtilIpIfIsOurAddress (pIPHdr->u4Src) == TRUE) ||
         (SecUtilIpIfIsOurAddress (pIPHdr->u4Dest) == TRUE)) &&
        ((u2UdpSrcPort == VPN_IKE_PORT) || (u2UdpDstPort == VPN_IKE_PORT)))
    {
        /* For IKE traffics check whether NAT has seen this traffic. If NAT has
         * already seen, it implies this traffic is a VPN pass through. So VPN
         * module should not try to interpret this traffic 
         * */

        /* Query NAT for the initiator cookie. VPN module process traffic 
         * only if NAT doesnt have this cookie in its database. */
        if (CRU_BUF_Copy_FromBufChain (pBuf, au1InitCookie,
                                       (UINT4) (((pIPHdr->
                                                  u1Ver_hdrlen & VPN_MASK_0xF) *
                                                 VPN_FOUR) + VPN_UDP_HDR_LEN),
                                       VPN_IKE_INIT_COOKIE_LEN) == CRU_FAILURE)
        {
            return (OSIX_FAILURE);
        }
        if (OSIX_SUCCESS == NatUtilIsVPNPassthroughTraffic (au1InitCookie))
        {
            /* NAT database contain this cookie so it is a VPN Passthrough 
             * traffic */
            return (OSIX_FAILURE);
        }
    }
#endif

    /* For Outgoing traffic
     * 1. Will be called, before NAT processing. Hence, no need to check
     *    NAT-T traffic.
     * 2. This is mainly for gateway initiated/responded IKE and responded 
     *    NAT-T traffic.
     */
    if (SecUtilIpIfIsOurAddress (pIPHdr->u4Src) == TRUE)
    {
        if (((u2UdpSrcPort == VPN_IKE_PORT) &&
             (u2UdpDstPort == VPN_IKE_PORT)) ||
            (u2UdpSrcPort == VPN_IKE_NATT_PORT))
        {
            return (OSIX_SUCCESS);
        }
    }

    /* For Incoming traffic
     * 1. If dest is ours, port is 500, then it is meant to gateway vpn.
     * 2. If dest is ours, dest port is 4500, then it is meant to gateway 
     *    vpn (NAT-T traffic - client termination).
     * 3. If dest is ours, source port is 4500, dst port is non-4500, then 
     *    it is meant to vpn client, inside our n/w (VPN passthrough)
     */
    if (SecUtilIpIfIsOurAddress (pIPHdr->u4Dest) == TRUE)
    {
        if (((u2UdpSrcPort == VPN_IKE_PORT) &&
             (u2UdpDstPort == VPN_IKE_PORT)) ||
            (u2UdpDstPort == VPN_IKE_NATT_PORT))
        {
            return (OSIX_SUCCESS);
        }
    }

    return (OSIX_FAILURE);
}

/*****************************************************************************/
/* Function Name : VpnUtilGetIkeVersion                                      */
/* Description   : This function gets the ike version from the policy        */
/*                 database                                                  */
/* Input(s)      : pPeerIpAddr - Perr IP                                     */
/*                                                                           */
/* Output(s)     : *pu1IkeVersion - Ike version                              */
/* Return value  : VPN_SUCCESS/VPN_FAILURE                                   */
/*****************************************************************************/
INT1
VpnUtilGetIkeVersion (tVpnIpAddr * pPeerIpAddr, UINT1 *pu1IkeVersion)
{
    tVpnPolicy         *pVpnPolicy = NULL;
    UINT1               u1Match = FALSE;
    INT4                i4RAVPNServer = VPN_INVALID;
    INT4                i4TunFlag = VPN_ZERO;

    nmhGetFsVpnGlobalStatus (&i4TunFlag);
    nmhGetFsVpnRaServer (&i4RAVPNServer);
    if ((i4TunFlag == VPN_DISABLE) || (i4RAVPNServer != RAVPN_CLIENT))
    {
        return VPN_FAILURE;
    }
    /* Scan through the database and find appropriate policy
     * matching the given parameters.
     */
    TMO_SLL_Scan (&VpnPolicyList, pVpnPolicy, tVpnPolicy *)
    {
        if (pVpnPolicy->u1VpnPolicyRowStatus != ACTIVE)
        {
            continue;
        }
        if ((MEMCMP (pPeerIpAddr, &(pVpnPolicy->RemoteTunnTermAddr),
                     sizeof (tVpnIpAddr)) == VPN_ZERO) &&
            (pVpnPolicy->u4VpnPolicyFlag == VPN_APPLY) &&
            ((pVpnPolicy->u4VpnPolicyType == VPN_IKE_RA_PRESHAREDKEY) ||
             (pVpnPolicy->u4VpnPolicyType == VPN_XAUTH) ||
             (pVpnPolicy->u4VpnPolicyType == VPN_IKE_RA_CERT) ||
             (pVpnPolicy->u4VpnPolicyType == VPN_IKE_XAUTH_CERT)))
        {
            *pu1IkeVersion = pVpnPolicy->u1VpnIkeVer;
            u1Match = TRUE;
            break;
        }
    }
    if (u1Match == TRUE)
    {
        return VPN_SUCCESS;
    }
    return VPN_FAILURE;

}

/*****************************************************************************/
/*                                                                           */
/* FUNCTION NAME    : VpnUtilValidateIpAddress                               */
/* DESCRIPTION      : This function returns the type of the IpAdress it      */
/*                    belongs.i,e BroadCast,MultiCast etc.                   */
/* INPUT            : u4IpAddr - IpAddress for Validation                    */
/* OUTPUT           : The Type IpAdrress belongs.                            */
/* RETURNS          : CFA_BCAST_ADDR     or                                  */
/*                    CFA_MCAST_ADDR     or                                  */
/*                    CFA_ZERO_NETW_ADDR or                                  */
/*                    CFA_LOOPBACK_ADDR  or                                  */
/*                    CFA_UCAST_ADDR     or                                  */
/*                    CFA_INVALID_ADDR   or                                  */
/*                        CFA_CLASSE_ADDR                                    */
/*****************************************************************************/
UINT4
VpnUtilValidateIpAddress (UINT4 u4IpAddr)
{
    /*If Address is 255.255.255.255 */
    if ((u4IpAddr & VPN_MASK_0xffffffff) == VPN_MASK_0xffffffff)
    {
        return (VPN_BCAST_ADDR);
    }

    /*If Address is 127.x.x.x */
    if ((u4IpAddr & VPN_MASK_0xff000000) == VPN_MASK_0x7f000000)
    {
        return (VPN_LOOPBACK_ADDR);
    }

    /*If Address Multicast, i.e between 224.x.x.x and 239.x.x.x */
    if ((u4IpAddr & VPN_MASK_0xf0000000) == VPN_MASK_0xe0000000)
    {
        return (VPN_MCAST_ADDR);
    }

    /*If Address is 0.0.0.0 */
    if ((u4IpAddr & VPN_MASK_0xffffffff) == VPN_ZERO)
    {
        return (VPN_ZERO_ADDR);
    }

    /*If Address is 0.x.x.x */
    if ((u4IpAddr & VPN_MASK_0xff000000) == VPN_ZERO)
    {
        return (VPN_ZERO_NETW_ADDR);
    }

    /*If Address is CLASS-E, i.e betwwen 240.x.x.x and 254.x.x.x */
    if ((u4IpAddr & VPN_MASK_0xf8000000) == VPN_MASK_0xf0000000)
    {
        return (VPN_CLASSE_ADDR);
    }

    /*If Address 255.x.x.x */
    if ((u4IpAddr & VPN_MASK_0xff000000) == VPN_MASK_0xff000000)
    {
        return (VPN_INVALID_ADDR);
    }

    /*If Address CLASS-A and A.0.0.0 or A.255.255.255 */
    if ((u4IpAddr & VPN_MASK_0x80000000) == VPN_ZERO)
    {
        if ((u4IpAddr & VPN_MASK_0x00ffffff) == VPN_ZERO)
        {
            return (VPN_CLASS_NETADDR);
        }

        else if ((u4IpAddr & VPN_MASK_0x00ffffff) == VPN_MASK_0x00ffffff)
        {
            return (VPN_CLASS_BCASTADDR);
        }
    }
    /*If Address CLASS-B and B.0.0.0 or B.255.255.255 */
    if ((u4IpAddr & VPN_MASK_0xc0000000) == VPN_MASK_0x80000000)
    {
        if ((u4IpAddr & VPN_MASK_0x0000ffff) == VPN_ZERO)
        {
            return (VPN_CLASS_NETADDR);
        }

        else if ((u4IpAddr & VPN_MASK_0x0000ffff) == VPN_MASK_0x0000ffff)
        {
            return (VPN_CLASS_BCASTADDR);
        }
    }

    /*If Address CLASS-C and C.0.0.0 or C.255.255.255 */
    if ((u4IpAddr & VPN_MASK_0xe0000000) == VPN_MASK_0xc0000000)
    {
        if ((u4IpAddr & VPN_MASK_0x000000ff) == VPN_ZERO)
        {
            return (VPN_CLASS_NETADDR);
        }

        else if ((u4IpAddr & VPN_MASK_0x000000ff) == VPN_MASK_0x000000ff)
        {
            return (VPN_CLASS_BCASTADDR);
        }
    }
    return (VPN_UCAST_ADDR);
}

/***************************************************************************
 * Function Name :  VpnUtilValidateMask                                    *
 *                                                                         *
 * Description   :  This function the is used to check whether a mask is   *
 *                  valid or not                                           *
 *                                                                         *
 * Input (s)     :  u4Mask - The mask                                      *
 *                                                                         *
 * Output (s)    :  None                                                   *
 *                                                                         *
 * Returns       :  OSIX_FAILURE  /  OSIX_SUCCESS                          *
 ***************************************************************************/
UINT4
VpnUtilValidateMask (UINT4 u4Mask)
{
    UINT1               u1Counter = VPN_ZERO;
    while (gau4VpnCidrSubnetMask[u1Counter] != u4Mask)
    {
        ++u1Counter;
        if (u1Counter > VPN_MAX_CIDR)
        {
            return VPN_FAILURE;
        }
    }
    return VPN_SUCCESS;
}

/***************************************************************************
 * Function Name :  VpnUtilGetNextAddr                                     *
 *                                                                         *
 * Description   :  The function returns the next ip address which resides *
 *                  pool end address range. If the given address is NULL   *
 *                  It will copy the start address.                        *
 *                                                                         *
 * Input (s)     :  pVpnIpAddr - IP address to which the next address is   *
 *                               required                                  *
 *                  pVpnStartIp - Pool start address                       *
 *                  pVpnEndIp   - Poll end address                         *
 *                                                                         *
 * Output (s)    :  pVpnIpAddr - Next IP address of the given IP address   *
 *                  incase of the success                                  *
 *                                                                         *
 * Returns       :  VPN_FAILURE  /  VPN_SUCCESS                            *
 *                                                                         *
 * NOTE          : pVpnIpAddr acts as input and as well as output          *
 *                 argument to the function                                *
 ***************************************************************************/

UINT4
VpnUtilGetNextAddr (tVpnIpAddr * pVpnIpAddr, tVpnIpAddr * pVpnStartIp,
                    tVpnIpAddr * pVpnEndIp)
{
    INT1                i1Index = VPN_ZERO;
    tIp6Addr            NullIp6Addr;

    MEMSET (&NullIp6Addr, VPN_ZERO, sizeof (tIp6Addr));

    if (pVpnIpAddr->u4AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (pVpnIpAddr->uIpAddr.Ip4Addr == VPN_ZERO)
        {
            pVpnIpAddr->uIpAddr.Ip4Addr = pVpnStartIp->uIpAddr.Ip4Addr;
            return VPN_SUCCESS;
        }

        pVpnIpAddr->uIpAddr.Ip4Addr++;

        if (pVpnIpAddr->uIpAddr.Ip4Addr <= pVpnEndIp->uIpAddr.Ip4Addr)
        {
            return VPN_SUCCESS;
        }
        else
        {
            return VPN_FAILURE;
        }
    }
    else if (pVpnIpAddr->u4AddrType == IPVX_ADDR_FMLY_IPV6)
    {
        if (MEMCMP (&NullIp6Addr, &(pVpnIpAddr->uIpAddr.Ip6Addr),
                    sizeof (tIp6Addr)) == VPN_ZERO)
        {
            MEMCPY (pVpnIpAddr, pVpnStartIp, sizeof (tVpnIpAddr));
            return VPN_SUCCESS;
        }

        for (i1Index = (IPVX_IPV6_ADDR_LEN - VPN_ONE);
             i1Index >= VPN_ZERO; i1Index--)
        {
            if (pVpnIpAddr->uIpAddr.Ip6Addr.u1_addr[i1Index] == VPN_OFFSET_0xFF)
            {
                pVpnIpAddr->uIpAddr.Ip6Addr.u1_addr[i1Index] = VPN_ZERO;
            }
            else
            {
                pVpnIpAddr->uIpAddr.Ip6Addr.u1_addr[i1Index]++;

                /* It should be less than the end IP adddress */
                if (MEMCMP (&(pVpnIpAddr->uIpAddr.Ip6Addr),
                            &(pVpnEndIp->uIpAddr.Ip6Addr),
                            sizeof (tIp6Addr)) <= VPN_ZERO)
                {
                    return VPN_SUCCESS;
                }
                else
                {
                    /* Did not get address with in the pool */
                    return VPN_FAILURE;
                }
            }
        }
    }

    return VPN_FAILURE;
}

/***************************************************************************
 * Function Name : VpnUtilConvMask2Prefix
 * Description   : Converts the given network mask into equivalent prefix
 *                 length.
 * Input(s)      : u4NetMask - Subnet mask to be converted
 * Output(s)     : None
 * Return value  : Returns the calculated prefix length
 ***************************************************************************/
UINT2
VpnUtilConvMask2Prefix (UINT4 u4NetMask)
{
    UINT2               u2CidrVal = VPN_ZERO;

    while (gau4VpnCidrSubnetMask[u2CidrVal] != u4NetMask)
    {
        ++u2CidrVal;
        if (u2CidrVal > VPN_MAX_CIDR)
        {
            break;
        }
    }

    return (u2CidrVal);
}

/**************************************************************************/
/*  Function Name   : VpnReadPreSharedKeyFromNVRAM                        */
/*  Description     : It reads remote id information from the file and    */
/*                  : populates the VpnRemoteIdList structure             */
/*  Input(s)        : None                                                */
/*  Output(s)       : None                                                */
/*  Returns         : VPN_SUCCESS or VPN_FAILURE                          */
/**************************************************************************/
INT4
VpnReadPreSharedKeyFromNVRAM (VOID)
{
    UINT1               au1Buf[KEY_FILE_LINE_LENGTH] = { VPN_ZERO };
    UINT1               au1FileName[ISS_CONFIG_FILE_NAME_LEN] = { VPN_ZERO };
    UINT1              *pu1KeyString = NULL;
    UINT1              *pu1IdType = NULL;
    UINT1              *pu1DisplayId = NULL;
    INT4                i4Index = VPN_ZERO;
    INT4                i4FileDesc = VPN_INVALID;
    INT1                i1IdType = VPN_ZERO;

    MEMSET (au1Buf, VPN_ZERO, sizeof (au1Buf));
    MEMSET (au1FileName, VPN_ZERO, sizeof (au1FileName));

    STRCPY (au1FileName, PRESHARED_KEY_SAVE_FILE);

    VpnUtilCallBack (VPN_ACCESS_SECURE_MEMORY, ISS_SRAM_FILENAME_MAPPING,
                     PRESHARED_KEY_SAVE_FILE, au1FileName);
    i4FileDesc = FileOpen (au1FileName, OSIX_FILE_RO);
    if (i4FileDesc < VPN_ZERO)
    {
        gu4VpnRestoredConfig = OSIX_TRUE;
        return VPN_SUCCESS;
    }
    /* The remote identity, its type and preshared key are read from the file */
    while (VpnReadLine (i4FileDesc, au1Buf) == VPN_SUCCESS)
    {
        i4Index = VPN_ZERO;
        pu1IdType = au1Buf;
        while (au1Buf[i4Index] != ',')
        {
            i4Index++;
        }
        au1Buf[i4Index++] = '\0';
        pu1DisplayId = au1Buf + i4Index;
        while (au1Buf[i4Index] != ',')
        {
            i4Index++;
        }
        au1Buf[i4Index++] = '\0';
        pu1KeyString = au1Buf + i4Index;
        i1IdType = (INT1) ATOI (pu1IdType);
        /* Data structure is updated */
        if (VpnRestorePreSharedKey (pu1DisplayId, pu1KeyString, i1IdType)
            == VPN_FAILURE)
        {
            FileClose (i4FileDesc);
            gu4VpnRestoredConfig = OSIX_TRUE;
            return VPN_FAILURE;
        }
        MEMSET (au1Buf, VPN_ZERO, KEY_FILE_LINE_LENGTH);
    }
    FileClose (i4FileDesc);
    gu4VpnRestoredConfig = OSIX_TRUE;
    return VPN_SUCCESS;
}

/**************************************************************************/
/*  Function Name   : VpnReadLine                                         */
/*  Description     : This function reads a line from the file            */
/*  Input(s)        : i4FileDesc - File Descriptor                        */
/*  Output(s)       : pi1Buf - Buffer to store the line                   */
/*  Returns         : VPN_SUCCESS or VPN_FAILURE                          */
/**************************************************************************/
INT4
VpnReadLine (INT4 i4FileDesc, UINT1 *pi1Buf)
{
    INT2                i2index = VPN_ZERO;
    INT2                i2ReadCount = VPN_ZERO;
    INT1                i1Char = VPN_ZERO;

    while ((i2ReadCount =
            (INT2) FileRead (i4FileDesc, (CHR1 *) & i1Char,
                             VPN_ONE)) == VPN_ONE)
    {
        i2index++;
        if ('\n' == i1Char)
        {
            break;
        }
        if ('\r' == i1Char)
        {
            continue;
        }
        *pi1Buf++ = (UINT1) i1Char;
    }
    *pi1Buf = '\0';

    if (i2ReadCount == VPN_ZERO)
    {
        return VPN_FAILURE;
    }
    return VPN_SUCCESS;
}

/**************************************************************************/
/*  Function Name   : VpnRestorePreSharedKey                              */
/*  Description     : It populates the VpnRemoteId structure              */
/*  Input(s)        : pu1IdValue   - The address of peer node             */
/*                  : pu1PresharedKey - The pre shared key                */
/*                  : i1IdType     - Type of peer ID                      */
/*  Output(s)       : None                                                */
/*  Returns         : VPN_SUCCESS or VPN_FAILURE                          */
/**************************************************************************/
INT4
VpnRestorePreSharedKey (UINT1 *pu1IdValue,
                        UINT1 *pu1PresharedKey, INT1 i1IdType)
{
    tVpnIdInfo         *pVpnIdInfo = NULL;
    tSNMP_OCTET_STRING_TYPE IdValue;
    tSNMP_OCTET_STRING_TYPE IdKey;
    tUtlIn6Addr         In6Addr;
    UINT1               au1IpAddr[VPN_IPV6_ADDR_LEN + VPN_ONE] = { VPN_ZERO };

    if (i1IdType == CLI_VPN_IKE_KEY_IPV6)
    {
        /* Convert Ipv6 identity to full address string */
        MEMSET (&In6Addr, VPN_ZERO, sizeof (tUtlIn6Addr));
        MEMSET (au1IpAddr, VPN_ZERO, (VPN_IPV6_ADDR_LEN + VPN_ONE));

        INET_ATON6 (pu1IdValue, &In6Addr);
        MEMCPY (au1IpAddr, INET_NTOA6 (In6Addr), (VPN_IPV6_ADDR_LEN + VPN_ONE));

        IdValue.i4_Length = (INT4) STRLEN ((au1IpAddr) + VPN_ONE);
        IdValue.pu1_OctetList = au1IpAddr;
    }
    else
    {
        IdValue.i4_Length = (INT4) STRLEN ((pu1IdValue) + VPN_ONE);
        IdValue.pu1_OctetList = pu1IdValue;
    }

    IdKey.i4_Length = (INT4) STRLEN (pu1PresharedKey);
    IdKey.pu1_OctetList = pu1PresharedKey;

    pVpnIdInfo =
        VpnGetRemoteIdInfo (i1IdType, pu1IdValue, (INT4) STRLEN (pu1IdValue));
    /* Entry already exists */
    if (pVpnIdInfo != NULL)
    {
        if (nmhSetFsVpnRemoteIdStatus (i1IdType, &IdValue, NOT_IN_SERVICE) ==
            SNMP_FAILURE)
        {
            return (VPN_FAILURE);
        }

        if (nmhSetFsVpnRemoteIdKey (i1IdType, &IdValue, &IdKey) == SNMP_FAILURE)
        {
            return VPN_FAILURE;
        }
    }
    /* No entry, so create a new entry */
    else
    {
        if (nmhSetFsVpnRemoteIdStatus (i1IdType, &IdValue, CREATE_AND_WAIT) ==
            SNMP_FAILURE)
        {
            return (VPN_FAILURE);
        }

        if (nmhSetFsVpnRemoteIdAuthType (i1IdType, &IdValue,
                                         VPN_AUTH_PRESHARED) == SNMP_FAILURE)
        {
            return VPN_FAILURE;
        }

        if (nmhSetFsVpnRemoteIdKey (i1IdType, &IdValue, &IdKey) == SNMP_FAILURE)
        {
            return VPN_FAILURE;
        }

    }
    if (nmhSetFsVpnRemoteIdStatus (i1IdType, &IdValue, ACTIVE) == SNMP_FAILURE)
    {
        return (VPN_FAILURE);
    }
    return VPN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VpnUtilCallBack                                    */
/*                                                                           */
/*     DESCRIPTION      : This function calls the registered custom function */
/*                                                                           */
/*     INPUT            : u4Event - Event for a specified custom feature     */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/
INT4
VpnUtilCallBack (UINT4 u4Event, ...)
{
    va_list             vaList;
    UINT4              *apu4VaArgs[VPN_CALLBACK_MAX_ARGS] = { NULL };
    INT4                i4RetVal = VPN_SUCCESS;
    UINT4               u4SecMemAction = VPN_ZERO;
    UINT1               u1Index = VPN_ZERO;

    MEMSET (&vaList, VPN_ZERO, sizeof (vaList));
    MEMSET (apu4VaArgs, VPN_ZERO, sizeof (apu4VaArgs));
    switch (u4Event)
    {
        case VPN_READ_PRE_SHARED_KEY_FROM_NVRAM:
            if (VPN_CALLBACK_FN[u4Event].pVpnReadPreSharedKeyFromNVRAM != NULL)
            {
                i4RetVal = VPN_CALLBACK_FN[u4Event].
                    pVpnReadPreSharedKeyFromNVRAM ();
            }
            break;

        case VPN_ACCESS_SECURE_MEMORY:
        {

            va_start (vaList, u4Event);
            /* Walk through the arguements and store in args array.  */
            for (u1Index = VPN_ZERO; u1Index < VPN_CALLBACK_MAX_ARGS; u1Index
                 = (UINT1) (u1Index + VPN_ONE))
            {
                apu4VaArgs[u1Index] = va_arg (vaList, UINT4 *);
            }
            va_end (vaList);

            if (NULL != VPN_CALLBACK_FN[u4Event].pVpnCustSecureMemAccess)
            {
                u4SecMemAction = PTR_TO_U4 (apu4VaArgs[VPN_INDEX_0]);
                if (ISS_FAILURE ==
                    VPN_CALLBACK_FN[u4Event].pVpnCustSecureMemAccess
                    (u4SecMemAction, ISS_FILE, apu4VaArgs[VPN_INDEX_1],
                     apu4VaArgs[VPN_INDEX_2]))
                {
                    i4RetVal = VPN_FAILURE;
                }
                break;
            }
        }
            break;

        default:
            break;
    }
    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : VpnRegCallBackforVPNModule                           */
/*                                                                           */
/* Description        : This function is invoked to register the call backs  */
/*                      from  application.                                   */
/*                                                                           */
/* Input(s)           : u4Event - Event for which callback is registered     */
/*                      pFsCbInfo - Call Back Function                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CLI__SUCCESS/CLI__FAILURE                            */
/*****************************************************************************/
INT4
VpnUtilRegCallBackforVPNModule (UINT4 u4Event, tFsCbInfo * pFsCbInfo)
{
    INT4                i4RetVal = VPN_SUCCESS;

    switch (u4Event)
    {
        case VPN_READ_PRE_SHARED_KEY_FROM_NVRAM:
            VPN_CALLBACK_FN[u4Event].pVpnReadPreSharedKeyFromNVRAM =
                pFsCbInfo->pVpnReadPreSharedKeyFromNVRAM;
            break;

        case VPN_ACCESS_SECURE_MEMORY:
            VPN_CALLBACK_FN[u4Event].pVpnCustSecureMemAccess =
                pFsCbInfo->pIssCustSecureProcess;
            break;

        default:
            i4RetVal = VPN_FAILURE;
            break;
    }
    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : VpnApiRestorePreSharedKeyFromNvRam                   */
/*                                                                           */
/* Description        : This function is invoked to retrieve pre shared      */
/*                      key from file.                                       */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                      None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : VPN__SUCCESS/VPN__FAILURE                            */
/*****************************************************************************/
INT4
VpnApiRestorePreSharedKeyFromNvRam ()
{
    return (VpnUtilCallBack (VPN_READ_PRE_SHARED_KEY_FROM_NVRAM));
}

/***************************************************************************/
/*  Function Name : VpnGetAssoIntfForVpnc                                  */
/*                                                                         */
/*  Description   : This function handles the outgoing packet.             */
/*                :                                                        */
/*  Input(s)      : pBuf - Buffer contains IP Secured packet.              */
/*                :                                                        */
/*  Output(s)     : pu4AssoIfIndex - Associated IfIndex of VPNC IfIndex    */
/*                : pu4Dest - Destination IP Address                       */
/*                :                                                        */
/*  Return Values : VPN_SUCCESS/VPN_FAILURE                                */
/***************************************************************************/
UINT4
VpnGetAssoIntfForVpnc (tIP_BUF_CHAIN_HEADER * pCBuf,
                       UINT4 *pu4AssoIfIndex, UINT4 *pu4Dest)
{
    t_IP_HEADER         IpHdr;
    t_IP_HEADER        *pTmpIpHdr = NULL;
    UINT1              *pu1Buf = NULL;
    UINT4               u4OptLen = VPN_ZERO;
    UINT4               u4PktSize = VPN_ZERO;
    UINT4               u4TunFlag = VPN_ZERO;
    UINT4               u4RuleFlag = VPN_ZERO;
    UINT4               u4Src = VPN_ZERO;
    UINT2               u2DstPortNumber = SEC_ANY_PORT;
    UINT2               u2SrcPortNumber = SEC_ANY_PORT;

    u4PktSize = CRU_BUF_Get_ChainValidByteCount (pCBuf);

    pu1Buf = CRU_BUF_Get_DataPtr_IfLinear (pCBuf, VPN_ZERO, u4PktSize);

    if (pu1Buf == NULL)
    {
        CRU_BUF_Copy_FromBufChain (pCBuf, (UINT1 *) &IpHdr, VPN_ZERO,
                                   SEC_IPV4_HEADER_SIZE);
        pTmpIpHdr = &IpHdr;
        u4OptLen = (UINT4) IPSEC_OLEN (pTmpIpHdr->u1Ver_hdrlen);
        if ((pTmpIpHdr->u1Proto == SEC_TCP) || (pTmpIpHdr->u1Proto == SEC_UDP))
        {
            CRU_BUF_Copy_FromBufChain (pCBuf, (UINT1 *) &u2DstPortNumber,
                                       (SEC_PORT_NUMBER_OFFSET + u4OptLen),
                                       sizeof (UINT2));
            CRU_BUF_Copy_FromBufChain (pCBuf, (UINT1 *) &u2SrcPortNumber,
                                       (SEC_SRC_PORT_NUMBER_OFFSET + u4OptLen),
                                       sizeof (UINT2));
        }
    }
    else
    {
        pTmpIpHdr = (t_IP_HEADER *) (void *) pu1Buf;
        u4OptLen = (UINT4) IPSEC_OLEN (pTmpIpHdr->u1Ver_hdrlen);
        if ((pTmpIpHdr->u1Proto == SEC_TCP) || (pTmpIpHdr->u1Proto == SEC_UDP))
        {
            u2DstPortNumber =
                *((UINT2 *) (void *) (pu1Buf +
                                      (SEC_PORT_NUMBER_OFFSET + u4OptLen)));
            u2DstPortNumber = OSIX_NTOHS (u2DstPortNumber);
            u2SrcPortNumber =
                *((UINT2 *) (void *) (pu1Buf +
                                      (SEC_SRC_PORT_NUMBER_OFFSET + u4OptLen)));
            u2SrcPortNumber = OSIX_NTOHS (u2SrcPortNumber);
        }
    }

    if (VpnGetTunnelEndpoint (VPN_ZERO, OSIX_NTOHL (pTmpIpHdr->u4Src),
                              OSIX_NTOHL (pTmpIpHdr->u4Dest),
                              (UINT2) pTmpIpHdr->u1Proto,
                              &u4TunFlag, &u4RuleFlag, &u4Src, pu4Dest)
        == SEC_FAILURE)
    {
        return (VPN_FAILURE);
    }
    CfaIpIfGetIfIndexFromIpAddress (u4Src, pu4AssoIfIndex);
    return (VPN_SUCCESS);
}

/***************************************************************************/
/*  Function Name : VpnUtilPolicyRestore                                   */
/*                                                                         */
/*  Description   : This function handles the restoration of policy status */
/*                  after the MIB restoration. When the key storage is     */
/*                  skipped for custom functionality, the policy cannot be */
/*                  made active on a particular interface while restoring  */
/*                  the MIB objects from file. This function does the      */
/*                  necessary to make the policy active                    */
/*                                                                         */
/*  Input(s)      : None                                                   */
/*                                                                         */
/*  Output(s)     : None                                                   */
/*                                                                         */
/*  Return Values : VPN_SUCCESS/VPN_FAILURE                                */
/***************************************************************************/
INT4
VpnUtilPolicyRestore ()
{
    tVpnPolicy         *pPolicy = NULL;
    tSNMP_OCTET_STRING_TYPE sPolicyName;
    UINT4               u4TempIfIndex = VPN_ZERO;

    SLL_SCAN (&VpnPolicyList, pPolicy, tVpnPolicy *)
    {
        if (pPolicy != NULL)
        {
            MEMSET (&sPolicyName, VPN_ZERO, sizeof (sPolicyName));
            sPolicyName.i4_Length = (INT4) STRLEN (pPolicy->au1VpnPolicyName);
            sPolicyName.pu1_OctetList = pPolicy->au1VpnPolicyName;
            if (ACTIVE == pPolicy->u1VpnPolicyRowStatus)
            {
                u4TempIfIndex = pPolicy->u4IfIndex;
                if (SNMP_FAILURE ==
                    nmhSetFsVpnPolicyRowStatus (&sPolicyName, NOT_IN_SERVICE))
                {
                    VPN_TRC (ALL_FAILURE_TRC,
                             "Unable to make the policy active\r\n");
                    return VPN_FAILURE;
                }
                if (u4TempIfIndex != VPN_ZERO)
                {
                    pPolicy->u4IfIndex = u4TempIfIndex;
                    if (SNMP_FAILURE ==
                        nmhSetFsVpnPolicyIntfIndex (&sPolicyName,
                                                    (INT4) u4TempIfIndex))
                    {
                        VPN_TRC (ALL_FAILURE_TRC,
                                 "Unable to map policy on the interface\r\n");
                        return VPN_FAILURE;
                    }
                }
                if (SNMP_FAILURE ==
                    nmhSetFsVpnPolicyRowStatus (&sPolicyName, ACTIVE))
                {
                    VPN_TRC (ALL_FAILURE_TRC,
                             "Unable to make the policy active\r\n");
                    return VPN_FAILURE;
                }
            }
        }
    }
    return VPN_SUCCESS;
}

/***************************************************************************/
/*  Function Name : VpnUtilUpdateCertDB                                    */
/*                                                                         */
/*  Description   : This API is used to update the IKE database with the   */
/*                  configured certificate information which is used at    */
/*                  the time of IKE negotiation of peer                    */
/*                                                                         */
/*  Input(s)      : pVpnCertInfo - pointer to certificate information      */
/*                                                                         */
/*  Output(s)     : None                                                   */
/*                                                                         */
/*  Return Values : VPN_FAILURE/VPN_SUCCESS                                */
/***************************************************************************/

INT1
VpnUtilUpdateCertDB (tVpnCertInfo * pVpnCertInfo)
{
    if (VpnIkeImportKey ((UINT1 *) pVpnCertInfo->ac1CertKeyId,
                         (UINT1 *) pVpnCertInfo->ac1CertKeyFileName,
                         (UINT4) pVpnCertInfo->i4CertKeyType) == OSIX_FAILURE)
    {
        return VPN_FAILURE;
    }

    if (VpnIkeImportCert ((UINT1 *) pVpnCertInfo->ac1CertFileName,
                          (UINT1) pVpnCertInfo->i4CertEncType,
                          (UINT1 *) pVpnCertInfo->ac1CertKeyId) == OSIX_FAILURE)
    {
        return VPN_FAILURE;
    }
    return VPN_SUCCESS;
}

/***************************************************************************/
/*  Function Name : VpnUtilDeleteCertKeys                                  */
/*                                                                         */
/*  Description   : This API is used to delete the IKE database with the   */
/*                  configured certificate information which is used at    */
/*                  the time of IKE negotiation of peer                    */
/*                                                                         */
/*  Input(s)      : pVpnCertInfo - pointer to certificate information      */
/*                                                                         */
/*  Output(s)     : None                                                   */
/*                                                                         */
/*  Return Values : ISS_TRUE/ISS_FALSE                                     */
/***************************************************************************/

INT1
VpnUtilDeleteCertKeys (tVpnCertInfo * pVpnCertInfo)
{
    if (VpnIkeDelKeys ((UINT1 *) pVpnCertInfo->ac1CertKeyId,
                       (UINT4) pVpnCertInfo->i4CertKeyType) == OSIX_FAILURE)
    {
        return VPN_FAILURE;
    }
    return VPN_SUCCESS;
}

/***************************************************************************/
/*  Function Name : VpnUtilDeleteCertDB                                    */
/*                                                                         */
/*  Description   : This API is used to delete the IKE database with the   */
/*                  configured certificate information which is used at    */
/*                  the time of IKE negotiation of peer                    */
/*                                                                         */
/*  Input(s)      : pVpnCertInfo - pointer to certificate information      */
/*                                                                         */
/*  Output(s)     : None                                                   */
/*                                                                         */
/*  Return Values : ISS_TRUE/ISS_FALSE                                     */
/***************************************************************************/

INT1
VpnUtilDeleteCertDB (tVpnCertInfo * pVpnCertInfo)
{
    if (VpnIkeDelCerts (CLI_VPN_ERASE_CERT,
                        (UINT1 *) pVpnCertInfo->ac1CertKeyId) == OSIX_FAILURE)
    {
        return VPN_FAILURE;
    }
    return VPN_SUCCESS;
}

/***************************************************************************/
/*  Function Name : VpnUtilUpdateCaCertDB                                  */
/*                                                                         */
/*  Description   : This API is used to update the IKE database with the   */
/*                  configured CA certificate information which is used at */
/*                  the time of IKE negotiation of peer                    */
/*                                                                         */
/*  Input(s)      : pVpnCaCertInfo - pointer to CA certificate information */
/*                                                                         */
/*  Input(s)      : None                                                   */
/*                                                                         */
/*  Output(s)     : None                                                   */
/*                                                                         */
/*  Return Values : VPN_SUCCESS/VPN_FAILURE                                */
/***************************************************************************/

INT1
VpnUtilUpdateCaCertDB (tVpnCaCertInfo * pVpnCaCertInfo)
{

    if (VpnIkeImportCACert ((UINT1 *) pVpnCaCertInfo->ac1CaCertKeyId,
                            (UINT1 *) pVpnCaCertInfo->ac1CaCertFileName,
                            (UINT1) pVpnCaCertInfo->i4CaCertEncType) ==
        OSIX_FAILURE)
    {
        return VPN_FAILURE;
    }

    return VPN_SUCCESS;
}

/***************************************************************************/
/*  Function Name : VpnUtilDeleteCaCertDB                                  */
/*                                                                         */
/*  Description   : This API is used to delete the IKE database with the   */
/*                  configured CA certificate information which is used at */
/*                  the time of IKE negotiation of peer                    */
/*                                                                         */
/*  Input(s)      : pVpnCaCertInfo - pointer to CA certificate information */
/*                                                                         */
/*  Input(s)      : None                                                   */
/*                                                                         */
/*  Output(s)     : None                                                   */
/*                                                                         */
/*  Return Values : VPN_SUCCESS/VPN_FAILURE                                */
/***************************************************************************/
INT1
VpnUtilDeleteCaCertDB (tVpnCaCertInfo * pVpnCaCertInfo)
{
    INT4                i4RetVal = OSIX_FAILURE;

    i4RetVal = VpnIkeDelCerts (CLI_VPN_ERASE_CA_CERT,
                               (UINT1 *) pVpnCaCertInfo->ac1CaCertKeyId);
    if (i4RetVal == OSIX_FAILURE)
    {
        return VPN_FAILURE;
    }
    return VPN_SUCCESS;
}

/***************************************************************************/
/*  Function Name : VpnGetMsrSaveStatus                                    */
/*                                                                         */
/*  Description   : This API is to get the status of MIB save process. In  */
/*                  kernel, MsrGetSaveStatus function is stubbed to return */
/*                  ISS_TRUE always.                                       */
/*                                                                         */
/*  Input(s)      : None                                                   */
/*                                                                         */
/*  Output(s)     : None                                                   */
/*                                                                         */
/*  Return Values : ISS_TRUE/ISS_FALSE                                     */
/***************************************************************************/
INT4
VpnGetMsrSaveStatus (VOID)
{
    INT4                i4MsrSaveStatus = ISS_FALSE;

    i4MsrSaveStatus = MsrGetSaveStatus ();

    return i4MsrSaveStatus;
}

/************************************************************************/
/*  Function Name   : VpnUtilRaAddrDbLookUp                             */
/*  Description     : Check the Status whether Dest Addr is matches     */
/*                    with range between start & end VPN RA Addr Pool's */
/*  Input(s)        : u4DestAddr:Refers  to Dest  Addr of the pkt       */
/*  Output(s)       : one if it matches otherwise zero                  */
/*  Returns         : Returns the 1 or 0                                */
/************************************************************************/
UINT1
VpnUtilRaAddrDbLookUp (UINT4 u4DestAddr)
{
    UINT4               u4StartIpAddr = VPN_ZERO;
    UINT4               u4EndIpAddr = VPN_ZERO;
    UINT1               u1RetVal = SEC_FAILURE;
    tVpnRaAddressPool  *pVpnRaAddressPoolNode = NULL;

    VpnDsLock ();
    do
    {
        pVpnRaAddressPoolNode =
            (tVpnRaAddressPool *) TMO_SLL_First (&gVpnRaAddressPoolList);

        if (pVpnRaAddressPoolNode == NULL)
        {
            break;
        }

        do
        {
            u4StartIpAddr = pVpnRaAddressPoolNode->VpnRaAddressPoolStart.
                uIpAddr.Ip4Addr;

            u4EndIpAddr = pVpnRaAddressPoolNode->VpnRaAddressPoolEnd.
                uIpAddr.Ip4Addr;

            /* Checking whether the dest addr is matches between the RA VPN Addr pool's */
            if ((u4DestAddr >= u4StartIpAddr) && (u4DestAddr <= u4EndIpAddr))
            {
                u1RetVal = SEC_SUCCESS;
                break;
            }
            pVpnRaAddressPoolNode =
                (tVpnRaAddressPool *) TMO_SLL_Next ((tTMO_SLL *) &
                                                    gVpnRaAddressPoolList,
                                                    (tTMO_SLL_NODE *)
                                                    pVpnRaAddressPoolNode);
        }
        while (pVpnRaAddressPoolNode != NULL);
    }
    while (VPN_FALSE);

    VpnDsUnLock ();

    return (u1RetVal);
}

/************************************************************************/
/*  Function Name   : VpnGetPolicyByIndex                               */
/*  Description     : Find the policy name by index from policylist     */
/*  Input(s)        : u4Index:Refers  to Policy Index                   */
/*  Output(s)       : pPolicyName:Refers to policy Name                 */
/*  Returns         : None                                              */
/************************************************************************/
VOID
VpnGetPolicyByIndex (UINT4 u4Index, tSNMP_OCTET_STRING_TYPE * pPolicyName)
{

    tVpnPolicy         *pVpnPolicy = NULL;

    SLL_SCAN (&VpnPolicyList, pVpnPolicy, tVpnPolicy *)
    {
        if (pVpnPolicy != NULL)
        {
            if (u4Index == pVpnPolicy->u4VpnPolicyIndex)
            {
                STRCPY (pPolicyName->pu1_OctetList,
                        pVpnPolicy->au1VpnPolicyName);
                pPolicyName->i4_Length =
                    (INT4) STRLEN (pVpnPolicy->au1VpnPolicyName);
            }
        }
    }

}

/************************************************************************/
/*  Function Name   : VpnValCryptoMapName                               */
/*  Description     : Validates the given crypto map name               */
/*  Input(s)        : pu1Ptr : Pointer to the Crypto Map Name           */
/*  Output(s)       : Returns success If the name is valid              */
/*  Returns         : None                                              */
/************************************************************************/
UINT1
VpnValCryptoMapName (UINT1 *pu1Ptr)
{
    UINT4              u4Count = 0;

    for (u4Count = 0; pu1Ptr[u4Count] != '\0'; u4Count++)
    {
       if (!(islower (pu1Ptr[u4Count])|| isupper(pu1Ptr[u4Count]) || isdigit(pu1Ptr[u4Count])))
       {
          return SNMP_FAILURE;
       }
    }
    return SNMP_SUCCESS;
}



/************************************************************************/
INT4
VpnAccessListCheckIpMask (UINT4 u4LocalMask, UINT4 u4LocalIpAddr,
                          UINT4 u4RemoteMask, UINT4 u4RemoteIpAddr)
{

if (((u4LocalMask == IPVX_IPV4_MAX_MASK_LEN) 
       && ((u4LocalIpAddr) & 0xff) == 0)
       && ((u4RemoteMask == IPVX_IPV4_MAX_MASK_LEN) 
        && ((u4RemoteIpAddr) & 0xff) == 0))
    {
        CLI_SET_ERR (CLI_VPN_ERR_ACL_SRC_DST_PARAM);
        return SNMP_FAILURE;
    }

if ((u4LocalMask == IPVX_IPV4_MAX_MASK_LEN)
         && ((u4LocalIpAddr) & 0xff) == 0)

    {
        CLI_SET_ERR (CLI_VPN_ERR_ACL_SRC_PARAM);
        return SNMP_FAILURE;
    }

if ((u4RemoteMask == IPVX_IPV4_MAX_MASK_LEN)
             && ((u4RemoteIpAddr) & 0xff) == 0)
    {
        CLI_SET_ERR (CLI_VPN_ERR_ACL_DST_PARAM);
        return SNMP_FAILURE;
    }
return SNMP_SUCCESS;

}
/*************************************************************************
* Function    : VpnUtilsIsDn                                             *
* Description : A Domain Name (or DN) is an unambiguous ex: example.com  *
*               or example.cas.com or ex-ample.com or ex-5ample.com      *
*               This function verifies the given string and says it is a *
*               valid DN or not.                                         *
* Input       : String to be validated against DN rules and its length.  *
* Output      : None                                                     *
* Returns     : TRUE if the string is DN else FALSE                      *
*************************************************************************/

INT4
VpnUtilsIsDn (CONST CHR1 * pc1Dn, INT4 i4Length)
{
 
    CHR1    i1Hyphen= '-';
    INT4    i4Count = VPN_ZERO;

    /* String Length should be between 3 to 63*/
    if ((i4Length <= VPN_THREE) && 
        (i4Length > VPN_DOMAIN_NAME_MAX_LEN ))
    {
        return (FALSE);
    }
    /*Should not begin or end with hyphen "-" and characters other than
     * alphanumeric*/
    if (((pc1Dn[VPN_INDEX_0]) == i1Hyphen)        || 
        ((pc1Dn[i4Length - VPN_ONE]) == i1Hyphen) ||
        (TRUE == !isalnum(pc1Dn[VPN_INDEX_0]))    ||
        (TRUE == !isalnum(pc1Dn[i4Length - VPN_ONE])))
    {
        return (FALSE);
    }
   
    /*Domain name shud have only alphanumeric characters and hyphen*/
    while (i4Count < i4Length)
    {
        if (TRUE == !isalnum(pc1Dn[VPN_INDEX_0]))
        { 
            if ((pc1Dn[i4Count] != i1Hyphen))
            {
                return (FALSE); 
            }
        }
        i4Count++;
    }
    return (TRUE);
    
}
/************************************************************************/

#endif
/* EOF */
